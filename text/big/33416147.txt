On the Downlow
{{Infobox film
| name          = On the Downlow
| image         = On-the-downlow-tadeo-garcia.jpg
| image_size    =
| caption       = Theatrical poster
| director      = Tadeo Garcia
| producer      = Tadeo Garcia
| writer        = Tadeo Garcia, Roger B. Domian
| starring      = Michael Cortez Tony Sancho
| music          = Gregory Martin
| cinematography = Chris Buzek  Tadeo Garcia
| editing        = Roger B. Domian   Tadeo Garcia
| studio         = Iconoclast Films 
| distributor    = 
| released       = 2004
| runtime        = 
| country        = United States
| language       = English, Spanish
| budget         =
| gross          = 
}}
On the Downlow is the 2004 debut film of director Tadeo Garcia, a low-budget film starring Michael Cortez and Tony Sancho.

Proving very popular with the film critics and the public, it was released on DVD in January 9, 2007.

==Synopsis== Little Village, sexuality in a long confession with a priest in the church.
 Latin Kings Southside Chicagos Two Six gang led by Reaper (Donato Cruz) after a cruel initiation beating. Although the film uses names of actual gangs in the area, the introduction to the film says there is no direct relation to the actual gang names used.

When Reaper is later informed that Angel is an ex-gang member of the rival Latin Kings gang, he decides that Isaac should kill him. Issac desperately tries to arrange for him and Angel to escape Chicago but the inevitable happens and the gang captures Angel. When Isaac refuses to execute Angel, Reaper does the job. Isaac then kills Reaper and then commits suicide.

==Cast==
*Michael Cortez as Angel
*Tony Sancho as Isaac
*Donato Cruz as Reaper
*Beatriz Jamaica as Angels Mother
*Carmen Cenko as Isaacs Mother
;in alphabetical order
*Felipe Camacho as Priest (credited as Phil Camacho)
*Eric Ambriz as Jimmy
*Jimmy Borras Jr. as Niko
*Juan Castaneda as Hector
*Eddie Cruz as Esau
*Jason R. Davis as Store Customer
*Jeff Docherty as Dorvaks Partner
*Perry Flores as Officer Dorvak
*Russell Foster as Man in Yard
*Ricardo Garcia as Jesus
*Nicolas Gomez as Ozzy
*Brian Parenti as Pinto
*Adelina Quinones as Laura
*Octavio Rivas as Raul
*Jonathan Rodriquez as Adam
*Pricilla Santiago as Burger Joint Girl
*Roberto Soto as Store Owner
*Tatiana Suarez-Pico as Mother on Stairs
*Andrew Todaro as Mike

==Awards==
*Won "Best Narrative Feature Film" at the New York Newfest Film Festival

==External links==
* http://iconoclastfilms.com
* 
* 

 
 
 