The Mad Bomberg (1932 film)
{{Infobox film
| name =  The Mad Bomberg 
| image =
| caption =
| director = Georg Asagaroff
| producer = Óscar Dancigers   Dagobert Koßmann
| based on =  
| writer = Curt J. Braun
| starring = Hans Adalbert Schlettow   Liselotte Schaak   Paul Heidemann   Adele Sandrock
| music = P.J. Haslinde  
| cinematography = Carl Drews   
| editing =    
| studio = Deuton-Film
| distributor = 
| released =   
| runtime = 92 minutes
| country = Germany 
| language = German
| budget =
| gross =
}} The Mad film of the same title. The films art direction was by Otto Erdmann and Hans Sohnle.

==Cast==
*   Hans Adalbert Schlettow as Baron Giesbert v. Bomberg  
* Liselotte Schaak as Sophie, seine Frau  
* Paul Heidemann as Dachs, Diener 
* Adele Sandrock as Herzogin Looz v. Curswaren  
* Hansi Arnstaedt as Frau v. Gutelager  
* Lizzi Natzler as Fiffi, Zofe  
* Vivian Gibson as Tänzerin  
* Paul Henckels as Professor Landois 
* Charles Puffy as Jean Matin 
* Hans Wassmann as Gerichtsvollzieher  Georges Boulanger as Ein bekannter Geiger  
* Wolf Roon as Leutnant Werner  
* Hertha Guthmar as Sophies Freundin  
* Helen Schöner as Bombergs Verwandte  
* Emmy Wyda as Bombergs Verwandte  
* Hugo Werner-Kahle as Bombergs Verwandter  
* Ferdinand von Alten as Bombergs Verwandter  
* Arthur Bergen as Bombergs Verwandter  
* Antonie Jaeckel    
* Walter Steinbeck 
* Bernhard Goetzke 
* Josef Peterhans 
* Edgar Pauly 
* Max Vierlinger 
* Edmund Pouch
* Anton Krilla 
* Bernhard Perponché 
* O. Pfahl 
* Martha von Konssatzki

== References ==
 

== Bibliography ==
* Waldman, Harry. Nazi Films In America, 1933-1942. McFarland & Company, 2008.  

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 