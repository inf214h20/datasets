Nurse 3D
 
{{Infobox film
| name           = Nurse 3D
| image          = Nurse3D.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Doug Aarniokoski
| producer       = Marc Bienstock
| writer         = {{Plainlist|
* Doug Aarniokoski
* David Loughery}}
| starring       = {{Plainlist|
* Paz de la Huerta
* Katrina Bowden
* Corbin Bleu}}
| music          = Anton Sanko
| cinematography = Boris Mojsovski
| editing        = Andrew Coutts Lionsgate
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $10 million
}} 3D horror film directed by Doug Aarniokoski and written by David Loughery.    Starring Paz de la Huerta, Katrina Bowden, and Corbin Bleu, the film is inspired by the photography of Lions Gate Entertainment|Lionsgates chief marketing officer, Tim Palen.    Production took place from September to October 2011.
 selected theaters and on Video on demand|VOD.

==Plot==
The film opens with Abby Russell at a night club where she explains to the audience via a narration that her "job" is to murder cheating men in order to "keep scum off of the streets"- a job that she thoroughly enjoys. She then murders a man by slicing through his   that enjoys harassing new nurses. 

Abby is further irritated when Danni chooses to call her paramedic boyfriend Steve for support rather than approaching Abby herself. Shes pleased when Dannis call to Steve ends badly due to a fight the couple had had previously that night over Dannis refusal to move in with him. She explains her actions to Abby by saying that she is unwilling to trust her stepfather enough to leave him alone with her mother. This distrust is later justified when Abby and Danni witness her stepfather having an affair while the two women are on their way to go drinking at a nightclub. Danni is upset at the encounter, which makes her more willing to drink later that night. However unbeknownst to her Abby spikes her drink with Date rape drug|drugs, which enables Abby to get Danni to have sex with both her and random strangers. The next day Danni wakes to find herself in Abbys apartment and leaves despite Abbys entreaties for Danni to skip work and spend the day with her. After she leaves, Abby downloads several photos that she had taken from the previous night before leaving to see a psychiatrist, which is revealed to be Larry. 

Abby seduces Larry by saying that shes addicted to men, alluding to her past history with her father. He gives her his phone number to contact him anytime she wishes, which seems to confirm Abbys suspicions that Larry is a cheater. She later approaches him while hes leaving his office and convinces him to give her a ride, which enables her to murder him by paralyzing him in his car with vecuronium bromide and putting it in reverse, which causes him to get into a car accident. After hearing of her stepfathers death Danni seeks solace from Abby, only for Abby to grow angry when Danni says that shes going to move in with her boyfriend. Abby comments that she hopes that Larrys genitals were cut off in the car crash, only for Danni to reveal that she had never told Abby how her stepfather had died and quickly leave. This infuriates Abby, who decides that instead of seeking to help Danni, she will now hurt her. She accomplishes this by slowly convincing Detective John Rogan that Danni is mentally unstable and obsessed with Abby.

The next day, Abby runs into Rachel Owens, a new human resources employee that remarks that Abby greatly resembles a girl she knew that was sent to a mental institution. Abby invites Rachel out for drinks and takes the opportunity to harass Danni by calling her via Skype and showing Danni a video of Abby injecting chemicals into Rachel. Danni tries to go to the police, only for Detective Rogan to dismiss her claims as evidence of her trying to hurt Abby because the other woman didnt return her affections. He uses the photographs Abby took as proof to this effect, which Steve sees as a result of Danni calling him to the police station for support. This prompts an argument between the two and Steve leaves in a fit of anger. Danni tries to approach Dr. Morris for help, only for him to use this as an opportunity to blackmail Abby into having sex with him. Abby initially pretends to agree to this arrangement but soon shows that it was only a way for her to get him alone so she could dismember and murder him. That same night Abby also knocks out Rachel and drags her away to her death. 
 severely beating her mother for coming to his office unannounced. Danni also discovers that Sarah was taken in by one of the nurses at the institution, a woman named Abigail Russell. Danni then realizes that that Abby is Sarah and has since taken the name of her caretaker. Danni then tries to call Rachel to warn her about Abby, only to find that Rachels phone is in her car. She then receives a call from Abby, who implies that she will kill Steve in the same manner as Rachel. 

Danni rushes to the hospital, where she and Abby begin to fight. The staff initially tries to intervene, only for Abby to set off on a killing spree and lock herself into a lab. Danni and Steve finally manage to get inside, only for Abby (who had hidden in one of the patients beds) to stab Steve in the neck and run off. Abby rushes home to grab some essentials but is confronted by Detective Rogan, who tries to arrest her. Noticing that her neighbor Jared is viewing the situation, Abby pretends that Rogan is trying to rob her. Her neighbor then comes outside and hits the detective over the head with a bat, killing him instantly. Her neighbor is horrified to discover that Rogan is a cop, but Abby convinces him to hide the body, saying that Rogan was corrupt and that Jared would be treated badly since he is now a cop killer.

Abby, having fled, is shown to have assumed a new identity, that of human resources employee Rachel Owens.

==Cast==
 
 
* Paz de la Huerta as Abby Russell
** Katia Peel as young Abby
* Katrina Bowden as Danni Rodgers
* Kathleen Turner as Head Nurse Betty Watson
 
* Judd Nelson as Dr. Morris
* Corbin Bleu as Steve
* Boris Kodjoe as Detective John Rogan
* Melanie Scrofano as Rachel Owens
 
* Martin Donovan as Larry Cook
* Michael Eklund as Richie
* Niecy Nash as Regina
* Yulia Lukin as Larrys date
 
* Adam Herschman as Jared
* Lauro Chartrand as Security guard
* Stephan Dubeau as ER doctor
* Brittany Adams as Young nurse
 

==Production==
In 2011,  , was signed on to direct,  with Shawn Ashmore, Dominic Monaghan, and Ashley Bell confirmed. 

===Casting===
In July 2011 Paz de la Huerta signed on to play Abby Russell, the films primary antagonist.  Dita Von Teese was initially intended to join the cast for a cameo as a nightclub performer,  but she later withdrew from the project.    Corbin Bleu was brought on in August 2011.   

===Filming===
Principal photography began in Toronto on September 6, 2011 and wrapped on October 21. Following completion, the film was shelved for two years before its rights were acquired by Lionsgate.

==Reception==
Critical reception has been mixed; the film holds a 65% "fresh" rating on Rotten Tomatoes (based on 17 reviews)  while also holding a rating of 29 on Metacritic (based on 7 reviews).  Much of the films criticism centered upon the films script,  which Neil Genzlinger commented "doesn’t have any of the wit that a film like this needs to give it campy coolness."  Peter Sobczynski of rogerebert.com gave the film a positive review rating it two and a half stars writing "It is ridiculously lurid trash from start to finish and anyone trying to argue otherwise is as crazy as its central character. However, while its aim may be low throughout, it at least comes close to consistently hitting its targets."  The Village Voice panned the film overall, opining that it "never truly embrace " its "B-movie trashiness".  In contrast, more positive reviews for the film for these same elements and Shock Till You Drop remarked that while they could understand why people would not like the film, it would have a solid appeal for "Those very special people out there with very special tastes that embrace “the awful” and know how to have a little bit of fun."  Fearnet|Fearnets Scott Weinberg also echoed this sentiment, saying that the movie was "nothing resembling a deep, intellectual, or insightful horror flick" but that it was "however, quite a bit of good, gruesome fun if you enjoy "body count" horror combined with a basic but serviceable plot yanked straight out of Single White Female." 

==Sequel==
A sequel for Nurse has been rumored by star de la Huerta on her Twitter page with her stating that Nurse 2 starts shooting soon. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 