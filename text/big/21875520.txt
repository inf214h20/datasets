Shantham
{{Infobox Film
| name           = Shantham
| image          = 
| image_size     = 
| caption        = 
| director       = Jayaraj P V Gangadharan
| writer         = P. Suresh Kumar Madampu Kunjukuttan
| narrator       =  K P I M Vijayan  M G Shashi  Kalamandalam Gopi   Madambu Kunjukuttan Kaithapram Damodaran Namboodiri
| cinematography = Ravi Varman
| editing        = N P Sathish
| studio         = 
| distributor    = 
| released       = 2001
| runtime        = 95 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 I M K P National Film Awards, 2001.  The film was produced by P V Gangadharan. According to the citation of the Award Jury: Shantham addrsses the very contemporary issue of political rivalry and violence in our society in an unusually imaginative way. The language of the film goes beyond the conventional narrative for appeal to calmness and good sense.

The film also received the National Best Supporting Actress Award for K P A C Lalitha.

This is Jayarajs third National Award. In 1996, his film Desadanam, won the award for the Best Malayalam Film and in the next year  he was named the Best Director.

==Plot==
Persons on both sides of a bloody political struggle search for grace and forgiveness in this emotional drama.

Velayudhan (I M  Vijayan) is a young man living in a place torn apart by civil strife. In the midst of a raging street war, he kills his best friend. He was overcome by guilt and remorse for his actions as Narayani  (K P A C Lalitha), his friends mother, tries to hold herself together while she arranges for her sons funeral. Velayudhans mother Karthiyayani (Seema Biswas) goes to a temple and prays for her sons forgiveness despite his violence and wrongdoing. There  she meets Narayani who is stricken with her own feelings of grief over the death of her child.

==Awards==
*2000 - National Film Award for Best Feature Film
*2000 - National Film Award for Best Supporting Actress - K.P.A.C. Lalitha

==External links==
* 

==References==
 

 

 
 
 
 
 
 
 