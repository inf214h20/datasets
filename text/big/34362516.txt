The Boy from Stalingrad
{{Infobox film
| name           = The Boy from Stalingrad
| image          =
| caption        =
| director       = Sidney Salkow
| producer       = Colbert Clark
| writer         = 
| starring       = 
| music          =
| cinematography = L. William OConnell
| editing        = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 69 min.
| country        = United States
| language       = English 
| budget         =
}}

The Boy from Stalingrad is a 1943 American film.

The plot centers on Russian youths in the path of the German armys assault on Stalingrad, who are forced to band together and rely on themselves to survive. They set fire to the grain harvest, rescue and care for other abandoned children, sabotage a tank, and fight back against the Nazis as best they can.
 The North Star, and MGMs 1944 Song of Russia.  They were all pieces of wartime propaganda, officially approved and encouraged by the U.S. government, which wanted to keep the alliance with the Soviets strong. The films would prove painfully embarrassing to their producers just a few years later, when the U.S. went back to treating the Soviet Union as an adversary.

== Cast ==
 
* Bobby Samarzich as Kolya
* Conrad Binyon as Grisha
* Mary Lou Harrington as Nadya
* Scotty Beckett as Pavel
* Steven Muller as Tommy Hudson
* Donald Mayo as Yuri
* John Wengraf as German Major
* Erik Rolf as German Captain
* Wilhelm von Brincken as German General
 

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 

 