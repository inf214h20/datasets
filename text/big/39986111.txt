Faithfully Yours
 
 
{{Infobox film
| name           = Faithfully Yours
| image          = FaithfullyYours.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 最佳女婿
| simplified     = 最佳女婿
| pinyin         = Zuì Jiā Nǚ Xù
| jyutping       = Zeoi3 Gaai1 Neoi2 Sai3 }}
| director       = Wong Wa Kei
| producer       = Wong Wa Kei
| writer         = 
| screenplay     = Cheung Hoi Hing
| story          = 
| based on       =
| narrator       = 
| starring       = Jacky Cheung Max Mok Stephen Chow Sharla Cheung
| music          = David Wu
| cinematography = Lau Hung Chuen
| editing        = Poon Hung
| studio         = Long Shong Pictures Golden Princess Film Production Kays Productions
| distributor    = Golden Princess Films
| released       =  
| runtime        = 90 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$5,807,710
}}
 1988 Cinema Hong Kong romantic comedy film directed by Wong Wa Kei and starring Jacky Cheung, Max Mok, Stephen Chow and Sharla Cheung.

==Plot==
Happy, Big Eye and Puddin Lai are good friends, Happy is a hairstylist who opens his "Great Grass Hair Salon" next to the "Great Shanghai Hair Salon", which dissatisfies Greater Shanghais owner Chuk Tai Chung. While the two are at loggerheads on the occasion, Chungs daughter Ying goes to Great Grass Hair Salon and the three friends do their best to pursue her. One time during a drunk accident, Ying becomes pregnant but does not know who the father is and can only wait the birth of her child to confirm the identity. Happy, Big Eye and Puddin begin to fawn Ying and her family in every possible way, resulting in a series of big jokes.

==Cast==
*Jacky Cheung as Happy Chan Hoi Sam
*Max Mok as Big Eye / Kei Ho Yan
*Stephen Chow as Puddin Lai
*Sharla Cheung as Ying
*Richard Ng as Chuk Tai Chung / Shanghai Man, Yings father
*Lydia Shum as Shanghai Lady, Yings mother
*Teddy Yip as Yings godfather
*Sing Yan as barbershop assistant
*Mak Hiu Wai as rich taxi driver
*Bowie Wu as Birdy
*Law Ching Ho as Elephant 
*Liu Kai-chi as medical laboratory worker
*Cheung Yuen Wah as girl in maternity ward
*Alan Chan as doctor
*Joyce Cheng as infant
*Yuen Ling To

==Box office==
The film grossed HK$5,807,710 at the Hong Kong box office during its theatrical run from 8 December 1988 to 5 January 1989 in Hong Kong.

==See also==
*Jacky Cheung filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 
 
 
 
 
 
 
 
 
 
 