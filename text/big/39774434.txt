No Limit Kids: Much Ado About Middle School
{{Infobox film
| name           = No Limit Kids: Much Ado About Middle School
| image          = No limit kids DVD cover.jpg
| alt            = 
| caption        = 
| director       = Dave Moody
| producer       = Dave Moody
| writer         = Josh Moody
| starring       = Bill Cobbs Lee Meriwether Blake Michael Celeste Kellogg  Ashton Harrell
| music          = Dave Moody
| cinematography = Damon Woods
| editing        = Josh Moody
| studio         = Elevating Entertainment Motion Pictures Phase 4
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}} family Comedy comedy film. It starred Bill Cobbs, Lee Meriwether, Blake Michael, Celeste Kellogg and Ashton Harrell.  The screenplay was written by Joshua Moody and the film was directed by Dave Moody for Elevating Entertainment Motion Pictures, who also provided the musical score for the film.

==Plot== middle school full of questions, doubts and fears, come together to form a club in an abandoned theater on Main Street. Inside the theater, they discover an interesting piece of their history, but also a seemingly homeless man named Charlie (Bill Cobbs), who connects with the kids through their mutual passion for musical theater.

When the teens learn the theater is scheduled for demolition, they embark on a mission to save the towns landmark and to keep Charlie safe. They decide to mount a modern day version of a Shakespeare play called Much Ado About Middle School based on Shakespeares Much Ado About Nothing.  Through the plays mistaken identities and false assumptions, the teens and others learn that you can not always judge a book by its cover.

==Cast==
* Bill Cobbs as Charlie 
* Blake Michael as Zach
* Celeste Kellogg as Celeste 
* Ashton Harrell as Ashton
* Amanda Waters as Becca 
* Lee Meriwether as Katie 
* Jeff Rose as Frank 
* Janet Ivey as Nicole

==Reception== Movieguide site, writes:

 No Limit Kids is an entertaining movie about middle school kids who like to sing trying to raise money to save a rundown theater from being torn down.   is well worth watching. Bad behavior is rebuked, good behavior is commended, and there’s a good story arc where good triumphs over evil.    

Baehr praised the singing as "wonderful," but did go on to note the films often "flat dialogue" and "silly scenes", commenting particularly on the mixed success of the films technique of having its characters talk directly to the camera. 

==Awards and Festivals==

No Limit Kids: Much Ado About Middle School was recognized at several youth and family-oriented film festivals:
;Winner
*2010 Winner Phoenix International Film Festival (Phoenix, Arizona) - No Limit Kids: Much Ado About Middle School "Best Film for Young Viewers"   
*2010 Winner Redemptive Film Festival (Regent University, Virginia) - No Limit Kids: Much Ado About Middle School "Redemptive Storytellers Award"
*2010 Winner Independents Film Festival (Tampa, Florida) - No Limit Kids: Much Ado About Middle School "Best Independent Feature Film"
*2011 Winner Silver Telly Award - No Limit Kids: Much Ado About Middle School  "Film/Video Category Winner"  

;Nominations/Official Selections
*2010 Official Selection Gideon Film Festival (Asheville, North Carolina) - "No Limit Kids: Much Ado About Middle School"  
*2010 Official Selection KIDS FIRST! Film Festival (San Francisco, California) - No Limit Kids: Much Ado About Middle School  http://www.kidsfirst.org/filmfestival/2010/2010BestFestNominees.htm 
*2010 Official Selection TriMedia Film Festival (Ft Collins, Colorado) - No Limit Kids: Much Ado About Middle School  
*2010 Official Selection International Black Film Festival of Nashville (Nashville, Tennessee) - No Limit Kids: Much Ado About Middle School  
*2010 Official Selection Eugene International Film Festival (Eugene, Oregon) - No Limit Kids: Much Ado About Middle School 
*2011 Nominee KIDS FIRST! Film Festival (San Francisco, California) - "Best Indie Feature Ages 8-12" No Limit Kids: Much Ado About Middle School  

==References==
 

==External links==
*  
*  

 

 
 
 
 