Just like Home (2007 film)
{{Infobox film
| name           = Just Like Home
| image          = Justlikehome.jpg
| caption        = Theatrical release poster
| director       = Lone Scherfig
| writer         = 
| starring       = Lars Kaalund Bodil Jørgensen Ann Eleonora Jørgensen
| music          = Kasper Winding
| cinematography = Anthony Dod Mantle
| editing        = 
| studio         = Zentropa
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}
Just like Home is a 2007 film directed by Lone Scherfig. It stars Lars Kaalund and Bodil Jørgensen. 

==Cast==
*Lars Kaalund as Apoteker
*Bodil Jørgensen as Myrtle
*Ann Eleonora Jørgensen as Margrethe
*Peter Gantzler as Erling
*Peter Hesse Overgaard as Lindy Steen

==References==
 

==External links==
*  

 

 
 
 

 