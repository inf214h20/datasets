The Tomb (2007 film)
{{Infobox Film   
| name           = HP Lovecrafts The Tomb (a.k.a. The Tomb)
| director       = Ulli Lommel
| producer       = Ulli Lommel Nola Roeper Jeff Frentzen
| writer         = Ulli Lommel
| starring       = Victoria Ullmann Christian Behm Gerard Griesbaum Michael Barbour
| cinematography = Bianco Pacelli
| editing        = Christian Behm (as XGIN)
| studio         = The Shadow Factory Boogeyman Movies International
| distributor    = Lions Gate Entertainment
| runtime        = 81 min. English
}}

HP Lovecrafts The Tomb is a 2007 United States production horror film that is supposedly based on H.P. Lovecrafts 1917 story, The Tomb (short story)|"The Tomb". 
However, many reviewers have noted that the plot of this film is completely unrelated to the Lovecraft short story.         

The film in fact has no single element whatsoever in common with the short story, save for the title. The film is often compared to the 2004 movie, Saw (2004 film)|Saw, going as far as having that series mentioned on the box art.

The film is also known simply as The Tomb, but the title on the DVD case is HP Lovecrafts The Tomb. However, on the film itself the title is "H.P. Lovecraft   The Tomb" with no apostrophe + s.  The movie was directed by Ulli Lommel.

==Plot== Charles Dexter Ward and one of his victims as Pickman (a reference to Lovecrafts story Pickmans Model). However these passing references to Lovecraftian characters (and a quote from one of Lovecrafts stories about going "beyond ye spheres") are largely irrelevant to the serial killer plot played out on screen.

==Filming==
Production of HP Lovecrafts The Tomb took place during August 2005 in Marina Del Rey, California, at a warehouse on Princeton Drive that has since been demolished. The scenes at the "Palm Desert Motel" were shot on an indoor set at the same warehouse. Exteriors were shot in the high desert near Palmdale, California.

Co-executive producer Jeff Frentzen is wearing the black gloves of the killer throughout the film.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 