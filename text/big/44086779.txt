Kaanatha Valayam
{{Infobox film 
| name           = Kaantha Valayam
| image          =
| caption        =
| director       = IV Sasi
| producer       =
| writer         = Thalassery Raghavan
| screenplay     = T. Damodaran
| starring       = Jayan Krishnachandran Mohan Sharma Bahadoor Shyam
| cinematography = K Ramachandrababu
| editing        = K Narayanan
| studio         = TV Combines
| distributor    = TV Combines
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by IV Sasi. The film stars Jayan, Krishnachandran, Mohan Sharma and Bahadoor in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Jayan
*Krishnachandran
*Mohan Sharma
*Bahadoor Meena
*Ravikumar Ravikumar
*Seema Seema
*Ushakumari

==Soundtrack== Shyam and lyrics was written by Ettumanoor Somadasan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Nimisham || K. J. Yesudas, Vani Jairam || Ettumanoor Somadasan || 
|-
| 2 || Oru sugandham Maathram || K. J. Yesudas || Ettumanoor Somadasan || 
|-
| 3 || Palliyankanathil || S Janaki || Ettumanoor Somadasan || 
|-
| 4 || Shilpi Poyal Shilayude Dukham || K. J. Yesudas || Ettumanoor Somadasan || 
|}

==References==
 

==External links==
*  

 
 
 

 