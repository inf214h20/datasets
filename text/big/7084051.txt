One Soldier
{{Infobox film
| name           = One Soldier
| image          =
| caption        = Steven Wright in One Soldier
| director       = Steven Wright
| producer       =
| writer         =
| narrator       =
| starring       = Sandi Carroll Steven Wright
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = March 1999
| runtime        = 30 mins
| country        = United States
| language       =
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
One Soldier is a black-and-white short film (30 min) from 1999 starring and directed by Steven Wright. Also starring is Sandi Carroll.
The film tells the story of a confused soldier (played by Wright) who narrates throughout the movie his bizarre thoughts about the army and war in general, using Wrights trademark deadpan humor. Some of One Soldier was filmed on Block Island, an island off the coast of Rhode Island. Horses from a local stable on the island, Rustic Rides Farm, were used for the scene of Wright with horses on the beach.

==Plot==
The film is told entirely in retrospect, from a veteran of the American Civil War who may already have been executed. During the war, his job was to play music for a general who decided, in the soldiers words, "where hundreds of men would die."  He has come home from the war skeptical about the meaning of life (or that there is a meaning), and trying to search for answers. He attempts to express his thoughts and doubts to his wife Becky, but she remains unconvinced that life is horrible and thinks he is crazy and going to hell. Convinced that people have come to have too much influence over art, he tries to play music not written by people by drawing music lines on his glasses and playing the stars as if they were notes. He also tries to get in touch with God, but, not wanting to be intrusive, he merely hangs about outside the church and whispers through the windows, "God...hey, God...whatre you doin?"

Finally, after many hints, it is revealed why he is sentenced to death: he fought in the war because a rich man paid him to fight in his (the rich mans) place. When the rich man showed up to see how he was fighting, he found the soldier standing and playing the concertina during a battle. The rich man gave him a gun and started yelling at him, so in frustration the soldier shot the rich man instead of the Confederate States of America|Confederates, picked up the concertina, and left him lying in the field. "He killed the wrong man in the war."

As he is about to be executed, the soldier has an epiphany: "My God...I wasted my whole life thinking about this stuff. I should have just gone fishing!  I should have had a sandwich, or had a few laughs!  Now I get it!" His illumination is cut off by the firing squad.

He (in spirit form?) walks through a graveyard, and muses, "Im gonna miss being alive."  The credits follow.

==Quotes==
Becky: Didnt your grandfather commit suicide?
 Soldier: Yeah, I mean, Im not saying thats something you should do...but, what he used to say was, just because you leave the table in the middle of dinner doesnt necessarily mean you didnt like the food.
 Becky: Is that supposed to mean something?  All that means is that insanity is hereditary.

Becky (finding the soldier sitting in front of a fireplace with no fire): Why isnt there a fire?
 Soldier: There is a fire, its just not here. There is no fire because there is no wood. I mean, theres no wood in here.
 Becky: Didnt you just chop a big tree down yesterday?
 Soldier: Yes. I chopped it down. And I dragged it back to the house, chopped it into little pieces, and stacked it behind the house. But I cant burn it. I cant do it. The tree was standing there so tall...I mean, who am I to cut it down?
 Becky: Honey, youre thinking too much again. Just relax and...and dont think so much.
 Soldier: ...the tree!

The soldier is lying on the floor, staring off into space.
 Becky: What are you doing?
 Soldier: Im waitin for the moon to pass by that window.
 Becky: I have something important to tell you. Im going to have a baby.
 Soldier (monotone): Thats wonderful. Thats fantastic. A little child from the world of children. Oh God, oh Becky, I dont care if its a boy or a girl as long as its a boy. Just think, little tiny fingers and little eyeballs. Oh my God, lets boil the water now!  Lets boil all the furniture!  Im gonna go outside and boil the horses!  Maybe we shouldnt name him, that way he can never be drafted. Oh, it almost makes we want to get up off the floor. How do you feel Becky?
 Becky: Fine.
 Soldier: Little baby gets to be inside of you all the time ... Im jealous!  What if the little baby doesnt want to live in this house?  What if he doesnt want to live here with us?  And then, when that little baby is about 80 years old, well just be dust!  Oh, Becky, I love you!
 With that, he crosses his legs and stares out the window again.

==External links==
*  

 
 
 
 
 
 