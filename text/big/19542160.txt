Charles' Aunt
 
{{Infobox film
| name           = Charles Aunt
| image          = 
| caption        = 
| director       = Poul Bang John Olsen
| writer         = Arvid Müller Brandon Thomas
| starring       = Dirch Passer
| music          = Sven Gyldmark
| cinematography = Ole Lytken
| editing        = Edith Nisted Nielsen
| distributor    = 
| studio         = Saga Studio
| released       =  
| runtime        = 101 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}
Charles Aunt ( ) is a 1959 Danish comedy film directed by Poul Bang and starring Dirch Passer.

==Cast==
* Dirch Passer as Grev Ditlev Lensby
* Ove Sprogøe as Charles Smith
* Ebbe Langberg as Peter Ahlevig
* Ghita Nørby as Laura Hornemann
* Annie Birgit Garde as Lone Hornemann
* Holger Juul Hansen as Ritmester Frederik Ahlevig
* Birgitte Federspiel as Donna Lucia dAlvadorez / Lise Holm
* Susse Wold as Henriette
* Hans W. Petersen as Etatsråd Ludvig Lohmann
* Keld Markuslund as Butler Olufsen
* Vivi Svendsen as Kokkepigen Kristine
* Børge Møller Grimstrup as Kusken
* Emil Halberg
* Peter Marcell
* Alfred Arnback

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 