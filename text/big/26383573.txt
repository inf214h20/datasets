The Greatest (2009 film)
 
{{Infobox film
| name           = The Greatest
| image          = The Greatest poster.jpg
| caption        = Theatrical poster
| director       = Shana Feste
| producer       = Lynette Howell Beau St. Clair
| writer         = Shana Feste
| starring       = Pierce Brosnan Susan Sarandon Carey Mulligan 
| music          = Christophe Beck
| cinematography = John Bailey
| editing        = Cara Silverman
| studio         = Barbarian Film Group Oceana Media Finance Silverwood Films Irish DreamTime
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $6 million
| gross          = $335,976
}} directorial debut, and starring Pierce Brosnan (also an executive producer), Susan Sarandon, Carey Mulligan, and Michael Shannon.

==Plot== Aaron Johnson) dies, his family feel like they cannot go on. His mother Grace (Susan Sarandon|Sarandon) and his father Allen (Pierce Brosnan|Brosnan) get an unexpected visitor knocking at their door; it turns out to be a young woman called Rose (Carey Mulligan|Mulligan), who is pregnant with Bennetts child. As the story develops Bennetts younger brother Ryan is introduced and he is grieving the fact he did not say a final good bye to his brother. One sees the true relationships of the family as the story comes together, Grace waits at the bed of Jordan Walker, the man driving the truck which collided with Bennetts car in the crash. She is waiting for his wake so she can ask him what her sons final 17 minutes were. After a while, she finds out the truth but is disappointed in what she hears. In the last moments, Bennett was not calling his mothers name, he was calling to Rose asking the other wounded passenger Walker to make sure that she was safe.

Allen hires a cleaner to clean the house, but when Grace finds out she is terribly angry, claiming their son has been washed away. Allen then appears to be having a heart attack whilst Grace carries on shouting until she realises what the matter is. At the hospital bed, shortly after Grace returns from finding out the truth about Bennett, Allen breaks down crying claiming to be holding in all his grief and upset, he claims he could have done something to prevent the crash and later his sons death. 

Meanwhile, Rose has overheard Grace saying she should have died and not Bennett and so she leaves and seeks the help of her mother. She soon realises that all her mother is bothered about was trying to get money from the Brewers, claiming it will help her. The Brewers try to find Rose and they find her in labour and persuade her to go to the hospital and, while in the car on the way, she demands to know everything about Bennett she did not already know.

When the film draws to a close, one sees Rose and the baby, a girl called Ruby, Graces favourite girls name. The film ends with the moment Bennett spoke to Rose earlier on the day he died.

==Production== directorial debut. To gain the confidence of investors and producers, she made a scrapbook which contained her ideas on tone, camera movement, color, space and lines. 
 35 mm format. 

==Reception==
The film screened at the 2009 Sundance Film Festival.   Another film starring Mulligan, the Academy Award|Oscar-nominated An Education, also screened at the festival. The film ranking website Rotten Tomatoes reported that 52% of critics had given the film positive reviews, based upon a sample of 56. 

==References==
 

==External links==
* 
* 


 

 
 
 
 
 
 
 
 
 