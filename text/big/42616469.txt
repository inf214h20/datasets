Samayada Gombe
{{Infobox film
| name           = Samayada Gombe
| image          = 
| image size     = 
| caption        = 
| director       = Dorai-Bhagawan
| producer       = Dorai-Bhagawan   Rathi Radhakrishna
| story          = Chitralekha
| screenplay     = 
| narrator       =  Rajkumar Roopadevi Kanchana Srinath
| music          = M. Ranga Rao
| cinematography = B. C. Gowrishankar
| editing        = P. Bhaktavatsalam
| studio         = Sri Thripura Sundari Combines
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Kannada
}}  Kannada film Rajkumar in lead roles along with Srinath, Roopadevi and Menaka in other lead roles.  It is based on the novel written by Chitralekha.

The film was received positively upon release and the music composed by M. Ranga Rao met with highly critical appreciation.

==Cast== Rajkumar 
* Roopadevi
* Menaka Kanchana
* Srinath
* Thoogudeepa Srinivas
* Sudarshan
* Musuri Krishnamurthy
* Shivaram
* Mysore Lokesh
* Shakti Prasad
* Uma Shivakumar

==Soundtrack==
The music of the film was composed by M. Ranga Rao with lyrics penned by Chi. Udaya Shankar. All the songs were received extremely well, particularly "Kogile Hadide" and "Chinnada Bombeyalla" songs were chart-busters. 
{{Infobox album	
| Name = Samayada Gombe
| Longtype = 
| Type = Soundtrack	
| Artist = M. Ranga Rao	
| Cover = 
| Border = Yes	
| Alt = Yes	
| Caption = 
| Released = 1984
| Recorded = 
| Length =  Kannada 
| Label = 
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Kogile Haadide"
| Rajkumar (actor)|Rajkumar, S. Janaki
|-
| 2
| "Sankochava Bidu"
| Rajkumar, S. Janaki
|-
| 3
| "Nanna Saradara"
| Rajkumar, S. Janaki
|-
| 4
| "Aakasha Kelageke"
| Rajkumar
|-
| 5
| "Chinnada Bombeyalla"
| Rajkumar
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 