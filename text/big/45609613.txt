The Last Proletarians of Football
{{Infobox film
| name                 = The Last Proletarians of Football
| image                = The Last Proletarians of Football poster.png
| alt                  = 
| caption              = Swedish poster for the film
| film name            = Fotbollens sista proletärer
| directors            =  
| producer             = Kalle Gustafsson Jerneholm
| writer               = Carl Pontus Hjorthén
| screenplay           = 
| story                =
| starring             =  
| music                =  
| cinematography       = 
| editing              = 
| production companies =  
| distributor          =  
| released             =  
| runtime              = 74 minutes
| country              = Sweden
| language             = Swedish
| budget               = 
| gross                =  
}} Swedish documentary football club IFK Göteborg and its success during the 1980s, but also about the development of Swedish society.

The documentary is written and directed by Martin Jönsson and Carl Pontus Hjorthén, with music by Ian Person, guitarist of former Swedish rock band The Soundtrack of Our Lives.  Producer Kalle Gustafsson Jerneholm has also been a member of the band.

The title of the film alludes to how the players of IFK Göteborg during the period were part of the class of Wage labour|wage-earners, playing the game as amateurs in a world of professional footballers, just like the proletariat are the class of wage-earners in a capitalist society.

== Synopsis == Swedish model) challenged the free market economies of Europe, IFK Göteborg challenged the fully professional European top clubs.
 second division from 1971 to 1976, get back to Allsvenskan in 1977 and transform into a European football powerhouse during the 1980s. Some of the players—such as Torbjörn Nilsson, Ruben Svensson, Dan Corneliusson and Glenn Hysén, alongside with manager Sven-Göran Eriksson—also appear and get to tell their view of that period.

The highlight of the film shows the success in the 1981–82 UEFA Cup, which culminated in the 1982 UEFA Cup Final against Hamburger SV. The contrast between the Swedish amateur side and their well-paid opponents is repeated. HSV had just won the Bundesliga, and were so confident that even after losing 1–0 in the first match in Gothenburg, pennants proclaiming "Hamburger SV – UEFA Cup Winners 1982" ( ) had been produced, distributed and sold in Hamburg before the second game. The Last Proletarians of Football then show how IFK Göteborg went on to beat the German team 3–0 away, the first ever European trophy won by a Swedish club (and currently only one of two as IFK repeated their success in the 1986–87 UEFA Cup).

Jönsson and Hjorthén show how Swedish society changes in the mid 1980s, diverging from the Swedish model. Ideals change, egoism is on the rise and solidarity is no longer inherent to Swedish society. They contend that the assassination of Olof Palme in February 1986 is the turning point.
 penalty shoot-out.

Scenes of IFK Göteborg getting eliminated from European football then cut to the scenes of Olof Palmes funeral as the documentary ends.

== Release ==
An extended 80-minute cut of the film was sneak-previewed at the Draken cinema in Gothenburg on 4 January 2011,  and on 1 April the general release 74-minute version started screening.  The Last Proletarians of Football was released on DVD on 27 May 2011,  early DVDs shipped with a replica of the Hamburger SV pennant claiming they won the 1982 UEFA Cup.   The documentary has been televised by Sveriges Television twice, a 59-minute cut later the release year on 4 September, and the full length film on 20 June 2012. 

The Last Proletarians of Football has also been screened abroad, in Barcelona (at the OffsideFest football film festival ), London, Berlin, Palermo (at the SportFilm Festival ), Liverpool (at the Kicking + Screening football film festival ), New York, Rio de Janeiro, São Paulo and Bilbao. 

== Reception and awards ==
The Last Proletarians of Football was praised in Sweden  as well as abroad,  and received favorable reviews in all the major Swedish newspapers.      The main criticism the film received was that it tried to force the connection between politics and football too hard,    but the connection has also been pointed out as one of the strengths of the film. 

The film won the Paladino doro, the award for the best football film at the SportFilm Festival in Palermo and was nominated to two futher categories, best film editing and best cinematography. 

== Citations ==
 

== References ==
 
* 
* 
* 
* }}
* }}
* 
* 
* 
* 
* 
* }}
* }}
* }}
* }}
*{{cite web |last=Ödmark |first=Sara |title=Bard, Bergqvist och fotbollsproletärer|date=2011-09-05 |url=
http://blogg.dn.se/tvbloggen/2011/09/05/bard-bergqvist-och-fotbollsproletarer/ |accessdate=2015-03-07 |publisher=Dagens Nyheter |ref=harv}}
* }}
* 
* 
* 
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 