Tatsumi (film)
{{Infobox film
| name = Tatsumi
| image =Tatsumi movie poster.jpg
| alt =
| caption = Film Poster
| director = Eric Khoo
| producer = Tan Fong Cheng Phil Mitchell Freddie Yeo Eric Khoo 
| screenplay = Eric Khoo
| based on =  
| starring = Tetsuya Bessho Yoshihiro Tatsumi
| music = Christopher Khoo Christine Sham
| cinematography = 
| editing = 
| studio = Zhao Wei Films
| distributor = 
| released =  
| runtime = 98 minutes
| country = Singapore
| language = Japanese
| budget = $ 800,000
| gross =
}}
Tatsumi is a 2011 Singaporean animated drama film directed by Eric Khoo. It is based on the manga memoir A Drifting Life and five earlier short stories by the Japanese manga artist Yoshihiro Tatsumi. The film is a Singaporean production with Japanese dialogue, and was animated in Indonesia. 
 Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Plot==
 
The film follows the career of Yoshihiro Tatsumi, as he begins to work as a comics artist in post-war occupied Japan, meets his idol Osamu Tezuka, and invents the gekiga genre of Japanese comics for adults. Interweaved with the biographical material are segments based on Tatsumis short stories "Hell", "Beloved Monkey", "Just a Man", "Good-Bye" and "Occupied". 

==Voice cast==
* Tetsuya Bessho
* Yoshihiro Tatsumi

==Production==
===Development===
Singaporean director Eric Khoo, who previously exclusively made live-action films but has a past as a comics artist, was first introduced to the works of Yoshihiro Tatsumi during his military service, and says that he immediately was stricken by the sadness and beauty of the stories. When Tatsumis 840-page autobiographical manga, A Drifting Life, was published in Singapore in 2009, Khoo realised that Tatsumi still was alive and wanted to pay tribute to him. Khoo visited Tatsumi in Japan in October 2009 and received the permission to adapt the work to film.    Production was led by Khoos Singaporean company Zhao Wei Films.  The budget of the film corresponded to 800,000 US dollars.   

The visual style and storyboards of the film were kept tightly to Tatsumis original drawings. Khoo said: "You see, Tatsumi loves cinema, and when he created this new movement of comics using strips with real characters, rather than the four-panel manga convention, he produced works that are like storyboards for a film. All we need to do is stretch them out to a widescreen format. And give them multi-planes, like layers, so there is more depth and feel. I also tweaked certain things, changed some of the sequences of the stories, so for the cinema his stories got a new voice." 

===Casting and Staff===
Tatsumi himself was involved in the project and oversaw details to secure authenticity, as well as provided the narration for the biographical segments. Other prominent voices were performed by the Japanese stage actor Tetsuya Bessho.     Minor characters were played by amateurs from Singapores Japanese minority. People with roots in Osaka were sought out specifically, and Tatsumi instructed the actors how to speak the Osaka dialect of the 1950s. 

===Principal Production=== syndicated television shows. 

===Music===
The theme song of this film was composed by director Eric Khoos then 13 year old son Christopher.  Christopher composed a total of 3 musical compositions for the film. 

==Critical Reception==
In a review written at the 2011 Cannes Film Festival, the Hollywood Reporter said reviewed that the film "evinces a mood that is unutterably sad, yet indescribably beautiful.", but added that "the sinister and decidedly adult subject matter may scale down its widespread marketability".  The reviewer also praised the three music compositions, which she felt "lend the biographical segments a sweetly rueful timbre".  The reviewer also said that "the uncompromising, shattering power of the stories, (made) the account of Tatsumi’s life seem less engaging."   

==See also==
* Cinema of Singapore
* History of manga
* List of films based on manga
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Singaporean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 