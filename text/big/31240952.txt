Veera (2011 film)
 
 

{{Infobox film
| name           = Veera
| image          = Veera 2011 poster hd.jpg
| caption        = 
| director       = Ramesh Varma
| producer       = Ganesh Indukuri
| writer         = Paruchuri Brothers
| screenplay     = 
| story          = 
| based on       =  
| starring       =   Thaman
| cinematography = Chota K. Naidu
| editing        = Marthand K. Venkatesh
| studio         = Sanvi Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         =  
| gross          =  
}} Thaman and was released on 20 May 2011. Though began with received mixed reviews, Veera proved its stamina strongly in box office collections and was a decent hit in 2011. The film had successfully completed 50 days in 60 centres  and a 100-day run at the box office according to Zee 24 Gantalu.    The film was released in Tamil as Veeraiyaah. The film was released as The Great Veera in Hindi by Mishrakram Films.

==Plot==
Shyam Prasad (Shaam) is an honest ACP who comes into conflict with the local don, Dhanraj (Rahul Dev). The goons kill Shyams son,Moksha,and Shyam does not reveal this to his family but fears the villains may kill his daughter also. Dhanraj threatens Shyams family so the police department arranges for an officer to provide security for them. Deva (Ravi Teja) arrives and saves Shyam from Dhanrajs men and introduces himself as the security officer. Shyams family still believes that their son is in the boarding school and waits for his call a Sunday, but instead,they are told their son went on an excursion. Shyam gets tensed and reveals everything to Deva. When he goes to drop Anjali (ACPs daughter) to school with Tiger (Brahmanandam) with whom he shares his room,on their way he met Aiki (Taapsee Pannu),who falls for him.

Shyaams wife Sathya (  Pradeep Rawat) who wants to vacate the whole village to build a factory. But, Veera opposes him. Pedda tries to torture on the villagers, and Veera beats him  badly. Veeras younger brother marries the daughter of the officer who is the one who later sends Veera to Shyams family. In that shame, the wife of Pedda suicides. Pedda becomes angry and kills the whole family of Veera, including his wife Chitti, when Veera is not present.Knowing this, Veera kills all of Peddas henchmen. Pedda was supposed to die in an explosion, but he survives. Veera doesnt know that Pedda survived. Sathya blames Veera for the death of their family. Thats why Sathya disliked Veera. Later, Veera comes to the city to save his step-sisters family. The flashback episode ends.
Then the climax starts. Pedda challenges Veera and after a long fight between the two, Veera kills Pedda and takes his revenge. Then Aiki proposes to Veera and he agrees. And, he unites with his sister happily.

==Cast==
  was selected as lead heroine marking her first collaboration with Ravi Teja.]]
* Ravi Teja as Deva/Veera
* Shaam as Shyam Prasad
* Kajal Aggarwal as Chitti (Veeras Wife)
* Taapsee Pannu as Aiki
* Sridevi Vijayakumar as Sathya (Veeras Stepsister) Pradeep Rawat as Pedda Rayudu
* Divya Vani as Pedda Rayudus wife Roja as Veeras Stepmother
* Brahmanandam as Tiger
* Rahul Dev as Dhanraj Ali
* Ajay as the real Deva Venu Madhav
* Krishnudu
* Srinivasa Reddy Pragathi 
* Chalapathi Rao Hema

==Production==
In January 2010, the first look of Veera was released by Sanvi Productions, the producers of the film, with Ravi Teja in the lead role and Ramesh Varma as the director.  Furthermore a leading technical crew was announced with Devi Sri Prasad as music composer, Chota K. Naidu as cinematographer, Marthand K. Venkatesh as editor and the Parachuri brothers as writers.  The casting process began in June 2010 with Anushka Shetty was signed on to play the lead female role with Taapsee Pannu selected to play a supporting role in the film.  Anushka walked out of the project before filming began and was subsequently replaced by Kajal Aggarwal, whilst Devi Sri Prasad was also replaced by Thaman.  Shaam and Sridevi Vijayakumar were also signed on to play a supporting roles in the film.

After further delays, the film began its first schedule in November 2010 at Madhapur Art Gallery in Hyderabad. 
Kajal is pleasant, but is given near-obscene lines and a vulgar romantic track for mass.kajal fully entertained mass with her  romantic sexy dialogues Kajal is playing a full length mass heoine role as Kabaddi Chitti.kajal fully entertained mass men at her best 

==Soundtrack==
{{Infobox album
| Name       = Veera
| Longtype   = 
| Type       = Soundtrack
| Artist     = S. Thaman
| Cover      =
| Border     = yes
| Alt        =
| Caption    = 
| Released   =  
| Recorded   = 2011 Feature film soundtrack
| Length     = 31:01 Telugu
| Label      = Aditya Music
| Producer   = S. Thaman
| Reviews    =
| Last album = Sabash Sariyaana Potti  (2011)
| This album = Veera  (2011)
| Next album = Kanchana (2011 film)|Kanchana  (2011)
}}

The audio release of the film was done in a very simple manner. Music director S. Thaman, director Gopichand Malineni, Aditya Music CEO Dayanand,B.A. Raju were present at the function. S. Thaman released the CD and handed it over to Gopichand Malineni.

S. Thaman said that since he entire unit is present at Switzerland as of now busy with the shooting he was organising this audio release and songs in the movie are according to the body language of Ravi Teja and also informed that NTR old classic Mavilla thota kada was remixed in the movie according to the requirement. 

The movie has celebrated its triple platinum disc function in Taj Deccan. Ravi Teja, Brahmanandam, Sridevi Vijaykumar, Ali (actor)|Ali, Chota K Naidu, Ram-Lakshman, Dayanand, S. Thaman and others were present at the function. 

{{tracklist
| headline       = Tracklist
| extra_column   = Singer(s)
| total_length   = 31:01
| lyrics_credits = yes
| title1  = Ekkadekkada
| lyrics1 = Ramajogayya Sastry
| extra1  = N.S. Ramya
| length1 = 4:29
| title2  = O Meri Bhavri
| lyrics2 = Rahman
| extra2  = S. Thaman & Bindu Mahima
| length2 = 4:42
| title3  = Chitti Chitti
| lyrics3 = Bhaskarabhatla Karthik
| length3 = 4:30
| title4  = Chinnari (Montage Bit)
| lyrics4 = Sira Sri
| extra4  = Karthik
| length4 = 2:36
| title5  = Hossanam
| lyrics5 = Bhaskarabhatla
| extra5  = Ranjith (singer)|Ranjith, Roshini, Karthik
| length5 = 5:28
| title6  = Mavilla (Remix)
| lyrics6 = Bandaru Danayya Kavi
| extra6  = Muralidhar, Ganga
| length6 = 4:55
| title7  = Veera Veera
| lyrics7 = Abhinaya Srinivas
| extra7  = M.L.R. Karthikeyan, Ranina Reddy
| length7 = 4:18
}}

==Reviews==
The film received mixed reviews.  Supergoodmovies wrote:"Veera is for die hard mass fans who like to see severed limbs and arms flying all over the screen. Audience with sensibilities won’t like it a bit".  Indiaglitz wrote:"An absolutely predictable and irritatingly boring film".  123telugu wrote:"Watch it, if you are a big fan of Ravi Teja".  Rediff wrote:"tedious watch".  Fullhyd wrote:"Veera, well-rounded in its lousiness, does neither. And yet, it audaciously traipses on, firm in its faith that this is the blockbuster youve been waiting for all summer".  sify wrote:"Veera is only fine in parts wherever Ravi Teja is shown in a mass dimension. There is unnecessary violence and the screenplay is hackneyed. Director Ramesh Varma was purely focusing to showcase Ravi Teja on the lines of a superhero but he failed miserably".  Cinegoer wrote:"Veera is an extremely ordinary or routine subject with the regular thrills, frills, love and romance bereft of reality, logic, nevertheless it has its share of twists here and there that could take you by surprise".  Greatandhra wrote:"this is a film which gives a stale taste of boredom even to the masses". 

==Box office==
The film got a good start and had a Decent run at the Box office. It completed 50 Days in 60 Centres

==References==
 

==External links==
* 

 
 
 
 