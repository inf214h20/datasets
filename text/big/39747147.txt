The Drama of the St. Mary's Church Tower
{{Infobox film
| name           = The Drama of the St. Marys Church Tower
| image          =
| image_size     = 
| caption        = 
| director       = Wiktor Bieganski
| producer       = 
| writer         = Wiktor Bieganski
| narrator       = 
| starring       = Włodzimierz Kosiński   Helena Górska   Władysław Puchalski   Wiktor Biegański
| music          = 
| editing        = 
| cinematography = Rajmund Czerny
| studio         = 
| distributor    = 
| released       = 1913
| runtime        = 
| country        = Poland
| language       = Silent   Polish intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Polish silent silent drama film directed by Wiktor Bieganski and starring Włodzimierz Kosiński, Helena Górska and Władysław Puchalski. The film was Bieganskis debut as a director. It was filmed in Lvov and Krakow, then part of the Austro-Hungarian Empire. It is likely that the film and Bieganskis next production, The Adventures of Anton, were never put on general release.  Elements of the film still survice. 

==Cast==
* Włodzimierz Kosiński as Szmidt
*Helena Górska as Ada
*Wiktor Biegański as Rudolf
* Władysław Puchalski as kamieniarz

==References==
 

==Bibliography==
* Haltof, Marek. Polish National Cinema. Berghahn Books, 2002.
* Skaff, Sheila. The Law of the Looking Glass: Cinema in Poland, 1896–1939. Ohio University Press, 2008.

 

 
 
 
 
 
 
 
 
 
 
 