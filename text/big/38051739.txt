Irumbu Pookkal
{{Infobox film
| name           = Irumbu Pookkal
| image          = 
| image_size     =
| caption        = 
| director       = G. M. Kumar
| producer       = R. Pushpa
| writer         = G. M. Kumar
| starring       =  
| music          = M. S. Viswanathan Ilaiyaraaja
| cinematography = Ilavarasan
| editing        = Shyam Vincent
| distributor    =
| studio         = Pushphalayam Movies
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Tamil
}}
 1991 Tamil Tamil crime Karthik and Pallavi in lead roles. The film, produced by R. Pushpa, had musical score by Ilaiyaraaja and M. S. Viswanathan and was released on 16 February 1991. 

==Plot==

A military officer (Prabhu (actor)|Prabhu), who lost his leg, was back to his village. His wife committed suicide when she received the letter of his presumed death. His children, his cousin (Rekha (South Indian actress)|Rekha) and he asked for a signature from the village politician for a pension but the corrupted politician refused until his cousin accepted to spend a night with him. His cousin committed suicide and he was sent to jail. There, the police officers killed him. His son Dharma took revenge on the police officer, was sent to jail and grew up there.

Dharma (Karthik (actor)|Karthik) becomes a famous singer in jail and his sister becomes a reporter. K. Balasubramaniyam (Sathyajith), an infamous politician, tries to become a minister. His party weakness was the lack of women voters. He arranges a speech with his female members, he then manages to kill and rape them by the ruling partys supporters and accuses the ruling party. Dharmas sister was also raped in this massacre. K. Balasubramaniyam wins widely the election. Chitra (Pallavi (entertainer)|Pallavi), a feminist journalist and the only witness of the massacre, tries to take revenge with Dharma.

==Cast==
 Karthik as Dharma Pallavi as Chitra
*Sathyajith as K. Balasubramaniyam
*R. P. Viswam Livingston
*Kumari Muthu Prabhu in a guest appearance Rekha in a guest appearance

==Soundtrack==

{{Infobox Album |  
| Name        = Irumbu Pookkal
| Type        = soundtrack
| Artist      = Ilaiyaraaja M. S. Viswanathan
| Cover       = 
| Released    = 1991
| Recorded    = 1991 Feature film soundtrack |
| Length      = 17:30
| Label       = 
| Producer    = Ilaiyaraaja M. S. Viswanathan
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja and M. S. Viswanathan. The soundtrack, released in 1991, features 4 tracks.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Composer !! Lyrics !! Duration
|-  1 || Gangai Amaran || 3:47
|- 2 || Muththaana Muththam || S. P. Balasubrahmanyam, P. Susheela || 4:41
|- 3 || Mano || Ilaiyaraaja || 4:23
|- 4 || Rettai Kuruvi || Mano, P. Susheela || 4:39
|}

==References==

 

 
 
 
 
 
 