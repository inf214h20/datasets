Trigger (film)
 
{{Infobox film
| name = Trigger
| image = Trigger2010Poster.jpg
| alt =  
| caption = Film poster Bruce McDonald
| producer = Leonard Farlinger Jennifer Jonas
| executive_producer = {{Plainlist|
Bryan Gliserman
*Dany Chiasson
*Hugh Dillon}}
| writer = Daniel MacIvor
| starring = {{Plainlist|
*Molly Parker
*Tracy Wright
*Don McKellar
*Sarah Polley}}
| music = Brendan Canning
| cinematography = Jonathon Cliff
| editing = Matthew Hannam
| studio =
| distributor =
| released =  
| runtime =
| country = Canada
| language = English
| budget =
| gross =
}} Bruce McDonald, the film stars Molly Parker and Tracy Wright as Kat and Vic, former rock stars reuniting their band Trigger for the first time since their retirement.

The film was originally planned in the late 1990s as a companion film to McDonalds Hard Core Logo, which would have starred Hugh Dillon and Callum Keith Rennie.  However, the film was never made at that time, and remained dormant until McDonald and screenwriter Daniel MacIvor decided to rewrite their original screenplay to be about two women instead.  Rennie does, however, appear in the film as his Hard Core Logo character Billy Tallent.

The films cast also includes Daniel MacIvor, Don McKellar, Sarah Polley, Lenore Zann, Carole Pope and Julian Richings. Brendan Canning of Broken Social Scene wrote the films score.

Wright was undergoing treatment for  , September 30, 2010.  It was the last film she completed before her death. 

==Awards and nominations==
The film garnered four nominations at the 31st Genie Awards, including Best Actress nods for both Wright and Parker.
 ACTRA Toronto Awards.  . Torontoist, February 26, 2011.  Wrights husband McKellar, who appeared in the film, accepted the award in her honour, stating in his speech that the award "means more to me than any Ive ever won". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 