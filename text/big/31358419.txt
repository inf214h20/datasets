Me or the Dog
{{Infobox film
| name           = Me or the Dog
| image          = 
| alt            =  
| caption        = Festival poster
| director       = Abner Pastoll
| producer       = Junyoung Jang Jonathan Hall
| starring       = Edward Hogg Martin Clunes Kemi-bo Millar Julian Bird
| music          = Laura Rossi
| cinematography = Kate Reid
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 14 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Me or the Dog   on IMDb  is a 2011 short film directed by Abner Pastoll, produced by Junyoung Jang and starring Edward Hogg, about a man who believes that his dog, Dudley, is talking to him. Martin Clunes plays the mischievous voice of the dog, who challenges his owner Tom with the prospect of him actually being a real talking dog, and in order to do so sets out to prove that Toms girlfriend is cheating on him.

It world-premiered at the Festival de Cannes   Court Métrage  in Coup de Coeur, out of competition, on the 20 May 2011 and was met with an overall positive reception. The film was part of a featured showcase in a programme of only 8 short films. It was subsequently nominated for Best Comedy at the 2012 London Short Film Festival, released theatrically in London, UK and screened in official competition at several film festivals including: LA Shorts Fest, Atlantic Film Festival, Anchorage, Asheville, Emden, Berlin Shorts and the Bradford Film Festival where it was described as  "Imaginative... Exploring mental illness and fidelity,   light viewing with dark underlying subject-matter."
 Jonathan Hall and also stars newcomer Kemi-bo Millar. It was funded by the Wellcome Trust   Catalogue  with the aim of a positive portrayal of schizophrenia and to raise public awareness of the condition. During the development process, the filmmakers received support from mental health organisations such as Mind and Rethink to ensure the condition, in this case specifically auditory hallucinations, was portrayed as accurately as possible.

In December 2013, it was featured in top German news publication Zeit Online,   Short Film Blog  as well as being named the LA Shorts Fest online Short of the Week   Short of the Week, Dec 3, 2013 

==References==
 

==External links==
*  

 
 
 
 


 