Aarada Gaaya
{{Infobox film
| name = Aarada Gaaya
| image =
| caption = 
| director = V. Somashekhar
| writer = M. D. Sundar
| producer = Gowramma Somasekhar   Dhanalakshmi Vijay Gayatri
| music = Chellapilla Satyam
| cinematography = P. S. Prakash
| editing = P. Bhaktavatsalam
| studio  = Vijayashekar Movies
| released =  1980
| runtime = 137 min
| language = Kannada  
| country = India
}}
 1980 Indian Kanchana appeared in pivotal roles.  The music was composed by Chellapilla Satyam to the lyrics of Chi. Udaya Shankar.

==Cast==
 
*Shankar Nag  Gayatri
*Sowcar Janaki Kanchana
*Vajramuni
*Jai Jagadish
*Thoogudeepa Srinivas
*Tiger Prabhakar
*Rajashankar
*Dinesh
*Shakti Prasad
*Jyothi Lakshmi
*Jayamalini
*N. S. Rao
 

==Soundtrack==
All the songs are composed and scored by Chellapilla Satyam.  The songs "Nanna Baala Baaninalli" and "Bidu Bidu Kopava" were instant hits upon release.

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) !! Lyricist
|-
| 1 || "Bedaruve Eke Heege" || S. P. Balasubramanyam, S. Janaki || Chi. Udaya Shankar
|-
| 2 || "Ninne Ninne" || S. Janaki || Chi. Udaya Shankar
|-
| 3 || "Bidu Bidu Kopava" || S. P. Balasubramanyam, S. Janaki || Chi. Udaya Shankar
|-
| 4 || "Eno Ariye Naanu" || S. Janaki  || Chi. Udaya Shankar
|-
| 5 || "Nanna Baala Baaninalli" || P. Susheela || Chi. Udaya Shankar
|}

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 


 