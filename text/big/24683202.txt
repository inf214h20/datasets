Pov Chouk Sar
{{Infobox film
| name           = Pao Chouk Sao/ Pao Chouk Saw
| image          = Pov-chhouk-sor.jpg
| caption        = Promotional poster
| director       = Tea Lum Kang
| producer       = Tea Lum Kang
| writer         = Tea Lum Kang
| narrator       =
| starring       = Chjea Yuton and Dy Saveth
| music          = Sinn Sisamouth
| cinematography = Deap Ttrong
| editing        =
| distributor    = Deap Va Sing
| released       = 1967
| runtime        =
| country        = Cambodia
| language       = Khmer
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Pao Chouk Saw is the story of a girl who was once an angel and later became a human for a particular reason. In order to return to her sisters as an angel again, she has to live in the human world with regular human beings for 17 whole months. She then ends up falling in love with a regular guy. But soon he has to leave her behind for a war without knowing that she is actually an angel taking form of a human being and that she can change back to an angel any time soon after he leaves for the war. He leaves having no idea that once he return from war, she may be gone forever.

==Cast==
* Chea Yuthon
* Dy Saveth

==Soundtrack==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| Nevea Jivit
| Sinn Sisamouth
|
|-
| Keng Tov Keng Tov
| Sinn Sisamouth
|
|-
| Keng Tov Keng Tov
| Ros Serey Sothear
|
|}

==References==
*  
*  

 
 
 


 