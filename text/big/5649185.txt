The Breed (2001 film)
 
{{Infobox film
| name           = The Breed
| caption        = 
| image	=	The Breed FilmPoster.jpeg
| director       = Michael Oblowitz Jim Burke Adam Richman
| writer         = Christos N. Gage Ruth Fletcher
| starring       = Adrian Paul Bokeem Woodbine Bai Ling Roy Hay
| cinematography = Chris Squires Matthew Booth Emma E. Hickox
| distributor    = Columbia TriStar Home Video
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $4 million
| preceded_by    = 
| followed_by    = 
}}
The Breed is a 2001 horror film with an estimated budget of 4 million dollars.

The film features a dystopic future in which vampires are a marginalized race living in formerly Jewish ghettos, often shot in actual abandoned Jewish ghettos.  Another major influence in the look of the film is Terry Gilliams Brazil (1985 film)|Brazil. 

==Plot==
In an unidentified, vaguely totalitarian future, Detective Stephen Grant (Bokeem Woodbine) investigates a series of strange murders; his partner is killed by a pale man in black who can survive bullet wounds, tosses Grant through a window, and climb up walls like a reptile.  After Grant reports the incident, his superiors introduce him to another officer, Aaron Gray (Adrian Paul).  Grant learns that vampires exist—Gray is one of them—and plan to gradually integrate themselves with the rest of humanity.  One of these vampires is responsible for the murders and the other vampires want to help catch the perpetrator.  Aiding the officers is a female vampire named Lucy Westenra (played by Bai Ling). 

Lucy Westenra and Detective Grant eventually become lovers and this complicates the lives of the police officer and the beautiful female vampire as later circumstances seem to implicate everyones hidden agenda, betrayal, and deception. All the various human and vampire characters seem to be up to something sneaky and questionable as the movie progresses.

In absolute secrecy, humans have created a virus that is capable of killing only vampires while leaving humanity unharmed. The virus is made as a failsafe device, in case the plan to coexist between vampires and humans fails. Hidden in a veil of deceit, the elder vampire makes a cynical plan to eradicate humanity if they wont take the cure made from vampire blood and become vampire themselves. The vampire leader believed that as long as humans and vampires remain separate races, there will always be conflict and warfare. He forces the creator of the vampire virus to alter the nature of the virus, making it deadly to humans and not to vampires. The virus creator, Dr. Fleming, doesnt seem to need all that much coercion to make a human biological weapon, as he wanted immortality and the Elder vampire was all too happy to manipulate him. Dr. Fleming pulls a gun and tries to eliminate the police officers when they discover his collaboration with the vampire leader. The vampire leader kills Fleming to silence him when the plot starts to unravel. 

A renegade vampire resistance leader mistrusts human-vampire cooperation and would use any excuse to start his human-vampire war to determine who would control the world. On the government side, Seward also mistrusts coexistence and would later order government troops to set up an ambush to attack the vampire migrants, and the vampire police force escorting them. 

Only due to the trust between the human police officers and their vampire allies is the deadly virus plot brought to an end. The subplot of the vampire renegade leader and the overly zealous government agent Seward were also resolved without a bloody war. The high ranking government director ordered Seward to stand down his troops. Its also implied that the surviving vampire leadership reined in the wild vampire renegades with vampire police forces. Human Detective Grant and Vampire Officer Grey become permanent police partners at the end of the movie. Grant and Lucy Westenra seem to have become a romantic couple, and he even moves into her luxurious mansion.

== Cast ==
*Adrian Paul - Aaron Gray, vampire police detective
*Bokeem Woodbine - Steve Grant, human police detective, Lucy Westenras boyfriend
*Bai Ling - Lucy Westenra, wealthy beautiful vampire artist, Steve Grants lover
*Péter Halász (actor)|Péter Halász - Cross
*James Booth - Fleming, traitor government scientist, co-conspirator allied to vampire Dr. Orlok
*Lo Ming - John Seward|Seward, aggressive war mongering & trigger happy government agent intent on declaring war on vampires Paul Collins - Calmet
*Debbie Javor - Section Chief, high ranking government official
*Reed Diamond - Phil
*John Durbin - Boudreaux, renegade vampire leader intent on starting human-vampire war for world domination where only vampires win & humans are exterminated
*Zen Gesner - West
*István Gőz - Dr. Orlock, supreme vampire leader, planned to dominate the world (vampire & humans) by making humans into vampires or wipe out all human opposition
*William Hootkins - Fusco, vampire Italian actor, aided Gray, Grant & Westenras mission

==See also==
*Vampire film

==References==
 

==External links==
*  
*  

 
 
 
 