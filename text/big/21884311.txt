Teza (film)
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name = Teza
| image =
| image size =
| alt =
| caption =
| director = Haile Gerima Philippe Avril Karl Baumgartner Marie-Michèlegravele Cattelain Haile Gerima
| writer = Haile Gerima
| narrator =
| starring =
| music = Vijay Iyer Jorga Mesfin
| cinematography = Mario Masini
| editing = Haile Gerima Loren Hankin
| studio =
| distributor = Mypheduh Films (USA)
| released =
| runtime = 140 minutes
| country = Ethiopia, Germany, France German
| budget =
| gross =
| preceded by =
| followed by =
}}
Teza is a 2008 Ethiopian drama film about the Derg period in Ethiopia. Teza won the top award at the 2009 Panafrican Film and Television Festival of Ouagadougou. The film was directed and written by Haile Gerima. 

==Synopsis==
Set in 1970s Ethiopia, Teza (Morning Dew) tells the story of a young Ethiopian as he returns from West Germany a postgraduate. Anberber comes back to a country at the height of the Cold War and under the Marxist regime of Mengistu Haile Mariam. Working in a health institution he witnesses a brutal murder and finds himself at odds with the revolutionary gangsters running the country. He is ordered by the regime to take up a post in East Germany and uses this opportunity to escape to the West until the Berlin Wall falls and Ethiopias military regime is overthrown.

Now aged 60, Anberber finally returns to his home village. Although he finds comfort from his ageing mother he feels alienated from those around him by his absence from home for so long and is disillusioned and haunted by his past. 

==Production==
According to the director, Haile Gerima, who is also the executive producer and writer, Teza took 14 years to make.  The writing underwent changes during those years, maturing and blossoming in certain areas. Tezas scheduling, especially its Germany shoot had to undergo radical change due to funding issues which cut the scheduled 3 week shoot to 10 days.  There was a 2-year gap between the wrap of shooting in Ethiopia in August 2004 and the beginning of the German shooting in November 2006. 

Actor Aaron Arefe(Anberber) burst a blood vessel in his right eye during a particularly stressful scene. The director sent the actors home for rest, the next day Arefaynes eye was completely covered in a film of blood.  The production schedule would not permit any more breaks and the director was obliged to continue shooting. There were other challenges, some epic, some humorous. All in all, the director, cast and crew seemed to be able to overcome the many challenges to deliver a film that delivers an important message that audiences from Italy, to Dubai responded to with enthusiasm. 

==Reception== The Wrestler. Even so Teza won the Special Jury and Best Screenplay awards. The film was next invited to Toronto, where it was also well received. It was entered in competition at the Carthage International Film Festival in Tunisia where it swept 5 categories, including Tanit DOr for Best Film, Best Screenplay (Haile Gerima), Best Music (Vijay Ayer and Jorga Mesfin), Best Supporting Male Lead (Abeye Tedla) and Best Cinematography (Mario Masini). Thereafter its showing in the Dubai International Film Festival achieved best score for Jorga Mesfin and Vijay Ayer.

==Awards and nominations==
* FESPACO in Burkina Faso:
* Golden Stallion of Yennenga
* UN Anti-Poverty Prize to Haile Gerima
* Zain prize for originality, technical quality and performance.
* Venice Film Festival  SPECIAL JURY PRIZE
* OSELLA Prize  Best Screenplay (Haile Gerima)
* SIGNIS Award Special mention to Teza by Haile Gerima
* Leoncino doro Award 2008 (Agiscuola):
* Cinema for UNICEF commendation  Teza by Haile Gerima
* CinemAvvenire Cinema for peace and the richness of diversity Award: Teza by Haile Gerima
* Carthage Film Festival in Tunisia
* Golden Tanit- Best Film
* Best Music Best Cinematography
* Best Male Supporting role
* Amiens International Film Festival in France:
* Golden Unicorn - Best Feature Film
* Rotterdam International Film Festival
* Dioraphte Award Hubert Bals film in highest audience regard.
* Dubai International Film Festival in Dubai
* Best Music
* Amazonia Film Festivalin Venezuela
* Amazonia Award
* Thessaloniki Film Festival in Greece
* Human Values Award

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 