Return of the Living Dead Part II
{{Infobox film name = Return of the Living Dead Part II image = TheReturnOfTheLivingDead2.jpg caption = director = Ken Wiederhorn producer = Tom Fox William S. Gilmore writer = Ken Wiederhorn starring = James Karen Thom Mathews Dana Ashbrook Marsha Dietlein Phil Bruns Michael Kenworthy cinematography = Robert Elswit music = J. Peter Robinson editing = Charles Bornstein distributor = Lorimar Pictures (theatrical release) Warner Bros. (DVD release) released =   runtime = 89 min country = United States language = English budget = $6,200,000 (estimated) gross = $9,205,924 (USA)
}}
 Lorimar Motion four sequels to The Return of the Living Dead. This film has a lighter tone as it was partially aimed at a teenage audience; however the misleading trailer suggested it was darker. The main protagonists, Jesse and Lucy, share the last name Wilson, giving the cult fans a clue that they are related to Burt Wilson, the main protagonist of the first film.

==Plot==

The story begins with a military truck transporting barrels of Trioxin. The soldier, driving the vehicle through a downpour, is unaware when a barrel breaks loose and falls into a river. The next morning, a young boy, Jesse Wilson, is at the cemetery with 2 local bullies. The trio investigate the Trioxin tank that they find, and Jesse warns them that they should not tamper with it. The bullies trap Jesse in a derelict mausoleum and leave him. They then return to the trioxin tank and manage to release the toxic gas. A van pulls up to the graveyard, introducing the characters Ed, Joey, and Brenda. Ed explains to Joey that they are there to rob graves; Brenda expresses her fears for cemeteries, but Joey assures her that it will be worth their time and leaves Brenda in the van.  He heads into the cemetery with Ed. They decide to loot the mausoleum and open the locked doors, releasing Jesse, who immediately runs home. At his home, Jesse watches his older sister, Lucy, doing aerobic exercises to a work-out video; she tells Jesse to do his homework or he will be grounded.

Later, in the night, a cable technician arrives at the Wilson house to install cable TV. As he enters, Jesse manages to sneak out the back door and heads to Billys home, one of the bullies from earlier. Upon Jesses arrival, Billys mother tells Jesse of Billys illness and allows him a brief visit. Jesse is shocked to see the effect of the Trioxin, which is making Billy ill. Billy whimpers to Jesse not to tell what theyve found, but is interrupted by his mother, who asks Jesse to leave. Jesse returns to the sewer drains to further examine the Trioxin tank, and finds a phone number for the military.  He is attacked by a tar-covered zombie who has escaped from the tank. He flees to the cemetery and witnesses a hand reaching from one of the graves and runs home before the zombies rise.

Ed and Joey are still inside the Mausoleum and witness a zombie awakening from its tomb. They club him with a crowbar, which has no effect. Outside, a worried Brenda enters the cemetery, unaware of what is going on. She is spooked by a zombies approach and tries to flee, only to be stopped by a zombie that has crept up on her from behind. She punches him, crushing his face, and runs deeper into the graveyard. Brenda encounters Joey and a frantic Ed, and they escape to the streets, where they meet Billys parents and warn them of the zombies before fleeing. Jesse returns home to an angry Lucy, who chases him into their parents bedroom; he locks the door and calls the military for help and is placed on hold.

Outside Joey, Ed, and Brenda steal Toms van and accidentally knock a zombie into a telephone pole, disconnecting Jesses call. Billys father threatens them with a gun, but he is attacked by the supposedly-dead zombie. The streets are now flooded with the living dead, and the traumatized group run to the Wilson house for refuge. After they enter, an argument occurs over what is happening; Joey and Ed also show symptoms very similar to Billys. Jesse assures everyone that theyre zombies and that Dr. Tom Mandel, a neighbor, can help them all escape. Billys mother leaves her house and sees her husband being eaten by a group of zombies. She quickly returns to her house, removing her glasses, and is then killed by a now-zombified Billy. Jesse and his group make their way to the Doctors house and get trapped in his garage. Zombies break in but the doctor escapes with Jesse and the others in a vehicle. Tom drives through a horde of zombies, knocking one onto the cars roof. During their journey, the zombie reaches through an open window, only to have its hand cut off. The hand continues to attack before it is thrown from the cars window.

Arriving at a deserted hospital, Joeys and Eds health deteriorate. Jesse, Lucy, and Tom leave to get more ammunition and guns from their uncles home after realizing there is no help. Dr. Mandel tells Brenda that Joey and Ed are more or less dead; she vehemently disagrees with the diagnosis and calls him a quack. She leaves with the dying Joey; Ed, however, follows - much to Brendas annoyance. They are stopped by three military men wielding guns. Ed attacks one, eating his brains, and the two remaining soldiers drive off, leaving Brenda helpless. Ed is occupied with eating his victim, so Brenda uses this opportunity to ditch him, and escapes with Joey. In the car, Joey transforms into a zombie. He confesses that he wants to eat Brendas brains and attacks her; she escapes unhurt from the car and bumps into a dazed zombie. She manages to get her hand trapped in the zombies mouth, and rips his jaw from his face. Joey runs toward her and chases her into an empty church, explaining that he wants her spicy brains. Brenda retaliates by saying, "Im not into dead guys!" Joey explains that he loves her. Seeing no other option, Brenda allows him to eat her brains.

The others return to the hospital and collect Dr. Mandel. They devise a plan to lead the zombies to a power plant and electrocute them all, using a trail of frozen brains. After arriving at the plant, they place brains into puddles of water, with electrical wires in each puddle. Zombies ambush them after Billy opens the large entry gates. Lucy and Tom hide in the back of the truck, and zombies begin breaking through the trucks door. Jesse, who is now inside the plant, is attacked by Billy and stabs him with a screwdriver. Jesse activates the power, killing all of the zombies. Billy walks in, holding the screwdriver, and pushes Jesse onto a control panel. While this is happening, a large transformer falls through the roof. Dr Mandel distracts Billy by telling him his fly is open, and Jesse kicks Billy into the transformer, electrocuting him. As Jesse, Lucy, Tom, and Dr. Mandel leave the plant, the military arrives to dispose of the bodies with flamethrowers.

==Cast==
* Michael Kenworthy as Jesse Wilson
* Thor Van Lingen as Billy Crowley
* Jason Hogan as Johnny
* James Karen as Ed
* Thom Mathews as Joey
* Suzanne Snyder as Brenda
* Marsha Dietlein as Lucy Wilson
* Hanala Sagal as Aerobics Instructor (as Suzan Stadner)
* Jonathan Terry as Col. Glover
* Dana Ashbrook as Tom Essex
* Sally Smythe as Mildred Crowley
* Allan Trautman as Tarman
* Don Maxwell as George Crowley
* Reynold Cindrich as Soldier
* Philip Bruns as Doc Mandel
* Mitch Pileggi as Sarge
* Art Bonilla as Les (as Arturo Bonilla)
* Terrence Riggins as Frank
* James McIntire as Officer
* Larry Nicholas as Jesses Double
* Forrest J Ackerman as Harvey Kramer (Special Zombie) (as Forrest Ackerman) Douglas Benson as Special Zombie
* David Eby as Special Zombie
* Nicholas Hernandez as Special Zombie
* Derek Loughran as Special Zombie
* Annie Marshall as Special Zombie Richard Moore as Special Zombie
* Steve Neuvenheim as Special Zombie
* Brian Peck as Pussface Zombie; Thriller Zombie; Jaw Zombie; Eye-Pop Zombie; Zombie on Car Roof
* Eric Clarke as Zombie (uncredited)
* Bobby Porter as Zombie Kid (uncredited)
* Dana Vitatoe as Zombie (uncredited)

==Soundtrack==
Released on Island Records in 1988.

# "Space Hopper" by Julian Cope
# "High Priest of Love" by Zodiac Mindwarp and the Love Reaction Anthrax
# "Big Band B-Boy" by Mantronix
# "Monster Mash" by The Big O
# "Alone in the Night" by Leatherwolf Anthrax
# "Flesh to Flesh" by Joe Lamont
# "The Dead Return" by J. Peter Robinson

==DVD release== French audio track. 

Actress Marsha Dietlein was asked in an August 2012 interview if the film would receive a newer and perhaps "special" DVD release, as was recently done for the first installment in the franchise. "I wish they would do a retrospective, but I havent heard anything about it," she said. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 