Life! Camera Action...
{{Infobox film
| name           = Life! Camera Action...
| image          = RNGFilms.jpg
| caption       = Movie poster
| director       = Rohit Gupta
| producer       = Rohit Gupta
| executive producer       = Rohit Gupta Parag Vaishnav
| writer         = Rohit Gupta Amanda Sodhi
| starring       = {{Plainlist|
* Dipti Mehta
* Shaheed Woods
* John Crann
* Swati Kapila
* Suneet Kochar
}}
| music          = Manoj Singh 
| cinematography = Ravi Kumar R. Rohit Gupta
| editing        = Rohit Gupta
| studio         = RNG Films Distrify (online worldwide)
| released       =   United States
| country        = United States Punjabi
| runtime        = 90 minutes

}}

Life! Camera Action... is a Drama film|family-drama film directed, written, edited, produced by Rohit Gupta.     Starring Dipti Mehta, Shaheed Woods,  Noor Naghmi,  Swati Kapila,    John Crann,  this ninety minutes quasi-autobiographical film follows a young girl in pursuit of her dreams of becoming a filmmaker against all odds. The film received several awards and nominations.          It received direct-to-video|direct-to-DVD release and subsequently on other video on demand (VOD) platforms.  

Life! Camera Action... is noted for its production value and its message widely praised.  Silicon India listed it as "One of the 10 Outstanding Movies by Indian American Filmmakers".  It made its way to the Limca Book of Records, Indias equivalent of the Guinness World Records for being the first full-length motion picture "shot by just a two-member crew".      

==Plot==
Life! Camera Action... is an inspiring story of Reina, who sets off to pursue a career in filmmaking without the consent of her parents. She is threatened to be disowned if she insists on her choice instead of doing what usually is the norm – become a doctor, engineer or an architect. At the center of this family drama, Reina is faced with a hard decision: She must give up her dreams to keep her parents happy or go against their wishes and follow her own path.

==Cast==
* Dipti Mehta as Reina
* Shaheed Woods as Mike
* John Crann as Professor Ed
* Swati Kapila as Simi
* Suneet Kochar as Actress 
* Chelsi Stahr as producer Teri 
* Subodh Batra as Reinas father
* Noor Naghmi 
* Prabha Batra as Reinas mother
* Nina Mehta as Actress    
* Bhavesh Patel as Patel DVD store owner

==Production==
Life! Camera Action... started as Rohit Guptas ten-minute short film assignment at the New York Film Academy that he expanded into a full length feature film.    Amanda Sodhi, the co-writer of the film, in response to an advertisement had originally submitted a fifteen-page short film script loosely based on her own life.    The project was tentatively titled "The Last Shot".  Gupta expanded upon the content, calling the extended version Life! Camera Action... with The Last Shot  becoming a part of it.   The film was shot in guerilla style on Panasonic AG-DVX100 in ten days and roughly edited on Guptas Mac Book Pro.    It was filmed on an initial budget of around $4000 with money raised from his savings that was cleaned up with post-production work costing several thousand dollars before its marketing and release. In an interview Dipti Mehta has said that in an informal meeting before the audition, Gupta thought she was not right for the part, although she was invited at the audition where he failed to recognize her at the first sight. Mehta did get the part and it became her debut film as a lead.   There was no official script of the film and was mainly shot with actors improvising during its filming. 

===Filming locations=== Newport Waterfront.

===Music===
The background score and songs were composed by Manoj Singh.  The music was produced in Mumbai, India. The following songs are featured in the film and used as a part of the background score.

* "Hain Yeh Kaisa Safar" (What kind of Journey is this) lyrics by Amanda Sodhi. 
* "Chalte Jaana Hain" (Have to keep Walking) lyrics by Rohit Gupta performed by KK (singer)|KK. 

==Release==
  New York FYE (For Your Entertainment), Suncoast and Wherehouse.com.  Flipkart, Indias largest e-commerce consumer company released the film on 17 February 2013.  On International Womens Day, 8 March 2013 the film was released worldwide through Distrify, a UK based VOD movie distribution company for online streaming and movie download in any global currency.   The film was released to other VOD service such as the Amazon Instant Video. 

==Reception==
 
  Indian International NFDC Film Bazaar in Goa; Beloit International Film Festival;   Filums (LUMS International Film Festival), Pakistan;  Bostons Museum of Fine Arts (MFA) South Asian Film Series, USA;  Heart of England International Film Festival; Dhaka International Film Festival (Cinema of the World section), Bangladesh;  Legacy Media Institute International Film Festival, Virginia;  Delhi International Film Festival, India;  International Youth Film Festival in the UK; Silent River Film Festival, USA;  and others.


==Awards and nominations==
The film received various accolades including some of the highest honors at its festival run, with the nominations categories mainly ranging from recognition of the film itself (Best Film) to its direction, Film editing, music, screenwriting to the casts acting performance, mainly Dipti Mehta (Best Actress), Swati Kapila (Best Supporting Actress),  Noor Naghmi (Best Supporting Actor)  and Shaheed Woods (Best Actor).

 
 

{| class="wikitable" style="font-size: 95%;"
|- style="text-align:centr;"
! style="background:#cce;"| Festival/Award
! style="background:#cce;"| Location
! style="background:#cce;"| Category
! style="background:#cce;"| Recipients and nominees
! style="background:#cce;"| Outcome
|- Accolade Film Awards  USA
|Award of Excellence – Best Feature Film
|Life! Camera Action...
| 
|- Award of Merit for Direction Rohit Gupta
| 
|- Award of Merit for Cinematography Ravi Kumar R.,Rohit Gupta
| 
|- Award of Merit for Dramatic Impact
|Life! Camera Action...
| 
|- Action On Film International Film Festival  USA
|Best Composition Manoj Singh
| 
|- Best Art Direction Ravi Kumar R, Rohit Gupta
| 
|-
 California Film Awards  USA
|Orson Welles Award for Best Narrative Film
|Life! Camera Action...
| 
|- Canada International Film Festival  Canada
|Royal Reel Award for Best Feature Film
|Life! Camera Action...
| 
|- Carmarthen Bay Film Festival  UK
|Best Feature Film
|Life! Camera Action...
| 
|- Golden Door Film Festival of Jersey City  USA
|Best Feature Film
|Life! Camera Action...
| 
|- Best Director Rohit Gupta
| 
|- Best Male Lead Shaheed Woods
| 
|- Best Female Lead Dipti Mehta
| 
|- Indie Fest  USA
|Award of Merit – Feature film
|Life! Camera Action...
| 
|- International Film Festival for Peace, Inspiration & Equality aka World Peace Film Festival  Indonesia
|Best New Comer film
|Life! Camera Action...
| 
|- International Film Festival of World Cinema  UK
|Best Film of the Festival
|Life! Camera Action...
| 
|- Best Director Rohit Gupta
| 
|- Best Supporting Actress Swati Kapila
| 
|- Los Angeles Movie Awards    USA
|Award of Excellence for Best Experimental Film
|Life! Camera Action...
| 
|- Nevada International Film Festival  USA
|Platinum Reel Award for Best Narrative Feature Film
|Life! Camera Action...
| 
|- Oregon Film Awards  USA
|Grand Jury Choice Award for Best Film
|Life! Camera Action...
| 
|- Silent River Film Festival  USA
|River Pursuit Award for Best Film
|Life! Camera Action...
| 
|- Best Actress Dipti Mehta
| 
|- River Amulet Award for Best Director Rohit Gupta
| 
|- Swansea Bay Film Festival 
||Wales, UK Best Feature Film-North America
|Life! Camera Action...
| 
|- Sunset International Film Festival   USA
 	 Award for Best Female Actor Dipti Mehta
| 	 	
|-
 Toronto Independent Film & Video Awards  Canada
|Best Experimental Film
|Life! Camera Action...
| 
|- World Music & Independent Film Festival 
 	 USA
	 Best Drama
	
|Life! Camera Action...
	
| 
	
|-
 Best Director
	 Rohit Gupta
	
| 
	
|-
	 Best Actress
	 Dipti Mehta
	
| 
 	
|-
	 Best Original Music
 	 Manoj Singh
	
| 
	
|-
	 Best Screenplay
	 Rohit Gupta, Amanda Sodhi
	
| 
	
|-
	 Best Actor in Supporting Role
	 Noor Naghmi
	
| 
	
|- Best Actress in Supporting Role Swati Kapila
| 
	
|- Yosemite International Film Awards 
 	 USA
  Grand Jury Choice Award for Best Film
 
|Life! Camera Action...
	
| 
|-
|}

==Limca Book of records==
{| class="wikitable" style="width: 85%; font-size: 0.98em;"
!Record
!Honoring body
|- Film Life! Camera Action... was shot by just a two member crew comprising of director/producer Rohit Gupta & Ravi Kumar R.  Limca Book of Records
|-
|}

==References==
 

==External links==
* 
* 

{{Navbox name = Rohit Gupta title = Films directed by Rohit Gupta state = autocollapse listclass = hlist list1 = Another Day Another Life (2009)
* Just do it! (2011)
* Life! Camera Action... (2012) Midnight Delight (2015)
}} 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 