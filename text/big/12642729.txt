City Girl (1930 film)
{{Infobox film
| name           = City Girl
| writer         = Marion Orth, Berthold Viertel
| based on       =  
| starring       = Charles Farrell Mary Duncan
| director       = F.W. Murnau William Fox Arthur Kay Ernest Palmer
| editing        = H.H. Caldwell Katherine Hilliker
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 89 minutes
| language       = Silent film English intertitles
| country        = United States
| budget         =
}}
City Girl is an American 1930 silent film directed by F.W. Murnau. The director wanted the film to be called Our Daily Bread. 

A version of the film, with some sound elements, was made alongside the silent version.   Today the sound version is lost.

==Synopsis==
The city girl Kate falls in love with farmer Lem. He takes Kate to his family farm but Kate has trouble being accepted by the family.

==Cast==
*Charles Farrell as Lem Tustine
*Mary Duncan as Kate
*David Torrence as Mr Tustine
*Edith Yorke as Mrs Tustine Anne Shirley as Marie Tustine
*Tom McGuire as Matey
*Richard Alexander as Mac Pat Rooney as Butch
*Ed Brady as Reaper
*Roscoe Ates as Reaper
*Mark Hamilton as Hungry Reaper
*Ivan Linow as Taxi Driver
*Arnold Lucy as Cafe Patron
*Helen Lynch as Girl On Train
*Jack Pennick as Reaper Guinn Williams as Reaper

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 


 
 