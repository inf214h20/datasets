The Assassination of Jesse James by the Coward Robert Ford
{{Infobox film
| name           = The Assassination of Jesse James by the Coward Robert Ford
| image          = Assassination poster.jpg
| caption        = Theatrical poster
| director       = Andrew Dominik
| producer       = Ridley Scott Jules Daly Brad Pitt Dede Gardner David Valdes
| screenplay     = Andrew Dominik
| based on       =  
| narrator       = Hugh Ross Paul Schneider Jeremy Renner Zooey Deschanel Sam Rockwell  Warren Ellis
| cinematography = Roger Deakins Michael Kahn
| studio         = Virtual Studios Scott Free Productions Plan B Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 159 minutes
| country        = United States
| language       = English
| budget         = $30 million   
| gross          = $15 million 
}}
  western drama Ron Hansens novel of the same name, the film dramatizes the relationship between James (Brad Pitt) and Robert Ford (Casey Affleck), focusing on the events that lead up to the titular killing.

Filming took place in Edmonton, Alberta; Winnipeg, Manitoba; and Grafton, Utah. Initially intended for a 2006 release, it was postponed and re-edited for a September 21, 2007 release.

==Plot==
  Robert Ford Charley (Sam Kansas City, Paul Schneider) and his cousin, Wood Hite (Jeremy Renner). Jesse sends Charley, Wood and Dick away, but insists that Bob stay. He wanted the younger man just for his help in moving furniture to a new home in St. Joseph. Bob becomes more admiring of James before being sent back to his sisters farmhouse, where he rejoins his brother Charley, Hite, and Liddil. 
 Jim Cummins, Ed Miller (Garret Dillahunt), who gives away information on Cummins plot. Jesse kills Miller, then departs with Liddil to hunt down Cummins. Unable to locate him, Jesse viciously beats Albert Ford (Jesse Frechette), a young cousin of Bob and Charley. Liddil returns to the Boltons farmhouse, and argues with Hite, which ends with Bob Ford killing Hite. They dump his body in the woods to conceal the murder from Jesse. 
 Thomas T. Crittenden (James Carville). He is given 10 days to capture or kill Jesse James, and promised a substantial bounty and full pardon for murder.
 Zee James (Mary-Louise Parker) and their two children. Jesse wants to revive his gang by robberies with the Fords, beginning with the Platte City bank. On the morning of April 3, 1882, Jesse and the Ford brothers prepare to depart for the Platte City robbery. Jesse reads about the arrest and confessions of Liddil in the morning paper. While the three men are in the living room, Jesse removes his gun belt, and climbs a chair to clean a dusty picture. Robert Ford shoots him in the back of the head, and the Ford brothers flee. They send a telegram to the governor to announce Jesses death, for which they were to receive $10,000. They never receive more than $500 each.
 Edward OKelley (Michael Copeman), at his saloon in Creede, Colorado. OKelley is sentenced to life in prison, but Colorado Governor James Bradley Orman pardons him after ten years in 1902.

==Cast==
 
*Brad Pitt as Jesse James Robert "Bob" Ford
*Sam Shepard as Frank James
*Jeremy Renner as Wood Hite Charley Ford Paul Schneider as Dick Liddil Ed Miller Zerelda "Zee" James
*Zooey Deschanel as Dorothy Evans
*Alison Elliott as Martha Bolton
*Kailin See as Sarah Hite
*Ted Levine as James Timberlake Thomas T. Crittenden
*Michael Parks as Henry Craig Edward OKelley
*Nick Cave as Balladeer
 
The film is narrated by Hugh Ross.

==Production==
  Ron Hansens Heritage Park, Kananaskis area, several private ranches    and the historical Fort Edmonton Park.     The historical town of Creede, Colorado was recreated at a cost of $1 million near Goat Creek in Alberta.    Filming also took place in Winnipeg in the citys historic Exchange District; the Burton Cummings Theatre (formerly known as The Walker Theatre) and the Pantages Playhouse Theatre,     and concluded in December 2005. 
 Michael Kahn (who was brought in for several weeks as the studios "go to" editor), collaborated to assemble and test different versions. These did not receive strong scores from test audiences. Despite the negative response, the audiences considered the performances by Pitt and Affleck to be some of their careers best.    Brad Pitt had it written into his contract that the studio could not change the name of the film.   

===Cinematography===
  used palettes of brown and black to produce a bleak yet  
One of the most well-known sequences of the film is the scene of a train robbery at night time. Cinematographer   on the negative, which was especially important in terms of rendering detail. 
 color aberration around the edges. Deakins recalls:
  

Several time-lapse sequences appear throughout the film, which were shot by Steadicam operator Damon Moreau. According to Moreau, he was sent to do such shots when the crew was not quite ready to shoot a scene.   These time-lapse sequences were often accompanied by the films melancholic score, suggesting the passage of time and contributing to the uneasiness that builds up to the inevitable yet unsettling climax.  

===Music===
  Warren Ellis.  Both men collaborated to create the award-winning score for the Australian film The Proposition (2005).
 The Ballad of Jesse James" as performed by Cave. This folk song referred to Ford as a coward.

Cave and Ellis released a double disc album titled White Lunar in September 2009, which contains several tracks from the Jesse James score, as well as tracks they composed for other films up to 2009. 

==Release==
The Assassination of Jesse James by the Coward Robert Ford was originally slated for a September 15, 2006 release.   The release date was postponed to February 2007 at first,  but ultimately set for a September 21, 2007 release,  almost two years after filming was completed. 

The film opened in limited release on September 21, 2007, in 5 theaters and grossed $147,812 in its opening weekend, an average of $29,256 per theater.    The film has a total gross of less than $4 million.

Warner Home Video released the film on DVD on February 5, 2008  in the US, and on March 31 in the UK. So far, about 566,537 DVD units have been sold, bringing $9,853,258 in revenue. 

==Reception==

===Critical reception===

The film received primarily positive reviews and garnered a wide range of awards. On Rotten Tomatoes, the film has a 76% fresh rating, based on 169 reviews.  On Metacritic, the film has an average score of 68 out of 100, based on 32 reviews. 

Brian Tallerico of UGO gave the film an "A" and said that it is "the best western since Unforgiven." Tallerico also said, "Stunning visuals, award-worthy performances, and a script that takes incredibly rewarding risks, Jesse James is a masterpiece and one of the best films of the year."  Kurt Loder of MTV said, "If I were inclined to wheel out clichés like Oscar-worthy, Id certainly wheel them out in support of this movie, on several counts." 
 Ebert & McCabe and Mrs. Miller and The Long Riders, this is your film."  Roger Ebert noted the "curiously erotic dance of death" between James and the "mesmerized" younger Ford. Finally, he said, "If Robert cannot be the lover of his hero, what would be more intimate than to kill him?"  , Roger Ebert, 4 October 2007  He notes that it has the "space and freedom" of classic Western epics, where "the land is so empty, it creates a vacuum demanding men to become legends." 

 .  Josh Rosenblatt of The Austin Chronicle gave the film 3.5 stars and said the film "grabs on to many of the classic tropes of the Western&nbsp;– the meandering passage of time, the imposing landscapes, the abiding loneliness, the casual violence&nbsp;– and sets about mapping their furthest edges." 

Film critic Emanuel Levy gave the film an "A" and wrote, "Alongside   said "Impeccably shot, cast and directed, this is a truly impressive film from sophomore writer-director Andrew Dominik... but suffers from an unfortunate case of elephantiasis." Beale said Affleck is "outstanding in a breakout performance" and said Pitt is "scary and charismatic." Beale wrote, "The director seems so in love with his languorous pacing, hes incapable of cutting the five or ten seconds in any number of scenes that could have given the film a more manageable running time. In the scheme of things, however, this amounts to little more than a quibble." Beale said that ultimately, the film is "a fascinating, literary-based work that succeeds as both art and genre film." 

Critic Mark Kermode named the film as his best of 2007 in his end-of-year review on Simon Mayos BBC radio programme. Kermode later wrote that historians a hundred years from now will consider it "one of the most wrongly neglected masterpieces of its era." 

Many critics opined that the film is too long. Kirk Honeycutt of The Hollywood Reporter said that the relationship between Pitt and Affleck "gets smothered in pointlessly long takes, repetitive scenes, grim Western landscapes and mumbled, heavily accented dialogue."  Los Angeles Daily News critic Bob Strauss gave the film 2.5 stars out of 4 and said, "To put it most bluntly, the thing is just too long and too slow." Strauss also said, "Every element of this Western is beautifully rendered. So why is it a chore to sit through?"  Pam Grady of Reel.com gave the film 2 stars out of 4 and said, "The movie is merely a long, empty exercise in style."  Stephanie Zacharek of Salon.com said that the film "represents a breakthrough in the moviegoing experience. It may be the first time weve been asked to watch a book on tape."   

Peter Bradshaws review in The Guardian noted Jamess contribution to his own demise as well as the apparent paradox in the title of both novel and film:
{{quotation|
As his career draws to an end, Jesse James becomes aware of the impossibility of facing an increasingly vast army of sheriffs, federal agents and Pinkerton men. He senses that, inevitably, one of his gang will in any case sell him out for a fat reward. Unwilling to give the lawmen that satisfaction, James embraces his own death and subtly cultivates the mercurial attentions of the most obviously cringing and cowardly of his associates: 20-year-old Robert Ford. With the taunts and whims of a lover, he encourages Fords envious, murderous fascination, and grooms him as his own killer, so that his own legend will be pristine after his death. He engineers a character-assassination of Ford, and the title, knowingly, gets it precisely the wrong way around. }}

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2007. 

{{columns-list|3|
*1st – Mark Kermode, BBC Radio 5 Live
*1st – Peter Vonder Haar, Film Threat
*1st – Ray Bennett, The Hollywood Reporter
*1st – Matt Cale, Ruthless Reviews 
*2nd – Dennis Harvey, Variety (magazine)|Variety
*3rd – Claudia Puig, USA Today
*3rd – Mick LaSalle, San Francisco Chronicle
*4th – Tom Charity, CNN 
*4th – Jack Mathews, New York Daily News
*4th – Scott Tobias, The A.V. Club
*5th – Empire (magazine)|Empire magazine
*5th – Keith Phipps, The A.V. Club
*5th – Tasha Robinson, The A.V. Club
*6th – Scott Foundas, LA Weekly
*6th – Jonathan Rosenbaum, Chicago Reader
*7th – Sight & Sound magazine
*9th – Lisa Schwarzbaum, Entertainment Weekly
*9th – Nick Schager, Slant Magazine
*9th – Michael Phillips, Chicago Tribune
*10th – J. Hoberman, The Village Voice
}}

===Accolades===
The Assassination of Jesse James by the Coward Robert Ford was identified by the National Board of Review of Motion Pictures as one of the top 10 films of 2007. The board also named Casey Affleck as Best Supporting Actor in the film.  The San Francisco Film Critics Circle named The Assassination of Jesse James by the Coward Robert Ford as the Best Picture of 2007. The circle also awarded Affleck as best supporting actor for the film. Affleck was nominated for Best Performance by an Actor in a Supporting Role in a Motion Picture for the 65th Golden Globe Awards.   
 Best Supporting Best Cinematography. Warren Ellis for their music in the film (see below).

The film also holds a place on Empire (magazine)|Empires recent list of The 500 Greatest Films of All Time, coming in at #396. 

{| class="wikitable" style="font-size:100%;" ;"
|- style="text-align:center;"
! style="background:#cce;"| Award
! style="background:#cce;"| Category
! style="background:#cce;"| Recipients and nominees
! style="background:#cce;"| Outcome
|- Academy Awards Best Performance by an Actor in a Supporting Role
| Casey Affleck
|  
|- Best Cinematography
| Roger Deakins
|  
|- American Society of Cinematographers (ASC)
| Outstanding Achievement in Cinematography in Theatrical Releases
| Roger Deakins
|  
|- Broadcast Film Critics (BFCA)
| Best Supporting Actor
| Casey Affleck
|  
|- Chicago Film Critics
| Best Cinematography
| Roger Deakins
|  
|-
| Best Original Score Warren Ellis
|  
|-
| Best Supporting Actor
| Casey Affleck
|  
|-
|rowspan="3"| Dallas-Fort Worth Film Critics Association Awards 2007|Dallas-Fort Worth Film Critics
| Top Ten Films of the Year
| -
|  
|-
| Best Cinematography
| Roger Deakins
|  
|-
| Best Supporting Actor
| Casey Affleck
|  
|- Detroit Film Critics
| Best Supporting Actor
| Casey Affleck
|  
|- Empire Awards
| Best Film
| -
|  
|-
| Film Critics Circle of Australia Awards
| Best Foreign Film - English Language
| Andrew Dominik
|  
|- Florida Film Critics Circle
| Best Cinematography
| Roger Deakins
|  
|- Golden Globes Best Performance by an Actor in a Supporting Role
| Casey Affleck
|  
|- Golden Reel Awards
| Best Sound Editing - Music in a Feature Film
| Gerard McCann William B. Kaplan Jonathan Karp
|  
|-
|rowspan="2"| Golden Trailer Awards
| Best Drama Poster
| -
|  
|-
| Best Voice Over
| -
|  
|- Houston Film Critics
| Best Cinematography
| Roger Deakins
|  
|-
|rowspan="4"| International Cinephile Society
| Top Ten Films of the Year
| -
|  
|-
| Best Supporting Actor
| Casey Affleck
|  
|-
| Best Cinematography
| Roger Deakins
|  
|-
| Best Original Score
| Nick Cave Warren Ellis
|  
|-
| rowspan="2"| Italian Online Movie Awards
| Best Cinematography
| -
|  
|-
| Best Actor in a Supporting Role
| Brad Pitt
|  
|-
| Las Vegas Film Critics
| Top Ten Films of the Year
| -
|  
|- London Film Critics
| Actor of the Year
| Casey Affleck
|  
|-
| Film of the Year
| -
|  
|- National Board of Review
| Top Ten Films of the Year
| -
|  
|-
| Best Supporting Actor
| Casey Affleck
|  
|- National Society of Film Critics
| Best Supporting Actor
| Casey Affleck
|  
|- Online Film Critics Society
| Best Cinematography
| Roger Deakins
|  
|-
| Best Score
| Nick Cave Warren Ellis
|  
|-
| Best Supporting Actor
| Casey Affleck
|  
|- San Francisco Film Critics
| Best Picture
| -
|  
|-
| Best Supporting Actor
| Casey Affleck
|  
|- Satellite Awards
| Best Supporting Actor
| Casey Affleck
|  
|-
| Best Art Direction and Production Design
| Patricia Norris Martin Gendron Troy Sizemore
|  
|-
| Best Cinematography
| Roger Deakins
|  
|-
| Best Score
| Nick Cave
|  
|- Screen Actors Guild Awards
| Outstanding Performance by a Male Actor in a Supporting Role
| Casey Affleck
|  
|- Southeastern Film Critics
| Top Ten Films of the Year
| -
|  
|-
|rowspan="4"| St. Louis Gateway Film Critics Association Awards 2007|St. Louis Gateway Film Critics
| Best Picture
| -
|  
|-
| Best Supporting Actor
| Casey Affleck
|  
|-
| Best Cinematography
| Roger Deakins
|  
|-
| Best Score
| Nick Cave Warren Ellis
|  
|-
|rowspan="2"| Utah Film Critics Association
| Top Ten Films of the Year
| -
|  
|-
| Best Actor
| Casey Affleck
|  
|- Vancouver Film Critics
| Best Supporting Actor
| Casey Affleck
|  
|-
|rowspan="2"| Venice Film Festival
| Golden Lion
| Andrew Dominik
|  
|- Volpi Cup for Best Actor
| Brad Pitt
|  
|-
| Western Writers of America
| Best Western Drama
| Andrew Dominik
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 