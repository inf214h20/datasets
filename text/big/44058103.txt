Avakaasham
{{Infobox film
| name = Avakaasham
| image =
| caption =
| director = AB Raj
| producer =
| writer = AB Raj
| screenplay = AB Raj
| starring = Jayabharathi Jose Prakash Sankaradi Alummoodan
| music = M. K. Arjunan
| cinematography = RS Pathi
| editing = MS Mani
| studio = Chandra Banu Productions
| distributor = Chandra Banu Productions
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed AB Raj. The film stars Jayabharathi, Jose Prakash, Sankaradi and Alummoodan in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Jayabharathi 
*Jose Prakash 
*Sankaradi 
*Alummoodan 
*MG Soman  Vincent

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ente Swapnathin Maalikayil || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 