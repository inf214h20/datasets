Monster Trucks (film)
{{Infobox film
| name           = Monster Trucks
| image          = 
| caption        = 
| director       = Chris Wedge
| producer       = Mary Parent Jonathan Aibel Glenn Berger Derek Connolly
| starring       = Jane Levy Lucas Till Amy Ryan Rob Lowe Danny Glover Barry Pepper Holt McCallany
| music          = Dave Sardy
| cinematography = 
| editing        = Conrad Buff IV
| studio         = Paramount Animation Disruption Entertainment  
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $100 million
| gross          = 
}}
 action film Jonathan Aibel, Glenn Berger, and Derek Connolly.  The film stars Jane Levy, Lucas Till, Amy Ryan, Rob Lowe, Danny Glover, Barry Pepper and Holt McCallany.

Principal photography of the film began on May 13, 2014 in Chilliwack. The film is scheduled to be released on December 25, 2015, by Paramount Pictures.

== Cast ==
*Jane Levy 
*Lucas Till 
*Amy Ryan 
*Holt McCallany 
*Rob Lowe 
*Danny Glover 
*Frank Whaley 
*Barry Pepper  Thomas Lennon 
*Tucker Albrizzi 
*Adrian Formosa
*Chad Willett

== Production == Thomas Lennon joined the cast.    On April 14, Barry Pepper joined the cast.    On April 24, Tucker Albrizzi who starred in Big Time Rush joined the cast.    Rob Lowe joined the cast five days later.    

In December 2013 it was announced that the films production was set to begin in early April 2014 in Vancouver,  and it would wrap up filming in mid-July, the studio Vancouver Film Studios was booked for the production.    Principal photography began on April 4, 2014, in Kamloops, British Columbia.  Filming was spotted on May 13, 2014 in downtown Chilliwack, Canada.  

== Release ==
The release date was previously set for May 29, 2015, but on January 26, 2015, the film was pushed back to December 25, 2015, which was first assigned for  . 

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 