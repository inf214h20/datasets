Civilisées
{{Infobox film
| name = Civilisées (A Civilized People)
| image = 
| caption = 
| writer = Randa Chahal Sabag
| starring = 
| director = Randa Chahal Sabag
| producer = Jean-Pierre Saire
| cinematography  = 
| editing = Juliette Weifling
| distributor  = 
| released  =  
| runtime = 95 minutes French
| country = Lebanon
| music = Ziad Rahbani
| budget = 
| gross = 
}}
 Lebanese film maker and screen writer.

==Critical Response==
Due to the controversial topic of the film, it was only screened once in Lebanon at the Beirut International Film Festival and was later banned by the Lebanese film commission. However, it received positive responses from international audiences.
The film was well appreciated by the Arab Film Festival which took place in San Francisco, CA
It also received positive responses at film festivals in Italy, France and Canada.

==Synopsis==
The film is a dark comedy/drama about the Lebanese civil war. During the civil war, many well-off Lebanese families fled the country to look for their personal interests internationally, leaving behind their residences under the care of maids and laborers from Egypt, the Philippines and Sri Lanka. The film draws the portrait of a war-tormented Beirut neighborhood, and the love between a Muslim militia fighter and a Christian maid.

==Cast==
* Jalila Baccar as Viviane
* Tamim Chahal as Samir
* Myrna Maakaron as Souad
* Carmen Lebbos as Najat
* Bruno Todeschini as Antoine

==Crew==
*Executive Producers : Daniel Toscan du Plantier, Frédéric Sichler
*Co-producer : Anne Fleischl
*Directors of Photography : Ricardo Jacques, Gale Breidi, Roby Breidi
*Sound Recordists : Nicolas Cantin, Dominique Gaborieau
*Press Attachés (film) : Denise Breton, Isabelle Duvoisin
*Production Designer : Sylvian Chauvelot
*Costume Designer : Souraya Bagdadi

==References==
*http://www.arteeast.org/pages/cinema/series/TheCalmAfterTheStorm/935/
*http://www.answers.com/topic/civilis-es
*http://www.unifrance.org/film/16045/civilisees
*http://www.arabfilmfestival.org/film_detail.php?id=885

 
 
 
 
 
 
 
 


 
 