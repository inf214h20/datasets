Fright Night (1947 film)
{{Infobox film|
  | name           = Fright Night |
  | image          = Frightnight47.jpg|
  | caption        = |
  | director       = Edward Bernds
  | writer         = Clyde Bruckman| Harold Brauer Cy Schindell Heinie Conklin Sammy Stein Stanley Blystone Dave Harper Tom Kingston|
  | cinematography = Philip Tannura | 
  | editing        = Paul Borofsky |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       = March 6, 1947 |
  | runtime        = 17 41"
  | country        = United States
  | language       = English
}}

Fright Night is the 98th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are managers of a beefy boxer named Chopper Kane (Dick Wessel), and they bet their bank roll on his next fight. When a gangster (Tiny Brauer) tells them to have Chopper lose (as he has a lot of money bet on his opponent Gorilla Watson) or they will lose their lives, the boys decide to play along. They try to soften Chopper up by feeding him rich food and having him spend time with their friend Kitty (Claire Carleton). The fight gets canceled when Kitty dumps Chopper for Gorilla and, in a fluke accident, Gorilla gets entangled with Moe and breaks his hand against a wall. The Stooges think they have put one over on the gangsters, only to have the bad guys corner them in a deserted warehouse. Instead of being rubbed out, the boys capture the crooks and get a reward.
 ) in Fright Night]]

==Production notes==
Fright Night marked the return of Shemp Howard to the Stooges, who had last performed with the act 17 years prior. Shemp agreed to rejoin the act until brother Curly Howard recovered enough to return to the Stooges (Curly never did). 

Fright Night was reworked in 1955 as Fling in the Ring, using ample stock footage. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion, p. 291; Comedy III Productions, Inc., ISBN 0-9711868-0-4  It was Shemps favorite Stooge film. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 