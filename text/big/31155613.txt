Sahadharmini
{{Infobox film 
| name           = Sahadharmini
| image          =Sahadharmini.jpg
| caption        =
| director       = PA Thomas
| producer       = PA Thomas
| writer         = PA Thomas S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan Sathyan Kaviyoor Hari
| music          = BA Chidambaranath
| cinematography = PB Maniyam
| editing        = TR Sreenivasalu
| studio         = Thomas Pictures
| distributor    = Thomas Pictures
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, Hari in lead roles. The film had musical score by BA Chidambaranath.   

==Cast== Sathyan
*Kaviyoor Ponnamma
*Adoor Bhasi Hari
*Muthukulam Raghavan Pillai
*O Ramdas
*T. R. Omana
*Shaji
*Pankajavalli
*Ushakumari

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aalolam || S Janaki, P. Leela || Vayalar Ramavarma || 
|-
| 2 || Bhoomikku Neeyoru Bhaaram || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Chaanchakkam || S Janaki || Vayalar Ramavarma || 
|-
| 4 || Himagiri || P. Leela || Vayalar Ramavarma || 
|-
| 5 || Naanichu Naanichu || B Vasantha || Vayalar Ramavarma || 
|-
| 6 || Paarijaathamalare || B Vasantha || Vayalar Ramavarma || 
|-
| 7 || Shilpikale Shilpikale || B Vasantha || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 