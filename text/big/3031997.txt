Big Eden
{{Infobox film
| name           = Big Eden
| image          = Big-eden.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Thomas Bezucha
| producer       = Jennifer Chaiken
| writer         = Thomas Bezucha
| starring       = Arye Gross Eric Schweig Tim DeKay Louise Fletcher Corinne Bohrer
| music          = Joseph Conlan
| cinematography = Rob Sweeney
| editing        = Andrew London
| studio         = Chaiken Films
| distributor    = Wolfe Video
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $512,451 
}} romantic Comedy-drama|comedy-drama film written and directed by Thomas Bezucha. It won awards from several gay and lesbian film festivals , and was nominated for best limited release film at the GLAAD Media Awards in 2002. Except for the opening sequence, this motion picture was entirely shot in Montana.

==Plot summary== Native American owner of the towns general store.

==Plot==
In August 2000, Henry Hart, a gay, successful artist living in New York, receives a call from his old friend Grace Cornwell, a kindergarten teacher in his hometown, who tells him that his grandfather Sam suffered a stroke. Although his assistant, Mary Bishop, wants him to stay, Henry feels himself bound to visit and help his ailing grandfather. He jumps on the next plane to his hometown, Big Eden Montana, giving up his new home and career. Stranded in his place of birth Henry is confronted by the changes of time. Though Sam is becoming better, Henry has the feeling that he should stay with his helpless grandfather because he himself fears becoming an orphan. While accompanying Sam to church every Sunday he involuntarily becomes part of the town life and gossip again. The town-folks somehow always knew about his sexuality, but never mentioned it publicly. Further complicating the situation is the presence of his former high-school crush Dean Stewart, who moved back in town a week earlier.  Dean has just split up with from his wife and has returned to Big Eden with his kids Ben and Andrew.  This leaves Henry trying to work out his unresolved feelings for Dean.

Grace set up a support system for both Henry and Sam. Included is widow Thayer and Pike Dexter.  Pike is the town‘s general-store owner, and the Widow Thayer is center of gossip and society in Big Eden.  She attempts several times to hook up Henry with different people, first women, but after a few "social gatherings“ she realizes her error, and invites men instead. While all this is going on the Widow Thayer cooks for grandfather and grandson daily, and Pike takes it over to their house and helps setting the table. After a few weeks Pike realizes that the food is inappropriate, and learns how to cook healthy dishes.  He keeps this secret, neither telling the Harts nor Thayer, exchanging Thayers dinner with his own delicious meals. He also orders the special supplies Henry orders so he can continue painting up in Big Eden.  Pike, a very shy Native American, who wants to "have things nice for Henry“,  has obviously fallen in love with him too.

Meanwhile Dean is around Henry a lot, helping him build a ramp for Sam‘s wheelchair, and taking Henry dancing and to the mountains. With all of his efforts he tries to show Henry his affection and feelings, but Dean eventually tells Henry that he couldn‘t do it, meaning by that he cannot live together with Henry.

Time passes and Sam becomes worse. One night Henry arrives at home and finds Sam dead in his bedroom. The town falls into mourning at Sam‘s death. He literally built every house in town, and was closely connected to each and every one. Though Dean comforts Henry, Pike does the opposite by secluding himself. A funeral is held for Sam, where everyone shows up except for Pike.

Henry, now completely alone, realizes that Pike meant something to him. Pike had shared a "promised dinner together“ with him one night when he fascinated Henry by his knowledge of stars and mystical stories. That upsets Henry even more because he thought Sam meant something to Pike too. They both don‘t talk until the day Henry is leaving for New York. In the very last minute Pike accepts his love for Henry and tries to catch him at the airport but he is too late. On his way home, Pike sees Sam‘s truck in front of his store, not expecting Henry to be waiting for him.

==Cast & roles==
{| class="wikitable" Cast
!colspan="3"|Role
|- Arye Gross Henry Hart
|colspan="5"|Protagonist; well-known artist from New York who returns to his backland hometown in Montana. Confused about his feelings.
|- Eric Schweig Pike Dexter very quiet Native American who owns the local general store; has a crush on Henry;
|- Tim DeKay Dean Stewart former best friend of Henrys; still Henrys crush, moved back to town.
|- Louise Fletcher Grace Cornwell old friend of family Hart; helps Henry out where she can.
|- Corinne Bohrer Anna Rudolph
|- George Coe Sam Hart
|colspan="5"|Henrys ailing grandfather, only reason why he is back in town
|- Nan Martin Widow Thayer Local busybody, although she means well
|-
|rowspan="1"|ONeal Compton Jim Soams
|colspan="5"|
|- Christopher Kendra Bird
|colspan="5"|
|- Veanne Cox Mary Margaret Bishop
|colspan="5"|
|- Cody Wayne Meixner Ben Stewart
|colspan="5"|
|-
|}

==Production== Glacier National West Glacier, and the Big Eden houses are on the shore of Lake McDonald. Pikes general store is a building located in Swan Lake, Montana.

==Reception==
Big Eden received mixed to positive reviews, currently holding a 65% "fresh" rating on  , which uses an average of critics reviews, the film holds a 59/100 rating, indicating "mixed or average reviews". 
IMDbs rating currently is set on 7.1/10. 

David Ehrenstein from   commented “Director Bezucha’s eyes are as starry as Montana’s sky, but it’s pretty hard to resist such a determinedly utopian vision of love.”  Carla Meyer from San Francisco Chronicle wrote “It’s unlikely that the whole cowboy town would really applaud all the queer goings-on, but it’s a lovely sentiment in a lovely movie.” 

===Awards===
The film received awards and nominations from a number of independent   in 2002. 
* 2000  
* 2000 San Francisco International Lesbian & Gay Film Festival: Audience Award - Best Feature
* 2000 Seattle Lesbian & Gay Film Festival: Audience Award - Favorite Narrative Feature
* 2000 Cleveland International Film Festival: Best American Independent Feature Film; Best Film 	
* 2001 Florida Film Festival: Audience Award - Best Narrative Feature
* 2001 Miami Gay and Lesbian Film Festival: Jury Award - Best Fiction Feature
* 2001 Toronto Inside Out Lesbian and Gay Film and Video Festival: Audience Award - Best Feature Film or Video

==Soundtrack==
The film combines both classic and contemporary country songs, though there was no commercially released soundtrack. Tracks featured in the film are:
{| class="wikitable" Soundtrack
|-
|rowspan="1"| George Jones 
|colspan="2"|"Dont Let the Stars Get in Your Eyes" (from the 1964 United Artists album The Race Is On)
|- Dwight Yoakam
|colspan="2"|"A Thousand Miles from Nowhere" (1993 Reprise/Warner Bros.)
|- Buck Owens
|colspan="2"|"Together Again" (1965 Capitol)
|- Jim Reeves
|colspan="2"|"Welcome to My World" (1963 RCA Victor)
|- Skeeter Davis
|colspan="2"|"Optimistic" (1961 RCA Victor)
|- Lucinda Williams
|colspan="2"|"Something About What Happens When We Talk" (from her 1993 album Sweet Old World)
|- George Jones
|colspan="2"|"Achin Breakin Heart" (1962 Mercury)
|- David Allan Coe
|colspan="2"|"A Sad Country Song" (1974 Epic/CBS)
|- Lari White
|colspan="2"|"Wishes" (from her 1994 RCA album "Wishes)
|- Dwight Yoakam and Patty Loveless
|colspan="2"|"Send a Message to My Heart" (1992 Reprise/Warner Bros.) 
|}

In addition to these tracks that are or were commercially available, the film features two performances by a group called Railroad Earth. Actress Louise Fletcher also performs Take Me in Your Arms and Hold Me which was originally recorded by Eddy Arnold in 1949. The Film score|films original score was composed by Joseph Conlan.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 