Randy Parsons: American Luthier
{{Infobox film
| name           = Randy Parsons: American Luthier
| image          = AmericanLuthierPoster.png
| alt            = 
| caption        = 
| director       = David Aldrich
| producer       = David Aldrich
| writer         = 
| narrator       = 
| starring       = Randy Parsons
| music          =  David Aldrich Timothy Batzel
| editing        = David Aldrich
| studio         = 
| distributor    = 
| released       =  
| runtime        = 8 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Jack White, Jimmy Page, Sammy Hagar, Death Cab for Cutie, Peter Frampton, Joe Perry, and Modest Mouse.

==Synopsis==
American Luthier focuses on Randy Parsons’ transformation from aspiring musician to guitar-maker. The guitar had been Parsons’ identity since he was a child, but after studying classical and jazz guitar in college, he realized that he would never make a living as a musician.  So he gave it up — he didn’t even own a guitar in his mid-twenties — and then one day he had a vision of how the guitar would come back into his life. Now Parsons is creating instruments that are highly sought after works of art for clients like Jack White, Jimmy Page and Joe Perry. This is a film is about someone who gave up their passion for playing guitars and discovered that they had a passion for making guitars. Parsons’ success is a testament to doing what we love in life and work, even if it requires taking a second look at your dreams, and finding a different way to be a rockstar.

==Production== David Aldrich. Timothy Batzel is also credited with cinematography. The music in scene four is Michael Chorney | Michael Chorneys "Shabaz", found on his album Oom Pah Of The Ghost Parade. The films original score is by David Aldrich, and Randy Parsons.

==Awards==
The film won Best Documentary Film at the 2011 ITSA Film Festival,  Best Cinematography and an Award of Excellence at the 2012 LA Movie Awards,  and has been an official selection for more than thirty-five film festivals. The film made its broadcast debut on Public Television as part of Reel NW | Season Four. It subsequently has been nominated for an Emmy Award. 

==References==
 

==External links==
*  
*  

 
 
 
 
 