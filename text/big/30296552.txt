Another Earth
 
{{Infobox film
| name           = Another Earth
| image          = Poster of the movie Another Earth.jpg
| caption        =
| alt            = A woman standing by the water, in the night sky Earth can be seen.  Mike Cahill
| producer       = {{Plainlist| 
* Hunter Gray 
* Mike Cahill 
* Brit Marling 
* Nicholas Shumaker
}}
| writer         = Mike Cahill Brit Marling
| screenplay     =
| story          =
| based on       =  
| starring       = {{Plainlist| 
* Brit Marling 
* William Mapother
}}
| music          = Fall On Your Sword
| cinematography = Mike Cahill
| editing        = Mike Cahill
| studio         = Artists Public Domain
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $100,000    
| gross          = $1.8 million
}}
 independent science Mike Cahill. The film stars William Mapother and Brit Marling. It premiered at the 27th Sundance Film Festival in January 2011 and was distributed by Fox Searchlight Pictures.
 performance and for Cahill and Marlings Saturn Award for Best Writing|writing.

==Plot==

Rhoda Williams (Brit Marling), a brilliant 17-year-old girl who has spent her young life fascinated by astronomy, is delighted to learn that she has been accepted into MIT. In a reckless celebratory moment, she drinks with friends and drives home intoxicated. Listening to a story on the radio about a recently discovered Earth-like planet, she gazes out her car window at the stars and inadvertently hits a stopped car at an intersection, putting John Burroughs (William Mapother) in a coma and killing his wife and son. Rhoda is a minor, so her identity is not revealed to John. After serving her juvenile prison sentence, and after four years of isolation, Rhoda continues to shield herself from the world outside, becoming a janitor at a local school, wanting to work "physically," almost as a means to struggle past the potential she has squandered with the decision of a single night.

After cleaning her former high school for a while and hearing more news stories about the mirror Earth, Rhoda visits Johns house after he has recovered, thinking she will apologize for the harm she did to him. He answers the door and she loses her nerve. Instead, she pretends to be a maid offering a free day of cleaning as a marketing tool for Maid in Haven (a New Haven based maid service). John, who has dropped out of his Yale music faculty position and is now living in a depressed and dirty stupor, agrees to Rhodas offer. When she finishes, John, who still does not know she is the person who killed his wife and son, asks her to come back next week. Rhoda tells him someone will come, but it may not be her.

Rhoda returns to clean and develops a caring relationship with John that eventually becomes more significant and romantic. They like each other and are intelligent and compatible conversationally. Rhoda genuinely wants to be of service to him.

Rhoda enters an essay contest sponsored by a millionaire entrepreneur who is offering a civilian space flight to the mirror Earth. Rhodas essay is selected, and she is chosen to be one of the first explorers to travel to the other Earth. Rhoda tells John she has won the space flight, but he asks her not to go. He tells her that they might have something together. Faced with this, she finally decides to confront him with the truth and tells him that she was the one who killed his wife and son. He struggles to accept this information and becomes upset. He forcibly removes her from his house.

Rhoda hears a scientist postulating in a telecast that the citizens of the mirror Earth might be identical to those on her Earth in every way until the moment they learned of the others existence. From that point on, the identical people on the different Earths probably began to deviate in small ways, changing their actions. Hearing this, Rhoda realizes that her identical self on the other Earth may not have caused the accident.

Excited, Rhoda rushes back to Johns house, but he is still in shock and will not listen. She breaks into the house, and  unable to talk to John properly, leaves him the ticket to the other Earth, telling him enough information to give him a small hope that his wife and son might be alive on that planet. John accepts the gift and becomes one of the first civilian space travelers to the other Earth.

Four months later, Rhoda looks up at the sky where Earth 2 is now hidden in fog. She approaches her back door and sees her other self from Earth 2 standing in front of her.

==Cast==
* Brit Marling as Rhoda Williams
* William Mapother as John Burroughs
* Jordan Baker as Kim Williams
* Robin Lord Taylor as Jeff Williams
* Flint Beverage as Robert Williams
* Kumar Pallana as Purdeep
* Richard Berendzen as himself (narrator)  

==Production==

The idea behind Another Earth first developed out of director Mike Cahill and actress Brit Marling speculating as to what it would be like were one to encounter ones own self. In order to explore the possibility on a large scale, they devised the concept of a duplicate Earth. The visual representation of the duplicate planet was deliberately made to evoke the Moon, as Cahill was deeply inspired by the 1969 Apollo 11 lunar landing.    This movie shares some of its plot details with the 1969 British sci-fi movie Doppelgänger (1969 film)|Doppelganger.
 The Tenth Man.

   Union Station – so that he could avail himself of the services of local friends and family and thus reduce expenses. His childhood home was used as Rhodas home and his bedroom as Rhodas room. The scene of the car collision was made possible through the help of a local police officer with whom Cahill was acquainted, who cordoned off part of a highway late one night. The scene in which Rhoda leaves the prison facility was filmed by having Marling walk into an actual prison posing as a yoga instructor and then exiting. 

  
According to Brit Marling, she approached William Mapother for the role of John after "being haunted" by his performance in In the Bedroom (2001). Mapother consented to work on Another Earth for $100 a day.  When asked why he agreed to join the cast, considering the "notoriously hit or miss" nature of independent films, Mapother replied that he was drawn by the films subject and by the names involved in the project. At Mapothers insistence, he and the production team worked extensively on the scenes of John and Rhoda in order to develop Johns character in the film. 

The film ignores the physical consequences of having a similar-sized planet and moon appear nearby (i.e. effect on tides, gravity and atmosphere) other than depicting night time as brighter due to the reflection of the Suns light off the other planet. The DVD / Blu-ray deleted scenes feature reveals that the film makers did intend to illustrate some of the consequences by filming a scene in which Rhoda encounters flowers floating in mid-air, but the scene was cut from the final film.

==Music==
 
The musical score was composed by Fall on Your Sword, with the exception of the song played in the musical saw scene, composed by Scott Munson and performed by Natalia Paruz.  Mike Cahill came upon Paruz, known also as the "Saw Lady", while riding the subway in New York. Mesmerized by her playing, he obtained her contact information and arranged for her to coach William Mapother on how to hold and act as if playing the saw for the scene in the film.   In the music score there is an instrument for each character. Rhoda is the cello and John is the piano. In the love scene both instruments are heard, however not fully in synch, since they are not really a full match in real life. 

==Release==

 

Another Earth had its world premiere at the   won distribution rights to the film in a deal worth   to  , beating out other distributors including Focus Features and the Weinstein Company.   
 United States, Canada, and other English-speaking territories.  The film had a limited release in the United States and Canada on  , 2011, expanding to a wide release in ensuing months. 

==Reception==

===Box office===
In its first week in theaters, it grossed $112,266.  Eventually, the film grossed $1,776,935 worldwide.

===Critical response===
Review aggregation website Rotten Tomatoes gave Another Earth a rating of 63% based on reviews from 163 critics. 

Film critic Roger Ebert of the Chicago Sun-Times gave the film three and a half stars out of four. 

===Accolades===
Another Earth won the Alfred P. Sloan Prize at the 2011 Sundance Film Festival, for "focusing on science or technology as a theme, or depicting a scientist, engineer or mathematician as a major character."  It went on to earn the Audience Award in the category of Narrative Feature at the 2011 Maui Film Festival.  

Another Earth was named one of the top 10 independent films of the year at the 2011 National Board of Review Awards and was nominated for a Georgia Film Critics Association Award for Best Picture.

==See also==
* Counter-Earth hypothesis—a presocratic hypothesis that posited a duplicate of Earth exists opposite of its orbit  in order to balance the otherwise unstable geocentric orbit of the planets.
* Journey to the Far Side of the Sun— The first science-fiction film produced (1969 film) that dealt with a similar double Earth premise. It was initially called Doppelganger.
* Melancholia (2011 film)|Melancholia—Another well-praised 2011 film depicting an encounter between Earth and a previously undiscovered planet.
* Solaris (1972 film)|Solaris—"Another Earth is as thought-provoking, in a less profound way, as Andrei Tarkovsky|Tarkovskys Solaris (1972 film)|Solaris, another film about a sort of parallel Earth," according to Roger Eberts review.   Seems Tarkovskys 1972 film of "Solaris (1972 film)|Solaris" may be about taking man from the earth, but not earth from the man.  The Quiet Earth—The scene in which Zac Hobson discovers a planet (similar to Saturn) rising out of the Ocean.
* Twin Earth thought experiment—Hilary Putnams classic thought experiment arguing in favor of semantic externalism.

==References==
 

==External links==
*  
*  
*  
*  
*   – composer of the musical saw track
*  

 
 
{{succession box Alfred P. Sloan Prize Winner
| years=2011
| before=Obselidia
| after=Robot & Frank}}
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 