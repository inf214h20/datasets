The Fifth Day of Peace
 
 
{{Infobox film
| name           = The Fifth Day of Peace
| image          = 5th day of peace DVD cover.jpg
| caption        = English version DVD cover
| director       = Giuliano Montaldo
| writers        = Andreas Barbato Lucio Battistrada Ottavio Jemma Giuliano Montaldo Richard Johnson Franco Nero Michael Goodliffe Bud Spencer   Helmuth Schneider  
| music          = Ennio Morricone
| studio         = Clesi Cinematografica Jadran Film
| distributor    = USA Scotia International France Les Films Jacques Leitienne 
| released       =  
| runtime        = English 103 minutes Italian 110 minutes
| country        = Italy and Yugoslavia
| languages      = English, French, German, Italian
}} 1969 about the 13 May 1945 German deserter execution in a Canadian Forces|Canadian-run POW camp in Amsterdam.

==Synopsis==
Two German deserters, Ensign Bruno Grauber (Franco Nero) and Corporal Reiner Schultz (Larry Aubrey) are captured by the Canadians at the end of World War II. They are interned in a Canadian-run POW camp where the senior German officer, Colonel Von Bleicher, is a career officer.  Their fellow German prisoners of war, led by Von Bleicher, discover that they are deserters. They are put through a formal military court martial organised by Von Bleicher and charged with cowardice. They are sentenced to death and are to be executed on the "fifth day of peace".  Von Bleicher pressures the Canadian camp commandant to allow the execution to be carried out and requests rifles and ammunition to carry out the sentence.

A Canadian general, Snow, persuades (but does not order) the Canadian camp commandant, Captain Miller, to allow the execution to be carried out for the higher purpose of preserving military discipline. He also motivates Miller with a promotion to major.

This movie is based on the true story of two German sailors, leading seaman Bruno Dorfer and machinists mate Rainer Beck, both executed for desertion on May 13, 1945, after being found guilty of cowardice by fellow POWs.  The sentence was carried out by German POWs who were under Canadian command.

==Casting==
* Bud Spencer as Cpl. Jelinek Richard Johnson as Capt. John Miller
* Franco Nero as Ens. Bruno Grauber
* Larry Aubrey as Cpl. Reiner Schultz
* Helmuth Schneider as Col. von Bleicher (as Helmut Schneider)
* Michael Goodliffe as Gen. Snow Relja Basic as Lt. George Romney
* Emilio Delle Piane as Gleason
* Enrico Osterman as Trevor
* Renato Romano as Canadian Doctor
* Sven Lasta as German Officer
* Demeter Bitenc German Major

== External links ==
*  
*  

 
 
 
 
 
 
 


 