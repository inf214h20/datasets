All My Life (2008 film)
{{Infobox film
| name           = All My Life
| image          = Toul omry.jpg
| caption        = theatrical poster
| director       = Maher Sabry
| producer       = Maher Sabry
| writer         = Maher Sabry
| starring       = Mazen Nassar  Jawa  Ayman  Julian Gonzalez Esparza  Mehammed Amadeus  Christopher White  Yousef El Shareif  Travis Creston  Munir Bayyari
| music          = Ilyas Iliya
| cinematography = Maher Mostafa
| editing        = Maher Sabry
| studio         = Maraia Film Egyptian Underground Film Society
| distributor    = Les Films de lAnge (France)
| released       =  
| runtime        = 120 minutes
| country        = Egypt
| language       = Arabic
| budget         =
| gross          =
}}
All My Life ( ; Transliteration|translit. Toul Omry;  ), is a 2008 Egyptian film by Maher Sabry. It is noted as being the first film to handle the subject of male homosexuality and the status of homosexuals in Egypt.  While a work of fiction, Sabry made efforts to use real-life influences from his own experiences to the 2001 arrests of the Cairo 52 to keep the portrayal of conditions for homosexuals in Egypt accurate.

==Plot==
 
The films plot revolves around the life of Rami, a 26-year-old homosexual, his life in Cairo, and his experiences with his friends and neighbors.

==Cast and characters==
*Mazen Nassar as Rami - a gay dance student in Cairo
*Ayman as  Walid - Ramis lover who leaves him to marry a woman
*Jwana as Dalia - Ramis friend, a student who plans to study abroad to escape the conservative atmosphere in Egypt
*Louay as Kareem - Ramis friend, a doctor active in the underground gay scene
*Julian Gonzalez Esparza as Ahmad - Ramis neighbor, a devout Muslim man with an inconvenient passion for women
*Mehammed Amadeus as Mina - Ramis teenage neighbor who lives a closeted life under his strict Christian mothers roof
*Maged as Atef - a poor waiter who becomes a love interest to Rami

Other actors

*Janaan Attia ... Nurse Latifa
*Munir Bayyari ... Hany
*Monica Berini ... Office Worker
*Travis Creston ...  Tourist
*Habeeb El-Deb ...  Prosecutor
*Youssef El-Shareif ...  Ashraf
*Sarah Enany ...  Nurse Safaa / Opera Singer
*Hala Fauzi ...  Belly Dancer
*Bassam Kassab ...  Hatem
*Ayman Kozman ...  Policeman
*Nabila Mango ...  Minas mother
*Jamal Mavrikios ...  Mazens colleague
*Amar Puri ...  Amar
*Mykha Ram ...  Mostafa
*Ashraf Sewailam ...  Rami (voice)
*Wedad ...  Khadra
*Christopher White ...  Mark
*Hesham El-Tahawi ...  TV actor one
*Naglaa Younis ...  TV actress two
*Seham Saneya Adelsalam ...  TV actress three

==Release==
The film premiered at Frameline in San Francisco in June 2008. 

==Reactions==
While previously in Egyptian film, gay characters had visbility in general in minor roles, as in The Bathhouse of Malatily (1973), Alexandria … Why? (1978), Mendiants et Orgueilleux (1991), Marcides (1993) and, recently, The Yacoubian Building (film)|The Yacoubian Building (2007), All My Life was the first all-gay film to be released. 
 New York, Athens, Melbourne, Sydney, Bangalore and Ljubljana.
 illusion of safety which, in turn, leads to the spread of the disease."

Regarding responses of conservative Muslim figures, Maher Sabry said "I’m not surprised that this happened. It was expected, yet it’s still painful to me, because it’s an indication of just how backward we’ve become. We’re now living in an age of cultural regression, an age where dissidents, presidential candidates and religious minorities are thrown into jail. We claim to be emulating Islamic civilization; but if the people who built that civilization were alive today, there would have been fatwas pronounced against them, and their books and other works would have been burned."  

===Awards=== Narrative Feature FACE à FACE film festival.   The festival is based in St Etienne, France, with the mission to promote positive attitudes towards homosexuality through art and Culture.

==See also==
 
* Cinema of Egypt
* Pleasure and Suffering

==References==
 

==Further reading==
* Mustapha, Tariq (طارق مصطفي). " ." ( ) Rose el Yousef. - Article about the film

==External links==
*  
*  

 
 
 
 
 
 
 