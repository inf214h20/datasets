The Palace of Angels
 
{{Infobox Film
| name           = The Palace of Angels
| image          = O Palácio dos Anjos.jpg
| caption        = Theatrical release poster
| director       = Walter Hugo Khouri
| producer       = Walter Hugo Khouri William Khouri
| writer         = Walter Hugo Khouri
| starring       = Geneviève Grad
| music          = Rogério Duprat Lanny Gordini Renato Mazola
| cinematography = Peter Overbeck
| editing        = Mauro Alice Vera Cruz Metro Goldwyn Mayer Les Films Number One 
| distributor    = Metro Goldwyn Mayer
| released       =   }}
| runtime        = 96 minutes
| country        = Brazil France
| language       = Portuguese
}}

The Palace of Angels ( ;  ) is a 1970 Brazilian-French drama film directed by Walter Hugo Khouri.  It was entered into the 1970 Cannes Film Festival.   

==Cast==
* Geneviève Grad as Bárbara
* Adriana Prieto as Ana Lúcia
* Rossana Ghessa as Mariazinha
* Luc Merenda as Ricardo
* Norma Bengell as Dorothy
* Joana Fomm as Rose John Herbert as Carlos Eduardo
* Alberto Ruschel as José Roberto
* Sérgio Hingst as Mr. Strauss
* Pedro Paulo Hatheyer as Dr. Luiz
* Zózimo Bulbul as Senegals ambassador

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 