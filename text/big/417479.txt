Day for Night (film)
 
{{Infobox film
| name           = Day for Night
| image          = La_Nuit_oscar.jpg
| caption        = Theatrical poster by Bill Gold
| director       = François Truffaut
| producer       = Marcel Berbert
| writer         = François Truffaut Suzanne Schiffman Jean-Louis Richard Dani Alexandra Stewart Jean-Pierre Aumont Jean Champion Jean-Pierre Léaud François Truffaut
| music          = Georges Delerue
| cinematography = Pierre-William Glenn
| editing        = Martine Barraquè-Curie, Yann Dedet
| studio         = Les Films du Carrosse PECF Produzione Internazionale Cinematografica
| distributor    = Columbia Pictures Warner Brothers
| released       =  
| runtime        = 115 minutes
| country        = France
| language       = French gross = 839,583 admissions (France) 
}} nuit américaine (American night) is a technical process whereby sequences filmed outdoors in daylight are shot using tungsten (artificial light) or infrared film stock and underexposed (or dimmed during post production) to appear as if they are taking place at night. In the English-speaking world the film is known as Day for Night, which is the equivalent English expression for the process.

==Plot==
La Nuit américaine chronicles the production of Je Vous Présente Paméla (Meet Pamela, also referred to as I want you to meet Pamela), a clichéd melodrama starring aging screen icon, Alexandre (Jean-Pierre Aumont), former diva Séverine (Valentina Cortese), young heart-throb Alphonse (Jean-Pierre Léaud) and a British actress, Julie Baker (Jacqueline Bisset) who is recovering from both a nervous breakdown and the controversy leading to her marriage with her much older doctor.

In between are several small vignettes chronicling the stories of the crew-members and the director; Ferrand (Truffaut himself) who tangles with the practical problems one deals with when making a movie. Behind the camera, the actors and crew go through several romances, affairs, break-ups, and sorrows. The production is especially shaken up when one of the secondary actresses is revealed to be pregnant. Later Alphonses fiancee leaves him for the films stuntman, which leads Alphonse into a palliative one-night stand with an accommodating Julie; whereupon, mistaking Julies pity sex for true love, the infantile Alphonse informs Julies husband of the affair. Finally, Alexandre dies on the way to hospital after a car accident.

==Cast==
* Jacqueline Bisset as Julie Baker
* Jean-Pierre Aumont as Alexandre
* Valentina Cortese as Severine
* Jean-Pierre Léaud as Alphonse Dani as Liliane
* François Truffaut as (Director) Ferrand
* Alexandra Stewart as Stacey
* Jean Champion as Bertrand
* Nathalie Baye as Joelle
* David Markham as Doctor Nelson
* Nike Arrighi as Odile
* Bernard Menez as Bernard, the Prop Man
* Zénaïde Rossi as Madame Lajoie
* Gaston Joly as Gaston
* Xavier Saint-Macary as Christian, Alexandres lover
* Jean Panisse as Arthur
* Maurice Séveno as the TV reporter
* Christophe Vesque as the boy in Ferrands dream
* Graham Greene as an Insurer (as Henry Graham)
*Marcel Berbert as an Insurer 

==Themes==
One of the films themes is whether or not films are more important than life for those who make them, its many allusions both to film-making and to movies themselves (perhaps unsurprising given that Truffaut began his career as a film critic who championed cinema as an art form). The film opens with a picture of  , Carl Theodor Dreyer, Ingmar Bergman, Alfred Hitchcock, Jean-Luc Godard, Ernst Lubitsch, Roberto Rossellini and Robert Bresson. The films title in French could be read as Lennui américain (American boredom): Truffaut wrote elsewhere  of the way French cinema critics inevitably make this pun of any title which uses nuit. Here he deliberately invites his viewers to recognise the artificiality of cinema, particularly the kind of American-style studio film, with its reliance on effects like day-for-night, that Je Vous Présente Paméla exemplifies.

The writer Graham Greene has a cameo appearance as an insurance company representative in the film, credited as "Henry Graham".  On the DVD of the movie, it was reported that Greene was a great admirer of Truffaut, and had always wanted to meet him, so as it turned out, when the small part came up where he actually talks to the director, he was delighted to have the opportunity.  It was reported that Truffaut was unhappy he wasnt told (until later) that the actor playing the insurance company representative was Greene, as he would have liked to have made his acquaintance, having admired Greenes work as well.

==Recognition==
The film was screened at the 1973 Cannes Film Festival, but wasnt entered into the main competition.   

The film won the 1974 BAFTA Award for Best Film and the Academy Award for Best Foreign Language Film.    Valentina Cortese was nominated for the Academy Award for Best Supporting Actress, and Truffaut for the Academy Award for Directing.

The film is often considered one of Truffauts greatest films. For example, it is one of two Truffaut films featured on Time magazines top 100 list of the 100 Best Films of the Century, along with The 400 Blows. 

==See also==
* List of French submissions for the Academy Award for Best Foreign Language Film
* List of submissions to the 46th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 