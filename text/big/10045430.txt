The Bunker (2001 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name = The Bunker 
| image = The_Bunker_2001_film.jpg
| caption = DVD cover Rob Green
| producer = Daniel Figuero
| writer = Clive Dawson
| starring = Jason Flemyng Andrew Tiernan Christopher Fairbank
| music = Russell Currie
| cinematography = John Pardue
| editing = Richard Milward
| studio = Millennium Pictures
| distributor = High Point Film and Television
| released =  
| runtime = 95 minutes
| country = United Kingdom English
| budget =
}}
 Rob Green, written by Clive Dawson and starring Jason Flemyng.

==Plot==
  John Carlisle) and young Pvt. Neumann (Andrew-Lee Potts) who had been left to defend the bunker under orders from Area Command to wait until relieved. Schenke confronts Heydrich about why the group didnt counterattack the Americans, claiming that there couldnt have been too many of them. Mirus expands on the history of the area, revealing that victims of the Black Plague were massacred on the orders of a stranger who influenced the townspeople to turn against one another. Low on ammunition, the men are haunted by bad dreams, memories, and the feeling that something about the area just isnt right.

As the night proceeds, strange tunnels are found beneath the bunker and the situation in the bunker becomes increasingly tense. Pvt. Schenke (Andrew Tiernan) wants to explore the tunnels, but Lt. Krupp (Simon Kunz) orders against it. Later, Mirus sneaks down into the tunnels and a curious Pfc. Kruezmann (Eddie Marsan) follows him. When they are discovered to be missing, Lt. Krupp believes they have deserted and orders a pursuit. Interrogation of Neumann reveals that Mirus had been wandering off into the tunnels for several weeks and treats the tunnels as his own personal property, as if something is down there that Mirus didnt want him to know about. Cpl. Baumann (Jason Flemyng) discovers a set of plans for the complex while Cpl. Ebert (Jack Davenport) finds a danger sign. Exploring further, Ebert comes to a mine shaft where he hears movement and a pile of sand nearly falls on top of him. Terrified, he is suddenly stabbed from behind. As he lays dying, a silhouetted figure resembling Kreutzmann walks up. Baumann hears his scream and packs up the papers to investigate when his flashlight goes dead. He lights a match and, seeing figures walking towards him, aims his weapon, but they turn out to be his fellow soldiers. When Lt. Krupp tries the flashlight, it turns on easily.

Baumann hypotheses that the Americans are in the tunnels. The group then hears movement, which turns out to be Mirus. The soldiers turn on a rusty old generator, causing a series of lights to turn on down in the tunnels. Lt. Krupp asks Mirus whats going on, but Mirus is evasive. Before the questioning can go any further, the men hear machine gun fire coming from the bunker. However when the men arrive, it appears that Neumann, Franke and Schenke had been firing at nothing. Lt. Krupp attempts to call headquarters but the phone lines are dead.

Convinced that the enemy has worked their way into the tunnels, a game of cat and mouse develops. Stalked by shadowy figures, it becomes increasingly unclear where their fantasies end and the danger begins. Krupp agrees to send an expedition to secure the back entrance to the tunnels, but under the condition that if too much resistance is encountered they return. Mirus and Neumann are left behind in the bunker, while the remaining men split up: Krupp and Schenke in one group and Baumann, Franke and Neumann in the other. Before leaving, Schenke gives a Heil Hitler salute which none of the men in the other group return. Neumann stands guard while Mirus scorns the whole idea of trying to fight anymore. Mirus then claims that his son Werner, who died in Normandy, still talks to him and told him something about the tunnels. 

Down in the tunnels, Schenke and Krupp force their way into an ammo storage room but find nothing but ammunition for artillery. Arriving at the rear entrance, they find it is still sealed, but Schenke hypothesizes that theres another way in. They continue searching and discover Eberts body. Meanwhile, the other group comes across the mine shaft and discover a mass grave. Following a voice, they find Kreutzmann, catatonic and repeatedly screaming "no". Kreutzmann begins babbling about someone being back. They hear a womens voice and try to get Kreutzmann out, but Kreutzmann resists them. He breaks away, knocking Baumann out cold. He then hallucinates a soldier with his throat ripped out while two others take pictures and runs off. 

Meanwhile, Lt. Krupp and Schenke explore a corridor when the lights begin to flicker out. They hear movement and look, only to spin around a second later when they hear movement behind them and see a lamp swinging from side to side. They slowly back down the corridor to the back door of the tunnel complex. They hear loud banging as something tries to break into the tunnels. Kreutzmann comes rushing up the tunnel, screaming. Startled, Schenke and Kruppe who open fire, killing him. The gunfire causes a collapse, burying Lt. Krupp under a pile of wooden beams. When Schenke realizes it was Kreutzmann that they killed, he begins laughing hysterically. 

Back in the bunker, Neumann hears the gunfire and ventures down into the tunnels despite Mirus protests. Alone, Mirus sees a shadowy figure and takes off in panic. As he flees, he hears pounding on each consecutive door he closes behind him, as if he is being pursued. Mirus leaves the bunker and falls outside into the barbed wire. He sees another dark figure approaching and in his panic to get away becomes more and more tangled in the barbed wire. Meanwhile Neumann, who is searching through the tunnels, is caught by an increasingly paranoid Schenke, who puts a knife to his neck. Neumann regains his trust him and the two return to the artillery ammo room and booby trap the room to prevent the ammo from falling into the hands of the Americans, Schenke is sure are stalking through the tunnels. Heydrich, Baumann and Franke decide to blow the bunker door with a stick grenade and fight their way out. Meanwhile, Schenke and Neumann hear the explosion and think that the Americans had set off an booby trap. 

Schenke lights a fuse to destroy the artillery ammo and the two of them move out. Meanwhile, Heydrich, Baumann and Frankes plan to blast the door open fails so they go looking for another entrance. They run straight into Schenke and Neumann, who thinking that the three of them are Americans, open fire. After a short fire fight in which Heydrich, Franke and Baumann use up their remaining STG-44 ammo they withdraw. Franke finally cracks and offers to surrender, but it turns out that instead of Americans Schenke is the one in the corridor. He looks rather deranged at this point. Neumann comes up and aims his K98K rifle at Heydrich, Baumann and Franke as Schenke basically announces his plan to execute the three of them. When Neumann refuses to execute the three of them Schneke pulls out his Luger and executes Franke. He then puts the gun to Neumanns head and threatens to kill him. Neumann is about to pull the trigger when the explosives go off, knocking everybody down. Heydrich, Baumann and Neumann make a run for it while Schenke wildly fires his pistol after them. The three men race through the tunnels but the main exit is blocked off by wreckage. Heydrich pulls Neumann into the mine shaft in order to get away, but Baumann, whose now holding the rifle, stays behind and shoots at the helmeted figure approaching. To his amazement and terror the bullets seem to do nothing. He then begins hallucinating and fires a third shot before Heydrich pulls him away. Soon Schenke arrives, but the others have already erected a barricade over the mine shaft entrance. The men stumble across the mass grave again and then Schenke appears, destroying the barricade. Heydrich shoots Schenke in the chest with a flare gun and Schenke suddenly bursts into flames, screaming and reeling away. The three men begin whacking at the mass grave wall with entrenching tools which break ground. Neumann breaks out and Heydrich is about to follow him when hes stabbed in the back by Schenke, who despite bursting into flames seems to have put them out, however hes badly burned. Baumann launches himself at Schenke and the two begin a fight, which seems even for a while until suddenly Schenke throws Baumann back. Heydrich suddenly intervenes only to get run through by Schenke. However, the distraction gives Baumann time to get away. After a bizarre scene in which Baumann breaks through a layer of soil made up of bones which seems to try and suck him back into the earth, hes saved by Neumann who pulls him out. Baumann then rolls over and hurls a grenade down into the hole, which lands next to the gravely injured Schenke. Several seconds before its explodes Schenke sees several silhouetted figures standing nearby. The movie then cuts to Neman and Baumann running away as the grenade explodes behind them, presumably killing Schenke. Neumann then checks out the bunker from the rear, and comes back to tell the wounded Baumann that its safe. Baumann gives Neumann a handkerchief and tells him to go surrender. Neumann wants to stay but Baumann point blank refuses, telling Neumann to go on, leave. Neumann agrees to leave and on his way down the road finds Mirus body tangled up in the barbed wire. He then sees American soldiers emerging from the woods and waves his handkerchief to get their attention. The film then cuts back to Baumanns memories which show the squad advancing across a sunny field only to find a bunch of deserters about to be executed. An officer hustles over Baumann to participate in the firing squad. One of the deserters is seen praying, and Baumann misses his first shot before shooting the man twice to finish him off. Quickly the firing squad moves in, with one soldier taking pictures with his camera and an officer finishing off the only wounded with a pistol. One member of the firing squad is seen striking a pose while the picture taking soldier snaps another shot. Baumann snaps out of remembering and staggers off towards the Americans.

==Cast==
* Jason Flemyng as  Cpl. Josef Baumann
* Andrew Tiernan as Cpl. Ernst Schenke
* Christopher Fairbank as Sgt. Hans Heydrich
* Simon Kunz as Lt. Michael Krupp
* Andrew-Lee Potts as Pvt. Lorenz Neumann John Carlisle  Pvt. Conrad Mirus
* Eddie Marsan as Pfc. Rudolph Kreuzmann
* Jack Davenport as Cpl. Reinhardt Ebert
* Charley Boorman as Pfc. Oliver Franke
* Nicholas Hamnett as Pvt. Hugo Engels

==Release==
 

==Reception==
 

==Production notes==
The harmonica tune coming from the forest (supposedly played by an American soldier) is Once Upon a Time in the West (Soundtrack)|"Man with a Harmonica" by Ennio Morricone, originally written for the 1968 film Once Upon a Time in the West.

==References==


 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 