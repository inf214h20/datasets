Ice Kacang Puppy Love
 
 
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Ice Kacang Puppy Love
| image          = Icekacangpuppylove.jpg
| alt            =  
| caption        = Official poster
| director       = Ah Niu
| producer       = Adrian Teh
| writer         = Ah Niu Lai Chaing Ming Victor Wong Danny Chan Nicholas Teo Penny Tai
| music          = Ah Niu Chet Ng
| cinematography = Yong Choon Lin
| editing        = Victor Lim Wai Tuck
| studio         = Asia Tropical Films Sdn Bhd
| distributor    = Regentact Company Limited
| released       =  
| runtime        = 105 minutes
| country        = Malaysia
| language       = Chinese
| budget         =
| gross          =
}}

Ice Kacang Puppy Love (Chinese: 初恋红豆冰) is a Malaysian film directed by and starring Ah Niu. Its a 2010 romantic comedy directed by Ah Niu, who also co-written with Lai Chaing Ming, the film tells the story of a young man, portrayed by Ah Niu, trying to convey his love to his long-time crush.

== Plot ==
The story is set in the 1980s in Tronoh, a small town in Perak. Twenty-year-old-plus Botak (Ah Niu) is the younger of a Chinese coffeeshop owners two sons. A reserved, quiet young lad with prickly hair, having been shaved during childhood (thus gaining his nickname Botak, which means baldy in Malay language|Malay), he harbours a secret admiration towards Chew Anqi (周安琪), or Fighting-Fish (Angelica Lee), daughter of a char kuey teow seller, Yue Feng (月凤) (Angela Chan), who rents a stall in the same coffeeshop. However, he was never bold enough to confess his love towards her and only conveyed it through drawing portraits of her.
 Victor Wong), often competed playing marbles (bakuli/buah guli) or fighting fish with each other. Anqi always won these games. After losing a game, Ma Linfan teased Anqi. She became angry and beat Ma Linfan, thus giving her a strong, fierce personality. Because of this, Anqi was given the nickname Fighting-Fish. Whenever she was upset, she would eat ais kacang (spelt as Ice Kacang for the movie) together with Botak, or idle away by the river chatting with him. She would always say that when she grew up, she would return to Penang to look for her father and move to a faraway place, which Botak desired not to realise. As a 20 year-old something adult, she was in a cold relationship with her mother, blaming her being estranged from her father.

Ma Linfan, frustrated at losing to Fighting-Fish each time in fighting fish duels, decided to steal hers, but was caught red-handed and escaped. The second time, which could have been a success, he ended up with a tussle with Fighting-Fish, which had her brushing against him on the lower bodies, causing him to have an erection. Because of this, he believed that he fell in love with Fighting-Fish and went to great lengths to win her love.

Ma Libing (马丽冰) (Fish Leong), the younger sister of Ma Linfan, was secretly in love with Botak. Throughout the story, Ma Libing was a quiet girl and was often seen holding a packet of iced barley drink (thus the nickname Barley-Bing) whenever she went. She chanced upon Botaks love letter to Fighting-Fish in her brothers room. She became upset after reading the letter and sought to win his love with even more determination. The letter somehow ended up with Yue Feng, who then knew of Botaks secret admiration of her daughter and later discovered Botaks portraits of Fighting-Fish in his room.

After a heated quarrel with her mother over her disapproval of her wanting to marry another man, Fighting-Fish travelled to Penang to look for her father with Botak. They arrived at her old home at Chew Jetty (姓周桥), a neighbourhood of wooden houses on water, occupied by Chinese families with the surname Chew (周 zhōu), only to discover that her father (Eric Moo) now operates an illegal gambling den and had remarried with a 5 year-old son and an unborn child. The reunion was disrupted with a raid by the police. Botak, Fighting-Fish and her father managed to escape, but she realised the predicament that her mother was in 10 years ago. She sought to mend ties with her and expressed her desire to further her studies in Singapore.

Botak went after her on the day of her departure, bringing a packet of ais kacang and the unread love letter. During the journey, he was met with two accidents but was only bruised from head to toe. He handed the packet to Fighting-Fish, moving her to tears (she had also discovered Botaks portraits of her during the night before). In the haste, Botak also forgot to hand the letter to her and left the scene drenched from rain. Botaks younger sister submitted one of Botaks portraits to a drawing competition, which won the first prize, even appearing in newspapers. When Botak found out, he ran away in tears, unwilling to embrace the fact that he had hesitated to confess his love to Fighting-Fish.

Towards the end of the scene, all of the young characters had left the town, each on their own career paths. The final scene showed Botak, now older and with a girlfriend, at a pedestrian crossing in Kuala Lumpur, not knowing that Fighting-Fish was also nearby. They both walk away in different directions.

== Cast ==
* 阿牛 Ah Niu as Botak
* 李心潔 Angelica Lee as Fighting-Fish 打架鱼 (real name: Chew Anqi 周安琪)
* 曹格 Gary Chaw as Ma Linfan 马麟帆 Victor Wong as Prince Charming 白马王子
* 梁靜茹 Fish Leong as Ma Libing 马丽冰 aka Barley-Bing (Iced Barley)
* 巫啟賢 Eric Moo as Fighting-Fishs Father
* 易桀齊 Yi Jet Qi as Radio (Botaks elder brother)
* 陳美娥 Angela Chan as Yue Feng 月凤 (Fighting-Fishs mother) Danny Chan as Aquarium Owner
* 戴佩妮 Penny Tai
* 張棟樑 Nicholas Teo

 
 
 
 