Onnorokom Bhalobasha
{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = Onnorokom Bhalobasha
| image          =
| alt            =  
| director       = Shahin Shumon
| producer       = Shish Monwar
| writer         = Abdullah Johir Babu
| starring       = Mahiya Mahi Mike Muniandy Sara Zerin Razzak Amit Hasan Nuton
| music          = Shafiq Tuhin Fuad Al Muktadir Shawkat Ali Emon Ahmed Imtiaz Bulbul Imon Saha
| cinematography = Masum Babul
| editing        = Tawhid Hossain Chowdhury
| studio         = Jaaz Multimedia
| distributor    = Jaaz Multimedia
| released       =  
| country        = Bangladesh
| language       = Bengali
| budget         = 60 lakhs BDT
| gross          = 79 lakhs BDT
}}
Onnorokom Bhalobasha ( ) is a Bangladeshi Bengali language film. Directed  by Shahin Shomon. Onnorokom Bhalobasa is the second film of Jaaz Multimedia. After the success of 1st movie Bhalobasar Rong, this time we are presenting a far better romantic movie named Onnorokom Bhalobasa. Stars Mahiya Mahi, Sara Zerin, Mike Muniandy, Amit Hasan (Villain), Nayok Raj Razzak and many more.
==Cast==
* Mahiya Mahi as Misti
*Sara Zerin as Nijhum
* Mike Muniandy as Shuvo
* Razzak as Karim Chowdhury
* Amit Hasan as Ajhgor Chowdhury (Villain)

==Plot==
Shuvo is a jobless Post-Graduate young guy. He became frustrated for being jobless and decided to go abroad to change his life because he has seen some of his friends went abroad and changed their life. He applies for visa to 3-4 countries and got refused but he was too much passionate to change his life. After he is being refused several times to get a visa, he starts searching a short-cut way and planned to marry someone who has foreign citizenship so that he can easily go abroad with her. Shuvo targets a girl who just has American Visa and planned to use her as the key of his luck. He starts collecting information about that girl whose name is Nijhum. He managed to know the likings and dislikings of Nijhum with the help of his Uncle. Nijhum is a very ultra modern girl from a huge rich family. She likes modern playboy type smart guy who has habit of smoking and drinking and who loves clubbing and party.

Shuvo manages a picture of a girl from his friend’s studio, shows that picture to Nijhum and explains the girl as his ex-girlfriend. He represents the girl in picture as a betrayer which helped him to gain sympathy from her. Nijhum has fallen in love with him and decided to tell Shuvo about her feelings. One day while they were sitting in a park and Nijhum was planning to say everything to Shuvo and they suddenly see girl of the picture over there. This incident change the story remarkably. Shuvo finds a file of that girl and get her name is Misty who is also searching for job. Shuvo returned the file to her and kept helping her silently in many ways. It made Shuvo and Misty closer to each other and they found themselves in a relationship. It creates a distance between Shuvo and Nijhum which made Nijhum upset and disconcert.

Ajgor, brother of Nijhum notices her sister and searched for the reason of her sister’s trouble. Ajgor takes different types of unethical attempts to bring Shuvo back in her sister’s life. Ajgor tries to use money and power to snatch the love of Shuvo for his sister Nijhum. After all these incidents; this movie proves how the real love wins.

==Music==
{{Infobox album
| Name = Onnorokom Bhalobasa
| Type = soundtrack
| Cover = 
| Artist = Nancy, Kishore, Doly Sayantoni, S I Tutul, Samina Chowdhury, Shafiq Tuhin, Mila, Fuad
| Released = 29 January 2013 (Bangladesh)
| Recorded = 2011
| Genre = Films Sound Track
| Length =  
| Label = Jaaz Multimedia
| Producer = Jaaz Multimedia
}}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 