Troublemakers (film)
 Troublemaker}}
{{Infobox film
| name           = Troublemakers (Botte di Natale)
| image          = Botte di natale.jpg
| writer         = Jess Hill
| starring       = Terence Hill Bud Spencer 
| music          = Pino Donaggio
| cinematography = Carlo Tafani
| director       = Terence Hill
| country        = Italy United States Germany
| released       = November 25, 1994
| runtime        = France: 107 min. English
}} 1994 spaghetti western comedy film. It is the last pairing to date of Terence Hill (who also directed) and Bud Spencer.

==Plot==

Travis, a gunslinger receives a letter from his mother Maw, asking him to bring his brother Moses and his family to a Christmas reunion and to give them an unknown treasure she claims to have inherited it from his father. Moses is a bounty hunter, who is angry at Maw, for letting his stolen horses loose, forcing Travis to set up a plan. Travis saves an outlaw named Sam Stone from being hanged and teams up with his brother to catch him. When the brothers stay at Moses familys house, Travis gives them plans to get to Maws. When they head off the next morning, Moses family heads on their way to Maws.

While passing some time at a town, Travis gets into a fist fight with a man, named Dodge and his gang, who were teasing a young woman. Moses unwillingly comes and helps Travis finish them off. The woman, Bridget invites the brothers to have cookies and tea with her and her sister, Melie. After leaving and setting up camp, Travis finds Stone and gang, talking about robbing a bank, the next day at noon. The next day, the brothers plan a trap to catch Stone. The plan backfires when they come in at the wrong time. The sheriff and his deputy mistake Travis and Moses as part of Stones gang and arrest them.
 hang the next morning, Travis tries to free him in the same way he freed Stone (shooting the noose.) A dog attacks Travis and Moses is hanged, but due to his enormous size, he destroys the gallows. Moses survives with only a few splinters. He comes up with a plan to catch Stone, but the plan fails and they are captured. Travis reveals to Stone that he saved his life, and Stone lets them go. Moses comes up with plan B, which works. When Moses falls asleep, Travis frees Stone and Stone dummy-ties him.
 Navajo Indians. Moses son, Junior wants to by a horse for his father. He heads to town and is captured by Stone, who learns of the Christmas reunion. Travis and Moses reunite and wait for Stones next move. Junior escapes and follows the brothers. He camps out for the night. Junior wakes up and finds himself face-to-face with a Western Diamondback Rattlesnake. He gets bitten, but manages to ride to Travis and Moses. The brothers take him to Bridgets and Melie, where he recovers from the venom.

Travis reveals the reunion, angering Moses. Moses is about to kill Travis, but Junior warns them about Stones plan. Before leaving, Bridget and Travis share a kiss. The brothers finally arrive at Maws and discover that she is holding Stone captive. Moses family, Travis, Bridget and Melie all share a Christmas dinner as a family. However, the sheriff, deputy, Dodge and his gang plan to kill them but drop their guns when they are hypnotized by the night skys beauty. A giant fist fight occurs between the two groups, involving the family winning. One of the daughters calls the event "The Fight Before Christmas."
 Denver and all the people are taken a picture of by the photographer.

==Cast==
* Terence Hill as Travis
* Bud Spencer as Moses
* Boots Southerland as Sam Stone
* Ruth Buzzi as Grandma Maw
* Jonathan Tucker as Moses Junior
* Neil Summers as Dodge
* Anne Kasprik as Bridget Eva Hassmann as Melie Ron Carey as Sheriff Fox
* Fritz Sperberg as Deputy Sheriff Joey
* Radha Delamarter as Janie John David Garfield as Photographer Bo Gray, Ottaviano DellAcqua as The outlaws Steven G. Tyler, Massimiliano Ubaldi as The cowboys
* Paloma von Broadley as Jessica
* Samantha Waidler as Mary Lou Kevin Barker, Brian Barker, Charlie Barker, Pilar OConnell, Sarah Waidler, Lauren Myers, Natasha Goslow as The sons
* Patrick Myers as Patrick Jess Hill as Telegraph clerk Geoffrey Martin as Executioner
* Lou Baker as Preacher
* Michael Huddleston: Blacksmith
* Adam Taylor: Blackjack
* Sommer Betsworth: Girl

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 
 