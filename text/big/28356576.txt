Maids (film)
{{Infobox film name            = Maids image           = Maids Meirelles.jpg caption         = Theatrical release poster director        = Fernando Meirelles Nando Olival writer          = Cecília Homem de Mello Fernando Meirelles Renata Melo Nando Olival  based on        =   starring        = Cláudia Missura Graziela Moretto Lena Roque Olivia Araújo Renata Melo music           = André Abujamra cinematography  = Lauro Escorel producer        = Andrea Barata Ribeiro Ana Soares editing         = Deo Teixeira studio          = O2 Filmes distributor     = Pandora Filmes released        =   runtime         = 85 minutes country         = Brazil language        = Portuguese budget          = R$1.2 million    gross           = R$422,675 
}}
Maids ( ) is a 2001 Brazilian film directed by Fernando Meirelles and Nando Olival. It is based on the play of the same name by Renata Melo, and has received multiple awards and nominations.

== Cast ==
* Cláudia Missura as Raimunda
* Graziela Moretto as Roxane
* Lena Roque as Créo
* Olivia Araújo as Quitéria
* Renata Melo as Cida
* Robson Nunes as Jailto
* Tiago Moraes as Gilvan
* Luís Miranda as Abreu
* Eduardo Estrela as Antônio
* Gero Camilo as Claudiney
* Charles Paraventi
* Milhem Cortaz

==Reception== Bread and Roses might envy."  Peter Bradshaw, writing for The Guardian said "it is indeed a little gem". 

The film won the Best Cinematography and Missura, Moretto, Roque, Araújo and Melo shared the Best Supporting Actress Award at the 2001 Recife Film Festival. The five shared again an award at the Ceará Film Festival; this time a Best Actress Award, though. At the Natal Film Festival, Moraes won the Best Actor Award, Estrela won the Best Supporting Actor Award, and it won the Best Score Award.  It won the Best Film Award at the Cuiabá Film Festival, where it also won the Best Screenplay and Best Newcomer (Moretto). 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 
 