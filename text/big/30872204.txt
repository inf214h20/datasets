Land of Plenty
{{Infobox film
| name = Land of Plenty
| image = Land of plenty.jpg
| caption = The German Poster
| director = Wim Wenders
| producer = In-Ah Lee Samson Mucke Gary Winick
| writer = Michael Meredith Wim Wenders Michelle Williams John Diehl
| music = Die Toten Hosen TV Smith Thom & Nackt
| cinematography = Franz Lustig
| editing = Moritz Laube
| distributor = IFC Films
| released =  
| runtime = 123 minutes
| country = United States
| language = English
}} Michelle Williams John Diehl. Sharon Robinson, which was used in the movie.

==Plot== Vietnam veteran who drives around filming and spying on Arabs and people with Arab features. Lana, in contrast, leans toward anti-war convictions and has been changed by her experiences abroad, so feels outside American culture.

Having first-hand knowledge of the Middle East and Africa, she sees similarities between the slums of Los Angeles and those of the Third World. After she and Paul see the murder of a young Pakistani outside the mission, they take his body to his family. Their road trip offers Paul a different view of Arab home life. Over the course of the film, Paul and Lana learn more about each other.

==Production== digital and hand-held Panasonic AG-DVX100 camera, so the production time was cut to a minimum.

In the United States, the film was distributed by IFC Films.

==External links==
* 
*http://www.land-of-plenty.de

 

 
 
 
 
 
 

 