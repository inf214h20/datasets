When We Were Kings
{{Infobox film
| name           = When We Were Kings
| image          = When We Were Kings DVD Cover art.jpg
| director       = Leon Gast
| producer       = Leon Gast David Sonenberg Taylor Hackford 
| distributor    = Gramercy Pictures  (theatrical)  PolyGram Filmed Entertainment
| released       =    }}  
| runtime        = 89 minutes
| country        = United States
| awards         = 
| language       = English
| gross          = $2,789,985
}} Rumble in the Jungle" heavyweight championship match between Muhammad Ali and George Foreman. The fight was held in Zaire (now the Democratic Republic of the Congo) on October 30, 1974.

The film features a number of celebrities, including James Brown, Jim Brown, B.B. King, Norman Mailer, George Plimpton, Spike Lee and Thomas Hauser.

When We Were Kings was released in 1996 to strong reviews,  .   

==Subject matter== Don King James Brown Soul Power. {{Cite news
  | last = Fawcett
  | first = Thomas
  | author2-link = 
  | title = SXSW Film: Daily Reviews and Interviews
  | newspaper = Austin Chronicle
  | date = 2009-03-20
  | url = http://www.austinchronicle.com/gyrobase/Issue/story?oid=oid%3A756486
  | postscript =  }}
  The film also emphasises the questionable ethics of locating the fight in Zaïre, as it was funded by the brutal dictatorship of Mobutu Sese Seko.

Norman Mailer, George Plimpton, Spike Lee, Malick Bowens and Thomas Hauser gave interviews for the film, describing their impressions of Zaire, the fight itself, and particularly their impressions of Ali. The film itself contains these interviews, with many news clips and photos.
 drafted into the United States Army during the Vietnam War.

A soundtrack album was released in 1997. It features live festival performances in addition to new music by Zelma Davis, the duet "When We Were Kings" performed by Brian McKnight and Diana King, and "Rumble In The Jungle", the final recording done by The Fugees, in a collaboration with A Tribe Called Quest and Busta Rhymes.

==Awards and recognition==
When We Were Kings is frequently regarded as one of the best boxing documentaries ever, and maintains a 98% positive rating at Rotten Tomatoes.  It received strong reviews from critics such as Roger Ebert    and Edward Guthmann. 
 Grand Prix of the Belgian Syndicate of Cinema Critics.

==References==
 

==External links==
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 