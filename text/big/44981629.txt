The Mating of Marcella
{{Infobox film
| name           = The Mating of Marcella
| image          = The Mating of Marcella.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Roy William Neill
| producer       = Thomas H. Ince
| screenplay     = Joseph F. Poland R. Cecil Smith 
| starring       = Dorothy Dalton Thurston Hall Juanita Hansen William Conklin Donald MacDonald Milton Ross
| music          = 
| cinematography = John Stumar
| editing        = 
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Roy William Neill and written by Joseph F. Poland and R. Cecil Smith. The film stars Dorothy Dalton, Thurston Hall, Juanita Hansen, William Conklin, Donald MacDonald and Milton Ross. The film was released on May 20, 1918, by Paramount Pictures.  

==Plot==
 

==Cast==
*Dorothy Dalton as Marcella Duranzo
*Thurston Hall as Robert Underwood
*Juanita Hansen as Lois Underwood
*William Conklin as Count Louis Le Favri
*Donald MacDonald as Jack Porter
*Milton Ross as Pedro Escobar
*Spottiswoode Aitken as Jose Duranzo
*Buster Irving as Bobby Underwood 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 