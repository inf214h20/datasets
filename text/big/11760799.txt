Blindness (film)
{{Infobox film
| name           = Blindness
| image          = Blindness poster.jpg
| caption        = Theatrical release poster
| director       = Fernando Meirelles
| producer       = Niv Fichman  Andrea Barata Ribeiro  Sonoko Sakai
| based on        =  
| screenplay     = Don McKellar
| narrator       = Danny Glover
| starring       = Julianne Moore  Mark Ruffalo  Gael García Bernal  Danny Glover  Yoshino Kimura  Alice Braga
| music          = Marco Antonio Guimarães
| cinematography = César Charlone (cinematographer)|César Charlone
| editing        = Daniel Rezende
| distributor    = Focus Features  Miramax Films  
| released       =  
| runtime        = 121 minutes English
| budget         = $25 million
| country        = Canada   Brazil  Japan
| gross          = $19.8 million 
}}
 1995 novel Portuguese author United States on October 3, 2008.

== Plot ==
In an unnamed city, a young Japanese professional (Yusuke Iseya) is suddenly struck blind for no apparent reason. The Japanese man is approached by a few concerned people, one of whom (Don McKellar) offers to drive him home, and later steals his car. The blinded man describes his sudden affliction: an expanse of dazzling white, as though he is "swimming in milk".

Upon arriving home later that evening and noticing her husbands blindness, the Japanese mans wife (Yoshino Kimura) takes him to a local ophthalmologist (Mark Ruffalo) who, after testing the mans eyes, can identify nothing wrong with his sight and recommends further evaluation at a hospital. Among the doctors patients are an old man with a black eye-patch (Danny Glover), a woman with dark glasses (Alice Braga), and a young boy (Mitchell Nye).
 john in a luxury hotel.
 hazmat crew arrives to pick up the doctor, his wife climbs into the van with him, lying that she has also gone blind in order to accompany him into isolation.

In the asylum, the doctor and his wife are first to arrive and both agree they will keep her sight a secret. Several others arrive: the woman with dark glasses, the Japanese man, the car thief, and the young boy. The doctors wife—who continues to remain sighted—comes across the old man with the eye-patch, who describes the condition of the world outside. The sudden blindness, known only as the "white sickness", is now international, with hundreds of cases being reported every day. Desperate by this point, the increasingly  totalitarian government resorts to increasingly ruthless measures to try to staunch the epidemic, refusing the sick any aid or medicines.

In due course, as more and more blind people are crammed into the fetid prison, overcrowding and total lack of any outside support cause the hygiene and living conditions to degrade horrifically in a short time. Soon, the walls and floors are caked in filth and human feces. Anxiety over the availability of food, caused by irregular deliveries, undermines the morale inside. The lack of organization prevents the blind internees from fairly distributing food among each other. The soldiers who guard the asylum become increasingly hostile.

Living conditions degenerate even further when an armed clique of men, led by an ex-barman who declares himself the king of ward 3 (Gael García Bernal), gains control over the sparse deliveries of food. The MRE rations are distributed only in exchange for valuables, and then for the women of the other wards. Faced with starvation, the doctors wife snaps and murders the king of ward 3. His death initiates a chaotic war between the wards, which culminates with the asylum being burned down and most of the inmates die in the fire. Only then do the few survivors discover that the military have abandoned their posts and they are free to venture into the city.

Society has fallen as the entire population is blind amid a city devastated and overrun with filth and dead bodies. The doctors wife leads her husband and several others in search of food and shelter. The doctor and his wife arrive in a supermarket filled with stumbling blind people, and they find food in a basement storeroom. As she prepares to leave and meet her husband outside, she is attacked by the starving people who smell the food she is carrying. Her husband, now used to his blindness, saves her and they manage to return to their friends.

The doctor and his wife with their new "family" eventually make their way back to the house of the doctor, where they establish a permanent home. Just as suddenly as his sight had been lost, the Japanese man recovers his sight one morning. As the friends all celebrate, the doctors wife stands out on the porch, staring up into a white overcast sky and for a moment appears to be going blind herself until the video camera shifts downwards, revealing that she sees the cityscape before her.

== Cast ==
* Julianne Moore as the doctors wife, the only person immune to the epidemic of blindness. Her sight is kept a secret by her husband and others, though as time goes on, she feels isolated in being the only one with sight.    Moore described her characters responsibility: "Her biggest concern in the beginning is simply her husband. But her ability to see ultimately both isolates her and makes her into a leader." The director also gave Moores character a wardrobe that would match the actors skin and dyed blond hair, giving her the appearance of a "pale angel". 
* Mark Ruffalo as Doctor.    The doctor also becomes something of a leader; in an early scene, he reveals that he has been elected as his wards official representative to the rest of the community. Meirelles originally sought to cast actor Daniel Craig as Doctor, but negotiations were not finalized.  Ruffalo said that his character loses the illusion of his self-perspective and perceives his wife as being a person he could aspire to. Ruffalo said, "Thats a very difficult moment for anybody, to have all their perceptions completely shattered, but I think the Doctor finally comes to a peace about his inabilities and his downfall, and admits to an admiration for his wifes strengths."  The actor wore a layer of makeup to appear older and also wore contact lenses to be blind while having his eyes open. The actor said of the experience as a blind character, "At first its terrifying and then its frustrating and then it gets quiet... were tormented by our eyesight... you dont know this until you go blind... As an actor I suddenly felt free." 
* Danny Glover as Man with Black Eye Patch. Glover described his character: "The Man with the Black Eye Patch comes into this new world of blindness already half blind, so I think he understands where he is within his own truth, within himself. I did feel like this character was very much like Saramago because he is completely unapologetic—he is who he is and he accepts who he is."    Glover explained his involvement with the role, "When you are blind you try to adopt another kind of sensitivity, so this role is definitely a challenge from a physical point of view."  democratic election as leader of Ward 1, the bartender immediately declares himself "King of Ward 3" and gains immediate popularity from his "subjects" by prioritizing food over his wards community responsibilities such as burying the dead. He somehow obtains a revolver (and more than six bullets) and uses it to bully the other wards by controlling the food supply. Meirelles followed the advice of Brazilian stage director Antunes Filho and changed the character from the novel by making him more ambiguous, explaining, "In the book, he is really a mean guy, terribly evil from the beginning... but I thought it was more interesting to have him be not evil but more like a child with a gun."  Bernal described the result of his character, "I think the King is just very practical, very pragmatic. He appears cold because he is not an idealist and does not see hope, but he is a survivor, the same as all the others."  The doctors wife kills him with a pair of medical scissors to the neck. His death marks the point when Ward 1 takes back control, with the doctors wifes threat to kill one of the men from Ward 3 for every day her ward goes without food.
* Maury Chaykin as Accountant,  who helps the King of Ward 3 bully the members of the other wards. Because he has been blind since birth, the Accountant is much more used to relying on his other senses, which gives him a major advantage over the other prisoners; he assumes control over Ward 3 and the food supplies for the community after the King is murdered.
* Alice Braga as Woman with Dark Glasses.  Braga described her character as mysterious, believing, "While she does sleep with men because it is easy money, I did not want to treat her purely as a prostitute. She starts out quite tough, but she develops very strong maternal feelings." Meirelles explained that the characters glasses and cascading hair gave her a cold appearance, but through her scenes with the orphaned Boy with the Squint, she develops warmth. 
Secondary characters include:
* Don McKellar as Thief.  McKellar, who wrote the screenplay for the film, had also acted in the past and was cast as the character. The screenwriter described the Thief, "I like the trick where you think the Thief is a bad guy. Hes a pathetic character you first believe is the villain of the piece and then you realize that, no, hes not even close to that. Theres something charming about his desperation because, after a point, you meet the King of Ward Three and learn what real desperation is." 
* Sandra Oh as Minister of Health. 
* Yusuke Iseya as First Blind Man. 
* Yoshino Kimura as First Blind Mans Wife. 
* Mitchell Nye as Boy. 
* Susan Coyne as Receptionist. 
* Martha Burns as Woman with Insomnia. 
* Joe Pingue as The Taxi Driver. 
*  ; its 2002 to 2005 spin-off television series   in 2007.

Meirelles chose an international cast. Producer Niv Fichman explained Meirelles intent: "He was inspired by   great masterwork to create a microcosm of the world. He wanted it cast in a way to represent all of humanity."   

==Production==

===Development===
The rights to the 1995 novel Blindness (novel)|Blindness were closely guarded by author José Saramago.  Saramago explained, "I always resisted because its a violent book about social degradation, rape, and I didnt want it to fall into the wrong hands." Director Fernando Meirelles had wanted to direct a film adaptation in 1997, perceiving it as "an allegory about the fragility of civilization". Saramago originally refused to sell the rights to Meirelles, Whoopi Goldberg, or Gael García Bernal.  In 1999, producer Niv Fichman and Canadian screenwriter Don McKellar visited Saramago in the Canary Islands; Saramago allowed their visit on condition that they not discuss buying the rights. McKellar explained the changes he intended to make from the novel and what the focus would be, and two days later he and Fichman left Saramagos home with the rights. McKellar believed they had succeeded where others had failed because they properly researched Saramago; he was suspicious of the film industry and had therefore resisted other studios efforts to obtain the rights through large sums of money alone.    Conditions set by Saramago were for the film to be set in a country that would not be recognizable to audiences,  and that the canine in the novel, the Dog of Tears, should be a big dog.   

Meirelles originally envisioned doing the film in Portuguese similar to the novels original language, but instead directed the film in English, saying, "If you do it in English you can sell it to the whole world and have a bigger audience."    Meirelles set the film in a contemporary large city, seemingly under a totalitarian government, as opposed to the novel that he believed took place in the 1940s (actually, the book is more likely to take place in the 80s or later, as evident by the fact that the characters stumble upon a store with modern appliances like microwave ovens and dishwashers, and referral to AIDS as a feared disease). Meirelles chose to make a contemporary film so audiences could relate to the characters.  The director also sought a different allegorical approach. He described the novel as "very allegorical, like a fantasy outside of space, outside the world", and he instead took a naturalistic direction in engaging audiences to make the film less "cold."   

===Writing===
Don McKellar said about adapting the story, "None of the characters even have names or a history, which is very untraditional for a   simplicity". The novel defines its characters by little more than their present actions; doing the same for the adaptation became "an interesting exercise" for McKellar. 

McKellar attended a summer camp for the blind as part of his research. He wanted to observe how blind people interacted in groups. He discovered that excessive expositional dialogue, usually frowned upon by writers, was essential for the groups. McKellar cut one of the last lines in the novel from his screenplay: "I dont think we did go blind, I think we are blind. Blind but seeing. Blind people who can see, but do not see." McKellar believed viewers would by that point have already grasped the symbolism and didnt want the script to seem heavy-handed. He also toned down the visual cues in his screenplay, such as the "brilliant milky whiteness" of blindness described in the novel. McKellar knew he wanted a stylistically adept director and didnt want to be too prescriptive, preferring only to hint at an approach. 

===Filming and casting===
 
Meirelles chose  , Iss. #1007/1008, August 22/29, 2008, pg.55. 

By September 2006, Fernando Meirelles was attached to Blindness, with the script being adapted by Don McKellar. Blindness, budgeted at $25 million as part of a Brazilian and Canadian co-production, was slated to begin filming in summer 2007 in the towns of São Paulo and Guelph.  Filming began in early July in São Paulo and Guelph.    Filming also took place in Montevideo, Uruguay.    São Paulo served as the primary backdrop for Blindness, being a city mostly unfamiliar to U.S. and European audiences. With its relative obscurity, the director sought São Paulo as the films generic location. Filming continued through autumn of 2007.   
 City of God led a series of workshops to coach the cast members. Duurvoort had researched the mannerisms of blind people to understand how they perceive the world and how they make their way through space. Duurvoort not only taught the extras mannerisms, but also to convey the emotional and psychological states of blind people.  One technique was reacting to others as a blind person, whose reactions are usually different from those of a sighted person. Meirelles described, "When youre talking to someone, you see a reaction. When youre blind, the response is much flatter. Whats the point  ?" 

===Filmmaking style===
 s 1568 painting The Parable of the Blind in the film Blindness.]]
Meirelles acknowledged the challenge of making a film that would simulate the experience of blindness to the audience. He explained, "When you do a film, everything is related to point of view, to vision. When you have two characters in a dialogue, emotion is expressed by the way people look at each other, through the eyes. Especially in the cut, the edit. You usually cut when someone looks over. Film is all about point of view, and in this film there is none."    Similar to the book, blindness in the film serves as a metaphor for human natures dark side: "prejudice, selfishness, violence and willful indifference." 

With only one characters point of view available, Meirelles sought to switch the points-of-view throughout the film, seeing three distinct stylistic sections. The director began with an omniscient vantage point, transited to the intact viewpoint of the doctors wife, and changed again to the Man with the Black Eye Patch, who connects the quarantined to the outside world with stories. The director concluded the switching with the combination of the perspective of the Doctors Wife and the narrative of the Man with the Black Eye Patch. 

The film also contains visual cues, such as the 1568 painting The Parable of the Blind by Pieter Bruegel the Elder. Allusions to other famous artworks are also made. Meirelles described the intent: "Its about image, the film, and vision, so I thought it makes sense to create, not a history of painting, because its not, but having different ways of seeing things, from Rembrandt to these very contemporary artists. But its a very subtle thing." 

==Release==

===Theatrical run===
Prior to public release, Meirelles screened Blindness to test audiences. He described the impact of test screenings: "If you know how to use it, how to ask the right questions, it can be really useful." A test screening of Meirelles first cut in Toronto resulted in ten percent of the audience, nearly 50 people, walking out of the film early. Meirelles ascribed the problem to a rape scene that takes place partway through the film, and edited the scene to be much shorter in the final cut.    Meirelles explained his goal, "When I shot and edited these scenes, I did it in a very technical way, I worried about how to light it and so on, and I lost the sense of their brutality. Some women were really angry with the film, and I thought, Wow, maybe I crossed the line. I went back not to please the audience but so they would stay involved until the end of the story."  He also found that a New York test screening expressed concern about a victim in the film failing to take revenge. Meirelles believed this concern to reflect what Americans have learned to expect in their cinema. 
 61st Cannes Film Festival on May 14, 2008,  where it received a "tepid reception."  Straw polls of critics were "unkind" to the film. 

Blindness was screened at the Toronto International Film Festival in September 2008 as a Special Presentation.  The film also opened at the Atlantic Film Festival on September 11, 2008,  and had its North American theatrical release on October 3, 2008.

===Critical reception===
The film was on some critics top ten lists of 2008 films but has received very mixed, predominantly negative reviews. With only 62 of 149 (42%) reviews on the film review site Rotten Tomatoes being positive Blindness is considered "rotten". The film has an average rating of 5.2 out of 10. 

Screen Internationals Cannes screen jury which annually polls a panel of international film critics gave the film a 1.3 average out of 4, placing the film on the lower-tier of all the films screened at competition in 2008.      Of the film critics from the Screen International Cannes critics jury, Alberto Crespi of the Italian publication LUnità, Michel Ciment of French film magazine Positif (magazine)|Positif and Dohoon Kim of South Korean film publication Cine21, all gave the film zero points (out of four). 

Kirk Honeycutt of   described the film: "Blindness emerges onscreen both overdressed and undermotivated, scrupulously hitting the novels beats yet barely approximating, so to speak, its vision." Chang thought that Julianne Moore gave a strong performance but did not feel that the film captured the impact of Saramagos novel.  Roger Ebert called Blindness "one of the most unpleasant, not to say unendurable, films Ive ever seen."  A. O. Scott of The New York Times stated that, although it "is not a great film, ... it is, nonetheless, full of examples of what good filmmaking looks like." 

Stephen Garrett of   and The Constant Gardener did not suit him for directing the "heightened reality" of Saramagos social commentary. 

Peter Bradshaw of The Guardian called it "an intelligent, tightly constructed, supremely confident adaptation": "Meirelles, along with screenwriter Don McKellar and cinematographer Cesar Charlone, have created an elegant, gripping and visually outstanding film. It responds to the novels notes of apocalypse and dystopia, and its disclosure of a spiritual desert within the modern city, but also to its persistent qualities of fable, paradox and even whimsy."    "Blindness is a drum-tight drama, with superb, hallucinatory, images of urban collapse. It has a real coil of horror at its centre, yet is lightened with gentleness and humour. It reminded me of George A Romeros Night of the Living Dead, and Peter Shaffers absurdist stage-play Black Comedy. This is bold, masterly, film-making." 

The Boston Globes Wesley Morris raved about the leading actress: "Julianne Moore is a star for these terrible times. She tends to be at her best when the world is at its worst. And things are pretty bad in "Blindness," a perversely enjoyable, occasionally harrowing adaptation of José Saramagos 1995 disaster allegory.   "Blindness" is a movie whose sense of crisis feels right on time, even if the happy ending feels like a gratuitous emotional bailout. Meirelles ensures that the obviousness of the symbolism (in the global village the blind need guidance!) doesnt negate the storys power, nor the power of Moores performance. The more dehumanizing things get, the fiercer she becomes." 

The film appeared on some critics top ten lists of the best films of 2008. Bill White of the Seattle Post-Intelligencer named it the 5th best film of 2008,  and Marc Savlov of The Austin Chronicle named it the 8th best film of 2008.     

===Protests===
The film has been strongly criticized by several organizations representing the blind community. Dr. Marc Maurer, President of the National Federation of the Blind, said: "The National Federation of the Blind condemns and deplores this film, which will do substantial harm to the blind of America and the world." {{cite news
| title = National Federation of the Blind Condemns and Deplores the Movie Blindness
| publisher = National Federation of the Blind
| date = 2008-09-30
| url = http://www.nfb.org/nfb/NewsBot.asp?MODE=VIEW&ID=368
| accessdate = 2008-10-01}} 
A press release from the American Council of the Blind said "...it is quite obvious why blind people would be outraged over this movie.  Blind people do not behave like uncivilized, animalized creatures." {{cite news
| title = Tens of Thousands of Blind Americans Object to the Movie ‘Blindness’
| publisher = American Council of the Blind
| date = 2008-09-29
| url = http://www.acb.org/press-releases/press-release_Blindness_the-movie.html
| accessdate = 2008-10-01}} 
The National Federation of the Blind announced plans to picket theaters in at least 21 states, in the largest protest in the organizations 68-year history.  {{cite news
| title = Blindness Protests
| publisher = The Associated Press
| date = 2008-09-30
| url = http://hosted.ap.org/dynamic/stories/F/FILM_BLINDNESS_PROTESTS
| accessdate = 2008-10-01}}  José Saramago has described his novel as allegorically depicting "a blindness of rationality". He dismissed the protests, stating that "stupidity doesnt choose between the blind and the non-blind." {{cite news
| title = Author decries Blindness protests as misguided
| publisher = CBC News
| date = October 4, 2008
| url = http://www.cbc.ca/news/arts/film/story/2008/10/04/saramago-blindness-reaction.html
| accessdate = May 26, 2013}} 

== José Saramago reaction to the movie ==

In a closed section, José Saramago watched the movie together with Fernando Meirelles. When the movie ended, Saramago was in tears. He turned to Fernando Meirelles and said: "Fernando, I am so happy to have seen this movie. I am as happy as I was the day I finished the book."  

==See also== The Day of the Triffids, the 1951 John Wyndham novel (and its many adaptations) about societal collapse following widespread blindness
*"Many, Many Monkeys", an episode from the The Twilight Zone with a similar premise

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   Official international press release and production information.

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 