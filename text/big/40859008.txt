Life and Extraordinary Adventures of Private Ivan Chonkin
{{Infobox film
 | name =  
 | image = Life and Extraordinary Adventures of Private Ivan Chonkin.jpg
 | caption =
 | director = Jiří Menzel
 | writer = 
 | starring =  
 | music =  
 | cinematography =  
 | editing =  
 | producer =
 | distributor = 
 }}
Life and Extraordinary Adventures of Private Ivan Chonkin, also known as Život a neobyčejná dobrodružství vojáka Ivana Čonkina, is a 1994 comedy drama film directed by Jiří Menzel. The film entered the competition at the 51st Venice International Film Festival, in which it won the President of the Italian Senates Gold Medal.  It is based on the novel The Life and Extraordinary Adventures of Private Ivan Chonkin by Vladimir Voinovich. It is a coproduction between Russia, Czech Republic, United Kingdom, France and Italy.

== Cast ==
* Gennadi Nazarov: Ivan Chonkin 
* Zoya Buryak: Njura 
* Vladimir Ilin: Golubev 
* Valeriy Zolotukhin: Kilin 

==References==
 

==External links==
* 
 
  
 
 
 
 