Apartment 1303 3D
{{Infobox film
| name           = Apartment 1303 3D
| image          = Poster for Apartment 1303 3D.jpg
| alt            =  
| caption        = Promotional poster
| director       = Michael Taverna
| producer       = Cindy Nelson-Mullen Michael Taverna
| writer         = Kei Ôishi Michael Taverna
| starring       = Mischa Barton Rebecca De Mornay Julianne Michelle Corey Sevier John Diehl
| music          = John Lissauer
| cinematography = Paul M. Sommers	 
| editing        = Mattias Morheden
| studio         = 
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = United States Canada
| language       = English
| budget         = $5 million 
| gross          = $3,363,093 
}} film of VOD platform on June 17, 2013, followed by a theatrical release on July 25, 2013. 

==Plot==
Following a family dispute, Janet moves out of the home she shares with her older sister, Lara and their single mother, Maddie. She moves into apartment 1303 on the thirteenth floor of a downtown Detroit apartment building. A nine-year-old neighbor, Emily, explains to Janet that a previous occupant of her new apartment killed herself. Strange things begin to occur in the apartment and when Janet appears bruised at work, she rebuffs concerns that her boyfriend, Mark, is abusing her and blames the marks on sleepwalking.
 ghost takes hold of Janets body leading to her suicide. Her sister, Lara, later arrives to gather Janets belongings and begins to experience the same terrors.

A detective that was on the case talks to Lara and she believes Janet was murdered. He agrees since hes been investigating unknown suicides with other tenants. Lara discovers the name of the first tenant from 20 years back, Jennifer Logan. The detective tells Lara the sad story of what happened to Jennifer. At the age of 12, she moved in to apartment 1303 with her mother, Mary, a respectable school teacher and recently divorced. For the first few years in the apartment, they lived in peace and Mary was a loving mother to Jennifer. However, the peace was shattered when Mary lost her job as a teacher during a dispute with a parent in a parent-teacher conference. She found work as a prostitute to pay for the apartment, became an alcoholic and brutally abused Jennifer. This leads to Jennifer murdering Mary and burying her in a built in closet. A few years later, neighbors complained about the smell of the apartment, causing the police and the health department to investigate. By the time they got to the apartment to confront Jennifer, she had already committed suicide by jumping out the window and the police found the decomposed body of Mary. In the years that followed more tenants were thought to have committed suicide with Janet being the recent one.

While taking a bath, Lara gets a cryptic warning from Janet to leave the apartment and never come back. However, another dispute with Maddie has Lara moving into the same apartment and unintentionally ignoring Janets warning. Jennifer soon arrives and kills Mark by throwing him out the window. Horrified, Lara tries to escape the apartment complex to avoid Jennifer trying to kill her and permanently stay away from the complex for good. However, she catches Emily and the landlord, ONeill in front of her. ONeill reveals that he and his daughter, Emily, were previously killed by Jennifer during their first year in apartment 1303. Since then, they and Janet had been warning others to never move in to apartment 1303 to no avail. As Maddie tries to talk some sense to Lara, Jennifer pushes her towards the knife and kills her. Just before she can finish the job to kill Lara, the police arrive and Jennifer disappears. Lara is arrested for both Maddie and Marks murder and is taken away to be booked. Jennifer is last seen sitting on the same spot where she committed suicide: an unseen warning of what happens when anyone moves into apartment 1303.

==Cast==
* Mischa Barton as Lara Slate
* Rebecca De Mornay as Maddie Slate
* Julianne Michelle as Janet Slate
* Corey Sevier as Mark Taylor
* John Diehl as Detective
* Gordon Masten as ONeil 
* Madison McAleer as Emily
* Kathleen Mackey as Jennifer Logan

==Production==
The Swedish director Daniel Fridell had originally been set to direct the project but was later replaced by Michael Taverna.

A specialised crew of 3D experts from Hong Kong were hired for the film. 

==Reception==
Justin Chang of Variety (magazine)|Variety called it an "inept and derivative tale" that is not unintentionally funny enough to be "so bad its good".   Frank Scheck of The Hollywood Reporter wrote, "This non-starter horror film, inexplicably released in 3D, wont haunt theaters for very long."   Shawn Macomber of Fangoria rated it 1/4 stars and wrote, "The movie exhibits the germ of something that could potentially be a lot of fun on Saturday night basic cable. Problem is, its higher aspirations are a gauntlet thrown in the way of a deviously barmy romp."   Gareth Jones of Dread Central rated it 0.5/5 stars and wrote, "Bereft of interesting characters, dialogue, acting ability, scares, visual aplomb or much of anything else, Apartment 1303 is occasionally good for a derogatory laugh, or simply to witness what must be the middle of one serious mire in Rebecca de Mornays career."   Andrew Pollard of Starburst (magazine)|Starburst rated it 3/10 stars and wrote that there is "no emotion, no care, no feeling and no reason to watch." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 