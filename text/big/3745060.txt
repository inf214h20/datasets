I, a Man
{{Infobox film
| name           = I, a Man
| image          =
| image_size     =
| caption        =
| director       = Paul Morrissey Andy Warhol
| producer       =
| writer         =
| narrator       = Tom Baker Ultra Violet
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 99 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}} American film Tom Baker, erotic Scandinavian film I, a Woman (1965) which had opened in the United States in October 1966.   

==Cast== Warhol Superstars from his studio The Factory. 
*Tom Baker as Tom
*Cynthia May as Girl in Kitchen
*Nico as Girl with TV
*Ingrid Superstar as Girl on Table
*Stephanie Graves as Girl in Penthouse 
*Valerie Solanas as Girl on Staircase
*Bettina Coffin as Last Girl Ultra Violet

Warhol gave Solanas a part in the film for $25 and as compensation for a script she had given to Warhol called Up Your Ass, which he had lost.  Solanas later attempted to kill Warhol by shooting him. According to a 2004 biography of Jim Morrison, Morrison had agreed to appear in this film opposite Nico, but the management of The Doors talked him out of it. Morrison then referred drinking buddy Tom Baker to Warhol.

==Reception==
Roger Ebert of the Chicago Sun Times wrote the film was "not dirty, or even funny, or even anything but a very long and pointless home movie," and described it as "an elaborate, deliberately boring joke."  Howard Thompson in his review for the New York Times wrote "The nudity is no match for the bareness of the dialogues drivel and the dogged tone of waste and ennui that pervade the entire film." 

==See also==
* Andy Warhol filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 