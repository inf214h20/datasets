National Parks Project
 
The National Parks Project is a Canadian music and film project. Released in 2011 to mark the 100th anniversary of the creation of the  , May 20, 2011.  the project sent three Canadian musicians and a filmmaker to each of 13 Canadian national parks, one in each province and territory, to create and score a short documentary film about the park. 
 Hot Docs.

==Awards==
Zacharias Kunuks segment, Sirmilik, won the Best Short Documentary at the 32nd Genie Awards.

==Projects==
{|class="wikitable"  width="100%"
!Province/Territory
!Park
!Musicians
!Filmmaker
|- Alberta
|Waterton Waterton Lakes Cadence Weapon, Mark Hamilton Peter Lynch Peter Lynch
|- British Columbia Gwaii Haanas Gwaii Haanas Sarah Harmer, Jim Guthrie Scott Smith Scott Smith
|- Manitoba
|Wapusk Wapusk
|Kathleen Edwards, Matt Mays, Sam Roberts Hubert Davis Hubert Davis
|- New Brunswick Kouchibouguac National Kouchibouguac
|Casey Mecija, Don Kerr, Ohad Benchetrit Jamie Travis
|- Newfoundland and Labrador Gros Morne Gros Morne Jamie Fleming, Melissa Auf der Maur, Sam Shalabi Sturla Gunnarsson
|- Northwest Territories Nahanni National Nahanni
|Shad (rapper)|Shad, Jace Lasek, Olga Goreas Kevin McMahon
|- Nova Scotia Cape Breton Cape Breton Highlands Tony Dekker, Old Man Luedecke, Daniela Gesundheit Keith Behrman
|- Nunavut
|Sirmilik Sirmilik
|Andrew Dean Stone, Tanya Tagaq Zacharias Kunuk
|- Ontario
|Bruce Bruce Peninsula John K. Samson, Christine Fellows, Sandro Perri Daniel Cockburn
|- Prince Edward Island Prince Edward Prince Edward Island Chad Ross, Sophie Trudeau, Dale Morningstar John Walker
|- Quebec
|Mingan Mingan Archipelago Sebastien Grainger, Dan Werb, Jennifer Castle Catherine Martin Catherine Martin
|- Saskatchewan
|Prince Prince Albert Andre Ethier Andre Ethier, Mathieu Charbonneau, Rebecca Foon
|Stéphane Lafleur
|- Yukon
|Kluane Kluane
|Graham Van Pelt, Ian DSa, Mishka Stein Louise Archambault
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 