Lego DC Comics: Batman Be-Leaguered
{{Infobox film
| name           = Lego DC Comics: Batman Be-Leaguered
| image          = 
| caption        = 
| alt            = 
| director       = Rick Morales
| screenplay     = 
| based on       = 
| starring       = Dee Bradley Baker Troy Baker Grey DeLisle John DiMaggio Tom Kenny Khary Payton Paul Reubens Kevin Michael Richardson James Arnold Taylor
| music          =  LEGO DC DC Entertainment
| distributor    = Warner Home Video
| released       =  
| runtime        = 
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
Lego DC Comics: Batman Be-Leaguered is a direct-to-video film animated superhero film based on the Lego and DC Comics brands. 

==Plot==
  Penguin who was trying to steal a priceless jewel, as well as Man-Bat, and Joker (comics)|Joker, Superman flies out, but he then disappears along with the villains and the gem.
 Flash to help with his search for Superman. The Flash travels around the world with Batman following in the Batwing, until they find Captain Cold trying to steal hieroglyphics. Flash and Batman fight him, but Flash disappears mid fight while Batman is frozen solid without his utility belt. Batman thaws and defeats Cold, but he and the hieroglyphics disappear.

Batman then asks Aquaman for help in the Batboat, but they find Black Manta and his robot sharks trying to steal the trident of Poseidon. Batman once again wins the fight, but Aquaman, Manta, and the trident all disappear.
 Cyborg are fighting Lex Luthor, who is trying to steal the Daily Planet Globe for an anonymus buyer. Once again, the Justice League members disappear, Batman defeats the villain, but Lex and the globe disappear.

Batman deduces that Bat-Mite is behind it, and travels to the Justice League headquarters at the Hall of Justice (the last place he wanted to go) where Bat-Mite prepared a trap for the Justice League including all of the stolen items. Batman does nothing which Bat-Mite did not expect. But he did hint them that since the cage was immune to their powers, they shouldnt use them, and they escaped by opening the door. Batman promply accepts the invitation to the Justice League. Bat-Mite summons the villains, but they are easily defeated by the combined might of the Justice League, so Bat-Mite decides that instead of cheering on one hero, he should cheer on an entire team. Before disappearing, Bat-Mite makes the villains disappear.

==Cast==
* Dee Bradley Baker - Aquaman, Man-Bat
* Troy Baker - Batman
* Grey DeLisle - Lois Lane, Wonder Woman
* John DiMaggio - Joker (comics)|Joker, Lex Luthor Penguin
* Nolan North - Superman, Alfred Pennyworth Cyborg
* Paul Reubens - Bat-Mite
* Kevin Michael Richardson - Black Manta, Captain Cold Flash

==Crew==
* Lisa Schaffer - Casting and Voice Director

==References==
 

==External links==
*   at Internet Movie Database

 
 

 
 
 
 
 
 
 
 