Where the Boys Are
 
 
{{Infobox film
| name           = Where the Boys Are
| image          = Wheretheboysare.jpg
| image size     =
| caption        = DVD cover by Reynold Brown
| producer       = Joe Pasternak
| director       = Henry Levin
| based on       =   George Wells George Hamilton Yvette Mimieux Jim Hutton  Frank Gorshin
| music          = Score: George E. Stoll Jazz:   (music) Howard Greenfield (lyrics)
| cinematography = Robert J. Bronner
| editing        = Fredric Steinkamp
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $2 million 
| gross          = $3.5 million (US rentals) Tom Lisanti, Hollywood Surf and Beach Movies: The First Wave, 1959–1969, McFarland 2005, p38-42 
}}
 American coming-of-age George Wells based on the novel of the same name by Glendon Swarthout, about four Midwestern college co-eds who spend spring break in Fort Lauderdale, Florida. The title song "Where the Boys Are" was sung by Connie Francis, who also co-starred in a supporting role. The film was aimed at the teen market, featuring sun, sand and romance. Released in the wintertime, it inspired thousands of additional American college students to head to Fort Lauderdale for their annual spring break.

Where the Boys Are was one of the first teen films to explore adolescent sexuality and the changing sexual morals and attitudes among American college youth. It won Laurel awards for Best Comedy of the Year and Best Comedy Actress (Paula Prentiss).

==Plot==
The main focus of Where the Boys Are is the "coming of age" of four girl students at a Midwestern university during spring vacation. As the film opens, Merritt Andrews (Dolores Hart), the smart and assertive leader of the quartet, expresses the opinion in class that premarital sex might be something young women should experience. Her speech eventually inspires the insecure Melanie Tolman (Yvette Mimieux) to lose her virginity soon after the young women arrive in Ft. Lauderdale, Florida. Tuggle Carpenter (Paula Prentiss), on the other hand, seeks to be a "baby-making machine," lacking only the man to join her in marriage. Angie (Connie Francis) rounds out the group as a girl who is clueless when it comes to romance.
 George Hamilton), Yale who Michigan State, but becomes disillusioned when he becomes enamored of the older woman Lola Fandango (Barbara Nichols), who works as a "mermaid" swimmer/dancer in a local bar. Angie stumbles into love with the eccentric jazz musician Basil (Frank Gorshin).

Merritt, Tuggle, and Angies post-adolescent relationship angst quickly evaporates when they discover Melanie is in distress after going to meet Franklin at a motel. However another of the "Yalies", Dill, arrives at the motel room instead and then rapes her. She ends up walking into the nearby road looking distraught, her dress torn. Just as her friends arrive, she is hit by a car and ends up in the hospital.

The friends realize the potentially serious consequences of their actions and resolve to act in a more responsible, mature manner. The film ends on a melancholy note, with Melanie recovering in the hospital while Merritt looks after her, and with Merritts promises to Ryder to continue a long-distance relationship. He then offers to drive them back to their college.

==Cast==
* Dolores Hart as Merritt Andrews
* Paula Prentiss as Tuggle Carpenter
* Yvette Mimieux as Melanie Tolman
* Connie Francis as Angie George Hamilton as Ryder Smith
* Jim Hutton as TV Thompson
* Rory Harrity as Franklin
* Frank Gorshin as Basil
* Chill Wills as Police Captain
* Barbara Nichols as Lola Fandango

==Production==
Joe Pasternak bought the film rights to the novel, which was originally known as Unholy Spring, even  before it was published. He assigned George Wells to write the script. Columbia to Join in Hawaiian Cycle: Carol Haney Going Dramatic: Nick Adams Agreeable Rebel
Scheuer, Philip K. Los Angeles Times (1923-Current File)   20 Oct 1959: A7.  
 Bill Smith, Russ Tamblyn, Luana Patten, Maggie Pierce, Carmen Phillips, and Nancy Walters; then get one star to head the cast." Looking at Hollywood: Joe Pasternak to Film College Vacation Tale
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   03 July 1959: a4.   Natalie Wood, who had just made All the Fine Young Cannibals for MGM, was mentioned as a possible star at one stage. Looking at Hollywood: Natalie and Career Are in Good Shape
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   24 Dec 1959: a1.  

MGM eventually persuaded the books author to change the title from Unholy Spring to Where the Boys Are. MGM, With 44 Films, Maps Plans Into 1960
Los Angeles Times (1923-Current File)   08 July 1959: C10.   Who Made That?: SPRING BREAK
Kennedy, Pagan. New York Times Magazine (Mar 24, 2013): 20.  

Henry Levin was signed to direct. The first two stars confirmed for the movie were George Hamilton and Yvette Mimieux. New Pictures Get Go-Ahead Signals: Karlson, Levin Will Direct for Widmark and Pasternak
Scott, John L. Los Angeles Times (1923-Current File)   18 May 1960: A11.   Paula Prentiss was cast despite never having made a movie before. Pasternak Signs Unknown as Star: Paula Prentiss Will Have Lead in Where Boys Are
Hopper, Hedda. Los Angeles Times (1923-Current File)   10 June 1960: A9.   Connie Francis also made her movie debut. Whats the Silliest Thing on Radio?
Wolters, Larry. Chicago Daily Tribune (1923-1963)   26 June 1960: n12.  

The novel contained a section where the students help raise money to ship arms to Fidel Castro for his revolution in Cuba. Pasternak decided to remove this. "The author was very sympathetic to Castro," said Pasternak. "Politics does not belong in entertainment. As actors or writers or movie makers of any sort, we have a right to our political preferences.  But that is why we have secret ballots... We felt that the only revolution these youngsters should be involved in was their personal revolution." PASTERNAK BARS POLITICS IN FILM: Producer Cuts Part on Cuba in Where the Boys Are, Based on Swarthout Book.
By MURRAY SCHUMACHSpecial to The New York Times.. New York Times (1923-Current file)   04 Aug 1960: 16.  
 Sean Flynn in the movie. Flynns Son Making His Movie Debut
Los Angeles Times (1923-Current File)   27 June 1960: 10.   Jimmy Stewart Wants to Do Play in London
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   16 July 1960: n18.  

Hamilton says he improvised the scene where he wrote a question mark in the sand to Dolores Hart. He thought he was making a "little nothing of a film" and did not enjoy the shoot but it became a big success. The film also featured the big screen debut, in an unaccredited role, by former Miss Ohio and Elvis Presley consort Kathy Gabriel.  George Hamilton & William Stadiem, Dont Mind If I Do, Simon & Schuster 2008 p 143 

==Music==
The kind of cool modern jazz (or west coast jazz) popularized by such acts as Dave Brubeck, Gerry Mulligan, and Chico Hamilton, then in the vanguard of the college music market, features in a number of scenes with Basil. Called "dialectic jazz" in the film, the original compositions were by Pete Rugolo. 
 MGM had bolstered the films success potential by giving a large role to Connie Francis, the top American female recording star and a member of the MGM Records roster. Francis had solicited the services of Neil Sedaka and Howard Greenfield, who had written hit songs for her, to write original material for her to perform on the films soundtrack including a "Where the Boys Are" title song. Sedaka and Greenfield wrote two potential title songs for the film, but producer Joe Pasternak passed over the song Francis and the songwriting duo preferred in favor of a lush 50s style movie theme.  Francis recorded the song on October 18, 1960, in a New York City recording session with Stan Applebaum arranging and conducting.

Although it only peaked at # 4 in the US, the theme song of "Where the Boys are" became Connie Franciss signature tune, followed by several cover versions.

 

Besides the theme song, Francis sang another Sedaka-Greenfield composition: "Turn on the Sunshine", in the film.

The films soundtrack also features "Have You Met Miss Fandango". The song was sung by co-star Barbara Nichols and featured music by Victor Young and lyrics by Stella Unger. TCM   

MGM did not release a soundtrack album for Where the Boys Are.  

==Reception==
The film was a success at the box office.

MGM signed Henry Levin, Dolores Hart, Prentiss and Hutton to long term contracts. MGM Reactivates Spring Musical: Its Early Lerner-Loewe; Paramount Takes New Play
Scheuer, Philip K. Los Angeles Times (1923-Current File)   13 Oct 1960: B17.  
===Critical===
American humanities professor Camille Paglia  has praised Where the Boys Are for its accurate depiction of courtship and sexuality, illustrating once-common wisdom that she contends has been obscured by second-wave feminism: 
:The theatrics of public rage over date rape are   way of restoring the old sexual rules that were shattered by my generation.  Because nothing about the sexes has really changed.  The comic film Where the Boys Are (1960), the ultimate expression of ‘50s man-chasing, still speaks directly to our time.  It shows smart, lively women skillfully anticipating and fending off the dozens of strategies with which horny men try to get them into bed.  The agonizing date rape subplot and climax are brilliantly done.  The victim, Yvette Mimieux, makes mistake after mistake, obvious to the other girls.  She allows herself to be lured away from her girlfriends and into isolation with boys whose character and intentions she misreads.  Where the Boys Are tells the truth.  It shows courtship as a dangerous game in which the signals are not verbal but subliminal.

===Proposed Sequel===
In 1960 it was announced Pasternak would make a follow up, Where the Girls Are starring George Hamilton. It was meant to be an entirely different story rather than a sequel. Sinatra, Martin Planning Comedy: Janet Leigh Paged for Lead; Barrie Chase in State Fair
Hopper, Hedda. Los Angeles Times (1923-Current File)   02 Nov 1960: A9.  But this was never produced. Harrison, Portman Up for Sherlock: Stevens Directs Gail Russell; Production in Spurt at 20th
Scheuer, Philip K. Los Angeles Times (1923-Current File)   15 Nov 1960: B7.  MOVIE PRODUCER CITES STAR POWER: Pasternak Has 2 Scripts Prepared for Doris Day -- 3 New Films Today
By EUGENE ARCHER. New York Times (1923-Current file)   19 Oct 1960: 55. 

Pasternak also announced plans to reunite Hamilton, Prentiss, Hutton and Mimieux in a romantic comedy Only a Paper Moon from a story by George Bradshaw, "Image of a Starlet". FOX ABANDONING CLEOPATRA SET: Studio Will Film Spectacle in Hollywood or Rome -- Comedy-Fantasy Opens
By EUGENE ARCHER. New York Times (1923-Current file)   16 Mar 1961: 44.   This became A Ticklish Affair, and was made, but without any of those actors. Swift Comes Back for Loot, Not Art
Smith, Cecil. Los Angeles Times (1923-Current File)   29 Nov 1963: C32.  
 Bachelor in Come Fly With Me and Follow the Boys.

It also inspired a number of imitations from other studios including the Beach Party series and Palm Springs Weekend.

==1984 film== 1984 by TriStar Pictures. While it bears the distinction of being the first film released by TriStar, the film was a critical and commercial failure. Although it was touted as a remake, Roger Ebert reported that "It isnt a sequel and isnt a remake and isnt, in fact, much of anything." 

== References ==

 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 