Anything to Survive
 
{{Infobox film
| name           = Anything to Survive
| image          = Anything to survive.jpg
| image size     = 
| alt            = 
| caption        = Promotional Photo
| director       = Zale Dalen 
| producer       = Warren Carr
| writer         = Jonathan Rintels
| screenplay     = 
| story          = 
| based on       = Almost Too Late by Elmo Wortman 
| narrator       = 
| starring       = Robert Conrad,  Emily Perkins,  Matt LeBlanc
| music          = 
| cinematography = 
| editing        = 
| studio         =  ABC
| released       = February 5, 1990 (USA)  
| runtime        = 91 mins
| country        = USA 
| language       = English 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Anything to Survive, also called Almost Too Late, is a 1990 American adventure film directed by Zale Dalen and starring Robert Conrad, Matt LeBlanc and Emily Perkins. It is loosely based on the true story of the Wortman family of British Columbia. 
  
==Plot==
The Barton family; siblings Wendy, Krista and Billy and their father Eddie become stranded on a beach while sailing from Prince Rupert to their home in Ketchikan, Alaska. The plot concerns the family going to extreme lengths to survive after they get stranded on an island in a storm and lose their boat. Billy builds a raft and they attempt to sail to safety but they dont get far. Eddie and Billie head off to seek help while the two daughters try to keep warm under the remains of their boats sail.  Eddie and Billy find their way to a cabin on the mainland but cant raise help. On day 23 they are able to return to the island and find that the girls are alive. Eddie and Wendy have frostbitten feet but all in the family survive and they are able to return to Ketchikan.

==Cast==

*Robert Conrad, Eddie
*Emily Perkins, Krista
*Matt LeBlanc, Billy
*Ocean Hellman, Wendy Tom Heaton, Dave 
*William B. Davis, Dr. Reynolds

==Reception and nominations==

Anything to Survive is a made for television film, although the film did have some half-price screenings at limited cinemas in the United Kingdom, including one at London IMAX. The film was nominated for a Gemini Award in 1990 for Best Performance by a Supporting Actress (Ocean Hellman).

==External links==

     
 
 
 
 
 