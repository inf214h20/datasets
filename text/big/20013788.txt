Jelenin svet
{{Infobox film
| name           = Jelenin svet
| image          = JJ-Filmposter.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical poster
| director       = Tanja Brzaković
| producer       = Tanja Brzaković Nebojsa Miljkovic
| writer         = Tanja Brzaković
| starring       = Jelena Janković Snežana Janković Ricardo Sánchez
| music          = Janja Lončar
| cinematography = Miona Bogovic Kathleen Herbst
| editing        = Tatjana Brzakovic Branka Pavlovic 
| studio         = Talas film
| distributor    = Serbian theatrical: Art Vista Worldwide: WTA Tour
| released       =  
| runtime        = 80 minutes
| country        = Serbia Germany Serbian Subtitle: English Russian
| budget         = $40,000
| gross          = 
}}
 independent documentary World No. 1 female tennis player, Jelena Janković.  

==Background==
The film follows Jelena Janković over a 14-month period, and includes tennis tournaments in Madrid and Berlin, as well as her visits to her home in Belgrade.      

At the beginning of the documentary, Janković was ranked as third best tennis player in the world.  The film follows her regime as she prepares for various meets, deals with maintaining her diet, trains, meets with fans, and begins her matches. 
   

==Cast==
* Jelena Janković as herself
* Snežana Janković as herself (Jelenas mother)
* Ricardo Sánchez as himself (Jelenas coach)
:In tennis matches:
* Anna Chakvetadze
* Elena Dementieva
* Justine Henin
* Ana Ivanovic
* Svetlana Kuznetsova

==Reception==
When the film premiered in Belgrade on November 12, 2008,  it outsold the James Bond film Quantum of Solace which opened there that same weekend,     bumping the Bond film to second place in the Serbian box office.      

In speaking toward the film, Politika made note that full-length theatrically released documentary films toward Serbian athletes are rare. The appreciated that Jelenin svet celebrated the efforts of one of the best among the best in the world, and that the film was able to document Jelena Jankovićs rise from third-best to worlds best.  The wrote that the film is dynamic, witty and cheerful in it portrait of Janković, and that it allows viewers to better understand the subject of the film.   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 