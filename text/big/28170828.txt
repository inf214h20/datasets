Nivedyam (2007 film)
{{Infobox Film
| name           = Nivedyam
| image          = Nivedyam.jpg
| image_size     = 
| caption        = 
| director       = A. K. Lohithadas
| producer       = Omar Sherif
| writer         = A. K. Lohithadas
| narrator       = 
| starring       = Vinu Mohan Bhama
| music          = M. Jayachandran
| cinematography = Sajan Kalathil
| editing        = Raja Muhammed
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Nivedyam is a 2007 Malayalam romance film written and directed by A. K. Lohithadas. It marked the debut of Vinu Mohan and Bhama in films.

== Synopsis ==
Nivedyam tells the life of Mohana Krishnan (Vinu Mohan) who is an outspoken Brahmin guy who is presently into carpentry work to keep his family composed of his widowed mother and sister. He is the son of C .K, a famous dramatist, who fought for the rights of writers.

Even after his fathers death, Mohana Krishnan managed to study all arts and crafts till his Plus Two, but as his familys prospects fell on his shoulders, he left his traditions to find better jobs to maintain his family intact.

Forwarded by one of his fathers friend lyricist Kaithapram, who happens to meet him in a composing session, Mohana Krishnan arrives at the Kovilakom, headed by Ramavarma Thampuram, a pious aged man who was a dear student of Chembai Vaidhya Natha Bhagavathar.

== Cast ==
* Vinu Mohan as Mohana Krishnan
* Bhama as Sathya Bhama
* Nedumudi Venu
* Bharath Gopi
* MB.Padmakumar as Surendran 
* Sreehari
* Aparna Nair
* Kochupreman
* Soumya Sathish
* Biju Babu
* Vaijayanthi
* Lakshmipriya

==Reception==
Critical reception of the film was not positive. Reviewers praised the acting by Bharath Gopi, but more that of Bhama. The storyline was described as "out of control" and "tries our patience".  However, in 2010 it was announced that it would be remade in Kannada language. 

==Soundtrack==

Nine songs of the movie were composed by M. Jayachandran. The songs were penned by Bichu Thirumala, A.K. Lohithadas, Kaithapram Damodaran Namboothiri & C.J. Kuttappan.

{{Infobox album Name = Nivedyam Type = soundtrack Artist = M. Jayachandran Cover =  Background = Gainsboro Released = 2007 Genre = Film soundtrack Length = 32:23 Last album = Kadha Parayumbol (2006) This album = Nivedyam (2006) Next album = Kanakasimhasanam (2007)
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyricist || Length (min:sec)
|-
| 1|| "Kolakkuzhal Vili Ketto" || Vijay Yesudas & Shweta Mohan || Kaithapram Damodaran Namboothiri || 04:32
|-
| 2|| "Chittattin Kaavil" || Shankaran Namboothiri || Bichu Thirumala || 04:23
|-
| 3|| "Kaayampoovo" || Sudeep Kumar & K.S. Chitra || Kaithapram Damodaran Namboothiri || 04:14
|-
| 4|| "Krishna Nee Begane" || Sudha Ranjith || Traditional Vyaasaraaya Theertha || 02:58
|-
| 5|| "Thaam Thakkida" || Pradeep Palluruthy || C.J. Kuttappan || 04:17
|-
| 6|| "Hey Krishna" || M. Jayachandran || Kaithapram Damodaran Namboothiri || 04:11
|-
| 7|| "Alaipayuthe" || Krishnakumar & Shweta Mohan || Traditional Oottukkadu Venkata Kavi || 03:31
|-
| 8|| "Alaipayuthe" || Krishnakumar || Traditional Oottukkadu Venkata Kavi || 01:56
|-
| 9|| "Lalithalavanga" || Sudeep Kumar & Shweta Mohan || Traditional Oottukkadu Venkata Kavi, Jayadevar || 02:20
|}

== References ==
 

==External links==
*  
* http://www.nowrunning.com/movie/4111/malayalam/nivedyam/index.htm
* http://popcorn.oneindia.in/title/1697/nivedhyam.html


 
 