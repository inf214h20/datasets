A Spray of Plum Blossoms
{{Infobox film
| image          = A Spray of Plum Blossoms.jpg
| caption        = 
| name           = A Spray of Plum Blossoms
| writer         = Huang Yicuo
| starring       = Ruan Lingyu Wang Cilong Jin Yan
| director       = Bu Wancang
| cinematography = Huang Shaofen
| producer       = Luo Mingyou
| studio         = Lianhua Film Company
| distributor    = United States: Cinema Epoch (DVD)
| runtime        = 100 min
| released       = 1931 China
| English intertitles
| budget         =
|
}}
A Spray of Plum Blossoms ( ) is a 1931 silent Chinese film directed by Bu Wancang and starring Ruan Lingyu, Wang Cilong and Jin Yan. It is a loose adaptation of William Shakespeares The Two Gentlemen of Verona. The film is one of several collaborations between Bu Wancang and two of the top Chinese movie stars of the day Ruan Lingyu and the Korean-born Jin Yan and was produced by the Lianhua Film Company. 

The film is noted for its attempted "Westernized stylings" including its surreal use of decor, women-soldiers with long hair, etc. The film also had English-subtitles, but as some scholars have noted, since few foreigners watched these films, the subtitles were more to give off an air of the West rather than to serve any real purpose. Pang, Laikwan, Building a New China in Cinema: The Chinese Left-Wing Cinema Movement, (New York: Rowman & Littlefield, 2002), 26.  

==Cast==
*Ruan Lingyu as Hu Zhilu (Julia)
*Jin Yan as Hu Luting (Valentine)
*Wang Cilong as Bai Lede (Proteus)
*Lin Chuchu as Shi Luohua (Silvia)
*Gao Zhanfei as Liao Diao (Tiburio) Chen Yanyan as A Qiao (Lucetta)
*Liu Jiqun as Fatty Liu
*Wang Guilin as General Shi
*Shi Juefei as Li Yi, the chief bandit

==Plot==
The film tells the story of Bai Lede (Wang Chilong) and Hu Luting (Jin Yan), two military cadets who have been friends since they were children. After graduating, Hu, a playboy uninterested in love, is appointed as a captain in Guangdong and leaves his home town in Shanghai. Bai however, deeply in love with Hus sister, Hu Zhuli (Ruan Lingyu) stays behind. At Guangdong, Hu falls in love with the local generals daughter, Shi Luohua (Lin Chuchu), although the general, Shi (Wang Guilin), is unaware of the relationship, and instead wants his daughter to marry the foolish Liao Diao (Gao Zhanfei). Meanwhile, Bais father uses his influence to get Bai posted to Guangdong, and after a sorrowful farewell between himself and Zhuli, he arrives at his new post and instantly falls in love with Luohua. In an effort to have her for himself, Bai betrays his friend, by informing General Shi of his daughters plans to elope with Hu, leading to Shi dishonourably discharging Hu. Bai tries to win Luohua over, but she is uninterested, only concerned with lamenting the loss of Hu. In the meantime, Hu encounters a group of bandits who ask him to be their leader, to which he agrees, planning on returning for Luohua at some point in the future. Some time passes, and one day, as Luohua, Bai and Liao are passing through the forest, they are attacked. Luohua manages to flee, and Bai pursues her into the forest. They engage in an argument, but just as Bai seems about to lose his temper, Hu intervenes, and he and Luohua are reunited. General Shi arrives in time to see Liao flee the scene, and he now realises that he was wrong to get in the way of the relationship between Hu and his daughter. Hu then forgives Bai his betrayal, and Bai reveals that he has discovered that his only true love is in fact Zhuli back in Shanghai.

==DVD release==
A Spring of Plum Blossoms was released on DVD region code|all-region DVD by Cinema Epoch as a packaged disc (along with Shi Dongshans Two Stars in the Milky Way). The disc was released on September 11, 2007.

==See also==
*One Plum Blossom

==References==
 

==External links==
*  
*   from the Chinese Movie Database (incorrectly referred to as The Peach Girl)
*   collection of Chinese and other foreign films

 

 
 
 
 
 
 
 
 