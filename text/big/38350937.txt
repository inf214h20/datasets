Grudge Match (film)
 
 
{{Infobox film
| name           = Grudge Match
| image          = Grudge Match Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Peter Segal
| starring       = {{Plain list|
* Robert De Niro
* Sylvester Stallone Kevin Hart
* Alan Arkin
* Kim Basinger
* Jon Bernthal
}}
| producer       = {{Plain list|
* Bill Gerber
* Mark Steven Johnson
* Michael Ewing
* Peter Segal
* Ravi Mehta
}}
| screenplay     = {{Plain list| Tim Kelleher
* Rodney Rothman
}} Tim Kelleher
| music          = Trevor Rabin
| cinematography = Dean Semler
| editing        = William Kerr
| studio         = Gerber Pictures Callahan Filmworks
| distributor    = Warner Bros. Pictures
| runtime        = 113 minutes
| country        = United States
| language       = English
| released       =  
| budget         = $40 million   
| gross          = $44,907,260 
}}
Grudge Match is a 2013 sports comedy film starring Robert De Niro and Sylvester Stallone as aging boxers stepping into the ring for one last bout. Stallone and De Niro have both previously been in successful boxing films (Rocky and Raging Bull, respectively) and worked together in Cop Land. The film is directed by Peter Segal. It was previously scheduled for a January 10, 2014 release, but was moved up to December 25, 2013.  The movie was released on January 24, 2014 in the United Kingdom. 

==Plot==

In their prime, Pittsburgh boxers Henry "Razor" Sharp (Sylvester Stallone) and Billy "The Kid" McDonnen (Robert De Niro) become rivals after two fights, one in which Kid beats Razor and one in which Razor beats Kid, the only defeats of their careers. Before they have a rematch, Razor announces his retirement without explanation, infuriating Kid and costing them a big payday.

Years later, Razor is low on money and working in a shipyard when hes visited by promoter Dante Slate, Jr. (Kevin Hart), who wants Razor to provide a motion capture performance for a video game. It was Slates father whose shady business dealings ended up leaving Razor in dire financial straits. The soft-spoken Razor wants no part of this stunt. Kid, a showoff who runs a bar and a car dealership, is all for it.
 Dancing with the Stars.

At the recording studio, Razor is surprised and taunted by Kid, who was also invited by Slate. The two get into a fight and damage the studio before being arrested. Cellphone footage of the fight is uploaded on YouTube and goes viral, giving Slate the idea of organizing a final grudge match between Razor and Kid, which he will promote as "Grudgement Day."

Kid eagerly accepts. Razor is forced to do so as well, not receiving his $15,000 and then learning he has been fired from the shipyard. At the press conference to announce the grudge match, Razor is approached by his ex-girlfriend Sally Rose (Kim Basinger), who cheated on him with Kid during their youth and ended up becoming pregnant. Now widowed, Sally wants to reconnect with Razor, but he is reluctant.

Razor asks old trainer Lightning to get him back in shape. Kid expects gym owner Frankie Brite (LL Cool J) to train him, but Frankie mocks the fight and is of little help. Kid is approached by his estranged biological son, B. J. (Jon Bernthal), against Sallys wishes. The two begin to bond after B. J. gives Kid helpful advice regarding his technique, and is invited to be his trainer.

Lightning convinces Razor to forgive Sally. He also finds out Razor is blind in one eye. A fight could cause permanent damage, so Lightning and Sally both implore Razor to call it off. At first he agrees, enraging Kid, but ultimately Razor explains that he needs to do this, Kid having cost him what he loved most, boxing and Sally.

Kid takes his grandson Trey to celebrate the news that the Grudge Match is sold out. B. J. believes theyre going to a movie, but Kid takes the child to a bar, then leaves him on his own while hooking up with a groupie. Trey ends up accidentally starting Kids car while hes in the backseat having sex. Kid prevents an accident, but he is arrested, and B. J. is infuriated that he endangered the boy. Kid apologizes to B. J. and gives him a scrapbook that he kept of B. J.s sports career in school, proving he hadnt been uninterested in him throughout the years. B. J. forgives him.

The Grudge Match arrives, televised and packed. Kid gains the upper hand, severely beating Razor by unknowingly exploiting his blind eye. Upon learning of Razors condition, however, Kid stops focusing on the eye and helps Razor to his feet. Razor turns the fight to his favor, but likewise helps Kid on his feet after nearly knocking him out. The fight ends and the judges decide it on points. Razor is declared the winner by a very close split decision. He celebrates with Sally and Lightning, while a satisfied Kid enjoys the company of B. J. and Trey, who are proud of what he has accomplished.

In a post-credits scene, Razor has a new TV, on which he and Lightning view a performance by Kid on  Dancing with the Stars and quickly deduce that hes having sex with his partner. In another, Slate tries to set up another grudge match between Mike Tyson and Evander Holyfield. Holyfield refuses several increasingly higher offers to fight Tyson before showing interest when Slate offers Holyfield a role in a fourth The Hangover (film series)|Hangover film. Tyson, frustrated by this, approaches Slate angrily before the scene ends.

==Cast==
 
 
 
*Robert De Niro as Billy "The Kid" McDonnen
*Sylvester Stallone as Henry "Razor" Sharp
*Kevin Hart as Dante Slate, Jr.
*Alan Arkin as Louis "Lightning" Conlon
*Kim Basinger as Sally
*Jon Bernthal as B. J.
*LL Cool J as Frankie Brite
*Anthony Anderson as Mr. Sandpaper Hands
* Camden Gray as Trey
  Ireland Basinger Baldwin, appears as Young Sally.

==Production== New Orleans, Louisiana, in late 2012 and wrapped in March 2013   while several different establishing shots of Downtown Pittsburgh and the Edgar Thomson Steel Works were shot in the Pittsburgh metropolitan area.    A trailer was released on September 12, 2013.   The climatic fight scene was filmed on location at the Consol Energy Center in Pittsburgh. 

==Critical reception==
Grudge Match has received generally negative reviews. On  , it has a score of 35 out of 100, based on 32 critics, indicating "generally unfavorable reviews".  

Peter Bradshaw of The Guardian called the film "A preposterous, worthless mediocrity. ...  Never have I yearned more passionately to climb into my time machine and journey back to before my memory of Raging Bull was needlessly trashed by this incredibly depressing and worthless mediocrity". 
 Escape Plan), but he lost to Jaden Smith for After Earth. (It was Stallones 14th bid for the "prize", a record, which he has "won" four times, also a record.)

==Box office==
Grudge Match grossed only $7 million in its opening weekend, finishing in 11th place at the box office. Made on a $40 million budget, the film made only $45 million worldwide, making it a box office disappointment. 

==References==
 

==External links==
*  
*  
*  
*   (unofficial)

 

 
 
 
 
 
 
 
 
 
 