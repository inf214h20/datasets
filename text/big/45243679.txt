The Truthful Liar
{{Infobox film
| name           = The Truthful Liar
| image          = The Truthful Liar (1922) - 1.jpg
| alt            = 
| caption        = Still with Wanda Hawley and Guy Edward Hearn (standing)
| director       = Thomas N. Heffron
| producer       =  Percy Heath Will J. Payne 
| starring       = Wanda Hawley Guy Edward Hearn Charles A. Stevenson Casson Ferguson Lloyd Whitlock George Siegmann E. Alyn Warren
| music          = 
| cinematography = William E. Collins 	
| editing        = 
| studio         = Realart Pictures Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 mystery silent Percy Heath and Will J. Payne. The film stars Wanda Hawley, Guy Edward Hearn, Charles A. Stevenson, Casson Ferguson, Lloyd Whitlock, George Siegmann, and E. Alyn Warren. The film was released on April 23, 1922, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Wanda Hawley as Tess Haggard
*Guy Edward Hearn as David Haggard
*Charles A. Stevenson as Harvey Mattison
*Casson Ferguson as Arthur Sinclair
*Lloyd Whitlock as Larry Steffens
*George Siegmann as	Mark Potts
*E. Alyn Warren as Peteer Vanetti 
*Charles K. French as Police Commissioner Rogers

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 

 
 