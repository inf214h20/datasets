La nouvelle guerre des boutons
 
{{Infobox film
| name           = La nouvelle guerre des boutons
| director       = Christophe Barratier
| producer       = Thomas Langmann Daniel Delume Emmanuel Montamat
| writer         = Stéphane Keller Christophe Barratier Thomas Langmann
| starring       = Guillaume Canet, Laetitia Casta
| music          = Philippe Rombi
| cinematography = Jean Poisson
| editing        = Anne-Sophie Bion Yves Deschamps	
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
}}
La nouvelle guerre des boutons (War of the Buttons) is a 2011 French film directed by Christophe Barratier. 

==Plot==
The story takes place in March 1944 in a small French village. The children from the neighbouring villages of Longeverne and Velrans have been waging this merciless war as long as anyone can remember: the buttons of all the little prisoners clothes are removed so that they head home almost naked, vanquished and humiliated. Consequently this conflict is known as the "War of the Buttons." The village that collects the most buttons will be declared the winner. Meanwhile, Violette, a young Jewish girl, has caught the eye of Lebrac, the chief of the Longeverne kids.

==Cast==
* Laetitia Casta as Simone
* Guillaume Canet as the teacher
* Kad Merad as Father Lebrac
* Gérard Jugnot as Father Aztec
* Jean Texier as Lebrac
* Clément Godefroy as Petit (Little) Gibus
* Marc-Henri Wajnberg as Vladimir
* Théophile Baquet as Grand (Big) Gibus
* François Morel (actor)|François Morel as Father Bacaillé
* Louis Dussol as Bacaillé
* Harold Werner as Crique
* Nathan Parent as Camus
* Ilona Bachelier as Violette
* Thomas Goldberg as the Aztec

==Discography==

The CD soundtrack composed by Philippe Rombi was released on Music Box Records label.

==References==
 

==External links==
*  

 
 
 
 
 


 