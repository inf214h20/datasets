The Man Who Loved Yngve
{{Infobox film
| name           = The Man Who Loved Yngve
| image          = TheManWhoLovedYngve.jpg
| caption        = Theatrical poster
| director       = Stian Kristiansen
| writer         = Tore Renberg
| starring       = Rolf Kristian Larsen Arthur Berning Ida Elise Broch  Ole Christoffer Ertvåg
| music          = John Erik Kaada
| cinematography = Trond Høines
| editing        = Vidar Flataukan
| runtime        = 90 minutes
| country        = Norway Norwegian
|}}

The Man Who Loved Yngve ( ) is a Norwegian film released on 15 February 2008. It is based on a book of the same name by Stavanger author Tore Renberg. It received critical acclaim as one of the best Norwegian movies of the year.
A sequel - I Travel Alone - was released in 2011  and a prequel - The Orheim Company - followed in 2012.

== Plot ==
In 1989, and in the shadow of the collapse of Communism in Europe, a group of young rural Norwegians form a band. Preparations for their first gig are derailed when the lead singer, Jarle, is smitten by a new arrival, Yngve. Confused and not completely in touch with his own emotions, Jarle neglects his band, his mother and his girlfriend to spend more time with his new crush. At a party after the concert, he lashes out at Yngve but also admits he loves him. Yngve becomes depressed and flees to a bridge with the intention of committing suicide, but decides not to. He ends up in a mental hospital, and stays there until Jarle sees him again, few weeks after the incident.

== Production details ==
* Production Company: Motlys A/S
* Producer: Yngve Sæther
* Writer: Tore Renberg
* Distributor: Sandrew Metronome
* Filming start date: 12.02.07
* Filming end date 30.03.07
* Director: Stian Kristiansen Kaada and Geir Zahl NKR

== Cast ==
* Jarle - Rolf Kristian Larsen
* Helge - Arthur Berning
* Yngve - Ole Christoffer Ertvaag
* Katrine - Ida Elise Broch
* Oljeungen - Erlend Stene
* Jarles father - Jørgen Langhelle
* Andreas - Knut Sverdrup Kleppestø

== References ==
 

== External links ==
*  

 
 
 
 
 

 