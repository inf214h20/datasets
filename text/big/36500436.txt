Branded (2012 film)
{{Infobox film
| name           = Branded
| image          = Branded US film poster.jpg
| caption        = Theatrical release poster
| director       = Jamie Bradshaw Aleksandr Dulerayn
| writer         = Jamie Bradshaw Aleksandr Dulerayn
| starring       = Ed Stoppard  Jeffrey Tambor Max von Sydow Leelee Sobieski
| music          = Edward Artemyev
| cinematography = Roger Stoffers
| editing        = Michael Blackburn TNT
| distributor    = Roadside Attractions Freestyle Releasing 
| released       =  
| runtime        = 106 minutes
| country        = Russia United States
| language       = English Russian
| budget         = 
| gross          = $3,754,070
}}

Branded (also known as The Mad Cow and Moscow 2017 (Москва 2017 in Russian)) is a 2012 Russian–American science fiction film written and directed by Jamie Bradshaw and Aleksandr Dulerayn. It was released on September 7, 2012.

== Plot ==
 
As the movie begins, the names of famous visionaries including Joan of Arc, Albert Einstein, and Alexander the Great flash on the screen. A caption reads, "All of them saw things others didnt see. All of them changed the world."
 Soviet Russia, young Misha Galkin is in a public park at night looking at the stars. Suddenly the stars shift into the outline of a cows head, which turns and looks at him. Moments later, Misha is struck by lightning. A woman examines him and, seeing that he is still alive, predicts that his life will not be ordinary.

In present-day Russia, Misha (  invented modern marketing, and Communism was the first true global brand.
 fat the new fabulous". An unseen voice-over narrator says the companies agreed to carry out the gurus plan.

In a series of documentary-style flashbacks, narrated by the same unseen narrator, we see how Misha used his natural marketing savvy to rise from a poor clerk to a marketing executive. Mishas big break came when he met Bob, an American hired to spread Western brands and businesses in post-Communist Russia. In the present, Bob discovers Abbys relationship with Misha and is furious.

Misha is hired to do marketing for a new reality TV show, Extreme Cosmetica, in which an overweight girl will undergo extensive plastic surgery to become skinny and beautiful. Everything goes wrong when, after the first operation, the girl falls into a coma. The public turns against the show, and the glorification of skinny body types in general, and Misha, as the shows marketer, becomes the scapegoat. He is swarmed by protesters, beaten by police, and arrested. When released from jail, he angrily confronts his former partner Bob, saying hes realized the truth: the show and the coma were orchestrated to induce Abby and Misha to split up. Bob denies it (saying: "it would take millions of dollars to manipulate public opinion that way, and it would take the greatest assassin in the world to fake the operation to put that girl in a coma!"). Later, at a bar, they get into a fight, and then Bob has a heart attack.

Full of guilt about the "Extreme Cosmetica" girls fate, Misha realizes "his marketing powers are a curse" (as explained by the narrator). He leaves Moscow, and withdraws from modern society. Six years later, Abby tracks him down to a rural community where Misha is living the simple life as a cowherd. While Abby is visiting, Misha has a strange dream. In a dreamlike state, he performs the Red Heifer ritual, sacrificing a red cow and bathing in its ashes. When he wakes up from the ritual cleansing, he discovers to his horror that he has developed the ability to see strange eel- or blob-like creatures which cling to peoples necks and appear to be the embodiment of marketing brand desires.

Abby takes Misha to her apartment in Moscow, where she reveals that she is rich (due to an inheritance from Bob), and they have a six-year-old son. In the intervening six years, the "fat is fabulous" campaign has changed society; everyone is overweight, and images of fat people are used in advertising everywhere. Their son is also overweight and loves "The Burger" and other junk-food brands. Distressed by his grotesque visions no one else can see and disgusted by the rampant commercialism around him, Misha impulsively trashes Abbys apartment. Frightened by his behavior, Abby leaves Misha and takes their son with her.

Misha develops a plan to fight back against the branding-creatures using their own methods. Going back to his old company, he accepts a job to do marketing for Dim Song, a vegetarian Chinese restaurant chain. At the meeting with the executives, he perceives tentacles growing out of their necks connecting them to the collective Dim Song corporate-branding entity. Using dialogue which parallels Pascals speech to the fast food executives earlier in the movie, he promises to fix Dim Songs problems. Mishas solution is to cause a fake anti-beef scare (using the publics fear of a mysterious virus similar to Bovine spongiform encephalopathy a.k.a. Mad cow disease) which will frighten people from eating meat, thus turning them towards vegetarian food.

The anti-beef scare works, and burger sales drop precipitously. From the rooftop of a building, Misha watches a dragon-like entity hatch from an egg on top of the Dim Song building and fly towards The Burger restaurant, ripping apart and killing The Burgers corporate embodiment. Misha predicts that The Burger will go bankrupt within a week, and his prediction comes true. Back on the Polynesian island, the marketing guru tells the distressed fast food executives they are in trouble, but there is still a way to save their brands. Before he can tell them his new plan, however, he is vaporized by a bolt of lightning.

Misha continues his plan to destroy the worlds major brands by using fear-based marketing to make customers afraid of them one by one. In a CG sequence, the brand creatures fly over the city attacking and killing one another: "Yepple" killing "GiantSoft", etc. Public opinion turns against marketing in general, and the Russian parliament considers a bill banning all advertising. Depressed and alone in his corporate office, Misha leaves a message on Abbys cellphone, requesting her forgiveness. At that very moment, Abby shows up. Suddenly, the building is raided by anti-advertising protesters, who smash through the doors and assault the employees. Misha is struck down while he and Abby try to escape. At that moment, an emergency broadcast plays on TV, saying that Russia and the other nations of the world have agreed to ban all advertising. The protesters stop their rampage, but Misha is already lying on the floor, bleeding from a head wound.

Some time later, all advertising has been banned, the Moscow skyline is free from billboards, and bulldozers are crushing old advertising materials in the dump. In the hospital, Misha has awakened with a bandage on his head and is playing with Abby and his son. In another room in the same hospital, the "Extreme Cosmetica" girl awakens from her coma and wanders out into the advertising-free city streets. The voice-over narrator explains that thanks to Misha, the world was changed forever. The camera pans up into the night sky and reveals that the narrator is the cow constellation young Misha saw at the beginning of the movie.

== Cast ==
* Ed Stoppard as Misha Galkin
* Leelee Sobieski as Abby Gibbons
* Max von Sydow as Joseph Pascal
* Jeffrey Tambor as Bob Gibbons
* Ingeborga Dapkunaite as Dubcek
* Rachel Davies as the Cow

== Release ==
The film was released in America on September 7, 2012 in 310 theaters. 

=== Critical response ===
The film was not screened for critics and has received predominantly negative reviews. Charlie Jane Anders of io9.com declared that "everything youve heard about Branded was false advertising," complaining that the trailers made the film appear to be "a weird, surrealistic version of They Live" but "unfortunately, instead of a fun monster movie, Branded is a truly dreary lecture on late-stage capitalism, in which logic basically goes out the window."  Robert Abele of the Los Angeles Times described the film as "convoluted and pretentious... so packed with ideological pretension and forced whimsy it has no time for characterization or cohesion, despite its scrappy use of post-Communist Russia as ground zero for capitalisms next nightmare scenario."  Lucius Shepard, admitting he did not understand the story fully, wrote that he wanted to sue the filmmakers "for defamation of the senses," adding, "Not since Mystery Science Theater 3000|MST3K went off the air have I watched a movie so lacking in basic competence and craft." He acknowledged, "Branded is also a satire, though it undergoes drastic and abrupt shifts in tone that suggest a more dramatic production."  In one of the few positive reviews, Andy Webster of the New York Times called the film "ambitious", observing that "Madison Avenue is going to hate Branded." 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 