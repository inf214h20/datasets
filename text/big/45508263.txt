The Bishop of the Ozarks
 

{{Infobox film
| name           = The Bishop of the Ozarks insert a caption here
| alt            = 
| caption        = Advertisement in Motion Picture News (1923)
| film name      = 
| director       = Finis Fox
| producer       = Milford W. Howard
| writer         = 
| screenplay     = Finis Fox
| story          = Milford W. Howard
| based on       = 
| starring       = Milford W. Howard Derelys Perdue Cecil Holland William Kenton
| narrator       = 
| music          = 
| cinematography = Sol Polito
| editing        = 
| studio         = Cosmopolitan Film Company 
| distributor    = Film Booking Offices of America
| released       = February 1923
| runtime        = Six reels
| country        = United States Silent
| budget         = 
| gross          = 
}}
The Bishop of the Ozarks is a 1923 American drama silent film directed by Finis Fox. The film is based on a story by Milford W. Howard, who both produced and starred in the feature. The film was distributed by Film Booking Offices of America, commonly referred to as FBO. The survival status of the film is presumed Lost film|lost.   

==Synopsis==
 

==Cast==
*Milford W. Howard - Roger Chapman and Tom Sullivan
*Derelys Perdue - Margery Chapman
*Cecil Holland - Dr. Earl Godfrey
*William Kenton - Dr. Paul Burroughs
*R.D. MacLean - Governor of Alabama
*Mrs. Milo Adams - Shepherd woman
*Rosa Melville - Mrs. Jack Armstead
*Fred Kelsey - Mart Stoneman George Reed - Simon

==Background==
Milford W. Howard, who wrote and produced the film, was an United States Representative from Alabama from 1894-1898, before moving to California in 1918. While still in Alabama, Howard wrote If Christ Came to Congress (1894), an exposé of corruption, published again in 1964,    and The American Plutocracy (1895), a novel about "two classes of people...the excessively rich and the abject poor".    According to the Cedar Rapids Tribune (April 6, 1923), the screenplay was due to be published as a novel following the picture’s release.  Howard returned to Alabama in 1923, and after the death of his first wife, he re-married and traveled to Europe where he interviewed Benito Mussolini of Italy. The interview altered his political philosophy, causing him to endorse fascism.  Howards last book published was Facism: A Challenge to Democracy, in 1928.    

The Bishop of the Ozarks was the only film produced by the Cosmopolitan Film Company, not to be confused with Cosmopolitan Films or Cosmopolitan Productions. Copyright documents on file at the Library of Congress show the film was registered by the R-C (Robertson-Cole) Pictures Corporation, which has been suggested was the holding company of Cosmopolitan. 
 The Wizard of Oz (1939). Holland would later head up the makeup department at Metro-Goldwyn-Mayer, and also authored the book, The Art of Make-up for Stage and Screen.    

==Reviews and reception==
A review in Moving Picture World at the time said of the film, "mysticism has been resorted to in several instances. There is a spiritual seance, a persistent strain of mental telepathy and a definite instance in which the occult powers of evil are demonstrated".    The Exhibitors Herald opined that the film was "an odd mixture of prison reform, second sight and crook reformation", and noted that Milfords acting was "stilted and unreal and at no time does it get over very forcibly".  A review of the film in the 1923 Exhibitors Trade Review said the feature was "an extremely entertaining picture...in spite of the fact that films which suggest a religious atmosphere are seldom viewed with favor by the average citizen in search of amusement...in the present instance there is no propaganda developed regarding any particular belief, no attempt at preaching, merely a thoroughly human story, shot through with trenchant dramatic punches and, if there is a moral to be traced, it is simply that it pays in the long run to do the right thing and keep your nerve intact". 

==See also==
*Lost film
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 