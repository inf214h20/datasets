Flight of the Lost Balloon
{{Infobox film
| name           = Flight of the Lost Balloon
| image          = Fotlbpos.jpg
| image_size     = 
| caption        = Original film poster
| director       = Nathan Juran
| writer         = Nathan Juran
| narrator       = 
| starring       = Mala Powers
| music          = 
| cinematography = Jacques Marquette
| editing        = 
| studio         = Woolner Brothers
| distributor    = 
| released       = December 28, 1961
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Flight of the Lost Balloon is a 1961 film produced, written and directed by Nathan Juran that was filmed in Puerto Rico.  It stars Mala Powers and Marshall Thompson  with the working title being Cleopatra and the Cyclops. 

The film was inspired by Jules Vernes Five Weeks in a Balloon and beat the major Irwin Allen film release of the book to the cinemas.  However the name of Jules Verne was dropped and was nowhere to be found in the credits, though the name of Vernes balloon, the Victoria remained. 

As a promotional gimmick people who bought tickets for the film were given a "motion sickness pill". 

==Cast==
*Mala Powers as Ellen Burton
*Marshall Thompson as Doctor Joseph Faraday
*James Lanphier as The Hindu Douglas Kennedy as Sir Hubert Warrington
*Robert W. Gillette as Sir Adam Burton
*Felipe Birriel as Golan
*Leo Ledbetter as The Witch Doctor (and two other roles) 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 