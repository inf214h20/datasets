Vayasu Pilichindi
{{Infobox film
| name           =  Vayasu Pilichindi
| image          = Vayasu Pilichindi.jpg
| image_size     =
| caption        =
| director       = C. V. Sridhar
| producer       = Kannaiyaa
| writer         = C. V. Sridhar
| narrator       =
| starring       = Kamal Hassan Rajnikanth Sripriya Jayachitra
| music          = Ilaiyaraaja
| cinematography = P. S. Nivas
| editing        = Kottagiri Gopalrao
| distributor    =
| released       =  
| runtime        = 141 min.
| country        = India Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1978 Telugu Telugu   film directed by C. V. Sridhar.  It is the remake version of the Tamil film Ilamai Oonjal Aadukirathu (1978).  The film became a big hit, due to the music.  The director remade the film in Hindi as Dil-E-Nadaan (1982), starring Rajesh Khanna, Shatrughan Sinha, Smita Patil, and Jayapradha, which fethched 4.3 crores at the box office.

==Sound track==
* Ilaage Ilaage Saraagamaadithe (Lyrics: Arudra; Singer: S. P. Balasubrahmanyam and P. Susheela)
* Jeevitha Madhusaala
* Maate Marichave Chilakamma (Lyrics: Arudra; Singer: S. P. Balasubrahmanyam)
* Muthyamalle Merisipoye Mallemogga (Lyrics: Arudra; Singer: S. P. Balasubrahmanyam)
* Nuvvadigindi Enaadaina Kaadhannaana (Lyrics: Arudra; Singer: Vani Jayaram)

==References==
 

==External links==
*  

 

 
 
 
 
 


 