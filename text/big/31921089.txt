Nothing But Lies
{{Infobox film
| name           = Nothing But Lies
| image          = 
| caption        = 
| director       = Paule Muret
| producer       = Bruno Pésery
| writer         = Paule Muret
| starring       = Fanny Ardant
| music          = 
| cinematography = Renato Berta
| editing        = Catherine Quesemand
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = France Switzerland
| language       = French
| budget         = 
}}

Nothing But Lies ( ) is a 1991 French-Swiss drama film directed by Paule Muret. It was entered into the 42nd Berlin International Film Festival.   

==Cast==
* Fanny Ardant as Muriel
* Alain Bashung as Adrien
* Jacques Perrin as Antoine, mari de Muriel
* Stanislas Carré de Malberg as Basile
* Christine Pascal as Lise
* Alexandra Kazan as Jo
* Jean-Pierre Malo
* Dominique Besnehard as Détective

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 