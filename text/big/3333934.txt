Dalagang Bukid
{{Infobox film
| name           = Dalagang Bukid
| image          = File:Dalagangbukid.jpg
| image_size     = 250
| caption        = 
| director       = José Nepomuceno
| producer       = 
| writer         = 
| based on       =  
| narrator       = 
| starring       = Atang de la Rama Marceliano Ilagan
| music          = 
| cinematography = José Nepomuceno
| editing        = 
| studio         = 
| distributor    = Malayan Movies
| released       =  
| runtime        = 
| country        = Philippines
| language       = Silent
| budget         = ₱25,000   
| gross          = ₱90,000 
}}
Dalagang Bukid (English: Country Maiden) is a 1919 Filipino silent film directed by José Nepomuceno. It is the first Filipino feature film to be locally produced in the Philippines. Like all of Nepomucenos works, Dalagang Bukid is now a lost film. 

== Plot ==
Dalagang Bukid is a story about a young flower vendor named Angelita (Atang de la Rama), who is forced by her parents to marry a wealthy old man, Don Silvestre, despite her love for Cipriano, a law student.

Angelitas parents are blind to her reciprocated love for Cipriano, as their shortage of money and consumption by various vices (gambling and cock-fighting) make Don Silvestres offer of marriage attractive. Don Silvestre is an old loan shark who visits cabarets and buys flowers from pretty young florists like Angelita. .

Meanwhile, Angelita and her brothers, who are working as shoe shiners at the threshold of the Church of the Holy cross, maintain the expenses of the house (and their parents vices) with their labor.  Don Silvestre, meanwhile, offers to arrange for Angelita to win a beauty contest in "La Vanguardia" in exchange for her hand in marriage.  Her parents accept the offer and the date of the wedding is set for the next day, which will see the formal crowning of Angelita as the beauty queen.

However, Cipriano, who has just finished his studies, goes to Angelitas house just in time to prevent Angeita from entering Don Silvestres car, which was to take her to the coronation ceremony. Instead of going to the event, Angelita and Cipriano go to the church, where they are wed in.  After the wedding, they go to the coronation event together and announce their union as husband and wife.  Don Silvestre faints upon learning the news.

== Cast ==
*Atang de la Rama
*Marceliano Ilagan

== Production ==
On May 15, 1917, Nepomuceno bought his first film equipment from Albert Yearsley and Edward Meyer Gross. For the next two years, Nepomuceno practiced using the equipment in preparation for making the first locally produced feature film of the Philippines. He decided to adapt Hermogenes Ilagans zarzuela titled Dalagang Bukid, which was successfully performing at the box office at the time. For the casting, he decided to use the original performers of the zarzuela, which included Atang de la Rama and Marceliano Ilagan, the latter being the brother of Hermogenes.

== Release ==
The film was released with English, Spanish, and Tagalog subtitles. During its theatrical run, leading actress de la Rama had to sing Nabasag ang Banga (a song which is a part of the film) for every screening of the film in Manila, along with three others playing a violin, a cornet, and a piano. 

=== Box office ===
Released on  September 12, 1919, it was a major box-office success, earning 90,000 pesos from a budget of 25,000 pesos. 

== Legacy ==
 

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 


 
 