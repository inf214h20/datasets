Footlight Serenade
{{Infobox film
| name           = Footlight Serenade
| image          = Footlightserenade.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Gregory Ratoff
| producer       = William LeBaron
| writer         = Kenneth Earl (story) Robert Ellis (writer) John Payne Betty Grable Victor Mature Jane Wyman
| music          = Charles Henderson
| cinematography = Lee Garmes Robert L. Simpson
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} John Payne and Victor Mature

==Plot==
Tommy Lundy (Victor Mature) is an arrogant ex-champion boxer who tries for an acting career on Broadway. He falls in love with his costar (Betty Grable), whos secretly married to actor John Payne. 

==Cast== John Payne as William J. Bill Smith
* Betty Grable as Pat Lambert
* Victor Mature as Tommy Lundy
* Jane Wyman as Flo La Verne
* James Gleason as Bruce McKay
* Phil Silvers as Slap
* Cobina Wright as Estelle Evans (as Cobina Wright Jr.)
* June Lang as June
* Frank Orth as Mike the stage doorman
* Mantan Moreland as Amos. Tommys Dresser (as Manton Moreland)
* Irving Bacon as Stagehand
* Charles Tannen as Charlie, Stage manager
* George Dobbs as Frank, Dance director

==Soundtrack==
*Except with You (uncredited). Music by Ralph Rainger. Lyrics by Leo Robin. Sung by Cobina Wright
*Are You Kiddin? (uncredited). Music by Ralph Rainger. Lyrics by Leo Robin. Sung and danced by Betty Grable John Payne Hermes Pan
*I Heard the Birdies Sing (uncredited). Music by Ralph Rainger. Lyrics by Leo Robin. Sung and danced by Betty Grable and chorus
*Ill Be Marching to a Love Song (uncredited). Music by Ralph Rainger. Lyrics by Leo Robin. Sung and danced by Betty Grable, Victor Mature, John Payne, chorus
*Living High (uncredited). Music by Ralph Rainger

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 