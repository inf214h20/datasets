My Sweet Orange Tree (film)
 
{{Infobox film
| name           = My Sweet Orange Tree
| image          = My Sweet Orange Tree 2012 poster.jpg
| caption        = Theatrical release poster
| director       = Marcos Bernstein
| producer       = Katia Machado
| writer         = Marcos Bernstein Melanie Dimantas
| starring       = João Guilherme Ávila  José de Abreu  Caco Ciocler
| music          = Armand Amar
| cinematography = Gustavo Hadba
| editing        = Marcelo Moraes
| studio         = Passaro Films
| distributor    = Imovision (Brazil)  Elle Driver (France) Sookie Pictures (South Korea)
| released       =  
| runtime        = 99 minutes
| country        = Brazil
| language       = Portuguese
| budget         = BRL|R$ 3 million 
}}

My Sweet Orange Tree is a 2012 Brazilian drama film, based on the book of the same name.  Directed by Marcos Bernstein and starring João Guilherme Ávila and José de Abreu, is the second film adaptation of the novel by José Mauro de Vasconcelos. The film follows the story of Zezé, a very imaginative but misunderstood boy.

The idea of adapting the novel started in 2004, when at the request of the producer of the movie Katia Machado, Marcos Bernstein and Melanie Dimantas wrote a screenplay. Later, Bernstein offered to direct the film, which was for a period a French-Brazilian production until finished as an exclusively Brazilian film.

After filming between 2010 and 2011 in Minas Gerais, My Sweet Orange Tree was exhibited for the first time in 2012, at the Festival do Rio. The film debuted in theaters in Brazil on April 19, 2013 and received varied reviews, which highlighted João Guilherme Avila as Zezé and Gustavo Hadba photography.

== Plot ==
My Sweet Orange Tree begins with José Mauro de Vasconcelos (Caco Ciocler) receiving an edition of his finished novel. Then, the film starts to tell the story from the writers memories, through flashbacks.  Zezé (João Guilherme Avila), a boy of eight years who lives in Minas Gerais in a very humble house with his family, consisting of his father (Eduardo Dascar), an unemployed and alcoholic, and his mother (Fernanda Vianna), which works to support the home and his brother and sisters. Despite the lack of understanding, affection and the aggression suffered by from his father and school colleagues, the boy has a great skill for storytelling using his imagination.

With financial difficulties, the family has to move. At the new home, Zezé finds an orange tree, which he talks everyday. However, for being extremely extrovert he got involved in several confusions. One of them, he tries to ride on the bumper of Manoel Valadares, the "Portuga" (José de Abreu), but is caught and spanked. The boy feels humiliated and wants revenge, however Valadares ends up understanding Zezé, which turns to share his world of fantasies, and a new friendship arises. 

== Cast ==
* João Guilherme Ávila as Zezé
* José de Abreu as Portuga
* Caco Ciocler as José Mauro de Vasconcelos
* Eduardo Dascar as Paulo (Zezés father)
* Fernanda Vianna as Selma (Zezés mother)
* Pedro Vale as Totoca
* Leônidas José as Luís
* Julia de Victa as Glória
* Kathia Calil as Jandira
* Eduardo Moreira as Ladislau
* Tino Gomes as Ariovaldo

==References==
 

==External links==
*    
*  

 
 
 
 
 
 