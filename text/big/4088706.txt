Alienator
{{Infobox film
| name           = Alienator
| image          = Alienator_DVD_cover.jpg
| alt            = Alienator VHS cover
| caption        = VHS cover for Alienator
| director       = Fred Olen Ray
| producer       = Jeffrey C. Hogue
| writer         = Paul Garson Teagan P. J. Soles
| music          = Jeffrey C. Hogue Dan Q. Kennis Tom Howard
| cinematography = Gary Graver Christopher Roth
| music          = Chuck Cirino
| editing        = Dan Q. Kennis
| studio         = Amazing Movies
| distributor    = Image Entertainment Prism Entertainment Corporation 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Alienator is a 1989 science fiction/Action film|action/Thriller (genre)|thriller, directed by filmmaker Fred Olen Ray—the Film producer|producer, Film director|director, and screenwriter of over one hundred low to medium-budget feature films in many genres—and starring Jan-Michael Vincent, star of the CBS television series Airwolf. Alienator is among several films produced by Jeffrey C. Hogue.

==Plot== spaceship into American suburb. The commander (Jan-Michael Vincent) of the spaceship dispatches The Alienator (Teagan Clive) -- a deadly female gynoid, to capture Kol (Ross Hagen). He meets up with some teenagers as they are all running from fiery death at the hands of The Alienator. She relentlessly pursues Kol and the teens through a series of action sequences and low budget special effects. Kol must face the toughest decision of his life: kill or be killed.

==Cast==
* Jan-Michael Vincent as Commander  
* John Phillip Law as Ward Armstrong  
* Ross Hagen as Kol  
* Teagan Clive as Alienator
* Dyana Ortelli as Orrie  
* Jesse Dabson as Benny  
* Dawn Wildsmith as Caroline  
* P.J. Soles as Tara  
* Robert Clarke as Lund  
* Richard Wiley as Rick  
* Leo Gordon as Cpl. Coburn  
* Robert Quarry as Doc Burnside  
* Fox Harris as Burt  
* Hoke Howell as Harley  
* Jay Richardson as Prison sergeant major  
* Dan Golden as Electrocuted prisoner
*Joseph Pilato as Tech #2

==DVD release==
Shout! Factory announced they would release Alienator as part of a four-film "Action-Packed Movie Marathon" DVD set on March 19, 2013. 

==See also==
*Gynoid Android
*Cyborgs in fiction

==References==
 

==External links ==
*  
*  
*  
*  

 
 
 
 
 
 


 
 