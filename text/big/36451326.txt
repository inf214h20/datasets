Sad Song of Yellow Skin
Sad Tom Daly for the National Film Board of Canada (NFB).   

==Production==
Rubbo had originally gone to Vietnam with the stated goal of making a documentary about the work of Foster Parents Plan with Vietnamese war orphans. However, once there, and confronted with the enormity of what was taking place, he felt a film about this humanitarian operation was missing the real story. Rubbos NFB producer, Tom Daly, supported him in his efforts to entirely rethink the film. 

Rubbo met the children in the film through Dick Hughes, a young American who offered his apartment as a safe haven for street kids. Hughes was part of a group of American student journalists who adopted a New Journalism approach covering the war—a highly personal and involved approach that would influence Rubbos own style in making this film. This group of young journalists included John Steinbeck IV.     
   
Rubbo recorded his own subjective observations in a diary and developed the idea for what would be the first of his metafilm|self-reflexive documentaries with the NFB. In Sad Song of Yellow Skin, Rubbo often comments on his own actions within the film, expressing his doubts, fears and concerns, reminding the viewer they are watching a film and not an objective representation of reality.  

==Awards== Melbourne Film Festival. 

==References==
 

==External links==
*Watch   at the National Film Board of Canada
* 

 
 
 
 
 
 
 
 
 