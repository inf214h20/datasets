Out of the Dark (2014 film)
{{Infobox film
| name           = Out of the Dark
| image          = Out of the Dark 2014 film poster.png
| alt            = 
| caption        = Film poster
| director       = Lluís Quílez
| producer       = {{Plainlist |
* Belén Atienza
* Cristian Conti
* Enrique López Lavigne
* Andrés Calderón}}
| writer         = {{Plainlist |
* Alex Pastor
* David Pastor
* Javier Gullón}}
| starring       = {{Plainlist |
* Julia Stiles
* Scott Speedman}}
| music          = 
| cinematography = 
| editing        = 
| production companies = {{Plainlist |
* Apaches Entertainment
* Cactus Flower
* Fast Producciones
* Dynamo
* XYZ Films }}
| distributor    = Vertical Entertainment    
| released       =  
| runtime        = 92 minutes
| country        = {{Plainlist |
* Colombia
* Spain}}
| language       = English
| budget         =  
| gross          = 
}}
Out of the Dark (Spanish: Aguas rojas) is a 2014 English-language supernatural thriller film starring Julia Stiles and Scott Speedman. The independent Spanish-Colombian co-production is directed by Lluís Quílez based on a screenplay by Alex Pastor, David Pastor, and Javier Gullón. Filming took place in Colombia between April 2013 and July 2013, after which it entered post-production. The film premiered at Germanys Fantasy Filmfest on  , 2014. 

==Synopsis==
 
In 1992,  Dr Contreras Sr. prepares to abandon a finca in Santa Clara, Colombia. As he walks down the stairs he receives a phone call.  The person on the other side of the line tells him to burn a group of medical files.  These files belong to several children.  He proceeds to burn, as he does, he hears noises around the house, disposes some of the files on a food lift as he cant burn them fast enough and goes to see what the noises are.  He is then chased by several children who push him out of a 2nd floor balcony and he falls to the patio impaling a piece of wood in his chest killing him. Also, dropping in a bush a scalpel he picked up from his belonging to defend himself from the intruders. 

Twenty years later, Sarah and Paul move from the UK to Santa Clara. Sara is Jordans only daughter and the new manager of the paper mill that Jordan owns.  The Paper company has been in town for more than 20 years. It has brought prosperity to the town and provided new infrastructure and jobs. 

Sarah and Paul are moved into their new house: the finca. There, Paul finds the scalpel in a bush that Dr Contreras dropped 20 years before.  Jordan tells them that the finca has been inhabited for a long time. Paul and Sarah love the new housing and Hannah has a new doll house in the back yard.  

During a walk on the market, a girl snatches Hannahs stuffed pony and Hannah follows her through the market.  The girl has a ragged appearance and seems to have gauze wrapped in her head and face. Hannah walks towards her to recover her toy but is picked up by her parents after losing sight of her for a few moments.

The next day, while playing at home, Hannah hears a noise coming from the jungle in her porch and wonders into the forest.  Her father is looking at pictures he has taken and see the snapshot he took the day they arrived of Hannah.  He then notices the shadows of several faces with green bright eyes in the background. He looks up and Hannah is gone from his sight and into the woods.  Hannah sees her pony placed in a rock and is about to take it when her father arrives and picks her up.

A girl is hired by Jordan to be a nanny, Catalina, a local girl who wishes to become a teacher and speaks fluent English. On the night of "Los Niños Santos" (The Holy Children or Children Saint) Catalina is baby sitting Hannah.  In town, her parents watch the celebration of the children. They are told about the story that is observe on the celebration: 500 years before a group of Spaniard conquistadors came to town, found the helpless fisherman town and kidnapped all the children and demanded silver for the rescue. The town, regardless of its poverty, manage to borrow the gold from neighboring towns and paid the ransom. Nevertheless, the children were locked up in a church by their captors and burn alive.   

At the finca, Hanna is awaken by the sound of the door of the "food lift" in her room opening. She walks closer and sees her pony in it. Meanwhile, her babysitter is following a red ball that has suddenly moved in the living room. Paul sees a boy in town with gauze on his head staring at him and them suddenly disappear from the spot.

Paul and Sara arrive home to hear Hannah scream inside the finca. Then, they find that Hannah has fallen inside the lift in her room. At the clinic, Dr Contreras Jr. is now the paper mill doctor and does medical work on Hannah to verify her condition. Catalina tells Paul and Sara that the ghost of children roamed the house the night before and Paul dismisses her. Jordan travels to Brazil and leaves his daughter ahead of the factory. Sara is reviewing all of the companys information and is unable to find statements from 20 years ago.  Her father tells her that the factory has gone digital, and someone will have to look up the physical files for her.

Hannahs condition worsens everyday, even though her blood work came out fine and she is given medicine. She now has rashes on her legs and both of her parents agree to take her back to UK for medical attention. That night, the shadow of several children are seen on the house as Paul roams the outside after hearing noises and Sara is making tea. Paul goes back to Hanna´s room but in her bed now is a boy who says in a native language "red waters" and then vanishes.  Outside, Sara runs after a group of children who have wrapped her daughter in the mosquitoes net and run away with her. 

They contact the police to report the kidnapping of their daughter. Paul goes back to town to find Catalina for help and Sara stays home. Paul and Catalina goes to a church in the town "Red Waters", there a ghost runs away and Paul runs after it.  The spirit directs Paul to a shanty house.  There he finds a family, whose son Daniel died 20 years ago.  The boy was sick with rashes and fever, went out to play one day and disappeared along with several of his friends who had the same symptoms. The mother says that something in the river made the children sick. That, her husband also lost his job as the "old paper mill" closed.

At home, Sara opens the food lift and finds the children medical files left there by Dr Contreras 20 years before. She reads the files and sees pictures of rashes and realizes they are the same symptoms her daughter has. He ruled they all had mercury poisoning. Sara goes to the Dr Contreras that she knows. He confirms that the signature on the files is his father and that he died 20 years ago.

When Jordan returns, Sara confronts her father.  He confesses that the old mill had a mercury leakage 20 years ago and contaminated the river. The children that were sick, had treatment by the factory physician. Wanting to destroy evidence of the error: the children that were sick were rounded up, killed and dumped inside the old paper mill. Their parents never knew what they were sick of or what became of them. To the town, the children were taken by the ghost of the Niños Santos.  Jordan expresses remorse but tells his daughter that he helped the town  more to make up for the incident and that he was just protecting his factory.  Sara decides to go to the old mill. Jordan takes her.

At the old mill, Sara runs into Paul and Jordan goes another way to find an old tower.  There, he is confronted by the spirits of several children.  There, he also sees his granddaughter wrapped in the mosquito net. He tells the ghost of the children that he is sorry and just wants to get Hanna back. He is attacked by a few of the spirits but manages to scream and get to Hanna. The scream direct Sara to her father.   Paul and Sara watch from the other side of a gate that takes Paul some time to open.  By then, Jordan "absorbed" the mercury from Hannahs blood while he hugged her, the spirits of the children stood around them in a circle and Jordan died in the pool of water. Appeased by the death of Jordan, the children spirits reflection on the water now smile and Mercury seems to float away from their bodies. Hanna awakens healthy in her mothers arms.

In the end, images of a school built by the paper mill to honor the children who died 20 years before can be seen. Hanna is a student at this school and Catalina is a teacher.

==Cast==

*Julia Stiles as Sarah Harriman
*Scott Speedman as Paul Harriman
*Stephen Rea as Jordan
*Pixie Davies as Hannah Harriman
*Alejandro Furth as Dr. Andres Contreras, Jr.
*Guillermo Morales Vitola

==Production==

Out of the Dark is directed by Lluís Quílez based on a screenplay by Alex Pastor, David Pastor, and Javier Gullón. The film is a Spanish-Colombian co-production.    Out of the Dark is Colombia-based Dynamos first English-language production.  Participant Media fully financed the production, which has a budget of under   after subsidies and tax breaks from the production companies countries.  Filming began in Bogota|Bogota, Colombia in late April 2013.    By July 2013, the film entered post-production. 

==Release==

In November 2013, Exclusive Media acquired international rights to sell Out of the Dark.  The film premiered at Germanys Fantasy Filmfest on  , 2014.  Vertical Entertainment released the film in a limited theatrical screening on February 27, 2015 the United States.   

==Reception==
Rotten Tomatoes, a review aggregator, reports that 29% of 17 surveyed critics gave the film a positive review; the average rating is 4.4/10.   Metacritic rated it 33/100 based on nine reviews.   Dennis Harvey of Variety (magazine)|Variety said that it "offers professional polish but no interesting ideas or atmospherics".   Frank Scheck of The Hollywood Reporter wrote, "The by-then-numbers thriller features the usual genre tropes, with a particular emphasis on placing its youngest main character in constant jeopardy."   Jeannette Catsoulis of The New York Times called it "derivative and devoid of tension".   Robert Abele of the Los Angeles Times wrote that it is "a movie lovely to look at but on-the-nose and crushingly dull".   Chris Packham of The Village Voice wrote, "Stylishly filmed and often scary, Out of the Dark unspools a conclusion as conventional and button-down as a wide tie knot and a pair of wingtips."   Michael Gingold of Fangoria rated it 2/4 stars and wrote that the film "is content to go through its good-looking motions without offering the audience much that’s fresh".   Patrick Cooper of Bloody Disgusting rated it 2/5 stars and wrote that it has "remarkable photography and palpable atmosphere" but is too predictable.   Matt Boiselle of Dread Central rated it 1.5/5 stars and called it a boring film with a lackluster conclusion. 

==See also==

*List of films depicting Colombia

==Notes==

 

==References==

 

==External links==
* 

 
 
 
 
 
 
 
 
 
 