The Dickson Experimental Sound Film
{{Infobox film
| name           = The Dickson Experimental Sound Film
| image          = DicksonFilm_Still.png
| caption        = Frame from restored version of The Dickson Experimental Sound Film (1894/95) William Dickson
| producer       =
| writer         =
| starring       = William Dickson
| music          = Robert Planquette
| cinematography = William Heise
| editing        =
| distributor    = Thomas A. Edison, Inc.
| released       =
| runtime        = ≈17 seconds
| country        = United States
| language       =
| budget         =
| preceded_by    =
| followed_by    =
}} William Dickson Black Maria", Edisons New Jersey film studio. There is no evidence that it was ever exhibited in its original format. Newly digitized and restored, it is the only surviving Kinetophone film with live-recorded sound.

The film features Dickson playing a violin into a recording horn for an off-camera wax cylinder.  The melody is from a barcarolle, "Song of the Cabin Boy", from Les Cloches de Corneville (literally The Bells of Corneville; presented in English-speaking countries as The Chimes of Normandy), a light opera composed by Robert Planquette in 1877. See, e.g.,  . Courtesy of Wikipedia editor Franz Jacobs, the following material can be accessed to compare Dicksons performance with a selection from "Song of the Cabin Boy", demonstrating that Dickson plays the vocal part on the violin:
 
* 
* 
* 
* 
  United States National Film Registry.

==Restoration==
A soundless 35mm   for conservation. Among the filmstrips was a print that the Library of Congress catalogued as "Dickson Violin." According to Patrick Loughney, the librarys film and TV curator, this print is "thirty-nine feet and fourteen frames  ."  
 Avid system, Murch synchronized the visual and audio elements.

On the cylinder, before the camera starts rolling, a mans voice can be heard to say, "Are the rest of you ready? Go ahead!" This extra sound is included on the version of the film that was distributed in the early 2000s.  However, since filming had not yet begun when the words were uttered, this cannot be claimed as the first incidence of the spoken word on film.

One question that remains unanswered is how the eventual running time of just over 17 seconds was arrived at. Per the curatorial reports, the 35-mm prints have a standard 16 frames per foot of film—  plus 14 frames thus equals a total of 638 frames. Murch describes the film as having been shot at 40 frames per second (fps); Loughney describes it as 46 fps. At 40 fps, 638 frames would run 15.95 seconds, which should be the maximum length of the restored film if all other reports are correct; as Loughney notes, at 46 fps, the film would last 13.86 seconds. If the latter figure is correct, as many as 9 seconds of film are missing from both extant prints if the entire violin performance was filmed. On the basis of his own tests of eighteen Kinetoscope films, scholar Gordon Hendricks argued that no Kinetoscope films were shot at 46 fps, making the speed of 40 fps reported by Murch more likely.  Yet there is still a difference of more than a second between the maximum potential running time at that speed and the actual duration of the film as digitized by Murch. That 17-second running time works out to an average camera speed of approximately 37.5 fps, a significant difference from Murchs report.

==Interpretation== 
In his book The Celluloid Closet (1981), film historian Vito Russo discusses the film, claiming, without attribution, that it was titled The Gay Brothers.  Russos unsupported naming of the film has been adopted widely online and in at least three books, and his assertions that the films content is homosexual are frequently echoed.  In addition to there being no evidence for the title Russo gives the film, in fact the word "gay" was not generally used as a synonym for "homosexual" at the time the film was made. A particularly relevant example of the way the word "gay" was actually used is provided by a later Edison Manufacturing Company film, directed by Edwin S. Porter. As described by scholar Linda Williams, The Gay Shoe Clerk (1903)
 
is composed of a static long shot.... A clerk is tidying up, when two women enter. The younger woman seats herself before the clerk as the older womans attention wanders. When the clerk begins to try a shoe on the young woman, the master long shot is replaced by an "insert" close-up of her foot and ankle showing the clerks hands fondling the foot. As the shot continues the womans full-length skirt rises, and the audience gets a good view of her stockinged calf. Returning to the original long shot, we see the rest of the action: the clerk, apparently aroused by the sight and touch of her calf, kisses the young woman; the older woman finally notices and begins beating him on the head with her umbrella.
  documentary based on Russos book, also titled The Celluloid Closet (1995).

==Notes==
 

==Sources==
===Published===
*Dixon, Wheeler Winston (2003). Straight: Constructions of Heterosexuality in the Cinema (Albany: State University of New York Press, 2003). ISBN 0-7914-5623-4
*Hendricks, Gordon (1966). The Kinetoscope: Americas First Commercially Successful Motion Picture Exhibitor. New York: Theodore Gaus Sons. Reprinted in Hendricks, Gordon (1972). Origins of the American Film. New York: Arno Press/New York Times. ISBN 0-405-03919-0
*Loughney, Patrick (2001). “Domitor Witnesses the First Complete Public Presentation of the The Dickson Experimental Sound Film in the Twentieth Century,” in The Sounds of Early Cinema, ed. Richard Abel and Rick Altman (Bloomington: Indiana University Press), 215–219. ISBN 0-253-33988-X
*Russo, Vito (1987). The Celluloid Closet: Homosexuality in the Movies, rev. ed. (New York: Harper & Row). ISBN 0-06-096132-5

===Online===
*  short, scholarly discussion; part of the UNLV Short Film Archive
*  interview with restoration editor Walter Murch by William Kallay, September 27, 2004; part of the from Script to DVD website

==See also==
*Treasures from American Film Archives

==External links==
*  DesigningSound.org web article published May 7, 2014 about the 2002 restoration of the sound film, with photographs of the brown wax cylinder soundtrack artifact; written by Cormac Donnelly with contributions from Ken Weissman, supervisor of the film preservation lab at the Library of Congress, Jerry Fabris, museum curator at the Thomas Edison National Historical Park and Paul Spehr, author and film historian
*  brief discussion by Walter Murch, with variously formatted clips of the film (note the credits table gives the title of Planquettes opera incorrectly as Les Cloches de Normandie and misdates it 1878); part of the FilmSound.org website
*  anonymously written discussion of films recovery, with downloadable versions of the film; part of the Internet Archive
*  soundless version on the Library of Congresss YouTube channel
*  movie credits and additional details; part of the Internet Movie Database
*  extensive discussion by Spencer Sundell, April 10, 2006; part of the Mugu Brainpan weblog

 
 
 
 
 
 
 
 
 
 