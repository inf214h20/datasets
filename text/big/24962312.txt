Women in the Mirror
 
{{Infobox film
| name           = Women in the Mirror
| image          = Women in the Mirror.jpg
| caption        = Film poster
| director       = Yoshishige Yoshida
| producer       = Manasori Ayabe Akira Narisawa Matsuo Takahashi
| writer         = Yoshishige Yoshida
| starring       = Mariko Okada
| music          =
| cinematography = Masao Nakabori
| editing        = Hiroaki Morishita
| distributor    =
| released       = 23 May 2002
| runtime        = 129 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}

Women in the Mirror ( , Transliteration|translit.&nbsp;Kagami no onnatachi) is a 2002 Japanese drama film directed by Yoshishige Yoshida. It was screened out of competition at the 2002 Cannes Film Festival.   

==Cast==
* Mariko Okada - Ai Kawase
* Yoshiko Tanaka - Masako
* Sae Isshiki - Natsuki
* Hideo Murota - Goda
* Tokuma Nishioka - The Protector
* Mirai Yamamoto - The Journalist
* Miki Sanjo - The Old Woman
* Hiroshi Inuzuka - The Old Man

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 