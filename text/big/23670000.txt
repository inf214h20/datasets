Five Days, Five Nights (1960 film)
{{Infobox Film
| name           = Five Days, Five Nights
| image          = 
| image_size     = 
| caption        = 
| director       = Lev Arnshtam Heinz Thiel Anatoly Golovanov
| producer       = Otman Karayev, Adolf Fischer
| writer         = Lev Arnshtam Wolfgang Ebeling
| narrator       = 
| starring       = Heinz-Dieter Knaup
| music          = Dmitri Shostakovich
| cinematography = Yu-Lan Chen Aleksandr Shelenkov
| editing        = Tatyana Likhacheva 
| distributor    = PROGRESS-Film Verleih (GDR) DEFA
| released       =  
| runtime        = 106 minutes
| country        = Soviet Union East Germany
| language       = Russian, German
| budget         = 
}}
 East German film, directed by Lev Arnshtam and Heinz Thiel.

==Plot== end of it was Zwinger Palace. During the next five days, while searching for the collection, he encounters several of the citys residents who have also returned from the war. Although they distrust the Soviets at first, they eventually assist them to recover the pictures.

==Cast==
* Wilhelm Koch-Hooge as Erich Braun
* Annekathrin Bürger as Katrin
* Erich Franz as Father Baum
* Heinz-Dieter Knaup as Paul Naumann
* Evgenia Kozireva as Nikitina
* Marga Legal as Luise Ramk
* Mikhail Mayorov as General
* Vladimir Pitsek as Galkin
* Nikolai Pogodin as Rudakov
* Vyacheslav Safonov as Captain Leonov
* Vsevolod Sanaev as Sergeant Kozlov
* Raimund Schelcher as farmer
* Gennadi Yukhtin as Strokov

==Production==
The pictures plot was inspired by the recovery of the art of the Old Masters Picture Gallery through the hands of Soviet troops in 1945. The art collection was then taken to the USSR, where it was kept until being returned to the Dresden Gallery during 1960.   The film was the first Soviet–East German co-production in the field of cinema.   

==Reception== 
Five Days, Five Nights sold more than two million tickets in the German Democratic Republic.  

The film critic of Der Spiegel described the picture as "making no claim to document history truthfully", while also quoting Walter Ulbricht, who called it "a great work of the Working Class" and a monument to Soviet–East German friendship.  The Die Zeit reviewer wrote: "the film portrays the Germans quite objectively. But the Soviets? We could only wish for it. Although we well realize that could not have been as they are depicted: noble, faultless and helpful." 

==References==
 

==External links==
*  
*   on ostfilm.de
*   on defa-sternstunden.de
*   on cinema.de
*   at the DEFA Film Library

 
 

 
 
 
 
 
 
 
 
 