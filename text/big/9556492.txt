Danton (1983 film)
{{Infobox film
| name           = Danton
| image          = DantonPoster.jpg
| caption        = Film poster
| director       = Andrzej Wajda
| producer       = Margaret Ménégoz
| writer         = Jean-Claude Carrière collaboration with Andrzej Wajda Agnieszka Holland Bolesław Michałek Jacek Gąsiorowski 
| narrator       = 
| starring       = Gérard Depardieu Wojciech Pszoniak Anne Alvaro
| music          = Jean Prodromidès
| cinematography = Igor Luther
| editing        = Halina Prugar-Ketling Gaumont
| released       =  
| runtime        = 136 minutes
| country        = France Poland West Germany|West&nbsp;Germany
| language       = French
| budget         = 
}} Polish play "The Danton Case" by Stanisława Przybyszewska.
 Solidarity movement was struggling against the oppression of the Soviet Union|Soviet-backed Polish government.  The film had 1,392,779 admissions in France. 

== Plot ==
The film begins in the spring of 1794, when the Reign of Terror was in full swing. On the borders of Paris, any vehicles entering Paris, including the carriage of Danton, who has just ridden in, are being searched.  Robespierre, meanwhile, is sick in his bed. His landladys daughter,  , who is publishing pro-Dantonist circulars. 
 general Westermann about a coup to overthrow Robespierre and the committee, of which Danton disapproves. Danton’s closest supporters warn him that Robespierre is planning on having him jailed. Danton, however, is positive that his newspaper and the support of the people will prevent anything like that from ever happening. All of his supporters urge him to strike now and take power, but he resists. That day, at the convention, one of Danton’s supporters, François Louis Bourdon|Bourdon, makes a speech against Heron and his secret police (a central part of Robespierre’s regime), and has Heron jailed.

That night, Danton and Robespierre have dinner together. Danton puts much work into setting the meal, but Robespierre refuses to drink or eat, insisting on a serious discussion. Robespierre wants Danton to join his cause and stop fighting because he does not want to be forced to have Danton executed. Danton simply drinks until he passes out, and refuses Robespierre’s advances. As Danton leaves the hotel, he is met by a group of armed men who turn out to be Westermanns assistants, preparing to stage a coup. Danton rebuffs Westermans attempt to coerce him into helping. Next, Robespierre goes to Camille Desmoulins house, where Camille entirely ignores his presence. Robespierre  tries to convince Camille that Danton is exploiting him, but he is again ignored. His wife Lucile begs Robespierre to stay and talk sense into her husband because she wants him to live, but Robespierre can do nothing. With no other options, Robespierre has Lacrois, Phillipeaux, Desmoulins, Westermann, Danton and other supporters arrested and jailed in the Luxembourg jail, after having the warrant signed by the Committee of General Security and the Committee of Public Safety. Although Danton has the power to raise up a force and resist, he doesn’t because he does not want any more bloodshed. The man who arrests Danton is scared of him, and Danton has to practically drag him along.

The next day at the national convention, the members are outraged by the arrest, but Robespierre simply justifies his action by stating that Danton is an enemy of the Republic, and must be tried regardless of his popularity. To save his own life, Bourdon joins Robespierres side, deserting Danton and Desmoulins, which disgusts Lucile. 

While Danton waits in custody, Robespierre plans out his trial. Only seven jurors are to be used, which is against the law, but Robespierre can only ensure seven men who will find Danton guilty. Danton has given up on the Revolution and on the people. At the trial, Danton consistently breaks the order by speaking out of turn. The people are still in support of him, and judge Fouquier finds no grounds to prosecute him. The accused are kept in prison overnight and there is a solitary scene in where Danton is brought to his knees when a condemned prisoner tells him how overjoyed he is to hear that Danton, the first president of the committee, is to be executed. While Robespierre is visiting Jacques-Louis David|David, he is informed that Dantons charisma is interrupting the planned process of the trial, and the sentence is going nowhere. In response a decree is issued that if anyone speaks out of turn again, which Danton has done repeatedly, they will be removed from court. Within minutes, the entire accused team has been dismissed, and the verdict of guilty is read. The day before his execution, Danton is depressed. Not due to his death, but due to the fact that he feels that he failed the people. They are led off to the scaffold and guillotined. When Robespierre finally hears of Danton’s death, he turns ghostly pale, and realizes how he has violated liberty, and the goals of the revolution. His mistress’s nephew, now fully practiced, is finally sent in to recite. As he reads off the Declaration of the Rights of Man and Citizen, Robespierre is fully brought to the reality of what he has done.

== Cast ==
* Gérard Depardieu as Georges Danton
* Wojciech Pszoniak as Maximilien Robespierre
* Anne Alvaro as Éléonore Duplay
* Patrice Chéreau as Camille Desmoulins
* Bogusław Linda as Louis de Saint-Just Lucile Desmoulins
* Andrzej Seweryn as François Louis Bourdon
* Serge Merlin as Pierre Philippeaeux
* Roland Blanche as Jean-François Delacroix|Jean-François de Lacroix
* Jacques Villeret as François Joseph Westermann
& (alphabetical order)
* Emmanuelle Debever as Louison
* Krzysztof Globisz as Jean-Pierre-André Amar
* Ronald Guttman as Herman
* Gérard Hardy as Jean-Lambert Tallien
* Tadeusz Huk as Georges Couthon
* Stéphane Jobert as Panis
* Marian Kociniak as Lindet
* Marek Kondrat as Bertrand Barère de Vieuzac
* Alain Macé as Héron
* Bernard Maître as Legendre
* Lucien Melki as Fabre dÉglantine
* Erwin Nowiaszek as Jean-Marie Collot dHerbois
* Leonard Pietraszak as Lazare Carnot
* Roger Planchon as Antoine Quentin Fouquier-Tinville
* Angel Sedgwick as Frère Éléonore
* Franciszek Starowieyski as Jacques-Louis David
* Jerzy Trela as Jacques Nicolas Billaud-Varenne
* Jean-Loup Wolff as Marie-Jean Hérault de Séchelles
* Czesław Wołłejko as Vadier
* Wladimir Yordanoff as Chef des gardes
* Małgorzata Zajączkowska as Servante Duplay
* Szymon Zaleski as Lebas

==References==
 

==External links==
*  
*  
* 1983 movie review by Vincent Canby at  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 