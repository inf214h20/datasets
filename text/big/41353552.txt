Helios (film)
 
 
{{Infobox film
| name           = Helios
| image          = Heliosfilm.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 赤道
| simplified     = 赤道
| pinyin         = Chì Dào
| jyutping       = Cek3 Dou6 }} 
| director       = Longman Leung Sunny Luk
| producer       = Catherine Kwan
| screenplay     = {{plainlist|
* Longman Leung
* Sunny Luk
}}
| starring       = {{plainlist|
* Jacky Cheung
* Nick Cheung
* Shawn Yue
* Wang Xueqi
* Janice Man
* Ji Jin-hee
* Choi Siwon
* Yoon Jin-yi
* Josephine Koo
* Feng Wenjuan
* Chang Chen
}}
| music          = 
| cinematography = Jason Kwan
| editing        = Kong Chi-leung
| studio         = {{plainlist| Media Asia Films
* Wanda Pictures
* Sun Entertainment Culture
* Sil-Metropole Organisation
* Guangzhou Chang Chang Film & TV Culture
* Media Asia Distribution (Beijing) Stars Shine Blue Sea Productions
}}
| distributor    = Media Asia Distributions
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese Mandarin Korean English
| budget         = US$26 million 
| gross          = US$20.68 million 
}} Hong Kong crime thriller film directed by Longman Leung and Sunny Luk and starring an international ensemble cast from Hong Kong, China, Taiwan and South Korea. The film was released on April 30, 2015.  

==Plot==
Number one wanted criminal "Helios" (Chang Chen) and his assistant (Janice Man) stole a quantity of uranium and plan to produce weapons of mass destruction. They are planning to trade with a terrorist organization in Hong Kong. Chief Inspector Lee Yin-ming (Nick Cheung) and Inspector Fan Ka-ming (Shawn Yue) lead the Counter Terrorism Response Unit of the Hong Kong Police Force. Along with Chinese senior official Song An (Wang Xueqi), Physics professor Siu Chi-yan (Jacky Cheung) and two South Korean weapon experts Choi Min-ho (Ji Jin-hee) and Pok Yu-chit (Choi Siwon), they hope to defuse a crisis involving a suitcase nuke.

==Cast==
*Jacky Cheung as Siu Chi-yan (肇志仁), a Physics professor at the University of Hong Kong
*Nick Cheung as Lee Yin-ming (李彦明), chief inspector of the Counter Terrorism Response Unit
*Shawn Yue as Fan Ka-ming (范家明), inspector of the Counter Terrorism Response Unit
*Wang Xueqi as Song An (宋鞍), a senior official from Mainland China
*Janice Man as Helios assistant
*Ji Jin-hee as Choi Min-ho (崔民浩), a South Korean weapon expert
*Lee Tae-ran as Choi Min-hos wife
*Choi Siwon as Pok Yu-chit (朴宇哲), a South Korean nuclear expert
*Yoon Jin-yi
*Josephine Koo
*Fen Wenjuan as Yuan Xiaowen (袁曉文), Song Ans assistant
*Chang Chen as Helios (赤盜), a South Korean weapon smuggler

==Release==
The film held its first press conference on 10 December 2013 at the Sheraton Hotel in Macau where its first trailer was unveiled.  The film is released in Singapore on 30 April 2015.

== Box office == You Are My Sunshine and The Left Ear.   

==See also==
*Jacky Cheung filmography

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 