Jaisi Karni Waisi Bharnii
 
{{Infobox film
| name           = Jaisi Karni Waisi Bharnii
| image          = 
| image_size     = 
| caption        = 
| director       = Vimal Kumar
| producer       = Vimal Kumar
| writer         = 
| narrator       =  Govinda Kimi Katkar Asrani Kader Khan Shakti Kapoor
| music          = Rajesh Roshan
| Lyricist       =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2 June 1989
| runtime        = 
| country        = India Hindi
| budget         = 
}} Indian film Govinda and Kimi Katkar in pivotal roles.

==Cast== Govinda - Ravi Verma 
*Kimi Katkar - Radha 
*Asrani - Datturam 
*Kader Khan - Gangaram Verma 
*Shakti Kapoor - Vijay Verma  
*Gulshan Grover - Ranjeet
*Neil Nitin Mukesh - Young Ravi Verma

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aaya Aaya Aankhon Ka Salam"
| Mohammed Aziz
|-
| 2
| "Aaj Kal Ke Bachchay"
| Shabbir Kumar, Sapna Mukherjee
|-
| 3
| "Jaisi Karni Waisi Bharni"
| Nitin Mukesh
|-
| 4
| "Aaja A Khelen Game Koi"
| Kumar Sanu, Sapna Mukherjee
|-
| 5
| "Mehke Huwe Tere Lab Ke Ghulab"
| Mohammed Aziz
|}

==External links==
*  

 
 
 


 