Rambo (2008 film)
{{Infobox film
| name           = Rambo
| image          = Rambowallpaperkr8.jpg
| caption        = Theatrical release poster
| director       = Sylvester Stallone
| producer       = Avi Lerner Kevin King Templeton John Thompson 
| writer         = Art Monterastelli Sylvester Stallone
| based on       =  
| starring       = Sylvester Stallone Julie Benz Paul Schulze Matthew Marsden Graham McTavish Rey Gallegos Tim Kang Jake La Botz Maung Maung Khin Ken Howard Brian Tyler  
| cinematography = Glen MacPherson
| editing        = Sean Albertson
| studio         = Millennium Films Nu Image Films  Emmett/Furla/Oasis Films|Emmett/Furla Films   for Equity Pictures Medienfonds GmbH http://www.rambothefilm.com/credits.jpg  Lionsgate The Weinstein Company
| released       =  
| runtime        = 92 minutes (Theatrical) 99 minutes (Extended)
| country        = United States Germany
| language       = English
| budget         = $50 million 
| gross          = $113.2 million 
}} independent   action film directed, co-written by and starring Sylvester Stallone reprising his famous role as Cold War/Vietnam veteran John Rambo. It is the fourth installment in the Rambo (film series)|Rambo franchise, twenty years since the previous film Rambo III. This film is dedicated to the memory of Richard Crenna, who played Sam Trautman|Col. Sam Trautman in the first three films, and who died of heart failure in 2003.
 Special Forces Burmese military regime.
 Spike TV on July 11, 2010. However, it was the extended cut that was broadcast, not the theatrical version. The extended cut was later released on Blu-ray two weeks later.

==Plot== after the crisis in humanitarian mission Karen tribespeople. Rambo refuses, claiming that without weapons, there will be no changes, but is eventually persuaded by missionary Sarah Miller (Julie Benz) to make the trip.

During their trip, the boat is stopped by a trio of pirates driving a gunboat who demand Sarah in exchange for passage. After negotiations fail, Rambo kills the pirates with his cold war-era Colt M1911 and later dumps a single body in the water to conceal the evidence, as well as burning the rest of the bodies on the return trip. Michael is greatly disturbed at Rambos actions; upon arriving in Burma, he says that the group will travel by road and will not need him for the return trip. The mission goes well until the Tatmadaw, led by Major Tint, suddenly attack, slaughtering most of the villagers and two missionaries and kidnapping the rest, including Michael and Sarah. When the missionaries fail to return after ten days, their pastor (Ken Howard) comes to ask Rambos help to guide a hired team of five mercenary|mercenaries, Lewis (Graham McTavish), School Boy (Matthew Marsden), En-Joo (Tim Kang), Reese (Jake La Botz), and Diaz (Reynaldo Galledos), to the village where the missionaries were last seen.
 rice paddy with Land mine|landmines, and betting on the outcome. The team takes cover, planning to stand by and (seemingly reluctantly) let the hostages be killed in order to avoid provoking a response from a much larger group of soldiers. Having disregarded Rambo as a simple boatman, the mercenaries are shocked when he appears and single-handedly wipes out the entire squad of Tatmadaw soldiers with his compound bow, allowing the hostages to escape unscathed.
 Karen rebels disembowels him.

In the final scene, encouraged by Sarahs words Rambo returns to his home in the United States, Rambo is seen walking along an Arizona highway until he sees a horse farm and a rusted mailbox. Reading the name "List of Rambo characters#R. Rambo|R. Rambo" Rambo smiles and walks down the gravel driveway as the credits roll.

==Cast==
*Sylvester Stallone as John Rambo
*Julie Benz as Sarah Miller
*Paul Schulze as Michael Burnett
*Matthew Marsden as School Boy
*Graham McTavish as Lewis
*Reynaldo Gallegos as Diaz
*Tim Kang as En-Joo
*Jake La Botz as Reese
*Ken Howard as Father Marsh
*Maung Maung Khin as Tint

==Production==
In between the making of the third and fourth films in the Rambo franchise, the films original producer, Carolco Pictures, went out of business. In 1997, Miramax Films purchased the Rambo franchise.  The following year, Miramax subsidiary Dimension Films intended to make another film, and a writer was hired to pen the script, but any attempts to make it were deterred by Stallone, who had stated that he no longer wanted to make action movies.  In 2005, the studio sold those rights to Nu Image/Millennium Films. 

Filming started on January 22, 2007 and ended on May 4, 2007. The movie was shot at Chiang Mai, Thailand as well as in Mexico and the United States in Arizona and California.

While filming near Burma, Stallone and the rest of the crew narrowly avoided being shot by the Burmese  military. Stallone described Burma as a "hellhole". He said "we had shots fired above our heads" and that he "witnessed survivors with legs cut off and all kinds of land-mine injuries, maggot-infested wounds and ears cut off."   

==Alternative titles== Rocky Balboa), which was not his original intent. In many other countries, the title John Rambo is kept because the first Rambo movie, First Blood, was released as Rambo in many foreign territories. The film premiered on television as Rambo, but the title sequence referred to it as John Rambo.
 The Searchers." 

==Music==
{{Infobox album  
| Name       = Rambo: Original Motion Picture Soundtrack
| Type       = Film Brian Tyler
| Cover      = 
| Alt        = 
| Released   =           
| Recorded   = 
| Genre      = 
| Length     = 75:59  Lionsgate   Brian Tyler
| Last album =   (2007)
| This album = Rambo (2008) The Lazarus Project (2008)
}}
 Brian Tyler The Hunted, a film noted with striking similarities to the first Rambo film, First Blood.  

Track listing
{{Track listing
| all_music       = Brian Tyler
| total_length    = 75:59

| title1 = Rambo Theme
| length1 = 3:34
| title2 = No Rules of Engagement
| length2 = 7:09
| title3 = Conscription
| length3 = 2:55
| title4 = The Rescue
| length4 = 4:04
| title5 = Aftermath
| length5 = 2:33
| title6 = Searching for Missionaries
| length6 = 7:07
| title7 = Haunting Missionaries
| length7 = 2:43
| title8 = Crossing into Burma
| length8 = 6:59
| title9 = The Village
| length9 = 1:43
| title10 = Rambo Returns
| length10 = 2:44
| title11 = When You Are Pushed
| length11 = 2:26
| title12 = The Call to War
| length12 = 2:51
| title13 = Atrocities
| length13 = 1:40
| title14 = Prison Camp
| length14 = 4:42
| title15 = Attack on the Village
| length15 = 3:01
| title16 = Rambo Takes Charge
| length16 = 2:22
| title17 = The Compound
| length17 = 7:48
| title18 = Battle Adagio
| length18 = 3:10
| title19 = Rambo Main Title
| length19 = 3:30
| title20 = Rambo End Title
| length20 = 2:58
}}

==Release==

===Box office===
Rambo opened in 2,751 North American theaters on January 25, 2008 and grossed $6,490,000 on its opening day,  and $18,200,000 over its opening weekend. It was the second highest grossing movie for the weekend in the U.S. and Canada behind Meet the Spartans. 
The film has a box office gross of $113,344,290, of which $42,754,105 was from Canada and the United States. 
 UCI followed Ireland which were managed by Odeon.  The film was, however, shown in Ireland and the United Kingdom by other theater chains such as Empire Cinemas, Vue (cinema)|Vue, Cineworld and Ward Anderson. The film was not shown in the French-speaking part of Switzerland due to legal and commercial problems with the distributor, even if it was available on screens of France and the Swiss German-speaking part. 

===Critical reception===
The film gained mixed reviews from critics, earning a 37% rating on the movie review aggregator website Rotten Tomatoes. 

In his review for   look like Sesame Street".
 First Blood writer David Morrell said: Im happy to report that overall I’m pleased. The level of violence might not be for everyone, but it has a serious intent. This is the first time that the tone of my novel First Blood has been used in any of the movies. Its spot-on in terms of how I imagined the character — angry, burned-out, and filled with self-disgust because Rambo hates what he is and yet knows its the only thing he does well. ...  I think some elements could have been done better,   I think this film deserves a solid three stars.   

====Reception in Burma==== ruling party has ordered DVD vendors in Burma not to distribute the film due to the movies content.    Despite having never been released there theatrically or on DVD, Rambo is, however, available there in bootleg versions. Despite the film being unpopular among some of the population due to the negative portrayal of the Tatmadaw, the opposition youth group Generation Wave copied and distributed the film as anti-Tatmadaw propaganda.   

According to Karen Freedom Fighters, the movie gave them a great boost of morale. Burmese freedom fighters have even adopted dialogue from the movie (most notably "Live for nothing, or die for something") as rallying points and battle cries. "That, to me," said Sylvester Stallone, "is one of the proudest moments Ive ever had in film."  Also, overseas Burmese have praised the movie for its vivid portrayal of the militarys oppression of the Karen people. 

==Home media== Dolby Digital Dolby Digital DTS HD 7.1 Tracks. The DVD and Blu-ray Disc on disc one have the film, deleted scenes, 6 featurettes, and commentary by Sylvester Stallone. The Blu-ray Disc also has 2 extra special features, that includes a trailer gallery.
 digital copy of the film. There is also  a 6 disc DVD set of all four Rambo (film series)|Rambo films, packaged in a limited edition tin case with over 20 bonus features. A Blu-ray Disc set with Rambo 1-3 was also released.   

The DVD was released in the UK on June 23, 2008.

The film was the 19th best selling DVD of 2008 with 1.7m units sold and an overall gross of $39,206,346. 

===Extended cut===
When asked about the moral of the film in a Daily Yomiuri Online interview, Sylvester Stallone mentioned that he will be doing an Extended Cut of the film, which will go by the original title of John Rambo.   Nevertheless, the news became most well known after a May 2008 interview with Jay Leno when he announced the work of a directors cut, and that the proceeds would go to Burma. On top of this, an online petition appeared shortly after the announcement in order to "motivate" Stallone in completing this new cut. At the 2008 Comic-Con International|Comic-Con, it was vaguely announced that the directors cut (or as they labeled it, "extended cut") would be released in 2009, though no other specifics were given.  Additionally, the directors cut premiered at the 2008 Zurich Film Festival.  DVD Active announced it would premiere only on Blu-ray Disc in Canada and the United States on July 27, 2010.
 Spike TV) The Expendables. The extended cut runs 99 minutes long, whereas the theatrical version runs 91 minutes. Of those run times the end credits roll for 11 minutes.

==Sequel==
On February 2012, Sean Hood offered an update on his Twitter account stating that Rambo 5 on hold as Sly finishes Expendables 2. He hasnt decided if R5 will be an Unforgiven or a passing of the torch." Meanwhile, Stallone confirmed a fifth installment, saying this one would be the last one. Under the title Rambo: Last Stand, it would bring Rambo to Arizonas Mexican border and to Mexico as well.

==See also==
*Human rights in Burma
*Karen people
*Aung San Suu Kyi
*Burma VJ
*Beyond Rangoon The Lady

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 