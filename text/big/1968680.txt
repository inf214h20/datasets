Spacehunter: Adventures in the Forbidden Zone
 
{{Infobox film name = Spacehunter: Adventures in the Forbidden Zone image = spacehunter_movie_poster.jpg caption = Spacehunter: The Adventures in the Forbidden Zone movie poster writer = David Preston Edith Rey Daniel Goldberg Len Blum story = Stewart Harding Jean LaFluer
| starring = {{plainlist|
* Peter Strauss
* Molly Ringwald
* Ernie Hudson
* Michael Ironside
}} director = Lamont Johnson producer = Don Carmody studio = Columbia Pictures Corporation Delphi I Productions (as Delphi Productions) Zone Productions distributor = Columbia Pictures cinematography = Frank Tidy released =   runtime = 90 minutes country = Canada / United States language = English music = Elmer Bernstein budget = $14,400,400 (estimated) gross = $16,500,000 (USA) (sub-total)
}}
Spacehunter: Adventures in the Forbidden Zone is a 1983 pulp, action-adventure, science fiction film. The movie stars Peter Strauss, Molly Ringwald, Ernie Hudson, Andrea Marcovicci, and Michael Ironside.  The films executive producer was Ivan Reitman, and it was directed by Lamont Johnson. The film had an adventurous music score composed by Elmer Bernstein.

When the movie came out in theaters, parts of it were shown in 3-D film|3-D and the film became part of the 3D film#Revival .281960.E2.80.931984.29 in single strip format|3-D movie revival craze of the early 1980s.

The movie is about a bounty hunter who goes on a mission to rescue three women stranded on a brutal planet and meets a vagrant teenage girl along the way.

==Plot==
Set in the 22nd century, the film opens with the destruction of a space cruise liner that collides with a meteor. The only apparent survivors are three beautiful women – Nova (Cali Timmins), Reena (Aleisa Shirley), and Meagan (Deborah Pratt) – who get away in an escape pod and land on the nearest habitable planet. There, they are quickly accosted by the hostile natives and taken aboard a sail-driven vehicle resembling a pirate ship on rails.

In space, an alert goes out for the safe return of the women with a reward of 3,000 "mega-credits". A small time salvage operator named Wolff (Peter Strauss) intercepts the message and heads to the planet. Joining him is his female engineer Chalmers (Andrea Marcovicci), who learns the planet – called Terra XI – is a failed colony that fell victim to a deadly plague and civil warfare. Wolff risks the dangers believing the reward will solve his debt problems.
 android – has been "killed". Wolff continues on alone, but soon catches a teenage Scav named Niki (Molly Ringwald) trying to steal his Scrambler. She convinces Wolff that he needs a tracker if he is to survive The Zone and Wolff reluctantly takes her lead.

In the meantime, the three women are taken before "The Chemist" (Hrant Alianak), the chief henchman of Overdog who administers pacifying drugs to the girls and prepares them for Overdogs pleasure.

Elsewhere, Wolff and Niki make camp, but soon come under attack by a strange plow-like vehicle. Wolff manages to disable the machine and learns the driver is a former military acquaintance of his – a soldier named Washington (Ernie Hudson), who reveals he too has come to rescue the women. His only problem is that he crashed his ship and has no way off world. Wolff refuses to help his rival and leaves him to fend for himself.

Back in The Zone, the women are taken to see the Overdog (Michael Ironside), who is revealed to be a hideous cyborg, almost entirely machine, with two metal claws for hands.

Still led by Niki, Wolff gets into more predicaments – from being attacked by mutated blob-like beings, to strange mermaid-like women and their giant water dragon. He even loses his trusty Scrambler and is forced to continue on foot. Eventually, they are found by Washington and Wolff finds the situation reversed as he now begs his rival for help. Washington assists, but only if Wolff cuts him in 50/50 on the reward.

Wolff and Washington team up and sneak into Overdogs fortress where they find the Zoners entertained by captured prisoners forced to run through a deadly maze of lethal obstacles, hazards and traps. Wolff spots the women being held in a cage and forms a rescue plan, but a bored Niki (who was left out of the rescue for her safety) decides to snoop around. She is captured and sent into the maze. Wolff spots Niki in the maze and tries to rescue her, but she uses her prowess to reach the end where Overdog congratulates her and drags her back to his lair. There, she is hooked to a machine that slowly drains her life energy. The energy in turn recharges Overdog. Wolff comes to the rescue and jabs a sparking power cable into one of Overdogs claws. The power feedback fries Overdog and thus causes cascading blowouts throughout the entire fortress.

As the complex explodes, the three heroes, and the three rescued women, manage to get away. In the end, Wolff invites Niki to stay with him and she agrees since they made good partners.

==See also==
* List of American films of 1983
* List of 3D films

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 