Toto, Peppino, and the Hussy
{{Infobox film
| name           = Totò, Peppino e la malafemmina
| image          = Totò, Peppino e... la malafemmina - La dettatura della lettera.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Camillo Mastrocinque
| producer       = Isidoro Broggi   Renato Libassi
| writer         = Nicola Manzari Edoardo Anton Sandro Continenza Francesco Thellung
| narrator       =  Dorian Gray  Teddy Reno
| music          = Pippo Barzizza   Lelio Luttazzi   Totò
| cinematography = Mario Albertelli, Claudio Cirillo
| editing        = Gisa Radicchi Levi
| studio         = D.D.L.
| distributor    = 
| released       = 1956
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         =  Italian lire
| preceded by    = 
| followed by    = 
}}

Toto, Peppino, and the Hussy (originally Totò, Peppino e... la malafemmina) is an Italian comedy film directed by Camillo Mastrocinque in 1956. It stars the  comedy duo of Totò and Peppino De Filippo. The film also stars the popular singer Teddy Reno, and features Reno singing some of his songs as well as Malafemmena, Totòs most famous work as a songwriter.
 Italian lire revenue (about 40 million Euros in 2009).   in Italian 

==Plot==
 ]]
The brothers Antonio (Totò) and Peppino (Peppino De Filippo) Caponi are boorish landowners living in southern Italy. Antonio is lavish and steals his stingy brothers money. Dorian Gray), a revue dancer, and follows her to Milan.

The news is broken to the family with an anonymous letter, and the three brothers travel to Milan in an attempt to stop the relationship, which they consider dangerous.
Antonio and Peppino try to bribe Marisa away from Gianni but he woos her back by moving her to tears with the song "Malafemmina", and Lucia realizes that she is a good girl.
In the end, she leaves the revue world, moves to their village and marries Gianni.

==The letter of the Capone brothers==
Memorable in the history of comedy films is the scene in the movie where Toto and Peppino (the Capone brothers) have to write a simple letter to be sent to his grandson. Unfortunately for two uneducated peasants and how they use this is almost impossible. 
In fact, everything that the two brothers should write to his girlfriends nephew concerns a notice to him to let the boy alone in order to continue his studies. Mistaking it for roughly a prostitute, however, the two brothers write that with 700,000 lire the girl can reward yourself for lost time with their grandson. 
Yet Toto, who is believed that most awake, does not realize the severity of grammatical errors that creates the dictate of simple sentences that are anything but pure Italian. 
The text of the letter says:


« Signorina
veniamo noi con questa mia addirvi una parola che che che scusate se sono poche ma sette cento mila lire; noi ci fanno specie che questanno c’è stato una grande morìa delle vacche come voi ben sapete.: questa moneta servono a che voi vi con linsalata consolate dai dispiacere che avreta perché dovete lasciare nostro nipote che gli zii che siamo noi medesimo di persona vi mandano questo   perché il giovanotto è studente che studia che si deve prendere una laura che deve tenere la testa al solito posto cioè sul collo.;.;
Salutandovi indistintamente i fratelli Caponi (che siamo noi i Fratelli Caponi) »

==Legacy==
Some of the most famous scenes of Totò and Peppino took place in this movie   (e.g. the dictation of a letter full of grammatical errors and their attempt to talk in French  with a Milanese policeman ("nous voulevan savua...")).

==Cast==
*Totò: Antonio Caponi
*Peppino De Filippo: Peppino Caponi Dorian Gray: Marisa Florian
*Teddy Reno: Gianni
*Vittoria Crispo: Lucia Caponi
*Mario Castellani: Mezzacapa
*Nino Manfredi: Raffaele

==References==
 

==External links==
* 
*  (German dub)

 
 
 
 
 
 
 