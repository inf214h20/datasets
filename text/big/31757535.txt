East End Forever
{{Infobox film
| name           = East End Forever
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Carole Laganière
| producer       = Nathalie Barton
| writer         = Carole Laganière
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Bertrand Chénier
| cinematography = Philippe Lavalette
| editing        = Guillaume Millet France Pilon InformAction Films
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Canada
| language       = French English
| budget         = 
| gross          =
}}
East End Forever ( ) is a 2011 Quebec documentary film about seven young people from the Hochelaga-Maisonneuve district of Montreal. Written and directed by Carole Laganière.   The film debuted at the Grand Library on February 18, 2011, {{cite news 
| url=http://www.ctvm.info/article.php3?id_article=2263 
| title=En grande primeur aux RVCQ " L’Est pour toujours ", documentaire de Carole Laganière 
| work=CTVM 
| date=February 18, 2011 
| accessdate=May 20, 2011 
| language=French}}  before theatrical release on May 13, 2011.  {{cite web 
| url=http://fctnm.org/2011/05/11/lest-pour-toujours-de-carole-laganiere-a-laffiche-du-cinema-parallele-des-vendredi-13/ 
| title="L’Est pour toujours" de Carole Laganière à l’affiche du Cinéma Parallèle dès vendredi 13 Mai 
| publisher=fctnm.org 
| accessdate=May 20, 2011 
| language=French}}  {{cite web 
| url=http://www.ipreview.ca/catalog/movie/L_Est_pour_toujours 
| title=East End Forever 
| publisher=ipreview.ca 
| accessdate=May 20, 2011}} 

==Synopsis==
In 2003, seven children had aspirations for their futures. Revisiting the neighbourhood and the children eight years later, L’Est pour toujours documents the progress they have made in their lives. Marianne Racine reconnected with his father, only to find he lives in Vancouver and does not speak French. Maxime Desjardins-Tremblay has combined work and study as a film and television actor. Though wishing to become a rapper, he still gets caught up in problems with street gangs. Proulx-Roy and Jean-Roch Beauregard, having spent time in youth centers and reform schools, are both still seeking their paths in life. Valérie Allard has aspirations of working with others through social services. Samantha Goyer has completed school. At 21, Vanessa Dumont is the oldest of the seven. She looks far younger than her biological age, but this affects her search for both job and boyfriend, and she deals with dark moods. The film shares how a persons future is not always determined by where they grew up.

==Cast==
* Marianne Racine
* Maxime Desjardins-Tremblay
* Maxime Proulx-Roy
* Jean-Roch Beauregard
* Valérie Allard
* Samantha Goyer
* Vanessa Dumont

==Background== Informaction Productions. 

==Reception==
Montreal Mirror, in speaking of Carole Laganières 2003 documentary Vues de l’Est and the follow up of L’Est pour toujours, wrote that viewers need not have seen the earlier film to be able to appreciate this later offering, "especially since Laganière weaves in moments from the original along with footage shot at intervals in the years since."  They noted a similarity to Michael Apted’s Up Series of films, as the viewer sees the subjects grow before their eyes. The reviewer writes that the subjects "discuss their lives and problems with remarkable frankness," and that while the films tone is occasionally depressing as the viewer learns that some of the subjects are repeating the patterns seen eightyears earlier, the director "captures their stories with heart, leaving you wishing for a large-scale project such as Apted’s to come out of this—and hoping that these kids somehow turn out okay." {{cite news 
| url=http://www.montrealmirror.com/wp/2011/05/12/weekly-round-up-19/ 
| title=Weekly round-up: Italian slowcore, French farce, gay Israeli antics and a Hochelaga documentary 
| work=Montreal Mirror 
| date=May 12, 2011 
| accessdate=May 20, 2011 
| author=Malcolm Fraser}} 

Le Cinema wrote that the film was "...touchant et révélateur, sintitule et il rappelle au tournant le brio de plusieurs documentaires québécois," (...touching and revealing, and seen as brilliant of the several extant Quebec documentaries) and after expanding on the individuals whose lives are being documented, concluded "En espérant que la cinéaste renoue avec ses sujets dans cinq ou dix ans, un peu comme XV le fait périodiquement dans sa série «Up»" (it is hoped that the filmmaker returns to his subjects in another five or ten years, giving them the regular coverage as has Michael Apted for the subjects of the Up Series). {{cite web 
| url=http://www.lecinema.ca/critique/2087/ 
| title=De l’espoir à l’horizon - Critique du film LEst pour toujours 
| publisher=Le Cinema 
| date=May 13, 2011 
| accessdate=May 20, 2011 
| author=Martin Gignac 
| language=French}} 

==References==
 

==External links==
*  
*  
*  May 13, 2011
*  May 13, 2011
*  May 14, 2011

 

 
 
 
 
 
 
 
 
 
 
 
 