The Story of Marie and Julien
 
 
{{Infobox film
| name           = The Story of Marie and Julien
| image          = Histoire de Marie et Julien poster.jpg
| alt            = A film poster showing a middle-aged man touching the shoulder of a young woman he is in bed with
| caption        = French theatrical release poster
| director       = Jacques Rivette
| producer       = Martine Marignac
| writer         = Pascal Bonitzer Christine Laurent Jacques Rivette
| starring       = Emmanuelle Béart Jerzy Radziwiłowicz Anne Brochet
| music          = 
| studio         = Pierre Grise Productions Cinemaundici Arte France Cinéma VM Productions   
| cinematography = William Lubtchansky
| editing        = Nicole Lubtchansky
| distributor    = Celluloid Dreams  Les Films De Losange 
| released       =  
| runtime        = 150 minutes
| country        = France Italy
| language       = French
| budget         = 
}} Nouvelle Vague supernatural Romance love story between Marie and Julien, played by Emmanuelle Béart and Jerzy Radziwiłowicz.      Anne Brochet plays the blackmailed Madame X. Béart had previously worked with Rivette in La Belle Noiseuse, as had Radziwiłowicz in Secret défense. The cinematographer was William Lubtchansky.

The film was originally going to be made in 1975 as part of a series of four films, but shooting was abandoned after two days, only to be revisited by Rivette 27 years later. It premièred at the Toronto International Film Festival in September 2003 and had a cinema release in France, Belgium and the UK. It was shown in competition at the San Sebastian International Film Festival and was nominated for the Prix Louis-Delluc. Some critics found the film over long, slow, and pretentious, while others said it was moving, intelligent, and among Rivettes best work. The films subject led to comparisons to Vertigo, The Sixth Sense, and The Others.

==Plot==
Julien (Radziwilowicz) is a middle-aged clockmaker who lives alone with his cat in a large house in the Paris suburbs. Julien is blackmailing Madame X (Brochet) who is importing fake antique Chinese silks,  and may have murdered her sister.  By chance, he meets Marie (Béart), a beautiful young woman he last saw a year ago, and they begin a passionate relationship. Though elusive, Marie agrees to move in with him; she acts strangely at times and appears absent.    A mystery connects Marie to Madame Xs dead sister and in uncovering Maries secret Julien risks losing her.  The film is separated into four parts, named to reflect the narrative perspective.  

Julien: Julien dreams of Marie, whom he met just over a year before at a party, and with whom he would have begun a relationship but for them both having partners. He immediately runs into her on the street as she is running for her bus and he is off to meet Madame X.       They agree to meet again, but Marie fails to appear and he returns home to find Madame X waiting for him against their agreement, so he raises his price tenfold. Madame X returns the next day to try to bargain, and asks for a letter back that he does not have. Marie invites him to her place for dinner, where Julien tells her his girlfriend ran away with another man and Marie says her boyfriend Simon died six months ago. They have sex, but in the morning Marie has checked out of her apartment. Julien returns home to find that his house has been ransacked. He tries to find her by ringing her old boss, then tracks her down when an unknown woman calls to tell him the hotel Marie is staying at. Julien visits her there, and Marie agrees to move in with him.

Julien et Marie: Marie makes herself at home, trying on the clothes of Juliens old girlfriend,  exploring the house, and watching him at work. Their lovemaking is passionate,  but Maries behaviour is unusual. She is sometimes cold or  " to " ".  She is jealous of his ex,    compulsively decorates and rearranges a room in his attic,    feels compelled to act out her dreams,  and does not bleed when scratched  — something she keeps from Julien. She sees a girl in her dreams who shows her a "forbidden sign" with her hands.  Marie helps Julien in his blackmailing, and after meeting Madame X, who only knows of Marie as "lautre personne",    Marie is handed a letter by someone who says she is Madame Xs sister (Bettina Kee); she is the girl Marie dreamed of before.

Marie et Julien: The letter is from Madame Xs sister Adrienne to Madame X. Julien meets Madame X again, and she tells him her sister killed herself by drowning six months before. He cannot understand who gave Marie the letter, but she insists that her sister left the letter to frame her and although dead she is "reliving" (a Revenant (folklore)|revenant)    — and Marie is also. Adrienne —who though dead still appears and speaks to her— has told Madame X that Marie is "like me". He thinks she is mad. Julien becomes frustrated at Marie spending so much time alone in the attic. When she finally shows him the room, she says she does not know what it is for. She leaves before Julien wakes and checks into another hotel. He rings Maries old boss who suggests talking to Maries friend Delphine; Delphine says that Maries relationship with Simon drove Marie mad and ended their friendship.

Marie: Julien visits Marie and Simons old apartment, where the letting agent shows him a room that Julien chillingly recognises — it is identical to the room Marie has prepared. This is where Marie hanged herself, trying to frame Simon in revenge after a terrible row.   Julien returns home and Marie silently leads him to the attic where she has prepared a noose, feeling she has to hang herself again. Julien carries her downstairs, and they make love again. She leaves to meet Adrienne, who says that she knows that Marie no longer wants to die. They agree they do not know the rules of their situation.  Returning, Marie interrupts Julien about to hang himself in a desperate attempt to join her.  He runs to the kitchen and tries to slit his wrist; Marie stops him and her wrist and his palm are cut. Marie warns him that he will lose all memory of her, but he says that all he wants is for her to be there. Marie slowly covers her face with her hands —"the forbidden sign"— and Julien becomes oblivious to her and unaware of why he is bleeding.  Madame X arrives for her letter and he hands it over, confused by her enquiries about "lautre personne". Madame X burns the letter, freeing Adrienne. Marie cries while watching Julien sleep, and as her tears land on her wrist her cut bleeds. Julien wakes and asks who she is; she replies that she is "the one he loved". He doubts it as shes "not his type", but she says with a smile to give her a little time.

==Cast==
 
* Emmanuelle Béart as Marie Delambre.
* Jerzy Radziwiłowicz as Julien Müller.
* Anne Brochet as Madame X.
* Bettina Kee as Adrienne, the sister of Madame X.
* Olivier Cruveiller as Vincent Lehmann, Léditeur, Maries old boss.
* Mathias Jung as Le concierge, the desk clerk at Maries apartment.
* Nicole Garcia as Lamie, Maries friend.

==Themes and analysis==
  }} Michael Atkinson believed that Rivette was working in the "border world between narrative meaning and cinematic artifice". 

The emotional distance of the characters and the intellectual and artificial-seeming, quasi-theatrical dialogue is deliberate,   depicting their simultaneous connection and isolation.  The chasm between Marie and Julien, due to his corporeality and her ghostly nature, is emphasised in the contrast between his physical activity and her status as an onlooker.  Rivette says he wanted the lovers to appear ill-suited and for the viewer to question the relationship;  they love each other passionately yet they are essentially strangers.  Béart believes that Marie was more alive than Julien, and that he literally wakes up to her existence only at the very end of the film. 

Finally revealed to be a ghost story inspired by nineteenth-century French fantasy literature,   the film uses the conventions of the genre —that people who die in emotional distress or with an unfinished task may become ghosts— and openly details these conventions. Marie and Adriennes lives as revenants are reduced to a single purpose, each with only the memory of her suicide and her last emotions remaining.  Julien, like the audience, is eventually confronted with Maries nightmare of repetition.     Elements of the horror genre are used, not to scare but to explore memory and loss.  To stay with Marie, Julien first has to forget about her,  and at the end they have the promise of a new beginning.  Marie becomes a living person again rather than an object of fantasy.  Maries tears and blood are a miracle overcoming her death, and may reflect a fantasy of turning back the menopause.  The credits are accompanied by an upbeat jazz song performed by Blossom Dearie, Our Day Will Come,  that represents love as a pledge,  the only music used in the film. 

 
There is an aesthetic focus on Béarts body, Julien telling Marie that "I love your neck, your arms, your shoulders, your mouth, your stomach, your eyes -- I love everything." The focus is more than erotic as it symbolises Maries fight for corporeality.   The film includes Rivettes first ever sex scenes, one of them arranged by Béart. The five candid and emotionally charged sex scenes focus on their upper bodies and faces,   and on their erotic monologues that employ elements of fairy tale, horror, and sadomasochism.  

Béart is given an ethereal quality by Lubtchanskys cinematography and lighting,  and she subtly portrays Maries detachment and vulnerability.  In the latter part of the film Béart is dressed in grey and looks tired and wan, showing Maries ageing and angst.  Béart says she made deliberate use of silence in playing the part.  Radziwiłowiczs performance allows the viewer to sympathise with Julien despite the characters initial dislikeable nature.  Brochet as Madame X has a cool ease and grace. 

==Production==
 

===Original shoot===
Rivette originally began to make Marie et Julien, as it was then titled, in 1975 with producer Stéphane Tchalgadjieff as part of a series of four films called Les filles du feu, and later Scènes de la Vie Parallele.   Rivette said in 2003 that the film was based on the true story of a woman who committed suicide.  He first shot  ) in March&ndash;April and  ) in May, although the latter was not released, and the fourth film, a musical comedy meant to star Anna Karina and Jean Marais, was never shot. Filming began on Marie et Julien that August, with Albert Finney and Leslie Caron in the lead roles and Brigitte Rouan as Madame X,    but after two days Rivette gave up filming due to nervous exhaustion.  He later used the names of the lovers, Marie and Julien, in his 1981 film Le Pont du Nord.  

===Revisiting the screenplay=== polyglot cat", characters whose names change, a "suicide room" similar to The Seventh Victim, "Madame X", and an unknown "forbidden gesture" that the notes stated: "Do not forget". 

===Filming===
Rivette worked with scriptwriters   in May 2003 after filming ended. 

===Direction===
  horror genre —no music, shock sound effects, special effects, or gore— evoking feelings and scenes verbally rather than showing them,  but he does employ Hitchcockian "MacGuffins"  such as chance encounters, "clues," and the blackmail plot-line.    The use of dream-like sequences at the start and end of the film was influenced by Rivettes 1985 film Hurlevent, an adaptation of Wuthering Heights.     Some of the dialogue that was in the original notes was read as though quoting. Interview with Jacques Rivette, DVD special feature. 

Glenn Kenny notes that the "calm precision" of the mise en scène in the opening dream sequence "put   under such a powerful spell" that it lasted the whole film.  Throughout the film, everyday sounds are amplified by a lack of music, and the film uses sweeping long shots,  and several incidental scenes of Julien working, talking with his cat,  and of the characters sleeping.  Slant Magazine commented that the cat is the films most interesting character,    and Philippa Hawker of The Age notes that "this has one of the best sequences involving a cat on film."  The camera follows the cat and films it looking directly at the camera, giving a sense of artistic freedom and spontaneity. 

The cinematography shows Rivettes interest in visual texture. The colours are natural, except in certain scenes like the initial dream sequence that are filmed in vivid colours.  The lighting when Marie is arranging the attic room changes from shadow to warm light when she places an oil lamp on a stool to indicate that she has placed it correctly, introducing a supernatural element that contrasts with the realism of the rest of the film.  

==Reception==
Critics responses were mixed: some found the film evocative and powerful, whereas others saw it as slow and frustrating. Guy Austin writing in Scope noted that "bodily reactions are not part of critical reactions to  . In both press and online, the head governs the body in reactions to Rivette."  Rivette had said before the release that "This I know in advance – whether it is good or not, some people will love it and others will hate it." 

{{quotebox|align=right|width=25%|bgcolor=#c6dbf7|quote=Q: "Were you hurt by the bad reception for Histoire de Marie et Julien?" A: "You always wish there were more of a response. But often it comes five, ten years down the road. As it turns out, for Marie et Julien, Im starting to get a sense these days of some change of heart."
|source=—Interview with Jacques Rivette in 2007 }}
It was nominated for the 2003 Prix Louis-Delluc.    Senses of Cinema suggested that it is Rivettes most important work since his 1974 film Celine and Julie Go Boating and saw it as "a film about filmmaking",  including it in their favourite films of 2004.  DVD Verdict concluded that "it is not only intelligent, but willing to assume the same of its audience".  Glenn Kenny rated it as his favourite film of the decade,    and film curator Miriam Bale writing in Slant Magazine included it in her ten most enduring films of the decade.  Film Comment was equally taken with the film, stating that "whats most remarkable about the film is how moving it is finally, how much is at stake after all--nothing Rivette has done before prepares you for the emotional undertow that exerts itself in The Story of Marie and Juliens final scenes."  LA Weekly described the film as "elegant and unsettling";  The Age called it "quietly mysterious and haunting"  and "heartrending". 
 Time Out The Others, but said that "its glacially slow pace will frustrate all but the most patient".    (Rivette said when promoting the film that "I like The Sixth Sense because the final twist doesnt challenge everything that went before it. You can see it again, which I did, and its a second film thats just as logical as the first one. But the end of The Others made the rest of it meaningless." ) "An intellectual exercise in metaphysical romance - Ghost (1990 film)|Ghost for art-house audiences" was Empire (magazine)|Empires wry take.  The Digital Fix argued that Rivettes direction resulted in a product that "if never exactly dull and certainly the work of a master, is ultimately an empty film that has nothing to offer but its own cleverness".  Keith Uhlich of Slant Magazine found it was "a lesser Rivette offering — a watchable, ultimately unfulfilling ghost story". 

==Distribution== Cannes and Venice Film Festival|Venice,    then premièred at the Toronto International Film Festival on 10 September 2003.    It was shown in competition at the San Sebastian International Film Festival later that month,  as well as at the 2004 Melbourne International Film Festival     and the 2004 International Film Festival Rotterdam,  among others.  The film opened in France and Belgium on 12 November 2003; that night 239 people watched the film in Paris.  The cinema release was on 26 August 2004 in Germany,  and on 8 October 2004 in the UK,   but there was no US cinema release.   

The DVD was released on a two-disc set by Arte Video in France on 18 May 2004,    and features the theatrical trailer, actor filmographies, a 40-minute interview with Rivette, covering the films origin, mythology, narrative viewpoints and relations to his other films, and a 15-minute interview with Béart, covering working under Rivettes direction and how the experience of acting in the film compared to her earlier role in La Belle Noiseuse.        The US and UK distributions, respectively released on 12 July 2005 by Koch Lorber Films and 28 February 2005 by Artificial Eye, come with optional English subtitles and the special features on a single disc.   The Arte Video release additionally features commentary by Lubtchansky over a cut-down (41:45 minute) version of the film, and an analysis of the film by Hélène Frappat (21:28 minutes).    The film was also released with Un Coeur en Hiver and Nathalie... in "The Emmanuel Beart Collection" by Koch Lorber in 2007.  

==References==
 

==Further reading==
*  
*  
*  

==External links==
*  
*   at Rotten Tomatoes
*   on Cinemovies (in French, Flash video)

 

 
 
 
 
 
 
 
 
 