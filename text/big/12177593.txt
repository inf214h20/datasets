Pellaindi Kaani
{{Infobox film
| name           = Pellaindi Kaani
| image          =
| caption        =
| director       = E. V. V. Satyanarayana
| producer       = M. Narasimha Rao
| writer         =
| narrator       = Sunil
| music          =
| cinematography =
| editing        =
| distributor    = Raasi Movies
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         = 2 crores
| website        =
}}
 Telugu film directed by E. V. V. Satyanarayana. The film was released in August 2007.

==Plot==
Pellaindi Kani portrays what happens between two bumps to the head of the main protagonist Attchi Babu (Allari Naresh). With the first bump, at the age of 10, he loses his mental balance. Twelve years later he regains his sanity with another bump to his head.
 Chandra Mohan) who is in need of money to get his heart operated. Bhanupriya knows that no sensible girl would ever wish to marry an abnormal person. Yet for the love of his son she arranges for the marriage. Kamalinee Mukherjee agrees for the marriage to save her father. She also hopes that someday Allari Naresh would become normal.

But Bhanupriyas brothers (Kota Srinivasa Rao and Krishna Bhagavaan) are working overtime to finish off both the mother and son to acquire their vast property. Will they succeed? Will Attchi Babu father a child? Pellaindi Kani has the uninteresting answers.

==Cast==
* Allari Naresh ... Attchi Babu
* Kamalinee Mukerji ... Heroine
* Krishna Bhagavaan ... Sunil ... Hero Friend
* Harish Kumar... Second Hero

==Crew==
* E V V Satyanarayana... Director
* Kamalakar, Jayasri... music directors
* M.Narasimha Rao... producer
* raasi movies... banner

==External links==
*  

 

 
 
 


 