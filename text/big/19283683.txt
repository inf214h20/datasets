Shibaji
{{Infobox film
| name = Shibaji
| image =
| caption        =
| director = Babu Ray & Kamal Sarkar
| writer = Manjil Banerjee
| starring = Prosenjit Swastika Mukherjee Tathoi Deb
| producer = Prasanta Bhattacharjee(Agartala) & Shibu Das(Kolkata)
| distributor =
| cinematography =
| editing =
| released = 8 August 2008
| country        = India
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| runtime = 165 min Bengali
| music = Bappi Lahiri
| website        =
}} Bengali film Directed by Babu Ray.

==Plot==
The story of this film revolves around the main character Shivaji (Prosenjit) who is a goon. He was hired by the villain Bishal Sarkar to kill the judge Prasanta Mullick (Ranjit Mullick) as he refuses to receive the bribe and announces death sentence for his youngest brother, Vicky Sarkar. Shivajis wife Durga (Swastika Mukherjee) on the other hand being insulted and humiliated again and again by people tries to commit suicide. Shivaji ultimately saves her life and leaves all criminal activities. Prasanta Mullick and his wife (Mousumi Saha) help him to start a business of a fast-food centre. But one day Bishal Sarkars brother along with goons attack the restaurant and kills a person. Inspector Satyaprakash (Tapas Paul) arrests innocent Shivaji and he was imprisoned. Prasanta Mullick resigns from the post of a judge, and tries to prove Shivaji innocent. In this mean time the three brothers of Bishal Sarkar attacks Shivajis house, kills his daughter Tumpa and rapes his wife. Durga commits suicide. To seek revenge Shivaji escapes from the police custody and starts killing Bishal Sarkars brothers one by one. He also kidnaps Insp. Satyaprakashs daughter Jaya (Tathoi). Jaya was a lonely child as her father and mother (Satabdi Roy) none has time for her. She was brought up by her Appa (Chumki Choudhury). Shivaji starts loving her as his daughter, and Jaya also forgets her loneliness. But police separated them and produces Shivaji in court. Suddenly Bishal Sarkar kidnaps Jaya and Shivaji saves her. But while doing so he gets injured though kill all villains. Doctors declare him dead but Tathoi or Jaya magically saves his life by singing in front of God and after 20 years with the marriage of Jaya the movie ends.

==Cast==
*Prosenjit
*Tathoi
*Swastika Mukherjee
*Tapas Paul
*Satabdi Roy
*Ranjit Mullick

==Crew==
*Presented by: Kamakkhya Films 2000
* Producer(s)prasanta Bhattacharjee & Shibu Das:
* Director: Babu Roy/ Kamal Sarkar
*Story:
*Production Design:
*Dialogue:
*Lyrics:
*Editing :Atish De Sarkar
*Cinematography:Niranjan Das
*Fight Composer: Ramu Kumar Shanu, Shreya Ghoshal, Alka Yagnik, Anik Dhar, Sumedha and Madhuchhanda

 
 

==External links==
* 
* 
* 
* 

==References==
 

 

 
 
 
 


 