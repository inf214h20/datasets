Holidays on the River Yarra
 
{{Infobox film
| name           = Holidays on the River Yarra
| image          = 
| caption        = 
| director       = Leo Berkeley
| producer       = Fiona Cochrane
| writer         = Leo Berkeley Craig Adams
| music          = 
| cinematography = Brendan Lavelle
| editing        = Leo Berkeley
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Australia
| language       = English
| budget         = A$425,000 
| gross = A$24,600 (Australia)
}}

Holidays on the River Yarra is a 1991 Australian drama film directed by Leo Berkeley. It was screened in the Un Certain Regard section at the 1991 Cannes Film Festival.   

==Cast== Craig Adams - Eddie
* Luke Elliot - Mick
* Alex Menglet - Big Mac
* Tahir Cambis - Stewie
* Claudia Karvan - Elsa Ian Scott - Frank
* Sheryl Munks - Valerie
* Angela McKenna - Mother
* Chris Askey - Mercenary
* John Brumpton - Mercenary
* Jacek Koman - Mercenary
* Eric Mueck - Billy
* Justin Connor - Danny
* Leong Lim - Shopkeeper
* Robert Ratti - Nick
* Arpad Mihaly - Chef

==Box office==
Holidays on the River Yarra grossed $24,600 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 