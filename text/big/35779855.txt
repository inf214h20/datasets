Cofralandes, Chilean Rhapsody
{{Infobox film
| name           = Cofralandes, Chilean Rhapsody
| image          =
| caption        =
| director       = Raúl Ruiz (director)|Raúl Ruiz
| producer       =
| writer         = Raúl Ruiz (director)|Raúl Ruiz
| starring       =
| music          = Jorge Arriagada
| cinematography = Inti Briones
| editing        =
| distributor    =
| released       =  
| runtime        = 312 minutes
| country        = Chile, France
| language       = Spanish, French
| budget         =
}}

Cofralandes, Chilean Rhapsody ( ) is an experimental four-part 2002 Franco-Chilean digital video series written and directed by Raúl Ruiz (director)|Raúl Ruiz.    The first part won a FIPRESCI Award at the Montreal World Film Festival in 2002 "for the directors personal exploration into his homeland, using DV in a rigorous yet playful manner".   

==Cast==
* Bernard Pautrat as Bernard
* Raúl Ruiz (director)|Raúl Ruiz as narrator
* Malcolm Coad as English journalist
* Rainer Krause as German artist
* Ignacio Agüero as Rafael
* Marcial Edwards as guide in the Museum of Nothing
* Miriam Heard as consular officer
* Javier Maldonado as guide in the Museum of the Sandwich
* Mario Montilles as old Rafael
* José Luis Barba as Cuban schoolteacher
* Néstor Cantillana as country bumpkin
* Amparo Noguera as patriotic priests realist sister
* Isabel Parra as Death
* Ángel Parra as Our Lord Jesus Christ Francisco Reyes as patriotic priest
* Luis Villaman as Don Marat the schoolteacher

==References==
 

==External links==
*  
*   by Clemente Sobourin

 

 
 
 
 
 
 