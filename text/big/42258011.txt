Kalam Vellum
{{Infobox film
| name =     Kalam Vellum
| image =
| caption =
| director =M. Karnan
| writer =     Mathangan
| starring =  Jaishankar Vijaya Kumari Gandhimathi Nagesh  O.A.K. Devar Vijaya Lalitha M.R.R.Vasu
| producer = M. Karnan
| music =     Shankar Ganesh
| editor =
| studio = Indirani Films
| country = India
| released =  
| runtime =
| language = Tamil
| budget =
}} Tamil thriller directed by  M. Karnan, released in 1970. The film follows a "Robin Hood"-like figure.

== Plot ==

Velu Jaishankar a poor farmer, loses his sister Dhanam due to the atrocities and exploitation of landlord Periyasamy. To avenge his sisters death, Velu kills Periyasamys brother. He then escapes from there. He joins a gang of dacoits, headed by Narasingam. The gang is initially perturbed by Velus presence, but his courage and good nature wins him their love and respect. Velu succeeds Narasingam as gang leader. His aim is to rob the rich and save the poor. He is constantly looking for an opportunity to take revenge against Periyasamy. Velu forgets that his wife is waiting for his return to his village. Finally, Velu takes revenge on Periyasamy and surrenders to police.

== Soundtrack ==

{{tracklist
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length =
| title1       = Ellorum Thirudargale
| extra1       = P. Susheela
| lyrics1      = Kannadasan
| length1      =
| title2       = Ennanga Sambandhi Eppo Namma Sambandham
| extra2       =  P. Susheela, T. M. Soundararajan, P.Mathuri
| lyrics2      =  Kannadasan
| length2      =
| title3       = Maalaiyittom Pongalittom
| extra3       =Sirkazhi Govindarajan, L. R. Eswari, Chorus
| lyrics3      =  Kannadasan
| length3      =
| title4       = Penn Oru Kannadi
| extra4       = L. R. Eswari
| lyrics4      =  Kannadasan
| length4      =
}}

== References ==

 

 
 
 
 
 


 