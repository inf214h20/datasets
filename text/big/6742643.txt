Artists and Models
 
{{Infobox film
| name = Artists and Models
| image = artistsandmodels.jpg
| director = Frank Tashlin Herbert Baker Hal Kanter
| starring = Dean Martin Jerry Lewis Dorothy Malone Shirley MacLaine Eva Gabor Anita Ekberg Eddie Mayehoff
| producer = Hal B. Wallis
| distributor = Paramount Pictures
| released =   
| runtime = 102 minutes
| language = English
| gross = $3.8 million  1,031,433 admissions (France)  
}}
 Paramount musical comedy in VistaVision and marked Martin and Lewiss fourteenth feature together as a team.  The film co-stars Shirley MacLaine and Dorothy Malone, also featuring Eva Gabor and Anita Ekberg in brief roles.

==Plot==
Rick Todd (Dean Martin) is a struggling painter and smooth-talking seduction|ladies man.  His goofy young roommate Eugene Fullstack (Jerry Lewis) is an aspiring childrens author who has a passion for comic books, especially those of the mysterious and sexy "Bat Lady."

Each night, Eugene has horrific screaming nightmares inspired by those ultra-violent comics, which he describes aloud in his sleep. They are about the bizarre bird-like superhero "Vincent the Vulture" who is, according to Eugenes nocturnal babblings,  the "defender of truth and liberty and a member of the Audubon Society" and is "half-boy, half-man, half-bird with feathers growing out of every pore" and a "tail full of jet propulsion."  Also known as "Vultureman" or more simply "The Vulture", the golden helmeted hero soars through space from his "homogenized space station" orbiting the Milky Way to battle his shapely but sadistic purple-eyed archenemy "Zuba the Magnificent," who hates Vincent because "shes allergic to his feathers" and who enjoys blasting big "oooozing" holes into his highly resilient flying form ("Itll take more than that to stop me!") with her "atomic pivot gun."
 New York comic book company called Murdock Publishing and is the creator of the "Bat Lady."  Her energetic horoscope-obsessed roommate is Bessie Sparrowbush (Shirley MacLaine), who is secretary to her publisher Mr. Murdock (Eddie Mayehoff) and Abigails model for the flying bat-masked superheroine.  Bessie develops a crush on Eugene, who is unaware that she is his beloved "Bat Lady" in the flesh.

Abigail becomes frustrated at work at the increasingly lurid and bloodthirsty stories the money-hungry Murdock demands. She quits to become an anti-comics activist, dragging Eugene into her crusade as an example of how trashy comic books can warp impressionable minds at the same time that Rick gets a job with the company after pitching the adventures of "Vincent the Vulture" from Eugenes dreams.  Rick  attains success at his new job, but after falling for Abigail he keeps his work a secret from both her and Eugene.

Unbeknownst to all, Eugenes dreams also contain the real top-secret rocket formula "X34 minus 5R1 plus 6-X36" that Rick publishes in his stories.  With spies all around them, they manage to entertain at the annual "Artists and Models Ball" and capture the enemy, preserving national security.

==Cast==
* Dean Martin as Rick
* Jerry Lewis as Eugene
* Shirley MacLaine as Bessie
* Dorothy Malone as Abigail
* Eva Gabor as Sonia
* Anita Ekberg as Anita

==Production==
Martin and Lewis fourteenth feature, Artists and Models was filmed from February 28 to May 3, 1955 at Paramount Studios. Neibaur, James L. and Okuda, Ted, The Jerry Lewis Films, An Analytical Filmography of the Innovative Comic, Pages 98-103.  McFarland & Company, Inc., 1995.  The film was released on November 7, 1955 by Paramount Pictures, and was one of the teams highest-budgeted pictures at $1.5 million ($12,589,869.40 in 2011 dollars). The film was shot in VistaVision and Eastmancolor, with prints by Technicolor, and stereophonic sound by Perspecta. Costumes were by Paramount wardrobe designer Edith Head. 

Artists and Models marked the first time Lewis worked with former Looney Tunes director Frank Tashlin, whom he admired greatly.   Martin and Lewis would reunite with him on their last film, Hollywood Or Bust, and Lewis would then work with Tashlin on six of his solo films.

Producer Hal B. Wallis chose Tashlin for Artists and Models on the basis of his background as a cartoonist, and the film contains many gags influenced by the directors animation work. When MacLaine kisses Lewis in front of a water cooler, the water steams up; in another scene, a massage therapist bends Lewiss leg all the way towards his head. Artists and Models is considered a milestone in movie satire for its mockery of mid-1950s pop culture. One scene satirizes the Kefauver hearings on violent comic books, and other targets in the film include the Cold War, the space race and the publishing business.

Tashlin brought a lot of sexual innuendo to Artists and Models, making it more adult in content than most previous Martin and Lewis movies and indulging his own fetishistic fascination with female characters in revealing costumes. Some of his most suggestive ideas were disallowed by the Production Code; in Tashlins original script, Lewiss character was named "Fullstick," but the censors ordered the removal of this phallic joke. The censors also asked Paramount to cut a scene where Dorothy Malone is seen wearing only a strategically placed towel, but the studio did not remove it. The finished film contains many jokes that push the boundaries of what was acceptable in the mid-50s, including many about womens breasts and a number of double entendres.
 Herbert Baker a 1958 Jerry Lewis film. 
 Jack Brooks, and included "When You Pretend", "You Look So Familiar", "Innamorata (Sweetheart)", "The Lucky Song", and "Artists and Models." A sixth number, sung by Shirley MacLaine during the party, entitled "The Bat Lady", was cut from the final edit. 
 All in a Nights Work, What a Way to Go! and Cannonball Run II.
 Scared Stiff. Scared Stiff.

The cast is filled with cameos by many Martin and Lewis regulars.  Eddie Mayehoff made his cinematic debut in Thats My Boy (1951 film)|Thats My Boy and co-starred in The Stooge. Kathleen Freeman also appeared in 3 Ring Circus, along with a number of Lewis solo films.  Jack Elam was in the teams second-to-last picture, Pardners.  Anita Ekberg would appear in Martin and Lewis final film, Hollywood Or Bust.

The spaceship model seen in the foreign experimental laboratory is actually a leftover miniature from Paramounts 1955 film, Conquest of Space, directed by George Pal.  
 pilot for the Get Smart television series.

==DVD release==
The film was included on a five-film DVD set, the Dean Martin and Jerry Lewis Collection: Volume Two, released on June 5, 2007.

==References==
 

== External links ==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 