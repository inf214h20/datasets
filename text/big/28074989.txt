The Last Lion
 
 
For the Churchill biography, see  
{{Infobox film
| name           = The Last Lion
| image          = "The_Last_Lion"_(1972).jpg
| image_size     = 
| caption        = 
| director       = Elmo De Witt
| producer       = 
| writer         = Wilbur Smith
| based on = 
| narrator       = 
| starring       = Jack Hawkins
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1972
| runtime        = 90 mins
| country        = South Africa
| language       = English
| budget         = 
| gross          = 
}} South African A Time to Die. 

==Plot==
Ryk Mannering, a terminally ill American millionaire, goes to Africa on a final hunting expedition to track down and shoot a mamil lion. He hires a private doctor to keep him alive and pays a local hunter to help him track down the lion.

==Poster tagline==
"For one it will be the last kill!"  

==Cast==
* Jack Hawkins - Ryk Mannering
* Karen Spies - Doctor
* Dawid Van Der Walt - David Land

==Release==
Copies of the film are preserved at the National Film, Video and Sound Archives, Pretoria, South Africa. www.national.archives.gov.za

The film has been released on DVD in February 2011.

==References==
 

==External links==
* 
*  at Mnet Corporate
 

 
 
 
 
 
 


 
 