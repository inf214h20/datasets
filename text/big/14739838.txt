Listen, Judge
{{Infobox Film |
  | name           = Listen, Judge
  | image          = Stooges Listen Judge poster.jpg
  | image size     = 190px
  | director       = Edward Bernds
  | writer         = Elwood Ullman John Hamilton
  | cinematography = Ellis W. Carter  
  | editing        = Edwin H. Bryant 
  | producer       = Hugh McCollum
  | distributor    = Columbia Pictures 
  | released       =  
  | runtime        = 17 01"
  | country        = United States
  | language       = English
}}

Listen, Judge is the 138th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are repairmen who are on trial for stealing  )]]

The trio comes upon a lady customer (Kitty McHugh) whose doorbell is in need of repair. The Stooges manage to ruin most of the house while working on the wiring, ultimately clobbering the chef (Emil Sitka) who is preparing dinner at the customers house. The irate chef abruptly quits, resulting in the Stooges being hired to prepare dinner for her husbands friends birthday party. To their shock and horror, the party in question is for none other than Judge Henderson, and their lady customer is the Judges wife.
 town gas through the gas stoves connection. 

During the party, Judge Hendersons friend blows out the candles, and the gas-filled cake explodes. The judge angrily realizes who the new "help" are, and the Stooges are forced to leave in a hurry.

==Production notes==
Filmed in November 1951, Listen, Judge is a hybrid of plot devices borrowed from several Stooge films featuring Curly Howard:
* The chicken stealing sequence in the courtroom is from A Plumbing We Will Go.
* The doorbell repair sequence is from They Stooge to Conga.
* The food preparation sequences and exploding cake (comprising the second half of the film) are from An Ache in Every Stake. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 