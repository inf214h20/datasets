Sing (1989 film)
{{Infobox Film
| name           = Sing
| image          = Sing (1989 film).jpg
| image_size     = 
| caption        =  Richard J. Baskin
| producer       = Craig Zadan
| writer         = Dean Pitchford
| starring       = Lorraine Bracco Peter Dobson Jessica Steen
| music          = Jay Gruska
| cinematography = Peter Sova
| distributor    = TriStar Pictures
| released       =     
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $11.5 million Holden, Stephen.   The New York Times (January 25, 1989). Retrieved January 17, 2011. 
| gross          = $2,260,616 
}} 1989 film, starring Lorraine Bracco, Peter Dobson and Jessica Steen. The movie is about a fictional New York City SING! production. Supporting cast includes Louise Lasser, George DiCenzo, Patti LaBelle, Yank Azman, Ingrid Veninger and Cuba Gooding Jr.

The film was written by Dean Pitchford (who also co-wrote the songs) and produced by Craig Zadan; both previously collaborated on Footloose (1984 film)|Footloose. 

Despite numerous petition drives, the film has never been released on DVD.

==Plot==
The story begins with Hannah, a young Jewish teen, as she is completing her senior year of high school. Her Brooklyn neighborhood is falling apart and SING! is one of the only traditions that keep the neighborhood alive. Newly arrived teacher, Miss Lombardo grew up in the neighborhood and returned to be their SING! leader. One cold Christmas night, Miss Lombardo is leaving a neighborhood party when a young man hails her a cab, then attempts to mug her. In self-defense, she bites his hand to release his grip and he screams in pain and terror and flees. The cab driver jokes about not starting the meter yet.

On the first day back at school, Miss Lombardo runs into difficulty when her students are uninterested and misbehaved. One such student, named Dominic, gets scolded for bringing stolen watches to school and putting his feet up on the desk. On the day of SING! leader elections, Miss Lombardo recognizes Dominic as her mugger by the bandage on his hand and decides to blackmail him into being co-SING! leader of the Senior class along with Hannah, who was rightfully elected. The school kids work hard to plan their SING! productions. Hannah and Dominic clash along the way as Hannah uses traditional SING! planning strategies while Dominic wants to introduce the flavor of the youth in the neighborhood. In order to put Dominic and Hannah on the same page, Miss Lombardo suggests that Hannah accompany Dominic to a local club. At first, the two are equally hesitant but Hannah agrees on the terms that it is not a date. However, by the end of the night, Hannah uses Dominic to make her ex-boyfriend, Mickey, jealous, and due to this, Hannah and Dominic start seeing each other in a different light. Dominic accompanies Hannah on her walk home and the two share a romantic kiss. Once the two are finally uniting and getting along, the Dept. of Education informs the school that it will close forever at the end of the semester and therefore, there wont be enough resources for them to complete this years SING!. This fuels the kids to work even harder on their productions and the neighborhood comes together even more to help finance the show, despite the school authorities ban. Ironically, just as things are starting to look up, Dominic reluctantly accompanies his brother on a robbery of Hannahs mothers diner, their sole source of income which already was at risk of failure due to the schools upcoming closure. A classmate sees Dominic standing outside the diner at the time of the crime and informs Hannah about it. Devastated, Hannah confronts Dominic and he promises to get the money back for her. He then steals the money from his brother and returns it to the diner, restoring Hannahs faith in him. The recent events have discouraged Dominic from fulfilling his co-SING! leader duties and he had been skipping out on rehearsals. In a moment of great need as the seniors main performer is injured and knocked unconscious, Dominic steps in to save the show. He sheds his bad-boy demeanor and exceeds all expectations. The underclassmen and seniors perform to a record-high sold-out audience. At the end of the show, Hannah makes a moving speech motivating the community to rejoice and always remember that despite compromising circumstances, they completed a successful SING! and proved their communitys worth.

==Soundtrack== Mickey Thomas - "Sing" (Dean Pitchford, Jonathan Cain, Martin Page) 4:52
# Johnny Kemp - "Birthday Suit" (Dean Pitchford, Rhett Lawrence) 4:30
# Paul Carrack & Terri Nunn - "Romance (Love Theme from Sing)" (Dean Pitchford, Patrick Leonard, Paul Carrack) 4:30
# Nia Peeples - "You Dont Have To Ask Me Twice" (Dean Pitchford, Tom Snow) 3:35
# Michael Bolton with the cast of Sarafina! - "One More Time" (Dean Pitchford, Tom Snow) 4:48
# Bill Champlin - "Somethin To Believe In" (Dean Pitchford, Desmond Child, Diane Warren) 3:29 Tom Kelly) 3:56
# Kevin Cronin - "(Everybodys Gotta) Face The Music" (Dean Pitchford, Richard Marx) 4:10
# Laurnea Wilkerson - "Whats The Matter With Love?" (Dean Pitchford, Tom Snow) 3:57
# Art Garfunkel - "Well Never Say Goodbye" (Dean Pitchford, Tom Snow) 2:57

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 