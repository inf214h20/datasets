Avan Chandiyude Makan
{{Infobox film 
| name           = Avan Chandiyude Makan
| image          =
| caption        =
| director       = Thulasidas
| producer       =
| writer         =
| screenplay     = Vijayaraghavan Vakkom Jayalal
| music          = Sanjeev Lal
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 2007 Cinema Indian Malayalam Malayalam film, Vijayaraghavan and Vakkom Jayalal in lead roles. The film had musical score by Sanjeev Lal.   

==Cast==
*Prithviraj Sukumaran Vijayaraghavan as Chandy
*Vakkom Jayalal Rekha
*Kalasala Babu Baburaj
*Cochin Haneefa

==Soundtrack==
The music was composed by Sanjeev Lal and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kuruthola || Afsal, Sabitha, Sobha || Gireesh Puthenchery || 
|-
| 2 || Maampoo Pookkum || Jyotsna, Anwar Sadath || Gireesh Puthenchery || 
|-
| 3 || Manthrakkolusu || MG Sreekumar, Sujatha Mohan || Gireesh Puthenchery || 
|-
| 4 || Seenaimaamalakal Vaazhthum || Adolf Jerome, PV Preetha || Gireesh Puthenchery || 
|}

==References==
 

==External links==

 
 
 

 