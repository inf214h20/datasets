Funeral for an Assassin
 
 
{{Infobox film
| name           = Funeral for an Assassin
| image          = Funasspos.jpg 
| image_size     =
| caption        = Original film poster
| director       = Ivan Hall
| producer       = Walter Brough (producer) Ivan Hall (producer)
| writer         = Walter Brough (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Koos Roets
| editing        = George Hivley
| distributor    =
| released       = 1974 (RSA) 1977 (USA)
| runtime        = 92 minutes
| country        = South Africa
| language       = English/Afrikaans
| budget         =
| gross          =
}}

Funeral for an Assassin is a 1974 South Africa film directed by Ivan Hall that was released in the United States in 1977.

==Plot==
Michael Cardiff is a professional revolutionary highly trained in a variety of techniques of assassination, infiltration and evading law enforcement.  After escaping from prison he places identification items on a decomposed body to make him appear dead as he plans his revenge against the government. Cardiff uses his skills to murder a prominent judge making his death look like an accident in order to plant an improvised explosive device at his funeral attended by the movers and shakers of the regime.  Only one non conformist police captain is on to his plans.

==Cast==
*Vic Morrow as Michael Cardiff
*Peter Van Dissel as Capt. Evered Roos
*Gaby Getz as Julia Ivens Sam Williams as Umzinga Stuart Parker as Commandant Overbeek
*Gillian Garlick as Nurse Schoenfeld
*Siegfried Mynhardt as Judge William Whitfield
*Norman Coombes as Fourie
*Chris Bezuidenhout as Karl Yates
*Albert Raphael as Claude Ormsby Bruce Anderson as Prime Minister
*Henry Vaughn as Reverend Martin Hemsley Gwynne Davies as Magdalena Stewart
*Nimrod Motchabane as Black Policeman John Boulter as Surgeon
*DeWet Van Rooyen as Minister
*Michael Lovegrove as D.I.S. Inspector
*Johan Brewis as T.V. Announcer Michael Jameson as D.I.S. Agent

==External links==
* 
* 

 
 
 
 
 
 
 


 