Bears and Bad Men
 
{{Infobox film
| name           = Bears and Bad Men
| image          =
| caption        =
| director       = Larry Semon
| producer       = Albert E. Smith
| writer         = Larry Semon
| starring       = Stan Laurel
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 silent comedy film directed by Larry Semon    and featuring Stan Laurel.

==Cast==
* Larry Semon - Larry Cutshaw
* Madge Kirby - The Slawson Daughter
* Stan Laurel - Pete William McCall - Stranded actor (as Billy McCall)
* Blanche Payson - Maw Cutshaw Frank Alexander - Paw Slawson
* William Hauber
* Pete Gordon - Paw Cutshaw
* Mae Laurel - Scared Woman
* Bessie the Bear
* Brownie the Bear

==See also==
* List of American films of 1918
* Stan Laurel filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 