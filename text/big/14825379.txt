Vigathakumaran
 
{{Infobox film
| name           = Vigathakumaran (The Lost Child)
| image          = Vigathakumaran.jpg
| caption        = A scene from the film
| director       = J. C. Daniel
| producer       = J. C. Daniel
| writer         = J. C. Daniel
| starring       = J. C. Daniel P K Rosy|P. K. Rosy Johnson Sundar Raj
| music          =
| cinematography =
| editing        = J. C. Daniel
| studio         = Travancore National Pictures
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam (Silent)
| budget         = 4 lakh (British Indian Rupee)
| gross          =
}} Malayalam feature film and J. C. Daniel is considered as the father of Malayalam film industry for this work.

==Plot==
Chandrakumar, son of a rich man in Trivandrum is kidnapped by the villain Bhoothanathan to Ceylon. The efforts of his parents to find him do not succeed and Chandrakumar is brought up as a labourer in an estate. The estate owner, who is British, takes a liking to him and in time, Chandrakumar rises to the post of Superintendent. At this time Jayachandran, a distant relative of Chandrakumar happens to come to Ceylon. Incidentally, he is robbed of all his belongings by Bhoothanathan. Stranded, he gets acquainted with Chandrakumar and they become close friends. They come to Thiruvananthapuram where Chandrakumars sister falls in love with Jayachandran. Meanwhile Bhoothanathan attempts to kidnap her and the duos timely intervention saves her. A scar on the back reveals Chandrakumars identity which eventually leads to the happy reunion of the family.

==Cast==
* J. C. Daniel as Jayachandran
* P K Rosy|P. K. Rosy as Sarojini
* Johnson as Bhoothanathan
* Sunder Raj as Chandrakumar

==Production==
 

===Pre-production===
Daniel was interested in martial arts and was an expert in chelambatam, the traditional martial art of southern Travancore. He published an English book titled Indian Art of Fencing and Sword Play in 1915, when he was 22.  Daniel was well aware of the scope of cinema as a public medium. He wished to popularise chelambatam by harnessing the popular influence of cinema. At that time the common mass of Kerala were not even aware of cinema, hence the idea was quite a challenge. He took the challenge and left to Madras (now Chennai) to learn techniques of film-making and to acquire necessary equipments.  Madras was the budding centre of film production in South India and had the only permanent talkies in South India, named Gaiety which was established in 1912. However, he could not get what he wanted from Madras and was even denied permission to enter various studio premises in there. That didnt make him to give up. He travelled to Bombay (now Mumbai), the centre of Hindi cinema production. He asked the studio owners for entry claiming that he is a teacher from Kerala and wanted to teach his students about cinema and got entry to the studios there. He could gather enough knowledge and equipments for film production from Bombay and came back to Kerala to fulfil his dream. cinemaofmalayalam.net -    

===Casting and filming===

In 1926, Daniel established the first  . The theme of the film was of social significance and was one of the early films in that genre. Most of the Indian films at that time were based on stories from the puranas and films with social themes were scarce. 

The first Malayalam actress was a scheduled caste labourer named P K Rosy|P. K. Rosy from a place called Thayycaud near Trivandrum.the cinematographer of the film was lala(a Britisher).  She used to come with lunch to act in the movie and go for her work in the evening. Daniel had earlier signed an actress from Bombay named Lana to act in the heroine role. Due to some reasons, she left the film and Daniel was forced to sign an inexperienced actress like Rosy in his film. Another important role was played by Johnson, who is the father of actress B. S. Saroja. Daniels friend Sundar Raj also acted in a pivotal role in the film.

== Release and post-release events ==
 
Vigathakumaran was exhibited in   groups in   at the Star Theatre. Alleppey being one of the most important port towns in Kerala during that time, the audience were more liberal. They received the movie with exhilaration. There was a minor glitch when the screen faded and the audience booed. The announcer explained that since this is the first Malayalam film, there will be some minor problems and the audience received the statement with applause. It is said that J.C.Daniel himself came to Alleppey with the film box, since there was only one print.  The film was also screened at Quilon, Trichur, Tellichery and Nagercoil. The film did only a moderate business at the box office and the collections were way less than the expenditure.

Females acting in films or theatre was considered at par with prostitution at the time. Angry at a Dalit lady portraying an upper caste Nair on screen,  some of the orthodox society burned down the heroine Rosys hut. She fled to Tamil Nadu where she got married and spent the rest of her life. Nobody knows the whereabouts of Rosy after she left Trivandrum.  It was only recently that her photo was retrieved from Malloor Govinda Pillais diaries.

After the films failure at the box office, Daniel suffered from debts and to overcome the situation, he had to sell his equipments and close down his studio.  Despite the setbacks, Daniel went on to make one more film, a documentary on martial arts, Adithadi Murai. He was completely bankrupt after the completion of this film. Almost a pauper, he left Trivandrum to seek a livelihood. Daniel spent the rest of his life as a dentist at Palayamkottai, Madurai and Karaikudi.

The Kerala Government initially rejected to give Daniel any honour because of Daniel was born and settled in Kanyakumari district which became the state of   as a part of the Kerala State Film Awards, to honour lifetime achievements in Malayalam cinema. Daniel is now known as the father of Malayalam cinema.

==Legacy==
A lot of literary and cinematic works have been made, based on the life of J. C. Daniel and the making of Vigathakumaran. Nashta Nayika is a novel by Vinu Abraham which details the life of P. K. Rosy, the heroine of Vigathakumaran. Saraswathy Nagarajan (11 October 2012).  . The Hindu. Retrieved 11 May 2013. 

In 2013,   and D. Babu Paul, former Chief Secretary of Kerala have pointed out factual inaccuracies in the films depiction of Malayattoor and Karunakaran.  

== References ==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 
 
 
 
 