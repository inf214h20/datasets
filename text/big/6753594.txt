La Villa Santo-Sospir
{{Infobox film
| name           = La Villa Santo-Sospir
| image          = 
| image_size     = 
| caption        = 
| director       = Jean Cocteau
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1952
| runtime        = 35 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

La Villa Santo-Sospir (1952 in film|1952) is a 35-minute amateur or home film directed by Jean Cocteau in which Cocteau takes the viewer on a tour of Francine Weisweillers villa on the French coast, a major location later used in his film Testament of Orpheus (1960). 

The house itself is heavily decorated, mostly by Cocteau (and a bit by Picasso), and we are given an extensive tour of the artwork. Cocteau also shows us several dozen paintings, most of which cover mythological themes. He also proudly shows paintings by Edouard Dermithe and Jean Marais and plays around his own home in Villefranche.

== External links ==
*  
*  

 

 
 
 
 
 
 

 