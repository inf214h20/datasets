The Last Valley (1970 film)
{{Infobox Film
| name           = The Last Valley
| image          = LastValley.jpg
| image_size     = 
| caption        = DVD cover
| director       = James Clavell
| producer       = James Clavell
| writer         = James Clavell J.B. Pick
| narrator       = 
| starring       = Michael Caine Omar Sharif John Barry
| cinematography = Norman Warwick John Wilcox John Bloom ABC Pictures Corporation MGM (2004, DVD)
| released       = 1971
| runtime        = 128 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $6,250,000 "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
| gross = $1,280,000 
| preceded_by    = 
| followed_by    = 
}} The Last 70 mm widescreen process; it was re-used to make the film Baraka_(film)|Baraka (1991).

==Plot==
"The Captain" (Michael Caine) leads a band of mercenaries who fight for the highest bidder regardless of religion. His soldiers  pillage the countryside, and rape and loot when not fighting. Vogel (Omar Sharif) is a former teacher trying to survive the slaughter of civilians occurring throughout south-central Germany. Vogel runs from The Captains forces, but eventually stumbles upon an idyllic mountain vale, untouched by war.

The Captain and his small band are not far behind. Trapped in the valley, Vogel convinces The Captain to preserve it and the village it shelters for their own benefit as the outside world faces famine and devastation. "Live", Vogel tells The Captain, "while the army dies." The Captain decides that his men will indeed rest here for the winter. He forces the locals to submit, especially their Headman Gruber (Nigel Davenport). The local Catholic priest (Per Oscarsson) is livid that the mercenaries include a number of Protestants (and nihilistic atheists for that matter), but there is little he can do to sway The Captain. The mercenaries are of one mind after The Captain kills a dissenting member of his band, and religious and ethnic divisions are set aside.

At first, the locals accept their fate. Vogel is appointed judge by Gruber, to settle disputes between villagers and soldiers. As long as food, shelter, and a small number of women are provided, the mercenaries leave the locals alone. Hansen (Michael Gothard) attempts to rape a girl and, exiled from the group, manages to lead a rival mercenary band to the valley, before the winter sets in and closes the valley to all outsiders. He and his band are destroyed and the valley goes into hibernation. But as winter fades, it becomes obvious that the soldiers will have to leave. The Captain learns of a major military campaign in the Upper Rhineland and decides to leave the valley in order to participate. Vogel wants to accompany him, fearing Gruber will have him killed once The Captain leaves. However, The Captain orders Vogel to stay as the condition of not sacking the village, leaving a few men as guards.
 major siege operation. Most of his men are killed. The Captain survives long enough to return to the valley, only to find himself faced by the villagers. Vogel intervenes so that no fight happens. The Captain reports the event and dies of his battle wounds, declaring to Vogel, "You were right. I was wrong." A young woman from the village wants to leave with Vogel, but he tells her to stay, and runs off alone in the mist, happy of having saved the valley with the intention of moving on.

==Production== Martin Miller collapsed and died on the set before shooting of the first scene commenced. 

==Reception== Queen Christina). In this light, George MacDonald Fraser wrote in 1988, "The plot left me bewildered - in fact the whole bloody business is probably an excellent microcosm of the Thirty Years War, with no clear picture of what is happening and half the cast ending up dead to no purpose. To that extent, it must be rated a successful film. ... As a drama, The Last Valley is not remarkable; as a reminder of what happened in Central Europe, 1618-48, and shaped the future of Germany, it reads an interesting lesson." Fraser says of the stars, "Michael Caine ... gives one of his best performances as the hard-bitten mercenary captain, nicely complemented by Omar Sharif as the personification of reason." 

==DVD==
The Last Valley was released on DVD by MGM Home Video May 25, 2004. 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 