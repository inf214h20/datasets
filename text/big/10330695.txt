Arcade (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Arcade
| image          = Arcade poster.jpg
| caption        = 
| director       = Albert Pyun
| producer       = Cathy Gesualdo
| writer         = Charles Band (story)   David S. Goyer
| starring       = Megan Ward Peter Billingsley John de Lancie Sharon Farrell Seth Green A.J. Langer   Bryan Dattilo  Alan Howarth  Tony Riparetti
| cinematography = George Mooradian
| editing        = Miles Wynton
| distributor    = Full Moon Entertainment Paramount Pictures
| released       =   July 20, 1993     March 30, 1994 
| runtime        = 85 min.
| country        = United States
| awards         =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 science fiction/horror Full Moon Entertainment and released in 1993. The film features heavy use of computer-generated imagery|CGI, which was fully redone after the film was completed because the producer Charles Band and director Albert Pyun were not satisfied with the original end result. The VideoZone video magazine (a staple of Full Moon films during the 1990s) as well as some trailers showed footage from the original version of the film. As a rarity, the VideoZone featured on the Full Moon Classics DVD release of the film contains no footage of the released films CGI, but only of the original films version.

Despite the change in CGI, the original CGI version did manage to find a release in some places outside of North America, such as Argentina, Germany, Poland and Italy. 

==Plot==
Alex Manning (Megan Ward) is a troubled suburban teenager. Her mother committed suicide and the school counselor feels that she has not dealt with her feelings properly.  Manning and her friends decide to visit the local video arcade known as "Dantes Inferno" where a new virtual reality arcade game called "Arcade" is being test marketed by a computer company CEO who is more than willing to hand out free samples of the home console version and hype up the game as if his job is depending on it, and it is.

However, it soon becomes clear that the teenagers who play the game and lose are being imprisoned inside the virtual reality world by the central villain: "Arcade". It would seem that "Arcade" was once a little boy who was beaten to death by his mother, and the computer company felt it would be a good idea to use some of the boys brain cells in order to make the games villain more realistic. Instead, it made the game deadly.

Nick and Alex enlist the help of the games programmer and head to the video arcade for a final showdown with "Arcade" and his deadly virtual world. While Alex is able to release her friends from a virtual prison, she also ended up freeing the evil little boy, who taunts Alex in the final moments of the film.

==Cast==
{| class="wikitable sortable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Megan Ward || Alex Manning 
|-
| Peter Billingsley || Nick 
|-
| John de Lancie || Difford 
|-
| Sharon Farrell || Alexs Mom 
|-
| Seth Green || Stilts
|-
| A.J. Langer || Laurie
|-
| Bryan Dattilo || Greg 
|-
| Brandon Rane || Benz
|-
| Sean Bagley || Lab Assistant
|-
| B.J. Barie || DeLoache 
|-
| Humberto Ortiz || Boy
|-
| Norbert Weisser || Albert
|-
| Don Stark || Finster
|-
| Dorothy Dells || Mrs. Weaver
|-
| Todd Starks || Burt Manning
|-
| Alexandria Byrne || Kid at Arcade Parlour
|}

==See also==
* Brainscan
* The Dungeonmaster
* Tron
* Johnny Mnemonic The Lawnmower Man

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 