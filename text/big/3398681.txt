La Grande Vadrouille
{{Infobox film
| name           = La Grande Vadrouille
| image          = La Grande Vadrouille poster.jpg
| border         = yes
| caption        = French theatrical release poster
| director       = Gérard Oury
| producer       = Robert Dorfmann
| writer         = Marcel Jullian
| starring       = Bourvil Louis de Funès  Claudio Brook Terry-Thomas
| music          = Georges Auric Hector Berlioz
| cinematography = André Domage Alain Douarinou Claude Renoir
| editing        = Albert Jurgenson
| distributor    =
| released       = 1 December 1966
| runtime        = 132 minutes
| country        = France United Kingdom French English German
| budget         =
}}
La Grande Vadrouille ( ; literally "The Great Stroll"; originally released in the United States as Dont Look Now... Were Being Shot At!) is a 1966 French comedy film about two ordinary Frenchmen helping the crew of a Royal Air Force bomber shot down over Paris make their way through German-occupied France to escape arrest.

For over forty years, until the release of Bienvenue chez les Chtis in 2008, La Grande Vadrouille was the most successful French film in France, topping the box office with over 17,200,000 cinema admissions. It remains the third most successful film ever in France, of any nationality, behind the 1997 version of Titanic (1997 film)|Titanic and Bienvenue chez les Chtis, both of which were seen by over 20,000,000 cinemagoers.  

==Plot== free zone Resistance fighters and sympathisers.

==Cast==
* André Bourvil as Augustin Bouvet
* Louis de Funès as Stanislas Lefort
* Terry-Thomas as Sir Reginald ("Big Moustache")
* Claudio Brook as Peter Cunningham Mike Marshall as Alan MacIntosh
* Marie Dubois as Juliette Pierre Bertin as Juliettes grandfather
* Andrea Parisy as Sister Marie-Odile
* Mary Marquet as The Mother Superior
* Benno Sterzenbach as Major Achbach
* Paul Préboist as The fisherman

==See also==
* Le Corniaud

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 