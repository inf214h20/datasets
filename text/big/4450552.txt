The House That Dripped Blood
 
{{Infobox film
| name           = The House That Dripped Blood
| image          = The House That Dripped Blood.jpg
| caption        = Original theatrical poster
| director       = Peter Duffell
| producer       = Milton Subotsky Max Rosenberg
| writer         = Robert Bloch Russ Jones
| narrator       = 
| starring       = Christopher Lee Peter Cushing Nyree Dawn Porter Denholm Elliott Jon Pertwee
| music          = Michael Dress
| cinematography = Ray Parslow
| editing        = Peter Tanner
| studio         = Amicus Productions
| distributor    = Cinerama Releasing Corporation
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = $500,000
| gross          = 
}}
The House That Dripped Blood is a 1971 British horror anthology film directed by Peter Duffell and distributed by Amicus Productions. It stars Christopher Lee, Peter Cushing, Nyree Dawn Porter, Denholm Elliott, and Jon Pertwee. The film is a collection of four short stories, all originally written and subsequently scripted by Robert Bloch, linked by the protagonist of each storys association with the eponymous building. The film carries the tagline "TERROR waits for you in every room in The House That Dripped Blood."

==Plot==
 
Shortly after renting an old country house, film star Paul Henderson mysteriously disappears and Inspector Holloway from Scotland Yard is called to investigate. Inquiring at the local police station, Holloway is told some of the houses history. He then contacts the estate agent renting the house, who elaborates further by telling Holloway about its previous tenants.

==Segments==

==="Method For Murder"===
A hack writer of horror stories (Denholm Elliot) moves into the house with his wife (Joanna Dunham) and is haunted by visions of Dominic (Tom Adams), the murderous, psychopathic central character of his latest novel.

==="Waxworks"===
Two friends (Peter Cushing and Joss Ackland) become fixated with a macabre waxwork museum that appears to contain a model of a lady they both knew.

==="Sweets to the Sweet"===
A private teacher (Nyree Dawn Porter) is perturbed by the cold and severe way a widower (Christopher Lee) treats his young daughter (Chloe Franks), even forbidding her to have a doll.

==="The Cloak"===
Temperamental horror film actor Paul Henderson (Jon Pertwee) moves into the house while starring in a vampire film being shot nearby. He buys a black cloak from a peculiar shopkeeper (Geoffrey Bayldon) to use as his film characters costume. The cloak seems to instill in its wearer strange powers, something Pauls co-star (Ingrid Pitt) quickly discovers.

==Cast (by segment)==
"Framework" John Bennett as Detective Inspector Holloway
*John Bryans as A.J. Stoker John Malcolm as Sergeant Martin

"Method For Murder"
*Denholm Elliott as Charles Hillyer
*Joanna Dunham as Alice Hillyer Tom Adams as Richard/Dominic Robert Lang as Dr. Andrews
*Judy Pace as Armenia McCormick

"Waxworks"
*Peter Cushing as Philip Grayson
*Joss Ackland as Neville Rogers
*Wolfe Morris as Waxworks Proprietor

"Sweets to the Sweet"
*Christopher Lee as John Reid
*Nyree Dawn Porter as Ann Norton
*Chloe Franks as Jane Reid
*Hugh Manning as Mark

"The Cloak"
*Jon Pertwee as Paul Henderson
*Ingrid Pitt as Carla Lynde
*Geoffrey Bayldon as Theo von Hartmann

==Production==
Vincent Price was first offered the part of Paul Henderson. He liked the script but was unable to accept because American International Pictures held an exclusive contract with him for horror films.

Originally, director Peter Duffell wanted to have the title Death and the Maiden. Producer Milton Subotsky decided on the more dramatic The House That Dripped Blood. Not one drop of blood appears in the actual film.

When Peter Duffell was engaged the participation of actors Lee, Cushing and Pitt had already been decided by the producers. All other actors were cast by Duffell.

==Critical reception==

AllRovi|Allmovies review of the film was positive, calling it "a solid example of the Amicus horror anthology."  Halliwells Film Guide described the film as "neatly made and generally pleasing despite a low level of originality in the writing." 

===Box Office===
The film was a minor success in the UK but did very well in the US. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 62-70 

==Home media==
{| class="wikitable"
|- style="text-align: center;" 
! Format
! Audio
! Subtitles Region
! Aspect Ratio
! Studio
! Release Date
|- style="text-align: center;" 
| DVD-Video, NTSC
| English: Stereo
| English, Spanish
| Region 1
|   Lions Gate Home Entertainment
| October 28, 2003
|- style="text-align: center;" 
| DVD-Video, PAL
| English: Dolby Stereo, 5.1 Dolby Surround, DTS 5.1 Surround
| none
| Region 2
| 1.85:1
| Anchor Bay Entertainment
| October 27, 2003
|}

==References==

 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 