Devanthakudu
{{Infobox film
| name           = Devanthakudu
| image          =
| caption        =
| director       = S. A. Chandrasekhar
| producer       = Narayana Rao
| writer         =
| starring       = Chiranjeevi Vijayashanti Narayan Rao
| music          = J.V. Raghavulu
| cinematography =
| editing        = Gowtham Raju
| distributor    =
| released       = April 12, 1984
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}} Telugu film directed by S. A. Chandrasekhar. The film stars Chiranjeevi, Vijayashanti, and Narayana Rao in important roles.

==Plot==
Vijay Kumar (Chiranjeevi) is a college student who is daring and dashing and has a weakness for betting and challenges. Chanti (Narayana Rao) is Vijays’s friend, who study in the same college. He challenges Vijay to kill a person and escape without being caught and without proof and this person is a professor. Vijay takes it lightly and tries to play away by acting as if he killed the professor but he is really killed by the time he reaches there and he is accused of the murder. The rest of the plot forms on how he frees himself from the blame and who killed the professor and why?

==Cast==
*Chiranjeevi
*Vijayashanti
*Narayan Rao
*Maruthirao Gollapudi Annapurna 

==References==
 


==External links==
* 

 

 
 
 


 