Kochaniyathi
{{Infobox film 
| name           = Kochaniyathi
| image          =
| caption        =
| director       = P. Subramaniam
| producer       = P Subramaniam
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Madhu Jayabharathi Vincent Ramachandran
| music          = Pukazhenthi
| cinematography = ENC Nair
| editing        = N Gopalakrishnan
| studio         = Neela
| distributor    = Neela
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, Vincent and Ramachandran in lead roles. The film had musical score by Pukazhenthi.   

==Cast==
  Madhu as Raju
*Jayabharathi as Indu Vincent as Doctor Mohan
*Ramachandran
*T. R. Omana as Rajus Mother
*Alummoodan as Cook
*Annamma
*Aranmula Ponnamma as Doctors Mother Baby Sumathi as Indus Childhood
*KPAC Sunny as Raghu
*Master Prabhakar as Rajus Childhood
*Pankajavalli as Kunchu Nairs Wife
*Paravoor Bharathan as Rowdy Soman
*S. P. Pillai as Kunchu Nair
*K. V. Shanthi as Karthyayini
*Sarasamma
*Somasekharan Nair
 

==Soundtrack==
The music was composed by Pukazhenthi and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Agniparvatham || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Kochilam Kaatte || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Sundararaavil || S Janaki || Sreekumaran Thampi || 
|-
| 4 || Theyyaare Thaka Theyyaare || S Janaki, P Jayachandran, Chorus || Sreekumaran Thampi || 
|-
| 5 || Thinkaleppole Chirikkunna || P. Leela || Sreekumaran Thampi || 
|-
| 6 || Thinkaleppole Chirikkunna || S Janaki || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 