Smokey and the Bandit Part 3
{{Infobox film|
  | name = Smokey and the Bandit Part 3
  | image = Sandbpart3.jpg
  | caption = Theatrical release poster
  | director = Dick Lowry
  | writer = Stuart Birnbaum David Dashey
  | starring = {{Plainlist|
* Jackie Gleason Paul Williams Pat McCormick
* Jerry Reed
}}
  | music = Larry Cansler
  | cinematography = James Pergola
  | editing =
  | distributor = Universal Pictures
  | released =  
  | runtime = 85 min.
  | language = English
  | budget = $9 million 
  | gross = $7,000,000 
}}
 Paul Williams, Pat McCormick, Mike Henry and Colleen Camp. The film also includes a very brief cameo near the films end by the original Bandit, Burt Reynolds.

With a budget of a television movie, many action and comedic scenes are rehashes of scenes from the previous two Smokey and the Bandit films.

==Plot==
As is the case with the two preceding Smokey and the Bandit films, Smokey and the Bandit Part 3 begins with Big Enos (Pat McCormick) and Little Enos (Paul Williams) offering a sizeable wager on ones ability to transport a shipment a large distance in a short period of time. Offering a slight twist, however, the offer is this time made to a retiring Sheriff Buford T. Justice (Jackie Gleason), betting United States dollar|$250,000 against his badge on his ability to transport a large stuffed fish from an eatery in Florida to Texas.

Unlike the two earlier films, Big and Little Enos this time seem to be quite active in their desire to see Buford fail in his goal. After Buford dodges their many traps (especially after he destroys their milk truck, which drenches them and disables their engine), they then go so far as to actually attempt to hire the Bandit (as a distraction) to stop him. Deciding that the Bandit is too egotistical and hard to manage, they hire Cledus "Snowman" Snow (Jerry Reed) as his replacement. Enthusiastic at the opportunity to portray the Bandit, Cledus parks his Peterbilt 359 and climbs behind the wheel of a black and gold 1983 Pontiac Trans Am.

He later picks up Dusty Trails (Colleen Camp), who quits her job as a "bookkeeper" for a used car dealership, but not before attempting to wreck her boss business (a seedy used car dealership) by badmouthing him in the middle of broadcasting a live TV commercial.

The scene of Cledus picking up Dusty in the middle of the road is almost an exact repeat of how Bo picked up Frog in the 1977 film. Cledus manages to catch up with Buford on an interstate, where he then lassoes Bufords fish off of the Justices police cruiser; Buford needs the fish to retrieve his $250,000. Buford then begins a hot pursuit of Cledus, with another local officer who attempts to take charge of the situation. Not long after the local officer is disabled, Buford becomes disabled as well when sand is dumped on his squad car.

The pursuit quickly resumes as Buford catches up to the duo after Cledus and Dusty stop at a redneck bar to pick up some food. The chase resumes as they enter a local town, where mass chaos comes with their entry. Cledus escapes when an 18 wheeler blocks the alleyway Cledus ran through. While trying to get the truck out, Bufords car is hitched to a tow-truck. After unsuccessfully pleading with the traffic officer to release his car, he sends Junior out to unhook it. Unable to wait, he angrily reverses the car and escapes. The tow truck operator chases him in pursuit, with Junior dangling on the hook, spinning freely. Eventually, Buford manages to make the truck flip over, sending the truck and Junior flying. A number of cars continue to crash into the pile-up. The next scene comes sudden as the Bandit and Justice are in the Mississippi fairgrounds. Buford continues to pursue on two wheels after driving on an incline with Cledus letting the fame get to his head.

Cledus and Dusty decide to stop at a hotel for the night, where there are people who are involved in "sexual" acts, several of which are quite deviant. Buford finds the Bandits Trans Am and decides to make a search of the building to find the fish, which he eventually does. While searching in the steamroom, Buford handcuffs himself to a muscular nymphomaniac woman (Faith Minton) who develops an immediate attraction for Sheriff Justice, and will not take no for an answer.

The next final scenes show Buford getting his tires blown by the "Enos Devil Darts." Cledus quickly arrives and retakes the fish. Cledus and Justice then start a final pursuit with Buford on two tires, first through a bunch of cattle, then to boats, then finally through a field where the Enoses set off a series of explosives, one of which destroys all of the bodywork, leaving the engine, seats, and police light bar (being held by Junior above his head). Cledus decides to surrender the fish to let Buford win. Just after cashing in on the $250,000, Buford finds Cledus and begins to apprehend him, but Buford then imagines Cledus to be the "Real" Bandit (in a cameo appearance by Burt Reynolds) who sweet talks him to letting him go and starting a new pursuit. Similar to the ending of the 1977 movie, Buford is again chasing the Bandit in the hulk of his police cruiser (the muscular woman has taken Juniors place riding shotgun this time), while Junior chases after "Daddy" for miles on end, dropping the reward money as he goes.

==Original version==
The film was originally entitled Smokey IS the Bandit, and did not include Jerry Reed in the cast.  Contemporary newspapers refer to original plans to feature Gleason as both "smokey" and "bandit",  and Reeds name does not appear in early promotional materials or newspaper accounts during the films production.  According to some accounts, Jackie Gleason was to play two roles: Sheriff Buford T. Justice and a different "Bandit". Reportedly test audiences reacted poorly, finding Gleasons two roles confusing, so the Bandit scenes were re-shot with Jerry Reed playing the role. Other accounts indicate that the title was more literal: that Gleason was to play only Sheriff Justice, but the character would also fill the role of "Bandit", by taking the Enos familys challenge (as Reynolds character had done in the previous films).  In a teaser trailer for the film (billed as Smokey is the Bandit), Gleason appears in character as Justice, explaining to the audience that to defeat the Bandit he would adopt the attributes of his prey, "becoming   own worst enemy".  A publicity still of Gleason apparently shows him in costume as the Bandit.  

Some TV versions of the film include a lengthier sequence between Buford and Burt Reynolds Bandit at the end of the film.

==Reception==
"Smokey and the Bandit Part 3" is generally regarded as the weakest of the three "Bandit" films, in terms of both storyline and revenue. Despite the enormous financial success of the original film (grossing over $300 million on a budget of less than $5 million), coupled with respectable (though significantly lower) numbers generated by the sequel, the third installment was both a critical and box office flop, grossing only $7,000,000 against the films $9,000,000 budget.

==Cast==
{| class="wikitable"
|- "
! Actor || Role
|-
| Jackie Gleason || Montague County Sheriff Buford T. Justice
|-
| Jerry Reed || Cledus "Snowman" Snow/ Bandit
|- Paul Williams || Little Enos Burdette
|- Pat McCormick || Big Enos Burdette
|- Mike Henry || Junior Justice
|-
| Colleen Camp || Dusty Trails
|-
| Faith Minton || Tina
|-
| Burt Reynolds || The Real Bandit (Bo "Bandit" Darville)
|-
| Sharon Anderson || Police Woman
|-
| Silvia Arana || Latin Woman
|-
| Alan Berger || Hippie
|-
| Raymond Bouchard || Fannen County Sheriff Purvis R. Beethoven
|-
| Connie Brighton || Girl #1
|-
| Earl Houston Bullock || Flagman
|-
| Ava Cadell || Blonde
|-
| Cathy Cahill || Mother Trucker
|-
| Dave Cass || Local Tough Guy
|-
| Leon Cheatom || Guide
|-
| Candace Collins || French Maid
|-
| Peter Conrad || Midget
|-
| Janis Cummins || Nudist Female
|-
| Jackie Davis || Black Man #1
|-
| Dee Dee Deering || Mrs. Fernbush
|-
| Al De Luca || Flower Vendor
|-
| Raymond Forchion || Tar Worker
|-
| Veronica Gamba || Girl at Picnic
|-
| Jorge Gil || Gas Station Attendant
|-
| Marilyn Gleason || Lady Getting Ticket
|-
| Charles P. Harris || Hot Dog Vendor
|-
| Timothy Hawkins || Man in Truck
|-
| Craig Horwich || Crash Guy
|-
| Pirty Lee Jackson || Blackman #2
|-
| Austin Kelly || Painter – Road
|-
| William L. Kingsley || Announcer
|-
| Will Knickerbocker || Hotel Clerk
|-
| Kim Kondziola || Baby Enos Burdette
|-
| Dick Lowry || Sand Dumper
|-
| Sandy Mielke || Driving Instructor
|-
| Toni Moon || Girl #2
|-
| Alejandro Moreno || Street Latin
|-
| Gloria Nichols || Latin Woman
|-
| Mel Pape || Police Officer
|-
| Dan Rambo || TV Director
|-
| Richard Walsh || Nudist Male
|-
| Curry Worsham || Skip Town
|-
| John Freda || Guest (Uncredited)
|-
| Niki Fritz || S&M Hooker (Uncredited)
|}

==Soundtrack==
*"Buford T. Justice" (main title song), performed by Ed Bruce
*"The Legend of the Bandit", performed by Lee Greenwood Bill Summers
*"The Bandit Express", performed by Lee Greenwood John Stewart
*"It Aint the Gold", performed by John Stewart

Original soundtrack and tapes are available from MCA Records.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 