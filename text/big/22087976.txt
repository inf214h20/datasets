A Knight for a Day
 
{{Infobox film
| name           = A Knight for a Day
| image          = A_knight_for_a_day_poster.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Jack Hannah
| producer      = Walt Disney
| writer         =
| screenplay     = 
| story          = Bill Peet
| narrator       = 
| starring       = 
| music          = Oliver Wallace 
| cinematography = 
| editing        = 
| studio         = Walt Disney Productions
| distributor    = 
| released       =  
| runtime        = 7 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

A Knight for a Day is a 1946 Disney short film starring Goofy. Directed by Jack Hannah, this 7-minute animated comedy short was written Bill Peet. While classified as a Goofy cartoon, Goofy himself is not used in this film, but his lookalikes are used as a basis for all the characters.

==Synopsis== Sir Loinsteak Sir Cumfrence, the black knight, with a sportscaster-like announcer calling the action of the battle. The prize for this contest is the right to marry Princess Esmeralda. 

Due to a prebout accident, Sir Loinsteak is knocked out, leaving his sappy yet clever squire, Cedric, to take his place in the tourney. While Sir Cumference dominates the inexperienced simpleton early on, Cedrics clever and unorthodox improvisations tip the scales in the youths favor. Finally after an assault with his lance, sword and mace, Sir Cumference collapses from exhaustion and Cedric wins by default. Princess Esmeralda leaps to her new fiancé with glee, Cedric soaks in the crowd adoration, while a serf nonchalantly pushes Sir Cumference from the field in a scoop shovel.

==Releases==
This film is included on the VHS releases of   and The Sword in the Stone.	

==External links==
* 

 

 
 
 
 
 
 
 