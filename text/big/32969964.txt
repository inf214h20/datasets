Gloria's Romance
 
{{Infobox film
| name           = Glorias Romance
| image          = Glorias Romance.jpg
| imagesize      =
| caption        = Colin Campbell
| producer       = George Kleine
| writer         = Rupert Hughes David Powell
| music          = Jerome Kern
| cinematography = Sidney Hickox
| editing        =
| distributor    = George Kleine
| released       =  
| runtime        = 40 reels; 20 chapters (12,000 meters, 39,370 feet)
| country        = United States
| language       = Silent (English intertitles)
}} silent film serial starring Colin Campbell lost film.  

==Cast==
  of Billie Burke boxing with woman in a gymnasium.]]
*Billie Burke as Gloria Stafford (*as Miss Billie Burke)
*Henry Kolker as Dr. Stephen Royce David Powell as Richard Freneau, a Broker
*William Roselle as David Stafford, Glorias Brother
*Frank Belcher as Frank Lulry, Freneaus Partner
*William T. Carleton as Pierpont Stafford
*Jule Power as Lois Freeman, Judge Freemans Daughter
*Henry Weaver as Judge Freeman
*Frank McGlynn, Sr. as Gideon Trask
*Helen Hart as Nell Trask
*Maxfield Moree as Stass Casimir
*Maurice Steuart as
*Rapley Holmes as Chooey McFadden
*Adelaide Hastings as Glorias Governess
*Ralph Bunker
*Richard Barthelmess - extra (*uncredited)

==Plot==
An adventurous young girl in Florida (Burke) gets lost in the Everglades. There she finds terror and excitement, as well as the rivalry of two men in love with her.

==List of chapters==
#Lost in the Everglades 
#Caught by the Seminoles 
#A Perilous Love 
#The Social Vortex 
#The Gathering Storm 
#Hidden Fires 
#The Harvest of Sin 
#The Mesh of Mystery 
#The Shadow of Scandal 
#Tangled Threads 
#The Fugitive Witness 
#Her Fighting Spirit 
#The Midnight Riot 
#The Floating Trap 
#The Murderer at Bay 
#A Modern Pirate 
#The Tell-Tale Envelope 
#The Bitter Truth 
#Her Vow Fulfilled 
#Loves Reward

==See also==
*List of lost films

==References==
 

==External links==
 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 