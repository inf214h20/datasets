Smilin' Through (1922 film)
{{Infobox film 
 | name           = Smilin Through
 | image          = Smilin Through 1922.jpg caption         = 1922 music sheet with movie cover Sidney Franklin 
 | producer       = Norma Talmadge Joseph M. Schenck  Sidney Franklin (scenario) Harrison Ford Wyndham Standing
 | music          = silent
 | cinematography = J. Roy Hunt Charles Rosher 
 | editing        = 
 | distributor    = First National Pictures 
 | released       =  
 | runtime        = 96 minutes
 | country        = United States
 | language       = English
 | budget         = 
| gross = $1 million (US/Canada)    accessed 19 April 2014 
}} same name, Harrison Ford, Sidney Franklin, who also directed the more famous 1932 remake at MGM. The film was produced by Talmadge and her husband Joseph M. Schenck for her company, the Norma Talmadge Film Corporation. It was released by First National Pictures. Popular character actor Gene Lockhart made his screen debut in this film.   

The story is essentially the same as the popular Jane Cowl play, with Talmadge in the dual role of Kathleen and Moonyean. Kathleen, a young Irish woman, is in love with Kenneth Wayne but is prevented from marrying him by her guardian John Carteret. John is haunted by memories of his thwarted love for Kathleens aunt, Moonyean.

The story was an especially popular one and was filmed twice more by MGM: in 1932 with Norma Shearer and 1941 with Jeanette MacDonald.

==Cast==
*Norma Talmadge - Kathleen/ Moonyeen Harrison Ford - Kenneth Wayne/Jeremiah Wayne
*Wyndham Standing - John Carteret
*Alec B. Francis - Dr. Owen Glenn Hunter - Willie Ainsley
*Grace Griswold - Ellen
*Miriam Battista - Lil Mary, Moonyeens sister
*Gene Lockhart - Village Rector (billed Eugene Lockhart)

==References==
 

==See also==
*Smilin Through (1932 film)
*Smilin Through (1941 film)
*Smilin Through (play) 1919
*Smilin Through (song)

==External links==
 
* .
* 
* 
* 

 
 

 
 
 
 
 
 
 