Lo chiameremo Andrea
 
{{Infobox film
| name           = Lo chiameremo Andrea
| image          = Lo chiameremo Andrea.jpg
| caption        = Film poster
| director       = Vittorio De Sica
| producer       = Marina Cicogna Arthur Cohn
| writer         = Leonardo Benvenuti Piero De Bernardi Cesare Zavattini
| starring       = Nino Manfredi
| music          = 
| cinematography = Ennio Guarnieri
| editing        = Adriana Novelli
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Lo chiameremo Andrea (also known as Well Call Him Andrew) is a 1972 Italian comedy film directed by Vittorio De Sica.   

==Cast==
* Nino Manfredi as Paolo Antonazzi
* Mariangela Melato as Maria Antonazzi
* Anna Maria Aragona as Teacher
* Giulio Baraghini as Mariani
* Maria-Pia Casilio as Bruna Parini
* Guido Cerniglia as Arturo Soriani
* Violetta Chiarini as Teacher
* Solveyg DAssunta as Ninos mother
* Antonino Faà Di Bruno as Schoolmaster (as Antonino Di Bruno)
* Donato Di Sepio as Carlo Alberto Spadacci
* Luigi Antonio Guerra as The janitor
* Alessandro Iacarella as Schoolboy
* Isa Miranda as Teacher
* Enzo Monteduro as Teacher

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 