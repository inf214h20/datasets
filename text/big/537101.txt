Roustabout (film)
{{Infobox film
| name           = Roustabout
| image          = RoustaboutElvis.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster John Rich
| producer       = Hal B. Wallis
| screenplay     = {{Plainlist|
* Anthony Lawrence
* Allan Weiss
}}
| story          = Allan Weiss
| starring       = {{Plainlist|
* Elvis Presley
* Barbara Stanwyck
* Joan Freeman Leif Erickson
}}
| music          = Joseph J. Lilley
| cinematography = Lucien Ballard
| editing        = {{Plainlist|
* Hal Pereira
* Walter H. Tyler
}}
| studio         = Hal Wallis Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $3,300,000   
}} musical feature John Rich Anthony Lawrence album was one of Elvis Presleys most successful, reaching no. 1 on the Billboard Album Chart. 

==Plot==
  Leif Erickson) and their employer, Maggie Morgan (Barbara Stanwyck). When Charlie tries to become friendly with Cathy, Joe forces him off the road and the bike is wrecked after crashing into a wooden fence.

Maggie offers him a place to stay and a job with her struggling traveling carnival while the bike is being repaired. Charlie becomes a "carnie", a roustabout. Maggie recognizes his musical talents and promotes him to feature attraction. His act soon draws large crowds. Off stage, Charlie romances Cathy, which creates animosity with Joe. After the two men repeatedly clash and Charlie is accused of holding back a customers lost wallet that Joe was accused of stealing, Charlie leaves to star in the much better financed show of rival carnival producer Harry Carver (Pat Buttram).

Once again, he is a great success. However, when Charlie learns that Maggie is facing bankruptcy, he returns to her carnival. In the musical finale, he is happily reunited with Cathy.

==Cast==
*Elvis Presley as Charlie Rogers
*Barbara Stanwyck as Maggie Morgan
*Joan Freeman as Cathy Lean Leif Erickson as Joe Lean
*Jack Albertson as Lou, a teahouse manager
*Sue Ane Langdon as Madame Mijanou, a fortune teller
*Pat Buttram as Harry Carver
*Joan Staley as Marge
*Dabbs Greer as Arthur Nielsen Steve Brodie as Fred the Pitcher
*Norman Grabowski as Sam
*Lynn Borden as a college student
*Jane Dulo as Hazel
*Joel Fluellen as Cody Marsh, another roustabout
*Wilda Taylor as Little Egypt, the principal dancer in the number "Little Egypt"

Uncredited actors listed alphabetically:
*Beverly Adams as Cora, a dancer
*Billy Barty as Billy, carnival midget
*Teri Garr as College Girl. Garr can also be seen as a backup dancer during several musical numbers.
*Joy Harmon as College Girl The Spy Who Loved Me (1977) and Moonraker (film)|Moonraker (1979)
*Kent McCord as Carnival Worker
*Raquel Welch as College Girl
*Red West as Carnival Worker

==Musical numbers==
:See also Roustabout (album)

*"Roustabout" by Bill Giant, Bernie Baum and Florence Kaye
*"Poison Ivy League" by Bill Giant, Bernie Baum and Florence Kaye
*"One Track Heart" by Bill Giant, Bernie Baum and Florence Kaye
*"Wheels On My Heels" by Sid Tepper and Roy C. Bennett
*"Its a Wonderful World" by Sid Tepper and Roy C. Bennett
*"Its Carnival Time" by Ben Weisman and Sid Wayne Fred Wise and Randy Starr
*"Hard Knocks" by Joy Byers
*"Theres a Brand New Day On the Horizon" by Joy Byers Lee Morris and Sonny Hendrix
*"Little Egypt" by Jerry Leiber and Mike Stoller
*"Im A Roustabout" by Otis Blackwell and Winfield Scott, a different and unreleased theme song for the movie

All tunes in the film were sung by Presley.

==Reception==

Roustabout reached #8 nationally at the box office in 1964 based on the Variety survey. The film finished as #28 on the year-end list of the top-grossing movies of 1964 and earned $3 million at the box office.

While the New York Times declined to review the film, Variety (magazine)|Variety was lukewarn, faulting mainly the script, but noted the film would likely be a box-office hit based upon its star names, songs, and Technicolor, Techniscope qualities.

==Awards and honors== Writers Guild album that went #1 on the Billboard (magazine)|Billboard charts. The soundtrack album would be Presleys final #1 soundtrack and last #1 album until 1969s From Elvis in Memphis, which topped the charts in the U.K.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 