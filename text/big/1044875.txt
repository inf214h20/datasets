Back to School
 
 
{{Infobox film
| name           = Back to School
| image          = Back to School Movie Poster.jpg
| caption        = Film poster
| director       = Alan Metter
| producer       = Chuck Russell
| screenplay     = Steven Kampmann Will Porter Peter Torokvei Harold Ramis
| story          = Rodney Dangerfield Greg Fields Greg Snee
| starring       = {{plainlist|
* Rodney Dangerfield
* Sally Kellerman
* Burt Young
* Keith Gordon
* Adrienne Barbeau
* Robert Downey, Jr.
* Sam Kinison
* Ned Beatty
}}
| music          = Danny Elfman
| cinematography = Thomas E. Ackerman
| editing        = David Rawlins
| studio         = Paper Clip Productions
| distributor    = Orion Pictures
| released       =  
| runtime        = 96 min.
| country        = United States
| language       = English
| budget         = $11 million 
| gross          = $91,258,000 (theatrical)  $41,948,000 (rentals)
}} Terry Farrell, William Zabka, Ned Beatty, Sam Kinison, and Robert Downey, Jr. It was directed by Alan Metter.

The plot centers on a wealthy but uneducated father (Dangerfield) who goes to college to show solidarity with his discouraged son (Gordon) and learns that he cannot buy an education or happiness.  

Author Kurt Vonnegut has a cameo as himself, as does the band Oingo Boingo, whose frontman Danny Elfman composed the score for the film.
 City of Industry, California.

After the ending scene, before the credits roll, there is a message: "For ESTELLE Thanks For So Much". This is a reference to Estelle Endler, one of the executive producers of the film. She was also Dangerfields manager and helped him get into films like Caddyshack. She died during the filming of Back to School, so he dedicated the film to her.

==Plot==
Thornton Melons is a rags-to-riches story.  The son of an Italian immigrant tailor, he is shown as a boy (Jason Hervey) in his fathers shop, bearing a report card with poor grades. His ambition is to go into his fathers line of work, but his father reprimands Thornton for his poor schoolwork, and tells him no matter how hardworking, skilled or wealthy one may be, "if a man has got no education, he has got nothing". 
 Tall and Fat" clothing store and eventually becoming a corporate giant, complete with a TV commercial in which he asks:

 Are you a large person?  Pleasantly plump?  A little on the hefty side, perhaps?  Well, lets face it: are you fat? When you go jogging, do you leave potholes?  When you make love, do you have to give directions?  At the zoo, do elephants throw you peanuts?  Do you look at a menu and say, Okay? 
 changed his last name to "Melon" (from the original "Meloni"). 
 social climbing Polaroids of her with other men, including a Dwarfism|midget) after she threatens to sue him for half of his net worth.

Thornton tells his friend, chauffeur and bodyguard Lou (Burt Young) to drive him to Jasons college. It turns out Jason has been keeping secrets from his father. He is not on the Grand Lakes diving team, but instead works as a towel boy, treated badly by star diver Chas Osborn (William Zabka). Jason has only his best friend Derek Lutz (Robert Downey, Jr.) for support and intends to drop out. Thornton pleads with him, offering to go to college with his son if hell stay, and also tells Jason that he has no reason to be ashamed of his failures, considering Thorntons early mediocre life.

Possessing neither a high school diploma nor any transcripts or SAT scores, Thornton’s efforts seem to be stalled. But when the universitys "Dean" Martin — a play on the crooner Dean Martin — played by Ned Beatty, asks how he can possibly admit an unqualified student, the next scene cuts to a groundbreaking of the universitys new Thornton Melon School of Business.

Thorntons bribery earns him the wrath of Dr. Philip Barbay (Paxton Whitehead), dean of the business school. The wrath is further exacerbated when Dr. Barbays ivory tower ways are at odds with Thorntons knowledge of business gained from actual experience. Thornton promptly strikes up a romance with Dr. Diane Turner (Sally Kellerman), an attractive literature professor who is dating Barbay.
 Terry Farrell), a girl that Chas has been trying to impress. Jasons popularity on campus also increases thanks to his father’s generosity and party-throwing. Jason even earns a spot on the diving team as well after Thornton — who claims to have once been a spectacular diver himself — talks the Grand Lakes coach (M. Emmet Walsh) into giving the kid another look.

As a student, even though Diane is inspiring a deeper appreciation of literature, Thornton prefers partying to studying. He hires a team of professionals to complete his assignments, including author Kurt Vonnegut, to write a paper on Kurt Vonnegut for literature class. To Thorntons surprise, a disappointed Diane, recognizing the fraud, gives Thornton an F on his paper, telling him that she will not accept work from him that was written by someone else, and adding that whoever did write the paper doesnt know the first thing about Kurt Vonnegut. Meanwhile, Jason is fed up with his father, his school, his status and his own failures. At his dads big party, he gets drunk, punches Chas and disappears.

Thornton’s fraud is further exposed by Dr. Barbay, who challenges him before Dean Martin to pass an oral examination from all of his professors.  If Thornton fails any part of it, he will be expelled. Not believing that he will be able to pass that oral exam, Thornton packs up and prepares to leave, knowing that he cant be expelled if he drops out. But Jason stops him and reminds him of how he wanted to drop out, too, but Thornton talked him out of it. Jason believes that Thornton can do it and says that theyll help him. Thornton then decides that hes up to the challenge.
 Vietnam vet), Terguson (Sam Kinison). He passes, nevertheless, after Dr. Turner inspires him by having him recite Dylan Thomas poem "Do not go gentle into that good night."

At the championship dive meet, father and son patch things up and, with a little distraction, Derek fouls up the opposing team’s dives, while Chas gives a lackluster effort, followed by a near-perfect performance from Jason. Chas fakes a cramp out of spite in an effort to make the team lose. This gives the coach an inspiration. He calls upon Thornton, who comes out of the grandstand to perform an “impossible” dive, the legendary “Triple Lindy," to win the meet.

Thornton learns that he has passed all his classes with Ds, except from Diane, who has given him an A. The movie closes with Thornton lecturing the graduating class that the real world is hard, so: "Move back in with your parents... let them worry about it!" and "Look out for Number One, but dont step in Number Two!" 

==Cast==
*Rodney Dangerfield as Thornton Melon
*Sally Kellerman as Dr. Diane Turner
*Burt Young as Lou
*Keith Gordon as Jason Melon
*Robert Downey, Jr. as Derek Lutz
*Paxton Whitehead as Dr. Phillip Barbay
*Sam Kinison as Professor Terguson (a.k.a. Turgeson) Terry Farrell as Valerie Desmond
*M. Emmet Walsh as Coach Turnbull
*Adrienne Barbeau as Vanessa Melon
*William Zabka as Chas Osborne
*Ned Beatty as "Dean (education)|Dean" David Martin
*Severn Darden as Dr. Borozini
*Jason Hervey as Young Thornton
*Kurt Vonnegut as Himself (cameo)

==Reception== Fields could have appeared in. Dangerfield brings it something they might also have brought along: a certain pathos."  

==Soundtrack==
{{Infobox album | 
| Name        = Back to School
| Type        = soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = 1986
| Recorded    =  Soul
| Length      = 35:34 MCA
| Producer    = 
| Reviews     = 
| 
| Last album  = 
| This album  = 
| Next album  = 
}}
The soundtrack was released on MCA Records|MCA, available in LP or Cassette (no CD), but the score was later released with selections from the score of Pee-wees Big Adventure on CD.

===Track listing (soundtrack)===
{{Track listing
| extra_column    = Performer(s)
| total_length    = 
| writing_credits = yes
| title1          = Back to School
| writer1         = Richard Wolf & Mark Leonard
| extra1          = Jude Cole
| length1         = 4:16
| title2          = Educated Girl
| note2           = 
| writer2         = 
| extra2          = Bobby Caldwell
| length2         = 4:07
| title3          = Learnin and Livin
| note3           = 
| writer3         = 
| extra3          = Tyson & Schwartz
| length3         = 3:25
| title4          = Everybodys Crazy
| note4           = from Everybodys Crazy, 1985
| writer4         = Bolton
| extra4          = Michael Bolton
| length4         = 4:37
| title5          = Ill Never Forget Your Face
| note5           = 
| writer5         = Wolf
| extra5          = Phillip Ingram
| length5         = 4:07
| title6          = Twist and Shout
| note6           = Isley Brothers cover, original 1962
| writer6         = Phil Medley, Bert Russell
| extra6          = Rodney Dangerfield
| length6         = 2:51 Dead Mans Party Dead Mans Party, 1985
| writer7         = Danny Elfman
| extra7          = Oingo Boingo
| length7         = 6:17
| title8          = On My Way
| note8           = 
| writer8         = 
| extra8          = Tyson & Schwartz
| length8         = 3:30 Respect
| note9           = from I Never Loved a Man the Way I Love You, 1967
| writer9         = Otis Redding
| extra9          = Aretha Franklin
| length9         = 2:24
}}

==See also==
* List of American films of 1986

==References==
 

==External links==
 
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 