Ormayilennum
{{Infobox film
| name           = Ormayilennum
| image          =
| caption        =
| director       = TV Mohan
| producer       =
| writer         = Madhavikkutty
| screenplay     = Devan
| music          = Jerry Amaldev
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Devan in lead roles. The film had musical score by Jerry Amaldev.   

==Cast==
*Sukumari
*Ratheesh
*Baby Chaithanya Devan
*Jagannatha Varma
*KR Savithri Seema

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Puthiyankam Murali. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaasha kanmani than || K. J. Yesudas, KS Chithra, Chorus || Puthiyankam Murali || 
|-
| 2 || Kankuliruvathellam || KS Chithra, Chorus || Puthiyankam Murali || 
|-
| 3 || Unarunaroo || K. J. Yesudas, Chorus || Puthiyankam Murali || 
|-
| 4 || Unnikkaattin || KS Chithra || Puthiyankam Murali || 
|}

==References==
 

==External links==
*  

 
 
 

 