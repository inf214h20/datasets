The Cop (1970 film)
 
{{Infobox film
| name           = The Cop
| image          = UnCondePoster.jpg
| caption        = French film poster for The Cop
| director       = Yves Boisset
| producer       = Véra Belmont Jean-François Bizot
| writer         = Yves Boisset Sandro Continenza Pierre Lesou Claude Veillot
| starring       = Michel Bouquet
| music          = 
| cinematography = Jean-Marc Ripert
| editing        = Albert Jurgenson Vincenzo Tomassi
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France Italy 
| language       = French
| budget         = 
| gross          = $9,965,887  
}}

The Cop ( ,  ) is a 1970 French-Italian crime film directed by Yves Boisset and starring Michel Bouquet.   

==Cast==
* Michel Bouquet as Linspecteur Favenin
* Françoise Fabian as Hélène Dassa
* Gianni Garko as Dan Rover (as John Garko)
* Michel Constantin as Viletti
* Anne Carrère as Christine Rufus as Raymond Aulnay
* Théo Sarapo as Lupo
* Henri Garcin as Georges Duval, dit Georgy Beausourire
* Pierre Massimi as Robert Dassa
* Bernard Fresson as Linspecteur Barnero
* Adolfo Celi as Le commissaire principal / Chief of police
* Jean-Claude Bercq as Germain (as Jean-Claude Berck)
* Serge Nubret as Le Noir

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 