The Raven (2012 film)
{{Infobox film
| name            = The Raven
| image           = The Raven Poster.jpg
| caption         = Teaser poster
| director        = James McTeigue
| producer        = Marc D Evans Trevor Macy Aaron Ryder
| writer          = Ben Livingston Hannah Shakespeare Luke Evans
| music           = Lucas Vidal
| cinematography  = Danny Ruhlmann
| editing         = Niven Howie
| studio          = FilmNation Entertainment Relativity Media Intrepid Pictures Galavis Films Pioneer Pictures
| distributor     = Relativity Media 
| released        =  
| runtime         = 111 minutes 
| country         = United States
| language        = English
| budget          = $26 million 
| gross           = $29.7 million 	
}} mystery thriller thriller film Luke Evans. It was released March 9, 2012 in Canada, Republic of Ireland|Ireland, the United Kingdom and the United States on April 27, 2012.
 1935 and 1963 films.
 musical score by Lucas Vidal, but reviewers criticized the various twists and turns of the plot-lines, as well as the performances.

==Plot==
In 19th-century Baltimore, Maryland, several policemen discover a murdered woman sprawled on the floor of her apartment, which was locked from the inside. While police search for the killers means of escape, they discover a second corpse in the chimney, later identified as the 12-year-old daughter of the first victim. A celebrated detective, Emmett Fields, is called to assist in the investigation and discovers that the crime resembles a fictional murder in the short story "The Murders in the Rue Morgue" that he had once read.

The writer Edgar Allan Poe is brought to Fields for questioning. After finding the body of a rival critic of Poes cut in half with a pendulum (as in Poes story "The Pit and the Pendulum"), the pair deduce that someone is copying Poes stories. Edgars love, Emily Hamilton, is kidnapped at a masquerade ball her father is hosting, like the one described in Poes "The Masque of the Red Death". The killer taunts Poe in a note, demanding that Edgar write and publish a new story. Poes lodgings are burned down by people who believe he is exploiting the murders for his own journalistic ends, and he is forced to move in with Fields.

A clue from the killer referring to "The Cask of Amontillado" leads Poe and Fields to search the tunnels under the city with several policemen, discovering the walled-up corpse of a man dressed as Emily. The man is determined to be a sailor, and the clues on his body bring the pursuers to Holy Cross Church, where an empty grave with Emilys name on it has been prepared. As the police attempt to break down the church doors, the killer attacks and kills one of the policemen, then shoots and wounds Fields. Poe gives chase on horseback, but the killer escapes.

Poe writes one last chapter for his newspaper column, offering his life for Emilys, suggesting that he could take a poison. In the morning, the maid gives Poe a letter from the killer, accepting his terms, but the note had been delivered long before the paper was distributed. Realizing that the killer must work at the paper, Poe races to the newspaper office to confront his editor, Henry, but instead finds Henry already dead, with another note.

The real killer, the papers typesetter, Ivan, appears, and congratulates Poe, offering him a drink. Ivan attempts to converse with Edgar, but Poe only demands to be told of Emilys location. Ivan pours a vial of poison, promising to end the story as Poe had written, to which Poe agrees, drinking the liquid. It is revealed that Ivans surname is Reynolds, and the killer quotes a line from the "The Tell-Tale Heart", allowing Edgar to deduce that Emily is concealed beneath the printing floor. As the killer leaves, Poe uses the last of his strength to tear up a false section of floor and open a trapdoor leading to where his fiancée is being held.

Poe rescues Emily and they share a poignant moment before she is taken away by ambulance. Delirious from the poison, Edgar wanders off to a park bench to die. A man walking in the park recognizes him as "Edgar Poe", the famous writer, and asks if he is all right. Poe can summon only enough strength to say, "Tell Fields his last name is Reynolds". Later, when Fields comes to view Poes corpse at the hospital, the attending physician is unable to tell him the exact cause of death, but mentions that the writer was incoherent, insisting that "his last name is Reynolds". Fields ponders the meaning of the phrase, slowly connecting the dots.

Some time later, Ivan (now under his new name "Reynolds") is shown disembarking from a train in Paris. As a porter carries his luggage, Ivan climbs into a carriage to be confronted by Fields, revolver levelled at him. He lunges for the detective, and is immediately shot.

== Cast ==
* John Cusack as Edgar Allan Poe Luke Evans as Inspector Emmett Fields
* Alice Eve as Emily Hamilton
* Brendan Gleeson as Captain Charles Hamilton
* Oliver Jackson-Cohen as PC John Cantrell
* Jimmy Yuill as Captain Eldridge
* Kevin McNally as Henry Maddux
* Sam Hazeldine as Ivan Reynolds
* Pam Ferris as Mrs. Bradley
* John Warnaby as Ludwig Griswold, a reference to Rufus Wilmot Griswold
* Brendan Coyle as Reagan

== Production ==
 .  Ewan McGregor was also in talks for a role,  but he also dropped out.  On August 28, 2010, it was confirmed that John Cusack would play Edgar Allan Poe in the film. Joaquin Phoenix was also approached to star at one point. 
 Belgrade and Relativity acquired U.S. rights for only $4 million. 

== Reception ==
The Raven received mixed to negative reviews from critics. On Rotten Tomatoes, the film holds a rating of 23%, based on 128 reviews, with the sites consensus reading, "Thinly scripted, unevenly acted, and overall preposterous, The Raven disgraces the legacy of Edgar Allen   Poe with a rote murder mystery thats more silly than scary."  On Metacritic, the film has a score of 44 out of 100, based on 33 critics, indicating "mixed or average reviews".  

James Berardinelli gave the film two and a half stars out of four, writing: "The Raven looks great and is well-paced, but a lack of a compelling resolution makes it an anemic effort." 
Mick LaSalle of the San Francisco Chronicle wrote, "The story has its moments, and yet there is something about this tale ... that doesnt completely satisfy." 
Richard Roeper though gave the movie a very positive review, giving the movie a B+.

The Raven brought back its budget at the box office by grossing $29.65 million worldwide on a budget of $26 million.

== See also ==
 
*Edgar Allan Poe bibliography
*Edgar Allan Poe in popular culture

== References ==
{{Reflist|30em|refs=
   
   
}}

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 