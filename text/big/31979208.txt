Sonny Boy (2011 film)
{{Infobox film
| name           = Sonny Boy
| image          = 
| caption        = 
| director       = Maria Peters
| producer       = Shooting Star Filmcompany
| writer         = Annejet van der Zijl (novel) Maria Peters
| starring       = Ricky Koole Sergio Hasselbaink Marcel Hensema
| music          = Henny Vrienten
| cinematography = Walther van den Ende
| editing        = Ot Louw
| distributor    = A-Film
| released       =  
| runtime        = 132 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Plot==
In  1928, a young man from Suriname named Waldemar Nods (Sergio Hasselbaink) goes to the Netherlands to study. His dark skin stands out and causes discrimination. He moves to lodging in The Hague with Rika, who went to live separated from her deeply religious husband Willem, after she discovered him cheating with the maid Jans. She has taken all four of their children (Willem, John, Bertha and Henk) with her.

Waldemar and the 17 year his senior Rika start a relationship and she becomes pregnant. She says nothing to Waldemar and goes to a woman for an illegal abortion, but changes her mind. When she is four months pregnant she tells Waldemar. He is angry for not having been told, and leaves. Wim and Jan run away, to their father Willem. Waldemar returns, and when. Willem visits Rika to tell her about a job opening in Indonesia, he asks her to come along. He will accept the baby as his own child. He changes his mind when he learns the father is dark skinned. He also gets a court order for Bertha and Henk to live with him, with success. He refuses a divorce, preventing Rika remarrying Waldemar. 

When the child is born, they call the boy Waldy, nicknamed Sonny Boy. Rika’s landlord terminates the rental, because of Rikas extramarital affair, especially with a Surinamese. Rika and Waldemar roam the streets with Waldy, when they meet an older Jewish man, Sam, who rents them a room. 

With financial support from Sam, Rika and Waldemar start a guest house in Scheveningen. When The Netherlands are invaded by the Germans, Rika and Waldemar are forced to give shelter to German soldiers. Later on they have to evacuate the guest house, as the area is cleared for the “Atlantik Wall” defense system.

Because Rika is mother of five children she is allotted a big replacement house. At the request of a young resistance fighter she met in church through the help of a clergyman, Rika starts hiding people in her house. As the fee for Jews is higher than for Dutch, she chooses to hide Jewish people. After some time also an SS deserter joins the hiding. At first Waldy is not informed of the hiders, but after witnessing Sam’s deportation and a street fight Rika is visited by a collaborating official. Waldy hears something upstairs and discovers the hide aways.
After a raid the hiders are discovered and arrested, together with Waldemar, Rika and Waldy. Waldy is released and goes to live with relatives. This is not safe, because the Germans want to put more pressure on Waldemar and Rika during interrogations. 
Waldy is therefore placed with a farmer. When people come to the farm during the hunger winter, Waldy prevents a couple selling their wedding rings for food, by offering his parents’ rings. The farmer refuses Waldy’s rings and explain everybody tries to make some money during wartime, also Rika and Waldemar were paid for the hiding.

Rika dies in concentration camp Ravensbrück. Waldemar is also shipped to a concentration camp, but through his language skills, he is drafted for the mailroom. This gives him opportunity to send letters to Waldy clandestinely. After Hitlers death Waldemar is deported to the ship Cap Arcona, which is attacked by the Allies. He jumps into the sea and swims to shore. He can swim well, which he learned in Surinam, where he swam long distances in the river – he used to say “water is my friend.. On arrival on the beach, he is killed by two German child soldiers.

==Cast==
* Ricky Koole as Rika van der Lans
* Sergio Hasselbaink as Waldemar Nods
* Daniel van Wijk as Waldy (Sonny Boy)
* Micha Hulshof as Marcel
* Wouter Zweers as Gerard
* Marcel Hensema as Willem
* Gaite Jansen as Bertha
* Martijn Lakemeier as Jan
* Frits Lambrechts as Sam
* Raymond Spannet as landlord
* Joy Wielkens as Hilda

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
* 

 
 
 
 
 
 