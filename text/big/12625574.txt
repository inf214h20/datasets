Lady from Louisiana
 
{{Infobox film
| name           = Lady from Louisiana
| image          = Lady from Louisiana FilmPoster.jpeg
| caption        = Film poster
| director       = Bernard Vorhaus
| producer       = Bernard Vorhaus
| writer         = Edward James Vera Caspary Francis Edward Faragoh
| starring       = John Wayne Ona Munson
| music          = Cy Feuer
| cinematography = Jack A. Marta
| editing        = Edward Mann
| distributor    = Republic Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States 
| language       = English
| budget         = 
}}

Lady from Louisiana is a 1941 American disaster film starring John Wayne. It was produced and directed by Bernard Vorhaus for Republic Pictures.

==Plot==
Yankee lawyer John Reynolds (Wayne) and Southern Belle Julie Mirbeau (Ona Munson) meet and fall in love on a riverboat going to New Orleans in the Gay Nineties. Upon arrival they are met by Julies father (Henry Stephenson) who runs the popular Louisiana State Lottery Company and Reynolds Aunt Blanche (Helen Westley) who is a key figure in the anti-Lottery forces hoping Wayne as States Attorney will end the Lottery.  

Correctly gauging the situation as "playing Romeo and Juliet", Wayne is invited to the Mirbeau mansion where Julie and her father explain that not only are the people of New Orleans fun loving and like gambling such as the Lottery, but the Lottery funds many charitable institutions such as hospitals and levees for the river.

Unknown to General Mirbeau is his assistant Blackies (Ray Middleton) protection rackets and murders of lottery winners through his army of thugs led by Cuffy Brown (Jack Pennick). The Lottery forces also have information sources in the States Attorneys office that reveals every move Wayne has planned to raid illegal activities as well as corrupting judges and other officials through their brothels.

The battle between the two forces escalates leading into a climax of lightning striking and destroying a courthouse where a trial is going on and a break in the levees during torrential rains that flood the city.

==Production==
Republic Pictures spared no expense in making the film, with large numbers of costumed extras and recreations of Mardi Gras. The studios high standard of action scenes and special effects miniatures come to the fore in the fight scenes and flood climax.
 Walking Tall type scenario.

A 1941 Time magazine review noted the similarities between Waynes Thomas E. Dewey type character and Huey Long. 

==Cast==
* John Wayne as John Reynolds
* Ona Munson as Julie Mirbeau
* Ray Middleton as Blackburn "Blackie" Williams
* Henry Stephenson as General Anatole Mirbeau
* Helen Westley as Blanche Brunot
* Jack Pennick as Cuffy Brown
* Dorothy Dandridge as Felice
* Shimen Ruskin as Gaston
* Jacqueline Dalya as Pearl
* Paul Scardon as Judge Wilson
* Major James H. McNamara as Senator Cassidy James C. Morton as Littlefield
* Maurice Costello as Edwards
* Mantan Moreland as Servant

==See also==
* John Wayne filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 