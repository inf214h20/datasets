Dream House (film)
{{Infobox film
| name           = Dream House
| image          = Dream House Poster.jpg
| image_size     = 215px
| alt            = Two girls holding hands, their dresses match the wallpaper behind them. 
| caption        = Theatrical release poster
| director       = Jim Sheridan
| producer       = {{Plainlist | 
* Jim Sheridan
* James G. Robinson
* David C. Robinson
* Daniel Bobker
* Ehren Kruger
}}
| writer         = David Loucka
| starring       = {{Plainlist | 
* Daniel Craig
* Rachel Weisz
* Naomi Watts
* Marton Csokas
}}
| music          = John Debney
| cinematography = Caleb Deschanel
| editing        = Barbara Tulliver
| studio         = Morgan Creek Productions
| distributor    = Universal Pictures  (North America) Warner Bros. Pictures  (International) 
| released       =  
| runtime        = 92 minutes 
| country        = United States Canada
| language       = English
| budget         = $50 million 
| gross          = $38,502,340
}}

Dream House is a 2011 American psychological thriller written by David Loucka directed by Jim Sheridan and starring Daniel Craig, Rachel Weisz, Naomi Watts and Marton Csokas.  It was released on September 30, 2011, in the United States and Canada by Universal Pictures and Morgan Creek Productions to mostly negative reviews and low box office results.

==Plot==
The film begins in Manhattan with Will Atenton (Daniel Craig) leaving his job as a successful editor in the city in order to spend more time with his wife, Libby (Rachel Weisz), and their two daughters and write a book. At first, they appear to be living the American dream in an idyllic country home they have just moved into. Early in his time at the house, Will notices tension between his neighbor, Anne Patterson (Naomi Watts), and her ex-husband, Jack, who is picking up their teenage daughter. Despite the seemingly perfect house, it soon becomes apparent to Will that something isnt right.

After asking around, Will learns of terrible murders that occurred there five years earlier - a man, named Peter Ward, the previous owner of the house, shot and killed his wife and two daughters. Whats more, Wills young daughters claim to begin seeing a man watching them through the windows at night. Will and his wife begin to uncover more information about the murders, despite the local police refusing to help them. Even his neighbor, Anne, who he has talked to on a few occasions, remains strangely distant and wont tell him anything. However, after uncovering some old things in a hidden attic space, they find out that Peter Ward had already been released from custody (as there was no concrete evidence that he actually killed his family) and is living in a half-way house -- to much public controversy.

When Wills wife also sees the strange man that her daughters had already claimed was watching the house (and the police fail to help), Will, believing it to be Peter Ward, sets off to the half-way house to find him and try and settle things. Will goes to the half-way house and sneaks into what he believes is Wards room (the number of which he found by looking at the mail pigeon hole marked with his name) and there he finds a picture of his wife and daughters. A man comes in and Will threatens him to stay away from his family. However, the man is not Peter Ward, but in fact a man named Martin Tishencko (Joe Pingue). Confused, Will returns home.

Further research leads Will to the psychiatric hospital where Peter Ward was initially hospitalized. There, Will is told that he is Peter Ward and that "Will Atenton" is a false identity he invented to mask the trauma of losing his wife and daughters. Although he doesnt believe it, the seams begin to show in what he once thought was reality. More and more evidence crops up, showing that the wife and daughters he believed he had are, rather, just projections in his mind of the family that he supposedly murdered.

It is here that Will begins to slip between realities -- One being his idyllic, yet completely invented, life with his wife and daughters, and the other, in which his house is in ruins and he is the accused murderer of his family, still living in that same house all alone. He tries to convince to his wife that he is Peter Ward, but she wont believe it, putting it down to a fever and Peters high temperature. However, when the girls also get this fever and she finds gaping wounds on the girls backs she realizes he was telling the truth. After his house is deemed by the city as being in an unlivable state, he is evicted - but does not want to leave as whenever he is in this house he returns to his fantasy land with his long dead family. As well as this, he is still unsure which reality is correct.

Anne, Wills neighbor, takes him in, giving him a bath and washing his clothes. She believes his innocence and even her daughter, who had previously had nightmares about the murders, is comfortable with his presence. However, Annes ex-husband, Jack, somehow hears that Peter is with his ex-wife in her home and comes over to collect his daughter, despite him not yet having custody for a few days. Anne refuses but her daughter obliges in order to avoid any conflict -- yet she runs away as soon as she is out the door. Jack goes after her, and Anne, before following, gives Peter the business card of the psychiatric hospital in which he was previously treated, which she found while washing his clothes.

Peter/Will goes to see his psychiatrist who urges him to come back to treatment, but she does not know if she believes his innocence or not and Peter storms back to his boarded up house. There, he is once again immersed in his fantasy land and asks Libby to recall the night of the murders and she tells him everything she can remember - that she heard someone coming up the stairs and the girls, thinking it was their dad, went out to greet him. She also came out, only to find a man with a gun. Peter is jerked from his fantasy by Anne, who is coming to tell him to get out of the house, but before they can leave, Jack and Boyce are there ready to kill Anne and blame the murder on Peter.

As it turns out, the original murders of Peters family were actually carried out by Boyce. He had been hired to kill Anne by Jack after their divorce but Boyce went to the wrong house and killed the Ward family instead. After being shot, Peters wife tried to shoot Boyce with a gun he had left on the ground while he was fighting with Peter. Instead, she hits Peter in the head, which explains the absence of any recollection of the events. Both Peter and Anne are knocked unconscious and taken to the basement where Anne is tied up. Jack shoots Boyce for his failure in the previous mission and goes on to light the house on fire with gasoline.

At this point, the truth is revealed about Wards family. Wards wife and daughters are visible to him because they are actually ghosts. This is shown when Libby follows Jack down the stairs where he has Anne and Peter. To prevent Jack from killing Anne and Peter, she starts to make things move in the basement to trick Jack.  Libby buys Peter enough time to free himself and Anne, and they escape from the house while Boyce and Jack are left inside to die. Peter rushes back into the burning house where he sees his wife and daughters for the last time.  During his escape, Libby makes the wind rush through the house and Peter is brought to his senses, finally letting go of his family to escape from the house.

The movie ends showing Peter Ward looking through a bookstore window at the book he has just written about his experience -- he was writing a book at the beginning of the film in his fantasy world, but this time the book is under his real name of Peter Ward.

== Cast ==
* Daniel Craig as Will Atenton/Peter Ward
* Rachel Weisz as Libby Atenton/Elizabeth Ward
* Naomi Watts as Ann Patterson
* Marton Csokas as Jack Patterson
* Claire Geare as Dee Dee Atenton/Katherine Ward
* Taylor Geare as Trish Atenton/Beatrice Ward
* Rachel G. Fox as Chloe Patterson Mark Wilson as Dennis Conklin
* Jonathan Potts as Tony Ferguson
* Lynne Griffin as Sadie
* Elias Koteas as Boyce Gregory Smith as Artie Chris Owens as Tom Barrion
* Jane Alexander as Dr. Greeley
* Sarah Gadon as Cindi
* Marlee Otto as Zara
* Joe Pingue as Martin Tishencko
* David Huband as Officer Nelson

==Production==
Director Jim Sheridan clashed with Morgan Creek’s James G. Robinson constantly on the set over the shape of the script and production of the film.  Sheridan then tried to take his name off the film after being unhappy with the film and his relationship with Morgan Creek Productions. 

Sheridan, Daniel Craig and Rachel Weisz disliked the final cut of the film so much that they refused to do press promotion or interviews for it. http://www.cinemablend.com/new/Daniel-Craig-s-Dream-House-Trailer-Spoils-The-Entire-Movie-25784.html   The trailer, cut by Morgan Creek Productions, came under fire for revealing the main plot twist of the film. 

==Soundtrack==
{{Infobox album|  
| Name        = Dream House: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = John Debney
| Cover       =
| Recorded    = 2011
| Released    = 11 October 2011
| Genre       = Soundtrack
| Length      = 56:47
| Label       = Varèse Sarabande
| Producer    = Stephanie Pereida
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =     Filmtracks
| rev2Score =    
}}
The score to Dream House was composed by John Debney and conducted by Robert Ziegler.  Christian Clemmensen, reviewer of Filmtracks.com, gave it four out of five stars, declaring it "among the biggest surprises of 2011" and stating, "Its not clear how badly Debneys work for Dream House was butchered by the studios frantic last minute attempts to make the film presentable, but Debneys contribution does feature a cohesive flow of development that is, at least on album, a worthy souvenir from this otherwise messy situation."   The soundtrack was released 11 October 2011 and features fifteen tracks of score at a running time of fifty-six minutes.

{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = 
| total_length    = 

| all_writing     = 
| all_lyrics      = 
| all_music       = 

| writing_credits = 
| lyrics_credits  = 
| music_credits   = 

| title1          = Dream House 
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = 
| length1         = 5:36

| title2          = Little Girls Die 
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = 
| length2         = 2:53

| title3          = Footprints in the Snow 
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = 
| length3         = 3:17

| title4          = Peter Searches
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = 
| length4         = 6:00

| title5          = Night Fever 
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = 
| length5         = 1:33

| title6          = Intruders 
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = 
| length6         = 1:41

| title7          = Libby Sees Graffiti 
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = 
| length7         = 2:33

| title8          = Peter Wards Room 
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = 
| length8         = 2:10

| title9          = Ghostly Playthings
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = 
| length9         = 3:17

| title10         = Peter Wards Story
| note10          = 
| writer10        = 
| lyrics10        = 
| music10         = 
| extra10         = 
| length10        = 3:13

| title11          = Ghost House
| note11           = 
| writer11         = 
| lyrics11         = 
| music11          = 
| extra11          = 
| length11         = 2:37

| title12          = Remember Libby 
| note12           = 
| writer12         = 
| lyrics12         = 
| music12          = 
| extra12          = 
| length12         = 4:05

| title13          = Murder Flashback 
| note13           = 
| writer13         = 
| lyrics13         = 
| music13          = 
| extra13          = 
| length13         = 3:59

| title14          = Peter Saves Ann/Redemption 
| note14           = 
| writer14         = 
| lyrics14         = 
| music14          = 
| extra14          = 
| length14         = 7:29

| title15          = Dream House End Credits 
| note15           = 
| writer15         = 
| lyrics15         = 
| music15          = 
| extra15          = 
| length15         = 5:55

}}

==Reception==
The film was not screened in advance for critics, and was critically panned. Review aggregation  , which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 35/100  based on 16 reviews. 

 

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 