The Defeated Victor
{{Infobox film
| name           = The Defeated Victor
| image          = 
| caption        = 
| director       = Paolo Heusch
| producer       = 
| writer         = Fausto Tozzi
| starring       = Maurizio Arena
| music          = Carlo Rustichelli	 	
| cinematography = Roberto Gerardi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Defeated Victor ( ) is a 1958 Italian drama film directed by Paolo Heusch. It was entered into the 9th Berlin International Film Festival.   

==Cast==
* Maurizio Arena as Romolo De Santis
* Lello Bersani as Speaker television
* Giulio Calì
* Cathia Caro as Giuditta
* Fosco Giachetti as Doctor boxing
* Alberto Grassi as Menicucci
* Tiberio Mitri as Enrico Costantini
* Giovanna Ralli as Lina
* Erminio Spalla as Coach (allenatore)
* Fausto Tozzi

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 