Not the Bradys XXX
{{Infobox film
| name           = Not the Bradys XXX
| image = Not the Bradys XXX Cover.jpg
| caption        =
| director       = Will Ryder
| producer       = Scott David, Jeff Mullen Hillary Scott Mike Horner  Paulina James  Veronique Vega  James Deen Ron Jeremy  Lynn LeMay
| distributor    = Larry Flynt Distribution
| released       = September 18, 2007
| language       = English
}}

Not the Bradys XXX is an adult film spoof the popular family television show The Brady Bunch, produced by Hustler Video.

The producers David & Mullen have made their careers out of lampooning pop culture in an adult entertainment vein. Before Not the Bradys they set their spotlight on Britney Spears when they made the adult film Britney Rears.

==Plot==
Faced with financial difficulties at home due to slow business at his architectural firm, Mike and his wife Carol reluctantly tell the kids that the entire household will be on a budget for the next few months until business picks back up and the cash crisis ends. The kids get together and decide to help out by taking on odd jobs, holding car washes and pitching in wherever they can to help save the family house from bank foreclosure. Mayhem ensues especially when Marcia unwittingly applies for a job as a figure model and finds out shes about to star in an adult movie.

==Publicity==
Not the Bradys got an unexpected boost of publicity due to the fact that its release date coincided with rumors that Brady Bunch actress Maureen McCormick admitted to a lesbian affair with co-star Eve Plumb in her memoir Heres the Story. Although the books publisher says the rumor is unfounded, the timing couldnt have been better. {{cite web|url=http://www.theinsideronline.com/news/2007/10/12925/
|title=A Brady Bunch Adult Film?|accessdate=2007-10-03|last=Insider|first=The|date=2006-12-23|work=}} 
 The Insider. {{cite web|url=http://www.avn.com/index.cfm?objectID=61AD88F2-C15E-CF73-0B27FBB2CAA73E7D
|title=A Brady Bunch Adult Film?|accessdate=2007-10-02|last=Sullivan|first=David|date=2006-12-23|work=}} 

== Sequels ==
The movie was successful enough to generate 4 sequels. These are:
* Not the Bradys XXX: Marcia, Marcia, Marcia (2008)
* Not the Bradys XXX: Pussy Power (2009)
* Not the Bradys XXX: Bradys Meet the Partridge Family (2010)
* Not the Bradys XXX: Marcia Goes to College (2013 compilation)

The Brady girls were portrayed by different actresses in the sequels. These include: Hillary Scott, Teagan Presley, Lexi Belle
* Jan: Aurora Snow, Dylan Riley
* Cindy: Leah Luv, Kacey Jordan
* Mrs. Carol Brady: Alana Evans
* Alice: Lynn LeMay

==Awards and nominations==
 
{| class="infobox" style="width: 25em; text-align: left; font-size: 90%; vertical-align: middle;"
|+  Accolades received by Not the Bradys XXX 
|-
|-
| colspan=3 |
{| class="collapsible collapsed" width=100%
! colspan=3 style="background-color: #D9E8FF; text-align: center;" | Awards & nominations
|-
|-  style="background:#d9e8ff; text-align:center;"
| style="text-align:center;" | Award
|  style="text-align:center; background:#cec; text-size:0.9em; width:50px;"| Won
|  style="text-align:center; background:#fcd; text-size:0.9em; width:50px;"| Nominated
|-
| style="text-align:center;"|
;AVN Awards
| 
| 
|-
| style="text-align:center;"|
;NightMoves Awards
| 
| 
|-
| style="text-align:center;"|
;XBIZ Awards
| 
| 
|-
| style="text-align:center;"|
;XRCO Awards
| 
| 
|}
|- style="background:#d9e8ff;"
| style="text-align:center;" colspan="3"|
;Total number of wins and nominations
|-
| 
| 
| 
|- style="background:#d9e8ff;" References
|}
{| class="wikitable"
|-
! Year
! Ceremony
! Result
! Category
! Recipient
|-
| 2007
| NightMoves Award
|   First Choice Award 
|  
|-
|rowspan="18"| 2008
|rowspan="13"| AVN Award
|   Best Actor, Video    Mike Horner Mike Horner
|-
|   Best Actress, Video  Hillary Scott Hillary Scott
|-
|   Best Art Direction, Video 
|  
|-
|   Best Director, Video  Will Ryder
|-
|   Best DVD Extras 
|  
|-
|   Best DVD Menus 
|  
|-
|   Best Music 
|  
|-
|   Best Non-Sex Performance  Lynn LeMay
|-
|   Best On-Line Marketing Campaign, Individual Project 
|www.BradysXXX.com
|-
|   Best Overall Marketing Campaign, Individual Project 
|  
|-
|   Best Screenplay, Video  Will Ryder
|-
|   Best Sex Comedy 
|  
|-
|   Best Supporting Actress, Video  Alana Evans
|-
| NightMoves Award
|   Best Comedy/Parody (Editors Choice) 
|  
|-
|rowspan="2"| XBIZ Award
|   Feature Movie of the Year 
|  
|-
|   Marketing Campaign of the Year 
|  
|-
|rowspan="2"| XRCO Award
|   Best Comedy or Parody 
|  
|-
|   Most Outrageous DVD Xtras 
|  
|}

== References ==
 

==External links==
 

*  
*  
*   at IAFD
*  
*  

 

 
 
 
 