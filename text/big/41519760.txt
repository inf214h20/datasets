The Better Angels (film)
{{Infobox film
| name           = The Better Angels
| image          = The Better Angels poster.jpg
| caption        = Film poster
| director       = A. J. Edwards
| producer       = Terrence Malick Nicolas Gonda Jake DeVito Charley Beil
| writer         = A. J. Edwards Jason Clarke Diane Kruger Brit Marling Wes Bentley Braydon Denney
| music          = Hanan Townshend
| cinematography = Matthew J. Lloyd
| editing        = Alex Milan
| studio         = Brothers K Productions Amplify
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 biographical drama-historical film about United States President Abraham Lincolns formative years. It was written and directed by A. J. Edwards.

The film had its premiere at 2014 Sundance Film Festival on January 18, 2014.  It was subsequently screened in the Panorama program at the 64th Berlin International Film Festival on February 8, 2014.   
 Amplify acquired distribution rights to the film. It was released on November 7, 2014.  

==Plot==
The story of Abraham Lincolns childhood, his upbringing in Indiana, and the hardships and tragedies that made him the man he was. 

==Cast== Sarah Lincoln Jason Clarke as Thomas Lincoln
* Brit Marling as Nancy Lincoln
* Wes Bentley as Mr. Crawford
* Braydon Denney as Abe (Young Abraham Lincoln)
*Cameron Mitchell Williams as Dennis Hanks

==Reception==
The Better Angels received mixed reviews from critics. Todd McCarthy of The Hollywood Reporter gave the film a positive review, saying its "a beautiful, arty, very Malick-influenced evocation of Abraham Lincoln’s childhood."  Katie Hasty  of HitFix praised the film by saying it is "a lushly conceived, exhaustively realized debut feature thatd be pretty formidable stuff coming from a more practised filmmaker — and derided in some quarters as a self-impressed knock-off."  Rodrigo Perez in his Indiewire review said the movie focuses "on mood, nature, divinity and celestial atmosphere" rather than story. 

Review aggregation website Rotten Tomatoes gives the film a score of 39 percent, based on reviews from 41 critics, 16 of which were positive. According to the website, the film is "Malick-inspired but not as inspired as Malick, The Better Angels muffles an interesting idea under ponderous execution."

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 


 
 