The Kingdom and the Beauty
 
{{Infobox film
| name           = The Kingdom and the Beauty
| image          = 
| caption        = 
| director       = Li Han-hsiang
| producer       = Run Run Shaw
| writer         = Yue-Ting Wang
| starring       = Lin Dai Zhao Lei
| music          = 
| cinematography = Jun Yang
| editing        = 
| distributor    = 
| released       =     
| runtime        = 100 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         = 
}}
 Best Foreign Language Film at the 32nd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Siu Loi Chow
* Lin Dai as Li Feng
* Li Jen Ho
* Bo Hong as Liu Jin Wei Hong as Lis sister-in-law
* King Hu as Ta Niu
* Ting Jing as (singing voice)
* Wei Lieh Lan
* Lam Ma as Chou Yung
* Rhoqing Tang as Dowager Empress
* Margaret Tu Chuan as Village Girl
* Yuanlong Wang as Liang Chu
* Chih-Ching Yang as Lis brother
* Lei Zhao as Emperor Chu Te Cheng

==See also==
* List of submissions to the 32nd Academy Awards for Best Foreign Language Film
* List of Singaporean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 
 