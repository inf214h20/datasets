Millennium Actress
{{Infobox film
| name           = Millennium Actress
| image          = sennenyoyu.jpg
| writer         = Sadayuki Murai Satoshi Kon
| starring       = Miyoko Shoji Mami Koyama Fumiko Orikasa Shōzō Iizuka
| music          = Susumu Hirasawa
| director       = Satoshi Kon
| producer       = Taro Maki
| cinematography = Hisao Shirai
| editing        = Satoshi Terauchi Madhouse
| distributor    = The Klockworx Co., Ltd
| released       =  
| runtime        = 87 minutes
| language       = Japanese
| country        = Japan
| budget         = 120,000,000 yen
}} director Satoshi Studio Madhouse. It tells the story of a documentary filmmaker investigating the life of an elderly actress in which reality and cinema become blurred. It is based on the life of Setsuko Hara  and Hideko Takamine. 

==Plot==
A movie studio is being torn down. TV interviewer Genya Tachibana has tracked down its most famous star, Chiyoko Fujiwara, who has been a recluse since she left acting some 30 years ago. Tachibana delivers a key to her, and it causes her to reflect on her career; as shes telling the story, Tachibana and Kyoji Ida, his long-suffering cameraman are drawn in. The key was given to her as a teenager by a painter and revolutionary that she helped to escape the police. She becomes an actress because it will make it possible to track him down, and she spends the next several decades acting out that search in various genres and eras. 

==Cast==
*Chiyoko Fujiwara is portrayed by three actresses during her lifetime:
**Miyoko Shōji voices Chiyoko as an elderly woman
**Mami Koyama voices Chiyoko as a middle-aged woman
**Fumiko Orikasa voices Chiyoko as a teenage girl
*Shōzō Iizuka as Genya Tachibana
**Masamichi Satō voices Genya as a young man
*Masaya Onosaka as Kyōji Ida
*Shōko Tsuda as Eiko Shimao
*Hirotaka Suzuoki as Junichi Otaki
*Tomie Kataoka as Mino
*Takkō Ishimori as the Chief Clerk
*Kan Tokumaru as the Ginei Managing Director
*Hisako Kyōda as Chiyokos mother
*Kōichi Yamadera as the Man with the Key
*Masane Tsukayama as the Man with the Scar

Additional voices were provided by Mitsuru Ogata, Tomohisa Asō, Kōji Yusa, Makoto Higo, Kōichi Sakaguchi, Tomoyuki Shimura, Akiko Kimura, Tomo Saeki, Hirofumi Nojima, Ruri Asano, Hiroko Ōnaka, Yoshinori Sonobe and Yumiko Daikoku.

==Production==
Following the release of Satoshi Kons previous film Perfect Blue, Kon considered adapting the Yasutaka Tsutsui novel Paprika (novel)|Paprika (1993) into his next film. However, these plans were stalled when the distribution company for Perfect Blue, Rex Entertainment, went bankrupt.    Millennium Actress had the same estimated budget as Perfect Blue (approximately 120,000,000 yen).    The screenplay was written by Sadayuki Murai,    who used a seamless connection between illusion and reality to create a "Trompe-lœil kind of film".    Millennium Actress is the first Satoshi Kon film to feature Susumu Hirasawa, whom Kon was a long-time fan of, as composer. 

==Reception==
Millennium Actress was favorably received by critics, gaining a 92% "fresh" rating at Rotten Tomatoes.    Los Angeles Times critic Kenneth Turan said of the film "as a rumination on the place movies have in our personal and collective subconscious, Millennium Actress fascinatingly goes where films have not often gone before".  Kevin M. Williams of the Chicago Tribune gave the movie 4 stars and put his feelings for the film this way: "A piece of cinematic art. Its modern day Japanese animation at its best... Its animated, but its human and will touch the soul of anyone who has loved deeply". 

===Box office performance===
 
{| class="wikitable"
|-
! Source !! Gross (United States dollar|USD) !! Number of Screens !! 
|-
| United States || $37,285 || 6 ||
|-
| United States Opening Weekend || $18,732 || 6 || 
|}

Commercially, the film performed modestly on its US release, earning $37,285 during its three-week release. The film was shown almost exclusively in New York and Los Angeles, and received a minimal advertising campaign from Go Fish Pictures.

=== Awards === 2001 Fantasia 8th Animation 2002 Mainichi 2001 Festival Academy Award Best Animated Feature, but it was not nominated. The film is ranked in the Top 50 Animated Films on the Internet Movie Database. 

==See also==
 
* Japanese films of 2001
 

==References==
 

==External links==
*   
* 
* 
*   at Keyframe - the Animation Resource
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 