Young Einstein
 
 
{{Infobox film
| name = Young Einstein
| image =Young-bg.gif
| caption = VHS cover.
| director = Yahoo Serious
| producer =  
| writer = David Roach, Yahoo Serious
| starring =  
| music =  
| cinematography = Jeff Darling
| editing =  
| distributor = Warner Bros. Pictures
| released = December 1988 (Australia)   (U.S.)
| runtime = 91 minutes
| country = Australia
| language = English
| budget = $5 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 pp. 315&ndash;318 
| gross =   (AUS) US$11,536,599 (US)
}}
Young Einstein is an Australian comedy film directed by and starring Yahoo Serious, released in 1988. It was based loosely on the life of Albert Einstein, but relocated the theoretical physicist to Australia and had him splitting the atom with a chisel, inventing rock and roll and surfing. Although the film was highly successful in Australia, and won an award from the Australian Film Institute Awards, it was poorly received by critics in America.

==Plot==
Albert Einstein, the son of an apple farmer in Tasmania in the early 1900s, splits a beer atom with a chisel in order to add bubbles to beer, discovers the theory of relativity and travels to Sydney to patent it. While there, he invents the electric guitar and surfing while romancing Marie Curie. He invents rock and roll and uses it to save the world from being destroyed due to misuse of a nuclear reactor under the watchful eye of Charles Darwin.

==Cast==
*Yahoo Serious - Albert Einstein
*Odile Le Clezio - Marie Curie John Howard - Preston Preston
*Peewee Wilson - Mr. Einstein
*Su Cruickshank - Mrs. Einstein
*Lulu Pinkus - The Blonde
*Kaarin Fairfax - The Brunette Michael Lake - Manager Jonathan Coleman - Wolfgang Bavarian
*Johnny McCall - Rudy Bavarian / Tasmanian Devil
*Michael Blaxland - Desk Clerk
*Ray Fogo - Bright Clerk
*Zanzi Mann - Young Kid
*Terry Pead - Inventor Couple
*Alice Pead - Inventor Couple
*Frank McDonald - Nihilist Basil Clarke - Charles Darwin

==Production== sticking out his tongue, taken by photographer Arthur Sasse.   
 David Roach. It was a story about an Australian who invents rock and roll. The two developed The Great Galute into Young Einstein. 

The film was created on an extremely low budget, so low that Serious sold his car to generate funds, cameras were borrowed,    and his mother cooked for the crew.   

Serious managed to get Australian Film Commission support for the movie. By March 1984, an hour of the film had been shot, partly by the AFC and partly by private investment. Philippa Hawker, "Young Einstein", Australian Film 1978-1992, Oxford Uni Press, 1993 p261  Serious was then able to pre-sell the film to an American company, Film Accord, for $2 million. This enabled him to raise the films original budget of $2.2 million.  The movie started filming again late in 1985 and went for seven weeks, from 23 September, taking place in Newcastle and Wollombi, near Cessnock in the Hunter Valley, with second unit at various locations throughout Australia. A 91 minute version of the film was entered in the 1986 AFI Awards where composer William Motzing won Best Music.  "Production round-up", Cinema Papers, November 1985 p48 

In 1986, Film Accord sued the production to recover its distribution guarantee and the rushes, claiming the film delivered was not the one it had contracted to buy. The dispute was settled out of court. 

Serious was unhappy with his first version of the film. Philippa Hawker, "Start Laughing ", Cinema Papers, January 1989 p11-12  Graham Burke from Roadshow saw it and became enthusiastic about its possibilities. Roadshow bought out Film Accord in March 1987, persuaded Warner Bros. to take on the film for international distribution outside Australia, and financed re-shooting, re-editing and re-scoring, resulting in an hour of new material (including a new ending) and new music score (including the addition of songs by bands such as Mental As Anything). This pushed the budget of the film up to $5 million. Warner Bros. contributed   to the full version of the film, and would go on to spend eight million on marketing the film in the United States alone.    

Serious key collaborators in the movie were co-writer David Roach, co-producer Warwick Rodd and associate producer Lulu Pinkus. He has said it helped that they all shared the same vision for the movie which got them through the long production process. 

Serious refused to consider making a sequel to the film, as he stated in interviews that he was opposed to them in general.   

==Reception==

===Critical response===
The film overall received mixed to negative reviews in the United States, with   was more tempered, noting that though the film was "an uneven series of sketches strung along an extended joke", that the first time director Serious "is a much more adept film maker than his loony plot suggests."   

The Los Angeles Times gave a favorable review, saying the film would appeal to younger audiences and that "its just about impossible to dislike a movie in which examples of the heros pacifism include his risking his life to save kitties from being baked to death inside a pie."    Neil Jillett of Australias The Age reviewed the film positively, noting that despite some "directorial slackness", the film was "a lively work that is sophisticated and innocent, witty and farcical, satirical and unmalicious, intelligent but not condescending, full of concern with big issues but not arrogantly didactic, thoroughly Australian but not nationalistic."  Variety (magazine)|Variety meanwhile thought that the film relied on the performance of Yahoo Serious, who they described "exhibits a brash and confident sense of humor, endearing personality, and a fondness for sight gags." 

In the UK, William Russell for the Glasgow Herald described the film as trying "too hard to be funny for its own good." The film currently holds a 35% rating on Rotten Tomatoes based on 31 reviews.

===Box office===
Young Einstein grossed A$13,383,377 at the box office in Australia.  It grossed over US$11 million in its United States theatrical run.  On release in Australia, it became the fifth biggest opening in Australian film history behind "Crocodile" Dundee and its "Crocodile" Dundee II|sequel, Rocky IV and Fatal Attraction. It grossed A$1.26 million in the opening weekend, despite being released in only three states.    It was only the third film of 1988 to break the A$1 million barrier at the Australian box office.  Young Einstein became the tenth most successful film released at the Australian box office,    after being the second most successful Australian film ever on release after "Crocodile" Dundee. 

The film has been released on DVD in region 1. The DVD is available in Australia by LA Entertainment.

===Awards and nominations===
{| class="wikitable" width="95%" cellpadding="5"
|- Organization
! Award category Result
|-
!scope="row" rowspan=4| Australian Film Institute Awards  Best Cinematography
| 
|- Best Original Music Score
| 
|- Best Original Screenplay
| 
|- Best Sound
| 
|}

==See also==
*Cinema of Australia
*Albert Einstein

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 