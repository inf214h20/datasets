Sampagitang Walang Bango
{{infobox book |  
| name = Sampagitang Walang Bango
| title_orig =
| translator =
| image =  
| author = Iñigo Ed. Regalado
| cover_artist =
| country = Philippines Tagalog
| series =
| genre = Novel
| publisher = P. Sayo 
| release_date = 1921
| media_type = Print 
| pages = 271
| isbn = 
| dewey=
| congress=
| oclc= 
| preceded_by = Ang Labing-apat na Awa 
| followed_by = May Pagsintay Walang Puso 
}}
  Jasmine Without Fragrance"), also rendered as Sampaguitang Walang Bango,  was a 1921 Tagalog-language novel written by notable Filipino novelist Iñigo Ed. Regalado. The theme of the novel revolves around love, romance, treachery, and endurance. In the novel, Regalado depicted the City of Manila during the American occupation of the Philippines    but before World War II.  Sampagitang Walang Bango was one of the novels Regalado had written during the Golden Age of the Tagalog Novel (1905-1935).   

==Plot== playboy husband of Nenita.  The alienated Nenita, weakened and rebelling against Bandinos indicencies, succumbed to an extramarital affair with Pakito, a lawyer.  Pakito was a man engaged to be married to Liling, a modest and demure woman.  After discovering Nenitas affair with Pakito, Bandino tried to commit suicide.  Only Bandinos daughter was able to stop him from shooting himself.  Bandino, with his daughter, left the Philippines.  Nenita, already abandoned by her husband, was also left by Pakito to fend for herself. 

==Description== balls (formal dance parties) that were presided over by the queen of the carnival   ), lavish parties, outings during the weekends, and the fashion trends of the times.   The mixed themes of "love, self-control, and infidelity" were portrayed by the author as events that happened within a "carnival-like mileiu".     The 2001 edition of the novel was published by the Ateneo de Manila University Press, and was edited with an introduction written by literary critic Roberto T. Añonuevo.   A variation of Sampagitang Walang Bango is Regalados Anak ng Dumalaga (Child of the Pullet).   

==Film adaptation==
Sampagitang Walang Bango was adapted into a film in 1937.     The film version was directed by Fermín Barva for Filippine Films and starred Leopoldo Salcedo and Angelita "Rhumba" Rey.   

==See also==
*May Pagsintay Walang Puso

==References==
 

 
 
 
 
 
 
 
 
 
 