A Bird in the Head
{{Infobox Film |
  | name           = A Bird in the Head
  | image          = BirdinHeadTITLE.jpg
  | image size     = 300px
  | caption        = 
  | director       = Edward Bernds
  | writer         = Edward Bern
  | starring       = Moe Howard Larry Fine Curly Howard Vernon Dent Robert Williams Frank Lackteen Art Miles
  | producer       = Hugh McCollum
  | cinematography = Burnett Guffey
  | editing        = Henry Batista
  | distributor    = Columbia Pictures
  | released       = February 28, 1946 (United States|U.S.)
  | runtime        = 16 54"
  | country        = United States
  | language       = English
}}
A Bird in the Head is the 89th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are mediocre paperhangers. Their client Mr. Beedle (Robert Williams) advises the boys to do a good job, but the end result looks like it was quickly cluttered with paper towels. Beedle is fuming, and threatens the boys, who make a quick escape across the hallway into the laboratory of the insane Professor Panzer (Vernon Dent) and his assistant Nikko (Frank Lackteen). Panzer is searching for a human brain puny enough to place in the head of his gorilla Igor (Art Miles). Curly becomes the prime candidate, and Panzer locks the boys in his lab in order to secure Curlys "contribution." Then Igor gets loose, but takes a liking to Curly, which the feeble-minded Stooge reciprocates. Eventually, the boys destroy Panzers lab and quickly depart, taking Igor with them.

==Production notes==
The title A Bird in the Head is a pun on the phrase "a bird in the hand is worth two in the bush."  A Bird in the Head was filmed over a period of five days (April 9-13, 1945), which was longer than usual. Due to the death of President Franklin D. Roosevelt on April 12, filming ended early out of respect for the deceased Commander-in-chief. Edward Bernds commented that the extra day of shooting allowed him to gather his thoughts, and devise ways to film around Curlys illness. 

==Curlys illness==
41-year-old Curly Howard had suffered a series of minor strokes prior to filming A Bird in the Head. As a result, his performance was marred by slurred speech, and slower timing. This film was the first directing effort for former Columbia sound man Edward Bernds. Bernds was thrilled that he was being given a shot at directing, but was horrified when he realized that Curly was in such bad shape (something Columbia short subject head/director Jules White failed to alert Bernds of).    Years later, Bernds discussed his trying experience during the filming of A Bird in the Head:
   I had seen Curly at his greatest and his work in this film was far from great. The wallpaper scene was agony to direct because of the physical movements required to roll up the wallpaper and to react when it curled up in him. It just didnt work. As a fledgling director, my plans were based on doing everything in one nice neat shot. But when I saw the scenes were not playing, I had to improvise and use other angles to make it play. It was the wallpaper scene that we shot first, and during the first two hours of filming, I became aware that we had a problem with Curly." }} 

Realizing that Curly was no longer able to perform in the same capacity as before, Bernds devised ways to cover his illness. Curly could still be the star, but the action was shifted away from the ailing Stooge. In A Bird in the Head, the action focuses more on crazy Professor Panzer and Igor. This allowed Curly to maintain a healthy amount of screen time without being required to contribute a great deal.   

Bernds often commented that he and Jules White never really got along. Bernds feared that his directing days would be over as soon as they began if he released A Bird in the Head with a weak Curly as his first entry. Producer Hugh McCollum reshuffled the release order, and the superior Micro-Phonies was released first, securing Bernds directing position. 
 .]]

== References ==
 

== External links ==
*  
*  
*  at  

 

 
 
 
 
 
 
 
 