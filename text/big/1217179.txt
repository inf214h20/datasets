Cabin in the Sky
 
{{Infobox film
| name           = Cabin in the Sky
| image          = Cabin in the Sky.jpg
| caption        = Theatrical release poster
| director       = Vincente Minnelli Busby Berkeley ("Shine" sequence, uncredited)
| based on       = Cabin in the Sky (play) Albert Lewis
| writer         = Marc Connelly (uncredited) Lynn Root (play) Joseph Schrank
| starring       = Ethel Waters Eddie "Rochester" Anderson Lena Horne Louis Armstrong
| music          = Harold Arlen Vernon Duke George Bassman Roger Edens Sidney Wagner
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $679,000  .  gross = $1,953,000 
}}
 John La Broadway at Albert Lewis Rex Ingram Lucifer Junior, and Todd Duncan as The Lawds General.
 Eddie "Rochester" Anderson of Jack Benny fame took over the role of Little Joe, Kenneth Lee Spencer portrayed The General, and Lena Horne co-starred as the temptress Georgia Brown in her first and only leading role in an MGM musical. Louis Armstrong was also featured in the film as one of Lucifer Juniors minions, and Duke Ellington and his Orchestra have a showcase musical number in the film.

==Plot summary==
 
Cabin in the Sky tells a version of the Faust legend in which Little Joe, a man killed over gambling debts, is restored to life by angelic powers and given six months to redeem his soul and become worthy of entering Heaven—otherwise he will be condemned to Hell.  Secretly guided by "The General" (the Lords Angel), Little Joe gives up his shiftless ways and becomes a hardworking, generous, and loving husband to his wife Petunia, whom he had previously neglected.  Unfortunately, demon Lucifer Jr. (the son of Satan himself), is determined to drag Little Joe to Hell.  Lucifer arranges for Joe become wealthy by winning a lottery, reintroduces Joe to beautiful gold-digger Georgia Brown, and manipulates marital discord between Joe and Petunia.  Little Joe abandons his wife for Georgia, and the two embark on a life of hedonistic pleasure.  As Little Joe and Georgia celebrate at a nightclub one evening, Petunia joins them, determined to win Joe back.  Little Joe fights with Domino for Petunia and she prays for God to destroy the nightclub. A cyclone appears and leaves the nightclub in ruins, as Joe and Petunia lie dead in the ruins after being shot by Domino.  Just as it appears that Joes soul is lost forever, the angelic General informs him that Georgia was so affected by the tragedy that she has donated all the money that he had given her to the church.  On this technicality, Little Joe is allowed to go to Heaven with Petunia.  As the two climb the Celestial Stairs, Joe suddenly wakes in his own bed.  He had not been killed in the initial gambling-debt fracas—he had only received a concussion, and all his supposed dealings with angels and demons were only a fever dream.  Now genuinely reformed, Little Joe begins a new, happy life with his loving Petunia.

==Cast==
* Ethel Waters as Petunia Jackson  
* Eddie Rochester Anderson as Little Joe Jackson  
* Lena Horne as Georgia Brown  
* Louis Armstrong as The Trumpeter   Rex Ingram as Lucius / Lucifer Jr.   Kenneth Spencer as The General / Rev. Green   John William Sublett as Domino Johnson (as Bubbles John W. Sublett)  
* Oscar Polk as The Deacon / Fleetfoot  
* Mantan Moreland as First Idea Man  
* Willie Best as Second Idea Man  
* Fletcher Rivers as Third Idea Man (as Moke Fletcher Rivers)  
* Leon James Poke as Fourth Idea Man (as Poke Leon James)   Bill Bailey as Bill  
* Ford Washington Lee as Messenger Boy (as Buck Ford L. Washington)  
* Butterfly McQueen as Lily

==Overview and history==
Produced by Arthur Freed and directed by Vincente Minnelli in his Hollywood debut, Cabin in the Sky in featuring an all-African American cast was an unusual production for its time. In the 1940s, movie theaters in many cities, particularly in the southern United States, refused to show films with prominent black performers, so MGM took a considerable financial risk by approving the film.

The film was nominated for an Academy Award for Best Original Song for "Happiness is a Thing Called Joe" sung by Ethel Waters. 

Some remember Cabin in the Sky for its intelligent and witty script, which some claimed treated its characters and their race with a dignity rare in American films of the time. Others, like actress Jean Muir, described Cabin in the Skys racial politics as "an abomination," arguing that moviegoers should write to the studios when they saw "old stereotypes of Negro caricature" like those in the film.  According to liner notes in the CD reissue of the films soundtrack, Freed and Minnelli sought input from black leaders before production began on the film.
 Pete Smith short subject entitled Studio Visit. As Horne later said in the documentary Thats Entertainment! III in which the excised performance was also featured, it was felt that to show a black woman singing in a bath went beyond the bounds of moral decency in 1943. (She did not state what she felt had changed in those three years to make it acceptable in 1946.) A second (non-bubble bath) performance of this song by Louis Armstrong was also cut from the final print, resulting in the famous trumpeter having no solo musical number in the film.

After years of unavailability, Warner Home Video and Turner Entertainment released Cabin in the Sky on DVD on January 10, 2006.

==Songs==
* "Taking a Chance on Love"
* "Cabin in the Sky"
* "Honey in the Honeycomb"
* "Aint It the Truth"
* "Love Me Tomorrow"
* "Happiness is a Thing Called Joe"
* "Things Aint What They Used To Be"
* "Shine (1910 song)|Shine"

==Reception==
According to MGM records the film made $1,719,000 in the US and Canada and $234,000 elsewhere resulting in a profit of $587,000.  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 321 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
* 

{{Navboxes|list1=
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 