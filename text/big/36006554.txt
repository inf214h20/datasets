Ricardo, Miriam y Fidel
 

{{Infobox film
| image  = Ricardo, Miriam y Fidel movie poster.jpg
| caption = Ricardo, Miriam y Fidel movie poster
| name = Ricardo, Miriam y Fidel
| starring = Ricardo Martínez   Miriam Martínez
| director = Christian Frei
| cinematography = Peter Indergand
| editing = Christian Frei, Damaris Betancourt
| producer = Christian Frei
| released = April 1997
| runtime = 90 min.
| country = Switzerland
| language = English, Spanish
| music = Arturo Sandoval   Chucho Valdés
 }}

Ricardo, Miriam y Fidel is the first feature length documentary of the Swiss director Christian Frei. It had his premiere at the Swiss film festival Visions du Réel. The film is a portrait of Miriam Martínez and her father, the Cuban revolutionary Ricardo Martínez. Daughter and father are torn between the desire to emigrate to the United States and the faith in the revolutionary ideas.

== Plot ==
The film talks about two individuals and their destiny. It shows the loss of utopia and the struggle of ideologies in  . Together with Che Guevara he has founded the Pirate radio Radio Rebelde that became the main revolutionary voice of Cuba. 
The younger Cuban generation listens to Radio y Televisión Martí that broadcasts from Miami, Florida. 
Miriam represents this younger generation. By comparing this to radio stations, the film is as well about media history.

== Reception ==
The Swiss journal Der Bund has emphasized the overall impact of the film:

 

The Swiss film critic Norbert Creutz has estimated the objectivity and the formal quality of the film:

 

The film had a positive reception as well in the Hispanic society:

 

== Awards ==
* Basic Trust International Human Rights Film Festival Ramallah-Tel Aviv 2000: winner public award
*   1998: competition
*   Saarbrücken 1998: section «Perspektiven des jungen Films»
* Solothurn Film Festival 1998: opening film
* International Documentary Film Festival Amsterdam IDFA 1997: section «Reflecting Images»
* São Paulo International Film Festival 1997: section «Competicao Novos Diretores»
* San Sebastián International Film Festival 1997: section «Made in Spanish»
* Human Rights Watch International Film Festival, New York City 1997: selection
* Chicago Latino Film Festival] 1997: selection
* Visions du Réel Nyon 1997: competition

==External links==
*  
*  

==References==
 

 
 
 