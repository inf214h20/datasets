Aa Gaye Munde U.K. De
{{Infobox film
| name = Aa Gaye Munde U.K. De
| image =  Manmohan Singh
| producer = Sunny Trehan Manmohan Singh
| starring =Jimmy Shergill Neeru Bajwa Gurpreet Ghuggi Binnu Dhillon Khushboo Grewal Om Puri Gugu Gill  
| music director = Jatender Shah 
| lyrics = Kumar and Veet Baljit
| editor = Manish More
| cinematography = Nazir Khan
| distributor = Yash Raj Films
| released =  Punjabi with English Subtitles
|}}
 Punjabi romantic Manmohan Singh and produced by Sunny Trehan/Trehan Productions. The film features Jimmy Shergill, Neeru Bajwa, Gurpreet Ghuggi, Binnu Dhillon, Khushboo Grewal, Om Puri and Gugu Gill.  Aa Gaye Munde U.K. De is scheduled for release on 8 August 2014.

Aa Gaye Munde U.K. De is a sequel to the Munde U.K. De. The trailer was launched on 10 July 2014.   

==Cast==
* Jimmy Shergill as Roopinder Singh Grewal (Roop) 
* Neeru Bajwa as Disha
* Om Puri as Disha’s father Dilip Singh Dhillon
* Khushboo Grewal Lovely
* Binnu Dhillon Mintu
* Navneet Nishan Roopinders Mother
* Gugu Gill Roopinders Father(Harjit Singh Grewal)
* Gurpreet Ghuggi as Roops best friend DJ - Daljeet Singh Jugaadi.


==Awards==
PTC Film Awards 2015

*Nominated - Best Editing - Manish More
*Nominated - Best Cinematography - Nazir Khan
*Nominated - Best Dialogues - Rana Ranbir
*Nominated - Best Supporting Actor - Gurpreet Ghuggi
*Nominated - Best Actress - Neeru Bajwa
*Nominated - Best Actor - Jimmy Shergill

== References ==
 

 