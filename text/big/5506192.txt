Swaham
{{Infobox film
| name           = Swaham
| image          = Swahamposter.jpg
| director       = Shaji N. Karun
| writer         = Shaji N. Karun S. Jayachandran Nair Reghunath Paleri
| starring       = Ashwini Kalamandalam Haridas Bharath Gopi Praseetha Mullenezhi Sarath Venmani Vishnu  Gopalakrishnan
| producer       = Shaji N. Karun
| distributor    =
| cinematography = Hari Nair
| editing        = P. Raman Nair
| released       = 1994
| runtime        = 141 minutes
| country        = India
| language       = Malayalam
| music          = Isaac Thomas Kottukapally K. Raghavan
}}

Swaham (My Own) (1994) is an award-winning feature film directed by Shaji N. Karun. The film is in Malayalam. It stars Ashwini of Uthiripookkal fame, Venumani Vishnu and Mullenezhi. The films music is composed by Isaac Thomas Kottukapally and K. Raghavan. Swaham met with widespread critical acclaim upon release. The film was screened at the 1994 Cannes Film Festival.   

==Plot==

Annapoorna lives with her son and daughter, and struggles to get by, trying to earn a living from the teashop near a railway station once owned by her now deceased husband. Her son Kannan does whatever he can to help whilst trying to continue his education, however this proves difficult and he fails his exams. His mother sends him to a military recruitment camp, hoping he will find employment. This seems to be the familys only hope, however this option is expensive. Eventually Kannan is admitted after Annapoorna pays a hefty sum of money to a man associated with the camp. Later, Kannan is killed in a stampede at the camp, and his bereaved mother brings his body back home in an ambulance. Annapoornas daughter waits anxiously for the return of her mother and brother.

==Awards and nominations==
The film has won and been nominated for the following awards since its release:

1995 Bergamo Film Meeting (Italy)
* Won - Bronze Rosa Camuna - Shaji N. Karun

1994 Cannes Film Festival (France) Golden Palm - Shaji N. Karun 
 National Film Awards (India)
* Won - Silver Lotus Award - Special Jury Award - Director - Shaji N. Karun

1995 Innsbruck Film Festival (Austria)
* Won - Best Film - Swaham - Shaji N. Karun

==Cast==
*Ashwini
*Kalamandalam Haridas
*Bharath Gopi
*Praseetha
*Mullenezhi
*Sarath
*Venmani Vishnu
*Gopalakrishnan

==References==
 

==External links==
* 

 

 
 
 
 
 
 