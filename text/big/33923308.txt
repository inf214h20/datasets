Alemari
{{Infobox film
| name = Alemari
| image = Alemari poster.jpg
| caption = Film poster
| director = Santhu
| producer = B. K. Srinivas
| writer = Santhu
| screenplay = Santhu Yogesh Radhika Pandit Raju Talikote Rakesh Adiga
| music = Arjun Janya
| cinematography = Manjunath M. Nayak
| editing = K. M. Prakash
| distributor = 
| studio = Ashwini Media Networks 9 Thots Entertainment
| released =  
| runtime = 
| country = India Kannada
| budget = 
| gross = 
}}
 Kannada romantic Yogesh and Radhika Pandit in the lead roles. The story, screenplay, lyrics and direction is by Santhu. The music of the film was composed by Arjun Janya. B. K. Srinivas has produced the venture under BK Srinivasa Productions. The film released in theatres across Karnataka on 9 March 2012.  However the film is rated as an average, but the director santhu fetched best director in Karnataka state film awards 2012-13.

== Cast == Yogesh
* Radhika Pandit
* Rakesh Adiga
* Ramesh Bhat
* Umashri
* Adhi Lokesh
* Raju Thalikote
* Nayana

==Critical Reception==
Alemari opened across Karnataka screen to positive response from all over. Supergoodmovies.com gave the film 3 stars out of 5 saying, "The cinematography, editing, art direction, music and stunts are top class in ‘Alemari’. The first half of the film is a real musical treat and second half the action scenes, rain effect and two more songs gives good standing to this film". Further it commented on Yogesh performance as, "This is the career best performance of Yogish. He really needs an award because of his portrayal in the change of his outlook. His mannerisms especially shaking the head, walking style is the growth of an actor in Yogi. He has his quota of action, dance and less hair on the head and slight beard he looks better." 

== Soundtrack ==
{{Infobox album  
| Name        = Alemari
| Type        = Soundtrack
| Artist      = Arjun Janya
| Cover       = 2012 Kannada film Alemari album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 29:06
| Label       = Ashwini Media
}}

Arjun Janya composed the films background score and music for its soundtrack, with all the lyrics written by Santhu. The soundtrack album consisting of nine tracks was distributed by Ashwini Media. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 29:06
| lyrics_credits = yes
| title1 = Ale Aleyo
| lyrics1 = Santhu
| extra1 = Fayaz Khan
| length1 = 4:18
| title2 = Alemari
| lyrics2 = Santhu
| extra2 = Fayaz Khan
| length2 = 1:21
| title3 = Baa Baa
| lyrics3 = Santhu
| extra3 = Arjun Janya, Rakesh Adiga, Chandan, Harsha, Santhosh
| length3 = 3:35
| title4 = Dheeraja
| lyrics4 = Santhu
| extra4 = Harsha
| length4 = 1:12
| title5 = Maribeku Ninna
| lyrics5 = Santhu Karthik
| length5 = 3:57
| title6 = Nee Modala Kavithe
| lyrics6 = Santhu
| extra6 = Vijay Prakash
| length6 = 4:33
| title7 = Neeli Neeli
| lyrics7 = Santhu
| extra7 = Javed Ali, Shreya Ghoshal
| length7 = 4:29
| title8 = Neeli Neeli (Patho)
| lyrics8 = Santhu
| extra8 = Javed Ali, Shreya Ghoshal
| length8 = 1:25
| title9 = Thundu Beedi
| lyrics9 = Santhu
| extra9 = Kailash Kher, Priya Himesh
| length9 = 4:16
}}

==References==
 

 
 
 
 
 


 