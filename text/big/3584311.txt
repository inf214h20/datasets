Aberdeen (2000 film)
 {{Infobox film
| name           = Aberdeen
| image          = Aberdeen (film).jpg
| caption        = Swedish DVD cover
| director       = Hans Petter Moland
| producer       = Petter J. Borgli   Tom Remlov
| writer         = Kristin Amundsen   Lars Bill Lundholm
| starring       = Stellan Skarsgård   Lena Headey   Charlotte Rampling
| music          = Zbigniew Preisner
| cinematography = Philip Øgaard
| editing        = Sophie Hesselberg
| distributor    = Svensk Filmindustri (SF) AB (Sweden)    First Run Features (USA)
| released       =  
| runtime        = Norway: 113 minutes   USA: 106 minutes
| country        = United Kingdom Norway Sweden
| language       = English
| budget         = $6,500,000
| gross          =
}}

Aberdeen is a 2000 Norwegian-British drama film directed by Hans Petter Moland, starring Stellan Skarsgård, Lena Headey and Charlotte Rampling.

==Synopsis==
Lena Headey stars as Kaisa, a young lawyer, just promoted, who has no apparent emotional attachments, prefers nameless encounters with men, and is surprised to receive a call from her dying mother (Charlotte Rampling) with a final request to bring her estranged father (Stellan Skarsgård) to the hospital. The film is a bit of a road movie with encounters along the way, some confrontational, such as boozing louts who harass her father or angry stewardesses issuing ultimatums, while some are romantic, such as a truck driver (Ian Hart) Kaisa attempts to use, but instead finds herself attached to. What started as an unavoidable chore, perhaps the last shed never be able to dodge, becomes a new starting point in her life.

==Cast==
*Stellan Skarsgård - Tomas Heller
*Jean Anderson - Young Kaisa
*Lena Headey - Kairo Kaisa Heller
*Charlotte Rampling - Helen
*Ian Hart - Clive
*Louise Goodall - Sara Jason Hetherington - Perkins
*Kate Lynn Evans - Emily
*John Killoran - Blake
*Fergis McLarnon - Eric
*Anders T. Andersen - Customs Official
*Nina Andresen Borud - Flight Attendant
*Henriette Steenstrup - Car Rental Clerk
*Kari Simonsen - Waitress
*J.J. Mckeown - Boy at door
*Jan Grønli - Granbakken Gard Eidsvold - Disagreeable Man

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 