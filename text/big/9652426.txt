Clothes Make the Pirate
{{Infobox film
| name           = Clothes Make the Pirate
| image          = Clothesmakethepirate1925.jpg
| image_size     =
| caption        =
| director       = Maurice Tourneur
| producer       = Sam E. Rork Productions
| writer         = Marion Fairfax
| based on       =  
| starring       = Leon Errol Dorothy Gish Nita Naldi Tully Marshall|
| cinematography = Henry Cronjager Louis Dunmyre
| editing        = Patricia Rooney
| distributor    = First National Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States Silent (English intertitles)
| budget         =
}} Holman Francis trailer surviving.

==Synopsis==
The film is a comedy that centers on a disgruntled 18th century Bostonian, played by Errol, who while wishing that he was a pirate, dons the clothes and play-acts the part. He is mistaken for the real pirate, Dixie Bull (played by Walter Law) whom Errol, of course, bumps into later in the film. More importantly, Errol "slays" the villain and puts his foot upon the pirates head. This is more than enough and he heads back home to his unappreciated wife, played by Dorothy Gish.

==Cast==
*Leon Errol	as Tremble-at-Evil Tidd
*Dorothy Gish as Betsy Tidd
*Nita Naldi	as Madame De La Tour
*George F. Marion as Jennison
*Tully Marshall as	Scute
*Frank Lawlor as Crabb
*Edna Murphy as Nancy Downs James Rennie as Lieutenant Cavendish Walter Law as Dixie Bull
*Reginald Barlow as Captain Montague

==Reception==
Contemporary reviewers of the time claimed Errol was miscast, perhaps for the comedic cowardice of the part. Variety Magazine gave the film a poor review, stating that the children would like it. However other reviews, such as that in the Los Angeles Times of 10 Jan 1926 gave the film, as a satire, generally good reviews.

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 