Free Hand for a Tough Cop
{{Infobox film
| name = Free Hand for a Tough Cop
| image = Il trucido e lo sbirro.jpg
| caption =
| director = Umberto Lenzi
| writer = Umberto Lenzi Dardano Sacchetti Elisa Briganti
| starring = Tomas Milian Claudio Cassinelli Henry Silva
| music = Bruno Canfora
| cinematography = Luigi Kuveiller
| editing = Eugenio Alabiso
| producer = Claudio Mancini Ugo Tucci
| distributor =
| released =  
| runtime = 90 minutes
| awards =
| country = Italy
| language = Italian
| budget =
}}
Free Hand for a Tough Cop  ( , also known as Tough Cop) is an Italian poliziottesco-action film directed in 1976 by Umberto Lenzi. In this movie Tomas Milian plays for the first time Sergio Marazzi aka "Er Monnezza", a role that he later played several more times, in Lenzis Brothers Till We Die (1978, a sort of sequel of this movie), in Destruction Force by Stelvio Massi (1977) and, with slight differences, in Uno contro laltro, praticamente amici by Bruno Corbucci (1980) and in Francesco Massaros Il lupo e lagnello (1980).  

== Plot ==
Camilla is a little girl suffering with a kidney disorder. Before she can receive her next due treatment she gets kidnapped. The gangsters intend to blackmail her rich father. Commissario Antonio Sarti knows that time is running out on the victim and takes desperate measures. He secretly organises a prison escape for petty crook Sergio Marazzi. By using Marazzis insider knowledge of the criminal milieu Sarti detects kidnappers hideout.

== Cast ==
* Tomas Milian: Sergio Marazzi aka "Er Monnezza"
* Claudio Cassinelli: Commissario Antonio Sarti
* Henry Silva: Brescianelli
* Biagio Pelligra: Calabrese
* Robert Hundar: Mario er Cinico
* Ernesto Colli: Roscetto
* Nicoletta Machiavelli: Mara
* Tano Cimarosa: Cravatta
* Massimo Bonetti: pickpocket
* Luciano Rossi: dealer

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 