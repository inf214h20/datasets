Suspiria
 
 
{{Infobox film
| name           = Suspiria
| image          = SuspiriaItaly.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Original Italian theatrical release poster
| director       = Dario Argento
| producer       = Claudio Argento Salvatore Argento
| writer         = Dario Argento Daria Nicolodi
| based on       =  
| narrator       = Dario Argento Miguel Bosè Alida Valli Joan Bennett Goblin Dario Argento
| cinematography = Luciano Tovoli
| editing        = Franco Fraticelli
| studio         = Seda Spettacoli
| released       =  
| runtime        = 98 minutes  
| country        = Italy
| language       = Italian Russian English German Latin
| budget         =  ITL £1,430,000,000  (Italy)  $1,800,000 (U.S./Canada|CA) 
}} Claudio and Miguel Bosè, Alida Valli, Udo Kier, and Joan Bennett in her final film role. It is the first Dario Argentos horror film to have THX-certified audio and video.
 cult classic and a remake was planned for 2013, but was subsequently cancelled.

==Plot==
Suzy Bannion, an American ballet student, arrives from her flight in Munich, Germany on a stormy night to enroll in a prestigious dance academy in Freiburg im Breisgau|Freiburg. After Suzy is unable to gain access into the academy, she decides to spend the night in town. Meanwhile, Pat Hingle, a former student who is expelled from the academy and seen leaving the academy in a somewhat frightened mood by Suzy, finds refuge at a friends apartment in town. After Pat locks herself inside of the bathroom, a mysterious arm smothers Pat against the glass. Her friend overhears her screaming and tries to scream for help, although nobody replies. Pat is then repeatedly stabbed in the chest and stomach with a large knife and bound with a cord, before she is hung in mid-air after crashing through a large stained-glass ceiling. Her friend is killed directly below by the falling glass and metal.

Upon Suzys arrival at the academy the next morning, she is introduced to Madame Blanc and Miss Tanner. She is escorted to the ballet students locker room, where she meets Sarah and Olga, the latter with whom she has arranged to stay off-campus. The following morning, Blanc offers Suzy a dormitory room, but she declines Blancs offer. After a strange encounter with the academys cook, Suzy seemingly struggles before fainting during a lesson. Later that night, Suzy awakens to discover that she has been moved into a dormitory room against her wishes. The doctors then tell Suzy that she is to be "medicated" with a glass of wine daily. Suzy befriends Sarah after the two are roomed together. As Suzy and the rest of the students prepare for dinner, hundreds of maggots fall from the ceiling. The students are told this was due to spoiled food boxed in the attic. The girls are then invited to sleep in the practice hall overnight. During the night, Sarah identifies a distinctive whistling snore as that of the schools director, who is not due to return to the academy for several weeks.

The next morning, Tanner orders the schools blind piano player, Daniel, to leave the academy immediately after his guide dog bites Albert. Later that night, Sarah overhears the teachers footsteps and begins to count them whilst Suzy becomes irresistibly drowsy and falls asleep. Meanwhile, while Daniel and his guide dog cross a plaza within the city, Daniel senses a strange presence. Suddenly, his seemingly calm dog lunges at Daniel and tears his throat out, killing him. The next day, Suzy recalls the words "Iris (plant)|iris" and "secret" from Pats mumbling before leaving the academy. Later that night, Suzy and Sarah go for a swim while Sarah reveals to Suzy that she and Pat were close friends, and recalls that Pat had been "talking strangely for some time". The two girls search for Pats notes in Sarahs room, but they appear to have been stolen. Suzy suddenly becomes drowsy and falls asleep before Sarah flees to the attic after hearing approaching footsteps. Sarah is chased by an unseen pursuer and, thinking she will be able to escape through a window into another room, falls into a huge pile of razor wire. Becoming entangled, she struggles in anguish until a mysterious black-gloved hand of a dark figure appears and slits her throat.
 Greek émigré who was widely believed to be a witch. Dr. Mandels colleague, Professor Millus, then tells Suzy that a coven can only survive with their queen. Upon Suzys return to the academy that night, she discovers that all of the students have gone to the theater. She overhears the footsteps Sarah identified before and follows them to Blancs office. She suddenly recalls Pats mumbling after discovering irises painted all over the walls of Blancs office. After entering a hidden passage, she discovers Blanc, Tanner and the staff forming a ritual where they plot Suzys death. Unseen, Suzy then turns to find Sarahs body nailed to a coffin. Frightened, Suzy then sneaks into another room, where she accidentally awakens a shadowy figure who reveals herself as Helena Markos. Helena then orders Sarahs nearby corpse to rise from the dead to murder Suzy. Suzy then stabs Helena through the throat with one of the rooms decorative knives, which appears to kill her (she fades from view screaming) and Sarahs reanimated corpse. Helenas demise causes the building to set alight. As the academy is slowly destroyed with the coven inside, Suzy manages to escape before the entire building catches on fire.

==Cast==
 
* Jessica Harper as Suzy Bannion
* Stefania Casini as Sarah
* Flavio Bucci as Daniel Miguel Bosè as Mark
* Alida Valli as Miss Tanner
* Joan Bennett as Madame Blanc
* Udo Kier as Dr. Frank Mandel
* Barbara Magnolfi as Olga
* Eva Axén as Pat Hingle
* Rudolf Schündler as Professor Milius
* Susanna Javicoli as Sonia
* Franca Scagnetti as Cook
* Jacopo Mariani as Albert
* Margherita Horowitz as Teacher
* Lela Svasta as Mother Suspiriorum/Helena Markos (uncredited)
* Dario Argento (uncredited) as Narrator
 

==Production== anamorphic lenses. The Wizard Gone with the Wind, is much more vivid in its color rendition than emulsion-based release prints, therefore enhancing the nightmarish quality of the film. It was one of the final feature films to be processed in Technicolor. 
 Fates and three Charites|Graces, there are three Sorrows: "Mater Lacrymarum, Our Lady of Tears", "Mater Suspiriorum, Our Lady of Sighs" and "Mater Tenebrarum, Our Lady of Darkness".

Scriptwriter Daria Nicolodi stated that Suspiria  s inspiration came from a tale her grandmother told her as a young child about a real life experience she had in an acting academy where she discovered "the teachers were teaching arts, but also black magic."  This story was later confirmed by Argento to have been made up. 

In the Suspiria: 25th Anniversary documentary, Harper commented on the fact that while making Suspiria, as was common practice in Italian filmmaking at the time, the actors dialogue was not properly recorded, but was later dubbed through Dubbing (filmmaking)|ADR, or additional dialogue recording. She also recalled that part of the reason for this was because each actor spoke their native language (for instance, Harper, Valli, and Bennett spoke English language|English; Casini, Valli, and Bucci spoke Italian; and several others spoke German language|German), and as each actor generally knew what the other was saying anyway, they each responded with their lines as if they had understood the other. Argento also expressed disappointment over the fact that Harpers voice, which he liked, was not heard in the Italian market as she was dubbed in Italian by another actress.

==Soundtrack==
  Goblin composed musical score in collaboration with Argento himself. Goblin had previously scored Argentos earlier film Deep Red as well as several subsequent films following Suspiria. In the films opening credits, they are referred to as "The Goblins".
 martial arts film Dance of the Drunk Mantis (1979) and Tsui Harks horror-comedy We Are Going to Eat You (1980).
 heavy metal band Daemonia. The 2001 Anchor Bay DVD release contains a video of the band playing a reworking of the Suspiria theme song. The DVD edition also contains the entire original soundtrack as a bonus CD, which is currently out of print in North America.

The main title theme was named as one of the best songs released between 1977-79 in the book  , compiled by influential music website Pitchfork Media|Pitchfork. It has been sampled on the Raekwon and Ghostface Killah song "Legal Coke", from the R. A. G. U. mix tape. It was also sampled by RJD2 for the song "Weather People Off Cages Album Weather Proof" and by Army of the Pharaohs in their song "Swords Drawn".  The soundtrack from the film has also been sampled in the cult television series Invader Zim. 

Goblin has played the soundtrack live in four locations to standing ovations - as a World Premiere at the Nov 2012 Melbourne Music Week at the Australian Centre for the Moving Image, in July 2013 at the Revelation Perth International Film Festival, on 19 July 2013 at Civic Theatre Auckland New Zealand in the Live Cinema Section of New Zealand Film Festival and on 18 October 2014 at a live screening of the film at Vooruit in Gent, Belgium in the Flanders International Film Festival Ghent.

A remake of the Suspiria soundtrack was released on House of Usher Records  under exclusive license with Bixio Music Group, Ltd. on October 1, 2014 and features remixes by Waning Moon. 

==Critical reception==
The film has received critical acclaim from contemporary critics. According to film review aggregator site Rotten Tomatoes, the film holds a 95% "fresh" rating based on thirty-one reviews with the consensus, "The blood pours freely in Argentos classic Suspiria, a giallo horror as grandiose and glossy as it is gory".  Rotten Tomatoes also ranked it number 41 on their 2010 list of the greatest horror films.  Whilst some critics praise the films visual performance, use of color and soundtrack, others have criticized it for its lack of sense and puzzling storyline.

Janet Maslin of The New York Times wrote a mixed review, saying the film had "slender charms, though they will most assuredly be lost on viewers who are squeamish."  Dave Kehr of the Chicago Reader gave a positive review, claiming that "Argento works so hard for his effects—throwing around shock cuts, colored lights and peculiar camera angles—that it would be impolite not to be a little frightened".  Although J. Hoberman of The Village Voice gave a positive review as well, he calls it "a movie that makes sense only to the eye". 
 AllMovie called it "one of the most striking assaults on the senses ever to be committed to celluloid   this unrelenting tale of the supernatural was—and likely still is—the closest a filmmaker has come to capturing a nightmare on film."  Entertainment Weekly ranked Suspiria #18 on their list of the 25 scariest films ever.  A poll of critics of Total Film ranked it #3 on their list of the 50 greatest horror films ever.  One of the films sequences was ranked at #24 on Bravo (US TV channel)|Bravos The 100 Scariest Movie Moments program.  IGN ranked it #20 on their list of the 25 best horror films. 

==In popular culture== Norwegian thrash an album an album by Darkwave band Miranda Sex Garden and Suspiria de Profundis by Die Form, which can also be regarded as inspired by Thomas De Quinceys work of the same title.

In the 2007 movie Juno (film)|Juno, Suspiria is considered by the title character to be the goriest film ever made, until she is shown The Wizard of Gore and changes her mind, saying it is actually gorier than Suspiria.
 Atmosphere on "Bird Sings Why the Caged I Know".
 Back in Blood.
 Satanists gather, a possible reference to Suspiria.

The American death metal band Infester included a sample from the film in their song, "Chamber of Reunion", from their 1994 album, To The Depths, In Degradation.

A section of the soundtrack cue "Markos" was incorporated into the noted Australian radiophonic work  , conceived and written by radio presenter and author Russell Guy, co-narrated by Guy and former ABC TV newsreader James Dibble, and co-produced by Guy and Graham Wyatt. It was originally broadcast in 1978 on the ABCs "youth" radio station 2JJ aka Double Jay (the Sydney-based AM-band precursor to the current Triple J network).

==Remakes cancelled== Pineapple Express.  As with many remakes of cult films, the announcement was met with hostility by some,    including Argento himself.    The film was to be produced by Italian production company First Sun.  In August 2008, the Bloody Disgusting website reported that Natalie Portman and Annette Savitchs Handsome Charlie Films were set to produce the remake and that Portman would play the lead role.    The First Sun project was also announced to be produced by Marco Morabito and Luca Guadagnino. 
 The Hunger Games) would play the lead role. 

In late 2012, the planned remake was put on hold. In January 2013, Gordon Green revealed that it may never happen due to  .

In April 2014, Green admitted the Suspiria remake was too expensive to make during the "found footage boom".  It is very likely that it will not be made.   

==Awards==
;Academy of Science Fiction, Fantasy & Horror Films, USA
* 1978 Nominated Saturn Award for Best Supporting Actress – Joan Bennett
* 2002 Nominated Saturn Award for Best DVD Classic Film Release

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 