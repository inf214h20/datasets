Bang Rajan (film)
{{Infobox film
| name           = Bang Rajan
| image          = 
| caption        = 
| director       = Tanit Jitnukul
| producer       = Nonzee Nimibutr Adirek Wattaleela Brian L. Marcar Prasarn Maleenont
| writer         = Tanit Jitnukul Kongkiat Khomsiri Patikarn Phejmunee Buinthin Thuaykaew	
| narrator       =
| starring       = Winai Kraibutr Jaran Ngamdee Bongkoj Khongmalai Carabao
| cinematography = Wichian Ruangwijchayakul
| editing        = Sunij Asavinikul Film Bangkok Magnolia Pictures
| released       =  : August 27, 2004
| runtime        = 127 min.
| country        = Thailand Thai
| budget         = 50 million baht
}} Thai historical Siamese village Burmese invaders in 1767, as remembered in popular Thai culture. Cross-checking the story with the events reported by the Burmese sources indicates that the purported events at Bang Rajan are likely a merger of at least two independent events that took place in the war.

The film was directed and co-written by Thanit Jitnukul. In 2004, the film was "presented" by Oliver Stone in a limited release in US cinemas.

==Plot== Burmese army water buffalo and rides the draft animal into battle.

==Historical Accuracy==
 
The plot follows the Thai version of events. According to Thai tradition, the Burmese northern invasion army led by Gen. Ne Myo Thihapate was held up for five months at Bang Rachan, a small village northwest of Ayutthaya by a group of simple villagers. Wyatt, p. 117  However, not all the points of this traditional Thai story could be true as the entire northern campaign took just over five months (mid-August 1765 to late January 1766), and the northern army was still stuck in Phitsanulok, in north-central Siam, as late as December 1765. Burmese sources do mention "petty chiefs" stalling the northern armys advance but it was early in the campaign along the Wang River in northern Siam (not near Ayutthaya) during the rainy season (August–October 1765). The Burmese general who was actually stationed near Ayutthaya was not Thihapate but rather Maha Nawrahta, whose southern army was waiting for the northern army to show up to attack the Siamese capital. Phayre, pp. 188-189  It appears that the three verified events—petty chiefs resisting Thihapate in the north, Thihapates campaign period of five months, and Maha Nawrahta staking out by Ayutthaya—have merged to create this popular mythology.

==Cast==
* Winai Kraibutr as Nai In
* Bin Binluerit as Nai Thongmen
* Jaran Ngamdee as Nai Chan Nuad Kheo
* Chumphorn Thepphithak as Nai Taen
* Bongkoj Khongmalai as E Sa
* Teerayut Pratchbamroon as Luang Por Dhammachote
* Suntri Maila-or as Nang Tang On

==Production and reception==
Bang Rajan was made on a budget of around 50 million Thai baht|baht, which is about four times the cost of other Thai productions being made at the time. It was a box-office hit in Thailand, earning more than 300 million baht.

The film was screened at several film festivals in 2001, including the Seattle International Film Festival, Toronto International Film Festival, the Vancouver International Film Festival and the Hawaii International Film Festival. At the Asia Pacific Film Festival, it won for best art direction. It was screen at the Fantasia Festival in Montreal in 2003, where it won second prize for Best Asian Film.
 The Battle Golden Doll" Awards. The award was personally handed to Sombat by King Bhumibol Adulyadej.
 water buffalo featured in the film died of old age shortly after the film was released and was feted in a lavish funeral ceremony.

==References==
 

==Bibliography==
*  
*  

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 