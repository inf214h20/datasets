The Lottery Bride
{{Infobox film
| title          = The Lottery Bride
| image          = The lottery bride 1930 poster.jpg
| caption        = Cover of the Kino DVD edition
| director       = Paul L. Stein
| producer       = Joseph M. Schenck Arthur Hammerstein
| writer         = Howard Emmett Rogers Horace Jackson Herbert Stothart (story)
| narrator       = Joe E. Brown
| music          = Rudolf Friml Hugo Riesenfeld
| cinematography =
| editing        = Ray June Karl Freund (uncredited)
| studio         = Joseph M. Schenck Productions Art Cinema Corporation
| distributor    = United Artists
| released       =  
| runtime        = 80 min. (1930 release) 67 min. (1937 release)
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Lottery Bride (1930 in film|1930) is a movie musical starring Jeanette MacDonald, John Garrick, ZaSu Pitts, and Joe E. Brown. The film was produced by Joseph M. Schenck and Arthur Hammerstein, based on the musical by Rudolf Friml, and released by United Artists. William Cameron Menzies is credited with the production design and special effects.

The films final reel was in Technicolor in the original 80-minute release in 1930. However, most existing prints are black-and-white prints of the shorter (67-minute) 1937 re-release. 

==Preservation status== tinted sequences and the final reel in Technicolor.

==References==
 

==See also==
*List of early color feature films

==External links==
* 

 

 
 
 
 