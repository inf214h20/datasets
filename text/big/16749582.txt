A Dream of Passion
 
{{Infobox film
| name           = A Dream of Passion
| image          = ADreamOfPassion1978Poster.jpg
| caption        = French Theatrical Poster
| director       = Jules Dassin
| producer       = Giorgos Arvanitis Minoas Volonakis
| starring       = Melina Mercouri Ellen Burstyn
| music          =
| cinematography = Giorgos Arvanitis
| editing        = Georges Klotz
| distributor    =
| released       =  
| runtime        = 106 minutes
| country        = Greece
| language       = English Greek
| budget         =
}}

A Dream of Passion ( , Transliteration|translit.&nbsp;Kravgi gynaikon) is a 1978 Greek drama film directed by Jules Dassin. The story follows Melina Mercouri as Maya, an actress playing Medea, who seeks out Brenda Collins, portrayed by Ellen Burstyn, a woman who is in jail for murdering her own children to punish her husband for his infidelity. 

==Cast==
* Melina Mercouri as Maya / Medea
* Ellen Burstyn as Brenda Collins

==Awards==
It was nominated for the Golden Globe Award for Best Foreign Language Film. It was also nominated for the Golden Palm at the 1978 Cannes Film Festival.    Ellen Burstyns performance was nominated for the Los Angeles Film Critics Association Award for Best Actress.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 