Ronin (film)
{{Infobox film
| name           = Ronin
| image          = Ronin movie 1998.jpg
| caption        = Theatrical release poster
| director       = John Frankenheimer
| producer       = Frank Mancuso Jr.
| story          = J.D. Zeik
| screenplay     = {{Plainlist|
* J.D. Zeik
* David Mamet  
}}
| starring       = {{Plainlist|
* Robert De Niro
* Jean Reno
* Natascha McElhone
* Stellan Skarsgård
* Sean Bean
* Jonathan Pryce
}}
| music          = Elia Cmiral Robert Fraisse Tony Gibbs
| studio         = {{Plainlist|
* FGM Entertainment
* United Artists
}}
| distributor    = {{Plainlist|
* Metro-Goldwyn-Mayer
* United Artists
}}
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = $55 million
| gross          = $70,692,101
}}
 spy Thriller thriller action film directed by John Frankenheimer and starring Robert De Niro, Jean Reno, Natascha McElhone, Stellan Skarsgård, Sean Bean, and Jonathan Pryce. Written by J.D. Zeik and David Mamet, the film centers on a team of hired operatives trying to steal a mysterious and heavily-guarded case while navigating a maze of shifting loyalties and alliances. The film is noted for its car chases through Nice and Paris. 

==Plot==
At a bistro in the  . As the team prepares, Deirdre meets with her IRA handler, Seamus ORourke (Jonathan Pryce), who reveals that the Russian mob is bidding for the case and the team must intervene. After Spence is exposed as a fraud by Sam and summarily dismissed, the others depart for Nice. Sam and Deirdre develop an attraction to each other during a stakeout. On the day of the sale, Deirdres team ambush the convoy at La Turbie and pursue the survivors back to Nice. After a gun battle at the port, Gregor steals the case and disappears.
 CIA contacts and corner him in the Arles Amphitheatre, where he is meeting two of Mikhis men. Gregor flees but is captured by Seamus, who kills Larry and escapes with Deirdre. Sam gets shot saving Vincents life and is taken to a villa in Les Baux-de-Provence owned by Vincents friend Jean-Pierre (Michael Lonsdale). After removing the bullet and letting Sam recuperate, Vincent asks Jean-Pierre to help them locate Gregor and the Irish.

Back in Paris, Gregor is brutally interrogated into leading Seamus and Deirdre to a post office, where they retrieve the case. Sam and Vincent pursue them in a high-speed chase, which ends when Vincent shoots out Deirdres tires and sends her car over a highway overpass. Gregor flees with the case while bystanders save Deirdre and Seamus from the burning vehicle. Sam and Vincent then decide to track down the Russians and learn from one of Jean-Pierres contacts that they are involved with figure skater Natacha Kirilova (Katarina Witt), who is appearing at Le Zénith arena. 

That night during her performance, Natachas boyfriend Mikhi meets with Gregor, who reveals there is a sniper in the arena who will shoot Natacha if Mikhi betrays him again. Mikhi kills Gregor anyway and leaves with the case, letting the sniper kill Natacha. Sam and Vincent follow the panicked mob out of the arena in time to see Seamus shoot Mikhi and steal back the case. Sam runs ahead and finds Deirdre waiting in the getaway car; he urges her to leave, revealing himself to be an active CIA agent pursuing Seamus, not the case. Seamus shoots his way past the crowd, wounding Vincent, and then heads back to the arena with Sam in pursuit. In the final showdown, Seamus is about to kill Sam when he is fatally shot by Vincent.

Sometime later, in the bistro where they first met, Sam and Vincent talk while a radio broadcast announces that a peace agreement was reached between Sinn Féin and the British government, partly as a result of Seamus death. As Sam looks toward the door expectantly, Vincent reminds him that Deirdre will not be coming back. They part, and Sam drives off with his CIA contact. Vincent pays the bill and leaves.

==Cast==
* Robert De Niro as Sam
* Jean Reno as Vincent
* Natascha McElhone as Deirdre
* Stellan Skarsgård as Gregor
* Sean Bean as Spence
* Skipp Sudduth as Larry
* Michael Lonsdale as Jean-Pierre
* Jan Triska as Dapper Gent
* Jonathan Pryce as Seamus ORourke
* Féodor Atkine as Mikhi
* Katarina Witt as Natacha Kirilova Bernard Bloch as Sergei

==Production==

===Screenplay===
The original screenplay for Ronin was written by J.D. Zeik, a newcomer to the film industry. According to Zeiks attorney, David Mamet was brought in just prior to production to expand De Niros role, and that his contributions were minor. In addition to enlarging De Niros role, Mamet added a female love interest and rewrote several scenes.    According to Frankenheimer, however, Mamets contributions were far more significant: "The credits should read: Story by J.D. Zeik, screenplay by David Mamet. We didnt shoot a line of Zeiks script."  When he learned that he would need to share the screenwriting credits with Zeik, Mamet insisted he be credited under the pseudonym Richard Weisz. 

===Cinematography=== Robert Fraisse The Lover Seven Years in Tibet (1997)—Fraisse impressed Frankenheimer with his work in the police thriller Citizen X (1995), convinced he could handle the more than two thousand individual setups he planned for Ronin.    Fraisse learned that the director had a very specific look and style in mind for the film. "I want a lot of setups," Frankenheimer told the cinematographer, "I want the shots to be very short, and I want to work with very short focal lengths." Fraisse would later recall, "John wanted this movie to appear onscreen almost like reportage, as if we had shot things that were really happening, so we didnt want to be too sophisticated. Instead, we tried to convey an ambiance, an atmosphere. Also, he didnt want too many colors, so we avoided colors in the sets, exteriors and costumes as much as we could." 

Fraisses creative use of film stock and lab processing helped achieve this atmosphere for the director. He suggested a special process using Kodaks Vision 500T 5279. Fraisse recalled, "After rating the stock at 250 ASA, which overexposed it one stop, we then underdeveloped it, reducing the contrast and desaturating the colors. I also knew that we were going to shoot in France during the winter, when it gets dark at 5 oclock. I needed to be able to shoot as late as possible, so I made the decision to use the 500 ASA stock for the whole movie. ... Very often, I was shooting at almost full aperture—T2.3 or 2.5." 

Fraisse used a variety of cameras to facilitate the directors ambitious photographic demands, including Panaflexes for dialogue scenes, Arriflex 435s and 35-IIIs for the car-chase sequences, and the Steadicam. "Aside from the car chases, we used the Steadicam for half the shots in the movie,"  Fraisse remembered. "John uses the Steadicam the way others would use a normal camera. Its faster and more convenient than putting down rails and dollies—as long as you have a really good camera operator. Fortunately, I had a great Steadicam operator in David Crone."  Having worked with Frankenheimer on three previous films, Crone impressed the cinematographer with his strength and ability to keep the camera stable during physically challenging sequences, as well as his "incredible sense of framing". 

===Car chases=== Grand Prix. Although action sequences are often shot by a second unit director, Frankenheimer did all these himself, and sometimes rode along. While he was aware of the many innovations in digital special effects since then, he elected to film all these sequences live, to obtain the maximum level of authenticity. To further this, many of the high-speed shots have the actual actors in the cars. Skipp Sudduth did nearly all of his own driving, while other cars were right hand drive models with stunt drivers driving - crashes were handled by a stuntman. To lend additional authenticity, the sound recordist re-recorded many of the vehicles in the chases to ensure that during the editing, the right sounds were dubbed in for each vehicle.
 Audi S8 BMW M5 E34 and Mercedes-Benz 450SEL 6.9, a rare Mercedes-Benz W116 variant with a high-powered engine, as noted by Frankenheimer in the DVD. Most famously, a 1998 Audi S8 quattro, portrayed as stolen to order and then fitted with a nitrous oxide power-booster, is chosen for its bulk, grip and torque and driven in Paris and Nice by Sudduths character. As a result the car was rated 9th in Car (magazine)|Car magazines Top 40 Coolest Movie Cars.    The Frankenheimer DVD commentary indicates that the cars were towed through the streets of France at high speed, not simulated, by a Mercedes-Benz 500E.

Jean-Claude Lagniez, the car stunt coordinator, supervised approximately 150 stunt drivers for various sequences in Ronin. They drove at speeds up to  , and 80 cars were intentionally wrecked during the course of the production.   

The final scene at the Zénith de Paris had 2,000 extras, according to Frankenheimer.

===Filming locations===
 
Ronin was filmed on location in Paris and southern France.      

* Arles Amphitheatre, Arles, France (where Gregor shoots Sam)
* Les Baux-de-Provence, France (where Vincents friend Jean-Pierre lives)
* Rue des Trois Frères, Montmartre, Paris, France (where Sam meets the others for the first time)
* Café Van Gogh, 11 Place du Forum, Arles, France (where Seamus calls Deirdre) Majestic Barrière, 10 la Croisette, Cannes, France (where Sam and Deirdre pose as tourists)
* Pont Alexandre III, Paris, France (where the arms deal ambush takes place beneath the bridge) Porte des Lilas, Paris Métro, Paris, France (where Deirdre meets Seamus)
* Port du Gros Caillou, Paris, France (where Spence carries out the arms deal)
* Rue Drevet staircase, between Rue André Barsacq and Rue des Trois Frères, Montmartre, Paris, France (the staircase)
* La Turbie, above Monte Carlo (where Sam and Deirdre kiss outside the gangs villa and where Sam and the others ambush the convoy)
* Villefranche-sur-Mer, France, between Nice and Monaco, (where Sam meets his CIA contact)
* Le Zénith, 211 avenue Jean-Jaurès, La Villette Park, Paris, France (where the ice-show scenes were filmed) Vieux Nice, Nice, France (where Sams team chases after the case, ending in Port de Nice)

==Reception==
Ronin received generally positive reviews from critics. The review aggregator website Rotten Tomatoes reported that 68% of critics gave the film a positive review, based on a sample of  60&nbsp;reviews, with an average score of 6.3/10.     Many reviewers, like Janet Maslin of The New York Times, praised the cast and Frankenheimers trademark chase scenes.   Roger Ebert praised the "skill and silliness" of the movie, while noting: "The movie is not really about anything; if it were, it might have really amounted to something, since it comes pretty close anyway." 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  Ronin was listed at 72nd place on this list. 

==Home video==
 
The DVD release has an extensive, detailed commentary about the making of the film by Frankenheimer, where he explains the production techniques used to realize the high speed chases.

The DVDs paper insert includes excerpts from a Frankenheimer interview in which he discusses the chase through a Paris tunnel that is remarkably similar to the site of Princess Dianas death on 31 August 1997. The filming took place in a different tunnel, however.  "Paris has a lot of tunnels," Frankenheimer commented. "That’s part of the thing about the city I wanted people to see. A crash in a tunnel in Paris is about as likely as someone having a crash on a freeway here. It happens all the time." (Rocky Mountain News, September 27, 1998).

The US edition of the original DVD release has several navigational hooks to DVD-ROM content, which were taken advantage of several weeks after the original release of the DVD, on MGMs website during a special RONIN event where viewers would be taken on a guided tour of the making of RONIN. Making of scenes shot during filming are hidden on the DVD. Since they are not present on the main menu of the DVD they can only be accessed on a computer using the DVD-ROM program that is on the disc or using a DVD viewing program that allows navigation through the titles of the disc manually. A "Gold Edition" was briefly introduced on the market by MGM, however is no longer in production.

On October 11, 2004 a two-disc Special Edition of the film was released in the US. This new version contains the same material as the old single-disc version on disc one and on disc two there are supplemental material about the film: one documentary, six featurettes, and a picture gallery.

A Blu-ray Disc edition was made available in 2008, which does not include any of the extras on the DVD versions.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*   Filming Locations at Movieloci.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 