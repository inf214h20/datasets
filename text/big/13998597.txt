The Burning Plain
 
{{Infobox film
| name           = The Burning Plain
| image          = The_burning_plain_film_poster.jpg
| caption        = Theatrical release poster
| director       = Guillermo Arriaga
| producer       = Walter F. Parkes Laurie MacDonald Charlize Theron
| writer         = Guillermo Arriaga
| starring       = Charlize Theron Kim Basinger Jennifer Lawrence Joaquim de Almeida
| music          = Omar Rodríguez-López Hans Zimmer
| cinematography = Robert Elswit
| editing        =  Wild Bunch 2929 Productions
| distributor    = Paramount Pictures (UK)
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $5,468,647 (worldwide)  . Box Office Mojo (2009-11-26). Retrieved on 2014-05-22. 
}}
The Burning Plain is a 2008/2009 drama film directed and written by Guillermo Arriaga, the screenwriter of Amores perros (2000), 21 Grams (2003), and Babel (film)|Babel (2006). The film stars Charlize Theron, Jennifer Lawrence, Kim Basinger and Joaquim de Almeida. In Arriagas directorial debut, he films a story that has multipart story strands woven together as in his previous screenplays. Filming of The Burning Plain began in New Mexico in November 2007, and the film was released in late 2008 in various festivals, before a limited theatrical release in 2009.

==Plot==
Typical of Arriagas works, this film is told in a non-linear narrative, where events are revealed out of sequence. The following plot summary is in chronological order, and thus does not reflect the exact sequence of the events as seen on screen.

The story starts some time during the mid-1990s in a small town near Las Cruces, New Mexico (close to the border with Mexico), where we are introduced to Gina (Kim Basinger), a wife and mother to four children. Gina is having an affair with a local man named Nick Martinez (Joaquim de Almeida), who also has a family of his own; but, unbeknownst to the two, Ginas teenage daughter Mariana (Jennifer Lawrence) finds out about their love affair. Mariana follows her mother to Nicks trailer. Knowing the two are inside; and, in an effort to make them end their affair, she disconnects the gas pipe leading into the trailer and sets it on fire. The flames eventually reach a gas tank, which explodes, consuming the entire trailer and claiming Nicks and Ginas lives, although Mariana had no intention of killing either of them. After their funeral, Mariana and Nicks teenage son, Santiago (JD Pardo), slowly begin to develop a relationship of their own. Mariana soon becomes aware that she is pregnant with Santiagos daughter. The two flee to Mexico amid disapproval from their families and decide to have the baby there; but, after she gives birth to their daughter, Mariana abandons her family and changes her name to Sylvia.

More than a decade later, Sylvia (now played by Charlize Theron) is working at a high-end restaurant in Portland. Despite her success, she resorts to promiscuity and has persistent thoughts of suicide. Here, we see a mysterious man following her around. It is Carlos (Jose Maria Yazpik), a close friend and business-partner of Santiago. After an accident involving their crop-dusting plane, the hospitalized Santiago urges Carlos to look for Sylvia, for whom Santiago has been searching since she abandoned him and their two-day-old daughter.

Because Carlos doesnt speak English and Sylvia doesnt speak Spanish, he has trouble explaining to her the purpose of his visit. Instead, he surprises Sylvia with her now twelve-year-old daughter, Maria (Tessa Ia). Maria, who was already reluctant to meet her estranged mother, is heartbroken when Sylvia hastily departs without speaking to either when she sees Maria and Carlos waiting for her outside her home. After realizing her mistake, Sylvia enlists the help of her friend Laura (Robin Tunney) to find Carlos and Maria.

After reuniting, Sylvia, Maria, and Carlos go to Mexico, where Sylvia apologizes to Maria for the years shes been absent from her life. They visit Santiago, who is sedated due to the extent of his injuries. Sylvia confesses her past sins by his bedside, unsure if he will ever wake up again. The doctor reassures them that he will be fine, and the story concludes on a hopeful note.

==Cast==
* Charlize Theron as Sylvia / Mariana
* Kim Basinger as Gina
* Jennifer Lawrence as Mariana
* Joaquim de Almeida as Nick Martinez John Corbett as John
* Robin Tunney as Laura
* Brett Cullen as Robert
* Danny Pino as Santiago Martinez
* José María Yazpik as Carlos
* J.D. Pardo as Young Santiago
* Tessa Ia as Maria Martinez
* Rachel Ticotin as Ana
* Diego J. Torres as Cristobal Martinez
* Rafael Hernández as Doctor Armendariz
* Lisa Kasper as dinner guest

==Production== Portland and Depoe Bay in Oregon.

==Release==
The Burning Plain was screened at TIFF, the Toronto International Film Festival, in September 2008, before being screened at the Savannah Film Festival, which was held from October 25 to November 1, 2008. It was an entrant of the international competition of the 65th Venice International Film Festival. The Burning Plain was released in theaters on September 18, 2009. 

==Reception==

===Critical reception=== Time Out, "Arriaga has delivered a compelling and entertaining debut that stays true to his earlier interests." 

=== Box office ===
The film grossed $58,749 in its first weekend in North America.  It grossed $200,730 domestically and $5,267,917 in foreign countries, for a total of $5,468,647 worldwide. 

==See also== The Burning Plain, Mexican literary classic of the same name

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 