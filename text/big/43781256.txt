Naan Sollum Ragasiyam
{{Infobox film
| name           = Naan Sollum Ragasiyam நான் சொல்லும் ரகசியம்
| image          =
| image_size     =
| caption        =
| director       = P. Sridhar Rao
| producer       = V. C. Subburaman
| writer         = Kalaipithan
| story          = Kalaipithan
| screenplay     = 
| starring       = Sivaji Ganesan Anjali Devi K. A. Thangavelu J. P. Chandrababu
| music          = G. Ramanathan
| cinematography = T. V. Balasundaram
| editing        = P. V. Manickam
| studio         = Kasturi Films
| distributor    = Kasturi Films
| released       =  
| country        = India Tamil
}}
 1959 Cinema Indian Tamil Tamil film,  directed by P. Sridhar Rao and produced by V. C. Subburaman. The film stars Sivaji Ganesan, Anjali Devi, K. A. Thangavelu and J. P. Chandrababu in lead roles. The film had musical score by G. Ramanathan.   

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Sivaji Ganesan || Karunakaran
|-
| Anjali Devi || Manorama  
|-
| K. A. Thangavelu || Vadivelu
|-
| J. P. Chandrababu || Doctor Shailak
|-
| M. N. Rajam || Kala 
|-
| S. V. Subbaiah || Velayutham 
|-
| G. Sakunthala || Rosi
|-
| C. K. Saraswathi || Akilandam
|- Manorama || Kamatchi 
|-
| S. R. Dhasarathan || Ravi 
|-
| K. M Nambirajan || Govindan
|-
| S. Rama Rao || Patient 
|-
| Nalli Subbiah || Kuppusami 
|-
| Dharmarajan || Drinker  
|-
| G. Mani || Somu  
|-
| N. Rukmani || Kuppusamis wife
|}

==Crew==
*Producer: V. C. Subburaman
*Production Company: Kasturi Films
*Director: P. Sridhar Rao
*Music: G. Ramanathan
*Lyrics: A. Maruthakasi
*Story: Kalaipithan
*Dialogues: Kalaipithan
*Art Direction: C. Ramaraju
*Editing: P. V. Manickam
*Choreography: K. N. Dhandayuthapani, B. Sohanlal, Sambath & Chinnilal
*Cinematography: T. V. Balasundaram
*Stunt: None
*Audiography: C. P. Kanniyappan Helen

==Soundtrack== Playback singers are T. M. Soundararajan, Seerkazhi Govindarajan, P. B. Sreenivas, P. Leela, P. Suseela & A. P. Komala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaanadha Sorgam Ondru || Seerkazhi Govindarajan & P. Leela || rowspan=8|A. Maruthakasi || 03:15
|-
| 2 || Naan Sollum Ragasiyam || T. M. Soundararajan || 01:52
|-
| 3 || Kandene Unnai Kannaale || P. B. Sreenivas & P. Suseela || 03:08
|-
| 4 || Parkkaadha Pudhumaigal || P. Leela || 04:54
|-
| 5 || Pollatha Ulagathile || T. M. Soundararajan || 03:11
|-
| 6 || Vilaiyadu Raja Vilaiyadu || J. P. Chandrababu & A. P. Komala || 03:24
|-
| 7 || Thaaye Idhu Nyaayamaa || P. Suseela || 01:33
|-
| 8 || Kannai Pol Thannai Kaakkum || P. Suseela || 03:04
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 