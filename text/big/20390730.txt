Lorelei: The Witch of the Pacific Ocean
{{Infobox Film director        =Shinji Higuchi producer        =Chihiro Kameyama writer          =Harutoshi Fukui Satoshi Suzuki music           =Naoki Sato cinematography  =Akira Sato  distributor     =Toho Fuji TV released        =March 5, 2005 runtime         =128 min. language  Japanese
}}

  is a 2005 Japanese film directed by Shinji Higuchi. A fictional story of the Japanese military saving Tokyo from the third atomic bomb during the waning days of the Second World War, it was the highest-grossing film in Japan during the week of its release. 

The story of "Lorelei", based on a best-selling novel written by Harutoshi Fukui, is a departure from the last 50 years of Japanese cinema by weaving a tale using a "what if" fictional narrative with a tip of the hat to modern manga storylines and styles.

==Plot==
In the last months of the Second World War, the Empire of Japan receives a final gift from the collapsing Nazi Germany: the I-507, a highly advanced submarine equipped with experimental technology. 
 Tinian Island, striking the Japanese home Islands. The man charged with the mission is Commander Masami (Yakusho Koji) - a brilliant destroyer of enemy ships relieved of his command when he opposed the Imperial Japanese Navy|Navys increasing reliance on suicide tactics. Given a last chance to redeem himself, he is burning with zeal, but is ignorant of the various secrets the I-507 carries on board.

Once at sea, Lt. Takasu (Ken Ishiguro), the owlish technician in charge of the imaging system, refuses to tell Masami what it is or how it works. Masami also discovers that two crew members belong to the "kaiten" suicide corps. He has no idea why they are there, and neither, for the moment, do they.

Meanwhile, the U.S. Navy is tracking the I-507 with more than usual interest. A teenaged girl (Yu Kashi) is part of the master plan and one of the minisub pilots (Satoshi Tsumabuki) becomes her protector.

==Cast==
Starring
* Koji Yakusho	 - 	Masami Shinichi
* Satoshi Tsumabuki	- 	Yukito Origasa
Supporting Cast
* Toshiro Yanagiba	- 	Kizaki Toshiro
* Yu Kashii	- 	Paula Atsuko Ebner
* Shinichi Tsutsumi	- 	Asakura Ryokitsu
* Ken Ishiguro	- 	Narumi Takasu
* Isao Hashizume	- 	Sadamoto Nishimiya
* Masato Ibu	- 	Eitaro Narazaki
* Takaya Kamikawa	- 	Man Kreva	- 	Shunpei Komatsu
* Jun Kunimura	- 	Matoi Tokioka
* Takehiko Ono	- 	Shichigoro Iwamura
* Shugo Oshinari		
* Ryuta Sato	- 	Kikuo Kiyonaga
* Pierre Taki	- 	Tokutaro Taguchi
* Yoshiyuki Tomino	- 	Ouwada
* Shingo Tsurumi	- 	Sankichi Oominato

==References==
 

==Sources==
*   
*  
*  

 

 
 
 
 
 
 
 
 
 