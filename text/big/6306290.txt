The Favorite
{{Infobox film
| name      = The Favorite (Intimate Power)
| image     = The Favorite.jpg
| caption   = 
| director  = Jack Smight
| producer  = Georges-Alain Vuille Ascona Films Inc.
| writer    = Prince Michael of Greece (novel) Larry Yust (screenplay)
| starring  = F. Murray Abraham Maud Adams Francesco Quinn Amber OShea
| music     = William Goldstein
| cinematography = Giorgio Tonti Howard Wexler
| editing   = Dennis Virkler Devon Heffley Curry, adr supervisor
| art       = R. Clifford Searcy
| costumes  = Tami Mor Rina Ramon
| stunts    = Kenny Bates
| distributor = 20th Century Fox
| released  = 9 August 1989 (France)
| runtime   = 104 min.
| country   = United States Switzerland English
| budget    = 
| gross     = 
| preceded_by = 
| followed_by = 
}} La Favorita. For the Canadian early music ensemble, see La Favoritte. For the Brazilian soap opera, see A Favorita.

The Favorite (also titled Intimate Power) is a 1989 film based on the life story of Aimée du Buc de Rivéry (1776-1817) that takes place at the dawn of the 19th century. It is the final film of director Jack Smight.

==Premise==
A young French woman named Aimee is kidnapped and forced into a sultans harem in Turkey. Fiercely independent, she resists, but must make choices in order to survive. She begins to influence the sultan toward more fair manners of solving his conflicts, but finds herself at odds with another of his wives, who wants her son Mustafa to become the new sultan. As the years pass, she must deal with the new sultans advances while protecting her adopted son Mahmud, and helping the Ottoman Empire against the Russians who have better weapons than they do.

The source for the story is a novel by Prince Michael of Greece titled Sultana - La Nuit du Serail.

==Cast== Abdul Hamid Sineperver
*Amber OShea as Aimée Dubucq de Rivéry
*Ron Dortch as Tulip Selim
*Laurent Sebastiani
*Francesco Mahmud
*Andréa Parisy as Mirishah
*Tom McGreevey as Uncle (as Thomas McGreevey)
*Celeste Simpson-Boyd as Zinah
*Robere Kazadi as Orchid
*Garth Wilton as British Consul
*Reuven Bar-Yotam as Algerian Captain
*Farouk Peker as Baktar
*John Kennedy Hayden as Chief Janissary Mike Johnson as First Mate
*Thomas Rosales Jr. as Third Mate
*Michael Saad as Jeweler
*Dale Dye as French Officer
*Joseph Darrell as Manservant
*Ayse Gungor as Harem Girl
*Starr Andreeff as Harem Girl
*Erica Zeitlin as Harem Girl
*Victoria Dakil as Old Woman
*Roz Witt as Nun
*George Marshall Ruge as Kamir
* Joe El Rady as Boy at the bazar
*Jonathan Vuille as Young Mahmud Mustafa

==Production==
*Director: Jack Smight
*Production Company: Ascona Films, Inc.
*Filming Locations: Istanbul, Turkey

==Sources==
*Jason Ankeny, Allmovie.
* 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 