Blackbeard's Ghost
 
{{Infobox film
| name           = Blackbeards Ghost
| image          = Blackbeards-Ghost-Poster.jpg
| image_size     =
| caption        = Blackbeards Ghost theatrical poster Robert Stevenson Ben Stahl Bill Walsh Dean Jones Suzanne Pleshette
| music          = Robert F. Brunner Edward Colman
| editing        = Robert Stafford Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 106 mins.
| country        = United States
| language       = English
| gross          = $21,540,050 
}}
 fantasy Comedy comedy Walt Disney film Dean Jones, Robert Stevenson. Ben Stahl Walt Disney Studios. The Disney Channel aired this film until the late 1990s.

==Plot== Dean Jones) pirate Blackbeard|Captain Edward Teach and now run by the Daughters of the Buccaneers, elderly descendants of the pirates crew. The owners are attempting to pay off their mortgage to keep the inn from being bought by the local crime boss, Silky Seymour (Joby Baker), who wants to build a casino on the land. Steve quickly discovers his track teams shortcomings and runs afoul of the dean of Godolphin College, its football coach, and Seymour. He also makes the acquaintance of attractive Godolphin professor Jo Anne Baker (Suzanne Pleshette), who is anxious to help the elderly ladies save Blackbeards Inn.

After a bidding war with the football coach at the charity auction, Steve wins an antique bed warmer once owned by Blackbeards 10th wife, Aldetha Teach.  Aldetha had a reputation of being a witch. Inside the hollow wooden handle of this bed warmer is hidden a book of magic spells that had once been the property of Aldetha. Steve recites, on a lark, a spell "to bring to your eyes and ears one who is bound in Limbo", unintentionally conjuring up the ghost of Blackbeard (Peter Ustinov), who appears as a socially-inappropriate drunkard.  His wife cursed him to an existence in limbo unless he can perform a good deed.

Steve and Blackbeard are bound to one another by the power of the spell, and only the very reluctant Steve can see or hear the ghost. As a result, Steve must deal with the antics of the wayward pirate while attempting to revive Godolphins track team and form a relationship with Jo Anne. Steve is falsely arrested for drunk driving when Blackbeard attempts to drive Steves automobile, steering it like a pirate ship. Because the arresting officer cant see Blackbeard (and because Blackbeards erratic driving caused the cops motorcycle to crash into a tree), Steve spends a night in jail. While in jail, Steve reminds Blackbeard that if he does a good deed, his curse will be broken.  Steve asks Blackbeard for his treasure to help the Daughters of the Buccaneers save the inn, but Blackbeard admits that he spent all of the money. Steve decides not to trust Blackbeard.

Steve is released from jail the next morning due to lack of evidence. Blackbeard creates further complications by stealing one of the Inns mortgage payments and betting it on Steves track team. Blackbeards intention is to use his ghostly powers to help Godolphin win the track meet, and then use the winnings to pay the mortgage in full. Steve is at first outraged by the pirates interference, but he decides the greater good is to win the money for the sake of the Inn. He also accepts the pirates help in shaking down Silky Seymour and his thugs after Seymour refuses to pay out the winnings from the bet.

With the mortgage paid, Blackbeard has performed his good deed and is released from the curse. After Steve asks the ladies and Jo Anne to recite the spell (Kree kruh vergo gebba kalto kree), thereby rendering Blackbeard visible to them, Blackbeard bids them all a cordial goodbye and departs to join his former crew, leaving Steve and Jo Anne to pursue their future together.

==Differences from the Ben Stahl novel==
In the original novel, instead of a track coach, two teenage boys find the book that brings the ghost of Blackbeard back to Godolphin. In addition, Peter Ustinov portrays the pirate Blackbeard as a charming individual in contrast with Stahls version of the character.

==Cast==
{{columns-list|2| Captain Blackbeard Dean Jones as Steve Walker
*Suzanne Pleshette as Jo Anne Baker
*Elsa Lanchester as Emily Stowecroft
*Joby Baker as Silky Seymour
*Elliott Reid as TV commentator Richard Deacon as Principal Roland Wheaton
*Norman Grabowski as Virgil
*Kelly Thordsen as Sheriff Ned Lamb
*Michael Conrad as Pinetop Purvis
*Herbie Faye as Croupier George Murdock as Head official
*Hank Jones as Gudger Larkin
*Ned Glass as Teller
*Gil Lamb as Waiter
*Alan Carney as Bartender
*Ted Markland as Charles
*Lou Nova as Leon
*Charlie Brill as Edward
*Herb Vigran as Danny Oly William Fawcett as Mr. Ainsworth, Bank Official 
*Betty Bronson as Old Lady 
*Elsie Baker as Old Lady
*Kathryn Minner as Old Lady
*Sara Taft as Old Lady
}}

==Reception==
Blackbeards Ghost received positive reviews from critics and audiences, earning a Rotten Tomatoes approval rating of 80%.

==See also==
* List of ghost films
*Chamatkar, a 1992 uncredited Bollywood remake of Blackbeards Ghost.

==References==
 
==External links==
*  
*  at Ultimate Disney|UltimateDisney.com
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 