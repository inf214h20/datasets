My Wife's Enemy
{{Infobox film
| name           = My Wifes Enemy
| image          = My Wifes Enemy.jpg
| caption        = 
| director       = Gianni Puccini 
| producer       = Isidoro Broggi Renato Libassi
| writer         = Bruno Baratti Franco Castellano Gino Mangini Giuseppe Moccia Gianni Puccini
| starring       = Marcello Mastroianni
| music          = Lelio Luttazzi
| cinematography = Gianni Di Venanzo
| editing        = Gisa Radicchi Levi
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

My Wifes Enemy ( ) is a 1959 Italian comedy film directed by Gianni Puccini.   

==Cast==
* Marcello Mastroianni - Marco Tornabuoni
* Giovanna Ralli - Luciana, sua moglie
* Vittorio De Sica - Ottavio Terenzi, padre di Marco
* Memmo Carotenuto - Nando Terenzi, padre di Lucia
* Luciana Paluzzi - Giulia
* Andrea Checchi - Dr. Giuliani
* Teddy Reno - Himself
* Giacomo Furia - Peppino Riccardo Garrone - Michele
* Raimondo Vianello - Mister La Corata
* Enzo Garinei - Scienziato tedesco
* Gisella Sofio - Worker
* Armida De Pasquali
* Mario Donati
* Salvo Libassi

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 