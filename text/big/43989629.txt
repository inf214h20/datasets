Interview (1973 film)
{{Infobox film 
| name           = Interview
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = Thiruppathi Chettiyar
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Jayabharathi Kaviyoor Ponnamma Adoor Bhasi
| music          = V. Dakshinamoorthy
| cinematography = CJ Mohan
| editing        = K Sankunni
| studio         = Evershine
| distributor    = Evershine
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by Thiruppathi Chettiyar. The film stars Prem Nazir, Jayabharathi, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Prem Nazir as Vijayan
*Jayabharathi as Susheela 
*Kaviyoor Ponnamma as Saraswathi
*Adoor Bhasi as Velu Pilla
*Sankaradi as Govinda Pilla
*Sreelatha Namboothiri as Ambika
*Bahadoor as Kuttappan Sujatha as Sreedevi
*T. R. Omana as Shankari
*T. S. Muthaiah as Keshava Pilla
*Muthukulam Raghava Pilla as Sekhara Pilla
*Pala Thankam as Susheelas mother
*Thodupuzha Radhakrishnan as Balakrishnan
*P. R. Menon as Panchayath President
*Mookkannoor Sebastian
*Hema
*Sathi
*Jose 
*Hari
*Latha Raju
*C. I. Balan

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ammaykkum Achanum || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Kanakam Moolam Dukham || KP Brahmanandan || Vayalar Ramavarma || 
|-
| 3 || Maala Maala Varanamaala || LR Eeswari || Vayalar Ramavarma || 
|-
| 4 || Naleekalochane || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Uthara Madhuraapuriyil || K. J. Yesudas, P Susheela, Chorus || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 