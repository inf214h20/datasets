Tear Gas Squad
{{Infobox film
| name           = Tear Gas Squad
| image          = "Tear_Gas_Squad"_(1940).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Terry O. Morse
| producer       = Bryan Foy
| writer         = Charles Belden Don Ryan Kenneth Gamet
| based on       = 
| screenplay     = 
| narrator       =  John Payne Gloria Dickson
| music          = Jack Scholl M.K. Jerome
| cinematography = Sidney Hickox
| editing        = Louis Hesse Ernest J. Nims
| studio         = Warner Bros.
| distributor    =  Warner Bros.
| released       =  
| runtime        = 55 min.
| country        =   English
| budget         = 
| gross          = 
}}
 1940 Drama (genre)| Drama directed by Terry O. Morse, starring Dennis Morgan, John Payne and Gloria Dickson.  The film was made under the working title of State Cop.  It includes the song  Im an Officer of the Law (M.K. Jerome, Jack Scholl). 

==Plot summary==
  John Payne). Falling in love with Jerry himself, Tommy joins the police force, where he is subject to the rigorous training program applied by Morrissey. Eventually suspended from the police because of his carelessness, Tommy ends up saving the day by saving Morriseys life. Tommy is finally reinstated in the force, thus rewarding Jerrys faith in him.    

==Cast==
* Dennis Morgan as Tommy McCabe John Payne as Bill Morrissey
* Gloria Dickson as Jerry Sullivan
* George Reeves as Joe McCabe
* Frank Wilcox as Sgt. Crump
* Herbert Anderson as Pliny Jones Julie Stevens as Lois
* Harry Shannon as Lt. Sullivan Mary Gordon as Mrs. Sullivan William Gould as Capt. Henderson John Hamilton as Chief Ferris
* Edgar Buchanan as Cousin Andy
* Dick Rich as Cousin Pat
* William Hopper as George (as DeWolf Hopper)

==Critical reception==
In The New York Times, Bosley Crowther wrote, "Tear Gas Squad, now at the Palace, is not the sort of propaganda to inspire confidence in the guardians of the public peace, nor, for that matter, in its highly improbable plot...if the film is dubious as a tribute to the police force, it is even more so as entertainment" ; {{cite web|url=http://www.nytimes.com/movie/review?res=9E05E6D91531E23ABC4950DFB366838B659EDE|title=Movie Review -
  My Favorite Wife - THE SCREEN; My Favorite Wife a Lively Farce, With Cary Grant and Irene Dunne, at the Music Hall--2 Other Films - NYTimes.com|publisher=}}  while Allmovie wrote, "Tear Gas Squad manages to pack thrills, comedy, romance and songs into a neat 55-minute package."  

==References==
 	

== External links ==
*  
*  
*  
*  

Terry Morse

 
 
 
 
 
 
 
 

 