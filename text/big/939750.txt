Pushing Hands (film)
{{Infobox film
| name           = Pushing Hands
| image          = Pushing hand.JPG
| image_size     =
| caption        = Movie poster
| director       = Ang Lee
| producer       = Hsu Li Kong executive producer  Liu Yiming Ted Hope James Schamus Ang Lee
| writer         = Ang Lee  James Schamus
| narrator       =
| starring       = Sihung Lung  Deb Snyder  Bo Z. Wang
| music          =
| cinematography = Lin Jong
| editing        = Ang Lee Tim Squyres
| distributor    =
| released       = 1992
| runtime        = 105 min
| country        = Republic of China|Taiwan, Republic of China English
| budget         =
}}
Pushing Hands ( ) is a film directed by Ang Lee. Released in 1992, it was his first feature film. Together with Ang Lees two following films, The Wedding Banquet (1993) and Eat Drink Man Woman (1994), it forms his "Father Knows Best" trilogy, each of which deals with conflicts between an older and more traditional generation and their children as they confront a  world of change.

The film was first released in Taiwan. After The Wedding Banquet and Eat Drink Man Woman became successful in the United States, Pushing Hands received a U.S. release. 

==Plot==
The story is about an elderly Chinese tai chi chuan teacher and grandfather who emigrates from Beijing to live with his son, American daughter-in-law, and grandson in a New York City suburb. The grandfather is increasingly distanced from the family as a "fish out of water" in Western culture. The film shows the contrast between traditional Chinese ideas of Confucian relationships within a family and the much more informal Western emphasis on the individual. The friction in the family caused by these differing expectations eventually leads to the grandfather moving out of the family home (something very alien to traditional expectations), and in the process he learns lessons (some comical, some poignant) about how he must adapt to his new surroundings before he comes to terms with his new life.

==Title==
The title of the film refers to the pushing hands training that is part of the grandfathers tai chi routine. Pushing hands is a two person training which teaches tai chi students to yield in the face of brute force. Tai chi chuan teachers were persecuted in China during the Cultural Revolution, and the grandfathers family was broken up as a result. He sent his son to the West several years earlier and when he could he came to live with his family with the expectation of picking up where they left off, but he was unprepared for the very different atmosphere of the West. "Pushing Hands" thereby alludes to the process of adaptation to culture shock felt by a traditional teacher in moving to the United States.

==Filming style==
Donald Lyons wrote that in the film the director Ang Lee "a mastery of the visual dynamics of interior spaces and their psychic pressures." Dariotis and Fung, p.  . 

==Characters==
* Alex Chu (T: 朱曉生, S: 朱晓生, P: Zhū Xiǎoshēng) -  , P: Wáng Bózhāo)
** Alex acts as an interpreter between Mr. Chu and Martha, but he deliberately mis-translates to reduce tensions between the two parties. Dariotis and Fung, p.  - .  When his father runs away from home, he trashes his house kitchen. Dariotis and Fung, p.  . 
* Mr. Chu (C: 朱老, P: Zhū-lǎo) - Sihung Lung
** Mr. Chu is a Tai chi chuan master who travels to the United States.  During the Cultural Revolution a group attacked his family instead of him because he was a Tai chi chuan master and they feared challenging him. Mr. Chu protected Alex but his wife was fatally injured. Dariotis and Fung, p.  . 
* Martha Chu (T: 瑪莎, S: 玛莎, P: Mǎshā) - Deb Snyder
** Martha is Alexs European American wife. Martha, who does not speak or understand Chinese, has conflicts with Mr. Chu being in the household since she cannot participate within her own family when he speaks Chinese.  Martha asks Alex to borrow money from her mother so he could buy a larger house that could accommodate his Chinese aspects separately. Alex refuses to do so, frustrating Martha. 
* Jeremy Chu (C: 吉米, P: Jímǐ) - Haan Lee (C: 李 涵, P: Lǐ Hán)
* Linda (T: 琳達, C: 琳达, P: Líndá) - Fanny De Luz
* Mrs. Chen  (T: 陳太太, S: 陈太太, P: Chén-tàitai) -  , S: 王 莱, P: Wáng Lái)
**Mrs. Chen is a widow who is a cooking instructor at the area Chinese Community Center. Mr. Chu befriends her and goes on a picnic with her family and Alex, but without Martha. Wei Ming Dariotis and Eileen Fung, authors of "Breaking the Soy Sauce Jar: Diaspora and Displacement in the Films of Ang Lee", wrote that it seems like, with the absence of Martha, there is no tension.  The groups go hiking, but the elderly lag behind. Mrs. Chen then has a breakdown and admits to the elder Mr. Chu that her daughter does not want her to be around anymore. 

==References==
 

==External links==
 
* 

==Further reading==
* Dariotis, Wei Ming and Eileen Fung. "Breaking the Soy Sauce Jar: Diaspora and Displacement in the Films of Ang Lee." in: Lu, Sheldon Hsiao-peng (Xiaopeng) (editor). Transnational Chinese Cinemas: Identity, Nationhood, Gender. University of Hawaii Press, January 1, 1997. ISBN 0824818458, 9780824818456.

 

 
 
 
 
 
 
 