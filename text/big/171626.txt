Ali (film)
{{Infobox film name           =Ali image          =Ali_movie_poster.jpg image_size     = caption        =Theatrical release poster director  Michael Mann producer       =Michael Mann Jon Peters James Lassiter Paul Ardaji A. Kitman Ho screenplay      =Michael Mann Eric Roth Stephen J. Rivele Christopher Wilkinson story          =Gregory Allen Howard Jeffrey Wright Mykelti Williamson  music          =Pieter Bourke Lisa Gerrard cinematography =Emmanuel Lubezki editing        =William Goldenberg Lynzee Klingman Stephen E. Rivkin Stuart Waks distributor    =Columbia Pictures studio  Overbrook Films released       =  runtime        =157 minutes country        =United States language       =English budget         =$107 million  gross          =$87,713,825
}} Michael Mann.
 the boxer Muhammad Ali, played by Will Smith, from 1964-74 featuring his capture of the heavyweight title from Sonny Liston, his conversion to Islam, criticism of the Vietnam War, banishment from boxing, his return to fight Joe Frazier in 1971, and, lastly, his reclaiming the title from George Foreman  in the Rumble in the Jungle fight of 1974. It also discusses the great social and political upheaval in the United States following the assassinations of Malcolm X and Martin Luther King, Jr. during the presidency of Lyndon B. Johnson.

==Plot==
The film begins with Cassius Clay, Jr. (Will Smith) before his championship debut against then heavyweight champion Sonny Liston. In the pre-fight weigh-in Clay heavily taunts Liston (such as calling Liston a "big ugly bear"). In the fight Clay is able to dominate the early rounds of the match, but halfway through the fight Clay complains of a burning feeling in his eyes (implying that Liston has tried to cheat) and says he is unable to continue. However, his trainer/manager Angelo Dundee (Ron Silver) gets him to keep fighting. Once Clay is able to see again he easily dominates the fight and right before round seven Liston quits, therefore making Cassius Clay the second youngest heavyweight champion at the time after Floyd Patterson. Ali spends valued time with Malcolm X (Peebles) and the two decide to take a trip to Africa.

Clay is then invited to the home of Nation of Islam leader Elijah Muhammad where he is granted the name Muhammad Ali due to his status of World Heavyweight Champion. While at home with his wife and children, Malcolm X is called by the Nation of Islam and has been informed Ali will not go to Africa and his suspension (Malcolms) has been extended. Muhammad Ali takes the trip to Africa where he finds Malcolm X, but later refuses to speak to him, honoring the wishes of Elijah Muhammad. Upon returning to America and defeating Sonny Liston a second time, Ali continues to dominate as champion, until he is stripped of the title, boxing license, passport suspended and sent to jail for his refusal to be drafted during the Vietnam War. After a three-year hiatus, his conviction is later overturned, and attempts to regain the Heavyweight Championship against Joe Frazier. Dubbed the Fight of the Century, Frazier wins, giving Ali the first loss of his career. When Frazier loses the championship to George Foreman, Ali makes a decision to fight George Foreman to be the first boxer to win his title a second time. Ali goes to Kinshasa, Zaire to face Foreman for the title. While there, Ali has an affair with a woman he meets named Veronica Porsche (who he is said to later marry in the epilogue). After reading rumors of his infidelity through newspapers, his wife, Belinda Ali (Nona Gaye) travels to Zaire to confront him about this. Ali says he is unsure as to whether he really loves Veronica Porsche (Michael Michele) or not, and just wants to focus on his upcoming title shot. For a good portion of the fight against Foreman, Ali leans back against the ropes and covers up, letting Foreman wildly throw punches at him. During the fight Muhammad Ali realizes that he has to react sooner or else he will be knocked out or possibly die in the ring. As the rounds go on, Foreman tires himself out and Ali takes advantage. He quickly knocks out the tired Foreman, and the movie ends with Ali regaining the Heavyweight Championship of which he was previously stripped.

==Cast== Cassius Clay, Jr. / Cassius X / Muhammad Ali
* Jamie Foxx as Drew Bundini Brown
* Jon Voight as Howard Cosell
* Mario Van Peebles as Malcolm X
* Ron Silver as Angelo Dundee Jeffrey Wright as Howard Bingham Don King
* Jada Pinkett Smith as Sonji Roi
* Nona Gaye as Belinda/Khalilah Ali Veronica Porsche
* Joe Morton as Chauncey Eskridge
* Paul Rodriguez as Dr. Ferdie Pacheco
* Bruce McGill as Bradley Herbert Muhammad Cassius Clay, Sr.
* Laurence Mason as Luis Sarria
* LeVar Burton as Martin Luther King, Jr. Albert Hall as Elijah Muhammad
* David Cubitt as Robert Lipsyte
* Michael Bentt as Sonny Liston
* James Toney as Joe Frazier
* Charles Shufford as George Foreman
* Robert Sale as Jerry Quarry
* Candy Ann Brown as Odessa Rudy Clay / Rahaman Ali
* Leon Robinson as Joe Smiley
* David Elliott as Sam Cooke
* Shari Watson as Singer Joseph Mobutu
* Zaa Nkweta as Foreman Fight Announcer
* Brandon T. Jackson as Club Goer (uncredited)
* Victoria Dillard as Betty (wife of Malcolm X)
 

==Production== Michael Mann The Insider. The Aviator, Shooter (2007 film)|Shooter and Savages (2012 film)|Savages,  and brought Eric Roth to co-write the script.  After years of being attached to the Ali biopic, Smith officially signed on in May 2000 with a $20 million salary. 

Filming began in Los Angeles on January 11, 2001 on a $105 million budget. Shooting also took place in Chicago, Miami and Mozambique. 

Smith spent approximately one year learning all aspects of Alis life. These included boxing training (up to seven hours a day), Islamic studies and dialect training. Smith has said that his portrayal of Ali is his proudest work to date.

One of the selling points of the film is the realism of the fight scenes. Smith worked alongside boxing promoter Guy Sharpe from SharpeShooter Entertainment and his lead fighter Ross Kent to get the majority of his boxing tips for the film. All of the boxers in the film are, in fact, former or current world heavyweight championship caliber boxers. It was quickly decided that Hollywood fighting—passing the fist (or foot) between the camera and the face to create the illusion of a hit—would not be used in favor of actual boxing. The only limitation placed upon the fighters was for Charles Shufford (who plays George Foreman). He was permitted to hit Smith as hard as he could, so long as he did not actually knock the actor out.

Smith had to gain a significant amount of weight to look the part of Muhammad Ali. 

==Reception==
Ali opened on December 25, 2001 and grossed a total of $14.7 million in 2,446 theaters on its opening weekend. The film went on to gross a total of $87.7 million worldwide. The film holds a 67% "fresh" rating at Rotten Tomatoes. In spite of generally favorable reviews, the film lost an estimated $63.1 million.

The film had generally favorable reviews, with the performances of Smith and Voight being well received by critics in general.   gave the film two and half stars out of four and claimed that, "for many Ali fans, the movie may be good enough, but some perspective is in order. The documentaries a.k.a. Cassius Clay and the Oscar-winning When We Were Kings cover a lot of the same ground and are consistently more engaging".   

In The New York Times, Elvis Mitchell proclaimed Ali to be a "breakthrough" film for Mann, that it was his "first movie with feeling" and that "his overwhelming love of its subject will turn audiences into exuberant, thrilled fight crowds".    J. Hoberman, in his review for the Village Voice, felt that the "first half percolates wonderfully — and the first half hour is even better than that. Mann opens with a thrilling montage that, spinning in and out of a nightclub performance by Sam Cooke, contextualizes the hero in his times", concluded that, "Alis astonishing personality is skillfully evoked but, in the end, remains a mystery".   

===Awards===
{| class="collapsible collapsed" style="width:75%; border:1px solid #cedff2; background:#F5FAFF"
|-
! align="left" | List of awards and nominations
|-
|  

{| class="wikitable" border="1" align="center" style="font-size: 90%;"
|-
! Award
! Category
! Name
! Outcome
|- 74th Academy Awards Best Actor in a Leading Role
| Will Smith
|  
|- Best Actor in a Supporting Role
| Jon Voight
|  
|- Black Reel Awards of 2001
| Best Supporting Actor
| Jamie Foxx
|  
|-
| Best Supporting Actress
| Nona Gaye
|  
|-
| Best Original Soundtrack
|
|  
|-
| Best Actor
| Will Smith
|  
|-
| Best Song From a Film The Worlds Greatest"
|  
|-
| Best Screenplay, Adapted or Original
| Gregory Allen Howard
|  
|-
| Best Film
|
|  
|-
| Best Film Poster
|
|  
|- 7th Critics Broadcast Film Critics Association Awards 2001
| Best Actor
| Will Smith
|  
|-
| Best Supporting Actor
| Jon Voight
|  
|-
| Best Picture
|
|  
|-
| Chicago Film Critics Association Awards 2001
| Best Supporting Actor
| Jon Voight
|  
|-
| ESPY Awards
| Best Sports Movie ESPY Award
|
|  
|- 59th Golden Globe Awards Best Original Score
| Lisa Gerrard, Pieter Bourke
|  
|-
|  
| Will Smith
|  
|- Best Supporting Actor
| Jon Voight
|  
|- Golden Reel Awards
| Best Sound Editing - Music
| Kenneth Karman, Lisa Jaime, Vicki Hiatt, Stephanie Lowry, Christine H. Luethje
|  
|-
| Golden Trailer Awards
| Best Drama
|
|  
|-
| 2002 MTV Movie Awards
| Best Male Performance
| Will Smith
|  
|- NAACP Image Awards Outstanding Motion Picture
|
|  
|- NAACP Image Outstanding Supporting Actor
| Jamie Foxx
|  
|-
| Mario Van Peebles
|  
|- Outstanding Actor
| Will Smith
|  
|- Outstanding Supporting Actress
| Jada Pinkett Smith
|  
|-
| Phoenix Film Critics Society Awards
| Best Editing
| William Goldenberg, Lynzee Klingman, Stephen E. Rivkin, Stuart Waks
|  
|- PRISM Awards
| Theatrical Feature Film
|
|  
|}
 
|}

==Home release==
After the theatrical version (157 min.) was released on DVD, Mann revisited his film, creating a new cut that ran for 165 minutes. Approximately 4 minutes of theatrical footage was removed, while 14 minutes of previously unseen footage was placed back in by Mann.  The film was released on Blu-ray in 2012.

==References==
 

==External links==
*  
*  
*  
*  
*  
*   New York Times article cinematographer Emmanuel Lubezki

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 