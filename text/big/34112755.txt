Double Trouble (1915 film)
{{infobox film
| name           = Double Trouble
| image          = DoubleTrouble-1917-newspaperad.jpg
| imagesize      =
| caption        = Newspaper advertisement.
| director       = Christy Cabanne
| based on       =  
| writer         = Christy Cabanne
| starring       = Douglas Fairbanks
| music          =
| cinematography = William Fildew
| studio         = Fine Arts Film Company Triangle Distributing
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent English intertitles
}}
 1915 American silent romantic comedy film  written and directed by Christy Cabanne and stars Douglas Fairbanks in his third motion picture. The film is based on the novel of the same name by Herbert Quick. A print of the film is held by the Cohen Media Group. 

==Cast==
* Douglas Fairbanks - Florian Amidon/Eugene Brassfield
* Margery Wilson - Elizabeth Waldron Richard Cummings - Judge Blodgett
* Olga Grey - Madame Leclaire
* Gladys Brockwell - Daisy Scarlett
* Monroe Salisbury - Hotel Clerk
* William Lowery Tom Kennedy - Judge Blodgett
* Kate Toncray  
* Lillian Langdon

==See also==
* List of rediscovered films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 