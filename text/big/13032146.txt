Beck – The Money Man
{{Infobox film
| name           = Beck – The Money Man
| image          = Moneyman.jpg
| image_size     = 
| alt            = 
| caption        = Swedish DVD-cover
| director       = Harald Hamrell
| producer       = Lars Blomgren Thomas Lydholm
| writer         = Rolf Börjlind
| narrator       = 
| starring       = Peter Haber Mikael Persbrandt Stina Rautelin
| music          = Ulf Dageby
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1998
| runtime        = 87 min
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Beck – The Money Man is a 1998 film about the Swedish police detective Martin Beck directed by Harald Hamrell.

== Cast ==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Stina Rautelin as Lena Klingström
*Per Morberg as Joakim Wersén
*Ingvar Hirdwall as Martin Becks neighbour
*Rebecka Hemse as Inger (Martin Becks daughter)
*Fredrik Ultvedt as Jens Loftsgård
*Michael Nyqvist as John Banck
*Lennart Hjulström as Gavling
*Lasse Lindroth as Peter (Ingers boyfriend) Bengt Nilsson as Leonard
*Peter Hüttner as Oljelund

== References ==
* 

== External links ==
* 

 

 
 
 
 
 


 
 