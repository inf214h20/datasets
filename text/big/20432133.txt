Never Surrender (film)
 
{{Infobox film
| name           = Never Surrender
| image          = Never surrender movieposter.png
| size           = 250
| alt            =  
| caption        = "Never Surrender" Film Poster
| director       = Hector Echavarria
| producer       = Hector Echavarria Curtis Petersen
| writer         = Hector Echavarria David Storey |
| starring       = Hector Echavarria B.J. Penn Georges St-Pierre Anderson Silva Quinton Jackson Heath Herring
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Lions Gate Entertainment
| released       =  
| runtime        = 
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
Never Surrender is a 2009 film about an MMA champion who finds himself fighting in illegal underground cage fights. The films cast features a number of real MMA fighters. It was filmed in Los Angeles, California and was produced by Destiny Entertainment Productions. It is distributed in the United States by Lions Gate Entertainment. 

==Cast==
*Hector Echavarria as Diego Carter
*Patrick Kilpatrick as Seifer
*Silvia Koys as Sandra
*James Russo as Jimmy
*Georges St-Pierre as Georges
*Anderson Silva as Spider
*Heath Herring as Stone
*Quinton Jackson as Rampage
*B.J. Penn as BJ
*Damian Perkins as Diamond
*Lateef Crowder as Marco
*Gunter Schlierkamp as Crusher
*Jesse Pruett as lead singer in band

==See also==
*Confessions of a Pit Fighter, a 2005 film with a similar theme

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 