Swann in Love (film)
{{Infobox film
 | name = Swann in Love
 | image = Swann in Love (film).jpg
 | caption =
 | director = Volker Schlöndorff
 | writer = {{plainlist |
* Peter Brook
* Jean-Claude Carrière
*  
* Marcel Proust (novel)
* Volker Schlöndorff
}}
 | starring = {{plainlist | 
* Alain Delon
* Jeremy Irons
* Ornella Muti
* Fanny Ardant
}}
 | music = {{plainlist | David Graham
* Hans Werner Henze
* Gerd Kühr
* Marcel Wengler
}}
 | cinematography =  Sven Nykvist gross = 807,611 admissions (France) 
 | editing =  Françoise Bonnot  
 }}
 
Swann in Love ( ,  ), is a 1984 Franco-German film directed by  . It was nominated for 2 BAFTA Film Awards.

==Plot==

Swann (Jeremy Irons), an eligible bachelor in the best circles of Fin de siècle|fin-de-siècle Paris, has also some more vulgar but rich  friends, the Verdurins. Through them he meets Odette (Ornella Muti), a courtesan, with whom he falls hopelessly in love. She seems to enjoy his company, for which he pays, but considers herself free to socialise and sleep where she pleases, particularly with a rival called de Forcheville. Swann’s passion turns to consuming jealousy, which leads him eventually to accept the social stigma of marrying her. One old friend, the overtly gay Charlus (Alain Delon), stays sympathetic. 

==Cast==
* Jeremy Irons as Charles Swann   
* Ornella Muti as Odette de Crecy   
* Alain Delon as Palamède de Guermantes, Baron de Charlus 
* Fanny Ardant as Duchesse de Guermantes
* Marie-Christine Barrault as Madame Verdurin 
* Anne Bennent as Chloé
* Nathalie Juvet as Madame Cottard
* Charlotte Kerr as sous-maîtresse
* Catherine Lachens as la tante 
* Philippine Pascal as Madame Gallardon 
* Charlotte de Turckheim as Madame de Cambremer 
* Nicolas Baby as the young Jew
* Jean-François Balmer as Dr. Cottard 
* Jacques Boudet as Duc de Guermantes 
* Jean-Pierre Coffe as Aimé
* Jean-Louis Richard as Monsieur Verdurin
* Bruno Thost as Saniette 
* Geoffroy Tory as Forcheville 
* Roland Topor as  Biche
* Vincent Martin  as Biche
* Arc Adrian as Swanns Valet
* Pierre Celeyron as Valet Pif
* Romain Brémond as Protocole Guermantes
* Véronique Dietschy as Madame Vinteuil
* Ivry Gitlis violinist
* François Weigel pianist

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 