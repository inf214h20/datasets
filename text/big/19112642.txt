Beijing Rocks
 
 
{{Infobox film
| name           = Bak Ging lok yue liu
| image          = 
| alt            =  
| caption        = 
| director       = Mabel Cheung
| producer       = John Chong Alex Law
| writer         = Alex Law
| starring       = Le Geng Richard Ng Qi Shu Daniel Wu
| music          = 
| cinematography = Peter Pau
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Mandarin
| budget         = 
| gross          = 
}}

Beijing Rocks (北京樂與路) is a 2001 Hong Kong film directed by award-winning director Mabel Cheung about the rock and roll music scene in Beijing.  Starring Shu Qi and Daniel Wu, it was nominated for five Hong Kong Film Awards including Best Picture and Best Cinematography.

==Cast and roles==
* Geng Le - Road
* Richard Ng - Wu De-hui
* Shu Qi - Yang Yin
* Daniel Wu - Michael Wu
* Faye Yu
* Henry Ng

 
 

==References==
 

==External links==
*  

 
 
 
 
 


 