On the Outs
 
On the Outs is a 2004 drama film chronicling three young women in Jersey City, New Jersey and the decisions they make in the period of a couple weeks.  

One girl is a 17 year old drug dealer, one girl is a teenage drug addict with a child and one is a teenager dealing with pregnancy.

==Cast==
*Anny Mariano as Suzette Williams
*Judy Marte as Oz
*Paola Mendoza as Marisol Pagan
*Dominic Colon as Chewey
*Flaco Navaja as Jimmy Ortiz
*Danny Rivera as J Stutter

==Honors==

===Deauville Film Festival===
*Winner of the Jury Special Prize 
*Nominated for the Grand Special Prize

===Gotham Awards===
*Nominated for the Breakthrough Director Award

===Independent Spirit Awards===
*Nominated for the Independent Spirit Award

===Slamdance Film Festival===
*Winner of the Grand Jury Prize

==Critical reception==
On the Outs received a 93% on Rotten Tomatoes.  The Villager wrote: "Run to see this film..."

==References==
 

 
 
 
 
 
 
 
 


 