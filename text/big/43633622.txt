The Visit (2015 film)
{{Infobox film
| name           = The Visit
| image          = The Visit (2015 film) poster.jpg
| alt            = 
| caption        = Teaser poster
| director       =M. Night Shyamalan
| producer       = M. Night Shyamalan Jason Blum Marc Bienstock Steven Schneider Ashwin Rajan
| writer         = M. Night Shyamalan
| starring       = Kathryn Hahn   Ed Oxenbould   Peter McRobbie   Benjamin Kanes
| music          = James Newton Howard
| cinematography = Maryse Alberti
| editing        = Luke Franco Ciarrocchi 
| studio         = Blinding Edge Pictures   Blumhouse Productions
| distributor    = Universal Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $5 million 
| gross          = 
}}

The Visit is an upcoming 2015 American found footage Thriller (genre)|thriller/horror film written and directed by M. Night Shyamalan.  The film stars Kathryn Hahn, Ed Oxenbould, Peter McRobbie and Benjamin Kanes. The film is scheduled to be released on September 11, 2015, by Universal Pictures.

==Plot==
Things appear normal until two children discover that Nana and Papa (Deanna Dunagan and Peter McRobbie) are involved in something deeply disturbing after being left to stay at their Grandparents house while their mother is on vacation. They’re warned not to come out of their room past 9:30 at night, but when they do they realize that theres something terribly wrong with their grandparents.  After pleading online to their Mom for rescue, they realize that theyre on their own against the seemingly insane elders.

==Cast==
* Kathryn Hahn   
* Ed Oxenbould 
* Olivia De Jonge 
* Peter McRobbie 
* Benjamin Kanes as Dad 
* Erica Lynne Marszalek as Train Passenger 
* Deanna Dunagan 
* Jon Douglas Rainey as Police Officer 
* Brian Gildea as Police Officer 
* Shawn Gonzalez as Train Passenger  
* Richard Barlow as Police Officer 
* Steve Annan as Man on Street 
* Michael Mariano as Hairy Chested Contestant 

== Production == Chester Springs Chestnut Hill, Philadelphia.  Blinding Edge Pictures and Blumhouse Productions observed the production under the leadership of Marc Bienstock, Jason Blum, Ashwin Rajan and M. Night Shyamalan. At the time, the film was titled Sundowning with Deanna Dunagan, Kathryn Hahn, Ed Oxenbould, Peter McRobbie and Benjamin Kanes in the lead roles. 

== Release ==
Universal Pictures has set the film for release on September 11, 2015 in the United States.   On April 17, 2015, the first official trailer was released to theaters attached to the film Unfriended. Shyamalan stated that the trailer would be released online later that week.  

==References==
 

== External links ==
*  

 

 
 
 
 
 