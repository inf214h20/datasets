María Morena
{{Infobox film
| name           = María Morena
| image          = 
| caption        = 
| director       = José María Forqué Pedro Lazaga
| producer       = Miguel Herrero Juan N. Solórzano
| writer         = Francisco Naranjo Pedro Lazaga José María Forqué
| starring       = Paquita Rico
| music          = 
| cinematography = Manuel Berenguer
| editing        = María Rosa Ester
| distributor    = 
| released       = 1951
| runtime        = 75 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

María Morena is a 1951 Spanish drama film directed by José María Forqué and Pedro Lazaga. It was entered into the 1952 Cannes Film Festival.   

==Cast==
* Paquita Rico - María Morena
* José María Mompín - Fernando (as José Mª Mompin)
* Rafael Luis Calvo - Cristóbal
* Félix de Pomés - Juan Montoya
* Alfonso Muñoz - Don Chirle
* Consuelo de Nieva - María Pastora
* Purita Alonso
* Julio Riscal - Relámpago
* Francisco Rabal - El Sevillano
* Luis Induni - Romero
* Carlos Ronda - Sargento
* Modesto Cid - Primer anciano en taberna
* Ana Rodríguez
* José Marco
* Enriquito

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 