Mean Dog Blues
{{Infobox film
| name           = Mean Dog Blues
| image          = Mean-dog-blues-poster.jpg
| image_size     = 
| caption        = 
| director       = Mel Stuart
| writer         = 
| narrator       = 
| starring       = Gregg Henry
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = March 1978
| runtime        = 108 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Mean Dog Blues is a 1978 drama film directed by Mel Stuart. It stars Gregg Henry and Kay Lenz.  

==Plot==
A friend driving under the influence seriously injures a child. Paul Ramsey, a singer, offers to take the rap in court, only to be double-crossed and sentenced to five years in prison.

He ends up with other inmates treated sadistically by a brutal prison official who makes them train his vicious attack dogs.

==Cast==
*Gregg Henry as Paul Ramsey
*Kay Lenz as Linda Ramsey
*Scatman Crothers as Mudcat
*Tina Louise as Donna Lacey
*Felton Perry as Jake Turner
*George Kennedy as Captain Omar Kinsman

==References==
 

==External links==
* 

 

 
 