Reunion in France
{{Infobox film
| name           = Reunion in France
| image          = Posterreunionusx.jpg theatrical poster
| director       = Jules Dassin
| producer       = Joseph L. Mankiewicz
| writer         = Story:  )
| screenplay     = Jan Lustig Marvin Borowsky Marc Connelly
| narrator       =
| starring       = Joan Crawford John Wayne Philip Dorn
| music          = Franz Waxman
| cinematography = Robert H. Planck
| editing        = Elmo Veron
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $1,054,000  . 
| gross          = $1,863,000 
}}

Reunion in France is a 1942 American Metro-Goldwyn-Mayer film starring Joan Crawford, John Wayne, and Phillip Dorn in a story about a woman in occupied France who, learning her well-heeled lover has German connections, aids a downed American flyer. The film was directed by Jules Dassin and Ava Gardner has a tiny role as a Parisian shopgirl.

==Plot==
 
1940 in Paris, Michele de la Becque (Joan Crawford) is a career woman in love with industrial designer Robert Cortot (Philip Dorn). Together they enjoy a luxurious lifestyle unfazed by the approach of World War II. After the Battle of France and subsequent German occupation, Michele discovers her lover is socializing with German officers and his plants are manufacturing weapons for them.  She confronts him and he does not deny her evidence. She is outraged.  She aids a downed American in the Eagle Squadron of the Royal Air Force bomber pilot Pat Talbot (John Wayne) from Pennsylvania and finds herself falling in love with him.  Later, she discovers Cortot is turning out defective weapons for the Germans and organizing a French fighting force. Michele is happily reunited with Cortot.

==Cast==
* Joan Crawford as Michele de la Becque
* John Wayne as Pat Talbot
* Philip Dorn as Robert Cortot
* Reginald Owen as Gestapo agent
* John Carradine as Head of the Paris Gestapo
* Moroni Olsen as Gerbeau
* Ava Gardner shopgirl Marie (uncredited)
* Natalie Schafer as Amy Schröder
* Henry Daniell as Fleuron
* Albert Bassermann as General Hugo Schroeder
* Ann Ayars as Juliette
* J. Edward Bromberg as Durand
* Henry Daniell as Emile Fleuron
* Howard Da Silva as Anton Stregel (as Howard da Silva)
* Charles Arnt as Honoré
* Morris Ankrum as Martin
* Edith Evanson as Genevieve
* Ernst Deutsch as Captain (as Ernest Dorian)

==Reception==
The movie made $1,046,000 in the US and Canada and $817,000 elsewhere, earning MGM a profit of $222,000. 

Film Daily noted, "The film, directed capably by Jules Dassin, has been given a first-rate production by Joseph L. Mankiewicz." 

T.S. in the New York Times observed,  "If Reunion in France is the best tribute that Hollywood can muster to the French underground forces of liberation, then let us try another time.   ... is ...simply a stale melodramatic exercise for a very popular star. In the role of a spoiled rich woman who finds her "soul" in the defeat of France, Joan Crawford is adequate to the story provided her, but that is hardly adequate to the theme." 

Years after making the film, Joan Crawford was quoting as saying this about Reunion in France: "Oh God. If there is an afterlife and I am to be punished for my sins, this is one of the pictures theyll make me see over and over again. John Wayne and I both went down for the count, not just because of a silly script but because we were so mismatched. Get John out of the saddle and youve got trouble."  

==See also==
* John Wayne filmography

==References==
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 