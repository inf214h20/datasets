The Dirt Bike Kid
The Dirt Bike Kid is a 1985 film directed by Hoite Caston, produced by Julie Corman, starring Peter Billingsley and Stuart Pankin, about a boy who discovers a magic dirt bike that has a mind of its own. Part of the story is inspired by Jack and the Beanstalk.

==Plot== occupied by a friendly spirit, and does things to help out Jack. Mrs. Simmons is furious that Jack spent her money on a dirt bike, and promptly confiscates the bike and sells it to a local shop owner Mr. Zak (Al Evans) to recoup $50. However, the bike returns in the middle of the night to visit Jack. Jack tells this to Mr. Zak, who says Jack can work off his debt by having himself and the bike make deliveries for him.
 Patrick Collins), hacking into the banks computer they learn that the Doghouses land would not make a very good location for Hodgkins bank and that Mr. Hodgkins personal account is not as sizable as the community is led to believe. Hodgkins learns of Jacks attempt to save the Doghouse, and enlists the aid of Max (who is a player on his Little League team) who brings in a biker named Big Slime (Weasel Forshaw).

When Mr. Hodgkins converses on Jacks house, Mr. Hodgkins calls in Police Chief Salt (John William Galt) of the local force. Chief Salt orders his men to impound the dirt bike in exchange for Hodgkins not foreclosing on Salts overextended mortgage at Hodgkins Bank. However, Mazie and Mike come to Jacks aid by paying the impoundment fee for Jack to get his dirt bike back.

When the groundbreaking ceremony on the banks construction is set to begin by having a bulldozer raze the Doghouse, Jack shows up with his Little League team, who disrupts the event by getting into a pie fight with Max, Hodgkins, and Big Slimes bikers. The dirt bike then takes away a shocked Hodgkins while Jack is driving it, where Jack tells Hodgkins that he is aware of Hodgkins and Mikes financial problems, and has an idea where all can benefit.

One year later, Hodgkins shows up in a goofy hot dog suit to commemorate the opening of a shopping mall that features a renovated Doghouse and the new Hodgkins Bank, now fully constructed. Pee Wee, once unpopular with girls, is now admired by pretty girls. Jacks mom has found gainful employment, Mike and Mazie are now married and expecting their first baby. Chief Salt now works as a security guard in the bank after having been presumably fired from his job as police chief by the town council for corruption. Big Slime (who is now wearing a shirt and tie and admits his true name is Arthur) has dissolved his biker gang and taken a job as a bank teller in the new Hodgkins Bank. With the community lifted, the dirt bikes magic stops working for Jack, whose mother tells him it may have been magic for Jack only to help him out. Jack gives the bike to another little boy, and it appears the magic returns once again for another child.

==Production==

The dirt bike used in the film is a 1985 Yamaha YZ80. 

==Cast==
* Peter Billingsley as Jack Simmons 
* Stuart Pankin as Mr. Hodgkins
* Anne Bloom as Janet Simmons Patrick Collins as Mike
* Sage Parker as Mazie Clavell
* Chad Sheets as Bo Gavin Allen as Max Daniel Breen as Flaherty
* Weasel Forshaw as Big Slime
* John William Galt as Police Chief Salt
* Courtney Kraus as Beth
* Holly Schenk as Sue Al Evans as Mr. Zak

==Home Video==

On November 18, 2014, "The Dirt Bike Kid" was released on DVD and Blu-ray by Kino Video & Scorpion Releasing based off of a brand new high-definition master. 

==External links==
*  

==References==
 

 
 
 