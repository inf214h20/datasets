Zhukovsky (film)
{{Infobox film
| name           = Zhukovsky
| image          = 
| image_size     = 
| caption        =  Dmitri Vasilyev
| producer       = 
| writer         = Anatoli Granberg
| narrator       = 
| starring       = Yuri Yurovsky
| music          = Vissarion Shebalin
| cinematography = Anatoli Golovnya Tamara Lobova	
| editing        = 
| distributor    = 
| studio         = Mosfilm
| released       = 13 July 1950
| runtime        = 90 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 Soviet film Dmitri Vasilyev, Nikolai Zhukovsky (1847–1921), founding father of modern aerodynamics|aero- and hydrodynamics. In 1950 Pudovkin received the Best Director award at the 5th Karlovy Vary International Film Festival for this film. In 1951 Pudovkin, Shebalin, Golovnya, and Belokurov received the Stalin Prize.
 
==Cast== Nikolai Zhukovsky
* Ilya Sudakov – Dmitri Mendeleev
* Vladimir Belokurov – Sergey Chaplygin
* Vladimir Druzhnikov – Pyotr Nesterov
* Sofiya Giatsintova – Zhukovskys Mother Boris Smirnov		
* Georgi Yumatov

==External links==
* 

 

 
 
 
 
 
 
 

 