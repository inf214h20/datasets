Sins of Rome
{{Infobox film
 | name =Sins of Rome
 | image =Sins of Rome.jpg 
 | caption =
 | director = Riccardo Freda 
 | writer =   
 | starring =   Renzo Rossellini 
 | cinematography =  Gábor Pogány 
 | editing =   Mario Serandrei 
 | producer = 
 | released = 
 | country = Italy
 | language       = Italian
}} Italian Epic epic historical drama film directed by Riccardo Freda and loosely based on the life story of Spartacus.     The rights of films negatives and copies were bought by the producers of Stanley Kubricks Spartacus (film)|Spartacus, as to prevent eventual new releases of the film that could have damaged the commercial outcome of Kubriks film; this resulted in Sins of Romes withdrawal from market for about thirty years. 

== Cast ==

*Massimo Girotti as Spartacus
*Ludmilla Tchérina as Amitis
*Gianna Maria Canale as Sabina
*Yves Vincent as Ocnomas
*Carlo Ninchi as Marcus Licinius Crassus
*Carlo Giustini as Artorige
*Teresa Franchini as  Spartacuss Mother
*Vittorio Sanipoli as Marcus Virilius Rufus 
*Umberto Silvestri as  Lentulus 
*Renato Baldini as  Gladiator 
*Nerio Bernardi  
*Cesare Bettarini 

==References==
 

==External links==
* 
 

 
 
 
  
 
   
 
 
 
 
 
 
 
 
 
 

  
 