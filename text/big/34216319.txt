Paradesi (2013 film)
 
 
 
{{Infobox film
| name           = Paradesi
| image          = Paradesi.png
| alt            =
| caption        = First look poster Bala
| producer       = Bala Bala   Nanjilnadan
| story          =
| starring       = Adharvaa Vedhicka Dhansika
| music          = GV.Prakash kumar
| cinematography = Chezhiyan
| editing        = Kishore Te.
| studio         = B Studios
| distributor    = JSK Film Corporation
| released       =  
| runtime        = 126 minutes 
| country        = India
| language       = Tamil
}}
 Tamil drama Bala  Telugu was released with the same name. The film opened mostly to positive reviews.    Sify said that the box office collections were  average. 

==Plot==
Rasa (Adharvaa) is a carefree young man living in a rural village in the Madras Presidency during the early days of the British Raj. Orphaned at a young age, he is brought up by his nagging but loving hunchback grandmother. Angamma (Vedhicka), a village girl falls for him and takes pleasure in bullying him. When she finally confesses her feelings for him, they have sex and soon reveal the fact that they are in love before the entire village and also get into physical relationship. Angammas mother objects as Rasa is jobless and irresponsible, making him unqualified to become anyones husband.

Rasa then goes to the nearby village in search of work. He comes across a friendly Kangani, who then follows Rasa back to his village. The Kangani offers a work for the villagers at the British tea plantations at the hillside. He promises them proper accommodation and high wages. Like many of the villagers, Rasa signs up with the Kangani, hoping he can send home money every month for his ailing grandmother. Both Angamma and his grandmother are sad to watch him go.

When Rasa and his villagers finally arrive at the tea plantation, things are not as what the Kangani promised. The Kangani and his henchmen rule the plantation with an iron fist. The British plantation manager does not care for the workers. Rasa becomes friends with Maragadham (Dhansika) and her little daughter, the wife and child of the only worker who have ever escaped the plantation so far. Rasa soon gets a letter from his grandmother stating that Angamma now lives with her after her family found out she is pregnant with his child.

It is soon revealed that all the workers daily wages go to their food and lodging. Rasa will have to work there for many more months if he wishes to leave the place. Maragadham too has to work for both her time and for her husbands contract. The workers finally realise that they have been made slaves to the British businessmen. Feeling homesick, Rasa tries to escape. Unfortunately, he is caught by the Kanganis henchmen and cuts the left leg main bone (fibula), just like every other worker who had tried to escape and failed.
 convert and his English wife come to the plantation. But rather than treating the sick workers, they spend all their time trying to convert them.

Rasas time at the plantation draws to an end. But he cannot rejoice as Maragadham becomes ill and finally dies. He then adopts her daughter and awaits his time to leave. However, he is then told that by adopting Maragadhams daughter, he has also inherited both her parents debt to the plantation and will have to work there for almost ten more years to pay it all off. As he is lamenting his fate on top of a hill, he notices a new group of slaves being brought in. Among them, he sees Angamma and their son. He runs after them and in tears, tells them they have both walked into the mouth of hell.

The film ends with a wide shot of the tea plantation and all its workers staring at Rasa crying his heart out.

==Cast==
* Adharvaa as Raasa
* Vedhicka as Angamma
* Dhansika as Maragadham
* Udhay Karthik as Thangarasu
* Ritwika as Karuthakanni Jerry as Kangaani
* K. Sivasankar as Parisutham
* Kalpana as Angammas mother

==Production==

===Development===
Paradesi, a pejorative Tamil term meaning a foreigner or wastrel, deals with the story of enslaved tea plantation workers in pre-independent India. The film has been adapted from Eriyum Panikadu a Tamil translation of the 1969 novel Red Tea by Paul Harris Daniel which deals with Harriss encounters with enslaved tea plantation workers in the Madras Presidency in colonial India. 

===Casting===
Adharvaa was signed on to play the lead role, in his third leading role after Baana Kaathadi and Muppozhudhum Un Karpanaigal, and Bala asked him to shed 10 kilos and sport a shaved hairstyle.  Initial reports suggested that Amy Jackson would play a pivotal role in the film, but the actress denied that she was approached for the role.  Vedhicka was signed to act in the film after a brief sabbatical away from Tamil films and the project marked her most high-profile venture til date.  Bala roped in about 200 junior artistes for this film and had them all go bald, requesting them to stay bald throughout the 200-day schedule as it is one of the major requirements for the film. 
 Srinivasan shot for a sequence in the film, but Balas unhappiness with the output saw the actor later replaced by choreographer Shivshankar in the brief role. 

===Filming===
First schedule of the film has been completed on 28 January 2012 on Ramanathapuram and the team is getting ready to start the next schedule in Connoor.  However, with the tiff between the FEFSI and Producers Council still going on, the director has decided to postpone the shooting schedules until the issue is resolved.  The film was shot in Salur and Manamadurai in Sivagangai district, Munnar and Talaiyar in Kerala, and the forest areas in Theni district.  To get her act right in the climax scene, Dhansika survived with mere water and fruit juice for six consecutive days.  The film that was wrapped in just 90 days had released its first look poster on 8 August 2012.   

===Promotion and release===
Though early reports stated that Paradesi would release in October 2012,  it was later announced that it would release on 21 December 2012. 

A one minute reality trailer drew controversies as soon as a preview was released on YouTube.  It showed Bala hitting and abusing the actors. Later after the release, the scenes were shown to be a part of the film and Adharva tweeted on his Twitter saying that the sticks Bala used were just dummies. 

After the film was passed with a "U" certificate from the Indian Censor Board,  its release was postponed to January 2013,  and later to March.  Finally it was released on 15 March 2013.

==Distribution==
Distributed by  , Paradesi is a 2013 film directed by Bala. The film is set in pre independence period, based on real life incidents that took place in the 1930s.

==Soundtrack==
{{Infobox album|  
| Name       = Paradesi
| Type       = Soundtrack
| Artist     = G. V. Prakash Kumar
| Cover      = Paradesi-Album-Art.jpg
| Released   =   Feature film soundtrack
| Length     = Tamil
| Label      = Gemini Audio
| Producer   = G. V. Prakash Kumar
| Reviews    =
| Last album =Thaandavam (2012)
| This album =Paradesi (2012)
| Next album =Naan Rajavaga Pogiren (2012)
}}

The soundtrack was composed by G. V. Prakash Kumar. The audio launch and trailer release of the film happened on 25 November 2012 at Sathyam cinemas, Chennai.  Vairamuthu joined hands with director Bala for the first time in this film. Prakash Kumar composed the music and Vairamuthu wrote the lyrics based on what has been shot. 

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    =
| all_lyrics      = Vairamuthu

| title1          = Avatha Paiyya
| lyrics1         =
| extra1          = Yazin nizar, Vandana Srinivasan
| length1         = 5.34

| title2          = Sengaade
| lyrics2         =
| extra2          = Madhu Balakrishnan
| length2         = 8.02

| title3          = Or mirugam
| lyrics3         =
| extra3          = V.V. Prasanna, Pragathi Guruprasad
| length3         = 5.47

| title4          = Thannai Thaane
| lyrics4         =
| extra4          = Gaana Bala
| length4         = 3.07

| title5          = Senneer Thaana
| lyrics5         =
| extra5          = Gangai Amaran, Priya Himesh
| length5         = 6.22
}}

==Critical reception==
{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|-
| Indiaglitz
|  
|-
| Bollywoodlife
|  
|-
|  Rediff
|  
|-
| Sify
|  
|-
| In.com
|  
|-
| Rediff.com
|  
|-
| colspan="2" style="text-align:center;"|   indicates that the given rating is an average rating of all reviews provided by the source
|}

Vivek Ramz rated it 4 out of 5 and stated that Paradesi is dark, gritty and bloody realistic & concluded that its film-making at its best. A must watch!  S Saraswathi of Rediff rated it 4/5, saying Paradesi  lifelike characters draw you into their lives with their realistic performances" and concluded, "Bala’s Paradesi stays with you long after you walk out of the theatre. In fact you need a few minutes to reorient yourself back to the present, Bala captivates with his authentic script, unadorned visuals and down-to-earth characters. A must-watch."  One India stated, "Paradesi is a brilliant made movie but the lack of commercial elements will not guarantee the success. Nonetheless, the film will be loved by Balas fans!"  Nandini Ramnath of Livemint said, "During Paradesi’s most heightened moments, it appears as though Bala is single-handedly trying to undo that cinematic legacy." and concluded, "Paradesi clocks a crisp 120 minutes– not enough to replicate the richness of Pithamagan and Avan Ivan, and not enough to accommodate new ideas on age-old forms of exploitation.".    Sify said, "Paradesi may be too dark for some viewers. But here is a definitive movie that touches a deep emotional chord and will leave a lump in your throat. Paradesi is definitively a classic with grace and power. Hats off to Bala for taking the road less traveled, and that makes all the difference." and gave the verdict "Brilliant".    The Hindu saying These traditional commercial-film elements are an odd fit in a film that’s attempting to be something wholly different. Paradesi is an important lesson on a forgotten chapter of history, but as cinema, Bala’s truest isn’t up there with Bala’s best." 
IBN Live saying Pardesis The result is prominent in Paradesi, perhaps more than any other film of his. This is pure unadulterated cinema and the screenplay and the plot rank high above everything else." and gave the verdict "Brilliant".Paradesi is a master class in great filmmaking 

==Accolades==
 
{|class="wikitable sortable" style="font-size:95%;"
|-
|+List of awards and nominations
! Award
! Category
! Recipients and nominees
! Result
|- National Film Awards Best Costume Design
| Poornima Ramaswamy
|  
|-
| rowspan="8" | London International Film Festival 
| Best Film of the Festival Bala
|  
|-
| Best Director of a Foreign Film Bala
|  
|-
| Best Foreign Language Feature Film Bala
|  
|-
| Best Director of a Foreign Feature Film Bala
|  
|-
| Best Lead Actor Adharvaa Murali
|  
|-
| Best Cinematography
| Chezhiyan Ra
|   
|-
| Best Costume
| Poornima Ramaswamy
|   
|-
| Best Music
| GV.Prakash kumar
|  
|-
| 11th Chennai International Film Festival
| Special Jury Award
| Adharvaa
|  
|-
| rowspan="4" | Norway Tamil Film Festival Awards      
| Best Film Bala
|  
|-
| Best Director Bala
|  
|-
| Best Cinematographer
| Chezhiyan Ra
|  
|-
| Best Actor Adharvaa Murali
|  
|-
| rowspan="4"| Vijay Awards Best Director Bala
|  
|- Best Actress
| Vedhicka
|  
|- Best Supporting Actress
| Dhansika
|  
|- Best Costume Designer
| Poornima Ramaswamy
|  
|-
| rowspan="4"| Filmfare Awards South  Best Director Bala
|  
|- Best Actor
| Atharvaa
|  
|- Best Actress
| Vedhicka
|  
|- Best Supporting Actress
| Dhansika
|  
|-
| rowspan="3"| Edison Awards (India)  {{cite web |title=7th Edison Awards: Winners List And Photos
|url=http://www.filmibeat.com/tamil/news/2014/winners-list-7th-edison-awards-photos-132255-pg1.html||date=17 February 2014}}  Best Director Bala
|  
|- Extreme Performance - Male
| Atharvaa
|  
|- Extreme Performance - Female
| Vedhicka
|  
|-
| rowspan="3"| Techofes  {{cite web |title=‘Paradesi’ won 5 awards at Techofes’14
|url=http://www.kollyinsider.com/2014/02/paradesi-won-5-awards-at-techofes14.html|date=14 February 2013}}  Best Director Bala
|  
|- Best Actor
| Atharvaa
|  
|- Best Actress
| Vedhicka
|  
|-
| rowspan="1"| Screen Moon Awards  Best Actress
| Vedhicka
|  
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 