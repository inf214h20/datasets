Empire of the Wolves
{{Infobox Film
| name           = Empire of the Wolves (LEmpire des loups)
| image          = EmpireoftheWolvesposter.jpg
| caption        = 
| director       = Chris Nahon
| producer       = Andrew Colton Patrice Ledoux
| writer         = Christian Clavier Chris Nahon Jean-Christophe Grangé Franck Ollivier Luc Bossi Simon Michaël
| starring       = Jean Reno  Arly Jover  Jocelyn Quivrin Olivia Bouyssou Luca De Medici	 	 Grégory Fougères Dan Levy Pascal Morel Samuel Narboni	 	
| cinematography = Michel Abramowicz     
| editing        = Marco Cavé
| distributor    = Gaumont/Columbia TriStar Films
| released       = 20 April 2005
| runtime        = 128 minutes
| country        = France
| awards         = 
| language       = French
| budget         = €24,000,000  http://www.imdb.com/title/tt0402158/business 
| gross          = $11,875,866  http://boxofficemojo.com/movies/intl/?page=&id=_fLEMPIREDESLOUPS01 
| preceded_by    = 
| followed_by    = 
}}
 movie Film directed by Chris Nahon,  written by Christian Clavier, Jean-Christophe Grangé, Chris Nahon and Franck Ollivier, and starring Jean Reno, Arly Jover, and Jocelyn Quivrin. 

==Plot summary== reconstructive surgery, Turkish heritage. A series of events escalates into a confrontation with the Turkish mafia and the death of Annas would-be assassination|assassin.

==Cast==

* Jean Reno	as Jean-Louis Schiffer
* Arly Jover as Anna Heymes
* Jocelyn Quivrin as Paul Nerteaux
* Laura Morante as Mathilde Urano
* Philippe Bas as Laurent
* David Kammenos as Azer
* Didier Sauvegrain as Dr. Ackerman
* Patrick Floersheim as Charlier
* Etienne Chicot as Amien
* Albert Dray as Le lieutenant
* Vernon Dobtcheff as Kudseyi
* Élodie Navarre as La fliquette
* Philippe du Janerand as Le légiste
* Corentin Koskas as Linterne
* Jean-Pierre Martins as Professeur Ravi
* Jean-Marc Huber as Gurdilek
* Donatienne Dupont as Clothilde
* Jean-Michel Tinivelli	as Caraccili
* Sandra Moreno as Marie-Sophie
* Laurence Gormezano as Chantal
* Vincent Grass as Marius
* Guillaume Lamant-Deboudt as Lacroux
* Emmanuelle Escourrou as Gozar
* Akim Colour as Louvrier
* Gérard Touratier as Gruss
* Aurélie Meriel as La secrétaire
* Emre Kinay as Le policier
* Robert Hoehn
* Arnaud Duléry	as Un flic médico-légal
* Jacques Curry as Roman 

==Release==
The film premiered on 20 April 2005 in France and was on 30 July 2005 part of the Fantasy Film Fest in München.

==Soundtrack==
{{Tracklist lyrics_credits = yes title1     = Kill Everything (Main Version) length1    = 03:23 lyrics1  Skin
|title2     = Passage Brady (Rap Version) length2    = 03:49 lyrics2    = Intouchable note2      = Featuring – Demon One, RimK title3     = Passage Brady (Movie Version) length3    = 01:05 lyrics3  Olivia Bouyssou title4     = Bloody Entertainer length4    = 03:07 lyrics4    = Dan Levy and Olivia Bouyssou title5     = Anna Takes Revenge length5    = 01:28 lyrics5    = Dan Levy title6     = Wolves length6    = 04:34 lyrics6    = Dan Levy and Olivia Bouyssou title7     = Hollow Of Your Shoulder length7    = 03:54 lyrics7    = Gregory Fougères and Olivia Bouyssou title8     = Black Dove length8    = 03:33 lyrics8    = Gregory Fougères, Olivia Bouyssou and Luca De Medici title9     = Austerman Fight length9    = 02:01 lyrics9    = Samuel Narboni title10    = Java length10   = 02:45 lyrics10   = Gregory Fougères and Olivia Bouyssou title11    = Woolen Silence length11   = 04:43 lyrics11   = Dan Levy and Olivia Bouyssou title12    = Columbarium length12   = 04:29 lyrics12   = Dan Levy title13    = The Sewers length13   = 02:20 lyrics13   = Pascal Morel title14    = Ackermann Flight length14   = 00:49 lyrics14   = Dan Levy title15    = Annas Shower Theme length15   = 02:53 lyrics15   = Pascal Morel title16    = Mist On The Seine length16   = 01:51 lyrics16   = Luca De Medici title17    = Requiem length17   = 03:15 lyrics17   = Dan Levy title18    = Kill Everything (Movie Version) length18   = 01:39 lyrics18   = Skin title19    = Metro Chase length19   = 01:27 lyrics19   = Gregory Fougères and Olivia Bouyssou title20    = Cemetery length20   = 02:29 lyrics20   = Luca De Medici title21    = Arrival In Turkey length21   = 03:33 lyrics21   = Dan Levy title22    = Azers Hill length22   = 02:03 lyrics22   = Dan Levy title23    = China Doll (Remix) length23   = 03:38 lyrics23   = Dan Levy and Olivia Bouyssou title24    = Anna Laurent Theme length24   = 03:09 lyrics24   = Samuel Narboni title25    = Police Station length25   = 03:51 lyrics25   = Luca De Medici title26    = China Doll length26   = 02:25 lyrics26   = Dan Levy and Olivia Bouyssou
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 