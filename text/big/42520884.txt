The Human Race (film)
 
{{Infobox film
| name           = The Human Race
| image          = 
| alt            = 
| caption        = 
| director       = Paul Hough
| producer       = Bryan Coyne
| writer         = Paul Hough
| starring       = Paul McCarthy-Boyington   Eddie McGee   Trista Robinson
| music          = Marinho Nobre
| cinematography = Matt Fore
| editing        = Paul Hough
| studio         = Paul Hough Entertainment
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Human Race is an American horror film directed and written by Paul Hough.    A work-in-progress copy was screened at the 2012 Fantasia Film Festival and the finished copy had its world premiere on April 11, 2013 at the Brussels International Fantastic Film Festival. It stars Paul McCarthy-Boyington, Eddie McGee and Trista Robinson as a group of people who find themselves forced to race or die.

== Synopsis ==
About eighty people are horrified to wake in a strange institutional setting, with the only common factor between them the knowledge that prior to their abductions they witnessed a sudden flash of white light. A voice in their heads demands that they all participate in a gruesome race to the finish. The rules are that they must all participate and that if they stray from the marked paths in any way or if they are lapped twice, they will die. Only one person will survive. Many die instantly, but others are forced to their deaths by other racers. As the numbers thin, the survivors become more and more desperate to stay alive.

== Cast ==
* Paul McCarthy-Boyington as Justin
* Eddie McGee as Eddie
* Trista Robinson as Deaf Female
* T. Arthur Cottam as Deaf Male
* Brianna Lauren Jackson as Veronica
* Fred Coury as Yellow Jersey
* B. Anthony Cohen as The Priest
* Noel Britton as Stressed Out
* J. Louis Reid as War Vet
* Celine Tien as Ting
* Ian Tien as Shio Lau Richard Gale as Evil Brother
* Luke Y. Thompson as Orange Vest
* Jonica Patella as Homeless
* Trip Hope as Jim Phillips (as A.K. Walker)

==Reception==
Critical reception has been mostly positive.  Variety wrote that "Although haphazardly assembled, Paul Houghs low-budget survival thriller is not without intrigue",  while the Los Angeles Times called it an "eerie, violent sci-fi survival tale".  Twitch Film commented that "while the film never quite transcends the genre in the ways that it could have, its still an exciting, well-acted and extremely bloody slice of survivalist action with some nice surprises up its sleeve". 

== References ==
 

==External links ==
*  
*  at Horror-Movies

 
 
 
 
 
 
 
 
 


 