Spanglish (film)
{{Infobox film 
| name           = Spanglish
| image          = Spanglish poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = James L. Brooks  
| producer       = Julie Ansell James L. Brooks Richard Sakai
| writer         = James L. Brooks
| narrator       = Aimee Garcia
| starring       = Adam Sandler Paz Vega Téa Leoni Cloris Leachman
| music          = Hans Zimmer
| cinematography = John Seale
| editing        = Richard Marks
| studio         = Gracie Films
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 131 minutes
| country        = United States
| language       = English Spanish
| budget         = $80 million 
| gross          = $55,041,367 
}}
Spanglish is a 2004 American comedy-drama film written and directed by James L. Brooks, and starring Adam Sandler, Paz Vega, and Téa Leoni. It was released in the United States on December 17, 2004 by Columbia Pictures and by Gracie Films, and in other countries over the first several months of 2005. This film grossed $55,041,367	  worldwide, significantly less than the $80 million production budget.   

==Plot==
Cristina Moreno is applying to Princeton University. For her application essay, she tells the story of her childhood.

Flor Moreno ( ) and Deborah Clasky (Téa Leoni), their kids Bernice (Sarah Steele) and Georgie (Ian Hayland), and Deborahs alcoholic mother Evelyn Wright (Cloris Leachman). John is head chef at a popular restaurant, Deborah is a former businesswoman turned stay-at-home mother, and Evelyn is a former jazz singer. Flor, who speaks very little English, does not mention that she has a daughter, Cristina (Shelbie Bruce). John, Evelyn, Georgie and Bernice are likeable people; Deborah, however, is uptight and unfriendly, her behavior often upsetting both households.

Summer comes and Flor is needed 24/7 at the Claskys summer home. Unable to communicate well in English, Deborah finds a neighbor to interpret. Flor admits she is unable to maintain these hours because she has a daughter, so Cristina is invited to come stay with them, acting as interpreter for her mother. She impresses Deborah, who begins to treat her like a daughter, taking Cristina shopping, getting her hair done, enrolling her in a private school, and showing her more love than she does to the sensitive Bernice.

Cristinas personality begins to be increasingly influenced by Deborah, upsetting Flor, who wants Cristina to keep in touch with her Mexican roots and working-class values, and feels that Deborah is overstepping her bounds. Flor voices her objections to John, who apologizes.  She is further upset upon discovering that John has given Cristina over $600 in cash for a minor task. She threatens to leave, but John convinces her to stay for Cristinas sake.

Flor begins to learn English so she can communicate better. She becomes closer to John, who is having difficulty with Deborahs self-centered behavior. The now-sober Evelyn realizes that her daughter is having an affair and that her marriage is in trouble. She pleads with Deborah to end the affair, telling her shell never get another man as good as John. Deborah confesses to John that she cheated on him; John walks out and gives Flor a ride in his car. They go to his restaurant, where he cooks for Flor and they enjoy the "conversation of their lives," feeling love for one another.

Flor quits and takes her daughter home, upsetting Cristina, who got along well with the Claskys. On their way home, she tells Cristina that she cant go to the private school anymore, upsetting Cristina even more; she screams in the middle of the street that Flor cant do this to her and that her life is ruined. Flor loses patience with Cristina after she asks her mother for space. Flor explains to her daughter that she must answer the most important question of her life, at a very young age: "Is what you want for yourself to become someone very different than me?" Cristina considers this on their bus ride home, and they make up and embrace.

Years later Cristina as an adult, acknowledges that her life rests firmly and happily on the simple fact that she is her mothers daughter.

==Cast==
* Adam Sandler as John Clasky. Brooks cast him after seeing his more dramatic performance in Punch-Drunk Love.   
* Paz Vega as Flor Moreno. Vega could not speak English when filming began and a translator was on set during filming so that she could communicate with the director. 
* Téa Leoni as Deborah Clasky
* Cloris Leachman as Evelyn Wright. Leachman replaced Anne Bancroft who dropped out of the part after four weeks of shooting due to illness. 
* Shelbie Bruce as Cristina Moreno, age 12
* Sarah Steele as Bernice "Bernie" Clasky
* Ian Hyland as George "Georgie" Clasky
* Victoria Luna as Cristina Moreno, age 6
* Cecilia Suárez as Monica
* Aimee Garcia as the narrator (Adult Cristina Moreno)
* Sarah Hyland as Sleepover Girl
* Thomas Haden Church as Mike the Realtor

==Reception==
===Critical response===
The film received mixed reviews. Based on 165 reviews collected by the film review aggregator Rotten Tomatoes, 53% of critics gave Spanglish a positive review.  Its proponents claim it is a moving portrayal of the difficulty of family problems and self-identity (and perhaps to a lesser extent the difficulties and rewards of cross-cultural communication). However, its detractors described it as "uneven", "awkward", and "mean-spirited".

===Accolades===
   Best Original Score. Best Supporting Actress.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 