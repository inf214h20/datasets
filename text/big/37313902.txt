Least Among Saints
{{Infobox film
| name           = Least Among Saints
| image          = Least among saints.jpg
| alt            =
| caption        = Theatrical release poster Martin Papazian
| producer       = Kurt David Anderson James G. Hirsch Robert Papazian Mary Vernieu
| writer         = Martin Papazian
| starring       = Martin Papazian Tristan Lake Leabu Laura San Giacomo Charles S. Dutton
| music          = Gary Lionelli
| cinematography = Guy Skinner
| editing        = Robert Florio	 
| studio         = LA Saints Production
| distributor    = Brainstorm Media Vertical Entertainment (U.S. DVD)   
| released       = 12 October 2012 2 July 2013 (DVD) 	
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $1.5 million
| gross          = $28,026 
}}
Least Among Saints is a 2012 film written by, directed by, and starring Martin Papazian.

==Synopsis== Marine who has just returned to his home in Arizona after a tour abroad. He suffers from posttraumatic stress disorder, frequently waking up in the middle of the night with nightmares. His ex-wife  Jenny (Anderson) has filed a restraining order against him, and he has had brush-ins with the law. He befriends his neighbor Cheryl (Cook), whom he defends from her boyfriend. Cheryls son Wade (Leabu) looks up to Anthony, and the two become friends. 
 University Medical Center to try and help her, but she dies. A social services worker named Jolene (San Giacomo) agrees to let Anthony take care of Wade while they find him a foster family. Anthony encourages Wade to beat up a bully and restrains a teacher who tries to intervene, which brings in police officer George (Dutton) to find them after they go on a mission to find Wades real father. 

Wade accidentally kills Anthonys dog and Anthony is arrested on numerous charges.  Wade goes into foster care. Anthony agrees to Jolenes demands to take care of himself and she finally allows him to spend time with Wade.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Martin Papazian || Anthony 
|-
| Tristan Lake Leabu || Wade
|-
| Laura San Giacomo || Jolene
|-
| Azura Skye || May 
|-
| Audrey Marie Anderson || Jenny
|-
| A.J. Cook || Cheryl
|-
| Charles S. Dutton || George
|-
| Taylor Kinney || Jessie
|-
| Lombardo Boyar || Armando
|-
| Kari Nissena || Beth
|-
| Max Charles || Dylan
|-
| Ronnie Gene Blevins || Ronnie
|}

==Critical reception==
Aggregate reviewer website Metacritic gave the film a 32 out of 100 based on 7 reviews, indicating "generally unfavorable" reviews. 

Anthony Barker of Daily Variety:
 

Simon Abrams of The Village Voice:
 

Chuck Bowen of Slant Magazine hated the film:
 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 