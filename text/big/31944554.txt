Whoops (film)
{{Infobox film
| name           = Whoops
| image          = 
| caption        = 
| director       = Gyula Maár
| producer       = Sándor Simó
| writer         = Gyula Maár
| starring       = Dezső Garas
| music          = 
| cinematography = János Vecsernyés
| editing        = Zsuzsa Jámbor
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Whoops ( ) is a 1993 Hungarian comedy film directed by Gyula Maár. It was entered into the 43rd Berlin International Film Festival.   

==Cast==
* Dezső Garas as Ede
* Mari Törőcsik as Kati
* Kati Lázár as Elvira
* István Avar (actor)|István Avar as Jenő
* Gábor Máté (actor)|Gábor Máté as Külföldi vőlegényjelölt
* Gábor Nagy (actor)|Gábor Nagy as Kati apja
* Andrea Söptei as Ede és Kati lánya
* Zoltán Varga (actor)|Zoltán Varga
* Olga Beregszászi as Jelena
* János Derzsi as Gabika
* Erzsi Pártos as Mama

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 