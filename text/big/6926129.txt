Quebec (1951 film)
{{Infobox film
| name = Quebec
| Name = Quebec
| image	=	Quebec FilmPoster.jpeg
| starring = John Drew Barrymore Corinne Calvet Barbara Rush Patric Knowles John Hoyt 
| director = George Templeton
| writer = Alan Le May
| producer = Alan Le May
| cinematography = W. Howard Greene
| editing        = Jack Ogilvie
| distributor = Paramount Pictures
| released =  
| runtime        = 85 minutes
| country        = United States
| language = English
| music          = Edward H. Plumb Nathan Van Cleave
}} Patriotes Rebellion. The popular uprising sought to make Lower Canada, now Quebec, a republic independent from the British Empire, and happened around the same time as a similar revolt in Upper Canada, now Ontario.

== Film ==
The film was actually shot on location in Quebec, in the fashion typical of  , Montmorency Falls and the Quebec countryside.  It also cast local actors.  Quebec also features Patsy Ruth Miller, a former silent-screen star making her first screen appearance since 1931.  
 British governor of the province. Lafleur ultimately sacrifices herself to prevent Douglas from being taken hostage. The climax of the movie depicts a military assault on the British fortress.

== Cast ==
*John Drew Barrymore as Mark Douglas  
*Corinne Calvet as Mademoiselle Stephanie Durossac / Lafleur 
*Barbara Rush as Madelon 
*Patric Knowles as Charles Douglas 
*John Hoyt as Father Antoine

== See also ==
*Patriote movement
*Quebec nationalism
*Quebec independence movement
*History of Quebec
*Timeline of Quebec history

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 