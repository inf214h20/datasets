The Reluctant Sadist
 
{{Infobox film
| name           = The Reluctant Sadist
| image          = The Reluctant Sadist 1967 Mac Ahlberg Peer Guldbrandsen poster Kurt Wenzel.jpg
| caption        = Poster by Kurt Wenzel
| director       = Mac Ahlberg Peer Guldbrandsen
| producer       = Palle Schnedler-Sørensen
| writer         = Peer Guldbrandsen
| starring       = Gabriel Axel Chopin Johann Johan Strauss Sven Gyldmark
| cinematography = Mac Ahlberg
| editing        = Edith Nisted Nielsen
| distributor    = Novaris Film
| released       =  
| runtime        = 101 minutes
| country        = Denmark Sweden
| language       = Danish
| budget         = 
}} 1967 Danish/Swedish comedy film directed by Mac Ahlberg and Peer Guldbrandsen, and starring Gabriel Axel in the role of Marcel de Sade.    

==Cast==
* Gabriel Axel - Marcel de Sade
* Buster Larsen - Lawyer
* Karl Stegger - Mikkelsen
* Elsa Prawitz - Mrs. Mikkelsen
* Carl Ottosen - Flyttemand
* Preben Nikolajsen - Flyttemand
*   - Mikkelsens office lady
* Bjørn Puggaard-Müller - Mikkelsens procurator
* Carl-Axel Elfving - Music professor
* Hans Brenå - Balletmester John Price - Auditor
* Tove Maës - Mrs Vibeke Poulsen
* Lise Thomsen - Office lady
* Børge Møller Grimstrup - Police officer
* Klaus Pagh - Bank clerk
* Poul Bundgaard - Bank direktor
* Preben Kaas - Publisher
* Lisbeth Lindeborg - Olga
* Hans Lindgren - Count
* Ove Sprogøe - Baron Neully du Prat
* Paul Hagen - James
* Lotte Horne - Else, manicure lady
* Joakim Rasmussen - Auktionarius
*   - Lady in bar
* Lotte Tarp - Baronesse
* Jeanne Darville - Countess
* Bente Juhl - En sagførerkone
* Simon Rosenbaum - En herre
* Ulla Johansson - Bogforlæggerens kone

==References==
 

==External links==
* 
*  at the Danish National Filmography

 
 
 
 
 
 