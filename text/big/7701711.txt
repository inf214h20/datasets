Luck by Chance
 
{{Infobox film
| name           = Luck by Chance
| image          = Luckbychance.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Zoya Akhtar
| producer       = Farhan Akhtar Ritesh Sidhwani
| story          = Zoya Akhtar
| starring       = Farhan Akhtar Konkona Sen Sharma Alyy Khan Rishi Kapoor  Dimple Kapadia  Juhi Chawla Isha Sharvani Hrithik Roshan Sanjay Kapoor
| music          = Shankar-Ehsaan-Loy
| cinematography = Carlos Catalan
| editing        = Anand Subaya
| studio         =  Excel Entertainment Big Pictures
| released       =  
| runtime        = 156 minutes
| country        = India
| language       = Hindi
| budget         =   15 crore 
| gross          =   19.50 crore 
}} John Abraham, Rani Mukherjee, and Aamir Khan in seamless cameos.

The film is about the journey of an actor who arrives in Mumbai to become a movie star. How he finds himself riding his fortune to becoming one, while struggling to sustain his relationships, forms the story.  The film was released on 30 January 2009, supported by positive reviews from critics, but failed to do well at the box office.

==Plot==
 props department.

Vikram befriends Abhimanyus neighbor, young actress Sona Mishra (Konkona Sen Sharma), with whom he eventually becomes romantically involved. Sona, the mistress of small-time producer Satish Chowdhury (Alyy Khan), who for three years has promised her a leading role in his dream project, meanwhile works in regional films and bit parts.

Sona finds out that Satish has secured financing for the new project and meets him expecting him to cast her as second heroine, but he refuses saying that they need a new face and since she has acted in many regional films and other small roles she is no longer a fresh face. She argues that she can act well but he says that is not a major criteria these days in Bollywood. Sona is crying when Satishs wife enters and asks her why she is crying. Sona answers by fabricating a lie about trouble at home to save both her and Satishs embarrassment. Sona while leaving the both of them gives photos of Vikram to Satish who shows it to his wife and who in turn shows them to Romy (Rishi Kapoor).

Vikram is shortlisted for Romys new movie. Neena (Dimple Kapadia), the mom of Nikki (Isha Sharvani)the actress cast for the movie, was a big film actress in the heyday. She sees Vikrams audition and tries to remember where has she met him before. Vikram had once approached her at a film fraternity party. Vikram is told his audition was for the lead in the film and that Neena has seen his audition. At home, he sees every movie Neena has starred in and he impresses her with his charm and knowledge about her work. Recalling past advice, Vikram successfully boosts a competing actors ego whose overconfident acting is rejected by the director. Finally, Vikram is selected by Neena, Ranjit (director of the film Sanjay Kapoor) and Romys wife (Juhi Chawla), despite Romys desire to cast the other macho, hunky actor.

On set Vikram becomes increasingly crafty and manipulative, while Nikki is smitten by him and sneaks into his room one night while her mother is asleep in an attempt to seduce him. Vikram succumbs to her advances and a secret affair starts between the two; the affair becomes stronger when Neena has to leave the set on business for a few days. Vikram tells Nikki he has no girlfriend. Meanwhile, Sona arrives at the hotel where the cast is staying to surprise Vikram. Her friend who is also working on the film warns her of a blossoming romance between Vikram and Nikki. She doesnt take heed of her friends warning but discovers the truth when she sees Vikrams unexpectedly cold behavior towards her. Distraught, she leaves the hotel. After coming back, Neena instantly recognizes that something is going on between them and makes them understand that it will be mutually beneficial not to let the press and public know of their intimacy.

Meanwhile, in Mumbai, news of the affair has leaked and a friend of Sona who works at a tabloid is assigned to prepare the article, which describes Vikram as a user who has shot to stardom by manipulating Neena and her daughter Nikki. The article also mentions details of when he was struggling to land a role and Sonas part in his life as the forgotten girlfriend. Neena yells at the magazine editor and tells Nikki to stay away from Vikram. Vikram and Nikki have a fight, and Vikram shows his frustration to Sona as he believes she is the one responsible for the article.

Eventually the film is released and it becomes a hit. Vikram rises to stardom but at the expense of his friends. At a party, he meets his idol Shah Rukh Khan, who advises him to not fall into the trappings of stardom and to always stay close to the people who stood by him when he was a nobody. He tries to get back with Sona, but she points out that he only wants to be with her because he is selfish and feels guilty. She refuses and walks out of his life. Sona soon gets good roles on television and is interviewed by a reporter for her fan following. The film ends with her telling the reporter that she is happy living the life of an independent and somewhat successful actress instead of being upset about not becoming a major movie star.

==Cast==

* Farhan Akhtar as Vikram Jaisingh
* Konkona Sen Sharma as Sona Mishra
* Rishi Kapoor as Rommy Rolly
* Dimple Kapadia as Neena Walia
* Juhi Chawla as Minty Rolly
* Sanjay Kapoor as Ranjit Rolly
* Isha Sharvani as Nikki Walia
* Hrithik Roshan as Ali Zaffar Khan
* Saurabh Shukla as Acting teacher
* Alyy Khan as Chaudhary

===Guest appearance===

Appearing as themselves:

* Aamir Khan
* Rani Mukerji
* Kareena Kapoor
* Abhishek Bachchan John Abraham
* Karan Johar
* Akshaye Khanna
* Vivek Oberoi
* Dia Mirza
* Anurag Kashyap
* Mushtaq Sheikh
* Javed Akhtar
* Shabana Azmi
* Boman Irani as Dinaz Aziz
* Manish Malhotra
* Mac Mohan
* Rajkumar Hirani
* Ronit Roy
* Manish Acharya
* Sheeba Chaddha
* Shahrukh Khan
* Ranbir Kapoor

==Production==

===Development===
Zoya Akhtar said in an interview that she wrote the first draft seven years ago while relaxing on Palolem beach in Goa. In an interview she said:
 I hand wrote it and it was some ridiculous, epic-length when I came back and transcribed it on my laptop. The first film is the easiest to write because its usually what the person knows their personal graphs, milieu and feelings. Luck By Chance is not about established actors, but those who are waiting for things to happen. Farhans character is fresh off the boat, while Konkanas does bit roles, looking for a big break.    
 Om Shanti Om.   

Javed Akhtar wrote the dialogues for the film, incorporating, as Zoya puts it, his bizarre sense of humour.  Farhan had to reportedly train to get six pack abs for the film. He was trained by Cheetah Yagnesh, who appeared on the film as Farhans characters trainer.

Filming faced lot of problems, including the unpredictable Mumbai rains, which halted a song shoot with Hrithik Roshan. The rains washed out the entire set, and eventually the entire tent began to leak. Zoya was afraid that this would lead to short-circuit, and so all the lights had to be switched off. 

According to Farhan, principal photography for the film was completed in October.   

===Casting=== Farhan to Fakir of Venice. She said in an interview, "Farhan was the perfect choice for the role in Luck By Chance because he knows the industry in and out. He is smart and bright and both of us have been working together for years".  Problems arose with the huge casting of the film. Zoya said, "I had to really think it out when I was deciding on these multiple actors that I wanted in the film. It was tough to decide especially because I wanted the right actors who could play themselves and still look believable as part of the film. Then there are other good actors like Boman Irani and Saurabh Shukla, who are playing characters in the film".  Next to be cast were her parents, Javed Akhtar and Shabana Azmi, along with such veteran stars as Rishi Kapoor and Dimple Kapadia.  Zoya said, "We could have had Shabana (Azmi) as the diva, but I needed a mainstream heroine. Only a leading lady would do. Dimple has played it edgy. Shes all warm, soft sunshine and then theres a flip and shes hard, cold, steely".  Isha Sharvani teams up again with Hrithik Roshan after appearing together in an ad.    Juhi Chawla joined the cast playing Minty, the wife of producer Rommy Rolly. 

===Promotion===
 
  Reliance BIG Entertainment Ltd, was given the task of promoting the movie. The first bit of promotion came from the music wherein winners were given signed albums by Farhan Akhtar and Konkona Sen Sharma. They promoted the whole movie across the nation in all 112 stores in 10 cities as well to the international audiences through its video-on-demand (VOD) site. The pre-release promotional activities included online marketing and publicity of the film’s music videos, trailers, downloads, previews, preview shows, contests and continue with other promotional activities post-release like meet-and-greet events with the star cast at select cities.
 Big FM radio station and Godrej Group|Godrej. One passenger would be interviewed live by an RJ and go on air with his experience of getting lucky and enjoying the free ride. The Indian gaming portal Zapak|Zapak.com created a microsite for the movie with a game, "Luck by Chance - Lucky Break".

==Release==
 
The movie was initially scheduled to be released 23 January 2009, alongside Raaz - The Mystery Continues, but was pushed to 30 January. Luck by Chance was released on 900 screens worldwide in 27 countries. Of this, over 700 screens are in India and the rest in such overseas markets as the United States, Canada, UK, UAE, Australia, South Africa, and New Zealand.

==Reception==

===Critical reception===
The film was well received by critics, Anupama Chopra of NDTV wrote, "Zoya pokes fun at Bollywood but she does it with a great affection. There are some lovely little moments like the star daughter in a super short skin-tight outfit struggling to touch her producers feet without splitting a seam. But what makes Luck By Chance compelling is the layers beneath the laughs. Though the first half wobbles precariously as the script struggles to find a momentum, but thankfully the narrative flows better in the latter half and culminates in a satisfying, bittersweet end".  In the review at UAE Daily the United Arab Emirates reviewer said, "One of the prime reasons why Luck By Chance works is because   the writing.   Right from the characters, to the individualistic scenes, to the way Zoya puts them in a sequence, Luck By Chance is easily one of the most cohesive scripts this side of the Atlantic. If Zoyas writing is superb, her execution of the written material deserves distinction marks".  The Times of India said, "Luck By Chance highlights how the film industry give regards to everything else but the story when making a movie and ironically weaves a fascinating story using that paradox".  Noyon Jyoti Parasara of AOL|AOL.in cited some drawbacks and said, "Luck By Chance provides you some smiles throughout the movie. However, it fails to leave you with the content smile that a feel-good movie ideally aims to do." 
 The Player seriocomic satire of the Bollywood film industry by a first-time director whose collaring of over a dozen major Hindi stars for cameos speaks well of its biting accuracy",  while Kyle Smith of the New York Post found it "gently humorous" and "almost totally lacking in the cynicism, self-hatred and ennui that characterizes inside-Hollywood flicks". 

===Box office performance===
The movie had a slow start at the box office and registered 25% to 30% attendance at the plexes.  According to trade analyst and critic Joginder Tuteja, the occupancy at the theatres came down to 50 percent.  Outside India also, the movie failed to perform per expectations.

In the US, Luck by Chance debuted at number 32. In its opening weekend, it collected $217,439 (approximately Rs. 1.06 crores) on 61 screens, a per-screen average of $3,556. In the UK, Luck By Chance debuted at number 21 and collected £73,822 (approximately Rs. 50.95 lacs) on 50 screens, with a per-screen average of £1,476. The movie did poorly in Australia and was a flop in India, grossing $3,914,500 at the box office.    Worldwide, the film grossed $4,504,365. 

==Soundtrack==

{{Infobox album  
| Name        = Luck By Chance
| Type        = soundtrack
| Artist      = Shankar-Ehsaan-Loy
| Cover       = Luck By Chance Album Cover.jpg
| Released    =  30 December 2008
| Recorded    = Feature film soundtrack
| Length      = 34:31
| Lyrics =Javed Akhtar
| Label       = Big Music
| Producer    = Farhan Akhtar & Ritesh Sidhwani
| Reviews     =
| Last album  = Chandni Chowk to China   (2008)
| This album  = Luck by Chance   (2008)
| Next album  = Konchem Ishtam Konchem Kashtam   (2009)
}}

Luck By Chance is the soundtrack album to this film. The music and score is composed by Shankar-Ehsaan-Loy, while the lyrics are penned by Javed Akhtar.

===Development=== raag Bhairavi Shankar sang drums and Rajasthani folk fusion where romantic song Rock On!!.   

===Promos=== Russian circus acts. Following "Baawre" was the video for the song "Sapnon Se Bhare Naina", showing Farhan and Konkona facing their trials and tribulations in the film industry. "Yeh Zindagi Bhi" was the last video released.

===Release===
The music of Luck By Chance was released on 30 December 2008. It had an early release through iTunes on 23 December 2008.  The promotion was done keeping the digital platform in mind. The music was made available directly and through partner sponsors on mobile, Internet and physical markets.   Retrieved on 2009-01-22 

===Track listing===
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration !! Notes
|-
| 1
| Yeh Zindagi Bhi
| Loy Mendonsa, Shekhar Ravjiani
| 6:39
| Opening Credits of the Film
|-
| 2
| Baawre Rajasthani ensemble
| 5:13
| Picturised on Hrithik Roshan, Farhan Akhtar, Isha Sharvani and Konkona Sen Sharma
|-
| 3
| Pyaar Ki Dastaan
| Amit Paul, Mahalakshmi Iyer
| 5:21
| Picturised on Isha Sharvani, Farhan Akhtar & Konkona Sen Sharma
|-
| 4
| Yeh Aaj Kya Ho Gaya
| Sunidhi Chauhan
| 3:25
| Picturised on Konkona Sen Sharma
|-
| 5
| Sapnon Se Bhare Naina
| Shankar Mahadevan
| 4:59
| Picturised on Farhan Akhtar
|-
| 6
| O Rahi Re
| Shankar Mahadevan
| 4:27
| Ending Credits of the Film with Konkona Sen Sharma
|-
| 7
| Baawre (Remix)
| Loy Mendonsa, Shankar Mahadevan
|4:27
| Promotional Song
|}

===Reception===
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =     
| rev2 = Rediff.com
| rev2Score =    MSN India
| rev3Score =   
| rev4 =  Hindustan Times
| rev4Score =   
| rev5 =  Planet Bollywood
| rev5Score =   
| rev6 =  GlamSham
| rev6Score =     
| rev7 =  ITunes
| rev7Score =     
}}
  MSN India Rajasthani ensemble in "Baawre" or the dark, fast-paced "Sapnon se bhare naina", which are reason enough to buy the soundtrack."  NDTV said, "On the whole, Luck By Chance stands for quality like most of the albums by Shankar-Ehsaan-Loy. A perfect soundtrack with medium-paced songs, great singing and hummable lyrics, the album is a sure shot hit". 

==References==
 

==External links==
* 
*  - Excel Entertainment
* 
* 
* 

 

 
 
 
 
 