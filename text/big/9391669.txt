Gambler's Choice
{{Infobox film
| name           = Gamblers Choice
| image_size     =
| image	         = Gamblers Choice FilmPoster.jpeg
| caption        = Frank McDonald
| producer       = William H. Pine William C. Thomas
| writer         = James Edward Grant (story) Irving Reis (screenplay) Howard Emmett Rogers (story) Maxwell Shane (screenplay)
| narrator       =
| starring       = Chester Morris Nancy Kelly
| music          = Mort Glickman
| cinematography = Fred Jackman Jr.
| editing        = Howard A. Smith
| distributor    = Pine-Thomas Productions
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
}}
 Frank McDonald and starring Chester Morris and Nancy Kelly.

==Plot==
 
The year is 1896, and the three young friends Ross Hadley, Mary Hayes and Michael McGlennon play in the streets of the Tenderloin District in New York, and they are totally oblivious to what surrounds them.

When Ross one day performs an act of delinquency by stealing a wallet, all of them get arrested. Since Ross has a history of delinquency, he is sent to reform school to be straightened out. The sweet, innocent Mary is released to her father, an alcoholic by the name of Ulysses S. Hayes, who plans to escape to the West. Mike’s father is one of New York’s finest, and he swears on his honor as a policeman that he will punish his son for what he did.

Many years pass, and we return to the story in 1911. Mike is working as a policeman, just like his father. Ross is working as a croupier at Chappie Wilsons gambling joint, but is about to quit to open his own casino. Ross encourages the affection he is shown by a woman named Faye Lawrence, who is the former wife of a wealthy bookie. His motives are to get her to give him some money for his club.

While working at his table, Ross happens to see the new burlesque dancer, Vi ”The Garter Girl” Parker, perform on stage. He recoginizes the woman as his old childhood friend Mary.

Mary is happy to be reunited with her old friends Ross and Mike. Ross indeed opens his own caisson, and after Mary gets into a fight with the management at Chappie’s, she comes to work as the featured entertainer at Ross’s casino instead. Mike shows his loyalty to his friend Ross, by pretending not to see when he pays off the city officials to get a license for the casino.

Faye becomes jealous of the attractive Mary, who gets so much of Ross’s attention, and tries to fire her. Ross, who is more interested in keeping Mary around, pays Faye back with the earnings from his new business and turns her out of the business. Mike and Mary then falls in love with each other, but she is still set on having a successful career as an entertainer, and is not interested in marrying him and start a family. Mary refuses to end p like her loser father, and Mike realizes that Mary is more similar to Ross than himself.

Ross gets into politics and in the process befriends a man called John MacGrady. John has a tendency towards greed, and when Ross doesn’t want to play along with John’s plans, his political career is ruined. He starts to scheme to get Mike promoted to Captain instead, but Mike is reluctant to accept the help of his ill-reputed friend, since it could cause him harm further on, Eventually Mike budges and accepts the nomination, after Ross has promised him there will be no strings attached.

Faye finds a new ally in Chappie, Ross’ and Mary’s former boss, and together they concoct a scheme to get rid of Ross. However, the attempt to take Ross’ life fails, and instead a policeman is murdered. Mike, who is now a Captain, responds by closing every casino in the Tenderloin area. He even goes so far as to raid Ross’ casino, and angers his friend. Ross considers Mike’s behavior and actions disloyal. As revenge, Ross pulls some strings within the force and gets Mike demoted to being a patrolling policeman again.

Mike responds in his turn, by waging another war against the gambling joints, and he is promoted to special investigator of the Citizen’s Anti-Crime Committee by the city prosecutor, Thomas J. Dennis. Chappie concocts a new plan to discredit and get rid of Mike, and uses Marys father as an unsuspecting pawn in a sly con game, set up by a man called Yellow Gloves Weldon. Chappie then sends a phony "tourist" to Mike to complain that he has been taken for $10,000 by Mary’s father.

Mike decides to arrest the perpetrator, but Mary pleads for her father’s mercy. Mike promises Weldon that he will not press charges if Weldon agrees to return the money. Unknown to Mike, the money is marked and he is photographed when he returns it to the "tourist."

Ross right-hand man, Benny, hears about the double-cross and warns Ross that Mike is being framed by Chappie and Weldon. By now Ross has also fallen in love with Mary, and he was planning to elope with her for Florida. When Ross hears about the set up for Mike, his loyalty compels him to come to the rescueof his old friend. Ross steals the evidence against Mike from Chappies office, but unfortunately he is caught red-handed by Chappie. Ross manages to escape from chappie, but he is shot and wounded in a gunfight, where he ultimately kills Chappie.

When Mike investigates the scene of shootout, he finds a mouth harp like the one Ross has played since childhood. He realizes that his friend is involved in the killing of Chappie, and reluctantly goes to arrest his friend. Meanwhile Ross, who has given Mary the evidence against Mike, insists that he should go alone to Florida. However, Ross is mortally wounded by the gunshot, and dies in front of his two old friends in the streets of the Tenderloin. Some time later, Mike returns to foot patrol after he and Mary are married. 

==Cast==
*Chester Morris as Ross Hadley
*Nancy Kelly as Mary Hayes aka Vi Parker
*Russell Hayden as Mike McGlennon Lee Patrick as Fay Lawrence
*Lloyd Corrigan as Ulysses S. Rogers
*Sheldon Leonard as Chappie Wilson
*Lyle Talbot as Yellow Gloves Weldon
*Maxine Lewis as Bonnie DArcy
*Tom Dugan as Benny
*Charles Arnt as John McGrady Billy Nelson as Danny May

==Soundtrack==
* Nancy Kelly - "The Sidewalks of New York" (Music by Charles Lawlor, Lyrics by James W. Blake)
* Nancy Kelly - "Hold Me Just a Little Closer" (Music by Albert von Tilzer, Lyrics by Benjamin Barnett)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 