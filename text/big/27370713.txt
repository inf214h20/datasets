Woman of Desire
{{Infobox Film
| name           = Woman of Desire
| image          =
| image_size     = 
| caption        = 
| director       = Robert Ginty
| producer       =  Danny Lerner
| writer         =  Robert Ginty Anthony Palmer
| narrator       = 
| starring       = Bo Derek Robert Mitchum
| music          = 
| cinematography =  
| editing        =  
| studio         = 
| distributor    =  
| released       = 1993
| runtime        =  96 min 
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
Woman of Desire is a 1993 film starring Bo Derek and Robert Mitchum, directed by Robert Ginty.

==Plot==
 
Two men &ndash; Jack Lynch (Jeff Fahey) and Jonathan Ashby (Steven Bauer) &ndash; are confronted by a beautiful woman, Christina Ford (Bo Derek).

==Starring==
*Bo Derek as Christina Ford
*Robert Mitchum as Walter J. Hill
*Jeff Fahey as Jack Lynch
*Steven Bauer as  Jonathan Ashby Thomas Hall as  Norman Landis
*Todd Jensen as  Wendell Huston

==References==
*Entertainment Celebrities by Norbert B. Laufenberg
* 

 
 
 
 


 