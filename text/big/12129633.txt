Ginostra
 
{{Infobox Film name           = Ginostra caption        =
| image	=	Ginostra FilmPoster.jpeg director       = Manuel Pradal producer       = Jean Francois Fonlupt writer         = Manuel Pradal starring       = Harvey Keitel Andie MacDowell Mattia de Martino music          = Carlo Crivelli  cinematography =  Maurizio Calvesi  editing        = Valerie Desiene distributor    =  released  2002
|runtime        = 135 min. country        = France language  English
|budget         = 
}}
 2002 crime fiction film written and directed by Manuel Pradal, starring Harvey Keitel (Matt Benson), Andie MacDowell (Jessie). The story is about an FBI officer (Harvey Keitel) investigating the murder of a would-be informant, attempts to contact the only person who knows the truth behind the killings - the dead mans eleven-year-old son.

As the film opens, we learn that Benson, a New Yorker, has been entrusted with protecting the 11-year old boy. The style of the film is intentionally ambiguous, and details about only obtained through the dialogue between the main characters, in many cases, in a way that only makes sense once later scenes are viewed. The boy, as is revealed later, is with Benson because his father had appealed for protection from the authorities in return for his testimony against a local mobster, Manzella. The title of the film refers to the location of the main characters, who live on an island that is remote enough from the mainland that fresh water must be shipped in. Also on the island is a mysterious convent of nuns who make a living producing what appears to be wine and food seasonings, mainly using the local plants. The convent is located high in the mountains, and takes its heat from the active volcano on the island.

Benson finds that his efforts to get the child to talk to him about the murder of his father, who was to be under protection at the time that he was killed (the plot does not reveal when the killing took place) are frustrated by the boys strong desire to exact his own revenge. From the actions of Manzella and others, it becomes apparent that the boy was either present when his father was killed, or knows details relevant to the murder. Much of the story revolves around a cat-and-mouse style chase between Manzella and the boy. All the while, Benson must deal with the local police, who have themselves been infiltrated by Manzellas organization (this, we learn, was how the boys father was betrayed).

Romantic tension in the story is provided by Andie MacDowells character, Jessie. She has come to stay on the island with Benson, and their young daughter. Because of the secretive nature of the case, however, there is very little that Benson can reveal to Jessie about their reason for being in Italy, and this begins to place a strain on their marriage. Jessie regards the boy as her own, and attempts to find things to occupy his time while her husband and the local police are  investigating Manzella. About midway through the story, we learn from the local detective, Giovanni Gigli, that the authorities are powerless to arrest Manzella, because he has the sympathy of those in the community of Naples, and it would be too dangerous to simply arrive in daylight with a warrant.

==Cast==
* Harvey Keitel: Matt Benson 
* Andie MacDowell: Jessie Benson 
* Francesca Neri: Helena Gigli 
* Harry Dean Stanton: Del Piero 
* Asia Argento: Nun
* Mattia De Martino: Ettore Greco 
* Stefano Dionisi: Giovanni Gigli 
* Luigi Maria Burruano: Uncle of Ettore  
* Violante Placido: Nurse
* Veronica Lazar: Susanna Del Piero 
* Angela Goodwin: Mother Superior
* Marino Masé: Night Watchman

==External links==
*  
*  
*  

 
 
 