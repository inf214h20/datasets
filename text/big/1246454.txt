Born Innocent (film)
{{Infobox television film
| name = Born Innocent
| image = BornInnocent.jpg
| caption = "Born Innocent" DVD cover
| genre = Drama
| writer = Book:  
| starring = Linda Blair Richard Jaeckel Kim Hunter|
| director = Donald Wrye 
| producer = Robert W. Christiansen (executive producer) Bruce Cohn Curtis (producer) Rick Rosenberg (executive producer)
| editing = Maury Winetrobe
| cinematography = David M. Walsh
| music = Fred Karlin Tomorrow Entertainment
| distributor = NBC
| released = September 10, 1974
| runtime = 98 minutes 
| network = NBC
| country = United States
| language = English  
| budget = 
}}
Born Innocent is a 1974 American television movie which was first aired under the NBC World Premiere Movie umbrella on September 10, 1974.  Highly publicized and controversial, Born Innocent was the highest-rated television movie to air in the United States in 1974. The movie deals with the physical, psychological and sexual abuse of a teenage girl, and included graphic content never before seen on American television at that time.

==Plot==
Linda Blair stars as a 14-year-old runaway who, after getting arrested one too many times, is sentenced to do time in a girls juvenile detention center, which doubles as a reform school for the girls. Blairs character, Christine Parker, comes from an abusive home. Her father (played by Richard Jaeckel) beat her on a regular basis, which caused Chris to run away many times. Her mother (Kim Hunter) is just as troubled as Chris is; unfeeling, sitting in her recliner, watching television and smoking cigarettes all day, and in denial as to what her husband is doing. While the movie had a morality play tone, calling attention to the harsh conditions of juvenile detention centers, it also blamed society for Christines downfall.

All through the movie, Chris social worker never realizes that her parents caused her to run away and the juvenile justice system focused all the blame and punishment on Christine for her bad behavior.  With the exception of one dedicated counselor (played by Joanna Miles), the reform school personnel were mostly apathetic and allowed an unhealthy, destructive culture to fester in the school.  By the very end of the movie, despite the counselors best efforts to help Christine talk about her problems, she watched helplessly as an innocent, decent girl is transformed into a violent, pathological, manipulative and cold person, no longer having any guilt or remorse for her actions and will most likely become an adult criminal.

==Cast==
* Linda Blair as Chris Parker
* Joanna Miles as Counselor Barbara Clark
* Allyn Ann McLerie as Emma Lasko Mary Murphy as Miss Murphy
* Janit Baldwin as Denny
* Nora Heflin as Moco
* Tina Andrews as Josie
* Sandra Ego as Janet
* Mitch Vogel as Tom Parker
* Richard Jaeckel as Mr. Parker
* Kim Hunter as Mrs. Parker
* Adrienne White as Ria
* Janice Lorraine Garcia as Child at funeral

==Rape scene==
One scene in particular that gained the movie infamy was the rape of Blairs character in the communal showers by a girl gang led by lesbian Moco (Nora Heflin) and Denny (Janit Baldwin) with a plunger handle; this scene had the distinction of being the first all-female rape scene aired on American television. This scene was not glossed over in promotional spots for the movie; Linda Blairs screams as she was being attacked were aired in the promos, with the announcer intoning, "She was born innocent, but that was fourteen years ago!"

The scene drew much outcry on its first airing and was eventually pulled from the movie entirely when it was blamed for the rape of a nine-year-old girl, committed by some of her peers with a glass soda pop bottle.  The California Supreme Court would declare the film was not obscene, and that the network which broadcast it was not liable for the actions of the persons who committed the crime.  Olivia N. v. National Broadcasting Company,  126 Cal. App.3d 488 (1981).

Linda Blair has gone on record saying she felt violated filming the rape sequence. "You go through all the motions of being raped and you come out of it feeling...pretty raped. And, once again, theres no one there for you (as with the rape scene in The Exorcist). That didnt sit well with me at all."

==Post original airings==
In a response to the incident, re-airings in the late 1970s and 1980s did not air any of the rape sequence. The real-life rape, in part, helped establish the Family Viewing Hour which became briefly mandatory for the networks in the late 1970s, as the movie was aired at 8PM or 9PM (depending on time zone), when some children may not have been in bed.

==VHS and DVD release==
After the edited re-airings in the 1980s, the uncut version appeared on VHS in numerous budget-priced editions. In 2004, VCI Entertainment released Born Innocent on DVD with the rape scene included.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 