Bhoomi Geetham
{{Infobox film
| name           = Bhoomigeetham
| image          =
| image_size     =
| caption        = Kamal
| producer       = Pavamani
| writer         = TA Razak
| screenplay     = TA Razak Murali Geetha Geetha Jagathy Sreekumar KPAC Lalitha
| music          = Ouseppachan
| cinematography = K. Ramachandrababu
| editing        = K. Rajagopal
| studio         = Silver Screen International
| distributor    = Silver Screen International
| released       =  
| country        = India Malayalam
}}
 1993 Cinema Indian Malayalam Malayalam film, Kamal and produced by Pavamani. The film stars Murali (Malayalam actor)|Murali, Geetha (actress)|Geetha, Jagathy Sreekumar and KPAC Lalitha in lead roles. The film had musical score by Ouseppachan.   

==Cast==
  Murali
*Geetha Geetha
*Jagathy Sreekumar
*KPAC Lalitha
*Nedumudi Venu
*Kunjunni Mash
*Kaveri
*N. F. Varghese
*T. G. Ravi Seetha
 

==Soundtrack==
The music was composed by Ouseppachan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme Nila Devi (F) || KS Chithra || P Bhaskaran ||
|-
| 2 || Amme Nilaadevi || K. J. Yesudas || P Bhaskaran ||
|-
| 3 || Chakravaalangal Nadungi || K. J. Yesudas || P Bhaskaran ||
|-
| 4 || Parayoo Nee Hridayame || KS Chithra, Murali || ONV Kurup ||
|}

==References==
 

==External links==
*  

 
 
 


 