Danger (film)
{{Infobox film
| name           = Danger
| image          = Danger Telugu Film.JPG
| caption        = Film poster Krishna Vamsi
| producer       = Sunkara Madhu Murali
| story          = Krishna Vamsi
| screenplay     = Krishna Vamsi
| writer         = Uttej  (dialogues)  Swathi Sherin|Shireen
| music          = Joshua Sridhar
| cinematography = Om Prakash
| editing        = Lokesh
| studio         = Karthikeya Creations
| distributor    = 
| released       =  
| runtime        = 186 minutes
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}
 Krishna Vamsi, Swathi and Shireen in lead roles.  The film soundtrack and background score were composed by Joshua Sridhar. Dialogues for the film were written by popular actor Uttej. The film was released on October 29, 2005. 

==Plot==
Karthik (Sairam Shankar), Satya (Allari Naresh), Ali (Abhishek), Lakshmi (Swathi) and Radhika Reddy (Shireen) are childhood friends. They set out to a farmhouse for a party to celebrate the prospective marriage of Lakshmi. On the way they collide with a police vehicle and speed away to the party happening in the outskirts of the city. The policemen in the vehicle are badly hurt and one of them takes note of the car number which hit them. The police raid the party in search of the owners of the car. Panic raises among the crowd and the group consisting of the five friends escape from the party and in process of avoiding the chasing cops, they get lost in the Vikarabad forest. There they tumble upon a ghastly incident where a man is taking the live of a toddler as part a sacrificial ritual and black magic. Ali shoots the entire episode with his video camera. It is revealed that the man (Siva Prasad) who killed the toddler is Gattayya, a politician who believes that using black magic can make him successful. The group gets discovered by the politician while filming the ritual. With the goons of the politician chasing them, the group escapes from there. A corrupt cop Raju Nayak (Satya Prakash) is hired by the politician to search for the group. The group cannot return to their homes because the police are still looking for them at their respective houses. The group struggles with the situation and finally emerge victorious when they upload the video that was recorded to the internet. Raju Nayak is killed by the Commissioner of Police and Gattayya is arrested.

== Cast ==
* Allari Naresh as Satya
* Sairam Shankar as Karthik
* Abhishek as Ali Shireen as Radhika reddy   Swathi as Lakshmi  Siva Prasad as Gattayya
* Satya Prakash as Raju Nayak, Police Circle Inspector
* Brahmanandam as Brahmam, a driving school instructor
* Shafi as Gattayyas brother
* Harsha vardhan as Rajesh, Inspector Hema as Lakshmis Mother
* Krishna Bhagavaan as Doctor
* Ahuti Prasad as Police Commissioner
* Ravi Prakash as Ravi (Lakshmis fiance)
* Kota Shankar Rao as Karthiks father
* Kavitha as Karthiks mother
* Lakshmipati as Karthiks uncle

==Production== Colours Swathi into films.

==Reception==
The film released on October 29, 2005 for mostly negative reviews. Jeevi from Idlebrain.com gave the film  2.75 of 5 rating and said that Krishna Vamsi must be appreciated for attempting a different film, but it is of no use if the film does not make a mark for itself. Jalapathy of TeluguCinema.com reviewed the film and  said that the film is a time pass one but on the whole a disappointing film.

==Awards an Nominations==
;Nandi Awards  Siva Prasad

==References==
 

== External links ==
*  
*  

 

 
 
 
 