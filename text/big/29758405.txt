Eyes of Texas (film)
 
{{Infobox film
| name           = Eyes of Texas
| image          =
| image_size     =
| caption        =
| director       = William Witney
| producer       = Edward J. White (associate producer)
| writer         = Sloan Nibley (writer)
| narrator       =
| starring       = See below
| music          = R. Dale Butts
| cinematography = Jack A. Marta
| editing        = Tony Martinelli
| distributor    =
| released       = 15 July 1948
| runtime        = 70 minutes (original version) 54 minutes (edited version)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Eyes of Texas is a 1948 American film directed by William Witney.

== Cast ==
*Roy Rogers as U.S. Marshal Roy Rogers
*Trigger as Trigger, Roys Horse
*Lynne Roberts as Nurse Penny Thatcher
*Andy Devine as Dr. Cookie Bullfincher
*Nana Bryant as Hattie E. Waters, Attorney
*Roy Barcroft as Hatties Henchman Vic Rabin
*Danny Morton as Frank Dennis / Frank Cameron Francis Ford as Thad Cameron
*Pascale Perry as Pete - Henchman with whip
*Stanley Blystone as Sheriff
*Bob Nolan as Bob
*Sons of the Pioneers as Musicians

== Soundtrack == Tim Spencer)
*Roy Rogers and the Sons of the Pioneers - "Padre of Old San Antone" (Written by Tim Spencer)
*The Sons of the Pioneers - "Graveyard Filler of the West" (Written by Tim Spencer)

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 