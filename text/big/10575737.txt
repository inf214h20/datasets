Crossroads (1937 film)
{{Infobox film
| image          = Poster of the film Cross Roads 1937 China.jpg
| caption        =
| name           = Crossroads
| writer         = Shen Xiling Bai Yang
| director       = Shen Xiling
| cinematography = Zhou Shimu Wang Yuru
| producer       = 
| studio         = Mingxing Film Company
| distributor    =
| runtime        = 110 minutes
| released       =  
| country        = China
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 十字街头
| fanti          = 十字街頭
| pinyin         = Shízì jiētóu}}
}} 1937 Chinese Bai Yang Sun Yu. 
	
Crossroads has its background in the Great Depression of the 1930s.  Produced by Mingxing Film Company, the film also represented an expansion by Mingxing into the leftist film market that had been dominated by its rival Lianhua Film Company, due to a flagging financial situation. {{cite web | url = http://asiapacifictriennial.com/cinema/hong_kong,_shanghai_cinema_cities/political_landscapes archiveurl = archivedate = 2007-05-22}} 

In 2001, a sequel was made, despite the half-century gap, entitled "New Crossroads".

==Plot==
The film begins at a dock at Shanghai, where college graduate Xu is contemplating suicide because he cannot find a job.  His friend Zhao (Zhao Dan) stops him, and leads him back to their rented apartment.  We learn that the four graduate friends are all jobless. Zhao serenades to Xu and dissuades him from thoughts of suicide, asking him to concentrate on his freelance translation job instead.  Xu tells Zhao he has decided to sell his degree in order to raise funds to see his mother in the countryside.  His other friend Tang comes to celebrate his own birthday with a fourth friend, Liu.

A female college graduate Yang (Bai Yang) comes to Shanghai to look for a job, and rents the room beside Zhaos.  Meanwhile, Zhao finds a job at the press.  Jubilant, he washes his laundry and hangs them up on a pole that extends into his neighbours room.  Yang is napping, and the wet laundry drips her pillow damp.  Yangs friend comes in and bangs a few nails for her to hang her clothes, and Zhaos framed photos fall off at the opposite side of the wall. 

Zhao comes home from work in the morning, while Yang goes to work as a trainer at a textile factory.  Zhao leaves a bag in the tram he is alighting, but fortunately Yang helps retrieve it for him through the window.  When Zhao gets home, he sees his scattered photos on the floor back in his apartment, and retaliates by hitting the nails through to the other side and flinging rubbish to the other room.  The girls hit back in the night, when he is not around, and write him a note of warning.  Hence begins a lighthearted feud that swings to and fro between the two parties.

Zhao meets Yang again in the tram and helps her retrieve something she dropped outside the window.  He goes to her textile company to find materials to write his journalistic report, and helps fight a hooligan who is harassing Yang.  The two arrange to meet at a park nearby a couple of days later for his interview.  By then, Yang has already realized he is the tenant next door.  After the interview, Zhao does a series of write-ups entitled "the sad life-story of a female worker".  

One day, Yang finally knocks on Zhaos door and reveals to him who she really is.  She tells him she is only disclosing this secret because she is leaving Shanghai – the factory she works in has recently closed down.  Zhao stops her and tells her that he will support her in Shanghai.  However, after he goes to work, Yang goes through a dilemma, and finally decides to leave so as not to be a financial burden on him.  When Yangs friend realize they are pining for each other, she decide to bring the couple together again.

By then, Zhao has received news he, too, has been dismissed from his job.  He and Tang walk towards the pier, where they meet Yang and her friend, who are also out of work.  They read from the newspaper that their friend Xu has committed suicide because he is unemployed.  Zhao and Tang conclude that Xu is too weak, and that they must be optimistic like their other friend Liu, in facing their paths in life.  The four friends then walk forward side by side with their arms entwined.

==DVD release== Sun Yus Daybreak (1933 film)|Daybreak.

==References==
 

==External links==
 
*  
*  
*  
*   summary at the University of California, San Diego
*   at the Hawaii International Film Festival

 
 
 
 
 
 
 