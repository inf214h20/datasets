Robert and Bertram (1938 film)
{{Infobox film
| name = Robert and Bertram
| image =
| image_size =
| caption =
| director = Mieczysław Krawicz
| producer = Józef Rosen
| writer = Gustav Räder (play)   Johann Nestroy (sketches)   Jan Fethke   Napoleon Sądek   Emanuel Schlechter   Ludwik Starski
| narrator =
| starring = Helena Grossówna Eugeniusz Bodo Adolf Dymsza   Antoni Fertner
| music = Henry Vars
| editing =
| cinematography = Stanisław Lipiński
| studio = Rex-Film
| distributor =
| released = 12 January 1938
| runtime = 80 minutes
| country = Poland
| language = Polish
| budget =
| gross =
| preceded_by =
| followed_by =
}} Polish comedy Robert and German adaptation was made the following year. The films art direction was by Jacek Rotmil and Stefan Norris. 

==Cast==
* Helena Grossówna - Irena
* Eugeniusz Bodo - Bertrand
* Adolf Dymsza - Robert
* Mieczysława Ćwiklińska - siostra Ippla
* Antoni Fertner - Ippel, ojciec Ireny
* Michał Znicz - baron Dobkiewicz
* Józef Orwid - dozorca więzienia
* Julian Krzewiński - lokaj
* Feliks Żukowski - pan młody
* Henryk Małkowski
* Edmund Minowicz
* Wincenty Łoskot

==References==
{{reflist|refs=
}}

==External links==
* 

 
 
 
 
 
 
 
 


 
 