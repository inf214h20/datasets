Drool (film)
 
{{Infobox film
| name           = Drool
| image          = Drool FilmPoster.jpeg
| alt            =
| caption        =
| director       = Nancy Kissam
| producer       =
| writer         = Nancy Kissam
| starring       = Laura Harring Jill Marie Jones Oded Fehr
| music          = Dana Boulé
| cinematography = Kara Stephens
| editing        = Jennifer Godin
| studio         =
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Drool is a 2009 American film, starring Laura Harring, Jill Marie Jones, Oded Fehr, Ashley Duggan Smith, and Christopher Newhouse. The film itself speaks of sexual assault, teenage sex, homosexuals, verbal abuse and physical abuse.

==Plot==

 
Anora Fleece (Laura Harring) who had dreams of a fairy tale marriage with her husband Cheb (Oded Fehr), discovers that reality is harsh, finding her only solace in daydreams. Cheb becomes sullen, abusive & disrespectful toward her. Her two children, Tabby & Little Pete (Ashley Duggan Smith, Christopher Newhouse) adopt similar attitudes toward her. When soft spoken Anora meets with her new neighbor Imogene Cochran (Jill Marie Jones), the bright bubbly woman offers exciting new perspective on life.

However, little Petes reaction to Imogene is less than favorable; Anora slaps Pete in the face for regarding her new friend as a "nigger." Little Pete threatens to inform their racist father that Anora had allowed a black woman to their home. At supper, little Pete acts as promised and tells Cheb about Imogene. Cheb breaks a beer bottle in a fit of rage and threatens Anora. Anora is hesitant to continue seeing her new friend, but decides to go ahead with it.

As the two women become more than friends, Tabby has been in a mess all her own. Having met and complied to have oral sex behind a bush with teenager Denny (Dalton Alfortish), they form a relationship. Tabby is infatuated with Denny. She mentioned this to her best friend, Princess (Rebecca Newman), who leads her to feel ashamed and insists Denny is trash. Tabby later breaks up her relationship with Denny, who insulted her by calling Tabby "an unfeeling emo scene bitch." Later Tabby discovers him receiving oral sex from Princess, and leaves school. Seething with betrayal & frustration, Tabby is driven further into rebellious, sullen, and disrespectful moods. She often takes this out on Anora. Pete gets an A+ on his exam and we briefly view a fantasy he has of his male teacher. He bumps into Tabby cutting class during his football practice and they leave together. 

The audience is shown Cheb at his day job in the factory, and in a meeting with his boss we are in no subtle way led to infer that Cheb must give his boss oral sex to maintain his job. Cheb leaves from work early after becoming sick. When Cheb arrives home, angry, he discovers Anora and Imogene lip-locked in his room. In yet another fit of fury, Cheb produces a gun. Pete and Tabby come home and, alerted by the sound of struggle, watch in shock. In Anoras struggle to disarm him, Cheb shoots Little Pete near the head. Anora lets out a terrible scream snatches away the gun; she shoots Cheb five times and kills him as Imogene and Tabby gawk in horror. 

After making sure that Little Pete (protected by his football gear) is okay, they decide to leave town. Tabby grows even more disrespectful against Anora and Imogene, siding more with her deceased father. She secretly cries and draws a scribble of Tabby punching & strangling Princess for a bitter betrayal in spite of the death of her father. The next morning, after Tabby has a brief confrontation with Imogene about alerting the police, the altered family sets out on a trip, with Chebs body on the trunk. They make a quick stop at a park that Tabby insists on saying goodbye to a strange rubber relic "Gully", and the journey begins.

They make a hotel stop for the night. When Anora leaves to buy food, a police officer pulls her over, believing that she is seling Kathy K make-up. Relieved, she provides him with a catalog. Anora returns with sandwiches, and Tabby throws a tantrum because she doesnt like her sandwich. Anora offers to take hers instead, but Tabby continues her fit and calls Anora useless amongst other things. Imogene confronts Tabby about talking to Anora so rudely. Tabby curses everyone in the room, angrily walks out, stealing away to sleep in Imogenes car for the night.

Anora sees Tabbys scribble about her killing Cheb, allowing her to see her daughters anger. In Imogenes car, Tabby begins to cry, as she still misses her father, holding Anora, and partially Imogene, responsible for intentionally murdering Cheb.

The next day, the gang continues on the road. Tabby and Imogene still have issues with each other. Imogene accidentally runs over a squirrel, Tabby and Pete insist on having a burial for the squirrel.

As the gang arrives at Kathy Ks house (Ruthie Austin), excited to see Imogene, she introduced everyone to her. After a warm welcome, Tabby and Pete spy two of Kathy Ks male workers kissing each other. Tabby says "Thats so sick", but Pete grins in fascination. Kathy questions Anora about shooting Cheb, as she was so shy to talk about it. Tabby & Pete looked at Chebs corpse, disgusted by the look, as they both paid their respects as they were sorry for his death.

Later, Anora, alone with Imogene, begins to kiss her bare shoulder, cheek, corner of her mouth, when they were suddenly interrupted by Tabby & Pete entering the room. Tabby has an emotional breakdown, Pete was surprised & shocked in confusion. Tabby describes that it was so gross and disgusting and that she hates homosexuality. Pete asks Anora if she and Imogene are normal, but Tabby interrupts, describing them as totally fucked up. Pete insulted Tabby by calling her a "slut". Tabby makes the mistake of calling Imogene the word "nigger", met with one of Anoras forceful slaps. Tabby guesses that the reason Cheb had his gun out in the first place was because she and Imogene had been togheter and Anora confirms it.

Anora and Pete talk about how Cheb had been abusive and had hit both of them on several accounts. While Tabby says that Cheb never hit her, she eventually reveals that once she went with Cheb to a bar and, after a few too many drinks, one of his friends tried to molest her in her fathers plain sight. She threw up on his face and was humiliated, yet Cheb only laughed. She kept it as a dark memory from Anora. 

The three decide to go along with Imogene & Kathy K to the beach. The adults happily watch Tabby & Pete play with each other in the water. They return to Kathy Ks and bond while applying makeup together. Tabby and  Pete announced that they are ready to bury Cheb. They do his funeral makeup and bury Cheb, looking like a fabulous drag queen amongst Kathy Ks various backyard graves.

The gang has a peaceful dinner. Anora, Tabby, & Pete, on the pool staring at the sky, wished for a miracle that had come true. Anora and Imogene, alone at last, can finally enjoy each other fully, as the final scene of the movie ends with Tabbys final diary entry to tell Gully to forget about what she said about Anora & Imogene, she tells Cheb to take care, she says, "Without him, we wouldnt have set loose & ended up living ", Tabby says amen, and quietly closes her diary.

==Cast==
*Laura Harring as Anora Fleece
*Jill Marie Jones as Imogene Cochran
*Oded Fehr as Cheb Fleece
*Ashley Duggan Smith as Tabby Fleece
*Christopher Newhouse as Little Pete Fleece
*Rebecca Newman as Princess
*Dalton Alfortish as Denny
*Ruthie Austin as Kathy K
*James Dumont as Mr. Wilder
*Kerrell Antonio as Officer

==External links==
*  
*  

 
 
 
 
 
 