Businessman (film)
 
 
 
{{Infobox film
| name           = Businessman
| image          = Businessman poster.jpg
| caption        = Theatrical release poster
| director       = Puri Jagannadh
| producer       = R. R. Venkat
| writer         = Puri Jagannadh
| starring       = Mahesh Babu Kajal Aggarwal Prakash Raj
| music          = S. Thaman
| cinematography = Shyam K. Naidu
| editing        = S. R. Shekhar
| studio         = R. R. Movie Makers
| distributor    = R. R. Movie Makers
| released       =  
| runtime        = 131 minutes
| country        = India
| language       = Telugu
| budget         =  400 million   
| gross          =  448 million   
}}
 Telugu crime film written and directed by Puri Jagannadh. Based on a concept by Ram Gopal Varma and produced by R. R. Venkat under the banner R. R. Movie Makers, the film features Mahesh Babu and Kajal Aggarwal in the lead roles, with Nassar, Prakash Raj, Sayaji Shinde, Raza Murad and Brahmaji in supporting roles.

Mahesh plays Vijay Surya, a ruthless man who comes to Mumbai from South India aspiring to become a mafia don. Surya arrives just as the Mumbai police declare the end of the "Mafia Raj" and begins his journey by helping a local politician and trapping the city commissioners daughter, only to fall in love with her. He eventually emerges as the biggest mafia leader of Mumbai and kills a national politician Jai Dev in retribution for cheating and murdering his parents.

S. Thaman composed the films music and Shyam K. Naidu was the films cinematographer. The film was made with a budget of  400 million and was launched formally on 15 August 2011 at Hyderabad, India|Hyderabad. Principal photography began on 2 September 2011 and was shot in Hyderabad, Mumbai and Goa. A few song sequences were shot in Bangkok. Filming ended on 10 December 2011 in 74 working days, one of the shortest periods in which a Telugu film has been shot.

Released during   in 2013.

== Plot ==
  slum in Mumbai, the films setting.]]
Mumbai Police commissioner Ajay Bhardwaj announces the end of the "Mafia Raj" in the city, in order to try to bring peace to the citys streets. However, a ruthless south Indian named Vijay Surya comes to the city, aspiring to be the most powerful Mafia Don there. Staying in Dharavi with his friend, he is introduced to a local politician Laalu, who is in trouble and offers him help. He recruits a portion of the gangsters and criminals living in Mumbai lead by Naseer to work under him. Suryas henchman Shakeel manages to kill a witness of Laalus crimes in the jail where he is kept in, winning Laalus friendship.

After this, Surya promises to clear the debts of Dharavi locals so that their houses will not be repossessed. His men rob the original copies of the loan documents in the local branch of Bank of Maharashtra and destroy the bank records. Surya slowly becomes both popular and feared as an extortionist, while Dharavi locals praise him as their savior. Surya is aware of Bhardwajs intentions. To safeguard himself, he traps Bhardwajs daughter Chitra, a keen painter, by posing as a NRI businessman interested in arts. They both fall in love. However, by the time Surya realises that he truly loves her, Bhardwaj has exposed Suryas real nature to Chitra which makes her hate him. Surya is immediately arrested.

Surya is released after manipulating Bhardwaj by having his associates kidnap Chitra. He promises to leave crime behind him and asks Bhardwaj for permission to marry Chitra. When they refuse his proposal, he insists to Bhardwaj that he will set up a business which is untouchable by the police, and will eventually marry Chitra. Surya sets up a company named "Surya Exports & Imports" as a front for the organized crime that they indulge in. He plans to grow his organized crime network all over India and begins to set up branch offices for "Surya Exports & Imports" in all major cities, towns and villages all over India. He recruits local gangsters for staff and begins to forcibly collect a two percent tax on every contract made in that area. Surya amasses a huge fortune, making him a billionaire, and during the inauguration of his "Business Bank", he reveals his thirst for power to Bhardwaj. He claims that the crime rate dropped significantly after he started to recruit all the gangsters. He also states that he wants to rid India of crime and help the needy.

Next, Surya helps Laalu to become the Mayor of Mumbai by defeating Arun Ghokle, endorsed by Ghanapuleti Jai Dev, a powerful national politician who wants to become the new prime minister of India in the upcoming elections. Ghokle then reveals Jai Devs illegal activities to Bhardwaj, after which he is killed by Jai Dev. After the meeting, Surya comes to know that Jai Dev is planning to kill Bhardwaj and Chitra. He reaches Chitras house and saves her but fails to save Bhardwaj. Chitra is devastated and Surya reveals to her that he was an American-born Indian who lost his philanthropist NRI father and mother at an early age; they were cheated and killed by Jai Dev. This incident and the peoples attitudes towards orphans while growing up made him hate that society.
 Indian parliament elections and meets Guru Govind Patel, the head of the opposition party through Laalu. He makes a deal with him, offering  350 billion for election campaigning and also promises him that he would make him the prime minister of India. Surya spends millions on every constituency in India, making Jai Dev unable to contest in the elections by revealing his illegal affairs. Jai Dev kidnaps Chitra to seek revenge on Surya, who fights with Jai Devs henchmen. He is injured, but eventually kills Jai Dev, saving Chitra and proposing to her for the last time. It is revealed at the end that Surya successfully installed Guru Govind Patel as the Prime Minister of India.

Surya is last seen recuperating in a hospital in the company of Chitra, who accepts his proposal.

== Cast ==
;Principal cast
* Mahesh Babu as Vijay Surya 
* Kajal Aggarwal as Chitra Bhardwaj 
* Prakash Raj as Jaidev Ghanapuleti 
* Nassar as Ajay Bhardwaj 
* Sayaji Shinde as Laalu 
* Raza Murad as Guru Govind Patel 

;Supporting cast
 
* Dharmavarapu Subramanyam as Laalus secretary  Subbaraju as Jai Devs assistant  Bharath Reddy as Inspector Bharath 
* Ayesha Shiva as Ayesha, Chitras friend 
* Rajeev Mehta as Arun Ghokle 
* Brahmaji as Suryas friend 
* Mahesh Balraj as Naseer 
* Sanjay Swaroop as Suryas father 
* Aakash as Young Surya 
* Bandla Ganesh as Puliraju 
* Shweta Bhardwaj in the Item number "We Want Bad Boys" 
* Puri Jagannadh as a Taxi driver (Cameo appearance|cameo) 
 

== Production ==

=== Development === Tamil and Hindi language|Hindi. The films protagonist was supposed to be a south Indian coming to Mumbai and setting up a crime business using tact and intelligence to emerge as one of the biggest gangsters.    The films production was expected to begin in October 2010 once Suriya completed his part in 7aum Arivu (2011) and was scheduled for release in the summer of 2011.  Jagannadh stated in an interview that principal photography would begin in September 2010.  It was speculated that the film was to be based on the real life story of a gangster who had moved from the Cuddalore district of Tamil Nadu to Mumbai in the late 1960s. 
 Hyderabad after pooja ceremony conducted at Hyderabad.  Shyam K. Naidu and S. Thaman were confirmed as the films cinematographer and music director respectively. 

=== Casting ===
{{multiple image
 
| align     = right
| direction = vertical
| footer    = Shruti Haasan (top) was initially considered for the female lead role, but Kajal Aggarwal (bottom) ended up being engaged for the role.
| width     =

 
| image1    = Shruti Haasan TeachAIDS Interview1.png
| width1    = 200
| alt1      = 
| caption1  =

 
| image2    = Kajal Aggarwal 3.jpg
| width2    = 200
| alt2      = 
| caption2  =
}} dubbing for Ali because of Maheshs specific characterisation; he added that Maheshs voice had changed when compared to his performance in Pokiri.  Mahesh also convinced Jagannadh to give up smoking, since he himself was trying to give up after being a chain smoker. 

Shruti Haasan was initially considered for the female lead role after her performance in her Telugu film debut, Anaganaga O Dheerudu (2011).  However, Kajal Aggarwal was signed as the female lead later in May 2011, marking her first collaboration with both Mahesh and Jagannadh respectively.  She was paid  10 million for this film.  Jagannadh confirmed reports stating Mahesh and Kajal sharing a kiss in the film, adding that he retained them after Maheshs wife Namrata Shirodkar approved it. It was reportedly the first onscreen kiss for both of them.  Kajal explained that it was not supposed to be sensual, but was meant to portray the conflict between two characters, and added that it was shot "aesthetically" by Jagannadh. 

Prakash Raj, Nassar, Sayaji Shinde, Dharmavarapu Subramanyam, Brahmaji and Bandla Ganesh among others were cast for key supporting roles.    Hansika Motwani was rumored to be a part of the films cast in late August 2011 but not immediately confirmed.   In mid October 2011, Hansika reportedly agreed to perform an item number in the film.  She later dismissed those reports as baseless rumors, claiming that other acting assignments keep her away from accepting. 

Actress Shweta Bhardwaj was therefore selected to perform the item number instead.    She was friends with Jagannadh, and he approached her for this song during the films shoot at Mumbai, saying that the song suited her and her personality as a dancer. She also tried to understand the meaning of the lyrics while dancing. Bhardwaj was afraid of dancing, suspecting a possible sprain, and found the costumes uneasy, but had to continue upon the choreographers insistence.  Canadian-born Indian actress Ayesha Shiva was signed to play Kajals friend in the film.   

=== Filming ===
 , where principal photography began.]]
The film was planned to be shot entirely in and around Mumbai and Jagannadh searched for distinct localities there which suit the scripts backdrops.  Jagannadh told Venkat that he needed 75 working days to complete the films shoot. He planned to start with Mahesh in the first week of September 2011 and established this schedule before finishing in the first week of January 2012.  Filming began on 2 September 2011 at Rajiv Gandhi International Airport in Hyderabad. 

The second schedule began at Mumbai from 20 September 2011 and Mahesh confirmed in an interview that it would last until 5 December 2011.  An action sequence choreographed by Vijayan was shot in mid October 2011 and it was announced that the films climax would be shot in and around Goa from 27 October 2011.  The song "We Want Bad Boys" was shot in late October 2011.  At the same time, Jagannadh planned to shoot a couple of songs with Mahesh and Kajal in Spain during November 2011.  The filming of the climax sequences featuring Mahesh, Prakash Raj and others was finished by 2 November 2011. 

After filming two songs in the districts of Krabi and Pattaya in Bangkok, the films unit returned to Hyderabad on 2 December 2011 for completing patch work. The films shooting was wrapped up on 10 December 2011 in 74 working days as expected.   Jagannadh revealed in an interview that the films length by the end of shoot was 84,000 feet as planned. He added that Mahesh and Kajal participated in the films shoot for 65 and 30 days respectively. 

=== Post production === dubbing activities began on 15 December 2011 at Shabdalaya Studios in Hyderabad.  The DTS mixing works were in progress in early January 2012.    The Central Board of Film Certification passed the film with an A certificate; board members found few of the films dialogues offensive and potentially upsetting to the people of Mumbai. 

== Music ==
 

The soundtrack consists of six songs composed by S. Thaman and written by Bhaskarabhatla.  Aditya Music acquired the audio rights.    The original along with both the dubbed versions of the soundtrack were released on 22 December 2011 at Shilpakala Vedika in Hyderabad, India|Hyderabad.  Na. Muthukumar and Kailas Rishi wrote the lyrics for the dubbed Tamil and Malayalam versions of the songs respectively.   The soundtrack was a huge success with nearly 200,000 compact discs sold on the first day of its release itself. 

== Release ==
The film was initially planned for a worldwide release on 11 January 2012.    Hari Venkateswara Pictures acquired the films overseas distribution rights and after requests by non-Telugu Mahesh fans, it was the first Telugu film to release with subtitles in overseas cinemas.  The makers planned for a release in new international markets like Japan and Dubai to cash in on Maheshs previous film, Dookudu.  The release was postponed by two days to 13 January 2012 due to post production delays.  Two thousand screens across the world were booked for the films initial release. 

It was released to 92 screens across Hyderabad, breaking the previous record set by Oosaravelli (2011) which was released to 70 screens. Prasads IMAX screened 33 shows on the release day.  The films Tamil and Malayalam dubbed versions, also titled Businessman, was released on 7 December 2012 to 200 screens across Tamil Nadu and 28 December 2012 respectively.  

=== Legal issues and criticism ===
 , where Akhil Bharatiya Vidyarthi Parishad activists burnt the films reels on 25 January 2012 in protest of few of the films dialogues and the lyrics of the song We want Bad Boys.]] Italian partisan song "Bella ciao" for the song "Pilla Chao"and "He Lives in You", a song written and performed by Lebo M and his South African Choir for the album Rhythm of the Pride Lands (1995), for the song "Chandamama".  "Pilla Chao" was one of the more successful songs on the soundtrack and many music lovers left angry comments about the plagiarism on the songs YouTube video. 

The makers violated the rules of The Cinematograph Act, 1952 and the Central Board of Film Certification regarding the re-working of its title from The Businessman to Businessman after the board cleared the film with the former title. The Andhra Pradesh Film Chamber of Commerce cleared the registration of the film title as The Businessman on 1 January 2011 through a letter and was renewed up to 22 April 2012. The set of posters sent to the board for certification purpose carried the full title. The film was publicised without the definite article, which was also missing from the posters. Also the A certification was omitted. 

After release, the film was criticised for the usage of curse-words by the protagonist and a few intimate sequences between the lead pair. The muting of a few dialogues and blurring of a few visuals by the board were criticised by viewers.  Bajrang Dal lodged a complaint with Municipal police on 14 January 2012 stating that the lyrics of the song "We love Bad Boys" contained vulgar words and conveyed an unacceptable meaning for which they demanded action against Jagannadh. The police confirmed this and stated they would seek legal opinions for taking further action. 

  and were anti-patriotic.  They burnt the films reels in front of Osmania Arts College in protest of the lyrics of the song "We want Bad Boys" alleging that the song was filled with abusive words. They also demanded a public apology by Mahesh and Jagannadh.  Journalist Rohit Vats in his article Real and reel: How Telugu cinema celebrates stalking mentioned the sequences in Businessman quoting, "The conversation between Mahesh Babu and Kajal Aggarwal in Businessman was termed vulgar but later it was accepted in the name of sarcasm. Basically, it was perceived as harmless teasing". 

=== Marketing ===
A first-look teaser of 41 seconds was released on 9 November 2011.  The first-look poster featuring a still of Mahesh sitting in a chair and deeply thinking was revealed on 11 November 2011 (11.11.11), receiving a positive response.   Two posters featuring Mahesh were unveiled on 5 and 11 December 2011.   The theatrical trailer was attached to the prints of Panjaa (2011) to be screened in theaters from 9 December 2011. 

As a part of the films promotion, Aditya Music offered a range of ringtone packs. Contests were introduced where the winners won posters autographed by Mahesh, the films tickets and soundtrack discs apart from others. 

=== Home Media === DVD and Universal Home Entertainment and were released in May 2012.  

== Reception ==

=== Critical reception ===
{{multiple image
 
| align     = right
| direction = vertical
| footer    = Mahesh (top) won critical acclaim for his performance while Jagannadh (bottom) was praised for his work as a dialogue writer.
| width     =

 
| image1    = Mahesh Mywork.jpg
| width1    = 150
| alt1      = 
| caption1  =

 
| image2    = Purijagannadh.jpg
| width2    = 150
| alt2      = 
| caption2  =
}}
Suresh Krishnamoorthy of The Hindu called Businessman a "wholesome package" and a "pure entertainer", quoting "Puri Jagannath has proved that hes still the director with the Midas touch with his latest release, Businessman".  Y. Sunita Chowdary, also from The Hindu, wrote "The Businessman has a mix of action, image, dialogue and montage but what makes it entertaining is that all the above quantities are inextricably bound and linked together at every level with an admirably tight script." She added that Jagannadh did not make any structural, technical or content errors, although the story was a regular one.  Suresh Kavirayani of The Times of India gave the film 3.25 out of 5 stars and called the film an "unpretentious entertainer that manages to strike the right balance", which according to him, had enough in it to keep the viewers interested till the end despite not being a spectacular production. 

Indo-Asian News Service gave the film 3 out of 5 stars and called Businessman a film for Maheshs fans and highlighted Maheshs performance and Jagannadhs dialogues as the films strong points.  Another reviewer from IANS called the film "senseless yet entertaining" and stated "Maheshs dashing performance as a mafia kingpin with a hidden personal agenda is commendable and deserves high appreciation. Businessman may be perverse and appeal only to the masses, yet is a winner for unexplainable reasons."  Radhika Rajamani of Rediff.com gave the film 2.5 out of 5 stars and opined that Businessman was an entertaining film if watched uncritically. She added "Theres no doubt that Puri Jagan has played to the gallery and kept the masses and Maheshs image in mind while conceiving this film. Its just Mahesh all the way." 
 Oneindia Entertainment called Businessman a serious film with limited appeal, which might be enjoyed by Maheshs fans but which would be an average for others.  B. V. S. Prakash of Deccan Chronicle gave the film 2 out of 5 stars and stated "Businessman is a typical Mahesh Babu film all the way, evoking laughs with comic one-liners and subtle expressions. However, his efforts could go in vain thanks to a contrived and highly exaggerated plot."  IndiaGlitz called Businessman a "Mahesh-Magnified film" and stated "It is clear that the director throws all logic to the winds in the hope that Mahesh the Prince and a few cheeky one-liners will salvage the film. There is so much meaningless lecturing in the movie in the form of the demented arguments that our hero employs in many a scene." 

=== Box office ===
Businessman opened to 85% occupancy in both single screens and multiplexes at AP/Nizam box office on its first day.  It grossed approximately  187.35 million and collected a share of  137.8 million at the global box office on its first day creating an all-time record in terms of opening day collections.    The film collected a distributor share of  120 million in AP/Nizam box office,  10 million in Karnataka,  7.5 million together in Tamil Nadu, Orissa and North India respectively  and  0.3 million together in the United States, United Kingdom, Australia and Japan on its first day. 

The film collected approximately  418.35 million by the end of its first week at the global box office and was declared a Blockbuster (entertainment)|blockbuster.  The film completed a 50-day run in 350 centers on 3 March 2012,  and a 100-day run in undisclosed number of centers on 21 April 2012.  The film collected a distributor share of  448 million  and grossed over  550 million in its lifetime at the global box office. 

=== Accolades ===
{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- TSR - TV9 National Film Awards 2012  Best Director Puri Jagannadh
| 
|- 2nd South Indian International Movie Awards  Best Director Puri Jagannadh
| 
|- Best Cinematographer Shyam K. Naidu
| 
|- Best Actor Mahesh Babu
| 
|- Best Actress Kajal Aggarwal
| 
|- Best Actor in a Supporting Role Nassar
| 
|- Best Music Director
|S. Thaman
| 
|- Best Lyricist Bhaskarabhatla for "Sir Osthara"
| 
|- Best Male Playback Singer
|S. Thaman for "Sir Osthara"
| 
|- Best Female Playback Singer Suchitra for "Sir Osthara"
| 
|- Best Fight Choreographer FEFSI Vijayan
| 
|- 60th Filmfare Awards South   Filmfare Award Best Film – Telugu
|R. R. Venkat
| 
|- Filmfare Award Best Director – Telugu Puri Jagannadh
| 
|- Filmfare Award Best Actor – Telugu Mahesh Babu
| 
|- Filmfare Award Best Music Director – Telugu
|S. Thaman
| 
|- Filmfare Award Best Playback Female Singer – Telugu Suchitra for "Sir Osthara"
| 
|- Santosham Film Santosham Film Awards 2013  Santosham Best Best Actor Mahesh Babu
| 
|- Best Lyricist Bhaskarabhatla
| 
|- Radio City Listeners Choice Suchitra for "Sir Osthara"
| 
|}

== Remakes == Jeet and Subhasree Ganguly reprising the roles of Mahesh and Kajal from the original respectively. 

== Possible sequel ==
Jagannadh expressed interest in making a sequel of the film after the response to the original on its first day and planned to begin work on the same from late 2012.  Mahesh too confirmed the same in late January 2012 during the scripting stage of the sequel titled Businessman 2.  It was planned as a bilingual film to be shot in Telugu and Hindi simultaneously.  Ram Gopal Varma also expressed interest after listening to the script and felt that the sequel would do much better business that the original.  However, Jagannadh shelved the sequel in early August 2012 and opted to direct Mahesh in an original film. 

== Notes ==
 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 