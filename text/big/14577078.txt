The Last Siege: Never Surrender
{{Infobox film
| name           = The Last Siege: Never Surrender
| image          = Last Siege.jpg
| caption        = 
| director       = Worth Keeter
| producer       = 
| writer         = Steve Latshaw
| starring       = Jeff Fahey Ernie Hudson
| music          = Elmer Bernstein
| distributor    = Lions Gate
| released       = 1999
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1999 independent independent action/thriller starring Jeff Fahey and Ernie Hudson. The theatrical release was called Hijack but this was changed for the 2002 DVD release.

==Plot== Union Station, ATF Agent (Jeff Fahey|Fahey) who happened to be a passenger on the train is the one man who just may be able of both freeing the hostages and defusing the bomb before time runs out.

==Cast==
* Jeff Fahey as Eddie Lyman 
* Ernie Hudson as Senator Douglas Wilson
* Beth Toussaint as Valerie Miller
* Brent Huff as David Anderson
* Patrick Kilpatrick as Carl Howard
* Robert Miano as John Gathers
* Larry Manetti as Thomas Grady
* Frank McRae as Roger Tate
* Rosalind Allen as Jennifer Benton James Stephens as Harold Besser
* Pete Antico as Jack Carpenter
* Ernie Hudson Jr. as Frank Jennings
* George Gerdes as Luke Besser
* Becky Israel as Melinda Benton
* David Gene Gibbs as Helicopter Pilot

==References==
 

==External links==
*  

 
 
 
 
 
 

 