One More Kiss (film)
{{Infobox Film
| name = One More Kiss
| director = Vadim Jean
| producer = Vadim Jean Paul Brooks
| writer = Suzie Halewood
| starring = Valerie Edmond Gerard Butler James Cosmo John Murphy
| editing = Joe McNally
| distributor = The Mob Film Company
| released = February 18, 2000
| country =  Scotland English Scots Scots
}}

One More Kiss is a romantic Scottish drama directed by award-winning film maker Vadim Jean (Leon the Pig Farmer, Beyond Bedlam, Clockwork Mice) and starring Valerie Edmond, Gerard Butler and James Cosmo in the main roles. The film’s tragic story revolves around a cancer-diagnosed woman who decides to live her remaining life to the full, which includes throwing herself into a triangle romance with an old flame.

==Plot==
When Sarah Hopson (Valerie Edmond) discovers she has a brain tumour, she leaves her successful high-rise lifestyle in New York and returns to her hometown in the Scottish Borders, where she left her widower father Frank (James Cosmo) and her childhood sweetheart Sam (Gerard Butler) to pursue a career in the United States seven years ago. Upon arriving Sarah finds out that Sam, a restaurant owner, is now happily married to Charlotte (Valerie Gogan). This, however, does not stop her from asking the couple a last favour – which is to let Sarah spend her remaining time in the company of the only man she has ever loved.

==Cast==
Director Vadim Jean decided on an all-Scottish cast with the following three actors starring in the main roles:

*Valerie Edmond (born in Edinburgh, Scotland in 1969) had her first leading role with the character of Ashley in the Scottish television series The Crow Road (1996), which also brought her a nomination for Best Actress at the BAFTA Scotland Awards in 1997.  For One More Kiss Edmond was the directors first choice to play the leading character Sarah Hopson – a successful New York literary agent, who returns to her home town in the Borders after going through a life-changing experience.   
*Gerard Butler (born in Paisley, Scotland in 1969) plays Sarah’s long-lost love Sam, who sees his otherwise happy marriage plunged into chaos by his ex-girlfriends return. Butler had his breakthrough performances with his first leading roles in 2000, when he played Attila the Hun in the American TV miniseries Attila and Count Dracula in Wes Craven’s Dracula 2000. 
*James Cosmo (born in Clydebank, Scotland in 1948) was best known for his performances as tough, violent characters in films such as Braveheart (1995) and Roughnecks (TV series)|Roughnecks (1994).  In One More Kiss, however, Cosmo is cast in the part of Sarahs doting father Frank, whose life has come to a standstill since the departure of his daughter.

===Full Cast-List===
*Valerie Edmond as Sarah
*Gerard Butler as Sam
*James Cosmo as Frank
*Valerie Gogan as Charlotte
*Carl Proctor as Barry
*Danny Nussbaum as Jude
*Dilys Miller as Mary
*Ron Guthrie as Robin
*R Gary Robson as Gary the kitchen porter Michael Murray as Market stall holder
*Lori Manningham as Michael Angello, Frank’s dog Hugh Wilson as Frank’s false teeth
*Robin Galloway as Shirley (voice) (as Robyn Calloway)
*Oscar Fullane as Chef (as Oscar Fullone)
*Simon Tickner as Chef
*Julian Jensen as Opera singer
*Kim Hicks (and Tim Francis) as Opera lovers on stage
*Nigel Pegram as Opera buff
*Andrew Townsley as Doctor Frith (as Andrew Townley)
*Collette King as Nurse King
*Molly Maher as Young Sarah

==Filming locations==
Scotland:

Edinburgh, Scotland

UK:

Berwick upon Tweed, Northumberland, England, UK

United States:

New York City, New York

The original intention was for the film to be set in Jean’s home town of Bristol, but in the end they decided to use the beautiful landscapes of Northern England and Edinburgh, Scotland instead – with the former being the main setting. The opening and closing sequences, however, were shot in New York. 

==Music / Soundtrack==
*"Amor Ti Vieta" – Tito Beltrán
*"Ave Maria" – Slava
*"Beautiful Dreamer" – James Cosmo
*"Caruso" – Julian Jensen
*"Hey Boy! Hey Girl!" – Louis Prima
*"How About You" – Connie Lush
*"Roses from the South" – Royal Philharmonic Orchestra Mylo
*"Through the Rain" – Gavin Clarke and Paul Bacon
*"Where Do You Go To (My Lovely)" – Peter Sarstedt
*"You Fascinating You (Tu Solamente Tu)" – Connie Lush

==Release==
{| class="wikitable" Country
!align="left"|Date
|- Canada
|align="left"|
September 18, 1999 (Atlantic Film Festival)
|- UK
|align="left"|
February 18, 2000
|- Iceland
|align="left"|
July 13, 2000 (Video Premiere)
|- Israel
|align="left"|
April 5, 2001
|- Belgium
|align="left"|
October 31, 2001
|- Japan
|align="left"|
August 31, 2005

January 21, 2006 (DVD)
|- USA
|align="left"|
June 6, 2006 (DVD)
|}

==Awards==
{|class="wikitable" Atlantic Film Festival
|-
!Year
!Result
!Award
!Recipient
|-
| align="center"| 1999 ||align="center"| Won ||align="center"| Audience Award ||align="center"| Vadim Jean
|-
!colspan="4"|
|- Emden International Film Festival
|-
!Year
!Result
!Award
!Recipient
|-
| align="center"| 2000 ||align="center"| Nominated ||align="center"| Emden Film Award ||align="center"| Vadim Jean
|}

==Trivia==
*Director Vadim Jean considers One More Kiss as his most personal work.   
 British premiere at the London Film Festival instead. 

*Jean did not deliberately pick an all-Scottish cast. He just chose the best from the screen test, and they all happened to be Scottish people|Scottish. 
 Berwick gave up his parking space for the production vehicle. 

==References==
 

==External links==
*  
* 
* 
*  
* 

 
 
 
 