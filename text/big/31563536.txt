Hatey Bazarey
{{Infobox film
| name           = Hatey Bazarey
| image          =
| caption        =
| director       = Tapan Sinha
| producer       = Asim Dutta
| writer         = Tapan Sinha (script) Banaphool (story)
| starring       = Ashok Kumar Vyjayanthimala Ajitesh Bandopadhyay Bhanu Bandopadhyay
| music          = Tapan Sinha
| cinematography = Dinen Gupta
| editing        = Subodh Roy
| studio         = 
| distributor    = Priya Films Neptune Distributors
| released       = June 25, 1967
| country        = India
| runtime        = 133 minutes Bengali
}}
Hatey Bazarey or Hate Bazare ( : The Market Place) is a 1967 award winning art film by noted Bengali director Tapan Sinha  and produced Asim Dutta, the story revolves around the conflict of good and evil. The film starred by Ashok Kumar, Vyjayanthimala in her first Bengali venture,  Ajitesh Bandopadhyay in the lead with Bhanu Bandopadhyay, Samit Bhanja, Rudraprasad Sengupta, Gita Dey as the ensemble cast of the film.  The film was produced by Priya Entertainment Production Limited owned by Asim Dutta. 

==Plot==
Dr Anadi Mukherjee (Ashok Kumar) is the civil surgeon in a small market town in a tribal-dominated small town in the foothills of the Himalayas. He is a god-like figure, loved and respected by both the poor tribal folks of the area like the beautiful young widow Chhipli(Vyjayantimala), Jagadamba the vegetable seller and old women like Komlididi and Nani (Chhaya Devi) and the bigwigs of the area like the District Magistrate and Superintendent of Police Mr. Pandey. Dr Mukherjee is a workaholic and lives with his young wife Manu who has a chronic heart element. He comes into conflict with Lacchmanlal (Ajitesh Bannerjee), the son of the local feudal lord Chhabilal, a veritable rogue who lusts after Chhipli who is protected by the good doctor. After the death of his wife Manu, Dr Mukherjee leaves his official job and utilizes his savings to start a mobile dispensary for the poor. Lacchmanlal gets irritated by his actions and spreads canards about Dr Mukherjee’s relationship with Chhipli who had been appointed as trainee nurse in the medical team. On the night of a tribal festival, Lacchmanlal tricks Chhipli into a tryst and attempts to rape her. Dr Mukherjee gets the news and in a fight with Lacchmanlal strangles the villain to death while getting mortally injured. The next morning he dies, but the work of the clinic is carried on by Chhipli and others of the team under the guidance of a young doctor who had earlier been reprimanded by Dr Mukherjee.

==Cast==
* Ashok Kumar as Dr. Anadi Mukherjee
* Vyjayanthimala as Chhipli
* Ajitesh Bandopadhyay as Lacchmanlal
* Rudraprasad Sengupta
* Samit Bhanja
* Gita Dey as Mrs. Pandey in a guest appearance. 
* Samita Biswas
* Chhaya Devi as Nani 
* Partho Mukerjee
* Chinmoy Ray

==Inspiration== Rabindra Puraskar Award in 1962.

==Box office==
The film was one of the most successful Bengali films of the 1960s. 

==Awards==
{| class="wikitable" style="font-size:95%;"
|-
! Ceremony
! Award
! Category
! Nominee
! Outcome
| Ref.
|- Asia Pacific 13th Asia Pacific Film Festival Asia Pacific Film Festival Best Film Tapan Sinha Asim Dutta
|rowspan="3"  
|rowspan=6|         
|- 32nd Annual BFJA Awards Bengal Film Journalists Association Awards Bengal Film Best Indian Film
|- 15th National Film Awards National Film National Film Awards National Film Best Feature Film
|- National Film Best Actor Award Ashok Kumar
| 
|- Presidential Award 1968 Presidential Award Best Film Tapan Sinha Asim Dutta
|rowspan="2"  
|- 1st Phnom Penh Film Festival Phnom Penh Film Festival  Silver trophy (Cup of honour)
|}

==References==
 

==External links==
* 
*  at Upperstall.com

 
 

 
 
 
 
 
 