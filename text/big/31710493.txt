La Tulipe noire
{{Infobox film
| name           = La Tulipe noire
| image          = Larvalove.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster for La Tulipe noire
| director       = Christian-Jaque   
| producer       = 
| writer         = Henri Jeanson
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       = 
| music          = Gérard Calvi 
| cinematography = Henri Decae 
| editing        = Jacques Desagneaux 
| studio         = Mediterranean Film Productions Agata Films Mizar Movies Flora Film 
| distributor    = 
| released       =   
| runtime        = 110 minutes   
| country        = France Italy Spain  
| language       = 
| budget         = 
| gross          = 3,107,512 admissions (France) 
}}
 novel of the same name  by Alexandre Dumas. It is, essentially, a star vehicle for the popular French actor Alain Delon.

==Synopsis==
Like the popular European Karl May movies of the same time, the script actually used only the main characters of a popular novel, but didnt stick to the original story. 
Alain Delon plays a swashbuckling French Robin Hood who fights corrupt nobles in the era before the French Revolution. Like Zorro, the Black Tulip is, in real life, a fixture at the court and, therefore, needs to camouflage himself before he can fight for justice. 
Unfortunately, Guillaume de Saint Preux, the first Black Tulip, gets injured. Marked by a scar on the face, he has to beseech his twin brother Julian to cover for him. 

The gentle, clumsy Julian fulfils his brothers wish, but Guillaumes arch enemy Baron La Mouche still seeks the Black Tulip. When he feels that there may be a connection between Guillaume de Saint Preux and the masked hero, he has Julian imprisoned. The original Black Tulip rescues him, but while Julian escapes, his brother is caught in the act and soon afterwards, executed.

In the end, Julian succeeds his brother as the Black Tulip. He rises to the occasion, and is now as good a fighter for justice as his brother was. He has also won the heart of Caroline, who supports him.
 Zorro film.

==Cast==
* Alain Delon - Julien de Saint Preux / Guillaume de Saint Preux
* Virna Lisi - Caroline „Caro“ Plantin
* Adolfo Marsillach - Baron La Mouche
* Dawn Addams - Marquise Catherine de Vigogne
* Akim Tamiroff - Marquis de Vigogne
* Laura Valenzuela - Lisette
* George Rigaud - Polizeichef
* Francis Blanche - Plantin
* José Jaspe - Brignon

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 