Terror on the Midway
{{Infobox Hollywood cartoon|
| cartoon_name = Terror on the Midway Superman
| image = Terroronthemidway1.JPG
| caption = Title card from Terror on the Midway
| director = Dave Fleischer Dan Gordon   Jay Morton
| animator = Orestes Calpini   James Davis
| voice_actor = Bud Collyer   Joan Alexander   Jack Mercer   Julian Noa
| musician = Sammy Timberg   Winston Sharples
| producer = Max Fleischer
| studio = Fleischer Studios
| distributor = Paramount Pictures
| release_date = August 26, 1942 (USA)
| color_process = Technicolor
| runtime = 9 min. (one reel)
| preceded_by = Volcano (animated short)|Volcano (1942)
| followed_by = Japoteurs (1942)
| movie_language = English
}}
Terror on the Midway is the ninth of the seventeen animated Technicolor short films based upon the DC Comics character of Superman, originally created by Jerry Siegel and Joe Shuster. This was the last cartoon by Fleischer Studios. The nine-minute short features Superman attempting to stop the chaos created when several circus animals escape their cages and restraints, including a giant ape. It was originally released on August 26, 1942 by Paramount Pictures.   

==Plot==
The story begins with the music and noise of the circus. Clark Kent and Lois Lane are at the Midway (fair)|Midway, Lois having an assignment to cover its events. She expresses her regret that she didnt have a more exciting assignment. Clark offers his condolences, then leaves for his own assignment. Later that night, as Lois attends the clown performance, a monkey wanders from the main tent and accidentally opens the cage of a giant gorilla (perhaps a homage to King Kong). Growling, the Gorilla named "Gigantic" wanders into the tent, sending everyone into pandemonium.

 
Circus workers attempt to tie the gorilla down with ropes, but are overpowered by its strength and are forced to flee. While other workers are struggling to keep the other animals under control, some of them stampede, or rear up against their owners, knocking other cages open. Lois, who has been taking pictures of the goirlla, is about to leave when she notices the monster lumbering toward a trapped young girl. She runs between the creature and the girl, and helps her escape, only to have the ape turn on her.

Clark arrives on the scene, supposedly to pick up Lois, and sees the chaos. Quickly he changes into his Superman costume and begins returning animals to their cages. Right after tossing an elephant into a cart, he hears a scream: Lois is trapped at the top of a pole holding up the tent, and the giant gorilla is climbing perilously close. Superman confronts the ape and ties it down, but during the fight one of the tent poles falls down and hits a power circuit, starting a fire. Superman saves Lois from the flames just in time and then goes to subdue the gorilla.

The final scene shows Lois vigorously typing the story, with Clark sitting lazily back in a chair at the next desk.

==References==
 

==External links==
*  
*  at the Internet Archive
*  at the Internet Movie Database
*   on YouTube

 

 
 
 
 