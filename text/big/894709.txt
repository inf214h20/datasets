Shanghai Triad
{{Infobox film
| name = Shanghai Triad
| image = Shanghai Triad poster.jpg
| caption =
| director = Zhang Yimou
| producer = Yves Marmion Jean-Louis Piel Wu Yigong
| writer =  
| starring = Gong Li Li Baotian Wang Xiaoxiao
| music = Zhang Guangtian
| cinematography = Lü Yue
| editing = Du Yuan
| distributor = United States: Sony Pictures Classics
| released = Cannes Film Festival|Cannes: May 1995 United States: 22 December 1995
| runtime = 103 minutes
| country = China
| language = Mandarin
| gross = $2,086,101 (USA)
}}
 1995 Cinema Chinese film, criminal underworld of 1930s Shanghai, Republic of China and spans seven days. Shanghai Triads Chinese title reads "Row, row, row to Grandma Bridge", refers to a well known traditional Chinese lullaby.   
 Red Sorghum, and had evolved into a romantic relationship as well. With the wrapping of filming for Shanghai Triad the two agreed to end their relationship both professionally and personally.   Gong Li and Zhang Yimou would not work together again until 2006s Curse of the Golden Flower.

==Plot==
 Triad Boss (played by Li Baotian), also named Tang. He is taken to a warehouse where two rival groups of Triads carry out an opium deal that goes wrong, leaving one of the rival members dead. Shuisheng is then taken by his uncle to Tangs palatial home, where he is assigned to serve Xiao Jinbao (Gong Li), a cabaret singer and mistress of the Boss. It is soon learned that Jinbao is also carrying on an affair with the Bosss number two man, Song (Sun Chun).

On the third night, Shuisheng witnesses a bloody gang fight between the Boss and a rival, Fat Yu, in which his uncle is killed. The Boss and a small entourage retreat to an island. There, Jinbao befriends Cuihua (Jiang Baoying), a peasant woman with a young daughter, Ajiao. When Jinbao unwittingly meddles in Cuihuas business, it results in the Bosss men killing Cuihuas lover. Furious, Jinbao confronts the Boss and tells Shuisheng to leave Shanghai.

By the seventh day, Song arrives to the island along with Zheng (Fu Biao), the Bosss number three man. During a mahjong game, the Boss calmly confronts Song with evidence of his treachery. The gang kills Songs men and buries Song alive. The Boss then informs Jinbao that she will have to die as well for her role in Songs betrayal, along with Cuihua. As Shuisheng attempts to save her from her fate, he is thrown back and beaten. The film ends with Shuisheng tied to the sails of the ship as it sails back to Shanghai. The Boss takes Cuihuas young daughter with him, telling her that in a few years, she will become just like Jinbao.

== Cast ==
* Wang Xiaoxiao as Tang Shuisheng, the young teenage boy who serves as the films protagonist and he falls under the spell of the bosss mistress, Jinbao.
* Gong Li as Xiao Jinbao, a Shanghai nightclub singer, Jinbao is the mistress of the Triad Boss. 
* Li Xuejian as Uncle Liu, a servant to a Triad organization and Tang Shuishengs uncle. 
* Li Baotian as Tang the Triad Boss who hides a ruthless side.
* Sun Chun as Song, the Bosss number two man, Songs affair with Jinbao sets up the films main conflict.
* Fu Biao as Zheng, the Bosss number three man.
* Yang Qianguan as Ajiao, a young girl living on the secluded island with her mother.
* Jiang Baoying as Cuihao, Ajiaos mother, a peasant woman who prepares meals for the Boss while he is hiding on his island estate.

== Production ==
 To Live had landed the director in trouble with Chinese authorities, and he was temporarily banned from making any films funded from overseas sources.  Shanghai Triad was therefore only allowed to continue production after it was officially categorized as local production. The director has since noted that his selection of Shanghai Triad to follow up the politically controversial To Live was no accident, as he hoped that a "gangster movie" would be a conventional film.   

The film was originally intended to be a straight adaptation of the novel Gang Law by author Li Xiao. This plan eventually changed with Gong Lis character becoming more important and the storys viewpoint shifting to that of the young boy, Tang Shuisheng. As a result the films title was changed to reflect its new "younger" perspective. 

==Reception== To Live and Raise the Red Lantern), Shanghai Triad was nevertheless generally praised by critics upon its release, with an 85% "fresh" rating on the review-database, Rotten Tomatoes. With its headline position in the New York Film Festival, The New York Times critic Janet Maslin opened her review that despite the cliched genre of the "gangster film," Shanghai Triad nevertheless "movingly affirms the magnitude of   storytelling power."  Derek Elley of the entertainment magazine Variety (magazine)|Variety similarly found the film to be an achievement, particularly in how it played with genre conventions, calling the film a "stylized but gripping portrait of mob power play and lifestyles in 1930 Shanghai."  Roger Ebert, however, provided a counterpoint to the films praise, arguing that the choice of the boy as the films main protagonist ultimately hurt the film, and that Shanghai Triad was probably "the last, and ... certainly the least, of the collaborations between the Chinese director Zhang Yimou and the gifted actress Gong Li" (though Gong would again work with Zhang in 2006s Curse of the Golden Flower).    Even Ebert however, conceded that the films technical credits were well done, calling Zhang one of the "best visual stylists of current cinema." 

===Awards and nominations=== Cannes Film Festival (1995 in film|1995)
**Technical Grand Prize   
* Camerimage Awards (1995 in film|1995)
**Golden Frog — Lü Yue (nominated) Los Angeles Film Critics Association Awards (1995)  LAFCA Award, Best Cinematography — Lü Yue 
* New York Film Critics Circle Awards (1995 in film|1995) NYFCC Award, Best Cinematography — Lü Yue 
* National Board of Review (1995 in film|1995) NBR Award, Best Foreign Language Film 
* 53rd Golden Globe Awards (1996 in film|1996) Golden Globe, Best Foreign Language Film (nominated)
* 68th Academy Awards (1996 in film|1996) Best Cinematography — Lü Yue (nominated)

== DVD release == region 1 DVD by Sony Pictures Columbia Tristar label.  The DVD edition includes English and Spanish subtitles. The DVD is in the widescreen letterbox format with an aspect ratio of 1.85:1.

== See also ==
  Triads — Chinese underground societies that play a major part of the film

== References ==
 

== External links ==
*  
*  
*  
*  
*   homepage at Sony Pictures Classics

 

 
 
 
 
 
 
 
 
 