Death in Buenos Aires
{{Infobox film
| name           = Death in Buenos Aires
| image          = File:Muerteenbsas.jpg
| alt            = Theatrical release poster for Death in Buenos Aires
| caption        = Theatrical release poster
| film name      =  Muerte en Buenos Aires
| director       =   Natalia Meta
| producer       =   Verónica Cura   Fabiana Tiscornia
| writer         =   Natalia Meta   Laura Farhi   Gustavo Malajovich   Luz Orlando Brennan
| starring       =         Carlos Casella    
| music          = Daniel Melero   Sebastián Escofet
| cinematography = Rodrigo Pulpeiro
| editing        = Eliane D. Katz
| studio         =  
| distributor    =  
| released       =  
| runtime        = 91 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          =  
}}

Death in Buenos Aires (    as Gómez, a young and handsome rookie on the police force with enigmatic motivations.

Set in the 1980s, the film was shot entirely in Buenos Aires during the months of January and February 2013. 

==Plot==
Inspector Chávez ( ) are called one summer night to the  ), who says that he found Alcortas body after investigating a noise complaint and who has been examining the dead mans possessions. The next day, the two men encounter each other in the police station restroom, where Gómez shows Chávez a matchbook from the gay nightclub Manila that he accidentally took from the crime scene.

Dolores and Chávez head to Manila in search of "Kevin," a lover of Alcorta who had left a message on the victims answering machine. Chávez watches a flamboyant performance of a song he remembers from a record at the victims apartment. He is approached by Gómez, claiming that he has also come to the club to investigate. Chávez angrily confronts Gómez, believing that the young man is following him, but accidentally starts a bar fight in the process, allowing the singer Kevin (Carlos Casella) to escape in the melee.

Gómez joins Chávez on the homicide investigation, and they hatch a plan for Gómez to go undercover, using the younger mans good looks to get closer to Kevin. Gómez goes on a number of dates with Kevin, leading him to believe that Kevin is innocent. Meanwhile, Chávez, who is stuck in a passionless marriage, finds himself increasingly drawn to Gómez, though he does not acknowledge or act on his attraction. Chávez tries to call off the plan, thinking that Gómez is growing too fond of the suspect and questioning Gómezs sexuality; Gómez retorts by wondering if Chávez is becoming jealous. Later, Kevin takes Gómez to a room at Manila and they begin to have sex, but they are interrupted by Chávez and Dolores, who have been watching via hidden camera. Kevin is arrested and taken into custody.

Chávez goes to interrogate the incarcerated Kevin, who convinces Chávez to release him from prison temporarily. Kevin shows Chávez a herd of horses that he claims the Alcorta family use for drug smuggling. Gómez arrives on the scene and fatally shoots Kevin as he attempts to escape on horseback. Chávez again confronts Gómez, who claims that he was only trying to protect Chávez and that Kevin invented the smuggling story. When Gómez attempts to pull Chávez in for a kiss, Chávez reacts with a mixture of anger and desire, holding Gómez at gunpoint and forcing him against his car. Chávez begins to unbuckle his belt, but is interrupted by the arrival of police backup.

The citys criminal justice system moves to close the investigation under pressure from the Alcorta family. However, Chávez discovers that the horses were surgically implanted with drugs, matching Kevins story.  Chávez now believes that Gómez killed Jaime Figueroa Alcorta on behalf of the Alcorta family, who were angered by the dead mans opposition to the smuggling business. When Chávez confronts Gómez with his hypothesis, Gómez pleads innocence, and Chávezs paranoia reaches an extreme when he draws his gun on two policemen. Realizing his mistake, Chávez exclaims that he is going insane. Gómez comforts Chávez and begins to kiss him. After initially pulling back, Chávez returns the kiss passionately, but is soon stopped by a gunshot to the stomach, confirming his suspicions about Gómez. As he sinks to the ground, Gómez tells him that he will miss him. Gómez throws his gun into the river, smokes a cigarette, and walks off into the night.

==Cast==
* Demian Bichir as Inspector Chávez 
*   as Officer Gómez
* Carlos Casella as Kevin "Carlos" Gonzalez
*   as Dolores Petric
* Emilio Disi as Judge Morales
* Jorgelina Aruzzi as Ana
* Hugo Arana as Commissioner Sanfilippo
*   as Blanca Figueroa Alcorta
*   as Calígula Moyano
*   as Tailor
*   as Anchorena
* Nehuen Penzotti as Miguel
* Martín Wullich as Jaime Figueroa Alcorta

==Reception==
The film received mixed reviews from Argentine critics.  Alejandro Lingenti, reviewing Death in Buenos Aires in La Nación, criticized the film for emphasizing style over substance; he called the storyline weak and oftentimes implausible and complained that the relationship between Bichir and Darin seemed forced and unnatural. 
 La Prensa was more favorable, praising Natalia Meta as a directorial novice and the performances of the lead actors, in particular Darin and Casella. He enjoyed the films humor and the dynamic between Bichir and Darins characters.  Pablo Scholz of Clarín (Argentine newspaper)|Clarín also reviewed the film positively, writing that “Natalia Meta’s debut is flawlessly wrought" and that the film "fulfills its purpose and will not disappoint.”  

==References==
 

==External links==
*  
*   at the Internet Movie Database
*   at Rotten Tomatoes

*
*
*
*

 
 
 
 
 
 
 
 
 
 