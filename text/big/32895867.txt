Snowdrift at Bleath Gill
 
 
{{Infobox film name           = Snowdrift at Bleath Gill image          = Snowdrift at Bleath Gill.jpg caption        = Snowdrift at Bleath Gill  director       = Kenneth Fairbairn producer       = Edgar Anstey music  Charles Williams cinematography = David Watkin distributor    = British Transport Films released       = 1955 runtime        = 10 minutes country        = United Kingdom followed by    = 
}}
 British Transport goods train industrial documentary, the British Film Institute call it "One of the most outstanding films of its kind". 

==Production==
The 4.20am goods train had set out from   11 November 2009  At 5am, she became stuck at Bleath Gill near Stainmore Summit which at 1370 feet high was the highest point on any railway line in England until its closure in 1962.   at BTF Productions  The train, along with its crew remained stranded there until 3pm the following Monday, when the first rescue teams arrived. 
 David Watkin—who had been hurriedly assigned by producer Edgar Anstey to travel to   to join the snowplough and a gang of fifty men travelling up the line to free the train. 

The task was an arduous one; winds of 40&nbsp;mph were blowing across the summit, which coupled with the arctic-like weather conditions to produce a terrible wind chill factor. The film crew themselves had not prepared for the filming, having been summoned to make the film at short notice; Bob Paynter recalled in 2008 that the film crew had not even brought anything to eat with them, and had to rely on the generosity of the workmen.  The light for the film was provided by large Tilley lamps, a type of pressure lamp, which needed pumping-up by hand frequently. 

Having dug the engine out of the snowdrift during the night, moving the steam locomotive was another difficult task; when a steam train gets becomes stuck in snow, the heat from its boiler melts the snow around it, but as the boiler cools the melted snow refreezes as ice, meaning that the engine is frozen solid.  Workmen had to drape paraffin-soaked rags around the moving parts of the engine and set fire to them to thaw the motion. 
 
David Watkin, the assistant on the film, who later became an Oscar-winning cinematographer, noted that at a subsequent screening of the film in the area, one of the railway officials commented that "If it hadnt been for the f***ing film people wed have just left her to thaw out." 

==Analysis== Charles Williams.  

In the 2008 BBC Four documentary on British Transport Films, Dominic Sandbrook noted that in its celebration of hard manual labour the film is almost reminiscent of Soviet propaganda films.  All of the people featured in the film are railway workmen from Darlington, West Auckland and Barnard Castle, as opposed to actors (as was sometimes the case in documentaries of the era).  The author John Tomlinson cites this film as typifying the film units "celebration of energetic progressive industry". 
 BR Standard Class 2 No. 78018 - survived and is currently being restored at the Great Central Railway.  The snow plough seen is also preserved on the North Yorkshire Moors Railway.  The original Stainmore Summit board is preserved within the National Railway Museum at York.  The film is currently available on DVD from the British Film Institutes British Transport Films collection (volume 1) and is sometimes used as a ten-minute "filler" on BBC Four, a use which alternates with its BTF sibling John Betjeman Goes By Train, also ten minutes in duration. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 