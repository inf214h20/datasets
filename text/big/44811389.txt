Gambier's Advocate
{{Infobox film
| name           = Gambiers Advocate
| image          =
| alt            = 
| caption        = 
| director       = James Kirkwood, Sr.
| producer       = Daniel Frohman
| screenplay     = Ronald MacDonald
| starring       = Hazel Dawn James Kirkwood, Sr. Fuller Mellish Dorothy Bernard Robert Broderick Maude Odell
| music          = 
| cinematography = 
| editing        = 
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by James Kirkwood, Sr. and written by Ronald MacDonald. The film stars Hazel Dawn, James Kirkwood, Sr., Fuller Mellish, Dorothy Bernard, Robert Broderick and Maude Odell. The film was released on June 17, 1915, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Hazel Dawn as Clarissa
*James Kirkwood, Sr. as Stephen Gambier
*Fuller Mellish as Cyrus Vane
*Dorothy Bernard as Gene Vane
*Robert Broderick as Mr. Muir
*Maude Odell as Mrs. Muir 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 