The Brave Archer 3
 
 
{{Infobox film name = The Brave Archer 3 image = The Brave Archer 3.jpg caption = DVD cover art traditional = 射鵰英雄傳第三集 simplified = 射雕英雄传第三集}} director = Chang Cheh producer = Mona Fong story = Louis Cha screenplay = Ni Kuang Chang Cheh starring = Alexander Fu Niu-niu music = So Jan-hung Stephen Shing Joseph Koo cinematography = Cho Wai-kei editing = Chiang Hsing-lung Lee Yim-hoi studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 92 minutes country = Hong Kong language = Mandarin budget =  gross =
}} Louis Chas novel The Legend of the Condor Heroes. The film was produced by the Shaw Brothers Studio and directed by Chang Cheh, starring Alexander Fu and Niu-niu in the lead roles. The film is the third part of a trilogy and was preceded by The Brave Archer (1977) and The Brave Archer 2 (1978). The film has two unofficial sequels, The Brave Archer and His Mate (1982) and Little Dragon Maiden (1983), both of which were based on The Return of the Condor Heroes. The theme song of the film, Say Cheung Kei (四張機), was composed by Chang Cheh, arranged by Joseph Koo and performed in Cantonese by Jenny Tseng.

==Cast==
* Alexander Fu as Guo Jing
* Niu-niu as Huang Rong
* Philip Kwok as Zhou Botong
* Yu Tai-ping as Yang Kang
* Yeung Hung as Wang Chongyang
* Ti Lung as Duan Zhixing
* Lau Wai-ling as Dali empress
* Ching Li as Liu Ying / Yinggu
* Lu Feng as Zhang Shaoshou
* Sun Chien as Zhu Ziliu
* Wong Lik as Wu Santong
* Chiang Sheng as Diancang Yuyin
* Lo Mang as Qiu Qianren
* Chu Ko as Qiu Li
* Siao Yuk
* Ricky Cheng
* Lau Fong-sai
* Ngai Tim-choi
* Tony Tam
* Lam Chi-tai
* Hung San-nam
* Lai Yau-hing
* Lam Wai
* Chow Kin-ping

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 

 