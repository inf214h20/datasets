Tokyo-Ga
{{Infobox film
| name           = Tokyo-Ga
| image          =
| director       = Wim Wenders
| producer       = Chris Sievernich
| starring       = Wim Wenders (narrator) Chishū Ryū Yuharu Atsuta Werner Herzog
| music          = "Dick Tracy", Laurent Petitgand
| cinematography = Edward Lachman
| editing        = Solveig Dommartin Jon Neuburger Wim Wenders
| released       = 1985
| runtime        = 92 minutes
| country        = United States West Germany French English English Japanese Japanese German German
}} 1985 documentary plastic food displays. Wenders introduces the film as a "diary on film." It was screened in the Un Certain Regard section at the 1985 Cannes Film Festival.   

== Sections ==
#Reflections on Ozu
#Tokyo
#The center of the world
#Chishū Ryū
#Mu
#Amusements
#Wax food
#Searching for images
#Trains
#Yuharu Atsuta
#A good-bye

== References ==
 

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 