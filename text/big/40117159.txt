City Across the River
{{Infobox film
| name           = City Across the River
| image          = City across the river poster small.jpg
| image_size     = 
| alt            =
| caption        = Theatrical release poster
| director       = Maxwell Shane
| producer       = Maxwell Shane
| screenplay     = Maxwell Shane Dennis J. Cooper
| based on       =  
| narrator       = 
| starring       = Stephen McNally Thelma Ritter Luis Van Rooten and Jeff Corey
| music          = Walter Scharf
| cinematography = Maury Gertsman
| editing        = Ted J. Kent Universal International
| distributor    = Universal Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
City Across the River is a 1949 American crime film noir directed by Maxwell Shane and starring Stephen McNally, Thelma Ritter, Luis Van Rooten and Jeff Corey. The screenplay of the film is based on the novel The Amboy Dukes by Irving Shulman. 

The film is notable as the screen debut of Tony Curtis (billed onscreen as "Anthony Curtis").

==Plot==
Two members of a tough Brooklyn street gang accidentally kill one of their teachers.

City Across the River follows the story of Frank Cusack, a leading member of the Amboy Dukes teenage gang based in a slum-ridden area of Brooklyn.His activities with the gang ultimately lead  him from vandalism and hooliganism to complicity in the murder of a school teacher. His hopes&mdash;and those of his parents&mdash;for an escape from the bleakness of slum life are dashed by his willingness to accept the gang code of not informing to the police.

The film lacks big name stars and consists mainly of unrecognizable actors makes it more convincing. Most importantly, the film emphasizes the terrible consequences of the sons thoughtless actions for his parents and sister.

The parents, especially the mother (Thelma Ritter) are shown as decent, thoughtful working-class people devoting their efforts to provide their children with an education that will enable both siblings to rise out of the tenements. It is  a tragic irony that these efforts mean their supervision and guidance of Frank is neglected.

Although the film does suggest that lack of parental supervision is a reason for juvenile delinquency, it squarely pins the blame on living conditions as the chief cause: squalid and unhygienic surroundings, run-down tenements, cramped living space, overcrowding.The moods of frustration and hopelessness created by such an environment, the movie insists, are the reasons behind juvenile delinquency. City Across the River is also interesting in the way in which it highlights the parents efforts to obtain a good education for their children as a way of uplifting the next generation from a sordid and dangerous environment.

==Cast==
* Stephen McNally as Stan Albert
* Thelma Ritter as Mrs. Katie Cusack
* Luis Van Rooten as Joe Cusack
* Jeff Corey as Police Lieutenant Louie Macon
* Sharon McManus as Alice Cusack
* Sue England as Betty Maylor
* Barbara Whiting as Annie Kane
* Richard Benedict as Gaggsy Steens
* Peter Fernandez as Frank Cusack
* Al Ramsen as Benjamin Benny Wilks
* Joshua Shelley as Theodore "Crazy" Perrin
* Tony Curtis as Mitch (as Anthony Curtis) Mickey Knox as Larry
* Richard Jaeckel as Bull

==Reception==

===Critical response===
Thomas M. Pryor, film critic for the New York Times gave the film a positive review, "Despite its limited view, City Across the River is nevertheless an honest and tempered reflection of life. It is rich in character delineation, especially in minor roles, and there is a coarse, natural tang to much of the writing by Director-Producer Maxwell Shane and his co-scenarist, Dennis Cooper. Most of the players are comparatively unfamiliar, with the exception of Stephen McNally, who plays the role of a community center director in the neighborhood, and this gives the film an added degree of realism." 

The staff at Variety (magazine)|Variety magazine praised the film, writing, "Out of Irving Shulman’s grim novel, The Amboy Dukes, Maxwell Shane has whipped together a hardhitting and honest film on juvenile delinquency ... The plot threads are smoothly woven into the social fabric ... The performances by all members of the cast are marked by Shanes accent on naturalness." 

Film critic Dennis Schwartz questioned the honesty of the screenplay, "This is a much softened version of Irving Schulmans The Amboy Dukes, a book about a rough gang of teenagers in the postwar   period of Brooklyn ... This is a tired and clichéd film with its main selling point all the good location shots of the city. Tony Curtis made his film debut, taking a small part as one of the Amboy Dukes. All the gang members are stock characters and the predictable story sheds little insight about juvenile delinquency, offering only an outsiders look into the grimness of street life ... This film missed what teenage life was like in the city slums by a country mile and instead threw together a cliché-ridden story. The book was a popular hard-hitting novel. This film lost everything about the novel that was essential, and the robotic acting didnt help." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 