Three Little Girls in Blue
{{Infobox film
| name           = Three Little Girls in Blue
| image          = Three Little Girls in Blue (1946) 1.jpg
| image_size     = 200px Publicity still for film with June Haver, Vera-Ellen, and Vivian Blaine
| director       = H. Bruce Humberstone
| based on       =  
| writer         = Stephen Powys Valentine Davies George Montgomery Vivian Blaine Vera-Ellen
| music          = Mack Gordon Josef Myrow Ernest Palmer Charles G. Clarke
| editing        = Barbara McLean
| studio         = 
| runtime        = 93 minutes
| distributor    = 20th Century-Fox 
| released       = October 1946 (USA)
| language       = English
| budget         = $2,335,000 
}}
 George Montgomery, Vivian Blaine, Celeste Holm, and Vera-Ellen.  The film was adapted from Guy Boltons 1938 play Three Blind Mice and featured songs with music by Josef Myrow and lyrics by Mack Gordon.   The score is notable for the first appearance of the classic "You Make Me Feel So Young", popularized by Frank Sinatra in 1956. 

==Plot summary==
In 1902 New Jersey, sisters of modest means Pam (June Haver), Liz (Vivian Blaine), and Myra Charters (Vera-Ellen) inherit a chicken farm from their aunt.  They soon discover that the windfall is not quite enough to finance their dreams of attracting and marrying millionaires.  Reasoning that if one of them catches a rich husband, the other two will thereafter find it easier to do the same, they decide to pool their inheritances.  Pam poses as a wealthy heiress, Liz poses as her social secretary, and Myra poses as her maid.

The three go to Atlantic City, check into a luxurious hotel, and promptly meet millionaire Steve Harrington (Frank Latimore).  When Steve sends a bottle of champagne to Pam, Myra meets Mike (Charles Smith), a waiter who becomes taken with her.  A third man, Steves friend Van Damm Smith (George Montgomery), ostensibly another millionaire, joins in the following day when the girls plot to interest Steve by pretending to be drowning goes awry.  Steve and Van both court Pam, while Myra and Mike fall in love.  Although ostensibly pursuing Pam, Steve begins to be attracted to Liz. Van proposes to Pam; however, she tells him the truth about her plan to marry a rich man and he in turn reveals that he is not really rich and is also scheming to marry a rich woman.  The two call off their romance, deciding to stick to their plans of marrying money.  Van helps Pam by telling Steve that she is in love with him; Steve proposes to Pam and she accepts.

The couples are all sorted out after a trip to Harringtons family home in Maryland, with some help from his sister Miriam (Celeste Holm).  Steve finally realizes he is in love with Liz, Van and Pam decide they would rather be with one another, and Mike and Myra are married. 

==Cast==
* June Haver as Pam Charters George Montgomery as Van Damm Smith
* Vivian Blaine as Liz Charters	
* Celeste Holm as Miriam Harrington
* Vera-Ellen as Myra Charters
* Frank Latimore as Steve Harrington
* Charles Smith as Mike Bailey (uncredited)

==Production==
Production on this movie began in November 1945 and was completed in February 1946,  over 100 days of shooting at a cost of $2,335,000.   The main cast members were all on contract to Fox;  Celeste Holm made her Hollywood film debut as Miriam Harrington. 

===Screenplay=== Moon Over Miami in 1941 before being tapped again five years later as the basis for Three Little Girls in Blue.   The plot—three fortune-hunting sisters encounter humorous difficulties—was scarcely altered; even the character names are largely unchanged from the play.  The locale was shifted to Atlantic City and the timeframe moved to 1902, reflecting a turn-of-the-century theme popular in musicals at the time. 

===Music=== standard when Frank Sinatra recorded it in 1956 (Songs for Swingin Lovers!).   "This is Always", composed by Harry Warren with lyrics by Gordon, was sung by Haver and Ben Gage (singing for Montgomery) in production, but the number did not make the final cut;  however, it became a popular ballad the year the film was released,  recorded by both Jo Stafford and Dick Haymes.  "If You Cant Get a Girl in the Summertime", playing in the background while Haver and Montgomery dance, was not written for the film but was composed in 1915 by Harry Tierney with lyrics by Bert Kalmar. 

Several of the main roles were not sung by the actors portraying them. Vera-Ellens singing voice was dubbed by Carol Stewart, Ben Gage dubbed for George Montgomery, Bob Scott sang for Frank Latimore, and Del Porter for Charles Smith. 

==Release==
Three Little Girls in Blue premiered at the Apollo Theatre in Atlantic City  on September 3, 1946, opened in New York on September 26, and opened across the US in October.   It was one of six films to earn over $3 million for Fox in 1946. 

===Reviews===
Bosley Crowther of the New York Times gave the film a reserved but positive review, calling it "just a sprightly, happy show"; Holm and Vera-Ellen were singled out for praise, as was the score.   The industry paper Film Daily also discounted the story but praised the score and the production; this review, too, took note of the performances of Holm and Vera-Ellen. 

==Notes==
 

==References==
{{reflist|refs=
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}}

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 