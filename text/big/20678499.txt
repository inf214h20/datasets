Bitter Sweet (1940 film)
 
{{Infobox film
| name           = Bitter Sweet
| image          = Bitter Sweet - Title.jpg
| caption        = Title card
| director       = W. S. Van Dyke
| producer       = Victor Saville
| based on       =  
| writer         = Lesser Samuels
| starring       = Jeanette MacDonald Nelson Eddy George Sanders
| music          = Gus Kahn
| cinematography = Oliver T. Marsh
| editing        = Harold F. Kress
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $1.1 million Turk, Edward Baron "Hollywood Diva: A Biography of Jeanette MacDonald" (University of California Press, 1998) 
| gross          = $2.2 million 
}}
 Best Cinematography Best Art Direction by Cedric Gibbons and John S. Detlie.   
 stage operetta, in 1933 in black-and-white (in Britain, with Anna Neagle and Fernand Gravet in the leading roles).  The 1940 film is much cut and rewritten, removing much of the operettas irony.  The opening and closing scenes are cut, focusing the film squarely upon the relationship between MacDonalds character, Sarah, and her music teacher, Carl Linden.  The opening scene was a flash forward, in which Sarah appears as an elderly woman recalling how she fell in love.  One reason for dropping this scene is that it had been appropriated for MGMs 1937 film Maytime (1937 film)|Maytime.  Coward disliked the 1940 film and vowed that no more of his shows would be filmed in Hollywood. Dugan, Eleanor Knowles, John Cocchi and J. Peter Bergman. The Films of Jeanette MacDonald and Nelson Eddy, pp. 399–400, Grand Cyrus Press (2011) ISBN 0979099455  In 1951 he told The Daily Express, "I was saving up Bitter Sweet as an investment for my old age. After MGMs dreadful film I can never revive it" on stage. 

==Plot==
 
Set in late 19th century Vienna, the story focuses on the romance between music teacher Carl Linden (Nelson Eddy) and his prize pupil Sarah Milick (Jeanette MacDonald).   

==Cast==
* Jeanette MacDonald as Sarah Millick, later Sari Linden
* Nelson Eddy as Carl Linden
* George Sanders as Baron Von Tranisch Ian Hunter as Lord Shayne
* Felix Bressart as Max
* Edward Ashley as Harry Daventry
* Lynne Carver as Dolly
* Diana Lewis as Jane
* Curt Bois as Ernst
* Fay Holden as Mrs. Millick
* Sig Ruman as Herr Schlick (as Sig Rumann)
* Janet Beecher as Lady Daventry
* Charles Judels as Herr Wyler
* Veda Ann Borg as Manon
* Herman Bing as Market Keeper
* Greta Meyer as Mama Luden

==Soundtrack==
* "Ill See You Again"
** Written by Noël Coward
** Sung by Jeanette MacDonald and Nelson Eddy

* "Polka"
** Written by Noël Coward
** Played at the party and danced to by the guests

* "If You Could Only Come With Me"
** Written by Noël Coward
** Sung by Jeanette MacDonald and Nelson Eddy

* "What Is Love"
** Written by Noël Coward
** Sung by Jeanette MacDonald and Nelson Eddy
** Reprised at Schlicks

* "Kiss Me"
** Written by Noël Coward
** Sung by Jeanette MacDonald

* "Tokay"
** Written by Noël Coward
** Sung by Nelson Eddy and the patrons at the cafe

* "Love In Any Language"
** Written by Noël Coward
** Sung by Jeanette MacDonald at the cafe
** Partly dubbed by Ann Harriet Lee

* "Dear Little Cafe"
** Words and Music by Noël Coward with additional lyrics by Gus Kahn
** Sung by Jeanette MacDonald and Nelson Eddy
** Reprised by Jeanette MacDonald

* "Ladies Of The Town"
** Written by Noël Coward and Gus Kahn
** Sung by Jeanette MacDonald and 2 uncredited female singers

* "Una voce poco fa"
** From Gioacchino Rossinis The Barber of Seville
** Danced by a dancing ensemble

* "Zigeuner (The Gypsy)"
** Written by Noël Coward
** Sung by Jeanette MacDonald in the operetta finale

==See also==
* Bitter Sweet (1933 film)|Bitter Sweet (1933 film)

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 