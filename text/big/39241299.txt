Si Ronda
 
{{Infobox film
| name           = Si Ronda
| image_size     = 275
| border         = 
| alt            = 
| director       = Lie Tek Swie
| producer       =Tan Khoen Yauw
| screenplay     =
| narrator       = 
| starring       ={{plain list|
*Bachtiar Effendi
*Momo
}}
| music          = 
| cinematography = A Loepias
| editing        = 
| studio         = Tans Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = 
| budget         = 
| gross          = 
}} Betawi oral tradition, it follows the exploits of a bandit, skilled in silat (traditional Malay martial arts), known as Si Ronda. In the lenong stories from which the film was derived, Ronda was often depicted as a Robin Hood type of figure. The production, now thought lost film|lost, was one of a series of martial arts films released between 1929 and 1931. Si Ronda received little coverage in the media upon its release. A second adaptation of the tale, Si Ronda Macan Betawi, was made in 1978.

==Production== Betawi oral ethnic Chinese native audiences of the time. The Ronda stories follow the Betawi bandit of the same name, who is skilled at silat (traditional martial arts) and reputed to take from the rich to give to the poor.  The Indonesian film scholar Misbach Yusa Biran suggests that Ronda was selected for adaptation because of its potential action sequences. In the domestic cinema, such sequences had generally been inspired by American works and been well received by audiences. 

Similar stories to Si Rondas include those of Si Jampang and   figure of stage. 
 Njai Dasima in 1929.  Cinematography was handled by A. Loepias. Shot in black-and-white,  this silent film starred Bachtiar Effendi, a set decorator with Tans, in his on-screen debut playing the title role.  It also featured Momo, an actor who had appeared in the film Njai Dasima. 

==Release and legacy==
 , who played the title role in Si Ronda]]
Si Ronda was released in 1930;  Effendi stated that it was released before Tans Nancy Bikin Pembalesan (Nancy Takes Revenge), a sequel to Njai Dasima, began screening in May 1930.  Dutch newspapers indicate that it had screened in Medan, North Sumatra, by 1932.  Biran writes that the film received little coverage; he notes that Sinematek Indonesia has no news clippings related to Si Ronda. 

After Si Ronda, Lie and Tan collaborated on three further films.  Lie left Tans in 1932, reportedly as his approach no longer matched Tans low-class target audience and caused the works to go over budget.  Effendi continued to work with Tans until 1932, when he left to head the cinema magazine Doenia Film.  Momo continued acting until 1941, first with Tans and later with Standard Film.  The film is likely lost film|lost. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives. 

Another film based on the Ronda stories, titled Si Ronda Macan Betawi (Ronda the Betawi Tiger), was released in 1978.  Directed by Fritz G. Schadt, it starred Dicky Zulkarnaen in the title role and Lenny Marlina as his lover. In this adaptation Ronda uses his silat skills to fight corrupt land owners and colonial government workers. 

==References==
 

==Works cited==
 
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |title=Gender, Sexuality and Colonial Modernities
 |chapter=Gender and Hyper-masculinity as Post-colonial Modernity during Indonesias Struggle for Independence, 1945 to 1949
 |last=Gouda
 |first=Frances
 |isbn=978-0-203-98449-9
 |editor1-last=Burton
 |editor1-first=Antoinette
 |publisher=Routledge
 |year=1999
 |location=London
 |pages=163–76
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G.
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Kredit Si Ronda
  |trans_title=Credits for Si Ronda
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s007-30-376864_si-ronda/credit
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 28 April 2013
  | archiveurl = http://www.webcitation.org/6GDP11xP1
  | archivedate = 28 April 2013
  | ref =  
  }}
*{{cite news
 |title=Kunst en Vermakelijkheden
 |trans_title=Art and Entertainment
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011023834%3Ampeg21%3Ap005%3Aa0110
 |work=De Sumatra Post
 |location=Medan
 |page=5
 |date=7 January 1932
 |publisher=J. Hallermann
 |accessdate=28 April 2013
 |ref= 
}}
* {{cite web
  | title = Momo
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b852da934003_momo/filmography
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 28 April 2013
  | archiveurl = http://www.webcitation.org/6GDPNk58N
  | archivedate = 28 April 2013
  | ref =  
  }}
*{{cite web
 |url=http://www.jakarta.go.id/web/encyclopedia/detail/2623/Ronda-Si
 |title=Ronda, Si
 |language=Indonesian
 |work=Encyclopedia of Jakarta
 |publisher=Jakarta City Government
 |accessdate=28 April 2013
 |archivedate=28 April 2013
 |archiveurl=http://www.webcitation.org/6GDRRIJpT
 |ref= 
}}
*{{cite book
 |title=Profil Dunia Film Indonesia
 |trans_title=Profile of Indonesian Cinema
 |language=Indonesian
 |last=Said
 |first=Salim
 |publisher=Grafiti Pers
 |location=Jakarta
 |year=1982
 |oclc=9507803
 |ref=harv
}}
*{{cite book
 |title=Indonesian Cinema: Framing the New Order
 |publisher=Zed Books
 |last1=Sen
 |first1=Krishna
 |ref=harv
 |year=1995
 |isbn=978-1-85649-123-5
 |location=London
}}
* {{cite web
  | title = Si Ronda
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s007-30-376864_si-ronda
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 22 July 2012
  | archiveurl = http://www.webcitation.org/69LYXyB55
  | archivedate = 22 July 2012
  | ref =  
  }}
* {{cite web
  | title = Si Ronda Macan Betawi
  | language = Indonesian
  | url =http://filmindonesia.or.id/movie/title/lf-s018-78-423638_si-ronda-macan-betawi
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 28 April 2013
  | archiveurl = http://www.webcitation.org/6GDPZX4VY
  | archivedate = 28 April 2013
  | ref =  
  }}
*   
 

==External links==
* 

 

 
 
 
 
 