Marriage in the Shadows
{{Infobox film
| name           = Ehe im Schatten
| image          = Ehe im schatten.jpg
| image size     =
| caption        =
| director       = Kurt Maetzig
| producer       = Georg Kiaup
| writer         = Hans Scweikart
| narrator       =
| starring       = Paul Klinger Ilse Steppat
| music          = Wolfgang Zeller
| cinematography = Friedl Behn-Grund, Eugen Klagemann
| editing        = Alice Ludwig
| studio    = DEFA
| distributor    = Sovexport-Film
| released       =  
| runtime        = 104 minutes
| country        = Soviet Occupation Zone German
| budget         =
| gross          =
}} East German 1947 by DEFA. The film was described as an "attempt to confront the German people about the morals of the past", being the first film to confront the people about the persecution of the Jews and the atrocities conducted during World War II.      

==Plot==
Actor Hans Wieland refuses to divorce his actress wife, Elisabeth, who is Jewish, even as extreme pressure is applied on him by the Nazi authorities. He even takes her to a premiere of one of his films where she is unwittingly introduced to a high Nazi Party official. Upon later discovering that the charming woman at the premiere was in fact Jewish, he orders her arrest.  Hans Wieland is given an ultimatum by his former friend Herbert Blohm, now a Nazi official at the Reichskulturministerium (culture ministry), to save himself by divorcing his wife. Knowing that his wife will die in a concentration camp, Hans Wieland returns home and they drink poison in coffee whilst reciting the closing scene of Friedrich Schillers tragic play Kabale und Liebe together.

The film ends with a dedication to the real-life actor Joachim Gottschalk who committed suicide with his Jewish wife Meta Wolff and their nine-year-old son Michael.

==Cast==
*Paul Klinger: Hans Wieland
*Ilse Steppat: Elisabeth 
*Alfred Balthoff: Kurt Bernstein
*Claus Holm: Dr. Herbert Blohm
*Hans Leibelt: Fehrenbach 
*Karl Hellmer: Gallenkamp 
*Hilde von Stolz: Greta Koch  Walter Werner: Mr. Hofbauer
*Karl Hannemann: Gestapo agent
*Elly Burgmer: aunt Olga
*Lothar Firmans: state secretary

==Production==
The screenplay was based on the life and suicide of actor Joachim Gottschalk and his family in 1941.       However, Kurt Maetzig said of the film, "almost everything in the film is based on what I myself, or my family and friends, have experienced."  Indeed the character of Kurt Bernstein, portrayed by Alfred Balthoff, is strongly based on Maetzig.  Maetzigs mother had committed suicide to avoid being caught by the Gestapo.  It was the Kurt Maetzigs first feature film as director.

==Reception==
Ehe im Schatten was the only film to be released simultaneously in all the sectors of occupied Berlin, on 3 October 1947, becoming the most successful film produced in the first post-war years and is widely considered one of the best German films of this period.    The picture sold 12,888,153 tickets. 
 Bambi Prize, in 1948. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 