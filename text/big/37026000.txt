Goats (film)
{{Infobox film
| name           = Goats
| image          = GoatsFilmPoster.jpg
| caption        = Goats film poster
| director       = Christopher Neil
| screenplay     = Mark Poirier
| based on       = Goats (novel)|Goats by Mark Poirier
| starring       = {{Plainlist|
* David Duchovny
* Vera Farmiga Graham Phillips
* Ty Burrell}}
| music          = {{Plainlist|
* Woody Jackson
* Jason Schwartzman}}
| cinematography = Wyatt Troll
| editing        = Jeremiah ODriscoll
| studio         = Red Crown Productions
| distributor    = Image Entertainment
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = 
}} Graham Phillips, David Duchovny, Vera Farmiga and Ty Burrell. The film premiered at the Sundance Film Festival on January 24, 2012, and was given a limited release in the United States on August 10, 2012.

==Synopsis== East Coast prep school. This means leaving behind Wendy, his flaky, New Age mother, and the only real father he has ever known, Goat Man. 

==Cast== Graham Phillips as Ellis Whitman
*David Duchovny as Javier/Goat Man
*Vera Farmiga as Wendy
*Ty Burrell as Frank Whitman
*Justin Kirk as Bennet
*Dakota Johnson as Minnie
*Keri Russell as Judy
*Alan Ruck as Dr. Eldridge
*Minnie Driver as Shaman (cameo – uncredited)
*Steve Almazan as Jesus

==Production==

===Casting===
In May 2010, it was reported that Ty Burrell had signed on to star in the film.  In January 2011, it was announced that David Duchovny and Vera Farmiga had been cast in leading roles for the film.  That same month, Keri Russell, Minnie Driver and Will Arnett were cast in supporting roles.  Arnett later dropped out of the cast before filming began. Producer Daniela Taplin Lundberg commented on the casting: "Goats is that wonderful combination of hilarious and poignant, and were so thrilled that actors as distinguished as this ensemble have responded to the script with such passion." 

===Filming===
Principal photography for the film took place in Albuquerque, New Mexico, Tucson, Arizona, and Watertown, Connecticut in February 2011. 

==Reception==
The film received generally negative reviews from film critics. It currently holds a 20% "rotten" rating on   as a narcissistic New Age mom, David Duchovny as her pot-smoking Jesus-bearded goat herder/poolman and Ty Burrell as the divorced dad with the new wife, would appear to have all sorts of behavioral flavors to chew on. Alas, Goats – to borrow from the traits of its titular ruminants – nibbles on a lot of stuff it never gets around to digesting."  Sara Stewart of The New York Post wrote: "Theres a particularly irritating type of rich-boy coming-of-age movie in which any emotional growth is reflected in only the slightest tweak on the handsome protagonists stony visage. If I were Holden Caulfield, I might call it lousy. Its the type of strummy-guitar-scored indie thats flypaper for quirky actors like Farmiga and Duchovny, who are given too much time to indulge their characters back stories and to show off, respectively, their primal scream and goat imitation." 

    , Goats could have been so much more than an episodic sequence of whimsical little psychodramas." 

However, not all reviews were negative. Shannon M. Houston of Paste (magazine)|Paste gave the film a positive review, writing: "Sweet and simple, almost to a fault, Goats tells a familiar story of a child at the center of a bitter feud between his long-since divorced parents. Still, theres something about Goats that makes the clichéd narrative almost forgivable. For one, each cast member delivers a solid performance, although it would have been nice to see someone go over-the top a bit. (Granted, this might have been difficult in what is, in many ways, a stoner comedy.) Duchovny, Phillips, Farmiga, and Burrell stay true to their roles, but few risks are taken and even the lovely visage of Anthony Anderson is under-utilized in a film that could have used a few more laughs. If anyone takes it there, its Farmiga, who represents a generation of mothers so afraid of being prototypical they lose their children."  Critic Kirk Honeycutt wrote: "Goats isnt a film filled with great adventures or hugely emotional scenes. Yet in its own quiet way, this is a very effecting drama about a teenage boy buffeted by starkly contrasting role models from the adult world. Director Christopher Neil and writer Mark Jude Poirer, adapting his own novel to the screen, very often go against the usual grain for dramatic films: No big attention-getting opening, an ending that is more like a beginning and not everything gets explained away. Rather the film concentrates on its very human characters, their foibles and vulnerabilities. Its richly rewarding in ways even indie adult movies rarely are these days." 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 