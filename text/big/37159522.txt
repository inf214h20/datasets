Nairobi Half Life
 
 
{{Infobox film
| name           = Nairobi Half Life
| image          = Nairobi half life movie poster.jpg
| caption        = Film poster
| director       = David Tosh Gitonga
| producer       = Sarika Hemi Lakhani Tom Tykwer Ginger Wilson
| writer         = Billy Kahora Potash Charles Matathia Samuel Munene Serah Mwihaki
| starring       = Joseph Wairimu
| music          = Xaver von Treyer
| cinematography = Christian Almesberger
| editing        = Mkaiwawi Mwakaba
| distributor    = 
| released       =     
| runtime        = 96 minutes
| country        = Kenya
| language       = Swahili English
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but did not make the final shortlist, and is the first time Kenya has submitted a film in this category.   

At the 33rd Durban International Film Festival, Joseph Wairimu won the award for Best Actor.   
 Africa Magic Viewers Choice Awards 2014.  

==Plot==
A young man, Mwas (Joseph Wairimu) still lives with his parents in their rural home in Kenya. He makes a living by selling western action films, he dramatically acts and portrays most of the action figures in his films in order to entice his customers. He is an aspiring actor, and when he comes across a group of actors from Nairobi performing in his town, he asks one of them to help him jump start his acting career. But, in return, he is asked to give ksh1000 (approximately US$10) in order for him to be cast in one of the plays. He can only afford ksh500 and is told to take the other 500 with him to the National theatre in Nairobi. He is very excited, and, after receiving some money from his mother, he embarks on his journey to Nairobi with a brief stop over in his town to bade his friends goodbye. He meets his cousin (a gang leader) who gives Mwas an expensive radio system and some money to take to Khanji electronic shop in downtown Nairobi.

After making his way to Nairobi, he quickly learns that there is more to Nairobi than just opportunities and glamour. On the first day, Mwas loses everything he has to Nairobi thugs and is left stranded and confused especially because he knows no one. He gets arrested and even spends a day in jail. In a twist of events, he meets a Nairobi crook Oti (Olwenya Maina) who becomes a close friend and takes him into his criminal gang. The gang itself specializes in snatch and grab thievery with vehicle parts being their main targets. During this time, Mwas auditions and successfully lands a part in a local play set up by Phoenix Players. He finds himself struggling and juggling the two separate worlds. Mwas finally meets his cousin again who ends up forcing him to steal a car in order to clear his debt. He convinces the gang to move up from stealing parts to stealing cars in order to earn more. During that time, he falls in love with Otis onscreen love interest Amina, coming to see her at the lodgings at which she receives customers and even taking her out to the films.

==Cast==
* Joseph Wairimu as Mwas
* Olwenya Maina as Oti
* Nancy Wanjiku Karanja as Amina
* Mugambi Nthiga as Cedric
* Paul Ogola as Mose
* Antony Ndungu as Waf
* Johnson Gitau Chege as Kyalo
* Kamau Ndungu as John Waya
* Abubakar Mwenda as Dingo
* Mburu Kimani as Daddy M
* Mehul Savani as Khanji
* Maina Joseph as Kimachia

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Sub-Saharan African submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 