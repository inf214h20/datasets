Aleksandr's Price
 
{{Infobox film
| name           = Aleksandrs Price
| image          = Aleksandrs Price DVD Region 1 Poster.jpg
| alt            = 
| caption        = DVD cover
| director       = {{Plainlist|
* Pau Masó
* David Damen}}
| producer       = Pau Masó
| writer         = Pau Masó
| starring       = {{Plainlist|
* Pau Masó
* Anatoli Grek
* Josh Berresford
* Samantha Glovin
* Keith Dougherty}}
| music          = Dave Klotz
| cinematography = David Damen
| editing        = Pau Masó
| studio         = {{Plainlist| Maso & Co Productions
* White Shark Pictures}}
| distributor    = Breaking Glass Pictures
| runtime        = 103 minutes
| released       =  
| country        = {{Plainlist|
* United States
* Spain}}
| language       = {{Plainlist|
* English
* Russian}}
}} psychological drama Russian boy in Manhattan who after losing his mother to suicide, is drawn into becoming a male prostitute.

==Plot==
Aleksandrs Price is an exploration to American youth in New York City and the excessive use of alcohol and other drugs in the gay community. Aleksandr Ivanov, played by Masó, falls into the dark business of prostitution after the loss of his family, which causes him to develop a mental disorder also known as Dissociative Identity Disorder.

==Cast==
* Pau Masó as Aleksandr 
* Anatoli Grek as Dr. Mary
* Josh Berresford as Keith
* Samantha Glovin as Emma
* Keith Dougherty as Tom

==Production==
Aleksandrs Price  was shot on the period of 25 days in March 2012 in New York City. Masó scouted the talent and locations. Aleksandrs Price released its official trailer in March 2013. 

==Release==
Prior to public release, Aleksandrs Price screened at the Philadelphia QFest on July 12, 2013.  The film officially premiered on September 24, 2013.

===Critical reception===
The film has received mixed reviews. According to Internet Movie Database|IMDb, the film holds a 5 based on 210 ratings, as November 2014. 
 Steve McQueens 2011 film Shame (2011 film)|Shame stating: "Aleksandr’s Price can be viewed as a gay companion to Steve McQueens Shame, which also dealt with sexual promiscuity in New York City". She also stated the film to depict prostitution very well, saying: "Aleksandr’s Price is a gay-themed movie, its depiction of the cruel and shallow world of New York City and its commerce in young men and women of all sexual orientations is spot on". 
 Black Swan were a film about a male escort rather than a ballerina (and had a dash of Requiem for a Dream thrown in), it would probably be something like "Aleksandrs Price" (although a bit more relentless, much more overblown, and without the hint of hopeful optimism). 

==Accolades==
Aleksandrs Price had its Austrian premiere on November 10, 2014, at the "Internationale Queere Migrantische Filmtage" (Eng. International Queer Migrant Filmdays which is held every year in Vienna. Pau Masó was awarded with an "Appreciation Award" for his outstanding work in Queer film and media. 

==References==
 

==External links==
*  
*  
* "No Solo Cine" (es)  
* "No Solo Cine" (es)  
* "Sosmoviers" (es)  
* Horrornews.net  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 