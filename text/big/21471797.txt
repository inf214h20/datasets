Shadows in the Storm
{{Infobox Film
| name           = Shadows in the Storm
| image          = Cover of the movie Shadows in the Storm.jpg
| caption        = 
| director       = Terrell Tannen
| writer         = Terrell Tannen Troy Evans
|   music        =  
| producer       = A.J. Cervantes Ronald S Altbach Strathford Hamilton J Daniel Dusek
| cinematography = 
| editing        = Marcy Hamilton
| distributor    =  
| released       = 1988
| runtime        = 90 min
| country        =   English
| budget         = 
}}

Shadows in the Storm is a 1988 drama/thriller film by Terrell Tannen.

== Plot ==

Thelonious Pitt (Ned Beatty), a daydreaming businessman, goes to the Redwood Forests of California. There, he meets a beautiful woman, Melanie (Mia Sara). She looks like the woman he has been seeing in his dreams.

At the river late at night, when Melanies husband finds them, he attacks Thelonious until Melanie pulls out a pistol and fires three shots at her husband. His body goes into the river. Thats when the nightmare begins.  

== Cast ==

*Ned Beatty...	   Thelonious Pitt
*Mia Sara...		   Melanie
*Michael Madsen...	   Earl
*Donna Mitchell...	   Elizabeth
*James Widdoes...	   Victor
*Joe Dorsey...	   Birkenstock (as Joe Dorcey)
*William Bumiller...	   Terwilliger Peter Fox...	   Hotel clerk
*William Johnson...	   Gas station attendant Troy Evans...	   Det. Harris Bob Gould...           Det. Wand
*Tracy Brooks Swope...  Mercy
*Ramon Angeloni...	   Birkenstocks assistant
*Marta Goldstein...	   Terwilligers girlfriend

== References ==
 


 
 