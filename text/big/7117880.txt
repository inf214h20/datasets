Deep in My Heart (1954 film)
{{Infobox film
| name           = Deep in My Heart
| image          = Deepinmyheartposter.jpg
| caption        = Theatrical release poster
| director       = Stanley Donen
| producer       = Roger Edens
| writer         = Leonard Spigelgass Elliott Arnold
| narrator       =
| starring       = José Ferrer Helen Traubel Merle Oberon
| music          = Sigmund Romberg
| cinematography = George J. Folsey
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 132 minutes
| country        = United States English
| budget         = $2,504,000  . 
| gross          = $3,978,000 
}} MGM biographical biographical musical film about the life of operetta composer Sigmund Romberg, who wrote the music for The Student Prince, The Desert Song, and The New Moon, among others.  Leonard Spigelgass adapted the film from Elliott Arnolds 1949 biography of the same name.  Roger Edens produced, Stanley Donen directed and Eugene Loring choreographed. José Ferrer played Romberg, with support from soprano Helen Traubel as a fictional character and Merle Oberon as lyricist Dorothy Donnelly.
 Tony Martin, James Mitchell, Robert Easton, Russ Tamblyn, Susan Luckey, and Ludwig Stössel make uncredited appearances.

==Cast==
* José Ferrer as Sigmund Romberg
* Merle Oberon as Dorothy Donnelly
* Helen Traubel as Anna Mueller
* Doe Avedon as Lillian Romberg
* Walter Pidgeon as J. J. Shubert
* Paul Henreid as Florenz Ziegfeld, Jr.
* Tamara Toumanova as Gaby Deslys Paul Stewart as Bert Townsend
* Isobel Elsom as Mrs. Harris David Burns as Lazar Berrison, Sr.
* Jim Backus as Ben Judson

== Musical numbers ==
* "Overture" — Orchestral and choral medley:
* "One Kiss"  
* "Desert Song"  
* "Deep in My Heart, Dear"  
* "You Will Remember Vienna"  
* "You Will Remember Vienna" — Helen Traubel  
* "Leg of Mutton" — José Ferrer and Helen Traubel  
* "Softly, as in a Morning Sunrise" — Betty Wand (dubbing for Tamara Toumanova)  
* "Softly, as in a Morning Sunrise" — Helen Traubel  
* "Mr. & Mrs." — Rosemary Clooney and José Ferrer  
* "I Love to Go Swimmin with Wimmen" — Gene Kelly and Fred Kelly  
* "Road to Paradise"/"Will You Remember (Sweetheart)" — Vic Damone and Jane Powell  
* "Girls Goodbye" — José Ferrer  
* "Fat Fat Fatima" — José Ferrer  
* "Jazza-Dada-Doo" — José Ferrer  
* "It" — Ann Miller  
* "Serenade" — William Olvis   James Mitchell  
* "Your Land and My Land" — Howard Keel (from 1927 musical My Maryland, lyrics by Dorothy Donnelly]
* "Auf Wiedersehn" — Helen Traubel (from 1915 musical The Blue Paradise, lyrics by Herbert Reynolds] Tony Martin with Joan Weldon (from 1928 operetta The New Moon, lyrics by Oscar Hammerstein II]
* "Stout-Hearted Men" — Helen Traubel  
* "When I Grow Too Old to Dream" — José Ferrer  
==Box Office==
According to MGM records the film earned $2,471,000 in the US and Canada and $1,507,000 elsewhere, resulting in a loss of $435,000. 
==Reception and distribution==
The film was not a critical success.  According to the reviewer for the New York Times, Deep in My Heart "calls for a strong digestive system and a considerable tolerance for clichés."   

However, the film is rich with the music of Sigmund Romberg. Rombergs songs are performed not only by stars José Ferrer and Helen Traubel, but also by a plethora of MGM stars giving delightful cameo performances.  Music lovers will enjoy the film and walk away singing Rombergs tunes.

Although the film is out of print in VHS and laserdisc formats, it has been released on DVD.   The soundtrack, earlier released on LP, was made available on iTunes in 2006.  The film has been shown on Turner Classic Movies.

==References==
 

==Further reading==
* Silverman, Stephen M. Dancing on the Ceiling: Stanley Donen and his Movies. New York: Knopf, 1996.  ISBN 0-679-41412-6.
* Tibbets, John C.  Composers in the Movies: Studies in Musical Biography.  New Haven: Yale University Press, 2005.  115-22.  ISBN 0-300-10674-2.

==External links==
 
* 
* 
*  

 

 
 
 
 
 
 
 