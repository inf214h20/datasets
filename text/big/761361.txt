Dog Park (film)
{{Infobox film
| name           = Dog Park
| caption        =
| image	         = Dog Park FilmPoster.jpeg
| director       = Bruce McCulloch
| producer       = Susan Cavan
| writer         = Bruce McCulloch
| narrator       =
| starring       = Luke Wilson Kathleen Robertson Gordon Currie Natasha Henstridge
| music          = Craig Northey
| cinematography = David A. Makin
| editing        = Norman Buckley Christopher Cooper
| studio         = Independent Pictures Accent Entertainment Lions Gate Films  (Canada) 
| released       = September 14, 1998 (Canada) September 24, 1999 (U.S.)
| runtime        = 91 min.
| country        = US/Canada English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} directed by Bruce McCulloch.

==Plot==
 
The main character Andy is a writer of newspaper classified ads who has been going from relationship to relationship since eighth grade. He loses custody of his dog when his girlfriend, Cheryl, breaks up with him for another man. Andy then meets Lorna, a childrens TV show host, but she is too obsessed with her own dog to commit to a relationship with anyone. Shes also still hurt emotionally because her boyfriend Trevor, left her for Andys ex, Cheryl. 

Andy also has a fling with another woman, Keiran. Meanwhile, Cheryl takes Andys dog to a psychiatrist, who tells her that her promiscuity is traumatizing the dog. While both Andy and Cheryl do their best to share custody, problems arise as Cheryl and Trevor break up and she tries to win Andy back unsuccessfully. Keiran figures out Jeris boyfriend Jeff is having an affair. Lorna goes out with Trevor. Its not much of date, but he helps her out in a big way. Both Andy and Lorna have feelings for each other, but arent sure how to follow through with it. Andy being with Kieran and Lorna being with Trevor help them come to a very important conclusion.

==Cast==
* Janeane Garofalo as Jeri
* Natasha Henstridge as Lorna
* Bruce McCulloch as Jeff
* Mark McKinney as Dog Psychologist
* Kathleen Robertson as Cheryl
* Harland Williams as Callum
* Luke Wilson as Andy
* Kristin Lehman as Keiran
* Amie Carey as Rachel
* Gordon Currie as Trevor
* Peter MacNeill as Neighbor
* Jerry Schaefer as Norm
* Zachary Bennett as Dougie Ron James as Male Dog Owner #1
* Albert Schultz as Male Dog Owner #2
* Terri Hawkes as Announcer Michael Bean as Driver

==Awards==
*Genie Awards
Won Best Performance by an Actor in a Supporting Role—Mark McKinney

Nominated for Best Original Screenplay—Bruce McCulloch

*Canadian Comedy Awards
Nominated for Film Directing—Bruce McCulloch

Nominated for Film Writing—Bruce McCulloch

Nominated for Film Performance Male—Mark McKinney

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 