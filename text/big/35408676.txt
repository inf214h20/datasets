The Beautiful City
{{Infobox film
| name           = The Beautiful City
| image          = 
| alt            =  
| caption        = 
| director       = Asghar Farhadi
| producer       = Iraj Taghipoor
| writer         = Asghar Farhadi
| starring       = Taraneh Alidoosti  Faramarz Gharibian  Babak Ansari   Hossein Farzi-Zadeh   Ahu Kheradmand
| music          = Hamidreza Sadri
| cinematography = Ali Loqmani
| editing        = Shahrzad Pouya
| studio         = 
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Iran
| language       = Persian
| budget         = 
| gross          = 
}}
The Beautiful City ( ) is a 2004 Iranian film directed by Asghar Farhadi.

==Plot==
Akbar has just turned eighteen. He has been held in a rehabilitation centre for committing murder at the age of sixteen when he was condemned to death. Legally speaking, he had to reach the age of eighteen so that the conviction could be carried out. Now, Akbar is transferred to prison to await the day of his execution. Ala, a friend of Akbar, who himself has undergone imprisonment for burglary, soon after his release tries desperately to gain the consent of Akbars plaintiff so as to stop the execution.

==Cast==
* Taraneh Alidoosti as Firoozeh
* Faramarz Gharibian as Abolqasem Rahmati
* Babak Ansari as Ala
* Hossein Farzi-Zadeh as Akbar
* Ahu Kheradmand as Mr. Abolqasems Wife

==Awards and nominations==
{| class="wikitable" style="font-size:100%;"
|-
! Year !! Group !! Award !! Recipients and nominees !!  Result
|- 2004
|Fajr Film Festival
| Crystal Simorgh Award – Best Sound Mixing
| Hassan Zahedi
|  
|-
| rowspan="2"| International Film Festival of India
| Golden Peacock
| Asghar Farhadi
|  
|-
| Special Jury Award
| Faramarz Gharibian
|  
|-
| Warsaw International Film Festival
| Grand Prix
| Asghar Farhadi
|  
|- 2005
| Split Film Festival
| FIPRESCI Prize
| Asghar Farhadi
|  
|}

==External links==
*  

 

 
 
 
 
 
 