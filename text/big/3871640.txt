I Not Stupid Too
{{Infobox film
| name        = I Not Stupid Too
| image       = INotStupidToo.jpg
| caption     =
| producer    = Chan Pui Yin Seah Saw Yam Daniel Yun
| director    = Jack Neo
| studio      = MediaCorp Raintree Pictures
| distributor = United International Pictures
| released    =  
| runtime     = 124 minutes Mandarin Hokkien
| country     = Singapore
| budget      =
| writer      = Jack Neo Shawn Lee Joshua Ang
}}
I Not Stupid Too ( ) is a 2006 Singaporean satirical comedy film and the sequel to the 2002 film, I Not Stupid. It portrays the lives, struggles and adventures of three Singaporean youths — 15-year-old Tom, his 8-year-old brother Jerry and their 15-year-old friend Chengcai — who have a strained relationship with their parents. The film explores the issue of poor parent-child communication.
 director and Shawn Lee, Joshua Ang and Ashley Leong. Filming took place at several Singaporean schools in June 2005.
 serialised version of the film was aired later that year.

==Plot== Shawn Lee), his younger brother Jerry (杨学强, Yáng Xuéqiáng; Ashley Leong) and their friend Lim Chengcai (林成才 Lín Chéngcái; Joshua Ang). 15-year-old Tom is technologically inclined and a talented blogger, while 8-year-old Jerry enjoys the performing arts and has the lead role in his school concert. Mr. and Mrs. Yeos (Jack Neo and Xiang Yun) busy schedules give them little time to spend with their children, leading to a strained relationship. With his mother absent, Chengcai was raised by his ex-convict father (Huang Yiliang), whose fighting skills he inherited.
 pornographic VCD. public caning for his part in the scuffle. Tom and Chengcai join a local street gang; as their initiation, they are forced to shoplift an iPod. However, they are caught by two conmen with connections to the street gang posing as police detectives, who demand that they pay the fine $2000 within two days or be arrested.

While tutoring his sons, Mr. Yeo tells them that people will pay $500 for an hour of his time. Jerry, who wants his parents to come to his school concert, starts saving money, but he cant save enough and eventually resorts to stealing. After he is caught, his furious father repeatedly canes him and shouts at him, but forgives him when the boy explains that he wanted $500 to "buy" an hour of his fathers time. This prompts Mr. and Mrs. Yeo to read Toms blog and realise how unappreciated and alienated their children feel.
 hell money; the conmen are then arrested by real police officers who have been waiting in ambush close by. Having finally understood their children, the Yeo parents watch Jerrys concert, much to his delight.

Later, the gangsters whom Chengcai bumped into earlier beat him up. Mr. Lim, who happens to be nearby, tries to protect his son, but suffers head trauma after being pushed down the stairs. He is taken to a hospital, critically injured. On his deathbed, Mr. Lim tells Chengcai that he loves him and that he should pursue his talent for fighting. Witnessing this scene, the principal is touched and allows Chengcai to return to school. The boy eventually becomes an internationally recognised martial artist.

==Production==
After the release of I Not Stupid, a sequel was suggested, but Neo had difficulty finding a suitable topic. Mervyn Tay, "I Not Stupid again", Today (Singapore newspaper)|Today, Singapore, 1 June 2005.  His inspiration was a book on appreciation education, a method of teaching developed by Chinese educator Zhou Hong.  Through the movie, Neo hoped to capture the culture of Singapore at the turn of the millennium, Ng Bao Ying. "I Not Stupid Too tackles communication problems in families", Channel NewsAsia, Singapore, 18 January 2006.  and to explore the issue of poor parent-child communication. 

Neo and   had agreed to co-produce I Not Stupid Too with Raintree Pictures, but backed out because they found the film too liberal. Wendy Teo, "Jacks not stupid",  . 

Filming took place at Saint Hildas Primary School, Presbyterian High School and other locations during the school holidays in June.  Neo hired real gangsters to act in several gangster scenes as he was dissatisfied with the extra (actor)|extras. According to him, communicating with the gangsters was difficult, but when he decided to apply the lessons from the movie and praised them for a good take, they reacted well.  Several members of the cast also said that I Not Stupid Too inspired them to communicate better with their family members.  On 26 January 2006, distributor United International Pictures released I Not Stupid Too on 36 screens in Singapore. 

==Reception==
With earnings of over S$1.41 million in the first six days, I Not Stupid Too achieved the biggest opening for a Singaporean film.  The movie rose to the top of the local box office, beating Jet Lis Fearless (2006 film)|Fearless.   In total, I Not Stupid Too grossed over S$4 million, becoming Singapore’s second-highest grossing movie after Money No Enough.  "I Not Stupid Too crosses $4m mark in box office takings", Channel NewsAsia, Singapore, 6 March 2006.  The film was then released in Malaysia, where it made RM1.1 million, and Hong Kong, taking in HK$3.1 million.  Following the success of the two I Not Stupid films, Neo has announced plans to make more sequels,   as well as a remake set in China. 

I Not Stupid Too was well received when it was showcased at the  , 26 October 2007.  The film was also nominated for Best Asian Film at the Hong Kong Film Awards, but lost to Riding Alone for Thousands of Miles. 

Critics praised I Not Stupid Too for its touching portrayal of the problems faced by Singaporean teenagers. According to a review in the   said that the movie "feels like a public service program written by Singapores social welfare department".  Geoffrey Eu, a reviewer for The Business Times, commented that it "takes the line that the viewer needs to be clubbed into submission rather than persuaded via a more subtle line of reasoning". 

==References==
 

==External links==
 
* 
* 

 

 

 
 
 
 
 
 
 
 