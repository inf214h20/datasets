Titanic (1915 film)
{{Infobox film
| name           = Titanic 
| image          =
| caption        =
| director       = Pier Angelo Mazzolotti 
| producer       = 
| writer         =  
| starring       = Mario Bonnard   Giovanni Casaleggio   Pierino Chiesa
| music          = 
| cinematography = Luigi Filippa
| editing        = 
| studio         = Bonnard Film  
| distributor    = Bonnard Film 
| released       = April 1915
| runtime        = 
| country        = Italy
| awards         =
| language       = Silent   Italian intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
Titanic is a 1915 Italian silent film directed by Pier Angelo Mazzolotti and starring Mario Bonnard, Giovanni Casaleggio and Pierino Chiesa. Despite its title, the film is not about the Sinking of the RMS Titanic but the discovery of a mineral of the same name. 

==Cast==
* Mario Bonnard
* Giovanni Casaleggio 
* Pierino Chiesa 
* Elide De Sevres 
* Luigi Duse 
* Felice Metellio

== References ==
 

== Bibliography ==
* Bottomore, Stephen. The Titanic and Silent Cinema. The Projection Box, 2000.

== External links ==
*  

 
 
 
 
 


 