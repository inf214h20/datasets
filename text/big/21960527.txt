Warm Summer Rain
{{Infobox Film
| name           = Warm Summer Rain
| image          = Warm Summer Rain.jpg
| image_size     = 
| caption        = 
| director       = Joe Gayton Lionel Wigram (producer)
| writer         = Joe Gayton
| narrator       = 
| starring       = Kelly Lynch Barry Tubb
| music          = Roger Eno
| cinematography = Fernando Argüelles
| editing        = Robin Katz Ed Rothkowitz	 
| distributor    = 
| released       = 
| runtime        = 82 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Warm Summer Rain is a 1989 drama film written and directed by Joe Gayton, starring Kelly Lynch and Barry Tubb.

==Synopsis== depressed thirty-something who unsuccessfully attempts suicide by slitting her wrists. Reflecting briefly upon her circumstance in the hospital, she realizes that nothing has been solved, whereupon she vacates the premises, wearing nothing but a hospital gown, a black coat, and flimsy sandals.  Shortly thereafter, at a bus station, she requests from the attendant a bus ticket to wherever, with consideration to the limited funds she has to spare. The attendant insists on a destination, or even a direction – Kate spins about, points, and says, "that way".

She then gets off the bus in the middle of nowhere, and after turning down a ride from a handsome stranger (Barry Tubb) in a Mustang convertible, she walks though the desert and finds an isolated bar.  Getting drunk at the bar, she parties with some people in the bar.  The stranger who tried picking her up along the road comes in and eventually she passes out.   She wakes up the next morning in an abandoned house, with the stranger, wearing a ring.  He tells her that they were married in the bar and shows her some Polaroid photos of the "ceremony".

Hiding out in an abandoned house in the desert, the pair embarks on a torrid journey of sensual and romantic discovery, who rekindles her will to live, but it soon becomes clear that things cant stay this way forever.

==Cast==
* Kelly Lynch ................... Kate
* Barry Tubb .................... Handsome Stranger
* Ron Sloan ..................... Andy
* Larry Poindexter ............. Steve

==Notes==
* The film contains much nudity, graphic sexual scenes, and extremely profane language.
* Feature directorial debut for Joe Gayton.
* Began shooting 19 August 1988, released theatrically in April 1989
* The film was theatrically released in a widescreen format, however the DVD released by MGM is cropped to the 4:3 Pan/Scan format.

==External links==
* 

 
 
 
 