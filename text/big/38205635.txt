Idealist (film)
 
{{Infobox film
| name           = Idealist
| image          = 
| caption        = 
| director       = Igor Pretnar
| producer       = 
| writer         = Ivan Cankar Igor Pretnar
| starring       = Radko Polič
| music          = 
| cinematography = Mile de Gleria
| editing        = 
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = Yugoslavia
| language       = Slovene
| budget         = 
}}

Idealist is a 1976 Yugoslav drama film directed by Igor Pretnar. It was entered into the 10th Moscow International Film Festival where Radko Polič won the award for Best Actor.   

==Cast==
* Radko Polič as Martin Kacur
* Milena Zupančič as Toncka
* Dare Ulaga as Ferjan
* Stevo Žigon as Priest from Zapolje
* Arnold Tovornik as Priest from Blatni Dol
* Bert Sotlar as Mayor from Blatni Dol
* Janez Albreht as Grajzar
* Marjeta Gregorac as Minka

==References==
 

==External links==
*  

 

 
 
 
 
 
 