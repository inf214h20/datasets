Black Octopus
{{Infobox film
| name           = Black Octopus / El pulpo negro
| caption        = 
| image          = 
| director       = Marta Reguera
| producer       = 
| writer         = Luis Murillo
| starring       = Narciso Ibáñez Menta Osvaldo Brandi Héctor Biuchet
| music          = 
| cinematography = 
| editing        = 
| studio         = Telearte Argentina Canal 9
| released       = 1985
| runtime        = 624 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
}} 1985 Argentine TV mini-series (624 min) directed by Marta Reguera and written by Luis Murillo with Narciso Ibáñez Menta in the main role. 

==Plot==
A man extorts several people so that they kill other people and leave the figure of a black octopus beside each cadaver.

==Cast==

* Narciso Ibáñez Menta as Arturo Leblanc / Héctor de Rodas / Claudio Leonard
* Osvaldo Brandi as Méndez
* Héctor Biuchet as Guevara
* Ariel Keller as Velázquez
* Juan Carlos Puppo as Duarte
* Beatriz Día Quiroga as Martha
* Tony Vilas as Armando
* Oscar Ferrigno as Police Inspector Alejandro Mendoza
* Juan Carlos Galván as Detective Marcos de la Hoz
* Erika Wallner as Eva Rinaldi
* Cristina Lemercier as Mara Salerno
* Wagner Mautone as Alfredo Heredia
* Zelmar Gueñol as Proprietor of Mortuary
* Villanueva Cosse as Martín Gávez
* Max Berliner as Collaborator of Radial Program
* Alfredo Iglesias as Enrique Ferrantes

==References==
 

==External links==
*  
*  
* El pulpo negro at the YouTube: part  ,  ,  ,  ,  

 

 
 
 
 