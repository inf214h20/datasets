The Mistress (2012 film)
{{Infobox film
| image            = The_Mistress(2012_Film).jpg
| caption          = Theatrical movie poster
| name             = The Mistress
| director         = Olivia Lamasan
| producer         =  
| music            =  
| cinematography   = Hermann Claravall
| story            =  
| screenplay       = Vanessa Valdez
| writer           =  
| editing          = Marya Ignacio
| starring         =  
| distributor      = Star Cinema
| released         =  
| country          = Philippines
| language         =  
| runtime          = 125 minutes   
| budget           =
| gross            = PHP 262.82 million 

 

 
}}
 romantic melodrama film directed by Olivia Lamasan,starring John Lloyd Cruz and Bea Alonzo with Ms.Hilda Koronel and   Ronaldo Valdez .   
 Miss You Like Crazy.
 highest grossing highest grossing Filipino film of all time.  {{cite web|url=http://forums.abs-cbn.com/star-cinema/list-of-highest-grossing-films-in-the-philippines-(ot)-8774|title=
List of highest-grossing films in the Philippines (OT)|publisher=ABS-CBN Forums|accessdate=November 16, 2013}}   It also won and received six several awards (including the cast and crew) in the 31st Luna Awards  and received nominations in 61st FAMAS Awards,  Golden Screen Awards,  and Star Awards for Movies  in 2013 respectively. Due to the films success, it was screened internationally in selected countries. 
  
Alonzo and Cruz also had their TV series entitled A Beautiful Affair that aired in ABS-CBN last October 29, 2013 and ended in January 18, 2013, is also part of their 10th-year anniversary as love team and the success of The Mistress.  

==Plot==

A young woman is torn between the affections of her two lovers. One is a young bachelor who brings passion into her life. The other is a married man who has kept her as his mistress for years.   

Architect JD (John Lloyd Cruz) has a chip on his shoulder about his businessman father Rico (Ronaldo Valdez) never really accepting him as his son. Seamstress Sari (Bea Alonzo) struggles to take care of her family. The two meet by chance, and JD aggressively pursues Sari’s affections. Though the two are clearly a good match, there is a problem standing in the way of their bliss: Sari is the mistress of JD’s father. Against his better judgment, JD continues to court Sari even after finding out this difficult fact. He hides his true identity as Rico’s son, and fights to win Sari’s heart.

==Cast==

===Main Cast===

 
*John Lloyd Cruz   as Frederico "JD / Eric" Torres Jr.
*Bea Alonzo       as Rosario "Sari" Alfonso
*Ronaldo Valdez    as Frederico "Rico" Torres
*Hilda Koronel    as Regina Torres

===Supporting Cast===

*Anita Linda as Lola Lina
*Carmi Martin as Marrion Alfonso
*Clarence Delgado as Mamon
* Tony Mabesa as Maestro Emil
*K Brosas as Rosa
* Minnie Aguilar as Olga
*Gabe Mercado as Mr. Zarate
*Nor Domingo as Brian

===Extended Cast===

 

*Carla Humphries as Carly
*Al Gaitmatan as Arnold
*Marj Lorico as Julie
*Eileen Gonzales as Abby
*Patrick Moreno as Arvin
*Nina Guirnaldo as Guia
*Jessica Rose Ashton as Lexie
*Dustin Guia as Joey
*Karl Ernest Alano as Aljo
*Joel Molina as Alex
 

==Production==

===Background===
 One More Miss You Like Crazy (2010), resumes in "The Mistress". The film also celebrates their joint 10th Anniversary in show business.  Described as an "imperfect love story," the film features a more daring and mature Alonzo  and Cruz. 

===Filming===

The film was shot on various locations. The Callao Cave in Peñablanca, Cagayan was closed for two days on 8 and 9 June for the shooting of the movie. They also used various locations in the Cagayan province, especially in Tuguegarao City and the Baua Bridge in Gonzaga, Cagayan, while the beach scenes were at the Anguib Beach and "Boracay of the North" in Santa Ana, Cagayan. Release date is 12 September 2012 in theatres nationwide. 

===Music===
The only song that is being played throughout the film is the hit single "Chasing Cars", written and performed by the Irish/Scottish alternative rock band "Snow Patrol". 

==International Screening==

Here are some selected international screening dates and venues of The Mistress:      
 
{| class="wikitable"
|-
! Date !! Country !! Notes/Venue !! Reference
|-
| September 14 || Guam || Micronesia Mall Theaters only ||      
|-
| September 14 October 22 || New Zealand || Auditorium Theater and SkyCity Convention centre only ||      
|-
| September 15 || Europe || Nice, France ||      
|-
| September 21–27 || USA || Select Cities and Theaters only ||       
|-
| rowspan=3| September 27 || UAE || Select Theaters only ||      
|-
| Qatar || Cine Center ||      
|-
| Bahrain || Seef Cineplex ||      
|-
| rowspan=2| September 28 || rowspan=2| Canada || Edwards Cerritos Stadium ||      
|-
| Select Cities and Theaters only ||      
|}
(notes: Please refer to the references links for the exact information for the International Screening of The Mistress)

==Reception==

===Critical response===
{{Album ratings
| rev1 = Click The City
| rev1Score =   
| rev2 = User Reviews
| rev2Score =   
| rev3 = Rotten Tomatoes
| rev3Score =   
}}

The Mistress received widely positive reviews. According to Rotten Tomatoes, the film holds 83% rating and an average rating score of 3.9 out of 5.   

Philbert Ortiz Dy gave the film 3 out of 5 stars. He stated that The Mistress doesn’t do a very good job of exploring what it is to be a mistress.    It only depicts a few stolen moments from the adulterous relationship, focusing instead on the budding romance between the two younger characters. The movie isnt as complex as the premise makes it sound. Though it tackles mature subject matter and plays with Oedipal themes, the film is really much more about the love story than it is about the adultery.  He also stated that The Mistress can still be quite affecting. Taken away from the larger narrative, the movie is able to build a compelling fantasy that’s far more romantic than one would expect and It still makes a compelling case for itself every now and then. 

Phillip Cu-Unjieng of The Philippine Star magazine gave also a positive feedbacks to The Mistress. He states that Star Cinemas The Mistress is a superb acting vehicle for both Bea Alonzo and John Lloyd Cruz, proving that even when tackling more mature roles, their chemistry, after a full decade, remains intact. {{cite web|url=http://www.philstar.com/Article.aspx?articleId=848674|title=
The chemistry of tears... and years|publisher=Philippine Star|accessdate=September 23, 2012}}  And The Mistress has it’s texture of the screenplay and the sympathetic depth of the characters that make it such an absorbing film. 
 Yahoo Philippines, The Mistress may be a serious film, but it can make moviegoers break into smiles with snippets of humor and “It’s a suspension of disbelief that will amaze even the most rabid mistress hater.”   

Mark Angelo Ching and Jocelyn Dimaculangan of PEP also gave a positive response to the film, they stated that, "This movie casts a sympathetic look at someone who agrees to be a kept woman in exchange for a huge debt. But far from glorifying the mistress, the film delves into the heartwrenching pain brought about by being caught in this four-sided love affair."   

===Box office===
 second highest grossing Filipino film of all time and the biggest local box-office hit this year after it remained the top-grossing movie in the Philippines for a second week in a row. 

In contrary to the reports by its own distributor, according to Box Office Mojo, the film grossed only PHP 254 Million only in October 7, 2012, still being the third highest grossing film in the Philippines.    As of its 5th and final week of run, the films total gross is PHP 262,790,300 Million Pesos, almost surpassed No Other Woman with a box-office gross of PHP 278,418,883 Million pesos. 
 highest grossing all time highest grossing local film.

===Accolades===
{| class="wikitable"
|+ List of awards and nominations
! Award !! Category !! Recipients and nominees !! Result !! Source
|- 2013 GMMSF 44th GMMSF Box-Office Entertainment Awards (2013)
| Most Popular Screenwriter
| Olivia Lamasan and Vanessa Valdez
|  
| rowspan=3|  
|- Box Office King
| John Lloyd Cruz
|  
|- Box Office Queen
| Bea Alonzo
|  
|- FAMAS Awards (2013)
| Best Picture
| The Mistress
|  
| rowspan="12"|          
|-
| Best Director
| Olivia Lamasan
|  
|-
| Best Actress
| Bea Alonzo
|  
|-
| Best Supporting Actress
| Hilda Koronel
|  
|-
| Best Supporting Actor
| Ronaldo Valdez
|  
|-
| Best Child Actor
| Clarence Delgado
|  
|-
| Best Screenplay
| Vanessa Valdez
|  
|-
| Best Cinematography
| Hermann Claravall
|  
|-
| Best Production Design
| Shari Marie Terese E. Montiague 
|  
|-
| Best Editing
| Marya Ignacio
|  
|-
| Best Musical Score
| Von de Guzman and Jessie Lasaten
|  
|-
| Best Sound
| Aurel Claro Bilbao
|  
|-
| rowspan="11"| 31st Luna Awards (2013)
| Best Director
| Olivia Lamasan
|  
| rowspan="6"|       
|-
| Best Actor
| John Lloyd Cruz
|  
|-
| Best Supporting Actor
| Ronaldo Valdez
|  
|-
| Best Supporting Actress
| Hilda Koronel
|  
|-
| Best Screenplay
| Vanessa Valdez
|  
|-
| Best Editing
| Marya Ignacio
|  
|-
| Best Picture
| The Mistress
|  
| rowspan="5"|     
|-
| Best Actress
| Bea Alonzo
|  
|-
| Best Cinematography
| Herman Claravall 
|  
|-
| Best Music
| Von de Guzman and Jessie Lasaten
|  
|-
| Best Sound
| Aurel Claro Bilbao
|  
|-
| rowspan="6"| Golden Screen Awards (2013)
| Best Motion Picture (Drama film|Drama)
| The Mistress
|  
| rowspan="6"|       
|-
| Best Performance by an Actress in a Leading Role (Drama)
| Bea Alonzo 
|  
|-
| Best Performance by an Actor in a Leading Role (Drama)
| John Lloyd Cruz
|  
|- Musical or Comedy film|Comedy)
| Hilda Koronel
|  
|-
| Best Performance by an Actor in a Supporting Role (Drama, Musical or Comedy)
| Ronaldo Valdez
|  
|-
| Best Director
| Olivia Lamasan
|  
|-
| rowspan="13"| Star Awards for Movies (2013)
| Movie of the Year
| The Mistress
|  
| rowspan="13"|       
|-
| Movie Director of the Year
| Olivia Lamasan
|  
|-
| Movie Actor of the Year
| John Lloyd Cruz
|  
|-
| Movie Actress of the Year
| Bea Alonzo
|  
|-
| Movie Supporting Actor of the Year
| Ronaldo Valdez
|  
|-
| Movie Supporting Actress of the Year
| Hilda Koronel
|  
|-
| Child Performer of the Year
| Clarence Delgado
|  
|-
| Movie Screenwriter of the Year
| Vanessa Valdez
|  
|-
| Movie Cinematographer of the Year
| Hermann Claravall
|  
|-
| Movie Production Designer of the Year
| Shari Marie Terese E. Montiague 
|  
|-
| Movie Editor of the Year
| Marya Ignacio
|  
|-
| Movie Musical Scorer of the Year
| Von de Guzman and Jessie Lasaten
|  
|-
| Movie Sound Engineer of the Year
| Aurel Claro Bilbao
|  
|- 
|}

==See also==
* My Neighbors Wife In the Name of Love
* No Other Woman
* A Secret Affair One More Try
* The Bride and the Lover
* When the Love Is Gone Trophy Wife

==References==
 

==External links==
* 
* 
*  at Box Office Mojo
* 
* 
*  at ABS-CBN Forums
*  at Click The City
* 

 
 
 
 
 
 
 
 
 