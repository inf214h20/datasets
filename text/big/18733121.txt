The Immortal Heart
{{Infobox film
| name           = The Immortal Heart
| image          = 
| caption        = 
| director       = Veit Harlan
| producer       = Gerhard Staab
| writer         = Richard Billinger Werner Eplinius Veit Harlan Walter Harlan
| starring       = Heinrich George
| music          = 
| cinematography = Bruno Mondi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Nazi Germany 
| language       = German
| budget         = 
}}

The Immortal Heart ( ) is a 1939 German drama film directed by Veit Harlan and starring Heinrich George.    

It was based on Walter Harlans play The Nuremberg Egg. Cinzia Romani, Tainted Goddesses: Female Film Stars of the Third Reich p86 ISBN 0-9627613-1-1 

==Cast==
* Heinrich George as Peter Henlein
* Kristina Söderbaum as Ev
* Paul Wegener as Dr. Schedel
* Raimund Schelcher as Konrad Windhalm
* Michael Bohnen as Martin Behaim
* Paul Henckels as Güldenbeck
* Ernst Legal as Bader Bratvogel
* Eduard von Winterstein as Richter Sixtus Heith
* Franz Schafheitlin as Burghauptmann Zinderl
* Jakob Tiedtke as Schöffe Weihrauch
* Wolf Dietrich as Graf Pankraz

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 