Thumbs Up (film)
{{Infobox film
| name           = Thumbs Up
| image          =
| caption        =
| director       = Joseph Santley
| producer       = Albert J. Cohen
| writer         = Frank Gill Jr.   Ray Golden   Henry K. Moritz   Paul Girard Smith Brenda Joyce Richard Fraser Elsa Lanchester   Arthur Margetson
| music          = Walter Scharf   Marlin Skiles Ernest Miller Thomas Richards
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       = July 5, 1943
| runtime        = 67 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} musical drama Brenda Joyce, Richard Fraser and Elsa Lanchester. For a publicity stunt to boost her career an American nightclub singer volunteers for a stint in a British munitions factory. She is so impressed by the spirit of her fellow workers that she decides to stay on.  The United States Office of War Information strongly approved of the film which they felt showed a more realistic, democratic version of modern Britain than most other Hollywood films of the period. 

==Main cast== Brenda Joyce as Louise Latimer  Richard Fraser as Douglas Heath 
* Elsa Lanchester as Emma Finch 
* Arthur Margetson as Bert Lawrence 
* J. Pat OMalley as Sam Keats 
* Queenie Leonard as Janie Brooke 
* Molly Lamont as Welfare Supervisor 
* Gertrude Niesen as Herself  George Byron as Foreman  Charles Irwin as Ray Irwin - Orchestra Leader 
* André Charlot as E.E. Cartwright

==References==
 

==Bibliography==
* Glancy, H. Mark. When Hollywood Loved Britain: The Hollywood British Film 1939-1945. Manchester University Press, 1999.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 