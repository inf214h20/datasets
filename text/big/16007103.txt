The Oil Factor
 
{{infobox film
|film= Oil Factor  Behind the War On Terror 
|genre= dokument
|director= Gerard Ungerman, Audray Brohy produced by= Free-Will Production
|editor=	
|distribution= 
|released=  
|length= 93 minutes
|language= English
}}
The Oil Factor, alternatively known as Behind the War on Terror, is an approximately 90 minute 2004 movie written and directed by Gerard Ungerman and Audrey Brohy, narrated by Ed Asner. The documentary analyzes the development of some global events since the beginning of the century (especially after the 9/11 terrorist attacks) from the perspective of oil and oil-abundant regions. 

The documentary aspires to bring an untraditional point of view over the reasons, aspects and motives of this war and the direction of current US foreign policy.

==Interviews==
Respondents, featuring in the Oil Factor, include: DoD adviser
*Noam Chomsky, professor at MIT
*Gary Schmitt, executive director of the Project for a New American Century
*Paul Bremer, temporary (Iraqi) coalition leader
*Karen Kwiatkowski, retired military adviser in The Pentagon
*Azees Al-Hakim, member of current Iraqi government
*Michael C. Ruppert, author of From the Wilderness, studying the peak oil issue (among others) French press agency in Jordan
*Gen. Piérre-Marie Gallois, energy-strategy analytist
*David Mulholland, editor of magazine focused on military technology
*Ahmed Rashid, author of "The Taliban" book

==Locations==
The filmmakers were shooting in several locations of Afghanistan, Pakistan and Iraq (besides the USA), interviewing local people or local authorities mostly on the influences and ramifications of the Operation Enduring Freedom and president George Walker Bush|Bushs spreading of democracy in the respective regions.

==Introductory presupposition==
During the film, spectator is given some prepositions and axioms that become a basis for film authors argumentation, such as:
*Oil is indispensable in every aspect of our modern-way lives.
*World food production is 95% dependent on hydro-carbon energy.
*Demand for oil is and will be growing as new markets (e.g. India and China) gains in strength and local consumers start to demand higher life standard.
*3/4 are the worlds oil discoveries located in Middle East, as well as the ratio of the volume of oil needed to be imported to the United States.
*From 2010 on, economies of some continents or world regions will run out of oil and that will make them utterly dependent on foreign oil supply. The film says: "The reality is, however, that major conflicts are likely to erupt before any of these players actually runs out of oil."

==Argumentation==
On the basis of these presuppositions, the film tries to seek a step taken in the name of US foreign policy from this point of view. It relies to the motives of the United States of 2000 to build new military bases in the Middle East for the sake of increase of their strategical power. It comes to conclusion that the best candidate to this was Iraq as the country with second biggest storages of oil and its military paralyzed by dozen years of bombing on a weekly basis. The possibility to potentially stop oil export from this country equals the political power in the region (according to David Mulholland).

Oil Factor is also skeptical about consequences of all current war both on local inhabitants and American soldiers.

==Iraq==
The documentary first analyzes the development of support of Iraqi citizens that showed exacerbation approximately year and half after the invasion. It also touches the issue of 320+ tons of American munition made of depleted uranium since the first Gulf War and its consequences to local inhabitants, as well as the discrepancy of Paul Bremers pledge to provide truly democratic elections with choosing representatives with pro-American who represent neither the Shiites majority nor Islamists as such. According to Shiites (present in neighboring Iran, current economic US enemy) it mentions the Iran-Contra affair of 1979 and Shiite support of the Hizballah as an aspect the United States "will not tolerate".

==Afghanistan==
  Operation Enduring Freedom), introduced by the film as a "war virtually forgotten by media" begins with a rhetoric question of why the coalition units invaded the "extremely poor and desolated country" and why this military operation, allegedly waged for capturing Osama bin Laden and other Al Qaeda members, involves such vast concentration of American military technologies and building big permanent military bases, supposing this search and destroy mission would last for decades.

The documentary answers this question via Ahmed Rashid - according to his reasoning the clandestine reason is upcoming struggle for dwindling energy sources like oil and natural gas, abundantly present in the area of Central Asia|Central-Asian states - Turkmenistan, Uzbekistan, Kyrgyzstan a Kazakhstan. The players of this struggle are Russia, China and the United States of America. While both China and Russia neighbors with at least some of the mentioned countries, USA do not and if they want to import Central-Asian oil or gas, they have to establish a pipeline to the Indian ocean. Such pipeline would have to run over Pakistan and Afghanistan. While Pakistani authorities would not object the construction, Afghan Taliban members and local warlords embody a peril of the intact existence of the pipeline.

==American presence in oil-rich regions==
In the last part of Oil Factor, the filmmakers go in for coalition (and especially US) soldiers, negatively acknowledge media campaigns to aid to recruit another young American men to join US Army and object that clandestine agents are best known and verified way to fight terrorism, instead of huge conventional waging of war.
  ended, what You see is American military hegemony - covering 90 per cent of global energy resources."

==See also==
*Michael Klare
*Trans-Afghanistan Pipeline

==Books==
*Michel Chossudovsky|Chossudovsky, M.: War and Globalisation: The truth about September 11, ISBN 80-903355-0-0
*Zbigniew Brzezinski|Brzezinski, Z.: The Grand Chessboard: American Primacy and Its Geo strategic Imperatives, Basic Books, 1998, ISBN 0-465-02726-1,  

==External links==
* 
*  on IMDB
*  on Google Video
*  on Democracy Now!
*  

<!--
===Other resources===
* ,  ,  ,  ,  ,  ,  ,  
-->

 
 
 
 
 
 