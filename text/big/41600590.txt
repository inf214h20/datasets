99 Homes
 
{{Infobox film
| name           = 99 Homes
| image          = 99 Homes.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Ramin Bahrani
| producer       = Ashok Amritraj   Ramin Bahrani   Andrew Garfield   Justin Nappi   Kevin Turen
| writer         = Ramin Bahrani
| starring       = Andrew Garfield   Michael Shannon   Laura Dern   Noah Lomax
| music          = Antony Partos
| cinematography = Bobby Bukowski
| editing        = 
| studio         = Noruz Films
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

99 Homes is a 2014 American drama film directed by Ramin Bahrani, and written by Bahrani and Amir Naderi. The film stars Andrew Garfield, Michael Shannon, Laura Dern and Noah Lomax. It competed for the Golden Lion at the 71st Venice International Film Festival.       It also screened in the Special Presentations section of the 2014 Toronto International Film Festival   , as well as the Telluride Film Festival.

== Plot ==
99 Homes is about a father, evicted from his home by a corrupt real estate broker and forced to move his family into a motel, who goes to work for the man who evicted him in hopes of reclaiming his house.

== Cast ==
* Andrew Garfield as Dennis Nash 
* Michael Shannon as Rick Carver 
* Laura Dern as Lynn Nash 
* Noah Lomax as Connor Nash 
* Tim Guinee as Frank Greene
* Cynthia Santiago as Mrs. Greene
* Manu Narayan as Khanna
* Cullen Moss as Bill
* Judd Lormand as Mr. Hester
*  Jonathan Vane as Adam Bailey
* Alex Aristidis as Alex Greene
* Alex Mendez as Connors friend
* Adam Mendez as Connors friend
* Liann Pattison as Dayna
* Jayson Warner Smith as Jeff
* James Guidry as James
* Sharon Farmer as Sharon
* Kyle Ching as Kyle
*Lynn Chambers as Locksmith

== Production ==
=== Casting ===
On July 24, 2013, Andrew Garfield joined the drama film 99 Homes to play Dennis Nash, an unemployed contractor who loses his home to foreclosure.    Later on 13 September, Michael Shannon joined the cast of the film, hell play Rick Carver, who teaches Dennis the legal and illegal ins and outs of the foreclosure game.    On December 10, Laura Dern also joined the cast of the film to play Lynn Nash, Dennis’s widowed mother.    On January 6, 2014, Noah Lomax joined the cast of the film to play Connor Nash, Dennis sarcastic son.   

=== Filming ===
The filming of 99 Homes began on November 18, 2013 in New Orleans which took a holiday break from Christmas to New Year on December 20.  Later, the film resumed shooting on January 6, 2014. 

=== Music ===
The films music will be scored by Antony Partos. 

=== Awards ===
Venice International Film Festival (2014) – SIGNIS Award Honorable Mention 

Venice International Film Festival (2014) Young Jury Members – Best Film

== Marketing ==
Following its 2014 Toronto International Film Festival premiere, the film was acquired by Broad Green Pictures for three million dollars. It is scheduled to be released on September 25, 2015.

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 