Papy fait de la résistance
{{infobox film
| name = Papy fait de la résistance
| image = 
| director = Jean-Marie Poiré
| starring = Christian Clavier Michel Galabru Roland Giraud Gérard Jugnot Dominique Lavanant Jacques Villeret Josiane Balasko
| runtime = 102 minutes
| released =  
| country = France
| language = French
| budget =
| gross = $24,624,492 
}} cult France|French film directed by Jean-Marie Poiré in 1983.

== Plot ==
Héléna Bourdelle, called "The Bourdelle," is a great singer and wife of maestro André Bourdelle. Joined the Resistance, he is killed by the explosion of a grenade. Following the defeat, the family sees Bourdelle soon his mansion invested by German forces. Complaining to the Kommandantur excesses, the Bourdelle, his daughters and their tenant help by chance an English soldier to escape and are then forced to hide it. The family, which is facing their former caretaker Ramirez became Gestapo agent, is best considered by the General Spontz that will bind with Bernadette Bourdelle. But he knows that Guy-Hubert, son of the family, a seemingly cowardly and effeminate hairdresser, is actually the elusive vigilante known as "Super-Resistant". As the tenant Michel Taupin, unhappy lovers Bernadette Bourdelle, who initially had designs on Colette, her insistent desire to join the Resistance leads many adventures. Imprisoned after the episode of the Kommandantur, he meets a tough, Felix / Frémontel who confides in him, thinking about to be shot, but thats released by Super-Resistant, and Felix who is cluttered a pot of glue ... Although she had vowed not to sing so that there would be a German in France, Helena Bourdelle is forced by General Spontz to attend a reception in honor of Hitlers half-brother, Ludwig Marshal von Apfelstrudel. Resistant entrusted with the task of Michel Taupin sauté a bomb. The story seems to end, but proves to be a "film within the film," and gives way to a contemporary television debate, designed to address the period of occupation, and to report on the reality of the depicted events in the film. The show brings together Bernadette Bourdelle, his brother Guy Hubert, Adolfo Ramirez (son of Ramirez, who came from Bolivia to defend his fathers memory), General Spontz and Michel Taupin (Minister of Veterans Affairs). Soon, the discussion turns to disaster: Ramirez Junior insult and defame the other protagonists of the story, who start to beat him on the TV set, forcing the host to cut the antenna.

==Cast==
*Christian Clavier as Michel Taupin
*Michel Galabru as Jean-Robert Bourdelle "Papy"
*Dominique Lavanant as Bernadette Bourdelle
*Jacqueline Maillan as Helena Bourdelle
*Jacques Villeret as Ludwig Von Apfelstrudel
*Roland Giraud as Hermann Spontz
*Gérard Jugnot as Ramirez
*Martin Lamotte as  Guy-Hubert Bourdelle
*Pauline Lafont as  Colette Bourdelle
*Julien Guiomar as  Colonel Vincent
*Jean Carmet as  André Bourdelle
*Josiane Balasko as The pharmacist
*Michel Blanc as Leboeuf father, the priest
*Jean-Claude Brialy as tennis player sycophant
*Bernard Giraudeau as resistant arrested
*Jacques François as Jacques de Frémontel, "Félix"
*Thierry Lhermitte as Colonel SS
*Jean Yanne as the militiaman Murat
*Roger Carel as General Muller
*Bruno Moynot as Flandu
*Jean Négroni as the narrator

==References==
 

==External links==
*  

 
 
 
 
 

 