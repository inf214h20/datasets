Two & Two
 

 

{{Infobox film
| name           = Two & Two
| image          = File:Two%26two.PNG
| alt            = 
| caption        = 
| film name      = 
| director       = Babak Anvari
| producer       = Babak Anvari Kit Fraser
| writer         = Babak Anvari Gavin Cullen
| based on       =  
| released       =  
| runtime        = 7 minutes
| country        = Iran
| language       = Persian
}}
Two & Two is a 2011 short film directed by Babak Anvari, co-produced by Babak Anvari and Kit Fraser and written by Babak Anvari and Gavin Cullen. The short film, which runs for 7 minutes, stars a male teacher and twelve students in a grey wall classroom, showing the first lesson which is an expression of 2 + 2 = 5.

== Plot ==

The film starts when a male teacher comes to the grey classroom. The headmaster says "good morning" and announced a message that there will be ongoing changes in the school, and said that the students teachers will advise them further. The teacher tells the children that two plus two equals five. He told the children to be silent, otherwise to have order in the classroom. The teacher writes 2 + 2 = 5 on the chalkboard and tells the children to repeat after him, in order to know. A student asked the teacher that two plus two is four, then the teacher said that two plus two was actually five, and told him to sit down and be quiet. The teacher told them to put their notebooks out to write 2 + 2 = 5. While the teacher was saying the expression, another student was saying that two plus two was four. The teacher asked him who gave the student permission to talk, and scolded the student. He was ordering the student to say 2 + 2 = 5 but the student denied. A student was saying 2 + 2 = 4, but the teacher was ordering to face the front. Then, the teacher sent the 3 older students and the older three students said two plus two equals five was true. A student had to stand next to the teacher. The teacher was holding the chalk. When the teacher said to the student that it was his last chance, he cleaned the 5 and replaced it with 4. At the end, the older students started shooting at the classroom and injured the student. An injured student was later taken by the older students. The teacher cleans everything from the chalkboard. A student writes on his notebook 2 + 2 = 5 and then he strikes the 5 and writes the 4 next to the stroken 5.

== Nominees ==
 2011 BAFTA Film Awards for Best Short Film. 

== Cast ==

* Bijan Daneshmand - Teacher
* Ravi Karimi - Student 1
* Pouyan Loti - Student 2

==References==
 

== External links ==
*  
*  

 
 