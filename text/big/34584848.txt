The Love Flower
{{Infobox film
| name           = The Love Flower
| image          = Carol Dempster in The Love Flower.jpg
| imagesize      =
| caption        = Carol Dempster in The Love Flower.
| director       = D. W. Griffith
| producer       = D. W. Griffith
| writer         = D. W. Griffith
| based on       =  
| starring       = Carol Dempster Richard Barthelmess
| cinematography = G. W. Bitzer Paul H. Allen
| editing        =
| budget         = $300,000   
| distributor    = United Artists
| released       =  
| runtime        = 7 reels
| country        = United States Silent (English intertitles)
}}
The Love Flower is a 1920 American silent drama film produced by D. W. Griffith and released through the then nascent United Artist company of which Griffith was a founding partner.   at silentera.com  

==Cast==
*Richard Barthelmess - Bruce Sanders
*Carol Dempster - Stella Bevan
*George MacQuarrie - Stellas father
*Anders Randolf - Matthew Crane
*Florence Short - Mrs. Bevan
*Crauford Kent - her visitor
*Adolph Lestina - Bevans old servant
*William James - Cranes assistant
*Jack Manning -

==Production==
Griffith filmed The Love Flower simultaneously with The Idol Dancer (1920) in Fort Lauderdale, Florida, and Nassau, Bahamas, in December 1919 to fulfill a contract with First National Pictures,  but after previewing the film on 2 April 1920 before the American Newspaper Publishers Association in New York, he purchased the rights to The Love Flower for $400,000.  Additional underwater footage of Dempster was shot in Florida along with scenes of her and MacQuarrie against a black background.  The reedited film was then released by United Artists.

==References==
 

==External links==
 
* 
*  at AllMovie
*  at Virtual History
* 
*  available for free download @  
*DVD availability at   and  

 

 
 
 
 
 
 
 
 


 