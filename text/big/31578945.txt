Nude Nuns with Big Guns
{{Infobox film
| name           = Nude Nuns with Big Guns
| image          = Nude nuns with big guns poster.png
| alt            =  
| caption        = Theatrical poster
| director       = Joseph Guzman
| producer       = Joseph Guzman Robert James Hayes II Maysam Mortazavi Jamie R. Thompson
| writer         = Joseph Guzman Robert James Hayes II
| starring       = Asun Ortega David Castro Perry DMarco
| music          = Dan Gross
| cinematography = Edwin M. Figueroa
| editing        = Joseph Guzman Robert James Hayes II
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
Nude Nuns with Big Guns is a 2010 nunsploitation thriller film.

The film was the subject of one of the largest copyright lawsuits in California.   The two lawsuits are the first time that two different companies claiming the intellectual-property rights of the same movie are each suing the same alleged 5,865 BitTorrent downloaders.   

==Plot==
A young Mexican nun, named Sister Sarah, is neglected and abused by a corrupt clergy that produces and distributes heroin. After a bad drug deal, she is handed over to thugs to be used as an instrument as sex for money. On the verge of death after being heavily drugged and wounded, the nun receives a commandment from God to take revenge. Acquiring heavy weapons (including big guns), Sister Sarah sets out to kill those who had abused her and are using the church for their own personal gain. The frightened drug lords in the church hires "Los Muertos", a violent motorcycle gang to track her down and eliminate her.  Los Muertos base of operations is the local brothel "Titty Flickers" where they to try and gather more information on the vigilante nun. After being wounded in a shootout, Sister Sarah hides out in a fleabag motel where she recovers and finally achieves vengeance by killing Los Muertos, degenitalizing Chavo (the brutal leader of Los Muertos), and saving her female lover who was raped. But in the final scene, the clergy drug lord, known only as the Monsignor, hires another hit man to track down the vigilante nun, leaving the door wide open for a sequel.

==Cast==
* Asun Ortega as Sister Sarah
* David Castro as Chavo
* Perry DMarco as Father Carlitos
* Maxie J. Santillan Jr. as Mr. Foo
* Ivet Corvea as Mistress Charlotte
* Aycil Yeltan as Sister Angelina
* Emma Messenger as Mother Magda
* Bill Oberst Jr. as Father John
* Maz Siam as Father Bernardo
* Xango Henry as Kickstand
* Robert Rexx as Half Breed
* Jessica Elder as Beverly
* Alfonso Castro as Father Frank
* Rene Arreola as Lobo
* Sarah Emmons as Butch

==BitTorrent lawsuit==
On March&nbsp;7, 2011, Camelot Entertainment Group, a film company based in Los Angeles, filed a federal lawsuit, Case No. CV 11-1949 DDP (FMOx), in the District Court for the Central District of California, against BitTorrent users who allegedly downloaded the movie between January and  .     The lawsuit which targets 5,865 IP addresses, seeks to compel ISPs to identify the defendants from their IP addresses.   The company has until May&nbsp;13 to "show cause why the Doe defendants should not be severed and/or dismissed from this action based on improper joinder of parties or lack of personal jurisdiction".    The Electronic Frontier Foundation will act as amicus counsel on the side of the defendants, who at this stage are known only by their internet IP addresses and rough geographic location. 

The lawsuit is seen as part of a courtroom based strategy in which defendants are asked to settle or risk being named in a public lawsuit.     If successful, the lawsuit could end up collecting more money than the movie earned at the box office. 

Incentive Capital of Utah also filed a nearly identical lawsuit against the same IP addresses with the same judge on May 6, 2011. 

On May 23, 2011, Camelot filed to dismiss their case, though the distribution group stated that they may refile the case in San Francisco.    The lawsuit filed by Incentive Capital was dropped on June 10, 2011. 

==Film rights==
Following the filing of the BitTorrent lawsuit, concerns have been raised as to whether Camelot Distribution Group actually owns the rights to the film.         Camelot defaulted on a loan financed by Incentive Capital used to purchase the movie rights.  Though Incentive Capital has already foreclosed on the film,  Camelot has stated that the foreclosure was an improper "usurpation of its assets". 

==References==
 

==External links==
*  
*  

 
 
 