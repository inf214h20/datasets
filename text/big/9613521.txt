Now You See Him, Now You Don't
{{Infobox film
| name           = Now You See Him, Now You Dont
| image          = Now_You_See_Him_Now_You_Dont.jpg
| caption        = Theatrical release poster Robert Butler
| producer       = Ron W. Miller
| writer         = Joseph L. McEveety Joe Flynn William Windom
| music          = Robert F. Brunner
| cinematography = Frank V. Phillips
| editing        = Cotton Warburton Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
}}
 Walt Disney Productions film starring Kurt Russell as a chemistry student who accidentally discovers the secret to Invisibility in fiction|invisibility. It is the sequel to the 1969 film The Computer Wore Tennis Shoes and was followed by 1975s The Strongest Man in the World .

Now You See Him, Now You Dont was the first Disney film to be shown on television in a two-hour time slot, in 1975.  Previous television showings of Disney films had either shown them edited or split into two one-hour time slots.

==Plot==
At Medfield College, science buff Dexter Riley and his friends, including Richard Schuyler and Debbie Dawson, eavesdrop via a hidden walkie-talkie on a board meeting led by Dean Eugene Higgins, discussing the small colleges continuing precarious finances. Later that afternoon, Professor Lufkin shows Higgins around the science lab where Dexter is working on an experiment with invisibility and another student, Druffle, explores the flight of bumblebees. That night, unknown to anyone, during a powerful thunderstorm, the roof of the lab is struck by lightning, sending a current of electricity down a metal beam and through Dexters complex experiment components. The next day, as Dexter examines his burnt equipment with dismay, Higgins meets with A. J. Arno, a recently released prisoner, who had also purchased Medfields mortgage. When Dexter accidentally drops one half of his glasses into a container of his experimental formula, it appears as if the substance destroys them, but upon closer examination, Dexter realizes the frames are merely partially invisible. After several excited tests, Dexter boldly places his fingers in the liquid and they disappear. Schuyler and Debbie arrive and are horrified to see Dexter with a partial hand, but Dexter insists Schuyler test the substance as well, admitting only afterward that he does not yet have an antidote. 

Just then, Higgins brings Arno to visit the lab, stunning the students, as only two years earlier, Dexter was instrumental in exposing Arnos crooked gambling scheme. Although distracted by the condition of their partially visible hands, Dexter and the others notice that Arno is more concerned with the campus architecture than Higgins curriculum speech. After Arno and Higgins depart, Dexter and Schuyler try washing their hands to remove the formula. Curious about Arnos behavior, Dexter convinces Schuyler to use the invisibility formula to sneak into Arnos office that night to look around. Although they are nearly discovered when Schuyler steps into a puddle, making his tennis shoes visible, the boys get inside Arnos office where they find a model of Medfield College redesigned as sprawling gambling establishment. After taking photos of the model, the boys flee with Debbies help. 

The next day Dexter shows the photos to Lufkin and Higgins, both of whom are alarmed and concerned about how to buy back the colleges mortgage. Convinced that Druffles bumblebee study would draw attention and investments to Medfield, Higgins reacts angrily when Dexter assures him that his invisibility formula could win the top prize money in the upcoming Forsythe science contest. Not having admitted to anyone that Medfield has been dropped from the contest for being too insignificant, Higgins determinedly contacts the contests sponsor, millionaire Timothy Forsythe, and agrees to meet over a game of golf, despite his inability to play. Upon learning of Higgins plan and suspecting it must be connected with raising money for the college, Dexter urges Schuyler to volunteer to serve as Higgins caddy while, hidden by the invisibility formula, he will take control of Higgins golf ball. At the golf club, Forsythe and the state university dean, Collingsgood, are amazed by Higgins quirky golfing abilities, which include numerous hole-in-one-shots, as is Arno who is also at the club. 

After the game, Forsythe enthusiastically agrees to reinstate Medfield into the competition for the Forsythe Award. Meanwhile, Arno accidentally sees Dexter becoming visible in the club showers and grows suspicious. When the local television news covers Higgins extraordinary golf game, he is invited to join an exclusive tournament in nearby Ocean City. Convinced that he will win enough money to pay the colleges mortgage, Higgins brashly accepts and that afternoon departs with Schuyler. Learning of the tournament from Druffle too late, Dexter misses the plane and is forced to watch the competition on television where Higgins game against two professionals is a disaster. In his business office, Arno and his henchmen, Cookie and Alfred, also watch the tournament and ponder Higgins odd inconsistency. Upon returning to the college, Higgins tells Lufkin that Druffles bee experiment is the schools last chance. Both men are stunned when just then Druffle appears swathed in bandages after being attacked by the bees, to which he is allergic. Hoping to assuage the crestfallen Higgins, Lufkin suggests that they give Dexters unproven formula a chance and the dean reluctantly agrees. 

That evening, Cookie, disguised as a janitor, sneaks into the campus lab where he witnesses Dexter and Schuyler using the invisibility spray, and reports to Arno, who orders him to return and steal it. The following day, Forsythe and members of his committee arrive on campus to judge the best science experiment at the college. Unaware that their spray bottle has been replaced by Cookie, Dexter and Schuyler make their presentation and are stunned when it has no effect. Disappointed and angry, Forsythe and Higgins depart as Dexter remains confused until he chats with Charlie, the janitor. Learning that there is no night janitor, Dexter realizes that Cookie was a spy and likely stole the formula. Concluding that Arno must be behind the theft, Dexter plants a walkie-talkie in his office. 

A couple of days later, Schuyler overhears Arno plotting with Cookie to rob the Medfield Bank by making themselves and the money invisible. Certain that if he could retrieve the formula before the Forsythe Award announcement that night he could still win the contest, Dexter sends Schuyler to the police and goes to inform the banks president, Wilfred Sampson. When both the police and Sampson dismiss the boys story about invisibility, Dexter and his friends gather outside of the bank to make plans. While an invisible Arno and Cookie knock out the guards and take the money, Dexter unsuccessfully tries to use a fire hydrant to hose the men down as they exit the bank. When Sampson realizes a theft has occurred, he contacts the police who join the college students in a wild chase of the car driven by the invisible robbers. 

After briefly eluding everyone, Arno orders Cookie to make the car invisible, but they are spotted on a dirt road in a park. Deducing Arno will not leave town but go to his home instead, Dexter drives there and forces Arnos car into a neighboring pool where it, the money, and the men become visible. Taking the formula, Dexter and the others dash to the presentation of the Forsythe Award and plead for one more opportunity to demonstrate their invention. Frustrated by Dexters determination, Higgins intervenes just as Dexter sprays Schuyler, and, again, there is no result. Realizing the dip in the pool has diluted the formula, Dexter tries to explain to Forsythe. And just when Higgins tells everyone for the last time that invisibility does not exist, the top half of him becomes invisible, thus shocking the group and winning the top prize to save Medfield for another year.

==Production notes==
=== Locations ===
The Medfield College exteriors were on the Disney lot: the main Medfield College building and courtyard used in the title sequence was the old Animation Building at the corner of Mickey Avenue and Dopey Drive.

=== Props ===
The Green VW used by Schuyler was two Herbie cars from The Love Bug: one was the vehicle carried by Tang Wus Chinese Camp students, (this was a gutted car and a rubber truck tire tube was placed under the passenger door, and when inflated suddenly, it would tip the car over, this car used in the scene where AJ Arno rams it). The other car was used in the scenes with Schuyler driving it on a flat tire. (The Art Dept. painted the car green, and dusted it to give a look of neglect. When the sunroof is open, the original Herbie Pearl white paint job under the tarp sunroof can be seen where the green was not painted.)

==Comics adaptations==
A text piece with illustrations adapting the film appeared in Walt Disney Comics Digest #37 (Oct. 1972) with a production still on the cover.  The Walt Disneys Treasury of Classic Tales comic strip ran an adaptation written by Frank Reilly and drawn by John Uslher that appeared between April 2, 1972 and June 25, 1972. 

==Cast==
* Kurt Russell as Dexter Reilly
* Cesar Romero as A.J. Arno Joe Flynn as Dean Eugene (E.J.) Higgins
* Jim Backus as Timothy Forsythe William Windom as Professor Lufkin
* Michael McGreevey as Richard Schuyler
* Ed Begley, Jr. as Druffle
* Richard Bakalyan as Cookie
* Joyce Menges as Debbie Dawson
* Alan Hewitt as Dean Edgar Collingswood
* Kelly Thordsen as Police Sergeant Cassidy
* Bing Russell as Alfred
* George OHanlon as Ted, Bank Guard
* John Myhers as Golfer
* Pat Delaney as Winifred Keesely, Higgins Secretary
* Robert Rothwell as Driver
* Frank Welker as Henry

==Reception==
The movie received a mixed reception. A negative review came from The New York Times, which accorded, "Now with all due respect to childrens intuition and judgment, may we suggest that they now try the Real McCoy, if they havent already. How about the original "The Invisible Man" on television? Theres grand, serious fun, kids. Plus—square or not—something to think about."  A positive review came from Variety (magazine)|Varietys staff, which stated that "Virtually all the key creative elements which early in 1970 made The Computer Wore Tennis Shoes encore superbly in Now You See Him, Now You Dont."  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 