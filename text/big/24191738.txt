Corral (film)
{{Infobox film
| name           = Corral
| image          = 
| caption        =  Colin Low 
| producer       = 
| writer         = Colin Low Wally Jensen
| starring       =   
| music          = 
| cinematography = Wolf Koenig  Tom Daly 
| distributor    = 
| released       = 1954
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Colin Low, partly shot in the Cochrane Ranch in what is now Cochrane, Alberta.

In the film, a cowboy rounds up wild horses, lassoing one of the high-spirited animals in the corral, then going on a ride across the Rocky Mountain Foothills of Alberta.

==Production==
Low got the idea for the film after attending a cattle auction with his father, who had worked as a foreman at the Cochrane Ranch. The following summer, Low asked NFB colleague Wolf Koenig, also an ex-farm boy, if he would like to come to Alberta to make a film about a cowboy. Lows father provided his top hand, Wally Jensen, and the horses. 

Corral was shot in 1953. Koenig was in the NFB Animation Department and this was his first live-action shooting using a new Arriflex and a tripod with a gyro stabilizer. Low had written a script but Wally Jensen re-wrote it as they filmed. 
 Tom Daly edited the film. Eldon Rathburn wrote the score for two jazz guitarists, based on well-known cowboy songs. 

The film represents a break in tradition for the NFB, which had until that time relied heavily on narration in its documentaries. Its gentle guitar score and use of handheld camera also breaks with Hollywoods traditionally epic portal of the cowboy.   

==Release==
The film, part of the Canada Carries On series, would play theatrically across Canada in April 1954 and in some American cities, including Washington, D.C. It received the Best Documentary award at the Venice Film Festival.   

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 


 