How Could You, Jean?
 
{{Infobox film
| name = How Could You, Jean?
| image = How Could You Jean Poster.jpg
| caption = Theatrical release poster
| director = William Desmond Taylor
| writer = Eleanor Hoyt Brainerd (novel) Frances Marion
| starring = Mary Pickford
| producer = Mary Pickford
| studio = Famous Players-Lasky/Artcraft
| cinematography = Charles Rosher
| distributor = Paramount Pictures
| budget =
| released =   
| country = United States
| language = Silent film English intertitles
| runtime =
}} silent comedy-drama film, starring Mary Pickford, directed by William Desmond Taylor, and based on a novel by Eleanor Hoyt Brainerd. Casson Ferguson was the male lead; Spottiswoode Aitken and a young ZaSu Pitts had supporting roles.

This is a lost film, with no surviving prints. 

==Plot==
The plot involves a young socialite pretending to be a cook, who falls in love with a man she thinks is a hired hand, but he is actually a millionaire. The film was not well received by critics, who generally found it pleasant but dull,  although the New York Times called it "a funny, extremely well-produced comedy".   

==Similar plot==
A novel by Norwegian writer Sigrid Boo, Vi som går kjøkkenveien (We Who Enter Through the Kitchen) has an almost identical plot to Brainerds original book. Boos novel was adapted for the U.S. film Servants Entrance (1934) starring Janet Gaynor, which had an identical plot to the 1918 film. As the New York Times commented, "apparently, the old Pickford comedy was already forgotten, and no copyright infringement suit was filed." 

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 


 