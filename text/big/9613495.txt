Trikal
{{Infobox film
| name = Trikal
| image = Trikal (Past, Present, Future), 1985.jpg
| image_size = 150px
| caption = Trikal (Past, Present, Future)
| director = Shyam Benegal
| producer = Freni M Variava
| writer = Shyam Benegal (Story & Screenplay) Shama Zaidi  (Dialogue)
| starring = Leela Naidu Anita Kanwar Neena Gupta Soni Razdan Dalip Tahil Naseeruddin Shah
| music = Vanraj Bhatia
| cinematography = Ashok Mehta
| editing = Bhanudas Divakar
| distributor =
| released = 30 August 1985
| runtime = 137 min Portuguese
| budget =
| preceded_by =
| followed_by =
}}

Trikal (1985) (Past, Present and Future) is an Indian movie written and directed by Shyam Benegal, set in Goa during the early 1960s (pre liberation) Period. The film starred yester years actress, Leela Naidu, in a comeback role after many decades. 

The film was selected for the Indian Panorama at Filmotsav 1986, and for the Indian Film Retrospective, Lisbon 1986. It was later invited to the London Film Festival 1986  

==Overview==

Set in 1961 Goa, when colonial rule of Portuguese was on its last gasp,  the movie revolves around the life and tribulations of a fictional Goan Christian family called "Souza Soares".

Liberally sprinkled with dashes of humor, this is a fast-paced comedy about a family and their friends who lived through the transition of Goa from a Portuguese colony to a district governed by India. Ana (Sushma Prakash) is about to become engaged (unwillingly) to the unappealing Erasmo when her beloved grandfather suddenly dies. Her grandmother is so overwhelmed by grief that Anas engagement is put on hold indefinitely (along with everything else) while granny tries to contact the spirit of her dead husband. But her wires get crossed each time and instead of grandpa, she brings forth ghosts of people the family has wronged in the past. Meanwhile, Anas real love is hiding out in the cellar. As Anas marriage to Erasmo draws near, she faints dead away because (as he soon finds out) she is pregnant, but not by him. These and other events are revealed in flashbacks as a former resident in the house returns to visit 25 years later.

The film was shot in the ancestral home of Mario Miranda, at Loutolim, Goa.

==Awards==
* 1986:  
* 1986: National Film Award for Best Costume Design: Saba Zaidi

==Cast==
* Leela Naidu as Dona Maria Souza-Soares
* Anita Kanwar as Sylvia
* Neena Gupta as Milagrenia
* Soni Razdan as Aurora
* Dalip Tahil as Leon Gonsalves
* K. K. Raina as Senor Lucio
*   as Kapitan Ribeiro/ Governor
* Keith Stevenson as Dr. Simon Pereira
* Lucky Ali as  Erasmo
* Salim Ghouse
* Ila Arun as Cook
* Jayant Kriplani as Francis
* Akash Khurana as Renato
* Sabira Merchant as Dona Amelia
* Sushma Prakash as Anna
* Remo Fernandes as Singer
* Alisha Chinai as Singer
* Naseeruddin Shah as Ruiz Pereira
* Kulbhushan Kharbanda as Vijay Singh Rane/Khushtoba Rane

==References==
 

==External links==
*  
http://daleluismenezes.blogspot.in/2011/04/goa-through-eyes-of-shyam-benegal.html

 

 
 
 
 
 
 
 
 