Arzoo (1965 film)
{{Infobox film
| name           = Arzoo
| image          = Arzoo 1965 film poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Ramanand Sagar
| producer       = Ramanand Sagar
| writer         = 
| narrator       =  Sadhana Rajendra Kumar Feroz Khan Nazir Hussain
| music          = Shankar Jaikishan
| cinematography = 
| editing        = 
| distributor    = Sagar Art Cooperation
| released       = 1965
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Arzoo ( : آرزو,   film directed by Ramanand Sagar. It stars Sadhana_Shivdasani|Sadhana, Rajendra Kumar, and Feroz Khan in the lead roles. It was a success at the box office.   Arzoo is the story of a love that triumphed over life and fate. The theme of hating the disabled was also tried in Rajashri films Ek Baar Kaho starring Navin Nischol and Shabana Azmi, but it did not work.

== Synopsis ==

Gopal (Rajendra Kumar) is a skiing champion. He meets Usha (Sadhana_Shivdasani|Sadhana) on his holidays at Jammu and Kashmir with the fake name Sarju. Then they both fall in love. One day, Usha tells Gopal that she does not like the  disabled. According to her instead of living a life of disabled, it is better to die.
After spending his holidays in Kashmir and promising Usha that he will marry her, he heads back to Delhi, where his parents and a sister, Sarla, live. Along the way, he loses his a leg in a car accident. 
Gopal becomes worried. Since he remembers the words of Usha, he tries to avoid her to go away from her life.
He thinks Usha will not accept him as he is now disabled. Then he goes back to Delhi and he does not tell anything about Usha. In the meantime, Usha tries a lot to find him, but after having no sign of him, she begins to think that Gopal is in some trouble and hence unable to contact her. 

Gopals best friend, Ramesh (Feroz Khan), unknowing about his friends love story, wants to marry Usha. After saying "no" several times, at Ushas father accepts on her behalf and Usha dutifully agrees to the marriage as well. But on the day of her wedding, one miracle happens in the form of Kashmir Houseboat owner Mangloo (Mehmood) and initially Ramesh and then Usha find out that Gopal and Sarju are not two persons, but one. Then this situation forms the climax of the movie.

==Soundtrack==
The music is composed by Shankar Jaikishan
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Jab Ishq Kahin Ho Jata Hai"
| Mubarak Begum, Asha Bhosle
|-
| 2
| "Aji Rooth Kar Ab Kahan Jaiyega"
| Lata Mangeshkar
|-
| 3
| "Ae Phoolon Ki Rani"
| Mohammed Rafi
|-
| 4
| "Bedardi Balma Tujhko"
| Lata Mangeshkar
|-
| 5
| "Ae Nargise Mastana"
| Mohammed Rafi
|-
| 6
| "Aji Humse Bachkar Kahan Jaiye Ga"
| Mohammed Rafi
|-
| 7
| "Chhalke Teri Ankhon Se"
| Mohammed Rafi
|}

== Filmfare Nominations ==

*Rajendra Kumar for Best Actor
*Ramanand Sagar for Best Director
*Ramanand Sagar for Best Story
*Hasrat Jaipuri for Best Lyricist for the song "Aji Rooth Kar Ab Kahan Jaiyega". 

== References ==
 

== External links ==
*  

 
 
 
 
 