Yaaron Ka Yaar
{{Infobox film
| name           = Yaaron Ka Yaar
| image          = Yaaron Ka Yaar.jpg
| image_size     = 240px
| caption        = 
| director       = A. Bhimsingh
| producer       = 
| writer         =
| narrator       = 
| starring       =Shatrughan Sinha Leena Chandavarkar
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood film directed by A. Bhimsingh. 
==Plot==
Nathu(Prem Nath) is untouchable. He takes water from common pond which is objected by some Pandits. Jaimal Singh(Ramayan Tiwari) orders his goons to teach Nathu a lesson. Goons beat him and burn the complete society. Nathus wife was pregnant. By the time Nathu reaches hime, Nathus wife had already left. Nathu takes Jaimals son and runs away. In the end, movie takes a dramatic turn and all unite happily.

==Cast==
*Shatrughan Sinha ...  Pratap / Shera 
*Leena Chandavarkar ...  Bindiya 
*Prem Nath ...  Nathu 
*Ramayan Tiwari ...  Jaimal Singh (as Tiwari)  Helen ...  Rita 
*Indrani Mukherjee ...  Shakuntala (Jaimals daughter) 
*Achala Sachdev ...  Dhanno (Nathus wife) 
*Aarti ...  Malti 
*Nana Palsikar ...  Maltis dad 
*Ramesh Deo ...  Shekar  Asit Sen   
*Yunus Parvez ...  Money-lender 
*Maruti ...  Money-lenders son 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Dekho Mehfil Men Main To Aai"
| Asha Bhosle
|-
| 2
| "Main Yaron Ka Hoon Yaar"
| Kishore Kumar
|-
| 3
| "Mere Lal Mujhpe Kar Tu Mehrbani"
| Asha Bhosle
|-
| 4
| "Meri Jaan Mujhpe Kar Tu Mehrbani"
| Manna Dey
|-
| 5
| "Pehli Pehli Bar Mujhko Ye Kya"
| Mohammed Rafi, Asha Bhosle
|}
==External links==
*  

 

 
 
 
 
 