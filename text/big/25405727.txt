The Wager (2007 film)
{{Infobox film
| name           = The Wager
| image          = The Wager.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Judson Pearce Morgan
| producer       = 
| writer         = Judson Pearce Morgan	(screenplay) Bill Myers (book)
| narrator       =  Kelly Overton Doug Jones
| music          = 
| cinematography = Todd Barron
| editing        = Judson Pearce Morgan Marcos Soriano
| studio         = Blazing Sun Productions
| distributor    = Pure Flix Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 2007 Christian Christian drama film, directed by Judson Pearce Morgan. It stars Randy Travis, Jude Ciccolella and Candace Cameron Bure. It was based on the novel The Wager by Bill Myers. Billy DaMota did casting for the film. 

== Synopsis == Best Actor at the Academy Awards. In the wake of that announcement, Michael is cast in what may be the role of a lifetime. 

Hes set to play the lead in an incredible story of monumental conflict, but will all of the swirling gossip around Michael cause him to stumble on the path of righteousness? His marriage is in jeopardy and his career is on the line. This man who has always tried to do the right thing must find the courage and conviction needed to set his life straight.

== Cast ==
* Randy Travis as Michael Steele
* Jude Ciccolella as Kenny
* Nancy Stafford as Annie Steele
* Nancy Valen as Tanya Steele Kelly Overton as Tessa
* Bronson Pinchot as Colin Buchanan
* Candace Cameron Bure as Cassandra Doug Jones as Peter Barrett

== Release ==
The Wager was released theatrically on June 27, 2007. It was released to churches by Outreach Cinema,  a Christian group that organizes showings of faith-based film at churches nationwide. Christian distribution company Pure Flix Entertainment released the film to DVD on May 13.  The Wager was one of 14 Christian films featured at the 2008 Gideon Media Arts Conference and Film Festival.  It was also featured at the REAL LIFE Film Festival. 

=== Reception === Movieguide Magazine said, "The Wager is produced well. The camerawork, the editing, and the direction capture your attention... The script loses its focus two or three times and could have been fixed ahead of time... Overall, however, the movie works very well and the filmmakers should be commended." 

== References ==
 

== External links ==
*  
*  
*   at Allmovie
*   at Rotten Tomatoes

 
 
 
 