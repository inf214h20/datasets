Joyful Noise (film)
{{Infobox film
| name           = Joyful Noise
| image          = Joyfulnoise-1012-final.jpg
| director       = Todd Graff Jeremy Jordan
| producer       = Broderick Johnson Andrew Kosove Michael Nathanson Joseph Farrell Catherine Paura
| writer         = Todd Graff
| music          = Mervyn Warren David Boyd
| editing        = Kathryn Himoff
| studio         = Alcon Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $31,157,914 (Worldwide)
}}
 musical comedy-drama Jeremy Jordan. It was written and directed by Todd Graff, with gospel-infused music by Mervyn Warren. The film was released in U.S. theaters on January 13, 2012. In the film, two strong-minded women are forced to cooperate when budget cuts threaten to shut down a small-town choir.  It received mixed to negative reviews from critics with praise for its songs and acting but criticism towards its script and tone. 

==Plot==
After the untimely death of a small-town church choir director (Kris Kristofferson) in Pacashau, Georgia (U.S. state)|Georgia, Vi Rose Hill (Queen Latifah|Latifah), a no-nonsense mother raising two teens alone, takes control of the choir using the traditional Gospel style that their Pastor Dale (Courtney B. Vance) approves of. However, the directors widow, G. G. Sparrow (Dolly Parton|Parton), the main benefactor to the church, believes she should have been given the position. As in previous years, the choir reaches the regional finals of the national amateur "Joyful Noise" competition, only to be disappointed when a rival choir beats them. Tough times in the town have led to budget problems that threaten to close down the choir, at the same time as the town needs the choirs inspiring music more than ever.

Vi Rose has a son, Walter (Dexter Darden), who has Asperger syndrome, and a talented, pretty and ready-to-date daughter, Olivia (Keke Palmer). But Olivia is not ready to date under her mothers household rules. G. G. has recently begun caring for her rebellious, drifter grandson, Randy (Jeremy Jordan). A romance blossoms between Olivia and Randy, which is strongly opposed by Vi Rose. Olivia also has a rival suitor, Manny (Paul Woolfolk). At Randys urging, G. G., Olivia and most of the choir come to believe that some more contemporary arrangements (prepared by Randy) would be more successful for the choir. It also turns out that the choir has a chance at the national finals of the competition when the rival choir is found to have cheated by hiring professionals. But the pastor says that the church will not sponsor the choir unless they continue to use their reverent, traditional style.

Vi Roses husband, Marcus (Jesse L. Martin), enlisted in the army after having trouble finding work at home, but his prolonged absence has saddened his family and causes additional tension between Vi Rose and Olivia. Meanwhile, a vivacious member of the choir who choreographs their routines, Earla (Angela Grovey), after a long dry stretch, finds passion first with Mr. Hsu (Francis Jue), whose weak heart gives way by morning, and later with Justin (Roy Huang). The towns tough times forces another choir member, Caleb (Andy Karl), and his family out of business. Vi Rose and G. G. come to blows in a confrontation, Olivias frustration with her mother boils over, and G. G. threatens Pastor Dale with disendowing the church if the choir is not allowed to compete in the finals with the new arrangements.

During the movie Randy befriends Walter and begins to teach him how to play the piano. One day, while at the quarry Randy and Walter are hanging out as Manny arrives. Randy and Manny begin to fight over Olivia ending with Walter giving Manny a bloody nose. When returning home Walter brags to Vi Rose about the fight. Angered by the news, she throws out Randy and tells him to leave her family alone. Randy, however, is able to befriend Manny and convince him to help out the choir with his guitar skills.
 Los Angeles for the finals, feeling very unsettled. Vi Rose and Olivia have a fight and Vi Rose slaps Olivia. Tough competition presents itself in the form of a choir made up of cute pre-teens, with a charismatic young soloist (Ivan Kelley Jr.). But Vi Rose, G. G., Olivia and Randy pull the choir together and they give a rousing performance, using the new arrangements and choreography, capturing first place. The choir returns to town in triumph. One year later Earla and Justin get married and then Marcus comes back to his family.

==Cast==
* Queen Latifah as Vi Rose Hill
* Dolly Parton as G. G. Sparrow
* Keke Palmer as Olivia Hill, Vis daughter and Randys love interest. Jeremy Jordan as Randy Garrity, Sparrows grandson and Olivias love interest.
* Dexter Darden as Walter Hill, Vis son and Olivias brother.
* Courtney B. Vance as Pastor Dale
* Kris Kristofferson as Bernard Sparrow, G. G.s late husband.
* Angela Grovey as Earla Hughes, the choirs choreographer.
* Paul Woolfolk as Manny, a young guitarist with an interest in Olivia. 
* Francis Jue as Ang Hsu, a choir member who is attracted to Earla.
* Jesse L. Martin as Marcus Hill, Vi Roses estranged husband.
* Andy Karl as Caleb, a choir member whose family loses its business. Dequina Moore as Devonne, a talkative choir member.
* Roy Huang as Justin, Earlas second lover.
* Judd Lormand as Officer Darrel Lino
* Shameik Moore as Our Lady of Perpetual Tears choir master
* Ivan Kelley Jr. as the soloist of the Our Lady of Perpetual Tears soloist
* Kirk Franklin as Baylor Sykes, the choir leader for a rival church.
* Karen Peck as competition host, lead singer "Mighty High"
* Chloe Bailey as a choir member of Our Lady of Perpetual Tears

==Production== Peachtree City, and historic Howards Restaurant in Smyrna, Georgia|Smyrna.  The movie finished filming in early April 2011. 

==Soundtrack==
The movie has twelve songs inspired by the film under the name Joyful Noise Original Motion Picture Soundtrack including "Fix Me, Jesus", "In Love", "Higher Medley" and more. The soundtrack is performed by the stars of the movie. Dolly Parton wrote three of the songs, including "Not Enough Love", "From Here To The Moon and Back", and "Hes Everything."

==Reception==

=== Critical reaction ===
Joyful Noise received mixed to negative reviews from critics.  , the film held an approval rating of 33% on Rotten Tomatoes with the consensus saying "Joyful Noise  s musical numbers are solidly entertaining, and it benefits from Queen Latifah and Dolly Partons sizable chemistry; unfortunately, they arent enough to make up for the rest of the film."  Roger Ebert of the Chicago Sun-Times described the film as "an ungainly assembly of parts that dont fit, and the strange thing is that it makes no particular effort to please its target audience, which would seem to be lovers of gospel choirs."  Metacritic, however said the film received mixed or average reviews and gave the film a 44% out of 100.  Christy Lemire of The Boston Globe felt that "if some incarnation of Glee (TV series)|Glee were to be developed for the Christian Broadcasting Network, it would probably look a lot like Joyful Noise.  Peter Debruge of Variety (magazine)|Variety claims that "despite the sheer volume of music on offer, very little of it feels authentic – or especially inspiring."  Todd McCarthy of The Hollywood Reporter felt that everyone in the film "is so fundamentally decent and goody-goody that no real tension or unresolvable conflicts ever surface." 

Some critics found positive things to say about the film. Richard Corliss of Time (magazine)|Time magazine stated that "the critic in me can authoritatively declare that the film is crap. The fan in me sent his shirt to the dry cleaners for tear removal."  Owen Gleiberman of Entertainment Weekly felt that "the movies musical numbers are catchy and rollicking and, in their bright sunshiny way, rather soulful."  Rex Reed of The New York Observer stated, "Dont expect high art, and you will leave Joyful Noise smiling to the beat." 

===Box office===
The movie opened at #4 at the weekend box office, grossing $11.2 million on its opening weekend. It was screened in  2,735 theaters and grossed $30,932,113 during its run. 

==See also==
*The Fighting Temptations
*Sister Act Let It Shine

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 