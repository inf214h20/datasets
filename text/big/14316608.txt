Fireman Save My Child (1918 film)
 
{{Infobox film
| name           = Fireman Save My Child
| image          = 
| caption        = 
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         = H.M. Walker
| starring       = Harold Lloyd
| cinematography = Walter Lundin
| editing        =  Pathe Exchange
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}} short comedy film starring Harold Lloyd.   

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels 
* William Blaisdell
* Sammy Brooks
* Harry Burns
* Lige Conley (as Lige Cromley)
* Billy Fay (as B. Fay)
* Gus Leonard
* Alma Maxim
* James Parrott
* Dorothea Wolbert

==See also==
* List of American films of 1918
* Harold Lloyd filmography

==References==
 

==External links==
* 
*   on YouTube

 
 
 
 
 
 
 
 
 
 
 