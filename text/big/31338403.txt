The Real Badlands

{{Infobox film
| image = Badlandsdvdcover.jpg
| name = The Real Badlands
| director = Tim Clark Craig Ford
| producer = Tim Clark Craig Ford Emily Renshaw Smith
| writer = Tim Clark
| starring = Simon Burgess Susan Moran
| music = Tim Clark
| cinematography = Tim Clark Craig Ford
| editing = Tim Clark Craig Ford
| distributor = Rockwood Pictures Current TV
| released =  
| runtime = 7 minutes
| country = United Kingdom
| language = English
| budget = £100
| gross = £1000
}}
The Real Badlands is a 2009 documentary film produced by Current TV and Rockwood Pictures, by British filmmakers Tim Clark and Craig Ford. The film tells the story of a Newark on Trent couple (Simon Burgess and Susan Moran), who fed up with modern day life, quit their jobs, sell all of their possessions, and move into the woods.

== Controversy ==
The film was particularly controversial upon its release due to inaccurate claims that the film was staged; it was claimed that the film was in fact a mockumentary. The filmmakers publicly denied these claims in a written statement read out live on Current TV, minutes after the film was broadcast.  . 

== Locations ==
The film was shot in Stapleford Woods and Kelham, near Newark on Trent, during the summer and winter of 2008.  . 

== Exhibition ==
The Real Badlands was broadcast by Current TV on the 30th of January 2009.  .  It has been repeated numerous times since its initial broadcast in the UK, US and Europe. It was briefly made available for viewing in the UK on Virgin Medias on-demand services. 

The film was released on DVD in late 2009.  . 

== Reception ==
Upon its release, The Real Badlands received almost universal critical acclaim. Current TV presenter Stew Game went on to describe the film as "brilliant" and compared it to The Office.  The film has gone on to develop a loyal cult following online, with fans affectionately describing the films central character, Simon Burgess, as "Burge Grylls", in reference to the TV personality and survival expert Bear Grylls.

== Remake ==
In 2010 principal photography began on a feature film version, under the working title of Plan B. It was later announced that the film, a joint collaboration between Rockwood Pictures, Death Ground Media and Devils Lake Films, would be released in mid 2015, with a new title; Once Upon a Time in the Wild.

== References ==
 

==External links==
*  
*  

 
 
 
 