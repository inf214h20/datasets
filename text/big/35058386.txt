Damascus with Love
{{multiple issues|
 
 
}}

{{Infobox film
| name           =Damascus with Love دمشق مع حبي
| writer         =Mohamad Abulaziz
| starring       =Marah Jabr   Fares Helou   Khalid Taja   Milad Youssef   Jihad Saad   Pierre Dagher
| director       =Mohamad Abdulaziz
| released       = 2010 Dubai International Film Festival 
| runtime        =95 minutes
| country        =Syria
| music          =Rim Banna
| cinematography =Wael Azaldin Arabic
| producer       =Nabil Toume
| distributor    =Al-Sharq Establishment for Production and Distribution
|
}}
 Syrian Christians and Syrian Jews. Damascus with Love is a story about the life of a young Jewish girl whose father reveals some past secrets to, and so decides to look for the truth in magical Damascus. This movie was translated and aired in the UK on Wednesday October 19, 2010.  

==Cast and characters==
*Marah Jaber
*Fares Helou
*Khalid Taja
*Milad Yousef
*Peirre Dagher
*Antoinette Najeeb
*Layla Jaber
*Maisoun Asaad

==Synopsis==
As in his first film, Half a Milligram of Nicotine ( ) there lies in the film Damascus with Love of young director Mohammad Abdul Aziz, a past that today records its reality and occurring. A story about a simple touch enough to revive an old genuine love. And another about the release of a deep secret that the past Damascene generations have tried over the decades to hide from their children, which restores the clarity and authenticity of the relationship between the city and its citizens regardless of their identity. A Jewish Syrian girl sets for departure from home city Damascus, but reverses her decision because her father discloses a secret that was kept for years. The secret being about a Lover who was thought to be dead. The girl reveals the mysteries of the past in search of another side of magical Damascus, hence falling in love with a Christian man who disappears in Lebanon’s civil war. She finds herself facing a difficult choice, but the city itself will keep the door of love and choices wide open to infinite possibilities. A story about the Levant city which regains its deep connection with its citizens leaving out any discriminations or exclusions. 

==Plot== Lebanese civil war, Hala Mizrahi (Marah Jaber) is actually searching for an identity as well as a home land. Her family has immigrated to Rome in escape from the Zionist action in Palestine, and left behind the only city she has ever known, Damascus. Her father after reaching Rome, is in great misery and isolation because he feels he has been forced out and pulled out from his roots and his city. Ironically, while still on the way from Rome’s airport to see his family, the Jewish Father (Khalid Taja) undergoes a stroke and dies just as he smelled a jar of soil he had packed from his homeland, and so his family decides to return his body for burial at the place he loved the most, Damascus. The movie ends with Hala ( Marah Jaber) deciding on leaving to Italy again after a long vain search for Nabil (Milad Yousef), when she receives a letter from him to meet her at the same place that has witnessed all their wonderful past memories. That place being, in the beautiful old city of Damascus.  

==Controversy== Kurdish and many other Arab minorities that cannot be separated from the Arab culture because they are an integral part of the region’s culture and geography. He continues to explain how since the outbreak of the Great Arab Revolt in 1916, those minorities were not able to achieve equivalence with other major sects and so have suffered for many years of ruling regimes. From talking about past Arab outbreaks, Abdul Aziz moves on to connecting that idea to today’s Arab Spring. He believes that that with the arrival of the Arab Spring and the success of the protests in some countries, those revolutions are a challenge to the culture of the Arab region, especially since one of the main objectives of those Arab revolts is to look for minority rights of equality and justice. 

==Festivals==

*Tetouan Mediterranean Film Festival, Rabat 2010  
*Dubai International Film Festival, 2010  
*Alexandria Film Festival, 2011 

==See also==
*Dubai International Film Festival

==External links==
* 
* 

==References==
 

 
 