Aduri
 Aduri, Iran}}
 

{{Infobox film
| name = Aduri
| image =
| caption = Theatrical release poster
| director = Riyadh Mahmood
| producer = Riyadh Mahmood Fahd Habeeb George Mao
| writer = Riyadh Mahmood Fahid Hasan
| starring = Ariana Almajan Kawan Karadaghi  Roger Payano Christy Sullivan
| cinematography = Jason Baustin
| music = Richard Ames
| editing = Riyadh Mahmood
| studio = Feni Media Fahd Habeeb Productions
| distributor = 
| released =  
| runtime = 101 minutes
| country = United States
| language = English
}}
Aduri is a 2008 American thriller film directed by Riyadh Mahmood. Written by Mahmood and Fahid Hasan, the film stars Ariana Almajan and Kawan Karadaghi.    

Produced by Washington DC filmmakers,  and released May 16, 2008 in film|2008,  Aduri presents the lives of two fugitives running from the FBI and correctly predicted the outcome of the United States presidential election, 2008 in November.    One of the news sequences in the film refers to President Barack Obama. 

==Production==
The production of the film from screenwriting to premiere took about 3 years to complete.    

==Plot==
Aduri Aman (Ariana Almajan), finds herself the target of federal agents, though she is herself employed by the FBI as an analyst. Unsuspecting that she may have knowledge of government secrets, she receives help and refuge from a stranger, Saif (Kawan Karadaghi). He seems overly eager to help. As the couple races through Washington D.C. suburbs, Aduris own past catches up to her.

Various contemporary events are used as plot devices in Aduri.  These include:

*United States War on Terror
*The 2004 Asian Tsunami
*The crude oil price hike from 2005 to 2008
*The 2008 United States Presidential Elections

==Cast==
* Ariana Almajan as Aduri Aman
* Kawan Karadaghi as Saif Uddin
* Roger Payano as Agent Mason
* Christy Sullivan as Bebe Theodore Taylor as Mr. Wright
* Karl Bittner as Adrian Shepherd
* Patricia Berry as Aunt Julie 
* Gita M. McCarthy as Mrs. Aman 
* Wilson White as Mr. Aman 
* Karthik Srinivasan as Rakib Shah 
* Lauren Meley as News Anchor 
* Yaser Ahmed as News Anchor
* Gytis Kanchas as Angry Customer

==Reception==
When reviewed by DesiChutney, it was said, "In a sea of independent films, Aduri shines through with much promise of thrills, drama and a touch of comedy." 

==DVD release==
Aduri was first released on DVD on February 5, 2009. 

==Soundtrack==
 
The soundtrack to the film was released in 2008.   Richard Ames composed approximately 50 minutes of original music for the soundtrack including the theme music.  It consisted of various fusions of music from the east and west.  

The song titled Conspiracy from the album Open Your Eyes,  by Nesreen and M. Mehdi, is featured for the credits.

==References==
 

==External links==
 
*   (film)
*  
*  
*  
*  

 
 
 