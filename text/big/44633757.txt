Sant Gyaneshwar
{{Infobox film
| name = Sant Gyaneshwar
| image = santgyaneswar.jpg
| caption = Theatrical poster
| film name = Sant Gyaneshwar
| director = Manibhai Vyas
| producer = Ranglok
| writer =
| screenplay =
| story =
| based on =
| starring = Sudhir Kumar Surekha Babloo Shahu Modak
| narrator =
| music = Laxmikant Pyarelal|Laxmikant-Pyarelal
| cinematography =
| editing =
| studio =
| production companies =
| distributor =
| released = 1964
| runtime =
| country = India
| language = Hindi
| budget =
| gross =
}}
Sant Gyaneshwar is a 1964 Hindi Bollywood film direct by Manibhai Vyas, starring Sudhir Kumar, Surekha, Babloo and Shahu Modak. 
== Cast ==
* Sudhir Kumar
* Surekha
* Babloo
* Shahu Modak Jeevan
* Ulhas
* Sulochana Latkar Asit Sen

== Soundtrack ==

{{Infobox album
| Name = Sant Gyaneshwar
| Type = soundtrack
| Artist = Laxmikant Pyarelal|Laxmikant-Pyarelal
| Cover =
| Released = 1964
| Recorded = Feature film soundtrack
| Length =
| Label = EMI
| Producer = Ranglok
}}

Music composed by Laxmikant-Pyarelal and lyrics by Bharat Vyas. 

{|
|+ Original Motion Pictures
|-
! Track
! Song
! Singer(s)
|-
| 1
| Jaago Re Jaago Re Prabhat Aaya
| Manna Dey
|-
| 2
| Ek Do Teen Char Bhaiya Bano Hoshiyar 
| Lata Mangeshkar
|-
| 3
| Mere Laadlo Tum Phoolo Phalo
| Lata Mangeshkar
|-
| 4
| Jyot Se Jyot Jagaate Chalo Prem Ki Ganga Bahaate Chalo
| Lata Mangeshkar
|-
| 5
| Bahut Din Beete
| Lata Mangeshkar
|-
| 6
| Jyot Se Jyot Jagaate Chalo Prem Ki Ganga Bahaate Chalo Mukesh
|-
| 7
| Main To Chhail Chhabeeli Naar (Darpan Dekhoon Roop Niharoon)
| Lata Mangeshkar
|-
|}
* Song "Jyot Se Jyot Jagaate Chalo Prem Ki Ganga Bahaate Chalo" was listed at #3 on Binaca Geetmala annual list 1965.

== Awards & Nominations ==
* Nomination Filmfare Award for Best Lyricist - Bharat Vyas for the song "Jyot Se Jyot Jagaate Chalo Prem Ki Ganga Bahaate Chalo". Filmfare Award for Best Playback Singer - Lata Mangeshkar for the song "Jyot Se Jyot Jagaate Chalo Prem Ki Ganga Bahaate Chalo".

== References ==
 
 
 
 


 