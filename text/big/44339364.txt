Sex Ed (film)
{{Infobox film
| name           = Sex Ed
| image          = Sex Ed poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Isaac Feder 
| producer       = Monika Casey Stephen Feder Elayne Schneiderman Dori Sperko 
| writer         = Bill Kennedy  Matt Walsh
| music          = Alexander Kemp 
| cinematography = Brian Burgoyne 
| editing        = Christopher Gay 
| studio         = Sweet Tomato Films
| distributor    = Marvista Entertainment 
| released       =   
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Matt Walsh. Sex Ed premiered at the Portland Film Festival on August 29, 2014,  and was released in theaters on November 7, 2014, by Marvista Entertainment. 

==Plot== Matt Walsh). He moves out of his apartment, allowing his roommate JT (Glen Powell) to engage in kinky sex with his girlfriend Ally (Castille Landon). While working with his students, Ed realizes most of the troubled youths are sexually confused, and begins an after school Sex Ed program. At the same time JT convinces Ed to have sex with someone to end his dry spell, leading to a disastrous blind date with Trish (Abby Elliott), a former co-ed.

Ed seeks counsel from his landlord Sydney (Retta), who also owns a bar in the apartment building. He meets Pilar (Lorenza Izzo), the adult sister of his student Tito (Kevin Hernandez). He and Pilar hit it off, and she invites Ed to her house for dinner after Tito tells her Ed is his favorite teacher. Pilars boyfriend Hector (Ray Santiago) belittles Ed and threatens him, leading to the two competing by taking tequila shots. Ed gets sick and vomits on Pilar and Titos mother, Lupe (Laura Harring). Later Ed apologizes to Pilar, who tells him she and Hector are taking a break. Ed asks her on a date, which she accepts. With the help of JT and Ally, Ed gets his students interested in learning, to the point that other children attend the after school class out of interest.
 Chris Williams) challenging Eds qualifications and reasoning to teach sexual education to the children. Ed asks the Reverend to sit in on a class, but the Reverend is not impressed and decides to shut down the program. Tito tells Ed he wants to have sex with his girlfriend and asks for condom. Ed tells Tito its not a good idea, but Tito says hell do it anyways, forcing Ed to give him a condom. Pilar finds out about this during her date with Ed, and goes home to stop Tito. Ed angrily leaves, but is confronted and beat up by Hector. He then attempts to hire a prostitute, but is caught by the police.

Ed admits to JT that he is a virgin, a fact JT previously knew but chose not to call Ed out on. Ed returns to work at the bagel shop, but then with Sydneys help decides to make a grand gesture to win Pilar back. He plays the oboe for her, despite not being very good at it. Ed and Pilar make up and go on a second date. Pilar decides to visit her favorite club, but Ed is not allowed in by the bouncer (Lamorne Morris). Pilar tells him shell be right back, but the bouncer assures him she wont be. Hector, in line for the club, confronts Ed, and Ed punches Hector, resulting in the police arresting Ed again. Motivated by his anger, Ed forces his way into Reverend Hamiltons support group with his class, insisting the importance of sex education, which the children support. Ed goes as far as admitting he is a virgin in front of everyone. For his demonstration, Reverend Hamilton is impressed and decides to allow the class.

Pilar shows up at Eds apartment and tells him she wants to be his first sexual partner, but bitter that she abandoned him at the club, Ed turns her down. She leaves angrily, but passes JT in her underwear, impressing him. Ed is upset that he still has not had sex, but Sydney tells him he is a man now because he is in control of his life, and it has nothing to do with his virginity. A year later, Ed is shown to have one of the most popular classes at the school, and the respect of his students.

== Cast ==
*Haley Joel Osment as Ed Cole
*Lorenza Izzo as Pilar
*Kevin Hernandez as Tito
*Retta as Sydney Matt Walsh as Washout
*Glen Powell as JT
*Abby Elliott as Trish
*Laura Harring as Lupe
*Castille Landon as Ally
*Lamorne Morris as Bobby the Bouncer
*Ray Santiago as Hector
*Isaac White as Leon Hamilton Chris Williams as Reverend Hamilton
*Parker Young as Montana
*Vadli Belizaire as Shelly
*Parvesh Cheena as Hank
*George Eads as Jimmy
*Monika Casey as Suzanne
*Abbe Meryl Feder as Louise 
*Kelly Gray as Ashley
*Julia E King as Stevie
*Nicholas Patel as Fish
*Ally Rahn as Margarita
*J. Larose as Reiny

==Reception==
Sex Ed received positive reviews from critics. On Rotten Tomatoes, the film has a rating of 70%, based on 10 reviews, with an average rating of 6.1/10.  On Metacritic, the film has a rating of 23 out of 100, based on 4 critics, indicating "generally unfavorable reviews". 

== References ==
 

== External links ==
*  

 
 
 
 
 