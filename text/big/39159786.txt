Blue Is the Warmest Colour
 
 
{{Infobox film
| name           = Blue Is the Warmest Colour
| image          = La Vie dAdèle (movie poster).jpg
| border         = yes
| caption        = French release poster
| director       = Abdellatif Kechiche
| producer       = Abdellatif Kechiche Brahim Chioua Vincent Maraval
| screenplay     = Abdellatif Kechiche  Ghalia Lacroix
| based on       =  
| starring       = Léa Seydoux  Adèle Exarchopoulos 
| cinematography = Sofian El Fani
| editing        = Albertine Lastera Camille Toubkis Sophie Brunet Ghalia Lacroix Jean-Marie Lengelle Wild Bunch France 2 Cinéma Scope Pictures Radio Télévision Belge Francofone Vertigo Films
| distributor    = Wild Bunch
| released       =     
| runtime        = 179 minutes  
| country        = France Belgium Spain 
| language       = French
| budget         = €4 million   
| gross         = $19.5 million   
}}
	 romantic coming-of-age  drama film written, produced, and directed by Abdellatif Kechiche and starring Léa Seydoux and Adèle Exarchopoulos. The film revolves around Adèle (Exarchopoulos), a French teenager who discovers desire and freedom when a blue-haired aspiring painter (Seydoux) enters her life.
 FIPRESCI Prize. It is the first film to have the Palme dOr awarded to both the director and the lead actresses, with Seydoux and Exarchopoulos becoming the only women apart from director Jane Campion to have ever won the award.      
 of the Best Foreign BAFTA Award Best Film not in the English Language.  Many critics declared it to be the best film of 2013.      

==Plot== gay dance bar. After some time, Adèle leaves and walks into a lesbian bar, where she experiences assertive advances from some of the women. The blue haired woman is also there and intervenes, claiming Adèle is her cousin to those pursuing Adèle. The woman is Emma, a graduating art student. They become friends and begin to spend more time with each other. Adèles friends suspect her of being a lesbian and ostracize her at school. Despite the backlash, she becomes very close to Emma. Their bond increases and before long, the two share a kiss at a picnic. They later have sex and begin a passionate relationship. Emmas artsy family is very welcoming to the couple, but Adèle tells her conservative, working-class parents that Emma is just a tutor for philosophy class.

In the years that follow, the two women live with each other as lovers. Adèle finishes school and joins the teaching staff at a local elementary school, while Emma tries to move forward with her painting career. Adèle feels ill at ease among Emmas intellectual friends; and Emma belittles her teaching career, encouraging her to find fulfillment in writing, Adèle enjoys playing the stereotypical feminine role in their relationship but Emma becomes physically and emotionally distant. They gradually begin to realise how little they have in common. Emotional complexities manifest in the relationship and Adèle, in an impulsive moment of loneliness and confusion, sleeps with a male colleague.

Emma becomes aware of the brief fling and kicks Adèle out of their apartment, leaving Adèle heartbroken and alone. Time passes and although Adèle finds satisfaction in her job as a kindergarten teacher, an indescribable sadness begins to overwhelm her. The two eventually meet again in a restaurant. Adèle is still very deeply in love with Emma and despite the powerful connection that is clearly still there between them, Emma is now in a committed partnership with Lise, the pregnant woman at the party they threw a few years earlier, who now has a young daughter. It is implied that the two had known each other for years, and had become reacquainted during the party. Adèle is devastated, but holds it in. Emma admits that she does not feel sexually fulfilled but has accepted it as a part of her new phase in life. She reassures Adèle, though, that their relationship was special: "I have infinite tenderness for you. I always will. All my life long." The two part on amicable terms.
 hang is played over the soundtrack and the film ends.

==Cast==
 
 
* Léa Seydoux as Emma
* Adèle Exarchopoulos as Adèle
* Salim Kechiouche as Samir
* Aurélien Recoing as Adèles father
* Catherine Salée as Adèles mother
* Benjamin Siksou as Antoine
* Mona Walravens as Lise
* Alma Jodorowsky as Béatrice
* Jérémie Laheurte as Thomas
* Anne Loiret as Emmas mother
* Benoît Pilot as Emmas stepfather
* Sandor Funtek as Valentin
* Fanny Maurin as Amélie
* Maelys Cabezon as Laetitia
* Stéphane Mercoyrol as Joachim
* Aurelie Lemanceau as Sabine
 

==Production==

===Adaptation===
  became the source material for the film.]] addiction to perscription pills which takes the life of Adèle when she suffers a seizure.

===Casting===
In late 2011, a casting call was held in Paris to find the ideal actress for the role of Adèle. Casting director Sophie Blanvillain first spotted Adèle Exarchopoulos and then arranged for her to meet Abdellatif Kechiche. Exarchopoulos described how her auditions with Kechiche over the course of two months consisted of improvisation of scenarios, discussions and also of them both sitting in a café, without talking, while he quietly observed her. It was later, a day before the New Year, that Kechiche decided to offer Exarchopoulos the leading role in the film; as he said in an interview, "I chose Adèle the minute I saw her. I had taken her for lunch at a brasserie. She ordered lemon tart and when I saw the way she ate it I thought, Its her!"     

On the other hand, Léa Seydoux was cast for the role of Emma, ten months before   on the preparation for her role, Seydoux said "During those ten months (before shooting) I was already meeting with him (Kechiche) and being directed. We would spend hours talking about women and life; I also took painting and sculpting lessons, and read a lot about art and philosophy."  

===Filming===
Initially planned to be shot in two and a half months, the film took five, from March to August 2012 for a budget of €4 million.  Seven hundred and fifty hours of dailies were shot.  Shooting took place in Lille as well as Roubaix and Liévin. 
 Kechiche this technique not only facilitates editing but also adds beauty to the scene which feels more truthful. Interview with Abdellatif Kechiche. Cahiers du Cinema. Octobre 2013. pp. 10-16  Another characteristic aspect of Blues cinematography is the predominance of close-ups.

===Controversies===
 ]]
Upon its premiere at the 2013 Cannes Festival, a report from the French Audiovisual and Cinematographic Union (Syndicat des professionnels de lindustrie de laudiovisuel et du cinéma) criticised the working conditions from which the crew suffered. According to the report, members of the crew said the production occurred in a "heavy" atmosphere with behaviour close to "moral harassment", which led some members of the crew and workers to quit.  Further criticism targeted disrupted working patterns and salaries.  Technicians accused director Abdellatif Kechiche of harassment, unpaid overtime and violations of labour laws. 

In September 2013, the two main actresses, Léa Seydoux and Adèle Exarchopoulos, also complained about Kechiches behaviour during the shooting. They described the experience as "horrible", and said they would not work with him again.  Exarchopoloulos later said about the rift: "No, it was real, but it was not as big as it looks. For me, a shoot is a human adventure, and in every adventure you have some conflict."  In an interview in January 2014, Seydoux clarified: "Im still very happy with this film. It was hard to film it and maybe people think I was complaining and being spoilt, but thats not it. I just said it was hard. The truth is it was extremely hard but thats OK. I dont mind that it was hard. I like to be tested. Life is much harder. Hes a very honest director and I love his cinema. I really like him as a director. The way he treats us? So what!"   

In an interview in September 2013, director Abdellatif Kechiche stated that the film should not be released. Speaking to French magazine Télérama, Kechiche said "I think this film should not go out; it was too sullied", referring to the negative press about his on-set behaviour.       Regarding his intention portraying young people, Kechiche claimed: "I almost wish I was born now, because young people seem to be much more beautiful and brighter than my generation. I want to pay them tribute." 

==Themes and interpretations==

===LGBT===
Lesbian sexuality is one of the strongest themes of the film, as the narrative deals mainly with Adele’s exploration of her identity in this context. However, the films treatment of lesbian sexuality has been questioned by academics, due to its being directed from a straight, male perspective. In Sight & Sound, film scholar Sophie Mayer suggests that in Blue is the Warmest Colour, "Like homophobia, the lesbian here melts away. As with many male fantasies of lesbianism, the film centres on the erotic success and affective failures of relations between women".  The issue of perspective has also been addressed in a Film Comment review by Kristin M. Jones who points out that "Emmas supposedly sophisticated friends make eager remarks about art and female sexuality that seem to mirror the director’s problematic approach toward the representation of women". 

===Social class===
One recurring thematic element addressed by critics and audiences is the division of  . His fascination and familiarity with the world of pedagogy, as shown here in Adèles touching reverence for teaching, is another notable characteristic", was noted by a Film Comment critic. 

===Realism===
The film portrays Adele and Emmas relationship with an overarching sense of realism. The camerawork, along with many of Kechiches directorial decisions allow a true-to-life feel for the film, which in turn has led to audiences reading the film with meaning that they can derive from their own personal experiences. In The Yale Review, Charles Taylor puts this into words: "Instead of fencing its young lovers within a petting zoo... Kechiche removes the barriers that separate us from them. He brings the camera so close to the faces of his actresses that he seems to be trying to make their flesh more familiar to us than his own." 

===Significance of the colour blue=== Blue Period. As Emma grows out of her relationship with Adèle and their passion wanes, she removes the blue from her hair and adopts a more natural, conservative hairstyle.  There is also a profusion of imagery of food in the film, from the regularly consumed spaghetti to Adèles first experience tasting oysters.  Relationships—both heterosexual and homosexual—are also a common dynamic throughout the film: from Adèles exploration of failed first romance with Thomas, to her affair with a male colleague, and to her love and loss with Emma.

===Food=== flirtation with Emma’s friend Samir, positioned as a potential future partner by the film’s end.’ 

===Music===
One reviewer noted the political stance adopted by Adèle, which changes as her life experiences change and reflect her alternating views: "Blue is the Warmest Colour is no different; at least, at first. Framed by black and Arab faces, Adèle marches in a protest to demand better funding for education. The music, "On lâche rien" ("We will never give up!"), by the Algerian-born Kaddour Haddadi, is the official song of the French Communist Party. Yet, soon after she begins her relationship with Emma, we see Adèle marching again, hip-to-hip with her new lover, at a gay pride parade." 

==Distribution==
 .]]

===Release=== 66th Cannes Film Festival on 23 May 2013. It received a standing ovation and ranked highest in critics polls at the festival.  In August 2013, the film had its North American premiere at the 2013 Telluride Film Festival and was also screened in the Special Presentation section of the 2013 Toronto International Film Festival on 5 September 2013. 		

The film was screened at more than 131 territories  and was commercially released on 9 October 2013 in France with a French film ratings#France|"12" rating.  In the United States, the film was rated List of NC-17 rated films|NC-17 by the Motion Picture Association of America for "explicit sexual content". It had a limited release at four theaters in New York City and Los Angeles on 25 October 2013, and expanded gradually in subsequent weeks.             The film was released on 15 November 2013 in the United Kingdom    and in Australia and New Zealand on 13 February 2014.   

===Home media=== Wild Side Transmission Films.

In Brazil, Blu-ray manufacturing companies Sonopress and Sony DADC are refusing to produce the film because of its content. The distributor is struggling to reverse this situation. 

==Reception==

===Box office===
 .]]
Blue Is the Warmest Colour grossed a worldwide total of $19,492,879.  During its opening in France, the film debuted with a weekend total of $2.3 million on 285 screens for a $8,200 per-screen average. It took the fourth spot in its first weekend, which was seen as a "notably good showing because of its nearly three-hour length".   The film had a limited release in the U.S, and it grossed an estimated $101,116 in its first weekend, with an average of $25,279 for four theaters in New York City and Los Angeles. 

===Critical response=== normalized rating out of 100 based on reviews from mainstream critics, the film has a score of 88 averaged from 41 reviews, indicating "universal acclaim". 

At Cannes, the film shocked some critics with its long  and graphic sex scenes (although fake genitalia were used),    leading them to state that the film may require some editing before it is screened in cinemas.    Several critics placed the film as the front-runner to win the Palme dOr and it went on to win the coveted prize.            The judging panel, which included Steven Spielberg, Ang Lee and Nicole Kidman, made an unprecedented move to award the top prize to the films two main actresses along with the director. Jury president Steven Spielberg explained:

 "The film is a great love story that made all of us feel privileged to be a fly on the wall, to see this story of deep love and deep heartbreak evolve from the beginning. The director did not put any constraints on the narrative and we were absolutely spellbound by the amazing performances of the two actresses, and especially the way the director observed his characters and just let the characters breathe."   

Justin Chang, writing for Variety (magazine)|Variety, said that the film contains "the most explosively graphic lesbian sex scenes in recent memory".    Jordan Mintzer of The Hollywood Reporter said that despite the film being three hours long, it "is held together by phenomenal turns from Léa Seydoux and newcomer Adèle Exarchopoulos, in what is clearly a breakout performance".   

In  , Peter Bradshaw added that "it is genuinely passionate film-making" and changed his star rating for the film to five out of five stars after previously having awarded it only four.     Stephen Garrett of The New York Observer said that the film was "nothing less than a triumph" and "is a major work of sexual awakening".   

{{quote box width     = 29em border    = 1px align     = right bgcolor   = #c6dbf7 halign    =  quote     = If the film were just a series of sex scenes it would, of course, be problematic, but its much, much more than that. Through the eyes of Adèle we experience the breathless excitement of first love and first physical contact, but then, inevitably, all the other experiences that make life the way it is - the quarrels, the disappointments, the infidelities, the break-ups, the difficult, painful re-connections. All of these are beautifully documented. source    =&nbsp;— David Stratton, The Australian. 
}}
 Happy Together, director Abdellatif Kechiche knows love and relationship well and the details he goes about everything is almost breathtaking to endure. There is a scene in the restaurant where two meet again, after years of separation, the tears that dwell on their eyes shows precisely how much they love each other, yet there is no way they will be together again. Blue Is the Warmest Colour is likely to be 2013s most powerful film and easily one of the best." 

Movie Room Reviews praised the film giving it 4   stars saying "This is not a movie that deals with the intolerance of homosexuality or what it’s like to be a lesbian. It’s about wanting to explore that piece of yourself you know is there while still dealing with the awkwardness of youth." 

Spanish film director Pedro Almodóvar named the film as one of the twelve best films of 2013. 

The film was not without its criticisms, many of them about the sex scenes. Manohla Dargis of The New York Times described the film as "wildly undisciplined" and overlong and wrote that it "feels far more about Mr. Kechiches desires than anything else". 
 heteronormative laughed because they dont understand it and find the scene ridiculous. The gay and queer people laughed because its not convincing, and found it ridiculous." She continued by writing that "as a feminist and lesbian spectator, I cannot endorse the direction Kechiche took on these matters. But Im also looking forward to hearing what other women will think about it. This is simply my personal stance." 

Richard Brody, writing in The New Yorker about "Sex Scenes That Are Too Good", stated:

: "The problem with Kechiche’s scenes is that they’re too good—too unusual, too challenging, too original—to be assimilated ... to the familiar moviegoing experience. Their duration alone is exceptional, as is their emphasis on the physical struggle, the passionate and uninhibited athleticism of sex, the profound marking of the characters souls by their sexual relationship."   

====Film critic Top Ten lists====
Various critics have named the film as one of the best of 2013.

 
* 1st&nbsp;– AlloCiné   
* 1st&nbsp;– The Times 
* 1st&nbsp;– Mick LaSalle, San Francisco Chronicle  NOW (Toronto)  Michael Atkinson, Sundance Now  Metronews (UK) 
* 1st&nbsp;– Guillaume Loison, Le Nouvel Observateur  Premiere (France)   
* 1st&nbsp;– Philippe Rouyer, Positif (magazine)|Positif 
* 1st&nbsp;– Thomas Baurez, Studio Ciné Live  Le JDD  TF1 News 
* 1st&nbsp;– Michael Melinard, LHumanité 
* 1st&nbsp;– Evangeline Barbaroux, La Chaîne Info|LCI  Metronews (France) 
* 2nd&nbsp;– Carlos Aguilar, Indiewire  
* 2nd&nbsp;– Stephanie Zacharek, The Village Voice   
* 2nd&nbsp;– Scott MacDonald, The A.V. Club  The Star   
* 3rd&nbsp;– Cahiers du cinéma   
* 3rd&nbsp;– Jessica Kiang, Indiewire  
* 3rd&nbsp;– Sight & Sound   
* 3rd&nbsp;– A. O. Scott, The New York Times    The Telegraph  Time Out London   
* 3rd&nbsp;– Marlow Stern, The Daily Beast   
* 3rd&nbsp;– WhatCulture   
* 4th&nbsp;– Scott Foundas & Justin Chang, Variety (magazine)|Variety      
* 4th&nbsp;– Michael Dunaway, Paste (magazine)|Paste 
* 4th&nbsp;– James Berardinelli, Reelviews 
* 4th&nbsp;– Robert Horton, Seattle Weekly 
* 5th&nbsp;– Eric Kohn, Indiewire   
* 5th&nbsp;- Total Films 50 Best Movies of 2013   
* 5th&nbsp;– Betsy Sharkey, Los Angeles Times 
* 5th&nbsp;– Brian D. Johnson, Macleans 
* 5th&nbsp;– Scott Feinberg, The Hollywood Reporter 
* 5th&nbsp;– Andrew OHehir, Salon (website)|Salon 
* 6th&nbsp;– Digital Spy 
* 6th&nbsp;– Film.com 
* 6th&nbsp;– Rich Cline, Contactmusic.com 
* 6th&nbsp;– Scott Feinberg, The Hollywood Reporter  Cleveland Plain Dealer 
* 6th&nbsp;– Keith Phipps & Scott Tobias, The Dissolve 
* 8th&nbsp;– Stephen Holden, The New York Times   
* 8th&nbsp;– Jake Coyle, Associated Press   
* 8th&nbsp;– Richard Brody, The New Yorker   
* 8th&nbsp;– Robert Gifford, The Diamondback   
* 9th&nbsp;– David Edelstein, New York (magazine)|Vulture   
* 9th&nbsp;– Ben Kenigsberg, The A.V. Club   
* 9th&nbsp;– Jordan Hoffman, ScreenCrush   
* 9th&nbsp;– Guy Lodge, Hitfix 
* 10th&nbsp;– IMDb Best of 2013  
* 10th&nbsp;– Chris Vognar, The Dallas Morning News    Time Out London   
* Best films of 2013 – Peter Bradshaw, The Guardian   
* Best movies of the year – David Denby, The New Yorker 
* Top Five Movies of 2013 – Daily Mail 
 

==Accolades==
 
  FIPRESCI Prize. Best Foreign BAFTA Award Best Film not in the English Language.    At the 39th César Awards, the film received eight nominations with Exarchopoulos winning the César Award for Most Promising Actress.      

==See also==
* List of French films of 2013
* List of lesbian, gay, bisexual or transgender-related films of 2013

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
{{Navboxes
|title=Awards for Blue Is the Warmest Colour
|list1=
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 