Malèna
{{Infobox film
| name           = Malèna
| image          = Malena 101.jpg
| caption        = Theatrical release poster
| director       = Giuseppe Tornatore
| producer       = Harvey Weinstein
| screenplay     = Giuseppe Tornatore
| story          = Luciano Vincenzoni
| starring       =  
| music          = Ennio Morricone
| cinematography = Lajos Koltai
| editing        = Massimo Quaglia
| distributor    = Miramax Films
| released       =  
| runtime        = 109 minutes (United States cut: 92 minutes)
| country        = Italy
| language       = Italian
| gross          = $14,493,284
|}} written by story by Luciano Vincenzoni.  It won the Grand Prix du Festival at the 2001 Cabourg Film Festival. 

==Plot==
The film begins in  , she continues to be faithful to her husband. Renato becomes obsessed with Malena and starts fantasizing about her. His fantasies become increasingly elaborate and he becomes obsessed with the shy young woman, peeping in her window often as she waits sadly for her beloved husband to return. Renato eventually steals Malenas underwear and begins to fantasize about her in bed, to the horror of his parents. They do everything to stop his behavior, but it is all in vain.

Malena soon receives word that her husband has been killed and her grief consumes her. Renato continues to watch Malena as she suffers from grief. Malena is shunned by the townspeople who begin to believe the worst about her, simply because of her beauty. Women spread terrible rumors and men encourage the rumors by lurking around the poor widow, who does nothing to defend herself; she just wants to be left alone.

She visits her father regularly and helps him with his chores, but when a slanderous letter reaches his hands, their relationship suffers a catastrophic blow. Things only get worse when the wife of the local dentist takes Malena to court, accusing her of having an affair with her husband, but Malena is acquitted. The Court is told that Malena is being harassed for being beautiful, as other ladies feel insecure and threatened by her. The only man that the lonely and sad Malena has a romance with, an army officer, is sent away after saying he and Malena were "just friends". The betrayal cuts deeply, but Malena says nothing to condemn the officer. After her acquittal, Malenas lawyer Centorbi comes to her home and asks for a dance and during the dance, using her unpaid legal fee as leverage, rapes her while Renato peeps in from outside her house.

Renato increasingly sees himself as Malenas protector, but he does not even realize that his views of her are little better than those of the townspeople. While he asks God to protect her and personally performs little acts of vengeance against those who slander Malena, he takes no time to realize how Malena herself feels. He even rationalizes the rape as a choice Malena made to pay her legal fee.
 German army possessed and take him to church for an exorcism. His father, however, takes him to a brothel; Renato has sex with one of the prostitutes while fantasizing that she is Malena.
 war ends anonymous letter about Malenas whereabouts, the fact that she always loved only him, and the lie in all the rumors about her cheating. Nino goes to Messina to find her and, a year later, they are seen walking down the street, Nino proudly arm-in-arm with his still-beautiful wife. The villagers, especially the women, astonished at her courage, begin to talk to "Signora Scordia" with respect. Though still beautiful, they think of her as no threat, claiming that she has wrinkles near her eyes and has put on some weight. Malena, however, is as shy as ever and wary of the attention after her experiences.

In the last scene near the beach, Renato helps her pick up some oranges that had dropped from her shopping bag. Afterwards, he wishes her "Buona fortuna, Signora Malena" (good luck, Mrs. Malena) and rides off on his bicycle, looking back at her for a final time, as she walks away. This is the first and only time they speak to each other in the movie. As this final scene fades out, an adult Renatos voice-over reflects that he has not forgotten Malena, even after the passage of so many years. He says, according to the English subtitles, "Time has passed and I have loved many women. And as theyve held me close and asked if I will remember them, I believed in my heart that I would. But the only one Ive never forgotten is the one who never asked ... Malena"

==Cast==
* Monica Bellucci as Malena Scordia
*   as Renato Amoroso
* Luciano Federico as Renatos Father
* Matilde Piana as Renatos Mother
* Pietro Notarianni as Professor Bonsignore
* Gaetano Aronica as Nino Scordia
* Gilberto Idonea as Avvocato Centorbi
* Angelo Pellegrino as Segretario politico
* Gabriella Di Luzio as Mantenuta del Barone

==Critical reception== existential epic, The Legend of 1900, Giuseppe Tornatores Malena is a beautifully crafted but slight period drama that chronicles a 13-year-old boys obsession with a small-town siren in World War II Sicily. Combining a coming-of-age story with the sad odyssey of a woman punished for her beauty, the film ultimately has too little depth, subtlety, thematic consequence or contemporary relevance to make it a strong contender for arthouse crossover. But its erotic elements and nostalgic evocation of the same vanished Italy that made international hits of Cinema Paradiso and Il Postino could supply commercial leverage." 

Film critic   and 8½). But Fellini sees the humor that underlies sexual obsession, except (usually but not always) in the eyes of the participants. Malena is a simpler story, in which a young man grows up transfixed by a woman and essentially marries himself to the idea of her. It doesnt help that the movies action grows steadily gloomier, leading to a public humiliation that seems wildly out of scale with what has gone before and to an ending that is intended to move us much more deeply, alas, than it can." 

The film has a 55% rating on Rotten Tomatoes, based on reviews of 76 critics. 

==Music==
 
 Best Original Best Original Score.

==Awards==
 
Wins
* Cabourg Film Festival: Swann dOr (Grand Prize to the Best Film), Giuseppe Tornatore, 2001.
*  . 2001.
*  .
 
Nominations Best Original Score, Ennio Morricone, 2001.
*  ; Best Editing: Massimo Quaglia; Best Production Design: Francesco Frigeri, 2001. Best Original Score - Motion Picture, Ennio Morricone; 2001.
*  , Harvey Weinstein, Carlo Bernasconi and Giuseppe Tornatore; 2001.
* Berlin International Film Festival: Golden Berlin Bear, Giuseppe Tornatore, 2001.
* David di Donatello Awards: David, Best Costume Design, Maurizio Millenotti; Best Music, Ennio Morricone; Best Production Design, Francesco Frigeri; 2001.
* European Film Awards: Audience Award, Best Actress, Monica Bellucci; Best Director, Giuseppe Tornatore, 2001.
* Las Vegas Film Critics Society Awards: Sierra Award, Best Foreign Film, 2001.
* Phoenix Film Critics Society Awards: PFCS Award, Best Foreign Language Film, 2001.
* Satellite Awards: Golden Satellite Award, Best Motion Picture, Foreign Language; Best Original Score, Ennio Morricone, 2001.
* Vihari Award: Best Actress for seductive role in the year 2000.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 