Highway to Hell (film)
 
{{Infobox film
| name           = Highway to Hell
| image          = Highwaytohell1992.jpg
| image_size     = 250px
| caption        = VHS release cover
| director       = Ate de Jong
| producer       = {{plainlist|
* John Byers
* Mary Ann Page
}}
| writer         = Brian Helgeland
| starring       = {{plainlist|
* Patrick Bergin
* Chad Lowe
* Kristy Swanson
* Jarrett Lennon
* Adam Storke
* Ben Stiller
* Jerry Stiller
}}
| music          =
| cinematography = Robin Vidgeon
| editing        = {{plainlist|
* Todd Ramsay
* Randy D. Thonrton
}}
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| studio         = {{plainlist|
* Goodman/Rosen Productions
* Josa
* High Street Pictures
}} Hemdale
| budget         =
| gross          = $26,055 
}} horror comedy Jerry and Ben Stiller.

== Plot ==
  Las Vegas.  On the road to Vegas, they ignore the warning of a local gas station attendant named Sam. They take an abandoned backroad where Rachel is kidnapped by a zombie Hell Cop who takes her to hell.  Charlie goes back to Sam, and Sam explains what the Hellcop is and how to save her. Sam then gives Charlie a shotgun with special ammo and a car that holds a special attribute.  On the highway, Charlie meets other dead people that live in Hell and even a motorcycle gang.  On the road, he meets a mechanic named Beezle, with his young apprentice.  Beezle gives him tips on how to save his girlfriend and even brings him back to life.  After Charlie rescues Rachel, Beezle reveals himself to be Satan and proposes a deal to let them, and his apprentice, go free if they can defeat the Hell Cop in a race to the portal that connects Earth and the backroads of Hell.

== Cast == Beezle
* Chad Lowe as Charlie Sykes
* Kristy Swanson as Rachel Clark
* Jarrett Lennon as Adam
* Adam Storke as Royce
* Pamela Gidley as Clara
* C.J. Graham as Sgt. Bedlam, Hellcop
* Richard Farnsworth as Sam
* Lita Ford as The Hitchhiker Hitler
* Anne Meara as Medea, Waitress in Plutos
* Amy Stiller as Cleopatra
* Ben Stiller as Plutos Cook/Attila the Hun 
* Jerry Stiller as The Desk Cop
* Be Deckard as Dentist, Royces Gang
* Michael Reid MacKay as Rachel Demon
* Kevin Peter Hall as Charon

== Production ==
The film was shot in Phoenix, Arizona.   

== Release ==
Hemdale shelved the film for a year before finally giving it a limited release.     It was released to home video on August 21, 1992. 

== Reception == Kevin Thomas of the Los Angeles Times wrote, "Although ambitious, amusing and even romantic, replete with lots of striking sets and jazzy special effects, its humor is not sophisticated enough to attract the wide audiences of a Beetlejuice."   Michael Dare of Billboard (magazine)|Billboard called it "smart, witty, and incredibly imaginative".  TV Guide rated it 2/5 stars and wrote, "Highway to Hell is no masterpiece, but it is a genuine video find."   Todd Rigney of Beyond Hollywood called it a "deliriously enjoyable satanic road trip" film that is "fun if you approach it in the right frame of mind".   HorrorNews.Net called it "one of the greatest campy horror films to never arrive on DVD". 
 cult following. 

== See also == The road to hell is paved with good intentions

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 