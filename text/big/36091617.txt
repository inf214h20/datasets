That Summer!
{{Infobox film
| name     = That Summer!
| image    =
| director = Harley Cokeliss
| producer = Davina Belling
| writer   = Tony Attard Jane Preger
| starring = Ray Winstone Tony London Emily Moore Julie Shipley
| released =  
| runtime = 94 minutes
| country = United Kingdom
| language = English
| budget =
}}
That Summer! is a 1979 British drama film directed by Harley Cokeliss, starring Ray Winstone, Tony London, Emily Moore and Julie Shipley. This was Ray Winstones theatrical film debut, playing the character Steve Brodie.

==Plot== chambermaids at Scottish youths have some confrontations with Steve and then frame him for a robbery at a chemist shop. This leads to his arrest just before the start of a round the bay swimming race in which he is due to compete against one of them.

The soundtrack was produced as an LP with several punk bands. The album was available on unique yellow vinyl.

==Award==
Winstone received a BAFTA award as Best Newcomer for his performance in the film and he met his wife-to-be Elaine when shooting it in Torquay. 

==Music== new wave groups including Elvis Costello| Elvis Costello and The Attractions and the Ramones can be heard in the background during beach and pub scenes and a soundtrack was released on Arista Records. 

==Cast==
* Ray Winstone – Steve Brodie
* Tony London – Jimmy
* Emily Moore – Carole
* Julie Shipley – Angie
*Andrew Byatt - Georgie
* John Morrison – Tam
* Ewan Stewart – Stu
* John Junkin – Mr Swales
* John Judd – Swimming coach
* David Daker – Pub landlord
* Jo Rowbottom – Pub landlady
* Stephanie Cole - Mrs Mainwaring
* Nicholas Donnelly – Detective
* Nick Stringer – Policeman
* Michael Crolla – Barman in Disco
* Colin Higgins – Probation officer
* Ben Howard – Borstal officer Michael J. Jackson – Hotel clerk
* David Lloyd Meredith – Beach skiff man
* David Pratt - Coach Driver

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 