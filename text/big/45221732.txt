I’m Obsessed with You
I’m Obsessed with You (But You’ve Got to Leave Me Alone)  is an American comedy-drama film directed by Jon Goracy, written by Genevieve Adams, and starring Manish Dayal, Rachel Brosnahan, Thomas McDonell, Genevieve Adams and Jason Ralph.  

The film premiered at the Sonoma International Film Festival in April 2014  and opened on video on demand November 11th.  

==Cast==
* Manish Dayal
* Rachel Brosnahan
* Thomas McDonell
* Olek Krupa
* Jason Ralph
* Genevieve Adams

==Plot==
When a magnetic movie star crashes their party the day before graduation, four collegiate comedians are forced to confront the groups friendship and future.  

==Production==
The film was based on an off-Broadway play, “IMPROVed”, written by Genevieve Adams. Adams developed the play from her Dartmouth College senior thesis.  After the Broadway success of “IMPROVed”, Adams created a Kickstarter campaign to raise funds for the production of a feature film based on the play.  Following his work with Adams on “IMPROVed”, John Goracy joined the production team for “I’m Obsessed with You” as the film’s director.  The film was shot in Dartmouth College,   New York City, and the Hamptons. 

==References==
 

* 
* 
* 
*

== External links ==
*  
*  
*  

 
 
 
 
 
 