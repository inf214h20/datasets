The 24 Hour Woman
 
{{Infobox film
| name           = The 24 Hour Woman
| image          = 24 Hour Woman.jpg
| image_size     =
| alt            =
| caption        = Original movie poster
| director       = Nancy Savoca
| producer       = Larry Meistrich Richard Guay
| starring       = Rosie Perez Marianne Jean-Baptiste Patti LuPone Karen Duffy
| music          =
| cinematography = Teresa Medina
| editing        = Camilla Toniolo
| studio         = Shooting Gallery
| distributor    = Artisan Entertainment
| released       = 29 January 1999 (USA)
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $2,500,000
| gross          = $109,535 (USA)
}}

The 24 Hour Woman  is a 1999 film directed and co-written by Nancy Savoca.  The film was shot on location in New York City. 

==Taglines==
"A story about getting everything you want and what comes next."

==Plot==
 
Grace (Rosie Perez) struggles to be both a successful television producer and mother.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Rosie Perez || Grace Santos
|-
| Marianne Jean-Baptiste || Madeline Labelle
|-
| Patti LuPone || Joan Marshall
|-
| Karen Duffy || Margo Lynn
|-
| Diego Serrano || Eddie Diaz
|- Melissa Leo || Dr. Suzanne Pincus
|-
| Wendell Pierce || Roy Labelle
|-
| Chris Cooper || Ron Hacksby
|}

==Critical reception==
Janet Maslin of The New York Times found the films depiction of working women to be genuine but overall did not think highly of the film:
{{cquote |The 24-Hour Woman, in which a woman with a high-powered job tries to cope with the high-powered hassles of balancing career and family. But somebody on the other side of the camera has clearly been here and done this, and boy, does it show on screen... It would be nice to report that Ms. Savoca, whose True Love belongs high on any list of films rich in womanly wisdom and whose Household Saints and Dogfight were no less fearless, had made the most of such a golden opportunity to hit a nerve. But The 24-Hour Woman is an unexpectedly strained farce, and not often a very credible one at that. With the notable exception of a long, classic comedy-of-errors sequence about a working mother racing to her childs first birthday party, the film is otherwise a homey compendium of feminist talking points laced with awkward satire. 
}}

However, Roger Ebert of the Chicago Sun-Times enjoyed the film and rated it 3 stars out of his 4 star rating system and overall thought it was a pleasant experience:
 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 