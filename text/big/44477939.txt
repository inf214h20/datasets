Nagara Hole
 
{{Infobox film
| name           = Nagara Hole
| image          = 
| caption        =
| director       = S. V. Rajendra Singh Babu
| producer       = S. N. Parthanath R. F. Manik Chand C. H. Balaji Singh
| writer         = H V Subba Rao Lyrics: Chi Udayashankar R N Jayagopal Vijaya Narasimha
| based on       = 
| screenplay     = S. V. Rajendra Singh Babu Vishnuvardhan Bharathi Bharathi Master Prasad Tagat Baby Indira Ambarish
| music          = Chellapilla Satyam
| cinematography = P. S. Prakash
| editing        = Bal G. Yadav
| production companies = Mahatma Productions
| distributor    = Varuna Pictures
| released       =  
| runtime        = 139 minutes
| country        = India
| language       = Kannada
| budget         = 
}}
 1977 Kannada Bharathi and Vishnuvardhan (actor)|Vishnuvardhan.

==Plot==
Madhu (Bharathi Vishnuvardhan|Bharathi) takes 4 children on a dangerous visit to the Nagarhole National Park

==Cast==
  Bharathi as Madhu Vishnuvardhan
* Ambarish
* Radha (actress)|B.V. Radha
* Prasad Tagat
* Baby Indira
* Shivaram
* Uma Shivakumar	
* Sundar Krishna Urs
* Bhanuprakash
* Sathish
* M.N Lakshmi Devi
* M. S. Umesh
* Shakthi Prasad
* Dinesh
* Chethan Ramarao
 

==Soundtrack==
The title song Ille Swarga Ille Naraka was sung Ravi (music director)|Ravi, and released in 2004 by Saregama.      

# Ee Notake Mai Maatake, singers: Bharathi Vishnuvardhan|Bharathi, Vishnuvardhan (actor)|Vishnuvardhan. lyrics: Chi Udayashankar	
# Ille Swarga Ille Naraka, singer: Ravi (music director)|Ravi, lyrics: Chi Udayashankar
# Hey Hey Piltu Hey Hey Chiltu, singer:  , R N Jayagopal
# Naagaraholeyo Ammaale, singers:  

==Recognition==
* Won Karnataka State Film Awards for best child actor 1976-1977 for Prasad Tagat, Bhanuprakash, Sathish and Baby Indira   

==References==
 

==External links==
*   at the Internet Movie Database

 

 
 
 


 