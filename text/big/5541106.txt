Spider Forest
{{Infobox film name           = Spider Forest image          = Spider Forest film poster.jpg director       = Song Il-gon producer       = Ahn Min-jun writer         = Song Il-gon starring       = Kam Woo-sung   music          = Yun Min-hwa cinematography = Kim Cheol-ju editing        = Choi Jae-geun distributor    = Chungeorahm released       =   runtime        = 113 minutes country        = South Korea language       = Korean budget         = United States dollar|$1,300,000
| film name      = {{Film name hangul         =   rr             = Geomisup mr             = Kŏmisup}}
}} 2004 South Korean psychological thriller written and directed by Song Il-gon.

==Plot==
 
Awaking alone in the middle of a dark forest, Kang Min sees a secluded cabin nearby and wanders towards it. Upon entering the small home, he is shocked to discover a brutal, bloody crime had taken place. A man, hacked repeatedly by a sickle, lies on the floor, dead. Min hears a noise nearby and discovers his girlfriend Su-Young, stabbed and nearly dead, stammering something about "spiders." 

Before he could save his loved one, Min is caught off guard by the killer who unexpectedly emerges and dashes out of the cabin. Min grabs the sickle and pursues the dark figure through the shadowy woods of the Spider Forest, only to be temporarily knocked out with a blunt blow to the face. Min, being disoriented and slightly dazed,  sees the killer enter into a tunnel on a road nearby and stumbles after him. Yet upon entering the tunnel, a speeding SUV collides with Min and throws him to the ground, severely injured by the impact. 

As he lies on the pavement with his blood streaming across the ground, the dark figure approaches Min and stands mere inches away, as if mocking his inability to capture him. Min extends his arm, desperately trying to grab him, but in vain and he soon loses consciousness...

Min wakes-up fourteen days later at a local hospital with his head heavily bandaged. His friend, a police detective named Choi, is at his bedside to both comfort and question him about the murders. Min discovers that he, in fact, is the prime suspect of the killings due to his fingerprints being on the sickle that he picked up and his relationship with the victims. Choi, wanting to believe in his friends innocence, asks Min to tell him everything that had happened leading up to the brutal crime. Weaving in and out of consciousness, Min tries to reconstruct the bizarre events, but it proves to be a too difficult task. Min finds that there are some details he cannot remember, while other details he cant even be sure they actually happened. The boundary between dreams and reality become blurred as he tries to piece together his enigmatic past in an effort to complete a puzzle that will, hopefully, prove his innocence...

==Cast==
*Kam Woo-sung ... Kang Min
*Kang Kyeong-heon ... Hwang Su-yeong
*Suh Jung ... Min Su-jin
*Jang Hyun-sung ... Choi Seong-hyeon
*Son Byung-ho ... Kim Cheol-ju

==Screenplay==
 
*Writer/director Song had originally written a script that thoroughly explained the mystery driving the plot, then deliberately removed as much of that explanation as he thought he could get away with. James Brown,  , Senses of Cinema. 
*His storytelling method is very much from the European arthouse mode: he leaves the answers, the interpretation, up to the audience. 
*Director Song on the script:
 I started Song Il-gon|Dont Forget Him
When Hes Cool: An Interview with Song Il-gon.}}
*The director also "brought a lot of mythology into Spider Forest, like Orpheus." 

==Budget issues==
 
*The films budget was US $1.3 million, which is low for a Korean film. The average is usually around $3 million. 
*Director Song said it was very difficult to get the money to make and finish the film. 
*Thirty percent of the money came from KOFIC   
*Director Song adds, "The government supported it, which was very important." 

===Actors pay===
*Actress Suh Jung accepted a back-end deal in lieu of a large up-front salary. Suh Jung received very little compensation for her points contract, which was only tied to domestic release. 
*Leading man Kam Woo-sungs salary was 20 percent of the films production budget, about US$260,000. 

==Box office==
*The film encountered difficulties in finding a domestic audience. It gained little more than 30,000 admissions in Seoul, one-tenth of the average (Korean pictures averaged more than 300,000 admissions in Seoul in 2004). 
*According to Song, it was successfully sold to various foreign territories at the European Film Market. 
*Total South Korea Box Office: 83,411 admissions.  , Koreanfilm.org. Retrieved on March 5, 2008. 

==Critical response==
===Domestic===
*The films short domestic run received scant attention from local critics. 
*The Korea Herald astutely, but disapprovingly, referred to the film as a “a jigsaw puzzle with some of its pieces missing”, calling attention to Songs intentional plot-trimming strategies. 
*The Korean Times said the film was "a web of psychological twists and turns that will leave you trying to get untangled long after you leave the theater." The review added, "The story does get pretty muddled at times but at its best moments the film is as frightening an experience as they come."  
*The Dong-A Daily News describes the film as "an unusual movie that drives audiences to solve a puzzle that never ends...   belongs to the mystery thriller genre, but its sophisticated editing makes the movie devoid of fast story development, the major feature of a mystery thriller."  

===International=== Tartan Video), the film received more attention from international critics than it received from its own country.
*Eye Weekly, which reviewed the film at The International Toronto Film Festival, referred to the film as a "supernatural thriller" with "a dark look, a genuinely puzzling (and engrossing) set-up, and a couple of memorable ghostly passages." However, it asks in a condescending manner, "Why cant it make even a lick of sense?"    
*Twitch also reviewed the film at The International Toronto Film Festival, describing it as "well written, well executed and well shot," but concludes by stating, "the film as a whole somehow ends up being slightly less than the sum of its parts."  Todd Brown,   Twitch. 

==Film festivals==
*2004 Cannes Film Market
*2004 San Sebastián International Film Festival
*2004 Toronto Film Festival
*2005 Adelaide Film Festival
*2005 Hong Kong International Film Festival
*2005 Cognac Film Festival
*2005 New York Korean Film Festival Josh Ralske,   allmovie.com. 
*2006 Amsterdam Fantastic Film Festival   imdb.com. 

==Release dates== Cannes Film Market)
*Spain - September, 2004 (San Sebastián Film Festival)
*South Korea - 3 September 2004 	
*Canada - 11 September 2004 (Toronto Film Festival)
*Australia - 20 February 2005 (Adelaide Film Festival)
*Hong Kong - 25 March 2005 (Hong Kong International Film Festival)
*France - 8 April 2005 (Cognac Film Festival)
*Japan - 9 April 2005
*Netherlands - 25 April 2006 (Amsterdam Fantastic Film Festival) 

==Awards==
*Award: 2004 - Nominated - Golden Seashell - San Sebastián International Film Festival   imdb.com. 
*Award: 2004 - Nominated -  Best Supporting Actress (Kang Gyeong-Heon) - Korean Film Awards (MBC)   cinemasie.com 
*On the top ten Korean films for 2004 by Koreanfilm.org Paolo Bertolin and Tom Giammarco,   Koreanfilm.org. 

==See also==
*K-Horror

==References==
 

==External links==
* 
*  at the Korean Movie Database
*  at HanCinema
* 
*  - An Interview with director Song Il-gon regarding his film Spider Forest
* 
* 
*  - Mike Walsh interviews Korean filmmaker Song Il-gon
* 
* 
* 
* 
*  

 
 
 
 
 
 