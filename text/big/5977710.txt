J-Men Forever
{{Infobox Film

 | name = J-Men Forever
 | image_size = 
 | caption = 
 | director = Richard Patterson
 | producer = Patrick Curtis William Howard Willy Schopfer Peter Bergman Philip Proctor
 | narrator =  Peter Bergman M. G. Kelly Philip Proctor
 | music = Richard H. Theiss
 | cinematography = Bruce Logan
 | editing = Gail Werbin
 | distributor = Pan-Canadian Film Distributors International Harmony (US)
 | released = 1979
 | runtime = 73 min.
 | country = United States
 | language = English
 | budget = 
 | gross = 
 | preceded_by = 
 | followed_by = 
 | image = J-Men Forever VideoCover.png
}}
 Republic Movie serial|serials, re-dubbed with comic dialog to tell a tale of world conquest by sex, drugs and rock and roll.
 DJ M.G. Kelly (also called "Machine Gun" Kelly). 

Peter Bergman plays The Chief and Philip Proctor plays Agent Barton. They appear in period-style black and white sequences that are used to frame the re-dubbed clips  of car chases, explosions, flying men, sinister villains and villainesses, fights, and various other perils that are strung together in a somewhat incoherent plot (narrative)|plot.

==Synopsis==
 Lawrence Milk Jive Davis, who are hypnotized or otherwise prodded into killing themselves, and bandleader Jimmy Dorsey|Scream Dorsey, whose car is boobytrapped and then run off a cliff.   The Bug, his henchmen and henchwomen (including the villainess Sombra) are opposed by the J-Men, a group of government agents hired by the legendary J. Edgar Hoover|J. Eager Believer.   
 Captain Marvel), Spy Smasher), Sleeve Coat, Juicy Withers, and Admiral Balzy.   Many of them appear to die horrible, inescapable deaths in the course of the film.

The J-Men work in cooperation with the Federal Communications Commission|F.C.C. (Federal Culture Control), opposing the Lightning Bug with Muzac (created by Muzak Holdings|M.U.S.A.C., the Military Underground Sugared Airwaves Command), then with a bomb to blow up the Lightning Bugs base on the Moon.   However, the Lightning Bug beats them to it, by turning his stereo up too loud and blowing up the Moon himself.   

At the end of the film, Agent Barton mournfully recites the list of J-Men who supposedly gave their lives in the epic struggle against the Bug.   The Chief laughs, then starts choking on a cigar he is smoking.   After he stops choking, The Chief points out that J-Men are flexible enough to survive any life-threatening situation, and the final clips (from next weeks edition of the serial) show exactly how each J-Man escaped their particular peril.

==Soundtrack==

The movie soundtrack features music from Budgie, The Tubes, Head East and Billy Preston.

*See also Whats Up Tiger Lily.

==References==
 
 

==External links==
 
*  
*  
 

 
 