Closed Mondays
{{Infobox Film
| name           = Closed Mondays
| image          =
| image_size     =
| caption        = Bob Gardiner Will Vinton
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1974
| runtime        = 8 mins.   Closed Mondays |url=http://www.animest.ro/closed-mondays.aspx|work=animest|publisher=ESTENEST Association|accessdate=4 June 2012|author=Staff|language=Romanian, English|year=2012}} 
| country        = USA English
| budget         =
| preceded_by    =
| followed_by    =
}}
 Bob Gardiner in 1974.  It was produced by Lighthouse Productions, released by Pyramid Films in the United States, and won the Academy Award for Best Animated Short Film. 

==Plot summary== pull back shot, the camera then shows the viewer that the words are part of a sign that reads:

 
{| class="wikitable" border="0" style="text-align:center"
| Aug 15–Oct 3 - One Woman Show - Celia Crazelsnuk Oct 3–March 19 - Usual Crap - Closed Mondays
|}
 
 abstract sculpture is transformed, imitating the man behind his back before returning to its original shape without his noticing.

The drunk sees a picture of colorful musical notes that form a circle around a jagged shape resembling a red staircase. The picture moves to upbeat music for a moment and then returns to normal. Doubting his own eyes ("HEY! What the HELL?!!" he mutters), the man looks again. The music begins to play, and a miniature man resembling the drunk skips down the stairs, stands on one of the circling musical notes, rides it for a while, then continues down the stairs to the bottom.  The entire picture then becomes two abstract colored clay blobs that pulsate to the music. Suddenly the music stops and the drunk is back in the gallery, where he makes a critical comment ("What was that guy thinking of?!") and staggers away.

The man sees a sculpture of a computer-like device with large lips and gauges for eyes. He laughs at the sculpture and flips a lever that starts it. The sculpture begins speaking rapidly and says it is a "replica of the model 505 type P electro brain," claims to be far superior to its creators, and carries out its "infinite mutation" program. The computer begins to stutter as it tries to say it has a short circuit and an error before changing into a talking globe, a talking apple, a colorful bust of Albert Einstein, a television, and finally a hand with smaller hands at the end of each of the fingers before entirely melting down into a shapeless mass of clay.

The drunk male walks away after making another comment ("Blabbermouth computer!"), and is then frightened by some jungle animals reaching through a glassless window pane that turns out to be a harmless painting. Distressed, the drunk male walks on, where he sees a painting of a medieval woman kneeling on a castle floor. She holds a brush in her hand and a bucket is beside her. The drunk male asks her, "Hey… wassa matter?" She weeps and tells him, "Oh, if my master could have seen more of the beauty in life… Here I am on my knees, doomed to wear this sorrowful face, scrubbing this cold stone floor forever and forever and forever…" Then the painting returns to normal.

The drunk male sees the still-open door and runs to get out of the gallery, but is stopped just before he gets there. He is a piece of statuary, and returns to his inanimate state before reaching the door.

==Release==
The short film was included in the 1975 movie, Fantastic Animation Festival. 

The short film was also included in a 1985 video compilation called, Academy Award Winners - Animated Short Films, released by Vestron Video, albeit altered somewhat; the sign outside the museum at the beginning leaves out the words "Usual Crap" and the drunk, after seeing the first painting briefly come to life, mutters, "What the...?!"

Closed Mondays featured in the 2012 program of the Romanian international animation film festival, "animest", as part of the "Classics" category. 

==Credits==
*Voices: Todd Oleson & Holly Johnson
*Music: Bill Scream
*Creators: Will Vinton & Bob Gardiner

==References==
 

==External links==
*  
*   for the British Film Institute
*  
*  

 

 
 
 
 
 
 
 
 
 
 