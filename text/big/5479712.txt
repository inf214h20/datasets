Tarzan and the Mermaids
{{Infobox film
| name           = Tarzan and the Mermaids
| image          = Tarzan and the Mermaids (movie poster).jpg
| caption        =
| director       = Robert Florey
| producer       = Sol Lesser
| writer         = Edgar Rice Burroughs (characters) Carroll Young (screenplay)
| based on       =   Brenda Joyce Andrea Palma
| music          =Dimitri Tiomkin
| cinematography = Jack Draper Gabriel Figueroa
| editing        = Merrill G. White
| distributor    = RKO Radio Pictures Inc.
| released       =    |ref2= }}
| runtime        = 68 min.
| language       = English
| budget         =
}}

Tarzan and the Mermaids is a 1948 adventure film based on the Tarzan character created by Edgar Rice Burroughs. Directed by Robert Florey, it was the last of twelve Tarzan movies to star Johnny Weissmuller in the title role. It was also the first Tarzan film since 1939 not to feature the character Boy, adopted son of Tarzan and Jane. (Boy was described in the film as being away at school, and the character never returned to the series.)

==Synopsis== Andrea Palma in a scene of the film.]] Brenda Joyce) help a native girl (Linda Christian) who has fled the village to avoid a forced marriage to a supposed local god. George Zucco portrays Palanth, the corrupt high priest attempting to force the girl into marriage, and Fernando Wagner plays a con man impersonating the god Balu.

==Cast==
* Johnny Weissmuller as Tarzan   Brenda Joyce Jane  
* George Zucco as Palanth, the High Priest   Andrea Palma as Luana, Maras Mother  
* Fernando Wagner as Varga, Pearl Trader   Edward Ashley as Commissioner  
* John Laurenz as Benji  
* Gustavo Rojo as Tiko, Maras Fiancé  
* Matthew Boulton as British Inspector-General  
* Linda Christian as Mara

==Production== Herman Brixs The New Adventures of Tarzan. 

The film is noted for its cinematography by Gabriel Figueroa, exotic Mexican scenery and coastal locales, a Dimitri Tiomkin score and much group singing. 

==Deaths==
Two members of the film crew were killed during production.    One Mexican crew member was crushed by a motorboat whilst Angel Garcia, a stunt diver who doubled for Tarzans high dive, was killed after he survived the dive but was swept by the surf into the rocks of the cliffs.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 