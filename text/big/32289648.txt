Biktima
{{Infobox film
| name = Biktima RD Alba
| producer = R.D. Alba Philip Anthony Disi Alba Ray Cuerdo Cesar Montano
| writer = Disi Alba RD Alba
| screenplay = Disi Alba RD Alba Ray Cuerdo
| starring = Cesar Montano Angel Aquino
| cinematography = Rain Yamson II
| editing = R.D. Alba
| music = Lutgardo Labad
| studio = Alba Productions CM Films
| distributor = Star Cinema
| released =  
| runtime = 90 minutes
| country = Philippines
| language =  
| budget =
}}
Biktima is a 2012 Filipino drama film starring Cesar Montano and Angel Aquino. The film is directed by R.D. Alba and produced under Alba Productions and CM Films. It is released by Star Cinema on September 19, 2012.         

==Plot==
Alice de la Cruz (Angel Aquino) is a TV field reporter at KVTV. But when she gets the dangerous assignment to cover kidnappings in the island of Kamandao, she takes the opportunity for this might be her big break. Her husband Mark(Cesar Montano) does not agree with this assignment that she took. He warns her of how risky it is to go to the island. Alice takes the chance anyway and that was the last time Mark has seen Alice.

Mark blames himself for not stopping Alice from going to the island. The movie unfolds as Mark discovers what has really happened to Alice in Kamandao.

==Cast==
* Cesar Montano as Mark de la Cruz
* Angel Aquino as Alice de la Cruz
* Mercedes Cabral
* Ricky Davao
* Sunshine Cruz
* Philip Anthony
* Rommel Montano
* Disi Alba
* JM Ibanez
* Ardy Batoy
* Scarlet
* Lloyd Samartino
* Iris Garsuta

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 