Thekkan Kattu
{{Infobox film
| name = Thekkan Kattu
| image =
| caption =
| director = J. Sasikumar
| producer = RS Prabhu
| writer = Muttathu Varkey Thoppil Bhasi (dialogues)
| screenplay = Thoppil Bhasi Madhu Sharada Sharada Sukumari KPAC Lalitha
| music = A. T. Ummer
| cinematography = TN Krishnankutty Nair
| editing = G Venkittaraman
| studio = Sree Rajesh Films
| distributor = Sree Rajesh Films
| released =  
| country = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by RS Prabhu. The film stars Madhu (actor)|Madhu, Sharada (actress)|Sharada, Sukumari and KPAC Lalitha in lead roles. The film had musical score by A. T. Ummer.  
  

==Cast==
   Madhu as Babu  Sharada as Sosamma 
*Sukumari as Shyamala Khanna 
*KPAC Lalitha as Gowri 
*Adoor Bhasi as Gopalan 
*Jose Prakash as Annakkuttys Husband 
*Sankaradi as Gowris Father 
*Paul Vengola 
*Adoor Bhavani as Babus Mother 
*Kaduvakulam Antony as Elikkuttys Husband 
*Kottarakkara Sreedharan Nair as Chacko Vakeel  Kunchan  Meena as Sosammas Mother 
*Rajakokila as Shyamala Khannas Daughter 
*S. P. Pillai as Babus Father  Sujatha as Annakkutty  Master Raghu
 

==Soundtrack==
The music was composed by A. T. Ummer. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || En Nottam Kaanaan || LR Eeswari || P Bhaskaran || 
|- 
| 2 || Neela Meghangal || Jayachandran || P Bhaskaran || 
|- 
| 3 || Neeye Sharanam || Adoor Bhasi, Chorus || P Bhaskaran || 
|-  Susheela || P Bhaskaran || 
|- 
| 5 || Priyamullavale || KP Brahmanandan || P Bhaskaran || 
|-  Janaki || P Bhaskaran || 
|- 
| 7 || Yerusaleminte Nandini || K. J. Yesudas || Bharanikkavu Sivakumar || 
|}

==References==
 

==External links==
*  

 
 
 


 