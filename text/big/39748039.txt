Mamsell Nitouche (1932 film)
{{Infobox film
| name           = Mamsell Nitouche 
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Lamac
| producer       = 
| writer         = Henri Meilhac (libretto) Albert Millaud (libretto)   Richard Genée   Hans H. Zerlett
| narrator       =  Hans Junkermann
| music          = Hervé (composer)|Hervé
| editing        = 
| cinematography = Otto Heller
| studio         = Ondra-Lamac-Film   Vandor Film  UFA
| released       =  23 February 1932
| runtime        = 76 minutes
| country        = France   Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German operetta Mamselle Nitouche which had been directed by Marc Allégret. Both films are based on the 1883 operetta Mamzelle Nitouche. The films art direction was by Heinz Fenchel.

==Cast==
* Anny Ondra as Mamselle Nitouche 
* Georg Alexander as Der Leutnant 
* Oskar Karlweis as Der Organist  Hans Junkermann as Der Major 
* Julia Serda as Die Oberin, Madame Audrin 
* Yvonne Albinus as Corinne 
* Karl Forest as Theaterdirektor

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 