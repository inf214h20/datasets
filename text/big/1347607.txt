Deadlier Than the Male
 
 
 
{{Infobox film
| name           = Deadlier Than the Male
| image          = Deadlier Than the Male - UK film poster.jpg
| image_size     = 
| caption        = UK cinema poster
| director       = Ralph Thomas
| producer       = Betty E. Box Sydney Box
| writer         = Liz Charles-Williams David D. Osborn Jimmy Sangster
| narrator       =  Richard Johnson Elke Sommer Sylva Koscina Nigel Green title song performed by The Walker Brothers
| cinematography = Ernest Steward
| editing        = Alfred Roome
| distributor    = J. Arthur Rank Film Distributors
| released       =  
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} crime and mystery film  featuring the character of Bulldog Drummond. It is one of the many take-offs of James Bond produced during the 1960s, but is based on an established detective fiction hero.
 Richard Johnson Terence Youngs assassins (Elke Sommer and Sylva Koscina) who kill for sport and profit. Drummonds American nephew, Robert Drummond (Steve Carlson, then a Universal Pictures contract star), becomes involved in the intrigue when he comes to visit. 
 The Female of the Species," which includes the line, "The female of the species must be deadlier than the male", and also refers to Sappers earlier Drummond book The Female of the Species. The working title of the film was The Female of the Species.  Filmed in Technicolor and Techniscope, portions of the film were shot in Lerici, La Spezia, Liguria, Italy.

The film was followed by a sequel, Some Girls Do, in 1969.

==Plot==
 Hugh "Bulldog" Drummond to investigate. Attempts on his own life lead him to believe two lovely females are "hit men" for an international crime syndicate. 

Drummond pursues them from London to the Mediterranean, but finds himself trapped in a deadly game of cat and mouse with a diabolical mastermind. It is revealed that Carl Petersen (Nigel Green), thought to have been a victim of assassination himself, is the evil genius behind the assassinations. Using two female assassins, Irma (Elke Sommer) and Penelope (Sylva Koscina), Petersen kills anyone who comes too close to revealing his true identity or who threatens his profit margin.  He captures and imprisons Drummond.

The finale involves Petersens attempt to kill King Fedra, who refuses to sell his oil fields. Grace, Petersens disillusioned mistress, is overheard confiding her dissatisfaction to Drummond. Petersen repays her by secreting a plastic explosive onto her and sending her to the Kings yacht. While playing chess against Petersen with giant motorized pieces, Drummond attempts to escape from Petersens castle. He kills Petersens bodyguard Chang (Milton Reid) and presumably kills Petersen himself by dropping him into a pit.

Drummond boards the Kings yacht to search Grace for the explosive. He strips her naked during the search; one of Fedras guards is distracted by her nudity, allowing Irma and Penelope to escape. Irma reveals that the bomb was in Graces Hairpin (fashion)|hairclip. Penelope is aghast; having envied Graces Chignon (hairstyle)|chignon, she stole the hairclip and is wearing it during their escape. The two assassins are killed instantly when the hairclip explodes, destroying their motorboat.

==Cast== Richard Johnson as Hugh Bulldog Drummond
* Elke Sommer as Irma Eckman
* Sylva Koscina as Penelope
* Nigel Green as Carl Petersen
* Suzanna Leigh as Grace
* Steve Carlson as Robert Drummond
* Virginia North as Brenda
* Justine Lord as Miss Peggy Ashenden
* Leonard Rossiter as Henry Bridgenorth
* Laurence Naismith as Sir John Bledlow
* Zia Mohyeddin as King Fedra
* Lee Montague as Boxer
* Milton Reid as Chang
* Yasuko Nagazumi as Mitsouko
* George Pastell as Carloggio John Stone as David Wyngarde
* William Mervyn as Chairman of the Phoenician Board

==Production== pilot for a television series.  It was filmed in three months with Thomas admitting he did it for "greed".
 X rating. 

==References==
 

== External links ==
*  

 
 
 

 

 
 
 
   
   
 
 
 
 
 