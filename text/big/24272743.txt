The Hunley
{{Infobox film
| name           = The Hunley
| image          = Poster of the movie The Hunley.jpg
| caption        = John Gray
| producer       = Mitch Engel
| writer         = John Gray   John Fasano
| starring       =
| music          = Randy Edelman John Thomas
| editing        = Maryann Brandon
| distributor    = Turner Network Television
| released       =  
| runtime        = 94 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}}
 John Gray and starring Armand Assante, Donald Sutherland, Alex Jennings, Michael Dolan and Christopher Bauer. The film is based on the true story of the H. L. Hunley (submarine)|H. L. Hunley submarine and the Action of 17 February 1864.

==Plot==
Horace Lawson Hunley|H. L. Hunley takes his ship, the H.L. Hunley (submarine)|H.L. Hunley, out in the Charleston, South Carolina  harbor and it sinks with all hands. As the blockade still needs to be broken, Brigadier General (CSA)|Brig. Gen. P. G. T. Beauregard has the ship raised and puts George E. Dixon in charge. He starts looking for a crew and after some difficulty, finally finds enough volunteers to man it. They practice cranking the propeller. The crew don’t all get along with each other. Dixon flashes back to the Battle of Shiloh, where a gold coin given to him by his wife (who was later killed in a steamboat explosion caused by a drifting mine), deflected a bullet and saved his life.  They take the ship down and sit on the bottom to see how long they can stay down and almost get stuck. The Union navy is warned about the sub. The crew votes that if after an attack they are stuck on the bottom, they will open the valves, flooding the ship rather than suffocate. They go out to attack the U.S.S. Wabash, but the attack fails. Following the warning the ship has draped metal chain netting over the side. Also the rope which was attached to the ‘torpedo’ they were to release under the ship gets loose and becomes entangled in the propeller. It has to be cut loose while sailors on the Wabash shoot at the Hunley.  Beauregard proposes putting the torpedo at the end of a long spar. The   is ordered to change its position in the harbor and always be ready to steam, meaning it can’t hang metal netting over the side. The second in command Lt. Alexander is ordered to Mobile, Alabama|Mobile, Alabama and a young soldier who had been volunteering to join the crew is allowed to do so.   On February 17, 1864, the C.S.S. H. L. Hunley sails out and attacks the U.S.S. Housatonic. The torpedo is rammed into the side of the ship. It blows up and the Housatonic is the first ship ever sunk by a sub.  A bullet from the ship breaks a window in the conning tower and wounds Dixon. The explosion opens the seams on the Hunley and it takes on water. It settles to the bottom and they can’t release the ballast or pump the ship. As agreed the crew opens the valves and the ship floods, killing the entire crew.

==Cast==
*Armand Assante as Lt. George E. Dixon
*Donald Sutherland as Brigadier General (CSA)|Brig. Gen. P. G. T. Beauregard William Alexander
*Christopher Bauer as Simkins
*Gerry Becker as Capt. Pickering
*Michael Dolan as Becker
*Sebastian Roche as Collins
*Michael Stuhlbarg as Wicks
*Jeff Mandon as Miller
*Frank Vogt as Ronald White
*Jack Baun as Ridgeway
*Kevin Robertson as Carlson
*Caprice Benedetti as Dixon’s wife

==External links==
*  

 

 
 
 
 
 
 
 
 
 