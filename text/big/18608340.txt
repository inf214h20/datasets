Niceland (Population. 1.000.002)
{{Infobox film
| name           = Niceland
| image          = 
| caption        = 
| italic title   = no
| director       = Friðrik Þór Friðriksson
| producer       = Thor S Sigurjonsson Skuli Fr Malmquist   Kate Barry   Mike Downey   Sam Taylor   Zorana Piggott   Helmut Weber
| writer         = Huldar Breiðfjörð Gary Lewis Peter Capaldi Kerry Fox
| music          = Mugison
| cinematography = Morten Søborg
| editing        = Sigvaldi J. Kárason Anders Refn
| distributor    = Zik Zak Kvikmyndir   Tradewind Pictures   Nimbus Film Productions   Film and Music Entertainment
| released       = 
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
  
Niceland (Population. 1.000.002) is an Icelandic drama directed by Friðrik Þór Friðriksson

==Cast==
* Martin Compston as Jed
* Gary Lewis as Max
* Peter Capaldi as John
* Kerry Fox as Mary
* Shauna Macdonald as Sandra
* Guðrún María Bjarnadóttir as Chloe (as Gudrun Maria Bjarnadottir)
* Guðrún Gísladóttir as Ruth (as Gudrun Gisladottir)
* Timmy Lang as Alex
* Ásta S. Ólafsdóttir as Mia
* Hugrún Þorfinnsdóttir as Diana
* Gísli Örn Garðarsson as Ethan
* Páll Sigþór Pálsson as Newsreader
* Þorsteinn J. Vilhjálmsson as News Reporter
* Andri Freyr Hilmarsson as Factory Worker
* Halldór Halldórsson as Factory Worker (as Halldór Steinn Halldórsson)
* Helga Pálína Sigurðardóttir as Factory Worker
* Helgi Hrafn Pálsson as Factory Worker
* Hlynur Steinarsson as Factory Worker
* Margrét Eiríksdóttir as Factory Worker
* Sigurþór Dan as Factory Worker
* Steve Hudson as Ikea Employee
* Steinunn Þorvaldsdóttir as Factory Worker
* Jón Ingi Hákonarson as Bus Driver
* Sigurður Skúlason as Middle Aged Driver
* Kristinn Vilbergsson as Young Driver
* Svana Friðriksdóttir as Woman in Flower Shop
* Sigríður Jóna Þórisdóttir as Woman in Bus
* Benóný Ægisson as Couple in Electric Store
* Júlía Hannam as Couple in Electric Store
* Birgitta Birgisdóttir as Bakery Assistant
* Thor Sigurjonsson as Couple in Bakery 
* Line Lind as Couple in Bakery
* Kristin Ingveldur Ingólfsdóttir as Old Woman in Bakery
* Erlendur Eiríksson as Radio Hoste

==Release==
Niceland  was released on 5 July 2004.

==Awards==
Edda Awards, Iceland  
* In 2004, Niceland won the ‘Edda Award’ for Screenplay of the Year, for Huldar Breiðfjörð.   
* In 2004 they were nominated for a ‘Edda Award’ for Best Film.

Karlovy Vary International Film Festival    
* In 2004, Niceland was nominated for a ‘Crystal Globe’ award for Friðrik Þór Friðriksson

==References==
 

==External links==
* 

 

 
 
 
 


 