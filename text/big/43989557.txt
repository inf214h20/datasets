Manthri Kochamma
{{Infobox film
| name = Manthri Kochamma
| image =
| image_size =
| caption =
| director = Rajan Sithara
| producer =
| writer = AR Mukesh
| screenplay = AR Mukesh Kalpana Mala Kanaka
| music = Mohan Sithara
| cinematography = Utpal V Nayanar
| editing = K Rajagopal
| studio = Family Films
| distributor = Family Films
| released =  
| country = India Malayalam
}}
  1998 Cinema Indian Malayalam Malayalam film, Kanaka in lead roles. The film had musical score by Mohan Sithara.   
 
==Cast==
  
*Indrans  Kalpana 
*Mala Aravindan  Kanaka 
*Prem Kumar 
*Jagathy Sreekumar 
*Salim Kumar  Vijayaraghavan 
*Shammi Thilakan 
 

==Soundtrack==
The music was composed by Mohan Sithara. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Anthimukil || K. J. Yesudas || S Ramesan Nair || 
|- 
| 2 || Devalokamaano || Daleema, Manoj Krishnan || S Ramesan Nair || 
|- 
| 3 || Hridayamuraliyude raagam || KS Chithra || S Ramesan Nair || 
|- 
| 4 || Koodevide Koodevide Oh Mridule || K. J. Yesudas || S Ramesan Nair || 
|- 
| 5 || Koodevide Koodevide Oh Mridule   || Daleema || S Ramesan Nair || 
|- 
| 6 || Onnaamthumbi || K. J. Yesudas || S Ramesan Nair || 
|- 
| 7 || Onnaamthumbi || K. J. Yesudas, Daleema || S Ramesan Nair || 
|- 
| 8 || Poo Virinja Pole || M. G. Sreekumar || S Ramesan Nair || 
|- 
| 9 || Raanthal Velichathil || M. G. Sreekumar || S Ramesan Nair || 
|}

==References==
 

==External links==
*  

 
 
 


 