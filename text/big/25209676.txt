Rikshavodu
{{Infobox film
| name           = Rikshavodu
| image          =
| image_size     =
| caption        =
| director       =Kodi Ramakrishna
| producer       =Kranthi Kumar
| writer         =
| narrator       = Manorama
| music          =  Raj, Koti
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 14 December 1995
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Rikshavodu is a 1995 Telugu film starring Chiranjeevi directed by Kodi Ramakrishna and produced by Kranthi Kumar. Chiranjeevi played a dual role in this film. It was dubbed in Hindi as Devaa The Power Man. It met with negative reviews and was declared a flop at the box office. It was one of the series of box office flops Chiranjeevi had.

==Plot==
Chiranjeevi has enacted dual role in this film. The film opens with scene where Chiranjeevi come to city in search of livelihood in the city with his granny Manorama. He is befriended by Brahmanandam and taken to Soundarya who gives rickshaws for rent. One day while driving passengers on his rickshaw, he confronts with Nagma who hit it by driving her car arrogantly. He files a suits against her in the court but fails due to the influence of Nagmas wealthy father Paresh Rawal. After some scenes Paresh Rawal forces his daughter to marry Chiranjeevi to build up his political career as Chiru has good mass following in his area. Manorama sees Paresh and then reveals Chiru and us about his past. His father (also Chiranjeevi) a good human being and also a village head. Envied by the good name he has in the village Paresh Rawal traps his sister and sees that she suicides and also blames his wife Jayasudha that she is illicit. He kills Chiranjeevi in a village fight and that blame is taken by Jayasudha and she is in jail and all the villagers think that she is actual cause of all this. Now the son takes revenge on the villain and opens the truth before the villagers. Also Nagma transforms to be his lovable wife after knowing her fathers deeds.

==Cast==
* Chiranjeevi as Raju / Rajus Father Village head
* Jayasudha as Rajus Mother Manorama as Bamma
* Brahmanandam as Rajus Rickshaw Friend
* Nagma as Rani
* Soundarya as Narsakka
* Paresh Rawal as G.K.Rao
* Amanchi Venkata Subrahmanyam|A.V.S as G.K.Raos Assistant
* Gundu Hanumantha Rao

==Music==
Music scored by Raj and Koti Lyrics penned by Veturi Sundararama Murthy and Bhuvana chandra

==External links==
*  

 
 
 

 