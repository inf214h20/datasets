Long Range Patrol (film)
 
{{Infobox film
| name           = Long Range Patrol
| image          =
| caption        =
| director       = Harri J. Rantala
| producer       = Harri J. Rantala
| writer         = Harri J. Rantala
| starring       = Eerik Kantokoski Ali Ahovaara Hannu Rantala Kalevi Haapoja Eeva Putro   Reeta Annala  Kauko Salo Kristiina Karhu Jaakko Seppä
| music          =
| cinematography = Jari Koskinen
| editing        = Harri J. Rantala
| studio         = Nurmo-Filmi
| distributor    = Nurmo-Filmi
| released       =  
| runtime        = 60 minutes
| country        = Finland
| language       = Finnish
| budget         =
| gross          = 
}}
Long Range Patrol ( ) is a 2013 Finnish feature film directed by Harri J. Rantala and starring Eerik Kantokoski, Ali Ahovaara, Hannu Rantala, Kalevi Haapoja and Eeva Putro.

==Plot==
Long-Range Patrol is a story about the Squad Peltoniemi Patrol trip to the enemies backside in spring 1943.
The capturing of a Russian female soldier changes the whole task from a routine mission to a battle for survival.

==Cast==
* Eerik Kantokoski as Captain Erkki Peltoniemi
* Ali Ahovaara as Corporal Eero Ilkka
* Hannu Rantala as Private Tuomas Loukasmäki 
* Kalevi Haapoja as Colonel Juho Loukasmäki
* Eeva Putro as Russian female soldier 
* Reeta Annala as Enni Peltoniemi
* Kauko Salo as Sameli Peltoniemi
* Kristiina Karhu as Laura Peltoniemi
* Jaakko Seppä as Lieutenant Itäniemi

==External links==
*  
*  

 
 
 
 
 

 