Blum Affair
{{Infobox film
| name           = Blum Affair
| image          = Blumaffair.jpg
| image size     =
| caption        =
| director       = Erich Engel
| producer       = Herbert Uhlich
| writer         = Robert A. Stemmle
| starring       = Hans Christian Blech
| music          = 
| cinematography = Karl Plintzner
| editing        =
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}}
 East German drama film directed by Erich Engel. It was released in List of East German films|1948. 

==Cast==
* Hans Christian Blech as Karlheinz Gabler
* Ernst Waldow as Kriminalkommissar Schwerdtfeger
* Paul Bildt as Untersuchungsrichter Konrat
* Karin Evans as Sabine Blum
* Helmuth Rudolph as Wilschinsky - Regierungspräsident
* Alfred Schieske as Kriminalkommissar Otto Bonte
* Gisela Trowe as Christina Burman
* Kurt Ehrhardt as Dr. Jakob Blum
* Gerhard Bienert as Karl Bremer
* Herbert Hübner as Langerichtsdirektor Hecht Friedrich Maurer as Rechtsanwalt Dr. Gerhard Wormser
* Klaus Becker as Hans Fischer - Gutsvolontär
* Arno Paulsen as Wilhelm Platzer
* Hilde Adolphi as Alma - das süße Mädchen

==Reception==
The film sold more than 4,330,000 tickets. 

==References==
  

==External links==
* 

 
 
 
 
 
 
 