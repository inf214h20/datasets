Nee Sneham
{{Infobox film
| name           = Nee Sneham
| image          =
| image_size     =
| caption        =
| director       = Paruchuri Murali
| producer       = M S Raju
| writer         = M S Raju
| screenplay     = M S Raju
| narrator       =
| starring       = Uday Kiran Aarti Agarwal Jatin Grewal K Vishwanath
| music          = R P Patnaik
| cinematography = S. Gopal Reddy
| editing        = Krishna Reddy
| distributor    =
| released       = November 1, 2002
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
Nee Sneham ( ) is a 2002 film directed by Paruchuri Murali and produced by M S Raju. The film stars Uday Kiran, Aarti Agarwal and Jatin Grewal. The film was a moderate hit and well received by family audience. The screenplay and music tracks of the movie are highlights and stud for the movie success. The music given by R P Patnaik was critical for the movie.It was remade in Bengali as Premi.Uday Kiran nominate second time Filmfare Best Actor Award (Telugu) for his excellent natural & emotional performance.

==Plot==
The film opens with Madhav (Late Uday Kiran) and Sreenu (Jatin Grewal) playing a football cup final in Kolkata. Madhav and Sreenu are best friends, football players, and neighbours. Subsequently, the team wins the cup.

A mishap happens when Sreenu tries to save Madhav from an accident. Madhav escapes with minor injuries, but Sreenu loses his leg. This puts an end to his football career. Sreenu, recovering from the incident, convinces Madhav not to tell anyone about the cause of Sreenus accident. He does this to save the two families from drifting apart. Remembering his sacrifice, Madhav works hard and becomes the captain of the team, a post once held by Sreenu.

Once, while visiting Goa for a match, Madhav sees Amrutha (Arti Agarwal) and falls in love with her. When Amrutha returns to her house, she finds her grandparents fixing her marriage with an Non-resident Indian and person of Indian origin|NRI. The marriage breaks apart when the father of the groom gets to know about Madhavs love for Amrutha. Her heartbroken grandfather dies a few days later, and Amrutha holds Madhav responsible for all these incidents and starts hating him.

Madhav tries to change her life by anonymously sending her gifts and money during her hardships. Amrutha desperately tries to find the person who was sending these to her. A few days later, during the festival of Holi, Amrutha mistakenly thinks Sreenu to be the sender of all the gifts and proposes him for marriage. Sreenu, not knowing about Madhavs love, agrees to the proposal. Madhav rushes to Sreenus house, where he realises that the bride is no one else but Amrutha.

Madhav sacrifices his love for his best friends happiness. He tries to burn the photos of him and Amrutha in Goa, but he is seen by his father. His father gets to know the truth but Madhav convinces him not to tell anyone.

Madhav decides to leave the town so that the marriage goes on smoothly. He fakes a letter asking him to report to Delhi for  national football team selection. Sreenu asks him to leave immediately and to fulfill both of their dreams. Madhav, instead of going to Delhi, flies to Mumbai.

Meanwhile, Sreenus father learns about the photograph and Amrutha is questioned about their relationship. Amrutha realises that it was Madhav, and not Sreenu, who helped her. Sreenu and Amrutha leave for Mumbai. The film ends with Amrutha and Madhav getting together and Sreenu walking away with satisfaction.

==Cast==
* Uday Kiran as Madhav
* Aarti Agarwal as Amrutha
* Jatin Grewal as Seenu
* K Vishwanath as Amruthas grandfather
* Giri Babu as Madhavs father
* Kaushal Ali
* Lakshmipati
* Paruchuri Venkateswara Rao
* Sangeeta
* Sudha
* Chitram Seenu
* Dharmavarapu Subramanyam

==External links==
*  

 
 
 

 