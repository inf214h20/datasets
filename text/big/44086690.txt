Ashwaradham
{{Infobox film 
| name           = Ashwaradham
| image          =
| caption        =
| director       = IV Sasi
| producer       = Pavamani
| writer         = Prabhakar Puthur T. Damodaran (dialogues)
| screenplay     = VT Nandakumar
| starring       = Srividya Raveendran Prameela Balan K Nair Shyam
| cinematography = Jayanan Vincent
| editing        = K Narayanan
| studio         = Prathap Chithra
| distributor    = Prathap Chithra
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by IV Sasi and produced by Pavamani. The film stars Srividya, Raveendran, Prameela and Balan K Nair in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Srividya as Jayanthy Shankar
*Raveendran as Ramootti
*Prameela as Sreedevi Kunjamma
*Balan K Nair as Veeraraghan
*Janardanan as Rajagopal
*K. P. Ummer as Shankar
*Kuthiravattam Pappu as Velayyan
*Kuttyedathi Vilasini
*Nithya as Shanty Ravikumar as Gopi

==Soundtrack== Shyam and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ragasangamam || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 2 || Thulaavarsha Melam Thudippattin Thaalam || K. J. Yesudas, S Janaki || Mankombu Gopalakrishnan || 
|-
| 3 || Ushamalarikal || S Janaki, Chorus || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 