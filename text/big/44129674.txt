Ithum Oru Jeevitham
{{Infobox film 
| name           = Ithum Oru Jeevitham
| image          =
| caption        =
| director       = Veliyam Chandran
| producer       = Udaya
| writer         = Veliyam Chandran
| screenplay     = Veliyam Chandran Kalpana Sukumaran
| music          = R Somasekharan
| cinematography = Hemachandran
| editing        = Hariharaputhran
| studio         = Abhilash Bhanu Films
| distributor    = Abhilash Bhanu Films
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, Kalpana and Sukumaran in lead roles. The film had musical score by R Somasekharan.   

==Cast==
*Jagathy Sreekumar
*Thikkurissi Sukumaran Nair Kalpana
*Sukumaran
*Kanakadurga
*Kottarakkara Sreedharan Nair
*Sharmila

==Soundtrack==
The music was composed by R Somasekharan and lyrics was written by Vellanad Narayanan and Konniyoor Bhas. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maaranicheppile || S Janaki, Somasekharan || Vellanad Narayanan || 
|-
| 2 || Prakrithee Prabhaamayi || K. J. Yesudas || Konniyoor Bhas || 
|}

==References==
 

==External links==
*  

 
 
 

 