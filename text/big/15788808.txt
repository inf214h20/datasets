The Orphan of Anyang
{{Infobox film
| name           = The Orphan of Anyang
| image          = Orphan_of_Anyang.jpg
| caption        =  Wang Chao Fang Li
| writer         = Wang Chao
| starring       = Sun Guilin Zhu Jie Yue Senyi
| music          =  Zhang Xi
| editing        = Wang Chao Wang Gang
| distributor    = Onoma 
| released       =  
| runtime        = 82 minutes
| country        = China
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 安阳的孤儿
| fanti          = 安陽的孤儿
| pinyin         = Ānyáng de gūér}}
}} Chinese film Wang Chao. Day and Luxury Car Fang Li. International distribution was by the French company Onoma. 

The Orphan of Anyang tells the story of a recently unemployed factory worker in the city of Anyang in Henan province, who comes across an abandoned child. Discovering that the child belongs to a local prostitute and mobster, the poor worker agrees to take care of the baby. When the mobster discovers that he is dying of cancer, however, he attempts to take the child back, now his only heir.

The film is marked by its use of static cameras and a naturalistic use of lighting and sound, prompting one critic to note its similarity to the films of Italian neorealism and the Dogma 95 manifesto.   

== Casting ==
The film consisted primarily of non-professional actors with most coming locally from Anyang itself. Director Wang Chao took four separate trips to Anyang in preparation for the making of the film, three of which were devoted to the purpose of casting the leads.      Wang gathered ten locals to audition for the role of Yu Dagang, the tender but lonely and desperate factory worker who finds himself the caretaker of a baby.  The role would eventually go to Sun Guilin, who Wang chose in part because he possessed "calm, but also with some personality and a little dignity and hope." Zhu Jie, the actor chosen to play Boss Si-De, the dying triad gangster and the father of the titular orphan, was another local actor found in Anyang.

Of the three leads, only Yue Senyi, who plays the tough prostitute, Feng Yanli was hired from Beijing. Wang noted that she possessed "slight stubbornness" but a "strong spirit," such that she could play the part of a prostitute without "conveying misery."  Extras, meanwhile, also came primarily from Anyang. Most of the thugs that surround Si-de, for example, were local criminals. Wang noted that he could never find actors who could walk in groups the way that real criminals could. 

== Reception ==
The film was not given wide distribution, but nevertheless was widely praised by critics. During its premiere in New York City, as part of Museum of Modern Art|MOMAs New Directors/New Films Series, critic Elvis Mitchell gave high praise to Wangs debut work. Mitchell writes that the film "with its no-frills honesty, is an arresting achievement..." and when he asks himself if The Orphan of Anyang can be considered one of the best films of the year, he answers simply, "Quite possibly."  Generally critics mirrored this praise to one degree or another, one calling the film "deceptively simple film of great depth, maturity, sensitivity and vision,"  while another gave it more subdued praise, but noted that Wang would be "a new talent to monitor on the indie Mainland scene." 

The films artistic pretensions (the slow pace and the use of non-professional actors for example) did garner the film the scorn of a few critics, with one opining that The Orphan of Anyang suffered from the unintentional humor of bad acting, ultimately calling the films relatively short 82 minute runtime as nevertheless being like "a life sentence." 

Such reviews, however, were outnumbered by positive accounts, with the review database website Rotten Tomatoes giving the film a 75% Fresh rating out of nine published reviews as of early 2008. 

===Awards and nominations=== 2001 Chicago International Film Festival
** FIPRESCI Award  2001 Amiens International Film Festival NETPAC Award 2001 Entrevues Film Festival
** Grand Prix 2002 Tromsø International Film Festival
** Aurora Special Award 2002 Kerala International Film Festival
** Golden Crow Pheasant 2002 Buenos Aires International Festival of Independent Cinema
** ADF Cinematography Award - Special Mention, Xi Zhang
** SIGNIS Award 2002 Vancouver International Film Festival
** Dragons and Tigers Award - Special Citation 2002 Warsaw International Film Festival
** Special Citation

==References ==
 

== External links ==
*  
*  
*  
*   at the Chinese Movie Database

 

 
 
 
 
 
 
 
 
 