Pest from the West
 
{{Infobox film
| name           = Pest from the West
| image          =
| caption        =
| director       = Del Lord
| writer         = Clyde Bruckman Buster Keaton|
| starring       = Buster Keaton Lorna Gray Gino Corrado Richard Fiske Bud Jamison Forbes Murray Eddie Laughton Ned Glass
| cinematography = Henry Freulich Charles Nelson
| producer       = Jules White
| distributor    = Columbia Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
}}

Pest from the West is the first short subject starring American comedian Buster Keaton made for Columbia Pictures. Keaton made a total of ten films for the studio between 1939 and 1941.

==Synopsis==
Keaton is a millionaire vacationing in Mexico traveler who falls in love with a senorita (Lorna Gray) and sets out to win her.

==Production== The Invader (1935). Keatons Silent movie|silent-era writer Clyde Bruckman collaborated on the screenplay and it was directed by comedy veteran Del Lord. The supporting cast features Columbia regulars Lorna Gray, Gino Corrado, Richard Fiske, Bud Jamison, Eddie Laughton, and Ned Glass with the voices of short-subject stars Charley Chase and Curly Howard heard on the soundtrack.

Much of Pest from the West was filmed on location at Balboa Peninsula, Newport Beach, California|Balboa, California, USA (Keaton repeatedly falls off his boat, into Balboa Bay). The Mexican-village settings were adapted from sets used in Columbias 1937 feature film Lost Horizon.

==Reception==
Pest from the West was a huge hit in theaters, and earned rave reports from exhibitors. Keaton starred in nine more Columbia shorts, the last of which was Shes Oil Mine. Like Pest from the West, this borrowed content from an older Keaton feature, The Passionate Plumber.
 
==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 