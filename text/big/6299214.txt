Inteha (2003 film)
{{Infobox film
| name           =Inteha
| image          =Inteha_(2003_film).jpg
| director       =Vikram Bhatt
| producer       =Mukesh Bhatt Kumkum Saigal
| writer         =Mahesh Bhatt Girish Dhamija
| starring       =Ashmit Patel Vidya Malvade Nauheed Cyrusi
| music          =Anu Malik
| cinematography =Pravin Bhatt
| distributor    =
| released       =24 October 2003
| runtime        =
| country        =India
| language       =Hindi / Urdu
| budget         =
| awards         =
}}

Inteha ( : انتہا   film released in 24 October 2003. It was produced by Mukesh Bhatt and directed by Vikram Bhatt, and stars Ashmit Patel, Vidya Malvade and Nauheed Cyrusi. It is inspired by the Hollywood film Fear (1996 film)|Fear. It marked a debut film for Ashmit Patel.

== Plot ==
Nandini and Tina are stepsisters and after their fathers death Nandini takes care of Tina as her daughter, Tina falls in love with Ranbir but she has to prove her love for him to Nandini.

== Cast ==
  
*Vidya Malvade ... Nandini Saxena
*Ashmit Patel ... Ranbir Oberoi
*Nauheed Cyrusi ... Tina Saxena
*Avtar Gill ... Advocate Ranjit S. Thakur
*Anup Soni ... Rohit
*Anjaan Srivastav ... Mohanlal

==Soundtrack==
{{Infobox album  
| Name       = Inteha
| Type       = Soundtrack
| Artist     = Anu Malik
| Cover      = 
| Alt        = 
| Released   = 
| Recorded   = 2003
| Genre      = Soundtrack
| Length     = 
| Label      = T-Series
| Producer   = 
| Last album = Mujhe Kucch Kehna Hai (2001)
| This album = Inteha (2003)
| Next album = Main Hoon Na (2004)
}}

The soundtrack of Inteha is composed by Anu Malik. Lyrics written by Rahat Indori, Praveen Bhardwaj and Dev Kohli.

===Track listing===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s) !! Lyrics !! Length
|-
| 1 || Humsafar Chahiye || Udit Narayan & Alka Yagnik || Rahat Indori || 7:47
|- Shaan & Shreya Ghoshal || Rahat Indori || 7:34
|-
| 3 || Dhalne Lagi Hai Raat (Duet) || Sonu Nigam & Shreya Ghoshal || Rahat Indori || 6:47
|-
| 4 || Dhalne Lagi Hai Raat (Sad Version) || Sonu Nigam || Rahat Indori || 2:16
|-
| 5 || Ab Humse Akele Raha Jaaye || Sonu Nigam, Alka Yagnik || Praveen Bhardwaj || 8:09
|-
| 6 || Deewana Dil || Shreya Ghoshal || Dev Kohli || 7:41
|-
| 7 || Dhalne Lagi Hai Raat || Sonu Nigam || Rahat Indori || 4:54
|}

== Trivia ==
Ashmit Patel, the younger brother of actress Amisha Patel, making his Bollywood debut. Actress Vidya Malvade made her debut with this film.

== References ==
 
 

== External links ==
 
* 
 
 

 
 

 
 
 
 
 
 
 
 


 