Love and Anarchy
{{Infobox film
| name           = Love and Anarchy
| image          =  Filmdamoreedanarchia-LinaWertmller.jpg
| writer         = Lina Wertmüller
| starring       = Giancarlo Giannini Mariangela Melato Eros Pagni Pina Cei
| director       = Lina Wertmüller
| editing        = Franco Fraticelli
| producer       = Romano Cardarelli
| music          = Nino Rota Carlo Savina
| cinematography = Giuseppe Rotunno
| distributor    =
| released       = May 20, 1973
| runtime        = 120 min
| country        = Italy
| language       = Italian
| budget         =
}}

Love and Anarchy ( ) is a 1973 film directed by Lina Wertmüller and starring Giancarlo Giannini and Mariangela Melato. The story, set in Fascist Italy before the outbreak of World War II, centers on Gianninis character, an anarchist who stays in a brothel while preparing to assassinate Benito Mussolini.  Gianninis character falls in love with one of the women working in the brothel.  This film explores the depths of his emotions concerning love, his hate for fascism, and his fears of being killed while assassinating Mussolini.
 Best Actor.   

==Plot==
The film begins with Tunin (Giancarlo Giannini) learning that his friend, an anarchist who was plotting to kill Benito Mussolini, has been killed by Mussolinis fascist police in the countryside.  Tunin decides to take up the cause his friend died for.  The movie then shows Tunin entering a brothel in Rome and meeting Salomè (Mariangela Melato).  The two have a casual sexual encounter.  Salomè explains her reasons for helping in the assassination plot as her former lover was wrongfully beaten to death by the Mussolinis police in Milan.  The story continues as Salomè arranges for her, Tunin and Tripolina (Lina Polito), another prostitute at the brothel, to spend the day with Spatoletti, the head of Mussolinis police.  The four of them go to the countryside near Rome where the assassination will take place in several days time. Salomè keeps Spatoletti busy while Tunin scouts out the area and makes a plan.  Tunin takes an interest, however, in Tripolina and they fall in love.  Tunin convinces Tripolina to spend the next two days with him before the assassination as he fears they may be his last.  On the morning of the assassination, Tripolina is supposed to wake Tunin early.  She loves him and is scared he will die so she decides she will not wake him.  Tripolina and Salomè argue about this and what to do but in the end they decide to let him sleep.  Tunin wakes up and is furious at both of them.  He goes into a tirade that draws the attention of the police.  He starts a shootout with them and screams that he wants to kill Mussolini.  He is captured and beaten to death by the police.  The film ends the way it began showing the full title of the film "Stamattina alle 10, in via dei Fiori, nella nota casa di tolleranza..." This morning at 10, on Via dei Fiori (Flowers Street), in a noted brothel which is the headline of an unnamed newspaper.  The article goes on to say that Tunin (who is unnamed) was arrested and then committed suicide, in a statement of the fascist censorship characteristic of the time.

==Cast==
* Giancarlo Giannini - Antonio Soffiantini Tunin
* Mariangela Melato - Salomè
* Lina Polito - Tripolina
* Eros Pagni - Giacinto Spatoletti
* Pina Cei - Madame Aida
* Elena Fiore - Donna Carmela
* Giuliana Calandra
* Isa Bellini
* Isa Danieli - Prostitute
* Enrica Bonaccorti - Prostitute
* Anna Bonaiuto - Prostitute
* Anita Branzanti - Prostitute
* Maria Sciacca - Prostitute
* Anna Melato - Prostitute
* Gea Linchi - Prostitute
* Anna Stivala - Prostitute

==Production notes== Swept Away, and Seven Beauties.

==References==
 

== External links ==
*  
 

 
 
 
 
 
 
 
 
 
 
 
 