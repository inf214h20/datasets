Banty Raids
{{Infobox Hollywood cartoon|
| cartoon_name = Banty Raids
| series = Merrie Melodies (Foghorn Leghorn)
| director = Robert McKimson
| producer = David H. DePatie (uncredited)
| story_artist = Robert McKimson Nick Bennion
| animator = George Grandpré Keith Darling Ted Bonnicksen Warren Batchelder
| layout_artist = Robert Gribbroek
| background_artist = Robert Gribbroek
| voice_actor = Mel Blanc Stan Freberg (uncredited) Bill Lava
| distributor = Warner Bros.
| release_date = June 29, 1963
| color_process = Technicolor
| runtime = 6
| movie_language = English
}}

Banty Raids is a "Merrie Melodies" cartoon animated short starring Foghorn Leghorn and the Barnyard Dawg, and features the only appearance of the "Banty Rooster" character. Released June 29, 1963, the cartoon is directed by Robert McKimson. The voices were performed by Mel Blanc and Stan Freberg.

This cartoon marked the last "classic-era" cartoon starring Foghorn Leghorn and Barnyard Dawg. Foghorn would make a cameo appearance in False Hare in 1964, but his next appearance after that was in 1980s The Yolks on You.

==Plot== rock music. The beatnik, after regaining his senses (and shooting his guitar), sees the neighboring barnyard is full of hens and is immediately overcome with lust.

But to gain access to the barnyard, he needs to get past its superintendent, Foghorn Leghorn. The young rooster disguises himself as a baby and Foghorn takes the bait. Adopting him as his "son," Foghorn immediately shows the beatnik how to keep Barnyard Dawg in his place, using a rubber band contraption to punch the dog square in the head before tossing him in a garbage can.

The beatnik rooster constantly sneaks away to dance with the hens and kiss them. Foghorn eventually becomes suspicious and puts the rooster through a test to see what interests him. When he shows him a picture of a lady hen in an evening dress, the rooster goes wild and runs through the picture, confirming Foghorns suspicions ("Hah! Just like I thought; hes wacky over females!"). Barnyard Dawg also finds out and offers to aid the hip rooster. After the hen obsessed rooster agrees, the dog sends a toy tank to seek out Foghorn ("Uh oh. Looks like one of that silly dawgs booby traps."). After Foghorn lands in a thresher retooled for the sole purpose of dressing him in drag (he was launched towards it by a horse, the real target of the toy tank), he eventually ends up between the Dawg and the rooster.

The beatnik rooster goes wild at Foghorns new form and demands an impromptu marriage ceremony and Barnyard Dawg readily obliges, donning a preachers hat. Foghorn tries to protest ("but Im a rooster!", which the beatnik rooster didnt understand), the beatnik rooster however, is willing to accept her shortcomings: ("Dont let it bug ya maam, like, we cant all be perfect!").

==Succession==
 
{{succession box |
before= Mother was a Rooster | Foghorn Leghorn cartoons |
years= 1963 |
after= The Yolks on You |}}
 

==References==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==External links==
* 

 
 
 