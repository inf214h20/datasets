Les petites fugues
{{Infobox film
| name           = Les petites fugues
| image          = 
| caption        = 
| director       = Yves Yersin
| producer       = Donat Keusch
| writer         = Yves Yersin Claude Muret
| starring       = Michel Robin
| music          = Léon Francioli
| cinematography = Robert Alazraki
| editing        = Yves Yersin
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = Switzerland
| language       = Swiss
| budget         = 
}}
 Best Foreign Language Film at the 52nd Academy Awards, but was not accepted as a nominee. 

==Plot==
The old farm worker Pipe is old enough to retire. Even so he cannot imagine a life without work. So he keeps on doing his job  and wonders what to do with his additional financial means. Soon a small moped comes to mind. Thus motorised he starts to explore the world around his village. One day he gets overly confident and drives under the influence of alcohol. This costs him his drivers license. But before this incident he has become the owner of a camera. Now he turns into a diligent photographer. Craving for new picture angles he even books a helicopter flight. With each little adventure he cares less for his old job until he embraces his retirement.

==Cast==
* Michel Robin as Pipe
* Fabienne Barraud as Josiane
* Fred Personne as John
* Dore De Rosa as Luigi
* Mista Préchac as Rose
* Laurent Sandoz as Alain
* Nicole Vautier as Marianne
* Léo Maillard as Stephane
* Pierre Bovet as the postman
* Roland Amstutz as the consultant
* Maurice Buffat as the policeman
* Yvette Théraulaz as fireman
* Joseph Leiser as the overseer at the chocolate factory
* Gerald Battiaz as the motorcyclist
* Martine Simon as Bica

==See also==
* List of submissions to the 52nd Academy Awards for Best Foreign Language Film
* List of Swiss submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 