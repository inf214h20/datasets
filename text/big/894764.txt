Sweet November (2001 film)
{{Infobox film
| name = Sweet November
| image = SweetNovember2.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster Pat OConnor
| producer = Elliott Kastner Steven Reuther Deborah Aal Erwin Stoff
| writer = Paul Yurick Kurt Voelker
| starring = Keanu Reeves Charlize Theron
| music = Christopher Young
| cinematography = Edward Lachman
| editing = Anne V. Coates
| studio = Bel Air Entertainment
| distributor = Warner Bros.
| released =  
| runtime = 120 minutes  
| country = United States
| language = English
| budget = $40 million
| gross = $65,754,228
}} The Devils earlier version made in 1968 and written by Herman Raucher.

==Plot==
Nelson Moss meets Sara Deever, a woman very different from anyone he has met before. His ignorance leads to her failing her driving test. She beguiles him and continually asks him to spend a month with her on the promise that she will change his life for the better. On the first night of November, after Nelson is fired and dumped on the same day, she sleeps with him, and the next day Chaz, a close friend of Saras, arrives and identifies Nelson as  

Throughout November, the two experience happy times together and fall in love. Nelson examines his life and past, and befriends a fatherless child named Abner. Eventually, he realizes he is in love with Sara and asks her to marry him. It is revealed that Sara has terminal cancer, Non-Hodgkin lymphoma. Because she cannot bear to have Nelson experience her death, she asks him to leave. Sara tells Chaz that Nelson proposed to her. Chaz says that it wasnt the first time that a man had proposed, implying Sara has had numerous "months". Sara confirms this but claims it was the first time she had wanted to say yes. She decides she will not continue the relationship to protect Nelson from being hurt. Nelson complies, but then stages a surprise return during the Thanksgiving holiday, giving her gifts that remind her of their happy times.

They stay together for one more day; he posts November calendars all over her apartment walls, saying it can always be November for them. They make love, but the next morning, Nelson finds Sara is dressed. She asks him to leave, with all his calendars taken down. Nelson becomes confused and heartbroken. Sara asks Nelson to let her go so that he will always have happy memories of her and explains that this is how she needs to be remembered. She will return home to her family (whom she had been avoiding) and face her last days.  The movie ends with Sara blindfolding Nelson, giving him one last kiss, and then walking away. Nelson then takes off the blindfold in tears, and is later shown at a park they went to on one of their dates.

==Cast==
* Keanu Reeves as Nelson Moss
* Charlize Theron as Sara Deever
* Jason Isaacs as Chaz Watley
* Greg Germann as Vince Holland
* Lauren Graham as Angelica
* Liam Aiken as Abner
* Frank Langella as Edgar Price Ray Baker as Buddy Leach
* Michael Rosenbaum as Brandon/Brandy
* Robert Joy as Raeford Dunne
* Jason Kravits as Manny
* Tom Bullock as Al

==Music==
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Cellophane
| extra1 = Amanda Ghost
| lyrics1 = Amanda Ghost, Sacha Skarbek, Ian Dench, Lucas Burton
| length1 = 3:33
| title2 = Only Time (Original Version)
| extra2 = Enya
| lyrics2 = Enya, Roma Ryan
| length2 = 3:38
| title3 = Shame
| extra3 = BT
| lyrics3 = Brian Transeau
| length3 = 3:21
| title4 = Touched by an Angel
| extra4 = Stevie Nicks
| lyrics4 = Stevie Nicks
| length4 = 4:23
| title5 = The Consequences of Falling (Lenny B Remix)
| extra5 = k.d. lang
| lyrics5 = Billy Steinberg, Rick Nowels, Marie-Claire DUbaldo
| length5 = 4:16
| title6 = Heart Door (With Dolly Parton)
| extra6 = Paula Cole with Dolly Parton
| lyrics6 = Paula Cole
| length6 = 4:08
| title7 = My Number
| extra7 = Tegan and Sara
| lyrics7 = Tegan Rain Quin, Sara Keirsten Quin
| length7 = 4:09
| title8 = Off The Hook
| extra8 = Barenaked Ladies
| lyrics8 = Steven Page, Ed Robertson
| length8 = 4:34
| title9 = Rock DJ extra9 = Robbie Williams
| lyrics9 = Robbie Williams, Guy Chambers, Kelvin Andrews, Nelson Pigford, Ekundayo Paris
| length9 = 4:16
| title10 = Baby Work Out
| extra10 = Jackie Wilson, Alonzo Tucker
| lyrics10 = Jackie Wilson
| length10 = 3:00
| title11 = You Deserve To Be Loved
| extra11 = Tracy Dawn
| lyrics11 = Dillon OBrian
| length11 = 5:02
| title12 = Wherever You Are
| extra12 =  Celeste Prince
| lyrics12 = Larry Klein, Tonio K
| length12 = 4:17
| title13 = The Other Half Of Me
| extra13 = Bobby Darin
| lyrics13 = S. Freeman, J. Lawrence
| length13 = 2:27
| title14 = Calafia
| extra14 = Jump With Joey
| lyrics14 = Dave Ralicke
| length14 =
| title15 = Middle of the Night
| extra15 = Rick Braun
| lyrics15 = Paul Brown, Roberto Vally, Rick Braun
| length15 =
| title16 = Time After Time
| extra16 = Keanu Reeves
| lyrics16 = Jules Styne, Sammy Cahn
| length16 =
}}

==Reception==
Sweet November received negative reviews from critics, as the movie holds a 16% rating on Rotten Tomatoes based on 96 reviews with the consensus: "Bad acting and direction plague this version of Sweet November. The story is unrealistic, as is the chemistry between Theron and Reeves."
 Worst Remake Planet of Worst Actor Worst Actress John Wilsons book The Official Razzie Movie Guide as one of the 100 Most Enjoyably Bad Movies Ever Made. 

===Box office=== Down To Earth and Hannibal (film)|Hannibal. It would ultimately gross only $25.2 million domestically with an additional $40.4 million overseas to a total of $65.7 million worldwide. 

==See also== Sweet November (1968)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 