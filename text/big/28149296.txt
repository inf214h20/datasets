How to Drown Dr. Mracek, the Lawyer
 
{{Infobox film
| name           = How to Drown Dr. Mracek, the Lawyer
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Václav Vorlíček
| producer       = 
| writer         = Miloš Macourek Petr Markov Václav Vorlíček
| narrator       = 
| starring       = Jaromír Hanzlík
| music          = 
| cinematography = Vladimír Novotný
| editing        = 
| studio         = 
| distributor    = 
| released       = 1974
| runtime        = 96 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech comedy film directed by Václav Vorlíček. It was released in 1974.

==Cast==
* Jaromír Hanzlík - Dr. Jindřich Mráček
* Libuše Šafránková - Jana Vodičková
* František Filipovský - Bertík
* Miloš Kopecký - Mr. Wassermann
* Vladimír Menšík - Karel
* Zdeněk Řehoř - Alois
* Stella Zázvorková - Doc. Mráčková
* Eva Trejtnarová - Polly Wassermann
* Čestmír Řanda - Albert Bach
* Míla Myslíková - Matilda Wassermann
* Vlastimil Hašek - Honza
* Jiří Hrzán - Tomáš
* Miroslav Masopust - Rolf
* Gabriela Wilhelmová - Růženka
* Milena Steinmasslová - Krista

==External links==
*  

 
 
 
 
 


 
 