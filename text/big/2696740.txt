Curious George (film)
{{Infobox film
| name = Curious George
| image = 3rd Curious George Poster.jpg
| caption = Theatrical release poster
| director = Matthew OCallaghan
| producer = Ron Howard David Kirschnerg Jon Shapiro
| screenplay = Ken Kaufman
| story = Ken Kaufman Mike Werb
| based on =  
| starring = Will Ferrell Drew Barrymore David Cross Eugene Levy Joan Plowright Dick Van Dyke Frank Welker Jack Johnson  
| editing = Julie Rogers
| studio = Imagine Entertainment A. Film A/S Universal Animation Studios Universal Pictures
| released =  
| runtime = 88 minutes
| country = United States
| language = English
| budget = $50 million
| gross = $69,834,815
}} animated adventure adventure family book series Jack Johnson. This project had been under development at Imagine Entertainment for a long time, dating back at least as long ago as 1992 (and possibly many years before this). 

The screenplay was written by Ken Kaufman, with a story by Kaufman and Mike Werb.  Although it is a traditionally animated film, about twenty percent of it takes place in 3D environments that were computer-generated. It was the first Universal Pictures theatrically released feature-length animated film since 1995 in film|1995s Balto (film)|Balto, and Imagine Entertainments first animated film.
 a TV series on PBS Kids, with Jeff Bennett replacing Ferrell as the voice of the Man with the Yellow Hat.

==Plot==
The introduction of the movie is a cartoon short where we are introduced to a happy and mischievous but lonely little monkey somewhere in the jungle.
Ted (Will Ferrell) is an employed guide at the Bloomsberry Museum. We are introduced to teacher Ms. Maggie Dunlop (Drew Barrymore) and her students who come to the museum often but they are the only regular visitors. Mr. Bloomsberry (Dick Van Dyke) heartbreakingly informs Ted that the museum will have to close, because it is no longer making any money. Bloomsberrys son, Junior (David Cross), wants to tear down the museum, and replace it with a parking garage. Ted is convinced to volunteer to go to Africa in place of Mr. Bloomsberry and bring back a mystical, forty-foot-tall idol known as "Lost Shrine of Zagawa" in the hopes that it will attract customers, much to Juniors envy. Ted is outfitted with a hideous yellow suit (which causes people to laugh at him), and boards a cargo ship to Africa.

With the help of a tour guide and tour group, Ted finds the "Lost Shrine of Zagawa", but discovers it to be only three inches tall, much to his disappointment. Ted also encounters the little monkey on his expedition to Africa, and gives the monkey his yellow hat. The monkey, who quickly grows fond of Ted, follows him and boards the cargo ship, unknown to Ted. Ted returns home, and enters his apartment, only to receive a call from Bloomsberry telling him to report to the museum, so Ted can do an interview on the news. The monkey follows Ted to his apartment, the monkey is discovered busily repainting a posh apartment in full scale African animals and due to the strict "no pets allowed policy", Ted is evicted by grumpy doorman Ivan (Ed ORoss). Ted returns to the museum, and reveals to Bloomsberry the idols size. Ted is kicked out of the museum by Junior, after the monkey accidentally destroys an Apatosaurus skeleton.

Ted and the monkey sleep outside in the park, and the next morning, Ted follows the monkey into the zoo, where Maggie and her young students name the monkey "George". George gets into trouble and begins dangerously floating away on balloons high up over the city, Ted takes flight as well. Georges balloons are popped by spikes on a building and Ted saves him from certain death.

Ted and George make their way to the home of an inventor named Clovis (Eugene Levy), where George uses an overhead projector to increase the idols size making it appear 40 feet tall. Ted and George head to the museum to show Bloomsberry the invention, but Junior, determined to get his parking garage, foils Teds plan by pouring hot coffee on the machine and framing George. Believing his life to be ruined, Ted allows animal control to take away George.

Ted speaks with Ms. Maggie who helps Ted "see" what is really important in Teds life. Ted regrets the decision to give George to animal control, and boards Georges cargo ship to get him back. Ted tells George that nothing else matters besides their "buddyship". In the hold of the ship, George discovers that when reflected in light, the idol reveals a pictogram with the message "turn your eye to the light, go from blindness to sight". It turns out that when held up to the sun, the small idol is actually a map to the real idol. They sail the ship back to Africa and George helps him find the real idol which is, indeed, forty feet tall.

The idol is put in the museum, and the museum goes back in business, and becomes more successful than ever when it becomes more hands-on because of the addition of Cloviss inventions, the interaction with Ms. Dunlop and her young students and, of course, George. Junior finally gets his parking garage, but is upset that Ted is still working at the museum. Ivan invites Ted to move back to his apartment because he likes George, and Ted and Maggie start a romance, almost.

==Cast==
* Frank Welker as Curious George, a curious monkey who is compassionate and clever, with a proficiency in visual art.
* Will Ferrell as Ted Shackleford (The Man in the Yellow Hat), Curious Georges friend. He is clumsy, but resilient and compassionate. In a deleted scene, his last name is established as Shackleford.
* Drew Barrymore as Margaret "Maggie" Dunlop, a teacher .
* David Cross as Junior Bloomsberry, the son and only child of the museums owner.
* Eugene Levy as Clovis, a museum employee who  builds robotic animals to help him with his work.
* Joan Plowright as Ms. Plushbottom,  Teds neighbor. She is an opera singer.
* Dick Van Dyke as Mr. Bloomsberry, the kind, elderly owner of the museum. 
* Ed ORoss as Ivan, the doorman at Teds apartment building.
* Michael Chinyamurindi as Edu, Teds African guide.

==Production==
In various points during its development, it was proposed that the film be entirely CG or live-action mixed with CG, before the decision was finally made to use traditional animation to bring the titular character to life.  As of 2001, Brad Bird had written a script for the film. 

Director Matthew OCallaghan greatly appreciated having Dick Van Dyke voice one of the characters. "I was surprised when I actually finally met him that he had never done an animated voice before, with his association with Disney for all those years. I was just blown away so Im going, This is great, because as an animation director you always want to use people who are fresh, who havent done animated voices – at least I do." he said. 

CG Supervisor Thanh John Nguyen states that they tried to duplicate the look of the  cars in the book, which Executive Producer Ken Tsumura describes as bearing the look of the 1940s and 1950s; According to Production Designer Yarrow Cheney, the filmmakers also partnered with Volkswagen to design the red car that Ted drives, simplifying it a bit and rounding the edges.   Cheney also said that prior to this they had based some of the models on Volkswagens due to their suitability.   

==Reception and box office performance==
The film was released to 2,566 theaters on February 10, 2006 and opened at #3 with a total opening weekend gross of $14,703,405 averaging $5,730 per theater. The film grossed a better-than-expected $58.3 million in the United States and $11.4 million overseas, totaling $69.8 million worldwide and becoming a minor success.

The film received generally positive reviews and earned a 69% approval rating at Rotten Tomatoes. Roger Ebert praised the design of the film and its faithfulness to the "spirit and innocence of the books."  Since he himself didnt particularly enjoy the film, Ebert made an exception in this case in recommending it for young children based on its better qualities, a point on which he said he disagreed somewhat with his TV show co-host Richard Roeper.    Owen Gleiberman of Entertainment Weekly was "pleasantly surprised" by the films calm tone, which he found to be against modern trends, but said that George was perhaps a bit too sweet and that the "movie comes close to denying hes any sort of troublemaker".  He noted somewhat negatively the few modern anachronisms in the film.    Brian Lowry of Variety (magazine)|Variety was negative about the film, criticizing the quality of the animation, the music, and other aspects.  Lowry states that there are some updates to the story, such as that "The Man in the Yellow Hat" from the books is finally given a name.  He also notes that David Cross animated character bears a strong resemblance to the actor himself.    Colin Covert of the Minneapolis Star Tribune noted that the films use of "traditional cell   painting and digital effects" compliments the original watercolor illustrations, and thought the film entertaining and yet still quite simple.  He considered the difficulties in adapting the original stories (in which George basically causes trouble and the Man in the Yellow Hat fixes it all up) into a film, and how some conflict and a slight romantic subplot were added.   

==Soundtrack==
  Jack Johnson On and On peaked at three) and making it the first soundtrack to reach number one since the Bad Boys II soundtrack in August 2003 and the first soundtrack to an animated film to top the Billboard 200 since Pocahontas (1995 film)|Pocahontas reigned for one week in July 1995.

==Home media history==
* September 26, 2006 (DVD)
* March 20, 2007 (DVD - 2-pack with The Land Before Time)
* November 20, 2007 ( ) (Note: This is a widescreen DVD box set only.)
* August 5, 2008 (Carrying Case   DVD copy only.) 
* March 3, 2015 (Blu-ray)

==TV series== Curious George narrated by William H. Macy for Season 1 and Rino Romano from Season 2 onwards.

==Sequel== The Tale of Despereaux. The sequel was released on March 2, 2010.

The plot for the sequel centers around George becoming friends with a young elephant named Kayla. George tries to help Kayla travel across the country to be reunited with her family.

==References==
 

==External links==
*    
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 