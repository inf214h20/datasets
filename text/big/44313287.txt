The Holy Queen
{{Infobox film
| name =   The Holy Queen
| image =
| image_size =
| caption =
| director = Henrique Campos   Aníbal Contreiras   Rafael Gil
| producer = Aníbal Contreiras    Cesáreo González
| writer = Tavares Alves    Luna de Oliveira    Antonio Tudela   Rafael Gil   Aníbal Contreiras 
| narrator =
| starring = Maruchi Fresno   Antonio Vilar   Luis Peña   Fernando Rey
| music = Ruy Coelho  
| cinematography = Manuel Berenguer   Alfredo Fraile 
| editing = Suevia Films 
| studio = Suevia Films 
| distributor = Mercurio Films 
| released = 15 September 1947 
| runtime = 110 minutes
| country = Portugal   Spain Portuguese  Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}} historical drama film starring Maruchi Fresno, Antonio Vilar and Luis Peña.  Separate Spanish and Portuguese versions were filmed with the Spanish directed by Rafael Gil and the Portuguese by Henrique Campos and Aníbal Contreiras. It was part of a popular group of Spanish costume films made in the late 1940s.
 Isabel of Aragon, a Spanish-born Queen of Portugal who played a role of peacemaker between different factions at the Portuguese court as well as between Portugal and Crown of Castile|Castile.

==Cast==
* Maruchi Fresno as Isabel of Aragón  
* Antonio Vilar as Dinis, King of Portugal  
* Luis Peña as Nuño de Lara 
* Fernando Rey as Infante Alfonso  
* José Nieto (actor)|José Nieto as Vasco Peres, the alderman 
* María Martín as Blanca 
* Juan Espantaleón as Pedro, the priest  Fernando Fernández de Córdoba as Pedro III de Aragón 
* María Asquerino as Leonor 
* Barreto Poeira as Álvaro, the squire  
* Milagros Leal as Doña Betaza  Virgilio Teixeira as Alfonso Sánchez 
* Julieta Castelo as Doña María Ximénez  
* Gabriel Algara as Juan Velho  
* Carmen Sánchez as Doña Constanza  
* Emilio G. Ruiz as Infante Pedro  
* Manuel Guitián as Ramiro  
* Joaquina Almarche as María de Molina  
* Rafael Luis Calvo as Fernán Ayres  
* José Prada as Martín Gil  
* Manrique Gil as Don Juan Manuel 
* Félix Fernandez (actor)|Félix Fernandez as Barredo  
* Joaquín Pujol 
* José Franco (actor)|José Franco as Froilas  
* Luisa España as Isabel as a child  
* Santiago Rivero 
* Julio F. Alymán Francisco Hernández as Villafranca  
* Elvira Real
* Mercedes Castellanos as María  
* Alfonso de Córdoba as Juan 
* Fernando Fresno as Físico  
* Emilio García Ruiz as Infante Pedro
* José Jaspe as Estevao

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008. 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 