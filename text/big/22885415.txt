Faith and Fate
{{Multiple issues|
 
 
}}

{{Infobox Film
| name           = Faith and Fate
| caption        =
| director       = Ashley Lazarus
| producer       = Berel Wein
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2005
| runtime        = 107 min. America
| English
| budget         =
| preceded_by    =
| followed_by    =
}}
 documentary about the Jewish people in the last century.

Faith and Fate, by Berel Wein, focuses on Jewish history and how the events and occurrences of the 20th century affected these people - a people whose survival has defied the ravages and challenges not only of this century, but of the over 40 centuries that have led up to it. Faith and Fate tells the story of how the events of the century affected the Jews - and the impact the Jews had on the century.

As Berel Wein puts it, “What makes this series so unique is that it puts the Jewish history of the 20th century into perspective. We can see ourselves in the “big picture”, how we fit into nearly 4,100 years of Jewish survival. Who we are as Jews? How, and why, we have survived so long? What is our purpose? And what is our Jewish Destiny? It tells our children, and our grandchildren, that despite the tragedies, there are triumphs and that knowing where we come from will help us understand where we are going.”

==External links==
*  
*  
*  

 
 
 
 
 
 
 

 