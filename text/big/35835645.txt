City of Lost Souls (1983 film)
{{Infobox film
| name           = City of Lost Souls
| image          = City of Lost Souls film poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Rosa von Praunheim 
| producer       = Rosa von Praunheim
| writer         = 
| screenplay     = Rosa von Praunheim	
| story          = 
| based on       =  
| narrator       = 
| starring       = Angie Stardust   Jayne County  Tara OHara Judith Flex   Joaquin La Habana
| music          = Holger Münzer
| cinematography = Stephan Köster
| editing        = 
| studio         = 
| distributor    =
| released       = 
| runtime        = 
| country        = West Germany
| language       = German   English
| budget         = 690,000 DM
| gross          = 
}}
 German film directed by Rosa von Praunheim.  The film is a low budget musical satire, a fictionalized account about the lurid lives of a group of eccentric cabaret artist who have come from America to Berlin looking for social acceptance and a place to give full reign to their creative nature. The film star black singer and drag artist Angie Stardust (born Mel Michaels), transgender punk singer Jayne County and transvestite Tara OHara. Murray, Images in the Dark, p. 108  County gave the film its title and wrote its theme song.

==Notes==
 

== References ==
*Murray, Raymond. Images in the Dark: An Encyclopedia of Gay and Lesbian Film and Video. TLA Publications, 1994, ISBN 1880707012

==External links==
* 

 
 
 
 
 


 