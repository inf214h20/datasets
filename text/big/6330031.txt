Annie Laurie (1927 film)
{{Infobox film
| name           = Annie Laurie
| image          = Annie Laurie FilmPoster.jpeg
| caption        = Film poster
| director       = John S. Robertson
| writer         = {{plainlist|
*Josephine Lovett (screenplay)
*Marian Ainslee (titles)
Ruth Cummings (titles)
}}
| starring       = {{plainlist|
*Lillian Gish
*Norman Kerry
}}
| music          = {{plainlist|
*William Axt
*David Mendoza
}}
| cinematography = Oliver Marsh
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = {{plainlist|
*Silent film
*English intertitles
}}
}}
Annie Laurie is a 1927 silent film directed by John S. Robertson, released by Metro-Goldwyn-Mayer, and starring Lillian Gish and Norman Kerry.

This was the third film of Lillian Gish at MGM, and its poor boxoffice returns marked a decline in the stars career. On a down note Gish stated that her mother became ill during the production of this film and that "...she only showed up for work" as opposed to putting her all into the film.  John Wayne makes an early film appearance as a crowd extra. 

==Synopsis==
A film about the battles of Scottish clans.

==Cast==
* Lillian Gish as Annie Laurie
* Norman Kerry as Ian Macdonald
* Creighton Hale as Donald
* Joseph Striker as Alastair
* Hobart Bosworth as The MacDonald Chieftain
* Patricia Avery as Enid Russell Simpson as Sandy
* Brandon Hurst as The Campbell Chieftain
* David Torrence as Sir Robert Laurie Frank Currier as Cameron of Lochiel
* John Wayne as Extra (uncredited)

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 
 


 
 