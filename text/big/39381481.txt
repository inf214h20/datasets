The Red Sneakers
{{Infobox film
| name           = The Red Sneakers
| image          = Sneakers Red television film.jpg
| image_size     =
| alt            =
| caption        = DVD Cover
| director       = Gregory Hines
| producer       = Tommy Lynch Gary L. Stephenson
| screenplay     = Mark Saltzman
| story          = Jeffrey Rubin
| starring       = Vanessa Bell Calloway Dempsey Pappion Ruben Santiago-Hudson
| music          = Stanley Clarke
| cinematography = John Berrie
| editing        = Tim King
| studio         = Dufferin Gate Productions
| distributor    = Showtime Networks
| released       =   
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         =
}}
The Red Sneakers is a 2002 American made for television fantasy film produced and directed by Gregory Hines.  The drama features Vanessa Bell Calloway, Dempsey Pappion, and Ruben Santiago-Hudson. 

==Plot==
This coming of age story features a mediocre high school basketball player (Dempsey Pappion) who is given a pair of magical basketball shoes by a stranger (Gregory Hines) and he quickly becomes a superstar shooter on his team.  He is recruited by college basketball scouts and plans his future in college basketball as he neglects potential academic scholarship possibilities.

==Cast==
* Vanessa Bell Calloway as Berniece
* Dempsey Pappion as Reggie
* Ruben Santiago-Hudson as Uncle Joe Scott Thompson  as Aldo
* Philip Akin as Mr. Seabrooke
* K. C. Collins as Roscoe
* Kendra FitzRandolph as Courtney
* Cabral Richards as Khalil
* Vincent DOnofrio as Mercado
* Gregory Hines as Zeke
* Sarah Barrable-Tishauer as Larosa
* Jordan Walker as Noah Greggory Drew Nelson as Jacob
* Neil Crone as Coach Blake
* Reuben Thompson as Alvin Duke
* Jake Goldsbie	as Boy

==Exhibition==
The film premiered on Showtime Networks on February 10, 2002.

==Reception==

===Critical response===
Sara Long, with the faith based and family oriented Dove Foundation gave the film a positive review, writing, "The Red Sneakers is an enjoyable movie based around inner desires, and what one knows is right as far as actions go ... Then the movie turns around to focus on the inner battle over how to behave against what one wants, and what one should do. Though this movie does have several instances of profanity, it is nothing too severe. Because the overall content is well-displayed, the film is approved for ages 12 and up. 

===Awards===
Nominations Emmy Award 2003  Emmy Award 2003 
* Writers Guild of America - WGA Award (Television) - Childrens Script - Mark Saltzman (teleplay) and Jeffrey Rubin (story)
* Young Artist Awards - Best Family Television Movie - Leading Young Actor - Jake Goldsbie - 2003

==Film Festivals==
* Chicago International Childrens Film Festival - 2002

==References==
 

==External links==
*   official web site
*  
*  
*  

 
 
 
 
 
 
 
 
 
 