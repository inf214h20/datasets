Zeemansvrouwen
 
{{Infobox film
| name           = Zeemansvrouwen
| image          =
| image_size     =
| caption        =
| director       = Henk Kleinmann
| producer       =
| writer         = Lodewijk de Boer Herman Bouber (play)
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = May 16, 1930
| runtime        = 85 minutes
| country        = Netherlands Dutch
| budget         =
}} Dutch film directed by Henk Kleinmann, and is the first ever sound film produced in the Netherlands. 

==Cast==
*Harry Boda	... 	Willem Broerse
*Josephine Schetser	... 	Mooie Leen
*Raas Luijben	... 	Lau
*Jos Pasch	... 	Dronken Lodewijk, vader van Leen
*Clara Vischer-Blaaser	... 	Manke Mie
*Henkie Klein	... 	Nelis
*Daan Scheffer	... 	Kruidenier
*Henriette Verbeek	... 	Lucy
*Willem Heideman	... 	Daantje
*Mejuffrouw Cellarius	... 	Jaantje
*Reina Menjon	... 	Bertha
*Henk Kleinmann	... 	Dokter
*Kees Grutter	... 	Rooie Bart
*Dick Menten	... 	De luie

==References==
 

==See also==
*Dutch films of the 1930s

== External links ==
*  

 
 
 

 