Jennifer Hale (film)
{{Infobox film
| name           = Jennifer Hale
| image          = 
| image size     = 
| caption        = 
| director       = Bernard Mainwaring John Findlay
| writer         = Bernard Mainwaring Ralph Stock Edward Dryhurst Rob Eden  (novel)
| screenplay     = 
| story          = 
| narrator       = 
| starring       = René Ray Ballard Berkeley John Longden
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Twentieth Century Fox
| released       = 1937
| runtime        = 66 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}


Jennifer Hale is a 1937 British crime film directed by Bernard Mainwaring and starring René Ray, Ballard Berkeley and John Longden.  Its plot follows a showgirl who is wrongly accused of murdering her manager and goes on the run to try to prove her innocence.

==Cast==
* René Ray – Jennifer Hale 
* Ballard Berkeley – Richard Severn 
* John Longden – Police Inspector Merton  Paul Blake – Norman Ives 
* Frank Birch – Sharman  Richard Parry – Jim Watson 
* Ernest Sefton – Police Sergeant Owen
* Patricia Burke - Maisie Brewer

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 