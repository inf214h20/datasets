Pearl Lust
 
{{Infobox film
  | name     = Pearl Lust
  | image    = 
  | caption  = 
  | director = A. R. Harwood		
  | producer = A. R. Harwood
  | writer   = 
  | starring = Fay Revel John Bowden 
  | music    = 
  | cinematography =
  | editing  = A. R. Harwood
  | studio = Apex Films Ltd
  | distributor = 
  | released = 1936
  | runtime  = 
  | language = 
  | country = Australia
  | budget   = 
}}

Pearl Lust is a 1936 Australian film directed by A. R. Harwood. It was shot in 16mm and was never released theatrically as it was aimed at the home movie market. 

==Production==
The movie was a South Seas romance, shot on location in Portsea and at Cinesound Productions studio in St Kilda.  Filming took place in August and September 1936. 

It was the first film produced under quota requirements in Victoria. 

==Cast==
*Fay Revel
*John Bowden
*George Edwards
*Lance Nicholls
*Fred McNaughten
*George Arthur

==References==
 

==External links==
*  at National Film and Sound Archive

 

 
 


 