Hanste Zakhm
{{Infobox Film
| name =Hanste Zakhm
| image = Hanste Zakhm (1973), DVD.jpg
| image_size =
| caption =  Chetan Anand
| producer = Chetan Anand
| writer = 
| narrator =  Nadira
| music = Madan Mohan
| cinematography = 
| editing = 
| distributor = 
| released = 1973
| runtime = 
| country = India
| language = Hindi
| budget = 
}} Chetan Anand. Nadira in lead roles. The film has music composed by Madan Mohan and lyrics penned by Kaifi Azmi.

==Plot==
The film begins with Mahendru (Balraj Sahni), a widowed police officer and his young daughter, Chanda. Chandas best friend is Rekha (Suman Sikand), the daughter of a prostitute. Rekha stays over at Chandas house, when Rekhas mother accidentally kills someone in an effort to stop her pimp (Jeevan (actor)|Jeevan) from carrying off Rekha into prostitution as well. She is sent to prison, and begs Mahendru not to reveal to Rekha anything about her life as a working girl. Mahendru promises to protect and raise the child as his own daughter. The pimp, Jeevan, attempts to kidnap Rekha but his goons mistakenly kidnap Chanda. When he realizes the error he decides to make the best of a bad situation by asking Mahendru for a large ransom. Mahendru borrows the ransom money from a friend, but the pimp deceives him and delivers the girl to a madam (Nadira (actress)|Nadira). 

Many years later, we find cab driver Somesh (Navin Nischol) driving Chanda (Priya Rajvansh) to her clients. Somesh is the neer-do-well scion of a rich family, who has a falling out with his father as he refuses to marry Rekha, the Police Officers daughter. He leaves his wealthy home & life-style to become a cab-driver. Chanda and Somesh fall in love. Chanda eventually realizes that she really is Mahendrus daughter. This leads her to assume the daughters duty by trying to convince Somesh to marry Rekha, her sister. She does this by turning alcoholic, in an effort to prove her essential wanton-ness to Somesh - who does not understand her intention and refuses to live his life according to her ideals of filial responsibility. Later, the pimp kidnaps Chanda again and informs Mahendru that his daughter is under his captivity and tries to blackmail him for release of Chanda. However, Mahendru refuses to give-in and attacks Jivans camp with police force. Chanda dies in this rescue operation. Before dying in her father Mahendrus arms, Chanda asks Somesh to marry Rekha, and he does.

==Cast==
* Navin Nischol as Somesh
* Priya Rajvansh as Chanda / Meena "Minno"
* Balraj Sahni as SP Dinanath Mahendru Nadira as Madame Jeevan as Kundan
* Mac Mohan as Braganza Murad as DIG of Police
* D.K. Sapru as Banwari
* Achala Sachdev as Heerabai (Rekhas mom)
* Satyen Kappu as Inspector Kumar
* Ram Sethi

==Music==
Composed by Madan Mohan the songs of the film are popular.
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = no
| title1 = Yeh Maana Meri Jaan, Mohabbat Saza Hai | extra1 = Mohammed Rafi | lyrics1 = | length1 = 
| title2 = Betaab Dil Ki Tamanna Yehi Hai| extra2 = Lata Mangeshkar | lyrics2 = | length2 = 
| title3 = Tum Jo Mil Gaye Ho, To Yeh Lagta Hai, Ke Jahaan Mil Gaya| extra3 = Mohammed Rafi,  Lata Mangeshkar | lyrics3 = | length3 = 
| title4 = Aaj Socha Toh Aansu Bhar Aaye| extra4 = Lata Mangeshkar | lyrics4 = | length4 = 
| title5 = Gali Gali Mein Kiya Re Badnaam | extra5 = Asha Bhonsle | lyrics5 = | length5 = 
}}

==References==
 

==External links==
* 
*  

 
 
 
 
 