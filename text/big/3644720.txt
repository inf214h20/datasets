Diary of a Chambermaid (1964 film)
{{Infobox film
| name           = Diary of a Chambermaid
| image          = Diary_of_a_Chambermaid.jpg
| caption        = Theatrical poster
| director       = Luis Buñuel
| producer       = Michel Safra Serge Silberman
| writers        = Luis Buñuel Jean-Claude Carrière
| starring       = Jeanne Moreau Michel Piccoli Georges Géret Françoise Lugagne Daniel Ivernel Jean Ozenne
| music          = 
| cinematography = Roger Fellous
| editing        = Louisette Hautecoeur
| distributor    = 
| released       =   
| runtime        = 97 minutes 85 minutes (alternative French version)
| country        = France Italy
| language       = French
| budget         = 
| gross          =
}}
 chambermaid who uses her feminine charms to control and advance her situation, in a social setting of corruption, violence, sexual obsession and perversion.
 Belle de novel of its second film adaptation, made in Hollywood in 1946, directed by Jean Renoir.

==Plot== chambermaid for pelvic "pain." M. Monteil (Michel Piccoli) amuses himself by hunting small game and pursuing all the females within range &ndash; the previous chambermaid seems to have left pregnant and had to be "bought off." 

The wifes father amuses himself with his collection of racy postcards and novels, and a closet full of womens shoes and boots, that he likes his chambermaids to model. Their next-door neighbor (Daniel Ivernel) is a burly, retired Army officer, with a chubby maid/mistress (Gilberte Géniat), and a violent streak of his own &ndash; he likes to throw refuse and stones over the fence, to the great annoyance of M. Monteil. Célestine almost immediately finds her role in the house completely defined by the sexual proclivities of the other characters, and she proceeds to use her own considerable sexual assets to accomplish her goals. 

The elderly father, M. Rabour (Jean Ozenne), is found dead in bed, dissheveled, clutching some boots that Célestine had worn earlier that evening; and Célestine decides to leave the job the next day. Previously, however, she had become motherly and protective of a sweet pre-pubescent girl named Claire (Dominique Sauvage) who visited the house; after the girls raped and mutilated body is found in a nearby wood, Célestine decides to stay on at the job, in order to get revenge on the murderer. She quickly finds reason to suspect the groom Joseph. She seduces and promises to marry him and join him to run a café in Cherbourg, so he will confess the crime to her, which he does not. She then contrives and plants evidence to implicate him in the girls murder. He is arrested, but eventually released for lack of solid evidence, although there is a suggestion that the real reason is his nativist political activism. Meanwhile Célestine agrees to marry the elderly ex-Army-officer neighbor, and after the marriage, we see him serving her breakfast in bed and obeying her commands. The final scene shows a crowd of nationalistic men marching past the Cherbourg café run by Joseph, who has another woman now and is shouting rightist slogans.

==Cast==
  
*Jeanne Moreau as Célestine
*Georges Géret as Joseph
*Daniel Ivernel as Captain Mauger
*Françoise Lugagne as Madame Monteil
*Marguerite Muni as Marianne
*Jean Ozenne as Monsieur Rabour
*Michel Piccoli as Monsieur Monteil  
 
*Marc Eyraud as Le secrétaire du commissaire
*Gilberte Géniat as Rose
*Bernard Musson as The sacristan
*Dominique Sauvage as Claire
*Jean-Claude Carrière as The Priest
*Claude Jaeger as The judge
 

==Production and release notes==
*The film was originally intended for the Mexican actress Silvia Pinal (star of the film Viridiana, 1961). Pinal learned French and was willing to charge nothing for her participation. However, the French producers ended up choosing Jeanne Moreau. 
*Shooting on Diary of a Chambermaid began on 21 October 1963. TCM   
*At the end of the film, the marching rightists shout "Vive Jean Chiappe|Chiappe", a reference to the Paris police chief who stopped director Buñuels 1930 film, LÂge dOr from being exhibited after the theater it was being shown in was destroyed by Fascists. 
*In 1964, the film was shown at the Venice Film Festival and the New York Film Festival. 
*The film was first released on home video in the U.S. on 22 March 1989. 
*The film was re-released at the Film Forum in New York City on 13 October 2000 in a new 35-mm print. 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 