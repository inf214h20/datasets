Shooting at the Moon (film)
{{Infobox film
| name           = Shooting at the Moon
| image          =
| image_size     =
| caption        =
| director       = Jesse Richards Nicholas Watson
| producer       =
| writer         = Jesse Richards Nicholas Watson
| narrator       =
| starring       = Matthew Quinn Martin Leila Laaraj
| music          = Billy Childish The Earls of Suave
| cinematography = Jennifer Martin Jennifer B. Katz
| editing        =
| distributor    = New Haven Stuckists Film Group/Remodernist Film and Photography
| released       =  
| runtime        = 10 min.
| country        = United States English
| budget         =
}}
Shooting at the Moon is a short Super-8 punk/Remodernist film directed by Jesse Richards and Nicholas Watson, starring Matthew Quinn Martin (billed as Matthew Martin) as Buddy and Leila Laaraj as Lana, and features music by Billy Childish. It was shot in the summer of 1998 and its final cut was completed in 2003. The film premiered at the New York International Independent Film and Video Festival in November 2003. On March 8, 2008 the film made its London premiere at Horse Hospital during their FLIXATION Underground Cinema event.

==External links==
*  
*  
* 

 
 
 
 
 


 