Priya (1970 film)
{{Infobox film 
| name           = Priya
| image          =
| caption        = Madhu
| producer       = NP Abu
| writer         = C Radhakrishnan
| screenplay     = C Radhakrishnan Madhu Sukumari Jayabharathi Adoor Bhasi
| music          = MS Baburaj
| cinematography = U Rajagopal
| editing        = Hrishikesh Mukherjee
| studio         = Jammu Films
| distributor    = Jammu Films
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film, Madhu and produced by NP Abu. The film stars Madhu (actor)|Madhu, Sukumari, Jayabharathi and Adoor Bhasi in lead roles. The film had musical score by MS Baburaj.    It is the first directorial venture of actor Madhu, also the Malayalam debut of Mahendra Kapoor and the only Malayalam film of actress Lilly Chakravarthy. The movie received two state awards in 1970 for best editing for Hrishikesh Mukherji and second best film of the year.   

==Cast==
  Madhu
*Sukumari
*Jayabharathi
*Adoor Bhasi
*Kottayam Santha
*Sankaradi
*Ramu Kariyat
*Abbas
*Bahadoor
*Chandraji Khadeeja
*Lilly Chakravarty Meena
*Paravoor Bharathan
*Philomina
*Prema Rao
*T. K. Balachandran
*Veeran
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadaanumariyaam || S Janaki || Yusufali Kechery || 
|-
| 2 || Kaneeraaloru || S Janaki || Yusufali Kechery || 
|-
| 3 || Kanninu Kannaaya Kanna || Latha Raju || Yusufali Kechery || 
|-
| 4 || Kannonnu Thurakkoo || S Janaki, P. Leela || Yusufali Kechery || 
|-
| 5 || Saagarakanyaka || Mahendra Kapoor || Yusufali Kechery || 
|-
| 6 || Vinnile Kaavil || S Janaki || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 


 