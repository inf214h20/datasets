Karate Kiba
 
{{Infobox film
| name           = Karate Kiba
| image          =
| image_size     =
| caption        =
| director       = Tatsuichi Takamori Simon Nuchtern (US footage)
| producer       =  Susumu Yoshikawa Terry Levene (US footage)
| writer         = Comic Book: Ikki Kajiwara Ken Nakagusuku
| narrator       =
| starring       = Sonny Chiba Masutatsu Ōyama 
| music          =
| cinematography = Joel Shapiro
| editing        =
| distributor    =
| released       = 1973 1976 (American release)
| runtime        =
| country        = Japan
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
  is a martial-arts film starring Sonny Chiba, released in 1973 and based on an action manga by Ikki Kajiwara.

A recut version was released in the U.S. in 1976 as The Bodyguard, with added footage in the first ten minutes of the film.

There were two more movie adaptations made in 1993 and 1995 by Takashi Miike in the beginning of his career. 

==Plot summary==
"Karate master and anti-drug vigilante Chiba returns to his home in Japan, where he holds a press conference announcing his intention to wipe out the nations drug industry. He also offers his services as a bodyguard to anyone who is willing to come forward and provide information about the drug lords activities. He is soon approached by a mysterious woman claiming to have important information and asking for Chibas protection. She seems to be legitimate, but is she really what she appears to be?"

==Cultural references==
The American version of the film opens with a quote:

"The path of the righteous man and defender is beset on all sides by the iniquity of the selfish and the tyranny of evil men. Blessed is he, who in the name of charity and good will, shepherds the weak through the valley of darkness, for he is truly his brothers keeper, and the father of lost children. And I will execute great vengeance upon them with furious anger, who poison and destroy my brothers; and they shall know that I am Chiba the Bodyguard when I shall lay my vengeance upon them!"
 
 Pulp Fiction.

==References==
 

==External links==
*  
*  

 
 
 
 