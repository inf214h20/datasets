Ammavin Kaipesi
{{Infobox film
| name = Ammavin Kaipesi
| image = 
| caption = 
| director = Thangar Bachan
| producer = Thangar Bachan
| writer = Thangar Bachan
| starring = Shanthnu Bhagyaraj Iniya
| music = Rohit Kulkarni
| cinematography = Thangar Bachan
| editing = Kishore Te.
| studio = Thankar Thiraikalam
| distributor = Maxpro Entertainers
| released =   
| runtime = 150 Minutes
| country = India
| language = Tamil
}} Tamil drama film directed and cinematographed by Thangar Bachchan. Shanthnoo Bhagyaraj, Iniya  form the cast. Director Thangar Bachan who gave the most memorable films like Azhagi, Solla Marandha Kadhai, Pallikoodam and Onbadhu Roobai Nottu, is coming with a new directorial and production venture Ammavin Kaipesi.

The film is based on a novel of the same name written by the Director. Shanthanu and Iniya play the lead role while Thangar Bachan plays a pivotal role. Ammavin Kaipesi was among 2012 Deepavali releases and was released along with Podaa Podi and Thuppakki receiving mixed to positive reviews.

==Plot==
The story is woven around a mobile phone, which Thankar says connects people by voice and helps to have new relationships and maintain them. A mother has nine children, but she lives in a situation, which separates her from the children. The only way she could be in touch with them and hear their voices is a mobile phone. She regards her mobile phone as the representative of her children and has become possessive of it. Her last son is Shanthanoo and his lover is Iniya. The film also registers the strong bonding between the mother and her last son. 

==Cast==
*Shanthnu Bhagyaraj as Annamalai
*Iniya as Selvi
*Revathi as Ranganayaki
*Thangar Bachan as Prasad
*Meenal as Kanaga
*Nagineedu as Chittibabu
*Azhagam Perumal as Chinnapillai
*Thambi Chozhan
*NSK. Senthil Kumar as Maatheswaran

==Production==
Thankar Bachan is back after a hiatus. Post his Onbadhu Roobai Nottu, he is all set to release his next drama, Ammavin Kaipesi. Starring Shanthnoo Bhagyaraj and Iniya in leading roles, this film is said to be about an abandoned mother. Kishore Te has edited the film. 

The film was issued a ‘U’ certificate, after it impressed the entire censor board. Thankar Bachan has also said that people who have abandoned their parents would feel terribly bad after watching the film. The film went on floor 15 July 2012. 
 
Ammavin Kaipesi released on 13 November 2012 with a clean U Certificate.

==Reception==
The movie released on 13 November 2012 to mixed reviews. MovieCrow called it outdated and overdramatic and rated it 1.5 out of 5.  Pavithra Shrinivasan of Rediff gave 2.5 out of 5 saying that Even though its heart is in the right place, too much melodrama brings Tamil film Ammavin Kaipesi down.  IBNLive rated it 3 out of 5 saying, This film bleed mothers sentiments.  Behindwoods rated 2.25 out of 5 saying, For audience who patronize, Thankar’s work, Ammavin Kaipesi does not disappoint. For the main stream entertainment expecting populace, AK will prove to be a different experience.The film didnt do well at the box office. 

==Soundtrack==
{{Infobox album 
| Italic title  = 
| Name          = Ammavin Kaipesi
| Type          = soundtrack
| Longtype      = to Ammavin Kaipesi
| Artist        = Rohit Kulkarni
| Cover         = Ammavin Kaipesi Audio Cover.jpg 
| Border        = 
| Alt           = 
| Caption       = Front Cover
| Released      = 1 October 2012
| Recorded      = 2012 Feature film soundtrack
| Length        =  Tamil
| Label         = Sa Re Ga Ma
| Producer      = Rohit Kulkarni
| Chronology    = 
| Last album    = Porkkalam (2010)
| This album    = Ammavin Kaipesi (2012)
| Next album    = 
}}

Film score and soundtrack of Ammavin Kaipesi are composed by Rohit Kulkarni who earlier composed music for Porkkalam. Audio was released in Satyam Cinemas Chennai on 1 October 2012 and was released by many famous actors of Tamil Cinema. The song Enna Senji Pora was shown in the trailer and got good response.
 

{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 32:53
| all_music =
| lyrics_credits = yes
| title1 = Enna Senji Pora
| extra1 = Rajiv Sundaresan
| lyrics1 = Na. Muthukumar
| length1 = 4:34
| title2 = Amma Thaane
| extra2 = Haricharan 
| length2 = 4:28
| lyrics2 = Ekadesi
| title3 = Nenjil Eno Indru (Female)
| extra3 = Harini
| lyrics3 = Ekadesi
| length3 = 5:30
| title4 = Nenjil Eno Indru (Male)
| extra4 = Haricharan
| length4 = 5:32
| lyrics4 = Ekadesi
| title5 = Rajapattai
| extra5 = Pushpavanam Kuppusamy, RaginiShri
| length5 = 4:54
| lyrics5 = Na. Muthukumar
| title6 = Thalai Mudhal Padham Varai
| extra6 = Instrument
| length6 = 4:12
| title7 = Ammavin Kaipesi Theme 1
| extra7 = Instrument
| length7 = 2:27
| title8 = Ammavin Kaipesi Theme 2
| extra8 = Instrument
| length 8 = 2:34
}}

==References==
 

==External links==
*  
 

 
 
 
 
 