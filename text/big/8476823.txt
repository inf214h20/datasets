Der Westwall
  1939 film about the Siegfried Line directed by Fritz Hippler, the head of the film division within the Ministry of Public Enlightenment and Propaganda|Propagandaministerium. The Siegfried Line, called the West Wall by the Germans, was a large series of fortifications around the German borders with France, Belgium, Luxembourg, and the Netherlands.

== See also ==
*List of German films 1933-1945

== External links ==
* 

 
{{succession box  Hippler Propaganda films 
| before=Wort und Tat
| years= Der Westwall (1939)
| after=Feldzug in Polen (1939)}}
 

 
 
 
 
 
 
 
 



 