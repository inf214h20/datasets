Beyond the Mask
{{Infobox film
| title = Beyond the Mask
| image = Beyond the Mask 2015.jpg
| caption = film poster
| producer = 
| director = Chad Burns
| based on = 
| writer = Paul McCusker
| starring = Andrew Cheney John Rhys-Davies Kara Killmer Adetokumboh MCormack Steve Blackwood Thomas D. Mahard
| music = 
| cinematography = 
| editing = 
| studio =  Burns Family Studios
| distributor = 
| released =   
| runtime = 
| country = United States
| language = English
| gross = $685,205 
}} 2015 Christian Christian Historical historical film directed by Chad Burns.  The film takes place during the American Revolution. 

== Plot ==
The film follows William Reynolds, an assassin for the British East India Company. To redeem his past and earn the heart of the woman he loves, he must aid in the cause of the colonists during the American Revolution.

== Cast ==
*Andrew Cheney as William Reynolds
*John Rhys-Davies as Charles Kemp
*Kara Killmer as Charlotte Holloway
*Adetokumboh MCormack as Joshua Brand
*Steve Blackwood as Richard Harrison
*Thomas D. Mahard as Dr. Harrow
*Alan Madlane as Ben Franklin

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 