Vathsalya
{{Infobox film 
| name           = Vathsalya
| image          =  
| caption        = 
| director       = Y. R. Swamy
| producer       = 
| story          = K. P. Kottarakkara
| screenplay     =  Rajkumar Udaykumar Narasimharaju Shivaraj
| music          = Vijaya Krishnamurthy
| cinematography = R Madhu
| editing        = R Hanumantha Rao
| studio         = Suchithra Movies
| distributor    = Suchithra Movies
| released       =  
| country        = India Kannada
}}
 1965 Cinema Indian Kannada Kannada film, Narasimharaju and Shivaraj in lead roles. The film had musical score by Vijaya Krishnamurthy.   The film was a remake of Tamil film Pasamalar.

==Cast==
  Rajkumar
*Udaykumar Narasimharaju
*Shivaraj
*Kuppuraj
*Ganapathi Bhat
*Krishna Shastry
*Nanjappa
*Siddaraj
*Siddalingappa
*Narayan Leelavathi
*Jayanthi Jayanthi
*Papamma
*Rama
*Baby Ramamani
*Baby Sunitha
 

==Soundtrack==
The music was composed by Vijaya Krishnamurthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Shramadi Naav Dudidhu || PB. Srinivas, Jikki || Sorat Aswath || 03.14
|-
| 2 || Maagida Hannu || PB. Srinivas || Sorat Aswath || 02.55
|}

==References==
 

==External links==
*  
*  

 

 
 
 


 