Chomana Dudi
{{Infobox Film |
  name           = Chomana Dudi|
  starring       = M. V. Vasudeva Rao  Padma Kumta  Jayarajan 	Sunder Rajan Honnaiah Govind Bhat|
  director       = B. V. Karanth|
  producer       = Praja Films|
  writer         = K. Shivaram Karanth|
  based on       =  |
  editing        = P. Bhaktavatsalam|
  released       = 1975 in film|1975|
  runtime        = 141 minutes|
  language       = Kannada |
  cinematography = S. Ramachandra|
  writer         = Shivaram Karanth|
  music          = B. V. Karanth|
  country        = India |
  }}

Chomana Dudi ( ,  ) is a feature film in the Kannada language. It is based on a novel of the same name, written by Shivaram Karanth. The film was released in the year 1975 and won the National Film Awards (India)#Golden Lotus Award|Swarna Kamal, Indias National Award for the best film.   

==Plot==
Choma is an untouchable bonded-labourer in a village who is working along with his family for a landlord, as he belongs to a backward class. Shampa Banerjee, Anil Srivastava (1988), p65  Due to his social status, he is not allowed to till his own land, something that he desires most. Though he managed to rear a pair of bullocks that he found straying in the forest, he cannot use them to till the land. He comes in contact of Christian missionaries who try to convert him giving him the lure of the land, but Choma does not want to let go of his faith. He releases the fury that fate has beset on him, by beating his drum.

He has three sons and a daughter; two of his elder sons work in a distant coffee estate trying to pay off the debt. One of the sons dies of cholera and the other one converts to Christianity by marrying a Christian girl. His daughter, Belli works in the plantation and falls for the charm of Manvela, the estate-owners writer. She is raped by the estate owner, who then writes off Chomas debt. She returns to Chomas home without telling him of the reality. His youngest son drowns in a river, with nobody coming to save him because of him being an untouchable. He then finds his daughter in an compromising position with Manvela. With anger, he beats her and kicks her out of the house. To defy his fate, he starts tilling a piece of land and then chases off his bullock into the forest. In the climax, Choma shuts himself in his house and starts playing the drum till he dies.

==Awards==
* National Film Award for Best Feature Film (1976)
* National Film Award for Best Actor - M. V. Vasudeva Rao (1976)
* National Film Award for Best Story – K Shivaram Karanth

;Karnataka State Film Awards 1975-76
* First Best Film
* Best Actor – M V Vasudev Rao
* Best Supporting Actress – Padma Kumata
* Best Story Writer – Shivaram Karanth
* Best Screenplay – Shivaram Karanth
* Best Sound Recording – Krishnamurthy

==Notes==
 

==References==
 
* 
 

==External links==
*  

 

 
 
 
 
 
 