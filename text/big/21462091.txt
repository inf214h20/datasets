Last Train to Mahakali
{{Infobox film
| director = Anurag Kashyap
| producer = Manish Tiwari
| writer = Anurag Kashyap
| starring = Kay Kay Menon Nivedita Bhattacharya Vijay Maurya Lovleen Mishra Shrivallabh Vyas
| editing = Aarti Bajaj
| cinematography = Natarajan Subramaniam ("Nutty")
| music = Shaarang Dev
| released = 1999 (India)
| runtime = 45 min. Hindi
| Star Screen Special Jury Award INR 200,000
}}
 Indian film Special Jury Award at the 8th Annual Star Screen Awards.    In 2005, the film was one of the four short films screened at an event organized by Films Anonymous at Hyderabad. 

The film tells the story of a doctor on death row who claims to have discovered a permanent cure for any virus-based disease and stars Kay Kay Menon and Nivedita Bhattacharya.

==Production==
Anurag Kashyap made Last Train to Mahakali in a moment of disenchantment. Kashyaps brother - Abhinav - was doing a television show and he used Kashyaps name to promote it. He advised Kashyap to contact television channel   - which showcased the work of young-and-upcoming writers and directors. Kashyap was a brand because he had written the scripts for three Ramgopal Varma films including the blockbuster Satya (film)|Satya. So they agreed immediately to the idea. Kashyap was to make a short film for Bestsellers.

Kashyap contacted cinematographer Natarajan (Nutty); he had liked Nuttys work in the music videos of band Euphoria (Indian band)|Euphoria. And they shot the entire film over a period of three-four days. The shooting inside a train was done using a DV camera that Kashyap borrowed from a friend who was working as a producer at Channel V, after promising him a role in the film. 

==Reception== Special Jury Award at the 8th Annual Star Screen Awards. 

==References==
 

 

 
 
 
 
 
 