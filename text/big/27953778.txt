Dream Home
 
 
 
{{Infobox film name           = Dream Home image          = Dream Home poster.jpg
| film name = {{Film name|traditional=維多利亞壹號
|simplified=维多利亚壹号}} caption        = Theatrical poster director       = Pang Ho-cheung producer       = Pang Ho-cheung Conroy Chan Josie Ho Subi Liang writer         = Pang Ho-cheung Derek Tsang Jimmy Wan starring       = Josie Ho Eason Chan Derek Tsang Lawrence Chou Juno Mak Michelle Ye Norman Chui Wong Ching music          = Gabriele Roberto cinematography = Yu Lik-wai editing        = Wenders Li studio         =  distributor    =  released       =   runtime        = 96 minutes country        = Hong Kong language       = Cantonese  budget         = 
}}
Dream Home (維多利亞壹號 Wai dor lei ah yut ho, literally Victoria No. 1)   is a 2010 Hong Kong slasher film directed and co-written by Pang Ho-cheung.  The film is the story of Cheng Lai-sheung (Josie Ho) who saves up money to buy her dream home. After the sellers decide to turn her down, she goes into a murderous frenzy.

Dream Home was originally meant to be released in October 2009 in Hong Kong but due to legal disputes between 852 Films and the director the film premiered in Italy on 23 April 2010 and in Hong Kong on 13 May. The film received mixed reviews which focused on whether or not the satirical and horrific scenes worked well together.

==Plot summary==
In Hong Kong, Cheng Lai-sheung (Josie Ho) works two jobs with the hope of earning enough money to buy her own apartment with a view of the Victoria Harbour.     In mixed chronological order, we see scenes from Lai-sheungs past. In her childhood, her family and friends are evicted from their low-rent housing so that developers can build expensive flats. Later in life, she vows to buy her mother and father a new apartment, but is unable to fulfill her promise before her mother dies. When her father becomes ill, she begins searching in earnest for a new place, having an obsession for the Victoria Bay No. 1 high-rise address from a childhood vow that she would one day buy a flat near the harbour so that her uncle would not have to walk there and back on a daily basis and not needing to search for him every evening.

The bank will only give her a 70% mortgage and payments would reach over $15,000. Unfortunately, because of an oversight in declaring her fathers medical history, she no longer has insurance to pay for his expensive treatment and has to take a second job. After Lai-sheung does save enough for a down-payment, her fathers medical bills become excessive. When she asks her married lover for a loan to cover these, he refuses.  One night her father has trouble breathing and, instead of giving him his oxygen, Lai-sheung allows him to die. The insurance payment now adds enough money to her current savings to purchase her dream flat.

On her way to finalise the purchase, there is a hike in the stock market that makes the owners decide to raise the price. This sends Lai-sheung into a frenzy where she goes to the flats and attacks people who live and work there, killing them with low-tech, household items. During the course of the final killings, the police arrive at the flat, demanding entry. A struggle ensues during which both officers are killed.

Returning to her day job, Lai-sheung receives a call from her agent saying the owners of the flat she wishes to buy are willing to sell after all. Lai-sheung suggests that they might want to sell for a lower price, since there were 11 murders in the building the previous evening. That night, Lai-sheungs lover comes round to pick her up, but she turns her back on him and walks away, ending their relationship.

==Production==
Dream Home was the first feature production from Josie Hos film financing and production studio 852 Films.      {{cite news
 | title = Its a Madhouse
 | first = Lee
 | last = Wing-sze
 | url = http://www.dreamhome.asia/images/dhnews-scmp-directorscut.jpg
 | format = jpg
 | agency = 
 | newspaper = South China Morning Post
 | date = 7 May 2010
 | page = C5
 | accessdate = 23 September 2010
 | archiveurl = http://www.webcitation.org/5sxVf7QVx
 | archivedate = 23 September 2010
 }}  Josie did research and found that horror films were the best selling genre for films which led to the idea of producing one. She was also influenced by the Hong Kong action film The Story of Riki proclaiming that she had "never seen such an outrageous film from Hong Kong before" and thought that "if that could be done, why cant we do the same"?    

Director and writer Pang Ho-cheung worked on the script in 2007.  Pang stated he wanted to write a film about the average Hong Kong citizen facing the local inflated property market.  Pang searched Hong Kong to make sure that no property name matched the one in his film stating that "otherwise, wed be in real trouble".  He chose to make the story a slasher film saying that he had always loved Hollywood b-movies.  The film was set to start filming 27 March 2009 in Hong Kongs Causeway Bay area.      Pang believed that music and lighting were important in slasher films and hired Wong Kar-wais regular light man Wong Chi-ming and Italian composer Gabriele Roberto.  Roberto had previously worked with Pang on his film Exodus (2007 Hong Kong film)|Exodus.   The film includes documentary footage of protesters against the demolition of Queens Pier in 2007. 

Dream Home finished filming around May 2009.  It was originally set to be released in October 2009 in Hong Kong.  The film was delayed due to legal disputes between the company and director Pang Ho-cheung. Despite the dispute, Pang continued to work on post-production on the film stat that he had not "received any message telling me to stop working on it".  Josie Ho stated that she pushed Pang to make the film more violent and outrageous while Pang wanted the film to be "more violent in a realistic manner".  Dream Homes worldwide rights were bought by Fortissimo Films. 

==Release==
Dream Home had its world premiere at the Far East Film Festival on 23 April 2010 in Udine, Italy.     Two audience members vomited and one fainted during the Udine premiere.    The North American premiere was at the Tribeca Film Festival in 2010.  On 25 July 2010, the film had its Canadian premiere at the Fantasia Festival where the film won the Bronze audience award for "Best Asian Film".     The film premiered in the United Kingdom on 28 August 2010 at the London FrightFest Film Festival. 

The film was released in Hong Kong on 13 May 2010.  The film debuted at fourth place at Hong Kong box office earning $171,351 on its first week.  The film ran for six weeks in Hong Kong grossing $378,650 in total.  Variety suggested that the mid-May opening date was "overshadowed by the socko B.O. of Ip Man 2".  Dream Home was shown at the Sitges Film Festival where it won awards for Best Make Up FX (Vitaya Deerattakul and Andrew Lin) and Best Actress (Josie Ho).  Dream Home is set for release in North America by the Independent Film Channel who will release the film theatrically and on video-on-demand in early 2011.  In the United Kingdom, Network DVD have scheduled the film for a Region 2 PAL DVD release on 28 March 2011. 

==Reception== Montreal Gazette wrote that the film was "really well done but also really gory".  Screen Daily gave the film a positive review, stating that "with its clever packaging and slasher credentials, the film will be grossing out viewers on the festival circuit and home video markets for some time to come" and that "Josie Ho in the lead role is focused but a bit wooden...Nonetheless Pangs direction is tight and cinematography by Yu Lik-wai is unobtrusively effective."  In New Zealand, 3 News gave the film five stars, calling it "a classy, clever film with an interesting story that is a darkly comic take on Hong Kong’s ever increasing property prices". 
 Time Out gave the film a three out of six rating, opining that it is "Rendered with tremendous style but with little narrative intrigue, its a shame that the macabre humour gracing Dream Homes (literally) eye-popping extreme violence doesn’t extend to its portrayal of Cheng’s hardship."  Kim Newman in Empire (magazine)|Empire gave the film three stars out of five stating that "theres an undeniable sense of liberation in the killing frenzy, though some scenes (the asphyxiation of a pregnant woman using a vacuum cleaner attachment) will cross a line for many." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 