A Man About the House (film)
{{Infobox film
| name           = A Man About the House
| image          = Manaboutthehouse1947.jpg
| caption        = UK release poster
| director       = Leslie Arliss Edward Black
| writer         = Francis Brett Young J.B. Williams
| starring       = Dulcie Gray Margaret Johnston Kieron Moore Guy Middleton
| music          = Nicholas Brodszky
| cinematography = Georges Périnal Russell Lloyd
| distributor    = British Lion Films
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = £187,115 (UK) 
}}
 Edward Black Russell Lloyd, with cinematography by Georges Périnal and music by Nicholas Brodszky.

==Plot==
Two impecunious English sisters, Ellen and Agnes Isit (Dulcie Gray and Margaret Johnston), unexpectedly inherit a Neapolitan villa from a deceased uncle and move to Italy to view and sell their property. A local man, Salvatore (Kieron Moore), has since a boy been employed by the deceased uncle becoming major domo and he now manages the villa and its vineyard. Exploring her late uncles studio, Ellen uncovers a painting of a nude Salvatore as Bacchus. Soon Ellen becomes drawn to the carefree life of the locals and the romantic charisma of Salvatore, while the prudish Agnes resists. During the raucous revelry of the grape-treading festival, Agnes succumbs to her suppressed desire. Rushing to the balcony she cries out for Salvatore who drops Ellen and climbs from the grape vat and to her bed.  The pair are quickly married, and husband Salvatore now is master of the estate. Soon, Ellen becomes aware of a change in Salvatores behaviour towards Agnes.  Not long after the marriage, Agnes health begins to deteriorate and Ellens suspicions are aroused.  She expresses her concerns to a visiting English doctor, Benjamin Dench (Guy Middleton) who is Agness former fiance. Ellen is convinced that Agnes is being poisoned.  She enlists Denchs help in trying to prove that Salvatore is slowly murdering her sister with arsenic.  The villa once belonged to Salvatores family and he has long been determined to regain ownership. Having poisoned his employer to inherit he had not anticipated the sisters arrival on the scene. The film culminates in a clifftop struggle between Salvatore and Dench, who beats Salvatore and tells him to leave at once or face the consequences. Ellen and Dench return to the villa to tend the sickened and weak Agnes. Suddenly they learn that Salvatore is dead. His body is borne from the bay by villagers, having cast himself from the clifftop in despair rather than lose his family property. Ellen and Dench, who have fallen in love, depart together and leave the recovered Agnes who is determined to remain at the villa and to fulfil her dead husbands wishes restoring the vineyards.

==Cast==
* Dulcie Gray as Ellen Isit
* Margaret Johnston as Agnes Isit
* Kieron Moore as Salvatore 
* Guy Middleton as Dr. Benjamin Dench
* Felix Aylmer as Richard Sanctuary
* Lilian Braithwaite as Mrs. Armitage
* Reginald Purdell as Higgs
* Wilfred Caithness as Solicitor
* Gina Lollobrigida as a young girl

==References==
 
TimeOut Film Guide - published by Penguin Books - ISBN 0-14-029395-7

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 