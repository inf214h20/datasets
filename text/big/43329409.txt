Credence (film)
 
{{Infobox film
| name           = Credence
| image          = 
| alt            = 
| caption        = 
| director       = Mike Buonaiuto
| producer       =Mike Buonaiuto 
| writer         =Mike Buonaiuto 
| screenplay     = 
| story          = 
| based on       =  
| starring       = {{Plain list|
* Alex Hammond
* Anthony Topham
* Tia Kenny
}}
| narrator       = 
| music          = {{Plain list|
* James Maloney  (Score) 
}}
| cinematography =Leighton Cox
| editing        = 
| studio         = 
| distributor    =Shape History 
| released       =  
| runtime        = 
| country        = UK
| language       = English
| budget         = 
| gross          = 
}} British Sci-fi film written and directed by Mike Buonaiuto. It stars Alex Hammond, Anthony Topham and Tia Kenny.  Credence is the story of a family of two torn gay fathers that sacrifice their lives to save their daughter in the last evacuation from the Earth, before being devastated by violent storms. 
 Prince Charles CInema.

== Plot ==
Credence tells the story of a family torn apart during the last evacuation on earth after violent storms have made survival impossible. Hope has been found in the form of new worlds that support human life, however due to limited rocket capacity and life expectancy only children are permitted to evacuate, and even then only the rich have ended up getting tickets. Following two fathers’ decision to make the ultimate sacrifice to give up all their possessions to ensure the survival of their daughter, and the entire human race.

==Production==
The preview production for the film had begun&nbsp;in the beginning of 2014.  The&nbsp;Director describes the film concept as “a universal message that in any kind of tragedy its important that couples come together and support one another,”.    The film was shot in an beach-house on the coast of Norfolk (England). In July 13, 2014 a crowdfunding campaign was&nbsp;launched&nbsp;on Indiegogo to support film production.     The campaign raised the goal of £6,000 in three days and in its ending, raised the amount of £22,189 (37.089,58 US Dollar). The short film begun to be shot on January 8, 2015 and was finished on January 12.

== Cast == Alex Hammond&nbsp;as Scott
 Anthony Topham&nbsp;as John
* Tia Kenny as the daughter

== Soundtrack ==
The soundtack feature songs composed by James Maloney and Craig Sutherland. 

==References==
 

== External links ==
*  

 
 
 
 
 

 
 
 