Campfire Tales
 {{Infobox film
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = William Cooke Paul Talbot
| producer       = Paul Talbot
| writer         = William Cooke Paul Talbot
| starring       = 
| music          = Kevin Green Stan Lollis
| cinematography = 
| editing        = William Cooke Paul Talbot Roger Thomas
| studio         = 
| distributor    = 
| released       =January 1, 1991
| runtime        = 88 min.
| country        =   USA
| language       = English
| budget         = 
| gross          = 
}}
 anthology about a group of teenagers telling ghost stories around a campfire. One of the storytellers is horror legend Gunnar Hansen. The movie also uses many elements from famous horror stories and directors (Lucio Fulci specifically).

==Plot==
The first story involves a young couple returning home after hearing on the radio that a murderer with a hook on his right hand has escaped from the local insane asylum and is terrorizing the countryside. Upon returning home, the girlfriend discovers that her parents have been Decapitation|decapitated. As she runs to get help from her boyfriend she discovers that he has also fallen victim to the hook. A battle ensues and she comes out victorious, killing the escaped prisoner with his own hook.

The second story involves two stoners searching for  .

The third story has to do with a greedy, selfish man returning home to his mother for Christmas. He kills her for the inheritance, pushing her down the stairs leading into the basement. He leaves the body and heads over to his brothers house to watch his two children while he and his wife leave for dinner. The children tell him a tale about an evil Santa Claus known as "Satan Claus" who comes and punishes those who do evil things throughout the year. He leaves to head back to his mothers house, plotting what he is going to tell the police and his brother. Upon returning he is attacked by Satan Claus, who rips his heart out.

The fourth story and final story is about a shipwrecked pirate on a desolate island. He discovers a man who warns him about buried treasure on the island being guarded by zombies. The pirate kills the man and goes in search for the treasure, ignoring the mans warnings. He discovers the treasure only to be attacked by a large group of pirate zombies. After running from them for some time they eventually catch and kill him.

The wrap around story involves the young men going to sleep with the narrator revealing a hook on his right hand.

==Re-release==
The film was re-released on DVD in 2002 from Sub Rosa.

==External links==
* 
* 

 
 
 
 
 