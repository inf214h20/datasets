A Small Act
 
{{Infobox film
| name           = A Small Act
| image          = A Small Act.jpg
| alt            =  
| caption        = 
| director       = Jennifer Arnold
| producer       = Jennifer Arnold Patti Lee Jeffrey Soros Joan Huang
| writer         = 
| screenplay     = 
| story          = 
| starring       =  Joel Goodman
| cinematography = Patti Lee
| editing        = Carl Pfirman Tyler Hubby
| studio         = 
| distributor    = New Video Group
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Small Act is a documentary film produced by HBO. The documentary chronicles Chris Mburus search for his benefactor, whose sponsorship allowed him to continue secondary school in Kenya. He would go on to earn degrees from the University of Nairobi and Harvard Law School.

==Plot==
Chris Mburu is a United Nations human rights advocate. As a child in Kenya, he was a good student, but would not have been able to enter secondary school, as only Primary School is free of cost in Kenya. Due to his outstanding grades, Chris was selected for a direct scholarship, provided to him by a Swedish woman named Hilde Back, who had fled Nazi Germany because of her Jewish heritage.

In the present day, Mburu has decided to create a scholarship program of his own, and names it after his former benefactor, Hilde Back. In Kenya, Three gifted students, Kimani, Ruth, and Caroline are vying for a scholarship that may be their only chance to continue school. Kimani, Ruth and Caroline take the Kenya Certificate of Primary Education Test, a nationwide standard that will determine their eligibility for Mburus scholarship program. However, the violence and mayhem of the 2007–2008 Kenyan crisis may jeopardize the students dreams.

Due to a lack of students meeting the predetermined threshold on the tests, Kimani is given a scholarship.  Unfortunately, Ruth and Caroline do not receive scholarships, but it is revealed in the closing credits that they have been sponsored to continue schooling by the documentary film crew.

==External links==
*  
*  
*   at HBO

 
 
 
 
 


 