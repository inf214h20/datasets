Should a Girl Propose?
 
{{Infobox film
| name           = Should a Girl Propose?
| image          = 
| image_size     =
| caption        = 
| director       = P.J. Ramster
| producer       = P.J. Ramster
| writer         = P.J. Ramster
| based on       = 
| narrator       =
| starring       = Cecil Pawley
| music          =
| cinematography = Jack Fletcher
| editing        = 
| studio = P.J. Ramster Photoplays
| distributor    = 
| released       = 24 April 1926
| runtime        = 4,000 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Should a Girl Propose? is a 1925 Australian silent film directed by P.J. Ramster set in the high society world of Sydney. The film featured several players from Ramsters acting school.    

It is considered a lost film.

==Cast==
*Cecil Pawley as Ellis Swift
*Thelma Newling as Esma
*Rex Simpson
*Joy Wood
*Norma Wood

==Reception==
The film critic from the Sydney Morning Herald stated that:
 To say that it is among the best of the Australian pictures presented within the last year or two is, unfortunately, not recommending it very highly. Providing one sees it in a good-humoured-frame of mind, there is mild entertainment, in it. One particularly satisfactory feature is the acting...  

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 

 
 
 
 
 
 


 