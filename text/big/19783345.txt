The Wild Man of the Navidad
{{Infobox film
| name           = The Wild Man of the Navidad
| caption        = Original one sheet poster
| director       = Duane Graves Justin Meeks
| producer       = Kim Henkel Justin Meeks Duane Graves
| writer         = Duane Graves Justin Meeks
| starring       = Justin Meeks Alex Garcia Tony Wolford Charlie Hurtin Bob Wood Edmond Geyer Mac McBride James Bargsley Stacy Meeks Patrick Hewlett
| music          = James McCrea James Brand Charlie Hurtin Marshall Jones David Turnbow Ben Buchanan Kevin Heuer Karl Rehn
| cinematography = Duane Graves
| editing        = Duane Graves Justin Meeks
| distributor    = IFC Films (North America)
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
}}
The Wild Man of the Navidad is a 2008 horror film picked up by IFC Films shortly after its world premiere at the 2008 Tribeca Film Festival.    

== Plot ==
  the original legend of the Wild Man of the Navidad surfaced back in the late 1800s.   The film follows Dale, his wheelchair-using wife Jean, and her oft-shirtless, lazy-eyed caretaker Mario. Though their ranch sits on vast acres prime for paying hunters, Dale has resisted opening up the land because of the strange, Bigfoot-like creatures supposedly inhabiting it. But after the prodding of some of the rifle-loving townsfolk and the loss of his welding job, Dale gives in and opens the gate to his compound. Then the hunters become the hunted. 

== Cast ==
* Justin Meeks as Dale S. Rogers
* Alex Garcia as Mario Jalisco
* Tony Wolford as The Wild Man
* Stacy Meeks as Jean Rogers
* Bob Wood as Earl Smith
* Edmond Geyer as Sheriff Lyle Pierce
* Mac McBride as Boss Man Jack
* Charlie Hurtin as Karl Crabtree
* Patrick Hewlett as Vern Findlay
* James Bargsley as Melvin Pennell

== Production ==
=== Background ===
The film is a throwback to drive-in Sasquatch classics of the 1970s like The Legend of Boggy Creek, and is known for its recreation of their specific vintage style, pacing and feel—from the real-life characters down to the period production design and music.   

The film was co-produced by Kim Henkel, co-writer of the original The Texas Chain Saw Massacre with Tobe Hooper in 1974 and was written, directed and edited by two of Mr. Henkels screenwriting/production students, Justin Meeks and Duane Graves. 

== Reactions ==
 
Aint It Cool News described it as "about as perfect a Bigfoot film I’ve seen so far."  Notable horror film critic Scott Foy of Dread Central describes the movie as "a creepy, southern-fried creature feature."  IFC Films Alison Willmore states that it is "a welcome palate cleanser" for the horror genre.  Notable film critic Scott Weinberg of Cinematical calls it "enthusiastically splattery...a rather spirited little terror tale...that actually feels like it has been hidden in a vault."  Daily Variety critic John Anderson writes "cult status is already achieved...Wild Man mixes homage with horror for a pretty potent dose of movie moonshine."  Merle Bertrand of Film Threat relates it as "wonderfully retro...its time to go back to the drive-in." 


==References==
 

== External links ==
* 
* 
*  Official Movie Website
*  at Filmmaker Magazine
*  at Time Out New York
* 
* 
* 
* 
* 
*  
*  
*  

 
 
 
 
 
 
 
 
 
  
 
 
 
 