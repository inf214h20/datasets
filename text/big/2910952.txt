The Seventh Victim
{{Infobox film
| name           = The Seventh Victim
| image          = The-Seventh-Victim-poster.jpg
| size           =
| caption        = Theatrical poster
| director       = Mark Robson
| producer       = Val Lewton
| writer         = DeWitt Bodeen Charles ONeal
| starring       = Tom Conway Jean Brooks Isabel Jewell Kim Hunter
| music          = Roy Webb
| cinematography = Nicholas Musuraca
| editing        = John Lockert RKO Radio Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
}}
 horror and devil worshippers in Greenwich Village while searching for her missing sister.

==Plot summary==
Mary (Kim Hunter), a young woman at Miss Highcliffs boarding school, learns that her sister Jacqueline (Brooks), her only relative, has gone missing and has not paid her tuition in months. The school officials tell Mary she can only stay on if she works for the school, to pay her tuition.

Mary decides to leave school to find her sister. She returns to New York City, and finds that her sister had sold her cosmetics business eight months earlier.  She locates the apartment Jacqueline was renting, and finds only a chair and a noose hanging from the ceiling in the otherwise bare apartment. This only makes Mary more anxious and determined to find her. 
 Satanic cult called the Palladists. She was seduced into joining the cult by her former co-workers. Mary enlists a private detective (Lou Lubin) to help in her investigation, but he is stabbed to death under mysterious circumstances. Judd helps her locate Jacqueline, who is hiding from the cult.  Ward falls in love with Mary. Jacqueline is kidnapped by the cult members and condemned to death, because their rules state that anyone who reveals the cult must die. She would be the seventh person condemned under these rules since the founding of the cult (hence the films title). 
 Elizabeth Russell), a young woman with a terminal illness. The neighbor says she’s afraid to die, but she is tired of being afraid and plans a last night out on the town. Jacqueline enters her own apartment and hangs herself. The thud of the chair falling over is heard, but the sick woman does not recognize the sound as she leaves for the evening.

==Cast==
* Kim Hunter as Mary Gibson
* Hugh Beaumont as Gregory Ward
* Tom Conway as Doctor Louis Judd 
* Jean Brooks as Jacqueline Gibson
* Isabel Jewell as Frances Fallon
* Evelyn Brent as Natalie Cortez Elizabeth Russell as Mimi
* Marguerite Sylva as Mrs. Bella Romari (uncredited)

==Critical reputation==
The Seventh Victim has been praised for the shadowy camera work by Musuraca.
The film was initially criticized in reviews for having too many characters and a storyline that doesnt always make sense. (According to the films DVD commentary, scenes containing additional story lines, some that may have made the film clearer, were cut before the films release.) Bosley Crowther, in The New York Times review, thought that the film “might make more sense if it was run backward”.
 minor key. The story goes that Lewton was warned not to make a film with a message, and he replied that this film did have a message: "Death is good."  

Purportedly, homosexual undercurrents run through the film, particularly in Jacquelines character and her relationship with Frances, a cult member who is an employee at the company she formerly owned; hence, the film was featured in Turner Classic Movies Channels Screened Out, which celebrated gay and lesbian themes in classic Hollywood cinema. If true this would be an extremely explicit film given the year it was released (1943). 
 Cat People, filmed the previous year by the same producer, Val Lewton. The Judd character dies in Cat People but is re-used for The Seventh Victim, calling into question the relation of the two fictional narratives. 

Critic Jonathan Rosenbaum has cited this as his favorite horror film. It is the only horror movie on his list of 100 favorite movies. 

==Notes==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 