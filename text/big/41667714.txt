Snehavin Kadhalarkal
 
 
{{Infobox film
| name           = Snehavin Kadhalarkal
| image          s Snehavin Kadhalarkal
| image_size     = 220px
| border         = yes
| alt            = Snehavin Kadhalarkal Poster
| caption        = Snehavin Kadhalarkal Poster
| film name      = Snehavin Kadhalarkal
| director       = Muthuramalincoln
| producer       =  
| writer         = Muthuramalincoln 
| screenplay     =  
| story          = Muthuramalincoln 
| Dialogue       =  
| starring       =  
| music          = R.Prabahar
| cinematography =  
| editing        = Shyjith Kumaran
| co-Director    = Anthony Joseph
| choreography   =  
| stunts         = Jaguar Thangam
| stills         = Jayakumaran
  Caption1
File:Example.jpg|Caption2
 
.
| PRO            = Nikhil Murugan
| studio         = Tamilan Kalaikkoodam
| distributor    = Raghuvaran
| released       =  
| country        = India
| language       = Tamil
}}
 Tamil film Advaitha who plays the leading role of Sneha, with the storyline revolving around the men who come into her life.

==Cast==
 Advaitha (Krithi Shetty) as Sneha.
* Thilak.
* Uday.
* Athif Jey.
* Rathna Kumar.

==Production==
The film was produced by Mr. Ka.Kalaikotudhaiyam and Mrs. Amala Kalaikotudhaiyam on behalf of Tamilan kalaikoodam. 
 

===Development===
The film was initially shot using a 7D digital camera, with a 5D camera also being added later.

===Crew===
* Anand was the Director of Photography.
* Editing carried out by Shyjith Kumaran.
* Script written by Muthuramalincoln.  
* Screenplay then written by Muthuramalincoln and R. Prabahar (Music Composer). 
* Dialogue written by Muthuramalincoln, R. Prabahar (Music Composer), Theesmas and Dhanu S. Prabhakar. 
* Stills by Jayakumaran M. 

===Location===
Filming took place in and around the areas of Chennai, Madurai, Kodaikanal and Coimbatore.

===Music===
The Soundtrack was composed by R.Prabahar (Music Composer).  and consists of five songs which were released on 19 February 2014 at Prasad Studios Lab Theatre, Chennai, by producer Mr. Keyar.
 

===Release===
The film was released on 15 August 2014. 
 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 


 