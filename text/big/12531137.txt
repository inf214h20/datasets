The Dollmaker
 
{{Infobox television film
| name = The Dollmaker
| image = dollmaker.jpg
| caption =
| format = Drama
| creator =
| writer = Harriette Arnow (novel) Susan Cooper Hume Cronyn
| director = Daniel Petrie Bruce Gilbert
| starring = Jane Fonda
| cinematography = Paul Lohmann
| editing = Rita Roland
| music = John Rubinstein
| studio = Finnegan Productions IPC Films ABC
| USA
| network = ABC
| runtime = 150 min.
| first_aired = May 13, 1984
| last_aired =
| num_episodes =
}}
 ABC in novel of the same title, written by Harriette Arnow and originally published in 1954. Director Daniel Petrie won a Directors Guild of America Award.

==Plot summary== Clovis believes that it will bring the family a regular income and better way of living. What Fondas Gertie finds is a new place to exist, rather than live, and the family settles down in a tar paper shack by the railroad tracks in an industrial area.
 Jesus calling to her to carve it from it.

One setback after another begins to pull the family apart.  Gerties husband doesnt find work and begins to get involved with matters that trouble her; her children begin to also get involved in unsavory affairs.

The event that breaks Gerties passivity to her situation is the death of her youngest daughter, who is killed by a railroad car.  She confronts her husband, whose best of intentions has led the family to this tragedy; Gertie decides that she will earn enough money to get the family back home to where it belongs.  To do this she will make dolls, but she has no material from which she can carve the dolls.  It is then that she takes the treasured piece of lumber that she longed to carve the Christ figure from, and splits it (in a slow motion scene) with an axe.  From one piece of wood, she will carve many dolls.  It is the only way to save the family.

From this sacrifice, the family is able to return to their home.

==Cast and crew==
in alphabetical order
* Mike Bacarella ...  Truck Driver
* Etel Billig ...  Mrs. Bomarita
* Nikki Creswell ...  Cassie
* David A Dawson ...  Amos
* Christine Ebersole ...  Miss Vashinski
* Jane Fonda ...  Gertie Nevels
* Derek Googe
* James N. Harrell ...  Old John Miller
* Ann Hearn ...  Max
* Dan Hedaya ...  Skyros
* Levon Helm ...  Clovis
* Dennis Kelly ...  Mr. Daly
* Susan Kingsley ...  Sophronie
* Geraldine Page ...  Mrs. Kendrick
* Amanda Plummer ...  Mamie Robert Swan ...  Victor
* Studs Terkel
* Mike Timoney ...  Army Major
* Starla Whaley ...  Clytie Jason Wild ...  Jean-Claude
* David Brady Wilson ...  Enoch
* Sheb Wooley
* Jason Yearwood ...  Reuben

==External links==
* 
* 

 

 
 
 
 
 
 
 
 