Schiava del peccato
 
{{Infobox film
| name           = Schiava del peccato
| image          = Schiava del peccato.jpg
| caption        = Film poster
| director       = Raffaello Matarazzo
| producer       = 
| writer         = Oreste Biancoli Aldo De Benedetti
| starring       = Miranda Campa Renzo Rossellini
| cinematography = Elio Polacchi Marco Scarpelli
| editing        = Mario Serandrei
| distributor    = 
| released       = 1954
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Schiava del peccato (Slave to Sin) is a 1954 Italian melodrama film directed by Raffaello Matarazzo.   

==Cast==
* Silvana Pampanini - Mara Gualtieri
* Marcello Mastroianni - Giulio
* Irene Genna  - Dina
* Franco Fabrizi - Carlo
* Renato Vicario - Husband of Dina
* Camillo Pilotto - Inspector
* Liliana Gerace - Elena
* Olinto Cristina - Director Paul Muller - Voyager
* Dina Perbellini - Miss Cesira
* Maria Materzanini - Maria Grazia Sandri
* Maria Grazia Francia
* Irène Galter
* Laura Gore
* Turi Pandolfini
* Loris Gizzi
* Miranda Campa
* Giorgio Capecchi
* Adriana Danieli
* Mirella Di Lauri
* Franca Dominici
* Checco Durante
* Lia Lena
* Maria Grazia Monaci
* Aldo Pini
* Isarco Ravaioli
* Andreina Zani

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 