Kanehsatake: 270 Years of Resistance
 
{{Infobox film
| name           = Kanehsatake: 270 Years of Resistance
| image          = 
| alt            =  
| caption        = 
| director       = Alanis Obomsawin
| producer       = {{Plainlist|
*Wolf Koenig
*Colin Neale}}
| writer         = Alanis Obomsawin
| starring       = Alanis Obomsawin
| music          = {{Plainlist|
*Francis Grandmont
*Claude Vendette}}
| cinematography = {{Plainlist|
*François Brault
*Zoe Dirse}}
| editing        = Yurij Luhovy
| studio         = 
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Kanehsatake: 270 Years of Resistance is a 1993 feature-length film documentary film by Alanis Obomsawin, chronicling the 1990 Oka Crisis.

Produced by the National Film Board of Canada, the film won 18 Canadian and international awards, including the Distinguished Documentary Achievement Award from the International Documentary Association and the CITY TV Award for Best Canadian Feature Film from the Toronto Festival of Festivals. 

==Release==
Rejected by Mark Starowicz of CBC Television, the film premiered in England, instead, on Channel Four. Kanehsatake: 270 Years of Resistance made its North American premiere at the Toronto Festival of Festivals. 

In his book, Alanis Obomsawin: The Vision of a Native Filmmaker, film scholar Randolph Lewis writes:

 

Brian McIlroy, in his chapter on Kanehsatake: 270 Years of Resistance for the book The Cinema of Canada, states that:

 

==Historical significance==
In every narrative there is a narrator, a voice, an author. Unfortunately, this voice is never truly objective. There is always some underlying bias, agenda, or philosophy guiding the perception of the narrator. As history itself is a narrative, the viewpoint of the narrator can have a profound effect on history and its meanings.  This has a particular impact on those who traditionally have not had a voice. One such group that has historically been lacking mainstream representation is the American Indian.
The history and traditions of American Indian peoples have, for the most part, been passed down through oral traditions. This leaves these peoples with little written record. On top of this, indigenous peoples in both the United States and Canada have been subject to a great deal of persecution. From being stripped of their traditional homelands and sacred sites to being subjected to forced assimilation through boarding schools, American Indian peoples have been unable to represent themselves, to share their stories with the mainstream. 
Instead of being able to represent themselves, indigenous people have historically been represented to the western world by outsiders.  Outsiders such as anthropologists, ethnographers, and others who often emerge from the very people and social viewpoint that have benefitted from the oppression of indigenous peoples.  This has led to ethnographic documentaries that, in their attempt to explore another culture, end up misunderstanding and even exploiting native cultures. This tends to continue the mainstream narrative of indigenous inferiority. 
It is in this context that Kanehsatake: 270 Years of Resistance is worth noting in the history of documentary. This film is remarkable because of the intent of the director, an indigenous woman who approaches documentary in order to explain more deeply the indigenous experience from an indigenous perspective. In Alanis Obomsawin: Vision of a Native Filmmaker, the author explains that the goal of all Obomsawin’s work “whether in singing or storytelling or filmmaking-has been a fight for inclusion of our history”.

In regard to the Oka issue, this goal of getting the indigenous narrative out into the mainstream was of vital importance. The media landscape concerning the crisis had profound slant against the indigenous activists and in support of the Canadian government response. According to Lewis, new coverage at the time “often relied on government press releases and other one-sided sources.” (91). This, combined with the ridiculous attempt by the CBC to avoid airing Kanehsatake: 270 Years of Resistance, shows that not only did the media not care to get the full story but that the powers of the time desired to construct a narrative where the Canadian government was the hero in order to maintain the status quo. If it had not been for the work of Obomsawin and other like her, it is likely that the Canadian government would have succeeded in having their narrative be the only narrative.
It is in this way that Alanis Obomsawin, through Kanehsatake: 270 Year of Resistance, avoided having the Oka Crisis become yet another lost indigenous historical narrative. In her article “Redressing Invisibility”, Beth Mauldin points out that documentary film has become “a modern-day extension of the Abenaki oral tradition” and a way of passing down history, beliefs, and culture to the future. It is for this reason, the taking of historical self-license for the benefit of the indigenous peoples of Canada, that the works of Obomsawin and particularly Kanehsatake: 270 Years of Resistance is worth remembering in the history of documentary film.

==Production conditions==
The National Film Board of Canada (NFB), created in 1939 by an act of parliament, (SOURCE: NFB ARTICLE) has supported much of Obomsawin’s work and provided financial backing for Kanehsatake: 270 Years of Resistance. The NFB reports to the Minister of Canadian Heritage under the authority of the Parliament of Canada. The agency, mandated to interpret Canada to Canadians and other nations globally, has produced and distributed over 13,000 documentary, animation, alternative drama and digital media productions.
According to the “Alanis Obomsawin: The Vision of a Native Filmmaker”, Obomsawin was in the middle of work on Le Patro, a short film about a Montreal community center, when she heard news of the Oka crisis. She dropped work on the project and went straight to the NFB with her plan to join the encampment and begin filming. The NFB gave her “a cameraman and an assistant, doing sound herself until another crew member joined her in the warriors’ camp” (page 92). 
	(93) For the next seventy-eight days, Obomsawin and her crew endured “near battle conditions” and other living discomforts. According to Lewis, the NFB repeatedly pleaded with Obomsawin to leave the encampment to protect her own safety and that of her crew. The encampment, even as depicted in the film, was a warzone with access to essentials like food and medical treatment being cut off by the Canadian government. Filming in these conditions was very taxing on both Obomsawin and her crew and the fact that they stayed is a testament to the commitment the filmmaker had to her work.
	The task of shaping a narrative out of the “over 250 hours of sixteen-millimeter film” began in the fall of 1991 (95). According the Yurij Luhovy, who edited the film, “It was a huge project. Just to give you an idea of the magnitude, it took me six months just to view the raw footage and mark the best elements of the film.”(95) Alanis Obomsawin was screenwriter, director and narrator of the award winning documentary. 
	The NFB has extensive distribution channels. The board releases its productions to libraries and universities throughout the Canadian provinces and territories, schedules theatre screenings through its centers in Toronto and Montreal as well as the Aboriginal Peoples Television Network (APTN). The documentary is easily accessible for online streaming at the NFB.ca Screening Room as well as through other leading digital media video portals like YouTube, smartphone apps, Facebook and Twitter (However, many of these digital medium were not available when the film debuted).  According to Lewis, this model of distribution is “a worthy goal in Obomsawin’s mind” as the primary goal in all her work is to “fight for inclusion of our history in the educational system”.

==See also==
* Ellen Gabriel

==References==
 

==External links==
*  
* , Watch on-line at NFB.ca
* , Canadian Film Encyclopedia
* , Canadian Women Film Directors Database
* , on archive.org

 
 
 
 
 
 
 
 
 
 
 