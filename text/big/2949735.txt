Angels in the Outfield (1951 film)
{{Infobox film
| name           = Angels in the Outfield
| image          = Angels in the Outfield Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Clarence Brown
| producer       = Clarence Brown
| screenplay     = {{Plainlist|
* Dorothy Kingsley George Wells
}}
| story          = Richard Conlin
| starring       = {{Plainlist| Paul Douglas
* Janet Leigh
}}
| music          = Daniele Amfitheatrof
| cinematography = Paul Vogel
| editing        = Robert Kern
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Cineplex Entertainment|Loews Inc.
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $1,081,000  . 
| gross          = $1,666,000 
}}
 Paul Douglas and Janet Leigh. Based on a story by Richard Conlin, the film is about a young woman reporter who blames the Pittsburgh Pirates losing streak on their abusive manager, who begins hearing the voice of an angel promising to help the team if he changes his ways. The film was released by Metro-Goldwyn-Mayer on October 19, 1951.

==Plot==
With baseballs Pittsburgh Pirates in last place, their combative, foul-mouthed manager Guffy McGovern has plenty to complain about. All this changes when, while wandering through Forbes Field in search of his good luck charm one night, Guffy is accosted by the voice of an angel (voice of James Whitmore), who hints at having been a ballplayer during his earthly life.

As the spokes-angel for the Heavenly Choir Nine, a celestial team of deceased ballplayers, he begins bestowing "miracles" upon the Pirates—but only on the condition that McGovern put a moratorium on swearing and fighting.

With the help of the invisible ghosts of past baseball greats, the Pirates make it into the pennant race. During a game, 8-year-old orphan Bridget White insists that she can see the angels helping out the "live" ballplayers—understandably so, since it was Bridgets prayers to the Archangel Gabriel that prompted the angel to visit McGovern in the first place.
 beaned during a game and himself confirms Bridgets claims, he falls into the hands of vengeful sportscaster Fred Bayles, who has been scheming to have McGovern thrown out of baseball and persuades the Commissioner of Baseball to investigate McGoverns fitness as a manager.

Complication piles upon complication until the National League pennant|pennant-deciding game, wherein Guffy is forced to rely exclusively upon the talents of his ballplayers—notably "over the hill" pitcher Saul Hellman (who, the angel has told Guffy, will be "signed up" by the Heavenly Choir team shortly). Guffy also wins over Jennifer, and they plan to adopt young Bridget.

The angels themselves are never actually seen by the viewing audience, just the effects of their presence—a feather dropping, or someone being jostled from time to time. The angel who talks to Guffy never reveals who he was in life. It being a time when profanity was never used in films, the "swearing" uttered by Guffy is audio gibberish, scrambled recordings of his own voice.

==Cast== Paul Douglas as Aloysius X. Guffy McGovern
* Janet Leigh as Jennifer Paige
* Keenan Wynn as Fred Bayles
* Donna Corcoran as Bridget White
* Lewis Stone as Commissioner of Baseball Arnold P. Hapgood
* Spring Byington as Sister Edwitha
* Bruce Bennett as Saul Hellman
* Marvin Kaplan as Timothy Durney
* Ellen Corby as Sister Veronica Jeff Richards as Dave Rothberg
* John Gallaudet as Reynolds
* King Donovan as McGee
* Don Haggerty as Rube Ronson
* Paul Salata as Tony Minelli Fred Graham as Chunk
* John McKee as Bill Baxter
* Patrick J. Molyneaux as Patrick J. Finley
Corcoran, Kaplan and Salata are the last surviving cast members.

==Cameo appearances==
* Bing Crosby has a short cameo in the film, playing golf, sinking a long putt. At that time, Crosby was a part owner (approximately 15%) of the Pirates.
* Baseball greats Ty Cobb and Joe DiMaggio, along with Hollywood songwriter Harry Ruby, are "interviewed" regarding the angels.
* Barbara Billingsley has an uncredited role as a hat check girl at Johns Steakhouse.

==Locations==
The film contains extensive baseball action shots, most of which were filmed at Forbes Field,  the former home of the Pittsburgh Pirates and Pittsburgh Steelers|Steelers, demolished in 1971, the year after the Pirates and Steelers moved to Three Rivers Stadium. The opening credits acknowledge "the kind cooperation of the Pittsburgh Pirates for the use of the team and its ballpark," while reminding the viewer that the story is fictional and "could be any baseball team, in any league, in any town in America."

Historians may note several distinguishing features of Forbes Field at the time. One is the "Kiners Korner" inner fence in left field, with the 365-feet left field foul line marker observable on the outer wall, and the 335-feet sign on the inner fence. The other distance markers (376-457-436-375-300) are visible in some scenes. Other objects on the field of play at Forbes are visible from time to time, including the flagpole and batting cage near the 457 foot marker in deep left center field, and the Barney Dreyfuss monument in straightaway center field.  The University of Pittsburghs Cathedral of Learning is prominent in many shots filmed in Forbes Field.
 Wrigley Field, Cubs Park. While Wrigleys ivy-covered outfield wall stands in nicely for that of Forbes Field, "Kiners Korner" is conspicuous in its absence, and visible distance markers (412 feet in centre field; 345 feet in left) are inconsistent with Forbes Fields grander dimensions.

Some stock footage alleged to be the Polo Grounds in New York City was actually Comiskey Park in Chicago, as evidenced by a quick glimpse of an auxiliary scoreboard reading "Visitors" and "White Sox".

==Reception==
According to MGM records the film made $1,466,000 in the US and Canada and $200,000 elsewhere, resulting in a loss of $171,000. 

==See also==
* Angels in the Outfield (1994 film)

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 