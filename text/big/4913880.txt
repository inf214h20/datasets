The Safety of Objects
{{Infobox film
| name           = The Safety of Objects
| image          = Safety of objects.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Rose Troche
| producer       = Dorothy Berwin
| writer         = Rose Troche A.M. Homes
| narrator       = 
| starring       = Glenn Close Dermot Mulroney Jessica Campbell Patricia Clarkson Joshua Jackson Moira Kelly Robert Klein Timothy Olyphant Mary Kay Place Charlotte Arnold Kristen Stewart
| music          = Barb Morrison Charles Nieland Nance Nieland
| cinematography = Enrique Chediak
| editing        = Geraldine Peroni
| distributor    = IFC Films
| released       =  
| runtime        = 121 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Safety of Objects is a 2001 independent film based upon a collection of short stories of the same name, written by A. M. Homes and published in 1990. It features four suburban families who find that their lives become intertwined. The film was directed by Rose Troche, who co-wrote the screenplay with Homes. It is often considered an "intellectual film"; it touches upon many deep issues of the human experience in life. There are about 15 major characters in the film. 

Perhaps most notable is the character Esther Gold, played by Glenn Close. She is the mother of several children, including a son in a coma from a car accident. The other characters are related to the accident either directly or indirectly. As the films story continues, the audience learns that all of the characters are connected in ways that they never knew.

==Plot summary==
In a suburban neighborhood, Paul Gold (Joshua Jackson) lies in his bedroom in a coma, caused by a traumatic car accident. He is cared for by his mother, Esther (Glenn Close) who in tending him closely has distanced herself from her husband Howard (Robert Klein) and teenage daughter Julie (Jessica Campbell). Trying to elicit her mothers attention, Julie enters Esther in a local radio contest, in hopes of winning a brand new car. 

Meanwhile, after years of putting his job first, Jim Train (Dermot Mulroney) feels his family, especially his efficient wife Susan (Moira Kelly), no longer needs him. He tries to reconnect with his son Jake (Alex House), on the edge of puberty, but the youth is preoccupied with romantic fantasies about his younger sisters twelve-inch plastic doll. After Jim is skipped over for a promotion, he stops going to work, claiming that a bomb threat was called into his office. Feeling unappreciated by his family, he convinces Esther and Julie to let him help them win the car.

The Trains neighbor, Helen Christianson (Mary Kay Place), feeling older and less desirable, tries new products to keep her feeling young. She succeeds only in alienating her husband, who loves her as she is. 

Helens good friend, Annette Jennings (Patricia Clarkson), in the midst of a messy divorce, struggles to provide financially for her two daughters. Sam (Kristen Stewart), the older tomboyish daughter, is desperate to go off to camp that summer. Sams younger sister suffers from mental disabilities and requires special schooling, which her father, Annettes ex-husband, refuses to pay for. Annette is also mourning the loss of Paul, with whom she was having a relationship. Randy (Timothy Olyphant), the neighborhoods landscaper, is coping (poorly) with his own younger brothers death in the same car accident that grievously injured Paul Gold.

Annettes ex-husband comes to visit his daughters. He says that he wants to take Sam on a holiday. Annette refuses because Sam isnt interested in spending time with her father, and her ex-husband does not want to care for the younger daughter. Sam overhears their argument and runs away from her father when he tries to talk to her at the park. Soon she bumps into Randy, who tells her that her mom instructed him to pick her up. 

Randy takes Sam to a remote cabin in the woods and keeps her there, not allowing her to call home. He calls her Johnny, after his late brother. After what appears to be three days, Randy starts driving back to the suburb, to recreate the night when the characters lives intersected. When the beer he asks Sam to hand him doesnt explode, he appears to realize that the person in the back seat is Sam, not his brother.
 mall security guard. Esther, who finally realizes how much she has neglected her daughter, goes home and tearfully suffocates her son. Jim returns to his home, and Randy lets Sam go home. Helen almost cheats on her husband, but eventually returns home without having taken that step. 

A flashback reveals the cause of the car crash: Randy, Paul, and Randys younger brother Johnny were traveling in a car after a gig that Pauls band played. Johnny gave Randy and Paul beers which he had shaken, and they exploded when opened, on Paul who was driving. Another car, carrying Julie and Bobby, came from the opposite direction, speeding to rush Julie home after an impromptu tryst so she would not get in trouble for violating her curfew. Both drivers became distracted, both cars had to swerve, and Pauls car crashed and flipped over. The guilt that has consumed both Randy and Julie throughout the film, is shown to have originated from each taking blame for the crash. 

The final scenes of the movie show the characters acknowledging their heightened understandings of the effects of these events in their lives and their choices, and lastly, the interconnectedness of these families.

==Partial cast==
*Glenn Close as Esther Gold
*Dermot Mulroney as Jim Train
*Jessica Campbell as Julie Gold
*Patricia Clarkson as Annette Jennings
*Joshua Jackson as Paul Gold
*Moira Kelly as Susan Train
*Robert Klein as Howard Gold
*Timothy Olyphant as Randy
*Mary Kay Place as Helen Christianson
*Kristen Stewart as Sam Jennings
*Alex House as Jake Train
*Charlotte Arnold as Sally Christianson
*Aaron Ashmore as Bobby Christianson
*C. David Johnson as Wayne Christianson

==External links==
*  

 

 
 
 
 
 