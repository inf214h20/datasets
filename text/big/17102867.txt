Book of Blood
 
{{Infobox film
| name           = Book of Blood
| image          = Book-hires.jpg
| image size     = 185px
| caption        = Teaser Poster John Harrison
| producer       = {{Plainlist|
* Clive Barker
* Lauri Apelian
* Joe Daley
* Anthony DiBlasi
* Micky McPherson
* Jorge Saralegui
* Nigel Thomas
}}
| based on       =  
| screenplay     =  
| starring       = {{Plainlist|
* Jonas Armstrong
* Sophie Ward
* Doug Bradley
}}
| music          = Guy Farley
| cinematography = Philip Robertson
| editing        = Harry B. Miller III
| studio         =  
| distributor    = Essential Entertainment
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} John Harrison framing stories from Clive Barkers Books of Blood.

==Plot==
A hooded, disfigured young man is eating at a diner, being watched by a stranger. The stranger is Wyburd (Clive Russell), who has been stalking the young man, Simon (Jonas Armstrong). Wyburd convinces Simon to join him in his truck, where Simon passes out and awakens strapped to a table. Wyburd offers him a choice: a slow death, or a quick and clean death by telling the story of the Book of Blood, a series of scars and inscriptions carved on Simon from head to toe. Opting for a clean death, Simon reveals his story.
 Paul Blair) investigate the house to unlock its mysteriously murderous past. Mary encounters Simon McNeal, a seemingly clairvoyant young man to whom she develops an attraction. Simon reluctantly signs on to assist, and the three of them move in. Reg spots a terrifying ghost and dies from a fall. Mary sees Simon is attacked twice by ghosts; the second time, the ghosts carve into Simons flesh with nails and glass shards, and Mary understands: she is the key to opening the way for the ghosts; her powers were what awakened them. She swears to the ghosts that she will tell all of their stories. The ghosts heed her words and depart, allowing Simon to survive the ordeal.

Simon reveals to Wyburd that he was from then on cursed to be the book on which the dead write while Mary wrote books and made millions off of the stories portrayed on him. As she aged, he remained the same youthful appearance, only more scarred with new stories for her to write. He admitted he couldnt take it anymore, so he fled, hence the reason Wyburd was hired to remove his skin. Wyburd, unmoved, lives up to his end of the bargain and kills Simon quickly. After placing his skin neatly into a suitcase, he waits for his payment. Blood suddenly starts pouring from the case, slowly filling the building that Wyburd is trapped in, and he drowns. Mary arrives, and is unfazed by Wyburds body. She opens the suitcase, pulling out Simons intact skin and smiling, as she begins to read the stories still being written upon the flesh.

==Cast==
* Jonas Armstrong as Simon McNeal
* Sophie Ward as Mary Florescu
* Clive Russell as Wyburd
* Paul Blair as Reg Fuller
* Romana Abercromby as Janie
* Simon Bamford as Derek
* James Watson as Jimmy
* Doug Bradley as Tollington
* Gowan Calder as Janies Mother
* Graham Colquhoun as Simons Father
* Marcus McLeod	as Janies Father
* James McAnerney as Doctor Nigel Blake
* Joy McBrinn as Waitress
* Charlie McFadden as Joanne
* Jack North as Simon (age 11)
* Andrew Scott-Ramsay as Male Student
* Siobhan Reilly as Cashier
* Jill Riddiford as Woman
* Isla Stewart as Simons Mother
* Greig Taylor as Stevie McNeal
* Emma Trevorrow as Child #1
* Hannah Bottone as Child #2
* Kirsty McEachran as Child #3
* Louise McCourt as Child #4
* Hannah OMalley as Child #5
* Adele Swordy as Child #6
* Sophie Franklin as Child #7
* Martin Brettle as Ghost Javed Khan as World War I and 80s Ghost
* Rhys Maitland-Jones as Contemporary Dead Guy
* Simon Green as Terrifying Ghost

==Production== Fantasia film Rawhead Rex filmed in The Forbidden The Last The Body Politic (filmed in 1997 within Quicksilver Highway); and The Midnight Meat Train. During his appearance at Fangorias Weekend of Horrors he noted this film will be followed by Dread, Pig Blood Blues, and then Madonna. 

==Release==
The film premiered on 7 March 2009 as part of the Hamburg Fantasy Filmfest Nights. It was released the week of 28 September 2009 in the UK.

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 