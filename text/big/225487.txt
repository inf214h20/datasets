Claudine (film)
 
{{Infobox film
| name           = Claudine
| image          = Claudine-film-1974.jpg
| image_size     =
| caption        = Original theatrical poster for Claudine. John Berry
| producer       = Hannah Weinstein
| writer         = Lester Pine Tina Pine
| narrator       =  Adam Wade Roxie Roker Elisa Loti
| music          = Curtis Mayfield
| cinematography = Gayne Rescher
| editing        = Louis San Andres
| studio          = Third World Cinema
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 minutes
| country        = United States
| gross          = $6 million
| language       = English
| budget         = 
}} 
Claudine is a 1974 American film, produced by Third World Films and distributed by 20th Century Fox. Starring James Earl Jones, Diahann Carroll, and Lawrence Hilton-Jacobs, it is noted for being one of the few mainstream films, featuring an African-American cast released during that time, which was not a blaxploitation film.
 John Berry. The film was released on April 22, 1974, grossing about $6 million. 

==Plot== Black Harlem, Harlem mother, welfare with six children, who finds love with a garbage collector, Rupert Marshall (James Earl Jones), whom she calls "Roop".  The pairs relationship becomes complicated because of a number of factors. Among these are that the couple do not want to marry because they would not be able to support the children without welfare, and that the kids themselves, particularly eldest son Charles (Lawrence Hilton-Jacobs), are apprehensive of Rupert, and believe that he will leave their mother just like her previous husbands had.

Claudine and Rupert meet while both of them are at work. Rupert asks Claudine out on a date with him and Claudine accepts. When Rupert becomes invited inside Claudine’s apartment, the children are rude and vulgar towards Rupert. This is also the first time the audience meets the children and sees the inside of Claudine’s slum-like apartment. Later on in the film, the audience finds out that Claudine receives financial aid from the government through the welfare program. Throughout the film, Miss Kabak, the social worker, visits Claudine at her home and asks her if she is employed and if she is dating anyone. Claudine always denies Miss Kabak the truth and lies to her about being unemployed and single. If Claudine tells the truth and says that she is employed, the amount of financial aid she receives from the welfare program would decrease or she could also no longer receive any more financial aid. If Claudine dates anyone and receives gifts from her boyfriend, the social worker has to deduct any money or gifts Claudine is receiving from whomever she is dating.

Just before he is to announce his engagement to Claudine to the kids, Rupert is served papers for a court order relating to underpayment of child support of his own children; his work wages are garnished to pay the difference. Rupert becomes so upset about this that he disappears for a couple of days and loses contact with everyone. He moves out of his apartment, does not show up to work, and does not show up to the Father’s Day celebration the children had prepared for him. Charles eventually finds him drunk at a bar and angrily confronts him. Charles is angry at Rupert because he left his mother without any explanation. Out of the anger Charles felt for Rupert, he engages in a physical fight with him. After the incident at the bar, Rupert eventually shows up outside of Claudine’s apartment and speaks to her. After some time, the couple talk things over and make up. After several hardships and debating over whether they should marry because of financial issues relating to welfare, the couple decide to marry. They hold a wedding ceremony but it is interrupted when Charles runs inside the apartment in the middle of the ceremony while the police are chasing after him. The couple and the rest of the children run after Charles, leave the ceremony, and board the police wagon. The film ends on a cheery note with the entire family, along with Rupert, walking happily hand in hand through the neighborhood.

==Themes==

=== Welfare and Employment ===
In the film, Claudine receives financial aid from living on welfare. She is able to raise her children and provide them with food and shelter because of the financial aid that she receives from being a part of the welfare system. Even though she does receive financial aid from the government, she receives barely enough money to provide a better living for herself and her six children. Because she barely receives enough financial aid and because she makes little money working, she lives in a slum-like neighborhood in Harlem. Claudine has a job but does not report the fact that she is employed to the social worker. Claudine works as a housekeeper where she cleans another person’s home. The homeowner is usually white, and financially well off. Claudine never reports her job to the social worker, who happens to visit Claudine’s home frequently, because if she does tell her that she has a job, Claudine will no longer be able to receive the same financial aid from the government. Therefore, there exists this sub-theme of welfare and employment and how they conflict in the financial aspect of Claudine’s life.

=== Welfare and Marriage ===
Another theme that occurs in the film is welfare and marriage. Claudine meets Rupert and after several dates, they begin to fall in love. They consider the possibility of getting married together but the main reason why they are hesitant to marry is because of the issue of welfare and other government financial aid programs such as the Aid to Families with Dependent Children (AFDC).   Claudine receives financial aid from government programs because she is a single mother with six children and cannot afford to raise them on her own without any outside financial aid. If Claudine and Rupert were to marry, Claudine would no longer receive financial aid from welfare and the welfare money would be distributed differently. The family would perhaps receive less financial aid which would consequently make it tougher for the couple to provide for themselves and for the six children.

=== Welfare and the African American community ===
The third theme that occurs in the film is related to welfare and the African American community. There are several occasions throughout the film when Claudine, Rupert, or other cast members make comments about African Americans and welfare. Some of the comments consist of jokes about African American women being single mothers with a plenitude of children and how they live off of welfare. The topic of welfare commonly arises during conversations and one example of this is when Rupert and Claudine are on their first date and Rupert makes a comment about Claudine, her children, and welfare. The topic of welfare is so common among the African American community that even Claudine’s younger children are aware of what welfare is and even ask Rupert if he receives welfare assistance. The one person, other than Rupert, who clearly opposes the welfare system is Charles, Claudine’s oldest son. Charles is a member of an activist group which stands for social justice in favor of African Americans. Charles opposes the welfare system because he believes in fighting for progress for the African American community and yet the welfare system represents regress for the African American community because in a way it encourages them to remain unemployed since they are able to receive financial aid from the government anyway. Charles strongly opposes this and believes in the creation of more job openings for African Americans so that they can have the opportunity to earn money and leave the welfare system.

== Main Characters ==

=== Claudine ===
The role of Claudine is played by Diahann Carroll. Claudine is a loving, thirty-six-year-old single mother of six children. She raises her children with the help of the welfare program. Her children are Charles, Charlene, Paul, Patrice, Francis, and Lurlene. Claudine had been married twice before and dated two men before she meets Rupert. She lives in a small, run-down apartment in Harlem, New York. Claudine works as a housekeeper for a wealthy older Caucasian woman who does not seem to like Claudine very much. Claudine eventually dates Rupert and towards the end of the movie, they become very close to getting married.

=== Rupert ===
The role of Rupert is played by James Earl Jones. Rupert is a cheery garbage collector who lives alone in his apartment. He has several children from a previous marriage. Even though he has children, he never gets to see them but he does provide them with some sense of financial child support. He meets Claudine while working. He stops at the house where Claudine works in order to pick up the garbage cans and while he is disposing the trash, he asks Claudine if she would like to go on a date with him. With time, he gains the respect and attention of Claudine’s children when they finally decide to accept him into their family.

===Charles===
Charles is Claudine’s oldest son and is played by Lawrence Hilton-Jacobs. Charles is a member of an African American activist group who fight for positive social change for the African American community. Charles disapproves of his mothers relationship with Rupert because he thinks that Rupert will leave her just like the previous men she dated did. Charles believes that Rupert will get Claudine pregnant and simply leave her to become a single mother again. Charles is also discontented with the fact that Claudine has so many children and that she has to depend on welfare to help her raise them. At times, Charles does not even consider the younger children his siblings because he greatly dislikes the fact that Claudine had several children with different men. He also sees the welfare system as regressive for the African American community; therefore, he advocates the creation of more jobs for African Americans. Charles also disapproves of families or single mothers having too many children and not being able to support them financially. Therefore, one day he comes home and confesses to his mother about having had a vasectomy because he does not want to procreate and be stuck in a situation similar to his mother’s.

==Soundtrack== Billboard Pop Singles Chart in 1974.

==Production== Car Wash fame can be seen toward the end of the film in the crowd that follows Claudine as she hops into the police truck.  He is wearing a red shirt.  Dixon was a long time friend of Diahann Carroll and James Earl Jones.

==Trivia==
Diahann Carroll was nominated for an Academy Award for Best Actress at the 1975 Awards.  The movie was nominated for the Writers Guild of America award for Best Comedy Written Directly for the Screen.

The part of Claudine was originally given to Diana Sands, but she was stricken with cancer. Just prior to her death, Sands called Carroll and insisted that she take the part in her place.

The location of this film was on Edgecombe Avenue and 140th street in Harlem, this area is called Sugarhill.

==Cast==
{|class="wikitable"
! Actor !! Role 
|- Diahann Carroll||Claudine Price
|- James Earl Rupert "Roop" B. Marshall
|- Lawrence Hilton-Jacobs||Charles Price (as Lawrence Hilton-Jacques)
|- Tamu Blackwell||Charlene Price
|- David Kruger||Paul Price
|- Yvette Curtis||Patrice Price
|- Eric Jones||Francis Price
|- Socorro Stephens||Lurlene Price
|- Adam Wade Adam Wade||Owen
|- Elisa Loti||Miss Kabak
|- Roxie Roker||Mrs. Winston
|}

==Reception==
The film earned rentals of $3 million in North America. 

==See also==
* List of American films of 1974
*Claudine (soundtrack)|Claudine - soundtrack album with songs performed by Gladys Knight & the Pips, written and produced by Curtis Mayfield

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 