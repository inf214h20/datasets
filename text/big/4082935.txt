Jack's Back
 
{{Infobox Film
| name           = Jacks Back
| image          = Jacks back.jpg
| caption        = Theatrical release poster
| director       = Rowdy Herrington
| producer       = Cassian Elwes Tim Moore
| writer         = Rowdy Herrington
| starring       = James Spader Cynthia Gibb Robert Picardo
| music          = Danny Di Paola Shelly Johnson
| editing        = Harry B. Miller III
| distributor    = Palisades Entertainment (theatrical) Paramount Pictures (home video)
| released       = May 6, 1988
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          = United States dollar|$492,519 (USA)
}}

Jack’s Back is a 1988 horror film directed and written by Rowdy Herrington and starring James Spader and Cynthia Gibb.

==Plot== doctor is copycat killings is committed. However, when the doctor himself is murdered, his identical twin brother claims to have seen visions of the true killer.

==Reception==
The film got a negative review in The New York Times, which read in part "Jacks Back, which opens today at the Cine 1 and other theaters, is so dull it leaves you plenty of time to marvel at how a plot can be this rickety, how a production can look this shabby, and how the first-time writer and director Rowdy Herrington could borrow a story with so relentless a grip on our imaginations and in no time at all declaw it." 
 Gene Siskel & Roger Ebert gave it a thumbs up, with Siskel declaring that it was a most impressive debut for Rowdy Herrington, as it was for Spader and Gibbs.  As of April 2012, it has an insufficient number of reviews for an overall rating on the Rotten Tomatoes website. 

==Cast and characters==
*John/Rick Wesford - James Spader
*Chris Moscari - Cynthia Gibb
*Sgt. Gabriel - Jim Haynie
*Dr. Carlos Battera - Robert Picardo
*Dr. Sidney Tannerson - Rod Loomis
*Jack Pendler – Rex Ryon
*Scott Morofsky - Chris Mulkey
*Anchorman - Mario Machado

==DVD release==
UK-based distributor Slam Dunk Media released the film on DVD in May 2007 in 1.33:1 full frame format. It is the only DVD release to date. It was available on Netflix video streaming service in SD widescreen format.  Scream Factory will release the film in summer 2015 first time on Blu-ray Disc and DVD in the US. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 