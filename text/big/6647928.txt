Slumber Party Massacre II
{{Infobox film
| name           = Slumber Party Massacre II
| image          = Slumberpartymassacre2.jpg
| caption        = 
| director       = Deborah Brock
| producer       = Roger Corman Deborah Brock Don Daniel
| writer         = Deborah Brock Joel Hoffman Scott Westmoreland Atanas Ilitch
| music          = Richard Cox
| cinematography = Thomas L. Callaway
| editing        = William Flicker
| distributor    = New Concord
| released       =   
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Slumber Party Massacre II is a 1987 rock n roll slasher film. It was directed by Deborah Brock.
It is preceded by The Slumber Party Massacre and succeeded by Slumber Party Massacre III, Cheerleader Massacre and  .

==Plot==
Courtney, the younger sister of the "new girl across the street" in the first film is all grown up now. But suffers from nightmares from the events from the first film. She and the other members of her female rock group go to a condo for the weekend to play music and have fun with their boyfriends. Courtneys dreams are of her sister, who is in a mental institution, warning her about the killer, and the dreams begin to spill into real life, threatening Courtney and her friends. 

The film ends with Courtney (being the only survivor) being confronted by the killer, but manages to kill him by ablazing him with a torch lighter. The next day, paramedics are seen carrying Amy on a stretcher. When Courtney checks on her, Amys eyes suddenly open and she starts evilly laughing in the killers voice. Courtney wakes up in her room, only realizing it was a nightmare the whole time. She then makes out with her boyfriend, but it reveals to be the killer. Courtney wakes up from her nightmare, screaming profusely as a drill bursts through the floor, indicating that the nightmare isnt over yet.

==Cast==
*Crystal Bernard as Courtney Bates
*Kimberly McArthur as Amy 
*Juliette Cummins as Sheila Barrington
*Patrick Lowe as Matt Arbicost
*Heidi Kozak as Sally Burns Joel Hoffman as T.J.
*Scott Westmoreland as Jeff
*Jennifer Rhodes as Mrs. Bates 
*Cindy Eilbacher as Valerie Bates
*Michael Delano as Officer Kreuger
*Hamilton Mitchell as Officer Voorhies
*Atanas Ilitch as The Driller Killer

==Release== Concorde Pictures in October 1987. It was subsequently released on VHS by Nelson Entertainment.  
 New Concorde Home Entertainment in September 2000. Extras included actor bios along with trailers for Slumber Party Massacre, Slumber Party Massacre II and Sorority House Massacre II.  The company re-released the film on a double feature DVD alongside the original The Slumber Party Massacre in July 2003.  These versions are both currently out of print. On 5 October 2010 Shout! Factory released Slumber Party Massacre, Slumber Party Massacre II and Slumber Party Massacre III on a two-disc special edition DVD set. 

==Reception==
The Bad Movie Planet review found Slumber Party Massacre 2 to be "terrible".  The Camp Blood review felt the film made up in weirdness what it lacked in "coherence, suspense, and production value". 

==References==
 

==External links==
* 
* 

 
 
 
 

 