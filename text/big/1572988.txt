The Jacket
 
{{Infobox film
| name           = The Jacket
| image          = The_Jacket_poster.JPG
| caption        = Film poster
| director       = John Maybury
| producer       = George Clooney Peter Guber Steven Soderbergh Marc Rocco
| writer         = Massy Tadjedin
| starring       = Adrien Brody Keira Knightley Kris Kristofferson Jennifer Jason Leigh Kelly Lynch Brad Renfro Daniel Craig
| music          = Brian Eno
| cinematography = Peter Deming
| editing        = Emma E. Hickox Section Eight   
| distributor    = Warner Independent Pictures
| released       =      
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $28.5 million
| gross          = $21,126,225
}}
The Jacket is a 2005   wrote the screenplay based on a story by Tom Bleecker and Marc Rocco. The original music score is composed by Brian Eno and the cinematography is by Peter Deming.

==Plot==
After miraculously recovering from a bullet wound to the head, Gulf War veteran Jack Starks (Adrien Brody) returns to Vermont in 1992, suffering from periods of amnesia. While walking, he sees a young girl, Jackie (Laura Marano), and her alcoholic mother (Kelly Lynch) in despair beside their broken-down truck. Starks and Jackie quickly form a certain affinity; she asks him to give her his dogtags and he does so. He gets the truck started for them and continues on his way. Shortly after, a man driving along the same highway gives Jack a ride and they get pulled over by a policeman. The scene changes: Starks is found lying on the deserted roadside near the dead policeman, with a slug from the policemans gun in his body. The murder weapon is on the ground nearby. Although he testifies there was someone else at the scene, he is not believed because of his amnesia. Starks is found not guilty by reason of insanity and is incarcerated in a mental institution.

Starks is placed in the care of Dr. Thomas Becker (  and then placed inside a morgue drawer as a form of sensory deprivation. While in this condition, he is somehow able to travel 15 years into the future and stay there for a short time. He meets an older version of Jackie (Keira Knightley) at a roadside diner where she works. He suspects this happens because it is the only memory he can ever fully hold on to. Seeing him standing forlornly, she takes pity on him and offers him shelter, just for the night. While in her apartment, Starks comes across his own dogtags and confronts her. Jackie tells him that Jack Starks died on New Years Day in 1993, and so he cannot possibly be who he says he is. She becomes upset and asks him to leave. Subsequently, Starks is transported back to the future on several occasions in the course of his treatment and, after earning Jackies trust, they try to figure out how to make use of the time-travelling so as to remove Jack from the hospital and save his life. 

Early on 1 January 1993, knowing that his time is quickly running out, Starks is briefly taken out of the hospital by Dr. Beth Lorenson, who he has finally convinced of his time travel experiences and his knowledge of future events.  She drives Starks to the home of Jackie and her mother, where he gives the mother a letter he has written, which outlines Jackies bleak future and warns the mother that she is fated to orphan Jackie when she falls asleep with a cigarette and is burned to death. When he returns to the hospital, Starks slips on the ice and hits his head.  Bleeding profusely, he convinces two of the more sympathetic doctors to put him into the jacket one last time.

Starks returns to 2007, where he finds that his letter has made all the difference. Jackie now has a better life than in the previous version of 2007.  She is no longer a waitress, she is now dressed in a nurses uniform, and she has a noticeably more cheerful outlook. They reprise their first 2007 meeting: she sees Starks standing in the snow and initially drives past him, but backs up when she notices his head wound. She stops and offers to take him to the hospital where she works. While they are in the car, Jackie receives a call from her mother — still alive and well. They drive on, the screen fades to white, and a voice-over reveals that the link to the "previous" future is not lost when Jackie says "How much time do we have?", a question she has asked him before.  As the credits start to roll, the answer to the question is given by the words of the song: "We have all the time in the world".

==Cast==
* Adrien Brody as Jack Starks
* Keira Knightley as Jackie Price
** Laura Marano as Young Jackie
* Kris Kristofferson as Dr. Thomas Becker
* Jennifer Jason Leigh as Dr. Beth Lorenson
* Kelly Lynch as Jean Price
* Brad Renfro as The Stranger
* Daniel Craig as Rudy Mackenzie
* Steven Mackintosh as Dr. Hopkins
* Brendan Coyle as Damon
* Mackenzie Phillips as Nurse Hardling Jason Lewis as Officer Harrison

==Basis== Ed Morrell, who told London about San Quentin prisons inhumane use of tight straitjackets. 

==Reception==
The Jacket opened on March 4, 2005, and grossed $2,723,682 on opening weekend, with a peak release of 1,331 theaters in the United States. The film went on to gross $6,303,762 domestically, for a total of $14,822,463 worldwide. 

The Jacket garnered mixed reviews on release; the film has a 44% "rotten" rating on Rotten Tomatoes,  and a 44% average critic rating on the aggregate reviews site Metacritic. 

==See also==
*La Jetée, a 1962 French science fiction featurette in which sensory deprivation and strong memories lead to time travel.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 