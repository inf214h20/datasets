Times Square (film)
 
{{Infobox film
| name = Times Square
| image= Times Square VideoCover.png
| caption = Times Square DVD cover
| director = Allan Moyle Kevin McCormick (Executive Producer) 
| writer = Jacob Brackman (screenplay) Allan Moyle (story) Leanne Ungar (story)
| starring = Trini Alvarado Robin Johnson Tim Curry Peter Coffield Herbert Berghof
| music = Blue Weaver
| cinematography = James A. Contner
| editing =
| studio = EMI Films Robert Stigwood Organisation
| distributor = Associated Film Distribution
| released = October 17th, 1980
| runtime = 111 min.
| country = United States
| awards = English
| budget =
| gross = $1.4 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 260 
}} runaways from Pump Up the Volume (1990).

==Plot== blood oath and make a pact to scream out each others names in times of trouble.

There is a city-wide search for Pamela, after her father reports her as missing and accuses Nicky of kidnapping her. Nicky and Pamela try to eke out a living by engaging in card games, petty larceny, and odd jobs. Using discarded furniture and lines, Nicky and Pamela deck out their warehouse hideout, making it a bohemianism|bohemian-style apartment, and craft costumes for themselves. Meanwhile, late-night radio station disc jockey Johnny LaGuardia (Tim Curry), who broadcasts from a penthouse studio overlooking Times Square, realizes that Mr. Pearls missing daughter is the same "Zombie Girl" who sent him letters, telling him how sad and lonely she feels. LaGuardia, who resents Mr. Pearls "Reclaim Rebuild Restore" campaign to redevelop and gentrify Times Square, uses his radio station, WJAD, to reach out to Nicky and Pamela; he reads out Pamelas poetry over the air.

Nicky and Pamela write songs together and form an underground punk rock band, The Sleez Sisters, with the help of LaGuardia, who sees Nicky and Pamela as a symbol of youthful rebellion and an opportunity to undermine Pamelas father. The Sleez Sisters become a hit with the citys disillusioned youth after broadcasting their volatile punk rock songs and speeches on WJAD. Nicky and Pamela encourage their fans to throw TV sets off apartment rooftops; they also send messages to the doctors who treated them, and to Pamelas father, through their songs. Pamela tells her father through WJAD, "Dear Daddy, I am not kidnapped. I am me-napped, I am soul-napped, I am Nicky-napped, I am happy-napped. We are having our own renaissance." She also says, "You want to make Times Square as cold as your icy eye? Why do you want to punish people who arent like you? You know, at home, Ive heard you use the following words: spic, faggot, nigger, psycho. Well, I just want you to know, your daughter is one."

Mr. Pearl responds by issuing a press release, stating that Pamela is ill and requires medication, and that Nicky is unstable and dangerous. Pamela and Nicky subsequently have a falling out when they realize that their lives are on divergent paths. Pamela is content with her newfound sense of identity, and wants to return home, telling Nicky, "I cant be like you. You’re different. You’re a star!" Nicky wants to continue with the Sleez Sisters, telling Pamela, "You cant disappear if youre famous!" Nicky becomes jealous of Pamelas relationship with LaGuardia. She accuses LaGuardia of exploiting Pamela and herself, and throws Pamela and LaGuardia out of the warehouse hideout.
 42nd Street Grindhouse. A message is sent out to the fans of the Sleez Sisters, inviting them to attend the concert. Nicky says, "If they treat you like garbage, put on a garbage bag. If they treat you like a bandit, black out your eyes!" Girls across the city create their own versions of the Sleez Sister uniform in response to the announcement, and board buses and subways to converge in Times Square.

In her signature garbage-bag costume and bandit-mask-style make-up, Nicky sings on the marquee roof above a crowd of cheering fans. Nicky shouts to the crowd, "They might be able to blow me away, but they cant blow all you away!" With the police approaching from behind, Nicky jumps off the edge of the marquee and into a blanket held taut by a group of fans. Camouflaged in the crowd, Nicky manages to evade capture by the police. Pamela watches her friend vanish into the night.

==Production==
Times Square was directed by Allan Moyle from a script written by Jacob Brackman, based on a story by Moyle and Leanne Ungar. The movie was inspired by a diary, found in a second-hand couch bought by Moyle, detailing the life on the streets of a young mentally disturbed woman. According to the DVD commentary, the original title of the project was "Shes Got the Shakes."  The script caught the attention of Robert Stigwood, the impresario behind the musical films Saturday Night Fever (1977), Grease (film)|Grease (1978), and Sgt. Peppers Lonely Hearts Club Band (film)|Sgt. Peppers Lonely Hearts Club Band (1978). The film went into production with a $6 million budget, and was advertised with the taglines "In the heart of Times Square, a poor girl becomes famous, a rich girl becomes courageous, and both become friends" and "TIMES SQUARE is the music of the streets." Although Tim Curry has a supporting role in Times Square (and filmed all his scenes in two days), his familiarity with film audiences ensured that he received top-billing onscreen and in the films advertising above the two unknown leads, 15-year-old Robin Johnson and 13-year-old Trini Alvarado. Robin Johnsons casting was a bit of a fluke. According to Robin, she was cutting class – cutting up – and had been approached by some supposed talent scout, claiming she should audition for the film. She had never seen or heard from this man after the one meeting and no one from the film crew knew of him. 

The original cut of Times Square contained lesbian content which was mostly deleted from the final print (which still has subtle lesbian overtones). Moyle revealed in the DVD audio commentary that the films integrity was compromised by the removal of the more overt lesbian content, and the addition of several "inappropriate" songs to the films soundtrack at the insistence of producer Robert Stigwood, who wanted the film to be another Saturday Night Fever and insisted that the soundtrack be a double album to make the film more commercially viable. Allan Moyle and Robin Johnson remarked on the audio commentary that the loss of key scenes made the narrative disjointed, and damaged the storys emotion and characterizations. They also note that the films focus changes, jarringly, from Pamela to Nicky, and that the increasingly outlandish and unrealistic story undermines the movies gritty, on-location documentary style. Moyle left production before the film was completed, and other people supervised scenes to accompany the soundtrack additions (for example, the sequence featuring teens preparing to go to the Sleez Sisters final concert was shot by the films second unit).

The version of the film released to theatres was not Moyles preferred cut; however, he still acknowledges the finished films importance as it documents a Times Square that mostly no longer exists: the film was shot on location and captured Times Squares seedy, grindhouse atmosphere before it was cleaned up in the mid-1990s.

==Reaction== Robert Stigwood Organization, with the understanding that RSO would develop film and music projects for her. RSO intended to market Johnson as "the female John Travolta," and her contract legally barred her from accepting offers or auditions from rival companies. Johnson therefore turned down calls from agents, producers and casting directors, but the projects RSO promised her never came to fruition. Johnson took a job as a bank teller whilst waiting for her RSO contract to expire, and by the time it did, there were no offers for work. Johnson did some minor film and TV roles, but by the late 1980s, she gave up on acting and got a job as a traffic reporter on a Los Angeles radio station.

Over the years since its original release, Times Square has been rediscovered and become a cult classic and a staple at gay and lesbian film festivals,  because of the aforementioned, subtly-portrayed lesbian relationship between the films two female leads. Kathleen Hanna of Bikini Kill and Le Tigre cites this  as one of her favorite films. Welsh rock group Manic Street Preachers covered the Times Square song "Damn Dog" on their debut album Generation Terrorists (1992) and quoted dialogue from the film in the album liner notes ("Damn Dog" was, however, excluded from the American release of the album). The Manics also named their song "Roses in the Hospital" (from their second album, 1993s Gold Against the Soul) after Pamelas line, "What about the roses in the hospital?" (alluding to the scene in which Nicky eats roses to distract Pamela from the doctors and her father). In concerts and publicity shots in 1993, Manics bassist Nicky Wire often wore bankrobber-mask-style makeup, as Nicky Marotta does in the film.

==Home video==
The movie was released on DVD by Anchor Bay Entertainment in 2000. Extra features on this DVD include audio commentary by director/co-writer Allan Moyle and star Robin Johnson, and the films original theatrical trailer. Allan Moyle has stated that a directors cut of Times Square is unlikely to ever surface because the footage needed for its restoration is missing.

== Soundtrack ==

 
 The Ramones, Joe Jackson, Suzi Quatro, Roxy Music, Patti Smith and The Pretenders.

The Suzi Quatro track, "Rock Hard," is identified in the film as being Nicky and Pamelas favorite record. The disco song "Help Me!", which plays over the films closing scene, is a duet between Robin Gibb (of The Bee Gees) and Marcy Levy, and was one of the songs added to the film without the consent of Allan Moyle. The song "Down in the Park" is credited as being performed by Gary Numan although, technically, it was recorded when Numan was using the band name Tubeway Army. The version of "Down in the Park" included on the Times Square soundtrack is not the album/single version from Replicas (album)|Replicas (1979), but an earlier version of the song that would later be released on Numans Replicas Redux (2008).

The soundtrack also features original songs sung by the films actors, "Damn Dog" by Johnson, "Your Daughter is One" by Johnson and Alvarado, and "Flowers of the City" by Johnson and David Johansen. The song "Dangerous Type" by The Cars features in the film, but was not included on the soundtrack.

As a compilation of some of the most important New Wave and punk music from the era, the Times Square soundtrack achieved far more notoriety than the film did on its release. It also became a collectors item among fans of XTC, because it included the specially-written XTC track "Take This Town", which for many years was only available on this soundtrack.

In his audio commentary for the Times Square DVD, Allan Moyle mentions that David Bowie was commissioned to provide a song for the movies soundtrack, but Bowies label at that time wouldnt let the filmmakers use it. (At the time, Bowie was still under contract with RCA Victor Records, and the Times Square album was issued by RSO Records, at the time distributed by RCA Victor competitor PolyGram; however, its notable that Lou Reed, who does appear on the album, was also under contract to RCA Victor.) Desmond Child has mentioned in a magazine interview that he collaborated with David Bowie on the song "The Night Was Not" (the song did appear on the Times Square soundtrack, performed by Childs band, Desmond Child & Rouge). Another rumour is that Bowie intended to provide a re-recorded version of his 1971 song "Life on Mars?" for the Times Square soundtrack.  Although it has not been confirmed whether or not Bowie re-recorded a studio version of "Life on Mars?" in 1980, it is worth noting that in that same year, Bowie performed a rearranged, punkier version of the song during a live appearance on The Tonight Show Starring Johnny Carson.

 
==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 