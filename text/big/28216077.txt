Steam of Life
{{Infobox film
| name           = Steam of Life
| image          = steam_of_life.jpg
| alt            = 
| caption        = Theatrical release poster
| director       =  
| producer       = Joonas Berghäll
| writer         =  
| starring       = 
| music          = Jonas Bohlin
| cinematography =  
| editing        = Timo Peltola
| studio         =  
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| gross          = 
}}
Steam of Life ( ) is a Finnish documentary film about male saunas directed by Joonas Berghäll and Mika Hotakainen. The movie was produced by Joonas Berghäll. It opened theatrically in New York City on July 30, 2010 and opened in Los Angeles on August 6, 2010 at the 14th Annual DocuWeeks.   
 Best Foreign Language Film at the 83rd Academy Awards, but it didnt make the final shortlist.    It is the first documentary film to represent Finland at the Academy Awards.    It was also nominated for "Best Documentary" at the 23rd European Film Awards.

It won the Best International Cinematography at the Documentary Edge Festival in New Zealand in 2011.

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Finnish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   at  

 
 
 
 
 
 


 
 