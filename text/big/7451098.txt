Cellar Dweller
{{Infobox Film
| name           = Cellar Dweller
| caption        = 
| image	=	Cellar Dweller FilmPoster.jpeg 
| director       = John Carl Buechler
| producer       = Bob Wynn Charles Band (executive)
| writer         = Don Mancini
| starring       = Yvonne De Carlo Debrah Farentino Brian Robbins Pamela Bellwood Vince Edwards Jeffrey Combs
| music          = Carl Dante
| cinematography = 
| editing        = Barry Zetlin
| distributor    = Empire Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Cellar Dweller is a 1988 horror film, about a comic book artist who unleashes a demon after drawing it. It was directed by John Carl Buechler, written by Don Mancini (as Kit Du Bois), and stars Debrah Farentino and Brian Robbins.

==Plot==
Thirty years have passed since the grisly murder/suicide of Colin Childress, creator of the comic book, Cellar Dweller.  But, as often happens to those ignorant of it, comic book artist Whitney Taylor is doomed to repeat history in a most grotesque way.  Little does she know that her twisted renderings will soon reincarnate the bloody hysteria of Cellar Dweller.

==Release== videocassette and New World Video. In 1991, Starmaker Video released a tape in the EP Mode.  10 years after that, MGM released an Amazon.com Exclusive VHS of the film. 
 Contamination 7, Catacombs (1988 film)|Catacombs and The Dungeonmaster as part of the second volume of their Scream Factory All-Night Horror Marathon series. It will be released again with Catacombs (1988 film)|Catacombs as a double feature Blu-ray on July 14, 2015 from Scream Factory. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 