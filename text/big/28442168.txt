The Young Runaways
{{Infobox film
| name           = The Young Runaways
| image          = Young runaways poster 01.jpg
| caption        =
| director       = Arthur Dreifuss
| producer       = Jerome F. Katzman (associate producer) Sam Katzman (producer)
| writer         = Orville H. Hampton Kevin Coughlin Lloyd Bochner Patty McCormack
| music          = Fred Karger
| cinematography = John F. Warren Ben Lewis
| studio         = Four-Leaf Productions 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}}
 Kevin Coughlin and Patty McCormack. The supporting players are Lloyd Bochner, Dick Sargent and in one of his earliest roles Richard Dreyfuss, has a small part as Terry, a juvenile delinquent who meets a bad end. 

==Plot==
The film begins with Shelley Morrison (Brooke Bundy) packing to run away from home because she feels she is not loved by her widowed father (Lloyd Bochner). She tells their maid (future The Jeffersons star Isabel Sanford) that to her father she is not his daughter,but a product. She has overheard him telling columnist Army Archerd that he is testing some of his psychological theories about teenagers on his own daughter. 

Shelley catches a ride with an older man. He tries to put the moves on her, and they have an accident when he loses control of the car. She escapes without her suitcase and winds up in Chicago. She meets a prostitute named Joanne in a diner, and Joanne tells Shelley that she is a model. Joanne offers Shelley a place to stay, with the ulterior motive of turning Shelley into a working girl.

Kevin Coughlin plays Dewey, who runs away from home because he fears he has gotten his girlfriend pregnant. He first stays in a boarding house run by Sage (Dick Sargent). There he meets Terry (Richard Dreyfuss), who has an allergy to work and makes fun of Terry for wanting to find a job. Dewey gets a job at a gas station for $1.00 per hour and moves into a rooming house. He is coming home one night during a downpour and meets Shelley, who has run away from Joanne the prostitute, after Joanne has taken her on a double date. Dewey convinces the police that Shelley is his sister and they allow her to go with him. She stays with Dewey (on the sofa, of course) and soon they are falling in love. 

Deannie (McCormack) cant stand her shrewish mother (Lynn Bari) constantly telling her what to do, runs away to Chicago and meets Loch (Ken Del Conte), a musician who is just a tad possessive. She moves in with him, but began to have feelings for his roommate Curly (Lance LeGault). Loch comes home and finds Deannie in bed with Curly. In a rage, he beats them both to death. 

Joannes pimp is afraid that Shelley will lead the police to him, so he has her kidnapped and held in a basement. The police find her in time and she returns home with her father. She and Dewey promise to keep in touch.

==External links==
*  

 
 
 
 