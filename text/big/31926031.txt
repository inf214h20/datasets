6 Days on Earth
{{Infobox film
| name = 6 Days on Earth
| image = 6 Days on Earth.jpg
| caption =
| director = Varo Venturi
| producer =
| writer = Varo Venturi M. Luisa Fusconi Giacomo Mondadori
| screenplay =
| story =
| based on =  
| starring = Massimo Poggio Laura Glavan Marina Kazankova Ludovico Fremont
| music = Reinhold Heil Johnny Klimek Jordan Balagot
| cinematography = Daniele Baldacci
| editing = Varo Venturi
| studio =
| distributor = Bolero Film
| released =  
| runtime = 101 minutes
| country = Italy
| language = Italian
| budget =
| gross =
}} science fiction film directed by Varo Venturi.  It stars Massimo Poggio, Laura Glavan and Marina Kazankova. Supporting actors include Ludovico Fremont and Pier Giorgio Bellocchio.  It was shot both in English  and Italian. 

It has been selected to be featured at the Moscow International Film Festival|33° Moscow International Film Festival.  

==Plot==
Dr. Davide Piso is a courageous scientist who has been studying thousands of cases involving   condition anymore, hence giving manifestation to Hexabor of  . 

==Cast==
* Massimo Poggio as Dr. Davide Piso
* Laura Glavan as Saturnia / Hexabor of Ur
* Marina Kazankova as Elena
* Ludovico Fremont as Leo
* Varo Venturi as Father Trismegisto
* Pier Giorgio Bellocchio as Lieutenant Bruni
* Nazzareno Bomba as Giovanni Cervo
* Emilian Cerniceanu as Matei
* Francesca Schiavo as Countess Gotha-Varano
* Giovanni Visentin as Prince Gotha-Varano
* Ruby Kammer as Rita
* Ferdinando Vales as Doc Enlil
* Daniele Bernardi as J.J. Enki
* Leon Kammer as Bill
* David Traylor as Danny

==References==
 

==External links==
*  
*  
* Il Sole 24 Ore film sheet: http://cinema.ilsole24ore.com/film/6-giorni-sulla-terra/
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 