The Hot Touch
{{Infobox film
| name           = The Hot Touch
| image          = 
| image_size     = 
| caption        = 
| director       = Roger Vadim
| producer       = 
| writer         = Peter Dion Jean-Yves Pitoun
| narrator       = 
| starring       = 
| music          = André Gagnon
| cinematography = François Protat
| editing        = Stan Cole Yurij Luhovy
| distributor    = 
| released       = 10 December 1982
| runtime        = 93 minutes
| country        = Canada  English 
| budget         = 
}}

The Hot Touch (also credited as Hot Touch) is a 1981 film directed by Roger Vadim. 

This caper film  is set in the world of art forgery.  An accomplished art forger and a businessman have for many years been successful in a company which authenticates paintings before they are auctioned. They are discovered by an art dealer and blackmailed into forging paintings which disappeared in the Second World War. Procedural detail around the act of forgery is exploited for high-rolling glamour. 

==Cast==
*Wayne Rogers - Danny Fairchild 
*Marie-France Pisier - Dr. Simpson 
*Lloyd Bochner - Severo 
*Samantha Eggar - Samantha OBrien 
*Patrick Macnee - Vincent Reyblack 
*Melvyn Douglas - Max Reich  
==References==
 
==External links==
*  
* 


 

 
 
  
  
 
 
 

 