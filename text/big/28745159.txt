The School for Scandal (1930 film)
 
{{Infobox film
| name           = The School for Scandal
| image          = 
| caption        = 
| director       = Maurice Elvey
| producer       = Maurice Elvey
| writer         = Jean Jay
| screenplay     = 
| story          = 
| based on       =   Ian Fleming Henry Hewitt
| music          = 
| cinematography = 
| editing        = 
| studio         = Albion Films
| distributor    = 
| released       =  
| runtime        = 76 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British comedy Ian Fleming.  It is the first sound film adaptation of Richard Brinsley Sheridans play The School for Scandal. It is also the only feature length film shot using the unsuccessful Raycol colour process, and marked the screen debut of Sally Gray. 

The British Film Institute has placed it on the BFI 75 Most Wanted list of lost films.   

==Cast==
* Basil Gill as Sir Peter Teazle
* Madeleine Carroll as Lady Teazle Ian Fleming as Joseph Surface
* Henry Hewitt as  Charles Surface
* Edgar K. Bruce as Sir Oliver Surface
* Hayden Coffin as Sir Harry Bumper
* Hector Abbas as Moses
* Dodo Watts as Maria
* Anne Grey as Lady Sneerwell
* John Charlton as Benjamin Backbite
* Stanley Lathbury as Crabtree
* Henry Vibart as Squire Hunter
* May Agate as Mrs. Candour
* Maurice Braddell as Careless
* Gibb McLaughlin as William
* Wallace Bosco as Rawley
* Sally Gray - Bit Part
* Rex Harrison - Bit Part
* Anna Neagle - Bit Part

==References==
 

==External links==
* , with extensive notes
* 

 

 
 
 
 
 
 
 
 
 


 