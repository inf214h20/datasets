Leonora (film)
 
{{Infobox film
| name           = Leonora
| image          = 
| image size     =
| caption        = 
| director       = Derek Strahan
| producer       = Geoffrey Brown
| writer         = Derek Strahan
| based on = 
| narrator       =
| starring       = Mandi Miller David Evans Leon Marvel Angela Menzies-Wills
| music          = 
| cinematography = Geoffrey Brown
| editing        = 
| studio =  Revolve Pty Limited
| distributor    = Showcase (video)
| released       = 1985
| runtime        = 
| country        = Australia English
| budget         = A$500,000 
| gross = 
| preceded by    =
| followed by    =
}} sex film about a couple who have an open marriage. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p137 

==Plot==
Ex motor ace Simon Erickson pushes his wife Leonara into an open marriage. She has an affair with Mark Trainer.

==Release==
The movie was not released theatrically and went straight to video.  The film now has a cult following and is one of the most sought after hard to find VHS Australian movies. It has not yet been released on DVD.

==References==
 

==External links==
*  at IMDB

 
 


 