Gentlemen of the Press
 
{{Infobox film
| name           = Gentlemen of the Press
| image          =
| caption        =
| director       = Millard Webb
| producer       = Monta Bell
| writer         = Ward Morehouse (play)  Bartlett Cormack (screenplay)
| starring       = Walter Huston Kay Francis
| music          =
| cinematography = George Folsey
| editing        = Morton Blumenstock
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
}}
Gentlemen of the Press is a 1929 all-talking film starring Walter Huston in his first feature film role and Kay Francis in her first film role. The film still survives. This films copyright has expired and is now in the public domain. It survives in a copy sold to MCA for television distribution. 

The film is based on Ward Morehouses 1928 Broadway play Gentlemen of the Press. 

==Cast==
* Walter Huston - Wickland Snell
* Charles Ruggles - Charlie Haven
* Kay Francis - Myra May
* Betty Lawford - Dorothy Snell Hanley Norman Foster - Ted Hanley
* Duncan Penwarden - Mr. Higgenbottom
* Lawrence Leslie - Red Harry Lee - Copy Editor

uncredited
* Brian Donlevy - Kelly, A Reporter
* Victor Killan - McPhee, A Reporter
* Chares Wagenheim - Copy Boy

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 