Indyfans and the Quest for Fortune and Glory
 
{{Infobox film
| name           = Indyfans and the Quest for Fortune and Glory
| image          = IndyfansMoviePoster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Film poster
| director       = Brandon Kleyla
| producer       = Kevin English Frank Bettag
| writer         = Brandon Kleyla
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Michael McCormack
| cinematography = James Filsinger Scott Keeler Bill Kleyla Brandon Kleyla
| editing        = Brandon Kleyla
| studio         = Red Dot Film Studios
| distributor    = Cinema Libre Studio
| released       =  
| runtime        = 60 minutes (festival) 80 minutes (DVD)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Indiana Jones films through interviews and profiles of more than 50 devotees of the films.             

==Background== Gods and Free Enterprise (1998). In 2000 he partnered with his sister Alexis to form Red Dot Film Studios, and in 2005 his first project as writer and director was the comedy film The Road to Canyon Lake.  Indyfans and the Quest for Fortune and Glory became Kleylas second project as writer and director.

===Production=== fourth film of the series that acted as his impetus.   As a self-professed fan of the Indiana Jones films, director Kleyla made note that while there are fan conventions for Star Wars and Star Trek films, there are none for Indiana Jones films.   In the summer of 2007, he began work to create a documentary about those he refers to as "the felt-hatted faithful". Stating that the film began "just for fun", Kleyla learned through his interview processes that the Indiana Jones films "have really gotten to people", just as he had himself become a fan of the films when as a child he repeatedly visited the Indiana Jones attraction at Disney World in Florida.  
 Las Vegas to attend an auction of film props which included Indy’s whip and the holy grail prop from Indiana Jones and the Last Crusade. 

==Critical response==
Film Threat made note of the films interviews with fans and with industry professionals "marginally" associated with the Indiana Jones films or franchises, and wrote of the filmmakers belief that the films made actual impact on peoples lives.  In panning the film, they wrote that they "threaten to be interesting on occasion, but the interviews do nothing to reinforce the filmmaker’s thesis, because he doesn’t have one."  Writing that the interviews seemed more haphazard than properly planned, they wrote that it seemed the director "just sort of rounded up people who like the films, and a few people who were marginally involved with the films, got them to speak a bit, and then edited the footage together without much of a direction to it".  The reviewer placed the lack of the films focus directly on "the director’s choice of questions, his inability to draw the best anecdotes out of his interview subjects, and a lack of skill in the editing room." 
 Lawrence of Arabia as a yardstick, they noted that films can accomplish this through "deft directing, stellar acting and breathtaking scenery", or achieve it through the passion of fans toward certain films and their characters. They expanded that while films such as Star Wars and Star Trek series are famous for their fans, "Indy devotees politely and proudly separate themselves from folks who prefer films set in an era of intergalactic travel".

Blogcritics make note that since filmmaker Brandon Kleyla since was born in 1983, he was too young to have seen the Indiana Jones trilogy original theatrical releases, but that after repetitively watching the Indiana Jones stunt show at Disneys MGM Studios, he "fell in love with the character", and began what is "reported to be one of the largest Indiana Jones memorabilia collections in the world". They expanded that the common theme about the persons being interviewed: "the love of Indiana Jones". They concluded by writing that the documentary "is a fun film that looks at how the Indiana Jones character and films have left their mark on pop culture and how Indy is one of the more recognizable icons of the 20th century." 

==Release== Newport Beach International Film Festival on April 27, 2008,    and had its DVD release on October 7, 2008 through Cinema Libre Studio. 

==References==
 

==External links==
*   at the Internet Movie Database
*  

 

 
 
 
 
 
 
 
 
 