Le Révélateur
{{Infobox Film
| name           = La Révélateur
| image          = 
| caption        = 
| director       = Philippe Garrel
| producer       = Philippe Garrel
| writer         = Philippe Garrel
| starring       = Laurent Terzieff Bernadette Lafont Stanislas Robiolles
| music          = 
| cinematography = Michel Fournier
| editing        = Philippe Garrel
| studio         = Zanzibar Films
| released       = 1968
| runtime        = 67 minutes
| country        = France
| language       = 
| budget         = 
}} experimental narrative film by Philippe Garrel. 

==Synopsis==
The film follows a 4-year old boy (Stanislas Robiolles) and his parents (Laurent Terzieff and Bernadette Lafont).
Cinematographer Michel Fournier, a then-frequent collaborator of Garrel, considers this their best work together.

== Cast ==
* Stanislas Robiolles as The Child 
* Laurent Terzieff as The Father 
* Bernadette Lafont as The Mother 

==Production==
Le Révélateur was photographed in Munich, Germany|Munich. The film is intentionally silent films|silent, and it is rumored that it is intended to be projected at 18 frames per second, the traditional speed for silent films.

==References==
 

== External links ==
*  

 

 

 
 
 
 
 
 