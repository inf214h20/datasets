Dawn of the Dead
 
 
 
{{Infobox film
| name           = Dawn of the Dead
| image          = Dawn of the dead.jpg
| alt            = Painted theatrical release that includes various credits, an ominous zombie looking over the horizon, and the words "Dawn of the Dead" in military print below.
| caption        = Theatrical release poster
| director       = George A. Romero
| producer       = {{Plainlist|
* Richard P. Rubinstein
* Claudio Argento
* Alfredo Cuomo}}
| writer         = George A. Romero
| starring       = {{Plainlist|
* David Emge
* Ken Foree
* Scott Reiniger
* Gaylen Ross}}
| music          = {{Plainlist| Goblin
* Dario Argento De Wolfe Music Library}}
| cinematography = Michael Gornick
| editing        = {{Plainlist|
* Dario Argento
* George A. Romero}}
| studio         = Laurel Group Inc.
| distributor    = United Film Distribution Company
| released       =  
| runtime        = {{Plainlist|
* 118 minutes  
* 127 minutes  
* 139 minutes  }}
| country        = {{Plainlist|
* United States
* Italy}}
| language       = English
| budget         = $650,000 
| gross          = $55 million   
}} apocalyptic effects reanimation of the dead, who prey on human flesh, which subsequently causes mass hysteria. The cast features David Emge, Ken Foree, Scott Reiniger and Gaylen Ross as survivors of the outbreak who barricade themselves inside a suburban shopping mall. 

Dawn of the Dead was filmed over approximately four months, from late 1977 to early 1978, in the Pennsylvania cities of Pittsburgh and Monroeville, Pennsylvania|Monroeville.   Its primary filming location was the Monroeville Mall. The film was made on a relatively modest budget estimated at $650,000, and was a significant box office success for its time, grossing approximately $55 million worldwide.  Since opening in theaters in 1978, and despite heavy gore content, reviews for the film have been nearly unanimously positive.   
 official sequels, parodies and remake of the movie premiered in the United States on March 19, 2004. It was labeled a "Remake#Reimagining|re-imagining" of the original films concept.  In 2008, Dawn of the Dead was chosen by Empire (magazine)|Empire magazine as one of The 500 Greatest Movies of All Time,    along with Night of the Living Dead. 

==Plot==
The United States is devastated by an unknown phenomenon which reanimates recently deceased human beings as flesh-eating Zombie (fictional)|zombies. Despite the best efforts by the U.S. government and local authorities to control the situation, society is beginning to collapse and the remaining survivors are given to chaos. Some rural communities and the military have been effective in fighting the zombies in open country, but cities are helpless and largely overrun. Confusion reigns at the WGON television studio in Philadelphia by the phenomenons third week, where staff member Stephen Andrews and Francine Parker are planning to steal the stations traffic helicopter to escape the zombies. Meanwhile, police SWAT officer Roger DiMarco and his team raid an apartment building where the residents are defying the martial law of delivering their dead to National Guardsmen. Some residents fight back with handguns and rifles, and are slaughtered by both the overzealous SWAT team and their own reanimated dead. During the raid, Roger meets Peter Washington, part of another SWAT team, and they become friends.  Roger tells Peter that his friend Stephen intends to take his networks helicopter, and suggests that Peter come with them.  The matter is decided when they are informed of a group of zombies sheltered in the basement, which they execute with grim determination.

That night, Roger and Peter escape Philadelphia with Francine and Stephen in the helicopter. Following some close calls while stopping for fuel, the group comes across a shopping mall, which becomes their sanctuary. To make the mall safe for habitation, they block the entrances with trucks to keep the undead masses outside from building up enough cumulative force to break through; they also craft a wooden "false wall" to hide the access to their living space. During the cleanup operation, Roger becomes reckless and is bitten, dooming himself to the infection.  After clearing the mall of zombies, the four enjoy a hedonistic lifestyle with all the resources available to them. As time goes by, however, they come to perceive themselves as imprisoned by the zombies, especially since Francine is four months pregnant. Peter offers to abort the child, but this is rejected. The men begin to consider leaving; Stephen, now seeing the mall as a kind of kingdom, opposes the plan, but teaches Francine how to operate the helicopter in case of emergency. Roger eventually succumbs to his wounds, reanimates, and is shot by Peter. All emergency broadcast transmissions eventually cease, suggesting that civilization as they know it has completely collapsed.

Their ironic salvation occurs when a gang of motorcyclists, having seen the helicopter during one of Francines flying lessons, break into and start looting the mall, which allows hundreds of zombies inside.  Stephen forces a gun battle with the bikers and is shot in the arm; he tries to escape through an elevator shaft, but is cornered by the undead and bitten several times.  As some of the bikers, shot by Peter, were consumed by the zombies, the rest retreat with their stolen goods. A reanimated Stephen (apparently knowing enough to remember the false wall) breaks through it and leads the undead to Francine and Peter. As Stephen enters their hideout, Peter kills him while Francine escapes to the roof.  Peter then locks himself in a room and contemplates suicide. When zombies burst into the room, he has a change of heart and fights his way up to the roof, where he joins Francine. The two then fly away in the partially fueled helicopter to an uncertain future.

===Alternate ending===
According to the original screenplay, Peter and Francine were to kill themselves, Peter by shooting himself and Fran by driving her head into the spinning helicopter blades. The ending credits would run over a shot of the helicopter blades turning until the engine winds down, implying that Peter and Fran would not have gotten far if they had chosen to escape.  During production it was decided to change the ending of the film.

Much of the lead-in to the two suicides remains in the film, as Francine leans out of the helicopter upon seeing the zombies approach and Peter puts a gun to his head, ready to shoot himself. An additional scene, showing a zombie having the top of its head cut off by the helicopter blades (thus foreshadowing Francines suicide) was included early in the film. Romero has stated that the original ending was scrapped before being shot, although behind the scenes photos show the original version was at least tested. The head appliance made for Frans suicide was instead used in the opening SWAT raid, made-up to resemble an African-American male and blown apart by a shotgun blast. 

==Cast==
 
* David Emge as Stephen "Flyboy" Andrews
* Ken Foree as Peter Washington
* Scott Reiniger as Roger "Trooper" DeMarco
* Gaylen Ross as Francine Parker David Crawford as Dr. James Foster David Early as Mr. Sidney Berman Richard France as Dr. Millard Rausch, Scientist Howard Smith as TV Commentator
* Daniel Dietrich as Mr. Dan Givens
* Fred Baker as Police Commander Jim Baffico as Wooley, Maniacal SWAT Cop
* Rod Stoufer as Rod Tucker, Young SWAT Cop on Roof
* Jese del Gre as Old Priest Joe Pilato as Head Officer at Police Dock
* Randy Kovitz as 2nd Officer at Police Dock/Biker (wearing blue beret & sunglasses)
* Ted Bank as 3rd Officer at Police Dock (wearing yellow sunglasses)
* Patrick McCloskey as 4th Officer at Police Dock (smoking cigar)
* Rudy Ricci as The Biker Leader (Radio Operator wearing Nazi Helmet) 
* Tom Savini as Blades, Assistant Head Biker/Mechanic Zombie shot through glass/Zombie hit by truck
* Larry Vaira as Mousey, Tommy-Gun Biker (riding in sidecar)
* Marty Schiff as Moonbaby, Biker (Blades Sidekick) Taso Stavrakis as Sledge, Biker (with Sledgehammer)/Fountain Zombie/Sailor Zombie/Chestburst Zombie 
* Nick Tallo as Jack, Biker (with seltzer bottle)
* Joe Shelby as Martinez, Bandit Leader/Red Shirted Zombie in car/Biker, Van Driver (wearing cowboy hat & glasses)
* Tony Buba as Pedro, Biker (wearing sombrero) 
* Pasquale Buba as Biker (wearing serape) 
* Butchie as Butchie, Wild Haired Bearded Biker (riding Harley-Davidson Panhead Bike with sidecar)/Biker (Timmys Friend)
* Barbara Lifsher as Mary "Chickie", Blonde Biker Chick driving Van 
* Dave Hawkins as Biker 
* Tom Kapusta as Biker (wearing black biker cap hat & glasses)
* Ken Nagin as Pendant Headband Biker (with axe)
* Gary Hartman as Blonde Biker (wearing black helmet & olive-green jacket)
* Lenny Roman as White Headband Biker riding Harley Motorcycle 
* George Heake as Biker with Long Hair
* Jeff Paul as Biker who shoots Flyboy
* Bobby Dauk as Biker
* Katherine Kolbert as Brunette Biker Chick (throwing pies & cakes)
* Zilla Clinton as Blonde Biker Chick (riding motorcycle)
* Cindy Roman as Blue Bandana Biker Chick riding with White Headband Biker
* Billie Walters as Biker Chick (wearing brown headband)
* Vickie Walters as Biker Chick 
* Jeanette Lansel Vaira as Biker Chick
 

==Development==
===Pre-production===
The history of Dawn of the Dead began in 1974, when George A. Romero was invited by friend Mark Mason of Oxford Development Company—whom Romero knew from an acquaintance at his alma mater, Carnegie Mellon—to visit the Monroeville Mall, which Masons company managed. After showing Romero hidden parts of the mall, during which Romero noted the bliss of the consumers, Mason jokingly suggested that someone would be able to survive in the mall should an emergency ever occur.     With this inspiration, Romero began to write the screenplay for the film.

Romero and his producer, Richard P. Rubinstein, were unable to procure any domestic investors for the new project. By chance, word of the sequel reached Italian horror director Dario Argento. A fan of Night of the Living Dead and an early critical proponent of the film, Argento was eager to help the horror classic receive a sequel. He met Romero and Rubinstein, helping to secure financing in exchange for international distribution rights.  Argento invited Romero to Rome so he would have a change of scenery while writing the screenplay.  The two could also then discuss plot developments.  Romero was able to secure the availability of Monroeville Mall as well as additional financing through his connections with the malls owners at Oxford Development.   Once the casting was completed, principal shooting was scheduled to begin in Pennsylvania on November 13, 1977.

===Production=== Monroeville Mall. Use of an actual, open shopping mall during the Christmas shopping season caused numerous time constraints. Filming began nightly once the mall closed, starting at 11 PM and ending at 7 AM, when automated music came on. As December arrived, the production decided against having the crew remove and replace the Christmas decorations—a task that had proved to be too time consuming. Filming was shut down during the last three weeks of the year to avoid the possible continuity difficulties and lost shooting time. Production would resume on January 3, 1978.  During the break in filming, Romero took the opportunity to begin editing his existing footage. 
 East Liberty district of Pittsburgh at the time.

Principal photography on Dawn of the Dead ended February 1978, and Romeros process of editing would begin.  By using numerous angles during the filming, Romero allowed himself an array of possibilities during editing—choosing from these many shots to reassemble into a sequence that could dictate any number of responses from the viewer simply by changing an angle or deleting or extending portions of scenes. This amount of superfluous footage is evidenced by the numerous international cuts, which in some cases affects the regional versions tone and flow.

====Make-up and effects====
    Tom Savini, who had been offered the chance to provide special effects and make-up for Romeros first zombie film, Night of the Living Dead, before being drafted into the Vietnam War, made his debut as an effects artist on Dawn of the Dead.   Slasherama.com  Savini had been known for his make-up in horror for some time, prior to Dawn of the Dead, and in his book explaining special effects techniques, Bizarro, explains how his time in Vietnam influenced his craft.    He had a crew of eight to assist in applying gray makeup to two to three hundred extras each weekend during the shoot. {{cite web |last=Mason|first=R.H.|url=http://web.archive.org/web/20091026220308/http://www.geocities.com/zodiaczombie/joeinterviews.html
|title=An Interview With The Villain |publisher=Fangoria (reprinted)
|accessdate=May 12, 2008|archiveurl=http://web.archive.org/web/20091023044908/http://geocities.com/zodiaczombie/joeinterviews.html|archivedate=October 23, 2009}}  One of his assistants during production was Joseph Pilato, who played a police captain in the film and would go on to play the lead villain in the films sequel, Day of the Dead. 

The makeup for the multitudes of extras in the film was a basic blue or gray tinge to the face of each extra. Some featured zombies, who would be seen close-up or on-screen longer than others, had more time spent on their look. Many of the featured zombies became part of the fanfare, with nicknames based upon their look or activity—such as Machete Zombie,    GamingReport.com  Sweater Zombie,  and Nurse Zombie.  "Sweater zombie" Clayton Hill, was described by a crew member as "one of the most convincing zombies of the bunch" citing his skill at maintaining his stiff pose and rolling his eyes back into his head, including heading down the wrong way in an escalator while in character. 

A cast of Gaylen Ross head that was to be used in the original ending of the film (involving a suicide rather than the escape scene finally used) ended up as an exploding head during the tenement building scene. The head, filled with food scraps, was shot with an actual shotgun to get the head to explode.  One of the unintentional standout effects was the bright, fluorescent color of the fake blood that was used in the film.  Savini was an early opponent of the blood, produced by 3M, but Romero thought it added to the film, claiming it emphasised the comic book feel of the movie. 

====Music====
 
The films music varies with Romeros and Argentos cuts. For Romeros theatrical version, musical cues and selections were chosen from the De Wolfe Music Library, a compilation of stock music scores and cues. In the montage scene featuring the hunters and National Guard, the song played in the background is "Cause Im a Man" by the Pretty Things; the song was first released on the groups LP Electric Banana.  The music heard playing in a sequence in the mall and over the films end credits is an instrumental titled "The Gonk"—a polka style tune from the De Wolfe Music Library, with a chorus of zombie moans added by Romero. 
 Goblin (incorrectly credited as "The Goblins") extensively. Goblin is a four-piece Italian band that mostly provides contract work for film soundtracks. Argento, who received a credit for original music alongside Goblin, collaborated with the group to get music for his cut of the film.  Romero used three of their pieces in his theatrical release version. The Goblin score would later find its way onto a Dawn of the Dead-inspired film, Hell of the Living Dead.  The version of Dawn released on video in the mid-nineties under the label "Directors Cut" does not use most of the Goblin tracks, as they had not been completed at the time of that edit.

===Post-production and releases=== X from the association because of its graphic violence. Rejecting this rating, Romero and the producers chose to release the film unrated to help the films commercial success.  United Film Distribution Company eventually agreed to release it domestically in the United States. It premiered in the US in New York City on April 20, 1979.   on IMDb.com  The film was refused classification in Australia twice: in its theatrical release in 1978 and once again in 1979. The cuts presented to the Australian Classification Board were Argentos cut and Romeros cut, respectively. Dawn of the Dead was finally passed in the country with an R18+ rating in February 1980.  It was banned in Queensland until at least 1986.

Internationally, Argento controlled the Euro cut for non-English speaking countries. The version he created clocked in at 119 minutes. It included changes such as more music from Goblin than the three cuts completed by Romero, removal of some expository scenes, and a faster cutting pace.  Released in Italy in September 1978, it actually debuted nearly nine months before the US theatrical cut.  In Italy it was released under the full title Zombi: L’alba dei Morti Viventi, followed in March 1979 in France as Zombie: Le Crépuscule des Morts Vivants, in Spain as Zombi: El Regreso de los Muertos Vivientes, in the Netherlands as Zombie: In De Greep van de Zombies, in Germany by Constantin Film as Zombie, and in Denmark as Zombie: Rædslernes Morgen.  
 Golden Screen Award, given to films that have at least three million admissions within 18 months of release.  A majority of these versions were released on DVD in the 2004 Special Edition, and have previously been released on VHS. The freelance photographer Richard Burke, working for Pittsburgh Magazine, released in May 2010 the first exclusive Behind-the-Scenes photos from the set.  

==Reception==
Dawn of the Dead premiered theatrically in New York City on April 20, 1979, and a month afterward in Los Angeles, California on May 11, 1979. Distribution and Release Date information for Dawn of the Dead at  . Retrieved December 10, 2009. 

===Box office===
Dawn of the Dead performed well thanks both to commercial advertising and word-of-mouth. Ad campaigns and posters declared the film "the most intensely shocking motion picture experience for all times".  , a review by Steve Biodrowski for Cinefantastique  The film earned $900,000 on its opening weekend in the United States (total estimate at 5 million), an international gross of 40 million, followed by a worldwide gross revenue of $55 million, making it the most profitable film in the Dead series.  

===Critical reception=== film reviews since its initial release. The film was regarded by many as one of the best films of 1978,     and it holds a 95% approval rating on Rotten Tomatoes based on 36 reviews.  The 25th anniversary issue of Fangoria named it the best horror film of 1979 (although it was released a year earlier),  and Entertainment Weekly ranked it #27 on a list of "The Top 50 Cult Films."  Roger Ebert of the Chicago Sun-Times gave it four out of four stars and proclaimed it "one of the best horror films ever made." While conceding Dawn of the Dead to be "gruesome, sickening, disgusting, violent, brutal and appalling," Ebert said that "nobody ever said art had to be in good taste."  Steve Biodrowski of Cinefantastique praised the film, calling it a "broader" version of Night of the Living Dead,  and gave particular credit to the acting and themes explored: "the acting performances are uniformly strong; and the script develops its themes more explicitly, with obvious satirical jabs at modern consumer society, as epitomized by the indoor shopping mall where a small band of human survivors take shelter from the zombie plague sweeping the country." He went on to say that Dawn of the Dead was a "savage (if tongue-in-cheek) attack on the foibles of modern society", showcasing explicit gore and horror and turning them into "a form of art". 

Dawn of the Dead was not without its detractors. Similar to the preceding Night of the Living Dead, some critical reviewers did not like the gory special effects. Particularly displeased at the large amount of gore and graphic violence was   wrote that, in contrast to the "truly frightening" Night of the Living Dead, "you begin to laugh with relief that youre not being emotionally challenged or even affected;   just a gross-out."    Leslie Halliwell of Halliwells Film Guide stated the film was "occasionally laughable, otherwise sickening or boring."

Dawn of the Dead is now widely considered a classic.    The film was selected as one of The 500 Greatest Movies of All Time by Empire (magazine)|Empire magazine in 2008.  It was also named as one of The Best 1000 Movies Ever Made, a list published by The New York Times. 

==Home media==
In 2004, after numerous VHS, Laserdisc and DVD releases of several different versions of the film from various companies, Anchor Bay Entertainment released a definitive Ultimate Edition DVD box set of Dawn of the Dead, following a single-disc U.S. theatrical cut released earlier in the year. The set features all three widely available versions of the film, along with different commentary tracks for each version, documentaries and extras.  Also re-released with the DVD set was Roy Frumkes Document of the Dead, which chronicled the making of Dawn of the Dead and Romeros career to that point. The Ultimate Edition earned a Saturn Award for Best Classic Film Release. 
 high definition Arrow Video which includes the theatrical cut and 2 DVDs with the Cannes and Argento cut.

==Remake==
  remake was cameos from original cast members Ken Foree, Scott Reiniger and Tom Savini.

==Book==
The Paperback based on the film was written by George A. Romero and will release May 26, 2015 over Gallery Books. 

==See also==
* List of American films of 1978
* List of zombie films
* Dawn of the Dead (2004 film)|Dawn of the Dead (2004 film)
* Dawn of the Dead (soundtracks)|Dawn of the Dead (soundtracks)
* Dawn of the Dead in popular culture|Dawn of the Dead in popular culture
* The Return of the Living Dead

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 