Oh Mary Be Careful
{{infobox film
| name           = Oh Mary Be Careful
| image          = 
| imagesize      =
| caption        =
| director       =  
| producer       = Goldwyn Pictures
| writer         = George Weston (story, scenario) ?Rupert Hughes (intertitles)
| starring       = Madge Kennedy
| music          =
| cinematography = William Fildew
| editing        =
| distributor    = Pioneer Film Company
| released       = September 1, 1921
| runtime        = 50 minutes; 5 reels
| country        = United States Silent (English intertitles)
}}
Oh Mary Be Careful is an extant 1921 American silent comedy film produced by Goldwyn Pictures and released by an independent distributor. Stage actress Madge Kennedy stars in the film. A copy is preserved at the Library of Congress.   

The film was allegedly shot several years earlier and released in 1921.

==Cast==
*Madge Kennedy - Mary Meacham
*George Forth - Morgan Smith
*George S. Stevens - Judge Adams
*Bernard Thornton - Dick Lester
*A. Drehle - Doctor Chase
*Marguerite Marsh - Susie
*Harry Fraser - Professor Putnam
*Dixie Thompson - Luke
*Mae Rogers - Nellie Burns
*Kathleen McEchran - Kate Lester
*Harry Myers - Bobby Burns

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 