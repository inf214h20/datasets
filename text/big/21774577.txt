Luxury Liner (film)
{{Infobox film
| name           = Luxury Liner
| image_size     =
| image	         = 
| caption        =
| director       = Richard Whorf
| producer       = Joe Pasternak
| screenplay     = Gladys Lehman Richard Connell Karl Kamb
| based on       = 
| starring       = George Brent Jane Powell Lauritz Melchior
| music          = Adolph Deutsch
| cinematography = Robert H. Planck
| editing        = Robert Kern
| distributor    = Metro-Goldwyn-Mayer
| released       = 9 September 1948
| runtime        = 97-99 min.
| country        = United States English
| budget         = $2,178,000  . 
| gross          = $4,128,000 

}}Luxury Liner is a 1948 romantic musical comedy film made by MGM in Technicolor. It was directed by Richard Whorf, and written by Richard Connell, Karl Kamb and Gladys Lehman. It was originally titled Maiden Voyage. 
 Luxury Liner (1933), starred George Brent and Zita Johann, and was directed by Lothar Mendes.

==Plot summary==
 

==Cast==
* George Brent as  Captain Jeremy Bradford
* Jane Powell as  Polly Bradford
* Lauritz Melchior as  Olaf Eriksen
* Frances Gifford as  Laura Dene
* Marina Koshetz as  Zita Romanka
* Xavier Cugat as Himself
* Thomas E. Breen as  Denis Mulvy
* Richard Derr as  Charles G.K. Worton
* John Ridgely as Chief Officer Carver
* Connie Gilchrist as Bertha
* The Pied Pipers as Themselves
* Jane Isbell as Girl (uncredited)

==Reception==
The film was a hit and earned $2,297,000 in the US and Canada, and $1,831,000 overseas, resulting in a profit of $428,000. 

== References ==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 

 

 