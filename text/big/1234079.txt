A Night in Casablanca
{{Infobox film
| image = A Night in Casablanca.jpg
| caption = Theatrical release poster.
| director = Archie Mayo
| producer = David L. Loew
| writer =
| starring = Groucho Marx Harpo Marx Chico Marx Charles Drake
| music = Bert Kalmar Harry Ruby Werner Janssen
| cinematography =
| editing =
| distributor = United Artists
| released =  
| runtime = 85 minutes
| country = United States
| language = English
| budget =
}}

A Night in Casablanca (1946) is the twelfth Marx Brothers film|movie, starring Groucho Marx, Chico Marx, and Harpo Marx. The picture was directed by Archie Mayo and written by Joseph Fields and Roland Kibbee, and is generally considered one of the better of the Marx Brothers later films.   

==Plot==
Set in Casablanca shortly after World War II, escaped Nazi war criminal Heinrich Stubel (Sig Ruman) has steadily murdered three different managers of the Hotel Casablanca. Disguised as a Count Pfferman, Stubels goal is to reclaim the stolen art treasures that he has hidden in the hotel. However, the only way he can do this undetected is by murdering the hotels managers and running the hotel himself.

The newest manager of Hotel Casablanca is former motel proprietor Ronald Kornblow (Groucho), who is very much unaware that he has been hired because no one else will dare take the position. Inept Kornblow takes charge of the hotel, and eventually crosses paths with Corbaccio (Chico), owner of the Yellow Camel company, who appoints himself as Kornblows bodyguard, aided and abetted by Stubels valet Rusty (Harpo). In his many efforts to murder Kornblow, Stubel sends beautiful Beatrice Reiner (Lisette Verea) to romance the clueless manager.

Before Stubel can make his escape to the airfield with the loot, Kornblow, his friends, and Miss Reiner invade his hotel room and sneak from suitcase to closet and back again to unpack his bags, which serves to drive him thoroughly mad. Arrested on false charges, Kornblow, Corbaccio and Rusty eventually crash Stubels plane into a police station where the brothers expose Stubel as an escaped Nazi.

==Cast==
* Groucho Marx as Manager Ronald Kornblow
* Harpo Marx as Rusty
* Chico Marx as Corbaccio
* Sig Ruman as Count Pfferman/Heinrich Stubel
* Charles Drake as Lieutenant Pierre Delmar
* Lois Collier as Annette
* Lisette Verea as Beatrice Reiner Lewis L. Russell as Governor Galoux
* Dan Seymour as Prefect of Police Captain Brizzard
* Frederick Giermann as Kurt
* Harro Mellor as Emile David Hoffman as Spy Paul Harvey as Mr. Smythe

==Controversy== Night and A Night A Day at the Races.

The true story is that the original storyline for the film was intended to be a direct parody of Casablanca, with the characters having similar sounding names to the characters and actors in the 1942 film. Groucho Marx has said that an early draft named his character "Humphrey Bogus", a reference to the leading actor in Casablanca, Humphrey Bogart.  Warner Bros. did not actually litigate, or even threaten to litigate, but it did issue a formal inquiry to the Marx Brothers concerning the plot and script of the film. 

The Marx Brothers exploited the situation for publicity, making it appear to the public that a frivolous lawsuit was in the works, and Groucho sent several open letters to Warner Bros. to get newspaper coverage.  These letters were among those he donated to the Library of Congress, and he reprinted them in his book The Groucho Letters, which he published in 1967. 

In the end, the matter died without legal action, and the storyline of the film was changed to be a send-up of the genre rather than Casablanca specifically.    Warner Bros. now owns the distribution rights to this film via Castle Hill Productions.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 