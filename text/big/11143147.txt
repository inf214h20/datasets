Anbu Thangai
{{Infobox film
| name           = Anbu Thangai
| image          = AnbuThangaifilm.jpg
| image_size     = 
| caption        = Vinyl Records Cover
| director       = SP. Muthuraman
| producer       = Gowri Art Enterprises
| writer         =
| narrator       = Muthuraman J. Jayalalitha Srikanth Srikanth Suruli Rajan Jaya Kamal Haasan
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| distributor    =
| released       = 30 August 1974
| runtime        =
| country        = India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Anbu Thangai is a Tamil language film directed by SP. Muthuraman, starring Muthuraman and Jayalalitha in the lead roles. It has Jaya as Muthuramans sister and Jayalalitha as his love interest. Srikanth played an important role as Muthuramans friend. Kamal Haasan plays a guest role in the film as Buddha in a dance sequence with Jayalalitha.

==Cast==
 Muthuraman
*J. Jayalalitha
*Major Sundarrajan Srikanth
*Suruli Rajan
*Jaya
*Kamal Haasan

==Trivia==

Kamal Haasan choreographed a song for Jayalalithaa and himself in Anbu Thangai. He has also choreographed for MGR in Naan Yen Pirandhen and Sivaji Ganesan in Savaale Samaali in his earlier career. 

==References==
 

==External links==
 

 

 
 
 
 
 
 
 


 