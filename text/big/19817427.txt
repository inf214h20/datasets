Rover's Big Chance
{{Infobox Film
| name           = Rovers Big Chance             
| image          = 
| image_size     = 
| caption        = 
| director       = Herbert Glazer
| producer       = Metro-Goldwyn-Mayer
| writer         = Hal Law Robert A. McGowan
| narrator       = 
| starring       = 
| music          = 
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 49"
| country        = United States 
| language       = English
| budget         = 
}}
 short comedy film directed by Herbert Glazer.  It was the 208th Our Gang short (209th episode, 120th talking short, 121st talking episode, and 40th MGM produced episode) that was released.

==Plot==
Future film star Stephen McNally (here billed as Horace McNally) appears in this Our Gang episode as Bill Patterson, ace director at Mammoth Studios. On the say-so of studio casting director J.D. Broderick, Patterson agrees to give a screen test to the Our Gang kids talented dog Rover. Alas, the petulant pooch does not take direction well, nor does he respond positively when the cameraman announces that hes "ready to shoot." 
   

==Note== Robert Blake. He retained the name "Mickey" for the remainder of the series.
*Rovers Big Chance was the second MGM Our Gang short to lose money upon its initial release, losing nearly $1,800 after post-production costs were calculated.

==Cast==
===The Gang=== Robert Blake as Mickey
* Janet Burston as Janet
* Billy Laughlin as Froggy
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* Horace Stephen McNally as Wm. "Bill" Patterson
* Freddie Chapman as Tony
* Clyde Demback as Fatty Bobby Anderson as Baseball player
* Billy Finnegan as Baseball player
* Bert Le Baron as Grip Barbara Bedford as Studio clerk Ben Hall as George 
* Hugh McCormick as Professor Ventriloko
* Byron Shores as J.D. Broderick

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 