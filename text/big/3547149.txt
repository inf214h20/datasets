The Raspberry Reich
 
{{Infobox Film 
| name = The Raspberry Reich
| image = Raspberry_Reich_LaBruce.jpg
| caption = U.S. release DVD cover art
| director = Bruce LaBruce
| producer = Jürgen Brüning
| writer = Bruce La Bruce
| narrator = 
| starring = Susanne Sachsse Daniel Bätscher Andreas Rupprecht Dean Monroe Anton Dickson Daniel Fettig Ulrike Schirm Sherry Vine
| music = 
| cinematography = James Carman Kristian Petersen
| editing = Jörn Hartmann
| distributor = Peccadillo Pictures (UK)
| released = 
| runtime = 90 mins
| country = Germany/Canada English
| budget = 
| gross          = $31,211 
| preceded_by = 
| followed_by = 
}}
 2004 film terrorist chic", cult dynamics, and the "innate radical potential of homosexual expression".  It is about a contemporary terrorist group who set out to continue the work of the Red Army Faction (RAF), also known as the Baader-Meinhof Gang. The group consists of several young men, and a female leader named Gudrun (after Gudrun Ensslin). All of the characters are named after original members of the Baader-Meinhof Gang or revolutionaries such as Che Guevara.

They call themselves the "Sixth Generation of the Baader-Meinhof Gang" and "The Raspberry Reich". "Reich" is a reference to communist sexologist Wilhelm Reich. In addition, the term "Raspberry Reich" was coined by RAF leader Gudrun Ensslin to refer to the oppression of consumer society.   An "uncut" version of the film has been released, titled The Revolution Is My Boyfriend, edited by the gay pornographic film company Cazzo Film including erotic scenes edited out in the original version. {{cite journal
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = The Revolution Is My Boyfriend
  | journal = Les Inrockuptibles
  | volume = 
  | issue = 661–663
  | pages = 100
  | publisher = Les Editions Indépendantes 
  | location = Paris, France
  | date = 2008-07-29
  | url = 
  | format = 
  | issn = 0298-3788
  | accessdate = }} 

==Plot==

The core plot begins with the  ".
 urban guerrillas escape into the night. In the dénouement, the characters are visited some time later. Several have found happiness in the homosexual relationships established during their revolutionary activities. Che has become a terrorist trainer in the Middle East. Patrick escapes with Clyde, where they embark on a spree of bank robberies. This action is reminiscent of Patty Hearsts actions with the Symbionese Liberation Army|SLA. Gudrun and Holger settle down and have a child named Ulrike (after Ulrike Meinhof), whom Gudrun believes could embody the next generation of the Red Army Faction.

==Style==
The films style is propagandistic. The actors are placed in rooms wallpapered with photographs of Gudrun Ensslin, Ulrike Meinhof, Andreas Baader, and Che Guevara. This symbolises the historical connection to the RAF. At several points during the film, the action pauses while the characters recite long passages from Raoul Vaneigem|Vaneigems The Revolution of Everyday Life, as though they are speaking from their own thoughts. Onscreen titles are also used to convey and enhance political messages.

Pornography plays a large visual role in the film. The opening sequence features a montage of sexual acts involving the two main characters, Gudrun and Andreas. There are also plot driven sex scenes involving the characters of Patrick and Clyde engaging in real explicit onscreen oral and anal sex acts with each other. Much of these scenes intentions are to not only arouse but also present themes of satire and of character development.

===Slogans===
Slogans are used to convey the politics of the Raspberry Reich. They are both an extension and parody of the slogans used by political organisations. Slogans used in the film include:

*  "The Revolution is my boyfriend!" up against the wall, motherfucker!" 
*  "Fuck me for the Revolution!"
*  "Are you revolutionary enough to give up your girlfriends?"  
*  "Join the homosexual intifada!"
*  "No revolution without sexual revolution. No sexual revolution without homosexual revolution." into the streets!" opiate of the masses" Madonna is counter-revolutionary!"; "Cornflakes are counter-revolutionary!"; "Masturbation is counter-revolutionary!".

===Satire=== meat is multinational corporate fast food drive through with their victim still in the trunk.

==See also==
*  , another political film which explores and relies heavily upon sexual imagery.

==References==
 

==External links==
*  
*   movie website
*   - interview about the film

 
 
 
 
 
 
 
 
 
 
 
 