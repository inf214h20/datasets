Voyage en Chine
{{Infobox film
| name           = Voyage en Chine
| image          = 
| caption        = 
| director       = Zoltan Mayer
| producer       = Carole Scotta Julie Billy Natacha Devillers Simon Arnal Caroline Benjo
| writer         = Zoltan Mayer
| based on       = 
| starring       = Yolande Moreau
| music          = 
| cinematography = Georges Lechaptois		
| editing        = Camille Toubkis
| distributor    = Haut et Court
| studio         = Haut et Court France 3 Cinéma
| released       =  
| runtime        = 96 minutes
| country        = France China
| language       = French
| budget         = $2,4 million
| gross          = $1,038,187 
}}
Voyage en Chine (Travel to China) is a 2015 French drama directed by Zoltan Mayer. 

==Plot==
Liliane goes in China for the first time in her life to repatriate the body of her son, who died in an accident. Immersed in this culture so long ago, this trip marked by mourning becomes a journey of initiation.

==Cast==
 
* Yolande Moreau as Liliane
* Jingjing Qu as Danjie
* Dong Fu Lin as Chao
* Ling Zi Liu as Li Shu Lan
* Qing Dong as Ruo Yu
* Yilin Yang as Yun
* André Wilms as Richard
* Chenwei Li as Master Sanchen
* Sophie Chen as Mademoiselle Yang
 

==Production==
The movie was shot in China.

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 