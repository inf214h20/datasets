Singles (1992 film)
{{Infobox film
| name           = Singles
| image          = Singles poster.jpg
| caption        = Theatrical release poster
| director       = Cameron Crowe
| producer       = Cameron Crowe Richard Hashimoto
| writer         = Cameron Crowe
| starring       = Bridget Fonda Campbell Scott Kyra Sedgwick Matt Dillon
| music          = Paul Westerberg
| cinematography = Tak Fujimoto Ueli Steiger
| editing        = Richard Chew
| distributor    = Warner Bros.
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $9 million
| gross          = $18,471,850
}}
Singles is a 1992 American romantic comedy film written, co-produced, and directed by Cameron Crowe. The film stars Bridget Fonda, Campbell Scott, Kyra Sedgwick, and Matt Dillon.

==Plot== Sheila Kelley as Debbie Hunt, who is trying to find Mr. Right - a man who would make an ideal romantic partner - by making a video to express her desire. The events of the film are set against the backdrop of the early 1990s grunge movement in Seattle.

In the end, aside from some setbacks, Debbie meets her perfect significant other at an airport, Linda and Steve finally commit to each other (Steve leaves the apartment block to be with Linda), and Cliff accepts Janet after realizing that she loved him all along. As the camera pans away from the apartment building, voices of various people comment on how relationships would work and whether they like someone or if they like them.

==Cast==
*Bridget Fonda – Janet Livermore
*Campbell Scott – Steve Dunne
*Kyra Sedgwick – Linda Powell Sheila Kelley – Debbie Hunt
*Jim True-Frost – David Bailey
*Matt Dillon – Cliff Poncier
*Bill Pullman – Dr. Jeffrey Jamison
*James LeGros – Andy
*Ally Walker – Pam
*Tom Skerritt – Mayor Weber
*Jeremy Piven – Doug Hughley
*Eric Stoltz – the mime
*Tim Burton – Brian
*Peter Horton – Jamie
*Devon Raymond – Ruth
*Camilo Gallardo – Luiz 
 Robin Wright Penn were all under consideration before Kyra Sedgwick won the part.

There are brief and early appearances from actors Victor Garber, Paul Giamatti, Jeremy Piven and Eric Stoltz (whom Crowe has said is in all of his films, and who in this film plays the loudmouthed mime), and a rare onscreen appearance from director Tim Burton. Cameron Crowe himself has a cameo as a rock journalist at a club.
 Tad and Hog Molly). Stone Gossard, Jeff Ament, and Eddie Vedder, all members of Pearl Jam, have small parts as members of Matt Dillons character Cliff Ponciers fictional band Citizen Dick. Their parts were filmed when Pearl Jam was known as Mookie Blaylock. Soundgarden frontman Chris Cornell has a cameo as the guy who comes out to listen to a car radio. He also appears in a later scene with his band Soundgarden performing the song "Birth Ritual". The members of Alice in Chains also appear in the film as a bar band, playing the songs "It Aint Like That" and "Would?". Longtime Seattle sportscaster Wayne Cody also makes a cameo.

==Production== Capitol Hill, Greenwood Memorial Renton and Pike Place Market. The central coffee shop featured in the film is the now-closed OK Hotel. The apartment building is located on the northwest corner of the intersection of E Thomas St & 19th Ave E. Additional concert footage was shot in the now-defunct RKCNDY bar.
 acoustic version Touch Me, Im Sick" by the Seattle band Mudhoney. Also, in the inside cover photo of the Singles (soundtrack)|soundtrack, there is a Citizen Dick CD with the track listing on the CD itself. One of the songs is called "Louder Than Larry (Steiner)", a wordplay on the Soundgarden album, Louder Than Love. The band name Citizen Dick is a play on the Seattle band Citizen Sane, which itself is a play on the 1941 film, Citizen Kane.

==Reception==
Singles holds an 80% critical approval rating on the review aggregator Rotten Tomatoes based on 49 reviews.  
 The Stranger was less kind to Crowes use of the local background, reviewing "hes relying on the general hipness of our little burg and on the star power of a few local musicians/bit actors to make a bundle of dough, and he hasnt bothered to back them up with anything worth remembering. Pleasant is about the only word I can think of to describe the thing."     

While completed in early 1991, the film was not released until September 1992. The films release went through repeated delays while studio executives debated how to market it. Warner Bros. did not know what to do with the film, but after the grunge scene exploded, the movie was finally released.
Warner Bros. Television tried immediately to turn Singles into a television series. Crowe claims that Singles inspired the television series Friends,  which ran successfully on NBC from 1994-2004. 

==Soundtrack==
  The Replacements contributed two songs to the soundtrack and provided the score for the film. The Smashing Pumpkins also contributed to the soundtrack with the song "Drown (The Smashing Pumpkins song)|Drown".

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   – A log kept by Crowe during the production of Singles and published in Rolling Stone in October 1992

 

 
 
 
 
 
 
 
 
 
 
 