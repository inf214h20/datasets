Audition (2007 film)
{{Infobox film
| name           = Audition
| image          =|
| image size     =
| caption        = Official Poster
| director       = Sam Holdren
| producer       = Lauren Kuznick
| writer         = Joseph W. Ng Sam Holdren
| starring       = Todd A. Waters Natasha Lee Martin Mark Kochanowicz Lewis Korff Dylan Clements Scott Skversky Hannah Tsapatoris
| music          = Christian Moehring
| cinematography = Seth Mulliken
| editing        = Pete Kruvczuk
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = English
}}
Audition is a short film directed by Sam Holdren, first released to festivals in 2007. The film is a tragic comedy about William Ashe, a grown man who still lives at home with mother and strongly believes in signs.  On the whim of a sign, he travels to the big city to audition for a movie, and is promptly sidetracked by murder, mayhem, and mistaken identity, only proving that life may lead anywhere once you choose to see the signs. 

==Story==
What begins as a story about William Ashe, an obnoxious actor who has come to the city to audition for a movie, soon turns into a murder mystery without the main character ever realizing it.  However, the mystery is a distraction from the real point of order, which is the fact that every sequence of the movie for William—including his trying to impress people in a hotel bar at the beginning, and also his interrogation by the police after a murder has occurred—is a constant audition.  By the time his real audition rolls around near the end of the film—after the solution to the mystery has been revealed to everyone but William—he has spent the entire movie seeking approval that he never gets.  Thus, the movie is a tragic comedy about a guy with hope and energy who is trying, but never quite seals the deal.

==Background==
The character of William Ashe was the result of several meetings in the summer of 2004 between co-writers Joseph W. Ng and Sam Holdren.  Ng pitched a scenario where a main characters zany actions were not very believable to Holdren.  However, when they began sifting through mutual friends in order to analyze what each person would do in the situation presented by Ng, they stumbled upon an actor friend out of Charleston, WV named Ashley Wilhelm, whose earnest and sometimes strong and spontaneous personality seemed a perfect inspiration for the character.   

The inspiration provided by Wilhelm allowed Ng and Holdren to adapt both his persona and elements of their own personalities into the character of William Ashe, a grown man still living at home with mother whose trip to the city to audition for a movie gets him caught up in a mystery without him actually realizing it.  Throughout the film, the character bases his choices upon what he believes are signs, but it becomes increasingly obvious as the movie proceeds that the hovering presence of his mother, who we never see, is evident.

Todd Waters, who portrayed William Ashe, didnt actually know that the character was based in part on a real person until late in the filming.  Holdren introduced Waters and Wilhelm in March 2005 while the film crew shot the final sequence in downtown Charleston, West Virginia.  Wilhelm appears briefly at the end of the film as the last person to come into contact with the main character. 

Wilhelm, who still continues to act, recently won the prestigious South Eastern Theatre Conferences Best Play of 2008 Award with The Exonerated. Written by Jessica Blank and Erik Jensen, the play is a true story of six wrongfully convicted people who spent years on death row and were exonerated through newly discovered DNA evidence, confessions and wire taps.

Wilhelms part, Male Ensemble #1, consisted of 12 characters throughout the production that entailed multiple facets of the criminal justice system, to one of the accused and other various roles in The Exonerated.

Wilhelm immersed himself into the roles of each character and even worked as a Corrections Officer with the Division Of Juvenile Services during the one year running of The Exonerated.  For Wilhelms legal characters, he spent many hours at both the county and federal courthouses becoming acquaintances with Kanawha County Prosecutors, Judges and Police Officers to delve into the criminal justice system.

Wilhelm still continues to act and audition in NYC, West Virginia, Kentucky and Ohio and is scheduled to appear in a new feature length film in 2009.  Wilhelm still resides in South Charleston, West Virginia with his older brother who both help with the care of their mother, Bette.

== Locations ==
Most of the movie was shot in different sections of  ; Temple University; and the former Marmont Bar & Grill.   The ending sequence, however, was shot months later in Charleston, West Virginia in front of the historic Capitol Theatre, now known as the WV State University Capitol Center Theater Complex. 

== Awards ==
Best Student Short Film & Best Regional Student Film - 2007 - Bluegrass Independent Film Festival    
Award of Excellence - 2007 - West Virginia Filmmakers Film Festival 

== Festival screenings ==

* Appalachian Film Festival 
* Bare Bones International Film Festival 
* Bluegrass Independent Film Festival 
* Route 66 Film Festival  
*   
* Delaware Valley Film Festival  
* Colony Film Festival  
* Macon Film & Video Festival 
* Fargo Film Festival
* Kent Film Festival 
* IU South Bend Independent Filmmakers Festival 
* Johns Hopkins Film Festival 
* Wildwood By The Sea Film Festival 
* Flint Film Festival 

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 
 