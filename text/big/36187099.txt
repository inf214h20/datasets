Heremias
{{Infobox film
| name           =Heremias
| image          =
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Lav Diaz
| producer       = Celso De Guzman
| writer         = Lav Diaz
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Ronnie Lazaro
| music          =Earl Drilon
| cinematography = Tamara Benitez
| editing        = Lia Martinez
| studio         = 
| distributor    = 
| released       =
| runtime        = 
| country        = 
| language       = Filipino, Tagalog 
| budget         = 
| gross          = 
}} indie crime drama film directed and written by Lav Diaz. It stars Ronnie Lazaro, Jordan and Sid Lucero as a bad cop.  |date=26 September 2006|accessdate=19 June 2012}}  The 9-hour digital film (540-minutes)   |date=1 August 2006|accessdate=19 June 2012}}  was produced by Celso De Guzman, with principal cinematography by Tamara Benitez, and was shot in Pililla, Rizal, Luzon. The film is "the story of a farmer (Ronnie Lazaro) who makes a pact with God to save a girl from rape."  |date=16 February 2006|accessdate=19 June 2012}}  
 	
==Cast==
 
*Ronnie Lazaro 	as Heremias
*Jordan as The Cow
*Sid Lucero as Sgt. Querubin
*Dante Balois  as Mang Tinong 
*Roeder as Jerry
*Perry Dizon as Mando
*Simon Ibarra as Ed
*Mayen Estanero as Wife
*Aero Joy Damaso as Little Girl
*Winston Maique as Allan
*Bart Guingona as Diego
*Yul Servo as Tonio
*Noel Millares as Priest
*Lou Veloso as Mang Teban
*Fonz Deza as Kapitan Fredo
 

==Reception==
At the Fribourg International Film Festival Lav Diaz won the Special Jury Award and was nominated for the Grand Prix Award. 
Discussing Heremias, a reviewer was impressed by the work of Tamara Benitez, calling her "so young and so talented". 

==References==
 

==External links==
*  

 
 
 
 
 
 

 