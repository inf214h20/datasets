Summer and Smoke (film)
 
{{Infobox film
| name           = Summer and Smoke
| image          = Summerandsmoke1961.jpg
| caption        = Film poster
| director       = Peter Glenville
| producer       = Paul Nathan Hal B. Wallis
| writer         = James Poe Meade Roberts Tennessee Williams (play)
| starring       = Laurence Harvey Geraldine Page
| music          = Elmer Bernstein
| cinematography = Charles Lang
| editing        = Warren Low
| distributor    = Paramount Pictures
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         =
}}
 same name. 
 Lee Patrick and Earl Holliman. It was adapted by James Poe and Meade Roberts.The story follows a young reserved girl who meets a doctor who lives on the wild side. They become friends, but the beliefs they hold create difficulties for the relationship.

==Cast==
* Laurence Harvey as John Buchanan, Jr
* Geraldine Page as Alma Winemiller
* Rita Moreno as Rosa Zacharias
* Una Merkel as Mrs. Winemiller
* John McIntire as Dr. Buchanan
* Thomas Gomez as Papa Zacharias
* Pamela Tiffin as Nellie Ewell
* Malcolm Atterbury as Rev. Winemiller Lee Patrick as Mrs. Ewell
* Max Showalter as Roger Doremus (as Casey Adams)
* Earl Holliman as Archie Kramer
* Pepe Hern as Nico

==Awards==
The film was nominated for four Academy Awards:    Best Actress in a Leading Role (Geraldine Page) Best Actress in a Supporting Role (Una Merkel) Best Art Walter Tyler, Samuel M. Comer, Arthur Krams) Best Music, Scoring of a Dramatic or Comedy Picture (Elmer Bernstein).

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 