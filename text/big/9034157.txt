Himmat (1996 film)
{{Infobox film
| name           = 
| image          = Himmat.jpg
| image_size     = 
| caption        = 
| director       = Sunil Sharma
| producer       =
| writer         =
| narrator       =  Tabu Shilpa Shetty Naseeruddin Shah
| music          = Anand-Milind
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1996
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Tabu and Shilpa Shetty. The Film released on January 5, 1996.

==Synopsis==

Two Indian Secret Service Agents, Ajay Saxena and Abdul, are not only co-workers, but also live like brothers and family. Their Chief asks Abdul to take charge of Project M, dealing with Indias nuclear capability for defense, while Ajay proceeds on a vacation to Switzerland. Once there, he meets with beautiful Anju and both fall in love with each other. When he returns to India, he is told that Abdul has been killed, the file pertaining to Project M is missing, and he is assigned the task of not only recovering the file but also apprehending the culprits. Ajay sets out to Bangalore and finds that Anju also lives there with her wealthy businessman dad, Brij Mohan alias BM.. Ajay will soon find out that his romance with Anju is short-lived as her dad may be connected with the masterminds who had the secret file stolen, and are now determined to do away with Ajay as they had with Abdul.

==Cast==
*Sunny Deol ....  Ajay Saxena 
*Sudesh Berry .... Abdul
*Naseeruddin Shah .... Luka  
*Shilpa Shetty ....  Nisha  Tabu ....  Anju
*Gulshan Grover ....  Ranjit 
*Mohan Joshi ....  BM 
*Mukesh Khanna ....  Inspector DSouza   
*Suresh Oberoi .... Blind lottery seller
*Kiran Kumar .... Kundan
*Navin Nischol .... Chief of Secret Service

==Music==
Music: Anand-Milind

===Tracklist===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)  
|-
| 1
| "Mujhse Tujhse Kuchh Kehna Hai"
| Kumar Sanu & Alka Yagnik
|-
| 2
| "Sathiya Bin Ter Dil Mane Naa"
| Kumar Sanu & Alka Yagnik
|-
| 3
| "Behaka Behaka Kadam Hai" Poornima
|-
| 4
| "Mathe Ki Bindia"
| Udit Narayan & Alka Yagnik
|-
| 5
| "Teri Khamoshi Ka Matlaab" Abhijeet & Suresh Wadkar
|-
| 6
| "Kuku Kuru Sun Zara"
| Kumar Sanu, Alka Yagnik
|}

==External links==
*  

 
 
 
 
 


 
 