3 Women
 
{{Infobox film
| name           = 3 Women
| image          = 3womenposter.jpg
| caption        = Theatrical poster
| director       = Robert Altman
| producer       = Robert Altman
| writer         = Robert Altman
| narrator       =
| starring       = Shelley Duvall Sissy Spacek Janice Rule
| music          = Gerald Busby
| cinematography = Chuck Roscher
| editing        = Dennis Hill
| distributor    = 20th Century Fox
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $1.7 million A Dream Movie From Altman
Kilday, Gregg. Los Angeles Times (1923-Current File)   08 Sep 1976: e10 
}}
3 Women is a 1977 American film written and directed by Robert Altman, and starring Shelley Duvall, Sissy Spacek and Janice Rule.

It depicts the increasingly bizarre, mysterious relationship between a woman (Duvall) and her roommate and co-worker (Spacek) in a dusty, underpopulated Californian town. The story came directly from a dream Altman had, which he did not fully understand but nonetheless adapted into a film treatment|treatment, intending to film without a Screenplay|script. 20th Century Fox financed the project on the basis of Altmans reputation. A script was completed before filming, although, as with most Altman films, the script was preliminary for what emerged during production.

Roger Ebert named 3 Women the best film of 1977.

For 27 years, the film was unavailable on home video. It gained the reputation of a cult film after frequent broadcasts on television in the 1980s and 1990s. The film was finally released on DVD in 2004 by the Criterion Collection, with a feature-length commentary by Altman. In 2011, it was released on Blu-ray Disc|Blu-ray, also by Criterion.

==Plot==

Pinky Rose (Sissy Spacek), a timid and awkward young woman, begins a job at a health spa for the elderly in a small California desert town. There, she becomes enamored of Millie Lammoreaux (Shelley Duvall), a confident and talkative employee. Both natives of Texas, the two begin to develop a friendship and, in spite of their stark personality differences, decide to become roommates. Pinky moves in with Millie at the Purple Sage Apartments, owned by a has-been cowboy, Edgar Hart, and his wife Willie (Janice Rule), a mysterious pregnant woman who paints striking and unsettling murals.

Millie takes Pinky along on her evening visits to Dodge City, a local tavern and shooting range also owned by Willie, where Millie talks incessantly. Tensions begin to rise between Pinky and Millie over their living situation; one night, when Millie prepares a dinner party for friends who fail to show up, she gets into a fight with Pinky and leaves the apartment, only to return with a drunk Edgar, and the two have sex. Pinky, distraught, jumps off the apartment balcony into the swimming pool.

Pinky survives the attempt but goes into a coma. Millie is devastated, and begins to visit Pinky daily. When Pinky still doesnt wake up, Millie contacts and invites Pinkys parents in Texas to see if their presence will awaken her. She wakes up, but doesnt recognize her parents and furiously demands that they leave. Once sent home to live with Millie again, Pinky begins to exhibit increasingly uncharacteristic behaviors— she begins drinking and smoking, has an affair with Edgar, and spends her time at the shooting range, just as Millie had.

Millie becomes increasingly frustrated by Pinkys imitative shift in personality, and begins to exhibit Pinkys timid and submissive personality herself. One night after Pinky has a bad dream (represented through an abstract montage of Millie crying and Willies bizarre murals), a drunk Edgar enters their apartment and awakens them, telling them that Willie is about to give birth. The two drive to Edgar and Willies farmhouse, where Willie is alone and in labor, and she gives birth to a Stillbirth|stillborn.

The film ends with Pinky and Millie, who are now working at Dodge City; a delivery vendor at the tavern references Edgars "gun accident" to Millie, who seems unaffected by it, and Pinky appears to have reverted to her childlike timidity; she now refers to Millie as her mother. Pinky and Millie leave the tavern and walk to Willies farmhouse, where the three of them begin to prepare dinner together.

==Cast==
* Shelley Duvall as Mildred "Millie" Lammoreaux
* Sissy Spacek as Mildred "Pinky" Rose
* Janice Rule as Willie Hart
* Robert Fortier as Edgar Hart Ruth Nelson as Mrs. Rose John Cromwell as Mr. Rose
* Sierra Pecheur as Ms. Bunweil
* Craig Richard Nelson as Dr. Maas
* Maysie Hoy as Doris
* Belita Moreno as Alcira
* Leslie Ann Hudson as Polly
* Patricia Ann Hudson as Peggy
* Beverly Ross as Deidre
 Criterion edition, Bodhi Wind).

==Awards== Best Actress at the 1977 Cannes Film Festival for her performance, and from the Los Angeles Film Critics Association. Sissy Spacek won the Best Supporting Actress award from the New York Film Critics Circle.   

==Home media==
For years, the film was not available in home video in any form.  This was alleged to be due to music rights; reportedly, the distributors of Altmans films Images (film)|Images, California Split, 3 Women, and Health (film)|Health, had not negotiated music rights for home video release of the films, and, due to their relative obscurity, they were never expected to be released.

3 Women was the first of these films to be released when The Criterion Collection licensed the rights from 20th Century Fox. The DVD includes an anamorphic transfer in the correct 2.35:1 aspect ratio and a commentary track by Robert Altman.

The film was released on Blu-ray on September 13, 2011.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 