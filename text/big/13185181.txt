Hustling for Health
 
{{Infobox film
| name           = Hustling for Health
| image          = 
| caption        = 
| director       = Frank Terry
| producer       = Hal Roach
| writer         = 
| starring       = Stan Laurel
| music          = 
| cinematography = Robert Doran
| editing        = Thomas J. Crizer
| distributor    = 
| released       =  
| runtime        = 15 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

Hustling for Health is a 1919 American silent film featuring Stan Laurel.   

==Cast==
* Stan Laurel - The Man
* Pearl Elmore - (uncredited)
* Sadie Gordon - (uncredited)
* Rosa Gore - Woman Guest (uncredited)
* Bud Jamison - Mr. Spotless (uncredited)
* Margaret Joslin - Mrs. Spotless (uncredited)
* Jerome Laplauch - (uncredited)
* Belle Mitchell - (uncredited)
* Marie Mosquini - Homeowners wife (uncredited)
* James Parrott - Man misses the train (uncredited)
* Clarine Seymour - Mr. Spotlesss Daughter (uncredited)
* Hazel Powell - (uncredited)
* Catherine Proudfit - (uncredited)
* Frank Terry - Home Owner (uncredited)
* Dorothea Wolbert - Woman Guest (uncredited)
* Noah Young - The Health Inspector (uncredited)

==See also==
* Stan Laurel filmography

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 


 