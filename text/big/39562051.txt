Carol (film)
{{Infobox film
| name           = Carol
| image          = 
| alt            = 
| caption        = 
| director       = Todd Haynes
| producer       = {{Plainlist|
*Elizabeth Karlsen
*Stephen Woolley 
*Tessa Ross 
*Christine Vachon}}
| writer         = Phyllis Nagy
| based on       =  
| starring       = {{Plainlist|
*Cate Blanchett
*Rooney Mara 
*Sarah Paulson 
*Kyle Chandler}}
| music          = Carter Burwell
| cinematography = Edward Lachman
| editing        = 
| studio         =  {{Plainlist|
*Number 9 Films
*Film4 Productions 
*Killer Films}} 
| distributor    = The Weinstein Company
| released       =  
| runtime        = 118 minutes 
| country        = {{Plainlist|
*United Kingdom 
*United States}}
| language       = English
| budget         = 
| gross          = 
}}

Carol is an upcoming British-American romantic drama film directed by Todd Haynes, with a screenplay by Phyllis Nagy based on the novel The Price of Salt (also known as Carol) by Patricia Highsmith. The film stars Cate Blanchett, Rooney Mara, Sarah Paulson and Kyle Chandler. It is set in 1952 in New York City.
 Hyde Park, Lebanon, Ohio|Lebanon, Hamilton, Ohio|Hamilton, and Over-the-Rhine. The film has been selected to compete for the Palme dOr at the 2015 Cannes Film Festival.

==Plot==
In 1950s New York, a young woman in her 20s working as a department-store clerk falls for an older, married woman.

== Cast ==
* Cate Blanchett as Carol Aird
* Rooney Mara as Therese Belivet
* Sarah Paulson as Abby Gerhard
* Kyle Chandler as Harge Aird
* Jake Lacy as Richard
* Cory Michael Smith as Tommy
* John Magaro as Dannie
* Carrie Brownstein as Genevieve Cantrell

== Production ==
=== Development === John Crowley would direct the film, titled Carol, starring Blanchett and Mia Wasikowska as Carol and Therese, respectively. Number 9 Films Elizabeth Karlsen and Stephen Woolley would produce, along with Film4 Productions Tessa Ross, who co-developed the project with Karlsen and Woolley.   On May 23, 2013, Todd Haynes signed on to direct, replacing Crowley who withdrew due to scheduling conflicts. Haynes collaborator Christine Vachon would co-produce the film.  On May 28, 2013 it was announced that The Weinstein Company had acquired the United States distribution rights.  On August 29, 2013, it was reported that Rooney Mara had replaced Wasikowska.  Carter Burwell will score the music for the film, and Edward Lachman would be the director of photography.     On January 22, 2014, Sarah Paulson was cast as Abby, a close friend of Carol;  nine days later, Kyle Chandler was cast as Harge, Carols husband.  On February 4 2014, Cory Michael Smith was cast as Tommy, a charming traveling salesman who meets Blanchett and Mara’s characters on the road.    On February 11, 2014, Jake Lacy joined the cast as Richard, Thereses boyfriend.  On April 8 2014, John Magaro was cast as Dannie, a writer who works at The New York Times.    On April 9, 2014, Carrie Brownstein joined the cast, playing the role of Genevieve Cantrell, a woman who has an encounter with Therese.   

=== Filming === Eden Park Hyde Park, wrapped on April 25, 2014.   

=== Post-production ===
On December 15, 2014, Haynes confirmed deliverables were completed.    In early 2015, Brownstein told Paste Magazine that most of her scenes were cut due to the films length. 

== Release ==

=== Marketing ===
The first official image from Carol, released by Film4, appeared on the London Evening Standard on May 16, 2014.  Despite deliverables being completed in late 2014, the producers decided to withhold the film until 2015 in order to benefit from a film festival launch.  A second image from the film was released on January 5, 2015. 

The film was selected to compete for the Palme dOr at the 2015 Cannes Film Festival.   

==See also==
*New Queer Cinema
*List of lesbian, gay, bisexual or transgender-related films of 2015
*List of lesbian, gay, bisexual, or transgender-related films by storyline

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 