Super Buddies (film)
 
{{Infobox film
| name           = Super Buddies
| image          = 
| caption        = DVD cover art
| director       = Robert Vince
| producer       = Anna McRoberts Robert Vince
| writer         = Anna McRoberts Robert Vince
| based on =  
| starring       = John Ratzenberger Trey Loney Veronica Diaz-Carranza Jay Brazeau Jason Earles
| music          = Brahm Wenger
| cinematography = Mark Irwin
| editing        = Kelly Herron Walt Disney Studios Home Entertainment
| released       =  
| runtime        = 91 minutes
| country        = United States English
}} Disney direct-to-DVD family film, directed by Robert Vince and produced by Anna McRoberts. It is the 7th installment in the Air Bud (series)|Air Buddies franchise. The Buddies acquire rings that give them super powers, now they must use them to stop a villain. The movie was released by Walt Disney Studios Home Entertainment on DVD, Blu-ray Disc|Blu-ray, and as a movie download on August 27, 2013.

==Plot==
The Buddies Budderball, Buddha, Rosebud, B.Dawg, and Mudbud five golden retriever puppies found five magical rings from the planet Inspiron. Each ring has given the Buddies unique super powers. Budderball got super strength, Buddha got mind-control, Rosebud got super speed, B-Dawg got super elasticity, and Mudbud got invisibility. Together, the Buddies must use the rings responsibly with the help of Captain Canine/Captain Megasis in order to stop a power hungry extraterrestrial warlord named Commander Drex, who wanted to take the rings for his own and full dictatorship of Inspiron. The Buddies soon learned that you dont need to have super powers to become a super hero.

==Cast==
===Live-action===
*John Ratzenberger as Marvin Livingstone
*Trey Loney as Bartleby Livingstone
*Veronica Diaz-Carranza as Sofia Ramirez
*Jay Brazeau as Mr. Swanson
*Jason Earles as Jack Schaeffer
*Jonathan Morgan Heit as Pete
*Harley Graham as Alice
*Darien Provost as Sam
*Sam Adler as Billy
*Michael Teigen as Sheriff Dan
*Jake Brennan as Young Jack Sharffer
*Kimberly Sustad as Joanne
*Sean Mathieson as Todd

===Voices===
*Cooper Roth as B-Dawg
*Jeremy Shinder as Budderball
*Tenzing Norgay Trainor as Buddha
*G. Hannelius as Rosebud
*Ty Panitz as Mudbud
*Colin Hanks as Megasis/Captain Canine
*Fiona Gubelmann as Princess Jorala
*John Michael Higgins as Drex
*Tim Conway as Deputy Sniffer
*Michael Teigen as Sheriff Dan
*Maulik Pancholy as Curly
*Chris Coppola as Mr. Bull
*Amy Sedaris as Betty
*Debra Jo Rupp as Cow
*Alyson Stoner as Strawberry
*Zendaya as Lollipop
*Atticus Shaffer as Monk-E
*Brian T. Finney as Dog
*Tatiana Gudegast as Cat

==Release==
===Home media===
Super Buddies was released on  .

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 