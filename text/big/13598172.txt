Variola Vera
 
{{Infobox_film | 
  name          = Variola Vera |
  image         =  Variola vera.jpg|
  caption       =  | Goran Marković | Goran Marković Milan Nikolić  |
  starring      = Rade Šerbedžija, Erland Josephson, Rade Marković |
  producer      = Aleksandar Stojanović |
  distributor   = Art Film 80 |
  released      = July 9, 1982 (Yugoslavia) September 9, 1982 (International Film Festival Rotterdam|IFFR) July 11, 1985 (Hungary)|
  runtime       = 110 min |
  country       = Yugoslavia |
  language      = Serbocroatian |
  budget        = |
  }} Yugoslav film Goran Marković. The subject of the film is the 1972 outbreak of smallpox in Yugoslavia, more specifically the events related to the epidemic and the subsequent quarantine at Belgrade|Belgrades General Hospital. Although inspired by the real events, the movie features elements of Horror film|horror.

==Plot== Kosovar pilgrim  on his way back to Belgrade from the Middle East. While at a bazaar, the pilgrim buys a flute from a man who is visibly ill. Upon his return to Belgrade, the pilgrim starts to show signs of illness and is transported to the citys General Hospital. His disease is initially misdiagnosed and the smallpox virus starts spreading through the hospital very quickly. Once the disease is correctly identified, the authorities attempt to subdue the outbreak by declaring martial law, enforcing quarantine and enlisting the help of the World Health Organization. Eventually, these measures prove to be effective to subdue the epidemic.

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 

 