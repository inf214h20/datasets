Kante Koothurne Kanu
{{Infobox film
| name           = Kante Koothurne Kanu
| image          = Kante Koothurne Kanu.jpg
| image_size     =
| caption        =
| director       = Dasari Narayana Rao
| producer       = Dasari Padma
| writer         = Dasari Narayana Rao (story) Thotapalli Madhu (dialogues)
| narrator       =
| starring       = Dasari Narayana Rao Jayasudha Ramya Krishna Brahmanandam
| music          = Vandemataram Srinivas
| cinematography = K. S. Hari
| editing        = B. Krishnam Raju
| studio         =
| distributor    =
| released       = 2000
| runtime        =
| country        = India Telugu
| budget         =
}}
 National Film Award Special Mention Feature Film.

==The Plot==
The concept is about Gender discrimination against Girl child in India.

==Cast==
* Dasari Narayana Rao  		
* Jayasudha  ...   Satya	 
* Ramya Krishna 		
* A.V.S. Subramanyam 	
* Ali 		
* Brahmanandam 	... Sofa set seller
* Narra Venkateswara Rao Jayanthi
* T. L. Kanta Rao
* Allu Ramalingaiah Chitti Babu

==Crew==
* Story, Dialogues, Screenplay, Lyrics, Direction : Dasari Narayana Rao
* Producer : Dasari Padma
* Dialogues : Thotapalli Madhu
* Director of Photography : K. S. Hari
* Editing : B. Krishnam Raju
* Lyrics : Suddala Ashok Teja
* Playback singers : S. P. Balasubrahmanyam, Vandemataram Srinivas, K. S. Chitra

==Soundtrack==
* Kante Koothurne Kanalira

==Awards==
* Dasari Narayana Rao won National Film Award – Special Jury Award / Special Mention (Feature Film) for the message in this film in the year 1999.
* This film also won Nandi Award for Best Feature Film from Government of Andhra Pradesh in 1998.

==External links==
*  

 
 
 
 

 