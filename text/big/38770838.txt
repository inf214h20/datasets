Shadows in a Conflict
{{Infobox film
| name           = Shadows in a Conflict
| image          = 
| caption        = 
| director       = Mario Camus
| producer       = 
| writer         = Mario Camus
| starring       = Carmen Maura
| music          = 
| cinematography = Manuel Velasco
| editing        = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Shadows in a Conflict ( ) is a 1993 Spanish drama film directed by Mario Camus. It was entered into the 18th Moscow International Film Festival.   

==Cast==
* Carmen Maura as Ana
* Joaquim de Almeida as José (as Joaquim De Almeida)
* Fernando Valverde as Darío
* Sonia Martín as Blanca
* Ramón Langa as Fernando
* Paco Hernández as Hombre I (as Francisco Hernández)
* Felipe Vélez as Hombre II
* Miguel Zúñiga as Funcionario
* Isabel de Castro as Madre José (as Isabel De Castro)
* Elisa Lisboa as Amalia

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 