Driving Miss Daisy
 
{{Infobox film
| name           = Driving Miss Daisy
| image          = Driving Miss Daisy .jpg
| caption        = Theatrical release poster
| director       = Bruce Beresford
| producer       = Richard D. Zanuck Lili Fini Zanuck
| screenplay     = Alfred Uhry Driving Miss Daisy  by Alfred Uhry Patti Lupone Esther Rolle
| music          = Hans Zimmer Peter James Mark Warner
| studio         = The Zanuck Company
| distributor    = Warner Bros. Pictures 
| released       =   
| runtime        = 100 minutes
| country        = United States
| language       = English Hebrew
| budget         = $7.5 million 
| gross          = $145,793,296   
}} play of the same name.  The film was directed by Bruce Beresford, with Morgan Freeman reprising his role as Hoke Colburn (whom he also portrayed in the play) and Jessica Tandy playing Miss Daisy. The story defines Daisy and her point of view through a network of relationships and emotions by focusing on her home life, synagogue, friends, family, fears, and concerns over a 25-year period.
 Best Picture, Best Actress Best Makeup, Best Adapted Screenplay.   

==Plot==
In 1948, Mrs. ("Miss") Daisy Werthan (Jessica Tandy), a 73-year-old wealthy white Jewish widowed school teacher, lives alone in Atlanta|Atlanta, Georgia, except for an African American housemaid named Idella (Esther Rolle). When Miss Daisy wrecks her car, her son, Boolie (Dan Aykroyd), hires Hoke Coleburn (Morgan Freeman), an African American chauffeur who drove for a local judge until he recently died. Miss Daisy at first refuses to let Hoke drive her, but gradually starts to accept him.

When Miss Daisy finds out that Hoke is illiterate, she teaches him how to read. As Miss Daisy and Hoke spend time together, she gains appreciation for his many skills and the two become friends. After Idella dies in 1963, rather than hire a new maid, Miss Daisy decides to care for her own house and cook her own meals. Hoke assists with the cooking and the two plant a vegetable garden.
 synagogue is bombed, Miss Daisy realizes that she is also a victim of prejudice. But American society is undergoing radical changes, and Miss Daisy attends a dinner at which Dr. Martin Luther King gives a speech. She initially invites Boolie to the dinner, but he declines, and suggests that Miss Daisy invite Hoke. However, Miss Daisy only asks him to be her guest during the car ride to the event and ends up attending the dinner alone, with Hoke insulted by the manner of the invitation, listening to the speech on the car radio outside.

Hoke arrives at the house one morning in 1971 to find Miss Daisy agitated and showing signs of dementia. Hoke calms her down and Miss Daisy tells Hoke that he is her best friend. Boolie arranges for Miss Daisy to enter a retirement home. In 1973, Hoke, now 81, retires. Boolie and Hoke drive to the retirement home to visit Miss Daisy, now 97. As Hoke feeds her and reminisces about the many years he spent driving her, the image of a car is seen driving into the distance.

==Cast==
* Morgan Freeman as Hoke Colburn
* Jessica Tandy as Daisy Werthan
* Dan Aykroyd as Boolie Werthan
* Patti LuPone as Florine Werthan
* Esther Rolle as Idella Joann Havrilla as Miss McClatchey
* William Hall, Jr. as Oscar
* Muriel Moore as Miriam
* Sylvia Kaler as Beulah
* Crystal R. Fox as Katey Bell

==Reception==
Driving Miss Daisy was well received by critics, with particular emphasis on Morgan Freeman and Jessica Tandys performances.  , which assigns a rating out of 100 based on reviews from mainstream critics, the film has a score of 81 based on 16 reviews, indicating "universal acclaim".  Peter Travers of Rolling Stone gave the film a positive review, calling Tandys performance "glorious...Tandys finest two hours onscreen in a film career that goes back to 1932."   

==Awards and nominations==
{| class="wikitable"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of Accolades
|- style="text-align:center;"
! style="background:#ccc;" width="40%"| Award / Film Festival
! style="background:#ccc;" width="30%"| Category
! style="background:#ccc;" width="25%"| Recipient(s)
! style="background:#ccc;" width="15%"| Result
|- style="border-top:2px solid gray;"
|-
| rowspan="9" | 62nd Academy Awards Best Picture
| Richard D. Zanuck Lili Fini Zanuck 
|  
|- Best Actress
| Jessica Tandy
|  
|- Best Adapted Screenplay
| Alfred Uhry
|  
|- Best Makeup
| Manlio Rocchetti  Lynn Barber Kevin Haney 
|  
|- Best Actor
| Morgan Freeman
|  
|- Best Supporting Actor
| Dan Aykroyd
|  
|- Best Art Direction-Set Decoration
| Bruno Rubeo Crispian Sallis
|  
|- Best Costume Design
| Elizabeth McBride
|  
|- Best Film Editing
| Mark Warner
|  
|-
| rowspan="3" | 47th Golden Globe Awards (January 20, 1990) Best Motion Picture – Musical or Comedy
|Driving Miss Daisy
|  
|- Best Performance by an Actor in a Motion Picture &ndash; Musical or Comedy
| Morgan Freeman
|  
|- Best Performance by an Actress in a Motion Picture &ndash; Musical or Comedy
| Jessica Tandy
|  
|}
Driving Miss Daisy also achieved the following distinctions at the 62nd Academy Awards:
* It is the only film based on an off Broadway production ever to win an Academy Award for Best Picture;    PG rating; oldest winner in the history of the Best Actress category.  Grand Hotel Best Director Argo in 2012). Wings, the 1927 film that was the first to win Best Picture, did not have a nomination for director William Wellman. In his opening monologue at the 62nd Academy Awards, Billy Crystal made fun of this fact by calling it "the film that apparently directed itself".

Driving Miss Daisy also won three Golden Globe Awards (Best Picture, Best Actor Morgan Freeman, and Best Actress Jessica Tandy) in the Comedy/Musical categories.  At the 1989 Writers Guild of America Awards, the film won in the Best Adapted Screenplay category. Rounding out its United States awards, the film won both Best Picture and Best Actor from the National Board of Review of Motion Pictures. In the United Kingdom, Driving Miss Daisy was nominated for four British Academy Film Awards, with Jessica Tandy winning in the Best Actress category. Jessica Tandy and Morgan Freeman won the Silver Bear for the Best Joint Performance at the 40th Berlin International Film Festival.   

==Soundtrack== BMI Film Music Award and was nominated for a Grammy Award for Best Instrumental Composition Written for a Motion Picture or for Television for his work. The score was performed entirely by Zimmer, done electronically using samplers and synthesizers, and did not feature a single live instrument.  There is a scene, however, in which the "Song to the Moon" from the opera Rusalka by Antonín Dvořák is heard on a radio as sung by Gabriela Beňačková. The soundtrack was issued on Varèse Sarabande.

==Home release== USA on April 30, 1997 and the special edition was released on February 4, 2003.
The movie was first released on Blu-ray disc in Germany and finally was released on Blu-ray in the US in a special edition digibook in January 2013 by Warner Bros.

==Vehicles Used==
 1949 Hudson 1957 Cadillac Eldorado Brougham and later driving himself and Hoke to the rest home scene in a Mercedes-Benz W108|1965-1972 Mercedes Benz S-Class. Hoke is also seen arriving with his granddaughter driving him in a [[Mercury Cougar#First generation (1967–1970)|
1969 Mercury Cougar]].

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 