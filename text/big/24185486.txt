The Animals Film
{{Infobox film
| name           = The Animals Film
| image          = Animalsfilm25th.jpg
| image size     =
| caption        = 25th Anniversary Edition DVD cover
| director       = Victor Schonfeld & Myriam Alaux
| producer       = Victor Schonfeld & Myriam Alaux
| writer         = Victor Schonfeld 
| narrator       = Julie Christie
| starring       = 
| music          = Robert Wyatt 
| cinematography = Kevin Keating
| editing        = Victor Schonfeld
| distributor    = 
| released       = 1981
| runtime        = 136 min
| country        = UK English
| budget         = 
| gross          = 
| preceded by    =
| followed by    =
}}

The Animals Film is a feature documentary film about the use of animals by human beings, directed by Victor Schonfeld and Myriam Alaux, and narrated by actress Julie Christie. The film was first released in 1981.

==Synopsis==
The Animals Film presents a survey of the uses of animals in factory farming, as pets, for entertainment, in scientific and military research, hunting, etc. The film also profiles the international animal rights movement. The film incorporates secret government footage, cartoons, newsreels and excerpts from propaganda films.

==Release==
The Animals Film was distributed in cinemas in United Kingdom|Britain, Australia, Germany, Austria, Canada and the USA, and was broadcast on numerous television networks. The British network, Channel Four, transmitted the film during the Channels third night on air in November 1982. It generated front page news in Britain at the time because Channel 4 broadcast a two-hour version of the film shorn of seven minutes of its concluding sequence. The original 136 minute film released in cinemas had been approved with no cuts by the British Board of Film Censors, but the Independent Broadcasting Authority instructed Channel 4 that certain scenes in the film could incite crime or lead to civil disorder.  Jonathan Porritt and David Winner write that, with over one million viewers, the screening is regarded as "an important moment in the growth of public awareness of animal exploitation."  Channel Four screened it again during its Banned series in 1991.

===Reception===
Alan Brien, film critic of the UK Sunday Times, wrote of the film: "The most impressive film maudit, possibly too hot to handle... stuffed with footage never before shown, and a wealth of newly-shot material often taken undercover, which documents... mankinds degradation, exploitation, and often pointless torture, of the creatures who share our planet. ...Proves, beyond contradiction, that this behaviour is not just random or personal but part of our organised society, with drug companies, government departments, scientists, military authorities, factory farmers, university research laboratories, for their own selfish ends, for profit in money or prestige. I do not know when I have come out of a screening so moved by the power of the cinema as a medium to transform the entire sensibility of an audience." 

Famous singer-songwriter Elvis Costello says he was moved to reject meat after seeing the documentary The Animals Film.  

==Soundtrack==
{{Infobox album |  
| Name        = The Animals Film
| Cover = 
| Type        = Soundtrack
| Artist      = Robert Wyatt
| Released    = 
| Recorded    =
| Genre       = 
| Length      = 28:06 min
| Label       = 
| Producer    = 
| Last album = Ruth Is Stranger Than Richard  (1975)
| This album = The Animals Film  (1982)
| Next album = Nothing Can Stop Us  (1982)
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =    

}}
Robert Wyatt composed an original soundtrack for the film (LP from Rough Trade). The film also features music from Talking Heads and ex-Audience frontman, Howard Werth.  Critical reception of the soundtrack was mixed. Ted Mills of album database Allmusic described the soundtrack as "moody" and filled with "tasty-sounding analog synths from the late 70s", but ultimately it "disappoint  fans of Wyatts vocals."  It was later issued in a heavily edited form (losing more than 10 minutes, with no explanation given) as a Japanese CD, and all later CD reissues have been cloned from this master.   | title = The Animals Film overview | work = Allmusic | accessdate = 2010-08-08}} 

===Home media===
In 2007 a DVD of The Animals Film was released with a new directors cut (running time 120 minutes ), via Beyond the Frame. In 2008 the British Film Institute released a remastered DVD in the UK, incorporating both the original uncensored cinema version and the directors cut.

In 2010 the BBC World Service broadcast One Planet: Animals & Us, a radio documentary series in which Victor Schonfeld investigates why little has changed since the making of The Animals Film.

==See also==
*Earthlings (documentary)|Earthlings, 2006 Behind the Mask, 2006

==Notes==
 

==External links==
*  
*  
*  
*   – The Guardian, July 5, 2007
*   – The Guardian, September 26, 2008
*   – The Times, September 30, 2008
*  

 
 

 
 
 
 
 