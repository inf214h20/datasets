Dreamkeeper
 
{{Infobox television film
| name         = Dreamkeeper
| image        =   
| image_size   = 
| caption      = 
| genre        = Drama
| runtime      = 180 minutes
| director     = Steve Barron
| producer     = Ron McLeod Matthew OConnor
| writer       = John Fusco
| starring     = Eddie Spears August Schellenberg Nathan Lee Chasing His Horse
| editing      = Colin Green
| music        = Stephen Warbeck CAD $40 million
| country      = Canada United States Germany
| language     = English Hallmark Entertainment ABC
| released     = Russia   United States  
| preceded_by  = 
| followed_by  =  
}} Lakota elder and storyteller named Pete Chasing Horse (August Schellenberg) and his Lakota grandson, Shane Chasing Horse (Eddie Spears).
 All Nations powwow in Albuquerque, New Mexico, a trip the grandson takes only under duress. Along the way, the grandfather tells his grandson various Indian stories and legends to help him understand and choose the "good red road," i.e. to embrace an Indian identity.

==Plot==
The film opens with Eagle Boy, a young man who is on a vision quest. It then cuts to the present, where a 17-year-old Lakota named Shane Chasing Horse is living on the Pine Ridge reservation. He is in trouble because he owes some money to a local gang—money he used to buy a beautiful ring for Mae Little Wounded, a girl he likes. Meanwhile, his mother asks him to drive his grandfather, Pete Chasing Horse, a storyteller, down to the powwow. Shane is reluctant. However, when the gang comes after him, Shane changes his mind and heads out to the powwow with his grandfather, who agrees to give him his truck once they reach the powwow. Grandfather tells Shane the story of a young Lakota man who tries to win the hand of Bluebird Woman. He also tells the story of how a thunder spirit falls in love with a Mohawk woman and brings her up into the ethereal world of Sky Woman, and of how she raised their son back in her village until he was struck by one of the villagers and brought back to live with his father.

Later, when a young redheaded man who is eager to learn about Native culture and hoping to be adopted by a Native American family asks to ride with them to the powwow, Shane says no. His grandfather then tells him the Kiowa story of Tehan, a white man who lived among the Kiowa and fought bravely alongside them, and Shane relents and lets the redhead ride with them. Shane’s grandfather then tells how Eagle Boy follows the advice of a shining spirit elk, and seeks out an old woman who can give him weapons with which to slay the mighty serpent Uncegila. He is repulsed when the ugly old woman embraces him, but reacts quite differently when she transforms into a beautiful younger woman. She reproaches him, but gives him what he needs. Eagle Boy slays Uncegila, whose heart instructs him and grants him great power and prophetic visions.

Eventually, the gang members who are after Shane catch up with them, but accidentally drive their car off a cliff and into the Rio Grande River while chasing him. Shane dives in and saves them, and his struggle is contrasted with Eagle Boy’s underwater battle with Uncegila. The gang members ride with them for a ways, until they and the redheaded hitchhiker leave them in order to travel with a group of attractive young women who are also headed to the powwow.

As they travel, Shane’s grandfather tells Shane many other stories: several are about the trickster Coyote and Iktomi the spider. Another is about a young Pawnee man and his mother who are scorned by the rest of their tribe until the young man finds an unwanted dun pony who brings them good medicine. As Shane and his grandfather look up at the stars, the grandfather tells the story of the Quillwork Girl and her seven star brothers, which is about a Cheyenne girl who puts her faith in a dream and searches for seven brothers, but who must then contend with the Buffalo nation.  The next story is about a young Chinook woman who sacrifices herself in order to cure her village of a terrible sickness, and the next is about a young Blackfoot hunter who cannot let go of the memory of his father.

Shane and his grandfather continue their journey, losing their truck along the way and continuing on horseback and on foot. The two become closer. However, it then turns out that Shane’s grandfather has led them not to the powwow but to Shane’s father’s (Sam Chasing Horse) trailer home. Shane is disgusted, but is persuaded to stay the night. The next morning Shane finally makes peace with his father. However, he then becomes grief-stricken when he discovers that his grandfather has died in his sleep. Shane decides to continue on to the powwow on horseback, and his father says that when Shane comes home he’ll be there too. The ending of Eagle Boy’s story is revealed: Eagle Boy decides that he wants to live like other men, and disobeys the heart by revealing it to the entire tribe (to whom it appears to be nothing more than an ordinary stone).  At the powwow, Shane takes on the role of a storyteller, and children gather around him.

==Cast==
* August Schellenberg as Grandpa Pete Chasing Horse 
* Eddie Spears as Shane Chasing Horse
* Gil Birmingham as Sam Chasing Horse
* Sheila Tousey as Janine
* Nathan Lee Chasing His Horse as Verdel 
* Chaske Spencer as Eagle Boy
* Gloria Eshkibok as Ugly Woman 
* Kimberly Norris Guerrero as Beautiful Woman
* Sean Wei Mah as High Horse
* Gordon Tootoosis as Kills Enemy
* Michael Greyeyes as Thunder Spirit  
* Alex Rice as She-Crosses-The Water
* Elizabeth Sage Galesi as Blue Bird Woman / Mae Little Wounded-credited as Sage
* Casey Camp-Horinek as Sky Woman
* Griffin Powell-Arcand as Thunder Boy
* Margo Kane as Clan Mother
* Scott Grimes as Red-Headed Stranger / Tehan
* Delanna Studi as Talks A Lot
* Nathaniel Arcand as Broken Lance
* Patric James Bird as Big Bow
* David McNally as The Colonel 
* Darren Lucas as Second Soldier
* Dave Leader as Young Soldier
* Clifford Crane Bear as Older Kiowa
* Kyle Daniels as Little Hand
* John Trudell as Coyote
* Gary Farmer as Iktome
* George Aguilar as Grandfather
* Geraldine Keams as Iktomes wife
* Dakota House as Dirty Belly
* Tantoo Cardinal as Old Woman 
* Floyd Red Crow Westerman as Iron Spoon
* Sheena Shymanski as Iron Spoons daughter
* Teneil Whiskey Jack as Quillwork Girl
* Michelle Thrush as Morning Horse
* Terry Bigcharles as First Brother
* Simon R. Baker as Second Brother
* William Daniels as Third Brother
* Zachary Nolan Auger as Fourth Brother
* Sarain Waskawitch as Fifth Brother
* Cody Lightning as Sixth Brother
* Russell Badger as War Chief
* Cliff Soloman as Village Chief
* Misty Upham as Chiefs daughter
* Lawrence Bayne as Raven
* Travis Dugas as Ekuskini
* Tyrone Tootoosis as Whirlwind Dreamer
* Sammy Simon as Ghost Hunter
* Stag Big Sorrel Horse as Hunter
* Jimmy Herman as Multnomah Elder
* Wilma Pelly as Old Woman
* Helmer Twoyoungmen as Salmon Hunter
* Tokala Clifford as Red Deer

==Production== Mohawk and Crow tribes  came on board the film to evaluate the authenticity of the production during filming and to suggest changes.  One of the advisors was shocked to see that the crew had managed to get rare Cheyenne leopard dogs for one scene.  Some scenes involved shooting a stampeding herd of 1,500 buffalo.   
 Nicholas Brooks blue screen.    Instead, the color and texture of the scenes were later altered, sometimes in a rather arbitrary manner controlled by the computer, which Brooks says lent the sequence a particular psychological feel. 

==Awards==
* 2003, American Indian Film Festival: Best Film
* 2004, Emmy Outstanding Special Visual Effects for a Miniseries, Movie or a Special
* 2004 Humanitarian Award—John Fusco—First Americans in the Arts

==References==
 

==External links==
*  
* " "—Reviews along with commentary by   of  

 
 
 