Oro, Plata, Mata
{{Infobox film
| name           = Oro, Plata, Mata
| image          =
| caption        =
| director       = Peque Gallaga
| producer       = Madeleine Gallaga and Charo Santos-Concio
| story         = Peque Gallaga Mario Taguiwalo Conchita Castillo
| screenplay     = José Javier Reyes
| starring       = Cherie Gil  Sandy Andolong  Liza Lorena  Fides Cuyungan-Asencio  Manny Ojeda  Maya Valdez  Lorli Villanueva  Ronnie Lazaro  Joel Torre  Abbo de la Cruz  Kuh Ledesma
| music          = Jose Gentica V
| cinematography = Rody Lacap
| editing        = Jesus Navarro
| distributor    = Experimental Cinema of the Philippines (1982) Star Cinema (2013)
| released       = 1982
| runtime        = 194 minutes
| country        = Philippines
| awards         = 1982 Gawad Urian awards for Best picture, direction, cinematography, production design, musical score, and sound.   Film Academy of the Philippines awards for Production Design and for Best Supporting Actress (Liza Lorena) Tagalog
| budget         =
| preceded_by    =
| followed_by    = Filipino film Negros during haciendero families cope with the changes brought about by the war. {{cite DVD notes
 | title = Oro, Plata, Mata
 | titlelink= Oro, Plata, Mata
 | origyear = 1982
 | others = Peque Gallaga
 | type = front cover
 | publisher = Star Recording/ABS CBN
 | location = Diliman, Quezon City
 | id = 17-75237-8
 | year = 2008
}}  In translation, the movie is also known either as "Gold, Silver, Bad Luck" or "Gold, Silver, Death." {{Cite web
  | last =
  | first =
  | author-link =
  | last2 =
  | first2 =
  | author2-link =
  | title = ORO, PLATA, MATA (1982)
  | date =
  | year =
  | url = http://ftvdb.bfi.org.uk/sift/title/305190
  | accessdate = 2008-05-11
  | postscript =   }} 

The title refers to an old Filipino architectural superstition saying that design elements in a house (particularly staircases) should not end in a multiple of three, in keeping with a pattern of oro (gold), plata (silver), and mata (bad luck).  The film is structured in three parts that depict this pattern played out in the lives of the main characters, from a life of luxury and comfort in the city ("oro/gold"), to a still-luxurious time of refuge in a provincial hacienda ("plata/silver"), and finally to a retreat deeper into the mountains, where they are victimized by bandit guerillas ("mata/bad luck"). 

The film is restored by ABS-CBN Film Archive and Central Digital Lab, Inc. ABS-CBNs Star Home Videos released its digitally restored version and re-mastered full HD version on DVD in all major video outlets nationwide.   

== Plot ==
 
Oro Plata Mata traces the changing fortunes of two aristocratic families in Negros during World War II. The Ojeda family is celebrating Maggie Ojedas (Andolong) debut. In the garden, Trining (Gil) receives her first kiss from Miguel Lorenzo (Torre), her childhood sweetheart. Don Claudio Ojeda (Ojeda) and his fellow landowners talk about the impending war. The celebration is cut short by news of the sinking of SS Corregidor by a mine. As the Japanese invasion force nears the city, the Ojedas accept the invitation extended by the Lorenzos, their old family friends, to stay with them in their provincial hacienda. Nena Ojeda (Lorena) and Inday Lorenzo (Asensio) try to deny the realities of war by preserving their pre-war lifestyle. Pining for her fiancé, Maggie goes through bouts of melancholy. Miguel and Trining turn from naughty children into impetuous adults.

Two more family friends, Jo Russell (Valdez)and Viring (Villanueva) join them. As the enemy advance, the families move to the Lorenzos forest lodge. A group of weary guerrillas arrive and Jo tends to their injuries. The guerrillas leave Hermes Mercurio (Lazaro) behind. Miguel endures more comments of the same kind when he fails to take action against a Japanese soldier who came upon the girls bathing in the river. It is Mercurio who kills the Japanese. Maggie comforts Miguel, who decides to learn how to shoot from Mercurio. Later, Virings jewelry is stolen by Melchor (de la Cruz), the trusted foreman. He justifies his action as a reward for his services. He tries to break the other servants loyalty by telling them to join him, but they did not forcing Melchor to leave. Later, Melchor and his band of thieves return. They raid the food supplies, rape Inday and chop off Virings fingers when she does not take off her ring. Trining unexpectedly goes with the bandits despite all the crimes they have committed against her family. These experiences committed Maggie and Miguel closer together. Miguel urges the survivors to resume their mahjong games to help them cope with their trauma. Miguel is determined to hunt the bandits down and bring Trining back. He catches them in an abandoned hospital, but his courage is replaced with bloodlust, driving him to a killing spree. An epilogue follows the violent climax. The Americans have liberated the Philippines from Japan. A party is held in the Ojeda home to announce Maggie and Miguels betrothal. The survivors attempt to reclaim their previous lifestyle, but the war has changed the world, just as it has forever changed each of them.

== Awards and Accolades ==
The movie won the 1982 Gawad Urian awards for Best picture, direction, cinematography, production design, musical score, and sound. In the same year, it won the Luna Awards for Production Design and for Best Supporting Actress (Liza Lorena). It is marketed as being one of the top ten best films of the 1980s.  

== Cast ==
*Cherie Gil as Trinidad "Trining" Ojeda
*Sandy Andolong as Margarita "Maggie" Ojeda
*Liza Lorena as Nena Ojeda
*Fides Cuyugan-Asencio as Inday Lorenzo
*Manny Ojeda as Don Claudio Ojeda
*Maya Valdez as Jo Russell
*Lorli Villanueva as Viring Ravillo
*Ronnie Lazaro as Hermes Mercurio
*Joel Torre as Miguel Lorenzo
*Abbo de la Cruz as Melchor
*Jaime Fabregas as Minggoy
*Robert Antonio as Carlos Placido
*Agustin Gatia as Lucio
*Kuh Ledesma as a diwata
*Dave Muller as a fisherman

== Sources ==
 

== External links ==
* 

 
 

 
 
 
 
 
 