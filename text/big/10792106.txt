Thee (1981 film)
{{Infobox film
| name           = Thee
| image          = Thee1981film.jpg
| alt            =  
| caption        = LP Vinyl Records Cover
| director       = R. Krishnamurthy
| producer       = Suresh Balaje
| writer         = A. L. Narayanan (dialogues)
| screenplay     = 
| story          =  Suman Sripriya Manorama
| music          = M. S. Viswanathan
| cinematography = N. Balakrishnan
| editing        = V. Chakrapani
| studio         = Suresh Arts
| distributor    = Suresh Arts
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Tamil film Suman plays Deewar starring Bollywood Mega Star Amitabh Bachchan with Shashi Kapoor, Neetu Singh and Parveen Babi. The Hindi film was directed by Yash Chopra.

==Plot==

===Synopsis=== AVM Rajan), who was defeated and disgraced by the management of his firm using his family as bait.

Unable to bear the public disgrace father deserts the family, and the sons are raised by their mother Sita (Sowcar Janaki) who brings them to Chennai, who suffers the trials and tribulations of a poor single mother. Raja, the elder brother, grows up with an acute awareness of his fathers humiliation and is victimized for his fathers supposed misdeeds. In the process of fighting for his rights, Raja, who starts out as a boot polisher and becomes a dockyard worker in his youth, becomes a smuggler and a leading figure of the underworld.

===Details===
The film opens with the strong leadership of trade unionist (  and struggles as a day labourer to care for her now homeless boys.

Raja, the elder brother, grows up with an acute awareness of his fathers failure and is victimized for his fathers supposed misdeeds. In the process of fighting for his rights, Raja, who starts out as a boot polisher and becomes a dockyard worker in his youth, becomes a smuggler and a leading figure of the underworld. He also sacrifices his own education so his brother Ravi can study. Ravi is an excellent student and grows up to become an upright police officer. He is also dating Radha (Sripriya), the daughter of a senior police officer (Major Sundarrajan). On the Commissioners suggestion, Ravi applies for employment with the police, and is sent for training. Several months later, he is accepted by the police, and has a rank of Sub-Inspector. Raja, on the other hand, becomes involved with Anita (Shoba|Shobha), a woman whom he meets at a bar. When Anita becomes pregnant, Raja decides to abandon his life in the underworld, marry her, and confess his sins. He also hopes to seek forgiveness from his mother and brother. When Ravi returns home, he finds that Raja has become a businessman overnight, has accumulated wealth, and a palatial home. When Ravi finds out that Raja has acquired wealth by crime, he decides to move out along with his mother. One of his first assignments is to be apprehend and arrest some of Chennais hardcore criminals and smugglers which includes his brother, Raja - much to his shock, as he had never associated his very own brother of having any criminal background. Ravi must now decide to proceed on with apprehending Raja, or quit from the police force. However, when Anita is brutally murdered by rival members of the underworld, Raja loses all sense of rational behavior and brutally murders his rivals in revenge for Anitas death, leading him to be branded a criminal forever. Their mother, who had sided with Ravi despite the fact that Raja was her favorite, is tormented by Rajas decisions and rejects him. The two brothers meet for a final showdown, where Ravi kills Raja. Raja dies in his mothers arms seeking forgiveness and Ravi is awarded for pursuing justice.

==Cast==
*Rajinikanth .... Rajshekhar Raja Suman .... Inspector Ravishankar Ravi
*Shobha .... Anantlakshmi Anita
*Sripriya .... Radha AVM Rajan .... Sitas Husband
*Sowcar Janaki .... Sita
*K. Balaji .... Jagdish
*R. S. Manohar .... Madan Major Sundarrajan .... Radha’s Father

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 