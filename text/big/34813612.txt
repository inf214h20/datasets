Diary of a Butterfly
{{Infobox film
| name = Diary of a Butterfly
| image = DOABF_Poster.jpg
| caption = Theatrical release poster
| director = Vinod Mikhi
| producer = Anil Kumar
| writer = Asad Ajmeri
| starring = {{Plainlist|
* Udita Goswami
* Rati Agnihotri
* Rajesh Khattar 
* Harsh Chhaya
* Naseer Abdullah
* Sofia Hayat
* Rajeev Singh
* Ronny Jhutti
* Gaurav Dixit
* Aryan Vaid
* Sneha Belani
* Johnny Nirmal
}} Taz – Stereonation, Mukhtar Sahota & Shibani Kashyap
| cinematography = Mohan Verma & Mohan Prajapati
| distributors = Bhaggyashri Production
| released =  
| runtime = 107 minutes
| budget = INR 32.5 million
| country = India
| language = Hindi
}}
Diary of a Butterfly is a 2012 film directed by Vinod Mukhi and written by Asad Ajmeri. Vinod Mukhi has earlier directed films like ‘Cabaret’ and ‘www.Love.com’.
 John Abraham. Recently she was seen in films like ‘Rokkk’ (2010), Apartment: Rent at Your Own Risk (2010), and ‘Chase’ (2010).

The film is produced by Anil Kumar under the banner of Bhaggyashri Production.

==Story==
This is a story   about a girl named Gul, who starts her journey for success from her hometown Jaipur. She is hungry for success so she decides to go to the city of dreams, Mumbai. When she is leaving for Mumbai, her mother gives her a Diary. She says to Gul that it will be very helpful if she records her day-to-day activities in the Diary. She also gives her a Note, on which there are some knowledgeable lines written.

In Mumbai she works for a Fashion House. She stays with her friends Piya, who works an as accountant in the same Fashion  House, and with Carol, who is an owner of a Spa.

When she starts her journey in Mumbai she forgets each and every relationship in the thirst for success. She uses people as stairs in her journey of life in order to reach to the top. She even takes the Diary and the Note as a joke. But when she reaches the peak, she finds out that while running for success she has hurt many people knowingly or sometimes even without any intention. She had abused many people who tried to correct her and now currently she has nobody to celebrate her newfound success. Her errant and reckless lifestyle and line approach to achieve her goal made her lose every friend in life.

So she starts reading her Diary from the start just to find  out if she has treated someone to the worst degree. Also the Notes Lines starts ringing in her head helping her realize her mistakes and also telling her to apologize to them.

When she decides to correct all the mistakes she has made in the past i.e. to apologize to each and everyone she has hurt, she finds out the truth about Mumbai and the Corporate World. She is so shocked seeing the response of the people that she decides to stay the way she is. She also refuses to listen to her Mother because she thinks because of the Diary and the Note she went to correct things which in-turn became a joke on herself.

Arrogant, Reckless, Social and a Dreamer who will continue her journey for more success but in her own way.

==Cast==
* Udita Goswami as Gul
* Rati Agnihotri as Shobha
* Rajesh Khattar as Xavier
* Harsh Chhaya as Vivek
* Naseer Abdullah as Gul’s father
* Sofia Hayat as Carol
* Rajeev Singh as Aran
* Aryan Vaid as Ravi Bajaj
* Ronny Jhutti as Adi
* Gaurav Dixit as Aakash
* Sneha Belani as Pia
* Johnny Nirmal as Delete Singh

==Soundtrack== Taz – Taz – Stereonation, Arif Lohar, Shibani Kashyap, Vasuda Sharma, Akriti Kakkar & Apeksha Dandekar.

{{track listing
| headline = Track listing
| music_credits = no
| extra_column = Artist(s)
| title1 = Alif Allah (Jugni)
| extra1 = Arif Lohar
| length1 = 04:27
| title2 = We Don’t Need Boys
| extra2 = Akriti Kakkar, Apeksha Dandekar
| length2 = 04:08
| title3 = Hor Pila De 
| extra3 = Tarsame Singh Saini|Taz-Stereo Nation
| length3 = 03:10
| title4 = Hungama Ho Gaya 
| extra4 = Shibani Kashyap
| length4 = 03:34
| title5 = Jugni Dhol Mix
| extra5 = Arif Lohar
| length5 = 04:29
}}

==Reception==
The film opened to negative reviews. Faisal Saif of Global Movie magazine gave 1 out of 5 stars and wrote "Painful experience sleaze fest filled with extremely bad performances".  Mumbais DNA tabloid wrote "Doesnt seem completely devoid and potential".  Komal Nahta of koimoi.com gave 1/2 star out of 5 and wrote "Apology of a story, Below the mark performance". 

==References==
 

==External links==
*  
*  

 
 
 