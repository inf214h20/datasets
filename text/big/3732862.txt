Jack and Sarah
 
 
{{Infobox Film
| name = Jack and Sarah
| image = Jack and sarah.jpeg
| image_size =
| caption = Original film poster (US) Tim Sullivan
| producer = Simon Channing-Williams Pippa Cross Janette Day
| writer = Tim Sullivan
| narrator =
| starring = Richard E. Grant Samantha Mathis
| music = Simon Boswell
| cinematography = Jean-Yves Escoffier
| editing = Lesley Walker Le Studio Canal + Granada Productions
| distributor = Gramercy Pictures (United States|USA)
| released = 2 June 1995
| runtime = 110 mins.
| country = France, United Kingdom
| language = English
| budget =
| gross = $218,626 (USA) (sub-total) £2,475,758 (UK)   Web. 29 Mar. 2014. 
| preceded_by =
| followed_by =
}} British romantic Tim Sullivan. The film was originally released in the UK on 2 June 1995.

The theme song in this film is "Stars (Simply Red song)|Stars" by British pop group Simply Red.

== Plot == birth leads to the death of Sarah. Jack, grief-stricken, goes on an alcoholic bender, leaving his daughter to be taken care of by his parents and Sarahs mother, until they decide to take drastic action: they return the baby to Jack whilst he is asleep, leaving him to take care of it. Although he struggles initially, he eventually begins to dote on the child and names her Sarah.

Despite this, he nevertheless finds it increasingly difficult to juggle bringing up the baby with his high-powered job, and though both sets of the childs grandparents lend a hand (along with William (Ian McKellen), a dried out ex-alcoholic who, once sober, proves to be a remarkably efficient babysitter and housekeeper), he needs more help. Amy (Samantha Mathis), an American waitress he meets in a restaurant who takes a shine to Sarah, takes up the role as nanny, moving in with Jack after one meeting.

Although clashing with William and the grandparents, especially Jacks mother, Margaret (Judi Dench), Jack and Amy gradually grow closer—but Jacks boss has also taken an interest in him.

== Cast ==
* Jack &ndash; Richard E. Grant
* Amy &ndash; Samantha Mathis
* Margaret &ndash; Judi Dench
* Phil &ndash; Eileen Atkins
* Anna &ndash; Cherie Lunghi
* Sarah &ndash; Imogen Stubbs
* William &ndash; Ian McKellen
* Baby Sarah &ndash; Bianca Lee & Sophia Lee
* Baby Sarah (as a toddler) &ndash; Sophia Sullivan

==Reception==
The film was a popular hit in the UK. 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 