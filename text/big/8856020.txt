The Prey (1984 film)
 
{{Infobox film
| name           = The Prey
| image          = The Prey (1984 film) cover.jpg
| image_size     = 
| caption        = 
| director       = Edwin Brown
| producer       = Summer Brown Edin Brown
| writer         = Edwin Scott Brown
| narrator       =
| starring       = Steve Bond  Lori Lethin  Jackie Coogan
| music          = Don Peake
| cinematography = Teru Hayashi
| editing        = Michael Barnard
| studio         = Essex Productions
| distributor    = New World Pictures Thorn EMI Video 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Prey  was a film made in 1978  — but not released until 1984 — by the duo of Edwin and Summer Brown  and starring Steve Bond, Lori Lethin and Jackie Coogan.  

It was Jackie Coogans final film.

==Plot==

The film is set during the summer between high school graduation and college. The film begins with a couple Frank (Ted Hayden) and Mary Sylvester (Connie Hunter) cooking food over a campfire while being watched by a mysterious figure in the shadows (played by Carel Struycken but not seen until the end of the film). While Mary is taking a walk in the woods, she hears her husband scream and returns to the campsite to find her husbands decapitated corpse. She is then herself killed by the killer wielding her husbands axe. Several weeks later, three teenage couples are hiking in the same remote forest high in the Colorado Rockies to enjoy the nature and to spend some time alone together. As they progress deeper into the wilderness its clear that the killer from the beginning of the film is stalking them. During their first night in the woods, Gail (Gayle Gannes) hears a noise and sends her boyfriend Greg (Philip Wenckus) to check out the disturbance. While isolated they are both murdered by the killer.

The next day, their friends find the couples gear missing and assume that they have turned back and gone home. The boys decide to head for the infamous Suicide Peak to do some rock climbing while the girls suntan. Meanwhile, the forest rangers Lester Tile (Jackie Coogan) and Mark OBrien (Jackson Bostwick) get a phone call about the missing Sylvester couple and Mark heads for the forest to investigate. Before leaving Lester tells Mark a story about a Gypsy boy he once saw during a forest fire years ago. The boy was covered in burns, horribly disfigured and left for dead.

While exploring, Mark discovers Gails decomposing body and sets off to find the killer. The killer attacks Skip (Robert Wald) and Joel (Philip Wenckus) while rock climbing, killing them both. Hearing the noise, Nancy (Debbie Thureson) and Bobbie (Lori Lethin) put on their clothes to investigate but are confronted by the killer. The killer is revealed to be the gypsy boy from Lesters story, who has survived in the wilderness but has razor sharp claws and is horribly deformed. They run but Bobbie stumbles into one of the killers traps and is killed instantly. Cornered by the killer, Nancy faces him alone until Mark appears, shooting him with his tranquilizer gun and bashing him in the face with a large stick. Mark then comforts Nancy, but the killer awakens and kills him. The killer then smiles as he reaches softly towards Nancy. In the final minutes of the film, we see the seasons change as many months go by. The final shot is of a cave, where we hear the crying of a newborn baby.

==Cast==
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Jackie Coogan
| Lester Tile 
|-
| Steve Bond
| Joel
|-
| Carel Struycken
| Gypsy Forest Fire Survivor/The Monster
|-
| Debbie Thureson
| Nancy
|-
| Lori Lethin
| Bobbie
|-
| Robert Wald
| Skip
|-
| Philip Wenckus
| Greg
|-
| Jackson Bostwick
| Mark OBrien
|-
| Gayle Gannes
| Gail
|-
| Garry Goodrow
| Sgt. Parsons
|-
| Ted Hayden
| Frank Sylvester
|-
| Connie Hunter
| Mary Sylvester
|- John Leslie (uncredited)
| Gypsy
|}

==Production==
 
The movie was filmed in 1978 in Utah. 

==Release==
 
It premiered on 11 November 1984 in the United States with a limited theatrical release  and Thorn EMI Video released it in 1988 on VHS.

==Reception==
 
Critical reception for the film has been mixed to positive.

Charles Tatum from eFilmCritic.com awarded the film 1 / 5 stars stating, "There is not one minute of suspense here," Tatum summarized by saying, "Sure, most of the slasher films of the 1980s were not worth the celluloid they were filmed on, but this video nightmare may well be the dullest produced". 
Hysteria Lives! gave the film a negative review stating, "THE PREY is dumb, boring (I had to push needles into my legs just to stop from slipping into a catatonic state), and pitifully indulgent". 

The film wasnt without any positive reviewers, Josh Gratton from slasherstudios.com gave the film  a score of 3 / 4 stating, "Often criticized for its pace and inserts of nature footage, I fail to see the problem that people have with this aspect of the movie. As if the forest being shot in wasn’t gorgeous enough, the extra bits of insects and wildlife going about their nightly duties add a grounding to the film that captures what many are unable to do while in your own home. The shots don’t even last for THAT long, and the overall pacing isn’t an issue with me whatsoever. When you have lovable characters like the mirror-obsessed city gal Gail and the dopey banjo-playing ranger Mark who tells jokes to animal friends, I fail to grasp what detestable being others appear to rag on". 

John Kenneth Muir also gave the film a positive review, calling it A modest but noteworthy entry into the mountain man/slasher genre. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 