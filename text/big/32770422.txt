The Good Bad Man
{{infobox film
| name           = The Good Bad Man
| image          = The Good Bad Man.jpg
| imagesize      =
| caption        = 1916 trace art theatrical poster
| director       = Allan Dwan
| producer       = Douglas Fairbanks
| writer         = Douglas Fairbanks
| starring       = Douglas Fairbanks
| music          =
| cinematography = Victor Fleming
| editing        =
| studio         = Fine Arts Film Company
| distributor    = {{Plain list|*Triangle Film Corporation  (1916 release) 
*Tri-Stone Pictures  (1923 re-release) }}
| released       =  
| runtime        = 50 minutes
| country        = United States Silent  (English intertitles) 
}}
 silent Western Western film directed by Allan Dwan. The film stars Douglas Fairbanks (who also produced and wrote the film) and Bessie Love. 

The film was originally distributed by Triangle Film Corporation. The film was edited and re-released by Tri-Stone Pictures in 1923.  A still photo from the film published in Overland Monthly (November 1916) refers to the film as Coyote of the Rio Grande and gives the name of Fairbanks character as "Coyote McCall".

==Cast==
*Douglas Fairbanks as "Passin Through"
*Sam De Grasse as Bud Frazer/The Wolf
*Pomeroy Cannon as Bob Evans the Marshal
*Joseph Singleton as Weazel
*Bessie Love as Amy
*Mary Alden as Jane Stuart
*George Beranger as Thomas Stuart Fred Burns as Sheriff Charles Stevens as a Bandit (uncredited) Jim Mason as a Bandit (uncredited)
  (November 1916) where title of film is given as Coyote of the Rio Grande]]

==Preservation status==
No print of the original 1916 release exists, but a print of the 1923 re-release is preserved at the Library of Congress.  

On May 31, 2014, a restored print of the 1923 version was shown at the  ".

==Legacy==
Fairbanks biographer Jeffrey Vance finds The Good Bad Man fascinating for what it reveals about Fairbanks the man. Vance writes:
 

==References==
 

==Further reading==
*Vance, Jeffrey. Douglas Fairbanks. Berkeley, CA: University of California Press, 2008. ISBN 978-0-520-25667-5.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 