Garter Colt
{{Infobox film
 | name =
 | image =  Giarrettiera Colt.jpg
 | image_size = 185px
 | caption =
 | director = Gian Andrea Rocco
 | writer = Giovanni Gigliozzi Brunello Maffei Vittorio Pescatori Gian Andrea Rocco
 | starring =   Nicoletta Machiavelli Claudio Camaso
 | music =  Enzo Fusco Gianfranco Plenizio
 | cinematography = Gino Santini
 | editing = Mario Salvatori
 | producer = Giovanni Vari Nicoletta Machiavelli (uncredited)
 | distributor =
 | released =  May 19, 1968
 | runtime = 102 min.
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} 1968 Cinema Italian spaghetti western directed by Gian Andrea Rocco. It is one of the rare Italo-westerns with a woman as lead character.   

== Plot ==
At the border with Mexico, a brave young woman defends herself from the attack of the fearsome bandit, "Red", mastering a gun and the game of poker. Falling in love with a young Frenchman, he asks her to give up gambling and start a quiet and normal life, but when the young man is killed by "Red", she is determined to seek revenge.

== Cast ==
* Nicoletta Machiavelli as Lulu, „Giarrettiera Colt“
* Claudio Camaso (Claudio Volonté) as Red
* Marisa Solinas as Rosie
* Yorgo Voyagis as Benito Juarez/Carlos
* Walter Barnes as General Droga
* Gaspare Zola: as Emperor Maximilian/Jean
* Silvana Bacci
* Franco Bucceri as the Doctor
* Elvira Cortese as Elvira
* James Martin as Sheriff
* Giovanni Ivan Scratuglia as Roger

==References==
 

==External links==
* 
*  at the Spaghetti Western Database

 
 
 
 
 

 
 