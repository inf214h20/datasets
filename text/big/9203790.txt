Galaxy Invader
 {{Infobox film
| name           = Galaxy Invader
| image          = Galaxy Invader.jpg
| image size     = 
| alt            = 
| caption        = Film poster
| director       = Don Dohler
| producer       = Don Dohler
| writer         = Don Dohler Anne Frith (additional scenes) David W. Donoho (additional scenes)
| narrator       = 
| starring       = Richard Ruxton George Stover
| music          = Norman Noplock 
| cinematography = Paul Loeschke
| editing        = Don Dohler
| studio         = Moviecraft Entertainment
| distributor    = Moviecraft Entertainment
| released       = April 1985
| runtime        = 72 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Galaxy Invader (aka The Galaxy Invader) is a 1985 direct-to-video Science fiction|sci-fi film directed and co-written by Baltimore filmmaker Don Dohler.  

==Plot==
When a glowing object which seems to be a meteor careens toward the Earth, a young student who sees it, is narrowly missed as it falls into the forest ahead of him. A couple of hours later, a young couple hears a noise in their basement and go down to see what it is. They are terrified and wrestled to the ground by a green monster known as the Galaxy Invader. The alien is hunted by a gang of locals intent on cashing in on the creature.

==Cast==
 
* Richard Ruxton as Joe Montague
* Faye Tilles as Carol Montague
* George Stover as J.J. Montague
* Greg Dohler as David Harmon
* Ann Firth as Ethel Montague
* Don Leifert as Frank Custer
* Dick Dyszel as Dr. William Tracy
* Theresa Harold as Vickie Johnson
* Glenn Barnes as the alien
* Cliff Lambert as Michael Smith
* Jerry Schuerholz as McGregor
* Paul Wilson as Thompson
* David W. Donoho as Giddings (credited as David Donoho)
* Doug Moran as Turner
 

==Production== B straight-to-VHS films. With a short timeline and limited budget, more emphasis was placed on characters than sci-fi special effects.  The green, web-footed alien monster in the film has a superficial resemblance to the titular entity from Creature from the Black Lagoon.  The rubber suit and mask was also unfavourably compared to other film monsters. 

The films cast, however, was part of Don Dohlers stable of friends and includes Donald Leifert and George Stove both featured in Dohlers earlier films, The Alien Factor, Fiend and Nightbeast. Richard Ruxton, another of the films stars, would go on to star in Dohlers next film, Blood Massacre.
 Spanish film Pod People in 1990, which in turn was featured as an episode of Mystery Science Theater 3000 the following year.


==Reception==
In a recent article in The New York Times dealing with 1980s B films, "This pre-video-age late-night cable TV favorite was created (almost singlehandedly) by a fanzine editor who managed to shape his micro-budget into a special effects extravaganza. The late Don Dohler would go on to direct many straight-to-VHS films of the ’80s, including Galaxy Invader and Blood Massacre." 

 Kevin Murphy took on Galaxy Invader. 

==Film rights==
Galaxy Invader is not in the public domain. The copyright renewal is (PA 1-374-171) by Wade Williams.  

==References==
Notes
 

==External links==
*  

 
 
 
 
 

 