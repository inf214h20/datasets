Best Man Down
{{Infobox film
| name           = Best Man Down
| image          = Best Man Down Theatrical Poster.png
| caption        = Theatrical release poster
| director       = Ted Koland
| producer       = David Abbitt Jen Roskind Sharyn Steele
| writer         = Ted Koland
| starring       = Justin Long Jess Weixler Tyler Labine Addison Timlin Shelley Long Frances OConnor
| music          = Mateo Messina
| cinematography = Seamus Tierney
| editing        = Grant Myers
| studio         = Koda Entertainment
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  $1,938 
}}
Best Man Down is an American film written and directed by Ted Koland. The film was primarily shot in the Twin Cities and premiered in the fall of 2012 at The Hamptons International Film Festival under the original title "LUMPY." The film was also chosen to close the Twin Cities Film Fest in October of 2012 and was the black tie gala screening for the Catalina Film Festival in September of 2013. It was released theatrically in the United States by Magnolia Pictures on November 8, 2013, but had primary distribution through digital channels. 

The film features a rare dramatic performance by Justin Long and introduces Addison Timlin in her first starring role in a feature. Although the film is technically a "dramedy," for commercial reasons Magnolia released the film as a comedy, which hurt reviews. 

==Plot== Minneapolis to contact Lumpys friends and plan the funeral.

Meanwhile, in a small town in Northern Minnesota we are introduced to Ramsey Anderson (Addison Timlin), a fifteen-year-old girl living with her mom and her moms boyfriend. Her mom is a drug addict with a history of moving around, and her moms boyfriend is a former soldier who has turned to cooking meth to make money. Ramsey catches the local priest (Michael Landes) with another man in his home and tacitly blackmails him out of $50, which she uses to purchase groceries for their house. Later on, she deliberately allows herself to be caught shoplifting cold medicine so that she wouldnt have to supply it to her moms boyfriend and is bailed out by the same priest.

Kristen and Scott find Ramseys name in Lumpys phone and try to contact her but she doesnt answer. They visit his old law school and job to try and locate Ramsey but discover that Lumpy suddenly quit school and was fired from his job for embezzling tips from the bar. The bar manager tells them that Lumpy was involved with some girl, whom they deduce to be Ramsey. After an uncomfortable argument at Christmas dinner, Kristen pushes Scott to drive up to Ramseys town and find her. In flashbacks, we learn that Ramsey met Lumpy ten months earlier when he knocked on her door asking for permission to cut across her parents property to go ice fishing. After catching a fish, Lumpy falls through the ice and is rescued by Ramsey. He takes her back to his hotel room where they both get cleaned up but end up having an argument after he asks her to leave. The hotel clerk calls the police on Lumpy and he is arrested, but released after Ramsey shows the police the fishing hole and accident. After this, Lumpy begins visiting Ramsey more frequently and the two develop a close friendship.

Scott and Kristen arrive in Ramseys town and find her house. They tell her that Lumpy has died and offer to give her details about the funeral, but instead she asks them to leave. Finding a hotel for the night, Scott admits that he quit his job after being denied a commission and that the money for their honeymoon was a gift from Lumpy. The next morning, Ramsey finds them at the hotel and asks for a ride to Minneapolis for the funeral. She tells them she is pregnant with Lumpys baby and needs to leave. They take her home to get permission from her mom, and are confronted by the moms gun-wielding boyfriend who believes they are police. Ramseys mom, realizing that this is Ramseys chance to get away from this lifestyle, gives her money and tells her its ok to go.

Back in Minneapolis Ramsey gets into an argument with Kristen and Scott when she reveals Lumpys biggest secret, that he had a heart defect and knew he was dying. After fighting, Ramsey walks out. The next day at the funeral Ramsey delivers a powerful eulogy, replete with information about Lumpy that no one knew but her. Afterwards, she admits to Scott that she is not pregnant and that Lumpy was a true gentleman who was never intimate with her. Lumpys mom gives Ramsey an envelope with some money in it that he left for her, and tells her theres more in a safe deposit box and that Lumpy meant for her to pay for her college with it. Ramsey sees the priests boyfriend from earlier at the funeral and realizes its Scotts boss. She takes him aside and talks to him, leading to Scott getting his job back.

The next day, Ramsey prepares to catch a bus home when Scott reveals that her mom called. Her mom left her boyfriend and was moving, and gave Ramsey permission to live with Scott and Kristen until she gets on her feet. Scott asks what Ramsey told his boss to get his job back, and she coyly responds that he probably doesnt want to know. The film ends with them all preparing to go out for dinner.

==Cast==
*Justin Long as Scott
*Jess Weixler as Kristin
*Tyler Labine as Lumpy
*Addison Timlin as Ramsey  
*Shelley Long as Gail
*Frances OConnor as Jaime Evan Jones as Winston
*Michael Landes as Priest
*Peter Syversten as Roger
*James Detmar as Uncle Willis  
*Jane Hammill as Aunt Helen
*Claudia Wilkens as Lumpy’s Mom
*Sasha Andreev as Rick
*Sara Marsh as Bridesmaid
*Corey Anderson as Cousin Neil

==References==
 
   
 

==External links==
* 
* 

 
 
 
 
 