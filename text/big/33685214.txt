Small Town Boy
{{Infobox film
| name           = Small Town Boy
| image          = 
| image_size     = 
| caption        = 
| director       = Glenn Tryon
| producer       = Zion Myers (producer)
| writer         = Manuel Komroff (story "The Thousand Dollar Bill") Glenn Tryon (screenplay)
| narrator       = 
| starring       = See below
| music          =  Edward Snyder
| editing        = James B. Morley
| studio         = 
| distributor    = 
| released       = 1937
| runtime        = 63 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Small Town Boy is a 1937 American film directed by Glenn Tryon.

== Plot summary ==
 

=== Differences from source ===
 

== Cast ==
*Stuart Erwin as Henry Armstrong
*Joyce Compton as Molly Summers
*Jed Prouty as Otis Armstrong
*Clara Blandick as Mrs. Armstrong
*Dorothy Appleby as Sandra French
*James Blakeley as Eddie Armstrong Clarence Wilson as Curtis French
*John T. Murray as C. Lafferty
*Lew Kelly as The Judge
*Victor Potel as Abner Towner
*Erville Alderson as Mr. Trindle
*William Ruhl as A Waiter
*George Chandler as Bill Clipper
*Henry Roquemore as Ted Fritter

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 


 