Mystic River (film)
 
{{Infobox film
| name = Mystic River
| image = Mystic River poster.jpg
| alt = 
| caption = Theatrical poster by Bill Gold
| director = Clint Eastwood
| producer = {{Plainlist|
* Robert Lorenz
* Judie G. Hoyt
* Clint Eastwood}}
| screenplay = Brian Helgeland
| based on =  
| starring = {{Plainlist|
* Sean Penn
* Tim Robbins
* Kevin Bacon
* Laurence Fishburne
* Marcia Gay Harden
* Laura Linney}}
| music = Clint Eastwood Tom Stern
| editing = Joel Cox
| production companies = {{Plainlist|
* Malpaso Productions
* Village Roadshow Pictures
* NPV Entertainment}} Warner Bros. Pictures
| released =  
| runtime = 137 minutes  
| country = United States
| language = English
| budget = $30 million Eliot (2009), p.307  Hughes, p.153 
| gross = $156.8 million 
}} mystery drama novel of the same name by Dennis Lehane. The film was produced by Robert Lorenz, Judie G. Hoyt and Eastwood. It is the first film on which Eastwood was credited as composer of the Film score|score.
 Best Picture, Best Director, Best Actor, Best Adapted Best Supporting Best Supporting Actor. Sean Penn won Best Actor and Tim Robbins won Best Supporting Actor, making Mystic River the first film to win both awards since Ben-Hur (1959 film)|Ben-Hur in 1959.

==Plot== sexually abuse him for four days, until he escapes.

Twenty-eight years later, the boys are grown and, while they still live in Boston, have drifted apart. Jimmy (Sean Penn) is an ex-con running a neighborhood store, while Dave (Tim Robbins) is a blue-collar worker, still haunted by his abduction. The two are still neighbors and related by marriage. Jimmys 19 Las Vegas.

Dave sees Katie at a local bar. That night, Katie is murdered, and Dave comes home with an injured hand and blood on his clothes, which his wife Celeste (Marcia Gay Harden) helps him clean up. Dave claims he fought off a mugger, "bashed his head into the concrete", and possibly killed him. Sean (Kevin Bacon), now a detective with the Massachusetts State Police, investigates Katies murder. In a subplot, Seans pregnant wife Lauren has left him.

Over the course of the film, Sean and his partner, Sergeant Whitey Powers (Laurence Fishburne), track down leads while Jimmy conducts his own investigation using his neighborhood connections. Sean discovers that the gun used to kill Katie was also used in a liquor store robbery during the 1980s by "Just Ray" Harris, the father of Katies boyfriend. Harris has been missing since 1989, but Brendan claims he still sends his family $500 every month. Brendan also feigns ignorance about Rays gun but Sean believes it was still in the house. Sergeant Powers suspects Dave as a possible perpetrator because he was one of the last people to see Katie alive. Dave also has a wounded hand and, although he continues to tell his wife he got it while being mugged, he tells the police a different story – soon Jimmy becomes suspicious of it. Dave continues to behave erratically, which upsets his wife to the point she is afraid he will hurt her. While Jimmy and his associates conduct their investigation, Celeste eventually tells Jimmy about Daves behavior, the bloody clothing, and her suspicions.

Jimmy and some associates get Dave drunk at a local bar. When Dave leaves the bar, the men follow him out. Jimmy tells Dave that he shot "Just Ray" Harris at that same location for ratting him out and sending him to jail. Jimmy informs Dave that his wife thinks he murdered Katie and tells Dave he will let him live if he confesses. Dave repeatedly tells Jimmy that he did kill someone but it was not Katie: he beat a child molester to death after finding him having sex with a child prostitute in a car. Jimmy does not believe Daves claim and threatens him with a knife. When Dave finally admits to killing Katie thinking he can escape with his life, Jimmy kills him and disposes of his body in the adjacent Mystic River.

While Dave is being killed, Brendan (having found out about his fathers gun during questioning) confronts his younger brother "Silent" Ray Jr. and his brothers friend John about Katies murder. He beats the two boys and threatens to kill them if they do not admit their guilt, but when John takes the gun and is about to shoot him, Sean and Powers arrive just in time to stop it.

The next morning, Sean tells Jimmy the police have Katies murderers – who have confessed. She was killed by Brendans brother Ray and his friend John OShea in a violent prank gone wrong: The kids got hold of Just Rays gun and saw a car coming which happened to be Katies. John aimed the gun just to scare her but the gun went off by accident. The car veered onto the curb and Katie got out and ran into the park. Silent Ray and John pursued her so she wouldnt tell anyone. The beating Katie received was from Silent Ray, who had a hockey stick. Once she was beaten, John shot her again, killing her. Sean asks Jimmy if he has seen Dave, because he is wanted for questioning in another case, the murder of a known child molester whose body has been recovered, serving to confirm Daves allegation of having murdered a pedophile and hiding the body thereafter. A distraught Jimmy thanks Sean for finding his daughters killers, but says, "if only you had been a little faster." Sean asks Jimmy if hes going to "send Celeste Boyle $500 a month too?"

Sean reunites with his wife and his daughter Nora, after apologizing for "driving her away". Jimmy goes to his wife, Annabeth (Laura Linney) and confesses. She comforts him by telling him what she told their little daughters at bedtime: that any decision a father takes to protect the ones he loves will be the right one, even when its the hardest. At a town parade, Sean sees Jimmy and mimics shooting him, to let Jimmy know he is watching.

==Cast==
 
* Sean Penn as James "Jimmy" Markum
** Jason Kelly as young Jimmy Markum
* Tim Robbins as Dave Boyle
** Cameron Bowen as young Dave Boyle
* Kevin Bacon as Detective Sean Devine
** Connor Paolo as young Sean Devine
* Laurence Fishburne as Detective Sergeant Whitey Powers
* Marcia Gay Harden as Celeste Boyle
* Laura Linney as Annabeth Markum
* Tom Guiry as Brendan Harris
* Spencer Treat Clark as Ray Jr. "Silent Ray" Harris
* Emmy Rossum as Katie Markum
* Kevin Chapman as Val Savage
* Adam Nelson as Nick Savage
* Robert Wahlberg as Kevin Savage
* Cayden Boyd as Michael Boyle
* Tori Davis as Lauren Devine
* Jonathan Togo as Pete
* Will Lyman as FBI Special Agent Birden
* Ari Graynor as Eve Pigeon
* Ken Cheeseman as Daves Friend in Bar Michael McGovern as 1975 reporter Kevin Conway (uncredited) as Theo
* Eli Wallach (uncredited) as Mr. Loonie
 

==Production==
Principal photography took place on location in Boston.  Eastwood claimed that the three lead actors were his first choices for the roles. 

==Release==
===Reception=== normalized rating The Sun wrote that the film was "a haunting masterpiece and probably   best film to date". Hughes, p. 155 

===Box office===
The film earned $156,822,020 worldwide with $90,135,191 in the United States and $66,686,829 in the international box office, which is significantly higher than the films $30 million budget.   

==Accolades==
;Awards
* 76th Academy Awards:
** Best Actor in a Leading Role (Sean Penn) 
** Best Actor in a Supporting Role (Tim Robbins) 
* Art Directors Guild: Feature Film – Contemporary Film (Henry Bumstead   and Jack G. Taylor Jr.  )
* Blue Ribbon Awards: Best Foreign Language Film 24th Boston Society of Film Critics Awards: Best Picture, Best Ensemble Cast
* 9th BFCA Critics Choice Awards: Best Actor (Sean Penn), Best Supporting Actor (Tim Robbins) Cannes Film Festival: Golden Coach (Clint Eastwood)
* Casting Society of America: Best Casting for Feature Film – Drama
* Central Ohio Film Critics: Best Actor (Sean Penn), Best Supporting Actor (Tim Robbins), Best Supporting Actress (Marcia Gay Harden)
* Chicago Film Critics Association: Best Supporting Actor (Tim Robbins)
* César Awards (France): Best Foreign Language Film
* Dallas-Fort Worth Film Critics Association: Best Actor (Sean Penn)
* Florida Film Critics Circle: Best Actor (Sean Penn), Best Supporting Actor (Tim Robbins)
* Fotogramas de Plata (Spain): Best Foreign Language Film
* 61st Golden Globe Awards: Best Actor in a Motion Picture – Drama (Sean Penn) Best Actor in a Supporting Role in a Motion Picture (Tim Robbins)
* Kansas City Film Critics Circle: Best Actor (Sean Penn)
* Kinema Junpo Awards (Japan): Best Foreign Language Film
* Las Vegas Film Critics Society: Best Actor (Sean Penn)
* London Film Critics Circle: Actor of the Year (Sean Penn), Director of the Year (Clint Eastwood)
* Mainichi Film Concours (Japan): Best Foreign Language Film
* National Board of Review: Best Picture, Best Actor (Sean Penn)
* National Society of Film Critics: Best Director (Clint Eastwood)
* PEN Center USA West Literary Awards: Best Screenplay (Brian Helgelan)
* Sant Jordi Awards (Spain): Best Foreign Language Film Satellite Awards: Best Performance by an Actor in a Motion Picture – Drama (Sean Penn), Best Screenplay – Adapted (Brian Helgeland)
* Screen Actors Guild: Outstanding Performance by a Male Actor in a Supporting Role (Tim Robbins) Seattle Film Critics: Best Supporting Actress (Marcia Gay Harden)
* Southeastern Film Critics Association: Best Supporting Actor (Tim Robbins), Best Screenplay – Adapted (Brian Helgeland)
* USC Scripter Award: Brian Helgeland (screenwriter) and Dennis Lehane (author)
* Uruguayan Film Critics Association (Uruguay): Best Film
* Vancouver Film Critics Circle: Best Actor (Sean Penn)

;Nominations
* 76th Academy Awards:
** Best Picture 
** Best Director (Clint Eastwood) 
**Best Actress in a Supporting Role (Marcia Gay Harden) 
** Best Screenplay – Adapted (Brian Helgeland) 
* American Cinema Editors: Best Edited Feature Film – Dramatic (Joel Cox)
* American Screenwriters Association: Discover Screenwriting Award (Brian Helgeland)
* Argentine Film Critics Association: Best Foreign Film (Clint Eastwood)
* Australian Film Institute: Best Foreign Film
* Awards of the Japanese Academy: Best Foreign Film
* 57th BAFTA Film Awards: Best Actor in a Leading Role (Sean Penn) Best Actor in a Supporting Role (Tim Robbins) Best Actress in a Supporting Role (Laura Linney) Best Screenplay – Adapted (Brian Helgeland)
* Broadcast Film Critics Association: Best Acting Ensemble, Best Supporting Actress (Mary Gay Harden), Best Director (Clint Eastwood), Best Writer (Brian Helgeland), Best Composer (Clint Eastwood) Cannes Film Festival: Golden Palm (Clint Eastwood)   
* Cinema Writers Circle (Spain): Best Foreign Film
* European Film Awards: Screen International Award (Clint Eastwood)
* 61st Golden Globe Awards: Best Motion Picture – Drama Best Director – Motion Picture (Clint Eastwood) Best Screenplay – Motion Picture (Brian Helgeland)
* IFTA Awards: Best International Film, Best International Actor (Sean Penn)
* International Horror Guild Award: Best Movie
* Motion Picture Sound Editors: Best Sound Editing in Domestic Features – Dialogue & ADR
* Online Film Critics Society: Best Picture, Best Actor (Sean Penn), Best Supporting Actor (Tim Robbins), Best Director (Clint Eastwood), Best Screenplay – Adapted (Brian Helgeland)
* PGA Golden Laurel Awards: Motion Picture Producer of the Year Award
* Robert Festival (Denmark): Best American Film Satellite Awards: Best Motion Picture – Drama, Best Performance by an Actress in a Supporting Role – Drama (Marcia Gay Harden), Best Director (Clint Eastwood), Best Cinematography, Best Film Editing (Joel Cox), Best Sound (Alan Robert Murray, Bub Asman, Michael Semanick, Christopher Boyes and Gary Summers)
* Screen Actors Guild: Outstanding Performance by a Cast in a Motion Picture (Kevin Bacon, Laurence Fishburne, Marcia Gay Harden, Laura Linney, Sean Penn and Tim Robbins), Outstanding Performance by a Male Actor in a Leading Role (Sean Penn)
* Writers Guild of America: Best Adapted Screenplay (Brian Helgeland)

==Home media==
 
The DVD was released on June 8, 2004 and three editions have been released: Full Screen Edition
* Widescreen Edition
* 3-Disc Deluxe Edition including CD Soundtrack

The film has also been released on Blu-ray Disc, both sold separately and as a part of the "Clint Eastwood Collection".

==References==
 
*  
*  
* Ostermann, Eberhard. Mystic River oder die Abwesenheit des Vaters. In: E.O.: Die Filmerzählung. Acht exemplarische Analysen. Munich (Fink) 2007. pp.&nbsp;29–43. ISBN 978-3-7705-4562-9.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 