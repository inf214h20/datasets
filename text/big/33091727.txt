Impulse (2010 film)
{{Infobox film name           = Impulse image          = Impulse 2010.jpg caption        = director       = Scott Beck Bryan Woods producer       = Christy Sullivan Darren Brandl writer         = Scott Beck Bryan Woods starring       = Chris Masterson James Serpento Kristen Norwood music          = Corey Wallace cinematography = Andrew M. Davis editing        = Russell Andrew distributor    = Shorts International ShortsTV released       =   runtime        = 20 minutes country        = United States language       = English
}}
Impulse is a 2010 suspense thriller short film starring Chris Masterson and written and directed by Scott Beck and Bryan Woods. The film is about the last day on earth as deadly events unfold around a man (Chris Masterson) racing to his final and most significant act.  The film made its debut on the closing night of the 2010 LA Shorts Fest and was later invited to the Sofia Independent Film Festival alongside The New Tenants and The Hurt Locker.   Impulse was picked up for distribution by Shorts International and is available worldwide on iTunes, DirecTV, Dish Network and AT&T Uverse. 

==Production== Bluebox Limited Films and shot in Perry, Iowa in November 2009. Post-production was completed at Skywalker Sound and Company 3. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 