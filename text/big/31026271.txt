La liga de las muchachas
{{Infobox film
| name           =La liga de las muchachas
| image          = 
| image size     =
| caption        =
| director       =  Fernando Cortés
| producer       = Óscar Dancigers, Mauricio de la Serna
| writer         = Janet Alcoriza (story), Luis Alcoriza (story)
| narrator       =
| starring       = Elsa Aguirre, Miroslava (actress)|Miroslava, Rubén Rojo 
| music          = Manuel Esperón
| cinematography = Agustín Martínez Solares
| editing        = Carlos Savage
| distributor    = 
| released       = 1950
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1950 Mexico|Mexican film. It was written by, and stars, Luis Alcoriza.

==Cast==
* 	 Elsa Aguirre	 ...	Dorita Miroslava	 ...	Marta
* 	 Rubén Rojo	 ...	Pablo
* 	 Consuelo Guerrero de Luna	 ...	Doña Remedios
* 	 Jorge Reyes	 ...	Amado
* 	 Alma Rosa Aguirre	 ...	Amelia
* 	 José Ángel Espinosa Ferrusquilla	 ...	Mario (as José A. Espinosa Ferrusquilla)
* 	 Rosina Pagã	 ...	(as Rosina Pagan)
* 	 Luis Alcoriza		
* 	 Conchita Carracedo	 ...	Irene
* 	 Antonio Bravo	 ...	Don Adolfo del Toro
* 	 Magda Donato	 ...	Celestina
* 	 Óscar Pulido		
* 	 Antonio R. Frausto	 ...	Don Pedro
* 	 Francisco Aguayo

==External links==
*  

 
 
 
 


 