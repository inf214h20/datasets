The Scarlet Streak
 
{{Infobox film
| name           = The Scarlet Streak
| image          = 
| caption        = 
| director       = Henry MacRae
| producer       = 
| writer         = 
| starring       = Jack Dougherty Lola Todd
| music          = 
| cinematography = 
| editing        =  Universal Pictures
| released       =  
| runtime        = 10 episodes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 action film serial directed by Henry MacRae. This is now considered a lost film. 

==Cast==
* Jack Dougherty - Bob Evans (as Jack Daugherty)
* Lola Todd - Mary Crawford
* John Elliott - Professor Richard Crawford
* Albert J. Smith - Count K (as Al Smith)
* Albert Prisco - Monk
* Virginia Ainsworth - Leontine, Monks accomplice
* Monte Montague - Butler

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 
 