Dangerous Number
{{Infobox film
| name           = Dangerous Number
| image          = Dangerous Number poster.jpg
| image_size     = 225px 
| caption        = theatrical release poster
| director       = Richard Thorpe
| producer       = Lou L. Ostrow  Carey Wilson
| story          = Leona Dalrymple Robert Young Ann Sothern Reginald Owen Cora Witherspoon David Snell Leonard Smith
| editing        = Blanche Sewell
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Carey Wilson. Robert Young, Ann Sothern, Reginald Owen and Cora Witherspoon, and features Dean Jagger. The film was released on January 22, 1937, by Metro-Goldwyn-Mayer.  

== Plot == Robert Young) returns from a year in Japan, learning about a new formula for synthetic silk, to discover that his girlfriend Eleanor (Ann Sothern) is engaged to marry another man. Hank persuades her to jilt the new man at the altar.

After he and Eleanor get married, Hank comes to dislike the show-business friends of his wife and mother-in-law Gypsy (Cora Witherspoon) who pop up at all hours. And a man named Dillman (Dean Jagger) turns up who claims that Eleanor is actually his legal wife, not Hanks.

Hank is distracted by Vera (Maria Shelton), a friend of Eleanors, but in the end pretends to be a cab driver and steers his taxi into a lake, with passenger Eleanor wearing a silk dress Hank gave her that disintegrates in the water.

== Cast == Robert Young as Hank
*Ann Sothern as Eleanor
*Reginald Owen as Cousin William
*Cora Witherspoon as Gypsy
*Dean Jagger as Dillman
*Marla Shelton as Vera
*Barnett Parker as Minehardi
*Charles Trowbridge as Hotel manager

==Production==
The role of "Eleanor" was originally slated to be played by    but it eventually went to Ann Sothern, who was on loan from RKO. LeVoit, Violet.   

== References ==
 

== External links ==
*  
*  
*  

 

 

 
 
 
 
 
 
 

 