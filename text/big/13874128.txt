Keep in a Dry Place and Away from Children
{{Infobox film
| name           = Keep in a Dry Place and Away from Children
| image          = keepindry.Jpg 
| alt            = 
| caption        = Cover to the 1999 EP release
| director       = Martin Davies
| producer       = 
| writer         = Martin Davies, Andy Brown
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = bolexbrothers
| distributor    = 
| released       = 1998
| runtime        = 10 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = 
}}

Keep in a Dry Place and Away From Children is an award-winning 1998 short film by Martin Davies.  The film features a soundtrack by Pram (band)|Pram, who released the music as an EP in 1999. Reception for the short was positive, with the film winning several awards at various film festivals. 

==Synopsis==
The short centers on a baby that is half human, half duck that becomes fascinated with a box of matches with the image of a swan imprinted on it that an adult left on the kitchen counter. As his toys watch, he breaks out of his crib and manages to discover his wings, which had been taken away from him at birth, from a cupboard. The baby grabs the matches and lights one, only for one of his wings to catch fire, plunging him into a sink full of water. His toys then spring into action in an attempt to rescue him, some destroying themselves in the process of trying to get to him. The baby manages to pull the plug, draining the water and inadvertently sending some of his toys down the drain. The baby is then shown motionless at the bottom of the sink except for a single hand twitch. A toy falls next to him and its left ambiguous as to whether he survived the ordeal.

==Awards==
*Aspen Shortsfest, Special Recognition in Animation (1998) 
*Berlin International Film Festival, Panorama Audience Award for Short Films (1998)  
*Chicago International Film Festival, Silver Hugo for Best Animated Short Film (1998)
*Crested Butte Reel Fest, Gold Award for Animation (1998)
*Edinburgh International Film Festival, McLaren Award for Best British Animation (1998) 
*San Francisco International Film Festival, Certificate of Merit in Film & Video - Animation (1998) 

==Soundtrack==
#Space Siren 
#Space Iron (Mouse On Mars Remix) 
#Keep in a Dry Place and Away From Children

==References==
 

==External links==
*  

 
 