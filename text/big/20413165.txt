Seven Hours of Gunfire
{{Infobox Film
| name           = Aventuras del Oeste 
| image          = SevenHoursofGunfire.jpg
| image_size     =
| caption        = 
| director       = Joaquín Luis Romero Marchent
| producer       = 
| writer         = Joaquín Luis Romero Marchent, Ángel de Zavala (book)
| narrator       = 
| starring       = 
| music          =  Angelo Francesco Lavagnino
| cinematography =    
| editor       =  
| distributor    = 
| released       = 1965
| runtime        = 76 minutes
| country        = Spain Italy
| language       = Spanish
| budget         = 
}}
 1965 Spanish-Italian Spaghetti Western film directed by Joaquín Luis Romero Marchent (as José Hernandez).

==Cast==
*Rik Van Nutter as Buffalo Bill Cody (as Clyde Rogers)
*Adrian Hoven as Wild Bill Hickock
*Kurt Großkurth as August Mai
*Helga Sommerfeld as Cora
*Gloria Milland as Calamity Jane
*Robert Johnson Jr.
*Carlos Romero Marchent
*Helga Liné
*Alfonso Rojas as Colonel Carr
*Antonio Molino Rojo
*Francisco Sanz as Pastor Lieberman
*Raf Baldassarre as Guillermo
*Cris Huerta as Steve
*María Esther Vázquez as Agnese

== External links ==
*  

 
 
 
 
 
 


 
 