Rakshassu
{{Infobox film
| name           = Rakshassu
| image          =
| caption        =
| director       = Hassan
| producer       = Areefa Hassan
| writer         = Hassan
| screenplay     = Baby Sukumaran Baby Ajitha Bheeman Raghu
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Arifa Enterprises
| distributor    = Arifa Enterprises
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed by Hassan and produced by Areefa Hassan. The film stars Baby (director)|Baby, Sukumaran, Baby Ajitha and Bheeman Raghu in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Baby
*Sukumaran as Sukumaran
*Baby Ajitha
*Bheeman Raghu as Raghu
*Baby Anju as Shalini
*Kuthiravattam Pappu as Mammad Seema as Seema
*Gomathi as Gomathi
*Sankaradi as Sankaran
*Sathaar as Ravi
*Ratheesh as Ratheesh

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Vasudevan Panampilly, Ramachandran Ponnani and KG Menon. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aamodam || K. J. Yesudas, Ambili, Baby Geetha || Vasudevan Panampilly || 
|-
| 2 || Ee mammadu ikkakkennumennum || K. J. Yesudas || Ramachandran Ponnani || 
|-
| 3 || Snehadhaarayil || Vani Jairam || KG Menon || 
|}

==References==
 

==External links==
*  

 
 
 

 