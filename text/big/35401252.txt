A Dead Man Among the Living
{{Infobox film
| name           =A Dead Man Among the Living 
| image          = 
| image size     =
| caption        =
| director       = Bořivoj Zeman
| producer       = 
| writer         = Sigurd Christiansen (novel)   Elmar Klos   Borivoj Zeman
| narrator       =
| starring       = Karel Höger   Eduard Dubský   Vjaceslav Irmanov   Zdenka Procházková
| music          = Jirí Šust
| cinematography = Petr Rovný
| editing        = Josef Dobřichovský
| distributor    = 
| released       = 1949
| runtime        =
| country        = Czechoslovakia Czech
| budget         =
| gross          =
| preceded by    =
| followed by    = Czech thriller To levende og en død by the Norwegian writer Sigurd Christiansen.  Following an armed robbery at his office, a postal worker suffers a crisis of conscience.

==Cast==
* Karel Höger - Jirí Valta - postal assistant 
* Eduard Dubský - Robert Munk - postal assistant 
* Vjaceslav Irmanov -  Cyril Popov - musician (as Václav Irmanov) 
* Zdenka Procházková - Helena Fejfarová - gardener 
* Lída Matousková - Marta Klecková 
* Jana Hrdličková - Jana Klecková - daughter 
* Radim Nikodém - Ivo Fejfar - son 
* Vladimír Řepa - Inspector 
* František Klika - Chief 
* Milos Hájek - Emil Klecka - postal auditor 
* Frantisek Šlégl - Karel Fejfar - sergeant major 
* Zdenka Baldová - Mr. Fejfarová  Rudolf Deyl - Officer 
* Blažena Slavíčková - Official 
* Jindřich Láznička - Postal attendant 
* Stanislav Neumann - Postal attendant at train 
* Josef Belský - Major 
* Josef Chvalina -  Police officer 
* Bohus Hradil -  Officer at postal management 1 
* Ferdinand Jarkovský - Officer at postal management 2 
* Josef Horánek -  Officer at postal management 3 
* J.O. Martin -  Post director 
* Anna Kadeřábková - Girl at gardening 
* Václav Švec - Rescuer 
* Milivoj Uzelac - Conductor 
* Vlasta Jelínková - Mr. Gabrielová 
* Vladimír Ráž - Officer at package postal 
* Jaroslav Štercl - Member of singer choir

==References==
 

==Bibliography==
* Soila, Tytti & Söderbergh-Widding, Astrid & Iverson, Gunnar. Nordic National Cinemas. Routledge, 1998.

==External links==
* 

 
 
 
 
 
 
 
 


 