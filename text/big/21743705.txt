The Objective
{{Infobox film
| name           = The Objective
| image          = Objectiveposter.jpg
| caption        = Promotional film poster
| director       = Daniel Myrick
| producer       = Zev Guber Jeremy Wall Richard Halpern
| writer         = Daniel Myrick Mark A. Patton Wesley Clark Jr.
| narrator       = Jonas Ball
| starring       = Jonas Ball Matthew R. Anderson Jon Huertas
| music          = Kays Al-Atrakchi
| cinematography = Stephanie Martin
| editing        = Michael J. Duthie Robert Florio
| distributor    = IFC Films
| released       =  
| runtime        = 90 minutes 
| country        = United States Morocco
| language       = English
| budget         = 
| gross          = 
}} Michael C. Williams. It premiered in Morocco in April 2008 and in the United States in February 2009.

==Plot== Special Forces CIA Agent Afghan cleric by the name of Mohammed Aban. The team leader, Wally Hamer, briefs the men to be ready. After being inserted, the team finds a local guide, Abdul, in a village in Southern Afghanistan, where Aban is from. Together, they go to the mountains, where the cleric has a reputation for hiding.
 Persian or Arabic language|Arabic, but no one can understand it. They cache Trinoskis body so that they can move to a safer position for the night. The next morning, the team finds parts of Trinoskis body strewn out across rocks. Further up the mountain, they spot strange, triangular markers made of sticks across the mountains surface. Immediately after seeing these strange upside down triangular markers, radio contact is attempted between Sydney and Bucks. After the second attempt to make contact with Sydney fails, Bucks turns his head to put his radio away,before he even turn his head back, two small bright lights can be seen flying in very tight formation just behind Bucks head. As they continue on foot with their mission in the rocky and barren landscape, fatigue, frustration and confusion take their toll on the members of the team and they come across a cave. Inside, they find an old man who gives them shelter and refills their canteens. Sergeant Sadler notices that under the mans robes, he appears to be wearing a nineteenth-century British army uniform. Sadler tells the others of a legend of how a British regiment disappeared in Afghanistans mountains, leaving only one survivor. In the morning Sergeant Cole observes the old man apparently talking to himself. But when he looks through his night vision goggles, the soldier sees a group of men with swords in black robes. Panicking, the soldier opens fire, accidentally killing the old man. Abdul says they must bury the body, but Keynes orders the team to move on in case the enemy heard them. One soldier, the medic, develops horrible stomach pains. As he tries to drink from a canteen, he finds that it is full of sand, as are everyone elses water containers. Further on, Abdul is surprised to find that there is an entire valley that wasnt there previously. Tensions further increase when the team encounters a bright light at night. As K.T. and Cole try to flank the light, believing it to be a ruse by the Taliban, theyre immediately vaporized. The next morning, Abdul warns Keynes that they are dealing with a supernatural phenomenon that is beyond human conception and has deadly consequences; he then commits suicide by stepping off a cliff.

As the team progresses further and tensions among the men increase, the soldiers confront Keynes and demand the truth. Keynes shows them a recording from his thermal imaging camera and informs them about the real motive. The thermal video shows a triangular object in the desert. Being nearly invisible to the naked eye, it lifts off the ground after three (supposed) men, including Mohammed Aban, walk to it and vanishes right in front of Keynes eyes. It was this object that killed the men. The CIA has been monitoring this phenomenon since 1980 and sent Keynes and the special forces team there to further investigate it. The whole time, Keynes has been recording with his special thermal camera and sending the images to Langley via an advanced laser aimed at CIA satellites. Keynes theorizes that this object originates from an ancient Indian mythology called Vimanas, a sort of UFO-related phenomenon that occurred when Alexander the Great rode through this area of land as he was conquering, and the bright lights and the ghostly gunmen are associated with it. He also explains that the team is an expendable for the investigation and they will not be rescued, which causes a brief scuffle with the agitated sergeant.

Running out of ammunition, water and food the team wanders further into the desert where they finally encounter the vimanas at what appears to be the location that the British regiment was destroyed. Sadler, overwhelmed with fear, opens fire upon the seemingly invisible vimanas only to be vaporized. Keynes flees with Degetau and abandons him later, as he is too sick to continue, and later hears his screams before being obliterated by the objects. Exhausted and traumatized, Keynes searches for water. He encounters an oasis and drinks water from it only to discover the body of Hamer laying next to the water. Unable to grasp the horror, he passes out. When he wakes up in the night, he hears the distant sound of a helicopter and fires his flare gun. Simultaneously, several flares fire up from the valley. The bright light he encountered earlier re-appears and two beings from it approach him. As it touches his forehead, he sees visions and hallucinations of various objects and landscapes from his previous encounters, causing him to go into a trance. In the final scene he is shown floating several inches above a bed with a talisman he took from Abans home in his hand, inside a hospital room, where doctors and a military colonel are observing him through a glass window. In a trance, he finally whispers, "It will save us all..."

In the final credits, interviews of Keynes wife are shown in which she says that the family has not yet been informed about him and concludes him to be missing.

==Cast== CIA Agent Benjamin Keynes
*Matthew R. Anderson as Chief Warrant Officer Wally Hamer
*Jon Huertas as Sergeant Vincent Degetau, Team medic Michael C. Williams as Sergeant Trinoski, Demolitions expert 	
*Sam Hunter as Sergeant Tim Cole, Rifle man 	
*Jeff Prewett as Sergeant Pete Sadler, Sniper 	
*Kenny Taylor as Master Sergeant Tanner, Machine gunner 	
*Chems-Eddine Zinoune as Abdul
*P. David Miller as Major Matt McCarthy
*Vanessa Johansson as Stacy Keanes
*Jacqueline Harris as Matilde Seymour

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 