Sankeertana
 
{{Infobox film
| name           = Sankeertana
| image          = 
| caption        =
| producer       = Dr. M. Gangaiah
| director       = Geetha Krishna
| writer         = Tanikella Bharani  
| story          = Geetha Krishna
| screenplay     = Geetha Krishna
| starring       = Akkineni Nagarjuna Ramya Krishna
| music          = Ilaiyaraaja
| cinematography = P. S. Nivas
| editing        = G.R. Anil Malnad
| studio         = Konark Movie Creations
| runtime        = 2:16:10
| released       =  
| country        =   India
| language       = Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Sankeertana is a 1987 Telugu-language classical film produced by M. Gangaiah on Konark Movie Creations banner, directed by Geetha Krishna. Starring Akkineni Nagarjuna and Ramya Krishna lead roles and music composed by Ilaiyaraaja.

==Cast==
*Akkineni Nagarjuna as Kaasi
*Ramya Krishna as Keerthana
*Girish Karnad
*J. V. Somayajulu
*Sarath Babu  Sai Kumar Rallapalli 
*Sakshi Ranga Rao
*Narra Venkateswara Rao  Mallikarjuna Rao 
*KK Sarma


==Soundtrack==
{{Infobox album
| Name        = Sankeertana
| Tagline     = 
| Type        = film
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 38:54
| Label       = Echo Music
| Producer    = Ilaiyaraaja
| Reviews     =
| Last album  = Aradhana (1987 film)|Aradhana   (1987) 
| This album  = Sankeertana   (1987)
| Next album  = Vijathalu   (1987)
}}

Music composed by Ilaiyaraaja. All the songs are hit tracks. Music released on ECHO Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 38:54
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Kaliki Menulo
| lyrics1 = C. Narayana Reddy  SP Balu, S. Janaki
| length1 = 4:32

| title2  = Manase Padenule
| lyrics2 = C. Narayana Reddy  SP Balu
| length2 = 3:32

| title3  = Manasuna Palikina Sirivennela  SP Balu, S. Janaki
| length3 = 5:06

| title4  = Ve Vela Varnala Sirivennela  SP Balu
| length4 = 3:27

| title5  = Devi Durga Devi
| lyrics5 = C. Narayana Reddy  SP Balu, Vani Jayaram
| length5 = 5:35

| title6  = Vanda Roopayalu Notu
| lyrics6 = C. Narayana Reddy 
| extra6 = S. P. Sailaja
| length6 = 4:33

| title7  = Divi Darula (Kavitha) Sirivennela  SP Balu
| length7 = 1:44

| title8  = Thillana
| lyrics8 = C. Narayana Reddy  SP Balu, S. P. Sailaja
| length8 = 0:47

| title9  = Ganam Aagi Podule
| lyrics9 = C. Narayana Reddy  SP Balu
| length9 = 4:44

| title10  = Ye Naava Ye Theeramo Acharya Athreya 
| extra10 = K. J. Yesudas
| length10 = 4:25

| title11  = Om Kara Vakyam Sirivennela
| SP Balu, S. Janaki
| length11 = 0:51
}}

==External links==
*  

 
 

 
 
 