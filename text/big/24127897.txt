Zor Lagaa Ke...Haiya!
{{Infobox film
| name           = Zor Lagaa Ke...Haiya!
| image          = Zor Laga Ke HaiyaMithun.jpg
| caption        = 
| director       = Girish Girija Joshi
| producer       = Kartikeya Talreja Basant Talreja
| writer         = Girish Girija Joshi
| narrator       = Amitabh Bachchan
| starring       = Mithun Chakraborty Mahesh Manjrekar Seema Biswas Gulshan Grover Sachin Khedekar Riya Sen Raj Zutshi Ritwik Tyagi Ayesha Kaduskar
| music          = Bapi,Tutul
| cinematography = Hari Nair
| editing        = V N Mayekar
| Marketing      = Ashwani Shukla
| genre          = Social
| distributor    = Gemini Pictures
| released       = June 12, 2009
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 2009 Cinema Indian childrens film starring Mithun Chakraborty and Mahesh Manjrekar. The film attracted media attention for its environmental message of Save our Trees. Amitabh Bachchan narrated the film.

==Plot==
A group of children with the help of a beggar try to save some trees from a construction companys intent on cutting them down to make way for new buildings.

==Cast==
 
* Mithun Chakraborty
* Mahesh Manjrekar
* Seema Biswas
* Gulshan Grover
* Sachin Khedekar
* Hardik Thakkar
* Riya Sen
* Raj Zutshi
* Ritwik Tyagi
* Meghan Jhadav
* Ayesha Kaduskar
* Ashwin Chitale

==Reception==
Zor Lagaa Ke...Haiya! received positive reviews and won four film festival awards.  

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 