Ivan Veramathiri
{{Infobox film
| name           = Ivan Veramathiri
| image          = Ivan Veramathiri Poster.jpg
| caption        = Film poster
| director       = M. Saravanan (film director)|M. Saravanan
| producer       = N. Linguswamy  (Presenter)  Thirupathi Brothers|N. Subash Chandrabose Ronnie Screwvala Siddharth Roy Kapur
| writer         = M. Saravanan (film director)|M. Saravanan
| starring       =  
| music          = C. Sathya Shakthi
| editing        = A. Sreekar Prasad
| studio         = Thirupathi Brothers
| distributor    = UTV Motion Pictures
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Tamil romantic thriller film directed by M. Saravanan (film director)|M. Saravanan and produced by UTV Motion Pictures and Thirupathi Brothers.  The film stars Vikram Prabhu, Surabhi, Ganesh Venkatraman, Vamsi Krishna and Hariraj.  The background score and soundtrack were composed by C. Sathya. The film was released on December 13, 2013 to positive reviews.  Its Telugu dubbed version, Citizen, released on March 21, 2014.

==Cast==
* Vikram Prabhu as Gunasekaran Surabhi as Malini
* Ganesh Venkatraman as Aravindan IPS
* Vamsi Krishna as Eeswaran
* Hariraj as Sadasivam Charmila as Malinis mother
* Malavika Menon as Divya
* Nisha Krishnan as Gunasekarans friend

==Production== Saravanan considered Arya and Vishal for the lead role in this film.  Later he said that "I watched the trailer of Kumki and was impressed with Vikram’s intense performance. Later when I met him in person, his height, build and confidence made an impact on me. Moreover, his positive attitude and eagerness to take on challenging roles made me sign him up for my film".  Vikram Prabhu plays a city-bred youth seething with anger which surfaces every now and then. It was reported that Monal Gajjar was selected for an equally important female role.  It was then reported that Surabhi, a Delhi based girl was cast opposite Vikram.  The heroine was not officially confirmed and it was said that the director wanted to reveal her identity once the shooting was completed.  Telugu actor Vamsi Krishna was chosen to play the antagonist role in this film. 

The films principal photography was commenced on September 19, 2012 in Chennai. 

==Soundtrack==
{{Infobox album|  
| Name        = Ivan Veramathiri
| Longtype    = to Ivan Veramathiri
| Type        = Soundtrack
| Artist      = C. Sathya
| Cover       = 
| Caption     = 
| Released    = 6 November 2013
| Recorded    = 
| Genre       = Film soundtrack
| Length      =  Tamil
| Sony Music
| Producer    = C. Sathya
| Last album  = Nedunchalai   (2013)
| This album  = Ivan Veramathiri   (2013)
| Next album  = 
}}

The films score and soundtrack were composed by C. Sathya.

; Tracklist
{{track listing
| extra_column    = Singer(s)
| lyrics_credits  = yes
| total_length    =

| title1      = Malayaala Porattala
| extra1      = Tippu (singer)|Tippu, Hyde Karty, Bizmac
| lyrics1     = Viveka
| length1     =

| title2      = Ennai Maranthaen
| extra2      = Madhushree
| lyrics2     = Na. Muthukumar
| length2     =

| title3      = Ranga Ranga
| extra3      = Rita
| lyrics3     = Mani Amudhavan
| length3     =

| title4      = Thanimayile
| extra4      = Anand Aravindakshan, Nivas
| lyrics4     = Viveka
| length4     =

| title5      = Loveulla
| extra5      = C. Sathya
| lyrics5     = Viveka
| length5     =

| title6      = Idhuthaan
| extra6      = Balram
| lyrics6     = Pudhiyadhoar Kavignan Seivom Team
| length6     = 
}}

==Release==
The satellite rights of the film were sold to STAR Vijay. 

===Critical reception===
Ivan Veramathiri opened to positive reviews by critics.  Baradwaj Rangan wrote, "Ivan Vera Maathiri is several notches above your usual action movie. The coincidences are smartly woven in, the location shooting is nicely done, and some of the action, especially a chase down a busy road, is staged quite well. More importantly, the film never loses sight of the personal costs, the collateral damage, that come with vigilantism".  Sify wrote, "Director Saravanan after the brilliant Engeyum Eppothum is not in his elements in the thriller genre, where he wants to show the nexus between the politician and currupt education system. However, he is able to keep the audience spellbound in the last 15 minutes of the film which is electrifying".  Filmibeat gave 3 out of 5 and wrote, "Director Saravanan has tried hard to write a screenplay to churn out a message-oriented movie with commercial ingredients. His screenplay manages to win audience appreciation for his attempt".  The Times of India gave 3 stars out of 5 wrote, "If the screenplay was the strength of Saravanans debut Enageyum Eppothum, here, it becomes a weakness as the directors attempts to make a "different" vigilante film get lost in implausibility, patronizing and sub-par acting. He has the outline of a terrific action movie plot; However, the scenes arent particularly interesting beyond a strictly functional level".  Rediff gave 2.5 stars out of 5 and wrote, "Ivan Veramathiri lacks the simplicity, depth and magic of Director M Saravanan’s last film Engeyum Eppodhum".  Indiaglitz gave 3 out of 5 saying "Saravanan is known to make his stories gripping and racy. With Sakthi and Srikar Prasad for technical expertise, the director has re-established his name in the tinsel town with Ivan Veramathiri. The films screenplay slows down at places, but that is not entirely a shortfall."   Desimartini gave 3 out of 5.  Behindwoods gave 3 stars out of 5 and wrote, "Ivan Vera Mathiri is likely to leave you satisfied having served its well distributed portions of comedy, romance, action, suspense and all round entertainment". 

===Box office===
At the Chennai box office, the film grossed   on its first day and earned over   during the first weekend.   The film later slowed down at the box office owing to big budget films like Biriyani, Endrendrum Punnagai, Veeram and Jilla, but yet remained steady at multiplexes and completed a 100 days-run in one theater (Sai Shanti in Chennai). 

==References==
 

==External links==
*  
 
 

 
 
 
 
 
 