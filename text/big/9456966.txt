Fräulein Doktor (film)
{{Infobox film
| name           = Fräulein Doktor
| image          = Fräulein Doktor.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Alberto Lattuada
| producer       = Dino De Laurentis
| story          = Vittoriano Petrilli
| screenplay     = Duilio Coletti H. A. L. Craig Alberto Lattuada Stanley Mann Vittoriano Petrilli
| starring       = Suzy Kendall Kenneth More 
| music          = Ennio Morricone
| cinematography = Luigi Kuveiller 
| editing        = Nino Baragli
| studio         = Avala Film Dino de Laurentiis Cinematografica
| distributor    = Paramount Pictures
| released       = 24 January 1969 (Italy) 15 May 1969 (US)
| runtime        = 104 minutes
| country        = Italy Yugoslavia
| language       = English
| budget         =
| gross          = 
}}

Fräulein Doktor is a First World War spy film released in 1969. It was an Italian and Yugoslavian co-production directed by Alberto Lattuada, starring Suzy Kendall and Kenneth More, and featuring Capucine, James Booth, Giancarlo Giannini and Nigel Green. It was produced by Dino De Laurentiis and has a music score by Ennio Morricone. It was distributed by Paramount Pictures in the United States.

==Plot== Lord Kitchener will be sailing on to Russia, and when it will sail.  She then helps a German U-boat to sink HMS HMS Hampshire (1903)|Hampshire outside Scapa Flow with Kitchener on it, taking his life. For this, she is awarded the Pour Le Merite. In flashbacks she recalls her stealing of a formula for a nerve gas which the Germans are to use to great effect against the Allies on the battlefield.  Meyer re-appears in Berlin and courts her.  The German intelligence service is suspicious of Meyers escape from the British but use him to poison Fraulein Doktor because of her addiction to morphine and their distaste for her having murdered Lord Kitchner.  Meyer is shown her dead body and later makes his way back to the British to confirm her death.

But her death was faked for Meyers benefit so she would be free of suspicion for her next assignment, getting Allied defense plans for a German attack in Belgium.  Under cover as a Spanish contessa, she recruits Spanish nurses to staff a hospital train to serve the Allied front.  During the trip from Spain to France, she brings onboard German agents who will impersonate Belgium officers to penetrate Belgium Army headquarters and steal the plans.

Col. Foremen is still not convinced of her death and shows up at the same army headquarters with Meyer in tow. The German agents steal the plans and in a deadly shootout with sentries, one gets away back to German lines.  The Germans then launch their attack with great success, but Col. Foreman confronts Fraulein Doktor.  Meyer then kills Foreman but is then killed by the advancing German troops.  Fraulein Doktor is then whisked away by the Germans, but suffers a breakdown as she is being driven off through all the carnage and death about her.

==Cast==
* Suzy Kendall as Fräulein Doktor
* Kenneth More as Colonel Foreman
* Capucine as Dr. Saforet
* James Booth as Meyer
* Alexander Knox as General Peronne
* Nigel Green as Col. Mathesius
* Giancarlo Giannini as Lieutenant Hans Rupert

==Production and release==
Location shooting for Fräulein Doktor took place in Yugoslavia and Hungary.   on TCM.com 

It was released in Yugoslavia under the name Gospodjica Doktor-pijunka bez imena, and in Italy as Fraulein Doktor .  In the United States, consideration was given to the possible titles "Nameless" and "The Betrayal".   Colonel Mathesius was played by Eric von Stroheim in the 1937 film Under Secret Orders.

==Home media==
In 2011 Fräulein Doktor was released on DVD by Underground Empire, most likely a bootleg. All available screenshots online refer to a TV screening on Finnish TV YLE Teema.  

==See also==
* Elsbeth Schragmüller
*Other films about the spy known as "Mademoiselle Docteur" or "Fräulein Doktor":
** Stamboul Quest &ndash; 1934 American film starring Myrna Loy Street of Shadows) &ndash; 1937 French film directed by G.W. Pabst
** Mademoiselle Doctor (also known as   

==References==
Notes
 

==External links==
* 
* 
*  at Allmovie.com

 
 
 
 
 
 
 
 
 
 
 
 