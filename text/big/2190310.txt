It's All Gone Pete Tong
 
{{Infobox film
| name           = Its All Gone Pete Tong
| image          = ItsAllGonePeteTong2005Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Michael Dowse
| producer       =  
| writer         = Michael Dowse
| starring       = Paul Kaye Beatriz Batarda Kate Magowan Mike Wilmot
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Matson Films
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = English Spanish
| budget         = 
| gross          = $1,591,879  
}} independent film DJ who goes completely deaf. The title is a reference to a cockney rhyming slang phrase used in Britain from the 80s to present day, referring to the BBC Radio 1 DJ Pete Tong, standing for "its all gone a bit wrong."

The film was released on April 15, 2005. The DVD was released on September 20, 2005. It won two awards at the US Comedy Arts Festival for Best Feature and Best Actor (Paul Kaye) and swept the Gen Art Film Festival awards (Grand Jury and Audience). It was filmed on location in Ibiza and shot entirely in High-definition television|HD.  
 Barry Ashworth, Paul van Dyk, Lol Hammond and Pete Tong appear in the film. Ibiza locations used in the movie include music venues; Pacha, Amnesia (nightclub)|Amnesia, Privilege Ibiza|Privilege, DC10 (nightclub)|DC10, the historic Pikes Hotel and Cala Longa beach.

A remake has been made by Indian film director Neeraj Ghosh titled Soundtrack (film)|Soundtrack which was released in 2011.

==Plot==
 Arsenal Association football match mixer onto the dance floor, and is forcibly removed from the club. 

The next day, Max confronts Frankie about the performance. Frankie agrees to see a doctor, who tells him hes lost hearing in one ear and has 20% left in the other. He warns Frankie that unless he stops abusing drugs and listening to loud noises, he will soon be completely deaf. Even the use of his hearing aid would only further degrade his hearing.

Then, during a recording session, Frankie confesses the full nature of his hearing loss to Alfonse. He inserts his hearing aid to demonstrate, and, overwhelmed by the sudden sound exposure, leans close to one of the monitor speakers. Before he can react, however, a frustrated Horst smashes a guitar into an amplifier whose volume Frankie has maximized. The noise is excruciating, and the feedback knocks Frankie unconscious. The damage leaves him permanently deaf. 
 Roman candles around his head, either an attempt at suicide or a drastic way to recover his hearing, but dives into the pool before they ignite.

In a culmination moment of the movie, Frankie flushes all his drugs down a toilet, only to be faced with the vision of the menacing badger again. The two begin to scuffle, with Frankie eventually gaining the upper hand and beating the badger with a shovel, then grabbing a shotgun and shooting it, upon which the badger begins to bleed cocaine. Frankie then grabs the dying badgers head, and removes it, only to reveal that the badger is, in fact, himself. He then shoots the dying vision of himself in the head.

After this dark period, Frankie finds a deaf organization and meets Penelope, an instructor for the deaf who coaches him in lip-reading. They become close, and eventually intimate. He confides his unhappiness at losing music, and she helps him perceive sound through visual and tactile methods instead.
 an oscilloscope trace while resting his feet on the pulsating speakers, a callback to an earlier claim he made in the film about flip flops. Using this system, he heads to the studio and manages to produce new mix CD (Hear No Evil) entirely by himself. He delivers it to Max, who is wildly pleased – particularly by the potential of using Frankies disability to increase record sales. He has Frankie take part in advertising and promotional deals which are increasingly offensive and insensitive to deaf people, which Penelope silently disapproves of. He also treats Penelope like he did Sonya; as Frankies sexual object, not recognizing her substantial role in Frankies life. In general, Max tends to patronizingly characterize the deaf as pained, helpless victims desperate for a deaf role model. 

Max convinces Frankie to play live at Pacha as a career comeback. He thinks it is an opportunity for Frankie to prove himself to others, despite Frankies insistence that he has nothing to prove to his critics. The gig goes exceedingly well, and many claim it showcases even greater talent than his early work. After the show, Frankie and Penelope disappear from Max, the media, and the music scene altogether. In a talking heads sequence, characters speculate on where he is now (if alive). 

As the film ends, we see Frankie disguised as a homeless street musician, who is then met by Penelope and a child (presumably their own). They affectionately walk together down a street unrecognized.  Additionally, we see Frankie teaching a group of deaf children how to perceive sound like he does.

==Characters==
===Primary===
 
*Frankie Wilde (Paul Kaye) is the king of DJs, slowly losing his hearing, and soon to lose everything he thinks is important to him: his job, his fame and his trophy wife.
*Penelope (Beatriz Batarda) is the deaf lip-reading instructor who gives Frankie the tough love he never had and always needed.
*Sonya (Kate Magowan) is Frankie Wildes supermodel wife. Her days are filled with deciding on what theme is more appropriate for their garden: Japanese or Spanish?
*Max Haggar (Mike Wilmot) is Frankies agent. Fat, balding, and brash, Max is all about money and his cell phone is his lifeline.
*Jack Stoddart (Neil Maskell) is the ruthless CEO of Motor Records who has no sympathy for Frankie. He says, "I didn’t want a deaf DJ on the label. I didn’t want the company to be touched with the deaf stamp. Well, business is tough and sometimes you have to make awkward decisions and I’ve made harder decisions than dropping the deaf DJ."

==Music==
===Soundtrack=== double disc soundtrack for the film.

{{Infobox album  
| Name        = Its all gone pete tong: original soundtrack recording
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = IAGPTOSR.jpg
| Released    =  
| Recorded    = House Balearic Balearic house Chill out Trance Big beat Techno Alternative rock Drum and bass
| Length      = 
| Label       = EMI
| Producer    = Executive Album Producer – Ben Cherrill For Positiva Records
| Reviews     =
}}

==Track listing==
===CD 1===
 Pacific State" – 808 State (exclusive mix)
#"Cloud Watch" – Lol Hammond
#"Dry Pool Suicide" – Graham Massey Moonlight Sonata" – Graham Massey
#"Baby Piano" – Lol Hammond
#"Ku Da Ta" – Pete Tong
#"Mirage" – Moroccan Blonde (Ben Cherrill, James Doman and Lol Hammond)
#"Troubles" – Beta Band
#"Parlez Moi DAmour" – Lucienne Boyer
#"Need To Feel Loved (12" Club Mix)" – Reflekt
#"Its Over" – Beta Band
#"Halo (Goldfrapp Remix)" – Depeche Mode
#"How Does It Feel?" – Afterlife
#"Holdin On" – Ferry Corsten
#"Four-Four-Four" – Fragile State
#"Music for a Found Harmonium"	– Penguin Café Orchestra
#"Learning to Lip-Read" – Graham Massey
#"Good Vibrations" – The Beach Boys
#"Interlude" – Ben Cherrill and James Doman
#"White Lines" – Barefoot

===CD 2===

#"Intro
#"DJs in a Row" – Schwab Deep Dish
#"Good 2 Go" – Juggernaut (Ben Cherrill and James Doman) Mixed With "Rock That House Musiq" – Christophe Monier and DJ Pascal feat. Impulsion Black Rock Shapeshifters
#"Up & Down" – Scent
#"Serendipity" – Steve Mac & Pete Tong Presents Lingua Franca 
#"Plastic Dreams (Radio Edit)"	– Jaydee
#"Rock Your Body Rock" – Ferry Corsten
#"Can You Hear Me Now"	– Double Funk feat. Paul Kaye (Ben Cherrill and James Doman)
#"Musak (Steve Lawler Mix)" – Trisco
#"Yimanya" – Filterheadz Reflekt feat. Delline Bass Chris Cox Orbital

==Film score==
Songs featured in film but not included in the soundtrack:
#"Al Sharp" – The Beta Band
#"Flamenco" – Flamenco Ibiza
#"Get On" – Moguai
#"G-Spot" – Lol Hammond
#"Hear No Evil" – Lol Hammond
#"I Like It (Sinewave Surfer Mix)" – Narcotic Thrust
#"Messa da Requiem" – Riccardo Muti/La Scala Milan
#"Electronika" – Vada Rise Again" – DJ Sammy
#"Ritcher Scale Madness" – ...And You Will Know Us by the Trail of Dead Michael McCann
#"Up & Down (Super Dub)" – Scent
#"You Cant Hurry Love" – The Concretes

==DVD Extras==
The U.S. version of the DVD includes 5.1 Dolby Digital, Subtitles, and includes several extras that were part of the online/Web marketing campaign: Frankie Wilde: The Rise, Frankie Wilde: The Fall, and Frankie Wilde: The Redemption.

==Awards==
===Won===
* Best Canadian Feature – Toronto International Film Festival – 2004
* Best Feature – US Comedy Arts Festival – 2005
* Best Actor (Paul Kaye) – US Comedy Arts Festival – 2005
* Grand Jury Award – Gen Art Film Festival – 2005
* Audience Award – Gen Art Film Festival – 2005
* Best British Columbian Film – Vancouver Film Critics Circle – 2005 Canadian Comedy Awards – 2005
* Best Overall Sound – Leo Awards – 2005
* Best Sound Editing – Leo Awards – 2005
* Best Feature-Length Drama – Leo Awards – 2005

===Nominated===
* Best Actor, Best Feature – Method Fest
* Best Achievement in Production – BIFA
* 8 Genie Awards

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 