Aalemane
{{Infobox film
| name = Aalemane, ಆಲೆಮನೆ
| image = Aalemane.JPG
| caption = DVD cover
| director = Mohan Kumar
| producer = D. Mallayya, M.Muniswamy, V. Krishnamurthy, M. Manjula, Venkatamma
| released = 1981
| runtime =
| language = Kannada C Ashwath, L Vaidyanathan
| lyrics = Doddarangegowda
| cinematography = B. C. Gowrishankar
| editing = A Subramanya
| studio = Shree Sougandhika Film Productions
| starring = Suresh Heblikar, Mohan Kumar, Roopa Chakravarthi, Upasane Seetaram
}}
 Vaidi duo. The film was a musical hit and won the Karnataka State Film Award for Best Lyricist to Prof. Doddarangegowda. 

==Story==
Bhaira is a villager well known for his psychotic nature and his beautiful wife Mala. At the dusk of the one evening Mala is brutally raped and murdered. The villagers blame Bhaira for Malas murder and are on the move to punish him. Bhaira confronts them the truth, but his truth is unheard. Bhaira then seeks help of the new village teacher and asks him to trace the truth for Bhaira. Will the teacher be able to end this blame game, story continues on this line........

==Soundtrack==
{| class="wikitable"
|-
! SL.No
! Song
! Lyricist
! Artist
|-
| 1
| Nammoora Mandara Hoove
| Doddarangegowda
| S P Balasubramanyam
|-
| 2
| Hottare Sooryanage
| Doddarangegowda
| S P Balasubramanyam, S. Janaki|S.Janaki
|-
| 3
| Ella Melu Keelu
| Doddarangegowda C Ashwath
|-
|} Namoora Mandara Hoove starring Shivarajkumar, Ramesh and Prema.

==References==
 

 
 
 


 