Clara Gibbings
 
 
{{Infobox film name           = Clara Gibbings image          = caption        = producer       = F.W. Thring director  Frank Harvey writer  Frank Harvey based on       = play by Aimee & Philip Stuart starring       = Dorothy Brunton Campbell Copelin music          = cinematography = Arthur Higgins editing        = studio  Efftee Film Productions distributor    = released       =   runtime        = 81 minutes country        = Australia language       = English budget         = £5,000 "Counting the Cash in Australian Films", Everyones 12 December 1934 p 19-20 
}}
Clara Gibbings is a 1934 Australian film directed by F.W. Thring about the owner of a London pub who discovers she is the daughter of an earl. It was a vehicle for stage star Dorothy Brunton. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p166 

==Synopsis==
Clara Gibbings discovers she is the legitimate but abandoned daughter of the Earl of Drumoor. She becomes a member of high society but soon becomes disillusioned with their morals. She falls in love with a young aristocrat, Errol Kerr, who proposes, and they go off to live in Australia.

== Cast ==
*Dorothy Brunton as Clara Gibbings
*Campbell Copelin as Errol Kerr
*Harvey Adams as Justin Kerr
*Noel Boyd as Yolande Probyn
*Harold Meade as Earl of Drumoor
*Byrl Walkley as Lady Drumoor
*Marshall Crosby as Tudor
*Russell Scott as Gallagher
*Guy Hastings as Ted

==Original Play==
{{Infobox play
| name       = Clara Gibbings
| image      = 
| image_size = 
| caption    = 
| writer     = Aimee & Philip Stuart 
| characters = 
| setting    = 
| premiere   = London
| place      = 1929
| orig_lang  = English
| subject    = 
| genre      =melodrama
| 
}}
The script was one of a number of play adaptations from F.W. Thring.  It was based on a 1929 English play  which originally been presented by Thring in Melbourne (one of the cast, Beatrice Day, collapsed and died during rehearsal).  It had also been produced on Broadway under the title of Lady Clara starring Florence Nash. 

==Production==
The film was shot at Efftees St Kilda studios in early 1934. Although Thring was credited as director, it is likely Frank Harvey did most of the actual direction on set. 
 The Streets of London (1934), and announced plans to revive production, but died before he was able to. 

"I took one look at myself in the rushes — and looked away", admitted Brunton. "I simply could not bear to see myself any more. I thought I looked terrible." 

==Reception==
The film was previewed in September and released in Melbourne at the Mayfair Cinema on 13 October where it was reported as "recording excellent business".   Reviewers commented on the fact it was basically a filmed play. 

It won third prize (amounting to £750) in a competition held by the Commonwealth government in 1935.  The judges said the film "contained sparkling dialogue supported by competent acting, although the adaptation of the English play on which it was based was inadequate."  However, as of 1936 the film had not been seen on Sydney screens.  It was released in England but received poor reviews. 

Peter Fitzpatrick, biographer of Thring, later described the movie as looking "like a run-of-the-mill British B-picture, and that is at once a badge of proficiency and a mark of its remoteness from everything that Effree stood for." 

== References ==
 
*Fitzpatrick, Peter The Two Frank Thrings, Monash University, 2012

==External links==
* 
*  at Oz Movies killed WW1 airman William Bonds widow, in Bonds article.

 

 
 
 
 
 