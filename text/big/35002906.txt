Bab Sebta
{{Infobox film
| name           = Bab Sebta
| image          = 
| caption        = 
| director       = Pedro Pinho Frederico Lobo
| producer       = Luisa Homen
| writer         = 
| starring       = 
| distributor    = 
| released       = 2008
| runtime        = 110 minutes
| country        = Portugal
| language       = 
| budget         = 
| gross          = 
| screenplay     = Pedro Pinho Frederico Lobo
| cinematography = Pedro Pinho Luisa Homem Frederico Lobo
| editing        = Editing Luisa Homem Frederico Lobo Rui Pires Claudia Silvestre Pedro Pinho
| music          = 
}}
 Portuguese 2008 documentary film.

== Synopsis == Melilla and Ceuta border fences, Bab Sebta interviews people in four North African cities to explore why some people are willing to risk all to emigrate to Europe. Interviews took place in Tangier and Oujda in Morocco, and Nouadhibou and Nouakchott in Mauritania.      

== Awards ==
* FIDMarseille 2008
* Doclisboa 2008

==See also==
*Victimes de nos richesses, a 2006 documentary film about violence at the Melilla and Ceuta border fences.

== References ==
 
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 