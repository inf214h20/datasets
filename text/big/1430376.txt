Life Is Sweet (film)
 
 
{{Infobox film
| name           = Life Is Sweet
| image          = Life is sweet.jpg
| image_size     =
| caption        =
| director       = Mike Leigh
| producer       = Simon Channing Williams
| writer         = Mike Leigh
| narrator       =
| starring       = Alison Steadman Jim Broadbent Claire Skinner Jane Horrocks Timothy Spall
| music          = Rachel Portman Dick Pope Jon Gregory
| studio = Thin Man Films
| distributor    = Palace Pictures (UK) October Films (US)
| released       =  
| runtime        = 103 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          = US$1,516,414 (USA)   
}}
Life Is Sweet is a 1990 British comedy-drama film directed by Mike Leigh, starring Jim Broadbent, Alison Steadman, Claire Skinner, Jane Horrocks and Timothy Spall. Leighs third cinematic film, it was his most commercially successful title at the time of its release.  The, by turns, tragi-comic story follows the fortunes of a working-class North London family over a few weeks one summer.

==Plot== binges on chocolate and snacks, then forces herself to vomit. Natalie – awake in the next room – overhears her.

Aubrey (Timothy Spall), a hyperactive but emotionally labile family friend, is opening a Parisian-themed restaurant named Non, je ne regrette rien|The Regret Rien. Wendy accepts a part-time job as waitress in the restaurant, but her and Andys initial confidence in the scheme is undermined by Aubreys unorthodox approach to the interior décor (a cluttered, half-realised combination of outmoded French clichés, such as a bicycle in the bay window, and of tasteless Victoriana such as a stuffed cats head framed by broken accordion sconces) and by his menu. His singularly grotesque interpretation of the excesses of nouvelle cuisine includes dishes such as saveloy on a bed of lychees, liver in lager and pork cyst.
 chocolate spread from her chest – a practice to which he only reluctantly agrees. He ultimately loses patience with her, accusing her of being "a bit vacant" and incapable of having a sincere, adult conversation or allowing herself to enjoy his companionship. She calls his bluff and loses: frustrated but resolute, he leaves her and her fragile emotional state deteriorates even further.

The opening night of The Regret Rien is a disaster. Wendy volunteers her help when it becomes clear that Aubreys waitress has let him down – she has gone to Prague with her boyfriend. And Aubrey forgot to advertise the opening of the restaurant, with the result that no customers turn up. Aubrey gets hopelessly drunk, takes to the pavement and rails against the world, tells Wendy that he fancies her, starts taking his clothes off and passes out, a quivering, sobbing gelatinous blob of disappointment.  Wendy is forced to deal not only with him but with his glum and passive sous-chef/dogsbody, Paula (Moya Brady).

Meanwhile, Andy and Patsy have gone to their local pub, where Andy gets uncharacteristically but emphatically drunk and ends up sleeping inside the decrepit fast-food van in his driveway. Wendy returns home from the disastrous opening night of Aubreys restaurant to find him there: unnerved by her bizarre evening, she loses her temper with the whole family.

Phlegmatic and dry-humoured Natalie enjoys her unconventional work as a plumber, the simple pleasures of a pint and a game of pool, and dreams of visiting the USA. In contrast, the fidgety and isolated Nicola becomes increasingly agitated, aggressive and reclusive, and Wendy finally confronts her.  During the course of their long and anguished confrontation, Wendy makes it clear to Nicola that she is deeply worried about her, for example, wondering why she makes no attempt to get involved with the causes she claims to believe in. She tells Nicola of the struggle she and Andy endured to care for their baby daughters – how it meant she never went to college and Andy working in a "job he hates."  It emerges that during an earlier phase of Nicolas bulimia, she almost starved to death. Ashamed and angry, Nicola is convinced that Wendy and the rest of the family hate her. Instead, as the exasperated Wendy tells her, "We dont hate you!  We bloody love you, you stupid girl!" and leaves the room, deeply upset. The brittle behavioural armour with which Nicola has protected her psyche is now shattered and she breaks down sobbing.

Meanwhile, Andy is seen running his kitchen at work with energy and authority but slips on a spoon, breaking his ankle. Wendy receives the news with a characteristic mixture of sympathy and amusement. She drives him home from the hospital; aided by Natalie she makes him comfortable, and then goes to see Nicola, still in her room. Mother and daughter reconcile.

The film ends with Natalie and Nicola sitting peacefully in the evening sunshine in the back garden. Natalie observes that Nicola must own up to her parents about her bulimia. She then asks Nicola "Dyou want some money?" and Nicola accepts gratefully, the first time in the film where she has accepted an offer of help.

==Cast== baby clothing shop and teaches a dance class to young children. She is the emotional core of the family and talks continually, keeping up an amused running commentary on everything around her, but concerned about the welfare of her family, especially her troubled daughter Nicola. She loves her husband, but recognises that he lacks entrepreneurial spirit; she describes him as having "two speeds, slow and stop". head cook in an industrial kitchen. Andy is presented as a loving but slightly ineffectual husband and father, fond of tinkering in his shed and buying broken things which he plans to get around to fixing at some unspecified future date. By contrast, the scenes depicting Andy at work show him as a highly competent executive chef. pool and drinking with her male workmates. She never shows any interest in dating or romance, but reads travel brochures about the USA in her room at night. Natalie is described by her mother as "happy", but she is the only principal character in the film who never smiles. binges on them and then makes herself vomit. on the turn", all the while pummelling it between his hands as if it were an American football. " Aubrey is like a disc jockey, with a phoney transatlantic air about him and a misconception of his own image that has spiralled into grotesque parody. He comes from St Albans but he sounds like Kid Jensen."  He appears to harbour unrequited lusts for both Wendy and Nicola.

==Production==
The film was a co-production between British Screen Productions, Channel Four Films and Thin Man Films, a production company created by Mike Leigh and producer Simon Channing-Williams. {{cite book
 | last=Raphael
 | first=Amy
 | year=2008
 | title=Mike Leigh on Mike Leigh
 | isbn =978-0-571-20469-4
 | place=London
 | publisher=Faber & Faber
 | page=384}}
  This was the first release by Thin Man, who have produced all Leighs films since Life Is Sweet. 
 improvising and rehearsing for several weeks prior to actual shooting. For example, Aubreys bizarre recipes were devised by Leigh and Timothy Spall over the course of an evening, and then checked for plausibility with a professional chef, who advised them about which ones were technically impossible to prepare; all the ones that appear in the film are, as Leigh put it, "all feasible, gross as it sounds." 

David Thewlis, who played Nicolas anonymous lover, was disappointed at being given such a small role. Leigh promised him that the next time he considered Thewlis for a role in a film, "hed be given a fair slice of the pie."  Thewlis next role in a Leigh film was his award-winning performance as the lead character Johnny in Naked (1993 film)|Naked. 

The film was shot entirely on location in London Borough of Enfield|Enfield, Middlesex, UK and used local people as extras including an Enfield based dance school for the opening title sequence. 

Alison Chitty found the house in Enfield for Life is Sweet and fell in love with it because of its garden shed. She also found the old mobile snack-bar, which Reas Patsy sells on to Broadbents Andy as a pig in a poke, in Northampton, and painted it up. 

Life is Sweet s visual world is bright, jaunty, primary-coloured – Leighs next film Naked (1993 film)|Naked was conceived in blacks and blues and a dark, dilapidated grunginess, the contrast with this, its predecessor, very marked. 

==Critical reception==
The film received very favourable reviews, and it is one of the few films on the Rotten Tomatoes review collaboration site to enjoy a 100% fresh rating at present.   The Guardian film reviewer awarded the film seven stars out of a possible ten.  Roger Ebert in the Chicago Sun-Times was full of praise, commenting that in spite of the constraints of independent film production, the film was "as funny, spontaneous and free as if it had been made on a lark by a millionaire" He added that "By the end of Life is Sweet we are treading close to the stuff of life itself – to the way we all struggle and make do, compromise some of our dreams and insist on the others. Watching this movie made me realize how boring and thin many movies are; how they substitute plots for the fascinations of life." 
Hal Hinson of the Washington Post called the film "sublime" and "gently brilliant". 
Desson Thompson of the same paper agreed, praising Leigh for discovering "the tragic beauty of the mundane". 
 This Happy Breed, evoked by Leigh in several panning shots across suburban back gardens, is patronising. Coward and Lean pat their characters on the back...Leigh shakes them, hugs them, sometimes despairs over them, but never thinks that they are other than versions of ourselves." 

==Cultural references==
Aubreys restaurant The Regret Rien is named after the 1956 song "Non, je ne regrette rien" by Charles Dumont and Michel Vaucaire, made famous by French singer Edith Piaf.

Andy often speaks in comic voices, at one point uttering the out-of-context line "Hes fallen in the water!". This was the catchphrase of Little Jim, a recurring character from the 1950s BBC radio comedy programme The Goon Show. 
 football club. According to Leigh this was a source of some discomfort to Stephen Rea who played the character, since Rea is a supporter of the teams North London derby|long-term rivals Arsenal F.C.|Arsenal. 

==Awards and nominations==
*Los Angeles Film Critics Association Awards, USA, 1991: Best Supporting Actress – Jane Horrocks
*Independent Spirit Awards, USA, 1992: Best Foreign Film (nominated)
*Bodil Awards, Denmark, 1992: Best European Film
*London Critics Circle Film Awards, UK, 1992: British Film of the Year
*National Society of Film Critics Awards, USA, 1992
**Best Film
**Best Actress – Alison Steadman
**Best Supporting Actress – Jane Horrocks 

==DVD==
The Region 2 DVD of Life Is Sweet was released on 11 February 2002. The Blu-ray was released by the Criterion Collection in May 2013.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 