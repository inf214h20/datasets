Raja Huli
{{Infobox film
| name           = Raaja Huli ರಾಜಾ ಹುಲಿ
| image          = 
| caption        =
| director       = Guru Deshpande
| producer       = K. Manju
| writer         = S. R. Prabhakaran
| story          = S. R. Prabhakaran
| based on       =   Yash Meghana Raj
| music          = Hamsalekha
| cinematography = K. M. Vishnuvardhana
| editing        = K. M. Prakash
| studio         = Laskhmisri Combines
| released       =  
| runtime        = 158 minutes
| country        = India
| language       = Kannada
| budget         =  
| gross          =  
}} Yash and Meghana Raj in the lead roles. The soundtrack and score for the film is composed by Hamsalekha   The film, which made its theatrical release on 1 November 2013, co-inciding with the Kannada Rajyotsava festival  became a Super Hit at the box office, becoming Yash (Kannada actor)|Yashs 3rd consecutive Super Hit after Drama (film)|Drama and Googly (film)|Googly.   

==Cast== Yash as Raja Huli    
* Meghana Raj as Cauvery 
* Harsha
* Chikkanna as Chikka 
* Charan Raj
* Vasishta N. Simha as Jagga
* Ashwini

==Soundtrack==
{{Infobox album  
| Name        = Raja Huli
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = Raja Huli audio.jpeg
| Border      = yes
| Caption     = Soundtrack cover
| Released    =  
| Recorded    = 2013 Feature film soundtrack
| Length      = 21:24
| Label       = D Beats
| Last album  = 
| This album = Raja Huli (2013)
| Next album  = 
}}

Hamsalekha composed the music for the film and the soundtracks, also writing the lyrics for four soundtracks. Lyrics for the soundtrack "Falling In Love" was penned by Yogaraj Bhat. The album has five soundtracks. 

{{Track listing
| total_length   = 21:24
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Chaltha Chaltha
| lyrics1 	= Hamsalekha
| extra1        = Sonu Nigam
| length1       = 4:03
| title2        = Falling In Love
| lyrics2 	= Yogaraj Bhat
| extra2        = V. Harikrishna
| length2       = 3:38
| title3        = Kaveri Kaveri
| lyrics3       = Hamsalekha
| extra3 	= Kavita Krishnamurthy
| length3       = 4:40
| title4        = Loveenalli Bidre
| extra4        = Hemanth, Shamitha Malnad
| lyrics4  	= Hamsalekha
| length4       = 5:03
| title5        = Om Hindu Guruthu
| extra5        = Shankar Mahadevan
| lyrics5       = Hamsalekha
| length5       = 4:00
}}

==Box Office==
Raja Huli was released on November 1, 2013, the day of Kannada Rajyotsava and got a fantastic opening at the box office upon release.    The film took the box office by storm despite facing competition from a major Bollywood release Krrish 3. According to trade reports, collections of Raja Huli was better than Krrish 3 and Tamil flick Arrambam at Bangalore Box Office.  The film also did very good business on weekdays and collected approximately  5 crore Nett and  8 crore Gross at the box office in the first week. The movie, which was made with a budget of  6 crore, recovered its production cost in just seven days.  Raja Huli went on to complete 119 days of successful run and collected a total of  13 crore at the box office and was declared a Super Hit, becoming the sixth highest grossing Kannada film of 2013.  Raja Huli also became Yash (actor)|Yashs third consecutive success after Drama (film)|Drama and Googly (film)|Googly,  thus cementing Yashs reputation as one of the most consistent actors in Kannada cinema. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 