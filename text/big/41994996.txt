Buddy Hutchins
{{Infobox film
|name=Buddy Hutchins
|starring=Jamie Kennedy Sally Kirkland Sara Malakul Lane
|director=Jared Cohn
|writer=Jared Cohn
|producer=Richard Switzer Gabriel Campisi Santino Aquino Judy Kim
|released=14 February 2015
|language=English
|country=United States
}}
 thriller film starring Jamie Kennedy  and written and directed by Jared Cohn and produced by Richard Switzer. 

==Plot==
Buddy Hutchins is just a regular guy doing his best to support a wife and two kids. A recovering alcoholic, Buddy hasnt had a drink for the better part of a year, but it turns out the only reward for his good behavior is a failing business and a cheating wife. Throw in a ruthless bounty hunter and a hot-tempered ex, and Buddys already short fuse is about to blow. Pushed over the edge and armed with a chainsaw, Buddy Hutchins is out for blood.Волков В

==Cast==
*Jamie Kennedy as Buddy Hutchins
*Sally Kirkland as Bertha
*Sara Malakul Lane as Evelyn
*David Gere as Don
*Steve Hanks as Troy
*Richard Switzer as Joel
*Demetrius Stear as Ryan

==Production==
The film was shot in February 2014 in Los Angeles, California with a large portion shot in director Jared Cohns house.  The film was executive produced by Santino Aquino, Gabriel Campisi, and Judy Kim, and stars Jamie Kennedy, Sally Kirkland, and Sara Malakul Lane.  The screenplay was also written by Cohn, ten years prior to production. 

==Release==
The film was released on February 14, 2015 (Valentines Day) in the United States.  

==References==
 

==External links==
* 

 
 
 
 
 
 