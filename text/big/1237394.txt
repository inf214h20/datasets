Anne of Green Gables (1985 film)
 
{{multiple issues|
 
 
}}

{{Infobox television film
| bgcolour =
| name = Anne of Green Gables
| image = Anne dvd.jpg
| caption = DVD cover
| format = Drama
| runtime = 199 minutes (approx.)
| creator = Kevin Sullivan Ian McDougall Kevin Sullivan
| writer = Lucy Maud Montgomery (original novels) Kevin Sullivan (adaptation) Joe Wiesenfeld (adaptation)
| starring = Megan Follows Colleen Dewhurst Richard Farnsworth Patricia Hamilton Marilyn Lightstone Schuyler Grant Jonathan Crombie
| music = Hagood Hardy
| country = Canada
| language = English CBC
| released = December 5, 1985 (Canada) February 17, 1986 (U.S.) Anne of Avonlea
}} television drama of the Canadian author Kevin Sullivan for the Canadian Broadcasting Corporation. It was released theatrically in Iran, Israel, Europe, and Japan.
 PBS in the United States on the series WonderWorks.

==Plot==
11 year old orphan Anne Shirley is living in servitude with the cruel Hammond Family in Nova Scotia. However, when Mr. Hammond dies, Anne is sent to an orphanage where she eventually receives the wonderful news that she has been adopted by a couple on Prince Edward Island.
Upon arriving on P.E.I, Anne is met at the train station by the elderly Matthew Cuthbert who is surprised to find a girl there instead of a boy. Matthew and his sister Marilla had requested a boy to help them with the farm chores but he couldn’t very well just leave the girl at the train station. Matthew decides to take Anne to meet Marilla and on the buggy ride home becomes completely smitten with the red-haired orphan girl.

When Anne Shirley arrives at the Cuthbert’s farm called "Green Gables", she is a precocious, romantic child desperate to be loved and highly sensitive about her red hair and homely looks. In her own unique headstrong manner, Anne manages to insult the town gossip, Rachel Lynde, in a dispute over her looks; smash her slate over Gilbert Blythe’s head when he calls her "Carrots" on her first day of school; and accidentally dyes her hair green in an effort to turn her red hair black and salvage her wounded pride.

Marilla Cuthbert is shocked and beside herself to know how she will ever cope with this sensitive, headstrong child so desperate to fit in. But shy, gentle Matthew is always there to defend Anne and hold her up on a pedestal.
 Currant Wine Cordial at a tea party. Diana’s mother and Rachel Lynde turn on Marilla for making wine in the first place. Hence Anne moves from one mishap to the next as her wild imagination and far-fetched antics combine to constantly bring trouble onto her shoulders.

Anne finds her element in the academic world, ultimately competing neck and neck with Gilbert Blythe who becomes her arch opponent. Anne and Gilbert go on to win the highest academic accolades, constantly vying for honors at every level. Eventually their fierce rivalry turns to a secret affection, which blossoms into love.
Marilla tries to prevent Anne from seeing Gilbert because Anne is still quite young and Marilla wants Anne to continue her education. In the end, however, when Matthew dies and forces Marilla into considering selling Green Gables, Gilbert gives Anne his teaching post in nearby Avonlea so she can stay at Green Gables and continue to support Marilla.

==Cast==
 
 
* Megan Follows – Anne Shirley
* Colleen Dewhurst – Marilla Cuthbert
* Richard Farnsworth – Matthew Cuthbert
* Patricia Hamilton – Rachel Lynde
* Marilyn Lightstone – Miss Stacy
* Schuyler Grant – Diana Barry
* Jonathan Crombie – Gilbert Blythe
* Charmion King – Aunt Josephine Barry
* Jackie Burroughs – Amelia Evans
* Rosemary Radcliffe – Mrs. Barry Joachim Hansen – John Sadler
* Christiane Kruger – Mrs. Allan Cedric Smith – Rev. Allan
*Paul Brown – Mr. Phillips
*Miranda de Pencier – Josie Pye
*Trish Nettleton – Jane Andrews
* Jennifer Inch – Ruby Gillis
* Jayne Eastwood – Mrs. Hammond
*Dawn Greenhalgh – Mrs. Cadbury
*Jack Mather – Station Master
*Samantha Langevin – Mrs. Blewett
*Vivian Reis – Mrs. Spencer
 
* Mag Ruffman – Alice Lawson Sean McCann – Dr. OReilly
*Roxolana Roslak – Madame Selitsky
* Robert Haley – Professor
*Robert Collins – Mr. Barry
*Morgan Chapman – Minne May Barry David Roberts – Tom
*Nancy Beatty – Essie David Hughes – Thomas Lynde
*Wendy Lyon – Prissy Andrews
* Zack Ward – Moody Spurgeon MacPherson
*Anna Ferguson – Punch Woman
*Rex Southgate – Section Head
*Julianna Saxton – Pink Woman
*Molly Thom – Lace Woman
* Jennifer Irwin – Student
*Sandra Scott – Mrs. Harrington
*Peter Sturgess – Porter
*Ray Ireland – Mr. Hammond
*Martha Maloney – Fairview Nurse
* Stuart Hamilton – Mme. Selitskys Accompanist
 

==Sequels and spin-off==
Anne of Green Gables is the first film in a trilogy of movies based on the titular character. In 1987 the films sequel,  , was aired on March 5, 2000 in Canada and on July 23, 2000 in the United States. The final film passed over Annes House of Dreams – the corresponding Anne novel – in favor of a plot not featured in Montgomerys series, and did not receive the same critical praise as the first two films.

The first two Anne films generated a spin-off television series which aired from 1989 to 1996 and starred Sarah Polley. The Road to Avonlea series featured characters and episodes from several of Montgomerys books. Anne herself did not appear in the episodes, but Gilbert Blythe, Marilla Cuthbert, and other characters from the Anne books were included.

In 2008, the fourth in the series, titled   was completed. The film stars Barbara Hershey, Shirley MacLaine, and Rachel Blanchard and it introduces Hannah Endicott Douglas as the new Anne Shirley. The film is both a sequel and a prequel to Sullivans trilogy.

In 2000 – 2001, Sullivan Animation produced   consisting of 26 half hour episodes. The series was developed for PBS and each episode contained an educational and/or moral component. In 2005, Sullivan Animation also produced the feature length animated film Anne: Journey to Green Gables which is an imaginative, whimsical prequel to Sullivans live action Anne of Green Gables film.

 

==Parodies and Spoofs==
A year after the mini-series originally aired, Canadian comedy duo Wayne and Shuster created and starred in a parody entitled Sam of Green Gables, in which a curmodgenly old man named Sam is sent to Green Gables instead of Anne.

==Awards and nominations==
The film swept the 1986 Gemini Awards, winning the following:
*Best Dramatic Miniseries
*Best Actress in a Single Dramatic Program or Miniseries: Megan Follows
*Best Supporting Actor: Richard Farnsworth
*Best Supporting Actress: Colleen Dewhurst
*Best Writing (TV Adaptation): Kevin Sullivan and Joe Wiesenfeld
*Best Music Composition: Hagood Hardy
*Best Costume Design: Martha Mann
*Best Photography: René Ohashi
*Best Production Design/Art Direction: Carol Spier
*Most Popular Program

The film was also nominated for Best Direction in a Dramatic Program or Mini-Series and Best Picture Editing in a Dramatic Program or Series.

The series also won an Emmy Award in 1986, for Outstanding Childrens Program.

Other Awards
* Peabody Award – to Kevin Sullivan for Outstanding Contribution to Broadcasting in the United States, 1986
*Prix Jeunesse: Best Drama, 1988 (Germany)
*TV Guide Award: Most Popular Program, 1986
*Grand Award – International Film and Television, New York
*Emily Award – American Film and Video Festival, 1986
*Macleans Medal of Merit – Macleans Magazine, 1986
*Chris Award – Columbus International Film Festival, 1986
*Silver Hugo Award – Chicago International Film Festival, 1986
*International TV Movie Festival: Nomination for Movie of the Year, 1986
*American TV Critics Award: Best Drama, 1986
*Grant Award: Best TV Program, Houston International Film Festival, 1987
*Golden Gate Award – San Francisco Film Festival, 1986
*CRTA Award: Outstanding Personal Achievement in TV, 1986
*Ohio State Award – Performing Arts and Humanities Award, 1987
*First Prize – Odyssey Institute Media Award, 1987
*The Ruby Slipper: Best Television Special, 1987
*Parents Choice Award – Parents Choice for TV Programmings, 1987
*Excellence in Programming – Award from Association of Catholic Communications in Canada, 1987
*Golden Apple Award – Best of National Educational Film and Video Festival, 1987

==DVD releases==

The Anne of Green Gables series was released on DVD in a collectors edition set on February 5, 2008 in the U.S., April 29, 2008 in Canada and Japan and on September 22, 2010 in Hungary. The set is the most comprehensive edition of all three movies ever released. In addition to the series, it also includes several DVD extras such as feature length commentary from director Kevin Sullivan and Stefan Scaini, 2 New Documentaries: L.M. Montgomerys Island and Kevin Sullivans Classic featuring new cast and crew interviews, missing scenes, lost footage and a condensed, 10-minute version of the missing "Road to Avonlea" episode "Marilla Cuthberts Death".

==Lawsuits== LC Page Co. in Boston that permitted them to publish all of her books for 5 years on the same terms: the main terms were a 10% royalty and world rights to all of the authors books; it also included the right to publish all of her future works. The relationship with Page actually spanned nearly ten years and resulted in the publication of nine novels and collections of short stories. However Montgomery became aggravated with the Page Co. When she contracted with a Canadian publisher (McClelland, Goodchild and Stewart) The Page Co claimed that they had the exclusive rights to her new books and threatened to sue her. Montgomery instead took the Page Co. to court to recover withheld royalties.   

The lawsuit resulted in a settlement in 1919 whereby Page bought out all of Montgomerys rights to all of her novels published by them. The settlement excluded any reversionary rights that might become due for the benefit of either her or her heirs if such rights were to become enacted.       The settlement paid Montgomery a flat sum of $18,000; at the time an amount she would have expected to see earned from her works during her lifetime. 

Sullivan purchased dramatic rights from the Montgomerys heirs in 1984, believing that they owned reversionary rights that had come into place as a result of changes to the copyright act subsequent to Montgomerys death. 

After Sullivans films were successful around the world and brought legions of tourists to Prince Edward Island the Montgomery heirs established an Anne of Green Gables Licensing Authority with the Province of Prince Edward Island to control trademarks to preserve Montgomerys works, through the mechanism of official trademarks.     The heirs and the AGGLA became successful at asserting control over the booming Anne -themed tourist industry that the province enjoyed, because of the lack of clarity about the different protections afforded by copyright, trademark and official marks in Canada. 

AGGLA and the heirs tried to assert control over trademarks Sullivan had established to their various Anne movies (Anne of Green Gables, Anne of Avonlea, Anne of Green Gables – the Continuing Story) and Road to Avonlea properties both in Canada, the US and Japan. 

A Japanese court then determined that the heirs were not entitled to the reversionary rights that they claimed they had sold to Sullivan and that the AGGLA was set up for pursuing private interests and not for serving public interests such as maintaining or managing the value, fame or reputation of the literary work, the author or even the main character of Anne. The Court determined that the AGGLA was the heirs private profit-seeking enterprise as far as its activities were concerned.  
 Anne of Avonlea), plus 10% of the profits of Anne 1 and 5% of the profits of Anne 2. The contract also gave them the right to examine Sullivan Entertainments financial records. However, when Sullivan claimed that neither of the movies had earned a net profit and (the heirs assert) refused to allow them to audit his books , they served a claim against him.       Sullivan argued that the heirs and the AGGLA had enjoined the films by usurping the Sullivan trademarks and drastically reduced the profitability of the ventures.  
The heirs staged a press conference in 1998 at exactly the time when Sullivan was about to close a public offering to take his company public, to force Sullivan to pay them further receipts. The offering however was pulled by the underwriters and Sullivan counter-sued for libel, insisting that the heirs should pay damages of $55&nbsp;million to all parties involved.  
A Superior Court of Ontario judge dismissed his suit on January 19, 2004.   
The Montgomery heirs subsequently dropped their claim for Sullivan to pay them any royalties however a settlement between Sullivan, the Montgomery heirs and the AGGLA was reached in 2006 to deal with all of their outstanding disagreements. 
Although Kevin Sullivans works were initially based upon the works of LM Montgomery, Sullivan developed most of his successful Anne-related film properties (Anne of Avonlea, Anne -the Continuing Story, Anne – A New Beginning and Road to Avonlea)  based on original material, not directly adapted from Montgomerys books.  Many questions have been raised in court as to the author’s heirs’ rights in her copyright. The heirs have tried to extend the copyright in Montgomerys unpublished works until 2017 but lost that opportunity in 2004 when the Canadian Parliament rejected the provision they had pursued so ardently for the unpublished works of dead authors. 
In a Japanese court decision which addressed the heir’s challenge to the validity of Sullivan’s ownership of Japanese trademark’s in the movie property, the Japanese High Court commented on the heirs entitlement to reversionary copyright which formed the basis of the rights that the family claimed to have sold to Sullivan.  The Court stated that the heirs reversionary copyright was non-existent and that there was no need for Sullivan or any other entity to account to the heirs for the use of the trademark in Japan.  

The Court stated: "It is not clear from a legal point of view why permission from the heirs of the author or its related entity the Anne of Green Gables Licensing (AGGLA) authority was necessary." 

The Japanese Court also extensively scrutinized whether the copyright in the book "Anne of Green Gables" had ever devolved to the heirs and called for extensive filing of evidence on this point. Sullivan filed an original 1919 agreement between Montgomery and L.C. Page & Co. which specifically excluded the heirs’ reversionary claims. Montgomery sold all of her publishing and copyright to her series of novels, in perpetuity, to her original American publisher in 1919, to the exclusion of her heirs.  

The Court further questioned whether the heirs licensing authority was engaged in activities of sufficient public interest as to qualify as a controlling body of LMMs works.  The Court stated:  " ....the possibility cannot be denied that the Anne of Green Gables Licensing Authority is the heirs private profit-seeking enterprise as far as the activities with which the heirs of the subject case are involved are concerned. It is not proved from the evidence submitted in the subject case that the Anne of Green Gables Licensing Authority is involved in activities of public interest that are sufficient for the Anne of Green Gables Licensing Authority to be qualified as the owner of the registration of the subject mark as a controlling body of the subject literary work."  

==Trademark and Copyright==

After recent speculation as to who owned the  ". Retrieved 25 July 2013.  and Sullivan Entertainment (the producers of the well-known films and TV series based on Montgomery’s novels). 

The Anne of Green Gables Licensing Authority controls certain exclusive trademarks relating to Anne of Green Gables commercial merchandise and service related to Lucy Maud Montgomerys literary works and any copyright in the LM Montgomery’s books which have not reverted to the public domain. 

Sullivan Entertainment Inc, under agreement with the Anne of Green Gables Licensing Authority, retains all of the dramatic copyright and motion picture copyright in over 125 hours of their original movies, mini-series and television series based on both the Anne and Avonlea series of novels and certain trade-marks relating to Sullivan sourced Anne of Green Gables merchandise and services. Sullivans use of the Anne of Green Gables trademarks extends from motion picture products and books, DVDs, CDs etc. to all commercial merchandise related to Sullivan’s films and television series based on their visual images, costume and production designs, settings, themes and original characters. Sullivan Entertainment also solely controls the commercial trademarks to Anne of Avonlea, Anne of the Island, and Road to Avonlea. 

==Production==
 CBC and PBS in order to film Anne of Green Gables. Sullivan amalgamated many of Montgomery’s episodes into the films plot that diverged from Montgomery’s original, but relied on strong characterizations and visuals in order to render the story for a contemporary filmic audience.

Primary locations for filming the movie included Prince Edward Island, Emmanuel International Canada#Anne of Green Gables|Stouffville, Ontario, Jacksons Point, Ontario, and Westfield Heritage Village in the Hamilton, Ontario neighbourhood of Rockton, over a consecutive ten-week shoot. Sullivan used several locations as Green Gables farm and combined them to appear as one property.

The original film and sequels (with Road to Avonlea and the animated Anne films and series being 130 hours of production) have been seen, thanks to satellite, in every country in the world that broadcasts. The films have now been translated and seen in more places around the world than even the original novels.

An open casting call was held throughout Canada in order to find a young actress to play Anne Shirley. Katharine Hepburn recommended that her niece, Schuyler Grant, play the role of Anne Shirley. Director Kevin Sullivan liked Grant’s performance and wanted to give her the role, however broadcast executives were resistant to casting an American as a Canadian icon. Schuyler Grant ended up playing Anne’s best friend, Diana, and Anne Shirley was ultimately played by Megan Follows.
In her first audition, Megan Follows came highly recommended, but she was quickly dismissed by Kevin Sullivan. For her second audition, after a turbulent morning leading up to her audition, a frantic Megan made a much better impression. 

==References==

 

==External links==
*   This site includes information about Montgomerys actual works (not reinterpreted), her life, and new research in the newsletter, The Shining Scroll.
*  – The official website of Sullivan series of Anne of Green Gables movies
*  – The Official website of Sullivan Entertainment. Includes a wealth of information on the Anne movies and its spinoffs
*  – The official website for Road to Avonlea, the spinoff to the Green Gables series of movies
*  This scholarly site includes a blog, a bibliography of reference materials, and a complete filmography of all adaptations of Montgomery texts. See, in particular, the page for  .
*  – A great resource for all Anne fans with galleries, fan art, timelines, recipes and calendars.
*  – excellent resource on L.M. Montgomery and her legacy in film and television
*  
*  
*   – This site includes information about the centenary anniversary of Lucy Maud Montgomery’s Anne of Green Gables.

 

 
 

 
 
 
 
 
 
 
 