Autumn Spring
{{Infobox film
| name     = Autumn Spring
| image    = 
| director = Vladimír Michálek
| producer = 
| writer = 
| starring = Vlastimil Brodský Stella Zázvorková
| cinematography = Martin Štrba
| music = Michał Lorenc
| country = Czech Republic
| language = Czech
| runtime = 100 minutes
| released =  
}}
Autumn Spring ( ) is a 2001 Czech drama film directed by Vladimír Michálek.

==Cast==
* Vlastimil Brodský - František Hána („Fanda“)
* Stella Zázvorková - Emílie Hánová
* Stanislav Zindulka - Eduard Stará („Eda“)
* Ondřej Vetchý - Jaroslav Hána („Jára“)
* Zita Kabátová - Maruška Grulichová
* Petra Špalková - Králová
* Jiří Lábus - Estate agent

==References==
 

== External links ==
* 

 
 

 