Ashlaa
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Ashlaa (In Pieces)
| image          = 
| caption        = 
| director       = Hakim Belabbes
| producer       = LTF Productions
| writer         = 
| starring       = 
| distributor    = 
| released       = 2010
| runtime        = 90 minutes
| country        = Morocco
| language       = 
| budget         = 
| gross          = 
| screenplay     = Hakim Belabbes
| cinematography = Hakim Belabbes Souad Mellouk Hamid Bellabes Don Smith
| editing        = Hakim Belabbes
| music          = Khalid Oueld El Bouazzaoui Mohamed El Meskaoui Salah El Morseli Cherqaoui
}}
 Moroccan 2010 documentary film.

== Synopsis ==
In Pieces is a posy of images taken over the last ten years that show family moments, reflections on life and death, on disappointments and successes, on aging and exile. This family chronicle becomes the chronicle of a country, of a society, as observed by Hakim Belabbes from inside and outside.

== Awards ==
* Festival de Tánger 2011

== References ==
 

 
 
 
 
 


 
 