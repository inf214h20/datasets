Victoria (2015 film)
 
{{Infobox film
| name           = Victoria
| image          = Victoria (2015 film) POSTER.jpg
| caption        = Film poster
| director       = Sebastian Schipper
| producer       = Jan Dressler Christiane Dressler Sebastian Schipper
| writer         = Olivia Neergaard-Holm Sebastian Schipper Eike Frederik Schulz
| starring       = Frederick Lau Laia Costa Franz Rogowski
| music          = Nils Frahm
| cinematography = Sturla Brandth Grøvlen
| editing        = Olivia Neergaard-Holm
| distributor    = Senator Film
| released       =  
| runtime        = 140 minutes
| country        = Germany
| language       = German English
| budget         = 
}}

Victoria is a 2015 German drama film directed by Sebastian Schipper. It was screened in the main competition section of the 65th Berlin International Film Festival    where it won the Silver Bear for Outstanding Artistic Contribution for Cinematography.    The film stars the German actor Frederick Lau and the Spanish actress Laia Costa.

==Plot==
The story is about the Spanish girl Victoria who, starting in a Berlin techno club and induced by four local guys to drift through the nightly streets, gets roped into the criminal adventure of the four unintended crooks.

==Cast==
* Laia Costa as Victoria
* Frederick Lau as Sonne
* Franz Rogowski as Boxer 
* Max Mauff as Fuss
* Burak Yigit as Blinker
* Nadja Laura Mijthab as Victorias Freundin

==Production==
The film is shot in one single   neighborhood.       The script consisted of twelve pages, with most of the dialogue being improvised.   

==Reception== Variety reviewed the film well, calling it "suffused with a surprising degree of grace and emotional authenticity." 

The films cinematographer Sturla Grovlen won a Silver Bear at the Berlin International Film Festival for extraordinary artistic contribution for their work on the film. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 