Little Tokyo, U.S.A.
{{Infobox film
| name           = Little Tokyo, U.S.A.
| image          = 
| image_size     = 
| caption        = 
| director       = Otto Brower
| producer       = Bryan Foy
| writer         = George Bricker (story)
| narrator       =  Brenda Joyce
| music          = Emil Newman
| cinematography = Joseph MacDonald Harry Reynolds
| studio         = Twentieth Century Fox
| distributor    = Twentieth Century-Fox Film Corp.   
| released       = August 14, 1942
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Little Tokyo, U.S.A. is a 1942 American film, produced during World War II widely considered at its release and now as a piece of anti-Japanese propaganda.

==Plot==
The story, set in late 1941, follows Los Angeles cop Michael Steele (Preston Foster) as he investigates a series of crimes involving the local Japanese American community. 

The story gradually reveals that the crimes are to cover up a Japanese American cabals efforts to facilitate Japans bombing of Pearl Harbor. After the horrific military attack, the Japanese American communitys demonstrations of pro-U.S. patriotism are portrayed as patently insincere. Policeman Steele tracks the crime trail to an American-born spy for Tokyo, Takimura (played in yellowface by Harold Huber). Takimura tries to throw Steele off the case by enlisting a neighborhood vixen, Teru (cast with June Duprez) to seduce him. 

Teru invites Mike to Satsumas house, where she drugs him. As Mike sleeps, Hendricks and Takimura kill Teru and make it look as if Mike murdered her while trying to assault her. Mike is arrested for the murder, and the next morning, is in prison when he learns of the Japanese attack on Pearl Harbor. Mike then escapes from jail and soon discovers where Takimura, Hendricks and the others meet. With Maris help, Mike tricks the spies into revealing their activities while the police listen, and soon the gang is rounded up. After Japanese Americans on the West Coast are taken to internment camps, Little Tokyo becomes a ghost town. 

The movie ends extolling the necessity for the internment with Maris commenting on her radio show that loyal Japanese Americans must suffer along with the disloyal in the interest of national security. She then reads an excerpt of Robert Nathans poem "Watch America," and urges Americans to maintain their vigilance against espionage.

==Controversy==
Filmed in the months immediately following Pearl Harbor, Twentieth Century Foxs Little Tokyo U.S.A. was termed "63 minutes worth of speculation about prewar Japanese espionage activities" by The New York Times. 
 documentary style Chinatown in Los Angeles instead.

In retrospect, knowing that not a single charge of espionage was ever brought against a Japanese American during wartime.


==Cast==
*Preston Foster as Michael Steel
*Harold Huber as Ito Takamura
*June Duprez as Teru
*Abner Biberman as Satsuma Brenda Joyce as Maris Hanover
*Don Douglas as Hendricks
*George E. Stone as Kingoro
*Charles Tannen as Marsten
*Frank Orth as Jerry
*Edward Soo Hoo as Suma
*Beal Wong as Shadow

==See also==
*Racism in Film of the United States
*Yellow Peril
*Japanese American internment
*United States Office of War Information

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 