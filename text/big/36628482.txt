Return (1985 film)
{{Infobox film
| name           = Return
| image          = Return1985Cover.jpg
| image_size     = 240px
| caption        = Video cover Andrew Silver
| executive producer = 
| producer       = Samuel Benedict (ep)  Yong-Hee Silver Philip J. Spinelli Donald Harington
| narrator       = 
| starring       = Karlene Crockett John Walcutt Anne Francis Frederic Forrest
| music          = Regnar Grippe Michael Shrieve
| cinematography = János Zsombolyai
| editing        = Gabrielle Gilbert Reeves 
| studio         = Silver Productions
| distributor    = Silver Productions
| released       =  
| runtime        = 82 minutes (US) 78 min. (DE)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1985 independent independent mystery Andrew Silver.  It was Silvers debut theatrical work. 

==Synopsis==
Diana Stoving, after experiencing a premonition, becomes convinced that her mother withheld the truth about her family history. Diana travels to Massachusetts to investigate. She coincidentally meets a woman studying past-life regression; one of her subjects is Day Whittaker, a young man who seems to be possessed by the spirit of Dianas deceased grandfather. Under regression, Day claims he died under suspicious circumstances.

==Cast ==
*Karlene Crockett as Diana Stoving
*John Walcutt as Day Whittaker
*Anne Francis as Eileen Sedgeley
*Frederic Forrest as Brian Stoving Lee Stetson as Daniel Montcross
*Barbara Kerwin as Ellen Fullerton
*Lisa Richards as Ann Stoving
*Hanna Landy as Elizabeth Holt
*Ariel Aberg-Riger as Diana (age 3)
*Thomas Cross Rolapp as Lucky the mechanic
*Lenore Zann as Susan
*Dennis Hoerter as Mechanics assistant

==Production== Donald Harington.  The film was shot in Los Angeles and Massachusetts,    and was released in theaters January 24, 1986.  It was released on VHS in 1988, with an "R" rating.  . Videohounds MovieRetriever.com. 

==Reception==  
Reviews varied from mildly to strongly negative. Vincent Canby of The New York Times found it to be a "technically competent but completely witless mystery movie about possession and reincarnation." He continued that the film is "always in focus, full of picturesque landscapes, devoid of character and even of suspense."     Boston Globe reviewer Michael Blowen wrote "Fans of Shirley MacLaines New Age fascination with past lives should find Return, a locally produced feature film, entertaining ... This independent film has first-rate production values and excellent performances, but it lacks the necessary tension and suspense to propel it into the rank of first-rate occult movies."         

Variety (magazine)|Variety noted that the subtitle ("A case of passion") "gave away" the romantic subplot, and the ending.   The film received a cool reception at eFilmCritic; Charles Tatum described it as generating "a complete feeling of indifference", spending too little time on either "character development or suspense." He described the "twist" ending as obvious.   Eleanor Mannikka (Rovi/AllMovie) wrote that "the theme veers from possession to suspense thriller, slowing up in the process...", giving the film two stars (of five). 

===Awards===
John Walcutt won Best Actor (Caixa de Catalunya) at Sitges:Catalonian International Film Festival.  

==References==
 

==External links==
*  

 
 
 
 
 
 