Ramudochadu
{{Infobox film
| name           = Ramudochadu
| image          =
| caption        =
| story          = Thotapalli Madhu
| screenplay     = A. Kodanadarami Reddy
| writer         = Thotapalli Madhu  
| producer       = Yarlagadda Surendra
| director       = A. Kodandarami Reddy Krishna Soundarya Ravali Raj
| cinematography = Rasool Ellore
| editing        = Shankar Suri
| studio         = S. S. Creations
| released       =   
| runtime        = 147 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
Ramudochadu is a  1996 Telugu film|Telugu, drama film produced by Yarlagadda Surendra on S. S. Creations banner directed by A. Kodandarami Reddy Starring Akkineni Nagarjuna, Krishna (actor)|Krishna, Soundarya, Ravali in lead roles and music is composed by Raj-Koti|Raj. The film recorded Average at box-office.   

==Plot==
Rivalry between two families splits a village Seetaramapuram into Seetapuram and Ramapuram. Seetapuram head Bhusayya (Nambiar) and Ramapuram head Bhadrayya (Kaikala Satyanarayana). Bhusayya is in trap of his son-in-law Gavaraju (Anand Raj) who always insists to take revenge against Bhadrayyas family. Bhadrayya wants to study his grand daughter Sundaralakshmi (Soundarya) in college but her father Chandrayah (Chandra Mohan (actor)|Chandramohan) fixes her marriage with their Village President Nagaraju (Sri Hari), but with his fathers pressure Chandrayah sends Sundaralakshmi to college.

Ram (Akkineni Nagarjuna) is a college student from America who one day meets Sundaralakshmi in college, as she isnt used to city life Ram teaches her how to live like one. Afterwards he goes with Sundaralakshmi to Seetapuram to spend his holidays and meets her family. But Chandrayah doesnt appreciate Rams foreign behavior. Ram beats up one of the goons from Ramapuram as they start another fight. But Ram stops them luckily and admits that hes the one who beat the guy up, so they stopped the fight. Ram meets a village girl named Seshagiri (Ravali) and she falls in love with him, with help of her Ram tries Sundaralakshmi to fall in his love. Chandrayah makes Sundaralakshmi engagement arrangements with Nagaraju where she admits that her love with Ram. But Chandrayah doesnt agree, he keeps a challenge to Ram that he should cultivate the agriculture land of 100 acres. Ram accepts the challenge and wins it.

Meanwhile Ram comes to know that Nagaraju secretly works for Gavaraju, they plan their next attack at their rival village. Ram then comes in and starts a fight against both villages in which Bhusayyas wife Annapurnamma (Jayanthi) is injured, Ram saves her and tells her that he is her grandson, Gavaraju listens to that and kills Annapurnamma and proclaims that if any heir is their in village to give funeral to her, then Ram open up secret.

Krishna Prasad (Krishna) son of Bhusayya treated as god by entire village because he doesnt have any disparity of rich and poor. He is loves and marries Bhadrayyas daughter Arundathi (Suhasini) without their permission for that reason his father sends him out. After some time Arundhati gets pregnant, Krishna Prasad is willing to send his wife to America for higher education. Gavaraju plans to kill Krishna Prasad and Arundathi in the attack Krishna Prasad dies saving Arundathi but people thinks both of them are died which splits the village into two and temple between the village is closed.

Ram is son of Krishna Prasad and Arundathi thats why he played this entire game to join both villages as his mothers last wish. Meanwhile Bhusayya also owns the truth and he changes. Finally Ram sees end of Gavaraju, joins both villages into Seetaramapuram and temple is re-opened with Ram & Sundaralakshmis marriage.

==Cast==
 
*Akkineni Nagarjuna as Ram Krishna as Krishna Prasad
*Soundarya as Sundarlakshmi
*Ravali as Seshagiri Satyanarayana as Bhadrayya
*Sri Hari as Nagaraju Nambiar as Bhusayya Anand Raj as Gavaraju Chandramohan as Chandrayah
*Chalapathi Rao as Servant 
*Prasad Babu as Madhavayya Ali as Basha AVS as Principal
*Babu Mohan as Bengal Mallikarjuna Rao as Chandamama Suhasini as Arundathi
*Sudha as Venkai Jayanthi as Annapurnamma
*Krishnaveni as Warden
*Poojitha as Nagma
*Kashmira Shah as special appearance in item song
*Y.Vijaya as Bhadrayyas Wife
*Master Harsha as Ankappa
 

==Soundtrack==
{{Infobox album
| Name        = Ramudochadu
| Tagline     = 
| Type        = film Raj
| Cover       = 
| Released    = 1996
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:48
| Label       = Supreme Music Raj
| Reviews     =
| Last album  = Bharatha Simham   (1995) 
| This album  = Ramudochadu   (1996)
| Next album  = Sambhavam   (1998)
}}
The music was composed by Raj-Koti|Raj. All songs are hit tracks. Music released on SUPREME Music Company. 
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:48
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Aishwarya Raiyo Sirivennela Sitarama Sastry  SP Balu,Suresh Peters
| length1 = 4:48

| title2  = Varava Vayarama
| lyrics2 = Veturi Sundararama Murthy SP Balu,K. Chitra
| length2 = 5:04

| title3  = Guvve Kuse
| lyrics3 = Veturi Sundararama Murthy SP Balu,K. Chitra 
| length3 = 5:14

| title4  = Maa Palle
| lyrics4 = Veturi Sundararama Murthy SP Balu,K. Chitra 
| length4 = 4:53

| title5  = Srungara Kavya  Sirivennela Sitarama Sastry  SP Balu,Swarnalatha
| length5 = 3:31

| title6  = Gumma Gumma 
| lyrics6 = Veturi Sundararama Murthy  SP Balu,K. Chitra 
| length6 = 5:18
}}

==References==
 

 
 
 
 