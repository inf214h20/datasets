Picture Bride (film)
{{Infobox film
 | name = Picture Bride
 | image = Picture Bride VideoCover.jpeg
 | image_size = 
 | caption = 
 | director = Kayo Hatta
 | producer = Diane Mark and Lisa Onodera
 | writer = Kayo Hatta Mari Hatta
 | narrator = 
 | starring = Youki Kudoh  Tamlyn Tomita
 | music = Mark Adler
 | cinematography = Claudio Rocha
 | editing = Lynzee Klingman
 | distributor = Miramax
 | released = 1995
 | runtime = 95 minutes
 | country = United States
 | language = English Japanese
 | budget = 
 | gross  = $1,238,905 
 | preceded_by = 
 | followed_by = 
}}
Picture Bride is an American Japanese language 1995 feature-length independent film directed by Kayo Hatta from a screenplay she co-wrote with Mari Hatta, and co-produced by Diane Mei Lin Mark and Lisa Onodera. It follows Riyo, who arrives in Hawaii as a "picture bride" for a man she has never met before. The story is based on the historical practice, due to U.S. anti-miscegenation laws, of (mostly) Japanese and Korean immigrant laborers in the United States using long-distance matchmakers in their homelands to find wives.

Released by Miramax Films, the film stars Youki Kudoh, Akira Takayama, Tamlyn Tomita, and Cary-Hiroyuki Tagawa, with a special appearance by Toshiro Mifune in his penultimate film role. Picture Bride premiered at the 1995 Sundance Film Festival, where it won the Audience Award for narrative feature film. Considered a landmark Asian American work, the film was an Official Selection at the 1994 Cannes Film Festival    in the Un Certain Regard section and received an Independent Spirit Award nomination for Best First Feature (for director Hatta). In 2004, Miramax released a  , which includes "The Picture Bride Journey," a documentary on the making of the film featuring the director, cast members, archival historical footage, and behind-the-scenes clips from the movie set.

==Plot==
The film is set in 1918.  Riyo (Kudoh) is a "city girl", who becomes a picture bride to a man who works as a field hand on a sugar cane plantation in Hawaii.  The film begins with the death of Riyos father, which leads Riyos aunt to make arrangements for Riyo to become a picture bride. As Riyo prepares to be photographed, her aunt shows her a picture of the handsome Matsuji (Takayama), her husband-to-be in Hawaii.  Not only is the photo intended as introduction, but also as a means of confirming that each has found the right partner when they meet for the first time on the docks. However, when Riyo finally arrives in Honolulu, the man who comes to greet her looks nothing like the man in the photo. Matsuji confesses that the photo he sent was old, taken when he was a young man.

Riyo goes through with the wedding (in a mass ceremony with numerous other "picture couples"), and travels out to the sugar plantation which is her new home. As she walks past the darkened fields to their ramshackle house, Riyo hears a faint sound on the wind of a woman singing. When she asks about it, Matsuji half-jokes that the ghosts of the canefields have come to welcome her.  That night, they sleep on the same mat, but Riyo fights off his attempts at sexual intimacy and hides beneath a blanket. 

The next day, after she is given an ID tag to wear around her neck, Riyo goes to work in the sugar cane fields for the first time. As a city girl, she is unfamiliar with farm work and slows the other workers down,leading her to be harassed and ridiculed by Antone (James Grant Benton) the Luna (field supervisor).  Yayoi (Kati Kuroda) prompts Kana (Tomita) to help Riyo learn. Kana is a young picture bride who had arrived several years before. Later, Riyo finds out that Kanas husband, Kanzaki (Tagawa) frequently beats her and goes out gambling. To escape the abuse, at night Kana often brings her baby and sleeps in the fields, singing.  One night, after an argument with Matsuji, Riyo runs off into the fields and discovers Kana.  Riyo gains Kanas trust, and agrees to help the other woman with her side business doing laundry for the workers.

Riyo begins industriously doing laundry work and saving all her earnings in a tin can, determined to earn her passage back to Japan. Matsuji begins drinking and gambling, saying that he intends to win enough money to get another bride.  Kana advises Matsuji to become romantic, and to take Rudolf Valentino as a role model in the task of winning Riyos heart.

Antone the luna starts to drive the workers to do more work in order to meet the harvest in time.  The womens leader, Yayoi, leaves with her family to Honolulu. As she leaves, she asks Kana to take care of the other women caneworkers.

In the fields, Kana and the other mothers are constantly worried about their children, who must be left, under a sunshade in the field when the parents are working. Kana tries to stand up against Antone to get the kids moved closer when the workers go to a new field. He threatens to use his whip on the workers, as he had done in the old days.

Preparatory to harvesting, cane fields are set on fire to burn off the leaves. Antone, in a hurry to get the job done, orders the fire to be set before the women can retrieve their children.  In the meantime, Kanas baby daughter, Kei, has wandered into the fields. Kana rushes into the burning field to find her daughter, and both are lost.

The workers talk about waging a strike.  Meanwhile, Riyo continues to hear the sound of a woman singing in the canefields. One night, she leaves the house to follow the sound, and Matsuji follows her. He accuses her of having a secret lover and a chase ensues. Then, Riyo admits that her parents had died of tuberculosis, which at that time carried tremendous social stigma. Although Riyo herself was healthy, the matchmaker had lied to Matsuji about his bride-to-bes outcast status. That night, when Riyo tentatively reaches out to him as they lie in bed, he brusquely turns away.

Betrayed, the next day Riyo gathers her savings and a few belongings and runs away, eventually finding herself at the ocean. Riyo falls asleep there. She is awakened by the sound of singing, and glimpses a woman walking among the shoreline rocks. She runs after the vision, and encounters Kana, who says she is leaving for Japan. When Riyo asks to go with her, Kana criticizes her, not unkindly:  "Who waiting for you there?"  Then, as Yayoi had done when she left the plantation, Kana hands her neck tag to Riyo and tells her "Take care of the girls."  Kana turns to walk toward the sea, fading out of sight.

Riyo awakes from her sleep, and finds Kanas neck tag in her hand.  She returns to the plantation and to her house, where she finds Matsuji drunk.  She puts him to bed, and he looks at her, saying "I thought I was all alone again."  Later that night, she hesitantly reaches out to touch his hand, and he reaches back; they embrace.

The next day, Riyo surprises the other workers when she begins singing in the fields, as had Kana and Yayoi. Antone tries to make fun of her, but she continues singing more strongly, and he realizes that she has picked up the mantle of leadership. Matsuji makes a gift to Riyo of a Buddhist altar, so she can honor her parents. She is touched at his acceptance of her past.

The film ends as Riyo, Matsuji, and the other workers dance in a circle at a lively Obon Festival. In a closing voiceover (by Nobu McCarthy), an older Riyo describes how she still imagines at times that she hears a womans voice singing, but then realizes it is the voice of Riyos own daughter as she sings to her children.

==Cast==
* Youki Kudoh - Riyo
* Cary-Hiroyuki Tagawa - Kanzaki
* Tamlyn Tomita - Kana
* Akira Takayama - Matsuji
* Yôko Sugi - Aunt Sode
* Christianne Mays - Miss Pieper
* Toshiro Mifune - The Benshi
* Jason Scott Lee

==Critical reception==
Picture Bride was met with Critical acclaim. The film has a score of 82% with a certified "Fresh" rating on the review aggregate website Rotten Tomatoes based on 19 reviews. 
 My Famiy" is another, about Mexican-Americans. Of course, those early generations suffered much, but somehow the films are suffused with a certain serenity, because after all, the stories had a happy ending: They produced the children and grandchildren who are telling the tales." 

Lisa Schwarzbaum of Entertainment Weekly gave the film a B+, saying the film is "a lyrical, elegantly composed drama". 

Peter Stack from the San Francisco Chronicle gave a positive review saying Picture Bride is an "exceptionally lovely first feature film by Kayo Hatta and ended by writing "Picture Bride captures with disturbing simplicity the lonely terror affecting one such bride."  

Alison Macor of the Austin Chronicle in her review said "Picture Bride deservedly won this years Audience Award at the Sundance Film Festival" as well as saying "director Hattas first feature skillfully blends humor with the day-to-day drama of living in a land that is not ones own" and ended by saying "although this is a small film in that it profiles an individuals drama rather than the human condition, Picture Bride does so with tremendous warmth and respect for its characters." 

==Cameos==
*Toshiro Mifune plays a short part as a Benshi, a Japanese performer who acts as a narrator for silent films.

*Actor Jason Scott Lee plays an uncredited cameo role as a Japanese laborer during a scene when the workers collect their pay and an inter-ethnic fight nearly erupts.

*A number of renowned Hawaii-based performers appeared in cameo roles, including the late   and taiko artist  .

*Yoko Sugi, one of Japans leading post-war actresses (starring in films by Kon Ichikawa and Mikio Naruse, among others), appears briefly early in the film, as Riyos aunt.

==References==
 

==External links==
* 

 
 
 
 
 
 
 