Dead Snow
 
{{Infobox film
| name           = Dead Snow
| image          = Dodsno.jpg
| image_size     = 200px
| alt            = 
| caption        = US DVD cover
| director       = Tommy Wirkola
| producer       = Tomas Evjen Harald Zwart
| writer         = Tommy Wirkola Stig Frode Henriksen
| starring       = Vegar Hoel Stig Frode Henriksen Charlotte Frogner Lasse Valdal Evy Kasseth Røsten Jeppe Laursen Jenny Skavlan Ane Dahl Torp Bjørn Sundquist Ørjan Gamst
| music          = Christian Wibe
| cinematography = Matthew Bradley Weston
| editing        = Martin Stoltz Zwart Arbeid Barentsfilm AS FilmCamp Storm Studios
| distributor    = Euforia Film
| released       =  
| runtime        = 91 minutes 
| country        = Norway
| language       = Norwegian
| budget         = $800,000
| gross          = $1,984,662
}} English Dubbing dub of this film for the home media release.

==Plot== zombies in SS uniforms.

Seven students on Easter vacation arrive at a small cabin near Øksfjord. The cabin is owned by Sara, Vegards girlfriend. The group begins to drink and party until a mysterious hiker arrives. He tells them the dark history of the region; during World War II, a force of Einsatzgruppe, led by Standartenführer (Oberst) Herzog|Standartenführer Herzog, occupied the area. For three years, the Nazis abused and tortured the local people. Near the end of the war, with Germanys defeat looming, the soldiers looted all the towns valuables. However, the citizens staged an uprising and ambushed them, killing many. The survivors, including Herzog, were chased into the mountains, and it was assumed that they all froze to death. The hiker then continues on his way. That night, Vegard wakes to a figure placing something beneath the floorboards of the cabin. He calls out, believing it to be Sara, but she ignores him and leaves. Vegard follows, and finds her outside covered in blood. Vegard suddenly jolts awake in his bed, revealing it was a dream. Meanwhile, the hiker has set up camp in the mountains and is eating dinner when he is disturbed by a noise outside. He investigates, and is attacked and killed by a zombie.

The next morning, Vegard, out looking for Sara, discovers the hikers body. He searches the area, falls through the snow into a cave, and is knocked unconscious. After sunset, Erlend finds an old wooden box filled with valuables and golden trinkets. They celebrate, and one of them pockets a gold coin. They eventually return the rest of the treasure to the box. Erlend goes to the outhouse where he and Chris have sex. Afterwards, Erlend return to the cabin, and drops a gold coin. Chris is attacked by a zombie, and killed. The others leave the cabin to look for her, and find Saras rucksack buried in the snow.

Upon returning to the cabin, they are attacked. Erlend is killed in an attempt to defend the cabin, and the others secure the building. Vegard comes around in the cave, discovering German firearms and helmets, as well as Saras severed head. He is attacked, but escapes to the surface, where he is confronted by a zombie. Vegard stabs the zombie in the eye, but is knocked from the cliff side by a second assailant. Vegard is bitten in the neck by the zombie, whilst the two hang from the cliff using an intestine as rope. He climbs back to the snowmobile, stitches his wounds, and mounts a machine gun to his snowmobile.

Meanwhile, the remaining four students decide to split up. The two men, Martin and Roy, attempt to distract the zombies, while the two women, Hanna and Liv, run for the cars and go for help. En route to the cars, the girls are ambushed. Liv is knocked out by the zombies and awakens to them pulling out her intestines. Using a hand grenade, she commits suicide and kills her assailants. Hanna leads a zombie to a cliff edge, breaks the ice, and they fall. The pair survive, and Hanna kills the zombie.

Martin and Roy accidentally set fire to the cabin with Molotov cocktails. They escape, and arm themselves with power tools. More zombies attack, but they are aided by Vegard. During the attack Vegard is killed and Martin accidentally kills Hanna, who has returned to the cabin. Herzog arrives, leading a group of zombies. They attack, and Martin is bitten on the arm. To avoid becoming infected, he cuts off his arm with a chainsaw. After killing the remaining undead, Martin and Roy attack Herzog, who calls upon hundreds of zombies, that rise from under the snow. Whilst running from their attackers, Roy is hit in the head by a hammer, disemboweled by a tree branch, and killed by Herzog, who retrieves a watch from his pocket.

Martin realises the zombies intent, and retrieves the box from the ruined cabin. He returns the box to Herzog, and escapes to the car. There, he finds a gold coin in his pocket, just as Herzog smashes the window of the car.

==Cast==
*Vegar Hoel as Martin
*Stig Frode Henriksen as Roy
*Charlotte Frogner as Hanna
*Lasse Valdal as Vegard
*Evy Kasseth Røsten as Liv
*Jeppe Laursen as Erlend
*  as Chris
*Ane Dahl Torp as Sara
*Bjørn Sundquist as The Wanderer
*Ørjan Gamst as Standartenführer (Oberst) Herzog|Standartenführer Herzog

==Production== cameo as one of the zombie soldiers, said of his monsters, "I like to think of them as Nazi zombies. Nazis first, then zombies." 

Even though a   mixed with Indiana Jones."  
Retrieved 20 Nov. 2010 
 Alta and Målselv.

==Release==
The film was distributed by Euforia Film  and released on 9 January 2009 in Norway.    The U.S. premiere was held at the Sundance Film Festival,    after which IFC Films purchased the U.S. distribution rights.    The film received a limited release in the U.S. starting 19 June 2009,  before its DVD release on 23 February 2010.  The film currently holds a 66% "fresh" rating on Rotten Tomatoes.

==Reception==
Dead Snow received mixed reviews from Norwegian critics, and was rated 3/6 by both Verdens Gang    and Dagbladet.    According to Manohla Dargis of The New York Times, director Tommy Wirkola, "who wrote the irrelevant screenplay with Stig Frode Henriksen, doesn’t just hit every horror beat; he pounds it to an indistinguishable pulp."  She highlighted the special effects team for acclaim for their "admirably disgusting" work.    Stephen Witty of The Star-Ledger also commented positively on the films visuals, and lauded Wirkola for his "steady hand with the action scenes",    but identified the plot as the films weakness. He accused the characters of lacking motivation, and for being "pretty much indistinguishable from one another." He also had a problem with the ending. 

On   gives the film a 61/100 rating, indicating "generally positive reviews". 

The film was nominated for four 2009 Scream Awards: Fight-to-the-Death Scene, Most Memorable Mutilation, Best Foreign Movie and Best Horror Movie.

==Sequel==
In 2012, following the release of his first big-studio film,  , Wirkola announced the  .  It had its Norwegian première in February 2014.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 