Virasat (1997 film)
{{Infobox film
| name = Virasat
| image = virasat.jpg
| image size =
| caption = DVD cover
| director = Priyadarshan
| producer = Mushir-Riaz
| screenplay = Vinay Shukla
| story = Kamal Haasan 
| based on =  
| narrator = Tabu Pooja Batra Milind Gunaji Govind Namdeo
| music = Anu Malik  (Songs)   S. P. Venkatesh  (Background Score) 
| cinematography = Ravi K. Chandran
| editing = N. Gopalakrishnan 
| distributor = M. R. Productions Pvt Ltd
| released = 30 May 1997  (India) 
| runtime = 168 minutes
| country = India
| language = Hindi 
| budget =
| preceded by =
| followed by =
}}

Virasat (  Inheritance) is a 1997 Indian Hindi film, a remake of the Tamil film Thevar Magan. It was directed by Priyadarshan and produced by the Mushir-Riaz duo. It starred Anil Kapoor, Tabu (actress)|Tabu, Amrish Puri, Pooja Batra, Milind Gunaji and Govind Namdeo. The music was composed by Anu Malik. The film marked a comeback vehicle for Priyadarshan in Hindi cinema.  Music director Annu Malik reused some of the original tunes of Ilaiyaraaja from Thevar Magan.

==Synopsis==
After completing his studies in London, Shakti Thakur (Anil Kapoor) returns to the small Indian town where his family resides. Accompanying him is his girlfriend, Anita (Pooja Batra), whom he is in love with and wants to marry, much to the disapproval of his family. After a few days, Shakti starts to feel that nothing much has changed in this part of India and longs to leave. He tells his father that he would like to sell his share in the family land and would open Restaurant business and settle with his girlfriend, to this his Father(Amrish Puri) urges him to stay in the village and uplift it with his education, by saying "A man gets Education not to become a selfish being but to uplift his uneducated brothers and society". Shakti does not agree with his father and decides to leave. He is unable to bear the animosity between the townspeople in general and in particular between his Zamindar dad, Raja Thakur (Amrish Puri), and rival Zamindar Birju (Govind Namdeo), none other than Rajas crippled brother. 

When Anita leaves town to meet her parents, Shakti wants to follow her; his plans change suddenly when his father passes away, and the region is ravaged by unprecedented natural calamities. At this moment Shakti realizes that his village needs him more than anyone else, He realizes that with his modern education he can uplift his backward village. During one of his attempt to solve the villages problem he meets village belle, Gehna (Tabu (actress)|Tabu).  By a twist of destiny, Shakti has to marry Gehna in order to save her from disgrace. Anita returns to find that nothing is the same anymore. She finds that, Shakti has turned from her lover boy to a mature and responsible family man, for whome his villages upliftment is everything. She returns home.

The film strongly depicts the true meaning of Education "A tool to uplift uneducated people and use education for social welfare".

==Cast==
* Anil Kapoor as Shakti Thakur
* Amrish Puri as Raja Thakur
* Pooja Batra as Anita Tabu as Gehna
* Milind Gunaji as Bali Thakur
* Govind Namdeo as Birju Thakur
* Satyen Kappu as Narayandas
* Dilip Dhawan as Shaktis brother
* Neeraj Vora as Sukhiya
* Sulabha Deshpande as Balis mother
* Rita Bhaduri as Mausi

==Awards==
 National Award=== Best Female Playback Singer - K. S. Chithra - (Song: "Payalein Chun Mun")

===Filmfare Awards===
Virasat was nominated for nine categories at the Filmfare awards and won six of them. The awards it won are in bold.
 Critics Award Best Movie Critics Award Tabu
* Best Supporting Actor - Amrish Puri Best Cinematography - Ravi K. Chandran Best Choreography - Farah Khan Best Story - Kamal Hassan Best Supporting Actress - Pooja Batra Best Actress Tabu
* Filmfare - Best Female Playback Singer - Kavita Krishnamurthy for "Dhol Bajne Laga"
* Filmfare - Best Female Playback Singer - K.S Chithra for "Payalein Chun Mun"

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Payalay Chunmun"
| Kumar Sanu, K. S. Chithra
| 06:50
|-
| 2
| "Tare Hain Barati"
| Kumar Sanu, Jaspinder Narula
| 06:33
|- 
| 3
| "Jayengi Pee Ke"
| Abhijeet Bhattacharya|Abhijeet, Anuradha Sriram, Anu Sriram
| 06:31
|-
| 4
| "Ek Tha Raja" Hariharan
| 03:52
|-
| 5
| "Sun Mausa Sun"
| Vinod Rathod
| 06:45
|- 
| 6
| "Dhol Bajne Laga"
| Udit Narayan, Kavita Krishnamurthy
| 07:00
|- 
| 7
| "Payalay Chunmun (Female)"
| K. S. Chithra
| 02:16
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 