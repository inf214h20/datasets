The Nightcomers
 
 
 
{{Infobox film
| name = The Nightcomers
| image = The Nightcomers VideoCover.jpeg
| image_size =
| caption = original poster
| director = Michael Winner
| producer = Elliott Kastner Jay Kanter Alan Ladd, Jr. Michael Winner Michael Hastings
| narrator =
| starring = Marlon Brando Stephanie Beacham Thora Hird Harry Andrews
| music = Jerry Fielding
| cinematography = Robert Paynter
| editing =
| studio = AVCO Embassy Pictures
| released = 1971
| runtime = 94 Mins
| country = United Kingdom
| language = English
| budget =
| gross =
}}
 The Innocents. The manor house in the film is Sawston Hall, a 16th-century Tudor manor house in Sawston, Cambridgeshire.

== Plot ==

Recently orphaned, Flora and Miles are abandoned by their new guardian (Harry Andrews) and entrusted to the care of housekeeper Mrs. Grose (Thora Hird), governess Miss Jessel (Stephanie Beacham), and Peter Quint (Brando), the former valet and now gardener. With only these three adults for company, the children live an isolated life in the sprawling country manor estate. The children are particularly fascinated by Peter Quint due to his eclectic knowledge and engaging stories, and willingness to entertain them. With this captive audience, Quint doses out his strange philosophies on love and death. The governess, Miss Jessel, also falls under Peters spell, and despite her repulsion the two embark on a sadomasochistic love affair. Flora and Miles become fascinated with this relationship, and help Quint and Jessel to escape the interference of disapproving Mrs. Grose. The children begin spying on Quint and Jessels violent trysts and mimick what they see, including the bondage, culminating in Miles nearly pushing Flora off a building to her death. Mrs. Grose determines to write to the absent Master of the House in order to get both Quint and Jessel fired. The children are most distressed by this, and decide to take matters into their own hands to prevent the separation. Acting on Quints assertions that love is hate and it is only in death that people can truly be united, the children murder Miss Jessel by knocking a hole in the boat she uses to wait for Quint (who never keeps the appointments), knowing that she cannot swim. Quint later finds Miss Jessels rigid body in the water, but is given little time to mourn before Miles kills him with a bow and arrow. The film ends with the arrival of a new governess, presumably the one who features in The Turn of the Screw.

== Cast ==

* Marlon Brando – Peter Quint
* Stephanie Beacham – Miss Jessel
* Thora Hird – Mrs. Grose
* Harry Andrews – Master of the House
* Verna Harvey – Flora
* Christopher Ellis – Miles
* Anna Palk – New Governess

== Differences from the book ==

The children in the film are portrayed as being a few years older than in the Henry James novel, probably due to the sexual nature of the film and their roles in it (Verna Harvey was in fact 19 at the time). 

== Reception and awards ==

The film has received positive reviews. Brandos performance earned him a nomination for a Best Actor BAFTA, but recent audiences have criticised his cartoonish Irish accent.  The film has an 75% fresh rating at Rotten Tomatoes.  Some reviewers have objected to the films premise of showing what happened before the novel, as this threatens the ambiguity the novel explores.  Tom Milne took a very negative view of The Nightcomers, describing it as "a film crass enough to have the outraged ghost of Henry James haunting Wardour Street". Milne also criticised Hastings script, stating his dialogue "sounds embarrassingly like a Cockney nanny doing her best to be genteel". 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 