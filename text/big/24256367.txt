Kakkothikkavile Appooppan Thaadikal
{{Infobox film
| name           = Kakkothikkavile Appooppan Thaadikal (1988)
| image          = KakkothikkavileAppooppanThaadikal.jpg
| image size     = 
| alt            = 
| caption        = Promotional Poster designed by P. N. Menon (director)|P. N. Menon Kamal
| producer       = A.M.Fazil, Joseph Valakuzhy (Ouseppachan) for Rishivarya Pictures
| writer         = 
| screenplay     = Fazil
| story          = Madhu Muttam
| based on       =  
| narrator       =  Ambika V. Krishnan Kutty Nair, Surasu, N.LBalakrishnanan, Philomina etc
| music          = Ouseppachan Bichu Thirumala (lyrics)
| cinematography = Vipin Mohan
| editing        = T. R. Sekhar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Ambika play the lead roles. The film had musical score by Ouseppachan.   

==Plot==
The film is about the story of two sisters. One day, a beggar (V. K. Sreeraman) comes to their house and asks for some water. Leaving the young sister (Raasi (actress)|Raasi) in the courtyard, the elder sister (Kaveri (actress)|Kaveri) goes to fetch water. When she returns, she finds that both her sister and the beggar are missing.

The film then progresses through the life of a school boy Murali who is scoffed at in his school because of his poor background. One day, Murali skips his studies and runs away with some gypsies. He makes good friendship with the gypsy girl named Kakkothi (Revathi). In the meantime, Muralis school teacher (Ambika (actress)|Ambika) enquires about Muralis background and recognises his pathetic condition.

One day, the school teacher finds him and persuades him to stay at her own house and study. Later, Kakkothi comes across Murali and insists that he should stay with her. Murali convinces her of the good qualities of his school teacher and takes her to the teachers house. On seeing the teachers house, Kakkothi reminisces her past. She finds her own childhood photos in the house. She was in fact the girl kidnapped by the beggar years ago. She then runs away from the house. The beggar then reappears. The remainder of the film deals with the happenings in Kakkothis life.

The film is picturised in rural locations such as Pandalam and Venmony. The film includes many funny sequences in school and gypsy life. The film was sleeper hit in the box office. Revathi won Filmfare Award for the Best Actress from this movie. 

==Cast==
* Revathi
* Kiran Vergis Ambika
* V. K. Sreeraman Krishnan Kutty Nair
* M. S. Thripunithura
* Surasu
* Philomina Kaveri
* Raasi

==Soundtrack==
The music was composed by Ouseppachan and lyrics was written by Bichu Thirumala. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kaakkothiyammaykku || Janamma David, Malaysia Vasudevan, SP Shailaja || Bichu Thirumala || 
|- 
| 2 || Kannaam Thumpee || KS Chithra || Bichu Thirumala || 
|- 
| 3 || Kannaam Thumpee (Sad) || KS Chithra || Bichu Thirumala || 
|- 
| 4 || Nannangaadikal || KS Chithra, Malaysia Vasudevan, SP Shailaja || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 

 
 
 


 