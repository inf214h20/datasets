The Ticket of Leave Man
{{Infobox film
| name           = The Ticket of Leave Man
| image          =
| image_size     =
| caption        = George King
| producer       =
| writer         = H.F. Maltby (writer) A.R. Rawlinson (writer)
| narrator       =
| starring       = See below
| music          = Jack Beaver
| cinematography = Hone Glendinning
| editing        = Robert Walters 
| distributor    =
| released       =
| runtime        = 71 minutes
| country        = UK
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 British thriller George King and starring Tod Slaughter, John Warwick and Marjorie Taylor,  based on The Ticket-of-Leave Man, an 1863 melodrama by Tom Taylor which introduced the character Hawkshaw the Detective. It takes its name from the Ticket of leave which was issued to convicts when they were released.

== Plot summary ==
 
A man is wrongly accused of a series of killings, leaving him to hunt the real murderer.

== Cast ==
*Tod Slaughter as The Tiger
*John Warwick as Robert Brierly
*Marjorie Taylor as May Edwards
*Frank Cochran as Melter Moss Robert Adair as Hawkshaw the Detective
*Peter Gawthorne as Joshua Gibson
*Jenny Lynn as Mrs. Willoughby
*Arthur West Payne as Sam Willoughby
*Norman Pierce as Maltby
*Billy Bray as Jackson

== Soundtrack ==
 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 
 