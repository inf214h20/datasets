Kiladi Kittu
{{Infobox film
| name = Kiladi Kittu ಕಿಲಾಡಿ ಕಿಟ್ಟು
| image = Kiladi Kittu poster.jpg
| caption = Film poster
| director = K. S. R. Das
| writer = M. D. Sundar (Story) Chi. Udaya Shankar (Diologue) Vishnuvardhan Rajnikanth Leelavathi Vajramuni
| producer = D. T. S. Rao Y. R. Ashwath Narayana Rao D. S. Narasinga Rao
| music = Mohan Kumar 
| editing = P. Venkateshwara Rao, K. S. R. Das
| released =  
| runtime = 140 Min.
| language = Kannada
| country = India
| budget =
}}
 1978 cinema Kannada film Vishnuvardhan and Rajnikanth in lead roles. The music for the film was composed by Mohan Kumar.

==Cast== Vishnuvardhan
* Rajnikanth Leelavathi
* Vajramuni
* B. Jaya (actress)|B. Jaya
* Shivaram

==Soundtrack==
The music of Kiladi Kittu was composed by Mohan Kumar.
{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Taavare Hoovu Yee Ninna
| extra1 = S. P. Balasubrahmanyam
| lyrics1 = Chi. Udaya Shankar
| length1 = 
| title2 =  Hoovante Hennu
| extra2 = Yesudas
| lyrics2 = Chi. Udaya Shankar
| length2 = 3:16
| title3 =  Nanna ninna Olavu Madhura
| extra3 = S. P. Balasubrahmanyam, S. Janaki
| lyrics3 = Vijaya Narasimha
| length3 = 
| title4 =  Madilalli Maguvagi Naanu
| extra4 = Vishnuvardhan (actor)|Vishnuvardhan, P. Susheela
| lyrics4 = R. N. Jayagopal
| length4 = 
| title5 =  Ee cheluvu seleva naguvu
| extra5 = S. P. Balasubrahmanyam, S. Janaki
| lyrics5 = R. N. Jayagopal
| length5 = 
| title6 =  Manasanu Kodu
| extra6 = S. Janaki
| lyrics6 = 
| length6 =

}}

==External links==
*  

 

 
 


 