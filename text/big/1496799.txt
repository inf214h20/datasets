Boys Life 4: Four Play
{{Infobox film
| name           = Boys Life 4: Four Play
| image          =
| image_size     =
| caption        =
| director       = Phillip J. Bartell Alan Brown Brian Sloan Eric Mueller
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 83 min.
| country        = United States English
| budget         =
| gross          = $53,169 
}}
Boys Life 4 is an anthology of four gay-themed short films that have been hits on the film festival circuit.

==Discography==
* L.T.R. (2002), a mockumentary about a gay couple in a "long-term relationship."
** Cole Williams
** Weston Mueller
* O Beautiful (2002), tells the aftermath of a gay bashing. Shown in split screen.
** Jay Gillespie David Rogers
* Bumping Heads (2002), about two men who meet in the emergency room.
** Craig Chester
** Andersen Gabrych
* This Car Up (2003), about a messenger and businessman who cross paths. It uses slot machine type images to convey what both characters are thinking.
** Michael Booth
** Brent Doyle

==See also==
* List of American films of 2003 Boys Life
* Boys Life 2
* Boys Life 3
* Boys Life 5
* Boys Life 6

==References==
 

== External links ==
*  

 
 
 
 
 
 
 

 
 