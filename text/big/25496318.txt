Wog Boy 2: Kings of Mykonos
 
 
{{Infobox film
| name           = The Wog Boy 2: Kings of Mykonos
| image          = Wog Boy 2 Kings of Mykonos theatrical poster.jpg
| caption        = Theatrical poster
| director       = Peter Andrikidis
| producer       = Chris Anastassiades Nick Giannopoulos
| writer         = Chris Anastassiades Nick Giannopoulos
| starring       = Nick Giannopoulos Vince Colosimo Zeta Makripoulia Costas Kilias Alex Dimitriades Kevin Sorbo
| music          = Amanda Brown Nick West
| cinematography =
| editing        =
| studio         = G.O. Films Film Victoria See-Saw Films
| distributor    = Constantin Film
| released       =  
| runtime        = 102 minutes
| country        = Greece,Australia
| language       = English, Greek, Italian
| budget         = $5 million
| gross          = $14,370,197
}}
 
The Wog Boy 2: Kings of Mykonos  is a 2010 Australian motion picture comedy sequel to the 2000 film The Wog Boy, starring Nick Giannopoulos, Vince Colosimo and Costas Kilias. It was released in Australia on 20 May 2010 and UK on 7 January 2011.

==Plot== Down Under" by Men at Work.

==Reception==
The film has received generally negative reviews. Review aggregate Rotten Tomatoes reports that 22% of critics have given the film a positive review based on 9 reviews, with an average score of 4.8/10. 
 At the Movies film critics Margaret Pomeranz and David Stratton both gave the film a negative review, awarding it 2.5 and 2 stars out of five respectively. The movie is a more accurate representation of Wogs in Australia. 

Shaun Micallef has used the movie as somewhat a running gag on the quiz show Talkin Bout Your Generation

==Cast==
*Nick Giannopoulos as Steve Karamitsis
*Vince Colosimo as Frank Zeta Makrypoulia as Zoe
*Costas Kilias as Tony the Yugoslav (or Tony the Cretan)
*Alex Dimitriades as Mihalis
*Kevin Sorbo as Pierluigi
*Cosima Coppola as Enza 
*Dimitris Starovas as Tzimis
*Galini Tseva as Voula
*Tony Nikolakopoulos as Theo
*Thomas Heyne as Otto
*Mario Hertel as Dieter

==See also==
*Cinema of Australia
*Cinema of Greece

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 