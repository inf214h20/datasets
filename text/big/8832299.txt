Ultraman Tiga: The Final Odyssey
{{infobox film
| name = Ultraman Tiga: The Final Odyssey
| image          = 
| caption        = 
| film name = {{Film name| kanji          = ウルトラマンティガ THE FINAL ODYSSEY
| romaji         = Urutoraman Tiga Za Fainaru Odesei}}
| director       = Hirochika Muraishi
| producer       = 
| writer         = Keiichi Hasegawa
| starring       =  
| music          = 
| cinematography = 
| artdirector    = 
| editing        = 
| studio         =  
| production     = 
| distributor    = Sony Pictures Entertainment
| released       =  
| runtime        = 85 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}
  is a tokusatsu movie, and part of the Ultraman franchise, released in 2000. The movie is a direct sequel to the original Ultraman Tiga television series, serving as an epilogue to the events between Tiga and the successor series, Ultraman Dyna.
A TPC excursion (including GUTS Captain Megumi Iruma) headed to an old ruins and unintentionally wakes up three evil ancient giants. Daigos impending marriage to fellow GUTS member Rena is interrupted by a mysterious woman who hands him a black Spark Lens, as well as disturbing visions of an evil, dark Tiga and three other mysterious giants. Daigo must discover the truth and history behind his visions, and defeat the darkness one more time.

==Plot==
Set two years after Tigas final battle, Daigo is approached by a mysterious woman, Kamila, who possessed a dark version of the Spark Lens. It is then revealed that more than 30,000,000 years ago, Tiga was originally evil in nature, part of a group of four that dominated Earth. The group consisted of Dark Tiga, the telepathic Kamila (gold patterns on silver skin), the powerful Dahram (red patterns on silver) and the speedy Hyudra (purple patterns on silver). One day, Tiga fell in love with a local human, and decided to convert from darkness to protect her from harm. Being the weakest of the four, Tiga quickly became a target of the group. However, unbeknownst to the group, the original "talentless" Dark Tiga possessed the ability to absorb powers from fallen enemies. As he eliminated each of the three dark members and sealed them away, he absorbed their powers which explains new Tigas color patterns (purple, red and gold lined with his original silver). Convinced by his ability to convert the last known Spark Lens to good, Daigo accepts the gift and becomes Tiga once again, survives his inner struggles and vanishes the darkness within. Soon after this, he marries Rena Yanase, fellow GUTS member and longtime love interest.(Shin Asuka, Dynas eventual human host, also makes a cameo during the finale as a junior crew member, in a symbolic passing of the torch moment.).

== Cast and Crew ==

*Hiroshi Nagano - Ultraman Tiga/Daigo Madoka
*Mio Takagi -  Captain Megumi Iruma/Yuzare
*Takami Yoshimoto -  Rena Yanase–Madoka
*Akitoshi Ohtaki - Deputy Captain Seichi Munakata
*Yukio Masuda - Masami Horii
*Shigeki Kagemaru -  Tetsuo Shinjoh
*Yoichi Furuya -  Jun Yazumi
*Kei Ishibashi - Mayumi Shinjoh
*Masaru Matsuda -  Dahram
*Miyoko Yoshimoto -  Kamilla (Human Form) Shin Asuka (Cameo)
*Ryô Kinomoto - Gosuke Hibiki
*Mariya Yamada -  Mai Midorikawa
*Lisa Saito - Ryoh Yumimura
*Jou Onondera - Tsutomu Nakajima
*Toshikazu Fukawa - Toshiyuki Koda
*Takao Kase - Kohei Kariya
*Scott T. Hards - Priest

==References==
 

 

 
 
 
 
 

 