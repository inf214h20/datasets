Kadhalna Summa Illai
{{Infobox film
| name           = Kadhalna Summa Illai
| image          = 
| alt            =  
| caption        = 
| director       = Ilankannan
| producer       = M. Rajendran M. Rajarathinam M. Ravethiran M. Ragunatha
| writer         = Sekar Prasad   (dialogues ) 
| screenplay     = Ilankannan
| story          = Radhakrishna Jagarlamudi
| starring       = Ravi Krishna Sharwanand Kamalinee Mukherjee Vidyasagar Mani Sharma E. S. Murthy
| cinematography = S. A. Vincent
| editing        = V. T. Vijayan Raj TV
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Raj TV, is the remake of the successful, award-winning Telugu language film, Gamyam directed by Radhakrishna Jagarlamudi.

==Plot==
Abhiram or Abhi, (Sharwanand) as called by his friends and family, is the son of a multi-millionaire called GK (Nassar). He is born and brought up among currency and for him life is just a game and he is accustomed to lavish lifestyle. He dates several girls and one day he comes across Janaki (Kamalinee Mukherjee), a young, charming doctor. Abhi challenges his friends that he would make her fall in love within few days. Abhi invites Janaki for coffee after a dance programme and proposes to her.

Janaki, being an orphan, is very kind towards the poor people and is service-minded. She mingles with poor people and provides succour to them. But, being a rich boy, Abhi hates those people. However, Janakis free-spirited nature takes him very near to her and he develops an intimate friendship. At the same time, Janaki keeps maintaining some distance and does not express her love. Though she likes him, she cannot love him as he is not sensitive to the people and things around him. A privileged upbringing makes him myopic to the hard realities of life. By the time, she wants to express her love, she learns that Abhi has challenged his friends about his love and decides to keep herself off.

While dropping her at her hostel on his car after a party, Abhi tries to convince her that he is really in love with her but she does not listen to him. In the process, Abhi causes an accident, where a woman dies and two of her children become orphans. Janaki survives the accident, while Abhi wakes up from injuries in hospital. After gaining consciousness, Abhi cannot find Janaki and he decides to go in search of her on his bike.

In the process, he happens to meet a motorbike thief called Vetti Velu (Ravi Krishna). Though Velu is a vehicle thief, he is good at heart. The journey Abhi takes, changes his life drastically as he is exposed to the hard realities of rural life and yet its simple joys. The landscapes  and the people he meets takes him through an emotional journey that alters his perceptions forever. Be it  a teacher, who supports orphan kids, a disillusioned ex-militant, a prostitute who craves for love of his own companion Vetti Velu, they all aid in his journey of self-discovery.

==Cast==
* Ravi Krishna as Vetti Velu
* Sharwanand as Abhiram
* Kamalinee Mukherjee as Janaki
* Nassar as GK
* Tejashree
* M. S. Bhaskar
* Ganja Karuppu
* Ilavarasu

==References==
 

 
 
 
 
 
 
 