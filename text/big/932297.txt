The Road to El Dorado
{{Infobox film
| name           = The Road to El Dorado
| image          = Road to el dorado ver3.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Don Paul   David Silverman
| producer       = Brook Breton Bonne Radford Ted Elliott Terry Rossio
| narrator       = Elton John
| starring       = Kevin Kline Kenneth Branagh Armand Assante Jim Cummings Edward James Olmos Tobin Bell Rosie Perez
| music          =   Tim Rice  
| editing        = John Carnochan Dan Molina Vicki Hiatt Lynne Southerland
| studio         = DreamWorks 
| distributor    = DreamWorks Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States 
| language       = English
| budget         = $95 million
| gross          = $76,432,727
}} animated Adventure adventure comedy soundtrack features songs by Elton John and Tim Rice, as well as composer Hans Zimmer.
 loaded dice, they win a map that supposedly shows the location of El Dorado, the legendary city of gold in the New World. However, their cheating is soon discovered and as a result, they end up as stowaways on Hernán Cortés fleet to conquer Mexico. They are discovered, but manage to escape in a boat with Cortés prize war horse and eventually discover the hidden city of El Dorado, where they are mistaken for gods. The film received mixed reviews from critics and was a box office bomb.

==Plot== con artists, Tulio (Kevin Kline) and Miguel (Kenneth Branagh), win a map to the legendary City of Gold, El Dorado, in a rigged gambling match. After their con is discovered, the two evade capture while being chased by a bull and hide in barrels, which are shortly loaded onto one of the ships to be led by Hernán Cortés (Jim Cummings) to the New World. During the trip, they are caught as stowaways and imprisoned, but manage to break free and take a rowboat with the help of Cortés horse, Altivo (Frank Welker). They land at an unknown shore at the edge of Mexico, and Miguel begins to recognize landmarks as stated on the map, which shows directions to El Dorado. The map leads them to a totem marker outside of a waterfall where a young woman approaches them, chased by a number of guards. The guards see the image of Tulio and Miguel riding Altivo as the same on the totem, and believing them to be gods, escort them and the woman under the falls and into El Dorado, truly a city made of gold.

Tulio and Miguel are brought to the citys elders, the kind-hearted Chief Tannabok (Edward James Olmos) and wicked high priest Tzekel-Kan (Armand Assante). While Tannabok warmly welcomes them to the city, Tzekel-Kan mainly sees them as a way to enhance his own standing. Tzekel-Kan also believes that with the arrival of the gods comes "The Year of the Jaguar", a year in which the city will be purged of all wicked people. Tulio and Miguel begin to argue on what to do. Everyone is convinced they are gods when as a volcano is beginning to erupt, Tulio yells at Miguel to stop pestering him, and the volcano suddenly stops. After celebrations offered by both Tannabok and Tzekel-Kan, the two are taken to private quarters along with the woman they met earlier, Chel (Rosie Perez), who has seen through their ploy but offers to help maintain it as long as they give her a share of the gold and take her with them when they leave. Tulio tells Tannabok the next day that they are only here for a visit, but will need a boat to leave the city with the gifts the city has showered upon them.
 a ball game with children, he demands that the gods play against Chief Tannaboks warriors, the citys best players. During the match, Tulio and Miguel are clearly over-matched, but Chel replaces the ball with an armadillo named Bibo, allowing the two to cheat and win the game. However, when Tzekel-Kan offers to have the defeated players sacrificed, Miguel declares there is no need for sacrifices or him.

As he is leaving, Tzekel-Kan sees a small cut on Miguels forehead and realizes he is not a god because gods do not bleed. Tzekel-Kan conjures a giant stone jaguar to chase them through the city. Tulio and Miguel manage to outwit the stone jaguar, causing both it and Tzekel-Kan to fall into a giant whirlpool, thought to be the entrance to Xibalba, the spirit world. Tzekel-Kan comes to outside El Dorado, where Cortés and his men are searching for gold. Thinking Cortés is a true god, Tzekel-Kan quickly offers to lead them to El Dorado. With their boat completed and loaded with gold, Tulio is ready to leave but Miguel announces that he will be staying because he finds the city peaceful. As Tulio and Chel start to leave, they spot smoke on the horizon, realizing that Cortés and his men are approaching the city with the help from Tzekel-Kan. To protect the city from the Spanish troops, Tulio determines they can use the boat to slam against rock formations under the waterfall path that will cave in and block access to the city.

The city residents pull down a large statue to create a wave to propel the boat, but Tulio cannot get the sails up to give the boat enough speed to avoid the statue. Miguel forgoes his chance to stay in the city and jumps into the boat with Altivo to finish hoisting the sails. The boat clears the statue in time, and Tulios plan is successful; though the boat and the gold are lost, the entrance to El Dorado is sealed for good. Tulio, Miguel, Chel  and Altivo hide as Tzekel-Kan brings Cortés and his men towards the waterfall. Once Tzekel-Kan finds out that the entrance has been blocked, an angry Cortés takes this as a lie. Cortés and his men then march away with a humiliated Tzekel-Kan in their hands. Tulio and Miguel, though disappointed they lost their treasure, takes off in a different direction for a new adventure with Chel, unaware that Altivo still wears the golden horseshoes he was outfitted with in the city.

==Cast==
* Kevin Kline as Tulio, one of the con artists who pretend to be gods so they can get gold. He is the planner who wanted to leave El Dorado with the treasure. 
* Kenneth Branagh as Miguel, one of the con artists who pretend to be gods so they can get gold. He is the fun-loving one who wants to stay in El Dorado.
* Rosie Perez as Chel, a young native from El Dorado who discovers Tulio and Miguels con and decides to play along.
* Jim Cummings as Hernán Cortés, the merciless and ambitious leader of the expedition to find the empires of the New World.
* Armand Assante as Tzekel-Kan, the fanatically vicious high priest who has a religious fixation for human sacrifices.
* Edward James Olmos as Chief Tannabok, the kind chief of El Dorado who welcomes Tulio and Miguel.
* Tobin Bell as Zaragoza, the original owner of the map, which he loses to Tulio and Miguel when gambling with them.
* Frank Welker as Altivo, Cortés horse who ends up teaming up with Tulio and Miguel.

==Production==
Under the working title El Dorado: City of Gold,   the film was originally scheduled for release in fall of 1999.    During production, the filmmakers drew much inspiration for the characters of Miguel and Tulio from those of the Bob Hope and Bing Crosby Road to... films. "The buddy relationship   is the very heart of the story. They need each other because theyre both pretty inept. Theyre opposites — Tulio is the schemer and Miguel is the dreamer. Their camaraderie adds to the adventure; you almost dont need to know where theyre going or what theyre after, because the fun is in the journey", remarked one of the films producers, Bonne Radford. Unusually for an animated film, both Kline and Branagh recorded their lines in the same studio room together, in order for the two to achieve more realistic chemistry. This proved difficult for the audio team. 

In late 1996, Tim Rice and Elton John were asked to compose seven songs, which they immediately worked on. In February 1999, before the release of Elton John and Tim Rices Aida, it was announced that ten songs had been composed for El Dorado. It was also announced that the release date had been pushed to March 2000.   

The creation of the film was a challenge for the studio because DreamWorks Animation had devoted most of its creative efforts to its previous animated film, The Prince of Egypt.

==Release==

===Critical reception===
The film received mixed reviews from critics; it holds a 48% "rotten" rating out of 104 reviews at  , which assigns a normalized rating out of 100 top reviews from mainstream critics, calculated a score of 51 based on 29 reviews, indicating "mixed or average reviews". 

Paul Clinton of CNN wrote, "The animation is uninspiring and brings nothing new to the table of animation magic," praising the Elton John/Tim Rice songs, but noting the weak plot. 

In contrast, Roger Ebert of the Chicago Sun-Times gave the film a thumbs up and commented that it was "bright and zesty," having enjoyed it as a simple comedic farce,  while Joel Siegel, on Good Morning America, called it "solid gold," claiming the film was "paved with laughs." 

===Box office=== Erin Brockovich s third weekend.  The film closed on April 29, 2001, after earning $50,863,742 in the United States and Canada and $25,568,985 overseas for a worldwide total of $76,432,727. Based on its total gross, The Road to El Dorado was a box office bomb, unable to recoup its $95 million budget. 

===Accolades===
{| class="wikitable" width="95%"
|- 
! Award !! Category !! Winner/Nominee Recipient(s) !! Result
|-
| rowspan= 8 | Annie Awards   
| Animated Theatrical Feature || ||  
|-
| Individual Achievement in Storyboarding || Jeff Snow (Story supervisor) ||  
|-
| Individual Achievement in Production Design || Christian Schellewald (Production Designer) ||  
|-
| Individual Achievement in Character Animation || David Brewster (Senior Supervising animator - Miguel) ||  
|-
| Individual Achievement in Character Animation || Rodolphe Guendonen (Supervising Animator - Chel) ||  
|-
| Individual Achievement in Effects Animation || Doug Ikeler (Effects Lead - Crashing the Gate) ||  
|-
| Individual Achievement in Voice Acting || Armand Assante ("Tzekel-Kan") ||  
|- John Powell (Music) ||  
|-
| 6th Critics Choice Awards|Critics Choice Awards 
| Best Composer
| Hans Zimmer
|  
|-
|}

===Video game===
 
The video game tie-in, released on PlayStation, Game Boy Color, and Personal computer|PC, was named Gold & Glory: The Road to El Dorado. 

===Home media===
The Road to El Dorado was released on DVD and VHS on January 20, 2001. 

==Soundtrack==
{{Infobox album  
| Name        = The Road to El Dorado
| Type        = Soundtrack
| Artist      = Elton John
| Cover       = 
| Released    = March 14, 2000
| Recorded    =  pop
| Length      = 62:14
| Label       = DreamWorks Records 
| Producer    = Patrick Leonard The Muse (1999)
| This album  = The Road to El Dorado (2000)
| Next album  = Elton John One Night Only – The Greatest Hits (2000) Misc = {{Singles
 | Name            = The Road to El Dorado
 | Type         = soundtrack
 | Single 1      = Someday Out of the Blue (Theme from El Dorado)
 | Single 1 date = 2000
 | Single 2      = Friends Never Say Goodbye
 | Single 2 date = 2000
}}}}
 John Powell. The Lion The Prince of Egypt.

In some instances (such as "The Trail We Blaze"), the songs have been altered musically and vocally from the way they appeared in the film.
A " " recording of the soundtrack exists, but was never released to the public. It includes the theatrical versions of the songs, including "Its Tough to be a God" recorded by Kevin Kline and Kenneth Branagh, and several of the score tracks by Hans Zimmer.
 Eagles members Don Henley and Timothy B. Schmit are credited as background vocalists on the song "Without Question".

===Track listing===
{{tracklist
| collapsed       = 
| headline        = 
| total_length    = 
| writing_credits = yes

| title1          = El Dorado
| writer1         = Elton John, Tim Rice
| length1         = 4:22
| title2          = Someday Out of the Blue (Theme from El Dorado)
| writer2         = Elton John, Patrick Leonard, Tim Rice
| length2         = 4:48
| title3          = Without Question
| note3           = featuring Don Henley and Timothy B. Schmit
| writer3         = Elton John, Tim Rice
| length3         = 4:47
| title4          = Friends Never Say Goodbye
| note4           = featuring Backstreet Boys 
| writer4         = Elton John, Tim Rice
| length4         = 4:21
| title5          = The Trail We Blaze
| writer5         = Elton John, Tim Rice
| length5         = 3:54
| title6          = 16th Century Man
| writer6         = Elton John, Tim Rice
| length6         = 3:40
| title7          = The Panic in Me
| writer7         = Elton John, Hans Zimmer, Tim Rice
| length7         = 5:40
| title8          = Its Tough to Be a God
| note8           = Duet with Randy Newman
| writer8         = Elton John, Tim Rice
| length8         = 3:50
| title9          = Trust Me
| writer9         = Elton John, Tim Rice
| length9         = 4:46
| title10         = My Heart Dances
| writer10        = Elton John, Tim Rice
| length10        = 4:51
| title11         = Queen of Cities (El Dorado II)
| writer11        = Elton John, Tim Rice
| length11        = 3:56
| title12         = Cheldorado
| note12          = Performed by Hans Zimmer with Heitor Pereira 
| writer12        = Hans Zimmer
| length12        = 4:26
| title13         = The Brig
| note13          = Performed by Hans Zimmer with Trilogy
| writer13        = Hans Zimmer
| length13        = 2:58
| title14         = Wonders of the New World
| note14          = Performed by Hans Zimmer John Powell
| length14        = 5:56
}}

{{tracklist
| collapsed      = yes
| headline       = Best Buy exclusive tracks
| title15        = Perfect Love
| writer15       = Elton John, Tim Rice
| length15       = 4:09
| title16        = Hey, Armadillo
| writer16       = Elton John, Tim Rice
| length16       = 3:46
}}

==References==
 

==External links==
 
*   archived from   on May 10, 2000
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 