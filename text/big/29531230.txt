Swing Your Lady
{{Infobox film
| image	         = Swing Your Lady FilmPoster.jpeg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Ray Enright
| producer       = Samuel Bischoff Charles Robinson (play) Joseph Schrank Maurice Leo
| starring       = Humphrey Bogart Frank McHugh Louise Fazenda Ronald Reagan
| music          =
| cinematography = Arthur Edeson
| editing        =
| released       = January 8, 1938
| runtime        = 77 minutes
| country        = United States 
| language       = English 
| studio         = Warner Bros.
| budget         =
| gross          =
}} country musical comedy film directed by Ray Enright, starring Humphrey Bogart, and featuring Ronald Reagan.

Bogart was apparently becoming very disenchanted with the film roles that the Warner Brothers studios were offering him at this stage of his career; the following year he appeared in his only horror/sci-fi film, The Return of Doctor X, and these were two roles he never liked talking about when he became a major film star several years later; he considered his performance in Swing Your Lady the worst of his career. Years later, Swing Your Lady was included as one of the choices in the book The Fifty Worst Films of All Time.

==Plot==
Promoter Ed Hatch comes to the Ozarks with his slow-witted wrestler 
Joe Skopapoulos whom he pits against a hillbilly Amazon blacksmith, 
Sadie Horn. Joe falls in love with her and wont fight, at least not until Sadies beau, Noah, shows up.

==Cast==
{| class="wikitable"
|- 
! Actor/Actress || Role
|-
| Humphrey Bogart || Ed Hatch 
|-
| Frank McHugh || Popeye Bronson 
|-
| Louise Fazenda || Sadie Horn  
|-
| Nat Pendleton || Joe Skopapolous 
|-
| Penny Singleton || Cookie Shannon 
|- 
| Allen Jenkins || Shiner Ward  
|-
| Leon Weaver || Waldo Davis 
|- 
| Frank Weaver || Ollie Davis  
|-
| June Weaver || Mrs. Davis (as Elviry Weaver)  
|-
| Ronald Reagan || Jack Miller 
|-
| Daniel Boone Savage || Noah Webster   
|-
| Hugh OConnell || Smith     
|-
| Tommy Bupp || Rufe Horn 
|-
| Sonny Bupp || Len Horn (as Sunny Bupp)  
|-
| Joan Howard || Mattie Horn 
|}

==References==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 