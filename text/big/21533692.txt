Bhalobasa Bhalobasa (1985 film)
{{Infobox film
| name = Bhalobasa Bhalobasa ভালবাসা ভালবাসা
| image =Bhalobasa Bhalobasa1985.jpg
| image_size=
| caption = DVD cover
| director = Tarun Majumdar
| writer = Tarun Majumdar
| starring = Tapas Paul Deboshree Roy
| producer = Manisha Sarkar
| distributor =
| cinematography = Shakti Banerjee
| editing =
| released =  
| country        = India
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| runtime = 120 minutes Bengali
| music = Hemanta Mukherjee
}}
Bhalobasa Bhalobasa ( )is a 1985 Bengali romantic film directed by Tarun Majumdar. The film is starring Tapas Paul, Deboshree Roy.

==Plot== Anup Kumar) stay in Shimultala (Bihar). Arup (Tapas Pal) is a small business man dealing in poultry and also helps poor people with free medicine and education. His mother (Madhabi Mukherjee) was a noted singer who was had recently died from cancer. His father had left his mother for another rich woman when she was expecting Arup.
Keya has a fight with Arup when he comes to her hostel to supply poultry. Later Keya comes to know that Arup is also a famous singer like his mother and both Keya and Arup fall in love with each other. Keya starts visiting Arups house and helps him in his work. Arup is approached by Keya on behalf of the Gramophone Company for recording Rabindrasangeet but Arup does not agree for some unknown reason and instead throws out Keya from his house. Later, he repents for his mistake and starts singing. The couple eventually sort their differences.
When Keya goes to Shimultala on Durga Puja vacation, Arup follows her as per pre-plan. There, they come to know that Keyas father (Utpal Dutt) has already fixed up her marriage with an Engineer (Kaushik Banerjee) who is also the son of a rich Barrister (Satya Banerjee) from Patna. Arup by his good-nature tries to win the favours of Keyas parents and brother. However, Arup comes to know the true identity of the Barrister and his son and at the behest of Keyas mother (Ruma Guha Thakurta) decides to leave the place without informing Keya. However, before leaving Shimultala he decides to sing his last song at the Bijoya Sammilani. Through the song he narrates the whole sad life of his mother, her life-long struggle and how he was ill-treated by his father (the Barrister) when Arup had requested his father to come and meet his dying mother. After hearing the song, Arups father realises his biggest mistake of life and requests forgiveness from his son. Both the parents decide to get Keya and Arup married to each other and eventually the movie ends with a happy note.

==Cast==
* Tapas Paul  as Arup
* Debashree Roy  as Keya
* Ruma Guha Thakurta as Keyas Mother
* Utpal Dutt as Keyas Father Anup Kumar
* Santu Mukhopadhyay
* Sumitra Mukherjee

== Crew ==
* Director: Tarun Majumder
* Producer : Manisha Sarkar
* Presenter : Hemonto Mukherjee
* Cinematographer: Shakti Banerjee
* Editor : Ramesh Joshi
* Playback Singer :Arundhati Hom Chowdhury, Arati Mukherjee, Shibaji Chattopadhyay

==Awards==
* BFJA Awards  (1986)
** Best Editing Ramesh Joshi
** Best Lyrics Pulak Bandyopadhyay
** Best Music Hemanta Mukherjee
** Best Playback Singer (Female) Arundhati Hom-Chowdhury
** Best Playback Singer (Male) Shibaji Chatterjee

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 

 