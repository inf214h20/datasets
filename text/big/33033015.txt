Moss Rose (film)
 

{{Infobox film
| name           = Moss Rose
| image          = Moss rose poster small.jpg
| image_size     = 200px
| alt            =
| caption        = Theatrical release poster
| director       = Gregory Ratoff
| producer       = Gene Markey	
| screenplay     = Niven Busch Jules Furthman Tom Reed
| based on       =  
| starring       = Peggy Cummins Victor Mature Ethel Barrymore Vincent Price
| music          = David Buttolph
| cinematography = Joseph MacDonald James B. Clark
| distributor    = 20th Century Fox
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Moss Rose is a 1947 period thriller film noir directed by Gregory Ratoff, and starring Peggy Cummins and Victor Mature. 

==Plot== Victorian London, the story concerns a music hall chorus girl who blackmails a gentleman after seeing him leave the house where another dancer was found murdered. Instead of accepting money she demands to be invited to the mans stately home to experience the life of a lady. The woman becomes friends with the mans family but her peace is disturbed when a police inspector arrives to question them about the murder. Then another murder is committed in similar circumstances.

==Cast==
* Peggy Cummins as Belle Adair aka Rose Lynton
* Victor Mature as Michael Drego
* Ethel Barrymore as Lady Margaret Drego
* Vincent Price as Police Inspector R. Clinner
* Margo Woode as Daisy Arrow
* George Zucco as Craxton, the butler
* Patricia Medina as Audrey Ashton Rhys Williams as Deputy Inspector Evans

==Reception==
===Box-office===
The film was a commercial disappointment. Darryl F. Zanuck called it "a catastrophe, for which I blame myself. Our picture was not as good as the original script and the casting was atrocious. The property lost $1,300,000 net." 

===Critical response===
When the film was released, The New York Times film critic, Bosley Crowther, praised the film, writing, "Readers of thriller fiction have been talking for quite some time about a writer called Joseph Shearing, whose many period mysteries are said to have a flavor and distinction all their own. And now it appears that film-goers will have reason to join the claque, if all of this authors output is as adaptable as the first to reach the screen. For Moss Rose, the first of several promised Shearing films, which hit the Roxy yesterday, is a suave and absorbing mystery thriller, neatly plotted and deliciously played ... Thanks to a splendid performance by Peggy Cummins in the role of the girl, there is something to watch when she is acting besides the consequence of the makeup artists work. Her job as the Cockney chorine has spirit, humor and brass—and a surprisingly tender quality which nicely rounds the role." 

The staff at Variety (magazine)|Variety magazine also gave the film a positive review.  They wrote, "Moss Rose is good whodunit. Given a lift by solid trouping and direction, melodrama is run off against background of early-day England that provides effective setting for theme of destructive mother love ... Gregory Ratoffs direction develops considerable flavor to the period melodramatics. He gets meticulous performances from players in keeping with mood of piece." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 