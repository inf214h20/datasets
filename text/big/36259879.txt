Biriyani (film)
{{Infobox film
| name           = Biriyani
| image          = Biriyani (film) poster.jpg
| caption        = First Look Poster
| director       = Venkat Prabhu
| producer       = K. E. Gnanavel Raja
| writer         = Venkat Prabhu Ramki Mandy Takhar
| music          = Yuvan Shankar Raja
| cinematography = Sakthi Saravanan
| editing        = Praveen K. L. N. B. Srikanth
| studio         = Studio Green
| distributor    =
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil Telugu
| budget         =  
| box office     =  
}} Tamil black Srikanth handled Hyderabad and Ambur. The film was released on 20 December 2013. 

==Plot==
The story starts with Sugan and his innocent friend Parasuram being chased by group of police.Then comes a flash back when they go to a party, there Sugan and Priyanka fight. One day they drink on their way to Ambur for their new branch opening, where they meet Varadharajan, who is the chief guest for the ceremony. In the meantime, its shown that a CBI officer Riyaz Khan has been deputed to track Vardharajans illegal activities. Varadharajan gets impressed with Sugan and decides later to make him his son-in law. In this matter, he also argues with his son in law Vijayakrishna. He later on invites him for his cocktail party. Sugan and Parasuram get drunk and search for a biriyani shop. There they both get to meet Maya. The very next day they find police cars searching for Varadharajan, who is missing. Meanwhile Sugan and Parasuram find Varadharajans dead body in their car trunk.They are blamed for Varadharajan"s death and are chased by police, there the flashback ends. Sugan and Parasu goes to their friends house and Parasu sends a picture of Maya. The next day, one of their friend finds Maya at Egmore Railway Station. Sugan comes there and Maya tells the truth but a police stops Maya from telling the truth. The police also kills Maya on the train tracks. Back at home, Sugan gets a message from the police that killed Maya and he finds out that she is not even a police but the Hit women. Her next victim is no one other than Varalakshmi, Sugans sister. At the temple Sugans friend, his brother-in-law and Varalakshmi are in a car accident and the Hit women captures Varalakshmi. Sugan gets a call from Vijayakrishna that he needs Varadharajan alive so that he can give Varalakshmi back to Sugan. Sugan decides that Parasu should impersonate Varadharajan alive since the real Varadharajan is dead. He comes there with Parasu, but finds the Hit women trying to kill Sugan. Vijayakrishna wants Varadharajan back but it was not him who called Sugan. Another police officer ACP Vikram kills the Hit women and returns Varalakshmi safe. Three days later at Varadharajans funeral Sugan finds the person who broke Sugans love and tells the truth about Varadharajans death. Avinash was in love with Varadharajans daughter for money only but Sugan and Parasu stopped their love on the way to the new branch opening. Avinash uses his brother to get his love back by bringing Sugan and Parasu using Maya and that Maya used a tablet to make Sugan and Parasu very drunk. Then the Hit women comes by killing Maya so that she does not tell the truth to Sugan. Sugan finds out who the setup was caused. It was no one other than ACP Vikram, Avinashs brother. Even Riyaz Khan also finds out the truth. Riyaz Khan tells Sugan to kill ACP Vikram and Sugan fights with him and they both fall from the first floor to the ground.

==Cast==
* Karthi as Sugan
* Hansika Motwani as Priyanka Sharma, Sugans girlfriend
* Premgi Amaren as Parasuram or Parasu or Come On Tiger. Parasu is Sugans best friend since young.
* Ramki as Vijay Krishna, Varadharajans brother-in-law.
* Mandy Takhar as Maya, a prostitute whom Sugan met in Bangalore highway.
* Nassar as Varadharajan, a businessman who is wanted by police for his illegal investments.
* Sampath Raj as Riyaz Ahmed, a police officer who investigates in the murder case of Varadharajan.
* Jayaprakash as AC Sampath, Riyazs assistant. Prem as Vikram
* Madhumitha as Varalakshmi
* Nithin Sathya as Hari
* Subbu Panchu as Subbhu
* Uma Riyaz Khan as Hitwoman
* Badava Gopi as Gopi
* Shanmugasundaram as Kasi Vishwanathan Vijayalakshmi as Rohini Varadharajan
* Rethika Srinivas as Radhika Varadharajan
* Tharika as Kalpana
* Vidhya Venkatesh as Shwetha
* Leena Maria Paul as Sree Devi
* Sangeetha Gopal as Shanthi Sam Anderson as Sam
* Haanii Shivraj as Hot Girl 1 at Ambur new branch opening

; Special appearances by: Jai as Jai Kanth
* Aravind Akash as Akash
* Ashwin Kakumanu as Ashwin
* Mahat Raghavendra as Raghav
* Vaibhav Reddy as Reddy
* Vijay Vasanth as Vasanth
* manchu manoj as special appearance in song

==Production==

===Development===
Following the release of Mankatha, Venkat Prabhu was reported to be writing a script for Suriya.  In December 2011, he was signed up by Studio Green, the production company owned by Suriyas cousin K. E. Gnanavelraja, while later the month the director informed that Suriya had agreed to be part of his next film,  which he was planning to shoot in 3D film|3D.  Further more, the media carried reports that it would be a bilingual, made simultaneously in Tamil and Telugu, and feature Telugu actor Ravi Teja, Sonam Kapoor and Taapsee Pannu in other lead roles. 
 Hari on the sequel to his Singam first after completing Maattrraan,   which prompted Venkat Prabhu to make another film for Studio Green instead with Suriyas brother Karthi in the lead.  The film was named Biriyani,  and the director stated that the same team of his previous four films, comprising music composer Yuvan Shankar Raja, cinematographer Sakthi Saravanan, editors Praveen K. L. and N. B. Srikanth and costume designer Vasuki Bhaskar, would work on Biriyani.  The film was launched on 16 July 2012 after a small Puja (Hinduism)|pooja,  but filming did not start until November 2012, since Venkat Prabhu was reworking his script and the casting was not finished yet.    He made it clear that the film planned with Suriya earlier was an "entirely different script",  and that he would work on that project later,  while revealing that Biriyanis script was initially intended for actor Vijay (actor)|Vijay. 

===Casting===
Venkat Prabhus brother Premgi Amaren, who had been part of all his brothers ventures, was given an "almost parallel role" as the lead characters best friend in Biriyani.    For the lead female role, the production team first approached Samantha Ruth Prabhu|Samantha.  Bollywood actresses Parineeti Chopra and Ileana DCruz were also considered for the role,  but in early July 2012, Bengali actress Richa Gangopadhyay was selected for the role, who said that she played "a typical girl in it; someone whos mischievous and not-so innocent".  However, she left the project by mid-October 2012 due to "changes in the role and script".  After Gangopadhyays exit, Venkat Prabhu expressed interest in casting Nayantara for the role.  while later the month Hansika Motwani was finalized to play the leading lady of the film.  Motwani told that she played a journalist named Priyanka, while classifying the film as a "black comedy thriller". 
 Sneha and Prasanna had Meena was Sam Anderson.  Reports suggested that Canadian film critic Review Raja had done a dance number choreographed by Shobhi during his visit in Chennai. 

===Filming===
The films principal photography was to start in August 2012,  but was delayed because of changes in the script and the cast. It was then supposed to begin by early October 2012 in Ambur,  but was officially commenced on November 5, 2012,  following a test shoot on November 1.  The first day of shoot involved scenes between Karthi and Premji, which was carried out on a set inside a house near the Kodambakkam railway station in Chennai.  The short Chennai schedule was followed by schedules in Gingee Fort|Senji, Ambur and Pondicherry.  A set of a hotel worth   1 crore was erected in Chennai,  where action scenes and songs were shot for the film.  Motion capture technology was used to film a fight sequence involving Karthi, Nitin Sathya, Murugadas and Panchu Subbu which was shot with a multiple camera setup at Puducherry.  The unit then took a break as Karthi moved on to shoot his other film All in All Azhagu Raja simultaneously,  and filming was continued on 30 March 2013 in Hyderabad, India|Hyderabad.  60% of the film had been completed at that time, according to the director. 
 Binny Mills and the Rajiv Gandhi Salai (OMR).  Venkat Prabhu further said that the crew would be heading to Spain and Portugal in May to shoot two songs involving Karthi and Hansika.    By mid-June, the entire talkie portion had been canned.  The final song, choreographed by Kalyan, was shot in July 2013.  Although the team had planned to shoot the song in a foreign country,  it was eventually filmed in Meenambakkam, Chennai,  where two sets had been created,  with Venkat Prabhu intending to shoot the song in the style of a Broadway musical, who went on to add that "the backgrounds in the song will keep changing throughout". 

==Soundtrack==
 
 Vaali and Sony Music in May 2013.  On August 16, the audio tracks were leaked online causing an uproar from the team.  Due to the leak, the planned audio launch was subsequently cancelled, and though it was announced that the album would be directly released to stores on 21 August 2013,  the audio launch eventually happened at an undisclosed FM station on 21 August.  The audio received positive response from critics as well as audience.     

==Marketing==
Although costume designer Vasuki Baskar wrote on her social media account that the first look of the film would be released on November 13, during the Diwali festival,  it was unveiled on December 15, 2012 at Yuvan Shankar Rajas concert in Malaysia.  A 75-second teaser of the film was released on 25 May 2013, on the occasion of Karthis birthday. 

==Release== Sun TV. Studio Green initially finalised September 6, 2013 as the release date, to be coinciding with the 2013 Vinayagar Chaturthi festival.  But the date was changed to January 2014, and later to December 20 2013.  In early August 2013, ATMUS Entertainment announced that it had acquired the US theatrical screening rights for the film. The film was given "U/A" certificate by the Indian Censor Board. The films producer initially went to the revision committee to get a "U" certificate to enjoy the benefit of entertainment tax exemption, but the committee rejected the appeal and awarded the film with a "U/A" certificate.  

Biriyani released in 1050 screens worldwide on 20 December 2013 with tough competition from Dhoom 3 and Endrendrum Punnagai. The Telugu version of the film releasing on the same day in Andhra Pradesh.

==Critical reception==
Biriyani received mixed to positive reviews from critics. Among the positive reviews were from The Times of India gave 3.5 stars out of 5 and wrote, "Venkat Prabhu definitely knows how to have a cake and eat it too. Fun is certainly the core of all his movies even when the genres are varied. That is the case with Biriyani as well. It is essentially a murder mystery but Venkat seasons it with his brand of wink wink nudge nudge filmmaking, and turns it into an engaging entertainer".  Behindwoods gave 3.25 stars out of 5 and wrote, "Venkat Prabhu’s Biriyani while adhering to the director’s recipe, has humor, suspense, music, action and glamour in delectable portions resulting in a delicious product".  Rediff gave 2.5 stars out of 5 and wrote, "Though the film takes its own sweet time to get a move on, once it gathers momentum, there is no stopping till the end, where there is an exciting climax, as well as an anticlimax, in typical Venkat Prabhu style", calling it "a fun-filled thriller".  Indiaglitz gave 3.5 out of 5 and wrote, "Through the first half the ingredients make up a commercial pot boiler and the build up to the story is slow and steady. The last 20 minutes serves the twist in the tale, and round of applause to the director to have correlated the events of 2 hours in the final climax".  The Hindu wrote, "Intelligent line, sensible humour, particularly at the most unexpected moments, Sakthi Saravanan’s contribution with the lens, Silva’s noteworthy action choreography and Karthi’s energy are scoring points of this Venkat Prabhu offering. Youth should find the Biriyani quite tasty".  hindustantimes wrote, "this Biriyani is hard to digest,
 IANS gave 2.5 stars out of 5 and wrote, "Venkat Prabhus Biriyani despite being fairly satisfying is not excellent because it gets too starchy due to unwanted ingredients. While the use of situational comedy works to some extent but what we see in some scenes is not humorous but downright awful and the double entendres directed at women are uncalled for". 

Another reviewer from The Hindu wrote, "It’s (the) irreverent fun that makes this film different from regular thrillers. Despite some good ingredients and reasonably good cooking methods, there’s something that doesn’t come together and stops this from being a delectable meal".  The New Indian Express wrote, "Venkat Prabhu’s Biriyani falls flat and struggles to make an impact with the audience with a nonsensical love story and even worse thriller plot. The director’s attempt at infusing some comedy as well, fails, with crass jokes on women that are almost nauseating to watch". 

== Box office ==
Although the movie gained mixed to positive reviews from critics, the film failed to sustain its momentum and only able to garner average collections in its lifetime run. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 