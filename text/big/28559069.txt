Petrang Kabayo
{{Infobox film
| image          = Petrang_Kabayo_2010.jpg
| caption        = Theatrical movie poster
| name           = Petrang Kabayo Wenn Deramas 
| producer       = Billy Briones Vicente G. del Rosario III Vic del Rosario Jr.
| music          = Vincent de Jesus
| cinematography = Elmer Despa Wenn Deramas
| writer         = Pablo S. Gomez
| editing        = Marya Ignacio	 Gloria Romero Candy Pangilinan Sam Pinto Tom Rodriguez DJ Durano Eagle Riggs
| distributor    = Viva Films
| released       =   
| country        = Philippines
| language       =  
| budget         =
| gross          = PHP 115,494,375
}}
Petrang Kabayo (lit. Petra the Horse) is a 2010 Filipino comedy-drama film produced and released by Viva Films.  It is a remake of Petrang Kabayo at ang Pilyang Kuting (1988). 

==Synopsis==
Peter is a boy who is always maltreated by his father because of his homosexuality. He decided to run away and was found by a wealthy lady, Doña Biday, and is adopted by her. But he becomes abusive of his newfound wealth, and after the death of Doña Biday he becomes more abusive of his wealth and mistreated most of his housemaids, employees, and friends. Because of that, he was given a curse that transforms him into a horse every time he gets angry, or does/says anything bad to others.

==Cast and characters==

===Main cast===
*Vice Ganda as Peter/Pedro/Voice of Petra
*Luis Manzano as Erickson
*Abby Bautista as Pauline Gloria Romero as Lola Idang
*Sam Pinto as Samantha
*Candy Pangilinan as Maita
*Eagle Riggs as Diobayo
*DJ Durano as Dickson

===Supporting cast===
*Joy Viado as Lina 
*Tom Rodriguez as Chito
*John Arcilla as Poldo
*Ricky Rivero as Geronino 
*Atak Arana as Dalvie 
*Nadine Lustre as Dina

===Special Participation===
*Anne Curtis as Kalesa Passenger
*Eugene Domingo    as Doña Biday 
*Gladys Reyes as Pregnant Kalesa Passenger
*John Lapus as Filipino/Chinese Matron
*Dennis Padilla as Marjoroi
*Makisig Morales as Young Peter
*Jason Francisco as Waiter

==Other Appearances of the Cast/s==
Ericson (Luis Manzano) and the Horse Petra appeared in The Unkabogable Praybeyt Benjamin where Vice Ganda is also the lead role.

==Home video release== Viva Video released DVD and VCD formats on December 15, 2010.

==References==
 

==External links==
*   in Internet Movie Database

 
 
 