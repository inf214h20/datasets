Annan (film)
{{Infobox film
| name           = Annan
| image          = Annan DVD cover.jpg
| image_size     =
| caption        = DVD cover Anu Mohan
| producer       = T. Seenuvasan
| writer         = Anu Mohan
| starring       =  
| music          = Ilaiyaraaja
| cinematography = T. Damodaran
| editing        = B. Krishnakumar
| distributor    =
| studio         = Myson Pictures
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 1999 Tamil Tamil drama Anu Mohan. Swathi in lead roles, with Manivannan, R. Sundarrajan (director)|R. Sundarrajan, newcomer Vaasan, newcomer Apoorva, Vadivukkarasi and Ponvannan playing supporting roles. The film, produced by T. Seenuvasan, had musical score by Ilaiyaraaja and was released on 29 March 1999.    

==Plot==

Velan (Ramarajan) is the manager of the village market and is a respected man. He loves dearly his only sister Lakshmi (Apoorva) who is the villages school teacher. Sundari (Swathi (actress)|Swathi) and her father (R. Sundarrajan (director)|R. Sundarrajan) come to Velans village. Velan and Sundari fall in love with each other whereas Lakshmi and the rural development officer Selvam (Vaasan) fall in love with each other. In the meantime, Velan and the village chief Rasappan (Manivannan) decide to arrange the marriage between Lakshmi and the wicked Rasappans son Manikkam (Ponvannan). One day, Selvam breaks the villages rule and he is beaten at the village court. Lakshmi finally reveals the love between them and they get married. Selvam is not a rural development officer but a police officer who urges to arrest Velan. In the past, Velan killed a local rowdy who was in fact Selvams father. Selvam arrests Velan and Velan is sent in jail. Manikkam becomes the new manager of the village market. Without sufficient evidences, the court releases Velan and he comes back to his village. Now, Velan has to face Selvam and Manikkam. What transpires later forms the crux of the story.

==Cast==

 
*Ramarajan as Velan Swathi as Sundari
*Manivannan as Rasappan
*R. Sundarrajan (director)|R. Sundarrajan
*Vaasan as Selvam
*Apoorva as Lakshmi
*Vadivukkarasi as Guruvamma, Selvams mother
*Ponvannan as Manikkam Pandu as Sangu
*Balu Anand
*Vishwanath
*Rajappa
*Rajendra Kumar
*Chinnakannu
*Kanthan
*K. Rameshraj
*R. S. Samiraj
*Padmanabhan
*Nadrayan
*Chatur Sethupathi
*Ashokrajan
*Master Jafar
*Baby Kavitha
*Chelladurai
*Kottai Perumal
*Kovai Senthil as Ayyakannu
*Anuja
*Kalpana
 

==Soundtrack==

{{Infobox album |  
| Name        = Annan
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1999
| Recorded    = 1999 Feature film soundtrack
| Length      = 28:57
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1999, features 6 tracks with lyrics written by Gangai Amaran, Muhammed Metha, Kamakodiyan and Arivumathi.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Kanmanikku Vazhthu ||  Bhavatharini || 4:23
|- 2 || Aalamarathu Kuyile || Ilaiyaraaja, Sujatha Mohan || 5:01
|- 3 || Vayasu Pulla Vayasu Pulla || Ilaiyaraaja, Sujatha Mohan || 5:07
|- 4 || Kanmanikku Vazhthu || Ilaiyaraaja || 4:23
|- 5 || Kutti Nalla Kutti || Arun Mozhi || 5:03
|- 6 || Otha Roopavukku Oru || Arun Mozhi, Sujatha Mohan || 5:00
|}

==References==
 

 
 
 
 
 