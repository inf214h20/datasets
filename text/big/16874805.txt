I Do (1921 film)
 
{{Infobox film
| name           = I Do
| image_size     =
| image          = I Do FilmPoster.jpeg
| caption        =
| director       = Hal Roach
| producer       = Hal Roach Sam Taylor
| narrator       =
| starring       = Harold Lloyd
| cinematography =
| editing        = Charles Bilkey
| distributor    = Pathé Exchange
| released       =  
| runtime        = 26 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 1921 short short comedy film featuring Harold Lloyd.    This short is notable for having a cartoon wedding in the first scene.

==Cast==
* Harold Lloyd - The Boy
* Mildred Davis - The Girl 
* Noah Young - The Agitation
* Jackie Morgan - The Disturbance
* Jackie Edwards - The Annoyance
* Irene De Voss

==Plot==
The Boy meets and marries The Girl.  A year later, the two walk down the street with a baby carriage carrying a bottle instead of a baby when they run into The Girls brother who asks the couple to do him a favor and babysit his children.  They accept and the remainder of the short consists of gags showcasing the difficulties of babysitting children.  At the very end, The Boy discovers some knitted baby clothes in a drawer (implying that The Girl is pregnant).

==See also==
* Harold Lloyd filmography
* Silent film

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 


 