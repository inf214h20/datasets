Insaniyat Ke Devta
{{Infobox film
| name           = Insaniyat Ke Devta
| image          = Insaniyat-Ke-Devta.jpg
| image_size     = 
| caption        = 
| director       = K. C. Bokadia
| producer       = I. A. Desai Salim Khan
| writer         = K. C. Bokadia Faiz Saleem Shanmugam Sundaram Iqbal Durrani
| narrator       = 
| starring       = Raaj Kumar Rajnikanth Vinod Khanna Jayapradha Manisha Koirala
| music          = Anand-Milind
| cinematography = Peter Pereira
| editing        = Govind Dalwadi
| distributor    = D.S Films
| released       = 1993
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 15 crores
}}
 Indian film directed by K. C. Bokadia.

==Cast==
* Rajnikant...The Boss
* Vinod Khanna...Balbir 
* Jayapradha...Paro 
* Raaj Kumar...Jailer Rana Pratap Singh 
* Manisha Koirala...Nisha (Ranjits daughter) 
* Shakti Kapoor...Thakur Shakti Singh 
* Vikas Anand...Principal 
* Rakesh Bedi...Dholakia 
* Avtar Gill...Assistant Jailer Choudhary
* Vivek Mushran

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Suno To Zara"
| Kumar Sanu, Sadhana Sargam
|-
| 2 
| "Mera Balma Thanedar"
| Shabbir Kumar, Uttara Kelkar
|-
| 3
| "Neend Nahi Aati"
| Udit Narayan, Sadhana Sargam
|-
| 4
| "Tan Bheeg Gaya"
| Udit Narayan, Kavita Krishnamurthy
|}

== External links ==
*  

 
 
 

 