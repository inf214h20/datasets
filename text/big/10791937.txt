Ninaithale Inikkum (1979 film)
{{Infobox film
| name = Ninaithale Inikkum
| image = Ninaithale Inikkum Poster.jpg
| caption = 
| director = K. Balachander   
| producer = R. Ventaraman
| story = Sujatha Rangarajan
| screenplay = K. Balachander
| starring = Kamal Hassan Rajinikanth Jayaprada
| music = M. S. Viswanathan
| cinematography = B. S. Lokanath
| editing = N. R. Kittu
| studio = Premalaya Productions
| distributor = Premalaya Productions
| released =  
| runtime = 141 minutes
| country = India
| language = Tamil
| budget =
| gross =
}}
 Geetha and Telugu as Andamaina Anubhavam.  A large part of the film was shot in Singapore. 

==Plot==
The film follows lead chandru (Kamal Hassan) who plays a singer and his band on a tour to Singapore. Over there Chandru meets his love interest Sona  (Jayaprada), only to find that she is terminally ill. The film ends on a tragic note with the ladys demise.

==Cast==
* Kamal Hassan as Chandru
* Rajinikanth as Deepak
* Jayaprada as Sona Geetha as the receptionist
* S. K. Misro as Kamalakar Rao
* Rajappa as Vichu
* Srinivas
* Jayasudha as Kamini (Guest appearance)
* Sarath Babu (Guest appearance) Narayana Rao (Guest appearance)
* S. Ve. Shekher (Guest appearance)

==Production== Geetha and Narayana Rao appear in cameo for their mentor. This was the debut film for actor S. V. Shekhar.  Jayasudhas sister Subhashini also appears.
The band in the movie and the music were inspired by the Beatles. 

The scene where Rajinikanth is challenged to flip his cigarette 10 times or lose a finger is based on Roald Dahls Man from the South.

==Soundtrack==
The soundtrack was composed by M. S. Viswanathan while the lyrics were written by Kannadasan. The song "Engeyum Eppothum" was one of the famous songs from the film. The song was remixed by Yogi B in Polladhavan (2007 film)|Pollathavan (2007).  The song "Sambo Siva Sambo" was remixed by Vijay Antony as "Avala Nambithan" for Salim (film)|Salim (2014). 

# Namma Ooru Sinkari - S. P. Balasubramaniam
# Nizhal Kandavan - S. P. Balasubramaniam
# Ninaiththaale Inikkum - S. P. Balasubramaniam, S. Janaki
# Palavanna - S. P. Balasubramaniam
# Aananda Thaandavamo  - L. R. Eswari
# Barathi Kannamma  - S. P. Balasubramaniam, Vani Jayaram
# Inimai Nirainda Ulagam  - S. P. Balasubramaniam, L. R. Eswari
# Kaaththirunthen  - S. P. Balasubramaniam
# Sambo Sivasambo  - M. S. Viswanathan
# Thattiketka Aalillai  - S. P. Balasubramaniam
# Yaathum Oore  - S. P. Balasubramaniam, P. Susheela
# Engeyum Eppothum  - S. P. Balasubramaniam

==Legacy== Ninaithale Inikkum, starring Prithviraj Sukumaran, Sakthi Vasu and Priyamani was released. G. N. R. Kumaravelan who directed the film, stated that the title was "right for my film on students and the college scenario". 

Abaswaram Ramji conducted a stage show called Ninaithale Inikkum in 2006.  A stage play called "Sambo Siva sambo" named after the films song was conducted by Theatre of Maham. M. S. Viswanathan composed the background music for the play. 

==Re-release==
A digitally restored version of the film was released on 4 October 2013. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 