Spotswood (film)
 
 

{{Infobox Film
|  name           = Spotswood
|  image          = Spotswood_movieposter.jpg Andrew Knight
|  starring       = Anthony Hopkins, Ben Mendelsohn, Alwyn Kurts, Bruno Lawrence
|  director       = Mark Joffe
|  producer       =
|  distributor    =
|  editing        = Offshoot Films
|  released       =   23 January 1992
|  runtime        = 95 minutes
|  country        = Australia
|  language       = English
|  music          = Ricky Fataar
|  budget         = A$3.4 million 
|  gross          = A$1,505,884 (Australia)
}}
 comedy drama film directed by Mark Joffe, made in 1990-1991, released in 1992 in some locations; also known as The Efficiency Expert in United States|America.

The film is a favourite of Rupert Murdochs. 

==Plot==
Late 1960s Melbourne. Errol Wallace (Anthony Hopkins) has a direction in life as an independent financial performance of businesses consultant hired by the board of Durmack, an automotive component manufacturer, and recommends a large work force redundancy.

Balls, a moccasin factory located in the Melbourne suburb of Spotswood, is his next client. Mr. Ball (Alwyn Kurts), the owner of the company, is affable and treats his employees benevolently. Wallace on a factory tour finds the conditions wanting with shabbiness, old machinery and the workers lackadaisical.

The young Balls worker Carey (Ben Mendelsohn) finding his place in the world and life is asked by Wallace to be part of the review compiling worker condition and performance information for Wallace. Carey is reluctant until he learns Mr. Ball’s daughter Cheryl (Rebecca Rigg), with whom he fancies, is part of the review staff.

Wallace learns that there is an instigator in the midst, his colleague Jerry (John Walton) that leaks the Durmack report so that the workers union will not object to Wallaces recommendation. And, Jerry showed his ruthlessness by inflating the quantity of sackings as a means to demoralize the union.

Kim (Russell Crowe), a salesman at Balls, shows his ruthlessness and ulterior plans when he comes to Wallaces home one night with a complete set of the company financial records that detail non-existent profit for years and Ball selling off company assets to keep the outfit afloat.

Wallace realizes what productivity improvements have been implemented are not enough to save the dire company let along the elimination of sacked workers. Mr. Ball responds, "It’s not just about dollars and cents. It’s about dignity, treating people with respect.”

Wallaces mind set starts to change when his car is vandalized, breaks down and when some Ball workers come to his aid start to include him in their off-hours activities. Mr. Ball announces the work force redundancies and Wallace is clearly uncomfortable seeing them, knowing that it was his recommendation that sealed their fate.

The union at Durmack capitulates and management celebrates with a party at which Wallace becomes disenchanted by rash sackings and realises that product diversity can potentially make the company profitable since the skills set is in the workers.

Carey realises he has feelings for his work mate and friend Wendy (Toni Collette) and together they climb up onto the roof of the factory and hold hands as they look out over Spotswood.

==Cast==
* Anthony Hopkins (Errol Wallace)
* Ben Mendelsohn (Carey)
* Alwyn Kurts (Mr. Ball)
* Bruno Lawrence (Robert, Careys Father)
* John Walton (Jerry Finn)
* Rebecca Rigg (Cheryl Ball)
* Toni Collette (Wendy Robinson)
* Russell Crowe (Kim Barry)
* Angela Punch McGregor (Caroline Wallace)
* Daniel Wyllie (Frank Fletcher)
* John Flaus (Gordon)
* Gary Adams (Kevin)
* Jeff Truman (Ron)
* Toni Lamond (Mrs. Lorna Ball)
* Jill Murray (Ophelia, Careys Mum; as Jillian Murray)

==Box Office==
Spotswood grossed $1,505,884 at the box office in Australia,  which is equivalent to $2,348,887 in 2009 dollars.

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 