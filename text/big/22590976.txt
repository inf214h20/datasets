Do You Hear the Dogs Barking?
 
{{Infobox film
| name           = Do You Hear the Dogs Barking?/¿No oyes ladrar los perros?
| image          = Do You Hear the Dogs Barking?.jpg
| caption        = Film poster
| director       = François Reichenbach
| producer       = Georges Bacri Leopoldo Silva
| writer         = Carlos Fuentes François Reichenbach Salvador Gomez Ahui Camacho
| music          = Vangelis
| cinematography = Rosalío Solano
| editing        = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
Do You Hear the Dogs Barking? ( , and also known as Ignacio) is a 1975 Mexican drama film directed by François Reichenbach. It was entered into the 1975 Cannes Film Festival.   

The film is based on a short story, El Llano en llamas#¿No oyes ladrar los perros? (Dont you hear the dogs barking?)|¿No oyes ladrar los perros?, written by Juan Rulfo and collected in El Llano en llamas. The short story tells the tale of an old man carrying his wounded (criminal) son on his back in search of help. The film has a younger father carrying his sick child on his back looking for help. Meanwhile he tells his son about what his future life will be like. The film intercuts between the story of the man and his child and the possible future of the child as a young indigenous man looking for work in Mexico City.

==Cast==
* Ahui Camacho as young Ignacio
* Aurora Clavel
* Ana De Sade
* Tamara Garina Salvador Gómez as adolescent Ignacio?
* Juan Ángel Martínez
* Gastón Melo
* Patrick Penn Salvador Sánchez as El Padre de Ignacio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 