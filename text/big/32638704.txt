List of horror films of 1961
 
 
A list of horror films released in 1961 in film|1961.

{| class="wikitable" 1961
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" | Cast
! scope="col" | Country
! scope="col" | Notes
|-
!   | The Beast of Yucca Flats
| Coleman Francis || Douglas Mellor, Larry Aten, Barbara Francis ||   ||  
|-
!   | The Brainiac Abel Salazar, Ariadne Welter, David Silvia ||   ||  
|-
!   | Creature from the Haunted Sea
| Roger Corman, Monte Hellman || Roger Corman, Edward Wain, E. R. Alvarez ||   ||  
|- The Curse of the Crying Woman Abel Salazar, Rita Macedo ||   ||  
|-
!   | The Curse of the Werewolf
| Terence Fisher || Clifford Evans, Oliver Reed, Yvonne Romain ||    ||  
|-
!   | Doctor Bloods Coffin Ian Hunter, Kieron Moore ||    ||  
|-
!   | Homicidal (film)|Homicidal
| William Castle || Glenn Corbett, Patricia Breslin, Jean Arless ||   ||  
|- The Innocents
| Jack Clayton || Deborah Kerr, Megs Jenkins, Pamela Franklin ||   ||  
|-
!   | Konga (film)|Konga
| John Lemont || Michael Gough, Margo Johns, Jess Conrad ||    ||  
|-
!   | Lycanthropus
| Paolo Heusch || Barbara Lass, Carl Schell, Curt Lowens ||    ||  
|-
!   | Maciste in the Land of the Cyclops
| Antonio Leonviola || Gordon Mitchell, Chelo Alonso, Vira Silenti ||   ||  
|- The Mask Paul Stevens, Claudette Nevins, Anne Collings ||   ||  
|-
!   | Mr. Sardonicus
| William Castle || Guy Rolfe, Audrey Dalton, Oscar Homolka ||   ||  
|- The Pit and the Pendulum John Kerr, Barbara Steele ||   ||  
|-
!   | Shadow of the Cat William Lucas ||    ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
 

 
 
 

 
 
 
 