Angulimala (2003 film)
{{Infobox film
| name           = Angulimala
| image          = Angulimala movie poster.jpg
| caption        = The Thai movie poster.
| director       = Sutape Tunnirut
| producer       = Adirek Watleela
| writer         = Sutape Tunnirut
| starring       = Peter Noppachai Jayanama Stella Malucchi
| music          =
| cinematography =
| editing        = Film Bangkok
| released       =  
| runtime        = 105 minutes
| country        = Thailand
| language       = Thai
| budget         =
}}
Angulimala ( ;  ) is a 2003 Thai  .  , The Nation, March 20, 2003 (retrieved on January 21, 2007). 

==Plot==
Born a Brahmin, Ahimsaka is studying under a guru when he sees a woman named Nantha attempting to commit suicide by jumping off a cliff. He saves her, but later learns that Nantha was intended to be the bride of his teacher.
 enlightenment is garland of fingers." He struggles to find 1,000 victims, so he resorts to killing all who cross his path. Nantha, wanting to stop the killing, tries to kill Ahimsaka, but only succeeds in killing herself.
 Buddha himself, Buddhist monk.

==Cast==
* "Peter" Noppachai Jayanama as Ahimsa|Ahimisaka/Angulimala
* Stella Malucchi as Nantha
* John Rattanaveroj as Vikul
* Kamron Gunatilaka as Sati
* Alisa Kajornchaiyakul as Mantanee
* Catherina Grosse		
* Varin Sachdev		
* Anant Sammarsup as Kakkapakawa

==Reception==
Religious leaders and  , who demanded the film be re-edited for the concerns of the public in Thailand most of which are Buddhist and not Christians.   , The Nation, October 14, 2003 (retrieved on January 21, 2007). 

Some violent scenes were removed and the films tagline was changed. It was then viewed by revered monk Phra Phisan Dhammavadhi, who had earlier called for the film to be banned, and found acceptable.  ,The Nation, April 1, 2003 (retrieved on January 21, 2007).   , The Nation, April 3, 2003 (retrieved on January 21, 2007). 

The film had poor box-office reception in Thailand, which critics blamed on the poor show and violent and negative depiction of the film.  , The Nation, March 21, 2003 (retrieved on January 21, 2007).   , The Nation, July 3, 2003 (retrieved on January 21, 2007). 

Nonetheless, Angulimala won at the Thailand National Film Association Awards for best costumes, best visual effects and best supporting actress for Alisa Kajornchaiyakul. It was screened at the 26th Moscow International Film Festival in 2004, where it was in competition for a Golden St. George.    It also screened at the Asian Film Festival of Paris in 2004.  , The Nation, February 26, 2004 (retrieved on January 21, 2007). 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 