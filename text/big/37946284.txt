The Further Adventures of the Flag Lieutenant
 
 
{{Infobox film
| name           = The Further Adventures of the Flag Lieutenant
| image          =
| caption        =
| director       = W.P. Kellino
| producer       = Julius Hagen
| writer         = W.P. Drury (story)   George A. Cooper Henry Edwards Isabel Jeans Lilian Oldland   Lyn Harding
| music          =
| cinematography = Horace Wheddon
| editing        = 
| studio         = Julius Hagen Productions
| distributor    = Williams and Pritchard Films
| released       = 4 November 1927
| runtime        = 8,500 feet 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent action Henry Edwards, Isabel Jeans and Lilian Oldland. 

==Production== independent producer The Flag The Flag Lieutenant, was released in 1932.

==Cast==
* Henry Edwards (actor)| Henry Edwards as Lieutenant Dicky Lascelles 
* Isabel Jeans as Pauline 
* Lilian Oldland as Sybil Wynne 
* Lyn Harding as The Sinister Influence 
* Fewlass Llewellyn as Admiral Wynne 
* Fred Raynham as Colonel William Thesiger 
* Albert Egbert as Bill 
* Seth Egbert as Walter 
* Vivian Baron as Ah Loom 
* Roy Kellino as A youth

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918–1929. George Allen & Unwin, 1971.
* Wood, Linda. British Films 1927–1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 
 

 