Nyayavidhi
{{Infobox film
| name           = Nyayavidhi
| image          = Nyayavidhifilm.png
| caption        = Poster designed by Gayathri Ashokan
| director       = Joshiy
| producer       = Joy Thomas
| writer         = Chalil Jacob Dennis Joseph (dialogues)
| screenplay     = Dennis Joseph
| starring       = Mammootty Shobhana Sukumaran Lalu Alex
| music          = M. K. Arjunan
| cinematography = Jayanan Vincent
| editing        = K Sankunni
| studio         = Jubilee Productions
| distributor    = Jubilee Productions
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by Joshiy and produced by Joy Thomas. The film stars Mammootty, Shobhana, Sukumaran and Lalu Alex in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Mammootty as Paramu
*Shobhana as Geetha
*Lalu Alex as Kavil Johnie
*Sukumaran as McPherson
*K. P. A. C. Azeez as Unnithan
*K. P. A. C. Sunny Innocent as Channar
*Paravoor Bharathan as Kochettan
*Mala Aravindan as Mathunni
*Adoor Bhavani as Kochanna
*Kundara Johny as Vareeth Kunchan as Sadaraman
*V. D. Rajappan as David Sulakshana
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Shibu Chakravarthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chellacheru veedu tharam || KS Chithra || Shibu Chakravarthy || 
|-
| 2 || Chelulla Malankurava || KS Chithra, Chorus || Shibu Chakravarthy || 
|-
| 3 || Etho yakshikkadha || Unni Menon || Shibu Chakravarthy || 
|}

==References==
 

==External links==
*  

 
 
 

 