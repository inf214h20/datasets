One Man Band (unfinished film)
 
{{Infobox film
| name           = One Man Band aka London aka Swinging London aka Orsons Bag
| image          =
| writer         = Orson Welles Charles Gray Jonathan Lynn Oja Kodar
| director       = Orson Welles
| cinematography = Gary Graver Tomislav Pinter Ivica Rajkovic Giorgio Tonti
| distributor    =
| runtime        = 29 min
| country        = United States
| budget         =
}} The Merchant of Venice, and assorted sketches around Europe. This was abandoned in 1969 when CBS withdrew their funding over Welles long-running disputes with US authorities regarding his tax status, he continued to fashion the footage in his own style.

==Segments==
The film consists of five segments, all of them comic: Churchill, Swinging London, Four Clubmen, Stately Homes and  Tailors.

===1. Churchill===
The first segment is filmed entirely in silhouette. Welles plays Winston Churchill fielding press questions and then exchanging bon mots with Nancy Astor (Oja Kodar). Each line spoken by Churchill is a well-known witticism commonly attributed to him.

===2. Swinging London===
The longest segment of the film (9 mins) has a bowler-hatted reporter ( , an old lady selling   DVD of F for Fake.

===3. Four Clubmen=== London club in which Welles plays four different members, all under heavy make-up, plus a noisy waiter who drops things.

===4. Stately Homes===
This segment features Welles as a journalist wandering around the stately home of Lord Plumfield. Welles also plays the eccentric, penniless Lord Plumfield. Tim Brooke-Taylor has a cameo as Plumfields son.

===5. Tailors=== Charles Gray and Jonathan Lynn) who are dismissive of his ignorance of tailoring, and keep mocking his weight.

==Production==
The bulk of filming was completed in 1968-9, although assorted linking narrations and inserts were filmed by Welles in 1971. The 1971 sequences are noticeable because Welles had grown a long beard by then, whereas he was clean-shaven in all the parts he played in 1968-9. The film was left unfinished in Welles lifetime.
 Munich Film Museum for preservation and restoration. In 1999 the Munich Film Museum then edited together the complete footage into a 29-minute cut, which has subsequently been screened at numerous film festivals. 

The full restored footage has never been released on video or DVD, although an unrestored print is used in Vassili Slovics 1995 documentary Orson Welles: the One Man Band, which includes the segments Churchill, Stately Homes and Tailors in their entirety, as well as clips from Four Clubmen and One Man Band.

The non-London sketch segments of Orsons Bag were edited by the Munich Film Museum into the 9 minute short Vienna (film)|Vienna.

== Cast ==
*Orson Welles as Presenter / Winston Churchill / One-man band / Policeman / Morris dancer / Old lady / Chinaman / Four clubmen / Waiter / Journalist / Lord Plumfield / American tourist
*Tim Brooke-Taylor as Reporter / Young aristocrat Charles Gray as Tailor
*Jonathan Lynn as Tailors Assistant
*Oja Kodar as Nancy Astor

==References==
 

== External links ==
*  
*   on British Film Institute

 
 

 
 
 
 
 