Stammheim (film)
{{Infobox film
| name           = Stammheim - Die Baader-Meinhof-Gruppe vor Gericht
| image          = Stammheim.jpg
| caption        = 
| director       = Reinhard Hauff
| producer       = 
| writer         = Stefan Aust
| starring       = Ulrich Pleitgen Ulrich Tukur Therese Affolter
| music          = Marcel Wengler
| cinematography = 
| distributor    =  
| released       =  
| runtime        = 107 minutes
| country        = West Germany
| awards         =
| language       = German
| budget         = 
}} 1986 Cinema West German film directed by Reinhard Hauff. It tells the story of the trial in the court of Stammheim Prison of the Baader-Meinhof Group of left-wing terrorists.

==Selected cast==
*Ulrich Pleitgen as Presiding Judge
*Ulrich Tukur as Andreas Baader
*Therese Affolter as Ulrike Meinhof
*Sabine Wegner as Gudrun Ensslin
*Hans Kremer as Jan-Carl Raspe

==Awards==
The film won the FIPRESCI Prize and the Golden Bear at the 36th Berlin International Film Festival in 1986.   

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 