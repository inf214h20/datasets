The Moon-Spinners
{{Infobox film
| name         = The Moon-Spinners
| image        = The Moon-Spinners (theatrical poster).jpg
| caption      = Theatrical release poster Mary Stewart Screenplay: Michael Dyne
| starring     = Hayley Mills Eli Wallach Peter McEnery James Neilson
| producer     = Walt Disney
| photography  = Paul Beeson
| music        = Ron Grainer Walt Disney Productions Buena Vista Distribution
| released     =  
| runtime      = 118 minutes
| language     = English
| gross        = est. $3,500,000 (US/ Canada) 
}} Walt Disney Mary Stewart James Neilson. The Moon-Spinners was Mills fifth of six films for Disney, and featured the legendary silent film actress Pola Negri in her final screen performance.

==Plot== folk musicologist aunt, Frances (Joan Greenwood), to a small coastal inn on the Greek island of Crete.

Owner Sophia (Irene Papas) refuses to allow them to stay at her inn, The Moon-Spinners, but Aunt Frances and Sophias teenage son Alexis (Michael Davis) persuades her into changing her mind. Whilst Nikky and Aunt Frances are in their room, Sophias brother Stratos (Eli Wallach) demands to know why they chose to stay at his sisters inn and says they should leave, but Aunt Frances insists on staying. Stratos reluctantly agrees to allow them to stay for one night.

During a wedding party at the inn later that evening, Nikky meets a stranger named Mark (Peter McEnery), who invites her and Aunt Frances to have a meal with him. They accept. Their dinner meeting attracts Stratoss suspicious stare, which Nikky notices and points out to Mark. Mark hints theres more to Stratos than appears. At end of the evening, Mark suggests that he and Nikky could meet in the morning to go for a swim in the Bay of Dolphins. Nikky agrees. She comes downstairs the next morning, and quickly learns that Mark has checked out of the inn.

During a walk on the island, she stumbles across Mark whos been shot.

And so her adventure begins, which will lead her to a mysterious wealthy woman, played by former silent film star Pola Negri in her final film role.

==Cast==
* Hayley Mills as Nikky Ferris
* Eli Wallach as Stratos
* Peter McEnery as Mark Camford
* Joan Greenwood as Frances Ferris
* Michael Davis as Alexis
* Pola Negri as Madame Habib
* Irene Papas as Sophia

==Production== Treasure Island. It was Walt Disneys penultimate live-action film in which he was credited as producer while alive.

Disney convinced silent film actress  .

==References==
 

==External links==
*  
*  
*  

==See also==
 
*Hayley Mills
*Eli Wallach
*Pola Negri

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 