Men in White (1934 film)
{{Infobox film
| name           = Men in White
| image          = Men in white poster 1934.jpg
| image_size     = 210 px
| alt            = 
| caption        = original film poster
| director       = Ryszard Bolesławski
| producer       = Monta Bell
| writer         = Sidney Kingsley
| narrator       = 
| starring       = Clark Gable   Myrna Loy
| music          = William Axt
| cinematography = George J. Folsey
| editing        = Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = 
| released       =  
| runtime        = 74 minutes United States
| language       = English
| budget         = $213,000  . 
| gross          = $1,455,000 
| preceded_by    = 
| followed_by    = 
}}
Men in White (1934) is a Pre-Code film starring Clark Gable and Myrna Loy, and directed by Ryszard Bolesławski. Because of the suggested illicit romance and the suggested abortion in the movie, it was frequently cut. The Legion of Decency cited the movie as unfit for public exhibition.

== Plot == play of the same name:  
a dedicated young doctor places his patients above everyone else in his life. Unfortunately, his Social Register fianceé cant accept the fact that he considers an appointment in the operating room more important than attending a cocktail party. He soon drifts into an affair with a pretty nurse who shares his passion for healing.

One thread of the story involves diabetic hypoglycemia: 
:In this story two doctors have a conflict at the bedside of a young girl who is desperately ill. The young doctor diagnoses (correctly) that the patient is in insulin shock (needing glucose), while the senior doctor insists that it is  a diabetic coma (needing insulin)...Fortunately, the doctor making the right diagnosis prevails: The child recovers and smiles up at him. Cut.

== Cast ==
* Clark Gable as Dr. George Ferguson
* Myrna Loy as Laura Hudson
* Jean Hersholt as Dr. Hockie Hochberg
* Elizabeth Allan as Barbara Denham
* Otto Kruger as Dr. Levine
* C. Henry Gordon as Dr. Cunningham
* Russell Hardie as Dr. Mike Michaelson
* Wallace Ford as Shorty
* Henry B. Walthall as Dr. McCabe
* Russell Hopton as Dr. Pete Bradley
* Samuel S. Hinds as Dr. Gordon
* Frank Puglia as Dr. Vitale
* Leo Chalzel as Dr. Wren Donald Douglas as Mac
* Ruth Channing (uncredited)
* Isabel Jewell (scenes deleted; uncredited)
* Edward J. Nugent (scenes deleted; uncredited)
* Frank Reicher (scenes deleted; uncredited)

==Reception==
The film was very successful at the box office.  According to MGM records it earned $890,000 in the US and Canada and $565,000 elsewhere resulting in a profit of $784,000. 

==References==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 