La Chambre obscure
{{Infobox film
| name           = La Chambre obscure
| image          = La Chambre obscure poster.jpg
| image_size     = 
| caption        = 
| director       = Marie-Christine Questerbert
| producer       = Raymond Blumenthal  Jimmy de Brabant (associate producer)  Sylvain Bursztejn (delegate producer)
| writer         = Marie-Christine Questerbert
| narrator       = 
| starring       = Caroline Ducey Melvil Poupaud Mathieu Demy Sylvie Testud Jackie Berroyer Edith Scob
| music          = 
| cinematography = Emmanuel Machuel
| editing        = Catherine Quesemand
| studio         = Blue Films  Canal+   Centre National de la Cinématographie (CNC) Delux Productions Eurimages Fonds National de Soutien à la Production Audiovisuelle du Luxembourg Gam Films Parnasse International
| distributor    = Les Films du Paradoxe
| released       = 29 November 2000
| runtime        = 107 min
| country        = France / Luxembourg / Canada / Italy 
| language       = French
| budget         = 
| gross          = 
}} 2000 French drama film directed and written by Marie-Christine Questerbert.

==Cast==

*Caroline Ducey as  Aliénor 
*Melvil Poupaud as  Bertrand 
*Mathieu Demy as  Thomas 
*Sylvie Testud as  Azalaïs 
*Jackie Berroyer as  The king 
*Hugues Quester as  Ambrogio 
*Alice Houri as  Lisotta 
*Pierre Baillot as  Maître Gérard de Narbonne 
*Dimitri Rataud as  Marc 
*Christian Cloarec as  Guillaume 
*Edith Scob as  The widow 
*Luis Rego as  The confessor

==External links==
* 
* 

 
 
 

 