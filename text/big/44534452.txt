Father, Son, and Holy War
{{Infobox film
| name = Father, Son, and Holy War
| image = Poster FatherSon Holy War.jpg
| caption = Film poster
| alt = A gigantic red-coloured man with six arms. Upper two arms flexing muscles. Rifle on the middle right hand. Sword on the lower right hand. Staff on the middle left hand. Decapitated head of a man on the lower left hand. Burning houses on the background. Front of the gigantic man is a campfire burning a mourning woman and a dead man.
| film name =  
| director = Anand Patwardhan
| producer = Anand Patwardhan
| writer =
| music = Navnirman, Vinay Mahajan
| sound = Simantini Dhuru, Narinder Singh, Sanjiv Shah
| cinematography =
| editing =
| distributor =
| released = 1995
| runtime = 120 minutes
| country = India
| language = English, Hindi
| budget =
}} Supreme court. The film received numerous national and international awards, and was also seen positively by critics.

== Synopsis ==

=== Part 1: Trial by Fire ===

The title of the first section is a reference to the ordeal that the Hindu god-king Rama used to test the fidelity of his wife after rescuing her from the demon king Ravana. The segment describes the various interconnected instances of communal violence in India in the years prior to the film. The film opens with the aftermath of the Bombay riots|anti-Muslim riots in Bombay that followed the demolition of the Babri Masjid in December 1992. Several Hindu youth are heard speaking to the cameraman, saying that they had enjoyed the killing and looting, and that a list of Muslim individuals had been prepared beforehand, and that some authority figures knew of the plans to target Muslims.   

The film then describes a connection between the Indian nationalist movement and violent masculinity. In a voice-over, Patwardhan states that as a result of the British Raj stereotypes of "effeminate" Hindus and "martial" non-Hindu communities, the nationalist movement turned to militant symbols like Shivaji and Rama.  This led to an identification of Hinduism with the traditions of communities with more militant traditions, such as the Rajputs and the Marathas, which included practices like Sati (practice)|sati.  This leads to a description of the murder of Roop Kanwar in Deorala, Rajasthan, in 1987. Kanwar was forced to immolate herself on the funeral pyre of her husband, supposedly in keeping with the tradition of Sati (practice)|sati,    a practice that had been illegal since 1830.   
 secular leaders misogynistic language Sikhs demonstrating in favor of Khalistan, and the Fatwa issues against Salman Rushdie. 

=== Part 2: Hero Pharmacy ===
 phallic in nature. Many political leaders are heard linking non-violence and secularism to weakness and impotency.  A religious leader campaigning for the Shiv Sena in Gujarat is seen asking Hindu women to have eight children apiece, as a means of combating the perceived menace of Muslims. 
 WWE wrestling, and at reasons for their popularity.  Examples of young children from different class backgrounds are shown exhibiting behavior that idolizes violence. Upper class children are shown mobbing professional wrestler Randy Savage, more commonly known by his stage name of "Macho Man", while young male members of the Shiv Sena, from less wealthy backgrounds, are shown engaged in street-fighting. Several young men are heard off-screen describing how watching rape in movies was "fun", and discussing the possibility of gang-raping a woman that they are not acquainted with. 

== Reception ==
 Indian Supreme Court, which ruled in Patwardhans favor in 2006, ordering that the film be screened without any cuts within eight weeks.  The judges observed that "This documentary film   showcases a real picture of crime and violence against women and members of various religious groups perpetrated by politically motivated leaders for political, social and personal gains."  The film was eventually screened following the ruling.   

The film has a rating of 8.7 on the Internet Movie Database.    History professor Vinay Lal, writing in the European art journal Third Text, stated that Father, Son, and Holy War was a nuanced and daring film, that examined the "nexus between communalism, the changing culture of the contemporary Hindi film, violence towards women in many domains of Indian society, vernacular forms of masculinity, and other aspects of Indian society and culture." communalism and the sexual aspects of its ideology.  Gail Minault, reviewing the film for the Journal of South Asian Studies, wrote that the film was "powerful" and "harrowing." 

== Awards ==
 National Film Best Investigative Documentary, India, 1995    Best Film on Social Issues, India, 1995 
* Special Jury Prize, Yamagata International Documentary Film Festival, Japan, 1995 
* In The Spirit of Freedom Award, Jerusalem International Film Festival, Israel, 1995   
* Special Jury Prize, Vancouver International Film Festival, 1995 Bombay International Film Festival, 1996  Sheffield International Documentary Festival, 2012   

== References ==
 

== External links ==

*  

 
 
 
 