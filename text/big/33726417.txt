The Third Half
{{Infobox film
| name = The Third Half
| image = The Third Half.jpg
| caption = 
| director = Darko Mitrevski
| producer = Robert Naskov and Darko Mitrevski
| screenplay = Grgur Strujic and Darko Mitrevski
| story = 
| based on = World War 2 events
| starring = Sasko Kocev Katarina Ivanovska Richard Sammel Rade Sherbedgia Emil Ruben Mitko S. Apostolovski
| music = Kiril Džajkovski
| cinematography = Klaus Fuxjager
| editing = Dejan Boskovic
| studio = Kino Oko Production
| distributor = 
| released =  
| runtime = 113 minutes
| country = Macedonia
| language = {{Plainlist| Macedonian
*German German
*Bulgarian Bulgarian
*Serbian Serbian
*Judaeo-Spanish|Ladino English
}}
| budget = € 2.500.000
| gross = 
}}
The Third Half (   ) is a Macedonian-Czech-Serbian film that deals with Macedonian football during World War II, and the deportation of Jews from Macedonia. It is a story of love during wartime and a countrys passion for soccer.  The government of Macedonia considered the movie of national interest and funded it with one million euros. 
 Bulgarian occupation zone of Macedonia during World War II. 

==Plot==
 
 Macedonian Jews who were deported to the gas chambers of Treblinka by the Bulgarian administrative and military authorities, who were cooperating with the Nazi regime.  In 1941, a young Eastern Orthodox man, Kosta, and a wealthy young Jewish woman, Rebecca, fall in love, despite her fathers effort to keep them apart. With the war raging around their borders, the Macedonians remain cocooned in their world of patriotic pleasures, primarily concerned about getting the beleaguered Macedonia Football Club on a winning streak. Their manager hires the legendary German-Jewish coach Rudolph Spitz to turn them into champions. But when the Nazi occupation begins and they start deporting Jews, Kosta and his teammates realize that the carefree days of their youth are over. As the Nazis try to sabotage the outcome of the championship game, and Spitzs life is threatened, Kosta and his teammates rise to the challenge to protect their coach, with all of Macedonia cheering them on. 

==Cast==
*Sasko Kocev as Kosta
*Katarina Ivanovska as Rebecca Cohen
**Bedija Begovska as Rebecca in 2012
*Richard Sammel as Rudolph Spitz, a Prussian footballer-turned-coach hired to coach FC Macedonia
*Rade Šerbedžija as Don Rafael Cohen, a wealthy Jewish banker and Rebeccas father
*Emil Ruben as Garvanov, a Bulgarian colonel
*Mitko S. Apostolovski as Dimitrija, the owner of FC Macedonia
*Toni Mihajlovski as Pancho
*Igor Angelov as Afrika
*Gorast Cvetkovski as Skeptic
*Oliver Mitkovski as Jordan
*Igor Stojchevski as Cezar
*Dimitrija Doksevski as Gengys
*Bajram Severdzan as Choro
*Whitney Montgomery as Rachel, Rebeccas granddaughter
*Zvezda Angelovska as Blagunja, Panchos wife
*Verica Nedeska as Zamila, Rebeccas friend
*Petre Arsovski as Papas, Dimitrijas friend
*Meto Jovanovski as a rabbi
*Salaetin Bilal as a shoemaker
*Petar Mircevski as a barber

==Production==
The film was directed by Darko Mitrevski and supported by the Macedonian Film Fund, the Holocaust Fund of the Jews from Macedonia, The Jewish Community of Macedonia and the Czech State Fund. It was declared a film of a national interest by the Macedonian Government.  The film was shot in Skopje, Bitola and Ohrid.  Filming took place between September 10  and October 27, 2011.  

==Reception==
 
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final cut for nomination.   

==Controversy== members of the European Parliament—expressed outrage over the film and called upon European Commissioner for Enlargement Štefan Füle to reprove the Republic of Macedonia over the film. They claimed the film was an "attempt to manipulate Balkan history" and "spread hate" on the part of the Republic of Macedonia against its neighbours.  The director of the film denied the accusations; he and the film crew described the objections to the film as an example of Holocaust denial. 

In late November 2011, the Macedonian media alleged that European MP Doris Pack dismissed the Bulgarian politicians criticism of the film.   Subsequently, in an extraordinary meeting of the EU Committee on Foreign Affairs, which was attended by the Minister of Foreign Affairs of the Republic of Macedonia, Doris Pack, denied this allegation.  

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Macedonian submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  
* Empty Boxcars (2011) Documentary  *  at IMDb   link Vimeo

 
 
 
 
 
 
 
 
 
 
 
 
 