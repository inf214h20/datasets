Pasó en mi barrio
{{Infobox film
| name           = Pasó en mi barrio
| image          =Pasó en mi barrio.jpg
| image_size     =
| caption        =
| director       = Mario Soffici
| producer       = Eduardo Bedoya Héctor Olivera
| writer         = Carlos A. Olivari Sixto Pondal Ríos
| narrator       =
| starring       = Tita Merello
| music          =
| cinematography = Francis Boeniger
| editing        = Ricardo Rodríguez Nistal Atilio Rinaldi
| distributor    =
| released       = 20 December, 1951
| runtime        = 78 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
}} Argentine film directed by Mario Soffici. It was entered into the 1952 Cannes Film Festival.    

==Cast==
* Tita Merello
* Mario Fortuna
* Mirtha Torres
* Alberto de Mendoza
* Daniel Tedeschi
* Benito Cibrián
* Sergio Renán
* Paride Grandi
* Francisco Audenino
* Carlos Cotto
* Carmen Giménez
* Tito Grassi - El Negro
* Eduardo de Labar
* Hugo Lanzilotta
* Domingo Mania
* Luis Medina Castro - Carozo
* Fausto Padín - Taxista
* Juan Carlos Palma - Ricardo
* Alberto Quiles
* Hilda Rey
* Walter Reyna
* Manolita Serra
* Vicente Thomas

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 

 