Escape from the Shadows (film)
{{Infobox film
| name           = Escape from the Shadows
| image          = 
| caption        = 
| director       = Jiří Sequens
| producer       = 
| writer         = Bedrich Kubala Jiří Sequens
| starring       = František Smolík
| music          = 
| cinematography = Alois Jirácek
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Escape from the Shadows ( ) is a 1959 Czech drama film directed by Jiří Sequens.  It was entered into the 1st Moscow International Film Festival where it won a Golden Medal.   

==Cast==
* František Smolík
* Lída Vendlová
* Stanislav Remunda
* Josef Bek
* Renata Olarova
* Milena Asmanová
* Jaroslava Adamová as Irena
* Bohus Smutný
* Ruzena Lysenková
* Oldrich Vykypel
* Josef Kemr

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 