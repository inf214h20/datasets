Gamperaliya (film)
{{Infobox film
 | name = Gamperaliya (Changement au Village) ගමිඦපරලිය
 | image = DVD cover of "Gamperaliya".jpg
 | caption = DVD Cover
 | director = Lester James Peries
 | producer = Anton Wickremasinghe
 | writer = Regi Siriwardena
 | starring = Henry Jayasena Punya Heendeniya Wickrema Bogoda Trilicia Gunawardene
 | music = Pandith Amaradeva|W.D. Amaradeva William Blake
 | editing = Sumitra Gunawardana
 | distributor = Cinelanka Ltd.
 | country    = Sri Lanka
 | released =   
 | runtime = 108 minutes
 | language = Sinhala
 | budget = 
}} Sinhala cinema shot entirely outside of a studio using one lamp and hand held lights for lighting.    The movie exemplifies Periess use of family tensions to symbolize wider issues. 
 Grand Prix Best Director Best Film 1965 Sarasaviya Film Festival.  It was entered into the 3rd Moscow International Film Festival.    It was shown in Cannes Film festival in May 2008 under the French title "Changement au village." Subsequently it went out on general release in French cinemas.

== Plot ==
Piyal (Henry Jayasena) is a handsome young teacher who is hired to teach English to Nanda (Punya Heendeniya), a member of a high class family. They fall in love, but cant elope because Piyal is of a lower class. Nandas parents instead push her into a marriage with Jinadasa (Gamini Fonseka), who is of the same class as them. With economic downturn in Sri Lanka, both families lose their status and Jinadasa leaves to try to make a better life for himself; he never achieves his goal and dies penniless. Piyal and Nanda can now finally come together. They have changed however, and the earlier idylic nature of their relationship is not recaptured.

== Cast ==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Henry Jayasena || Piyal
|-
| Punya Heendeniya || Nanda
|-
| Wickrema Bogoda || Tissa
|-
| Trilicia Gunawardene || Anula
|-
| Gamini Fonseka || Jinadasa
|-
| Shanthi Lekha || Nandas mother
|-
| David Dharmakeerthi || Nandas father
|-
| Tony Ranasinghe || Baladasa
|-
| Anula Karunatilleke ||  Laisa 
|}

== Production ==
Lester James Peries admired Martin Wickramsinghes work and was inspired to attempt an adaption of Wickramasinghes novel Gamperaliya into a movie in 1964.  Wickremasinghe was initially reluctant thinking it wouldnt make a good movie, but eventually agreed.   Scholar Regie Siriwardene was asked to script the film.   

 

== Reception ==
Playwright Ediriweera Sarachchandra championed the film writing "At last a Sinhalese film has been made which we could show the world without having to hide our heads in shame. I want to say a great film has been made of a great novel." British director Lindsay Anderson hailed "its elegiac, near-Chekhovian grace." {{cite journal
  | authorlink = Gunawardana, A.J.
  | title = A Personal Cinema. An Interview with Lester James Peries
  | journal = TDR
  | volume = 15
  | pages = 303–309
  | year = 1971
  | publisher = MIT Press}} 

==References==
 

==External links==
* 
* 

 
 
 
 
 