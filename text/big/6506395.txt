The Goddess (1934 film)
{{Infobox film
| name           = The Goddess
| image          = Goddess 1934 film poster.jpg
| caption        = Film poster
| director       = Wu Yonggang
| writer         = Wu Yonggang
| starring       = Ruan Lingyu Zhang Zhizhi
| studio         = Lianhua Film Company
| released       =  
| runtime        = 85 minutes
| language       = Silent Written Chinese intertitles
| country        = China
| budget         = 
}}
The Goddess ( ) is a 1934 Chinese silent film released by the Lianhua Film Company (United Photoplay). It starred Ruan Lingyu in one of her final roles, and was directed by Wu Yonggang. 

Today The Goddess is one of the best known films of Chinas cinematic golden age, and has been named by director Chen Kaige as his favorite film of the 1930s. 

==Plot==
A never-named young woman (Ruan Lingyu) works as a prostitute to support herself and her baby son Shuiping. One night, fleeing from a police street sweep, she hides in the room of a gambler named Zhang (Zhang Zhizhi).  When he suggests she stay the night with him for not betraying her to the police, she agrees. Later, however, he and two of his pals show up at her place. He makes it clear that he considers her his property.  The woman attempts to flee, but Zhang tracks her down and frightens her by claiming to have sold Shuiping to punish her.  Zhang returns the child, but he has made his point; she realizes she cannot protect her son from him and gives in. However, she manages to hide some of her earnings behind a loose brick in her wall.

When Shuiping is about 5 or 6, she enrolls him in a school. Soon, however, the other parents learn that Shuipings mother is a prostitute, and complain to the school. The old principal visits the mother to find out if there is any truth to the accusations. She admits she is a prostitute, but upon seeing her genuine love of her child, the principal realizes that he cannot penalize Shuiping for his mothers unfortunate situation. However, he is unable to persuade the rest of the staff to be lenient, so he resigns and Shuiping is expelled.

The mother decides to take both of them away to somewhere where nobody knows them. She takes the brick out of her wall to get her money, but Zhang has already found her hiding place, and the money is gone. She confronts him and demands the money back, and when he tells her he has already spent the money, she hits him on the head with a bottle, killing him. 

She is sentenced to 12 years in prison and Shuiping is sent to an orphanage. The school principal comes to visit her, and he tells her that he will take care of Shuiping. She asks him to tell him that his mother is dead, so he does not have to suffer the shame of having a mother like her. The movie ends with her imagining a bright future for Shuiping.

==Title==
The films title has several layers of meaning. On one level, it is a description of the nameless character played by Ruan Lingyu, who is equated with a protective goddess in the film. On another level, the title refers to her characters occupation, in that the Chinese term shennü, while primarily meaning "goddess," also was an old euphemism for a prostitute.

==References==
 

==External links==
*  
*   (English intertitles)
*   for The Goddess
*  

 
 
 
 
 
 
 
 