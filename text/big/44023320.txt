Kuttichaathan
{{Infobox film
| name           = Kuttichaathan
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       =
| writer         = MP Narayana Pillai Kakkanadan (dialogues)
| screenplay     = Kakkanadan
| starring       = Adoor Bhasi Sreelatha Namboothiri Bahadoor K. P. Ummer
| music          = R. K. Shekhar
| cinematography = NA Thara
| editing        = Chakrapani
| studio         = Rajpriya Pictures
| distributor    = Rajpriya Pictures
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed Crossbelt Mani. The film stars Adoor Bhasi, Sreelatha Namboothiri, Bahadoor and K. P. Ummer in lead roles. The film had musical score by R. K. Shekhar.   

==Cast==
 
*Adoor Bhasi
*Sreelatha Namboothiri
*Bahadoor
*K. P. Ummer
*Kuthiravattam Pappu Meena
*Rajakokila
*Vidhubala Vincent
 

==Soundtrack==
The music was composed by R. K. Shekhar and lyrics was written by Vayalar and Bharanikkavu Sivakumar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ippozho Sukhamappozho || K. J. Yesudas || Vayalar ||
|-
| 2 || Kaaveri Kaaveri || S Janaki || Vayalar ||
|-
| 3 || Omkaali Mahaakali || KP Brahmanandan || Vayalar ||
|-
| 4 || Raagangal Bhavangal || K. J. Yesudas, P Susheela || Bharanikkavu Sivakumar ||
|}

==References==
 

==External links==
*  

 
 
 


 