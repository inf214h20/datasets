The Christian Licorice Store
{{Infobox film
| name           = The Christian Licorice Store
| image          = TheChristianLicoriceStore.jpg
| caption        =
| director       = James Frawley
| writer         = Floyd Mutrux
| producer       = Michael Laughlin Floyd Mutrux
| narrator       =
| starring       = Beau Bridges
| music          = Lalo Schifrin
| cinematography = David L. Butler
| editing        = Richard A. Harris
| studio         = Cinema Center Films
| distributor    = National General Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
The Christian Licorice Store is a 1971 film drama directed by James Frawley. It stars Beau Bridges and Maud Adams. The unusual title is not explained in the film.

==Plot==
Promising tennis pro Franklin Cane lives in Los Angeles and is mentored by his coach, Jonathan "J.C." Carruthers, who warns him of the perils of success. J.C. advises him to concentrate on his game and not on outside interests, such as a lucrative offer to endorse a hair spray in a TV ad.

Cane takes his advice. He wins a tournament in Houston and has a one-night stand there with a girl, cheating on Cynthia Vicstrom, the photographer he has been seeing. Things are going well for Cane until one day J.C. dies peacefully in his sleep.

A distraught Cane begins going to wild California parties and spending time on Hollywood interests, neglecting Cynthia and his tennis. Cynthia breaks up with him and begins seeing Monroe, a film director who has fallen for her. Cane leaves a party with a girl hes just met, drives down the Pacific Coast Highway at a high rate of speed, then crashes, killing them both. The next time she turns on a TV, Cynthia sees him in the hair-spray ad.

==Cast==
* Beau Bridges as Franklin Cane
* Maud Adams as Cynthia
* Gilbert Roland as J.C.
* Allan Arbus as Monroe
* Jean Renoir as Himself

==External links==
* 

 

 
 
 
 
 
 
 
 