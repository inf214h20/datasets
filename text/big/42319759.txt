Flowers from Nice
{{Infobox film
| name =  Flowers from Nice
| image =
| image_size =
| caption =
| director = Augusto Genina 
| producer = Franz Hoffermann
 | writer =   Franz Bronow     Max Wallner 
 | narrator = Paul Kemp
| music = Dénes Buday   
| cinematography = Franz Planer   Hans Heinz Theyer   Walter Tuch 
| editing = Wolfgang Wehrum      
| studio = Gloria Film
 | distributor = Kiba Kinobetriebsanstalt 
| released = 24 September 1936
| runtime = 111 minutes
| country = Austria  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical comedy film directed by Augusto Genina and starring Erna Sack, Friedl Czepa and Karl Schönböck. The films sets were designed by art directors Emil Stepanek and Julius von Borsody. The film premiered in Vienna in September 1936. In 1939 it was screened in the United States. 

==Main cast==
*   Erna Sack as Maria Castoldi  
* Friedl Czepa as Lisette  
* Karl Schönböck as Graf Ulrich von Traunstein  Paul Kemp as Rudi Hofer  
* Jane Tilden as Christl Niedermeyer  
* Hans Homma as Francois  
* Johanna Terwin as Frau Keller  
* Alfred Neugebauer as Chapelet  
* Anda Bori as Florence

== References ==
 

== Bibliography ==
* Waldman, Harry. Nazi Films In America, 1933-1942. McFarland & Company, 2008.

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 