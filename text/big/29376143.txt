Dans l'eau qui fait des bulles
{{Infobox film
| name           = Dans leau qui fait des bulles
| image          = 
| caption        = 
| director       = Maurice Delbez
| producer       =  Floralies Films, C.I.C.C, Licorne Films (France)
| writer         = Marcel Prêtre Maurice Delbez Michel Lebrun
| starring       = Marthe Mercadier Jean Richard Louis de Funès
| music          = Pierre Dudan
| cinematography = 
| editing        = 
| distributor    = Les Films Fernand Rivers
| released       = 25 October 1961 (France)
| runtime        = 90 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1961, directed by Maurice Delbez, written by Marcel Prêtre, starring Louis de Funès. In France the film is also known under the title "Le garde-champêtre mène lenquête" (alternative title); other titles: "Un cadavere in fuga" (Italy), "Louis, die Schnatterschnauze" (West Germany), "Louis, lass die Leiche liegen" (West Germany).  the scenario was written on the basis of "Les Pittuitis" of Michel Duran.

== Cast ==
* Louis de Funès : Paul Ernzer, the fisher who withdraws the body
* Marthe Mercadier : Georgette Ernzer, wife of Paul
* Pierre Dudan : Charles Donadi Philippe Lemaire : Heinrich, le convoyeur
* Jacques Castelot : Baumann, the trafficker
* Claudine Coster : Eléna, the teacher of Baumann
* Pierre Doris : Le boy-scout camionneur
* Olivier Hussenot : the commissioner Guillaume
* Maria Riquelme : Arlette Preminger
* Serge Davri : Le vagabond
* Jacques Dufilho : Alphonse, the gravedigger
* Philippe Clay : the storyteller and the voice of dying (Jean-Louis Preminger)
* Max Elloy : a fisher

== References ==
 

== External links ==
*  
*   at the Films de France
*   donnée en 2009 à propos du film Dans leau qui fait des bulles

 
 
 
 
 
 


 