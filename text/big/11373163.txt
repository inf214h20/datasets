Kohinoor (1960 film)
{{Infobox film
| name           = Kohinoor
| image          = Kohinoor1960.jpg
| image_size     =
| caption        = 
| director       = S U Sunny
| producer       = Dr V N Sinha
| writer         = 
| narrator       = 
| starring       = Dilip Kumar Meena Kumari
| music          = Naushad
| cinematography = 
| editing        = Moosa Mansoor
| distributor    = 
| released       = 1960
| runtime        = 151 minutes
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1960 Bollywood film produced by Dr V. N. Sinha and directed by S U Sunny. The film stars Dilip Kumar, Meena Kumari, Leela Chitnis and Kumkum (actress)|Kumkum. The films music is by Naushad.

It is said that Dilip Kumar went into depression after playing tragic roles in films such as Devdas and his psychiatrist recommended to do light roles. One such role that he took up was Kohinoor. Kohinoor cast Dilip Kumar and Meena Kumari to play a prince and princess of different kingdoms and was full of sword fights, songs and dances. This film is also notable for some rare comical and funny scenes by Meena Kumari, who is otherwise known as tragedy queen. Its tone was light and it lacked the intense characterisations of their earlier films. It was a major hit of the year.

The film included the very melodious songs Madhuban mein Radhika Nache Re and Do Sitaron Ka Zameen Par Hai Milan Aaj Ki Raat

==Plot==
After the passing of Maharaj Dhiraj Rana Chandrabhan of Kailash Nagar, the Senapati, Veer Singh, crowns Rajkumar Dhivendra Pratap Bahadur Chandrabhan as the next King. Veers wife, who has brought up Dhivendra like her own son, Surinder, would like Dhivendra to get married to Rajkumari Chandramukhi of Rajgarh. The Maharaja of Rajgarh is pleased to hear of this, and asks his daughter to set forth to Kailash Nagar. On the way there, she is abducted by her very own Senapati and held captive until she gives her consent to marry him. Dhivendra rescues her, both fall in love, but are captured by Senapatis men. Dhivendra is gravelly injured but manages to escape and is looked after Rajgarhs Raj Nartaki, Rajlaxmi, who also falls in love with him. After Dhivendra recuperates, he finds out that Chandramukhi is being against her will at Senapatis castle, he sets forth to set her free, but is captured in the process. Held in chains, he is blinded by a vengeful Rajlaxmi, and Chandramukhi is given an ultimatum - either wed Senapati or witness the death of Dhivendra. 

==Cast==
*Dilip Kumar   
*Meena Kumari   
*Azim   
*Bina
*Leela Chitnis   
*Jeevan   
*Raja Kapur   
*Nazir Kashmiri   
*Kumar    Kumkum  
*Rekha Mallick   
*Mukri   
*Nissar  
*Qamar   
*M.Y. Shaikh  
*Sood   
*Tun Tun   
*Uma

==Soundtrack==
{{Infobox album   
| Name = Kohinoor
| Type = soundtrack 
| Artist = Naushad 
| Cover = 
| Released = 1960
| Recorded =  Feature film soundtrack 
| Length = 
| Label =  
| Producer =
| Music Director = Naushad 
| Reviews =  Sohni Mahiwal (1958)
| This album = Kohinoor (1960)
| Next album = Mughal-e-Azam (1960)
}}
The well acclaimed soundtrack for the movie was composed by Naushad and lyrics penned by Shakeel Badayuni. The soundtrack became a cult favourite among music lovers for its soul stirring music compositions. The soundtrack consists of 10 songs, featuring vocals by Mohammed Rafi, Lata Mangeshkar and Asha Bhonsle. The best known song in the soundtrack is Madhuban Mein Radhika Nache Re, a classical dance song, composed on rāga Hamir and sung by Mohammed Rafi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Length
|-
| 1 || Madhuban Mein Radhika Nache Re || Mohammed Rafi || 6:02 
|-
| 2 || Dil Mein Baji Pyar Ki Shehnaiyan || Lata Mangeshkar || 3:27
|-
| 3 || Tan Rang Lo Ji Aaj Man Ranglo || Mohammed Rafi, Lata Mangeshkar || 3:26
|-
| 4 || Jadugar Qatil Hazir Hai Mera Dil || Asha Bhonsle ||  3:33
|-
| 5 || Zara Man Ki Kewadiya Khol || Mohammed Rafi || 3:17
|-
| 6 || Chalenge Teer Jab Dil Par || Mohammed Rafi, Lata Mangeshkar || 3:27
|-
| 7 || Yeh Kya Zindagi Hai || Lata Mangeshkar || 3:00
|-
| 8 || Dhal Chuki Sham-E-Gham || Mohammed Rafi || 3:17
|-
| 9 || Do Sitaron Ka Zameen Par || Lata Mangeshkar, Mohammed Rafi || 3:32
|-
| 10 || Koi Pyar Ki Dekhe Jadugari || Mohammed Rafi, Lata Mangeshkar || 3:24
|}

==Awards==
*Filmfare Best Actor Award for Dilip Kumar
*Filmfare Best Editing Award for Moosa Mansoor

== External links ==
*  

 
 
 

 