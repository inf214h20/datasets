Gadar: Ek Prem Katha
 
 

{{Infobox film|
| name           = Gadar: Ek Prem Katha 
| image          = Gadar.jpg
| image_size     =
| caption        = Movie Poster
| director       = Anil Sharma
| producer       = Nitin Keni
| writer         = Shaktiman Talwar
| narrator       = Om Puri
| starring       = Sunny Deol Amisha Patel Amrish Puri Lilette Dubey
| music          = Uttam Singh
| cinematography = Naj
| editing        = A.D. Dhanashekharan Keshav Naidu Arun V. Narvekar
| distributor    = Zee Telefilms T-Series
| released       =  
| runtime        = 178 minutes
| country        = India Punjabi
| budget         =   
| gross          =   (worldwide gross) 
}}
 Indian history, would create box-office history. Gadar clashed with Aamir Khans Lagaan at the box office. Made for  , Gadar has made more than   and after its theatrical run, was commercially one of the most successful movies in India  when it was released. It is in list of "One of the Biggest Blockbusters in Hindi Cinema".  The shy role of Sunny Deol was very admired and led to the Filmfare nomination for the best actor.

The story of this film is loosely based on the real life of Boota Singh.  The film is known for its anti-Pakistan sentiment and for showing Pakistan in a poor light.  The film was also the big controversy for its name because a big school believe that the things shown in the film cant be stated to be "Gadar".

== Plot ==
Set in 1947, during the Partition of India, the film tells the story of a truck driver, Tara Singh (Sunny Deol), a Jatt Sikh, who falls in love with a Muslim girl, Sakina (Amisha Patel), belonging to an aristocratic family.

The story begins with Sikhs and Hindus being attacked by Muslims in Pakistan when trying to migrate to India on a train. In response, Sikhs and Hindus react by killing Muslims migrating to Pakistan from India. During the Hindu-Muslim riots that erupted soon after the Partition, Tara saves Sakina from a murderous mob chasing her as she failed to get onto the train after being lost in the crowd. As the mob wants to rape and then murder her, Tara Singh defends Sakina and disguises her as a Sikh to protect her.

While driving back to Taras house, the story has a major flashback showing the relationship between Tara and Sakina during her collage days, but the real ambition of Tara is to become a singer. Some girls in college who are friends of Sakina, fool Tara into thinking that they have got him a spot on a music show in return for a favor. Tara performs badly in front of the musyed by Sakina (Amisha Patel). His friend then gives him tablets that help him prove his singing skills. Soon after it is shown that Sakina is not the real music teacher, which saddens him. When performing on the music show Sakina announces that she will not do her act, instead giving Tara a chance to sing despite being against the will of the seniors at the college.

Subsequently and back in the present, Sakina starts living in Taras house and their respect culminates into love. Sakina and Tara Singh get married and become parents of a baby boy. Their life seems like a bed of roses, until Sakina sees an old newspaper that has a photograph of her father, Ashraf Ali (Amrish Puri), whom she believes had been killed during the riots during the Partition.

Her father is now the mayor of Lahore. When Sakina calls him from the Pakistani Embassy in Delhi, he arranges to fly her to Lahore. However, Tara and their son, who are supposed to accompany her to Lahore, are told at the last minute that their visa formalities have not been completed, which compels them to stay in India. This does not stop Tara. He and his son, accompanied by a friend, enter Pakistan illegally at the border. There they find out that Sakina is getting married and reach her before the marriage can start and reunite.

Seeing this, Sakinas husband-to-be attacks Tara but is instead injured by him. A fight is about to break out when the priest stops them, as this can end up harming Sakinas fathers career in politics. Ashraf Ali agrees for their marriage under two conditions: They should live in Pakistan and Tara should convert to Islam.

These conditions are accepted by Tara in public the next day which was against Ashraf Alis plans. He makes Tara insult his country to prove that he is a true Pakistani, which enrages him and this makes him kill the mob that was hired by Ashraf to kill him. Tara, Sakina, their son, and a friend manage to escape.

After a long period of turmoil they catch a cotton mill train which will be their ticket to India. Ashraf Ali finds out, and he takes some men to stop them. In the ensuing fight Sakina gets shot by her own father. In the hospital Sakina has lapsed into a coma whis mistake. Sakina gains consciousness after having a nightmare. The movie ends with Ashraf Ali accepting Tara as his son-in-law and they return to India.

== Production ==
 Que sera sera" which was first published in 1956.

The movie was also shot in the city of Lucknow, Uttar Pradesh where the city was depicted as Lahore, Pakistan and parts were shot at La Martiniere Boys School, Lucknow a UNESCO Heritage site. A significant part was shot in Pathankot, Sarna and Amritsar to depict the division torn country. 

== Cast ==
 
 
* Sunny Deol  as Tara Singh
* Amisha Patel as Sakeena
* Amrish Puri as Ashraf Ali (Sakeenas father)
* Lillete Dubey as Shabana (Sakeenas mother)
* Vivek Shauq as Darmiyaan Singh
* Utkarsh Sharma as Charanjeet
* Suresh Oberoi as Taya
* Madhu Malti as Tayee
* Pramod Moutho as Gurdeep (Taras father)
* Dalip Tahil as Sarfaraz
* Mushtaq Khan as Gul Khan
* Dolly Bindra as Gul Khans wife
* Rakesh Bedi as Vaidya
* Vishwajeet Pradhan as Daroga Suleiman
* Tony Mirchandani
* Samar Jai Singh as Salim
* Ahsaan Khan as Abdul Ali
 
* Vikrant Chaturvedi as Kalim
* Asha Bachchani as Zaheera
* Naresh Sharma as Baldev
* Rajshree Seem as Baldevs wife
* Pratima Kazmi as Greedy Woman
* Gyan Prakash as Wali Mohammed
* Amita Khopkar as Bano
* Prince Deepak as Jumman
* Rahul as Aslam
* Kanika Shivpuri as Taras mother
* Malvika Shivpuri as Taras sister
* Charan Preet
* Tarika Khanna as Baby Sakina
* Master Polyster as Baby Aslam
* Santosh Gupta as Chanta
* Om Puri as Narrator
*
 

== Crew ==
 
 
* Producer: Nitin Kenir: Anil Sharma
* Story: Shaktimaan
* Screenplay: Shaktimaan
* Dialogues: Shaktimaan
 
* Lyrics: Anand Bakshi
* Music: Uttam Singh
* Choreography: Jay Borade
* Editing: Babu Ali Ansari
* Costume Design: Bhavna
 

==Box office==
Gadar collected 43 crores after 18 weeks.  Inflation adjusted in 2013 equals 175 crores 

Gadar has second highest footfalls  after Sholay

== Music ==
{{Infobox album |  
 Name = Gadar: Ek Prem Katha |
 Type = Album |
 Artist = Uttam Singh |
 Cover = |
 Released = 2001 (India)|
 Recorded = | Feature film soundtrack |
 Length = |
 Label =  T-Series|
 Producer = Uttam Singh |
 Reviews = |
 Last album = Farz (2001 film)|Farz (2001) |
 This album = Gadar: Ek Prem Katha (2001) |
 Next album = Pyaar Diwana Hota Hai (2002)|
}}

The music. The song listing is as follows:
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; "
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
|"Udja Kale Kawan - Folk"
| Udit Narayan
|-
|"Musafir Jaane Wale"
| Udit Narayan, Preeti Uttam  
|-
| "Main Nikla Gaddi Leke"
| Udit Narayan
|-
|"Udja Kale Kawan - Marriage"
| Udit Narayan, Alka Yagnik
|-
|"Hum Juda Ho Gaye"
| Udit Narayan, Preeti Uttam
|-
| "Udja Kale Kawan - Search"
| Udit Narayan, Alka Yagnik
|-
| "Aan Milo Sajna"
|  Ajay Chakraborty, Parveen Sultana
|-
| "Traditional Shaadi Geet "
| Preeti Uttam
|-
| "Udja Kale Kawan - Victory"
| Instrumental
|}

== Awards ==
Winner:
*Filmfare Best Action Award - Tinnu Verma
*Filmfare Special Performance Award - Amisha Patel
*Sansui Best Actress Award - Amisha Patel Best Actress - Amisha Patel
*Sansui Best Actor Award - Sunny Deol
*Zee Cine Special Award for Outstanding Performance - Male - Sunny Deol
*Star Screen Best Actor Award - Sunny Deol
Nominated:
*Filmfare Best Film Award - Nitin Keni
*Filmfare Best Actor Award - Sunny Deol
*Filmfare Best Actress Award - Amisha Patel
*Filmfare Best Director Award - Anil Sharma
*Filmfare Best Music Director Award - Uttam Singh
*Filmfare Best Lyricist Award - Anand Bakshi
*Filmfare Best Villain Award - Amrish Puri
*Filmfare Best Male Playback Award - Udit Narayan
*IIFA Best Film Award - Tinnu Verma
*IIFA Best Director Award - Anil Sharma
*IIFA Best Actress Award - Amisha Patel
*IIFA Best Actor Award - Sunny Deol
*Star Screen Best Film Award - Nitin Keni
*Star Screen Best Actress Award - Amisha Patel
*Star Screen Best Director Award - Anil Sharma
*Zee Cine Award for Best Film - Nitin Keni
*Zee Cine Award for Best Actor - Female - Amisha Patel
*Zee Cine Award for Best Actor - Male - Sunny Deol
*Zee Cine Award for Best Director - Anil Sharma
*Zee Cine Award for Best Actor in a Negative Role - Amrish Puri

== See also ==
 
*List of highest-grossing Bollywood films

== References ==
 

== External links ==
* 

 
 
 
 
 
 