Turn of Faith
{{Infobox film
| name           = Turn of Faith
| image_size     = 
| image	=	Turn of Faith FilmPoster.jpeg
| caption        = 
| director       = Charles Jarrott
| writer         = 
| narrator       = 
| starring       = Ray Mancini
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2001
| runtime        = 96 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Turn of Faith is a 2001 film directed by Charles Jarrott. It stars Ray Mancini and Mia Sara. 

==Plot==
The film follows three friends, a cop, a priest and a kid from the neighborhood who wants to handle the family business. Then a man named Philly enters the 
picture and suddenly things become difficult.

==Cast==
*Ray Mancini as Joey
*Mia Sara as Annmarie De Carlo
*Costas Mandylor as Bobby Giordano
*Alan Gelfant as Father Frank Tom Atkins as Charlie Ryan
*Tony Sirico as Jimmy
*Charles Durning as Philly

==References==
 

==External links==
* 

 

 
 