Nammanna
{{Infobox film
| name       = Nammanna ( Dowrjanyam ) ( Our Brother )
| image      = Nammanna Kannada Movie.jpg
| director   = N. Shankar
| producer   = Balamuttaiah
| story      = 
| screenplay = 
| starring   =  
| music      = Gurukiran
| cinematography = 
| editing    = 
| studio     = 
| distributor = 
| released   =  
| country    = India Telugu (dubbed)
| runtime    = 
| budget     = 
| gross      = 
| website    =  
}}

Nammanna ( : Our Brother) is a 2005  , Anjala Zaveri and Asha Saini in the lead roles. The film features background score and soundtrack composed by Gurukiran. The film released on 18 November 2005.  This movie is released in Telugu as Dowrjanyam. The film is based on Telugu film Anna (1994 film)|Anna.

==Cast==
* Sudeep ... Muttanna
* Anjala Zaveri ... 
* Asha Saini ...
* Kota Srinivasa Rao ...
* Sadhu Kokila ...
* Subbaraju ...
* Ashish Vidyarthi ... Ashok ...
* Mukhyamantri Chandru ... 
* Kishori Ballal ...
* Aravind ...
* and others...

==Soundtrack==
  The film features background score and soundtrack composed by Gurukiran and lyrics by Jayant Kaikini, Kaviraj and Goturi.
{{Infobox album
| Name = Nammanna
| Type = soundtrack
| Artist = Gurukiran Kannada
| Label = Dhanalaxmi Audio
| Producer = 
| Cover = 
| Released    =   Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}} 

==References==
 

==External links==
*  

 
 
 
 
 


 