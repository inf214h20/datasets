Sons and Lovers (1960 film)
{{Infobox film
| name           = Sons and Lovers
| image          = Sons and Lovers poster.jpg
| image_size     =
| caption        = film poster
| producer       = Jerry Wald
| director       = Jack Cardiff
| writer         = T. E. B. Clarke Gavin Lambert
| starring       = Trevor Howard Dean Stockwell Wendy Hiller  Mary Ure
| music          = Mario Nascimbene
| cinematography = Freddie Francis
| editing        = Gordon Pilkington
| distributor    = Twentieth Century Fox
| released       =  
| country        = United Kingdom
| language       = English
| runtime        = 103 minutes
| budget = $805,000 
| gross = $1,500,000 (US/ Canada) 
}} of the William Lucas and Donald Pleasence.

The film won an Academy Award for Best Cinematography (Freddie Francis) and received nominations in six additional categories,    and was entered into the 1960 Cannes Film Festival.   

==Plot== emotionally manipulative, domineering mother — a literary, psychological interpretation of the Oedipus story. Gertrude Morel, miserable in her marriage, puts her hope into her son, Paul. In her attempt to manipulate Pauls life she jealously attempts to prevent Paul from having a relationship with any woman. However, Paul goes to the city for a job and becomes enchanted with self-actualized and "liberated" feminist co-worker, Mrs. Clara Dawes, who is married. Paul and Clara become involved sexually and Clara realizes that Pauls emotional attachment, as with her own, lies with another person — in Pauls case, his mother. Gertrude learns of Pauls involvement with Clara, and she slips into a morose depression and physical sickness. Paul flees to his mother, to care for her and sit by her side. After his mothers death, Paul meets the girlfriend of his youth, Miriam, and tells her that because of his codependency with his mother he intends to live the rest of his life without any serious relationship with another woman — in essence fulfilling his mothers desire and objective.

==Cast==
*Trevor Howard as Walter Morel
*Dean Stockwell as Paul Morel
*Wendy Hiller as Gertrude Morel
*Mary Ure as Clara Dawes
*Heather Sears as Miriam Leivers William Lucas as William Morel
*Conrad Phillips as Baxter Dawes
*Ernest Thesiger as Henry Hadlock
*Donald Pleasence as Mr. Puppleworth
*Rosalie Crutchley as Mrs. Leivers Sean Barrett as Arthur Morel
*Elizabeth Begley as Mrs. Radford
*Edna Morris as Mrs. Anthony
*Ruth Kettlewell as Mrs. Bonner
*Anne Sheppard as Rose
*Dorothy Gordon as Fanny

==Production==
The movie was filmed on location in Nottingham, England and at the Pinewood Studios, Iver Heath, Buckinghamshire, England. The musical theme by Mario Nascimbene was popular and was arranged for both piano and orchestra.

==Reception==
Bosley Crowther of the New York Times said: "Sons and Lovers is sensitively felt and photographed in Jerry Walds British-made film version of (Lawrences novel). ... Jack Cardiff, cameraman turned director, has filled it with picture poetry. ... The drabness of a north-of-England coal town, the warmth of a poor coal miners home, the bleakness of the wintry English country near Eastwood, where Lawrence was born — all are caught and concentrated in this film, appropriately black-and-white, which puts forth the generalized Lawrence story in a stunning pictorial style. ... (T)he most dynamic and emotional character in the film is the discarded miner-father, played brilliantly by Trevor Howard. His frequent violent flare-ups of rebellion, his pitiful complaints of chagrin and his always underflowing indications of a sense of being not wanted and alone are perhaps the most clear articulations of the theme of frustration in the tale. Through him is expressed most intensely the realization of the mortality of young love." 

== Academy Awards ==
*Winner Best Cinematography (Black and White) (Freddie Francis) Best Actor in a Leading Role (Trevor Howard) Best Actress in a Supporting Role (Mary Ure) Best Art Direction-Set Decoration, Black-and-White (Thomas N. Morahan, Lionel Couch) Best Director (Jack Cardiff) Best Picture (Jerry Wald, producer) Best Writing, Screenplay Based on Material from Another Medium (T.E.B. Clarke, Gavin Lambert). 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 