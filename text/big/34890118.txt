The Frankenstein Syndrome
{{Infobox film
| name           = The Frankenstein Syndrome
| image          = TheFrankensteinSyndrome2010Poster.jpg
| caption        = Film poster
| director       = Sean Tretta
| producer       = {{plainlist|
* Dustin Lowry
* Tiffany Shepis
* Noah Todd
* Sean Tretta
}}
| writer         = Sean Tretta
| based on       =  
| starring       = {{plainlist|
* Ed Lauter
* Louis Mandylor
* Tiffany Shepis
* Scott Anthony Leet
}}
| music          = Lawrence Shragge
| cinematography = Eve Cohen
| editing        = {{plainlist|
* Eric Weston
* Sean Tretta
}}
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}}
The Frankenstein Syndrome (originally titled The Prometheus Project) is a 2011 American science fiction-horror film written and directed by Sean Tretta.  Scott Anthony Leet stars as a murdered security guard who is reanimated by a research institute in the tradition of Mary Shelleys novel Frankenstein.  It was released directly to DVD.

== Plot ==
Researchers conduct secret and illegal experiments using stem cells. The researchers accidentally discover a serum derived from these stem cells capable of reviving dead cellular tissue. When a security guard, David Doyle, is injured while working and threatens to sue the research company, he is promptly murdered and used as a test subject. 

David returns to life but, in the style of The Reanimator, David is not the same man he once was. Not only is his personality and memory changed, but he is seen to acquired psychic and telekinetic powers, as well as increased strength and aggression. David begins to act out against the researchers, who all at once are his captors, murderers, and creators.

== Cast ==
* Ed Lauter as Dr. Walton
* Louis Mandylor as Marcus
* Tiffany Shepis as Elizabeth Barnes
* Scott Anthony Leet as David Doyle

== Production ==
Dread Central reported that it was in post-production in March 2010. 

== Release ==
American World Pictures purchased the film in October 2010 and retitled it to The Frankenstein Syndrome. 
MTI Home Video released it on July 5, 2011. 

== Reception ==
Darryl Loomis of DVD Verdict wrote, "The Frankenstein Syndrome isnt a great film, but for those into both Mary Shelleys beautiful novel and independent horror, you can do a whole lot worse than this."   Bill Gibron of PopMatters rated it 7/10 stars and wrote, "While some of the subplots and a few of the asides dont add up to much and the payoff promises something the movie might not be willing to breach, this is still a bold, audacious statement." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 