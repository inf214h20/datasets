Three Kingdoms: Resurrection of the Dragon
 
 
 
{{Infobox film
| name = Three Kingdoms: Resurrection of the Dragon
| image = ThreeKingdomsResurrectionoftheDragon.jpg
| caption = Film poster
| film name = {{Film name
| traditional = 三國之見龍卸甲
| simplified = 三国之见龙卸甲
| pinyin = Sān Guó Zhī Jiàn Lóng Xiè Jiǎ
| jyutping = Saam1 Gwok3 Zi1 Gin3 Lung4 Se6 Gaap3 }} Daniel Lee
| producer = Chung Taewon Suzanna Tsang Dong Yu
| writer = Lau Ho-Leung Daniel Lee
| starring = Andy Lau Sammo Hung Maggie Q Vanness Wu Andy On Ti Lung
| music = Henry Lai
| cinematography = Tony Cheung Cheung Ka-Fai Tang Man-To Beijing Polybona Beijing Polybona Sundream Scholar Films
| released =  
| runtime = 101 minutes 
| country = China Hong Kong South Korea
| language = Mandarin
| budget = US$20 million {{cite web|title=Three Kingdoms: Resurrection of the Dragon
|url=http://www.the-numbers.com/movies/2008/03KD.php}} 
| gross = US$21.2 million 
}}
{{Infobox Korean name title = Korean name context = south hangul  = 삼국지: 용의 부활 hanja   =  : 용의   rr      = Samgukji: Yong-ui Buhwal mr      = Samgukchi: Yong-ŭi Puwhal
}}

Three Kingdoms: Resurrection of the Dragon is a 2008  . Friday 27 June 2008. Retrieved on 13 January 2012. 
 Red Cliff. 

==Plot== Zhao Zilong begins his career by enlisting in Liu Beis army. He befriends a fellow soldier, Luo Pingan, who is also from his hometown in Changshan. Not long later, Zhao participates in a battle against Liu Beis rival Cao Cao. He follows Zhuge Liangs plan and launches a sneak attack on the enemy camp at night, achieving his first victory. Liu Bei is overwhelmed by Cao Cao and is forced to retreat to Phoenix Heights but is separated from his family during the chaos. Luo is ordered to find and bring Lius family back safely but fails. Zhang Fei is angered and thrusts his spear towards Luo, but Zhao blocks the attack and engages Zhang and Guan Yu in a fight. Zhao remains undefeated after dueling for several rounds and Liu Bei is impressed by his skill. Zhao offers to retrieve Lius family, and Guan Yu and Zhang Fei cover him while he breaks through enemy lines to begin the search.

Zhao rescues Liu Beis infant son and holds off dozens of enemy soldiers alone despite being surrounded on all sides. He fights his way out and charges towards Cao Cao, who is observing the battle nearby. Cao is shocked and loses his sword to Zhao, but the latter spares his life and leaps to safety on a cliff. Cao Caos granddaughter Cao Ying witnesses the attack. Zhao later returns to Changshan as a hero and falls in love with a girl putting on a shadow puppet show dedicated to him.
 Wei to Zhang Bao and Luo Pingan. Zhuge Liang gives him two envelopes, and tells him that he is to open the first envelope when he comes to the first fork in the road, and he is to open the second envelope when things become difficult.

Zhaos army reaches a fork in the road, opens the first envelope, and learns that Zhuge Liang orders him to split his forces into two and send each half down each fork. He splits his forces, as instructed, with one led by Zhao himself while Guan and Zhang command the other. Zhao later encounters the Wei general Han De and slays Hans four sons all by himself. However, he is lured into a trap set by the Wei commander, Cao Ying, and has to retreat to Phoenix Heights. While he is surrounded on all sides and his forces has sustained heavy casualties, he opens the second envelope and learns that his task is actually to distract the Wei army while Guan Xing and Zhang Bao proceed to capture enemy territory.

After attempts by both the Shu and Wei sides to instigate each other into battle, Zhao engages Cao Ying in a duel and defeats her but lets her go. Cao Yings forces later advance towards Phoenix Heights and Zhao allows his subordinates to lead all his men into battle. The Shu soldiers launch a fierce assault on the Wei army and are nearly all wiped out when Han De sacrifices himself to launch a kamikaze-style attack to blow up the enemy with gunpowder.

Zhao and Luo Pingan watch the battle and aftermath from Phoenix Heights. At the end, Luo reveals that he has been very jealous of Zhao all these years because Zhao kept rising up the ranks while Luo remained as a foot soldier. The two men make peace with each other and Luo helps Zhao remove his armour and tearfully beats the battle drum as Zhao makes a long charge towards the enemy.

==Cast==
* Andy Lau as Zhao Zilong, the main character. Regarding his performance, Lau said "There are three perspectives on Zhao historians professional knowledge, how normal people like us remember him and his character known through a popular computer game. I cant satisfy all these, but I tried to stay true to the script."  Sammo Hung said that "Lau had to portray a man from his 20s to 70s, and he did it perfectly, with the look in his eyes and all, particularly in the last scene when an invincible hero loses for the first time." 
 Daniel Lee and "everything attracted me about the role and the film."  With regards to her character, she said that like Cao Ying, she "got to feel like that in this industry, where you sort of wish you could just be a woman and relax and be happy but it takes a lot of strength to be where you are."  Regarding Maggie Qs performance, Hung said "I had doubts. But she blew me away."  Zhang Yujiao played the younger Cao Ying.

* Sammo Hung as Luo Pingan, a soldier from the same hometown as Zhao Zilong.

* Vanness Wu as Guan Xing, Guan Yus son.

* Andy On as Deng Zhi, a Shu general and a subordinate of Zhao Zilong.

* Ti Lung as Guan Yu, Liu Beis sworn brother and one of the Five Tiger Generals of Shu.

* Elliot Ngok as Liu Bei, a warlord and the founding emperor of Shu.

* Pu Cunxin as Zhuge Liang, Liu Beis advisor and the chancellor of Shu.

* Chen Zhihui as Zhang Fei, Liu Beis sworn brother and one of the Five Tiger Generals of Shu.

* Damian Lau as Cao Cao, a warlord and Liu Beis rival.

* Yu Rongguang as Han De, a Wei general and subordinate of Cao Ying.
 Zhang Bao, Zhang Feis son.

* Jiang Hongbo as Ruaner, Zhao Zilongs romantic interest

* Wang Hongtao as Huang Zhong, one of the Five Tiger Generals of Shu.

* Menghe Wuliji as Ma Chao, one of the Five Tiger Generals of Shu.

* Liang Yujin as Lady Gan, Liu Beis wife.

* Bai Jing as Lady Mi, Liu Beis wife.

* Zhao Erkang as Ruaners father

* Timmy Hung as Han Ying, Han Des son.

* Yang Jian as Han Yao, Han Des son.

* Chen Guohui as Han Jing, Han Des son.

* Zhang Mang as Han Qi, Han Des son.

* Hu Jingbo as Liu Shan, the last emperor of Shu. Feng Xiaoxiao played the younger Liu Shan.

===Replaced cast===
Qian Zhijun was originally considered for the role of Liu Shan.  The film producers said that they invited Qian to act in the film because, in the words of China Radio International, "they think hes a really interesting guy and the movie needs a lighthearted character for comic relief."  An article from the China Film Group Corporation said that the role of Liu Shan suits Qians appearance and that "Liu Shan was not a very bright person, the Chinese idiom lèbùsī shǔ (乐不思蜀 ) originated from him, he was unassertive and submissive. To qualify for this role, an actor simply needs to relax his mind completely and have no thoughts. The level of difficulty in playing this character is 2 on a scale of 1 to 5 (in order of increasing level of difficulty)."  La Carmina of CNN said that Qians obtaining of the role illustrated that he went "from obscurity to movie stardom".  In 2007 the film script was modified due to financing issues. The Liu Shan role was altered, and Qians planned role in the film was removed.  The role of Liu Shan eventually went to Hu Jingbo.

==Production==
The production companies involved in making the film are Visualizer Film Production Ltd, Taewon Entertainment, and SIL-Metropole Organization of mainland China. The individuals who headed the production were Susanna Tsang of Visualizer and Chung Taewon of Taewon. The Hong Kong company Golden Scene took sales rights within East Asia, including Japan. The company Polybona Films took sales rights within mainland China. Arclight Films took the international sales rights to the film, which it distributed under its Easternlight label. Arclight planned to handle the distribution at the Cannes Film Festival in Cannes, France and all other international points. 
 The Lord of the Rings films.  Sammo Hung, a martial artist, served as the choreographer of fighting scenes. Lee, Hyo-won. "Three Kingdoms." Reuters at The China Post. Friday 4 April 2008.  . Retrieved on 13 January 2012.   In addition, Sammo Hung also plays a character, Luo Pingan, who is a friend of Zhao but becomes jealous of him. Hung expressed satisfaction in the performances of the actors.  As of 2008, it was the largest film production that Lee had directed. 

The creators opted to use computer graphics, a phenomenon common among war movies with large budgets made in the 2000s. Lee said that the computer graphics were crucial for the overall war scenes and for the smaller details. Lee explained that, while filming in a desert, the weather would change constantly, with rainy weather and sunny weather occurring during the day and snow occurring at night. To compensate for having "four seasons in one day," computer graphics were applied to alter the presentation of the setting.  Lee asked Mixfilm, a Korean company, to do the special effects for the film. Lee explained that "I wanted to film a documentary version of The Three Kingdoms through the characters. So I didnt want anything too beautified, and I am very satisfied with the results." 

With regards to historical accuracy in the film, Lee said that the creators were not striving for "100% historical authenticity which we were after" since "there were only fictional descriptions in Luos novel and very limited reliable historical data on the costumes and weapons of the Three Kingdoms era, it did leave a lot of room for the imagination."  Lee further explained that "While insisting on retaining the Chinese cultural integrity of the designs, we decided to do a revamp of all the known elements derived from careful research and to develop a visual style that conveys the feelings and moods of the Three Kingdoms period."  Lee said that the historical China as depicted in the film "might differ from the historical looks in an average   opinion, and would naturally surprise existing fans of the Three Kingdoms epic,   might already have their own preconceived visions of what each character, especially Zhao Zilong, looked like."  Lee stated that the film was not intended to perfectly represent the original work, which itself is derived from various fictions and oral traditions. Lee said that his film "incorporated more creative manifestations and personalization of the stories in order to explore the character of the legendary Zhao Zilong, both as a warrior and as a man."  Lee said that he had no intention of debasing the original work, nor did he have the intention of offending literary purists who were fans of the original book. 

==Soundtrack==
{{Infobox album  
| Name        = Three Kingdoms: Resurrection of the Dragon Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Henry Lai & Capellen Orchestra and Capellen Choir
| Cover       =
| Released    = 20 April 2009
| Recorded    = 
| Genre       = Soundtrack
| Length      = 70:53
| Label       = Java Music Productions
| Producer    = 
| Reviews     = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Henry Lai created the soundtrack. It has inspiration from the soundtracks of Ennio Morricones "Dollars" films and several Hong Kong films such as Once Upon a Time in China. 

Tracklist: 
#Three Kingdoms (4:27)
#Story of Luo Pingan (3:35)
#The Ambush Squad (9:12)
#Save the Young Lord (8:49)
#Love Theme (2:54)
#Shu March (0:47)
#The Five Generals (2:28)
#The Northern Expedition (1:21)
#The Dust Bowl (3:11)
#The Siege (1:53)
#The Karmic Wheel (2:00)
#The Romance of the Princess (1:55)
#Deng Zhi (0:56)
#Wei Funeral (3:03)
#Shu Requiem (2:51)
#Returning the Sword (2:35)
#The Duel (5:43)
#Zhao Army (1:56)
#Battle on the Phoenix Height (3:25)
#After the Snow (3:02)
#Resurrection of the Dragon (2:24)
#Zhao Zilong (2:26)

==Awards==
28th Hong Kong Film Awards 
*Nominated: Best Cinematography (Tony Cheung)
*Nominated: Best Art Direction (Daniel Lee & Horace Ma)
*Nominated: Best Costume Makeup Design (Thomas Chong & Wong Ming Na)
*Nominated: Best Action Choreography (Sammo Hung & Yuen Tak)
*Nominated: Best Original Film Score (Henry Lai)
3rd Asian Film Awards 
*Won: Best Production Designer (Daniel Lee)
*Nominated: Best Composer (Henry Lai)

==Significance==
 
Dr. Ruby Cheung, the author of "Red Cliff: The Chinese-language Epic and Diasporic Chinese Spectators," described this film as one of the "immediate precedents" of the film Red Cliff. 

==Release==
  Daniel Lee, and crew member Sammo Hung. Many of Laus fans waited for him outside of the CGV Yongsan; as of 2008 Lau is very popular in East Asian countries. 
 National TV during Nowruz 2008. 

==Reception==
Derek Elley of Variety (magazine)|Variety said that the " ightly cut movie has almost no downtime but also no sense of rush" and that the cast "is relatively clearly defined, and details of costuming, armor and massive artillery have a fresh, unfamiliar look."  Elley added that the music "motors the picture and gives it a true heroic stature." 

There was controversy regarding the costumes worn by the cast, as some critics argued that Zhao Zilongs armour resembles the samurais. Lee responded, saying that elements from Japanese soldier costumes originated from Chinese soldier costumes, so one should not be surprised that they appear like Japanese soldier costumes.  In addition, some critics said that Maggie Q, an American actress, was not suitable to be placed in a Chinese period piece, due to her Eurasian appearance. Lee argued that "We didnt find it a problem at all, as historically inter-ethnic marriages were a strategy of matrimonial alliances, commonly adopted by Chinas rulers to establish peace with the aggressive neighbouring, non-Chinese tribes." 

==See also==
 
 Red Cliff
* List of historical drama films of Asia List of media adaptations of Romance of the Three Kingdoms
* Andy Lau filmography
* Sammo Hung filmography

==Notes==
 

==References==
 

==Further reading==
* Guégan, Jean-Baptiste. " ." Excessif (e-TF1). 15 May 2008.

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 