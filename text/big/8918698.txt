István, a király
István, Hungarian rock Saint Stephen of Hungary. The storyline was based on the play Ezredforduló (Turn of the Millennium) by Miklós Boldizsár, who co-wrote the libretto.

The opera was first staged in 1983 on an open-air stage in Budapest. This first performance was also made into a 1984 film, directed by Gábor Koltay, and its music released on an album. The musical became a smash hit and is still very popular in Hungary and among Hungarian minorities in neighboring countries.

==Historical background== pagan Hungarian Magyar (Hungarian) Christian state. Catholic missionaries to his lands and let his son Vajk be baptized István (Stephen) and brought up as a Catholic.
 hereditary monarchies. According to ancient Hungarian custom, however, the oldest male member of the family - in this case, a relative named Koppány - would have been the successor.
 quartered and the pieces exposed upon the walls of the main castles of the country.

In the year 1000 or 1001, István received a crown from the Pope. He was crowned the first king of Kingdom of Hungary|Hungary.

==Synopsis==

===Dramatis personae===
*István, Hungarian prince, later ruling prince of Hungary
*Giselle of Bavaria|Gizella, his wife, a Bavarian princess
*Sarolt, Istváns mother
*Asztrik, bishop
*Hont   \
*Pázmány German knights
*Vecellin / pagan relative of Istváns Christian
*Laborc, a follower of Koppánys
*Torda, a pagan shaman
*Picur    \
*Enikő      Koppánys wives
*Boglárka /
*Súr   \
*Solt    Hungarian noblemen, opportunists
*Bese  /
*people, followers, soldiers, priests

The piece is set in Hungary, around the year 1000.

===Act I, Az örökség (the heritage)===
A singer sings about good leadership for the country and asks "Whom would you choose?" (Te kit választanál?)
The ruling prince of Hungary, Géza, has invited Christian missionaries into the country (Veni lumen cordium/Töltsd el szívünk, fényesség). In order to strengthen ties with the West, his son István is marrying the Bavarian princess Gizella.

Súr, Solt and Bese, a group of opportunist noblemen, talk about human frailty - every man chooses the side that seems the most promising. Principles are unimportant (Gyarló az ember).

The daughter of Istváns relative Koppány, Réka, has also converted to Christianity and is praying to the new God. Laborc, a follower of her fathers, tells her they dont need a foreign god - she should rather trust her father (Nem vagyunk még hozzád méltók/Nem kell olyan isten).

Géza has died, the country is in mourning (Géza fejedelem temetése - Kyrie eleison). István promises at his grave to be a good prince to his people, yet he is defied by Koppány, who claims to be the rightful successor of Gézas. The followers of both gather behind them and hail them (Nincs más út csak az Isten útja).

===Act II, Esztergom===
Réka, Gizella, the priests and the people are saddened and pray for peace (Adj békét Uram/Da pacem, Domine).

A group of singers hail István, yet they also sing about past glory (Üdvöz légyen Géza fia). Istváns tempestuous mother Sarolt is suspicious of such pagan attitudes.
Laborc arrives in Koppánys name, proposing Sarolt a marriage with his lord (Koppány küldött, jó úrnőm). Koppány hopes that by marrying Gézas widow, he may be accepted as his successor. Sarolt finds the proposal outrageous. Laborc is executed immediately.

The three noblemen start making fun of Koppány in front of István, describing him as uncivilized and stupid (Abcúg Koppány). István is disgusted with them and chases them away.

István is torn apart by the situation. His deep faith and his loyalty towards his family forbid him to fight, yet there dont seem to be any other possibilities. Sarolt admonishes him to be cunning. She tells him to prepare for war (István fiam!).

Gizella declares herself "bored with politics" and is upset with István: she would like them to have a baby. The German knight Vecellin is also rather unnerved by the fact that they have not yet started a war (Unom a politikát).

In triumph, István is elected to be the new ruling prince of the Hungarians. The people hail him as their new lord (Fejedelmünk István!).

After the feast, István goes away to be alone. He is sad and undecided and converses with God about what to do. Réka observes him. She is secretly in love with István - her fathers arch-enemy -, yet has to keep her feelings secret (Oly távol vagy tőlem (és mégis közel)).

===Act III, Koppány vezér (Koppány, the chief)===
Koppány convokes his followers. He promises a glorious future to them and prepares them for the fight. The people respond enthusiastically (Szállj fel, szabad madár).

Koppány sits in his tent with his three pretty young wives. They extol his qualities as a husband and lover and express their desire for him (Te vagy a legszebb álmunk). Koppány is rather distracted and he is soon unnerved by their chattering. He sends them away.

The three opportunists now appear in front of Koppány. They propose him different ways of assassinating István (Abcúg István). Koppány sends them away - he wants to fight honorably. He also confirms this in front of the shaman Torda and the people - he wants to stand "face to face" with István and his army (Szemtől szembe). Torda prays to the pagan gods for victory and brings them sacrifice (Áldozatunk fogadjátok).

Réka has had a nightmare where she saw her father dead. She implores him not to pursue the conflict. István proposes Koppány the crown if he submits to the Church of Rome. But Koppánys hatred of priests and his determination to win the country are stronger. He tells them that it is "too late" for peace now (Elkésett békevágy).

Torda presents Koppánys followers the bloody sword, the symbol of war. If they win, Hungary will have a glorious future, he prophesies. The war breaks out and ends with Istváns victory (Véres kardot hoztam/Vezess minket, István!).

===Act IV, István a király (István, the king)===
Koppánys side has lost, he has died in battle. A singer laments the dead (Gyászba öltözött csillagom). Istváns followers celebrate at his court. Everyone demands his due (Hála néked, fejedelem!). Finally, Réka appears and asks István to give her her dead fathers body. He is moved by her sorrow and beauty, but Sarolt brutally chases her away: Koppány would be quartered, as a deterrent for potential rebels (Halld meg uram, kérésem/Felnégyelni!).

István is shattered and demands to be alone. He desperately prays to God (Oly távol vagy tőlem - reprise). Finally, he backs his mothers decision.

Koppánys body is quartered (Koppány felnégyelése/Gloria gloria). Finally, István is triumphally crowned king of Hungary (István a király).

==Political background and interpretation==
  Communist party chief János Kádár, who was still in power at the time István, a király was written.

On "another hand", the fact that the character of István was presented as a thoughtful man who nevertheless does "what a man gotta do", made the interpretation that he stood for Kádár who also "had to do what hed gotta do" quite compelling for the regime. Though Istváns antagonist Koppány is presented as a noble, honest soul, he could also be seen as being somewhat "behind the times", as a person who does not understand what the country needs most. In such an allegorical interpretation, the German knights and the Catholic priests could be put into parallel with the Soviet army stationed in Hungary, and the vital connection of Hungary with the papacy with the countrys relationship with the Soviet Union.

On "a still another hand", however, the rock opera can also be read as a subversive piece that introduced daring themes such as an open uprising against the state or religion into a mainstream theater piece. Especially among the minority Hungarians in the neighboring countries, István, a király was understood as an expression of nationhood and patriotism, explicitly spelled out in the last words of its last song: "Szép Magyarország, édes hazánk." ("Beautiful Hungary, our sweet home.")

==István, a király as a work of art==

===Music===
Composer Levente Szörényi, who had already worked together with János Bródy (lyrics) for more than two decades when they wrote István, a király (most famously in the bands Illés (band)|Illés and Fonográf), chose to characterize every major character and group in the play by its own style of music. Thus, the music of the rock opera encompasses a great variety of styles, ranging from Gregorian chant to hard rock.
 pop songs, folk tunes The English Patient and for her frequent collaborations with the well-known folk music ensemble Muzsikás). The priests and missionaries sing music akin to Gregorian chant. The mass scenes of the election in Act II and the coronation in Act IV soar to symphonical heights at some moments.

The transition from the old to the new is often compellingly expressed musically, for example in the funeral scene where a traditional Hungarian folk melody is blended into and then vanishes behind a Gregorian "Kyrie eleison".

The wide range of musical styles also explains the large orchestra that is needed for the opera - it has to encompass both most features of a classical orchestra and of a rock band.

===Lyrics===
János Bródy has been acknowledged as one of the most talented lyrics writers of Hungary and István, a király was certainly written in his prime. His subtlety of characterization makes a one-sided interpretation difficult and contributes to the credibility of the characters. Especially the main characters István and Koppány appear as complex figures: István as a pious, deeply affected, somewhat "Hamlet-y", yet finally determined man; Koppány as a passionate character who knows what he wants, yet is not power-hungry, but is rather led by his respect for tradition and the fear that tradition would disappear through the "foreigners" István is bringing into the land.

Bródy, himself a secular Jew, is especially subtle in his characterization of the Catholic Church. The rock opera can both be read as traditional "priest criticism", or as a piece that is deeply respectful of faith. In this way, directors are free to determine how to present and represent the priests and missionaries. The original version that was made into a 1984 film favored a neutral stance in this respect.

==External links==
 
*  

 
 
 
 
 
 