Le Petit Monde de Don Camillo
{{Infobox film
| name           = Le Petit monde de don Camillo (French)  Don Camillo (Italian)
| image          = Le Petit Monde de Don Camillo.jpg
| caption        =
| director       = Julien Duvivier
| producer       =
| writer         = Giovannino Guareschi (novel), Julien Duvivier, Oreste Biancoli, René Barjavel
| narrator       =
| starring       = Fernandel, Gino Cervi, Franco Interlenghi, Vera Talchi
| music          = Alessandro Cicognini
| cinematography = Nicolas Hayer
| editor       = Maria Rosada
| distributor    =
| released       = 1952
| runtime        = 107 min
| country        = Italy, France
| language       = Italian, French 
| budget         =
}}
Le Petit Monde de don Camillo ("The Little World of don Camillo"), or Don Camillo in Italian, is a 1952 French-Italian film directed by Julien Duvivier, starring Fernandel and Gino Cervi. It was the first film in the "Don Camillo" series, which made Fernandel an international star. The film was based on the novel Don Camillo by author Giovannino Guareschi. It was followed in 1953 by The Return of Don Camillo, also directed by Duvivier.

==Synopsis==
The story starts in a small   town, simply known as "a small world", in the  ic appeal by Peppone himself (as well as some admonishment from Jesus of Nazareth|Christ) to a reluctant Don Camillo, the child is baptized in Camillos church. Similar conflicts arising in the course of the story are settled between Don Camillo and Peppone in a similarly conflicting, but ultimately unified fashion, such as: fascists during World War II to finance the construction of his new community hall and blackmails him with this knowledge; getting milked until both Don Camillo and Peppone surreptitiously resolve the problem together; 
*a river blessing procession and the funeral of the towns generally respected old teacher, Ms. Christina, which are both kept strictly non-political despite the Communists initial intentions;

An important side story within the film is the   stall, which results in a public mass brawl.

Even though Peppone resents Don Camillos interferences and their after-effects on personal health, he secretly enjoys their amicable quarrels and repeatedly tries his best to persuade the local bishop not to have Camillo replaced. However, with this last misdeed the bishop decides to send Camillo to a different community, and Peppone has threatened Camillos parish not to say farewell to him as he is about to depart. But to his delightful surprise, Don Camillo does receive a touching goodbye from the people of his town – first from his parish at the train station next town, then from Peppone and his party comrades at the very next station afterward. Before Camillo departs for his new destination, Peppone asks him to come back soon and promises that Camillos successor will not last long under his attention.

==Cast==
*Fernandel ...  Don Camillo
*Gino Cervi ...  Giuseppe Peppone Bottazzi

==Production notes==
This film was produced by Francinex (Paris) and Rizzoli Amato (Rome). It belongs to a long series of Franco-Italian (or Italo-French) coproductions which provided hundreds of movies to the cinema during 30 years after World War II.
In Le Petit Monde de Don Camillo (released in Italy as simply Don Camillo) one of the characteristics is a certain balance between the two countries, since the original author, the place of action, and one of the two stars are Italian, while the director, the screenwriters, and the first star are French. The crew and the rest of the cast are also equally divided between both nations.

During filming, the actors spoke their own language. So there are two originals, one Italian, and one French, in which the actors of the other language are dubbed. The masterful performance of Fernandel is of course particularly valuable in the French version. 

Note however that more often, a bit like in pop music the Lennon-McCartney duet, the Franco-Italian films can be allotted to one or the other country, if we only consider the artistic aspect, cooperation being primarily financial. To satisfy the two parts, they used to incorporate into the casting of a film depending to a country, some actors of the other one. Among a lot of famous examples, we can quote "Il Gattopardo" (English:  ) a typical French cult comedy of  " (English:  ).

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 