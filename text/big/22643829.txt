Rechter Thomas
 
{{Infobox film
| name           = Rechter Thomas
| image          = 
| image_size     = 
| caption        = 
| director       = Walter Smith
| producer       = 
| writer         = François Pauwels (book), Walter Smith (scenario)
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released     =  October 16, 1953
| runtime        = 105 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1953 Netherlands|Dutch film directed by Walter Smith in black and white. The plot is based on the book Rechter Thomas by Francois Pauwels.  The film drew circa 244,000 visitors.

==Cast==
*Piet Bron	... 	Rechter Thomas
*Ton van Duinhoven	... 	Joop
*Johan Valk	... 	Inbreker
*Rini van Slingelandt	... 	Lenie
*Max Croiset	... 	Gevangenisbewaarder
*Henri Eerens	... 	President der Rechtbank
*Bob De Lange	... 	Officier van Justitie
== External links ==
*  

 
 
 

 