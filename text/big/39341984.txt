La Leyenda de la Nahuala
{{Infobox film
| name           = La Leyenda de la Nahuala
| image          = 
| alt            = 
| caption        = 
| director       = Ricardo Arnaiz
| producer       = Jean Pierre Leleu Soco Aguilar Paul Rodoreda
| screenplay     = Omar Mustre Antonio García
| based on       = 
| starring       = 
| music          = Gabriel Villar
| cinematography = 
| editing        = 
| studio         = Animex Producciones Fidecine
| distributor    = Gussi Cinema
| released       = 31 October 2007
| runtime        = 86 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = $4,038,144 
}}
 Mexican animation|animated horror comedy October 31, 2007 in Mexico. It was the sixth animated feature film in Mexico to be released, and the first to be exhibited in DTS (sound system)|DTS.    The film was also a box-office success on its opening weekend.    It was produced by Animex Producciones and directed by Ricardo Arnaíz.

It was released direct-to-video in the United States in 2008.

The film spawned to two sequels, La Leyenda de la Llorona, released on 21 October 2011,  and La Leyenda de las Momias, released on 30 October 2014.

==Plot==
The movie takes place in the year 1807 in the city of Puebla, New Spain. Leo San Juan, a shy 9-year-old boy, lives with his grandmother and his older brother Nando. Leo is constantly frightened, due to the legend of the Nahuala that his brother told him about. The legend speaks of an old abandoned house, in which "La Nahuala", a witch, lives. 52 years ago, this witch took control of the spirits of 2 little girls, Xóchitl (pronounced so - CHEE - ul) and Teodora. These two little girls are still locked up in the old house along with la Nahuala to this day. Now, la Nahuala is looking for a third spirit, because if she manages to get it on the Día de Muertos, she will obtain all the necessary power to eliminate all of the inhabitants of the city.
 sugar skulls, and a Quixotic Spanish ghost in an armor. La Nahuala is at first helped by a man named Santos, who believes she is his mother. In an unexpected turn of events, Santos finds out that La Nahuala is not actually his mother and therefore begins to help Leo. Towards the end of the film, Leo faces La Nahuala and manages to destroy the source of her power. At the end, the friar gives Leo a cross, and with this he will fight all the monsters mentioned in legends from Mexico. Then, a year later, on Día de Muertos, all of the characters whom Leo has met celebrate, and the friar gives Leo his next mission, which is defeating La Llorona.  

==Cast==
*Andrés Bustamante as Don Andrés
*Jesús Ochoa as Santos
*Rafael Inclán as Alebrije
*Martha Higareda as Xóchitl
*María Teresa Cordeiro as Teodora Villavicencio Manuel Loco Valdés as Lorenzo Villavicencio
*Ofelia Medina as La Nahuala
*Fabrizio Santini as Leo San Juan
*Pierre Angelo as Ciego
*Luna Arjona as Nana Dionisia
*Bruno Coronel as Fernando San Juan
*Verónica De Ita as Sra. Pérez
*Ginny Hoffman as Sra. López
*Germán Robles as Friar Godofredo
*María Santander as Toñita San Juan
*Carlos Segundo as Sr. López
*Gabriel Villar as Merenguero
*Grecia Villar as Friar Sinfonolo

==Release== PG "for some scary images, language, thematic elements and rude humor" by the Motion Picture Association of America|MPAA. 

===Box office=== pesos ($1,479,685 USD). It is one of the most successful releases in Mexican cinema history. 

==Awards and nominations==
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Year !! Award !! Category !! Result !! Nominee
|-
| rowspan="2" | 2008 || rowspan="2"| Ariel Award  || Best Animated Feature (Largometraje de Animación) ||   || Ricardo Arnaiz 
|-
| Best Original Score (Música Original) ||   || Gabriel Villar
|-
|}

==Sequels==
Due to the success of the film, a sequel, titled La Leyenda de la Llorona, was released on October 21, 2011.   The third film, titled La Leyenda de las Momias de Guanajuato was released on 30 October 2014 in the 4DX format.  Both of the sequels were animated and produced by Ánima Estudios.   

==See also==
*Animex Producciones
*La Leyenda de la Llorona
*La Leyenda de las Momias

==References==
 

== External links==
*  
*  
* 
* 
* 

 
 
 
 
 
 
 
 
 
 