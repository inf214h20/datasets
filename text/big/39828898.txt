The False Prince (film)
{{Infobox film
| name           = The False Prince 
| image          =
| image_size     = 
| caption        = 
| director       = Heinz Paul
| producer       = Lothar Stark
  | writer         = Harry Domela (book)   Heinz Paul   Hella Moja
| narrator       = 
| starring       = Harry Domela   Ekkehard Arendt   John Mylong   Hans Heinrich von Twardowski
| music          =   
| editing        = 
| cinematography = Gustave Preiss
| studio         = Lothar Stark-Film
| distributor    = Bavaria Film
| released       = 1 December 1927
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent film directed by Heinz Paul and starring Harry Domela, Ekkehard Arendt and John Mylong.  The films art direction was by Karl Machus. The film was based on Domelas book recounting his own adventures in post-First World War Germany when he briefly masqueraded as Prince.

==Cast==
* Harry Domela as Harry Domela 
* Ekkehard Arendt as Baron von Korff, sein Freund 
* John Mylong as Fritz Stein, sein Freund  
* Hans Heinrich von Twardowski as Wolf, sein Freund 
* Hans Mierendorff as Wolfs Vater 
* Mary Kid as Wolfs Schwester 
* Corry Bell as Elizza, ein Flüchtling 
* Adolphe Engers as Frabrikant Müller 
* Else Reval as Seine Frau 
* Alexander Murski as Baron von Raaden 
* Carl Auen as Legationsrat Garry 
* Wilhelm Bendow as Der Hoteldirektor 
* Ferdinand Bonn as Der Intendant 
* Hans Sternberg as Der Bürgermeister 
* Josefine Dora as Seine Frau 
* Maria Forescu   
* Herta Laurin   
* Trude Lehmann   
* Sophie Pagay  
* Lotte Stein  
* Lotte Spira
* Siegfried Berisch   
* Carl Geppert  
* Willy Kaiser-Heyl   
* Hans Leibelt   
* Alfred Loretto   
* Edgar Pauly   
* Paul Rehkopf   
* Fritz Richard   
* Robert Scholz

==References==
 

==Bibliography==
* Hermanni, Horst O. Das Film ABC Band 5: Von La Jana bis Robert Mulligan. Books on Demand, 2011.

==External links==
* 

 
 
 
 
 
 
 
 


 