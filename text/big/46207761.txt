Pact with the Devil (2004 film)
{{Infobox film
| name = Pact with the Devil
| image =
| image_size =
| caption =
| director = Allan A. Goldstein
| producer = Luciano Lisi; Christine Kavanagh; Charmaine Carvalho; Allan A. GoldsteinAlberto Salvatori
| writer =  Ron Raley   Peter Jobin 
| narrator =
| starring = Ethan Erickson   Malcolm McDowell   Christoph Waltz 
| music =  
| cinematography = 
| editing = 
| studio =   
| distributor = 
| released = February 17, 2004
| runtime = 84 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Pact with the Devil  (American) is a 2004 drama film directed by Allan A. Goldstein and starring Ethan Erickson, Malcolm McDowell and Christoph Waltz.

==Cast==
* Malcolm McDowell as Henry
* Ethan Erickson as Louis/Dorian
* Christoph Waltz as Rolf Steiner Victoria Sanchez as Mariella Steiner
* Jennifer Nitsch as Bae
* Ron Lea as Detective Giatti  
* Karen Cliche as Christine
* Amy Sloan as Sybil  
* Carl Alacchi as James  
* Bronwen Booth as Trina  
* Henri Pardo as Trina  
* Luigi Tosi as Cop at Crime Scene 
* Daniela Ferrera as Woman #1 at Dorians Loft 
* Jane McLean as Woman #2 at Dorians Loft
* Ellen David as Diana Baker

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 

 