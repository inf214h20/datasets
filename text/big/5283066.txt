King of the Ants
{{Infobox film
| image          =King of the ants 01.jpg
| alt            =
| caption        =
| director       = Stuart Gordon
| producer       = Duffy Hecht David Michael Latt
| screenplay     = Charlie Higson
| based on       =   Chris McKenna
| music          = Bobby Johnston
| cinematography = Mac Ahlberg
| editing        = David Michael Latt
| studio         = Anthill Productions The Asylum Hecht Productions Red Hen Productions
| distributor    = The Asylum
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} 2003 American Chris McKenna.  It was adapted from Higsons novel of the same name.

== Plot == Chris McKenna), a struggling young man trying to make ends meet by painting houses in suburban Los Angeles. One day, Sean meets "Duke" Wayne (George Wendt), who introduces Sean to his boss, a shady developer named Ray Matthews (Daniel Baldwin). Ray hires Sean as a spy, and orders him to follow Eric Gatley (Ron Livingston), an accountant who has been investigating Rays company. Problems start when Ray, while drunk, offers Sean $13,000 to kill Eric. Sean accepts his offer and, although ambivalent, ends up killing Gatley by breaking into his house and beating him to death. When Sean goes to collect his pay, however, he is double-crossed and when he insists that they pay him, he is kidnapped and taken to Rays secluded farm. It emerges that Matthews never had any intention of paying Sean for the killing, for he only wanted to use and eliminate him. But Sean survives from having a bullet put in his head then reveals that he had taken Gatleys work file of evidence and hidden it along with his documentation of events leading to the murder.

When torture fails to make him disclose the whereabouts of the file, Sean is then brutally beaten about the head with golf clubs many times by Ray and his henchmen daily for weeks, in an effort to destroy his memory. After suffering a heavy amount of trauma, Sean escapes, killing Duke, and finds his way to a downtown homeless shelter where Gatleys widow, Susan (Kari Wuhrer), takes him under her wing, oblivious to his role in her personal tragedy. After she nurses him back to normal, he feels he has been reborn and they become lovers and he moves into her house. But after a few weeks, Susan finds his file describing the murder and, enraged, physically attacks him. Defending himself, Sean accidentally kills her.

Having lost what he saw as his redemption and rebirth, more angry and cynical than ever, Sean returns to Rays farm and methodically and ruthlessly exacts revenge on his captors; having arrived before Ray, he finds Dukes body and decapitates it, removing the wounds that would implicate his involvement. While waiting for Ray, Sean burns Dukes head in a fire pit along with pictures of Susan, exclaiming that if it wasnt for him and his friends he could have had a happy life. He hides in the house while Rays henchmen search for him. Sean jumps Carl upstairs and hits him in the chest with a sledgehammer and kicks him down the stairs, Carl lies on the floor unable to move from internal bleeding. Beckett comes into the house to find Carl, when Sean breaks his leg and hits his back, paralyzing him.

With the two henchmen unable to move, he moves on to Ray, dousing him in gasoline and setting him on fire, he returns to the house to deal with the wounded Carl and Beckett, begging him for medical assistance and asking why. Sean simply replies asking if there needs to be a reason for his revenge. Sean turns the stove on and leaves the house, he changes Rays shoes to make it appear that the trio were working on the house when an accident occurs. Sean lights one of Rays shoes on fire and throws it in the house, Beckett and Carl scream and cry as Sean walks away from the house before it explodes, and the screen fades to black.

The title of the film refers to an episode after the murder, when Sean and Duke are at the zoo and Duke is musing about the game of deciding which animal most closely resembles a person, not physically, but on a deeper level. After roughing Sean up to discourage him from pursuing his quest for payment, he disparagingly remarks that hes found which animal resembles Sean: an ant.

== Cast == Chris McKenna as Sean Crawley
* Kari Wuhrer as Susan Gatley
* George Wendt as Duke Wayne
* Vernon Wells as Beckett
* Lionel Mark Smith as Carl
* Timm Sharp as George
* Daniel Baldwin as Ray Matthews

== Production ==
It is based on a novel by writer Charlie Higson, who also wrote the screenplay for the film.  Actor George Wendt read the novel and contacted Higson about a film adaptation.  Higson replied that there had been interest in the past but nothing had materialized.  Wendt then brought the novel to Stuart Gordons attention, and they were able to get the project off the ground.  Wendt and Gordon had previously worked together in Chicago theater.   It took seven years to find a company willing to produce the film.  The Asylum was the only studio willing to commit to such a dark and violent story.   This was the first film that The Asylum produced; they had previously worked exclusively as a distributor. 

== Release ==
King of the Ants premiered at the 2003 Seattle International Film Festival. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that a rare 100% of nine surveyed critics gave the film a positive review; the average rating was 7/10.   Ken Eisner of Variety (magazine)|Variety wrote that although the film has clever writing, a veteran director, and "starts out engagingly enough", it cant decide whether it is a horror film, neo-noir caper, or psychological thriller.   Marjorie Baumgarten of The Austin Chronicle rated it 3/5 stars and called it "an intriguing indie effort" that is "refreshingly unpredictable".   Ross Williams of Film Threat rated it 4/5 stars and called it Gordons best film since Re-Animator.   Bloody Disgusting rated it 3/5 stars and described it as a brutal, nihilistic film that lacks escapism.   Mike Pinsky of DVD Verdict wrote that the first half of the film has promise, but "the second half of the script is a complete mess." 

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 