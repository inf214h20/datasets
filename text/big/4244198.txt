Balto II: Wolf Quest
 
 
 
{{Infobox film
| name           = Balto II: Wolf Quest
| image          = Balto2.jpg
| image_size     =
| caption        = DVD release cover
| director       = Phil Weinstein
| producer       = Phil Weinstein
| screenplay     = Dev Ross   
| starring       = {{Plainlist|
* Jodi Benson
* David Carradine
* Lacey Chabert
* Mark Hamill
* Maurice LaMarche
* Peter MacNicol
* Charles Fleischer
* Rob Paulsen
* Nicolette Little
* Melanie Spore
* Kevin Schon }}
| music          = Adam Berry
| cinematography =
| editing        = {{Plainlist|
* Jay Bixsen
* Ken Solomon }}
| studio         = Universal Cartoon Studios
| distributor    = Universal Studios Home Entertainment
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
}} Universal Pictures/Amblin Entertainment 1995 animated film Balto (film)|Balto.

==Plot==
Balto and his mate Jenna have a new family of six puppies. Five of their puppies look like their husky mother, while one pup named Aleu clearly takes her looks from her wolfdog father. When they all reach eight weeks old, all of the other pups are adopted to new homes, but no one wants Aleu. Aleu stays with her father, Balto. A year later, after she is almost killed by a hunter, Balto tells Aleu the truth about her wolf heritage. In anger and sadness, she runs away, hoping to find her place in the world.

At the same time, Balto has been struggling with strange dreams of a raven and a pack of wolves and he cannot understand their meaning. When Aleu doesnt come back the next day, he runs off to find her and bring her back home. He meets with mysterious creatures, like a cunning fox, a trio of wolverines that taunt him, the same guiding raven from his dreams, and a furious grizzly bear that suddenly disappears as if it was never there.

During the journey, his friends Boris, Muk and Luk hope to find Balto, but they are halted by some unknown force. They soon realise that this journey to find Aleu is meant for the father and daughter themselves.

Aleu, after taking refuge in a cave, meets the field mouse called Muru who lets Aleu realise that being part-wolf isnt so bad. He teaches her that everyone has a spirit guide. Muru is Aleus spirit guide.

When Aleu and Balto reunite after a close escape from the bear, Aleu realizes she has gained a strange ability that allowed her to see the bears thoughts. Aleu has started to grow, telling her father that shes not going home until she finds out who she is. The two of them travel onward, both following the raven, to a starving pack of wolves by the ocean. They are led by an old wolf named Nava, who has magic powers and can contact the mysterious white wolf Aniu in his "dream visions". He tells his pack that one day soon, they will be led by a new leader, "the one who is wolf but does not know". Everyone believes that Balto, who is half wolf himself, is the chosen one that Aniu was speaking of. However, Niju (a young wolf), hopes that he will be the next leader since he is stronger and more powerful than the old, wise Nava. He plans to accomplish that with his followers Nuk, Yak and Sumac.

The day comes to depart from their home to follow the caribou, the wolves food source, across the large sea using pieces of ice like a bridge, with Balto in the lead. When Nava is separated from the rest of the pack, Aleu joins him to help him across, but runs into Niju, who is ready to take the elderly leaders life and the young half-wolfs as well. Balto abandons the pack to save his daughter, but before anyone gets hurt, they realise that the pack is floating away, leaderless. Nava cannot make the swim in his old age, so Balto tells Niju to be their new leader and to swim across to the pack, but Niju refuses to leave his homeland. Balto is prepared to help the pack, but Aleu realises that this is where she truly belongs. She makes the swim to the pack to become its leader as Nava returns to his home to find Niju. As Balto makes his way back to Nome, Alaska|Nome, the raven reveals its true form as the great white wolf, Aniu, who is Baltos mother.

==Voice cast==
  Balto
* Jodi Benson as Jenna
* Lacey Chabert as Aleu
* David Carradine as Nava
* Mark Hamill as Niju
* Charles Fleischer as Boris
* Peter MacNicol as Muru
* Rob Paulsen as Terrier, Sumac, Wolverine #2
* Nicolette Little as Dingo
* Melanie Spore as Saba
* Kevin Schon as Muk, Luk, Wolverine #1
* Joe Alaskey as Hunter, Nuk
* Monnae Michaell as Aniu
* Mary Kay Bergman as Fox, Wolverine #3 Jeff Bennett as Yak

==Reception==
Rotten Tomatoes page for Balto II: Wolf Quest does not have a rating, because only one review was counted (that one being a negative review). However, out of nine reviews counted by the RT community, the film holds a 56% "rotten" rating. 

==Awards==
Balto II: Wolf Quest was nominated for an Annie Award    in 2003 for "Outstanding Storyboarding in an Animated Television Production". Annies are honors bestowed in animation similar to other awards presented by academies. They honor the industrys highest achievements.

The Humanitas Prize was received by Balto II s screenplayer, Dev Ross, during 2002 for this movie in the Childrens Animation category.  The Humanitas website states that "A signature Humanitas story challenges us to use our freedom to grow and develop, confronts us with our individual responsibility and examines the consequences of our choices."  Other winners of the Humanitas Prize include A River Runs Through It, Schindlers List, and WALL-E.

==References==
 

==External links==
 
* 
* 
*   Info & media about the film.
*  Interview with director Phil Weinstein

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 