Ee Dil Helide Nee Bekkantha
{{Infobox film
| name           = Ee Dil Helide Nee Bekkantha
| image          = Ee Dil Helide Nee Bekkantha poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = TM Srinivasa
| producer       = Sridhara S
| writer         = TM Srinivasa
| starring       = 
| music          = Satish Aryan
| cinematography = 
| editing        = 
| studio         = Sai Krishna Enterprises
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}} Kannada Romance film|romance-drama film directed by TM Srinivasa and produced by Sridhara S under the banner of Sai Krishna Enterprises. It features actors Avinash Diwakar and Sri Sruthi in the lead roles.   

== Cast ==
* Avinash Diwakar
* Sri Sruthi
* Shashank Bangalore Padmanabhan

== Music ==
{{Infobox album  
| Name       = Ee Dil Helide Nee Bekkantha
| Type       = soundtrack
| Artist     = Satish Aryan
| Cover      =
| Size       = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Kamadhenu Audio & Video
| Producer   = 
| Last album = 
| This album = 
| Next album = 
}}
The soundtrack album of the film has been composed by Satish Aryan, who makes his debut as a music director with this. Lyrics of the songs have been penned by S Sridhara. 

=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Singer(s)
| total_length    = 
| all_lyrics      = S Sridhara
| all_music       = Satish Aryan
| title1          = Dildu Formula Tippu
| length1         = 4:30
| title2          = Modala Barige
| extra2          = Shreya Ghoshal, Satish Aryan
| length2         = 4:27
| title3          = Hrudayada Vishaya
| extra3          = Satish Aryan
| length3         = 2:46
| title4          = Lavvu Madi
| extra4          = Vijay Prakash
| length4         = 4:36
| title5          = Savira Januma
| extra5          = Hemanth, Supriya Lohith
| length5         = 4:42
| title6          = Ninu Ethake
| extra6          = Sneha
| length6         = 2:03
| title7          = Hanneru Bindege
| extra7          = Meghana Kulkarni, Sneha
| length7         = 0:42
| title8          = Hennu Ninnadaythu
| extra8          = Rajesh Krishnan, Sneha
| length8         = 5:30
}}

== References ==
 

 
 
 
 
 