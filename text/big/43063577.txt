Her Man (film)
{{infobox film
| name           = Her Man
| image          = Ricardo Cortez Helen Twelvetrees Her Man 1930.jpg
| imagesize      =
| caption        = Ricardo Cortez and Helen Twelvetrees
| director       = Tay Garnett
| producer       = E. B. Derr Pathé Exchange
| writer         = Tay Garnett (story) Howard Higgin(story) Tom Buckingham
| starring       = Phillips Holmes Helen Twelvetrees Marjorie Rambeau
| music          = Josiah Zuro Edward Snyder
| editing        = Doane Harrison Joseph Kane
| distributor    = Pathé Exchange
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
}}
Her Man is a 1930 Pre-Code Hollywood|Pre-code talking film drama produced and distributed by Pathé Exchange and directed by Tay Garnett. It starred Phillips Holmes, Helen Twelvetrees and Marjorie Rambeau. The film is a slightly disguised version of the stage play Frankie and Johnny. 

At least one copy is preserved at the Library of Congress. 

==Plot==
A Paris bargirl with tough "protector" falls for young sailor.

==Cast==
*Helen Twelvetrees - Frankie Keefe
*Phillips Holmes - Dan Keefe
*Marjorie Rambeau - Annie
*James Gleason - Steve
*Ricardo Cortez - Johnnie
*Harry Sweet - Eddie
*Slim Summerville - The Swede
*Thelma Todd - Nelly
*Franklin Pangborn - Sport
*Stanley Fields - Al
*Matthew Betz - Red
*Mike Donlin - Bartender

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 

 