Peter and the Magic Egg
{{Infobox film
| name        = Peter and the Magic Egg Fred Wolf
| producer    = Fred Wolf
| writer      = Romeo Muller
| starring    = Ray Bolger
| music       = Howard Kaylan Mark Volman
| studio      = Murakami-Wolf-Swenson
| distributor = Liberation Entertainment
| released    =  
| runtime     = 24 minutes
| country     = United States English
}}
 1983 musical animated Easter holiday. It is narrated as story by Uncle Amos the egg, voiced by Ray Bolger.

==Plot== colored eggs for Easter. He is helped by  cast of anthropomorphic farm animals to produce and dye the eggs and make the annual mortgage payment on Easter day. Tinwhiskers, enraged that he cannot repossess the farm, challenges Peter to a ploughing contest and arranges for Peter to fall down a well. Peter remains in a coma and sadness hangs over the farm until and egg provided by Mother Nature hatches into the Kookibird bringing laughter back to the farms. This wakes Peter and returns Tinwhiskers to human form. Tobias Toot gives back the town, renamed Passville, and goes to work for the Dopplers while Peter leaves the farm to return to Mother Nature to help other families in need.

==Cast==

*Ray Bolger as Uncle Amos Egg Al Eisenmann as Peter Paas
*Joan Gerber	Joan Gerber as Mama Doppler
*Bob Holt as Papa Doppler / Kookybird
*Robert Ridgely as Tobias Tinwhiskers
*Russi Taylor
*Charles Woolf

==Trivia==

This special was created as a tie-in to Paas Easter egg dye.

==External links==
* 
* 
* 

 
 
 

 