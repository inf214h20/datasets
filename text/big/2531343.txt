Bomb the System
 
{{Infobox film
| name           = Bomb the System
| image          = Bomb the System film poster.jpg
| caption        = Promotional movie poster
| director       = Adam Bhala Lough
| producer       = Ben Rekhi Sol Tryon
| writer         = Adam Bhala Lough Mark Webber Jaclyn DeSantis Gano Grills Jade Yorker
| music          = Sebastian Demian El-P Ethan Higbee International Friends
| cinematography = Ben Kutchins Ben Rekhi Jay Rabinowitz
| distributor    = Palm Pictures
| released       = 2002
| runtime        = 91 min.
| country        = United States
| awards         = English / Hindi
| budget         =
| gross          =  $15,520 
| preceded by    =
| followed by    =
}} drama film American theaters Mark Webber, Gano Grills, Jaclyn DeSantis, Jade Yorker, Bönz Malone, Kumar Pallana and Joey SEMZ. Bomb the System was the first major fictional feature film about the subculture of graffiti art since Wild Style was released 1982.  Several well-known graffiti artists participated in the making of the film including Lee Quinones, Cope2 and Chino BYI. The films score and soundtrack were composed by El-P.

In January 2004 the film was nominated for an Independent Spirit Award for Best First Feature. 

== Brief history ==
The film was produced for US$500,000, expanded from the directors thesis project at NYU. The director, producer, cinematographer, and other key members of the crew had recently graduated from NYU and this was their first film. The film played at over 26 film festivals on four continents and was first offered distribution by Now on Media in Japan after the companys president saw a 1-minute clip at the 2004 Independent Spirit Awards. The film received a wide theatrical release in Japan and extensive press coverage.  Palm Pictures distributed the film in the US and gave it a limited 2-theater release in New York City and Los Angeles on May 27, 2005. Palm decided not to open the film wider despite positive reviews in The New York Times, LA Times, Variety, Rolling Stone, and The Village Voice and a per-screen average gross of US$4,588.  The film was released on DVD October 8, 2005. 

== Critical response ==
 . Bomb the System, which rides on a subtle hip-hop soundtrack, might be described as soulful pulp; cult recognition awaits it." 

The negative reviews were mainly scathing. The New York Post called the film "a mild, slow-moving drama that belatedly tries to argue that graffiti writers are political artists, not an urban blight"; The New York Daily News called the film "brashly passionate in its desire to express the power and validity of graffiti art. But its also preachy and single-minded, populated by a world of sympathetic heroes and hissable villains"; and Sean Axmaker in the Seattle Post-Intelligencer likened the film to "tomcats spraying outside their yards." 

Jim Jarmusch wrote, "For Bomb the System director Adam Lough takes far more inspiration from the on-going graffiti culture than from the depleted stylistic formulas of recent commercial cinema. His refreshing use of skewed camera angles, blasts of color, and inventive cutting are deftly blended, becoming much more than calculated atmosphere. The performances are also consistently strong, and Mark Webber in particular, in the central role, never hits a false note. Bomb the System is welcome proof that the spirit of graffiti writing has a continuing cultural influence on both the subtleties of form and explosive personal expression." Parts of the quotation ran in a Village Voice ad on the second weekend of the films release.

== Sticker controversy ==
Shortly after the theatrical release, a movie theater in Delaware was closed down after a promotional Bomb the System sticker was found illegally posted in the theater. Due to fear of terrorism, the theater manager called the police and bomb squad and the theater was shut down for a few hours while the canine unit sniffed for bombs. Nothing was found.  In all actuality, "Bombing" has nothing to do with actual explosives in graffiti terminology - "to bomb" is urban slang for covering a surface with graffiti. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 