Bugsy
 
{{Infobox film
| name = Bugsy
| image = Bugsy poster.jpg
| image_size = 220px
| alt = 
| caption = Theatrical release poster
| director = Barry Levinson Mark Johnson
| screenplay = James Toback
| based on =  
| starring = Warren Beatty and Annette Bening
| music = Ennio Morricone
| cinematography = Allen Daviau
| editing = Stu Linder Christopher Holmes ( )
| studio = Baltimore Pictures
| distributor = TriStar Pictures
| released =  
| runtime = 137 minutes     150 minutes  
| country = United States
| language = English
| budget = $30 million 
| gross = $49,114,016 
}} Bill Graham.

The screenplay was written by James Toback from research material by Dean Jennings 1967 book We Only Kill Each Other.

There is a Directors Cut released on DVD, containing an additional 13 minutes not seen in the theatrical version.

==Plot== Benjamin "Bugsy" New York mob, goes to California and instantly falls in love with Virginia Hill, a tough-talking Hollywood starlet. The two meet for the first time when Bugsy visits his friend, actor George Raft, on a film set. He buys a house in Beverly Hills from opera singer Lawrence Tibbett, planning to stay while his wife and two daughters remain in Scarsdale.
 Charlie Luciano, Bugsy is in California to wrestle control of betting parlors away from Los Angeles gangster Jack Dragna. Mickey Cohen robs Dragnas operation one day. He is confronted by Bugsy, who decides he should be in business with the guy who committed the robbery, not the guy who got robbed. Cohen is put in charge of the betting casinos; Dragna is forced to admit to a raging Bugsy that he stole $14,000, and is told he now answers to Cohen.

After arguments about Virginias sexual trysts with drummer Gene Krupa and a variety of bullfighters and Siegels reluctance to get a divorce, Virginia makes a romantic move on Bugsy. On a trip to Nevada to visit a gambling joint, Bugsy comes up with the idea for a hotel and casino in the desert. He obtains $1 million in funding from lifelong friend Lansky and other New York mobsters, reminding them that in Nevada, gambling is legal.
 Las Vegas, Nevada, but the budget soon soars to $6 million due to his extravagance. Bugsy tries everything to ensure it gets completed, even selling his share of the casino.

Bugsy is visited in Los Angeles by former associate Harry Greenberg. Harry has betrayed his old associates to save himself. Harry has also run out of money, from a combination of his gambling habits and being extorted by prosecutors who want his testimony. Though he is Harrys trusted friend, Bugsy has no choice but to kill Harry. He is arrested for Harrys murder, but the only  witness is a cab driver who dropped off Harry in front of Bugsys house. The driver is paid off to leave town, escorted to the train station by Cohen.

Lansky is waiting for Bugsy outside the jail. He gives a satchel of money to his friend. "Charlie doesnt have to know about it," he tells Bugsy, but warns, "I cant protect you anymore." The Flamingos opening night is a total failure, and $2 million of the budget is unaccounted for, whereupon Bugsy discovers that Virginia stole the money. He tells her to "keep it and save it for a rainy day." He then tells Lansky never to sell his share of the casino because he will live to thank him someday.

Later that night, Bugsy is killed in his home by several gunshots. Virginia is told the news in Las Vegas and knows her own days could be numbered. An epilogue states that Virginia returned the missing money a week later and committed suicide at some point after that. It also states that by 1991, the $6 million invested in Bugsys dream of Las Vegas had generated revenues of over $100 billion.

==Cast== Benjamin "Bugsy" Siegel 
* Annette Bening as Virginia Hill
* Harvey Keitel as Mickey Cohen
* Ben Kingsley as Meyer Lansky
* Elliott Gould as Harry Greenberg
* Joe Mantegna as George Raft
* Bebe Neuwirth as Countess di Frasso Bill Graham Charlie "Lucky" Luciano
* Lewis Van Bergen as Joe Adonis Esta Siegel
* Richard C. Sarafian as Jack Dragna
* Giancarlo Scandiuzzi as Count di Frasso

==Production== Bonnie and John Reed in Reds, he felt he was the right actor to play both Bugsy and Hughes.
 Hollywood and desert city in which legal gambling is allowed. Several filmmakers attempted to make a film based on Bugsys life, most famously French director Jean-Luc Godard, who wrote a script entitled The Story and envisioned Robert De Niro as Siegel and Diane Keaton as Virginia Hill. Heaven Can Wait. Years later, when Beatty was in pre-production on Ishtar (film)|Ishtar, he contacted Toback and asked him to write a script on Bugsy. 

During the course of six years and in between two films that he was involved in, Toback wrote a 400-page document of Bugsys life, however, under some strange circumstances, Toback lost the entire document. Under pressure from Warner Bros., whom Beatty learned also had a Bugsy Siegel-script ready to be produced, Beatty pursued Toback to write a script on Bugsy based on the lost document Toback wrote. Toback handed his new script to Beatty and Beatty approved it and went to several studios in hopes of financing and distributing the film. Beatty presented Tobacks script to Warner Bros. and claimed their script version of Bugsy was a much better one than the one Warner Bros. was interested in producing. Needless to say, Warner Bros. passed on the project and Beatty eventually got the film ready to be made at TriStar Pictures.

Initially, Toback was under the impression that he would be the director of the film. For a while, Beatty could not find a director to direct the film (he didnt know or chose not to know of Tobacks desire to direct the film). Beatty feared that he would be stuck in the position of being the director of the film. He said, "Im in just about every scene of the picture, and I didnt want to have to do all that other work." However, Beatty announced to Toback that Barry Levinson was on board to direct Bugsy. At first he was disappointed, but Toback quickly learned that Levinson was the right person to direct the film. Despite the length of the script (which would have been a three and a half to four hours length feature film), Beatty, Levinson and Toback condensed the script into a two and half/three hour structure script. The three (Beatty, Levinson and Toback) worked very closely during the production of the film and all three worked very closely and in a very collaborative way.

During casting, Beatty wanted  )  Later that summer, Bening was pregnant with her and Beattys first child, which resulted in tabloid/media frenzy at the time. It was also during this time that Beatty asked Bening for her hand in marriage.
 Golden Globe Best Drama Award and the film nominated for 10 Academy Awards and won two.

Warren and Annettes first child was born January 8, 1992 and Warren Beatty and Annette Bening were married on March 12, 1992. The couple are still married and they are the parents of four children.

==Reception==

===Critical Response===
 
Bugsy received very positive reviews from critics. It currently has an 85% "Fresh" rating on Rotten Tomatoes based on 59 reviews.

===Accolades=== Best Art Best Costume Best Actor Best Actor Best Cinematography, Best Director, Best Music, Best Picture Best Writing, The Silence of the Lambs won many categories where Bugsy received nominations in 1991. The film was nominated for the Golden Bear at the 42nd Berlin International Film Festival.   

* American Film Institute Lists
** AFIs 100 Years...100 Heroes and Villains:
*** Bugsy Siegel – Nominated Villain 
** AFIs 100 Years...100 Movie Quotes:
*** "Why dont you go outside and jerk yourself a soda?" – Nominated 
** AFIs 10 Top 10 – Nominated Gangster Film 

==Inaccuracies==
 
The film shows Siegel closing the Flamingo on Christmas of 1946 for improvements and being murdered that night alone at Virginia Hills house. While Siegel did close the hotel that day, his murder took place six months later in June 1947. Furthermore, he was with associate Allen Smiley.

The film also completely ignores the role of William Wilkerson (The Man Who Built Las Vegas) in the building of the Flamingo; Siegel is shown gazing over an empty desert and deciding to build the Flamingo, but the hotel was conceived and constructed wholly by Wilkerson — Siegel only became involved as it neared completion (Wilkerson owned 48% of the Flamingo until he sold out much later).

==References==
 

==External links==
*  
*  
*  

 
 
{{Succession box |
  | before = Dances with Wolves Scent of a Woman Golden Globe for Best Picture – Drama
  | years = 1992|}}
 

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 