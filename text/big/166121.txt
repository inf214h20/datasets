The World Is Not Enough
 
 
{{Infobox film
| name = The World Is Not Enough
| image = The World Is Not Enough (UK cinema poster).jpg
| alt=Poster shows a circle with Bond flanked by two women at the centre. Globs of fire and action shots from the film are below. The films name is at the bottom.
| caption = British cinema poster for The World Is Not Enough, designed by Brian Bysouth
| director = Michael Apted
| producer = Michael G. Wilson Barbara Broccoli
| based on =   Neal Purvis Robert Wade Bruce Feirstein
| story = Neal Purvis Robert Wade
| starring = Pierce Brosnan Sophie Marceau Robert Carlyle Denise Richards Robbie Coltrane Judi Dench
| cinematography = Adrian Biddle, BSC Jim Clark
| music = David Arnold
| distributor = Metro-Goldwyn-Mayer United International Pictures (UK)
| studio = Eon Productions
| released =  
| runtime = 128 minutes
| country = United Kingdom
| language = English
| budget = $135 million
| gross = $361,832,400
}}

  in spring 2000.]] MI6 agent James Bond. Neal Purvis, Robert Wade and Bruce Feirstein.  It was produced by Michael G. Wilson and Barbara Broccoli. The title is taken from a line in the 1963 novel On Her Majestys Secret Service (novel)|On Her Majestys Secret Service.

The films plot revolves around the assassination of billionaire Sir Robert King by the terrorist Renard, and Bonds subsequent assignment to protect Kings daughter Elektra, who had previously been held for ransom by Renard. During his assignment, Bond unravels a scheme to increase petroleum prices by triggering a nuclear meltdown in the waters of Istanbul.

Filming locations included Spain, France, Azerbaijan, Turkey and the UK, with interiors shot at Pinewood Studios. Despite mixed critical reception, The World Is Not Enough earned $361,832,400 worldwide. It was also the first Eon Productions|Eon-produced Bond film to be officially released by Metro-Goldwyn-Mayer instead of United Artists, the original distributor.

==Plot== James Bond his assistant before he can reveal the assassins name. Bond escapes with the money.
 Thames to the Millennium Dome, where the assassin attempts to escape via hot air balloon. Bond offers her protection, but she refuses. She causes the balloon to explode, killing herself.

Bond traces the recovered money to Renard, a KGB agent-turned-terrorist. Following an earlier attempt on his life by MI6, Renard was left with a bullet in his brain which is gradually destroying his senses, making him immune to pain. M assigns Bond to protect Kings daughter, Elektra; Renard previously abducted and held her for ransom, and MI6 believes that he is targeting her a second time. Bond flies to Azerbaijan, where Elektra is overseeing the construction of an oil pipeline. During a tour of the pipelines proposed route in the mountains, Bond and Elektra are attacked by a hit squad in armed, paragliding|paraglider-equipped snowmobiles.
 Valentin Zukovsky ICBM base nuclear physicist Christmas Jones and enters the silo. Inside, Renard removes the GPS locator card and weapons-grade plutonium from a bomb. Before Bond can kill him, Jones blows his cover. Renard steals the bomb and flees, leaving everyone to die in the booby-trapped missile silo. Bond and Jones escape the exploding silo with the locator card.
 inspection rig heading towards the oil terminal. Bond and Jones enter the pipeline to deactivate the bomb, and Jones discovers that half of the plutonium is missing. They both jump clear of the rig and a large section of the pipe is destroyed. Bond and Jones are presumed killed. Back at the command centre, Elektra reveals that she killed her father as revenge for using her as bait for Renard. She abducts M, whom she resents for advising her father not to pay the ransom money.
 Bullion blows up the command centre. Zukovsky is knocked unconscious, and Bond and Jones are captured by Elektras henchmen. Jones is taken aboard the submarine, which was seized by Renards men. Bond is taken to the tower, where Elektra tortures him with a garrote. Zukovsky and his men seize the tower, but Zukovsky is shot by Elektra. The dying Zukovsky uses his cane gun to free Bond. Bond frees M and kills Elektra.

Bond dives after the submarine, boards it and frees Jones. Following a fight, the submarine starts to dive, and hits the bottom of the Bosphorus, causing its hull to rupture. Bond catches up with Renard and fights and kills him. Bond and Jones escape from the submarine, leaving the flooded reactor to detonate safely underwater.

==Cast== James Bond, 007.
* Sophie Marceau as Elektra King, an oil heiress who is seemingly being targeted by Renard, the worlds most wanted terrorist. Bond is tasked by M to protect her at all costs, although he suspects that there is more to her than meets the eye.
* Robert Carlyle as Renard, a former KGB agent turned high-tech terrorist. Years ago, Renard kidnapped Elektra King in exchange for a massive ransom demand. The ordeal resulted in a failed assassination attempt by MI6 and left Renard with a bullet lodged in his brain which renders him impervious to pain as well as slowly killing off his other senses. Renard now seeks revenge on both the King family and MI6.
* Denise Richards as Christmas Jones, a nuclear physicist assisting Bond in his mission.  Richards stated that she liked the role because it was "brainy", "athletic", and had "depth of character, in contrast to Bond girls from previous decades". 
*   boss and Baku casino owner. Bond initially seeks out Zukovsky for intel on Renard and is subsequently aided by him when Zukovskys nephew falls into Renards captivity. Coltrane reprises his role from GoldenEye.
* Judi Dench as M (James Bond)|M: The head of MI6.
* Colin Salmon as Charles Robinson: The Chief of Staff of MI6
* Desmond Llewelyn as Q (James Bond)|Q: MI6s "quartermaster" who supplies Bond with multi-purpose vehicles and gadgets useful for the latters mission. The film would be Llewelyns final performance as Q. Although the actor was not officially retiring from the role, the Q character was training his eventual replacement in this film. Llewelyn was killed in a car accident shortly after the films premiere.
*  : Qs assistant and successor. The character is never formally introduced as "R" – This was simply an observation on Bonds part: "If youre Q....does that make him R?"
* Samantha Bond as Miss Moneypenny: Ms secretary
* Serena Scott Thomas as Dr. Molly Warmflash: An MI6 physician who gives 007 "A clean bill of health."
* John Seru as Gabor: Elektra Kings bodyguard who is seen accompanying King wherever she travels.
* Ulrich Thomsen as Sasha Davidov: Elektra Kings head of security in Azerbaijan and Renards secret liaison.
* Goldie as Bullion: Valentin Zukovskys gold-toothed bodyguard.
* Maria Grazia Cucinotta as Giulietta da Vinci, credited in the film as "Cigar Girl": An experienced assassin working for Renard. David Calder as Sir Robert King: Elektras father and an oil tycoon who is later killed during a bomb attack on MI6 headquarters.

==Production== The Lord of the Rings.  MI6 Headquarters Foreign Office spokesperson rejected the claims and expressed displeasure with the article. 
 Sir Thomas On Her film adaptation, this is revealed to be the Bond family motto. The phrase originates from the epitaph of Alexander the Great. 
 Dana Stevens did an uncredited rewrite before Bruce Feirstein, who worked in the previous two films, took over the script. 

===Filming===
  that is used in the film. The backdrop is intended to be Azerbaijan.]] Guggenheim Museum. Eilean Donan castle in Scotland is used by MI6 as a location headquarters. Other locations include Baku, Azerbaijan, the Azerbaijan Oil Rocks and Istanbul, Turkey, where Maidens Tower, Istanbul|Maidens Tower is shown.   
 Guggenheim Museum. parking wardens was filmed at Wapping and the boat stunts in Millwall Dock and under Glengall Bridge were filmed at the Isle of Dogs. Chatham Dockyard was also used for part of the boat chase.  Stowe School, Buckinghamshire, was used as the site of the King family estate on the banks of Loch Lomond. Filming continued in Scotland at Eilean Donan Castle which was used to depict the exterior of MI6 temporary operations centre "Castle Thane". The skiing chase sequence in the Caucasus was shot on the slopes of Chamonix, France.  Filming of the scene was delayed by an avalanche; the crew helped in the rescue operation. 

 .]]
The interior (and single exterior shot) of LOr Noir casino in Baku, Azerbaijan, was shot at Halton House, the Officers Mess of RAF Halton. RAF Northolt was used to depict the airfield runway in Azerbaijan. 
Zukovskys quayside caviar factory was shot entirely at the outdoor water tank at Pinewood.

The exterior of Kazakhstan nuclear facility was shot at the Bardenas Reales, in Navarre, Spain, and the exterior of the oil refinery control centre at the Motorola building in Groundwell, Swindon.  The exterior of the oil pipeline was filmed in Cwm Dyli, Snowdonia, Wales, while the production teams shot the oil pipeline explosion in Hankley Common, Elstead, Surrey. Istanbul, Turkey, was indeed used in the film and Elektra Kings Baku villa was actually in the city, also using the famous Maidens Tower which was used as Renards hideout in Turkey. The underwater submarine scenes were filmed in The Bahamas. 

The BMW Z8 driven by Bond in the film was the final part of a three-film product placement deal with BMW (which began with the Z3 in GoldenEye and continued with the 750iL in Tomorrow Never Dies) but, due to filming preceding release of the Z8 by a few months, several working mock-ups and models were manufactured for filming purposes.

===Music===
  Don Black Scott Walker, is the nineteenth and final track on the album and its melody is Elektra Kings theme. The theme is heard in "Casino", "Elektras Theme" and "I Never Miss".    Arnold added two new themes to the final score, both of which are reused in the following film, Die Another Day.
 The World Is Not Enough", was written by David Arnold with Don Black and performed by Garbage (band)|Garbage. It is the fifth Bond theme co-written by Black, preceded by "Thunderball (soundtrack)|Thunderball", {{cite video date = 25 February 2003 title = Thunderball url = medium = Audio CD publisher = EMI accessdate =27 November 2007 id = UPN: 7-2435-80589-2-5 isbn = oclc = quote = Diamonds Are Forever", {{cite video date = 11 February 2003 title = Diamonds Are Forever url = medium = Audio CD publisher = EMI accessdate =27 November 2007 id = UPN: 7-2435-41420-2-4 isbn = oclc = quote = The Man with the Golden Gun", {{cite video date = 25 February 2003 title = The Man with the Golden Gun url = medium = Audio CD publisher = EMI accessdate =27 November 2007 id = UPN: 7-2435-41424-2-0 isbn = oclc = quote = Tomorrow Never Dies". {{cite video date = 25 November 1997 title = Tomorrow Never Dies url = medium = Audio CD publisher = A&M Records accessdate =27 November 2007 id = UPN: 7-3145-40830-2-7 isbn = oclc = quote =
}}  Garbage also contributed to the music heard during the chase sequence ("Ice Bandits"), which was released as the  s "Top 89 Songs of 1999"  and No. 100 in WIQI|Q101s "Top 101 of 1999". 

==Release and reception==
The World Is Not Enough premiered on 19 November 1999 in the United States and on 26 November 1999 in the United Kingdom.  At that time MGM signed a marketing partnership with MTV, primarily for American youths, who were assumed to have considered Bond as "an old-fashioned secret service agent". As a result MTV broadcast more than 100 hours of Bond-related programmes immediately after the film was released, most being presented by Denise Richards. 
 Academy Award Blockbuster Entertainment BMI Film 1999 Razzie Awards. Richards and Brosnan were also nominated for "Worst Screen Couple".  
 commentary tracks—one video game, and the Garbage music video.  The Ultimate Edition released in 2006 had as additional extras a 2000 documentary named "Bond Cocktail", a featurette on shooting the Q Boat scenes, Pierce Brosnan in a press conference in Hong Kong, deleted scenes, and a tribute to Desmond Llewelyn. 

Reception was mixed. Chicago Sun-Times critic Roger Ebert said the film was a "splendid comic thriller, exciting and graceful, endlessly inventive", and gave it three-and-a-half stars out of four.    On the other hand, Eleanor Ringel Gillespie of The Atlanta Journal-Constitution disliked the film, calling it "dated and confused".  Rotten Tomatoes gave The World Is Not Enough a 51% rating,    and Metacritic gave the film a score of 59 out of 100.  Negative criticism was focused on the execution of the plot, and the action scenes were considered excessive.  Entertainment Weekly picked it as the worst Bond film of all time, saying it had a plot "so convoluted even Pierce Brosnan has admitted to being mystified".  Norman Wilner of MSN chose it as the third worst film, above A View to a Kill and Licence to Kill,  while IGN chose it as the fifth worst. 

Richards was criticised as not being credible in the role of a nuclear scientist.    She was ranked as one of the worst Bond girls of all time by Entertainment Weekly in 2008.  Richards has gone on to top numerous polls as "Worst Bond Girl".

==Adaptations==
 

The film was adapted into a trading card series which was released by Inkworks. Bond novelist Raymond Benson wrote his adaptation of The World Is Not Enough from the films screenplay. It was Bensons fourth Bond novel and followed the story closely, but with some details changed. For instance, Elektra sings quietly before her death and Bond still carries his Walther PPK instead of the newer P99. The novel also gave the cigar girl/assassin the name Giulietta da Vinci and retained a scene between her and Renard that was cut from the film (this scene was also retained in the card series).
 of the Black Ops. PC and the PlayStation 2 were planned for release in 2000, but both were cancelled.  These versions would have used the id Tech 3 game engine. Although this game marks Pierce Brosnans fifth appearance in a Bond video game, the game includes only his likeness; the character is voiced by someone else.

==See also==
 
* Outline of James Bond

==References==
* {{Cite book
 | last = Simpson
 | first = Paul
 | editor =
 | others =
 | title = The Rough Guide to James Bond
 | origyear =
 | month =
 | url =
 | accessdate =11 December 2007
 | edition =
 | series =
 | date = 7 November 2002
 | publisher = Rough Guides
 | location = London
 | isbn = 1-84353-142-9
 | oclc =
 | id =
 | chapter =
 | chapterurl =
 | quote =
 | ref =
 }}
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 