The Happy Family (1952 film)
 
 
{{Infobox film
| name           = The Happy Family
| image          =
| caption        =
| director       = Muriel Box
| producer       = Sydney Box   William MacQuitty
| writer         = Michael Clayton Hutton Muriel Box Sydney Box
| starring       = Stanley Holloway Kathleen Harrison Naunton Wayne Eileen Moore
| music          = Francis Chagrin
| cinematography = Reginald H. Wyer
| editing        = Jean Barker
| studio         = London Independent Producers
| distributor    = Apex Film Distributors  Souvaine Selective Pictures
| released       =  
| runtime        = 86 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}

The Happy Family is a 1952 British comedy film directed by Muriel Box and starring Stanley Holloway, Kathleen Harrison and Naunton Wayne.  The plot of the film centres on resistance by a family to the disruption caused by the construction of the Festival of Britain. It is also known by the alternative title Mr. Lord Says No. It was an adaptation of a play by Michael Clayton Hutton.

==Plot== Festival Hall which is noisily under construction. It is owned by the Lord family, a husband and wife with several children. Lillian Lord runs the shop, while Henry is a British Railways train driver, who has worked on the railways for over 30 years and who is just about to retire. He is looking forward to enjoying a quiet retirement at the family shop looking after his pet hare, Winston, though his spiritualist sister in law Ada has had supernatural visions of "men in black" bringing discord.

Their plans are disrupted by the arrival of Filch, a senior civil servant dressed in a black suit. He announces that he is overseeing work on the Festival of Britain, due to begin in just six weeks. He explains that, due to an error by one of the planners, the Lords shop and house will have to be demolished to allow an entrance route to be built assuring them that they will be financially compensated and will be moved to a new house in South Harrow. He expects this to settle the matter. However, the Lords show reluctance to leave their house with Henry demanding £6 million if he is to move; an amount he calculates by Mr Filchs account of the estimate of the monetary value the Festival of Britain will bring.  Filch goes away hoping either to buy them off eventually or to forcibly evict them.

Filch has underestimated how attached they are to their property, which is a symbol of security and family to them after their years of hardship during the Great Depression and the Second World War where they lost a son. In an attempt to halt the eviction the Lords appeal to a series of political figures including their councillor, mayor and Member of Parliament|MP. They are eventually sent to the official in charge of the work, who insists it must go ahead. They are served with eviction notices, and demolition is due to begin in a few days. Filch expects them to leave now, but they are unmoved by these threats, declaring that they would rather go to jail than South Harrow. When it becomes clear that their appeals from political channels are not working, the Lords turn to more active resistance at the urging of Cyril, their daughters fiancé. They begin barricading their house and preparing to fight the governments attempts to turn them out. At the appointed hour, Filch demands they leave the house but they refuse. They are joined by Maurice Hennessey, an ambitious BBC broadcaster hoping to use the case as a springboard to greater career success. He begins a running commentary on the events to the outside world.

Filch brings in a large number of police who attempt to storm the shop, but are driven off by missiles and flour bombs. After the assault descends into chaos, Filch launches a prolonged siege in the hope of starving them out. The Lords soon become a cause célèbre, with support coming in from across the world, putting further pressure on the civil servants who are desperate to get work completed before the Festival begins.

In spite of their popularity, the Lords situation begins to grow desperate as they run out of food. Just as they are about to give in Filch arrives and announces that, following the personal intervention of the Prime Minister, the architects have redrawn their plans and the road will now go either side of the shop thereby saving it from demolition. The film ends with the family including Winston enjoying a day out at the Festival of Britain with Ada flying into the clouds.

==Cast==
* Stanley Holloway – Henry Lord
* Kathleen Harrison – Lillian Lord
* Naunton Wayne – Mr. Filch
* Dandy Nichols – Ada John Stratton – David
* Eileen Moore – Joan
* Shirley Mitchell – Marina
* Margaret Barton – Anne George Cole – Cyril Tom Gill – Maurice Hennessey
* Miles Malleson – Mr. Thwaites
* Geoffrey Sumner – Sir Charles Spanniell
* Laurence Naismith – Councillor
* Edward Lexy – Alderman Cameron Hall – Mayor
* Hal Osmond – BR Shop Steward
* John Salew – Mr. Granite
* Ernest Butcher – Neighbour
* Lyn Evans – Neighbour Michael Ward – BBC announcer
* Richard Wattis – M.P.
* David Keir – Process Server
* Anthony Oliver – Fireman
* Campbell Singer – Policeman Peter Martyn – Policeman
* Arthur Hambling – Granger
* Eileen Way – Mrs. Potter

==Production==
The film was released in the year following the real Festival of Britain which had taken place successfully in 1951. It was given a working title of South Bank Story which was later changed to The Happy Family. 

==References==
 

==Bibliography==
* Geraghty, Christine. British cinema in the fifties: gender, genre and the new look. Routledge, 2000.
* Spicer, Andrew. Sydney Box. Manchester University Press, 2006.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 