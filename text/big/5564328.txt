Neighbors (1920 film)
{{Infobox film name            = Neighbors image           = Buster Keaton (left) and Joe Roberts in the movie Neighbors (1920).jpg caption         = Buster Keaton and Joe Roberts in a still from Neighbors (1920) director        = Edward F. Cline Buster Keaton producer        = Joseph M. Schenck writer          = Edward F. Cline Buster Keaton starring  Jack Duffy music           = cinematography  = Elgin Lessley editing         = distributor     = Metro Pictures released        =   runtime         = 18 minutes language  English intertitles budget          = country         = United States
}}
 1920 short comedy film co-written, co-directed by, and starring comedian Buster Keaton.

==Plot==
Buster Keaton and Virginia Fox play young lovers who live in tenements, the rear of which face each other, with backyards separated by a wooden fence.  Their families feud over the lovers relationship, resulting in much mayhem and slapstick.

==Cast==
* Buster Keaton - The Boy
* Virginia Fox - The Girl
* Joe Roberts - Her Father
* Joe Keaton - His Father
* Edward F. Cline - The Cop
* Jack Duffy - The Judge
* The Flying Escalantes - Themselves

==See also==
* List of American films of 1920
* Buster Keaton filmography

==External links==
*  
*  
*   on YouTube
*  

 

 
 
 
 
 
 
 
 
 
 


 