The Father and the Foreigner
{{Infobox film
 | name = The Father and the Foreigner
 | image = The Father and the Foreigner.jpg
 | caption =
 | director = Ricky Tognazzi
 | writer =   
 | starring =  
 | music =   
 | cinematography =  Tani Canevari
 | editing =   
 | producer =   
 | released =  
 | language = Italian
 | country = Italy
 }} 2010 Cinema Italian drama film directed by Ricky Tognazzi. It is based on the novel Crime Novel by Giancarlo De Cataldo.  

== Plot ==
Diego (Alessandro Gassman), an employee from Rome with a disabled son, meets Walid (Amr Waked), a rich Syrian businessman who son is also handicapped. From their shared suffering blooms quite an unusual friendship, and the two dads start spending time together at Turkish baths, luxury shopping sprees, and meeting a mysterious sister in law named Zaira (Nadine Labaki). With a private jet, they head to Syria to see the plot of land Walid purchased for his son. Once back from that short and rather unusual trip, the pain preventing Diego and his wife to enjoy some passionate moments starts fading away, while Walid disappears following terrorist allegations. Tailed by the Secret Service in a suffocating and ambiguous Rome, Diego sets out looking for the man, uncovering a shockingly sad truth. 

== Cast ==
*Alessandro Gassman: Diego
*Kseniya Rappoport: Lisa
*Amr Waked: Walid
*Leo Gullotta: Santini
*Mohamed Zouaoui: Michel Arabesque
*Nadine Labaki: Zaira
*Emanuele Salce: Mazzoleni

==References==
 

==See also==
*Movies about immigration to Italy

==External links==
* 

 
 
 
 
 
 


 
 