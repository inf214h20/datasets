Falling in Love Again (1980 film)
Falling in Love Again is a 1980 American romantic comedy film directed by Steven Paul and starring Elliott Gould, Susannah York.  Its plot concerns a man who reminisces about a relationship he had with a girl in his youth. The film also introduces Michelle Pfeiffer as the girl of the story in her early years.

==Cast==
* Kaye Ballard - Mrs. Lewis
* Twink Caplan -Melinda John Diehl - Pompadours friend
* James Dunaway -Man on the street
* Elliott Gould  - Harry Lewis
* Robert Hackman - Mr. Lewis
* Todd Hepler - Alan Childs
* Iren Koster - Piano player
* Marian McCargo - Mrs. Wellington
* Tony ODell - Bobby Lewis
* Bonnie Paul - Hilary Lewis
* Steven Paul - Stan the Con
* Stuart Paul - Pompadour (Young Harry)
* Michelle Pfeiffer - Sue Wellington
* Herbert Rudley - Mr. Wellington
* Alan Solomon - Max the Brain
* Cathy Tolbert - Cheryl Herman
* Susannah York - Sue Lewis
* Terrence Evans - Beaver

==References==
 

==External links==
* 

 
 
 
 
 
 


 