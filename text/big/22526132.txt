T.R. Baskin
{{Infobox film
| name           = T.R. Baskin
| image          = BaskinPoster.JPG
| image_size     =
| caption        = Original poster
| director       = Herbert Ross
| producer       = Peter Hyams
| writer         = Peter Hyams
| narrator       = James Caan Jack Elliott
| cinematography = Gerald Hirschfeld
| editing        = Maury Winetrobe
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 1971 Cinema American drama film directed by Herbert Ross. It stars Candice Bergen, Peter Boyle, Marcia Rodd and James Caan.

The screenplay by Peter Hyams focuses on a naïve young woman who moves to Chicago with the hope of finding romance and a fulfilling career.

The film was released as A Date with a Lonely Girl in the United Kingdom.

==Plot==
When Jack Mitchell (Peter Boyle), a married middle-aged salesman with children from Utica, New York, meets his old friend Larry Moore while on business in Chicago, he asks him if he knows any prostitutes Jack can get with while away on business. Larry gives Jack T.R. Baskins phone number, and Jack invites T.R to visit him at his hotel. T.R. arrives at the hotel and is relieved when Jack is impotent and cannot have sex, and she begins to tell Jack about her time so far in Chicago, a story that unfolds via Flashback (narrative)|flashback.
 bigot and Misogyny|misogynist. T.R. realizes shed rather be alone than spend time with such a callous individual. T.R meets other individuals at her job and becomes more affiliated with the city but seems uninterested in her surroundings. 

One night, after leaving a noisy bar, T.R. sees a man reading a book at the window in a café. She joins him at his table and learns his name is Larry and he edits and publishes books. The two hit it off and go back to his apartment and discuss their lives. Larry is divorced and misses spending time with his children, while T.R. confesses she always has felt like an outsider. The two make love, and the following morning T.R. feels she finally has taken the first step towards an intimate relationship, only to discover Larry has put a $20 bill in her coat pocket and mistaken her as a prostitute. Feeling betrayed and humiliated, she rushes out and walks the streets of Chicago contemplating what just happened while a voiceover of her saying that something "clicked" in her mind about what she wants to do with her life. Once she arrives at home, T.R. calls her parents to apologize for leaving home without telling them and has a breakdown. 

Back in the hotel, T.R and Jack discuss their current situations; why Jack is cheating on his wife with a prostitute, and why T.R is one. T.R tells Jack that she was tired of working as a typist and needed more excitement in her life and the two agree they are glad they met. The film ends with T.R leaving the hotel debating about whether she should continue being a prostitute or not and what she should do with her life.

==Production== Rush Street.  

==Cast==
*Candice Bergen ..... T.R. Baskin 
*Peter Boyle ..... Jack Mitchell  James Caan ..... Larry Moore 
*Marcia Rodd ..... Dayle Wigoda 
*Howard Platt ..... Arthur

==Critical response==
Vincent Canby of the New York Times noted the title character "is never at a loss for words, most of which sound as if they had come straight from the notebook of a writer who spent most of time jotting down funny lines without ever worrying much about character. It thus falls to Candice Bergen, a beautiful actress who projects intelligence, humor, vulnerability and self-reliance — all more or less simultaneously — to make something credible of the mouthpiece character written for her by Peter Hyams. . . . Somewhere deep inside T.R. Baskin, there is, I suspect, a real, touching film crying to get out with something more than a wise-crack, but neither Hyams, nor Herbert Ross, the director, have been able to find it."  

Roger Ebert of the Chicago Sun-Times said the film "gets in trouble right off the bat with a flashback style that neatly drains away all of our interest in half of the story" and added, "The problem is that everyone in the movie acts so stupidly. Real people of average intelligence would have cut through this plot in about three minutes, and the movie would have been over. It lasts two hours only because people are at such pains not to catch on."   

Time (magazine)|Time stated, "Peter Boyle . . . and James Caan . . . do the best they can, which is extremely well indeed, but the movies clumsy feints at sophistication and its grotesque sentimentality prevail."  

Variety (magazine)|Variety said the film "makes a few good comedy-comments on modern urban existence, but these are bits of rare jewelry lost on a vast beach of strung-out, erratic storytelling . . . Peter Hyams debut production is handsomely mounted, but his screenplay is sterile, superficial and inconsistent . . . Bergens screen presence is too sophisticated for the role, and both her acting, direction and dialog result in confusion."  

TV Guide rated the film 1½ stars and commented, "Although she raises some interesting questions, Bergens character evokes neither the sympathy nor the interest intended."  

==References==
 

==External links==
 

 

 
 
 
 
  
 
 