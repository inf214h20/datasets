Tulsi (film)
{{Infobox Film
| name           = Tulsi: Mathrudevobhava
| image          = Tulsi-2008-218x300.jpg
| caption        = Poster Ajay Kumar
| producer       = Padmavati C. H. Ajay Kumar
| narrator       = 
| starring       = Manisha Koirala Irrfan Khan
| music          = Nikhil-Vinay
| cinematography = Ajay Vincent
| editing        = 
| distributor    = 
| released       = 18 January 2008 (India)
| runtime        = 
| country        = India
| language       = Hindi
}} 2008 Bollywood Ajay Kumar Telugu language film Mathru Devo Bhava which, again has been remade from Malayalam language film Akashadoothu (1993) directed by Sibi Malayil. 

==Plot== Yashpal Sharma) tries to molest Tulsi. When Suraj gets to know of it, he beats Yashpal black and blue. A furious Yashpal swears revenge. In the meanwhile, Tulsi is diagnosed with blood cancer. Yashpal attacks Suraj and murders him. A distraught Tulsi now decides to get her kids adopted by different families before shes gone.

==Cast==
* Manisha Koirala .... Tulsi
* Irrfan Khan .... Suraj
* Sadashiv Amrapurkar .... Annoying Foster Father
* Tinu Anand .... Foster Father with Crooked Teeth
* Kulbhushan Kharbanda .... The Guru Yashpal Sharma
* Anjana Mumtaz .... Sexy Mom
* Sahila Chadda
* Arzoo Govitrikar
* Vikram

==Reception==
The film fared badly at the boxoffice. Media described the low publicity and zero pre-release hype as shocking as it was actress Manisha Koiralas comeback as the lead since Mumbai Express (2005). 

==References==
 

== External links ==
*  
*  

 
 
 