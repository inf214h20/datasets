L'argent des autres
{{Infobox film
| name           = LArgent des autres
| image          = L-argent des autres.jpg
| caption        = Film poster
| director       = Christian de Chalonge
| producer       = Michelle de Broca   Henri Lassa  Adolphe Viezzi 
| screenplay     = Pierre Dumayet Christian de Chalonge 
| based on       =  
| starring       = Jean-Louis Trintignant Catherine Deneuve Claude Brasseur Michel Serrault
| music          = Guy Boulanger Patrice Mestral 
| cinematography = Jean-Louis Picavet 		 
| editing        = Jean Ravel 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 Best Director, and was nominated for Best Supporting Actor, Best Writing and Best Editing. 

== Plot ==
Henri Rainier is an employee at Mirement bank. He makes a loan to adventurous client the Chevalier dAven, thinking he has the backing of his superiors. However, the venture proves disastrous, and the bank is faced with having to cover up its deficit. The banks managers want to disassociate themselves from the scandal. As it is Rainiers client who is accused of fraud, the managers believe Rainier is responsible for it and fire him from his bank job. However, Rainier refuses to take it lying down, and knows that it was the banks director who approved the loan. Rainiers wife Cécile and union representative Arlette suggest he sue his former company. Anxious to avoid being framed, Rainier sets out to prove it his former superiors responsibility for this and other shady transactions.

== Cast ==
* Jean-Louis Trintignant as Henri Rainier
* Catherine Deneuve as Cécile Rainier
* Claude Brasseur as Claude Chevalier dAven
* Michel Serrault as Miremant
* Gérard Séty as De Nully
* Jean Leuvrais as Heldorff
* François Perrot as Vincent
* Umberto Orsini as Blue
* Michel Berto as Duval
* Francis Lemaire as Torrent
* Juliet Berto as Arlette Rivière
* Raymond Bussières as Claude Chevalier dAvens father

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 