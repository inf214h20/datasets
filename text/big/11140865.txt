Apoorva Sagodharargal (1989 film)
 
 
 
{{Infobox film
| name = Apoorva Sahodharargal
| image = Apoorva Sagotharangal.jpg
| caption = Original poster
| director = Singeetam Srinivasa Rao
| producer = Kamal Hassan
| writer = Crazy Mohan (dialogues) Manohar Shyam Joshi (dialogues - Hindi version)
| screenplay = Kamal Haasan
| story  = Panchu Arunachalam
| narrator = Rupini Srividya Manorama
| music = Ilaiyaraaja
| cinematography = P. C. Sriram
| editing = B. Lenin V. T. Vijayan
| studio = Raaj Kamal Films International
| distributor = Raaj Kamal Films International
| released =   
| runtime = 150 minutes
| country = India
| language = Tamil  
| gross =
}} Tamil dramedy dwarf and a police officer, along with Gouthami, Srividya, Manorama (Tamil actress)|Manorama, Nagesh, Jaishankar, Nassar and Delhi Ganesh in main roles. 
 Telugu as Vichithra Sodarulu and into Hindi as  Appu Raja  in 1990. 

==Plot== dwarf , grows up in the circus with his mother.

Appu falls in love with the daughter (Rupini) of the circus owner mistakenly thinking that she was asking him to elope with her. But she actually had asked him to be witness to her marriage with her fiancé, which was not approved by her father. Heartbroken over this and insecurity over his height, he tries to commit suicide but is prevented by his mother, who then reveals the reason that his dwarfism might have been because of the poison force-fed to her when she was pregnant with the twins. This leads Appu to learn about his father’s murder and he decides to avenge by killing the four murderers.

Meanwhile, Raja falls in love with Janaki (Gouthami) who happens to be Satyamoorthys daughter. As Raja resembles Sethupathy, Sathyamoorthy and his 3 friends gain interest in Raja.
 Pomeranian puppy Rube Goldbergian contraption  to kill him and the dead body falls in a lorry covered with hay. Raja and Janaki have car trouble and hitchhike a ride in the same lorry but are oblivious to the dead body. The lorry driver (Kavithalaya Krishnan) discovers the body when he reaches his destination and calls the police. The inspector (Janakaraj) in charge of the case, suspects Raja by tracing the car number given by the lorry driver.

Appu kills Nallasivam in a golf course using a tiger from his circus but Nallasivams caddy sees Appu’s face and the tigers tail from afar. This leads the inspector to Raja again who, coincidentally is wearing a tiger costume while performing a song at a festival in his street. Janaki becomes enraged when she learns that Raja has killed his father’s friends and breaks up with him.

Raja is released from custody as the postmortem examination has revealed real tiger wounds that could not be inflicted by Raja’s costume. He goes over to Janaki’s house to smooth things over with the inspector following him covertly. When he is talking to her, Appu tricks Sathyamoorthy into killing himself with a circus hand gun that shoots backwards. Appu escapes but Raja and Janaki enter Sathyamoorthys room hearing the gunshot a few moments before the inspector arrives. Janaki faints when she sees her dad dead and the inspector now believes that Raja had shot Sathyamoorthy. Raja too escapes and is on the run while the police had released a sketch of his face to the public. An exhausted Raja is spotted by some people in a market and they try to catch him. Raja, in an attempt to evade capture threatens to kill the nearest woman he gets hold of. When the crowd back down, he releases the woman and escapes. The woman, who incidentally happens to be Srividya, realizes that Raja is the other twin and seeks out Muniyamma. They both realize that the murders are committed by Appu and Raja is mistaken as Appu, who overhears this. Dharmaraj believes that it is Raja avenging his father’s death and that he is the next target, and decides to seek out Raja’s mother and is shocked to see Srividhya, whom he believed to be dead, also present there. Nonetheless, he kidnaps both women and threatens to kill them unless Raja surrenders to him. Appu escapes and helps Raja evade the police and tells him everything. They both collaborate and go to the circus where their mothers are held captive. With the help of well trained circus animals, Appu and Raja overpower the goons. Ignoring his mothers call to stop, Appu shoots Dharmaraj, he falls down and eaten by circus lions. Appu surrenders to the police and Raja is set free and unites with Janaki.

==Cast==
*Kamal Hassan: Sethupathy (Raghupathy in Hindi version)/Appadurai aka Appu/Raja
*Srividya as Kaveri
*Gouthami as Janaki
*Janakaraj as The Inspector Rupini as Appus girlfriend Manorama as Muniyamma
*Nagesh as Dharmaraj
*Delhi Ganesh as Francis Anbarasu
*Nassar as Nallasivam
*Jaishankar as Satyamoorthy
*Kullamani
*T. S. B. K. Moulee as Rupinis father Anand as Rupinis fiance
*Kavithalaya Krishnan as Truckdriver
*R. S. Shivaji as Constable Sambandham
*Singeetham Srinivasa Rao as special appearance as Marriage registrar

==Production==
Singeetham said that in the initial version of the story, there was only one Kamal Haasan playing the dwarf character. After getting rejected by a couple of girls, he would seen walking into desert with his circus troupe in the climax. Singeetham later dropped it as this idea was not feasible then he created another character for Kamal Haasan to create entertainment and drama and added revenge angle to make it commercial.  Kamal revealed that he rewrote the script all over again after listening to an alternate take on the film.  The film was the second script after the first one was discarded by Kamal and this itself was majorly toned down for violence because Ilayaraaja and Panchu Arunachalam did not approve of the amount of violence there initially was.  One of the writers Dinesh Shailendra was discussing the concept of the film, Kamal initially suggested Man Behind the Iron Mask, Dinesh said that it would be nice if one the characters is a dwarf. A Scientist from Bangalore was invited and he came up with a whole lot of sketches he had worked out on how the dwarf would be created. 

Kamal Haasan was featured in three distinct characters; a police character, a dwarf and a mechanic.  Actress Gandhimadhi was originally cast in Manorama (Tamil actress)|Manoramas role. Due to changes in the script, she was replaced.  Singeetham said that the dwarf look was done in different angles and innovative ideas had been adopted for each angle. A pair of special shoes was prepared to be attached to the folded knees of the actor for the straight angle shots. The legs had been taken care of and the mannerism adopted by Mr. Kamal Hassan by holding his arms in a particular way jelled with his “dwarf” legs. For the side angle shots, a trench was dug up just to cover up the actor’s legs from the feet to knees, with special shoes attached at the knee level.   Kamal approached Crazy Mohan to write dialogues for the film.  Nagesh was casted in a negative role alongside Delhi Ganesh, Jaishankar and Nassar.  Ravikanth who debuted in Manathil Uruthi Vendum (1987) did a small role as Kamals friend but his portion was deleted from the final product.  S. T. Venky made his debut as visual effects designer for this film and also became the first person to use digital technology in visual effects.   Stage actor Suppuni was offered a role in the film but he declined as he felt uncomfortable. 

Animals used in the films were trained by Ravindra Sherpad Deval.  The circus portions in the film were filmed at Gemini Circus.  The first song "amma aaatha kaalai thottu kumbidanum" recorded for the film was dumped as the script was doctored by Kamal Hassan based on Ashokamitrans novel. After 50 days of successful run, the song was included as an added attraction. 

==Themes and influences== same name which featured actors MK Radha and Bhanumathi in titular roles and was produced in Tamil, Telugu and Hindi simultaneously. Directed by Acharya and written by Kothamangalam Subbu, the old film was an adaptation of the novella “The Corsican Brothers” by Alexander Dumas. The idea of brothers coming together to avenge the death of their father is the common thread running between both the old and the new film. 

==Soundtrack==
The music was composed by Ilayaraja. The songs remain popular even today. The sad theme music in the movie was highly acclaimed and remains popular to this day. Regarding the song "Pudhu Mappillaiku", Kamal said that he wanted a tune similar to the way which is sang in Oscar Awards function whereas Ilayaraja said that Kamal wanted a tune similar to the song "Naan Paarthathile" from Anbe Vaa (1966).   The song "Unna Nenachen" is one of the classic songs from the film. The song was rewritten by Vaali for over 5 times and Kamal Haasan was still unhappy with it only at his sixth attempt, he became successful in delivering what Kamal wanted.   Elements of "Annatha Aadurar" has been used in the song "Saroja Saman Nikalo" from Chennai 600028 (2007). 
 Vaali while Prem Dhawan and Rajashri wrote the lyrics for Hindi and Telugu versions.

{{Infobox album  
| Name        =  Apoorva Sahodharargal /Appu Raja
| Type        = Album
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 4 May 1989 (Tamil) 5 June 1990 (Hindi)
| Recorded    = Raajkamal Film International Feature film soundtrack
| Length      =
| Label       = Raajkamal Film International
| Producer    =
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}

{{Track listing
| headline = Tracklist - Tamil 
| extra_column = Singer(s)
| title1 = Raja Kaiya Vachchaa
| extra1 = Kamal Hassan
| length1 = 04:55
| title2 = Puthu Maappillaikku
| extra2 = S. P. Balasubrahmanyam, S. P. Sailaja
| length2 = 04:34
| title3 = Unna Nenachen Paattu
| extra3 = S. P. Balasubrahmanyam
| length3 = 04:38
| title4 = Vaalibaththin Kaathalukku Jey
| extra4 = S. P. Balasubrahmanyam, S. Janaki
| length4 = 04:40
| title5 = Annaaththe Aaduraar
| extra5 = S. P. Balasubrahmanyam
| length5 = 04:39
}}

{{Track listing
| headline = Tracklist - Hindi 
| extra_column = Singer(s)
| title1 = Raja Naam Mera
| extra1 = Kamal Hassan
| title2 = Woh To Bana Apna
| extra2 = S. P. Balasubrahmanyam, Asha Bhosle
| title3 = Tune Saathi Paya Apna Jag Mein
| extra3 = S. P. Balasubrahmanyam
| title4 = Matwale Yaar Teri Jai
| extra4 = S. P. Balasubrahmanyam, Asha Bhosle
| title5 = Aaya Hai Raja
| extra5 = S. P. Balasubrahmanyam
}}

{{Track listing
| headline = Tracklist - Telugu 
| extra_column = Singer(s)
| title1 = Raja Cheyyaveste
| extra1 = S. P. Balasubrahmanyam
| title2 = Bujji Pelli Kodukki
| extra2 = S. P. Balasubrahmanyam, S. P. Sailaja
| title3 = Ninnu Talichi
| extra3 = S. P. Balasubrahmanyam
| title4 = Vedi Vedi
| extra4 = S. P. Balasubrahmanyam, K. S. Chitra
| title5 = Aadedi Nenura
| extra5 = S. P. Balasubrahmanyam
}}

==Critical reception==
Behindwoods praised Appus character as "one of Kamals landmark makeovers in cinema" and also said that it "as a tragic character that had to wear a clown’s mask to show a cheerful face to the world though he himself is angst ridden and weeping inside more often than not".   Times of India praised Kamal that he "created history by playing a dwarf who was almost half his original height".  Rediff wrote: "Under Singeethams very able direction, the movie blended mainstream cinema and emotion very well,   and marked the beginning of what was to be a long career, for Kamal Haasan, in getting more into the skin of his character, and setting higher standards for himself with the aid of superior make-up and body language". 

==Legacy==
The Hindu included "Unna Nenachen" among lyricist Vaalis best songs in their collection, "Best of Vaali: From 1964 - 2013".  Indiaglitz included Apoorva Sagotharargal alongside another Kamal starrer Michael Madhana Kamarajan (1990) in their list "Top Twin roles of Kollywood".  The film was also included by Rediff in their list titled "10 Best films of Kamal Haasan".  Behindwoods included "Raja Kaiya Vacha" in their list "Unforgettable songs of Kamal".  Malathi Rangarajan of Hindu included Apoorva Sagotharargal in her list "Two to Tango".  Singer Mahathi listed "Pudhu Maappillaiku" as her all time favourite song.  Bharadwaj Rangan of The Hindu stated in his review of I (film)|I (2015) that the film reminds of "Apoorva Sagotharargal".  On September 2007, Sakthi Vasu expressed interest in remaking the film.  On Kamal Haasans 60th birthday, an agency named Minimal Kollywood Posters designed posters of Kamal Haasans films, One of the posters depicted the two minions with one of them being a dwarf reminiscent of the characters from the film. 

==In popular culture== Thillu Mullu Periya Marudhu (1995), Goundamani imagines himself as dwarf similar to Kamals look and dances to the song "Pudhu Mappillaiku".  In a comedy scene from Velayudham (2011), Vaidehis father (M. S. Bhaskar) is seen watching the film on television.  Scenes from the film has been parodied in Thamizh Padam (2010), Delhi Ganesh reprises his character in the film. Shiva tries to use Rupe goldberg machine similar to the one Kamal used in the film but fails.  Dhanush sported a dwarf look in Padikkadavan (2009 film)|Padikkathavan (2009).  Director Vinayan said that idea of Malayalam film Albhuthadweep (2005) was inspired from Apoorva Sagotharargal. 

==Notes==
 

==See also==
*List of Dwarfism media depictions

==References==
 

==External links==
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 