Purple Heart (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name            = Purple Hearth
| director        = Bill Birrell
| producer        = Bill Birrell Russel Gannon Demetrius Navarro Josh Siegel Stefan Beese
| writer          = Bill Birrell Russel Gannon
| music           = Ralph Rieckermann
| cinematography  = Guy Livneh
| editor          = Edgar Burcksen William Sadler Dennis Hayden
| casting         = Ivy Isenberg
| studio          =
| distributor     =
| budget          =
| gross           =
| released        =  
| country         = United States
| language        = English
}} William Sadler, Mel Harris and Emilio Rivera. It is released on November 11, 2006.

== Plot ==
The film is the story of Sgt. Oscar Padilla, member of an elite military unit designed for covert operations. His first mission: assassinate Saddam Hussein prior to the beginning of the 2003 Iraq War. Unfortunately the mission is compromised; Padilla is captured and tortured by the Iraqis. Later, Padilla is rescued, but is severely damaged by his ordeal. Back in the United States, he escapes from the lock down ward of the military hospital where he is being treated. Given what he knows about the illegal attempt at political assassination, he is considered dangerous. As the leader of the unit that trained Padilla, Colonel Allen is sent with specific orders to go find him and "solve the problem" permanently. Allen intends to convince Padilla to come back. As it turns out, there was no real-life mission at all: it was the final, most severe training exercise that broke Padillas soul – and mind. The film raises questions about the moral obligations of the use of military power, and the methods that the U.S. uses to train and ultimately sacrifice its own soldiers.

== Cast == William Sadler as Colonel Allen
* Demetrius Navarro as Sgt. Oscar Padilla
* Mel Harris as Dr. Harrison
* Ed Lauter as Civilian
* Emilio Rivera as Deputy
* Russell Gannon as Al-Sadr
* Mary L. Carter as News Reporter
* Douglas Tait as Halliday
* Ric Smith as Hank
* Tulsi Ram as Toufig Tulsiram/Iraqi soldier
* H. Charles Parrish as Drill Instructor Dennis Hayden as Earl
* Charles Fathy as Iraqi officer
* Dave Erickson as News Anchor
* Icarus the Wonder Dog as Claudias Dog
* Joseph Aguilar as M. A. Guiterrez

== Production ==
Purple Heart was produced by the company of Claymore Inc..

== Home release ==
The DVD version of the film was distributed by Indican Pictures in 2008.

== Release ==
Purple Heart was released in the Egyptian Theatre in Los Angeles, CA for the Artivist Film Festival.

== External links ==
*  
*  

 