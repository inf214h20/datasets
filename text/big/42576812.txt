A Woman Between Two Worlds
{{Infobox film
| name = A Woman Between Two Worlds
| image =
| image_size =
| caption =
| director = Goffredo Alessandrini
| producer = 
| writer =  Corrado Alvaro    Georg C. Klaren
| narrator =
| starring = Isa Miranda   Assia Noris   Giulio Donadio   Mario Ferrari 
| music = Franz Grothe 
| cinematography = Ubaldo Arata
| editing = Fernando Tropea 
| studio =   ICAR
| distributor = Cinès-Pittaluga
| released = 1934
| runtime = 76 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
A Woman Between Two Worlds (Italian:Una donna tra due mondi) is a 1936 Italian drama film directed by Goffredo Alessandrini and starring Isa Miranda, Assia Noris and Giulio Donadio. The films sets were designed by art director Hans Ledersteger. It is the Italian version of the German film Die Liebe des Maharadscha. The film largely takes place in a grand hotel setting. 

==Cast==
*Isa Miranda as  Mina Salviati
*Assia Noris as Daisy Atkins
*Giulio Donadio as Suraj 
*Váša Příhoda as Saverio Lancia
*Mario Ferrari as dottor Lawburn
*Oreste Bilancia as Saverio Lancia
*Tatiana Pavoni as Mimi
*Ernesto Sabatini as Lord Winston
*Vinicio Sofia as lamministratore di Trenchman
*Renato Malavasi as il segretario di Trenchman
*Alfredo Martinelli as Il barbiere
*Gino Viotti as Il direttore dellhotel  
*Olinto Cristina
*Carlo Petrangeli

== References ==
 

== Bibliography ==
* Clarke, David B. & Doel, Marcus A. Moving Pictures/Stopping Places: Hotels and Motels on Film. Lexington Books, 2009.
 
== External links ==
*  

 

 
 
 
 
 
 
 
 


 