Natpukkaga
{{Infobox film
| name = Natpukaaga
| image =
| director = K. S. Ravikumar
| writer = K. S. Ravikumar Jyothi Krishna Vijayakumar Senthil Senthil Ranjith
| producer = A. M. Rathnam Deva
| cinematography = Ashok Rajan
| editing = K. Thanikachalam
| studio = Sri Surya Movies
| distributor = Sri Surya Movies
| released = June 25, 1998
| runtime =
| country = India
| language = Tamil
| budget =
}}

Natpukaaga (    film directed by K. S. Ravikumar. It starred Sarath Kumar and Simran Bagga in the lead roles. It became a success upon release.

It was remade into Telugu as Sneham Kosam which starred Chiranjeevi with K. S. Ravikumar directing the version.
Later remade in Kannada as Diggajaru (2000).

==Plot==

Chinnaiya (Sarath Kumar) is a faithful servant in the house of a rich man (Vijayakumar (actor)|Vijayakumar). There is no love lost between the rich man and his elder daughter (Sitara). His second daughter, Prabhavathy (Simran Bagga), who arrives from a foreign land, falls in love with Chinnaiya but after gaining his trust, accuses him of attempting to rape her. Turns out that she did this to try to unite her sisters family with her father and send Chinnaiya out since it was Chinnaiyas father who killed her mother. So Chinnaiya is kicked out of the house. Meanwhile Muthaiya (also Sarath Kumar), Chinnaiyas father, is released from jail.

==Cast==
* Sarath Kumar as Chinnaiyya and Muthaiya
* Simran Bagga as Prabhavathy Vijayakumar
* Manorama as Muthaiyas mother
* Senthil Sundarrajan
* Anumohan
* Sithara
* Sujatha
* Mansoor Ali Khan Ranjith
* Manobala
* K. S. Ravikumar as Nondi Samiyar

==Reception==

The film was a blockbuster at the box office.

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|- 1998
|Sneham Kosam Telugu
|Chiranjeevi, Meena
|K. S. Ravikumar
|- 2000
|Diggajaru Kannada
|Vishnu Vardhan, Ambareesh
|D. Rajendra Babu
|-
|}

==Soundtrack==
There are 6 songs composed by Deva (music director)|Deva. Lyrics by Kavignar Kaalidhasan  

*1. Chinna Chinna Mundhiriya - Mano (singer)|Mano, K. S. Chitra
*2. Adikkira Kai - Harini Deva
*4. Vijayakumar
*5. Meesakkaara Nanba - Krishnaraj Sujatha

==References==
 

 
 
 

 
 
 
 
 
 
 