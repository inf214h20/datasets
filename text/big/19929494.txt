Les Musiciens du ciel
{{Infobox film
| name           = Les Musiciens du ciel
| image          = 1940 Les Musiciens du ciel film poster.jpg
| image_size     = 
| caption        =  Georges Lacombe
| producer       = 
| writer         = René Lefèvre (actor)|René Lefèvre  Jean Ferry
| narrator       = 
| starring       = Michèle Morgan  Michel Simon René Lefèvre
| music          = Arthur Honegger Arthur Hoérée          
| cinematography = Henri Alekan  Paul Portier  Eugen Schüfftan  
| editing        = Louise Mazier
| studio         = Regina Films
| distributor    = Filmsonor English Films (USA) 
| released       = 10 April 1940 (Paris)   28 February 1945 (New York City) 
| runtime        = 98 min 
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Georges Lacombe, based on novel "Musiciens Du Ceil" by René Lefèvre (actor)|René Lefèvre who co-wrote screenplay with Jean Ferry. The music score is by Arthur Honegger and Arthur Hoérée. The film stars Michèle Morgan, Michel Simon and René Lefèvre.

The principal actors Michèle Morgan and Michel Simon, had earlier appeared together in Port of Shadows (1938), but then they had not been comrades.

==Primary cast==
*Michèle Morgan as  Le lieutenant Saulnier 
*Michel Simon as  Le capitaine Simon 
*René Lefèvre (actor)|René Lefèvre as  Victor 
*René Alexandre as  Louis 
*Auguste Bovério as  Le commissaire 
*Sylvette Saugé as  La Louise 
*Alexandre Rignault as  Le grand Georges

==External links==
*  at DvdToile
* 
* 

 
 
 
 

 