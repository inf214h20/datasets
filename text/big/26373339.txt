Panchagni
 
{{Infobox film
| name = Panchagni
| image = Panjagni 300.jpg
| image_size =
| alt =
| caption = Hariharan
| producer = G. P. Vijayakumar M. G. Gopinath
| writer = M. T. Vasudevan Nair Geetha  Mohanlal  Nadia Moidu Thilakan
| music = Bombay Ravi (Songs) Pukazhenthi (Background score) O N V Kurup (Lyrics)
| cinematography = Shaji
| editing = M. S. Money
| released =  
| runtime = 140 minutes
| country = India
| language = Malayalam
}}
Panchagni ( ) (  film scripted by M.T. Vasudevan Nair and directed by Hariharan (director)|Hariharan, music by Bombay Ravi and lyrics by O N V Kurup. It stars Geetha (actress)|Geetha, Mohanlal,  Nadia Moidu and Thilakan.

A remarkable tale of a remarkable woman, who puts her commitment to social activism above everything else, including her own love for an adoring man. Under the direction of Hariharan, Geetha made a sensational debut in Malayalam in one of the most powerful female characters in Indian cinema ever.  It is one of the classics in Malayalam cinema.  

==Plot==
The movie revolves round the incidents in a two-week period, when Indira (Geetha (actress)|Geetha), a Naxal activist is out in parole. She is serving life sentence in the central jail, Kannore after being charged for the murder of Avarachan, a landlord, who she had witnessed kill a young tribal woman after she was raped and impregnated (by him).

Indiras mother, a past freedom fighter who is on her deathbed, is relieved to see her, and is under the impression that she is free now. Her younger sister Savithri (Nadiya Moidu), her husband Prabhakaran (Devan (actor)|Devan) and her nephew are happy to have her back home. But her younger brother, Ravi (Meghanathan), an unemployed guy, addicted to drugs is angered by her mere presence, blaming her for his inability to secure a good job. Indiras older brother who is home from Delhi to perform the death rites of her mother refuses to even talk to her, and leaves after a big brouhaha, leaving his nephew to do the rites. Most of her acquaintances are intimidated by her, except her old classmate Sharadha (Chithra (actress)|Chitra). Sharadha had married her college sweetheart, Rajan (Murali (Malayalam actor)|Murali) and lives close to Indiras home.

Rasheed (Mohanlal), a freelance journalist, tries to get an interview with Indira, she declines initially and is annoyed by his persistence.

As the days pass on, Indira feels unwanted, and ends up having no place to live. Savithri suspects an affair between her husband and Indira, making it hard for Indira to stay with them. Sharadhas husband had changed a lot in years, had degraded into a womanizer, and Indira cant stay with them either. Ultimately Indira, asks Rasheed for help her out and ends up staying at his place.
 

With time, Indira and Rasheed get closer, and a lovely relationship blossoms between the two. As Indira is nearing the completion of her parole, Rasheed, with great difficulty, succeeds in getting the government remission order in time, so that Indira no longer has to go back to jail. By this time Savithri and Ravi reconcile with Indira, and are overjoyed to hear about her release. Indira rushes to Sharadhas place to share the good news, but there she is shocked to see Sharadhas servant being gang-raped by her husband Rajan and friends. True to her righteous self, Indira ends up shooting Rajan with his hunting rifle and ultimately surrenders herself at the police station.

==Cast==
* Mohanlal as Rashid Geetha as Indira
* Nadia Moidu as Savithri
* Thilakan as Raman Devan as Prabhakaran Nair
* Nedumudi Venu as Shekharan Murali as Rajan Chithra as Sarada
* Prathapachandran as Avarachan Soman as Mohandas
* Lalitha Sree as Convict at jail
* Babu Antony as a Naxal activist
*Kunjandi
* Subair as another Naxal activist

==Casting==
Hariharan about Mohanlals casting, "Panchagni was mainly Geethas film, but when we asked Mohanlal whether he would do the film, he said he would do any role. The first thing I wanted was to give him a different appearance. Till then, he hadnt appeared without a moustache. So I asked him to shave it off. He immediately agreed. Panchagni went on to become a turning point in his career.". 

== Soundtrack ==
{{Track listing
| headline        = Song
| extra_column    = Singer
| all_lyrics      = O N V Kurup
| all_music       = Bombay Ravi
| title1          = Aa Rathri..
| extra1          = K S Chithra
| length1         = 4:25
| title2          = Saagarangale..
| extra2          = K J Yesudas
| length2         = 4:18
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 