Colossus and the Headhunters
{{Infobox film
| name = Colossus and the Headhunters
| image = Colossus and the Headhunters.jpg
| caption = Theatrical release poster.
| director = Guido Malatesta
| writer = Guido Malatesta Laura Brown Alfredo Zammi
| producer = Giorgio Marzelli
| distributor = American International Pictures
| released = 1960
| runtime = USA:79 min
| language = Italian
| budget = 
}}
Colossus and the Headhunters, also known in its original Italian title as Maciste contro i cacciatori di teste ("Maciste against the head-hunters"), is a sword and sandal film created in Italy and released in 1960. It is notable for having no monsters, which was a hallmark of the sword and sandal genre, instead featuring the title Headhunter tribe as antagonists.

In the movies plot, Maciste arrives on an island only to find it rocked by a catastrophic volcanic eruption... and sinking. The survivors board Macistes raft, and he pilots it toward a nearby yet unknown island. The group gets captured by a tribe led by Queen Amoa. Her people are under continuous attack by another tribe on the island—a tribe of savage headhunters. Maciste and his survivors decide to help her, so he and some of his friends set out to find Amoas father. They travel to a ruined castle occupied by the headhunters—which used to be a great city of gold—and find Amoas father, now blind, imprisoned alone in one of the dungeon rooms by the headhunters. Meanwhile, the leader of the headhunters captures Amoa and decides to marry her by compelling her father to give his blessing. A large battle erupts at Queen Amoas village when the headhunters attack. During the fight, the headhunter leader abducts Queen Amoa. Maciste pursues them, saves Amoa, and fights and defeats the wily leader of the headhunters. Having saved the day, Maciste rides away on his raft with Amoa in his arms.

==Popular Culture==
The film was featured in a season six episode of the movie-mocking show Mystery Science Theater 3000.

==External links==
*  
*  

 
 
 
 
 
 


 