Fun in the Streets
{{Infobox film
| name           = Fun in the Streets
| image          = 
| caption        = 
| director       = Carl Ottosen
| producer       = Palle Schnedler-Sørensen
| writer         = Carl Ottosen
| starring       = Dirch Passer
| music          = Sven Gyldmark
| cinematography = Claus Loof
| editing        = Knud Hauge
| distributor    = 
| released       = 1 August 1969
| runtime        = 88 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Fun in the Streets ( ) is a 1969 Danish comedy film directed by Carl Ottosen and starring Dirch Passer.

==Cast==
* Dirch Passer - Peter Jensen
* Winnie Mortensen - Winnie
* Ove Sprogøe - Fløjten
* Willy Rathnov - Rasmussen
* Karl Stegger - Bagermester Jacobsen
* Carl Ottosen - Chefen
* Lotte Horne - Elisabeth
* Birgit Sadolin - Faster Anna
* Arne Møller - Carlo Kurt Andersen - Orla Ernst Meyer - Gossain
* Poul Bundgaard - Fordrukken mand i opgang
* Bodil Udsen - Hans sure kone
* Inger Gleerup - Ekspeditrice i stormagasin
* Poul Glargaard - Kriminalassistent
* Marianne Tholsted - Ekspeditrice i legetøjsforretning
* Claus Ryskjær - Cykelbud
* Palle Justesen - Hotelportier

==External links==
* 

 
 
 
 
 
 


 
 