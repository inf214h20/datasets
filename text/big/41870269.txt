Alan Poza
{{Infobox film
| name           = Alan Poza
| image size     = 
| image	         = Alan_Poza_Poster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Charles Novia
| producer       = Charles Novia
| screenplay     = Charles Novia
| story          = 
| narrator       = 
| starring       =  
| music          = 
| cinematography = Uzodinma Okpechi
| editing        = Tokunboh Akinsowo   Michael Asesse
| studio         = November Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = Nigeria
| language       = English
| budget         =
}}

Alan Poza is a 2013 Nigerian romantic comedy drama film starring OC Ukeje as Alan Poza. It was written, produced and directed by Charles Novia. It received 2 nominations at the 9th Africa Movie Academy Awards. OC Ukeje also won a Best Of Nollywood Awards for his leading role as Alan Poza.   


==Cast==
*OC Ukeje as Alan Poza
*Beverly Naya as Pride
*Okey Uzoeshi as Kokori
*Belinda Effah as Bunmi
*Kemi Lala Akindoju as Ina
*Sylvia Oluchi as Senami
*Norbert Young as M.D
*Charles Novia as Pastor
*

==Reception==
The film received mostly negative reviews from critics, especially as regards the script and storytelling. Nollywood Reinvented gave the film 24% rating and wrote "For a comedy, Alan Poza is very unfunny. As a romance flick, it is very confusing. And for a drama, it is very chaotic. In the end O.C still didn’t come off to me as the player type. I was not certain how the heck anything happened."  Sodas and Popcorn gave it a 3 out of 5 rating and wrote "The acting was my favorite bit of the whole movie, It pretty much beat the script. All the acts most notably OC Ukeje did justice to their roles. Together as an entire team, the chemistry wasn’t perfect but it was enjoyable and their characters were hilarious." 

==Accolades== BON award. 

== References ==
 


 
 
 
 
 
 
 
 
 
 
 
 

 