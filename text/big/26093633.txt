Suspended Animation (film)
{{Infobox Film
| name           = Suspended Animation 
| image          = 
| image_size     = 
| caption        = 
| director       = John D. Hancock
| writer         = 
| narrator       = 
| starring       = Alex McArthur
| music          = Angelo Badalamenti
| cinematography = 
| editing        = 
| distributor    =  
| released       = December 25, 2001
| runtime        = 114 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Suspended Animation is a 2001 film directed by John D. Hancock.

==Cast==
*Alex McArthur	... 	Tom Kempton
*Laura Esterman	... 	Vanessa Boulette
*Sage Allen	... 	Ann Boulette
*Rebecca Harrell	... 	Hilary Kempton
*Fred Meyers	... 	Sandor Hansen
*Maria Cina	... 	Clara Hansen
*Jeff Puckett	... 	Cliff Modjeska
*Daniel Riordan	... 	Jack Starr
*J.E. Freeman	... 	Philip Boulette
*Sean Patrick Murphy	... 	Fred Phelps
*Daniel Mooney	... 	Arnold Mann
*Gary J. Mion	... 	Sheriff Montaigne
*Joe Forbrich	... 	Production Manager
*Robert Breuler	... 	Dr. Leo Sagan
*Denise Bohn	... 	Correspondence 1

==External links==
* 

 

 
 
 
 


 