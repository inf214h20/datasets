The Best Things in Life Are Free (film)
{{Infobox film
| name           = The Best Things in Life Are Free
| image          = The Best Things in Life Are Free film.jpg
| image_size     =
| caption        = film poster
| director       = Michael Curtiz
| producer       = Henry Ephron
| writer         =  
| narrator       =
| starring       = Gordon MacRae Dan Dailey Ernest Borgnine Sheree North 
| music          = Lionel Newman
| cinematography = Leon Shamroy
| editing        = Dorothy Spencer
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         =$1.16 million  
| gross          = $2.7 million 
| website        =
}}
The Best Things in Life Are Free  is a 1956 American musical film directed by Michael Curtiz. The film stars Gordon MacRae, Dan Dailey and Ernest Borgnine as the real-life songwriting team of Buddy DeSylva, Lew Brown and Ray Henderson of the late 1920s and early 1930s; and Sheree North as Kitty Kane, a singer (possibly based on Helen Kane).
 Oscar nomination Best Music, Scoring of a Musical Picture.

==Cast==
*Gordon MacRae as Buddy DeSylva
*Dan Dailey as Ray Henderson
*Ernest Borgnine as Lew Brown
*Sheree North as Kitty Kane
*Tommy Noonan as Carl Frisbee
*Murvyn Vye as Manny Costain
*Phyllis Avery as Maggie Henderson
*Larry Keating as Winfield Sheehan
*Tony Galento as Fingers Norman Brooks as Al Jolson Jacques dAmboise as Specialty dancer
*Roxanne Arlen as Perky Nichols
*Byron Palmer as Hollywood star

==Reception==

===Critical response===
Premiering in September-1956, The Best Things in Life Are Free was greeted with mixed to positive critical acclaim. Reviews said "the biggest new musical this year" and others said "a musical-comedy that couldve been produced on a higher budget with bigger and better production numbers".

===Box office performance===
Because it was a Technicolor musical, the film was more expensive to produce than other films of the era. The film ended with a budget of $2.86 million and made just over $4 million at the box office, earning $2,250,000 in North American rentals in 1956. 

==Songs==
*"Lucky Day"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Dan Dailey
*"If I Had a Talking Picture of You"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Byron Palmer
*"Here Am I, Broken Hearted"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Gordon MacRae Button up Your Overcoat"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Dan Dailey and Gordon MacRae
*"Good News"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Gordon MacRae
*"Youre the Cream in My Coffee"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Dan Dailey The Best Things in Life Are Free"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Sheree North (dubbed by Eileen Wilson)
*"Lucky in Love"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Gordon MacRae
*"Black Bottom"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Danced by Sheree North and Jacques dAmboise
*"Birth of the Blues"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Danced by Sheree North and Jacques dAmboise
*"Sonny Boy"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
:Sung by Norman Brooks
*"Follow Thru"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
*"One More Time"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
*"Thank Your Father"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
*"This Is the Missus"
:Music by Ray Henderson
:Lyrics by Lew Brown
*"Together"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
*It All Depends on You"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
*"You Try Somebody Else (Well Be Back Together Again)"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva
*Without Love"
:Music by Ray Henderson
:Lyrics by Lew Brown and Buddy G. DeSylva

==References==
 

==External links==
* 
* 
*  (in German)
* 
* 

 

 
 
 
 