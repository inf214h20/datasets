For Them That Trespass
{{Infobox film
| name           = For Them That Trespass
| image_size     =
| image	         = "For_Them_That_Trespass"_(1949).jpg	
| caption        = UK theatrical poster
| director       = Alberto Cavalcanti 
| producer       = Victor Skutezky
| writer         = J. Lee Thompson  novel by Ernest Raymond
| narrator       = Stephen Murray Patricia Plunkett  and introducing  Richard Todd Philip Green
| cinematography = Derick Williams
| editing        = Margery Saunders
| studio         = Associated British Picture Corporation
| distributor    = Associated British-Pathé  (UK)
| released       = 21 April 1949 (London UK)
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =£124,978 (UK) 
| preceded_by    =
| followed_by    =
}} Stephen Murray.  It is an adaptation of the 1944 novel of the same name by Ernest Raymond  

==Plot==
Promising writer Christopher Drew conceals his relationship with a murdered woman in order to protect his career, even though this results in an innocent man going to prison for the killing.  The upper-class Drew decides he needs some first-hand experience to invigorate his work, so he explores the seedier areas of town in search of inspiration. Much to his dismay, he witnesses a murder but refuses to help an innocent man, Herbert Logan, arrested for the crime, because his presence in such a neighbourhood would cause a scandal. Logan is freed after serving 15 years in jail and he hears his "crime" detailed on a radio drama written by Drew, which enables him to gather enough evidence to finally clear his name. 

==Cast==
* Richard Todd - Herbert Edward Logan
* Patricia Plunkett - Rosie Stephen Murray - Christopher Drew
* Michael Laurence - Jim Heal
* Vida Hope - Olive Mockson
* Rosalyn Boulter - Frankie Ketchen James Hayter - John Cragie Glenn
* Harry Fowler - Dave
* George Hayes - Artist Michael Brennan - Inspector Benstead
* Joan Dowling - Gracie
* Michael Medwin - Len
* Mary Merrall - Mrs. Drew
* Irene Handl - Inn owner
* John Salew - Ainsley, Prosecutor
* Robert Harris - Sir Huntley, Defence counsel

==Critical reception==
*The New York Times called it "a drab and dreary little film." 
*Sky Movies called it a "gripping movie drama which has a lot of high feeling and style...Still impressive, though more than 40 years after." 

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 