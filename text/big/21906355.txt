My Love, My Love (film)
 
{{Infobox film
| name           = My Love, My Love
| image          = 
| caption        = 
| director       = Nadine Trintignant
| producer       = André Génovès
| writer         = Nadine Trintignant
| starring       = Jean-Louis Trintignant
| music          = 
| cinematography = Willy Kurant
| editing        = Nicole Lubtchansky
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = France
| language       = French
| budget         = 
}}

My Love, My Love ( ) is a 1967 French drama film directed by Nadine Trintignant. It was entered into the 1967 Cannes Film Festival.   

==Cast==
* Jean-Louis Trintignant - Vincent Falaise
* Valérie Lagrange - Agathe
* Annie Fargue - Jeanne
* Michel Piccoli - Marrades
* Katarina Larsson - Marilou
* Bernard Fresson - Serge
* Marie Trintignant
* Marie-José Nat - Cameo appearance (uncredited)

==References==
 

==External links==
* 
 
 
 
 
 
 
 