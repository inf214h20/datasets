The Refrigerator (film)
{{Infobox film
| name           = The Refrigerator
| image          = TheRefrigerator1991Film.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = VHS cover.
| director       = Nicholas Jacobs
| producer       = Christopher Oldcorn
| writer         = Nicholas Jacobs
| screenplay     = 
| story          = Christopher Oldcorn Nicholas Jacobs Philip Dolin
| based on       = 
| narrator       = 
| starring       = Julia McNeal Dave Simonds Angel Caban
| music          = Adam Roth Don Peterkofsky Christopher Burke
| cinematography = Paul Gibson
| editing        = P.J. Pesce Nicholas Jacobs Suzanne Pillsbury Christopher Oldcorn
| studio         = Avenue D Films Connexion Film Productions
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        =  
| language       = English
| budget         = $500,000 (estimated)
| gross          = 
}}

The Refrigerator is a 1991 horror film starring Julia McNeal, Dave Simonds and Angel Caban. It was written and directed by Nicholas Jacobs. The film tells the story about a couple that move into an apartment in New York with a refrigerator that contains the portal to hell.

==Background==
The first draft of the script was written in 1987 and the film took four years altogether to make.  The Refrigerator was filmed within New York in America, by production companies Connexion Film Productions and Avenue D Films. http://www.imdb.com/title/tt0102767/companycredits?ref_=tt_dt_co  As an independent production with a limited budget, the filmmakers decided not to get the film rated by the MPAA and as a result it was released straight to video and promptly disappeared.   

It was first seen in Germany at the June 1991 Munich Film Festival. On 25 September 1992, it premiered in New York and during 1993 it had a theatrical release across Germany via Impuls-Film Hans-Joachim Flebbe Co.  The film was released on VHS in Germany via Warner Home Video on 25 February 1993.  Later in the UK, on 10 July 1995, it had a limited release on VHS via the studio Mia (Missing in Action).  In America, the film had a very limited video release in 1993 via Monarch Home Video.  It has never seen an official DVD release.

The film is similar to the Filipino horror film  , which was also remade in 2012, also known as Fridge. 

==Plot==
The film opens with a couple drunk driving through the streets of New York. Upon arriving at their apartment, they make love in front of the kitchen. Afterwards, the wife walks into the kitchen whereupon the refrigerator opens up and sucks her in.

Steve & Eileen Bateman move to New York, renting the apartment with the killer refrigerator. Steve takes a new job, while Eileen dreams of becoming a performer. She pretends to receive an award then walks all over Broadway (Manhattan)|Broadway.

Steve and Eileen begin to have nightmares about the refrigerator: Steve sees mini people inside the refrigerator (supposed victims) while Eileen sees unborn babies.

Steve is slowly driven to insanity by the refrigerators influence, leaving Eileen to cope with her acting career on her own.

Juan, a plumber, comes to the apartment one night, warning Eileen that her refrigerator is from hell and the devil controls it.

Steve and Eileen throw a party at their apartment one night. The refrigerator brings the other kitchen appliances to life and begins slaughtering the guests. Juan ties the refrigerator shut, and the surviving guests flee. Later, another couple is shown interested in buying the apartment.

==Cast==
* Julia McNeal - Eileen Bateman
* Dave Simonds - Steve Bateman
* Phyllis Sanz - Tanya
* Angel Caban - Juan
* Nena Segal - Eileens Mother
* Jaime Rojo - Paolo
* Alex Trisano - Hector
* Peter Justinus - Mr. Walters
* Karen Wexler - Nikki
* Michael Beltran - Ramon
* Jack Mason - Chipper
* Larry Tate - Mr. Jarvis
* Phil Butard - Bob
* Michelle DeCosta - Young Eileen Bateman
* Anthony McGowen - Pete Jackson

==Critical reception==
The Fright Site writer Adam Groves wrote "This is, quite simply, the Citizen Kane of killer fridge movies. No, this cheapie will never be mistaken for Halloween or The Exorcist, but it is a fun, wildly imaginative treat for open-minded horror fans with a sense of humor. For those in search of primo evil fridge thrills, look no further than this 1991 comedic horror fest, which manages to wring nearly every variation you can think of from its admittedly ludicrous concept. The opening scenes, with their dumb-assed slapstick and dull attempts at "character development," are pretty sorry, but the film gets better as it goes along. Writer-director Nicholas Jacobs is adept at mixing comedy and horror in the manner of Peter Jacksons early films (such as Bad Taste and Braindead). The Refrigerator works best, surprisingly, as straight-up horror mixed with comedy, and not the other way around. Much of it is played for laughs, certainly, but the refrigerator is an authentically menacing, even eerie presence throughout, and the special effects are quite impressive considering the low budget." 

Maynard Morrisseys Horror Movie Diary gave the film five out of ten and wrote "Undoubtedly one of the stupidest movies in horror history. I mean... a killer refrigerator? Come on. Whoever came up with this idea was probably drunk or high on drugs. Still its a pretty enjoyable little indie-flick that gladly takes itself not too serious. The acting is crap, the script is badly written and its often way too slow - but the fridge scenes and the kills are all pretty fun, it has some wonderfully atmospheric shots and the dialogue is often super-hilarious ("I am the waffle maker!")." 

Horror-Movies.ca gave the film six out of ten and stated "The Refrigerator is hands down one of the most bizarre and strange horror films Ive seen in a long time. Before watching this film Id thought I had downloaded another low budget cheesy 80s flick were the filmmaker had one day simply looked around his house and tried to think of some household abject that could kill someone. In a way thats exactly what the film is. Its about a killer refrigerator in a New York apartment. I got the feeling that in the last moments of the film the crew decided to just go all out because for a film that was so strange and interesting it was pretty dull for the most part but when the garbage can, fans and just about everything else starts to get possessed the blood really flies. Overall if your into strange grade-b horror fun then this is a must-see but that may be a little harder to do then with most films. From what I can tell this film has never been released on DVD but you can pick up used VHS copies on amazon. Like Ive said on other reviews films like these are available off of private tracker sites though and if youd like to get a copy just PM and I can give you the names of sites. Once again though, a very strange but entertaining piece of low budget filmmaking thats well worth the effort in seeking out." 

TV Guide gave the film two out of four stars, and stated "The Refrigerator is billed as a supernatural horror comedy which ultimately is its downfall - too many chickens in the pot. First-time director Nicholas Jacobss juxtaposition of genres is compelling in thought, but unfortunately on film the results just dont measure up. While the basic premise of The Refrigerator - take a couple of dream yuppies and plop them into a world of madness and horror - is satirical and wry, the final results arent very satisfying. The main failure of the film is that none of the separate entities work on their own - the funny scenes generally arent funny and the scary parts, while original in idea, arent scary, only overly gory. On top of this, Jacobs throws in a couple of ultraserious segments involving the mother-daughter relationship that are completely out of place. Mueller and Simonds as the jinxed couple offer up the right amount of exuberance and angst. Caban, though, as the plumber who looks more like an urban commando, best fits his role. Jacobss eclectic use of music, from campy to melodramatic, is one of the highlights of the film. If he had put a little more punch in his screenplay, been more daring with his satire and put some real scares in the film, the end result would have made a lot more sense. Maybe his intentions werent to make us laugh or get chills down our spine - its a parody, after all. Perhaps he just wanted to make a statement about the inherent evil of domesticity. If so, it could have been done many other ways." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 