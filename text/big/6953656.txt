Unhook the Stars
{{Infobox film
| name           = Unhook the Stars
| image          = Unhook the stars.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Nick Cassavetes
| producer       = René Cleitman,Panos Nicolaou co-producer
| writer         = Helen Caldwell Nick Cassavetes 
| narrator       =
| starring       = Gena Rowlands Marisa Tomei Gérard Depardieu
| music          = Steven Hufsteter	 
| cinematography = Phedon Papamichael Jr.	
| editing        = Petra von Oelffen
| distributor    = Miramax
| released       =   
| runtime        = 103 min.
| country        = France United States English
| budget         =
}}
 1996 drama SAG Award nominations for their performances.  Filmed in the Sugarhouse neighborhood of Salt Lake City, Utah.
 twentysomething daughter, Annie (Moira Kelly), has just left home. Shortly after Annie leaves, Mildred befriends Monica (Marisa Tomei|Tomei), a single mother from across the street, and Mildred eventually finds herself babysitter of Monicas young son, J.J. (Jake Lloyd|Lloyd). Throughout the film, Monica and J.J. inadvertently teach Mildred valuable life lessons about herself and her relationships with others.

The films title refers to a song of the same name by Cyndi Lauper, which can be heard over the closing credits.

==Cast==
* Gena Rowlands as Mildred
* Marisa Tomei as Monica
* Jake Lloyd as J.J.
* Gérard Depardieu as Big Tommy
* Moira Kelly as Annie
* David Rowlands as George
*Karon Cook as an Upscale Airport Passenger

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 