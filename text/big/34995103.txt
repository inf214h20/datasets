A ponte: uma história do ferryboat Bagamoyo
{{multiple issues|
 
 
}}

{{Infobox film
| name           = A ponte: uma história do ferryboat Bagamoyo
| image          = 
| alt            =  
| caption        = 
| director       = Diana Manhiça
| producer       = Promarte Zoom
| screenplay     = Diana Manhiça
| starring       = 
| music          = Azagaia Nhatchandje
| cinematography = Chico Carneiro
| editing        = Diana Manhiça
| studio         = 
| distributor    = 
| released       =   
| runtime        = 39 minutes
| country        = Mozambique
| language       = 
| budget         = 
| gross          = 
}}
 Mozambican 2010 documentary film.

== Synopsis ==
Bagamoyo is a ferryboat that has provided a daily connection for the last 37 years between the city of Maputo and Catembe – a largely rural district of the Mozambican capital– carrying several hundred passengers and vehicles each day, as well as goods, from five in the morning until half past eleven at night, seven days a week. The history of the ferry is told through the journeys and the stories of people who know the Bagamoyo and depend on it for their day-to-day lives. To this day, there is no bridge.

== External links ==
 

 
 
 
 
 
 
 


 
 