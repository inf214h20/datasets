The Moderns
{{Infobox film
| name           = The Moderns
| image          = Modernsposter.jpg
| caption        = Theatrical release poster
| writer         = Jon Bradshaw Alan Rudolph Kevin J. OConnor
| director       = Alan Rudolph
| producer       = David Blocker Shep Gordon Carolyn Pfeiffer
| music          = Mark Isham CharlElie Couture
| cinematography = Toyomichi Kurita
| studio         = Nelson Entertainment
| distributor    = Alive Films (theatrical) MGM Home Entertainment (DVD)
| released       =  
| runtime        = 126 min.
| country        = United States English
| budget         = $5 million
| gross          = $2,011,497
}}

The Moderns is a 1988 film by Alan Rudolph, which takes place in 1926 Paris during the period of the Lost Generation and at the height of modernist literature. The film stars Keith Carradine, Linda Fiorentino and John Lone among others.

American film critic, Roger Ebert, in his review stated that The Moderns is:
:"sort of a source study for the Paris of Ernest Hemingway in the 1920s; its a movie about the raw material he shaped into The Sun Also Rises and A Moveable Feast, and it also includes raw material for books by Gertrude Stein, Malcolm Cowley and Clifford Irving." 

==Plot summary== Kevin J. OConnor),  Gertrude Stein (Elsa Raven), and Alice B. Toklas (Ali Giron).

Nick is torn between his wife Rachel (Linda Fiorentino) and Nathalie de Ville (Geraldine Chaplin) who hires him to forge her paintings. He must also contend with Rachels current husband, Bertram Stone (John Lone), who does not know that his wife is still married to another man.

==Cast==
*Keith Carradine as Nick Hart
*Linda Fiorentino as Rachel Stone
*John Lone as Bertram Stone
*Wallace Shawn as Oiseau
*Geneviève Bujold as Libby Valentin
*Geraldine Chaplin as Nathalie de Ville Kevin J. OConnor as Ernest Hemingway

==Production==
Meg Tilly was set to play the part of Rachel Stone, but withdrew due to scheduling conflicts and Linda Fiorentino eventually signed on to replace her. Mick Jagger and Sam Shepard were considered to play Bertram Stone, before John Lone was immediately cast. Isabella Rossellini screen-tested for the role of Nathalie de Ville, but lost to Geraldine Chaplin.

==Reception==
The film received fairly positive reviews from critics, as it currently holds a 75% rating on Rotten Tomatoes. It was nominated for three Independent Spirit Awards including Best Supporting Male for John Lone, Best Screenplay and Best Cinematography.

==Notes==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 