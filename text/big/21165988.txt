Bongolo (film)
 
{{Infobox film
| name           = Bongolo
| image          =
| caption        =
| director       = André Cauvin
| producer       = André Cauvin Fernand Rigot  (uncredited) 
| writer         = André Cauvin
| narrator       =
| starring       =
| music          =
| cinematography = Roger Fellous
| editing        = Max Brenner
| distributor    =
| released       = 1952
| runtime        = 85 minutes
| country        = Belgium
| language       = French
| budget         =
}}

Bongolo (also known as Bongolo and the Negro Princess) is a 1952 Belgian film directed by André Cauvin. It was entered into the 1953 Cannes Film Festival.   

==Plot==
A young Congolese man works as a nurse at a health center lost in the jungle. He falls in love with the daughter of the local king and convinces her to forget her prejudices and ancestral rites. The elders, who oppose the wedding, burn down the health center.

==Cast==
* Petronelle Abapataki as Doka
* Peter Baylis as Comments
* Yves Furet as (voice)
* Joseph Lifela as Bongolo
* Sonar Senghor as (voice)

==References==
 

==External links==
* 
* 

 
 
 
 
 