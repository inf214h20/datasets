Shinjuku Boys
{{Infobox Film
| name           = Shinjuku Boys
| image          = Shinjuku_Boys_1995.jpg
| image_size     = 
| caption        = The Shinjuku Boys: Kazuki, Tatsu, Gaish
| director       = Jano Williams, Kim Longinotto
| producer       = Kim Longinotto
| writer         = 
| narrator       = 
| starring       = Gaish   Kazuki   Tatsu
| music          = Nigel Hawks
| cinematography = Kim Longinotto
| editing        = John Mister
| distributor    = Second Run DVD
| released       = 1995
| runtime        = 53 minutes
| country        = United Kingdom
| language       = English, Japanese, subtitled
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 transgender men who work at the New Marilyn Club in Tokyo, Japan.

==Reception== Houston Film Festival.  The film received positive reviews following its 2010 release by Second Run DVD. In a review at DVDTalk, Chris Neilson praised the films directors, commenting that "Through low-key cinéma vérité filmmaking, Longinotto and Williams provide insight into the professional and personal lives of the trio of onnabe  ".  Sarah Cronin of Electric Sheep Magazine also notes that "Despite the fact that its a cruder, more dated film, its the strength of the interviews in Shinjuku Boys that makes it an even more arresting documentary." 

==See also==
*Shinjuku Ni-chōme, an LGBT bar district in Tokyo

==References==
 

== External links ==
*  
*   at Women Make Movies

 
 
 
 
 
 
 
 
 