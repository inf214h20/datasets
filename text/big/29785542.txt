Super Inday and the Golden Bibe
{{multiple issues|
 
 
}}

{{Infobox film
| name = Super Inday and the Golden Bibe
| image = SuperIndayAndTheGoldenBibe2010Poster.jpg
| caption = Theatrical movie poster
| director = Mike Tuviera
| starring = Marian Rivera John Lapus
| producer =  
| distributor = Regal Entertainment Regal Multimedia
| released =   
| runtime = 124 minutes
| country = Philippines
| language =  
| budget =
| gross = Php 18,000,000
}}
Super Inday and the Golden Bibe is a remake of the original 1988 movie starring Maricel Soriano and a fantasy-adventure flick official entry to 36th Metro Manila Film Festival–Philippines 2010 of Regal Entertainment co-produced with Regal Multimedia, Inc. on December 25, 2010. This stars Marian Rivera as Super Inday and John Lapus as the Golden Bibe.

==Critical reaction==
Philippine Entertainment Portal complained that while Super Inday was still the same as in the original film, the character of the golden bibe had changed considerably. While calling the film "respectable" and "a genuine superhero movie" that doesnt try to be something else, they found it overlong and not saying anything of importance.  In contrast Click The City called the movie "awful" claiming it unsuccessfully tried to appeal to everyone by cramming in far too much, and scoring it 1.5/5. 

==Main cast==
*Marian Rivera as Inday "Super Inday"
*John Lapus as the Golden Bibe
*Jake Cuenca as Jeffrey "Amazing Jay"
*Pokwang as Kokay "Copycat"

==Supporting cast==
*Cherry Pie Picache as Monina ( mother of Inday )
*Jestoni Alarcon as Danillo "Danny" Sta. Cruz ( father of Inday )
*Mylene Dizon as Ingrid (present wife of Danny)
*Sheena Halili as Gina (Moninas anak-anakan )
*Buboy Villar as Etnok
*Sabrina Man as Jinky Sta. Cruz
*Jairus Aquino as Francis Sta. Cruz
*Irma Adlawan as Lucita
* Elijah Alejo as Angelica

==Awards==
===Metro Manila Film Festival Awards===
Super Inday and the Golden Bibe received one award and two nominations at Gabi ng Parangal last December 26, 2010. 

==Accolades==
===Awards and nominations===
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2010
| Best Actress
| align="center"| Marian Rivera
|  
|- Best Make-up Artist
| align="center"| Nestor Dayao, et al.
|  
|- Best Sound Engineering  (tied with RPG Metanoia) 
| align="center"| Ditoy Aguila
|  
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 