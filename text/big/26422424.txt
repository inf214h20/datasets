Are You Scared 2
{{Infobox Film |
name = Are You Scared 2|
image = |
caption = |
writer = Russell Appling, John Lands|
starring = Adrienne Hays Adam Busch Tristan Wright Chad Guerrero Kathy Gardiner Andrea Monier Hannah Guarisco Tony Todd Katherine Rose Mark Lowry Dallas Montgomery Robin Zamora Laura Buckles |
director = John Lands Russell Appling (co-director) |
producer = Russell Appling Laurie Herbert John Lands Jordan Kessler (executive producer) Nick Thurlow (executive producer) Oak Porcelli (associate producer) Andrea Warner (associate producer) Todd Williams (co-executive producer) |
distributor = Louisiana Media Services |
released = February 10, 2009 |
runtime = 93 minutes |
country = United States |
language = English |
budget =  $750,000 (estimated) }}
Are You Scared 2 is a 2009 action film directed by John Lands, and released by Louisiana Media Services. It stars Adrienne Hays, Adam Busch, Tristan Wright, Chad Guerrero, Kathy Gardiner, Andrea Monier, Hannah Guarisco, Tony Todd, Katherine Rose, Mark Lowry, Dallas Montgomery, Robin Zamora, and Laura Buckles. It is an unrelated sequel to Are You Scared?,  that released in 2006.

==Plot==
Dallas, Andrew, his girlfriend Taryn and Reese compose of the DNA Team in an Internet game where the objective is to find treasures in hidden caches. While searching the last treasure to beat the games record score, they are chased by two psychopaths controlled by the sick owner of an underground site that is filming their movements and they have to fight to survive

==Cast==
* Adrienne Hays - Victim #1
* Adam Busch - Steven
* Tristan Wright - Dallas
* Chad Guerrero - Andrew
* Kathy Gardiner - Reese
* Andrea Monier - Taryn
* Hannah Guarisco - Victim #2
* Tony Todd - Controller
* Katherine Rose - Victim #3
* Mark Lowry - Hulking Brute
* Dallas Montgomery - Hulking Brute
* Robin Zamora - Hunter (as Rodrigo Robles)
* Laura Buckles - Talia (scenes deleted)

==Reception==

 

== References ==
 

==External links==
*  
*  

 
 


 