Proxies (film)
{{infobox film
| name           = Proxies
| image          = Proxies (1921) - 1.jpg
| image_size     =
| caption        = Newspaper ad
| director       = George D. Baker
| producer       = Cosmopolitan Productions
| writer         = Frank R. Adams (story) George D. Baker
| starring       = Norman Kerry
| music          =
| cinematography = Harold Wenstrom
| editing        = Charles J. Hunt
| distributor    = Paramount Pictures
| released       = May 1, 1921
| runtime        = 7 reels
| country        = United States Silent (English intertitles)
}}
Proxies is a 1921 American silent drama film feature produced by Cosmopolitan Productions and distributed by Paramount Pictures. It was directed by George D. Baker and starred Norman Kerry. A copy is preserved at the Library of Congress. 

==Plot==
As described in a film publication summary,    Carlotta Darley (Dean) is engaged to Homer Carleton (Crosby), but regrets that Homer is not as tall and handsome as the butler Peter (Kerry). Her father Christopher Darley (Tooker) was aware that Peter was a former crook but believes that he has reformed. Clare Conway (Keefe), the household maid, is in love with Peter and jealous of Carlottas admiration of him. A reception is in progress when John Stover (Everton) arrives with a paper that will bring about the Darleys financial ruin. Peter arranges to have John brought into the reception room, then holds up the guests to secure the paper and save his employer. John and Clare then escape together.

==Cast==
*Norman Kerry - Peter Mendoza
*Zeena Keefe - Clare Conway
*Raye Dean - Carlotta Darley
*Jack Crosby - Homer Carleton
*Paul Everton - John Stover
*William H. Tooker - Christopher Darley
*Mrs. Schaffer - Mrs. Darley
*Robert Broderick - Detective Linton

==References==
 

==External links==
* 
* 
*  at silenthollywood.com

 
 
 
 
 
 
 
 


 
 