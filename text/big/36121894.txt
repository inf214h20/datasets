The Spark Divine
{{infobox_film
| name           = The Spark Divine
| image          = Alice Joyce 1919.jpg
| imagesize      =
| caption        = Period lobby advertisement
| director       = Tom Terriss
| producer       = Vitagraph Company of America
| writer         = Alicia Ramsey (story) George DuBois Proctor
| starring       = Alice Joyce
| music          =
| cinematography = Joseph Shelderfer
| editing        =
| distributor    = Vitagraph Company of America
| released       = June 30, 1919
| runtime        = 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} 1919 American silent drama film, starring Alice Joyce, that was directed by Tom Terriss and produced and distributed by Vitagraph Company of America. This is now considered to be a lost film.  

==Plot==
As described in a film magazine,  the Van Arsdales come into sudden wealth and raise their naturally sweet and sentimental child Marcia (Joyce) into a beautiful and cultured woman who is cold and calculating and with a heart of ice. She becomes known in society as the girl who challenges any man to arouse sentiment in her. When bankruptcy threatens the family, they believe their only salvation is their daughters marriage to a millionaire. Robert Jardine (Carleton) is the man they select. Robert believes he sees the woman beneath the shell of Marcia, and when she coldly tells him that she will marry him only for his wealth, he agrees on the condition that she "will a wife - and a mother." Marcia keeps her part of the marriage agreement, but apparently cares nothing for her own child. Gradually the spark of mother-love brightens, but she is ashamed of her show of sentiment. Then the child is kidnapped, and Marcia is fraught with the fears that only a mother can know, showing the real woman in her. Then her child is brought to her, and it turns out that the kidnapping being a "frame" on the part of her husband.

==Cast==
*Alice Joyce - Marcia Van Arsdale William Carleton, Jr. - Robert Jardine
*Eulalie Jensen - Mrs. Van Arsdale
*Frank Norcross - Mr. Van Arsdale
*Mary Carr - Mrs. Jardine

==References==
 

==External links==
 
* 
* 

 
 
 
 
 