The Break
{{Infobox film
| name           = The Break
| image_size     = 
| image	         = The Break FilmPoster.jpeg
| caption        = DVD cover
| director       = Lee H. Katzin
| writer         = 
| narrator       = 
| starring       = Vincent Van Patten
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = September 1, 1995
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Break is a 1995 film directed by Lee H. Katzin. It stars Vincent Van Patten as Nick, a former tennis player turned coach to a new up and coming player, and Rae Dawn Chong as a former girlfriend. The film appeared during the 1990s, mostly on cable TV. 

==Cast==
*Vincent Van Patten as Nick Irons
*Rae Dawn Chong as Jennifer Hudson
*Martin Sheen as Gil Robbins
*Valerie Perrine as Delores Smith
*Betsy Russell as Candy

==References==
 

==External links==
* 

 

 

 
 