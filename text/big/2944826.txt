Dragon Ball: Sleeping Princess in Devil's Castle
 
{{Infobox film name           = Dragon Ball:  Sleeping Princess in Devils Castle image          = sleepi.jpg image_size     = caption        = Australian DVD Release director       = Daisuke Nishio producer       = Keizō Shichijō screenplay  = Kenji Terui based on     =   starring       = See   music          = Shunsuke Kikuchi cinematography = Motoaki Ikegami editing        = Shinichi Fukumitsu studio         = Toei Animation distributor    = Toei Company released       = July 18, 1987 runtime        = 45 minutes budget         = country       = Japan gross          =¥850 million
|}}

  is the second   movie and the film versions of Hikari Sentai Maskman and Choujinki Metalder. An English dub by Funimation was released on home video in 1997, though an alternative English dub was also released in Europe from AB Groupe sometime in the early 2000s.

==Plot summary== Goku seeks out Master Roshi to ask to be accepted as a student, while the little monk, Krillin, arrives for the same reason (and uses a porno magazine as a bribe). Roshi sends them on a quest far to the west, where "five mountains stand, called the Devils Hand." Inside a castle, there lies the legendary, and beautiful, "Sleeping Princess." Whoever brings back the Sleeping Princess will become Master Roshis student. The two boys set out, and Krillin uses all the standard tricks to trip Goku up.
 Puar —who Launch (in her evil, blonde state) arrives to steal the Sleeping Princess—which is really a giant jewel. Everyone is captured and encased in a wall of rock.

It is the night of a full moon, and the moonlight is used to power the jewel. The moonlight also turns Goku into a Giant Ape, and everyone escapes (with enough time to cut off Gokus tail, reversing the transformation). They fight Lucifer, and Goku uses the Kamehameha blast to destroy Lucifers Princess-powered laser cannon (the full-moonlight is to be used to destroy the sun, and begin the Reign of Darkness), and kills Lucifer. The heroes escape, and Krillin acts contrite, but doesnt actually apologize to Goku for his behavior. The two boys return to Kame House with Launch, and Roshi takes them both on as students.

==New characters==
; 
:A vampire who lives in Devils Castle.
; 
:A giant red monster who dwells within Devils Castle.

==Cast==
{| class="wikitable"
|-
! Character name
! Voice Actor (Japanese)
! Voice Actor (English)
|- Goku || Masako Nozawa || Ceyli Delgadillo
|-
| Bulma || Hiromi Tsuru || Leslie Alexander
|- Krillin || Mayumi Tanaka || Laurie Steele
|-
| Yamcha || Tōru Furuya || Christopher R. Sabat
|- Launch || rowspan="2"| Mami Koyama || Christine Marten (Bad)
|-
| Monika Antonelli (Good)
|- Master Roshi || Kōhei Miyauchi || Mike McFarland
|- Oolong || Naoki Tatsuta || Bradford Jackson
|- Puar || Naoko Watanabe || Monika Antonelli
|-
| Butler || Shōzō Iizuka || Christopher R. Sabat
|-
| Lucifer || Nachi Nozawa || Mike McFarland
|-
| Ghastel || Daisuke Gōri || Mike McFarland
|-
| Turtle || Daisuke Gōri || Christopher R. Sabat
|-
| Narrator || Jōji Yanami || Christopher R. Sabat
|-
| Goblin || Kazuo Oka ||
|-
| Goblin || Ikuya Sawaki ||
|- Masato Hirano ||
|-
| Goblin || Kazumi Tanaka ||
|}

== Music ==
* Opening Theme
*#  
*#* Lyrics: Yuriko Mori, Music: Takeshi Ike, Arrangement: Kōhei Tanaka, Performed by: Hiroki Takahashi

* Ending Theme
*#  
*#*Lyrics: Takemi Yoshida, Music: Takeshi Ike, Arrangement: Kōhei Tanaka, Performed by: Ushio Hashimoto

==English Release==
The English rights to Sleeping Princess in Devils Castle were granted to Harmony Gold USA originally in conjunction with their rights to the TV series in 1989. They edited the other two movies into a special, but never actually did anything with this film other than use bits of footage in their opening theme. FUNimation acquired the film in 1995, along with the Dragon Ball series, and the rest of the movies. 
The English dub of Sleeping Princess in Devils Castle produced by   and  . This set was re-released in a thinpack on February 12, 2008.  The film was distributed to VHS and Region 4 DVD in Australia by Madman Entertainment. The film was re-released to Region 1 DVD again in a remastered 4-disc Movie pack release with the other Dragon Ball films on February 8, 2011  containing Funimations existing English dub,  but restored the opening/ending.

An alternative English dub was produced in France by AB Groupe and released in English speaking markets in Europe. It featured an uncredited voice cast.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 