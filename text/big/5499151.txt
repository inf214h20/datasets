Crazy in Alabama
{{Infobox Film |
  name = Crazy in Alabama |
  image = Crazy in alabama poster.jpg|
  caption = |
  director = Antonio Banderas |
  writer = Mark Childress | David Morse John Beasley |
  producer = Debra Hill |
  music = Mark Snow |
  editing = Robert C. Jones |
  distributor = Columbia Pictures |
  released = October 22, 1999 |
  runtime = 111 minutes |
  country = United States |
  language = English |
  budget = $15 million |
  gross = $2,005,840 |
}}
Crazy in Alabama is a 1999 comedy-drama film directed by Antonio Banderas, written by Mark Childress (based on his own 1993 novel of the same name), and starring Melanie Griffith as an abused wife who heads to California to become a movie star while her nephew back in Alabama has to deal with a racially motivated murder involving a corrupt sheriff. The movie was filmed in Houma, Louisiana.

==Plot summary== Civil Rights Movement. He becomes involved with a group of black students protesting the towns racially segregated municipal swimming pool, leading to a protest that explodes into deadly violence. A young black boy, Taylor Jackson, is killed by the town sheriff. Peejoe, the only witness, is pressured by the sheriff to keep it quiet. However, Peejoe has learned from the example of his free-spirited Aunt Lucille Vinson, who has killed her abusive husband and is headed for Hollywood, where she is convinced that television stardom awaits her. 

Lucille takes her husbands head everywhere she goes in a black hat box  and looks forward to Hollywood promises. When the head is discovered by the hostess of a party, Lucille tries to get rid of the head by throwing it off the Golden Gate Bridge. Two policemen, thinking she is about to jump over herself, open the hat box and discover the head inside. She is arrested and escorted back to Alabama for her trial, where she is given a warm welcome by her town. 

After being convicted of first-degree murder, Lucille is sentenced to twenty years in prison. However, the sentence is suspended, and she is put on a five-year probation with the condition that she seek psychiatric help. Lucille, her children, and all her friends joyfully exit the courtroom while the sheriff (through Peejoes testimony) is put under arrest for Taylors murder.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Melanie Griffith || Lucille Vinson
|- David Morse || Dove Bullis
|-
| Lucas Black || Peter Joseph Bullis
|-
| David Speck || Wiley Bullis
|-
| Cathy Moriarty || Earlene Bullis
|-
| Meat Loaf || Sheriff John Doggett
|-
| Rod Steiger || Judge Louis Mead
|-
| Richard Schiff || Norman
|- John Beasley || Nehemiah Jackson
|-
| Robert Wagner || Harry Hall
|-
| Noah Emmerich || Sheriff Raymond
|-
| Sandra Seacat || Meemaw
|-
| Paul Ben-Victor || D.A. Mackie
|-
| Brad Beyer || Jack
|-
| Fannie Flagg || Sally
|-
| Elizabeth Perkins || Joan Blake
|-
| Linda Hart || Madelyn
|-
| Paul Mazursky || Walter Schwegmann
|-
| Holmes Osborne || Attorney Larry Russell
|-
| Tony Amendola || Casino Boss
|-
| Randal Kleiser || Bob
|}

==Reception== ALMA award European Film Award for Outstanding European Achievement in World Cinema, and was nominated for a Golden Lion.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 