The Winning Team
{{Infobox film
| name           = The Winning Team Theatrical poster
| caption        = Theaterical poster
| director       = Lewis Seiler
| producer       = Bryan Foy
| screenplay     = 
| starring       = Doris Day Ronald Reagan Frank Lovejoy Rogers Hornsby.
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 mins
| country        = USA
| language       = English
| budget         = 
| gross          = $1.7 million
}}
The Winning Team is a 1952 biographical film directed by Lewis Seiler.  It is a fictionalized biography of the life of major league pitcher Grover Cleveland Alexander (1887–1950) starring Ronald Reagan as Alexander, Doris Day as his wife, Aimee and Frank Lovejoy as baseball star Rogers Hornsby.

The story covers Alexanders life as telephone company lineman and amateur ballplayer, pitcher for the Philadelphia Phillies and Chicago Cubs, his battles with alcoholism and epilepsy, and finally his comeback with the St. Louis Cardinals. It includes his heroic performance in three games in the 1926 World Series against the New York Yankees, where the 7th inning strikeout of Tony Lazzeri is used as the game-ending, Series-winning pitch.

The film earned an estimated $1.7 million at the North American box office in 1952. 

==Plot==
Poor health and alcoholism force Grover Cleveland Alexander out of baseball, but through his wifes faithful efforts, he gets a chance for a comeback and redemption.

==Cast==
* Doris Day as Aimee Alexander 
* Ronald Reagan as Grover Cleveland Alexander 
* Frank Lovejoy as Rogers Hornsby
* Eve Miller as Margaret Killefer 
* James Millican as Bill Killefer 
* Russ Tamblyn as Willie Alexander (as Rusty Tamblyn) Gordon Jones as George Glasheen 
* Hugh Sanders as Joe McCarthy 
* Frank Ferguson as Sam Arrants 
* Walter Baldwin as Pa Alexander 
* Dorothy Adams as Ma Alexander 
* Bob Lemon as Jesse Pop Haines 
* Jerry Priddy as Ballplayer 
* Peanuts Lowery as Ballplayer (as Peanuts Lowrey) 
* George Metkovich as Ballplayer

==See also==
* Ronald Reagan films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 