Krakatoa, East of Java
 
{{Infobox film
| name           = Krakatoa, East of Java
| image          = Krakatoa east of java.jpg
| caption        = Theatrical release poster
| director       = Bernard L. Kowalski
| producer       = William R. Forman Philip Yordan Bernard Gordon Clifford Newton Gould
| starring       = Maximilian Schell Diane Baker Brian Keith Sal Mineo Sumi Haru
| music          = Frank De Vol Mack David
| studio         = Security Pictures MGM (2005, DVD)
| released       =  
| runtime        = 131 min
| language       = English
| awards         =
| budget         = 
}}

Krakatoa, East of Java is a 1969 American disaster film starring Maximilian Schell and Brian Keith.  The film was nominated for the Academy Award for Best Visual Effects. During the 1970s, the film was re-released under the title Volcano.
 1883 eruption of the volcano on the island of Krakatoa. The characters are engaged in the recovery of a cargo of pearls from a shipwreck perilously close to the volcano.

==Cast==
* Maximilian Schell as Captain Chris Hanson
* Diane Baker as Laura Travis
* Brian Keith as Harry Connerly
* Sal Mineo as Leoncavallo Borghese
* Rossano Brazzi as Giovanni Borghese
* John Leyton as Douglas Rigby
* J.D. Cannon as Lester Danzig
* Barbara Werle as Charley Adams
* Jacqui Chan as Toshi
* Sumi Haru as Sumi

==Plot==
 steamer Batavia saloon hostess; pearl divers hold in appalling conditions. One of the prisoners, Lester Danzig (J. D. Cannon), is an acquaintance of Hanson s, and Hanson allows him to make the voyage on deck instead of in the hold.
 salvage the pearls, and determine Peter s fate &ndash; and to find Peter if he is still alive. Hanson plans to use a variety of techniques to search for the wreck and salvage the pearls, with the Borgheses  balloon conducting an aerial search of shallow waters around Krakatoa, the pearl divers providing a mobile underwater search-and-salvage capability in shallow waters, Rigby in his diving bell searching in deeper water, and Connerly responsible both for recovering the pearls if they are in waters too deep for the pearl divers and for assisting in the heavy work of bringing the Arianna s safe to the surface. Hanson plans to deliver the convicts to Madura after recovering the pearls off Krakatoa. Aware that Krakatoa has begun to erupt and warned by a colonial official that the island is a "raging volcano," he replies that the volcano had been quiet for the previous 200 years and posed no threat now.

During the Batavia Queen s voyage to Krakatoa, her crew and passengers observe strange phenomena: They see seabirds swarming in huge flocks by day, witness a series of fiery explosions erupting from the sea one evening, and hear a high-pitched, ear-splitting hissing and whistling sound like that of escaping steam on another night. During a conversation on deck one night, Danzig discovers that Connerly is suffering from a painful lung disease which he is keeping secret from Hanson because it might interfere with his diving abilities and is using laudanum to kill the pain. Danzig informs Connerly that Laura had spent a year in a mental institution, calling into question the veracity of her story about the pearls. The Borgheses, Connerly, Charley, Rigby, and Toshi confront Hanson about Lauras mental state, but Hanson assures them that Laura s story about the Arianna is true. Connerly takes so much laudanum that he hallucinates one night, attacks one of the pearl divers, and assaults several crewmen coming to her aid before they can subdue him. On Hansons orders, the Batavia Queen s crew suspends Connerly in a slatted box above the main deck so that he will pose no danger to others aboard the ship; Charley tearfully pleads with Hanson for Connerlys release, and Hanson relents and frees him. Meanwhile, Leoncavallo and Toshi take a romantic interest in one another.

The Batavia Queen arrives off Krakatoa to find the island shrouded in thick smoke. It clears when she anchors off the island, and the Borgheses ascend in their balloon while Rigby descends in his diving bell. The Borgheses quickly discover the wreck of the Arianna and guide the Batavia Queen and the submerged Rigby to it. Immediately afterwards, the motor driving the propeller that allows them to steer their balloon fails and they careen helplessly over Krakatoa and into its active Volcanic crater|crater. They jettison the useless engine and propeller into the crater s lava lake to reduce weight and finally are blown clear of the crater by a volcanic explosion which sets their balloon afire. They drift away from the island, leap into the sea, and are rescued, but the fire destroys the balloon.

Danzig tells Hanson of Connerly s lung problems, and Hanson decides that he will dive on the Arianna instead of Connerly. While Connerly and Hanson argue over this, Rigby s diving bell becomes snagged on coral. The pearl divers, Hanson, and Connerly all dive into the water to free Rigby, and while they and the Batavia Queen s other passengers and crew are thus occupied, Danzig steals a pistol he finds in the ship s chart room, knocks the jailer unconscious, and frees the prisoners. They take over the ship, throw the unconscious jailer overboard to drown, and imprison the passengers and crew in the hold, where they also place Rigby and the pearl divers when they return to the Batavia Queen. Before returning, and unaware of the turn of events aboard the Batavia Queen, Hanson and Connerly swim to the wreck of the Arianna, find the ship s safe, and attach a cable to it to have it hoisted aboard the Batavia Queen. Upon their return, Danzig has Connerly lowered into the hold but forces Hanson to look on at gunpoint as he opens the Arianna s safe on the Batavia Queen s deck. They find nothing in the safe but a cheap pocket watch. When an explosion on Krakatoa distracts Danzig, Hanson overpowers him, takes the pistol from him, pushes the heavy safe over onto one convict, shoots two others, and uses steam from a hose to force the rest of the prisoners to jump overboard. They swim to nearby Krakatoa.

After Hanson frees the passengers and crew from the hold, Rigby finds another compartment in the safe which contains the Arianna s logbook. Laura and Hanson examine the logbook for clues about Peter s fate. The logbook reveals that the Arianna made a last port call at Palembang before sinking, and a letter tucked into the logbook says that Peter disembarked there to attend the mission school. Hanson decides to steam to Palembang to find Peter. By now, Krakatoa is erupting continually, and the volcano s explosions begin to hurl lava bombs into the surrounding sea. A number of them strike the Batavia Queen as she gets underway for Palembang, starting fires which the crew puts out. As Toshi runs across the deck toward Leoncavallo, one of the lava bombs strikes and kills her.

The Batavia Queen arrives off Palembang to find the mission school heavily damaged, burning, and abandoned. Hanson hails a passing Junk (ship)|junk, and someone aboard the junk tells him that the staff and students of the school all are alive and had fled Palembang that morning aboard another boat to sail to Java. The Batavia Queen soon comes to the assistance of an overcrowded and sinking sampan, which proves to be the school s boat. The Batavia Queen s passengers and crew rescue everyone aboard the sampan, including Peter, who has a joyful reunion with Laura. A chest belonging to Peter comes aboard the Batavia Queen during the rescue; it contains the pearls, and Connerly, Rigby, the Borgheses, and the three surviving pearl divers receive their shares of the fortune.

Krakatoa s violent explosions become larger and continuous; Hanson assumes that they will generate a tsunami and begins to prepare the Batavia Queen to ride it out. Although Hanson assures him that a tsunami will destroy nearby Anjer and that he is safer at sea aboard the Batavia Queen if she can get to deep water in time, Connerly disputes the ship s ability to survive and demands that Hanson allow those who wish to go ashore to row to Anjer with him in one of the ship s Lifeboat (shipboard)|lifeboats. Giovanni Borghese, Charley, and the three surviving pearl divers all join Connerly in the lifeboat and row to Anjer. Krakatoa disintegrates in one final, cataclysmic explosion, which generates an enormous tsunami. It strikes Anjer shortly after the Batavia Queen s lifeboat arrives there; unable to outrun the wave, Connerly and Charley embrace for the last time before the wave engulfs and kills them. At sea, Hanson, Laura, Peter, Rigby, Leoncavallo Borghese, the refugees from the mission school, and the ship s crew ride out the tsunami successfully aboard the Batavia Queen.

==Production==

===Special effects===

In an unusual approach to making the film, the producers of Krakatoa, East of Java had the special effects scenes shot before the script had been completed. The script then was written so as to incorporate the special effects sequences.   
 funnel and masts and yards on figurehead on her Bow (ship)|bow. The steamer also was provided with functioning sails for her masts and yards. 

For special effects, Louriés team constructed two models of the modified steamer, an 18-foot (5.5-meter), one-to-ten-scale model and, for long shots of the Batavia Queen as she approaches Krakatoa, a one-to-twenty-scale model. The latter proved too small to provide realistic effects, so Lourié chose not to use it in the film, instead using only the larger model. 
 metric conversion United States imperial gallon, then the amount in liters is 11,820.  spraying water into the tank with powerful fire hoses, and employing a wind machine to disturb the waters surface. 

For sequences in which live actors are seen against a village in the background, the film employed traveling mattes in the foreground and miniatures in the background.   
 pyrotechnic sequences split screen, with real footage of the ocean in the lower part of the frame and a flopped volcano miniature reflection added above it in an optical printer.   

Although now quite dated in appearance, the film s special effects were considered impressive enough by 1969 standards for it to be nominated for the Academy Award for Best Visual Effects. It lost to Marooned (film)|Marooned. 

Lourié himself makes a non-speaking cameo appearance in Krakatoa, East of Java, portraying a lighthouse keeper on the coast of Java who observes Krakatoas final, cataclysmic explosion and enters the lighthouse to send news of it by telegraph.   

===Other production issues===

In addition to its challenging special effects, the makers of Krakatoa, East of Java encountered various difficulties during the films production. Producer   Charley performs for Connerly in their   but seems strangely out of place in an adventure or disaster film, and the vocal version of the movie s romantic theme song "East of Java" incongruously plays during scenes of filthy prisoners shuffling into the Batavia Queen s hold and sweating sailors performing the labor necessary to get the ship out to sea as she begins her voyage from Anjer. 
 the spelling "Krakatoa", as opposed to the Indonesian spelling "Krakatau."

Krakatoa, East of Java was filmed in Super Panavision 70 (with some scenes filmed in Todd-AO), and presented in 70 mm film|70&nbsp;mm Cinerama in some cinemas. Appearing in cinemas as interest in Cineramas widescreen format waned, it is the only disaster movie ever to appear in the format. 

Michael Avallone wrote a novelization of the movie with the same title.

==Historical accuracy==

Krakatoa, East of Java is only very loosely based on the actual events surrounding the catastrophic 1883 eruption of Krakatoa, which destroyed most of the uninhabited island and generated tsunamis exceeding 30 meters (100 feet) in height that struck the western coast of Java and southern coast of Sumatra, killing 36,000 people.  It uses them merely as a backdrop for its storyline.

Hanson s statement early in the film that Krakatoa had been quiet for 200 years is accurate &ndash; the last eruption prior to 1883 appears to have been in 1680  &ndash; and his view that the ongoing volcanic activity on the island, which had begun in May 1883, did not pose a threat to anyone not actually on Krakatoa itself reflected the attitude of many people in the area during the summer of 1883, some of whom treated the erupting volcano as a tourist attraction. 

The Batavia Queen appears to require at least three days to make the voyage from Anjer to Krakatoa. In fact, the two locations are only 31 miles apart (50 km) apart, and the ship could have made the voyage in a few hours. 
 Musi River, lies well inland. The Batavia Queen finds the mission school in ruins and ablaze because of Krakatoas eruption; although Krakatoas eruption was audible in Palembang and the air pressure wave from its final explosion was strong enough to shake the walls of houses and cause cracks to appear in some, the town did not suffer the serious damage implied by the condition of the mission school in the film.  
 Merak and destroying both Anjer &ndash; where it was 10 meters (33 feet) tall &ndash; and the Fourth Point Lighthouse. 

While the Batavia Queen, her passengers and crew, and the story of her voyage are entirely fictitious, her experience in encountering the tsunami at sea at the end of the film bears a striking resemblance to that of the interisland steamer Governor General Loudon (ship)|Gouverneur-Generaal Loudon, which rode out a very large tsunami while steaming in the Sunda Strait on the morning of 27 August 1883. 

==Critical reception==

Krakatoa, East of Java was commercially unsuccessful and received generally poor reviews, with critics claiming that the story was pedestrian, badly paced, and poorly told, and the special effects so constant and overwhelming as to become numbing.  However, a few critics declared the film enjoyable and a vivid depiction of exotic places and life at sea. 

==Later releases==
Reprocessed in "Feelarama," a version of the then-popular Sensurround, the movie was re-released under the title Volcano during the 1970s. 

Although it originally had a running time of 127 minutes (not counting overture, intermission, and exit music included in the 1969 theatrical release), the movie has often been seen since then on television and in 16 mm film|16-mm prints in a truncated 101-minute version, with some scenes shortened or deleted.  In the 101-minute version, the sequences showing key passengers arriving aboard the Batavia Queen at Anjer and the voyage of the Batavia Queen s lifeboat to Anjer are shortened, while the opening sequence showing terrified children at the mission school in Palembang, Charleys song and striptease for Connerly in their stateroom, and Charleys tearful pleas to Hanson to have Connerly set free from the box suspended above the Batavia Queens deck are missing. 

===DVD===
Krakatoa, East of Java was released by MGM Home Video on March 22, 2005 as a Region 1 widescreen DVD.

==In pop culture==
 pirate leader to recover a legendary fortune in diamonds from Krakatoa in 1883 just before the island explodes. 

In the episode "Someone to Watch Over Me" of the series Frasier, one of the titles between sections reads "KRAKATOA, WEST OF JAVA (THE MOVIE WAS WRONG)".
 The Truth": "Those brave Krakatoans, east of Java, who sacrificed so much for so long!"

It is parodied in the Monty Python sketch "Scott of the Antarctic" as "Krakatoa, East of Royal Leamington Spa|Leamington".

In the final episode of Eerie, Indiana, one character can be heard saying on the phone, "Who cares if Krakatoa isnt really east of Java?", referring to getting a bad grade on a test.

Krakatoa, East of Java was the first film that British film critic Mark Kermode ever saw. 
 album of the same name.

The film is mentioned in the Wings (NBC TV series)|Wings episode "Just Say No". When Brian jokes that if his date the previous night had been a movie, "waves would be crashing, rockets would be launching, and volcanoes would be erupting," Lowell replies, "Ive seen that movie: Krakatoa, East of Java. There wasnt much sex, but nobody had any time."

==Notes==
 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 