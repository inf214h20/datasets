The Invisible Circus (film)
{{Infobox film
| name           = The Invisible Circus
| image          = Invisiblecircus.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster Adam Brooks
| producer       = {{Plainlist|
* Julia Chasman Nick Wechsler
}}
| screenplay     = Adam Brooks
| based on       =  
| starring       = {{Plainlist|
* Cameron Diaz
* Jordana Brewster
* Christopher Eccleston
}}
| music          = Nick Laird-Clowes
| cinematography = Henry Braham
| editing        = Elizabeth Kling
| studio         = Fine Line Features
| distributor    = Fine Line Features
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $77,578
}}
 Adam Brooks and starring Cameron Diaz, Jordana Brewster, and Christopher Eccleston. Based on the 1995 novel The Invisible Circus by Jennifer Egan, the film is about a teenage girl who travels to Europe in 1976 in search of answers to her older sisters suicide. During her search, she falls in love with her dead sisters former boyfriend.    The film premiered at the Sundance Film Festival on January 11, 2001, and was released in the United States on February 2, 2001. 

==Plot== California dreamin days are done. Lost and confused in 1977 post free-love San Francisco, this headstrong flower child (played by Jordana Brewster) decides to unravel the mystery of her sisters (Cameron Diaz) suicide in Portugal. In her desperate search for answers, Phoebe is forced to face the past, as well as disturbing truths about her own future. She falls in love with her sisters boyfriend and they travel together to Portugal. She finally discovers the cause of her sisters death and returns home.

==Cast==
* Cameron Diaz as Faith OConnor
* Jordana Brewster as Phoebe OConnor
* Christopher Eccleston as Wolf
* Blythe Danner as Gail OConnor
* Camilla Belle as Phoebe, age 10–12
* Patrick Bergin as Gene
* Isabelle Pasco as Claire
* Moritz Bleibtreu as Eric
* Philipp Weissert as Safehouse Leader
* Nikola Obermann as Hannah
* Robert Getter as American Statesman
* Ricky Koole as Nikki

==Reception==
The film received negative reviews from critics and holds a 21% rating on Rotten Tomatoes based on 61 reviews.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 