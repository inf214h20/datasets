Miffo
{{Infobox film
| name           = Miffo
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Daniel Lind Lagerlöf
| producer       = Daniel Lind Lagerlöf Susanne Lundqvist Joakim Hansson
| writer         = 
| screenplay     = Malin Lagerlöf
| story          = 
| narrator       = 
| starring       = Jonas Karlsson Livia Millhagen Ingvar Hirdwall Liv Mjönes
| music          = 
| cinematography = Olof Johnson
| editing        = 
| studio         =  Sonet Film AB
| released       =  
| runtime        = 97 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Miffo is a 2003 Swedish film directed by Daniel Lind Lagerlöf.

==Cast==
* Jonas Karlsson as Tobias Carling
* Livia Millhagen as Carola Christiansson
* Ingvar Hirdwall as Karl Henrik
* Liv Mjönes as Jenny Brunander
* Kajsa Ernst as Sonja
* Isa Aouifia as Leo
* Fyr Thorwald as Håkan "Håkke" Bodin
* Carina Boberg as Karin
* Gustav Levin as Erik
* Malin Crépin as Anna
* Robin Keller as Jonny
* Jan-Erik Emretsson as Gunnar
* Joel Östlund as Ruben
* Stig Asp as Leif

==External links==
*  
*  

 
 
 
 