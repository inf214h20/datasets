Bloody Spear at Mount Fuji
{{Infobox Film
| name           = Bloody Spear at Mount Fuji
| image          = Chiyari Fuji poster.jpg
| caption        = Japanese movie poster showing an actor Chiezō Kataoka and Mount Fuji at background
| director       = Tomu Uchida Toei
| writer         = Kintaro Inoue (idea) Shintarō Mimura (writer) Fuji Yahiro (writer)
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = February 27, 1955 
| runtime        = 94 minutes
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}} Japanese film directed by Tomu Uchida.

It is a "gruelling cruel tale" from the Edo period. 
 Blue Ribbon Award for best supporting actor. 

==Plot==
The samurai Sakawa Kojūrō is on the road to Edo with his two servants Genta and Genpachi. Kojūrō is a kindly master, but his character totally changes when he consumes alcohol. On the road, they encounter many different people: a traveling singer with her child, a father taking his daughter Otane to be sold into prostitution, a pilgrim, a policeman searching for a notorious thief, and Tōzaburō, the suspicious man the officer has his eyes on. Genpachi, the spear carrier, is also followed by an orphaned boy named Jirō who wants to be a samurai. When Kojūrō and Genpachi inadvertently capture the thief—who was the pilgrim in disguise—Kojūrō is disgusted when the authorities praise him and not his servant, even though Genpachi probably contributed more. He is also upset that he does not have the money to save Otane from being sold. In the end it is Tōzaburō who saves Otane, using the money he saved to rescue his own daughter, but decided to use for Otane after finding out his daughter had died. Depressed, Kojūrō takes Genta out drinking, despite the protests of the latter. When a band of boisterous samurai complain of Kojūrō drinking with someone of lower birth, Kojūrō gets upset. The samurai pull their swords and kill both the servant and his master. Genpachi arrives too late, but in a fury kills all the samurai with the spear. Authorities do not charge him with a crime, so he heads home carrying the ashes of Kojūrō and Genta. When Jirō tries to follow him, he shoos him off, telling him never to become a samurai.

== Cast ==
* Chiezō Kataoka as Genpachi
* Ryunosuke Tsukigata as Tōzaburō
* Chizuru Kitagawa as the Shamisen player
* Yuriko Tashiro as Otane
* Daisuke Katō as Genta
* Eitarō Shindō as the pilgrim
* Toranosuke Ogawa
* Kyoji Sugi Yoshio Yoshida
* Kunio Kaga
* Teruo Shimada (credited as Eijirō Kataoka) as Sakawa Kojūrō
* Chie Ueki as the Shamisen players daughter
* Motoharu Ueki as Jirō

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 