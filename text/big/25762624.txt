Irene (1926 film)
{{Infobox film
| name           = Irene 
| image          = File:Irene lobby card.jpg
| image_size     = 
| caption        = Lobby card
| director       = Alfred E. Green John McCormick
| writer         = James Montgomery (play) June Mathis Rex Taylor George Marion, Jr. (titles)
| narrator       = 
| starring       = Colleen Moore Lloyd Hughes George K. Arthur Joseph McCarthy
| cinematography = Ted D. McCord
| editing        = Edwin Robbins
| distributor    = First National Pictures
| released       =  February 21, 1926 (US)
| runtime        = 90 minutes
| country        = United States Silent English intertitles
}}
 silent romantic John McCormick Joseph McCarthy.

As reported in the book and documentary film The Celluloid Closet, actor George K. Arthur plays a flamboyant gay man in the film named "Madame Lucy". 

==Cast==
* Colleen Moore as Irene ODare
* Lloyd Hughes as Donald Marshall
* George K. Arthur as Madame Lucy Maryon Aye as Helen Cheston
* Ida Darling as Mrs. Warren Marshall
* Edward Earle as Larry Hadley
* Bess Flowers as Jane Gilmour
* Betty Francisco as Cordelia Smith (Uncredited)
* Cora Macey as Mrs. Gilmour Charles Murray as Pa ODare
* Eva Novak as Eleanor Hadley Kate Price as Ma ODare Laurence Wheat as Bob Harrison
* Lydia Yeamans Titus as Mrs. Cheston

==Production==
The scenes which were shot in Technicolor cost a total amount of $100,000. The total budget was $1,500,000. Dutch film magazine Het Weekblad: Cinema & Theater #145 

==Preservation status==
The film exists, with the Technicolor sequences intact. 

==See also==
*List of early color feature films

==References==
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 