The Mouth Agape
{{Infobox film
| name           = The Mouth Agape
| image          = La gueule ouverte poster.jpg
| border         = yes
| caption        = French film poster
| director       = Maurice Pialat
| producer       =
| writer         = Maurice Pialat
| starring       = Hubert Deschamps, Monique Mélinand, Philippe Léotard, Nathalie Baye, Henri Salquin, Alain Grestau, Anna Gayane
| music          =
| cinematography =
| editing        = Arlette Langmann
| distributor    =
| released       = 1974
| runtime        = 82 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}} cinematic realist fashion, a woman going through a terminal illness and also dealing with the tumorous lives of her husband and son.  It was one of the least commercially successful of Pilats films.  It was Pilats third film of the ten that he directed before his death in January 2003. It is also known as The Gaping Mouth and The Gaping Maw.
 The Blue Lagoon, and Kramer vs. Kramer, did the cinematography.  The title is a poetic reference to the open mouth position sometimes found in corpses.  A French anarchist-oriented Magazine|news-magazine has the same name. So does a French alternative rock group. There is no relationship whatsoever between the three.

==Plot== relationships with opera Così fan tutte to poetic effect, relating to these scenes.  In the end scenes, she goes through several final, deeply emotional moments as the disease claims her life.   

==Reception and legacy==
La Gueule ouverte was one of the least commercially successful of Maurice_Pialat#Filmography|Pilats films. 

Some critics have viewed the film as semi-autobiographical,   and it was described as such in a Masters of Cinema re-release.  Pialat’s mother died in the same real place as the one depicted in the film, and the Philippe character is somewhat similar to Pialat himself such that he could be an author surrogate. 

Critic Noel Megahey of the cinema website   has described the film as "of such intensity and uncommon brutal honesty about a subject that is usually treated with more delicacy and sensitivity that it can be difficult and challenging to the viewer" but "the effort is certainly rewarded".    Critic Jonathan McCalmont of the arts website   has labeled the film as one of Pilats most "intrusive" works. McCalmont has also stated that "One of the things that is most fascinating about Pialat as a director is that though completely devoid of sentimentality, his work also shows a perpetual awareness of the temptations that it offers...   lack of sentimentality presents itself as a ruthless focus upon the present."   

 
 film journal Senses of Cinema has praised the film, and commented that:
  }}

==See also==
 
*End-of-life care Artistic realism
*French films of 1974
*List of Masters of Cinema releases

==References==
 

==External links==
*  
*   

 
 

 
 
 
 
 