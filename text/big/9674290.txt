Taxandria (film)
{{Infobox Film
| name           = Taxandria
| image          = Taxandria b.jpg
| caption        = (German DVD cover, 2008)
| director       = Raoul Servais
| producer       = Iblis Films Bibo TV&Film (Berlin) Les Productions Drussart (Paris) Prascino Pictures (Amsterdam)
| writer         = Frank Daniel Raoul Servais Alain Robbe-Grillet
| narrator       = 
| starring       = Armin Mueller-Stahl Elliott Spiers
| music          = Kim Bullard
| cinematography = Raoul Servais (animation)
| editing        = 
| distributor    = ANAGRAM
| released       = 1994
| runtime        = 76 min
| country        =   English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Taxandria (1994) is a partially animated fantasy film by Raoul Servais, based on a book by French writer Alain Robbe-Grillet, and starring, among others, Armin Mueller-Stahl. Being Servaiss first and to date only feature film, Taxandria is notable for the use of Servaiss distinct animation style, the servaisgraphie, as well as its connection to the Belgian graphic novel series Les Cités Obscures whose creator François Schuiten was the films production designer.

==Plot==
Young prince Jan has been sent to a quiet coastal resort to study for his final exams, but instead he spends most of his time with his new friend, the lighthouse keeper. Jan ignores the warnings of the locals who claim that the loony lighthouse man (Armin Mueller-Stahl) eats seagulls for breakfast. Maybe the lighthouse keeper is crazy, but this does not prevent him from introducing prince Jan to the dreamland Taxandria, a phantasmagorical place devoid of time, memory, and progress. Jan learns that a reason for the lighthouse keepers notoriety among the locals is that from time to time he hides political refugees from Taxandria from the authorities; in one scene, we see some of these fugitives arrive at the coast in a boat and taken in by the lighthouse keeper. It is however never made clear whether Taxandria really exists or if the viewer only sees what the lighthouse keeper relates to prince Jan, although once Jan is transported to Taxandria simply by looking into the lighthouses rotating light while the keeper is away.
 conjoined princes, always hidden behind a curtain, and their police force who insure that everyone there lives in the Perennial Present, as it is illegal to discuss the past or future. The police is headed by this worlds evil counterpart of the benevolent lighthouse keeper (a double role for actor Armin Mueller-Stahl), communicating the will and orders of the mute Two Princes, making him the effective ruler of Taxandria.

While at first Taxandria seems a magical, wonderful place, Jan soon sees the darker side of this strange world. The people are not happy living only in the present; it is repressive. Soon he sees that many suffer from extreme paranoia. Aimé (Elliott Spiers), the son of the evil Head of Police, seems to be a catalyst for change in Taxandria, as he is obsessed with making new inventions and learning about the countrys past. Later, Aimé falls in love with Princess Ailée who is trying to free herself from the paradisaical confines of the Garden of Mirth, where women are kept away from men, and discovers the secret of his father that made him Taxandrias effective ruler while being the Head of Police, as well as the true nature of the Two Princes.

==Filming and release==
Taxandria was filmed in Budapest in 1989.  However, due to the enormous amount of post-production work that needed to be completed, it was not released until October 1994. In the interim, Elliott Spiers, who portrayed Aimé Perel in the film, became gravely ill from the side-effects of an anti-malaria inoculation. As a result, he was unable to complete the voice-dubbing required for his character and, consequently, the voice of Aimé Perel is that of another actor. Spiers died in January 1994,  before the films release. Taxandria was premiered at the 1994 Flanders Film Festival and was dedicated to his memory. 

==Taxandria and Les Cités Obscures==
 
Benoît Peeters, co-creator of the Belgian graphic novel series Les Cités Obscures, had collaborated with Servais before on a documentary entitled Servaisgraphia on Servaiss unique animation style that was released in 1992.
 Victorian times on our earth, Taxandrias architecture is reminiscent of Schuitens trademark phantasmagorical architectural fantasies, and another feature the film shares with Les Cités Obscures is a bloated absurd, Kafkaesque bureaucracy.

A reinterpreting graphic novel adaptation of the movie Taxandria was published by Schuiten and Peeters one year prior to the films official release under the title Souvenirs de lEternel Présent: Variation sur le Film Taxandria de Raoul Servais (Arboris, 1993, ISBN 90-344-1011-0, ISBN 978-90-344-1011-5), also including production background informations on the film.

==Awards==

Taxandria won the 1996 Best Film Award at the Franco-Belgian Fantafestival, the Diploma of Merit at the 4th Kecskemét Animation Film Festival,  the Grand Prize of European Fantasy Film in Silver, the International Fantasy Film Award, and the International Fantasy Film Special Jury Award at the 1996 Fantasporto festival. It was nominated for the 1996 Grand Prize of European Fantasy Film in Gold at the Brussels International Festival of Fantasy Film, the 1996 Golden Frog Award at the Camerimage festival, and the International Fantasy Film Award at the 1996 Fantasporto festival.

==Home video releases==
 
*The German VHS was released by absolut Medien GmbH on January 24, 2000.
*The German DVD, including optional English and French language tracks as well, was released by SUNFILM Entertainment on October 24, 2008.

==See also==
* Taxandria (region)

==Notes==
 

==External links==
*  
*  
*   on  

 

 
 
 
 
 