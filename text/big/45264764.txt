Daddy or Mommy
{{Infobox film
| name           = Daddy or Mommy
| image          = 
| caption        =
| director       = Martin Bourboulon
| producer       = Dimitri Rassam Alexandre de La Patellière
| writer         = Guillaume Clicquot de Mentque Matthieu Delaporte Jérôme Fansten Alexandre de La Patellière 
| starring       = Marina Foïs   Laurent Lafitte
| music          = Jérôme Rebotier
| cinematography = Laurent Dailland 	 
| editing        = Virginie Bruant 
| studio         = Chapter 2   Pathé   M6 Films   Jouror Films   Fargo Films   UMedia
| distributor    = Pathé Distribution
| released       =  
| runtime        = 85 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $19,913,803 
}}

Daddy or Mommy ( )  is a 2015 French comedy film directed by Martin Bourboulon. 

== Cast ==
* Marina Foïs as Florence Leroy  
* Laurent Lafitte as Vincent Leroy  
* Alexandre Desrousseaux as Mathias Leroy  
* Anna Lemarchand as Emma Leroy  
* Achille Potier as Julien Leroy 
* Judith El Zein as Virginie 
* Michaël Abiteboul as Paul 
* Vanessa Guide as Marion 
* Michel Vuillermoz as Coutine
* Anne Le Ny as Le Juge
* Yves Verhoeven as Henri
* Yannick Choirat as Xavier

== Box office ==
The film earned $3.85 million in its opening weekend in France.   

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 
 