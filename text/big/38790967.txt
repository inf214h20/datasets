Swami Ayyappan (2012 film)
{{Infobox film
| name           = Swami Ayyappan
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       =  
| producer       = Raju Odunghat
| writer         = Mahesh Vettiyar
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          =  
| cinematography = 
| editing        = 
| studio         =  
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = India
| language       =  
| budget         = 
| gross          = 
}} Indian film Swami Ayyappan.    {{cite web | title = Ayyappan now in toon avatar |publisher=The Hindu Toonz Animation Telugu languages.  Mahesh Vettiyar wrote the story, and co-directed the film with Chetan Sharma.    P. Jayakumar served as executive producer. 

==Synopsis==
The film covers Ayyappans life at Pandalam as the young Ayyappan|Manikandan, born out of the union between Lord Shiva and Vishnu in his Mohini avatar, his childhood at the Gurukula|gurukul, and the miracles he performed.  It depicts Manikandan’s fight with Udayana, the slaying of the demoness Mahishi and the instances that lead to his friendship with Vavar, a robber from Turkistan. This friendship is considered to be the epitome of religious tolerance till this day. Once the purpose of his embodiment is fulfilled, Manikandan gets united with the idol in the Sabarimala Temple and is hence forth known as ‘Swami Ayyappan’.

==Reception== TASI Anifest India Viewer’s Choice Award in the best film - commissioned category,    and the 2014 FICCI BAF award for Best Animated promo (international).  

==References==
 

==External links==
*   on Facebook
*   at the Internet Movie Database

 
 
 

 