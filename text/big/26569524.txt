And Everything Is Going Fine
 
{{Infobox film
| name           = And Everything Is Going Fine
| image          = And Everything Is Going Fine.jpg
| director       = Steven Soderbergh
| producer       = Kathie Russo Amy Hobby Joshua Blum
| writer         = 
| editing        = Susan Littenberg
| cinematography = 
| starring       = Spalding Gray
| music          = Forrest Gray
| distributor    = 
| released       =  
| runtime        = 89 minutes
| language       = English
| country        = United States
| budget         = 
}}
And Everything Is Going Fine is a 2010 documentary film directed by Steven Soderbergh about the life of the late monologuist Spalding Gray. It premiered at the 2010 Slamdance Film Festival and was screened at the 2010 SXSW Film Festival and the 2010 Maryland Film Festival. Soderbergh had earlier directed Grays filmed monologue, Grays Anatomy (film)|Grays Anatomy.

Soderbergh decided against recording narration and new interviews in the manner of, for instance, Errol Morris. The film instead consists entirely of archival footage, principally numerous excerpts from monologues by and interviews with Gray, spanning some 20 years, as well as home movies of Spalding as an infant.

Music for the film was composed by Grays son Forrest.

==External links==
*  
*  
*  
*  
*  
*   from Variety (magazine)|Variety
*  
*  

 
 

 
 
 
 
 
 
 
 


 