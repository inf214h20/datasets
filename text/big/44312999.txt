The Rascal of Madrid
{{Infobox film
| name =   The Rascal of Madrid 
| image =
| image_size =
| caption =
| director = Florián Rey
| producer =
| writer = Florián Rey
| narrator =
| starring = 
| music = 
| cinematography = Carlos Pahissa  
| editing = 
| studio = Florián Rey P.C. 
| distributor = 
| released = 1926
| runtime = 
| country = Spain Spanish intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
}}
The Rascal of Madrid (Spanish:El pilluelo de Madrid) is a 1926 Spanish silent film directed by Florián Rey. 

==Cast==
* Guillermo Figueras 
* Alfredo Hurtado    
* Pedro Larrañaga
* Manuel Montenegro   
* Ricardo Núñez  
* Ricardo Piroto   
* Flora Rossini  
* Elisa Ruiz Romero

== References ==
 
 
==Bibliography==
* Hortelano, Lorenzo J. Torres. World Film Locations: Madrid. Intellect Books, 2012.

== External links ==
* 

 

 
 
 
 
 
 
 

 