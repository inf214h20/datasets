The Red Shadows
{{Infobox film
 | name = The Red Shadows
 | image = The Red Shadows.jpg
 | caption =
 | director =  Francesco Maselli
 | writer = Francesco Maselli
 | story = 
 | starring = Ennio Fantastichini
 | music = Giovanna Marini  Angelo Talocci 
 | cinematography = Felice De Maria
 | editing =   
 | producer =  
 | released =  
 | language = Italian
 }} 2009 Cinema Italian drama film written and directed by Francesco Maselli.

It was screened out of competition at the 66th Venice International Film Festival.  

== Cast == 
* Ennio Fantastichini as Varga
* Arnoldo Foà as  Massimo
* Roberto Herlitzka as Sergio Siniscalchi
*  Valentina Carnelutti as  Margherita
*  Flavio Parenti as  Davide
* Lucia Poli as  Vanessa
*  Eugenia Costantini as  Betta
* Luca Lionello as Stefano
*  Carmelo Galati as  Alessandro
*  Veronica Gentili as  Rossana 
*  Antonino Bruschetta as  Conduttore TV1
* Roberto Citran as  Bergonzi
* Laurent Terzieff as  Director of Le Monde 
* Ricky Tognazzi as  Politician 

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
   

 
 