Nous, princesses de Clèves
{{multiple issues|
 
 
}}

{{Infobox film
| name = Nous, princesse de Clèves
| director = Régis Sauder
| producer = Régis Sauder
| story = Régis Sauder from an original idea of Anne Tesson
| studio = Nord / Ouest Documentaires, in co-production with France Ô
| distributor = Shellac Distribution (France)
| released =  
| runtime = 69 minutes
| country = France
| language = French
}}
Nous, princesses de Clèves is a French documentary film directed by Régis Sauder, filmed at the Lycée Diderot and released on 3 March 2011. 

==Synopsis==
The movie follows the thoughts and emotions of various teenagers as they prepare to take their Baccalauréat by reading the classic 17th century French novel, La Princesse de Clèves. The film highlights the differences and connections between the lives of the students, many of which are from immigrant and working-class families, and the passions and plots of the 17th century French court.

==Selected cast==
*Sarah Yagoubi as herself
*Abou Achoumi as himself
*Laura Badrane as herself
*Morgane Badrane as herself	
*Manel Boulaabi as herself
*Virginie Da Vega as herself
*Thérèse Demarque as herself

==References==
 

==External links==
*  

 

 
 
 
 
 


 