Pendragon: Sword of His Father
{{Infobox Film
| name           = Pendragon: Sword of His Father
| image          = Pendragon- Sword of His Father.jpg
| image_size     = 
| caption        = 
| director       = Chad Burns
| producer       = 
| writer         = 
| narrator       = 
| starring       = Aaron Burns Nick Burns Marilyn Burns Erik Dewar Andy Burns
| music          = Aaron Burns Marilyn Burns	
| cinematography = Ethan Ledden
| editing        = Nicholas Burns
| studio         = Burns Family Studios
| distributor    = 
| released       = November 25, 2008
| runtime        = 111 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Christian historical fiction  film based on the Arthurian legend directed by Chad Burns.  It was filmed in five U.S. states, and was released on November 25, 2008. The film won "Best Family Picture" and two other awards at the 2009 Bare Bones International Film Festival,  and will also be featured at the SENE Film, Music & Arts Festival. 

== Plot == Artos who is raised to believe that God has a purpose for his life. After a tragic event resulting in the burning of his village, the death of his father, and the disappearance of his little sister Adria, he is taken into slavery by the Saxons, where Artos begins to question his God. He soon manages to escape from the Saxons and is nursed back to health by a Roman outcast named Lailoken. When he fully recovers, Artos travels to a Celtic fortress hidden in the Welsh mountains where he becomes a great warrior under King Ambrosius. Advancing through the military ranks, Artos begins to understand that his fathers vision was not based on the strength of man, but on the plan of God. However, his success causes one of Ambrosiuss men, his pagan Captain of the Guard Cadern, to become jealous of Artoss rapid rise to power in the military. Using a secret guard that he created several years ago, Cadern is able to murder Ambrosius, and to frame the assassination on Artos. While on trial, Artos escapes from the court, and is able to elude Caderns elite guard. Further events force Artos to decide between following Gods plan unto certain death or abandoning God to save himself.

== Cast == Artos Pendragon
* Nick Burns as Cadeyrn-Ambrosius pagan captain of the guard, ruthless and jealous of Artos rise in rank and favor
* Marilyn Burns as Guinevere|Wenneveria-The beautiful daughter of Ambrosinus
* Erik Dewar as Brotus Ambrosius
* Chad Burns as Lailoken
* Raymond Burns as Justinian Pendragon-father of Artos and a garrison commander in the east of britannia; killed during the initial Saxon invasion.
* Rebekah Wixom as Adria-Artos sister

== References ==
 

== External links ==
*  
*  
*   at The Dove Foundation

 
 
 
 
 
 
 
 