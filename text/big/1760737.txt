Red's Dream
{{Infobox film
| name = Reds Dream
| image = Reds Dream poster.jpg
| caption = Poster for Reds Dream
| alt = Poster for Reds Dream
| director = John Lasseter
| producer = 
| writer = John Lasseter
| starring =
| music = David Slusser
| cinematography =
| editing =
| studio = Pixar
| distributor = Pixar
| released =  
| runtime = 4 minutes
| country = United States
| awards =
| language = English
| budget =
| gross =
}}
Reds Dream is a 1987 American computer-animated short film produced by Pixar and directed by John Lasseter. The short film, which runs four minutes, stars Red, a unicycle. Propped up in the corner of a bicycle store on a rainy night, Red dreams about a better place. Reds Dream was Pixars second computer-animated short following Luxo Jr. in 1986, also directed by Lasseter.

Reds Dream is more strongly character-driven than Luxo Jr. The short was designed to demonstrate new technical innovations in imagery. The short was created by employing the companys own Pixar Image Computer, but the computers memory limitations led the animation group to abandon it for further projects. Space was growing tight at the company, and as a result Lasseter and his team worked out of a hallway during production, where Lasseter sometimes slept for days on end.

The short premiered at the annual SIGGRAPH conference in Anaheim in July 1987 and received general enthusiasm from its attendants.  Reds Dream was never attached to any later Pixar feature unlike many other early Pixar shorts. The film was released for home video as part of Tiny Toy Stories in 1996 and Pixar Short Films Collection, Volume 1 in 2007.

== Plot ==
Set in a lonely city on a rainy night, the film takes place in a bicycle shop (named "Ebens Bikes" for Pixar animator Eben Ostby) that is closed for the night. In the corner of the shop sleeps Red, a red unicycle who languishes in the "clearance corner", waiting to be purchased. As the camera zooms on him, the sound of rain falling turns into a drumroll, and we go into the dream-sequence. In his dream, Red is being ridden by a circus clown (which was nicknamed Lumpy due to his appearance) as part of a juggling act. The clown enters the ring, accompanied by a fanfare, expecting a huge applause, but instead receives only a few scattered claps from different parts of the (unseen) audience. Nevertheless, Lumpy starts juggling three balls whilst riding Red, occasionally dropping them as he does. However, Red slides out from underneath Lumpy (while the clown stays afloat) and spikes the balls back to him with his bike pedals. The confused clown ponders this for only a second before continuing on with his act. At this point, Red is forced to catch another ball which Lumpy unintentionally throws across the ring. Lumpy continues to ride in the air while juggling the other two balls while Red bounces the green ball up and down. Eventually Lumpy comes to a sudden realization, and looks between his legs, only to discover hes been riding on nothing before he falls to the ground (and seemingly disappears from the dream). Red catches the other two balls and begins juggling all three of them, and then balances them on top of each other, after which he receives an uproarous applause. But then the sound of clapping turns into the sound of rain, and Red awakens, left to face bleak reality. Depressed, he returns to the corner where he was previously resting, and goes back to sleep. The short ends with the final image of the neon sign for "Ebens Bikes".

==Production== William Reeves and Eben Ostby starting working on their own separate ideas. Ostby had wanted to animate a bicycle, and Reeves began working on a city during a rainy night. Ultimately, the three combined their ideas, which resulted in the creation of Reds Dream. The film project came with two technical rationales. The bike shop scenes at the beginning and end were to demonstrate the tendering of highly complex imagery; with the bikes and their spokes and the shop fixtures, a typical frame of the scene had more than ten thousand geometric primitives, which in turn were made up of more than thirty million polygons. Price, p. 102 

The idea of a bike shop setting was inspired by Eben Ostby, a cycling enthusiast and graphics programmer at Pixar, who had been working on generating a complex still image of a bike shop.  The dream sequence was to be a demonstration of rendering with the Pixar Image Computer. An engineer named Tony Apodaca had converted Pixars rendering software to run on the PIC, but it turned out that the machines design left its processors without enough memory for a program as complex as Reyes, and so Apodaca was able to convert only a portion of Reyess features. On account of those limitations, the dream sequence was cruder in its look than the rest of the film, and Reds Dream was both the first and last Pixar film to be made with the Pixar Image Computer. 

Space at Pixar was growing tight in its Marin County bungalow; while Reds Dream was underway, the animation group—Lasseter, plus several "technical directors" who created models and shaders and such—worked out of a hallway. Toward the end of production, Lasseter worked and slept in the hallways for days on end.  One night about two weeks before the deadline for SIGGRAPH, an engineer named Jeff Mock brought his camcorder around and shot an ersatz interview with Lasseter, who joked about the conditions.  He had just spent five days animating a sequence of three hundred frames-twelve and a half seconds of film. Price, p. 103 
 Frank Thomas Nine Old Men, visited Lasseter at Pixar after Reds Dream had just been finished and they watched a screening. Thomas was evidently freed of his former doubts about computer animation, expressed in 1984 essay in which he argued computer animation could never produce anything as meaningful as its hand-drawn predecessor. He shook Lasseters hand afterward and said meaningfully, "John, you did it." 

==References to other media==
*Andre from The Adventures of André and Wally B. can be seen on the clock.
*Luxo Sr. can be easily seen on the counter.
*The circus floor design looks like a Luxo Ball.

==Notes==
 

==References==
*    

==External links==
*  
*  
*  
*  

 
{{succession box
 | before = Luxo Jr. short films
 | years  = 1987
 | after  = Tin Toy}}
 

 
 

 
 
 
 
 
 
 
 
 