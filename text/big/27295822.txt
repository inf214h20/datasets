Private Angelo (film)
{{Infobox film
| name           = Private Angelo
| image_size     =
| image  	 = "Private_Angelo"_(1949_film).jpg	
| caption        = Australian 1-sheet poster Michael Anderson Peter Ustinov
| producer       = Michael Anderson Peter Ustinov
| writer         = Michael Anderson Peter Ustinov Eric Linklater (novel)
| narrator       =
| starring       = Peter Ustinov Godfrey Tearle María Denis Marjorie Rhodes James Robertson Justice
| music          = Vittorio Pirone (musical director) Società Filarmonica di Trequanda (played by)
| cinematography = Erwin Hillier
| editing        = Charles Hasse Associated British-Pathé
| released       = 1949
| runtime        = 106 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =£89,980 (UK) 
| preceded_by    =
| followed_by    =
}}
 British comedy comedy war Michael Anderson and Peter Ustinov. It starred Ustinov, Godfrey Tearle, Maria Denis and Marjorie Rhodes.  It depicts the misadventures of a soldier in the Italian Army during the Second World War. It was adapted from the novel Private Angelo by Eric Linklater. The films costumes were designed by Ustinovs mother Nadia Benois.

A number of scenes in the film were filmed in the Italian village of Trequanda in the Province of Siena.

==Cast==
* Peter Ustinov - Private Angelo
* Godfrey Tearle - Count Piccologrando
* Maria Denis - Lucrezia
* Marjorie Rhodes - Countess
* James Robertson Justice - Feste
* Moyna Macgill - Marchesa Dolce
* Robin Bailey - Simon Telfer
* Harry Locke - Corporal Trivet
* Bill Shine - Colonel Michael John Harvey - Corporal McCunn
* Diana Graves - Lucia

==References==
 
Nicol, Christopher (2012) "Eric Linklaters Private Angelo and The Dark of Summer". Glasgow: ASLS ISBN 978-1906841119

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 