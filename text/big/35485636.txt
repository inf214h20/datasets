Pudhu Padagan
{{Infobox film
| name           = Pudhu Padagan
| image          = Pudhu Padagan DVD cover.svg
| caption        = DVD cover
| director       = S. Thanu
| producer       = S. Thanu
| writer         = S. Thanu
| starring       =  
| music          = S. Thanu
| cinematography = Vidya
| editing        = G. Jayachandran
| studio         = Kalaippuli International
| distributor    = Kalaippuli International
| released       =  
| country        = India
| runtime        = 130 min
| language       = Tamil
}}
 1990 Cinema Indian Tamil Tamil drama Amala in lead roles. The film, produced and had musical score by S. Thanu, was released on 14 April 1990.  

==Plot==

Manikkam (Vijayakanth) and Devi (Amala (actress)|Amala) are cousin and they are in love since their childhood. Durai has a sister Valli and she gets married with Raja. Devi has a brother (Vagai Chandrasekhar), a sister-in-law Gowri (Rajalakhsmi) and a niece Baby ( Baby  Aparna).

Manikkams former boss (Sarathkumar) steals god status and sells it illegally, he also wants to wed Devi. One day, Manikkam finds the culprit and sends him to jail. Valli and her husband has trouble with Arumugam (Anandaraj). Manikkam goes to Rajas village to solve their troubles.

Baby vanishes several times and the doctor (Radha Ravi) says that Baby has a serious decease. To save Baby they must have lot of money but the doctors son (Mano) asks Devi to marry him and they will operate Baby for free.

When Manikkam is back to his village, he sees Devi married. Devi goes to the doctors house and the doctors mother is found dead. Manikkam tells what happened in Rajas village. Raja killed Arumugam, who beat his wife Valli, and Manikkam to save his brother-in-law went to jail for six months.

Lonely without his lover, Manikkam becomes mad. Devis husband has a car accident and dies before their wedding night. Devis father-in-law advises her to go to her village to forget this misadventure. Devis father-in-law treats Manikkam but he fails. With Devis help, Manikkam become again as before and the doctor tells Devis choice.

Manikkams former boss tries to rape Devi but Manikkam kills him and Devi also dies because of her injuries.

==Cast==

*Vijayakanth as Manikkam Amala as Devi
*Sarathkumar as Manikkams former boss
*Radha Ravi as the doctor Chandrasekhar as Devis brother
*Rajalakshmi as Gowri
* Baby  Aparna as Baby
*Chinni Jayanth
*Charle
*Anandaraj
*Rajalakshmi
*Meesai Murugesh as Kunjithapatham
*Usilaimani
*LIC Narasimhan
*Thalapathy Dinesh
*Mano as the doctors son (guest appearance)
*Anandaraj as Arumugam (guest appearance)

==Soundtrack==

{{Infobox album |  
  Name        = Pudhu Padagan |
  Type        = soundtrack |
  Artist      =  S. Thanu |
  Cover       = |
  Released    = 1990 |
  Recorded    = 1990 | Feature film soundtrack |
  Length      = 37:06 |
  Label       = |
  Producer    = S. Thanu |  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer S. Thanu. The soundtrack, released in 1990, 11 features tracks with lyrics written by S. Thanu.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration
|- 1 || Adhikaalai Naan Paadum Boopalamea || S. P. Balasubrahmanyam, K. S. Chithra  || 4:19
|- 2 || Akkam Pakkam Yarum Ella || S. P. Balasubrahmanyam, S. Janaki || 4:50
|- 3 || Chinna Poove Mella || S. P. Balasubrahmanyam || 2:30
|- 4 || Paingkilile Parthathu Nenjam || S. P. Balasubrahmanyam || 0:45
|- 5 || Aththa Ponnu Vaadi Yun Manasa Thadi || S. P. Balasubrahmanyam || 4:19
|- 6 || Baby Shalini || 1:15
|- 7 || Kaadoram Kanagambaram || S. P. Balasubrahmanyam || 5:00
|- 8 || Azhagu Mayil Thogai Virithaduthu || S. P. Balasubrahmanyam || 4:45
|- 9 || Vaaraayo Vanna Kili Yengae Chendrayo || Jayachandran || 4:01
|- 10 || Unnai Ninanthu Naan Urigi Nindren || S. P. Balasubrahmanyam || 1:12
|- 11 || Malaiya Kodainju Pathaya Vechu || S. P. Balasubrahmanyam, K. S. Chithra || 4:10
|}

==References==
 

 
 
 
 