Otto – Der Film
{{Infobox film
| name           = Otto – Der Film
| image          = 
| image_size     = 
| caption        = 
| director       = Xaver Schwarzenberger, Otto Waalkes
| producer       = Horst Wendlandt
| writer         = Bernd Eilert, Robert Gernhardt, Peter Knorr, Otto Waalkes
| narrator       = 
| starring       = Otto Waalkes Jessika Cardinahl Elisabeth Wiedemann Sky du Mont Peter Kuiper
| music          = Herb Geller
| cinematography = Xaver Schwarzenberger
| editing        = Jutta Hering
| distributor    = 
| released       =  
| runtime        = 
| country        = Germany German
| budget         = 
}}

Otto – Der Film (German: Otto, the movie) is a German comedy film from 1985, starring Otto Waalkes. With 14.5 million sold tickets its the most successful German movie.

==Plot==
The film starts with debris floating in the middle of the ocean, including a toilet seat, through which Otto emerges and begins to relate how he has gotten to be in this situation.
 DM and 50 Pfennig becomes a constant object of worry and temptation for Otto throughout the film.

During one of his earlier attempts to make money, Otto inadvertently saves the life of Silvia von Kohlen und Reibach, the young heiress of an enormously wealthy family. Otto is introduced to the Kohlen und Reibachs to receive their gratitude, but Otto is quick to note that he could use their wealth to pay back his debt. But every chance he gets slips through his fingers, either owing to the callings of his conscience (such as when he attempts to shoot a hare for a reward which matches his debts exactly) or by dumb luck (when the wine he receives from the Kohlen und Reibachs turns out to be quite valuable, but only after a wine lover has consumed a considerable quantity of it). Silvia and Otto also find themselves drawn to each other, but their greatest hazard against their getting together is Silvias stern mother, Konsulin ("Consul (representative)|Consul") von Kohlen und Reibach, who wishes her daughter to marry befitting to her status and who has selected a prospective candidate named Ernesto, a handsome South American millionaire.
 hijack the plane. Unfortunately, the two break out into another argument, in which course they knock out both pilots, so they force Otto to fly. This of course wreaks havoc as Otto sends the plane rolling, subduing the two bankrobbers in the process; Otto reveals his presence and his love to Silvia, who happily joins him in the cockpit, and Konsulin von Kohle und Reichbach has to learn to her shock that "Ernesto" is really a fraud named Harald.
 passenger jet aircraft carrier, warm welcome from the local carnival-obsessed natives and Otto and Silvia finally become an item.

==Cast==
* Otto Waalkes: Otto
* Jessika Cardinahl: Silvia von Kohlen und Reibach
* Elisabeth Wiedemann: Konsulin von Kohlen und Reibach
* Sky du Mont: Ernesto (aka Harald)
* Peter Kuiper: Shark
* Karl Lieffen: Flopmann
* Tilly Lauenstein: madam
* Gottfried John: Sonnemann
* Andreas Mannkopff: Haenlein
* Lutz Mackensy: springbok owner
* Johannes Heesters: wine lover
* Günther Kaufmann: soldier „Bimbo“
* Panos Papadopulos: Stavros
* Wilken F. Dincklage: barkeeper
* Erich Bar: rocker

==Notes==
*In German, the terms in the name "Kohlen und Reibach" are informal terms for "money" and "profit", used here as a parodic statement playing off the name of industrial dynasty (Krupp) von Bohlen und Halbach.
*The film features a parody of Michael Jacksons music video Michael Jacksons Thriller (music video)|Thriller, except that the zombie dancers are replaced here with lookalikes of the German folksinger Heino adapting the songs tunes to the lyrics of Heinos song "Schwarzbraun ist die Haselnuss".

== External links ==
*  
*   at Filmportal.de

 
 
 
 
 
 
 
 