The Legend of Seven Cutter
{{Infobox film name        = The Legend of Seven Cutter image       =  The Legend of Seven Cutter.jpg caption     = Poster to The Legend of Seven Cutter (2006) director    = Kwon Nam-gi producer    = Kim Nam-hee writer      = Oh Sang-ho   Huh Jung starring    = Ahn Jae-mo   Yoon Eun-hye cinematography = Lee Seok-ki editing     = Lee Eun-su music       = Yang Jun-yeong   Kim Bong-soo   distributor = CJ Entertainment  released    =   country     = South Korea language    = Korean budget      =   gross       =    film name   = {{Film name hangul      = 카리스마 탈출기 hanja       =  rr          = Kariseuma Talchoolgi mr          = K‘arasŭma T‘alch‘ulgi}}
}}
The Legend of Seven Cutter ( ; lit. "Escaping from Charisma") is a 2006 South Korean film starring Ahn Jae-mo and Yoon Eun-hye. 

== Plot ==
The "Seven Cutter" is a legendary fighter throughout various high schools within Korea. His name is Jung Han-soo and he remains unbeaten by the toughest fighters from many different schools. Many who had tried to defeat him not only suffered the circumstances of losing, but is left with his trademark: a 7-centimeter scar that he always leaves behind them to remind them of their loss.

When a high schooler named Jung Han-soo arrives at Seong Ji High School, he is instantly mistaken for the "Seven Cutter." An innocent kid, at first he is blissfully unaware of why the other students are giving him such an icy reception. So when the toughest boy at Seong Ji High School challenges him to a battle for the title of the toughest guy in school, he has no choice but defend himself and fight. Then, when he accidentally wins, things start getting really confusing for him.

Soon, Han-soo meets tomboy Han Min-joo, who is not just a great, skilled boxer, but a tough, hard-to-deal-with knockout as well. After a few coincidences that make him look like a pervert, she also asks him for a battle for revenge. He defends himself by pretending to have a crush on her, even though he likes the pretty, but snotty, girl in school. Min-joo decided to accept his "love," and eventually starts to like his unique personality. Later, when Han-soo finally realizes he has fallen for her, Min-joo is heart-broken to find out that he likes the pretty girl, and that he never liked her. Then the bad guys arrive and try to kidnap Min-joo.

== Cast ==
*Ahn Jae-mo as Jung Han-soo
*Yoon Eun-hye as Han Min-joo
*Lee Jung as Baek Sung-gi
*Hyun Young as Kim Seon-mi
*Jeong Jun-ha as Koh Min-shik
*Park Seul-gi as Kang Hae-soo, Min-joos friend whos in love with Mr. Koh
*Cheon Myeong-hoon as Han-soos brother
*Park Hyo-jun as Sung-gis friend
*Joo Ho as Pyo Jung-shik
*Han Da-min as Lee Yoon-ah

==References==
 

== External links ==
*   
* 
* 
* 
*  at Love HK Film

 
 
 
 
 
 
 