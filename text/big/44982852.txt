Maidan (film)
{{Infobox film
| name           = Maidan
| image          = Maidan filmposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Sergei Loznitsa
| producer       = Sergei Loznitsa Maria Baker
| writer         = 	
| screenplay     = 
| based on       = 
| starring       = 
| music          = 
| cinematography = Sergei Loznitsa Serhiy Stetsenko Mykhailo Yelchev
| editing        = Sergei Loznitsa Danielius Kokanauskis
| studio         = Atoms & Void
| distributor    = United States: The Cinema Guild France: ARP Sélection
| released       =  
| runtime        = 130 minutes
| country        = Ukraine Netherlands
| language       = Ukrainian
| budget         = 
}}

Maidan ( ) is a 2014 documentary film, directed by Sergei Loznitsa. It focuses on the Euromaidan movement of 2013 and 2014 in Maidan Nezalezhnosti (Independence Square) in Ukraines capital Kiev. It was filmed during the protests and depicts different aspects of the revolution, from the peaceful rallies to bloody clashes between police and civilians.  

The film premiered on May 21, 2014 at the 2014 Cannes Film Festival.  It had a theatrical release in the United States on December 12, 2014. 

==Synopsis==
The follow explores and follows the protests and violence in Kievs Maidan Nezalezhnosti (Independence Square) which lead to the overthrow of President Viktor Yanukovych.

==Releases== world wide on February 20, 2015, which coincided with the date of the revolution in Ukraine. 

==Reception==
The film has received positive reception from critics. Review aggregator Rotten Tomatoes reports that 100% of 20 film critics have given the film a positive review, with a rating average of 7.7 out of 10.  On Metacritic, which assigns a weighted mean rating out of 100 reviews from film critics, the film holds an average score of 86, based on 9 reviews, indicating a Universal acclaim response. 
 Variety said that "In contrast with most documentaries made in the wake of an historic event, "Maidan" will last beyond the current Ukrainian upheaval to stand as compelling witness and a model response to a seminal moment too fresh to be fully processed."  Lee Marshal reviewing the film for Screen International  said that " ven in the midst of the molotovs, Loznitsas sincere and memorable document retains its fascination with those small details that make us human, and make democracy worth fighting for."  Michael Atkinson of The Village Voice called it "Easily the most rigorous, vital, and powerful movie of 2014, Sergei Loznitsas Maidan may be a perfect Bazinian cinema-machine - reality is captured, crystallized, honored for its organic complexity, and delivered unpoisoned by exposition or emphasis."  Andrew Pulver, in a review for The Guardian, stated: " n some ways its perfect, in its Eisenstein-esque refusal to compromise on the idea of the crowd as the key participant. In other ways, though, its sternness hampers it." 

Howard Feinstein of   gave the film three out of four stars and said that " t puts the viewer inside Maidan, allowing them to draw their own conclusions about the ideas and agendas espoused by the movements leaders and participants." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 