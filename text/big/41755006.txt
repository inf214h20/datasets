Exit Dying
{{Infobox film
| name           = Exit Dying
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Bob Jacobs
| producer       = Bob Jacobs
| writer         = Bob Jacobs
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Henry Darrow
| music          = 
| cinematography = 
| editing        = Bob Jacobs
| studio         = 
| distributor    = 
| released       =    
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $15,000
| gross          = 
}}
Exit Dying (also referred to at Exit, Dying) is a 1976 made for TV film that was written, directed, produced, and edited by University of Wisconsin–Oshkosh professor Bob Jacobs.   The film starred Henry Darrow and centered upon a man caught up in supernatural happenings at an opera house. Exit Dying was filmed in Oshkosh, Wisconsin at the Grand Opera House.   

The film premiered on October 31, 1976 at the Oshkosh Grand Opera House and received a positive review from the Daily Northwestern. 

==Synopsis==
When a renovator (Henry Darrow) decides to purchase an old theater with the intent to fix it up and sell it for a profit, hes unaware that the place is a safe haven for the supernatural. As he spends more and more time there, it becomes more apparent that his attentions to the location are very unwelcome.

==Production==
Author Ray Bradbury was consulted for the film and Jacobs used the film as a project for his students.  The students raised $15,000 towards the funding of the film.    Filming took place during mid 1976 and Jacobs and his students shot primarily between midnight and 8 am.   After filming Jacobs claimed that he saw the ghost of Percy Keene, a former manager for the Oshkosh Grand Opera House. 

==References==
 

==External links==
* 

 
 
 


 