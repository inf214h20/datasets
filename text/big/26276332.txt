Charlie Chan's Greatest Case
{{Infobox film
| name           = Charlie Chans Greatest Case
| image          = Charlie-chan-greatest-case.jpg
| image_size     =
| caption        =
| director       = Hamilton MacFadden
| producer       =
| writer         = Earl Derr Biggers (novel) Lester Cole Marion Orth
| narrator       = Heather Angel
| music          =
| cinematography =
| editing        =
| studio         = Fox Film Corporation
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Charlie Chans Greatest Case (1933) is a murder mystery film starring Warner Oland as the Oriental detective Charlie Chan. It was based on the Earl Derr Biggers novel The House Without a Key.

Oland made a series of Charlie Chan films; along with three others, this one is considered to be a lost film.

==Reception==
The New York Times reviewer wrote, "As far as the mystery of these particular murders is concerned it is not difficult for the audience to decide on the identity of the slayer, but the manner in which Chan makes his deductions is always interesting." 

==Cast==
*Warner Oland as Charlie Chan Heather Angel as Carlotta Eagan
*Roger Imhof as The Beachcomber John Warburton as John Quincy Winterslip
*Walter Byron as Henry Jennison
*Ivan F. Simpson as Brade
*Virginia Cherrill as Barbara Winterslip Francis Ford as Captain Hallett
*Robert Warwick as Dan Winterslip Frank McGlynn Sr. as Amos Winterslip
*Clara Blandick as Minerva Winterslip
*Claude King as Capt. Arthur Cope
*William Stack as James Eagan
*Gloria Roy as Arlene Compton
*Cornelius Keefe as Steve Letherbee

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 