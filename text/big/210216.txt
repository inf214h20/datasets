The L-Shaped Room
 
 
{{Infobox film
| name           = The L-Shaped Room
| image          = L-shapedroom .jpg
| director       = Bryan Forbes
| writer         = Bryan Forbes Tom Bell Brock Peters
| music          = John Barry (composer)
| cinematography = Douglas Slocombe
| editing        = Anthony Harvey James Woolf
| distributor    = British Lion Films (UK)   Columbia Pictures (US)
| released       = 1962
| runtime        = 126 min.
| gross = $1,000,000 (US/ Canada) 
| language       = English
}}
  Tom Bell.
 novel by Lynne Reid Banks.

Leslie Carons performance won her the Golden Globe Award and BAFTA Award for best actress, and also earned her a nomination for the Academy Award for Best Actress. 

==Synopsis==
A 27-year old French woman, Jane Fosset (Caron), arrives alone at a rundown boarding house in Notting Hill, London, moving into an L-shaped room. Beautiful but withdrawn, she encounters the residents of her house, each a social outsider in his or her own way.

Jane is pregnant and has no desire to marry the father. On her first visit to a doctor, she wants to find out if she really is pregnant and consider her options. The doctors facile assumption that she must want either marriage or an abortion insults her to the extent that Jane determines to have the child.

She and Toby (Bell) start a romance, which is disrupted when he learns that she is pregnant by a previous suitor. They try to work things out, but he is also unhappy with his lack of income and success as a writer.

Jane befriends the other residents and they help her when she goes into labour. Toby visits her in the hospital and gives her a copy of his new book, called The L-Shaped Room. After leaving the hospital, Jane journeys home to her parents in France, saying goodbye to the room where she had lived for seven months.

==Cast==
*Leslie Caron – Jane Fosset Tom Bell – Toby
*Brock Peters – Johnny
*Cicely Courtneidge – Mavis
*Bernard Lee – Charlie
*Patricia Phoenix – Sonia
*Emlyn Williams – Dr. Weaver
*Avis Bunnage – Doris
*Gerry Duggan – Bert
*Mark Eden – Terry
*Antony Booth – Youth in Street  
*Harry Locke – Newsagent
*Gerald Sim – Doctor in Hospital

==In popular culture==
The Smiths chose to open their 1986 album, The Queen Is Dead, with a sound sample from this film – taken from the scene at the house in London during the Christmas season, in which Mavis leads her fellow Brits through an off-key chorus of "Take Me Back to Dear Old Blighty".

==Music== Piano Concerto No. 1 in D minor, Op. 15 is used as the background music, and excerpts occur frequently throughout the film. 

== References ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 