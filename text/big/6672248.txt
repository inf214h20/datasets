Masahista
{{Infobox Film |
  name     = Masahista |
  image          =|
  caption  = Movie Screenshot|
  writer         = Brillante Mendoza Boots Agbayani Pastor |
  starring       = Coco Martin Jacklyn Jose Allan Paule Katherine Luna |
  director       = Brillante Mendoza |
  producer       = |
  distributor    = |
  released   = 2005|
  runtime        = 76 mins |
  country = Philippines |
  language = Tagalog, Kapampangan |
  music          = |
  awards         = |
  budget         = }}

Masahista (English Title: The Masseur) is a 2005 Filipino film written and directed by Brillante Mendoza. This film is about a young man who gives massages to gay men in Manila and had a relationship.

==Synopsis==

Iliac is a young masseur who went home to Pampanga to find out that his bedridden father is dead. Iliac assists in the preparation of his fathers burial including dressing his dead father up inside the morgue.

==Awards==

{|class=wikitable
!Year
!Film Festival/Award
!Award
!Category/Recipient
|-
| 2006 || Young Critics Circle, Philippines || Best Movie || 
|-
| 2006 || Young Critics Circle, Philippines || Best Actor || Coco Martin
|- Brisbane Filmfest, Australia || Interfaith Award || 
|-
| 2005 || 58th Locarno International Film Festival, Switzerland || Golden Leopard award  || Video competition
|}

== External links ==
*  

 

 
 
 
 
 
 
 
 

 
 