Duello nel Texas
{{Infobox film
| name           = Duello nel Texas
| image          = Gringoposter.jpg
| caption        = Spanish film poster
| director       = Ricardo Blasco Mario Caiano 
| producer       = Albert Band José Gutiérrez Maesso
| writer         = Albert Band Ricardo Blasco James Donald Prindle (story) Richard Harrison
| music          = Ennio Morricone
| cinematography = Massimo Dallamano
| editing        = Rosa G. Salgado 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}} 1963 Italian/Spanish Richard Harrison.

==Plot==
Outside of Carterville, Texas, the sheepherding Martinez family has discovered gold on their land.  Manuel, the wayward youngest son boasts about the fact under the influence of alcohol in a saloon. The next day three masked men ride to the Martinez house where they murder the father, wound Manuel and steal the gold belonging to the family.  Unaware of the crimes they committed, the three masked riders are seen by Richard Martinez, called "Gringo" who is an adopted Anglo-American son of the Martinez family.  Gringo has been away for four years fighting with guerillas against the Mexican Government.  Discovering the tragedies, Gringo seeks the help of Sheriff Lance Corbett to find the killers who Gringo can only identify by their horses.  Gringo kills one of the men who attempts to ambush him and gradually discovers that certain people in the town of Carterville want to obtain the land of the Martinez family for themselves.

==Cast== Richard Harrison  ...  Richard - Ricardo "Gringo" Martinez  
*Giacomo Rossi-Stuart  ...  Sheriff Lance Corbett 
* Mikaela  ...  Maria Huertas  
*Sara Lezana  ...  Elisa  Lisa Martinez  
* Daniel Martín  ...  Manuel Martinez   
*Barta Barri  ...  Lou Stedman  
*Aldo Sambrell  ...  Juan Guardo  
*Agustín González  ...  Risitas Wilson   
*Barbara Simon  ...  Rosa Cardena  
*Ángel Solano  ...  Miller   
*Gonzalo Esquiroz  ...  King Wilson 
*Rodolfo del Campo  ...  Doctor 
*Guillermo Vera  ...  Mexicano

==Notes==
 

==External links==
* 

 
 
 
 
 
 
 
 
 