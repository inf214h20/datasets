House Broken (1936 film)
{{Infobox film
| name =  House Broken 
| image =
| image_size =
| caption =
| director = Michael Hankinson
| producer = Anthony Havelock-Allan 
| writer =  Vera Allinson   Paul Hervey Fox
| narrator = Jack Lambert Mary Lawson   Enid Stamp-Taylor
| music = 
| cinematography = Francis Carver
| editing =  British and Dominions 
| distributor = Paramount Pictures
| released = June 1936
| runtime = 73 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jack Lambert Mary Lawson. A wife tries to make her husband jealous by flirting with a Frenchman.

The film was made at Elstree Studios as a quota quickie for release by Paramount Pictures. 

==Cast==
* Louis Borel as Charles Delmont  Jack Lambert as Jock Macgregor  Mary Lawson as Angela Macgregor 
* Enid Stamp-Taylor as Peggy Allen

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 


 