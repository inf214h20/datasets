Zombie Night (2003 film)
{{Infobox film
| name           = Zombie Night
| image          = Zombie Night.jpg
| image size     = 200px
| caption        = DVD Cover 
| director       = David J. Francis Amber Lynn Francis David J. Francis Mike Masters
| writer         = Amber Lynn Francis David J. Francis
| starring       = Danny Ticknovich Sandra Segovic
| music          = Kevin Eamon Rich Hamelin Rodney Lee Nelson Dan Turcotte Jeff Vidov
| cinematography =
| editing        = Chris Bellio
| distributor    = Primal Films Inc.
| released       =  
| runtime        = 93 minutes
| country        = Canada
| language       = English
| budget         =
| gross          =
}}
Zombie Night is a 2003 Canadian horror film directed by David J. Francis, written by Francis and Amber Lynn Francis, and starring Danny Ticknovich and Sandra Segovic.

== Plot ==
After World War 3, the dead have risen and are eating the living after the sun goes down. A group of survivors are held up in a building fighting off the undead. One night though, the zombies break through and the group is forced to evacuate. They find themselves running through the woods, trying at all costs to stay alive.

== Cast ==
* Danny Ticknovich as Dave
* Sandra Segovic as Shelley
* Dwayne Moniz as Derek
* Alexiannia Pearson as Becca
* Steve Curtis as Keith
* Andrea Ramolo as Amber Amber Lynn Francis as Lesley
* David J. Francis as Man Peeing

== Reception ==
Mike Watt of Film Threat rated it 3.5/5 stars and wrote, "It was made with such earnest affection to the genre that its difficult to be negative towards it."   Bloody Disgusting rated it 2/5 stars and criticized the acting and dialogue.   Peter Dendle wrote, "Unconvincing actors try to make you think the world is coming to an end by running around abandoned buildings in this anemic offering from Hamilton, Ontario." 

== Sequels ==
A sequel was produced,  . There was also a mock documentary, following the film makers as they attempt to make Reel Zombies in a time of real zombies.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 