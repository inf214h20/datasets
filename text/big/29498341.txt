Irandu Mugam
{{Infobox film
| name = Irandu Mugam
| image = IranduMughamnew.jpg
| caption = Promotional Poster of Irandu Mugam
| director = Aravind Raj
| producer = Vaithyalinga Udayar
| writer =  Karan Suhani Livingston M. S. Bhaskar Ganja Karuppu Anu Hasan Bharadwaj
| cinematography = Abdul Kalam
| editing = R. T. Annadurai
| distributor =
| released =  
| runtime = 
| country = India
| language = Tamil
| budget =
| gross =
}} Karan in the lead roles. The film released to good reviews on September 3, 2010 portraying the poor political system in India. 

==Plot==
Parthasarathy (Karan) is the son of a cook and a political science graduate. He aspires to become a Minister. His mimicry skills get him acquainted with the Chief Minister, who comes to his village for a meeting. He impresses him to become local leader of the party. This incurs him the wrath of Thamizh Sakthi (Nasser), a leader of the same party, who wants to promote his younger brother.

Turn of events lead to Thamizh Sakthi helping Parthasarathy become MLA with a promise that he should be with him and support him in all shady deals. As it happens Parthasarathy now becomes a Minister. Their corrupt ways earn them money.

There is one Pavithra (Suhani) daughter of opposition leader who is in love with Parthasarathy. Meanwhile, enters Sarveswaran (Sathyaraj), an IAS officer, who is committed to clean the political system. He has a bad past. He hatches a conspiracy and succeeds in reforming Parthasarathy. The duo now starts to work for the welfare of the people and also bring to book the greedy and corrupt.

==Cast==
* Sathyaraj as Sarveswaren Karan as Parthasarathy
* Suhani Kalita as Pavithra
* Anu Hasan as Sarveswarans wife
* Nassar as Tamizh Sakthi Livingston as Parthasarathys uncle
* Anuradha Krishnamoorthy as Deivanayaki
* Malavika Avinash as Thilakavathy
* M. S. Bhaskar 
* Ganja Karuppu

==References==
 

==External links==
*  
*   at Oneindia.in

 
 
 
 
 


 
 