Diljale
 
 
{{Infobox film|
| name = Diljale
| image = Diljalefilm.jpg
| caption = Theatrical release poster
| director = Harry Baweja
| producer =
| music = Anu Malik
| distributor =
| starring = Ajay Devgan Sonali Bendre Madhoo Amrish Puri Shakti Kapoor Parmeet Sethi
| country = India
| released = 27 September 1996 
| runtime = 185 minutes
| language = Hindi
| budget =
| gross =   177.5&nbsp;million 
}}

Diljale (The Heart Burns) is an Indian Hindi action romance film released in the year 1996. It stars Ajay Devgan, Sonali Bendre and Madhoo as the lead protagonists. Amrish Puri, Shakti Kapoor and Parmeet Sethi played supporting roles in the film. The film was directed by Harry Baweja. It was a major hit and established Sonali Bendres acting credentials. The sets in the film resembled those of other films like Zulm Ki Hukumat (1992) and Roja (1992). 

==Plot==
The story is based in Jammu and Kashmir|Kashmir. It starts with a sad Radhika (Sonali Bendre) being introduced to an Army Major (Parmeet Sethi) by her father, Raja Saab (Shakti Kapoor) an erstwhile king and current politician. Raja Saab tells her that the Major will be a good husband for her. On the day of the engagement, news breaks of a terrorist attack in a nearby village led by the terrorist, Shaka (Ajay Devgan). The Major and Raja Saab leave with a large force to that village. But that turns out to be a ruse as Shaka comes to the place of the engagement and burns the marriage Mandap. Then he gives a long look to Radhika, who looks at him with hatred, and leaves. Shaka reaches his lair where he meets his leader, Dara (Amrish Puri) and Shabnam (Madhoo) and the rest of his gang. Dara welcomes him and proclaims him to be Diljale. Shabnam, who loves him, tries to hug him but Shaka rebuffs it.

Shaka then goes to visit his mother (Farida Jalal) where the story of Shaka is shown in a flashback. Shaka was originally Shyam, a patriotic college student and son of a local village leader. Radhika studies in the same college and they both fall deeply in love. When Raja Saab tries to usurp all the village land, Shyams father organises all the villages against him. Raja Saab bribes a local police officer (Gulshan Grover) who proclaims Shyams father as a terrorist and kills him in an encounter. When Shyam goes to Raja Saabs home to take revenge, Raja Saab (who knows of their love affair and feels it is below his familys dignity) frames Shyam also as a terrorist and tries to kill him. Shyam escapes but Radhika, who only sees him threatening her father, berates him and proclaims him a terrorist. Brokenhearted Shyam joins Daras group and becomes Shaka.

Back in the present time, the Major surrounds Shaka at his mothers home but finds himself outnumbered by Shakas men. Shaka asks him to go away. The Major then confronts Radhika and asks her if she knows Shaka from earlier. Under pressure from Raja Saab, she denies knowing Shaka.

The army captures four associates of Dara. To get them freed, Dara asks Shaka, Shabnam and some others to hijack a bus travelling from Vaishno Devi. When Shaka captures them and brings them to a ruined temple, he is shocked to find Radhika (who had gone to Vaishno Devi to pray for Shaka) among the hostages. She confronts him and tells him that he never loved her or the homeland, India. Shaka is stung and tells he that he lost everything for her love. She then tells him to release everyone since they are innocent and says that if he loves her, he will do so. He agrees under the condition that she will remain with him. When Shabnam and the others tried to stop this release, Shaka disarms them and releases the hostages and runs away with Radhika.

Dara is livid with the betrayal and wants to kill Shaka. Raja Saab meets him and tells him to kill Shaka and release Radhika. Dara agrees but in return wants his four associates released and for his entire group to be safely escorted by Raja Saab to Pakistan.

Meanwhile the corrupt police officer, who killed Shyams father, tries to kill Shaka who disarms him. To save himself, the police officer tells the truth to Radhika who is shocked to see the depth of love which Shaka has for her. Shaka kills the police officer but is captured by Daras men. Dara puts both of them under lock and key but Shabnam releases Shaka. Shaka confronts Dara and asks him not to sell his homeland to politicians like Raja Saab but Dara orders his men to kill Shaka. The Major and his army attack at that very moment. Raja Saab, Dara and his men escape with Radhika in their custody with Shaka and Shabnam in close pursuit. The Major finds Raja Saabs son hiding in a corner and finds out the truth about Shyam and Raja Saab.

Dara speaks to his Pakistani Intelligence controller that they need help crossing the border now. Since some Americans are visiting the border and Pakistan doesnt want to be caught supporting terrorists, the Pakistanis decide to mine the border crossing and kill Dara and his men.

Dara, Raja Saab and others reach the last border outpost. Raja Saab goes to talk to the captain of the post to facilitate the escape of Dara and his men. But Shaka has already reached there and is waiting for him in the captains office. He kills Raja Saab but Dara and his men escape. When a couple of Daras men try to kill Radhika, Shabnam kills them and rescues her. Shabnam brings Radhika to Shaka but they are all confronted by the Major. He tells Shaka that he knows the truth and is sorry but will still have to arrest him. Shaka agrees but wants to stop Dara and is men from crossing the border since he believes that they still have good in their heart. The Major agrees and they go in a snowmobile towards the border and reach there before Dara and his men. They see Pakistani soldiers laying mines but are captured by them.

The Pakistani Major tells them they want to kill Dara and his men since they can always create more terrorists. Shaka and the Major kill them all and go towards the border. When Shaka goes to stop Dara and his men, the Major stops him saying that he wants to kill terrorists. Shaka says that he wants to kill terrorism. He shouts at Dara to stop but Dara thinks it is a trick. So Shaka jumps at one of the mines and is blasted. Shocked, Dara asks his men to stop.

Shaka wakes up in a hospital to find his mother, Radhika and the Major next to him. The Major takes him outside where Dara, Shabnam and everyone is waiting. Dara hugs him and apologises and admits that love can defeat anything even terrorism. He says that they have all surrendered. Shabnam too bids him goodbye. Shyam and Radhika hug each other in the backdrop of the Tiranga.

==Cast==
* Ajay Devgan.... as Shyam/Shaka/Diljale
* Sonali Bendre.... as Radhika
* Madhoo.... as Shabnam
* Shakti Kapoor.... as Raja Saab
* Parmeet Sethi.... as Captain Ranveer
* Amrish Puri.... as Dara
* Gulshan Grover.... as Inspector Yajwendra
* Farida Jalal.... as Shyams Mother
* Akash Khurana.... as Shyams Father
* Rakesh Bedi.... as Raja Saabs Son
* Himani Shivpuri.... as Raja Saabs Sister

== Music ==
Music of the film created waves among the youths. The film has 8 songs. The music composer is Anu Malik. 
{| class="wikitable"
|-
! Song
! Singer
|-
| "Ho Nahin Sakta"
| Udit Narayan
|-
| "Kuch Tum Beheko"
| Udit Narayan, Alka Yagnik
|-
| "Mera Mulk, Mera Desh"
| Kumar Sanu, Aditya Narayan
|-
| "Ek Baat Main Apne Dil"
| Kumar Sanu, Alka Yagnik
|-
| "Shaam Hain Dhuan Dhuan"
| Sushma Shrestha|Poornima, Ajay Devgan
|-
| "Mera Mulk Mera Desh (Sad)" 
| Alka Yagnik
|-
| "Boom Boom"
|  Nisha, Shobhana
|-
| "Jiske Aane Se"
| Kumar Sanu
|-
|}

==Reception==
Diljale was a landmark in Madhoos career.  The film received positive critical reception. Avinash Ramchandani of Planet Bollywood wrote, "Basically, this movie runs on the excellent direction of Harry Baweja (who directed another excellent movie of Ajay Devgan, Dilwale) and the excellent acting by Ajay Devgan."    Nisha Mehta of The Times of India said, "Not only do I love the heart-warming song Mera Mulk Mera Desh from Diljale but also I like the essence of the film. Ajay Devgn was too good in it." 

The film earned a lifetime gross of   17.75 cr, with a distributor share of   75&nbsp;million. The film was declared a "Semi-Hit" by  |date=21 March 2013|accessdate=14 December 2014|archiveurl=http://web.archive.org/web/20141214071859/http://ibnlive.in.com/news/ajay-devgn-meaty-roles-are-for-men-not-
for-kids/380308-8-66.html|archivedate=14 December 2014|deadurl=no}} 

Juhi Chawla  was first choice of Radhikas role.
Sunil Shetty and Aditya Pancholi were offered Parmeet Sethis role but both declined due to date issues Then Milind Gunaji was selected but opted out later as he do not want to shave his beard.

==References==
 

==External links==
*  
 
 
 