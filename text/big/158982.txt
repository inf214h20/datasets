You've Got Mail
 
{{Infobox film
| name           = Youve Got Mail
| image          = Youve_Got_Mail.jpg
| caption        = Theatrical release poster
| director       = Nora Ephron
| producer       = Nora Ephron Lauren Shuler Donner
| screenplay     = Nora Ephron Delia Ephron
| based on       =  
| starring       = Tom Hanks Meg Ryan Parker Posey Jean Stapleton Dave Chappelle Steve Zahn Greg Kinnear
| music          = George Fenton John Lindley
| editing        = Richard Marks
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $65 million
| gross          = $250.8 million   
}}
 romantic comedy-drama correspondence courtship who are unaware that they are also business rivals. An adaptation of Parfumerie was previously made as The Shop Around the Corner, a 1940 film by Ernst Lubitsch and also a 1949 musical remake, In the Good Old Summertime by  Robert Z. Leonard starring Judy Garland. Youve Got Mail updates that concept with the use of e-mail. Influences from Jane Austens Pride and Prejudice can also be seen in the relationship between Joe Fox and Kathleen Kelly — a reference pointed out by these characters actually discussing Mr. Darcy and Miss Bennet in the film. Ephron stated that Youve Got Mail was as much about the Upper West Side itself as the characters, highlighting the "small town community" feel that pervades the Upper West Side. 
 AOL users hear when they receive new e-mail .

The film received significant media coverage  leading up to its release in anticipation of the romantic coupling of Tom Hanks and Meg Ryan, who had both appeared together previously in Joe Versus the Volcano (1990) and Sleepless in Seattle (1993).

==Plot== screen name Borders or Barnes & Noble. Kathleen, on the other hand, runs the independent bookstore The Shop Around The Corner that her mother ran before her. The two then pass each other on their respective ways to work, where it is revealed that they frequent the same neighborhoods in upper west Manhattan. Joe arrives at work, overseeing the opening of a new Fox Books in New York with the help of his friend, branch manager Kevin (Dave Chappelle). Meanwhile, Kathleen and her three store assistants, George (Steve Zahn), Birdie (Jean Stapleton), and Christina (Heather Burns) open up shop for the day.

Following a day on the town with his eleven-year-old aunt Annabel (Hallee Hirsh) and four-year-old half brother Matthew (Jeffrey Scaperrotta) (the children of his frequently divorced grandfather and father, respectively), Joe enters Kathleens store to let his younger relatives experience storytime. Joe and Kathleen have a friendly conversation that reveals Kathleens fears about the Fox Books store opening around the corner, shocking Joe. He introduces himself as "Joe. Just call me Joe," omitting his last name, and makes an abrupt exit with the children. However, at a publishing party later in the week, Joe and Kathleen meet again, both of them being in the New York book business, where Kathleen discovers Joes true identity.

Following suggestions from Frank/Joe via "NY152" Kathleen begins a media war, including both a boycott of Fox Books and an interview on the local news. All the while, "NY152" and "Shopgirl" continue their courtship, to the point where "NY152" asks "Shopgirl" to meet. Too embarrassed to go alone, Joe brings Kevin along for moral support. He insists that "Shopgirl" may be the love of his life. Meanwhile Kevin, looking in a cafe window at the behest of Joe, discovers the true identity of "Shopgirl." When Joe discovers that it is actually Kathleen behind the name, he confronts her as Joe (concealing his "NY152" alter ego – and feelings). The two exchange some bitter words and Joe leaves the cafe hurt, leaving Kathleen initially remorseful.  Kathleen later returns home puzzled why NY152 might have stood her up.

Despite all efforts, The Shop Around the Corner slowly goes under. In a somber moment Kathleen enters Fox Books to discover the true nature of the store is one of friendliness and relaxation, yet without the same dedication to childrens books as her independent shop. Eventually, her employees move on to other jobs; as Christina goes job hunting, George gets a job at the childrens department at a Fox Books store (Joe later compares Georges knowledge of the contents of the department to a PhD) and Birdie, who is already wealthy from investments, retires.
 Jane Adams), a talk show host who interviewed him.  This is predated by one week by Joe and his uptight girlfriend, Patricia (Parker Posey), who broke up in their apartment building while stuck in the elevator. Kathleen and Joe develop a tentative friendship that blossoms over the course of a few weeks and they begin to spend more time with one another.
 Riverside Park. Kathleen admits that she had wanted "NY152" to be Joe so badly, and the two kiss.

==Cast==
* Tom Hanks as Joe "NY152" Fox
* Meg Ryan as Kathleen "Shopgirl" Kelly
* Parker Posey as Patricia Eden
* Jean Stapleton as Birdie Conrad
* Greg Kinnear as Frank Navasky
* Steve Zahn as George Pappas
* Heather Burns as Christina Plutzker
* Dave Chappelle as Kevin Jackson
* Dabney Coleman as Nelson Fox John Randolph as Schuyler Fox
* Deborah Rush as Veronica Grant
* Hallee Hirsh as Annabel Fox
* Jeffrey Scaperrotta as Matthew Fox
* Cara Seymour as Gillian Quinn
* Peter Mian as "The Capeman"
* Sara Ramirez as Rose, Zabars cashier Jane Adams as Sydney Ann, TV talk show host (Uncredited)
* Michael Badalucco as Charlie
* Veanne Cox as Miranda Margulies
* Reiko Aylesworth as Thanksgiving guest

==Reception==

===Critical reaction===
 
Youve Got Mail received generally positive reviews from critics. Rotten Tomatoes gives the film a "Certified Fresh" rating of 69% based on 85 reviews, with a critical consensus of, "Great chemistry between the leads made this a warm and charming delight." Metacritic gives a weighted average score of 57 out of 100, based on 19 critics, indicating "mixed or average" reviews. 

The film was relatively well reviewed by New York Times critic, Janet Maslin and is a New York Times Critics Pick.  Movie critic Roger Ebert rated the movie three out of four stars. 

===Box office===
 
The film was a financial success, grossing more than three times its $65m budget.  It grossed $115,821,495 from the domestic market and $135,000,000 from foreign markets for a worldwide total of $250,821,495. 

==Soundtrack==
 
  soundtrack was released on December 1, 1998, and featured a mixture of classics from the 1960s and 1970s, particularly the work of Harry Nilsson, as well as new original recordings and covers.

===Track listing===
# Harry Nilsson - "The Puppy Song" - 2:43
# The Cranberries - "Dreams (The Cranberries song)|Dreams" - 4:31 Splish Splash" - 2:12
# Louis Armstrong - "Dummy Song" - 2:19
# Harry Nilsson - "Remember" - 4:02
# Roy Orbison - "Dream (song)|Dream" - 2:12
# Bobby Day - "Rockin Robin (song)|Rockin Robin" - 2:36 Lonely at the Top" - 2:32
# Stevie Wonder - "Signed, Sealed, Delivered Im Yours" - 2:38
# Harry Nilsson - "I Guess the Lord Must Be in New York City" - 3:08
# Harry Nilsson - "Over the Rainbow" - 3:31
# Carole King - "Anyone At All" - 3:09 Billy Williams - "Im Gonna Sit Right Down and Write Myself a Letter" - 2:08
# George Fenton - "The Youve Got Mail Suite" - 5:36 You Made Me Love You" - 3:04

==References==
 

==External links==
 
*  
*  
*  
*  
*  
* The Onion  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 