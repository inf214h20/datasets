Mutiny on the Bounty (1935 film)
 
{{Infobox film
| name           = Mutiny on the Bounty
| image          = Poster - Mutiny on the Bounty (1935).jpg
| caption        = Advertorial poster
| director       = Frank Lloyd
| producer       = Frank Lloyd Irving Thalberg Carey Wilson Movita Mamo Mamo
| music          = Score:   Bronisław Kaper (both uncredited)
| cinematography = Arthur Edeson
| editing        = Margaret Booth
| distributor    = Metro-Goldwyn-Mayer Capitol Theatre, the site of many prestigious MGM film premieres. 
| runtime        = 132 minutes
| country        = United States
| language       = English
| budget         = $1,950,000  . 
| gross          = $4,460,000    (rentals) 
}} Mutiny on the Bounty.

The film was one of the biggest hits of its time.  Although its historical accuracy has been questioned (inevitable as it is based on a novel about the facts, not the facts themselves), film critics consider this adaptation to be the best cinematic work inspired by the mutiny.

==Plot==
The HMS HMS Bounty|Bounty leaves England in 1787 on a two-year voyage over the Pacific Ocean. The ships captain, William Bligh (Charles Laughton), is a brutal tyrant who routinely administers harsh punishment to officers and crew alike who either lack discipline, cause any infraction on board the ship, or defy his authority. Fletcher Christian (Clark Gable), the ships lieutenant, is a formidable yet compassionate man who disapproves of Blighs treatment of the crew. Roger Byam (Franchot Tone) is an idealistic midshipman who is divided between his loyalty to Bligh, owing to his familys naval tradition, and his friendship with Christian.

During the voyage, the enmity between Christian and Bligh grows after Christian openly challenges Blighs unjust practices aboard the ship. When the ship arrives at the island of Tahiti, where the crew acquire breadfruit plants to take home, Bligh punishes Christian by refusing to let him leave the ship during their stay. Byam, meanwhile, sets up residency on the island, living with the island Chief, Hitihiti (William Bambridge), and his daughter, Tehanni (Movita Castaneda), and translating an English dictionary of the Tahitian language. Hitihiti persuades Bligh to allow Christian a day pass on the island. Bligh agrees but quickly repeals the pass out of spite. Christian disregards the order and spends his one day off the ship romancing a local Tahitian girl named Miamiti (Mamo Clark). Christian promises her he will be back someday.
 Dudley Digges), and Bligh cuts water rationing to the crew in favor of providing water for the acquired breadfruit plants. Christian, although initially opposing the idea, decides he can no longer tolerate Blighs brutality when he witnesses crew members shackled in iron chains, and he approves the mutiny. The crew raid the weapons cabinet and seize the ship. Bligh and his loyalists are cast into a boat and set adrift at sea with a map and rations to ensure their survival. Due to Blighs steady leadership, they are able to find their way back to land.

Meanwhile, Christian orders that the HMS Bounty return to Tahiti. Byam, who was in his cabin during the mutiny, disapproves of what Christian has done and decides the two can no longer be friends. Months later, Byam is married to Tehanni and Christian has married Miamiti and has a child with her, while the rest of the crew are enjoying their freedom on the island. After a long estrangement, Byam and Christian reconcile their friendship. However, when the British ship HMS Pandora is spotted approaching, Byam and Christian decide they must part ways. Byam and several crew members remain on the island for the ship to take them back to England while Christian leads the remaining crew, his wife, and several Tahitian men and women back on board the Bounty in search of a new island whereon to seek refuge.

Byam boards the Pandora and, much to his surprise, discovers that Bligh is the captain. Bligh, who suspects that Byam was complicit in the mutiny, has him imprisoned for the remainder of the journey across the sea. Back in England, Byam is court-martialed and found guilty of mutiny. Before the court condemns him, Byam speaks of Blighs cruel, dehumanising conduct aboard the Bounty. Due to the intervention of his friend Sir Joseph Banks (Henry Stephenson) and Lord Hood (David Torrence), Byam is pardoned by King George&nbsp;III and allowed to resume his naval career at sea.

Meanwhile, Christian has found Pitcairn, an uninhabited yet sustainable island that he believes will provide adequate refuge from the reach of the Royal Navy. The Bounty crashes on the rocks and Christian orders it to be burned.

==Historical inaccuracies==
  as Captain Bligh set adrift by Fletcher Christian (Clark Gable)]] Captain William Bligh was HMS Pandora, nor was he present at the trial of the mutineers who stayed on Tahiti. At the time he was halfway around the world on a second voyage for breadfruit plants. Fletcher Christians father had died many years before Christians travels on board the Bounty, whereas the film shows the elder Christian at the trial. It should be noted, though, that the movie was always presented as an adaptation of the Nordhoff and Hall trilogy, which already differed from the actual story of the mutiny.

Bligh is depicted as a brutal, sadistic disciplinarian. Particular episodes include a keelhauling and flogging a dead man. Neither of these happened. Keelhauling was used rarely, if at all, and had been abandoned long before Blighs time. Indeed, the meticulous record of the Bountys log reveals that the flogging rate was lower than the average for that time. Prior to the mutiny, the Bounty had only two deaths&mdash;one seaman died of scurvy (not keelhauling), and the ships surgeon died apparently of drink and indolence and not as a result of abuse by Bligh. Likewise, the film shows the mutineers taking over the ship only after killing several loyal crewmen, when in fact none died (although one crewman came very close to shooting Bligh until stopped by Christian). Lastly, Christian is shown being inspired to take over the ship after several crewmen have unjustly been put into irons by Bligh; this is fictional license.

For historical accuracy, Clark Gable reluctantly had to shave off his famous moustache because the sailors in the Royal Navy in the 18th century had to be clean-shaven.

In the final scene of the film, Gable gives a rousing speech to his fellow mutineers, speaking of creating a perfect society of free men on Pitcairn Islands|Pitcairn, away from Bligh and the navy. The reality was very different.

Midshipman Roger Byam was based on a real person, Midshipman Peter Heywood, who is not listed in the novel or motion picture. Just as the fictional Byam is pardoned at the end of the film, the real-life Peter Heywood was pardoned for his part in the mutiny. MGM trailers in 1935 made an error calling Midshipman Byam an ensign.
 Thomas Ellison is depicted as being allowed to see his wife before his execution. There is no record to indicate that the real Ellison was married, and in any case a consolation visit of this type never would have been permitted in real life.

==Cast== Captain Bligh
* Clark Gable as Fletcher Christian
* Franchot Tone as Byam Smith
* Ellison
* Dudley Digges as Bacchus
* Donald Crisp as Burkitt
* Henry Stephenson as Sir Joseph Banks
* Francis Lister as Captain Nelson
* Spring Byington as Mrs. Byam
* Movita Castaneda as Tehani (as Movita)
* Mamo Clark as Maimiti (as Mamo) Byron Russell Quintal
* Lord Hood Douglas Walton as Stewart
* Ian Wolfe as Maggs Fryer
* Ivan F. Simpson as Morgan (as Ivan Simpson) Hayward
* Hitihiti (as William Bambridge)
* Marion Clayton as Mary Ellison (as Marion Clayton) Stanley Fields Muspratt
* Morrison
* Lieutenant Edwards (as Craufurd Kent) Pat Flaherty as Churchill McCoy
* Hal LeSueur as Millard

==Production==

===Filming locations===
  as Fletcher Christian]]
*French Polynesia
*Metro-Goldwyn-Mayer Studios - 10202 W. Washington Blvd., Culver City, California, USA (studio)
*Monterey Bay, Monterey, California, USA
*Monterey Harbor, Monterey, California, USA
*Sailing Ship Restaurant, Pier 42, The Embarcadero, San Francisco, California, USA (ship "Ellen" as "The Bounty")
*San Miguel Island, California, USA
*Santa Barbara Channel, Channel Islands, California, USA Santa Catalina Island, Channel Islands, California, USA
*South Beach Harbor, South Beach, San Francisco, California, USA (ship "Ellen" as "The Bounty") South Pacific, Pacific Ocean
*Tahiti, French Polynesia

Hollywood star James Cagney (then on a hiatus from Warner Bros. during a contract dispute) and future stars David Niven and Dick Haymes were uncredited extras in the movie. Cagney is clearly visible toward the beginning of the film.

Charles Laughton, who had a severe self-image complex concerning his weight and unattractive looks, suffered horribly in comparing himself to the handsome, masculine Clark Gable.    Laughton would constantly watch his own walk, gestures, and face, making sure not to let his complex be projected. 

===Ship===
A British merchant navy officer  recalled in his memoirs seeing the fore and aft-rigged schooner Commodore II being broken up in Cape Town in 1945, having suffered severe gale damage, and that this was the ship that had been re-rigged for the film.

==Box Office==
According to MGM records the film earned $2,250,000 in the US and Canada and $2,210,000 elsewhere resulting in a profit of $909,000. 

==Awards and honors==

===Academy Awards=== Grand Hotel). Best Actor nominations.

{| class="wikitable" border="1"
|-
! Award
! Nominee
! Result
|- Best Picture
| Metro-Goldwyn-Mayer   (Irving Thalberg and Albert Lewin producers)
|  
|- Best Director
| Frank Lloyd The Informer
|- Best Actor
| Clark Gable The Informer
|-
| Charles Laughton
|-
| Franchot Tone
|- Best Writing, Screenplay Carey Wilson The Informer
|- Best Music, Scoring
| Nat W. Finston and Herbert Stothart   ("Love Song of Tahiti" written by Walter Jurmann, uncredited) The Informer
|- Best Film Editing
| Margaret Booth A Midsummer Nights Dream
|-
|}

===Other honors===
American Film Institute recognition
*AFIs 100 Years... 100 Movies #86
*AFIs 100 Years... 100 Heroes and Villains:
**Captain Bligh, Villain #19

==Gallery==
 
Image:Charles_Laughton_in_Mutiny_on_the_Bounty_trailer.jpg
Image:Clark_gable_mutiny_bounty_9.jpg
Image:Franchot_tone_mutiny_bounty_2.jpg
Image:Movita_franchot_tone_mutiny_bounty_1.jpg
Image:Mutiny bounty 3.jpg
Image:Mutiny_bounty_5.jpg
Image:Mutiny bounty 8.jpg
Image:Mutiny bounty 10.jpg
Image:Mutiny bounty 11.jpg
Image:Mutiny bounty 15.jpg
Image:Mutiny bounty 21.jpg
Image:Mamo_clark_gable_mutiny_bounty.jpg
Image:Clark_gable_franchot_tone_mutiny_1.jpg
 

==Proposed Sequels==
In 1940 Frank Lloyd was reported as wanting to make a film about the life of Captain Bligh starring Spencer Tracy or Charles Laughton at Universal. It was never made TWO STUDIOS PLAN A BOUNTY MUTINY: M-G-M and Paramount Race to Complete Productions of Films on Sea Adventure
Special to The New York Times.. New York Times (1923-Current file)   02 Nov 1959: 39.  

In 1945 it was reported MGM would make a sequel with Gable, Christian of the Bounty. It would be based on a novel by Charles Nordhoff about Christians romantic adventures in England and South America following the colonisation of Pitcairn Island and would be produced by Carey Wilson. NEWS OF THE SCREEN: M-G-M to Make Christian of Bounty, With Gable-- Southerner Premiere Again Postponed Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   14 Aug 1945: 17.   It was never made.

== Other film versions ==

A 1962 three-hours-plus widescreen Technicolor Mutiny on the Bounty (1962 film)|remake, starring Marlon Brando as Fletcher Christian and Trevor Howard as Captain Bligh, was a disaster both critically and financially at the time, but has come to be reevaluated by critics. 

In 1984, Mel Gibson played Christian opposite Anthony Hopkins as Bligh in a film (based not upon the Nordhoff-Hall novels but on an historical work by Richard Hough) called The Bounty. This latest version, which gives a far more sympathetic view of Bligh, is considered to be the closest to historical events.

The 1935 version was itself not the first film account of the mutiny. In 1933, an Australian film entitled In the Wake of the Bounty, with the then-unknown Errol Flynn as Fletcher Christian, was released, but was not successful and received few bookings outside Australia. There was also an even earlier film, the 1916  Australian–New Zealand film, The Mutiny on the Bounty directed by Raymond Longford

==Parodies==
*Friz Frelengs cartoon Mutiny on the Bunny casts Yosemite Sam (called Shanghai Sam) as a foul-tempered skipper who shanghais Bugs Bunny, only to see Bugs rebel. Also, in one scene in Frelengs earlier Buccaneer Bunny, Bugs dresses up as Capt. Bligh (including a visual and vocal impression of Charles Laughton) and barks out orders to Sam (called Seagoin Sam). Mutiny in Space" features Ronald Long imitating Charles Laughton in the role of spaceship captain "Admiral Zahrk." 
*Morecambe and Wise performed a sketch with Arthur Lowe (famous for playing Captain Mainwaring in Dads Army) as Captain Bligh. At the end of the sketch it is announced Bligh has some loyal crewmen who turn out to be other stars of Dads Army. 
*"Holidays at Home", a 1978 episode of It Aint Half Hot Mum, includes a dream sequence where the sitcoms cast enact scenes from the film.
*In The Simpsons episode "The Wettest Stories Ever Told" features the family telling stories set on ships. The second segment is a parody on Mutiny on the Bounty and casts Principal Skinner as Capt. Bligh, brutalizing the crew members (played by Bart, Milhouse, Martin, Nelson, Jimbo, Dolph and Kearney).

==References==
 

== External links ==
 
 
*  
*  
*  
*  
* 
*   at Virtual History

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 