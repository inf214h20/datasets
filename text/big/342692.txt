Charlie's Angels (film)
 
{{Infobox film
| name           = Charlies Angels
| image          = Charlies Angels (2000) Poster.jpg
| caption        = Theatrical release poster
| director       = McG
| producer       = Leonard Goldberg Drew Barrymore Nancy Juvonen
| writer         = Ryan Rowe Ed Solomon John August
| based on       =  
| narrator       = John Forsythe
| starring       = Cameron Diaz Drew Barrymore Lucy Liu Bill Murray Sam Rockwell Kelly Lynch Crispin Glover Tim Curry Edward Shearmur
| cinematography = Russell Carpenter
| editing        = Wayne Wahrman Peter Teschner
| studio         = Flower Films Tall Trees
| distributor    = Columbia Pictures
| released       = November 3, 2000
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $93 million
| gross          = $264.1 million
}}
 private investigation television series of the same name from the late 1970s, which was adapted by screenwriters Ryan Rowe, Ed Solomon, and John August. Unlike the TV series which had dramatic elements, the film featured more comedic elements than seen in the series.

Co-produced by Tall Trees Productions and Flower Films, Charlies Angels was distributed by Columbia Pictures, and co-starred Bill Murray as Bosley, with John Forsythe reprising his role from the original TV series as the unseen Charlies voice. Making cameo appearances are Tom Green, who dated Drew Barrymore at the time of the making of this film, and L.L. Cool J.

The film was followed with the 2003  .

==Plot== Bosley (Bill Murray) works with them directly when needed.

The Angels are assigned to find Eric Knox (Sam Rockwell), a software genius who created a revolutionary voice-recognition system and heads his own company, Knox Enterprises. Knox is believed to have been kidnapped by Roger Corwin (Tim Curry), who runs a communications-satellite company called Redstar. The Angels infiltrate a party held by Corwin and spot the Thin Man (Crispin Glover) who was seen on the surveillance videos during Knoxs kidnapping. They chase and fight the Thin Man, but he runs away.  
When they follow him, they discover Knox.

After the Angels reunite Knox with his business partner Vivian Wood (Kelly Lynch), Charlie explains that they must determine whether the Thin Man has stolen Knoxs voice-recognition software. The Angels infiltrate Redstar headquarters, fool the security system, and plant a device in the central computer that will enable them to explore it remotely. They retire for the night after giving Bosley the laptop computer that communicates with the Redstar computer.  Dylan takes up Knoxs offer to spend the night with him, but he betrays her later that night, explaining that he faked the kidnapping with help from Vivian and the Thin Man.  He has kidnapped Bosley, and, with access to Redstars central computer, he intends to use his voice software with the Redstar satellite network to find and kill Charlie, who he believes killed his father in the Vietnam War.

Knox shoots at Dylan, apparently killing her, but she escapes unharmed.  Natalie and Alex are also attacked, and Corwin is murdered by the Thin Man. When the Angels regroup, Charlies offices are blown up, but a radio receiver survives in the rubble, and the Angels deduce Bosleys location as he speaks to them using a radio transmitter implanted in his teeth.

With help from Dylans current boyfriend Chad (Tom Green), the Angels approach the abandoned lighthouse where Knox is holding Bosley prisoner.  The Angels rescue Bosley and defeat Vivian, the Thin Man, and some henchmen before Knox blows up the lighthouse, but Knox uses his software and the Redstar satellite network to locate Charlie when he telephones Bosley.  When Knox escapes in a helicopter armed with a missile, Bosley helps the Angels board the helicopter, and Alex reprograms the missile, which blows up the helicopter and kills Knox while the Angels land safely in the ocean.  Seeing the opportunity to finally meet Charlie in person, the Angels enter the beach house that Knox targeted, but Charlie has already left.  He remotely congratulates them on a job well done, and treats them and Bosley to a vacation. Charlie tells them that Knoxs father was undercover; however, he was discovered and he was killed by someone else not Charlie. When Charlie speaks to the Angels by telephone on the beach, unseen by most of the group, Dylan suspects that she sees him nearby talking into a cell phone.

==Cast==
 
* Cameron Diaz as Natalie Cook
* Drew Barrymore as Dylan Sanders
* Lucy Liu as Alex Munday John Bosley
* Sam Rockwell as Eric Knox
* Kelly Lynch as Vivian Wood
* Crispin Glover as the Thin Man
* Tim Curry as Roger Corwin
* Matt LeBlanc as Jason Gibbons
* LL Cool J as Mr. Jones
* Tom Green as Chad
* Luke Wilson as Peter Kominsky
* Sean Whalen as Pasqual
* Steven Ito as Knox Thug 
* Alex Trebek as Himself
* Karen McDougal as Roger Corwins girlfriend
* John Forsythe as Charles "Charlie" Townsend (Voice)
* Melissa McCarthy as Doris

==Soundtrack==
Released October 24, 2000.
 

  Independent Women Part I" by Destinys Child The Tavares
# "You Make Me Feel Like Dancing" by Leo Sayer
# "True (Spandau Ballet song)|True" by Spandau Ballet
# "Dot" by Destinys Child
# "Baby Got Back" by Sir Mix-A-Lot
# "Angels Eye" by Aerosmith Heart
# "Turning Japanese" by The Vapors Looking Glass
# "Got to Give It Up (Part 1)" by Marvin Gaye Ya Mama" by Fatboy Slim
# "Groove Is in the Heart" by Deee-Lite
# "Charlies Angels 2000" by Apollo 440 Caviar

 

;Other songs used in the film
* "Blind (Korn song)|Blind" by Korn Live Wire" by Mötley Crüe
* "Wake Me Up Before You Go Go" by Wham!
* "Money (Thats What I Want)" by The Flying Lizards Joan Jett and the Blackhearts
* "Angel of the Morning" by Juice Newton Undercover Angel" by Alan ODay Enigma
* Twiggy Twiggy" by Pizzicato Five
* "Sukiyaki (song)|Sukiyaki" by Kyu Sakamoto
* "Zendeko Hachijo" by Zenshuji Zendeko
* "Smack My Bitch Up" by The Prodigy
* "Another Town" by Transister
* "Belly" by Nomad
* "When Angels Yodel" written and arranged by Frank Marocco
* "The Humpty Dance" by Digital Underground 
* "Miami Vice Theme" by Jan Hammer
* "Simon Says" by Pharoahe Monch
* "Leave You Far Behind" by Lunatic Calm
* "Skullsplitter" by Hednoize Blur
* "Billie Jean" by Michael Jackson
* "Angel (Jimi Hendrix song)|Angel" by Rod Stewart
* "All the Small Things" by Blink-182

 

==Reception==
Charlies Angels received generally favorable reviews from critics. On Rotten Tomatoes, the film has a 67% "Fresh" rating based on 141 reviews, despite the lower 45% audience rating. At Metacritic, which assigns a normalized rating out of 100 to reviews from film critics, it has a rating score of 52, indicating "mixed or average reviews".

During the making of Blade II, Guillermo del Toro commented that while films like Charlies Angels had helped to popularize the wire fu style of fighting choreography in Western films, they also served as a "nail in the coffin" and prompted many filmmakers to want to get back to more "hard-hitting" action. "Production Workshop" documentary. Blade II DVD. Roadshow Entertainment, 2002.  "The moment you see Cameron Diaz flying in the air, and you know that she is incapable of flying in the air and kicking five guys... you realize that it is done using wires.   I mean, Charlies Angels was great, but it  was almost satirical." 

==Home media==
Charlies Angels was released on both VHS and DVD on 27 March 2001.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 