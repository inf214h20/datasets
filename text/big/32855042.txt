The Italian Key
{{Infobox film
| name           = The Italian Key
| caption        = 
| image	=	The Italian Key FilmPoster.jpeg
| director       = Rosa Karo
| producer       = Peter O. Almond Tuomas Kantelinen Seppo Toivonen
| writer         = Rosa Karo
| starring       = Gwendolyn Anslow Joana Cartocci
| music          = Tuomas Kantelinen
| cinematography = Ville Tanttu Gianni Giannelli
| editing        = Pauliina Punkki
| studio         = Harmaa Media Oy
| distributor    = Nordisk Film
| released       =  
| runtime        = 95 minutes
| country        = Finland
| language       = English
| budget         = €1,370,000   
| gross          = 
}}
The Italian Key is a 2011 film directed and written by Rosa Karo. The film follows the story of Cabella, a 19-year-old orphan girl, who travels to Italy after she has inherited an old key from her unrelated Uncle Max.

== Plot ==
Upon receiving a key from her Uncle Max, Cabella travels to Italy where she discovers the key is related to a house named Cabella near a village. While traveling she stops near a waterfall to swim and loses the key, but a mysterious man returns the key. She then travels to the village, finds the house, and uses the key to open it.

The next day she goes to the market where the mysterious man works and learns from his cousin Maria (Joanna Cartocci) that his name is Leo (Leo Vertunni) and he is deaf and mute. Maria and Cabella become friends and Maria introduces her sisters Sophia (Elisa Cartocci) and Giulia (Isadora Cartocci). Later that evening Maria tells Cabella that she has a crush on Lord Jai (Moose Ali Khan), a rich man from India that attended a boarding school. That night, Cabella has a conversation with a spirit named Angelo and has strange dreams about her mother. 

The next morning, Cabella finds a basket with goods such as eggs and apples sent by Leo. Maria then takes Cabella to her sister Ambrosias funeral because she died from a heart attack. That night Angelo visits her and confesses that she must go to the cemetery to learn more information. At the cemetery, she meets Senior Bronzini, who, according to rumors, had a relationship with a nun when he was younger. Cabella decides to leave flowers for Chiara, a woman buried next to Ambrosia who has no flowers.

Lord Jai comes back with a disabled friend. He is desperately looking for his sister, who turns out to be Cabella. She does not this know yet, and she visits Bronzini with Leo in order to learn more about Ambrosia. He tells her that she was pregnant and that right after she learned it she decided to join the monastery and to give the child up. The name of the child was Chiara. It also turns out that all the sisters have a passion for something. One is keen on painting and goes to the forest everyday to paint and the other believes that someone is going to come to her. That is why she waits at the bus station.

Lord Jai asks Maria to come to his party as a guest. Maria tries to convinces Cabella to attend, but while they are trying to find an appropriate dress they discover an old red dress and purse which has a diary inside belonging to Chiara. They read it and discover Chiara went to India where she met a rich man named Max and Alexander, who is pianist, both of whom she loved. She was conflicted on who to love. Maria and Cabella then start a bet. If Chiara chose Alexander, Cabella is obliged to attend the party, but if not, then Maria must confess her love for Lord Jai. It is obvious that Chiara made love with Alexander the night before her departure and became pregnant. Angelo tells Cabella that Chiara is her mother. 

Giulia meets the disabled man and falls for him. Then, Maxs nieces attempt to take the house where Cabella used to live. Lord Jai helps her and tells them to leave. Cabella learns that Lord Jai is her brother and that her mother adopted him and kept him until she died because of high fever. Later at the party he announces to everyone that he found his lost sister. After that Cabella meets Leo and she discovers that he is not deaf and is able to speak. Lord Jai marries Maria, Sophia start a romantic relationship with her friend, and Giulia finds her love. Angelo tells Chiara the news and she reunites with Max. Then Cabella finds Alexander, her father, and the scene ends with her and Leo walking in the fields together.

== Cast ==
* Gwendolyn Anslow as Cabella
* Joana Cartocci as Maria
* Leo Vertunni as Leo
* Moose Ali Khan as Lord Jai
* Mikko Leppilampi as Mr. Fabian
* Elisa Cartocci as Sophia
* Isadora Cartocci as Giulia
* Joeanna Sayler as Chiara
* Peter O. Almond as Bronzini
* John Shea as Alexander
 

== Accolades ==
;Park City Film Music Festival
 
|-
| 2011|| The Italian Key  || Directors Choice/Audience Choice - Gold Medal for Outstanding Achievement in Original Music for a Feature Film for the film ||  
|}

;The Feel Good Film Festival
 
|-
| 2011|| The Italian Key  || Best  Feel Good Feature Film ||  
|}

;Honolulu Film Awards
 
|-
| 2011|| The Italian Key  || Aloha Accolade Award ||  
|}

;Las Vegas Film Festival
 
|-
| 2011|| The Italian Key  || Silver Ace Award ||  
|}

;Prince Edward Island International Film Festival
 
|-
| 2011|| The Italian Key  || Best Foreign Film ||  
|}

;Tulsa International Film Festival
 
|-
| 2011|| The Italian Key  || Best "Women Behind the Camera" Film ||  
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 