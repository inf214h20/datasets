Scapa Flow (film)
{{Infobox film
| name           = Scapa Flow
| image          = 
| image_size     = 
| caption        = 
| director       = Leo Lasko
| producer       = 
| writer         = Leo de Laforgue   Leo Lasko
| narrator       =  Claus Clausen  Carl Balhaus
| music          = 
| editing        = 
| cinematography = Edgar S. Ziesemer
| studio         = Olympia Film 
| distributor    = Olympia Film 
| released       = February 1930
| runtime        =
| country        = Weimar Republic
| language       = German
| budget         = 
| gross          = 
}} German drama Claus Clausen. Wilhelmshaven Mutiny Allies had come to be seen as a popular patriotic act. The inclusion of the Mutiny, however, was more controversial as it highlighted the political divisions which continued to exist. The film was praised by the right wing press, and comparisons were made to the Russian film Battleship Potemkin.  The film was partly inspired by the 1918 play Seeschalt by Reinhardt Goering. 

==Cast==
* Otto Gebühr    Claus Clausen   
* Claire Rommer 
* Erna Morena
* Aribert Mog   
* Arthur Duarte   
* Carl Balhaus   
* Heinz Klockow

==References==
 

==Bibliography==
* Kester, Bernadette. Film Front Weimar: Representations of the First World War in German films of the Weimar Period (1919-1933). Amsterdam University Press, 2003.

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 