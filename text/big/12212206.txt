The Surrogate Woman
{{Infobox film name           = The Surrogate Woman image          = Ssibaji.jpg caption        = Theatrical poster to The Surrogate Woman  director       = Im Kwon-taek producer       = Jeong Do-hwan writer         =  starring       = Kang Soo-yeon Lee Gu-sun Yun Yang-ha music          = Shin Pyong-ha cinematography = Ku Jung-mo editing        = Park Sun-duk distributor    = Shin Han Films released       =   runtime        = 100 minutes country        = South Korea language       = Korean budget         =  gross          = 
| film name = {{Film name hangul         =   hanja          =  rr             = Sibaji mr             = Ssibaji }}
}} 1987 film directed by Im Kwon-taek, dealing with the love affair between a rich aristocrat and a poor servant during the Joseon Dynasty.  

The movie won multiple accolades at the 1987 Asian Film Festival, including Best Film, Best Director, Best Actress, and Best Supporting Actress. Lead actress Kang Soo-yeon was widely acclaimed in her role as the surrogate mother, for which she won Best Actress at the 1987 Venice Film Festival.

==Plot==
Shin, a nobleman, had been trying to conceive a male heir to pass his family name. Unable to provide a male heir, Shins wife gives her husband permission to search for a surrogate wife to bear a male heir.On the way to finding a surrogate wife, Shin runs into a 17 year old girl, Ok-nyo (Kang Soo-yeon). She was a poor feisty girl who stated she would do anything for money. The stubborn nature of Ok-nyo attracts Shin and influences him to choose her to become the surrogate mother for his child. Ok-nyo holds the social status of a servant, but the relationship changes both of them through the course of the movie. Ok-nyo had to obey rules which keep her hidden during the day and delegated her to perform the mating ritual during Shins wifes hours of choosing. No matter what was happening, Ok-nyo could not leave the building in which she was housed. With Shins infatuation and Ok-nyos attachment, both secretly meet for passionate affairs.  Unfortunately both eventually get caught, which causes their separation. Ok-nyos mother tries to dissuade her to break off the relationship by telling her the realities of life. Even though both were punished for the incursion, they still meet until the end. Eventually Ok-nyo conceives the child heir for Shin and is burdened with the mystery of whether Shin will stay by her side or leave with her child forever.

==Remake==
The film inspired 1989 Malayalam film Dasharatham.

==References==
* 
*  
* 
* {{cite web |url=http://wc03.allmovie.com/cg/avg.dll?p=avg&sql=1:176426|title=Sibaji
|accessdate=2007-10-25|last=Fountain|first=Clarke|publisher=Allmovie}}
* 
* 

 

 
 
 
 
 
 
 