Fellers (film)
 
{{Infobox film
| name           = Fellers
| image          = 
| image_size     = 
| caption        = 
| director       = Austin Fay Arthur Higgins
| producer       = 
| writer         = Ashley Druham
| based on      = 
| narrator       = 
| starring       = Arthur Tauchert   Les Coney
| music          = Barney Cuthbert
| cinematography = Tasman Higgins
| editing        = 
| studio         = Artaus Productions
| released       = 23 August 1930
| runtime        = 8,000 feet
| country        = Australia
| language       = English
}}
Fellers is a 1930 Australian comedy about three friends in the Australian Light Horse during the Palestine Campaign of World War I starring Arthur Tauchert, who was the lead in The Sentimental Bloke (1919). The film is mostly silent with a recorded music score as an accompaniment, but the last reel was synchronised with a few minutes of dialogue and a song. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p153 

==Plot==
Three friends serve in the Australian Light Horse during the Palestine Campaign of World War I. One of them enlisted for excitement; another because he thought he killed a man in a fight over a girl; the third because he thought the girl he loved (Jean Duncan) was in love with another. The second man is killed laying a pipe to supply the army with water.

To save the girl back home from heartbreak (her father has recently died), the third man swaps identification tags with the dead man, and has himself reported as dead. He then finds out that the girl loves him. After the war, the complications are resolved and the third man is reunited with the girl 

==Cast==
*Arthur Tauchert as Roughie
*Jean Duncan as the girl
*Arthur Clarke
*Joan Milton as a nurse
*Grace Quine
*Peggy Pryde
*Les Coney
*Arthur Greenaway

==Production==
Scenes set in the Palestinian desert were shot in sandhills at Kensington near Sydney and in Western New South Wales. The film incorporates documentary footage of the real campaign.  The silent sequences were direct by Arthur Higgins and the sound ones by Austin Fay.

==Release==
The film was entered in the ₤10,000 Commonwealth Government film competition. It was awarded third prize of ₤1,500 – there was no second and first prize as the judges felt none of the entries were good enough.  Higgins and Fay argued that their films had been given enough prints under the judging system to warrant the second prize of ₤2,500 and threatened to sue, but the government did not change its mind. 

Contemporary reviews were poor, the critic from the Sydney Morning Herald complaining that the film:
 Leaves much to be desired. The technique of its production Is amateurish; its acting forced and stilted. It has an absurd, trivial story. In its attempts at humour it is stolidly unamusing. The only thing that saves the picture from utter mediocrity is the series of scenes supposed to be taken in the Palestinian deserts, but actually photographed among the sandhills at Kensington... The directors go to endless trouble and expense transporting their apparatus and the members of their company from place to place in order to secure the correct local colour for their stories; yet as regards the plots themselves, and still more as regards the captions, they seem to think that anything will suffice... The plot bristles with absurdities... The change of identification discs in the desert was so puzzling in its consequences that it left the beholders yesterday completely at a loss.    
Commercial results appear to have been disappointing. 

==Preservation status==
This is now considered a lost film. 

==See also==
*List of lost films

==References==
 

==External links==
*  in the Internet Movie Database
*  at Oz Movies

 
 
 