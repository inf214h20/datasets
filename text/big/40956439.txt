See Here, Private Hargrove (film)
{{Infobox film
| name           = See Here, Private Hargrove
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Wesley Ruggles Tay Garnett
| producer       = George Haight
| based on       =  
| screenplay     = Harry Kurnitz
| narrator       =  Robert Walker Donna Reed Keenan Wynn David Snell Lennie Hayton
| cinematography = Charles Lawton Jr.
| editing        = Frank E. Hull
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 101 min.
| country        =   English
| budget         = 
| gross          = 
}}
 1944 comedy Robert Walker, Donna Reed and Keenan Wynn. The story is basically a series of humorous anecdotes about Marion Hargroves tenure at boot camp in Fort Bragg during the early days of World War II. The film was followed by a lesser 1945 sequel, What Next, Corporal Hargrove?, which followed the leading character to France. 

==Plot summary==
 

==Cast== Robert Walker as Pvt. Marion Hargrove
* Donna Reed as Carol Holliday
* Keenan Wynn as Pvt. Mulvehill Grant Mitchell as Uncle George Ray Collins as Brody S. Griffith
* Chill Wills as First Sgt. Cramp
* Bob Crosby as Bob
* Marta Linden as Mrs. Holliday
* George Offerman Jr. as Pvt. Orrin Esty
* Edward Fielding as Gen. Dillon
* Donald Curtis as Sgt. Heldon
* William Bill Phillips as Pvt. Bill Burk (as Wm. Bill Phillips)
* Douglas Fowley as Capt. R.S. Manville

==References==
 	

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 
 