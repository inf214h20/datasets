Yuganthaya (film)
{{Infobox film
 | name = Yuganthaya
 | image =
 | caption = Scene from film
 | director = Lester James Peries
 | producer =
 | writer = A.J. Gunawardena
 | starring = Gamini Fonseka Richard De Zoysa Ramani Bartholomeusz Wickrema Bogoda
 | music = Premasiri Khemadasa
 | cinematography = William Blake 
 | editing = Gladwin Fernando
 | distributor = Tharanga Chithrapata
 | country    = Sri Lanka
 | released =  
 | runtime = 130 minutes
 | language = Sinhala
 | budget = 
}}
Yuganthaya is a 1983 Sri Lankan drama film directed by Lester James Peries; it was adapted from the novel Yuganthaya by Martin Wickramasinghe, and deals with the beginning of labor unions in Sri Lanka. 

For the film, Lester won Sri Lankas Presidential Award for best director.    Critical reception was mixed however, and some critics found the film not up to par with the earlier films. It was entered into the 14th Moscow International Film Festival.   

==Plot==
Simon Kabilana (Gamini Fonseka) is a powerful ruthless capitalist who uses terror to keep his workers under control and yield high production quotas. His son Malin (Richard De Zoysa) is the complete opposite, coming back from England idolizing Marx and Lenin, causing them to clash. Malin eventually gives up his fortunes in his fathers company and works toward a confrontation with his father.

==Cast==
* Gamini Fonseka as Simon Kabilana 
* Suwinitha Weerasinghe as Nalika
* Richard De Zoysa as Malin
* Mahal Wijewardena
* Punya Hiendeniya

==Production==
The film was shot on 35mm and used eastmancolor.   

==References==
 

==External links==
*  of Lester James Peries
* 
*  

 
 
 
 
 


 