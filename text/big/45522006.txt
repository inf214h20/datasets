Lorai: Play to Live
{{Infobox film
| name           = Lorai:Play to Live
| image          = Lorai poster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Parambrata Chatterjee
| producer       = Shyam Sundar Dey
| executive producer = Arijit Dutta
| writer         = Padmanabha Dasgupta
| screenplay     = 
| starring       = Prosenjit Chatterjee Payal Sarkar
| music          = Indradeep Dasgupta
| cinematography = Gopi Bhagat
| editing        = Bodhaditya Bandopadhyay
| studio         = GreenTouch Entertainment
| distributor    = 
| released       =  
| runtime        = 157 minutes
| country        = India Bengali
| budget         = 
| gross          = 
}}
Lorai:Play to Live  is a sports drama film directed by Parambrata Chatterjee,  which stars Prosenjit Chatterjee, Payal Sarkar and Indrasish Roy in lead roles. The fil was released on 9 January 2015.

==Synopsis==
Sebastian Ryan (Prosenjit Chatterjee), an alcoholic ex-football star, is assigned the task of training the youths of a remote violence-torn village in Purulia district. Though he is very sceptical about the task at the outset, circumstances force him to change his perception.

== Cast ==
* Prosenjit Chatterjee  as Sebastian Ryan
* Indrasish Roy as Deep Narayan Chowdhury
* Gargi Roychowdhury
* Payal Sarkar as Anuradha
* Kanchan Mullick
* Kharaj Mukherjee as Mokhar Alam
* Deepankar De
* Biswajit Chakraborty
* Bharat Kaul
* Rajat Ganguly
* Alvito DCunha (special appearance)
* Apurbo Roy
* Parambrata Chatterjee as Naxalite leader Manas (special appearance)

==Release==
Lorai was slated to release with Ebar Shabor, but the producers of this film backed it by a week to avoid competition. 

===Marketing===
Greentouch Entertainment released the films trailer on 2 December 2014 at the presence of the lead cast and the director. The film had a grand premiere at INOX of South City Mall. 

===Controversy===
Lorai have run into trouble with the censor board. The board has asked the makers to get an NOC from the Animal Welfare Board of India for a chicken chase sequence. Story goes that the makers had not taken a prior NOC, which is mandatory for censoring a film. Says a source close to the unit, "In the synopsis of the film, there was no mention of the sequence and the Animal Welfare Board has raised an objection to it. The film, starring Prosenjit Chatterjee, is set for release this weekend; we are hoping things will be sorted out soon." A representative from the production house is already in Chennai to do the needful. Says Parambrata, "The review committee will look into the sequence featuring chickens.Hope things fall into place." 

===Promotion===
The film was promoted on one of the hugely popular bengali television quiz show, Dadagiri Unlimited.

===Critical response===
Reviewer from The Times of India commented Lorai is a good-hearted film that gives us a fair share of drama, humour and, of course, football. And despite our inclination to draw parallels with hits that have similar storylines — like Chak De! India — Lorai still manages to make a good impression.
 
 The Telegraph reviewed "Parambrata’s choice of genre as well as his concept set me thinking not just about the film but about contemporary Bengali cinema and its new breed of young filmmakers. It’s actually a breath of freshness to watch a young filmmaker daring to get away from the glossy plastic world of our city to engulf an earthy India far bigger than our provincial smartphone existence. The charm of the film lies in this rusticity, an essence that’s beautifully and painstakingly shot by cinematographer Gopi Bhagat. Kharaj Mukherjee’s ever optimistic and pleasing Mokhtar is the actor’s most pleasant performance since Patalghar. Just when one stopped expecting anything from Kanchan Mullick, he surprises as the chicken thief. His funny ostrich-like Doa with all its oddities and idiosyncrasies will keep you glued till the end. In fact, Doa’s perfectly crafted introduction and climax are two of the most enjoyable sequences in the film.But the surprise of the film lies in its two young actors — Indrasish Roy as Deepnarayan, the rakishly handsome captain of the football team, is stunning. And debutant Kaushik Kar as the quiet and confident Chipla Mahato is brilliant. Both these fine young actors show a lot of promise. I loved the gentle accordion melody accompanied by a stringed instrument used in a couple of scenes after Sebastian Ryan enters Kushumdi." IMDb gave a rating   to the film.

== Soundtrack ==
Music Composer Indradeep Dasgupta was roped in to score the music for the film. 
{{Infobox album  
| Name       = Lorai:Play to Live
| Type       = Soundtrack
| Director   = Indradeep Dasgupta
| Cover      = Lorai poster.jpg
| Alt        = 
| Released   = 
| Recorded   =  Feature film soundtrack
| Length     = 
| Label      = 
| Producer   = GreenTouch Entertainment
}}
Arijit Singh launched the music of the film was held at a city hotel, with the entire cast and crew in attendance.  The soundtrack was ranked Best Bengali Album of March 2015 by Deccan Music 

=== Track listing ===
{{track listing
| extra_column    = Singer(s)
| total_length    = 
| writing_credits = 
| lyrics_credits  = 
| music_credits   = 
| title1          = Kichhu Kichhu Kotha
| extra1          = Arijit Singh and Kaushiki Chakrabarty
| length1         = 2:58
| title2          = Jonaki
| extra2          = Angaraag Mahanta
| length2         = 3:26
| title3          = Ashchhe Shokal-
| extra3          = Bonnie Chakraborty
| length3         = 2:45
| title4          = Chhoto Ekta Bhor
| extra4          = Mohan Kannan
| length4         = 3:40
| title5          =  Jekhane Shob Ses
| extra5          = Timir Biswas
| length5         = 5:28}}

==References==
 

==External links==
 

 
 