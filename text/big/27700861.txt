The Royal Hunt of the Sun (film)
{{Infobox film
| name           = The Royal Hunt of the Sun
| image          = The Royal Hunt of the Sun.jpg
| caption        = Theatrical Release poster by Howard Rogers
| director       = Irving Lerner
| producer       = Robert Sisk
| screenplay     = Philip Yordan
| based on       =  
| narrator       = Robert Shaw Christopher Plummer
| music          = Marc Wilkinson
| cinematography =
| editing        = Bill Lewthwaite
| studio         = Cinema Center Films Security Pictures CBS (2014,DVD)
| released       =  
| runtime        = 121 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
| gross          =
}} play of Robert Shaw as Francisco Pizarro and Christopher Plummer as the Inca leader Atahualpa. Plummer appeared in stage versions of the play before appearing in the film, which was shot in Latin America and Spain. The film and play are based on the Spanish conquest of Peru by Pizarro in 1530.

==Plot==
With a small rag-tag band of soldiers, Francisco Pizarro enters the Inca Empire and captures its leader, Atahualpa. Pizarro promises to free him in return for a golden ransom, but later finds himself conflicted between his desire to conquer and his friendship for his captive...

==Cast== Robert Shaw as Francisco Pizarro
* Christopher Plummer as Atahualpa
* Nigel Davenport as Hernando de Soto
* Leonard Whiting as Young Martin

==DVD==
The Royal Hunt of the Sun was released to DVD by CBS Home Entertainment on November 25th, 2014 via its CBS MOD DVD-on-demand service.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 