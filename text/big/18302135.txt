Share Bazaar (film)
{{Infobox Film
| name        = Share Bazaar
| starring    = Jackie Shroff  Ravi Kishan  Anupam Kher  Dimple Kapadia  Tinnu Anand C.S. Dubey Jankidas
| director    = Manmohan
| released    = 1997
| country     =   India
| language    = Hindi
|}} 1997 Bollywood film, directed by Manmohan and released in 1997.

==Synopsis==
In Bombays business district, on Dalal Street, stands a multi-storied building called the "Bombay Stock Exchange" or the Share Bazaar. This is where fortunes are made and lost. Two of such traders in shares are the Mehta brothers, Hasmukh and Mansukh. They also manipulate peoples lives, and this time they have chosen to financially ruin Shekar, by getting him arrested on trumped-up charges. And on the other hand, they have singled out a street-smart young man by the Raj, and get him to take Shekars place. Will Raj be the next casualty of the influential Mehta brothers?

==External links==
* 

 
 
 


 