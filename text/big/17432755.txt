China is Near
{{Infobox film
| name = China is Near
| image = China is Near.jpg
| caption =
| director = Marco Bellocchio
| producer = Franco Cristaldi   Oscar Brazzi
| writer = Marco Bellocchio   Elda Tattoli
| starring = Glauco Mauri Elda Tattoli Paolo Graziosi
| music = Ennio Morricone
| cinematography =Tonino Delli Colli
| editing =Roberto Perpignani
| distributor =
| released =  
| runtime = 116 minutes
| country = Italy
| language = Italian
| budget =
| gross =
}}
China is Near ( ) is a 1967 Italian drama film written and directed by Marco Bellocchio. It is a satirical movie about the struggle for political power. It focusses on the conflict between a middle class professor running for office as a socialist and his brother, who is a Maoist.

==Plot==
A pair of working class lovers - a secretary and an accountant, scheme to marry into the rich landed gentry. Their targets are a professor, Vittorio Gordini Malvezzi, (Glauco Mauri), who is running for municipal office as a Socialist candidate, and his sister Elena, (Elda Tattoli), a great lady who lets every man in town climb on top of her but wont marry because socially theyre all beneath her. Vittorio doesnt get what is going on. Their little brother Camillo, a seventeen-year-old seminary student turned Maoist provides the title of the film when he scrawls China is Near on the walls of the Socialist Party building, his brothers campaign headquarters.

==Cast==
* Glauco Mauri as Vittorio
* Elda Tattoli as Elena
* Paolo Graziosi as Carlo
* Daniela Surina as Giovanna
* Pierluigi Aprà as Camillo
* Alessandro Haber as Rospo
* Claudio Trionfi as Giacomo
* Laura De Marchi as Clotilde
* Claudio Cassinelli as Furio
* Rossano Jalenti
* Mimma Biscardi

==Critical response== Best Foreign Language Film at the 40th Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 40th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  

 
 
{{succession box Special Jury Prize, Venice
| years=1967 tied with La Chinoise
| before=Yesterday Girl tied with Chappaqua (film)|Chappaqua
| after=Our Lady of the Turks tied with Le Socrate}}
 

 
 
 
 
 
 
 
 
 


 