A Romance of Seville
{{Infobox film
| name     = A Romance of Seville Norman Walker
| writer   = Arline Lord   Alma Reville   Garnett Weston
| starring = Alexander DArcy Marguerite Allan Randle Ayrton   Cecil Barry
| cinematography = Claude Friese-Greene
| released = May 1929  July 1930 (sound version)
| runtime    = 62 minutes
| studio   = British International Pictures
| distributor = Pathé Pictures
| country  = United Kingdom
| language = English
}} Norman Walker and starring Alexander DArcy, Marguerite Allan and Cecil Barry.  This was the first British sound film to be made in colour, using the Pathecolor process.  The film is also known by the alternative title The Romance of Seville.

==Cast==
* Alexander DArcy - Ramon
* Marguerite Allan - Pepita
* Randle Ayrton - Estavian
* Cecil Barry - Estaban
* Hugh Eden - Juan
* Eugenie Amami - Dolores

==See also==
*List of early color feature films
*List of lost films

==References==
 

==Bibliography==
* Mundy, John. The British musical film. Manchester University Press, 2007.

==External links==
*  

 

 
 
 
 
 
 
 
 
 