Pyaar Ke Side Effects
 
 

{{Infobox Film
| name = Pyaar Ke Side Effects
| image = Pyaar-ke-Side-Effects Official-Poster.jpg
| caption = Film poster
| director =Saket Chaudhary
| producer =Pritish Nandy,  Rangita Pritish Nandy
| writer =Saket Chaudhary
| starring =Mallika Sherawat Rahul Bose Ranvir Shorey
| music =Pritam
| lyrics = Mayur Puri
| dialogues = Pratibha Acharya
| cinematography  = Manoj Soni
| art =Omung Kumar
| choreographer =Remo
| distributor = 
| released =15 September 2006
| runtime = 
| language =Hindi
| budget = Rs. 5&nbsp;million
| awards = 
|}}
 romantic comedy film. It was produced under the banner of Pritish Nandy communications and was written and directed by first time director Saket Chaudhary.  Mallika Sherawat and Rahul Bose played the lead cast. The film is a romantic comedy, and portrays the intricacies of a modern relationship. A sequel, Shaadi Ke Side Effects, was released in 2014 with Vidya Balan and Farhan Akhtar in the lead roles. 

==Plot==

Sid (Rahul Bose) plays a short, thirtyish DJ, who finds himself playing music at Trishas (Mallika Sherawats) marriage to Vivek (Jas Arora) in Delhi. However, he witnesses her fight her sense of responsibility and duty towards her parents and the groom, and runs away.

Six months later, he meets her again at a DJ competition in Mumbai, which he has just lost, yet again. Before you know it, theyre in a relationship, and three years have passed. Trisha thinks she is ready for marriage, and gets down on her knee to propose to Sid. Sid suffering from the typical commitment phobia, is at a loss for a reasonable answer. In a bid to not lose her, Sid finds himself engaged.
 father of the bride! The father has more than one problem with Sid. He doesnt have a future, he earns a lot less than Trisha, and he doesnt seem responsible enough. Over an altercation Sid has with Trishas father, the couple break up. Vivek is waiting in the wings, to help mend her broken heart. Sid on the other hand, finds himself being wooed by item girl, Tanya (Sophie Choudry), star of the Baby Girl vol. 3 video.

After a few amusing run-ins, an attempt to be just-friends, and a sad-song ("Jaane Kya" sung by Zubin), Sid, finally realises, thanks to his mother, that his commitment phobia is the result of a childhood scar. His father abandoned Sid and his mother, when Sid was very young. His mother reassures him, that he is nothing like his father, and would make a great husband. But its too late, Trisha has already agreed to marry Vivek. This leads him to Trishas dear friend Anjali (Suchitra Pillai), who reminds him of Dracula because she always attacks him, for making Trisha unhappy. She helps him crash Trishas wedding party.

The climax builds up, as Sids job involves not just convincing Trisha that his hang-ups have abated, but also avoid a deadly confrontation with Trishas father.

==Cast==
* Mallika Sherawat as Trisha Mallick
* Rahul Bose as Siddhant "Sid" Roy
* Ranvir Shorey as Narayanan "Nanu" Iyer
* Sophie Choudry as Tanya (Baby Girl Vol.3)
* Suchitra Pillai-Malik as Anjali (Dracula)
* Sharat Saxena as Retd. Major Gen. Veera Bhadra Mallick
* Tarana Raja as Shalini
* Aamir Bashir as Kapil
* Jas Arora as Vivek Chaddha
* Sapna Bhavnani as Nina

==Music==
The films music was composed by Pritam and the lyrics were written by Mayur Puri. The album, released in the month of July 2006, contained the following songs.
{| class="wikitable"
|-
! Song
! Singer
! Picturised on
! Lyrics
|-
| "Tauba Main Pyar Karke Pachtaya"
| Suzanne DMello, Labh Janjua
| Title and end-song
| Mayur Puri
|-
| "Will You Be My Bad Boy"
| Sophie Choudry, Earl, Alisha Chinoy
| Sophie Choudry
| Mayur Puri
|-
| "Jaane Kya Chaahe Maan Baawra"
| Zubeen Garg
| Rahul Bose, Mallika Sherawat
| Mayur Puri
|-
|"Dil Tod Ke Na Ja"
| Rakesh Pandit
| Rahul Bose
| Mayur Puri
|}

==Reception==
Pyaar Ke Side Effects was a modest box office success, earning 74.7&nbsp;million.  The film garnered mixed reviews from critics. Several critics, such as Indiafms Taran Adarsh felt that the second half of the film "drags". 

==Sequel==
The Pyaar Ke Side Effects team announced a sequel Shaadi Ke Side Effects in June 2012 with Vidya Balan and Farhan Akhtar in the roles of Trisha and Sid. All the other technicians will return for the sequel along with the director and music director. The film released on 28 February 2014.

==Awards==
For their work in Pyaar Ke, production company Pritish Nandy Communications won the Remi Award for Creative Excellence at the 2007 WorldFest-Houston International Film Festival. http://entertainment.oneindia.in/bollywood/news/pritish-nandy-remi-award-140507.html
"PNC wins Remi award at Houston" 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 