Subash (film)
{{Infobox film
| name           = Subash
| image          = 
| image_size     = 
| caption        = 
| director       = R. V. Udayakumar
| producer       = K. S. Srinivasan K. S. Sivaraman
| story          = Sujatha Udhayakumar
| screenplay     = R. V. Udayakumar
| writer         = S. Gajendra Kumar (dialogues)
| starring       =   Vidyasagar
| cinematography = Siva
| editing        = B. S. Nagaraj
| distributor    = Sivasree Pictures
| studio         = Sivasree Pictures
| released       = 20 September 1996
| runtime        = 150 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1996 Tamil Tamil action Arjun and Vidyasagar and was released on 20 September 1996. The film was an average grosser at the box office.  

==Plot==

Subash (Arjun Sarja|Arjun), a good-for-nothing youth, enjoys his life as much as possible. His brother, Rajasekhar (Siddique (actor)|Siddique), is a minister and his father (Jaishankar) is a court judge. Subash falls in love with Savitri (Revathi), a middle class Brahmin girl. Savitri loves him but she wants to marry with a intrepid man so Subash joins the army and becomes a military officer.

Upon his return, his family reveals that Savitri and her father died in an accident. Arumugasamy (Prakash Raj), an honest politician, thought being Rajasekhars opposite party is Rajasekhars best friend, dies mysteriously.

Subash suspects Thangamani (Manivannan), Arumugasamys right hand, for Arumugasamys death. Later, he finds Savitri in Thangamanis house and saves her. Savitri discloses that Rajasekhar is a corrupted politician and is linked to the terrorists. Rajasekhars henchmen killed Arumugasamy and Thangamani began to sequester her.

Subash swears to punish his brother and saves his country.

==Cast==
 Arjun as Subash
*Revathi as Savitri
*Monica Bedi as Anitha Siddique as Rajasekhar
*Prakash Raj as Arumugasamy Mounika as Arumugasamys wife
*Jaishankar as Subashs father
*Manivannan as Thangamani
*Vadivelu Vivek
*Silk Smitha
*Sudha as Lakshmi
*Y. Vijaya as Anithas mother

==Production==
The film was initially titled August 15th and was engaged in a long battle during production with another film to secure the title, but eventually had to settle for Subash. Furthermore, during the making of the film, Karthik Raja was dropped and replaced by Vidyasagar (music director)|Vidyasagar. 

==Soundtrack==
{{Infobox album |  
| Name        = Subash
| Type        = soundtrack Vidyasagar
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack
| Length      = 
| Label       = Pyramid Sa Re Ga Ma Vidyasagar
| Reviews     =
}}

The film score and the soundtrack were composed by film composer Vidyasagar (music director)|Vidyasagar. The soundtrack, released in 1996, features 6 tracks.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Lyrics !! Duration
|- 1 || Hawala || rowspan=6|R. V. Udayakumar || 4:29
|- 2 || Kudi Megan Iyan || 1:30
|- 3 || Mugham Enna || 4:41
|- 4 || Nay Saloma || 5:20
|- 5 || Nero Nonda || 4:37
|- 6 || Thendral Mela || 4:21
|}

==References==
 

 

 
 
 
 