Mexicali Rose (1929 film)
{{Infobox film
| name           = Mexicali Rose
| image          = Mexicali Rose 1929 Poster.jpg
| border         = yes
| alt            = 
| caption        = Lobby card. Sam Hardy and Barbara Stanwyck.
| director       = Erle C. Kenton
| producer       = Harry Cohn
| screenplay     = {{Plainlist|
* Norman Houston
* Gladys Lehman
}}
| story          = Gladys Lehman
| starring       = {{Plainlist|
* Barbara Stanwyck Sam Hardy
}}
| music          = 
| cinematography = Ted Tetzlaff
| editing        = Leon Barsha
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} romantic drama Sam Hardy.   

A silent and sound version are preserved at the Library of Congress. 

==Cast==
* Barbara Stanwyck as Mexicali Rose Sam Hardy as Happy Manning
* William Janney as Bob Manning
* Louis Natheaux as Joe, the Croupier
* Arthur Rankin as Loco, the Halfwit
* Harry J. Vejar as Ortiz
* Louis King as Dad, the Drunk
* Julia Bejarano as Manuela

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 


 