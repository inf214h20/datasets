Summer Bachelors
{{Infobox film
| name           = Summer Bachelors
| image          =
| imagesize      =
| caption        =
| director       = Allan Dwan William Fox
| based on       =  
| writer         = James Shelley Hamilton (scenario) Matt Moore Allan Forrest  Hale Hamilton
| music          =
| cinematography = Joseph Ruttenberg
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English intertitles
}} silent romantic Warner Fabian Matt Moore, Allan Forrest, and Hale Hamilton.    

A copy of Summer Bachelors is preserved at a film archive in Prague. 

==Cast==
* Madge Bellamy - Derry Thomas
* Allan Forrest - Tony Landor Matt Moore - Walter Blakely
* Hale Hamilton - Beverly Greenway
* Leila Hyams - Willowdean French
* Charles Winninger - Preston Smith
* John Holland - Martin Cole
* Olive Tell - Mrs. Preston Smith
* Walter Catlett - Bachelor #1
* James F. Cullen - Bachelor #2
* Cosmo Kyrle Bellew - Bachelor #3
* Charles Esdale - Bachelor #4

==Production notes==
Interiors shot were filmed at Foxs New York studio, while exteriors were shot on location in Lake Placid, New York. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 