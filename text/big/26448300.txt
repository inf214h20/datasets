Mahakavi Kalidasu
{{Infobox film
| name           = Mahakavi Kalidasu
| image          = Mahakavi Kalidasu.jpg
| image_size     =
| caption        =
| director       = Kamalakara Kameswara Rao
| producer       = K. Nagamani  P. Suri Babu
| writer         = Pingali Nagendra Rao
| narrator       = Sriranjani Relangi Vangara P. Suri Babu
| music          = Pendyala Nageshwara Rao P. Suri Babu Annayya
| editing        = R. V. Rajan
| studio         =
| distributor    =
| released       = 1960
| runtime        =
| country        = India Telugu
| budget         =
}}

Mahakavi Kalidasu (  based on the life of the great Sanskrit poet Kālidāsa|Kalidas. It is directed by Kamalakara Kameshwara Rao. Akkineni Nageswara Rao played the role of Kalidas. Some poems of Kalidas including Shyamala dandakam by Ghantasala Venkateswara Rao is memorable.

==Credits==

===Cast===
* Akkineni Nageshwara Rao	... 	Kalidasa Mahakavi
* S. V. Ranga Rao	... 	Bhoja Raju Sriranjani
* Chilakalapudi Seetha Rama Anjaneyulu
* Rajasulochana
* Relangi Venkataramaiah
* Vangara Venkata Subbaiah
* Vaasanti	... 	Kalika Devi
* Sandhya
* P. Suri Babu
* K. V. R. Sharma
* Sitaram
* Lingamurthy

===Crew===
* Producers: K. Nagamani and P. Suri Babu
* Production company: Sarani Productions
* Director: Kamalakara Kameshwara Rao
* Assistant Director: V. Ramchand
* Story, Dialogues and Screenplay: Pingali Nagendra Rao
* Producers: Nagamani and Suribabu
* Cinematography: Annayya
* Choreography: Pasumarthi Krishna Murthy, Vempati Satyam
* Original Music: Pendyala Nageshwara Rao and P. Suri Babu (for poems and slokas)
* Art Direction: Madhavapeddi Gokhale
* Editing: R. V. Rajan
* Playback Singers: Ghantasala Venkateswara Rao, Madhavapeddi Satyam, P. Leela, Jikki Krishnaveni, P. Susheela, Jayalaxmi and K. Rani.
* Stills: M. Sathyam
* Sound effects: A. Krishnan

==Story==
Nageshwar Rao who acts as the poet was a shephard who was very foolish. But he was a great devotee of goddess Mahakali. There lived a king in his kingdom whose daughter was a great scholar. She told that she would marry the person who would answer her questions. Many scholars tried their luck but could not answer her questions. One of the scholars felt insulted and wanted to take revenge. While he was walking in the forest he met Kalidas and found that there would be no foolish person other than Kalidas because he was cutting the branch in which he was sitting. He thought he could take revenge by making him marry the princess.He convinced Kalidas by saying that he could marry the princess without speaking a word. So Kalidas in the disguise of poet went to the kingdom along with the scholar who wanted to take the revenge. Saying the king that the poet would not speak today the scholars answered the kings daughters questions in a different way which Kalidas meant in a different way. The princess impressed by his answers married Kalidas. Later she knows that she had been fooled and faints in depression. Kalidas feels sad and prays to goddess Kali to give him the knowledge of everything. Impressed by his severe penance goddess Kali gives him knowledge of all the vedas etc.So he forgets his past and becomes a great scholar. On his way he meets many kings and one of them Bhoja (S.V.Ranga Rao) requests him to stay in his kingdom. Kalidas agrees. Meanwhile his daughter gets attracted to the poet. But his wife comes to know the place where he is staying after many days of searching him. But Kalidas could not recognize her.But she works as an apprentice to him. She feels insulted from the kings daughter when she asks her why she is been always with Kalidas even when they are talking personal matters.At last Kalidas comes to know about his wife(Due to Devi Kalis grace) and rejoins with her and the kings daughter begs for forgiveness from Kalidasas wife for her mean behaviour.

==Songs==
* Jaya Jaya Jaya Sharada Jaya Kalabhi Sharada (Singer: P. Susheela)
* Manikya Veena Mupaalalayanti Madalasam Manjula Vagvilasam (Singer: Ghantasala)
* Neekettundogani Pilla Naakubhalega Vundile (Singer: Ghantasala)

==Awards== National Film Awards
*     

==References==
 

 

==External links==
*  

 
 
 
 
 
 