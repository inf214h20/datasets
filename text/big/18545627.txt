Tweety's Circus
{{multiple issues|
 
 
 
}}
{{Infobox Hollywood cartoon|
| cartoon_name = Tweetys Circus
| series = Merrie Melodies (Tweety) Arthur Davis| voice_actor = Mel Blanc| musician = Milt Franklyn
| producer = Eddie Selzer
| story_artist = Warren Foster Warner Bros. Pictures
| release_date = June 4, 1955
| color_process = Technicolor
| runtime = 7 minutes 4 seconds
| movie_language = English
}}

Tweetys Circus is a "Merrie Melodies" cartoon animated short starring Tweety Bird|Tweety, Sylvester (Looney Tunes)|Sylvester, and various circus animals. Released June 4, 1955, the cartoon is directed by Friz Freleng. All the voices are performed by Mel Blanc.

==Plot==
The story centers on Sylvester visiting a circus, where he not only tries to catch Tweety for his meal, but attempt to one-up a lion (an attraction billed as "King of the Cats").

A carefree Sylvester walks into the circus singing his theme "Meow!" where he visits the various animal exhibits. There, upon seeing the lion exhibit, the unimpressed cat immediately expresses his displeasure over the large felines billing.  All that changes when he realizes hed just passed by the Tweety Bird... and thus the chase begins.

Tweety runs into the big top, where the lion (now uncaged) is waiting to maul Sylvester for his earlier remarks (not to mention Sylvester clobbering him with a shovel). From this point forward, the lion serves as both an antagonist for Sylvester and a protector of Tweety.

Sylvester tries beating what he thinks is a fire hose to free Tweety, unknowing that the "hose" is an elephants elephant#Trunk|trunk. The elephant grabs Sylvester with his trunk and—after crushing his chest—throws the battered puss into the lions cage, where the lion finishes the job.

Other run-ins with the lion, elephant and other animals, all ending with Sylvester getting the worst of things, involve him exploiting his abilities as a high diver (Tweety directs the elephant to "drink it all down" (referring to the water) before Sylvester lands), a fire eater (the lion makes Sylvester eat the fire) and a high-wire walker ("hewwooooo, puddy tat!").
 carnival barker ("Huwwy! Huwwy! Huwwy! Step wight up for da gweatest show on Eawth! Fifty wions and one puddy tat!") A loud roar erupts, and with Sylvester presumably having met his fate, Tweety changes his spiel: "Step wight up! Fifty wions, count em, fifty wions!"

==Succession==
 
{{succession box |
before= Sandy Claws | Tweety and Sylvester cartoons |
years= 1955 |
after= Red Riding Hoodwinked|}}
 

==References==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==External links==
*  

 
 
 
 
 


 