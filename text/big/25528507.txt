Hop and Go
{{Infobox Hollywood cartoon
| cartoon_name = Hop and Go
| series = Looney Tunes
| director = Norman McCabe
| story_artist = Melvin Millar
| animator = Cal Dalton
| voice_actor = Pinto Colvig (uncredited) Mel Blanc (uncredited)
| musician = Carl Stalling
| producer = Leon Schlesinger Leon Schlesinger Productions Warner Bros. Pictures The Vitaphone Corporation
| release_date = March 27, 1943 Black and White
| runtime = 7 min. (one reel)
| movie_language = English
}}

Hop and Go is a 1943 Looney Tunes animated cartoon directed by Norman McCabe and animated by Cal Dalton. It stars the voices of Pinto Colvig (Claude Hopper) and Mel Blanc (Scottish Rabbits). It was released to theatres on March 27, 1943 by Warner Bros.

==Edited versions==
When this cartoon aired on Nickelodeon, the ending where Claudes jump destroys Tokyo was edited to remove the reveal that the rubble Claude is resting on is, in fact, the remains of Tokyo. The rubble was replaced with a fake iris out. 

==Availability==
  presents this cartoon as a bonus cartoon on Disc 2, uncut but not digitally remastered. The cartoon uses an unreleased stereo mix of its soundtrack. Note that some of the character dialogues volume has been decreased, making it hard for viewers to understand the lines the characters say.

==References==
 

 
 
 


 