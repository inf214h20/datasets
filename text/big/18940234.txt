The Yellow Balloon (film)
{{Infobox film
| name           = The Yellow Balloon
| image          = "The_Yellow_Balloon"_(1953).jpg
| caption        = 
| director       = J. Lee Thompson
| producer       = Victor Skutezky
| writer         = Anne Burnaby J. Lee Thompson
| starring       = 
Andrew Ray; 
Kenneth More 
Kathleen Ryan 
William Sylvester 
| cinematography = Gilbert Taylor
| music          = Philip Green ABPC
| distribution   = Associated British-Pathé
| released       = 10 February 1953 (UK)
| runtime        = 76 minutes
| country        = United Kingdom English
| gross = £116,245 (UK) 
}} 1953 British drama film starring Kenneth More, Bernard Lee, Andrew Ray, Kathleen Ryan and Sid James.  

==Plot==

The scene: a bombed – out building in London’s East End. Two young boys, 12 year olds Frankie Palmer (Andrew Ray) and Ronnie Williams (Stephen Fenemore) are running about in the ruins when Ronnie slips and falls thirty feet to his death. Frankie scrambles down to help, but realises that there is nothing he can do. Hiding in the shadows and seeing it all, Len Turner (William Sylvester), a criminal on the run and using the ruins as a hideout from the police, convinces Frankie that the police will arrest the boy and charge him with the murder of his friend for pushing him to his death and that they must both make their getaway. Although Frankie and Len agree it was an accident, Len is adamant that the police will not see it that way and Frankie goes off with him. Len blackmails Frankie into stealing money from his parents (Kenneth More and Kathleen Ryan) to help fund Len’s escape and then uses the boy as a decoy in a pub robbery that goes horribly wrong when Len murders the publican. Realising that Frankie is the only witness to his crime, Len knows he must kill the boy, too. This develops into a terrifying hide and seek chase through a bomb-damaged; abandoned and highly perilous London Underground station with Len hot on the heels of Frankie, who is desperately trying to escape with his life…!
 Hunted (1952) and equally as good, The Yellow Balloon was one of the first films to be passed with the then new Adults Only “X” certificate by the British Board of Film Censors, which barred anyone under the age of 16 years being allowed into a cinema to see the film. This was because the censor felt that the chase through the Underground station in the last reel would be very frightening for young children and Andrew Ray, 13 years old when the film was shot in 1952 and 14 years old when it was released in May, 1953, was disappointed that he wasn’t allowed to go into a cinema to see his own film because he was way under the age of 16. However, after complaints from cinema exhibitors that the “X” certificate wasn’t really necessary for the film and it was losing them the family audience they had relied on up until that time, the BBFC eventually relented and in October, 1953, they re-classified the film with an “A” certificate (children under 16 allowed in to see the film if accompanied by an adult).

==Cast==
* Andrew Ray as Frankie Palmer 
* Kathleen Ryan as Emily Palmer 
* Kenneth More as Ted Palmer 
* Bernard Lee as PC Chapman 
* Stephen Fenemore as Ron Williams
* William Sylvester as Len
* Marjorie Rhodes as Jessie Stokes  Peter Jones as Sid
* Eliot Makeham as Pawnbroker 
* Sid James as Barrow Boy
* Veronica Hurst as Sunday School Teacher 
* Sandra Dorne as Iris
* Campbell Singer as Potter  
* Laurie Main as Bibulous Customer  
* Hy Hazell as Mary

==Critical reception==
In The New York Times, Bosley Crowther wrote, "it is a leisurely sort of chiller that trades intriguingly upon a youngsters far-fetched fears...The moral is, of course, that children should speak up rather than harbor their fears. But they dont. So probably the British will be able to go right on making these variably fascinating films for years."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 