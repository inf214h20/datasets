Destination Hitchcock: The Making of North by Northwest
{{Infobox Film
| name           = Destination Hitchcock: The Making of North by Northwest
| image          = 
| image_size     = 
| caption        = 
| director       = Peter Fitzgerald
| producer       = Turner Classic Movies Peter Fitzgerald
| writer         = 
| narrator       = 
| starring       = Eva Marie Saint Patricia Hitchcock
| music          = 
| cinematography = Andrew B. Andersen
| editing        = Glenn Erickson
| distributor    = Turner Classic Movies Warner Home Video
| released       = 2000
| runtime        = 40 min
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Destination Hitchcock: The Making of North by Northwest is a 2000 documentary film about the making of Alfred Hitchcocks classic thriller film North by Northwest.  It is hosted and narrated by the films female lead, Eva Marie Saint and features interviews with Hitchcocks daughter, several of the technical and behind-the-scenes people involved with the production of the film, and the main surviving actor, Martin Landau.  The film has gained wide circulation due to its inclusion in Universals release of North by Northwest in VHS and DVD formats which include this film as an additional feature.   

==References==

 

== External links ==
*  

 
 
 
 


 