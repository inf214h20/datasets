To Sir, with Love II
 
{{Infobox television film
| name           = To Sir, with Love II
| image          = To Sir, with Love II.jpg
| image_size     = 
| caption        = To Sir, with Love II DVD cover
| genre          = Drama
| distributor    = TriStar Television
| creator        = 
| director       = Peter Bogdanovich
| producer       = Richard Stenta
| writer         = Philip Rosenberg
| screenplay     = 
| story          =  To Sir, With Love
| narrator       = 
| starring       = Sidney Poitier Christian Payton Dana Eskelson
| editing        = Dianne Ryder-Rennolds
| music          = Trevor Lawrence
| cinematography = William Birch
| studio         = Verdon-Cedric Productions, Inc. Adelson/Baumgarten Productions, Inc.
| budget         = 
| country        = United States
| language       = English
| network        = CBS
| released       =  
| first_aired    = 
| last_aired     = 
| runtime        = 92 minutes
| num_episodes   = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
To Sir, with Love II is a 1996 American television film. It is a sequel to the 1967 British film, To Sir, with Love. Like the first film, it deals with social issues in an inner city school. It was directed by Peter Bogdanovich (his first made-for-TV film) and stars Sidney Poitier reprising the role of Mark Thackeray.

==Plot== West Indian, who in the 1967 film took a teaching position in a London East End school. He spent twenty years teaching and ten in administrative roles. He has taught the children of his former pupils, and is now retiring.
 Lulu reprising their roles from the original film), come to the farewell party. Thackeray announces that he is leaving for an inner-city school in Chicago where he will teach again. In Chicago, he meets his former colleague Horace Weaver (Daniel J. Travanti) who is the principal of the school. Thackeray learns that there is an A class with good students and an H (for "horror") class for the "no-gooders". He convinces the principal to let him take the H class as a history teacher. His new pupils are Hispanic, black, and white students who are noisy, unruly and engaged in destructive behaviors. As he did in London, he starts by teaching them respect for others. He addresses them as Mr X or Miss Y, and expects to be called Mr. Thackeray or Sir (hence the titles).

Little by little he learns their personal stories: Wilsie (Christian Payton) is a gang leader who protects his younger brother. Another is a black female who battles against double prejudice. Evie (Dana Eskelson) is growing up without parents and hides this to avoid being fostered. Unlike the British film, there is no infatuation with him among his pupils, but a fellow teacher Louisa Rodriguez (Saundra Santiago) admires him.
 Britain to study, became a teacher and got married. He is now a widower but decided to take this teaching opportunity to find his earlier love.

At school he sets out to teach these troubled kids of their true potential if they take their fate in their hands. He teaches about the non-violent resistance of the historic fighters of civil rights. When he discovers Wilsie smuggling a gun into the school, he confronts him and convinces him to yield the gun. Mr. Thackeray delivers it to a policeman as a found object.

Later, the police pressures him to give the name of the armed kid, since the gun was involved in a cop killing. He refuses to give up the name of the student and has to leave the school.

Meanwhile, Evie has taken a job in a newspaper and decides to investigate on the old Chicago love of Thackerays. The girl arranges an appointment for him. Thackeray meets the son of his former love in a hospital. His mother, Emily Taylor (Cheryl Lynn Bruce), is ill. Thackeray learns that she loved him back but her father retained all his letters, because she had gotten pregnant, so the young man whom he had just met is his son.

Thackeray learns that Wilsie is hidden because he thinks that the police are after him.
His brother takes Thackeray to the hideaway to explain the real situation and avoid Wilsie ruining his life. Through courage and talking, the teacher convinces Wilsie to yield his new gun and confronts a rival gang that had come to fight Wilsie. Wilsie and the friend who had got him the gun explain themselves at the precinct.
 stand in" and force the principal to accept their beloved teacher back.

The film ends with the graduation ceremony and dance. Mr. Thackeray announces that he is not going back to Britain but staying at Chicago to teach the new generation.

==Cast==
* Sidney Poitier as Mark Thackeray
* Christian Payton as Wilsie Carrouthers
* Dana Eskelson as Evie Hillis
*Fernando López as Danny Laredo
*Casey Lluberes as Rebecca Torrado
*Michael Gilio as Frankie Davanon
* LZ Granderson as Arch Carrouthers
*Bernadette L. Clarke as LaVerne Mariner
*Jamie Kolacki as Stan Cameli
* Saundra Santiago as Louisa Rodriguez
*Cheryl Lynn Bruce as Emily Taylor
* Daniel J. Travanti as Horace Weaver Lulu as Barbara Pegg
* Judy Geeson as Pamela Dare
*Kris Wolff as Billy Lopatynski
* Mel Jackson as Tommie Rahwn John Beasley as Greg Emory
*Antonia Bogdanovich (the directors daughter) as Lynn Guzman
* Jason Winer as Leo Radatz

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 