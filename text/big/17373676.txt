That's Me, Too
{{Infobox Film
| name           = Sådan er Jeg Osse
| image          = Sådan er Jeg Osse.jpg
| image_size     = 200px
| caption        = Poster
| director       = Lise Roos 
| producer       = Erik Overbye  
| writer         = Lise Roos 
| narrator       = 
| starring       = 
| music          = Morten Kærså    
| cinematography = Jan Weincke
| editing        = Edith Toreg     
| distributor    = Focus Film
| released       = 29 February 1980 
| runtime        = 103 minutes
| country        = Denmark
| language       = Danish language
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1980 Denmark|Danish drama film directed and written by Lise Roos. It was the last film to star Inger Stender.

==Cast==
*Stine Sylvestersen ...  Stine 
*Avi Sagild ...  Stines mor 
*Maria Tagliani ...  Stines lillesøster 
*Thomas Roos ...  Stines lillebror 
*Preben Kaas ...  Stines far 
*Anne-Lise Gabold ...  Stines fars nye kone 
*Eline Roos ...  Stines fars nye datter 
*Tobias Hansen ...  Stines fars nye søn 
*Inger Stender ...  Stines mormor 
*Morten Krøgholt ...  Stines ven 
*Rasmus Kærså ...  Stines ven 
*Gitte Schödt ...  Stines veninde 
*Sussie Egesten ...  Stines veninde 
*Jakob Olsen ...  Den ene venindes fyr 
*Søren Thomsen ...  Den voksne 
*Birgit Kragh ...  Stines gamle lærerinde 
*Kirsten Olesen ...  Bryggeriarbejder 
*Arne Skovhus ...  Arbejdsformidler 
*Kim Sagild ...  En drømmemand 
*Ulrich Breuning ...  Madsen

==External links==
*  

 
 
 
 


 
 