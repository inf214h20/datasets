The Brass Bullet
{{Infobox film
| name           = The Brass Bullet
| image          = The Brass Bullet.jpg
| caption        = 
| director       = Ben F. Wilson
| producer       = Ben F. Wilson Walter Woods
| starring       = Juanita Hansen Jack Mulhall
| music          = 
| cinematography = 
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 18 episodes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 adventure film serial directed by Ben F. Wilson. It is now considered to be lost film|lost.   

==Cast==
* Juanita Hansen as Rosalind Joy
* Jack Mulhall as Jack James
* Charles Hill Mailes as Homer Joy
* Joseph W. Girard as Spring Gilbert
* Harry Dunkinson
* Helen Wright as Mrs. Strong
* Ashton Dearholt as Victor King
* Charles Force as A minister
* Hallam Cooley as The Mystery Man

==Chapter titles==
# A Flying Start
# The Muffled Man
# The Mysterious Murder
# Smoked Out
# The Mock Bride
# A Dangerous Honeymoon
# Pleasure Island
# The Magnetic Bug
# The Room of Flame
# A New Peril
# Evil Waters
# Caught By Wireless
# $500 Reward
# On Trial For His Life
# In The Shadow
# The Noose
# The Avenger
# The Amazing Confession

==See also==
* List of American films of 1918
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 