Magno Mainak
{{Infobox film
| name           = Magno Mainak
| image          = Magno Mainak.jpg
| alt            = 
| caption        = DVD cover of the film.
| director       = Swapan Ghosal
| producer       = New Wave Communications
| writer         =  Magno Mainak by Sharadindu Bandyopadhyay
| starring       =
| music          = Debjit
| editing        =
| studio         = New Wave Communications
| distributor    = 
| released       =   cinematography  = 
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 Crime Thriller Thriller based on the novel of same name by Sharadindu Bandyopadhyay.    The film is be directed by Swapan Ghosal, and produced by New Wave Communications. 
This is the third Byomkesh Bakshi film adaptation. Actor Subhrajit Dutta acted as Byomkesh while Rajarshi Mukherjee played the role as Ajit. Piyali Munsi, Rupanjana Mitra, Gargi Roychowdhury and Biplab Chatterjee acted in other roles.  

==Cast==
* Subhrajit Dutta as Byomkesh Bakshi
* Rajarshi Mukherjee as Ajit Bandyopadhyay
* Piyali Munsi as Satyabati
* Rupanjana Mitra
* Gargi Roychowdhury
* Biplab Chatterjee

==Production==
Director Swapan Ghoshal made two Byomkesh Bakshi TV series before this film. Byomkesh Bakshi in 2004 aired on DD Bangla. Another is a short lived series Byomkesh in 2007, which aired on Tara Muzik. After these two successful series Swapan Ghoshal decided to make a full length feature film and he made it in 2009 with Sharadindu Bandyopadhyays Magno Mainak adaptation.  {{cite news| url=http://www.telegraphindia.com/1090618/jsp/entertainment/story_11125093.jsp| title=Sleuth saga
- The first Byomkesh Bakshi film;|accessdate=27 July 2014|publisher=The Telegraph}} 

==References==
 

==See also==
* Shajarur Kanta (1974 film) Byomkesh Bakshi
* Abar Byomkesh
* Byomkesh Phire Elo
* Satyanweshi
* Shajarur Kanta (upcoming film)

 
 

 
 
 
 
 

 