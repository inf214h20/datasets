Don 2
 
 
{{Infobox film
| name           = Don 2
| image          = Don2new.jpg
| alt            = A poster that features two people is shown. A man wearing a tuxedo, looking angry, is sitting on the edge of a throne made of rocks. A female, wearing a black gown and holding a gun in her right hand, is sitting between his parted legs.
| caption        = Theatrical release poster
| director       = Farhan Akhtar
| producer       = Shahrukh Khan Farhan Akhtar Ritesh Sidhwani
| screenplay     = Farhan Akhtar
| story          = Farhan Akhtar Ameet Mehta Amrish Shah
| based on       =  
| starring       = Shah Rukh Khan Priyanka Chopra Lara Dutta Kunal Kapoor Boman Irani Om Puri
| music          = Shankar–Ehsaan–Loy|Shankar-Ehsaan-Loy
| cinematography = Jason West
| editing        = Anand Subaya Ritesh Soni
| studio         = Excel Entertainment Red Chillies Entertainment
| distributor    = Reliance Entertainment
| released       =  
| runtime        = 144 minutes   
| country        = India
| language       = Hindi
| budget         =   
| gross          =      
}}
 Indian action action crime crime thriller Don franchise, directed and co-written (with Ameet Mehta and Amrish Shah) by Farhan Akhtar. The film was produced by Ritesh Sidhwani, Shah Rukh Khan and Farhan Akhtar, and is a sequel to 2006s Don (2006 Hindi film)|Don. It stars Shah Rukh Khan, Priyanka Chopra, Lara Dutta, Om Puri, Boman Irani and Kunal Kapoor, with Hrithik Roshan in a cameo appearance. The films plot picks up at the end of Don, beginning in Malaysia and moving to Europe. Don, now king of the Asian underworld, plans to take over the European drug cartel. Don 2 marked Akhtars return to directing after nearly five years. It was filmed in India, Thailand, Germany, Malaysia and Switzerland.
 Best Action. Don 2 ended up with a final gross collections of   including all versions in 100 Days worldwide. 

== Plot ==
The film opens five years after the ending of the first film, with  . Instead of the expected uneventful transaction, Don fights his way out. He returns to Malaysia, surrendering to Roma (Priyanka Chopra) and Inspector Malik (Om Puri). Don is sentenced to death and sent to prison, where he meets old rival Vardhaan (Boman Irani). He offers Vardhan a partnership, and they escape from prison after poisoning the other inmates.
 Nawab Shah), an assassin. Don escapes, however, and baits Jabbar to working for him. With no other choice, Diwan gives Don the blueprints.

Don and his team plan and execute a bank robbery, taking hostages. After stealing the printing plates he is betrayed by Vardhaan and Jabbar, but escapes. However, Sameer (a team member, played by Kunal Kapoor) calls the police and Don is arrested; he threatens Sameer for informing on him. Unable to enter the bank and free the hostages, the police are forced to work with Don in exchange for giving him German immunity.

Don and Roma reach Vardhaan and Jabbar, but Roma is shot when she refuses to kill him. She still has feelings for Don, although he killed her brother five years earlier. He defeats Vardhaan and his thugs, and Jabbar is killed. Don obtains his immunity papers, surrendering the plates and a disc with information about his team. He brings Roma to a waiting ambulance, and they exchange glances before the doors close. Don later detonates a bomb planted earlier in Diwans car.

In a final scene Don still has one of the currency plates, which the police think was destroyed by the explosion. Sameer was actually loyal to Don; informing the police was part of the plan. The disc Don gave them actually contains the names of the European drug cartel; when they are arrested, Don becomes king of the European underworld and tells Ayesha and Sameer they have no idea how rich they will become. When Ayesha tells him she was afraid he would be caught, he says: "Don ko pakadna mushkil hi nahin, namumkin hai" ("It is not just hard to catch Don, it is impossible.") and the film ends with the song "Dushman Mera"

== Cast ==

 Don
: A drug lord and the narrator of the film. He has established his dominion over Asia, and sets his sights on organised crime in Europe. Khan reprised his role in the 2006 film, Don. In this film, unlike the first, he plays only one part.Khan excersied exstensivley for the role and performed almost all the stunts by himself.Don 2 marked Khans return to playing villains after earlier films such as Darr and Baazigar.  It is the first sequel in Khans two-decade-long career. 
 
* Priyanka Chopra as Roma
: Now an Interpol officer, Roma continues chasing Don to avenge the deaths of her brother Ramesh and his fiancée Kamini at his hands. Chopra learned martial arts, trained for over two months and wanted to perform her own stunts. 
* Boman Irani as Vardhaan:
: The chief antagonist, Vardhaan was an underground criminal who disguised himself as a police officer named DSilva. He is framed by Don, and arrested in the first film. In Don&nbsp;2, he works with Don on the robbery but secretly seeks revenge. Irani lost 12 kg for the role and grew a beard to make his character look "cold, cunning and deceitful"; he felt that playing a villain was "a big stretch" for him as an actor. 
* Lara Dutta as Ayesha
: Dons trusted companion, she helps him in his plans to rob the German Central Bank. Dutta was cast after Eesha Koppikar|Koppikar, who played the role in Don, dropped out of the film. She was recommended to Akhtar by Khan, who had worked with her on Billu in 2009. In an interview with The Hindustan Times Dutta said it was exciting to play a new character, since there would be no predecessor to live up to. 
* Kunal Kapoor as Sameer:
: A computer hacker hired by Don for his bank robbery. It was reported that Kapoor was cast as a replacement for Arjun Rampal, who played Jasjeet in Don; however, the producers and Kapoor denied the reports. 
* Om Puri as Vishal Malik, head of the Malaysian Interpol
* Alyy Khan as Jamal Khawar (J.K.) Diwan, vice-president of the DZB (Deutsche Zentral Bank; German Central Bank) Nawab Shah as Abdul Jabbar, an assassin hired by Diwan to kill Don. He is forced to work with his target, but teams up with Vardhaan to kill Don.
* Sahil Shroff as Arjun, Romas friend since she joined Interpol
* Florian Lukas as Jens Berkel
* Rajesh Khattar as Singhania, Dons boss. Killed by Vardhaan in Don, he appears in flashbacks.
* Hrithik Roshan as Don in disguise
*   as Yana, Sameers girlfriend

== Production ==

=== Development ===
  DNA India Akhtar said that he decided to make a sequel to the 2006 remake of a 1970s classic because it "had a lot to offer" and freed him to explore the character of Don, who fascinated him.   
 Reliance Mediaworks. 

The sequel had a different team of writers from Don, and included Akhtar, Ameet Mehta and Amrish Shah. Akhtar said, "The first film was my fathers vision, but Don 2 is being written by fans only. These are yours truly and two new enthusiastic boys, Ameet and Amreesh, who came to me with their ideas on how the story can be carried forward. Ive thrown my ideas at them. Don 2 is basically being written by three Don fans who are having a ball carrying the story forward."  Khan became the first celebrity to copyright a tattoo when he registered the "D" tattoo seen on his arm in the film. 

In August 2011, the producers said they would release Don 2 in Stereoscopy|3D. The idea of 3D conversion occurred to Akhtar while shooting; after tests in Los Angeles, it was decided to convert the entire film.  The director of photography had used special lenses, which made conversion easier. Sidhwani said that the idea of converting the film into 3D came to him after seeing Harry Potter and the Deathly Hallows – Part 2. Chuck Comisky, a veteran S-3D innovator who supervised 3D stereo and visual effects for Avatar (2009 film)|Avatar and Sanctum (film)|Sanctum, was given the job. 

=== Filming === The French Cathedral and the East Side Gallery. 

Khan experimented with a variety of looks and trained for the film, deciding to perform his own stunts.      In Berlin, he performed a dangerous 300-foot jump for a scene.  A car-chase scene using 67 cars closed main thoroughfares (including the Brandenburg Gate) for three weeks. Each day, alternate routes were provided around roads cordoned off for filming.  The shooting schedule also included a special appearance by Hrithik Roshan, whose presence was kept under wraps by filmmakers.  In December 2010, the Berlin shooting ended.  

In February 2011 the actors flew to Malaysia, where several scenes were shot in Malacca Prison with prisoners as extras. A section of the prison block was made available for filming, with special T-shirts distinguishing crew members from prisoners in the high-security zone.  Filming in Malaysia lasted for a month, ending on 20 March.

Khan became the only Indian actor to have filmed in Germany, a country known for refusing permits for foreign film shoots. A production executive said, "Unlike the US, UK and Thailand, it isnt easy to obtain permission to shoot in Germany. The rules are very strict ... Shahrukh Khan is so famous in Germany and that made a great difference to shoot Don 2 there." Sidhwani said, "We are the first Indian film which has managed to do that. There were certain pre-conditions that we were required to meet and we had to be very careful. Eventually they did open the doors for us. Once we were together on making the film, they proved to be extremely helpful." 

Khan trained with international stunt and fight director Wolfgang Stegemann, who has worked on a number of big-budget Hollywood films. Stegemann (who also played Karl, an antagonist) created a unique fighting style for the actor: "These are powerful and effective to knock off Dons enemies. I had to analyse Mr. Khans motions and find the perfect fight style for him. It is a mix between martial arts, Krav Maga, Wing Chun and Trapping, which has not been practised in Bollywood. It helped him endure long settings of fights together." 

In September 2011, filming resumed on a song featuring the two leads.  Another song was shot with Khan in Goa in late November, only a month before the films release.  This was a first-of-its-kind music video; instead of a choreographed song and dance, the filmmakers shot the song as an action sequence. The song, a promotional video, did not appear in the film.   

=== Soundtrack ===
 
Don 2 s score and soundtrack were composed by Shankar–Ehsaan–Loy, with lyrics by Javed Akhtar; both also worked on Don. Director Farhan Akhtar announced that singer Usha Uthup would record a song, "Hai Ye Maya", for the film.  The album features nine tracks, including two remixes and two instrumentals. It received a mixed response, with some describing it as disappointingly safe, self-conscious and too similar to Don. However, most reviewers agreed that the music was true to the films identity and genre and followed the narrative. The soundtrack was released on 17 November 2011, over a month before the films release, by T-Series. 

== Marketing ==

===  Pre-release revenue === Zee Network acquired the satellite rights for  , the highest satellite price for a Reliance Entertainment film that year.    Distribution rights in Tamil Nadu and Kerala were bought by the newly formed Sudha Screens Sreeraj for an undisclosed price.  Don 2 music rights were sold to T-series for  .

=== Promotion ===
  Mumbai bombings. A second trailer was released online on 24 October.  The film previewed at the Dubai International Film Festival,  and its trailer was the most-watched YouTube video in India during 2011. 
 Microsoft India to launch a "Meet the Don: Mission Berlin" contest offering a chance to meet Khan and a trip to Berlin. To popularise the contest, Microsoft planned a marketing campaign which included a television commercial, radio advertisements and digital banners. 

Since Dons catchphrases in the first film became popular, Sidhwani and Akhtar decided to publicise dialogue from Don 2 by releasing 10 lines delivered by Don as "Don Says...": one line each Friday from 15 October until the films release on 23 December. The lines were released simultaneously across all media platforms: television, print, radio, Internet and mobile.  The "Don says..." one-liners, also known as "Don-isms", became popular worldwide. 
 Hyderabad and other cities. 

=== Comic book and video game ===
In October 2011 a comic book based on Don 2, Don: The Origin, was published in Mumbai. A prequel, it details Dons past. About the comic, Ritesh Sidhwani said: "Don 2 takes off from where Don: The Chase Begins Again left off. In these five years, many have forgotten the film and must be curious to know how Don came into being. This original story will help them understand him and also offer a background on other characters like Roma and Vardhaan."   

 , was released in India in February 2013 as the final PAL game for PS2.

== Release ==
Don 2 was released worldwide 23 December 2011 on 3,105 screens in the domestic market, including 500 prints in  
|date=22 August 2011| accessdate=22 August 2011}}  The second phase of the films international release began in January 2012 in 84 countries.  In February 2012, Don 2 was shown at the 62nd Berlin International Film Festival  and became the most popular film at the festival. 

Reliance Entertainment obtained a "John Doe" order from the Delhi High Court allowing it to serve cease-and-desist notices on film pirates. Anti-piracy chairman Rajkumar,  speaking about the order, said: "We have already served notices to 43 ISPs, who we consider potential pirates. We have also alerted the cops and our team indigenously, but our primary intention is to halt menacing online piracy generating across shores." 

===  Controversy === Reliance and Excel Entertainment about the sequel. According to the notice, Nariman had given the rights to Excel Entertainment in perpetuity for Don alone.  The producers replied, "Whatever we have done is legal, so the claim is false and so are the charges. This is an absolutely new film and there is no question of a delay in releasing Don 2."  Khan added in an interview with Mumbai Mirror, "While my producers are best qualified to give a fitting reply to the whole matter, I as a lead actor can say that as for the sequel, the Amitabh Bachchan Don (1978 film)|Don never had a sequel. So where is the question of us copying anything. Farhan and his team have worked on the script, I know for a fact that we havent copied from anywhere and this is an original script."  On 19 December, a week before Don 2 s release, the Bombay High Court refused to stay the films release. The courts ruling considered the films scheduled release on 21 December overseas and 23 December in India, for which many theatres had been booked. 

The Health Ministry advised the producers to have the lead character talk about the negative health consequences of smoking, demanding that smoking scenes in the film be scrolled down or blurred. Sidhwani and Javed Akhtar met Information and Broadcasting Minister Ambika Soni to resolve the matter, saying: "We all agree that smoking is a major health issue and there is no intention of glorifying it in the film. It is shown as a weakness or a trait of the character. We reasoned with them and they saw our point of view. So the film will only have a disclaimer in the beginning. No blurring of scenes or scrolls during the movie." 

== Critical reception ==
Don 2 had a positive-to-mixed critical reception in India and a mixed reception overseas. The film was praised for its action, direction and cinematography (with Shahrukh Khan receiving accolades for his performance), but criticised for its pace and music.

=== India ===
  gave it 3.5 out of 5 stars, praising its action.    also gave it 3.5 stars, saying: "Don 2 is a perfect entertainment package for this Christmas."    also gave the film three stars out of five, saying "Shah Rukh Khan makes Don 2 work."    gave Don 2 three out of five stars, adding: "Don 2 is smart, urbane, grey and good looking. It is also entertaining, even impressive...Though the   story is borrowed from here and there, its tail end is clever and skewed towards creating a wicked but cool legend."    gave it a mixed review, saying: "Don 2 is all about Shah Rukh Khan and he does not disappoint his fans either. Watch it for Khan and his daring stunts, specially the leap from the 37th floor." 

  gave Don 2 three out of five stars, saying: "The Badshah of bragging and the King of the hamming is the same person and hes out there in multiplexes waiting to con you off your money."    of the   gave the 3.5 out of 5 stars, saying that the original Don was better.  Mayank Shekhar of the Hindustan Times gave the film three out of five stars, agreeing that the first Don was better. 

=== International ===
  said, "If Mission: Impossible and Ocean’s Eleven had a bombastic, funny and slick cousin, it might be Don 2."    rated it on a par with "the best mindless entertainments the American film industry can crank out", saying: "If Hollywood needed any further proof that its days as the world’s premier entertainment manufacturer may be numbered, it need look no further than Don 2."    gave Don 2 a warm review, calling it a slick, twisting thriller and concluding that Khan fell enjoyably between overacting and subdued madness.  The Los Angeles Times called Don 2 an "intermittently enjoyable movie" which was far removed from reality. 

===Budget===
Don 2 was made with a production cost of   excluding SRKs acting fee with shares in profits which is  .

==Pre-release business==
{| class="wikitable sortable"  style="margin:auto; margin:auto;"
|+Don 2 Pre-release business http://www.koimoi.com/box-office/exclusive-economics-of-shah-rukh-khan-s-don-2/ 
! Territories and ancillary revenues
! Price
|-
|  Satellite rights with a TV channel
|  
|-
| worldwide distribution rights with Reliance entertainment
|  
|-
| Audio rights
| 
|-
| Subsidy from German government
| 
|- Total
| 
|-
|}
* The figures dont include the Print and Advertising (P&A) costs. 

== Box office ==

=== Domestic ===
On its first day, Don 2 had 80-percent occupancy levels throughout India. In multiplexes its occupancy level was 80–85 percent and 70–75 percent in single-screen theatres.  The film grossed   from its Hindi version, {{cite web
|url=http://www.boxofficeindia.com/arounddetail.php?page=shownews&articleid=4006&nCat=
|publisher=Box office India
|title=Top Opening Days ALL TIME
|date=6 February 2012
|accessdate=6 March 2012 highest opening-day fourth highest-grossing Bollywood film of 2011.

=== International ===
Don 2 was released overseas in two phases, with most major markets covered in the first phase. The film set several records on its opening day. It grossed $550,000 from 164 theatres in the United States. In the United Kingdom Don 2 s revenue debuted in the top ten, earning $527,000 in three days from 76 theatres; in the Middle East, the film grossed $930,000 in two days. It broke the record for highest single-day revenue in Australia (Australian dollar|$62,000), grossing $136,000 in Australia, New Zealand and Fiji during the first two days of its run.
 third highest-grossing Bollywood film overseas at that time, after My Name Is Khan and 3 Idiots.  
It face stiff competition with Mission Impossible Ghost protocol in its initial weeks which in some started topped the box office.
Satellite rights for Don 2 were sold to Zee TV for  , and it was also released on the Zee Cinema HD channel.

==  Awards ==

Don 2 won 14 awards from Bollywood award shows, receiving 19 additional nominations.

{| class="wikitable"  
|-
! Ceremony
! Category
! Recipient
! Result
|- 57th Filmfare Awards 
| Best Film
| Farhan Akhtar Ritesh Sidhwani
| rowspan="4"   
|-
| Best Director
| Farhan Akhtar
|-
| Best Actor
| Shahrukh Khan
|-
| Best Action
| Matthias Barsch
|-
| Best Sound Design
| Nakul Kamte
| rowspan="2"  
|-
| Best Performance in a Negative Role
| Boman Irani
|- 18th Colors Star Screen Awards  
| Best Actor (Popular)
| Shahrukh Khan
| rowspan="2"   
|-
| Favorite Onscreen Jodi
| Shahrukh Khan Priyanka Chopra
|-
| Best Special Effects
| Don 2
| rowspan="5"  
|-
| Best Action
| Matthias Barsch
|- 15th Zee Cine Awards 
| Best Film Popular
| Farhan Akhtar Ritesh Sidhwani
|-
| Best Director Popular
| Farhan Akhtar
|-
| Best Art Direction
| Don 2
|-
| Best Actor – Critics
| Shahrukh Khan
| rowspan="3"  
|-
| rowspan="2" | 18th Lions Gold Awards 
| Favorite Actor in a Leading Role (Male)
| Shahrukh Khan
|-
| Favorite Actor in a Leading Role (Female)
| Priyanka Chopra
|-
| rowspan="2" | Hindustan Times Readers Choice Awards
| Hindustan Times Readers Choice Best Entertainer (Male)
| Shahrukh Khan
| rowspan="2"  
|-
| Hindustan Times Readers Choice Best Entertainer (Female)
| Priyanka Chopra
|- 10th Stardust Awards 
| Film of the Year
| Don 2
| rowspan="7"   
|-
| Dream Director
| Farhan Akhtar
|-
| Best Actor in a Thriller/Action Film
| Shahrukh Khan
|-
| Best Actress in a Thriller/Action Film
| Priyanka Chopra
|-
| Star of the Year (Female)
| Priyanka Chopra
|- 3rd BIG Star Entertainment Awards 
| Best Actor Thriller – Male
| Shahrukh Khan
|-
| Best Actor Thriller – Female
| Priyanka Chopra
|-
| rowspan="4" | 1st ETC Bollywood Business Awards 
| Most Profitable Actor – Overseas
| Shahrukh Khan
| rowspan="4"   
|-
| Best First Look
| Don 2
|-
| Most Successful Corporate House
| Reliance Entertainment
|-
| Best Cinema Chain
| Reliance BIG Cinemas
|-
| rowspan="2" | 13th IIFA Awards 
http://zeenews.india.com/entertainment/iifa-2012/  IIFA Award Best Actor Shahrukh Khan
| rowspan="2"   
|- IIFA Award Best Actor in a Negative Role Boman Irani
|- National Media Network Film & TV Awards  Best Director  Farhan Akthar
| 
|}

== Sequel ==
About a sequel, Shahrukh Khan said: "It would be great to revisit Don after sometime. I’ve told Farhan that we could take 60 days out of our schedule and make a Don 3. But we need a script first, one that could take the last 10 minutes of Don 2 forward to another differently exciting thriller."  Farhan Akhtar also expressed a desire to take the story forward, and said he was fascinated by the character of Don. However, the filmmaker wanted to explore other genres without restriction and added that he was currently focusing on his acting career. 
In August 2014, It is reported that Farhan Akhtar is planned to play a role in the film. 

== Release ==
A two-disc DVD was released by Reliance Entertainment in April 2012, with special features on the second disc;  in June, Reliance released the film on Blu-ray Disc. 

== See also ==
*List of highest-grossing Bollywood films
*Don 2 (soundtrack)
*  

== References ==
 

== External links ==
 

*  
*  
*   at Bollywood Hungama
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 