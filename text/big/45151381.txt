Devudu
{{Infobox film
| name           = Devudu
| image          =
| caption        =
| writer         = G.Satyamurthy  
| story          = Janardhan Maharshi
| screenplay     = Ravi Raja Pinisetty
| producer       = A.Gopinath M.Venkatrao C.Krishna Rao
| director       = Ravi Raja Pinisetty
| starring       = Nandamuri Balakrishna Ramya Krishna Ruthika
| music          = Sirish
| cinematography = V. S. R. Swamy
| editing        = Shiva Krishna Murthy
| studio         = Sri Chitra Creations
| released       =  
| runtime        = 157 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}

Devudu ( ) is a 1997 Telugu cinema|Telugu, drama film produced by A.Gopinath, M.Venkatrao & C.Krishna Rao on Sri Chitra Creations banner, directed by Ravi Raja Pinisetty. Starring Nandamuri Balakrishna, Ramya Krishna, Ruthika in the lead roles and music composed by Sirish.   

==Cast==
 
*Nandamuri Balakrishna as Devudu
*Ramya Krishna as Shanti
*Ruthika as Madhavi
*Satyanarayana 
*Allu Ramalingaiyah 
*Pundarikakshiyah 
*Kota Srinivasa Rao 
*Raja Krishna Murthy 
*Gajar Khan
*Maharshi Raghava
*Sivaji Raja
*Narayana Rao Annapurna
*Rama Prabha
*Sana 
*Raja Kumari
*Y.Vijaya  
 

==Soundtrack==
{{Infobox album
| Name        = Devudu
| Tagline     = 
| Type        = film
| Artist      = Sirish
| Cover       = 
| Released    = 1997
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:45
| Label       = T-Series
| Producer    = Sirish
| Reviews     =
| Last album  =   
| This album  = 
| Next album  = 
}}

Music composed by Sirish. Music released on T-Series Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:45
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ra Chilaka Kulukula  Sirivennela Sitarama Sastry
| extra1  = Mano (singer)|Mano, K. S. Chithra|Chitra, Anuradha Sriram
| length1 = 4:40

| title2  = Tanantu Nanu
| lyrics2 = Sirivennela Sitarama Sastry
| extra2  = Mano,Chitra
| length2 = 5:13

| title3  = Made in India
| lyrics3 = Bhuvanachandra
| extra3  = Mano,Chitra,Gopika Poornima
| length3 = 4:52

| title4  = Apaka Chusina
| lyrics4 = Sirivennela Sitarama Sastry SP Balu,Chitra
| length4 = 4:54

| title5  = Gullo Ramayo
| lyrics5 = Sirivennela Sitarama Sastry 
| extra5  = Mano,Chitra
| length5 = 4:50

| title6  = Ra Ro Ranganna  
| lyrics6 = Sirivennela Sitarama Sastry   Sujatha
| length6 = 4:16
}}

==Others==
* VCDs and DVDs on - VOLGA Videos, Hyderabad

==References==
 

 

 
 
 


 