High School Musical: O Desafio
{{Infobox film
| name           =High School Musical: O Desafio 
| image          = O Desafio.jpg
| border         = yes
| caption        = High School Musical: O Desafio poster
| director       = César Rodrigues
| writer         = Susana Cardoso Carol Castro
| starring       = Olavo Cavalheiro Renata Ferreira Paula Barbosa Fellipe Guadanucci Wanessa
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| gross          = $1,149,290
}} Battle of the Bands. It began filming in April 2009 and it is the third Disney-branded feature film made in Latin America. However, the Brazilian version does not follow the caricatural style seen in the Argentinian and Mexican productions.
The movie has a script created specifically for the cinema and an original plotline.  It is a remake of High School Musical with the Brazilian versions of characters Troy, Gabriella, Ryan, Sharpay, Chad, Taylor and Kelsie.

It was filmed in Rio de Janeiro and Olodum, the cultural group from Bahia, was invited to bring its unique rhythm to a special choreography number.

The roles in the movie are played by the winners and finalists of the Brazilian TV program  : Olavo, Renata, Fellipe, Paula, Moroni, Beatriz, Samuel and Karol. It also has the special participation of Wanessa Camargo.

== Plot ==
=== 1ª part ===
A new school year begins at the High School Brasil (HSB), and the students return from the summer vacations. Olavo, the captain of the school futsal team, the Lobos-Guará, discovers that Renata, his neighbor and classmate, has changed a lot over the summer. Paula, however, continues being vain and wastes her time dominating her poor brother, Fellipe, and her associates Alícia, Clara and Karol, or, as she prefers to call them, "The Invisibles".
=== 2ª part ===
The principal of the school and Ms. Márcia, the art teacher, invite the students to take part in the schools first battle of the bands, where the kids will have a chance to be showcased as true music stars. Wanessa, a former student and now a famous singer, comes to the school as adviser to the contest.
=== 3ª part ===
Working against the clock and with limited resources, the kids put all the forces for the big day. Olavo and Renata, together with their friends Moroni, Bia, Samuel, Fábio and Ed, as well as Fellipe, participate in the contest, forming a band named The Tribe. At the same time, Paula participates with her friends, and she tries the impossible task of separating Fellipe from his new friends. But only one band will be the winner, the one which can understand that teamwork, personal development, and study will make them better artists and also better people.

==Cast==
* Olavo (Olavo Cavalheiro) is the male protagonist of the movie. He is the most popular male student at High School Brazil, and the captain of the Futsal team, the Lobos-Guará. He faces a new challenge this year in the school: forming a band for the Battle of the Bands, where he will show, despite of the difficulties, his true leadership.
* Renata (Renata Ferreira) is the female protagonist of the movie. She is the shy and studious student who, under the astonished eyes of all her peers, suddenly becomes an attractive young girl with a talent for singing. When she feels insecure, Wanessa encourages her to be herself and exhibit her artistic talents without fears.
* Paula (Paula Barbosa) is the antagonist of the film. She is the typical "rich girl", vain, selfish, who does not spare a second for the others, even for her brother Fellipe, except to get what she wants: to be the absolute and undisputed star of the school. But at the end, she learns a valuable lesson and redeems herself.
* Felipe (Fellipe Guadanucci) is Paulas brother, who must use all his ingenuity to evade the surveillance of his sister to be the coach of Olavos band. With Renatas help, and with his perseverance and recklessness, Fellipe manages to become independent and show his true artistic abilities.
* Moroni (Moroni Cruz) is Olavos friend and he does his best to help the Lobos-Guará team to win the state competition.
* Samuel (Samuel Nascimento) is Olavos other friend, who has a deep platonic love for Wanessa. When there is a funny comment or sudden laughter, that is Samuel, the guy who never loses his sense of humor.
* Bia (Beatriz Machado) is the talented composer of the band. She also demonstrates her positive attitude when time is their principal enemy.
* Karol (Karol Cândido) is a great singer, but because she joins Paulas musical group, she is always overshadowed by the "star".
* Wanessa (Wanessa Camargo) is the adviser of the contest, is a former HSB student and now a famous singer.
* Teacher Márcia (Débora Olivieri) is HSBs art teacher, who, together with the principal, convenes the Battle of the Bands.
* The High School Principal (Cláudio Torres Gonzaga)
* Renatas Mon (Tereza Seiblitz)
* Olavos Dad (Tadeu Aguiar)
* Paulas and Felipes father (Herbert Richers Jr)
* Paulas and Felipes mother (Carolina Ilana Kaplan)
* Clara and Alícia (Gisele Batista and Michelle Batista) are Paulas unconditional allies and members of her band. The two girls are completely subordinated to the whims and arrogance of the "star".
*Fábio (Fábio Enriquez) is a member of the Lobos-Guará team.
*Ed (Eduardo Lamdim) is the goalkeeper of the Lobos-Guará team.

===List of songs===
{| class="wikitable" border="1"
|-
! # || Song || Performer(s)
|- bgcolor=#FFF0F5"
| 1 || Novo Ano Começou (El Verano Terminó)   || Renata, Olavo, Beatriz,  Fellipe, Paula, Moroni, Karol & Samuel
|- bgcolor="efefef"
| 2 || Tudo Está Melhor (Arpoador) (La Vida Es Una Aventura)  || Olavo
|- bgcolor=#FFF0F5"
| 3 || Conselho de Amiga || Renata & Wanessa
|- bgcolor="efefef"
| 4 || Futebol  || Olavo
|- bgcolor=#FFF0F5"
| 5 || Matemática  || Renata & Fellipe
|- bgcolor="efefef"
| 6 || Que Papel é Esse?   || Paula & Fellipe
|- bgcolor=#FFF0F5"
| 7 || A Procura do Sol (A Buscar El Sol)  || Renata & Beatriz 
|- bgcolor="efefef"
| 8 || Romeu e Julieta (Yo Sabía) || Olavo & Renata
|- bgcolor=#FFF0F5"
| 9 || Tamanho Não É Documento || Camila Caputi (Marta and the Volleyball Players)
|- bgcolor=#FFF0F5"
| 10 || Eu Sou Única (Paula E As Invisíveis) (Superstar)  || Paula e Karol (Paula and the Invisibles)
|- bgcolor=#FFF0F5"
| 11 || Faça Parte Desse Show  || Renata, Olavo, Beatriz,  Fellipe, Moroni & Samuel (The Tribe)
|- bgcolor=#FFF0F5"
| 12 || Atuar, Dançar, Cantar (Actuar, Bailar, Cantar)  || All
|- bgcolor=#FFF0F5"
| 13 || Gosto Tanto || Wanessa
|- bgcolor=#FFF0F5"
|}

There are songs in the film that werent included in the official soundtrack above.

==External links==
*  

 

 
 
 
 
 
 