Balls (film)
{{Infobox film
| name           = Balls
| image          = Balls film.jpg
| alt            = 
| caption        = 
| director       = Josef Fares
| producer       = Anna Anthony
| writer         = Josef Fares Torkel Petersson
| screenplay     = 
| story          = 
| narrator       =  Juan Rodriguez Anita Wall Jessica Forsberg
| music          = 
| cinematography = Linus Eklund
| editing        = 
| studio         = 
| distributor    = AB Svensk Filmindustri
| released       =  
| runtime        = 98 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Balls (Swedish: Farsan) is a 2010 Swedish comedy film that was directed by Josef Fares.

==Cast==
*Jan Fares as Aziz (farsan)
*Torkel Petersson as Jörgen
*Hamadi Khemiri as Sami
*Nina Zanjani as Amanda Juan Rodriguez as Juan
*Anita Wall as Edith
*Jessica Forsberg as Lotta

==Reception==
 
*Aftonbladet - 3/5 
*Expressen - 2/5 
*Dagens Nyheter - 3/5 
*Upsala Nya Tidning - 4/5 
*Svenska Dagbladet 2/6 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 

 
 