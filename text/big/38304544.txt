Six Cylinder Love (1931 film)
 
{{infobox film
| title          = Six Cylinder Love
| image          =
| caption        =
| director       = Thornton Freeland William Fox John W. Considine, Jr. (associate producer)
| writer         = William Anthony McGuire(play) William Conselman(adaptation),br>Norman Houston(adaptation
| starring       = Spencer Tracy Sidney Fox Edward Everett Horton Ernest Palmer
| editing        = J. Edwin Robbins
| distributor    = Fox Film Corporation
| released       = May 10, 1931
| runtime        = 71 minutes
| country        = USA
| language       = English
| gross = $327,000 James Curtis, Spencer Tracy: A Biography, Alfred Knopf, 2011 p. 161  }}

Six Cylinder Love is a 1931 comedy film starring Spencer Tracy and Edward Everett Horton, and directed by Thornton Freeland. It was produced and distributed by Fox Film Corporation and is a remake of their 1923 original. Both films are based on the 1921 Broadway play.   It recorded a loss of $25,000. 

==Cast==
* Spencer Tracy - William Donroy
* Edward Everett Horton - Monty Winston
* Sidney Fox - Marilyn Sterling
* Lorin Raker - Gilbert Sterling
* William Collier, Sr. - Richard Burton
* Una Merkel - Margaret Rogers
* Bert Roach - Harold Rogers
* Ruth Warren - Mrs. Burton
* El Brendel - Axel

==References==
 

==External links==
*   in the Internet Movie Database
* 

 

 
 
 
 
 
 
 
 
 


 