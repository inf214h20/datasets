I Love a Mystery (film)
{{Infobox film
| name           = I Love a Mystery
| image size     =
| image	=	I Love a Mystery FilmPoster.jpeg
| alt            =
| caption        =
| director       = Henry Levin
| producer       = Wallace MacDonald
| writer         = Carlton E. Morse (radio series) Charles ONeal (writer)
| narrator       =
| starring       = George Macready Jim Bannon Nina Foch
| music          = Mario Castelnuovo-Tedesco
| cinematography = Burnett Guffey
| editing        = Aaron Stell
| studio         =
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 69 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}} radio serial of the same name, I Love a Mystery was the first of three Columbia "B" pictures inspired by the radio series and the only one actually based on a script written by Morse for the radio series. The Devils Mask and The Unknown (1946 film)| The Unknown followed in 1946.  

==Plot==
Two detectives, Jim Packard (Bannon) and Doc Long (Yarborough), make the acquaintance of Jefferson Monk (Macready) in a nightclub they frequent. When a flaming dessert is nearly spilled onto the trio, Monk informs them that it was meant for him. He explains that, according to a prophesy, he is to die in three days. Upon learning their profession, he hires them to protect him, particularly from a  man with a peg leg and a valise, the latter (he suspects) to transport his severed head to the ancient secret society of the Barokan. The leader of the society, Mr. Gee (Matthews), has offered to buy Monks head (after he is dead) because he looks exactly like the ancient mummified founder of his order. When the two trail Monk and his woman companion, Jean Anderson (Mathews), out into the night, the one-legged man does approach, but eludes their efforts to catch him.

As Packard delves into the mystery, he comes to suspect that someone is trying to drive Monk into committing suicide after he learns that Monks two million dollar inheritance will go to charity if he divorces his wheelchair-bound wife Ellen (Foch). Along with Ellen, the suspects include her physician, Dr. Han (an uncredited Gregory Gaye); her nurse (Isabel Withers, uncredited); her lover, Justin Reeves; Jean; and the minions of the society.

Then the one-legged man has his throat cut. He is identified as Jeans father. Jean and Reeves soon suffer the same fate.

It turns out that all of Monks acquaintances, led by his wife, are plotting his death. However, he found out by bribing Dr. Han and started killing them off one by one. Fearing that Packard and Long will eventually figure out the identity of the murderer, Monk tries to dispatch them as well, but they manage to foil his scheme. Unmasked, Monk flees in his car, only to have a crash result in his decapitation.

==Cast==
* Jim Bannon as Jack Packard
* Nina Foch as Ellen Monk
* George Macready as Jefferson Monk
* Barton Yarborough as Doc Long
* Carole Mathews as Jean Anderson
* Lester Matthews as Justin Reeves / Mr. Gee

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 