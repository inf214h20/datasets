The Weavers (1905 film)
 
 .]] Aromanian village Ottoman vilayet spinning and weaving.    It was originally called "Our 114 year old grandmother at work weaving", but has come to be known as The Weavers. 

It is said to be the first film shot in the Ottoman Balkans.   (Mk.) 

The film was shot with 35 mm film with an Urban Bioscope movie camera (serial number 300) imported from London. 

==Appropriation==

An extract from the film appears at the beginning of Theo Angelopouloss 1995 film Ulysses Gaze.

==References==
 

==Bibliography==
* Greece in modern times: an annotated bibliography of works published in English in twenty-two academic disciplines during the twentieth century, 1:109
* Katerina Zacharia, "Reel Hellenisms: Perceptions of Greece in Greek Cinema" in Katerina Zacharia, Hellenisms, p.&nbsp;323

 
 
 
   
 
 


 