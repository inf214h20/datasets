Tin Pan Alley Cats
{{Infobox Hollywood cartoon|
| cartoon_name = Tin Pan Alley Cats
| series = Merrie Melodies
| image = caption =
| director = Bob Clampett
| story_artist = Warren Foster
| animator = Rod Scribner 
| voice_actors = Fats Waller(uncreaited) musician = Carl W. Stalling 
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. Pictures   The Vitaphone Corporation
| release_date =  July 17, 1943 (USA)
| color_process = Technicolor
| runtime = 7 min
| movie_language = English
}} Leon Schlesinger Productions as part of Warner Bros. Merrie Melodies series. A follow-up to Clampetts successful Coal Black and de Sebben Dwarfs, released earlier in 1943, Tin Pan Alley Cats focuses upon contemporary themes of African-American culture, jazz music, and World War II, and features a caricature of jazz musician Fats Waller as an anthropomorphic cat. The shorts centerpiece is a fantasy sequence derived from Clampetts black and white Looney Tunes short Porky in Wackyland (1938).

Like Coal Black, Tin Pan Alley Cats focuses heavily on stereotypical gags, character designs, and situations involving African-Americans. As such, the film and other Warner Bros. cartoons with similar themes have been withheld from television distribution since 1968, and are collectively known as the Censored Eleven.

==Plot== manic fantasy realm filled with surreal imagery (including caricatures of Adolf Hitler, Hideki Tojo and Joseph Stalin). This world frightens him so much that, when he wakes up, he gives up his partying ways and joins the religious music group singing outside, much to their surprise.

== Production ==

In part because of budget limitations and wartime shortages, several sequences borrow animation and audio recordings from earlier Schlesinger cartoons. From Friz Frelengs 1937 "products come to life" Merrie Melodies short, September In The Rain, the recorded performance of "Nagasaki (song)|Nagasaki" is re-used completely intact, and the "Fats Waller" cat, "Louis Armstrong" trumpeter, jitterbugging woman and the trio of singing bartenders are re-purposed for this cartoon. Gags from the "out-of-this-world" sequence feature color-redrawn versions of characters and visuals (along with re-recorded audio segments) from Clampetts Porky in Wackyland.

Segments specifically created for the nightmare sequence (such as the "Rubber (musical) Band" made up of rubber bands) would resurface in Friz Frelengs 1949 color remake of Porky In Wackyland, Dough for the Do-Do.

This short premiered 5 months before the death of Fats Waller in December 1943.

== Home video and television availability ==
Following the African-American Civil Rights Movement (1955–1968)|African-American Civil Rights Movement of the 1960s, United Artists withheld Tin Pan Alley Cats, along with the rest of the "Censored Eleven", from American television in 1968. Turner Entertainment (today owned by Time Warner) acquired the rights to these cartoons in 1986, and has continued to withhold it from release.
 Snow White and the Seven Dwarfs, is frequently included on lists of the greatest cartoons ever made, while the latter is a hot jazz re-interpretation of Clampetts now-classic 1938 short Porky in Wackyland. Author Michelle Klein-Hass wrote the following:
{{cquote|. . . some even look at Clampetts Jazz cartoons and cry racism when Clampett was incredibly ahead of his time and was a friend to many of the greats of the LA jazz scene. All of the faces you see in Tin Pan Alley Cats and Coal Black and de Sebben Dwarfs are caricatures of real musicians he hung out with at the Central Avenue jazz and blues clubs of the 40s. He insisted that some of these musicians be in on the recording of the soundtracks for these two cartoons. {{cite newsgroup |title=Re: R.I.P. Chuck Jones - 1912-2002
|author=Michelle Klein-Hass |date=2002-02-24 |newsgroup=rec.arts.animation |message-id=msgeek-2402020908000001@192.168.2.93 |url=http://groups.google.com/group/rec.arts.animation/msg/d67c97d842389289?hl=en&dmode=source |accessdate= - - }} }}

Bootleg copies have surfaced on videotape and DVD, and are frequently added to (and - due to copyright infringement - subsequently removed from) sites such as   DVD collectors set. In October 2010, it was announced that a complete version will be officially released, along with the rest of the "Censored 11", on DVD through the Warner Archives collection. 

==Notes==
 

==External links==
*  
*  
 
 
 
 
 
 
 
 