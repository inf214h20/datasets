School Ties
 
{{Infobox film
| name         = School Ties
| image        = School ties.jpg
| caption      = Theatrical release poster 
| screenplay   = Darryl Ponicsan Dick Wolf
| story        = Dick Wolf
| starring     = Brendan Fraser Matt Damon Chris ODonnell Ben Affleck Cole Hauser
| director     = Robert Mandel
| music        = Maurice Jarre
| distributor  = Paramount Pictures
| released     = September 18, 1992
| runtime      = 106 minutes
| language     = English
| movie_series =
| awards       =
| producer     = Stanley R. Jaffe   Sherry Lansing
| budget       =
| gross        = $14,715,067 (USA)
}} 1992 sports film directed by Robert Mandel starring Brendan Fraser, Matt Damon, Ben Affleck, Chris ODonnell, Cole Hauser, Randall Batinkoff, Andrew Lowery and Anthony Rapp.  Fraser plays the lead role as David Greene, a Jewish high school student who is awarded an athletic scholarship to an elite preparatory school in his senior year.

==Plot== Jewish teenager prejudiced against Jews, he suppresses his background.

David becomes the team hero and wins the attentions of beautiful débutante Sally Wheeler (Amy Locane), whom Dillon claims is his girlfriend. In the afterglow of a victory over the schools chief rival St. Lukes, Dillon inadvertently discovers that David is Jewish and, out of jealousy, makes this widely known, causing Sally and his teammates to turn against David—soon after, he finds a sign above his bed bearing a swastika and the words "Go home Jew". David is constantly harassed by his classmates, led by Richard "McGoo" Collins (Anthony Rapp) and his body guard-like roommate Chesty Smith (Ben Affleck); only Reece and another unnamed student remain loyal to Greene.
 Michael Higgins), discovers it, he informs the class that he will fail all of them if the cheater does not confess. He leaves the task of finding the cheater up to the students, led by Van Kelt, the head prefect.

When David confronts Dillon and threatens to turn him in if he does not confess, Dillon unsuccessfully attempts to buy Davids silence with money. Just when David is about to reveal Dillon to the other students, Dillon accuses David. Both agree to leave and to trust the rest of the class to decide who is telling the truth. The majority of the class blame David out of antisemitic prejudice, while Reece, the unnamed student and Connors, going against his own self-professed antisemitism, argue that it is unlike David to cheat or be dishonest. The class votes that David is guilty, prompting Van Kelt to tell him to report to the elitist headmaster, Dr. Bartram (Peter Donat), to confess to cheating.

David goes to Bartrams office and says that he was the cheater, but unbeknown to him Van Kelt has already told the headmaster that the real offender was Dillon. Bartram tells David and Van Kelt that they should have reported the offense, but absolves them; Dillon, meanwhile, is expelled. As David leaves the headmasters office, he sees Dillon leaving the school. Dillon says that he will be accepted to Harvard anyway and that years later everybody will have forgotten about his cheating at school, while David will still just be a Jew. "And youll still be a prick," David replies, and walks away.

==Cast==
* Brendan Fraser as David Greene
* Matt Damon as Charlie Dillon
* Chris ODonnell as Chris Reece
* Randall Batinkoff as Rip van Kelt, head prefect
* Cole Hauser as Jack Connors
* Andrew Lowery as "Mack" McGivern
* Ben Affleck as Chesty Smith
* Anthony Rapp as Richard "McGoo" Collins
* Amy Locane as Sally Wheeler
* Peter Donat as Headmaster Dr. Bartram
* Željko Ivanek as Mr. Cleary, French language teacher
* Kevin Tighe as Coach McDevitt, American football coach Michael Higgins as Mr. Gierasch, history teacher
* Ed Lauter as Alan Greene, Davids father
* Peter McRobbie as Chaplain

==Filming==
 
The scene at the bus depot in Scranton, Pennsylvania, was filmed at a liquor store in Leominster, Massachusetts. The scene shot at Skips Blue Moon Diner was filmed in downtown Gardner, Massachusetts. Most of the movie was filmed on location at Middlesex School in Concord, Massachusetts. In addition, Groton School, Worcester Academy, Lawrence Academy at Groton and St. Marks School (Massachusetts)|St. Marks School (all area prep schools) were also involved in the filming. Opening scenes are of the south and west sides of Wyandotte Street (Route 378 heading north), the Bethlehem Steel Plant and Zion Lutheran Church from the top of the graveyard looking northwest to 4th Street in Bethlehem, Pennsylvania. The scene in the opening credits in front of Danas Luncheonette and some scenes inside were filmed in Lowell, Massachusetts. 

==Reception==
The film received mixed to positive reviews. The film has a 68% rating on Rotten Tomatoes based on 37 reviews.  Roger Ebert found it "surprisingly effective",  whereas Janet Maslin found it followed a "predictable path". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 