Veruthe Oru Bharya
{{Infobox film
| name           = Veruthe Oru Bharya
| image          = Veruthe Oru Bharya.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Cover art of Veruthe Oru Bharya
| film name      = 
| director       = Akku Akbar
| producer       = Salahudeen
| writer         = K. Gireesh Kumar
| screenplay     = K. Gireesh Kumar
| story          = K. Gireesh Kumar
| based on       =  
| narrator       =  Innocent
| music          = Shyam Dharman
| cinematography = Shaji
| editing        = Ranjan Abraham
| studio         = Cinema Kottaka
| distributor    = Pyramid Saimira
| released       = 2008 
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = N/A
| gross          = N/A
}}

Veruthe Oru Bharya (  written by K. Gireesh Kumar and directed by Akku Akbar. The film stars Jayaram, Gopika and Nivetha Thomas in the lead roles. 

The film was an "unexpected success" in the box office and gave a fillip to the careers of both director Akku Akbar and veteran actor Jayaram.  

Nivetha Thomas received Kerala State Film Award for Best Child Artist (Female, 2008) for her performance in the film. 

Bharya Athra Pora (2013), written by K. Gireesh Kumar and directed by Akku Akbar, is not a sequel to the film.  

==Plot==
Sugunan (Jayaram) is a stereotypical Malayali husband who habitually underestimates his wife Bindu (Gopika) in managing a family. Sugunan wants his wife only to keep house - cook food for him, iron his clothes and clean the house. Bindu does all these and more, but theres never a word of appreciation or a compliment from her husband. He doesnt even acknowledge the work that she is doing without any rest in her life. This often results in quarrels between them, but like any docile Indian wife, Bindu tolerates it for some time.

Sugunan is also indifferent (and possibly hostile) to Bindus family - even though her father makes many attempts to get close to him. Bindu is upset at his behaviour. When he has the electricity supply to her fathers house shut down since they were drawing power from the electrical grid, during her brothers wedding, Bindu finally sees him for the tyrannical man he is.

Later, during a trip to Kodaikanal, Sugunan doesnt "allow" Bindu to indulge in normal tourist activities like train rides etc. Sugunans indifference when Bindus mother dies drives home the point that she is just a non entity in her husbands life. Once Sugunan slaps her in a fit of rage and she decides enough is enough and goes back to her house. 

A boy persuades Sugunans teenage daughter Anjana (Nivetha Thomas) to go for a drive with him, and the jeep breaks down. Some thugs chance upon the hapless girl and try to molest her. The police arrive and rescue the two youngsters. Alarmed by the situation, Sugunan and Bindu are reunited and live happily ever after.

==Cast==
*   )
* Gopika: Bindu Sugunan (the unemployed wife of Mr. Sugunan)
* Nivetha Thomas: Anjana Sugunan (the 14-year-old daughter of the Sugunans )
* Innocent (actor)|Innocent:  Bindus father
* Suraj Venjaramoodu: Ali
* Madhu Warrier: Bindus brother Rahman (cameo appearance)
* Jaffar Idukki
* Sivaji Guruvayoor
* Kalabhavan Prajod
* KB Ganesh Kumar
* Sona Nair
* Mini Arun
* Sarayu as Betty
* Mahima
* Ambika Mohan as Headmistress

==Production==
The film was mainly shot at various locations in Thodupuzha.
== Reception ==
The film was a major box-office success. 

==External links==
 

==References==
 
 

 
 
 
 