Agni Muhurtham
{{Infobox film 
| name           = Agni Muhurtham
| image          =
| caption        =
| director       = Soman
| producer       = Soman
| writer         = KB Saseendran & Soman Mosses (dialogues)
| screenplay     = Mosses Urvashi Ratheesh Santhosh Mala Aravindan
| music          = S. P. Venkatesh
| cinematography = J Williams
| editing        = K Sankunni
| studio         = Navayuga Films
| distributor    = Navayuga Films
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Santhosh and Mala Aravindan in lead roles. The film had musical score by S. P. Venkatesh.   

==Cast== Urvashi
*Ratheesh Santhosh
*Mala Aravindan
*Soorya

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Balu Kiriyath. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arayannathooval || KS Chithra || Balu Kiriyath || 
|-
| 2 || Manjaninja Maamalayil || Unni Menon, Baby Sangeetha || Balu Kiriyath || 
|-
| 3 || Manjaninja Maamalayil || KS Chithra, Unni Menon || Balu Kiriyath || 
|-
| 4 || Manjaniyum   || KS Chithra || Balu Kiriyath || 
|}

==References==
 

==External links==
*  

 
 
 

 