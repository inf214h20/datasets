Pin (film)
{{Infobox Film 
| name           = Pin 
| image          = 
| director       = Sandor Stern
| writer         = Sandor Stern
| producer       = {{plainlist|
* Pierre David
* Rene Malo
}}
| based on       =  
| starring       = {{plainlist|
* David Hewlett
* Cynthia Preston
* Terry OQuinn
}}
| cinematography = Guy Dufaux
| editing        = Patrick Dodd
| music          = Peter Manning Robinson
| released       =  
| studio         = {{plainlist|
* Image Organization
* Lance Entertainment
* Malofilm
* Telefilm Canada
}}
| distributor    = New World Pictures
| runtime        = 102 minutes
| country        = Canada
| language       = English
}}
Pin is a 1988 Canadian thriller film starring David Hewlett, Cynthia Preston and Terry OQuinn, directed by Sandor Stern.  The film was released in Canada with the title Pin, A Plastic Nightmare. It was released direct-to-video in the USA on January 27, 1989. The running time is 102 minutes. It is based on the novel of the same name by Andrew Neiderman.

==Plot==
Dr. Frank Linden has a life-size, anatomically correct medical dummy in his office which he calls "Pin". Via ventriloquism, Dr. Linden uses Pin to teach his children, Leon and Ursula about bodily functions and how the body works in a way the children can relate to without it being awkward. Dr. Lindens interactions with the children are otherwise cold and emotionally distant, and his ventriloquism act is the only sign of a more warm and playful side to his nature. Unknown to Dr. Linden, Leon is mentally ill and has come to believe that Pin is alive. Due in part to his mother, who discourages Leon from playing outdoors or bringing anyone home, Leon has no real friends and sees Pin as closest analogue. Leon is further traumatized when he secretly witnesses his fathers nurse use Pin as a sex toy.

When Leon turns eighteen, Dr. Linden, having come back to retrieve case studies for a speech, catches him having a conversation with Pin (via ventriloquism, which Leon had learned). Realizing the extent of Leons psychosis and that his son is mentally ill, Dr. Linden takes Pin away to use as a visual aid for a speech with the intention of leaving Pin at the medical school. As Dr. and Mrs. Linden speed to the hall, they get into a car crash caused by either Dr. Lindens recklessness or Pin; the Lindens are both killed instantly. Later, as Ursula sits in the back of a police car, crying, Leon secretly retrieves Pin from the scene.

Leon and Ursula, though grieving and orphaned, enjoy their newfound freedom until Mrs. Lindens sister, Aunt Dorothy, moves in. She encourages Ursula to take a job at the library, which Leon is against. Believing that she is influencing Ursula and after talking it over with Pin, Leon causes Aunt Dorothy to die from a heart attack by using Pin to frighten her. However, Ursula continues to work at the library, where she meets handsome athlete Stan Fraker and falls in love. Meanwhile, Leon takes his fixation with Pin to pathological extremes, first by dressing him in Dr. Lindens clothes and finally fitting him with latex skin and a wig.

Leon believes that Stan is only interested in Ursulas inheritance and that he wants to put Leon in a sanitarium. He invites Stan over under the guise of discussing a surprise birthday party for Ursula. Leon drugs Stans drink, and when Stan fights back, Leon bludgeons Stan with a wooden sculpture. Following Pins instructions, he puts Stan in a bag and plans to dump him in the river. Leon is interrupted by a call from Ursula, who says she intends to come home early. Leon quickly hides Stans body in a woodpile outside the house and cleans up the blood.

To calm her, Leon tells Ursula that Stan is visiting a sick friend out of town; she believes him until she discovers a gift she gave Stan and a wet spot on the carpet. When she confronts Leon, he blames it on Pin, which causes her to run out of the house in hysterics. Leon asks Pin why he would not help him. Pin states that he has never lied to or for him and that Leons motives were selfish. Ursula returns with an axe, which she raises ready to strike; the screen goes white as Leon screams and cowers.

The police find Stans body; to their amazement, he is still alive. Some time later, Ursula and Stan return to the house to visit Pin. Ursula tells him that shes going on a trip with Stan. Pin inquires as to whether shes heard from Leon. Ursula replies "No." Pin says that he misses him a great deal. Ursula agrees, it is revealed that she is talking to Leon, who has taken Pins persona. When the dummy was destroyed, Leon had a psychotic break, which left only the Pin side of his personality to completely take over.

==Cast==
* David Hewlett as Leon Linden
* Cynthia Preston as Ursula Linden
* Terry OQuinn as Dr. Frank Linden
* Bronwen Mantel as Mrs. Linden
* John Pyper-Ferguson as Stan Fraker
* Jonathan Banks as Pin (voice)

== Production ==
The film was shot in Montreal, Quebec, Canada, in 1987. 

== Release ==
Pin was released on VHS on May 28, 1989,  and DVD on April 24, 2001, in Widescreen Anamorphic.  The DVD has commentary by director Sandor Stern and journalist Ted Newsom.

== Reception ==
Janet Maslin of The New York Times called it "a cool, bloodless, well-made thriller with a taste for the quietly bizarre."     Andrew Marshall of Starburst (magazine)|Starburst rated it 9/10 stars and wrote, "A low-key psychological horror produced at a time when the genre was swamped with interminable sagas of invincible otherworldly serial killers, Pin is subtle, disturbing, and brilliant."   

== Legacy ==
Pin was featured in Fangoria magazines 101 Best Horror Movies Youve Never Seen.   It has since become a cult film,  and a remake, to be directed by Stern, was announced in 2011. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 