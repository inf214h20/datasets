The Crew (2000 film)
{{Infobox film
| name           = The Crew
| image          = Crew poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Michael Dinner
| producer       = Barry Sonnenfeld
| writer         = Barry Fanaro
| narrator       = Richard Dreyfuss
| starring       = Burt Reynolds Seymour Cassel Richard Dreyfuss Dan Hedaya Carrie-Anne Moss Jeremy Piven Jennifer Tilly
| music          = Steve Bartek
| cinematography = Juan Ruiz Anchía
| editing        = Nicholas C. Smith
| studio         = Touchstone Pictures Buena Vista Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $23 million
| gross          = $13 million
}}
The Crew is a 2000 black comedy crime film, directed by Michael Dinner and starring Burt Reynolds, Seymour Cassel, Richard Dreyfuss, Dan Hedaya and Jennifer Tilly. Barry Sonnenfeld was one of the films producers.

==Plot==
Four retired mobsters (Richard Dreyfuss, Burt Reynolds, Dan Hedaya and Seymour Cassel) plan one last crime to save their apartment at a retirement home (the owners are forcing them out with a rent increase so that the apartments can be rented to young, affluent South Beach couples).  The four steal a corpse from the mortuary to use as the "victim" in a staged murder scene.  Unknown to them, the body belonged to the head of a Colombian drug smuggling ring. As a result of the "murder", many of the young renters leave and the four men are given cash and a rent discount by the complex to keep living there.  Much of this money is spent on high living and women, which causes a young woman (Jennifer Tilly) to discover that the four men staged the murder - while spending time with the normally-silent "Mouth," he reveals that his mouth is loosened by intimacy with women.

To keep the woman quiet, the four agree to kill her stepmother (Lainie Kazan), but instead kidnap her and fake her death by setting fire to her mansion.  In the process, they accidentally burn down the house of the drug-smugglers son (Miguel Sandoval), who happened to live in a nearby mansion.  Believing that someone is trying to usurp his power, the drug lord offers $100,000 to anyone who brings him the head of the man responsible.  This results in a confrontation at the apartment, leading to the capture of a female police officer and her partner (Carrie-Anne Moss and Jeremy Piven), one of the wiseguys (Seymour Cassel), the young woman, and the stepmother. The other wiseguys manage to escape this conflict, and track down the men who kidnapped their friend.
 Cuban cigars is taken by the men and used to make their apartment complex into "a retirement home for old wiseguys who are down on their luck."

A sub-plot of the movie involves Richard Dreyfuss search for his long lost daughter (Carrie-Anne Moss), whom he hasnt seen since he was in his 30s and she was a small child.

==Cast==
*Richard Dreyfuss as Bobby Bartellemeo 
*Burt Reynolds as Joey "Bats" Pistella 
*Dan Hedaya as Mike "The Brick" Donatelli 
*Seymour Cassel as Tony "Mouth" Donato 
*Carrie-Anne Moss as Detective Olivia Neal 
*Jeremy Piven as Detective Steve Menteer

*Jennifer Tilly as Maureen "Ferris" Lowenstein 
*Lainie Kazan as Pepper Lowenstein 
*Miguel Sandoval as Raul Ventana 
*Casey Siemaszko as Young Bobby 
*Matthew Borlenghi as Young Joey "Bats"  
*Billy Jayne as Young Tony "Mouth" 
*Jeremy Ratchford as Young Mike "The Brick"
*Samantha Kurzman as Young Olivia
*Jose Zuniga as Escobar
*Mike Moroff as Jorge  Carlos Gomez as Miguel 
*Louis Lombardi as Jimmy Whistles 
*Frank Vincent as Marty

==Reaction==
The Crew received negative reviews upon release, and was mostly noted for its similarities to Space Cowboys, which also involved four retirees who return for one last job (in that case, to go back into space). The film holds a 20% rating on Rotten Tomatoes based on 83 reviews. It was a box office flop, grossing only US$13 million off an estimated $23 million budget.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 