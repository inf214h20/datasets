Qayamat: City Under Threat
{{Infobox Film
| name           = Qayamat: City Under Threat (Bollywood movie) 
| image          = Qayamat.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Harry Baweja
| producer       = Pammi Baweja
| story          = Harry Baweja
| screenplay     = Harry Baweja Suparn Verma
| narrator       =  Arbaaz Khan Isha Koppikar Riya Sen Neha Dhupia Aashish Chaudhary
| music          = Nadeem-Shravan
| cinematography = Sanjay F. Gupta
| editing        = Merzin Tavaria
| studio         = 
| distributor    = Baweja Movies
| released       = July 11, 2003
| runtime        = 157 minutes
| country        = India
| language       = Hindi
| budget         = 40 crores
| gross          = 75 crores
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 The Rock.  Neha Dhupias voice was dubbed by dubbing artist Rajanika Ganguly Mukherjee as the director thought her voice did not suit her character perfectly. It was a blockbuster upon its release, providing Ajay Devgn with a hit after a series of unsuccessful films. The action scenes and story of the movie were so highly praised that the demand from fans for a sequel was quite high. The film completed 100 days on single screen theaters in Mumbai, Indore, Bhopal, Delhi and Ujjain. The Elphiston Jail of Mumbai copy describe in it was originally situated in Diu Island,  Daman & Diu, U.T, India.The film was declared a Hit.

== Plot == Arbaaz Khan) and their common girlfriend Laila (Isha Koppikar). As a part of a plan hitched by a corrupt Pakistani general, Ali, Abbas and Laila hitch a plan to extort money from the Indian government. They take control of Elphinston Jail, in the city of Mumbai and using the help of a corrupt scientist, Gopal (Chunky Pandey) (a part of a team headed by Rahul (Aashish Chaudhary), investigating the effects of a deadly virus which could kill any living organism within a 3-kilometer radius.), they load three missiles with the virus. They then take a group of 213 tourists hostage in the jail and demand a ransom of 1500 crores from the government within 24 hours, failing which they will release the missile into major water bodies in Mumbai, thus creating an apocalypse (Qayamat).

Akram asks the Chief Minister to allow him to release Rachit (Ajay Devgan), a man who has previous experience of escaping from Elphinston Jail, a feat which has never been accomplished in history, to get help in entering the jail, through the very route which he had used to escape. Rachit is a man who has been silent for a long time now, owing to the fact that the love of his life, Sapna (Neha Dhupia) was killed the night he was arrested. The main point of note here was that Rachit was an associate of Ali and Abbas and had been double crossed by them, leading to his arrest. In turn, they had killed Sapna. However, unknown to Rachit, she was still alive. Akram and his team, which includes Rahul use all the help, they can to enter the jail through, first, an underwater route, and then a maze of tunnels through which Rachit had escaped. they successfully get to the septic tank of the jail but a mole in the CBI gives the news of the teams arrival to Ali and Abbas, whose men kill the whole team in a brutal gunfight, resulting in Akrams death as well. Rahul and Rachit are the only ones that are alive.

Rachit suffers from severe mental trauma, the effects of which can be seen every 12 hours, whereby he starts hallucinating and sees odd shadows everywhere. He has even lost his power of speech due to this.  These effects are taken away, when Sapna, who is now at the CBI headquarters, calls him on a walkie-talkie and assures him of her true love and the fact that she is indeed alive. This is enough for Rachit as he gets up and single-handedly takes out all of Ali and Abbass men, one by one.

Meanwhile, Rahul is also able to disarm the missiles one by one. However, while Rachit is taking out some more of the men, Ali gets to the last missile and tries to release it. Rachit kills Abbas and Laila and gets to Ali in the nick of time. He disarms the missile and kills Ali after firing the rocket straight at him. Thus, the city is saved in the nick of time. At the end of the film, Rachit reunites with Sapna and reveals that he can talk.

== Cast ==
*Ajay Devgn as Rachit
*Sunil Shetty as Akram Sheikh
*Isha Koppikar as Laila
*Sanjay Kapoor as Abbas Arbaaz Khan as Ali
*Aashish Chaudhary as Rahul
*Riya Sen as Sheetal
*Neha Dhupia as Sapna
*Chunky Pandey as Gopal

==Soundtrack==
The music is composed by Nadeem Shravan and the lyrics are by Sameer (lyricist)|Sameer.
{| class=wikitable 
|-
!Song
!Singer(s)
|-
|"Woh Ladki Bahut Yaad Aati Hai (Duet)" Kumar Sanu & Alka Yagnik
|-
|"Dil Chura Liya" Abhijeet Bhattacharya|Abhijeet, Kavita Krishnamurthy
|-
|"Aitbaar Nahin Karna (Duet)"
|Abhijeet, Sadhana Sargam
|-
|"Mujhe Tumse Muhabbat Hai" Kumar Sanu, Mahalakshmi Iyer
|-
|"Yaar Pyar Ho Gaya"
|Abhijeet, Alisha Chinai
|-
|"Aitbaar Nahin Karna (Solo)" Abhijeet
|-
|"Mera Dil Dil Toh Lele" Shaan (singer)|Shaan, Mahalakshmi Iyer
|-
|"Woh Ladki Bahut Yaad Aati Hai (Solo)" Kumar Sanu
|-
|"Qayamat Qayamat" Sonu Niigam, Hema Sardesai
|}

==Notes==
 

== External links ==
* 

 
 
 
 
 
 
 
 