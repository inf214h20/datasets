Dal: Yma/Nawr
{{Infobox film
| name           = Dal: Yma/Nawr
| image          =
| image_size     =
| caption        =
| director       = Marc Evans
| producer       = Maurice Hunter Fizzy Oppe Ed Thomas Ynyr Williams
| writer         = Daniel Evans Rhys Ifans Matthew Rhys Siân Phillips Iola Gregory
| music          = Bedwyr Humphreys Owen Powell
| cinematography = Jimmy Dibling
| editing        = Mike Hopkins
| studio         = Fiction Factory
| distributor    = S4C
| released       =  
| runtime        = 74 minutes
| country        = Wales
| language       = Welsh
}}
Dal: Yma/Nawr (  directed by Marc Evans starring John Cale, Ioan Gruffudd and Rhys Ifans.  

==Premise==
The documentary focuses on the poetic soul of Wales. It explores poetic responses, historical and contemporary, to the issues of social and cultural survival and it celebrates the 2,000-year odyssey through Europes oldest surviving bardic tradition. 

==Cast==
* John Cale 
* Ioan Gruffudd 
* Rhys Ifans 
* Siân Phillips Daniel Evans
* Guto Harri  Nia Roberts
* Cerys Matthews
* Iola Gregory
* Betsan Llwyd
* Cerys Matthews
* Maureen Rhys

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 