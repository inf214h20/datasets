Desperate Moment
{{Infobox film
| name           = Desperate Moment
| image          = "Desperate_Moment"_(1953).jpg
| image_size     =
| caption        = British quad poster
| director       = Compton Bennett
| producer       =  George H. Brown
| writer         =  George H. Brown Patrick Kirwan
| based on       = a novel by Martha Albrand
| starring       = Dirk Bogarde  Mai Zetterling  Philip Friend
| music          = Ronald Binge
| cinematography = C.M. Pennington-Richards
| editing        =  John D. Guthridge
| studio         =  George H. Brown Productions    (as Fanfare)
| distributor    = General Film Distributors   (Uk)
| released       = 17 March 1953	(London)  (UK)
| runtime        = 88 min.
| country        = UK
| language       = English
}}

Desperate Moment is a 1953 British thriller film directed by Compton Bennett and starring Dirk Bogarde, Mai Zetterling and Philip Friend. 

==Plot==
A Dutchman wrongly accused of a crime goes on the run through Germany in search of the only witness who can clear him.

==Cast==
* Dirk Bogarde - Simon Van Halder 
* Mai Zetterling - Anna DeBurg 
* Philip Friend - Captain Bob Sawyer 
* Albert Lieven - Paul Ravitch
* Fritz Wendhausen - Warder Goeter 
* Carl Jaffe - Becker 
* Gerard Heinz - German Prison Doctor
* André Mikhelson - Polizei Inspector 
* Harold Ayer - Captain Trevor Wood
* Walter Gotell - Ravitchs Servant-Henchman 
* Friedrich Joloff - Valentin Vladek
* Simone Silva - Mink, Valentins girl 
* Ferdy Mayne - Detective Laurence 
* Walter Rilla - Colonel Bertrand, Dutch consulate 
* Antonio Gallardo - Spanish Dancer

==Critical reception==
The New York Times wrote, "the sum and substance of this producion...is a great deal of panting exercise within and all over two cities, offering little about which to care" ;   whereas TV Guide found it "quite suspenseful, with Bogarde turning in an exceptionally fine performance."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 