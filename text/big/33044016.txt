No Other Woman
 
{{Infobox film
| name           = No Other Woman
| image          = NoOtherWomanOfficial.png
| caption        = Theatrical movie poster
| director       = Ruel S. Bayani
| producer       = Charo Santos-Concio Malou Santos Vic Del Rosario Vicente Del Rosario III Veronique Del Rosario-Corpus Kriz G. Gazmen Marivic B. Ong June T. Rufino Marizel V. Samson
| writer         = Keiko Aquino Ricardo Fernando III Kris G. Gazmen
| screenplay     = Kris G. Gazmen Ricardo Fernando III
| story          = Kris G. Gazmen
| based on       =  
| starring       = Anne Curtis Derek Ramsay Cristine Reyes
| music          = Raul Mitra
| cinematography = Charlie Peralta
| editing        = Vito Cajili
| studio         = Star Cinema Viva Films
| distributor    = Star Cinema
| released       =  
| runtime        = 
| country        = Philippines
| language       = Tagalog English
| budget         = 
| gross          = P 278,418,883   in Box Office Mojo (retrieved July 29, 2012) 
}}

No Other Woman is a 2011 Filipino romantic drama film starring Anne Curtis, Derek Ramsay, and Cristine Reyes.
 third highest 2012 Metro Manila Film Fest entry, Sisterakas. The film was also screened in select cities internationally.

Though regarded as one of the memorable romance-drama movies for Filipinos and the second highest grossing Filipino film of all time, the film received mixed reviews from local film critics. Critics criticized the weak ending of the story line which is strong especially at the middle part but praised the film for memorable one-liners and the values in the movie.

==Synopsis==
Furniture supplier Ram (Derek Ramsay) is happily married to Charmaine (Cristine Reyes). One day, Ram lands a big client, a new luxury resort. But he needs the help of Kara (Anne Curtis), the daughter of the owner of the resort, to finalize the deal. Karas help, however, comes with a price, because she fancies Ram to be her lover. Not before long, Kara successfully seduces Ram, even though she knows about his marriage. When Charmaine learns of the affair, she finds ways to fight for her husbands waning attention.

==Cast and characters==

*Anne Curtis as Kara Zalderiaga
*Derek Ramsay as Ram Escaler
*Cristine Reyes as Charmaine Escaler
*Tirso Cruz III as Fernando Zalderiaga
*Carmi Martin as Babygirl Dela Costa
*John Arcilla as Mario Dela Costa
*Marlann Flores as Violet Dela Costa
*Johnny Revilla as Jaime Escaler
*Matt Evans as Jake Escaler
*Melvin Lee as Ito Dela Cruz
*Niña Dolino as Marian
*Ricci Chan as Raymond
*Ron Morales as Victor Kitkat as Mimi
*Kat Alano as Michelle
*Peter Serrano as Sales Team Staff
*Fred Payawan as Sales Team Staff

==Release==

===Distribution=== Typhoon Pedring, which affected parts of Luzon. This cancellation was announced by Anne Curtis in her show Its Showtime (variety show)|Showtime on the morning of the supposed premiere date.

No Other Woman was released nationwide on September 28, 2011. It was also screened in selected cities around the world, including Chicago, Las Vegas, and Los Angeles. 

The movie was given an R-13 rating by the Movie and Television Review and Classification Board.

===Box Office=== Typhoon Pedring highest grossing Filipino film of all time with an approximate P278 Million gross income, breaking the box office record set by the 2009 film, You Changed My Life, which grossed P225 million.  

Due to the movies good performance at the box office, its cast and crew received monetary rewards from the production studios. 

On October 26, 2011, the box office records set by No Other Woman was surpassed by The Unkabogable Praybeyt Benjamin, another movie produced by Star Cinema and Viva Films. The Unkabogable Praybeyt Benjamin opened with a P200 million gross on its first week.  and has a current gross of P331.6 million. 

No Other Woman currently holds the record as the eighth highest-grossing Filipino film of all time.

==Critical Reception==
No Other Woman was graded "A" by the Cinema Evaluation Board of the Philippines. 

The film received mixed reviews from local film critics. Aaron Lozada of Philippine Star praised the films casting, direction, and scoring.  Abby Mendoza of GMA New Media#Philippine Entertainment Portal (PEP)|PEP.ph praised the movie for handling a sensitive matter fairly.  Philbert Dy of ClickTheCity.com criticized the movie for its uneven story, adding that the ending is "truly ugly."  Jessica Zafra of Interaksyon.com commented that the characters are stereotypical. 

==Accolades==

===Awards and nominations===
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Work
! width="10%"| Result
|- 2012
| 28th PMPC Star Awards for Movies
| Movie of the Year
| rowspan=10|No Other Woman
|  
|-
| Movie Actress of the Year (Best Actress) for (Anne Curtis)
|  
|-
| Movie Actor of the Year (Best Actor) for (Derek Ramsay)
|  
|-
| Best Original Screenplay of the Year (Kriz Gazmen) ||  
|-
| Movie Cinematographer of the Year (Charlie Peralta) ||  
|-
| Movie Editor of the Year (Vito Cajili) ||  
|-
| Movie Musical Scorer of the Year (Raul Mitra) ||  
|-
| Movie Sound Engineer of the Year (Ditoy Aguilar) ||  
|- 14th Gawad PASADO Awards
| PinakaPASADOng Aktres (Anne Curtis)
|  
|-
| PinakaPASADOng Dulang Pampelikula (Kriz Gazmen)
|  
|- 2012
| rowspan="2" align="left"| GMMSF Box-Office Entertainment Awards  Derek Ramsay|| 
|- GMMSF Box-Office Anne Curtis and Cristine Reyes|| 
|- 2012
| 60th FAMAS Awards
| Best Direction for (Ruel S. Bayani)
| rowspan=15| No Other Woman
|  
|-
| Best Actress for (Anne Curtis)
|  
|-
| Best Actress for (Cristine Reyes)
|  
|-
| Best Actor for (Derek Ramsay)
|  
|-
| Best Supporting Actor for (Tirso Cruz III)
|  
|-
| Best Supporting Actor for (Carmi Martin)
|  
|-
| Best Screenplay (Kriz Gazmen)
|  
|-
| Best Cinematography (Charlie Peralta)
|  
|- 30th Luna Awards
| Best Picture
|  
|-
| Best Direction 
(Ruel S. Bayani)
|  
|-
| Best Actor for (Derek Ramsay)
|  
|-
| Best Screenplay (Kriz Gazmen) ||  
|-
| Best Cinematography (Charlie Peralta) ||  
|-
| Beat Editing (Vito Cajili) ||  
|-
| Best Musical Score (Raul Mitra) ||  
|}

==See also==
* My Neighbors Wife In the Name of Love The Mistress
* A Secret Affair One More Try
* Seduction (2013 film)|Seduction
* The Bride and the Lover
* When the Love Is Gone Trophy Wife
* Once a Princess The Gifted The Trial

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 