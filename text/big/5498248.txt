The King Maker
{{Infobox film
| name           = The King Maker
| image          = Kingmaker.jpg
| caption        = Thai movie poster.
| director       = Lek Kitaparaporn David Winters Sean Casey
| starring       = Gary Stretch John Rhys-Davies Yoe Hassadeevichit Cindy Burbridge Ian Livingstone
| cinematography = Jiradeht Samnansanor
| editing        = William Watts
| distributor    = Alpha Beta Films International Sahamongkol Film International (Thailand)
| released       = October 20, 2005
| runtime        = 100 minutes
| country        = Thailand UK English Thai Thai
| budget         = 250 million baht
}} Portuguese mercenary David Winters, 1941 film, King of the White Elephant, produced by Pridi Phanomyong. This was also the first Thai film sold to a "Major film studio".

==Plot== Portuguese soldier Arab slave traders and taken to Ayutthaya kingdom|Ayutthaya. Released from his bonds to be put on the auction block, he promptly knocks down his captors and leads them on a chase throughout the ancient city.
 Eurasian beauty, Maria, who buys him his freedom back. After recovering from his recapture, Maria brings Fernando to meet her father, Phillippe. Fernando immediately recognizes Phillippe as the man who killed Fernandos father many years ago, whom Fernando has been seeking on a lifetime quest for revenge.
 King Chairacha, who has to go into battle. It is a multi-national taskforce, not only including Portuguese mercenaries, but also samurai warriors (likely Christians expelled from Japan). In battle, Fernando bonds with a Thai warrior named Tong. They distinguish themselves by saving the King from an assassination attempt and are appointed his personal bodyguards.
 consort who schemed her way into becoming queen, is behind the assassination plot. Because it failed, she calls on Don Phillippe for help. Phillippe in turn enlists a scar-faced ninja to kill the king. This plan is also thwarted by Fernando, Tong and other Siamese troops.  After the assassins are killed in the kings bedroom, King Chairacha assigns Tong and Fernando the task of seeking out whoever is behind this scheme.
 Prince Yodfa (who is next in line for the throne), to clear the way for her boyfriend Worawongsathirat. This she achieves by hiring a spear-wielding African warrior.
 Burmese invasion and the eventual decline and destruction of Ayutthaya.

==Cast==
*Gary Stretch as Fernando De Gamma
*John Rhys-Davies as Phillippe
*Cindy Burbridge as Maria
*Yossavadee Hassadeevichit as Queen Sudachan
*Dom Hetrakul as Tong
*Nirut Sirichanya as King Chairacha
*Charlie Trairat as Prince Yodfa
*Akara Amarttayakul as Phan Bud Sri Thep
*Byron Bishop as Scar-Face Ninja

==Language issues== Thai characters were interacting with other Thais were dubbed in Thai language|Thai.

==External links==
* 
* 
* 

 
 
 
 
 
 
 