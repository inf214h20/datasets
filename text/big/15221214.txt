Zee and Co.
 
 
{{Infobox film
| name           = Zee and Co.
| caption        = Theatrical release poster bearing an alternate title:  X, Y & Zee
| image	=	Zee and Co. FilmPoster.jpeg
| director       = Brian G. Hutton
| producer       = Elliot Kastner Jay Kanter Alan Ladd, Jr.
| writer         = Edna OBrien
| starring       = Elizabeth Taylor Michael Caine Susannah York   Margaret Leighton
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English language|English}}

Zee and Co, also known as X, Y and Zee and Zee and Company, is a 1972 British film released by Columbia Pictures. It was directed by Brian G. Hutton, and was based upon a novel by Edna OBrien. 

The film starred Elizabeth Taylor and Michael Caine as a middle-aged, bickering couple whose marriage is on its last legs, and Susannah York as the woman who comes between them. Margaret Leighton was also featured in a supporting role as a dizzy socialite.
 The Family of Man" from the previous album, "Harmony (Three Dog Night album)|Harmony" (1971).  

==Plot summary==
Zee Blakely (Elizabeth Taylor) is a loud, coarse, forty-something socialite, whose marriage to her architect husband Robert (Michael Caine) is on the rocks, as witnessed by their frequent verbal sparring matches. Sick of Zees antics, Robert is drawn to quiet boutique owner Stella (Susannah York) who is the complete antithesis to Zee in terms of personality.

Feeling bored and rejected, Zee attempts a number of methods to regain Roberts sympathy, such as attempting suicide, but these do not work. Zee discovers that Stella had a lesbian affair in the past, and uses this against both her, and Robert, to dare him to partake in a love triangle with Stella.

==Critical reception==
Critical opinions of the film were varied. Roger Ebert said that while the movie is "no masterpiece" it still satisfies audiences as it "unzips along at a nice, vulgar clip".    He said that Elizabeth Taylor is the films main attraction, but the emphasis upon her detracts somewhat from a fuller representation of the love triangle in the film.  Steven Scheuer praised the film for its "intelligent dialogue" and as a "change of pace" for its director.    Michael McWilliams cited Taylors work as "her greatest movie performance" and called the film "outrageously funny" (McWilliams, 1987: 32).

Other critics were less sympathetic.   felt the film was sabotaged by the directors "indulgent" take on it, thereby skewing   offered a very brief review of the film being that it was a "pointless tale of sexual relationships", (Martin and Porter, 1996: p.&nbsp;1213).

==Cast==
* Elizabeth Taylor - Zee Blakeley
* Michael Caine - Robert Blakeley
* Susannah York - Stella
* Margaret Leighton - Gladys
* John Standing - Gordon
* Mary Larkin - Rita
* Michael Cashman - Gavin

==DVD==
A remastered Region 1 DVD-R  , sonypictures.com; accessed 26 August 2014.  was released by Sony Pictures on 17 December 2010.

==Bibliography==
 
* Hirschhorn, Clive (1989) The Columbia Story, Pyramid Books, London.
* Maltin, Leonard (1991) Leonard Maltins Movie and Video Guide 1992, Signet, New York.
* Martin, Mick and Porter, Marsha (1996) Video Movie Guide 1997,  Ballantine Books, New York.
* McWilliams, Michael (1987) TV Sirens: A Tantalizing Look at Prime Times Fabulous Females, Perigee Books, New York.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 