Shanghai Rhapsody
{{Infobox film
| name           = Shanghai Rhapsody
| image          =
| image_size     =
| caption        =
| director       = Kinji Fukasaku
| producer       =
| writer         =
| narrator       =
| starring       =
| music          = Nobuyoshi Koshibe
| cinematography = Keiji Maruyama
| editing        =
| distributor    =
| released       = 1984
| runtime        =
| country        = Japanese
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
}}
Shanghai Rhapsody (上海バンスキング, Shanghai bansu kingu) is a 1984 Japanese film directed by Kinji Fukasaku.

==Cast==
*Keiko Matsuzaka as Madoka Hatano
*Morio Kazama as Shiro Hatano
*Etsuko Shihomi as Lily
*Ryudo Uzaki as Bakumatsu
*Mitsuru Hirata as Shinzo Hirota
*Noboru Mitani as Ho-san
*Isao Natsuyagi as Shirai Sam Sloan was an extra

==Other credits==
*Writing:
**Kinji Fukasaku
**Ren Saito: story
**Yôzô Tanaka
*Art Direction:
**Kyohei Morita
**Yutaka Yokoyama
*Assistant Director: Hideo Nanbu
*Sound recordist: Kazuhisa Takahashi

== External links ==
*  

 
 

 
 
 