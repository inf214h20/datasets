The Goose and the Gander
{{Infobox film
| name           = The Goose and the Gander
| image          = 
| image_size     = 
| caption        = 
| director       = Alfred E. Green
| producer       = James Seymour (uncredited)
| writer         = Charles Kenyon
| story          = Charles Kenyon
| based on       =
| narrator       = 
| starring       = Kay Francis George Brent Genevieve Tobin
| music          = 
| cinematography = 
| editing        = 
| studio         = Warner Bros.
| distributor    = Warner Bros. / The Vitaphone Corp.
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Goose and the Gander is a 1935 American romantic comedy film starring Kay Francis, George Brent and Genevieve Tobin. A woman finds out her ex-husbands new wife is cheating on him and decides to expose her.

==Cast==
* Kay Francis as Georgiana
* George Brent as Bob McNear
* Genevieve Tobin as Betty John Eldredge as Lawrence
* Claire Dodd as Connie
* Ralph Forbes as Ralph Summers
* Helen Lowell as Aunt Julia
* Spencer Charters as Winkelsteinberger William Austin as Arthur Summers
* Eddie Shubert as Sweeney
* Charles Coleman as Jones
* Olive Jones as Miriam Brent Bill Elliott as Teddy (as Gordon Elliott)
* John Sheehan as Murphy
* Wade Boteler as Sprague

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 