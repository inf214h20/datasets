The Revenant (2009 film)
{{Infobox film
| name           = The Revenant
| image          = 
| alt            = 
| caption        = 
| director       = Kerry Prior
| producer       = Kerry Prior Jacques Thelemaque Liam Finn
| writer         = Kerry Prior
| starring       = David Anders Chris Wylde Louise Griffiths Jacy King
| music          = 
| cinematography = Peter Hawkins
| editing        = Kerry Prior
| studio         = Putrefactory Limited
| distributor    = Lightning Entertainment
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Revenant is a 2009 dark comedy/horror film written and directed by Kerry Prior and starring David Anders and Chris Wylde. The film was shot in Los Angeles, California.

== Plot ==
The film focuses on Second Lieutenant Bart Gregory (David Anders) who has been killed under mysterious circumstances in Iraq.   After his friends and girlfriend Janet (Louise Griffiths) attend his funeral, Bart awakens in his grave.  Enlisting the help of his best friend, Joey Leubner (Chris Wylde), Bart begins to understand and learn how to deal with his new undead state; mainly, the fact that Bart needs blood to hold back decomposition and that he returns to a state of in-animation during daylight hours. Joey does research online to find out what Bart is and seems to be stuck between a Zombie and Vampire, finally stating that Bart is a Revenant.

While buying beer from a small store in Koreatown, Bart and Joey become vigilantes when Bart both kills and feeds off of a gangster who is holding up the store. They enjoy the media coverage of the incident, and Joey asks Bart to bestow him with the "dark gift". Bart refuses to do so and laughs the idea off. However, after a subsequent attempt at vigilantism goes wrong and Joey is fatally wounded, Bart is forced to drink Joeys blood in order to save him.

The two continue their vigilante killing spree for a while, until Mathilda (Jacy King), a friend of Janet who dislikes both Bart and Joey, follows them and threatens to reveal their activities to the world, especially Janet. Joey shoots Mathilda through the chest, but before she dies, she is able to send the information to Janet.

Fearing they will be caught, Joey tells Bart to meet him back at the apartment with a packed bag in half an hour, then drives away mysteriously. Bart meets a teary Janet at the apartment, who forces him to explain the fact that he requires blood to stay stable. She then begs him to feed off of her instead, so that he will no longer need to kill. Bart loses control and drains her until she dies.
 Las Vegas to continue their reign. However, after Bart shows him Janets corpse, the two begin to fight, and proceed to shoot each other repeatedly, although this is insufficient to kill either of them. Joey storms out and states that he will continue on to Vegas alone. Bart decapitates Janet in order to ensure her death, then drops her remains over the bridge where he and Joey usually disposed of their corpses. Bart is captured by SWAT teams and taken to jail, where, come dawn, he collapses in his cell. Upon nightfall, Bart reawakens in the morgue and escapes, returning to the apartment. Inside is a package containing Joeys severed head.

Since he was decapitated at night, Joey is still "alive", and Bart uses a vibrating dildo to enable Joeys head to talk. Joey warns Bart that a gangbanger who was their first kill is after him for revenge, and then requests that Bart kill him for good. Bart crushes Joeys head underneath a bulldozer, and then tries to find a way to kill himself.

Against normal convention, a bullet through the brain does not have the desired effect, and neither does hanging himself with Christmas lights. He even throws himself in front of the subway train, but only succeeds in severing his arm. Bart then boards a train, where he finds and reads a letter that Janet left in his uniforms pocket at his funeral. He breaks down and attacks the only other passenger. He is caught and flees into the station where more SWAT teams attempt to catch him.

He finally escapes to a hilltop and at dawn collapses once more, while he is being surrounded by men in hazmat suits.

The film then cuts to a tour of sorts, where various military personnel are being shown revenants in glass containers, including Bart. A General asks Bart if he was a soldier, and then states that this fact may give him an advantage.

Bart is then shown in a large canister being airdropped into Khūzestān Province, Iran, along with the other revenants, where the canister opens upon landing, releasing him on the country.

==Cast==
* , Bart must deal with his new condition, a potential for a second chance on life, and trying to come to terms with the girlfriend that he had left behind.
*Chris Wylde as Joey Leubner: As Barts best friend, Joey acts as a partner and sidekick to Bart, providing him background of Barts deteriorating condition, and encouraging him to act on his darker impulses in order to survive. 
*Louise Griffiths as Janet: Barts love interest who is devastated after Barts unexpected death. 
*Jacy King as Mathilda (Matty): A Wiccan nurse who is close friend to Janet, Matty acts as the voice of reason.
*Emiliano Torres - Miguel
*Senyo Amoaku – liquor store robber
*Cathy Shim – Sophia Chang
*David Ury – ATM robber
*Wally White – The Minister
*Zana Zefi – Ms. Rahmanov
*Clint Jung - Marty

== Development == Liam Finn and Jacques Thelemaque were co-producers. The film was edited by Kerry Prior, though the opening credits state "image juxtaposition designer Walter Montague Urch".

"Walter Montague Urch" could be a reference to Walter Map and Montague Summers, who have written about revenants and men who have returned from the dead.

== Filming ==
Filming for The Revenant began in April 2008 in Los Angeles, California. The film is presented in 4K digital cinema with an aspect ratio of 2.35:1 spherical.

== Reception ==
The Revenant has received positive feedback from several film reviewers and the horror film community. A post-screening review from DreadCentral.com declared it is "sure to become a cult classic." {{cite web
 | title = Movie Review: The Revenant
 | url = http://www.dreadcentral.com/reviews/revenant-the-2009
 | date = May 23, 2009
 | author = Erik W. Van Der Wolf
 | publisher = Dread Central
 | accessdate = 2010-03-19
}} 

The film won the Audience Award for "Best Narrative Feature" at the 2009 CineVegas Film Festival, in Las Vegas. Harry Knowles of Aint It Cool News wrote an early review, calling it "a great little genre film that gets exactly as nuts as one would hope a violent buddy flick can get with the undead." {{cite web
 | title = Harry was blown away by the bloody great film, THE REVENANT in CineVegas!!!
 | url = http://www.aintitcool.com/node/41413
 | date = June 14, 2009
 | author = Harry Knowles 
 | publisher = Aint It Cool News
 | accessdate = March 18, 2010
}} 

Chris Alexander of  ; this is the best dark fantasy/comedy I’ve seen since classic stuff I hold so dear to my heart, masterworks like Return of the Living Dead, Re-Animator, RoboCop and Fright Night." 

Aaron Morgan from Fantastic Fest commented that "Overall this movie is a real gem and deserves a wide release here in the states to shake us out of this remake/retread horror trend."

Toronto After Dark listed The Revenant as one of the "10 Movies To See Before You Die" .

==Awards==
Since its debut on the film festival circuit, The Revenant has won the following awards:

*Xanadu Sci Fi and Horror Convention 2009:  "Best Feature Film"
*Zompire, The Undead Film Festival 2009:  "Best Feature," "Best Director" Kerry
Prior; "Audience Award" 
*CineVegas 2009:  "Audience Award, Best Narrative Feature"
*Toronto After Dark Film Festival 2009:  "Feature Film, Silver"
*Fantastic Fest 2009: "Best Director," Kerry Prior; "Audience Award (4th Place)"
*ScreamFest, Los Angeles 2009: "Best Make Up," "Best Special FX"
*Vampire Film Festival, New Orleans 2009: "Outstanding Vampire Film" (1st Place)
*Falstaff International Film Festival 2009: "Best of the Festival Award"
*New York City Horror Film Festival 2009: "Best Picture," Kerry Prior; "Best Director," "Best Actor" (Tie: Chris Wylde, and David Anders), "Audience Choice" 
*Omaha Film Festival 2010: "Best Off the Edge Film"
*A Night of Horror International Film Festival 2010: "Best Film", Best Director" (Kerry Prior)
* Celluloid Screams: Sheffield Horror Film Festival 2010 "Audience Award: Best Feature Film"

The film was also an "Official Selection" of the following film festivals:

*Sitges 42nd International Film Festival 2009: Official Selection (10/09)
*Chicago International Film Festival 2009: Official Selection (10/09)
*Grimm Up North- UK 2009: Official Selection: (10/09)
*Mar del Plata International Film Festival 2009: Official Selection (11/09)
*Leeds International Film Festival- UK 2009: Official Selection (11/09)
*Starz Denver International Film Festival 2009: Official Selection (11/09)
*Stockholm International Film Festival 2009: Official Selection (11/09)

==References==
 

== External links ==
* 
* 
* 

 
 
 
 
 
 
 
 