Doorbeen
{{Infobox film
| name           = Doorbeen
| image          = Doorbeen poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Swagato Chowdhury
| producer       = Saikat Mitra
| writer         = 
| screenplay     = Subrato Guha Roy
| story          = Subrato Guha Roy
| based on       =  
| narrator       =  See below
| music          = Saikat Mitra
| cinematography = Shamit Gupta
| editing        = Sujay Dutta Roy
| studio         = Macneill Media Pvt. Ltd.
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 Bengali thriller film directed by Swagato Chowdhury and produced by Saikat Mitra. Music of the film has been composed by Saikat Mitra.   

== Cast ==
* Rangeet as Pupul
* Diptodeep as Tatai
* Ahana as Bhebli
* Soumitra Chatterjee as Byomkesh Bakshi   
* Sabyasachi Chakraborty as Feluda 
* Shantilal Mukherjee as Gobinda Gargori
* Anjana Basu as Pupuls mother 
* Aparajita Auddy as Madhabi Dutta 
* Subrato Guha Roy
* Rajat Ganguly
* Pratik Chowdhury
* Aritra Dutta Banik
* Pradip Chakraborty
* Debranjan Nag
* Arunabha Dutta
* Nitya Ganguly
* Tapas Biswas
* Sanjib Sarkar
* Ranjan Bandyopadhyay

==Critical reception==
{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|-
| The Times of India
|  
|-
|}

Madhusree Ghosh of The Times of India gave the movie 3 out of 5 stars and wrote, "Unfortunately, the director, riding high on the excitement of doing something mind-blowing, messes up that balance quite a few times. The weak and predictable plot also doesnt help things much." 

== References ==
 
 
 
 
 
 
 
 

 
 

 