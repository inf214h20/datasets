Kallu Kondoru Pennu
{{Infobox film
| name           = Kallu Kondoru Pennu
| image          = Kallu Kondoru Pennu.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Shyamaprasad
| producer       = G. Jayakumar
| writer         = G. Jayakumar
| narrator       =  Dileep
| music          = Ilaiyaraaja
| cinematography = P.Sukumar Ramachara Babu  Salu George   Saroj S. Padi
| editing        = B. Lenin V. T. Vijayan
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Dileep and Vijayashanti. This movie is about an unnmaried nurse from Kuwait City who adopts a young kid.

==Plot==
Kallu Kondoru Pennu movie tells us the story of a nurse Sita (Vijayashanti), who toils hard in the Gulf for the sake of her family consisting of two brothers and a sister. It is her family which is above everything for her and for this she even tries to forget the love that she has for Dr. Suresh (Suresh Gopi), who works with her in the same hospital and who too loves her sincerely. 

At home in Kerala, her elder brother Haridasan (Murali (Malayalam actor)|Murali) and his wife Ambika (Chithra) along with their children, her sister Raaji and her younger brother Venu (Dileep (actor)|Dileep) all are leading a comfortable life, all with the money that she sends home. It is Sita herself who is consulted by all in all matters including the marriage of her sister, which is to take place with Mohanachandran (Maniyanpilla Raju), whose main demand as part of the dowry is a visa which could help him go to the Gulf and earn money though he has a good government job here.

It is when Sita starts for home in connection with her sisters marriage that war breaks out in the Gulf and for some days the whereabouts of Sita are unknown. Few days later, a photograph of Sita holding a child in her hands appears in the newspapers with the caption " A mother and child in a refugee camp". This sets tongues wagging and finally when Sita arrives, that too with a child in her hand, she finds that life has changed a lot for her.

==Cast==
*Vijayshanti as Sita
*Suresh Gopi as Dr. Suresh Dileep as Venu Murali as Haridasan Chithra as Ambika
* Maniyanpilla Raju as Mohanachandran
* Chandini as Raji
* Rajan P. Dev as Sreekandan
* Harisree Ashokan as Antappan
* M. S. Thripunithura as Annandakuttan ammavan
* K. P. Ummer as Dr Sureshs father
* Mammukkoya as Kabeer Reena as Dr Sureshs sister Keerthana as Deepa
* Appu as Baby Appu
* V P Ramachandran as Brother-in-law
* Sathyanarayanan as Dr Nooruddin
* Geethsadanandan as Pankajam
* Baby Neeraja as Munnu
* Baby Anju as Chinnu
* Bindu Madhavi as Mrs Nooruddin

==External links==
*  
* http://popcorn.oneindia.in/title/3558/kallukondoru-pennu.html

 

 
 
 
 
 
 
 
 