Ashleelotar Daye
 
{{Infobox film
| name  =Ashlilatar Daye
| image =
| starring       =
| story          =
| director       =Uma Nath Bhattacharya 
| distributor    =M. F. Movies
| released   = 1983 Bengali
| music          =
| cinematography =Uma Nath Bhattacharya 
| editing        =
| art direction  =
| budget         =
| runtime        =
}} Bengali film released on 1983. The film is based on a thriller novel by the Narayan Sanyal with the same title, which was published in 1975. Sanyal took the of this novel theme from Seven Minutes by Irving Wallace.

Directed by Uma Nath Bhattacharya, this film starred Chiranjit, Alpana Goswami, Dilip Roy, Biplab Chatterjee, Satya Banerjee, Sanghmitra Banerjee, Santana Basu and Anamika Saha.
Arundhati Hom Chaudhury and Haimanti Shukla sang in this film, Pabitra Chattopadhyay was the lyrist.

==See also==
*Bhanu Pelo Lottery
*Golpo Holeo Sotyi
*Ora Thake Odhare
*Sare Chuattor

 
 
 
 

 