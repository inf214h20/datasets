Wide Sargasso Sea (1993 film)
 

{{Infobox film name        = Wide Sargasso Sea image       = Sargasso1993.jpg caption     = Theatrical release poster writer      = John Duigan Carole Angier Jan Sharp (based on the novel by Jean Rhys) starring  Michael York director    = John Duigan producer    = Jan Sharp music       = Stewart Copeland cinematography       =Geoff Burton editing     = Anne Goursaud Jimmy Sandoval distributor = New Line Cinema released    =   (US) runtime     = 98 minutes country     = Australia language    = English budget      =  gross       = $1,614,784 
}}
 
 1993 film novel of the same name, directed by John Duigan.

==Plot summary== plot summary of Wide Sargasso Sea.

==Release==
Fine Line Features released the film for the United States market. The film was given a restrictive Motion Picture Association of America film rating system#X is replaced by NC-17|NC-17 rating due to its sexual content. Fine Line opted not to pursue a less restrictive, more marketable R rating. 

==Cast and roles== Antoinette Cosway
* Nathaniel Parker - Edward Rochester
* Rachel Ward - Annette Cosway Michael York - Paul Mason
* Martine Beswick - Aunt Cora
* Claudia Robinson - Christophene Richard Mason
* Casey Berna - Young Antoinette
* Rowena King - Amelie
* Ben Thomas - Daniel Cosway
* Naomi Watts - Fanny Grey

==Production==
John Duigan did not enjoy the experience of making the film:
 It was probably the only really unsatisfying interaction that Ive had with a production company and I found that I had major disagreements with them and with the producers. It was unfortunate. Jan Sharp, the producer of the film, had the tenacity to get the film made, but she and I had differences of opinion. She was very well informed on the book, and Im sure her opinions were arguable, as I like to think mine were, but when you have a situation like that, I think the overall project can suffer. I think the film did suffer from that division.   accessed 18 November 2012  

==Box office==
Wide Sargasso Sea grossed $45,806 at the box office in Australia. 

==See also==
*Cinema of Australia
*Wide Sargasso Sea (TV)

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 