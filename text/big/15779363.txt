Man on a Tightrope
{{Infobox film
| name           = Man on a Tightrope
| image          = Man on a Tightrope poster.jpg
| caption        = Theatrical poster
| producer       =
| director       = Elia Kazan
| based on       =  
| screenplay     = Robert E. Sherwood Terry Moore Cameron Mitchell Adolphe Menjou
| music          = Franz Waxman
| cinematography = Georg Krause
| editing        = Dorothy Spencer
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $1.2 million 
}}
 1953 cinema American film Terry Moore Neil Paterson. Paterson based his true story, which first appeared as the magazine novelette International Incident, on the escape of the Circus Brumbach from East Germany in 1950. Members of the Circus Brumbach appeared in the film version in both character roles and as extras.

==Plot== Terry Moore) Cameron Mitchell), who has been with the circus for only a year.
 Marxist propaganda acts dictated by the government. Cernik explains that the skits were not funny, and that audiences prefer his usual act. The S.N.B. chief (John Dehner) orders him to resume the required act, and to dismiss a longtime trouper who calls herself "The Duchess." Propaganda minister Fesker (Adolphe Menjou) casually asks him about a radio in his trailer, alerting Cernik to a spy in his midst. Cernik is fined and released, although Fesker believes that he is a threat to the state.
 American Army who is planning an escape attempt of his own. Cerniks longtime rival Barovik visits and reveals that he knows of the escape plan. Barovik assures Cernik that because they are both circus men, that he will not betray him. Cernik agrees to leave behind most of his equipment for Barovik. Realizing that he must act swiftly, Cernik discovers that Krofta (Richard Boone), who has worked for Cernik for twenty years, is actually the spy. Cernik ties up Krofta but is confronted by Fesker about a travel permit, which he issues to catch Cernik in the act of trying to escape. Fesker is about to pursue the circus when he is arrested by a commissar sergeant for issuing the travel permit. 

Joe reveals himself to Cernik, who incorporates him into the plan. At the border crossing, Krofta escapes, but is stopped by Cernik from warning the border guards. In the fracas Krofta mortally wounds him. Using an audacious and violent dash across the only bridge, most of the circus safely escape only to be told that Cernik has paid with his life. Obeying his dying wish, Zama orders the troupe to march on to their next performance.

==Cast==
* Fredric March as Karel Cernik Terry Moore as Tereza Cernik
* Gloria Grahame as Zama Cernik Cameron Mitchell as Joe Vosdek
* Adolphe Menjou as Fesker
* Robert Beatty as Barovic
* Alexander DArcy as Rudolph
* Richard Boone as Krofta
* Pat Henning as Konradin
* Paul Hartman as Jaromir
* John Dehner as the SNB chief

==Production==
Shot on location in Bavaria, Germany, authentic acts were used, and the entire Brumbach Circus was employed for the production.  The original plot to escape in small increments across the border was the actual means used by the Circus Brumbach in their escape.

== References ==
 

== External links ==
*  
*  
*  
*  
*  , New York Times film review, June 5, 1953

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 