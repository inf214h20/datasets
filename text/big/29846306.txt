Theodora (1921 film)
 
{{Infobox film
| name           = Theodora 
| image          =Theodora (1921) - 1.jpg
| caption        =
| director       = Leopoldo Carlucci
| producer       = 
| writer         = Victorien Sardou
| starring       = Rita Jolivet and Emilia Tosini
| music          = 
| cinematography = 
| editing        = 
| studio         = Ambrosio-Zanotta
| distributor    = 
| released       = October 14, 1921 
| runtime        = 
| country        = Italy
| awards         =
| language       = silent
| budget         = 
}}

Theodora ( ) is a 1921 Italian silent film dramatization of the life of the Byzantine empress Theodora (wife of Justinian I)|Theodora. Its survival status is classified as being unknown,  which suggests that it is a lost film.

==Plot==
Theodora, a Roman courtesan and former slave girl, marries the Byzantine emperor Justinian and assumes the throne as Empress of Rome. However, a love affair with a handsome Greek leads to revolution and armed conflict in both Byzantium and Rome.

==Cast==
*Rita Jolivet	 ... 	Teodora Augusta
*Ferruccio Biancini	... 	Justinian
*René Maupré		... 	Andreas
*Emilia Rosini		... 	Antonina
*Adolfo Trouché		... 	Belisarius
*Mariano Bottino	... 	Marcellus
*Guido Marciano		... 	Boia principale
*Marie Belfiore		... 	Tamyris
*Giovanni Motta		... 	Buzes
*Leo Sorinello		... 	Mara
*G. Rosetti		... 	Amru
*Luigi Rinaldi		... 	Calcante
*Alfredo		... 	Philo
*Alfredo Bertoncelli	... 	Euphrata
*Giuliano Gardini	... 	Cospiratore
*François Renard	... 	Cospiratore
*Pietro Ferrari		... 	Cospiratore
*Alberto Belfiore	... 	Cospiratore

==See also==
*List of historical drama films
*Late Antiquity

==References==
 

==External links==
 
*  

 
 
 
 
 
 
 
 
 

 
 