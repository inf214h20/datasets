Her Honor, the Governor
 
{{Infobox film
| name           = Her Honor, the Governor
| image          = 
| caption        = 
| director       = Chester Withey
| producer       = Joseph P. Kennedy
| writer         = Doris Anderson Hyatt Daab Weed Dickinson
| narrator       = 
| starring       = Pauline Frederick Carroll Nye
| cinematography = André Barlatier
| editing        =  FBO
| released       =  
| runtime        = 70 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film starring Pauline Frederick, directed by Chester Withey and featuring Boris Karloff.    Complete prints of the film survive.   

==Cast==
* Pauline Frederick as Adele Fenway
* Carroll Nye as Bob Fenway
* Greta von Rue as Marian Lee
* Tom Santschi as Richard Palmer
* Stanton Heck as Jim Dornton
* Boris Karloff as Snipe Collins Jack Richardson as Slade
* Charles McHugh
* Kathleen Kirkham William Worthington

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 