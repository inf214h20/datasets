In the Spirit (film)
{{Infobox film
| name           = In the Spirit
| image size     = 
| image	=	In the Spirit FilmPoster.jpeg
| border         = 
| alt            = 
| caption        = 
| director       = Sandra Seacat
| producer       = {{Plainlist|
* Beverly Irby
* Julian Schlossberg
}}
| writer         = {{Plainlist|
* Jeannie Berlin
* Laurie Jones
}}
| starring       = {{Plainlist|
* Jeannie Berlin
* Olympia Dukakis
* Peter Falk
* Melanie Griffith
* Elaine May
* Marlo Thomas
}} Patrick Williams
| cinematography = Richard Quinlan
| editing        = Brad Fuller
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

In the Spirit is a 1990 film starring Marlo Thomas and Elaine May, directed by noted acting coach Sandra Seacat, with a screenplay co-authored by Mays daughter Jeannie Berlin and Laurie Jones, both of whom also appear in the film. Runtime is 94 minutes.

==Cast==
* Marlo Thomas as Reva Prosky
* Elaine May as Marianne Flan
* Peter Falk as Roger Flan
* Melanie Griffith as Lureen
* Olympia Dukakis as Sue
* Jeannie Berlin as Crystal
* Phillip Schopper as The Voice
* Agda Antonio as Yolanda
* Brian Hickey as Attacker in Hall
* Laurie Jones as Pamela
* Phil Harper as Homeless Executive
* Steve Powers as Documentary Interviewer
* Hope Cameron as New Age Lecturer
* David Eigenberg as Handyman #1
* David Baer as Handyman #2
* Matt Carlson as 1st Policeman
* Chad Burton as Lieutenant Kelly
* Thurn Hoffman as Detective Pete Weber
* Mark Boone Junior as 2nd Policeman
* Angelo Florio as 3rd Policeman 
* Gary Swanson as 1st Detective
* Rockets Redglare as Bartender
* Candy Trabucco as Accident Bystander
* Nora York as Accident Bystander
* Roy Nathanson as Accident Bystander
* Michael Emil as Abu Bashati
* Emidio LaVella as Tomaso
* Christopher Durang as Ambulance Attendant
* Danny Devin as Sheriff
* Stanley as Alley Dog
* Franklin as Abus Dog

==Further reading==
* May, Elaine.  . The New York Times. (April 1, 1990), p.&nbsp;15, p.&nbsp;24
* Maslin, Janet.  . The New York Times. April 6, 1990.
* Benson, Sheila.  . The Los Angeles Times. April 11, 1990.
* Kehr, Dave.  . The Chicago Tribune. August 3, 1990.

==External links==
*  
*   on YouTube

 
 
 
 
 
 


 