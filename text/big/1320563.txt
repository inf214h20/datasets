Shall We Dance? (1996 film)
{{Infobox film
| name           = 
| image          = Shall We Dansu.jpg
| image size     = 
| caption        = Publicity poster
| film name      = Shall we ダンス?
| director       = Masayuki Suo
| producer       = Yasuyoshi Tokuma 
| writer         = Masayuki Suo
| narrator       = 
| starring       = Kōji Yakusho Tamiyo Kusakari
| music          = Yoshikazu Suo
| cinematography = Naoki Kayano
| editing        = Junichi Kikuchi
| distributor    = Toho
| released       =  
| runtime        = 136 minutes  (Japan)  
| country        = Japan
| language       = Japanese
| gross          = $42,976,677
| preceded_by    = 
| followed_by    = 
}}
  is a 1996 Japanese film. Its title refers to the song, "Shall We Dance?" which comes from Rodgers and Hammersteins The King and I. It was directed by Masayuki Suo.

==Plot==
The film begins with a close-up of the inscription above the stage in the ballroom of the   by William Shakespeare. As the camera pans around the ballroom giving a view of the dancers, a voice-over explains that in Japan, ballroom dancing is treated with suspicion.

Shohei Sugiyama (Kōji Yakusho) is a successful salaryman, with a house in the suburbs, a devoted wife, Masako (Hideko Hara), and a teenage daughter, Chikage (Ayano Nakamura). He works as an accountant for a firm in Tokyo. Despite these external signs of success, however, Sugiyama begins to feel as if his life has lost direction and meaning and falls into Depression (mood)|depression.

One night, while coming home on the Tokyo Subway, he spots a beautiful woman with a melancholy expression looking out from a window in a dance studio. This is Mai Kishikawa (Tamiyo Kusakari), a well-known figure on the Western ballroom dance circuit. Sugiyama becomes infatuated with her and decides to take lessons in order to get to know her better.

Sugiyamas life changes once his classes begin. Rather than Mai, his teacher is Tamako Tamura ( ) who joined to impress his wife, and Masahiro Tanaka (Hiromasa Taguchi) who joined to lose weight. He also meets Toyoko Takahashi (  to participate in Western ballroom dance.

Later, after being rebuffed by Mai, Sugiyama discovers to his surprise that his passion for ballroom dance outweighs his infatuation with her. Indeed dancing, rather than Mai, gives Sugiyama the meaning in life that he was looking for.

Masako, noticing his odd behavior, thinks that he is having an affair — so she hires a private detective to follow him. Meanwhile, along with his classmates, Sugiyama enters an amateur competition – only to find out that his wife, having finally learned the truth from the detective (who has now become a devoted fan of ballroom dancing) is in the audience. Surprised by this, he stumbles and nearly knocks his dance partner to the floor. Though he is able to catch her, he accidentally rips the skirt of her dress off. Both leave the contest. Later, they learn that Tomio won the contest. When Tomio is ridiculed at work after his colleagues read of his success in the newspaper, Sugiyama stands up and tells them not to make fun of something they dont understand.

At home, Sugiyamas wife tries to understand her husbands new passion by asking him to teach her to dance as well. He is invited to a good-bye party for Mai, who is leaving for Blackpool. At the party, Mai joins him to dance, asking him "Shall we dance?"

== Cast ==
*Kōji Yakusho - Shohei Sugiyama
*Tamiyo Kusakari - Mai Kishikawa
*Naoto Takenaka - Tomio Aoki
*Eriko Watanabe - Toyoko Takahashi 
*Yu Tokui - Tokichi Hattori
*Hiromasa Taguchi - Masahiro Tanaka
*Reiko Kusamura - Tamako Tamura
*Hideko Hara - Masako Sugiyama
*Hiroshi Miyasaka - Macho
*Kunihiko Ida - Teiji Kaneko
*Amie Toujou - Hisako Honda
*Ayano Nakamura - Chikage Sugiyama
*Katsunari Mineno - Keiri-kacho
*Tomiko Ishii - Haruko Haraguchi
*Masahiro Motoki - Hiromasa Kimoto

==Reception==
Shall We Dance? received a 93% rating from   stated in the Chicago Sun Times that Shall We Dance? is "one of the more completely entertaining movies Ive seen in a while&mdash;a well-crafted character study that, like a Hollywood movie with a skillful script, manipulates us but makes us like it."   Critic Paul Tatara noted that "It isnt really fair to suggest that the movies main subject is dance, though. As much as anything else, its about the healing powers (and poetry) of simple Emotional expression|self-expression." 
 American theaters earning roughly $9.7 million during its US release. 

==Awards== Japanese Academy Awards it won 14 awards: Best Actor, Best Actress, Best Art Direction, Best Cinematography, Best Director, Best Editing, Best Film, Best Lighting, Best Music Score, Best Screenplay, Best Sound, Best Supporting Actor, Best Supporting Actress, and Newcomer of the Year (in short, every award it was eligible to win). 

==Releases==
The U.S. theatrical cut of the film cuts 26 scenes from the original Japanese version, reducing its running time from 136 minutes to 119 minutes. Additionally, the voiceover narration at the beginning of the film is different:  the Japanese version introduces the history of ballroom dancing in Europe, while the American version explains that ballroom dancing is considered shameful or embarrassing by some Japanese because of cultural norms. Finally, the subtitles include certain explanations of Japanese culture that are not in the original.

The U.S. and European DVD releases also featured this cut of the film.

==U.S. remake== Shall We American remake of this film. It stars Richard Gere, Susan Sarandon, and Jennifer Lopez.

==See also==
*Strictly Ballroom (1992)

==Further reading==
* 

==Notes==
 

==External links==
*  
*  at Rotten Tomatoes
* 
*  

{{Navboxes|list1=
 
 
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 