Christabel (2001 film)
{{Infobox film
| name           = Christabel
| image          = 
| caption        = 
| film name      = 
| director       = James Fotopoulos
| producer       = James Fotopoulos
| screenplay     = James Fotopoulos
| based on       =  
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = Timothy Farrell
| studio         = 
Fantasma Inc. Facets Video (DVD)
| released       = 2001  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 the unfinished poem of the same name by Samuel Taylor Coleridge. 

==Production==
Christabel was Fotopoulos’ first feature-length narrative film production|production, consisting of two half-hour segments shot on digital video and two short sequences shot in 16mm film.      As an adaptation, it eliminates some of the male characters from the Coleridge text and focuses on the theme of one woman commandeering an evil possession of another. 

==Plot==
 

==Cast==
* Kiersten DeBrower as Geraldine
* Jenna Lecce as Sir Leoline
* Veronica Sheaffer as Christabel
* Cherise Silvestri as Bard Bracy

==Release== Facets Video.

==Critical response==
Austin Chronicle wrote that Chistabel "poses perceptual and emotional challenges to his viewers", and that within the film  "sexual symbolism is dense and not for all tastes."   
 Phil Hall of Film Threat panned the film, writing "for those who actively loathe experimental cinema, please avoid James Fotopoulos’ “Christabel” at all costs. And for those who actively love experimental cinema…well, the same advice applies", expanding that as a “loose adaptation of the poem by Samuel Taylor Coleridge, If this adaptation was any looser, it would fall off the screen." He found the film to be both plotless and pointless, and one that "offers absolutely nothing which could even vaguely or charitably be defined as art, imagination or stimulation."   

Conversely, Chicago Reader wrote "Chicagoan James Fotopoulos has garnered critical acclaim", and that of his film Christabel, it was a "creepy, beautiful" feature,     and of the films screening at the 2002 New York Underground Film Festival, Christian Science Monitor felt that it was a "frontrunner in the festivals avant-garde lineup",    with Independent Film & Video Monthly writing Cristabel would "set festivals ablaze". 

==References==
 

==External links==
*  
*  

 
 