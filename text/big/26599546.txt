Princess Fragrance (film)
 
 
{{Infobox film name = Princess Fragrance image = Princess Fragrance (film).jpg caption = VCD cover art traditional = 香香公主 simplified = 香香公主 pinyin = Xiāng Xiāng Gōng Zhǔ}} director = Ann Hui producer = Shen Minhui Guo Fengqi story = Louis Cha screenplay = Chun Sai-sam starring =  music = Chow Git Law Wing-fai cinematography = Bill Wong editing = Chau Muk-leung studio = Sil-Metropole Organisation distributor =  released =   runtime = 94 minutes country = Hong Kong language = Mandarin budget =  gross = HK$5,608,797.00
}} Louis Chas novel The Book and the Sword. The film is a sequel to The Romance of Book and Sword, released earlier in the same month and also directed by Ann Hui.

The film is largely seen as a faithful adaptation of the original novel; however, it is less available now due to limited releases on VCD and DVD. 

==Plot==
The film covers the second half of the Louis Chas novel The Book and the Sword. It introduces another protagonist, Princess Fragrance, who does not appear in the first film The Romance of Book and Sword.

==Cast==
*Zhang Duofu as adult Chen Jialuo
**Jiang Wei as young Chen Jialuo
*Aiyinuo as Princess Fragrance
*Chang Dashi as Qianlong Emperor
*Liu Jia as Huoqingtong
*Ding Cuihua as Luo Bing
*Lü Yongquan as Taoist Wuchen
*Yu Dalu as Zhao Banshan
*Guo Bichuan as Wen Tailai
*Wang Jingqiu as Zhang Jin
*Hou Changrong as Yu Yutong
*Chen Youwang as Xu Tianhong
*Ren Naichang as Shi Shuangying
*Zhang Jun as Chang Bozhi
*Wang Wei as Chang Hezhi
*Zheng Jianhua as Jiang Sigen
*Fu Yongcai as Wei Chunhua
*Sun Chenxi as Xinyan
*Wang Hongtao as Yu Wanting
*Wu Chunsheng as Zhang Zhaozhong
*Yang Junsheng as Heshen
*Ding Tao as Zhaohui
*Si Gengtian as Messenger
*ASier as Dahu
*Kuli Sitan as Erhu
*Fan Yin as Sanhu
*Jiapaer as Sihu
*Hadier as Muzhuolun
*Aniwaer as Huoayi

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 