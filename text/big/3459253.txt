The Secret of the Telegian
{{Infobox film
| name           = The Secret of the Telegian
| image          = Secret of Telegian 1960.jpg
| image_size     = 
| caption        = Theatrical poster for Densō Ningen (1960)
| director       = Jun Fukuda
| producer       = Tomoyuki Tanaka
| writer         = Shinichi Sekizawa

| starring       = Koji Tsuruta   Akihiko Hirata   Yoshio Tsuchiya   Tadao Nakamaru   Yumi Shirakawa   Seizaburô Kawazu   Sachio Sakai   Yoshifumi Tajima
| music          = Sei Ikeno
| cinematography = Kazuo Yamada
| editing        = Kazuji Taira
| distributor    =   Toho    Herts-Lion International Corp.
| released       =   April 10, 1960
| runtime        = 85 min.
| country        =   Japanese English English
}}
 mystery film. Toho Company, Dungeons of Horror. This proposed US theatrical release was aborted, and the film was subsequently syndicated to TV. Besides being in black and white, the TV prints were identical to Tohos uncut international English version, dubbing and all. 

==Plot==
Sudo (Tadao Nakamaru) is a disfigured World War II soldier who had been wounded, robbed and left for dead by his fellow soldiers years before. Under the alias Goro Nakamoto, he is armed with the bayonet that had been used to wound him, and uses the Cryotron, a device that can give its user the ability to teleport anywhere. The inventor of the machine, Professor Niki, is unaware that it is being used for such a purpose.

As an act of vengeance, Sudo uses the Cryotron to hunt down the soldiers, killing them one by one and leaving an ID tag on each of the bodies. Reporter Masaru Kirioka (Koji Tsuruta) aids detectives Kobayashi (Akihiko Hirata) and Okazaki (Yoshio Tsuchiya), who have been assigned to investigate the murders.

==Cast==
* Koji Tsuruta - Masaru Kirioka (Journalist) 
* Akihiko Hirata - Kobayashi (Inspector) 
* Yumi Shirakawa - Akiko Nakajou 
* Tadao Nakamaru - Corporal Sudo/“Densou-Ningen”/Goro Nakamoto
* Seizaburou Kawazu - Oonishi (President of Kainan) 
* Yoshifumi Tajima - Ryuu-Syougen (Owner of cabaret Dai-hon-ei)
* Sachio Sakai - Taki (President of building company) 
* Yoshio Tsuchiya - Okazaki (Detective chief investigation) 
* Fuyuki Murakami - Dr. Miura 
* Takamaru Sasaki - Dr. Niki 
* Junichirou Mukai - Director at Metropolitan Police Department 
* Shin Ootomo - Tsukamoto 
* Shirou Tsuchiya - Director of Tamagawa-En 
* Ikio Sawamura - Attraction man of Chiller House 
* kiyoshi kodama - Man Visitor of Chiller House 
* Sachiyuki Uemura - Man Visitor of Chiller House 
* Jirou Kumagai - Man visitor of Chiller House 
* Yoshie Kidaira - Woman visitor of Chiller House 
* Akira Sera - Genzou (Villager) 
* Senkichi Oomura - Otokichi (Villager) 
* Yutaka Nakayama - Oono (Employee of Dai-hon-ei)
* Toshio Miura - Waiter of Dai-hon-ei
* Yasuhiko Saijou - Waiter of Dai-hon-ei
* Kenzou Echigo - Driver of Jeep 
* Toshiko Higuchi - Yumiko (Hostess of Dai-hon-ei)
* Rumi Konishi - Tomiko (Hostess of Dai-hon-ei)
* Akemi Ueno - Hostess of Dai-hon-ei
* Eisei Amamoto - Employee of trading company Kainan 
* Syouichi Hirose - Employee of trading company Kainan 
* Nadao Kirino - Employee of trading company Kainan

==References==
 

==External links==
*  
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 