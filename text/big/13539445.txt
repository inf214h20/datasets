The Fox (1921 film)
 
{{Infobox film
| name           = The Fox
| image	         = The Fox FilmPoster.jpeg
| caption        = Film poster
| director       = Robert Thornby
| producer       = Universal Film Manufacturing Company Harry Carey Arthur Henry Gooden Lucien Hubbard
| starring       = Harry Carey
| cinematography = William Fildew
| editing        = 
| distributor    = Universal Film Manufacturing Company
| released       =  
| runtime        = 70 minutes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 lost 1921 silent Western Western film Harry Carey.    Directed by Robert Thornby, it was produced and distributed by Universal Film Manufacturing Company. 

==Cast== Harry Carey as Ol Santa Fe
* C. E. Anderson as Rollins
* Harley Chambers as Hubbs
* Gertrude Claire as Mrs. Farwell
* Betty Ross Clarke as Annette Fraser (credited as Betty Ross Clark) George Cooper as K.C. Kid
* B. Reeves Eason Jr. as Pard Alan Hale as Rufus B. Coulter
* John Harron as Dick Farwell (as Johnny Harron) Charles Le Moyne as Black Mike George Nichols as Sheriff Mart Fraser
* Gertrude Olmstead as Stella Fraser

==See also==
* List of American films of 1921
* Harry Carey filmography

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 
 


 
 