American Stories, Food, Family and Philosophy
{{Infobox film
| name           = American Stories, Food, Family and Philosophy
| image          = American Stories, Food, Family and Philosophy.jpg
| caption        = Film poster
| director       = Chantal Akerman
| producer       = Bertrand Van Effenterre
| writer         = Chantal Akerman
| starring       = Mark Amitin
| music          = 
| cinematography = Luc Benhamou
| editing        = Patrick Mimouni
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Belgium
| language       = French
| budget         = 
}}

American Stories, Food, Family and Philosophy ( ) is a 1989 Belgian drama film directed by Chantal Akerman. It was entered into the 39th Berlin International Film Festival.   

==Cast==
* Mark Amitin
* Eszter Balint
* Stephan Balint
* Kirk Baltz
* George Bartenieff
* Billy Bastiani
* Jacob Becker
* Isha Manna Beck
* Max Brandt
* Maurice Brenner
* David Buntzman
* Marilyn Chris
* Sharon Diskin
* Carl Don
* Dean Zeth Jackson as Teitelbaum
* Judith Malina
* Irina V. Passmoore (as Irina Pasmur)
* Herbert Rubens as Martin
* Victor Talmadge

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 