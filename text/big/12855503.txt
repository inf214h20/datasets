Jealous James
 
{{Infobox film
| name           = Jealous James 
| image          = 
| image size     = 
| caption        = 
| director       = 
| producer       = Arthur Hotaling
| writer         = E. W. Sargent	
| narrator       = 
| starring       = Jerold T. Hevener
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  English intertitles
| budget         = 
}}
 1914 silent silent comedy film featuring Oliver Hardy.

==Cast==
* Jerold T. Hevener - Jim Jenkins
* Marguerite Ne Moyer - Clara Jenkins
* Eva Bell - Maude Mullen
* Bert Tracy - Sam (as Herbert Tracy)
* Raymond McKee - Harry
* Oliver Hardy - Grocery Boy (as Babe Hardy)

==See also==
* List of American films of 1914
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 

 