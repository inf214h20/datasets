Draft Day
 
{{Infobox film
| name            = Draft Day
| image           = Draft Day poster.jpg
| alt             = 
| caption         = Theatrical release poster
| director        = Ivan Reitman
| producer        = Ivan Reitman Ali Bell Joe Medjuck Gigi Pritzker
| writer          = Rajiv Joseph Scott Rothman
| starring        = Kevin Costner Jennifer Garner Denis Leary Frank Langella Tom Welling Sam Elliott Ellen Burstyn Chadwick Boseman
| music           = John Debney
| cinematography  = Eric Steelberg
| editing         = Sheldon Kahn Dana E. Glauberman Montecito Picture Company Lionsgate
| released        =  
| runtime         = 110 minutes  
| country         = United States
| language        = English
| budget          = $25 million   
| gross           = $29.5 million 
}} sports drama general manager number one NFL Draft.
 premiered in Los Angeles on April 7, 2014,  with its United States release following on April 11.

==Plot== general manager consistently losing salary cap coached the Browns before Weaver Jr. fired him, and died a week before the draft. His mother is upset at him for missing the reading of Weaver Sr.s will.

The Seattle Seahawks hold the first overall pick, which general manager Tom Michaels offers to trade to the Browns. Weaver declines, but before leaving for the draft team owner Anthony Molina—dissatisfied with current quarterback Brian Drew—orders him to accept. The Browns give their three first-round draft picks over the next three years for the top pick. Many Seahawks fans want Callahan, however, and express their displeasure with picket signs at CenturyLink Field and on social media, demanding Michaels firing.
 front office Florida State. Ohio State, sacked Callahan four times. Teams contact Weaver for possible transactions based on the trade; one from the Houston Texans implies that Mack may not remain available to the Browns in the second round.

The only flaws in Callahan are possible character issues: None of his teammates ostensibly attending his birthday party, and Callahan allegedly lying to the Washington Redskins about reading the teams playbook. When the draft begins that evening at Radio City Music Hall, the Browns have ten minutes to make the first overall pick. Weaver abruptly chooses Mack; Roger Goodells announcement of the selection amazes the league and the front office. While a relieved Drew believes that his job is safe, Molina angrily flies back to Cleveland to confront Weaver. Callahan has an anxiety attack, and leaves the theater until his agent persuades him to return. The pick surprises Penn as much as it does the others, but he discovers that before dying Weaver Sr. advised his son to choose Mack "no matter what".

Weavers unexpected choice disrupts the draft. Rumors spread about Callahan as other teams avoid him, and the Seahawks will be able to select him with the seventh pick. The Jacksonville Jaguars hold the sixth pick; the teams first two choices are no longer available, but its general manager is wary about Callahan because of the rumors. Weaver persuades the Jaguars to trade the sixth pick for the next three years of the Browns second-round draft picks. Molina arrives and confronts Weaver over choosing Mack; he convinces Molina to let him do his job. Weaver calls Michaels, who wants Callahan, and receives all three first-round draft picks back plus the Seahawks punt returner David Putney. The Seahawks choose Callahan with the sixth pick, and are able to sign him for $7 million less than if the team had made him the first overall selection, and Weaver selects Jennings for Penn with the seventh pick.
 2014 season FirstEnergy Stadium. Weaver laughs as he overhears retired Browns stars Jim Brown and Bernie Kosar congratulate Molina for doing "a hell of a job" with the draft. The team, including Mack, Jennings, and Drew, runs onto the field.

==Cast==
 
* Kevin Costner as Sonny Weaver, Jr.   
* Jennifer Garner as Ali Parker 
* Denis Leary as Vince Penn 
* Frank Langella as Anthony Molina 
* Tom Welling as Brian Drew 
* Ellen Burstyn as Barb Weaver 
* Sam Elliott as Coach Moore 
* Chadwick Boseman as Vontae Mack 
* Rosanna Arquette as Angie 
* Russ Brandon as himself
* Terry Crews as Earl Jennings 
* Arian Foster as Ray Jennings
* Griffin Newman as Rick the Intern  
* Patrick St. Esprit as Tom Michaels
* Chi McBride as Walt Gordon 
* W. Earl Brown as Ralph Mowry 
* Kevin Dunn as Marvin 
* Sean Combs as Chris Crawford 
* Josh Pence as Bo Callahan 
* Wallace Langham as Pete Begler 
* Christopher Cousins as Max Stone
* Patrick Breen as Bill Zotti Pat Healy as Jeff Carson
* Roger Goodell as himself
* Jon Gruden as himself
* Jim Brown as himself
* Bernie Kosar as himself
* Chris Berman as himself
* David Ramsey as Thompson
* Rich Eisen as himself
* Ray Lewis as himself
 

==Production==
When the idea was first made public, the film was to be centered around the Buffalo Bills, but the studio subsequently changed it to the Cleveland Browns because of cheaper production costs in Ohio.

Crowd reactions of fans at the actual 2013 NFL Draft, as well as Cleveland Browns fans at local bars, were filmed. Cameos with real-life NFL figures such as league commissioner Roger Goodell and ESPN sportscaster Chris Berman were filmed before and after the draft took place. The rest of the film began filming on May 8, 2013.

===2014 NFL Draft===
Like in the film, the Cleveland Browns made splashes at the draft, trading up to select quarterback Johnny Manziel with the 22nd pick. The team also made several deals, trading away their fourth pick to the Buffalo Bills but for their ninth pick, as well as their 2015 first round pick. They later traded up to the eighth pick to draft Justin Gilbert. Finally, after watching Manziel drop farther than projected, they again traded up for the 22nd pick. Chris Berman, who played himself in the fictionalized draft, commented at the 2014 NFL Draft, that the events surrounding the Cleveland Browns were more exciting than the film. Unlike the film, the Browns selected the much-hyped Heisman Trophy winning quarterback, as opposed to passing on Bo Callahan, the fictionalized first pick favorite.

===Marketing=== trailer for the film were released on December 23, 2013. 

==Reception==
Draft Day has received mixed to positive reviews from critics. On Rotten Tomatoes, the film has a rating of 61%, based on 145 reviews. The sites consensus reads, "Its perfectly pleasant for sports buffs and Costner fans, but overall, Draft Day lives down to its title by relying too heavily on the sort of by-the-numbers storytelling that only a statistician could love".  On the aggregated review site Metacritic, the film has a score of 54 out of 100, based on 33 critics, indicating "mixed or average reviews". 

Chicago Sun-Times critic Richard Roeper gave the film a "B", stating the film is "a sentimental, predictable, sometimes implausible but thoroughly entertaining, old-fashioned piece."
 preferably while CTE   crisis, the NFL is doubling down on its fantasy of paternalism, and Draft Day is that fantasys porn film." {{cite news|last=Hamilton|first=Jack|title=
Draft Day movie: Kevin Costner and Roger Goodell star in the NFL’s version of Moneyball|url=http://www.slate.com/articles/arts/culturebox/2014/04/draft_day_movie_kevin_costner_and_roger_goodell_star_in_the_nfl_s_version.html|newspaper=Slate (website)|Slate|date=April 10, 2014|accessdate=April 11, 2014}} 

Former Green Bay Packers vice president Andrew Brandt criticized Draft Day as "lacking any true depiction of how an NFL team operates leading up to and during the draft", and less realistic about the business of sports than Jerry Maguire and Moneyball (film)|Moneyball.   

===Box office===
The film grossed $9,783,603 in its opening weekend, finishing in fourth place at the box office behind  , Rio 2, and Oculus (film)|Oculus (the latter two also being new releases). 

 , the film has grossed $28,842,237 domestically with an additional $604,801 overseas for a worldwide total of $29,447,038    against a $25 million budget. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 