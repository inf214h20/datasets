Saatchya Aat Gharat
{{Infobox film
| name           = Saatchya Aat Gharat
| image          = Saatchya Aat Gharat.jpg
| image_size     =
| caption        =  Theatrical release poster
| director       = Sanjay Surkar
| producer       = Smita Talwalkar
| writer         = Sanjay Surkar Nishikant Kamat
| screenplay     = Sanjay Pawar
| narrator       = Reema Lagoo
| starring       = Manav Kaul Kartika Rane Nishikant Kamath Makarand Anaspure Vibhawari Deshpande Bharati Achrekar  Bharat Ganeshpure  Suhas Joshi Reema Lagoo  Neena Kulkarni
| music          = Rahul Ranade
| cinematography = Sanjay Jadhav
| editing        =
| distributor    = Asmita Chitra
| released       =  
| runtime        =
| country        = India
| language       = Marathi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Marathi movie released on 31 May 2004. The film is directed by Sanjay Surkar and produced by Smita Talwalkar.  It is partly based upon a 2002 incident in the University of Pune campus when a student was raped by someone posing as a policeman.  This was the debut movie for Kartika Rane in Marathi Industry.

== Synopsis ==

The film speaks of societys unchanged outlook towards the women – especially rape victims – despite modernization. The movie centres around the rape of a college girl by an imposter cop. While tackling the issue of morality, the film also discusses the pub-crawling, body-piercing, jeans-clad youth culture.

==Cast==
The movie stars Kartika Rane, Manav Kaul, Nishikant Kamat, Beneka, Mrunmayee Lagoo, Amruta Patki, Vibhavari Deshpande, Makarand Anaspure, Rakhi Sawant, Neena Kulkarni, Deepa Limaye, Suhas Joshi, Smita Talwalkar, Bharti Achrekar, Deepa Lagu, Prasanna Ketkar, Dr. Damle, Sharad Avasthi, Dr. Girish Oak, Nilu Phule, and Uday Tikekar. 

Below are the details of main cast for Saatchya Aat Gharat. 
*Vibhawari Deshpande as Ketaki
*Makarand Anaspure as Yuvraj
*Bharat Ganeshpure as Advocate
*Reema Lagoo as Narrator
Bharati Achrekar, Suhas Joshi, Neena Kulkarni and Kartika Rane.

==Credits==
The films opening credits list the following filming locations: 
* Symbiosys InfoPark College
* Deccan Gymkhana, Talwalkar Gym
* Law College
* Sams Garden
* Agarwal Bungalow 
* Mokashi Bungalow
* V I T College Hostel 
* Kavi Bar
* Film and Television Institute of India
* Durga Tekdi
* Pancard Club
* Chatushrungi
* Mahesh Sanskrutik Bhavan
* Chandivali Studio
* Raj Farms
* UTI Bank
* K K Bazaar
* Patankar Bungalow
* Bhave Bungalow

==Awards==

===Nominations===
* 2005 Screen Awards: Best Film (Marathi)
* 2005 Screen Awards: Best Director (Marathi) - Sanjay Soorkar
* 2005 Screen Awards: Best Actress (Marathi) - Beneka

==Soundtrack==

===Tracklist===
Following table shows list of tracks in the film. 
{| class="wikitable"
! Track # 
! Song 
! Duration
|-
| 1 || Baghata Baghata|| 1:41
|-
| 2 || Hill Hil Pori Hila || 4:47
|-
| 3 || Navi Navalai || 2:28
|-
| 4 || Tu An Mi || 5:44
|}

==References==
 

 

 
 
 