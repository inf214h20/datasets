The Neon Bible (film)
 
 
{{Infobox film
| name           = The Neon Bible
| image          = DVD cover of The Neon Bible.jpg
| image_size     =
| caption        =
| director       = Terence Davies
| producer       = Elizabeth Karlsen Nik Powell Olivia Stewart Victoria Westhead Stephen Woolley
| screenplay     = Terence Davies
| based on       =  John Kennedy Toole.
| narrator       =
| starring       = Jacob Tierney Gena Rowlands Diana Scarwid Denis Leary
| music          =
| cinematography = Michael Coulter
| editing        = Charles Rees
| distributor    = Strand
| released       =
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = $78,072
| preceded_by    =
| followed_by    =
}}
 the novel Georgia in the 1940s. His abusive father (Denis Leary) enlists in the army during World War II and disappears and David is left to take care of his mother (Diana Scarwid) with his Aunt Mae (Gena Rowlands) who is a singer. It was filmed in the state of Georgia, in the cities Atlanta, Crawfordville, Georgia|Crawfordville, and Madison, Georgia|Madison.

The film was released in France in August 1995, the United Kingdom in October 1995, Australia in November 1995, and released in the United States on 1 March 1996. It was a selection of the 1995 New York Film Festival.

==Cast==
* Jacob Tierney as David, aged 15
* Drake Bell as David, aged 10
* Gena Rowlands as Mae Morgan
* Diana Scarwid as Sarah
* Denis Leary as Frank
* Bob Hannah as George
* Aaron Frisch as Bruce
* Charles Franzen as Tannoy Voice
* Leo Burmester as Bobbie Lee Taylor
* Sherry Velvet as First Testifier
* Stephanie Astalos-Jones as Second Testifier
* Ian Shearer as Billy Sunday Thompson
* Joan Glover as Flora
* Jill Jane Clements as Woman
* Tom Turbiville as Clyde
* Sharon Blackwood as Schoolmistress
* Peter McRobbie as Reverend Watkins
* Ken Fight as Schoolmaster
* Dana Seltzer (credited as Dana Atwood) as Jo Lynne
* Virgil Graham Hopkins as Mr. Williams
* Ducan Stewart as Boy in Drugstore
* J.T. Alessi as Boy in Drugstore
* Duncan Stewart as Head Boy
* Frances Conroy as Miss Scover
* Marcus Batton as School Boy

==Reception== Thomas Hart Benton painting come to life."  Judd Blaise of Allmovie gave the film 2  out of 5 stars and said "Some viewers will likely be frustrated by the slow pace and elliptical style, though others may be transfixed by the often stunning photography and poetic approach."  The New York Times film critic Stephen Holden said one of the problems with the film was that it "may have succumbed to its own dreamy esthetic" by focusing on the same image too often, and that the end of the film "loses its balance." 
 The Tech, The Long Day Closes but that the plot was weak and that the ending of the film was absurd.  San Francisco Examiner critic Barry Walters said the film was "unrelentingly downbeat" and that "it starts off dark and gets darker". He called it "one long crawl into an emotional abyss without catharsis" and said that the director Davies had created a nightmare. 
 The House of Mirth without it." 

Shown on three screens in the United States, the film grossed $78,072 in its theatrical release. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 