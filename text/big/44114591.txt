Dangerous Venture
{{Infobox film
| name           = Dangerous Venture
| image          = Dangerous Venture poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George Archainbaud
| producer       = Lewis J. Rachmil
| screenplay     = Doris Schroeder William Boyd Fritz Leiber Douglas Evans Harry Cording
| music          = David Chudnow 
| cinematography = Mack Stengler
| editing        = Fred W. Berger
| studio         = Hopalong Cassidy Productions
| distributor    = United Artists
| released       =  
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Fritz Leiber, Douglas Evans and Harry Cording. The film was released on May 23, 1947, by United Artists.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*Andy Clyde as California Carlson
*Rand Brooks as Lucky Jenkins Fritz Leiber as Chief Xeoli Douglas Evans as Dr. Grimes Atwood
*Harry Cording as Dan Morgan
*Betty Alexander as Dr. Sue Harmon
*Francis McDonald as Henchman Kane
*Neyle Morrow as Jose
*Patricia Tate as Talu
*Bob Faust as Henchman Stark
*Kenneth Tobey as Red
*Jack Quinn as The Marshal
*Bill Nestell as Pete, the Cook 

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 
 
 

 