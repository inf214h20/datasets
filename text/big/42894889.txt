A Grim Becoming
{{Infobox film
| name           = A Grim Becoming
| image          = File:A Grim Becoming film poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Adam R. Steigert
| producer       = 
| writer         = Janeen Avery, Christopher Brechtel, Mark Mendola, T.R. Smith, Adam R. Steigert
| screenplay     = 
| story          = 
| based on       =  
| starring       = Bill Oberst Jr., Jessica Cameron, Brandyn T. Williams, Michael Sciabarrasi
| narrator       = 
| music          = 
| cinematography = Matthew A. Nardone
| editing        = Christopher Burns Jr., Adam R. Steigert
| studio         = DefTone Pictures Studios
| distributor    = DefTone Pictures Studios
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Grim Becoming is a 2014 horror comedy film that was directed by Adam R. Steigert.      The film had its world premiere on October 31, 2014 and stars Brandyn T. Williams as a young businessman that finds he has become a Grim Reaper.   Including writing, production for A Grim Becoming took place over a two and a half year period, with filming taking place in New York during the summer of 2013.  

==Synopsis==
Raphael (Brandyn T. Williams) is an executive on the cusp of either making it big or losing it all, depending on how a deal with a large distribution company goes. His co-worker Wayne (Britt Griffith) would love to see Raphael fail so he can himself progress within the business, a situation that is made worse when Raphael must take time off of work to go to the funeral of a family member in Metzburgh. On his way to Metzburgh Raphael witnesses a Grim Reaper claiming a soul and ends up becoming a Grim Reaper himself. Raphael now has to find out what hes willing to do to get this status reversed and what Death (Michael Sciabarrasi) himself has planned for him.

==Cast==
*Brandyn T. Williams as Raphael (as Brandon Williams)
*Michael Sciabarrasi as Magoo/Death
*Bill Oberst Jr. as Phill
*Britt Griffith as Wayne
*Jessica Cameron as Life
*Lynn Lowry as Mother
*Melantha Blackthorne as Meyrl
*Devanny Pinn as Jamie

==Reception== cult classic.  In contrast Nerdly panned the film, criticizing it for its "childish jokes" and "technical issues as well which destroy any redeeming qualities that this film might have had." 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 