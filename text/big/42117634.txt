Friends (2002 film)
{{Infobox film
| name = Friends
| image = 
| caption =
| director = M. D. Sridhar
| writer = M. D. Sridhar Sharan  Ruthika   Vasu   Anand
| producer = M. C. Neha
| music = G. Krishna   Ramesh Krishna (background score)
| cinematography = Ramana
| editing = P. R. Soundar Raj
| studio = Komal Enterprises
| released =  
| runtime = 139 minutes
| language = Kannada
| country = India
| budget =
}} comedy film Telugu film 6 Teens (2001). The film features newcomers Vasu, Ruthika, Sharan (actor)|Sharan, Anand and Shyam in the lead roles. 

The film featured original score by Ramesh Krishna and soundtrack composed by G. Krishna who composed the songs in the original version as well. The film, upon release received high critical and commercial success and was one of the top grossers of the year 2002. 

== Cast ==
* Vasu
* Anand Sharan
* Ruthika
* Shyam
* Hari
* Mimicry Dayanand
* Tennis Krishna
* Kashi
* Rekha Das

== Soundtrack == Deva who also coincidentally reused the song "Devaru" as "Oru Varam" in its Tamil remake.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Devaru Varavanu Kotre
| extra1 = Kumar Sanu
| lyrics1 = K. Kalyan
| length1 = 06:11
| title2 = Tirupathi Tirumala Venkatesha
| extra2 = Ashok Sharma
| lyrics2 = K. Kalyan
| length2 = 04:58
| title3 = Sixteen Years Urvashi
| extra3 = Suresh Peters
| lyrics3 = K. Kalyan
| length3 = 04:32
| title4 = Thakadimi Thakadimi
| extra4 = Udit Narayan
| lyrics4 = K. Kalyan
| length4 = 04:59
| title5 = Styla Model
| extra5 = Hemanth
| lyrics5 = K. Kalyan
| length5 = 04:50
| title6 = Savira Kanasina
| extra6 = Udit Narayan, Manjula Gururaj
| lyrics6 = K. Kalyan
| length6 = 04:47
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 