A Night in May
{{Infobox film
| name           = A Night in May
| image          = A Night in May DVD Cover.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = A Night in May DVD Cover
| director       = Georg Jacoby
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  Marika Rökk
*Viktor Staal
*Karl Schönböck
 
| music          = 
| cinematography = Robert Baberske
| editing        =  UFA
| distributor    = 
| released       =   
| runtime        = 
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}} German historical historical comedy UFA the leading German studio.

==Cast==
* Marika Rökk - Inge Fleming 
* Viktor Staal - Willy Prinz 
* Karl Schönböck - Waldemar Becker 
* Gisela Schlüter - Alma 
* Oskar Sima - Direktor Fleming Jr. 
* Albert Florath - Direktor Fleming Sr. 
* Ingeborg von Kusserow - Friedl 
* Mady Rahl - Mimi 
* Franz Arzdorf - Berghoff 
* Ursula Herking - Johanna 
* Ludwig Schmitz - Schupo Emil

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Rentschler, Eric. The Ministry of Illusion: Nazi Cinema and Its Afterlife. Harvard University Press, 1996.

==External links==
* 
* Ein Nacht im Mai at MoviePilot.de  
 

 
 
 
 
 
 
 


 