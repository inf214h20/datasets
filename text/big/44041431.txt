Surveykkallu
{{Infobox film
| name = Surveykkallu
| image =
| caption =
| director = Thoppil Bhasi
| producer =
| writer = Thoppil Bhasi
| screenplay = Lakshmi Manavalan Joseph Mohan Sharma
| music = G. Devarajan
| cinematography = U Rajagopal
| editing = G Venkittaraman
| studio = Chithramala
| distributor = Chithramala
| released =  
| country = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film, directed by Thoppil Bhasi. The film stars KPAC Lalitha, Lakshmi (actress)|Lakshmi, Manavalan Joseph and Mohan Sharma in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  
*KPAC Lalitha  Lakshmi 
*Manavalan Joseph 
*Mohan Sharma 
*Sankaradi 
*MG Soman 
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kanakathalikayil || P. Madhuri || ONV Kurup || 
|- 
| 2 || Mandaakini || K. J. Yesudas || ONV Kurup || 
|- 
| 3 || Poothumbi || K. J. Yesudas, P. Madhuri || ONV Kurup || 
|- 
| 4 || Thenmalayude || P Jayachandran, P. Madhuri || ONV Kurup || 
|- 
| 5 || Vipanchike || P. Madhuri || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 


 