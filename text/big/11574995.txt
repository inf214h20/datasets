The Devonsville Terror
{{Infobox Film
| name           = The Devonsville Terror
| image_size     = 180px
| image	=	The Devonsville Terror FilmPoster.jpeg
| caption        = Cover art of 1984 VHS release
| director       = Ulli Lommel
| producer       = Charles Aperia Jochen Breitenstein David Dubay Ulli Lommel Tim Nielsen Bill Rebane
| writer         = George T. Lindsey Ulli Lommel Suzanna Love
| narrator       = 
| starring       = Suzanna Love Donald Pleasence Robert Walker Jr.
| music          = Ray Colcord
| cinematography = Ulli Lommel
| editing        = Richard S. Brummer
| distributor    = Motion Picture Marketing Embassy Pictures
| released       = October 1983
| runtime        = 82 min.
| country        = USA English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1983 horror Robert Walker. The plot focuses on three different women who arrive in a conservative New England town, one of whom is the reincarnation of a witch who was wrongfully executed along with two others by the towns founding fathers in 1683.
 Embassy Home The Boogeyman (1980).

==Plot==
The film opens in 1683 Massachusetts where three women in the town of Devonsville are kidnapped by the townsfolk and systematically tortured and executed. After the final womans execution, her apparition appears in the sky and a thunderstorm begins.

Then, 300 years later, Dr. Warley (Pleasence) investigates the witchs purported curse on Devonsville. Three liberated, assertive women move into town, which angers the bigoted, male-dominated town fathers. One of the women is a reincarnation of the witch, who proceeds to exact revenge on the town males. Meanwhile, Dr. Warley fights a supernatural illness resulting from his lineage to the towns founding fathers who were responsible for the execution.

==Main cast==
*Suzanna Love as Jenny Scanlon
*Donald Pleasence as Dr. Warley
*Robert Walker, Jr. (as Robert Walker) as Matthew Pendleton
*Paul Willson as Walter Gibbs
*Mary Walden as Chris
*Deanna Haas as Monica
*Michael Accardo as Ralph Pendleton
*Bill Dexter as Aaron Pendleton
*Priscilla Lowe as Myrtle Pendleton
*Angelica Rebane as Angel Pendleton

==Production and release== witchcraft inquisition in the colonial era of the United States. Lommel stated that he had spent some time in Massachusetts and was inspired by the Salem Witch Trials.  Star Suzanna Love, Lommels wife, also helped write the film. Filming primarily took place in Lincoln County, Wisconsin. 
 The Boogeyman (1980), which is now out of print. 

==External links==
* 

==References==
 

 
 
 
 


 