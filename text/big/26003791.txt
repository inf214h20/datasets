Judgement (1999 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Judgement
| image          = Judgement1999Poster.jpg
| caption        = Film poster
| film name = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Simpan
 | mr             = Simp‘an}}
| director       = Park Chan-wook
| producer       = Park Chan-wook
| writer         = Park Chan-wook
| starring       = Key Joo-bong Koh In-bae Kwon Nam-hee Park Ji-il Choi Hak-rak Myeong Ji-yeon
| music          =
| cinematography = Park Hyun-Cheol
| editing        = Kim Sang-Bum
| distributor    =
| released       =  
| runtime        = 26 minutes
| country        = South Korea
| language       = Korean
}}
Judgement is a 1999 short film by South Korean director Park Chan-wook.

==Plot== Sampung department Store collapsed, killing about 500 people and injuring many others. This tragedy was caused by human negligence. Half a million dollars were offered in indemnity to the victims relatives, and this triggered a merciless looting made by unscrupulous people. In a morgue where the lifeless body of a girl lies, waiting to be identified, a man and a woman, who introduce themselves as the parents of the victim, are distraught over the loss of the young girl. However, after a moments hesitation, a morgue employee claims the body of the girl. The grotesque controversy about who is the real "owner" of the corpse and the legitimate beneficiary of the money starts here. Between turn of events and paradoxical punishments of fate, the ending leads to a tragic and ironic conclusion. In black and white, the whole story is set in the cold morgue, where the characters absurd and pathetic dialogues take place, like a journalist and a police detective who keep on squabbling. Halfway between Hitchcock and Polanski, Judgement reveals all Parks irony about a shabby, ignorant and greedy humanity, that does not hesitate to come to vile compromises for money.

==Cast==
* Key Joo-bong
* Koh In-bae
* Kwon Nam-hee
* Park Ji-il
* Choi Hak-rak
* Myeong Ji-yeon

==External links==
*  
*  
*   at parkchanwook.org

 

 
 
 
 
 
 
 
 


 