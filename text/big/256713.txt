Dancer in the Dark
 
 
{{Infobox film
| name = Dancer in the Dark
| image = Dancer in the Dark movie poster.jpg
| alt = 
| caption = US theatrical release poster
| director = Lars von Trier
| producer = {{Plainlist|
* Peter Aalbæk Jensen
* Vibeke Windeløv}}
| writer = Lars von Trier
| starring = {{Plainlist|
* Björk
* Catherine Deneuve David Morse
* Peter Stormare
* Jean-Marc Barr
* Joel Grey}}
| music = Björk
| cinematography = Robby Müller
| editing = {{Plainlist|
* François Gédigier
* Molly Marlene Stensgård}}
| studio = {{Plainlist|
* Canal+
* FilmFour
* France 3 Cinéma}}
| distributor = Angel Films
| released =  
| runtime = 140 minutes     
| country = {{Plainlist|
* Denmark
* Argentina
* Finland
* France
* Germany
* Iceland
* Italy
* Netherlands
* Norway
* Spain
* Sweden
* United Kingdom
* United States}}
| language = English
| budget = United States dollar|USD$12.5 million    (120 million Swedish krona|kr)
| gross = $45.6 million    
}} musical drama David Morse, Mark Bell and the lyrics were by von Trier and Sjón. Three songs from Rodgers and Hammersteins The Sound of Music were also used in the film.

This is the third film in von Triers "Golden Heart Trilogy"; the other two films are  , Argentina, Finland, France, Germany, Iceland, the Netherlands, Norway, Spain, Sweden, the United Kingdom, and the United States.. It was shot with a handheld camera, and was somewhat inspired by a Dogme 95 look.
 Academy Award Best Song.

==Plot==
  Washington state Czech immigrant David Morse) and his wife Linda (Cara Seymour). She is also pursued by the shy but persistent Jeff (Peter Stormare), who also works at the factory.

What no one in Selmas life knows is that she has a hereditary degenerative disease which is gradually causing her to go blind. She has been saving up every penny that she makes (in a candy tin in her kitchen) to pay for an operation which will prevent her young son from suffering the same fate. To escape the misery of her daily life, Selma accompanies Cvalda to the local cinema where together they watch fabulous Hollywood musicals (or more accurately, Selma listens as Cvalda describes them to her, to the annoyance of the other theater patrons, or acts out the dance steps upon Selmas hand using her fingers).

In her day-to-day life, when things are too boring or upsetting, Selma slips into daydreams or perhaps a trance-like state where she imagines the ordinary circumstances and individuals around her have erupted into elaborate musical theater numbers. These songs use some sort of real-life noise (from factory machines buzzing to the sound of a flag rapping against a flag pole in the wind) as an underlying rhythm. Unfortunately, Selma slips into one such trance while working at the factory. Soon Jeff and Cvalda begin to realize that Selma can barely see at all. Additionally, Bill reveals to Selma that his materialistic wife Linda spends more than his salary, there is no money left from his inheritance, and he is behind in payments and the bank is going to take his house. He asks Selma for a loan, but she declines. He regrets telling Selma his secret. To comfort Bill, Selma reveals her secret blindness, hoping that together they can keep each others secret. Bill then hides in the corner of Selmas home, knowing she cant see him, and watches as she puts some money in her kitchen tin.

The next day, after having broken her machine the night before through careless error, Selma is fired from her job. When she comes home to put her final wages away she finds the tin is empty; she goes next door to report the theft to Bill and Linda only to hear Linda discussing how Bill has brought home their safe deposit box to count their savings. Linda additionally reveals that Bill has "confessed" his affair with Selma, and that Selma must move out immediately. Knowing that Bill was broke and that the money he is counting must be hers, she confronts him and attempts to take the money back. He draws a gun on her, and in a struggle he is wounded. Linda discovers the two of them and, assuming that Selma is attempting to steal the money, runs off to tell the police at Bills command. Bill then begs Selma to take his life, telling her that this will be the only way she will ever reclaim the money that he stole from her. Selma shoots at him several times, but due to her blindness manages to only maim Bill further. In the end, she performs a coup de grâce with the safe deposit box. In one of the scenes, Selma slips into a trance and imagines that Bills corpse stands up and slow dances with her, urging her to run to freedom. She does, and takes the money to the Institute for the Blind to pay for her sons operation before the police can take it from her.
 Communist sympathizer and murderess. Although she tells as much truth about the situation as she can, she refuses to reveal Bills secret, saying that she had promised not to. Additionally, when her claim that the reason she didnt have any money was because she had been sending it to her father in Czechoslovakia is proven false, she is convicted and given the death penalty. Cvalda and Jeff eventually put the pieces of the puzzle together and get back Selmas money, using it instead to pay for a trial lawyer who can free her. Selma becomes furious and refuses the lawyer, opting to face the death penalty rather than let her son go blind, but she is deeply distraught as she awaits her death. Although a sympathetic female prison guard named Brenda tries to comfort her, the other state officials show no feelings and are eager to see her executed. Brenda encourages Selma to walk. On her way to the gallows, Selma goes to hug the other men on death row while singing to them. However, on the gallows, she becomes terrified, so that she must be strapped to a collapse board. Her hysteria when the hood is placed over her face delays the execution. Selma begins crying hysterically and Brenda cries with her, but Cvalda rushes to inform her that the operation was successful and that Gene will see. Relieved, Selma sings the final song on the gallows with no musical accompaniment, although she is hanged before she finishes. A curtain is then drawn in front of her body, while the missing part of the song shows on the screen: "They say its the last song/They dont know us, you see/Its only the last song/If we let it be."

==Cast==
 
* Björk as Selma Ježková
* Catherine Deneuve as Kathy (Cvalda) David Morse as Bill Houston
* Peter Stormare as Jeff
* Jean-Marc Barr as Norman
* Joel Grey as Oldřich Nový
* Cara Seymour as Linda Houston Siobhan Fallon as Brenda
* Vladica Kostic as Gene Ježek
* Vincent Paterson as Samuel
* Željko Ivanek as District attorney
* Udo Kier as Dr. Pokorný
* Jens Albinus as Morty
* Reathel Bean as Judge
* Michael Flessas as Angry man
* Mette Berggreen as Receptionist
* Lars Michael Dinesen as Defense attorney
* Katrine Falkenberg as Suzan
* Stellan Skarsgård as the doctor
 

==Style==
Much of the film has a similar look to von Triers earlier  s to create a documentary-style appearance. It is not a true Dogme 95 film, however, because the Dogme rules stipulate that violence, Diegesis#Film sound and music|non-diegetic music, and period pieces are not permitted. Trier differentiates the musical sequences from the rest of the film by using static cameras and by brightening the colours.

==Production==
  Dancing in the Dark" from the 1953 film The Band Wagon, which ties in with the films musical theatre theme.

Actress Björk, who is known primarily as a contemporary composer, had rarely acted before, and has described the process of making this film as so emotionally taxing that she would not appear in any film ever again   (although in 2005, she appeared in Matthew Barneys Drawing Restraint 9). She had disagreements with the director over the content of the film, wanting the ending to be more uplifting.  She later called Trier sexist.  Deneuve and others have described her performance as feeling rather than acting.  Björk has said that it is a misunderstanding that she was put off acting by this film; rather, she never wanted to act but made an exception for Lars von Trier. 

The musical sequences were filmed simultaneously with over 100 digital cameras so that multiple angles of the performance could be captured and cut together later, thus shortening the filming schedule.

Björk lies down on a stack of birch logs during the "Scatterheart" sequence. In Icelandic language|Icelandic, Faroese language|Faroese, and Swedish language|Swedish, "Björk" means "birch."
 MY class Great Northern T43 class locomotive was repainted too, though never used in the film.

==Critical response==
Reaction to Dancer in the Dark was polarized. For example, on The Movie Show, Margaret Pomeranz gave it five stars while David Stratton gave it a zero, a score shared only by Geoffrey Wrights Romper Stomper (1992).   Stratton later described it as his "favourite horror film".  Peter Bradshaw of The Guardian said it was "one of the worst films, one of the worst artworks and perhaps one of the worst things in the history of the world."  The response is reflected in the films official website, which posts both positive and negative reviews on its main page.  The diverse reviews result in an overall "Fresh" rating; 68% grade on the review aggregator website Rotten Tomatoes. 

The film was praised for its stylistic innovations.   wrote, "Its great to see a movie so courageous and affecting, so committed to its own differentness." 
 Jonathan Foreman of the New York Post described the film as "meretricious fakery" and called it "so unrelenting in its manipulative sentimentality that, if it had been made by an American and shot in a more conventional manner, it would be seen as a bad joke." 

==Accolades==
  promoting the film at the 2000 Cannes Film Festival.]] Best Actress Oscar for her famous swan dress.

Sight & Sound magazine conducts a poll every ten years of the worlds finest film directors to find out the Ten Greatest Films of All Time. This poll has been going since 1952, and has become the most recognised  poll of its kind in the world. In 2012  Cyrus Frisch voted for "Dancer in the Dark". Frisch commented: "A superbly imaginative film that leaves conformity in shambles."

;Won Award of the Japanese Academy - Best Foreign Film
* Bodil Award - Best Actress (Björk) Cannes Film Festival - Best Actress (Björk) 
* Cannes Film Festival - Golden Palm Award (Lars von Trier) 
* Edda Awards (Iceland) - Best Actress (Björk)
* European Film Awards - Best Actress (Björk)
* European Film Awards - Best Film
* Golden Satellite Awards - Best Original Song (Ive Seen It All)
* Goya Awards - Best European Film (Lars von Trier)
* Independent Spirit Awards - Best Foreign Film (Lars von Trier)

;Nominated Academy Award - Best Original Song (Ive Seen It All)
* Bodil Award - Best Film
* BRIT Awards - Best Soundtrack
* Camerimage Awards - Gold Frog Award
* Chicago Film Critics Association Awards - Best Actress (Björk)
* Chicago Film Critics Association Awards - Best Original Score
* Cinema Writers Circle Awards (Spain) - Best Foreign Film
* César Awards (France) - Best Foreign Film
* Golden Globe Awards - Best Actress in a Film - Drama (Björk)
* Golden Globe Awards - Best Original Song (Ive Seen It All)
* Golden Satellite Awards - Best Drama
* Golden Satellite Awards - Best Actress, Drama (Björk)
* Golden Satellite Awards - Best Supporting Actress, Drama (Catherine Deneuve)

==Music==
 
* Original music: Björk Siobhan Fallon, David Morse, Cara Seymour, Vladica Kostic, Peter Stormare (In the soundtrack Selmasongs, Thom Yorke sings instead of Stormare)
* Lyrics: Björk, Lars von Trier and Sjón
* Non-original music: Richard Rodgers
* Choreographer: Vincent Paterson

==References in other media== Finnish band Dancer in the Dark" in the special edition of their 2005 album Hide from the Sun. The song is about the film.
* Drummer Jeff "Tain" Watts arranged and recorded a version of "107 Steps" on his live album Detained: Live at the Blue Note.
* The character of Selmas father, Oldřich Nový, was an actual movie and musical star in Czechoslovakia.
* The independent spoof film My Big Fat Independent Movie features a musical number in the style of Dancer in the Dark, with the main female character wearing a version of Björks swan dress.

==References==
 
* Georg Tiefenbach: Drama und Regie (Writing and Directing): Lars von Triers Breaking the Waves, Dancer in the Dark, Dogville. Würzburg: Königshausen & Neumann 2010. ISBN 978-3-8260-4096-2.

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
{{succession box European Film Award for Best European Film
| years=2000
| before=All About My Mother
| after=Amélie}}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 