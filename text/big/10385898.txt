Seven Days to Noon
 
 
{{Infobox film
| name           = Seven Days to Noon
| image          = Seven daysPoster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = John Boulting Roy Boulting
| producer       = John Boulting Roy Boulting James Bernard Frank Harvey Barry Jones
| music          = John Addison
| cinematography = Gilbert Taylor
| editing        = John Boulting Roy Boulting
| studio = Charter Film Productions
| distributor    = British Lion Films  
| released       =  
| runtime        = 94 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 drama / James Bernard won the Academy Award for Best Story for this film.

==Plot== Ronald Adam), Barry Jones), Superintendent Folland (André Morell) of Scotland Yards Special Branch is charged with tracking down Willingdon and stopping him.

Arriving at the (fictitious) Wallingford Research Centre (based on the Atomic Weapons Research Establishment {AWRE} at Aldermaston), Follands team find Willingdon missing, along with a nuclear bomb. Willingdons assistant Lane (Hugh Cross) is recruited to help and they return to London to search for him.

Willingdon, carrying his bomb in a Gladstone bag, finds lodgings with Mrs. Peckitt (Joan Hickson), but spooks her with his constant pacing around his room during the night. The following morning, he leaves early and seeing a wanted poster with his face, disguises himself with a new coat and having his moustache shaved off.

Follands team plan for the worst and get Cabinet approval to evacuate London. Rumours begin to fly that another war is about to be declared, and the Prime Minister agrees to do a radio broadcast to try to quash these, and appeal to Willingdon to give himself up.

The next day, Willingdons daughter, Ann (Sheila Manahan), turns up at Follands office to demand some answers. Folland tells her all and asks her to stay and help – she may be the only person the professor will listen to.

Mrs. Peckitt reports Willingdon to the police, thinking that he is a landlady murderer reported in the paper, but a quick-thinking constable realises the description better matches Willingdon and a car is sent to check him.

Unfortunately, Willingdon spots it on his way back to his lodgings and makes a quiet get-away. Driving back to their hotel from the police operations centre, Lane and Ann Willingdon spot the professor and try to catch him. An updated description is quickly circulated.

That evening Willingdon bumps into Mrs "Goldie" Phillips (Olive Sloane), she offers that he can buy her a drink, the two of them having met, by chance, earlier at a pawn brokers. Without any lodgings, Goldie offers him her spare bed for the night. By this time, London is being evacuated and Willingdon decides to lie low. The troops have begun to search and Goldies bedsit seems a good place to remain hidden. Willingdon is forced to hold Goldie hostage, fearing that if he didnt, she would inform the authorities of his location.
 bomb blitzed church. The net steadily closes and Willingdon is finally found, praying. Lane, Ann and Folland arrive to try to talk the professor away from his bag. He panics, runs from the church, and is killed by an even more panicking soldier (Victor Maddern). With seconds to spare, Lane has the bomb defused.

==Main cast==
  Barry Jones as Professor John Malcolm Francis Willingdon
*Olive Sloane as Goldie Phillips
*André Morell as Superintendent Folland
*Sheila Manahan as Ann Willingdon
*Hugh Cross as Stephen Steve Lane
*Joan Hickson as Mrs. Emily Georgina Peckett Ronald Adam as Honorable Arthur Lytton, the Prime Minister
*Marie Ney as Mrs. Willingdon
*Wyndham Goldie as Reverend Burgess, the vicar of Wallingford
*Russell Waters as Detective Davis
*Martin Boddey as General Willoughby
*Frederick Allen as Himself, a BBC announcer
*Victor Maddern as Private Jackson
*Geoffrey Keen as Alf, loudmouth in the pub
*Merrill Mueller as the American commentator
*Joss Ackland as Young policeman at the police station (uncredited)
*Jean Anderson as Mother at Railway Station (uncredited)
*Ernest Clark as Barber (uncredited)
*Sam Kydd as Soldier in House Search (uncredited)
*Bruce Seton as Brigadier Grant (uncredited)
*Marianne Stone as Woman in Phone Box (uncredited)
 

== Awards  ==
The movie received an Academy Award (Oscar) for Academy Award for Best Story|Writing (Motion Picture Story) at the 24th Academy Awards held in 1952 at the  RKO Pantages Theatre.   

==DVD release==
Seven Days to Noon became available on DVD in 2008. It is incorrectly framed in matted widescreen, a process not developed until two years later. 

==References==

===Notes===
 

===Bibliography===
*The Great British Films, pp 144–146, Jerry Vermilye, 1978, Citadel Press, ISBN 0-8065-0661-X

==External links==
* 
*  
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 