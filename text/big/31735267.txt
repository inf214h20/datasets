The Nevadan
{{Infobox film
| name           = The Nevadan
| image          = Poster of The Nevadan.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster Gordon Douglas
| producer       = Harry Joe Brown
| writer         = {{Plainlist|
* George W. George
* George F. Slavin
}}
| starring       = {{Plainlist|
* Randolph Scott
* Dorothy Malone
* Forrest Tucker
* Frank Faylen
* George Macready
}} Arthur Morton
| cinematography = Charles Lawton Jr.
| editing        = Richard Fantl
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States English
| budget         = 
| gross          = 
}} western film Gordon Douglas and starring Randolph Scott, Dorothy Malone, Forrest Tucker, Frank Faylen, and George Macready. Written by George W. George and George F. Slavin, the film is about a mysterious stranger who crosses paths with an outlaw bank robber and a greedy rancher.    The Nevadan was filmed in Lone Pine, California. 

==Plot==
United States Marshal Andrew Barclay (Randolph Scott) arranges the escape of outlaw Tom Tanner (Forrest Tucker) in order to locate the $250,000 in gold stolen by Tanner in stagecoach robbery. Tanner notices hes being followed by Barclay, whose appearance suggests he is a greenhorn. Tanner ambushes Barclay and forces him to trade clothes and accompany him to a bank, where Tanner retrieves an envelope containing a map from a safe deposit box showing the location of the stolen gold.

On the road, Tanner and Barclay are stopped by two brothers, Jeff and Bart, who pull their guns and demand the map. To Tanners surprise, Barclay disarms the brothers and takes their horses. Later he explains that he is a fugitive just like Tanner and proposes that they work together as a team. That night, while Barclay is asleep, Tanner rides on without him.

The next day, Barclay stops at a ranch owned by beautiful Karen Galt (Dorothy Malone) and trades his lame horse for a fresh one. He continues on to the nearby town of Twin Forks, which is run by Karens father, Edward Galt (George Macready). At the local saloon, Barclay sees Tanner who pretends not to know him. Galt watches their exchange and later questions Barclay about Tanners stolen gold, which was never discovered following the robbery. When Barclay denies knowing Tanner, Galt orders his henchmen to beat him up.

Later that night, Tanner kills an intruder in his room. In an effort to force Tanner to reveal the location of the gold, Galt sets him up, making it look like cold-blooded murder rather than self defense. After being taken to jail, Tanner escapes with the help of Barclay after agreeing to share the gold. The two men ride out to the old Galt ranch, now used as a pasture for sick horses. When Karen discovers them hiding there, Barclay takes her aside and reveals that he is in fact a U.S. Marshal. 

Meanwhile, Galt recognizes the escape horses used by Tanner and Barclay as belonging to his ranch. Later he questions his daughter about them, and she reveals Barclays secret, unaware that her father is after the gold himself. When Karen overhears Galt plotting with his henchmen, however, she realizes that Barclays life is in danger and rides to the hideout to warn him. One of Galts men follows her, however, and summons the others to the old Galt ranch. When they arrive, Karen meets them with gunfire, which gives Barclay and Tanner a head start on their escape. 

Galt catches up with his daughter and has her put in custody while he and the others track Barclay and Tanner to an old mine shaft where Tanner has hidden the stolen gold. During the ensuing gunfight, Galt and his men are killed. Barclay reveals that Tanner was allowed to escape so that the gold could be retrieved. When the mine shaft caves in, Barclay overcomes Tanner and takes his prisoner back to jail. Karen knows he will return to her because he has left his horse in her care.

==Cast==
* Randolph Scott as Andrew Barclay
* Dorothy Malone as Karen Galt
* Forrest Tucker as Tom Tanner
* Frank Faylen as Jeff
* George Macready as Edward Galt
* Charles Kemper as Sheriff Dyke Merrick
* Jeff Corey as Bart
* Tom Powers as Bill Martin
* Jock Mahoney as Sandy   

==Production==
===Filming locations===
* Alabama Hills, Lone Pine, California, USA 
* Columbia/Warner Bros. Ranch, 411 North Hollywood Way, Burbank, California, USA  Hidden Valley, Thousand Oaks, California, USA 
* Hoppy Cabin, Alabama Hills, Lone Pine, California, USA 
* Iverson Ranch, 1 Iverson Lane, Chatsworth, Los Angeles, California, USA   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 