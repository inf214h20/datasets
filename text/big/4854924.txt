Rockshow
 
 
{{Infobox film
| name = Rockshow
| image = Rockshow.gif
| image_size = 
| alt = 
| caption = Release poster
| producer = Bob Mercer Joe English Wings
| cinematography = Jack Priestley
| editing = Robin Clarke
| studio = MPL Communications
| distributor = Miramax Films   Eagle Rock Entertainment  
| released = 26 November 1980
| runtime = 125 minutes   102 minutes     134 minutes    
| country = United States
| language = English
}}
Rockshow is a 1980   DVD acknowledge only the Seattle concert, however. These concerts were part of the 1975–76 Wings Over the World tour, which also produced the triple live album Wings over America (1976) and the Wings Over the World television documentary (1979).

==Production==
The music for what was to become Rockshow and part of Wings Over the World was produced by Chris Thomas and engineered by Phil McDonald during 1979. However, the processing of the Rockshow overdubs was obviously completed by the airdate of Wings Over the World, as these same performances were featured in both productions. Not all of the songs made it to the film in their entirety ("Medicine Jar" and "Letting Go" have been edited), and the song introductions did not always match the performance which followed. For example, several of the introductions came from the Seattle performance while the actual song was taken from a Los Angeles show: "Magneto & Titanium Man", "Spirits of Ancient Egypt", "Lady Madonna", "Let Em In", and "Soily". The introduction of the horn section was a composite as well with portions coming from Seattle and the final Los Angeles show. 

==Release==
The film premiered 26 November 1980 at the Ziegfeld Theatre in New York; McCartney did not attend, as he was in the studio at the time. They did attend the London Premiere at the Dominion Theatre in Piccadilly Circus on 8 April 1981. The film was later edited down to 102 minutes for its home video release. Its first home video release was on Betamax in October 1981, with a VHS, laserdisc, and CED release in 1982.  This would be the last time a feature-length version of the film would be available to the general public until 31 years later, as McCartney had remained reluctant to make the entire film available to the general public on VHS or DVD. In 2007, a shortened cut with only seven songs was released as part of the The McCartney Years DVD set – one of which, a version of the Beatles "Lady Madonna", was originally left out of the Rockshow home video release.

===2013 re-release=== limited worldwide BAFTA on 15 May. This restored cut was released on DVD and Blu-ray Disc|Blu-ray in June 2013. 

==Song listing==
The following list is for the full-length, 125-minute version of Rockshow.

*  Songs edited out of the first home video versions of the film. 
+  Songs included in The McCartney Years DVD box set. 
 Venus and Mars" + Rock Show" +
# "Jet (song)|Jet" + 
# "Let Me Roll It"
# "Spirits of Ancient Egypt"
# "Medicine Jar"
# "Maybe Im Amazed" +
# "Call Me Back Again" *
# "Lady Madonna" * +
# "The Long and Winding Road" * Live and Let Die"
# "Picassos Last Words (Drink to Me)" * Richard Cory" *
# "Bluebird (Paul McCartney & Wings song)|Bluebird" +
# "Ive Just Seen a Face"
# "Blackbird (Beatles song)|Blackbird"
# "Yesterday (Beatles song)|Yesterday"
# "You Gave Me the Answer"
# "Magneto and Titanium Man" Go Now" My Love" *
# "Listen to What the Man Said" +
# "Let Em In" Time to Hide"
# "Silly Love Songs"
# "Beware My Love" Letting Go" Band on the Run"
# "Hi, Hi, Hi"
# "Soily"

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 