Kismet (1944 film)
 

{{Infobox film
| name           = Kismet
| image          = Kismet (1944).jpg
| caption        = 
| director       = William Dieterle
| producer       = Everett Riskin John Meehan
| starring       = Ronald Colman
| music          = 
| cinematography = Charles Rosher
| editing        = Ben Lewis
| distributor    = Metro-Goldwyn-Mayer
| released       = August 22,1944
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
}} James Craig Edward Arnold was the treacherous Grand Vizier. It was directed by William Dieterle, but was not a success at the box office.
 play of 1953 musical. John Francis Dillon and in a German-language version directed by William Dieterle.

==Plot==

The story takes place "when old Baghdad was new and shiny", in an Arabian Nights atmosphere. Colman plays Hafiz, a middle-aged trickster and magician who calls himself the King of Beggars. He occasionally puts on elegant attire and goes about the city pretending to be the "Prince of Hassir".  On one such occasion, he meets and amuses Lady Jamilla (Dietrich), the head wife of the Grand Vizier.
 Harry Davenport). On one such excursion, he meets the "Prince of Hassir" and is amused by his magic tricks.

Determined to make a beautiful life for his daughter Marsinah (Page), Hafiz has built high walls around his house, brought her up on fairy tales and promised her she will marry royalty. Marsinahs nurse, Karsha (Bates), growls "Bah!" every time Hafiz gets expansive about the future. She knows Marsinah has fallen in love with a "gardeners son", but keeps it from Hafiz. Marsinah tells her suitor about Hafiz promise of a "prince who will batter the walls down". The Caliph returns to his palace, planning to do exactly that and propose to Marsinah. 

The next day, Hafiz witnesses an attempt on the Caliphs life by an agent of the Grand Vizier (Arnold). The Vizier kills the would-be assassin before he can be caught and questioned, but the Caliph suspects him of being behind the plot nonetheless.

Although he knows the Caliph is unmarried, Hafiz decides the Vizier is good enough for his daughter, for he might be Caliph himself tomorrow. Donning fancy stolen clothes, Hafiz talks his way into the Viziers presence as the Prince of Hassir and offers him Marsinahs hand in marriage. The Vizier plies Hafiz with wine and food and shows off his dancing girls. A reluctant Jamilla only agrees to perform when she realizes the guest is her friend. In a private moment, Hafiz asks Jamilla to leave the Vizier and marry him, and she agrees; Marsinah will take her place as the head of the household. Returning home, Hafiz tells his daughter to prepare for her wedding day; Marsinah is outraged, then resigned. 

Then Hafiz is arrested for theft and brought before the amused Vizier. He is sentenced to have his hands cut off, but before the sentence can be carried out, a messenger ominously summons the Vizier to appear before the Caliph. To ensure his obedience, the Viziers palace is surrounded by the Caliphs soldiers. Hafiz bargains with the Vizier for his hands and his life (at least temporarily) by offering to kill the Caliph. 

Hafiz pretends to seek employment with the Caliph during a public audience. The plan goes awry when the Caliph, whose spies have revealed Hafiz to be Marsinahs father, laughingly tells Hafiz that they have met before. Hafizs hidden dagger misses its mark. In the confusion, the Vizier makes his escape, and Hafiz also manages to flee. Hafiz follows the Vizier to his palace. There he kills the Vizier to stop him from fleeing with Marsinah.

The Caliph orders his men to tear down the walls of Hafizs house, just as Hafiz had prophesied, and rides in on his white horse; Marsinah then realizes that the gardeners son and the Caliph are one. Though Hafiz is exiled from Baghdad for life, he sees his beloved daughter married to the Caliph and is sent to Hassir—as a prince, with Jamilla at his side.

==Cast==
* Ronald Colman - Hafiz
* Marlene Dietrich - Jamilla James Craig - Caliph Edward Arnold - The Grand Vizier
* Hugh Herbert - Feisal
* Joy Page - Marsinah (as Joy Ann Page)
* Florence Bates - Karsha Harry Davenport - Agha
* Hobart Cavanaugh - Moolah
* Robert Warwick - Alfife
* Barry Macollum - Amu  (uncredited)

==Awards== Best Cinematography, Best Music, Best Sound Best Art Direction (Cedric Gibbons, Daniel B. Cathcart, Edwin B. Willis, Richard Pefferle).      

==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 
 