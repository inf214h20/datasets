Anthimanthaarai
 
{{Infobox film
| name           = Anthithaamarai
| image          = anthimanthaaraifilm.jpg
| caption        = Audio CD Cover
| director       = P. Bharathiraja 
| producer       = Chandraleela Bharathiraja Thilaka Ganesh
| writer         = R. Selvaraj
| screenplay     = P. Bharathiraja
| story          = R. Selvaraj
| narrator       =  Vijayakumar Sanghavi
| music          = A. R. Rahman
| cinematography = C. Dhanapal
| editing        = K. Pazhanivel
| studio         = Megaa Movies
| distributor    = Megaa Movies
| released       = June 1, 1996
| runtime        = 131 minutes
| country        = India Tamil
| budget         =
| awards         =
}} National Award for the Best Feature Film in Tamil for the year 1997.

The music for the film is composed by A. R. Rahman. The film is relatively unknown and it had a brief run lasting for a week in major cinemas. This film is known for the background score by A.R. Rahman and had become a cult classic among his fans.

==Plot==
A wartime freedom fighter seeks refuge in a young womans home after being pursued. A platonic relationship forms between them which they will not forget for the rest of their lives.

==Production==
Anthimanthaarai is a low budget production targeted primarily at the arthouse movie audience. It features period contemporary backdrop settings. Originally, it was set to be released without a soundtrack. When the film got the tax break from the Govt of India, the director Bharathiraja had included music and songs for commercial purpose. A.R. Rahman accepted no payment for the music of this film.

==Cast==
*Jayasudha Vijayakumar

==Soundtrack==
{{Infobox album   
| Name = Anthimanthaarai
| Type = soundtrack 
| Artist = A. R. Rahman 
| Cover = 
| Released = 1996
| Recorded = Panchathan Record Inn Feature film soundtrack 
| Length = 
| Label =  
| Producer = A. R. Rahman
| Music Director = A .R .Rahman 
| Reviews = 
| Last album = Mr. Romeo (1996)
| This album = Anthimanthaarai (1996)
| Next album = Minsara Kanavu (1997)
}} Carnatic song written in Sanskrit and 3 instrumental themes. The lyrics written by Vairamuthu.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) 
|-
| 1 || Bharathiraja Voice I || Bharathiraja 
|-
| 2 || Sakiyaa || Unnikrishnan 
|-
| 3 || Theme Music I || Instrumental
|-
| 4 || Bharathi Rajas Voice II || Bharathiraja 
|-
| 5 || Oru Naal || Swarnalatha
|-
| 6 || Theme Music II || Instrumental
|-
| 7 || Sakiyaa II || Unnikrishnan
|-
| 8 || Pullai Thinkum || Sumangali
|-
| 9 || Music Bit I || Instrumental
|-
| 10 || Music Bit II || Instrumental
|}
The soundtrack became a cult favourite among fans of A.R. Rahman for its soul stirring music compositions.

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 


 