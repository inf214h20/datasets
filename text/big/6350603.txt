The Saint's Vacation
{{Infobox Film
| name            = The Saints Vacation
| caption         =  
| image	          = The Saints Vacation FilmPoster.jpeg
| director        = Leslie Fenton
| producer        = William Sistrom
| writer          = 
| based on        = Charteris novel, Getaway (The Saint)|Getaway
| screenplay      = Leslie Charteris Jeffrey Dell Hugh Sinclair Leueen MacGrath
| music           = Bretton Byrd
| cinematography  = Bernard Knowles
| editing         = Al Barnes Ralph Kemplen
| studio          = RKO Radio British Productions
| distributor     = RKO Radio Pictures
| released        =  
| runtime         = 61 min.
| language        = English
| budget          = 
}}
 Hugh Sinclair film series about the character created by Leslie Charteris. It was Sinclairs first appearance as Templar, having taken over the role from George Sanders. The film is the seventh of nine features produced by RKO Pictures featuring suave detective Simon Templar and it marks a major change in the series, shifting production to England.
 The Last Knight Templar. Unlike other films in the Saint series, Charteris himself co-wrote the screenplay. Also, unlike the previous Saint films, which were produced in Hollywood, The Saints Vacation was produced and filmed in the United Kingdom.

==Plot summary==
  Gordon McLeod) of Scotland Yard. 

==Cast== Hugh Sinclair as Simon Templar, aka The Saint  
* Sally Gray as Mary Langdon  
* Arthur Macrae as Monty Hayward  
* Cecil Parker as Rudolph Hauser  
* Leueen MacGrath as Valerie (as Leueen Macgrath)  
* John Warwick as Gregory  
* Manning Whiley as Marko  
* Felix Aylmer as Charles Leighton  
* Ivor Barnard as Emil   Gordon McLeod as Inspector Teal

==Notes== The Last Knight Templar. One notable omission from the character list is Templars literary girlfriend, Patricia Holm, who is replaced by another character, Mary Langdon, played by Sally Gray. 

This was the first of two appearances by Sinclair as The Saint; he would later return in the film adaptation of Meet - The Tiger!, a.k.a. The Saint Meets the Tiger.

==References==
 	

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 