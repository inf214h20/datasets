Fletch (film)
{{Infobox film
| name           = Fletch
| image          = Fletchmovieposter.jpg
| image_size     =
| caption        = Theatrical release poster Michael Ritchie
| producer       = Peter Douglas Alan Greisman
| writer         = Novel:  
| starring = {{Plainlist|
* Chevy Chase
* Joe Don Baker
* Dana Wheeler-Nicholson
* Richard Libertini
* Kareem Abdul-Jabbar
* Tim Matheson
}}
| narrator       = Chevy Chase
| music          = Harold Faltermeyer
| cinematography = Fred Schuler
| editing        = Richard A. Harris
| distributor    = Universal Pictures
| released       = May 31, 1985
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $8 million  
| gross          = $59,612,888
}}
 Michael Ritchie and written by Andrew Bergman, loosely based on the popular Gregory Mcdonald Fletch (novel)|novels. Tim Matheson, Dana Wheeler-Nicholson, Geena Davis and Joe Don Baker appear in supporting roles.

In the 1970s, Burt Reynolds and Mick Jagger were considered to portray Fletch but these suggestions were rejected by Mcdonald. The author agreed to the casting of Chevy Chase despite never seeing the comedian in anything. Chase reportedly enjoyed the role because it allowed him to play several different characters and work with props. In a 2004 interview with Entertainment Weekly, Chase confirmed this was his favorite role. 
 Fletch Won, has been in development for over two decades.

==Plot==
Los Angeles Times reporter Irwin "Fletch" Fletcher (Chase) is writing an article exposing the drug traffic on the beaches of Los Angeles. Posing as an addict during his investigation, he is approached by Boyd Aviation executive vice president Alan Stanwyk (Matheson) who mistakenly assumes Fletch is a Substance dependence|junkie. Stanwyk claims to have bone cancer, with only months left to live, and wishes to avoid the pain and suffering. Stanwyk offers $50,000 for Fletch to come to his mansion in a few days time, kill him, and then escape to Rio de Janeiro, staging the murder to appear as the result of a burglary. 

Fletch is suspicious but reluctantly agrees to the plan. Along with his colleague Larry (Davis), he begins investigating Stanwyk instead of completing his drug trafficking exposé, much to the dismay of his stern editor Frank Walker (Libertini). Fletch endures a rectal exam from Stanwyks doctor, Dr. Dolan (Walsh), in a failed attempt to talk Dolan into divulging medical information about Stanwyk. Later, disguised as a doctor, Fletch accesses Stanwyks file at the hospital and learns Stanwyk lied about having cancer. 

Next, Fletch visits Stanwyks wife Gail (Wheeler-Nicholson) at her tennis club and, pretending to be a tennis instructor and friend of her husbands, flirts with her during an impromptu tennis lesson. Looking into Stanwyks finances, Fletch finds out that Gail recently converted $3 million of her personal stock in Boyd Aviation into cash for her husband to buy a ranch in Provo, Utah. Fletch breaks into the realtors office and discovers the deed lists the sale price as only $3,000. 

Meanwhile, LAPD Chief Jerry Karlin (Baker) gets wind of Fletchs soon-to-be-published drug article and meets with him, saying the article will jeopardize his undercover operation on the beach. Chief Karlin threatens to kill Fletch unless he agrees to drop the investigation. Back at the tennis club, Fletch overhears a rude club member, Mr. Underhill, insulting a waiter and decides to use Underhills tab to treat Gail to an expensive lunch in her private cabana. Fletch informs Gail of the details to her husbands murder scheme and the fraudulent ranch deal. 

Fletch spies Stanwyk making a suspicious briefcase exchange with Chief Karlin, but is unable to determine the nature of their relationship. He returns home to find LAPD officers lying in wait at his apartment and flees and manages to avoid being caught after a harrowing car chase. Forced to go into hiding, Fletch returns to Provo and, under the disguise of an insurance investigator, interviews Stanwyks parents. Fletch discovers Stanwyk has been secretly married to another woman for eight years, having used Gail for her money. 

At the climax, Fletch arrives at Stanwyks mansion on the night of the plotted murder only to find Stanwyk armed and ready to kill him instead. Fletch reveals he is aware that Stanwyks real plan was to fake his own death by killing Fletch and burning his body beyond recognition. Stanwyk would then escape to Brazil with his other wife as well as Gails $3 million. Stanwyk was also using his private jet to smuggle cocaine from South America to supply Chief Karlin who in turn profited by blackmailing ex-convicts like Fat Sam (Wendt) to peddle it on the beaches. Chief Karlin unexpectedly shows up and, upon learning of Stanwyks intentions to flee with $800,000 of the Chiefs drug money, kills Stanwyk. Chief Karlin and Fletch then indulge in a brief brawl over the gun until Chief Karlin is knocked unconscious by Gail. 

Some time later, Chief Karlin is indicted due to Fletchs investigation. Fletch begins dating Gail and, in the final scene, takes her on a vacation to Rio... using Mr. Underhills tab.

==Cast==
* Chevy Chase as Irwin M. "Fletch" Fletcher
* Joe Don Baker as Chief Jerry Karlin
* Dana Wheeler-Nicholson as Gail Stanwyk
* Richard Libertini as Frank Walker
* Geena Davis as Larry
* Larry "Flash" Jenkins as Gummy
* Tim Matheson as Alan Stanwyk
* M. Emmet Walsh as Dr. Joseph Dolan
* George Wendt as Fat Sam
* Kenneth Mars as Stanton Boyd
* George Wyner as Marvin Gillet
* Kareem Abdul-Jabbar as Himself
* Chick Hearn as Himself James Avery as Detective #2
* Reid Cruickshanks as Sergeant Bruce French as Dr. Holmes
* Burton Gilliam as Bud
* David W. Harper as Teenager (as David Harper)
* Alison La Placa as Pan Am Clerk (as Alison Laplaca)
* Joe Praml as Watchman
* William Sanderson as Jim Swarthout
* Penny Santon as Velma Stanwyk
* Robert Sorrells as Marvin Stanwyk
* Beau Starr as Willy
* William Traylor as Ted Underhill

==Production==
Gregory Mcdonalds novel was very successful and soon Hollywood came calling. His Fletch books were optioned around the mid to late 1970s but the author had retained the right of approving the actor cast to play Fletch. He rejected the likes of Burt Reynolds and Mick Jagger. When the studio mentioned Chevy Chase as Fletch, Mcdonald (even though he had never really seen Chase in anything) agreed. {{cite news
 | last = Laker
 | first = Jim
 | title = Laker Jims Fletch Won Interview with Gregory Mcdonald
 | publisher = Fletch Won Interviews
 | url = http://www.fletchwon.net/mcdonald.html
 | accessdate = 2006-06-20
}}  Years before, Chases manager recommended Mcdonalds books to him but he was not interested at the time.  When an old friend and producer Alan Greisman and screenwriter Andrew Bergman got involved, Chase agreed to do it. {{cite news
 | last = Bygrave
 | first = Mike
 | title = Chevy Chase
 | work = Movie Magazine
 | page = 7
 | date = Summer 1985
 | url =
 | accessdate =
}}  Mcdonald sent Chase a telegram saying, "I am delighted to abdicate the role of Fletch to you." {{cite news
 | last = Collis
 | first = Clark
 | title = The Curse of Fletch
 | work = Entertainment Weekly
 | date = February 5, 2010
 | url = http://www.ew.com/ew/article/0,,20342679,00.html
 | accessdate = 2010-03-26
}}  Bergman was hired to adapt Mcdonalds book into screenplay form. Bergman remembers that he wrote the screenplay "very fast&nbsp;– I did the first draft in four weeks . . . Then there was a certain amount of improv, and something that we used to call dial-a-joke." {{cite news
 | last = Foreman
 | first = Jonathan
 | title = Fletch Fanatics - A Modest 1985 Film Builds A Cult Following
 | work = New York Post
 | date = May 12, 1999
 | url = http://www.nypost.com/p/entertainment/fletch_fanatics_modest_film_builds_KlNdqLyHjfbZHbUc0ZWIeJ
 | accessdate =
}}  Mcdonald read the script and was angry by how far it strayed from his book. He wrote to the studio and listed his many objections to the screenplay. Director Michael Ritchie invited Mcdonald to the set of the film and took him out to dinner where, according to Mcdonald, "Point by point, he showed me where I was wrong. I was beautifully chewed out." {{cite news
 | last = Thomas
 | first = Bob
 | title = Father of Fletch happy with film
 | work = The Globe and Mail
 | publisher =
 | date = August 1, 1984
 | url =
 | accessdate =
}} 

According to actor Tim Matheson, Fletch was the first film Chase did after cleaning up his drug problem. {{cite news
 | last = Murray
 | first = Noel
 | title = Tim Matheson
 | work = The A.V. Club
 | publisher = The Onion
 | date = February 17, 2009
 | url = http://www.avclub.com/articles/tim-matheson,23873/
 | accessdate = 2009-04-17
}}  However, the studio hired director Michael Ritchie to keep Chase in check. During principal photography, Ritchie would do one take sticking close to the script and then another take allowing Chase to ad-lib.  Chase enjoyed the role because it allowed him to play a wide variety of different characters. He said in an interview, "I love props, like wigs and buck-teeth and glasses. At one point I wear an Afro and play basketball with Kareem Abdul-Jabbar. There were some scenes where I didnt recognize myself." {{cite news
 | last = Goodman
 | first = Joan
 | title = A whole cast of characters
 | work = The Times
 | publisher =
 | date = September 26, 1985
 | url =
 | accessdate =
}}  The comedian enjoyed working with director Ritchie because he gave him the freedom to improvise: "It all began when   Tim Matheson asked me what my name was. Right away, with a straight face: Ted Nugent." 

==Soundtrack==

{{Infobox Album  
| Name        = Fletch (Music From the Motion Picture Soundtrack)
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = Fletchsoundtrack.jpg
| Released    = 1985
| Recorded    =
| Genre       = Soundtrack
| Length      = 36:13 MCA
| Producer    =
| Reviews     = 
| Last album  =
| This album  =
| Next album  =
}}

{{Album ratings
| rev1      = Allmusic
| rev1Score = (not rated) 
| rev2      = 
| rev2Score = 
}}

# Stephanie Mills - "Bit by Bit (Theme from Fletch)" 3:38
# Dan Hartman - "Get Outta Town|Fletch, Get Outta Town" 4:11
# John Farnham - "Running for Love" 2:54
# Dan Hartman - "Name of the Game" 6:02
# Harold Faltermeyer - "Fletch Theme" 3:48
# The Fixx - "A Letter to Both Sides" 3:20
# Kim Wilde - "Is It Over" 3:52
# Harold Faltermeyer - "Diggin In" 2:44
# Harold Faltermeyer - "Exotic Skates" 3:00
# Harold Faltermeyer - "Running for Love"   2:44

==Reception==

Fletch was released on May 31, 1985, in 1,225 theaters, grossing $7&nbsp;million on its opening weekend. It went on to make $50.6&nbsp;million in North America and $9&nbsp;million in the rest of the world for a worldwide total of $59.6&nbsp;million. {{cite web
 | title = Fletch
 | publisher = Box Office Mojo
 | url = http://www.boxofficemojo.com/movies/?id=fletch.htm
 | accessdate = 2009-02-10
}}  The film performed well on home video, earning $24.4&nbsp;million in rentals. {{cite news
 | title = Fletch
 | publisher = The Numbers
 | url = http://www.the-numbers.com/movies/1985/0FLE1.php
 | accessdate = 2009-04-17
}} 

Fletch received generally positive reviews and has a 75&nbsp;percent rating on Rotten Tomatoes.  Film critic Roger Ebert gave the film two-and-a-half stars out of four and wrote, "The problem is, Chases performance tends to reduce all the scenes to the same level, at least as far as he is concerned. He projects such an inflexible mask of cool detachment, of ironic running commentary, that were prevented from identifying with him . . . Fletch needed an actor more interested in playing the character than in playing himself." {{cite news
 | last = Ebert
 | first = Roger
 | authorlink = Roger Ebert
 | title = Fletch
 | work = Chicago Sun-Times
 | date = May 31, 1985
 | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19850531/REVIEWS/505310301/1023
 | accessdate = 2009-02-10
}}  Vincent Canby in his review for The New York Times praised Chases performance, writing, "He manages simultaneously to act the material with a good deal of nonchalance and to float above it, as if he wanted us to know that he knows that the whole enterprise is somewhat less than transcendental." {{cite news
 | last = Canby
 | first = Vincent
 | authorlink = Vincent Canby
 | title = Film: Fletch, Starring Chevy Chase, Reporter
 | work = The New York Times
 | publisher =
 | date = May 31, 1985
 | url = http://www.nytimes.com/1985/05/31/movies/film-fletch-starring-chevy-chase-reporter.html
 | accessdate = 2007-04-20
}}  Time (magazine)|Time magazines Richard Schickel wrote, "In Fletch, the quick, smartly paced, gags somehow read as signs of vulnerability. Incidentally, they add greatly to the movies suspense. Every minute you expect the heros loose lip to be turned into a fat one." {{cite news
 | last = Schickel
 | first = Richard
 | authorlink = Richard Schickel
 | title = Gliberated in Dreamland Fletch Time
 | publisher =
 | date = June 3, 1985
 | url = http://www.time.com/time/magazine/article/0,9171,957763,00.html
 | accessdate = 2009-02-10
}}  In his review for the Chicago Reader, Dave Kehr wrote, "Chase and Ritchie make a strong, natural combination: the union of their two flip, sarcastic personalities produces a fairly definitive example of the comic style of the 80s, grounded in detachment, underreaction, and cool contempt for rhetorically overblown authority figures." {{cite news
 | last = Kehr
 | first = Dave
 | title = Fletch
 | work = Chicago Reader
 | publisher =
 | url = http://onfilm.chicagoreader.com/movies/capsules/3586_FLETCH
 | accessdate = 2009-02-10
}} 

==Legacy==

Fletch became a cult film.  In an interview for the  , too, that captured a certain wise-ass thing."  In particular, the film appeals to college students who have asked Chase to talk about it at film classes.  The actor has said that the appeal of the character is "the cheekiness of the guy. . . everybody at that age would like to be as quick-witted as Fletch, and as uncaring about what others think."  Chase has said that this film is his favorite to date because "it allowed me to be myself. Fletch was the first one with me really winging it. Even though there was a script, the director allowed me to just go, and in many ways, I was directing the comedy." {{cite news
 | last = Sayre
 | first = Carolyn
 | title = 10 Questions: Chevy Chase Time
 | publisher =
 | date = April 11, 2007
 | url = http://www.time.com/time/arts/article/0,8599,1609309,00.html
 | accessdate = 2007-04-20
}}  Perhaps the most meaningful praise comes from Mcdonald himself: "I watched it recently, and I think Chevy and   depicted the Big Bad Wolf (from Little Red Riding Hood) as a sarcastic investigative reporter in a direct parody of Fletch, right up to the Lakers shirt, disguises, and a version of Fletchs theme playing during his scenes. {{cite news
 | last = Boucher
 | first = Geoff
 | title = The 25 best L.A. films of the last 25 years
 | work = Los Angeles Times
 | date = August 31, 2008
 | url = http://www.latimes.com/entertainment/news/movies/la-ca-25films31-2008aug31,0,70218.htmlstory
 | accessdate = 2008-08-31
}} 

===Home media===
Fletch was originally released on DVD in 1998, but that release quickly went out of print. Universal Home Video re-released a special edition of Fletch - the "Jane Doe" Edition on May 1, 2007. The film is presented in 1.85:1 anamorphic widescreen, along with an English Dolby Digital 5.1 Surround track and includes the retrospective featurettes, "Just Charge It to the Underhills: Making and Remembering Fletch," "From John Coctoastan To Harry S. Truman: The Disguises" and "Favorite Fletch Moments." IGN felt that this version was a decent replacement for anyone who still owned the film on VHS but for "anyone seeking more than that will be sadly disappointed by the ill-executed extras and slap-dash sound upgrade." {{cite news
 | last = Schorn
 | first = Peter
 | title = Fletch (The "Jane Doe" Edition)
 | publisher = IGN
 | date = May 1, 2007
 | url = http://dvd.ign.com/articles/784/784932p1.html
 | accessdate = 2009-04-17
}}  

Additionally, the film was also the next-to-last to be released by Universal on the HD DVD format, March 11, 2008, and later released on Blu-ray disc on June 2, 2009. 

==Sequel and prequel==
The film was followed by a 1989 sequel, Fletch Lives.

A follow-up to Fletch Lives had been discussed in the 90s at Universal Studios. During his association with Universal after the production of Mallrats (this was because Gramercy Pictures, which released Mallrats, was co-owned by Universal), Kevin Smith expressed interest in doing a third "Fletch" film as a sequel starring Chevy Chase but it never came to fruition. In June 2000, it was announced that Kevin Smith was set to write and direct a Fletch film at Miramax Films, after the rights to the books, which Universal Studios had owned, reverted. {{cite news
 | url = http://www.variety.com/article/VR1117783207.html
 | date = 2000-06-29
 | title = Mmax, Smith fetch Fletch for franchise
 | author = Jonathan Bing
 |author2=Claude Brodesser
 | work = Variety
 | accessdate = 2007-02-17
}}  At the time, Miramax co-head Harvey Weinstein expressed the hope that a new Fletch series would be "Miramax Films first-ever franchise." 
 Jason Lee and Ben Affleck as possible choices to play Fletch. {{cite web
 | url = http://www.ew.com/ew/article/0,,475789,00.html
 | date = 2003-08-13
 | title = Fletch Lives
 | author = Liane Bonin
 | work = Entertainment Weekly
 | accessdate = 2007-02-17
}} 

In August 2003, it was reported that the film was set to start shooting in January, with Smith still at the helm. Though Smith insisted on casting Lee in the lead role, Miramax head Harvey Weinstein refused to take a chance on Lee, citing the general inability of his films to gross more than $30&nbsp;million at the box office. The role of Fletch remained uncast, with Smith considering a list of actors including Affleck, Brad Pitt, Will Smith, and Jimmy Fallon.  Though Smith considered compromising and casting Zach Braff in the role, he eventually left the project in October 2005. 
 Bill Lawrence, in what would have been his directorial debut. He had enthused, "Not only can I recite the original Fletch movie line for line, I actually read all the Greg Mcdonald books as a kid. Consider me obsessed&nbsp;— Im going to try as hard as I can not to screw this up." {{cite web
 | url = http://www.comingsoon.net/news/movienews.php?id=15669
 | date = 2006-07-26
 | title = Lawrence to Write & Direct Fletch Movies!
 | publisher = ComingSoon.net
 | accessdate = 2007-02-17
}}  Lawrence was signed to direct both Fletch Won and a sequel.  Scrubs star Zach Braff was rumored to be in talks for the lead role,  and in January 2007, Braff posted on his web site that "Bill Lawrence is writing and directing Fletch in the spring and he wants me to play young Fletch, but no firm plans are in place yet. He is still writing the script." {{cite web
 | url = http://www.zachbraff.com/comments.php?id=101
 | date = 2007-01-17
 | title = The Most Awesomest Blog Ever Written
 | publisher = ZachBraff.com
 | accessdate = 2007-02-17 Open Hearts. {{cite web
 | url = http://www.tvguide.com/news/Exclusive-Zach-Braff-8379.aspx
 | title = Exclusive: Zach Braff Bails on Fletch! Plus: Scrubs to ABC?
 | author = Michael Ausiello
 | work = TV Guide
 | date = 2007-04-23
 | accessdate = 2009-10-03}}  In June 2007, it was announced that Lawrence was off the project and had been replaced by Steve Pink. 

In 2011, rights to the project were purchased by Warner Brothers, who requested screenplays from several writers that turned out to be unsuitable. In 2013, David List, who represents the McDonald estate, stepped in with his own draft which proved attractive enough to engage Jason Sudeikis in the title role. The studio signed off on the screenplay, described as more of a "gritty action comedy with heart", and has began looking for a director. {{cite web
| url = http://www.hollywoodreporter.com/news/jason-sudeikis-star-fletch-won-687430
| date = 2014-03-10
| title = Jason Sudeikis in Talks to Star as Fletch in Fletch Won
| publisher = The Hollywood Reporter
| accessdate = 2014-03-15
| author = Borys Kit
| work = The Hollywood Reporter}} 

==References==
 

==External links==
 

*  
*  
*  
*  
*   at The Numbers

 

 
 
 
 
 
 
 
 
 
 
 
 
 