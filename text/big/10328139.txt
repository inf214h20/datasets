The Brothers Bloom
{{Infobox film
| name           = The Brothers Bloom
| image          = The Brothers Bloom poster.jpg
| caption        = Promotional poster
| director       = Rian Johnson
| producer       = Wendy Japhet Ram Bergman James D. Stern
| writer         = Rian Johnson
| narrator       = Ricky Jay
| starring       = Rachel Weisz Adrien Brody Mark Ruffalo Rinko Kikuchi Maximilian Schell Robbie Coltrane Nathan Johnson
| cinematography = Steve Yedlin
| editing        = Endgame Entertainment 
| distributor    = Summit Entertainment
| released       =  
| runtime        = 113 minutes 
| country        = United States
| language       = English
| budget         = $20,000,000 
| gross          = $5,530,764   
}} caper comedy film written and directed by Rian Johnson. The film stars Mark Ruffalo, Adrien Brody, Rachel Weisz, Ricky Jay, Rinko Kikuchi, and Robbie Coltrane. Originally released in only four theaters on May 15, 2009, the film moved into wide release two weeks later on May 29. 

==Plot==
The Brothers Bloom, orphaned at a young age, begin performing confidence tricks as young children; Stephen dreams up elaborate scenarios and his younger brother, Bloom, creates trust with the marks. Stephen creates his first con as a way of encouraging his brother to talk to girls.

Twenty-five years later, the brothers are the worlds most successful con men. They even have a regular accomplice: Bang Bang, a Japanese explosives expert who rarely speaks. Bloom, however, is dissatisfied with being nothing but an actor in Stephens schemes. He quits and moves to Montenegro. Three months later, Stephen finds Bloom and convinces him to execute one final con. The brothers will masquerade as antiques dealers and target Penelope Stamp, a rich, socially-isolated heiress who lives alone in a New Jersey mansion. 

Bloom and Penelope meet when Bloom purposely runs into Penelopes sports car with his bike. Penelope reveals that she has been alone for most of her life and has picked up an array of strange hobbies such as juggling and kung fu. Bloom senses Penelopes craving for adventure and hints that he is sailing to Europe tomorrow. The next morning, Penelope arrives at the harbor to sail with the brothers to Greece.

On the boat, Melville, a Belgian hired by Stephen, begins the con, telling Penelope that the brothers Bloom are in fact antiques smugglers and he wants their help with a smuggling job in Prague. Penelope is thrilled with the idea of becoming a smuggler and convinces the brothers to accept the job, unaware that this is part of the con. Meanwhile, Bloom and Penelope are becoming attracted to one another, but Stephen warns Bloom that the con will fail if he actually falls in love with Penelope.

At the hotel bar in Prague, Bloom is visited by the brothers former mentor and current enemy, Diamond Dog. He warns Bloom that Stephen will not be around forever, and tells Bloom he should join him. Stephen arrives and stabs Diamond Dog in the hand with a broken bottle, telling him to stay away.

In Prague, Melville cons Penelope out of a million dollars and flees, according to plan. Penelope still wants to go ahead as an antiques smuggler and steal the rare book that Melville told her about. The brothers tell Bang Bang to set off a small explosive in Prague Castle that will trigger the fire alarm, allowing Penelope to sneak in and steal the book. Instead, Penelope accidentally blows up the entire tower, creating panic in Prague. Despite this, Penelope enters the museum and steals the book. She is caught, but somehow convinces the chief of police to let her go.

The team goes to Mexico to complete the con. Bloom, who has fallen in love with Penelope, reveals to her that they are con men and the whole adventure has been a con. Stephen has anticipated his brothers change of heart and written it into his plan. The brothers fight and a gun accidentally discharges, wounding Stephen. Penelope checks out the wound, realizes that it is fake blood, and leaves with a broken heart. Bloom punches Stephen and leaves for Montenegro. 

Three months later, Penelope finds Bloom, wanting to be with him and to become a con artist. Unable to deny his love for her but not wanting her to be like him, Bloom meets with Stephen to set up one final con, where they will fake their own deaths. The team goes to St. Petersburg, where they must sell the rare book to Diamond Dog.  They are ambushed by Diamond Dogs gang while heading to the exchange. Stephen is kidnapped and held for $1.75 million. Bloom suspects this is just another one of Stephens tricks; Penelope, just in case, wires the money from her bank account to the mobsters. Bang Bang takes this opportunity to quit working for the Brothers Bloom; as soon as she leaves, her car explodes, leaving Penelope and Bloom uncertain whether she was caught in the blast or faked her death.

Bloom goes into an abandoned theater to make the exchange, and finds Stephen tied up and beaten. Bloom demands that Stephen tell him if this is real or if it is a con. A hit man tosses Bloom a phone, and Diamond Dog confirms that it is real. The hit man attacks them, and Stephen takes a bullet for Bloom and collapses on the floor. Bloom again asks whether this was real, or just the "perfect con." Stephen gets up and assures Bloom that he is fine. Stephen tells Bloom to leave St. Petersburg with Penelope, and that they will meet again.

Bloom and Penelope drive away. After several hours, Bloom discovers that Stephens bloodstain on his shirt has changed in color from red to brown, indicating that it is not fake blood. Realizing that Stephen has surely died, Bloom breaks down on the side of the road while Penelope tries to comfort him. As they are leaving, Bloom recalls what Stephen had said earlier, "The perfect con is one where everyone involved gets just the thing they wanted"—and that perhaps his brother really pulled off the perfect con.

==Cast==
*   and "was impressed by its originality and subtlety."  While working on the film Brody considered a "bromance" to have formed between himself and Ruffalo which led to more genuine rapport between them. 
* Mark Ruffalo as Stephen: When Johnson first sat down with Ruffalo it was for the part of Bloom but his actual personality was so similar to Stephen, Johnson chose to switch. 
* Rachel Weisz as Penelope Stamp: Weisz was being offered mostly drama roles but was interested in doing a comedy.  She was drawn to the script because it is well written but still unusual.  After Weisz decided on it she told her agent, "this is the one, this is the one."  While working on the film she developed a rapport with her costar Adrien Brody. 
*  . 
* Robbie Coltrane as Maximillen "The Curator" Melvile
* Maximilian Schell as Diamond Dog. This is the actors final film role.
* Ricky Jay as Narrator
* Zachary Gordon as Young Bloom
* Max Records as Young Stephen
* Andy Nyman as Charleston
* Nora Zehetner as Rose
* Noah Segan as The Duke
* Joseph Gordon-Levitt as Bar Patron (uncredited cameo)
* Lukas Haas as Bar Patron (uncredited cameo)
* Stefan Kapicic as German bar owner

==Production==
The original script was titled Penelope after Weiszs character. Shooting began in Ulcinj, Montenegro on March 19, 2007.

===Script and development===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"Paper Moon is probably the closest to a direct influence. I love The Sting and House of Games, but Paper Moon was really the first thing I watched that took more of a fairy-tale approach and was more relationship based. Other than that, God, take your pick."
&mdash;Rian Johnson, director/writer   
|} The Man Paper Moon. The Conformist and 8½ for visual style. 

===Filming locations===
Penelopes castle is the Peleș Castle in Sinaia, Romania.  Other locations include Belgrade (Serbia), the Constanța Casino, the Port of Constanța (Romania) and various locations in Greece and Montenegro. The exterior scenes involving the theft of the book were shot in Prague, both in and around Prague Castle and on the Charles Bridge. 

===Penelopes skills=== card tricks, among others.   Brody helped Weisz learn to skateboard; she said, "Brody is a good skateboarder, so we were in the parking lot outside the place we were filming."  Brody also helped her to learn to rapping|rap; when she first tried "he was so ashamed."  The card trick was the most difficult for Weisz and took her a month of practicing every day to learn.  The shot itself took 11 or so takes, but the one continuous shot in the film is not enhanced in any way. 

==Soundtrack==
{{Infobox album
| Name        = The Brothers Bloom: Original Motion Picture Soundtrack
| Type        = Soundtrack Nathan Johnson
| Cover       = 
| Released    =  
| Length      = 48:12
| Label       = Cut Narrative Records
| Producer    =
| Reviews     =
}}
 Nathan Johnson, composed the score for the film as he did on Johnsons directorial debut, Brick (film)|Brick.

Three songs in the film are not available on the soundtrack:"Tonight Ill Be Staying Here With You" by Bob Dylan, "Miles From Nowhere" by Cat Stevens, and "Sleeping" by The Band, which was performed karaoke-style by Rinko Kikuchi. Rian Johnson listened to The Band while writing the script, and their music was a major influence on the score.

In a digitally-released soundtrack companion booklet, Nathan Johnson said that since the film was about storytelling, it made sense to use lyric-based songs as an inspiration. He also credited Italian composer Nino Rota as an influence.

{{Tracklist collapsed = yes  headline  = Track listing title1    = Brothers In a One Hat Town (Overture) length1   = 5:03 title2    = Cackle Bladder length2   = 2:09 title3    = Charlestons Denoument length3   = 1:14 title4    = This Is Camels length4   = 1:43 title5    = Montenegro length5   = 1:00 title6    = Meeting Penelope length6   = 1:18 title7    = An Enlightened Euphoria length7   = 3:06 title8    = Double Dutch Queens length8   = 1:08 title9    = The Curator length9   = 1:19 title10   = The Grecian Docks length10  = 2:12 title11   = Penelopes Theme length11  = 2:42 title12   = The Diamond Dog length12  = 3:04 title13   = The Castle Heist length13  = 2:53 title14   = Mexico length14  = 2:13 title15   = Off-script length15  = 2:46 title16   = An Empty Stage length16  = 2:53 title17   = Cackle Bladder (Revisited) length17  = 1:17 title18   = The Perfect Con length18  = 6:43 title19   = The Fabulist length19  = 3:29
}}

==Release==
The Brothers Bloom had its world premiere at the Toronto International Film Festival on September 9, 2008.  The film was then screened as the opening night feature at the Boston Independent Film Festival on April 22, 2009. At the Newport Beach Film Fest Johnson won a festival honors award in the category of Outstanding Achievement in Directing.  The first seven minutes of the film were posted to the online streaming video site, Hulu, on April 23, 2009. 

===Critical reception===
Review aggregate website Rotten Tomatoes reports that 66% of critics have given the film a positive review, based on 124 reviews with an average rating of 6.1/10. The sites consensus stated that "Despite strong performances The Brothers Bloom ultimately does not fulfill its lofty ambitions".  On Metacritic, the film was assigned a weighted average score of 55 out of 100 based on 26 reviews from mainstream critics. 

Claudia Puig writing for USA Today stated that the film "has it all" with an "offbeat perspective" and "magical realism style that works exquisitely". She gave The Brothers Bloom a 3.5 out of 4 and wrote that it "is an often rapturous trot around the globe" but noted that the film "loses some steam in the final half hour."  Robert Wilonsky thought that Johnson had "infused The Brothers Bloom with so much heart and beauty that one can and should easily overlook its discomfiting moments."  Wilonsky suggested a second viewing of the film is "even more profound and touching". 

Roger Ebert commented how the films "acting is a delight" but it was "too smug and pleased with itself". He continued by complaining that the film had "too many encores and curtain calls".   Robert Abeles review of The Brothers Bloom for the Los Angeles Times criticized Brody for over-moping and considered Ruffalo as "out of sorts" but thought Weiszs performance as "the best thing in the movie". Abele also thought Johnson used too many filmmaking quirks and when Johnson was not distracting the audience he had his actors doing it. 

===Box office===
The Brothers Bloom was originally to be released in the fall of 2009 but Summit pushed it forward to May.   The film opened in four theaters in the U.S. in its first week, earning $90,400.  During the memorial day weekend from May 23, 2009 to May 25, 2009, the first weekend after its initial limited release, The Brothers Bloom grossed $495,527, from 52 theaters, ranking it #15.  During its wide release weekend starting May 29, 2009, in 148 theaters the film grossed $627,971, ranking it #11.  The Brothers Bloom finished its theatrical run after 12 weeks reaching at most 209 theaters during its sixth week.  The film has grossed $3,531,756 domestically and $1,997,708 abroad for a total of $5,529,464.  This placed it at number 167 for all films released in 2009.  The film was released in the UK on 4 June 2010. 

===Home media===
The DVD and Blu-ray Disc became available to rent on September 29, 2009 and to own on January 12, 2010. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 