Bangalore Mail
{{Infobox film
| name           = Bangalore Mail
| image          = 
| caption        = 
| director       = L. S. Narayana
| producer       = Yaragudipati Varada Rao|Y. V. Rao
| story          = 
| screenplay     = 
| narrator       = Rajkumar Narasimharaju Narasimharaju Jayanthi Jayanthi
| music          = Chellapilla Satyam
| cinematography = R. Madhu
| editing        = Bal G. Yadav
| studio         = Gowri Art Films
| distributor    = Eshwari Pictures
| released       =  
| runtime        = 151 minutes
| country        = India
| language       = Kannada
}}
 Kannada film Narasimharaju and Jayanthi in lead roles.

==Cast== Rajkumar
* Narasimharaju as Babu Rajendra Prasad Jayanthi
* Bhagawan
* Kottarkar
* B. V. Radha
* Jyothilakshmi
* K. S. Ashwath as Mrutyunjaya Rao in a guest appearance

==Soundtrack==
{{Infobox album
| Name        = Bangalore Mail
| Type        = Soundtrack
| Artist      = Chellapilla Satyam
| Cover       = Kannada film Bangalore Mail album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1968
| Recorded    =  Feature film soundtrack
| Length      = 22:03
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}
 Satyam with lyrics for the soundtracks penned by Chi. Udaya Shankar. The album consists of six soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 22:03
| lyrics_credits = yes
| title1 = Kannugale Kamalagalu
| lyrics1 = Chi. Udaya Shankar
| extra1 = P. B. Sreenivas, S. Janaki
| length1 = 3:29
| title2 = Ore Nota
| lyrics2 = Chi. Udaya Shankar
| extra2 = L. R. Eswari
| length2 = 3:27
| title3 = Pony Tailu Nodu Nodu
| lyrics3 = Chi. Udaya Shankar
| extra3 = S. P. Balasubrahmanyam, L. R. Eswari
| length3 = 4:33
| title4 = Bangara Balayuna
| lyrics4 = Chi. Udaya Shankar
| extra4 = S. Janaki, B. K. Sumitra
| length4 = 2:55
| title5 = Are Jaa Jaa
| lyrics5 = Chi. Udaya Shankar
| extra5 = S. Janaki
| length5 = 4:11
| title6 = Kuhusiri Cha Cha
| lyrics6 = Chi. Udaya Shankar
| extra6 = L. R. Eswari, chorus
| length6 = 3:28
}}

== References ==
 

==External links==
*  

 
 
 
 


 