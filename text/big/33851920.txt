Gangs of New York (1938 film)
{{Infobox film
| name           = Gangs of New York
| image_size     = 
| image	=	Gangs of New York FilmPoster.jpeg
| caption        = 
| director       = James Cruze
| producer       = Armand Schaefer (producer)
| writer         = Herbert Asbury (The Gangs of New York (book)|book) Samuel Fuller (screenplay) Samuel Fuller (story) Charles F. Royal (writer) Wellyn Totman (writer) Jack Townley (writer) Nathanael West  (uncredited)
| narrator       = 
| starring       = See below
| music          = Alberto Colombo  (uncredited) Ernest Miller William Morgan
| studio         = 
| distributor    = Republic Pictures
| released       = 1938
| runtime        = 67 minutes 53 minutes (edited American TV version)
| country        = USA
| language       = English
}}

Gangs of New York is a 1938 American film directed by James Cruze and written by Samuel Fuller.

Crime lord Rocky Thorpe is impersonated by police officer John Franklin in order to infiltrate his organization and bring an end to it once and for all.

==Cast==

*Charles Bickford as Rocky Thorpe / John Franklin
*Ann Dvorak as Connie Benson Alan Baxter as Dancer
*Wynne Gibson as Orchid
*Harold Huber as Panatella
*Willard Robertson as Inspector Sullivan
*Maxie Rosenbloom as Tombstone
*Charles Trowbridge as District Attorney Lucas John Wray as Maddock
*Jonathan Hale as Warden
*Fred Kohler as Kruger Howard Phillips as Al Benson
*Robert Gleckler as Nolan
*Elliott Sullivan as Hopkins
*Maurice Cass as Phillips

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 