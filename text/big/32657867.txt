Dad Rudd, MP
 
 
{{Infobox film
| name           = Dad Rudd, MP
| image          = Dad_Rudd_MP.jpg
| image_size     = 
| caption        = Poster for theatrical release
| director       = Ken G. Hall
| producer       = Ken G. Hall Frank Harvey
| based on       = characters created by Steele Rudd
| narrator       =  Grant Taylor  Fred MacDonald
| music          =  George Heath
| editing        = William Shepherd
| studio         = Cinesound Productions distributor = British Empire Films
| released       = June 1940
| runtime        = 83 mins
| country        = Australia
| language       = English
| budget         = £18,000   
| gross          = £28,000 
}} Frank Harvey.

==Synopsis==
Dad Rudd wants the size of a local dam increased for the benefit of local farmers but faces opposition from a wealthy grazier, Henry Webster. When the local Member of Parliament dies, Webster runs for his seat, and Rudd decides to oppose him.

Webster and his team use dirty tricks to defeat Rudd, so he calls in his old friend from the city, Entwistle to help. Matters are complicated by the fact that Rudds daughter Ann falls in love with Websters son Jim.

On polling day, a fierce storm causes the dam to collapse. A major flood traps workers on the wrong side of the dam and the Rudds and Jim Webster team up to save the day. Dad Rudd is elected to parliament, where he gives a rousing speech.

==Cast==
 
*Bert Bailey as Dad Rudd
*Fred MacDonald as Dave Rudd Grant Taylor as Jim Webster Frank Harvey as Henry Webster
*Connie Martyn as Mum
*Yvonne East as Ann Rudd
*Ossie Wenban as Joe
*Valerie Scanlan as Sally
*Alec Kellaway as Entwistle
*Jean Robertson as Mrs Webster
*Barbara Weeks as Sybil Vane
*Ronald Whelan as Lewis
*Letty Craydon as Mrs McGrury
*Marshall Crosby as Ryan
*Joe Valli as MacTavish
*Field Fisher as Jenkins
*Billy Stewart as Bloggs
*Natalie Raine as Susie
*Lorna Westbrook as Minnie
*Leo Gordon as Fordham
*Chips Rafferty as fireman
*Raymond Longford as electoral officer
*John Sherman as interjector
 

==Production==

===Script===
The last six films made by Cinesound Productions were all comedies as producer Ken G. Hall sought to ensure guaranteed box office successes. He elected to make another Dad and Dave film instead of two other long-planned projects, an adaptation of Robbery Under Arms  and a story about the Overland Telegraph.  Hall said in 1939 that:
 Though we were entertaining the idea of other types of stories, the amazing enthusiasm for Dad and Dave Come to Town makes another Bailey picture the wisest commercial choice. We feel that, by placing Dad in politics, we will inject any amount of comedy material which is typical of Bailey at his best.  

William Freshman was originally reported as having worked on the script but is not credited in the final film. 

The movie was more serious than others in the series, being basically a drama with comic interludes. Bert Bailey commented during filming that:
 In one of the old Selection books, Dad did stand for Parliament. But that was for comedy purposes. In Dad Rudd, M.P., when Dad does come down and speak in Parliament, there is not one tinge of comedy. He is an earnest old chap, speakong in a plain, ordinary, common-sense way on water conservation. He is saying what he believes is the right thing to be done for the farmer, and for the country. For water is a national asset. In this scene, Dad does allude to the war. He says that the spirit which animated the pioneers who crossed the plains and fought the land is the same spirit behind the adventurous boys who go abroad to fight for Australia."  

Ken Hall himself edited out this speech when the film screened on ABC TV in 1970. "In the light of the world as we know it in the seventies, it all sounded so follow, so phony, so naive", he wrote. But the speech remains in most copies of the film available today. 

===Casting===
The romantic leads were played by Yvonne East and Grant Taylor, both graduates of the Cinesound Talent School making their first film.  Chips Rafferty makes an early screen appearance as a fireman in the Keystone Kops-style opening sequence.

The cast had more continuity than usual for a Cinesound Rudd film, with Alec Kellaway, Connie Martyn, Ossie Wenban, Valerie Scanlan and Marshall Crosby all reprising their roles from Dad and Dave Come to Town (1938). 

American actor Barbara Weeks, who was visiting Australia at the time of shooting with her husband, played a small role at the behest of Ken G. Hall. 

===Shooting===
During pre production, Cinesound was visited by Adolph Zukor, founder of Paramount Pictures, then touring Australia. He had seen Dad and Dave Come to Town on the boat trip from the US, and been so impressed with the films quality he wanted to visit the studio. 

Shooting took place in February and March 1940, in the Cinesound Studio and on location at Woronora Dam and Camden, New South Wales|Camden.  Cinesound hired space on the lot of the closed-down Pagewood studios for building a scale reproduction of the dam for the climax. These were supervised by J Alan Kenyon, who did the special effects for most of Cinesounds movies. The fake dam was 125 feet long and held 12,000 gallons of water. 

According to Hall:
 It was the smoothest, best-made of the Bert Bailey films. In the process of the gradual evolution of the people and the storylines we had set down for these productions, the rawness had gone off the characters. There was much less burlesque of the types. The story was more modern and believable.  

The movie was partly financed with an guaranteed overdraft of £15,000 from the New South Wales government. 

==Release==
Dad Rudd MP was a hit at the box office, earning £28,000 and achieving a successful release in Britain. However it was generally felt this result was below expectations.  Hall thought this was due in part to the fact that it was released "when Britain was standing alone under the blitz and all of Europe was aflame. It was a grim time for the whole world and a disastrous time for the entertainment industry. The theatres in Australia reached their lowest ebb since the depths of the Depression. Some suburban houses went dark and many city houses might just as well have been closed for all the good they were doing." 

The drain on material caused by World War II saw Cinesound abandon feature production in June 1940 for the duration of the war.  They never made another feature film.

==References==
 

==External links==
*  in the Internet Movie Database
*  at Australian Screen Online
*  at Oz Movies
*  at National Archives of Australia
*  at Australian Screen Online
* 

 
 

 
 
 