The Bohemian Girl (1936 film)
 
{{Infobox film
| name = The Bohemian Girl
| image = L&H Bohemian Girl 1936.jpg
| caption = 1946 theatrical re-release poster
| director = James W. Horne Charley Rogers
| producer = Stan Laurel Hal Roach Frank Butler (screenplay) Jacqueline Wells Jimmy Finlayson
| music = Michael William Balfe (original operetta) Robert Shayon Nathaniel Shilkret
| cinematography = Francis Corby Art Lloyd
| editing = Bert Jordan Louis McManus
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 70 52"
| language = English
| country = United States
}}
The Bohemian Girl is a 1936 feature film version of the opera The Bohemian Girl by Michael William Balfe. It was produced at the Hal Roach Studios, and stars Laurel and Hardy and Thelma Todd in her last role before her death. This was also the only appearance of Darla Hood in a full-length feature produced by Hal Roach. Hood was best known as "Darla" from the Our Gang (Little Rascals) comedy shorts which also star George "Spanky" McFarland and Carl "Alfalfa" Switzer. Both McFarland and Switzer do not appear in the film.

==Plot== Gypsies in 18th- century Austria. When Oliver is out pickpocketing, fortune-telling or attending his zither lessons, his wife (Mae Busch), has an affair with Devilshoof (Antonio Moreno). A cruel nobleman, Count Arnheim, persecutes the Gypsies, who are forced to flee, but Devilshoof, in revenge for having been lashed by the count, kidnaps his daughter, Arline (Darla Hood), and Mrs. Hardy fools Hardy into thinking she is their daughter. She soon elopes with Devilshoof, and leaves Hardy holding the baby.

Twelve years later, the Gypsies return to Arnheims estate. When grown up Arline ( : Hardy emerges stretched to a height of eight feet, while stand has been crushed to only a few feet tall.

==Thelma Todds role==
Thelma Todd had starred in four Laurel and Hardy films, including their first talkie, Unaccustomed As We Are.
 present from her soon afterwards. The jury brought out a verdict of accidental death because there was little or no evidence for suicide.

Three films starring Todd were released after her death. In The Bohemian Girl, Todd had played the Gypsy Queen, a very substantial role. All of her scenes were re-shot and her character was renamed as the Gypsy Queens Daughter, and Zeffie Tilbury playing the Queen, and with a vampish Mae Busch character replacing her in the narrative. One scene of Todds was kept in as a tribute to her: a musical number where she sings "Heart of a Gypsy".

==Quotes==
Meta-reference: James Finlayson, well- known for his comical squinting, gets poked in the eye at one point and cries: "Oh! My good eye!"

==Casting and production details==
Metro-Goldwyn-Mayer wanted to cast a talented newcomer as Arline. Hal Roach cast Darla Hood, who had just begun appearing in Roachs Our Gang comedies, as young Arline and Julie Bishop as adult Arline.

Rosina Lawrence dubs Jacqueline Wellss singing.

Paulette Goddard has a small uncredited role as a Gypsy.

Stan Laurels pet myna, Yogi, appears in the film.

The Count was played by W.P. Carleton, who had played the role on stage over a number of decades and who was a distant cousin of the British actor Sir Guy Standing.

==Banned== 
The film was banned in Malaysia due to its depictions of Roma themes.   

== Cast ==
* Stan Laurel as Stan
* Oliver Hardy as Ollie Julie Bishop as  Arline as an Adult 
* Darla Hood as Arline as a child
* Mae Busch as Mrs. Hardy
* Antonio Moreno as  Devilshoof, Mrs. Hardys lover
* William P. Carleton as Count of Arnheim  James Finlayson as Finn, Captain of the Guard
* Zeffie Tilbury as old Gypsy Queen 
* Mitchell Lewis as Salinas, Gypsy Queens advisor
* Harry Bowen as Laurel and Hardys first victim (the drunkard)
* Sam Lufkin as Laurel and Hardys second victim (the innkeeper)
* Eddie Borden as Laurel and Hardys third victim (the nobleman)
* James C. Morton as the officer who arrests the nobleman
* Harry Bernard as bell ringer
* Thelma Todd as singer of "Heart of a Gypsy"
* Felix Knight as singer of "When Other Lips"

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 