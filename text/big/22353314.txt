Hoodlum & Son
{{Infobox film
| name           = Hoodlum & Son
| image          = Hoodlum-movie.jpg
| caption        = 
| director       = Ashley Way
| writer         = Ashley Way Ted King Ron Perlman Robert Vaughn Myles Jeffrey
| music          = 
| producer       = 
| cinematography = 
| editing        = 
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
Hoodlum & Son is a 2003 comedy-crime film. 

==Plot== Ted King), a reluctant gangster indebted to mob boss Benny “The Bomb” Palladino (Robert Vaughn). Benny gives Charlie a last chance to clear his ‘debt’ by collecting money from a rival, but when Archie follows his father into a rival gangster’s speakeasy, a series of bizarre events take place culminating in them fleeing without the money.

They head south to a dustbowl town where Charlie plans to repay his debt by stealing money from a bootlegging business run by Ugly Jim McCrae (Ron Perlman). However, Charlie realises things will not be that easy and is forced into taking a teaching job as cover which brings father and son closer.

When a beautiful widowed mother, Ellen Heaven (Mia Sara) takes offence at the new teacher, she does all she can to get him thrown out of town. Torn between their responsibilities and their attraction to each other, passions are awoken and Charlie and Ellen fall in love. However, the two revengeful mob bosses have not forgotten Charlie and arrive in town to settle scores.

==Cast==
* Mia Sara - Ellen Heaven Ted King - Charlie Ellroy
* Ron Perlman - Ugly Jim McCrae
* Robert Vaughn - Benny The Bomb Palladino
* Myles Jeffrey - Archie Ellroy
* Emily McArthur - Alabama Lubitsch Michael Richard - Sheriff Duggan
* Russel Savadier - Four Eyes Morton Ian Roberts - Earl
* Karin van der Laag - Big Juicy Lucy
* Anthony Bishop - Gillis Johnson Anthony Fridjhon - Police Chief 
* Charlotte Savage - Virginia Heaven
* Giles Hudson - Undertaker

==References==
 

==External links==
*  
*  

 
 