The Iron Claw (1916 serial)
 
{{Infobox film
| name           = The Iron Claw
| caption        = Film poster
| image	         = The Iron Claw 3.jpg
| director       = George B. Seitz Edward José
| producer       = Edward José
| writer         = George B. Seitz
| starring       = Pearl White Creighton Hale
| cinematography = 
| editing        = 
| distributor    = Pathé Exchange
| released       =  
| runtime        = 20 episodes
| country        = United States
| language       = Silent
| budget         = 
}}
 silent adventure 20 episode film serial starring Pearl White, directed by George B. Seitz and Edward José, and released by Pathé Exchange.    A print of the seventh episode exists in the UCLA Film and Television Archive.   

==Cast==
* Pearl White as Margery Golden
* Creighton Hale as Davey
* Sheldon Lewis as Legar, The Iron Claw
* Harry L. Fraser as The Laughing Mask
* J. E. Dunn (actor)|J. E. Dunn as Enoch Golden
* Carey Lee as Mrs. Golden Clare Miller as Margery, as a Child
* Henry G. Sell as Wrench
* Edward José as Manley
* E. Cooper Willis Allan Walker
* Bert Gudgeon
* George B. Seitz

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 


 