Misleading Lady
{{Infobox Film
| name           = Misleading Lady
| image          = Misleading Lady poster.jpg
| caption        = Theatrical release poster Stuart Walker
| producer       =
| writer         = Paul Dickey (play) Caroline Francke Charles W. Goddard (play) Adelaide Heilbron
| starring       = Claudette Colbert   Edmund Lowe   Stuart Erwin
| music          = Johnny Green
| cinematography = George J. Folsey
| editing        =
| distributor    = Paramount Pictures
| released       = 15 April 1932
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} Stuart Walker, Metro silent film original which starred Bert Lytell and Lucy Cotton,  also based on the play.   
 Astoria Studios in Astoria, Queens, New York City.

==Plot==
Story of girl, who to convince a producer of her ability to act, undertakes to make a man propose to her within three days. From the play by Charles W. Goddard and Paul Dickey.

==Cast==
*Claudette Colbert as Helen Steele
*Edmund Lowe as Jack Craigen
*Stuart Erwin as Boney Robert Strange as Sydney Parker
*George Meeker as Tracy
*Selena Royle as Alice Connell
*Curtis Cooksey as Bob Connell
*William Gargan as Fitzpatrick
*Nina Walker as Jane Neatherby
*Edgar Nelson as Steve Fred Stewart as Babs
*Harry Ellerbe as Spider
*Will Geer as McMahon
*Donald MacBride as Bill

==Filming locations==
*Kaufman Astoria Studios - 3412 36th Street, Astoria, Queens, New York City, New York, USA


==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 