A Happy Divorce
{{Infobox film
| name           = A Happy Divorce
| image          = Un divorce heureux.jpg
| image size     =
| caption        =
| director       = Henning Carlsen
| producer       = Mag Bodard Philippe Dussart
| writer         = Benny Andersen Henning Carlsen
| narrator       =
| starring       = Jean Rochefort
| music          =
| cinematography = Henning Kristiansen
| editing        = Claire Giniewski Christian Hartkopp
| distributor    =
| released       = 25 April 1975
| runtime        = 102 minutes
| country        = France Denmark
| language       = French Danish
| budget         =
}}

A Happy Divorce ( ,  ) is a 1975 Danish-French drama film directed by Henning Carlsen. It was entered into the 1975 Cannes Film Festival.   

==Cast==
* Jean Rochefort - Jean-Baptiste Morin
* André Dussollier - François
* Daniel Ceccaldi - Antoine
* Bulle Ogier - Marguerite
* Bernadette Lafont - Jacqueline, linfirmière
* Anne-Lise Gabold - Sylvie
* Etienne Bierry - Pierre

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 