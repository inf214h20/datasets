New York Confidential (film)
{{Infobox film
| name           = New York Confidential
| image	         = New York Confidential FilmPoster.jpeg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Russell Rouse
| producer       = Clarence Greene Edward Small
| screenplay     = Clarence Greene Russell Rouse
| based on       =  
| narrator       = Ralph Clanton
| starring       = Broderick Crawford Richard Conte
| music          = Joseph Mullendore
| cinematography = Eddie Fitzgerald
| editing        = Grant Whytock
| studio         = Edward Small Productions
| distributor    = Warner Bros.
| released       =   
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.3 million (US) 
}}
New York Confidential is a 1955 crime film directed by   and Lee Mortimer. 

==Plot==
Charlie Lupo is a gangster who runs the New York branch of a crime syndicate. He is a widower with a grown daughter, Kathy and a new lover, Iris.

Hit man Nick Magellan of the Chicago mob impresses Lupo, who hires Magellan to be his bodyguard. They form a friendship and Kathy is attracted to Nick, but he resists her advances.

When a political lobbyist interferes with the syndicates plans and needs to be eliminated, Lupo arranges for three men to handle it. But they leave too many clues and need to be eliminated, a task Lupo turns over to Nick.

Nick quickly dispatches two of the targets, but a third flees and rats out Lupo to the cops. While hiding out, Lupos unhappy daughter Kathy gets drunk and is killed in a car crash.

Lupos heart is no longer in his work and he decides to cooperate with the authorities, so the syndicate orders Nick to get rid of his friend. Nick obeys orders, killing Lupo, but then is eliminated himself.

==Cast==
* Broderick Crawford as Charlie Lupo
* Richard Conte as Nick Magellan
* Marilyn Maxwell as Iris Palmer
* Anne Bancroft as Katherine Lupo
* J. Carrol Naish as Ben Dagajanian
* Onslow Stevens as Johnny Achilles
* Barry Kelley as Robert Frawley
* Mike Mazurki as Arnie Wendler
* Celia Lovsky as Mama Lupo
* Herbert Heyes as James Marshall
* Steven Geray as Morris Franklin
* William Bill Phillips as Whitey
* Henry Kulky as Gino
*Nestor Paiva as Martinelli
* Joseph Vitale as Batista
* Carl Milletaire as Sumak William Forrest as Paul Williamson
* Ian Keith as Waluska Charles Evans as Judge Kincaid
* Mickey Simpson as Leon Hartmann
* Tom Powers as District Attorney Rossi
* Lee Trent as Ferrari
* Leonard Bremen as Larry
* John Doucette as Shorty
* Frank Ferguson as Dr. Ludlow
* Hope Landin as Mrs. Wesley (as Hope Landon)
* Fortunio Bonanova as Senor

==Production==
Edward Small bought the rights to the book in 1953. He assigned it over to the team of Clarence Greene and Russell Rouse, who had a six picture deal with Small. Greene and Rouse wanted George Raft and Paul Muni to star. 

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine praised the cast in their review of the film, "Among crime exposes New York Confidential stacks up as one of the better-made entries, thanks to a well-fashioned story and good performances by a cast of familiar names ... Conte does a topnotch job of making a coldblooded killer seem real and Crawford is good as the chairman of the crime board, as is Marilyn Maxwell as his girl friend. Anne Bancroft, showing continuing progress and talent, scores with a standout performance of Crawford’s unhappy daughter." 
 The Enforcer ... New York Confidential was never exciting, tense or eye-opening. Its narrative was a cliché driven mob story that was only mildly diverting and even though the performances were energetically delivered, it still tasted like a stale salami sandwich." 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*  

 
 

 
 
 
 
 
 
 
 
 
 