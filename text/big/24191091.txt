Son of the Navy
{{Infobox film
| name           = Son of the Navy
| image          = Cover of the movie Son of the Navy.jpg
| image_size     =
| caption        = VHS cover
| director       = William Nigh
| producer       = Scott R. Dunlap (producer) Grant Withers (associate producer)
| writer         = True Boardman (story) Grover Jones (story) Marion Orth (screenplay) George Waggner (screenplay)
| narrator       = James Dunn Martin Spellman
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = Russell F. Schoengarth
| distributor    =
| studio         = Monogram Pictures
| released       = 30 March 1940
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Son of the Navy is a 1940 American comedy-drama film directed by William Nigh.

The film is also known as The Young Recruit in the USA.

== Plot summary ==
Racing to his battleship the USS Florida, Chief Gunners Mate Mike Malone attempts to hitchhike to the Naval Base San Diego and runs into Tommy and his Cairn terrier Terry who are runaways from an orphanage. 
 Chief Petty Officer and gives her the impression that Mike is his father, especially after Steve saw Tommy see Mike off.  Steve spreads the word that Mike is a runaway father abandoning his child getting him in trouble with his ships captain and fellow chief petty officers.

== Cast ==
*Jean Parker as Steve Moore James Dunn as Chief Gunners Mate Mike Malone
*Martin Spellman as Tommy Terry as Terry
*Selmer Jackson as Capt. Parker
*William Royle as Chief Moore
*Sarah Padden as Mrs. Baker, Landlady Craig Reynolds as Brad Wheeler Dave OBrien as Chief Machinists Mate
*Gene Morgan as Burns Charles King as Duke Johnson

== Soundtrack ==
Anchors Aweigh 
by Charles A. Zimmermann


== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
Military humor in film

 