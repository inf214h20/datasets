Soosi
{{Infobox film 
| name           = Soosi
| image          =
| caption        =
| director       = Kunchacko
| producer       = M Kunchacko
| writer         = Thoppil Bhasi
| screenplay     = Thoppil Bhasi Sharada Adoor Hari
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, Hari in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Rajan Sharada as Susie
*Adoor Bhasi as Dallal Lazar Hari as George
*Manavalan Joseph as Supervisor
*P. J. Antony as Ouseppachan
*Sreelatha Namboothiri as Jolly
*Alummoodan as Thaaraavukaaran
*G Gopinath as Income Tax Officer
*Kaduvakulam Antony as Clerk
*Kottarakkara Sreedharan Nair as Chacko Sir
*N. Govindankutty as Driver Kutty
*Raghavan Nair as Joy
*S. P. Pillai as Thomachan
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Kaikalil Rakthamundo || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Jil Jil Jil || B Vasantha, Chorus || Vayalar Ramavarma || 
|-
| 3 || Maanathe Mandakini || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Naazhikakku Naalppathu || P Susheela || Vayalar Ramavarma || 
|-
| 5 || Nithyakaamuki || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 6 || Rakthachandanam || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|-
| 7 || Sindoora Meghame || P Susheela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 