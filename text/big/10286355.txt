Primary Colors (film)
 
{{Infobox film
| name = Primary Colors
| image = Primaryposter.jpg
| caption = Promotional film poster
| director = Mike Nichols   
| producer = Mike Nichols Jonathan Krane Neil Machlis
| screenplay = Elaine May
| based on =  
| starring = John Travolta Emma Thompson Billy Bob Thornton Kathy Bates Adrian Lester Maura Tierney Larry Hagman
| music = Ry Cooder
| cinematography = Michael Ballhaus Arthur Schmidt
| studio = Mutual Film Company
| distributor = Universal Studios
| released =  
| runtime = 143 minutes
| country = United States
| language = English
| budget = $65 million   
| gross = $52,090,187   
}} Bill Clintons Joe Klein, who had been covering Clintons campaign for Newsweek.        It was directed by Mike Nichols and starred John Travolta, Emma Thompson, Billy Bob Thornton, Kathy Bates, Maura Tierney, Larry Hagman, and Adrian Lester. 

Bates was nominated for an Academy Award for Best Supporting Actress for her performance, and the film itself was nominated for an Academy Award for Best Adapted Screenplay.

==Plot== presidential primary.  
  
After Stanton completes an impressive debate performance against his Democratic rivals, Henry’s ex-girlfriend shows up to question Stanton about his arrest during the 1968 Democratic Convention in Chicago. The team becomes worried that Stanton’s past indiscretions may be used against him by the press and his political opponents. They hire Jack and Susans old friend, tough but unbalanced Libby Holden (Kathy Bates), to respond to any attacks designed to undermine Stantons candidacy, as well as anticipate problems such as allegations about Stantons notorious womanising. One of Stantons mistresses, Cashmere McLeod (Gia Carides), produces secret taped conversations between them to prove they had an affair. Henry discovers that the tapes have been doctored, so Libby tracks down the man responsible for the tapes and forces him at gunpoint to confess his guilt in a signed letter to the American public.

The campaign is then rocked by a fresh allegation when Stantons old friend, "Big Willie" McCullison (Tommy Hollis) approaches Henry to tell him that his 17-year old daughter Loretta (who worked for the Stantons as a babysitter) is pregnant and that Stanton is the father. Henry and Howard tell Willie he must allow his daughter to undergo an amniocentesis to determine Stantons paternity. Although they convince Willie to remain silent on the issue, Henry is nonetheless sickened and disillusioned with the experience.

Realising the campaign is falling behind in the polls, Stantons team adopt a new strategy. Stanton begins going on the offensive by attacking his nearest rival, Senator Lawrence Harris (Kevin Cooney) for being insufficiently pro-Israel and for threatening to cut Medicare (United States)|Medicare. Harris confronts Stanton during a radio debate but suffers a heart attack after the encounter. He subsequently withdraws from the race and is replaced by his friend, former Florida Governor Fred Picker (Larry Hagman). Pickers wholesome, straight-talking image proves an immediate threat to the campaign.  
 
Henry and Libby decide to seek out information about Picker’s past. They discover from his brother-in-law, Eddie Reyes (Tony Shalhoub), that Picker had a cocaine addiction as Governor, which led to the disintegration of his first marriage. They also meet Lorenzo Delgado (John Vargas), with whom Picker had a homosexual affair. Not expecting the information to ever be used, Libby and Henry share their findings with Jack and Susan, but are dismayed when they both decide to leak the information to the press. Libby says that if Jack does so, she will reveal that he tampered with the results of the paternity test, proving that he slept with Willies daughter. Libby commits suicide after she realizes she spent her life idealizing Jack and Susan only to learn how flawed they truly are. Racked with guilt over Libbys death, Stanton takes the incriminating information to Picker, and apologizes for seeking it out. Picker admits to his past indiscretions, and agrees to withdraw from the race and to endorse Stanton. Henry intends to quit the campaign, admitting he has become deeply disillusioned with the whole political process. Stanton begs Henry to reconsider, persuading him that the two of them can make history.

Months later, President Stanton is dancing at the Inaugural Ball with First Lady, Susan. He shakes the hands of all his campaign staff, the last of whom is Henry.

==Cast==
 
* John Travolta as Governor Jack Stanton
* Emma Thompson as Susan Stanton
* Billy Bob Thornton as Richard Jemmons
* Kathy Bates as Libby Holden
* Larry Hagman as Governor Fred Picker
* Adrian Lester as Henry Burton
* Stacy Edwards as Jennifer Rogers
* Maura Tierney as Daisy Green
* Diane Ladd as Mamma Stanton
* Paul Guilfoyle as Howard Ferguson
* Kevin Cooney as Senator Lawrence Harris
* Rebecca Walker as March Cunningham
* Caroline Aaron as Lucille Kaufman
* Tommy Hollis as William "Fat Willie" McCullison
* Rob Reiner as Izzy Rosenblatt
* Ben L. Jones as Arlen Sporken
* J. C. Quinn as Uncle Charlie
* Allison Janney as Miss Walsh
* Robert Klein as Norman Asher
* Mykelti Williamson as Dewayne Smith
* Geraldo Rivera as himself
* Charlie Rose as himself
* Larry King as himself
* Bill Maher as himself
* Sophia Choi as herself
* Chelcie Ross as Charlie Martin
* Tony Shalhoub as Eddie Reyes
* John Vargas as Lorenzo Delgado
* Gia Carides as Cashmere McLeod
 
 

==Production==
 
Following the publication of the book in 1996, director Mike Nichols paid more than $1 million for the screen rights. 

At the Cannes Festival, Thompson said she did not base her performance on Hillary Clinton, while Travolta said he based his on several presidents, but mostly on Bill Clinton. 

Nichols was criticized for cutting an interracial love scene from the final version of the film. He responded that he had removed the scene because of unfavorable reactions from a preview audience. 

The film also generated controversy for its depiction of a Clinton-like character as it was also released close to the Lewinsky scandal.             

==Reception==
The movie received a positive reception from critics. Variety (magazine)|Varietys reviewer called it a "film à clef" and said that the American public was likely to accept it as a factual account because it so closely mirrored real life characters and events.  The Los Angeles Times gave high marks to the movie, noting Travoltas close mirroring of Bill Clinton, but describing Thompsons character as actually not based on Hillary Clinton.  Entertainment Weekly called Travolta "Clintonian".  The Cincinnati Enquirer gave accolades to the character portrayals of Bill and Hillary Clinton.  Syndicated reviewer Roger Ebert said the movie was "insightful and very wise about the realities of political life"  and the Cincinnati Enquirer said the movie was a "nuanced dissection of how real American politics work". 

In a negative review, Jeff Vice of The Deseret News wrote that the last half of the movie dragged, Travoltas performance seemed more like an impersonation than actual acting, the movie lacked subtlety or depth, and it was loaded with cheap and obvious jokes. Nevertheless, Vice wrote that "solid support is provided by Maura Tierney, Larry Hagman, and Stacy Edwards". 

Primary Colors currently holds an 80% rating on Rotten Tomatoes based on 75 reviews.

===Box office===
 
The film earned a disappointing box office gross,       only taking $39,001,187 domestically and $13,089,000 in foreign markets, for a worldwide total gross of $52,090,187 against a budget of $65 million. 

==Soundtrack==
The soundtrack album, featuring music by and produced by Ry Cooder, was released in March 1998.  

==References==
{{reflist|colwidth=30em|refs=

   

   

   

   

   

 {{cite news|title= Primary by a landslide|publisher= The Cincinnati Enquirer|author=Margaret A. McGurk|date=1998
The Cincinnati Enquirer|url= http://cincinnati.com/freetime/movies/mcgurk/primarycolors.html|accessdate=January 24, 2011}} 

   

}}

==Further reading==
*  

==External links==
*  
*  
*  
*  
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 