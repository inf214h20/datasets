Zorro's Fighting Legion
{{Infobox film
| name           = Zorros Fighting Legion
| image          = File:Zorros Fighting Legion poster.jpg John English
| producer       = Hiram S. Brown Jr
| writer         = Ronald Davidson Franklin Adreon Morgan Cox Sol Shor Barney A. Sarecky Johnston McCulley (Original Zorro Novel)
| starring       = Reed Hadley Sheila Darcy William Corson Leander De Codova Edmund Cobb John Merton C. Montague Shaw
| cinematography = Reggie Lanning
| distributor    = Republic Pictures
| released       =   {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 42–43
 | chapter =
 }} 
| runtime         = 12 chapters (211 minutes) (serial)  6 26½-minute episodes (TV) 
| country         = 
| language        = English
| budget          = $137,826 (negative cost: $144,419) 
| gross           =
}} film serial John English. The plot revolves around his alter-ego Don Diegos fight against the evil Don Del Oro.

A trademark of this serial is the sudden demise of at least one native informant in each episode.  The direction was identical for each informants death, creating a source of unintentional humor:  each informant, upon uttering the phrase, "Don Del Oro is...", is shot by a golden arrow and dies before being able to name the villains alter ego. The serial is also unusual in featuring a real historical personage, Mexican President Benito Juárez, as a minor character.

==Plot==
The mysterious Don Del Oro ("Lord of Gold"), an idol of the Yaqui Indians, has emerged and attacks the gold trade of the Republic of Mexico, planning to take over the land and become Emperor. A man named Francisco is put in charge of a fighting legion to combat the Yaqui tribe and protect the gold, but he is attacked by men working for Don Del Oro.  Zorro comes to his rescue, but it is too late for him. Franciscos partner recognizes Zorro as the hidalgo Don Diego Vega. Francisco asks Diego, as Zorro, to take over the fighting legion and defeat Don Del Oro.

==Cast==
*Reed Hadley as Don Diego Vega and his alter ego Zorro.  
Though there were numerous Zorro serials, Hadley was the only actor to play the original Zorro in any of them. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 76
 | chapter = 5. A Cheer for the Champions (The Heroes and Heroines)
 }} 
*Sheila Darcy as Volita
*William Corson as Ramon
*Leander De Cordova as Governor Felipe
*Edmund Cobb as Manuel Gonzalez
*John Merton as Commandante Manuel
*C. Montague Shaw as Chief Justice Pablo/ Don Del Oro (when unmasked)
*Budd Buster as Juan
*Carleton Young as Benito Juárez
*Bud Geary as Don Del Oro (body and voice)

==Production==
Zorros Fighting Legion was budgeted at $137,826, although the final negative cost was $144,419 (a $6,593, or 4.8%, overspend).  It was filmed between 15 September and 14 October 1939 under the working title Return of Zorro.   The serials production number was 898. 

This film was shot in Simi Hills and Chatsworth, Los Angeles.

===Stunts===
*Dale Van Sickel doubling Reed Hadley
*Yakima Canutt James Fawcett
*Ted Mapes
*Ken Terrell

==Release==

===Theatrical===
Zorros Fighting Legion s official release date is 16 December 1939, although this is actually the date the sixth chapter was made available to film exchanges.  The serial was re-released on 24 March 1958, making it the last serial released by Republic, which re-released serials for several years following the release of their final serial King of the Carnival in 1955. 

===Television===
In the early 1950s, Zorros Fighting Legion was one of fourteen Republic serials edited into a television series.  It was broadcast in six 26½-minute episodes. 

==Chapter titles==
#The Golden God (27 min 38s)
#The Flaming "Z" (16 min 41s)
#Descending Doom (16 min 41s)
#The Bridge of Peril (16 min 40s)
#The Decoy (16 min 39s)
#Zorro to the Rescue (16 min 40s)
#The Fugitive (16 min 41s)
#Flowing Death (16 min 41s)
#The Golden Arrow (16 min 39s)&nbsp;– Clipshow|Re-Cap Chapter
#Mystery Wagon (16 min 40s)
#Face to Face (16 min 40s)
#Unmasked (16 min 41s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 226
 | chapter = Filmography
 }} 

==Differences from the Zorro canon==
The story takes a few liberties with  ).  However, this story is presented as a further adventure of Zorro, a sequel to the traditional "Mark of Zorro" origin story originally starring Douglas Fairbanks and Noah Beery, Sr., which would be remade the year after Zorros Fighting Legion with Tyrone Power and Basil Rathbone: Don Diego is said to be visiting from Los Angeles, and the serial intentionally did not remake the Zorro story; instead, it shows Zorro visiting Mexico because his help is needed there. The people of Mexico immediately recognize Zorro when he first appears, strongly suggesting that Zorro is a well-known hero.

The date given for the movie is 1824, which in and of itself establishes that it takes place well after Zorros California adventures: Zorro opposed a corrupt Spanish Colonial government in his canon tales, and California ceased being a Spanish Colony in 1821.

==References==
 

==External links==
*  
*  
*  
*  

===Download or view online===
*  
  
*  
*  
*  
*  
*  
*  
 
*  
*  
*  
*  
*  
*  
 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 