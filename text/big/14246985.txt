Three Missing Links
{{Infobox Film |
  | name           = Three Missing Links |
  | image          = ThreeLinksTITLE.jpg|
  | caption        = |
  | director       = Jules White|
  | writer         = Searle Kramer| Jane Hamilton Naba |
  | cinematography = Henry Freulich| Charles Nelson|
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 18 04" |
  | country        = United States
  | language       = English
}}

Three Missing Links is the 33rd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The boys are janitors at Hollywood studio Super Terrific Productions. They are cleaning the office of B. O. Botswaddle (James C. Morton), who is reviewing a script a new picture "Jilted In The Jungle" where the leading man will be a gorilla. After nearly destroying Botswaddles office, Curly does his classic imitation of a "chicken with its head cut off" (Curlys trademark spinning around on the floor move) followed by other motions that cause Botswaddle to declare Curly to be the "dead image of the missing link" and sends the boys off to Africa to begin shooting.
 Jane Hamilton). Problems arise when Curly (dressed as a gorilla) gets entangled with a real gorilla, who scares the film crew off the set. The gorilla then turns his attention to Curly, who, in trying to placate the beast, eats some of the love candy and falls in love with it. The gorilla, repulsed, dashes off with a lovestruck Curly in pursuit.

==Production notes==
This is the fourth of sixteen Stooge shorts to use the word "three" in the title. Whoever was in the "real" gorilla suit was clearly having fun with his role. When Moe (thinking he is Curly) berates him, he shrinks meekly away, and even shudders his shoulders as if on the verge of tears. Later, watching Curly remove the head of his gorilla suit, the "real" gorilla experiments with trying to take off his own head too.   

==Quotes==
*Moe: "Were terrific!"
*Larry: "Were colossal!"
*Curly: "Were even mediocre!"

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 