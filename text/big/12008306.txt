Shanti (film)
{{Infobox film
| name           = Shanti
| image          = Shanti film.jpg
| alt            = 
| caption        = Bhavana, playing the role of Shanti in the film
| film name      = 
| director       = Baraguru Ramachandrappa
| producer       = Ramesh Yadav
| writer         = 
| screenplay     = Baraguru Ramachandrappa
| story          = Baraguru Ramachandrappa
| narrator       =  Bhavana
| music          = Hamsalekha
| cinematography = Nagaraj Adavani
| editing        = Suresh Urs
| studio         = 
| distributor    = Ramesh Yadav Movies
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

Shanti ( ,  ) is a film in the Kannada language directed by renowned Kannada writer and director Baraguru Ramachandrappa.    The film features only a single actor and hence has found an entry in the Guinness World Records in the category Fewest actors in a narrative film.       Bhavana (Kannada actress)|Bhavana is the only actress in this film and there are no other actors.

==Theme==
Baraguru Ramachandrappa decided to direct this film to portray different faces of peace by juxtaposing symbolism and monologues.  The main theme of the film is to explore the various ways to fight terrorism without any compromise with the human values promoted by the United Nations. The UN was used as a vital aspect in the film since Baragur feels that it is a world body that has created two contradicting influences of war and peace. Baragur indicates that though this was sort of an experiment, experimentation was not the main motivation for producing this film. Rather than the concept, more focus has been given on the creative aspects of the theme.  A female character was chosen as the sole actress in the film since peace is generally considered feminine whereas war is more masculine. The film does not end with a moral, something uncommon among Indian movies. This is because Baraguru wanted the audience to start off a discussion to weigh the pros and cons of war against peace.

==Cast and crew== Bhavana is the sole actor. The other characters in the film were only represented by symbols and voices. A dove in the film is used as a symbol for peace and guns are used to portray war and terror. A voice over the telephone represents a character called as Samara (a term that means war in Kannada). In the film, Bhavana had to wear dresses to portray Mahatma Gandhi and the Gautama Buddha|Buddha, the ambassadors of peace. The other crew members include Ramesh Yadav - the producer, Hamsalekha - the music director, Suresh Urs - the editor and Nagaraja Adavani - the cameraman. Apart from direction, Baraguru has also written the screenplay for the film which he indicates was a tough task as he could not compromise on logic and reasoning while accommodating ideological issues. Baraguru who was a professor of Kannada in the Bangalore University is a well known personality in the Kannada literary field since he has held high positions like the chairman of Kannada Development Authority. The film was entirely shot in the vicinity of the Nalknad Palace in the Kodagu district of Karnataka. 

==Plot==
The only character in the film is of an illustrator and a painter called as Shanti. An international peace body chooses her to create the logo of a world peace conference. In order to draw inspiration, Shanti travels to a picturesque hilly area where she gets accosted by a group of terrorists. They kidnap her and put forth a demand that she should take them to the conference as her assistants. Their ultimate plan is to blow up the conference hall. The main storyline of the film is the way in which Shanti solves this problem and saves the conference.

From the storyline, it seems that more than one actor is required on the screen. However, Baraguru has handled this uniquely by showing only the character of Shanti on the screen while the remaining characters only contribute in terms of their voices.

==Release==
Baraguru decided to do some innovation in order to promote this film. He realised that due to the unique theme of this film; releasing this film in big theatres would not attract a sizeable audience. He decided to show this film in small towns and villages instead. He decided to travel along the highway between the towns of Bangalore and Belgaum and show this film in places that fall along the way. This unique method was termed as Shanti Chitra Yaatre (travel of the Shanti film).  The film saw a fabulous response and in some towns like Davangere and Chitradurga people had to turn back since the film ran house-full. Despite being an art film, around 50000 people are supposed to have seen this film. 

==Awards== American film. La Derniere Lettre. of 2002. 
* Karnataka state award for the second-best film of the year 2004, thereby winning a cash award of Rs.75,000 and a silver medal.

==References==
 

 
 
 
 
 