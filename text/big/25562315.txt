Here (2009 film)
{{Infobox Film
| name = Here
| image = Here film 2009.jpg
| caption = Official Picture House Poster
| director = Tzu Nyen Ho
| producer =Fran Borgia Michel Cayla Jason Lai
| writer = Tzu Nyen Ho
| cinematography = Jack Tan
| editing = Fran Borgia
| released =  
| runtime = 86 minutes
| country = Singapore Mandarin
}}
Here is a   in 2009.

==Synopsis==
Here follows the journey of a man who struggles to make sense of his reality. Reeling from the sudden death of his wife, he loses the will to speak and is interned at Island Hospital. There, he meets strident kleptomaniac Beatrice with whom he forms an inexplicable bond. As the man adjusts to life within, he is selected for an experimental treatment, which forces him to confront the devastating truth behind his past, present, and future.

==Cast==
* John Low as H
* Jo Tan as B
* Hemang Yadav as Freddie
* George Kuruvilla as Editor

==References==
 

==External links==
*  
*  

 
 
 
 

 