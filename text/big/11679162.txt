Nidhanaya
{{Infobox film
 | name = Nidhanaya
 | image = Nidhanaya.jpg
 | imagesize=150px
 | caption = 
 | director = Lester James Peries
 | producer = R Anthonypillai
 | writer = G. B. Senanayake
 | starring = Malini Fonseka Gamini Fonseka Shanti Lekha Saman Bokalawela Trilishia Gunarathne Francis Perera Wijeratne Warakagoda
 | music = Premasiri Kemadasa
 | cinematography = 
 | editing = Lester James Peries Edvin Leetin Gladvin Fernando
 | distributor = 
 | country    = Sri Lanka 1972 
| runtime =   Sinhala
 | budget = 
 | followed_by =
}} 1972 Sinhalese film directed by Lester James Peries, starring Gamini Fonseka and Malini Fonseka. Movie is based on a short story written by G.B.Senanayake in one of his short story collection known as "The Revenge". It revolves around a murder which is committed for the purpose of gaining access to a hidden treasure. The film won the Silver Lion of St. Mark award at the 1972 Venice International Film Festival and was also selected as one of the outstanding films of the year, receiving a Diploma, at the London Film Festival. It was also  chosen as the best film of the first 50 years of Sri Lankan cinema.

==Synopsis==
 sacrifice the virgin woman who has four black birth marks in her neck, in order to gain access to the treasure.

One day when he walk by a river, he accidentally comes across a young lady (Malini Fonseka), who has four black birth marks in her neck. After following the lady and collecting information about her, he decides to marry her. He eventually marries the lady and spends time with her.

In the meantime, the lady notices that her husband always spends time thinking. One day she asks him as to what keeps the man thinking all the time. He replies that he has to do a religious custom to a god in a rock. So, his wife agrees with him and thereby arranges everything for it.

After going to the rock, the man starts to fulfill the custom and finally he kills his wife as a sacrifice, with the hope of gaining access to the hidden treasure. But, unfortunately he is yet unable to get the treasure.

Sadly he comes home and decides to write the complete story, in his diary. After completing the story, he commits suicide by hanging himself. 

==Cast==

Gamini Fonseka as Willy
Malini Fonseka as Irene

 

==Restoration==
In 2013, Shivendra Singh Dungarpur has collaborated with the World Cinema Foundation (WCF) for the restoration of the film. The film won the Silver Lion at the 1972 Venice International Film Festival.

Dr. Peries’ films are in a poor condition and many of the original camera negatives have been lost. This restoration is a joint effort of the WCF, the Sri Lankan National Film Corporation, Mr. Padmarajah (the copyright holder), and the National Film Archive of India (NFAI) and Sameera Randeniya of Film Team, Sri Lanka.

==External links==
* 
* 
*  

==References==
 

 
 
 
 
 