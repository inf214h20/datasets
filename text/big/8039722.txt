Wake Up, Ron Burgundy: The Lost Movie

{{Infobox film
| name           = Wake Up, Ron Burgundy:  The Lost Movie
| image          = Anchormanwakeupdvd.jpg
| alt                =
| director       = Adam McKay
| writer         = Will Ferrell Adam McKay
| starring       = {{Plain list | 
* Will Ferrell
* Christina Applegate
* Paul Rudd
* Steve Carell
}}
| music          = Alex Wurman
| cinematography = Thomas E. Ackerman
| editing        = Brent White
| producer       = Judd Apatow
| distributor    = DreamWorks Distribution 
| runtime        = 93 minutes 
| country        = United States English
| budget         =
}}

Wake Up, Ron Burgundy: The Lost Movie is the 2004 counterpart film to the film  , which was also released in the same year. Directed by Adam McKay and written by McKay and Will Ferrell, it stars Ferrell, Christina Applegate, David Koechner, Steve Carell, and Paul Rudd.

==Plot==
The film "follows the KVWN Channel 4 News Team as they investigate the extremist bank-robbing organization The Alarm Clock." 

==Cast==
Main cast in credits order: 
 
* Will Ferrell as Ron Burgundy
* Christina Applegate as Veronica Corningstone
* Paul Rudd as Brian Fantana
* Steve Carell as Brick Tamland
* David Koechner as Champ Kind
* Kevin Corrigan as Paul Hauser
* Fred Willard as Ed Harken
* Chris Parnell as Garth Holliday
* Chuck D as Malcolm Y
* Maya Rudolph as Kanshasha X
* Kathryn Hahn as Helen
* Fred Armisen as Tino
* Chad Everett as Jess Moondragon
* Tara Subkoff as Mouse
* Justin Long  as Chris Harken Michael Coleman as Construction Worker 
* Steve Bannos as Nikos 
 

==Reception==
Bill Beyrer of CinemaBlend, reviewing the film as part of the DVD release for Anchorman: The Legend of Ron Burgundy, called it "quite possibly the very worst pseudo sequel ever"; according to Beyrer, "As much as it claims to be, this is not a continued adventure. Wake-Up Ron Burgundy is nothing more than a collection of deleted scenes and alternate takes ... sewn together with narration and a left out story element included to make it seem like a follow up. The worst part about this is that a majority of the alternate takes already appear on the deleted scenes or blooper reel of the first film, as well as appearing a majority of the theatrical trailers. 

==References==
{{reflist|refs=

   
}}

 
 
 

 
 
 
 
 
 
 
 
 
 
 


 