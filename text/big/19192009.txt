Shirin (film)
{{Infobox film
| name = Shirin|
| image = Shirin.jpg
| caption = © 2008 MK2
| director =Abbas Kiarostami|
| starring = Niki Karimi  Golshifteh Farahani  Juliette Binoche| Mahnaz Afshar  Hedye Tehrani
| producer =|
| released =  
| country = Iran
| language = Persian
| runtime = 92 minutes
| distributor =
}} Iranian filmmaker Abbas Kiarostami. The film is considered by some critics as a notable twist in the artistic career of Kiarostami.
 Persian Romance romance tale of Khosrow and Shirin, with themes of female self-sacrifice.     The film has been described as "a compelling exploration of the relationship between image, sound and female spectatorship."    The film depicts the audiences emotional involvement with the story. The story is read between the tragic and kitsch by a cast of narrators led by Manoucher Esmaieli and is accompanied by a historical "film score" by Morteza Hananeh and Hossein Dehlavi. 

The films production is replete with curious anecdotes. According to some reports, the women were filmed individually in Kiarostamis living room, with the director asking them to cast their gaze at a mere series of dots above the camera. The director has also stated that, during the filming process, he had no idea what film they were watching, and settled on the Khosrow and Shirin myth only after shooting had concluded.  In the making-of "Taste of Shirin" one can see how the film really was shot. The brief appearance of Juliette Binoche in the film came as the result of her role in another Kiarostami project.

It was first screened at the 65th Venice International Film Festival. 

==Cast==
More than 100 actresses took part in this film.  Some of them are:
 
*Mahnaz Afshar
*Pegah Ahangarani
*Taraneh Alidoosti
* Zahra Amir Ebrahimi
* Khatereh Asadi
* Vishka Asayesh
* Rana Azadivar
* Pantea Bahram
* Pouri Banai
* Afsaneh Bayegan
*Juliette Binoche
* Sahar Dolatshahi
*Setareh Eskandari
*Golshifteh Farahani
*Shaghayegh Farahani
* Bita Farahi
*Elsa Firouz Azar
* Soraya Ghasemi
*Fatemeh Gudarzi
* Azita Hajian
*Leila Hatami Irene
* Behnaz Jafari
*Negar Javaherian
*Falamak Jonidi
*Niki Karimi
* Mahtab Keramati
*Hamideh Kheirabadi
* Gohar Kheirandish
*Niku Kheradmand
*Sara Khoeniha
*Baran Kosari
*Fatemah Motamed-Aria
*Shabnam Moghaddamy
*Ladan Mostofi
* Yekta Naser
*Roya Nonahali
*Zhale Olov
*Rabee Oskooyi
*Mahaya Petrossian
*Leili Rashidi
*Katayoun Riahi
*Homeira Riazi Elnaz Shakerdust
*Hanieh Tavassoli
*Roya Taymourian
*Hedye Tehrani
*Sahar Valadbeigi
*Laya Zanganeh Merila Zarei Niousha Zeighami

 

==See also==
*List of Iranian films

==References==
 

==External links==
* Deborah Young,   (Reuters, 2008)
*  

 

 
 
 