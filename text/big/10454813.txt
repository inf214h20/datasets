Wake in Fright
 
{{Infobox film
| name           = Wake in Fright
| image          = WakeInFrightAd1.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Original Australian theatrical release poster
| director       = Ted Kotcheff
| producer       = George Willoughby Evan Jones
| based on       =  
| starring       = Gary Bond Donald Pleasence Chips Rafferty John Scott
| cinematography = Brian West
| editing        = Anthony Buckley Group W
| distributor    = United Artists   Drafthouse Films  
| released       =  
| runtime        = 108 minutes  
| country        = Australia United States
| language       = English
| budget         =  
}} thriller film Evan Jones, based on Kenneth Cooks 1961 novel of the same name.

Made on a budget of  , the film was an Australian/American co-production by NLT Productions and Group W.  Wake in Fright tells the story of a young schoolteacher who descends into personal moral degradation after finding himself stranded in a brutal, menacing town in outback Australia.

For many years, Wake in Fright enjoyed a reputation as Australias great "lost film" because of its unavailability on VHS or DVD, as well as its absence from television broadcasts. In mid-2009, however, a thoroughly restored digital re-release was shown in Australian theatres to considerable acclaim. Later that year it was issued commercially on DVD and Blu-ray Disc. Wake in Fright is now recognised as a seminal film of the Australian New Wave.  Australian musician and screenwriter Nick Cave called Wake in Fright "The best and most terrifying film about Australia in existence." 

==Plot==
John Grant is a middle-class teacher from the big city. He feels disgruntled because of the onerous terms of a financial bond which he signed with the government in return for receiving a tertiary education. The bond has forced him to accept a post to the tiny school at Tiboonda, a remote township in the arid Australian Outback. It is the start of the Christmas school holidays and Grant plans on going to Sydney to visit his girlfriend, but first he must travel by train to the nearby mining town of Bundanyabba (known as “The Yabba”) in order to catch a Sydney-bound flight.

At "The Yabba", Grant encounters several disconcerting residents including a policeman, Jock Crawford, who encourages Grant to drink repeated glasses of beer before introducing him to the local obsession with the gambling game of two-up. Hoping to win enough money to pay off his bond and escape his "slavery" as an outback teacher, Grant at first has a winning streak playing two-up but then loses all his cash. Unable now to leave "The Yabba", Grant finds himself dependent on the charity of bullying strangers while being drawn into the crude and hard-drinking lifestyle of the towns residents.

Grant reluctantly goes drinking with a resident named Tim Hynes (Al Thomas) and goes to Tims house. Here he meets Tims daughter, Janette. While he and Janette talk, several men who have gathered at the house for a drinking session question Grants masculinity, asking: “Whats the matter with him?  Hed rather talk to a woman than drink beer.” Janette then tries to initiate an awkward sexual episode with Grant, who vomits. Grant finds refuge of a sort, staying at the shack of an alcoholic medical practitioner known as  "Doc" Tydon. Doc tells him that he and many others have had sex with Janette. He also gives Grant pills from his medical kit, ostensibly to cure Grants hangover.

Later, a drunk Grant participates in a barbaric kangaroo hunt with Doc and Docs friends Dick and Joe. The hunt culminates in Grant clumsily stabbing a wounded kangaroo to death, followed by a pointless drunken brawl between Dick and Joe and the vandalizing of a bush pub. At nights end, Grant returns to Docs shack, where Doc apparently initiates a homosexual encounter between the two. 

A repulsed Grant leaves the next morning and walks across the desert. He tries to hitch-hike to Sydney, but accidentally boards a truck that takes him straight back to "The Yabba". He contemplates shooting Doc, but instead attempts suicide. Grant recovers in hospital from his suicide attempt and Doc sees him off at "The Yabbas" rail station. He returns to Tiboonda for the new school year.

==Cast==
 
* Gary Bond as John Grant
* Donald Pleasence as Doc Tydon
* Chips Rafferty as Jock Crawford
* Sylvia Kay as Janette Hynes
* John Meillon as Charlie Jack Thompson as Dick
* Peter Whittle as Joe
* Al Thomas as Tim Hynes
* Dawn Lake as Joyce
* John Armstrong as Atkins
* Slim de Grey as Jarvis
* Maggie Dence as receptionist
* Norman Erskine as Joe the cook

==Production== film version The Apprenticeship Fun with Dick and Jane (1977), First Blood (1982) and Weekend at Bernies (1988).

The shooting of Wake in Fright began in January 1970 in the mining town of Broken Hill, New South Wales (the area which had inspired Cook for the setting of his novel), with interiors shot the next month at Ajax Studios in the Sydney beach-side suburb of Bondi, New South Wales|Bondi. It was the last film to feature the veteran character actor Chips Rafferty, who died of a heart attack prior to Wake in Fright s release, and the first film with Jack Thompson, the future Australian cinema star, among its cast members. Coincidentally, Rafferty (real name John William Pilbean Goffage) had been born in Broken Hill, the films stand-in for "The Yabba", in 1909.

==Response== world premiere of Wake in Fright (as Outback) occurred at the 1971 Cannes Film Festival, held in May. Ted Kotcheff was nominated for a Golden Palm Award.    The film opened commercially in France on 22 July 1971, Great Britain on 29 October 1971, Australia during the same month and the United States on 20 February 1972.

Wake in Fright received generally excellent reviews throughout the world and found a favourable public response in France (where it ran for five months) and in the United Kingdom. However, despite receiving such critical support at Cannes and in Australia, Wake in Fright suffered poor domestic box-office returns. Although there were complaints that the films distributor, United Artists, had failed to promote the film successfully, it was also thought that the film was “perhaps too uncomfortably direct and uncompromising to draw large Australian audiences".  During an early Australian screening, one man stood up, pointed at the screen and protested "Thats not us!", to which Jack Thompson yelled back "Sit down, mate. It is us." 

The un-restored version of Wake in Fright received a three stars (out of four) rating from the American film reviewer Leonard Maltin in his 2006 Movie Guide, while Brian McFarlane, writing in 1999 in The Oxford Companion to Australian Film, said that it was “almost uniquely unsettling in the history of new Australian Cinema”. Askmen.com echoed these sentiments, citing that "its not hard to see why the dusty savagery and clown-faced surrealism of Ted Kotcheffs fourth feature was never shown on telly at the time." 

Following the films restoration, Wake in Fright screened at the 2009 Cannes Film Festival on 15 May 2009 when it was selected as a Cannes Classic title by the head of the department, Martin Scorsese.    Wake in Fright is one of only two films ever to screen twice in the history of the festival.    Scorsese said, "Wake in Fright is a deeply -- and I mean deeply -- unsettling and disturbing movie. I saw it when it premiered at Cannes in 1971, and it left me speechless. Visually, dramatically, atmospherically and psychologically, its beautifully calibrated and it gets under your skin one encounter at a time, right along with the protagonist played by Gary Bond. Im excited that Wake in Fright has been preserved and restored and that it is finally getting the exposure it deserves." 
 SBS gave the film four stars out of five, claiming that "Wake in Fright deserves to rank as an Australian classic as it packs enormous emotional force, was bravely and inventively directed, and features superb performances. "  Rex Reed, an early advocate of Wake in Fright, praised the films restoration as "the best movie news of the year", and said it "may be the greatest Australian film ever made". 

Currently, the film has a rating of 100% on Rotten Tomatoes, based on 45 reviews; the consensus states: "A disquieting classic of Australian cinema, Wake in Fright surveys a landscape both sun-drenched and ruthlessly dark."  The site also ranks Wake in Fright as the 68th best-reviewed film of all time. 

==Controversy==
In addition to the films atmosphere of sordid realism, the kangaroo hunting scene contains graphic footage of kangaroos actually being shot.  A disclaimer at the conclusion of the movie states:

 Producers Note.

The hunting scenes depicted in this film were taken during an actual kangaroo hunt by professional licensed hunters.

For this reason and because the survival of the Australian kangaroo is seriously threatened, these scenes were shown uncut after consultation with the leading animal welfare organisations in Australia and the United Kingdom.  

The hunt lasted several hours, and gradually wore down the filmmakers. According to cinematographer Brian West, "the hunters were getting really drunk and they started to miss, ... It was becoming this orgy of killing and we   were getting sick of it." Kangaroos hopped about helplessly with gun wounds and trailing intestines. Producer George Willoughby reportedly fainted after seeing a kangaroo "splattered in a particularly spectacular fashion". The crew orchestrated a power failure in order to end the hunt. 

At the 2009 Cannes Classic screening of Wake in Fright, 12 people walked out during the kangaroo hunt. 

Director Ted Kotcheff, a professed vegetarian,  has defended his use of the hunting footage in the film. 

==Restoration and release on DVD and Blu-ray Disc==
For many years, the only known print of Wake in Fright, found in Dublin, was considered of insufficient quality for transfer to DVD or videotape for commercial release. In response to this situation, Wake in Fright s editor, Anthony Buckley, began to search in 1994 for a better-preserved copy of the film in an uncut state. Ten years later, in Pittsburgh, Buckley found the negatives of Wake in Fright in a shipping container labelled "For Destruction". He rescued the material, which formed the basis for the films painstaking 2009 restoration.  Evidently a 35mm print in excellent condition had also survived in the collection of the Library of Congress, which screened it in the librarys Mary Pickford Theater in 2008, although its reported running time of only 96 minutes suggests this was an edited version. 

Wake in Fright was released on DVD and Blu-ray Disc formats on 4 November 2009,  based on a digital restoration completed earlier that year. This restoration was shown to the general public for the first time at the Sydney Film Festival in June 2009, was subsequently given limited theatrical re-release in Australia and received wide and consistently positive coverage in the Australian media.   

==Further reading==
* Adams, B & Shirley, G. (1983)	Australian Cinema: The First Eighty Years, Angus and Robertson, ISBN 0-312-06126-9
* Buckley, Anthony (2009) Behind a Velvet Light Trap: A Filmmakers Journey from Cinesound to Cannes. Prahran: Hardie Grant Books. ISBN 978-1-74066-7906
* Caterson, S., (2006) "The Best Australian Film Youve Never Seen", Quadrant, pp.&nbsp;86–88, Jan–Feb 2006.
* Greenwood, P.  (2006)	Wake in Fright, Murdoch University,  .  Accessed 15 January 2007.
* Kaufman, Tina (2010) Wake in Fright: Australian Screen Classics. Sydney: Currency Press. ISBN 978-0-86819-864-4
* McFarlane, B. (1999) The Oxford Companion to Australian Film, Melbourne: Oxford University Press, Melbourne.  ISBN 978-0-19-553797-0
* Maddox, G. (2004) "Treasure, not trash: classic found in US", The Sydney Morning Herald, p 13, 16 October 2004.
* Maltin, L. (2006) Leonard Maltins 2006 Movie Guide, Signet, ISBN 0-451-21609-1.
* Pike, A. & Cooper, R. (1998) Australian Film 1900–1977, Oxford University Press, Melbourne.  ISBN 0-19-550784-3
* Williamson, G. (2006)	“The  Forum”, The Australian, p.&nbsp;5, 30 December 2006.
* Zion, L. (2006) "DVD Letterbox", The Australian, p.&nbsp;25, 29 July 2006.
* Hoey. Gregory   "Wake in Fright:by an elephant in the room" a book of personal memoirs.

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at    
*  
*  at Oz Movies
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 