Variety Time
{{Infobox film
| name           = Variety Time
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Hal Yates
| producer       = George Bilson
| writer         = 
| screenplay     = Hal Law Hal Yates Leo Solomon Joseph Quillan
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Constantin Bakaleinikoff
| cinematography = Robert de Grasse Vincent J. Farrar George Diskant
| editing        = Les Millbrook Edward W. Williams RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Variety Time is a 1948 American variety film directed by Hal Yates. The film is a composite of two film shorts interspersed with routines by  , Hal Yates, Leo Solomon, and Joseph Quillan. Aside from Paar, the film also stars Edgar Kennedy and Leon Errol, each in one of the film shorts.

==References==
 

 
 
 


 