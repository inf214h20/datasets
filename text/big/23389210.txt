The Runaway Princess
{{Infobox film
| name           = The Runaway Princess
| image          = 
| image_size     = 
| caption        = 
| director       = Anthony Asquith   Fritz Wendhausen 
| producer       = Harry Bruce Woolfe Elizabeth Russell (novel)   Alfred Schirokauer
| narrator       = 
| starring       = Mady Christians Norah Baring Paul Cavanagh Anne Grey
| music          =  Henry Harris   Fritz Wendhausen   Arpad Viragh
| editing        = 
| studio         = British Instructional Films   Laender Film
| distributor    = Jury Metro-Goldwyn 
| released       = March 1929
| runtime        = 7,053 feet
| country        = United Kingdom   Germany
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} German silent silent drama film directed by Anthony Asquith and Fritz Wendhausen and starring Mady Christians, Fred Rains, Paul Cavanagh and Anne Grey. 

==Production== Elizabeth Russell. An alternative German-language version known as Priscillas Fahrt ins Glück was directed by Fritz Wendhausen

==Cast==
* Mady Christians - Princess Priscilla 
* Paul Cavanagh - Prince of Savonia 
* Norah Baring - The Forger 
* Fred Rains - The Professor 
* Claude Beerbohm - The Detective 
* Eveline Chipman   
* Lewis Dayton   
* Anne Grey

==References==
 

==External links==
 

 

 
 
 
 
 
 
 
 
 
 
 


 
 