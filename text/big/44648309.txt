Kabadi Kabadi (2003 film)
{{Infobox film
| name           = Kabadi Kabadi
| image          =
| caption        =
| writer         = Veegesna Satish  
| story          = Venky
| screenplay     = Venky
| producer       = Valluripally Ramesh Babu
| director       = Venky Kalyani
| Chakri
| cinematography = S.K.A. Bhupathi
| editing        = Basva Paidireddy
| studio         = Maharshi Cinema
| released       =  
| runtime        = 131 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Kalyani in the lead roles and music composed by Chakri (music director)|Chakri.          

==Cast==
 
* Jagapati Babu as Rambabu Kalyani as Kaveri 
* Brahmanandam
* Tanikella Bharani as Veerabhadrayah
* M. S. Narayana as Appa Rao
* Jaya Prakash Reddy as Head Constable Kondavalasa as Krishna Rao 
* Krishna Bhagawan as Bosu
* Raghu Babu as Seenu
* Suman Shetty as Priest
* Surya as Nagendra Chinna as Madman
* Potti Prasad as Villager
* Sarika Ramachandra Rao as Anjineelu
* Gundu Sudarshan as Priest
* Gowtham Raju as Villager Jeeva as President Jenny as Kabadi Refinery
* Rajitha as Rani
* Preeti Nigam as Nagendras wife
 

==Soundtrack==
{{Infobox album
| Name        = Kabadi Kabadi
| Tagline     = 
| Type        = film Chakri
| Cover       = 
| Released    = 2003
| Recorded    = 
| Genre       = Soundtrack
| Length      = 27:35
| Label       = Aditya Music Chakri
| Reviews     =
| Last album  = Avunu Valliddaru Ista Paddaru   (2002)
| This album  = Kabadi Kabadi   (2003)
| Next album  = Donga Ramudu & Party   (2003)
}}

Music composed by Chakri (music director)|Chakri. All songs are blockbusters. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 27:35
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Goruvanka Godarivanka
| lyrics1 = Bhaskarabhatla Kousalya
| length1 = 4:48

| title2  = Jabilli Bugganugilli
| lyrics2 = Bhaskarabhatla
| extra2  = Hariharan (singer)|Hariharan,Kousalya
| length2 = 5:04

| title3  = Kokila Kokila
| lyrics3 = Bhaskarabhatla
| extra3  = Chakri,Kousalya
| length3 = 3:57

| title4  = Prema Prema 
| lyrics4 = Sandeep,Kousalya
| extra4  = Sai Sriharsha
| length4 = 5:06

| title5  = Kabadi Kabadi 
| lyrics5 = Kaluva Krishna Sai 
| extra5  = Ravi Varma,Chakri
| length5 = 4:31

| title6  = Kokila Kokila -II
| lyrics6 = Bhaskarabhatla
| extra6  = Chakri,Kousalya
| length6 = 3:55
}}
   

==References==
 

 
 
 


 