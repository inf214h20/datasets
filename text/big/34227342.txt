Poovizhi Vasalile
{{Infobox film
| name           = Poovizhi Vasalile
| image          = 
| caption        =  Fazil
| producer       = Lakshmi Priya Combines Fazil Gokula Krishna (dialogues)
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Anandakuttan
| editing        = T. R. Sekhar
| studio         = Lakshmi Priya Combines
| distributor    = Lakshmi Priya Combines
| released       =  
| country        = India
| runtime        = 155 min
| language       = Tamil
}}
 1987 Cinema Indian Tamil Tamilsuspense & Malayalam film Poovinu Puthiya Poonthennal.   This film received critical acclaim from the critics and went on screens for more than 25 weeks in Chennai, Kovai, Madurai and Trichy and completed 100 days in other parts as well. It was declared a super hit at the box office. The film has a certain cult following and has been noted for the villainous performance of Babu Antony and Raghuvaran.

==Plot==

A deaf-mute boy, Benny (Sujitha) and his mother (Rajalakshmi) witnessed a man get killed by two men (Raghuvaran and Babu Antony). So they also killed the mother but the boy escaped from them.

Jeeva (Sathyaraj) is an alcoholic who is not able to recover from the tragic death of his family. Jeeva finds the boy when he is sleeping in the trash and adopts him as Raja naming him after his son. Soon, he meets Yamuna (Karthika (actress)|Karthika) and become close friends without knowing that she is Rajas aunt. Raja recognizes his mothers murderer in a bar along with their boss.

First, the police arrests Jeeva for kidnapping Raja and for hiding his mother Stella, but the police discovers the body of Stella with Rajas help and Yamuna understands it is her sisters son. Anand and Ranjith kidnap Raja, from Yamuna, so Jeeva kills the murderers and goes to jail.

==Cast==

*Sathyaraj as Jeeva
*Sujitha as Benny/Raja Karthika as Yamuna
*Raghuvaran as Anand
*Babu Antony as Ranjith
*Nizhalgal Ravi as a police inspector
*T. S. Raghavendra as Viswanathan
*Rajalakshmi as Ganga/Stella
*Sreenath
*Charle in a guest appearance
*Delhi Ganesh in a guest appearance

==Soundtrack==

{{Infobox album |  
  Name        = Poovizhi Vasalile |
  Type        = soundtrack |
  Artist      = Ilaiyaraaja|
  Cover       = |
  Released    = 1987 |
  Recorded    = 1987 | Feature film soundtrack |
  Length      = 26:43 |
  Label       = |
  Producer    = Ilaiyaraaja|  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1987, features 6 tracks with lyrics written by Ilaiyaraaja, Gangai Amaran, Muthulingam  and Kamakodiyan.     

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || "Aattam Ingey" || Malaysia Vasudevan, S. P. Sailaja || 4:27
|- 2 || Mano || 4:14
|- 3 || "Chinna Chinna Roja Poove" || K. J. Yesudas || 4:29
|- 4 || "Oru Kiliyin" (male) || K. J. Yesudas || 4:35
|- 5 || "Oru Kiliyin" (female) || K. S. Chithra || 4:31
|- 6 || "Paattu Engae || S. P Shailaja, Malaysia Vasudevan || 4:27
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 