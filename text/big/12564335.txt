From Within (film)
{{Infobox film
| name           = From Within
| image          = From Within poster.jpg
| caption        = Teaser poster
| director       = Phedon Papamichael Jr.
| producer       = Chris Gibbin Adrian Butchart
| writer         = Brad Keene Thomas Dekker Elizabeth Rice Adam Goldberg Rumer Willis Jason Cooper Oliver Kraus The Hoosiers Ane Brun
| cinematography =
| editing        =
| distributor    = After Dark Films Lions Gate
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
From Within is a horror film directed by Phedon Papamichael Jr. and written by Brad Keene.  Filming took place in Maryland in fall 2007.  The film premiered at the Tribeca Film Festival in April–May 2008.

==Premise== Thomas Dekker), a non-believer. Lindsay discovers that something evil is behind the suicides and that she is next to die.

==Plot==
The film begins with a teenage girl, Natalie (Rumer Willis) sitting with her boyfriend Sean as he reads from a black book. He finishes reading, and kisses Natalie passionately. Natalie believes he wants to have sex with her – until he pulls out a gun and commits suicide.

In the main town high school girl, Lindsay, is shopping with her mother, Trish, for new church clothes. As they argue, Natalie bursts into the shop covered in blood and mumbling about a girl following her. Lindsay watches over her and notices the black book she holds. When Lindsay goes to fetch Natalies father, the door to the back room slams shut on Natalie. Natalies father knocks down the door, only to find Natalie dying, with a pair of scissors lodged into her neck.

That night Natalies father is haunted by a ghostly figure and is found the following day, hanged in the same room Natalie died in, by his niece Molly.

On Monday at school Lindsays boyfriend, Dylan, who is the son of the local pastor, is found publicly attacking Aidan Spindle, a newly returned local misfit. Dylan blames Aidans family for the recent deaths. Following the fight, Lindsay helps Aidan with his minor wounds and drives him home. It turns out that Aidans brother is Sean, the young man who killed himself at the start of the film but whose death has been overshadowed by Natalies.

The same night Molly starts seeing a ghost that looks exactly like herself which slits Mollys wrists. The following day, Lindsays best friend Claire drives past Mollys body, and immediately begins to hear voices. Later the same day, a duplicate of Claire is seen in the toilets while Claire is using the facilities.

Simultaneously, Lindsay becomes aware of the religious fervor Dylan is stirring up against Aidan among the townsfolk and goes to warn him of the danger. On entering his house, she meets his cousin, Sadie who had traveled down from New York for Aidans brother Seans funeral.

Meanwhile, Claire encounters and unsuccessfully tries to evade her doppelganger. It finally catches up with her as she is driving and her car explodes in front of Lindsays mother Trish, who tried to rescue her.

Aidan takes Lindsay to a stream running underneath a bridge, and confirms that his mother practiced a form of witchcraft, which may be the cause of the suicides. He tells her to leave Grovetown.

Lindsay arrives home that night and is abducted by Dylan and Roy, Trishs lover, with Trishs consent. She is taken to the local church to be "purified". Returning home that night, Trish is jubilant about "being saved" but is later haunted by doppelgangers of herself on the television. After being stalked throughout the house, Trishs doppelganger compels her to drink drain cleaner, believing it to be whiskey.

Lindsay finds Trishs body and she rushes to Aidans house, frantic in her belief that a curse has gotten inside of her and will demand her death. Aidan admits that victims of the curse see themselves, but swears that all victims commit suicide because they are compelled to, not because something takes over them. Aidan supplies Lindsay with an oil on her skin that will slow the progress of the curse. Aidan realizes that the black book is the key to lock up what Sean had unlocked.

Lindsay suggests the church as the new location of the book and Lindsay and Aidan break in. Dylan, Roy, Pastor Joe and Paul, another member of Joes congregation, realize that Aidan retrieved the book. Back at Aidans house Sadie pulls a gun on Aidan and Lindsay; she wants the book and explains that Sean deliberately killed himself in order for the curse to start.

They manage to rescue the book just as Sadie attempts to burn it. Paul, Roy and Dylan burst into the home. Aidan and Lindsay escape to the woods, Dylan and Paul following as Roy guards Sadie. While running, Aidan and Lindsay split, intending to meet up at the pier.

Back at the home Roy tells Sadie how he became a religious man and then sets her alight with gasoline, claiming that he is doing Gods will. In the woods, Lindsay makes it to the pier, but her doppelganger ghost is hot on her tail. She stops at the edge of the platform and starts reciting a prayer, when she falls into the water.

Meanwhile Dylan continues to hunt Aidan but he is surprised from behind by Aidan. Aidan puts Sadies gun to Dylans head when Lindsays screams for help draw him away. Lindsay panics in the water as her doppelganger emerges from the water behind her. Lindsay quickly climbs out of the water alive, then blood drips onto her back, the ghosts eye is bleeding as Lindsay sits up. The ghost grabs the rope and hands it to Lindsay, who starts to wrap it around her own neck, and almost strangling herself.

Aidan rushes to her and stops her, they continue with dispelling the curse. He merges his own blood with Seans on the book, when he says to Lindsay that a sacrifice started the curse so a sacrifice must end it. Aidan passionately kisses her and tells her to burn the book, before seemingly shooting himself.

Dylan and Paul then show up, Dylan believing Lindsay to be a devil worshiper he pulls his gun on her. Lindsay stands, Sadies gun in her hands as she begs Dylan to walk away. He raises his rifle at her and she shoots him. Paul quickly runs off. Not long after the cops arrive, but they find that Aidans sacrifice doesnt count; Dylan had actually shot him in the chest as Aidan was pointing the gun at his own head.

In the woods, Lindsay tries to burn the book, but to no avail as the curse is still around, the ghost creates a gust of wind blowing the fire out before claiming Lindsay.

==Cast==
*Elizabeth Rice as Lindsay, the films central character.  Thomas Dekker as Aidan, a supposed Devil worshipper   
*Kelly Blatz as Dylan, Lindsays religious boyfriend
*Laura Allen as Trish, Lindsays religion-obsessed stepmother. 
*Adam Goldberg as Roy, Trishs lover 
*Rumer Willis as Natalie, girlfriend to Sean 
*Margo Harshman as Sadie, Aidan and Seans cousin
*Britt Robertson as Claire, Lindsays best friend
*Jared Harris as Bernard, Natalies father
*Steven Culp as Pastor Joe, Dylans father
*Amanda Babin as Molly, Natalies cousin
*Michelle Babin as Evil Molly, a form the entity takes on when targeting Molly
*Shiloh Fernandez as Sean, Natalies boyfriend and Aidans brother
*Mark A. Cummins as Classroom Teacher

==Production==

In July 2007, director   (1980) and  Rosemarys Baby (film)|Rosemarys Baby (1968).  For From Within, filming took place in Perry Point, Maryland, a defunct estate with homes that would be torn down after the completion of production.     The film entered post-production in August 2007. 

==Release==
Filmmakers originally planned to finish production in time to release the film for the Sundance Film Festival in February 2008,  but not being complete in time, From Within ultimately had its world premiere at the Tribeca Film Festival that runs between April and May 2008.   The film entered the festival "riding a considerable wave of buzz and expectation",  but The Hollywood Reporter reported that it played to "mediocre reception".   From Within later  won awards at the Solstice Film Festival in June 2008, for Best Feature Film, Best Original Score, Best Actor (Thomas Dekker) and Best Actress (Elizabeth Rice).  It was chosen for the Official Selection at the London FrightFest Film Festival and the Fantasia Festival in Montreal.

The film was released on January 9, 2009 as part of the third After Dark Films 8 Films to Die For series.

==References==
 
 

==External links==
*  
*   at Baltimore Sun
*   at Fangoria

 

 
 
 
 
 
 