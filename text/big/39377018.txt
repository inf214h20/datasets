Sex and the Single Mom

 

{{Infobox film  name = Sex and the Single Mom image = Sex and the Single Mom DVD cover.jpg
| caption        =DVD cover director = Don McBrearty  producer = Terry Gould , Lee Alexander , Andrea Baynes, Tim Enright writer = Judith Paige Mitchell starring =  music = Alexina Louie, Alex Pauk cinematography = Rhett Morita editing = Jeff Warren studio = Andrea Baynes Productions distributor =   released =   runtime = 120 minutes country = USA language = English
|genre=Drama
}}

Sex and the Single Mom is an American 2003 made for television drama film directed by Don McBrearty.   
 
== Synopsis ==
 
Jess (Gail OGrady)is a single and overly concerned mother of a 15 year old teenage girl, Sara (Danielle Panabaker).  She becomes even more over protective when Sara tells her about thinking of having sex with her new boyfriend. The things between Sara and Jess start to change when Jess begins an affair with a newly single doctor. 

== Cast ==
* Gail OGrady as Jess Gradwell
* Grant Show as Alex Lofton
* Danielle Panabaker as Sara Gradwell
* Nigel Bennett as Nick Gradwell
* Cindy Sampson as April Gradwell
* Joshua Close as Tyler
* Kyle Schmid as Chad
* Shelley Thompson as Alyssa
* Barbara Gordon as Valerie
* Heather Blom as Leeza
* John Maclaren as Harrison
* Maria Ricossa as Deena
* Geordie Brown as Frankie
* Jamie Bradley as Howard.

== Reception ==
The Oklahoma Gazette panned the movie overall, while DVD Verdict gave a more positive review for Sex and the Single Mom, saying it was "quite entertaining, well acted, and well made, save for certain unnecessary plot developments".  

== Sequel ==
A sequel to Sex and the Single Mom, entitled More Sex and the Single Mom, was released in 2005 with OGrady reprising her role as Jess. The film focuses on Jesss life as a mother of a teenage daughter and three-year-old son, as well as on her increasingly complicated love and sex life. In an interview with The Tuscaloosa News, OGrady admitted that she had been "pleasantly surprised" when informed that there would be a sequel and stated that the ending of the second film left the door open for future sequels. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 