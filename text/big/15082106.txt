Krantiveer
{{Infobox film
| name = Krantiveer
| image =Krantiveer.jpg
| image_size =
| caption = DVD cover
| director = Mehul Kumar
| producer = Mehul Kumar   
| writer = Ishraq-Suja 
| narrator =
| starring =Nana Patekar Dimple Kapadia Atul Agnihotri Mamta Kulkarni Danny Denzongpa Paresh Rawal Sameer (lyrics)
| cinematography =
| editing = Yusuf Sheikh
| distributor =Mehul Movies Pvt Ltd
| released =  
| runtime = 159 min
| country = India
| language = Hindi
| budget = 2.5 Million 
| gross = 24.4 Million
}}
 action crime National Film Award.
 Telugu as Punya Bhoomi Naa Desam (1995) with Mohan Babu and in Kannada as Parodi (2007) with Upendra.

==Plot==
Pratap Tilak(Nana Patekar) is the grandson of Bheeshmanarayan Tilak a freedom fighter. Pratap starts gambling and spoiling attack and he dies. Prataps mother Durgadevi(Farida Jalal) asks him to leave the village and go away. Pratap comes to Mumbai where he saves life of chawl owner, Laxminaths(Paresh Rawal) son Atul(Atul Agnihotri). Laxminath decides to keep Pratap with him. When they grow up Atul falls in love with Mamta(Mamta Kulkarni), who is the daughter of a builder named Yograj(Tinu Anand). Pratap keeps laughing at press reporter Megha Dixit (Dimple Kapadia) who lives in the chawl and keeps on fighting injustice by writing about it in newspapers. Pratap teaches people to become strong and fight for themselves instead of waiting for other people to help them. Chattursingh Chita(Danny Denzongpa) and Yograj plan to build a resort and the place they arrange communal riots mass killings and burn the houses of people. Laxminath is murdered by Chattursingh Chita. Pratap learns that Meghas parents were murdered by Chattursingh Chita and she was raped by him. He proposes marriage to her. Mamta leaves her fathers house and comes to Atuls house. Pratap kills the corrupted ministers, the judge and the police officer. He is caught and ordered to be hanged till death. Chattursingh Chita plans to kill Pratap but dies by Prataps hands. This story is about a brave person who decides to face injustice and is ready to lay his lifedown for this cause.

==Cast==
*Nana Patekar&nbsp;... Pratap Narayan Tilak
*Dimple Kapadia&nbsp;... Meghna Dixit
*Atul Agnihotri&nbsp;... Atul
*Mamta Kulkarni&nbsp;... Mamta
*Danny Denzongpa&nbsp;... Chatur Singh Chitah
*Paresh Rawal&nbsp;... Slum Landlord Laxmidas Dayal
*Farida Jalal&nbsp;... Mrs. Tilak
*Tinu Anand ... Builder Yograj
*Shafi Inamdar ... TV Interviewer
*Mushtaq Khan ... Babbanrao Deshmukh
*Ishrat Ali ... Chandrasen Azad
*Mahesh Anand ... Vaisiram
*Sujit Kumar ... Police Commissioner
*Vikas Anand ... Hukum Ali Javed
*Man Mauji ... Dayal(Casino Owner)
*Viju Khote ... Dr Vishwanath(Dr at Hospital where dimple kapadiya admitted)
Ghanashyam Nayak.......Kalyanji Bhai man at toilet

==Crew==
* Producer: Mehul Kumar
* Director: Mehul Kumar
* Story: Ishraq-Suja
* Dialogues: Nana Patekar Sameer
* Music: Anand-Milind
* Choreography: Chinni Prakash, Madhav Kishen
* Editing: Yusuf Sheikh
* Costume Design: Shammim

==Awards==

===National Film Awards===
*National Film Award for Best Actor - Nana Patekar

===Filmfare Awards===
*Filmfare Best Actor Award - Nana Patekar
*Filmfare Best Supporting Actress Award - Dimple Kapadia
*Filmfare Best Story Award - Ishraq-Suja
*Filmfare Best Dialogue Award - Nana Patekar

===Star Screen Awards===
*Star Screen Award Best Actor - Nana Patekar
*Star Screen Award Best Dialogue - Ishraq-Suja
*Star Screen Award Best Story - Ishraq-Suja

== Music ==
{{Infobox album |
| Name = Krantiveer
| Type = Soundtrack
| Artist = Anand-Milind
| Cover =
| Released =   1994 (India)
| Recorded = Feature film soundtrack
| Length =
| Label = Venus Records and Tapes
| Producer = Anand-Milind
| Reviews =
| Last album = Eena Meena Deeka (1994)
| This album = Krantiveer (1994)
| Next album = Suhaag (1994 film)|Suhaag (1994)
}}

The soundtrack of the film contains 6 songs. The music is composed by Anand-Milind, with lyrics authored by Sameer (lyricist)|Sameer.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
|"Chunri Udi Sajan" Kumar Sanu, Poornima
|-
|"Jab Se Hum Tere" Kumar Sanu, Alka Yagnik
|-
| "Jai Ambe Jagdambe" Praful Dave, Sapna Awasthi, Sudesh Bhosle
|-
| "Jhankaro Jhankaro" Udit Narayan, Sapna Awasthi
|-
| "Love Rap" Sapna Mukherjee, Sudesh Bhosle, Amit Kumar, Poornima
|-
| "Phool Kali Chand" Udit Narayan, Sadhana Sargam
|}

==References==
 

==External links==
* 

 
 
 
 
 
 