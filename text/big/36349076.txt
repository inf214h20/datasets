Velvet Hands
{{Infobox film
 | name = Velvet Hands (Mani di velluto)
 | image = Mani di velluto.jpg
 | caption =
 | director = Castellano & Pipolo (Franco Castellano & Giuseppe Moccia)
 | writer = Castellano & Pipolo 
 | starring = Adriano Celentano
 | music = Nando De Luca
 | cinematography = Alfio Contini
 | editing =Antonio Siciliano
 | producer = Mario Cecchi Gori
 | distributor = Cecchi Gori Group
 | released = 1979
 | runtime = 100 min
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} 1979 Cinema Italian comedy for Best Actor.    The film also won the David di Donatello for Best Producer. 

== Plot ==

Adriano Celentano performs the role of Guido Quiller, the inventor and main patent propietor of a virtually indestructible type of armored glass which is used to secure jewelry shop windows. The success of this merchandise occurs at the chagrin of several influential insurance companies, chiefly the La Suisse Assurance in Geneva, who see their winnings from one of their most profitable income niches dwindling, and they attempt to gain the patent to the invention and then get rid of it altogether. As it so happens, Guido lives enstranged from his wife Petula, the other patent holder to the glass, who would be more accessible to the insurance companies offers. Her condition for the divorce from Guido would be that he should transfer all his exclusive rights to the production of the Quiller glass - and thereby his whole fortune - to her, whereupon she would sell them to the Swiss Assurance and gain a princely monetary compensation in return.
 subways who has a passion for horoscopes and Arsene Lupin, and her brother Momo, a bumbling counterfeit money|counterfeiter, two members of a family clan of petty criminals. Their grandfather had a short time earlier attempted to break into a jewelry shop window, only to be thwarted by the Quiller glass, and was arrested by the police. When Guido crashes into a fountain, Tilli and Momo, thinking that he is a fellow thief, take him to their home; but his involuntary bath has infected Guido with laryngitis, rendering him temporarily mute and thus unable to identify himself.

Upon waking, Guido meets Tilli and develops an attraction to the pretty thief. After leaving her home and following a minor misunderstanding with the police (who assume that Guido has been kidnapped) due to his infection, he returns home and gets himself cured. He visits Tilli again, this time in his most prestigious outfit, but before he can tell her his name, he learns that Tilli and her friends consider him a scumbag because of his invention. Guido eventually confides in his faithful butler, Benny, about his growing affection for Tilli, his issues concerning his true identity and his intention to bail Tillis grandfather out of prison, whereupon the latter suggests that Guido should become a thief himself. Using his intelligence, Guido, who publicly "borrows" his butlers name to remain anonymous, becomes a highly successful master thief and gradually wins Tillis affection.
 ring and prepares to tell her his true identity. But an unfortunate call to the police by Leo, Guidos rival for Tillis heart, exposes him very ungently, whereupon Tilli leaves him in a fury. The next day, however, Guido sets a trap for Tilli in her usual haunt and drags her into the next plane to Geneva, where - in the office of the Suisse Assurance - he demonstrates his true intentions by making her witness him surrendering his entire fortune to his ex-wife in return for the divorce. Tilli deftly steals the money from Petula, who soon notices the loss, however, and demands it back at gunpoint. But as Guido, Tilli and Benny drive off, seemingly defeated, Guido reveals to Tillis joy that he has retained the real money while surrendering to Petula the flawed dud money Momo has produced, leading to a happy ending after all.

== Cast ==

* Adriano Celentano: Ingegner Guido Quiller
* Eleonora Giorgi: Tilli
* Olga Karlatos: Petula Quiller John Sharp: Benny, the butler
* Benno Dittongo: Momo
* Gino Santercole: Leo Di Giordano
* Pippo Santonastaso: Commissioner 
* Ania Pieroni: Maggie
* Sandro Ghiani: Barman

== References ==
 

== External links ==

*  

 
 
 
 
 