Tomie vs Tomie
{{Infobox film
| name           = Tomie vs Tomie
| image          = Tomie vs Tomie poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Tomohiro Kubo
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       = Yū Abiru   Tōru Hachinohe   Emiko Matsuoka   Masaki Miura
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 86 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 Tomie film series, based on a manga Tomie| series of the same name by Junji Ito, specifically The Gathering chapter from Vol. 3.

==Plot==
The plot revolves around two children who were injected with the original Tomie’s blood, and thus grew into full-fledged Tomies themselves. This didn’t work perfectly, so they started degrading, needing more Tomie blood to sustain themselves. 

A young man with a troubled past begins work in a factory, where he attracts the amorous attention of a mysterious woman who lurks behind the scenes. He is a relatively unique young man, perhaps a genetic one-off, in that he feels no particular attraction to Tomie. This of course intrigues Tomie, and she begins to become obsessed with him. Soon, he is caught in a vicious struggle between two rival Tomies. And neither Tomie will die.

==Cast==
* Yū Abiru 
* Tōru Hachinohe
* Emiko Matsuoka 
* Masaki Miura
* Nathan Jerome

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 