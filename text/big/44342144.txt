The Little Boy Scout
{{Infobox film
| name           = The Little Boy Scout
| image          =
| caption        =
| director       = Francis J. Grandon
| producer       = Adolph Zukor
| writer         = Charles Sarver Ann Pennington
| music          = William Marshall
| editing        =
| distributor    = Paramount Pictures
| released       = June 18, 1917 reels
| country        = United States
| language       = Silent (English intertitles)
}} lost  Ann Pennington. 

==Plot==
As described in a film magazine review,  the film takes place during the American troop maneuvers along the boarder during the Mexican Revolution. Justina Howland (Pennington) lives with Miguel Alvarez (Fraunholtz), her Mexican guardian, who insists that she marry his son Luis (Burton). On the eve of the wedding Justina rebels and leaves, and at the boarder is taken in by Thomas Morton (Moore) and his company of Massachusetts soldiers. Justina goes to live with her aunt Elizabeth (Harris) and, shortly after her arrival there, the troops from the boarder return and Justina renews her acquaintance with Thomas. In order to save herself from her Mexican uncle, who has pursued her, she marries Thomas.

==Cast== Ann Pennington - Justina Howland
*Owen Moore - Thomas Morton
*Fraunie Fraunholtz - Miguel Alvarez
*Marcia Harris - Elizabeth Howland George Burton - Luis Alvarez
*Harry Lee - Sergeant Jones

==Reception== city and state film censorship boards. The Chicago Board of Censors required the cutting of a closeup showing currency in a hat. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 