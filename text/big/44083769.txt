Agni Vyooham
{{Infobox film 
| name           = Agni Vyooham
| image          =
| caption        =
| director       = P Chandrakumar
| producer       = RS Prabhu
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan Shubha Sukumaran Kanakadurga Sankaradi
| music          = A. T. Ummer
| cinematography = Anandakkuttan
| editing        = G Venkittaraman
| studio         = Sree Rajesh Films
| distributor    = Sree Rajesh Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by P Chandrakumar and produced by RS Prabhu. The film stars Shubha (actress)|Shubha, Sukumaran, Kanakadurga and Sankaradi in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Shubha
*Sukumaran
*Kanakadurga
*Sankaradi
*Sreelatha Namboothiri
*K. P. Ummer Kunchan
*Kuthiravattam Pappu

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Innathepulari || Chorus, Jolly Abraham || Sathyan Anthikkad || 
|-
| 2 || Maanathuninnum || P Jayachandran || Sathyan Anthikkad || 
|-
| 3 || Yaaminee || S Janaki || Sathyan Anthikkad || 
|-
| 4 || Yaaminee   || S Janaki || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 