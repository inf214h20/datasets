The Adventures of François Villon
 

The Adventures of François Villon are a series of four films released in 1914, directed by Charles Giblyn and featuring Murdock MacQuarrie as François Villon.  The four films are The Oubliette, The Higher Law, Monsieur Bluebeard, and Ninety Black Boxes. 

==The Oubliette==
{{Infobox film
| name           = The Oubliette
| director       = Charles Giblyn
| producer       = 
| writer         = George Bronson Howard Harry G. Stafford Pauline Bush Lon Chaney
| cinematography = Lee Bartholomew Universal Pictures
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
}}
 silent drama Lon Chaney. This film and By the Suns Rays are two of Chaneys earliest surviving films.   

===Cast of The Oubliette===
* Murdock MacQuarrie as François Villon Pauline Bush as Philippa de Annonay Lon Chaney as Chevalier Bertrand de la Payne King Louis XI
* Chester Withey as Colin
* Agnes Vernon

==The Higher Law==
{{Infobox film
| name           = The Higher Law
| image          = 
| caption        = 
| director       = Charles Giblyn
| producer       = Joe De Grasse
| writer         = George Bronson Howard Harry G. Stafford Pauline Bush
| music          = 
| cinematography = Lee Bartholomew
| editing        =  Universal Pictures
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama Pauline Bush. Lon Chaney also has a role. The film is now considered to be lost film|lost.   

===Cast of The Higher Law===
* Murdock MacQuarrie as François Villon Pauline Bush as Lady Eleyne
* Doc Crane as King Louis XI Lon Chaney as Sir Stephen
* Millard K. Wilson
* Chester Withey
* William B. Robbins

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 