Kisses for Breakfast
 
 
{{Infobox film
| name = Kisses for Breakfast
| image =
| alt =
| caption =
| director = Lewis Seiler
| producer = Harlan Thompson
| writer = Yves Mirande André Mouëzy-Éon Seymour Hicks Kenneth Gamet
| narrator =
| starring = Dennis Morgan Jane Wyatt Shirley Ross
| music = Adolph Deutsch
| cinematography = Arthur Edeson
| editing = James Gibbon
| studio =
| distributor = Warner Bros. Pictures
| released =  
| runtime = 82 minutes
| country = United States English
| budget =
| gross =
}}
 Code comedy Warner Brothers Pictures from an English stage play adaptation by Seymour Hicks (Mr. Whats His Name) of a French comic farce, Au Premier de Ces Messieurs ("To the First Husband"), written by Mirande and André Mouëzy-Éon. 

==Plot summary== blackmails him Lee Patrick), sees them drive away in Rodneys car. Rodney refuses to pay and is hit over the head by Claras accomplice, developing amnesia. The blackmailers drive the car over a cliff, where it bursts into flames, and although no body is found, Rodney is believed to be dead. Rodney, not knowing who he is, takes the name "Happy Homes" from an Federal Housing Administration|F.H.A. billboard he chances to see, finds Lauras address in his pocket, and travels to her cotton plantation in search of his true identity. Laura has no idea who he is, but Rodney talks her into hiring him to run the nearly bankrupt plantation.
 hypnotized by Juliets psychiatrist uncle and recovers his memory, thinking it is the day of his first wedding. Lucius reveals the entire story to the innocent Laura.

Refusing to admit defeat because she loves Happy, Laura hogties Juliet and locks her in the sabotaged shower. She confronts Rodney alone is his bedroom. She convinces him that she is the "Cousin Laura" that he spoke to on the phone and that a year has passed. Rodney realizes that he is married to both women. She also charms him into kissing her, and his latent feelings for her arise again. Juliet, covered in black oil, escapes the shower and finds Rodney kissing Laura, leading to a pillow fight over his affections that winds up with Juliet being tarred and feathered. Laura decides to leave for South Carolina. Rodney realizes that he loves Laura. He tricks her into bashing him over the head with an urn containing his supposed remains (buttons from his overcoat) and "becomes" Happy again. Juliet disgustedly concludes that he is a "chameleon" and gives him up.

==Cast==
* Dennis Morgan as Rodney Trask/"Happy Homes"
* Jane Wyatt as Laura Anders
* Shirley Ross as Juliet Marsden Lee Patrick as Betty Trent
* Jerome Cowan as Lucius Lorimer Una OConnor as Ellie
* Barnett Parker as Phillips
* Romaine Callender as Dr. George Burroughs
* Lucia Carroll as Clara Raymond
* Cornel Wilde as Chet Oakley
* Willie Best as Arnold
* Louise Beavers as Clotilda
* Clarence Muse as Old Jeff

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 