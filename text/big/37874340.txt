The Winds of Kitty Hawk
{{Infobox film
| name           = The Winds of Kitty Hawk
| image          =
| caption        =
| director       = E. W. Swackhamer Charles Fries International Telephone and Telegraph Corporation
| writer         = William Kelley Jeb Rosebrook
| starring       = Michael Moriarty David Huffman Charles Bernstein
| cinematography = Dennis Dalzell
| editing        = John A. Martinelli
| distributor    = Fries Distribution Company National Broadcasting Company(original TV broadcast) Fries Home Video(1989)
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
}} Amelia Earhart and The Lindbergh Kidnapping Case.
 vignettes sometimes historically rearranged. The film makes a claimer at the beginning stating that dramatic license being taken but for the most part their story is told chronologically. 

In 2012 the film became available on DVD from MGM Limitied Edition. 

==Cast==
*Michael Moriarty as Wilbur Wright
*David Huffman as Orville Wright Tom Bower as William Tate
*Robin Gammell as Harry Aubrey Toulmin, Sr.|H.A. Toulmin
*Scott Hylands as Glenn Curtiss John Randolph as Alexander Graham Bell
*Kathryn Walker as Katharine Wright Bishop Milton Wright
*John Hoyt - Professor Samuel Langley Joseph Bernard - Mayor of New York, George B. McClellan, Jr.
*Lew Brown - Harlan Mumford(banker)
*Carole Tru Foster - Agnes Osborne
*Dabbs Greer - Ace Hutchin
*Mo Malone - Elizabeth Mayfield
*Robert Casper - Man
*Tom Lawrence - Doctor
*Ross Durfee - William Howard Taft
*Charles Macaulay - 2nd Genteral
*Frank Farmer - Mr. Newell
*Steffen Zacharias - Fisherman
*Laurence Haddon - 1st General
*Ari Zeltzer - Tom Tate
*Vaughn Armstrong - Reporter
*Marie Todd - 
*Gerald Berns - Reporter

==References==
 

==External links==
* 
* 
* (Period Orchestral - 1904 flight)

 

 
 
 
 