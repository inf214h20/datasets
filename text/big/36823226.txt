The Traffickers
{{Infobox film
| name           = The Traffickers
| film name =  
 | hanja          =  들
 | rr             = Gongmojadeul
 | mr             = Kongmochadŭl}}
| image          = File:The_Traffickers_poster.jpg
| director       = Kim Hong-sun
| producer       = Choi Yeon-ju   Choi Hyeon-muk   Kim Seong-geun
| writer         = Kim Sang-myung   Kim Hong-sun
| starring       = Im Chang-jung   Choi Daniel 
| music          = Kim Jun-seong
| cinematography = Yun Nam-ju
| editing        = Shin Min-kyung
| distributor    = TimeStory/Cinus Entertainment 
| released       =  
| runtime        = 110 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =  
}}
 
The Traffickers ( ) is a 2012 South Korean crime thriller starring Im Chang-jung, Choi Daniel, Oh Dal-su and Jo Yoon-hee. It takes place in six hours on a passenger boat with an ongoing black-market organ-trafficking operation, and a desperate husband out to find his missing wife.    

==Plot==
After his best friend dies in an unexpected accident, black market dealer Young-gyu decides to wash his hands off his illegal activities and start a new life. He then falls in love with an acquaintance, Yu-Ri, who is unaware of his feelings and unknowing of his past. He later learns that Yu-ri needs money for her fathers surgery and she  has resorted to procuring the services of a local organ broker, who is Young-gyus former client. The broker promises to help her secure a legal organ donation, but secretly relies on black market dealers like Young-gyu, who engage in multiple kidnappings and killings. To help her out, Young-gyu agrees to do the job one last time when he is once again approached by his former client. His smuggling ring gets back together for a final run and they resume their M.O. of operating out of a ferry boat that runs between Korea and China, picking victims from among its passengers, abducting them from their rooms, harvesting their organs on board the ship while they are still alive yet restrained, and then brutally disposing of the bodies afterwards.  

Meanwhile, a married couple, Sang-ho and Chae-hee (who is disabled and relies on a wheelchair), boards Young-gyus ferry boat heading to Weihai, China. That very evening, just when the boat enters international sea waters where countries laws cannot be enforced, Chae-hee suddenly goes missing and all her pictures and belongings vanishes without a trace.   

Young-gyu runs into Yu-ri on the same ship, and she ironically claims to be the only witness to Chae-hees disappearance...

==Cast==
 
*Im Chang-jung - Young-gyu
*Choi Daniel - Sang-ho
*Oh Dal-su - Kyung-jae / "Old Man"
*Jo Yoon-hee - Yu-ri
*Jung Ji-yoon - Chae-hee
*Jo Dal-hwan - Joon-sik
*Lee Young-hoon - Dae-woong
*Shin Seung-hwan - Dong-bae
*Choi Il-hwa - Chul-soo
*Lee Moon-soo - Dr. Oh
*Heo Joon-seok - victim in prologue
*Kim Jae-hwa - Gong Choon-ja
*Go Jung-il - customs officer
*Yoon Sang-ho - boat crew member
*Song Yi-joo - Inspector Jang 
*Park Se-yeong - Min-seo
*Park Sung-taek - Choi 
*Park Il-mok - office head
*Son Jong-hak - company chairman
*Gong Jung-hwan - Yong-chul 
*Oh Sang-jin - police station reporter
*Ra Mi-ran - female courier 1
*Shin Hye-jeong - female courier 2
 

==Awards and nominations== 2012 Blue Dragon Film Awards
*Best New Director - Kim Hong-sun
*Nomination - Best New Actress - Jung Ji-yoon

2012 Korean Culture and Entertainment Awards  
*Top Excellence Award, Actor in a Film - Im Chang-jung

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 