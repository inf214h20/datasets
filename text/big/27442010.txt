The Secret of Nikola Tesla
{{Infobox film
| name           = The Secret of Nikola Tesla
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Krsto Papić
| producer       = 
| writer         = Ivo Brešan Ivan Kušan Krsto Papić
| starring       = Petar Božović Strother Martin Orson Welles Dennis Patrick Oja Kodar Boris Buzančić
| music          = Anđelko Klobučar
| cinematography = Ivica Rajković
| editing        = Boris Erdelji
| studio         = 
| distributor    = 
| released       = 1980
| runtime        = 115 minutes
| country        = Yugoslavia
| language       = Serbo-Croatian
| budget         = 
| gross          = 
}}
 Yugoslav biographical film which details events in the life of the discoverer Nikola Tesla (portrayed by Serbian actor Petar Božović). Tesla was born to ethnic Serb parents in 1856 Croatia (at the time, part of the Austro-Hungarian Empire). He arrived in New York in 1884, became an American citizen in 1891, made immense contributions to science and died in Manhattan at age 86 during World War II in 1943. 

This biography includes references to his amazing abilities of detailed mental visualization as well as the slowly intensifying personal habits, indulgences or eccentricities for which he became nearly as well known. The film portrays Tesla in a battle with Thomas Edison over the clear superiority of Alternating Current over Direct Current. It also depicts Teslas dream of supplying consumers all around the globe with limitless free energy.
 Katharine Johnson (1855–1924), with whom Tesla corresponded for many years, and whose husband, Robert Underwood Johnson (1853–1937) (portrayed by Croatian actor Boris Buzančić) was a poet, scholar, diplomat and Teslas longtime friend and supporter.  The other Americans in the cast are character actors Strother Martin (who died six weeks before the films September 12, 1980 English-language premiere at the Toronto Film Festival) and Dennis Patrick as Westinghouse and Edison, respectively. 

==Cast==
* Petar Božović as Nikola Tesla
* Strother Martin as George Westinghouse
* Orson Welles as J. P. Morgan
* Dennis Patrick as Thomas Edison Katharine Johnson
* Boris Buzančić as Robert Underwood Johnson
* Demeter Bitenc
* Vanja Drach

==References==
 

==External links==
* 
* 
*  at Filmski-Programi.hr  

 
 

 
 
 
 
 
 
 
 