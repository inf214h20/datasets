Design for Leaving
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
| cartoon_name = Design for Leaving
| series = Looney Tunes/Daffy Duck/Elmer Fudd
| director = Robert McKimson
| story_artist = Tedd Pierce Herman Cohen
| layout_artist = Bob Givens
| background_artist = Richard H. Thomas
| voice_actor = Mel Blanc Arthur Q. Bryan (uncredited)
| musician = Carl Stalling
| distributor = Warner Bros. Pictures
| release_date = March 27, 1954
| color_process = Technicolor
| runtime = 6:38
| movie_language = English
}} Blue Ribbon Homes of Tomorrow Exhibition at the 1933 Worlds Fair in Chicago.

==Plot==
Reprising a salesman role that Daffy previously played in Daffy Dilly (1948), The Stupor Salesman (1948) and Fool Coverage (1952), Design for Leaving opens with Daffy as a fast-talking door-to-door salesman from the Acme Future-Antic Push-Button Home of Tomorrow Household Appliance Company, Inc. Daffy visits Elmer Fudd at his house and says that Acme has authorized him to install, at no cost, a complete line of ultra-modern automatic household appliances (on a 10-day free trial). Elmer tries to speak but is repeatedly interrupted by Daffy, who grabs Elmer by the arm and escorts him to a bus to take him to the office. Despite Elmers protests, Daffy puts him on the bus, which unknown to Elmer has a sign on the back that reads "Duluth Express Non-Stop".

Later that day, Elmer returns to his house (hitching a ride in a truck from the Duluth Van and Storage Co.). Daffy greets Elmer at the front door and welcomes him to his new future-antic push button home. Elmer sees that his house is different and asks Daffy what hes done, but Daffy quickly pushes a button and a machine removes Elmers hat and coat. Daffy then guides Elmer to a massaging chair.  Elmer likes it at first, but Daffy pushes a button and Elmer receives an aggressive massage, which dazes him. The chair then automatically puts a cigar in Elmers mouth and lights it, but the smoke activates a robot fire extinguisher from another room which douses Elmer with a bucket of water. Daffy states "Its, uh, very sensitive to heat. Probably needs adjusting", then guides Elmer into the kitchen. Daffy encourages Elmer to bask in the kitchens "treasure trove of work-saving appliances" and demonstrates a new knife sharpener which ends up destroying the blade on one of Elmers knives. Undaunted, Daffy points out the garbage disposal, which is revealed to be a pig which is housed under the kitchen sink (this would technically be regarded as low-tech). Daffy then shows Elmer the "main control panel" which operates all of the new appliances. Daffy suggests what Elmer would do if the walls were dirty. Elmer simply says that he would scrub them, though Daffy pushes a button marked "Wall Cleaner" and a robotic device emerges to clean the walls but it removes Elmers wallpaper instead (humorously removing the outer clothing from a portrait painting in the process). Daffy tries to adjust the device but he adjusts it the wrong way and it starts removing the plaster ("Oh! My walls are wuined!"). Daffy quickly deactivates it, then asks Elmer if he is tired of looking at his dirty windows; Daffy then summons a machine which covers Elmers window with bricks, and says that hell "never have to looks at those dirty windows again". Elmer becomes angry, telling Daffy that "hes so angwy, hes burning up!" which again activates the fire extinguisher and Elmer is doused with another bucket of water ("I tried to warn you!"). Daffy tries to continue the demonstration, but Elmer objects, saying that something bad happens to him whenever Daffy pushes a button. So Daffy agrees to let Elmer push a button. Elmer spots one, saying in his distinct voice, "I think Ill push this wed one." Daffy stops Elmer, shouting, "No, no, no, no, no!  Not the WED one!  Dont EVER push the WED one!" Elmer pushes another button that reads "Burglar Alarm" and a mechanical dog comes out of the wall and bites him.

Daffy then takes Elmer into a bedroom and shows him a device which will automatically tie a neck tie (from the options of Bow tie|Bow, Four-in-hand, Five-in-hand, False Granny, Windsor knot|Windsor, Smindsor and an unlabled option). Daffy tries to demonstrate it but the machine puts Elmer in a noose ("Help! Get me down!"). Daffy shuts off the machine and casually refers to the noose as the "Alcatraz Ascot" as if it were a type of neck tie (the unlabled option). Elmer is exhausted, telling Daffy that he wants all of the "push-button nonsense" removed and tries to go upstairs and take an aspirin, but cannot do so because his stairway has been removed. Daffy confidently boasts that there is no need to walk up stairs in a push-button home, and uses an elevator-like device to bring the "upstairs (to the) downstairs". Elmer seems impressed but asks what happens to the downstairs, and Daffy raises the upstairs which shows that everything downstairs has been destroyed. Elmer asks if there is "any more cwever gadgets to demonstwate, Mr. Smarty Salesman?", and when Daffy says no, Elmer makes a phone call but the conversation is inaudible. When Elmer hangs up there is a knock on his front door and a large crate is brought inside. Elmer opens the crate and starts the motor, telling Daffy about his new "future-antic push-button salesman ejector" which grabs Daffy by the shoulders and wheels him out of the house, kicking him repeatedly (it was done as revenge from Elmer). With Daffy gone, Elmer remembers the red button and wonders "what that wed button is for?" He pushes it and a display reads "IN CASE OF TIDAL WAVE".  A hydraulic lift raises his house high into the air. Elmer looks out of the front door and Daffy flies by in a helicopter and delivers the final punch line, "For a small price, I can install this little blue button to get you down!".

==Historical Perspective== Kirby vacuum Fuller brushes, etc. This is contrasted with the post-World War II automation and modernization of the American home; Acmes innovations are at once prescient and outlandish, predating The Jetsons (with its robot maid and household gadgetry) by a decade.

==Availability==
"Design for Leaving" is available restored, uncensored and uncut, on the Looney Tunes Superstars-Daffy Duck:Frustrated Fowl DVD. However, it was cropped to widescreen.

==External links==
*  

 
 
 
 
 
 
 
 
 