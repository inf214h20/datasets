Death Nurse
{{Infobox film
| name           = Death Nurse
| image          = File:DeathNurseDVD.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Slasher // Video
| film name      = 
| director       = Nick Millard
| producer       = Irmgard Millard
| writer         = Nick Millard
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Nick Millard   Priscilla Alden   Albert Eskinazi   Irmgard Millard   Frances Millard
| music          = 
| cinematography = 
| editing        = 
| studio         = I.R.M.I. Films Corporation
| distributor    = Slasher // Video
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Death Nurse is a 1987 horror film written and directed by Nick Millard. It was followed by a 1988 sequel entitled Death Nurse 2.

== Plot ==
 indigents sent con artists who murder their patients (usually during "surgeries" performed on them by Gordon) and continue billing the state for their care afterward. The only permanent resident of the clinic is the alcoholic Louise Kagel, who Gordon supplies with liquor in exchange for sex. One day, a new social services worker named Faith Chandler drops off John Davis, a man afflicted with tuberculosis. Edith smothers John, and Gordon buries his body, though he is later forced to dig it up and crudely puppeteer it to create the illusion that Davis is still alive when Faith asks to check in on him after bringing Charles Bedowski, who has a heart condition, to Shady Palms.

After Faiths visit, Edith and Gordon kill Bedowski while attempting to replace his heart with a dead dogs; during the procedure, the dogs heart is grabbed by the Mortleys pet cat, forcing the duo to humorously chase the feline, and deem the transplant a failure. Bedowskis remains are buried by Gordon, and Edith feeds pieces of him to the rats that live in the garage. The infestation of vermin does not go unnoticed by the authorities, and when Mr. Smith, an environmental health officer, threatens to shut the clinic down, Edith stabs him to death.

Faith herself checks into Shady Palms, and grows suspicious of the facility, which causes Edith to knife her, after feeding the woman rats she had cooked her for lunch. Louise witnesses Faiths murder, so Edith kills her with a syringe, despite Gordons fondness for her. Edith has Gordon place the bodies of Smith, Faith, and Louise in the garage, from which their smell attracts the attention of a police lieutenant who had stopped by to visit Charles Bedowski. Observing from a window as the lieutenant opens the garage and discovers what is inside it, a dejected Edith sits on a sofa with Gordon, and the film ends.

== Cast ==

* Priscilla Alden as Nurse Edith Mortley
* Albert Eskinazi as Doctor Gordon Mortley
* Royal Farros as Mr. Powell/Charles Bedowski
* Frances Millard as Faith Chandler
* Irmgard Millard as Louise Kagel
* Nick Millard as John Davis
* Fred Sarra as Lieutenant Cal Bedowski

== DVD release ==

Slasher // Video released Death Nurse and its sequel on DVD in 2012. Limited to 1000 copies, the DVD includes features such as a Q&A with Nick Millard, and a commentary provided by him, Irmgard Millard, and Slasher // Video founder Jesus Terán.  

== Reception ==
 slashers go, this one ranks down there with the cheapest of the cheap, the worst of the worse. As such, its easy to love if youre in the right frame of mind for it. Youd be foolish to take any of it seriously, its obvious that those making the picture werent".  Slasherpool gave Death Nurse a 2 out of 5, finding some entertainment in Priscilla Aldens performance, the death scenes, and the films ineptness and absurdity, concluding "The premise isnt a bad one... in fact, in more capable hands, this would make a pretty good black comedy... but you get what you get". 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 