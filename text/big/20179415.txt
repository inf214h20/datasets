Halloween (1978 film)
 
{{Infobox film
| name           = Halloween
| image          = Halloween cover.jpg
| caption        = Theatrical release poster
| director       = John Carpenter
| producer       = Debra Hill
| screenplay     = {{plainlist|
* John Carpenter
* Debra Hill
}}
| starring       = {{plainlist|
* Donald Pleasence
* Jamie Lee Curtis
* P. J. Soles
* Nancy Loomis
 
}}
| music          = John Carpenter
| cinematography = Dean Cundey
| editing        = {{plainlist|
* Tommy Wallace
* Charles Bornstein
}}
| studio         = Falcon International Productions
| distributor    = Compass International Pictures
| released       =  
| runtime        = 91 minutes 
| country        = United States
| language       = English
| budget         = $300,000–$325,000       
| gross          = $70 million 
}} independent Slasher slasher horror scored by Midwestern town Michael Myers murders his older sister by stabbing her with a kitchen knife. Fifteen years later, he escapes from a psychiatric hospital, returns home, and stalks teenager Laurie Strode and her friends. Michaels psychiatrist Samuel Loomis|Dr. Sam Loomis suspects Michaels intentions, and follows him to Haddonfield to try to prevent him from killing.

Halloween was produced on a budget of $300,000 and grossed $47 million at the box office in the United States,  and $70 million worldwide,  equivalent to $250 million as of 2014, becoming one of the most profitable independent films.  Many critics credit the film as the first in a long line of slasher films inspired by Alfred Hitchcocks Psycho (1960 film)|Psycho (1960). Halloween had many imitators and originated several clichés found in low-budget horror films of the 1980s and 1990s. Unlike many of its imitators, Halloween contains little graphic violence and gore.     In 2006, it was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 
 Halloween II was released in 1981, three years after its predecessor.

==Plot== Michael Myers (Will Sandin) murders his older sister Judith Myers (Sandy Johnson) by stabbing her with a large kitchen knife.  Fifteen years later, on October 30, 1978, Michael escapes Warren County Smiths Grove Sanitarium, where he had been committed since the murder, stealing the car that was to take him to a court hearing, the intention of which was for him to never be released.
 Nancy Loomis) and Lynda van der Klok (P. J. Soles), that she believes someone is following her but they dismiss her concerns. Later at her house, Laurie becomes startled to see Michael outside in the yard staring into her room. Elsewhere, Michaels psychiatrist, Dr. Sam Loomis (Donald Pleasence), having anticipated Michaels return home, goes to the local cemetery only to discover that Judith Myers headstone is missing. Later, Loomis meets with Sheriff Leigh Brackett (Charles Cyphers), and the two search for Michael.
 Brian Andrews), while Annie babysits Lindsay Wallace (Kyle Richards) across the street from the Doyle house. When Annie gets a call from her boyfriend Paul asking her to pick him up, she drops Lindsay off at the Doyle house. Annie gets in her car to pick up Paul but Michael, who was hiding in the backseat of her car, strangles her before slitting her throat, killing her. At the Doyle house, while he plays hide-and-seek with Lindsay, Tommy spots Michael carrying Annies corpse and tries to tell Laurie, who doesnt believe in any "bogeyman|boogeyman" that Tommy says he saw. Later that evening, Lynda and her boyfriend Bob enter the Wallace house and have sex in the upstairs bedroom. While downstairs to get a beer for Lynda, Bob is attacked by Michael, who kills him by pinning him to the wall with his knife. Michael then appears in the bedroom doorway, pretending to be Bob in a ghost costume. Gaining no response from him, Lynda becomes annoyed and calls Laurie, just as Michael kills her by strangling her with the telephone cord.

Feeling unsettled, Laurie puts Tommy and Lindsay to bed and goes to the Wallace house, where she discovers the corpses of Annie, Bob, and Lynda. She is suddenly attacked by Michael. She falls down the staircase, but she quickly recovers. Fleeing the house, she screams for help, but to no avail. Running back to the Doyle house, she realizes she lost the keys and the door is locked, as she sees Michael approaching in the distance. Laurie panics and screams for Tommy to wake up and open the door quickly. Luckily, Tommy opens the door in time and lets Laurie inside. Laurie instructs Tommy and Lindsay to hide and then realizes the phone line is dead and that Michael has gotten into the house through a window. As she sits down in horror next to the couch, Michael appears and tries to stab her, but she stabs him in the side of his neck with a knitting needle.

Laurie goes upstairs telling Tommy and Lindsay she killed the "boogeyman", but Michael reappears in pursuit of her. Telling the kids to hide and lock themselves in the bathroom, Laurie opens the French windows to feign escape and hides in a bedroom closet. Michael punches a hole in the closet door to get to her. However, Laurie frantically undoes a metal clothes hanger to stick Michael in the eye, and she then stabs Michael with his own knife. Michael collapses and Laurie exits the closet, then tells the children to go find help. Dr. Loomis sees Tommy and Lindsay running away from the house screaming and suspects Michael could be inside. Back inside, Michael gets up and tries to strangle Laurie, but Loomis arrives in time to save her. Loomis shoots Michael in the chest at point-blank range, who then falls from the second-story patio onto the lawn below. Laurie asks Loomis if that was the "boogeyman", to which Loomis confirms. However, when Loomis looks over the balcony, he finds Michaels body is missing.

== Cast ==
* Donald Pleasence as Samuel Loomis|Dr. Loomis
* Jamie Lee Curtis as Laurie Strode
* Charles Cyphers as Sheriff Leigh Brackett 
* Nancy Kyes as Annie Brackett (credited as Nancy Loomis)
* P. J. Soles as Lynda Van Der Klok Michael Myers)
* Kyle Richards as Lindsey Wallace Brian Andrews as Tommy Doyle
* John Michael Graham as Bob Simms
* Will Sandin as Michael Myers (age 6) Judith Margaret Myers

==Production== The Exorcist."  Carpenter and his then-girlfriend Debra Hill began drafting a story originally titled The Babysitter Murders, but, as Carpenter told Entertainment Weekly, Yablans suggested setting the movie on Halloween night and naming it Halloween instead. Carpenter, Entertainment Weekly interview, quoted at  ; last accessed April 19, 2006. 

Akkad agreed to put up $300,000 for the films budget, which was considered low at the time;  (Carpenters previous film, Assault on Precinct 13, had an estimated budget of $100,000). Akkad worried over the tight, four-week schedule, low budget, and Carpenters limited experience as a filmmaker, but told Fangoria, "Two things made me decide. One, Carpenter told me the story verbally and in a suspenseful way, almost frame for frame. Second, he told me he didnt want to take any fees, and that showed he had confidence in the project". Carpenter received $10,000 for directing, writing, and composing the music, retaining rights to 10 percent of the films profits. 

Because of the low budget, wardrobe and props were often crafted from items on hand or that could be purchased inexpensively. Carpenter hired   for around a hundred dollars. 

The limited budget also dictated the filming location and time schedule. Halloween was filmed in 20 days in the spring of 1978 in South Pasadena, California and the cemetery at Sierra Madre, California. An abandoned house owned by a church stood in as the Myers house. Two homes on Orange Grove Avenue (near Sunset Boulevard) in Hollywood were used for the films climax.    The crew had difficulty finding pumpkins in the spring, and artificial fall leaves had to be reused for multiple scenes. Local families dressed their children in Halloween costumes for trick-or-treat scenes. 

In August 2006, Fangoria reported that Synapse Films had discovered boxes of negatives containing footage cut from the film. One was labeled "1981" suggesting that it was additional footage for the television version of the film. Synapse owner Don May, Jr. said, "What weve got is pretty much all the unused original camera negative from Carpenters original Halloween. Luckily, Billy   was able to find this material before it was destroyed. The story on how we got the negative is a long one, but well save it for when were able to showcase the materials in some way. Kirkus should be commended for pretty much saving the Holy Grail of horror films."  It was later reported, "We just learned from Sean Clark, long time Halloween genius, that the footage found is just that: footage. There is no sound in any of the reels so far, since none of it was used in the final edit." 

===Writing=== Celtic traditions of Halloween such as the festival of Samhain. Although Samhain is not mentioned in the plot of the first film, Hill asserts that:

 
 Leigh Brackett shared the name of a film screenwriter. 

===Casting===

 
  You Only Live Twice (1967). 
 Anne Lockhart, the daughter of June Lockhart from Lassie (1954 TV series)|Lassie, as Laurie Strode. However, Lockhart had commitments to several other film and television projects. Carpenter interview.  Hill says of learning that Jamie Lee was the daughter of Psycho actress Janet Leigh, "I knew casting Jamie Lee would be great publicity for the film because her mother was in Psycho."  Halloween was Curtis feature film debut and launched her career as a "scream queen" horror star. Another relatively unknown actress, Nancy Kyes (credited in the film as Nancy Loomis) was cast as Lauries friend Annie Brackett, daughter of Haddonfield sheriff Leigh Brackett (Charles Cyphers). Kyes had previously starred in Assault on Precinct 13 (as had Cyphers) and happened to be dating Halloweens art director Tommy Lee Wallace when filming began.  Carpenter chose P. J. Soles to play Lynda Van Der Klok, another friend of Lauries, best remembered in the film for dialogue peppered with the word "totally." Soles was an actress known for her supporting role in Carrie (1976 film)|Carrie (1976) and her minor part in The Boy in the Plastic Bubble (1976). According to one source, "Carpenter realized she had captured the aura of a happy go lucky teenage girl in the 70s." 
 Dennis the Menace (1993), and Major Payne (1995). 

===Direction===
Historian Nicholas Rogers notes that film critics contend that Carpenters direction and camera work made Halloween a "resounding success".  Roger Ebert remarks, "Its easy to create violence on the screen, but its hard to do it well. Carpenter is uncannily skilled, for example, at the use of foregrounds in his compositions, and everyone who likes thrillers knows that foregrounds are crucial ...." Roger Ebert, review of Halloween, Chicago Sun-Times, October 31, 1979, at  ; last accessed April 19, 2006. 
  Black Christmas, Halloween made use of seeing events through the killers eyes.
 

The first scene of the young Michaels voyeurism is followed by the murder of Judith seen through the eye holes of Michaels clown costume mask. According to one commentator, Carpenters "frequent use of the unmounted first-person camera to represent the killers point of view ... invited   to adopt the murderers assaultive gaze and to hear his heavy breathing and plodding footsteps as he stalked his prey".  Another technique that Carpenter adapted from Alfred Hitchcocks Psycho (1960 film)|Psycho (1960) was suspense with minimal blood and gore. Hill comments, "We didnt want it to be gory. We wanted it to be like a jack-in-the box."  Film analysts refer to this as the "false startle" or "the old tap-on-the-shoulder routine" in which the stalkers, murderers, or monsters "lunge into our field of vision or creep up on a person."  Carpenter worked with the cast to create the desired effect of terror and suspense. According to Curtis, Carpenter created a "fear meter" because the film was shot out-of-sequence and she was not sure what her characters level of terror should be in certain scenes. "Heres about a 7, heres about a 6, and the scene were going to shoot tonight is about a 9½", remembered Curtis. She had different facial expressions and scream volumes for each level on the meter. 

Carpenters direction for Castle in his role as Myers was minimal. For example, when Castle asked what Myers motivation was for a particular scene, Carpenter replied that his motivation was to walk from one set marker to another.  The documentary titled Halloween Un-masked, featured in the 22nd anniversary DVD of Halloween, John Carpenter states he also instructed Castle to tilt his head a couple of times as if he was observing the corpse, particularly in the scene when Myers impaled one of his victims against a wall. 

===Music=== meter composed and performed by director John Carpenter. Critic James Berardinelli calls the score "relatively simple and unsophisticated", but admits that "Halloweens music is one of its strongest assets".  Carpenter stated in an interview, "I can play just about any keyboard, but I cant read or write a note."  In the end credits, Carpenter bills himself as the "Bowling Green Philharmonic Orchestra" for performing the films score, but he did receive assistance from composer Dan Wyman, a music professor at San José State University.  

Some songs can be heard in the film, one being an untitled song performed by Carpenter and a group of his friends who formed a band called The Coupe DeVilles. The song is heard as Laurie steps into Annies car on her way to babysit Tommy Doyle.  Another song, "(Dont Fear) The Reaper" by classic rock band Blue Öyster Cult, appears in the film. 

The soundtrack was first released in the United States in October 1983, by Varese Sarabande. It was subsequently released on Compact Disc in 1985, re-released in 1990, and again in 2000.

{{Track listing
| collapsed = yes
| headline = Original track listing
| title1 = Halloween Theme Main Title
| length1 = 2:54
| title2 = Lauries Theme
| length2 = 2:05
| title3 = Shape Escapes
| length3 = 1:42
| title4 = Myers House
| length4 = 5:35
| title5 = Michael Kills Judith
| length5 = 3:11
| title6 = Loomis and Shapes Car
| length6 = 3:32
| title7 = The Haunted House
| length7 = 3:33
| title8 = The Shape Lurks
| length8 = 1:35
| title9 = Laurie Knows
| length9 = 3:01
| title10 = Better Check the Kids
| length10 = 3:27
| title11 = The Shape Stalks
| length11 = 3:08
}}

{{Track listing
| collapsed = yes
| headline = 20th Anniversary Edition track listing
| title1 = Halloween Theme
| length1 = 2:21
| title2 = Halloween 1963
| length2 = 3:11
| title3 = The Evil Is Gone!
| length3 = 4:08
| title4 = Halloween 1978
| length4 = 2:50
| title5 = The Boogeyman Is Coming
| length5 = 0:40
| title6 = The Shape
| length6 = 1:43
| title7 = The Hedge
| length7 = 1:35
| title8 = He Came Home
| length8 = 2:40
| title9 = Trick or Treat
| length9 = 0:39
| title10 = The Haunted House
| length10 = 1:43
| title11 = The Devils Eyes
| length11 = 1:39
| title12 = The Boogeyman Is Outside
| length12 = 1:27
| title13 = Damn You for Letting Him Go!
| length13 = 1:34
| title14 = Empty Street
| length14 = 0:33
| title15 = See Anything You Like?
| length15 = 2:22
| title16 = Lock the Door
| length16 = 2:53
| title17 = Hes Here?
| length17 = 0:55
| title18 = Lights Out
| length18 = 2:49
| title19 = Cut It Out
| length19 = 1:19
| title20 = Tombstone
| length20 = 1:19
| title21 = The Shape Stalks Laurie
| length21 = 1:35
| title22 = Turn Around
| length22 = 0:33
| title23 = Unlock the Door
| length23 = 2:53
| title24 = The Hanger
| length24 = 3:04
| title25 = Call the Police
| length25 = 0:28
| title26 = Last Assault
| length26 = 1:34
| title27 = Was That the Boogeyman?
| length27 = 0:32
| title28 = End Credits/Halloween Theme (Reprise)
| length28 = 3:36
}}

==Release==
 , Nov. 6, 1978: only known, published window for date of films New York City premiere ("Held over...2nd week") ]]

===Theatrical run===
Halloween premiered on October 25, 1978 in Kansas City, Missouri (at the AMC Midland/Empire) and sometime afterward in Chicago, Illinois, and in New York City. It had its Los Angeles debut October 27, 1978.  Distribution at  ; last accessed April 19, 2006.  It opened in Pittsburgh, Pennsylvania, on November 22, 1978.  The film grossed $47 million in the United States  and an additional $23 million internationally, making the theatrical total $70 million. 

On September 7, 2012, the official Halloween Movies Facebook page announced that the original Halloween would be theatrically Reissue#Films|re-released starting October 25, 2013 in celebration of the films 35th anniversary in 2013. A new documentary was screened before the film at all locations, entitled, You Cant Kill the Boogeyman: 35 Years of Halloween, written and directed by HalloweenMovies.com webmaster Justin Beahm.    

===Television rights===
 
In 1980, the television rights to Halloween were sold to the   and   in 2008.

===Home video releases===
Since Halloween s premiere, it has been released in several   as well, marking the films first ever Blu-ray release.  The Blu-ray features a commentary track by Carpenter, Hill and Curtis, the trailer, TV spots, radio spots, fast film facts and the documentary Halloween: A Cut Above the Rest.  In 2008, a "30th Anniversary commemorative set" was released, containing the film on DVD and Blu-ray along with the "extended edition", disc one of the 25 Years of Terror documentary DVD and the sequels:   and  , including a replica Michael Myers mask. The DVD release was THX certified. The film has made $18,500,000 in home video rentals.
 Saturn Award for Best Classic Film Release. 

==Reception==

===Critical reception===
Although Halloween performed well with little advertising—relying mostly on word-of-mouth—many critics seemed uninterested or dismissive of the film.   and to the  , November 6, 1978, pp. 67, 70. While the review gives no New York City premiere date or specific theater, a display advertisement on page 72 reads: "Held over! 2nd week of horror! At a Flagship Theatre near you". Per the movie listings on pages 82, 84 and 85, respectively, it played at four since-defunct theaters: the Essex, located at 375 Grand Street in Chinatown, per  ; the RKO 86th Street Twin, on East 86th Street near Lexington Avenue; the Rivoli, located at 1620 Broadway, in the Times Square area, per  ; and the Times Square Theater, located at 217 West 42nd Street, per     , reprinted at Criterion.com, "The Criterion Collection, Online Cinematheque"  The following month,  Voice lead critic Andrew Sarris wrote a follow-up feature on cult films, citing Allens appraisal of Halloween and saying in the lead sentence that the film "bids fair to become the cult discovery of 1978. Audiences have been heard screaming at its horrifying climaxes".  Renowned American critic Roger Ebert gave the film similar praise in his 1979 review in the Chicago Sun-Times, and selected it as one of his top ten films of 1978.    Once-dismissive critics were impressed by Carpenters choice of camera angles and simple music, and surprised by the lack of blood, gore, and graphic violence.  Review aggregate website Rotten Tomatoes reports 94% of critics gave the film positive write-ups based on 50 reviews, with a rating of 8.5 out of 10 with the general consensus reading "Scary, suspenseful, and viscerally thrilling, Halloween set the standard for modern horror films." Halloween at  ; last accessed May 19, 2008. 
 Friday the 13th (1980)). Claiming it encouraged audience identification with the killer, Martin and Porter pointed to the way "the camera moves in on the screaming, pleading, victim, looks down at the knife, and then plunges it into chest, ear, or eyeball. Now thats sick." 

More than 35 years after its debut, Halloween enjoys a reputation as a classic  and is considered by many as one of the best films of 1978.     

===Themes and analysis=== postmodern academia. Some feminist critics, according to historian Nicholas Rogers, "have seen the slasher movies since Halloween as debasing women in as decisive a manner as hard-core pornography."  Critics such as John Kenneth Muir state that female characters such as Laurie Strode survive not because of "any good planning" or their own resourcefulness, but sheer luck. Although she manages to repel the killer several times, in the end, Strode is rescued in Halloween and Halloween II only when Dr. Loomis arrives to shoot Myers. 

On the other hand, other feminist scholars such as Carol J. Clover argue that despite the violence against women, slasher films turned women into heroines. In many pre-Halloween horror films, women are depicted as helpless victims and are not safe until they are rescued by a strong masculine hero. Despite the fact that Loomis saves Strode, Clover asserts that Halloween initiates the role of the "final girl" who ultimately triumphs in the end. Strode herself fought back against Myers and severely wounds him. Had Myers been a normal man, Strodes attacks would have killed him; even Loomis, the male hero of the story, who shoots Michael repeatedly at near point blank range with a large caliber handgun, cannot kill him. 

Aviva Briefel argued that moments such as when Michael loses his mask are meant to give pleasure to the male viewer. Briefel further argues that these moments are masochistic in nature and give pleasure to men because they are willingly submitting themselves to the women of the film; they submit themselves temporarily because it will make their return to authority even more powerful.  Critics, such as Pat Gill, see Halloween as a critique of American social values. She remarks that parental figures are almost entirely absent throughout the film, noting that when Laurie is attacked by Michael while babysitting, "No parents, either of the teenagers or of the children left in their charge, call to check on their children or arrive to keen over them." Pat Gill, "The Monstrous Years: Teens, Slasher Films, and the Family", Journal of Film and Video, 54:4, (Winter 2002), 22. http://www.jstor.org/stable/20688391. 

Another major theme found in the film is the dangers of pre-marital sex. Clover believes that killers in slasher films are fueled by a "psychosexual fury"  and that all the killings are sexual in nature. She reinforces this idea by saying that "guns have no place in slasher films" and when examining the film I Spit on Your Grave she notes that "a hands-on killing answers a hands-on rape in a way that a shooting, even a shooting preceded by a humiliation, does not."  Equating sex with violence is important in Halloween and the slasher genre according to Pat Gill, who made a note of this in her essay "The Monstrous Years: Teens, Slasher Films, and the Family". She remarks that Lauries friends "think of their babysitting jobs as opportunities to share drinks and beds with their boyfriends. One by one they are killed... by Michael Myers an asylum escapee who years ago at the age of six murdered his sister for preferring sex to taking care of him." 

The danger of suburbia is another major theme that runs throughout the movie and the slasher genre itself, Pat Gill states that slasher films "seem to mock white flight to gated communities, in particular the attempts of parents to shield their children from the dangerous influences represented by the city". Pat Gill, "The Monstrous Years: Teens, Slasher Films, and the Family", Journal of Film and Video, 54:4, (Winter 2002), 16.  http://www.jstor.org/stable/20688391.  Halloween and slasher films, generally, are supposed to represent the underside of suburbia. Michael Myers was raised in a suburban household and after he escapes the mental hospital he returns to his hometown to kill again; Myers is a product of the suburban environment. 

Carpenter himself dismisses the notion that Halloween is a morality play, regarding it as merely a horror movie. According to Carpenter, critics "completely missed the point there". He explains, "The one girl who is the most sexually uptight just keeps stabbing this guy with a long knife. Shes the most sexually frustrated. Shes the one thats killed him. Not because shes a virgin but because all that sexually repressed energy starts coming out. She uses all those phallic symbols on the guy."  

===Awards=== The Wicker 100 Years...100 Thrills.  The film was #14 on Bravo (US TV channel)|Bravos The 100 Scariest Movie Moments (2004).  Similarly, the Chicago Film Critics Association named it the 3rd scariest film ever made.  In 2006, Halloween was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".  In 2007, the AOL 31 Days of Horror countdown named Halloween the greatest horror movie.  In 2008, the film was selected by Empire (film magazine)|Empire magazine as one of The 500 Greatest Movies of All Time.  In 2010, Total Film selected the film as one of The 100 Greatest Movies of All Time. 

American Film Institute lists
* AFIs 100 Years...100 Thrills – #68
* AFIs 100 Years...100 Heroes & Villains:
** Michael Myers – Nominated Villain
* AFIs 100 Years...100 Movies (10th Anniversary Edition) – Nominated

==Influence== A Nightmare on Elm Street, would follow.

The major themes present in Halloween would also become common in the slasher films it inspired. Film scholar Pat Gill notes that in Halloween, there is a theme of absentee parents  but films such as A Nightmare on Elm Street and Friday the 13th feature the parents becoming directly responsible for the creation of the killer. 
 Black Christmas which contained prominent elements of the slasher genre; both involving a group of teenagers being murdered by a stranger as well having the final girl trope. Halloween, however, is seen by historians as being responsible for the new wave of horror films because it not only used these tropes but also pioneered many others.  

The 1981 horror film spoof  , no alcohol or illicit drugs, and never say "Ill be right back". 
 of the same name, written by Curtis Richards, was published by Bantam Books in 1979. It was reissued in 1982; it later went out of print. The novelization adds aspects not featured in the film, such as the origins of the curse of Samhain and Michael Myers life in Smiths Grove Sanitarium, which contradict its source material. For example, the novels version of Michael speaks during his time at the sanitarium;  in the film, Dr. Loomis states, "He hasnt spoken a word in fifteen years." In 1983, Halloween was adapted as a video game for the Atari 2600 by Wizard Video. None of the main characters in the game were named. Players take on the role of a teenage babysitter who tries to save as many children from an unnamed, knife-wielding killer as possible. The game was not popular with parents or players and the graphics were simple, as was typical in Atari 2600 games. In another effort to save money, most versions of the game did not even have a label on the cartridge. It was simply a piece of tape with "Halloween" written in marker. The game contained more gore than the film, however. When the babysitter is killed, her head disappears and is replaced by blood pulsating from the neck. The games primary similarity to the film is the theme music that plays when the killer appears onscreen.  

==Sequels and remake==
 
Halloween spawned seven   (1982), the plot of which is unrelated to the other films in the series due to the absence of Michael Myers.  He, along with Alan Howarth, also composed the music for the second and third films. After the negative critical and commercial reception for Season of the Witch, the filmmakers brought back Michael Myers in   (1988) which was successful enough to warrant its own direct-sequel in 1989 with  . 

The sequels feature more explicit violence and gore, and are generally dismissed by mainstream film critics, although   (2002), boasted a budget of $15 million.  Financier Moustapha Akkad continued to work closely with the Halloween franchise, acting as executive producer of every sequel until his death in the 2005 Amman bombings. 
 remake of its sequel.

==References==
 

;Bibliography
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 