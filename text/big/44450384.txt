The Visitant
{{Infobox film
| name = The Visitant
| image =
| alt = 
| caption = 
| director = Nicholas Peterson
| writer = Nicholas Peterson
| starring = Amy Smart Doug Jones
| music =
| cinematography = Chris Saul
| editing = CJ Miller
| distributor =
| released = 
| runtime = 7 minutes
| country = United States
| language = English
}}
The Visitant is a short film written and directed by Nicholas Peterson, starring Amy Smart and Doug Jones. Produced by Jon Heder, Nicholas Peterson, Alec Eskander, Jason Speer, Jim Peterson, Sean Michael Smith and many others, most of the films budget came from a successful kickstarter campaign that lead to over a dozen executive producers, co-producers, associate producers, and producers. 

==Plot==
A mother (Amy Smart) protects her children from a demon (Doug Jones) inside their home.

==Video game==
The origin story of The Visitant is told in the form of a video game of the same name. Part of the kickstarter campaign went to funding the game that would tie the two stories together. 

==References==
 

==External links==
*  

 
 
 
 
 
 