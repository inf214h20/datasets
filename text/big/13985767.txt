Tico-Tico no Fubá (film)
{{Infobox film
| name           = Tico-Tico no Fubá
| image          = Tico-Tico no Fubá52.jpg
| image_size     =
| caption        =
| director       = Adolfo Celi
| producer       = Adolfo Celi, Fernando De Barros
| writer         = Jacques Maret, Oswaldo Sampaio
| narrator       =
| starring       =
| music          = Radamés Gnattali
| cinematography = José María Beltrán, H.E. Fowle
| editing        = Edith Hafenrichter, Oswald Hafenrichter
| distributor    =  Vera Cruz Studios, Columbia Pictures
| release        = 21 April 1952
| runtime        = 109 minutes
| country        = Brazil Portuguese
| budget         =
}} 1952 Brazilian drama film directed by Adolfo Celi and starring Anselmo Duarte. It was entered into the 1952 Cannes Film Festival.   

The film is a fictionalized biography of Brazilian composer Zequinha de Abreu (1880–1935), who penned the song "Tico-Tico no Fubá" that became an international hit in the 1940s.

==Cast==
*Anselmo Duarte as Zequinha de Abreu
*Tônia Carrero as Branca
*Marisa Prado as  Durvalina
*Marina Freire as Amália
*Zbigniew Ziembinski as  Circus Master
*Modesto De Souza as  Luís
*Haydée Moraes Aguiar
*Luiz Augusto Arantes
*Tito Livio Baccarin
*Lima Barreto as  Inácio
*Xandó Batista as  Vendedor de rádio

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 