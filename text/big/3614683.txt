The Land Before Time VI: The Secret of Saurus Rock
{{Infobox film
| name         = The Land Before Time VI:  The Secret of Saurus Rock
| image        = LBT_SSR.JPG
| director     = Charles Grosvenor
| producer     = Charles Grosvenor Melinda Rediger Rocky Solotoff
| writer       = Libby Hinson John Loy
| italic title = force
| narrator     = Kenneth Mars Thomas Dekker Aria Noelle Nancy Cartwright Sandy Fox Kris Kristofferson John Ingle Kenneth Mars Miriam Flynn Jeff Bennett
| music        = Michael Tavera James Horner (music from The Land Before Time)
| studio       = Universal Cartoon Studios
| distributor  = Universal Studios Home Video
| released     =  
| country      = United States
| runtime      = 77 min.
| language     = English
| budget       =
}}
 animated adventure adventure musical The Land Thomas Dekker is both the singing and speaking voice of Littlefoot.

==Plot==
Littlefoots grandfather one night tells the children a story about "The Lone Dinosaur" (a pun on The Lone Ranger), a legendary longneck who once protected the Great Valley from the most ferocious sharptooth ever to live. A fight ensued, which led to the Sharptooths death. However, the sharptooth left "The Lone Dinosaur" with a scar slashed across his right eye. Soon after the battle, a huge monolith that resembled a proud sauropod, having life-sized Sharptooth teeth arranged around his neck, came out of the ground during an earthquake|earthshake. The dinosaurs called it "Saurus Rock". The legend also states that if anyone damages the monolith, bad luck would descend upon the valley.
 Whiptail rescues him. This longneck introduces himself only as "Doc" and gives no knowledge of his history. Littlefoot is intrigued by this newcomer, who is scarred across one eye and displays prior knowledge of the Great Valleys topography.

For the preceding reasons, Littlefoot assumes that Doc is the Lone Dinosaur. He tells his friends this, narrating an apparently extemporaneous legend to support his assumption. Inspired, List of The Land Before Time characters#Cera|Ceras infant nieces, the twins Dinah and Dana, go to Saurus Rock without anyone noticing. Later when the friends are playing, they notice that Dinah and Dana are missing. Recalling their talk of the day before, they go to Saurus Rock to find them.

When they finally reach Saurus Rock, they see Dinah and Dana on the top. As they climb up to rescue them, Dinah and Dana fall off the top and land on Cera, causing the life-sized stone tooth on which she is standing to break off.

As they walk home, an Allosaurus|odd-looking Sharptooth chases them. As they are being pursued, they cross a gorge via a suspended log, and as the Sharptooth follows them, he throws the log over the ravine. They start to get out, with no sign of the Sharptooth yet, until Spike gets stuck in a gap in the log. When he gets out, the Sharptooth breaks the log and falls into the ravine below.

When they get home, Cera is confronted by her father, who scolds her (angry and disappointed) for losing the twins. Over the next few days, ill fortunes plague the valley. First Ducky and Spike are attacked and stung by wasps. Then the watering hole drys up. Then a tornado hits the valley. The adults blame Doc, in whose wake the misfortunes have apparently come, while Littlefoot blames himself and his friends, recalling the breaking of Saurus Rock.
 another sharptooth, resembling the one from the story, and it almost has him.

Just as the new Sharptooth is near to him, he is rescued by his grandfather, who tells him to run as he finds his friends who lead his grandfather there. The new Sharptooth is then joined by the original, who break through the boulder and are working together to take down his grandfather. One of the Sharptooths leg is suddenly grabbed and pulled down by Docs tail, who helps his grandfather. The two Sharpteeth try to make a clear shot together, but hit the rock tower instead. The two longnecks then combine their efforts and crush the carnivores to death by tearing down the rock tower. The children cheer as the sharpteeth have been defeated and have a tooth (from the Allosaurus) for Saurus Rock.

Littlefoot and his friends then complete Littlefoots self-assigned mission. Doc departs, remarking as he goes that Littlefoot already has a hero on whom to depend, referring to Littlefoots grandfather, as Littlefoot glances back at his grandfather carrying his friends to the neck of Saurus Rock. He then glances back at Doc, who is already almost out of sight, and then runs back to his grandfather. He asks his grandfather how the new tooth looked, His grandfather replies positively and Littlefoot wonders if the bad luck will finally be over. His grandfather starts to remind him, and Littlefoot finishes it for him, that there is no such thing as bad luck, but that there is no harm in making sure. Littlefoot and Cera later build a legend of their own based on this new paradigm, portraying Grandpa Longneck as a savior.

==Cast== Thomas Dekker &ndash; Littlefoot Cera
*Aria Ducky
*Jeff Spike  Dana
*Sandy Dinah
*Kris Doc
*Kenneth Grandpa Longneck Grandma Longneck
*John Ingle &ndash; List of The Land Before Time characters#Topsy|Ceras father
*Danny Mann &ndash; Allosaurus

==Songs==
The songs are written by Michele Brourman and Amanda McBroom. Thomas Dekker, Anndi McAfee, Aria Curzon and Jeff Bennett) Thomas Dekker, Anndi McAfee, Aria Curzon and Jeff Bennett) Thomas Dekker)

==Soundtrack==
"If We Hold On Together" (instrumental)

==Home video release history==
*December 1, 1998 (VHS and laserdisc - Universal Family Features)
*December 4, 2001 (VHS)
*April 1, 2003 (DVD)
*December 2, 2003 (VHS and DVD - 4 Movie Dino Pack (Volume 2) and 9 Movie Dino Pack)
*November 29, 2005 (DVD - 2 Mysteries Beyond the Great Valley)

==Cultural references==
*Beethovens 3rd shows some scenes from this film.
*"The Lone Dinosaur" is a parody of The Lone Ranger.  Doc also contains similar traits to characters played by John Wayne, Docs name even sounding similar to Waynes nickname, "Duke".

==References==
* 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 