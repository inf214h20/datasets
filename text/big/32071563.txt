Poppies of Flanders
{{Infobox film
| name           = Poppies of Flanders
| image          =
| caption        =
| director       = Arthur Maude
| producer       = 
| writer         =  Herman C. McNeile   Violet E. Powell
| starring       = Jameson Thomas Eve Gray Henry Vibart   Daisy Campbell
| music          =
| cinematography = George Pocknall
| editing        = Sam Simmonds
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = October 1927 
| runtime        = 8,700  
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} British drama film directed by Arthur Maude and starring Jameson Thomas, Eve Gray and Henry Vibart.  It was based on a novel by Herman C. McNeile.

==Cast==
* Jameson Thomas - Jim Brown 
* Eve Gray - Beryl Kingwood 
* Henry Vibart - Earl of Strangeways 
* Gibb McLaughlin - Shorty Bill 
* Daisy Campbell - Countess of Strangeways 
* Malcolm Tod - Bob Standish  Cameron Carr - Merrick 
* Tubby Phillips - Fat Man

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 


 