Kalyana Agathigal
{{Infobox film
| name           = Kalyana Agathigal
| image          = 
| caption        = 
| director       = K. Balachander
| producer       = Rajam Balachander Pushpa Kandaswamy
| writer         = K. Balachander Seema Ashok Kuyili Vanitha Raveendran Nassar Charle Delhi Ganesh
| music          = V. S. Narasimhan
| cinematography = Raghunatha Reddy
| editing        = N. R. Kittu
| studio         = Kavithalayaa Productions
| distributor    = Kavithalayaa Productions
| released       =  
| runtime        = 148 minutes
| country        = India Tamil
}}
 1985 Tamil language drama film directed by K. Balachander. The film features Saritha in lead role. The film was released on April 12, 1985.  The film marked the acting debut of Nassar.

==Plot==

The movie begins with a group of six women who live together as friends after their attempt at living a successful married life is thwarted due to various reasons, majorly because of men, sexual abuse, dowry harassment and the backward society. They also form a music band called "kalyana agathigal" to raise funds for charity to the underprivileged. 
Ammulu, a young runaway girl, joins the gang of girls and becomes a part of the household.

The story is about how their lives make wild twists and turns as they cope with the society and try to find true love.

==Cast==

*Saritha as Ammulu
*Ashok as Robert
*Y. Vijaya as Thangam
*Vanitha as Yesodha Kuyili as Hemalatha
*Lalitha Mani as Premalatha
*Nisha Noor as Zaira
*J. Lalitha as Sumangali
*Akalya as Thamizharasi Seema as Valliammai  Raveendran as Ambikapathy
*Delhi Ganesh as Ammulus father
*Poornam Vishwanathan
*Poovilangu Mohan as Gurumoorthy
*T. S. Babu Mohan
*Charle
*Nassar as Kannayiram, Thangams husband

==Soundtrack==

The music composed by V. S. Narasimhan and lyrics written by Vairamuthu.

==References==
 

==External links==
*  

 

 
 
 
 


 