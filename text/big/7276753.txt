Dr. Jekyll y el Hombre Lobo
{{Multiple issues|
 
 
}}

{{Infobox Film
| name           = Dr. Jekyll y el Hombre Lobo
| image          = 
| caption        = 
| director       = León Klimovsky
| producer       = Alfredo Fraile Arturo González
| writer         = Jacinto Molina Jack Taylor
| music          = Antón García Abril
| cinematography = Francisco Fraile
| editing        = Petra de Nieva
| distributor    = Regia-Arturo González Rodríguez  (Spain, theatrical) , Filmaco  (USA, theatrical) 
| released       = 6 May 1972 (Spain)
| runtime        = 96 min
| country        = Spain Spanish
| budget         = 
}}
Dr. Jekyll y el Hombre Lobo (Dr. Jekyll and the Wolfman), also known as Dr. Jekyll and the Werewolf, is a 1971 Spanish horror film, the sixth in the series, about the werewolf Count Waldemar Daninsky, played by Paul Naschy. It was followed by a sequel, El retorno de Walpurgis.

==Plot summary==

Paul Naschy returns as El Hombre Lobo for the sixth time as he searches for a cure to his lycanthropy by visiting the grandson of the infamous Dr. Jekyll. He is given a serum that transforms him into a Hyde-like personality in the hope that it will sublimate his werewolf self, but it results unfortunately in an even more savage monster. The film features a classic scene wherein the wolfman transforms in an elevator, much to the chagrin of a female fellow passenger. Famed Euro-horror star Jack Taylor played Dr. Henry Jekyll.

==Cast==

* Jacinto Molina as Waldemar Daninsky/Wolfman/Mr. Hyde
* Shirley Corrigan as Justine Jack Taylor as Dr. Henry Jekyll
* Mirta Miller as Sandra
* José Marco as Imre Kosta
* Luis Induni as Otvos
* Bernabe Barta Barri as Gyogyo, the inn-keeper
* Luis Gaspar as Thurko, Otvoss thug
* Elsa Zabala as Uswika Bathory
* Lucy Tiller
* Jorge Vico
* Adolfo Thous

==External links==
*  
*  

 
 

 
 
 
 
 
 


 
 