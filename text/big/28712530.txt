Men of Steel (film)
{{Infobox film
| name           = Men of Steel
| image          =
| image_size     =
| caption        = George King
| producer       = Ray Wyndham
| writer         = Edward Knoblock Billie Bristow Douglas Newton (novel) John Stuart Heather Angel Franklin Dyall
| music          = Arthur Dulay Arthur Grant
| editing        =
| studio         = Langham Films
| distributor    = United Artists
| released       = September 1932
| runtime        = 71 minutes
| country        = United Kingdom English
}}
 George King John Stuart, Heather Angel. The screenplay was adapted by Edward Knoblock and Billie Bristow from a novel by Douglas Newton.

==Production background== distribution by United Artists and, as its title implies, is set in a steel-producing town.  Location filming took place in Middlesbrough, with the steelworks scenes being shot in the long-defunct Acklam Iron and Steel Works in the town, rendering it of great interest to social and industrial historians of the Teesside area.  Men of Steel does not appear ever to have been shown on television in the UK, nor has it been made available commercially; 

==Preservation status==
Unlike many quota quickie productions, the film has survived and is available to view by appointment at any of the Mediatheques run by the British Film Institute. 

==Plot==
James Harg (Stuart) and his father work in a steelmaking plant which is incompetently run, with scant attention being paid to worker safety.  In his own time, Harg works on ideas for a revolutionary new manufacturing process for hard steel.  When his father is badly injured in a workplace accident resulting from employer negligence, Harg uses some of the compensation payment to develop his invention to a stage where it can be tested in practice.  It is a huge success and Harg patents his process.  He rises to a position on the board of the company, before staging a coup to oust his former employer and take over the business himself.
  
==Cast== John Stuart as James Iron Harg
* Benita Hume as Audrey Paxton Heather Angel as Ann Ford
* Franklin Dyall as Charles Paxton
* Mary Merrall as Mrs. Harg Alexander Field as Sweepy Ford
* Edward Ashley-Cooper as Sylvano

==References==
 

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 
 
 
 