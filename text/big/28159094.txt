Delivery Health Girl: The Moisture of Silken Skin
{{Infobox film
| name = Delivery Health Girl: The Moisture of Silken Skin
| image = Delivery Health Girl - The Moisture of Silken Skin.jpg
| image_size =
| caption = Theatrical poster for Delivery Health Girl: The Moisture of Silken Skin (2002)
| director = Yutaka Ikejima 
| producer = 
| writer = Kyōko Godai
| narrator = 
| starring = Noriko Masaki Kyōko Hashimoto
| music = Ichimi Ōba
| cinematography = Shōji Shimizu
| editing = Shōji Sakai
| studio = Cement Match
| distributor = OP Eiga
| released = September 19, 2002
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross =  
| preceded_by = 
| followed_by = 
}}

  is a 2002 Japanese pink film directed by Yutaka Ikejima and written by his wife and frequent collaborator, Kyōko Godai. It won the award for Ninth Best Film and Noriko Masaki was given a Best New Actress award at the Pink Grand Prix ceremony. 

==Synopsis==
A delivery health service is run by a mysterious middle-aged man named Yata. The girls who work for Yata include, Koya, Harumi and Akino. Customers include Kizaki, a nerdish young man, and an Adult Video company president and his Assistant Director who secretly film their exploits.   

==Cast==
* Noriko Masaki ( ) as Kaya (Delivery health girl) 
* ( ) as Harumi (Delivery health girl)
* Kyōko Hashimoto as Akino (Delivery health girl)
* Kikuo Honda ( ) as Hattori
* Ginchi ( ) as Kizaki
* Kyōsuke Saaski ( ) as Shibata (AV Company President)
* Naohiro Hirakawa ( ) as Mori (Assistant Director)
* Hyōdo Mikihiro ( ) as Takashi
* Kazu Itsuki ( ) as Inoue
* ( ) as Yata
* Shiori Kawamura ( ) as Editor
* Kaede Matsushima ( ) as Fuyuko

==Availability==
  on September 19, 2002.    On October 16, 2008 it became available through the Hokuto Corporations online DMM service. 

==Bibliography==
*  
*  
*  

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 
 


 
 