Darpan Chaya
{{Infobox film name     = Darpan Chaya image    = DarpanChaya.jpg caption  = Promotional Poster director = Tulsi Ghimire producer = Shrawan Ghimire writer   = Tulsi Ghimire released =   language = Nepali
|country  = Nepal gross    = NRs 70 Million (1,000,000 USD)
}}

Darpan Chaya (  Nepali film directed by Tulsi Ghimire, starring Dilip Rayamajhi, Niruta Singh, Uttam Pradhan and Tulsi Ghimire.

==Film==
Darpan Chaya is considered to be one of the best Nepali movies. It grossed NRs 70 million at the box office, surpassing Tulsi Ghimires own 1987 classic Kusume Rumal to become the highest grossing Nepali movie of all time.  The soundtrack of the film, with music composed by Ranjit Gazmer, became hugely popular. 

== Cast ==
*Dilip Rayamajhi as Raj
*Niruta Singh as Smriti
*Uttam Pradhan as Abhishek
*Tulsi Ghimire as Smritis dad

==Soundtrack==
{| class="wikitable sortable"
|-
! Track !! Artists !! Length
|-
|Lahana Le  Jurayo Ki|| Sadhana Sargam || 5.56
|-
|Tyo Danda Paari|| Udit Narayan and Deepa Jha || 5.16
|-
|Ukali Ko Dandai Danda|| Rubi Joshi and Meera Rana || 1.45
|-
|Bainshalu Mana Le Rojeko|| Udit Narayan and Sadhana Sargam || 3.43
|-
|Lahana Le Jurayo Ki|| Indrajeet Mijar || 5.06
|-
|Lahana Le Jurayo Ki|| Ram Krishna Dhakal || 5.50
|}

==See also==
*List of Nepalese films

==References==
 

==External links==
*  
*  

 
 
 
 


 