The Man Who Wouldn't Talk
  1958 British drama film directed by Herbert Wilcox. It starred Anna Neagle, Anthony Quayle, Zsa Zsa Gabor, Dora Bryan, John Le Mesurier and Lloyd Lamble.

==Synopsis== American scientist charged for murder by the British police for his supposed role in the death of an Eastern Bloc defector.

==Cast==
* Anna Neagle as Mary Randall, Q.C.  
* Anthony Quayle as Frank Smith  
* Zsa Zsa Gabor as Eve Trent  
* Katherine Kath as Miss Delbeau  
* Dora Bryan as Telephonist  
* Patrick Allen as Kennedy   Hugh McDermott as Bernie  
* Leonard Sachs as Professor Horvard  
* Edward Lexy as Hobbs   John Paul as Castle  
* John Le Mesurier as Judge  
* Anthony Sharp as Baker
* Lloyd Lamble as Bellamy

==External links==
* 

 

 
 
 
 
 
 
 


 