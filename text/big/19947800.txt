Timestalkers
 
{{Infobox television film
| name           = Timestalkers
| image          = Timestalkers.jpg
| caption        = 
| genre          = Adventure
| director       = Michael Schultz Richard Maynard John Newland Milton T. Raynor
| writer         = 
| screenplay     = Brian Clemens
| story          = 
| based on       =  
| narrator       = 
| starring       = William Devane Klaus Kinski Lauren Hutton
| music          = Craig Safan
| cinematography = Harry Mathias
| editing        =  Conrad M. Gonzalez
| studio         =
| country        = USA
| language       = English
| first_aired    =  
| network        = CBS
| runtime        = 100 minutes
}}

Timestalkers is a 1987 made for TV adventure film directed by Michael Schultz and starring William Devane.    The film is based on Ray Browns novel The Tintype.

==Plot==
Dr. Scott McKenzie (William Devane) is a college professor and ardent fan of the gunslinger culture of the Old West.  A year ago, McKenzie watched his wife and son die in a car crash caused by a drunk driver attempting to flee the police.  With his friend, General Joe Brodsky (John Ratzenberger), McKenzie attends an auction of Wild West memorabilia, where they agree to bid on a pair of steamer trunks and split the contents between them.  As the auction is conducted, flashbacks show the items histories in the 19th century.  At the time a man, Joseph Cole (Klaus Kinski), is looking for a gunslinger who has a pair of distinctive ebony-handled pistols marked with stars.  Some cowboys he encounters on the road point him towards the town of Crossfire, California.  At the town saloon, he inquires about the man but is harassed by a trio of local thugs, one of whom shoots at him, hitting instead one of the trunks which Dr. McKenzie is bidding on in the present.  Cole quickly shoots all three men dead, an event memorialized by a local photographer.  In the present, McKenzie and Brodsky win the trunks at auction and McKenzie begins sorting through the contents, noticing the picture of the men who Cole killed.  

Under photo enhancement, McKenzie notices Cole in the background of the picture and identifies the gun Cole is carrying as a .357 Magnum from the 1980s.  Despite this, chemical and spectroscopic testing indicates that the photograph is at least 100 years old.  McKenzie becomes convinced that Cole is a time traveler.  After writing up his findings, he is approached by a woman, Georgia Crawford (Lauren Hutton), who claims to be working on similar ideas.  Together they locate Crossfire in the present.  When they split up to search the town, Georgia ducks into an old barn and removes a crystalline device which she uses to travel back to the 1880s.  There she searches for Cole at a nearby river, but her horse is spooked by a rattlesnake. She dispatches the snake with a futuristic gun.  Hurrying back to town, she is followed by Cole, who had observed her from afar.  He arrives too late to stop her from traveling to the present, but he uses a device of his own to find out the time to which she traveled.  In the present, McKenzie hears a noise and arrives at the barn just in time to see Georgia return from the past.  When he confronts her, she admits to being one of a number of time travelers from the 26th century.  Returning to McKenzies home she explains that Cole is a renegade scientist from her time who she has been sent back to stop.  She believes that Cole, who developed the time travel technology with her father, is intending to change history.  Through research, Georgia and McKenzie determine that Cole is likely trying to kill Matthew Crawford, an adviser to President Grover Cleveland.

The gunfighter who Cole is searching for, the mysterious "Star-Handled Stranger," was a gunslinger who crucially helped protect the President and his escort from bandits.  Matthew Crawford was Georgias ancestor and killing him would erase her entire family from history, including her father who had opposed Coles desire to continue research on time travel.  McKenzie and Georgia enlist Joe Brodskys help in determining President Clevelands movements, but before Brodsky can give them the information, Cole murders him and flees into the past.  Finding a copy of the information, they discover that Cole is traveling back to July 11, 1886, when the mysterious "Star-Handled Stranger" helped save President Cleveland from a bandit attack.  Traveling back in time to 1886, Georgia and McKenzie watch as the attack begins.  When the Stranger arrives on the scene and tries to enter the attack, he is shot dead by Cole.  McKenzie then takes the Strangers signature pistols and rides towards the battle himself.  Cole follows him.  While McKenzie manages to kill the attacking bandits, Cole shoots Matthew Crawford.  McKenzie then kills Cole in a one-on-one duel.  Returning to the stagecoach, Georgia and McKenzie discover that Matthew Crawford was merely wounded.  As the stagecoach drives off, Georgia and McKenzie return to the present where Georgia gives him a gift before returning to her own time.  She is somehow able to send the knowledge of the death of McKenzies wife and child back to his earlier self so that he is able to save their lives.

==Cast==
* William Devane as Scott McKenzie
* Lauren Hutton as Georgia Crawford
* John Ratzenberger as General Joe Brodsky
* Forrest Tucker as Texas John Cody
* Klaus Kinski as Dr. Joseph Cole
* Tracey Walter as Sam James Avery as Blacksmith
* R. D. Call as Bart John Considine as Doctor Crawford / Matthew Crawford
* Danny Pintauro as Billy McKenzie
* Gail Youngs as Mrs. Laurie McKenzie
* Patrik Baldauff as Callan
* Ritch Brinkley as Barman Michael Flynn as Michael
* A. J. Freeman as Grover Cleveland
* Tim Russ as Sergeant Filton

==Broadcast history== Lifetime and Syfy|Sci-Fi Channel networks thru the 90s. It was aired several times during the spring and summer of 2010 on the This TV Network.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 