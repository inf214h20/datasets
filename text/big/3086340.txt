Gozu
 
{{Infobox film
| image          = Gozu_Poster.jpg
| caption        =
| director       = Takashi Miike
| producer       = Kana Koido Harumi Sone
| writer         = Sakichi Sato
| starring       = Hideki Sone Show Aikawa
| music          = Kôji Endô
| cinematography = Kazunari Tanaka
| editing        = Yasushi Shimamura
| distributor    = Cinema Epoch
| released       = July 12, 2003
| runtime        = 130 min.
| country        = Japan
| language       = Japanese
}} Japanese cult film directed by Takashi Miike and written by Sakichi Sato.

==Plot==
Structurally, Gozu is a succession of bizarre scenes sandwiched between a storyline involving Minami’s search for his Yakuza brother Ozaki in a small town, that is reminiscent of the episodic quests in Greek Mythology.   

Minamis encounter with a minotaur-like creature gives the film its name (Gozu is Japanese for cows head).

==Cast==
*Hideki Sone as Minami
*Show Aikawa as Ozaki
*Kimika Yoshino as Female Ozaki
*Shohei Hino as Nose
*Keiko Tomita as Innkeeper
*Harumi Sone as Innkeepers Brother
*Renji Ishibashi as Boss

==Reception== Cannes Film Festival in May 2003 secured its theatrical release overseas. 
The film received mostly positive reviews from the New York Times, BBC, Washington Post and internet sites Rotten Tomatoes and IMDB, with most critics commenting on the deeply surreal and disjointed nature of the film.   

==See also==
* Ox-Head and Horse-Face

==References==
 

==External links==
*  
*  
*   at Metacritic
*  
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 