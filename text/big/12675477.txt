Dhamaal
 
 
 
{{Infobox film
| name           = Dhamaal
| image          = Dhamaal 2007.jpg
| caption        = Theatrical release poster
| director       = Indra Kumar
| producer       = Indra Kumar Ashok Thakeria
| story          = Tushar Hiranandani
| screenplay     = Bunty Rathod
| starring       = Ritesh Deshmukh Arshad Warsi Javed Jaffrey Sanjay Dutt Aashish Chaudhary
| music          = Adnan Sami
| cinematography = Vijay Arora
| editing        = Sanjay Sankla
| studio         =
| distributor    = Maruti International
| released       =  
| runtime        = 137 mins
| country        = India Hindi
| awards         =
| budget         =  
| gross          =  
}}
 Dhamaal film series.
 Road Trip, Bean - Rat Race, Dude, Wheres My Car? and Johnny English. Dhamaal released on 7 September 2007, and received positive response from critics and public audiences. It  managed to do  well at the box office, and was a surprise super-hit.The movie was a classic one and the title of the laugh for many people especially in Nepal.

In 2011, the film spawned a sequel, under the title of Double Dhamaal, with the lead cast reprising their roles.  Double Dhamaal didnt manage to attract the attention of many critics, and did average business at the box office unlike its original.

==Plot==
Dhamaal is about four friends; Boman Contractor (Aashish Chaudhary), who lives a wealthy lifestyle with his eccentric dad, Nari (Asrani), who loves his antique car more than his son, and asks his son to leave when he damages the car; dim-witted Manav (Jaaved Jaffrey), his elder brother Adi (Arshad Warsi); and Roy (Ritesh Deshmukh), a trickster who wears a magnetized belt buckle. All four are afflicted by the get-rich-quick virus. Their fortunes take a turn after they accidentally bump into an underworld don Bose (Prem Chopra) whilst he is about to die, who tells them about a hidden treasure in St. Sebastian Garden, Goa including  10crores. The group race towards Goa in Naris stolen car, little knowing that their plans will soon be foiled, albeit hilariously, by Police Inspector Kabir Nayak (Sanjay Dutt), who wants the money at any cost.

==Cast==
* Sanjay Dutt  as  Inspector Kabir Nayak
* Riteish Deshmukh  as  Deshbandhu Roy
* Arshad Warsi  as  Aditya "Adi" Shrivastav
* Jaaved Jaffrey  as  Manav Shrivastav
* Aashish Chaudhary  as  Boman Contractor
* Asrani  as  Nari Contractor
* Sanjai Mishra  as  Babubhai (Aatank)
* Vijay Raaz  as  Dev Kumar Malik (D.K Malik)
* Prem Chopra  as  Bose
* Suhasini Mulay  as  the Landlady
* Murli Sharma  as  Inspector Kulkarni (Crime Branch)
* Tiku Talsania  as  Commissioner Chaturvedi
*  Manoj Pahwa  as  Pilot
* Vijay Patkar  as  Air Traffic Controller
* Vinay Apte  as  Prabhakarna Sripalawardhana Atapattu Jayasuriya Laxmansriramkrishna Shivavenkata Rajasekara Sriniwasana Trichipalli Yekya Parampeel Parambatur Chinnaswami Muthuswami Venugopal Iyer

==Soundtrack==
{{Infobox album| 
| Name     = Dhamaal
| Type     = soundtrack
| Artist   = Adnan Sami
| Cover    = Dhamaal_music.jpg
| Released = 
| Recorded = 
| Producer =  Feature film soundtrack
| Length   = 
| Label    = Big Music
}}

The films soundtrack is composed by Adnan Sami with lyrics penned by Sameer (lyricist)|Sameer. The songs, "Dekho Dekho" and "Miss India Marthi Mujhpe", were both known as the title song, since the main chorus of both the songs included the title "Dhamaal".

===Track listing===
{{Track listing
| all_music = Adnan Sami Sameer
| extra_column = Singer(s)
| title1 = Chal Na Che Shor Machlein Shaan
| length1 = 4:56
| title2 = Chandani Raat Hai Saiyan
| extra2 = Asha Bhosle, Amit Kumar
| length2 = 4:22
| title3 = Dekho Dekho Dil Ye Bole
| extra3 = Adnan Sami, Shaan
| length3 = 5:13
| title4 = Miss India Martee Mujhpe
| extra4 = Adnan Sami, Amit Kumar
| length4 = 5:05
| title5 = Chal Na Che Shor Machlein
| note5 = Instrumental
| extra5 = Navin Prabhakar
| length5 = 4:22
| title6 = Chandani Raat Hai Saiyan
| note6 = Instrumental
| extra6 = Raghav Sachar
| length6 = 4:02
| title7 = Chandani Raat Hai Saiyan
| note7 = Remix
| extra7 = Sandeep Shirodkar
| length7 = 4:56
}}

==Reception==
Dhamaal received mixed to positive reviews upon release. Taran Adarsh rated the film 3/5 describing it as a "laugh-riot". At the box office, the film had an good opening, grossing RS.41,00,00,000 with a nett gross of RS.29,34,00,000. After a full theatrical run, the film was declared a super-hit verdict.

==Unknown Fact==
This is one of the very few movies in Hindi which has not a single female lead (or Heroine).

==Sequel==
A sequel named Double Dhamaal was released in the year 2011 which was a moderate success.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 