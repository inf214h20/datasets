Gomathiyin Kaadhalan
 
{{Infobox film
| name = Gomathiyin Kaadhalan கோமதியின் காதலன்
| image =
| director = P. Neelakantan
| story = Devan
| screenplay = P. Neelakantan Savithri K. A. Thangavelu K. Sarangkapani D. Balasubramaniam R. Balasubramaniam  T. P. Muthulakshmi
| producer = T. R. Ramachandran
| music = G. Ramanathan
| distributor = TRR Productions
| released =   
| runtime =
| country = India
| language = Tamil
| budget =
}}
Gomathiyin Kaadhalan ( ) is a Tamil language film starring T. R. Ramachandran and Savitri (actress)|K. Savithri. The film was released in the year 1955.

==Cast==
* T. R. Ramachandran 
* Savitri (actress)|K. Savithri
* K. A. Thangavelu
* K. Sarangkapani
* D. Balasubramaniam
* R. Balasubramaniam
* Friend Ramasamy
* P. D. Sambandhan
* T. P. Muthulakshmi
* P. S. Gnanam
* S. R. Janaki

==Soundtrack== Playback singers are Seerkazhi Govindarajan, A. M. Rajah, Jikki, P. Leela, Radha Jayalakshmi, A. P. Komala & T. V. Rathinam

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ananganai Nigarttha Azhagane  || Jikki ||  || 
|- 
| 2 || Vanameedhil Neendhi Odum Vennilaave || Seerkazhi Govindarajan ||  || 
|- 
| 3 || Pudhumai Nilaa Ange || A. M. Rajah ||  ||  
|- 
| 4 || Anbe En Aaruyire Vaaraai || Seerkazhi Govindarajan & Jikki ||  || 
|- 
| 5 || Varavendam Endru Solladi || A. P. Komala ||  || 
|- 
| 6 || Kaaviya Kaadhal Vaazhvil Oviyam Naame || A. M. Rajah & Jikki ||  || 
|- 
| 7 || Thelli Tharum Tinai Maa || Seerkazhi Govindarajan ||  || 
|- 
| 8 || Ambigaapathy Naadagam || P. Leela, Radha Jayalakshmi & T. V. Rathinam ||  || 
|- 
| 9 || Minnuvadhellam Ponnendru Enni || Seerkazhi Govindarajan & Jikki ||  || 
|- 
| 10 || Kongu Naattu Sengkarumbe || Seerkazhi Govindarajan ||  || 
|}

 
 
 
 
 


 