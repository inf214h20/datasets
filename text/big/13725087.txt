Hogan's Romance Upset
 
{{Infobox film
| name           = Hogans Romance Upset
| image          = 
| image size     = 
| caption        = 
| director       = Charles Avery
| producer       = Mack Sennett
| writer         = 
| narrator       =  Charles Murray
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 11 minutes
| country        = United States  Silent English English intertitles
| budget         = 
}}
 short comedy Fatty Arbuckle and Harold Lloyd in uncredited roles as a spectators.

==Cast== Charles Murray - Hogan
* Bobby Dunn - Weary Willie
* Louise Fazenda
* Ben Turpin
* Ted Edwards - Athletic Club Member
* Vivian Edwards
* Billy Gilbert Frank Hayes
* Charles Lakin - Athletic Club Member
* Josef Swickard Roscoe Fatty Arbuckle - Fight Spectator (uncredited)
* Billie Brockwell - Bit Role (uncredited)
* Charley Chase - Bit Role (uncredited)
* Harold Lloyd - Fight Spectator (uncredited)
* Ford Sterling - Fight Spectator (uncredited) Al St. John - Bit Role (uncredited)
* Mack Swain - Fight Spectator (uncredited)

==See also==
* List of American films of 1915
* Fatty Arbuckle filmography
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 