Oru Sindoora Pottinte Ormaykku
{{Infobox film
| name           = Oru Sindoora Pottinte Ormaykku
| image          =
| caption        =
| director       = Cochin Haneefa
| producer       = Muhammed Mannil
| writer         =
| screenplay     = Urvashi Lissy Lissy Cochin Haneefa Shyam
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Lissy and Cochin Haneefa. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Mammootty Urvashi
*Lissy Lissy
*Cochin Haneefa
*Lalu Alex
*Prathapachandran
*KPAC Sunny
*Ramu
*Jose
*Captain Raju
*Mala Aravindan
*Jose Prakash

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Innee Naadin Raajaavu || Krishnachandran || Poovachal Khader || 
|-
| 2 || Kunjaadin Veshathil || P Jayachandran, Chorus || Poovachal Khader || 
|-
| 3 || Prakaashame || KS Chithra, Unni Menon || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 