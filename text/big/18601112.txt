Temptation Island (1980 film)
{{multiple issues|
 
 
}}

{{Infobox Film
| name         = Temptation Island
| caption      =
| image        = TemptationIsland1980.jpg
| writer       = Toto Belano (story and screenplay)
| starring     = Azenith Briones Jennifer Cortez Deborah Sun Bambi Arambulo Dina Bonnevie Alfie Anido Ricky Belmonte Domingo Sabado Jonas Sebastian
| director     = Joey Gosiengfiao
| producer     = Lily Monteverde
| music        = Jun Latonio
| cinematography = Ricardo Jacinto
| editing        = Rogelio Salvador Regal Films
| released     = July 4, 1980
| runtime      = 115 minutes Filipino and English
| country      = Philippines
}}

Temptation Island is a 1980 Filipino film directed by Joey Gosiengfiao starring four beauty contest winners: Azenith Briones (Miss Photogenic, Mutya ng Pilipinas 1975), Jennifer Cortez (Binibining Pilipinas-Universe 1978), Bambi Arambulo (Miss Maja Pilipinas 1977) and Dina Bonnevie (1st Runner-up, Miss Magnolia 1979). Written by Toto Belano, the film is about a group of beauty contest finalists stranded in a desert island without food, water and shelter.

==Plot==
The film focuses on four young ladies from different social backgrounds and each for their own various reasons, enlists in the fictional "Miss Manila Sunshine Beauty Pageant".

The first of which is Dina (Bonnevie), a college student who entered the contest in order to earn independence from her family and buy her own car.  Next is spoiled, rich socialite, Suzanne (Cortez), whose every whim is attended to by her maid, Maria (Sun), and who learned of the contest when fliers, dropped from a helicopter, interrupted her sunbathing at the family pool. Out of sheer vanity and boredom, she decided to sign up. Thirdly, Bambi (Arambulo), while planning her 18th birthday party, when she and her mother argue over the budget, since her once rich family cannot afford the grand Debutante|debut, Bambi is forced to settle for a much simpler party. But during their argument, Bambi falls on her birthday cake; when she sees the pageants TV spot, her frustrations over her current situation inspire her to join. Rounding up the group is Azenith (Briones), a con artist who plans to rig the contest by using her and her boyfriends sexuality to influence the judges into voting for her.

The ladies later became the finalists for the competition. En route to the evening gown competition, the ship they boarded catches fire, and the passengers scramble to evacuate. The four women, Maria, Joshua (the gay pageant coordinator played by Jonas Sebastian) and his boyfriend Ricardo (Ricky Belmonte), Umberto (one of the ships waiters played by Domingo Sabado) and Alfredo (played by Alfie Anido) land on a desert island.

==Cast==
*Dina Bonnevie - Dina Espinola
*Azenith Briones  - Azenith Tobias
*Jennifer Cortez - Suzanne Reyes
*Bambi Arambulo - Bambi Belisario
*Deborah Sun - Maria
*Ricky Belmonte - Ricardo
*Alfie Anido - Alfredo
*Domingo Sabado  - Umberto
*Jonas Sebastian - Joshua
*Tonio Gutierrez - Tonio
*Anita Linda - Nenuca Belisario

==Reception==
The film has been described as "campy", but it has earned positive responses from several critics. Jessica Zafra calls it a "classic Filipino film   dares to expose complex themes..." Noel Vera stated that "By turns grotesque, deadpan witty and surreal...  leave you howling in laughter and tears, both."  Boy Abunda has referred to it as a "cult classic."  Regal Films, the distributor of the film and its subsequent release on video bills it "Hands-down, most hilarious Filipino film ever made!"  The film has also been adapted for theater. Directed by renowned theater playwright and director, Chris Martinez, Temptation Island...Live! was a theater adaptation that starred gay actors (John Lapuz and Tuxqs Rutaquio among others) as the beauty queens.

==Filming locations== De La Salle University, Manila
*Araneta Coliseum (now Smart Araneta Coliseum), Quezon City
*Paoay, Ilocos Norte Manila International Airport (now Ninoy Aquino International Airport)

==2011 Version==
 
A new version of Temptation Island was released, featuring a new batch of cast. In the movie are Heart Evangelista, Rufa Mae Quinto, Lovi Poe, Solenn Heussaff and Marian Rivera. It also features Aljur Abrenica, Tom Rodriguez and John Lapus.  The new version used Computer Graphic Imaging replacing the analog in the original 1980 movie.  Two of the original cast, Deborah Sun and Azenith Briones, are also included in the new movie version.

==References==
 

==External links==
*  

 
 
 
 
 
 
 