Bhaaryaye Aavashyamundu
{{Infobox film
| name = Bhaaryaye Aavashyamundu
| image =
| caption =
| director = M. Krishnan Nair (director)|M. Krishnan Nair
| producer =
| writer = Cheri Viswanath
| screenplay = Cheri Viswanath
| starring = Prameela Sathar MG Soman Radhika
| music = MS Baburaj
| cinematography =
| editing =
| studio = Archana Filims
| distributor = Archana Filims
| released =  
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed M. Krishnan Nair (director)|M. Krishnan Nair. The film stars Prameela, Sathar, MG Soman and Radhika in lead roles. The film had musical score by MS Baburaj.  

==Cast==
*Prameela 
*Sathar
*MG Soman 
*Radhika Vincent

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Indraneelamanivaathil || K. J. Yesudas || ONV Kurup || 
|- 
| 2 || Mandaaratharupetta || K. J. Yesudas || ONV Kurup || 
|- 
| 3 || Poovum ponnum || K. J. Yesudas || ONV Kurup || 
|}

==References==
 

==External links==

 
 
 


 