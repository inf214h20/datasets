Broadway Melody of 1938
{{Infobox film
| name           = Broadway Melody of 1938
| image          = Broadway Melody 1938 poster.jpg
| image_size     = 190px
| caption        = Theatrical release poster
| director       = Roy Del Ruth Jack Cummings
| writer         = Jack McGowan Robert Taylor Eleanor Powell
| music          = Nacio Herb Brown (songs-music) Arthur Freed (song-lyrics)
| cinematography = William H. Daniels
| editing        = Blanche Sewell MGM
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $1,588,000  .  gross = $2,846,000 
}}
 musical film, Robert Taylor and features Buddy Ebsen, George Murphy, Judy Garland, Sophie Tucker, Raymond Walburn, Robert Benchley and Binnie Barnes.
 The Wizard of Oz as Dorothy.

==Plot== Robert Taylor) who, impressed with her dancing and singing, sets her on the road to stardom and romance blossoms between the two. A subplot involves a boarding house for performers run by Sophie Tucker, who is trying to find a big break for young Judy Garland.

==Cast==
   Robert Taylor as Steve Raleigh
*Eleanor Powell as Sally Lee
*Judy Garland as Betty Clayton
*Buddy Ebsen as Peter Trot
*Sophie Tucker as Alice Clayton
*George Murphy as Sonny Ledford
 
*Binnie Barnes as Caroline Whipple
*Raymond Walburn as Herman J. Whipple  
*Robert Benchley as Duffy, P.R. Man  Willie Howard as The Waiter 
*Charley Grapewin as James K. Blakeley
*Billy Gilbert as George Papaloopas
*Robert John Wildhack as The Sneezer
 

==Songs==
The majority of songs in Broadway Melody of 1938 were written by Nacio Herb Brown (music) and Arthur Freed (lyrics):  

*"Broadway Melody" (1929) - in opening credits
*"You Are My Lucky Star" (1935) - in opening credits
*"Yours and Mine" (1937) - sung by Eleanor Powell (dubbed by Marjorie Lane); danced by Eleanor Powell and George Murphy; danced by Judy Garland and Buddy Ebsen
*"Follow in My Footsteps" (1937) - sung and danced by George Murphy, Buddy Ebsen and Eleanor Powell (dubbed by Marjorie Lane)
*"Everybody Sing" (1937) - sung by Judy Garland, Sophie Tucker, Barnett Parker and chorus
*"Im Feeling Like a Million" (1937) - sung and danced by George Murphy and Eleanor Powell (dubbed by Marjorie Lane); recorded by Judy Garland but cut from the film Charles Igor Gorin , danced by George Murphy, Eleanor Powell, Buddy Ebsen and Judy Garland; recorded by Judy Garland and chorus but cut from the film
*"Broadway Rhythm" (1935) - sung by a chorus and danced by Eleanor Powell
*"Got a Pair of New Shoes" (1937) - sung by a chorus and danced by Eleanor Powell in the finale
*"Sun Showers" (1937) - recorded by Charles Igor Gorin but cut from the film

;Other songs:
*"The Toreador Song" (1875) - from Carmen, by Georges Bizet (music) and Henri Meilhac and Ludovic Halévy (libretto), sung by Charles Igor Gorin in the opening scene
*"Some of These Days" (1910) - music and lyrics by Shelton Brooks, sung by Sophie Tucker
*"Largo al factotum" (1816) - from Il Barbiere di Siviglia (The Barber of Seville), by Gioacchino Rossini (music) and Cesare Sterbini (libretto), sung by Charles Igor Gorin Joseph McCarthy (lyrics) with special lyrics for the "Dear Mr. Gable" segment by Roger Edens; sung by Judy Garland

==Production==
This was the third of the "Broadway Melody" series, and had the working title of Broadway Melody of 1937.   When it was released, late in 1937, it was advertised with the tagline So new its a year ahead!.

MGM borrowed Binnie Barnes from Universal Pictures for the film. TCM   

The film was in production from late February to 20 July 1937, and was released on 20 August. TCM     Its initial running time was 115 minutes, compared to the final running time of 110 minutes. 
 Everybody Sing, which was held for later release in 1938.   

The finale of the film takes place on a giant set upon which neon signs are visible showing the names of famous stage and screen stars. During Sophie Tuckers final number, all of the signs in the background actually change to read "Sophie Tucker" in tribute to her.

==Box Office==
According to MGM records the film earned $1,889,000 in the US and Canada and $957,000 elsewhere resulting in a profit of $271,000. 
==In popular culture==
*A 1937 Our Gang comedy, Our Gang Follies of 1938, spoofs the title, concept, and style of Broadway Melody of 1938.
*Judy Garland singing "You Made Me Love You" to a picture of Clark Gable gets parodied in the 2007 musical film Hairspray (2007 film)|Hairspray when Link (Zac Efron) sings to a picture, of Tracy (Nikki Blonsky), which comes to life to sing with him.
*Tom Lehrer, in a song satirising George Murphys election to the U.S. Senate, sang, "Imagine Broadway Melody of Nineteen Eighty-Four|1984".
*A song on Genesis (band)|Genesis album The Lamb Lies Down on Broadway is entitled "Broadway Melody of 1974".

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 