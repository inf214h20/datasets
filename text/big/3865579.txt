My Wild Irish Rose
{{Infobox film
| name           = My Wild Irish Rose
| image	         = My Wild Irish Rose FilmPoster.jpeg
| image_size     =
| alt            =
| caption        = Theatrical release poster David Butler
| writer         =
| narrator       =
| starring       = Dennis Morgan
| music          = Max Steiner Ray Heindorf
| cinematography = Arthur Edeson William V. Skall
| editing        =
| studio         =
| distributor    = Warner Bros.
| released       = December 27, 1947
| runtime        = 101 minutes
| country        = United States 
| language       = English
| budget         =
| gross          =
}} David Butler. It stars Dennis Morgan and Arlene Dahl. It was nominated for an Academy Award in 1948. 

A fictionalized bio-pic of Chauncey Olcott, the film traces the rise of an Irish-American tenor to stardom at the end of the 19th century and start of the 20th.

Olcotts original composition of the same name was included in the films music and was nominated for an Academy Award for Best Scoring of a Musical Picture. In the soundtrack to the film, the singing voice was provided by Dennis Day.

==Cast==
* Dennis Morgan as Chauncey Olcott
* Arlene Dahl as Rose Donovan
* Andrea King as Lillian Russell
* Alan Hale, Sr. as John Donovan
* George Tobias as Nick Popolis
* Ben Blue as Hopper George OBrien as William "Duke" Muldoon
* William Frawley as William J. Scanlan

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 