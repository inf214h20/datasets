Mixing Nia
 
{{Infobox Film
| name           = Mixing Nia
| image          = 
| caption        = Love Isnt Always Black & White
| director       = Alison Swan
| producer       = 
| writer         = Alison Swan
| starring       = Karyn Parsons Isaiah Washington Eric Thal Diego Serrano
| music          = 
| cinematography = 
| editing        = 
| distributor    = Arrowhead Pictures Caminer-Gallagher Productions Third Man Films
| released       = April 1997
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} director Alison Swan. The film stars actress Karyn Parsons, as Nia, a biracial woman on a journey to find her true identity.

== Plot ==

The movie begins with Nia (Parsons), an upwardly mobile biracial woman working for an advertising firm in New York City. When Nia and her co-worker at the firm, Matt (Thal) are put in charge of marketing a malt-liquor beverage to African-American teens, Nia quits her job. She decides to presue her dream of writing a novel. She hopes to write about her Jewish father and African American mother meeting and living in Greenwich Village in the 1960s. In doing research and beginning her novel, Nia goes on an unexpected journey into her personal identity.

Though her circle of friends is of all races, she has spent most of her life identifying with the Jewish side of her culture, and is a self-proclaimed yuppie. She hopes to identify more with the African-American side of her heritage, and find an authentic black experience. Her first attempt at this is in enrolling in an African-American writing workshop. In this class she meets and falls for the instructor, Lewis (Washington). At the same time, she goes out with her former co-Worker Matt for drinks, and finds that he has had a long time attraction to her as well. Nia pursues both of these relationships, with each man exploring a different side of her identity. She begins to learn that the two sides of her identity clash, and that she cant even be "too much" of either race. She is at times too white for Lewis, who comments that maybe he should find a "real black woman", and leave Nia. She also at times feels out of place with Matt, feeling she does not fit in with his friends, and in some instances even feels isolated by their opinions and racial remarks.

Everything comes to a head when Nia invites both men to a wedding in which she is a bridesmaid. She is forced to make a decision between the two, but ultimately decides on being alone. She returns home, hoping to speak with her downstairs neighbor Joe (Serrano), a musician. During the whole movie she has had a fleeting flirty relationship with him, free of discussion of her race, and more about finding who she is as a person. She looks for him as her last person to turn to, but finds that he is gone, and has subleased his apartment for 6 months. Joe left a gift for her, a guitar, which inspires her to continue her novel, which she had given up on.

The movie ends with Nia, now reading a poem to the same audience that at one time she had been "too white" for, and Joe clapping for her in the audience.

== References ==
 
* http://www.imdb.com/title/tt0155877/
 

== Film Festival Showings ==
This film was show at the 1998 New York Womens Film Festival.

== External links ==
*  

 
 
 
 
 