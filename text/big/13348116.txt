The Slayer (film)
{{Infobox Film
| name           = The Slayer 
| image          = The_slayer_1982_vhs_video.jpg
| imagesize      = 250px 
| caption        = Vipco video cover 
| director       = J. S. Cardone
| producer       = William R. Ewing Eric Weston Anne Kimmel
| writer         = William R. Ewing  
| starring       = Sarah Kendall Frederick Flynn Alan McRae Carol Kottenbrook
| music          = Robert Folk
| cinematography = Karen Grossman
| editing        = M. Edward Salier
| distributor    = 21st Century Film Corporation International Picture Show Continental Video Inc. Marquis Video Planet Video Inc. 1982
| runtime        = 80 min
| country        = USA English
}} 1982 horror film directed by J. S. Cardone.  The film gained notoriety and was classified in the UK as a video nasty in the 1980s.

==Plot== abstract artist sister Kay (Sarah Kendall), her doctor husband David (Alan McRae), her sister-in-law Brooke (Carol Kottenbrook) along with pilot Marsh (Michael Holmes) become stranded on a rugged isle. For thirty-something Kay her current situation is her worst fears realised, for she has been troubled since her childhood by recurring prophetic nightmares in which she is stalked and slain in a burning room by figure known as The Slayer. Now it seems this place may be in fact its dwelling and she is sure somehow that this so-called entity is lurking close by, biding its time until nightfall, where it will be drawn to Kay who (for whatever reason) dreams of its killings. But, then again, what is real or make believe? Not everything is what it seems in this place.

==Cast==
* Sarah Kendall as Kay
* Frederick Flynn as Eric
* Alan McRae as David
* Carol Kottenbrook as Brooke
* Michael Holmes as Marsh
* Paul Gandolfo as Fisherman
* Newell Alexander as  Kays Father
* Ivy Jones as Kays Mother
* Richard Van Brakel as  Eric (as a child)
* Jennifer Gaffin as Kay (as a child)
* Carl Kraines as The Slayer
* Alan McRae as Stripper

==Release==

=== Critical reception ===

Allmovie wrote "The Slayer boasts some effectively eerie atmosphere and a dark, downbeat attitude. Unfortunately, sluggish pacing eliminates the tension that might have been established between the minimal cast and the sinister deserted-island setting." 

===Home Video===
The film has been released on VHS and DVD format.  The Slayer was released in the U.S. on double feature video format by Continental Video alongside another feature - Scalps.  It was cut by five minutes or so, in order to make room for the second feature, but all the gruesome scenes and violence are intact.  The film has never been made available on DVD in the U.S.

In the UK, the film has had several releases. It was initially released in the UK on VHS uncut from   full screen. 

==See also==
*Exploitation film
*Horror-of-personality
*Slasher film Splatter

==References==
 

==External links==
*  
*   at Yahoo! Movies

 
 
 
 
 
 
 
 