A Fair to Remember
 
{{Infobox film
| name     = A Fair to Remember
| director = Allen Mondell Cynthia Salzman Mondell
| producer = Allen Mondell Cynthia Salzman Mondell Phillip Allen
| writer   = Allen Mondell Cynthia Salzman Mondell
| narrator = Barry Corbin
| music    = Brave Combo
| released =  
| country  = United States
| language = English
}}
A Fair to Remember is a documentary film about the State Fair of Texas that debuted in February 2007 at the Hall of State on the grounds of Fair Park in Dallas, Texas.

==Synopsis==
The film chronicles the history of the State Fair of Texas beginning in 1886. It covers the banning of gambling at the fair, early Midway rides and some of Dallas firsts such as electric lights, power stations and air planes. Old stock footage of the State Fair shows why it attracted people from all over the country. It includes scenes from the bake-offs and cook-offs held annually, plus a comical scene that has women singing a song about Spam (food)|SPAM. A Fair to Remember also shows foods that have been included at the fair. Interviews of attendees provide a look into historical Dallas.

==Production==
The film was written, directed and produced by Allen Mondell, Cynthia Salzman Mondell and co-produced by Phillip Allen. Narration is provided by actor and Texas native Barry Corbin. Original music provided by Brave Combo.

==Other films by the Mondells== A Reason to Live
* Sisters of 77

==References==
 
    p.&nbsp;42-47. Retrieved on October 1, 2007.

 
CBS Channel 11, Dallas, TX
http://cbs11tv.com/video/?id=21680@ktvt.dayport.com Retrieved October 2, 2007.

   

    Retrieved October 1, 2007.

    Retrieved October 1, 2007.

  Preservation Tree Services. Proud sponsor of A Fair to Remember television broadcast
http://www.preservationtree.com/content.asp?menuid=&contentid=News&content=Details&id=69
Retrieved October 1, 2007.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 


 