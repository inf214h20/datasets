Balipeetam
{{Infobox film
| name           = Balipeetam
| image          = Balipeetam.jpg
| image_size     =
| caption        =
| director       = Dasari Narayana Rao
| producer       = Battini Syam Prasad
| banner         = Ravi Shankar Pictures
| writer         = Based on novel Balipeetam by Ranganayakamma
| narrator       = Sharada Murali Mohan
| music          = K. Chakravarthy
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1975
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Balipeetam is a 1975 Telugu Drama film directed by Dasari Narayana Rao.It is produced by Battini Syam Prasad. It is based on the popular socialist novel written by Muppala Ranganayakamma entitled Balipeetam.

==Plot==
Bhaskar (Shobhan Babu), a Dalit marries Aruna (Sharada), a Higher-caste woman, when she is on her deathbed. Fulfilling her wish to die a Sumangali Bhaskar marries her. Aruna does not die and comes back to life from deadly consumption. After her recovery she understands the enormity of her decision and its consequences. She goes to live with Bhaskars family. However Aruna finds it unable to accustom in Bhaskars household. She finds them very much unlike Bhaskar. Their dialect, dressing sense, practices makes her feel the cultural distance. One of the major themes of Dalit Literature, Intercaste marriage is the leitmotif in this movie. Aruna insults Bhaskar on the basis of his lower caste and goes back to her Aunt and Uncle. There she sees that her relatives are more interested in her money than her ill health. Surprisingly, her cousin Kamala marries a man of different religion. She takes care of her family becoming a housewife. She becomes an instrument in Arunas comprehension of human nature. In the end when Aruna is ill again due to consumption, she finds that she and her children need a safer abode than her own home. She goes to Bhaskars family, and she sees that outward appearances do not account for inner purity. The Dalit In-law family takes care of Aruna and her two children. Bhaskar comes to her and Aruna dies in his arms apologizing for sacrificing so many lives with her decisions. And thus the title, Balipeetam: The pedestal of Sacrifice. The core issues of Inter-caste marriages and Interpersonal relations are woven into the movie . 

==Cast==
* Sobhan Babu Sharada
* Nirmalamma
* Roja Ramani Raja Babu
* Hemalatha

==Soundtrack==
* "Chandamama Rave Jabilli Rave" (Lyrics:   and P. Susheela; Cast: Shobhan Babu and Sharada)
* "Kushalama Neeku Kushalamena" (Lyrics: Devulapalli Krishnasastri; Singers: S. P. Balasubrahmanyam and P. Susheela; Cast: Shobhan Babu and Sharada)
* "Maarali Maarali Manushula Nadavadi Maarali" (Lyrics: C. Narayana Reddy; Singers: S. P. Balasubrahmanyam and P. Susheela)
* "Kalasi Padudam" (Lyrics: Srirangam Srinivasa Rao; Singers: S. P. Balasubrahmanyam and P. Susheela)
* "Takku Tikku Takkuladi Bandira" (Lyrics:  )
* "Yesukundam Buddoda" (Lyrics: Kosaraju Raghavaiah; Singers: Pithapuram Nageswara Rao and Madhavapeddi Satyam)

==Boxoffice==
* The film ran for more than 100 days in two centers: Vijayawada and Guntur. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 