Aga Bai Arechyaa 2
{{Infobox film
| name           = Aga Bai Arechyaa 2
| image          = 
| alt            = 
| caption        = 
| director       =  Kedar Shinde
| producer       = Narendra Firodia     Bela Shinde
| writer         = Dilip Prabhavalkar   Kedar Shinde
| screenplay     = Omkar Mangesh Datt
| starring       = Sonali Kulkarni Bharat Jadhav Prasad Oak Dharmendra Gohil Surabhi Hande
| music          = Nishaad
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = May 22, 2015
| runtime        = 
| country        = India Marathi
| budget         =  gross =
}}

 Aga Bai Arechyaa 2  is a Marathi film directed by Kedar Shinde.  It is a sequel to the blockbuster Marathi movie Aga Bai Arrecha!. The movie is produced by Anushka Motion Pictures & Entertainment.  Sonali Kulkarni plays the lead role along with Bharat Jadhav and Prasad Oak.  The movie is slated to release on 22 May 2015. {{cite web|url= http://marathimovieworld.com/news/sulochana-didi-felicitated-by-kedar-shinde-team.php|title= Sulochana Didi felicitated by Kedar Shinde & team
|publisher= marathimovieworld.com|date= Mar 11, 2015|accessdate= Mar 11, 2015}} 

==Cast==
*Sonali Kulkarni
*Bharat Jadhav
*Prasad Oak
*Dharamendra Gohil 
*Surabhi Hande
*Shivraj Waichal

==Soundtrack==
{{tracklist
| extra_column    = Artist(s)
| title1          = Jagnyache Bhaan
| extra1          = Shankar Mahadevan
| length1         = 4:47
| title2          = Dil Mera   
| extra2          = Vaishali Samant
| length2         = 6:23
}}

== References ==
 

== External links ==
* 
* 

 
 
 

 