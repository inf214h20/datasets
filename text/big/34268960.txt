Alter Egos
 
{{Infobox film
| name = Alter Egos
| image =
| alt =  
| caption =
| director = Jordan Galland
| producer = Attic Light Films
| writer = Jordan Galland
| starring = Kris Lemche Sean Lennon Danny Masterson Geneva Carr  Marina Squerciati Brooke Nevin Joey Kern
| music = Sean Lennon
| cinematography =
| editing =
| studio =  Capcom Pictures SModcast Pictures
| released =  
| runtime =
| country = United States
| language = English
| budget =
| gross =
}}
Alter Egos is a comedy film about  , Danny Masterson, John Ventimiglia, and Joey Kern also stars in this film.

Musician Sean Lennon, besides playing "Electric Death", also contributed the musical score for this film.  Galland and Lennon have been friends for around 15 years, and have collaborated on each others albums. Lennon also scored Gallands feature, 2009s Rosencrantz and Guildenstern Are Undead, and also worked with Galland on his award winning debut short, surreal film "Smile For The Camera.  Brooke Nevin appears in this film as well. 
 Daniel Schechter, a personal friend of Jordans, did the trailer for Alter Egos, in exchange for Jordan providing the soundtrack for Supporting Characters.

==Plot==
{{Quote box
| quote = We went through several versions of the style, or the approach, that we were going to take. At first, we did a kind of goofy, Napoleon Dynamite sort of thing which didnt really work out. It sort of highlighted the wrong aspects of the film. Then there was another direction we went, which was a retro Italian superhero film, kind of a Barbarella direction. I really enjoyed making that music, but it didnt ground the film and it lacked gravity. Despite how silly the film is and how kind of comedic it is, it needed a certain seriousness to sort of propel the more important aspects of the narrative&nbsp;– you know, the parents having been killed by this bad guy and the superhero avenging the murder of his parents. It needed some grounding. That was when we sort of went with the third direction, which was kind of a straight-up action hero style– a serious, orchestrated superhero score.
| source = Sean Lennon in Rolling Stone Interview   
| align = left
|bgcolor=#90ee90

| width = 28%
}}

The plot follows an under-appreciated superhero, at a time when superheroes have lost government funding and all public support. In a synopsis by Interview Magazine:

"Alter Egos inhabits a fantasy world where superheroes are a dime a dozen. If you have the powers, as lead supers Fridge (Kris Lemche), and C-Thru (Joey Kern), do, then, well, you can practice them for good, as long as you follow the guidelines, or at least some of them. Quite obviously, this is a parody of parodies and flips the superhero genre on its head. Like Superman, Fridge is dorky in his human clothes. Unlike Superman, its not because he has to do so to keep his identity under wraps—more like he uses his superhero garb to explore different facets of his personality, as evidenced by the title. Somewhere along the storyline, he must tackle the fact that his girlfriend loves his superhero identity more than his unmasked self, and, naturally, face his nemesis, the man who killed his mom and dad—although he really doesnt want to."   

==Cast==
* Sean Lennon as "Electric Death"
* Danny Masterson as "Jimmy"
* Geneva Carr as Newscaster
* Kris Lemche as "Fridge"
* Joey Kern as "C-Thru"
* John Ventimiglia as Shrink
* Brooke Nevin as Claudel
* Christine Evangelista as Emily
* Kristina Klebe as "Ice Scream"
* Marina Squerciati as Dr. Sara Bella
* Aurélie Claudel as rich woman
* Carlos Velazquez (III) as Moon Dog

==Awards==
*Best narrative feature at Bay City Michigans Hells Half Mile Movie Festival 

==In the media==
Popular online magazine  ,  with Alter Egos winning all categories and tying in one.

  as a super villain, began working on the project earlier this summer, composing what Galland describes as "an epic superhero sound." 

In   at times."   GeekChicDaily. 

==Availability==
The film is now available on Amazon.com, Netflix, and iTunes. In Canada its available on Super Channel (Canada).

The movie has grossed over 1 million dollars on Video on demand|VOD. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 