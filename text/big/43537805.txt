Top Hero
{{Infobox film
| name           = Top Hero
| image          =
| caption        =
| writer         = Diwakar Babu  
| story          = S. V. Krishna Reddy
| screenplay     = S. V. Krishna Reddy
| producer       = Mulpoori Venkatrao Aachnta Gopinath
| director       = S. V. Krishna Reddy Balakrishna Soundarya
| music          = S. V. Krishna Reddy
| cinematography = Sarath
| editing        = K. Ram Gopal Reddy
| studio         = Sri Chitra Creations
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}

Top Hero is a 1994  Telugu cinema|Telugu, drama film film produced by Mulpoori Venkatrao & Aachnta Gopinath on Sri Chitra Creations banner, directed by S. V. Krishna Reddy. Starring Nandamuri Balakrishna|Balakrishna, Soundarya in the lead roles and music also composed by S. V. Krishna Reddy.   

==Cast==
 
*Nandamuri Balakrishna as Balu
*Soundarya
*Aamani (Cameo appearance)
*Mahesh Anand as Jala Rakshasudu
*Kota Srinivasa Rao 
*Brahmanandam  Ali as Akkum Bakkum     AVS
*Tanikella Bharani as manager Mallikarjuna Rao
*Gundu Hanumantha Rao as police constable
*Kishore Rathi 
*Vidya Sagar 
*Sivaji Raja Sriman
*Sakshi Ranga Rao 
*Subbaraya Sharma  Jenny
*Rohini Hattangadi
*Kinnera
*Rajeswari 
*Archana 
*Radha Prashanthi 
*Krishnaveni 
*Master Baladitya
 

==Soundtrack==
{{Infobox album
| Name        = Top Hero
| Tagline     = 
| Type        = film
| Artist      = S. V. Krishna Reddy
| Cover       = 
| Released    = 1994
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:06
| Label       = Aakash Audio
| Producer    = S. V. Krishna Reddy
| Reviews     =
| Last album  = Shubhalagnam   (1994)  
| This album  = Top Hero   (1994)
| Next album  = Ghatotkachudu   (1995)
}}

Music composed by S. V. Krishna Reddy. All songs are hit tracks. Music released on Aakash Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length =25:06
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = O Muddu Paapa
| lyrics1 = Jonnavithhula Ramalingeswara Rao SP Balu
| length1 = 4:48

| title2  = Samaja Varagamana
| lyrics2 = Jonnavithhula Ramalingeswara Rao Chitra
| length2 = 5:01

| title3  = Beedilu Taagandi Sirivennela Sitarama Sastry
| extra3  = SP Balu,Chitra
| length3 = 4:43

| title4  = Bhaama Nee Cheera 
| lyrics4 = Jonnavithhula Ramalingeswara Rao
| extra4  = SP Balu,Chitra 
| length4 = 4:31

| title5  = Jaamu Raathiri
| lyrics5 = Jonnavithhula Ramalingeswara Rao
| extra5  = SP Balu,Chitra
| length5 = 4:54

| title6  = Okkasaari Okkasari  
| lyrics6 = Sirivennela Sitarama Sastry  
| extra6  = SP Balu,Chitra
| length6 = 5:09
}}

==Others== Hyderabad

==References==
 

==External links==
*  

 

 
 
 


 