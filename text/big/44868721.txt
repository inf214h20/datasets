Wild Life (2014 film)
{{Infobox film
| name           = Wild Life
| image          = 
| caption        =
| director       = Cédric Kahn
| producer       = Kristina Larsen   Jean-Pierre Dardenne   Luc Dardenne   Delphine Tomson 
| screenplay     = Cédric Kahn   Nathalie Najem  
| based on       =  
| starring       = Mathieu Kassovitz   Céline Sallette 
| music          = Mathias Duplessy 	
| cinematography = Yves Cape 	 
| editing        = Simon Jacquet 
| studio         = Les Films du Lendemain   Les Films du Fleuve   France 2 Cinéma   Belgacom
| distributor    = Le Pacte
| released       =  
| runtime        = 106 minutes
| country        = France Belgium 
| language       = French
| budget         = 
| gross          = 
}}

Wild Life ( ) is a 2014 French-Belgian drama film directed by Cédric Kahn and adapted from the 2010 book Hors système, onze ans sous létoile de la liberté by Xavier Fortin, Okwari Fortin, ShahiYena Fortin and Laurence Vidal.  The film won the Special Jury Prize at the 62nd San Sebastián International Film Festival.  In January 2015, the film received three nominations at the 20th Lumières Awards. 

== Cast ==
* Mathieu Kassovitz as Paco (Philippe Fournier)
* Céline Sallette as Nora (Carole Garcia)
* David Gastou as Tsali Fournier (9 year-old)
* Sofiane Neveu as Okyesa Fournier (8 year-old)
* Romain Depret as Tsali Fournier (teenager)
* Jules Ritmanic as Okyesa Fournier (teenager)
* Jenna Thiam as Céline 
* Tara-Jay Bangalter as Thomas (11 year-old)
* Amandine Dugas as Clara 
* Michaël Dichter as Gaspard 
* Brigitte Sy as Geneviève 
* Olivier Granier as Noras father
* Dominique Bes as Noras mother
* Julien Thiou as Clovis 
* Judith Simon as Dom

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 