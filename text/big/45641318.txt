The Boxcar Children (film)
{{Infobox film
| name                 = The Boxcar Children
| image                = 
| alt                  = 
| caption              = 
| director             = Daniel Chuba, Mark A.Z. Dippe and kyungho Jo
| producer             = {{Plain list|
* Daniel Chuba
* Mark A.Z. Dippe
}}
| screenplay           = {{Plain list|
* Justin Marz
* Zack Strauss
}}
| story                = {{Plain list|
* Gertrude Chandler Warner
}}
| starring             = {{Plain list|
* Illeana Douglas
* Mackenzie Foy
* Zachary Gordon
* Joey King
* Jadon Sand
* Martin Sheen
* J. K. Simmons
* D.B. Sweeney
* Audrey Wasilewski
}}
| music                = Kenneth Burgomaster
| editing              = {{Plain list|
* Michael Rafferty
}}
| production companies = {{Plain list|
* Hammerhead Productions
* Village Roadshow Pictures
* Warner Brothers
}}
| distributor          = Phase 4 Films
| released             =  
| runtime              = 86 minutes
| country              = United States
| language             = English
| budget               = $5 million
| gross                = 
}}

The Boxcar Children is a 2014 animated film based on The Boxcar Children by Gertrude Chandler Warner

==Plot==

For a detailed plot see The Boxcar Children

==Voice Cast==
* Illeana Douglas as Mary
* Mackenzie Foy as Violet
* Zachary Gordon as Henry
* Joey King as Jessie
* Jadon Sand as Benny
* Martin Sheen as Grandfather Alden
* J. K. Simmons as Dr. Moore
* D.B. Sweeney as the Baker
* Audrey Wasilewski as the Bakers wife

==Links==
*  

 


 