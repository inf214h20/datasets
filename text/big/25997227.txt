Love Is Better Than Ever
{{Infobox film
| name           = Love Is Better Than Ever
| image          = Love is better than ever ver2.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = Stanley Donen
| writer         = Ruth Brooks Flippen
| starring       = Larry Parks Elizabeth Taylor
| music          = Lennie Hayton
| cinematography = Harold Rosson
| editing        = George Boemler
| studio         = Metro-Goldwyn-Mayer
| distributor    = 
| released       =   
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $941,000  . 
| gross          = $974,000 
}}

Love Is Better Than Ever is a 1952 American romantic comedy film directed by Stanley Donen from a screenplay by Ruth Brooks Flippen, starring Larry Parks and Elizabeth Taylor. The plot concerns a small-town girl who falls in love with a big-city talent agent. 

==Plot==
Confirmed bachelor Jud Parker likes his life the way it is. A talent agent, he goes to New Haven, Connecticut on a clients behalf and meets Anastacia "Stacie" Macaboy, who owns a dance school.

Stacie then runs into him in New York when she goes to a convention. Jud takes her to a New York Giants baseball game and to dinner and dancing. Stacie falls in love, but Jud is furious when a story in the New Haven paper claims they are engaged.

Mrs. Levoy and her daughter, who run a rival dance school, sully Stacies reputation and cause students to drop out. Stacie and Jud disagree on how to explain their relationship until Stacie ultimately bets everything on the outcome of the Giants next game.

==Cast==
*Larry Parks as Jud Parker
*Elizabeth Taylor as Anastacia (Stacie) Macaboy
*Josephine Hutchinson as Mrs. Macaboy
*Tom Tully as Mr. Charles E. Macaboy
*Ann Doran as Mrs. Levoy
*Elinor Donahue as Pattie Marie Levoy
*Kathleen Freeman as Mrs. Kahrney
*Doreen McCann as Albertina Kahrney
*Alex Gerry as Hamlet (Smitties regular)
*Dick Wessel as Smitty - cafe owner
*John Handley as Johnny - Little boy dancer who plays the "grape"
==Box Office==
According to MGM records the film earned $634,000 in the US and $340,000 elsewhere resulting in a loss of $362,000. 
==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 