Den store barnedåpen
{{Infobox film
| name = Den store barnedåpen
| image =
| image size =
| caption =
| director = Tancred Ibsen
| producer =
| writer = Oskar Braaten (play)   Tancred Ibsen
| narrator =
| starring = Einar Sissener   Aase Bye   Agnete Schibsted-Hansson
| music =
| cinematography =
| editing =
| distributor =
| released = 1931
| runtime = 103 minutes
| country = Norway
| language = Norwegian
| budget =
| gross =
| preceded by =
| followed by =
}}
 Norwegian comedy film directed by Tancred Ibsen, starring Einar Sissener, Aase Bye and Agnete Schibsted-Hansson. It was the first feature-length Norwegian sound film. The lacklustre Harald (Sissener) is taken in by Alvilde (Bye), and charged with looking after her bastard child. It soon dawns on both of them that Harald has more potential than previously assumed.

==External links==
*  
*  
*   at the Norwegian Film Institute

 
 
 
 


 
 