New Kids Turbo
 
 
 
 
}}
{{Infobox film
| name           = New Kids Turbo
| image          = 
| image size     = 
| border         = 
| alt            = 
| director       = Steffen Haars Flip van der Kuil
| producer       = Eyeworks Comedy Central
| writer         = Steffen Haars Flip van der Kuil
| screenplay     = 
| story          = 
| based on       =
| narrator       = 
| starring       = Huub Smit Tim Haars Wesley van Gaalen Steffen Haars Flip van der Kuil Nicole van Nierop
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 82 minutes
| country        = Netherlands
| language       = Dutch
| budget         = € 1.500.000
| gross          = € 7.500.000
}}

New Kids Turbo, sometimes promoted as New Kids: Turbo!, is a Dutch film based on the television series New Kids. The film premiered on 6 December 2010 and was released in all Dutch cinemas on 9 December. It broke the Dutch record for receiving the highest audience figures on the opening evening. It is Comedy Centrals first film to be produced outside the United States and also Paramount Pictures first film to be produced by Comedy Central in the Netherlands.

==Plot==
Set in the small village of   to work with him, who in turn gnaws at and breaks the shovels of Richards supervisor, and Rikkert is sacked by his boss, a sarcastic and abusive garage owner.

Gerrie is kicked out by his mother, after she catches him trying to steal from her purse. At around the same time, Rikkert breaks up with Manuela, his girlfriend, and Barries house (which is also an illegal cannabis farm) catches fire. Subsequently all three take refuge in the home of Richard and Robbie. The group go shopping and rely on Richard to pay, however his bank card is declined following the refuelling of Rikkerts Opel Manta. The group travel together to the local employment and benefits office and demand more benefits from the official, who politely refuses. Richard climbs over the desk and thrusts the official against the wall, telling him to "Give us more money, kut". The following morning, Richard reads a letter sent from the benefits office and informs the group that as a whole their benefits have ceased entirely. The result of which is that the group decides that they simply will no longer pay for anything and go on a spree of petty robbery, most notably from their local petrol filling station, as the group run from the forecourt each carrying two large capacity containers full of fuel, chased by the petrol station attendant, similarly the group flee the local supermarket "Lupus" each carrying large quantities of lager. Gerrie steals deep-fried croquette snacks from the local takeaway whilst Barrie acts as a diversion. Richard, Robbie and Rikkert phone a delivering Chinese takeaway, but explain they have no money to pay the delivery rider when he comes to deliver.

With their level of criminality rising, more and more letters and final demands come through Richards letterbox, followed by a visit from a debt collector, firstly deceived by a local Down syndrome sufferer - who is duped into opening the front door by Richard and the group - who confuses the collector by uttering the words "Lorry driver", before slamming the door closed again. He is paid with a slice of ham, which he is led to believe is a ten-Euro note. The collector rings the doorbell again and this time is met by Richard. He explains there are serious concerns, before Richard reiterates that they are paying for nothing anymore. The debt collector starts to explain that that is not going to work, before being promptly punched in the nose and falling to the ground. He returns shortly afterwards, flanked by the local Police chief who is additionally punched by Richard.

Their actions draw the attention of a popular Brabant-based journalist, who makes a report on group for TV Brabant. They are presented in a sympathetic light and are described as victims with no focus being placed upon the severity of their illegal actions. The report sparks a series of imitation riots in other towns and cities across The Netherlands.

In the full swing of the crisis, Gerrie takes Barrie by bus to go to an old friend of his, Peter Aerts, aka "The Lumberjack", who is now a  professional kickboxer to borrow a large sum of money. With this money he goes to a casino, where he places the entire loan on a single number at the Roulette table and wins. The entire group join him and although are unnaware of the origin of such a large amount of cash, are thrilled to have won against the odds. Gerrie however bets it all again, loses everything and promptly vanishes from the casino before the rest of the group discover what he has done. Then Rickert loses his Opel Manta as it gets picked up and towed away by the local authorities. Despite chasing it for a short distance with a small childs bicycle, the car is lost (except for a wheel that was secure to the kerbstone in an effort to prevent it from being removed.)

In response to the rising levels of civil unrest throughout the region, the Ministry of Defence in The Hague decides to wipe Maaskantje off the map in an attempt to stop the riots; however, the plan goes wrong when the neighboring town of Schijndel is accidentally bombed by mistake. The government explain the accident as a rogue Belgian missile experiencing technical problems and assures the population that the Netherlands and Belgium are not at war. When they see it, the boys know the situation is critical.

Walking back to Maaskantje they are apparently ambushed by multiple bursts of automatic weapon fire. Barrie is shot in the chest, but is apparently unharmed. The source of the gunfire is, however, Gerrie - who had met a farmer who had kept some weapons in his barn from World War II. He shows the group his cache of antique weaponry and invites them to take their pick. Whilst singing the Nazi anthem, he is accidentally shot in the head by Gerry, than Rikkert laughs and says: "more weapons per man."

Maaskantje is evacuated by the local residents, whilst the group make their way back into town, rendezvousing with their friends and colleagues. The five men team with Manuela, the owner of the local takeaway and the recently sacked local police chief. The Chinese restaurant delivery rider arrives and explains that he too wishes to help, before being one of the first casualties of the incoming troops. The troops are held back by bazooka, machine gun and rifle fire from the group and their friends.

 
The takeaway owner is cornered by a sole officer and was saved by a single shot to the back of the officers head by Gerrie. The cafetaria owner exclaims that he could not be more proud of his son. Gerrie is taken back, but accepts a hug from his long-lost father.
 DAF curtainsider lorry careering through his troops. The lorry comes to a crashing halt off-screen and the curtain splits open, spilling the load of lager. The driver is revealed to be the Down syndrome sufferer.

The Local police chief re-arms himself and promptly shoots the commander in the head.

The group are subsequently arrested and charged with a laundry-list of offences. The punishment is passed at 250 hours Community Service each. Whilst the five are carrying out the community service, "The Lumberjack" returns and demands repayment. He is about to launch into an attack on the group when he is crushed by a falling set of speakers and turntable deck, accompanied by DJ Paul Elstak.

The tagline of the film is Niemand komt aan Maaskantje! (No one messes with Maaskantje!). In German this is Niemand fickt mit Maaskantje!. (No-one fucks with Maaskantje!)

==Trivia==
A supermarket called LUPUS is shown in the film. This is actually a parody of the Dutch supermarket chain PLUS, which bears a similar logo.

The character with Down syndrome references a character in Dodeskaden, a movie by Akira Kurosawa with poverty as central theme.

==External links==
*    
*  
*  

 
 
 
 
 