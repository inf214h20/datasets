Aranyaka (film)
{{Infobox film
| name           = Aranyaka
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Apurba Kishore Bir NFDC Doordarshan
| writer         = Apurba Kishore Bir
| screenplay     = 
| story          = 
| based on       =   
| narrator       = 
| starring       = Sarat Pujari Navni Parihar Sanjana Kapoor Mohan Gokhale
| music          = Bhavdeep Jaipurwalle
| cinematography =Apurba Kishore Bir
| editing        = Dilip Panda
| studio         = 
| distributor    = 
| released       = 1994 
| runtime        = 84 min.
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 1994 Indian Hindi drama film directed by Apurba Kishore Bir.    The film stars Sarat Pujari, Navni Parihar, Sanjana Kapoor and Mohan Gokhale in lead roles. Based on a short story Aranyaka by Manoj Das, the film is set in rural Orissa, where a formal local ruler organizes a hunt for his invited guests, which goes wrong. The film highlights the clash between ruling class and indigenous people of the region. 

==Cast==
* Sarat Pujari as Raja Saheb
* Mohan Gokhale as Mr. Mitty
* Lalatendu Rath as Major
* Sanjana Kapoor as Elina
* Navni Parihar as Mrs. Mitty
* Sunil Sing as Shyamal
* Subrata Mahapatra as Chowkidar
* Yagnaseni Jena as Tribal girl
* Ajay Nath as Tribal boy
* Pritikrushna Mahanty as Tribal boy
* Ashim Basu as Mad beggar

== References ==
 

==External links==
*  
* 

 
 
 
 
 
 
 


 