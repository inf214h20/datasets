The Last Mountain
 
{{Infobox film
| name           = The Last Mountain
| image          = 
| alt            =  
| caption        = 
| director       = Bill Haney
| producer       = Clara Bingham Eric Grunebaum Bill Haney
| writer         = Bill Haney Peter Rhodes
| narrator       = William Sadler
| starring       = Robert F. Kennedy, Jr. Maria Gunnoe Bo Webb Ed Wiley Jennifer Hall-Massey
| music          = Claudio Ragazzi
| cinematography = Tim Hotchner Stephen McCarthy Jerry Risius
| editing        = Peter Rhodes
| studio         = 
| distributor    = Dada Films Uncommon Productions
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Last Mountain is a feature-length documentary film directed by Bill Haney and produced by Haney, Clara Bingham and Eric Grunebaum. The film premiered at the 2011 Sundance Film Festival and went into general release on  June 3, 2011. The film explores the consequences of mining and burning coal, with a particular focus on the use of a method for coal strip-mining in Appalachia commonly known as mountaintop removal mining.
 Crimes Against Nature and featuring Kennedy and a cast of activists and experts, the film considers the health consequences of mining and burning coal and looks at the context and history of environmental laws in the United States. Exploring a proposal to build a wind farm on a mountain in the heart of "coal country," rather than deforesting and demolishing the mountain for the coal seams within, the film suggests that wind resources are plentiful in the U.S., would provide many domestic jobs and that wind is a more benign source of power than coal and has the potential to eliminate the destructive aspects of coal.

==Story==
The Last Mountain tells the story of the fight for Coal River Mountain in West Virginia, where community members and environmental activists are pitted against a coal company in the struggle to save one of the last large mountain ranges in the area from mountaintop removal.

Massey Energy (purchased by Alpha Natural Resources in 2011) has many of the necessary permits and plans to blow up the mountain and fill the nearby valleys and streams with the resulting rubble, but the activists would like to build a wind farm on the mountain’s ridges instead. They have even commissioned a study demonstrating that Coal River Mountain has a high wind potential – high enough to produce 328 megawatts of electricity, which can power 70,000 homes.

Both approaches – a wind farm or a coal strip mine – would produce needed electricity, however the results are very different. Massey and the coal industry maintain that they are creating jobs and would reclaim the mountain, while the activists present a very different picture and claim that with mountaintop removal mining the land cannot be properly reclaimed, that the practice destroys their air and water, produces fewer jobs than traditional underground mining. While the locals work these jobs and worry constantly if they will have a job the following day.

The film is told from the point of view of environmental litigator and activist   from demolishing the mountain.

The film highlights statistics on mining including the fact that five hundred of Appalachia’s mountains have been flattened by mountaintop removal coal mining while being rebuilt by land reclamation projects and   of streams that only have water in them during hard rains moved to the sides of the valley fills. Experts including Alan Hershkowitz, Senior Scientist at the Natural Resources Defense Council, former dean of the Yale School of Forestry & the Environment Gus Speth, and Vanity Fair (magazine)|Vanity Fair writer and Coal River author Michael Shnayerson, put the story in political and environmental context. Scientists including Ben Stout III and Devra Davis provide additional explanation on the dangers of the toxins which are produced by coal mining, coal processing and coal burning.
 burning coal, 30% of that coal comes from Appalachia and burning coal is the number one contributor to greenhouse gas emissions worldwide. The film maintains that huge amounts of money are poured into the political system from coal mining, coal transportation and coal burning industries and that in the face of this, the resulting political system is not fully engaged in the regulation of these activities, and that the democratic system itself is compromised.

==Characters==
The Last Mountain begins by introducing local activist Maria Gunnoe, who describes how the hills surrounding her home in the town of Bob White, WV have been "turned inside out" and demolished, stripping away forest cover and topsoil and leaving less permeable rock in its place. She argues that this is the cause of flash floods in the community, and one flood in particular that nearly washed her and her family away. She also describes how her and her friends work has caused people to move out because of the fewer jobs available in the community.

The film takes viewers to the town of Prenter, West Virginia, in the Coal River Valley where a citizen-turned-activist Jennifer Hall-Massey explains that six of her immediate neighbors have died of brain tumors and the only thing they all have in common is well water. As reported by the NY Times, Hall-Massey along with 264 of her neighbors, are suing the local coal companies and state that the companies have pumped millions of gallons of coal slurry into the ground surrounding the town of Prenter, polluting their well water with heavy metals like arsenic and lead, and causing disease.

Another citizen activist, Ed Wiley, a former mountaintop removal coal miner himself, who lives in the Coal River Valley, is trying to move his granddaughters school, Marsh Fork Elementary, to a safer location. The school sits several hundred feet below an earthen dam holding back a slurry pond with   of coal waste discharged by an industrial coal preparation plant next door to the school. Wiley’s granddaughter told him that the coal was making the children sick, driving Wiley to take his protest to then Governor Joe Manchin (now a U.S. Senator from West Virginia), and demand help from the state. Manchin and other West Virginia politicians argue that the state’s economy and its jobs depend on coal mining. Manchin calls himself "friend of coal". But the film presents statistics showing that over the last 30 years while coal production has increased 140% due to the use of massive mining equipment and explosives, the number of jobs has decreased by 65%, due to the use of technology. While members of the coal industry maintain that environmental regulations take away jobs, others in the film suggest that the use of machines and explosives – in particular in the pursuit of strip mining – has been the cause of job losses in recent decades.

The film presents Don Blankenship as an antagonist. The ex-CEO of Massey Energy does not believe in climate change, believes that environmentalists are "extremists", and continues by saying, "they are making American labor the real endangered species as they tell us that their goal is to save the planet." The film presents statistics asserting that between 2000 and 2006, Massey was cited for 60,000 environmental violations by the federal Environmental Protection Agency, but given a fine of less than 1% of what the regulations call for – and then continued to violate environmental laws 8,500 more times in the subsequent months.

Meanwhile, activists like Bo Webb and Lorelei Scarbro, who live in the shadow of Coal River Mountain, have organized a group of citizens to propose that instead of destroying the mountain, a sustainable wind farm be built on the mountain’s ridges instead – an energy project that could power 70,000 homes with renewable energy virtually forever. But Massey Energy and now Alpha Natural Resources has a permit to destroy over   of the mountain instead. Over time, the wind farm would provide more jobs to the community, and on day-one it would pay more taxes to the county than the coal strip mine, Scarbro explains. Members of Climate Ground Zero pull off a tree-sit in the blasting zone of Coal River Mountain one winter night and sit in protest for nine days until they are arrested for attempting to halt the destruction of this symbolic mountain.

While Robert F. Kennedy, Jr. maintains that "The United States is the Saudi Arabia of wind," The Last Mountain concludes by exploring small scale wind projects on the coast of Rhode Island which have paid for themselves within a few years. The film ends with the fate of Coal River Mountain still in the balance and Maria Gunnoes assertion that, "Coal is mean. Coal is cruel and coal kills. And the American people need to find their position where that is. Youre connected to coal, whether you realize it or not. Everybodys connected to this. And everybodys causing it, and everybodys allowing it."

==Criticism==

Green For All writer Eric Mathis called the wind study presented in the movie "misleading", and said the movie paradoxically maintains the "destructive mechanism" that it opposes. 
     
Ken Ward noted that "The movie has problems, though. A bit of the effort to tell the story with Bobby Kennedy as the central character seems overly forced. Also, to fit into their narrative, the filmmakers ignore the fact that a major rule making change that helped mountaintop removal continue was started by the Clinton administration, not by George W. Bush.” 

==Reception==
 SoHo House in Manhattan, NY and  moderated a Q&A with producer  . 

==See also== Mountaintop Removal
* The Price of Sugar, a 2007 documentary by Haney

==References==
 

==External links==
*  
*  
*   at the Sundance Film Festival
*  

 
 
 
 
 
 
 
 
 
 