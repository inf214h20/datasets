Bride of Frankenstein
  
{{Infobox film
| name           = Bride of Frankenstein
| alt            = Movie poster with the head of Frankensteins monster at the center, looking forward with a somber expression. Elevated above him is a woman looking down towards the center of the image. Near the bottom of the image is the Bride of Frankenstein, looking off to the right of the image as her hair surrounds the head of Frankensteins monster and the body of the woman. Text at the top of the image states "Warning! The Monster Demands a Mate!" The bottom of the image includes the films title and credits.
| image          = Brideoffrankposter.jpg
| caption        = theatrical release poster
| image_size     = 200px
| director       = James Whale
| producer       = Carl Laemmle, Jr.
| writer         = Screenplay: William Hurlbut Adaptation: William Hurlbut John L. Balderston
| based on       =  
| starring       = Boris Karloff
| music          = Franz Waxman
| cinematography = John J. Mescall
| editing        = Ted Kent
| studio         = Universal Pictures Roxy Theatre. 
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = $397,000 
| gross          = 
}}
 The Monster, Henry Frankenstein, and Ernest Thesiger as Doctor Septimus Pretorius.
 mate for him.

Preparation began shortly after the first film premiered, but script problems delayed the project. Principal photography started in January 1935, with creative personnel from the original returning in front of and behind the camera. Bride of Frankenstein was released to critical and popular acclaim, although it encountered difficulties with some state and national censorship boards. Since its release the films reputation has grown, and it is hailed as Whales masterpiece. 

==Plot== Douglas Walton) Lord Byron (Gavin Gordon) praise Mary Shelley (Elsa Lanchester) for her story of Frankenstein and his Monster. Reminding them that her intention was to impart a moral lesson, Mary says she has more of the story to tell. The scene shifts to the end of the 1931 Frankenstein.
 Henry Frankenstein Mary Gordon) Una OConnor), who flees in terror.
 homunculi he has created, including a miniature Queen regnant|queen, Monarch|king, archbishop, devil, ballerina, and mermaid. Pretorius wishes to work with Henry to create a mate for the Monster and offers a toast to their venture: "To a new world of gods and monsters!" Upon forcing Henry to help him, Pretorius will grow an artificial brain while Henry gathers the parts for the mate.

The Monster saves a young shepherdess (Anne Darling) from drowning. Her screams upon seeing him alert two hunters, who shoot and injure the creature. The hunters raise a mob that sets out in pursuit. Captured and trussed to a pole, the Monster is hauled to a dungeon and chained. Left alone, he breaks his chains and escapes.
 Ave Maria", the Monster encounters an old blind hermit (O. P. Heggie) who thanks God for sending him a friend. He teaches the monster words like "friend" and "good" and shares a meal with him. Two lost hunters stumble upon the cottage and recognize the Monster. He attacks them and accidentally burns down the cottage as the hunters lead the hermit away.

Taking refuge from another angry mob in a crypt, the Monster spies Pretorius and his cronies Karl (Dwight Frye) and Ludwig (Ted Billings) breaking open a grave. The henchmen depart as Pretorius stays to enjoy a light supper. The Monster approaches Pretorius, and learns that Pretorius plans to create a mate for him.

Henry and Elizabeth, now married, are visited by Pretorius. He is ready for Henry to do his part in their "grand collaboration". Henry refuses and Pretorius calls in the Monster who demands Henrys help. Henry again refuses and Pretorius orders the Monster out, secretly signaling him to kidnap Elizabeth. Pretorius guarantees her safe return upon Henrys participation. Henry returns to his tower laboratory where in spite of himself he grows excited over his work. After being assured of Elizabeths safety, Henry completes the Bride of Frankenstein (character)|Brides body.

A storm rages as final preparations are made to bring the Bride to life. Her bandage-wrapped body is raised through the roof. Lightning strikes a kite, sending electricity through the Bride. Henry and Pretorius lower her and realize their success. "Shes alive! Alive!" Henry cries. They remove her bandages and help her to stand. "The bride of Frankenstein!" Doctor Pretorius declares.

The excited Monster sees his mate (Elsa Lanchester) and reaches out to her, asking, "Friend?" The Bride, screaming, rejects him. "She hate me! Like others" the Monster dejectedly says. As Elizabeth races to Henrys side, the Monster rampages through the laboratory. The Monster tells Henry and Elizabeth "Yes! Go! You live!" To Pretorius and the Bride, he says "You stay. We belong dead." While Henry and Elizabeth flee, the Monster sheds a tear and pulls a lever to trigger the destruction of the laboratory and tower.

==Cast==
  
*Boris Karloff as The Monster
*Colin Clive as Henry Frankenstein
*Valerie Hobson as Elizabeth Lavenza
*Ernest Thesiger as Doctor Pretorius The Monsters Bride  Gavin Gordon as Lord Byron Douglas Walton as Percy Bysshe Shelley Una OConnor as Minnie
  Burgomaster
*Lucien Prival as the butler
*O.P. Heggie as Hermit
*Dwight Frye as Karl
*Reginald Barlow as Hans Mary Gordon as Hans wife
*Anne Darling as the shepherdess
*Walter Brennan as a peasant (unbilled)
 

==Production==
  as Frankensteins monster in Bride of Frankenstein (1935)]]
 The Invisible Man, producer Carl Laemmle, Jr. realized that Whale was the only possible director for Bride; Whale took advantage of the situation in persuading the studio to let him make One More River.  Whale believed the sequel would not top the original, so he decided instead to make it a memorable "hoot".  According to a studio publicist, Whale and Universals studio psychiatrist decided "the Monster would have the mental age of a ten-year old boy and the emotional age of a lad of fifteen". 
 treatment entitled The New Adventures of Frankenstein&nbsp;— The Monster Lives!, but it was rejected without comment early in 1932. {{cite video people     = MacQueen, Scott date      = 2004 title      = DVD commentary, Bride of Frankenstein Legacy Collection edition medium     = DVD publisher  = Universal Studios Hays office for review. The script passed its review, but Whale, who by then had been contracted to direct, complained that "it stinks to heaven".  Lawrence Blochman|L. G. Blochman and Philip MacDonald were the next writers assigned, but Whale also found their work unsatisfactory. In 1934, Whale set John L. Balderston to work on yet another version, and it was he who returned to an incident from the novel in which the creature demands a mate. In the novel Frankenstein creates a mate, but destroys it without bringing it to life. Balderston also created the Mary Shelley prologue. After several months Whale was still not satisfied with Balderstons work and handed the project to playwright William J. Hurlbut  and Edmund Pearson. The final script, combining elements of a number of these versions, was submitted for Hays office review in November 1934.  Kim Newman reports that Whale planned to make Elizabeth the heart donor for the bride,    but film historian Scott MacQueen states that Whale never had such an intention. 

Sources report that Bela Lugosi and Claude Rains were considered, with varying degrees of seriousness, for the role of Frankensteins mentor, Pretorius;  others report that the role was created specifically for Ernest Thesiger. Skal, p. 185  Because of Mae Clarkes ill health, Valerie Hobson replaced her as Henry Frankensteins love interest, Elizabeth.  Early in production, Whale decided that the same actress cast to play the Bride should also play Mary Shelley in the films prologue, to represent how the story&nbsp;— and horror in general&nbsp;— springs from the dark side of the imagination. Vieira, p. 82  He considered Brigitte Helm and Phyllis Brooks before deciding on Elsa Lanchester. Lanchester, who had accompanied husband Charles Laughton to Hollywood, had met with only moderate success while Laughton had made a strong impact with several films including The Private Life of Henry VIII (for which he had won an Academy Award|Oscar) and Whales own The Old Dark House. Lanchester had returned alone to London when Whale contacted her to offer her the dual role. Curtis, pp. 243–44  Lanchester modeled the Brides hissing on the hissing of swans. She gave herself a sore throat while filming the hissing sequence, which Whale shot from multiple angles. Vieira, p. 86 

Colin Clive and Boris Karloff reprised their roles from Frankenstein as creator and creation, respectively. Hobson recalled Clives alcoholism had worsened since filming the original, but Whale did not recast the role because his "hysterical quality" was necessary for the film.  Karloff strongly objected to the decision to allow the Monster to speak. "Speech! Stupid! My argument was that if the monster had any impact or charm, it was because he was inarticulate&nbsp;– this great, lumbering, inarticulate creature. The moment he spoke you might as well&nbsp;... play it straight." Gifford, p. 55  This decision also meant that Karloff could not remove his dental plate, so now his cheeks did not have the sunken look of the original film.  Whale and the studio psychiatrist selected 44 simple words for the Monsters vocabulary by looking at test papers of ten-year olds working at the studio.  Dwight Frye returned to play the doctors assistant, Karl, having played the hunchback Fritz in the original. Frye also filmed a scene as an unnamed villager and the role of "Nephew Glutz", a man who murdered his uncle and blamed the death on the Monster.  Boris Karloff is credited simply as KARLOFF, which was Universals custom during the height of his career.  Elsa Lanchester is credited for Mary Wollstonecraft Shelley, but in a nod to the earlier film, the Monsters bride is credited only as "?" just as Boris Karloff had been in the opening credits of Frankenstein.

 
 Jack Pierce iconic hair Marcel wave over a wire frame to achieve the style.  Lanchester disliked working with Pierce, who she said "really did feel that he made these people, like he was a god&nbsp;... in the morning hed be dressed in white as if he were in hospital to perform an operation." Vieira, p. 85  To play Mary Shelley, Lanchester wore a white net dress embroidered with sequins of butterflies, stars, and moons, which the actress had heard required 17 women 12 weeks to make. 
 stock scene, matted onto the rear plate. Diminutive actor Billy Barty is briefly visible from the back in the finished film as a homunculus infant in a high chair, but Whale cut the infants reveal before the films release. 

Whale met Franz Waxman at a party and asked him to score the picture. "Nothing will be resolved in this picture except the end destruction scene. Would you write an unresolved score for it?" asked Whale.  Waxman created three distinctive themes: one for the Monster; one for the Bride; and one for Pretorius. The score closes, at Whales suggestion, with a powerful dissonant chord, intended to convey the idea that the on-screen explosion was so powerful that the theater where the film was being screened was affected by it.  Constantin Bakaleinikoff conducted 22 musicians to record the score in a single nine-hour session. 

Shooting began on January 2, 1935 Mank, p. xvii  with a projected budget of US$293,750 ($ |0}} as of  )&nbsp;– almost exactly the budget of the original&nbsp;– and an estimated 36-day shooting schedule.   On the first day, Karloff waded in the water below the destroyed windmill wearing a rubber suit under his costume. Air got into the suit and expanded it like an "obscene water lilly".  Later that day, Karloff broke his hip, necessitating a stunt double.  Clive had also broken his leg.  Shooting was completed on March 7, 1935. The film was ten days over schedule because Whale shut down the picture for ten days until Heggie became available to play the Hermit.  With a final cost of $397,023 ($ |0}} as of  ), Bride was more than $100,000 ($ |0}} as of  ) over budget.   As originally filmed, Henry died fleeing the exploding castle. Whale re-shot the ending to allow for their survival, although Clive is still visible on-screen in the collapsing laboratory.  Whale completed his final cut, shortening the running time from about 90 to 75 minutes and re-shooting and re-editing the ending, only days before the films scheduled premiere date. 

==Censorship== Hays office and following its release by local and national censorship boards. Joseph Breen, lead censor for the Hays office, objected to lines of dialogue in the originally submitted script in which Henry Frankenstein and his work were compared to that of God. He continued to object to such dialogue in revised scripts, Skal, pp. 187–91  and to a planned shot of the Monster rushing through a graveyard to a figure of a crucified Jesus and attempting to "rescue" the figure from the cross.  Breen also objected to the number of murders, both seen and implied by the script and strongly advised Whale to reduce the number.  The censors office, upon reviewing the film in March 1935, required a number of cuts. Whale agreed to delete a sequence in which Dwight Fryes "Nephew Glutz"  kills his uncle and blames the Monster, Curtis, p. 250  and shots of Elsa Lanchester as Mary Shelley in which Breen felt too much of her breasts were visible. Curiously, despite his earlier objection, Breen offered no objection to the cruciform imagery throughout the film&nbsp;– including a scene with the Monster lashed Christ-like to a pole&nbsp;– nor to the presentation of Pretorius as a coded homosexual.  Bride of Frankenstein was approved by the Production Code office on April 15, 1935. 
 Henry VIII with tweezers constituted "making a fool out of a king". 

==Reception==
 
Bride of Frankenstein was profitable for Universal, with a 1943 report showing that the film had by then earned approximately $2 million ($ |0}} as of  ) for the studio, a profit of about $950,000 ($ |0}} as of  ).   The film was critically praised upon its release, although some reviewers did qualify their opinions based on the films being in the horror genre. The New York World-Telegram called the film "good entertainment of its kind".  The New York Post described it as "a grotesque, gruesome tale which, of its kind, is swell".  The Hollywood Reporter similarly called the film "a joy for those who can appreciate it". Curtis, pp. 250–51 

Variety (magazine)|Variety did not so qualify its review. "  one of those rare instances where none can review it, or talk about it, without mentioning the cameraman, art director, and score composer in the same breath as the actors and director." Variety also praised the cast, writing that "Karloff manages to invest the character with some subtleties of emotion that are surprisingly real and touching&nbsp;... Thesiger as Dr Pretorious   a diabolic characterization if ever there was one&nbsp;... Lanchester handles two assignments, being first in a preamble as author Mary Shelley and then the created woman. In latter assignment she impresses quite highly."   

In another unqualified review, Time (magazine)|Time wrote that the film had "a vitality that makes their efforts fully the equal of the original picture&nbsp;... Screenwriters Hurlbut & Balderston and Director James Whale have given it the macabre intensity proper to all good horror pieces, but have substituted a queer kind of mechanistic pathos for the sheer evil that was Frankenstein."  The Oakland Tribune concurred it was "a fantasy produced on a rather magnificent scale, with excellent stagecraft and fine photographic effects".  While the Winnipeg Free Press thought that the electrical equipment might have been better suited to Buck Rogers, nonetheless the reviewer praised the film as "exciting and sometimes morbidly gruesome", declaring that "All who enjoyed Frankenstein will welcome his Bride as a worthy successor."  The New York Times called Karloff "so splendid in the role that all one can say is he is the Monster.  {{cite news
| last = F.S.N.
| title = Bride of Frankenstein At the Roxy
| work = The New York Times
| date = May 11, 1935
|quote=Mr. Karloff is so splendid in the role that all one can say is "he is the Monster." Mr. Clive, Valerie Hobson, Elsa Lanchester, O. P. Heggie, Ernest Thesiger, E. E. Clive, and Una OConnor fit snugly into the human background before which Karloff moves. ...
| url = http://movies.nytimes.com/movie/review?res=9807EEDA1139E33ABC4952DFB366838E629EDE&scp=5&sq=bride%20of%20frankenstein&st=cse
| accessdate =February 1, 2009}}  The Times praised the entire principal cast and Whales direction in concluding that Bride is "a first-rate horror film",  and presciently suggested that "The Monster should become an institution, like Charlie Chan."  Bride was nominated for one Academy Award, for Best Sound Recording (Gilbert Kurland).    

The films reputation has persisted and grown since its release. In 1998, the film was added to the United States National Film Registry, having been deemed "culturally, historically or aesthetically significant".   Frequently identified as James Whales masterpiece,  the film is lauded as "the finest of all gothic horror movies".  Time rated Bride of Frankenstein in its "Time magazines "All-TIME" 100 best movies|ALL-TIME 100 Movies", in which critics Richard Corliss and Richard Schickel overruled the magazines original review to declare the film "one of those rare sequels that is infinitely superior to its source".  In 2008, Bride was selected by Empire Magazine|Empire magazine as one of The 500 Greatest Movies of All Time.  Also in 2008, the Boston Herald named it the second greatest horror film after Nosferatu.  Entertainment Weekly considers the film superior to Frankenstein. {{cite book
|title= The Entertainment Weekly Guide to the Greatest Movies Ever Made
|year=1996
|publisher=Warner Books
|location=New York
|isbn=
|pages=99–100}} 

==Christian imagery== Christian imagery is "hidden in plain sight" throughout the film. In addition to the scenes of the Monster trussed in a cruciform pose and the crucified figure of Jesus in the graveyard, the hermit has a crucifix on the wall of his hut (which, to Whales consternation, editor Ted Kent made glow during a fade-out)  and the Monster consumes the Christian sacraments of bread and wine at his "last supper" with the hermit. Horror scholar David J. Skal suggests that Whales intention was to make a "direct comparison of Frankensteins monster to Christ".  Film scholar Scott MacQueen, noting Whales lack of any religious convictions, disputes the notion that the Monster is a Christ-figure. Rather, the Monster is a "mockery of the divine" since, having been created by Man rather than God, it "lacks the divine spark". In crucifying the Monster, he says, Whale "pushes the audiences buttons" by inverting the central Christian belief of the death of Christ followed by the resurrection.  The Monster is raised from the dead first, then crucified. 

==Homosexual interpretations==
In the decades since its release, modern film scholars have noted the possible gay reading of the film. Director James Whale was openly gay, and others of the cast, including Ernest Thesiger and Colin Clive, {{cite journal
| last =Morris
| first =Gary
| title =Sexual Subversion: The Bride of Frankenstein
| journal =Bright Lights Film Journal
| issue =19
| date =July 1997
| url =http://www.brightlightsfilm.com/19/19_bride1.html camp sensibility,  particularly embodied in the character of Pretorius and his relationship with Henry.

Gay film historian Vito Russo, in considering Pretorius, stops short of identifying the character as gay, instead referring to him as "sissy|sissified"  ("sissy" itself being Hollywood code for "homosexual"). Pretorius serves as a "gay Mephistopheles",  a figure of seduction and temptation, going so far as to pull Frankenstein away from his bride on their wedding night to engage in the unnatural act of creating non-procreative life. A novelization of the film published in England made the implication clear, having Pretorius say to Frankenstein  Be fruitful and multiply. Let us obey the Biblical injunction: you of course, have the choice of natural means; but as for me, I am afraid that there is no course open to me but the scientific way." 

The Monster, whose affections for the male hermit and the female Bride he discusses with identical language ("friend") has been read as sexually "unsettled" and bisexual.  Gender studies author Elizabeth Young writes: "He has no innate understanding that the male-female bond he is to forge with the bride is assumed to be the primary one or that it carries a different sexual valence from his relationships with  : all affective relationships are as easily friendships as marriages."  Indeed, his relationship with the hermit has been interpreted as a same-sex marriage that heterosexual society will not tolerate: "No mistake&nbsp;– this is a marriage, and a viable one&nbsp;... But Whale reminds us quickly that society does not approve. The monster&nbsp;– the outsider&nbsp;– is driven from his scene of domestic pleasure by two gun-toting rubes who happen upon this startling alliance and quickly, instinctively, proceed to destroy it", writes cultural critic Gary Morris for Bright Lights Film Journal.  (In the parody of this scene in Mel Brooks Young Frankenstein, the hermit is explicitly gay.) The creation of the Bride scene, Morris continues, is "Whales reminder to the audience&nbsp;– his Hollywood bosses, peers, and everyone watching&nbsp;– of the majesty and power of the homosexual creator". 
 David Lewis stated flatly that Whales sexual orientation was "not germane" to his filmmaking, saying, "Jimmy was first and foremost an artist, and his films represent the work of an artist&nbsp;– not a gay artist, but an artist." 

==Remakes== The Bride (1985), starring Sting (musician)|Sting, Clancy Brown, and Jennifer Beals.    In 1991, the studio sought to remake the film for cable television, and Martin Scorsese expressed interest in directing. 
 American Splendor, Jacob Estes was also involved with the project at one point and wrote a draft.  In June 2009, Universal and Imagine entered discussions with director Neil Burger and his writing partner Dirk Wittenborn,  and producer Brian Grazer was assigned to oversee the development of the remake. 

==See also==
 
* Boris Karloff filmography
* List of films featuring Frankensteins monster
* Frankenstein in popular culture|Frankenstein in popular culture Gods and Monsters, a 1998 James Whale biopic that draws its title from a quote from Bride of Frankenstein
* List of horror films of the 1930s
* Universal Monsters

==Notes==
 

==References==
* Brunas, Michael, John Brunas & Tom Weaver (1990). Universal Horrors: The Studios Classic Films, 1931–46. Qefferson, NC, McFarland & Co.
* Curtis, James (1998). James Whale: A New World of Gods and Monsters. Boston, Faber and Faber. ISBN 0-571-19285-8.
* Gelder, Ken (2000). The Horror Reader. New York, Routledge. ISBN 0-415-21355-X.
* Denis Gifford|Gifford, Denis (1973) Karloff: The Man, The Monster, The Movies. Film Fan Monthly.
* Goldman, Harry (2005). Kenneth Strickfaden, Dr. Frankensteins Electrician. McFarland. ISBN 0-7864-2064-2.
* Johnson, Tom (1997). Censored Screams: The British Ban on Hollywood Horror in the Thirties. McFarland. ISBN 0-7864-0394-2.
* Lennig, Arthur (1993). The Immortal Count: The Life and Films of Bela Lugosi. University Press of Kentucky. ISBN 0-8131-2273-2.
* Mallory, Michael (2009) Universal Studios Monsters: A Legacy of Horror.  Universe.  ISBN 0-7893-1896-2.
* Mank, Gregory W. (1994). Hollywood Cauldron: Thirteen Films from the Genres Golden Age. McFarland. ISBN 0-7864-1112-0.
* Picart, Carolyn Joan, Frank Smoot and Jayne Blodgett (2001). The Frankenstein Film Sourcebook. Greenwood Press. ISBN 0-313-31350-4.
* Vito Russo|Russo, Vito (1987). The Celluloid Closet: Homosexuality in the Movies (revised edition). New York, HarperCollins. ISBN 0-06-096132-5.
* Skal, David J. (1993). The Monster Show: A Cultural History of Horror. Penguin Books. ISBN 0-14-024002-0.
* Vieira, Mark A. (2003). Hollywood Horror: From Gothic to Cosmic. New York, Harry N. Abrams. ISBN 0-8109-4535-5.
* Elizabeth Young (journalist)|Young, Elizabeth. "Here Comes The Bride". Collected in Gelder, Ken (ed.) (2000). The Horror Reader. Routledge. ISBN 0-415-21356-8.

==External links==
 
 
*  
*  
*  
*  
*  

 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 