Zelig
 
 
{{Infobox film
| name           = Zelig
| image          = Zeligposter.jpg
| image size     =
| caption        = Original poster
| director       = Woody Allen
| producer       = Robert Greenhut
| writer         = Woody Allen
| narrator       = Patrick Horgan
| starring = {{plainlist|
* Woody Allen
* Mia Farrow
}}
| music          = Dick Hyman
| cinematography = Gordon Willis
| editing        = Susan E. Morse
| studio         = Orion Pictures MGM  
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $11,798,616 (US)
}}

Zelig is a 1983 American mockumentary film written and directed by Woody Allen and starring Allen and Mia Farrow. Allen plays Leonard Zelig, a nondescript enigma who, out of his desire to fit in and be liked, takes on the characteristics of strong personalities around him. The film, presented as a documentary, recounts Zeligs intense period of celebrity in the 1920s and includes analyses from present day intellectuals.

The film was photographed and narrated in the style of 1920s black-and-white newsreels, which are interwoven with archival footage from the era, and re-enactments of real historical events. Color segments from the present day include interviews of real and fictional personages, including Saul Bellow and Susan Sontag.

==Plot==
Set in the 1920s and 1930s, the film focuses on Leonard Zelig (Woody Allen), a nondescript man who has the ability to transform his appearance to that of the people who surround him. He is first observed at a party by F. Scott Fitzgerald, who notes that Zelig related to the affluent guests in a thick, refined accent and shared their Republican sympathies, but while in the kitchen with the servants he adopted a ruder tone, and seemed to be more of a Democrat. He soon gains international fame as a "human chameleon".

Interviewed in one of the witness shots, Bruno Bettelheim makes the following comment:  

Dr. Eudora Fletcher (Mia Farrow) is a psychiatrist who wants to help Zelig with this strange disorder when he is admitted to her hospital.  Through the use of hypnotism, she discovers Zelig yearns for approval so strongly he physically changes to fit in with those around him. Dr. Fletchers determination allows her to cure Zelig, but not without complications; she lifts Zeligs self-esteem but much too high and thus he temporarily develops a personality which is violently intolerant of other peoples opinions.

Dr. Fletcher realizes she is falling in love with Zelig. Because of the media coverage of the case, both patient and doctor become part of the popular culture of their time. However, fame is the main cause of their division; the same society that made Zelig a hero destroys him.
 Nazis before the outbreak of World War II. Together they escape and return to America, where they are proclaimed heroes (after Zelig, using his ability to imitate one more time, mimics Fletchers piloting skills and flies back home across the Atlantic upside down).

The character Eudora Fletchers name was taken from the principal of Public school (government funded)|P. S. 99, the elementary school that Woody Allen attended as a boy.

==Production==
  bluescreen technology. To provide an authentic look to his scenes, Allen and cinematographer Gordon Willis used a variety of techniques, including locating some of the antique film cameras and lenses used during the eras depicted in the film, and even going so far as to simulate damage, such as crinkles and scratches, on the negatives to make the finished product look more like vintage footage. The virtually seamless blending of old and new footage was achieved almost a decade before digital filmmaking technology made such techniques in films like Forrest Gump (1994) and various television advertisements much easier to accomplish.

The film uses cameo appearances by real figures from academia and other fields for comic effect. Contrasting the films vintage black-and-white film footage, these persons appear in color segments as themselves, commenting in the present day on the Zelig phenomenon as if it really happened. They include essayist Susan Sontag, psychoanalyst Bruno Bettelheim, Nobel Prize-winning novelist Saul Bellow, political writer Irving Howe, historian John Morton Blum, and the Paris nightclub owner Ada "Bricktop" Smith|Bricktop.
 Bobby Jones, and Pope Pius XI.

In the time it took to complete the films special effects, Allen filmed A Midsummer Nights Sex Comedy and Broadway Danny Rose.

==Soundtrack==
 
The soundtrack includes such period songs as: Sam Lewis, Joe Young; 
* "Sunny Side Up" by Henderson, Lew Brown, and Buddy G. DeSylva;
* "Aint We Got Fun?" by Richard A. Whiting, Raymond B. Egan, and Gus Kahn;
*  "Charleston (song)|Charleston" by James P. Johnson and Cecil Mack;
* "Ill Get By (As Long as I Have You)" by Fred E. Ahlert and Roy Turk;
* "Ive Got a Feeling Im Falling" by Fats Waller, Harry Link, and Billy Rose;
* "I Love My Baby (My Baby Loves Me)" by Harry Warren and Bud Green;
* "A Sailboat in the Moonlight" by Carmen Lombardo and John Jacob Loeb;
* "Chicago (That Toddlin Town)" by Fred Fisher;
* "Anchors Aweigh" by Charles A. Zimmerman and Alfred Hart Miles.

In addition, Dick Hyman composed a number of tunes allegedly inspired by the Zelig phenomenon, including "Leonard the Lizard", "Reptile Eyes", "You May Be Six People, But I Love You", "Doin the Chameleon", "The Changing Man Concerto", and "Chameleon Days", the latter performed by Mae Questel, the voice of Betty Boop.

==Release==
Before being shown at the Venice Film Festival, the film opened on six screens in the US and grossed US$60,119 on its opening weekend; it eventually earned $11,798,616 in the United States. 

==Critical reaction==
Zelig has an overall approval rating of 100% on the review aggregator Rotten Tomatoes based on reviews from 21 professional critics with a weighted average of 7.9/10.   In his review in the New York Times, Vincent Canby observed: and perfectly original Woody Allen comedy.   }}
 Time Out described it as "a strong contender for Allens most fascinating film",  while TV Guide said, "Allens ongoing struggles with psychoanalysis and his Jewish identity stridently literal preoccupations in most of his work are for once rendered allegorically. The result is deeply satisfying". 

In Empire (film magazine)|Empire magazines poll of the 500 greatest movies ever made, Zelig was ranked number 408. 

==Awards and nominations==
 
 
*56th Academy Awards
**Academy Award for Best Cinematography (Gordon Willis, nominee) Academy Award for Best Costume Design (Santo Loquasto, nominee)
*37th British Academy Film Awards
**BAFTA Award for Best Original Screenplay (nominee)
**BAFTA Award for Best Cinematography (nominee)
**BAFTA Award for Best Special Visual Effects (nominee)
**BAFTA Award for Best Editing (nominee)
**BAFTA Award for Best Makeup (nominee)
*Writers Guild of America Award for Best Comedy Written Directly for the Screen (nominee) National Society of Film Critics Award for Best Cinematography (Gordon Willis, nominee)
 
*41st Golden Globe Awards
**Golden Globe Award for Best Motion Picture - Musical or Comedy (nominee)
**Golden Globe Award for Best Actor - Motion Picture Musical or Comedy (Woody Allen, nominee)
*Saturn Award for Best Direction (nominee) New York Best Cinematography (winner) Kansas City The Year of Living Dangerously)
*  (winner) David di Best Foreign Actor (Allen, winner)
*Venice Film Festival Pasinetti Award for Best Film (winner) Bodil Award for Best Non-European Film (winner)
 

==References==
;Notes
 

;Bibliography
*  
*  
*  

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 