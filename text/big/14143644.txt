The Secret Code (serial)
{{Infobox film
| name           = The Secret Code
| image          = The Secret Code-lobby card.jpg
| image_size     =
| caption        = Lobby card (courtesy of  ) Spencer G. Bennet
| producer       = Ralph Cohn Robert Beche Leighton Brill Original screenplay
| narrator       = Knox Manning Paul Kelly Robert O. Davis
| music          = Lee Zahler
| cinematography = James S. Brown Jr. Black and white Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 episodes (275 minutes)
| country        = United States English
| budget         =
| gross          =
}} serial released Spy Smasher serial of the same year.  The chapters of this serial each ended with a brief tutorial in cryptography.

==Plot==
This serial introduces the World War II scenario when a masked hero tries to prevent Nazi agents from crippling the USs war effort. The spy ring is led by fifth columnist Jensen, who, with his lieutenant Rudy Thyssen and a network of Nazi saboteurs, is trying to get possession of a top-secret formula the United States had developed for manufacturing synthetic rubber while creating explosive gases and radio-controlled bombs to sabotage the exhausting war effort. Then Police Lieutenant Dan Barton stages a public dismissal from the police department, in order to join the saboteurs ring and learn the secret code they have been using. To further assist his efforts (especially after his superior, the only person to know that Barton is working undercover, is murdered), Barton assumes the secret identity of the Black Commando, a masked man who is wanted both by the villains (who want the secret formula they think he has) and police (who are also searching for Barton for murder). Finally, Barton steals the formula and is captured by Thyssen and put under the protection of the sabotage ring. Joining the gang, he learns of their plans, which he immediately leaks to his best friend and former partner Pat Flanagan and news reporter girlfriend Jean Ashley and, as "The Black Commando", continually frustrates the Nazi plots. After innumerable dangers and lost efforts in trying to decipher the enemys secret codes, Barton and Flanagan discover the key to the Nazi code, capture the Nazi ring and make sure that the Nazi U-boat which has been waiting to help the Nazis escape is depth-bombed and destroyed.

As an aggregate value, at the end of each episode, the audience is given a short lecture on solving complex secret messages.

==Cast== Paul Kelly as Lt. Dan Barton/Black Commando
*Anne Nagel as Jean Ashley
*Trevor Bardette as Jensen Robert O. Davis as Rudi Thysson 
*Clancy Cooper as DS Pat Flanagan Gregory Gay as Feldon, Nazi agent Louis Donath as Professor Metzger
*Beal Wong as Quito
*Eddie Parker as Berk, Chief henchman
*Wade Boteler as Burns, Police chief Charles C. Wilson as Desk Sergeant Cullen
*Alex Callam as P.I. Hogan
*Justin Cousson as P.I. Drake Robert Fiske as P.I. Ryan
*Selmer Jackson as Major Henry Barton
*Jacqueline Dalya as Linda

==Production== Spy Smasher serial had been released several months before The Secret Code in 1942.  Columbias adverts for The Secret Code included the phrases "Smash spies with the Secret Service" and "Thrill again to spy smashers biggest chase!" {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 273–274
 | chapter = 10. The Long-Underwear Boys "Youve Met Me, Now Meet My Fist!"
 }} 

Each chapter ended with a quick lesson in cryptography and a "brief patriotic admonishment" given by Selmer Jackson.  Cline describes this as "propaganda in its basic form...delivered in the most effective way possible - by a respected authority figure in the person of one of Hollywoods most credible actors." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 136
 | chapter = 9. They Who Also Serve (The Citizens)
 }} 
 The Green Hornet (1940) and The Green Hornet Strikes Again! (1941) were reunited in this Columbia serial.

==Release==

===Theatrical===
The Secret Code was released in Latin America in May 1944, under the title La Clave Secreta, in English with Spanish subtitles.  This serial also was released as a feature film overseas.

==Critical reception==
Harmon and Glut consider this serial to be above average for a Columbia production. 

==Chapter titles==
# Enemy Passport
# The Shadow of the Swastika
# Nerve Gas
# The Sea Spy Strikes
# Wireless Warning
# Flaming Oil
# Submarine Signal
# The Missing Key
# The Radio Bomb
# Blind Bombardment
# Ears of the Enemy
# Scourge of the Orient
# Pawn of the Spy Ring
# Dead Men of the Deep
# The Secret Code Smashed
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 234
 | chapter = Filmography
 }} 

==See also== List of film serials by year
*List of film serials by studio

==References==
 

==External links==
* 
* 
* 
* 

 
{{succession box  Columbia Serial Serial 
| before=Perils of the Royal Mounted (1942)
| years=The Secret Code (1942)
| after=The Valley of Vanishing Men (1942)}}
 

 

 
 
 
 
 
 
 
 
 