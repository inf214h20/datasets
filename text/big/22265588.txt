Arappatta Kettiya Gramathil
{{Infobox film
| name           = Arappatta Kettiya Gramathil
| image          = Arappatta Kettiya Gramathil.jpg
| image size     = 
| alt            = 
| caption        = VCD Cover
| director       = P. Padmarajan
| producer       = 
| screenplay         = P. Padmarajan
| based on = Arappatta Kettiya Gramathil by  P. Padmarajan
| narrator       =  Ashokan   Kanakalatha   Sukumari  Unnimary  Jagathy Sreekumar
| music          =
| cinematography = Shaji N. Karun, Venu Isc
| editing        = B. Lenin
| studio         = Supriya Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Arappatta Kettiya Gramathil ( ,  ) is a 1986 Malayalam film directed by P. Padmarajan, based on his own short story of the same name. The film revolves around three men, Zechariah (Mammootty), Hilal (Ashokan (actor)|Ashokan), and Gopi (Nedumudi Venu), who go to a brothel in a remote village and the problems they had to face there in the next 24 hours. It also stars Kanakalatha, Sukumari, Unnimary and Jagathy Sreekumar in other pivotal roles.

The film was a commercial failure though considered as a cult classic by Padmarajans fans.   Sukumari won both the Kerala State Film Award and the Kerala Film Critics Award for Best Supporting Actress for her role as Maluvamma.

== Plot ==
One night at a dance bar, Joseph, a rich, but spoiled brat of a textile businessman offers a trip to his three friends to a brothel. Zechariah(Mammootty), Gopi (Nedumudi Venu), and Hilal (Ashokan (actor)|Ashokan), accepts the offer and sets out in Josephs car. But on the way, Zechariah and Joseph gets into a fight and the trip gets canceled, with Joseph, leaving them back. But Zechariah decides to take both his friends to the brothel, though, he just has a vague knowledge of the route. The trio sets out on the next morning and reaches the remote village. The suspicious look of the villagers frightens both Gopi and Hilal, but Zechariah is cool in his moves. The trio reaches the brothel run by Maluvamma, which is actually her ancestral house. They enter inside the huge house, and is shocked to find few children playing and an old man scribbling something on a paper, without even bothering to take a look at them. The trio enters inside the house and comes across a girl who is both deaf and dumb. From her body language, the trio finds out that she is a prostitute kept by Maluvamma and Gopi gets wooed by her. Just in a couple of minutes, arrives Devaki (Unnimary), who is more ravishing and sensual. She informs them that Maluvamma (Sukumari) has gone outside with Gowrikutty, a new girl and will be back in few minutes. But the trio is shocked by the sudden entry of Bhaskaramenon (Jagathy Sreekumar), who asks them to vacate the house as a group of Muslims are planning to attack the house. He also shows them the injury marks caused a few days back in a riot and shows them the door to outside. On their way back, a group of Muslims surrounds and  verbally abuse them. With Zechariah beginning to retaliate, they turn violent. But the leader of the mob, pacifies the crowd and takes them to a nearby tea stall and decides to consult with Moopan, about the next move. By keeping the trio under the custody of the mob, he reaches the house of Moopan (Kunjandy). Generally, the girls brought by Maluvamma are first enjoyed by Moopan and it was the opposition of Gowrikutty to be a prey to Moopan that caused all the riot in the village.  Maluvamma, by the time convinces Moopan that Gowrikutty will be brought to him in a couple of days and Moopan is convinced. He orders release of the trio and Maluvamma takes them to her house. She lets Gopi to go for the deaf and dump girl, while Zechariah opts to play cards with Devaki and the old man. Hilal enters inside the room of Gowrikutty.  But while trying to fondle her, she turns violent, making Hilal to forcefully go on for her. Hilal is shocked to find out that Gowrikutty is forcefully brought in to the business by Maluvamma and is still a virgin. He sympathizes on her and asks her to tell her story. She tells him that she was sold to Maluvamma by her uncle after the death of her father. It was only a few days after the arrival at the house that she realized that it is a brothel. On refusing to entertain the clients, she was brutally assaulted by Maluvamma. She pleads Hilal to save her from the brothel. He promises to take her out and marry her. But his plan to take her out is foiled by Bhasi, Maluvammas son. Hilal tells Gopi and Zechariah about Gowrikutty and Zechariah promises him to help him in saving her. Narayanan (Achankunju), a pimp of Panicker, a local feudal  landlord arrives and demands Maluvamma to release Gowrikutty along with him at night. Maluvamma agrees to it and asks him to come at night.That night, Gowrikutty refuses to go along with Narayanan, which enrages him. He returns to collect more people to avenge on Maluvamma. But she is now supported by Moopan, who sends a bunch of his henchmen for security of the house. A few hours later, Narayanan is back with a dozen of his goons. He is attacked by the goons of Moopan and a riot sets out. Zechariah decides to use this opportunity and stab Bhasi, making Maluvamma to come out of her room. Using this opportunity, Hilal and Gopi takes Gowrikutty and runs out of the house. While escaping, Zechariah is attacked by Narayanan. In an attempt to save himself, Narayanan stabs Zechariah to death. Reaching at a safe place, Gopi asks Hilal to run away with the girl and decides to go back to find out what had happened to Zechariah. He reaches out the house and calls out Zechariahs name loudly, but gets no response.

==Cast==
*Mammootty as Zechariah Ashokan as Hilal
*Kanakalatha as Sreedevi
*Nedumudi Venu as Gopi
*Unnimary as Devaki
*Sukumari as  Malu Amma
*Jagathi Sreekumar as Bhasi
*Kunjandi as Moopan Soorya
*Kunjandi
*Achankunju as Narayanan

==Changes from the short story==

Though adapted from his own short story with same name, P.Padmarajan has brought several changes to the script. While the short story does not gives out the reason of how Zechariah, Gopi and Bilal reaches the brothel, the film explains it in detail. When the short story had just a handful of characters, the film had more than a dozen characters. Several characters including Joseph, Panikkar, Narayanan and Devaki were added by Padmarajan.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 