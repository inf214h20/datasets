Kaalinga
{{Infobox film 
| name           = Kalinga
| image          =  
| caption        = 
| director       = V. Somashekhar
| producer       = Srikanth Nahatha
| story          = Ghai Bhalla
| writer         = R. N. Jayagopal (dialogues)
| screenplay     = V. Somashekhar Vishnuvardhan Rati Agnihotri Udaykumar Vajramuni
| music          = Chellapilla Satyam
| cinematography = S. V. Srikanth
| editing        = D. Venkatarathnam
| studio         = Srikanth & Srikanth Enterprises
| distributor    = Srikanth & Srikanth Enterprises
| released       =  
| runtime        = 147 min
| country        = India Kannada
}}
 1980 Cinema Indian Kannada Kannada film, directed by V. Somashekhar and produced by Srikanth Nahatha. The film stars Vishnuvardhan (actor)|Vishnuvardhan, Rati Agnihotri, Udaykumar and Vajramuni in lead roles. The film had musical score by Chellapilla Satyam.  

==Cast==
  Vishnuvardhan
*Rati Agnihotri
*Udaykumar
*Vajramuni
*Dinesh
*Thoogudeepa Srinivas
*Shakthi Prasad
*Tiger Prabhakar
*Sampath Geetha
*B. Jayashree
*Rajeshwari
*Baby Rekha
*Baby Lakshmi
*Jyothilakshmi
*Hanumanthachar
*Shivaprakash
*Ashwath Narayana
 

==Soundtrack==
The music was composed by Satyam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhayavanu Sidu || S. Janaki|Janaki, Malaysia Vasudevan || Chi. Udaya Shankar || 04.56
|- Susheela || R. N. Jayagopal || 04.58
|- Susheela || Chi. Udaya Shankar || 04.41
|-
| 4 || Thayi Thande Ibbaru || S. Janaki|Janaki, Vani Jayaram || Chi. Udaya Shankar || 04.55
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 