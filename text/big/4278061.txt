King Uncle
{{Infobox film
| name = King Uncle किंग अंकल  
| image = Kingunclefilm.jpg
| caption = Audio Cassette Cover
| producer = Rakesh Roshan
| Director = Rakesh Roshan
| music = Rajesh Roshan
| starring =Shahrukh Khan Jackie Shroff  Anu Agarwal  Paresh Rawal Deven Verma Nagma
| distributor = Venus Records & Tapes (audio) Baba Eros International (video)
| released = 5 February 1993
| runtime = 171 min.
| country = India
| language = Hindi   Urdu
| budget =
}}
 Annie that starred Aileen Marie Quinn and Albert Finney,  which in turn is based upon the 1924 comic strip Little Orphan Annie by Harold Gray. 

==Synopsis==
Ashok Bansal (Jackie Shroff) is a strict industrialist, inspired by the British stiff upper lip. Ashok starts hating poor people & starts working hard to become rich. In the process of acquiring wealth, he neglects his family consisting of his younger brother Anil (Shahrukh Khan) and sister Suneeta (Nivedita Saraf). He gets Suneeta married to a man who turns out to be a golddigger although she is in love with Ashoks manager (Vivek Vaswani). Anil goes against his brother’s highhanded ways and marries Kavita (Nagma), opting to leave the house.
Munna (Pooja Ruparel), an orphan arrives a the house & tries to thaw the icy strict Ashok. She creates havoc in Ashok’s home while he tries to get her taken back by the orphanage. When orphanage warden calls to take her back, he lets her go. Realizing that he misses her, he decides to adopt her and mend fences with his own family. He frees his sister from her abusive husband and brings her home.

== Cast ==
* Shahrukh Khan as Anil Bansal
* Jackie Shroff as Ashok Bansal 
* Nivedita Joshi as Suneeta Bansal
* Anu Agarwal as Fenni Fernando
* Paresh Rawal as Pratap    
* Nagma as Kavita  
* Deven Verma as Karim 
* Pooja Ruparel as Munna
* Sushmita Mukherjee as Shanti 
* Dinesh Hingoo as Chunilal  
* Dalip Tahil as Pradeep Mallik

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Akkad Bakkad Bombay Bo"
| Sudesh Bhosle, Alka Yagnik
|- 
| 2
| "Dil Mane Jise Wohi Apna"
| Kumar Sanu
|-
| 3
| "Dil Mane Jise Wohi Apna"
| Sudesh Bhosle, Asha Bhosle
|-
| 4
| "Hum Rahe Na Rahe Yahan Par"
| Sadhana Sargam
|-
| 5
| "Is Jahan Ki Nahin Hain"
| Nitin Mukesh, Lata Mangeshkar
|-
| 6
| "Khush Rahne Ko Zaroori"
| Vinod Rathod, Nitin Mukesh, Alka Yagnik, Sadhana Sargam
|-
| 7
| "Parody"
| Sudesh Bhosle, Sapna Mukherjee
|-
| 8
| "Tare Aasman Ke Dharti Pe"
| Sadhana Sargam
|}

==Trivia==
*The titular role was originally offered to Amitabh Bachchan, but he declined.
*The film is based on the American musical and film "Annie (1982 film)|Annie".
*The films song "Tare Aasman Ke Dharti Pe" as well as some continued melodies through the movie are sampled from the whistling tune used by Roxette in their song "Joyride (Roxette song)|Joyride".

==External links==
* 

 

 
 
 