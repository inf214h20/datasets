Death at a Funeral (2007 film)
 
 
{{Infobox film
| name           = Death at a Funeral
| image          = Death-at-a-funeral-poster.jpg
| image_size     = 220px
| border         = yes
| alt            =
| caption        = British theatrical release banner
| director       = Frank Oz
| producer       = {{Plainlist|
* Sidney Kimmel
* Laurence Malkin
* Diana Phillips
* Share Stallings }}
| writer         = Dean Craig
| starring       = {{Plainlist|
* Matthew Macfadyen
* Rupert Graves
* Andy Nyman
* Kris Marshall
* Peter Dinklage
* Keeley Hawes
* Daisy Donovan
* Alan Tudyk
* Ewen Bremner }}
| music          = Murray Gold
| cinematography = Oliver Curtis
| editing        = Beverley Mills Sidney Kimmel Entertainment
| distributor    = {{Plainlist|
* Verve Pictures  
* Metro-Goldwyn-Mayer   }}
| released       =  
| runtime        = 90 minutes  
| country        = United Kingdom United States Germany 
| language       = English
| budget         = $9 million   
| gross          = $46,789,413   
}} British comedy film directed by Frank Oz. The screenplay by Dean Craig focuses on a family attempting to resolve a variety of problems while attending the funeral of the patriarchy|patriarch.

==Plot== first class airline ticket to England than help finance the funeral, leaving Daniel to cover the burial expenses. As guests begin to arrive at the family home, where the funeral service is to be held, he struggles to complete a eulogy, although everyone expects Robert the writer will be the one to deliver some appropriate remarks.
 hallucinogenic drug manufactured by her brother Troy, a pharmaceutical student. While on the way to the funeral, Simon begins to feel its effect.
 dwarf named Peter introduces himself to Daniel, who is too busy to speak to him at that moment and suggests they talk later. None of Daniels relatives can identify the man. The service begins and the hallucinating Simon, certain he hears scratching noises in the coffin, tips it over, causing the body to spill out onto the floor. During the ensuing chaos, Martha drags Simon outside, where her father forbids her to marry him. When Simon is told why he is reacting as he is, he panics and locks himself in the upstairs bathroom. Martha tries to persuade him to open the door while fending off the unwelcome advances of Justin, with whom she once had a one-night stand that she deeply regrets and would like to forget. When the drug overcomes Simon, he steps out of the window naked and sees Justin kissing Martha. Thinking what he sees reflects mutual feelings, he proceeds to climb onto the roof, where he threatens to jump. Hoping to calm him, Martha reveals she is pregnant with their child.
 germaphobic family friend Howard cannot feel a pulse, so they believe Peter to be dead. Forced to dispose of the body as quickly and surreptitiously as possible, Daniel and Robert decide to place it in the casket with their father.

The service resumes, and Daniels awkward eulogy is interrupted when the still very-much-alive Peter leaps from the coffin and the compromising photos fall out of his pocket for everyone, including the widow, Sandra, to see, who, in shock, attacks Peter and tries to choke him to death, only to be stopped by Robert. Daniel demands everyone stay calm and declares his father was a good man, although clearly one with secrets, and he delivers a loving tribute to the man.

That same night after all the mourners (including Peter) have left, Robert tells Daniel that he plans on taking their mother to New York so that Daniel and Jane can finally buy their own flat and live alone like they had always wanted. Their conversation is interrupted when Jane shows up and tells them that Uncle Alfie will be staying over that night because of his panic attack after having seen Daniel and Robert move the body. She also tells them that she gave him some "Valium" (unbeknownst to her it is actually Troys hallucinogenic), which shocks Daniel and Robert. The scene cuts to a shot of Uncle Alfie on the roof, naked like Simon had been, complaining about how "everything is so f***ing green".

==Cast==
* Matthew Macfadyen as Daniel
* Rupert Graves as Robert
* Andy Nyman as Howard
* Kris Marshall as Troy
* Peter Dinklage as Peter
* Keeley Hawes as Jane
* Daisy Donovan as Martha
* Alan Tudyk as Simon
* Ewen Bremner as Justin
* Peter Vaughan as Uncle Alfie Thomas Wheatley as The Reverend
* Jane Asher as Sandra
* Peter Egan as Victor

==Production== premiered at European Film US Comedy Arts Festival, the Seattle International Film Festival, the Breckenridge Film Festival, the Maui Film Festival, the Sydney Film Festival, the Provincetown International Film Festival, and the Tremblant Film Festival before going into limited release in the United States on 17 August 2007. It opened throughout Europe, Asia, South America, and Australia before going into theatrical release in the United Kingdom on 2 November 2007.

==Release==
===Critical reception===
Ruthe Stein of the San Francisco Chronicle said the film is "in the tradition of those classics, in black-and-white and starring Peter Sellers or Alec Guinness, in which disasters keep piling up, each one more drolly funny than the last. Thats high praise for Death but no more than it deserves. The humor manages to be simultaneously sophisticated, supremely silly and very dark . . . The casting couldnt be better. With no big stars to upset the balance, the actors work together as a true ensemble, the best since Little Miss Sunshine." 

At Rotten Tomatoes, the film scored 61%, according to 124 critics, with the consensus "Death at a Funeral is a rousing British farce, with enough slapstick silliness to overcome its faults." 

Roger Ebert of the Chicago Sun-Times rated it three out of four stars and commented, "The movie is part farce (unplanned entrances and exits), part slapstick (misbehavior of corpses) and part just plain wacky eccentricity. I think the ideal way to see it would be to gather your most dour and disapproving relatives and treat them to a night at the cinema." 

Sid Smith of the Chicago Tribune called the film "lethal farce, combining hints of The Lavender Hill Mob, doses of Joe Orton and a smidgen of the Farrelly brothers scatology in its mix." He added, "The sibling rivalry/resolution meant to give the movie its sweet, heartfelt thread is weak, and there are stretches in which the comedy sags or settles for the predictable. But Death provides an adult tonic in a season typically abandoned to the comic book cocktail. There are worse ways to escape the August heat." 

Steve Dollar of the New York Sun said, "The ensemble approach allows for a maximum of comic scenarios to be put into effect, as the films motor revs into higher and higher gear. Expert pacing and delivery, as well as the abundance of hyper-articulate, over-educated, effervescently British character types, ratchet up the cringe factor accordingly . . . The films antic disposition, or rather its disposition toward antics, may strike some as an exercise in overkill, even though, curiously, the film disappointingly resolves all the comic trauma by letting everyone off the hook. Moreover, a lot of the material, especially the closeted-gay jokes, seems dated. But for Mr. Oz, finger-snap pacing and an expert cast (surely familiar to any BBC America viewer) bring this Death to giddy life." 

Philip French of The Observer said the film, "in which a fine British cast is wasted on feeble material, is directed by Frank Oz in less than wizardly form." 

Phelim ONeill of The Guardian rated it two out of five stars and commented, "Scientists believe that black holes can slow down the progress of time. A similar effect can be felt by viewers of this damp squib of a farce as it grinds on from one lame set-up to the next. This sort of sub-Alan Ayckbourn trawl might work on stage but never on the big screen, where all the deficiencies of the form are thrown into sharp relief. From Oz downwards, the credits are full of familiar names foolishly squandering the goodwill their past works have generated." 

===Box office===
The film eventually earned US$46,600,669, split US$8,580,428 in North America and the equivalent of US$38,020,241 in other markets. It opened in 260 theatres in the United States on 17 August 2007 and grossed US$1,282,973 on its opening weekend, ranking #17 at the box office. 

===Awards===
Director Frank Oz won the Audience Award at both the US Comedy Arts Festival and the Locarno International Film Festival.

===Home media===
The   and another with screenwriter Dean Craig and cast members Alan Tudyk and Andy Nyman, and a gag reel.

==Remakes== Hindi remake Daddy Cool, was released starring Sunil Shetty, Aashish Chaudhary, and Rajpal Yadav.
 remake of the same name, directed by Neil LaBute. Peter Dinklage returned to play the role of Peter, although the character was renamed Frank. The film was released on 16 April 2010 to mixed reviews.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 