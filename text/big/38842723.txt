Sajan (film)
{{Infobox film
| name           = Sajan
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Mohan Segal
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = Manoj Kumar Asha Parekh Om Prakash Madan Puri
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Mohan Segal. It stars Manoj Kumar and Asha Parekh in pivotal roles.

==Cast==
* Manoj Kumar...Ashok
* Asha Parekh...Rajni
* Om Prakash...Driver Balam
* Madan Puri...Seth Dharamdas

==Plot==
Ashok comes from a business family and lives a very wealthy lifestyle. One day while perusing newspapers he comes across an article which claims that he is to marry a dancer named Rajni. Angered at this insinuation, he goes to confront Rajni, all ready to threaten with a lawsuit, but is instead bewitched by her beauty and innocence and ends up falling in love with her. Initially he does not reveal his real identity, but does inform her eventually, and both are ready to take the next step. Before that could happen both get embroiled in a murder - a murder that ends up having three different killers - all confessing to the same crime. 

The first half of the story has inspired parts of the movie Ghajini (2005 film)|Ghajini (2005 film).

==External links==
* 

 
 
 

 