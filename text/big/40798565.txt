Marci és a kapitány
 

{{Infobox television
 | show_name          = Marci és a kapitány
| writer          = Ottó Demény
 | director      = Márta Beregszászi
 | voices         = Lajos Varanyi Irén Szöllősy Ildikó Simándi Gertrúd Havas István Bölöni Kiss Hugó Gruber
| theme_music_composer   = Ferenc Váry
| country       = Hungary Hungarian
 | num_seasons = 1
 | num_episodes = 13
| editor         = Gabriella Karátsony
| cinematographer     = Anna Czóbel
 | runtime     = 14 minutes per episode
 | company       = Magyar Televízió M1 M2 M2
 | first-aired = 1977
}}

Marci és a kapitány is a Hungarian series of  puppet films from 1977.

== Narrators ==

* Captain: Lajos Varanyi
* Mrs Orsolya: Irén Szöllősy
* Anikó: Ildikó Simándi
* Marci: Gertrúd Havas
* Béka: István Bölöni Kiss
* Bernát: Hugó Gruber

== List of episodes ==
# Ki a jó gyermek?
# Ki esik a vízbe?
# Az emberrablók
# Hinta-palinta
# Érdemes hegyet mászni
# Orsolya néni születésnapja
# Selyemhajú Stefánia
# Hol a szemüveg?
# Hét tenger ördöge
# A kedvező szél
# Hajótörés
# Palackposta
# ?

== External links ==
*  

 
 
 


 