Qué hombre tan sin embargo
{{Infobox film
| name           = Qué hombre tan sin embargo
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Julián Soler
| producer       = Gregorio Walerstein
| writer         = José María Fernández Unsáin Gregorio Walerstein Eulalio González
| screenplay     = José María Fernández Unsáin
| story          = 
| based on       =  
| narrator       = 
| starring       = Eulalio González Julissa Enrique Rambal Lucy Gallardo
| music          = Manuel Esperón
| cinematography = Jorge Stahl, Jr.
| editing        = Rafael Ceballos
| studio         = Cima Films
| distributor    =
| released       =  
| runtime        = 99 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} Eulalio González "Piporro", Julissa, Enrique Rambal, and Lucy Gallardo in the leading roles.

==Synopsis==
An angel, disguised as a witty vagabond named Filomeno and instructed by God, becomes the butler of an affluent and excessively materialistic family.

==Cast==
 
*Eulalio Gonzalez as Filomeno
*Julissa as Laura
*Enrique Rambal as Don Jaime
*Lucy Gallardo as Doña María
*León Michel as Hipólito "Polo" 
*Ricardo Carrión as Raúl
*Óscar Ortiz de Pinedo as Lucrecio 
*Jessica Munguía as Lauras friend
*Sergio Ramos as a lawyer
*Juan Salido as Jorge
*Silvia Fuentes as Lauras friend
*Conjunto de los Hermanos Carreón as the band at the party
*Hilda Aguirre as Rosa
 

==Production== Estudios San Ángel in Mexico City and ended on 22 December 1965.   

===Casting===
The film featured the "accidental" cinematic debut of actress Hilda Aguirre; her father (José María Aguirre) was a friend of producer Gregorio Walerstein.    Aguirres father had told the producer that he had a "half-crazy daughter who wanted to work in cinema."  The producer then set up an appointment with Aguirre, made her walk, laugh, and talk, closely observing her, and then told her: "Your hired for three years."  She started shooting her scenes in the film on November 1965. 

The film also featured Ricardo Carrións "first role of importance."   

==Soundtrack==
*"El Abuelo Yeh Yeh," written and performed by Eulalio González with Los Hermanos Carrión.
*"Quiereme," written and performed by Eulalio González.
*"Puros Hombres de Delito," written and performed by Eulalio González.

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 