Tapeheads
{{Infobox film
| name = Tapeheads
| image = Tapeheads (movie poster).jpg
| caption = Original movie poster
| director = Bill Fishman
| producer = Peter McCarthy Michael Nesmith
| writer = Bill Fishman Peter McCarthy
| starring = John Cusack Tim Robbins Mary Crosby Katy Boyer Don Cornelius Clu Gulager Jessica Walter Ebben Roe Smith Keith Joe Dick
| music = Fishbone
| cinematography = Bojan Bazelli
| studio = NBC Productions
| distributor = Avenue Pictures
| released = October 21, 1988
| runtime = 93 min.
| country =   English
| budget = $10,000,000
| gross = $343,786
}}
 1988 comedy film directed by Bill Fishman. The film stars John Cusack, Tim Robbins, Sam Moore and Junior Walker. The movie was produced by Michael Nesmith, who is seen briefly in the film as a bottled water delivery man.

==Plot== soul duo Menudo concert, getting them to perform in Menudos place, and broadcasting it live across the country on a television satellite hook-up. 
 incidental score) FBI agent.

==Cast==
*John Cusack as Ivan Alexeev
*Tim Robbins as Josh Tager
*Mary Crosby as Samantha Gregory
*Clu Gulager as Senator Norman Mart
*Doug McClure as Sid Tager
*Katy Boyer as Belinda Mart
*Jessica Walter as Kay Mart
*Sam Moore as Billy Diamond
*Junior Walker as Lester Diamond
*Susan Tyrrell as Nikki Morton
*Lee Arenberg as Norton
*Xander Berkeley as Ricky Fell
*"Weird Al" Yankovic as Himself
*Don Cornelius as Mo Fuzz King Cotton as Roscoe
*Zander Schloss as Heavy Metal Fan Martha C. Quinn as RVTV-VJ
*Ted Nugent as Rock Star
*Jello Biafra as FBI Man
*Connie Stevens as June Tager
*Courtney Love (uncredited) as Normans Spanker
*Stiv Bators as Dick Slammer
*Bob Goldthwait (credited as Jack Cheese) as Don Druzel
*David Anthony Higgins as Visual Aplomb
*Michael Nesmith as Water Man
*Jennifer Balgobin as Calypso Dancer
*Sy Richardson as Bartender
*Brie Howard as Flygirl

==Soundtrack==
The music supervisor for the film was Nigel Harrison.  The soundtrack album was released on Island Records.
{{tracklist
| extra_column    = Artist

| title1          = Ordinary Man
| extra1          = Swanky Modes (Sam Moore and Junior Walker)
| length1         = 2:53

| title2          = Roscoes Rap King Cotton
| length2         = 4:26

| title3          = Surfers Love Chant
| extra3          = Bo Diddley
| length3         = 4:56

| title4          = You Hooked Me Baby
| extra4          = Swanky Modes
| length4         = 3:32

| title5          = Betcher Bottom Dollar
| extra5          = Swanky Modes
| length5         = 2:20
 Baby Doll (Sung in Swedish)
| extra6          = Devo
| length6         = 3:36
 Slow Bus A-Movin (Howards Beach Party)
| extra7          = Fishbone ("Ranchbone")
| length7         = 2:39

| title8          = Audience for My Pain
| extra8          = Swanky Modes
| length8         = 4:22

| title9          = Language of Love
| extra9          = Swanky Modes
| length9         = 3:00

| title10         = Ordinary Man (Cant Keep a Good Man Down Mix)
| extra10         = Swanky Modes
| length10        = 4:19
}}

The films soundtrack (but not the soundtrack album) includes the song "Repave America" written and performed by Tim Robbins, credited as Bob Roberts four years before that movie was released. "Repave America" also appeared in the Bob Roberts soundtrack with the lyrics slightly altered to become "Retake America".

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 