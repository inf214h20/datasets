Road to Ninja: Naruto the Movie
{{Infobox film
| name           = Road to Ninja: Naruto the Movie
| image          = Road to Ninja.jpg
| director       = Hayato Date
| producer       =

| story          = Masashi Kishimoto
| writer         = Yuka Miyata
| starring       = Junko Takeuchi Noriaki Sugiyama Toshiyuki Morikawa Emi Shinohara
| music          = Yasuharu Takanashi
| cinematography =
| editing        =  Studio Pierrot
| distributor    = Toho
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         =
| gross          = ¥1,480,000,000 
$17,635,872
}}
Road to Ninja: Naruto the Movie is a 2012 Japanese anime film, the ninth overall   has supervised the movie.    The film is set between the Kage Summit and the Fourth Great War arcs, the anime series having two episodes connected to the movie storyline, with Naruto and Sakura ending up in an alternate reality in a test version of Project Tsuki no Me where the former never lost his parents and many characters have different personalities. The band Asian Kung-Fu Generation performs the films theme song  .  Both the single CD and the films original soundtrack were released on July 25, 2012.   The first 1.5 million movie-goers were given Motion Comic: Naruto, a DVD that shows scenes by Kishimoto.    The film was released on November 25, 2014 by Viz Media on DVD & Blu-ray.

==Plot== Hidden Leaf Akatsuki who Tobi as he uses the   to transport them into an alternate reality based on their inner thoughts. Being called  , Naruto and Sakura are surprised to find their friends acting the exact opposite of their usual selves along with a flirty Sasuke who never left the village. While Sakura learns that her parents are dead with her father as the realitys Fourth Hokage, Naruto sees his parents, Minato and Kushina, alive and well. Shortly after, Naruto and Sakura participate in a mission to retrieve a Red Moon scroll which details on how to defeat a mysterious masked ninja. During the mission, Kushina is injured when she protects Naruto and dissuades the youths refusal to bond with his parents. 

After successfully retrieving the scroll, the masked man takes Sakura hostage and destroys a massive part of Konoha. While the Konoha ninja recuperate, Naruto leaves to exchange the Red Moon scroll for Sakuras life. He is joined by that realitys Akatsuki, led by Itachi, who were sent by Tsunade to act as Narutos backup. A battle with the masked man reveals him to be the real Menma, Narutos counterpart. Though Naruto is victorious over Menma, it turns out that Menma was acting under the influence of Tobi who proceeds to possess the youths body to extract the Nine-Tails. Luckily, using the Red Moon scroll, Naruto purges Tobi from Menma and forces him to cancel the Limited Tsukuyomi. Menma, now freed, is tended to by Minato and Kushina while Naruto bids farewell to his parents before returning to his world with Sakura. Sakura apologies to her parents, having experienced loneliness. At Narutos apartment, Iruka Umino welcomes Naruto home with a cake, reminding Naruto that even though he does not have parents, he does have a family.

==Voice cast==
{| class="wikitable"
|-
! Character
! Japanese voice
! English voice
|-
| Naruto Uzumaki, Menma || Junko Takeuchi || Maile Flanagan
|-
| Sakura Haruno || Chie Nakamura || Kate Higgins
|- Minato Namikaze || Toshiyuki Morikawa || Tony Oliver
|- Kushina Uzumaki Laura Bailey
|- Tobi || Naoya Uchida || Neil Kaplan
|- Hinata Hyuga || Nana Mizuki || Stephanie Sheh
|-
| Rock Lee || Yōichi Masukawa || Brian Donovan
|- Neji Hyuga || Kōichi Tōchika || Steve Staley
|- Tenten || Yukari Tamura || Danielle Judovits
|- Kiba Inuzuka || Kōsuke Toriumi || Kyle Hebert
|- Shino Aburame || Shinji Kawada || Derek Stephen Prince
|-
| Shikamaru Nara || Shōtarō Morikubo || Tom Gibis
|- Choji Akimichi || Kentarou Itou
| Robbie Rist
|- Ino Yamanaka || Ryōka Yuzuki || Colleen OShaughnessey
|- Jiraiya || David Lodge
|- Tsunade || Masako Katsuki || Debi Mae West
|- Shizune || Keiko Nemoto || Megan Hollingshead
|-
| Sasuke Uchiha || Noriaki Sugiyama || Yuri Lowenthal
|- Sai || Ben Diskin
|-
| Kakashi Hatake || Kazuhiko Inoue || Dave Wittenberg
|- Might Guy || Masashi Ebara || Skip Stellrecht
|- Iruka Umino || Toshihiko Seki || Quinton Flynn
|-
| Choza Akimichi || Nobuaki Fukuda || Michael Sorich
|-
| Inoichi Yamanaka || Daiki Nakamura || Kyle Hebert
|-
| Tsume Inuzuka || Seiko Fujiki || Mary Elizabeth McGlynn
|-
| Kizashi Haruno || Yasunori Matsumoto || Steven Blum
|-
| Mebuki Haruno || Kazue Ikura || Kate Higgins
|-
| Itachi Uchiha || Hideo Ishikawa || Crispin Freeman
|- Kisame Hoshigaki || Tomoyuki Dan || Kirk Thornton
|- Deidara || Katsuhiko Kawamoto || Roger Craig Smith
|- Hidan || Masaki Terasoma || Chris Edgerly
|- Kakuzu || Takaya Hashi || Fred Tatasciore
|- Zetsu || Nobuo Tobita || Travis Willingham
|-
| Gamabunta || Hiroshi Naka || Michael Sorich 
|-
| Gamariki || Toshiharu Sakurai || Dave Wittenberg
|- Kurama || Tesshō Genda || Paul St. Peter
|}

==Reception==

===Box office===
The film debuted in the Japanese box office third earning US$3,799,276.  On August 14, 2012   (1.37 billion yen/US$17.4 million). As a result, Kishimoto drew a picture of Naruto and the film antagonist Menma to commemorate the films achievement.  Road to Ninja was the highest grossing Naruto movie making ¥ 1.46 billion (US$18.3 million) between its opening on July 28 and September 23  before it was surpassed by   in December 2014.   For the year 2012, the movie made ¥ 1.48 billion and ranked 29th among Japanese movies in Box office including live action movies.  In the Philippines, the movie earned a total grossed amount of $123,613.00 (PhP 5,068,627) on its 2-week run. 

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 