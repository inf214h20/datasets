Gabriela (1983 film)
 
{{Infobox film
| name           = Gabriela
| image          = Gabriela, Cravo e Canela (film).jpg
| caption        = Film poster
| director       = Bruno Barreto
| producer       = Ibrahim Moussa Harold Nebenzal
| writer         = Bruno Barreto Leopoldo Serran Flávio R. Tambellini
| based on       = Gabriela, Cravo e Canela by Jorge Amado
| starring       = Sônia Braga Marcello Mastroianni
| music          = Antonio Carlos Jobim
| cinematography = Carlo Di Palma
| editing        = Emmanuelle Castro    
| studio         = Sultana Corporation Metro United International Pictures
| distributor    = Metro-Goldwyn-Mayer|MGM/UA Classics
| released       =  
| runtime        = 99 minutes
| country        = Brazil
| language       = Portuguese 
| budget         =
| gross          = $1.3 million 
}}
 Santa Catarina.   

==Plot==
Nacib (Mastroianni) is the owner of bar in a small town. He meets Gabriela (Braga), a sensual girl, who he is immediately attracted to. Taken by her, he hires her on as a cook. However, Nacib soon grows annoyed by the attention she receives. He proposes to her in the hopes that the attention quells. 

After their marriage, he insists that she dress and behave more prudishly so that men are not so enamored of her. Unfortunately, Gabriela cannot help but stray and Nacib is forced to annul the marriage when he finds her in bed with his friend Tonico (Cantafora). Later, as both Nacib and the town begin to undergo a transformation, Nacib takes in Gabriela as his mistress.

==Cast==
* Sônia Braga as Gabriela
* Marcello Mastroianni as Nacib
* Antonio Cantafora as Tonico Bastos 
* Paulo Goulart as João Fulgêncio
* Ricardo Petráglia as Prof. Josué
* Lutero Luiz as Cel. Manoel das Onças
* Tania Boscoli as Glória 
* Nicole Puzzi as Malvina
* Flávio Galvão as Mundinho Falcão
* Joffre Soares as Cel. Ramiro Bastos
* Maurício do Valle as Cel. Amâncio Leal
* Nildo Parente as Maurício Caires

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 