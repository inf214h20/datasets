Glastonbury Anthems
{{Infobox Film |
  name     =Glastonbury Anthems|
  image          =Glastonbury_anthems.jpg|
  imdb_id        = |
  writer         = |
  starring       = |
  director       = |
  producer       = |
  music          = | 
  distributor    =|
  released   =March, 2005 |
  runtime        = 120 minutes|
  language =English |
  movie_series_label=|
  movie_series   =|
  awards         =|
  budget         =|
}}
Glastonbury Anthems is a DVD featuring live performances from the Glastonbury Festival from 1994 to 2004. The performances on the DVD were voted for by fans on the official festival website. Extras include A Visit to the Glastonbury Greenfields, Glastonbury by Air, a 1995 interview with Michael and Jean Eavis, a photo gallery and footage from the 1971 film Glastonbury Fayre

Producer: Ben Challis.  Co-producer: Caroline McGee.  Executive producers for EMI: Stefan Demetriou and Jo Brooks. Executive producers for the BBC: Mark Cooper and Alison Howe.  Artwork  Alex Creedy. Festival Organisers  Michael Eavis & Emily Eavis. Mastered and authored at Abbey Road Studios. Directors: Gavin Taylor (1994, 1995) Janet Fraser Crook (1997-2004), Declan Lowney (1994-2004) & Phil Heyes (2004). A Visit To The Glastonbury Greenfields directed and Produced by Dorian Williams. Released by EMI.

==Performances== Franz Ferdinand, The Dark of the Matinée (2004) 
#Travis (band)|Travis, Driftwood (Travis song)|Driftwood (2000) 
#Faithless, We Come 1 (2002) 
#Manic Street Preachers, A Design For Life (1999) 
#Moby, Why Does My Heart Feel So Bad (2000) 
#Robbie Williams, Angels (Robbie Williams song)|Angels (1998) 
#Supergrass, Pumping On Your Stereo (2003) 
#Ash (band)|Ash, Shining Light (2002)  The Levellers, One Way (1994) 
#Primal Scream, Rocks (2003) 
#Elastica, Connection (1995) 
#The Chemical Brothers, Hey Boy Hey Girl (2000) 
#Basement Jaxx, Good Luck (2004) 
#Coldplay, Yellow (Coldplay song)|Yellow (2002) 
#Fun Lovin Criminals, Scooby Snacks (1999) 
#The Prodigy, Breathe (1997) 
#Blur (band)|Blur, This Is A Low (1994) 
#Placebo (band)|Placebo, The Crawl (1998) 
#Radiohead, Karma Police (1997) 
#Paul McCartney, Hey Jude (2004)

==See also==
* Glastonbury (film)|Glastonbury
*Glastonbury Fayre
* Glastonbury the Movie

==External links==
* 
* 

 

 
 
 
 
 
 


 
 