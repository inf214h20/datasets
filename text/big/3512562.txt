Dingo (film)
 
 
{{Infobox film
| name           = Dingo
| image          = Dingo (film).jpg
| image_size     =
| caption        = 
| director       = Rolf de Heer Marc Rosenberg
| writer         = Marc Rosenberg
| narrator       =
| starring       = Colin Friels Miles Davis Onzy Matthews
| music          = Miles Davis Michel Legrand
| cinematography = Denis Lenoir
| editing        = Suresh Ayyar
| studio         = Gevest Australia Productions Greycat Films Umbrella Entertainment
| released       = 31 January 1992
| runtime        = 109 minutes
| country        = Australia
| language       = English
| budget         = A$5 million 
| preceded_by    =
| followed_by    =
}}
 Marc Rosenberg. It traces the pilgrimage of John Anderson (played by Colin Friels), an average guy with a passion for jazz, from his home in outback Western Australia to the jazz clubs of Paris, to meet his idol, jazz trumpeter Billy Cross (played by legendary trumpeter Miles Davis). In the films opening sequence, Davis and his band unexpectedly land on a remote airstrip in the Australian outback and proceed to perform for the stunned locals. The performance was one of Davis last on film.

==Production==
The movie was filmed in Meekatharra, Perth, and Sandstone, Western Australia, as well as Paris, France. 

==Music==
 
Davis, who plays the role of Cross, provided the Dingo (soundtrack)|films soundtrack in cooperation with Michel Legrand.

==Box office==
 
Dingo grossed $132,500 at the box office in Australia. 

==Home Media==
Dingo was released on DVD by Umbrella Entertainment in July 2005. The DVD is compatible with all region codes and includes special features such as a new 5.1 channel soundtrack, trailers, and an image gallery.   

==See also==
* Cinema of Australia
* South Australian Film Corporation

==References==
 

==External links==
* 
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 


 
 