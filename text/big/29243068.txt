The Night Invader
 
 
{{Infobox film
| name           = The Night Invader
| image          = 
| image_size     = 
| caption        = 
| director       = Herbert Mason
| producer       = Max Milder
| based on       =   Brock Williams David Farrar Ronald Shiner
| music          = Jack Beaver
| cinematography = Otto Heller
| editing        =  Warner Brothers UK
| released       =  
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
}}
 directed by David Farrar. produced by Warner Brothers First National Productions.

==Plot==
Dick Marlow, a British agent, has parachuted into the occupied Netherlands to retrieve vital documents.  Whilst on the trail of the papers, he poses occasionally as an American journalist and a Gestapo Officer (armed forces)|officer.  He meets and falls in love with a Dutch woman who professes solidarity with the British, but matters become complicated and dangerous when it transpires that the womans brother is in possession of the documents Dick Marlow needs, and is far less kindly disposed towards the British than his sister &ndash; or is she?

==Availability==
No print of The Night Invader is known to survive and the film is classed as "missing, believed Lost film|lost". 

==Cast==
* Anne Crawford as Karen Lindley David Farrar as Dick Marlow
* Sybille Binder as Baroness von Klaveren
* Carl Jaffe as Count von Biebrich
* Marius Goring as Oberleutnant
* Jenny Lovelace as Liesje von Klaveren
* Kynaston Reeves as Sir Michael
* George Carney as Conductor
* Ronald Shiner as Witsen Martin Walker as Jimmy Archer

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 