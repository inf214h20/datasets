Patel Ki Punjabi Shaadi
{{Infobox film
| name           = Patel Ki Punjabi Shaadi 
| image          = 
| alt            = Theatrical release poster
| caption        = Theatrical release poster
| director       = Sanjay Chhel
| producer       = Bharat Patel
| writer         = Sanjay Chhel
| editing        = Sanjay Sankla
| starring       = Paresh Rawal  Rishi Kapoor Vir Das  Payal Ghosh Prem Chopra
| music          = Lalit Pandit   Uttank Vora
| cinematography = Neelabh Kaul 
| studio         = Bholenath Movies
| released       = 
| country        = India
| language       = Hindi
| gross          = 
}}

Patel Ki Punjabi Shaadi  ( , Paresh Rawal, Vir Das, Payal Ghosh and Prem Chopra.  This film is directed by Sanjay Chhel and produced by Bharat Patel of Bholenath Movies. Rishi Kapoor and Paresh Rawal will be seen together for the first time in 20 years. Patel Ki Punjabi Shaadi is scheduled to release in 2015.

==Plot==
The story is about a Punjabi, played by Rishi Kapoor and a Gujarati, played by Paresh Rawal.  South Indian Actress Payal Ghosh will debut as Paresh Rawals daughter. Her debut Telugu movie was opposite Telugu star Mohan Babu’s son Manoj Manchu. Payal plays the love interest of Rishi Kapoors son, to be played by Vir Das.

==Cast==
*Rishi Kapoor                                                                                              
*Paresh Rawal                                                                                              
*Vir Das                                                                                                      
*Prem Chopra                                                                                              
*Payal Ghosh                                                                               
*Jinal Belani
*Divya Seth                                                                                                 
*Bharati Achrekar                                                                                      
*Tiku Talsania

== References ==
 
* http://timesofindia.indiatimes.com/entertainment/hindi/bollywood/news/Rishi-Kapoor-and-Paresh-Rawals-Patel-Ki-Punjabi-Shaadi-out-before-Valentines-Day-2015/articleshow/44956025.cms
* http://indiatoday.intoday.in/story/rishi-kapoor-dengue-malaria-lilavati-hospital-bandra-west/1/398375.html
* http://www.mid-day.com/articles/rishi-kapoor-paresh-rawal-back-together-after-20-years/204156
* http://www.bollywoodhungama.com/moviemicro/cast/id/505815

==External links==
*  
*  
*   

 
 
 


 