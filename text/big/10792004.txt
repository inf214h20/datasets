Enkeyo Ketta Kural
{{Infobox film
| name = Enkeyo Ketta Kural
| image =
| caption =
| director = SP. Muthuraman
| writer = Panju Arunachalam
| starring =  
| producer = Meena Panju Arunachalam
| music = Ilaiyaraaja
| cinematography = Babu
| editing = R. Vittal
| studio = P. A. Art Productions
| distributor = P. A. Art Productions
| released = 14 August 1982
| runtime =
| country = India Tamil
| budget =
}}
 Ambika and Radha playing Meena as Rajinis daughter. The movie proved that Rajini is not only an action hero, but an excellent actor when it comes to business. The movie did only below average business.

==Plot==
Ambika and Radha are Rajinis cousins. Rajini likes Ambika but she does not because she wanted a wealthy life but Radha loved Rajini. Rajini did not accept her because of her silly things and she is younger than Ambika. Then Ambika was forced to marry him and to give birth to a child (Meena). Then she ran away with her boss son. Later everyone was sad and ashamed. Ambikas father wanted Rajini to marry Radha so that the child (Meena) would have a mother. He agrees and marries her. They lead a happy life. During the climax, Ambika reforms and wants to see her child (Meena). They saw each other and became to come closer. In the end, Ambika dies and Rajini does the funeral.

==Cast==
* Rajinikanth as Kumaran Ambika as Ponni (1st wife) Radha as Kamatchi (2nd wife) Meena as child Meena (1st wife’s daughter)
* Delhi Ganesh as Vishwanathan (Ponni and Kamatchis father)

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Jency || Panchu Arunachalam || 04:27
|-
| 2 || Nee Paadum Paadal || S. Janaki || 04:13
|-
| 3 || Pattu Vanna Selaikaari || Malaysia Vasudevan || 04:18
|- Vaali || 04:25
|}

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 


 