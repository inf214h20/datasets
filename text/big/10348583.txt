Undisputed II: Last Man Standing
 
{{Infobox film
| name           = Undisputed II: Last Man Standing
| image          = UndisputedIIposter.jpg
| caption        =
| director       = Isaac Florentine
| producer       = Boaz Davidson Lati Grobman Kevin Kasha Avi Lerner Trevor Short John Thompson David Varod
| writer         = James Townsend David N. White
| starring       =  
| music          = Stephen Edwards
| cinematography = Ross W. Clarkson
| editing        = Irit Raz
| studio         = Millennium Films Nu Image
| distributor    = New Line Cinema
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = US$8 million 
| gross          =
}}
Undisputed II: Last Man Standing is a 2006  , which continues the story of Russian prison fighter Yuri Boyka, played by Adkins.

==Plot==
Visiting the Russian Federation for a series of boxing matches, George "Iceman" Chambers is subsequently framed for possession of cocaine and sent to prison. There, he discovers a series of illegal mixed martial arts matches dominated by inmate Yuri Boyka. The prison officials arrange these fights and place large side-bets to make a personal profit, often at the expense of the fighters. He shares a cell with Steven Parker, a British junkie. Mob boss Gaga and Warden Markov tell Chambers that if he fights Boyka, he will likely get an expedited appeal and early release. Chambers initially refuses, but after spending time in demeaning physical labor in the prisons sewer system and experiencing firsthand the brutality of the guards, he reluctantly agrees. He is rescued from both forms of humiliation by a wheelchair-bound inmate named Crot. Both fighters train hard for the match, though Chambers still relies on his boxing background while Boyka prepares a series of deadly kicks, throws, and grappling combinations designed to humiliate his opponent in the ring. Prior to the fight however, Boykas gang force Steven, who is acting as Chambers cornerman, to spike his water with a light sedative during the fight.
 former soldier fighting style.

Once the rematch begins, it is apparent to all the viewers that the combatants are evenly matched and Chambers is more impressive in his style. The fight is long and intense, with flurries of combinations, grapples, and throws traded between the two. Eventually, Chambers realizes that Boyka will not lose consciousness, will not submit, and will likely knock him out if the fight goes on too long. Chambers alters his strategy and manages to get Boyka in a joint lock and ends the fight by breaking Boykas leg, proving that he is the undisputed new champion of the prison. Shortly thereafter, Chambers is released from prison and uses his winnings to buy Crots freedom as well. In a final scene, he wheels Crot to a train station to meet with his estranged niece in a happy reunion. Crot thanks Chambers for giving him the remainder of the winnings to start his life again, while Chambers expresses his gratitude for the help and training. Crot then meets with his long lost niece and the two embrace.

==Cast==
*Michael Jai White as George Chambers
*Scott Adkins as Yuri Boyka
*Eli Danker as Crot/Nikolai
*Ben Cross as Steven Parker
*Mark Ivanir as Gaga
*Valentin Ganev as Warden Markov
*Ken Lerner as Phil
*Silvio Simac as Davic
*Violeta Markovska as Crots niece

==Reception==
Mark Pollard of Kung Fu Cinema gave Undisputed II: Last Man Standing four out of five stars, calling it "the first great martial arts movie of 2007 and Isaac Florentine’s best to date."   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 