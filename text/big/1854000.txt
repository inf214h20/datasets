The Bourne Ultimatum (film)
 
 
{{Infobox film
| name                 = The Bourne Ultimatum
| image                = The Bourne Ultimatum (2007 film poster).jpg
| caption              = Theatrical release poster
| director             = Paul Greengrass
| producer             = {{Plain list| Frank Marshall
* Patrick Crowley
* Paul L. Sandberg
}}
| screenplay           = {{Plain list|
* Tony Gilroy
* Scott Z. Burns
* George Nolfi
}}
| story                = Tony Gilroy
| based on             =  
| starring             = {{Plain list|
* Matt Damon
* Julia Stiles
* David Strathairn
* Scott Glenn
* Paddy Considine
* Édgar Ramírez
* Albert Finney
* Joan Allen
}} John Powell Oliver Wood Christopher Rouse
| production companies = {{Plain list|
* MP BETA Productions
* The Kennedy/Marshall Company
* Ludlum Entertainment
}}
| distributor          = Universal Pictures
| released             =  
| runtime              = 115 minutes 
| country              = {{Plain list|
* Germany   
* United States 
}}
| language       = English
| budget         = $110 million   
| gross          = $442.8 million 
}}
 action spy spy Thriller thriller film novel of The Bourne The Bourne The Bourne Legacy, was released in August 2012.
 CIA assassin and psychogenic amnesiac Jason Bourne.   In the film, he continues his search for information about his past before he was part of Operation Treadstone and becomes a target of a similar assassin program.
 Best Sound Best Sound Editing.

==Plot==
 pursuit by CIA Deputy Deputy Director Director Ezra Kramer (Scott Glenn). Meanwhile, in Turin, journalist Simon Ross (Paddy Considine) of The Guardian meets a paid informant to learn about Jason and Operation Blackbriar, the program succeeding Treadstone.  The CIA tracks Ross as he returns to London, after his mention of "Blackbriar" during a cell-phone call to his editor is detected by the ECHELON system. Jason reappears in Paris to inform Martin Kreutz (Daniel Brühl), the step-brother of his girlfriend Marie Helena Kreutz (Franka Potente) of her assassination in India.

Jason reads Rosss articles and arranges a meeting with him at London Waterloo station. Jason realizes that the CIA is following Ross and helps him evade capture, but Ross deviates from Jasons instructions and is executed by Blackbriar assassin Paz (Edgar Ramirez), on orders of Deputy Director Noah Vosen (David Straithairn). Vosens team, reluctantly assisted by Landy, analyzes Rosss notes and realize Neal Daniels (Colin Stinton), a CIA Station chief involved with Treadstone and Blackbriar, was his source. Jason makes his way to Danielss office in Madrid but finds it empty. He incapacitates gunmen sent by Vosen and Landy. Nicolette "Nicky" Parsons (Julia Stiles), a former Treadstone technician who shares a history with Jason, tells him that Daniels has fled to Tangier and aids his escape from an arriving CIA unit.

Nicky learns that Blackbriar "asset" Desh Bouksani (Joey Ansah) has been tasked with killing Daniels. Vosen sees that Nicky accessed information about Daniels and sends Bouksani after Nicky and Jason as well, a decision with which Landy fiercely disagrees. Jason follows Bouksani to Daniels but fails to prevent Danielss death by a planted bomb. However, Jason manages to kill Bouksani before he can kill Nicky. After sending Nicky into hiding, Jason examines the contents of Danielss briefcase and finds the address of the deep-cover CIA bureau in New York City, where Vosen directs Blackbriar. Jason travels to New York.
 birthdate is rendition team to capture him. Instead, Jason breaks into Vosens office and steals classified Blackbriar documents. Realizing that he has been had, Vosen sends Paz after Jason, resulting in Paz forcing Jasons car to crash into a concrete barrier.  Jason holds Paz at gunpoint before sparing his life.

Jason arrives at a hospital at 415 East 71st Street, memories of which were triggered by the false birthdate that Landy had given him earlier. Outside, Jason meets Landy and gives her the Blackbriar files before going inside. Vosen figures out Landys code and warns Dr. Albert Hirsch ( .

Some time later, Nicky watches a news broadcast about the exposure of Operation Blackbriar, the arrests of Hirsch and Vosen, a criminal investigation against Kramer, and the whereabouts of David Webb a.k.a. Jason Bourne. Upon hearing that his body has not been found after a three-day search of the river, Nicky smiles. Jason is shown swimming away after his fall.

==Cast==
 
* Matt Damon as Jason Bourne, a former operative for the black ops Operation Treadstone.
* Julia Stiles as Nicolette "Nicky" Parsons, Bournes former Treadstone contact in Paris. 
* David Strathairn as Noah Vosen, CIA Deputy Director in charge of the new Treadstone black ops upgrade called Operation Blackbriar.
* Scott Glenn as Ezra Kramer, Director of the CIA.
* Paddy Considine as Simon Ross, a reporter for The Guardian who has been investigating Treadstone.
* Edgar Ramirez as Paz, a Blackbriar assassin.
* Albert Finney as Dr. Albert Hirsch, the psychologist who oversaw Treadstones behavioral modification program.
* Joan Allen as Pamela "Pam" Landy, CIA Deputy Director and Task Force Chief, sent in to aid Vosen in tracking down Bourne.
 Corey Johnson plays Ray Wills, Vosens deputy at Operation Blackbriar. Daniel Brühl plays Martin Kreutz, Maries brother. Joey Ansah plays Desh Bouksani, a Blackbriar asset tasked to kill Bourne in Tangier. Colin Stinton plays Neil Daniels, CIA Station Chief in Madrid and a former member of Treadstone, who observed David Webbs initiation into the project and his transition to Jason Bourne. Lucy Liemann plays Lucy, a Blackbriar technician. Franka Potente has an uncredited appearance in a flashback as Marie Helena Kreutz, Bournes murdered girlfriend.

==Production==
  between October 2006 and April 2007]]
The Bourne Ultimatum was filmed at Pinewood Studios near London and in multiple locations around the world, including Tangier, London, Paris, Madrid (as itself and double for Turin), Berlin (as double for Moscow), New York City including the Springs Mills Building (as the deep cover CIA offices), and other locations in the U.S.  

Tony Gilroy, who had co-written the screenplays of the first two Bourne films, had intended The Bourne Supremacy to emphasise Bournes repentance and atonement for his murders, but felt that the released film omitted this focus.    Gilroy was persuaded to write an initial draft of The Bourne Ultimatum, but did not participate further, and as of 2009 had not watched the finished film.   Gilroys screenplay draft was subsequently criticized by Matt Damon. 

Tom Stoppard wrote a draft of the screenplay, later saying "I dont think theres a single word of mine in the film." 

Paul Greengrass spoke about the characterization of Jason Bourne in The Bourne Ultimatum shortly before its release:
  }}

===References to previous films===

====Within the series==== director Paul Greengrass confirmed the following scenes were deliberate allusions to scenes from the previous installments of the Bourne film franchise.  They include:
* The opening chase sequence of The Bourne Ultimatum is a continuation of the Russian police attempts to capture Bourne in Moscow near the end of The Bourne Supremacy and takes place soon after Bournes apology to Neskis daughter in the previous film.
* The scene where Bourne tells Maries brother, Martin, of his sisters death is very similar to the ending of The Bourne Supremacy, when Bourne apologizes to the Neskis daughter for killing her parents.
* The scene where Bourne crashes through a window to attack Desh is similar to the scene where Castel attacked Bourne in The Bourne Identity.
* After Bourne tells Nicky she will have to run, Nicky dyes and cuts her hair, similar to the scene in The Bourne Identity with Marie dyeing and cutting her hair. She even cuts and dyes her hair into an identical style.
* During the car chase with Paz, Bournes car is destroyed in a similar fashion to Kirills in the climax of The Bourne Supremacy. The sequence also includes similar staging, such as Bourne walking up to Paz with gun in hand but deciding not to shoot.
* In the rooftop climax, Bourne tells Paz, "Look at us. Look at what they make you give", reiterating the dying words of The Professor (Clive Owen) in The Bourne Identity.
* The scene at the end of The Bourne Supremacy in which Bourne tells Landy she "looks tired" is replayed with minor variations in The Bourne Ultimatum. It is followed by his brisk walk down the block. However, in Ultimatum, hes identified there.
* The ending of The Bourne Ultimatum, with Bourne floating motionless in the East River, links the opening scene of The Bourne Identity, which utilizes a similar image.  The music in both scenes is also repeated.
* Operation Blackbriar is referred to at the very end of The Bourne Identity by Conklins superior, Ward Abbott, but not mentioned whatsoever in the The Bourne Supremacy. The Bourne Identity. He is identified here as "Robert Golding" and is labeled "US Citizen Classified."
* Bourne says "This is real" on two occasions: in The Bourne Supremacy to Marie when she questions whether the man he spotted in Goa was actually an assassin; and in The Bourne Ultimatum when trying to convince the journalist Ross that he is in danger at Waterloo Station. The two characters spoken to are both killed by a shot from a sniper soon afterwards.

====Outside the series==== The French Connection. 

* The scene of the explosion in Tangier and the reaction of peoples and Nicky at the cafe is, according to Greengrass, an homage to the La battaglia di Algeri di Gillo Pontecorvo another one of his favorite films. 

==Music==
  John Powell. A new version of Mobys "Extreme Ways", entitled "Extreme Ways (Bournes Ultimatum)", was recorded for the films end credits.

==Release== Harkins Bricktown Bricktown Theaters  to benefit The Childrens Center, located in suburban Bethany, Oklahoma|Bethany. The film was shown simultaneously on three screens. Matt Damon was at the event to greet guests.
* UK premiere — The film premiered at Leicester Square in London on August 15, 2007, with Matt Damon, Julia Stiles and Joan Allen attending. The film was released the next day.  State Theatre, with Matt Damon attending. Frank Marshall and actor Matt Damon were in attendance.  The first two films, The Bourne Identity and The Bourne Supremacy, also had advance charity screenings in Boise.

The Bourne Ultimatum was released nationwide on August 30, 2007. 
* Home Video Release — The film was released on both DVD and   Widescreen aspect ratios. The HD DVD and DVD special features    include several deleted scenes, featurettes, audio commentary, and exclusively on the HD DVD version, HDi Interactive Format features such as Picture-in-Picture Video Commentary.
 Swiss Bank safe deposit box packaging including foreign currency and a Jason Bourne passport.  
 lossless and Dolby Digital Plus 5.1 audio options. 

==Reception==
  Guardians of the Galaxy in 2014. 

On the review aggregator Rotten Tomatoes, the film had an overall approval rating of 94% based on 234 reviews and an average score of 8/10, higher than both predecessors.   The sites consensus describes the film as "(...) an intelligent, finely tuned non-stop thrill ride. Another strong performance from Matt Damon and sharp camerawork from Paul Greengrass make this the finest installment of the Bourne trilogy,"    At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 85 based on 38 reviews, again higher than the first two films.   At the end of its theatrical release, the film grossed at total of $227,471,070 in the U.S. and $215,353,068 in foreign markets for a worldwide total of $442,824,138, making it the highest grossing film in the series. 

Like its predecessor, The Bourne Supremacy, the film was criticized for its use of "shaky camera" work, as Richard Corliss of Time (magazine)|Time magazine, in an otherwise positive review, wondered "why, in the chat scenes, the camera is afflicted with Parkinsons? The film frame trembles, obscures the speaker with the listeners shoulder, annoys viewers and distracts them from the content of the scene." 

In the British press, the inclusion of a fictional journalist from the real British paper The Guardian and scenes set in the United Kingdom (particularly Waterloo railway station) were commented upon. In particular, that newspapers reviewer joked that "dodging bullets from a CIA sniper... is the sort of thing which happens to us Guardian journalists all the time."    
 hacker subculture, as it showed actual real-world applications such as the Bourne-again shell and Nmap, unlike many other films featuring hacking scenes (such as Hackers (film)|Hackers). 

===Top ten lists===
The film appeared on several critics top ten lists of the best films of 2007. 
* 1st&nbsp;— Empire (magazine)|Empire
* 1st&nbsp;— Best Action/Adventure, Rotten Tomatoes 
* 2nd&nbsp;— Claudia Puig, USA Today
* 2nd&nbsp;— Steven Rea, The Philadelphia Inquirer
* 2nd&nbsp;— Joshua Rothkopf, Time Out New York
* 9th&nbsp;— Rene Rodriguez, The Miami Herald
* 10th&nbsp;— Christy Lemire, Associated Press 

===Academy Awards=== No Country for Old Men):  Christopher Rouse Best Film Editing    David Parker Best Sound Mixing  Best Sound Editing 

===Other awards===
* ITV3 Crime Thriller Award for Film of the Year, 2008 

==Sequel==
In May 2007, prior to the release of The Bourne Ultimatum, Matt Damon claimed that he would not be interested in returning for a fourth Bourne film, stating (of his participation in the Bourne franchise): "We have ridden that horse as far as we can."   Damon said in August 2007:
 
However, on February 22, 2008, Variety (magazine)|Variety reported that a fourth film was indeed in the works, with both Damon and Greengrass on board. 

On October 16, 2008, it was announced that George Nolfi would write the script, with Frank Marshall producing, and Jeffrey Weiner and Henry Morrison executive producing. Matt Damon, Julia Stiles, Joan Allen, and Paul Greengrass were also attached to the film.    Joshua Zetumer had been hired to write a parallel script&mdash;a draft which could be combined with another (Nolfis, in this instance)&mdash;by August 2009 since Nolfi would be directing The Adjustment Bureau that September.   That December, Greengrass announced that he had decided not to direct the fourth Bourne film, saying that "  decision to not return a third time as director is simply about feeling the call for a different challenge." 

On February 1, 2010, Damon, speaking at the UK premiere of Invictus (film)|Invictus, revealed that a follow-up to The Bourne Ultimatum was "at least five years away". Greengrass, also at the premiere, re-stated that he would not be part of any further Bourne films "unless the right script came along". However, Damon revealed that in the meantime there may be a Bourne "prequel of some kind, with another actor and another director".  Matt Damon reconfirmed this on a March 10, 2010 appearance of Today (NBC program)|Today and that he would only be involved if Greengrass was directing. 

In June 2010, it was announced that Tony Gilroy would be writing The Bourne Legacy and it would have a 2012 release date.   That October, Gilroy was announced as the director of The Bourne Legacy;  he confirmed that Damon would not return for this film and that there would be "a whole new hero":
 
 The Bourne Legacy was released in the U.S. on August 10, 2012. 

==See also==
*List of films featuring surveillance

==References==
 

==External links==
 
*  
*  
*   on UGO Networks|UGO.com

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 