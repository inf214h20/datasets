101 Chodyangal
{{Infobox film
| name = 101 Chodyangal
| image = 101_Chodyangal.jpg
| caption = Official poster
| director = Sidhartha Siva 
| producer = Thomas Kottakkakam
| writer = Sidhartha Siva
| based on =  
| narrator =
| starring = Indrajith Sukumaran Lena Abhilash Jagathy Sreekumar Minon
| music = Songs:  
| cinematography = Prabhath E K
| editing = Bibin Paul Samuel
| studio =7E Studios
| distributor =
| released =  
| runtime = 
| country = India
| language = Malayalam
| budget = 
| gross =  
}} Best First Best Child Artist (Minon).      It also won the Silver Crow Pheasant Award for Best Feature Film (Audience Prize) at the 18th International Film Festival of Kerala.

The film is set in Kaviyoor, Tiruvalla, directors hometown in Kerala, which inspired the film. The story revolves around a school assignment given to a class V student to frame 101 questions. 

The film is slated to release in theatres in April 2013.

The movie received a lot of awards and appreciations with the support of All Lights Film Services (ALFS) . 

==Plot==
A Class V student is given an assignment to frame 101 questions. The film is about the efforts he has to put in to frame those questions. At this point, his father, who is a factory employee, loses his job. The two parallel streams converge at the end.

==Cast==
* Indrajith Sukumaran as Mukundan
* Minon as Anilkumar Bokaro
* Lena Abhilash as Sati
* Nishanth Sagar as Radhakrishnan
* Sudheesh
* Baby Diya as Anagha
* Rachana Narayanankutty
* Murugan as Sivanandan
* Manikandan Pattambi

==References==
 

==External links==
*  

 

 
 
 
 
 


 