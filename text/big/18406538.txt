The Blind Princess and the Poet
 
{{Infobox film
| name           = The Blind Princess and the Poet
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Harriet Quimby
| starring       = Blanche Sweet
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 18 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent drama film directed by D. W. Griffith and starring Blanche Sweet.   

==Cast==
* Blanche Sweet - The Princess Charles West - The Poet Charles Gorman
* Francis J. Grandon
* Guy Hedlund
* Grace Henderson
* Florence La Badie
* Jeanie Macpherson
* W. Chrystie Miller
* Alfred Paget

==See also==
* List of American films of 1911
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 