The Taking of Beverly Hills
{{Infobox film
| name = The Taking of Beverly Hills
| image = Beverlyhillsposter.jpg Promotional film poster
| director = Sidney J. Furie
| producer = Graham Henderson
| screenplay = Rick Natkin David Fuller David J. Burke
| story = Sidney J. Furie Rick Natkin David Fuller
| starring = Ken Wahl Matt Frewer Harley Jane Kozak Robert Davi
| music = Jan Hammer
| cinematography = Frank E. Johnson
| editing = Antony Gibbs
| studio = Nelson Entertainment
| distributor = Columbia Pictures
| released =  
| runtime = 95 min.
| country = United States English
| budget = $19 million
| gross = $939,277 (USA) 
}}
The Taking of Beverly Hills is a 1991 American action film, directed by Sidney J. Furie. The film stars Ken Wahl, Matt Frewer, Harley Jane Kozak and Robert Davi.  In the film, football hero Boomer Hayes (Wahl) battles a group of ex-cops, who are using a chemical spill as a front to rob several homes and bank vaults in Beverly Hills. The film also features Pamela Anderson in her first film part in an uncredited role playing a cheerleader.

==Plot==
One night in Beverly Hills, California, a truck carrying hazardous materials crashes, releasing a deadly chemical. The citizens of Beverly Hills are sent to quarantine in a hotel in Century City, while the police and the EPA agents stay behind to keep an eye on the valuables and clean up the town.

However, the spill is a cleverly executed hoax masterminded by the head of L.A.s football team, Robert Bat Masterson. The police officers  and DEA agents are bitter ex-cops eager for a piece of what the citizens have hoarded from them. Within the 70 minutes that it will take for the National Guard to arrive, they plot to loot every home and business in the city.

However, one man has been forgotten in the rush to get everyone out. Aging football player Boomer Hayes was in his hot tub, expecting to get lucky, when his lady friend, Laura Sage went to see what was going on and was taken in the rush to evacuate everyone. The officers thought that "Boomer" was her dog, but checked anyway. After taking care of one of the cops sent to kill him, Boomer is trapped in the hot tub by an officer, but before he can shoot him, hes shot from behind. Ed Kelvin, a cop in on the whole thing but disgusted by the ruthless murder of the Mayor (he was told there would be no killing), fills in Boomer on the whole situation, and Boomer decides to help bring in the real police, who are locked in the stations hazmat suit room. Donning his jersey, injecting cortizone for his bum knee, and enlisting Kelvins help, Boomer will spend the next 70 minutes attempting to stop the robbery and bring Masterson to justice, while evading ex-cops and the hired thug Benitez, who has commandeered a SWAT tank and is gunning for Boomer and Kelvin.

==Cast==
*Ken Wahl as David Boomer Hayes
*Matt Frewer as Officer Ed Kelvin
*Harley Jane Kozak as Laura Sage
*Robert Davi as Robert Bat Masterson
*Lee Ving as Varney
*Branscombe Richmond as Benitez Lyman Ward as Chief Healy
*George Wyner as The Mayor of Beverly Hills William Prince as Mitchell Sage Michael Bowen as L.A. Cop at Roadblock
*Tony Ganios as EPA Man
*Michael Alldredge as Dispatch Sergeant
*Raymond Singer as Mr. Tobeason
*Pamela Anderson as Cheerleader (uncredited)

==Release==
The film was picked up for theatrical distribution by Columbia Pictures after original distributor Orion Pictures ran into financial trouble.  The film was given a limited release in the fall of 1991, grossing $939,277 at the box office.  Despite Wahls presence, and due to rather lackluster advertising, the film bombed on this initial release, but later found an audience when the film was released on VHS.

All quotes are from  

*"The Taking of Beverly Hills is sound and Furie signifying nothing. Director Sydney J. Furie takes a promising premise -- the robbery of the worlds most upscale community while residents are fleeing a fake toxic spill -- and reduces it to an assortment of car stunts. Cars screech, cars crash, cars flip, cars burn, and so on. Take that away and you have Ken "Wiseguy" Wahl. Youll miss the cars." - Richard Harrington, WASHINGTON POST

==Computer game==
A computer game was released in 1991 to coincide with the films theatrical release. The game was an action/adventure hybrid where you could play as both Boomer Hayes and Laura Sage (strangely, Ed was left out) to solve puzzles and defeat bad guys in order to stop the looting of the city.

However, the game was developed and released by Capstone Software, a company notorious for their poorly created movie tie-ins. The game had some significant bugs and poor gameplay, and, like the film its based on, faded into obscurity.

==References==
 

==External links ==
*  
* 
* 

 

 
 
 
 
 
 
 