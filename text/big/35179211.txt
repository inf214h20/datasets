Fire from the Heartland: The Awakening of the Conservative Woman
 
{{Infobox film
| name           = Fire from the Heartland: The Awakening of the Conservative Woman
| image          = Fire from The Heartland DVD cover.jpg
| alt            = 
| caption        = DVD coverart
| film name      = 
| director       = Stephen K. Bannon
| producer       =  
| writer         = Stephen K. Bannon
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = David Cebert
| cinematography = Matthew A. Taylor
| editing        = Matthew A. Taylor
| studio         =  
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American documentary film written and directed by Stephen K. Bannon, and produced by David N. Bossie for Citizens United Productions.    The documentary stars Michele Bachmann, Deneen Borelli, and Ann Coulter, and focuses on female participation in conservative politics.  

==Background== John McCains vice president running mate in the 2008 United States presidential election.     In exploring the Tea Party movement, the film interviews only women. The sole male voice heard in the film is from a clip of an on-air rant by CNBCs Rick Santelli from a February 2009 broaddcast. 

==Synopsis==
The documentary looks at the idea of the conservative political female in the United States and how they have impacted and been impacted by the Tea Party movement. Bannon interviews women from different socioeconomic backgrounds and how this has had an effect on their outlook on life and in politics, as well as what they believe what the future will bring and their opinions on how conservative politics and the Tea Party is portrayed in the media.   

==Cast==
 
*Michele Bachmann
*Deneen Borelli
*Ann Coulter
*S.E. Cupp
*Dana Loesch
*Cynthia Lummis
*Jenny Beth Martin
*Michelle Malkin
*Jamie Radtke
*Phyllis Schlafly
*Jean Schmidt
*Janine Turner
*Michelle Easton
*Sonnie Johnson
*Michelle Moore
 

==Reception==
 
Breitbart.com gave Fire from the Heartland a positive review. 

==References==
 

==External links==
*  
*  

 
 
 
 