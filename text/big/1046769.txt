Speed 2: Cruise Control
 
 
{{Infobox film
| name           = Speed 2: Cruise Control
| image          = Speed two cruise control.jpg
| alt            = The faces of Jason Patric and Sandra Bullock and shown among streaks of diagonal lines in blue and orange.  The top reads "From the director of Speed and Twister " and right side reads "Rush Hour Hits the Water".  The bottom features Sandra Bullocks and Jason Patrics names, followed by "Speed 2" and "Cruise Control" in red text, with film credits underneath.
| caption        = Theatrical release poster
| director       = Jan de Bont
| producer       = Jan de Bont Steve Perry Michael Peyser
| screenplay     = Randall McCormick Jeff Nathanson
| story          = Jan de Bont Randall McCormick
| based on       =  
| starring       = Sandra Bullock Jason Patric Willem Dafoe Temuera Morrison Glenn Plummer
| music          = Mark Mancina
| cinematography = Jack N. Green
| editing        = Alan Cody
| studio         = 20th Century Fox Blue Tulip Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = $160 million http://www.boxofficemojo.com/movies/?id=speed2.htm 
| gross          = $164.5 million 
}} action Thriller thriller film, and a sequel to Speed (1994 film)|Speed (1994). The film was produced and directed by Jan de Bont, and written by Randall McCormick and Jeff Nathanson, based on a story by de Bont and McCormick. Sandra Bullock stars in the film, reprising her role from Speed, while Jason Patric and Willem Dafoe co-star. The film was released by 20th Century Fox on June 13, 1997.

The plot involves couple Annie and Alex taking a vacation in the Caribbean aboard a luxury cruise ship, which is hijacked by a villain named Geiger who hacked into the ships computer system. As they are trapped aboard the ship, Annie and Alex work with the ships first officer to try to stop the ship, which they discover is programmed to crash into an oil tanker.
 Seabourn Legend, the ship on which the film is set. The films final scene, where the ship crashes into the island of Saint Martin, cost $25 million of the films $160 million budget (more than the entire production budget of the first film), and set records as the largest and most expensive stunt ever filmed. Many interior scenes aboard the ship were shot on soundstages in the Greater Los Angeles Area. The films soundtrack featured mostly reggae music, and Mark Mancina returned to compose the film score, which was released as an album 13 years after the films release.
 Worst Re-Make or Sequel".

==Plot==
Alex Shaw is on a motorcycle chase after a vehicle with stolen goods. He eventually catches the driver of the vehicle, but his girlfriend Annie runs into him during her driving test. She finds out that Alex is on the SWAT team after he lied and told her he was a beach cop. As an apology, Alex surprises her with a Caribbean cruise on Seabourn Legend.
 hacks into bridge to first officer, Juliano, that the captain is dead and Juliano is now in charge. He is ordered by Geiger to evacuate the ship, during which Geiger steals jewelry from the ships vault. As passengers evacuate, Drew, a young deaf girl, becomes trapped in an elevator, and a group of people become trapped behind locked fire doors in a hallway filling up with smoke. Annie and Alex try to board the last lifeboat, however, Geiger programs the ship to start moving and the winch lowering the lifeboat gets jammed. Alex jumps onto the boat to rescue the passengers falling off, and Annie and Juliano use the ships gangplank to get the passengers back onto the deck.
 ballast doors. As the ship floods, Alex sees Drew on a monitor after she climbed out of the elevator, and runs to save her. Alex notices Geiger exiting the vault and holds him at gunpoint, but Geiger escapes by closing the fire door in front of Alex. Using the ships intercom, Geiger explains that he designed the ships autopilot system and is taking revenge against the cruise line after getting fired once he got sick from copper poisoning. Geiger again escapes from Alex by attaching a grenade to a door.

The crew notice that Geiger has set the ship to crash into an oil tanker off the coast of Saint Martin. Alex decides to stop the ship by diving underneath it and jamming the propeller with a steel cable. Geiger realizes Alex is trying to stop the ship, so he jams the cable winch while Alex is underwater, causing it to break off the ship and free the cable. Geiger takes Annie hostage and escapes with her on a boat from the back of the ship.

To avoid collision with the oil tanker, Alex and Dante go into the bilge pump room and use the bow thrusters to turn the ship. The ship screeches down the side of the tanker, but manages to withstand the damage, and heads straight into a marina. It then crashes into a Saint Martin town, which eventually brings the ship to a halt. Alex jumps off to rescue Annie and hijacks a speed boat from Maurice, forcing him to chase after Annie. Geiger takes Annie onto a seaplane, and Alex shoots it from the boat with a speargun and reels himself in through the water. Alex climbs onto the plane and rescues Annie, and both escape from the plane on one of its float (nautical)|floats, which falls onto the water. Geiger loses control of the plane and crashes into the oil tanker, causing it to explode. The tanker crew however are safe, because they launched their lifeboat just in time. Annie and Alex travel back to shore in Maurices boat, and Alex gives Annie a wedding ring, asking her if she will "wear it for a while", and she accepts.

==Cast==
* Sandra Bullock as Annie Porter
* Jason Patric as Alex Shaw
* Willem Dafoe as John Geiger ,the main antagonist in the film.
* Temuera Morrison as Juliano
* Brian McCardie as Merced
* Mike Hagerty as Harvey
* Colleen Camp as Debbie 
* Bo Svenson as Captain Pollard
* Enrique Murciano as Alejandro
* Glenn Plummer as Maurice
* Joe Morton (uncredited Cameo appearance|cameo) as Lieutenant McMahon, or "Mac"
* Tamia as Sheri Silver

==Production==

===Background and writing===
{{multiple image total_width = 320 image1 = Sandra Bullock, The Heat, London, 2013.jpg|width1=667|height1=1000 alt1 = A portrait of Sandra Bullock wearing black dress, with paparazzi standing in the background. image2 = Keanu Reeves (Berlin Film Festival 2009).jpg|width2=928|height2=1557 alt2 = A candid portrait of Keanu Reeves wearing a gray suit. footer = Speed starred Sandra Bullock and Keanu Reeves, both of whom were expected by the studio to reprise their roles in Speed&nbsp;2, however, Reeves eventually declined to appear in the film.
}} Daily Variety}}  and negotiations with the actors began later that year. 

Hundreds of ideas for a sequel were submitted to de Bont, all of which he turned down in favor of his own idea, based on a recurring nightmare he experienced about a cruise ship crashing into an island.     Speed screenwriter Graham Yost had an idea for a film involving a boat, with a Vietnam War-era vessel loaded with weapons that would explode if its ammunition came in contact with water. He also had an idea for a story about a plane that has to fly through the Andes mountains, but cannot ascend above  .    Neither Yost nor Speed producer Mark Gordon were asked to participate in the sequel, although Yost received a "characters created by" credit and Gordon was credited as executive producer for Speed&nbsp;2.    Randall McCormick was hired to write the sequel in 1994 and received a story credit along with de Bont.  McCormick and Jeff Nathanson collectively wrote the screenplay, working back from the idea based on de Bonts nightmare,  while writers Kevin Peterka and Greg Chabot provided additional uncredited work to the screenplay.  Prior to production, details about the film were kept secret, and de Bont refused to confirm rumors about the film taking place on a ship,  although he did state that the sequel would be "funnier".  Speed&nbsp;2 star Jason Patric said that details were kept secret because "people do tend to steal other peoples ideas", but said the sequel is a "very complex movie" and would have "bigger sequences". 

De Bont produced Speed&nbsp;2 with his company Blue Tulip Productions and producer Steve Perry.  Producer Michael Peyser later joined the project during production in late 1996, after joining Blue Tulip as de Bonts partner.  While Speed was produced for $30 million,  the sequel was green-lit at "just under $100 million" due to the larger production and higher cast salaries.  The director began working on the pre-production prior to the release of his previous film, Twister (1996 film)|Twister (1996).    He started location scouting in the Caribbean in May 1996, and chose Saint Martin as the primary filming location because he felt it was unlikely to be subjected to a hurricane, as it was struck by a hurricane the previous year for the first time in 100 years.   De Bont wanted a cruise liner that was luxurious enough to possibly have millions of dollars of jewelry aboard and that was sleek enough for the films poster. He learned about Seabourn Legend in a hotel brochure, and chose the ship for the film after visiting ships from other cruise lines.    

===Casting===
  was cast as Geiger, the films villain.|alt=A portrait of Willem Dafoe wearing a blue shirt.]] Chain Reaction The Devils Advocate (1997), which was filmed at the same time as Speed&nbsp;2, and subsequently toured with his band, Dogstar (band)|Dogstar. He said that Fox was "furious" with his decision and released "propaganda" against him, claiming that he turned down the role to tour with his band, which Reeves stated had nothing to do with his decision.  De Bont said that the character in the sequel was not specific to Reeves and could be played by any young actor, as long as he could have chemistry with Bullock. 
 A Time to Kill.    After McConaughey passed up the role to star in Contact (1997 US film)|Contact (1997),  Bullock suggested Jason Patric, with whom she had wanted to work since seeing his performance in After Dark, My Sweet (1990). De Bont was skeptical of featuring a relatively unknown actor such as Patric, but was reminded by the studio that Bullock and Reeves were also relatively unknown prior to Speed.    He chose Patric based on his role in Sleepers (film)|Sleepers (1996).  Patric was paid a reported $4.5–$8 million for his role in Speed&nbsp;2 and also used his salary to finance a 1998 drama, Your Friends & Neighbors.   After accepting the role, Patric stated that he never saw Speed or had any intentions of seeing it,    and Reeves said he was looking forward to seeing Patric star in the sequel.  After Reeves declined to appear in Speed&nbsp;2, the screenplay was rewritten to remove his character from the story, which de Bont wanted to deal with early in the film.  His absence is explained in the first scene, where Annie talks about how her relationship with Jack did not work out, and mentions her current relationship with Alex (Patric), before his character is introduced in the film.   
 Air Force Once Were Warriors (1994).    Despite not liking the script, Brian McCardie accepted the role as Merced as his agents assured him it would be good for his acting career. 
 Jaguar owner debut album, but said the part was "too perfect for   to resist".  Joe Morton reprised his role from Speed as SWAT lieutenant "Mac" in an uncredited cameo appearance in the beginning of the film.  Speed&nbsp;2 defies all logic|first=Chris|last=Hicks|work=Deseret News|date=June 15, 1997|page=E11}} 

===Filming===
  in the films opening chase scene, and almost died while performing a stunt on the motorcycle during filming.|alt=A red motorcycle parked on a street.]]
 fear of Navy SEALs scuba gear were present inside the tank during shooting, as the actors had to hold their breath during the scene.  According to Bullock, she performed all of her own stunts "except for a quarter of one stunt";  |first=Betsy|last=Pickle|work=The Knoxville News-Sentinel|date=June 13, 1997|page=T11}}  her stunt double worked for only three days during production. 
 West Palm Beach and Miami|Miami, Florida in July 1996 with the anticipation of shooting in each location for several weeks later that year.   However, due to scheduling issues with Patric, production did not take place in West Palm Beach and only filmed in Miami for "just a few days".   The Miami production took place in a gymnasium and a boat hangar at Dinner Key in the Coconut Grove neighborhood of Miami, which were rented by Fox. After spending over $55,000 in repairs to both facilities, Fox refused to pay the $35,000 in rental fees to the City of Miami.  The city sued for the rent since Fox did not seek approval for the repairs, and a compromise was reached when the city credited some of the repair costs, resulting in Fox paying around $26,000 for the rent. 

 , which was used for six weeks of filming during production.|alt=A white cruise ship in the water. Mountains are visible in the background and part of a road is seen in the foreground, indicating that the ship is close to land.]] Hurricane Lili, which was predicted to be headed towards the Florida Keys. Shooting the evacuation scene was put on hold, and the ship was forced to sail to safe waters. The producers rearranged the shooting schedule and shot additional scenes on the ships bridge while sailing towards Cuba. As Hurricane Lili approached Cuba, the ships violent movements caused seasickness among those on board. The ship was again forced to relocate, and sailed from Cuba towards the Gulf of Mexico, and returned to the Keys three days later. The filming of the evacuation sequence continued and took place over the next two weeks. Approximately 30 hoses and the ships fire sprinkler system were used to simulate heavy rainfall in the scene.    De Bont said that during shooting, he learned to "never film a boat from a stationary point of view". To make the ship appear faster, all exterior shots of the ship were filmed from a moving vehicle.    Following the production at sea, de Bont said that filming on water "was 100 percent more difficult than   imagined".   

{{multiple image|align=right|direction=vertical|width=250
|image1=Marigot 2.jpg|alt1=A village on the coast of an island. Small buildings are located throughout the island, with mountains in the background and the ocean on the right. Among the buildings in the foreground is a parking lot adjacent to a marina. A peninsula stretches out into the ocean and boats are on the ocean in the background.|caption1=The finale scene was filmed in the town of Marigot, Saint Martin.
|image2=Speed 2 rail ship.jpg|caption2=A full-scale mock-up of the ships bow, known as the "rail ship" was placed a top a rail and propelled into the set constructed in Marigot. The rest of the ship was added through computer-generated special effects during post-production.|alt2=A full-size model of a cruise ships bow is seen from behind, supported by large frame. The model sits on top of a rail, which is underwater and leads underneath a marina pier. Boats are located in the water near the marina, and small buildings are in the background.}}
 bow and bridge built hull of the Sturgeon Atlantic cargo ship.   of steel were used to construct the bridge ship mock-up, which was 18 percent smaller than the original.     The bridge ship was used in the first part of the finale when the cruise ship is crashing into boats in the harbor prior to hitting the island. A cargo ship was used for the scene because the actual Seabourn Legend could not navigate in the harbors shallow waters.  The second mock-up was a full-scale replica of the Seabourn Legend  bow, known as the "rail ship".  The rail ship was  , about one-third the length of the Seabourn Legend, and weighed  . A   rail was built   underwater, and the rail ship was placed on top and sat on 50 wheels.   
 explosives and hydraulics to ensure the sets structures collapsed precisely. Concrete was also removed from the buildings and replaced with sand-coated balsa wood so the buildings would "crumble" more effectively after being hit by the rail ship.  In the scenes final shot, the ship had to stop within a   area, and was completely successful on the first take.  The five-minute scene cost $25 million to produce, roughly one quarter of the films entire budget,  and set records as both the largest and the most expensive stunt ever filmed.     The remaining two-thirds of the ship was added into the film during post-production by Industrial Light & Magic using computer-generated imagery (CGI). 

Following production in the Saint Martin, filming moved to The Bahamas to shoot the underwater shots in the scene where Alex swims underneath the ship to try to jam the propeller.  A location in the Tongue of the Ocean off the coast of New Providence was chosen for filming due to the clarity of the water.    The scene was filmed underneath a propeller-less barge, and the barges hull was designed to resemble that of the Seabourn Legend. A propeller was later added into the scene using CGI during post-production. To provide a sense of velocity to the scene, the barge was towed by tugboats at one and a half knot (unit)|knots. The production crew did not have a winch system available for the underwater shoot as depicted in the scene, so a pulley system was created by feeding Patric a rope that was attached to the axle of a car that drove along the barge.    After reviewing the dailies from the shooting, de Bont was unhappy with the footage because the water was so clear that "it looked like a swimming pool". The scene was later reshot with divers above the camera dusting the area in front of the lens with sediment to alter the clarity of the footage. 
 Culver City and Valencia, Santa Clarita, California|Valencia, California, respectively. Full-scale replicas of the ships atrium, cabins, and engine rooms were constructed at the soundstages where production took place for over a month. The scene where Alex rescues Drew while the ship is being flooded was filmed by camera operators wearing wet suits inside a tank at Sony Pictures Studios. The set inside the tank was constructed with plywood and included a hydraulic lift that gave the effect that the water level was rising.  Part of the seaplane scene was filmed outdoors in Valencia. The seaplane was suspended from a crane, with its engines and fuel tanks removed to ease its lift, and large fans were used to simulate wind. The outdoor shoot was filmed in one day for a brief, "one- or two-second" shot in the film, according to Nemec.   

===Music===

====Soundtrack====
 
  had a cameo in the film as themselves performing their song "Tell Me Is It True".|alt=The members of UB40 are performing on a stage, most of whom are wearing black. A light focuses on the lead singer in the middle of the image, singing with a microphone in hand. The background of the stage has "UB" and "40" spelled in large letters with an oval shape in the middle.]] demo of their song "Tell Me Is It True", and wanted them to perform it in the film.  Brazilian singer Carlinhos Brown was also chosen to be featured as a performer on the ship because de Bont wanted music that was "lively" and felt that Browns music was "full of energy".  Tamia worked with de Bont and musician Quincy Jones to choose a song for her character to perform in the film, and selected "Make Tonight Beautiful",  which was written by Diane Warren.   
 Common Sense, Tetsuya "TK" Komuro, who made his debut in the United States performing the track, called "Speed TK Re-mix". 

====Score==== chord is sustained and "slithers down" into the opening theme, while the studio logo fades into a traveling shot of the ocean on screen. Originally, the studio was hesitant to feature an altered version of the fanfare, but allowed the alteration after being convinced by de Bont and hearing it performed by an orchestra.   

  Latin feel" cleanse his blood, Mancina felt the scene was "so gross" that he wrote a "slimy theme" for the character, which is distinctively different from the rest of the music.  He mixed the score at the same time the film was being edited,    which caused the music to be constantly re-edited into the film. During the scoring of Speed&nbsp;2, Mancina said in an interview that keeping up with the editing of the film was the "hardest thing   ever done". 
 demo of French horns, nylon string guitar on several cues. The reggae music featured a band with steel drums, in addition to Cuban drums and Latin percussion. De Bont wanted 16 steel drum players, but due to the lack of players available, Mancina used eight drums which were double tracking|double-tracked.  The orchestra had 96 players, which was significantly larger than the orchestra of 63 players that performed the score for Speed.  Music was recorded on an Electro-Voice microphone that allowed the music to be recorded directly to a computer without the need for equalization or dynamic range compression|compression, due to the microphones high clarity. 

Mancinas score was initially not released on CD to avoid competition with sales of the soundtrack album. De Bont made a deal with Virgin Records that the score could not be released until at least six months after the release of the soundtrack.    An official release of the score was not made until 13 years after the films release.   gave the release four out of five stars, saying the album was "perhaps   finest offering of a previously unreleased score", although it also stated that " ome of the action and suspense material in the latter half of the score becomes a bit generic." 

==Reception==
{|class="wikitable sortable" style="float:right; margin:10px 10px 0px 10px; font-size:90%"
|+Worst sequel list rankings
|-
!scope=col| List
!scope=col| Rank
|-
|scope=row| Complex (magazine)|Complex ||   
|-
|scope=row| Total Film ||   
|-
|scope=row| WatchMojo.com ||   
|-
|scope=row| Entertainment Weekly ||   
|-
|scope=row| Moviefone ||   
|-
|scope=row|   ||   
|-
|scope=row| MSN ||   
|-
|scope=row| Salon (website)|Salon ||   
|-
|scope=row|   ||   
|-
|scope=row| Virgin Media ||   
|-
|scope=row| Toronto Sun ||   
|-
|scope=row| Comcast ||   
|}
The film was generally regarded as a critical disaster and received mostly negative reviews. Rotten Tomatoes reported a  % approval score with an average score of   based on   reviews.  The websites consensus reads, "Speed&nbsp;2 falls far short of its predecessor, thanks to laughable dialogue, thin characterization, unsurprisingly familiar plot devices, and action sequences that fail to generate any excitement."     Website Metacritic rated the film "generally unfavorable" based on 22 reviews, with an average score of 23/100.    Time (magazine)|Time said that Patrics character was "fundamentally uninteresting", but blamed de Bont and the screenwriters for "not providing their actors with stuff to act".  Many critics stated that a major issue with the film was the lack of thrill due to the setting of the slow-moving ship. Entertainment Weekly heavily criticized the lack of story and said the film is "as slow-moving as a garbage scow".  According to the Los Angeles Times, even children who saw the film felt it was strange that it took place on a ship "not capable of going more than a few knots per hour  ", and claimed that Speed was "much more logical". 
 New York film critic David Edelstein featured an article on Speed&nbsp;2 that described it as the "Worst Sequel of All", mainly due to the films explanation for the absence of Reeves character.  In addition to being ranked among the worst sequels, Empire (magazine)|Empire ranked the film at number 24 on a list of "The 50 Worst Movies Ever". 

Bullock has since regretted starring in the film, and stated that the script was to blame for the negative reception of the film.  She stated she had doubts about its success during production and "knew it was going to be a big flop" once she saw the final product.  Patric also admitted "it wasnt a good movie" and said that its lack of success was due to de&nbsp;Bonts directing, while praising Bullock and the rest of the films crew.  Mark Gordon and Graham Yost stated they felt "bitter and happy" after initially not being asked to be involved in Speed&nbsp;2, then seeing how the film was unsuccessful. 

Despite the overwhelming negative reviews, the film did receive some positive feedback. Roger Ebert of the Chicago Sun Times and Gene Siskel of the Chicago Tribune both gave Speed&nbsp;2 three out of four stars,     and wrote the films only two positive reviews, respectively, on Rotten Tomatoes.  On their film review TV series Siskel & Ebert, they collectively gave Speed&nbsp;2 a positive rating of "Two Thumbs Up", calling it a "truly rousing ocean liner adventure story", although Ebert criticized Bullocks more limited role in the sequel while Patric "stole all the action sequences".  Since his original review, Ebert claimed that he enjoyed Speed&nbsp;2 more than Bullock,    and wrote an article in 2013 that his favorable review of the film "inspired more disbelief" than any other he had written and was frequently cited as an example of him being a poor film critic.  Speed 3--Winner of My 1999 Contest|work=Chicago Sun-Times|first=Roger|last=Ebert|authorlink=Roger Ebert|date=January 17, 2013|accessdate=June 11, 2013}}  At the Conference on World Affairs in 1999, Ebert spoke about the difficulty of making films such as Speed&nbsp;2 and defended his review by offering a "Speed&nbsp;3" contest for anyone to create a five-minute short film that takes place on something that cannot stop moving. 

Speed&nbsp;2 was listed on About.coms "Top 9 Cruise Ship or Ocean Liner Movies", which said it had " ood shots of the ship and a spectacular ending", but also described the plot as "lame".  The Atlanta Journal-Constitution,  Los Angeles Daily News,  and The Sacramento Bee  all gave favorable reviews, while stating that the film was not as good as Speed. Empire Magazines Andrew Collins gave the film a 3/5 stars while commenting that the film "...top-billed Sandra Bullock, formerly an accidental heroine, is insultingly sidelined here to boyfriends little helper and hostage-in-waiting. Patric is the films actual seabourn legend, and a watchable one, but the pairs gooey relationship sorely lacks Speeds thrown-together dynamic." 

===Release and box office=== Cineplex Odeon Men in Black and Titanic (1997)|Titanic, then moved up one week to avoid competition with Con Air.    

During its opening weekend, Speed&nbsp;2 was shown on 2,615 screens and grossed $16.2 million. Despite its negative reviews, it ranked at number one in the box office, grossing just $500,000 more than Con Air in second place.  Box office sales for Speed&nbsp;2 dropped 54 percent the following weekend, grossing only $7.8 million and ranking at number five. 

The film grossed only $48 million in the United States, and made a total gross of $164.5 million worldwide.    Moviefone and Time have both ranked the film among the biggest box office bombs of all time.  

===Razzie Awards=== Batman and Worst Re-Make Worst Picture" The Postman (1997).   
{| class="wikitable"
|+Razzie Award nominations and wins 
|- Award
! Nominee
! Result
|- Golden Raspberry Worst Picture
| Jan de Bont, Steve Perry, Michael Peyser
|  
|- Golden Raspberry Worst Actress
| Sandra Bullock
|  
|- Golden Raspberry Worst Supporting Actor
| Willem Dafoe
|  
|- Golden Raspberry Worst Screen Couple
| Sandra Bullock, Jason Patric
|  
|- Golden Raspberry Worst Re-Make or Sequel
|  
|  
|- Golden Raspberry Worst Director
| Jan de Bont
|  
|- Golden Raspberry Worst Screenplay
| Randall McCormick, Jeff Nathanson, Jan de Bont
|  
|- Golden Raspberry Worst Song
| "My Dream" (written by  Orville Burrell, Robert Livingston, Dennis Haliburton)
|  
|}

== Legacy == sitcom Father Arthur Mathews a bus Blind Ambition" (2005) includes a parody of the films finale where a cruise ship crashes into a pier and through a city before stopping in the middle of an airport. 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 