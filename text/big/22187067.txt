The Fabulous Baron Munchausen
 
{{Infobox film
| name = The Fabulous Baron Munchausen
| image = Barongc3.jpg
| caption = Japanese DVD cover
| director = Karel Zeman
| producer =
| writer = Karel Zeman Josef Kainar Jiří Brdečka
| starring =
| music = Zdeněk Liška
| cinematography = Jiří Tarantík
| editing =
| distributor =
| released =  
| runtime = 85 minutes (original) 83 minutes (Japanese release) 80 minutes (other releases) 
| country = Czechoslovakia
| language = Czech
| budget =
}}
 romantic adventure adventure film directed by Karel Zeman, based on the tales about Baron Munchausen. The film combines live-action with various forms of animation and is highly stylized, often evoking the engravings of Gustave Doré. 

==Plot==
The film begins with footsteps leading to a pond. The camera continually moves upwards to show the flight of butterflies, birds, and a progression of historical aircraft ending with a rocketship travelling through space and landing on the moon.

The astronaut/cosmonaut leaves his spacecraft and sights other footsteps on the moon leading him to an old phonograph, then a crashed rocket with a plaque reading Jules Vernes From the Earth to the Moon.  Taken to a dinner table, the surprised space traveller meets the characters from Vernes book and Baron Munchausen.  Inviting him to their table, the characters believe that the cosmonaut is a man actually from the moon, and kindly treat him as a small child.
 diplomatic protocol and his falling in love with Princess Bianca, a damsel in distress held prisoner by the Sultan, leads to a series of romantic and fanciful adventures that transform the modern scientific space traveller into a hero rivalling the Baron.

Among the exciting and satiric adventures are sword and sea battles with the Turks, being swallowed by a giant fish, and ending the conflict between two warring kingdoms.

==Cast==
* Miloš Kopecký as Baron Munchausen
* Rudolf Jelínek as Tonik
* Jana Brejchová as Princess Bianca Cyrano de Bergerac
* Naděžda Blažíčková as Court Dancer
* Karel Effa as Adjutant
* Josef Hlinomaz as Spanish General
* Zdeněk Hodr as Nicole
* Miloslav Holub as Enemy General
* Miroslav Homola as Chess Player
* Rudolf Hrušínský as The Sultan
* Eduard Kohout as General Ellemerle
* Otto Šimánek as Ardan
* František Šlégr as Pirate Captain
* Václav Trégl as Sailor
* Jan Werich as Ships Captain
* Bohuš Záhorský as Admiral
* Richard Záhorský as Barbicain

==Reception==
The film was released internationally during the 1960s, including a 1964 American dub under the title "The Fabulous Baron Munchausen."    called the film "a   influence is present throughout the film, which reaches the same level of poetry as the works of that old master."    The film historian Peter Hames described Baron Prášil as "perhaps   finest film."   

When the film was screened at the British Film Institute in the 1980s, it served as an influence for Terry Gilliam, who was then making his own version of the Munchausen stories, The Adventures of Baron Munchausen.     According to Gilliam:

  }}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 