Trouble Backstairs (1949 film)
{{Infobox film
| name = Trouble Backstairs
| image =
| image_size =
| caption =
| director = Erich Kobler
| producer = Willy Zeyn  
| writer =  Maximilian Böttcher (play)   Karl Peter Gillmann       Jo Hanns Rösler    Erich Kobler
| narrator =
| starring = Paul Dahlke   Fita Benkhoff   Ursula Herking   Bruni Löbel
| music = Hans Georg Schütz     
| cinematography = Werner Krien  
| editing =  E. Martin    
| studio = Willy Zeyn-Film 
| distributor = Schorcht Filmverleih 
| released = 4 July 1949
| runtime = 85 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} play of Trouble Backstairs.   

==Cast==
*  Paul Dahlke as August Krüger  
* Fita Benkhoff as Irma Schulze  
* Ursula Herking as Malchen Krüger  
* Bruni Löbel as Edeltraud Panse 
* Traute Rose as Frau Bock  
* Gisela von Jagen as Ilse Bock, ihre Tochter  
* Carl Kuhlmann as Oberpostschaffner Hermann Schulze  
* Ilse Melcher as Paula, seine Tochter  
* Bum Krüger as Gustav Kluge, Bäckermeister  
* Ernst von Klipstein as Assessor Dr. Erich Horn  
* Friedrich Domin as Justizrat Dr. Horn, sein Vater  
* Franz Schafheitlin as Staatsanwalt  
* Walter Janssen as Amtsgerichtsrat Meier 
* Hilli Wildenhain as Frau Puschke  

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1 Jan 1999. 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 