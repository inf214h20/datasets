I Stand Alone (film)
 
{{Infobox film
| name           = I Stand Alone
| image          = SeulContreTous.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gaspar Noé
| producer       = Lucile Hadžihalilović Gaspar Noé
| writer         = Gaspar Noé
| starring       = Philippe Nahon Blandine Lenoir Frankye Pain Martine Audrain
| music          = 
| cinematography = Dominique Colin
| editing        = Lucile Hadžihalilović Gaspar Noé
| studio         = Les Cinémas de la Zone
| distributor    = Rezo Films
| released       =  
| runtime        = 92 minutes  
| country        = France
| language       = French
| budget         = 
}}
I Stand Alone is a 1998 French drama film written and directed by Gaspar Noé, and starring Philippe Nahon, Blandine Lenoir, Frankye Pain and Martine Audrain. The original French title is Seul contre tous, which means "Alone against all". The film focuses on several pivotal days in the life of a butcher facing abandonment, isolation, rejection and unemployment. The film was the directors first feature-length production, and is a sequel to his 1991 short film Carne (film)|Carne.

==Plot==
The history of the butcher, who doesnt have any other name, is narrated through voice-over and a montage of still photographs. Orphaned at a young age, he was sexually abused by a priest. As a teenager, unable to have the opportunity to study and learn a profession of his choice, he reluctantly embraces the career of butcher specialized in horse meat, a profession already frowned upon at the time in France. After several years of hard work, hes finally able to open his own horse meat butcher shop, and his girlfriend gives birth to a daughter. But when the woman realizes the infant is not a boy, she leaves the young father alone with the child. Embracing it as fate, the butcher decides to take care of his daughter alone. But as loneliness grows on the single father, he becomes overprotective and develops incestuous feelings for his child. When he sees blood on her skirt, he stabs the man who he thinks raped his daughter. He later understands that the stains were only menstrual blood. He is sentenced to prison and forced to sell his shop to a Muslim butcher, and his troubled daughter is sent to an institution. In prison, the butcher has sex with a cellmate. Upon his release, he vows to forget it all happened. He finds a job working as a bartender for the woman who owns the tavern where he was a regular customer. They begin dating, and soon she becomes pregnant. As they start making plans for their future together, she sells her business and they move to northern France, where she said she would buy a butcher shop for him.

There, she backs out of her promise. He has to take a night watchman job at a nursing home, where he meets a young and caring nurse, the complete opposite of his aging, careless mistress. As he and the nurse witness together an elderly patient die, the butcher thinks back about the lack of affection throughout his life, from the orphanage to a life with a careless mistress who abuses the power she has over him because of her money. When she unjustly accuses him of having an affair with the nurse, he snaps and punches her in the belly several times, very likely killing their unborn child, then steals a pistol and flees.

He decides to return to Paris, where he rents the same flophouse room where he conceived his daughter, and begins looking for a job as a horse meat butcher. But the customers taste changed during his time in prison, provoking a collapse of the horse meat market. Despite his patience, his job interviews consistently end up with rejection. He then proceeds to broaden his job search, but in general butchery hes considered as an unskilled worker. He has to start all over again at the bottom, which he does, not long before being fired for being too old for the position. He starts looking outside his branch, but the more he broadens his searches, the more humiliating the job interviews become. He remains polite, but the more desperate he becomes, the more quickly hes rejected by managers. When he turns to his old friends for advice, they all reject him. After being turned away at a slaughterhouse that once did business with his shop, the butcher decides to kill the slaughterhouse manager. He plots the murder at a local tavern, but is ejected from the bar at gunpoint after squabbling with the owners son. The butcher finds that he has only three bullets in his gun, and begins assigning them to the men he feels have humiliated him the most.

More and more isolated, he decides to look for the only person he feels has ever loved him, his daughter. After meeting her at the asylum in which she is a patient, he takes her back to his room where hes prey to opposite feelings towards her. As hes about to lose his sanity, he contemplates having sex with his daughter, before killing her. After the representation of this fantasy, the movie returns to the moment of the butchers hesitation. He decides to put the gun away, resolving to be good, and tearfully embraces his daughter. But he starts again to contemplate having sex with her, in the same manner he did with her mother.
Standing at a window, he unzips his daughters jacket and begins fondling her. As he starts to abuse his daughter, the butcher, between more and more incoherent thoughts, tries to justify his act by asserting that the world condemns his love for his daughter only because it is too powerful.

==Cast==
* Philippe Nahon as the butcher
* Blandine Lenoir as his daughter, Cynthia
* Frankye Pain as his mistress
* Martine Audrain as his mother-in-law
* Roland Guéridon as his old friend
* Aïssa Djabri as Dr. Choukroun
* Gérard Ortega as the bar owner
* Alain Pierre as the owners son
* Zaven as the man with morality

==Production==
The film was produced by Les Cinémas de la Zone, a production company run by Gaspar Noé and his girlfriend Lucile Hadžihalilović. It was shot in an unusual combination of 16 mm film and the CinemaScope format. Recording took place sporadically over a period of two and a half years, with frequent budget problems. The fashion designer agnès b. eventually granted a loan which Noé says saved his production company. The gimmick of having a warning text before the storys climax was borrowed from William Castles 1961 film Homicidal. 

According to a 2010 interview, Noé came up with the idea of the butcher character following a conversation he had with his father as a teenager. The  , which is unheard of in Argentina). Noé then decided that a horse meat butcher would make a great character in a film, and this formed the basis for his first short Carne.

==Style==
  interior monologue, spoken in voice-over.

The camera is usually stationary throughout the film, but this trend is sometimes contrasted by abrupt, rapid movements of the camera. The sudden movements are always accompanied by a loud sound effect, usually an explosive gunshot. A notable exception is the final crane shot, which moves gently away from the Butchers window and turns to look down an empty street.
 title cards that display a variety of messages. The cards often repeat a notable word spoken by the Butcher, such as "Morality" and "Justice". At the films climax, a "Warning" title card counts down 30 seconds, to give viewers an opportunity to stop watching and avoid the remainder of the film.

==Film connections==
The film is a sequel to Noés short film Carne (film)|Carne, which is essentially a shortened version. The Butcher also makes a cameo appearance at the beginning of Irréversible, Noes follow-up to I Stand Alone. In a drunken monologue, the Butcher reveals that he was arrested for having sex with his daughter.

==Accolades==
* International Critics Week Award at the 1998 Cannes Film Festival
* Official Selection of Telluride, Toronto, New York, Rotterdam, San Francisco, Sundance film festivals. John Waters annual selection within Maryland Film Festival 2003.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 