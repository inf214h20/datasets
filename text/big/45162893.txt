Michiel de Ruyter (film)
{{Infobox film
| name           = Michiel de Ruyter
| image          = Michiel_de_Ruyter_film_poster.jpg
| alt            = 
| caption        = Dutch film poster
| film name      = 
| director       = Roel Reiné
| producer       = Klaas de Jong
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = Frank Lammers Barry Atsma Egbert-Jan Weber
| narrator       =  
| music          = 
| cinematography = 
| editing        = 
| studio         =  
| distributor    =  
| released       =   
| runtime        = 130 minutes    , Film Totaal. Retrieved on 27 January 2015. 
| country        = Netherlands 
| language       = 
| budget         = € 8 million
| gross          =  
}}

Michiel de Ruyter ( ) is a 2015 Dutch film about the  , 2015. Retrieved on 25 January 2015.  and has been released in cinemas in the Netherlands on 29 January 2015.    , Film Totaal, 2014. Retrieved on 22 January 2015.  On the English promotional website, the film has the title Admiral. 

== Cast ==
  (center, wearing a hat) with cast and crew members at the film set in 2014]]

First choice for the title role was  , 2014. Retrieved on 27 January 2015. 

The actors in starring roles are:
* Frank Lammers as Michiel de Ruyter 
* Barry Atsma as Johan de Witt  William III 

The other actors are:
{{columns-list|2|
* Gene Bervoets 
*  , 2014. Retrieved on 27 January 2015. 
* Daniel Brocklebank 
* Hajo Bruins as Cornelis Tromp 
* Jules Croiset  Charles II 
* Lukas Dijkema 
* Roeland Fernhout as Cornelis de Witt  Joseph van Ghent 
* Rutger Hauer as Maarten Tromp 
* Jelle de Jong 
* Joost Koning 
* Youval Kuipers 
* Isa Lammers 
* Sanne Langelaar as Anna de Ruyter 
* Lieke van Lexmond as Wendela de Witt 
* Derek de Lint as Johan Kievit 
* Victor Löw as De Waerd 
* Aurélie Meriel 
* Filip Peeters 
* Pip Pellens 
* Bas van Prooijen 
* Nils Verkooijen 
* Viv Weatherall 
}}

== Production ==
  in the Netherlands]]

The film was directed by  , 2014. Retrieved on 25 January 2015. 

== Reception == Dutch prince Mary II. 

== Awards ==
* Golden Film for 100,000 visitors 

== References ==
 

== External links ==
 
*  
*  

 

 
 
 
 