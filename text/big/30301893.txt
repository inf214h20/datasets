Dead Line (film)
{{ Infobox film
| original title= Interferencia
| title = Dead Line 
| script and direction = Sergio Esquenazi
| production company = CYK Films
| music = Pablo Monlenzún
| editor = Guille Gati  Hernán Findling
| DP = Matías Lago (ADF)
| wardrobe =  Mad Crampi  Julieta Candia 
| cast = Andrés Bagg Virginia Lustig Oliver Kolker Chris Longo Julio Luparello
| country = Argentina
| length = 90 minutes, approximately
| lenguage = English
| distribución = The Asylum
}}
Dead Line is a 2006 Argentine thriller (genre)|thriller, which also contains horror elements, written and directed by Sergio Esquenazi. It stars Andrés Bagg, Virginia Lustig, Oliver Kolker, Chris Longo and Brent Garlock. It was distributed by The Asylum.

== Production ==

Originally called Interference, Dead Line is a low-budget film produced by Christian Koruk and shot entirely in Buenos Aires, Argentina in English language.
The shoot lasted 10 days and the post-production one month. The film was acquired by The Asylum and distributed in over 30 countries. Moreover, the film was sold to different TV stations in Europe. In general the title Dead Line was respected with few exceptions. In Japan it was called Next and in Mexico and Central America is known as La Voz del Asesino. After the success of Dead Line, various Argentine producers and distributors began producing low budget horror films, in English language.
These films are Directors Cut, Hernán Findling,
Death Knows Your Name, Daniel de la Vega and The Last Gateaway, Demián Rugna, among others.

== Also Known As (AKA) ==

Dead Line
USA (DVD title)
Foniko mystiko
Greece (DVD title)
Interference
International (English title)
Interferencia
Argentina (promotional title)
Linhas Mortais
Brazil (DVD title)
Next
Japan (DVD title) (English title)

=== Cast ===

* Andrés Bagg
	 
* Virginia Lustig
	
* Oliver Kolker
	 
* Chris Longo

* Julio Luparello

* Brent Garlock
	 
* Hugo Halbrich
	 
* Alejandra Lapola	

* Jorge Sabate	

* Ian Duddy

* Jackie Bousquet

== External links ==
*  
*  
*  

 
 
 
 


 
 