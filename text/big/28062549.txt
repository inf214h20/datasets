Just like a Woman (1967 film)
{{Infobox film
| name           = Just Like a Woman
| image          = "Just_Like_a_Woman"_(1967).jpg
| image_size     =
| caption        = British quad poster
| director       = Robert Fuest
| producer       = Bob Kellett (as Robert Kellett)
| writer         = Robert Fuest
| narrator       = Francis Matthews John Wood
| music          =  Kenny Napper Billy Williams
| editing        = Jack Slade
| studio         = Dormar Productions Limited
| distributor    = Monarch Film Corporation  (UK)
| released       = 26 February 1967  (UK)	
| runtime        = 89 mins.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}} Francis Matthews, Dennis Price and Clive Dunn.  The films plot follows a wealthy couple who work in the entertainment industry and decide to separate, but soon begin to miss each other. 

==Cast==
* Wendy Craig - Scilla Alexander Francis Matthews - Lewis McKenzie John Wood - John Martin
* Dennis Price - Bathroom Salesman
* Miriam Karlin - Ellen Newman Peter Jones - Saul Alexander
* Clive Dunn - Graff von Fischer
* Ray Barrett - Australian
* Sheila Steafel - Isolde
* Aubrey Woods - T.V. Floor Manager
* Barry Fantoni - Elijah Stark
* Juliet Harmer - Lewiss Girl Friend Mark Murphy - Singer Michael Brennan - Commissionaire
* Angela Browne - Scillas Friend

==Critical reception== Craig here Not in Front of the Children and Butterflies (TV series)|Butterflies. Robert Fuest|Fuests script strives too hard to be offbeat, however, notably in the creation of a goose-stepping interior designer." 

==References==
 

==External links==
* 
 

 
 
 
 
 
 


 
 