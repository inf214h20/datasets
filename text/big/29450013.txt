Mickey's Explorers
{{Infobox film
| name           = Mickeys Explorers
| image          =
| caption        =
| director       = Albert Herman
| producer       = Larry Darmour
| writer         = Fontaine Fox
| narrator       =
| starring       = Mickey Rooney Billy Barty Jimmy Robinson Delia Bogard Marvin Stephens Douglas Fox IV
| music          =
| cinematography =
| editing        =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 18 minutes
| country        = United States
| language       = English
| budget         =
}}
 Mickey McGuire series starring a young Mickey Rooney. Directed by Albert Herman, the two-reel short was released to theaters on February 17, 1930 by RKO Radio Pictures|RKO.

==Synopsis==
Mickey and the Gang espire to be like Christopher Columbus and explore new worlds. Mickey competes against Stinkie Davis in order to borrow Mary Anns fathers boat for the exploring. When Mickey loses, he and the Gang use their own boat. Later, the kids travel to a new world (actually a small area close to home) and find themselves up against Native Americans, skeletons, and a real-live bear.

==Notes==
* Re-issued as Mickeys Brigade.

==Cast==
*Mickey Rooney - Mickey McGuire
*Billy Barty - Billy McGuire Jimmy Robinson - Hambone Johnson
*Delia Bogard - Tomboy Taylor
*Marvin Stephens - Katrink Douglas Fox - Stinkie Davis

== External links ==
*  

 
 
 
 
 
 
 


 