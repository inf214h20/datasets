To the Death (film)
{{Infobox film
| name           = To the Death
| image          =
| caption        =
| director       = Burton L. King
| producer       = 
| writer         = Olga Petrova Lillian Case Russell
| starring       = Olga Petrova
| music          =
| cinematography = Harry B. Harris
| editing        = 
| studio         = Metro Pictures
| distributor    = Metro Pictures
| released       =  
| runtime        = Five reels
| country        = United States
| language       = Silent
| budget         =
| gross          =
}}
 silent American drama film directed by Burton L. King, and released by Metro Pictures. 

== Cast ==
* Olga Petrova as Bianca Sylva (as Mme. Olga Petrova)
* Mahlon Hamilton as Etienne Du Inette
* Wyndham Standing as Jules Lavinne
* Henry Leone as Antonio Manatelli
* Evelyn Brent as Rosa
* Violet Reed as The Woman of Mystery
* Marion Singer as Maria
* Boris Korlin as Valet

==Preservation status==
The film is considered to be lost film|lost. 

==References==
 

==See also==
*List of lost films

== External links ==
* 

 
 
 
 
 
 
 
 

 
 