An Image of the Past
{{Infobox film
| name           = An Image of the Past
| image          = An Image of the Past (1915) - 6.jpg
| caption        = Film still with Charles Cosgrave and Seena Owen, and J.H. Allen in the back room. The names of the three child actors are not recorded.
| director       = Tod Browning
| producer       =
| writer         =
| starring       = Seena Owen
| music          =
| cinematography =
| studio         = Majestic Motion Picture Company
| distributor    =
| released       =  
| runtime        = 1 reel
| country        = United States Silent English intertitles
| budget         =
}}
 short drama film directed by Tod Browning.    The survival status of the film is unknown,  suggesting that it may be a lost film.

==Plot==
As recorded in a film magazine,  Jessie Curtis elopes with artist Jack Dexter. Her wealthy father, in a fury, disinherits her. He repulses all of his daughters attempts at reconciliation, and ten years pass. Jack has been very ill and the family is penniless. The three children dress themselves as a gypsy, Indian, and sailor and with face masks, and fo out to sing in the street. They make their way to their grandfathers house and sing below the windows. the old man is having one of his unendurably lonely hours, filled with regretful dreams of Jessie. When he hears the children singing, he calls them in, listens to their story, and promises that he will give them money for their sick father. Suddenly one of the youngsters rushes across the room to a portrait of Jessie that had been painted by Dexter at her fathers order from a photograph of her when she was seven years old. "Why is my sisters picture in your house?" the small boy asks as he removes the mask from his sisters face. The grandfather learns the truth. He returns with the children to the rescue of his daughter and her husband.

==Cast==
* J.H. Allen - Jack Dexter
* Charles Cosgrave - Mr. Curtis
* Seena Owen - Jessie Dexter

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 