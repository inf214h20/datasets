Congo Jazz
 
Congo Jazz is a Looney Tunes cartoon starring Warner Bros. first cartoon star, Bosko. The cartoon was released in September 1930. It was distributed by Warner Brothers and the Vitaphone Corporation. Congo Jazz was the first cartoon to feature Boskos falsetto voice that he would use for the bulk of the series run (the previous Bosko short, Sinkin in the Bathtub, had used a stereotypical Negro dialect). It has the earliest instance of a trombone gobble in animation.

== Credits ==
* Supervision-Hugh Harman and Rudolf Ising
* Musical Score by Frank Marsales
* Animated by Max Maxwell and Paul Smith

== Cast ==
Carman Maxwell: Bosko

== Synopsis ==
As Bosko is hunting in the  es, kangaroos, and more. They play music on themselves, on each other, or with the jungle scenery. A kangaroo plays a tree, monkeys play a giraffe, and an elephant plays its trunk. A tree does a provocative fanny-slapping dance, gyrating its coconut bosoms, until one flies off and hits Bosko in the head. Bosko and a trio of hyenas laugh.

== Songs ==
"When the Little Red Roses Get the Blues for You" arr. Frank Marsales.
"Im Crazy for Cannibal Love"

==References==
* Schneider, Steve (1990). Thats All Folks!: The Art of Warner Bros. Animation. Henry Holt & Co.
* Beck, Jerry and Friedwald, Will (1989): Looney Tunes and Merrie Melodies: A Complete Illustrated Guide to the Warner Bros. Cartoons. Henry Holt and Company.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 