Chef (film)
 
 
{{Infobox film
| name           = Chef
| image          = Chef 2014.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Jon Favreau
| producer       = Jon Favreau Karen Gilchrist Sergei Bespalov
| exec.producer  = Robert Steindorff
| writer         = Jon Favreau
| starring       = {{Plain list | 
* Jon Favreau
* Sofía Vergara
* John Leguizamo
* Scarlett Johansson
* Oliver Platt
* Bobby Cannavale
* Dustin Hoffman
* Robert Downey, Jr.
* Emjay Anthony}} 
| music          = 
| cinematography = Kramer Morgenthau Robert Leighton
| studio         = Fairview Entertainment Aldamisa Entertainment
| distributor    = Open Road Films (US) StudioCanal (Australia)
| released       =  
| runtime        = 114 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $45.9 million 
}}
 Cubanos in various cities along the way.
 Austin and New Orleans. Chef premiered at South by Southwest on March 7, 2014 and was released theatrically on May 9, 2014 by Open Road Films. It grossed over US$45 million at the box office and was well received by critics.

==Plot==
Miami-born Carl Casper is the head chef of Gauloise in Brentwood, California. While popular with his kitchen staff and hostess Molly, the restaurant owner Riva wants Carl to stick to tired "classics" rather than innovative dishes. Carl has a strained relationship with his tech-savvy preteen son Percy and ex-wife Inez.

When Carl has a chance to prove his talents during a visit from prestigious critic and blogger Ramsey Michel, Riva demands that he stick with old favorites at the last minute, causing Carl to concede, leading to a scathing review. On Twitter, Carl insults Ramsey, not realizing that his reply is public, and gains a large Twitter following. Carl comes up with an inspirational new menu that his staff loves and invites Ramsey to a "rematch". After a confrontation with Riva, Carl walks out, quitting. At home, he prepares the menu he wanted to serve to Ramsey, goes to the restaurant, and angrily berates Ramsey.

Videos featuring Carls meltdown go viral video|viral, and his professional credibility evaporates. Molly and Inez encourage him to run a food truck. He accepts Inezs invitation to Miami, where he spends some quality time with Percy and rediscovers his love for Cuban cuisine. Inezs ex-husband Marvin offers him a dilapidated food truck, and Carl reluctantly accepts. He and Percy bond while restoring the truck and buying groceries and Carl buys him a chefs knife. Martin, his friend from Gauloise, turns down his restaurant promotion to work with Carl, who has become an exuberant and passionate chef again.

The three drive the food truck across the country back to Los Angeles, serving top-quality Cuban sandwiches and yuca fries. Percy finds ways to promote the food truck on social media websites, and the truck becomes successful in New Orleans and Austin, Texas, where the daily specials include items made with local ingredients such as po boys and barbecued brisket.

Back in Los Angeles, Carl realizes the importance of his relationship with his son and accepts Percys enthusiastic offer to help out on weekends and holidays. Ramsey visits the truck to explain that he wrote the bad review as he knew Carls creativity did not suit a restaurant which had been serving the same menu for years. He leaves with an offer to bankroll a new restaurant. In a flashforward set six months later, the new restaurant is a hit and closed for a private event: Carl and Inez remarry.

==Cast==
* Jon Favreau as Carl Casper
* Sofía Vergara as Inez
* John Leguizamo as Martin
* Scarlett Johansson as Molly
* Oliver Platt as Ramsey Michel
* Bobby Cannavale as Tony
* Dustin Hoffman as Riva
* Robert Downey, Jr. as Marvin
* Amy Sedaris as Jen
* Colombe Jacobsen as Lisa
* Russell Peters as Miami cop
* Emjay Anthony as Percy
* Jose Caridad Hernandez, "Perico" as Abuelito

==Production==
 
{{multiple image Fontainebleau Hotel (top) and the Versailles restaurant (bottom).
| direction = vertical
| image1    = Miami Beach FL Fontainebleau01.jpg
| alt1      = Fontainebleau Miami Beach Hotel
| width1    =  
| image2    = 2008-0426-FL-Versailles-INT.jpg
| alt2      = Versailles restaurant interior
| width2    =  
}}
On April 25, 2013, Variety (magazine)|Variety announced that Jon Favreau would be directing, writing, and starring in his next comedy project, titled Chef.    On June 12, 2013, it was announced that Open Road Films had acquired Chef  U.S. distribution rights while Aldamisa Entertainment would finance and produce the film. 

===Conception===
Jon Favreau, the writer, director and star of Chef, wrote the films script in about two weeks.    He had long wanted to make a film about food and chefs, and felt that the subject was suited to a small-scale independent film rather than a big-budget production.     He cited Jiro Dreams of Sushi, Eat Drink Man Woman and Big Night as inspirations for creating a food-centric film.  The script was semi-autobiographical, incorporating parts of Favreaus life into the main character, such as being a father while having a busy career and coming from a "broken home".  Favreau also drew a comparison between his career as a director and Carls career as a chef in the film, noting that he stepped down from directing major studio films to go "back to basics" and create Chef on a smaller budget, much like Carls resignation from a popular restaurant to work in a food truck.   

Favreau contacted Roy Choi, a restaurateur who created the Kogi Korean BBQ food truck, to serve as a consultant on the film; Choi was eventually promoted to co-producer. While the film was in pre-production, Favreau shadowed Choi in his restaurants and worked as part of Chois kitchen crew after training at a culinary school.  Choi oversaw all of the menus prepared for the film and created the Cuban sandwiches that formed a central part of the storyline. 

===Casting===
In addition to Favreau, the first actors cast in main roles were Sofía Vergara, John Leguizamo and Bobby Cannavale. It was announced that Robert Downey, Jr.—whom Favreau had previously directed in two Iron Man franchise|Iron Man films—had joined the cast in May 2013.    Scarlett Johansson and Dustin Hoffman were cast later that month.       Favreau felt that the films casting was one of its biggest successes, which provided him with "a tremendous amount of confidence"; in particular, he was impressed by Emjay Anthony, who was ten years old at the time of filming. 

===Filming=== Austin and Fontainebleau Hotel, and the Cuban restaurant Hoy Como Ayer in Little Havana.       In New Orleans, some scenes were filmed at Café du Monde in the citys French Quarter.  In Austin, filming locations included Franklin Barbecue and Gueros on South Congress.  All food prepared for the shoot was eaten by members of the cast and crew after filming had finished. 

==Soundtrack== New Orleans incidental music was scored by Lyle Workman. 

{{Track listing
| collapsed = yes
| headline = Track listing 
| extra_column = Artists I Like It Like That Pete Rodriguez
| length1 = 4:25
| title2 = Lucky Man
| extra2 = Courtney John
| length2 = 3:16
| title3 = A Message to You, Rudy Lone Ranger (musician)
| length3 = 5:50
| title4 = Cavern
| extra4 = Liquid Liquid
| length4 = 5:17
| title5 = C.R.E.A.M
| extra5 = El Michels Affair
| length5 = 2:54
| title6 = Hung Over
| extra6 = The Martinis
| length6 = 2:07
| title7 = Que Se Sepa
| extra7 = Roberto Roena
| length7 = 3:14
| title8 = Ali Baba
| extra8 = Louie Ramirez
| length8 = 4:16
| title9 = Homenaje al Benny (Castellano Que Bueno Balia Usted)
| extra9 = Gente de Zona
| length9 = 4:00
| title10 = Mi Swing Es Tropical Quantic & Nickodemus
| length10 = 3:56
| title11 = Bustin Loose
| extra11 = Rebirth Brass Band
| length11 = 3:55
| title12 = Sexual Healing
| extra12 = Hot 8 Brass Band
| length12 = 4:59
| title13 = When My Train Pulls In
| extra13 = Gary Clark Jr.
| length13 = 7:13
| title14 = West Coast Poplock Ronnie Hudson And The Street People
| length14 = 5:29
| title15 = Oye Como Va
| extra15 = Perico Hernandez
| length15 = 4:06
| title16 = La Quimbumba
| extra16 = Perico Hernandez
| length16 = 6:05
| title17 = One Second Every Day
| extra17 = Lyle Workman
| length17 = 2:22
}}
;Charts
{|class="wikitable plainrowheaders sortable"
|- Chart (2014) Peak position
|- Australian Albums (ARIA Charts|ARIA)  96
|-
 
|- US Billboard 200|Billboard 200    160
|- US Independent Albums (Billboard (magazine)|Billboard)  22
|- US Top Soundtracks (Billboard (magazine)|Billboard)  5
|}

==Release==
Chef premiered on March 7, 2014 at South by Southwest, where it was the opening film of the festival and was attended by Favreau, Leguizamo, Anthony, and Platt.  It was subsequently screened at the Tribeca Film Festival in April.  On August 19, Open Road Films announced to re-release the film nationally on August 29 for a Labor Day weekend, which would grow 100 screens to 600-800. 

===Box office===
The film was released theatrically on May 9, 2014, beginning in limited release in six theaters and expanding throughout May and June to a peak of 1,298 theaters.  Its total gross in the United States as of November 2, 2014 is $31.4 million.   

Outside of the U.S., Chef performed best in Australia (earning $2.8 million), the United Kingdom and Spain ($2.6 million in each country) and Mexico (earning a little over $1 million). In total, Chef has grossed almost $15 million outside America. 

===Critical response===
Reviews for Chef have been positive. On   gave the film a score of 68 out of 100, based on 36 critics, indicating "generally favorable reviews". 

Rolling Stone  Peter Travers gave the film 3.5 out of 4 stars, describing it as "an artful surprise and an exuberant gift" and "deliciously entertaining, comic, touching and often bitingly true".  Ty Burr of the Boston Globe also awarded the film 3.5 out of 4 stars; he thought it was "funny and heartfelt" and that, despite its weaknesses, the strengths "overpower the parts of the meal that are undercooked".  Chicago Sun-Times critic Richard Roeper gave Chef 3 out of 4 stars, finding it "funny, quirky and insightful, with a bounty of interesting supporting characters" but also noting the lack of plot and character development in some parts.  Gary Goldstein of the Los Angeles Times gave particular praise to the "terrific supporting cast" and the scripts lack of cliché, such as in its presentation of family dynamics.  Joe Leydon from Variety (magazine)|Variety found the films plot predictable and slow-paced, but noted "the trip itself is never less than pleasant, and often extremely funny".  The New York Times  Stephen Holden described Chef as "aggressively feel-good" and "shallow but enjoyable".  Michael OSullivan of The Washington Post gave the film 3.5 out of 4 stars and found it "deeply satisfying, down to the soul", praising the "incredible" food photography, the "colorful supporting cast" and the "wryly observant" humor, raving, "There’s nothing terribly profound about “Chef.” But its message — that relationships, like cooking, take a hands-on approach — is a sweet and sustaining one."  San Francisco Chronicle film critic Mick LaSalle opined that Chef was Favreaus best film to date, highlighting the "natural and convincing" chemistry between Favreau and Anthony and the "vivid" scenes featuring big-name actors in small roles.  USA Today  Scott Bowles gave Chef 3.5 out of 4 stars and called it "a nuanced side dish, a slow-cooked film thats one of the most heartwarming of the young year".  Ken Choy of   noted the structural problems but admitted, "If you ever saw the Kristen Bell sloth video on Ellen, that was me during the entire 2nd half of the movie. Non-stop tears. It was happy-crying because Favreau’s character was doing what he wanted." 

Slant Magazine critic Chris Cabin, however, gave Chef 1.5 out of 4 stars and described it as Favreaus "most self-satisfied, safe, and compromised film to date", chiefly criticizing the films lack of realism and credibility.  Writing for The Village Voice, Amy Nicholson agreed that the storyline was implausible and summarized the film as "so charmingly middlebrow that its exactly the cinematic comfort food it mocks".  Indiewires Eric Kohn opined that with Chef, "Favreau has no sweeping thematic aims", and that the end product was a "self-indulgent vanity project". 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 