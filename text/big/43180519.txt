In the Name of Life
{{Infobox film
| name = In the Name of Life
| image =
| image_size =
| caption =
| director = Iosif Kheifits   Aleksandr Zarkhi
| producer =
 | writer = Yevgeni Gabrilovich    Sergei Yermolinsky    Iosif Kheifits   Aleksandr Zarkhi  
| narrator = Mikhail Kuznetsov   Oleg Zhakov   Klavdiya Lepanova
| music = Venedikt Pushkov    
| cinematography = Vyacheslav Gordanov 
| editing = 
| studio =  Lenfilm Studio 
| distributor =  Sovexport
| released =  1946 
| runtime = 101 minutes
| country = Soviet Union Russian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Mikhail Kuznetsov and Oleg Zhakov. 

==Cast==
*    Viktor Khokhryakov as Doctor Vladimir Petrov  Mikhail Kuznetsov as Doctor Aleksandr Kolesov 
* Oleg Zhakov as Doctor Aleksei Rozhdestvensky  
* Klavdiya Lepanova as Lena  
* Lyudmila Shabalina as Vera   Mikhail Rostovtsev as Uchenyi
* Nikolai Cherkasov as Lukich, the attendant 
* Margarita Gromyko as Anushka 
* Aleksandr Zrazhevsky 
* Boris Kudryavtsev 
* Vladimir Dorofeyev

==References==
 

==Bibliography==
* Liehm, Mira & Liehm, Antonín J. The Most Important Art: Eastern European Film After 1945. University of California Press, 1977. 

==External links==
* 

 
 
 
 
 
 

 