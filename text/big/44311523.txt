Wyoming (1928 film)
{{Infobox film
| name           = Wyoming
| image          = 
| alt            = 
| caption        = 
| director       = W. S. Van Dyke
| producer       = 
| screenplay     = Ruth Cummings Madeleine Ruthven Ross B. Wills 
| story          = W. S. Van Dyke
| starring       = Tim McCoy Dorothy Sebastian Charles Bell William Fairbanks Chief John Big Tree
| music          = 
| cinematography = Clyde De Vinna
| editing        = William LeVanway 	
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western silent film directed by W. S. Van Dyke and written by Ruth Cummings, Madeleine Ruthven and Ross B. Wills. The film stars Tim McCoy, Dorothy Sebastian, Charles Bell, William Fairbanks and Chief John Big Tree. The film was released on March 24, 1928, by Metro-Goldwyn-Mayer. 

==Plot==
 

== Cast ==
*Tim McCoy as Lt. Jack Colton
*Dorothy Sebastian as Samantha Jerusha Farrell
*Charles Bell as Chief Big Cloud
*William Fairbanks as Buffalo Bill
*Chief John Big Tree as An Indian 
*Goes in the Lodge as Chief Chapulti
*Blue Washington as Mose
*Bert Henderson as Oswald

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 