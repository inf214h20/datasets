Max Wants a Divorce
 
{{Infobox film
| name           = Max Wants a Divorce
| image          =
| caption        =
| director       = Max Linder
| producer       =
| writer         = Max Linder
| starring       = See below
| cinematography =
| editing        =
| studio         = Essanay Studios
| distributor    = Essanay Studios
| released       =  
| runtime        = 2 reels
| country        = United States Silent English language intertitles
| budget         =
| gross          =
| website        =
}}

Max Wants a Divorce is a 1917 American short film directed by Max Linder.

== Plot summary ==
Max is forced to choose between losing his newly-wedded wife and a fortune. He hits upon a brilliant scheme: He will give his wife grounds for a divorce, secure the money, and then make his ex-wife Mrs. Linder again. He goes through any amount of trouble in helping her to get the necessary evidence, only to find that it is all a mistake on the part of a stupid lawyer - the money and the wife are both to be his. -- Edward Weitzel, Moving Picture World (April 7, 1917)

== Cast ==
*Max Linder as Max
*Martha Mansfield as Maxs Wife (billed as Martha Early)
*Helen Ferguson
*Francine Larrimore
*Ernest Maupain
*Leo White
*Mathilde Comont as the Loony Diva

==Premiere and preservation status== Strand Theatre in New York City on 26 March 1917. A restored print was presented by Serge Bromberg of Lobster Films at the San Francisco Silent Film Festival on 1 June 2014.

==See also==
*List of rediscovered films

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 