Hero of Tomorrow
 
 
 
{{Infobox film
| name           = Hero of Tomorrow
| image          = HeroofTomorrow.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 江湖接班人
| simplified     = 江湖接班人
| pinyin         = Jiāng Hú Jiē Bān Rén
| jyutping       = Gong1 Wu4 zip3 Baan1 Jan4 }}
| director       = Poon Man Kit
| producer       = Wallace Chung
| writer         = 
| screenplay     = Clarence Yip
| story          = 
| based on       = 
| narrator       = 
| starring       = Max Mok Michael Miu
| music          = Law Kit
| cinematography = Mark Lee
| editing        = Robert Choi Chow Tak Yeung
| studio         = Movie Impact Mei Ao Movies
| distributor    = Movie Impact
| released       =  
| runtime        = 86 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$5,278,572
}}
 1988 Cinema Hong Kong crime film directed by Poon Man Kit and starring Max Mok and Michael Miu. Mius nickname "Third Brother" (三哥) came from his character his this film as a former triad leader.

==Plot==
Three years ago, Lee Sam was a triad leader who was victimized by his enemy when leads him arrested and imprisoned. Upon his release, he single handedly takes revenge and flees to Taiwan afterwards and takes refugee from Billy Lee, whom he helped before. Sam decides to conceal his identity and leave the underworld but Billy is belligerent and repeatedly makes fuss and finds newcomer Crow to be his scapegoat. Crow has nothing to do all the time and always dreams of becoming a triad leader. Crow is then appreciated by Billy for being his scapegoat and his reputation rises which gets Billy jealous and arranges him to assassinate a police officer in Hong Kong. Billy plans a collateral kills Crows girlfriend and older sister. The anger of Sam and Crow forces them to be back in the underworld.

==Cast==
*Max Mok as Crow Yeung Tin Sun (烏鴉/楊天順), a young man who sells betel nut for a living in Taipei who is ambitious of becoming a triad leader, which caused the death of his girlfriend and older sister.
*Michael Miu as Lee Sam / Third Brother (李森/三哥), a former triad leader who seeks vengeance after release from prison and flees to Taiwan. He was betrayed by his own brothers and dishearteningly leaves the underworld.
*William Ho as Billy Lee Kui (李駒), a ruthless triad leader who would even betray his own brothers. He was later killed by Crow. 
*Joan Tong as Naive Ng (吳天真), Crows girlfriend after she was saved by him in a "Hero to the rescue" style. She was raped by Billy and committed suicide.
*Kam So Mui as Yeung Lai Ling (楊麗玲), Crows older sister who develops a relationship with Sam who was killed Billy while Crow was absent from Taiwan.
*Cheung Wing Ching as Little Santung (小山東), Crows good friend. He was corrupt with Billys money and Crow chopped his finger off to save him.
*Fong Ming Kit as Little Bread (小肉包), Littles Santungs girlfriend.
*Ku Feng as Uncle Chung (鍾叔), a triad elder who helps Lee Sam to flee to Taiwan (cameo)
*Lung Ming Yan as Lung (阿龍), a triad leader who collides with Billy to use Crow to kill a police officer in Hong Kong. He then tries to kill and silence Crw. (cameo)
*Philip Chan as Officer Chan (陳警官), a the police officer who Crow was lured to kill. (cameo)
*Blackie Ko as Peter (阿昆), a traid leader whose territories in Taipei were taken by Billy. While failing to negotiate with Billy, he was killed by Crow. (cameo) James Tien as Superintendent Cho (曹警司), a gambler in Billys casino. (cameo)
*Chen Song Yong as Big Ma (馬老大), a Taiwanese triad leader who was killed by Billy.
*Tommy Wong as Ng Kong (吳江), Billys sworn brother who framed Lee Sam to prison. He was killed by Sam after his release from prison. (cameo)
*Phillip Kwok as Big B (大B), a police officer who is Ng Kongs friend and helped him when he was attacked by Sam. (cameo)
*Thomas Sin as Bulldog (土狗), Billys henchman
*Chan King as Hong Kong triad leader
*Lam Chung as Hong Kong triad leader
*Shan Chin Po as Peters gangster
*Mak Wai Cheung as Monkey
*Wong Yiu as Dwarf
*Wong Hung as Hong Kong triad leader
*Liu Chun Hung as Uncle Chungs chauffeur
*Yiu Man Kei as Billys thug
*Chang Seng Kwong as bodyguard
*Lam Foo Wai as chauffeur
*San Sin

==Box office==
The film grossed HK$5,278,572 during its theatrical run from 23 September to 5 October 1988 in Hong Kong

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 