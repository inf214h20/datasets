My Best Friend, General Vasili, Son of Joseph Stalin
{{Infobox Film
| name = My Best Friend, General Vasili, the Son of Joseph Stalin
| original_name = Moy luchshiy drug, general Vasiliy, syn Iosifa 
| director = Viktor Sadovsky
| writer = Valentin Ezhov
| starring = Boris Schcherbakov  Vladimir Steklov  Andrei Boltnev Irina Malysheva Andrei Tolubeyev Petr Shelokhonov   Igor Yefimov   Georgi Shtil  Ernst Romanov  Valentina Kovel
| music = Vladlen Chistyakov
| cinematography = Vadim Grammatikov
| editing = G. Baranova
| studio = Lenfilm   Leninterfilm   Kraun
| distributor = -Soviet Union-  Lenfilm -non-Soviet Union Leninterfilm / Kraun 1991
| runtime = 102 min.
| country =   Soviet Union   Belgium English Russian Russian
}}
My Best Friend, General Vasili, the Son of Joseph Stalin ( ) is a 1991 film, directed by Viktor Sadovsky and starring Boris Schcherbakov and Vladimir Steklov.

==Plot summary== Soviet leader Joseph Stalin, and the famous Russian sports star Vsevolod Bobrov.

Vasili Stalin was Lieut. General of the Red Army in charge of the Army and Airforce sports teams. He befriended the talented athlete Bagrov (Bobrov) and made him a sports star in the Soviet Union. After each game played by his "toy-star" Bobrov, General Vasili Stalin would throw massive and wild drinking parties, with women dancing on their dining table among bottles of vodka. But after the death of his father, general Vasili Stalin was arrested by the new Soviet leadership, and was charged with "anti-Soviet" conspiracy, because of his opinions expressed in conversations with foreign diplomats.

==Cast==
===Main Characters===
*Boris Schcherbakov - as Vsevolod Bagrov (Vsevolod Bobrov)
*Vladimir Steklov - as Vasili Josifovich Stalin
*Andrei Boltnev
*Irina Malysheva - as Ninel
*Andrei Tolubeyev
*Petr Shelokhonov - as Colonel Savinykh
*Igor Gorbachyov - as Doctor
*Viktoriya Sadovskaya
*Valentina Kovel
*Igor Yefimov
*Georgi Shtil
===Cameos===
Yan Yanakiev, Ernst Romanov, Andrei Ponomaryov, I. Myachina, Anatoli Rudakov, Aleksandr Berda, Sergei Losev, Yevgeni Barkov, Yuri Dedovich, Yevgeni Dergachyov, Mikhail Devyatkin, Yefim Yoffe, Nikolai Makarov, Viktor Solovyov, A. Strepetov, Aleksei Vanin, Ye. Yerofeyev, O. Yudi, Yu. Zabludovsky

==Crew==
*Director: Viktor Sadovsky
*Writers: Valentin Ezhov, Natalya Gotovtseva, Pavel Kortobaj, Viktor Sadovsky
*Cinematographer: Vadim Grammatikov
*Composer: Vladlen Chistyakov
*Editor: G. Baranova
*Production designer: Vladimir Svetozarov

==Production==
*Production companies: Lenfilm, Leninterfilm, Kraun (Belgium)
*Production dates: 1990 - 1991
*Filming locations: St. Petersburg, Russia, Moscow, Russia.
*Additional production assistance was received from the Red Army and the Central Archives of the USSR.
*Original period military uniforms of the Red Army were used in the film production.
*Vintage Soviet cars of the 1940s and 1950s period were used in the film production.

==Release==
*Theatrical release in Russia was in 1991
*Theatrical release outside of Russia was in 1992
*Video release was in 1993

==Reception==
*Estimated theatrical viewership in the former Soviet Union was about 10 million.
*International theatrical viewership - no data.

==Facts of film production==
*The treatment for the film script was initially written by Valentin Ezhov in the 1980s, but he was waiting for the right time and circumstances together with director Viktor Sadovsky. The final script was written by the group of four authors.
*Filmmakers changed the name of the main character to Bagrov, in order to avoid direct mentioning of the reputable Russian star Vsevolod Bobrov, whose popularity was high among sport fans in Russia, as well as internationally.
*At the time of filming the former Soviet censorship was practically obsolete because of "perestroyka" and "glasnost" under Mikhail Gorbachev.
*Absence of the Soviet censorship allowed to portray Stalins son, Vasili Stalin, giving some artistic freedom to filmmakers, and also allowing Russian actresses to be involved in nudity and sex scenes, which were less than usual in the Soviet cinema before 1991.

== Facts of history ==
*Vasili Stalin was the second and youngest son of Joseph Stalin.
*In real life Vasili Iosifovich Stalin was imprisoned under a fictitious name "Vasili Vasilyev" in the Vladimir central prison. He was temporarily released under Nikita Khrushchev and returned to Moscow, but then was arrested again and exiled to the city of Kazan, where he died aged 42, in 1962.  
*Vsevolod Bobrov excelled in both football (soccer) and ice hockey, and led the Soviet ice hockey team to victory in the 1956 Winter Olympic Games in Cortina dAmpezzo, Italy, earning himself an Olympic Gold Medal.

== External links ==
*My Best Friend, General Vasili, the Son of Joseph Stalin (in Russian: Мой лучший друг - генерал Василий, сын Иосифа) на IMDb :  
*Мой лучший друг генерал Василий, сын Иосифа на Kinoexpert.ru:   (Russian)
*Мой лучший друг генерал Василий, сын Иосифа на Kinopoisk.ru:   (Russian)
*  

 
 
 
 
 
 