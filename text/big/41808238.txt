Bhagyada Lakshmi Baramma
{{Infobox film|
| name = Bhagyada Lakshmi Baramma
| image = 
| caption =
| director = Singeetham Srinivasa Rao Rajkumar  Madhavi   K. S. Ashwath 
| producer = Parvathamma Rajkumar
| music = Singeetham Srinivasa Rao
| cinematography = V. K. Kannan
| editing = P. J. Mohan
| studio = Dakshayini Combines
| released =  
| runtime = 156 minutes
| language = Kannada
| country = India
| budget =
}} Madhavi and K. S. Ashwath playing the pivotal roles.  The film, produced by Parvathamma Rajkumar under Dakshayini Combines, was received exceptionally well at the box-office and was one of the biggest hits of 1986. The dialogues and lyrics were written by Chi. Udaya Shankar.

== Cast == Rajkumar as Panduranga Madhavi as Parvati
* K. S. Ashwath  Balakrishna as Tarle Tammayya
* Thoogudeepa Srinivas
* Vijay Kashi
* Shivaram
* Mysore Lokesh

== Soundtrack ==
The music was composed by Singeetham Srinivasa Rao to the lyrics of Chi. Udaya Shankar. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Innu Hattira Hattira Rajkumar
| lyrics1 = Chi. Udaya Shankar
| length1 = 
| title2 = Yenu Mayavo Yenu Marmavo
| extra2 = Rajkumar (actor)|Rajkumar, Vani Jayaram
| lyrics2 = Chi. Udaya Shankar
| length2 = 
| title3 = Nee Atthare Entha Chenna
| extra3 = Rajkumar (actor)|Rajkumar, B. R. Chaya
| lyrics3 = Chi. Udaya Shankar
| length3 = 
| title4 = Bhagyada Lakshmi Baramma
| extra4 = S. P. Balasubrahmanyam
| lyrics4 = Chi. Udaya Shankar
| length4 = 
| title5 = Yaava Kaviyu Bareyalara Rajkumar
| lyrics5 = Chi. Udaya Shankar
| length5 = 
| title6 = Ananda Ananda
| extra6 = Rajkumar (actor)|Rajkumar, Vani Jayaram, Chi. Dattaraj
| lyrics6 = Chi. Udaya Shankar
| length6 =
}}

==Awards==
* Karnataka State Film Awards
Best Screenplay - Singeetham Srinivasa Rao and Chi. Udaya Shankar
*This film screened at 11th IFFI mainstream section.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 

 

 