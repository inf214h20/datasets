Invasion of Astro-Monster
{{Infobox film
| name           = Invasion of Astro-Monster
| image          = Invasion of Astro-Monster poster.jpg
| director       = Ishirō Honda
| producer       = Tomoyuki Tanaka|
| writer         = Shinichi Sekizawa Nick Adams Akira Takarada Kumi Mizuno Jun Tazaki Akira Kubo Yoshio Tsuchiya Haruo Nakajima
| music          = Akira Ifukube
| cinematography = Hajime Koizumi
| editing        = Ryohei Fujii UPA
| distributor    = Toho
| released       = December 19, 1965 (Japan) July 29, 1970 (USA)
| runtime        = 94 minutes (Japan) 92 minutes (USA)
| language       = Japanese
| country        = Japan
| budget         = 
}} Godzilla film Nick Adams and Japanese actors Akira Takarada, Kumi Mizuno and Akira Kubo. The film is the first in the franchise to feature alien invaders, combining the series with outer space themes such as civilizations on other planets and interplanetary space travel.

The film was released theatrically in the United States in the summer of 1970 by Maron Films as Monster Zero, where it played nationwide on a double bill with War of the Gargantuas.

==Plot==
In the 1960s, two astronauts, Fuji and Glenn, are sent to investigate the surface of the mysterious "Planet X". Upon landing, Fuji goes to look around while Glenn begins running some tests. Fuji spots footprints and hurries back to the landing sight, only to find that Glenn and their ship have disappeared. Suddenly, a glowing cylinder emerges from the ground and a voice, claiming to be the Controller of Planet X, orders him to enter. The cylinder takes him to an underground base where he is reunited with Glenn and meets the planets cyborg inhabitants, the Xiliens. The Controller assures the astronauts of his good will toward them and claims that he brought them under ground for their protection. Sure enough, seconds later the surface is attacked by a creature the Xiliens call "Monster Zero," but which the astronauts recognize as King Ghidorah, a planet-devouring monster that that had attacked Earth once before. The monster eventually leaves, but the Controller states that Ghidorah has attacked repeatedly, forcing them to live underground in constant fear of the next attack. He requests to borrow Godzilla and Rodan, known to the Xiliens as Monster Zero-One and Monster Zero-Two, to act as sentries against Ghidorahs attacks. He tells them the monsters locations and promises that in return, Planet X will give humanity the cure for cancer (the English dub says the formula can cure any disease). The astronauts then return home with the proposal to share it with Dr. Sakurai, the head of the space program, and the rest of the world. The people of Earth enthusiastically accept the offer.

Meanwhile, Fujis sisters boyfriend, Tetsuo, has invented a personal alarm that emits an ear-splitting electric siren. Miss Namikawa, a representative for an educational and toy-making company, makes an offer to buy the alarm, which he accepts enthusiastically. However, he grows concerned when Namikawa apparently leaves town before paying him. Later, Glenn, Fuji, his sister Haruno, and Tetsuo meet up at a cafe. Fuji thinks that Tetsuos invention is worthless and scolds him for getting suckered into a bad deal. Glenn leaves with a woman in a car, whom Tetsuo recognizes as Namikawa. Furious at having been lied to, he decides to follow her. He tracks her to a house on an island, where he falls into a trapdoor and finds himself in a prison cell guarded by Xiliens.

Glenn later meets up with Fuji again and claims that, while staying with Namikawa, he awoke during the night and saw Namikawa talking to the Controller. He is unsure whether he dreamed it or not but is concerned that it might have actually happened; he also recalls overhearing while on Planet X that their natural resources, particularly water, are nearly depleted. His suspicions seem to be confirmed when three Xilien spacecraft appear in Japan. The Controller speaks with Sakurai and apologizes for coming to Earth without permission. The Xiliens locate Godzilla and Rodan, both sleeping, and use their technology to transport them to Planet X, bringing Glenn, Fuji, and Sakurai with them. There, after a brief confrontation, the Earth monsters succeed in driving Ghidorah away. Glenn and Fuji sneak away during the battle and meet two Xilien women, both of whom look identical to Namikawa. Xilien guards confront the astronauts and bring them back to the Controller, who reprimands them for sneaking around and denies that Planet X is short on water. Glenn, Fuji, and Sakurai are given a tape containing instructions for the special drug, which they take back to Earth with them, leaving Godzilla and Rodan behind. They play the tape for the worlds leaders only to be surprised by a recorded ultimatum demanding that they surrender Earth to the Xiliens or be conquered by force. As it turns out, the aliens were using their technology to control Ghidorah all along and are now doing the same with Godzilla and Rodan; with the combined might of the three monsters, they plan to wipe out Earths defenses.

Riots break out; those willing to surrender to protect the peace clash with others who want to resist. Glenn storms into Namikawas office where he finds her in Xilien garb. She admits to being one of their spies but also claims to have fallen in love with him. She begs him to join her rather than die fighting, but he angrily refuses. Namikawas commander arrives to capture Glenn, but Namikawa clings to him, still proclaiming her love for him. The commander executes her on the spot for letting emotion cloud her judgment, but not before she slips a note into Glenns pocket. Glenn is taken to the same cell as Tetsuo, where they read Namikawas note which explains that Tetsuos invention disrupts the Xiliens electronics. Testsuo happens to have a prototype on him, which he activates, causing the guards to lose control of themselves and bumble around helplessly, allowing the prisoners to escape.

Meanwhile, Sakurai and Fuji learn that the Xiliens are controlling the monsters using magnetic waves and create a device to block the transmission. Glenn and Tetsuo arrive to share their knowledge of the Xiliens weakness. As the monsters attack, Sakurais device is activated and the sound from Tetsuos alarm is broadcast over the radio. The Xiliens lose control of the monsters and are stricken by mass panic as they begin to malfunction. The invasion force, unable to retreat and fearing what will happen if they are captured, destroy themselves en masse. The monsters awaken from their trance and a fight ensues. All three topple off a cliff; Ghidorah emerges and flies away, while those watching speculate that Godzilla and Rodan are probably still alive. Fuji acknowledges Tetsuos important role in the victory and no longer thinks poorly of him. Sakurai states that he wants to send Glenn and Fuji back to Planet X to study the planet thoroughly (the English dub says they are to be ambassadors). The two astronauts are reluctant but decide to make the best of it, happy at least that the Earth is safe.

==Cast==
  on the set of the film.]] Nick Adams as Astronaut Glenn
*Akira Takarada as Astronaut Fuji
*Kumi Mizuno as Namikawa
*Jun Tazaki as Dr. Sakurai
*Akira Kubo as Tetsuo Torii
*Keiko Sawai as Haruno Fuji
*Yoshio Tsuchiya as Controller of Planet X
*Takamaru Sasaki as Chairman of Earth Committee 
*Gen Shimizu as Minister of Defense 
*Yoshifumi Tajima as General 
*Nadao Kirino as Military Aide 
*Kenzo Tabu as Commander from Planet X, Earth Unit 
*Koji Uno as Namikawas Associate 
*Somesho Matsumoto as Buddhist Priest 
*Haruo Nakajima as Godzilla
*Masaki Shinohara as Rodan
*Shoichi Hirose as King Ghidorah

==Production==
During Godzilla and Rodans city rampage, footage from Rodan (1956)  is used, specifically the scenes where Rodan blows over a train and a pair of soldiers with the hurricane-force winds generated by the monsters wings.

==English version==
 
 UPA in 1970 under the title Monster Zero. It played on a double bill with War of the Gargantuas.

There were several alterations made:
* The opening theme was changed, and some of Akira Ifukubes score was re-arranged. Several sound effects were also added.
* Deleted: Several shots of Godzillas foot stepping on houses and huts. Some short shots of flying saucers. Rodan blowing away tanks from the top of a hill. Several scenes with the Xiliens speaking in the language of Planet X.
*The scene where Godzilla does his dance has added stomping noises in the English version.

The English version runs 93 minutes, seventy-one seconds shorter than the Japanese version. In his book Japans Favorite Mon-Star: An Unauthorized History of The Big G , Steve Ryfle says "The Americanization...is respectful to the original Japanese version."

Also, in the original Japanese version of the film, the drug the Xiliens promised was a cure for all forms of cancer. However, in the English version of the film, the cure was for all forms of disease (perhaps due to a translation mistake).
 Marvin Miller Jack Grimes provided the voice of Akira Kubo as Tetsuo Tori.  The remaining voice talent is unknown.
 international version, Invasion of the Astro-Monsters, which was released in the United Kingdom. Unlike later Toho international versions, Invasion of the Astro-Monsters is slightly edited from the Japanese version and the dubbing was not commissioned by Toho or produced in Tokyo or Hong Kong.

==Titles==
* The Great Monster War - Translated Japanese title.

* Invasion of Astro-Monster - Tohos official English title and current home video title.

* Monster Zero - Original US title.

* The Great Monster War - King Ghidorah vs. Godzilla - Japanese 1971 Champion Festival reissue title.

* Invasion of the Astro Monsters - UK home video title.

* Godzilla vs. Monster Zero - Previous home video and TV title.

==Box office==
In Japan, the film sold approximately 3,780,000 tickets. 

==DVD release==
Classic Media
* Release date: June 5, 2007
* Special features: Audio Commentary by Stuart Galbrath IV, Tomoyuki Tanaka biography, and 1971 reissue trailer. 
* Note: Contains both original Japanese and English versions of the film.
* Note: Part of the Toho Collection

==References==
 

==External links==
* 
* 
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 