Vikramaditya (film)
{{Infobox film
| name           = Vikramaditya 
| image          = Vikramaditya_1945.jpg
| image_size     = 
| caption        = 
| director       = Vijay Bhatt
| producer       = "Vijay-Shankar" Bhatt
| writer         = 
| narrator       = 
| starring       = Prithviraj Kapoor Prem Adib Baburao Pendharkar Ratnamala
| music          = Shankar Rao Vyas 
| cinematography = 
| editing        = 
| distributor    = 
| studio         = Prakash Pictures
| released       = 1945
| runtime        = 166 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1945 Hindi historical drama film directed by Vijay Bhatt for his banner Prakash Pictures.     The title role of the King of Avanti (India)|Avanti, Chandragupta II|Vikramaditya, was played by Prithviraj Kapoor, who had made a name for himself as a good actor by then and was extremely popular.    The film was made as a result of Bhatts interest in history and  on the request of the Vikram Bimillenium Committee, celebrating the 2000 years of the King Vikramaditya according to the Vikram Samvat, Hindu calendar. The film-maker concentrated on factual history rather than fiction for his story.       The music direction was by Shankar Rao Vyas with the lyricists being Ramesh Gupta and Roopdas.  The film starred Prithviraj Kapoor, Prem Adib, Baburao Pendharkar, Ratnamala, Bhagwandas and Ranjana. 
 Shakas (Scythians).

==Plot==
The Shakas (Scythians) have invaded and defeated the Kashmir region. The Kashmiri princess Shreelakha, is on the run to avoid captivity and unwanted attention of Shakraj. The Shakas have already managed to capture several areas in India and the princess is finding it difficult to find refuge. She meets Kalidas the poet, who tells her to ask for shelter from King Vikramaditya, the ruler of Avanti. Kalidas, through his poetry impresses Madhvi, the daughter of Acharya Varamihir and she makes it possible for them to meet Vikramaditya. The King appoints Kalidas as the court poet. The princess asks for refuge and war against the Shakas. When Vikramaditya promises to do so he is met with opposition from his Minister, Vaital. Vaital on being rebuffed by the King then incites the people against the princess and tries to poison her. After several incidents involving the honesty of the King and his travails, the people think that Vaital has poisoned the King and the princess. The magnanimity and valour of the King is shown when he fights the Shakas, killing Shakaraj and releasing Shreelekhas father from captivity and restoring his kingdom.

==Cast==
* Prithviraj Kapoor
* Ratnamala
* Baburao Pendharkar
* Prem Adib
* Ranjana
* Bhagwandas
* Jilloobai

==Soundtrack==
Music Director was Shankar Rao Vyas and the lyricists were Ramesh Gupta and Roopdas. The singers were Rajkumari, Amirbai Karnataki, G. M. Durrani, Manna Dey. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| Hai Gagan Mein
| Rajkumari, Manna Dey
| Ramesh Gupta
|-
| 2
| Om Jai Jai Shankar
| Amirbai Karnataki, G. M. Durrani, Manna Dey
| Ramesh Gupta
|-
| 3
| Ajab Hai Kudrat Ki Maya
| Manna Dey
| Ramesh Gupta
|-
| 4
| Ek Chakori Dev Pe Apne
| Manna Dey
| Ramesh Gupta
|-
| 5
| Shambho Mahadev Shankar
| G. M. Durrani
| Ramesh Gupta
|-
| 6
| Tej Phoonk Do Aaj
| Manna Dey
| Roopdas
|-
| 7
| Jai Jai Se Goonj Utha Bharat
| Amirbai Karnataki
| Ramesh Gupta
|-
| 8
| Om Jai Jai Shankar (Male)
| Manna Dey
| Ramesh Gupta
|-
| 9
| Neel Gagan Hai
| Manna Dey
| Roopdas
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 