Born to Raise Hell (film)
 
{{Infobox film
| name           = Born to Raise Hell
| image          = Born to Raise Hell Film Poster.jpg
| caption        = Film poster
| director       = Lauro Chartrand
| producer       = Phillip B. Goldfine Steven Seagal
| writer         = Steven Seagal
| starring       = Steven Seagal Dan Bădărău Darren Shahlavi
| music          = Michael Neilson
| cinematography = Eric J. Goldstein
| editing        = Trevor Mirosh
| studio         = Voltage Pictures
| distributor    = Paramount Home Entertainment 
| released       =   |2011|4|19|US|ref2=   }}
| runtime        = 98 minutes
| country        = United States
| language       = English Romanian
| budget         = $10 million  
| gross          = 
}}
Born to Raise Hell is a 2010 American action film directed by Lauro Chartrand, and also written and produced by Steven Seagal, who also starred in the film. The film co-stars Dan Bădărău and Darren Shahlavi. The film was released on direct-to-video|direct-to-DVD in the United States on April 19, 2011.

==Plot==
After 9/11, the United States government realized that narcotics were responsible for financing the majority of terrorist cells. Thats why they created the International Drug Task Force (IDTF). The IDTF task forces are fully funded and overseen by the United States government. They took their top narcotics officers and created interdiction teams throughout Asia and Eastern Europe.

Bobby Samuels (Steven Seagal) heads an IDTF team in Bucharest, Romania. Six months ago, Bobbys partner was killed. Now, headquarters has sent him a new partner named Steve (D. Neil Mark). Steves wife is eight months pregnant, and he’d really like to make it home to see the birth of his child.

Russian drug kingpin Dmitri (Dan Bădărău) has a wife (Silvia Stanciu) and a young son (Ștefan Iancu), both of whom he holds sacred, and they are not aware of his drug business. Recently, Dimitri has formed a drug dealing partnership with Costel (Darren Shahlavi), a Romanian man who runs the Roma Ace, one of the most popular clubs in Bucharest.

Costel and his men specialize in home invasions - they invade wealthy peoples homes, steal whatever loot they can get, and kill the family. During each invasion, Costel himself rapes and kills the wife. And Costel owes Dimitri some money. Dimitri has threatened that Costel will die if he doesnt come up with the money.

Bobby and Steve are put on Costels trail, and Bobby tries to get Dimitri to point them to Costel, but Dimitri is suspicious at first. After Bobby and Steve arrest Dimitri in an effort to get him to help them, Dimitri butts his head through a door window of their SUV, and accuses Bobby and Steve of brutality. Dimitri gets released, and Bobby and Steve continue their hunt for Costel.

One night while Dimitri is away from his mansion, Costel and his men break into the mansion, and Costels right-hand man Dada (Zoltán Butuc) fatally shoots Dimitris wife. Before Costel can kill Dimitris son, Dimitris men intervene, killing a couple of Costels men as Costel and Dada escape.

Dimitri arrives at the mansion, learns what happened, and vows to make Costel and Dada pay. Dimitris traumatized son describes his wifes shooter to him. Dimitri contacts Bobby, and offers to help Bobby find Costel, if Bobby will let Dimitri handle Dada himself. Bobby agrees, and he, Steve, and Dimitri set out to bring Costel and Dada down.

==Cast==
* Steven Seagal as Robert Bobby Samuels
* Dan Bădărău as Dimitri
* Darren Shahlavi as Costel
* D. Neil Mark as Steve
* George Remeș as Ronnie
* Claudiu Bleonț as Sorin
* Mădălina Marinescu as Tami
* Călin Puia as Christian
* Zoltán Butuc as Dada

==Release==
Born to Raise Hell was released on DVD in the United Kingdom on October 18, 2010,  and in the United States on April 19, 2011. 

==Reception==
Ian Jane of DVD Talk rated it 3/5 stars and wrote, "Born To Raise Hell doesnt bring anything new to Seagals filmography but its entertaining enough in its own ridiculous way."   Roy Hrab of DVD Verdict wrote, "Born To Raise Hell doesnt have much to offer anyone."   James Dennis of Twitch Film wrote, "Why does he still bother, when he clearly cant be bothered?" 

==References==
 

==External links==
* 

 
 
 
 
 
 
  
 
 
 