Les Parents terribles (film)
{{Infobox film
| name           = Les Parents terribles
| image          = Parentsterribles.jpg
| image_size     = 
| caption        = 
| director       = Jean Cocteau
| producer       = Francis Cosne,  Alexandre Mnouchkine
| writer         = Jean Cocteau
| narrator       = Jean Cocteau
| starring       = Jean Marais  Yvonne de Bray   Gabrielle Dorziat   Marcel André   Josette Day
| music          = Georges Auric
| cinematography = Michel Kelber
| editing        = Jacqueline Sadoul
| distributor    = Les Films Ariane (Paris)
| released       = 1 December 1948 (France);  1950 (USA)
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 1948 film adaptation directed by Jean Cocteau from his own stage play Les Parents terribles. Cocteau used the same cast who had appeared in a successful stage revival of the play in Paris in 1946. The film has sometimes been known by the English title The Storm Within.

==Background==
Cocteaus stage play Les Parents terribles was first produced in Paris in 1938, but its run suffered from a number of disruptions, first from censorship and then the outbreak of war. In 1946 it was revived in a production which brought together several of the actors for whom Cocteau had originally conceived their roles, notably Yvonne de Bray, Gabrielle Dorziat, and Jean Marais. Cocteau said that he wanted to film his play for three reasons. "First, to record the performances of incomparable actors; second, mingle with them myself and look them full in the face instead of seeing them at a distance on the stage. I wanted to put my eye to the keyhole and surprise them with a telescopic lens." 

==Plot==
In a rambling apartment a middle-aged couple, Yvonne and Georges, live with their 22-year old son Michel and Yvonnes spinster sister Léonie ("tante Léo"), who has also been in love with Georges.  Yvonne is a reclusive semi-invalid, dependent on her insulin treatment, and intensely possessive of her son (who returns her immoderate affection and calls her "Sophie"); Georges distractedly pursues his eccentric inventions; it is left to Léo to preserve such order as she can in their life and their apartment, which she describes as a "gypsy caravan" ("la roulotte"). When Michel announces that he is in love with a girl, Madeleine, whom he wishes to introduce to them, his parents are immediately hostile and seek to forbid the relationship, reducing Michel to despair. Georges realises that Madeleine is the same woman who has been his own mistress in recent months, and he confesses all to Léo, who devises a plan to extricate father and son by forcing Madeleine into silent surrender of them both.

The family visit Madeleine in her apartment where they are impressed by her modest and well-disciplined manner. Michels initial joy at this apparent reconciliation turns to despair as Madeleine is blackmailed into rejecting him by Georgess secret threats. Yvonne consoles her son with satisfaction as they return home. Léo however is appalled by the cruelty and selfishness of what has been done and decides to support Madeleine.

The next day Léo persuades Georges, and then the more reluctant Yvonne, that the only way to rescue the inconsolable Michel is to allow him to marry Madeleine. Michel and Madeleine are joyfully reunited, but Yvonne is unnoticed as she slips away and poisons herself. When the others realise what she has done, it is too late to save her. A new order is established in the "roulotte".

==Cast==
* Jean Marais : Michel
* Josette Day : Madeleine
* Yvonne de Bray : Yvonne ("Sophie")
* Marcel André : Georges
* Gabrielle Dorziat : Léonie ("Léo")
* Jean Cocteau (voice - uncredited) : Narrator

==Production==
Cocteau made the important decision that his film would be strictly faithful to the writing of the play and that he would not open it out from its prescribed settings (as he had done in his previous adaptation, The Eagle with Two Heads|LAigle à deux têtes).  He wrote no additional dialogue for the film, but substantially pruned the stage text, making the drama more concentrated. Gérard Lieber, "La Mise-en-scène des voix dans Les Parents terribles", in Le Cinéma de Jean Cocteau... actes du colloque...: textes réunis par Christian Rolot... (Montpellier: Centre d’études littéraires françaises du XXème siècle, Université Paul-Valéry, 1994.) pp.51-52.  He did however reinvent the staging of the play for the camera, employing frequent boldly framed close-ups of his actors, and he made full use of a mobile camera to roam through the rooms of the apartment, emphasising the claustrophobic atmosphere of the setting.  The translation from theatre to screen was a challenge which Cocteau relished: he wrote, "What is exciting about the cinema is that there is no syntax. You have to invent it as and when problems arise. What freedom for the artist and what results one can obtain!". 

Another significant contribution to the atmosphere of the film was the art direction by Christian Bérard which filled the spaces of the apartment with objects and décor - awkward heavy furniture, piles of trinkets and ornaments, pictures crooked on the walls, unmade beds, and dust - which described the way in which the characters lived. 

Cocteau refuted however the suggestion of some critics that this was a realist film, pointing out that he had never known any family like the one portrayed, and insisting that it was "painting of the most imaginative kind". 

Filming took place between 28 April and 3 July 1948 at the Studio Francœur. Cocteaus assistant director was Raymond Leboursier, who was joined by Claude Pinoteau (uncredited).

At the time of shooting the final shot (where one sees the apartment receding into the distance), some insecure tracks for the camera produced a shaky image on the film. Rather than reshoot the scene, Cocteau made a virtue of the problem by adding the sound of carriage wheels on the soundtrack together with some words (spoken by himself) to suggest a deliberate effect:  "And the caravan continued on its way. The gypsies do not stop."  

==Critical reception==
When the film was first shown in France in December 1948, the critical reception of it was overwhelmingly favourable and Cocteau was repeatedly congratulated on having produced an original piece of cinema out of a work of the theatre: for example, "It is what one may rightly call pure cinema...  The correspondence between image and text has never been so complete, so convincing".   

André Bazin wrote a detailed review of the film in which he took up the idea of "pure cinema" and tried to analyse how Cocteau had succeeded in creating it out of the most uncinematic material imaginable. Bazin highlights three features which assist this transition. Firstly the confidence and harmony of the actors, who have previously played their roles together many times on stage and are able to inhabit their characters as if by second nature, allow them to maintain an intensity of performance despite the fragmentation of the film-making process. Secondly, Cocteau shows unusual freedom in his choice of camera positions and movements, seldom resorting to the conventional means of filming dialogue with reverse angle shots, and introducing close-ups and long shots with a sureness of touch that never disrupts the movement of the scene; the spectator is always placed in the position of a witness to the action (as in the theatre), rather than a participant, and even that of a voyeur, given the intimacy of the cameras gaze. Thirdly, Bazin notes the psychological subtlety with which Cocteau chooses his camera positions to match the responses of his ideal spectator. He cites an example of the shot in which Michel tells Yvonne about the girl he loves, his face placed above hers and both facing the audience, just as they had done in the theatre; but in the film Cocteau uses a close-up which shows only the eyes of Yvonne below and the speaking mouth of Michel above, concentrating the image for the greatest emotional impact. In all of these aspects, the theatricality of the play is preserved but intensified through the medium of film. 

Cocteau himself came to regard Les Parents terribles as his best film, at least from a technical point of view.  This opinion has frequently been endorsed by later critics and historians of cinema.  

==References==
 

== See also == Les Parents terribles (1980 TV film)

== External links ==
*  

 

 
 
 
 
 
 
 