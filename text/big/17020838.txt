Three Weeks (film)
{{Infobox film
| name           = Three Weeks
| image          = Three Weeks - film poster.jpg
| image_size     = 
| caption        = Three Weeks poster.
| director       = Alan Crosland
| producer       = 
| writer         = Carey Wilson
| narrator       = 
| starring       = Aileen Pringle Conrad Nagel
| music          = 
| cinematography = John J. Mescall
| editing        = 
| distributor    = Goldwyn Pictures
| released       = February 10, 1924
| runtime        = 80 minutes
| country        = United States Silent English English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1924 drama novel of the same name by Elinor Glyn. Currently a lost film, FIAF database indicates a print is preserved by Russias Gosfilmofond.   

The novel had previously been made into a film in 1914 in film|1914, directed by Perry N. Vekroff and starring Madlaine Traverse and George C. Pearce. 

==Plot==
The Queen of Sardalia is in a bad marriage with the brutal King Constantine II. She decides to get away from her normal life for a period and goes on vacation to Switzerland. There, she meets Paul Verdayne. They have an affair, which lasts for three weeks. 

==Cast==
* Aileen Pringle - The Queen
* Conrad Nagel - Paul Verdayne
* John St. Polis - The King
* H. Reeves-Smith - Sir Charles Verdayne
* Stuart Holmes - Petrovich
* Mitchell Lewis - Vassili Robert Cain - Verchoff
* Nigel De Brulier - Dimitri
* Claire de Lorez - Mitze Dale Fuller - Anna
* Helen Dunbar - Lady Henrietta Verdayne
* Alan Crosland Jr. - Young King of Sardalia
* Joan Standing - Isabella
* William Haines - Curate
* George Tustain - Captain of the Guards

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 