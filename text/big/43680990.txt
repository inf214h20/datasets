Kartavya (1995 film)
{{Infobox film
| name           = Kartavya
| image          = 
| alt            =  
| caption        = 
| director       = Raj Kanwar
| producer       = 
| writer         = 
| starring       = Sanjay Kapoor  Juhi Chawla
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| synopsis:  Karan (Sanjay Kapoor) and Kajal (Juhi Chawla) are in love and ready to settle down and lead a happy life. But tragedy strikes as Karans mother has a heart attack and before she dies, Karan discovers that she wasnt his real mother. He is told that his actual mother is an heiress, Gayatri Singh (Aruna Irani) who lives in Sundernagar. Karan sets out for Sundernagar to confront her and on reaching there, comes to know the tragic story of his mothers life. Her stepbrother, Ugranarayan Singh (Amrish Puri) had killed her husband and had her thrown in a mental asylum so that he can inherit all the wealth. Karan must now punish the people responsible for his mothers morbid state. Will Ugranarayan Singh be brought to justice? Can Karan and Kajals love survive the fury of vengeance?
}}
Kartavya is a 1995 Indian action film directed by Raj Kanwar.

== Cast ==
* Sanjay Kapoor  - Karan Varma
* Juhi Chawla - Kajal Sahay
* Moushumi Chatterjee - Sharda Varma
* Aruna Irani - Gayetridevi Singh
* Amrish Puri - Thakur Ugranarayan Singh
* Om Puri - Ghulam Rasul
* Asha Sachdev - Roop Sundari
* Jagdeep - P.K. Dhoot
* Saeed Jaffrey - Ramakant Sahay
* Gulshan Grover - Inspector Tribhuvan

==External links==
* 

 
 
 


 
 