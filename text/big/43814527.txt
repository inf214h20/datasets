Hanuman Patal Vijay
{{Infobox film
| name           = Hanuman Patal Vijay
| image          = Hanuman Patal Vijay 1951.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Homi Wadia
| writer         = 
| screenplay     = Homi Wadia
| story          = 
| based on       = 
| narrator       =  Mahipal S. N. Tripathi
| music          = S. N. Tripathi
| cinematography = 
| editing        = 
| studio         = Basant Pictures
| distributor    = 
| released       = 1950
| runtime        = 139 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1951 Hindi Baiju Bawra (1951). {{cite book|last1=Mehta|first1=Vinod|title=    S. N. Tripathi besides acting in the film also composed the music. His costars were Meena Kumari, Mahipal (actor)|Mahipal, Niranjan Sharma,  Dalpat and Amarnath.  
 Ram and his battle with the two demon brothers Ahiravan and Mahiravan.

==Plot== Ravan to Ram and Lakshmana|Lakshman. The film follows Hanuman’s encounter with Makari the daughter of the sea who wants to marry him, but instead through the swallowing of a bead of his sweat she gives birth to Makardhwaj who guards the gates of Patal (Hell) where Ram and Lakshman are taken when kidnapped. Hanuman gets the better of Makardhwaj and rescues Ram and Lakshman. A major battle ensues and Ahiravan and Mahiravan are killed, but somehow they keep regenerating. Hanuman manages to find out the secret of their regeneration and puts a stop to it with the help of Ahiravan’s wife Chandrasena.

==Cast==
* Meena Kumari
* Mahipal
* S. N. Tripathi
* Shanta Kunwar
* Vimal
* Dalpat
* H. Prakash
* Kanta Kumar
* Niranjan Sharma
* Bimla
* Amarnath

==Music==
Songlist. 
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| Kyun Ruth Gaye Mujhse 
|-
| 2
| He Ambe Jagdambe Chandi 
|-
| 3
| Gori Ankhiyon Se Ankhiyan Milao 
|-
| 4
| Hey Shankar Pralaykar 
|-
| 5
| Kahe Bhatakta Phirta Man 
|-
| 6
| Main Kya Karun Hai Kya Karun 
|-
| 7
| Mann Mein Basakar Nainon Se 
|-
| 8
| Ram Shri Ram Raja Ram 
|}

 
==References==
 

==External links==
* 

 

 
 
 
 
 