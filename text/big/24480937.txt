Absolute 100
{{Infobox film
| name           = Absolute 100
| image          =
| caption        =
| director       = Srdan Golubović
| producer       = Miodrag Đorđević Srdan Golubović Ana Stanić
| writer         = Srdan Golubović Biljana Maksić Đorđe Milosavljević
| starring       = Vuk Kostić Srđan Todorović Bogdan Diklić Milorad Mandić Paulina Manov Saša Ali
| music          = Andrej Aćin
| cinematography = Aleksandar Ilić
| editing        = Stevan Marić
| distributor    = Tuck See release dates
| runtime        = 93 minutes
| language       = Serbian
| country        = FR Yugoslavia
| budget         =
| gross          = 	
}}
Absolute 100 ( n film written and directed by Srdan Golubović. It stars Vuk Kostić, Srđan Todorović and Bogdan Diklić.

== Plot summary == shooter who went footsteps of older brother Igor Gordić (Srđan Todorović), a 1991 Junior European shooting gold medallist. However, Igor has voluntarily gone to war in former Yugoslavia, from which he returned as a drug addict. In order to repay debt of local dealers, Igor is forced to sale their property sales, which their parents left them. So Saša decides to get revenge on everyone who destroyed life of his brother.

== Cast ==
{| class="sortable wikitable" style="font-size:97%;"
|-
|Actor
|Role
|-
| Vuk Kostić
| Saša Gordić
|-
| Srđan Todorović
| Igor Gordić
|-
| Bogdan Diklić
| Raša Knežević
|-
| Milorad Mandić
| Runda
|-
| Paulina Manov
| Sanja
|-
| Saša Ali
| Cvika
|- Dragan Petrović
| Neške
|}

== Release dates ==
{| class="sortable wikitable" 
|-
! Country
! Release date
! Notes
|-
| FR Yugoslavia
| 6 September 2001
|
|-
| Canada
| 12 September 2001
| Toronto Film Festival
|-
| Greece
| 17 November 2001
| Thessaloniki Film Festival
|-
| Netherlands
| 24 January 2002
| Rotterdam Film Festival
|-
| Poland
| 10 October 2002
| Warsaw Film Festival
|-
| Slovenia
| 12 December 2002
|
|-
| Czech Republic
| 2 April 2005
| Febio Film Festival
|-
| Hungary
| 2 October 2005
| Pécs International Film Celebration
|}

== Awards and nominations ==
{| class="sortable wikitable"
|-
! Award
! Category
! Nominated
! Result
! Notes
|-
| Art Film Festival
| Art Fiction: Best Cinematography
| Aleksandar Ilić
|  
|
|-
| rowspan="3"| Cottbus Film Festival
| Special Award of the Ecumenical Jury
| rowspan="3"| Srdan Golubović
|  
|
|-
| FIPRESCI Prize
|  
|
|-
| Feature Film Competition Special Prize
|  
| Tied with Kruh in mleko
|-
| rowspan="2"| Paris Film Festival
| Best Actor
| Vuk Kostić
|  
|
|-
| Grand Prix
| Srdan Golubović
|  
|
|-
| Selb Film Festival
| Hawk Cinematography Award
| Aleksandar Ilić
|  
|
|-
| rowspan="3"| Thessaloniki Film Festival
| Audience Award
| Srdan Golubović
|  
|
|-
| Best Actor
| Vuk Kostić
|  
| Tied with Alexandru Papadopol
|-
| Golden Alexander
| Srdan Golubović
|  
|
|}

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 