Revelation (2001 film)
{{Infobox Film
| name           = Revelation
| image_size     = 
| image	=	Revelation FilmPoster.jpeg
| caption        = 
| director       = Stuart Urban
| producer       = 
| writer         = Stuart Urban
| narrator       =  Liam Cunningham
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2001
| runtime        = 111 min.
| country        = U.K. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2001 film, directed by Stuart Urban and starring James DArcy, Natasha Wightman, Udo Kier and Terence Stamp.  Revelation tells the story of the final search for an ancient relic known as the Loculus, dating back to 50 AD, and the effect of this relic on the Martel family and the whole world.

==Plot== flay his father. Jake then flees with another researcher on the project (the occultist Mira) and seeks help from the Roman Catholic priest Father Ray Connolly. 
 make love within the symbol in an attempt to unlock it, experiencing a vision of themselves as Christ and Mary Magdalene before fleeing into the crypt when they hear a pursuer approaching. 

The pursuer turns out to in fact be Father Connolly and together they finally work out the Loculuss meaning - its significance in fact lies in the four nails holding it together, which are those used to  ) so as to oppose the Grand Masters puppet Christ.

==Locations==
Much of the film is shot on location, including on Malta.

==Cast==
 
 
*Natasha Wightman - Mira
*Udo Kier - The Grand Master
*Diran Meghreblian - Craftsman
*David Urban - Child with Loculus
*Uri Roodner - Jewish Alchemist
*Coryse Borg - Jewish Alchemists Daughter
*Manuel Cauchi - Mob Priest
*Anna Beck - Jewish Alchemists Wife
*Heathcote Williams - New Age Man
*Terence Stamp - Magnus, Lord Martel Ben Feitelson - Tour Guide at Rennes le Chateau
*James DArcy - Jake Martel
  Joe Johnson - Young Jake Martel
*Alan Talbot - Limo Driver
*Celia Imrie - Harriet Martel, Jakes mother
*Derek Jacobi - Librarian
*Mark Dymond - Backpacker in Malta
*Pip Torrens - Prof. Claxton
*Nicolas Chagrin - Pompous Academic
*Ron Moody - Sir Isaac Newton
*Miles E. Gregius - Knight Commander
*Oliver Ford Davies - Prof. Casaubon
*Charlotte Weston - Lord Martels PA
*Paul Birchard - Gen. Demolay Chris Rogers - Newsreader Robert Hudson - Director, Serious Fraud Office
 
*Mark Heenehan - Gen. Demolays Aide Liam Cunningham - Father Ray Connolly
*Jean-Marc Perret - Newtons Acolyte
*Vernon Dobtcheff - Curé at Rennes-le-Chateau
*Nicholas Murchie - Biotechnologist Earl Cameron - Cardinal Chisamba Joseph Long - Brigadiere
*Massimo Marinoni - Police Torturer
*Danica Bezanov - TV Reporter
*Andreas Markos - Abbot at Patmos Monastery
*Sidney Kean - Europolitician
*Lolly Susi - American Politician
*Vicky Paritoglou - Greek Orthodox Nun
 

==Awards==
Nominated for "Best Film" (2001) in the Sitges-Catalonian International Film Festival.

==Reception==
Overall reception was hostile. The Guardian, for example, noted the films "rag-bag of extraordinary cameos" but criticised the "vulgar heresies" on which it was based and its "clean-limbed young hero" and likened the "significant, ritual sex with a beautiful alchemist" to an Occult-based sketch in Not the Nine OClock News.  The Observer wrote of the film as "ludicrous", a "farrago", "straight-faced" and "the fag end of a millennial cycle of apocalyptic pictures". 

==References==
 

==External links==
*  
* 
*  at Rotten Tomatoes

 
 
 
 
 
 
 
 
 
 