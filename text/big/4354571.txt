Matchstick Men
 
{{Infobox film
| name           = Matchstick Men
| image          = Matchstick_Men.jpg
| caption        = Theatrical release poster
| director       = Ridley Scott Sean Bailey Jack Rapke Ted Griffin
| screenplay     = Ted Griffin Nicholas Griffin
| based on       =  
| starring       = Nicolas Cage Sam Rockwell Alison Lohman Bruce McGill
| music          = Hans Zimmer
| editing        = Dody Dorn John Mathieson ImageMovers Ridley Scott Free Productions Saturn Films
| distributor    = Warner Bros.
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| gross          = $65,573,198
}}
Matchstick Men is a 2003 American comedy drama film directed by Ridley Scott. Based on Eric Garcias 2002 novel of the same name, the film stars Nicolas Cage, Sam Rockwell, and Alison Lohman.

==Plot==
Roy Waller (Nicolas Cage) is a con artist residing in Los Angeles who has severe obsessive-compulsive disorder. Alongside his partner and protégé Frank Mercer (Sam Rockwell), Roy operates a fake lottery, selling overpriced water filtration systems to unsuspecting customers. After Roy experiences a violent panic attack, Frank suggests he see a psychiatrist, Dr. Harris Klein (Bruce Altman).

Klein provides Roy with medication, and in therapy has Roy recall his past relationship with his ex-wife, Heather ( ), an arrogant businessman whom the duo decides to con with the Pigeon drop.

One night, Angela unexpectedly arrives at Roys house, saying that she has had a fight with her mother, and decides to stay for the weekend before returning to school. She explores his belongings and causes him to rethink his life, which he mentions during therapy with Klein. Angela returns home late one night, leading to an argument between the two. During dinner, Roy admits that he is a con artist and reluctantly agrees to teach Angela a con. The two of them go to a local laundromat and con an older woman into believing she has won the lottery, and she shares half of her expected winnings with Angela; however, Roy then forces Angela to return the money.

Roy goes bowling with Angela but is interrupted when Frank reveals that Chucks flight to the Caymans has been updated to that day instead of Friday as planned. With little time left, Roy reluctantly decides to let Angela play the part of distracting Chuck midway through the con; however, after the con is finished, Chuck realizes what has happened and chases the two into the parking lot before they escape. Roy then learns that Angela was arrested a year ago, and asks that she stop calling him.

Without Angela, Roys myriad phobias resurface, and during another panic attack, he ultimately learns that the medication given to him by Klein is a placebo, proving that he doesnt actually need his pills to be happy. He decides that he needs Angela in his life but that he would have to change his lifestyle, much to Franks disappointment. Roy and Angela return from dinner one night to find Chuck waiting for them with a gun, alongside a badly beaten Frank. Angela shoots Chuck and Roy sends her off with Frank into hiding until the matter can be sorted out. As Roy prepares to take care of Chucks body, Chuck suddenly springs to life and knocks Roy unconscious.

Roy awakens in a hospital, where the police inform him that Chuck eventually died from the gunshot and Frank and Angela have disappeared. Klein appears and Roy gives him the password to his bank account, ordering him to give the money to Angela when she is found. Later, Roy awakens to find that the "police" have disappeared, his "hospital room" is actually a freight container on the roof of a parking garage, "Dr. Kleins" office is vacant, and essentially all of his money has been taken. As he begins to realize that Frank pulled a con on him, Roy drives over to Heathers (whom he hasnt seen for years) looking for Angela. While speaking with Heather, Roy learns the truth: Heather miscarried their child. There is no "Angela": the young girl he thought was his child was actually Franks accomplice.

One year later, Roy has become a salesman at a local carpet store, which Angela and her boyfriend one day wander into. Roy confronts Angela but ultimately forgives her, realizing that he is much happier as an honest man. Angela reveals that she did not receive her fair share of the cut from Frank, and that it was the only con she ever pulled. Angela and her boyfriend depart and Roy returns home to his new wife Kathy, who is now pregnant with his child.

==Cast==
* Nicolas Cage as Roy Waller
* Sam Rockwell as Frank Mercer
* Alison Lohman as Angela
* Bruce Altman as Dr. Harris Klein
* Bruce McGill as Chuck Frechette Sheila Kelley as Kathy
* Beth Grant as Laundry Lady
* Jenny OHara as Mrs. Schaffer
* Steve Eastin as Mr. Schaffer
* Melora Walters as Heather (uncredited)

==Soundtrack==
{{Infobox album Name = Matchstick Men: Original Motion Picture Soundtrack Type = soundtrack Artist = Hans Zimmer and various artists Longtype = Digital download / Audio CD) Cover = Matchstick Men (Original Motion Picture Soundtrack).jpg Released = September 30, 2003 Length = 55:28 Label = Varèse Sarabande
}} The Good Life" (performed by Bobby Darin) – 2:39
#"Flim Flam" – 0:12
#"Ichi-Ni-San" – 2:51
#"Matchstick Men" – 2:09
#"Weird Is Good" – 6:42
#"The Lonely Bull" (performed by Herb Alpert & the Tijuana Brass) – 2:15
#"Ticks & Twitches" – 2:48
#"I Have a Daughter?" – 1:06 Swedish Rhapsody" (performed by Mantovani & His Orchestra) – 2:37
#"Keep the Change" – 1:24
#"Nosy Parker" – 2:44
#"Leaning on a Lamp Post" (performed by George Formby) – 3:00
#"Pool Lights" – 0:54
#"Pygmies!" – 2:07
#"Charmaine (song)|Charmaine" (performed by Mantovani & His Orchestra) – 3:05
#"Roys Rules" – 2:04
#"Carpeteria" – 2:26
#"Shame on You" – 2:55
#"Tuna Fish and Cigarettes" – 1:55
#"No More Pills" – 4:39
#"Tijuana Taxi" (performed by Herb Alpert & the Tijuana Brass) – 2:05
#"The Bankers Waltz" – 3:07

==Reception==
The film opened to positive reviews from critics, who called it the finest con film since The Sting.
Matchstick Men holds an 83% critical approval rating on the review aggregation website Rotten Tomatoes. 
Roger Ebert rated the film four stars (out of four), and described it as "so absorbing that whenever it cuts away from the plot, there is another, better plot to cut to." He also recommended the film for several Oscar nominations, most notably Nicolas Cages performance and the films screenplay.  James Berardinelli awarded the film three-and-a-half stars (out of four), praising the film for its "sly, biting sense of humor" and "emotionally satisfying" elements. He also praised the films acting, and ultimately noted that the film was "worth every cent" of the ticket price and was "the first winner of the fall movie season." 

Some critics, however, were not impressed. Renee Graham of The Boston Globe criticized the film for its sentimentality, writing that "director Ridley Scott goes all gooey in this off-key adaptation of Eric Garcias cynical novel." Despite praising the performances of Sam Rockwell and Alison Lohman, Graham wasnt fond of Cage, writing that he is more "irritating than interesting" and that the film follows a similar style.  Similarly, Lou Lumenick of the New York Post praised the films acting but noted that the viewer "may end up feeling as suckered as Roys victims." Lumenick was also not fond of the twist ending, believing that it was a large detractor to the films value. 

Opening in 2,711 theaters in the United States and Canada, the films opening weekend gross stood at second place with $13.0 million for a mild per-theater-average of $4,827; despite receiving better reviews than its fellow openers, it ultimately lost the number-one position to Once Upon a Time in Mexico. The film eventually grossed $36.9 million domestically, which was an underwhelming total. The film was not much more successful in the global market, grossing a worldwide total of $65.5 million.  Additionally, the film was virtually ignored during the awards season, receiving no Oscar nominations despite positive reviews. However, the film found better success on home video and has since garnered a cult following.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 