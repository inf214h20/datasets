Baaz
 
{{Infobox film
| name           = Baaz
| image          = 
| director       = Guru Dutt
| producer       = Geeta Bali
| writer         = Guru Dutt Lalchand Bismil, Sarshar Sailani
| cinematography = V. K. Murthy
| editing        = Y.G. Chawhan
| music          = O. P. Nayyar Johnny Walker
| runtime        = 150 minutes
| country        = India
| language       = Hindi
}}
Baaz ( : باز) is a 1953 Hindi film directed by Guru Dutt. This film is Guru Dutts first starring film, an action film packed with adventure staged mainly on a ship.

==Plot==
The 16th century, the Malabar Coast. General Barbosa (KN Singh) signs a treaty with the queen (Sulochana) of a small state giving the Portuguese right to trade in exchange for military protection. With the help of the queens nephew Jaswant (Ramsingh), he begins to meddle in the administration as well. He arrests merchant Ramzan Ali and his friend Narayan Das. Das daughter Nisha (Geeta Bali) tries to save her father but is caught by Barbosa and both are sold to a cruel Portuguese pirate Cabral. Cabral kills Narayan Das. Nisha rouses her fellow slaves to revolt against Cabral and once Cabral is killed Nisha becomes a pirate queen pillaging all Portuguese ships in sight. One such ship includes heir to the throne Prince Ravi (Guru Dutt), a Portuguese woman Rosita (Kuldip Kaur) and a court astrologer (Johnny Walker). Nisha spares their lives as Ravi had saved her life earlier. They inevitably fall in love. Ravi joins the mutineers without revealing his identity. Back on shore, Ravi learns Jaswant is to be crowned king. Ravi is arrested and sentenced to death. Nisha saves him and they join forces with other local chiefs to defeat Barbosa.

==Cast==
* Geeta Bali       	 ...	Nisha N. Das
* Guru Dutt	         ...	Raj Kumar Ravi
* K. N. Singh   	 ...	General Barborosa
* Kuldip Kaur	 ...	Rosita
* Yashodra Katju	 ...	Nishas friend (as Yashodhara Katju)
* Ruby Myers	 ...	Raj Mata (as Sulochana Devi)
* Ram Singh	                (as Ramsingh) Johnny Walker  	 ...	The Court Astrologer (as Johny Walker)
* M. A. Latif		 (as M.A. Lateef)
* Jankidas       	 ...	Ramzan Ali Saudagar
* Moolchand 	 ...	Mothu (uncredited)
* Tun Tun    	 ...	A masseuse (uncredited)

== External links ==
*  

 
 
 
 
 
 

 