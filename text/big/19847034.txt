Last of the Renegades
 
{{Infobox film
| name           = Last of the Renegades
| image          = Winnetou 2. Teil .jpg
| caption        = Film poster
| director       = Harald Reinl
| producer       = Erwin Gitt Stipe Gurdulic Wolfgang Kühnlenz Horst Wendlandt
| writer         = Karl May Harald G. Petersson
| starring       = Lex Barker
| music          = 
| cinematography = Ernst W. Kalinke
| editing        = Hermann Haller
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = West Germany 
| language       = German
| budget         = 
}}
 Western film directed by Harald Reinl and starring Lex Barker.   

==Cast==
* Lex Barker as Old Shatterhand
* Pierre Brice as Winnetou Anthony Steel as Bud Forrester
* Karin Dor as Ribanna
* Klaus Kinski as David Luke Lucas
* Renato Baldini as Col. J.F. Merril
* Terence Hill as Lt. Robert Merril (as Mario Girotti)
* Marie-Noëlle Barre as Susan Merril (as Marie Noëlle)
* Ilija Ivezić as Red (as Elija Ivejic)
* Velemir Chytil as Carter
* Stole Arandjelovic as Caesar
* Djordje Nenadovic as Capt. Bruce (as George Heston)
* Mirko Boman as Gunstick Uncle
* Rikard Brzeska as Tah-Sha-Tunga
* Eddi Arent as Lord Castlepool

== See also ==
* Karl May movies
* Klaus Kinski filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 