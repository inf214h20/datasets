Jack Lemmon - A Twist of Lemmon
{{Multiple issues|
 
 
}}

{{Infobox film
| name = Jack Lemmon – A Twist of Lemmon
| director = Annett Wolf 
| writer = Annett Wolf
| cast = Jack Lemmon Walter Matthau Billy Wilder
| cinematography = Donald H. Stern
| editing = William H. White
| production companies = DR (broadcaster) Don Stern Productions 
| released = 1976
| runtime = 59 minutes
| country = Denmark / United States
| language = English
}}
 Jack Lemmon – A Twist of Lemmon is a 1976 Danish T.V. documentary directed by Annett Wolf, about the American comedian Jack Lemmon. The film won the 1976 Billedbladets Gyldne Rose. It was the first film directed by Annett Wolf in Hollywood.

==Synopsis==
In the setting of his office, Jack Lemmon shares some memories of his childhood and his relationship with his father that led him to develop a certain sense of humour and, ultimately, to become a professional entertainer. Lemmon expresses his personal mantra about film acting (“Simple is good”), and discusses some of his most important parts in Some Like it Hot and The Days of Wine and Roses. The film also contains original interviews with Walter Matthau and Billy Wilder, and film clips from The Odd Couple (1968), Kotch (1971), Lemmon’s first directorial effort, The Front Page (1974) and the 1976 TV adaptation of The Entertainer (play).

==Production== Robert Evans, David Brown. All these films were finally produced in cooperation with DR (broadcaster).
 Missing for Universal.

==Release==
The film first premiered in 1976.

==See also==
* Cinema of Denmark

==References==
*  

==External links==
*  
*  

 
 
 
 
 
 