Non pensarci
{{Infobox film
| name           = Non pensarci
| image          = Non pensarci.jpg
| caption        = Non pensarci theatrical poster
| director       = Gianni Zanasi
| producer       = Beppe Caschetto Rita Rognoni
| writer         = Gianni Zanasi Michele Pellegrini
| starring       = Valerio Mastandrea Anita Caprioli Giuseppe Battiston
| music          = Matt Messina
| cinematography = Giulio Pietromarchi
| editing        = Rita Rognoni
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Non pensarci is a 2007 Italian-language Comedy directed by Gianni Zanasi. The film follows Stefano Nardini a post-punk guitarist stuck in a strange career limbo.

==Cast==

*Valerio Mastandrea: Stefano Nardini
*Anita Caprioli: Michela Nardini
*Giuseppe Battiston: Alberto Nardini
*Caterina Murino: Nadine
*Paolo Briguglia: Paolo Guidi
*Dino Abbrescia: Carlo
*Teco Celio: Walter Nardini
*Gisella Burinato: Mamma Nardini 
*Paolo Sassanelli: Francesco 

==External links==
*  
*  
*  

 
 
 
 
 

 