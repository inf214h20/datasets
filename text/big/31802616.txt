Camp Hope (film)
{{Infobox film
| name           = Camp Hope
| image          = Camp Hope.jpg
| alt            = 
| caption        = 
| director       = George VanBuskirk
| producer       =  
| writer         = George VanBuskirk
| starring       = Bruce Davison Dana Delany Will Denton Jesse Eisenberg Andrew McCarthy Connor Paolo
| music          = Gary DeMichele
| cinematography = Michael McDonough
| editing        = David Leonard, Misako Shimzu
| production company         = Holedigger Films
| distributor    = Warner Home Video Lionsgate  New Films International
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $3 million 
}}
Camp Hope is a 2010 American thriller film starring Will Denton, Dana Delany, Andrew McCarthy, Bruce Davison and Jesse Eisenberg. The film is also known as Camp Hell. It was released August 13, 2010 on United States.

==Plot==
By the end of each summer, children from the fundamentalist Catholic community in the suburbs of New Jersey visited Camp Hope. Deep in the woods, away from civilization, the children are taught the ways of cult-like Christianity, the temptations of the flesh, and the horror of Satan. The head priest, who teaches the children these doctrines of fundamentalist Catholicism, has unknowingly brought evil with him by solely focusing on Sin instead of salvation. The priest fills these young minds with the belief that everything they do is an occasion of sin. He delves deep into their psyche asking them things like whether or not they masturbate, because masturbation, according to the Catholic Church, is the same as premarital sex and gives the devil a foothold in their hearts and minds. The priest also goes so far as to call a girl at the camp a whore simply because she was talking to a male member of the camp. These psychological attacks take their toll, and the presence of a demon starts to creep into the minds of the children, especially the main character, Tommy Leary. 

Tommys grandfather had recently died and the afterlife is very much on his mind. At the same time, his innocent relationship with his childhood crush slowly becomes more physical until the two escape into the woods for a sexual encounter. Melissa, his crush, is caught and sent home, allowing the priest to focus on Tommy. The priest takes Tommy down a psychological and spiritual path that makes the presence of the demon real to him, and it begins to haunt him. 

The movie ends with Tommy, after a confrontation with the priest and a near suicide attempt, denouncing his faith in the God he was being taught about and goes on a different path. The priest, after his emotionally brutal encounter with Tommy in his cabin, suffers a stroke. The members of the community struggle over whether to pull the plug, hoping for a miracle, while the priest spends the rest of his life in a persistent vegetative state, eyes wide open, with a look of horror on his face.

On the way home from the hospital, Tommy finds a note given to him earlier by Melissa telling him when he can to find her and be with her.  As the car drives on, he opens the window and throws out the copy of Dantes Inferno, which falls open on the road to a picture of a demon.

==Cast==
* Will Denton as Tommy Leary
* Dana Delany as Patricia Leary
* Andrew McCarthy as Michael Leary
* Bruce Davison as Fr. Phineas McAllister
* Valentina de Angelis as Melissa
* Connor Paolo as Jack
* James McCaffrey as Dr. John
* Sasha Neulinger as Jimmy
* Spencer Treat Clark as Timothy
* Chris Northrop as Death Metal Kid
* Drew Powell as Bob
* Jesse Eisenberg as Daniel
* Joseph Vincent Cordaro as Ryan
* Christopher Denham as Christian
* Caroline London as Rose Leary

== Release ==
The film was released in the United States August 13, 2010. Lionsgate release film on DVD in the United States in 2011. 

== Lawsuit==
Jesse Eisenberg has filed a lawsuit against Lionsgate Entertainment and Grindstone Entertainment for trying to fraudulently capitalize on his fame by using his name and face to promote a movie in which he was largely absent. Legal documents state:  "Eisenberg is bringing this lawsuit in order to warn his fans and public that, contrary to the manner in which the defendants are advertising the film, Eisenberg is not the star of and does not appear in a prominent role in Camp Hell, but instead has a cameo role in Camp Hell." 

Eisenberg asked for damages of at least US$3 million. 

==References==
 

==External links==
*  

 
 
 