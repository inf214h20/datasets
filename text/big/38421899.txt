Kohta 18
 
{{Infobox film
| name = Kohta 18
| image = Kohta_18.jpg
| alt=
| caption = Theatrical poster
| director = Maarit Lalli
| producer = Maarit Lalli
| writer = Maarit Lalli, Henrik Mäki-Tanila
| based on = 
| starring =  
| music = Kepa Lehtinen
| cinematography = Jan Nyman, Maarit Lalli, Harri Räty
| editing = Jenny Tervakari, Maarit Lalli
| studio = Huh huh -filmi Oy Oy Nordisk Film Ab
| released =  
| runtime = 110 minutes
| country = Finland
| language = Finnish
| budget = 
| gross = 
}}
Kohta 18 (Almost 18) is a 2012 Finnish film. It is the first feature film by director Maarit Lalli who also wrote the screenplay with her son Henrik Mäki-Tanila, one of the main actors in the film. 

==Plot==

Kohta 18 tells six stories of five teenage boys on the edge of adulthood. Karri, Pete, André, Akseli and Joni are dealing with same issues as every other young man; fears and hopes for future, disillusions and problems with parents. Henrik Mäki-Tanila has said that some parts of the stories are taken from his own experiences as a teenager, as well as from the lives of his friends. 

==Reception==

The film has received mixed to positive reviews. Some critics have not been impressed by the work of mostly amateur cast,  while others have complimented Lalli for creating a genuinely honest and authentic portrait of what it is to be a young adult in Finland in 2012. 

Harri Närhi of City noticed that the filmmakers are in love with their characters and are therefore making the viewer feel the same way.  Tarmo Poussu of Ilta-Sanomat was more reserved, writing that while the film is creating believable situations, it fails to expand them into stories. 

On 3 February 2012, Kohta 18 received three Jussi Awards; for Best Film, Best Director and Best Screenplay. 

== Main cast ==

*Karim Al-Rifai as André  
*Arttu Lähteenmäki as Akseli  
*Henrik Mäki-Tanila as Karri  
*Anton Thompson Coon as Pete  
*Ben Thompson Coon as Joni  
*Elina Knihtilä as Karris mother  
*Ilari Johansson as Akselis father  
*Niina Nurminen as Jonis mother  
*Mats Långbacka as Jonis stepfather 
*Mari Perankoski as Andrés mother  
*Hannu-Pekka Björkman as Petes father 
*Tarja Heinula as Petes mother

==References==
 

 
 
 