Hong Kong Confidential (2010 film)
{{Infobox film
| name           = Hong Kong Confidential
| image          = Amaya.jpg
| caption        = 
| director       = Maris Martinsons (director)|Māris Martinsons
| producer       = Linda Krūkle Māris Martinsons Chen On Chu
| writer         = Māris Martinsons
| screenplay     = 
| story          = 
| based on       =  
| starring       = Andrius Mamontovas Kaori Momoi Dexter Fletcher
| music          = Andrius Mamontovas
| cinematography = Gints Bērziņš, LGC
| editing        = Māris Martinsons
| studio         = Krukfilms October Pictures
| distributor    = House of Film
| released       =  
| runtime        = 95 minutes
| country        = Latvia Hong Kong
| language       = English Cantonese Japanese
| budget         = 
| gross          = 
}}
Hong Kong Confidential ( ) is a 2010 romantic comedy-drama film written and directed by Latvian director Maris Martinsons (director)|Māris Martinsons.

It was chosen as Latvias submission for the Academy Award for Best Foreign Language Film at the 83rd Academy Awards,     but it didnt make the final shortlist.   

==Plot==
Hong Kong: one week in one of the most exotic and picturesque cities of the world.  The movie follows six characters whose lives are all connected in one way or another.  The day of changes comes when Amaya (Kaori Momoi) meets a charming Englishman, Paul. Their encounter dramatically changes Amayas perception of her cultural and personal identity. Their lives change forever. But one thing remains universal, love.

==Cast==
*Kaori Momoi as Amaya
*Andrius Mamontovas as Paul
*Monie Tung as Jasmine
*Kristīne Nevarauska as Lori
*Lau Dan as Tao
*Hui Shiu Hung as Renshu
*Dexter Fletcher as Frenchman
*Laura Luīze Dzenīte as Kitty
*Margaret Cheung as Lang

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Latvian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*    
*  

 
 
 
 
 
 
 
 


 
 