The Curious Case of Benjamin Button (film)
{{Infobox film
| name           = The Curious Case of Benjamin Button
| image          = Curious case of benjamin button ver3.jpg
| caption        = Theatrical release poster
| director       = David Fincher Kathleen Kennedy Frank Marshall Ceán Chaffin
| screenplay     = Eric Roth
| story          = Eric Roth Robin Swicord
| based on       =  
| starring       = Brad Pitt Cate Blanchett Taraji P. Henson Julia Ormond Jason Flemyng Elias Koteas Tilda Swinton  
| music          = Alexandre Desplat
| cinematography = Claudio Miranda
| editing        = Kirk Baxter Angus Wall
| studio         = The Kennedy/Marshall Company Warner Bros. Pictures    
| released       =  
| runtime        = 166 minutes
| country        = United States
| language       = English
| budget         = $150 million      
| gross          = $333.9 million   
}}
 romantic fantasy short story love interest throughout his life.
 Academy Award Best Picture, Best Director Best Actor Best Supporting Best Art Best Makeup Best Visual Effects.

== Plot == November 11, 1918, a boy is born with the appearance and physical maladies of an elderly man. The babys mother died after giving birth, and the father, Thomas Button, abandons the infant on the porch of a nursing home. Queenie and Mr. "Tizzy" Weathers, workers at the nursing home, find the baby, and Queenie decides to care for him as her own.

Benjamin learns to walk in 1925; he declares it a miracle, after which he uses crutches in place of a wheelchair. On Thanksgiving 1930, Benjamin meets six-year-old Daisy, whose grandmother lives in the nursing home. He and Daisy become good friends.  Later, he accepts work on a tugboat captained by Mike Clark. Benjamin also meets Thomas Button, who does not reveal that he is Benjamins father. In Autumn 1936, Benjamin leaves New Orleans for a long-term work engagement with the tugboat crew; Daisy later is accepted into a dance company in New York City under choreographer George Balanchine.
 attacks Pearl Harbor, thrusting the United States into World War II. Mike volunteers the boat for the U.S. Navy; the crew is assigned to salvage duties. During a patrol, the tugboat finds a sunken U.S. transport and the bodies of many American troops. A German submarine surfaces; Mike steers the tugboat full speed towards it while a German gunner fires on the tugboat, killing most of the crew, including Mike. The tugboat rams the submarine, causing it to explode, sinking both vessels. Benjamin and another crewman are rescued by U.S. Navy ships the next day.

In May 1945, Benjamin returns to New Orleans and reunites with Queenie. A few weeks later, he reunites with Daisy; they go out for dinner. Upon failing to seduce him afterward, she departs. Benjamin later reunites with Thomas Button, who, terminally ill, reveals he is Benjamins father and wills Benjamin his button company and his estate.

In 1947, Benjamin visits Daisy in New York unannounced but departs upon seeing that she has fallen in love with someone else. In 1954, Daisys dancing career ends when her leg is crushed in an automobile accident in Paris. When Benjamin visits her, Daisy is amazed by his youthful appearance, but, frustrated by her injuries, she tells him to stay out of her life.

In spring 1962, Daisy returns to New Orleans and reunites with Benjamin. Now of comparable physical age, they fall in love and go sailing together. They return to learn that Queenie has died, then move in together. In 1967, Daisy, who has opened a ballet studio, tells Benjamin that she is pregnant; she gives birth to a girl, Caroline, in the spring of 1968. Believing he can not be a father to his daughter due to his reverse aging, Benjamin sells his belongings, leaves the proceeds to Daisy and Caroline, and departs the next spring; he travels alone during the 1970s.

Benjamin returns to Daisy in 1980. Now married, Daisy introduces him, as a family friend, to her husband and daughter. Daisy admits that he was right to leave; she could not have coped otherwise. She later visits Benjamin at his hotel, where they again share their passion for each other, then part once more.

In 1990, widowed Daisy is contacted by social workers who have found Benjamin—now physically a pre-teen. When she arrives, they explain that he was living in a condemned building and was taken to the hospital in poor physical condition, and that they found her name in his diary. The bewildered social workers also say he is displaying early signs of dementia. Daisy moves into the nursing home in 1997 and cares for Benjamin for the rest of his life. In the spring of 2003, Benjamin dies in Daisys arms, physically an infant but chronologically 84 years of age. Daisy dies as Hurricane Katrina approaches.

== Cast ==
 
* Brad Pitt as Benjamin Button (adult), Carolines biological father
** Robert Towers as Benjamin Button (apparent adult)
** Peter Donald Badalamenti II as Benjamin Button (apparent adult)
** Tom Everett as Benjamin Button (apparent adult)
** Spencer Daniels as Benjamin Button (apparent age 12)
** Chandler Canterbury as Benjamin Button (apparent age 8)
** Charles Henry Wyson as Benjamin Button (apparent age 5)
* Cate Blanchett as Daisy Fuller (adult)
** Elle Fanning as Daisy Fuller (age 6)
** Madisen Beaty as Daisy Fuller (age 11)
* Taraji P. Henson as Queenie
* Julia Ormond as Caroline Fuller (adult), Benjamin and Daisys daughter Shiloh Jolie-Pitt as Caroline Fuller (age 2)
* Jason Flemyng as Thomas Button, Benjamins father 
* Mahershalalhashbaz Ali as Tizzy Weathers
* Jared Harris as Captain Mike Clark
* Faune A. Chambers as Dorothy Baker
* Elias Koteas as Monsieur Gateau, a blind clockmaker in a story Daisy tells Caroline
* Ed Metzger as  Theodore Roosevelt
* Phyllis Somerville as Grandma Fuller
* Josh Stewart as Pleasant Curtis
* Tilda Swinton as Elizabeth Abbott
* Bianca Chiminello as Daisys friend
* Rampai Mohadi as Ngunda Oti
* Lance E. Nichols as Preacher
* Edith Ivey as "The woman who taught him to play piano"
 

== Production ==

=== Development === The Curious Jurassic Park Kathleen Kennedy Frank Marshall, Jim Taylor to adapt a screenplay from the short story. The studio also attached director Spike Jonze to helm the project.  Screenwriter Charlie Kaufman had also written a draft of the adapted screenplay at one point.  In June 2003, director Gary Ross entered final negotiations to helm the project based on a new draft penned by screenwriter Eric Roth.  In May 2004, director David Fincher entered negotiations to replace Ross in directing the film.   

=== Casting ===
In May 2005, actors Brad Pitt and Cate Blanchett entered negotiations to star in the film.  In September 2006, actors Tilda Swinton, Jason Flemyng and Taraji P. Henson entered negotiations to be cast into the film.    The following October, with production yet to begin, actress Julia Ormond was cast as Daisys daughter, to whom Blanchetts character tells the story of her love for Benjamin Button.  Brad Pitt had collaborated with many of his co-stars in previous films. He co-starred with Ormond in Legends of the Fall, with Flemyng in Snatch (film)|Snatch, with Harris in Oceans Twelve, with Blanchett in Babel (film)|Babel and with Swinton in Burn After Reading.

=== Filming ===
  of New Orleans, including this home at 2707 Coliseum St.]]
 ]]
For Benjamin Button, New Orleans, Louisiana and the surrounding area was chosen as the filming location for the story to take advantage of the states production incentives, and shooting was slated to begin in October 2006.  Filming of Benjamin Button began on November 6, 2006 in New Orleans. In January 2007, Blanchett joined the shoot.  Fincher praised the ease of accessibility to rural and urban sets in New Orleans and said that the recovery from Hurricane Katrina did not serve as an atypical hindrance to production.   

In March 2007, production moved to Los Angeles for two more months of filming. Principal photography was targeted to last a total of 150 days. Additional time was needed at visual effects house Digital Domain to make the visual effects for the metamorphosis of Brad Pitts character to the infant stage.  The director used a camera system called Contour (camera system)|Contour, developed by Steve Perlman, to capture facial deformation data from live-action performances. 
 
Several digital environments for the film were created by Matte World Digital, including multiple shots of the interior of the New Orleans train station, to show architectural alterations and deterioration throughout different eras. The train station was built as a 3D model and lighting and aging effects were added, using Next Limits Maxwell rendering software—an architectural visualization tool.  Overall production was finished in September 2007.   

=== Music ===
The score to The Curious Case of Benjamin Button was written by French composer Alexandre Desplat, who recorded his score with an 87-piece ensemble of the Hollywood Studio Symphony at the Sony Scoring Stage.   

== Reception ==
The Curious Case of Benjamin Button was originally slated for theatrical release in May 2008,  but it was pushed back to November 26, 2008.  The release date was moved again to December 25 in the United States, January 16, 2009 in Mexico, February 6 in the United Kingdom, February 13 in Italy  and February 27 in South Africa.

=== Box office performance === Marley & Bedtime Stories with $26,853,816 in 2,988 theaters with an $8,987 average. The film has come to gross $127.5 million domestically and $206.4 million in foreign markets, with a total gross of $333.9 million. 

=== Critical response ===
The film has received positive reviews, with Pitts performance receiving praise. The review aggregator Rotten Tomatoes reports that 72% of critics gave the film positive reviews based on 235 reviews.  According to Metacritic, the film received an average score of 70 out of 100, based on 37 reviews.  Yahoo! Movies reported the film received a B+ average score from critical consensus, based on 12 reviews. 
 pure cinema."    

  than to  ,   states: "Theres no denying the sheer ambition and technical prowess of The Curious Case of Benjamin Button. Whats less clear is whether it entirely earns its own inflated sense of self-importance" and further says, "It plays too safe when it should be letting its freak flag fly."  Kimberley Jones of the Austin Chronicle panned the film and states, "Finchers selling us cheekboned movie stars frolicking in bedsheets and calling it a great love. I didnt buy it for a second." 

Roger Ebert of the Chicago Sun-Times gave the film two and a half stars out of four, saying that it is "a splendidly made film based on a profoundly mistaken premise." He goes on to elaborate that "the movies premise devalues any relationship, makes futile any friendship or romance, and spits, not into the face of destiny, but backward into the maw of time."  Peter Bradshaw in The Guardian called it "166 minutes of twee tedium", giving it one star out of a possible five. 

Cosmo Landesman of the   Hollywood film that offers a safe and sanitised view of life and death. Its Forrest Gump goes backwards," while awarding the film two out of five stars.  James Christopher in The Times called it "a tedious marathon of smoke and mirrors. In terms of the basic requirements of three-reel drama the film lacks substance, credibility, a decent script and characters you might actually care for"  while Derek Malcolm of Londons Evening Standard notes that "never at any point do you feel that theres anything more to it than a very strange story traversed by a film-maker who knows what he is doing but not always why he is doing it." 

== Home media ==
The film was released on DVD on May 5, 2009 by Paramount Pictures|Paramount, and on Blu-ray and 2-Disc DVD by The Criterion Collection. The Criterion release includes over three hours of special features, and a documentary about the making of the film. 

As of November 1, 2009 the DVD has sold 2,515,722 DVD copies and has generated $41,196,515 in sales revenue.   

== Accolades ==
 
{| class="wikitable"
|-
! Award
! Category
! Recipient
! Result
|- 81st Academy Awards    Academy Award Best Picture Kathleen Kennedy Kathleen Kennedy Frank Marshall Ceán Chaffin
| 
|- Academy Award Best Director David Fincher
| 
|- Academy Award Best Actor in a Leading Role Brad Pitt
| 
|- Academy Award Best Actress in a Supporting Role Taraji P. Henson
| 
|- Academy Award Best Adapted Screenplay Eric Roth
| 
|- Academy Award Best Film Editing Kirk Baxter Angus Wall
| 
|- Academy Award Best Cinematography Claudio Miranda
| 
|- Academy Award Best Art Direction Donald Graham Burt Victor J. Zolfo
| 
|- Academy Award Best Costume Design Jacqueline West
| 
|- Academy Award Best Makeup Greg Cannom
| 
|- Academy Award Best Original Score Alexandre Desplat
| 
|- Academy Award Best Sound Mixing David Parker David Parker Michael Semanick Ren Klyce Mark Weingarten
| 
|- Academy Award Best Visual Effects Eric Barba Steve Preeg Burt Dalton Craig Barron
| 
|- American Society of Cinematographers  American Society Outstanding Achievement in Cinematography in Theatrical Releases Claudio Miranda
| 
|- Austin Film Austin Film Critics Association  Best Supporting Actress Taraji P. Henson
| 
|- 62nd British British Academy Film Awards Best Film Kathleen Kennedy Frank Marshall Ceán Chaffin
| 
|- Best Makeup & Hair
!
| 
|- Best Director David Fincher
| 
|- Best Adapted Screenplay Eric Roth
| 
|- Best Leading Actor Brad Pitt
| 
|- Best Costume Design
!
| 
|- Best Music Alexandre Desplat
| 
|- Best Cinematography Claudio Miranda
| 
|- Best Editing
!
| 
|- Best Production Design
!
| 
|- Best Visual Effects
!
| 
|- 14th Critics Broadcast Film Critics    Broadcast Film Best Film
!
| 
|- Broadcast Film Best Actor Brad Pitt
| 
|- Broadcast Film Best Actress Cate Blanchett
| 
|- Broadcast Film Best Director David Fincher
| 
|- Broadcast Film Best Supporting Actress Taraji P. Henson
| 
|- Broadcast Film Best Cast
!
| 
|- Broadcast Film Best Screenplay Eric Roth
| 
|- Broadcast Film Best Composer Alexandre Desplat
| 
|- Central Ohio Film Critics Association Awards Best Score Alexandre Desplat
| 
|- Top 10 Films of the Year
! 9th place
|- Chicago Film Critics Association Chicago Film Best Picture
!
| 
|- Chicago Film Best Director David Fincher
| 
|- Chicago Film Best Screenplay, Adapted Eric Roth
| 
|- Best Cinematography Claudio Miranda
| 
|- Chicago Film Best Original Score Alexandre Desplat
| 
|- Directors Guild Directors Guild of America Awards Directors Guild Outstanding Directorial Achievement in Motion Pictures David Fincher
| 
|- 66th Golden Golden Globe Awards Golden Globe Best Motion Picture Drama
!
| 
|- Golden Globe Best Actor - Motion Picture Drama Brad Pitt
| 
|- Golden Globe Best Director - Motion Picture David Fincher
| 
|- Golden Globe Best Screenplay Eric Roth
| 
|- Golden Globe Best Original Score Alexandre Desplat
| 
|- Houston Film Houston Film Critics Society Awards Best Picture
!
| 
|- Best Director David Fincher
| 
|- Best Actor Brad Pitt
| 
|- Best Actress Cate Blanchett
| 
|- Best Supporting Actress Taraji P. Henson
| 
|- Best Screenplay Eric Roth
| 
|- Best Cinematography Claudio Miranda
| 
|- Best Score Alexandre Desplat
| 
|- Las Vegas Film Critics Society Awards Best Art Direction
!
| 
|- Best Cinematography Claudio Miranda
| 
|- Best Costume Design Jacqueline West
| 
|- London Film London Film Critics Circle London Film Best Film
!
| 
|- London Film Director of the Year David Fincher
| 
|- British Supporting Actress of the Year Tilda Swinton
| 
|- Screenwriter of the Year Eric Roth
| 
|- MTV Movie MTV Movie Awards MTV Movie Best Female Performance Taraji P. Henson
| 
|- National Board National Board of Review    
| 
!
|  
|- National Board Best Director David Fincher
| 
|- National Board Best Adapted Screenplay Eric Roth
| 
|- Satellite Awards Satellite Awards Satellite Award Best Adapted Screenplay Eric Roth Robin Swicord
| 
|- Satellite Award Best Art Direction and Production Design Donald Graham Burt Tom Reta
| 
|- Satellite Award Best Cinematography Claudio Miranda
| 
|- Satellite Award Best Costume Design Jacqueline West
| 
|- Saturn Award Saturn Award Best Fantasy Film
!
| 
|- Saturn Award Best Actor Brad Pitt
| 
|- Saturn Award Best Actress Cate Blatchett
| 
|- Saturn Award Best Supporting Actress Tilda Swinton
| 
|- Saturn Award Best Director David Fincher
| 
|- Saturn Award Best Writing Eric Roth
| 
|- Saturn Award Best Music Alexandre Desplat
| 
|- Saturn Award Best Make-Up
!
| 
|- Saturn Award Best Visual Effects
!
| 
|- 2009 Scream Scream Awards Best Fantasy Actor Brad Pitt
| 
|- 15th Screen Screen Actors Guild Awards Screen Actors Outstanding Performance by a Male Actor in a Leading Role Brad Pitt
| 
|- Screen Actors Outstanding Performance by a Female Actor in a Supporting Role Taraji P. Henson
| 
|- Screen Actors Outstanding Performance by a Cast in a Motion Picture
!
| 
|-
|St. Louis Gateway Film Critics Association Awards 2008|St. Louis Gateway Film Critics Association Awards  Best Film
!
| 
|- Vancouver Film Vancouver Film Critics Circle Awards  Vancouver Film Best Director David Fincher
| 
|- Washington D.C. Washington D.C. Area Film Critics Association Washington D.C. Best Art Direction
!
| 
|- Writers Guild Writers Guild of America Awards Writers Guild of America Award for Best Adapted Screenplay Eric Roth Robin Swicord
| 
|}

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 