The Knife of the Party
{{Infobox Film |
  | name           = The Knife of the Party |
  | image          =  |
  | caption        =  |
  | director       = Leigh Jason | Art Jarrett, Sr. H.O. Kusell| Jack Good Shemp Howard James Fox Charles Senna The Girl Friends Gertrude Mudge Leo Kennedy Rogers & Anthony|
  | cinematography = Joseph Ruttenberg | 
  | editing        =  |
  | producer       = Monroe Shaff Meyer Davis|
  | distributor    = RKO Pictures |
  | released       =   |
  | runtime        = 20 mins. |
  | country        = United States
  | language       = English
}}
 
The Knife of the Party is a black-and-white short film starring Shemp Howard. The comedy was filmed at Van Beuren Studios and released by RKO Radio Pictures on February 16, 1934.. 

==Shemp Howard and his Stooges== Three Stooges until he left the act to be replaced by his brother Curly Howard, then returned after Curly retired in the wake of a series of strokes.

This feature appears on the Three Stooges DVD The Three Stooges: Greatest Hits and Rarities. 

== Cast ==
* Lilian Miles - Donna Jack Good - Walter Brown
* Shemp Howard and his Stooges
** James Fox - Stooge
** Charles Senna - Stooge
* The Girl Friends - Singing Trio
* Gertrude Mudge - Mrs. Dora
* Leo Kennedy - bit role
* Rogers & Anthony - bit roles

== External links ==
*  
*  at  

 
 
 
 
 
 
 