A Star Is Born (1937 film)
{{Infobox film
| name           = A Star Is Born
| image          = A Star Is Born 1937 poster.jpg
| image size     = 225px
| caption        = theatrical poster
| director       = William A. Wellman
| producer       = David O. Selznick
| writer         =  
| starring       =  
| music          = Max Steiner
| cinematography = W. Howard Greene
| editing        =  
| studio         = Selznick International Pictures
| distributor    = United Artists
| released       =   
| runtime        = 111 minutes
| language       = English
| country        = United States
| budget         = $1,159,000 David Thomson, Showman: The Life of David O. Selznick, Abacus, 1993 p 245 
| gross = over $2 million 
}}
 Robert Carson, Alan Campbell. Hollywood actress, and Fredric March as an aging movie star who helps launch her career. Other members of the cast include Adolphe Menjou, May Robson, Andy Devine, Lionel Stander and Carole Landis.

== Plot ==
  in A Star Is Born (1937) ]]

North Dakota farmgirl Esther Victoria Blodgett (Janet Gaynor) yearns to become a Hollywood actress.  Although her aunt and father discourage such thoughts, Esthers grandmother (May Robson) gives her her savings to follow her dream.

Esther goes to Hollywood and tries to land a job as an extra, but so many others have had the same idea that the casting agency has stopped accepting applications. Esther is told that her chances of becoming a star are one in 100,000. She befriends a new resident at her boarding house, assistant director Danny McGuire (Andy Devine), himself out of work. When Danny and Esther go to a concert to take their minds off their troubles, Esther has her first encounter with Norman Maine (Fredric March), an actor she admires greatly. Norman has been a major star for years, but his alcoholism has sent his career into a downward spiral.

Danny gets Esther a one-time waitressing job at a fancy Hollywood party. While  serving hors d’œuvre, she catches Normans eye. He gets his longtime producer and good friend, Oliver Niles (Adolphe Menjou), to give her a screen test. Impressed, Oliver gives her a new name ("Vicki Lester") and a contract. She practices her few lines for her first tiny role.

However, when the studio has trouble finding a female lead for Normans current film, entitled The Enchanted Hour, Norman persuades Oliver to cast Esther.  The film makes her an overnight success, even as viewers continue to lose interest in Norman.

Norman proposes to Vicki; she accepts when he promises to give up drinking. They elope without publicity, much to press agent Matt Libbys (Lionel Stander) disgust, and enjoy a trailer-camping honeymoon in the mountains. When they return, Vickis popularity continues to skyrocket, while Norman realizes his own career is over, despite Olivers attempts to help him. Norman stays sober for a while, but his frustration over his situation finally pushes him over the edge. He starts drinking again. When Vicki wins the industrys top award, he interrupts her acceptance speech by drunkenly demanding three awards for the worst acting of the year.

A stay at a sanatorium seems to cure Normans increasingly disruptive alcoholism, but a chance encounter with Libby gives the press agent an opportunity to vent his long-concealed contempt and dislike for Norman. Norman resumes drinking. Esther decides to give up her career in order to devote herself to his rehabilitation. After Norman overhears her discussing her plan with Oliver, he drowns himself in the Pacific Ocean.

Shattered, Vicki decides to quit and go home. Soon afterward, her grandmother shows up once she hears Vicki is quitting. Her grandmother tells her of a letter Norman sent her when they got married. The letter stated how proud he was of Vicki, and how much he loved her. Because of her grandmothers words, and the reminder of Normans deep love; Vicki is convinced to stay in show business. At the premiere of her next film at Graumans Chinese Theatre, Vicki is asked to say a few words into the microphone to her many fans listening across the world; she announces, "Hello everybody. This is Mrs. Norman Maine."

  in A Star Is Born (1937)]]

==Cast==
  
*Janet Gaynor as Esther Blodgett/Vicki Lester  
*Fredric March as Norman Maine
*Adolphe Menjou as Oliver Niles
*May Robson as Grandmother Lettie
*Andy Devine as Daniel "Danny" McGuire
*Lionel Stander as Matt Libby
*Owen Moore as Casey Burke
 
*Peggy Wood as Miss Phillips Elizabeth Jenns as Anita Regis
*Edgar Kennedy as Pop Randall
*Clara Blandick as Aunt Mattie
*J. C. Nugent as Mr. Blodgett Guinn "Big Boy" Williams as posture coach
 

==Production== Los Angeles, 1954 remake.

It is not known how much Dorothy Parker contributed to the finished script. When she first saw the film, Parker was proud of her contribution and boasted about both the script and the film, but in later life she believed that she had contributed nothing of significance. 
 script reader musical remake starring Judy Garland. 

==Background== Frank Fay John Bowers has also been identified as inspiration for the Norman Maine character and the dramatic suicide-by-drowning scene near the end of the film (Bowers drowned in November 1936). The film contains several inside jokes, including Gaynors brief imitations of Greta Garbo, Katharine Hepburn, and  Mae West; the "Crawford Smear", referring to Joan Crawfords lipstick; and the revelation that the glamorous Norman Maines real last name is Hinkle. (Hinkle was the real last name of silent film star Agnes Ayres, and not far removed from Fredric Marchs real last name, Bickel.)
 Tom Forman, who committed suicide following a nervous breakdown.   
 1954 musical remake starring Judy Garland.  

A common Hollywood myth about the film is that Lana Turner appeared as an extra in one of the scenes in the film. Turner often denied the myth over the years, mentioning that she was discovered several months after the picture had finished production.

==Soundtrack==
*"A Star is Born", sung by Buddy Clark with the orchestra of Eddy Duchin. Lyrics by Dorothy Dick to the music of Max Steiner from the film soundtrack.

==Reception==
By the end of 1939 the film had earned a profit of $181,000. 
 Academy Awards==
Wins    Robert Carson
*  for the color photography of A Star Is Born. This Award was recommended by a committee of leading cinematographers after viewing all the color pictures made during the year.

Nominations color film to be nominated for best picture. Best Directing: William Wellman Best Actor: Fredric March Best Actress: Janet Gaynor
*  Alan Campbell, Robert Carson

==Adaptations to other media==
At the time of the release of the film, a 15-minute transcription &ndash; a pre-recorded radio show issued on 16-inch disc &ndash; promoting the films release was made. The narrated promotional radio show included sound clips from the film. The show was recorded and released through the World Broadcasting System,  with disc matrix number H-1636-2.
 Robert Montgomery and Janet Gaynor, the November 17, 1940 episode of The Screen Guild Theater starring Loretta Young and Burgess Meredith, the December 28, 1942 episode of Lux Radio Theater with Judy Garland and Walter Pidgeon, the June 29, 1946 episode of Academy Award Theater, starring Fredric March, the May 23, 1948 episode of the Ford Theatre and the June 16, 1950 episode of Screen Directors Playhouse starring Fredric March.

==Remakes== remade twice, 1954 with 1976 with Barbra Streisand and Kris Kristofferson.  Warner Bros. has plans to finance another remake, with Clint Eastwood possibly directing the film, while singer-actress Beyoncé Knowles is "in negotiations" with the studio to play the female lead. On October 11, 2012, it was announced that Knowles had withdrawn from consideration.  In March 2015, it was announced that Bradley Cooper entered negotiations to direct and co-produce the remake.   

 

==Ownership and copyright status==
  movie remake. public domain (in the USA) due to Warners failure to renew its copyright registration in the 28th year after publication.    The original 35mm master elements remain with Warner Bros.

==Home media==
The film was released on Blu-ray in the U.S. by Kino Lorber Inc. on February 2012, featuring an authorized edition from the estate of David O. Selznick from the collection of George Eastman House. 

==References==
Notes
 

==External links==
 
*  
*  
*  
*  
*  
Streaming audio
*  on Lux Radio Theater: September 13, 1937 
*  on Screen Guild Theater: November 17, 1940 
*  on Lux Radio Theater: December 28, 1942  Academy Award Theater: June 29, 1946
*  on Ford Theater: May 23, 1948

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 