The Substitute 4: Failure Is Not an Option
 

{{Infobox Film
| name           = The Substitute 4: Failure Is Not An Option
| image          = Substitute 4.jpg
| director       = Robert Radler
| producer       = Thomas J. Busch
| writer         = Roy Frumkes   Rocco Simonelli
| starring       = Treat Williams Angie Everhart Patrick Kilpatrick 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Artisan Entertainment
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
}}

The Substitute 4: Failure Is Not An Option is a 2001 action film. Starring Treat Williams once again as Karl Thomasson who is now an undercover policeman that must infiltrate a military schools faculty to cease the actions of a white supremacist cult. This was the last entry in the popular "The Substitute" series and was released direct-to-video.

==Plot==
Karl Thomasson (Williams), an ex-Special Forces soldier and retired mercenary, is now working as a police detective. One day, he is approached by his old army buddy Teague who gives him a mission: working undercover at a military school where Ted, Teagues nephew, is one of the cadets. Teague believes that the cadets and the student faculty are part of a white supremacist cult being run at the school. Karl accepts the mission and begins working as a history teacher at the school, seeking to expose and eradicate the cult.

While investigating, Karl teams up with Devlin, a former member of Karls Mercenary team who works at the school as a martial arts teacher. They learn that Colonel Brack is leader of the cult and Ted is one of the cult members.

==Cast==
*Treat Williams as Karl Thomasson,a former U.S. Special Forces soldiers and mercenary, now a cop.
*Angie Everhart as Jenny, a beautiful doctor at the academy and Karls love interest.
*Patrick Kilpatrick as Col J.C. Brack, the military academys Commanding officer and founder of the Werewolf Cult.
*Bill Nunn as Luther, an ex-soldier who works as a janitor at the military academy.
*Tim Abell as Devlin, an ex-soldier who works as a martial arts teacher at the academy.
*Grayson Fricke as Ted Teague, a cadet and Werewolf cult member who later regrets joining it.
*Simon Rhee as Lim, a Korean soldier who is a Drill Instructor of the Werewolf cult.
*Scott Miles as Buckner, an arrogant, racist cadet and the cadet commander of the academy and of the Werewolf cult.
*Brian Beegle as Frey, Teds best friend and Werewolf cult member.
*Samantha Thomas as Harmon, a bossy, loud-mouthed female cadet.
*Jonathan Michael Weatherly as Malik, a cadet.
*Lori Beth Edgeman as Cunningham, a female cadet who is picked on at by Harmon.
*Moe Michaels as Robson, a cadet.
*J. Don Ferguson as Colonel Teague, Karls old war buddy and Teds uncle.
*K.C. Powe as Sissy, a waitress at a bar and Bracks daughter.

== External links ==
*  

 
 
 
 
 


 