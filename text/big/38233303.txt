Devki
 

{{Infobox film
| name           = Devki - Marathi Movie
| image          = Devki Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Milind Ukey
| producer       = Mayur Shah Naushir Misstry
| screenplay     =
| story          =
| starring       = Alka Athalye Sudhir Joshi Dr. Girish Ukey Shilpa Tulaskar Milind Gawali
| music          = Lalit Sen Sanjeev Kohli
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Marathi
| budget         =
| gross          =
}}
Devki is a Marathi movie released on 10 December 2001. The movie has been directed by Milind Ukey  and produced by Mayur Shah as well as Naushir Misstry. The plot of the movie is based on an endearing and touching melodrama about relationships and a womans sublime sacrifice.

== Synopsis ==
Vasudha and Sujata are sister-in-laws. Since Sujata can’t bear a child, Vasudha asks her to adopt her son, Rohan. But their callous neighbour Bharti, plants insecurity in Sujata’s mind, resulting in chaos and turmoil within the family, the same family who once lived in harmony, where an atmosphere of gaiety and joy reigned supreme. Both the families decide to part ways and amidst all this Rohan learns that Sujata has adopted him and he decides to leave the house. 

== Cast ==
The cast includes Alka Athalye, Sudhir Joshi, Dr Girish Oak, Shilpa Tulaskar, Milind Gawali  & others.

==Soundtrack==
The music has been directed by Lalit Sen and Sanjeev Kohli

== References ==
 
 

==External links==
*  
*  
*  

 
 
 


 