Ditto (1937 film)
 
{{Infobox film
| name           = Ditto
| image          = 
| caption        = 
| director       = Charles Lamont
| producer       = E. H. Allen E. W. Hammons
| writer         = Paul Girard Smith
| starring       = Buster Keaton
| music          = 
| cinematography = Dwight Warren
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes
| country        = United States 
| language       = English
| budget         = 
}} short comedy film featuring Buster Keaton.

==Plot==
Buster Keaton plays a delivery man who falls in love with one of his customers.  Little does he know, though, that her twin lives right next door.

==Cast==
* Buster Keaton as The Forgotten Man
* Gloria Brewster as Housewife
* Barbara Brewster as Housewifes twin sister Harold Goodwin as Hank
* Lynton Brent as Bill
* Al Thompson
* Robert Ellsworth

==See also==
* List of American films of 1937
* Buster Keaton filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 