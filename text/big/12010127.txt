Stone of Destiny (film)
{{Infobox film
| name           = Stone of Destiny
| image          = Stone of destiny.jpg
| caption        = Theatrical release poster
| director       = Charles Martin Smith
| producer       = Andrew Boswell  Rob Merilees
| writer         = Charles Martin Smith Billy Boyd  Robert Carlyle  Kate Mara  Bryan Lowe  Brenda Fricker
| music          =
| cinematography = Glen Winter
| editing        = Fredrik Thorsen
| distributor    = Alliance Films
| released       =  
| runtime        = 96 minutes
| country        = Scotland   Canada
| language       = English
| budget         = £6 million
| gross          =
}}
 comedy film Billy Boyd, Scone in Perthshire, was stolen by King Edward I of England in 1296 and placed under the throne at Westminster Abbey in London. In 1950, a group of student Scottish nationalists succeeded in removing it from Westminster Abbey and returning it to Scotland where it was placed symbolically at Arbroath Abbey, the site of the signing of the Declaration of Arbroath and an important site in the Scottish nationalist cause.

Filming began in June 2007 in various locations throughout Scotland, Wales and England.    The filmmakers were given rare access to shoot scenes inside Westminster Abbey.    The film was premiered at the Edinburgh International Film Festival in Fountainbridge, Edinburgh, Scotland on June 21, 2008.    The film closed the  33rd Annual Toronto International Film Festival on September 13, 2008;       and was presented at The Hamptons International Film Festival in the United States.    The film was released in the United Kingdom on October 10, 2008, and in Canada on February 20, 2009.   

==Plot== Ian Hamilton Scottish nationalist Billy Boyd), he creates a daring scheme to bring the Stone of Scone back to Scotland from Westminster Abbey in London, where it has resided for centuries following English military victories over the Scots in the Middle Ages.

Hamilton and Craig research the floor plans and security setup of Westminster Abbey and plan the theft, but once Craig realises the legal implications of liberating the stone and the potential impact to his personal life and career, he backs out. Undaunted, Hamilton decides to liberate the stone by himself. He turns to John MacCormick (Robert Carlyle), a prominent campaigner for Scottish devolution, and asks for financial help with the project. Although he initially refuses to take seriously Hamiltons proposal and request for a mere £50, MacCormick reconsiders and provides his support. Later at a party, MacCormick refers him to Kay Matheson (Kate Mara), a young woman with strong nationalist ideas, to help him retrieve the stone.

After meeting Matheson, Hamilton is soon introduced to Gavin Vernon (Stephen McCole), a strong young man known mainly for his drinking ability. On the day of their departure for London, Vernon unexpectedly brings his quiet friend Alan Stuart (Ciaron Kelly) along with him. At first Hamilton opposes bringing in a fourth member, but Vernon convinces him that Stuart and his car will be valuable assets to the group. They agree to steal the stone on Christmas Eve while all of London is distracted by the holiday celebration.

The four nationalist students arrive in London the day before Christmas Eve and decide to steal the stone that very night. They drive to Westminster Abbey, but their plans are interrupted when Hamilton is discovered by a watchman, who mistakes him for a homeless man and lets him go. Soon after, Matheson falls ill from a fever and Hamilton brings her to a bed & breakfast inn to recover. The landlady is suspicious of their Scottish accents and phones the police, who likewise suspect the young Scots of being up to something, but they avoid being arrested.
 Romani people who are camped in the field.

After the two parts of the stone are reattached, the students offer to return it to the authorities at the symbolically significant Arbroath Abbey, the site of the signing of the Declaration of Arbroath. The police arrive and arrest the student nationalists, who are never prosecuted.  |group=N}} The Stone of Scone was returned to London, where it remained until 1996, when it was moved to Edinburgh Castle "on loan" with the understanding that it would be brought back to Westminster Abbey for the next Coronation of the British monarch|Coronation.

==Cast==
 
  Ian Hamilton
* Kate Mara as Kay Matheson
* Robert Carlyle as John MacCormick Billy Boyd as Bill Craig
* Stephen McCole as Gavin Vernon
* Ciaron Kelly as Alan Stuart
* Peter Mullan as Ians Father
* Brenda Fricker as Mrs. McQuarry
* Juliet Cadzow as Ians mother
* Alex Donald as London Policeman
* John Comerford as Pub Landlord
* Richard Addison as Tour Guide
* Danielle Stewart as Brochure Girl
 
* Suzanne Dance as Church Woman
* Pauline King as Jen
* Bryan Lowe as Student In Pub
* Margaret Newlands as B&B Owner
* Ron Donachie as Night Watchman
* Rab Affleck as Gypsy
* Stewart Porter as Veteran In Pub
* Bernard Horsfall as Archdeacon
* Stephen Clyde as Abbey Constable
* Johnny Meres as Detective
* Stewart Cairns as Reporter
* Ian Hamilton as English Businessman   
 

==Production==
;Filming locations
* Arbroath, Angus, Scotland
* Bridgend, Wales, UK 
* Glasgow University, Glasgow, Strathclyde, Scotland
* Glasgow, Strathclyde, Scotland
* Glenfinnan Viaduct, Fort William, Highland, Scotland
* London, England, UK 
* Oakshaw Street East, Paisley, Renfrewshire, Scotland
* Paisley Abbey, Paisley, Renfrewshire, Scotland
* Vancouver, British Columbia, Canada
* Westminster Abbey, London, England, UK    

==Reception==
;Critical response
Stone of Destiny received mixed reviews. From Canadian Film: "A heartwarming triumph for the human spirit. For the non-English, a powerful tale of courage, pride, and the innocence of youth." From  : "A wee-dram-and-bagpipes invitation to a mythical Scotland of yesteryear." From Channel 4: "A woeful slice of sentimental whimsy that makes Braveheart look like a documentary."

;Box office
The £6m movie took in just £140,000 in the three weeks subsequent to its release in the UK.

==References==
;Notes
 

;Citations
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 