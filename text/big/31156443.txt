Darsanam
{{Infobox film 
| name           = Darsanam
| image          =
| caption        =
| director       = P. N. Menon (director)|P. N. Menon
| producer       = PC George
| writer         = P. N. Menon (director)|P. N. Menon
| screenplay     = P. N. Menon (director)|P. N. Menon Raghavan
| music          = G. Devarajan Ashok Kumar
| editing        = Ravi
| studio         = Mother India Productions
| distributor    = Mother India Productions
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, Raghavan in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Adoor Bhasi
*Jayalakshmi
*Sankaradi Raghavan
*Sasidharan Pillai
*Balan K Nair
*CA Vasantha
*EP Balakrishnan
*Junior Geetha
*Junior Raji
*Kottarakkara Sreedharan Nair
*Kuthiravattam Pappu
*Muthu
*Oduvil Unnikrishnan
*PK Udayakumar
*Pattambi Subhadra
*Pattambi Thankam
*Pecheri Xavier
*Radhadevi
*Ravi Shankar
*Rexona
*SK Palissery
*Santha Devi
*Saraswathi
*Sathyapal
*Roja Ramani
*Sree Narayana Pillai
*Surasu
*Susheela
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Poonthanam and Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Innaleyolavum || P. Madhuri, Ambili || Poonthanam || 
|-
| 2 || Peraattin Karayilekkoru || K. J. Yesudas, Chorus || Vayalar Ramavarma || 
|-
| 3 || Thiruvanchiyooro || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Veluppo Kadum Chuvappo || P. Madhuri || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 