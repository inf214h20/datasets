Gadis Desa
 
{{Infobox film
| name      = Gadis Desa
| image     = 
| image size   = 
| border     = 
| alt      = 
| caption    = 
| director    =Andjar Asmara
| producer    =
| writer     = Andjar Asmara
| starring    = {{plain list|
*Basuki Djaelani
*Ratna Ruthinah
*Ali Joego
*Djauhari Effendi
}}
| music     = 
| cinematography = AA Denninghoff-Stelling
| editing    = 
| studio     = South Pacific Film Corp
| distributor  = 
| released    =  
| runtime    = 
| country    = {{plain list|
*Indonesia
*Dutch East Indies
}}
| language    = Indonesian
| budget     = 
| gross     = 
}} comedy from what is now Indonesia  written and directed by Andjar Asmara. Starring Basuki Djaelani, Ratna Ruthinah, Ali Joego, and Djauhari Effendi, it follows the romantic hijinks of a village girl who is taken to be a rich mans second wife. The film, produced by a Dutch-run company, is recognised as the first in which future "father of Indonesian film"  Usmar Ismail was involved.

==Plot== second wife. Rusli (Basuki Zaelani), Aisahs cousin and a manservant at Abu Bakars home, discovers this plan and tells Abu Bakars wife. Aisah is sent back to her village and Rusli, who has fallen in love with her, proposes. 

==Production== Dardanella in native men had limited creative input, serving more as acting and dialogue coaches. The Dutch cameraman, AA Denninghoff-Stelling, held more power over the final product.  
 Japanese occupation beginning seven years earlier.   

==Release and reception==
Gadis Desa was released in 1949,  followed by a novelisation in 1950.  Although he does not record its box-office performance, the Indonesian film historian Misbach Yusa Biran suggests that the films dated plot was reflective of a Dutch belief that native audiences would prefer unsophisticated comedy.  A 35&nbsp;mm copy is kept at Sinematek Indonesia. 

The film proved Andjars last as a director; he resigned from SPFC before its next production, Tjitra (1949), and spent the rest of his film career as a screenwriter.   Ismail would go on to become two films for SFPC, Tjitra and Harta Karun (1949), and, after the Netherlands recognised Indonesias independence, he established himself as the "father of Indonesian film" with Darah dan Doa (1950).   SPFC would produce four further films before being dissolved in 1949.  

==Notes==
 

==Footnotes==
 

==Works cited==
 
* {{cite web
  | title = Abisin Abbas   Filmografi
  | trans_title = Abisin Abbas   Filmography
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b9bad3846f1c_andjar-asmara/filmography
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 23 September 2012
  | archiveurl = http://www.webcitation.org/6AtD74kp3
  | archivedate = 23 September 2012
  | ref =  
  }}
* {{cite web
  | title = Ali Yugo
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b9bad932aa5f_ali-yugo
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 1 December 2012
  | archiveurl = http://www.webcitation.org/6CaSr61kQ
  | archivedate = 1 December 2012
  | ref =  
  }}
*{{cite web
 |title=Andjar Asmara
 |language=Indonesian
 |url=http://www.jakarta.go.id/jakv1/encyclopedia/detail/709
 |work=Encyclopedia of Jakarta
 |publisher=Jakarta City Government
 |ref= 
 |accessdate=7 August 2012
 |archivedate=7 August 2012
 |archiveurl=http://www.webcitation.org/69jK75pUT
}}
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
* {{cite web
  | title = Djauhari Effendi
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4bc56cfc79c29_djauhari-effendi
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 1 December 2012
  | archiveurl = http://www.webcitation.org/6CaT0jxD4
  | archivedate = 1 December 2012
  | ref =  
  }}
* {{cite web
  | title = Gadis Desa
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-g009-49-002956_gadis-desa
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 23 August 2012
  | archiveurl = http://www.webcitation.org/6A7i1GLaT
  | archivedate = 23 August 2012
  | ref =  
  }}
* {{cite web
  | title = Gadis Desa   Kredit
  | trans_title = Gadis Desa   Credits
  | language = Indonesian
  | url =http://filmindonesia.or.id/movie/title/lf-g009-49-002956_gadis-desa/credit
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 1 December 2012
  | archiveurl = http://www.webcitation.org/6Ca0pOOj1
  | archivedate = 1 December 2012
  | ref =  
  }}
*{{cite book
 |url=http://books.google.ca/books?id=sl92GYNaKJIC
 |title=A to Z about Indonesian Film
 |language=Indonesian
 |isbn=978-979-752-367-1
 |last=Imanjaya
 |first=Ekky
 |ref=harv
 |publisher=Mizan
 |location=Bandung
 |year=2006
}}
* {{cite news
  | last = Kurniasari
  | first = Triwik
  | title = Reviving Usmar Ismail’s legacy
  | url = http://www.thejakartapost.com/news/2012/06/24/reviving-usmar-ismail-s-legacy.html
  | work = The Jakarta Post
  | date = 24 June 2012
  | accessdate = 23 September 2012
  | archiveurl = http://www.webcitation.org/6AtBXkwEm
  | archivedate = 23 September 2012
  | ref =  
  }}
* {{cite web
  | title = Ruth Threse Niesen
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4bc6f4ab6d3cf_ratna-ruthinah
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 1 December 2012
  | archiveurl = http://www.webcitation.org/6CaS4blsw
  | archivedate = 1 December 2012
  | ref =  
  }}
* {{cite book
  | title = Profil Dunia Film Indonesia
  | trans_title=Profile of Indonesian Cinema
  | language = Indonesian
  | last = Said
  | first = Salim
  | publisher = Grafiti Pers
  | location = Jakarta
  | year = 1982
  | oclc = 9507803
  | ref = harv
  }}
* {{cite web
  | title = South Pacific Film   Filmografi
  | trans_title = South Pacific Film   Filmography
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmo4b9bcdde9ed16_south-pacific-film/filmography
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 1 December 2012
  | archiveurl = http://www.webcitation.org/6Ca1Uc78e
  | archivedate = 1 December 2012
  | ref =  
  }}
 
 

 
 
 
 