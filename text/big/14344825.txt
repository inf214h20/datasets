Le Roi danse
{{Infobox Film
| name           = Le Roi danse
| image          = Le Roi danse - DVD cover - IMDB.jpg
| caption        = 
| director       = Gérard Corbiau
| producer       = 
| writer         = Marcel Beaulieu Andrée Corbiau Gérard Corbiau Ève de Castro Didier Decoin
| starring       = Benoît Magimel Boris Terral Tchéky Karyo
| music          = 
| cinematography = Gérard Simon
| editing        = Philippe Ravoet Ludo Troch
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = France Germany Belgium
| language       = French
| budget         = 
}} Enlightenment figure Louis XIV of France in his conflicts with the Catholic establishment, focuses on Lullys personal and possibly romantic relationship with the King, as well as his camaraderie with Molière and rivalry with Robert Cambert.

==Plot== Versailles favourite Molière (Tchéky Karyo) are keen to further disarm the old court but they get to understand their limits when conflict becomes more manifest at events such as staging (and consequent ban) of Tartuffe in 1664. Meanwhile, the passing years bring an end to Lullys position as the kings dance teacher and choreographer and he also has to face the emotional tensions growing with his wifes niece Julie (Claire Keim), which will culminate at the gala of Camberts Pomone (opera)|Pomone in 1671.

==Cast==
* Benoît Magimel : Louis XIV
* Boris Terral : Jean-Baptiste Lully
* Tchéky Karyo : Molière
* Johan Leysen : Robert Cambert
* Cécile Bois : Madeleine Lambert
* Colette Emmanuelle : Anne of Austria
* Claire Keim : Julie Prince de Conti
* Caroline Veyt : Armande Béjart
* Ingrid Rouif : Madame de Montespan
* Jacques François: Jean de Cambefort
* Pierre Gérald: Jean-Baptiste Boësset

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 