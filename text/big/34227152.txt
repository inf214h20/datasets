Forty Thieves (film)
{{Infobox film
| name           =  Forty Thieves
| image          =
| image_size     =
| caption        =
| director       = Lesley Selander
| producer       = Harry Sherman, Lewis J. Rachmil (associate producer) Michael Wilson (screenplay) Bernie Kamins (screenplay) Clarence E. Mulford (characters)
| narrator       = William Boyd, Andy Clyde
| music          = Mort Glickman
| cinematography = Russell Harlan
| editor         = Carroll Lewis
| distributor    = United Artists
| released       = 1944
| runtime        = 60 min.
| country        = United States English
| budget         =
}}
 American Western western starring William Boyd in the lead role of Hopalong Cassidy.  It was directed by Lesley Selander, produced by Harry Sherman and released by United Artists.

==Cast== William Boyd as Hopalong Cassidy
*Andy Clyde as Deputy California Carson
*Jimmy Rogers as Deputy Jimmy Rogers
*Douglas Dumbrille as Tad Hammond
*Louise Currie as Katherine Reynolds
*Kirk Alyn as Jerry Doyle
*Herbert Rawlinson as Buck Peters
*Robert Frazer as Judge Reynolds
*Glenn Strange as Ike Simmons
*Hal Taliaferro as Jess Clanton
*Jack Rockwell as Sam Garms
*Bob Kortman as Joe Garms

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 