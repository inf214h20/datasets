A Kentucky Cinderella
{{Infobox film 
| name = A Kentucky Cinderella
| image =
| caption =
| director = Rupert Julian
| writer =
| starring = Ruth Clifford
| producer = Bluebird Photoplays
| distributor =
| budget =
| released =   
| country = United States English intertitles
| runtime = 5 reels
| language = Silent
}}

A Kentucky Cinderella is a 1917 silent film adapted from an 1898 short story by Francis Hopkinson Smith.

The film was directed by  , in June 1917.  ,   (July 7, 1917), p. 75    Its preservation status is unknown.

The short story on which the film is based, of the same name, first appeared in the Ladies Home Journal in late 1898, and was included in Smiths 1899 short story collection The Other Fellow.

The 1921 film Desperate Youth is also based on the short story.

==References==
 

==External links==
* 
* 
* , afi.com entry
* , at silentera.com
* , original short story

 
 
 
 
 
 
 
 
 


 