Wages of Virtue
{{Infobox film
| name           = Wages of Virtue
| image          = 
| alt            = 
| caption        = 
| director       = Allan Dwan
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Forrest Halsey Percival Christopher Wren
| starring       = Gloria Swanson Ben Lyon Norman Trevor Ivan Linow Armand Cortes Adrienne DAmbricourt Paul Panzer
| music          =  George Webber
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Allan Dwan and written by Forrest Halsey and Percival Christopher Wren. The film stars Gloria Swanson, Ben Lyon, Norman Trevor, Ivan Linow, Armand Cortes, Adrienne DAmbricourt and Paul Panzer. The film was released on November 10, 1924, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Gloria Swanson as Carmelita
*Ben Lyon as Marvin
*Norman Trevor as John Boule
*Ivan Linow	as Luigi
*Armand Cortes as Giuseppe 
*Adrienne DAmbricourt as Madame Cantinière
*Paul Panzer as Sergeant LeGros
*Joe Moore as Le Bro-way

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 