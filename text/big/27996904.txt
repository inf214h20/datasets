Lily, aime-moi
 
{{Infobox film
| name           = Lily, aime-moi
| image          = 
| caption        = 
| director       = Maurice Dugowson
| producer       = Michel Seydoux
| writer         = Maurice Dugowson Michel Vianey
| starring       = Patrick Dewaere
| music          = 
| cinematography = André Diot
| editing        = Cécile Decugis
| distributor    = 
| released       = 22 April 1975
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
}}

Lily, aime-moi is a 1975 French comedy film directed by Maurice Dugowson. It was entered into the 25th Berlin International Film Festival.   

==Cast==
* Jean-Michel Folon - François
* Patrick Dewaere - Gaston, dit Johnny Cash Rufus - Claude Zouzou - Lily
* Juliette Gréco - Flo
* Jean-Pierre Bisson - Le frère de Flo / Flos brother
* Roger Blin - Le père de Lily / Lilys father
* Jean Capel
* Roland Dubillard - Lintellectuel de chez Flo / Intellectual at Flos
* Pauline Godefroy - Une invitée chez Flo
* Anne Jousset - Lauto-stoppeuse / Hitchhiker
* Tatiana Moukhine - La mère de Lily / Lilys mother
* Maurice Vallier - Le rédacteur chez François / François coworker
* Andréas Voutsinas - Le barbu de chez Flo / Bearded man at Flos
* Henry Jaglom - Un invité chez Flo / A guest at Flos
* Bernard Freyd
* Miou-Miou - La fille dans le café / The girl in the coffee-shop

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 