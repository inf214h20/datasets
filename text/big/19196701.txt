My Heroes Have Always Been Cowboys (film)
{{Infobox film
| name           = My Heroes Have Always Been Cowboys
| image_size     =
| image          = My Heroes Have Always Been Cowboys FilmPoster.jpeg
| caption        =
| director       = Stuart Rosenberg
| producer       = E.K. Gaylord II Martin Poll
| writer         = Joel Don Humphreys Ben Johnson
| music          = James Horner
| cinematography = Bernd Heinl
| editing        = Dennis M. Hill
| distributor    = The Samuel Goldwyn Company
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3,603,615 
}}
 Western drama film starring Scott Glenn and Kate Capshaw and directed by Stuart Rosenberg.

==Plot==
Scott Glenn is H.D., a champion rodeo rider whose career is ruined after being gored by a bull. He returns home to discover things have drastically changed — the family farm has been abandoned, his old girlfriend Julie (Kate Capshaw) is a now a widowed mother, and his sister Cheryl (Tess Harper) has put his father (Ben Johnson) in a nursing home. H.D. rescues his father from the home and returns him to the ranch. But when H.D. leaves the farm to visit Julie, his father seeks out Cheryl. Cheryl retaliates by threatening to return her father to the nursing home and sell the ranch. At this point, H.D. takes notice of a rodeo contest which would give him $100,000 if he can ride four bulls for a total of 32 seconds. H.D. bonds with his father as he gruelingly prepares for a return to the rodeo to win the contest and buy the ranch. 

==Cast==
* Scott Glenn.....H.D. Dalton
* Kate Capshaw.....Jolie
* Balthazar Getty.....Jud Ben Johnson.....Jesse Dalton
* Gary Busey.....Clint Hornby
* Tess Harper.....Cheryl
* Mickey Rooney.....Junior
* Clarence Williams III.....Deputy Sheriff Virgil
* Bill Clymer.....Rodeo Announcer

==Reception==
Balthazar Getty was nominated for best young actor of the year by the Young Artist Awards.
 The Right The River, The Silence Rocky ending. 

The score by James Horner was well received.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 