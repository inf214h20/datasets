They Don't Wear Black-tie
{{Infobox film 
 | name = They Dont Wear Black Tie
 | image = They Dont Wear Black Tie.jpg
 | caption = 
 | director = Leon Hirszman 
 | writer = Leon Hirszman Gianfrancesco Guarnieri
 | based on =  
 | starring = Gianfrancesco Guarnieri Fernanda Montenegro Carlos Alberto Riccelli Bete Mendes
 | music = Radamés Gnattali Adoniran Barbosa Chico Buarque Gianfrancesco Guarnieri
 | cinematography = Lauro Escorel
 | editing = Eduardo Escorel
 | producer = Leon Hirszman
 | studio = Leon Hirszman Produções Embrafilme
 | distributor = Embrafilme
 | released =   }}
 | runtime = 123 minutes
 | country = Brazil
 | language = Portuguese
 | budget =
 }} Brazilian drama play of the same name. 

== Plot == syndicalist leader, and Romana are the parents of Tião, whose girlfriend, Maria, becomes pregnant. Fearing to be fired and thus cannot support his now fiancée, Tião does not participate on a strike, what starts a series of family conflicts.

== Cast ==
* Gianfrancesco Guarnieri as Otávio
* Fernanda Montenegro as Romana 
* Carlos Alberto Riccelli as Tião
* Bete Mendes as Maria
* Milton Gonçalves as Bráulio 
* Francisco Milani as Sartini
* Lélia Abramo as Marias mother
* Fernando Ramos da Silva as Marias brother

==Reception== Special Jury Prize.  It won the Best Film Award at the 3rd Havana Film Festival,  at the 26th Valladolid International Film Festival,  and shared with Plae Kao at the 3rd Three Continents Festival. 

==References==
 

==Further reading==
* 

==External links==
* 

 
 
 
 
 
 
 
 


 