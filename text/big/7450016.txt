The Littlest Rebel
{{Infobox film
| name = The Littlest Rebel
| image = The Littlest Rebel 1935 film poster.jpg
| image_size =
| alt =
| caption = Theatrical poster David Butler
| producer = Darryl Zanuck (producer) Buddy G. DeSylva (associate producer)
| writer = Screenplay:   John Boles Jack Holt Karen Morley Bill Robinson Shirley Temple
| music =
| cinematography = John F. Seitz
| editing = Irene Morra
| distributor = 20th Century Fox
| released =  
| runtime = 70 minutes
| country = United States
| language = English
| budget =
| gross =more than $1 million   accessed 19 April 2014 
}}
 1935 American David Butler. John Boles, Jack Holt Union officer.   
 Curly Top, was listed as one of the top box office draws of 1935 by Variety (magazine)|Variety.  The film was the second of four cinematic pairings of Temple and Robinson.  In 2009, the film was available on videocassette and DVD in both black-and-white and computer-colorized versions.

==Plot==
The film opens in the ballroom of the Cary plantation on Virgie’s sixth birthday.   Her slave Uncle Billy dances for her party guests, but the celebration is brought abruptly to an end when a messenger arrives with news of the assault on Fort Sumter and a declaration of war.   Virgie’s father is ordered to the Armory with horse and side-arms. He becomes a scout for the Confederate Army, crossing enemy lines to gather information.  On these expeditions, he sometimes briefly visits his family at their plantation behind Union lines.  

One day, Colonel Morrison, a Union officer, arrives at the Cary plantation looking for Virgie‘s father. Virgie defies him, hitting him with a pebble from her slingshot and singing “Dixie”.   After Morrison  leaves, Cary arrives to visit his family but quickly departs when slaves warn of approaching Union troops. Led by the brutal Sgt. Dudley, the Union troops begin to loot the house.  Colonel Morrison returns, puts an end to the plundering, and orders Dudley lashed.  With this act, Morrison rises in Virgie’s esteem.  

One stormy night, battle rages near the plantation. Virgie and her mother are forced to flee with Uncle Billy when their house is burned to the ground.  Mrs. Cary falls gravely ill but finds refuge in a slave cabin.  Her husband crosses enemy lines to be with his wife during her last moments.  After his wife’s death, Cary makes plans to take Virgie to his sister in Richmond. When Colonel Morrison learns of the plan, he aids Cary by providing him with a Yankee uniform and a pass.  The plan is foiled, and Cary and Morrison are sentenced to death.  

The two are confined to a makeshift prison where Virgie and Uncle Billy visit them daily.  A kindly Union officer urges Uncle Billy to appeal to President Lincoln for a pardon.   Short on funds, Uncle Billy and Virgie sing and dance in public spaces and ‘pass the cap’.   Once in Washington, they are ushered into Lincoln’s office where the President pardons Cary and Morrison after hearing Virgie’s story.  The film ends with Virgie happily singing “Polly Wolly Doodle” to her father, Colonel Morrison and a group of soldiers.

== Cast ==
*Shirley Temple as Virgie Cary, the six-year-old daughter of plantation owner Herbert Cary John Boles as Herbert Cary, a plantation owner, a Confederate officer, and Virgie’s father Jack Holt as Colonel Morrison, a Union officer 
*Karen Morley as Mrs. Cary, Herbert’s wife and Virgie’s mother Guinn Williams as Sergeant Dudley, a brutal Union officer 
*Frank McGlynn, Sr. as President Abraham Lincoln
*Bill Robinson as Uncle Billy, a Cary slave  
*Willie Best as James Henry, a Cary slave
*Bessie Lyle as Mammy Rosabelle, a Cary slave
*Hannah Washington as Sally Ann, a Cary slave

==Production==
Off-camera, Temple told associate producer Buddy DeSylva, “Of course the pardon has to be granted. We can’t make a heavy out of Lincoln.” 

Both Boles and Robinson nearly drowned in an escape scene when a log they were riding down a river overturned under their combined weight.  Boles swam to safety but Robinson was rendered unconscious in the accident and was rescued by a crew member. 

The slingshot scene was written into the movie by screenwriter Edwin Burke after he learned of Temples natural ability to use the slingshot. She was perfectly on target and needed only one take for the scene. Temple made international headlines when in the context of trying to keep noisy doves on the prison set (which the director explained did not belong in war, like down in Africa) she asked "Why doesnt someone make Mussolini stop?" Someone overheard her comment and it made it into the newspapers, angering Mussolini.  

Virgie’s party scene, its sudden end with the announcement of the assault on Fort Sumter, and her  boredom with war possibly influenced Margaret Mitchell’s barbecue scene in Gone With the Wind.  Although the main body of Mitchell’s novel was finished by early 1935, the opening was not completed until late in the year.  Mitchell was a filmgoer, and Temple films were among her favorites. 

==Release==
===Critical responses===
Andre Sennwald in his New York Times review of December 20, 1935 wrote, “The film is shrewdly spiced with humor and there is a winning quality in the utter shamelessness of its sentimental phases.”  He praised the performances of the principals, the dance routines, and the film‘s production values.  He thought  Temple “the most improbable child in the world” and that the film is “an eventful slice of meringue and quite the most palatable item in which the baby has appeared recently”. 
 Hal Erickson happy darkies nervously pondering the prospect of being freed from slavery and shivering in their boots when the Yankees arrive." 

  and Dimples (film)|Dimples is enough to warrant a clear critical caveat." However Gibron, echoing most film critics who continue to see value in Temples work despite the racism that is present in some of it, also wrote: "Thankfully, the talent at the center of these troubling takes is still worthwhile for some, anyway." 

===Home media===
In 2009, the film as available on both videocassette and DVD in the original black-and-white version and a computer-colorized version of the original.  Some versions included theatrical trailers and other special features .

==Adaptations==
The Littlest Rebel was dramatized as an hour-long radio play on the October 14, 1940 broadcast of Lux Radio Theater, with Shirley Temple and Claude Rains .

==See also==
* Shirley Temple filmography

==References==
;Footnotes
 

;Works cited
*  
*  

;Bibliography
*    The author expounds upon father figures in Temple films.
*   In the essay, "Cuteness and Commodity Aesthetics: Tom Thumb and Shirley Temple", author Lori Merish examines the cult of cuteness in America.
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 