Ottran
|{{Infobox film|
| name     = Ottran
| image          = 
| director       = Ilankannan
| writer         = Ilankannan Simran Manorama Manorama
| producer       = Gandhilal Bansali
| distributor    = 
| cinematography = K. S. Selvaraj
| editing = P. Sai Suresh
| released   = 24 October 2003|
| runtime        = 
| language = Tamil 
| music = Pravin Mani
}}
 Tamil film directed by score and soundtrack was composed by Pravin Mani. The film was a hit at box office. 

==Cast== Arjun as Karthik
*Simran Bagga as Sudha
*Vadivelu as Maadasaamy Manorama as Karthiks Mother
*Tejashree
*Gaurav Chopra as a Terrorist
*Sarath Babu
*Riyaz Khan as Peer Mohammed
* Ajay Rathnam as Saran
*Pyramid Natarajan as Kumara Saamy
*Mahanadhi Shankar
*Singamuthu
*Rajeev
*Anu Mohan Chitti Babu as Marriage Broker
*Sathya Prakash
*Hemanth
*Shyam Ganesh
*Hari Nair
*Dharmesh
*Vimal Raj
*Chitra
*Vishal
*Satheesh
*Ambareesh
*Peer Mohammed
*Baby Rani

==Production==
After the Arjun-directed Ezhumalai, the Arjun-Simran pair come together in yet another film titled ‘Ottran’. Directing the film is first-timer Ilankannan, who had apprenticed with director Shanker. Arjun plays a secret agent in the film, which gives him scope for enough of action scenes. Shooting commenced in Chennai in a forty-day schedule. 

==Soundtrack==
{{Infobox Album |  
| Name        = Ottran
| Type        = soundtrack
| Artist      = Pravin Mani
| Cover       = 
| Released    = 2003
| Recorded    =  Feature film soundtrack |
| Length      = 
| Label       = Hit Music
| Producer    = Pravin Mani
| Reviews     = 
}}

These 6 songs in Ottran are composed by Pravin Mani. 

*"Yeh Thiththippey" - Karthik (singer)|Karthik, Suchitra Sujatha
*"Chinna Veeda"  -  Manikka Vinayagam, Srilekha Parthasarathy
*"Kitchu Kitchu"  -  Shankar Mahadevan, Lavanya
*"Uttalangadi"   -  Manikka Vinayagam Sujatha

==Critical reception==
Hindu wrote:"first half of the film is the screenplay that allows no room for sluggishness or dampeners. The film moves on at breakneck speed and by the time you take a breather its intermission". 

==References==
 

 
 
 
 