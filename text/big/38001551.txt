A Kitten for Hitler
 
 
{{Infobox film
| name           = A Kitten for Hitler
| image          = A Kitten for Hitler.jpg
| image_size     =
| border         =
| alt            =
| caption        = Screenshot
| director       = Ken Russell Dan Schreiber
| writer         = Ken Russell and Emma Millions Phil Pritchard, Rosey Thewlis, Rusty Goffe
| music          =
| cinematography =
| editing        = Michael Bradsell
| studio         =
| distributor    =
| released       =  
| runtime        = 8 minutes 20 seconds
| country        = United Kingdom
| language       = English
| budget         = £1,000,000
| gross          =
}} dwarf actor appearing in the lead role of a Jewish child. It was released on Comedybox.tv on 1 July 2007.

==Plot==
In 1941, a Jewish boy named Lenny is watching a newsreel at a theatre in Brooklyn with his mother when one featuring Adolf Hitler appears. The crowd boos. The child questions why no one likes him and wonders what Santa will give him for Christmas. His mother tells him that no one will get Hitler anything for Christmas. The boy travels to Germany to give Hitler a kitten for Christmas. Once he arrives, he presents the boxed gift to Hitler, who initially thinks it is a bomb, and throws it to Eva Braun. She opens it and gives the kitten to Hitler, who is apparently deeply moved. Hitlers mood changes when he discovers that the child is Jewish and has a swastika-shaped birthmark on his stomach. Hitler orders Eva to kill the child and make his skin into a lampshade, which the couple then have on their bedside table lamp. Following the war, the lamp is returned to the boys mother who keeps it as a memento. When she touches it, it supernaturally lights up of its own accord and the swastika magically transforms into a Star of David. A miracle is proclaimed. The president awards  the purple heart to the lampshade.

==Cast== Phil Pritchard
*Eva Braun – Rosey Thewlis
*Lennys Mum – Lisi Tribble
*Lenny – Rusty Goffe
*Harry S. Truman – Rufus Graham

==Production==
Following a discussion about film censorship with British broadcaster Melvyn Bragg while they worked on The South Bank Show,      Russell was challenged by Bragg to create a film which Russell himself would want banned.    A Kitten for Hitler was the result of the process that followed.  After Russell sent Bragg an initial draft, Bragg responded "Ken, if ever you make this film and it is shown, you will be lynched." 
 Phil Pritchard. dwarf actor green screen Dan Schreiber by screenwriter Emma Millions. 

==Reception== The Devils,  although he also described it as a comedy.  Russell told the story of the film to fellow director Quentin Tarantino at a film festival, who reacted favourably.  In 2012 it was included in a list of six alternative Christmas films by British newspaper The Guardian. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 