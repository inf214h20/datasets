Wigstock: The Movie
{{Infobox film
| name           = Wigstock: The Movie
| image          = Wigstock-the-movie.jpg
| caption        = Theatrical release poster
| director       = Barry Shils
| producer       = Dean Silvers  
| writer         =
| starring       =
| music          = Robert Reale
| cinematography =
| editing        = Tod Scott Brody
| distributor    = Samuel Goldwyn Company
| released       = June 9, 1995
| runtime        = 85 minutes
| country        = USA
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} drag music East Village through the 1980s and 1990s. The film presents a number of performances from the 1994 festival, including Crystal Waters, Deee-Lite, Jackie Beat, Debbie Harry, Leigh Bowery, Joey Arias and the Dueling Bankheads. The film also captures a performance by RuPaul at the height of his mainstream fame during the 90s.
 Mistress of Ceremonies Lady Bunny on the telephone with a city representative inquiring about the possibility of placing a wig on the Statue of Liberty.

==Soundtrack==
 {{Infobox album | 
| Name        = Wigstock: The Movie
| Type        = Soundtrack
| Artist      = Various artists
| Cover       =
| Released    =  1995
| Recorded    = Dance
| Length      = approximately 1 hour
| Label       = Elektra Entertainment Group
| Producer    = Various producers
| Reviews     =
|
| Last album  =
| This album  =
| Next album  =
}}
A soundtrack album for the film, featuring many of the performers included in the film, was released in 1995. In addition to the performances the CD includes comments delivered from the stage by Lady Bunny.

===Song listing===
# Lady Bunny - "Calling Lady Liberty"
# Lady Miss Kier - "Touch Me with Your Sunshine"
# Erasure - "Cold Summers Day"
# Crystal Waters - "100% Pure Love"
# NYGs - "The Real Thing"
# Stephen Tashjian|Tabboo! - "Its Natural"
# Jackie Beat - "Kiss my Ass"
# RuPaul - "Free to Be"
# Billie Ray Martin - "Space Oasis" Chic - "Chic Cheer"
# Mistress Formika - "Fight for Your Right (to be Queer)" Nancy Boy - "All That Glitters (Come Out Come Out)"
# Joey Arias - "Them There Eyes"
# Donna Giles with David Morales - "Gimme Luv" Loveland - "Hope (Never Give Up)"
# Jon of the Pleased Wimmin - "Passion"
# Marc Almond - "What Makes a Man a Man"
# Deee-Lite - "Somebody"

==DVD release==
Wigstock: The Movie was released on Region 1 DVD on June 30, 2003.

==1987 film==
In 1987, filmmaker   circuit in 1998. The title sequence of the 1995 Goldwyn film is cut from footage from the 1987 Rubnitz film.

==External links==
*  on IMDB
*  on IMDB

 
 
 
 
 