The Rose of Blood
 
{{Infobox film
| name           = The Rose of Blood
| image          = The-Rose-of-Blood-1917.jpg
| caption        = Film poster (1917)
| director       = J. Gordon Edwards William Fox
| writer         = Bernard McConville
| story          = Ryszard Ordynski
| starring       = Theda Bara Genevieve Blinn Charles Clary
| cinematography = John W. Boyle Rial Schellinger
| editing        = 
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 5 reels
| country        = United States 
| language       = Silent with English intertitles
}}
 silent drama film directed by J. Gordon Edwards and starring Theda Bara. Based on the story "The Red Rose" by Ryszard Ordynski, the film was written by Bernard McConville.

==Cast==
* Theda Bara as Lisza Tapenka
* Genevieve Blinn as Governess
* Charles Clary as Prince Arbassoff
* Marie Kiernan as Kosyla Joe King as Prime Minister
* Herschel Mayall as Koliensky
* Ryszard Ordynski as Vassea
* Hector Sarno as Revolutionist
* Bert Turner as Princess Arbassoff

==Preservation status==
The Rose of Blood is now considered a lost film.    

==References==
 

==See also==
*List of lost films

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 