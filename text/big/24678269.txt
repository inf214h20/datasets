Sharada (1957 film)
{{Infobox film
| name           = Sharada
| image          = Sharada8.jpg
| caption        = Poster
| director       = L.V. Prasad
| producer       = 
| writer         = Vishwamitter Adil Inder Raj Anand Pinisetty Vempati Sadasivabrahmam
| narrator       = 
| starring       = Raj Kapoor Meena Kumari Shyama Raj Mehra Anita Guha
| music          = C. Ramchandra Rajinder Krishan (lyrics)
| cinematography = M.W. Mukadam
| editing        = Shivaji Awdhut
| distributor    = Yash Raj Films
| released       = 1957
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1957 Cinema Indian Bollywood film directed by L.V. Prasad. The film stars Raj Kapoor and Meena Kumari in lead roles, and Shyama, Raj Mehra and Anita Guha in supporting roles.
 highest grossing film at the Indian Box Office in 1957. 

==Plot==
Chiranjeev ( ), falls ill due to alcohol, and gets treated at an  ), who come from a reputed family background. Things then get complicated when Chanchal finds that her husband and his stepmother had been in love before and may not have got over their feelings for each other.

==Cast==

* Raj Kapoor ...  Chiranjeev / Shekhar
* Meena Kumari ...  Sharada Ram Sharan
* Shyama ...  Chanchal
* Anita Guha ...  Padma Agha ...  Ganesh Gope ...  Hukamdas
* Raj Mehra ...  Kashiram Manorama ...  Lajjo

==Awards==
*Filmfare Best Supporting Actor Award - Raj Mehra
*Filmfare Best Supporting Actress Award - Shyama
*Filmfare Best Editing Award - Shivaji Awdhut

==Music==

The soundtrack of the film contains 8 songs. The music is composed by C. Ramchandra, with lyrics authored by Rajinder Krishan

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)
|-
|1.
| "Duniya Ne Mujh Ko Chhod Diya" Manna Dey
|-
|2.
| "O Chand Jahan Woh Jaaye" Lata Mangeshkar, Asha Bhosle
|-
|3.
| "Achha Hai Mauka" Asha Bhosle
|-
|4.
| "Ajure Baju Naju" Asha Bhosle, Joe Alvares
|-
|5.
| "Chahe Zindagi Se" Manna Dey
|-
|6.
| "Jap Jap Jap Jap Re" Mukesh (singer)|Mukesh
|-
|7.
| "Joru Ka Ghulam" Asha Bhosle, Shamshad Begum
|-
|8.
| "Lahraye Jiya" Asha Bhosle
|}

==References==
 

==External links==
* 

 
 
 
 
 