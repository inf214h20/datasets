A Chinese Tall Story
 
 
{{Infobox film
| name           = A Chinese Tall Story
| image          = AChineseTallStory2005Poster.jpg
| caption        = 
| director       = Jeffrey Lau
| producer       = Albert Lee
| writer         = Jeffrey Lau Steven Cheung Joe Phua
| music          = Joe Hisaishi
| cinematography =
| editing        =
| distributor    = Emperor Motion Pictures
| released       =  
| runtime        =
| country        = Hong Kong
| language       = Cantonese
| budget         =
}}
A Chinese Tall Story ( ) is a 2005 Hong Kong fantasy adventure film written and directed by Jeffrey Lau. The story is loosely based on the novel Journey to the West.

==Synopsis==
It is a twisted story about the monk Tripitaka and his three disciples who are journeying west to acquire Buddhist scriptures. While stopping in Shache City (present day Yarkant County|Yarkand), they come under attack by minions of the evil Tree Demon. The demons capture his three disciples.

Tripitaka is then captured by the king of reptiles and placed under the care of the ugly and shunned Meiyan, who falls in love with the monk. Luckily for Tripitaka, an alien princess rescues him, and Meiyan decides to team up with the princess in order to rescue the disciples.

==Plot==

Tang monk Tripitaka (Nicholas Tse) and his three disciples Monkey King Sun WuKong (Bolin Chen), Pig Monk Zhu WuNeng (Kenny Kwan), and Sand Monk Sha WuJing (Steven Cheung) arrive triumphantly to a hero’s welcome in Shache city. Little do they know that ahead of them lays Tripitaka’s most arduous challenge before he achieves deification…and it is a test that he and only he alone can overcome.

During their stay in the city, the three disciples are captured by evil Tree Spirits. Tripitaka borrows the Golden Pole and tries to find a way to save them. He meets a young lizard imp Meiyan (Charlene Choi) who is more than a visual match for Quasimodo: matted bushy hair, and teeth of any dentist’s nightmare. Meiyan falls in love with Tripitaka at first sight and devotes herself to trailing him. She even sets a love trap to ensnare him. Tripitaka unwittingly falls into the trap and in the process breaks the Heavenly Code.

The region is one full of monsters, strange beings and creatures of unknown origins and among them are the beautiful Princess XiaoShan (Fan Bing-bing) and her army. On a passing journey to Earth her path crosses Tripitaka’s and she vows her aid. Tripitaka decides to leave with the Princess.

Meiyan is heartbroken. She picks a fight with Princess XiaoShan and, although she loses, she finally discovers her own identity as a galactic warrior. She eventually helps the princess in defeating the enemy and rescues Tripitaka and his disciples.

After the battle, Meiyan surrenders to the Temple of Heaven for judgment. Torn between passion and righteousness, Tripitaka rebels against the heavens to rescue the gallows-bound Meiyan. A benevolent Buddha is moved and pardons the two on condition that they embark on a journey to the West to accomplish the Eight-One Tasks to redeem themselves and save the world.

==Cast== Tripitaka
*Charlene Choi as Yue Meiyan
*Fan Bingbing as Princess Xiaoshan Monkey King Pig Monk Steven Cheung Sand Monk Red Child Patrick Tam
*Yuen Wah as Turtle
*Kenny Bee
*Joe Phua
*Tats Lau
*Yat-fei Wong
*Michael Chan
*Gordon Liu
*Kara Hui
*Lee Kin-Yan

 
 

==Soundtrack==
{{Infobox album
| Name        = A Chinese Tall Story
| Type        = soundtrack
| Artist      = Joe Hisaishi
| Cover       = A_chinese_tall_storyost.jpg
| Length      = 62:00
| Recorded    =
| Released    = 22 December 2005
| Label       = Emperor Entertainment Group
| Producer    =
}}
All compositions by Joe Hisaishi.

# "Sacred Love"
# "Prologue - Triumphant Entrance"
# "Dogfight Over Shache"
# "Words Are Lethal"
# "Rout Of The Four Heavenly Knights"
# "Lovers Gambit"
# "Longing for You"
# "Yours Truly, Tripitaka"
# "The Conspiracy"
# "Capitulation"
# "Twirling Snow"
# "Alien Invasion"
# "I Can Fly!"
# "Help Is on the Way"
# "Annihilation Of The Tree Spirit"
# "The Princesss Secret"
# "Storming of the Celestial Court"
# "I Know"
# "Divine Manifestation"
# "A Journey West"

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 