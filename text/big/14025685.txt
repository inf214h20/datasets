Every Girl Should Be Married
{{Infobox film
| name           = Every Girl Should Be Married
| image_size     = 
| image          = Every Girl Should Be Married FilmPoster.jpeg
| caption        = 
| director       = Don Hartman
| producer       = Don Hartman
| writer         = Stephen Morehouse Avery Eleanor Harris (story) Don Hartman
| starring       = Betsy Drake Cary Grant Franchot Tone
| music          = Charles Trénet
| cinematography = Robert De Grasse
| editing        = Frederic Knudtson
| distributor    = RKO Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States English
}}
 1948 American romantic comedy film directed by Don Hartman and starring Cary Grant, Betsy Drake and Franchot Tone. Grant and Drake married a year after the films release.

==Plot summary==
Department store salesclerk Anabel Sims (Betsy Drake) is very enamored with the idea of getting married. So when handsome pediatrician Madison Brown (Cary Grant) asks for her help in making a purchase, she decides that he is the one for her. 

He is quite happy as a bachelor, but Anabel proves to be a very determined schemer. She learns all she can about him, everything from where he went to school to his favorite foods. Madison soon realizes her intentions and does his best to fend off the young woman.

Anabel makes a reservation at a restaurant on a day when she knows that Madison habitually dines there. In an attempt to make him jealous, she pretends to be waiting for wealthy, thrice-married playboy Roger Sanford (Franchot Tone), who happens to be her employer and Madisons university classmate. By chance, Roger shows up. Fortunately for her, Roger believes that she is using Madison as a ruse to get acquainted with him. However, the maneuver fails; Madisons feelings remain unchanged.

Anabel comes up with more ingenious schemes, but they are all unsuccessful. However, Roger falls in love with her. He eventually asks her to marry him, but she only invites him to dinner at her home. When Anabels best friend Julie (Diana Lynn) warns Madison, he begins to worry, knowing something of Rogers success with women. The doctor invites himself to the little soirée. While waiting for Anabel, they are unexpectedly joined by "Old Joe" (Eddie Albert), Anabels longtime hometown beau, who announces that he and Anabel are finally going to get married. At first, Madison congratulates them, but after thinking about it, makes his own bid for her hand. Anabel leaves the decision up to Joe, who bows out, saying that he only wants her to be happy. After Joe leaves, Madison informs Anabel that her research on him was incomplete; he recognized "Joes" voice as that of a radio performer he listens to frequently.

==Main cast and characters==
{| class="wikitable"   
|- Franchot Tone  as Roger Sanford
|- Betsy Drake  as Anabel Sims
|-
|}

* Alan Mowbray as Mr. Spitzer
* Elisabeth Risdon as Nurse Mary Nolan
* Richard Gaines as Sam McNutt
* Harry Hayden as Gogarty
* Chick Chandler as Harry, the Soda Clerk
* Leon Belasco as Violinist
* Fred Essler as Pierre, the Restaurant Owner
* Anna Q. Nilsson as Saleslady

==Music== La Mer is played several times in the film.

==Reception==
The film made a profit of $775,000.$250,000 Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p232 

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 

 