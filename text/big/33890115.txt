Butterfly Crush
 
 
{{Infobox film
| name           = Butterfly Crush
| image          = Butterfly Crush poster.jpg
| caption        = Theatrical release poster
| director       = Alan Clay
| writer         = Alan Clay
| starring       = Amelia Shankley Courtney Hale Hayley Fielding Richard Adams Sally Kelleher
| cinematography = Garfield Darlington Ben Ruffell
| editing        = Anand Doshi
| studio         = Artmedia
| distributor    = Vanguard Cinema (US)
| released       =  
| runtime        = 90 minutes
| country        = New Zealand
| language       = English
}}
Butterfly Crush is a 2010 film, written and directed by New Zealand filmmaker Alan Clay. The film is adapted from his novel Dance Sisters. The story is about a female song and dance duo who threaten to self-destruct on the brink of fame. The girls are up for the Australasian Song Awards, but their chance at success is jeopardised when Eva gets involved with a Kings Cross cult, the Dreamguides, and Moana must risk everything to save her.

Butterfly Crush won a string of Best Feature Drama awards at US festivals in 2011 and an Accolade Award of Excellence for Best Supporting Actor for Amelia Shankleys portrayal of the Dreamguides leader, Star. The film won the Best International Narrative Feature award at the Anthem Film Festival in Las Vegas, which presents ‘the year’s best films about personal and civil liberty’. It has also won the Best Feature Drama at the Indie Gathering Film Festival in Ohio and the Best Feature Film award at the Reel Independent Film Extravaganza in Washington D.C.

==Plot==
Talented young singing and dancing duo, Moana and Eva are performing as Butterfly Crush at Circular Quay in Sydney, when a riot engulfs their show. Moana awakes at the Dreamguides, a cult with which Eva has recently become involved and learns that the riot was just a virtual dream.

Their dream to enter the Australasian Song Awards and become the next big thing starts to evaporate, as Eva slips further into the Dreamguides under the pull of the cult’s magnetic leader, Star, who tries to take control of the duos management. 

Despite her distrust and suspicions about the cult, Moana finds herself being drawn in by attractive young cult member, Matt, as he shows her how the virtual dreaming technology and physical relationships can combine to create a new experience of passion.

Deceived and controlled by Star, Eva quits the duo. Moana realizes she must find a way to turn the tables on Star, if she wants to get Eva and Butterfly Crush back.  She attempts to trap Star but it backfires when Star succeeds in undermining her faith in herself, leaving her badly shaken and wandering Sydney, lost and dazed.

Moana and their manager, Angel, force Star to allow Eva to participate in the Awards, which they win. Matt decides to leave the cult and escapes his cult minders to join the duo to celebrate the win.

==Cast==
*Courtney Hale as Moana
*Hayley Fielding as Eva
*Richard Adams as Matt
*Amelia Shankley as Star
*Sally Kelleher as Angel

==Production==

===Development===
Alan Clay adapted his novel Dance Sisters over a two-year period in 2007/8. The characters and style of the film were developed over three 2-week podcast shoots in 2009 where the musical numbers were also recorded and music videos shot. Through this period podcasts from the central characters were uploaded onto the net and audience feedback shaped the development of the films style.

Coming from a theatre directing and performing background Alan values real emotion in a performance. With this as the goal, he worked with the young cast over a period of 14 months, using a process involving yoga and emotional warm ups to develop their natural talents into compelling performances.

===Casting===
The movie stars award winning actor Amelia Shankley as the leader of the Dreamguides cult and introduces three new young stars, Courtney Hale, Hayley Fielding and Richard Adams. Over 200 actors auditioned in Melbourne, Sydney, Auckland and Wellington for the lead roles.

Amelia Shankley was an award winning child actor winning the Paris Film Festival Best Actress award for the feature film Dreamchild, where she played the girl who inspired Lewis Carroll’s Alice stories. She returns to the industry with Butterfly Crush for which she won the Accolade Award of Excellence for Best Supporting Actor.

===Locations=== Kings Cross. All the interior scenes were shot in Wanganui, New Zealand, where filmmaker Alan Clay is based.

==Awards==
Butterfly Crush won a string of Best Feature Drama awards at US festivals in 2011 and an Accolade Award of Excellence for Best Supporting Actor for Amelia Shankleys portrayal of the Dreamguides leader, Star. 

The film won the Best International Narrative Feature award at the  in Anthem Film Festival in Las Vegas, which presents ‘the year’s best films about personal and civil liberty’. It has also won the Best Feature Drama at the Indie Gathering Film Festival in Ohio and the Best Feature Film award at the Reel Independent Film Extravaganza in Washington D.C.

==Release==
The film was released in New Zealand in 2010 to good reviews  and will be released in Australia in April 2012.  The North American DVD and TV distribution is being done by Vanguard Cinema and Video On Demand by Indie Rights. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 