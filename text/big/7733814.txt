The Great Gatsby (1974 film)
 
{{Infobox film
| name = The Great Gatsby
| image = Great gatsby 74.jpg
| image_size = 220px
| alt =
| caption = Theatrical release poster
| director = Jack Clayton
| producer = David Merrick
| screenplay = Francis Ford Coppola
| based on =  
| starring = {{Plainlist|
*Robert Redford
*Mia Farrow
*Bruce Dern
*Sam Waterston
*Karen Black}}
| music = Nelson Riddle
| cinematography = Douglas Slocombe
| editing = Tom Priestly
| studio = Newdon Productions
| distributor = Paramount Pictures
| released =  
| runtime = 146 minutes  
| country = United States
| language = English
| budget = $6.5 million
| gross = $26,533,200   The Numbers. Retrieved May 22, 2012. 
}} romantic drama film distributed by Newdon Productions and Paramount Pictures. It was directed by Jack Clayton and produced by David Merrick, from a screenplay by Francis Ford Coppola based on F. Scott Fitzgeralds 1925 novel The Great Gatsby.
 Scott Wilson, 1949 version.

==Cast==
* Robert Redford as Jay Gatsby
* Mia Farrow as Daisy Buchanan
* Bruce Dern as Tom Buchanan
* Sam Waterston as Nick Carraway
* Karen Black as Myrtle Wilson Scott Wilson as George Wilson
* Lois Chiles as Jordan Baker
* Edward Herrmann as Ewing Klipspringer
* Howard Da Silva as Meyer Wolfsheim
* Kathryn Leigh Scott as Catherine Wilson, Myrtles sister
* Regina Baff as Miss Baedecker
* Vincent Schiavelli as Thin Man
* Roberts Blossom as Mr. Gatz
* Beth Porter as Mrs. McKee
* Patsy Kensit as Pammy Buchanan

===Casting=== Robert Evans so that his wife Ali MacGraw could play Daisy. After MacGraw left Evans for Steve McQueen, he considered other actresses for the role, including Faye Dunaway, Candice Bergen, Natalie Wood, Katharine Ross, Lois Chiles, Cybill Shepherd, and Mia Farrow. Eventually Farrow was cast as Daisy and Chiles was given the role of Jordan. Warren Beatty, Jack Nicholson, and Steve McQueen were considered for the role of Gatsby but they were rejected or declined the offer. Beatty wanted to direct producer Evans as Gatsby and Nicholson didnt think that MacGraw was right for the role of Daisy, who was still attached when he was approached. Farrow was pregnant during the shooting and the movie was filmed with her wearing loose, flowing dresses and in tight close-ups.

==Production==

===Screenplay===
Truman Capote was the original screenwriter but he was replaced by Francis Ford Coppola. On his commentary track for the DVD release of The Godfather, Coppola makes reference to writing the Gatsby script at the time, though he comments: "Not that the director paid any attention to it. The script that I wrote did not get made."

In 2000 William Goldman, who loved the novel, said he actively campaigned for the job of adapting the script, but was astonished by the quality of Coppolas work:
 I still believe it to be one of the great adaptations... I called him   and told him what a wonderful thing he had done. If you see the movie, you will find all this hard to believe... The director who was hired, Jack Clayton, is a Brit... he had one thing all of them have in their blood: a murderous sense of class... Well, Clayton decided this: that Gatsbys parties were shabby and tacky, given by a man of no elevation and taste. There went the ball game. As shot, they were foul and stupid and the people who attended them were foul and silly, and Robert Redford and Mia Farrow, who would have been so perfect as Gatsby and Daisy, were left hung out to dry. Because Gatsby was a tasteless fool and why should we care about their love? It was not as if Coppolas glory had been jettisoned entirely, though it was tampered with plenty; it was more that the reality and passions it depicted were gone.  

===Filming===
The Rosecliff and Marble House mansions in Newport, Rhode Island, were used for Gatsbys house while scenes at the Buchanans home were filmed at Pinewood Studios in Buckinghamshire, England. One driving scene was shot in Windsor Great Park, UK. Other scenes were filmed in New York City and Uxbridge, Massachusetts.

==Reception==
The film received mixed reviews. The film was praised for its interpretation and staying true to the novel, but was criticized for lacking any true emotion or feelings towards the Jazz Age. Based on 32 total reviews collected by Rotten Tomatoes, the film has an overall approval rating from critics of 41%.  Despite this, the film was a financial success, making $26,533,200  against a $6.5 million budget.

Tennessee Williams, in his book Memoirs (p.&nbsp;178), wrote: “It seems to me that quite a few of my stories, as well as my one acts, would provide interesting and profitable material for the contemporary cinema, if committed to ... such cinematic masters of direction as Jack Clayton, who made of The Great Gatsby a film that even surpassed, I think, the novel by Scott Fitzgerald.”  

Vincent Canbys 1974 review in The New York Times typifies the critical ambivalence: "The sets and costumes and most of the performances are exceptionally good, but the movie itself is as lifeless as a body thats been too long at the bottom of a swimming pool," Canby wrote at the time. "As Fitzgerald wrote it, "The Great Gatsby" is a good deal more than an ill-fated love story about the cruelties of the idle rich.... The movie cant see this through all its giant closeups of pretty knees and dancing feet. Its frivolous without being much fun." 

Variety (magazine)|Variety s review was likewise split: "Paramounts third pass at The Great Gatsby is by far the most concerted attempt to probe the peculiar ethos of the Beautiful People of the 1920s. The fascinating physical beauty of the $6 million-plus film complements the utter shallowness of most principal characters from the F. Scott Fitzgerald novel. Robert Redford is excellent in the title role, the mysterious gentleman of humble origins and bootlegging connections.... The Francis Ford Coppola script and Jack Claytons direction paint a savagely genteel portrait of an upper class generation that deserved in spades what it received circa 1929 and after." 

Roger Ebert gave the movie two and a half stars out of four. He stated, "It would take about the same time to read Fitzgeralds novel as to view this movie -- and thats what Id recommend." 

==Awards and nominations== Best Costume Best Music Best Art Best Cinematography Best Costume Barbara Matera.) Best Supporting Best Supporting Most Promising Newcomer (Sam Waterston).

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 