Return of the Seven
 
{{Infobox film
| name           = Return of the Seven
| image          = Poster of the movie Return of the Seven.jpg
| caption        = 
| director       = Burt Kennedy
| producer       = Ted Richmond
| writer         = Larry Cohen
| narrator       =  Robert Fuller Warren Oates Claude Akins
| music          = Elmer Bernstein
| cinematography = Paul Vogel
| editing        = Bert Bates
| distributor    = United Artists
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}}
Return of the Seven (1966) (also called Return of the Magnificent Seven, and The Magnificent Seven 2) is the first sequel to the Western (genre)|western, The Magnificent Seven (1960). Yul Brynner is the sole returning cast member from the first film, portraying Chris Adams.
 Robert Fuller assumes the role of Vin from Steve McQueen. Various stories say that McQueen was either too busy a star in 1966, that Brynner wanted him back but McQueen hated the script, or that Brynner disliked him and refused to have him in the film. 
 Virgilio Teixeira and Julian Mateos (as Chico, replacing Horst Buchholz). Emilio Fernández is the villain. Fernando Rey portrays a priest. Rey was in the next film, Guns of the Magnificent Seven, as a different character.

Since it was filmed in Spain, Return of the Seven may be considered, along with Guns of the Magnificent Seven, as a  European or spaghetti western.

==Plot==

Fifty gunmen force all of the men in a small Mexican village to ride off with them into the desert.  Among the captured farmers is Chico, who years before was one of seven hired gunslingers responsible for ridding the village of a tyrannical bandit, Calvera.  Chicos wife, Petra, seeks out the other members of the band of whom only two, Chris and Vin, survive.  She begs them to save the village once more.  To replace the deceased members of the group, Chris buys the release of Frank (a taciturn gunman) and Luis (a famous bandit), held in the local jail and recruits Colbee, a ladies man and deadly gunman, and Manuel, a young cockfighter.  

The six men discover that the missing villagers are being used as slave labor to rebuild a desert village and church as a memorial to the dead sons of wealthy rancher Lorca.  In a surprise attack, the six force Lorcas men to leave, and, with Chico, prepare for a counterattack.  The cowed farmers offer no assistance, but the seven defenders successfully repulse Lorcas initial attack.  The rancher then gathers all of the men on his land to rout the seven.  

The situation seems bleak until Manuel discovers a supply of dynamite which the seven use in a counteroffensive.  They are eventually overrun, but Chris emerges victorious from a shootout with Lorca.  The ranchers gang flee, leaving Frank, Luis, and Manuel dead in the fighting.  Chico plans to resettle the village on Lorcas fertile land, and Colbee remains to help teach the villagers how to defend themselves against future attacks - and pursue the available women.  Chris and Vin once more ride off together.

==Reception==
The film was critically panned, with industry journal Variety (magazine)|Variety calling it "unsatisfactory ... plodding, cliche-ridden".  However, composer Elmer Bernstein received an Academy Award nomination for his score.

The film earned an estimated $1.6 million in rentals during its initial release. 

The movie was re-released in 1969 and earned rentals of $1.3 million. 

==Cast==
* Yul Brynner as Chris Adams Robert Fuller as Vin
* Julián Mateos as Chico
* Warren Oates as Colbee
* Claude Akins as Frank
* Elisa Montés as Petra, Chicos Wife
* Fernando Rey as Priest
* Emilio Fernández as Francisco Lorca Virgilio Teixeira as Luis Emilio Delgado
* Rodolfo Acosta as Lopez
* Jordan Christopher as Manuel
* Gracita Sacromonte as Flamenco Dancer
* Carlos Casaravilla as First Peon
* Ricardo Palacios as Jailer
* Felisa Jiminez as Female Prisoner

==References==
 

==External links==
*  
*  
*  
*  , Bob Yareham, VLC News, 12 June 2013. An article about the locations in Spain.

 
 

 
 
 
 
 
 
 
 
 