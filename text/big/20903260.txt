Aaj Ka Mahaatma
{{Infobox film
| name           = Aaj Ka Mahaatma
| image          = 
| image_size     = 
| caption        = 
| director       = Kundan Kumar
| producer       = 
| writer         =
| narrator       = 
| starring       =Randhir Kapoor  Rekha 
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1976
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1976 Bollywood romantic action film directed by Kundan Kumar. The film stars Randhir Kapoor and Rekha .

==Cast==
*Randhir Kapoor ...  Randhir / Ranvir Varma
*Rekha ...  Mala
*Ranjeet ...  Tony Bindu ...  Julie
*Manmohan ...  Shankar
*Manmohan Krishna ...  Khanna
*Kumud Chuggani Purnima
*M.B. Shetty ...  Shetty (as Shetty)
*Keshav Rana
*Abhimanyu Sharma
*Keshto Mukherjee ...  Pedro
*Sanjana
*Dilip Dutt Manorama

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chandni Chand Se Hoti Hai"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "Dekho Ri Kaisi Sundar Bala"
| Lata Mangeshkar
|-
| 3
| "Tha Wo Bhi Kya Zamana"
| Kishore Kumar
|-
| 4
| "Tum Aise Kahan Tak Chhoopoge"
| Lata Mangeshkar
|}

==External links==
*  

 
 
 
 
 


 
 