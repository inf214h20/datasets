Heavy Metal 2000
:This article is about the 2000 film. For the original movie, see Heavy Metal (film).
 
{{Infobox film
| name = Heavy Metal 2000
| image = Heavy Metal 2000 poster.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = DVD cover
| director = Michael Coldewey Michel Lemire
| producer = Jacques Pettigrew Michel Lemire
| screenplay = Robert P. Cabeen Carl Macek
| based on =   Julie Strain Eastman Billy Idol Various Artists
| cinematography = Bruno Philip
| editing = Brigitte Brault
| studio = CinéGroupe
| distributor = Sony Pictures Home Entertainment 
| released =  
| runtime = 88 minutes
| country = Canada Germany
| language = English
| budget = $15 million
}} adult Animated animated science Heavy Metal, fantasy magazine The Melting Pot, written by Kevin Eastman, Simon Bisley and Eric Talbot. The film was made by CinéGroupe, a studio based in Montreal, Quebec.

==Plot==
In an asteroid excavation, space crewman Tyler and his colleague find a green crystal that is rumored to be the key to a fountain of immortality, however, it makes whoever touches it insane. Tyler ends up killing his mining partner, and takes over the ship, killing most of the resisting crew except for Dr. Schechter, and the pilots Lambert and Germain.    His search for the planet with the fountain leads to Eden,    a planet that is designated F.A.K.K.² (Federation-Assigned Ketogenic Killzone to the second level), but has inhabitants whose bodies carry the immortality fluid. Tyler invades Eden, and kills the Edenites, but captures some so he can extract their fluids. He also keeps Kerrie for his own purposes, but when Germain resists the idea, he is left on Eden.

Kerries sister Julie, who survived the attack, finds Germain, and they team up to follow Tyler. At a renegade space station, Julie finds Tyler at a restaurant and critically injures him, however, he ingests a vial with the immortality serum and heals.  In the ensuing gunfight, Tyler blows up the club. Julie escapes the explosion; she and Germain board a shuttle-craft that latches onto Tylers ship with a tractor beam before it jumps into hyperspace. Discovering them mid-travel, Tyler tries to shake them off, but the fight causes the hyperspace to collapse and the two ships to crash.

Julie wakes up on the desert planet called Oroboris, and meets a mysterious sage named Odin and his assistant, Zeek, a rock-like creature, both of whom are guardians to the fountain. Elsewhere, Tylers ship is totaled and most of his crew and abductees are dead. Tyler orders Dr. Schechter to extract Kerries fluids. He explores the planet and finds a race of reptilian beings, which he conquers by defeating their champion and then their leader in a death match. Julie enters the reptilian city disguised as a woman that the reptiles found for Tyler. That night, she seduces Tyler, but when she tries to kill him, Zeek captures her and takes her back to Odin. Julie infiltrates Tylers ship where she discovers Kerrie is still alive. She takes out Dr. Schechter, frees Kerrie, and escapes as the complex explodes. As a result, Tyler vows to make Julie immortal so he can "screw her and kill her every hour of every day for all eternity". With only three vials of serum, he orders his troops to storm the citadel where the immortality fountain is located.

At the citadel, Julie undergoes a ritual where she is outfitted in armor. She, Kerrie, and Germain help the fountains guardians defend against Tylers army. In the fighting, Lambert suffers an near fatal injury and while reaching for Tylers last vial of immortality serum, he knocks it loose from Tylers belt and it breaks on the ground. Tyler, enraged, kills Lambert for the blunder. Tyler then walks to the pit of immortality and is about to put the crystal into the fountains final lock but is stopped by Julie. A fight ensues where Tyler appears to have the advantage, until Odin intervenes, which allows Julie to kill Tyler. Odin reveals himself to be the last of the creatures responsible for creating the fountain, and tries to claim it as his own. However, Zeek pulls the crystal key from the pedestal, locking Odin inside the fountain chamber, and heads to outer space. As Germain and Kerrie help Julie, Zeek wraps the crystal into another asteroid. 

==Cast==
* Michael Ironside as Tyler
* Julie Strain Eastman as Julie; the on-screen character was also based on Strains likeness
* Billy Idol as Odin
* Pierre Kohn as Germain St. Germain
* Sonja Ball as Kerrie
* Brady Moffatt as Lambert Rick Jones as Zeek
* Arthur Holden as Dr. Schechter

==Reception==
Heavy Metal 2000 received negative reviews; based on 9 critics, the film currently holds a 0% rating on review aggregator website Rotten Tomatoes. 

==Video game==
The film had a video game about the events after 


Heavy Metal 2000, titled  , in which the player assumes the role of Julie as she fights to save Eden from an evil entity called "GITH". The game is set some time after the film and features cameo appearances of several characters, for example, Julies sister Kerrie, the pilot Germaine (now married to Kerrie), and resurrected Tyler.

==Sequel==
After the release of 2000, a third film has been in various stages of development since. During 2008   and into 2009,  reports circulated that David Fincher and James Cameron would executive produce, and each direct one of the eight to nine segments for a new film based on Heavy Metal. Eastman would also direct a segment, as well as animator Tim Miller, with Zack Snyder, Gore Verbinski, and Guillermo del Toro attached to direct segments. However, Paramount Pictures decided to stop funding the film by August 2009  and no distributor or production company has shown interest in the second sequel since. 

In 2011, filmmaker Robert Rodriguez announced at San Diego Comic-Con International|Comic-Con that he had purchased the film rights to Heavy Metal and planned to develop a new animated film at the new Quick Draw Studios. 

==Soundtrack==
{{Infobox album  
| Name = Heavy Metal 2000 OST
| Type = soundtrack
| Artist = Various artists
| Cover = Heavy Metal 2000 soundtrack.jpg
| Released = April 18, 2000
| Recorded = Heavy metal, alternative rock, alternative metal, industrial metal, hard rock
| Length = 73:47
| Label = Restless
| Producer = Heavy Metal film soundtracks Heavy Metal OST (1981)
| This album = Heavy Metal 2000 OST (2000)
| Next album =
| Misc = {{Extra album cover
 | Upper caption = Heavy Metal 2000
 | Type = soundtrack
 | Cover = Heavy Metal 2000 clean soundtrack.jpg
 | Lower caption = Alternative cover used for the "clean" edition of the album.
 }}
}} Bauhaus and hip hop track by Twiztid and Insane Clown Posse.

# "F.A.K.K. U" — 1:44
# "Silver Future" by Monster Magnet — 4:29
# "Missing Time" by MDFMK — 4:35
# "Immortally Insane" by Pantera — 5:11 Zilch — 4:07
# "The Dirt Ball" by Insane Clown Posse and Twiztid — 5:33
# "Störagéd" by System of a Down — 1:17
# "Rough Day" by Days of the New — 3:18
# "Psychosexy" by Sinisstar — 4:02
# "Infinity" by Queens of the Stone Age — 4:40 Machine Head — 3:38
# "Green Iron Fist" by Full Devil Jacket — 3:51
# "Hit Back" by Hate Dept. — 3:52 Puya — 5:34
# "Dystopia" by Apartment 26 — 2:56
# "Buried Alive" by Billy Idol — 5:10
# "Wishes" by Coal Chamber — 3:06 Bauhaus — 6:44

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 