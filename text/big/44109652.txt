Mahazar
{{Infobox film 
| name           = Mahassar
| image          =
| caption        =
| director       = CP Vijayakumar
| producer       =
| writer         =
| screenplay     =
| starring       =
| music          = Raveendran
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1991 Cinema Indian Malayalam Malayalam film, directed by CP Vijayakumar.  The film had musical score by Raveendran.   

==Cast==
*Sukumaran as MG Panikkar
*MG Soman as Venugopal
*KPAC Sunny as Judge
*Mala Aravindan as Shivadasan
*Master Raghu as Suresh
*Abhilasha as Shari
*Bobby Kottarakkara as Babu

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Hari Kudappanakkunnu.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadippaadinadakkunna || KS Chithra || Hari Kudappanakkunnu || 
|-
| 2 || Etho kilinaadamen karalil || K. J. Yesudas || Hari Kudappanakkunnu || 
|-
| 3 || Venchandanamo Thoomanjo || K. J. Yesudas || Hari Kudappanakkunnu || 
|}

==References==
 

==External links==
*  

 
 
 


 