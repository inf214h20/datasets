Angano... Angano... nouvelles de Madagascar
 
{{Infobox film
| name           = Angano... Angano... nouvelles de Madagascar
| image          = 
| caption        = 
| director       = Marie-Clémence Paes,  
| producer       = Laterit Productions
| writer         = 
| starring       = 
| distributor    = 
| released       = 1989
| runtime        = 63 minutes
| country        = France Madagascar
| language       = 
| budget         = 
| gross          = 
| screenplay     = Marie-Clémence, Cesar Paes
| cinematography = Cesar Paes
| editing        = Cesar Paes
| music          = Carson Rock Rangers, Jean & Marcel
}}

Angano... Angano... nouvelles de Madagascar is a 1989 documentary film.

== Synopsis ==
A journey through the tales and legends of Madagascar, between reality and imagination, tinged with humor and tenderness that opts for the oral tradition to portray Malagasy culture. This documentary was chosen among the twenty most outstanding films in the Cinéma du Réel Festival.

== Awards ==
* Festival dei Popoli 1989
* Cinéma du Réel 1989
* Vues d’Afrique 1989

== References ==
 

 
 
 
 
 
 
 


 
 