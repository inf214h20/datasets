Breaktime
 
{{Infobox film
| name = Breaktime
| director =Abbas Kiarostami
| writer = Abbas Kiarostami
| editor = Rouhollah Emami
| starring = 
| producer =
| released =Iran 1972 Persian 
| runtime = 
| distributor =
}} 1972 Iranian drama film directed by Abbas Kiarostami.

== Film details ==
When the breaktime bell rings, young Dara leaves school; he has been sent home as a punishment for breaking a window with his football. On his way home, he comes across some other boys playing a game of football, and cannot resist joining in. He ends up annoying them, and they chase him away. Dara gets lost and tries to find his way home through unknown streets, which lead him to the outskirts of the city.

==See also==
*List of Iranian films
*Cinema of Iran

==External links==
* 

 

 
 
 
 
 
 
 
 