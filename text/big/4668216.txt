Alakazam the Great
{{Infobox film
| name           = Alakazam the Great
| image          = Alakazam the Great (1961).jpg
| alt            = 
| caption        = 1961 US release lobby card
| director       = Taiji Yabushita Osamu Tezuka Daisaku Shirakawa
| producer       = Hiroshi Okawa
| writer         = Keinosuke Uekusa
| based on       =  
| narrator       = Sterling Holloway
| starring       = Kiyoshi Komiyama Noriko Shindou Hideo Kinoshita Setsuo Shinoda Peter Fernandez Frankie Avalon
| music          = Ryoichi Hattori  (Japanese Version)  Les Baxter  (American Version) 
| cinematography = Harusato Otsuka Komei Ishikawa Kenji Sugiyama
| editing        = Shintaro Miyamoto Kanjiro Igusa Salvatore Billitteri Laurette Odney
| studio         = Toei Animation
| distributor    = Toei Company (Japan) American International Pictures (USA)
| released       =  
| runtime        = 88 minutes  (Japanese Version)  80 minutes  (American Version) 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Chinese novel Journey to the West, and was one of the earliest anime films to be released in the United States. Based on the manga   by Osamu Tezuka, he was named as a director of the film by Toei Company. However, Tezuka later stated that the only time he was in the studio was to pose for publicity photos.  His involvement as a consultant for the adaptation of his manga, and in promoting the film, however, led to his interest in animation. 

== Plot ==
Alakazam is a young and brave monkey (a macaque) who has been encouraged by all the other monkeys to become their king. After attaining the throne, he becomes rude and dictatorial, and does not believe that human beings are greater than he is. Then he tricks/forces Merlin the magician to teach him magic (reluctantly on Merlins part, who warns Alakazam that the powers he acquires now will bring him much unhappiness later). 

Alakazam becomes so arrogant that he abuses his magic powers, and chooses to go up to Majutsu Land (the Heavens), to challenge King Amo. He is defeated by King Amo. For his punishment, he is sentenced to serve as the bodyguard of Prince Amat on a pilgrimage; in order to learn humility, mercy and to fight with wisdom. Ultimately, he learns his lesson and becomes a true hero.

== Characters ==
* Alakazam/Sun Wukong|Son-Goku
* DeeDee the Monkey/RinRin Chohakkai
* Shagojo
* Prince Amat/Xuanzang (fictional character)|Sanzo-hoshi
* King Amo/Gautama Buddha|Shaka-nyorai Kanzeon
* Filo Fester/Shoryu
* King Gruesome/Gyu-Mao Rasetsujo
* Herman Mcsnarles/Kinkaku
* Vermin Mcsnarles/Ginkaku

== U.S. release == American International on July 26, 1961. For the American release, a few scenes were heavily edited and rearranged and bandleader Les Baxter was hired to compose a new soundtrack. Teen idol Frankie Avalon supplied the singing voice of Alakazam (the speaking voice was done by Peter Fernandez), and Sterling Holloway provided English language|English-language narration.
Other famous voices included Jonathan Winters, Arnold Stang, Dodie Stevens, & E.G. Marshall.
===Reception===
Despite a large marketing budget and heavy promotion, the movie did not do well in America. The Los Angeles Times called it "warm, amusing and exciting... the art work is really excellent".  It was included as one of the choices in The Fifty Worst Films of All Time, and is the only animated film featured in the book.

== Home media release == Orion Home Video re-released the film in both pan-and-scan and widescreen letterbox VHS editions and on a widescreen laserdisc in 1995. As of May 27, 2014, plans for a DVD and Blu-ray release of the AIP version have yet to be announced, although the film is currently available for streaming on Netflix.

== See also ==
* Journey to the West
* List of animated feature films
* List of Osamu Tezuka anime
* List of Osamu Tezuka manga
* Osamu Tezuka
* Osamu Tezukas Star System

== References ==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 