The Devil Is a Woman (1950 film)
{{Infobox film
| name           = The Devil Is a Woman
| image          = 
| caption        = 
| director       = Tito Davison
| producer       = Victor Purcell Joseph Satinsky Gregorio Walerstein
| writer         = Edmundo Báez Tito Davison Luis Fernández Ardavín Ricardo López Méndez
| starring       = María Félix
| music          = 
| cinematography = Alex Phillips
| editing        = Rafael Ceballos
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

The Devil Is a Woman ( ) is a 1950 Mexican drama film directed by Tito Davison. It was entered into the 1951 Cannes Film Festival.     

==Cast==
* María Félix - Angela
* Víctor Junco - Adrian Villanueva
* Crox Alvarado - Esteban
* José María Linares-Rivas - Lic. Octavio Sotelo Vargas
* Perla Aguiar - Angélica
* Dalia Íñiguez - Gertrudis
* Luis Beristáin - Cura
* José Baviera - Ernesto Solar Fuentes
* Beatriz Ramos - Carmela
* Nicolás Rodríguez (actor) - Pepe Luis
* Salvador Quiroz - Amante Viejo
* Alejandro Cobo - El Cojo
* Juan Orraca - Policia
* Isabel del Puerto - Clara
* Carlos Múzquiz - Martínez, de la casa de juegos

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 