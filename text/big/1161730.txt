Jackass: The Movie
{{Infobox film
| name           = Jackass: The Movie
| image          = Jackass_poster.jpg
| caption        = Theatrical release poster
| director       = Jeff Tremaine
| producer       = Spike Jonze Johnny Knoxville Jeff Tremaine Jason "Wee Man" Acuña Preston Lacy Ehren McGhehey Brandon Dicamillo Dimitry Elyashkevich Whitey McConnaughy Sean Cliver Loomis Fall Phil Clapp Vernon Chatman based on         =   Jason "Wee Man" Acuña Preston Lacy Ehren McGhehey
| music          = Nicole Tocantins
| studio         = MTV Films Dickhouse Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $5 million   
| gross          = $79,497,930
}} reality comedy film directed by Jeff Tremaine with the tagline "Do not attempt this at home." It is a continuation of the stunts and pranks by the various characters of the MTV television series Jackass (TV series)|Jackass, which had completed its unique series run by this time. The film was produced by MTV Films and Dickhouse Productions and released by Paramount Pictures.
 Jason "Wee Raab Himself also appear but not as frequently as in the show.

Other regular Jackass personalities who made appearances include Rake Yohn, Manny Puig, Phil Margera, and April Margera. In addition, Rip Taylor, Henry Rollins, Spike Jonze, boxing star Eric Butterbean|Butterbean, Mat Hoffman, and Tony Hawk make cameo appearances. An unrated version of the film was released in 2006 and clocked in at 91 minutes long.

=="Failed ending"==
In the ending of the film, Johnny Knoxville is launched from a catapult into a lake, where Rip Taylor sits in a boat, announcing that "this is the end." This is followed with the credits being shown over outtakes from the film. The original ending for the film was supposed to be a Rube Goldberg-type contraption, with each of the cast members performing a stunt that either has something to do with what they did on the show (for example, the first stunt would have Preston as "The Human Wrecking Ball", knocking him into a Port-a-john|Port-A-Potty), or simply for a sight gag (such as Ehren being knocked over in the Port-A-Potty and landing on a bed of toilet paper rolls), ending with Johnny being launched off the catapult next to Rip Taylor.

However, as the entire contraption didnt work together the way they wanted it to, the producers of the film decided to try filming an alternate ending, which is how they came to film the "Son Of Jackass" skit. The "Son Of Jackass" skit involves dressing all the performers in old man clothing and having them run around exploding buildings and sheds, with only Steve-O surviving to proclaim "Yeah, dude." In the cast commentary, it is said that this is ironic as Steve-O is the least likely to even reach old age. Some bits of the failed ending were incorporated into the end credits montage, like Dave England dressed in a penis costume and the giant Plinko contraption.

==Cut out== beanbag projectile from a pump-action shotgun.

The first time Knoxville is shot at, it misses him making him extremely nervous. The scene was later edited out as, while the "Jackass" crew could waive civil liability, they could not waive criminal liability. Hence, should Johnny or any cast member have been killed or grievously injured as a result of a stunt, the producers of the film could be held liable on the grounds of negligent or reckless homicide or battery.  While Knoxville and other Jackass participants are clearly aware of the risks involved in their stunts, the threat of criminal liability was significant enough that the Los Angeles law firm Irell & Manella advised cutting out segments which could potentially be used as evidence in such a case. 

In addition, the final skit in the film called "Butt X-Ray" was edited to remove the insertion of the toy car into Ryan Dunns anus, the reason being that displaying the insertion might have been considered pornographic or otherwise highly objectionable by the MPAA, and could have earned the film an NC-17 rating, severely limiting its distribution.

==Japanese version==
Since some scenes of the film were shot in Tokyo, Japan, a special edited version was made and screened for Japanese audiences. Some bits were edited out for legal reasons (especially scenes showing peoples faces without their consent); however, they were placed back in for the special DVD version.

==Box office performance==
The film had a budget of $5 million  and was the number one film at the United States box office when it opened, grossing $22,763,437, revenue from 2,509 theaters, for an average of $9,073 per venue. The film fell to fourth place in its second weekend, but dropped a lower than expected 44 percent to $12,729,732, expanding to 2,530 theaters, averaging $5,032 per theater, and bringing the 10-day gross to $42,121,857.  The film went on to gross $64,255,312 in the United States alone, with the opening weekend making up 35.43 percent of its final gross. It also made $15,238,519 in other countries, bringing the worldwide gross of $79,493,831, returning its investment nearly 16 times over, and thus making the film a huge financial success.   

==Reception==
 
As of November 2014, on the review aggregate site Rotten Tomatoes, 48% of critics gave the film positive reviews based on 95 reviews with the critics consensus being, "Theres a good chance youll be laughing hysterically at one stunt, but getting grossed out by the next one in this big screen version of the controversial MTV show".  On Metacritic, the film had an average score of 42 out of 100, based on 14 reviews, indicating "mixed or average reviews." 
 Ebert & Roeper – Richard Roeper called it the "feel-sick movie of the year" and said the film is "a disgusting, repulsive, grotesque spectacle, but its also hilarious and provocative. God help me, thumbs up." Ebert gave the film a low rating, but only barely, explaining his rating comes "somewhere between a thumbs down and a sort of waving over recommendation. 
* The Austin Chronicle – Kimberly Jones gave the film 3 stars and said the film is the "feature-length rendering of jackass the MTV show, meaning no plot, no script, just wall-to-wall idiocy." Jones said "Its silly, often stomach-churning, but also awfully addictive, inspiring the same kind of vicarious adrenaline rush as Fight Club, with its I bleed, therefore I am; he-man mentality." Jones also remarked, "Consisting of a steady clip of barely minutes-long gags...this piece of outré performance art defies typical movie conventions...but that shouldnt surprise, or even disappoint, anyone lining up for a ticket." Jones wrote "the query can I have one for jackass the movie please? sort of implies you know what youre getting yourself into" and "all told, either you get it or you dont." 
*  . How far and low will these guys go? Whats the pinnacle of pointlessness?" then concluded "I dont like conceptual art." 
* Deseret Morning News – Jeff Vice gave the film 1½ stars and said the 80 minute runtime was too much. Vice said the film should have been rated NC-17 and said that many people will find the film to be "possibly the most irresponsible picture ever released by a major film studio." 
* Entertainment Weekly – Owen Gleiberman gave the film a "B" and said the film "provokes a suspense halfway between comedy and horror. Im not sure if I enjoyed myself, exactly, but I could hardly wait to see what Id be appalled by next." Gleiberman also said "In the movie version of the show that might just as well have been called Americas Funniest Frat-House Hazing Rituals, the boys engage in infantile Candid Camera grossouts...but mostly, the happy masochistic stunts just keep coming", and also remarked, "its difficult to reprimand Johnny Knoxville and his crew of merry sick pranksters when their principal pastime consists of dreaming up elaborate new ways to punish themselves." 
* Film Journal International – Ethan Alter, who admitted to having never seen an episode of the TV show, said he couldnt say he enjoyed watching it, and said "it would be easy for me to hold Jackass: The Movie up as a leading example of the decline of Western civilization." Alter said he was disturbed by "the films, and by extension the audiences, cavalier attitude towards pain." Alter went on to say the film "deliberately defies any and all cinematic conventions", "theres no story or characters to analyze", and said "simply put, theres no movie to review here, just a series of blackout scenes youre either going to find supremely funny or incredibly idiotic." Ethan Alter also said the film "may be the most experimental feature ever released by a major Hollywood studio" and also that it "appears to be hailing the birth of a new reality genre: Call it Americas Most Sadistic Home-Videos." 
* LA Weekly – film critic Paul Malcolm listed Jackass: The Movie as one of the 10 best films of 2002 and also called it the most underrated film of 2002.  Moe first Curly on the head" and called it "one of the funniest films Ive seen all year." 
*  , although the movie does leave you marveling at these guys superhuman capacity to withstand pain (and their even stranger eagerness to suffer it)." 
* New York Post – film critic Lou Lumenick said "  plotless collection of moronic stunts is by far the worst movie of the year." 
* The New York Times – A.O. Scott said the film "is essentially an extended episode of the popular Jackass (TV series)|Jackass MTV series" and that "some of the undertakings, amateurishly recorded on video, are like demented science experiments." Scott said "Jackass the Movie is like a documentary version of Fight Club, shorn of social insight, intellectual pretension and cinematic interest" and also remarked, "Occasionally, there is a flicker of Candid Camera-style conceptual inventiveness, especially in the bits filmed in Japan." 
* The Village Voice – Ed Halter said "their feature debut plays like a longer episode of the show" and said "its funny, as the old saying goes, because its true." Halter wrote "the structure is ruthlessly efficient: no plot, no characters, no sets, and no downtime—just one sight-gag right after another."   
* Scott Foundas of   when reviewing The Real Cancun in April 2003.   
* In a film critic roundup of 2002 films in The Village Voice, film critic Armond White said "Best Documentary: Jackass, far and away."  Real World franchise, but its roots lie elsewhere", saying "their self-destructive brand of docu-comedy emerged as a bizarrely elaborate version of a skateboard-video mainstay: slam sections..." 
* Jennie Punter of The Globe and Mail said the film "belongs in the too-hot-for-TV direct-to-video/DVD category". 
 Razzie Award for Most Flatulent Teen-Targeted Movie.

==Soundtrack==
  American Recordings. The soundtrack features songs that were featured in the movie, and various audio clips from the movie. 

==Sequels==
Jackass: The Movie was filmed with a modest budget of approximately $5 million, but earned more than $22 million during its opening weekend, effectively managing to secure the top spot at the box office for its debut. It eventually grossed more than $64 million in North America alone.
 Jackass 2.5 was compiled from outtakes shot during the making of the second film and released direct-to-DVD on December 26, 2007.
 3D starting Jackass 3.5 was compiled from outtakes shot during the making of the third film. The film was released in weekly installments on Joost from April 1 through June 13, 2011.  The entire film was then released direct-to-DVD on June 14, 2011.

==References==

 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 