Night Watch (1928 film)
{{Infobox film
| name           = Night Watch
| image          =
| caption        =
| director       = Alexander Korda
| producer       = Ned Marin Michael Morton (play) Lajos Bíró Rufus McCosh Dwinelle Benthall
| narrator       =
| starring       = Billie Dove Paul Lukas Donald Reed Nicholas Soussanin
| music          = Emil Bierman Mortimer Wilson
| editing        = George McGuire
| cinematography = Karl Struss
| studio         = First National Pictures
| distributor    = First National Pictures
| released       =   
| runtime        = 70 minutes
| country        = United States Silent English English intertitles
}}
 American drama Michael Morton. music but no dialogue. Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles, pp. 49-51. Virgin Books, 1990.   

A print is preserved at Cineteca Italiana, Milan. 

==Cast==
* Billie Dove - Yvonne Corlaix 
* Paul Lukas - Captain Corlaix 
* Donald Reed - Lieutenant DArtelle 
* Nicholas Soussanin - Officer Brambourg 
* Nicholas Bela - Leduc 
* George Periolat - Fargasson 
* William H. Tooker - Mobrayne 
* Gusztáv Pártos - Dagorne 
* Anita Garvin - Ann

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 