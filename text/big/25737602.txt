Vincent, François, Paul and the Others
{{Infobox Film
| name           = Vincent, François, Paul and the Others
| image          = 
| image_size     = 
| caption        = 
| director       = Claude Sautet
| producer       = 
| writer         = Claude Néron (novel)  Jean-Loup Dabadie
| narrator       = 
| starring       = Yves Montand Michel Piccoli Serge Reggiani Gérard Depardieu
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1974
| runtime        = 113 minutes
| country        = France
| language       = French
| budget         = 
| website        = 
}} French film directed by Claude Sautet based on the novel La grande Marrade by Claude Néron.

== Plot ==
Three friends face mid-life crises. Paul is a writer whos blocked. François has lost his ideals and practices medicine for the money; his wife grows distant, even hostile. The charming Vincent, everyones favorite, faces bankruptcy, his mistress leaves him, and his wife, from whom hes separated, wants a divorce.

==Cast==
*Yves Montand - Vincent
*Michel Piccoli - François
*Serge Reggiani - Paul
*Gérard Depardieu - Jean Lavallee
*Stéphane Audran - Catherine, Vincents Wife
*Marie Dubois - Lucie, François Wife
*Umberto Orsini - Jacques
*Ludmila Mikaël - Marie
*Myriam Boyer - Laurence

== References ==
 

== External links ==
*  

 

 
 
 
 
 

 