Klassfesten
{{Infobox film
| name           = Klassfesten
| image          = Klassfesten.jpg
| caption        = Swedish dvd-cover
| director       = Måns Herngren Hannes Holm
| producer       = Patrick Ryborn
| writer         = Måns Herngren Hannes Holm
| screenplay     = 
| story          = 
| based on       =  
| starring       = Björn Kjellman Cecilia Frode Henrik Hjelt Inday Ba The Ark
| cinematography = 
| editing        = Fredrik Morheden
| studio         = S/S Fladen Film
| distributor    = Buena Vista International (Sweden)
| released       =   
| runtime        = 103 minutes
| country        = Sweden
| language       = Swedish
| budget         = SEK 18,500,000
| gross          = 
| followed by    = 
}} Swedish comedy/drama film written and directed by Måns Herngren and Hannes Holm. The film stars Björn Kjellman, Cecilia Frode, Inday Ba, Henrik Hjelt, Lisa Lindgren and Ulf Friberg. The film was released February 27, 2002 in Sweden.

== Plot ==
20 years after Magnus Edkvist (Björn Kjellman) graduated from the ninth grade in Hagsätra, he gets an invitation to a class reunion. He declines the invitation, because he doesnt want to relive some of the most embarrassing moments of his life. Magnus rather stays home with his wife Lollo (Cecilia Frode) and his daughter. But when Magnus starts to think about his teenage crush, Hillevi (Inday Ba), and whether she will go or not. He decides to go to the reunion, in hope that Hillevi will show up.

== Cast ==
*Björn Kjellman as Magnus Edkvist
*Inday Ba as Hillevi
*Cecilia Frode as Lollo Edkvist, Magnus wife
*Lisa Lindgren as Jeanette
*Ulf Friberg as Tommy
*Henrik Hjelt as Ove
*Jimmy Lindström as Lill-Micke
*Johan Ehn as Jonas
*Jessica Forsberg as Pia
*Ingrid Luterkort as The teacher
*Urban Bergsten as Leffe lort
*Jan Åström as Micke P
*Frida Öhman as Alva, Magnus and Lollos daughter 
*Oscar Taxén as Young Magnus
*Sacha Baptiste as Young Hillevi
*Anders Timell as Fabbe

== Music == The Ark. The Arks singer Ola Salo has written the two songs. He wrote "Calleth You, Cometh I" together with Peter Kvint.

== Accolades == Best Supporting Actress.

== External links ==
*  
*  

 
 
 
 