Le Scandale (1934 film)
{{Infobox film
| name           = Le Scandale
| image          = 
| caption        = 
| director       = Marcel LHerbier
| producer       = 
| writer         = Henry Bataille Henri Duvernois
| starring       = Gaby Morlay Henri Rollan
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 3 February 1934 (France); 12 February 1935 (Portugal)
| runtime        = 106 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Romance romance Drama drama film from 1934, directed by Marcel LHerbier, written by Henry Bataille, starring Gaby Morlay.   It is also known as "O Escândalo" (Portugal).

== Cast ==
* Gaby Morlay: Charlotte Férioul
* Henri Rollan: Maurice Férioul
* Jean Galland: Count Artanezzo
* Mady Berry: Misses Férioul
* Pierre Larquey: Parizot
* Jean Marais:  the lift operator
* Milly Mathis: the café keeper
* Mircha: Little Riquet
* Gaby Triquet: Suzanne
* André Nicolle: Jeannetier
* Paulette Burguet: Margaridou

== References ==
 

== External links ==
*  
*   at the Films de France

 

 
 
 
 
 
 
 
 
 


 