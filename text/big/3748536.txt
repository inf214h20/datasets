Rang De Basanti
 
 
{{Infobox film
| name = Rang De Basanti
| image = RDB_poster.jpg
| caption = Promotional poster for the film
| director = Rakeysh Omprakash Mehra
| producer = Rakeysh Omprakash Mehra Ronnie Screwvala Aamir Khan
| story = Kamlesh Pandey  Prasoon Joshi  (dialogue) 
| screenplay = Renzil DSilva Rakeysh Omprakash Mehra
| starring = Aamir Khan Siddharth Narayan Sharman Joshi Soha Ali Khan Waheeda Rehman R. Madhavan Kunal Kapoor Atul Kulkarni Alice Patten
| music = A. R. Rahman
| cinematography = Binod Pradhan
| editing = P. S. Bharathi
| distributor = UTV Motion Pictures Aamir Khan Productions
| released =  
| runtime = 157 minutes
| country = India Hindi Punjabi Punjabi English English
| budget =   
| gross =   worldwide 
}}
Rang De Basanti ( ;  ) is a 2006 Indian drama film written and directed by Rakeysh Omprakash Mehra. It features an ensemble cast comprising Aamir Khan, Siddharth Narayan, Soha Ali Khan, Kunal Kapoor, R. Madhavan|Madhavan, Sharman Joshi, Atul Kulkarni and British actress Alice Patten in the lead roles. Made on a budget of  , it was shot in and around New Delhi. Upon release, the film broke all opening box office records in India. It was the highest-grossing film in its opening weekend in India and had the highest opening day collections for a Bollywood film. The film was well received and praised for strong screenplay and dialogues.

The story is about a British documentary filmmaker who is determined to make a film on Indian freedom fighters based on diary entries by her grandfather, a former officer of the British Indian Army. Upon arriving in India, she asks a group of five young men to act in her film.
 Indian Defence Animal Welfare Board due to parts that depicted the use of Mikoyan-Gurevich MiG-21|MiG-21 fighter aircraft and a banned Indian horse race.
 Republic Day Best Foreign BAFTA Awards. Academy Awards in the Best Foreign Language Film category, though it did not ultimately yield a nomination for either award. A. R. Rahmans soundtrack, which earned positive reviews, had two of its tracks considered for the Academy Award nomination. The film was well received by critics and audiences for its production values and had a noticeable influence on Indian society. In India, Rang De Basanti did well at many of the Bollywood awards ceremonies, including a win for Best Movie at the Filmfare Awards. The film was declared "Superhit" by Box Office India.

==Plot==
A young, struggling British filmmaker Sue McKinley ( , Bhagat Singh, Shivaram Rajguru, Ashfaqulla Khan, and Ram Prasad Bismil. McKinley, in his diary, states that he had met two type of people in his life: the first one... who died without uttering a sound and the second kind ... who died with lots of anguish.. crying over their deaths... McKinley reveals that it was then that he met with the third kind....

Having decided to make a self-financed documentary film about these revolutionaries, Sue travels to India, with the help of her friend, Sonia (Soha Ali Khan), from the Institute for International Studies at the University of Delhi. After a few unsuccessful auditions in search of the actors, Sue finally casts Sonias friends, four young men – Daljit "DJ" (Aamir Khan), Karan Singhania (Siddharth Narayan), Aslam Khan (Kunal Kapoor) and Sukhi Ram (Sharman Joshi) — to portray the revolutionaries.

Though they arent very enthusiastic at the idea of acting in a film about the independence movement, Sue eventually manages to convince them. Laxman Pandey (Atul Kulkarni), a political party activist, joins the cast later, despite initially being unpopular due to his anti-Western ideology, due to which he is often and odds with the other four, and anti-Muslim beliefs and contempt for Aslam Khan. In the process of filming, the idealism of Indias revolutionary heroes seeps into the protagonists. They gradually begin to realize that their own lives are quite similar to the characters they portray in Sues film and that the state of affairs that once plagued the revolutionaries continues to torment their generation.

Meanwhile, Ajay Singh Rathod (R. Madhavan), a flight lieutenant in the Indian Air Force who is Sonias fiancé, is killed when his jet, a Mikoyan MiG-21|MiG-21, crashes. The government proclaims that the crash was caused by pilot error and closes the investigation. Knowing that Rathod was an ace pilot, Sonia and her friends do not accept the official explanation. Instead, they claim that he sacrificed his life to save hundreds of other lives that would have been lost had he ejected from the aircraft and left it to crash into a populous city. They investigate and learn that the crash was due to a corrupt defence minister (Mohan Agashe), who had signed a contract exchanging cheap and illegal MiG-21 aircraft spare parts for a personal favour. To their surprise, they learn that the key person who was responsible for organizing the deal was Karans father, Rajnath Singhania (Anupam Kher).

Angered by the situation, the group and their supporters decide to protest peacefully at India Gate, a war memorial in New Delhi. Police forcefully break up their protest using batons; in the process, Rathods mother (Waheeda Rehman) is severely hurt and she slips into a coma. DJ, Karan, Aslam, Sukhi, and Laxman decide that they must emulate the early freedom fighters and resort to violence to achieve justice. As a result, they kill the defence minister to avenge Rathods death, while Karan murders his father for his corrupt actions. The minister is reported to have been killed by terrorists and is hailed as a martyr by the media. To bring forth their intentions behind the killings, the five of them attempt to reach the public through a radio station. They forcibly take over the All India Radio station premises after having evacuated its employees. Karan goes on air and reveals the truth about the defence minister and his wrongdoings. While still on the air, the police proclaim that they are dangerous terrorists who have forcefully taken over the AIR, and therefore they are to be shot on sight. The first to be shot is Daljit, who tries to get out of cover and establish that they are not terrorists, however the shot is not a fatal one. Sukhi, unable to control his anger at this gets out and is instantly shot to death. He dies with the last smile still etched on his face. As they are trying to lock the terrace doors, Aslam and Pandey are then killed due to a grenade explosion and the once archenemies die holding hands and smiling, as they have visions of Ram Prasad Bismil and Ashfaqullah.

Daljit manages to crawl to the Recording Room, where Karan is still on air. When Karan understands that he has been shot, they speak amongst themselves for the last time, regarding the others, about Sue and about Daljits love for her. It is there that they are shot to death, but not without the sound of their hearty laughter still hanging in the air like an echo of long dead music. It is then revealed that McKinley described the third kind of people he came across as being the ones who embraced death as a friend and an equal, with a heartfelt laughter.  After their death, the public reacts with outrage and expresses urge to bring Indian Politics to justice, following the motives of all the boys.

The film comes to an end with Sue describing the impact of the boys on her life. As she and Sonia watch from the rooftop that Ajay proposed to her on, they have a vision of the boys running in the fields, singing happily and victoriously throwing their shirts in the air, acting as if they are celebrating life itself, as if the ebb of their once-there vitality still reverberates in the places where they once used to go, and a wave of melancholy comes over the two surviving women.

In an afterlife-like state, the boys watch as a father tells his son (a young Bhagat Singh) about gardening. They watch over him with smiling faces, then depart as friends for eternity.

==Cast==
* Aamir Khan as Daljit DJ / Chandrashekhar Azad Siddharth as Karan R. Singhania / Bhagat Singh
* Atul Kulkarni as Laxman Pandey / Ramprasad Bismil
* Kunal Kapoor as Aslam / Ashfaqullah Khan
* Sharman Joshi as Sukhi / Rajguru
* Alice Patten as Sue McKinley
* Soha Ali Khan as Sonia / Durga Bhabhi / Durgawati Devi
* Steven Mackintosh as Mr. McKinley
* R. Madhavan as Flight Lt. Ajay Rathod
* Waheeda Rehman as Ajays mother Mrs. Rathod
* Anupam Kher as Rajnath Singhania, Karans father
* Kiron Kher as Mitro, DJs mother
* Om Puri as Amanullah Khan, Aslams father
* Lekh Tandon as DJs grandfather
* Cyrus Sahukar as Rahul (Radio Jockey)
* Mohan Agashe as Defence Minister Shastri

==Production==

===Development=== Independence Day speeches and watching patriotic films such as Mother India.  Although Mehra denies that the film is autobiographical, he confessed that the character sketches were loosely inspired by himself and his friends.

Mehra approached Angad Paul after having been impressed with his production work on British films Lock, Stock and Two Smoking Barrels and Snatch (film)|Snatch. Paul, who was keen to work in India, liked Mehras story and agreed to produce the film,    bringing with him David Reid and Adam Bohling as executive producers. Despite having no prior knowledge of Hindi cinema, Reid and Bohlings belief in the script was strong enough that they each were willing to work at half their normal rate.    While it was originally suggested that two language versions of the film would be made concurrently, in English (as Paint it Yellow) and Hindi,   the plans for an English version were dropped during development. Mehra believed that English-language version felt alien and that "one can tell a film in just one language".  After the English version was dropped, the writer Kamlesh Pandey was brought on board to pen the first draft of Rang De Basanti in Hindi,  marking the start of his screenwriting career.  Thereafter Mehra and co-writer Rensil DSilva took over the script, working on it for about two years.   Prasoon Joshi, the films lyricist, worked on the dialogue, marking his first venture into screenwriting. 
 masala fare",    and that such films "find favour with the audience owing to their elaborate sets and period costumes".  The budget was reported as Indian rupee|Rs.&nbsp;250&nbsp;million (approximately United States dollar|US$5.5&nbsp;million),  and, despite going a little over the initially planned budget, Mehra did not have any serious disagreements with UTV. 

===Casting===
Aamir Khan agreed to act in Rang De Basanti immediately after reading Mehras script.  Mehra described his character as a simple man with a strong sense of integrity and dignity.  Khan, who would turn 40 during the shoot, lost about   with a strict diet and exercise regime to more convincingly depict a man in his late twenties.  Atul Kulkarni and Kunal Kapoor were publicly attached to the film by the time it was officially announced;  Kapoor had been the assistant director to Mehra during the filming of Aks and was already familiar with the material Mehra had been developing.  Mehra gave Kulkarni biographies of Ram Prasad Bismil as preparation, including Bismils autobiography.  Early rumors indicated that actors Arjun Rampal and Arjan Bajwa would be amongst the male leads,   but these roles ultimately were filled by Sharman Joshi, Siddharth Narayan and R. Madhavan.
 Telugu film Tamil cinema actor, took the smaller role of a fighter aircraft pilot because he was convinced of the films potential and wanted to be a part of it.  Om Puri appears in a two-scene cameo as Aslams staunch Muslim father.

Soha Ali Khan and Alice Patten immediately became Mehras clear favorites for each of their roles during casting,  which led to Patten flying to Mumbai for a screen test with the entire cast. She was informed that she had won the documentary filmmaker role after she returned home to the United Kingdom.  Soha, portraying the pilots fiancée, was filming Rituparna Ghoshs Antarmahal and David Dhawans comedy Shaadi No. 1 concurrently with her work in Rang De Basanti. In particular, the demands of her emotional scenes in Antarmahal often left her exhausted, thus requiring "a lot of personal overhauling" to ensure that her performance in Rang De Basanti was unaffected.  During filming, reports indicated that co-stars Siddharth and Soha had become romantically involved with each other.  Alongside the two lead actresses, Lakh Tandon played the role of Aamirs Grand Father and Kirron Kher played the mother of Khans character.

===Filming===
The film, which was shot in New Delhi, Mumbai, Rajasthan and Punjab (India)|Punjab,    was officially launched at a hotel on 1 February 2005.    When shooting began, Mehra made an announcement to his crew saying that they would enjoy their holiday only in July. 
 Modern School Delhi Tourism Golden Temple. Punjabi character and it took him some time to get the right dialect and diction.    While speaking about his experience of visiting the temple for the first time, he said:

 Its one of the most peaceful places Ive been to. As you enter the place theres a certain serenity that surrounds you. I really enjoyed being there. The first shot we took was of our feet entering the water just as you pass the doorway of the temple. The water was cold but it was great!  

Once the locations were finalized, the team of Lovleen Bains and   (1996) and Dil Chahta Hai (2001), the latter of which featured Aamir Khan, and he was referred to Mehra by Khan due to their previous association. Since the films plot focused on men in their late twenties, Bhasin designed their look accordingly. Although he was responsible for Khans rebellious look, Sharman Joshis (who played Sukhi) lovable persona or Madhavans dignified appearance, Bhasin credited Bains for her major contributions to the film.  Khans hair was styled by Avan Contractor, who came up with soft curls falling over Khans forehead. This new look, which took Contractor one hour to come up with, surprised the audience at the films launch. 

In post-production, the visual effects were handled by Tata Elxsis Visual Computing Labs. The military aircraft they created was so realistic that the Indian Air Force called to check the producers permission of using an actual MiG-21. 

==Music==
{{Infobox album 
|  Name        = Rang De Basanti: The Original Motion Picture Soundtrack
|  Type        = soundtrack
|  Artist      = A. R. Rahman
|  Cover       = RDB_soundtrack.jpg
|  Background  = gainsboro
|  Released    = 8 December 2005
|  Length      = 44:00
|  Label       = Sony BMG
}}
 
 pop singer Nelly Furtado, he said that she was to originally have featured on the soundtrack, although this was ultimately prevented from happening due to a change in producers and other factors.  Aamir Khan, with his knowledge of Hindi and Urdu,  worked with Rahman and Joshi for the soundtrack.  In addition, Mehra and Rahman chose him to sing for one of the songs. 

Joshi, one of the lyricists, was impressed with the director, Rakeysh Mehra, who was ready to adjust to his style of writing as well as his creativity.  Confessing that the films soundtrack was his favorite out of all his previous works, Joshi felt that it "was a wonderful experience getting to know the mindset of todays youth and to pen down their feelings".  Speaking about one of his songs, "Luka Chuppi", in which veteran Lata Mangeshkar sang with Rahman,  Joshi said that it was developed while discussing with Rahman the scene about a mother losing her son. Joshi wrote the lyrics about the mother and son playing hide-and-seek with the sad reality of the son being hidden forever.  He confessed to have been in tears while Mangeshkar was singing the song.    The soundtrack won the Filmfare Award for Best Music Director,  and had two of its tracks, Khalbali and Luka Chuppi, considered for an Academy Award for Best Original Song nomination. 
 bhangra harvest dance are incorporated alongside more contemporary, global styles such as hard rock and hip hop to depict the cosmopolitan lifestyle of the youngsters in the film. 

==Release== Indian Defence Indian censor board to urge the filmmakers to seek clearance from the ministry.  Accordingly, Khan and Mehra screened the film for the then Defence Minister Pranab Mukherjee along with other top officials from the armed forces.    One Air Force official reportedly said that it was "not a review, but a preview".  After the special screening, the defence ministry did not insist on any cuts, but on their recommendation more names were added to the slide that dedicates the film to deceased MiG pilots.  After this clearance, the Animal Welfare Board raised objections on the use of animals in the film. Although the filmmakers had obtained a No Objection Certificate from the board officials, Maneka Gandhi, a well-known animal rights activist and member of the welfare board, found flaws in this certificate.  Subsequently, this certificate was revoked and with only a few days left for the world premiere, Mehra personally requested Gandhi to reconsider her objection. After another viewing, the board cleared their objection stating that the use of animals in the film was natural and justified. However, after they recommended the deletion of a 20-second scene that depicted a banned horse race conducted by the Nihang Sikhs, the filmmakers deleted this scene.   Mrs. Kavita Gadgil whose son, late Flight Lieutenant Abhijeet Gadgil was killed when his MiG-21 fighter crashed, objected to the films release because she believed that the film was loosely based on her sons life and the producers should have shown her the film. In response, Kamlesh Pandey, one of the writers of the film, said that the film was not inspired by Abhijeet Gadgil. 
 Hyderabad and Pune with an intention of interacting with the students.  After hiring international experts for the films publicity,  the marketing expenditure for the film grew to 40 percent of the total production budget of  . This expenditure was unprecedented in Bollywood because usually the Indian filmmakers spend only about five percent of their production budget on marketing.  Out of the   marketing campaign, a fifth of it came from the producers while the rest was obtained through brand tie-ups and partnering.   

Since Rahmans last musical success,   nomination. 

Before its theatrical release, the producers tied up with several top brands to help in the marketing the film. An alliance was formed with The Coca-Cola Company by releasing special edition bottles to commemorate the films release, a first of its kind in Bollywood. Besides this, the music CDs and cassettes were co-branded with the cola company along with the launch of the sale of collectibles from the film.  Provogue, a well-known clothing retail chain in India, launched a special limited edition clothing merchandise targeting the youth of India.  Besides these, the producers collaborated with LG Group, Berger Paints, Bharti Airtel and Hindustan Petroleum.  The producers tied up with several media partners such as MSN India, Channel V and Radio Mirchi to further enhance their marketing efforts.    A video game launched by an Indian mobile content company was based on an adaptation of the films plot. 

In India, The Hindu reported that with audiences from the metropolitan cities turning out in large numbers, Rang De Basanti was notching up record collections in its opening week.    Accordingly, 55 percent of the films revenues came from multiplexes in these cities.  While the opening week box-office collections from Mumbai, the home of Bollywood, were reported to be over  , theaters in New Delhi earned about half of Mumbais revenue. Throughout the country, the cumulative collections in the first week was about  . Overseas collections from the United States, United Kingdom and Australia were collectively put at over   for the same week.  Released in about 60 theaters in the United States, the film grossed   in its opening weekend and earned   within 10 weeks.    With   alone coming from the Indian territory, the film earned more than   worldwide.  Currently, the film holds the record for the List of highest-grossing Bollywood films#Highest-grossing films by month, unadjusted for inflation|highest-grossing film to be released in January.

Within a week of the films theatrical release, pirated copies of the film priced at   were seized at an Indian airport.  A report carried out by The Times of India highlighted piracy on the internet where movies like Rang De Basanti could be downloaded freely.  To counter this, UTV Motion Pictures launched anti-piracy raids on local retailers in New York City, Houston and Dallas, which consist of a fairly large Indian diaspora. This was to ensure that there would be no pirated DVD sales before its intended DVD release on 15 March 2006.  The DVD release sold more than 70,000 copies over six months, and as a result the film was the highest selling title at the time of its release.

Rang De Basanti was released on Blu-ray (plus steelbook edition) in May 2014. 

==Reception==

===Critical reception===
Critics gave the film an overwhelmingly positive response, in particular the ensemble casts performance and credibility. Although The Indian Express spoke positively of the cinematography and the films story, it noted that "the message that the film carries with it tends to get diluted towards the climax.    Praising the films cast for their performance and the cinematography of Binod Pradhan, Taran Ardash wrote that the film would be successful with the urban audiences.  The Hindustan Times summarized the film as being a "well-scripted, skilfully crafted   thought-provoking entertainer".     Saisuresh Sivaswamy of Rediff.com wrote that films like Rang De Basanti can easily get into "preachiness", but believed Mehra got his message across while avoiding this, also appreciating the music, cinematography, dialogues and art direction.  The Hindu credited Kamlesh Pandey for writing a story that would have been a difficult film to make, but it added by saying that the transformation of the youngsters into heroes seemed poetic. Although the screenplay, direction and the cast were also well-appreciated, the reviewer felt that Rahmans soundtrack lacked pace. 
 five star Bloomberg website wrote positively about "the raw energy of a young cast and A. R. Rahmans splendidly rousing soundtrack". 

Sight & Sound magazine conducts a poll every ten years of the worlds finest film directors to find out the Ten Greatest Films of All Time. This poll has been going since 1992, and has become the most recognised  poll of its kind in the world. In 2012  Cyrus Frisch voted for "Rang De Basanti". Frisch commented: "Corruption became the subject of fierce debate in India after the major success of this film among youngsters."

===2007 Academy Awards Best Foreign Film submission=== East Coast to promote the film. Besides his efforts, producer Screwvala planned to use resources and expertise from his partners in 20th Century Fox and Walt Disney Pictures for organizing its publicity efforts.  When the nominations in the foreign film category did not feature this film, it sparked off debates on whether the film should have been Indias entry for the Oscars. In one such debate on a television channel that involved Screwvala, the selection committee was questioned about its knowledge of the requisite artistic criteria for such award ceremonies. While one outcome of the debate was on how Omkara would have been a better choice, the other discussed the Western world|West-centric sensibilities of the Academy members. However, results from a simultaneously conducted SMS poll indicated that 62 percent felt that the film was the right choice for the Oscars. 

===Awards and nominations=== Best Film, Best Director, Best Music Critics Best Best Editing Best Cinematography RD Burman Best Movie, Best Supporting Best Music Director among other technical awards.   Apart from these, the film won eight awards apiece at the 2006 Global Indian Film Awards  and 2007 Star Screen Awards,  and six at the 2007 Zee Cine Awards. 
 Best Film not in English language at the 2007 British Academy of Film and Television Arts awards.  

==Social influence==
 
Rang De Basanti had a noticeable impact on Indian society. A study of bloggers behavioral patterns during the first month of the films release revealed a significant increase in public ire towards government and politicians for constantly being mired in corruption and bureaucracy and their inefficiency in providing basic amenities. Intense political discussions spurred on by the films influence were observed in these patterns.  While commenting on this, writer DSilva said that the film "has struck a chord somewhere".  Besides instigating political thought and discussions, it evoked social awakening for many. Some discussions rallied on how citizens should support and contribute to non-governmental organizations and exercising simple citizen duties of paying taxes and voting, while the others contemplated on how to become more responsible towards the country.  Unlike other Indian films with jingoistic overtones, many young Indians could relate well to the characters of this film. 
 Jessica Lall socially backward classes in educational institutions. Young doctors and engineers joined hands in peaceful rallies in major cities across India.  Though the film was not released in the neighbouring Pakistan, it evoked similar reactions there. Inspired by the film, Pakistans national newspaper, Jang, launched a television channel that was to focus on citizens issues and support public awakening.  Reacting to these strong social reactions, actor Kunal Kapoor thought that the film was just a catalyst that presented "patriotism in a package that the youngsters understood and empathised with". 
 Indian expatriate whose life changes after watching this film. 

==Further reading==
 
* Dilip, Meghna (2008), " ", Master of Arts Thesis, University of Massachusetts, Amherst, February 2008.

==References==
 

==External links==
*  
*  
*   
*   at Rediff.com

 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 