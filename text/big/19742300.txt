Aerial Gunner
 
{{Infobox Film
| name           = Aerial Gunner
| image          = Aerial Gunner poster.jpg
| caption        = Theatrical release poster
| director       = William H. Pine
| producer       = William H. Pine William C. Thomas
| writer         = Jack F. Dailey Maxwell Shane
| starring       = Chester Morris Richard Arlen Jimmy Lydon
| music          = Daniele Amfitheatrof
| cinematography = Fred Jackman Jr.
| editing        = William H. Ziegler
| studio         = Paramount Pictures

| distributor    = Paramount Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Aerial Gunner is a 1943 American World War II film directed by William H. Pine and starring Chester Morris, Richard Arlen and Jimmy Lydon. 

==Plot== shooting gallery, that his criminal father has died. Foxy blames all policemen, feeling they harassed him all his life and were responsible for his death. Both men enlist in the United States Army Air Forces where Foxy becomes the instructor at an aerial gunnery school. He makes life miserable for Jon, now a "Flying Sergeant" student, trying to force the former policeman to resign. 

Despite Foxys hostility, Jon is able to pass the course. He later befriends a young Texas boy, Sandy (Jimmy Lydon), whose father was an airman killed at Hickam Field during the attack on Pearl Harbor. Sandy invites Jon and Foxy to his familys ranch, where both men fall for Sandys sister Peggy (Amelita Ward).

After graduation, Jon is commissioned as a Lieutenant and is assigned as a pilot of a light bomber, with many of his classmates now his crew. A belligerent Foxy serves as his gunner and is not accepted as a team player by the others. During a bombing mission against the Japanese, however, he makes the ultimate sacrifice in trying to protect the other crew members when the bomber is shot down behind enemy lines.

==Cast==
* Chester Morris as T/Sgt. "Foxy" Pattis
* Richard Arlen as T/Sgt, later Lt., Jonathan "Jon" Davis
* Jimmy Lydon as Pvt. Sanford "Sandy" Lunt
* Amelita Ward as Peggy Lunt (credited as Lita Ward)
* Dick Purcell as Pvt. Lancelot "Gadget" Blaine
* Keith Richards as Sgt. Henry "Jonesy" Jones
* William Billy Benedict as Pvt. Jackson "Sleepy" Laswell 
* Olive Blakeney as Mrs. Sanford Lunt
* Robert Mitchum as S/Sgt Benson (uncredited)  

==Production==
 
Principal photography for Aerial Gunner by the Paramount Pictures Pine-Thomas Productions unit took place over a period from October 21–mid-November 1942.  Location work at the air gunner training school at Harlingen Air Force Base, Texas. Many of the real AAF trainees there appear in the film as extras. 
 Lockheed B-34 B film feature, although a number of ground scenes that were later added, which had to rely on studio rear projection work, looked amateurish. Hardwick and Schnepf 1989, p. 51. 

==Reception==
Aerial Gunner had its world premiere at Harlingen Air Force Base, where much of the film is set. Orriss 1984, p. 73.  Other premieres at major cities followed. 
 The New York Daily News describing the film as the "most ambitious picture" that Paramount producers William Pine and William Thomas had turned out.  Bosley Crowther completely disagreed in his review for The New York Times; he dismissed the effort as nothing more than "... heroics for the bumpkins in one-syllable clichés. There are a few interesting sequences in it of training at an aerial gunnery school and some routine, but always pretty pictures of planes climbing up and setting down. But never do they rise above the ceiling prescribed by a normal B-film. This is strictly a picture for the shooting-gallery trade." 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
*  
*  
*  
*  


 
 
 
 
 
 
 
 
 