Chilamboli
{{Infobox film
| name = Chilamboli
| image =
| caption =
| director = GK Ramu
| producer = Kalyanakrishna Iyer
| writer = Chinthamani Thikkurissi Sukumaran Nair (dialogues)
| screenplay = Thikkurissi Sukumaran Nair
| starring = Prem Nazir Sukumari Adoor Bhasi Thikkurissi Sukumaran Nair
| music = V. Dakshinamoorthy
| cinematography = GK Ramu
| editing = P Venkatachalam
| studio = Vrindavan Pictures
| distributor = Vrindavan Pictures
| released =  
| country = India Malayalam
}}
 1963 Cinema Indian Malayalam Malayalam film, directed by GK Ramu and produced by Kalyanakrishna Iyer. The film stars Prem Nazir, Sukumari, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
  
*Prem Nazir 
*Sukumari 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*T. S. Muthaiah 
*Adoor Pankajam  Ambika 
*Baby Vilasini Baby Vinodini 
*Kumari Santha Ragini 
*S. P. Pillai 
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Abhayadev and Vilwamangalam. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Deva Ninniluracheedunna || P. Leela || Abhayadev || 
|- 
| 2 || Doorennu Doorennu || P. Leela || Abhayadev || 
|- 
| 3 || Kalaadevathe Saraswathy || P. Leela, Kamukara || Abhayadev || 
|- 
| 4 || Kannane Kanden Sakhi || P. Leela || Abhayadev || 
|- 
| 5 || Kasthoorithilakam || Kamukara || Vilwamangalam || 
|- 
| 6 || Kettiyakaikondu || P Susheela || Abhayadev || 
|- 
| 7 || Maadhava Madhukai || P. Leela || Abhayadev || 
|- 
| 8 || Maayaamayanude leela || Kamukara || Abhayadev || 
|- 
| 9 || Odivaava || Kamukara || Abhayadev || 
|- 
| 10 || Paahi Mukunda || P Susheela, Kamukara || Abhayadev || 
|- 
| 11 || Poovinu Manamilla || P. Leela, Kamukara || Abhayadev || 
|- 
| 12 || Priyamanasa Nee || P. Leela || Abhayadev || 
|}

==References==
 

==External links==
*  

 
 
 


 