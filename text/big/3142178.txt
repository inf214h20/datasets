The Blue Kite
{{Infobox film
| name           = The Blue Kite
| film name = {{Film name| jianti         = 蓝风筝
 | fanti          = 藍風箏
 | pinyin         = Lán fēngzheng}}
| image          = Blue kite poster.jpg
| alt            = 
| caption        = Release poster
| director       = Tian Zhuangzhuang
| producer       = Yongping Chen Guiping Luo
| writer         = Mao Xiao
| starring       = Lu Liping Pu Quanxin Chen Xiaoman Li Xuejian Guo Baochang
| music          = Yoshihide Otomo Hou Yong|
| editing        = Qian Lengleng
| studio         = Beijing Film Studio Longwick Film Kino International
| released       =  
| runtime        = 140 minutes
| country        = China Hong Kong
| language       = Mandarin
}} Zhang Yimous To Live Chen Kaiges Farewell My Fifth Generation filmmaking, and in particular reveals the impact the various political movements, including Anti-Rightist Movement and Cultural Revolution, had upon directors who grew up in the 1950s and 1960s. 

The film won the Grand Prix at the Tokyo International Film Festival, and Best Film at the Hawaii International Film Festival, both in 1993.

==Synopsis==
The story is told from the perspective of a young boy (铁头, Tietou, literally meaning iron head) growing up in the 1950s and 1960s in Beijing. Three episodes – Hundred Flowers Campaign, the Great Leap Forward and the Cultural Revolution – show the family members evolving, e.g. from the real father, the "loving patriarch," to the protective but unemotional stepfather.

===Father===
The first episode, entitled "Father," begins with a wedding between Lin Shaolong and Chen Shujuan in the early 1950s, shortly after the Communist victory. The wedding draws the whole neighborhood, a happy moment that will soon serve as a stark contrast with the years to come. The house and courtyard are shown in a warm bright light as children play happily together. 

The couple soon give birth to a son, Tietou, meaning "iron head." In these early years Tietous father creates for him a blue  . We soon find out that Shujuans older brother was going blind, and will soon be forced to leave the party. The girl he loved also resigned over her refusal to meet with senior party members in clandestine affairs. Without given cause, simply that she was flaming "counter revolutionary thoughts", she was taken away to prison. Shujuans youngest brother, an art school student, was called to task for his "critique" of the party and shown standing before his fellow students as they rally against him.

In one of the films most chilling scenes, Lin Shaolongs workplace has convened a meeting on the issue of who they will have to report to the Communist Party as a "rightist" in order to meet Maos quota. The father quickly leaves for the bathroom. When he returns, all eyes are on him, it is clear who his colleagues have selected. Realizing his terrible mistake of leaving, the father briefly mistreats his son. Tietou, just a small boy, is still bitter when his father is sent to a work camp. Shujuans youngest brother was also sent to a reeducation camp.  The chapter ends when Tietous mother receives a letter; his father has been killed by a falling tree.

===Uncle===
The second episode of the film is entitled "uncle" and deals with Tietous mothers courtship by Li, her husbands former colleague, and subsequent remarriage to "Uncle" Li. Li felt haunted by his role in sending his friend to the work camp which resulted in Lins death.  Li spent every moment helping out the mother and child, and every penny in easing their distress in the rapidly declining society.  

The episode begins with a group picture during a wedding, with the men wearing blue work uniforms. Much of the episodes palette seems to follow this opening trope as both the courtyard and house are shown in a cold blue. Uncle Li cares for the boys material needs and desires but it soon grows clear that his health is failing. Li wants to become a party member, and is working feverishly at his job. Soon, malnutrition during the Great Leap Forward takes its toll and Uncle Li dies due to his poor health.

===Stepfather===
No wedding ceremony, no feelings shown at all. This marriage is just to save the mother and son from poverty, and to give them protection. They move in with the stepfather (Lao Wu). The house is so big that everyone has his own room, no family life at all. 

Meanwhile, the Cultural Revolution is about to break forth, led by adolescents seeking to "rebel" against those who seek to rein them in: their parents, teachers, and even party members the central government deemed dangerous. The stepfather, a prominent party member about to be disgraced, worries about saving his wife and stepson and does what he can to provide a safe life for them before it is too late. He offers them money and to divorce Shujuan. Perhaps unable to see another husband taken from her, Shujuan returns to her husbands home as hes being lifted out of his home by the rebelling Red Guards. The last scenes are of Tietous mother being dragged away by Red Guards, who also beat Tietou. At the end, the boy is lying on the ground, bloodied. In a voice-over, he tells of his stepfathers death from heart failure; his mother is sent to the work camps, but his own fate is left unknown. The camera pans out from his bruised body as he lies there looking up to see a broken blue kite hanging in the tree.

==Themes==
{{Quote box   
| quote = "I finished shooting The Blue Kite in 1992. But while I was involved in post-production, several official organizations involved with Chinas film industry screened the film. They decided that it had a problem concerning its political leanings, and prevented its completion. The fact that it can appear today seems like a miracle... The stories in the film are real, and they are related with total sincerity. What worries me is that it is precisely a fear of reality and sincerity that has led to the ban on such stories being told."  
| source = — Director Tian Zhuangzhuang    
| width = 40%   
| align = right   
}} 
The film shows a series of patriarchal figures in Tietous life. Each of the fathers somehow offends the party, and each fails to provide a happy life for his family. Many symbols are used to show that the party is usurping the father, the mother and the family itself. The more the party takes control, the less emotions are shown and the more depressed the characters are.  The party is also shown reaching out for those who seek to undermine it and no one can escape: not the student, not the ordinary librarian, and not even the soldier who fought for those very ideals.

==Banned in mainland China==
Because of its content, it was banned in mainland China by the government.

==Awards==
* Hawaii International Film Festival, 1993
** Best Feature Film
* Tokyo International Film Festival, 1993
** Grand Prix
** Best Actress Award – Lu Liping
* Independent Spirit Awards, 1995
** Best Foreign Film nomination

==See also==
* List of banned films
* Hibiscus Town — another Cultural Revolution drama, made several years prior to The Blue Kite.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 