When Night Falls (1985 film)
 
{{Infobox film
| name           = When Night Falls
| image          = 
| caption        = 
| director       = Eitan Green
| producer       = 
| writer         = Eitan Green
| starring       = Assi Dayan
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}
 Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Assi Dayan as Giora
* Yosef Millo as Bernard
* Orna Porat as Ruth
* Dani Roth as Dudi
* Haya Pik-Pardo as Sheri
* Lasha Rosenberg as Karen

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 