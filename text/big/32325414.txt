The First Time (2012 American film)
 
{{Infobox film
|name=The First Time
|image=The_First_Time_Movie_Poster_2012.jpg
|caption=Theatrical release poster
|alt=
|director=Jon Kasdan
|producer=Martin Shafer Liz Glotzer
|writer=Jon Kasdan
|starring=Dylan OBrien Britt Robertson Craig Roberts Joshua Malina James Frecheville Christine Taylor Victoria Justice LaMarcus Tinker
|music=Alec Puro
|cinematography=Rhet W. Bear
|editing=Hugh Ross
|studio=Jerimaca Films
|distributor=Samuel Goldwyn Films Destination Films
|released= 
|runtime=95 mins
|country=United States
|language=English
|budget=$2 million
|gross=$92,654 
}}
The First Time is a 2012 teen romantic comedy film written and directed by Jon Kasdan and stars Britt Robertson, Dylan OBrien, James Frecheville and Victoria Justice.

Dave Hodgman (Dylan OBrien) is a high school senior who spends most of his time pining away over Jane Harmon (Victoria Justice), a girl he cant have. Aubrey Miller (Britt Robertson), a junior at a different high school, has an older boyfriend Ronny (James Frecheville) who doesnt quite understand her or seem to care. A casual conversation between Dave and Aubrey sparks an instant connection, and, over the course of a weekend, things turn magical, romantic, complicated, and funny as Aubrey and Dave discover what its like to fall in love for the first time.

==Plot==
Dave is in a back alley rehearsing a confession of love for Jane, a girl he has a crush on, when hes surprised by Aubrey. They talk, and he ends up trying out the speech for her to get her opinion. Shes not impressed. He asks her if shed like to dance with him, and she refuses. Hes hurt, and she explains that she finds public displays of affection distasteful. She changes her mind and they start to dance, but its announced that the police are breaking up the party, and they leave, eventually ending up at her house. She invites him inside, noting that she has a boyfriend, and takes him to her bedroom. She gets some wine, and during their conversation she asks him if hes ever had sex. He admits he hasnt, but when he asks her she refuses to answer. She gets him to lay down with her on the floor and they fall asleep. They are awakened by Aubreys mother knocking on the door, and Dave escapes through the window. He spills wine on the rug.

Dave meets with his friends Simon and Big Corporation at a diner, and he tells them about Aubrey. They suggest that claiming to have a boyfriend could be a strategic lie and suggest texting her. Dave discovers he doesnt have her number. They help him find her home number, and Dave calls, catching Aubrey as her parents are berating her about the wine on the rug. She tells Dave shes going to the movies with her boyfriend Ronny, and Dave decides to go too.

Jane and her friends are also at the theatre. Aubrey appears and introduces Dave to Ronny. Aubrey persuades Ronny not to go to the Almovador movie as planned and to see the same movie as Dave and Jane. Aubrey goes out during the film, and Dave follows her. In the lobby she apologizes for being strange on phone and having him jump off the roof. Dave gets her cell phone number and invites her and Ronny to Janes friends house afterwards. 

At the friends house, Aubrey and Jane talk about Dave. Jane has high praise for Dave, but clearly takes him for granted while complaining about all the awful guys shes been with. Aubrey seems to be about to suggest she consider Dave but stops and pretends to have forgotten what she was about to say. 

Ronny brags to Dave that he and Aubrey are planning to have sex that night in his van. Dave finds Aubrey and tells her not to have sex with Ronny, that her first time shouldnt be with a guy like him. Aubrey denounces the romantic notion of the first time being so important, and tells him its none of his business--hes in love with Jane. He says "You dig me." Things take a flirtatious turn but suddenly Ronny appears. Ronny tells Aubrey they need to go. Dave intervenes and Ronny purposely calls Dave by the wrong name. Dave tells Ronny his name is Dick. Ronny pushes Dave and Simon and Big Corporation intervene. Aubrey finally leaves with Ronny.

Dave ends up in a bedroom with Jane, where she half flirts with him before starting to complain about her latest sexual misadventures. Jane notes that something weird is going on with Dave. While he tries to answer, she physically pulls away assuming hes going to kiss her. He realizes hed rather be with Aubrey and leaves. On his way home, he gets a text from Aubrey asking him to pick her up. They drive around, she tells him that shes just dumped Ronny, and they gradually reveal and discover their feelings for each other. At her house they kiss passionately.

They talk on the phone the next day and meet at a park. Then Aubrey finds out her parents are going out for the evening. She invites Dave over. Dave struggles with himself over bringing a condom. Finally he goes without it. At her place, they begin to kiss again, and hesitate about whether to have sex or not. Eventually they decide to do it. Cut to afterwards. Both of them are upset. It hasnt gone well, and they dont know how to talk about it. Everything they say seems to make it worse, and when they part theyve both agreed it was all a mistake and perhaps should not see each other.

Both want to talk with the other. Aubrey keeps looking at her phone hoping Dave calls. Dave repeatedly picks up his phone, presumably to call Aubrey, but ends up calling Simon and Big Corporation. At the diner, he lays out the problem. He shares his feeling that the idea of sex was better than the experience. Simon tells him its okay; hes going off to university; she goes to a different school; no one he knows will hear her side. Big Corporation, however, doesnt agree. He reminds him that they spend every weekend looking for someone special and it never happens, but this time it did. He says Dave and Aubrey hit a speed bump and because it was their first time they dont know anything different. He tells him to be a man and give it another try.

The next day, Aubrey tells her parents that she found a great guy and messed it up by pushing him away. Her parents try to make her feel better. While she is leaving her house, Dave is waiting for her in the driveway. He declares his feelings and wants to try again. She tells him shes going to be late for school and leads him to believe shes not interested, but then she asks him to drive her to school. In the car, she expresses her desire to be together. She also wants to work on the sex. He is all for it and happy she wants to be together. He drops her off at school and they say goodbye awkwardly. She manages to tear herself away but ends up running back. Breaking her no PDA rule, she throws herself into his arms and they kiss.

==Cast==
*Dylan OBrien as Dave Hodgman
*Britt Robertson as Aubrey Miller
*Craig Roberts as Simon Daldry
*James Frecheville as Ronny
*Victoria Justice as Jane Harmon
*LaMarcus Tinker as Big Corporation
*Joshua Malina as Aubreys Dad
*Christine Taylor as Aubreys Mom
*Maggie Elizabeth Jones as Stella Hodgman
*Halston Sage as Brianna
*Adam Sevani as Wurtzheimer guy
*Molly C. Quinn as Erica #1
*Christine Quynh Nguyen as Erika #2
*Matthew Fahey as Brendan Meltzer

==Music==
{{tracklist
| collapsed=no
| headline=The First Time Unofficial Soundtrack
| writing_credits=yes
| extra_column=Artist
| total_length=
| title1=Silly Boy
| writer1=Soeren Christiansen, Steffen Westmark, Allan Villadsen, Per Joergensen
| extra1=The Blue Van
| length1=3:19
| title2=Teenage Daydream
| writer2=Robin Feher
| extra2=The Nights
| length2=3:57
| title3=Were #1 
| writer3=Andrew Creighton 
| extra3=The World Record
| length3=3:54
| title4=Mama
| writer4=Trey Johnson Sorta
| length4= 
| title5=Lonely Soul
| writer5=Robin Feher
| extra5=The Nights
| length5=3:56
| title6=Oh My Love
| writer6=Daniel Varjo, Ludvig Rylander, Lisa Milberg, Maria Eriksson, Martin Hansson, Per Nystrom, Ullik Jonusson, Dante Holgersson
| extra6=The Concretes
| length6=2:54
| title7=Out of Touch
| writer7=Elizabeth Borden
| extra7=Liz Borden and The Axes
| length7=2:32
| title8=If It Be Your Will
| writer8=Leonard Cohen
| extra8=Leonard Cohen
| length8=3:42
| title9=Trouble
| writer9=Matthew Beighley, Thomas King, Jacqueline Santillan
| extra9=Wait. Think. Fast.
| length9=4:12
| title10=The End
| writer10=Bethany Cosentino
| extra10=Best Coast
| length10=2:42
| title11=Head Spin
| writer11=Dane Schmidt
| extra11=Jamestown Story
| length11=3:13
| title12=Diamond Eyes
| writer12=Michael Haggins
| extra12=Michael Haggins
| length12=18:02
| title13=I Cannot Love You
| writer13=Michael Lerner
| extra13=Telekinesis
| length13=2:01
| title14=Cant Stop Thinking
| writer14=Tom Wolfe
| extra14=Buva
| length14=4:11
| title15=Come On
| writer15=Chad Marshman The Wind
| length15=4:39
| title16=In Your Mind
| writer16=Matt Weinberger, Abe Seiferth, Jared Elioseff, John Graham Davis, David Burnett
| extra16=Phonograph
| length16=4:30
| title17=Come and Go (feat. KU)
| writer17=Danica Rozelle, Jacques Slade, Lamar Van Sciver, Frank Greenfield
| extra17=Danica Rozelle
| length17=4:06
| title18=Anne With an E
| writer18=Kip Berman, Peggy Wang, Kurt Feldman, Alex Naidus
| extra18=The Pains of Being Pure at Heart
| length18=4:06
| title19=Wait For Me
| writer19=Daniel Blue, Josiah Sherman
| extra19=Motopony
| length19=4:57
| title20=Vampires Kiss
| writer20=John Gold
| extra20=John Gold
| length20=3:41
| title21=Coming Home
| writer21=Pete Kilpatrick, Zach Jones
| extra21=Pete Kilpatrick Band
| length21=3:38
| title22=Till The Morning
| writer22=Afie Jurvanen
| extra22=Bahamas
| length22=1:58
| title23=Girls Like You
| writer23=Thomas Powers, Aaron Short, Alisa Xayalith
| extra23=The Naked And Famous
| length23=6:04
| title24=Sweet Louise
| writer24=Barbara Gruska, Ethan Gruska
| extra24=The Belle Brigade
| length24=3:06
}}

==Reception==
The First Time received mixed reviews and has a score of 47% on Rotten Tomatoes. Among the negative reviews, Mark Olsen of the Los Angeles Times wrote, "There is much to like here, a sense of nuance and nonjudgmental emotional openness, yet Kasdans teenage miniaturism never quite blooms.", whilst Joshua Rothkopf of Time Out New York said "Writer-director Jonathan Kasdan cant even bother to satisfy the buildup with a real moment of consummation (welcome to the fade to black) or believable postcoital complications." 

However, New York Times critic Neil Genzlinger wrote a positive review, stating that "The list of temptations a filmmaker can fall into when making a movie about high school students and virginity is quite long, but Jonathan Kasdan avoids most of them in his sweet, low-key comedy “The First Time.” No gratuitous raunchiness here and only a few tired caricatures in a genre usually jammed with them." 

==Release==
The First Time had a very limited release into theaters in October 2012, and grossed just $22,836. It was released on DVD and digital download on March 12, 2013. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 