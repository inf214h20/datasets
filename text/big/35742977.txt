Varuthapadatha Valibar Sangam
 
 
{{Infobox film
| name           = Varuthapadatha Valibar Sangam
| image          = Varuthapadatha Valibar Sangam Poster.jpg
| caption        = Promotional poster
| director       = Ponram
| producer       = P. Madhan
| writer         = Ponram M. Rajesh  (dialogues)  Soori
| cinematography = Balasubramaniem
| editing        = Vivek Harshan
| studio         = Escape Artists Motion Pictures
| distributor    = Escape Artists Motion Pictures
| released       =  
| runtime        = 158 minutes
| country        = India
| language       = Tamil
| music          = D. Imman
| budget         =     
| gross         =   
}}
Varuthapadatha Valibar Sangam ( ) is a 2013 Tamil romantic comedy film directed by Ponram, a former assistant of directors M. Rajesh and S. A. Chandrasekhar.     It stars Sivakarthikeyan, Sathyaraj and Sri Divya.     The film was produced by Escape Artists Motion Pictures and has music by D. Imman. Rajesh wrote the dialogues for the film.  It was filmed in padalur, Theni, Tiruchi and Chennai.  The film released on 6 September 2013 and received  mixed reviews from critics and became a box office success.  The film was remade in Kannada as Adyaksha with Sharan playing the lead role and in Telugu as Current Theega with Manoj Manchu.  

==Plot==
The film begins with police arriving at Sivanandis (Sathyaraj) house questioning him about killing his daughter Lathapandi (Sri Divya) because she ran away with the guy she loved. Bosepandi (Sivakarthikeyan) and Kodi (Soori) are two friends who are the leaders of a group called Varuthapadadha Valibar Sangam. One day, Bosepandi falls in love with Kalyani (Bindu Madhavi) who is a teacher at a school. Bosepandi writes a love letter for her but wants someone to go give it to her so thats when he finds Lathapandi. Lathapandi gives the letter to her teacher and tricks Bosepandi into believing many things.

Sivanandi fixes a marriage for Lathapandi but she is not willing to marry because she is very young and she wants to study further. Despite her attempts, nothing stops her marriage. Her marriage was posted on a billboard and Bosepandi and Kodi decide to stop it. They go to the police station and threaten the police that they will go to the commissioner. The police talks to Sivanandi and makes him stop the marriage. One day, Lathapandi delivers the news to Bosepandi that Kalyani is getting married. He decides to move on in life, so he arranges for a Dindukal programme to happen in his area. He sees Lathapandi in a sari and immediately falls in love with her. The same night, the police reveals to Sivanandi that the person who stopped Lathapandis marriage was Bosepandi.

A day later he tells her about his love for her but she ignores him and says no. Bosepandi walks away listening to a sad song when a gang comes and bashes him. He later finds out that it was Sivanandis gang who hit him. So Bosepandi and Kodi decide to steal what Sivanandi considers his soul and that is his gun. Bosepandi and Kodi steal the gun and run away and cause a lot of trouble. Bosepandi then tells Lathapandi that he will return the gun if she comes to their friends marriage and so he returns the gun without anyone knowing. Lathapandi in return, goes to that marriage and takes lots of photos with Bosepandi. Her mother warns her that this is not correct and she should stop it. Sivanandis cow falls into a well when he goes out of town so Bosepandi helps to get it out. That night he stays with Lathapandi in her house and they see Sivanandi sleepwalking. After that, they wake up in the morning and they see Sivanandi walking again. This time he is awake but Bosepandi thinks hes sleepwalking again and so he tells Sivanandi about liking his daughter. Once he finds out that he is indeed awake, he runs out of that house.

Sivanandi then makes Lathapandi promise that she will only marry the guy who Sivanandi tells her to marry and so she does promise her dad. They fix a marriage for Lathapandi but on the night before her marriage, she decides to run away with Bosepandi. When running away at night, they see Sivanandi and he tells them to run away and gives them some money so that they never come back. Sivanandi wants Bosepandi and Lathapandi to run away because he doesnt want Lathapandi to marry the guy he has chosen for her and he cant stop her marriage because he has too much respect in his village. He watches them get married and then he walks home with blood on his shirt (the blood is of a goat that he killed) and lies to everyone that he killed his daughter because of his reputation. Bosepandi and Lathapandi return though and ruin Sivanandis plans and Bosepandi stated that he returned because his father offered him more money. The film ends with Sivanandi and Bosepandi laughing happily.

==Cast==
* Sivakarthikeyan as Bosepandi
* Sathyaraj as Sivanandi
* Sri Divya as Lathapandi Soori as Kodi
* Shalu Shanmu as Kodis lover
* Rajendran as Coolmayi
* Sriranjini as Dhanalakshmi Lollu Sabha Swaminathan as Lawyer Veerasamy
* Elango Kumaravel as constable
* Yaar Kannan as Bosepandis father
* Kadhal Dhandapani
* Vinodhini as Inspector
* Bindu Madhavi as Kalyani (Special Appearance)

==Soundtrack==
{{Infobox album
| Name        = Varuthapadatha Valibar Sangam
| Longtype    = to Varuthapadatha Valibar Sangam
| Cover       = Varuthapadatha Valibar Sangam Audio Cover.jpg
| Caption     = Front Cover
| Type        = Soundtrack
| Artist      = D. Imman
| Producer    = D. Imman Feature film soundtrack Tamil
| Label       = Sony Music India
| Last album  = Desingu Raja (2013)
| This album  = Varuthapadatha Valibar Sangam (2013)
| Next album  = Ennathan Pesuvatho (2013)
}}

The soundtrack was composed by D. Imman, which is the second time he has contributed the score for a film starring Sivakarthikeyan after Manam Kothi Paravai. Sivakarthikeyan had sung a song in this film.  The audio was released on 14 July,  Actor Dhanush released the audio CD and director A. R. Murugadoss received it. Behindwoods wrote:"Made for onscreen enjoyment"  while Indiaglitz wrote:"Overall one for the mass and fun factor". 

Tracklist
{{track listing
| extra_column = Singer(s)
| lyrics =
| title1     = Oodha Color Ribbon
| extra1     = Hariharasudan

| title2     = Indha Ponnungale
| extra2     = Jayamoorthy

| title3     = Paarkathe Paarkathe
| extra3     = Vijay Yesudas, Pooja Vaidyanath

| title4     = Varuthapadatha Valibar Sangam
| extra4     = Sivakarthikeyan, Anthony dasan

| title5     = Yennada Yennada
| extra5     = Shreya Ghoshal, Sooraj Santhosh, Shweta Suresh

| title6     = Indha Ponnungale (Dubstep mix)
| extra6     = Jayamoorthy, Tha Prophecy length1 = length2 = length3 = length4 = length5 = length6 = total_length = 25:48}}

==Release== Central Board . The film was released in 340+ screens worldwide, the widest release for Sivakarthikeyan till then. 

==Reception==

Baradwaj Rangan wrote, "The film has enough silliness to qualify as mild amusement, especially in the scenes with Bosepandi and his friend Kodi (Soori) — but these gags would work just as well as a compilation clip on YouTube. The plotting is too loose to warrant a two-hour-and-forty-minute movie, with sentimental detours and meandering subplots". 

Behindwoods Review Board rated 2.75 out of 5 and wrote, "Charmingly mischievous Siva Karthikeyan, comical Soori and the majestic Sathyaraj make VVS a jolly good entertainer." 

Praising the work of the actors in the film, Indiaglitz says, "The films biggest strength is Sathyaraj, and the climax alone is enough to show his strength. The man who has shifted gears towards character roles is doing a good job out of it. Siva again, it is a wonder to see this guy in yet another healthy fun movie." 

Rediff wrote, "Varuthapadatha Valibar Sangam is boring and gets repetitive after a while". 

Sandesh of Oneindia says, " Varutha Padatha Valibar Sangam is a full-ledged comedy and family entertainer. Actor Sivakarthikeyan, Sathyaraj and comedian Parota Soori are the main highlight of the movie."

According to Sify, "Ponram through VVS follows the comedy template set by his guru Rajesh and blends them with his hero Sivakarthikeyans image. Add D Immans peppy melodies with a rural touch and you get a mass comedy entertainer". 

"IBNLive has given 3 out of 5 "you are likely to enjoy it more".  IBTimes review verdict is "Good Entertainer, Worth a Watch". 

The New Indian Express wrote, "A promising work by a debutant, the film though not the best of comedies, makes for a fairly pleasant watch". 

==Box office==
Varuthapadatha Valibar Sangam had the biggest opening in Sivakarthikeyans career at the time of its release;  it earned approximately   4.55&nbsp;crore net on the first day of its release.  According to Sify, the film, which released in 343 screens in Tamil Nadu, took   14.25 Cr in its first three days and had collected   31.5 Cr after 17 days at the Tamil Nadu box office. 

It was termed as a "Super hit" by Behindwoods. It has completed 100 days successfully in many theatres. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 