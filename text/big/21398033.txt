Seagulls Die in the Harbour
 
{{Infobox film
| name           = Seagulls Die in the Harbour
| image          = Seagulls Die in the Harbour.jpg
| caption        = Film poster
| director       = Rik Kuypers Ivo Michiels Roland Verhavert
| producer       = Bruno De Winter
| writer         = Rik Kuypers Ivo Michiels Roland Verhavert
| narrator       = 
| starring       = Tine Balder
| music          = Jack Sels and Max Damasse
| cinematography = Johan Blansjaar
| editing        = Raymonde Beaudoux
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Belgium
| language       = Dutch
| budget         = 
}}

Seagulls Die in the Harbour ( ) is a 1955 Belgian drama film directed by Rik Kuypers, Ivo Michiels and Roland Verhavert, for which Jack Sels wrote the soundtrack. It was entered into the 1956 Cannes Film Festival.     

==Cast==
* Tine Balder - the boatmans wife 
* Tone Brulin - the pimp
* Alice De Graef
* Jenny Deheyder
* Piet Frison - the boatman
* Désiré Kaesen
* Robert Kaesen - (as Bob Kaesen)
* Gisèle Peeters - the orphan girl (as Gigi)
* Eric Peter Marcel Philippe
* Julien Schoenaerts - the stranger
* Paul SJongers
* Panchita Van de Perre
* Dora van der Groen - the prostitute
* Albert Van der Sanden
* Miriam Verbeeck
* Geneviève Wayenbergh

==References==
 

==External links==
* 

 
 
 
 
 
 
 