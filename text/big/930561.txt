Remo Williams: The Adventure Begins
{{Infobox film
| name           = Remo Williams: The Adventure Begins
| image          = Remowilliamsposter.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Guy Hamilton
| producer       = Larry Spiegel Judy Goldstein
| writer         =   Richard Sapir
| narrator       =
| starring = {{Plainlist|
* Fred Ward
* Joel Grey
* Wilford Brimley
* J. A. Preston
* George Coe
* Charles Cioffi
* Kate Mulgrew
}}
| music          = Craig Safan
| cinematography = Andrew Laszlo
| editing        = Mark Melnick
| studio          = Dick Clark Productions MGM (2003, DVD)
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $12,421,181 (United&nbsp;States)
}} thriller film directed by Guy Hamilton. The film featured Fred Ward, Joel Grey, Wilford Brimley and Kate Mulgrew.
 The Destroyer pulp paperback series. The movie was the only adaptation featuring the character Remo Williams, and fared poorly in theaters. It received mixed reviews from critics, although it did earn Joel Grey a Golden Globe nomination. The film and a Remo Williams television pilot both credited Dick Clark as executive producer. The film was supposed to be the first of a series based on The Destroyer series of novels. 

A significant setpiece within the film takes place at the Statue of Liberty, which was surrounded by scaffolding for its restoration during this period.

The movie was also nominated for the Academy Award for Best Makeup at the 58th Academy Awards but lost to Mask (film)|Mask.

==Plot==
Sam Makin ( ).

Though Remos training is extremely rushed by Chiuns standards, Remo learns such skills as dodging bullets and running on water. Chiun teaches Remo the Korean martial art named "Sinanju (martial art)|Sinanju".  Remos instruction is interrupted when he is sent by CURE to investigate a corrupt weapons procurement program within the US Army.

==Cast==
 
* Fred Ward as Remo Williams/(Samuel Edward Makin)
* Joel Grey as Chiun
* Wilford Brimley as Harold Smith
* J.A. Preston as Conn MacCleary
* George Coe as General Scott Watson
* Charles Cioffi as George Grove
* Kate Mulgrew as Major Rayner Fleming 
* Michael Pataki as Jim Wilson
* Reginald VelJohnson as Ambulance Driver
* Jon Polito as Zack
* Gene LeBell as Red
* Sebastian Ligarde as Pvt. Johnson Tom McBride as "Jim" in the television soap opera
* Suzanne Snyder as "Nurse" in the television soap opera William Hickey as Coney Island Barker
* Patrick Kilpatrick as Stone
 

==Production==
Fred Ward claimed in the news magazines  that he performed most of the stunts himself including the scene on the giant ferris wheel shot on Denos Wonder Wheel located at Denos Wonder Wheel Amusement Park at Coney Island, in Brooklyn, New York. Screenwriter Christopher Wood claimed he wrote a climax with more action that was discarded for budgetary reasons. 

For the Statue of Liberty scenes, a replica of the Statues torso, head and arm was built in Mexico. The shots of the replica were intertwined with footage shot at the real Statue of Liberty.

==Soundtrack== instrumental score Styx member What If.

==Reception==
The movie received mixed responses from critics.   

==DVD==
Remo Williams: The Adventure Begins was released to DVD by MGM Home Video on July 15th, 2003.

==Notes==
 

== External links ==
 
 
*  
*  

 

 
 
 
 
 
 
 