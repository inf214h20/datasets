Duct Tape Forever
 
{{Infobox film
| name           =Duct Tape Forever
| image          =Duct Tape Forever poster.jpg
| image_size     =240px
| caption        =
| director       =Eric Till
| producer       = Steve Smith  Graham Greene Jeff Lumby Jerry Schaefer 
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = April 12, 2002
| runtime        = 90 minutes
| country        = Canada
| language       = English
| budget         =
}} Steve Smith, Red Green.

==Plot==

When a developers limousine gets stuck in a sink hole on Possum Lodges property and several attempts to recover it send it into the lake, the developer takes the matter to court.  The town and the presiding judge, who have been looking for decades for a way to shut down the Lodge, fine the lodge $10,000 and give them thirty days to pay, and in default the Lodge reverts to the town.  Reds nephew Harold Green (Patrick McKenna) pleads with the judge for a change to the time limit and she agrees - she makes it ten days instead.

The Lodge members try to brainstorm a way out of the mess, but once again Harold is the only one with a viable idea - 3M is running a duct tape sculpture contest in Minnesota, with a third prize of Canadian dollar|$10,000 (which the members feel they have a shot at).  The members beg, borrow and steal (literally) enough duct tape to construct a goose, and Red, Harold and Dalton Humphrey (Bob Bainborough) set off for the long ride to the contest.

However, sinister forces are at work.  The developer has convinced the towns sheriff (Darren Frost) to stop the trio by any means necessary.  The sheriff is accompanied by his beautiful deputy (Melissa DiMarco) who, against all logic, is smitten with Harold, although Harold is currently smitten with Daltons indifferent daughter.

The sheriff attempts a number of dirty tricks in order to waylay Red.  At one point they flatten the Possum Vans tires, only to find out that Dalton has siphoned all the gas out of their police car, and that the nearest working gas pump is 20 miles away.  At another point, the sheriff digs a hole in the road with a backhoe, but find their police car on the opposite side from the van.  In attempting to use the backhoe to move the police car to the other side of the hole, the sheriff instead drops the car into the hole, and the Possum Van drives right over it.

Eventually the developer takes matters into his own hands and kidnaps Harold.  Red sends Dalton back for reinforcements, as he plans to get away with both Harold and the goose.  While Harold is suffering the company of the developer, the developer reveals that his father was also a lazy Possum Lodge member, and that his mothers frustration over this fact drove him to succeed at any cost.  He plans on buying Possum Lodge to convert it into a womens club called "Possum Landing".  

At noon the next day, Red and the developer meet with Red backed by his lodge members, and the developer backed by a gang of thugs.  Just as the exchange is made, Red makes a getaway with Harold as the lodge members steal the cars of the thugs.
Just as Red feels he has made his getaway, he finds that the developer is chasing him with the only vehicle left unstolen – the bus that brought his lodge members to the show down.  Nevertheless, Red and Harold manage to make it to the contest just before they award third prize, winning it when the goose (pulled behind the Possum Van on a small trailer) detaches and flies over the contest to land majestically with all of the other entries.

After the contest the members of the lodge gather in the meeting room to celebrate with a large cake. Harold has invited the developer to the meeting, which the man takes as an attempt to humiliate him, before realizing Harold has invited his elderly mother as well and the two reconcile. While the candles on the cake are lit, the sheriffs deputy entices Harold outside where they share a passionate kiss before the candles (which are revealed to be dynamite) explode. The film ends with a dazed Harold rejoining the group as Red asks for the duct tape to repair the damage.

==Cast== Steve Smith  ...  Red Green  
*  Patrick McKenna  ...  Harold Green  
*  Bob Bainborough  ...  Dalton Humphrey  
*  Wayne Robson  ...  Mike Hamar  
*  Jeff Lumby  ...  Winston Rothschild III  
*  Jerry Schaefer  ...  Ed Frid  
*  Richard Fitzpatrick  ...  Robert Stiles Graham Greene  ...  Edgar K.B. Montrose
*  Lawrence Dane ... Prosecutor

==External links==
* 

 

 
 
 
 
 