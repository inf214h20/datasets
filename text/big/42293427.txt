I Am a Ukrainian
 
"I Am a Ukrainian"  is an Internet viral video, first posted on YouTube in 2014 featuring a young Ukrainian woman supporting the protestors in the 2014 Ukrainian revolution. It was directed by documentary maker Ben Moses. By late March that year the video had been viewed about 8 million times.

==Background==
The woman in the video was initially not named  in order to keep her safe,  but was eventually identified as Yulia Marushevska, a Kyiv Ph.D. student of Ukrainian literature at Taras Shevchenko National University.  Marushevska and British photographer Graham Mitchell shot the video after the death of five people, three of whom died of gunshot wounds, on January 22.   Marushevska felt she needed to do more for the EuroMaidan, and was frustrated with what she perceived to be the foreigners’ ignorance about why the protests were happening.  She wanted to inform the viewers that the Ukrainians want to change their government due to concerns over alleged unchecked corruption within it.   They ended up shooting a 2-minute, 4 second long video  where she speaks in English. 

==Popularity==
The video was uploaded to YouTube on 10 February 2014.  By 19 February it was reported to have about 3.5 million views.  By 21 February it had about 5.2 million views,  by 22 February it had about 6 million views,  and by 27 February it passed 7 million views.  At 14 March the video had been viewed over 7.8 million times;  dropping from a peak of 800,000 to 53,000 views per day. This can be seen as evidence that the video had passed the peak of its view rate, and will now begin to decrease in significance. As of 15 May 2014, the video has 8.1 million views, and as of 23 October 2014, the video has 8.25 million views.

Its popularity has been attributed to it being an anonymous work which allows viewers to identify with the presenter.  Its professional production was also credited; it was directed by American documentary film maker Ben Moses   and released on his YouTube channel, "Whisper Roar".  Moses was shooting a different documentary in Ukraine and Yulia was working as his translator; together they came up with the idea for the "I Am a Ukrainian" video.  

The video has received a mostly positive reception, with the majority of the tens of thousands of comments in support.    A 21 February count on YouTube gave the video about 70,000 "likes" and 4,000 "dislikes".  A minority of voices, primarily those opposed to the revolution, argued that it is too one-sided.   It has also been criticized for its professional production value, invoking a comparison to the controversial Kony 2012 viral video, which misled viewers into thinking it was a purely amateur production.  

BBC News has described it as having by far the greatest impact of any video from the 2014 Ukrainian revolution.  Moses is now working on a feature-length documentary about Yulias and Ukraines progress in the year following her viral video.  Moses launched a Kickstarter campaign on 17 June to crowd-source funding for the film. 

==Notes==
{{reflist|refs=
   

   

   

  . Retrieved on March 31, 2014. 

   

   

 André Crous,  , Prague Post,  (19 February 2014). 

   

 Jim Hoft,  , The Gateway Pundit, (March 14, 2014). 

   

   Voice of America. 2014-04-01. 
}}

==External links==
*  
*  
*  , The Big Issue, March 11
*  , thedailybeast.com, March 21
*  , Sun News, March 31, 2014
*  , Berlin Global, March 27, 2014

 
 
 
 
 
 