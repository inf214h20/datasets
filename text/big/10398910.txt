Four Frightened People
{{Infobox film
| name           = Four Frightened People
| image          = Four Frightened People poster.jpg
| caption        = Theatrical poster
| director       = Cecil B. DeMille James Dugan (assistant)
| producer       = Cecil B. DeMille Emanuel Cohen (executive producer) (uncredited)
| writer         = Lenore J. Coffee Bartlett Cormack E. Arnot Robertson (novel)
| starring       = Claudette Colbert   Herbert Marshall   Mary Boland  William Gargan
| music          = Karl Hajos   John Leipold   Milan Roder  Heinz Roemheld
| cinematography = Karl Struss
| editing        = Anne Bauchens
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 min / 78 min (1935 re-release) (USA)
| country        = United States
| awards         =
| language       = English
| budget         =
}}
Four Frightened People (1934) is a film directed by Cecil B. DeMille, released by Paramount Pictures, and starring Claudette Colbert, Herbert Marshall, Mary Boland, and William Gargan. Based on the novel by E. Arnot Robertson.

==Plot== Malayan jungle.

==Cast==
*Claudette Colbert as Judy Jones
*Herbert Marshall as Arnold Ainger
*Mary Boland as Mrs. Mardick
*William Gargan as Stewart Corder
*Leo Carrillo as Montague
*Nella Walker as Mrs. Ainger
*Tetsu Komai as Native Chief
*Chris-Pin Martin as Native Boatman Joe De La Cruz as Native
*Minoru Nishida as Native
*Teru Shimada as Native
*E.R. Jinedas as Native
*Delmar Costello as Sakais
*Ethel Griffies as Mrs. Aingers Mother

==Filming locations==
*Hawaii, USA
*Waialua, O`ahu, Hawaii, USA

==Production crew==
*Art Direction - Roland Anderson Roy Burns
*Assistant Director (uncredited) - Cullen Tate, James Dugan
*Sound Mixer (uncredited) - Harry Lindgren
*Double (uncredited) - Mildred Mernie as Claudette Colbert, Bruce Warren as Herbert Marshall, Leota Lorraine as Mary Boland, Carl Mudge as William Gargan, Curley Dresden as Leo Carrillo

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 