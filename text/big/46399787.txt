Quick (1932 film)
{{Infobox film
| name = Quick
| image = 
| image_size =
| caption =
| director = Robert Siodmak
| producer = Erich Pommer
| writer = Félix Gandéra  (play)   Hans Müller  
| narrator =
| starring = Lilian Harvey   Hans Albers   Willy Stettner
| music = Hans-Otto Borgmann   Werner R. Heymann   Gérard Jacobson 
| cinematography = Otto Baecker   Günther Rittau
| editing = Viktor Gertler  UFA
| distributor = UFA
| released =  8 August 1932 
| runtime = 97 minutes
| country = Germany German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Quick is a 1932 German comedy film directed by Robert Siodmak and starring Lilian Harvey, Hans Albers and Willy Stettner. A separate French-language version was made, also directed by Siodmak and starring Harvey.  The film is based on a play by Félix Gandéra. It was made by Germanys largest studio Universum Film AG|UFA, with sets by art director Erich Kettelhut.

==Cast==
*  Lilian Harvey as Eva Prätorius  
* Hans Albers as Quick, Music Clown  
* Willy Stettner as Herr von Pohl, named Dicky  
* Albert Kersten as Professor Bertram  
* Paul Hörbiger as Lademann, Quicks manager  
* Karl Meinhardt as Direktor Henkel  
* Paul Westermeier as Clock  
* Genia Nikolaieva as Marion, dancer  
* Käthe Haack as Frau Koch  
* Flockina von Platen as Charlotte  
* Fritz Odemar as Headwaiter  

== References ==
 

== Bibliography ==
* Hardt, Ursula. From Caligari to California: Erich Pommers Life in the International Film Wars. Berghahn Books, 1996. 

== External links ==
*  

 

 
 
 
 
 
 
 

 