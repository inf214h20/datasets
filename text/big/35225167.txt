Kizhakku Mugam
{{Infobox film
| name           = Kizhakku Mugam
| image          = Kizhakku Mugam DVD cover.svg
| caption        = DVD cover
| director       = M. Anna Thurai
| producer       = P. Govindaraj M. Thangavel
| writer         = M. Anna Thurai
| starring       =  
| music          = Adhithyan
| cinematography = Shantha Moorthy
| editing        = K. P. K. Raviy
| studio         = Thai Films International
| distributor    = Thai Films International
| released       =  
| country        = India
| runtime        = 135 min
| language       = Tamil
}}
 1996 Cinema Indian Tamil Tamil film, Karthik and Reshma in lead roles. The film, produced by P. Govindaraj and M. Thangavel, had musical score by Adhithyan and was released on 15 January 1996 alongside Karthiks another film Ullathai Allitha.  
 
==Plot==

Venu (Karthik (actor)|Karthik) and his friend Mukka (Charle) work at the graveyard. Venu has a mother (Vadivukkarasi) and a sister called Sindhamani, a studious student. Poongodi (Reshma (actress)|Reshma), daughter of the village president, eve-teases Sindhamani. Venu orders to Poongodi to stop it immediately and Poongodi decides to revenge Venu. They have some quarrels. Nagaraj (Anandaraj), Poongodis uncle and a smuggler, wants to marry his niece Poongodi. After Poongodis puberty ceremony, she changes her behaviour and shows sympathy for Venu.

The village president (Radha Ravi) doesnt seem to care about caste or money, he united some lovers. Nagaraj steals the god status at the temple but Venu fight the henchmen and he saves the status. The villages Brahmins criticized his action and to prove the opposite, he recites Sanskrit very well. The village gurukkal (Delhi Ganesh) proposes to marry his daughter but Venu declares that Poongodi and he are in love. Poongodis father supplies Poongodi to forget him and Poongodis father arranges to find grooms but Kannapan (Alex (actor)|Alex), a bad turns good after Venus advises, helps Venu by menacing the grooms. All the village supports Venu and they dont understand Poongodis father hypocritical decision.

The gurukkal asks the Venus past and his mother discloses that he is not her real son. Venu was the son of a widow Brahmin and he was a gifted boy appreciated by the gurukkal. Venus real mother worked in her relatives house and her chief decided to rape her, the chiefs wife surprised them and humiliated Venus real mother front of the villagers. Venus real mother and the chief committed suicide. Venu leaves his village with his mother dead-body and he was adopted by a poor family. Poongodis father accepts his caste, he requests him to separate from his poor family, Venu refuses here and now.

Nagaraj kidnaps Sindhamani and tries to rape her, though to save her virginity, she commits suicide. Venu fights against Nagarajs henchmen and kills Nagaraj. Feeling guilty of Sindhamanis death, Poongodis father accepts Venu to marry his daughter.

==Cast==
 Karthik as Venu (Vettiyan Venugopal) Reshma as Poongodi
*Radha Ravi as the village president and Poongodis father
*Anandaraj as Nagaraj
*Charle as Mukka
*Delhi Ganesh as a gurukkal Alex as Kannapan
*Vadivukkarasi as Venus mother
*T. S. Raghavendra as the chief
*Sathyapriya as Poongodis mother
*Kumari Muthu as a villager

== Soundtrack ==
{{Infobox Album |  
  Name        = Kizhakku Mugam |
  Type        = soundtrack |
  Artist      = Adhithyan |
  Cover       = |
  Released    = 1996 |
  Recorded    = 1995 | Feature film soundtrack |
  Length      = |
  Label       = |
  Producer    = Adhithyan |  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer Adhithyan. The soundtrack, released in 1996, features 8 tracks with lyrics written by Vairamuthu.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || "Chinthamani" || S. P. Balasubrahmanyam || 3:41
|- 2 || "Muthu Muthu" || Shahul Hameed, Sujatha Mohan  ||  4:18
|- 3 || "Kadhal Enbadhu" || Adhithyan || 4:39
|- 4 || "Aathukullea" || P. Unni Krishnan, Sangeetha || 4:59
|- 5 || "Kannum Kannum" || K. S. Chithra, Adhithyan || 5:32
|- 6 || "Naangu Vedham" || S. P. Balasubrahmanyam || 2:20
|- 7 || "Iniyenna Pechu"  (female)  || Vasavi || 4:01
|- 8 || "Iniyenna Pechu"  (male)  || Unni Menon || 3:33
|}

==References==

 

 
 
 
 