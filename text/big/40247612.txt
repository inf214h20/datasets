Angel Unchained
{{Infobox film
| name           = Angel Unchained
| image          = Angel Unchained 1970 poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Lee Madden
| producer       = American International Pictures
| writer         = 
| screenplay     = Jeffrey Alan Fiskin
| story          = 
| based on       =  
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Angel Unchained (also known as Hells Angels Unchained) is a 1970 American action film directed by Lee Madden for American International Pictures and starring Don Stroud as the title character Angel. It was released in the United States on September 2, 1970. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 13 

==Plot==
Following a gang fight, biker Angel (Don Stroud) calls it quits and leaves his gang, the Nomads, in pursuit of a new life. He meets hippie community leader Jonathan Tremaine (Luke Askew), who is running from the anti-hippie townsfolk. Angel is quick to fall in love with another hippie, Merilee (Tyne Daly). When the situation becomes too tough to handle, Angel is forced to ask the Nomads to help out the hippies. They agree to, but their ineptitude to fight calls for him to face the scathing village people alone.

==Cast==
* Don Stroud as Angel
* Luke Askew as Jonathan Tremaine
* Larry Bishop as Pilot
* Tyne Daly as Merilee
* Aldo Ray as Sheriff
* T. Max Graham as Magician

==Production==
Also known as Hells Angels Unchained, Angel Unchained was directed by Lee Madden, written by Jeffrey Alan Fiskin, and produced by American International Pictures.    Filming locations included Phoenix, Arizona|Phoenix, Arizona, and real-life bikers were recruited for the film.  Angel Unchained marked the film debut of T. Max Graham, who played Magician. 

==Release==
The film had its United States premiere in August 1970,  before being theatrically released nationwide on September 2, 1970.  Up till at least December 1971, there were still screenings of Angel Unchained in Gadsden, Alabama|Gadsden, Alabama. 

===Reception===
A reviewer of The Nevada Daily Mail considered the film to be an unsuccessful attempt to combine elements of both The Wild Angels and Easy Rider. Although offering that the film was not quite meaningful, he found it to be a "fair motorcycle picture". 

==See also==
* List of films related to the hippie subculture

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 