Insaaf Ki Awaaz
{{Infobox film
| name           = Insaaf Ki Awaaz
| image          = Insaaf Ki Awaaz.jpg
| image_size     = 200px
| caption        = Movie poster
| director       = B. Gopal
| producer       = D. Rama Naidu
| writer         = 
| starring       = Anil Kapoor Madhuri Dixit   Rekha Anupam Kher Kader Khan
| released       =  
| proceeded by   = 
| cinematography = 
| editor         = 
| music          = Bappi Lahiri
| studio         = Suresh Productions
| lyrics         = Indeevar
| distributor    = 
| country        = India
| language       = Hindi
| runtime        = 
| budget         = 
| gross        =  
}}

Insaaf Ki Awaaz is a 1986 Bollywood action film directed and produced by D Rama Naidu under the banner of Suresh Productions.The film features Rekha, Anupam Kher and Kader Khan in pivotal roles.The music director of the film is Bappi Lahiri.    

==Cast==
*Anil Kapoor-Ravi Kumar
*Madhuri Dixit
*Rekha-Inspector Jhansi Rani
*Anupam Kher-Kailashnath
*Asrani
*Gulshan Grover-Vikram Domukhiya
*Roopesh Kumar-Mahendranath
*Kader Khan-Politician Chaurangilal Domukhia
*Shafi Inamdar-Sannyasi Raja
*Richa Sharma
*Raj Babbar-Chandrashekhar Azaad

==Music==

The soundtrack was composed by the music director-duo of Bappi Lahiri while the lyrics were penned by Indeevar under music banner T-series.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| title1 = Love In The Rain  Janaki
| lyrics1 = 
| title2 = Radhe Pyar De 
| extra2 = Kishore Kumar, Janaki 
| lyrics2 = 
| title3 = Insaaf Ki Awaaz 
| extra3 = Kishore Kumar
| lyrics3 = 
| title4 = Pyar Pyar 
| extra4 = Mohammad Aziz, Janaki
| lyrics4 =  
| title5 = Irada Karo To Poora Karo 
| extra5 = Lata Mangeshkar, SP.Balasubramaniam 
| lyrics5 = 
}}

==References==

 

==External links==
*  

 
 
 


 