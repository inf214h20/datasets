Passion Flower Hotel (film)
{{Infobox film
| name           = Passion Flower Hotel
| image          = Leidenschaftliche Blümchen.jpg
| caption        = Theatrical poster
| director       = André Farwagi
| producer       = Artur Brauner Robert Russ Allexander Zellermeyer
| writer         = Roger Longrigg (novel) Ken Globus Paul Nicholas
| starring       = Nastassja Kinski Gerry Sundquist Stefano DAmato Gabriele Blum Sean Chapman Véronique Delbourg Nigel Greaves Marion Kracht Fabiana Udenio Kurt Raab
| music          = Francis Lai
| cinematography = Richard Suzuki Jair Ganor Gernot Köhler Sandro Tamborra
| editing        = Daniela Padalewski-Junek Atlantic Releasing Corporation Audifilm
| released       =  
| runtime        = 100 minutes
| country        = Germany
| awards         =
| language       = German
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 1978 coming-of-age coming of age comedy film directed by André Farwagi. It is a liberal adaptation of the novel Passion Flower Hotel and stars Nastassja Kinski as one of the schoolgirls, in her third feature film.

==Plot== American girl Deborah Collins (Kinski) arrives at the St. Claras Boarding School in Switzerland. The school headmistress wants to use Deborah as a tool to discipline the other girls but she is revealed to be more experienced and daring in sexual matters. Tha girls now plot to lose their virginity with the boys in the private school across the lake. After Deborah finally has sex with Frederick Sinclair (Sundquist) in a romantic setting, she is expelled and the other girls feel that everything will be so sad and boring without her. She informs the headmistress that she will tell everyone that the school is run by disreputable teachers if she expels the other girls. She departs on a train after kissing Frederick goodbye.

==Cast==
*Nastassja Kinski.....Deborah Collins
*Gerry Sundquist.....Frederick Irving Benjamin Sinclair
*Stefano DAmato.....Plumpudding
*Gabriele Blum.....Cordelia
*Sean Chapman.....Rodney
*Veronique Delbourg.....Marie Louise
*Nigel Graves.....Carlos
*Marion Kracht.....Jane
*Carolin Ohrner - Gabi
*Fabiana Udenio.....Gina

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 

 
 