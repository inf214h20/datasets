Pobočník Jeho Výsosti
 
{{Infobox film
| name = Pobočník Jeho Výsosti
| image =
| image size =
| alt =
| caption =
| director = Martin Frič
| producer =
| writer = Václav Wasserman Emil Artur Longen
| starring = Vlasta Burian
| music =
| cinematography = Otto Heller
| editing =
| studio =
| distributor =
| released =  
| runtime = 77 minutes
| country = Czechoslovakia
| language = Czech
| budget =
| gross =
}}
 Czech comedy film directed by Martin Frič. It was released in 1933.   

==Cast==
* Vlasta Burian as 2nd Lt. Alois Patera
* Nora Stallich as Princ Evzen
* Suzanne Marwille as Princezna Anna Luisa
* Jaroslav Marvan as Plukovník
* Bedrich Vrbský as Kinzl, podplukovník
* Helga Nováková as Pepina Kalasová
* Ela Sárková as Bardáma
* Alexander Trebovský as Guth, policejní rada
* Frantisek Kreuzmann as Paces, sikovatel
* Frantisek Cerný as Kuchar u posádky v Mnuku
* Marie Grossová as Císnice u Maxima
* Ladislav Hemmer as Richard Mádr, rytmistr

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 