America: A Call to Greatness
 
{{Infobox film show_name = America: A Call to Greatness image = Poster for America-A Call to Greatness.jpg image_size = 200px caption =  Poster  show_name_2 =  genre = Docudrama format = Film director = Warren Chaney writer = Warren Chaney  producer = Warren Chaney executive_producer = Joe Hillyard Michael Teilmann Cress Darwin starring = Charlton Heston Mickey Rooney Deborah Winters Peter Graves Rita Moreno Dorian Harewood Jane Russell Gene Autry location = Disney-MGM Studios Paige-Brace Studios 50 state location filming music = Ron Dixon cinematography = John Snavely Max Balchowsky editing = Marco DuBose company = Paige-Brace Cinema distributor = Millennial Entertainment runtime = 2 hours country = USA language = English budget = $4,500,000}}

America: A Call to Greatness is a 1995 docudrama feature from Paige-Brace Cinema, chronicling United States history from its inception through the 20th century. It stars Charlton Heston, Mickey Rooney, Deborah Winters, Peter Graves, Jane Russell, and Rita Moreno, among others and was written, directed and produced by Warren Chaney.    Actors portray historical figures in American history, using dialogue mirroring words spoken or written by them.      

The two-hour movie incorporates music-video like production numbers centered on specific American themes. It employs such musicians as Lee Greenwood, Phil Driscoll, Greg X. Volz, The Imperials, Larnelle Harris, Sandi Patty and others. Gene Autry, former western actor, businessman, film and recording star also appeared in what was to be his final screen performance.   Commentaries from known businessmen, U.S. Senators, Congressmen, and three former U.S. Presidents are interspersed throughout the picture. The production was filmed at the Disney-MGM Studios, Paige-Brace Studios and on location throughout the United States.  It was shot on an estimated budget of $4.5 million. 

==Plot==
Opening remarks by Charlton Heston and Mickey Rooney are used to establish the film’s direction and mood. Introductory scenes quickly dissolve into one laced with sounds of thunder and artillery fire. The setting is atop Niagara Falls where waters cascade over the falls’ edge. Superimposed in the haze are dissolving scenes of American conflicts from the American Revolutionary War through Desert Storm.

At the misty base of the falls, a glass-like figure materializes from the fog and walks toward the camera. As it draws closer, it assumes the features and flesh tones of George Washington who states, “In my years of public service for this nation, I have observed one truth...that no people can be bound to acknowledge the Invisible Hand which conducts the affairs of men, more than the people of the United States. Every step by which they have advanced to the character of an independent nation seems to have been distinguished by some token of providential agency.”
  in America: A Call to Greatness (1995)]]

Washington’s figure morphs into that of Benjamin Franklin who declares, “Everywhere I turn today, I see dissent. I see, Sir, those who say this great American experiment cannot work. They fail in their understanding, to realize that their very right to say that, was paid for with human life. Yet, for their own marginal good and safety, they are freely willing to forsake the heritage of this nation. To those I say.... They, that can give up essential liberty to obtain a little temporary safety, deserve neither liberty nor safety.”

Franklin turns from the camera, transposing into President John Adams who proclaims, “Posterity—you will never know how much it has cost my generation to preserve your freedom. I hope you will make good use of it.... If you do not, I shall repent it in Heaven, that I ever took half the pains to preserve it.”
 Declaration of Independence. He served for eight years under George Washington as this nations first Vice-President. His words still ring through the halls of time. They call to those of us here today...to carry on the tradition of freedom, for which so many have died. We are that posterity... and, we have been summoned.”

  stars in America: A Call to Greatness (1995)]]
Hestons scene transitions to that of the giant arch in Washington Square at the Disney-MGM Studios. A lowering camera reveals vocalist Larnelle Harris, singing, Let Freedom Ring. As the song concludes, fifty doves released on each side of the arch fly toward the camera. One dove trails the others in a similar arc but flies closer to the camera spreading its wings and passing over the lens. As it does so, it transitions to title for the picture and its opening credits.

The duration of the picture provides a continuing narrative of America’s founding and evolution from the revolutionary days of 1776 through the 20th century. By means of prosthetic make-up technology and historical locations, famous leaders from the past are used to speak to the present.

  sings   Petra founder, Greg X. Volz, to Babbie Mason, Andrae Crouch and Lee Greenwood. Both music and commentary are interspersed with the re-creations of famous Americans in history.

  as  
Successive scenes build upon the other, moving from the nation’s founding to the problems of a 20th-century contemporary society. One dramatic episode originates with Senators John McCain and Bob Kerrey discussing military sacrifices throughout the nation’s history.  Their settings transition to President Abraham Lincoln on location in Gettysburg, Pennsylvania delivering his Gettysburg Address. As Lincoln speaks, the background dissolves into evolving war scenes progressing through each of Americas major conflicts. The segment ends with Lincoln’s closing lines over the Tomb of the Unknown Soldier as the surroundings dissolve into an early American street with The Imperials singing the "Battle Hymn of the Republic".

Closing scenes contain commentaries from presidents (past and living), each expressing how he views Americas greatness. President George H. W. Bush speaks last and with his closing words, the setting dissolves to one with actor, singer and businessman, Gene Autry. Autry addresses the viewing audience explaining what America has meant to him. At the close, he recites the Pledge of Allegiance after which his scene transitions to one with President Ronald Reagan articulating farewell remarks to the audience.
 American flag Liberty Square at the Walt Disney World Resort. Sandi Patty and a sizable choir sing "The Star-Spangled Banner" as a large American flag rises behind them. At the song’s close, a wide shot of the singer, choir and pavilion reveal a massive display of overhead fireworks. The scene dissolves to the final credits underscored by a new version of America the Beautiful by Gene Autry (his first song release since 1965). 

== Cast ==
 
* Charlton Heston Host & Narrator
* Mickey Rooney Host & Narrator
* Deborah Winters Host & Narrator
* Peter Graves Host & Narrator
* Jane Russell Host & Narrator
* Rita Moreno Host & Narrator
* Gene Autry Host, Narrator and Performer
* Christopher Hewett Host & Narrator
* Dorian Harewood Host & Narrator
* Steve Geyer Host & Narrator
* Cassidy Rae Host & Narrator
* Julianne Morris Host & Narrator
* Blue Deckert as George Washington
* Rutherford Cravens as Benjamin Franklin
* James Black as John Adams
* Gabriel Folse as Thomas Jefferson
* Charles Charpiot as Calvin Coolidge
* Raymond Baker as Abraham Lincoln
 
 
* Nick Hagler as Mark Twain
* David Sanders as Will Rogers
* Daniel Kamin as Theodore Roosevelt
* Charles Rimmel as Harry S. Truman
* Leonora Scelfo as  Laura Whitfield
* John Meadows as Andrew Jackson
* David Logan Rankin as Patrick Henry
* Kyle Scott Jackson as Booker T. Washington
* Robert Marich as Franklin D. Roosevelt
* Jean Pennington as Clara Barton
* Susan Hilyard as Susan B. Anthony
* Luis Lemus as James Madison
* Steve Word as Woodrow Wilson
* Daniel Silverheels as Chief John Big Tree
* E.P. Ryan as Benjamin Disraeli
* Suzanne Savoy as Abigail Adams
* Morgan Redmond as Reverend John Witherspoon
* James Huston as General Douglas MacArthur
 
 
 
 

== Musicians (in order of appearance) ==
 
 
* Larnelle Harris
* Phil Driscoll
* Benjamin
* Greg X. Volz
* Point Of Grace
* Leon Patillo
* Tata Vega Terry Clark
* Annie Herring Jamie Collins
* Rick and Cathy Riso
* Jean Luc Lajoie
* 4 Him
* Babbie Mason
* Andrae Crouch
* Lee Greenwood One Nation
* The Imperials
* Sandi Patty
* International Cinema Choir & Chorus
* Gene Autry

== Program guests (in order of appearance) ==
 
* D. James Kennedy
* Bill Bright and Vonette Bright
* Rev. Peter J. Marshal
* David Noebel
* David Barton (author)
* Ambassador Alan Keyes
* Joe Weider
* Buzz Aldrin
* Major General John K. Singlaub
* U.S. Representative Steve Stockman
* Senator Bob Kerrey
* Senator John Mccain
* Senator Paul Simon (politician)
* President Jimmy Carter
* President George H.W. Bush
* President Ronald Reagan

== Overview == Gettysburg and George Washington during wintertime at Valley Forge. 

The hosts for America: A Call to Greatness included film and television personalities including  , Bill Bright and his wife, Vonette Bright, Rev. Peter J. Marshal and David Noebel, founder of Campus Crusade for Christ.     
 Paul Simon, Congressman Steve Stockman, Senator John McCain, Ambassador Alan Keyes, Senator Bob Kerrey, and three former United States Presidents (Presidents Jimmy Carter, Ronald Reagan and George H. W. Bush).  

  (right) directing  
The film interweaves music and drama using music video like production numbers employing vocalists and musicians including Lee Greenwood, Phil Driscoll, Greg X. Volz, The Imperials, Sandi Patty and others.  Gene Autry, former western actor, businessman, film and recording star made an appearance in what was to be his final performance.  Autry introduced the first song, "America the Beautiful", since his retirement in 1965.  

Remarks by Presidents (past and present) synopsize the picture before the production closes with singer Sandi Patty singing "The Star-Spangled Banner". On the musics final notes, fireworks fill the sky overlooking the singer, choir and Disneys Liberty Square.

== Distribution ==
After the films fall 1995 premier, theatrical run and opening broadcast, the picture was released on Laserdisc in the United States by Millennial Entertainment. The discs were withdrawn when the picture was re-released for theatrical venue screenings throughout the United States. Following a long independent venue run, the film was released to DVD in March 2011.   

== Production ==
Casting was conducted January through March 1994. Subsequently the picture was filmed at the   For the productions release to television, the films director (Warren Chaney) forewent traditional station break announcements (bumper (broadcasting)|bumpers) by filming various people and population groups as his film crew gathered background footage across the nation. An Indian Chief from Pagosa Springs, Colorado delivers a station break message in sign language and an Army Sergeant announces the break from his military post. A young child proclaims, "We be right back," before the pictures final credits 

Editing and post production was carried out at the The Post Group, Orlando Florida and South Coast Film and Video, Houston, Texas. 

==Reception==
Winston Aaronson of Screen Times said the film is “well directed and extremely well written.” Verna Schroder of The Sun News wrote, “the film is emotionally moving.” Some reviewers cautioned that the historic nature of the film possibly made it too religious or conservative for some audiences. 

== References ==
 
 

== External links ==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 