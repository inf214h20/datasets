Si le soleil ne revenait pas
 
{{Infobox film| title = Si le soleil ne revenait pas
| image = Si_le_soleil_ne_revenait_pas_-_poster.jpg
| director = Claude Goretta
| starring = Charles Vanel  
Catherine Mouchet  
Philippe Léotard released = 1987 runtime = 115 minutes
}}

Si le soleil ne revenait pas (If the sun never returns) is a Franco/Swiss film directed by Claude Goretta, with a script written by the director, from the original novel by Charles Ferdinand Ramuz and stars Charles Vanel, Catherine Mouchet and Philippe Léotard. It was one of the  In-Competition Films at the 1987 Venice Film Festival.

==Plot==
In a little village, lost at the bottom of a valley in the midst of mountains and deprived of sun for long months, an old man Anzerul (Charles Vanel), prophet and magician, announces the end of the world. According to his calculations the sun will not return to the village and the village  will descend into an endless winter. The villagers give way, one after the other, to panic, piling up wood or giving themselves up to drink. Only Isabelle Antide (Catherine Mouchet), holds up against the hysteria. She urges them not to give in to terror, and to struggle against a damaging fatalism. On the 13 April, the day the sun returns to the village each year, she leads them above the cover of fog which hangs over their valley.

==Cast==
*Charles Vanel  as  Anzerul
*Catherine Mouchet  as  Isabelle Antide
*Philippe Léotard  as Arlettaz
*Raoul Billeray   as Denis Revaz
*Claude Evrard  as Follonier
*Fred Ulysse  as Tissières
*Jacques Mathou  as  Cyprien Métrailler

==External links==
*  at the   

 
 
 
 
 

 
 