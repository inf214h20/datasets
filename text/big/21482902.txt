Hatchet for the Honeymoon
 
{{Infobox film
| name           = Hatchet for the Honeymoon
| image          = Hatchet-for-the-honeymoon.jpg
| alt            =
| caption        = Italian theatrical release poster
| director       = Mario Bava
| producer       = Manuel Cano
| writer         = Santiago Moncada Mario Bava
| starring       = Stephen Forsyth Dagmar Lassander Laura Betti Jesús Puente
| music          = Sante Maria Romitelli
| cinematography = Mario Bava
| editing        = Soledad López
| studio         = Manuel Caño Sanciriaco Mercury Films Pan Latina Films Películas Ibarra y Cía.
| runtime        = 105 min.
| released       = 2 June 1970
| country        = Italy Spain
| language       = Italian
| gross          = Italian lira|€59,000,000  (Italy)  Spanish peseta|€65,559  (Spain) 
}}

Hatchet for the Honeymoon (Italian: Il rosso segno della follia / The Red Mark of Madness); also known as Blood Brides and An Axe for the Honeymoon) is a 1970 Italian giallo film directed by Mario Bava and starring Stephen Forsyth, Dagmar Lassander and Laura Betti.

== Plot ==

The film is about a series of murders committed by a sexually charismatic but impotent man who owns a bridal shop. Alienated from his wife because of his failure to consummate the marriage, he kills women dressed in his bridal gowns. He disposes of the bodies in an incinerator. The police suspect him, but have no evidence. After he eventually kills his wife, her ghost appears and informs him that she will never leave his side - and will be visible to everyone but him. At a nightclub he tries to pick up a woman, but she rejects him when she sees his wifes ghost by his side.

Attracted to one woman, Helen, he tries to resist her, but eventually takes her to his bridal room. While there, his inner struggle comes to a head, and it is revealed that in childhood he murdered his own mother to stop her from marrying again. Helen fights him off, and the police enter. As he is taken away, his wifes ghost reappears and tells him that now she will only be seen by him, and they will be together forever.

== Critical reaction ==
 AllMovie called it "not the best of Mario Bavas work", but "a must see for those who love the genre and admire stylish horror films." 

Although the film does not have enough reviews for a Rotten Tomatoes rating, the site records two positive and two negative reviews from critics. 
 American Psycho in particular." 

== References ==

 

== External links ==

*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 