The Vortex (film)
{{Infobox film
| name           = The Vortex
| image          =
| image_size     =
| caption        =
| director       = Adrian Brunel 
| producer       = Michael Balcon  C. M. Woolf
| writer         = Noël Coward (play)   Eliot Stannard   Roland Pertwee
| narrator       =
| starring       = Ivor Novello   Willette Kershaw   Simeon Stuart   Julie Suedo
| music          =  James Wilson
| editing        = 
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service
| released       = 1928
| runtime        = 
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
  British drama film directed by Adrian Brunel and starring Ivor Novello, Willette Kershaw and Simeon Stuart.  It was an adaptation of the Noël Coward play The Vortex and was made by Gainsborough Studios. The films sets were designed by Clifford Pember.

==Cast==
* Ivor Novello - Nicky Lancaster
* Willette Kershaw - Florence Lancaster
* Frances Doble - Bunty Mainwaring
* Alan Hollis - Tom Veryan
* Simeon Stuart - David Lancaster
* Kinsey Peile - Pouncefort Quentin
* Julie Suedo - Anna Vollof Dorothy Fane - Helen Saville

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 