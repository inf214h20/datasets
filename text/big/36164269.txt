Una farfalla con le ali insanguinate
{{Infobox film
| name           = Una farfalla con le ali insanguinate
| image          = Una farfalla con le ali insanguinate poster.jpg
| alt            = 
| director       = Duccio Tessari
| writer         = Gianfranco Clerici Duccio Tessari
| starring       = Helmut Berger 
| music          = Gianni Ferrio
| cinematography = Carlo Carlini
| released       = 10 September 1971
| country        = Italy
| language       = Italian
}}

Una farfalla con le ali insanguinate is a 1971 giallo film directed by Duccio Tessari. It was also distributed internationally as The Bloodstained Butterfly, and as Das Geheimnis der Schwarzen Rose (Secret of the Black Rose) in West Germany.

== Cast ==
*Helmut Berger: Giorgio
*Giancarlo Sbragia: Alessandro Marchi Evelyn Stewart (Ida Galli): Maria Marchi
*Wendy DOlive: Sarah Marchi
*Silvano Tranquilli: Inspector Berardi
*Carole André: Françoise Pigaut
*Lorella De Luca: Marta Clerici
*Günther Stoll: Attorney Giulio Cordaro
*Wolfgang Preiss: The Prosecutor
*Dana Ghia: Diamante

== Critical reception ==

Allmovie gave the film a positive review, writing "this beautifully assembled giallo is among the best of its time" and that it features a "genuinely intelligent script, a rarity in a subgenre generally known for flamboyant visuals at the expense of narrative cohesion." 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 

 

 