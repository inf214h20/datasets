Wither (film)
{{Infobox film
| name           = Wither
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sonny Laguna   Tommy Wiklund
| producer       = David Liljeblad   Tommy Wiklund
| writer         = Sonny Laguna   David Liljeblad   Tommy Wiklund
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Patrik Almkvist   Lisa Henni   Johannes Brost   Patrick Saxe   Amanda Renberg   Max Wallmo  
| music          = Samir El Alaoui
| cinematography = Tommy Wiklund
| editing        = David Liljeblad   Tommy Wiklund
| studio         = Stockholm Syndrome Film
| distributor    = Njutafilms   Artsplotation   NonStop Entertainment
| released       =  
| runtime        = 95 minutes
| country        = Sweden
| language       = Swedish SEK
| gross          = 460. 000 SEK
}}

Wither (Swedish: Vittra) is a Swedish 2012 horror film directed by Sonny Laguna and Tommy Wiklund, who wrote and produced the film along with David Liljeblad. The plot follows a group of young Swedes who travel to an abandoned cabin over the weekend. The cabin turns out to be the home of a Vittra (folklore)|Vittra, and the friends have to fight for their lives, aided by the hunter Gunnar (Johannes Brost).

The film was marketed as the first Swedish zombie film shown in cinemas and was incorrectly stated by several Swedish newspapers as being the first Swedish zombie film. The first Swedish zombie film is Die Zombiejäger from 2005. 

==Vittra==
 the Norse religion. Vättern is believed by some to possibly be named after the Vættr, which would mean the original name meant something along the lines of "lake of the Vættr".

==Plot==

The hunter Gunnar is looking for his daughter Lisa. He finds her lying on the ground being eaten alive by his wife. Gunnar shoots his wife twice in the head, killing her. The opening titles include rough sketches, showing the history of the Vættr.

The young couple Albin and Ida are planning a weekend trip to a remote cabin with Idas brother, Simon, and a couple of friends. The group drive to a remote woodland area and are forced to walk the last bit. While walking to the cabin, the group notices Gunnar watching them from a cliff and the aggressive Simon shouts him off. The group finds the cabin locked, contrary to the information given by Albins father, Olof. While Albin tries to lockpick the front door, his friend Marcus finds an open window on the backside and talks Marie into climbing in to scare the others. Marie climbs in and goes into the basement to investigate, finding an axe. She does not notice an earthlike creature watching her from the shadows.

The group finlly gets into the cabin and is scared by Maries sudden appearance. She does not seem to be well. While the others install themselves on the ground floor, Idas goth (fashion)|goth-friend Tove brings Simon upstairs and instigates a sexual encounter with him despite knowing that Linnea is in love with Simon. The gang throws a party, but Maries sickness gets worse and she attacks Tove, biting off her lip, and then spits blood in Linneas eyes. Simon captures and binds Marie. Gunnar arrives at the cabin, suggesting the group should kill Marie. He tells them about the Vættr and how his own family were killed in the cabin just a few days prior. Gunnar informs the group that the Vættr have the ability to steal the souls of the living by looking in their eyes, turning them into the undead. Gunnar also warns them that the undead slaves of the Vættr are very infectious. While Marcus tends to Tove she is resurrected and attacks Marcus, wounding him. Gunnar arrives and saves Marcus but Tove manages to escape. Simon and Linnea arm themselves with shovels in the shed and go to fetch police aid which Albin was called for. Gunnar goes out to kill Tove, but she ambushes and bites him. Gunnar proceeds to beat Tove into a pulp before carving off her head with his fishing knife. Gunnar then commits suicide in the shed. Simon notices infection in Linneas eyes and beats her unconscious and buries her alive. Overcome with grief over his action, he ignores his instruction and goes back to the cabin to save his sister. The aid of armed lawmen is now impossible. Meanwhile, Marie breaks free and attacks the others. Albin, Ida and Marcus fights her but Marcus dies before Albin crushes her head with a rock. Marcus turns into an undead and Albin and Ida has to hide upstairs, unable to escape. Simon arrives and tries to help them but is thrown from a window by Marcus. Simon survives but is attacked by Linnea and killed. Albin uses Idas cellphone to determine that they can leave their hideout and Albin grabbs the axe Simon had fetched before but dropped. Marcus and Linnea attack but Albin kills Linnea with the axe before Marucs take it from him. Albin and Ida lock themselves into a room and dig through the floor, dropping in the kitchen. Albin breaks his arm in the process. Marcus jumps after him, but Albin pushes a table beneath the hole, which Marcus impales himself on. Albin then kills him with a hammer. Simon attacks Ida and forces his blood into her mouth with a kiss. Albin is unable to save her and rushes to the shed and grabs Gunnars rifle and manages to blow off Simons head with a point blank|point-blank gun blast.

Albin returns but realises Ida is (un)dead too. Albin barely manages to fight back out of grief but finally kills Ida by dropping a heavy shelf on her. Albin cries for his girlfriend and his friends, but suddenly there is movement beneath the floorboards. The Vættr herself ascends from the basement and inspects the devastation. However, Albin closes his eyes and turns off the lights, rendering the ancient and fragile creature powerless. Albin drops the fridge on the Vættr, crushing it. Albin then leaves the cabin, now having become the "new Gunnar".

==Cast==

*Patrik Almkvist as Albin
*Lisa Henni as Ida
*Patrick Saxe as Simon
*Johannes Brost as Gunnar
*Amanda Renberg as Linnea
*Jessica Blomkvist as Marie / The Vættr
*Max Wallmo as Marcus
*Anna Henriksson as Tove
*Ralf Beck as Olof
*Sanna Ekman as Eva
*Julia Knutson as Lisa
*Ingar Sigvardsdotter as Gunnars Wife 

Voice acting veteran Fredrik Dolk and Swedish exploitation star A.R. Hellquist had roles as policemen, but both were cut from the film. Hellquist still makes a small, uncredited appearance as the voice of the police Albin talks to over the phone.

==Response==

The film was met with very polarised reviews. Aftonbladet gave the film a 1-star score,  while Expressens Ronny Svensson praised the films atmosphere and musical score, urging viewers to note see it alone and gave the film a 4/5 score.  Some Swedish newspapers criticised the film for not having irony, which the critics of Nordic Fantasy, while remaining critical to the film, thought that complaint was absurd. 
 Evil Dead 2013 should have been" and stated that the film was "Gruesome, grim, well-written, directed and acted".  Horror Talk gave the film 4 stars out of 5.  FEARnets reviewer thought the film was decent but was very critical to the lack of originality.  Cookie N Screens reviewer also commented that the film met her expectations for an Evil Dead remake, unlike the actual remake. The reviewer felt parts were tedious and over the top but ended up enjoying the film as whole. 

==References==
 

 