Who'll Stop the Rain
 
{{Infobox film
| name           = Wholl Stop the Rain
| image          = Wholl stop the rain.jpg
| image size     =
| caption        = Theatrical release poster by Tom Jung
| director       = Karel Reisz
| producer       =  Herb Jaffe Robert Stone
| narrator       =
| starring       = Nick Nolte
| music          = Laurence Rosenthal
| cinematography = Richard H. Kline John Bloom
| distributor    = United Artists
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}}
 1978 psychological Robert Stone Dog Soldiers. The music score was by Laurence Rosenthal and the cinematography by Richard H. Kline.

It was entered in the 1978 Cannes Film Festival.   

==Plot==
The film opens in Saigon at the height of the Vietnam War.
 DEA agent (Zerbe) who initially set the deal in motion.  As Marge is separated from her supply of prescription drugs, she experiences Drug withdrawal|withdrawal, and Hicks decides to help her wean off her dilaudid addiction by using the heroin.  Hicks also attempts to find another buyer for the heroin before his pursuers can catch up to him.

==Cast==
* Nick Nolte as Ray Hicks
* Tuesday Weld as Marge Converse
* Michael Moriarty as John Converse
* Anthony Zerbe as Antheil
* Richard Masur as Danskin
* Ray Sharkey as Smitty
* Gail Strickland as Charmian
* Charles Haid as Eddie Peace
* David Opatoshu as Bender
* Joaquín Martínez as Angel (as Joaquin Martinez)
* James Cranna as Gerald
* Timothy Blake as Jody
* Shelby Balik as Janey
* Jean Howell as Edna
* José Carlos Ruiz as Galindez (as Jose Carlos Ruiz)

==Background and production == Dog Soldiers, which had been winner of the National Book Award (US) for fiction in 1975.  For its original US theatrical release it was re-titled Wholl Stop the Rain, after the song by Creedence Clearwater Revival, which features prominently (along with several other popular CCR tracks) on the films soundtrack. The film was released as Dog Soldiers for release in several foreign territories. Some copies of the DVD of Wholl Stop the Rain actually contain prints titled Dog Soldiers.
 Beat writer Neal Cassady, with whom Stone became acquainted through novelist Ken Kesey, a classmate of Stones in graduate school at Stanford University.
 commune setting, where lights and stereo speakers placed throughout the woods are utilized in Hicks escape plan, was partially based on Keseys home in La Honda, California, where Kesey and his friends &mdash; known as the Merry Pranksters &mdash; famously wired the surrounding woods with lights and sound equipment to enhance their experiments with LSD.  Though technically not a commune, Keseys home was a frequent site for large parties attended by a mixture of literary luminaries such as poet Allen Ginsberg and journalist Hunter S. Thompson, music figures (including Jerry Garcia, whose group The Grateful Dead later became the house band for Keseys famous Acid Tests), and outlaws, especially members of the infamous Hells Angels motorcycle club.  These parties are described intimately in works by Ginsberg and Thompson and in Tom Wolfes book The Electric Kool-Aid Acid Test. Mention of Ken Kesey, his Merry Pranksters, and Neal Cassady are also discussed in detail in Martin Torgoffs book Cant Find My Way Home. 

The Saigon scenes were actually also filmed on a set in Mexico. There was a casting advertisement in Mexico City for people of any Asian background to represent the Vietnamese.

==Awards==
*Nominee Palme dOr Cannes Film Festival (Karel Reisz)
*Nominee Best Actor National Society of Film Critics (Nick Nolte) Robert Stone)

== Soundtrack ==
*Del Reeves - "Philadelphia Fillies"
*Jackie DeShannon - "Put a Little Love in Your Heart" American Pie"
*Slim Whitman - "Ill Step Down"
*Creedence Clearwater Revival - "Hey Tonight"
*Creedence Clearwater Revival - "Wholl Stop the Rain (song)|Wholl Stop the Rain"
*Creedence Clearwater Revival - "Proud Mary
*The Spencer Davis Group - "Gimme Some Lovin" Golden Rocket"

== References ==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 