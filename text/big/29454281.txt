The Help (film)
 
 
{{Infobox film name           = The Help image          = Help poster.jpg alt            = Two black maids standing, beside two white women on a bench. Against a bright yellow background, the text in light purple. caption        = Theatrical release poster director       = Tate Taylor producer       = {{Plain list| Chris Columbus
*Michael Barnathan
*Brunson Green
}} writer         = Tate Taylor based on       =   starring       = {{Plain list| 
*Jessica Chastain
*Viola Davis
*Bryce Dallas Howard
*Allison Janney
*Octavia Spencer
*Emma Stone 
}} music          = Thomas Newman cinematography = Stephen Goldblatt editing        = Hughes Winborne studio  DreamWorks Pictures Reliance Entertainment Participant Media Image Nation 1492 Pictures distributor  Walt Disney Studios Motion Pictures  released       =   runtime        = 146 minutes country        = United States language       = English budget         = $25 million   The Business Behind the Show (blog of the Los Angeles Times)|date=August 11, 2011|accessdate= July 7, 2012}}  gross          = $216.6 million   
}} period drama adapted from novel of Civil Rights era in 1963 Jackson, Mississippi. Skeeter is a journalist who decides to write a book from the point of view of the maids (referred to as "the help"), exposing the racism they are faced with as they work for white families.
 DreamWorks Pictures and released by Touchstone Pictures, the film opened to positive reviews and became a commercial success with a worldwide box office gross of $216.6 million  against its production budget of $25 million.
 Best Picture, Best Actress Best Supporting Best Supporting Actress for Spencer. On January 29, 2012, the film won the Screen Actors Guild Award for Outstanding Performance by a Cast in a Motion Picture.

==Plot==
In 1963 in Jackson, Mississippi, Aibileen Clark (Viola Davis) is a black maid spending her life raising white children after the death of her only son from an industrial accident. She works for the Leefolt family, principally taking care of the children of Elizabeth Leefolt, a young woman gripped with postpartum depression who refuses to acknowledge her daughter except when disciplining her. Aibileens best friend is Minny Jackson (Octavia Spencer), an outspoken black maid who has long worked for Hilly Holbrooks (Bryce Dallas Howard) mother, Mrs. Walters (Sissy Spacek), to the point that they are very comfortable with each other. Minnys stormy temper is tolerated due to respect for her great cooking skills. Eugenia "Skeeter" Phelan (Emma Stone) is an independent thinking young white woman returning to the family plantation after graduating from the University of Mississippi      to find that her beloved childhood maid, Constantine (Cicely Tyson), has quit while she was away. She is perplexed as she believes Constantine would not have left without writing her, and she eventually learns that Constantine was fired by Skeeters mother Charlotte (Allison Janney).

While Skeeters social group attended college to find husbands, Skeeter herself earned a double-major degree and remains single, much to Charlottes chagrin, and aspires to a successful writing career. She begins making inroads towards this goal when she lands a job with the local paper as a "homemaker hints" columnist, asking Elizabeth if Aibileen could help her in answering the letters; after gaining permission from Elizabeth (who in turn "gained permission" from Hilly) and approaching Aibileen herself, the maid agrees. Upon spending time with Aibileen one-on-one, Skeeter becomes increasingly uncomfortable with the attitude of her white socialite female friends towards their "help", especially after learning of Hillys "Home Help Sanitation Initiative", a plan to legislate mandatory separate toilets for black domestic help because "black people carry different diseases than white people".

Inspired by her relationship with Constantine, Skeeter forms an idea of writing about the relationships between whites and their black help, especially since the children raised by black maids tend to take on the prejudiced attitudes of their parents when they become adults. The maids are reluctant to cooperate, afraid of retribution from their employers, but Aibileen eventually agrees. Minny also cooperates after being fired by Hilly for using the guest bath as instructed by Mrs. Walters instead of going out into tornado weather to use the helps outdoor toilet.

Hilly makes finding work difficult for Minny by saying she had stolen from her, which then causes Minnys daughter to leave school to work as a maid. Minny eventually finds work with a working-class outcast Celia Foote (Jessica Chastain), who is married to wealthy socialite Johnny Foote (Mike Vogel), the ex-boyfriend Hilly never truly got over. Celia informs Minny that shes pregnant.  Because of her sweet disposition, ignorance to the unspoken social caste and exuberant personality, Celia is clueless about how to properly treat Minny as an employee in her home; she often joins Minny in the kitchen for cooking lessons and they share the meals they make together while providing each other with advice on how to deal with the bullies each woman faces in her life.

The relationship between Celia and Minny deepens further after Celia miscarries and she informs Minny that she and Johnny had married because she got pregnant, but lost the baby a month later; she has also miscarried two other babies. During a charity event at which Hillys circle further mock her, Celia consumes only cocktails and has an unfortunate confrontation with Hilly which further ostracizes her from the popular-girls circle. At the same time, Skeeter has begun to withdraw from this same circle of her own volition despite meeting Stuart (Chris Lowell) through Hilly and beginning a relationship with him that delights Charlotte immensely.

Skeeter submits the draft book to a New York City editor with Harper & Row, Elaine Stein (Mary Steenburgen), who advises her that more maids stories need to be included, and that it has to happen quickly as the holidays are approaching and the newly developing Civil Rights Movement may be short-lived. A culmination of the Medgar Evers assassination, and Hilly having Minnys replacement arrested for stealing a diamond ring, brings forth more maids than Skeeter could have hoped would speak out, as the maids realize Skeeters book could give them an opportunity to make known what they experience in life.

What Skeeter, Aibileen, and Minny realize so late in their writing of the book is that some stories are very connected with particular maids and families, and begin to worry that they will be exposed. Their worries are heightened following the   to it, making it a point to keep Mrs. Walters away from it. Hilly ate two slices of the pie before Minny told her what she had put in it, causing Mrs. Walters to break out in a laughing fit, for which Hilly subsequently locked her away in a convalescent home. Minny predicts that putting the Terrible Awful into the book will keep the other maids safe from retribution, as Hilly would rather die than let it be known she was the subject of the Terrible Awful, and she will wield her social influence to convince anyone who will listen that the story did not take place in Jackson.

The book, published anonymously to protect Skeeter and her contributors identities, is a success, and the royalties are shared with the maids. When Stuart becomes aware of the books contents and feeling deceived, he splits up with her. Minny confesses about the Terrible Awful to Celia, who finally sees Hilly for the manipulative bully she is. Thinking that all is safe with the origins of the book, Hilly becomes enraged when a contribution from Celia to one of Hillys charitable works is made out to "Two Slice Hilly." Drunk, humiliated and spoiling for a fight, she drives to Skeeters home to confront Skeeter. When Hilly moves to storm into the house and tattle on Skeeters hippie ways to Charlotte, Charlotte appears on the porch, commenting on her haggard appearance and observing she must have been eating too much pie, implying she knows exactly who the Terrible Awful was written about without Skeeter saying a word, then kicks her off the property.

After Hillys departure, Charlotte and Skeeter are able to reconcile, with Charlotte voicing her strong-spirited and independent daughter really is quite extraordinary and should be admired, especially when she takes a phone call intended for Skeeter from Elaine about being offered a publishing job and Charlotte offers to help Skeeter prepare for this life-changing move from Jackson to Manhattan.

Celia thinks that she has adequately deceived her husband about bringing in Minny to help her manage the housekeeping; however, when Johnny comes up the drive while Minny is walking to the house with groceries, Minny thinks he will be furious with her, and she runs terrified toward the house. He is able to catch up with her on foot and reveals that not only has he known Minny has been working there the whole time, but that he also learned of Celias multiple miscarriages and that once Minny arrived, Celias health began to improve, a grace for which he is profoundly grateful. Johnny helps Minny take her bags to the house, where she is surprised by a table full of food Celia has prepared entirely herself as a result of Minnys cooking lessons. Both Johnny and Celia inform Minny she has a job with them for as long as she wants. This kindness gives Minny the courage to leave her abusive husband, and she takes her children to live with the Footes.

In conclusion Hilly is back to her old ways: knowing she cannot have Aibileen imprisoned for her writings without exposing herself, she vindictively intends instead to frame her for the theft of some loaned silver cutlery. Aibileen attempts to hold her ground timidly at first while Elizabeth attempts to let the issue pass. Hilly presses the issue to the point where she fires Elizabeths help; for Aibileen this is the turning point, as she has had enough of Hillys hideous selfishness and blindly arrogant attempts to control everyone around her, and denounces her as a godless vindictive woman never at peace. A departing Aibileen reassures Elizabeths distraught daughter Mae Mobley with the creed that she has said to all her charges, then, compelled by Elizabeth, leaves for a new life, reflecting on how she may possibly become a writer herself.

==Cast==
 
*Viola Davis as Aibileen Clark, a black maid and Skeeters good friend. She also narrates the story.
*Emma Stone as Eugenia "Skeeter" Phelan, a college graduate and aspiring writer.
*Octavia Spencer as Minny Jackson, a brilliant cook, who has been fired numerous times for not holding her tongue; she is Aibileens best friend.
*Jessica Chastain as Celia Foote, Minnys naive but kind employer and Johnnys wife.
*Bryce Dallas Howard as Hilly Holbrook, the racist, mean and spiteful queen bee of the towns women.
*Allison Janney as Charlotte Phelan, Skeeters mother.
*Ahna OReilly as Elizabeth Leefolt, Aibileens employer and the films secondary antagonist. She neglects her daughter, Mae.
*Chris Lowell as Stuart Whitworth, Skeeters boyfriend and a senators son.
*Cicely Tyson as Constantine Bates, Skeeters beloved childhood maid.
*Mike Vogel as Johnny Foote, Hillys ex-boyfriend and Celias husband.
*Sissy Spacek as Mrs. Walters, Hillys mother.
*Anna Camp as Jolene French, a friend of Hilly and Elizabeth.
*Brian Kerwin as Robert Phelan, Skeeters father
*Aunjanue Ellis as Yule May Davis, a maid fired by Hilly for pawning a ring she stole from the Holbrook house to pay for her twin sons college tuition, after Hilly refused to lend her $75 to do so.
*Emma and Eleanor Henry as Mae Mobley Leefolt, Elizabeths baby.
*Ted Welch as William Holbrook, Hillys husband.
*LaChanze as Rachel Bates, daughter of Constantine.
*Mary Steenburgen as Elaine Stein, an editor for Harper & Row.
*Leslie Jordan as Mr. Blackly
*Nelsan Ellis as Henry, the waiter
*Wes Chatham as Carlton Phelan, Skeeters brother.
*Tiffany Brouwer as Rebecca, Carltons fiance.
*Kelsey Scot as Sugar Jackson, Minnys daughter.
*David Oyelowo as Preacher Green
*Dana Ivey as Grace Higginbotham
*Ashley Johnson as Mary Beth Caldwell
 

==Production== Chris Columbus, Michael Barnathan, and Mark Radcliffe would produce a film adaptation of The Help, under their production company 1492 Pictures. Brunson Green of Harbinger Productions also co-produced. The film was written and directed by Stocketts childhood friend, Tate Taylor, who optioned film rights to the book before its publication.   His 1492 Prods. Readies Adaptation of Stockett Novel|date=December 15, 2009|work=Variety (magazine)|Variety|accessdate=July 7, 2012}} 

The first casting news for the production came in March 2010, was reported that Stone was attached to play the role of Skeeter Phelan.    Other actors were since cast, including Davis as Aibileen; Howard as Hilly Holbrook, Jacksons snooty town ringleader; Janney as Charlotte Phelan, Skeeters mother; and Lowell as Stuart Whitworth, Skeeters boyfriend and a senators son.            Leslie Jordan appears as the editor of the fictional local newspaper, The Jackson Journal.  Mike Vogel plays the character Johnny Foote. Octavia Spencer portrays Minny. A longtime friend of Stockett and Taylor, Spencer inspired the character of Minny in Stocketts novel and voiced her in the audiobook version.  

Filming began in July 2010 and extended through October. The town of   and the Mayflower Cafe downtown. Scenes set at the Jackson Journal office were shot in Clarksdale at the building which formerly housed the Clarksdale Press Register for 40 years until April 2010. 

The Help was the most significant film production in Mississippi since O Brother, Where Art Thou? (2000)            "Honestly, my heart would be broken if it were set anywhere but Mississippi", Stockett wrote in an e-mail to reporters. In order to convince producers to shoot in Greenwood, Tate Taylor and others had previously come to the town and scouted out locations; at his first meeting with DreamWorks executives, he presented them with a photo album of potential filming spots in the area. The states tax incentive program for filmmakers was also a key enticement in the decision.   

==Release==
On October 13, 2010, Walt Disney Studios Motion Pictures gave the film a release date of August 12, 2011. On June 30, 2011, the films release date was rescheduled two days earlier to August 10, 2011. 

===Home media===
The film was released by  . The digital download version includes the same features as the DVD version, plus one additional deleted scene. Both the two-disc and three-disc combo packs include the same features as the DVD version, as well as "The Making of The Help: From Friendship to Film", "In Their Own Words: A Tribute to the Maids of Mississippi", and three deleted scenes with introductions by director Taylor.         

==Reception==
===Critical response===
The Help received mostly positive reviews from critics. Review aggregator Rotten Tomatoes reported that 76% of 202 professional critics gave the film a positive review, with an average score of 7.0 out of 10. The sites consensus states, "Though arguably guilty of glossing over its racial themes, The Help rises on the strength of its cast—particularly Viola Davis, whose performance is powerful enough to carry the film on its own."  Metacritic, a review aggregator which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 62 based on 41 reviews.  CinemaScore polls reported that the average grade moviegoers gave the film was an A+ on an A+ to F scale. 

Tom Long from   gave the film three out of four stars and said it "will make you laugh, yes, but it can also break your heart. In the dog days of August moviegoing, thats a powerful recommendation." 

A more mixed review from Karina Longworth of  , giving the film two out of four stars, said: "Typically, this sort of film is an earnest tear-jerker with moments of levity. Instead, what we have here is a raucous rib-tickler with occasional pauses for a little dramatic relief."  Referring to the film as a "big, ole slab of honey-glazed hokum", The New York Times noted that "save for Ms. Daviss, however, the performances are almost all overly broad, sometimes excruciatingly so, characterized by loud laughs, bugging eyes and pumping limbs." 

Some of the negative reviews criticized the film for its inability to match the quality of the book. Chris Hewitt of the St. Paul Pioneer Press said about the film: "Some adaptations find a fresh, cinematic way to convey a books spirit but The Help doesnt." 
 New York magazine commented that, "The Help belongs to Viola Davis." 

Ida E. Jones, the national director of the  . Retrieved November 1, 2011. 

Roxane Gay of literary web magazine  . Retrieved November 1, 2011. 

===Box office===
The Help earned $169,708,112 in North America and $46,931,000 in other territories for a worldwide total of $216,639,112. 

In North America, on its opening day (Wednesday, August 10, 2011), it topped the box office with $5.54 million. It then added $4.33 million on Thursday, declining only 21 percent, a two-day total to $9.87 million.  On its first weekend, the film grossed $26.0 million, coming in second place behind Rise of the Planet of the Apes.  However, during its second weekend, the film jumped to first place with $20.0 million, declining only 23 percent, the smallest drop among films playing nationwide.  The film crossed the $100 million mark on its 21st day of release, becoming one of only two titles in August 2011 that achieved this.  On its fourth weekend (Labor Day three-day weekend), it became the first film since Inception (2010), to top the box-office charts for three consecutive weekends.   Its four-day weekend haul of $19.9 million  was the fourth largest for a Labor-day weekend.  Notably, The Help topped the box office for 25 days in a row. This was the longest uninterrupted streak since The Sixth Sense (35 days), which was also a late summer release, in 1999. 

To promote the film, TakePart.com hosted a series of three writing contests.  Rebecca Lubin, of Mill Valley, California, who has been a nanny for nearly two decades won the recipe contest. Darcy Pattisons "11 Ways to Ruin a Photograph" won "The Help" Childrens Story Contest with her story about a tenacious young girl who refuses to take a good photograph while her father is away "soldiering". After being chosen by guest judge and childrens-book author Lou Berger, the story was professionally illustrated.  The final contest was about "someone who inspired you". Genoveva Islas-Hooker charmed guest judge Doc Hendley (founder of Wine to Water) with her story, A Heroine Named Confidential. A case manager for patients with HIV, Islas-Hooker was consistently inspired by one special individual who never gave up the fight to live.

===Accolades===
 

==Soundtrack==
{{Infobox album   Name       = The Help: Music from the Motion Picture Type       = Soundtrack Artist     = Various artists Cover      = Released   = July 26, 2011 Recorded   = Genre      = Blues, Soul music|soul, rhythm and blues, rock and roll Length     = Label  Geffen
|Chronology = Last album = This album = The Help (2011) Next album =
}}

The original song is "The Living Proof" by Mary J. Blige.  The soundtrack was released on July 26, 2011, through Geffen Records. 

{{Track listing extra_column = Performers headline = Track listing title1   = The Living Proof length1  = 5:57 extra1   = Mary J. Blige title2  Jackson
|length2  = 5:28 extra2   = Johnny Cash and June Carter title3  Sherry
|length3  = 5:35 extra3   = Frankie Valli title4   = I Aint Never length4  = 1:56 extra4   = Webb Pierce title5   = Victory Is Mine length5  = 3:47 extra5   = Dorothy Norwood title6  Road Runner length6  = 2:48 extra6   = Bo Diddley title7   = Hallelujah I Love Her So length7  = 2:35 extra7   = Ray Charles title8   = The Wah-Watusi length8  = 2:32 extra8   = The Orlons title9  Personality
|length9  = 10:29 extra9   = Lloyd Price title10  = Dont Think Twice, Its All Right length10 = 3:38 extra10  = Bob Dylan title11  = Lets Twist Again length11 = 2:19 extra11  = Chubby Checker title12  = Dont Knock length12 = 2:30 extra12  = Mavis Staples
}}

==See also==
*White savior narrative in film

==References==
 

==External links==
 
* 
* 
* 
* 
* , The Helps filming locations and associated civil-rights history award

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 