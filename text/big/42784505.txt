Hungry Ghost Ritual
 
 
{{Infobox film
| name           = Hungry Ghost Ritual
| image          = Hungry Ghost Rituals poster.jpg
| image size     =
| border         =
| alt            =
| caption        =
| director       = Nick Cheung
| producer       = Adrian Teh
| writer         =
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       = Nick Cheung Annie Liu Carrie Ng Cathryn Lee Lam Wai
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = Clover Films Golden Village Pictures
| released       =  
| runtime        = 82 minutes
| country        = Hong Kong Malaysia
| language       = Cantonese
| budget         =
| gross          = US$1,428,745 
}}
 horror thriller film directed by Nick Cheung.  The film was released on 10 July 2014 in Hong Kong and Malaysia. 

==Synopsis==
After incurring debts from his failed business venture in China, Zong Hua (Cheung) returns to Malaysia after a decade hiatus. The demoralised Zong Hua faces problem finding a job and tries hard to get use to things at home, including his estranged relationship with his step-father, Xiaotian, who runs a Cantonese opera troupe and half-sister, Jing Jing (Cathryn Lee). Jing Jing is hostile towards Zong Hua as she always has this impression that the death of their mother is caused by the excessive fights between Zong Hua and his step-father.

==Cast==
* Nick Cheung
* Annie Liu
* Carrie Ng
* Cathryn Lee
* Lam Wai

==References==
 

==External links==
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 