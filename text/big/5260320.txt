A Kid for Two Farthings (film)
 
 
{{Infobox film
|  name           = A Kid For Two Farthings
|  image          = akidfortwofarthings.jpg
|  caption        = DVD Cover Art
|  director       = Carol Reed
|  producer       = Carol Reed
|  writer         = Wolf Mankowitz Joe Robinson
|  music          = Benjamin Frankel |
|  cinematography = Edward Scaife
|  editing        = Bert Bates
|  distributor    = London Films
|  released       = 15 August 1955
|  runtime        = 96 minutes
|  language       = English
|  country        = United Kingdom
|}}
 novel of the same name.

==Plot== East End everyone, it seems, has unattainable dreams. Then a small boy - Joe - buys a unicorn, in fact a sickly little goat, with just one twisted horn in the middle of its forehead. This, he has been led to believe by a local tailor, Kandinsky, will bring everyone good fortune.

The film has a haunting last image, of Kandinsky carrying the tiny body of the "unicorn" to the graveyard, whilst passing in the opposite direction is a Torah-reading Rabbi pushing a horn phonograph|gramophone, a character that appears in the background several times during the film.

==Cast==
* Celia Johnson as Joanna
* Diana Dors as Sonia
* David Kossoff as Mr. Kandinsky Joe Robinson as Sam
* Jonathan Ashmore as Joe
* Brenda De Banzie as Lady Ruby
* Primo Carnera as Python Macklin
* Lou Jacobi as Blackie Isaacs
* Irene Handl as Mrs. Abramowitz Danny Green as Bason
* Sydney Tafler as Madame Rita
* Sid James as Ice Berg
* Daphne Anderson as Dora Harry Baird as Jamaica
* Joseph Tomelty as Vagrant
* Harold Berens as Oliver

==Reception==
A Kid for Two Farthings was nominated for a Golden Palm at the 1955 Cannes Film Festival.   

Critically, this was one of Carol Reeds least successful films, however the rich ensemble cast, and the interweaving of harsh reality and fantasy remain a potent mix. The character of Kandinsky in particular is seen to embody the plight of surviving European Jews ten years after the Second World War. His mythologizing about a race of unicorns with magic powers that were destroyed everywhere but may still exist in some far-off country, can be seen as analogous to the The Holocaust|Holocaust.

The film was the 9th most popular movie at the British box office in 1955. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 