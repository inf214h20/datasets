Looking for an Echo
 
{{Infobox film
| name                    = Looking for an echo
| image                   =
| caption                 =
| director                = Martin Davidson
| producer                = Paul Kurta Martin Davidson Jeffrey Goldenberg Robert Held Martin Davidson
| starring                = Armand Assante Diane Venora Edoardo Ballerini
| music                   = Phillip Namanworth Kenny Vance	 	
| cinematography          = Charles Minsky
| editing                 = Jerrold L. Ludwig
| distributor             = Regent Releasing Arthur Kananack & Associates Regent Entertainment  Screen Media Films
| released                =
| runtime                 = 97 min.
| country                 = USA English
}}
Looking for an Echo is  2000 independent drama film.

==Plot==
The lead singer of an oldies group reminisces about the good ol days and a potential comeback.

==Cast==
*Armand Assante – Vince Vinnie Pirelli
*Diane Venora – Joanne Delgado Tom Mason – Augustus Augie MacAnnally III
*Anthony John Denison – Ray Nappy Napolitano (as Tony Denison) Johnny Williams – Phil Pooch Puccirelli
*Edoardo Ballerini – Anthony Pirelli
*Christy Carlson Romano – Tina Pirelli
*David Vadim – Tommie Pirelli
*Monica Trombetta – Francine Pirelli
*David Margulies – Dr. Ludwig
*Paz de la Huerta – Nicole Delgado (as Paz De La Huerta)
*Ilana Levine – Sandi (Vics Date)
*Gena Scriva – Arlene (Blonde at Bar)
*Gayle Scott – Renee (Brunette at Bar)
*Cleveland Still – Singer on Bus
*Peter Jacobson – Marty Pearlstein (Backstage Agent)
*Murray Weinstock – Orchid Blue Lead / Vocals for The Dreamers
*Amanda Homi – Orchid Blue Singer
*Machan Notarile – Orchid Blue Singer (as Machun)
*Michael Cooke Kendrick – Jason (Bar Mitzvah Boy) (as Michael Cooke)
*Kresimir Novakovic – Waiter at Bar
*Uri Teddy Dallal – Public Access Drums
*Jorge Pequero – Public Access Bass
*Eva Giangi – Public Access Keyboard
*Lisa France – Night Nurse
*Fanni Green – Day Nurse
*Alesandra Asante – Neighborhood Girl
*Rick Faugno – Young Vinnie
*Johnny Giacalone – Young Vic
*Eric Meyersfield – Young Nappy
*Tommy J. Michaels – Young Augie (as Tommy Michaels)
*Danny Gerard – Young Pooch
*Kenny Vance – Vocals for Vince (voice)
*Norbert Leo Butz – Vocals for Anthony (voice) (as Norbert Butz)
*Eddie Hokenson – Vocals for The Dreamers (voice)
*Vinny DeGennaro – Vinny (uncredited)
*Joe Grifasi – Vic (uncredited)
*Dolores Sirianni – Garyn (uncredited)

==External links==
* 

 

 
 
 
 
 
 
 