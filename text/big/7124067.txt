Vampyres (film)
{{Infobox film
| name           = Vampyres
| image          = Vampyresfilmposter.jpg
| caption        = 
| director       = José Ramón Larraz
| producer       = Brian Smedley-Aston
| writer         = D. Daubeney Thomas Owen José Ramón Larraz (uncredited) Anulka Marianne Morris Murray Brown
| music          = James Clark
| cinematography = Harry Waxman
| editing        = Geoff R. Brown
| studio         = Lurco Films
| distributor    = Cambist Films  (USA, theatrical)  Cinépix Film Properties Inc.  (Canada, theatrical)  1974
| runtime        = 87 min.
| country        = United Kingdom English
| budget         =
}} British erotic film|erotic/lesbian vampire horror film directed by José Ramón Larraz. It was filmed on location in England.

== Synopsis ==
Two beautiful women, Fran (Marianne Morris) and Miriam (Anulka Dziubinska) roam the English countryside. They lure unsuspecting men to their estate for orgies of sex and blood. But when an innocent young couple John and Harriett (Brian Deacon and Sally Faulkner)  stumble into the vampires lair, they find themselves sucked into an unforgettable vortex of savage lust and forbidden desires.

== Cast ==
* Marianne Morris as Fran
* Anulka Dziubinska as Miriam
* Murray Brown as Ted
* Brian Deacon as John
* Sally Faulkner as Harriet Michael Byrne as Playboy
* Karl Lanchbury as Rupert
* Margaret Heald as Receptionist
* Gerald Case as Estate Agent
* Bessie Love as American Lady
* Elliott Sullivan as American Man

== Production == Denham churchyard) at break of dawn.

Anulka had been Playboys Miss May 1973, whilst Marianne Morris appeared naked in the October 1976 edition of British mens magazine Mayfair (magazine)|Mayfair.

== Legacy ==
A novelisation was published in 2001 by Tim Greaves, a fan of the film. 

== Release ==
Though initially heavily censored in the UK, it is now available uncut on DVD, the uncut Blu-Ray was released on 30 March 2010, include commentary with director José Ramón Larraz and producer Brian Smedley-Aston, interviews with stars Marianne Morris and Anulka, the international trailer, and the U.S. trailer. 

=== Alternate titles ===
*Blood Hunger
*Daughters of Dracula (USA)
*Satans Daughters
*Vampyres, Daughters of Dracula
*Vampyres: Daughters of Darkness (USA)

==See also==
*Vampire film

== References ==
 

== Bibliography ==
*  

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 