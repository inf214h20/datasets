Mixed Company
{{Infobox film name           = Mixed Company image          = Mixedcompany1974.jpg caption        = director       = Melville Shavelson writer         = Mort Lachman Melville Shavelson starring  Barbara Harris Joseph Bologna producer       = Melville Shavelson Llenroc Productions  (company)  music          = Fred Karlin cinematography = Stan Lazan editing        = Ralph James Hall Walter Thompson	  distributor    = United Artists released       =   runtime        = 109 min. country        = United States budget         = language       = English
}} 1974 comedy-drama Barbara Harris, Joseph Bologna, and Haywood Nelson.

==Plot summary==
Kathy Morrison (Harris), mother of three, who helps run a "color-blind" adoption program, wants to have another biological child.  Her husband, Pete (Bologna), the head coach of the Phoenix Suns, finds out he cant produce another child.  Kathy thinks about adopting a boy, Frederic "Freddie" Wilcox, and Pete does not want to adopt a boy who happens to be black.  When he relents, Freddies arrival causes an upheaval in the Morrisons neighborhood, their school, and family.  Kathys answer is to adopt another child, in this case two, a war-traumatized half-Vietnamese girl, Quan Tran, and a Hopi boy, Joe.  The new extended family must now learn to live together.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 