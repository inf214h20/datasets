Stolen Identity (film)
{{Infobox film
| name           = Stolen Identity
| image          = Stolen Identity film poster.jpg
| caption              = Theatrical release poster
| director             =  Gunther von Fritsch
| producer           = Turhan Bey Robert Hill
| story                  = Alexander Lernet-Holenia (novel Ich war Jack Mortimer)
| starring              = Donald Buka Joan Camden Francis Lederer
| music                 = Richard Hageman
| cinematography = Helmut Ashley
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Austria
| language       = English
| budget         = 
| gross          = 
}}

Stolen Identity is a 1953 Austrian film directed by Gunther von Fritsch, starring Donald Buka, Joan Camden and Francis Lederer . 

The film is the English-language version of the film   (1952), directed by Emil-Edwin Reinert, starring Gustav Fröhlich and Cornell Borchers. Besides the two leading roles the cast of both films is essentially the same.

==Plot==
Vienna taxi driver Toni Sponer dreams of going to the US. One day, an American businessman is waiting for his cab, when jealous concert pianist Claude Manelli (Lederer) shoots him dead because he suspected him of having an affair with his American wife Karen (Camden). Toni grabs the dead mans papers and takes over his identity. Later he falls in love with Karen who initially thinks that Toni is the killer but he is able to convince her that he is innocent. Together they try to flee to America with Karens husband in hot pursuit. Both men are finally captured by the police but Toni receives only a small sentence of a few months in prison. Karen decides to wait for him.

==Cast==	 
* Donald Buka as Toni Sponer 
* Joan Camden as Karen Manelli 
* Francis Lederer as Claude Manelli 
* Adrienne Gessner as Mrs. Fraser 
* Inge Konradi as Marie 
* Hermann Erhardt as Inspector 
* Egon von Jordan as Kruger

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 