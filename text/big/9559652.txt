Savi Savi Nenapu
{{Infobox Film
| name = Savi Savi Nenapu
| image = Savisavinenapu.jpg
| caption = 
| director = Santosh Rai Pathaje
| writer = Santosh Rai Pathaje
| producer = S V Babu Prem Kumar Mallika Kapoor Tejaswini Prakash
| music = R P Patnaik
| cinematography = Chandrashekhar
| editing = Suresh Urs
| released =  2007
| runtime =  
| language = Kannada
| lyrics =  nagatihalli chandrashekar, K. Kalyan
| Photography = 
| country = India
}}
 Prem and Mallika Kapoor in the lead roles amongst others. The film was released in 2007.

==Story==
Prem (played by Prem Kumar) passionately loves his classmate Preethi (Mallika Kapoor) and they get married. But destiny turns cruel towards them and Preethi passes away. Preethis heart is transplanted to Pallavi (Tejaswini) who is also facing marital problems, as her husband is very career bound as a Model and is not able to keep Pallavi Happy. There is also a co-incidence that Pallavis husband is identical to Prem. Prem does not want Preethis heart to suffer and plans things in such a way that Pallavi remains happy. He impersonates himself as Pallavis husband and spend quality time with her - which she is expecting very badly.

There comes a point when Pallavis heart fails to adjust after transplantation and she has to be operated. Then Prem reveals about his impersonation to Pallavis husband and he understands that all was done to keep Pallavi happy. He promises Prem that from now onwards he shall take good care of Pallavi, and try to keep her happy in life.

R P Patnaik devised the music for this movie, including the title track titled "Saviyo Saviyo", rendered by Sonu Nigam and  Shreya Ghoshal and written by Nagatihalli Chandrashekar.

==Cast==
 Prem Kumar
*Mallika Kapoor
*Tejaswini Prakash

==References==
http://entertainment.oneindia.in/kannada/reviews/savi-savi-nenapu-review-110807.html

http://www.nowrunning.com/movie/reviews/moviereview.aspx?movie=3821

==External links==
*  

 
 
 


 