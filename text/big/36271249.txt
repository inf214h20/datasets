Si Agimat, si Enteng Kabisote at si Ako
{{Infobox film
| name     = Si Agimat, si Enteng Kabisote at si Ako
| image    = 
| caption  = 
| director = Tony Reyes
| starring = Bong Revilla Vic Sotto Judy Ann Santos
| studio   = Octoarts Films M-Zet Productions Imus Productions APT Entertainment GMA Films
| distributor = OctoArts Films
| released =  
| country  = Philippines
| language =   38th MMFF lobbying placement fee)   
| gross   = ₱133.5 Million  (as of January 8, 2013 - MMFF season)  ₱152 Million  (4 weeks) 
}}
 fantasy action film starring Vic Sotto, Bong Revilla, Jr. and Judy Ann Santos. The film is a crossover of Agimat and Enteng Kabisote film franchise. It is a joint production by five movie outfits, Octoarts Films, M-Zet Productions, Imus Productions, APT Entertainment and GMA Films. It is one of the 8 official entries for 2012 Metro Manila Film Festival. It was shown in December 25, 2012.

==Production==

jushua mendiola is the writer::
The movie was first announced in early June when it beat the deadline for the submission of scripts for the aspiring   of M-Zet Productions and Atty. Annette Gozon-Abrogar of   announced that the film is one of the 8 official entries for the film festival.  The Greater Manila Theater Operators Association selected the 8 film out of 14 through level of creativity, cultural or historical value, and commercial viability. 

===Filming=== Startalk on August 18, 2012, he revealed that they already completed 40% of the movie.  Revilla also revealed that they are the first movie to have a shoot at the "Kapurpurawan"(white) rock formations in Burgos, Ilocos Norte. 

==Cast==

===Main Cast===
*Bong Revilla, Jr. as Agimat
*Vic Sotto as Enteng Kabisote
*Judy Ann Santos as Angelina Kalinisan Orteza ("Si Ako")

===Supporting Cast===
*Gwen Zamora as Faye Kabisote
*Oyo Boy Sotto as  Benok Kabisote
*Aiza Seguerra as Aiza Kabisote 
*Mikylla Ramirez as Ada Kabisote
*Jose Manalo as Bodyguard Jose
*Wally Bayola as Bodyguard Boggart
*Ruby Rodriguez as Amy 
*Amy Perez as Ina Magenta
*Sam Pinto as Samara 
*Ryzza Mae Dizon as Chichay Sweet as Che/Pink Hulk
*Jolo Revilla as Makisig
*Jillian Ward as Bebeng
*Yassi Pressman as Sol/Winged Horse

===Extended Cast=== Jimmy Santos as Jimboy
*Jinri Park as Jim Girl
*TJ Trinidad as Byron 
*Alden Richards as Fino
*Zea Usi as Marian (Engkantada)
*Romano Rhumbert Bulatao as Kal El of Planet Krypton
*King Gutierrez as Upaw
*Edwin Reyes as Abawa
*Jeffrey Santos as Punk #1
*Rico Barrera as Punk #2
*Michael Conan as Punk #3
*Jun Cabatu as Cabatu
*Marc Pingris as Sakuragi
*Ronald Tubid as Tubid
*Thou Reyes as Batoktok
*Val Sotto as Coach Meg-B
*Princess Velasco as Queenie
*Nica Peralejo as Nica

===Special Guest===
*Sofia Aznar as Sofia
*Patrick Aznar as Patrick
*Rez Cortez  as Rez
*Kiray Celis  as Kiray
*Igiboy Flores as Igiboy
*Derrick Monasterio as Derrick
*Barbie Forteza as Barbie
*Joyce Ching as Joyce
*Anton Revilla as Anton
*Gian Sotto as Gian
*Dianne Medina as Dianne
*IC Mendoza as IC
*Wahoo Sotto as Wahoo
*Ryan Agoncillo as Ryan

==Reception==

===Critical===
In a review for Rappler.com, Carljoe Javier declared that the film did not surprise him other than the fact that it was extremely politically incorrect. He also pointed out that the movie had commercials inserted in it and that the storyline lacked a clear narrative. 

At ClickTheCity.com, Philbert Ortiz Dy declared "Si Agimat, Si Enteng, at Si Ako is utterly unnecessary. There are already several movies just like it in existence, ones with less blatant product placement and a less skeevy overall tone. The people involved in the picture seem to be producing it completely out of inertia at this point, just going through the motions for another year, hoping to lure in people based on name recognition alone." 

===Box Office=== Presidential sister Kris Aquinos Sisterakas at #1, which had P71,200,000 gross, earning the 1st place position for two consecutive days so far.

==See also==
*2012 Metro Manila Film Festival

==References==
 

 
 

 
 
 
 
 
 