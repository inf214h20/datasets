Aakhri Badla
{{Infobox film
 | name = Aakhri Badla
 | image = AakhriBadla89.jpg
 | caption = VCD Cover
 | director = Mangal Chakraborty
 | producer = Vishwanath Ghosh
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Yogita Bali Prem Chopra Tom Alter Jayshree T. Shiva Rindani
 | music = Salil Chowdhury
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 1989
 | runtime = 135 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1989 Hindi Indian feature directed by Mangal Chakraborty, starring Mithun Chakraborty, Yogita Bali, Prem Chopra, Tom Alter, Jayshree T. and Shiva Rindani.

==Summary==
Kama Kazi is an international smuggling gang. When the Interpol is about to raid the gang, they moves all its wealth in form of gold and silver diamonds by ship, but the ship sinks in a cyclone. An Indian company is subcontracted to salvage the sunken ship and the Indian Intelligence sends its ace agent Himadri Choudhari, and his sidekick Mantu Ghosh along with a Japanese karate master Kitahara.

==Cast==
*Mithun Chakraborty
*Yogita Bali
*Prem Chopra
*Tom Alter
*Jayshree T.
*Shiva Rindani

==References==
* http://www.bollywoodhungama.com/movies/cast/5386/index.html
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Aakhri+Badla -
* http://www.theindiabazaar.com/images/P/aakhribadla_mvcd_246x250.jpg

==External links==
*  

 
 
 
 

 