The Garage (1920 film)
{{Infobox film
| name           = The Garage
| image          = Thegarage-newspaperad-1920.jpg
| image size     =
| caption        = Newspaper advertisement for the film. Fatty Arbuckle
| producer       =
| writer         = Jean C. Havez
| narrator       = Fatty Arbuckle Buster Keaton
| music          =
| cinematography = Elgin Lessley
| editing        =
| distributor    =
| released       = January 11, 1920 {{cite book
|last=Knopf
|first=Robert
|title=The theater and cinema of Buster Keaton
|url=http://books.google.com/books?id=fU5qDx7tawIC&pg=PA181
|accessdate=21 October 2010
|date=2 August 1999
|publisher=Princeton University Press
|isbn=978-0-691-00442-6
|page=181}} 
| runtime        = 25 minutes
| country        = United States Silent (English intertitles)
| budget         =
| preceded by    =
| followed by    =
}}
 
The Garage is a 1920 American short comedy film starring Buster Keaton and Roscoe "Fatty" Arbuckle. It was directed by Arbuckle himself. The film was also known as Fire Chief. This was the fourteenth film starring the duo. The film also stars Luke the Dog who starred in many other short comedies starring Keaton and Arbuckle.

==Plot==
Two men who work as both automobile mechanics and firemen operate a garage in a fire station. A car has been left for them to clean, but they destroy it instead. In the second half of the film, Roscoe and Keaton have been called to a fire, but it turns out to be a false alarm. When they return, they find their own fire station on fire.

The film is available on DVD, as part of the "Arbuckle and Keaton Collection".

==Cast== Roscoe Fatty Arbuckle - Mechanic / Fireman
* Buster Keaton - Mechanic / Fireman Molly Malone - Garage Owners Daughter
* Harry McCoy
* Dan Crimmins - (as Daniel Crimmins)
* Luke the Dog - The Mad Dog
* Alice Lake - Undetermined Role (uncredited) (unconfirmed)

==Product Placement==
The favorable review of this movie by the weekly trade publication Harrisons Reports was followed by the statement

: "Exhibitors of Los Angeles might ask Mr. Arbuckle how much he received for advertising Red Crown gasoline, handled by almost every Oil Station in their city.  The trade mark of that product appears in numerous scenes on the portable gasoline pump.  If he states it was an oversight, it would be well to caution him to avoid such oversights in the future." 

Brand name product placement in movies may have occurred before the 1920s, but this is the earliest movie cited by Harrisons Reports for that practice. For the next four decades, Harrisons Reports frequently denounced product placement.

==See also==
 
* List of American films of 1920
* Fatty Arbuckle filmography
* Buster Keaton filmography
* List of firefighting films

==References==
 
 
 

==External links==
*  
*   on YouTube

 
 
 
 
 
 
 
 
 
 
 


 