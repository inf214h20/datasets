S.O.S. Mulheres ao Mar
{{Infobox film
| name           = S.O.S Mulheres ao Mar
| image          = S.O.S_Mulheres_ao_Mar!_Poster.jpg
| caption        = Theatrical release poster
| director       = Cris DAmato
| producer       = 
| screenplay     = Sylvio Gonçalves  Rodrigo Nogueira   Marcelo Saback
| based on       = 
| starring       = Giovanna Antonelli  Reynaldo Gianecchini  Fabíula Nascimento  Marcelo Airoldi Thalita Carauta
| music          = 
| cinematography = 
| editing        = 
| studio         = Globo Filmes Ananã Miravista
| distributor    = Buena Vista International
| released       =  
| runtime        = 
| country        = Brazil
| language       = Portuguese
| budget         =
| gross          = 
}}

S.O.S. Mulheres ao Mar is a 2014 Brazilian comedy film directed by Cris DAmato starring Giovanna Antonelli, Reynaldo Gianecchini, Fabíula Nascimento, Thalita Carauta, Marcelo Airoldi and Emanuelle Araújo. 

The film follows the story of Adriana, which disappointed with the end of her marriage, decides to win back her ex-husband embarking on the same cruise that he is with his new girlfriend, a soap opera star. The film was shot almost entirely on an ocean liner, but also had scenes shot in Venice. 

==Plot==
Adriana (Giovanna Antonelli) embarks on a cruise decided to win back her ex-husband Eduardo (Marcelo Airoldi) which is with a new girlfriend, Beatriz (Emanuelle Araújo), a TV star. Adriana takes her sister Luiza (Fabíula Nascimento) and the maid Dialinda (Thalita Carauta) encouraged by the book "SOS-Saving a dream" to ruining the trip of her former boyfriend. However, during the trip, they will meet new people and discover new ways and solutions to their lives. 

==Cast==
* Giovanna Antonelli as Adriana
* Fabíula Nascimento as Luiza
* Thalita Carauta as Dialinda
* Reynaldo Gianecchini as André
* Marcello Airoldi as Eduardo
* Emanuelle Araújo as Beatriz
* Theresa Amayo as Sonia
* Sérgio Muniz as Franco
* Carmine Signorelli as Giorgio
* Flávio Galvão as Andrés father

==References==
 

==External links==
*  

 
 
 
 
 
 

 