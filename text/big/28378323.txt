Nagina (film)
 
 
 
{{Infobox film
| name           = Nagina
| image          = Naginanew.jpg
| caption          = VCD Cover
| director       = Harmesh Malhotra
| writer         = Dr. Achala Nagar
| screenplay     = Ravi Kapoor
| story          = Jagmohan Kapoor
| producer       = Harmesh Malhotra
| cinematography = V. Durga Prasad
| editing        = Govind Dalwadi
| studio         = Emkay Enterprises
| distributor    = Emkay Enterprises
| starring       = Sridevi Rishi Kapoor Amrish Puri Prem Chopra
| music          = Laxmikant-Pyarelal
| country        = India
| language       = Hindi
| released       = 28 November 1986
|}}
Nagina is 1986 fantasy Hindi film, produced and directed by Harmesh Malhotra, written by Jagmohan Kapoor, and starring Sridevi and Rishi Kapoor in lead roles. This was the second women-centric commercial film of actress Sridevi after Tohfa.

==Plot==
 snake who has married Rajiv to avenge the death of her spouse during Rajivs childhood. To remove her from the household, Bhairo and his disciples do a snake dance song, forcing Rajni to dance for them as she is a naga. However, when Rajiv comes into the house, Rajni escapes, only to be caught by Bhairo again, who reveals his sinister plot, to control the world with the Mani, a sacred jewel that only Rajni knows is hidden. However, Rajiv comes and engages in a fight with the sadhu. The sadhu dies, and Rajiv and Rajni live happily ever after.
==Reception==

According trade Taran ardarsh Nagina turned out to be the biggest blockbuster of the year   with Box Office India stating that Sridevi remained "the undisputed No.1".    The movie was widely appreciated for its screenplay, dialogues and direction.  Named one of the best snake fantasy films by Yahoo,  Times of India ranked Nagina as one of the Top 10 Snake Films of Hindi Cinema.  Sridevis climax dance number Main Teri Dushman also remains one of the best snake dances in Bollywood  with Desi Hits calling it "one of Sridevis most iconic dance numbers...that still gives fans goose bumps"  and iDiva describing it as "the stuff of movie legends".   

In 2013, Sridevi was given the Filmfare Special Award for her performances in Nagina as well as Mr. India (1987 film)|Mr. India (1987) to recognise her work at that time.

==Cast==
* Sridevi
* Rishi Kapoor
* Amrish Puri
* Prem Chopra

==Sequel==
This movie was followed by a sequel in 1989 called  . 

==Music==
The films music was provided by Laxmikant-Pyarelal, with lyrics by Anand Bakshi, and had classic songs like, Main Teri Dushman. 

===Tracklist===
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tu Ne Bechain Itna Ziada Kiya"
| Mohammed Aziz, Anuradha Paudwal
|-
| 2
| "Main Teri Dushman, Dushman Tu Mera"
| Lata Mangeshkar
|-
| 3
| "Balma Tum Balma Ho Mere Khali Naam Ke"
| Kavita Krishnamurthy
|-
| 4
| "Bhooli Bisri Ek Kahani"
| Anuradha Paudwal
|-
| 5
| "Aaj Kal Yaad Kuch Aur Rehta Nahin" Mohammed Aziz
|-
|}

==References==
 

==External links==
*  

 
 
 
 