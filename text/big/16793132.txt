The Courage of Marge O'Doone
 
{{Infobox film
| name           = The Courage of Marge ODoone
| image          = The Courage of Marge ODoone (1920) - Ad 2.jpg
| caption        = Advert for film
| director       = David Smith
| producer       =
| writer         = Robert N. Bradbury James Oliver Curwood (novel)
| starring       = Pauline Starke Niles Welch
| music          =
| cinematography =
| studio         = Vitagraph Company of America
| distributor    =
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}

The Courage of Marge ODoone is a 1920 American silent drama film directed by David Smith and featuring Boris Karloff.    The film is considered to be lost film|lost.   

==Cast==
* Pauline Starke - Marge ODoone
* Niles Welch - David Raine
* George Stanley - Michael ODoone Jack Curtis - Brokaw William Dyer - Hauck
* Boris Karloff - Tavish
* Billie Bennett - Margaret ODoone James ONeill - Jukoki
* Vincente Howard
* Jeanne Carpenter

==See also==
* List of American films of 1920
* Boris Karloff filmography

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 

 