Missing You...
{{Infobox film name = Missing You... image = border = yes caption = director =  Lin Kun Hui producer =  writer =  starring = Dasmond Koh Joshua Ang Hsu Chiung Fang Christina Lim Ah Nan music = cinematography = editing = studio =  distributor = InnoForm Media  released = 2008 (Singapore) runtime =  77 mins country = Singapore language = Mandarin
|budget =
}} romance cum drama film about the highs and lows of the Singaporean getai trade.  Directed by Lin Kun Hui, the film stars Joshua Ang, Christina Lim, Dasmond Koh, Hsu Chiung Fang and Ah Nan. 

==Plot==
When Zhen (Christina Lim) was young, her grandmother used to bring her to getai shows hosted by getai icon Fang (Hsu Chiung Fang), who now faces spouse abuse behind the glorious front. Zhen grew up to be a pretty and kind hearted lady who displayed an aloof attitude towards men. With a passion in singing, her childhood dream was to perform for her grandmother at a getai show. Qiang (Joshua Ang), a blue collar worker who washes cars for a living likes Zhen but feels inferior because of his livelihood. Oblivious to Qiangs pursuit, Zhen falls for Simon but being a Casanova, he soon leaves Zhen for another woman. Thereafter, Qiang realized that he suffers from a terminal illness but perseveres to fulfill Zhens wish of singing at the getai show and ends up sacrificing his own life... 

==Cast==
*Dasmond Koh as Simon
*Joshua Ang as Qiang
*Christina Lim as Zhen

==References==
 

 
 
 