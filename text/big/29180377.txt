Fear in the Night (1972 film)
 
 
 
{{Infobox film
| name           = Fear in the Night
| image          = Fear in the Night poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jimmy Sangster
| producer       = Jimmy Sangster
| writer         = Jimmy Sangster Michael Syson
| starring       = Judy Geeson Joan Collins Peter Cushing
| music          = John McCabe
| cinematography = Arthur Grant
| editing        = Peter Weatherly
| studio         = Hammer Film Productions
| released       = 9 July 1972
| runtime        = 94 min.
| country        = United Kingdom
| language       = English
| budget = ₤141,000 Tom Johnson and Deborah Del Vecchio, Hammer Films: An Exhaustive Filmography, McFarland, 1996 p355 
}}
 double bill with Demons of the Mind. 

== Plot ==
 
The film follows a young woman who goes to take up a new position working in a boys boarding school. She soon begins to believe she is losing her mind when she starts being terrorized by a one-armed man.

== Cast ==

* Judy Geeson as Peggy Heller
* Joan Collins as Molly Carmichael
* Peter Cushing as Michael Carmichael
* Ralph Bates as Robert Heller
* James Cossins as The Doctor
* Gillian Lind as Mrs. Beamish
* John Bown as 1st Policeman
* Brian Grellis as 2nd Policeman

== Background ==

Fear in the Night derived from a script written by Jimmy Sangster called Brainstorm that was originally developed for Universal Pictures in 1963.  The film had been scheduled to go into production several times: first in Autumn 1964, then "tentatively" in 1965.  In 1967 he retitled the film The Claw.  It was not until 1971 that the script was altered by Sangster and co-writer Michael Syson and turned into Fear in the Night. 

== Critical reception ==
  Time Out A Taste of Fear (sic)". 

== References ==

 

; Sources

*  
*  

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 
 
 