Dear Alice
{{Infobox film name = Dear Alice image = Dear Alice.jpg director   = Othman Karim producer   = Malin Holmberg-Karim Hans Lönnerheden writer     = Othman Karim Grace Maharaj-Eriksson starring   = Danny Glover Tuva Novotny Stefan Sauk Regina Lund Ulf Brunnberg Peter Gardiner Moses Said cinematography = Esa Vuorinen editing = Katarina Duske Darek Hodor distributor = Nordisk Film  budget =  released =   runtime = 120 minutes country = Sweden language = Swedish English
|}}
Dear Alice ( ) is a 2010 Swedish drama film directed by Othman Karim starring Danny Glover, Tuva Novotny and Peter Gardiner. The film is written by Karim and Grace Maharaj-Eriksson. 

Dear Alice competed at the 2010 Moscow Film Festival. 

==Plot==
Very different lives, becomes interweaved during what seems like an ordinary day. Franzis Namazi (Danny Glover) is a newly arrived immigrant from Gambia to Sweden.  He is about to give up on his little store selling African art. Karin Carlsson-Said (Tuva Novotny)  is a lawyer who is about to enter a new important step in her career as a lawyer. Her husband Moses (Peter Gardiner) must send money to his hospitalized father in Uganda, but there are problems with the transaction and has issues with keeping up his work as a Social Worker. Bosse (Ulf Brunnberg)  is the TV star who finds out he has been fired off his own show and finds his young wife with another man, Håkan (Stefan Sauk) is a charming celebrity with an alcohol problem and now once again needs help from his lawyer Karin Carlsson-Said.  

==Cast==
*Danny Glover - Franzis Namazi
*Tuva Novotny - Karin Carlsson-Said
*Stefan Sauk - Håkan Pettersson
*Regina Lund - Elisabeth Krantz
*Ulf Brunnberg - Bosse Krantz
*Peter Gardiner - Moses Said
*Meta Velander - Elsa

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 


 