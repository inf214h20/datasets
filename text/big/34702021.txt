Chino (1991 film)
{{Infobox film name     = Chino image    = Chinofilm.jpg caption  = Film cassette cover director = Tulsi Ghimire producer = Om Trinetra Production music    = Ranjit Gazmer released =   language = Nepali
|country  = Nepal
}}
 Shiva Shrestha, Bhuwan K.C., Sunil Thapa, Kristi Mainali, Sharmila Malla, Subhadra Adhikari, Sinaura Mistry, Anoop Malla and Sushila Raymajhi.  Ranjit Gazmer was the music director of the film.

==Film==
Chino had all the elements of an entertaining Nepali movie: action, story, romance and above all good music. Mohani Lagla Hai, sung by Narayan Gopal and Asha Bhosle, rocked the Nepali music charts for several months. Chino is often referred to as the Sholay of Nepali cinema.

==Soundtrack==
{| class="wikitable sortable"
|-
! Track !! Artists !! Length
|- Kumar Kancha and Asha Bhosle||
|- Asha Bhosle||
|- Narayan Gopal ||
|- Narayan Gopal and Asha Bhosle||
|- Narayan Gopal and Asha Bhosle||
|}

==References==
 

==External links==
*  
*  

 
 
 