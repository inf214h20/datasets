Living Together (film)
{{Infobox film
| name           = Living Together
| image          = Living Together (2011 film).jpg
| caption        = Film poster
| alt            = Fazil
| producer       = Pilakandy Mohammed Ali
| writer         = Fazil Sreejith Nedumudi Innocent
| music          = M. Jayachandran
| cinematography = Anandakuttan
| editing        = Saajan
| studio         = Pilakandy Films International
| distributor    = Playhouse
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Menaka 
made her comeback through this film. 

==Plot==
Shyama is a free spirit who lost her parents at a young age. After their death, she becomes attached to her grandfather and uncle. She is brought in a wrong manner and she used to flirt with boys. She becomes friendly with Hemachandran, a boy in the neighbourhood who falls in love with her. However, she moves away from the locality. Later, Hemanth discovers Shyama does love him. However, Shyama is not willing for a marriage as she is told that either she or Hemachandran will die after six months from the marriage. The story continues to tell how they overcome this obstacle in their relationship and family problems.

==Cast==
* Hemanth Menon as Hemachandran
* Sreelekha as Shyama Sreejith as Niranjan, Hemachandrans friend
* Jinoop as Bavappan, Hemachandrans friend
* Nedumudi Venu as Vasudevan Kartha, Shyamas grandfather Menaka as Valsala, Hemachandrans mother Innocent as Krishnaprasad Kartha, Shyamas uncle.
* Bindu Panicker as Vasanthi, Krishnaprasads wife
* Anoop Chandran as Manikantan, an adopted family member.
* Lakshmipriya as Manikantans wife
* Darshak as Neighbour

==Soundtrack==
The music of Living Together is composed by M. Jayachandran. The lyrics are written by Kaithapram Damodaran Namboothiri.
{{Track listing
| extra_column = Performer(s)
| title1 = Mayangoo Nee | extra1 = K. J. Yesudas
| title2 = Samarasa Ranjini | extra2 = M. G. Sreekumar
| title3 = Pattinte | extra3 = Vijay Yesudas
| title4 = Pattinte | extra4 = Shreya Ghoshal
| title5 = Kuttikurumba | extra5 = Sudeep Kumar
| title6 = Ragachandran | extra6 = Karthik (singer)|Karthik, Shweta Mohan
| title7 = Kuttikurumba | extra7 = Anila
| title8 = Ilakoo Naage | extra8 = Shweta Mohan, Janardhanan Puthuserry
}}

==Box office==
The film received negative reception from critics and viewers, and its turning out as one of the biggest box office disasters of recent times. {{cite web|url=http://www.sanscinema.com/2011/02/not-many-takers-for-living-together/
|title=Not Many Takers For Living Together |date=22 February 2011|publisher=SansCinema}} 

==References==
 

==External links==
*  . The Hindu. Retrieved 2011-02-18.

 

 
 
 
 