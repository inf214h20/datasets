The Jade Box
 
{{Infobox film
| name           = The Jade Box
| image          =
| caption        = Ray Taylor
| producer       = Henry MacRae
| writer         = Frederick J. Jackson
| narrator       = Francis Ford
| music          = Sam Perry
| cinematography =
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 10 chapters (220 minutes)
| country        = United States
| language       = English
| budget         =
}} Universal Serial movie serial. silent elements.  Only an incomplete version survives today.

==Plot==

John Lamar buys a Jade Box in Asia but it is stolen by his friend Martin Morgan.  A cult, searching for the box because it contains the secret to invisibility, catches up with and abducts Lamar.  After discovering the theft, the cult send a message to Martin and the pairs children: John Lamars son, Jack, who is engaged to Martin Morgans daughter, Helen.  Jack searches for the Box while Martin attempts to discover the secret of invisibility for his own schemes.

==Cast==
*Jack Perrin as Jack Lamar, Johns son & Helens fiance
*Louise Lorraine as Helen Morgan, Martins daughter & Jacks fiance Francis Ford as Martin Morgan, false friend of John Lamar
*Wilbur Mack as Edward Haines
*Leo White as Percy Winslow
*Monroe Salisbury as John Lamar, original purchaser of the Box and cult abductee
*Jay Novello as Bit
*Eileen Sedgwick
*Frank Lackteen

==Critical reception==
Cline states that, while The Jade Box is not of a high technical quality, it did show at the time that a mystery serial could be improved by the addition of music and sound effects. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 47
 | chapter = 3. The Six Faces of Adventure
 }} 

==Chapter titles==
# The Jade of Jeopardy
# Buried Alive
# The Shadow Man
# The Fatal Prophecy
# The Unseen Death
# The Haunting Shadow
# The Guilty Man
# The Grip of Death
# Out of the Shadows
# The Atonement
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 201
 | chapter = Filmography
 }} 

==References==
 

==External links==
* 
* 

 
{{succession box  Universal Serial Serial 
| before=Tarzan the Tiger (1929 in film|1929)
| years=The Jade Box (1930 in film|1930)
| after=The Lightning Express (1930 in film|1930)}}
 

 

 
 
 
 
 
 
 
 
 
 