Jaihind 2
{{Infobox film
| name           = Jaihind 2
| image          = Jai Hind 2.jpg
| alt            = 
| caption        = Poster Arjun
| producer       = Arjun Aishwarya Arjun (co-producer)
| writer         = G. K. Gopinath (dialogues)
| story          = Arjun
| starring       = Arjun Surveen Chawla Rahul Dev Simran Kapoor
| music          = Arjun Janya
| cinematography = HC Venugopal
| editing        = Kay Kay
| studio         = Sri Ram Films Internationals
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil Kannada
| budget         = 
| gross          = 
}}
 Tamil action masala film Jai Hind,  it was simultaneously released in Kannada under the title Abhimanyu.  The film focusses on India becoming a superpower as dreamt by A. P. J. Abdul Kalam and tries to tackle the issues in the education system.    The film released on November 7, 2014.This movie is dubbed in hindi as "Arjun ka badla".

==Plot==
Jaihind is the journey of five people, linked by one man, who fights for childrens right to education. Little Parvathi from a very poor family background gets an admission into one of the most affluent schools in the city. But unable to pay the exorbitant fees, the man of the house sells one of his kidneys. They still fall short of a big chunk of money and the finally gives up and commits suicide. This disturbs Abhimanyu (Arjun Sarja) deeply, and the computer service engineer becomes a hero over night, by propagandizing a solution for education costing so much. Abhimanyu suggests that all private schools be nationalised, which will compel them to offer services at very little fees compared to that being collected now, while still ensuring that the quality of education is not compromised. This is bad news for the private school owners, who make a business  out of educating children. And hence Abhimanyu is targeted and pushed to extremes.He is put behind bars and is conspired to be murdered. How he succeeds in his mission,despite all this, forms the crux of the movie.

==Cast==
 
*Arjun Sarja as Abhimanyu
*Surveen Chawla as Nandhini
*Atul Mathur as Vikram Tagore
*Rahul Dev
*Charlotte Claire (Simran Kapoor) as Simran
*Yuvina Parthavi as Parvathi
*Brahmanandam as Ezhumalai
*Mayilsamy as Kulfi Gopalan Ali
*Manobala as Nandhinis father
*Vinaya Prasad as Nandhinis mother Shafi as Chandru, Parvathis father
*Vaijanath Biradar as Parvathis grandfather
*Gowtham Sundararajan
*Ravi Kale
*Bose Venkat
*D. Sasikumar ("Melur" Sasi)
*Amit Kumar Tiwari
*Siva Narayanamoorthy
*Sakthivel
*Amjad
*Abhijith
*Sampath Ram
*Scissor Manohar
*Gana Bala
 

==Production==
The movie shoot was launched on June 9, 2013 in a grand manner in Mumbai and budgeted at 20 crores.   Art director, Sasidhar, erected a huge set, resembling a jail at a cost of Rs 25 lakhs.  A high octane action sequence was filmed in a former army facility in Bangkok. 

==Soundtrack==
*Adada Nenjil - Karthik, Saindhavi
*Ayya Padichavare - Gaana Bala, Ramesh Krishnan
*Ivan Yaarivan - Karthik, Priya
*Mazhaye - Ravivarma

==Release== Zee Thamizh.

==Critical reception==
The movie opened with moderate to mixed reviews from critics.  The Hindu stated "Arjunin Jai Hind 2 is the kind of film about which you shrug and say, “Well, if you liked Jai Hind…”. If you have to watch a film about a one-man army, you could do worse than watch one with Arjun in it. He totally pulls it off".  The Times of India gave 3 stars out of 5 and wrote, "For most parts, Arjunin Jai Hind 2 plays like a collage of various Arjun films...Whats interesting, however, is that instead of telling his story as a straightforward narrative, Arjun presents it as events from the lives of different individuals who come into Abhimanyus life...The problem with the film is that it tells a predictable story and in a rather longwinded manner".  The New Indian Express wrote, "The film conveys a relevant message, but one that could have been better executed. The saving grace is Arjun who has penned the plot and screenplay, directed and produced the film. The actor tackles his role with cool intensity. His physique well toned, the action king is a delight to watch in the fights-stunt scenes as he packs a powerful punch at his tormentors. If only his screenplay had matched the lofty message".  Rediff gave 2 stars out of 5 and wrote, "Unfortunately, Arjun fails as a director. Despite the good storyline, he struggles to keep the audience involved. The real issue seems lost in all the unnecessary twists and drama surrounding the characters. Patriotic action dramas have been his forte, but Jai Hind 2 neither creates empathy for the characters and their situation, nor has entertainment value".  Behindwoods.com gave 1.75 stars out of 5 and wrote, "Jai Hind 2 travels on a very predictable path and the writing is quite disjointed. There is plenty of melodrama which gives the film a tonality of 80s...The theme of education for all is quite relevant at the time of ‘Right to Education, but had it been executed well, Jai Hind 2 would have worked well".  Indiaglitz.com gave 2 out of 5 and wrote, "Keeping himself up with the pace, Arjun has delivered movies of social concern and importance, from time to time. This take on education is a much needed wake up call for the current scenario". 

The Kannada version received more positive reviews. Bangalore Mirror wrote, "Abhimanyu may not be unique in how the story is told, but the idea is novel and is appealing enough".  Indiaglitz.com gave 8 out of 10 and wrote, "Terrific action, tolerable narration, appealing in contents – Arjun Sarja in his first direction in Kannada has given a ‘Paisa Vasool’ cinema Abhimanyu".  The New Indian Express wrote, "Abhimanyu is a taut action film with a strong social message. The subject would not have lost its grip if he was a little focussed on what exactly he wanted to say. However, Arjun has put enough effort into the film to make it a worthy one-time watch".  Cinemalead rated a 2.5 out of 5 and wrote," Watchable for Arjun" 

==References==
 
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 