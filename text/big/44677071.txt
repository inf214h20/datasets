Nana to Kaoru
{{Infobox animanga/Header
| name            = Nana to Kaoru
| image           = 
| caption         = 
| ja_kanji        = ナナとカオル
| ja_romaji       = 
| genre           = Erotic romantic comedy 
}}
{{Infobox animanga/Print
| type            = manga
| title           = 
| author          = Ryuta Amazume
| illustrator     = 
| publisher       = Hakusensha
| publisher_en    = 
| demographic     = seinen manga|Seinen
| imprint         = 
| magazine        = Young Animal Arashi Young Animal
| magazine_en     = 
| published       = 
| first           = November 28, 2008 (volume 1)
| last            = 
| volumes         = 15
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = 
| director        = Atsushi Shimizu
| producer        = Mitsuru Ohshima
| writer          = Atsushi Shimizu
| music           = 
| studio          = 
| licensee        = 
| released        =  
| runtime         = 80 minutes
}}
{{Infobox animanga/Video
| type            = ova
| title           = 
| director        = Hideki Okamoto
| producer        = 
| writer          = 
| music           = 
| studio          = AIC PLUS+
| licensee        = 
| released        = March 29, 2011
| first           = 
| last            = 
| runtime         = 
| episodes        = 
| episode_list    = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = Nana to Kaoru Black Label
| author          = Ryuta Amazume
| illustrator     = 
| publisher       = Hakusensha
| publisher_en    = 
| demographic     = seinen manga|Seinen
| imprint         = 
| magazine        = Young Animal Arashi
| magazine_en     = 
| published       = 
| first           = June 29, 2011 (volume 1)
| last            = April 4, 2014
| volumes         = 5
| volume_list     = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = Nana Kao Pink Pure
| author          = Tamami Momose
| illustrator     = 
| publisher       = Hakusensha
| publisher_en    = 
| demographic     = seinen manga|Seinen
| imprint         = 
| magazine        = Young Animal Arashi
| magazine_en     = 
| published       = 
| first           = October 7, 2011
| last            = January 29, 2014 (volume)
| volumes         = 1
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = Nana to Kaoru: Chapter 2
| director        = Atsushi Shimizu
| producer        = Mitsuru Ohshima
| writer          = Atsushi Shimizu
| music           = 
| studio          = 
| licensee        = 
| released        =  
| runtime         = 
}}
 
  is an ongoing Japanese erotic romantic comedy seinen manga|seinen manga series written and illustrated by Ryuta Amazume.    It was serialized on Hakusenshas Young Animal Arashi manga magazine and is now serialized on Young Animal.  15 volumes have been published so far.  Two other manga series have also been released, as well as an original video animation and two live action films.

==Characters==
*Kaoru (played by Rakuto Tochihara)   
*Nana (played by Maho Nagase (first film) and Miku Aono (second film)) 

==Media==
===Manga=== 
The manga is published in French by Pika Édition|Pika.  

Nana to Kaoru Black Label is set 8 months after the events in Nana to Kaoru.  The first volume was published in June 29, 2011 and the series ended on April 4, 2014.     

Nana Kao Pink Pure, a yonkoma spin-off (media)|spin-off manga by Tamami Momose, started being serialized on Young Animal Arashi on October 7, 2011.  A single complete volume of the series was published on January 29, 2014.  

====Volumes====
=====Nana to Kaoru=====
*1 (November 28, 2008)   
*2 (May 29, 2009)  
*3 (January 29, 2010)  
*4 (May 28, 2010)  
*5 (October 29, 2010)  
*6 (March 29, 2011)  
*7 (October 28, 2011)  
*8 (April 27, 2012)  
*9 (September 28, 2012)  
*10 (April 26, 2013)  
*11 (July 29, 2013)  
*12 (January 29, 2014)  
*13 (August 29, 2014)  
*14 (December 26, 2014)  

=====Nana to Kaoru Black Label=====
*1 (June 29, 2011) 
*2 (May 29, 2012)  
*3 (February 28, 2013)  
*4 (December 27, 2013)  
*5 (July 29, 2014) 

===Live action films=== romance erotic films based on the manga were released, both directed by Atsushi Shimizu. The first, on March 19, 2011 and the second,  , on September 8, 2012.    

===Anime===
An original video animation produced by AIC PLUS+, directed by Hideki Okamoto and with character designs by Atsuko Watanabe was released on March 29, 2011.  

==Reception==
Volume 3 reached the 30th place on the weekly Oricon manga chart and, as of January 31, 2010, has sold 25,008 copies;  volume 5 reached th 22nd place and, as of October 31, 2010, has sold 34,146 copies;  volume 6 reached the 15th place and, as of April 3, 2011, has sold 34,792 copies;  Nana to Kaoru Black Label volume 1 reached the 23rd place and, as of July 3, 2011, has sold 33,619 copies;  Nana to Kaoru Black Label volume 2 reached the 28th place and, as of June 3, 2012, has sold 21,655 copies;  volume 9 reached the 24th place  and, as of October 7, 2012, has sold 55,005 copies;  Nana to Kaoru Black Label volume 3 reached the 46th place and, as of March 2, 2013, has sold 18,741 copies;  volume 11 reached the 22nd place and, as of August 4, 2013, has sold 40,873 copies. 

At Manga Sanctuary, two of the staff members gave the series a combined grade of 5.5 out of 10. 

==References==
 

==External links==
*   
*   
*   
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 
 