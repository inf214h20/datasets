Chameli Ki Shaadi
{{Infobox film
| name = Chameli Ki Shaadi
| image =
| starring =  Anil Kapoor Amrita Singh Amjad Khan Pankaj Kapur Ram Sethi
| director = Basu Chatterjee
| music = Kalyanji Anandji
| story = Om Prakash Sharma
| language = Hindi
| released = 21 February 1986
| gross = 
|}}

Chameli Ki Shaadi is a 1986 Bollywood comedy film directed by Basu Chatterjee starring Anil Kapoor and Amrita Singh. The film was declared an all-time blockbuster.

==Summary==

The film is a satire on caste system in India. Charandas (Anil Kapoor) is a bachelor who aspires to become a wrestler. He is training himself under the tutelage of Mastram Pehelwan , who preaches & practices absolute abstinence. Charan has no problem with his life, but his elder brother Bhajandas (Satyen Kappu) & his sister-in-law (Tabassum) do. The film is also one of the examples of a female-centric movie where Amrita Singh as the young, headstrong, outspoken Chameli, was acknowledged as the star of the film, and her role was later critiqued to be one of the first takes on the realization of feminine desires and domination in Indian cinema. 

One day, however, Charandas runs into Chameli (Amrita Singh), the daughter of Kallumal (Pankaj Kapoor), a coal depot owner. The duo start an affair, which is soon exposed & opposed by both the families, due to the fact that they are from different castes. Charandas has to take the advice of Adv. Harish (Amjad Khan). How the lovers find their way out of the situation with help of Harish, Mastram & the friends of the duo forms the story.

==Plot==

Charandas is a fledgling wrestler who lives with Bhajandas. According to Bhajandas, Charandas should marry and start a family of his own. But Charan is hell bent on becoming a wrestler & is even ready to remain a bachelor until 40 as per the norms of Mastram. However, one day, Charans thoughts are challenged when he sees Chameli on Kallumals coal depot. He realizes that to marry her, he may have to leave Mastrams Akhara.

Charan does so & seeks help of Adv.Harish, a close friend of his brother. Harish appreciates Charans love & is ready to help latter. Charan dares to confess his love to Chameli. On other hand, Anita (Roopini), Chamelis best friend is also convinced of Chamelis true love. The lover duo start meeting secretly. One day, however, the love affair stands exposed when a relative of Chameli spots the duo in a restaurant.

The respective families soon take up cudgels against their wards & make it clear that they wont allow inter caste marriage. Chameli is kept under house arrest & her parents decide to get her married to one of their acquaintances. Charan comes to know of this development. Mastram is surprisingly supportive of Charans love. He explains that just as bachelorhood is a challenge for would be wrestlers, a lover should not back out from the challenge posed by the world.

Mastram along with Charans friends back the lovers to overcome the obstacles. Here, Champa calls in the help of her rogue brother Chhadam Lal aka Chhadmi (Annu Kapoor) against Charan. With help of Harish & all his friends, Charan chalks out a plan to rescue Chameli from her house. As per the plan, Charan abducts Chameli from her own house, while Harish sees to it that the duo legally get married. Charan & Chameli are initially frightened, but Harish convinces them by telling them that as long as people do not dare to marry outside the caste like them, Indians will never be truly united.

Meanwhile, on learning that Chameli is missing, her parents lodge a complaint against Charan. They proceed to the place where Charan & Chameli are going to get married. Meanwhile, Bhajandas also comes with his men to disrupt the marriage. Both the parties have arrived late, as Charandas & Chameli are legally husband & wife now. They vent their ire on Harish, who according to them is responsible for corrupting the duo. Harish takes Bhajandas sidewise & explains that if the marriage is permitted by him, he will garner votes of both the castes in the election. Besides, he will get coal & cement at subsidized rate from Kallumal.

Convinced by the argument of Harish, Bhajandas relents in the control of greed. Similarly, Harish tells Kallumal that an influential Bhajandas is sure to win the elections. If Kallumal accepts the marriage, he will get special perks for his coal business thanks to Bhajandas. Kallumal too goes greedy & relents. In the end, Chameli & Charandas are united with the blessing from both the parties.

== Cast ==
* Anil Kapoor .... Charandas
* Amrita Singh .... Chameli
* Amjad Khan .... Adv. Harish
* Pankaj Kapoor .... Kallumal Koylewala Chamelis Father
* Satyen Kappu .... Bhajandas Charandass Elder Brother
* Om Prakash .... Ustad Mastram Pahelwan
* Tabassum .... Bhajandass Wife
* Bharati Achrekar .... Champa, Chamelis Mother
* Annu Kapoor .... Chhadam Lal/Chhadami
* Ram Sethi .... Nathulaal
* Jayshree T ....  Gulabo Naththilals Wife
* Ghanshyam Rohera .... Fatchat
* Shail Chaturvedi .... Lachchuram Kaphanchi / Makhkhans Father
* Rupini (actress) .... Anita, Chamelis Friend

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Peena Haraam Hai"
| Kishore Kumar, Alka Yagnik
|-
| 2
| "Mohabbat Ke Dushman" Anwar
|-
| 3
| "Chameli Ki Shaadi"
| Anil Kapoor
|- 
| 4
| "Tu Jahan Bhi Chalega"
| Asha Bhosle
|-
| 5
| "Utar Hai Akhade Mein"
| Asha Bhosle
|}

== External links ==
* 

 
 
 
 
 
 