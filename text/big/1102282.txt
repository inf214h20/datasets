Hotel Rwanda
{{Infobox film
| name           = Hotel Rwanda
| image          = Hotel Rwanda movie.jpg
| caption        = Theatrical release poster
| director       = Terry George
| producer       = Terry George
| writer         = {{Plainlist|
* Keir Pearson
* Terry George }}
| starring       = {{Plainlist|
* Don Cheadle
* Sophie Okonedo
* Joaquin Phoenix
* Nick Nolte }}
| music          = {{Plainlist|
* Afro Celt Sound System
* Rupert Gregson-Williams Andrea Guerra }} Robert Fraisse
| editing        = Naomi Geraghty
| studio         = {{Plainlist|
* Lions Gate Entertainment
* United Artists }}
| distributor    = {{Plainlist|
* Metro-Goldwyn-Mayer
* Lionsgate Films }}
| released       =  
| runtime        = 121 minutes
| country        = United Kingdom South Africa Italy
| language       = English
| budget         = $17.5 million 
| gross          = $33.9 million   
}}
 hotelier Paul Rusesabagina and his wife Tatiana Rusesabagina|Tatiana. Based on real life events in Rwanda during the spring of 1994, the film which has been called an African Schindlers List, documents Rusesabaginas acts to save the lives of his family and more than a thousand other refugees, by granting them shelter in the besieged Hôtel des Mille Collines. {{cite journal 
| last = Burr | first = Ty | date = January 7, 2005 | title = Hotel Rwanda Movie Review: Cheadle brings quiet power to Rwanda | url = http://www.boston.com/movies/display?display=movie&id=7112 | journal = The Boston Globe | accessdate = 2007-04-09}}  Hotel Rwanda explores genocide, political corruption, and the repercussions of violence.  
 Berlin and Andrea Guerra, and the Afro Celt Sound System.

Hotel Rwanda premiered in theaters in limited release in the United States on December 22, 2004 and in wide release on February 4, 2005 grossing more than $23 million in domestic ticket sales. It earned an additional $10 million in business through international release to top out at a combined total near $34 million in gross revenue. The film was technically considered a moderate financial success after its theatrical run, and was met with positive critical reviews before its initial screening in cinemas. The Blu-ray Disc edition of the film featuring special documentaries along with selected scenes and audio commentary, was released in the United States on May 10, 2011.

==Plot==
Tensions between the Hutu and Tutsi peoples lead to a war in Rwanda, where corruption and bribes between politicians are routine. Paul Rusesabagina (Cheadle), the manager of the Sabena owned Hôtel des Mille Collines, is Hutu, but his wife Tatiana (Okonedo), is Tutsi. His marriage is a source of friction with Hutu extremists, most prominently Georges Rutaganda (Kae-Kazim), a friendly goods supplier to the hotel who is also the local leader of Interahamwe, a brutal Hutu militia.
 assassination of the president, Paul and his family observe neighbors being killed in ethnic violence, initiating the early stages of a Rwandan Genocide|genocide. Paul curries favor with people of influence, bribing them with money and alcohol, seeking to maintain sufficient influence to keep his family safe. When civil war erupts and a Rwandan Army officer threatens Paul and his neighbors, Paul barely negotiates their safety, and brings them to the hotel. More evacuees arrive at the hotel from the overburdened UN refugee camp, the Red Cross, and various orphanages. Paul must divert the Hutu soldiers, care for the refugees, be a source of strength to his family, and maintain the appearance of a functioning hotel as the situation becomes more violent.

The UN Peacekeeping forces, led by Canadian Colonel Oliver (Nolte), are unable to take assertive action against the Interahamwe since they are forbidden to intervene in the genocide. The foreign nationals are evacuated, but the Rwandans are left behind. When the UN forces attempt to evacuate a group of refugees, including Pauls family, they are ambushed and must turn back. In a last-ditch effort to save the refugees, Paul pleads with the Rwandan Army General, Augustin Bizimungu (Mokoena) for assistance. However, when Pauls bribes no longer work, he blackmails the General with threats of being tried as a war criminal. Soon after, the family and the hotel refugees are finally able to leave the besieged hotel in a UN convoy. They travel through retreating masses of refugees and militia to reach safety behind Tutsi rebel lines.
 tried and convicted by the UN for war crimes in 2002, while also conveying that the  genocide ended in July 1994 leaving almost a million people dead.

==Cast==
 
{| style="margin-left: 10px;"
| Don Cheadle
| as Paul Rusesabagina
|-
| Sophie Okonedo
| as Tatiana Rusesabagina
|-
| Nick Nolte
| as Colonel Oliver (based on Roméo Dallaire)
|-
| Joaquin Phoenix
| as Jack Daglish
|-
| Fana Mokoena
| as General Augustin Bizimungu
|-
| Cara Seymour
| as Pat Archer
|-
| Jean Reno Sabena Airlines President Mr. Tillens (uncredited)
|-
| Tony Kgoroge
| as Gregoire
|-
| Desmond Dube
| as Dube
|-
| Hakeem Kae-Kazim
| as Georges Rutaganda
|-
| Leleti Khumalo
| as Fedens
|-
| Antonio Lyons
| as Thomas Mirama
|}

==Production==
===Development===
 ]]
The film is set in 1994 during the Rwandan Genocide, in which an estimated 800,000 people, mainly Tutsi, were killed by Hutu extremists.  During that year, Rwandas population of seven million was composed of two major ethnic groups: Hutu (approximately 85%), and Tutsi (14%). In the early 1990s, Hutu extremists within Rwandas political elite blamed the entire Tutsi minority population for the countrys economic and political problems. Tutsi civilians were also accused of supporting a Tutsi-dominated rebel group, the Rwandan Patriotic Front.   

On April 6, 1994, a plane carrying President Juvénal Habyarimana, a Hutu, was shot down.    Following that incident, the genocide began. Hutu extremists belonging to the Interahamwe militia launched plans to destroy the entire Tutsi civilian population. Tutsi and people suspected of being Tutsi were killed in their homes and as they tried to flee the country. It is estimated that some 200,000 people participated in the perpetration of the Rwandan genocide.  Hotelier Paul Rusesabagina of the Belgian owned luxury Hôtel des Mille Collines, used his power and influence to personally save both Tutsi and Hutu refugees. Rusesabagina regularly bribed Rwandan Hutu soldiers and kept militias outside the hotels property during the hundred days of killing.    Following the carnage, Rusesabagina survived along with his wife, four children, two adopted nieces; as well as most of the refugees he sheltered. 

  2007 dramatic motion picture.

===Filming===
Principal filming was shot on location in  .  Paul Rusesabagina was consulted during the writing of the film. Although the character of Colonel Oliver played by Nolte is fictional in nature, the role was inspired by the UN force commander for UNAMIR, Roméo Dallaire.  Ugandan president Yoweri Museveni, then-Rwandan president Juvénal Haryabimana, and Rwandan Patriotic Front leader (now president) Paul Kagame appear in archive television footage in the film.

The producers of the film partnered with the United Nations Foundation to create the International Fund for Rwanda, which supported United Nations Development Programme initiatives assisting Rwandan survivors.  "The goal of the film is not only to engage audiences in this story of genocide but also to inspire them to help redress the terrible devastation," said George. 

===Soundtrack=== Andrea Guerra, and the Afro Celt Sound System, while being edited by Michael Connell.  

{{Infobox Album
| Name = Hotel Rwanda: Music From The Film
| Type = Film score
| Artist = Afro Celt Sound System and Rupert Gregson-Williams 
| Cover = Hotel Rwanda Soundtrack.jpg
| Released = 1/11/2005
| Length = 49:25
| Label = Commotion
| Reviews = 
}}

{{Track listing
| collapsed       = no
| headline        = Hotel Rwanda: Music From The Film
| total_length    = 49:25
| title1          = Mama Ararira Pt. 1/Mama Ararira We!, Pt. 2
| length1         = 3:41
| title2          = Mwali We!
| length2         = 1:09
| title3          = Million Voices
| length3         = 4:23
| title4          = Interahamwe Attack
| length4         = 2:48
| title5          = Nobody Cares
| length5         = 4:12 Umqombothi (African Beer)
| length6         = 4:53
| title7          = The Road to Exile
| length7         = 4:47
| title8          = Whispered Song
| length8         = 3:06
| title9          = Finale
| length9         = 3:02
| title10         = Ambush
| length10        = 2:49
| title11         = Ne Me Laisse Pas Seule Ici
| length11        = 3:33
| title12         = Mwari Sigaramahoro
| length12        = 2:22
| title13         = Olugendo Lwe Bulaya
| length13        = 5:54
| title14         = Children Found
| length14        = 1:57
| title15         = Icyibo
| length15        = 0:49
}}

==Marketing==
===Novel===
A paperback novel published by Newmarket Press entitled Hotel Rwanda: Bringing the True Story of an African Hero to Film, was released on February 7, 2005. The book dramatizes the events of the Rwandan Genocide in 1994, as depicted in the film. It expands on the ideas of how hotelier Paul Rusesabagina, sheltered and saved more than 1,200 people in the hotel he managed in Kigali.  Rusesabaginas real life experience encouraged director George to produce the film. The book summarizes three years of research, articles that chronicle the historical events, and the ensuing aftermath. A brief history and timeline, the making of the film, and the complete screenplay written by Keir Pearson and Terry George are covered in thorough detail. 

==Release==
===Home media===
Following its cinematic release in theaters, the film was released in   version of the film was released on May 10, 2011.  The film is available in other media formats such as Video on demand as well.  

==Reception==
===Critical response=== weighted average out of 100 to critics reviews, the film received a score of 79 based on 40 reviews. 

{{quote box|quote=
As Rusesabagina, Cheadle is simply terrific. Not only does he assume a totally believable African accent and manner, he convinces us with every move and gesture that hes a resourceful Everyman elevated to genuine heroism by a struggle to do the right thing.|source=—William Arnold, writing in the Seattle Post-Intelligencer |align=left|width=40%|fontsize=85%|bgcolor=#FFFFF0|quoted=2}}
Michael Rechtshaffen, writing in The Hollywood Reporter, said actor "Cheadle impressively carries the entire picture, delivering the kind of note-perfect performance thats absolutely deserving of Oscar consideration."  Roger Ebert in the Chicago Sun-Times called it a "riveting drama", while exclaiming "The film works not because the screen is filled with meaningless special effects, formless action and vast digital armies, but because Cheadle, Nolte and the filmmakers are interested in how two men choose to function in an impossible situation. Because we sympathize with these men, we are moved by the film."  In the San Francisco Chronicle, Mick LaSalle wrote that the film was a "harrowing experience", and that "it documents for a mass audience what it was like. Its useful, in that it shows how it can happen. Its even hopeful, in that it shows that its possible—not guaranteed, but possible—for people to maintain their humanity in the face of unhinged barbarism."  Claudia Puig of USA Today, said the film was "one of the years most moving and powerful films, anchored by a magnificent performance by Don Cheadle." She declared, "Hotel Rwanda emerges as an African version of Schindlers List."  The film however, was not without its detractors. Dave Sterrit of The Christian Science Monitor, felt that although the subject matter was crucially important, he commented that "the movie dilutes its impact with by-the-numbers filmmaking, and Cheadles one-note performance displays few of his acting gifts."  Left equally unimpressed was Lisa Schwarzbaum of Entertainment Weekly. Commenting on the character significance of the U.N. personnel, she said it was "a bad day for narrative, if not for diplomacy, when there is only one 3-D character among the entire U.N. lot, clad in their blue helmets, and that role is rasped by Nick Nolte with moral remorse rather than his more usual hint of dissolution." In her overall summation, she wrote "Hotel Rwanda is a strange history lesson that leaves us more overlectured than properly overwhelmed."  Michael Atkinson of The Village Voice, added to the negativity by saying the film was "told to us secondhand, or glimpsed in distant scuffles" and "Like the majority of movies about the last century of holocausts, Hotel Rwanda is as earnest and tasteful as its creators. To capture the white-hot terror of social calamity, someone a little more lawless and fierce might be called for." 

Writing for   writing for  . Retrieved 2010-06-06.  Describing some pitfalls, Jeff Vice of the  . Retrieved 2010-06-06. 

  Edward Herman, author Matthew Alford called the film "sensitive, humane and powerful" but noted that it was "striking how the history of bloodshed has been spun in line with Western interests". 

===Accolades=== 100 most inspirational movies of all time. 

{|class="wikitable" border="1"
|- 
! Award
! Category
! Nominee
! Result
|- 77th Academy Awards  Best Actor Don Cheadle
| 
|- Best Supporting Actress Sophie Okonedo
| 
|- Academy Award Best Original Screenplay Keir Pearson, Terry George
| 
|- American Film Institute Awards 2004  Top Audience Award
|align="center" |————
| 
|- Discover Screenwriting Award 2004  Discover Screenwriting Award Keir Pearson, Terry George
| 
|- 2005 Berlin International Film Festival  In Competition
|align="center" |————
| 
|- 2005 BET Bet Awards  Best Actor Don Cheadle
| 
|- Black Reel Awards of 2005  Best Actor in a Drama Don Cheadle
| 
|- Best Actress in a Drama Sophie Okonedo
| 
|- 59th British Academy Film Awards  Best Original Screenplay Keir Pearson, Terry George
| 
|- 10th Critics Broadcast Film Critics Association Awards 2004  Best Picture 
|align="center" |————
| 
|- Best Actor Don Cheadle
| 
|-
|rowspan=2|Dallas-Fort Worth Film Critics Association Awards 2005  Best Actor Don Cheadle
| 
|- Best Picture
|align="center" |————
| 
|- 2005 David David Di Donatello Awards  Best Foreign Film
|align="center" |————
| 
|- 2005 18th European Film Awards  Best Composer Rupert Gregson-Williams, Andrea Guerra
| 
|- 62nd Golden Globe Awards  Best Picture - Drama
|align="center" |————
| 
|- Best Actor - Drama Don Cheadle
| 
|- Golden Globe Best Original Song Jerry Duplessis, Andrea Guerra, Wyclef Jean
| 
|- Golden Satellite Awards 2004  Satellite Award Best Actor – Motion Picture Drama Don Cheadle
| 
|- Best Motion Picture Drama
|align="center" |————
| 
|- Best Original Song Jerry Duplessis, Andrea Guerra, Wyclef Jean
| 
|- Best Original Screenplay Keir Pearson, Terry George
| 
|- 2006 Grammy Awards  Best Song Written For Motion Picture, Television or Other Visual Media Jerry Duplessis, Andrea Guerra, Wyclef Jean
| 
|- 2005 Humanitas Prize  Humanitas Prize Keir Pearson, Terry George
| 
|- 2005 3rd Irish Film & Television Awards  Best Director Terry George
| 
|- Best Script for Film Keir Pearson, Terry George
| 
|- 31st Japan Japan Academy Prize Ceremony  Best Foreign Language Film
|align="center" |————
| 
|- London Film Critics Circle Awards 2005  Best British Director Terry George
| 
|- Best Actor  Don Cheadle
| 
|- Best British Supporting Actress Sophie Okonedo
| 
|- 2005 MovieGuide Awards  Most Inspiring Movie Acting Don Cheadle
| 
|- 2005 36th NAACP Image Awards  Outstanding Actor in a Motion Picture Don Cheadle
| 
|- Outstanding Motion Picture
|align="center" |————
| 
|- Outstanding Supporting Actress in a Motion Picture Sophie Okonedo
| 
|- 2006 Nastro dArgento Silver Ribbon Award Best Score Andrea Guerra
| 
|- 2004 National Board of Review of Motion Pictures Awards  Top Ten Films
|align="center" |————
| 
|- Online Film Critics Society Awards 2004  Best Actor Don Cheadle
| 
|- 2004 Political Film Society Awards 
|align="center" |Exposé
|align="center" |————
| 
|- Human Rights 
|align="center" |————
| 
|- Peace
|align="center" |————
| 
|- Producers Guild of America Awards 2004  Stanley Kramer Award
|align="center" |————
| 
|- 2006 Robert Awards  Best Non-American Film Terry George
| 
|- San Diego Film Critics Society Awards 2004  Body of Work Don Cheadle
| 
|- 11th Screen Actors Guild Awards  Best Ensemble Acting
|align="center" |————
| 
|- Best Actor Don Cheadle
| 
|- Best Supporting Actress Sophie Okonedo
| 
|- Southeastern Film Critics Association Awards 2004  Best Picture
|align="center" |————
| 
|- 2004 Toronto International Film Festival 
|align="center" |Peoples Choice Award Terry George
| 
|- Washington D.C. Area Film Critics Association Awards 2004   Best Actor Don Cheadle
| 
|- World Soundtrack Awards 2005  Best Original Song Written for a Film Jerry Duplessis, Andrea Guerra, Wyclef Jean 
| 
|- Writers Guild of America Awards 2004  Best Original Screenplay Keir Pearson, Terry George 
| 
|-
|}

===Box office===
The film premiered in cinemas on December 22, 2004 in limited release throughout the U.S. During its limited opening weekend, the film grossed $100,091 in business showing at 7 locations. Its official wide release was screened in theaters on February 4, 2005.    Opening in a distant 14th place, the film earned $2,316,416 showing at 823 cinemas. The film Boogeyman (film)|Boogeyman soundily beat its competition during that weekend opening in first place with $19,020,655.    The films revenue dropped by 11.8% in its second week of release, earning $2,043,249. For that particular weekend, the romantic comedy Hitch (film)|Hitch unseated Boogeyman to open in first place with $43,142,214 in revenue, while Hotel Rwanda remained in 14th place not challenging a top ten position.    During its final weekend in release, the film opened in 62nd place grossing $23,176 in business.  The film went on to top out domestically at $23,530,892 in total ticket sales through an 18-week theatrical run. Internationally, the film took in an additional $10,351,351 in box office business for a combined worldwide total of $33,882,243.  For 2004 as a whole, the film would cumulatively rank at a box office performance position of 99. 

==See also==
 
* 2004 in film
* Radio Télévision Libre des Mille Collines
* Hutu Power
 

==References==
;Footnotes
 

;Further reading
 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 