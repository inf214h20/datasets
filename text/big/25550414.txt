Born of Hope
{{Infobox film
| name           = Born of Hope
| image          = Poster boh ring.jpg
| image_size     = 
| caption        = 
| director       = Kate Madison
| producer       = Kate Madison
| writer         = Screenplay: Paula DiSante (as Alex K. Aldridge) Based on the writings of J. R. R. Tolkien Additional writing: Christopher Dane Kate Madison Matt Wood
| starring       = Christopher Dane Beth Aynsley Danny George Kate Madison Andrew McDonald Philippa Hammond Iain Marshall Howard Corlett
| music          = Tobias Bublat Adam Langston Jacob Shelby Kevin Webster Rob Westwood Peter Bateman Arjan Kiel Toby and Cody McClure Martin Westlake
| cinematography = Neill Phillips
| editing        = Christopher Dane Kate Madison
| studio         = Actors at Work Productions
| released       =  
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         = £25,000   
| gross          =
}} appendices of J. R. R. Tolkiens The Lord of the Rings. The film centres on the communities affected by Saurons war;  the story of Arathorn II and his relationship with Gilraen, and the importance of the Dúnedain bloodline.
 West Stow Snowdonia National Park, Clearwell Caves and the Brecon Beacons.

It debuted at Ring*Con 2009 before being streamed on DailyMotion for free, and later on YouTube.

==Plot==
In the late Third Age, Sauron|Saurons power is increasing, and he has sent his orcs to seek out the remnants of the bloodline of Elendil, kept alive in the Dúnedain. Dirhael, his wife Ivorwen and their daughter Gilraen are fleeing from an attack on their village when they are ambushed by Orcs on a forest road, and saved by a group of rangers led by Arathorn. Not having any place safer to go, the refugees go with Arathorn to Taurdal, the village led by his father and Chieftain of the Dúnedain, Arador. While there, Arathorn and Arador ponder the orcs motives after finding various pieces of jewelry on their bodies. During her stay in Taurdal, Gilraen falls in love with Arathorn.

In light of the attacks on surrounding settlements, Arador leads his forces on a campaign against the orcs in the area in an attempt to restore peace to the region. Meanwhile, he sends Arathorn separately in an attempt to determine the meaning behind the attacks. Both are successful, and Arathorn discovers the orcs are serving Sauron, who seeks the Ring of Barahir. Arathorn and Gilraen receive Aradors blessing to be wed, but Arathorn cannot summon the courage to ask Dirhael for his daughters hand. Arador is summoned to Rivendell to seek Elronds counsel, and the wedding is postponed until his return. Arathorn eventually confronts Dirhael, and receives permission to marry his daughter. Arathorn and Gilraen are married.
 troll in the Coldfells, making Arathorn the chieftain of the Dúnedain. Gilraen becomes pregnant and gives birth to a son, Aragorn. Taurdal knows peace for a while, until Elladan and Elrohir come with news from Rivendell. Elrond has sensed that danger is once again threatening the region, and they request that Gilraen and Aragorn be brought back to Rivendell for safekeeping, as is the tradition with all Dúnedain heirs to the Chieftains of the Dúnedain|chiefdom. Before Arathorn and Gilraen can come to a decision, orcs attack the village. They are beaten off; however, many Rangers fall, and Arathorns closest friend, Elgarain, is mortally wounded while defending Gilraen. Arathorn then leads the remaining Rangers in pursuit of the stragglers. They are successful, but Arathorn is mortally wounded in the process. Without a chieftain capable of leading them, the Dúnedain abandon Taurdal and go into hiding in small secret settlements in the forests of Rhudaur, while the Elven twins, Elladan and Elrohir, bring Aragorn with his mother Gilraen to Rivendell, and safety.

==Cast== Arathorn
*Beth Aynsley as Gilraen, Arathorns wife
*Iain Marshall as Arador, Chieftain of the Dúnedain and father of Arathorn
*  as Dirhael, Gilraens father
*  as Ivorwen, Gilraens mother
*Howard Corlett as Halbaron,* Arathorns right hand and de facto leader of the Dúnedain after his death
*  as Elgarain,* a Ranger and friend of Arathorn Matt and Sam Kennard as Elladan and Elrohir, twin sons of Elrond
*Luke Johnston as Aragorn, son of Arathorn
*Danny George as Dirhaborn,* a Ranger
*Amani Johara as Evonyn,* Halbarons wife
*Raphael Edwards as Mallor,* a Ranger
*Ollie Goodchild and Lars Mattes as Halbarad, Halbarons son
*Phoebe Chambers and Amylea Meiklejohn as Maia,* a refugee escaping from the orcs, adopted by Halbaron and Evonyn
*Tom Quick as Dorlad,* Gilraens brother who was killed in the raid on their village
*Richard Roberts as Shaknar,* orc leader, chief villain of the film
*Lewis Penfold as Gorganog,* orc commander during attack on Taurdal
*Daniel Tyler-Smith as Maias father,* killed during an orc raid 
 *Characters created for the film, they do not appear in Tolkiens legendarium. 

==Production==
The idea for the film was born in 2003 when director/producer/actor Kate Madison wanted to submit a film for the Tolkien Fan Film Exhibition. Originally a modest plan, it grew until April 2006 when the first test shoot occurred. Principal photography started in June 2008, and continued through 2009. The goal was to debut at Ring*Con 2009, which it did. It was later streamed for free on various video websites including DailyMotion and YouTube.

Madison spent her life savings of £8,000 on the film. An extra £17,000 was generated by posting a trailer online, raising the budget to £25,000. Born of Hope was made over a period of six years, using a cast of 400, who would camp in tents so as to be able to shoot early. 

Christopher Dane (Arathorn) ended up getting very involved in the process of making the film, contributing to the script as well as handling the editing of the final product. Kate Madison, who directed and produced the film, was cast as Elgarain as well.

Chris Bouchard of The Hunt for Gollum contributed to the production of the film as a camera operator and effects artist.

==Reception==
Wendy Ide, writing for The Times, gave the film a positive review. She awarded it 4 stars out of 5, calling it a "near note-perfect homage to Jackson’s vision for the Rings". She also noted that the film is "very well cast" and that "practically all the performances have a skill level far above that which is usually evident in Low-budget film|low-budget cinema". 

The Observer stated that Born of Hope was the most credible adaptation among the many fan films based on The Lord of the Rings, and noted that in March 2010, the film had reached almost a million hits at online streaming media. 

==Awards==
Born of Hope won the 2010 London Independent Film Festival in the category "Best Micro-Budget Feature". 

==See also==
 
*The Hunt for Gollum

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 