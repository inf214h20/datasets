All Our Desires
 
{{Infobox film
| name           = All Our Desires
| image          = 
| caption        = 
| director       = Philippe Lioret
| producer       = Marielle Duigou Philippe Lioret Christophe Rossignon Stéphane Célérier 
| screenplay     = Emmanuel Carrère Philippe Lioret
| based on       =  
| starring       = Vincent Lindon Marie Gillain
| music          = Flemming Nordkrog 	
| cinematography = Gilles Henry 
| editing        = Andrea Sedlácková
| distributor    = Mars Distribution
| released       =  
| runtime        = 120 minutes
| country        = France
| language       = French
| budget         = 
}}

All Our Desires ( ) is a 2011 French drama film directed by Philippe Lioret.     Marie Gillain was nominated for the Magritte Award for Best Actress for her role.

==Cast==
* Vincent Lindon as Stéphane
* Marie Gillain as Claire Conti
* Amandine Dewasmes as Céline
* Yannick Renier as Christophe (as Yannick Rénier)
* Pascale Arbillot as Marthe
* Isabelle Renauld as Le docteur Hadji
* Laure Duthilleul as Carole, la mère de Claire
* Emmanuel Courcol as Le docteur Stroesser
* Anna-Bella Dreyfus as Mona
* Thomas Boinet as Arthur
* Léna Crespo as Léa
* Oriane Solomon as Zoé

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 