Rose of Tralee (1942 film)
{{Infobox film
| name           = Rose of Tralee
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Germain Burger
| producer       = F. W. Baker
| writer         = Kathleen Butler H. F. Maltby
| story          = Oswald Mitchell Ian Walker Kathleen Tyrone
| starring       = John Longden Lesley Brook Angela Glynne
| music          = 
| cinematography = Geoffrey Faithfull
| editing        = A. Charles Knott
| studio         = 
| distributor    = Butchers Film Service
| released       = 1942
| runtime        = 77 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British musical film directed by Germain Burger and starring John Longden, Lesley Brook and Angela Glynne.  An Irish singer heads to America to pursue their dream of stardom.

==Cast==
* John Longden - Paddy OBrien
* Lesley Brook - Mary OBrien
* Angela Glynne - Rose OBrien
* Mabel Constanduros - Mrs Thompson
* Talbot OFarrell - Tim Kelly
* Gus McNaughton - Gleeson
* George Carney - Collett
* Virginia Keiley - Jean Hale
* Iris Vandeleur - Mrs Crawley
* Morris Harvey - McIsaac

==See also==
*The Rose of Tralee (song)
*Rose of Tralee (1937 film)

==References==
 

==External links==
* 

 
 
 
 


 
 