Hero (1987 film)
{{Infobox film
| name           = Hero
| image          = Hero DVD Cover.jpg
| image_size     = 
| caption        = Hero DVD/VHS Cover
| director       = Tony Maylam
| producer       = Drummond Challis Lee Stern
| writer         = Tony Maylam
| narrator       = Michael Caine
| starring       = 
| music          = Rick Wakeman
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} official documentary film of the 1986 FIFA World Cup held in Mexico.  , FIFAFilms.com. Accessed July 10, 2008. 

The film was narrated by Michael Caine and the music score was written by Rick Wakeman. Hero found the most success in Argentina since the focal point of the film was Diego Maradona and his role in the quarter-final match against England national football team|England, more specifically the Hand of God goal and the Goal of the Century.  The match and the championship were eventually won by Argentina national football team|Argentina.  The film also enjoys cult status in the UK.

==Distribution==
*West Germany: July 2, 1987
*Italy: June 7, 1990 (TV premiere)
   

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 