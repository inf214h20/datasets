Midnight Menace
{{Infobox film
| name           = Midnight Menace
| image          = 
| image_size     = 
| caption        = 
| director       = Sinclair Hill
| producer       = Harcourt Templeman
| writer         = Alexander Mackendrick   Roger MacDougall   George Moresby-White  D.B. Wyndham-Lewis 
| cinematography = Cyril Bristow
| editing        = John E. Morris
| music          = John Reynders
| narrator       = 
| starring       = Charles Farrell   Margaret Vyner   Fritz Kortner
| studio         = Grosvenor Films
| distributor    = Associated British Film Distributors
| released       = 1 July 1937
| runtime        = 79 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British thriller Danny Green. An international arms manufacturing firm plans to start a war in Europe by bombing London.   It was released in the United States as Bombs Over London. Its original script was written by Alexander Mackendrick. 

==Cast==
* Charles Farrell - Brian Gaunt
* Margaret Vyner - Mary Stevens
* Fritz Kortner - Peters Danny Green - Socks
* Wallace Evennett - Smith
* Monti DeLyle - Pierre
* Dino Galvani - Tony
* Arthur Finn - Mac, Newspaper Editor 
* Laurence Hanray - Sir George, Lead Conspirator 
* Arthur Gomez - Baron von Kleisch, Delegate 
* Raymond Lovell - Harris
* Evan John - Doctor Marsh 
* Reynes Barton - Conference President  Terence OBrien - Secret Agent Fearns 
* Dennis Val Norton - Vronsky, Peters Aide
* Billy Bray as Banks  
* Sydney King as Graham Stevens 
* Andreas Malandrinos as Zadek 
* Victor Tandy as Groves

==References==
 

 

 
 
 
 
 
 


 
 