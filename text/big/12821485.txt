Blood Wedding (1981 film)
{{Infobox film
| name           = Blood Wedding
| image          = Bodasdesangre.jpg
| image size     =
| caption        =
| director       = Carlos Saura
| producer       = Emiliano Piedra	
| writer         = Carlos Saura Antonio Artero Antonio Gades Alfredo Mañas
| narrator       =
| starring       = Antonio Gades Cristina Hoyos Juan Antonio Jiménez
| music          = Emilio de Diego
| cinematography = Teodoro Escamilla
| editing        = Pablo González del Amo
| distributor    =
| released       = March 9, 1981 Spain
| runtime        = 72 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}
 1981 Spain|Spanish El amor brujo (1986).
 Blood Wedding. As with all Sauras flamenco films, the film is overtly theatrical: it begins with the company arriving at the studio and putting on costumes and makeup. The dance is then performed in a bare windowed space with a minimum of props and no set. There are no elaborate costumes and many of the actors wear only their rehearsal clothes.

It was shown out of competition at the 1981 Cannes Film Festival.   

==Cast==
*Antonio Gades as Leonardo
*Cristina Hoyos as Bride
*Juan Antonio Jiménez as Groom
*Pilar Cárdenas as Mother
*Carmen Villena as Wife

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 
 