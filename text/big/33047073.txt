Tahrir 2011: The Good, the Bad, and the Politician
{{Infobox film
| name           = Tahrir 2011 : The Good, The Bad and the Politician
| image           = The Good, the Bad, and the Politician.jpg
| director       = Tamer Ezzat   Ahmad Abdalla   Ayten Amin   Amr Salama
| producer       = Frederic Sichler   Mohamed Hefzy
| distributor    = Pacha Pictures
| released       =  
| runtime        = 90 minutes
| country        = Egypt
| language       =  Arabic
}}

Tahrir 2011: The Good, the Bad and the Politician ( ) is an Egyptian documentary directed by Tamer Ezzat, Ahmad Abdalla, Ayten Amin and Amr Salama. The film is divided in three parts covering respectively the protesters, the police forces and a profile of Hosni Mubarak by several political figures. The film mixes interviews and real footage from the demonstrations.  It premiered at 68th Venice International Film Festival as an out of competition feature film   and the 2011 Toronto International Film Festival in the Mavericks section. 

==See also== The Square, a 2013 Egyptian-American documentary film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 
 