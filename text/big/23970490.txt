Veerathirumagan
{{Infobox film
| name           = Veera Thirumagan
| image          = 
| caption        = 
| director       = A. C. Tirulokchandar
| producer       = A. V. Meiyappan
| writer         = A. C. Tirulokchandar
| screenplay     = 
| story          = 
| starring       = C. L. Anandan Sachu E. V. Saroja
| music          = Viswanathan–Ramamoorthy
| cinematography = 
| editing        = 
| studio         = AVM Productions
| distributor    = AVM Productions
| released       =  
| runtime        = 
| country        =   India Tamil
| budget         = 
| gross          = 
}}

Veerathirumagan is 1962 Tamil-language film produced by AVM Productions, and directed by A. C. Tirulokchandar.  This was debut film for A. C. Thirulokachandar.

It starred C. L. Anandan, Sachu, E. V. Saroja with music by Viswanathan–Ramamoorthy. The film released in Telugu as Prajasakthi.

==Soundtrack==
The music composed by Viswanathan–Ramamoorthy. 

The movie is very popular for its songs, Vethala Potta Pathini Ponnu, Paadatha Paatellam and Roja Malarae, were very popular even today.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:35
|-
| 2 || Kettathu || L. R. Eswari || 04:20
|-
| 3 || Neelapattadai Katti || P. Susheela, L. R. Eswari || 04:32
|-
| 4 || Paadatha || P. B. Sreenivas, S. Janaki || 03:12
|-
| 5 || Roja Malarae || P. B. Sreenivas, P. Susheela || 03:00
|-
| 6 || Vethala Potta || T. M. Soundararajan, Sadan || 03:56
|-
|}

==References==
 

 

 
 
 
 
 
 


 