Agent Vinod (1977 film)
{{Infobox film
| name           = Agent Vinod
| image          = Agent Vinod.jpg 
| caption        = 
| director       = Deepak Bahry
| producer       = Tarachand Barjatya
| writer         = Khalid-Narvi-Girish
| narrator       = 
| starring       =Mahendra Sandhu Nazir Hussain
| music          =Raam Laxman
| cinematography = Arvind Laad
| editing        = Mukhtar Ahmed
| distributor    = Rajshri Productions
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} action Spy film directed by Deepak Bahry. The film stars  Mahendra Sandhu as a dashing Indian secret agent and Jagdeep as a comic side-kick. This movie turned out to be a surprise hit. 

==Story==
The kidnapping of a prominent scientist, Ajay Saxena (Nazir Hussain) prompts the Chief of Secret Services (K.N. Singh) to assign flamboyant Agent Vinod (Mahendra Sandhu) to this case. While on this assignment, Vinod meets with Ajays daughter, Anju (Asha Sachdev), who insists on assisting him. The duo are then further assisted by Chandu James Bond (Jagdeep) and his gypsy girlfriend (Jayshree T.). The two couples will soon have numerous challenges thrust on them, and will realize that their task is not only very difficult but also life threatening.

==Cast==
*Mahendra Sandhu as Agent Vinod
*Asha Sachdev as Anju Saxena
*Rehana Sultan as Zarina
*Jagdeep as Chandu alias James Bond
*Iftekhar as Madanlal
*Pinchoo Kapoor as Chacha of Agent Vinod
*Nazir Hussain as Ashok Saxena (Anjus dad) (as Nazir Husain)
*Ravindra Kapoor
*K.N. Singh as Chief of Agent Vinod Helen as Dancer - Lovelina
*Jayshree T. as Gypsy sardars daughter
*Leena Das as Leena (Chachas assistant)

== References ==
 

==External links==
*  

 
 
 
 
 
 


 
 