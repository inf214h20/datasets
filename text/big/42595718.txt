Bridge of Glass (film)
{{Infobox film
| name = Bridge of Glass
| image =
| image_size =
| caption =
| director = Goffredo Alessandrini
| producer = 
| writer =  Gerardo De Angelis     Piero Ballerini 
| narrator =
| starring = Isa Pola   Rossano Brazzi   Filippo ScelzoDonadio   Regina Bianchi 
| music = Edgardo Carducci  
| cinematography = Ubaldo Arata 
| editing = Mario Bonotti   Eraldo Da Roma 
| studio =   Scalera Film 
| distributor = Scalera Film 
| released = 16 February 1940 
| runtime = 85 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Bridge of Glass (Italian:Il ponte di vetro) is a 1940 Italian comedy film directed by Goffredo Alessandrini and starring Isa Pola, Rossano Brazzi and Filippo Scelzo.  It was shot at the Scalera Studios in Rome.

==Cast==
*Isa Pola as Luciana Dorelli
*Rossano Brazzias  comandante Mario Marchi
*Filippo Scelzo as dottor Paolo Dorelli
*Regina Bianchi as Anna
*Carlo Romano as  Leone
*Adriano Rimoldi 
*Renato Chiantoni 
*Fedele Gentile  Walter Grant 
*Mario Lodolini
*Felice Romano

== References ==
 

== Bibliography ==
* Savio, Francesco. Ma lamore no. Sonzogno, 1975. 
 
== External links ==
*  

 

 
 
 
 
 
 

 