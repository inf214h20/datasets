Quiz Whizz
{{Infobox Film |
  | name           = Quiz Whizz
  | image          =QuizwhizzTITLE.jpg
  | caption        = 
  | director       = Jules White 
  | writer         = Searle Kramer Bill Brauer
  | cinematography = Irving Lippman| William A. Lyon
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 15 25"
  | country        = United States
  | language       = English
}}

Quiz Whizz is the 183rd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Bill Brauer) by investing his winnings in Consolidated Fujiyama California Smog Bags. The Stooges head to their offices to get Joes money back. Instead, they find two sympathetic businessmen (the two crooks, in disguise) who offer to pay back the losses if Moe, Larry and Joe will pose as juvenile wards for a rich and eccentric millionaire, Montgomery M. Montgomery (Gene Roth). But Montgomery is actually Princes and Broads gang leader, and plotting to kill the Stooges.
 

==Production notes==
Quiz Whizz features Moe and Larrys more "gentlemanly" haircuts, first suggested by Joe Besser. However, these had to be used sparingly, as most of the shorts with Besser were remakes of earlier films, and new footage had to be matched with old. However, in Quiz Whizz, Larrys frizz is combed back, while Moe retained his sugarbowl bangs. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion, p. 502-503; Comedy III Productions, Inc., ISBN 0-9711868-0-4 

==Quotes==
*Moe (frantically): "Id like to report a missing person. Name? Joe Besser. Height? Well, hes about 5-foot-5 by 5-foot-5. Color of hair? Skin!"

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 