Yahudi Ki Ladki (1933 film)
{{Infobox film
| name           = Yahudi Ki Ladki
| image          = 
| image_size     = 
| caption        = 
| director       = Premankur Atorthy
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = K. L. Saigal Rattan Bai Pahari Sanyal Nawab
| music          = Pankaj Mullick
| cinematography = Nitin Bose
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1933
| runtime        = 137 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1933 Urdu/Hindi costume drama film directed by Premankur Atorthy.    Produced by New Theatres Ltd. Calcutta, the cast included K. L. Saigal, Rattan Bai, Pahari Sanyal, Gul Hamid,  Nawab and Kumar (actor)|Kumar.    The film was adapted from Agha Hashar Kashmiri’s play of the same name Yahudi Ki Larki which had been written in Bengali as Misar Kumari. He also wrote the screenplay and lyrics. The film saw Pankaj Mullicks debut as a Hindi music director. The dialogues were by Wajahat Mirza.    The story revolves around the rivalry and revenge between the Jewish merchant Prince Ezra and the Roman priest Brutus.

==Synopsis==
Prince Ezra (Nawab) is a Jewish merchant persecuted by the Romans, especially the Roman Priest Brutus. Ezra lives with his seven-year-old son Yameen. One day while playing with his catapult, Yameen manages to unintentionally hit the Priest Brutus as he’s walking past. The boy is taken prisoner and no imploring can change Brutus’ punishment. He has the boy thrown to the animals. Elias, Ezra’s devoted slave kidnaps Brutus’ young daughter Decia and brings her to a grief-stricken Ezra for revenge. Unable to harm her and missing Yameen, Ezra brings Decia up as his daughter and names her Hannah. 

Years later, a grown Hannah (Rattan Bai) is rescued by the Roman Marcus (K. L. Saigal) from Roman soldiers who have accosted her. Since Marcus is in disguise of a Jew, Hannah believes him to be one and brings him home. The two fall in love though Marcus is engaged to the Princess Octavia (Tarabai). Hannah soon finds out that Marcus is a Roman. After some pleading and pacifying both Ezra and Hannah, Marcus manages to get Ezra to agree to their marriage. Ezra wants him to convert to a Jew and Marcus refuses and leaves. On reaching home he agrees to marry Octavia;  on the day of the wedding Hannah recognises him. Both Ezra and Hannah plead with the Emperor to punish Marcus for being unfaithful. The Emperor orders for Marcus to be put on trial. Octavia begs Hannah to retract her statement. When she does so,  Brutus has them arrested for lying against the prince. They are sentenced to be burnt. Ezra first extracts a promise from Brutus that on his telling him where his long lost daughter Decia is he will immediately have Hannah burnt in oil. Brutus impatiently agrees. Ezra now tells Brutus the truth about Hannah being his daughter Decia. Brutus is humbled as he realises the futility of race and religion. Hannah sees a saintly figure and follows him renouncing her life. 

==Cast==
*K. L. Saigal as Prince Marcus Hannah
*Nawab Kashmiri as Ezra
*Gul Hamid  
*Tarabai as Princess Octavia
*Pahadi Sanyal as the Roman Emperor Kumar
*Nemo
*Ghulam Mohammed
*Radharani

==Music==
One reason for the popularity of New Theatres films was its music. It had three of the renowned music directors of its time working for them: Timir Baran, R. C. Boral and Pankaj Mullick. Yahudi Ki Ladki film was the debut of Pankaj Mullick.       Saigal’s rendition of Ghalib’s ghazal "Nuktacheen Hai Ghame Dil" is considered a classic performance. Sung under the direction of Pankaj Mullick who composed it in Raag Bhimpalasi, the only instruments used were the tabla and harmonium#On the Indian subcontinent|harmonium.    The other popular songs of K. L. Saigal from the film were "Lag Gayi Chot Karejwa Mein", "Yeh Tasarruf Allah Allah" and "Lakh Sahi Ab Pi Ki Batiyan".

===Songs===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Min
|-
| 1
| "Ye Tasarruf Allah Allah Tere Maikhane Mein Hai" 
| K. L. Saigal
| 3.16
|-
| 2
| "Lag Gayi Chot Karejwa Mein"
| K. L. Saigal 
| 3.15
|-
| 3
| "Lakh Sahi Hain Pi Ki Batiyan" 
|  K. L. Saigal
| 3.07
|-
| 4
| "Nuktacheen Hai Ghame-Dil" 
| K. L. Saigal
| 
|-
| 5
| "Ab Shaad Hai Dil"
| Utpala Sen
| 3.09
|-
|}

 
==References==
 

==External Links==
* 

 
 
 
 
 
 
 