Kanave Kalaiyadhe
{{Infobox film
| name           = Kanave Kalayadhe
| image          =
| director       = V. Gowthaman
| writer         = Murali Simran Simran Shakti Kumar Charle Chinni Jayanth Dhamu Kovai Sarala
| producer       = Sivasakthi Pandian
| distributor    =
| cinematography = Thangar Bachan
| editing        = B. Lenin V. T. Vijayan
| released       = 6 August 1999
| runtime        =
| language       = Tamil Deva
}}
 1999 Tamil Tamil film directed by score and soundtrack was composed by Deva (music director)|Deva. It was produced by Sivasakthi Movie Makers.

== Cast == Murali
*Simran
*Shakti Kumar
* Chinni Jayanth
* Charle
* Dhamu Ponnambalam

== Plot ==
Anand (Murali (Tamil actor)|Murali) and Amrita (Simran) are lovers. Amrita hails from Punjab while Anand is a Tamilian. In order to seek the approval of Amritas parents for their marriage, Anand goes to Amritas home in Punjab. But before they can unite, Amrita and her family die in a freak accident. Unable to bear the separation, Anand goes into depression. His friends take him to Chennai with the hope that he will start life afresh. But there, life throws a googly at him when he sees Sharada (also played by Simran), Amritas look-alike. After the initial hiccups, Anand and Sharada fall in love with each other and decide to get married. But at this point, Anand learns that Shekar, Sharadas old lover, is still waiting for her. How is Anand going to handle this strange game of fate?

== Soundtrack ==
 Deva and lyrics by Vairamuthu. 

*"Kannil Unnai" - K. S. Chitra
*"Kannodu Kannodu"  - P. Unni Krishnan, Shobana Hariharan
*"Dilli Thaandi"  -  K. S. Chitra
*"Poosu Manjal"   -  Anuradha Paudwal Deva

== References ==
 

 
 
 
 


 