Kentucky (film)
 
{{Infobox film
| name           = Kentucky
| image          = Kentucky1938.JPG
| caption        =
| producer       = Gene Markey Darryl F. Zanuck David Butler
| writer         = Lamar Trotti John Taintor Foote
| starring       = Loretta Young Richard Greene Walter Brennan
| music          = Louis Silvers Ernest Palmer Ray Rennahan
| editing        = Irene Morra
| distributor    = 20th Century Fox
| released       =  
| budget         =
| gross          =
| runtime        = 96 min
| language       = English
}}
 David Butler. Civil War and is kept alive by Sallys Uncle Peter.

==Plot==
During the Civil War, Thad Goodwin (Charles Waldron) of Elmtree Farm, a local horse breeder resists Capt. John Dillon (Douglass Dumbrille) and a company of Union soldiers confiscating his prize horses. He is killed by Dillon and his youngest son Peter (Bobs Watson) cries at the soldiers riding away with the horses.

75 years later, in 1938, Peter (Walter Brennan) now a crotchety old man, still resides on Elmtree Farm and raises horses with his niece Sally (Loretta Young). Dillons grandson Jack (Richard Greene) and Sally meet, her not knowing that he was a Dillon. Peter Goodwin dies when his speculation on cotton drops. The Goodwins are forced to auction off nearly all their horses and Jack offers his services to Sally, as a trainer of their last prize horse, "Bessies Boy", who falls ill.

Sally eventually loses the farm, and Mr. Dillon makes good on his original bet with Thad and  offers her any two-year-old on his farm. She picks "Blue Grass" instead of the favorite, "Postman", and Peter trains him for the Derby. She eventually learns of Jacks real identity. During the race, Blue Grass runs neck and neck with the Dillons horse Postman, but Blue Grass wins. Sally embraces Jack, but Peter collapses before the decoration ceremony and dies. At his funeral, Dillon eulogizes him and of the American life of the past, as "The Grand Old Man of the American Turf".

==Cast==
As appearing in screen credits (main roles identified):   
{| class="wikitable" width="60%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Loretta Young || Sally Goodwin
|-
| Richard Greene || Jack Dillon
|-
| Walter Brennan || Peter Goodwin
|-
| Douglass Dumbrille || John Dillon – 1861
|-
| Karen Morley || Mrs. Goodwin – 1861
|-
| Moroni Olsen || John Dillon – 1938
|- Russell Hicks || Thad Goodwin Sr. – 1861
|-
| Willard Robertson || Bob Slocum
|-
| Charles Waldron || Thad Goodwin – 1938
|-
| George Reed || Ben
|-
| Bobs Watson || Peter Goodwin – 1861
|-
| Delmar Watson || Thad Goodwin Jr. – 1861
|-
| Leona Roberts || Grace Goodwin
|- Charles Lane || Auctioneer
|-
| Charles B. Middleton || Southerner
|}

A full cast and production crew list is too lengthy to include, see: IMDb profile. 

==Notes==
Walter Brennan won his second Oscar (Best Supporting Actor) in his role as Peter Goodwin.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 