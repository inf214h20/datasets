Sweet Emma, Dear Böbe
 
{{Infobox film
| name           = Sweet Emma, Dear Böbe
| image          =
| caption        = 
| director       = István Szabó
| producer       = Gabriella Grósz
| writer         = Andrea Vészits István Szabó
| screenwriter   = István Szabó
| starring       = Johanna ter Steege Enikő Börcsök
| music          = Mihály Móricz   Tibor Bornai Feró Nagy Robert Schumann
| cinematography = Lajos Koltai
| editing        = Eszter Kovács
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}
 Silver Bear - Special Jury Prize.   

==Plot==
The film shows the political systems changes in Budapest. The Sweet Emma, Dear Böbe shows the losers of the change and their searching of way. After that Russian language had been removed from compulsory subjects of Hungarian schools the two Russian teacher, Emma and Böbe became redundant. The teaching staff also were shaken of insecurity, accusing each other. Emma and Böbe are learning English in the evenings. Emma also sells newspapers and she has an affair with the married school director, who is not too brave to decide. One of the best scenes of the film when Böbe and many naked women - teachers and nurses - are waiting for casting in a film studio. Böbe is acquainted with foreigners and she trades in foreign currencies. Böbe will be arrested and she finally jumps out of the window of the teachers accommodation.

"Szabós sensitive handling of the material culminates in a meditative passage in which Emma stands in church, musing on the passion for love which masks lack of purpose. Collective sin may be dead, according to Böbe, but this movingly delineates the private pain of atonement"- Time Out Film Guide.

==Cast==
* Johanna ter Steege as Emma
* Enikö Börcsök as Böbe
* Péter Andorai as Stefanics - Director
* Éva Kerekes as Szundi
* Ildikó Bánsági as Emma (voice)
* Irma Patkós as Hermina
* Erzsi Pásztor as Rózsa
* Hédi Temessy as Mária
* Irén Bódis as Emmas mother
* Erzsi Gaál as Storekeeper
* Zoltán Mucsi as Szilárd, Art teacher
* Tamás Jordán as Szaglár Capt.
* Gábor Máté (actor)|Gábor Máté as Officer

==Awards==

*1992 Berlin International Film Festival, Won Prize of the Ecumenical Jury - Special Mention,  Competition- István Szabó 
*1992 Berlin International Film Festival, Silver Berlin Bear, Special Jury Prize- István Szabó 
*1992 European Film Award, Best Screenwriter- István Szabó

==References==
 
* 

==External links==
* 
* 
* 

 

 
 
 
 
 
 