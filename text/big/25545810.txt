Mambattiyan
{{Infobox film
| name           = Mambattiyan
| image          = "Mambattiyan, 2011".jpg
| alt            =  
| caption        = Movie Poster
| director       = Thiagarajan
| producer       = Thiagarajan
| writer         = Thiagarajan
| based on       =  
| starring       =  
| music          = S. Thaman
| cinematography = Shaji Kumar
| editing        = Don Max
| studio         = Lakshmi Shanthi Movies 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| gross          = 
}} Tamil action Prashanth as the titular character.  The film, also starring Meera Jasmine, Prakash Raj, Vadivelu and Mumaith Khan in other pivotal roles, is a remake of the 1983 Tamil blockbuster Malaiyoor Mambattiyan that starred Thiagarajan himself and Saritha. The film released on December 16, 2011 to above average reviews from critics.  

==Plot==
IG ranjith recalls the memories of a criminal who he failed to caputure. landlord (Kota Srinivasa Rao) rules a village in western Tamil Nadu. When he is opposed by Mambattiyans father (Vijayakumar (actor)|Vijayakumar), the jameen kills him. Coming to know of this, Mambattiyan (Prashanth (actor)|Prashanth) kills the influential person and those in support of him.

A group of youngsters in the village join hands with Mambattiyan. They lead a life in a forest. By robbing the rich and distributing the wealth to the poor, Mambattiyan becomes the local Robin Hood.

Meanwhile, police forces led by IG Ranjith (Prakash Raj) go from pillar to post to nab Mambattiyan. This is the start of a cat and mouse game between the two. Also, Mambattiyan has romance in the form of Kannathal (Meera Jasmine) and Sornam (Mumaith Khan). When Sornam raises a green flag that means she needs him to come an officer in disguises realizes this and reports it to Ranjith, so one by one all of his gang members die.Meanwhile kannathal panicked and she asked mabattiyan to go to a farway place and asked to forgo his robinhood job.mambattiyan agrees and asked him to wait at the river.so,with the help of bullet kannathal waits at the river,Mambattiyan defeats the police forces and escape At the end a villager, Bullet, shoots him because whoever captures Mambattiyan gets a rewards of money and land so Kannathal hits him and he dies then she dies too. at the end Ranjith is sad that the whole village is crying and he takes Mambattiyans chain.

==Cast== Prashanth as Mambattiyan
* Meera Jasmine as Kannaathal
* Mumaith Khan as Sornam
* Prakash Raj as IG Ranjith
* Vadivelu as Silk Singaram Vijayakumar as Mambattiyans father
* Kota Srinivasa Rao as Annachchi
* Kalairani as Village Doctor
* Ganeshkar as Driver
* Riyaz Khan as Fake Mambattiyan
* Aravinth as Oomaiyan
* Manobala as Annachchis Accountant
* Hemalatha
* Raaghav in a special appearance

==Production==
It was first reported in September 2008, that Thiagarajan would remake Rajasekhar (director)|Rajasekhars successful 1983 film Malaiyoor Mambattiyan, with his son Prashanth portraying the lead role of a Robin Hood-esque figure. Meera Jasmine, Prakash Raj and Mumaith Khan were assigned roles shortly after, while the team also held talks with Kalabhavan Mani unsuccessfully for another role.   The team began filming in early 2009, with Thiagarajan noting that apart from the plot, most features in the film would be different from the original. Prashanth grew his hair and put on weight for this film and the team shot in the forests of Tamil Nadu, Kerala and Karnataka.  The team also filmed scenes in virgin locales across Orissa in the backdrop of waterfalls. 
 Thaman to Ponnar Shankar, and subsequently released that film earlier than Mambattiyan. The film finally geared up for release in December 2011, more than three years after production began, when the son of a tribal leader called Mambattiyan filed a petition preventing the release of the film. He withdrew the claims shortly after, gaining an out of court settlement.  Prior to the films release, Thiagarajan considered a possible sequel to the venture, stating he would decide after seeing the audiences response. 

==Release==
The film opened in December 2011 to above average reviews from film critics. Malathi Rangarajan of The Hindu claimed that "Thyagarajan has developed and executed a screenplay without gaffes, which gallops at commendable speed. Eschewing frills, he holds the viewers attention throughout". In her review, she also added "in action, comedy, sadness and sedateness, Prashanth always makes a mark" and that "Mambattiyaan should be a milestone in Prashanths career".  The New Indian Express also gave the film a positive review, noting "For, makers while replicating the content, invariably fail to capture the soul of the original. But, Thiagarajan has managed to recapture the essence of his earlier work, even while giving the rustic saga a more contemporary feel. The screenplay is taut, the director rarely losing his grip on his narration."  A reviewer from Sify.com noted the film "is a bit dated, though it moves at a rapid pace" and that it "is not engaging like the original".  Another critic from Sify also noted "It is predictable, tame, out-dated and slow. No single scene could be cited as well conceived or well made. Full of cliches".  A critic from Behindwoods.com noted "Mambattiyan could have been so much better if the period setting was maintained in the 80s or if the characters were moulded to suit current times". 

The film took a good opening at the box office, but petered out to do average business commercially and due to its big budget, failed to recover costs.  The film was later dubbed and released in Telugu as Bebbuli. 

==Soundtrack==
{{Infobox album
| Name = Mambattiyan
| Type = soundtrack
| Artist = S. Thaman
| Cover = 
| Caption = Front Cover
| Released =  
| Recorded =
| Genre = Film soundtrack
| Length = 
| Label = Saga Music
| Producer = S. Thaman
| Last album = Mouna Guru (2011)
| This album = Mambattiyan (2011)
| Next album = Bodyguard (2012 film)|Bodyguard (2011)
}} Thaman and was released live on Sun Musics channel on 23 November 2011.  Actor Silambarasan recorded a song for the film, with the making of the song video being used for promotional purposes. 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)
|- 1 || "Chinna Ponnu" || Harish Raghavendra, Shaila 
|- 2 || "Karuppanswamy" || Ranjith (singer)|Ranjith, Priya Himesh 
|- 3 || "Kaattuvazhi" || Thiagarajan 
|- 4 || "Kaattuvazhi Remix" || Silambarasan 
|- 5 || "Malaiyiru" || Thaman, Srivardhini, Megha (singer)|Megha, Janani, Rita 
|- 6 || "Yedho Agudhey" || Rita 
|- 7 || "Kaattuvazhi" || Silambarasan 
|}

==References==
 

==External links==
*  

 
 
 
 
 