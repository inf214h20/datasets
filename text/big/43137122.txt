The Heart of Broadway
 
{{infobox film
| title          = The Heart of Broadway
| image          =
| imagesize      =
| caption        =
| director       = Duke Worne
| producer       = Duke Worne
| writer         = Arthur Hoerl
| starring       = Pauline Garon
| music          =
| cinematography = Walter L. Griffin
| editing        = Malcolm Sweeney
| distributor    = Rayart
| released       = January 1928
| runtime        = 6 reel#Motion picture terminology|reels; 60 minutes
| country        = US
| language       = Silent English intertitles

}}
The Heart of Broadway is a 1928 silent film melodrama directed by Duke Worne and starring Pauline Garon. Worne produced and Rayart Films distributed.  

The film is preserved at Bois dArcy(France) and the Library of Congress.  

==Cast==
*Pauline Garon - Roberta Clemmons
*Robert Agnew - Billy Winters (as Bobby Agnew)
*Wheeler Oakman - Dandy Jim Doyle
*Oscar Apfel - Dave Richards
*Duke R. Lee - Duke Lee

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 