Mappusakshi
{{Infobox film
| name           = Mappusakshi
| image          =
| caption        =
| director       = P. N. Menon (director)|P. N. Menon
| producer       = United Producers
| writer         = MT Vasudevan Nair
| screenplay     = MT Vasudevan Nair Madhu Jayabharathi Balan K Nair Kuthiravattam Pappu
| music          = MS Baburaj Ashok Kumar
| editing        = Ravi
| studio         = United Films
| distributor    = United Films
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by P. N. Menon (director)|P. N. Menon and produced by United Producers. The film stars Madhu (actor)|Madhu, Jayabharathi, Balan K Nair and Kuthiravattam Pappu in lead roles. The film had musical score by MS Baburaj.   

==Cast== Madhu
*Jayabharathi
*Balan K Nair
*Kuthiravattam Pappu
*Nellikode Bhaskaranf

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Sreekumaran Thampi and Mankombu Gopalakrishnan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Pakalukal Veenu || P Jayachandran || Sreekumaran Thampi || 
|-
| 2 || Udayam Kizhakku thanne || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 3 || Vrischika Karthika poo || S Janaki || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 