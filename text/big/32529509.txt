The Secret Garden (1987 film)
 
 
{{Infobox film
| name           = The Secret Garden
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Alan Grint
| producer       = Steve Lanning
| based on       =  
| starring       = Gennie James Barret Oliver Jadrien Steele Billie Whitelaw Derek Jacobi Alison Doody Colin Firth John Cameron
| cinematography = 
| editing        = 
| studio         = Rosemont Productions Limited Hallmark Hall of Fame
| distributor    = CBS
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English}}

The Secret Garden is the 1987 Hallmark Hall of Fame TV film adaptation of the novel The Secret Garden, aired on CBS November 30, 1987 and produced by Rosemont Productions Limited, who also produced Back to the Secret Garden. The movie starred Barret Oliver, Jadrien Steele, Billie Whitelaw and Sir Derek Jacobi.

==Plotline==

The story is told as a flashback of the adult Mary (Irina Brook) returning to Misselthwaite Manor after World War I, during which she worked as a nurse in a hospital. She looks for the key to the secret garden, but doesnt find it, so she sits down and remembers her childhood. 

The main story begins in colonial India where the young, neglected and selfish Mary Lennox (Gennie James) waking in the night to find her servants not answering and her parents having a late dinner party. The dinner guests discuss a cholera epidemic that is wiping out their servants, but Marys vain, shallow mother cares only about attending another party. Only moments later, Colonel Lennox collapses. The following morning, Mary wakes to find her parents dying and all their servants either dead or fled. She is discovered by English officers and is soon sent to live with a friend of the family named Mr. Craven (Derek Jacobi), even though the two of them have never before met. 
 moors of Yorkshire. She is shocked and disappointed when the servants do not defer to her as they did in India. While adjusting to life in England, Mary meets the maid Martha (Cassie Stuart) who tells her the story of a secret walled garden that was locked up, with the key thrown away, after the late Mrs. Craven died there. Mary distracts herself from her loneliness and boredom by searching for the door to this garden. Eventually, she finds both, only to learn that the garden has fallen to ruin. With the help of Marthas brother Dickon (Barret Oliver), Mary works to revive the garden.

Meanwhile inside Misselthwaite Manor, Mary frequently wakes in the night to the ghostly sounds of sobbing. The servants insist that the sound is the wind, but one night Mary goes exploring and discovers Mr. Cravens bed-bound son Colin who(Jadrien Steele), who weeps incessantly because he is convinced he is going to die because he looks like a wee girl just and pretends he cant walk. Everyone in the house hates and hopes he will finally die because of his bad temper. The two gradually become friends as Mary tells him about his mother’s garden and how she and Dickon have been restoring it. At last Colin is curious enough that he demands to see this garden.

With Dickons help, Mary take Colin in his wheelchair to visit the garden in secret. Soon Colin declares that the garden must be magic, which inspires him to take his first steps unassisted.  The house gardener Ben Weatherstaff (Michael Hordern), who has been spying on the children, witnesses this and is amazed. Ben offers to help revive the garden as well, and Colin tries to learn to stand and walk by himself.

Far away in London, Mr. Craven receives a letter from Dickons mother Susan insisting he return to Misselthwaite Manor at once. Mr Craven arrives to discover the secret garden in full bloom, with the children gathered there. Colin rises and walks to his father for the first time, announcing that he is well now and will live forever.

When the adult Mary finishes remembering her childhood, Ben Weatherstaff greets her and gives her the key to the secret garden. They discuss what happened to Dickon, who died in the war, at the Forest of Argonne. Then the adult Colin (Colin Firth) enters the garden, having been wounded and released from the hospital. He says he has asked Mary to marry him before, but she never answered. She says she has been waiting for him to ask her in their garden. Colin proposes again, and Mary accepts.

==Production==

===Filming Locations===
Highclere Castle was used for interior and exterior settings of Misslethwaite Manor.

===Soundtrack=== Nocturne no. 19 as its main theme.

==External links==
* 

 

 
 
 
 
 