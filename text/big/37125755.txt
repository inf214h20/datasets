12 Years a Slave (film)
 
 
{{Infobox film
| name           = 12 Years a Slave
| image          = 12 Years a Slave film poster.jpg
| alt            = 
| caption        = Theatrical release poster Steve McQueen
| producer       = {{Plainlist|
* Brad Pitt
* Dede Gardner
* Jeremy Kleiner
* Bill Pohlad
*Steve McQueen
* Arnon Milchan
* Anthony Katagas
}}
| screenplay     = John Ridley
| based on       =  
| starring       = {{Plainlist|
 
* Chiwetel Ejiofor
* Michael Fassbender
* Benedict Cumberbatch
* Paul Dano
* Paul Giamatti
* Lupita Nyongo
* Sarah Paulson
*Brad Pitt
* Alfre Woodard
}}
| music          = Hans Zimmer
| cinematography = Sean Bobbitt Joe Walker
| production companies   = {{Plainlist|
* Regency Enterprises
*River Road Entertainment Plan B Film4
}}
| distributor    = Fox Searchlight Pictures (United States) Entertainment One and Summit Entertainment (United Kingdom) Universal Pictures (Germany) 
| released       =  
| runtime        = 134 minutes 
| country        = {{Plainlist|
*United States
*United Kingdom
}}
| language       = English
| budget         = $22 million 
| gross          = $187.7 million    
}} period drama film and an adaptation of the 1853 slave narrative memoir Twelve Years a Slave by Solomon Northup, a New York State-born free African-American man who was kidnapped in Washington, D.C., in 1841 and sold into Slavery in the United States|slavery. Northup worked on plantations in the state of Louisiana for 12 years before his release. The first scholarly edition of Northups memoir, co-edited in 1968 by Sue Eakin and Joseph Logsdon, carefully retraced and validated the account and concluded it to be accurate.  Other characters in the film were also real people, including Edwin and Mary Epps, and Patsey.

This is the third feature film directed by  , Bocage Plantation|Bocage, Destrehan Plantation|Destrehan, and Magnolia Plantation (Schriever, Louisiana)|Magnolia. Of the four, Magnolia is nearest to the actual plantation where Northup was held.
 Best Supporting Best Adapted Best Film Best Actor award for Ejiofor. 

==Plot== slave pen owned by James Burch.
 William Ford. Northup impresses Ford when he engineers a waterway for transporting logs swiftly and cost-effectively across a swamp, and Ford presents him with a violin in gratitude. Northrup carves the names of his wife and children into the violin.

Fords carpenter John Tibeats resents Northup, and the tensions between them escalate.  Tibeats attacks Northup, but Northup overpowers him and beats him. In retaliation, Tibeats and his friends attempt to lynch Northup, but they are prevented by Fords overseer, Chapin, though Northup is left in the noose standing on tiptoe for many hours. Ford finally cuts Northup down, but chooses to sell him to planter Edwin Epps to protect him from Tibeats. Northup attempts to explain that he is actually a free man, but Ford states that he "cannot hear this" and responds "he has a debt to pay" on Northups purchase price.
 sadist who biblically sanctioned right to abuse his slaves. Epps beats his slaves if they fail to pick at least   of cotton every day.  Epps is attracted to Patsey, a young female slave who picks more than   daily, and repeatedly rapes her. Epps wife becomes jealous and frequently humiliates and degrades Patsey. Patseys only comfort is visiting Mistress Shaw, a former slave whose owner fell in love with her and elevated her to Mistress. Patsey begs Northup to kill her, but he refuses.

Some time later, an outbreak of cotton worm befalls Epps plantation. Unable to work his fields, he leases his slaves to a neighboring plantation for the season. While there, Northup gains the favor of the plantations owner, Judge Turner, who allows him to play the fiddle at a neighbors wedding anniversary celebration, and to keep his earnings. When Northup returns to Epps, he attempts to use the money to pay a white field hand and former overseer, Armsby, to mail a letter to Northups friends in New York state. Armsby agrees to deliver the letter, and accepts all Northups saved money, but betrays him to Epps. Northup is narrowly able to convince Epps that Armsby is lying and avoids punishment. Northup tearfully burns the letter, his only hope of freedom.

Northup begins working on the construction of a gazebo with a Canadian laborer named Bass. Bass is unsettled by the brutal way that Epps treats his slaves and expresses his opposition to slavery, earning Epps enmity. One day, Epps becomes enraged after discovering Patsey missing from the plantation. When she returns, she reveals she was gone to get a bar of soap from Mistress Shaw. Epps does not believe her and orders her flogged. Encouraged by his wife, Epps forces Northup to flog Patsey to avoid doing it himself. Northup reluctantly obeys, but Epps eventually takes the whip away from Northup, savagely lashing Patsey.

Northup purposely destroys his violin, and while continuing to work on the gazebo, Northup confides his kidnapping to Bass. Once again, Northup asks for help in getting a letter to Saratoga Springs. Bass, risking his life, agrees to send it.

One day, Northup is called over by the local sheriff, who arrives in a carriage with another man. The sheriff asks Northup a series of questions to confirm his answers match the facts of his life in New York. Northup recognizes the sheriffs companion as C. Parker, a shopkeeper he knew in Saratoga. Parker has come to free him, and the two embrace, though an enraged Epps furiously protests the circumstances and tries to prevent him from leaving.  Before Northup can board the coach to leave, Patsey cries out to him, and they embrace in a bittersweet farewell. Knowing that he is in potential danger, Northup leaves the plantation.

After being enslaved for 12 years, Northup is restored to freedom and returned to his family. As he walks into his home, he sees Anne, Alonzo, Margaret and her husband, who present him with his grandson and namesake, Solomon Northup Staunton. Concluding credits recount Northups unsuccessful suit against Brown, Hamilton and Burch, the 1853 publication of Northups slave narrative memoir, Twelve Years a Slave, and the mystery surrounding details of his death and burial.

==Cast==
  at the premiere of 12 Years a Slave]]
 
 
 
 
 
* Chiwetel Ejiofor as Solomon Northup
* Michael Fassbender as Edwin Epps William Ford
* Lupita Nyongo as Patsey
* Paul Dano as John Tibeats
* Paul Giamatti as Theophilus Freeman
* Scoot McNairy as Brown
* Adepero Oduye as Eliza
* Sarah Paulson as Mary Epps
* Brad Pitt as Samuel Bass
* Garret Dillahunt as Armsby Michael Kenneth Williams as Robert
* Alfre Woodard as Harriet Shaw
 
* Chris Chalk as Clemens
* Taran Killam as Hamilton
* Bill Camp as Radburn
 
  Dwight Henry, have small roles as Margaret Northup  and Uncle Abram,  respectively.

==Production==

===Development===
  at the 2013 San Diego Film Festival]] Steve McQueen got in touch with Ridley about his interest in making a film about "the slave era in America" with "a character that was not obvious in terms of their trade in slavery."  Developing the idea back and forth, the two did not strike a chord until McQueens partner, Bianca Stigter, found Solomon Northups 1853 memoir Twelve Years a Slave. McQueen later told an interviewer:
 Anne Franks diary but written 97 years before – a firsthand account of slavery. I basically made it my passion to make this book into a film.  

After a lengthy development process, during which Brad Pitts production company Plan B Entertainment backed the project, which eventually helped get some financing from various film studios, the film was officially announced in August 2011 with McQueen to direct and Chiwetel Ejiofor to star as Solomon Northup, a free negro who was kidnapped and sold into slavery in the Deep South.  McQueen compared Ejiofors conduct "of class and dignity" to that of Sidney Poitier and Harry Belafonte.    In October 2011, Michael Fassbender (who starred in McQueens previous films Hunger and Shame (2011 film)|Shame) joined the cast.  In early 2012, the rest of the roles were cast, and filming was scheduled to begin at the end of June 2012.  

To capture the language and dialects of the era and regions in which the film takes place, dialect coach Michael Buster was brought in to assist the cast in altering their speech. The language has a literary quality related to the style of writing of the day and the strong influence of the King James Bible.  Buster explained: Benedict  , I found some real upper-class New Orleanians from the 30s. And then I also worked with Lupita Nyongo, who is Kenyan but she did her training at Yale. So she really shifted her speech so she could do American speech.  

After both won Oscars at the 86th Academy Awards, it was reported that McQueen and Ridley had been in an ongoing feud over screenplay credit. McQueen reportedly had asked Ridley for shared credit, which he declined. McQueen appealed to Fox Searchlight, which sided with Ridley. Neither thanked the other during their respective acceptance speeches at the event.    Since the event, Ridley has noted his regret for not mentioning McQueen     and denied the feud.        He spoke favorably of working with McQueen, and explained that his sole screenplay credit was due to the rules of the Writers Guild of America.  McQueen has not commented on the alleged feud.    

===Filming===
  at the premiere of 12 Years a Slave at the 2013 Toronto Film Festival]]
With a production budget of  ,    principal photography began in  , Bocage Plantation|Bocage, Destrehan Plantation|Destrehan, and Magnolia Plantation (Schriever, Louisiana)|Magnolia.  Magnolia, a plantation in Schriever, Louisiana, is just a few miles from one of the historic sites where Northup was held. "To know that we were right there in the place where these things occurred was so powerful and emotional," said actor Chiwetel Ejiofor. "That feeling of dancing with ghosts – its palpable."    Filming also took place at the Columns Hotel and Madame Johns Legacy in the French Quarter of New Orleans. 

Cinematographer   aspect ratio using both an Arricam LT and ST. "Particularly for a period piece, film gives the audience a definite sense of period and quality," said Bobbitt. "And because of the storys epic nature, widescreen clearly made the most sense. Widescreen means a big film, an epic tale – in this case an epic tale of human endurance." 

The filmmakers avoided the desaturated visual style that is typical of a more gritty documentary aesthetic.  Deliberately drawing visual comparisons in the filming to the works of Spanish painter Francisco Goya, McQueen explained,
 When you think about Goya, who painted the most horrendous pictures of violence and torture and so forth, and theyre amazing, exquisite paintings, one of the reasons theyre such wonderful paintings is because what hes saying is, Look – look at this. So if you paint it badly or put it in the sort of wrong perspective, you draw more attention to whats wrong with the image rather than looking at the image.  

===Design===
To accurately depict the time period of the film, the filmmakers conducted extensive research that included studying artwork from the era.  With eight weeks to create the wardrobe, costume designer Patricia Norris collaborated with Western Costume to compile costumes that would illustrate the passage of time while also being historically accurate.  Using an earth tone color palette, Norris created nearly 1,000 costumes for the film. "She   took earth samples from all three of the plantations to match the clothes," McQueen said, "and she had the conversation with Sean   to deal with the character temperature on each plantation, there was a lot of that minute detail."  The filmmakers also used some pieces of clothing discovered on set that were worn by slaves. 

===Music===
  John and Alan Lomaxs arrangement of "Run, Nigger, Run".  A soundtrack album, Music from and Inspired by 12 Years a Slave, was released digitally on November 5 and received a physical format release on November 11, 2013 by Columbia Records.  In addition to Zimmers score, the album features music inspired by the film by artists such as John Legend, Laura Mvula, Alicia Keys, Chris Cornell, and Alabama Shakes.  Legends cover of "Roll, Jordan, Roll" debuted online three weeks prior to the soundtracks release. 

==Historical accuracy==
African-American history and culture scholar Henry Louis Gates Jr. was a consultant on the film, and researcher David Fiske, co-author of Solomon Northup: The Complete Story of the Author of Twelve Years a Slave, provided some material used to market the film.  Nevertheless, news and magazine articles around the time of the films release described a scholar alleging some license that Northup could have taken with his book, and liberties that McQueen definitely took with Northups original, for dramatic, modernizing, or other reasons.
 abolitionists invited an ex-slave to share his experience in slavery at an antislavery convention, and when they subsequently funded the appearance of that story in print, "they had certain clear expectations, well understood by themselves and well understood by the ex-slave, too."   

Noah Berlatsky wrote in the The Atlantic about a scene in McQueens movie version, shortly after Northup is kidnapped, when he is on a ship bound south, when a sailor who has entered the hold is about to rape a slave woman when a male slave intervenes. "The sailor unhesitatingly stabs and kills him," he wrote, and "this seems unlikely on its face—slaves are valuable, and the sailor is not the owner. And, sure enough, the scene is not in the book." 
 Christian abolitionists of the 19th century but not contradictory to Northup himself. Valerie Elverton Dixon in The Washington Post characterized the Christianity depicted in the movie as "broken". 

Emily West, an associate professor of history at the University of Reading who specializes in the history of slavery in the U.S., said she had "never seen a film represent slavery so accurately".  Reviewing the film for History Extra, the website of BBC History Magazine, she said: "The film starkly and powerfully unveiled the sights and sounds of enslavement – from slaves picking cotton as they sang in the fields, to the crack of the lash down people’s backs.  We also heard a lot about the ideology behind enslavement. Masters such as William Ford and Edwin Epps, although very different characters, both used an interpretation of Christianity to justify their ownership of slaves. They believed the Bible sanctioned slavery, and that it was their ‘Christian duty’ to preach the scriptures to their slaves."   

==Release==
 ]] premiered at the Telluride Film Festival on August 30, 2013,  before screening at the 2013 Toronto International Film Festival on September 6,  the New York Film Festival on October 8,  The New Orleans Film Festival on October 10, 2013,  and Philadelphia Film Festival on October 19, 2013. 
 New Regency Productions agreed to co-finance the film.  Because of a distribution pact between 20th Century Fox and New Regency, Fox Searchlight Pictures acquired the films United States distribution rights.  However, instead of paying for the distribution rights, Fox Searchlight made a deal in which it would share box-office proceeds with the financiers of the independently financed film.    12 Years a Slave was commercially released on  , 2013 in the United States for a limited release of 19 theaters, with a wide release in subsequent weeks.  The film was initially scheduled to be released in late December 2013, but "some exuberant test screenings" led to the decision to move up the release date.  The film was distributed by Entertainment One in the United Kingdom.

===Marketing=== Boxoffice Magazine. art house Black Swan awards seasons.   

During its marketing campaign, 12 Years a Slave received unpaid endorsements by celebrities such as   and Chris Rock responded to the movie by calling it the best film of 2013.   

==Reception==

===Box office===
As of May 20, 2014, 12 Years a Slave had earned $187.7&nbsp;million including $56.7&nbsp;million in the United States.  During its opening limited release in the United States, 12 Years a Slave debuted with a weekend total of $923,715 on 19 screens for a $48,617 per-screen average.  The following weekend, the film entered the top ten after expanding to 123 theatres and grossing an additional $2.1 million.  It continued to improve into its third weekend, grossing $4.6 million at 410 locations. The film release was expanded to over 1,100 locations on November 8, 2013.  

===Critical response===
  at the premiere of 12 Years a Slave]] Steve McQueens direction, John Ridleys screenplay, its production values, and its faithfulness to Solomon Northups account.
 average score of 9/10, with the sites consensus stating, "Its far from comfortable viewing, but 12 Years a Slave s unflinchingly brutal look at American slavery is also brilliant—and quite possibly essential—cinema."    Metacritic, another review aggregator, assigned the film a weighted average score of 97 (out of 100) based on 48 reviews from mainstream critics, considered to be "universal acclaim". It is currently one of the sites highest-rated films as well as the best reviewed film of 2013.  CinemaScore reported that audiences gave the film an "A" grade. 

Richard Corliss of   and Goodbye Uncle Tom|Goodbye, Uncle Tom. Except that McQueen is not a schlockmeister sensationalist but a remorseless artist." Corliss draws parallels with Nazi Germany, saying, "McQueen shows that racism, aside from its barbarous inhumanity, is insanely inefficient. It can be argued that Nazi Germany lost the war both because it diverted so much manpower to the killing of Jews and because it did not exploit the brilliance of Jewish scientists in building smarter weapons. So the slave owners dilute the energy of their slaves by whipping them for sadistic sport and, as Epps does, waking them at night to dance for his wifes cruel pleasure." 
 Dolby Theater next 86th Academy Awards|March." He also admired the films "gorgeous" cinematography and the musical score, as "one of Hans Zimmers more moving scores in some time." 

Paul MacInnes of The Guardian scored the film five out of five stars, writing, "Stark, visceral and unrelenting, 12 Years a Slave is not just a great film but a necessary one." 

The reviewers of Spill.com gave it high acclaim as well, with two reviewers giving it a "Better Than Sex," their highest rating. However, the reviewers agreed that it was not a film they would watch again anytime soon. When comparing it to the miniseries version of Roots (TV miniseries)|Roots, reviewer Cyrus stated that "Roots is The Care Bears Movie in comparison to this." 
  signing autographs at the premiere of the film at TIFF, September 2013]]

Owen Gleiberman of Entertainment Weekly praised it as "a new movie landmark of cruelty and transcendence" and as "a movie about a life that gets taken away, and thats why it lets us touch what life is." He also commented very positively about Ejiofors performance, while further stating, "12 Years a Slave lets us stare at the primal sin of America with open eyes, and at moments it is hard to watch, yet its a movie of such humanity and grace that at every moment, you feel youre seeing something essential. It is Chiwetel Ejiofors extraordinary performance that holds the movie together, and that allows us to watch it without blinking. He plays Solomon with a powerful inner strength, yet he never soft-pedals the silent nightmare that is Solomons daily existence." 

Peter Travers of Rolling Stone, gave the film a four-star rating and said: "you wont be able to tuck this powder keg in the corner of your mind and forget it. What we have here is a blistering, brilliant, straight-up classic." He later named the film the best movie of 2013. 

Manohla Dargis wrote, in her review for The New York Times, "the genius of 12 Years a Slave is its insistence on banal evil, and on terror, that seeped into souls, bound bodies and reaped an enduring, terrible price." 

The Daily Telegraphs Tim Robey granted the film a maximum score of five stars, stating that "its the nobility of this remarkable film that pierces the soul," while praising Ejiofor and Nyongos performances. 

Tina Hassannia of Slant Magazine said that "using his signature visual composition and deafening sound design, Steve McQueen portrays the harrowing realism of Northups experience and the complicated relationships between master and slave, master and master, slave and slave, and so on." 

David Simon, the creator of the TV series The Wire, highly praised the movie, commenting that "it marks the first time in history that our entertainment industry, albeit with international creative input, has managed to stare directly at slavery and maintain that gaze". 

The film was not without its criticisms.   criticized the story, saying, "12 Years a Slave is constructed as a story of a man trying to return to his family, offering every viewer a way into empathizing with its protagonist. Maybe we need a story framed on that individual scale in order to understand it. But it has a distorting effect all the same. Were more invested in one hero than in millions of victims; if were forced to imagine ourselves enslaved, we want to imagine ourselves as Northup, a special person who miraculously escaped the system that attempted to crush him." Describing this as "the hero problem", Malamud Smith concluded his review explaining, "We can handle 12 Years a Slave. But dont expect 60 Years a Slave any time soon. And 200 Years, Millions of Slaves? Forget about it."  At   and make them feel bad about themselves. Regardless of your race, these films are unlikely to teach you anything you dont already know." 

====Top ten lists====
12 Years a Slave has been named as one of the best films of 2013 by various ongoing critics, appearing on 100 critics top-ten lists in which 25 had the film in their number-one spot, both the most of any film released in its year. 

 
* 1st&nbsp;– Peter Travers, Rolling Stone
* 1st&nbsp;– Owen Gleiberman, Entertainment Weekly
* 1st&nbsp;– Tom Brook, BBC
* 1st&nbsp;– Ann Hornaday, Washington Post
* 1st&nbsp;– Eric Kohn, Indiewire
* 1st&nbsp;– Manohla Dargis, New York Times
* 1st&nbsp;– Bill Goodykoontz, Arizona Republic
* 1st&nbsp;– Scott MacDonald, The A.V. Club
* 1st&nbsp;– TV Guide
* 1st&nbsp;– Yahoo! Movies
* 1st&nbsp;– Jake Coyle, Associated Press
* 1st&nbsp;– Lisa Kennedy, Denver Post
* 1st&nbsp;– Brian D. Johnson, Macleans
* 1st&nbsp;– Mike Scott, Times-Picayune
* 1st&nbsp;– Randy Myers, San Jose Mercury News
* 1st&nbsp;– Katey Rich, Vanity Fair (magazine)|Variety
* 1st&nbsp;– Keith Phipps, The Dissolve
* 1st&nbsp;– Rafer Guzmán, Newsday
* 1st&nbsp;– Bob Fischbach, Omaha World-Herald
* 1st&nbsp;– Steve Persall, Tampa Bay Times
* 1st&nbsp;– Bruce R. Miller, Sioux City Journal
* 1st&nbsp;– Wesley Morris, Grantland
* 1st&nbsp;– Genevieve Valentine, Philadelphia Weekly
* 1st&nbsp;– David Chen, Slashfilm
* 2nd&nbsp;– A. O. Scott, New York Times
* 2nd&nbsp;– Lou Lumenick, New York Post
* 2nd&nbsp;– Peter Debruge, Variety (magazine)|Variety New York Daily News
* 2nd&nbsp;– Luke Wulfensmith, Freelance
* 2nd&nbsp;– Sasha Stone, Awards Daily NOW Magazine
* 2nd&nbsp;– Andrew OHehir, Salon (website)|Salon
* 2nd&nbsp;– James Rocchi, Cinephiled
* 2nd&nbsp;– Stephen Schaefer, Boston Herald
* 2nd&nbsp;– Ty Burr, Boston Globe
* 2nd&nbsp;– Michael Phillips, Chicago Tribune Christopher Orr, The Atlantic
* 2nd&nbsp;– Sam Adams, The A.V. Club
* 2nd&nbsp;– Barbara Vancheri, Pittsburgh Post-Gazette
* 2nd&nbsp;– David Ehrlich, Film.com
* 2nd&nbsp;– Film School Rejects
* 2nd&nbsp;– Connie Ogle, Miami Herald
* 2nd&nbsp;– Chris Vognar, Dallas Morning News The Wire
* 2nd&nbsp;- Andrew Saladino, NothingButFilm
* 3rd&nbsp;– Chris Nashawaty, Entertainment Weekly
* Top 10 (listed alphabetically, not ranked)&nbsp;– David Denby, The New Yorker
* Top 10 (listed alphabetically, not ranked)&nbsp;– Steven Rea, Philadelphia Inquirer
* Top 10 (listed alphabetically, not ranked)&nbsp;– Stephen Whitty, The Star-Ledger Joe Williams, St. Louis Post-Dispatch
* Top 10 (listed alphabetically, not ranked)&nbsp;– Calvin Wilson, St. Louis Post-Dispatch
* Top 10 (listed alphabetically, not ranked)&nbsp;– James Verniere, Boston Herald
* Top 10 (listed alphabetically, not ranked)&nbsp;– Kenneth Turan, Los Angeles Times
 

===Accolades===
  Best Actor award.   

==See also==
* List of films featuring slavery
* White savior narrative in film
* Solomon Northups Odyssey (1984), a PBS television film directed by Gordon Parks and starring Avery Brooks.
* Frederick Douglass and the White Negro, a documentary film telling the story of Frederick Douglass and his relationship with Ireland.

==References==
 

==External links==
 
*  
*  
*   at History vs. Hollywood
*  
*  
*  
*  

{{Navboxes
|list= 
 
 
 
 
 
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 