Gooby
 
 
 
{{Infobox film
| name           = Gooby
| image          = Gooby.jpg
| alt            =  
| caption        = 
| director       = Wilson Coneybeare
| producer       = {{Plainlist|
*John Calvert
*Wilson Coneybeare}}
| writer         = Wilson Coneybeare
| starring       = {{Plainlist|
*Matthew Knight*
*Robbie Coltrane
*David James Elliott
*Ingrid Kavelaars
*Eugene Levy}}
| music          = {{Plainlist|
*Kevin Lau
*Ronald Royer}}
| cinematography = Michael Storey
| editing        = Ellen Fine
| studio         = 
| distributor    = Christian Torres 
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = $6,500,000
| gross          = $3,234
}} Canadian film directed by Wilson Coneybeare featuring Robbie Coltrane as the voice of Gooby, a living teddy bear, and Matthew Knight as Willy, an 11 year old child who is scared of his new house.

The film was shot on a budget of $6.5 million. Review aggregation website Rotten Tomatoes rates it 20% positive ("rotten") based on five reviews. 

==Plot==
Willy is an 11-year-old boy with an overactive imagination, always believing weird creatures are lurking, just waiting to attack him. He believes that the only thing keeping him safe is his house, which is located on the best street in the world. When his parents decide to move, Willy isnt thrilled with the idea of going to a new school and living in a strange home. However, one day, Gooby, a teddy bear that Willy had as a little boy, arrives at the new house. This version of Gooby is nearly six feet tall, able to talk, and wants to help—Willy decides that the two will be friends, but that he must hide the bear from his parents.

Left alone for long hours while his friend goes to school, Gooby is frequently bored. He passes the time mostly by getting into trouble, convincing the nanny that there are critters inside the house, stealing cookies from the kitchen, ransacking the garage for parts to build Willys fantasy racer and sneaking around Willys school where anyone could see him. When Willys teacher goes on maternity leave, she is replaced by Mr. Nerdlinger, a childrens book writer who desperately wants to become famous. When he catches sight of Gooby, he becomes determined to photograph the creature. Willy has trouble making friends, and when Goobys appearance outside Mr. Nerdlingers class makes Willy freak out (earning him and three of his classmates detention) his reputation at school is only worsened. On Halloween day, Willy and Gooby go out during daylight to have fun (Gooby pretending to be Willys father in a costume) and Willy makes friends with the most popular kids in school by having Gooby buy them all tickets to an R-rated action movie. Gooby soon gets jealous when Willy starts hanging out more and more with his friends, until the two get into an argument and Gooby runs away.

Willys grades start to drop and his parents begin to worry. When Gooby eventually returns one night, he convinces Willy to follow him to a surprise location far away. Willys parents notice that their son is missing and call the police, but meanwhile Gooby shows Willy what he has discovered: his fathers childhood home. While exploring it, Willy gets scared to the point of falling backwards, breaking through the floor and catching a metal pipe too far down for Gooby to help him up. Gooby uses Willys cellphone to call his father, who comes to the rescue. After saving Willy, Gooby talks to Mr. Dandrige and reminds him of how when he was a child his father never spent enough time with him. Willys mother and father decide to stop working so hard and focus more on their son. One day, while shopping for materials to build Willy a playhouse, the family foils Mr. Nerdlingers attempt to get pictures of Gooby when Willy notices that Gooby has wandered off. Gooby explains to Willy that now that he is happy and no longer so alone (and afraid of imaginary creatures), its time for Gooby to move on and help another child. Gooby transforms back into a teddy bear and Willy offers him to a little girl who is being ignored by her parents.

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 