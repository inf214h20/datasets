Son of the Mask
 
{{Infobox film
| name = Son of the Mask
| image = sonofthemask.jpg
| caption = Theatrical film poster
| director = Lawrence Guterman
| producer = Erica Huggins Scott Kroopf
| writer = Lance Khazei
| starring = Jamie Kennedy Alan Cumming Traylor Howard Steven Wright Kal Penn Bob Hoskins
| music = Randy Edelman
| cinematography = Greg Gardiner Malcolm Campbell John Coniglio Debra Neil Fisher
| studio = Dark Horse Entertainment Gang of Seven Animation 
| distributor = New Line Cinema
| released =  
| runtime = 94 minutes
| country = United States
| language = English
| budget = $84 million   
| gross = $57.6 million 
}}
 fantasy Family The Mask, an adaptation of  Dark Horse Comics which starred Jim Carrey.
 Golden Raspberry Dirty Love.

==Plot== the first film, Dr. Arthur Neuman (Ben Stein) is giving a tour of the hall of Norse mythology in Edge City Museum. When Dr. Neuman reaches the part concerning Lokis mask, a man in black becomes increasingly anxious. Dr. Neuman mentions that Loki created the mask and unleashed it on Earth, and that those who wear the mask would have the powers of Loki. When Dr. Neuman mentions that Odin punished Loki with imprisonment, the stranger becomes very angry and transforms, revealing himself to be Loki (Alan Cumming). The tourists panic and flee, but Dr. Neuman stays to argue with the angry god. Loki takes the mask, but realizes it is a fake. In anger, he removes Dr. Neumans still talking face from his body and puts it on the mask stand, before getting rid of the guards and storming out of the museum in a whirlwind of rage.

In a town called Fringe City, Tim Avery (Jamie Kennedy), an aspiring cartoonist at an animation company, is feeling reluctant to become a father. He has a beautiful wife, Tonya (Traylor Howard), and a best friend, Jorge (Kal Penn), who is very shy around women. Tim has a close relationship with his dog Otis ("Bear"), who finds the real mask of Loki in a creek and brings it to his owners house. At the island, Loki is relaxing until Odin (Bob Hoskins) confronts him and orders his son to find the mask. Loki asks Odin to help him, but Odin tells Loki that this is his mess and he has to clean it up. Later that night, Tim puts on the mask for a Halloween party, transforming into a party animal similar to the mask character from the first film. Tim notices Jorges crush, Sylvia, standing alone in the back, eagerly confronts her and has her stripped out of her costume and into a skimpy red suit. Sylvia falls into Jorges arms, which pleases them both. When the company party turns out to be a bore, Tim uses his mask powers to perform a remix of "Cant Take My Eyes Off You", making the party a success, and giving Tims boss the idea for a new cartoon, resulting in his promotion the next day.

Tim returns to his house and, while still wearing the mask, conceives a baby. The baby, when he is born, has the same powers as Loki. Meanwhile, Loki is trying to find the child born from the mask, as his father Odin, possessing a store clerk, tells him if he finds the child, he will find the mask. Later, Tonya goes on a business trip, leaving Tim with the baby. Tim, who has been promoted at work, desperately tries to work on his cartoon at home, but is continuously disrupted by baby Alvey. In order to get some peace and quiet, Tim lets Alvey watch TV, which shows Michigan J. Frog. Alvey devilishly obtains the idea to mess with his fathers head by using his mask powers. Meanwhile, Otis the dog, who has been feeling neglected by Tim because of Alvey, dons the mask by accident and becomes a crazed animal version of himself, who wishes to get rid of the baby, but all his attempts are overturned by Alvey. Tim starts to notice his son and dogs wild cartoonish behavior when Alvey starts harassing him.

Eventually, Loki finds the mask-born baby, and confronts Tim for the mask back, but is thwarted again and again by Alvey who uses his powers to protect his father. Eventually, Odin becomes fed up with Lokis destructive approach and strips his son of his powers. A seemingly-deranged Tim is later fired after failing to impress his boss during a pitch, but is able to reconcile and bond with Alvey. Loki, still determined to please his father, manages to complete a summoning ritual and appeal to Odin to restore his powers. Odin agrees, but only for a limited time, stating this as "your final chance". Loki then kidnaps Alvey to exchange for the mask, but decides to keep him despite the exchange, forcing Tim to don the mask again to fight Loki. The subsequent confrontation is relatively evenly matched due to Loki and Tim-in-the-Mask possessing equal powers, prompting Loki to halt the fight, and suggest that they let Alvey decide who he wants to live with. Although Loki tries to lure Alvey to him with toys and promises of fun, Tim wins when he removes the mask and asks Alvey to come back to him using the human connection he has forged with his son. Saddened and enraged, Loki tries to kill Tim, but his time runs out and Odin appears in person. Odin disowns Loki, calling him a failure, and begins to banish Loki, but Tim confronts the powerful Norse god and tells him that the most important thing in life is a relationship with your family, and Odin accepts Loki as a son, accepting the mask from Tim as well. Tims cartoon, based on his own experiences of a boy and a dog competing for the fathers attention, is a hit, and Tonya reveals that she is pregnant again before the film closes.

==Cast==
* Jamie Kennedy as Tim Avery/The Mask
* Alan Cumming as Loki, God of Mischief
* Traylor Howard as Tonya Avery
* Kal Penn as Jorge
* Steven Wright as Daniel Moss
* Bob Hoskins as Odin, All-Father of the Gods.
* Ben Stein as Dr. Arthur Neuman
* Magda Szubanski as Betty
* Sandy Winton as Chris
* Rebecca Massey as Clare Ryan Johnson as Chad
* Victoria Thaine as Sylvia
* Duncan Young as Mansion Doorman
* Peter Flett as Mr. Kemperbee
* Amanda Smyth as Mrs. Babcock
* Ryan and Liam Falconer as Alvey Avery Otis
* Masked Otis voiced by Bill Farmer and Richard Steven Horvitz Mary Matilyn Mouser
* Alveys deep voice provided by Neil Ross

==Production==
 
Not long after the release of The Mask, it was announced in   convinced him that reprising a character hed previously played offered him no challenges as an actor. Due to Carrey declining to reprise his role, the project never came to fruition, and the concept for the sequel was completely changed. Since the film never came to fruition, in the final issue of Nintendo Power, an apology was issued to the winner of the contest.
 Fox Studios Sydney.

==Reception==
On  , the film has a score of 20 out of 100, based on 26 critics, indicating "generally unfavorable reviews". 
 At the Movies, Richard Roeper stated "In the five years Ive been co-hosting this show, this is the closest Ive ever come to walking out halfway through the film, and now that I look back on the experience, I wish I had." Roger Ebert gave the film 1.5 stars and stated "What we basically have here is a license for the filmmakers to do whatever they want to do with the special effects, while the plot, like Wile E. Coyote, keeps running into the wall.". He later named it the fifth worst film of 2005.  Lou Lumerick of the New York Post said "Parents who let their kids see this stinker should be brought up on abuse charges; so should the movie ratings board that let this suggestive mess slip by with a PG rating." When placing blame for the films critical failure, critic Willie Waffle of WaffleMovies.com asserted, "How far down the Hollywood food chain do you have to go before you get stuck with Jamie Kennedy as the star of your movie? Did Ben Affleck turn down Son of the Mask? Was Carrot Top busy? Did Pauly Shore refuse to return your calls?" 

It was the most nominated film at the 2005 Golden Raspberry Awards with eight, winning for Worst Remake or Sequel,    and won several 2005 Stinkers Bad Movie Awards, including Worst Actor (Jamie Kennedy), Worst Sequel, and Worst Couple (Kennedy and anyone forced to co-star with him).   The film earned back $57.6 million of its $84 million budget, making it a box office bomb. 
 my show and Malibus Most Wanted where I had a good amount of control. And then in this movie I didnt have any control. I just cant do that. I have to have my voice in there. If I cant, Im just going to be like Im doing someone elses thing. I have to have some of my voice because I have my own experiences that I lived through. All I can do is just try to make things independently. Thats the only way you can do it. The only way you can do that is if youre a huge, huge, huge star. Im not there yet. Im just like a working actor."  The largely negative reviews of Son of the Mask, some of which attacked Kennedy personally, inspired Kennedy to co-create the documentary film Heckler (film)|Heckler, an examination of both hecklers and professional critics.

==Video game==
A video game based on the film was released on Wireless Phone on February 10, 2005. The game was published and developed by Indiagames.

==Awards and nominations==
{| class="wikitable"
|-
!Award
!Category
!Nominee
!Result
|- Golden Raspberry Razzie Award  Golden Raspberry Worst Picture Erica Huggins and Scott Kroopf, producers
| 
|- Golden Raspberry Worst Director Lawrence Guterman
| 
|- Golden Raspberry Worst Screenplay Lance Khazei
| 
|- Golden Raspberry Worst Actor Jamie Kennedy
| 
|- Golden Raspberry Worst Screen Couple Jamie Kennedy and anybody stuck sharing the screen with him.
| 
|- Golden Raspberry Worst Supporting Actor Alan Cumming
| 
|- Bob Hoskins
| 
|- Golden Raspberry Worst Remake or Sequel
|
| 
|- Stinkers Bad Movie Awards  , Rotten Tomatoes  Worst Picture
|
| 
|- Most Painfully Unfunny Comedy
|
| 
|- Worst Sequel
|
| 
|- Foulest Family Film
|
| 
|- Worst Actor Jamie Kennedy
| 
|- Worst Screen Couple Jamie Kennedy and anyone forced to co-star with him.
| 
|- Most Annoying Fake Accent, Male Kal Penn
| 
|- Least "Special" Special Effects
|
| 
|- Most Intrusive Musical Score Randy Edelman
| 
|- Worst Song 
|"Cant Take My Eyes Off You" (Jamie Kennedy)
| 
|-
|}

==References==
 

==External links==
 
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 