The Providence Effect
 
{{Infobox film
| name           = The Providence Effect
| image          = Providence effect.jpg
| alt            = 
| caption        = Promotional film poster
| director       = Rollin Binzer
| producer       = Rollin Binzer Joey Dedio Julie Esch Hurvis Tom Hurvis Donald A. Johnson
| writer         = 
| starring       = 
| music          = Tom Dumont Ted Matson
| cinematography = Robert Tutman
| editing        = Richard La Porta
| studio         = Dinosaurs of the Future
| distributor    = Slowhand Cinema Releasing
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Providence Effect is a 2009 documentary film directed by Rollin Binzer about the transformation of Providence-St. Mel from a typical "inner city" school to a top tier institution.  It had its world premier at the Dawn Breakers International Film Festival in Zurich. 

==Background==
This documentary was made to promote discussion and interest in school reform.  Tom Hurvis has been a long-time sponsor of scholarships and internships for inner city youth.  The current strengths of Providence St. Mel, its curriculum, its success, and its discipline, are seen as anomalies when compared to typical inner city schools.  When Hurvis discovered Providence St. Mel, he was inspired to create the documentary. 

== Synopsis ==
The Providence Effect is a film about Paul Adams III and the inner city Chicago school he founded and brought to prominence.  Formerly a public school, Providence St. Mel has become a private school with rigorous educational standards and expectations.  The film documents how the school sets and encourages those goals for students.  

==Awards and nominations==
*2009, Official Selection, International Premier Dawn Breakers International Film Festival
*2009, Winner, "Best Documentary". Omaha Film Festival
*2009, Winner, "Most Inspirational Documentary", Seattle True Independent Film Festival
*2009, Winner, "Audience Choice Best Documentary", Lake County Film Festival

== References ==
 

==External links==
*   
*  

 
 
 
 
 
 
 
 
 