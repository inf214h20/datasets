New Police Story
 
 
{{Infobox film
| name           = New Police Story
| image          = NewPoliceStoryposter.jpg
| caption        = Hong Kong film poster
| film name = {{Film name| traditional    = 新警察故事
 | simplified     = 新警察故事
 | pinyin         = Xīn Jing Cha Gu Shi
 | jyutping       = San Ging Chat Gu Si}} Benny Chan
| producer       = Benny Chan Jackie Chan Willie Chan Barbie Tung Solon So
| writer         = Alan Yuen
| narrator       = 
| starring       = Jackie Chan Nicholas Tse Charlie Yeung Charlene Choi Daniel Wu Ken Lo Liu Kai-chi
| music          = Tommy Wai
| cinematography = Anthony Pun
| editing        = Yau Chi-wai
| distributor    = JCE Movies Limited Emperor Motion Pictures China Film Group Corporation
| released       =  
| runtime        = 124 minutes
| country        = Hong Kong Cantonese
| budget         =  HK $21,109,502
}} Crime Story.
 Hong Kong Benny Chan, and also produced by Jackie Chan, who also starred in the film. The film was released in the Hong Kong on 24 September 2004.
 reboot of the Police Story (film series)|Police Story series. New Police Story relies much more on drama and heavy action than its predecessors.

==Plot==
  Chan Kwok-wing ( ) drinking one whiskey after another. Later, he collapses in an alley, and is found by a stranger, played by Nicholas Tse.

The timeline then moves back a year, to show the heroics of Inspector Chan, as he disguises himself as a news cameraman in order to subdue a victim of man holding a victim of woman hostage in Central, Hong Kong|Central.
 Legislative Council building. They play a sadistic game, in which they are awarded money for shooting police officers with assault rifles, before making their escape.

Inspector Chan and his squad are called to arrest the gang after their hideout is revealed. However, the hideout is rigged and the ten man police squad fall into various traps one by one. Chan then finds his men in a large warehouse, suspended from the high ceiling by ropes. The gang challenges him to training regimes that are taught to policemen, betting lives of his men each time. Under mental pressure and the taunts of the gang, Chan loses and is left with only his would-be brother-in-law to save, who soon dies nevertheless. He tries to save the bodies of his comrades for burial before explosives blow up the building.

Being the sole survivor of the incident, Chan takes a year long leave from the police force, drinking heavily to drown his sorrows and guilt.

The timeline jumps back to the present day. The stranger, who first featured at the start, brings him home. When Chan wakes up, the man identifies himself as PC 138, Frank Cheng ( ), his new partner. Frank tries various means to convince Chan to cancel his leave and take up the case but Chan refuses. However, he comes to his senses eventually by apprehending the same two youths that robbed him. At the police station, Frank tells Chan that he is Kwongs younger brother, which convinces Chan to relook into the case.

Frank and Chan convinces Sam Wong, a former colleague of Chan, to reveal a clue from the night of the first robbery; a watch which he snatched from one of the robbers. Chan and Frank are tailed by the police as they go in search of the owner of the watch. Sam Wong was also arrested by the police to assist in the investigation. Sam Wong is shot dead by another gang member, Fire, before he manages to identify the robber (Sue). Before dying, Wong confesses to Chan that he was blackmailed by the gang into revealing their rigged hideout to Chans team a year ago, due to the bag of money he took from Sue to pay off his debts.

Fire and Sue both manage to escape the building, with Chan and Frank pursuing Fire. To distract Chan, Fire shoots at a bus driver and sends the vehicle out of control. Chan quickly jumps onto the roof of the bus in an attempt to stop it. After the careening bus has caused a great deal of havoc, Chan finally manages to climb in and hit the brakes, halting it from falling into Victoria Harbour, while Frank rams a truck full of rubber ducks in front of the bus path.

After the incident, Chan discovers that Frank is not really a policeman, and confronts him. Frank attempts to explain to Chan, but Chan refuses to believe the story. Regardless, they stay together to continue to track the gang leader, Joe. They are informed by Officer Sa Sa that the gang members all come from rich families and the gang leader is actually the son of the police chief. 

Sue and Fire return to the gangs new hideout. Sue is badly wounded, after being shot by Frank earlier. Seeing her injury, Joe shoots and kills her. Joes gang access Chans police file on a computer, with Joe bent on taking revenge.

Joe then arranges to meet Chans girlfriend, Ho Yee, in the police station. He wraps a time bomb around her neck before leaving. When Chan learns of the bomb he desperately tries to free her. Ho Yee cuts the wires, with no effect, leading the police to believe that it is a fake. But when the two get ready to leave, a small wire attached to Ho Yees back pulls out the secondary trigger, causing the bomb to explode. Before she can escape, it explodes and some falling pipes land on her, knocking her into a coma. Unfortunately for Chan, he is charged with assisting the impersonation of an officer (Frank) and for acting as an officer while under suspension.
 Convention and Exhibition Centre.

A squad of police officers arrive, but this time Chan stops them from rushing in, assigning them the task of calmly escorting the public out of the building. Once the public are moved to safety, the gang members parents are sent in, much to their dismay. One of the gang members, Max, is so ashamed of himself that he attempts to run down the escalators to his parents, only to be shot dead by Joe.

Afterwards, Chan and Frank run up to engage the three remaining gang members. Frank manages to shoot Fire in the leg with his own gun, disabling him. Chan chases another gang member, Tin Tin, into a Lego exhibition. When the exhibition hall is emptied, Chan reminds him that they had struck a draw the last time they fought. The two engage in hand-to-hand combat, with Chan defeating Tin Tin. Joe then chases Frank into the hall and shoots at him. In the crossfire, Tin Tin is shot in the chest.

Joe manages to chase Frank out of the room just moments before the Special Duties Unit of the police arrive. Tin Tin grabs a gun from the floor and points it at Chans back. Chan, oblivious to whats happening behind him, calls out to the squad to bring in a medic for Tin Tin. Contemplating what he has done and how Chan is helping him, Tin Tin decides not to shoot Chan.
 semi automatic pistol, a rematch of an earlier challenge, and wagering Franks life in the process. This time, Chan chambers the bullet first and wins the game, much to Joes disbelief. A number of policemen arrive on the roof, along with Joes father, the police chief. Joes father reprimands him, while Chan tells Joe that he knows that he doesnt hate cops, he merely hates his father for berating him.
 causing the sniper to fire once more, hitting him in the chest and presumably killing him despite Chans yelling that the gun was empty. Chan rushes to rescue Frank and both of them fall off the building. They eventually land onto a firemans inflated cushion.

In the hospital, Ho Yee has recovered and prepares to leave. She is met by several policemen and nurses who persuade her to accept Chans marriage proposal. At first, she tries to hide her facial scars from Chan, but eventually accepts the proposal to the crowds applause. Meanwhile, Frank is led away by Officer Sa Sa. He leaves his jacket on a railing. When Chan looks at the jacket, he suddenly recalls an event from years ago: a homeless man from the mainland arrives without much money, and his son is starving. He tries to steal some food for his son from a 7-Eleven store. While running across the road to escape from the shopkeeper and the police, he is hit by a truck and killed. Whilst another officer sneers at the dead man, Chan arrives at the scene and uses his jacket to cover the body of the boys father, telling the other officer that even a thief has dignity. Chan then buys the boy a loaf of bread. He comforts him, saying that the best thing do is to try to forget the past and look forward to the future. He gives the boy his jacket and asks his name to which the boy replies "Frank Cheng".

== Cast == Inspector Chan Kwok Wing
* Nicholas Tse – Frank Cheng Siu-fung
* Charlie Yeung – Sun Ho-yee
* Daniel Wu – Joe Kwan
* Lee Ting-fung – Joe Kwan - Age 6
* Charlene Choi – Sa Sa
* Dave Wong – Sam Wong Sum
* Hayama Go – Max Leung
* Terence Yin – Fire
* Yu Rongguang – Commander Chiu Chan
* Chun Sun – Joe Kwans father
* Lui Yau-wai – Joe Kwans mother (as Lisa Lui)
* Coco Chiang – Sue Chow
* Andy On – Law Tin-tin
* Liu Kai-chi – Chief Tai
* John Shum – Eric Chow
* Ken Lo – Kwong, Wings team member
* Asuka Higuchi – Kwongs wife Steven Cheung – Green-haired thief
* Kenny Kwan – Red-haired thief
* Wu Bai – Father of Frank Cheng
* Deep Ng – Rocky, Wings team member
* Tony Ho – Chui, Wings team member
* Timmy Hung – Tin Ming, Wings team member
* Sammy Hung – Tin Chiu, Wings team member
* Carl Ng – Carl, Wings team member
* Andrew Lin – Hoi, Wings team member
* Samuel Pang – Sam, Wings team member
* Philip Ng – Philip, Wings team member
* Winnie Leung – Female Hostage
* Eric Kwok – Male Hostage
* Mandy Chiang – Chuis girlfriend Mak Bau – Negotiator
* Ringo Chen – Tourist
* Park Huyn-jin – Disco Bouncer
* Ng Kong – Disco Bouncer
* He Jun – Disco Bouncer
* Anthony Carpio – Disco Bouncer
* Chan Tat-kwong – Disco Bouncer
* Stephen Rohn – X-Game Player
* Stephen Julien – X-Game Player
* Ho Wai-yip – Police Officer Outside Convenience Store
* Audiotraffic – Jazz Bar Band
* Victy Wong – Cop
* Zac Koo – HKCEC Police man
* Stephen Julien – X-Game Player
* Stephen Bohn – X-Game Player Mars (extra) (uncredited)
* Roderick Lam – Sams subordinate
* Jason Yip – Students father
* Nic Yan – Detention Suspect

===Jackie Chan Stunt Team=== Bradley James Allan
* Paul Andreovski
* Nicky Li
* Ken Lo Mars
* Wu Gang
* Park Hyun-jin
* He Jun
* Lee In-seob
* Han Kwan-hua

==Production==
Principal photography took place in Hong Kong between from 1 January until 31 December 2003.

== Box office == HKD in its first three days. It ended its run with $21 million HKD to making it the fourth highest-grossing domestic release of the year.

On 13 October 2006, the film received a limiting release in the United Kingdom. In its opening weekend the film grossed $19,332 having been shown in 16 theatres. It ranked #21 at the box office and averaged $1,208 per theatre.  As of 22 October 2006, New Police Story had grossed a total of $33,404 in its two week release in the UK.  British film critic Jonathan Ross gave the film a fairly positive review and felt that Chan could "still do the business". 

== International version == Lions Gate PRC by Chan.

A Blu-ray Disc|Blu-ray version was released in the United States on 24 November 2009.

== Awards == 24th Annual Hong Kong Film Awards
* Nomination - Best Picture Benny Chan)
* Nomination - Best Actor (Jackie Chan)
* Nomination - Best Supporting Actor (Daniel Wu)
* Nomination - Best Editing (Yau Chi-wai)
* Nomination - Best Action Design (Lee Chung-chi, Jackie Chan Stunt Team)
* Nomination - Best Sound Effects (Kinson Tsang Kin-cheung)
* Nomination - Best Visual Effects (Wong Won-tak, Ho Chi-fai)

41st Annual Golden Horse Awards
* Winner - Best Supporting Actor (Daniel Wu)
* Winner - Best Action Choreography (Lee Chung-chi, Jackie Chan Stunt Team)
* Winner - Best Visual Effects (Victor Wong, Brian Ho)
* Winner - Audience Choice Award
* Nomination - Best Editing (Yau Chi-wai)
* Nomination - Best Art Direction (Wong Ching-ching, Choo Sung-pong, Oliver Wong)
* Nomination - Best Sound Effects (Tsang King-cheung)

== See also == Crime Story The Protector
*  
* List of Hong Kong films
* Jackie Chan filmography

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 