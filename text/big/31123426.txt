Innocent Meeting
{{Infobox film
| name           = Innocent Meeting 
| image          = 
| image_size     =
| caption        =
| director       = Godfrey Grayson
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         = Brian Clemens
| narrator       =
| starring       = 
| music          = 
| cinematography = 
| editing        =
| studio         = 
| distributor    = 
| released       = 1958
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Innocent Meeting is a 1958 British crime film directed by Godfrey Grayson and starring Sean Lynch, Beth Rogan and Raymond Huntley.  In the film, a young tearaway bonds with the daughter of middle-class parents after meeting her in a record shop. The screenplay was by Brian Clemens.

==Cast==
* Sean Lynch - Johnny Brent
* Beth Rogan - Connie
* Raymond Huntley - Harold Ian Fleming - Garside
* Howard Lang - Macey
* Arnold Bell - Fry
* Colin Tapley - Stannard
* Robert Raglan - Martin
* Denis Shaw - Uncle

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 