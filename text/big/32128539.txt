Inner City (film)
{{Infobox film
| name           = Inner City
| image          = EtatDesLieux.jpg
| image_size     = 
| caption        = 
| director       = Jean-François Richet
| writer         = 
| narrator       = 
| starring       = Cyrille Autin
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = June 14, 1995
| runtime        = 80 mins.
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Inner City is a 1995 film directed by Jean-François Richet. It stars Cyrille Autin and Emmanuelle Bercot.  It won an award at the 1995 Avignon Film Festival. 

==Cast==
*Cyrille Autin as Samurai
*Emmanuelle Bercot as Gary Ainsworth
*Anne-Cécile Crapie as Deborah
*Andrée Damant as La Mere
*Marc de Jonge as Lagresseur 

==References==
 

==External links==
* 

 

 
 
 

 