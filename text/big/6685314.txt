The Endurance: Shackleton's Legendary Antarctic Expedition
{{Infobox Film |
  name           = The Endurance: Shackletons Legendary Antarctic Expedition |
 
  caption        = |
  writer         = Caroline Alexander, Joseph Dorman | Liam Neeson (narrator) | George Butler |
  producer       = George Butler | White Mountain Discovery Channel Pictures |
  distributor    = Cowboy Booking International |
  released       = September 2, 2000 |
  runtime        = 97 mins |
  language       = English |
  budget         = N/A |
  }} George Butler Ernest Shackletons Antarctic expedition in 1914. The Endurance was the name of the ship of Shackletons expedition.

==Plot==
 Frank Hurleys original film footage was used by Butler along with interviews of surviving relatives to present the story of Shackletons expedition.

==Reception==
The film was well received by critics,  and was nominated for and won several awards.  Butler followed it up the next year with another documentary about Shackletons expedition entitled Shackletons Antarctic Adventure.

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 
 
 