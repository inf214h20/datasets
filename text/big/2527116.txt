The Magician (1958 film)
{{Infobox film
| name           = The Magician
| image          = The-Magician-poster.jpg
| caption        = Film poster
| director       = Ingmar Bergman 
| producer       = Allan Ekelund 
| writer         = Ingmar Bergman 
| starring       = Ingrid Thulin Max von Sydow Naima Wifstrand Gunnar Björnstrand Bengt Ekerot Bibi Andersson Erland Josephson 
| music          = Erik Nordgren
| cinematography = Gunnar Fischer
| editing        = Oscar Rosander
| distributor    = AB Svensk Filmindustri 
| released       = 26 December 1958
| runtime        = 107 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross         = 
}}
 1958 film Swedish title is Ansiktet, which means "the face", and it was released theatrically as The Face in the United Kingdom, although video releases have used the U.S. title.

The film stars Max von Sydow as a traveling magician named Albert Vogler. Reading reports of a variety of supernatural disturbances at Voglers prior performances abroad, the leading townspeople request that Voglers troupe provide them a sample of their act, before allowing them public audiences. The scientifically minded disbelievers try to expose them as charlatans, but Vogler has a few tricks up his sleeve.
 Swedish at Best Foreign Language Film at the 31st Academy Awards, but was not accepted as a nominee. 

==Cast==
* Max von Sydow – Albert Emanuel Vogler
* Ingrid Thulin – Manda Vogler (alias Mr. Aman)
* Gunnar Björnstrand – Dr. Vergerus, Minister of Health
* Naima Wifstrand – Granny Vogler
* Bengt Ekerot – Johan Spegel
* Bibi Andersson – Sara
* Gertrud Fridh – Ottilia Egerman
* Lars Ekborg – Simson, the coach driver
* Toivo Pawlo – Police Superintendent Starbeck
* Erland Josephson – Consul Egerman
* Åke Fridell – Tubal
* Sif Ruud – Sofia Garp
* Oscar Ljung – Antonsson, burly stableman
* Ulla Sjöblom – Henrietta Starbeck
* Axel Düberg – Rustan, young manservant

==See also==
* List of submissions to the 31st Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 
 
{{succession box Special Jury Prize, Venice
| years=1959 The Lovers tied with La sfida
| after=Rocco and his Brothers}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 

 
 