Queen of the Mountains (film)
 
{{Infobox film
| name           = Queen of the Mountains
| image          = Queen of the Mountains.jpg
| caption        = Film poster
| director       = Sadyk Sher-Niyaz
| producer       = Zhyldyzkan Dzholdoshova Sadyk Sher-Niyaz
| writer         = Sadyk Sher-Niyaz
| starring       = Elina Abai Kyzy
| music          = Bakyt Alisherov Murzali Jenbaev 
| cinematography = Murat Aliyev
| editing        = Eldiyar Madakim 
| distributor    = 
| released       =  
| runtime        = 135 minutes
| country        = Kyrgyzstan
| language       = Kyrgyz
| budget         = $1.3M   

}}
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.   

==Cast==
* Elina Abai Kyzy as Kurmanjan Datka
* Nasira Mambetov as older Kurmanjan Datka
* Aziz Muradillayev as  Alymbek Datka
* Adilet Usubaliyev  as  Kurmanjan Son Kamchybek

==Critical reception== Montreal Gazette calling it "hauntingly poetic".    Queen of the Mountains was named as one of the best of the Montreal World Film Festival by Cult Montreal    and that it "Should be nominated for an Oscar".   
 Egyptian Theater. Laura Coleman, Kyrgystans Academy Award Contender Kurmanjan Datka Queen of the Mountains Comes To Laemmle, The Beverly Hills Courier, Volume XXXXVIIII, Number 45, November 14, 2014, p. 8  Stone hailed the film as "riveting" claiming its "great Academy potential"   

===Awards===
*2014: Award for Best Feature Film at the 2014 Eurasian Film Festival 

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Kyrgyz submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 