A Chinese Torture Chamber Story
 
 
{{Infobox film name = A Chinese Torture Chamber Story image =  alt =  caption =  traditional = 滿清十大酷刑 simplified = 满清十大酷刑}} director = Bosco Lam producer = Wong Jing writer = Cheuk Bing Chui Dat-choh starring = Yvonne Yung Lawrence Ng music = Lee Hon-kam Marco Wan cinematography = Tony Miu editing = Chan Gan-shing studio = Wong Jings Workshop Ltd. distributor = Golden Harvest released =   runtime = 92 minutes country = Hong Kong language = Cantonese budget =  gross = HK$10,404,725
}} Category III) comedy film produced by Wong Jing and directed by Bosco Lam. The films Chinese title literally means Ten Tortures of the Manchu Qing Dynasty.

==Plot==
Siu-bak-choi (lit. "Little Cabbage") is a servant of the physician Yeung Nai-mou. Yeung is open towards the topic of sex, which is usually considered taboo in traditional Chinese society. He invents a type of condom to help people avoid contracting sexually transmitted diseases, but his idea was not accepted and he was scorned at. Lau Hoi-sing, the lecherous son of a local judge, has an adulterous affair with Yeungs wife, but they are discovered by Little Cabbage. Yeungs wife tries to send Little Cabbage away by forcing the latter to marry Gok Siu-dai so as to prevent Little Cabbage from telling Yeung about her secret affair. Lau has been eyeing Little Cabbage for some time and he rapes her, but is discovered by Gok. Yeungs wife is afraid of being implicated so she instigates Lau to murder Gok and frame Yeung and Little Cabbage for the deed. The pair are put on trial and subjected to tortures to force them to "confess" to the crime.

==Cast==
* Yvonne Yung as Siu-bak-choi (Little Cabbage)
* Lawrence Ng as Yeung Nai-mou
* Tommy Wong as Gok Siu-dai
* Ching Mai as Jane
* Oh Yin-hei as Yeungs sister
* Kenny Wong as Lau Hoi-sing
* Elvis Tsui as Win Chung-lung
* Kingdom Yuen as Nanny
* Julie Lee as Ki Dan-fung
* Lo Hung as Judge Lau
* Lee Siu-kei as Judge of Supreme Court
* Liu Fan as Prison guard
* Dave Lam as Court assistant
* Leung Sai-on
* Leung Kei-hei
* Aman Chang

==External links==
*  
*  

 
 
 
 
 
 
 


 
 