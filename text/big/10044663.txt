The God Makers II
  The God Makers.

==Overview of the film==

===Introduction===
The introductory segment contains the following warning:
 
Due to the nature of the subject matter, this program is recommended for mature audiences. All the following information pertaining to Mormon theology can be verified using Mormon publications.
 
A photo of the Salt Lake Temple is shown twisting and distorting. A voiceover by Decker comments on his earlier films The Godmakers and Temple of the Godmakers, stating that the earlier films “caused mayhem” and suggests that his films caused the LDS Church to “modify several so-called unchangeable sacred doctrines” as a result.

Decker introduces himself with the statement that he was an active member of the LDS Church for 19 years.

===Financial power of the LDS Church===
The LDS Church is described as a huge business enterprise. John Heinerman, author of “Mormon Corporate Empire” describes various business ventures that the church is involved in. Heinerman states that 25% of the church’s holdings are in agribusiness. John L. Smith, Director of Utah Missions, Inc. states that the men at the top of the “Mormon empire” are extremely wealthy and hold a lot of the corporate power in the country.

===Polygamy===
  plural wife cessation of fundamentalist splinter groups who continue the practice today. A series of interviews follows with men and women from some of these fundamentalist groups as they relate their experiences with polygamy.

Thelma Greer, identified as a “Former Mormon” and author of Mormonism, Mama and Me, talks about her great-grandfather John D. Lee. 

A man identified only as “Art, Polygamist, Mormon Fundamentalist Prophet and Leader” is interviewed as he stands in front of the LDS Church Office Building in Salt Lake City. 

Lillian LeBaron Chynoweth relates her experiences living in a polygamous fundamentalist group. Chynoweth, identified in the film as “Lillian, Former Mormon Fundamentalist,” was the daughter of Ervil LeBaron. LeBaron was the leader and prophet of the Church of the Lamb of God, although the name of the church is not identified in the film and is instead referred to by Chynoweth as the “Mormon Church.” Chynoweth states that the group was sincere in practicing “all aspects of Mormonism” and describes her father as controlling every aspect of their lives through revelation.

James R. Spencer, identified as a “Former Mormon” and the author of Beyond Mormonism, states that any “horny” Mormon needs to be polygamous. 

===Blood atonement=== Mormon Doctrine, claiming that McConkie contradicts himself on a single page.

Chynoweth relates the account of the murder of her husband, her brother-in-law, and his eight-year-old daughter by her half-brothers. She states that their names were “on the list to be atoned for” because her father believed that they were “traitors to God’s cause.”  At the end of Chynoweth’s interview, she states that if anything happens to her that the “Mormon” church will be responsible. Immediately following this statement, a text overlay states that shortly after the interview, Lillian was found dead in her home of a gunshot wound.

===Changes to the Book of Mormon===
 
Decker refers to “4000 changes” in the Book of Mormon since it was first published. In addition, Decker states that LDS Church leaders have ”covered up thousands of historical and archeological errors,” while contrasting this with the statement that the Bible is “historically and archaeologically” accurate.  Interspersed with Decker’s comments are statements from David Breese, author of Know the Marks of a Cult.

===Spirit wives and “celestial sex”===
 
The film include segments of the animation video from God Makers, which depicts God the Father surrounded by numerous, blonde, identical “spirit wives” who are caring for “spirit children.”

Greer states that Mormon men are promised that they will have “unlimited eternal sex” and that the Mormon woman is promised a life of “eternal pregnancy.” Greer also states that if Mormon men do not marry in a Mormon temple that “they will be castrated” and “made eunuchs” as the result of “an operation” that will take place after they reach heaven. Decker later follows up on this theme by stating that the goal of every Mormon man is to “enjoy everlasting, celestial sex with thousands of goddess wives.”

===BYU Jerusalem Center=== BYU Jerusalem Center, Sackett states that he wants to warn the Jews about the “deception and misrepresentation that was employed in building this Mormon edifice” and claims that the true purpose of the structure is to proselytize the Jews. Sackett also makes a number of statements that he claims represent Mormon beliefs, including the following:

* a claim that Mormons believe that they are the only “true Jews” on earth today.

* a claim that Mormons believe that they all come from the tribe of Ephraim. 

* A statement that, upon Baptism (Mormonism)|baptism, Mormons believe that their blood actually changes to the blood of Israel.  The video accompanying this statement does not show a baptism, but instead shows two men in dressed in white, one of whom is making hand motions over the other.

===New Age practices===
The film shows what are said to be New Age related practices, which involved filming a woman wearing a pyramid on her head and a man who appears to be having a seizure. During this segment Heinerman states that “people of the new age movement are often more open to the truths of Mormonism.”

===Goddess wives and the temple===
  sacred temple underwear” places LDS members under bondage.

===The occult=== Mormon temple practices with Satanism. Regarding these allegations of satanic practices, Decker states that the “LDS Church has officially acknowledged that we were right,” apparently referring to the Pace memorandum. 
 Mormon ordinance of baptism for the dead, stating that this is “when the dead are called up to convert to Mormonism,” and that the dead will “seek out” those who enter the temple.

Decker claims that Joseph Smith, Jr. was a sorcerer and fortune teller and that “ t is therefore quite natural to surmise that Smith’s followers would be involved in the same practices that he advocated.” Decker also claims that Smith was convicted of sorcery and “crystal ball gazing or fortune telling” by courts in New York.
 Alvin and bring part of his body to the hill in order to obtain the golden plates.

Decker claims that Smith used “blood sacrifices in his magic rituals” in order to locate treasure. Decker quotes C. R. Stafford, while Stafford quotes earlier critics. 

Decker states that Smith was found to be in possession of a “magic talisman” at the time of his death that would bring him “wealth, power and success in seducing women.” 

===Allegations against church leaders===
 
A dying AIDS patient named Charles Van Damme is interviewed. Van Damme claims that he arranged women and drugs for church leaders (including Gordon B. Hinckley). These comments are interspersed with video of several people carrying protest signs near Temple Square in Salt Lake City. Commenting on the lack of news responding to these allegations, Decker states that the church executed “an extraordinary media blackout” and that they “stopped the hottest story of the ’80s.” Decker further states that the “Mormon church” has the “ability to control virtually all media programming with their minds.”

==Controversy==

===Responses to the film=== exorcise the Tanners’ demons, and expressed great sadness when they refused. 

The film provoked bomb threats against LDS meetinghouses and death threats against members. 

===BYU Jerusalem Center===
  
 
A segment in the film focuses on the BYU Jerusalem Center for Near Eastern studies located on Mount Scopus outside the old city of Jerusalem. The center was constructed in 1984, and teaches curriculum concerning Near Eastern history, Hebrew and Arabic language, and the Gospels in the New Testament. Part of the agreement which allowed its construction was that students are forbidden to proselytize. If a student breaks this agreement, he or she is sent home. The center was closed during the period between 2000 and 2006 due to security concerns as the result of the Second Intifada and reopened in 2007.

===Blood atonement===
 
Lillian Chynoweth’s description of the “blood atonement” administered by the followers of Ervil LeBaron is briefly described in Jon Krakauer’s book Under the Banner of Heaven. Although not explicitly named in the film The God Makers II, the list that Chynoweth referred to was called The Book of the New Covenants, and was written by Ervil LeBaron before his death in prison. The document contained a list of individuals that LeBaron believed deserved to die. Upon receipt of the list by several of his sons, they proceeded to administer this punishment. 

With regard to the statement of Lillian Chynoweth’s death after the completion of the film, The God Makers II does not make it clear that she committed suicide in 1992, and instead leaves the impression with the viewer that she was murdered as the result of the “blood atonement” threat. The Tanners take issue with the manner in which this is presented in the film, stating, " his statement certainly suggests to all those who see the video, that Lillian Chynoweth was murdered in cold blood. What the producers of The God Makers II fail to tell the viewer is that Lillian took her own life.   Also, the “Mormon Church” Chynoweth refers to on film is the “Church of the Lamb of God” but the film does not make this clear, so the viewer is left to infer she was speaking about the LDS Church. The Tanners, who do make the claim that the LDS Church practiced “blood atonement” in the 19th century, state, “ nfortunately, The God Makers II has presented the material concerning blood atonement in a way that has caused many people to believe that the Mormon (LDS) Church is still involved in the practice”. 

The LDS Church claims that this doctrine was never practiced in the 19th century church at all, and formally repudiated the allegations of this practice in 1889. 

===Allegations of sorcery and necromancy===
 
Joseph Smith was never “convicted” or even tried on charges of sorcery, crystal ball gazing or fortune telling. In 1826 a written complaint was filed against Smith as a “disorderly person.” This resulted in what is referred to as the “1826 trial” of Joseph Smith. The charge was “glass looking,” in reference to Smith’s use of a stone to assist in the search for treasure during the time that he worked for Josiah Stowell. Contradictory accounts of the trial exist, and the outcome is not specified. 

The allegation that Joseph Smith dug up and took with him a part of his brother Alvin’s body to the hill Cumorah is unconfirmed.   accepts Joseph Smith Sr.’s story on face value where he states that the family, “heard a rumor that Alvin’s body had been exhumed and dissected. Fearing it to be true, the elder Smith uncovered the grave on September 25, 1824 and inspected the corpse.” 
 Alvin with him to obtain the plates.  Alvin died on November 19, 1823, well before Smith’s second visit to the hill on September 22, 1824. Although Chase’s statement makes no further comment regarding Alvin, Hofmann’s forgery adds a claim that Smith said to the angel, “he is dead shall I bring what remains but the spirit is gone.” This statement reintroduced speculation regarding the exhumation of Alvin’s body for the purpose of satisfying the requirements for obtaining the plates. Jerald and Sandra Tanner point out that the only known source of such a requirement is the discredited Salamander Letter and suggest that Decker relied upon this letter as the source of his claim. 

Author  , speculates that the allegation is in fact true and that Joseph Smith Sr. stated there was a rumor, or even started it himself, merely to create a pretext for the exhumation. Vogel suggests that the family would not have had to dig up Alvin’s grave in order to see if it had been tampered with. Further, accepting Chase’s testimony in Mormonism Unvailed, Vogel states that the timing of the exhumation (September 25, close to the equinox during which Lucy Mack Smith states Joseph Jr. made his visits to Cumorah) further suggests it was part of an attempt to secure the golden plates. 

==Notes==
 

==References==
*{{Citation
 | last=Anderson
 | first=Richard Lloyd
 | authorlink=Richard Lloyd Anderson
 | title=The Alvin Smith Story: Fact and Fiction Ensign
 | date=Aug 1987
 | publisher=LDS Church
 | place=Salt Lake City, Utah
 | pages=58
 | url=http://www.lds.org/ensign/1987/08/the-alvin-smith-story-fact-and-fiction
 | accessdate=2007-02-20
 }}.
*{{Citation
 | last=Brodie
 | first=Fawn M
 | authorlink=Fawn Brodie
 | title=No Man Knows My History
 | publisher=Alfred A. Knopf
 | place=New York
 | year=1971
 | isbn=0-679-73054-0 
 }}.
*{{Citation
 | last=Hill
 | first=Marvin S.
 | authorlink=Marvin S. Hill
 | title=Joseph Smith and the 1826 Trial: New Evidence and New Difficulties
 | journal=BYU Studies
 | volume=12
 | year=1972
 | publisher=Brigham Young University
 | place=Provo, Utah
 | url=https://byustudies.byu.edu/shop/PDFSRC/12.2Hill.pdf
 | issue=2
 }}.
*{{Citation
 | last=Howe
 | first=Eber Dudley
 | author-link=Eber Dudley Howe
 | title=Mormonism Unvailed
 | place=Painesville, Ohio
 | publisher=Telegraph Press
 | year=1834
 | url = http://archive.org/stream/mormonismunvaile00howe#page/254/mode/2up
 }}. See also: Mormonism Unvailed
*{{Citation
 | last=Introvigne
 | first=Massimo
 | title=The Devil Makers: Contemporary Evangelical Fundamentalist Anti-Mormonism
 | journal= 
 |volume=27
 |issue=1
 | date=Spring 1994
 | url=http://content.lib.utah.edu/cdm4/document.php?CISOROOT=/dialogue&CISOPTR=15882&CISOSHOW=15746&REC=14
 }}.
*{{Citation
 | last=Jacobs
 | first=Marlin L
 | title=(Review of) The Mormon Corporate Empire
 | year=1985
 | publisher=SHIELDS
 | url=http://www.shields-research.org/Reviews/MCE_Rvw.htm
 | accessdate=2007-03-15
 }}.
*{{Citation
 | last=Krakauer
 | first=Jon
 | authorlink=Jon Krakauer
 | title=Under the Banner of Heaven: A Story of Violent Faith
 | year=2003 Doubleday
 | isbn=0-385-50951-0 
}}.
*{{Citation
 | surname1=Peterson
 | given1=Daniel C
 | authorlink=Daniel C. Peterson
 | surname2=Ricks
 | given2=Stephen
 | authorlink2=Stephen D. Ricks
 | date=Oct 1998
 | title=Offenders for a Word: How Anti-Mormons Play Word Games to Attack the Latter-day Saints
 | publisher=Foundation for Ancient Research and Mormon Studies (FARMS)
 | place=Provo, Utah
 | isbn=0-934893-35-7
 }}.
*{{Citation
 | surname1=Tanner
 | given1=Jerald and Sandra
 | authorlink=Jerald and Sandra Tanner
 | title= Problems in the Godmakers II
 | year=1993
 | publisher=Utah Lighthouse Ministry
 | place=Salt Lake City, Utah
 }}.

==External links==
 
*  on Google Video
* 
* , Jerald and Sandra Tanner, Salt Lake City Messenger, April 1993.
*  Ed Deckers response to criticism of The God Makers II.

 
 
 
 
 
 