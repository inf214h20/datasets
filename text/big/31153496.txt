Chittu Kuruvi
{{Infobox film
| name           = Chittu Kuruvi
| image          = ChittuKuruvifilm.png
| image size     =
| caption        = LP Vinyl Records Cover
| director       = Devaraj Mohan
| producer       = V. Kandhasamy Devaraj-Mohan Vaali (dialogues) Vaali
| story          = R. Selvaraj Sumithra Sreekanth Manorama
| music          = Ilayaraja
| cinematography = R. N. K. Prasad
| editing        = N. Vellaichamy
| distributor    = Sree Vishnupriya Creations
| studio         = Sree Vishnupriya Creations
| released       =  
| runtime        = 128 minutes
| country        = India Tamil
}}
 1978 Cinema Indian Tamil Tamil film, Manorama in lead roles. The film had musical score by Ilayaraja. 

==Cast==
 
* Sivakumar Sumithra
* Sreekanth Manorama
* Pushpalatha
* S. N. Lakshmi
* Meera
*Suruli Rajan
*Venniradai Moorthy
*Senthamarai
*A. R. Srinivasan
*Kovai Balu
*Sheshadri
*Ananthan
*Vairam Krishnamoorthy
*Rathina Kumar
*Veeramani
*Rangamani
*K. Vijay
 

==Soundtrack== 
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Vaali || 5.18 
|-  Susheela || Vaali || 4.16 
|-  Susheela || Vaali || 4.11 
|-  Susheela || Vaali || 3.55 
|-  Vaali || 04.24 
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 


 