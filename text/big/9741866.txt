Still Kicking (film)
{{Infobox Film image = StillKickingPoster.jpg caption = Original movie poster name = Still Kicking: Six Artistic Women of Project Arts & Longevity director = Greg Young producer = Golden Bear Casting starring = Amy Gorman Frances Kandl Frances Catlett Elsie Ogata Ann Davlin Grace Gildersleeve Madeline Mason Lily Hearst released = 19 July 2006 runtime = 35 minutes language = English
|}}

Still Kicking: Six Artistic Women of Project Arts & Longevity is a 2006 32-minute documentary film by Pacific Grove filmmaker Greg Young, featuring six six Bay Area women role models over 85 years old who remained artistically active. The catalyst for Youngs film was Amy Gorman and Frances Kandls Project Arts & Longevity through which they were exploring the link between longevity and artistic vitality.  Along with the film the joint project resulted in a book entitled Aging Artfully.  

Still Kickings first festival screening was at the 2006 Real to Reel Film Festival in North Carolina. The film was also selected for screening by the 2006 Independents Film Festival, the 2006 Port Townsend Film Festival, the 2007 Missouri International Film Festival, and the 2007 Santa Barbara International Film Festival.

In 2003 Young produced his first documentary called "Do You Know Yellowlegs is a Storytelling Museum?"  on the Oakland storyteller Orunamamu, known for her flamboyant attire which included yellow "psychedelic spandex leggings"  Gorman had interviewed Orunamamu as part of the Project and the film caught their attention.  When the three met, Young asked if he could document their process. 

 


== Citations ==
 

== References ==
* }} Forward by Robert A. Johnson
* }}
* }}
*  }}


== External links ==
* 
* 

 
 
 
 
 


 