Girl, Boy, Bakla, Tomboy
 
{{Infobox film
| name             = Girl, Boy, Bakla, Tomboy
| image            = Girl,_Boy,_Bakla,_Tomboy.jpg
| caption          = Theatrical movie poster
| director         = Wenn V. Deramas
| producer         = 
| writer           = Mel Mendoza-Del Rosario
| screenplay       =
| starring         = {{plainlist|
* Vice Ganda
* Maricel Soriano
* Joey Marquez
* Ruffa Gutierrez
* Cristine Reyes
* JC De Vera
* Ejay Falcon
}}
| studio           = {{plainlist|
* Star Cinema
* VIVA Films
}}
| distributor      = {{plainlist|
* Star Cinema
* VIVA Films
}}
| released         =  
| country          = Philippines
| language         = {{plainlist| Tagalog
* English
}}
| runtime          = 103 mins
| budget           = 
| gross            = PHP436,044,343 Million
}}
 comedy parody film produced by Star Cinema and VIVA Films starring Vice Ganda with Maricel Soriano, Joey Marquez, Ruffa Gutierrez, and Cristine Reyes. It is one of the official entries of the 2013 Metro Manila Film Festival.



The movie is supposedly the first solo movie of John Lapus but Deramas dropped him after he refused to drop weight. 

==Synopsis==
After long years of separation, quadruplets Girlie (Girl), Peter (Boy), Mark (Bakla), and Panying (Tomboy) meet again when Peter needed a compatible liver donor. Mark is the only match, and he sets a condition in exchange of his liver - for Girlie, Peter, and their father Pete (Joey Marquez) experience the hardships he, Panying, and their mother Pia (Maricel Soriano) have gone through. He then makes sure Girlie goes through hell on their behalf. In the process, Mark starts to give their family another chance and sees them on a different light. However, just when things are going well for them, Girlie schemes to get even with Mark. Worse, Pete’s current girlfriend Marie (Ruffa Gutierrez) plans to have him and Pia permanently separated. Is there a happy ending for this unconventional family?

==Cast and Characters==

===Main Cast===
*Vice Ganda as the quadruplets:
** Girlie Jackstone  (the Girl)
**Peter Jackstone (the Boy)
**Mark Jill Jackstone (the Bakla / Gay)
**Panying Jackstone (the Tomboy / Lesbian)
*Maricel Soriano as Pia Jackstone
*Joey Marquez as Pete Jackstone
*Ruffa Gutierrez as Marie
*Cristine Reyes as Liza
*JC De Vera as Osweng
*Ejay Falcon as Harry
*Kiray Celis as Snow White
*Xyriel Manabat as Cindy
*Rhed Bustamante as Bella
*JM Ibanez as Ariel
*Ryan Bang as Jun Pyo
*Angelu de Leon as Jack
*Bobby Andrews as Jill
*Joy Viado as  Lola Amparo
*Jasper Visaya as Michael Jackstone

===Special Participation===
*Karylle Tatlonghari as Peters Crush (cameo role)
*Luis Manzano as the priest (cameo role)

==Awards==
{| class="wikitable"
|-
! Award !! Category !! Recipient!!Result
|- Vice Ganda||  
|- 2013 Metro 39th Metro Maricel Soriano||  
|-
| Best Actor || Vice Ganda|| 
|-
| 2nd Best Picture || ||  
|-
| Gender Sensitivity || || 
|- 2014 GMMSF Box-Office Entertainment Awards  Film Actress Maricel Soriano|| 
|- Phenomenal Stars||align=center|Vice Ganda  (with Vic Sotto) || 
|}

== References ==
 
*http://www.rappler.com/entertainment/movies/47249-mmff-girl-boy-bakla-tomboy-review

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 