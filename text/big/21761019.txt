Clay (1965 film)
 
 
{{Infobox film
| name           = Clay
| image          = 
| caption        = 
| director       = Giorgio Mangiamele
| producer       = Giorgio Mangiamele
| writer         = Giorgio Mangiamele
| starring       = Janina Lebedew
| music          = 
| cinematography = Giorgio Mangiamele
| editing        = Giorgio Mangiamele
| distributor    = Giorgio Mangiamele 
| released       =  
| runtime        = 85 minutes
| country        = Australia
| language       = English
| budget         = ₤12,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p237 
}}
Clay is a 1965 Australian drama film directed by Giorgio Mangiamele. The film was nominated for the Golden Palm award at the 1965 Cannes Film Festival,    but it lost to The Knack ...and How to Get It.

==Plot==
Nick is a murderer on the run from the police. He finds a remote artists colony and takes shelter there. Whilst there, he falls in love with a sculptor named Margot. When Nick is betrayed to the police by a jealous rival, Chris, Margot kills herself.   

==Cast==
* Janina Lebedew as Margot George Dixon as Nick
* Chris Tsalikis as Chris Claude Thomas as Father Bobby Clark as Charles
* Sheila Florance as Deaf-mute
* Lola Russell as Mary
* Cole Turnley as Businessman

==Production==
The film was shot in 1964, with the crew consisting of Mangiamele, a camera assistant and a sound technician. The budget was raised by Mangiamele mortgaging his house and the cast contributing ₤200 each. Filming started in May and took six weeks, mostly at an artists colony in Montsalvat. Lead actor Janina Lebedew had her voice dubbed by Sheila Florence. 

==Release==
The movie was the first Australian film selected for competition at the Cannes Film Festival. 
 Melbourne Film Festival and struggled to get commercial release.  

==References==
 

==External links==
* 
*  at Australian Screen Online
*  at Oz Movies

 
 
 
 
 
 
 