Holiday Hotel
{{Infobox film
| name           = LHôtel de la plage
| image          = LHôtel de la plage.jpg
| image_size     = 
| caption        = 
| director       = Michel Lang
| producer       = Marc Goldstaub, Alain Poiré, Robert Sussfeld 
| writer         = Michel Lang
| narrator       = 
| starring       = Sophie Barjac  Myriam Boyer  Daniel Ceccaldi  Michèle Grellier  Bruno Guillain  Guy Marchand  Anne Parillaud  Michel Robin  Martine Sarcey
| music          = Mort Shuman
| cinematography = Daniel Gaudry
| editing        = Hélène Plemiannikov
| distributor    = 
| released       =January 11, 1978
| runtime        = 110 minutes
| country        = France French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French comedy film directed and written by Michel Lang. The film stars Sophie Barjac and Myriam Boyer on a summer holiday in Brittany.

==Cast==
*Sophie Barjac ...  Catherine Guedel 
*Myriam Boyer ...  Aline Dandrel 
*Daniel Ceccaldi ...  Euloge St. Prix 
*Michèle Grellier ...  Marie-Laure Delambre 
*Bruno Guillain   
*Francis Lemaire ...  Lucien Vermaelen  Robert Lombard ...  Guedel 
*Bruno Du Louvat ...  Antoine 
*Guy Marchand ...  Hubert Delambre 
*Jean-Paul Muel ...  Paul Dandrel 
*Anne Parillaud ...  Estelle 
*Michel Robin ...  Léonce 
*Martine Sarcey ...  Elisabeth Rouvier 
*Bernard Soufflet ...  Bertrand 
*Rosine Cadoret ...  Cécile St.Prix  Anna Gaël ...  Brenda 
*Blanche Ravalec ...  Yveline 
*Germaine Delbat ...  Madame Léonce 
*Madeleine Bouchez ...  Belle-Maman Dandrel / Grandma Dandrel 
*Marcelle-Jeane Bretonniere ...  Odette Vermaelen 
*Hélène Batteux ...  Mme Guedel 
*Valérie Boisgel ...  Mother of Rose-Annie 
*Marilyne Canto ...  Juliette Guedel 
*Marie Bunel ...  Claudie (as Marie-Laure Bunel) 
*Gérard Gustin ...  Jean-Pierre 
*Denis Lefebvre Duprey ...  Stéphane (as Denis Lefèbvre) 
*Philippe Ruggieri ...  Pierre-Alain 
*Dominique Michielini   
*Geoffrey Carey ...  Dave (as Geoffroy Carey) 
*Arch Taylor   
*Fenella Maguire ...  Langlaise du train 
*Roger Trapp ...  Monsieur télégramme 
*Jacques Bouanich ...  Le présentateur de chanson 
*Jean Texier ...  Monsieur Muscre 
*Katy Amaizo ...  Une chanteuse (as Cathy Amaizo) 
*Vicky Fury ...  Une chanteuse 
*Didier Savary ...  Le chanteur 
*Malène Sveinbjornsson ...  Charlotte 
*Lionel Melet ...  Jean-François 
*Thomas Sussfeld ...  Doudou 
*Pascal Beguet ...  Sébastien 
*Olivier Jurion ...  Le premier jumeau 
*Benoît Jurion ...  Le second jumeau 
*Martine Desroches ...  Caroline Dandrel

== External links ==
* 
* 

 
 
 
 
 
 
 


 
 