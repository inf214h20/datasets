The Chocolate Chase
 
 
The Chocolate Chase is a 1980 Warner Bros. cartoon featuring Daffy Duck and Speedy Gonzales.

Daffy Duck is given the job to guard the Chocolate Bunny factory from mice. The mice try to offer Daffy money but he still refuses. The mice then decide to call Speedy Gonzales, the fastest mouse in all of Mexico. Speedy succeeds in every way he tries to bring chocolate bunnies to the mice, hurting and winning against Daffy Duck. At the end, Daffy chases Speedy Gonzales into the Chocolate Bunny factory. Daffy is stuck inside a giant chocolate bunny and all the mice laugh at him. Daffy, though, is shown to be appreciative at the end.

This short is very similar to Speedy Gonzales, where Sylvester keeps away the mice from getting the cheese at the AJAX cheese factory and the mice use Speedy Gonzales to outsmart Sylvester. In Chili Weather is Sylvester keeping mice away from Guadalajara Food Processing and Speedy Gonzales outsmarts him. The Chocolate Chase also serves as the last true battle between Daffy Duck and Speedy Gonzales.
 
The Chocolate Chase is part of Daffy Ducks Easter Egg-Citement, Daffys award-winning Easter cartoon special.

 
 
 
 
 


 