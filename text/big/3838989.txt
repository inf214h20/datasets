Hawk the Slayer
 
{{Infobox film
| name           = Hawk the Slayer
| image          = Hawk-Slayer.jpg
| caption        = DVD cover of Hawk the Slayer
| director       = Terry Marcel  Harry Robertson
| writer         = Terry Marcel Harry Robertson
| narrator       =  John Terry Bernard Bresslaw Ray Charleson Peter OFarrell W. Morgan Sheppard
| music          = Harry Robertson
| cinematography = Paul Beeson
| editing        = Eric Boyd-Perkins
| distributor    = ITC Entertainment
| released       = 1980
| runtime        = 90 min.
| country        = United Kingdom
| language       = English
| budget         = 
}}
 John Terry and Jack Palance.   

==Plot==
The wicked Voltan kills his own father when the latter refuses to turn over the magic of the "last elven mindstone". Before the old man dies, he bequeaths a great sword with a pommel shaped like a human hand to his other son, Hawk.  The hand comes to life and grasps the mindstone.  The sword is now imbued with magical powers and can respond to Hawks mental commands. Hawk then vows to avenge his father by killing Voltan. 

Voltans evil touches the whole countryside.  Some time later, a man named Ranulf arrives at a remote convent. Ranulf tells the nuns that he survived Voltans attack on his people, which resulted in the brutal deaths of women and children. Ranulf was seriously injured in the attack.  The nuns nurse him back to health, but his hand cannot be saved. Voltan appears at the convent and kidnaps the Abbess, demanding a large sum of gold as a ransom. The High Abbot sends Ranulf to find someone who might help. 

Hawk discovers Ranulf with the help of a local sorceress, a woman whom he defended from an accusation of witchcraft. Ranulf has been captured by brigands, but Hawk rescues him. Ranulf convinces Hawk to rescue the Abbess. After a long and dangerous journey, Hawk locates his old friends: Gort, a dour giant who wields a mighty mallet; Crow, an elf of few words who wields a deadly bow; and Baldin, a wisecracking dwarf, skilled with a whip. The five men arrive at the convent, protecting the nuns and devising a way to lure Voltan into a trap. They use their combined skills to steal gold from a slave trader with which to pay the ransom. 

Hawk doubts that Voltan will free the Abbess after the ransom is paid.  He explains that Voltan treacherously murdered Hawks wife, Eliane. Hawk and his friends decide to rescue the Abbess, but they fail. Hawk kills Voltans son Drogo, who had previously assaulted the convent. Enraged, Voltan confronts the heroes in a final battle at the convent. A rogue nun helps Voltan capture the heroes; Voltan repays her by murdering her. With the help of the sorceress, the heroes escape, but the dwarf is mortally wounded. 

In the subsequent battle, Hawk exacts his revenge on Voltan and the Abbess is rescued. An evil entity decides that Voltan will be restored to life to carry out further evil. Heeding the sorceress advice, Hawk and Gort travel south to continue their battle against evil.

==Planned sequel==
A possible sequel was referred to in the US Magazine Cinefantastique (Fall 1980 Issue). The director is quoted as saying "...Ill be going on a trip looking for locations for the next one. Whether ITC does it or not, we will be making HAWK - THE DESTROYER in February  . The film was not made.

According to the Internet Movie Database a sequel titled "Hawk The Hunter" is categorized as "in development". 

==Cast== John Terry as Hawk
*Jack Palance as Voltan
*Bernard Bresslaw as Gort
*Ray Charleson as Crow
*Peter OFarrell as Baldin
*W. Morgan Sheppard (billed as Morgan Sheppard) as Ranulf
*Patricia Quinn as Woman (Sorceress)
*Cheryl Campbell as Sister Monica
*Annette Crosbie as Abbess
*Catriona MacColl as Eliane
*Shane Briant as Drogo
*Harry Andrews as High Abbot
*Christopher Benjamin as Fitzwalter
*Roy Kinnear as Innkeeper Patrick Magee as Priest
*Ferdy Mayne as Old Man (Hawk & Voltans Father)
*Graham Stark as Sparrow
*Warren Clarke as Scar

==Trivia== The Darkness. 

Several references to the film appear in the second series of the British sitcom Spaced. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 