Parijatha
{{Infobox film
| name           = Parijatha
| image          = Parijatha poster.jpg
| alt            =  
| caption        = Film poster
| director       = Prabhu Srinivas
| producer       = Paramesh
| writer         = 
| starring       = Diganth   Aindrita Ray
| music          = Mano Murthy
| cinematography = 
| editing        = Sai Suresh
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        =   Kannada
| budget         = 
| gross          = 
}} Kannada romantic film directed by Prabhu Srinivas. The film stars Diganth and Aindrita Ray in the lead roles.       
 Tamil Successful Arya and Nayantara.  The film was a musical hit with the soundtrack and score composed by Mano Murthy. The film also received appreciation for its neat screenplay.

==Cast==
*Diganth
*Aindrita Ray
*Mukhyamantri Chandru
* Ramji
* Sharan
* Padmaja Rao
* Rachitha Rachu
* Kadhal Dandapani

==Critical Reception==
Parijatha is remake of a tamil movie "Boss Engira Bhaskaran"
Parijatha opened to a good positive response from both masses and critics. IBNLive.com reviewed the film giving 3 stars out of 5 and said, "The comedy sequences, borrowed from some other Tamil films, strengthen the fun moment in the movie. The cast is so perfect that even an artist doing a small role impresses.Parijatha is an enjoyable comedy film, thanks to chemistry of the lead pair Diggy and Andy".  Rediff.com reviewed with 3.5 stars as "This is a film for those who are in love. Go for it and enjoy" 

==Soundtrack== Nokia Ovi Nokia Ovi Store. 
{{Infobox album  
| Name        = Parijatha
| Type        = Soundtrack
| Artist      = Mano Murthy
| Cover       = 
| Alt         = 
| Released    = 30 November 2012 
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Ananda Audio Video
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= A Chandrika
| lyrics1 	= Kaviraj
| extra1        = Kunal Ganjawala
| length1       = 
| title2        = Hudugee
| lyrics2 	= 
| extra2        = Rajesh Krishnan
| length2       = 
| title3        = Nee Mohisu
| extra3        = Shreya Ghoshal Kaviraj
| length3       = 
| title4        = Oh Parijatha
| extra4        = Sonu Nigam, Shreya Ghoshal Kaviraj
| length4       = 
| title5        = Ogoolo Nodtheeni
| extra5        = Shashikala
| lyrics5 	= 
| length5       = 
| title6        = Nee Mohisu (Unplugged)
| extra6        = Shreya Ghoshal Kaviraj
| length6       = 
}}

==Release==
Parijatha was released only 18 Movie theaters in Bangalore.

==References==
 

 
 
 
 
 
 