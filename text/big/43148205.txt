Somewhere Only We Know (film)
{{Infobox film
| name           = Somewhere Only We Know
| image          = Somewhere Only We Know poster.jpg
| alt            = 
| caption        = 
| director       = Xu Jinglei
| producer       = 
| writer         = Wang Shuo   Xu Jinglei   Shi Qing   Wang Yun   Zhao Meng   Liang Qinger   Situ Pengli
| starring       = Kris Wu Wang Likun Xu Jinglei Gordon Alexander   Cong Shan   Juck Zhang   Re Yizha
| music          = 
| cinematography = Mark Lee Ping Bin
| editing        = 
| studio         = Kaila Pictures Co Ltd
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = China
| language       = Mandarin   English
| budget         = 
| gross          = US$45.2 million   
}}
Somewhere Only We Know ( ) is a 2015 Chinese romantic drama film directed by Xu Jinglei. Filming took place in Prague, Czech Republic.   The film was released on February 10，2015.  

==Plot==
Jin Tian (Wang Likun) is a young woman who recently got dumped by her fiance and lost her grandmother, Chen Lanxin (Xu Jinglei). Feeling heartbroken, she travels to Prague, the place where her grandmother once spent years of her life. In her grandmothers belongings she finds a letter from 1970.

In Prague, Jin Tian meets Peng Zeyang (Kris Wu), a young single father who lives with his daughter and mother. The two develop a mutual attraction during their journey on searching Josef Novak (Gordon Alexander), her grandmothers past lover.

==Cast==
*Kris Wu as Peng Zeyang
*Wang Likun as Jin Tian
*Xu Jinglei as Chen Lanxin
*Gordon Alexander as Josef Novak
*Cong Shan as Zeyangs Mom
*Sophia Cai Shuya as Ni Ni, Zeyangs daughter
*Juck Zhang as Luo Ji
*Re Yizha as Shanshan

==Production==
Principal photography started on June 2014 in Prague and ended on August 2014.

==Box office==
In mainland China, the film grossed US$37.81 million in its first six days debuting at No. 1 at the box office.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 
 