The Phantom Stockman
{{Infobox film
| name           = The Phantom Stockman Lee Robinson George Heath Chips Rafferty Lee Robinson Victoria Shaw   Chips Rafferty   Max Osbiston   Guy Doleman
| music          =William Lovelock George Heath
| editing        = Gus Lowry
| studio         = Platypus Productions
| distributor    = Universal Pictures (Australia) Astor Corporation (US) Renown (UK)
| released       = June 1953 (Australia)
| budget         = ₤10,800   
| gross          = ₤23,000 (outside Australia) 
| runtime        = 67 minutes
| country        = Australia
| language       = English
}}  Lee Robinson Victoria Shaw, Max Osbiston and Guy Doleman. 

It was the first of several movies produced by Lee Robinson in association with Chips Rafferty in the 1950s.

==Synopsis==
Kim Marsden inherits a cattle station near Alice Springs after the death of her father. Kim becomes convinced her father was murdered. She sends for a legendary local bushman called the Sundowner, who was one of her fathers best friends.

Adopting the name Ted Simpson, the Sundowner arrives at Kims station with his aboriginal offsider, Dancer. They are given work by the station manager, McLeoud.

The Sundowner and Dancer discover that cattle rustlers have been stealing stock. The realise the person behind the murder is Kims neighbor, Stapleton, who is in league with the cattle rustlers and is romantically interested in Kim. 

The rustlers kidnap Sundowner but he uses telepathy to get Dancer to come to his rescue. Kim is united with her true love, McLeod. 

==Cast==
* Chips Rafferty as The Sundowner Janette Elphick as Kim Marsden
* Max Osbiston as McLeod
* Guy Doleman as Stapleton
* Henry Murdoch as Dancer
* Bob Darken as Roxey
* Joe Scully as the Moth
*George Neil
* Albert Namitjira as himself

==Production==
Chips Rafferty and Lee Robinson had both failed to raise finance for individual projects. Rafferty wanted to make a £120,000 13-part series and film, The Green Opal, about immigration problems.    Robinson wanted to make a thriller, Saturday to Monday which later became The Siege of Pinchgut. Both were stymied by a government rule at the time which prohibited invent in non-essential industry over £10,000. 

The two men knew each other because Robinson wrote scripts for Raffertys radio show, Chips: the Story of Outback. They decided to team up together and make a film that cost under £10,000, with Robinson directing and Rafferty starring. They were joined by cinematographer George Heath and formed Platypus Productions. Said Rafferty at the time:
 We nutted it out this way. Whats the good of imitating English and American pictures when we can get into places these foreign production units cant reach for sandflies and skeeters? Well pick locations and backgrounds the world knows nothing about. Well study them for dramatic values. But were not buying stories. The stories will just come out of our heads and still leave enough wood to make chairs.  
Robinson later elaborated:
 We said, “Let’s forget what the Australian public thinks about, what they might take to, because if you put an Australian tag on a film it was the worst possible thing you could do.” You see we were on a third-rate level as far as the public was concerned in comparison to imported films from anywhere. The thing was to try and go for different locales and different lines, new material but fairly standard in the international approach. I remember a motto that we used to remind ourselves of. It was something that Les Norman (the producer of Eureka Stockade) said to us. “If you are working in a known background like London or New York you can go for very different story lines, but if you are working in a new background that is unfamiliar to your audience you have to be a bit conventional in your story line because audiences find it difficult to accept a totally new background and a really new story line at the same time.” So I think there was a bit of that inherent in all of those early films with Chips. We always went for the unusual background and therefore didn’t try to get terribly tricky with the story lines.  
It was decided to make the film in the Northern Territory where Robinson had worked for a number of years.

The film was originally known as Dewarra, then The Tribesman. 

===Casting===
Charles Tingwell was meant to play a role but was unable to fit it in his schedule and was replaced by Guy Doleman.

Seventeen-year-old Jeanette Elphick, 1952 model of the year, was cast in the lead.  

===Shooting===
It was shot around Alice Springs in the Northern Territory of Australia starting July 1952.   Several days shooting were lost due to unexpected rain.  Interiors were shot in Sydney.

Robinson later recalled:
 My experience with actors was limited. Chips on the other hand had by now made quite a number of films and he was an impeccable technical actor.... There were people in the picture of course who had never made a picture before. There weren’t the opportunities here for them to do so. He helped them a good deal by walking through scenes with them on his own and getting things sorted out, timing their dialogue and so on. The other thing was that we were working in actual locations. We decided right from the beginning we would never, ever build sets. We were working to a large extent in situations that were fairly genuine. The Aboriginal involvement, the themes were genuine themes. I suppose, given my documentary background and the fact that you are on actual locations and in many cases using actual people, it was inevitable that that would come through.   accessed 30 March 2015  
The painter Albert Namatjira appeared as himself in the film. Lee Robinson had previously made a documentary about Namitjira called Namatjira the Painter.

==Release==

===Critical===
The Sun Herald wrote that:
 The film was made in a hurry, and looks like it; and the editing of many scenes is ludicrously slow. Hopalong Cassidy could probably clean up a dozen mysteries in the time it takes Chips Rafferty to draw wisely upon a cigarette. The romance is developed clumsily by script and direction. There were some satisfactory punches on the jaw, and a little gunplay later on, but generally there is not enough action to make the "dead heart" come to life.  

===Box Office===
Rafferty and Robinson managed to sell the Pakistan, India, Burma and Ceylon rights for £1,000. While filming The Desert Rats in Hollywood, Rafferty sold the American rights for $35,000, then the English rights for £7,500.  (The movie would later screen on US TV as Return of the Plainsman. Other 39 -- No Title
Chicago Daily Tribune (1923-1963)   21 July 1956: c6.  )

Robinson later claimed that the film recouped its costs within three months of being filmed. 

The film was distributed in Australia by Universal. The deal was done through Herb McIntyre who had supported a number of local films. 
===Foreign Release===
In the United States it was released as Return of the Plainsman whilst the working title was The Sundowner.  In Britain the movie was known as Cattle Station or The Tribesman. 

==Legacy==
Heath left the team and tried to get up his own film called The Jackeroo but was unsuccessful. 

Elphick later went to Hollywood and enjoyed a successful career under the name "Victoria Shaw".

==See also==
* Cinema of Australia

==References==
 

== External links ==
*  
*   at the National Film and Sound Archive
*  at Australian Screen Online
*  at Oz Movies
 

 
 
 
 
 
 
 