El ojo de vidrio (1969 film)
 
{{Infobox film
| name           = El ojo de vidrio
| image          = El Ojo de Vidrio movie poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = René Cardona Jr.
| producer       = Jacobo Derechín
| writer         = 
| screenplay     = Alfredo Varela, Jr.
| story          = Alfredo Varela, Jr. Antonio Aguilar
| based on       =  
| narrator       = 
| starring       = Antonio Aguilar Flor Silvestre Manuel Capetillo Eleazar García Alejandro Reyna Guillermo Rivas
| music          = Enrico C. Cabiati
| cinematography = Raúl Domínguez
| editing        = Federico Landeros
| studio         = Estudios América 
| distributor    = Cinematografica Águila
| released       =   
| runtime        = 117 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} Antonio Aguilar, Flor Silvestre, Manuel Capetillo, Eleazar García, Alejandro Reyna and Guillermo Rivas. With a backdrop of the Mexican Revolution, the film recounts the story of former horse wrangler and bandit Porfirio Alcalá y Buenavista who becomes the subject of a popularly known corrido along with his four cousins, after being notoriously heroic for raiding rich landlords and helping the poor. Being each notable for having one eye as the result of an injustice, the five heroes meet two townswomen and a theater actor who helps them disguise for their various raids. As their last raid attack, they take vengeance to the man who caused their tragedy, and evade revolutionary troops who call for peace after Porfirio Díaz resigns and is exiled.

El ojo de vidrio, shot on location in Tayahua, Zacatecas, was a box-office hit  in Mexican theaters and particularly distinctive for its imaginative use of mixing drama with comic relief within the main characters. The films story and screenplay were written by Antonio Aguilar and Alfredo Varela, who also portrays the theater actor. The film is also notable for being 117 minutes long, which is not the average runtime for normal low-budget Mexican films. It spawned the sequel Vuelve el ojo de vidrio released the next year.

==Cast==
*Antonio Aguilar as Porfirio Alcalá y Buenavista "El Ojo de Vidrio": Former horse wrangler of Palo Quemado who, after a confrontation, became one-eyed as a result of the brutality of his boss, Barbosa. He is in love with Coralillo and plans to marry her and retreat to a peaceful farm life.
* , and a native of San José de Gracia she is known for being headstrong and feisty. She is the cousin of Cocorito and later becomes the love interest of Porfirio. Her nickname references her dangerous personality to that of a milk snake. 
*Manuel Capetillo as Gumaro Buenavista: One-eyed colonel of Porfirios army and his cousin, he is the bad-mouthed, impatient one of the four brothers. Whenever he wants to curse, Porfirio or his brothers whistle at him in order to prevent him from cursing. 		
*Eleazar García as Chelelo Buenavista: One-eyed colonel of Porfirios army and his cousin, he is the funny, dim-witted one of the four brothers. 
*Alejandro Reyna as Plácido Buenavista: One-eyed colonel of Porfirios army and his cousin, he is the logical, and oldest one of the four brothers. 
*Guillermo Rivas as Jerónimo Buenavista: One-eyed colonel of Porfirios army and his cousin, he is the slow-thinking, and shy one of the four brothers. He is in love with Cocorito, and plans to marry her.
*  whom Porfirios father owes money, and then causes his death and leaves his son and nephews each without an eye. 
*Raúl Meraz as Capitán Mendiosabál: Federal army official whose regiment is located at San José de Gracia, and is infatuated with Coralillo and is friend to Don Ramíro and Señor de la Maza.
*  horse rancher who is also raided by Porfirio.
*Arturo Castro as Don Ramiro: Affluent cattle rancher of San Isidro, raided by Porfirio. 
*Yuyú Varela as Socorro González "La Cocorito": Hilarious cousin of Coralillo, in love with Jerónimo. 
*Alfredo Varela, Jr. as Señor Fregoli "Fregolini": Theatrical actor and makeup artist, he helps Porfirio with his and his cousins disguises and also accompanies them to their various journeys.
*Víctor Alcocer as Bernardo Iglesias del Toro: Last cattle-rancher raided by Porfirio, and also reports him and orders to arrest Coralillo and Cocorito.

==Home entertainment releases==
El ojo de vidrio was first released in   format, with bonus features such as scene selections and biographies.

==References==
 

==External links==
* 

 
 
 
 
 
 