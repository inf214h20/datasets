The National Health (film)
 
 
{{Infobox film
| name           = The National Health
| image          = File:Film_Poster_for_The_National_Health.jpg
| image_size     = 350px
| caption        = Theatrical release poster
| director       = Jack Gold
| producer       = Terry Glinwood Ned Sherrin 
| writer         = Peter Nichols
| narrator       = 
| starring       = Lynn Redgrave Colin Blakely Eleanor Bron Donald Sinden Jim Dale
| music          = Carl Davis
| cinematography = John Coquillon
| editing        = Ralph Sheldon
| studio         = Virgin Films
| distributor    = Columbia Pictures
| released       = 1973
| runtime        = 95 min
| country        = United Kingdom
| language       = English
}}
 1973 Cinema British comedy The National NHS hospital.  The film satirically interweaves the story of the real hospital with a fantasy hospital which exists in a soap-opera world where all the equipment is new and patients are miraculously cured - although the only "patients" seen are doctors or nurses who are themselves part of the soap opera plots.  In the real hospital, the patients die while the out-of-touch administrators focus on impressing foreign visitors.

==Cast==
* Lynn Redgrave - Nurse Sweet / Nurse Betty Martin
* Colin Blakely - Edward Loach
* Eleanor Bron - Sister McFee / Sister Mary MacArthur
* Donald Sinden - Mr. Carr / Senior Surgeon Boyd
* Jim Dale - Barnet / Dr. Neil Boyd
* Sheila Scott-Wilkenson - Nurse Powell / Cleo Norton
* Neville Aurelius - Leyland / Monk
* Gillian Barge - Dr. Bird
* George Browne - The Chaplain
* Patience Collier - The Lady Visitor
* Jumoke Debayo - Nurse Lake
* Robert Gillespie - Tyler
* John Hamill - Kenny
* Don Hawkins - Les
* James Hazeldine - Student Doctor
* Bob Hoskins - Foster
* David Hutcheson - Mackie
* Mervyn Johns - Rees
* Bert Palmer - Flegg
* Maureen Pryor - The Matron
* Richie Stewart - Mortuary Attendant
* Clive Swift - Ash
* Graham Weston - Michael

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 