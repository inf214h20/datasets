Tapologo
 

{{Infobox film
| name           = Tapologo
| image          = 
| caption        = 
| director       = Gabriela & Sally Gutiérrez Dewar
| producer       = Estación Central de Contenidos, S.L.
| writer         = 
| starring       = 
| distributor    = 
| released       = 2007
| runtime        = 98 minutes
| country        = South Africa Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Gabriela & Sally Gutiérrez Dewar
| cinematography = Pablo Rodríguez
| editing        = Ana Rubio, Pablo Zumárraga
| music          = Joel Assaizky
}}

Tapologo is a 2007 documentary film.

== Synopsis ==
In Freedom Park, a squatter settlement in South Africa, a group of HIV-infected former sex-workers, created a network called Tapologo. They learned to nurse their community, transforming degradation into solidarity and squalor into hope. Catholic bishop Kevin Dowling participates in Tapologo, and raises doubts on the official doctrine of the Catholic Church regarding AIDS and sexuality in the African context.

== References ==
 

 
 
 
 
 
 
 
 
 


 