Histoire de Pen
{{Infobox film
| name           = Histoire de Pen
| image_size     =
| image	         = Histoire de Pen FilmPoster.jpeg
| caption        = A poster with the films international title: Inside
| director       = Michel Jetté
| producer       =
| writer         = Michel Jetté Léo Lévesque
| narrator       =
| starring       = Emmanuel Auger Karyne Lemieux David Boutin
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = Canada French
| budget         =
| gross          =
}}
 classified 13+ in Québec and 18+ in the United Kingdom, and features Emmanuel Auger, Karyne Lemieux and David Boutin.

==Plot summary==
19-year-old Claude begins a 10-year sentence at a penitentiary. Rousseau, a member of Tarzans gang, attempts to rape him soon after his entry, but Claude is not a novice at fighting and dissuades him from further attempts. Tarzan puts out a contract to assassinate him.

The leader of a rival gang, Zizi Grenier, reveals the contract to Claude and offers his protection if Claude will fight for him. The two gangs hold public boxing events to avoid all-out war, with disputed territories put on the line. Claude accepts, but over the course of these duels learns of the power games playing out in the background.
 car theft, drug use, an attempted armed robbery gone wrong, and finally involuntary manslaughter during their escape. Lucia is later found dead in her cell.

Claude quits the fights, and befriends a man known as "the Phantom". After Claude is raped with the secret backing of the prison administration, the Phantom helps him escape by illicitly getting him into the prison workshop, and hiding him in a locker shipped out of the prison.

Claude rejoins Karine in her drug business, but on a delivery comes across one of the gang members who aided in his rape, arranges a meeting, and murders him. He is arrested and returned to the penitentiary, to learn of Jacques suicide. Soon after he is subdued when he learns that he has AIDS, and at the Phantoms request is transferred to a minimum-security prison. The movie ends with a letter from the Phantom to Karine informing her of his transfer.

==References==
*  
*   at Radio-Canada
 

== External links ==
*  

 
 
 
 
 