Marie and Bruce
{{Infobox film
| name           = Marie and Bruce
| image          = Marie and Bruce.jpg
| caption        = Film poster
| producer       = George VanBuskirk
| director       = Tom Cairns
| screenplay   = Wallace Shawn Tom Cairns
| starring       = Julianne Moore Matthew Broderick
| music          = Mark degli Antoni 
| cinematography = Patrick Cody
| editing        = Andy Keir
| distributor    = 
| released       =  
| country        = United States
| runtime        = 90 minutes
| language       = English
| budget         = 
| gross          = 
}}
Marie and Bruce is a 2004 drama film from Holedigger and New Films. It is directed by Tom Cairns and stars Julianne Moore and Matthew Broderick. It was based on the 1978 play of the same name by Wallace Shawn and premiered at the Sundance Film Festival on January 19, 2004. Although the film was well received and starred many major motion picture stars, it failed to receive distribution and remained obscure, until it was released on DVD in March 2009. The music was done by Mark degli Antoni of the band Soul Coughing.   

==Plot==
The film is based on the somewhat whimsical and somewhat comical relationship between a fairly unlikely couple involved in a complex and emotionally tortured cohabitation. The real scandal of the film begins when Marie decides to leave Bruce. Her lack of measured decisiveness in leaving, however, leads to the heaping on of verbal invective and a stream of sarcastic and pejorative language, assumed to have accumulated over the years. At the climactic dinner scene at a restaurant, Marie finally expresses her verbal contempt for Bruce but without much effect. The two return to their apartment and are seen falling asleep in their double bed as the film ends.

==Cast==
* Julianne Moore as Marie
* Matthew Broderick as Bruce
* Bob Balaban as Roger
* Julie Hagerty as Party Guest at Franks
* Campbell Scott as Tommy
* Griffin Dunne as Restaurant Guest

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 