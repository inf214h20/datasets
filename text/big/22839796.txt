The Single Girls
{{Infobox Film
| name           = The Single Girls
| image          = Thesinglegirlsposter.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Beverly Sebastion Ferd Sebastion
| producer       = Beverly Sebastion
| writer         = Ann Cawthorne William Kerwin
| narrator       = 
| starring       = Claudia Jennings Greg Mullavy Wayne C. Dvorak
| music          = Peter J. Elliott
| cinematography = Ferd Sebastion
| editing        =  Dimension Pictures
| released       = April 1974
| runtime        = 83 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 swingers at a Caribbean resort who are stalked by a mysterious killer. The film was directed by Beverly Sebastion and Ferd Sebastion, and stars Claudia Jennings, Jean Marie Ingels, and Greg Mullavy.

Tagline: Searching for a man was a way of life.

==Plot==
A group of men and women travel to a Caribbean resort to discover themselves sexually but unfortunately one of them has also discovered that they like to murder people too.

==Cast==
*Claudia Jennings - Allison
*Jean Marie Ingels - Phyllis
*Cheri Howell - Shannon (as Chéri Howell)
*Joan Prather - Lola
*Greg Mullavey - George
*Edward Blessington - Bud (as Ed Blessington)
*Victor Izay - Andrew
*Wayne C. Dvorak - Dr. Phillip Stevens (as Wayne Dvorek)
*Albert Popwell - Morris
*Jason Ledger - Blue
*Merci Montello - Cathy (as Mercy Rooney)
*Robyn Hilton - Denise

==External links==
*  
*List of American films of 1974

==Additional information==
This film was also released under the following titles:
*Bloody Friday	
*Private School

==Soundtrack==
*"Ms. America" - Written by Bobby Hart & Danny Janssen

 
 
 
 
 
 
 
 


 
 