Rent Boys
{{Infobox film
| name           = Rent Boys
| image          = Rent boys film poster.jpg
| alt            =
| caption        = Theatrical release poster
| film name      =  
| director       = Rosa von Praunheim
| producer       = Rosa von Praunheim
| writer         = Rosa von Praunheim
| starring       = Sergiu Grimalschi  Peter Kern Master  Patrick Wolfgang Werner
| music          = Andreas Wolter
| cinematography = Nicolai Zoern  Lorenz Haarmann  Jens Paetzold  Dennis Paul  Thomas Ladenburg
| editing        = Mike Shephard
| studio         =  Rosa von Praunheim Film produktion  RBB  NDR production   
| distributor    = 
| released       =  
| runtime        = 86 minute
| country        = Germany
| language       = German  Romanian 
| budget         =  €140,000
| gross          = 
}}
 German documentary film directed, written and produced by Rosa von Praunheim. The original German title translates as The guys from Bahnhof Zoo. 

The film focuses on male prostitution oriented to gay men in and around Bahnhof Zoo train station, a central transport facility in Berlin that has been a meeting place between gay men and male prostitutes for more than forty years.  

The film consists of several interviews with current and former hustlers, most of them poor immigrants from Eastern Europe, the men who have sex with them and the social workers that try to help them.    

==References==
 

==External links==
*  

 
 
 
 

 