Daughter of the Nile
{{Infobox film
| name           = Daughter of the Nile (Ni luo he nyu er) (I kori tou Neilou)
| image          =
| caption        =
| director       = Hou Hsiao-hsien
| producer       = Lu Wen-jen
| writer         = Chu Tien-wen
| narrator       = Tianlu Li Fu Sheng Fan Yang Lin Yang
| music          = Chang Hung-yi Cih-Yuan Chen
| cinematography = Chen Huai-en
| editing        = Ching-Song Liao
| studio         = Fu-Film
| distributor    =
| released       =  
| runtime        = 91 minutes
| country        = Taiwan Mandarin
| budget         =
| gross          =
}} Ni luo I kori 1987 Feature film by Taiwanese filmmaker Hou Hsiao-Hsien. {{cite news
|url=http://movies.nytimes.com/movie/119473/Daughter-of-the-Nile/credits
|title=Daughter of the Nile (1987) production credits
|publisher=New York Times
|accessdate=June 8, 2009}}  {{cite web
|url=http://www.filmreference.com/Directors-Ha-Ji/Hou-Hsiao-Hsien.html
|title=HOU Hsiao-Hsien
|publisher=Film Reference
|accessdate=June 7, 2009}}  {{cite journal
|last=Rowin
|first=Michael Joshua
|title=Daughter of the Nile: Lost City
|journal=Reverse Shot
|location=Hou Hsiao-hsien 
|issue=23
|url=http://www.reverseshot.com/article/daughter_nile
|accessdate=June 7, 2009}} 

==Background==
The films title is a reference to a character in a manga called Crest of the Royal Family who is hailed as Daughter of the Nile.   The film is a study of the life of young people in contemporary Taipei urban life, focusing on the marginalised figure of a woman and centred on a fast-food servers hapless crush on a gigolo. {{cite web
|url=http://www.bfi.org.uk/sightandsound/feature/49304
|title=Songs For Swinging Lovers, Hou Hsiao-hsien, Aesthetic strategies
|date=August 2006
|publisher=British Film Institute
|accessdate=June 6, 2009}}   The introductory sequence of the film suggests a parallel between the difficulties faced by people in the film (Taiwans urban youth, transitioning from a classical civilization into a changing world) and the mythic struggles of characters in the Egyptian Book of the Dead.  
 Lin Yang, Tianlu Li in the role of the grandfather.  Li became a central part of Hous major films, and Kao starred in several of them.

==Synopsis== Lin Yang), Fu Sheng Tsui) works out of town. Its up to Lin Hsiao-yang to take care of her pre-teen sister, who has already begun to steal, and a brother (Jack Kao) who is a burglar and gang member. {{cite news
|url=http://www.nytimes.com/1988/09/30/movies/rootless-in-americanized-taiwan.html?sec=&spon=
|title=Rootless in Americanized Taiwan
|last=Canby
|first=Vincent
|date=September 30, 1988
|publisher=New York Times
|accessdate=June 8, 2009}} 

==Cast {{cite news
|url=http://movies.nytimes.com/movie/119473/Daughter-of-the-Nile/cast
|title=Daughter of the Nile (1987) acting credits
|publisher=New York Times
|accessdate=June 8, 2009}} == Lin Yang as Lin Hsiao-yang 
*Jack Kao as Lin Hsiao-fang, the brother Fan Yang as Ah-sang  Tianlu Li as Grandfather  Fu Sheng Tsui as Father
*Xin Shufen as Xiao-fen, Shao Fangs fiance
*Yu An-shun as Hsiao-yangs Classmate
*Wu Nian-zhen as Tutor
;Additional cast
*Huang Chiung-yao
*Chen Chien-wen
*Yang Tzu-tei
*Lin Chu
*Chen Shu-fang
*Cai Cande
*Tian Weiwei
*You Jingru
*Lei Guowei
*Zue Zhizheng
*Zhu Youcheng

==Critical response==
In his in-depth analysis of Daughter of the Nile, Michael Joshua Rowin of Reverse Shot wrote that Daughter is one of Hous most accessible films, and that although the film never found theatrical distribution in the United States and never received a home video release, its foreshadowing of the themes Hou would later use in Millennium Mambo,  Hous first film to be distributed in the United States, make Daughter ripe for rediscovery, summarizing "Daughters themes and immediate imagery would be the future of Hou.". 

==Screenings and reception==
The film was originally released in October 1987 at the Turin International Film Festival of Young Cinema in Italy, where it won a Special Jury Prize in the International Feature Film Competition for Hou Hsiao-hsien. 
When it screened in January 1988 at the AFI Fest, the Washington Post wrote "Hou Hsiao-hsien has the slickness that gives Daughter of the Nile the most East-West crossover appeal. {{cite news
|url=http://pqasb.pqarchiver.com/washingtonpost/access/73566164.html?dids=73566164:73566164&FMT=ABS&FMTS=ABS:FT&type=current&date=Jan+31%2C+1988&author=Scarlet+Cheng&pub=The+Washington+Post+(pre-1997+Fulltext)&desc=Electric+Images+of+the+Other+China%3B+Festival+Showcases+the+New+Wave+of+Films+From+Taiwan&pqatl=google
|title=Electric Images of the Other China; Festival Showcases the New Wave of Films From Taiwan
|date=January 31, 1988
|work=Washington Post
|accessdate=June 8, 2008
| first=Scarlet
| last=Cheng}}   In September 1988 it screened at both the Toronto Film Festival and the New York Film Festival.   After the NYFF screening, Vincent Canby of the New York Times wrote the film "...is not about alienation as much as it is an example of it. It is an artifact from a revolution taking place elsewhere". {{cite news
|url=http://www.nytimes.com/1988/10/23/movies/film-view-why-some-movies-don-t-travel-well.html?sec=&spon=&scp=7&sq=DAUGHTER%20OF%20THE%20NILE&st=cse&pagewanted=2
|title=FILM VIEW; Why Some Movies Dont Travel Well
|last=Canby
|first=Vincent
|date=October 23, 1988
|publisher=New York Times
|pages=page 2
|accessdate=June 8, 2009}}   When it aired at the Chicago International Film Festival in October, 1988, Lloyd Sachs of the Chicago Sun-Times wrote "slow and grudgingly revealing, Taiwanese director Hou Hsiao-hsiens "Daughter of the Nile" does not lend itself to easy description". {{cite news
|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=CSTB&p_theme=cstb&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB36E0B4FC7A212&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM
|title=Daughter of the Nile is challenging, unsettling film
|last=Sachs
|first=Lloyd
|date=October 25, 1988 
|publisher=Chicago Sun-Times
|pages=38
|accessdate=June 8, 2009}}  
 New Yorks Anthology Film Archives.  {{cite news
|url=http://www.villagevoice.com/1999-10-12/film/time-regained/http
|title=Time Regained
|last=Hoberman
|first=J.
|date=October 12, 1999
|publisher=Village Voice
|accessdate=June 8, 2009}}  {{cite web
|url=http://newyorkfilmfestival.net/archive/wrt/programs/10-99/hou/hou.htm
|title=an unfolding horizon: the films of hou hsiao-hsien 
|date=October 1999
|publisher=New York Film Festival Taiwanese film retrospective at both the National Gallery of Art and the Freer Gallery. {{cite news
|url=http://pqasb.pqarchiver.com/washingtonpost/access/59713307.xml?dids=59713307:59713307&FMT=ABS&FMTS=ABS:FT&type=current&date=Sep+08%2C+2000&author=Michael+OSullivan%3B+Desson+Howe&pub=The+Washington+Post&desc=FILM+NOTES%3B+More+Films+at+Visions%3B+Taiwanese+Retrospective&pqatl=google
|title=FILM NOTES; More Films at Visions; Taiwanese Retrospective
|date=September 8, 2000
|work=Washington Post
|accessdate=June 8, 2009}} 

In April 2002 in screened at the Buenos Aires International Festival of Independent Cinema in Brazil, and in 2005 it screened at the Thessaloniki International Film Festival in Greece. 

In December 2006 it screened as part of a Hou Hsiao-hsien retrospective at the Canadian National Film Repository. {{cite news
|url=http://www2.canada.com/montrealgazette/news/preview/story.html?id=58589c03-4148-428f-9095-b33b4245ddd0&p=1
|title=The Calendar: A selection of events happening this week, Cinematheque quebecoise
|date=December 8, 2006 The Gazette
|accessdate=June 8, 2009}} 

==Awards== Turin International Festival of Young Cinema, and entered into the Directors Fortnight at Cannes Film Festival.

==Additional reading==
*Literary culture in Taiwan: martial law to market law by Sung-sheng Chang ISBN 0-231-13234-4 {{cite book
|last=Chang
|first=Sung-sheng
|title=Literary culture in Taiwan: martial law to market law
|publisher=Columbia University Press
|year=2004
|pages=page 176
|chapter=7, High Culture Aspirations and Transformations of Mainstream Fiction
|isbn=0-231-13234-4
|oclc=9780231132343
|url=http://books.google.com/?id=JGg3LoNNM_0C&pg=PA176&dq=%22Daughter+of+the+Nile%22,+Hsiao-hsien+Hou
|accessdate=June 7, 2009}} 
*Senses of Cinema, "Hou Hsiou-hsiens Urban Female Youth Trilogy", by Daniel Kasman {{cite web
|url=http://archive.sensesofcinema.com/contents/06/39/hou_urban_female_youth.html
|title=Hou Hsiou-hsiens Urban Female Youth Trilogy
|last=Kasman
|first=Daniel
|year=2006
|publisher=Senses of Cinema
|accessdate=June 7, 2009}} 
*New Chinese cinemas: forms, identities, politics, by Nick Browne ISBN 0-521-44877-8 {{cite book
|last=Browne
|first=Nick
|title=New Chinese cinemas: forms, identities, politics|publisher=Cambridge University Press
|year=1994
|pages=Pages 151–158
|chapter=6, The Ideology of Initiation: The Films of Hou Hsiou-hsien
|isbn=0-521-44877-8
|oclc=9780521448772
|url=http://books.google.com/?id=bZSuqR7XIiUC&pg=PA151&dq=%22Daughter+of+the+Nile%22,+Hsiao-hsien+Hou
|accessdate=June 7, 2009}} 
*Envisioning Taiwan: fiction, cinema, and the nation in the cultural imaginary, by June Chun Yip ISBN 0-8223-3367-8 {{cite book
|last=Yip
|first=June Chun
|publisher=Duke University Press
|year=2004
|edition=illustrated
|pages=pages 222–229
|chapter=4, Toward Post-Modernism: The "Global Teenager" and Hou Hsiou-hsiens Daughter of the Nile"
|isbn=0-8223-3367-8
|oclc=9780822333678
|url=http://books.google.com/?id=i6NV3Bb_YQwC&pg=PA222&dq=%22Daughter+of+the+Nile%22,+Hsiao-hsien+Hou
|accessdate=June 7, 2009
|title=Envisioning Taiwan : fiction, cinema, and the nation in the cultural imaginary}} 

==References==
 

 

 
 
 
 
 