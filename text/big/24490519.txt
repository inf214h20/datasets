Shagird
 
{{Infobox film
| name           = Shagird
| image          = Shagird.jpg
| caption        = DVD cover
| director       = Samir Ganguly
| producer       = Subodh Mukherjee
| writer         = Gulzar Subodh Mukherjee Nirmal Dey Vishwamitter Adil
| starring       = Joy Mukherjee Saira Banu
| music          = Laxmikant-Pyarelal
| lyrics         = Majrooh Sultanpuri 
| cinematography = N. V. Srinivas
| editing        = V. K. Naik
| distributor    = Subodh Mukherji Productions
| released       =  
| country        = India Hindi
}}

Shagird ( ;  ) is a 1967 Indian Bollywood comedy film directed by Samir Gunguly. The film stars Joy Mukherjee and Saira Banu in lead roles.

==Plot==
Professor Brij Mohan Agnihotri (I. S. Johar) is a confirmed bachelor, and he firmly refuses to get married. He has a student named Ramesh (Joy Mukherjee), who considers him his mentor. On a visit to his friend out of town, Brij meets his friends beautiful daughter, Punam (Saira Banu), and falls in love with her. Throwing all caution to the winds, he changes his appearance, and attempts to conquer Punam, but he later finds that Punam is attracted to his student, Ramesh. Now the professor has to decide whether to get rid of Ramesh, or to reverse their roles and make him his mentor.

==Cast== Joy Mukerji as Ramesh
*Saira Banu as Poonam
*I. S. Johar as Prof. Brij Mohan Agnihotri/Birju Nasir Hussain as Rameshs Father
*Achala Sachdev as Rameshs Mother
*Madan Puri as Madan Chicagowala Asit Sen as Narayan Hangal as Kedarnath Badri Narayan Shetty as Ruffian Mac as Rameshs Friend at an Engagement party
*Shivraj as Annoyed Husband
*Bhola as Waiter
*Urvashi Dutta as Shefali

==Awards==
*Nominated, Filmfare Best Actress Award - Saira Banu

==Music==
{{Infobox album
| Name = Shagird
| Type = soundtrack
| Artist = Laxmikant-Pyarelal
| Cover =
| Released =  
| Recorded = Feature film soundtrack
| Length =
| Label = Sa Re Ga Ma
| Producer = Laxmikant-Pyarelal
}}

The soundtrack of the film contains 6 songs. The music is composed by Laxmikant-Pyarelal, with lyrics authored by Majrooh Sultanpuri. 
*Song "Dil Wil Pyar Wyar" topped the Binaca Geetmala 1968 annual list.

{{Track listing
| all_lyrics      = Majrooh Sultanpuri
| all_music       = Laxmikant-Pyarelal
| extra_column = Singer(s)
| title1 = Bade Miya Diwane | extra1 = Mohammed Rafi, Joginder
| title2 = Dil Wil Pyar Wyar | extra2 = Lata Mangeshkar
| title3 = Kanha Kanha Aan Padi | extra3 = Lata Mangeshkar
| title4 = Woh Hain Zara Khafa Khafa | extra4 = Lata Mangeshkar, Mohammed Rafi
| title5 = Ruk Jaa Aye Hawa, Tham Jaa Aye Bahaar | extra5 = Lata Mangeshkar
| title6 = Duniya Paagal Hai, Ya Phir Main Deewana | extra6 = Mohammad Rafi
}}

== References ==
 

==External links==
*  
* 

 
 
 
 

 