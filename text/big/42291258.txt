Mundane History
{{Infobox film
| name           = Mundane History
| image          = 
| caption        =
| director       = Anocha Suwichakornpong
| writer         = Anocha Suwichakornpong
| producer       = Soros Sokhum Anocha Suwichakornpong
| starring       = Phakpoom Surapongsanuruk Arkaney Cherkam Paramej Noiam Anchana Ponpitakthepkijo
| music          = Furniture (Malaysian band) The Photo Sticker Machine (Thai band)
| cinematography = Ming Kai Leung
| editing        = Lee Chatametikool
| studio         = Electric Eel Films
| distributor    = Survivance
| released       =  
| runtime        = 82 minutes
| country        = Thailand
| language       = Thai
}}
Mundane History ( ), is a 2009 film by the female Thai film-maker Anocha Suwichakornpong. She wrote, co-produced and directed the film. It is described as “one of the most startling and original feature debuts of recent years",  and received its world premiere on 10 October 2009 at the Pusan International Film Festival in South Korea. It was the first Thai film to receive the countrys most restrictive viewing rating, due to a scene of full-frontal male nudity and masturbation.      

Mundane History won the Tiger Award at the 2010 International Film Festival Rotterdam.   

==Plot==
Mundane History centres on the friendship that develops between a young paralysed man from a wealthy Bangkok family and his male nurse from Isan in the North of Thailand. The film is also a commentary on Thailands Social class|class-based society and the frailty of life.    It premiered at the 2009 Pusan International Film Festival, where it was in the New Currents competition and also opened the World Film Festival of Bangkok.  It made its European Premiere in the Tiger Awards competition at the International Film Festival Rotterdam, and was among the three films in 15-title line-up that won the Tiger Award. 

==Main cast==
*Arkaney Cherkam ... Pun
*Paramej Noiam ... Father
*Anchana Ponpitakthepkijo ... Somjai
*Phakpoom Surapongsanuruk ... Ake

==Making of Mundane History==
Made for $150,000, Mundane History was financed by the filmmakers family and friends as well as grants and awards from the Hubert Bals Fund of the Rotterdam International Film Festival. 

==Rating== Thai film under Motion picture rating system#Thailand|Thailands motion-picture rating system to be given the most-restrictive 20+ rating.  

==See also==
* List of Thai films Nudity in film (East Asian cinema since 1929)

==References==
 

==External links==
* 

 
 
 
 
 
 
 