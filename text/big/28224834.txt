Eva Perón: The True Story
{{Infobox film
| name = Eva Perón
| image =  Eva Peron pelicula.jpg
| image size =
| alt = last speech
| director = Juan Carlos Desanzo
| producer = Hugo E. Lauría María de la Paz Marino
| writer = José Pablo Feinmann
| narrator =
| starring = Esther Goris Víctor Laplace
| music = José Luis Castiñeira de Dios
| cinematography = Juan Carlos Lenardi
| editing = Sergio Zottola
| studio =
| distributor = Líder Films David Lamping
| released = Argentina  24 October 1996 United States 18 December 1996
| runtime = 114 min
| country = Argentina
| language = Spanish
| budget =
| gross =
| preceded by =
| followed by =
}} Argentine drama film|drama-historical film based on the life of Eva Perón. It was directed by Juan Carlos Desanzo and starred Esther Goris and Víctor Laplace. It was released on October 24, 1996. It was awarded 3 "Cóndor" awards by the Argentine Film Critics Association in 1997.

==Plot==
The movie does not offer a linear interpretation of the full life of Eva Perón. Instead, it focuses on the political disputes during the last year of Evas life. These disputes involved womens suffrage in Argentina, failed coup attempts against the Peronist government, and Evas failed bid for the vice presidency. The movie concludes with Evas death in 1952.

==Cast==
* Esther Goris (Eva Perón)
* Víctor Laplace (Juan Domingo Perón)
* Cristina Banegas (Juana Ibarguren, Evitas mother)
* Pepe Novoa (Gral. Franklin Lucero)
* Irma Córdoba (Mercedes Ortiz de Achával Junco)
* Lorenzo Quinteros (Gral. Eduardo Lonardi)
* Tony Vilas (Gral. Edelmiro Farrell)
* Jorge Petraglia (Bishop)
* Enrique Liporace (Raúl Apold)
* Tony Lestingi (Alejandro Achával Junco)
* Leandro Regúnaga (José Espejo)
* Fernando Sureda (Armando Cabo)
* Danilo Devizia (Enrique Santos Discépolo)
* Carlos Roffé (Gral. Ángel Solari)
* Jean Pierre Reguerraz (Gral. Benjamín Menéndez)
* Miguel Tarditti (Américo Ghioldi)
* Francisco Nápoli (Arturo Frondizi)
* Horacio Roca (Paco Jamandreu)
* Regina Lamm (Guillermina Bunge de Moreno)
* Ariel Bonomi (Julio Álvarez)
* Luis Herrera (John William Cooke)
* Joselo Bella (Hernán Benítez)
* Eduardo Ruderman (Héctor Cámpora)
* Ramón Acuña (Presidential chauffeur)
* Fernando Álvarez (Senior worker)
* Mario Barreiro (Peronist worker) Socialist worker)
* Lindor Bressan (Casa Rosada military man)
* Raúl Florido (Casa Rosada military man)
* Inés Flomenbaum (Nurse)
* Pedro Mansilla (Radio drama actor)
* Ernesto Catalán (Radio drama sound engineer) Wake ceremony man)
* Florencia Sztajn (Evita as a child)
* Darío Contartesi (Juan Duarte as a child)
* Carla Damico (Blanca Duarte)
* Gabriela López (Elisa Duarte)
* Jorge García Marino (Minister)
* Luis Mazzeo (Military officer jeep)
* Jovita Dieguez (Woman Eva Perón Foundation|Foundation)
* Inés Rey (Old woman health institution)
* Martín Coria (Printing house worker)
* Nora Mercado (Woman radio)
* Pablo Carnaghi (Florencio Soto)
* Julio Pozueta (Isaías Santín)
* Enrique Flynn (Wake ceremony young man)
* Javier Faur
* Valeria Lorca
* Rosa Albina Ibáñez
* Adriana Pérez Gianny
* Roberto Fratantoni

==Reception== Madonna in the American film, Evita (1996 film)|Evita (1996), based on the musical. Alspectot described the Argentine film as "an effective character study with plenty of subtext." She also stated that "its fascinating to watch Goris and Victor Laplace (as Juan Peron) demystify sensationalized figures." She praised the greater emphasis on politics rather than Evas brief acting career and felt the title character engaged viewers so that "youre compelled to ponder her complex motivations throughout." 

===Awards and nominations===
Argentinean Film Critics Association Awards
*Best Actress (Mejor Actriz) – Esther Goris (WON)
*Best Art Direction (Mejor Dirección Artística) – Miguel Ángel Lumaldo (WON)
*Best Screenplay, Original (Mejor Guión Original – José Pablo Feinmann  (WON)
*Best Cinematography (Mejor Fotografía) – Juan Carlos Lenardi (nomination)
*Best Director (Mejor Director) – Juan Carlos Desanzo (nomination)
*Best Film (Mejor Película) – (nomination)
*Best Music (Mejor Música) – José Luis Castiñeira de Dios (nomination)

Biarritz International Festival of Latin American Cinema
*Audience Award – Juan Carlos Desanzo (nomination)

==References==
 

==External links==
*  
*   at Cine Nacional.com  

 

 
 
 
 
 
 
 
 
 