Aadmi (1968 film)
 
{{Infobox film
| name = Aadmi
| image = Aadmi1968.jpg
| image_size =
| caption =
| director = A Bhimsingh
| producer = P S Veerappa
| writer =Akhtar ul Iman, Kaushal Bharati, Ramarao, Shamanna
| narrator =

|story= Jawar N. Sitaraman
| starring = Dilip Kumar Manoj Kumar Waheeda Rehman
| music = Naushad
| cinematography =Faredoon A. Irani
| editing =A. Paul Doraisingam
| distributor =
| released = 1968
| runtime =
| country = India
| language = Hindi
| budget =
| preceded_by =
| followed_by =
}}
 1968 Bollywood|Hindi drama film produced by P. S Veerappa and directed by A Bhimsingh. The film stars Dilip Kumar, Waheeda Rehman, Manoj Kumar, Simi Garewal and Pran (actor)|Pran. The films music is by Naushad. The film is a remake of the Tamil film Aalayamani starring Sivaji Ganesan. Besides being noted Dilip Kumars acting as man on wheel chair, the film is also known for its dialogues by Akhtar ul Iman and trick work cinematography by Faredoon A. Irani.  It was a moderate success at the box office.

==Plot==
Rajesh (Dilip Kumar) is orphaned at a very young age and comes from a very wealthy and noble family, but is very insecure. His childhood sweetheart Meena tragically dies and Rajesh substitutes her with a doll. When he finds that another young boy has touched the doll, he starts a fight with him, and ends up murdering him. Years later, Rajesh, now a grown man, still has the doll in his closet and he has now fallen in love with a woman named Meena (Waheeda Rehman). He becomes engaged to her after the approval of his best friend, Dr. Shekhar (Manoj Kumar). However, Rajesh and Meena have an automobile accident, in which Rajesh becomes paralysed and uses a wheelchair. It is then that Rajesh finds out that Shekhar and Meena are having an affair. His old murderous and possessive hatred surfaces again and his best friend Shekhars life is in danger. 

 

==Cast==
* Dilip Kumar as Rajesh/Raja Saab
* Waheeda Rehman as Meena
* Manoj Kumar as Dr. Shekhar
* Simi Garewal as Aarti (as Simmi) Pran as Mayadas (Aartis father)
* Padma Chavan as Parvati (as Padma Chauhan) Agha as Prem
* Shivraj as Girdharilal
* Ulhas as Govind (as Ullhas)
* Mohan Choti as Hari (Rajeshs servant)
* Sulochana Latkar as Shekhars mother

==Songs==

# Aaj purani raahon se -   Mohammed Rafi
# Kal ke sapne aaj bhi -   Lata Mangeshkar 
# Kaisi Haseen Aaj    -     Talat Mahmood
# Kaari badariya -   Lata
# Mein tooti hui ek naiya -   Rafi
# Na aadmi ka koi bharsa -   Rafi
# Music theme  

==References==
 

==External links==
*  

 

 
 
 
 