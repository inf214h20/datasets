Is Everybody Happy? (1943 film)
:For the earlier Ted Lewis film, see Is Everybody Happy? (1929 film).
{{Infobox film
| name           = Is Everybody Happy?
| image          =
| image_size     =
| alt            =
| caption        = Charles Barton
| producer       = Irving Briskin
| writer         = Monte Brice
| narrator       = Ted Lewis Michael Duane Nan Wynn Larry Parks
| music          = John Leipold
| cinematography = L. William OConnell James Sweeney
| studio         =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 73 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Is Everybody Happy? (1943) is an American black and white musical film.

The taglines for the film were: "18 of the grand songs made famous by the High-Hatted Tragedian of Song", "A FAST-STEPPING MUSICAL JAMBOREE!", "GET HAPPY! - Here comes the sweetest show in town!" and "ITS GAY IN A GREAT BIG WAY!".

== Cast == Ted Lewis - Ted Lewis aka Tom Todd Michael Duane - Tom Todd
*Nan Wynn - Kitty ORiley
*Larry Parks - Jerry Stewart
*Lynn Merrick - Ann Bob Stanton - Artie (as Bob Haymes)
*Dick Winslow - Joe
*Harry Barris - Bob
*Robert Stanford - Frank Stewart, Jr.
*Fern Emmett - Mrs. Broadbelt, Landlady
*Eddie Kane - Salbin Ray Walker - Lou Merwin
*Anthony Marlowe - Carl Muller
*George Reed - Missouri

==Soundtrack==
*"Am I Blue?"
:Music by Harry Akst
:Lyrics by Grant Clarke
:Sung by Nan Wynn
*"Cuddle Up a Little Closer"
:Music by Karl Hoschna
:Lyrics by Otto Harbach
*"On the Sunny Side of the Street"
:Music by Jimmy McHugh
:Lyrics by Dorothy Fields
*"St. Louis Blues (song)|St. Louis Blues"
:Written by W. C. Handy It Had to Be You"
:Music by Isham Jones
:Lyrics by Gus Kahn
*"Chinatown, My Chinatown"
:Music by Jean Schwartz
:Lyrics by William Jerome
*"Way Down Yonder in New Orleans"
:Music by Turner Layton
:Lyrics by Henry Creamer By the Light of the Silvery Moon"
:Music by Gus Edwards
:Lyrics by Edward Madden When My Baby Smiles at Me"
:Music by Bill Munro
:Lyrics by Andrew Sterling and Ted Lewis
*"I Wonder Whos Kissing Her Now"
:Music by Joseph E. Howard and Harold Orlob
:Lyrics by William M. Hough and Frank R. Adams
*"Whispering (song)|Whispering"
:Music by John Schonberger
:Lyrics by Malvin Schonberger
*"Moonlight Bay"
:Music by Percy Wenrich
:Lyrics by Edward Madden
*"Put on Your Old Grey Bonnet"
:Music by Percy Wenrich
:Lyrics by Stanley Murphy
*"Its a Long, Long Way to Tipperary"
:Music by Jack Judge
:Lyrics by Harry Williams
*"The Passing Show of 1918|Smiles"
:Music by Lee S. Roberts
:Lyrics by J. Will Callahan Pretty Baby"
:Music by Tony Jackson and Egbert Van Alstyne
:Lyrics by Gus Kahn
*"Im Just Wild About Harry"
:Music by Eubie Blake
:Lyrics by Noble Sissle

==See also==
*Is Everybody Happy? (1929 film)

== References ==
 
 

== External links ==
*  
*  
*  
*  
*   at OV Guide
*   at msn. movies

 

 
 