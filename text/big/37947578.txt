Bread and Roses (1967 film)
{{Infobox film
| name           = Bread and Roses
| image          = 
| caption        = 
| director       = Heinz Thiel Horst E. Brandt
| producer       = Robert Kreis
| writer         = Gerhard Bengsch
| starring       = Günther Simon
| music          = 
| cinematography = Horst E. Brandt
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Bread and Roses ( ) is a 1967 East German drama film directed by Heinz Thiel and Horst E. Brandt. It was entered into the 5th Moscow International Film Festival.   

==Cast==
* Günther Simon as Georg Landau
* Harry Hindemith as Paul Kallam
* Eva-Maria Hagen as Jutta Lendau geb. Krell
* Carola Braunbock as Emmi Krell
* Helga Göring as Dr. Helene Seydlitz
* Johanna Clas as Eleanore Mergenthin
* Fred Delmare as 	Kurt Kalweit
* Jürgen Frohriep as Siegfried Schlentz Helmut Schreiber as Emil
* Horst Kube as Börner
* Günther Polensen as Erich
* Willi Schrade as Willi

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 