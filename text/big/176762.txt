Whale Rider
 
 
{{Infobox film
| name           = Whale Rider
| image          = Whale Rider movie poster.jpg
| image_size     = 215px
| alt            = 
| caption        = US release poster
| director       = Niki Caro
| producer       = John Bartnett Frank Hübner Tim Sanders
| screenplay     = Niki Caro
| based on       =  
| starring       = Keisha Castle-Hughes Rawiri Paratene Vicky Haughton Cliff Curtis
| music          = Lisa Gerrard
| cinematography = Leon Narbey
| editing        = David Coulson
| studio         = South Pacific Pictures ApolloMedia Pandora Films
| distributor    = Pandora Film   Newmarket Films  
| released       =  
| runtime        = 101 minutes   
| country        = New Zealand Germany
| language       = English Māori
| budget         = New Zealand dollar|NZ$$9,235,000  (approx. US $3.5 million)   
| gross          = $41.4 million 
}} Maori girl who wants to become the chief of the tribe. Her grandfather Koro believes that this is a role reserved for males only. The film was a coproduction between New Zealand and Germany. It was shot on location in Whangara, the setting of the novel. The world premiere was on 9 September 2002, at the Toronto International Film Festival. The film received critical acclaim upon its release. At age 13, Keisha Castle-Hughes became the youngest nominee for the Academy Award for Best Actress before she was surpassed by Quvenzhané Wallis, at age 9, for Beasts of the Southern Wild less than a decade later.

==Plot== waka is hauled into the sea for its maiden voyage.

==Cast==
* Keisha Castle-Hughes as Paikea Apirana 
* Rawiri Paratene as Koro Apirana
* Vicky Haughton as Nanny 
* Cliff Curtis as Porourangi
* Grant Roa as Uncle Rawiri

==Production==
 , where the film is set]] NZ On workshop and whale beaching waka seen at the end of the film was made in two halves in Auckland before being transported to Whangara. The waka was given to the Whangara community after filming concluded. 

==Reception==
===Critical response===
The film received critical acclaim and Castle-Hughess performance won rave reviews. Based on 144 reviews collected by   s Kenneth Turan praised Caro for her "willingness to let this story tell itself in its own time and the ability to create emotion that is intense without being cloying or dishonest."  Claudia Puig of USA Today gave the film three-and-a-half out of four stars and praised Castle-Hughes acting, saying "so effectively does she convey her pained confusion through subtle vocal cues, tentative stance and expressive dark eyes." 

===Awards===
The film won a number of international film-festival awards, including:
* the Toronto International Film Festivals AGF Peoples Choice award in September 2002
* the World Cinema Audience award at the January 2003 Sundance Film Festival in the United States
* the Canal Plus Award at the January 2003 Rotterdam Film Festival.

At the age of 13, Keisha Castle-Hughes was nominated for the Academy Award for Best Actress for her performance, becoming the youngest actress ever nominated for the award at that time. She held the record until 2012 when Quvenzhané Wallis (at the age of 9) was nominated for that category for the film Beasts of the Southern Wild.

Academy Awards:
* Best Actress (Keisha Castle-Hughes, lost to Charlize Theron for Monster (2003 film)|Monster) 
Chicago Film Critics Association:
* Best Actress (Keisha Castle-Hughes, lost to Charlize Theron for Monster (2003 film)|Monster)  American Splendor)
* Most Promising Performer (Keisha Castle-Hughes, winner)
Image Awards: Bringing Down the House)
* Best Film (lost to The Fighting Temptations)
Independent Spirit Awards:
* Best Foreign Film (winner) New Zealand Film Awards:
* Best Film
* Best Director (Niki Caro)
* Best Actress (Keisha Castle-Hughes)
* Best Supporting Actor (Cliff Curtis)
* Best Supporting Actress (Vicky Haughton)
* Best Juvenile Performer (Mana Taumanu)
* Best Screenplay (Niki Caro)
* Best Original Score (Lisa Gerrard)
* Best Costume Design (Kirsty Cameron)
Satellite Awards 
* Best Art Direction (lost to  ) In America) In America) Mystic River)
Screen Actors Guild: Cold Mountain)
Washington D.C. Area Film Critics Association:
* Best Actress (Keisha Castle-Hughes, lost to Naomi Watts for 21 Grams)

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 