Amour (2012 film)
 
 
 
{{Infobox film
| name           = Amour
| image          = Amour-poster-french.jpg
| caption        = French release poster
| director       = Michael Haneke Michael Katz
| writer     = Michael Haneke
| starring       = Jean-Louis Trintignant Emmanuelle Riva Isabelle Huppert
| cinematography = Darius Khondji
| editing        = Monika Willi Nadine Muse	
| studio         = Les Films du Losange X-Filme Creative Pool Wega Film France 3 Cinéma Canal+
| distributor    = Les Films du Losange   Artificial Eye   Sony Pictures Classics  
| released       =  
| runtime        = 127 minutes     
| country        = France Germany Austria
| language       = French
| budget         = United States dollar|$8.9 million 
| gross          = $26 million   
}} French for "Love") is a 2012 French-language drama film written and directed by the Austrian filmmaker Michael Haneke, starring Jean-Louis Trintignant, Emmanuelle Riva and Isabelle Huppert. The narrative focuses on an elderly couple, Anne and Georges, who are retired music teachers with a daughter who lives abroad. Anne suffers a stroke which paralyses her on the right side of her body.  The film is a Co-production (filmmaking)|co-production between the French, German, and Austrian companies Les Films du Losange, X-Filme Creative Pool, and Wega Film.
 Best Actress Best Original Best Director (Michael Haneke).    At the age of 85, Emmanuelle Riva is the oldest nominee for Best Actress in a Leading Role.      
 47th National Best Film, Best Director, Best Actor Best Actress.      

==Plot==
A brigade of firemen break down the door of an apartment in Paris to find the corpse of Anne (Emmanuelle Riva) lying on a bed, adorned with cut flowers.
 silently suffers a stroke. She sits in a catatonic state, not responding to Georges. She comes around as Georges is about to get help, but doesnt remember anything that took place. Georges thinks she was playing a prank on him. Anne is unable to pour herself a drink.

Anne undergoes surgery on a blocked carotid artery, but the surgery goes wrong, leaving her paralyzed on her right side and confined to a wheelchair. She makes Georges promise not to send her back to the hospital or into a nursing home. Georges becomes Annes dutiful, though slightly irritated, caretaker. One day, Anne tells Georges that she doesnt want to go on living.

The pupil whose performance they attended stops by and Anne gets dressed up and carries on a lively conversation during the visit, giving Georges hope that her condition was temporary. However, she soon suffers a second stroke that leaves her demented and incapable of coherent speech. Georges continues to look after Anne, despite the strain it puts on him.

Georges begins employing a nurse three days a week. Their daughter, Eva (Isabelle Huppert), wants her mother to go into care, but Georges says he will not break the promise he made to his wife. He employs a second nurse, but fires her after he discovers she is mistreating his wife.

One day, Georges sits next to Annes bedside and tells her a story of his childhood, which calms her. As he reaches the storys conclusion, he picks up a pillow and smothers her.

Georges returns home with bundles of flowers in his hands, which he proceeds to wash and cut. He picks out a dress from Annes wardrobe and writes a long letter. He tapes the bedroom door shut and catches a pigeon which has flown in from the window. In the letter, Georges explains that he has released the pigeon. Georges imagines that Anne is washing dishes in the kitchen and, speechless, he gazes at her as she cleans up and prepares to leave the house. Anne calls for Georges to bring a coat, and he complies, following her out the door.

The film concludes with a continuation of the opening scene, with Eva seated in the living room, after she has wandered around the now-empty home.

==Cast==
* Jean-Louis Trintignant as Georges Laurent
* Emmanuelle Riva as Anne Laurent
* Isabelle Huppert as Eva Laurent
* Alexandre Tharaud as Alexandre
* Rita Blanco as Concierge
* Dinara Droukarova as Nurse
* William Shimell as Geoff
* Ramón Agirre as Concierges husband
* Carole Franck as Nurse
* Laurent Capelluto as Police officer
* Jean-Michel Monroc as Police officer
* Suzanne Schmidt as Neighbor
* Walid Afkir as Paramedic
* Damien Jouillerot as Paramedic

==Production==
The film was produced for €7,290,000 through Frances Les Films du Losange, Germanys X-Filme Creative Pool and Austrias Wega Film.     It received co-production support from France 3 and €404,000 in support from the Île-de-France (region)|Île-de-France region.  Further funding was granted by the Medienboard Berlin-Brandenburg in Germany and National Center of Cinematography and the moving image in France.    Principal photography took place from 7 February to 1 April 2011. 

After 14 years, Jean-Louis Trintignant came back on screen for Haneke.  Haneke had sent Trintignant the script, which had been written specifically for him.    Trintignant said that he chooses which films he works in on the basis of the director, and said of Haneke that "he has the most complete mastery of the cinematic discipline, from technical aspects like sound and photography to the way he handles actors". 

The film is based on an identical situation that happened in Hanekes family.       The issue that interested him the most was: "How to manage the suffering of someone you love?" 

Haneke called the collaboration with Jean-Louis Trintignant and the subject of the film itself as a motivation to make the film. The starting point for Hanekes reflections was the suicide of his 90-year-old aunt, who had raised him. According to Haneke, she was suffering under heavy rheumatism and lived the last years alone in her apartment, because she did not want to be placed in a nursing home. She had even asked the director unsuccessfully for euthanasia. According to Haneke, the main theme of his script is not old age and death, but "the question of how to deal with the suffering of a loved one". 
 The Piano Teacher (2001) and Caché (film)|Caché (2005) specifically for actors (Isabelle Huppert and Daniel Auteuil). Haneke prefers this way of working, because in this way one "writes specifically something that fits to the advantages of each actor and helps to particularly work them out". 

==Release==
Artificial Eye acquired the distribution rights for the United Kingdom. 
 

==Critical reception==
The film was met with wide acclaim from film critics. Review aggregation website Rotten Tomatoes gives the film a score of 93% based on 195 reviews, with an average rating of 8.7/10,  while Metacritic gives a weighted average rating of 94 based on reviews from 41 critics, indicating "universal acclaim." 
 Time Out London also gave the film 5 out of 5 stars, stating "Amour is devastatingly original and unflinching in the way it examines the effect of love on death, and vice versa".  Calling Amour the best film of 2012, critic A. O. Scott of The New York Times said that "months after its debut at Cannes this film already feels permanent."  Writing in The Times, critic Manohla Dargis hailed the film as "a masterpiece about life, death and everything in between."  The newspaper flagged the film as a critics pick. The Wall Street Journals film critic Joe Morgenstern wrote of Amour: "Mr. Hanekes film, exquisitely photographed by Darius Khondji, has won all sorts of prizes all over the world, and no wonder; the performances alone set it off as a welcoming masterpiece." 

Among the few negative reviews, Calum Marsh of the Slant Magazine gave the film 2 out of 4 stars and indicated that the film "isnt the work of a newly moral or humanistic filmmaker, but another ruse by the same unscrupulous showman whose funny games have been beguiling us for years", adding that "Hanekes gaze, trained from an unbridgeable remove, carries no inflection of empathy; his style is too frigid, his investment too remote, for the world of these characters to open up before us, for their pain to ever feel like something more than functional." 

==Box office==
The film earned a total of $6,739,492 in the United States.  In total, it grossed $25,915,719 worldwide   against its $8.9 million budget. 

==Accolades==
 
 
{| class="wikitable"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of Accolades
|- style="text-align:center;"
! style="background:#ccc;" width="40%"| Award / Film Festival
! style="background:#ccc;" width="30%"| Category
! style="background:#ccc;" width="25%"| Recipient(s)
! style="background:#ccc;" width="15%"| Result
|- style="border-top:2px solid gray;"
|- 85th Academy Awards   Best Picture Michael Katz 
| 
|- Academy Award Best Actress in a Leading Role Emmanuelle Riva
| 
|- Academy Award Best Achievement in Directing
|rowspan="3"| Michael Haneke
| 
|- Academy Award Best Original Screenplay
| 
|- Best Foreign Language Film
|  
|- 2nd AACTA International Awards   Best International Actress
| Emmanuelle Riva
| 
|- Alliance of 7th Alliance of Women Film Journalists Award  Top 10 Films
|rowspan="2"|Amour
|  
|- Best Non-English-Language Film
|  
|- Best Actress
| Emmanuelle Riva
|  
|- Best Original Screenplay Michael Haneke
|  
|- Actress Defying Age and Ageism Emmanuelle Riva
|  
|- Bavarian Film 34th Bavarian Film Awards  Bavarian Film Best Director Michael Haneke
|  
|- Bodil Awards|66th Bodil Awards  Bodil Award Best Non-American Film
|rowspan="2"|Amour
|  
|- Boston Society 33rd Boston Society of Film Critics Award  Boston Society Best Foreign Film
|  
|- Boston Society Best Actress Emmanuelle Riva
|  
|- 66th British Academy Film Awards   Best Leading Actress
|  
|- Best Director Michael Haneke
|  
|- Best Original Screenplay
|  
|- Best Film Not in the English Language
|rowspan="3"|Amour
|  
|- British Film 2012 British Film Institute 
| Top 10 Films
|  
|- British Independent 15th British Independent Film Awards   Best International Independent Film
| 
|- 65th Cannes Film Festival 
| Palme dOr
| Michael Haneke
|  
|- 38th César Awards   Best Film
| Amour
|  
|- Best Director
| Michael Haneke
|  
|- Best Actor
| Jean-Louis Trintignant
|  
|- Best Actress
| Emmanuelle Riva
|  
|- Best Supporting Actress
| Isabelle Huppert
|  
|- Best Original Screenplay
| Michael Haneke
|  
|- Best Production Design
| Jean-Vincent Puzos
|  
|- Best Cinematography
| Darius Khondji
|  
|- Best Editing
| Monica Willi
|  
|- Best Sound
| Guillaume Sciama, Nadine Muse, Jean-Pierre Laforce
|  
|- Chicago Film 23rd Chicago Film Critics Awards  Best Actress Emmanuelle Riva
| 
|- Best Foreign-Language Film
|Amour
| 
|- 18th Critics Choice Awards  Best Actress Emmanuelle Riva
| 
|- Best Foreign Language Film
|Amour
| 
|- 19th Dallas-Fort Worth Film Critics Awards  Best Actress Emmanuelle Riva
| 
|- Best Foreign Language Film
|Amour
| 
|- Durban International 33rd Durban International Film Festival 
| Best Feature Film Award Michael Haneke
|  
|- 25th European Film Awards  European Film European Film
|Amour
|  
|- European Film European Director Michael Haneke
|  
|- European Film European Actor
|Jean-Louis Trintignant
|  
|- European Film European Actress Emmanuelle Riva
|  
|- European Film European Screenwriter Michael Haneke
|  
|- European Film Carlo di Palma European Cinematographer Award Darius Khondji
|  
|- International Federation 65th FIPRESCI Awards   Grand Prix
|Amour
| 
|- 2nd Georgia Film Critics Awards  Top 10 Films
|rowspan="3"|Amour
| 
|- Best Foreign Film
| 
|- Best Picture
| 
|- Best Director Michael Haneke
| 
|- Best Actress Emmanuelle Riva
| 
|- Best Original Screenplay Michael Haneke
| 
|- 70th Golden Globe Awards   Golden Globe Best Foreign Language Film
|rowspan="4"|Amour
| 
|- Motion Picture 60th Golden Reel Awards  Best Sound Editing - Sound Effects, Foley, Dialogue and ADR in a Foreign Feature Film
| 
|- 48th Guldbagge Awards    Best Foreign Film
| 
|- Houston Film 6th Houston Film Critics Awards  Best Foreign Language Film
| 
|- Best Actress Emmanuelle Riva
| 
|- 28th Independent Spirit Awards  Independent Spirit Best International Film Michael Haneke
| 
|- 10th Irish Film & Television Awards  Best International Film
|Amour
| 
|- Best International Actress Emmanuelle Riva
| 
|- Kansas City 46th Kansas City Film Critics Awards  Kansas City Best Foreign Language Film
|rowspan="4"|Amour
| 
|- 16th Las Vegas Film Critics Awards  Best Foreign Language Film
| 
|- London Film 33rd London Film Critics Circle Awards   Film of the Year
| 
|- Foreign Language Film of the Year
| 
|- London Film Actor of the Year
|Jean-Louis Trintignant
| 
|- London Film Actress of the Year Emmanuelle Riva
| 
|- Supporting Actress of the Year Isabelle Huppert
| 
|- London Film Director of the Year Michael Haneke
| 
|- Screenwriter of the Year
| 
|- Los Angeles 38th Los Angeles Film Critics Awards  Best Film
| Amour
|  
|- Los Angeles Best Actress Emmanuelle Riva
|  
|- 18th Lumières Awards  Best Film
| Amour
|  
|- Best Actress Emmanuelle Riva
|  
|- Best Actor
|Jean-Louis Trintignant
| 
|- Best Director Michael Haneke
|  
|- National Board 84th National Board of Review  Best Foreign Language Film
|rowspan="2"| Amour
|  
|- National Society 47th National Society of Film Critics Awards  Best Film
|  
|- Best Director Michael Haneke
|  
|- Best Actress Emmanuelle Riva
|  
|- 2012 New 78th New York Film Critics Circle Awards      New York Best Foreign Language Film
|Amour
| 
|- New York Best Actress Emmanuelle Riva
| 
|- New York 12th New York Film Critics Online Awards  Best Foreign Film
|Amour
| 
|- Best Actress Emmanuelle Riva
| 
|- 7th Oklahoma Film Critics Awards  Best Foreign Language Film
|rowspan="2"|Amour
| 
|- Online Film 16th Online Film Critics Society Awards  Online Film Best Film Not in the English Language
| 
|- Online Film Best Actress Emmanuelle Riva
| 
|- 13th Phoenix Film Critics Society Awards  Phoenix Film Best Foreign Language Film
|rowspan="2"|Amour
| 
|- Polish Film 15th Polish Academy Awards  Polish Academy Best European Film
| 
|- 69th Prix Louis Delluc 
| Best Film
|Amour
| 
|- San Diego 17th San Diego Film Critics Society Awards  San Diego Best Foreign Language Film
| Amour
| 
|- San Francisco 14th San Francisco Film Critics Awards  San Francisco Best Foreign Film
| Amour
| 
|- San Francisco Best Actress Emmanuelle Riva
| 
|- Satellite Awards 17th Satellite Awards  Satellite Award Best Foreign Language Film
|Amour
| 
|- Satellite Award Best Actress Emmanuelle Riva
| 
|- Southeastern Film 19th Southeastern Film Critics Awards  Southeastern Film Best Foreign Language Film
|Amour
| 
|- The Globe 2012 The Globe and Mail Review  Best Film
|Amour
| 
|- The Village 2012 The Village Voice Poll  Best Film
|Amour
| 
|- Best Actress Emmanuelle Riva
| 
|- Best Actor
|Jean-Louis Trintignant
| 
|- Toronto Film 16th Toronto Film Critics Association Awards  Toronto Film Best Foreign Language Film
|rowspan="2"|Amour
| 
|- Toronto Film Best Picture
| 
|- Toronto Film Best Actress Emmanuelle Riva
| 
|- 12th Utah Film Critics Awards  Best Non-English Language Feature
|rowspan="3"|Amour
| 
|- Vancouver Film 13th Vancouver Film Critics Circle 
| 
|- Washington D.C. 11th WDCAFCA Awards  Washington D.C. Best Foreign Language Film
| 
|- Washington D.C. Best Actress Emmanuelle Riva
| 
|}

===Best of 2012===
Both Sight & Sound film magazine and Peter Bradshaw of The Guardian named Amour the third best film of 2012.  

==See also==
* Isabelle Huppert filmography
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Austrian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
{{Navboxes
|title=Awards for Amour
|list1=
 
 
 
 
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 