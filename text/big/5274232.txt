The Fountainhead (film)
{{Infobox film
| name           = The Fountainhead
| image          = Fountainheadmp.jpg
| caption        = Promotional release poster
| director       = King Vidor
| producer       = Henry Blanke
| based on       =  
| screenplay     = Ayn Rand
| starring       = Gary Cooper Patricia Neal Raymond Massey Kent Smith
| music          = Max Steiner
| cinematography = Robert Burks
| editing        = David Weisbart
| distributor    = Warner Bros.
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = 
}}
 book of the same name by Ayn Rand, who wrote the screenplay adaptation.
 individualistic young architect who chooses to struggle in obscurity rather than compromise his artistic and personal vision, following his battle to practice what the public sees as modern architecture, which he believes to be superior, despite resistance from a traditionally minded architectural establishment. The complex relationships between Roark and the various kinds of individuals who assist or hinder his progress, or both, allow the film to be at once a romantic drama and a philosophical work. Roark is Rands embodiment of the human spirit, and his struggle represents the struggle between individualism and collectivism.
 Robert Douglas as Ellsworth Toohey and Kent Smith as Peter Keating. Although Rands screenplay was used with minimal alterations, Rand criticized the film for elements such as editing, production design and acting.

== Plot ==
Howard Roark (Gary Cooper) is an individualistic architect who follows a new artistic path in the face of conformity and vulgar mediocrity.
 Robert Douglas), an architecture critic for The Banner newspaper, opposes Roarks individualism and volunteers to crusade in print against him. The wealthy and influential publisher, Gail Wynand (Raymond Massey), pays little attention, but approves the idea and gives Toohey a free hand.

Dominique Francon (Patricia Neal), a glamorous socialite who writes a Banner column admires Roarks work and opposes the newspapers campaign against him. She is engaged to be married to an architect herself, the unimaginative Peter Keating (Kent Smith). She has never met or seen Roark, but she believes that he is doomed in a world that abhors individualism.

Wynand falls in love with Francon and exposes Keating as someone who values a big opportunity more than her. In the meantime, Roark is unable to find a client willing to build according to his vision. He walks away from opportunities that involve any compromise of his standards. Broke, he takes a job as a laborer in a quarry.

The quarry belongs to Francons father and is near their summer home. The vacationing Francon visits the quarry on a whim. As Roark drills into the stone, Francon spots him and watches him work. When he sees her they openly and repeatedly stare at each other.
 fades to sexual intercourse.

Back in his small room, Roark finds a letter offering him a new project. He packs up and leaves. Francon goes to the quarry and learns that he quit. The boss offers to find out where he went, but she declines. She has no idea that he is Howard Roark, the brilliant architect.

Wynand offers to marry Francon, even though he is aware that she is not in love with him. Francon defers the offer until she feels a great need to punish herself. She learns Roarks true identity when they are introduced at the party opening the new building that Roark has designed which The Banner has campaigned against.

Francon goes to Roarks apartment and offers to marry him if he gives up architecture to save himself from a hopeless struggle. Roark rejects her fears and says that they face many years apart until she overcomes the error of her thinking.

Francon finds Wynand and accepts his previous marriage proposal. Wynand agrees regardless of her true feelings or motives. Wynand discovers Roark as an architect and hires him to build Francon a secluded country home. Wynand and Roark become friends which drives Francon to jealousy over Roark.

Keating resurfaces. He has been employed to create an enormous housing project. It is beyond his skill, so he requests Roarks help. On one condition, Roark says, that if Keating promises to build it exactly as designed, Roark will design the project while permitting Keating to take all the credit.

With prodding from the envious Toohey, the firm backing the project decides to alter the design presented by Keating. They erect a housing development that departs from Roarks design in crucial ways. Roark decides, with Francons secret help, to rig explosives to the project and destroy it. Roark is arrested at the building site. In order to demonstrate Roarks guilt, Toohey breaks down Keating into privately confessing that Roark designed the project.

Roark goes on trial. He is painted as a public enemy by every newspaper apart from The Banner, where, breaking with previous policy, Wynand campaigns publicly on Roarks behalf. But under Wynands nose, Toohey has permeated The Banner with men loyal to him. Toohey has them quit and uses his clout to keep others out. He leads a campaign against The Banners new policy that all but kills the paper.

Operating the fading Banner with help only from Francon and a few loyal men, Wynand is exhausted by the struggle. Faced with losing the enterprise, he saves The Banner by bringing back Tooheys gang to join the rest of the public in condemning Roark.

Calling no witnesses, Roark addresses the court on his own behalf. He makes a long and eloquent speech defending his right to offer his own work on his own terms. He is found innocent of the charges against him.

A guilt-stricken Wynand summons the architect to his office. He presents him with a contract to design the Wynand Building, to be the greatest structure of all, with complete freedom to build it however Roark sees fit. Wynand maintains an impenetrably formal demeanor with his one-time friend. As soon as Roark leaves the room, Wynand commits suicide.

In the final scene, Francon enters the construction site of the Wynand Building, and identifies herself as Mrs. Roark. She rides the elevator towards Roark, awaiting her atop his magnificent new building.

== Cast ==
* Gary Cooper as Howard Roark
* Patricia Neal as Dominique Francon
* Raymond Massey as Gail Wynand
* Kent Smith as Peter Keating Robert Douglas as Ellsworth M. Toohey
* Henry Hull as Henry Cameron Ray Collins as Roger Enright
* Moroni Olsen as Chairman
* Jerome Cowan as Alvah Scarret

== Production ==
  as Dominique Francon. This role propelled Neal to film stardom.]]
Warner Bros. purchased the film rights to Ayn Rands The Fountainhead in late 1943, asking Rand to write the screenplay. Rand agreed, on the condition that not a single word of her dialogue be changed.    The Fountainhead went into production, with Mervyn LeRoy hired to direct, but the production was delayed.  LeRoy said that the delay was the result of the influence of the War Production Board, spurred by Rands anti-Russian politics. 

Three years later, production commenced under the direction of King Vidor, although there were disputes between Rand, Vidor and Warner Bros. throughout the production.  Vidor wanted Humphrey Bogart to play Howard Roark, while Rand wanted Gary Cooper to play the part.  Cooper was cast alongside Lauren Bacall as Dominique Francon, but Bacall was replaced by Patricia Neal.  Cooper criticized Neals audition as being badly acted, but she was cast against his judgment; during the production, Cooper and Neal began an affair. 

=== Writing ===
Rand completed her screenplay in June 1944. The setting of The Fountainhead is a collective society in which individuals and new ideas of architecture are not accepted, and all buildings must be constructed "like Greek temples, Gothic cathedrals and mongrels of every ancient style they could borrow", as Roarks patron Henry Cameron puts it in his deathbed speech. Rands screenplay, among other things, criticized the Hollywood film industry and its self-imposed mandate to "give the public what it wants."  Roark, in his architecture, refuses to give in to this demand "by the public." He refuses to work in any way that compromises his integrity, and in which he would succumb to the dictation of popular taste.  In a similar vein, Rand wrote a new scene for the film, in which Roark is rejected as architect for the Civic Opera Company of New York, an allusion to Edgar Kaufmann, Jr., Frank Lloyd Wright and the Civic Light Opera Company of Pittsburgh. 

While communism is not explicitly named, the film is also interpreted as a criticism of this ideology, as well as the lack of individual identity in a collective life under a communist society.     However, the novels criticisms were aimed at Franklin D. Roosevelts New Deal, and this is reflected in Rands endorsement of modernism in architecture in both the book and the film.    In adapting her novel, Rand utilized the melodrama genre to dramatize the novels sexuality and aesthetic of modernistic architecture. 

Patricia Neal remembered that Rand often visited the set to "protect her screenplay".  During filming, Vidor decided that Roarks speech at the end of the film was too long, and decided to omit segments that he did not feel relevant to the plot.  After learning of Vidors decision, Rand appealed to Jack Warner to honor her contract, and Warner persuaded Vidor to shoot the scene as she had written it.   

Rand later wrote a note thanking Warner and the studio for allowing the preservation of the novels "theme and spirit, without being asked to make bad taste concessions, such as a lesser studio would have demanded."   

Rand did however alter the films plot slightly, in order to be approved by the Production Code Administration. In the novel, Wynand divorces Dominique, but because the Motion Picture Production Code prohibited such divorces, Rand selected to have Wynand commit suicide instead. 

=== Production design ===
Rands screenplay instructed "It is the style of Frank Lloyd Wright -- and only of Frank Lloyd Wright -- that must be taken as a model for Roarks buildings. This is extremely important to us, since we must make the audience admire Roarks buildings."    According to Warner Bros., once it was known that the film had gone into production, the studio received letters from architects throughout the country suggesting designs; Wright himself turned down an offer to work on the film. 
 International Style" of the East Coast in the late 1940s than Wrights architecture of the mid-West in the 1920s when Rands book was written, and thus has its roots in German rather than American modernism. During filming, Rand told Gerald Loeb that she disliked this style, ascribing this later to the fact that Carrere had trained as an architect, but not practiced architecture. She described his designs as copied from pictures of "horrible modernistic buildings", and judged them as "embarrassingly bad".  The films closing image depicting Roark standing atop "the tallest structure in the world", which he designed, arguably evokes Futurism. 

=== Music score ===
The films score was composed by Max Steiner. Chris Matthew Sciabarra described Steiner as a "veritable film score architect   perhaps, the "fountainhead" of film music"    in analyzing Steiners appropriateness in composing the films music, and says that Steiners cues "immediately call to mind the story of Howard Roark." 

In Sciabarras article about the films music, he quotes philosophy professor Glenn Alexander Magee, who says that Steiners score suggested "a strong affinity for The Fountainhead     perfectly conveys the feel of a Rand novel".  Magee suggests that Steiners music accents themes of redemption and renewal present in the story, providing insight into Roarks opposition, Francons sense of life, and Wynands flaw. 
 Charles Gerhardt and released on LP in 1973 and reissued on CD. 

== Release and reception ==
Patricia Neal appeared on the NBC television series Hollywood Calling with Milton Berle to discuss their upcoming films, which included The Fountainhead and Berles Always Leave Them Laughing.    The film premiered at the Warners Hollywood theater. Warner Bros. erected two banks of bleachers on Hollywood Boulevard to accommodate fans that were expected to mob the premiere.  Neal attended the premiere with Kirk Douglas as her date, and the two signed autographs for fans.  The Los Angeles Times wrote that the audience "strongly responded to the unusual elements in the production."  After the film ended, Neal noticed that many people were avoiding her and turning their faces away, except for Virginia Mayo, who approached Neal and exclaimed, "My, werent you bad!"  Once Cooper saw the film as a whole, he felt that he had not delivered the final speech as he should have.  Compelled by the message of Rands novel, Cooper and Neal let it be known publicly that they were having an affair, and the public knowledge of their relationship somewhat negatively impacted the films box office.   

The Fountainhead went on to gross $2.1 million, $400,000 less than its production budget.  However, sales of Rands novel increased as a result of public interest in the book, spurred by this film.   In letters written at the time, the authors reaction to the film was positive, saying "The picture is more faithful to the novel than any other adaptation of a novel that Hollywood has ever produced"    and "It was a real triumph."  She conceded to friend DeWitt Emery that "I can see your point in feeling that Gary Coopers performance should have been stronger," but concluded, "I would rather see the part underplayed than overdone by some phony-looking ham."   However, she displayed a more negative attitude towards it later, saying that she "disliked the movie from beginning to end", and complaining about its editing, acting and other elements.  As a result of this film, Rand said that she would never sell any of her novels to a film company that did not allow her the right to pick the director and screenwriter as well as edit the film, as she did not want to encounter the same production problems that occurred on this film. 
 fascist movie".  The trade magazine Variety (magazine)|Variety called the film "cold, unemotional, loquacious   completely devoted to hammering home the theme that mans personal integrity stands above all law."   The New Yorker deemed the film to be "asinine and inept".  Cue described it as "shoddy, bombastic nonsense".  Bosley Crowther, in his review for The New York Times, called the film "wordy, involved and pretentious" and characterized Vidors work as a "vast succession of turgid scenes." 

== Legacy == philosophical novel into one of his finest and most personal films, mainly by pushing the phallic imagery so hard that it surpasses Rands rightist diatribes."  Architect David Rockwell, who saw the film when he visited New York City in 1964, has said that the film influenced his interest in architecture and design.    Rockwell also stated that, at his university, many architecture students named their dogs Roark as a tribute to the protagonist of the novel and film. 

== References ==
;General
 
*  
*  
*  
 

;Specific
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 