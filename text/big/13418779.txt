Garage (film)
 
{{Infobox film
| name           = Garage
| image          = Garage poster lg.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Lenny Abrahamson
| producer       = Ed Guiney
| writer         = Mark OHalloran
| starring       = Pat Shortt   Anne-Marie Duff   Conor J. Ryan
| music          = Stephen Rennicks Peter Robertson
| editing        = Isobel Stephenson
| distributor    = Element Pictures (IE) Soda Pictures (UK)
| released       =  
| runtime        = 85 minutes
| country        = Ireland
| language       = English
| budget         = 
| gross          = 
}}
Garage is a 2007 Irish film directed by Lenny Abrahamson and written by Mark OHalloran, the same team behind Adam & Paul. It stars Pat Shortt, Anne-Marie Duff and Conor J. Ryan. The film tells the story of a lonely petrol station attendant and how he slowly begins to come out of his shell.

Garage won the CICAE Art and Essai Cinema Prize at the Cannes Film Festival {{cite news | url = http://www.rte.ie/arts/2007/0528/garage.html | title = 
New Pat Shortt film wins at Cannes| author = RTE | date =  May 28, 2007 | accessdate = 2007-09-24 | work=RTÉ News}}  and the Best Film prize at the 25th Torino Film Festival.

==Plot==
Josie (Pat Shortt) is a good-natured man with learning difficulties who lives and works at a garage in a small rural Irish village. The owner, Mr Gallagher, is a former schoolmate who is not interested in the garage and is only waiting for the right offer from developers so he can sell. For Josie, one day rolls into another with nothing but his menial job and a few pints in the local pub, even though the regulars mock him and his ways. Kind-hearted Josies only other companion is a large horse that is tethered alone in a field. He talks to the animal and brings it food.

One day his boss hires his girlfriends 15-year-old son, David (Conor Ryan) to help Josie. Slowly Josie connects with David as they endure the slow and menial pace of the garage. One night after work, Josie innocently shares some beers with David. They sit and watch the sunset at the rear of the garage. Josie joins David and other local teenagers down by the railway tracks and brings beer for all of them. The new social aspects to his life lifts his confidence. At the local pub, he gets the courage to dance with Carmel, the local shopkeeper. But she shows her cynicism for Josie by explicitly telling him she feels no physical attraction towards him.

The friendship between Josie and David progresses nicely until one fateful day after work. Josie shows David a pornographic film which Josie received from a trucker who frequents the petrol station. David feels uncomfortable and leaves. Josie, sensing something is wrong, follows him outside but is unable to clarify the situation.

David returns the following week but does not stay after work.  Instead, he leaves with Declan, a local boy who openly mocks and despises Josie because he is different. Nevertheless Josie offers Declan a beer and cheerful goodbye. The next day, the local Garda come to the garage and take Josie to the police station because there has been "a complaint". It transpires that David told Declan about the previous weekends incident and word reached Davids mother that Josie had supplied her son with alcohol and shown him pornography. After an interview with a sympathetic officer, Josie explains it was just a bit of "craic" and "pure innocent". No charges are brought and he is released. But he is told to stay away from the town and especially to avoid contact with David. Josie returns to the garage. While eating dinner, he suddenly realises what has happened. Shocked, he puts his head in hands all alone in his little room at the back of the garage.

The next day Mr Gallagher comes to the garage. Although not explicitly stated, Josie is told that he or the garage are finished. Unable to sleep that night, Josie gets dressed and sits on the edge of his bed. He then goes down to the river at dawn and sits for a while on the bank. Josie then takes off his well-polished shoes and socks before neatly placing his garage cap on them. He then wades slowly into the water arms outstretched.

In the final scene, the lonely horse which was tethered in a field, has been cut free. It stops and looks directly into the camera as the screen fades to black.

==Cast==
* Pat Shortt as Josie John Keogh as Mr. Gallagher
* Anne Marie Duff as Carmel
* Conor J. Ryan as David Tommy Fitzgerald as Declan

==Production==
The film was shot on location in Cloghan, County Offaly|Cloghan, County Offaly; Woodford, County Galway|Woodford, County Galway; and Rathcabbin, County Tipperary, over a six-week period in late summer 2006. Some interior scenes were also shot in Dublin. The initial cut of the film was two hours long, but this was subsequently cut to its running time of 85 minutes. The film was financed by the Irish Film Board, Film 4, RTÉ and the Broadcasting Commission of Ireland. {{cite web | url = http://www.patshortt.com/garage3.htm | title =  archiveurl = archivedate = 2007-11-17}}  The films premiere was in Rathcabbin, where the garage part was filmed.   Garage initially had a limited release in 11 cinemas around Ireland, with a wider release afterwards. {{cite web
| url = http://archives.tcm.ie/businesspost/2007/09/23/story26858.asp
| title = Garage set for nationwide release in October
| date = 2007-09-23
| accessdate = 2007-10-06
}}
 

==Reception==
 
Garage received generally positive reviews, based on its showing at Cannes. The Irish Times declared "Pat Shortt is a revelation in the central role. He brings a wonderful physicality to the character of Josie." {{cite news | url = http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/film/cannes/article1830701.ece | title = 
The best of the rest of the world| author = Irish Times |  accessdate = 2007-09-27 | location=London | work=The Times | date=2007-05-24}} 
Screen daily also gave good reviews stating "The comic timing of the first two thirds of the film, on the part of both actor and director, is impeccable." {{cite web | url = http://www.screendaily.com/ScreenDailyArticle.aspx?intStoryID=32731 | title = 
Garage Review| author = Screen Daily |  accessdate = 2007-09-27}} 
RTÉ said "Playing the misfit with a poignancy that bears down on you more with every scene" and gave it four stars. {{cite news
| date = 2007-10-04
| author = Harry Guerin
| title = Garage Review
| url = http://www.rte.ie/arts/2007/1004/garage.html
| archiveurl = http://web.archive.org/web/20071011002940/http://www.rte.ie/arts/2007/1004/garage.html
| archivedate = 2007-10-11
| publisher = RTÉ
}}
 

===Awards=== Torino Film Festival and the Pusan Film Festival. {{cite web | url = http://www.garagethefilm.com/Home.html | title = 
Garage the film home| author =  ) and Best Actor in a Lead Role Film for Pat Shortt. {{cite web
| url = http://www.ifta.ie/pressreleases/2008/news17-02-08.htm
| title = IFTA 2008 Winners Official Press Release
| accessdate=2008-02-18
}}
 

==References==
 

==External links==
*  
*  
*  
*    at the 2007 Toronto International Film Festival
*   at Pearl and Dean
*     on Orange Film

 

 
 
 
 
 