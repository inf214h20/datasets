The Lone Ranger Rides Again
{{Infobox film
| name      = The Lone Ranger Rides Again
| image     = Lonerangerridesagain.JPG John English
| producer    = Robert M. Beche
| writer     = Franklin Adreoon Ronald Davidson Gerald Geraghty Barry Shipman Sol shor Fran Striker (radio show) George W. Trendle (radio show) Robert Livingston Jinx Falken Ralph Dunn J. Farrell MacDonald William Nobles
| editing    = Helene Turner Edward Todd
| music     = William Lava
| distributor  = Republic Pictures
| released       = February 25, 1939 
| runtime         = 15 chapters (263 minutes) 
| country    = United States| English
| budget          = $193,878 (negative cost: $213,997) 
}} Republic Serial The Lone Ranger, which had been highly successful, and the thirteenth of the sixty-six serials produced by Republic.
 lost for a long time but copies, with Spanish subtitles, have since been found and re-issued.  

==Plot==
  Homesteaders moving into a valley in New Mexico are being attacked by the Black Raiders. The valley had been settled by rancher Craig Dolan, who does not want the new homesteaders to be there. His son, Bart, has taken matters into his own hands and formed the Black Raiders. The Lone Ranger attempts to aid the homesteaders but he is hampered by the fact that he has been framed for being part of the Raiders. In particular, Juan Vasquez believes that he killed his brother, although when this is disproven he becomes another of the Lone Rangers partners. However, the Ranger is forced to remove the mask and operate under the name of "Bill Andrews" at times in order to successfully protect the homesteaders.

==Cast==
;Main cast Robert Livingston as The Lone Ranger and undercover as homesteader Bill Andrews.  Avoiding the deliberate mystery of the radio show and the gradual revelation of the first serial, the Lone Ranger is clearly revealed as Bill Andrews from the start. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | chapter = 12. The Westerns "Who Was That Masked Man!"
 | page = 310
 }} 
* Chief Thundercloud as Tonto (Lone Ranger character)|Tonto, the Lone Rangers sidekick
* Silver Chief as Silver, the Lone Rangers horse.  Silver Chief replaced Silver King, the horse in the original serial. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | chapter = 4. Perilous Saturdays
 | page = 114
 }} 
* Duncan Renaldo as Juan Vasquez, who originally believes the Lone Ranger killed his brother Jinx Falken as Sue Dolan
* Ralph Dunn as Bart Dolan, Craig Dolans son, the villain and leader of the Black Raiders
* J. Farrell MacDonald as Craig Dolan

;Supporting cast William Gould as Jed Scott
* Rex Lease as Evans
* Ted Mapes as Merritt, a settler
* Henry Otho as Pa Daniels
* John Beach as Hardin, one of the Black Raiders
* Glenn Strange as Thorne, one of the Black Raiders
* Stanley Blystone as Murdock, one of the Black Raiders
* Eddie Parker as Hank, one of the Black Raiders Al Taylor as Colt, one of the Black Raiders Carlton Young as Logan
* Forrest Taylor (uncredited) as Judge Miller

;Additional cast
* Billy Bletcher as the voice of The Lone Ranger

==Production== Captain America (1944, $222,906), just beating Secret Service in Darkest Africa (1943, $210,033). {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 36–37
 | chapter =
 }} 

The studio was willing to spend so much on this serial because the previous Lone Ranger serial had been a major success and was making a profit after only a few months on release. {{cite book
 | last = Witney
 | first = William
 | authorlink = William Witney
 | title = In a Door, Into a Fight, Out a Door, Into a Chase
 | year = 2005
 | publisher = McFarland & Company
 | isbn = 978-0-7864-2258-6
 | pages =
 | chapter =
 }} 

It was filmed between 9 December 1938 and 20 January 1939 under the working title The Lone Ranger Returns.   The serials production number was 895. 
 The Lone Ranger but for the first time the directors insisted on being part of the casting process for this serial. 

===Stunts===
*Yakima Canutt
*Tommy Coats
*George DeNormand
*Ted Mapes
*Eddie Parker
*Post Park David Sharpe Ted Wells
*Bud Wolfe
*Bill Yrigoyen
*Joe Yrigoyen

==Release==

===Theatrical===
The Lone Ranger Rides Agains official release date is 25 February 1939, although this is actually the date the seventh chapter was made available to film exchanges. 

==Chapter titles==
#The Lone Ranger Returns (28 min 54s)
#Masked Victory (16 min 43s)
#The Black Raiders Strike (16 min 45s)
#The Cavern of Doom (16 min 44s)
#Agents of Deceit (16 min 37s)
#The Trap (16 min 39s)
#Lone Ranger at Bay (16 min 42s)
#Ambush (16 min 40s)
#Wheels of Doom (16 min 44s)
#The Dangerous Captive (16 min 37
#Death Below (16 min 40s)
#Blazing Peril (16 min 41s) -- Clipshow|Re-Cap Chapter
#Exposed (16 min 42s)
#Besieged (16 min 39s)
#Frontier Justice (16 min 45s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 224
 }} 

==See also==
*List of film serials
*List of film serials by studio

==References==
 

==External links==
* 
* 
* 
* 

 
{{Succession box Republic Serial Serial
| before=Hawk of the Wilderness (1938 in film|1938)
| years=The Lone Ranger Rides Again (1939 in film|1939)
| after=Daredevils of the Red Circle (1939 in film|1939)}}
{{Succession box English Serial Serial
| before=Hawk of the Wilderness (1938 in film|1938)
| years=The Lone Ranger Rides Again (1939 in film|1939)
| after=Daredevils of the Red Circle (1939 in film|1939)}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 