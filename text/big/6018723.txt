Killing Zelda Sparks
{{Infobox film
| name           = Killing Zelda Sparks
| image          = Killing Zelda Sparks FilmPoster.jpeg
| caption        = 
| director       = Jeff Glickman
| producer       = 
| writer         = Josh Ben Friedman
| starring       = Colm Feore Sarah Carter Vincent Kartheiser Geoffrey Arend
| music          = 
| cinematography = 
| editing        = 
| distributor    = Warner Bros. Lightyear Entertainment
| released       =  
| runtime        = 95 minutes
| country        = Canada United States
| language       = English
| budget         = 
}}
 Copper Cliff, Greater Sudbury|Sudbury, Ontario, Canada standing in for the town of New Essex.  Post production was completed on January 24, 2007.  The film stars Colm Feore, Sarah Carter, Vincent Kartheiser, and Geoffrey Arend.  It is directed by Jeff Glickman and adapted for the screen by Josh Ben Friedman from his play Barstool Words.

The film was released on DVD on May 20, 2008.

==Plot==
When Zelda Sparks comes back to the small town of New Essex, two old high school buddies pull a vicious prank on her for wronging them in the past.  But they are shocked to learn that the prank may have turned deadly. 

==References==
 

== External links ==
* 

 
 
 
 
 
 
 