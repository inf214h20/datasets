Marriage Italian Style
{{Infobox film
| name           = Marriage Italian Style
| image          = Marriage Italian Style.jpg
| caption        = 
| director       = Vittorio De Sica
| producer       = Carlo Ponti
| writer         = Eduardo De Filippo Renato Castellani Tonino Guerra Leo Benvenuti Piero De Bernardi
| starring       = Sophia Loren Marcello Mastroianni Aldo Puglisi Tecla Scarano
| music          = Armando Trovajoli
| cinematography = Roberto Gerardi
| editing        = Adriana Novelli Surf Film
| released       = 1964
| runtime        = 102 minutes
| country        = Italy France
| language       = Italian
| budget         = 
}}
Marriage Italian Style ( ) is a 1964 film by Vittorio De Sica.

==Plot==
It tells the World War II era story of a cynical, successful businessman named Domenico (Mastroianni), who, after meeting a naive country girl, Filumena (Loren), one night in a Neapolitan brothel, keeps frequenting her for years in an on-off relationship (as she continues working as a prostitute).  He eventually takes her in his house as a semi-official mistress under the pretense that she take care of his ailing, senile mother.  After having fallen for a younger, prettier girl and having planned to marry her, he finds himself cornered when Filumena feigns illness and "on her deathbed", asks to be married to him.  Thinking shell be dead in a matter of hours and that the marriage wont even be registered, he agrees.  After having been proclaimed his legal bride, the shrewd and resourceful Filumena drops the charade and reveals to have put up the show for the one child she bore from him (she gave birth to three sons but Domenico always maintained to have fathered none).  Domenico tries to cajole her into telling him which one is his but she stalwartly refuses, telling him that sons cant be picked and chosen and that he has to be the father of all three.  It stars Sophia Loren, Marcello Mastroianni and Vito Moricone.   

One of the films most memorable moments is when Domenico is on the phone with his new flame, shortly after having married the "moribund" Filumena.  As he reassures his fiancée that death is near, a wild-eyed and vengeful Filumena emerges from a curtain behind him and exclaims in Neapolitan that she is in fact alive and well—the Madonna having taken pity on her.

The film was adapted by Leonardo Benvenuti, Renato Castellani, Piero De Bernardi and Tonino Guerra from the play Filumena Marturano by Eduardo De Filippo.  It was directed by Vittorio De Sica.

Filumena Marturano had already been adapted as a film in 1950 in Argentina. 

==Cast==
* Sophia Loren - Filumena Marturano
* Marcello Mastroianni - Domenico Soriano
* Aldo Puglisi - Alfredo
* Tecla Scarano - Rosalia
* Marilù Tolo - Diana (as Marilu Tolo)
* Gianni Ridolfi - Umberto
* Generoso Cortini - Michele
* Vito Moricone - Riccardo
* Rita Piccione - Teresina, seamstress
* Lino Mattera
* Alfio Vita- Una pasticcere
* Alberto Castaldi - (as Alberto Gastaldi)
* Anna Santoro
* Enza Maggi - Lucia, maid
* Mara Marilli

==Awards== Best Foreign Best Actress in a Leading Role in 1965. It was also entered into the 4th Moscow International Film Festival.   

==See also==
* Filumena Marturano
* List of submissions to the 38th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 