Belly (film)
 
{{Infobox film
| name           = Belly
| image          = Belly poster.jpg
| caption        = Theatrical release poster
| director       = Hype Williams
| producer       = Larry Meistrich Ron Rotholz Robert Salerno Hype Williams
| story          = Nas Anthony Bodden Hype Williams
| writer         = Hype Williams DMX Taral Tionne "T-Boz" Watkins  Method Man
| narrator       = Nas 
| cinematography = Malik Hassan Sayeed
| editing        = David Leonard
| music          = Stephen Cullo
| studio         = Big Dog Films
| distributor    = Artisan Entertainment
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $9,639,390   
}}
 directing debut. DMX and Nas, alongside with Taral Hicks, Method Man, dancehall artist Louie Rankin and R&B singer Tionne Watkins|T-Boz. Besides starring in the film, Nas also narrates and collaborated with Hype Williams on the films script along with DMX (who, uncredited, also narrates the beginning and the end parts of the film).

==Plot== Tionne "T-Boz" Watkins) and infant daughter Kenya. Meanwhile, Tommy learns of a new form of heroin which he takes as a lucrative business opportunity.

The next day, Tommy meets with Sincere to discuss the venture. His rhetoric is met with resistance from the more intellectually inclined Sincere, who has become enlightened to the plight of urban poor and seeks a life free of crime. After Tommy ridicules Sincere for his sentiments and mentions his connection with a Jamaican drug lord "Ox" residing in New York City, Sincere reluctantly agrees. Tommy then visits Ox in his lavish mansion, to plead for Oxs assistance in obtaining the heroin. Ox agrees on the grounds that Tommy repays him via a huge favor at a later date.

In Marks grandmothers basement, the group convenes to discuss the imminent flux of business. Sin explains via narration that one of Tommys associates, Knowledge, is to be involved in the operation. Meanwhile, Sincere teases Black, who is upset that his share of the money from the robbery was less than Sincere received. While this is happening, Knowledge tells Tommy over the phone that he had heard that Black had been talking about wanting to rob Sincere. Tommy becomes incredibly enraged. He pushes Black to the floor and forces him to strip naked in front of everyone while firing a handgun wildly into the floor. Tommy then orders Black to sit on the couch, as he visibly harbors hostile feelings towards the rest of the men.
 Queens to Omaha, and begin to overrun the drug business there. This is met with jealousy from a local drug dealer Big Head Rico (Tyrin Turner), who informs the police of their activities. This results in a raid at their stash-house, which results Marks death and Knowledges arrest. Knowledge calls Tommys house from jail and is told by Keisha that Tommy isnt there, nor should he have called since the FBI could have tapped their phone. Knowledge gets angry that Tommy wont come to bail him out, and calls Shameek, aka Father Sha (Method Man), to not only infiltrate Ricos gang, but to handle Tommy. While at a strip club in Omaha, Shameek confronts Rico and informs him that Knowledge sent him here to kill him and his associates. When Rico tries to run from Shameek, Shameek shoots and kills Rico as well as some members of his crew. While reloading, Shameek is shot by the bartender. Stumbling out the club, he manages to evade police.

Meanwhile, Tommy goes to Jamaica with Ox, and repays him by killing Sosa, the son of a drug lord there. Back at Tommys house, Keisha is arrested by police and later bailed out by Tionne. Tommy finds out about the raid and leaves town. Later, when Sosas family and friends finds out that it was Ox who ordered the hit, Pelpa, the gangs leader and close friend of Sosa, sends a hit squad to kill him in his home. Ox is able to kill them all before a femme fatale named Chiquita drops down from his top floor stairwell and slits his throat.

While these events occur, Sincere prepares to leave the drug game and move his girlfriend and child to Africa, despite Tommys jokes that he was getting soft for wanting to leave. They make plans to leave on New Years Day, 2000. However, Sincere is now suspicious of Tommy since the Omaha incident. Tommy, meanwhile, has been laying low in Atlanta, selling marijuana with Wise and LaKid, two younger men from New York. One night at dinner, Tommy instigates an argument between Wise and LaKid, in which ends with both men to drawing their guns and LaKid shooting Wise dead. Tommy, extremely intoxicated, remains seated, calmly glancing at Wises dead body as the police arrive to arrest him and is, inexplicably, later released. It is later revealed that Tommy was chosen by federal agents to assassinate a black Muslim leader, Rev. Saviour, (Dr. Benjamin Chavis) who plans to preach against the government on New Years Eve.

Tionne comes home from shopping to find Shameek and a few other men waiting for her inside and is confronted concerning the whereabouts of Sincere and Tommy. After a tense standoff with pistols, the men leave. Later while talking to a friend (AZ (rapper)|AZ) outside a barbershop, Black and an accomplice confront Sincere and shoots him in the leg, causing Sincere to shoot back in self-defense, killing Black and his accomplice, before fleeing the scene.

On New Years Eve, Tommy confronts Rev. Saviour in his study before he was to make a massive speech of hope and points his gun at him. Saviour convinces Tommy not to go through with his mission, even though this will be seen as an act of betrayal by the feds and will put his life at risk. Tommy, in tears, agrees, and the two men embrace.

Shameek visits Keishas home, in the hopes of finding Tommy there. He physically assaults Keisha, and after a struggle, Keisha gets ahold of Shameeks gun, and shoots him in the face at point-blank range.

Sincere, now in Africa with Tionne and their child, reflects on recent events, relieved and happy to start a new life.

==Cast==
*Nas – Sincere DMX – Tommy "Buns" Bundy
*Taral Hicks – Keisha Tionne "T-Boz" Watkins – Tionne
*Method Man – Shameek aka Father Sha
*Tyrin Turner – Big Head Rico
*Hassan Johnson – Mark
*Oliver "Power" Grant – Knowledge
*Louie Rankin – Lennox aka Ox
*Stan Drayton – Wise
*James Parris – LaKid
*Benjamin Chavis | Minister Benjamin F. Muhammed - Reverend Saviour
*Sean Paul – Himself
* Lavita “Vita” Raynor – Kionna
* Ghostface Killah – cameo appearance

==Reception==
The film was poorly received by critics, scoring 13% on Rotten Tomatoes.  Although it was generally praised for its highly stylized "Film noir|noir-like"  visual design and cinematography, it was criticized for what was seen as a weak plot. 

==Possible sequel== The Game. Lions Gate Entertainment gave the Games version of the film the "Belly 2" prefix, from a marketing standpoint, to attract a broader hip-hop audience, but the films are in no way related.

==Soundtrack==
{|class="wikitable" 
|- Year
!rowspan="2"|Album Peak chart positions Certifications
|-
!width=40| Billboard 200|U.S. 
!width=40| Top R&B/Hip-Hop Albums|U.S. R&B 
|- 1998
|Belly (soundtrack)|Belly 
* Released: November 3, 1998 Def Jam 5
| 2
|
*Recording Industry Association of America|US: Platinum
|}

==References==
 
 
#  LaSalle, Mick (Nov. 4, 1998).  . San Francisco Chronicle.
#  Richberg, Chris and Williams, Houston (Jun. 12, 2006).  . AllHipHop.com: Daily Hip-Hop News.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 