Run Ronnie Run
 
{{Infobox film
| name            = Run Ronnie Run
| image           = Run Ronnie Run.jpg
| caption         = Release poster
| director        = Troy Miller
| producer        = Troy Miller Mark Burg Oren Koules
| writer          = David Cross Bob Odenkirk Scott Aukerman B. J. Porter Brian Posehn
| starring        = David Cross Bob Odenkirk David Koechner Jill Talley Ben Stiller Jack Black
| music           = Scott Aukerman Eban Schletter
| cinematography  = Shawn Maurer
| editing         = Dean Holland
| distributor     = New Line Home Video
| released        =  
| runtime         = 86 minutes
| country         = United States
| language        = English
}}
Run Ronnie Run is an American comedy film & a Spin-off (media)|spin-off inspired by the HBO sketch comedy show Mr. Show. The recurring character Ronnie Dobbs (David Cross) is the focal point of the movie. It was directed by Troy Miller. While the film was produced in 2001 it was released direct-to-video in 2003.

==Plot==
 
 redneck petty criminal whose hijinks are caught on tape by a Cops (1989 TV series)|Cops-like television show called Fuzz—is noticed by failing infomercial personality/inventor Terry Twillstein (Bob Odenkirk), who notices Dobbss popularity with lowbrow viewers. He promotes the idea for a Ronnie Dobbs show to television executives entitled "Ronnie Dobbs Gets Arrested" in which Ronnie is arrested in a different city each week. The show becomes a phenomenal success leading to a level of fame & fortune that dramatically changes Dobbs life.

==Cast==
* David Cross as Ronnie Dobbs / Pootie T / voice of Chow Chow
* Bob Odenkirk as Terry Twillstein / Wolfgang Amadeus Thelonious Von Funkenmeister the XIX 3 / 4 / Daffy Mal Yinkle Yankle
* David Koechner as Clay
* Jill Talley as Tammy
* Ben Stiller as Himself
* Jack Black as Lead chimney sweep
* Brian Posehn as Tank
* Patton Oswalt as Dozer
* M.C. Gainey as Hark Trellis
* Dave Foley, Andy Richter, and Sarah Silverman as Network executives
* John Stamos and Rebecca Romijn as Themselves
* Trey Parker and Matt Stone as Themselves
* Mandy Patinkin as Himself
* Nikki Cox as Kayla
* Laura Kightlinger as Birthday woman
* Jeff Garlin as Birthday womans friend
* Scott Ian as Himself
* Patrick Warburton as Head of gay conspiracy
* Kathy Griffin as Herself
* Garry Shandling as Himself
* Mary Lynn Rajskub as Herself
* Rhoda Griffis as TV anchorwoman
* Scott Adsit as Police negotiator
* R. Lee Ermey as Lead kidnapper
* Scott Aukerman as Starving kidnapper
* Deborah Theaker (uncredited) as Mrs. Robinson
* Jeff Goldblum (uncredited) as Himself
* David Baddiel (uncredited) as Himself
 Odenkirk and Cross portrayed Three Times One Minus One. Jack Black starred as a chimney sweep who sings "The Golden Rule Song".

Many well known celebrities had brief cameos in the film, such as Trey Parker, Matt Stone, John Stamos, Rebecca Romijn, Ben Stiller, Jeff Goldblum, Blaine Cartwright, Mandy Patinkin, David Baddiel, Jeff Garlin, Scott Ian & Kathy Griffin.

==Production== Arrested Development.

==Reception==
 
The film was reviewed favorably by most critics, but was dismissed by others including those in Mr. Show’s fan base, most likely based on Odenkirks comments. Rotten Tomatoes has given the film a 75% fresh rating. Bob Odenkirk has been outspoken about his dissatisfaction with the edit, while David Cross has stated it is ultimately the film that they wrote.

==See also==
* Bob Odenkirk
* David Cross
* Mr. Show

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 