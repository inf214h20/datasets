The Unusual Youth
 
 
{{Infobox film
| name           = The Unusual Youth
| image          = UnusualYouth.jpg
| caption        = Promotional poster
| film name =  
| producer       = Dennis Law Herman Yau Dennis Law
| writer         = Dennis Law
| cinematography = Herman Yau
| music          = Tommy Wai
| editing        = Yau Chi-Wai Raymond Wong studio  Point of View Movie Production Co. Ltd.
| distributor    = China Star Entertainment Group
| released       =  
| runtime        = 99 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}}
 2005 Cinema Hong Kong teen comedy-drama 2R fame, Raymond Wong. Several veteran Milkyway Image actors, which include Law Wing-Cheong, Cheung Siu-Fai, Lam Suet and Simon Yam (in a brief, unbilled appearance) make cameo appearances.
 directing debut Dennis Law, chairman for Point of View Movie Production Co. Ltd.

==Cast==
* Race Wong as Suki
* Yan Ng as May May
* Marco Lok as Leung Guy-Cheung, a.k.a. Big Chick
* Raymond Wong Ho-Yin as Biggie
* Helena Law as Grandma
* Kitty Yuen as Siu Yiu
* Sammy Leung as Sai Hung
* Lam Suet as Uncle Hong
* Cheung Siu-Fai as Kwok Sir
* Simon Yam (uncredited) as Police Superintendent
* Law Wing-Cheong as Inspector Lam

==External links==
*  
*  
*  

 

 
 
 
 
 
 

 