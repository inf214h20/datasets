Mississippi (film)
{{Infobox film
| name           = Mississippi
| image          = Mississippi 1935 film.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = A. Edward Sutherland
| producer       = Arthur Hornblow Jr.
| screenplay     = {{Plainlist|
* Francis Martin
* Jack Cunningham
}}
| based on       =  
| starring       = {{Plainlist|
* Bing Crosby
* W. C. Fields
* Joan Bennett
}}
| music          = Howard Jackson (uncredited)
| cinematography = Charles Lang
| editing        = Chandler House
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} musical comedy film directed by A. Edward Sutherland and starring Bing Crosby, W. C. Fields, and Joan Bennett. Written by Francis Martin and Jack Cunningham based on the novel Magnolia by Booth Tarkington, the film is about a young pacifist who, after refusing on principle to defend his sweethearts honor and being banished in disgrace, joins a riverboat troupe as a singer and acquires a reputation as a crackshot after a saloon brawl in which a villain accidentally kills himself with his own gun. The film was produced and distributed by Paramount Pictures. Mississippi has the distinction of being the only W. C. Fields film with a score by Richard Rodgers and Lorenz Hart. Also, it is the only film in which he co-starred with Bing Crosby. Photographed by Charles Lang, the film featured art direction by Hans Dreier and Bernard Herzburn and was edited by Chandler House. The sound man was Eugene Merritt. The original running time of this black-and-white film was 80 minutes. The film has been released on VHS and DVD as part of the W.C. Fields Collection in the United Kingdom.

==Plot==
Commodore Jackson (W. C. Fields) is the captain of a Mississippi showboat in the late nineteenth century. Tom Grayson (Bing Crosby) is engaged to be married and has been disgraced for refusing to fight a duel with Major Patterson (John Miljan).

Accused of being a coward, Grayson joins Jacksons showboat. Over the duration of the film, the behaviour of the meek and mild Tom Grayson alters as a consequence of the constant representation of him, by Commodore Jackson, as "The Notorious Colonel Steele", "the Singing Killer", and the constant attribution, by Jackson, of duelling victories by Grayson to unrelated corpses freshly dragged from the river beside the showboat as "yet another victim of the notorious Colonel Steele, the Singing Killer".

The film provides sufficient opportunities for Crosby to sing the Rodgers and Hart songs, including the centerpiece number, "Soon", while Fields gets to tell some outlandish stories. Crosby and Fields worked well together and there is one memorable scene in which Fields tries to tell Crosby how to act tougher. In the film, Crosby does a number of brilliantly engineered sight gags involving a chair and a bowie knife. Another highlight is Fields remarkable story about his exploits among one notorious Indian tribe.

==Cast==
* Bing Crosby as Tom Grayson
* W. C. Fields as Commodore Jackson
* Joan Bennett as Lucy Rumford
* Queenie Smith as Alabam
* Gail Patrick as Elvira Rumford
* Claude Gillingwater Sr. as General Rumford
* John Miljan as Major Patterson
* Edward Pawley as Joe Patterson
* Fred Kohler as Captain Blackie
* Five Cabin Kids as the Cabin Kids John Larkin as Rumbo
* Libby Taylor as Lavinia
* Teresa Maxwell-Conover as Miss Markham Paul Hurst as Hefty
* Jan Duggan as Thrilled Passenger in Pilot House   Arthur Knight 

==Notes==
There were two previous Paramount film versions of Booth Tarkingtons play, Magnolia. The first in 1924 filmed as a silent under the title The Fighting Coward starred Cullen Landis, Phyllis Haver, Mary Astor, Ernest Torrence and Noah Beery, Sr.. The second version released in 1929 , as River of Romance;  in early talkie and in silent editions, starred Buddy Rogers, Wallace Beery, Fred Kohler, Mary Brian, June Collyer and Henry B. Walthall. Fred Kohler reprises his Captain Blackie here from the 1929 film.

==Reception==
*New York Times - "Amid an atmosphere of magnolia, crinoline, and Kentucky whiskey, the boozy genius of Mr. Fields and the subterranean croon of Mr. Crosby strike a happy compromise."
*Motion Picture Herald - "The   is a melodramatic and sometimes tense romance.  Fields comedy, in both dialogue and action, is good for its full quota of laughs."
*Variety (magazine)|Variety - "Paramount obviously couldnt make up its mind what it wanted to do with the film; its rambling and hokey. For a few minutes its sheer farce, for a few moments its romance. And it never jells...Fields works hard throughout the film and saves it, giving it whatever entertainment value it has."

==Sources==
*Deschner, Donald, The Films of W.C. Fields (New York: The Citadel Press, 1966)

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 