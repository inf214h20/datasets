Puthri
{{Infobox film 
| name           = Puthri
| image          =
| caption        =
| director       = P. Subramaniam
| producer       = P Subramaniam
| writer         = Kanam EJ
| screenplay     = Kanam EJ Madhu Thikkurissi Sukumaran Nair Babu Joseph Aranmula Ponnamma
| music          = MB Sreenivasan
| cinematography = ENC Nair
| editing        = N Gopalakrishnan
| studio         = Neela
| distributor    = Neela
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film,  directed and produced by P. Subramaniam. The film stars Madhu (actor)|Madhu, Thikkurissi Sukumaran Nair, Babu Joseph and Aranmula Ponnamma in lead roles. The film had musical score by MB Sreenivasan.   

==Cast== Madhu
*Thikkurissi Sukumaran Nair
*Babu Joseph
*Aranmula Ponnamma GK Pillai
*Jyothikumar
*Pankajavalli
*S. P. Pillai
*K. V. Shanthi
*Sarasamma

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaanaan Kothichenne || S Janaki || ONV Kurup || 
|-
| 2 || Kaattupoovin Kalyaanathinu || K. J. Yesudas || ONV Kurup || 
|-
| 3 || Kanpeeli || P. Leela, Kamukara || ONV Kurup || 
|-
| 4 || Paapathin Pushpangal || Kamukara || ONV Kurup || 
|-
| 5 || Thaazhathe Cholayil || S Janaki || ONV Kurup || 
|-
| 6 || Thozhukaithirinaalam || P. Leela || ONV Kurup || 
|-
| 7 || Vaarmukile Vaarmukile || Kamukara || ONV Kurup || 
|-
| 8 || Vaarmukile Vaarmukile || S Janaki || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 


 