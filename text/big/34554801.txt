The Verona Trial
 
{{Infobox film
| name = The Verona Trial
| image = The Verona Trial.jpg
| caption =
| director = Carlo Lizzani
| writer =
| starring = Silvana Mangano
| music = Mario Nascimbene
| cinematography = Leonida Barboni
| editing =
| producer = Dino De Laurentiis
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget =
}} Italian historical Italian fascist regime, in particular the affair of the 1944 Verona trial, in which Galeazzo Ciano, Emilio De Bono and other eminent Fascist officials were sentenced to death and almost immediately executed by a shooting detachment. 
 Edda Mussolini-Ciano, for Best in the same category. 

== Cast ==
* Silvana Mangano as Edda Ciano (Mussolinis daughter; Cianos wife) Frank Wolff as  count Galeazzo Ciano (Mussolinis son-in-law and former Minister of Foreign Affairs) Donna Rachele (Mussolinis wife)
* Françoise Prévost (actress)|Françoise Prévost as  Frau Beetz (a member of a German Secret Service)
* Salvo Randone as  Andrea Fortunato (public prosecutor)
* Giorgio De Lullo as  Alessandro Pavolini (a fanatic fascist, who later led the execution)
* Ivo Garrani as  Roberto Farinacci
* Andrea Checchi as  Dino Grandi
* Henri Serre as  Emilio Pucci
* Claudio Gora as  Judge Vincenzo Cersosimo
* Carlo DAngelo as  Console Vianini
* Umberto DOrsi as  Luciano Gottardi
* Filippo Scelzo as Giovanni Marinelli
* Andrea Bosic as Tullio Cianetti

==References==
 

==Further reading==
* Carlo Lizzani, Ugo Pirro, Il Processo di Verona, Capelli, 1963

==External links==
* 

 

 
 
 
 
 
 
 
 

 