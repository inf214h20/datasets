I Am Zozo
 
{{Infobox film
| name           = I am Zozo
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Scott Di Lalla
| producer       = Zack Coffman, Martin Perlberger
| writer         = Scott Di Lalla
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Kelly McLaren, Courtney Foxworthy, Demetrius Sager
| music          = BC Smith
| cinematography = Scott Di Lalla
| editing        = Zack Coffman 
| studio         = One World Studios Ltd.
| distributor    = Image Entertainment
| released       =  
| runtime        = 85 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}} horror thriller film that was directed by Scott Di Lalla. The film first released on February 18, 2012 and was picked up officially in 2013 by Image Entertainment.    I am Zozo follows a group of people that run afoul of a demon after using a Ouija board. 

==Plot==
The film follows five friends that end up attracting a demon to them after they decide to play with a Ouija board on Halloween.

==Cast==
*Kelly McLaren as Tess
*Courtney Foxworthy as Mel
*Demetrius Sager as Aiden
*Caleb Courtney as Nick
*Caleb Debattista as Dean
*Darren Wayne Evans as Himself
*John Vejvoda as Skip
*Sophia Mitri Schloss as Young Tess
*Demeri Millikin as Cello Tess

==Reception==
Critical reception for the movie has been mixed, with most reviews either being solidly positive, praising the films unique look and suspense, or very negative focusing on its lack of story structure or expected horror violence. Christopher Gibson of trulydisturbing.com proclaimed I Am ZoZo "the coolest movie I have ever seen about a Ouija board."  horrophilia called the film "a visually stunning arthouse picture."  roguecinema.com gave the film a B, calling I Am ZoZo a "good little supernatural thriller"  with assignmentx.com finding that "the film has a look that harks back to the ‘70s and early ‘80s, when all kinds of crazy stuff was scaring people in the cinema."  themortonreport.com offered a mixed review calling the film both "frustrating" and "a visually interesting film from start to finish."  Shock Till You Drop panned the film, saying that while they liked the idea of shooting the entire film on Super 8, there was little else they enjoyed about the feature.  JoBlo.com and Bloody Disgusting both gave scathing reviews,  with JoBlo.com saying that it "fails on almost every discernible level" and that they did not recommend it. 

===Awards===
* Best Feature Award at the United States Super 8 Film Festival (2012)  
* Audience Choice Award at the United States Super 8 Film Festival (2012) 

==References==
 

==External links==
*  
*  

 
 
 


 