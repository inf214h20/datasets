New Dragon Gate Inn
 
 
{{Infobox film name = New Dragon Gate Inn image = new dragon gate inn dvd cover.jpg alt =  caption = Region 1 DVD cover traditional = 新龍門客棧 simplified = 新龙门客栈 pinyin = Xīn Lóng Mén Kè Zhàn jyutping = San1 Lung4 Mun4 Haak3 Zaan6}} director = Raymond Lee producer = Tsui Hark writer = Tsui Hark Charcoal Tan Hiu Wing starring = Brigitte Lin Tony Leung Ka-fai Maggie Cheung Donnie Yen music = Philip Chan Chow Gam-wing cinematography = Arthur Wong Tom Lau editing = Poon Hung studio = Film Workshop Seasonal Film Corporation distributor = Golden Harvest released =   runtime = 103 minutes country = Hong Kong language = Cantonese budget =  gross = 
}}
New Dragon Gate Inn is a 1992 Hong Kong wuxia film directed by Raymond Lee and produced by Tsui Hark, starring Brigitte Lin, Tony Leung Ka-fai, Maggie Cheung and Donnie Yen. It was released as Dragon Inn in North America.

The film is a remake of Dragon Gate Inn (1966). New Dragon Gate Inn was shot as a standard wuxia action thriller, with fast-paced action including martial arts, sword fighting and black comedy set in ancient China.

== Plot ==
This is a period film set during the Ming Dynasty in the desert region of China.

Tsao Siu-yan is a power-crazed eunuch who rules his sector of China as if he were the Emperor and not a mere official. He is the leader of the security agency of the Emperor, this ruthless body is known as the Dong Chang (東廠; East Factory). He has built up an elite army of skilled archers and horsemen who receive intensive training and powerful weapons.

When elements of his administration plot against him and his despotic rule, Tsao comes down ruthlessly. One such plotter is defence minister Yang Yu-xuan, who is executed along with his family. Tsao does spare two of the younger children and instead sentences them to exile in order to lure Yangs subordinate general Chow Wai-on into a trap.

Escorted by a couple of rather poorly East Factory soldiers, the children are sent out into the desert. Rebels, led by Chows lover, swordswoman Yau Mo-yan, arrive to free them, but are attacked by East Factory troops. Tsao later calls off the attack when he realises that Chow is not among the fighters, and instead orders his troops to pursue them to find where they will be meeting with Chow. The rebels and the children then proceed to the Dragon Gate Pass through which they will cross the border.

They reach the Dragon Gate Inn, which is a meeting place run by brigands. The innkeeper, the lively Jade, runs a sideline in which she seduces and murders her guests. Jade also keeps whatever money the customer has, then drops them down a chute to the kitchen and has them served as the meat in buns. The cutting up is done by her cook Dao, an expert at stripping meat to the bones.

Mo-yan and her followers arrive at the inn. She is disguised as a man, but Jade is not fooled, claiming that only a woman would pass her without so much as a glance. That night she confronts Mo-yan and the pair engage in a lively acrobatic fight with both women trying to remain clothed, while stripping the other.

Rebel leader Chow arrives and is re-united with Mo-yan. They plan to cross the border with the children but the bad weather delays their departure. Furthermore Jade takes a liking to Chow and resolves to get him for herself, also has in mind the reward offered for his capture. Things are made even more complicated when East Factory officials led by Cha arrive at the inn posing as merchants.

The scene is set for a vicious battle of bodies and wits between both sides, with Jade trying to keep the peace and getting every advantage, monetary or otherwise, that she can get out of it. Meanwhile, the bulk of the East Factory forces, led by eunuch Tsao himself, are on their way to the inn.

Chow believes that, like most den of thieves, the inn has a secret passage through which his comrades can escape. Jade agrees to show them the passage if Chow will sleep with her. He agrees if they marry first. Jade, a practical girl, is rather surprised at having to marry for a one-night stand but proceeds anyway, with Cha acting as host for the wedding. The heart-broken Mo-yan drowns her sorrows in drink.

The growing tension inside the inn breaks out into open battle when Cha and his men realise that the rebels want to use the secret passage to escape. The fight that follows results in the deaths of all the Dong Chang at the inn and most of the rebels and brigand hosts. Mo-yan herself is seriously injured.

Tsao and his army arrives and lays siege to the inn. Inside there is only a handful of survivors: Jade, Chow, Mo-yan, Dao the cook, and the children. They escape through the passage, but a loose ribbon gives them away and Tsao himself sets off in pursuit.

A vicious one-on-three battle amidst a desert storm as Tsao fights Jade, Chow and Mo-yan. Weakened by her wounds, Mo-yan perishes in quicksand. Just as Tsao is about to finish off Jade and Chow, Dao suddenly appears and takes him on with his carving knife. He carves away at Tsao, leaving the warlord with a skeletal arm and leg. Chow then moves in for the kill and Tsao is finished.

Chow and the children make their way to the border. Realising how much Chow meant to her, Jade and Dao decide to follow Chow after burning down the infamous inn.

==Cast==
*Brigitte Lin  as Yau Mo-yan (邱莫言)
*Tony Leung Ka-fai  as Chow Wai-on (周淮安)
*Maggie Cheung as Jade (金鑲玉)
*Donnie Yen as Tsao Siu-yan (曹少欽)
*Lau Shun as Cha (查捕頭)
*Yuen Cheung-yan as Iron
*Hung Yan-yan as Ngai
*Yen Shi-kwan as Ho Fu
*Lawrence Ng as Siu-chuen
*Ngai Chung-wai as Dao (阿刀)
*Yuen Bun
*Chan Chi-fai
*Elvis Tsui
*Wong Wai-shun
*Cheng Lit-king
*Choi Ho

==Themes==
This was one of a number of films in which Brigitte Lin is a woman passing off as a man. Others included Peking Opera Blues and Swordsman II.

==DVD release date== Region 2.
 The Swordsman and Moon Warriors.
 Iron Monkey.

==External links==
* 
* 
* 

 

 
 
 
 
 
 