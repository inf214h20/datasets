Cold Heart
 
{{Infobox Film 
 | name = Cold Heart
 | image = Cold heart dvd cover.jpg
 | size = 220
 | caption = Cold Heart DVD cover 
 | director = Dennis Dimster 
 | writer = Zvia Dimbort Dennis Dimster
 | starring = Nastassja Kinski Jeff Fahey Josh Holloway 
 | distributor = Filmexperiment Company 
 | released = 10 February 2001 (USA) 
 | runtime = 95 min. English
 | country =  
 | }}
Cold Heart is a 2001 thriller genre film starring Nastassja Kinski and Jeff Fahey. The film conveys the atmosphere of conspiracy, the essence of which becomes clear to an innocent victim at the very last moment.

==Plot==
Dangerous psychopath Sean Clark tries to kill a woman and gets into a prison psychiatric hospital. Thanks to the efforts of a talented psychiatrist Philip Davis, Clark is released prematurely. Davis explains that he acts on behalf of Seans father but mentions that his father does not want to meet or communicate with Sean. Davis gives Clark money and takes him to his new home, reminding that the main condition for the Seans release is his weekly visits to Davis.

After that, Sean starts a new life. He gets a job with a large company, the owner of which turns out to be Daviss wife, Linda, who falls in love with Sean. In turn, Sean performs her every wish and even says to Davis that he loves his new employer.

Meanwhile, Linda suspects that her husband has an extramarrital affair. Secretive phone calls, leavings for conferences (which he does not attend)—all of this convince her that she is right. One day, Linda plans to attend the presentation of her film. Her assistant, who was to accompany her, proposes to replace her with Sean. On a trip, a whirlwind romance sparks between Linda and Sean. Blinded by passion, Linda cannot suspect that all the events are the parts of her husbands insidious plan. He wants to profit from her death and chooses the most reliable weapon—a convicted maniac, whose guilt no one will doubt.

==Cast==
*Nastassja Kinski as Linda Cross 
*Jeff Fahey as  Phil 
*Josh Holloway as Sean 
*Hudson Leick as Julia  
*Janne Campbell as  Natalie 
*Lincoln Myerson as  Mr. Peterson 
*Bob Sattler as Detective Harris

== External links ==
*  

 
 
 
 
 


 