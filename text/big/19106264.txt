The Gamma People
{{Infobox film
| name           = The Gamma People
| image          = The Gamma People movie poster.png
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = John Gilling
| producer       = John Gossage 
| screenplay     = John Gilling John Gossage
| story          = Robert Aldrich Louis Pollock Paul Douglas Martin Miller Philip Leaver
| music          = George Melachrino
| cinematography = Ted Moore
| editing        = Jack Slade
| studio         = Warwick Films
| distributor    = Columbia Pictures
| released       = 30 January 1956 (United Kingdom: general release) 
 
| runtime        = 76 or 78 minutes 
| country        = United Kingdom United States 
| language       = English
| budget         =
| gross          =
}} Paul Douglas, Eva Bartok and Leslie Phillips. 

==Plot==
A passenger car carrying a reporter and his photographer mysteriously breaks away from their train, accidentally ending up on a side track in Gudavia, an isolated Ruritanian romance|Ruritanian-style one-village Eastern Bloc dictatorship. The newsmen find a mad scientist using gamma rays to turn the countrys youth into either geniuses or subhumans at the bidding of an equally mad dictator.

==Cast==
   Paul Douglas as Mike Wilson 
* Eva Bartok as Paula Wendt 
* Leslie Phillips as Howard Meade
* Walter Rilla as Boronski 
* Philip Leaver as Koerner   Martin Miller as Lochner 
* Michael Caridia as Hugo Wendt  
 
* Pauline Drewett as Hedda Lochner  
* Jocelyn Lane as Anna  
* Olaf Pooley as Bikstein  
* Rosalie Crutchley as Frau Bikstein  
* Leonard Sachs as Telegraph Clerk  
* Paul Hardtmuth as Hans
* Cyril Chamberlain as Graf
 

==References==
{{Reflist|refs=
   
   
   
}}

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 