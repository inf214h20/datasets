The Fighting Dervishes of the Desert
{{infobox film name = The Fighting Dervishes of the Desert image = Fighting_Wiki.jpg caption = Adertising published in The Moving Picture World, Vol 12, p 545 director = Sidney Olcott producer = Kalem Company writer =  starring = Jack J. Clark Gene Gauntier distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 1028 ft
| country = United States language = Silent film (English intertitles) 
}}

The Fighting Dervishes of the Desert is a 1912 American silent film produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott with himself, Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier - Zahrah
* Jack J. Clark - Hassan Ali
* Robert Vignola - Ishmail
* J.P. McGowan - Father Moosa
* Allan Farnham

==Production notes==
The film was shot in Luxor, Egypt.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 


 
 