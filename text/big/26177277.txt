Nobody's Baby (2001 film)
{{Infobox film
| name           = Nobodys Baby
| image          = Nobodysbaby.jpg
| image_size     =
| caption        =
| director       = David Seltzer
| writer         = David Seltzer
| narrator       =
| starring       = Skeet Ulrich Gary Oldman Radha Mitchell Mary Steenburgen
| music          =
| cinematography = Christopher Taylor
| editing        = Hughes Winborne SE8 Group Front Street Pictures
| distributor    = Artisan Entertainment
| released       =  
| runtime        = 112 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Nobodys Baby is a 2001 comedy film written and directed by David Seltzer and starring Gary Oldman and Skeet Ulrich.

==Plot==
In this comedy, Billy Raedeen (Skeet Ulrich) escapes the law after being convicted with his partner in crime Buford Bill (Gary Oldman). On his way to Utah, Billy rescues a baby from an auto wreck and decides to keep it though he knows next to nothing about caring for an infant. He gets help from diner waitress Shauna Louise (Radha Mitchell) and her neighbor Estelle (Mary Steenburgen). When Buford tracks Billy down, he sees the baby as a monetary  potential. However, Billy and Shauna Louise have grown attached to the child and they arent willing to give her up.

==Cast==
* Skeet Ulrich as Billy Raedeen
* Gary Oldman as Buford Bill
* Radha Mitchell as Shauna Louise
* Mary Steenburgen as Estelle
* Gordon Tootoosis as Dog Havasu
* Anna Gunn as Stormy
* Peter Greene as Vern
* Matthew Modine as Sonny
* Ed ONeill as guy in car dealership

==Release==
Following its premiere at the 2001 Sundance Film Festival, the film was not released theatrically.  It received a USA home video release on August 20, 2002. 

==Reception==
Geoffrey Gilmore, director of the Sundance Film Festival, called the film "a pure delight from start to finish":  Oldman play Something about Mary, Nobodys Baby is a wonderfully entertaining odyssey that should bring Seltzer nothing but accolades.

Variety (magazine)|Variety, reviewing the film after its January 21, 2001 premiere at Sundance, described it as "aim  somewhere between Dumb and Dumber and Three Men and a Baby.  The films "witless script wrings few laughs from its retread conceits...Whats most toxic, however, is having to watch these actors sweat for their paychecks. Oldman vanishes into mutton chops and Walter Brennan mannerisms, gamely making an idiot of himself, to absolutely no humorous result....Steenburgen, Greene and ONeill are allowed to be little more than unpleasant; Matthew Modine surfaces in a nothing role.  Twinkling amidst the cow pies, Ulrich clearly relishes playing dum-dum, and his disarming sweetness lends the film whatever fleeting conviction it can claim." 

==References==
 

==External links==
* 
*  at Rotten Tomatoes

 

 
 
 
 
 
 