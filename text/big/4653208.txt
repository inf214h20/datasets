Sgt. Kabukiman N.Y.P.D.
 
{{Infobox Film 
|  name           = Sgt. Kabukiman N.Y.P.D.
|  image          = Sgt kabukiman nypd poster.jpg
|  caption        = Poster Michael Herz Michael Herz   Lloyd Kaufman   Tetsu Fujimura
|  writer         = Lloyd Kaufman Andrew Osborne   Jeffery W. Sass
|  starring       = Rick Gianasi Susan Byun Bill Weeden
|  music          = 
Bill Mithoff|  cinematography = Robert Paone
|  editing        = Peter Novak   Ian Slater
|  studio         = Namco Limited   Gaga Communications.inc
|  distributor    = Troma Entertainment 
|  released       = 1990 (United States|USA) 
|  runtime        = 105 min.  USA
| English 
|  budget         = 
|}} 1990 comedic Michael Herz and distributed by Troma Entertainment.

==Synopsis==
The film follows Sergeant Detective Harry Griswold (Rick Gianasi), a clumsy N.Y.P.D. cop investigating a string of murders involving Kabuki actors. While attending an amateur Kabuki play, Harry witnesses thugs gun down the entire cast. In the ensuing gunfight, Harry is forcefully kissed by one of the dying actors, unknowingly becoming blessed with the powers of Kabuki. Before he knows it, Griswold finds out that he has the ability to transform into Kabukiman, a colorfully dressed slapstick superhero who has the ability to fly and access to such unique weapons as heat seeking chopsticks and fatal sushi.

With the assistance of the beautiful Lotus (Susan Byun), he helps clean up the crime-ridden streets of New York and try to stop maniacal businessman Reginald Stuart (Bill Weeden) and his Goons, who plan to fulfill an ancient evil prophecy that will summon The Evil One which demonic powers and enslave the world.

==Production== Masaya Nakamura of Namco to create a Kabuki-themed superhero movie, supposedly based on an idea by Kaufman. Namco became a producer, giving Troma a one and a half million dollar budget to begin preproduction - the most expensive film in Tromas history.

Creative differences troubled production from the start - both Namco and Herz wanted a mainstream-accessible film geared towards children, whereas Kaufman wanted the usual Troma-esque sex and violence style. Kaufman had his way and Kabukiman was turned into an R-rated comedy in the vein of Tromas previous films. A PG-13 cut was shown to the films investors who, thoroughly dissatisfied, withdrew their distribution deals.  Although Kaufman screened Kabukiman at the Cannes Film Festival for several years, the movie did not see theatrical distribution until 1996, when the PG-13 cut was exhibited. 

Although Kabukiman received some positive reviews from both The New York Times and The New York Post (whose quotes are prominently featured on the video package), most Troma fans have mixed feelings towards the film.  Though some consider it a classic to rival the original Toxic Avenger, most find it a slow-paced, uneven mix between family-friendly fare and Troma-esque sex and violence.  The film was also reviewed by Siskel & Ebert - despite giving it a thumbs down, Ebert took a liking toward the film and compared it favorably to another independent film at the time, Switchblade Sisters.

===The car flip===
Sgt. Kabukiman N.Y.P.D. is perhaps best known for its car chase scene that happens midway through the film where several carloads of gangsters chase Harry Griswold, wearing a clown costume, through the streets of Jersey City, which climaxes when one of the cars, a blue sedan, strikes another vehicle, flips upside-down 30 feet in the air, lands, and then inexplicably explodes. Five years later, exactly the same footage was used in a scene in Tromeo and Juliet for not only being cost-effective, but also because Kabukiman had yet to be widely distributed on video (and thus brought some confusion as to which movie the footage originated from).

Despite obvious continuity flaws, Troma has managed to fit in the same footage into each of their films as a tongue-in-cheek homage, including  , Poultrygeist, and Return to Nuke Em High Vol.1.

==Later appearances of the character== James Gunn.

Plans for a Sgt. Kabukiman N.Y.P.D. animated series also went into the works, however the series never went to production. There had only been an animated teaser that was completed, it has since been made available as a bonus feature on the Sgt. Kabukiman N.Y.P.D. DVD. The cartoon was to feature the Kabukiman character, and a number of Japanese themed super-heroes fighting crime in New York, with similar parallels to Tromas other animated spin-off, Toxic Crusaders.

The character of Kabukiman made the return to the silver screen in 2001s  , where he was once again played by Paul Kyrmse. In the film, Kabukiman has gone "from a serious superhero to a pathetic, drunken has-been who is looked upon with disdain by the citizens of Tromaville", states film critic Chris Gore. This change in character persona is attributed to fan backlash from the original film. Kabukiman is also portrayed as "Evil Kabukiman" in an alternate universe: a less wacky, more threatening villain.

Since 2000, there have been rumors that a sequel, Sgt. Kabukiman L.A.P.D., would be made, but as of 2010, there are no plans to revive the series.   A sequel/spin-off called Sgt. Kabukiman and the Lesbians of Bonejack High started production in early 2006, but was ultimately never finished.

Kabukiman makes an appearance in the upcoming  Return to the Class of Nuke Em High alongside the Toxic Avenger in a party scene as a tribute to his similar cameo in Tromeo & Juliet from 1996.

Creator Lloyd Kaufman has revealed that Kabukiman will have a large role in the upcoming Toxic Twins: The Toxic Avenger V. 

Kabukiman appeared in the "We Are All Made of Stars" music video by Moby in 2002 with The Toxic Avenger, signing an autograph for Leelee Sobieski.

== External links ==
* 
*  – at the Troma Entertainment movie database
*  

 
 

 
 
 
 
 
 
 
 
 