Seven Days in January
{{Infobox film
| name           = Seven Days in January
| image          = 
| caption        = 
| director       = Juan Antonio Bardem
| producer       = Roberto Bodegas
| writer         = Gregorio Morán Juan Antonio Bardem
| starring       = Manuel Angel Egea
| music          = 
| cinematography = Leopoldo Villaseñor
| editing        = 
| distributor    = 
| released       =  
| runtime        = 180 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Seven Days in January ( ) is a 1979 Spanish drama film directed by Juan Antonio Bardem about the 1977 Massacre of Atocha. It was entered into the 11th Moscow International Film Festival where it won the Golden Prize.   

==Cast==
* Manuel Angel Egea as Luis Maria Hernando de Cabral
* Madeleine Robinson as Adelaïda
* Virginia Mataix as Pilar
* Jacques François as Don Thomas
* Alberto Alonso as Cisko Kid (as Alberto Alonso Lopez)

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 