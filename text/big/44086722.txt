Vedikkettu
{{Infobox film 
| name           = Vedikkettu
| image          =
| caption        =
| director       = KA Sivadas
| producer       = Thevannur Maniraj Santha Gopinathan Nair
| writer         = Thevannur Maniraj TV Gopalakrishnan (dialogues)
| screenplay     = TV Gopalakrishnan
| starring       = Sukumaran Jalaja Prameela Ramesh
| music          = M. K. Arjunan
| cinematography = GV Suresh
| editing        = K Sankunni
| studio         = Masi Pictures
| distributor    = Masi Pictures
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by KA Sivadas and produced by Thevannur Maniraj and Santha Gopinathan Nair. The film stars Sukumaran, Jalaja, Prameela and Ramesh in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Sukumaran
*Jalaja
*Prameela
*Ramesh
*Alummoodan GK Pillai
*Kaduvakulam Antony
*Kottarakkara Sreedharan Nair
*Kuthiravattam Pappu
*Nellikode Bhaskaran
*Stanley
*Vettoor Purushan
*Vijaya Kumari
*R Balakrishna Pillai
*Kollam GK Pillai
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by . 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maanam Irundu || K. J. Yesudas ||  || 
|-
| 2 || Paalaruvikkarayile || Vani Jairam ||  || 
|}

==References==
 

==External links==
*  

 
 
 

 