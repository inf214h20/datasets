Yoyes
{{Infobox film
| name           = Yoyes
| image          = Yoyes (movie poster).jpg
| image size     = 210px
| caption        = Spanish film poster
| director       = Helena Taberna
| producer       = Enrique Cerezo	
| writer         = Helena Taberna  Andrés Martorell
| narrator       =
| starring       = Ana Torrent   Ernesto Alterio  Florence Pernell
| music          = Ángel Illarramendi
| cinematography = Federico Ribes
| editing        = Rosario Sáinz de Rozas
| distributor    =
| released       = 31 March 2000 Spain
| runtime        = 104 minutes
| country        =     France   Italy Spanish
| budget         =
}} 2000 Spain|Spanish Basque Country.

==Synopsis ==
In the early 1970s, during the last years of Francisco Franco|Francos dictatorship, ETA was a group of Basque using political violence in Spain under the leadership of Argi. Yoyes, a young independent woman, joins ETA in Ordizia, a small town in the Basque Country. She quickly plays an important role in ETAs organization. Yoyes meets Joxean, a philosophy student and they start a relationship.

Eventually, Yoyes finds herself losing faith in ETAs cause. Once the group begins to kill civilians, she decides to leave after Argi dies during a bombing. Dropping out of the group, Yoyes goes underground, and moves to Mexico, where she goes back to college and receives a Ph.D.

12 years later, Yoyes is in Paris, where her husband Joxean gets a teaching position with a French University. When Joxean decides to visit the Basque regions with their small daughter Zuriñe, Yoyes decides to join them, knowing it puts her at great personal risk.

When a newspaper prominently publishes the news of Yoyes’s return, her former ETA colleagues mistakenly believe that her presence there means that she has turned informant in exchange for permission to return home. They dont want her example to spread and more people to leave the group. Yoyes has tried to forget the past and be forgotten, wanting just to blend back into society. In the autumn, her former comrades have condemned Yoyes to death. While at a local fair in Ordizia, Yoyes is shot in the head in front of her young daughter.

==Cast==	
*Ana Torrent-  Yoyes
*Ernesto Alterio - Joxean
*Florence Pernell - Hélène
*Ramón Langa - Koldo
*Iñaki Aierra - Argi
*Isabel Ordaz - Ana
*Ruth Nuñez - Bego
*Martxelo Rubio - Kizkur
*Gonzalo Gonzalo – Zaldu

==Awards==
*Toulouse Cinespaña: best Actor, best Actress, Special Jury Prize,  2000
*Festival de Cine de Cartagena: Best First workr Opera Prima, 2001
*Festival de Cine de Gramado: Best Latin Film, 2001

== External links ==
*  
* http://www.yoyes.com/english/comohizo.html

 
 
 
 
 