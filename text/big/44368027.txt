Beautiful Dreamer: Brian Wilson and the Story of Smile
{{Infobox film
| name           = Beautiful Dreamer: Brian Wilson and the Story of Smile
| image          = Beautiful Dreamer Smile.jpg
| alt            =
| caption        = Promotional DVD cover
| film name      = 
| director       = David Leaf
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =       
| distributor    = 
| released       =   
| runtime        = 109 minutes
| country        = 
| language       = English
| budget         = 
| gross          =  
}}

Beautiful Dreamer: Brian Wilson and the Story of Smile is a 2004 documentary film directed by David Leaf about American musician Brian Wilson, the Beach Boys unfinished Smile (The Beach Boys album)|Smile album, and the making of Wilsons Brian Wilson Presents Smile. Besides featuring interviews from Wilson himself, the film includes contributions by Elvis Costello, Burt Bacharach, Paul McCartney, and Roger Daltrey.    The title takes its name from the 19th century Stephen Foster song "Beautiful Dreamer"; its opening lyrics begin with the phrase "beautiful dreamer wake", the same initials as the documentarys subject, Brian Douglas Wilson, who references this coincidence in the film.   

==Summary==
The film is divided in three segments: a history of the Beach Boys original Smile (The Beach Boys album)|Smile album, its rebirth as the Brian Wilson solo album Brian Wilson Presents Smile, and the albums 2004 live performances.  

==Release== Showtime  before being bundled as an extra on the DVD for the 2005 concert film Brian Wilson Presents Smile.

==Cast==
{{columns-list|colwidth=20em|
*Brian Wilson
*Van Dyke Parks
*Melinda Ledbetter-Wilson
*Danny Hutton
*David Anderle
*Michael Vosse
*Lorren Daro
*Darian Sahanaja
*David Oppenheim (archival)
*Tony Asher
*Carol Kaye
*Hal Blaine
*Elvis Costello
*Burt Bacharach
*Paul McCartney
*Roger Daltrey
}}

==External links==
*  

==References==
 

 
 

 
 
 
 
 
 
 
 
 
 
 

 