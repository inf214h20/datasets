Putham Pudhu Payanam
{{Infobox film
| name           = Putham Pudhu Payanam
| image          = 
| image_size     = 
| caption        = 
| director       = K. S. Ravikumar
| producer       = R. B. Choudary
| writer         = Erode Soundar  (dialogues) 
| screenplay     = K. S. Ravikumar
| story          = Erode Soundar
| starring       =  
| music          = Soundaryan
| cinematography = Ashok Rajan
| editing        = K. Thanikachalam
| distributor    = Super Good Films
| released       = 22 November 1991
| runtime        = 120 minutes
| country        = India
| language       = Tamil
}}
 1991 Tamil Sivaji and Shiju.

==Plot==

Vivek  (Vivek (actor)|Vivek), Narayanan (Chinni Jayanth) and Kannan (Supergood Kannan) are admitted in a hospital for treatment. Babu (Anand Babu), son of a rich businessman, is also admitted in the same ward as them. They become friends in a short time. They finally come to know that they will live only for few more months, they all have blood cancer. They decide to leave the hospital and to travel. They arrive in a small village where Sivalingam (K. S. Ravikumar) spreads terror among the villagers.

==Cast==

*Anand Babu as Babu Vivek as Vivek
*Chinni Jayanth as Narayanan
*Supergood Kannan as Kannan
*K. S. Ravikumar as Sivalingam Chithra
*Sukumari Sulakshana as Sivalingams wife
* Loose Mohan as Loose
*Kumarimuthu as Arumugam Nagesh as a doctor

==Soundtrack==

{{Infobox album |  
| Name         = Putham Pudhu Payanam
|  Type        = soundtrack
|  Artist      = Soundaryan
|  Cover       = 
|  Released    = 1991
|  Recorded    = 1991 Feature film soundtrack
|  Length      = 
|  Label       = Lahari Music
|  Producer    = Soundaryan
|  Reviews     = 
}}

The film score and the soundtrack were composed by film composer Soundaryan. The soundtrack, released in 1991, features tracks with lyrics written by Soundaryan. 

==References==
 

 

 
 
 
 
 