Annakili
{{Infobox film
| name           = Annakili
| image          = Annakili.jpg
| image size     =
| caption        = LP Vinyl Records Cover
| director       = Devaraj Mohan
| producer       = P. Thamizharasi
| writer         = Panju Arunachalam
| narrator       = Sujatha
| music          = Ilaiyaraaja
| cinematography = A. Somasundaram
| editing        = B. Kandasamy
| studio         = S. P. T. Films
| distributor    = 
| released       = 14 May 1976
| runtime        =
| country        = India Tamil
| budget         =
| gross          = Rs. 1.5 crore
| preceded by    =
| followed by    =
}}
Annakili is a 1976 Tamil language film. It was directed by Devaraj Mohan. It was also notable for being Isaignani Ilaiyaraajas first film.    The soundtrack to the film was a major success.       The film reportedly had a "South Indian rural theme", shot in a village Thengumarahada      and like later films, many of Ilayarajas songs "fused Tamil folk music with Western classical and South Indian Carnatic music."   

The film was a blockbuster and completed a 196-day run at the box office.

==Cast==
*Sivakumar Sujatha
*Phataphat Jayalaxmi
*S. V.Subbaiah
*Sreekanth
*Thengai Srinivasan
*Goundamani

==Soundtrack==

{{Infobox album Name     = Annakili Type     = film Cover    = annakkiliaudio.jpg Released =  Artist    = Illayaraja Genre  Feature film soundtrack Length   = 23:54 Label    = EMI The Gramaphone Company of India Limited
}}

* Annakili Unnai - S.Janaki
* Machaana Pathingala - S.Janaki
* Muththu Muththa(Adi Raakayee) - S. Janaki
* Annakili (M) -TM Soundarajan
* Sontham Illai - P Susheela

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 