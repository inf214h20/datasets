Count Yorga, Vampire
{{Infobox Film
| name           = Count Yorga, Vampire 
| image          = Countyorgaposter.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Bob Kelljan
| producer       = Michael Macready
| writer         = Bob Kelljan Michael Murphy Michael Macready Donna Anders
| music          = Bill Marx
| editing        = Tony de Zarraga
| cinematography = Arch Archambault
| studio         = Erica Productions Inc.
| distributor    = American International Pictures
| released       = June 10, 1970
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} vampire horror film directed by Bob Kelljan and starring Robert Quarry. It was followed by a sequel, The Return of Count Yorga. Count Yorga... is arguably the first modern film adaptation of a vampire character as living in the 20th century.

== Plot ==
The scenario opens with narration by character actor George Macready (whose son Michael produced the film) about superstition and the abilities of vampires.  A truck is loaded at the Port of Los Angeles, and as it climbs to a gated mansion in the California hills, the cargo is revealed to be a coffin.
 hysterical during the proceedings, and Yorga uses hypnosis to calm her. After the party is over, Erica and her boyfriend Paul offer to drive the Count home.  Experiencing car trouble outside of Yorgas mansion (though Paul notices the road was dry a minute ago), the two resign themselves to spend the night in their van. Yorga watches the couple Sexual intercourse|copulate, then attacks them, revealing himself to be a vampire. The following day, Paul tells Michael, Donnas boyfriend, about the attack.  Paul didnt see their attacker, and Erica doesnt remember the attack at all.

Erica visits Dr. Hayes to have the mysterious bite wounds on her neck inspected. In contrast to her exuberant personality on the night before, Erica now seems despondent and listless. Hayes notices she has lost a lot of blood.  Unable to diagnose the cause, he recommends rest and a high protein diet. Paul and Michael discuss the strange changes in Ericas behavior. Unable to reach her by phone, the concerned men drive to her home. They find the place in disarray, and an hysterical Erica eating her pet kitten. She reacts erratically to their presence, first threatening them with violence and then attempting to seduce Paul before coming to her senses and breaking down. They restrain her and call Dr. Hayes, who begins an emergency  s. It is revealed here that one of the brides is Donnas mother, meaning that Yorga was the cause of her death and she is now his undead-servant. He awakens two of them and watches as they have sex, presumably using his powers of mind-control to force them to do so.

Although Michael is skeptical, the three men consider the possibility of vampirism as an excuse for Ericas behavior. That night, Yorga visits Erica while Paul sleeps downstairs. Promising her immortality, he drains Erica of blood and completes her transformation before taking her back to his manor to add Erica as his third vampire bride. Paul, upon finding Erica missing, rashly goes to Yorgas mansion to rescue her. Yorga easily kills him with help of his deformed servant, Brudah. Michael alerts Hayes that Paul has gone to the mansion, and Hayes confides that Pauls lack of preparation will probably lead to his death. Hayes considers his girlfriends advice, citing a baby being found drained of its blood with bite wounds on the neck, to involve the police. But he is rejected as a deluded prankster following a recent rash of such calls. Hayes, Michael and Donna go to the mansion themselves to inquire about Pauls whereabouts and keep Yorga active until sunrise. While Hayes distracts Yorga with enthusiastic questions about Yorgas occult experiments, Brudah rebuffs Michaels attempts to explore the mansion.  Michael and Hayes switch places to keep Yorga off his guard, but Yorga becomes increasingly insistent that it is late and his guests must leave. Yorga distracts Hayes, and Yorga strengthens his hypnotic control over Donna.
 telepathically and summons her back to the mansion. On her arrival, Brudah rapes her. Michael oversleeps due to Donnas sabotage of the alarm clock, and its nearly evening when he calls to awaken Hayes. They stock up on stakes and makeshift crosses before heading to Yorgas mansion as night falls. They split up, and Hayes is confronted by Yorga. Both drop the pretense that Yorga is anything but a vampire, and Yorga leads Hayes into his basement where his vampire-brides lie dormant. Hayes finds Ericas body but can find no pulse or heartbeat on her. He attacks Yorga with cross and stake, while yelling out for Michael (who hears Hayes and begins to run in the direction of his call). Yorga is irritated by Hayes cross and taunts the doctor as he silently commands his brides to awaken. With Heyes back to the approaching brides (and Hayes attention fixed on Yorga), the brides attack and drain the helpless Hayes.

Yorga reunites Donna with her mother. Michael finds Pauls mutilated body while navigating the crypt. Brudah attacks him, but Michael stabs him—presumably to death. Michael manages to reach the throne room but find Hayes as he lays dying from bites wounds and blood loss, though with his last breath, Hayes tells Michael where Donna is. However the vampiric-Erica and an unnamed, red-headed vampire charge into the room no sooner then Hayes tells him intent on killing the last intruder. Michael fends them off, chasing away the red-headed while Erica oddly pauses, giving Micheal a chance to stake her. Despite seeing that shes no longer the Erica he knows, Michael cant bring himself to kill her and proceeds upstairs while she hisses at him and presumably finishes devouring Hayess body.

On the way to the staircase, Bruddah emerges from the living room, holding his profusely bleeding knife wound, but still intent on attacking Michael. Michael—somewhat stunned that Bruddah still lives—moves up the staircase as Bruddah reaches out for him, but Bruddah collapses—finally dying. Upstairs, Michael confronts Yorga and Donnas mother. Yorga pushes Donnas mother into Michaels stake and flees out of the room. Michael follows and Yorga ambushes him outside the room. Michael rams the charging Yorga with his stake, killing him. Donna mourns her mother a second time before Michael collects her. He and Donna watch Yorga turn to dust.

As they start to leave, they are confronted by Erica and the red-headed bride who, despite Yorgas death, still remain under the vampire-curse and continue to come after them, likely to kill Michael and reclaim Donna. They chase the two downstairs until repelled by Michaels cross. As the vampire women are forced back and toward a cellar, Erica glances ominously at Donna. Michael locks them in and takes Donnas hand, believing the danger is over. However, as he turns to leave, Donna hisses and lunges at him, fangs bared, fully transformed into a vampire. He was too late to prevent Yorga from turning her.

In a final line of voice-over, the narrator sarcastically disputes that vampirism is just superstition as he laughs evilly. The film ends on a shot of Michaels bloodied and lifeless corpse.

== Production ==

=== Origin of the film ===
The film was originally to have been a soft core porn film called The Loves of Count Iorga, and some prints of the film display this as the on-screen title.  Actor Robert Quarry told Michael MacReady he would play the vampire role if they turned the story into a straight horror film.   Marsha Jordan, the actress who played Donnas mother, had previously starred in such fare as Marsha, the Erotic Housewife.

=== Difficulties with the MPAA === rating (formerly known as M, later renamed to PG) from the Motion Picture Association of America, which initially was divided as to whether to give the film an R or X rating.  AIP insisted that they needed an unrestricted GP rating for the film in order to get the film released into the largest possible number of theaters, most importantly drive-in theaters.  The film ended up going before the MPAA ratings board six times before being granted the GP rating, and two or three minutes of violent and sexual content were ultimately removed by AIP.  Alterations to the movies soundtrack were also required to lessen the impact of violent scenes that remained in the film.  The current MGM DVD release of the film carries a PG-13 rating, indicating the possibility that some of the deleted footage may have been restored.

The most obvious excision was the scene wherein a woman, having succumbed to bloodlust after having been bitten by Yorga, is discovered with her dead pet cat in her hands. In the theatrical version, the scene is so brief that it was hard to tell what was happening. Complete prints of the film show the bloody cat quite clearly.

== Legacy == street people, but it never materialised.

American International Pictures had planned at one stage to revive Count Yorga as an adversary for The Abominable Dr. Phibes|Dr. Anton Phibes in Dr. Phibes Rises Again. This plan was dropped, however, and Robert Quarry appeared as the artificially young Dr. Biederbeck.

Robert Quarry later played another vampire, the messianic Khorda in 1973s The Deathmaster, which is often confused with the Yorga films because AIP picked up the distribution rights and began using the term "The Deathmaster" to promote the Yorga sequel, The Return of Count Yorga.

== Home video release ==
Count Yorga, Vampire has been the subject of several home video releases in nearly all formats since the 1980s.

Of note, in April 1991, the film was packaged as a Laserdisc double feature, paired with the Vincent Price horror film Cry of the Banshee; both films were not letterboxed, but employed a full screen, pan-and-scan process.

In 2004, MGMs Midnite Movies DVD line (which redistributed much of the American International Pictures horror catalog previously owned by Orion Pictures Home Video) released Count Yorga, Vampire and its sequel, The Return of Count Yorga as a DVD double feature. Both films were presented in the widescreen format, and included original theatrical trailers.

==See also==
*Vampire film

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 