The Oregon Trail (1936 film)
 
{{Infobox film
| name           = The Oregon Trail 
| image          = Oregon trailposter.jpg
| caption        = Film poster
| director       = Scott Pembroke
| producer       = Trem Carr
| writer         = {{plainlist|
*Jack Natteford
*Lindsley Parsons
*Robert Emmett Tansey
}}
| starring       = {{plainlist|
*John Wayne
*Ann Rutherford
}}
| cinematography = Gus Peterson
| editing        = Carl Pierson
| released       =  
| runtime        = 59 minutes
| country        = United States
| language       = English
}} Western film starring John Wayne. The film is believed to be a lost film with no known prints remaining. In 2013 forty still photographs from the film were found by a collector. 

Wayne plays retired army captain John Delmont, who discovers from his fathers journal that he was left to die by a renegade, and vows to hunt down the killer.

==Cast==
*John Wayne as Capt John Delmont
*Ann Rutherford as Anne Ridgeley
*Joseph W. Girard as Col. Delmont 
*Yakima Canutt as Tom Richards Frank Rice as Red
*E. H. Calvert as Jim Ridgeley
*Ben Hendricks Jr. as Maj. Harris Harry Harvey as Tim
*Fern Emmett as Minnie Jack Rutherford as Benton
*Marian Ferrell as Sis
*Roland Ray as Markey
*Gino Corrado as Forrenza
*Edward LeSaint as Gen. Ferguson
*Octavio Giraud as Don Miguel

==See also==
* John Wayne filmography
* List of lost films

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 
 