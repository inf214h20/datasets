The Adorable Deceiver
 
 
{{Infobox film
| name           = The Adorable Deceiver
| image          =
| imagesize      =
| caption        =
| director       = Phil Rosen
| producer       =
| writer         = Harry O. Hoyt
| cinematography =
| editing        =
| distributor    = Robertson-Cole Pictures Corporation
| released       =  
| runtime        = 57 min.
| country        = United States
| language       = Silent English intertitles
}}

The Adorable Deceiver is a 1926 American silent film comedy, starring Alberta Vaughn as a princess, forced to flee her home country with her father King Nicholas to New York City, where they make their way as well-meaning con artists. 

== Cast ==
* Alberta Vaughn as Princess Sylvia
* Daniel Makarenko as King Nicholas
* Harland Tucker as Tom Pettibone
* Frank Leigh as Jim Doyle
* Jane Thomas (actor) as Flo Doyle
* Cora Williams as Mrs. Pettibone
* Rosa Gore as Mrs. Schrapp
* Sheila Hayward as Bellona

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 