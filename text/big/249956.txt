Rabbit-Proof Fence (film)
 
 
 
{{Infobox film
| name = Rabbit-Proof Fence
| image = Rabbit-Proof Fence movie poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Phillip Noyce John Winter
| screenplay = Christine Olsen
| based on =  
| starring = Everlyn Sampi Kenneth Branagh David Gulpilil
| music = Peter Gabriel
| cinematography = Christopher Doyle
| editing = Veronika Jenet John Scott
| studio = HanWay Films
| distributor = Miramax Films
| released =  
| runtime = 93 minutes  
| country = Australia
| language = Aboriginal English
| budget = United States dollar|USD$6 million
| gross = USD$16.2 million
}} Aboriginal girls, walk for nine weeks along   of the Australian rabbit-proof fence to return to their community at Jigalong Community, Western Australia|Jigalong, while being pursued by white law enforcement authorities and an Aboriginal tracker.   

The  , is by Peter Gabriel. British producer Jeremy Thomas, who has a long connection with Australia, was executive producer of the film, selling it internationally through his sales arm, HanWay Films.

==Plot==
In Western Australia in 1931, in the remote town of Jigalong, three children live with their mother and grandmother. They are two sisters, 14-year-old Molly, 8-year-old Daisy, and their 10-year-old cousin Gracie. The town lies along the northern part of Australias rabbit-proof fence, which runs for several thousand miles.

Thousands of miles away, the "protector" of Western Australian Aborigines,  , in the south.
  showing the trip from Moore River to Jigalong.]]
During their time at the camp, Molly notices a rain cloud in the sky and deduces that if she, Gracie and Daisy were to escape and go back to Jigalong on foot, the rain will cover their tracks, so nobody can track them. Gracie and Daisy decide to go along with Molly and the three girls sneak off, without being noticed and run away. Moments after their escape, an Aboriginal tracker, Moodoo, is called in to find them. However, the girls are well trained in disguising their tracks. They evade Moodoo several times, receiving aid from strangers in the harsh Australian country they travel. They eventually find the rabbit-proof fence, knowing they can follow it north to Jigalong. Neville soon figures out their strategy and sends Moodoo and Riggs after them. Although he is an experienced tracker, Moodoo is unable to find them.

Neville spreads word that Gracies mother is waiting for her in the town of Wiluna, Western Australia|Wiluna. The information finds its way to an Aboriginal traveller who "helps" the girls. He tells Gracie about her mother and says they can get to Wiluna by train, causing her to break off from the group and attempt to catch a train to Wiluna. Molly and Daisy soon walk after her and find her at a train station. They are not reunited, however, as Riggs appears and Gracie is recaptured. The betrayal is revealed by Riggs, who tells the man he will receive a shilling for his help. Knowing they are powerless to aid her, Molly and Daisy continue on. In the end, after a harsh long journey, the two sisters make it home and go into hiding in the desert with their mother and grandmother. Meanwhile, Neville realizes he can no longer afford the search for Molly and Daisy and decides to suspend the pursuit.

===Epilogue===
The films epilogue shows recent footage of Molly and Daisy. Molly explains that Gracie has died and she never returned to Jigalong. Molly also tells us of her own two daughters; she and they were taken from Jigalong back to Moore River. She managed to escape with one daughter, Annabelle, and once again, she walked the length of the fence back home. However, when Annabelle was 3 years old, she was taken away once more, and Molly never saw her again. In closing, Molly says that she and Daisy "... are never going back to that place".

==Cast==
* Everlyn Sampi as Molly Craig
* Tianna Sansbury as Daisy Craig Kadibill
* Laura Monaghan as Gracie Fields
* David Gulpilil as Moodoo the Tracker Jason Clarke as Constable Riggs
* Kenneth Branagh as A. O. Neville
* Ningali Lawford as Maude, Mollys mother
* Myarn Lawford as Mollys grandmother
* Deborah Mailman as Mavis
* Garry McDonald as Mr Neal

==Production== film is adapted from the book Follow the Rabbit-Proof Fence, by Doris Pilkington Garimara, which is the second book of her trilogy documenting her familys stories.   

==Release==
The film stirred debate over the historical accuracy of the claims of the Stolen Generation.    Andrew Bolt,        a conservative journalist who has frequently attempted to downplay the facts of the "Stolen Generation", criticised Nevilles portrayal in the film, arguing that he was inaccurately represented as paternalistic and racist, and the films generally rosy portrayal of the girls situation prior to their removal from their parents.  Bolt questioned the artistic portrayal in the film of the girls as prisoners in prison garb. He claimed they would have been dressed in European clothes, as shown in contemporary photos, and says they were tracked by concerned adults fearful for their welfare.  He claimed that when Molly Craig saw the film, which portrayed her journey, she stated that it was "not my story". However, she clarified that statement by saying her story continued into her adult life and was not nicely resolved, as the films ending made it appear.   

==Reception==

===Critical response===
The film received positive reviews from critics. Rotten Tomatoes gave it a rating of 88% based on 138 reviews, with an average rating of 7.6 out of 10. The sites consensus states, "Visually beautiful and well-acted, Rabbit-Proof Fence tells a compelling true-life story."  On Metacritic the film has a score of 80 out of 100, indicating "generally favorable reviews". 
  SBS awarded the film four stars out of five, commenting that Rabbit-Proof Fence is a "bold and timely film about the stolen generations." 

===Box office===
Rabbit-Proof Fence grossed US$3,756,418 in Australia, and $6,199,600 in the United States. Worldwide, it grossed $16,217,411.  

===Accolades===
Selected accolades.

====Wins====
; 2001 - Queensland Premiers Literary Awards.    
:*Film Script—the Pacific Film and Television Commission Award (Christine Olsen)   
; 2002 - Australian Film Institute Awards    John Winter)
:* Best Original Music Score (Peter Gabriel)
:* Best Sound (Bronwyn Murphy, Craig Carter, Ricky Edwards, John Penders) Film Critics Circle of Australia Awards   
:* Best Director (Phillip Noyce)
:* Best Screenplay—Adapted (Christine Olsen)
; 2002 - Inside Film Awards   
:* Best Actress (Everlyn Sampi) Roger Ford) New South Wales Premiers History Awards      The Premiers Young Peoples History Prize (Christine Olsen and Phillip Noyce)
; 2002 (USA) - Aspen Filmfest     
:*Audience Award, Audience Favourite Feature    (Phillip Noyce)
; 2002 (Switzerland) - Castellinaria International Festival of Young Cinema,   
:* ASPI Award (Phillip Noyce)
:* Golden Castle (Phillip Noyce)
; 2002 (USA) - The 2002 Starz Encore Denver International Film Festival     
:* Peoples Choice Award: Best Feature-Length Fiction Film (Phillip Noyce)
; 2002 (South Africa) - Durban International Film Festival    
:* Audience Award (Phillip Noyce)
; 2002 (UK) - Edinburgh International Film Festival    
:* Audience Award (Phillip Noyce)
; 2002 (UK) - Leeds International Film Festival    
:* Audience Award (Phillip Noyce)
; 2002 (USA) - National Board of Review Awards 2002   
:* Freedom of Expression Award
:* Best Director (Phillip Noyce)
; 2002 (USA) - San Francisco Film Critics Circle   
:* Special Citation (Phillip Noyce, also for The Quiet American (2002))
:* Audience Award: Best Foreign Film (Phillip Noyce)
; 2002 (Spain) - Valladolid International Film Festival    
:* Audience Award: Feature Film (Phillip Noyce) London Critics Circle Film Awards (ALFS)    
:* Director of the Year (Phillip Noyce, also for The Quiet American (2002))
; 2003 (Brazil) - São Paulo International Film Festival    
:* Audience Award: Best Foreign Film (Phillip Noyce)

====Nominations==== Australian Film Institute Nominations   
:* Best Actor in a Supporting Role (David Gulpilil)
:* Best Cinematography (Christopher Doyle) Roger Ford)
:* Best Direction (Phillip Noyce)
:* Best Editing (Veronika Jenet, John Scott)
:* Best Production Design (Roger Ford)
:* Best Screenplay Adapted from Another Source (Christine Olsen) Film Critics Circle of Australia Nominations  Australia
:* Best Actor—Female (Everlyn Sampi)
:* Best Cinematography (Christopher Doyle)
:* Best Film
:* Best Music Score (Peter Gabriel)
; 2002 (Poland): Camerimage|Camerimage—2002 International Film Festival of the Art of Cinematography    
:* Golden Frog (Christopher Doyle) Golden Trailer Award Nominations    
:* Golden Trailer: Best Independent Golden Globe Nominations    
:* Golden Globe: Best Original Score—Motion Picture (Peter Gabriel)
; 2003 (USA): Motion Picture Sound Editors Nomination  
:* Golden Reel Award: Best Sound Editing in Foreign Features (Juhn Penders, Craig Carter, Steve Burgess, Ricky Edwards, Andrew Plain) Political Film Society Awards   
:* Exposé
:* Human Rights
; 2003 (USA): Young Artist Awards   
:* Best Performance in a Feature Film—Supporting Young Actress (Everlyn Sampi)
:* Best Performance in a Feature Film—Young Actress Age Ten or Under (Tianna Sansbury)

==See also==
* Cinema of Australia
* Survival film, about the film genre, with a list of related films

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  —Phillip Noyce on Rabbit-Proof Fence

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 