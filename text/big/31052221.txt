The Pirates! In an Adventure with Scientists!
 
 
{{Infobox film
| name           = The Pirates! In an Adventure with Scientists!
| italic title   = yes
| image          = PiratesTeaserPoster.jpg
| alt            = 
| caption        = UK theatrical release poster
| director       = Peter Lord Jeff Newitt  (co-director) 
| producer       = Julie Lockhart Peter Lord David Sproxton
| writer         = Gideon Defoe 
| based on       =  
| starring       = Hugh Grant Martin Freeman Imelda Staunton David Tennant Jeremy Piven Salma Hayek Lenny Henry Brian Blessed Theodore Shapiro 
| cinematography = Frank Passingham
| editing        = Justin Krish
| studio         = Aardman Animations Sony Pictures Animation
| distributor    = Columbia Pictures
| released       =  
| runtime        = 88 minutes 
| country        = United Kingdom United States
| language       = English
| budget         = $55 million   
| gross          = $123,054,041 
}} 3D stop-motion|stop-motion swashbuckler comedy film produced by Aardman Animations in partnership with Sony Pictures Animation. It was directed by Peter Lord. The film was distributed by Columbia Pictures and was released on 28 March 2012 in the UK and on 27 April 2012 in the US.  The Pirates! features the voices of Hugh Grant, Martin Freeman, Imelda Staunton, David Tennant, Jeremy Piven, Salma Hayek, Lenny Henry and Brian Blessed.

The film is loosely based on   in 2005, and Aardmans first stop-motion clay animated film released in 3D and shot in 2.35:1 widescreen.

The film received positive reviews,  while it was a modest box office success, earning $123 million against the budget of $55 million.   The film was nominated for the 2013 Academy Award for Best Animated Feature.

==Plot== last living dodo, and implores the Pirate Captain to enter it into the Scientist of the Year competition at the Royal Society in London for a valuable prize. The Pirate Captain directs his ship to London. The Pirates disguise themselves as Girl Scouts, so that they dont get caught and executed, as Queen Victoria has a strong hatred of Pirates.

Darwin desires to win the Scientist prize on his own in order to impress Queen Victoria, on whom he has a crush. He uses Mr. Bobo, his trained  ), who requests Polly for her petting zoo. During the ceremony, the Pirate Captain accidentally reveals his pirate identity, but Darwin convinces the Queen to spare the Captains life because he knows the location of Polly. The Queen lets the Captain go free with a full pardon, but orders Darwin to locate the dodo by any means necessary.

Darwin takes the Pirate Captain to a tavern, and the latter ultimately reveals that he had stashed Polly in his beard. Darwin and Mr. Bobo are able to capture Polly and are chased by the Captain up into the Tower of London, where the Queen is waiting. The Queen quickly dismisses Darwin and Mr. Bobo, and then offers the Pirate Captain a large amount of treasure in exchange for Polly. Soon, the Pirate Captain reunites with his crew with his newfound wealth, stating that Polly is sleeping within his beard, and sets off for the Pirate of the Year ceremony. The Pirate with a Scarf expresses doubt to the validity of the Captains story.

At the ceremony, the Pirate Captain wins the grand prize from the Pirate King (Brian Blessed), but rival pirate Black Bellamy (Jeremy Piven) makes the Queens pardon known to all pirates in attendance. The Captain is stripped of the prize, his plunder and his pirating licence, and banned from Blood Island.  The crew also deserts him when they find out that he lied about Pollys fate. The Captain returns alone to London to sell baby clothes, but soon becomes determined to free Polly. He re-encounters a now-devastated Darwin, who has learned that the Queen is part of a secret dining society with several other world leaders who are now aboard her steamship, the QV1, waiting to eat the most rarest and most endangered animals. According to the menu, the highlight of the year is: "dodo à lorange". The Pirate Captain enlists Darwins help to steal an airship presented at the science contest and rescue Polly.

Aboard the QV1, the Pirate Captain and Darwin disrupt the meal, and are soon joined by the rest of the Captains crew, who have been informed by Mr. Bobo of his need.  The Queen locates them and attempts to kill both of them, but together they best her. In the battle, they accidentally mix the ships store of baking soda with vinegar, causing a violent reaction that rends the ship in two. The Pirate Captain rescues Polly and they escape safely, leaving behind a furious Queen. With his reputation among pirates restored because of the large bounty now on his head, the Pirate Captain and his crew continue to explore the high seas in search of adventure.

In a few post-credits scenes, they leave Darwin on the Galapagos Islands, Mr. Bobo joins the Captains crew, the Queen is left at the mercy of some of the rare animals she had planned on eating, and Black Bellamy is also forcefully stripped of his trophy by the Pirate King because of the Pirate Captains new infamy.

==Voice cast==
 
* Hugh Grant as Pirate Captain 
* Martin Freeman as the Pirate with a Scarf/Number Two   
* Imelda Staunton as Queen Victoria 
* David Tennant as Charles Darwin
* Jeremy Piven as Black Bellamy
* Salma Hayek as Cutlass Liz   
* Lenny Henry as Peg-Leg Hastings 
* Brian Blessed as Pirate King 
* Russell Tovey as Albino Pirate (UK version) 
* Anton Yelchin as Albino Pirate (US version) 
* Brendan Gleeson as Pirate with Gout 
* Ashley Jensen as Surprisingly Curvaceous Pirate 
* Ben Whitehead as Pirate Who Likes Sunsets and Kittens (UK Version)  
* Al Roker as Pirate Who Likes Sunsets and Kittens (US Version)
* Mike Cooper as Admiral Collingwood David Schneider as Scarlett Morgan
* Mitchell Mullen as Gameshow Host

==Production==
Aardman extensively used computer graphics to complement and enrich the primarily stop-motion film with visual elements such as sea and scenery. Peter Lord commented, "With Pirates!, I must say that the new technology has made Pirates! really liberating to make, easy to make because the fact that you can shoot a lot of green screen stuff, the fact that you can easily extend the sets with CG, the fact that you can put the sea in there and a beautiful wooden boat that, frankly, would never sail in a million years, you can take that and put it into a beautiful CG scene and believe it." 

===Naming===
For the release in the USA, the film has been retitled to The Pirates! Band of Misfits. The official explanation from Aardman was that Defoes books dont have "the same following outside of the UK," so it was not necessary to keep the original title.    Hugh Grant, the voice of The Pirate Captain, said that the studio "didnt think the Americans would like the longer title."  Response from the director of the film, Peter Lord, was that "some people reckoned the UK title wouldnt charm/ amuse / work in the US. Tricky to prove eh?"  Quentin Cooper of the BBC analyzed the change of the title and listed several theories. One of them is that the British audience is more tolerant for the eccentricity of the British animators. Another is that the film makers did not want to challenge the US viewers who do not accept the theory of evolution. He also developed his own explanation, in which he notes that the word "scientist" is rarely used in the Hollywood films due to it not being "cool," representing "the mad scientist or the dweeby nerd that dress funny, have no social skills, play video games, long for unattainable women." 

===Controversy=== Lepra Health in Action and some officials from the World Health Organization, expressed that the joke shows the illness in a derogatory manner, and it "reinforces the misconceptions which leads to stigma and discrimination that prevents people from coming forward for treatment." They demanded an apology and removal of the offending scene,  to which Aardman responded: "After reviewing the matter, we decided to change the scene out of respect and sensitivity for those who suffer from leprosy. The last thing anyone intended was to offend anyone...". LHA responded that it was "genuinely delighted that Aardman has decided to amend the film," while the trailer was expected to be pulled down from websites,  and the theatrical version of the film has the word "leper" replaced with "plague". 

===Music=== Theodore Shapiro The Beat, London Calling" by The Clash, "You Can Get It If You Really Want" by Jimmy Cliff, "Alright (Supergrass song)|Alright" by Supergrass, and "Im Not Crying" by Flight of the Conchords. 

{{Infobox album
| Name       = The Pirates! Band of Misfits
| Type       = Film score Theodore Shapiro
| Cover      = 
| Released   = 24 April 2012
| Recorded   = 2012 Score
| Length     = 51:04
| Label      = Madison Gate Records
| Producer   =  Theodore Shapiro film scores
| Last album = The Big Year (2011)
| This album = The Pirates! Band of Misfits (2012) Hope Springs (2012)
}}

{{Track listing
| total_length = 51:04
| headline = Track listing: 
| title1 = I Hate Pirates!
| length1 = 2:25
| title2 = Attacking Ships
| length2 =2:22
| title3 = The Competition
| length3 = 2:18
| title4 = Not a Total Success
| length4 = 2:03
| title5 = The Ascent of Man
| length5 = 0:38
| title6 = Masked Monkey Chase
| length6 = 2:20
| title7 = Attacking the Beagle
| length7 = 2:31
| title8 = Feathery Heart and Soul
| length8 = 0:43
| title9 = Fog on the Thames
| length9 = 1:02
| title10 = Girl Guides
| length10 = 2:24
| title11 = Wait a Mo!
| length11 = 5:17
| title12 = Dreams Turn to Dust
| length12 = 0:46
| title13 = The Captains Dream
| length13 = 1:33
| title14 = Baby Clothes
| length14 = 2:56
| title15 = The Queens Lair
| length15 = 4:28
| title16 = Market Chase
| length16 = 1:59
| title17 = The Dream Fulfilled
| length17 = 3:23
| title18 = Panda Face Fritters
| length18 = 2:51
| title19 = Poor Defenseless Me
| length19 = 4:28
| title20 = Unpardoned
| length20 = 4:37
}}

==Release==

===Home media===
The Pirates! was released on DVD, Blu-ray Disc|Blu-ray, and Blu-ray 3D on 28 August 2012 in the US,  and on 10 September 2012 in the UK.  The film is accompanied with an 18-minute  short stop motion animated film called So You Want to Be a Pirate!, where The Pirate Captain hosts his own talk show about being a true pirate.  The short was also released on DVD on 13 August 2012, exclusively at Tesco stores in the UK.  As a promotion for the release of The Pirates!, Sony attached to every DVD and Blu-ray a code to download a LittleBigPlanet 2 minipack of Sackboy clothing that represents 3 of the characters: The Pirate Captain, Cutlass Liz and Black Bellamy.  

==Reception==

===Box office===
The film has grossed $123,054,041 worldwide. $26 million came from United Kingdom,  $31 million from the United States and Canada, along with around $92 million from other territories, including the UK. 

===Critical response===
The film received very positive reviews from critics. Review aggregator Rotten Tomatoes reports that 86% of 144 critics have given the film a positive review, with a rating average of 7.2 out of 10. The websites consensus is, "It may not quite scale Aardmans customary delirious heights, but The Pirates! still represents some of the smartest, most skillfully animated fare that modern cinema has to offer."    Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 73 based on 31 reviews. 

===Accolades===
{| class="wikitable"
|+List of awards and nominations 85th Academy Academy Awards    Academy Award Best Animated Feature Peter Lord
| 
|- Annie Awards   Best Animated Feature
|
|rowspan=5  
|- Character Animation in a Feature Production Will Becher
|- Production Design in an Animated Feature Production Norman Garwood, Matt Berry
|- Voice Acting in an Animated Feature Production Imelda Staunton as Queen Victoria
|- Writing in an Animated Feature Production Gideon Defoe
|- 25th European European Film Awards     Best Animated Feature Film
|
| 
|- Visual Effects Society  
| Outstanding Animated Character in an Animated Feature Motion Picture
| Will Becher, Jay Grace, Loyd Price
|  
|}

==Potential sequel==
On 14 August 2011, Peter Lord tweeted that they are working on the sequel idea.  In June 2012, Lord said in an interview that, "Weve got the story, all we need now is the backing."  In January 2013, Lord responded to a question whether the sequel will be out soon: "Theres not going to be a sequel - well not in the foreseeable future."  At a talk later in the year, he explained this was due to the film not meeting box office expectations in the United States. 

==References==
 

==External links==
 
 
*  – official site (US)
* 
* 
* 
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 