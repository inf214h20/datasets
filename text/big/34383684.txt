Leathernecking
{{Infobox film
| name           = Leathernecking
| image          = LeatherneckingFilmPoster.jpg
| image size     = 
| caption        = Film poster
| director       = Edward F. Cline Fred Fleck (assistant)
| producer       = William Le Baron Louis Sarecky (assoc.)
| writer         = Alfred Jackson Jane Murfin
| based on       =  
| starring       = Irene Dunn Eddie Foy Jr.
| music          = Victor Baravalle Oscar Levant Sidney Clare
| cinematography = J. Roy Hunt
| editing        = 
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =    |ref2=    }}
| runtime        = 79 min.
| country        = United States
| language       = English
}}
 Present Arms, by Richard Rodgers, Lorenz Hart and Herbert Fields.  Although based on a musical, it used none of the original Rodgers and Hart songs from the Broadway hit, instead having original songs, including three by Oscar Levant and Sidney Clare.

An early part-color feature film with a Technicolor insert, this film was Irene Dunnes film debut, although no copies of it are thought to exist.

==Plot==
Chick Evans is a Marine private in Honolulu, Hawaii.  He falls for society girl Delphine Witherspoon, and begins to scheme as to how to win her over.  His first plan involves impersonating an officer in order to get invited to a society party.  However, when his Marine buddies decide to crash the party as well, his real rank is revealed, and so having the opposite effect on Delphine as he had planned.  

Despondent, he bares his soul to a mutual friend, Edna, who arranges to have the two reunited on Delphines yacht at sea.  However, this meeting goes terribly wrong as well, and a desperate Chick convinces the yachts captain to fake a shipwreck, in order to give him time to win Delphine over.  Unfortunately, a real storm arises and the ship is actually wrecked, coming to rest on a desert island.  

While on the island, Chicks persistence pays off, and he gets the girl.  Not only that, on their return to Honolulu, he is hailed as a hero and promoted to Captain.  

== Cast ==
* Irene Dunne as Delphine Witherspoon Ken Murray as Frank
* Louise Fazenda as Hortense
* Ned Sparks as Reynolds (Sparks)
* Lilyan Tashman as Edna
* Eddie Foy Jr. as Chick Evans
* Benny Rubin as Stein
* Rita La Roy as Fortune Teller
* Fred Santley as Douglas William von Brinken as Richter
* Carl Gerard as Colonel
* Werther Weidler as Richters son
* Wolfgang Weidler as Richters son

(Cast list as per AFI database) 

==Songs (partial list)==
*"All My Life"
*"Careless Kisses"
*"Evening Star"
*"Mighty Nice and So Particular"
*"Shake It Off and Smile"

All songs were written by Oscar Levant and Sidney Clare, except for "All My Life", which had words and music by Benny Davis and Harry Akst.  

==Notes==
This film is now considered a lost film. 
 Lew Fields Mansfield Theatre (currently the Brooks Atkinson Theatre).  Produced by Lew Fields, it had music by Richard Rodgers, lyrics by Lorenz Hart, with a book by Herbert Fields.  It starred and was choreographed by Busby Berkeley. 
 public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

==See also==
* List of early color feature films
* List of lost films

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 