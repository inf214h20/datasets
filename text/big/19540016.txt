Nasser Asphalt
{{Infobox film
| name           = Nasser Asphalt/ Wet Asphalt
| caption        = A poster with the films English title: Wet Asphalt
| image	=	Nasser Asphalt FilmPoster.jpeg
| director       = Frank Wisbar
| producer       = Wenzel Lũdecke 
| writer         = Will Tremper
| starring       = Horst Buchholz Martin Held Gert Fröbe Maria Perschy
| music          = Hans-Martin Majewski
| cinematography = Helmut Ashley
| editing        = Klaus Dudenhöfer
| distributor    = 
| released       = 1958
| runtime        = 90 minutes
| country        = West Germany
| language       = German
| budget         = 
}} thriller starring Horst Buchholz and featuring Gert Fröbe, written by Will Tremper and directed by Frank Wisbar.

==Plot==
The young journalist Bachmann (Horst Buchholz) is released from prison by the intervention of Cesar Boyd (Martin Held), a respected and prosperous journalist who hires him as his assistant. Bachmann had been in jail for his attempt to interview war criminals at Spandau prison. Bachmann works for Boyd who acts as his mentor, however, Bachmann’s stories are published under Boyd’s name.  Bettina (Maria Perschy), the daughter of a friend comes to study in Berlin and will stay at Boyd. Both Bachmann who picks her up at the airport and Boyd develop an interest in the young woman. In the commotion, a story that has to go out to a Paris newspaper is not prepared, and Boyd, after listening to a tale from his chauffeur Jupp (Gert Fröbe), in the last minute makes up a story of five German soldiers who have lived in a supply bunker in Poland for six years where they had gotten trapped when it was exploded at the end of the war. There is one survivor, now blind, who was brought to a hospital. The story becomes an international sensation and people want to know more. Journalists are sent to Poland to investigate. The Russians suspect the story to be a ruse to send spies. Pressure is put on the Polish government to release the blind soldier. In Germany, widows believe the blind soldier may be their missing husband.  Bachmann who initially thought that Boyd had reliable sources for his story, soon  recognizes that it is all a big lie. He breaks with his mentor, and tries to warn the public about the hoax. He convinces Bettina of Boyd’s lies, and both of them leave him to start a new beginning.

==Cast==
* Horst Buchholz as Greg Bachmann
* Martin Held as Cesar Boyd
* Maria Perschy as Bettina
* Gert Fröbe as Jupp
* Heinz Reincke as Der Blinde
* Inge Meysel as Gustl
* Peter Capell as Donnagan
* Renate Schacht as Wanda Richard Münch as Dr. Wolf

==Comments==
The black-and-white movie features Horst Buchholz who was already a star at this time and dubbed the German James Dean. Gert Fröbe has a supporting role. It also brought wider attention to Maria Perschy, and all three actors would soon move on to go to a Hollywood career before they would return to Europe. In contrast Frank Wisbar had come from Hollywood and Nasser Asphalt was one of several films he made in post-war Germany to deal with the lessons of the war and Germanys recent history. The film is set in war-scarred Berlin.

While the DVD version of the film is marketed as a film noir, it lacks the characteristic of the film noir genre.  
 Danzig (Gdansk) Gdingen (Gdynia).

==Awards==
*1958 Film Award in Silver for "Outstanding Individual Achievement: Music", awarded to Hans-Martin Majewski.

==References==
 

==External links==
*  

 
 
 
 
 
 