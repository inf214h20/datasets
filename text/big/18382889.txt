Sangram (1950 film)
{{Infobox film
| name           = Sangram
| image          = Sangram50.jpg
| image_size     =
| caption        = 
| director       = Gyan Mukherjee
| producer       = 
| writer         = 
| narrator       = 
| starring       = Ashok Kumar
| music          = 
| cinematography = Josef Wirsching
| editing        = 
| distributor    = 
| released       = 1950
| runtime        = 139 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1950 Bollywood film about a spoilt child in adulthood directed by Gyan Mukherjee and starring Ashok Kumar. A box-office success, the film became the sixth highest earning Indian film of 1950, earning an approximate gross of Rs. 1,00,00,000 and a nett gross of Rs. 55,00,000.  
“Sangram” is a precursor to the cop-father-criminal-son that later came to be a favourite Bollywood theme, though it was made in noir style cinema that had begun to inspire filmmakers, and Gyan Mukherjee was no exception. An intense, bold 139-minute violent crime drama, and Ashok Kumar daring to essay a negative character at a time he was in great demand as a hero. And he excelled in it. First of the many such films he went on to star in during the early 50s.



==Plot==

A cop brings up his motherless son, spoiling him by succumbing to even his unnecessary demands, resulting in his getting familiar with ruffians and even gambling. The boy Kunwar (Shashi Kapoor) even once carries his fathers pistol and fires it at a chum in a fit of rage. Timely help from father (Nawab) saves him from the lock-up.

On growing up Kunwar (Ashok Kumar), moves to a city and starts running a casino in the guise of a hotel. But he is betrayed by one of his own confidantes, and the police raid the hotel, and although gets hurt, manages to escape, and happens to meet his childhood companion (Baby Tabassum) who has grown up to be an attractive young woman (Nalini Jaywant). Kunwars dark past catches up with him in the form of an old accomplice (Tiwari), who blackmails him as he is about to get married. He steals his deceased mothers jewellery helped by the dancing girl in his casino who, unwittingly, gets arrested.

To avenge the wrong, he goes after the criminals. During the fight in a running train the gangster falls off and gets killed. When he returns with the stolen jewellery, the cops recognise him as the guy wanted in the casino raid, and put him behind bars. He escapes from prison on learning that his ladylove was going to be married off.

He kidnaps her and takes her to the dancing girls house for refuge. The story takes yet another twist as the dancer feels betrayed, as she too loves him, and tries to alert the cops.

In a frenzy he shoots her, upsetting his own woman in the process. Cornered by cops he indiscriminately fires, killing them all in the process. He is eventually shot dead by his own father when he tries to fool him by threatening to kill his own beloved though his pistol is empty.

But not before the customary dying speech. 

==Cast==
*Nalini Jaywant ...  Kunwars Fiancee 
*Shashi Kapoor ...  Kunwar in childhood 
*Ashok Kumar ...  Kunwar 
*Nawab ...  Kunwars Father 

==Soundtrack==

The film, which had C. Ramachandra composing the six lyrics by P.L. Santoshi, Raza Mehndi, and Vijendra Gaur including two duets by Chitalkar, the music director himself — one each with Shamshad Begum and Lata Mangeshkar who also wrote the dialogue, with insightful black-and-white camerawork by Josef Wirsching turned out to be one of the biggest blockbusters of its time, raking in, reportedly, Rs.55 lakhs in profits. “Sangram” was also Guru Dutts last film as an assistant director


==References==
 
==External links==
*  

 
 
 

 