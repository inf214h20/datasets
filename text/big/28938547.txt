The Man Who Liked Funerals
 
{{Infobox Film
| name           = The Man Who Liked Funerals
| image          =  
| image_size     = 
| caption        =  David Eady
| producer       = Jon Penington
| writer         = Margot Bennett Cecily Finn Joan OConnor 
| starring       = Leslie Phillips Susan Beaumont Bill Fraser
| music          = Edwin Astley Eric Cross
| editing        = John Seabourne Sr.
| distributor    =  
| released       = January 1959
| runtime        =  
| country        =  
| language       = English
| budget         = 
}} David Eady and written by Margot Bennett, Cecily Finn and Joan OConnor. The film was released in the United Kingdom in January 1959.

==Synopsis==

In order to help a youth club which is under threat of closure, a man begins attending funerals where he blackmails the relatives of the recently deceased, threatening to publish incriminating stories about them. However, his plans encounter problems when he tries to blackmail the family of a prominent villain.

==Cast==
* Leslie Phillips as Simon Hurd 
* Susan Beaumont as Stella 
* Bill Fraser as Jeremy Bentham 
* Thelma Ruby as Junior Mistress 
* Mary Mackenzie as Hester Waring 
* Paul Stassino as Nick Morelli  Jimmy Thompson as Lieutenant Hunter  Charles Clay as Colonel Hunter 
* Anita Sharp-Bolster as Lady Hunter 
* Shaun ORiordan as Reverend Pitt 
* Marianne Stone as Benthams secretary

==External links==
* 

 
 
 
 
 
 
 


 