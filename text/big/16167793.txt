Kadhal Dot Com
{{Infobox film
| name = Kadhal Dot Com
| image =
| caption =
| director = S. R. Selvaraj
| writer = Prasanna Anu Sasi Shruthi Raj
| producer =
| music = Bharathwaj
| editor =
| cinematographer = Shankar
| studio = AAA Movies International
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
}}
Kadhal Dot Com is a 2003 Tamil romantic film directed by Selvaraj. The film features Prasanna (actor)|Prasanna, Anu and Shruthi Raj in the lead roles, while Vadivelu and S. V. Shekher played supporting roles. The film released in August 2003 to a below average response at the box office. 

==Cast== Prasanna as Vinod
*Anu Sasi as Sandhya
*Shruthi Raj as Priya
*Vadivelu
* Prashanth kharun as Guna
*S. V. Shekher
*Nirosha
*Batista
*Radhika Chaudhari in an item number.

==Production==
The film marked the debut in Tamil films for Anu, daughter of Malayalam director I. V. Sasi. 

==Release==
A critic from The Hindu noted "at a time when much-hyped, hero-backed films end up being mere dampeners, a low-key film like  this comes with no unsavoury loudness or lewdness, is at least a decent offering."  Another critic noted "the story has predictable turnings. At times it appears as if we are watching an old film."  The films low key release meant that the film went unnoticed at the box office.

==Soundtrack==
{{Infobox album|  
  Name        = Kadhal Dot Com
|  Type        = Soundtrack
|  Artist      = Bharathwaj
|  Cover       =
|  Released    = 
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Kadhal Dot Com (2004)
|  Next album  = 
}}
The soundtrack of the film was composed by Bharathwaj.

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Imaikkatha Vizhigal
| extra1          = Srinivas (singer)|Srinivas, Srimathumitha
| length1         = 5:00
| title2          = Pacha Thanni 
| extra2          = Mano (singer)|Mano, Reshmi
| length2         = 4:05
| title3          = Kadhal Kadhal Priyadarshini
| length3         = 5:49
| title4          = Unna Enakku
| extra4          = Unnikrishnan, Anuradha Sriram
| length4         = 5:08
| title5          = Vennila
| extra5          = Reshmi, Geetha, Bindu, Uma Maheshwaran 
| length5         = 4:54
| title6          = Atha Ponn
| extra6          = Balram, Pop Shalini
| length6         = 4:43
}}

==References==
 

 
 
 
 


 