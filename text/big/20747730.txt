If You Could Say It in Words
{{Infobox film
| name           = If You Could Say It in Words
| image	=	If You Could Say It in Words FilmPoster.jpeg
| caption        = 
| director       = Nicholas Gray
| producer       = Nicholas Gray Katharine Clark Gray Adam Eisenstein
| writer         = Nicholas Gray
| narrator       = 
| starring       = Alvin Keith Marin Ireland
| music          = David Goodrich
| cinematography = Richard Sands
| editing        = Ariel Roubinov
| distributor    = Vanguard Cinema
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

If You Could Say it in Words is a 2008 American romance film film written and directed by Nicholas Gray. It focuses on the relationship between two disparate individuals. The film played at multiple American film festivals in 2008-09. It has also been screened in connection with autism "awareness" programs in Nebraska in 2009 and by the Dutch Ministry of Healths during Autism "Awareness" Week in 2010. The film will be distributed on DVD in North America by Vanguard Cinema in November 2010.

==Plot==
Nelson Hodge is a lower class painter with undiagnosed Asperger syndrome who is immersed in completing a triptych of emotion paintings. Sadie Mitchell is attempting to get her life back in order while sacrificing in most areas of her life. The two meet by chance and engage in an awkward romance that starts to bring their flaws to the surface.

==Cast==
*Alvin Keith ..... Nelson Hodge
*Marin Ireland ..... Sadie Mitchell
*Gerry Lehane ..... Dr. Mark Gibson
*Yvonne Woods ..... Julie Gibson
*Susanna Guzman ..... Maria Ramirez
*PaSean Wilson ..... Lucy
*Douglas Stewart ..... Dr. Sid Meyers
*Dana Snyder ..... Baseball Fan #1
*Stephen McKinley Henderson ... Baseball Fan #2
*McDonald Gray Jr. ..... Dr. Radison
*Sam Torpio ..... Ewan Lancaster
*Katie Cook ..... Sally Stoop
*Brett Casne ..... Yankee Hater #1
*Joseph Wickersham ..... Yankee Hater #2
*John Lewis ..... Yankee Hater #3
*Joshua Minton ..... Yankee Hater #4
*Tom Dudzic ..... Yankee Hater #5

==Critical reception==
Alex Plank, founder of the Wrong Planet community for autism and autism spectrum disorders, first discussed the film on the organizations website with a February 2008 interview with actor Alvin Keith and director Nicholas Gray.  In September 2008 he wrote a review in which he described his appreciation for the films three-dimensional and unexaggerated interpretation of the disorder, calling it "...the most authentic portrayal of an autistic person Ive ever seen in the movies." 

New York Film Critics Circle member, Mike dAngelo, gave special praise to actress Marin Ireland, calling her work "...one of the most stunning performances Ive seen in the past couple of years — easily my top Best Actress pick for various polls and surveys, at this writing, should the film get a New York commercial release." 

==Awards and nominations==
The film was named Best Feature and Best of Festival at the 2008 Derby City Film Festival in Louisville, Kentucky.

The films West Coast premiere was at the 2009 Method Fest in Calabasas Ca, near Los Angeles.  Method Fest is named after the famous "Method" school of acting, showcases breakout acting performances in character and story-driven films.  At this festival the film was presented by award-winning writer Dennis Lehane and earned nominations for Outstanding Achievement in Low Budget Filmmaking and for Best Actress (Marin Ireland). 

==References==
 

==External links==
* 
*  
*  
*  
*  

 
 
 
 
 
 