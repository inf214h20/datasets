Aftershock (2010 film)
 

 
{{Infobox film
| name = Aftershock
| image = Aftershock.jpg
| caption = Film poster
| film name = {{Film name| traditional = 唐山大地震
| simplified = 唐山大地震
| pinyin = Tángshān Dà Dìzhèn}}
| director = Feng Xiaogang
| producer = Huayi Brothers
| story = Zhang Ling
| screenplay = Su Xiaowei Lu Yi Li Chen
| music = Wang Liguang
| cinematography = Lü Yue
| editing = Xiao Yang
| distributor = Huayi Brothers
| released =  
| runtime = 135 minutes
| country = China
| language = Mandarin
| gross = 665 million yuan (US$108 million) 
| budget = less than $25 million   
}} Lu Yi, Li Chen.    The film depicts the aftermath of the 1976 Tangshan earthquake. It was released in China on 22 July 2010, and is the first "big commercial film" IMAX film created outside the United States.  The film was a major box office success, and has grossed more than US$100 million at the Chinese box office. 

== Plot ==
In 1976 Tangshan, Li Yuanni lives in a small apartment with her husband and their twins Fang Deng and Fang Da. Li tells her husband, Fang Daqiang, that she wishes to have one more child, and they get into the back of their truck after putting their son and daughter to bed. Suddenly the ground shakes, and buildings begin tumbling down. Running back to save their children, Li is pulled back by her husband, who runs ahead of her and is instantly crushed. Their house collapses, trapping her two children.

In the aftermath of the 1976 Tangshan earthquake, a rescue team informs Li that her twins are trapped together under a slab of concrete. Lifting the slab in any way will kill one of her children. Heartbroken, she decides to save her son, Fang Da. The girl, Fang Deng, survives and wakes up later to find herself among several dead bodies.

Assumed to be an orphan, Fang Deng is adopted by a military couple. She refuses to speak, but eventually opens up and bonds with her adopted parents, the couple Wang Deqing and Dong Guilan. Ten years later, Fang Deng (renamed to "Wang Deng" after taking on her adoptive fathers surname) is accepted into medical school and moves away, where she meets a graduate student, Yang Zhi, and begins an intimate relationship with him. In Fang Dengs third year of study, her adoptive mother falls ill. Before dying, she asks Fang Deng to use the money they saved to find her real family. Fang Deng finds out she is pregnant, and despite being pressured by Yang to get an abortion, she refuses to abandon her baby. She secretly drops out of university and loses contact with Yang and her adoptive father. 

During that period of time, Fang Da grows up with his mother. The earthquake claimed his left arm, leaving him physically disabled. Rather than taking his university entrance exams, Fang Da opts to make it on his own by transporting people with his bike. He leaves his mother in Tangshan and moved to Hangzhou, and eventually becomes the boss of a successful travel agency. He marries and has a son, named Diandian.

After a four-year absence, Fang Deng goes back to see her adoptive father with her daughter, also named Diandian. She apologises to her adoptive father and reconciles with him. On New Years Eve, Fang Deng tells her adoptive father that she is getting married to a foreigner and will be emigrating to Vancouver, Canada, with her daughter.
 earthquake in Sichuan on television. She immediately volunteers to join rescuers and returns to China. Fang Da has also decided to help in the rescue efforts. While taking a break, Fang Deng overhears Fang Da talking about the Tangshan earthquake. She reunites herself with her brother, and they both decide to visit their mother. At first, Fang Deng is angry at her mother for abandoning her. Later, after realising the remorse, emotional agony and guilt that her mother had gone through, she forgives the latter. 

The screen then cuts to a stone memorial in Tangshan with the names of all the 240,000 victims of the earthquake.

==Cast==
* Xu Fan as Li Yuanni, Fang Deng and Fang Das mother.
* Zhang Jingchu as Fang Deng / Wang Deng
** Zhang Zifeng as Fang Deng (child) Li Chen as Fang Da, Fang Dengs brother.
** Zhang Jiajun as Fang Da (child)
* Chen Daoming as Wang Deqing, Fang Dengs adoptive father.
* Zhang Guoqiang as Fang Daqiang, Fang Deng and Fang Das father. Lu Yi as Yang Zhi, Fang Dengs boyfriend.
* Wang Ziwen as Xiaohe, Fang Das wife. Chen Jin as Dong Guilan, Fang Dengs adoptive mother.
* Lü Zhong as Fang Deng and Fang Das grandmother
* Yong Mei as Fang Deng and Fang Das aunt
* Yang Lixin as Laoniu, a man who has a crush on Li Yuanni.
* Liu Lili as the mother of a child victim in the Sichuan earthquake

== Development and release ==
The film was produced by Huayi Brothers, which partnered with IMAX to produce three Chinese films (of which Aftershock is the first).  In Singapore, it is distributed by Homerun Asia with Scorpio East and Golden Village Pictures.

Aftershock was released in over 5,000 conventional and 14 IMAX theaters in late July 2010.    In early August 2010, the film surpassed The Founding of a Republic as the highest-grossing locally-made film in China, with a RMB532 million gross. 
 was selected Best Foreign Language Film at the 83rd Academy Awards,    but failed to make it into the final shortlist.   

After the filming of Aftershock, director   and the massive earthquake. 

==Theme songs==
* Shang Wenjie — "23 Seconds, 32 Years" (end credits)
* Faye Wong — "Heart Sutra" (just before end credits)

==Reception and awards==
Aftershock won the Best Feature Film and Best Performance by Actor for Chen Daoming at the 4th annual Asia Pacific Screen Awards.    Raymond Zhou of China Daily placed the film on his list of the best ten Chinese films of 2010.  The film currently holds a 90% rating on Rotten Tomatoes with an average score of 6.4/10, although there is not enough consensus to "certify" it fresh or rotten. 

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Chinese submissions for the Academy Award for Best Foreign Language Film

== References==
 

== External links ==
*    
*  
* Cheng, Cen (2012) “No Dread for Disasters. Aftershock and the Plasticity of Chinese Life”, in: manycinemas issue 3, 26-39    

 
 

 
 
 
 
 
 
 
 