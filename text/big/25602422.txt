The Curse of King Tut's Tomb (1980 film)
{{Infobox film
| name           = The Curse of King Tuts Tomb
| image          = 
| image_size     = 
| caption        = 
| director       = Philip Leacock
| producer       = Peter Graham Scott
| writer         = Barry Wynne (book Behind the Mask of Tutankhamen) Herb Meadow (writer)
| narrator       = 
| starring       = Eva Marie Saint Harry Andrews
| music          = Gil Mellé
| cinematography = Bob Edwards
| editing        = Adrian Brenard HTV West
| released       = 8 May 1980 (USA) 31 August 1980 (UK)
| runtime        = 98 minutes
| country        = UK USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} American mystery thriller film directed by Philip Leacock and starring Eva Marie Saint, Harry Andrews and Paul Scofield, with Tom Baker.

== Plot ==
The English archaeologist Howard Carter and his financier, Lord Carnarvon discover after years of search the grave of Tutankhamun|Tut-Ench-Amun. Rumors about a curse that invites to anyone who disturbs the peace grave circulate in public. For even the unscrupulous art collector Sabastian is after the legendary gold sarcophagus. The Curse of the Pharaoh seems to be effective, for there will be a series of mysterious deaths.

== Cast ==
*Eva Marie Saint as Sarah Morrissey
*Robin Ellis as Howard Carter
*Raymond Burr as Jonash Sebastian Lord Carnarvon
*Wendy Hiller as Princess Vilma
*Angharad Rees as Lady Evelyn Herbert
*Tom Baker as Hasan
*Barbara Murray as Giovanna Antoniella Lady Almina Carnarvon
*Patricia Routledge as "Posh" Lady
*John Palmer as Fishbait
*Darien Angadi as Ahmed Nahas
*Rupert Frazer as Collins
*Rex Holdsworth as Doctor
*Stefan Kalipha as Daoud
*Andy Pantelidou as Lieutenant
*Alfred Hoffman as Stallholder
*Paul Scofield as Narrator (voice)

==Production==
It based on the book Behind the Mask of Tutankhamen by Barry Wynne. It was shot in Egypt and in England. 

== Soundtrack ==
The score was composed by American jazz musician Gil Mellé. 

==Release==
It was released as two-part film on 8 May and 9 May 1980. 

==DVD release==
A Region 2 DVD release by Network DVD was released in 2011.

==References==
 

== External links ==
* 
* 


 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 