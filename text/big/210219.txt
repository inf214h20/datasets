Irma la Douce
 

:This article is about the film. For the stage musical, see Irma La Douce (musical)

{{Infobox film
| name          = Irma la Douce
| image         = Irma la Douce 1963 film poster.jpg
| caption       = Italian release poster
| director      = Billy Wilder
| producer      = Billy Wilder   I. A. L. Diamond   Edward L. Alperson   Doane Harrison   Alexandre Trauner
| writer        = Billy Wilder   I. A. L. Diamond   Alexandre Breffort (play)
| starring      = Jack Lemmon   Shirley MacLaine
| narrator      = Louis Jourdan
| studio        = The Mirisch Corporation
| distributor   = United Artists
| music         = André Previn
| cinematography= Joseph LaShelle
| editing       = Daniel Mandell
| released      =  
| runtime       = 147 minutes
| country       = United States
| language      = English
| budget        = $5 million   IMDb. Retrieved September 5, 2013. 
| gross         = $25,246,588   The Numbers. Retrieved September 5, 2013. 
}}
Irma la Douce is a 1963 romantic comedy starring Jack Lemmon and Shirley MacLaine, directed by Billy Wilder.
 Irma La Douce by Marguerite Monnot and Alexandre Breffort.

==Plot==
Irma la Douce   tells the story of Nestor Patou (Jack Lemmon), an honest cop, who after being transferred from the park Bois de Boulogne to a more urban neighborhood in Paris, finds a street full of prostitutes working at the Hotel Casanova and proceeds to raid the place. The police inspector, who is Nestors superior, and the other policemen, have been aware of the prostitution, but tolerate it in exchange for bribes. The inspector, a client of the prostitutes himself, fires Nestor, who is accidentally framed for bribery.

Kicked off the force and humiliated, Nestor finds himself drawn to the very neighborhood that ended his career with the Paris police - returning to Chez Moustache, a popular hangout for prostitutes and their pimps. Down on his luck, Nestor befriends Irma La Douce (Shirley MacLaine), a popular prostitute. He also reluctantly accepts, as a confidant, the proprietor of Chez Moustache, a man known only as "Moustache."  In a running joke, Moustache (Lou Jacobi), a seemingly ordinary barkeep, tells of a storied prior life &ndash; claiming to have been, among other things, an attorney, a colonel, and a doctor, ending with the repeated line, "But thats another story."  After Nestor defends Irma against her pimp boyfriend, Hippolyte, Nestor moves in with her, and he soon finds himself as Irmas new pimp.

Jealous of the thought of Irma being with other men, Nestor comes up with a plan to stop Irmas prostitution. But he soon finds out that it is not all that it is cracked up to be.  He invents an alter-ego, "Lord X", a British lord, who "becomes" Irmas sole client. Nestors plans to keep Irma off the streets soon backfire and she becomes suspicious, since Nestor must work long and hard to earn the cash "Lord X" pays Irma. When Irma decides to leave Paris with the fictitious Lord X, Nestor decides to end the charade. Unaware he is being tailed by Hippolyte, he finds a secluded stretch along the river Seine and tosses his disguise into it.  Hippolyte, not having seen Nestor change his clothes, sees "Lord X"s clothes floating in the water, and concludes Nestor murdered him.  Before Nestor is arrested Moustache advises him not to reveal that Lord X was a fabrication.  He tells him, "The jails are full of innocent people because they told the truth." Nestor admits to having killed Lord X, but only because of his love for Irma.

Hauled off to jail, but with Irma in love with him, Nestor is sentenced to 15 years hard labor.  Learning that Irma is pregnant Nestor escapes from prison, with Moustaches help, and returns to Irma. He narrowly avoids being recaptured when the police search for him in Irmas apartment, but donning his old uniform Nestor simply blends in with the other police. With the help of Hippolyte, Nestor arranges for the police to search for him along the Seine from which, dressed as Lord X, he emerges. Knowing he cannot be rearrested for a murder the police now know did not occur, Nestor rushes to the church, where he plans to marry Irma. As she walks down the aisle she begins to experience contractions and they continue during the wedding ceremony.  Nestor and Irma barely make it through the ceremony before she goes into labor and delivers their baby. While Nestor and everyone else is occupied with Irma, Moustache notices one of the guests sitting alone at the front of the church. Rising from his seat and walking past Moustache, the guest is none other than Lord X! A clearly baffled Moustache looks at Lord X, and then at the audience. "But thats another story," he says.

== Cast ==
* Jack Lemmon as Nestor Patou/Lord X
* Shirley MacLaine as Irma la Douce
* Lou Jacobi as Moustache
* Bruce Yarnell as Hippolyte
* Grace Lee Whitney as Kiki the Cossack
* Joan Shawlee as Amazon Annie
* Hope Holiday as Lolita
* Sheryl Deauville as Carmen
* Ruth Earl as one of the Zebra twins
* Jane Earl as one of the Zebra twins
* Harriette Young as Mimi the MauMau
* Herschel Bernardi as Inspector Lefevre
* Cliff Osmond as police sergeant
* Tura Satana as Suzette Wong
* Billy Beck as Officer Dupont
* Edgar Barrier as General Lafayette
* Bill Bixby as the tattooed sailor
* James Caan as the soldier with radio
* Louis Jourdan as Narrator
* Paul Frees as Trailer Narrator

==Awards==

Though the film is not a musical, it won André Previn an Academy Award for Best Score—Adaptation or Treatment. There is also a scene in the film, in which Shirley MacLaine exclaims "Dis-donc!" whilst dancing on a table, which appears to be a deliberate tribute to the musical from which the film is derived.
 Best Actress Best Cinematography, Color.

==Production==
MacLaine was paid $350,000 plus a percentage. Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 171 

==Reception== 5th highest theatrical rentals.  Irma la Douce earned over $15 million in worldwide rentals but because of profit participation for Wilder and the two stars, United Artists only made a profit of $440,000 during its theatrical run. 

==Soundtrack==
{{Infobox album  
| Name        = Irma La Douce
| Type        = soundtrack
| Artist      = André Previn
| Cover       = Irma_la_douce_(rykodisc.jpg
| Released    = 13 July 1998
| Length      =
| Genre       =
| Label       = Rykodisc
}}
All compositions by André Previn.

# "Main Title" 2:14
# "Meet Irma" 1:42
# "This Is the Story" 3:16
# "Nestor the Honest Policeman" 1:54
# "Our Language of Love" 2:04
# "Dont Take All Night" 5:43
# "The Market" 6:28
# "Easy Living the Hard Way" 3:16
# "Escape" 2:13
# "Wedding Ring" 1:35
# "The Return of Lord X" 1:24
# "In the Tub with Fieldglasses" 2:27
# "Goodbye Lord X" 3:17
# "Im Sorry Irma" 1:38
# "Juke Box: Lets Pretend Love" 3:07
# "Juke Box: Look Again" 2:16
# "But Thats Another Story" 0:38

==Remakes==
*Irma la Douce was remade for French television in 1972.
*The film is remade in India as the controversial Hindi film Manoranjan with Sanjeev Kumar and Zeenat Aman reprising the roles of Jack Lemmon and Shirley MacLaine respectively.

==Others==
In 1968, the Egyptian movie Afrit Mirati (My wifes Goblin) starring Shadia and Salah Zulfikar contained a soundtrack entitled Irma la Douce performed by Shadia. 

== References ==
 

== External links ==
 
*  
*   
*  
*  
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 