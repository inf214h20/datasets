The Judge and the Assassin
{{Infobox film
| name           = The Judge and the Assassin
| image          = JudgeAndTheAssasin.jpg
| image_size     = 
| caption        = Video tape cover
| director       = Bertrand Tavernier
| producer       = Raymond Danon
| writer         = Jean Aurenche Pierre Bost Bertrand Tavernier
| narrator       = 
| starring       = Philippe Noiret Michel Galabru
| music          = Philippe Sarde
| cinematography = Pierre-William Glenn
| editing        = Armand Psenny
| distributor    = Libra Films (USA)
| released       = March 10, 1976
| runtime        = 128 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} French drama film directed by Bertrand Tavernier. The film won two César Awards in 1977.
It is based on the life of mass murderer Joseph Vacher.

==Selected cast==
*Philippe Noiret as Judge Rousseau
*Michel Galabru as Sgt. Joseph Bouvier
*Isabelle Huppert as Rose
*Jean-Claude Brialy as Villedieu, Attorney
*Renée Faure as Mme. Rousseau
*Cécile Vassort as Louise Leseuer
*Jean-Roger Caussimon as Street Singer
*Jean Bretonnière as Deputy
*Christine Pascal (uncredited)

==See also==
* Isabelle Huppert filmography

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 