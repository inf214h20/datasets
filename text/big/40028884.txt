Bad Asses
 
{{Infobox film
| name           = Bad Asses
| image          = Bad_Ass_Production_Poster.jpg
| caption        = Theatrical film poster
| director       = Craig Moss
| producer       = Ash R. Shah Ben Feingold Jim Busfield
| writer         = Craig Moss
| starring       = Danny Trejo Danny Glover Andrew Divoff Jacqueline Obradors
| music          = Todd Haberman
| cinematography = Paul Marschall
| editing        = Clark Burnett
| studio         = Silver Nitrate
| distributor    = 20th Century Fox Home Entertainment  , Indie Wire 
| released       = 
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Bad Ass, and was released on DVD during spring 2014. 

==Plot synopsis==

Since we last saw Frank, he followed his dream and opened a Community Center in East Los Angeles where he mentors young boxers, not only in the ring, but in life. When his prized student, Manny, gets in over his head with a bad crowd and winds up dead, Frank and Bernie team up, finding themselves ensnared in one life-threatening predicament after another. Forced to escape using the only weapons they have – their wits and their fists – they must survive the onslaught of fury that is brought upon them and Frank’s new found love from a high powered, politically connected foe, Leandro (Andrew Divoff).

==Cast==
*Danny Trejo as Frank Vega
*Danny Glover as Bernie Pope
*Andrew Divoff as Leandro Herrera
*Jacqueline Obradors as Rosaria Parkes
*Charlie Carver as Eric
*Jonathan Lipnicki as Hammer
*Leon Thomas III as Tucson
*Dante Basco as Gangly Asian
*Rob Mello as Buford Granger

==Sequel==
A third installment of the franchise, titled Bad Asses on the Bayou, is scheduled for release on March 6, 2015. 

== References ==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 


 