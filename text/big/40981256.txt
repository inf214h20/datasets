Joseph and the Amazing Technicolor Dreamcoat (film)
Joseph of the same name.

== Plot == Broadway musical. The only change is the addition of a very brief subplot in which the actors begin the film as "teachers" in a school where the "students" become the childrens chorus of the musical. This subplot opens the film and doesnt reappear until the very end during the credit role. For a full plot summary, see Joseph and the Amazing Technicolor Dreamcoat.

== Cast  ==
 Joseph
* Maria Friedman as Narrator
* Richard Attenborough as Jacob
* Robert Torti as Pharaoh
* Ian McNeice as Potiphar
* Joan Collins as Mrs. Potiphar
* Christopher Biggins as Baker
* Alex Jennings as Butler
* Nicolas Colicos as Reuben
* Jeff Blumenkrantz as Simeon
* David J. Higgins as Levi
* Patrick Clancey as Issachar
* Shaun Henson as Napthali
* Martin Callaghan as Asher
* Sebastien Torkia as Dan
* Michael Small as Zebulun
* Peter Challis as Gad
* Nick Holmes as Benjamin
* Gerry McIntyre as Judah

== Musical Numbers ==

Prologue - Narrator

Any Dream Will Do - Joseph, Children

Jacob and Sons/Josephs Coat - Jacob, Joseph, Narrator, Brothers, Wives, Children, Ensemble

Josephs Dreams - Narrator, Brothers, Joseph

Poor, Poor Joseph - Narrator, Brothers, Children

One More Angel in Heaven - Reuben, Narrator, Brothers, Wives, Jacob, Children

Potiphar - Children, Narrator, Male Ensemble, Mrs Potiphar, Potiphar, Joseph

Close Every Door - Joseph, Children

Go, Go, Go Joseph - Narrator, Butler, Baker, Ensemble, Joseph, Guru, Children

Pharaohs Story - Narrator, Children

Poor, Poor Pharaoh - Narrator, Butler, Pharaoh, Children

Song of the King - Pharaoh, Ensemble

Pharaohs Dream Explained - Joseph, Ensemble, Children

Stone the Crows - Narrator, Pharaoh, Children, Joseph, Female Ensemble

Those Canaan Days - Simeon, Jacob, Brothers

The Brothers Come To Egypt/Grovel, Grovel - Narrator, Brothers, Joseph, Female Ensemble, Children

Whos the Thief? - Joseph, Brothers, Female Ensemble

Benjamin Calypso - Judah, Brothers (but Benjamin), Female Ensemble

Joseph All the Time - Narrator, Joseph, Children

Jacob in Egypt - Narrator, Jacob, Children, Ensemble

Any Dream Will Do (Reprise)/Give Me My Colored Coat - Joseph, Narrator, Children, Ensemble

Joseph Megamix - Ensemble

== Release ==
 Fathom Event.  The film was released by PolyGram Filmed Entertainment|PolyGram. 

== Reception ==

Reviews of the film were generally positive. The film has maintained an 83% approval rating on the film aggregate "Rotten Tomatoes."   The film has been described by PBS as being a "lively interpretation."  Michael Dequina, online film critic for "TheMovieReport.com", described the film as a "sweet, candy-colored confection for the entire family." 

==See also==
*List of films featuring slavery

== References ==

 

 