Before We Lose
{{Infobox film
| name           = Before We Lose 
| image          = Before We Lose Poster.jpg
| caption        = Temporary Poster 2
| director       = Cristhian Andrews
| producer       = Cristhian Andrews Ruya Koman Ranjeet S.Marwa
| writer         = Cristhian Andrews Ruya Koman
| starring       = Saadet Yuce Ener Bozoglu
| music          = Cristhian Andrews
| cinematography = 
| editing        = Cristhian Andrews
| studio         = Audacity Innovative Empire Motion Pictures
| distributor    = 
| released       =  
| runtime        = 17 minutes
| country        = United Kingdom United States
| language       = Turkish
}}
Before We Lose is a 2014 short film directed by Cristhian Andrews. The film is scheduled to premiere in Derry, Northern Ireland at the 2014 Foyle Film Festival.  

==Plot==
The short tells the story of a mother and her 3-year-old son and the turn their lives take with her deteriorating health condition. Based on recent findings in the Black Sea Region of Turkey and inspired by the article Chernobyl Health Effects in Turkey by Nilay Kar.  

==Cast==
* Saadet Yuce as the mother
* Ener Bozoglu as the son
* Funda Duval as Funda
* Ruya Koman as Buket

==References==
 

==External links==
* 

 