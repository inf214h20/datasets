Higher Than a Kite
{{Infobox Film |
  | name           = Higher Than a Kite |
  | image          = Highthanakite43LOBBY.jpg|
  | caption        = |
  | director       = Del Lord
  | writer         = Monte Collins Elwood Ullman |
  | starring       = Moe Howard Larry Fine Curly Howard Vernon Dent Dick Curtis Duke York|
  | cinematography = John Stumar | 
  | editing        = Paul Borofsky |
  | producer       = Del Lord Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 17 28" |
  | country        = United States
  | language       = English
}}

Higher Than a Kite is the 72nd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges want to fly for the Royal Air Force, but end up as mechanics working in a motor-pool garage. When given the assignment of getting a squeak out of the Colonels car from his assistant Kelly (Duke York), they get sidetracked after Moes head gets stuck in a pipe. After several painful attempts, they finally unscrew Moe from the tight quarters, and he eventually chases Larry and Curly around the vehicle, breaking the windshield in the process. The Stooges disassemble the entire engine, and are still puzzled, as they are not exactly certain what a squeak looks like. Kelly comes to retrieve the Colonels car, with the Stooges still hoping to be airmen.
  repair are made in Higher Than a Kite.]]

The trio promptly evacuate the garage after Kelly realizes what they have done, only to end up hiding out in a bomb mistaken for a sewer pipe. The bomb is then dropped behind enemy lines (reflecting the recent British bombing of  ) and Boring (Vernon Dent) - parodies of German generals Erwin Rommel and Hermann Goering - then enter, and go about flirting with Moronica. The Stooges eventually steal enemy secrets from under the nose of the Nazi officers, knock them cold, and escape. During their escape, a photo of Adolf Hitler gets stuck on Curlys behind. A bulldog wearing a "United States Marine Corps|U.S. Marines" coat and helmet runs in and bites Curly where Hitlers photo is, and Curly runs off with the bulldog still hanging from his hindquarters.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 