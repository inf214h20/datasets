Wax Works
{{Infobox Hollywood cartoon|
| cartoon_name = Wax Works
| series = Oswald the Lucky Rabbit
| image = WaxworksOswald.jpg
| caption = The baby desperately wants to be with Oswald. Bill Nolan
| story_artist = Walter Lantz Bill Nolan
| animator = Manuel Moreno George Grandpre Lester Kline Verne Harding Fred Kopietz Victor McLeod
| voice_actor =
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = June 25, 1934
| color_process = Black and white
| runtime = 8 min. English
| preceded_by = Annie Moved Away
| followed_by = William Tell
}}

Wax Works is an animated short subject by Walter Lantz, and features Oswald the Lucky Rabbit.

==Plot==
One evening, a penniless woman in an old hood is walking on the street, carrying a basket with a baby in it. She then lays it by the door of a wax museum owned by Oswald. The woman knocks on the door and leaves. Oswald opens up and sees whats in front of him. In doubt that he would make a good caretaker, however, the rabbit is reluctant to take the child in, and therefore goes back inside. But before the door closes, the baby, who is a boy, climbs out of the basket and enters the place.

To his surprise, Oswald finds the baby boy indoors. He then goes on walking around, wondering what he should do. But when the child clings onto his leg and asks to be accepted, Oswald changes his mind. As it gets late that night, Oswald goes to sleep, sharing his bed with his new little brother figure. The baby boy, however, isnt sleepy and decides to have a little tour of the museum.

While wondering the museums hallways, the baby boy finds part of his pyjamas opened. He then asks some statues to close it for him. After one of them provides assistance, that statue decides to show the little sightseer around. Thus all the other wax characters in the area come to life and go into a celebration by singing and dancing. It is a beautiful experience.

The baby boy walks further in the museum and into another section. Unlike the ones he met previously, however, the statues there are hideous and hostile. They want nothing more than to torment anyone who steps into their abode. To defend himself, the baby boy grabs a blow torch, liquifying some of the wax monsters. Despite the childs advantage, the wax monsters are able to get close enough and take his weapon away. They then force their victim onto a platform and begin to pour molten wax on him. Back in the bedroom, Oswald hears the baby boys cries for help, and makes the run. But by the time the rabbit reaches the location, it is too late, and all thats left on the platform is a wax relic in the shape of an infant. Oswald is then caught by the wax monsters to suffer a similar fate.

It turns out all that trouble was in Oswalds dream. Finally waking up in his bed, Oswald is relieved to see the baby boy completely unscathed. He is then asked by the child to button the rear part of the latters jammies.

==Availability==
The short is available on   DVD box set. {{cite web
|url=http://lantz.goldenagecartoons.com/1934.html
|title=The Walter Lantz Cartune Encyclopedia: 1934
|accessdate=2012-01-11
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==See also==
*Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*   at the Big Cartoon Database
* 

 

 
 
 
 
 
 
 
 
 
 