The Human Stain (film)
{{Infobox film
| name           = The Human Stain
| image          = HumanStainPoster.jpg
| caption        = Theatrical release poster
| director       = Robert Benton
| producer       = Tom Rosenberg Gary Lucchesi Scott Steindorff Bob Weinstein Harvey Weinstein
| screenplay     = Nicholas Meyer 
| based on       = The Human Stain by Philip Roth
| starring       = Anthony Hopkins Nicole Kidman Gary Sinise
| music          = Rachel Portman
| cinematography = Jean-Yves Escoffier
| editing        = Christopher Tellefsen
| studio         = Lakeshore Entertainment
| distributor    = Miramax Films
| released       = October 31, 2003
| runtime        = 106 minutes
| country        = United States Germany France
| language       = English
| budget         = $30 million
| gross          = $19,379,387
}} 2000 novel of the same name by Philip Roth. The film stars Anthony Hopkins and Nicole Kidman.

==Plot == racist remarks in class. Colemans wife died suddenly following the scandal, and he wants to avenge his loss of career and companion by writing a book about the events with Nathans assistance.
 stalker ex-husband Lester (Ed Harris), a mentally unbalanced Vietnam War veteran who blames her for the deaths of their children in an accident. Flashbacks of Colemans life reveal to the audience his secret—he is an African American who has passed as a Jewish man for most of his adult life.

==Cast==
* Anthony Hopkins as Coleman Silk
* Nicole Kidman as Faunia Farley
* Gary Sinise as Nathan Zuckerman
* Ed Harris as Lester Farley
* Wentworth Miller as Young Coleman Silk
* Jacinda Barrett as Steena Paulsson
* Mimi Kuzyk as Delphine Roux
* Clark Gregg as Nelson Primus
* Anna Deavere Smith as Dorothy Silk
* Phyllis Newman as Iris Silk
* Mili Avital as Young Iris
* Harry Lennix as Clarence Silk
* Tom Rack as Bob Cat
* Lizan Mitchell as Ernestine Silk 
* Danny Blanco-Hall as Walter Silk

==Release==
The film debuted at the Venice Film Festival. It was shown at the Toronto Film Festival, the Bergen International Film Festival, and the Hollywood Film Festival before its release in the US.

===Box office===
The film grossed $5,381,227 in the US and $13,998,160 in foreign markets for a total worldwide box office of $19,379,387. 

===Critical reception===
Review aggregator Rotten Tomatoes reports that 41% of 148 professional critics gave the film a positive review, with a rating average of 5.5 out of 10. The sites consensus is that "Though the acting is fine, the leads are miscast, and the story is less powerful on screen than on the page." 

In his review in the New York Times, A.O. Scott called it "an honorable B+ term paper of a movie: sober, scrupulous and earnestly respectful of its literary source . . . The filmmakers explicate Mr. Roths themes with admirable clarity and care and observe his characters with delicate fondness, but they cannot hope to approximate the brilliance and rapacity of his voice, which holds all the novels disparate elements together. Without the active intervention of Mr. Roths intelligence . . . the story fails to cohere . . . At its best - which also tends to be at its quietest - The Human Stain allows you both to care about its characters and to think about the larger issues that their lives represent. Its deepest flaw is an inability to link those moments of empathy and insight into a continuous drama, to suggest that the characters lives keep going when they are not on screen." 
 Greek gods and comic book heroes: We learn their roles and powers at the beginning of the story, and they never change. Here are complex, troubled, flawed people, brave enough to breathe deeply and take one more risk with their lives." 

In the San Francisco Chronicle, Mick LaSalle called it "a mediocre movie . . .   falls victim to a fatal lack of narrative drive, suspense and drama. Kidman and Hopkins are wrong for their roles, and that, combined with a pervading inevitability, cuts the film off from any sustained vitality. The result is something admirable but lifeless." 

David Stratton of Variety (magazine)|Variety described it as "an intelligent adaptation of Philip Roths arguably unfilmable novel powered by two eye-catching performances . . . A key problem Benton is unable to avoid is that Hopkins and Miller dont look (or talk) the least bit like one another. Miller, who gives a strong, muted performance, convinces as a light-skinned African-American in a way Hopkins never does, which is not to suggest that the Welsh-born actor doesnt give another intelligent, powerful portrayal. Its just that the believability gap looms large." 

In Rolling Stone, Peter Travers said, "Hopkins and Kidman . . . are both as mesmerizing as they are miscast . . . The Human Stain is heavy going. Its the flashes of dramatic lightning that make it a trip worth taking." 

The Times of London called it "sapping and unbelievable melodrama . . . an unforgivably turgid lecture about political correctness." 

==Awards and nominations== American Film Institute Award for Best Movies of 2003 (winner) Washington D.C. Area Film Critics Association Award for Best Supporting Actress (Anna Deavere Smith, winner) Black Reel Award for Best Supporting Actress in a Motion Picture (Smith, winner) Black Reel Award for Best Actor in a Motion Picture (Wentworth Miller, nominee) Black Reel Award for Best Breakthrough Performance (Miller, nominee)

==Soundtrack==

The soundtrack to The Human Stain was released on September 23, 2003.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 35:02 

| title1          = Opening Credits
| length1         = 3:11
| extra1          = Rachel Portman

| title2          = Iris Dies/Library Coleman Waits for Faunia
| length2         = 2:29
| extra2          = Rachel Portman

| title3          = Its in the Mail/End Credits
| length3         = 7:03
| extra3          = Rachel Portman

| title4          = The Two Urns/Father Dies
| length4         = 2:31
| extra4          = Rachel Portman

| title5          = Navy Recruiting
| length5         = 1:01
| extra5          = Rachel Portman

| title6          = Steena Rejects Coleman
| length6         = 1:28
| extra6          = Rachel Portman

| title7          = Audobon Society/The Crow
| length7         = 2:35
| extra7          = Rachel Portman

| title8          = Colemans Funeral/Faunia Dances
| length8         = 1:14
| extra8          = Rachel Portman

| title9          = The Accident
| length9         = 2:46
| extra9          = Rachel Portman

| title10         = You Think Like a Prisoner
| length10        = 2:05
| extra10         = Rachel Portman

| title11         = Frozen Lake
| length11        = 1:36
| extra11         = Rachel Portman

| title12         = Its in the Mail/End Credits (Rewrite)
| length12        = 7:03
| extra12         = Rachel Portman

}}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 