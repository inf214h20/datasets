Neighborhood House (film)
{{Infobox film
| name           = Neighborhood House
| image          = 
| alt            = 
| caption        = 
| director       = Charley Chase Harold Law Alan Hale, Sr.
| producer       = Hal Roach
| screenplay     = Charley Chase Harold Law Richard Flournoy Arthur Vernon Jones
| starring       = Charley Chase Rosina Lawrence Darla Hood George Meeker Ben Taggart Dick Elliott
| music          = 
| cinematography = Art Lloyd
| editing        = Ray Snyder 
| studio         = Hal Roach Studios
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Neighborhood House is a 1936 American comedy film directed by Charley Chase, Harold Law and Alan Hale, Sr. and written by Charley Chase, Harold Law, Richard Flournoy and Arthur Vernon Jones. The film stars Charley Chase, Rosina Lawrence, Darla Hood, George Meeker, Ben Taggart and Dick Elliott. The film was released on May 9, 1936, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Charley Chase as Charley Chase
*Rosina Lawrence as Rosina Chase
*Darla Hood as Mary Chase
*George Meeker as Adolph
*Ben Taggart as Cop
*Dick Elliott as Perkins 
*Harry Bowen as Irate Movie Patron 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 