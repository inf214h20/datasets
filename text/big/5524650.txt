Hands of the Ripper
 
 
 
{{Infobox film
| name = Hands of the Ripper
| image = Hands of the rippermp.jpg
| caption = Promotional movie poster for the film 
| director = Peter Sasdy
| producer = Aida Young
| writer = L.W. Davidson Edward Spencer Shew
| starring = Eric Porter Angharad Rees Jane Merrow Keith Bell Derek Godfrey
| music = Christopher Gunning
| cinematography = Kenneth Talbot
| editing = Chris Barnes
| studio  = Hammer Film Productions
| distributor = Universal Pictures
| released = 3 October 1971 (United Kingdom|UK)
| runtime = 85 min. English
| budget =
}} 1971 United British horror film directed by Peter Sasdy for Hammer Film Productions.

==Plot==
The infant daughter of Jack the Ripper is witness to the brutal murder of her mother by her father. Fifteen years later she is a troubled young woman who is seemingly possessed by the spirit of her late father. While in a trance she continues his murderous killing spree but has no recollection of the events afterwards. A sympathetic psychiatrist takes her in and is convinced he can cure her condition. However, he soon regrets his decision...
==Cast==
*Eric Porter as Dr. John Pritchard
*Angharad Rees as Anna
*Jane Merrow as Laura
*Keith Bell as Michael Pritchard
*Derek Godfrey as Mr. Dysart
*Dora Bryan as Mrs. "Granny" Golding
*Marjorie Rhodes as Mrs. Bryant
*Lynda Baron as Long Liz
*Marjie Lawrence as Dolly, the maid
*Margaret Rawlings as Madame Bullard
*Elizabeth MacLennan as Mrs. Wilson 
*Barry Lowe as Mr. Wilson
*April Wilding as Catherine
==Production==
 
The film also stars Jane Merrow, Keith Bell and Derek Godfrey. The film was an early starring role for Angharad Rees. Later 
in the 1970s, she appeared with Robin Ellis, Ralph Bates and an all star cast in the BBC TV costume drama Poldark.

It was filmed at Pinewood Studios, with some location work at St. Pauls Cathedral, London.

== Critical reception ==
 

The Hammer Story: The Authorised History of Hammer Films wrote that the film "expertly mixes the sophistication expect of Hammers films with the gore its new audiences demanded." 
== References ==

 

; Sources

*  

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 


 