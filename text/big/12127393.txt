The Bong Connection
{{Infobox film
| name           = The Bong Connection
| image          = The Bong Connection.jpg
| image_size     =
| caption        =
| director       = Anjan Dutt
| producer       = Joy Ganguly
| writer         = 
| narrator       = 
| starring       = Raima Sen Shayan Munshi Parambrata
| music          = Neel Dutta
| cinematography = 
| editing        =
| distributor    = Moviex Entertainment
| released       =  
| runtime        =
| country        = India English
| budget         =
| gross          =
}}
 
The Bong Connection is a 2006 English-language Indian film starring Raima Sen, Shayan Munshi and Parambrata Chatterjee and directed by Anjan Dutt. The movie is based on the lives of Bengalis in the United States|U.S. and Kolkata.

==Plot==
Two young men, Apu (Parambrata Chatterjee) and Andy (Shayan Munshi) are trying to achieve their dreams. Apu is a Bengali who has lived in Kolkata all his life and works for an IT company. He has dreams of going to the US and making it big there. He gets his chance when a relative offers him a job in Houston, Texas. As he is leaving he promises his girlfriend, Sheela (Raima Sen) that he will come back soon and take her to the US.

Andy, a second-generation Bengali born and brought up in New York, has dreams of becoming a musician and working in films. He comes to Kolkata to pursue his passion and stays at the house of his paternal uncle and grandfather. In Kolkata he meets Sheela, befriends her and falls in love with her.

Meanwhile Apu, arrives in the US and struggles to find his way in an alien nation. In the process he befriends Rita (played by Peeya Rai Chowdhary) whose parents want to get her married to Apu. The rest of the movie revolves around the socio-comic adventures of Apu and Andy as they struggle to find their calling in life.

The movie featured an appearance by Kolkata alt-rock band Cassinis Division.

==DVD release==
DVD has released in feb08. 

==Cast==
*Shayan Munshi as Andy
*Parambrata Chatterjee as Apu
*Raima Sen as Sheela
*Peeya Rai Chowdhary as Rita
*Victor Banerjee
*Soumitra Chatterjee
*Mamata Shankar
*Biswajit Chakraborty
*Saswata Chatterjee
*Samrat Chakrabarti
*Shauvik Kundagrami
*Mir Afsar Ali
*June Malia

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 