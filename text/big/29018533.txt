The Rejuvenation of Aunt Mary
 
{{Infobox film
| name           = The Rejuvenation of Aunt Mary
| image          = 
| caption        = Betty Brown
| director       = Erle C. Kenton Edmond F. Bernoudy (2nd Unit)
| producer       = John C. Flinn Anne Warner (play) Zelda Sears Anthony Coldeway (adaptation) Raymond Cannon (scenario) Harrison Ford
| music          =
| cinematography = Barney McGill
| editing        =
| distributor    = Producers Distributing Corporation
| released       =  
| runtime        = 60 minutes
| country        = United States Silent (English intertitles)

}}
The Rejuvenation of Aunt Mary (1927 in film|1927) is a lost  silent comedy starring veteran actress May Robson and released by Cecil B. DeMilles Producers Distributing Corporation (PDC). 

Robson had first appeared in the Broadway play version of this story in 1907 when she was 49.  In this film she returns to enact the same part 20 years later at the age of 69. A previous 1914 movie version of the play had been produced minus May Robson.

==Cast==
* May Robson - Aunt Mary Watkins Harrison Ford - Jack Watkins
* Phyllis Haver - Martha Rankin
* Franklin Pangborn - Melville
* Robert Edeson - Judge Hopper
* Arthur Hoyt - Gus Watkins
* Betty Brown - Alma

==References==
 

==External links==
* 
* 
* 
*posters to the film:  ... 
 

 

 
 
 
 
 
 
 
 


 