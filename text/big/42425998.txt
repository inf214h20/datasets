I Live Again
{{Infobox film
| name =   I Live Again
| image =
| image_size =
| caption =
| director = Arthur Maude
| producer =G.B. Morgan  
| writer =  John Quin 
| narrator =
| starring = Noah Beery   Bessie Love   John Garrick   Vi Kaley
| music = 
| cinematography = Horace Wheddon 
| editing = 
| studio = G.B. Morgan Productions 
| distributor = National Provincial Film Distributors
| released = November 1936 
| runtime = 74 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
I Live Again (alternate title: Live Again) is a 1936 British musical film directed by Arthur Maude and starring Noah Beery, Bessie Love, and John Garrick. It was made at Elstree Studios. 

==Cast==
*  Noah Beery as Morton Meredith
* Bessie Love as Kathleen Vernon
* John Garrick as John Wayne
* Vi Kaley as Jane Bloggs
* Stan Paskin as Joshua Bloggs Cecil Gray as Vivian Turnbull
* Pamela Randall as Grace Felton
* Lynwood Roberts as Major Cannon Frank Stanmore as Magistrate

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 