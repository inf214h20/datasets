Lounge Chair (film)
{{Infobox film
| name           = Lounge Chair
| image          = La méridienne poster.jpg
| caption        = 
| director       = Jean-François Amiguet
| producer       = Marie-Pascale Osterrieth Jean-Louis Porchet Gérard Ruey
| writer         = Anne Gonthier Jean-François Goyet
| starring       = Jérôme Anger
| music          = 
| cinematography = Emmanuel Machuel
| editing        = Elisabeth Waelchli
| distributor    = 
| released       = 15 June, 1988
| runtime        = 80 minutes
| country        = Switzerland France
| language       = French
| budget         = 
}}
 1988 cinema French drama film directed by Jean-François Amiguet. It was screened in the Un Certain Regard section at the 1988 Cannes Film Festival.   

==Cast==
* Jérôme Anger - François
* Kristin Scott Thomas - Marie
* Sylvie Orcier - Marthe
* Patrice Kerbrat - Dubois, le détective
* Alice de Poncheville - Léa
* Judith Godrèche - Stéphane
* Michel Voïta - Le libraire
* Jean Francois Aupied - Narrator
* Véronique Farina - Fleuriste

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 