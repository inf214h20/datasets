Milap
 
{{Infobox film
| name           = Milap
| image          = Milap.gif
| image_size     =
| caption        = 
| director       = B.R. Ishara 
| producer       = 
| writer         =
| narrator       = 
| starring       = Shatrughan Sinha and Danny Denzongpa
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1972
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1972 Bollywood action film directed by B.R. Ishara . The film stars Shatrughan Sinha and Danny Denzongpa.

==Cast==
*Shatrughan Sinha ...  Ravi / Raju 
*Reena Roy ...  Rani Chalava / Rukmani 
*Danny Denzongpa ...  Raju 
*Manmohan Krishna ...  Judge 
*Fazlu ...  Raghunath   Sarita ...  Radha

Similar movie to the one filmed by Basu Chatterjee in the same year. 

==Music==
The film has a popular song "Kai Sadiyon Se, Kai Janmo Se" sung by Mukesh (singer)|Mukesh.

==External links==
*  

 
 
 
 


 
 