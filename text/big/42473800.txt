List of Telugu films of 1974
 
This is a list of Cinema of Andhra Pradesh|Telugu-language films produced in the year 1974.

{| class="wikitable sortable"
|-
! Title !! Director !! Cast !! Genre !! Notes
|-  Sharada || ||
|- Bharathi || ||
|-
|Ammayi Pelli || P. Bhanumathi || N. T. Rama Rao, P. Bhanumathi || ||
|- Lakshmi || ||
|- Bharathi || ||
|- Alluri Seetarama Gattamaneni Krishna, This film is biographical film of Alluri Sitaramaraju.   
|-  Lakshmi || ||
|-
|Bantrothu Bharya || Dasari Narayana Rao || Krishnam Raju, Chalam, Vijaya Nirmala || ||
|-
|Chakravakam (1974 Telugu film)|Chakravakam || V. Madhusudhan Rao || Sobhan Babu, Vanisri, Chandrakala || ||
|-
|Dhanavanthulu Gunavanthulu || K. Varaprasad || Krishna (actor) |Krishna, Vijaya Nirmala || ||
|- Jamuna || ||
|- Jamuna || ||
|- Devadasu || Vijaya Nirmala || Krishna (actor) |Krishna, Vijaya Nirmala || ||
|- Manjula || ||
|- Raja Baby, Roja Ramani || ||
|-
|Gali Patalu || T. Rama Rao || Krishna (actor) |Krishna, Vijaya Nirmala || ||
|- Gowri || Jamuna || ||
|- Kanta Rao, Jyothi Lakshmi || ||
|- 
|Intiniti Katha || K. Satyam || Krishna (actor) |Krishna, Chandrakala || ||
|- Latha || Aradhana
|-
|Khaidi Babai || T. Krishna || Sobhan Babu, Vanisri || ||
|- Lakshmi || ||
|-
|Krishnaveni (film) |Krishnaveni|| V. Madhusudhan Rao ||  Krishnam Raju, Vanisri || || This film is a film about a women suffering from hysteria. 
|- Kanchana || ||
|- Manjula || ||
|-
|Manushullodevudu || G. V. Prasad || N. T. Rama Rao, Vanisri || ||
|- Jamuna || ||
|- Latha || Zanjeer
|- Kanta Rao, Chandramohan (actor) |Chandramohan, Roja Ramani || ||
|- Jamuna || ||
|- Manjula || ||
|- Lakshmi || ||
|- Sharada || ||
|-
|Satyaniki Sankellu || K. S. Prakash Rao || Krishna (actor) |Krishna, Vanisri || ||
|-
|Taatammakala || N. T. Rama Rao || N. T. Rama Rao, P. Bhanumathi || ||
|- Sharada || || 
|-
|Uthama Illalu || P. Sambasiva Rao || Krishna (actor) |Krishna, Chandrakala || ||
|}

==References==
 
 
 
 

 
 
 
 