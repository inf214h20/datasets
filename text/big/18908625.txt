The Square Ring
 
{{Infobox film
| name           = The Square Ring
| image          = "The_Square_Ring"_(1953).jpg
| image_size     = 150px
| caption        = 
| director       = Basil Dearden
| producer       = Michael Relph
| writer         = Alec Grahame
| based on       =  
| narrator       =  Jack Warner Bill Owen
| music          = Dock Mathieson
| cinematography = Otto Heller
| studio         = Ealing Studios
| editing        = Peter Bezencenet GFD  
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 British film Jack Warner, Bill Owen. Ralph Peterson,       centers on one night at a fairly seedy boxing venue and tells the different stories of the various fighters and spectators.

==Plot==
Five stories that take place mainly in the locker room prior to and after various bouts during a single evening at a cheap boxing stadium. These include a novice cheated of victory, a boxer who refuses to "throw" a fight, and a veteran trying to make a come-back who pays with his life.  

==Cast== Jack Warner as Danny Felton  
* Robert Beatty as Jim Kid Curtis   Bill Owen as Happy Burns  
* Maxwell Reed as Rick Martell   George Rose as Whitey Johnson  
* Bill Travers as Rowdie Rawlings  
* Alfie Bass as Frank Forbes   Ronald Lewis as Eddie Lloyd  
* Sid James as Adams  
* Joan Collins as Frankie  
* Kay Kendall as Eve Lewis  
* Bernadette OFarrell as Peg Curtis  
* Eddie Byrne as Lou Lewis   Michael Golden as Warren  
* Joan Sims as Bunty
* Sydney Tafler as 1st Wiseacre 
* Alexander Gauge as 2nd Wiseacre

==Production== Ralph Peterson who arrived in London only the year before. Canadian actor Robert Beatty played the lead; he had no boxing experience so he trained for two weeks in preparation for the role. 

==Reception==
Critical reception was mixed. One review called the film "uneven", accusing it of "veering between comedy and tragedy". 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 