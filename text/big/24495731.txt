La Piscine (film)
 
{{Infobox film
| name           = La Piscine
| image          = LaPiscinePoster.jpg
| image size     = 
| alt            = 
| caption        = French film poster
| director       = Jacques Deray
| producer       = 
| writer         = Jacques Deray Jean-Claude Carrière
| based on       =   under the pseudonym Jean-Emmanuel Conil}}
| narrator       = 
| starring       = Alain Delon Romy Schneider Maurice Ronet Jane Birkin
| music          = Michel Legrand 
| cinematography =  
| editing        =  
| studio         = 
| distributor    = Avco Embassy (US) 
| released       = France:   Italy:  
| runtime        = 120 minutes
| country        = France Italy
| language       = French
| budget         = 
| gross          = 2,341,721 admissions (France) 
| preceded by    = 
| followed by    = 
}}

La Piscine (The Swimming Pool)  is a 1969 Italian-French film directed by Jacques Deray, starring Alain Delon, Romy Schneider, Maurice Ronet and Jane Birkin.

Set in summertime on the French Riviera|Côte dAzur, it is a drama of sexual jealousy and possessiveness. French and English-language version of the film have been made, which was unusual at a time when movies were always either dubbed or subtitled. It means that the actors have been filmed speaking English for the international version. That 114 minutes version, shorter than the French version, also offers a slightly different editing than the French version.

== Plot ==
Jean-Paul, a writer and Marianne, his girlfriend of just over two years, are holidaying at a friends villa. There is a tension in their relationship which excites Marianne: the film begins with a scene in which they are together beside the villas swimming pool and she urges him to claw her back. He does as she asks, but then throws her into the pool and jumps in after her. In a later scene he takes a branch and uses it to lash her bare buttocks, playfully but with a force that increases as the scene cuts away.

Harry, an old friend and record producer, arrives for a visit, surprising the couple by bringing his 18-year-old daughter Penelope of whose existence they have not previously known. Before Jean-Paul knew Marianne, Harry was her lover.

The four stay together. As the days go by, Harry draws Marianne back towards him. He taunts Jean-Paul for having given up serious writing to work in advertising and drinks a great deal, throwing a surprise party while Jean-Paul, a recovering alcoholic, stays sober. Meanwhile it becomes clear that Penelope neither likes nor respects her father, whom she has barely known while growing up. She and Jean-Paul become close. They spend a day alone together by the sea; what happens there is left unshown, but if their relationship has not yet become sexual, clearly it soon will.

That night, while the women are asleep, the two men finally confront each other. Harry falls into the pool and is too drunk to swim. Jean-Paul, who has also been drinking, at first stops him from climbing out of the water, then deliberately pushes him under and holds him down till he is drowned. He covers up the crime by hiding Jean-Pauls wet clothes, making it look like an accident.

After the funeral, a policeman, Inspector Lévêque, visits the house more than once. He confides to Marianne his reasons for doubting the story of an accident. She tells Jean-Paul; when he confesses everything to her, she goes to see the evidence that would have given him away. But she does not give it to the police, and the inquiry is dropped.

Penelope returns to her mother. Marianne takes her to the airport and sees her off. She and Jean-Paul are then about to leave the villa when she tells him that they will not go together. She is calling a taxi when he places his hand on the telephone, cutting off her call and silencing her. In the end, neither leaves that day; in the films last shot stand are side by side. They look out through the window at the swimming pool, and then embrace.

==Cast==
*Romy Schneider as Marianne
*Alain Delon as Jean-Paul
*Maurice Ronet as Harry Lannier
*Jane Birkin as Penelope Lannier
* Paul Crauchet as Inspector Lévêque

==Production==
Delon and Schneider had been real life partners from 1958 to 1963, and it was he who presented her posthumous Honorary César in César Awards 2008|2008. Delon ended their six year romance by sending her a note "Je regrette". 

It was during the making of this film on 22 September 1968 that the dead body of Delons former bodyguard, Stephan Markovic, was discovered, ushering the Markovic Affair. 

==Reception==
It was the fourth most popular movie at the French box office in 1969. 

The movie was released in the UK as The Sinners to limited box office response. It was released in Italy with twenty minutes cut out, but was a popular success. 

===Critical reception===
The Los Angeles Times called it a "handsome, stunningly designed film" which was at its best in "the deft way in which it coolly depicts how beautiful, chic people, dedicated to a sophisticate, amoral view of love, can be utterly defenseless against an onslaught of passion – a favorite Gallic theme." 

==Remake==
In 2014, it was announced that a remake was in the works under the name A Bigger Splash with Luca Guadagnino directing and being written by David Kajganich, The film will star Dakota Johnson,Ralph Fiennes,Tilda Swinton, and Matthias Schoenaerts,  on February 19, 2015, it was announced the Fox Searchlight Pictures had acquired US film rights and are aiming for a 2015 release 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 