The Fatal Hour (1920 film)
{{Infobox film
| name           = The Fatal Hour
| image          = The Fatal Hour by George W. Terwilliger Film Daily 1920.png
| caption        = Wids Daily advert
| director       = George W. Terwilliger
| producer       = Metro Pictures Maxwell Karger
| writer         = Julia Burnham
| based on       = play by Cecil Raleigh
| starring       = Thomas W. Ross Bert Lytell
| cinematography = Louis J. Dunmyre
| editing        =
| distributor    = Metro Pictures
| released       = November 1, 1920
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
}}
The Fatal Hour is a lost  1920 American feature-length silent film directed by George W. Terwilliger. It starred Broadway star Thomas W. Ross (1873-1959) and Wilfred Lytell and was released by Metro Pictures. Ross is forgotten to modern audiences while Lytell is a known name amongst silent film buffs. 

==Cast==
*Thomas W. Ross - Jim Callender
*Wilfred Lytell - Nigel Villiers
*Frank Conlan - Lord Adolphus Villiers (credited as Francis X. Conlan)
*Lionel Pape - The Duke of Exmoor
*Jack Crosby - Dudley
*Henry Hallam - Anthony
*Louis Sealy - Felix (credited as Louis Sealey)
*Frank Currier - The Abbot
*Gladys Coburn - Dorothy Gore
*Thea Talbot - Bessie Bissett
*Jennie Dickerson - Mrs. Bissett
*Florence Court - Lily de Mario
*Marie Schaefer - Lady Margaret Villiers (credited as Marie Shaffer)
*Effie Conley - Sally

==References==
 

==External links==
 
*  
* 

 
 
 
 
 
 
 


 