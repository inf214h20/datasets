The War Against Mrs. Hadley
{{Infobox film
| name           = The War Against Mrs. Hadley
| image          = Jean Rogers in The War Against Mrs. Hadley trailer.jpg
| image_size     = 175px
| alt            =
| caption        = Jean Rogers in film
| director       = Harold S. Bucquet
| producer       = Irving Asher
| writer         = George Oppenheimer
| narrator       = Edward Arnold Fay Bainter David Snell
| cinematography = Karl Freund
| editing        = Elmo Veron
| studio         =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =$307,000  . 
| gross          = $1,357,000 
| preceded by    =
| followed by    =
}}
 Edward Arnold and Fay Bainter. Wealthy American society matron, Stella Hadley  (Fay Bainter) refuses to sacrifice her material comforts to aid the war effort until she realizes that her selfishness is cheating the boys overseas who are fighting for her freedom. 

== Plot summary ==
 

==Cast== Edward Arnold as Elliott Fulton
* Fay Bainter as Stella Hadley
* Richard Ney as Theodore Hadley
* Jean Rogers as Patricia Hadley
* Sara Allgood as Mrs. Michael Fitzpatrick
* Spring Byington as Cecilia Talbot
* Van Johnson as Michael Fitzpatrick
* Isobel Elsom as Mrs. Laura Winters
* Frances Rafferty as Sally
* Dorothy Morris as Millie
* Halliwell Hobbes as Bennett, The Butler
* Connie Gilchrist as Cook
* Stephen McNally as Peters (as Horace McNally)
* Miles Mander as Doctor Leonard V. Meecham
* Rags Ragland as Louie

==Reception==
According to MGM records the film made $695,000 in the US and Canada and $662,000 elsewhere, earning the studio a profit of $603,000. 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 