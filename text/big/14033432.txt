Manband the Movie
 
 2006 independent comedy film about pop music, reality television and age discrimination. Written and directed by Irish filmmaker Dermott Petty and produced by Mee Vaj and Lon Casler Bixby, the movie stars Pat Towne, Jonathan Mangum and Jaclyn Fjeslad.

Manband recently played at the Unfringed Festival in Limerick, Ireland and was nomated for Best Production.
 

==Plot==
 

ManBand! is the tale of two music promoters looking to find the next big thing in pop music for television. They inadvertently create a "manband", a boy band with middle-aged men. The television company follows and sabotages their progress.

The band, cruelly named ‘Parazone’ (a European brand of bleach), consists of five members:
*Barrelman (Michael Wallot), an opera singer;
*RS Smoothskin (Tim Murphy), who has boy band looks but no talent;
*Mild Bob (Kurt Scholler), a Tupperware salesman who mistakenly auditions thinking it to be a Tupperware convention;
*Bosco Barret (Kevin Kearns), an “Irish lunatic/Troubadour; and
*Bartender (Ed Bell), a bartender/crazed heavy metal singer.

Their choreographer, TT Bags, is a less than successful dancer and songwriter Lun E. Tune is a recently released mental patient who writes songs about maiming his old girlfriends.

==Background/Production==
 

Petty stated in an interview “One day at the day job I had to watch a commercial for fast food featuring a Boys Band. Just as I was about to give up on Western Civilization a thought occurred to me What if some Joker of Promoter had the zany idea of creating a Boysband with Middle Age Men, hence a ManBand!"
 SAG experimental movie shot on DV. Petty felt that going digital would serve the project best. He explains that “it was an aesthetic and practical decision giving ManBand! a more Reality TV look …besides we had no money.”

ManBand! was shot entirely in Orange County, California to make it appear that it was filmed in Los Angeles. Filming was done in a period of weekends to accommodate the cast and crew’s hectic schedules.

==Cast and crew information==
===Cast===
 

===Crew===
Filmmaker Dermott Petty is from Lisdoonvarna, County Clare, Ireland. His short film Paddy Takes A Meeting has been shown at film festivals worldwide and was featured in “The Scene,”  an exhibition at the Laguna Art Museum which showcases Orange County artists working in all mediums of art. Petty has also received a Drama-Logue critic’s award for Theatre Direction and a development grant from the Irish Film Board.

Mee Vaj came to the USA as a refuge from Laos. She is a USC graduate and also produced Paddy Takes A Meeting and Pipe Dreams.

Lon Casler Bixby is a native of Texas. Alongside his film projects, he is an award-winning photographer. He managed Edie Brickell & New Bohemians in their early days.

==Distribution==
It has screened at the Rat Powered Film Festival in Santa Ana, California, Chapman University in Orange, California and at the Glór Irish Music Centre in Ennis.

==Reception==
At its screening at Chapman University, The Orange County Register wrote an article about it saying “On Sunday Petty showed a rough cut of "ManBand" to 50 people at the Argyros Forum at Chapman University. The applause was deafening.”

==References==
 

==External links==
* 

 
 
 
 
 