Madea's Family Reunion
{{Infobox film
| name           = Madeas Family Reunion
| image          = Madeas Family Reunion.jpg
| caption        = Theatrical release poster
| director       = Tyler Perry
| producer       = Tyler Perry Reuben Cannon
| writer         = Tyler Perry
| starring       = Blair Underwood Keke Palmer Lynn Whitfield Lisa Arrindell Anderson Jenifer Lewis Rochelle Aytes Boris Kodjoe Tyler Perry
| music          = Tyler Perry
| cinematography = Toyomichi Kurita
| editing        = John Carter
| studio         = Tyler Perry Studios Reuben Cannon Productions Lionsgate
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English Spanish
| budget         = $6 million
| gross          = $63,308,879
}} stage production of the same name written by Tyler Perry and sequel to Diary of a Mad Black Woman. It was written and directed by Perry, who also played several characters, including Madea (Mabel Simmons)|Madea. It was released on February 24, 2006, nearly one year following its predecessor, Diary of a Mad Black Woman. The independent film was produced by Lions Gate Entertainment|Lionsgate.

==Plot==
  Madea (Tyler Perry), must contend with the other dramas on her plate, including the runaway who has been placed under her care and her troubled nieces, half-sisters Lisa and Vanessa. Her niece Lisa is engaged and has an abusive fiancé and informs her mother but is ignored due to her mothers greed.  Vanessa is trying to find love while struggling to forgive her mother for allowing her stepfather to rape her numerous times in her younger teen years.  Madea is on to help fix their problems and help them realize who they are.

==Cast==
* Blair Underwood as Carlos
* Lynn Whitfield as Victoria Breaux 
* Keke Palmer as Nikki Grady
* Tyler Perry as Brian, Joe, and Mabel Simmons (Madea)
* Lisa Arrindell Anderson as Vanessa Breaux 
* Rochelle Aytes as Lisa Breaux 
* Boris Kodjoe as Frankie Henderson 
* China Anne McClain as Tamara
* Henry Simmons as Isaac 
* Maya Angelou as Aunt May
* Jenifer Lewis as Milay Jenay Lori 
* Tangi Miller as Donna 
* Cicely Tyson as Myrtle 
* Johnny Gill as Wedding singer
* Cassi Davis as Aunt Sarah
* Georgia Allen as Aunt Ruby
* Nicholas Ortiz as Himself Judge Mablean Ephriam (cameo) as Herself
* David Wiebers as Wedding Musician (trumpet)

==Soundtrack==
The soundtrack was released by Motown Records on February 21, 2006.
{{Track listing
| headline        = Track listing
| extra_column    = Performer(s)
| writing_credits = no
| title1          = Find Myself in You
| extra1          = Brian McKnight
| length1         = 4:14
| title2          = Were Gonna Make It
| extra2          = LL Cool J and Mary Mary
| length2         = 4:54
| title3          = Keep Your Head Up
| extra3          = Chaka Khan
| length3         = 4:34
| title4          = Tonight Kem
| length4         = 3:55
| title5          = Everyday (Family Reunion) Carl Thomas, Yolanda Adams, and Gerald Levert
| length5         = 4:48
| title6          = Love and Happiness
| extra6          = Al Green
| length6         = 5:02
| title7          = You For Me (Wedding Song)
| extra7          = Johnny Gill
| length7         = 5:37
| title8          = Family Reunion
| extra8          = The OJays
| length8         = 6:55
| title9          = Ill Be
| extra9          = Will Downing
| length9         = 4:10
| title10         = Wounds in the Way
| extra10         = Rachelle Ferrell
| length10        = 4:21
}}

==Release and reception==

===Box office===
Madeas Family Reunion was budgeted at $6 million and opened at #1 in its opening weekend (2/24-26) with $30,030,661 http://www.boxofficemojo.com/weekend/chart/?yr=2006&wknd=08&p=.htm  and eventually grossed $63,257,940 in North America with an additional $50,939 internationally, tying $63,308,879 worldwide after 9 weeks in theaters. 

Small independent filmmaker Tyler Perry has garnered one of the highest wide-release openings to date in 2006, in both gross ($30 million) and screen average ($13,687). 

"The number one movie is Madeas Family Reunion, a small comedy/melodrama which grossed an astounding $30.3 million from 2,194 venues. It had a super-hot venue average of $13,787...." 

"Playing at 2,194 locations across North America, the film averaged a remarkable estimated $13,788 per screen, demonstrating the enormous breadth and depth of Perrys audience. The debut weekend of Madeas Family Reunion outperformed the opening weekend of Lionsgates first Tyler Perry film, Diary of a Mad Black Woman, by nearly 40 percent." 

===Audience===
The reasoning for the films relative success,  according to Steve Rothenberg, Lions Gate president of domestic distribution, is due to its targeted market appeal, "I believe that were in all the right theaters...Im not sure theres much room for expansion." http://www.boxofficemojo.com/news/?id=2013&p=.htm  To illustrate the point, Lions Gates exit polls  showed 52 percent of the audience were black women over the age of 35.

===Critical reception===
Madeas Family Reunion received mixed reviews from critics. Metacritic gives the film a score of 45% based on reviews from 18 critics. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 