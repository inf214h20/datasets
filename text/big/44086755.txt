Swarga Devatha
{{Infobox film
| name           = Swarga Devatha
| image          =
| caption        =
| director       = Charles Ayyampally
| producer       =
| writer         = Thoppil Bhasi
| screenplay     = Thoppil Bhasi Sharada K. Seema Sudheer Sudheer
| music          = M. S. Viswanathan
| cinematography =
| editing        =
| studio         = RC Combines
| distributor    = RC Combines
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Seema and Sudheer in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast== Sharada
*K. P. Ummer Seema
*Sudheer Sudheer
*Vincent Vincent

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambalathulasiyude parishudhi || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 2 || Krishnashilaathala Hrudayangale || S Janaki || Mankombu Gopalakrishnan || 
|-
| 3 || Kudumbam Oru Devaalayam || K. J. Yesudas || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 