The Stranger Within (1990 film)
{{Infobox film
| name           = The Stranger Within
| image          = The Stranger Within (1990 film).jpg
| image_size     =
| caption        = Tom Holland
| producer       = Barry Rosen Paulette Breen
| writer         = John Pielmeier
| narrator       = 
| starring       = Rick Schroder Kate Jackson
| music          = Vladimir Horunzhy
| cinematography = James Hayman
| editing        = Scott Conrad
| studio         =
| distributor    = CBS
| released       = November 27, 1990
| runtime        = 100 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        =
}} 1990 television thriller film Tom Holland, and starring Rick Schroder and Kate Jackson.

== Plot ==
In October 1974 rural Groves Mill, Minnesota, widow Mare Blackburns (Kate Jackson) 3-year-old son Luke (Ross Swanson) is kidnapped by an unknown woman when Mare briefly relaxes her supervision of Luke in the small towns supermarket. Mare blames herself for the kidnapping and has difficulty processing the loss. Sixteen years later, Mare is now in a happy long-distance relationship with Dan (Chris Sarandon) - a California man who moved to Minneapolis after the suicide of his son - when a young man named Mark (Rick Schroder) shows up, claiming to be her son. Mare is reluctant to take him in, fearing that he is playing a cruel joke, until he shares memories from the past and shows a scar from a stove burn that matches with Lukes. He explains that he was raised in Idaho, and is now on a rest stop in Minnesota before returning to New York to take a job. Mare is excited to catch up on sixteen years with her son, but Dan continues to doubt Marks credibility.

One day, Mare takes Mark to the supermarket where the kidnapping took place, but faints. In the hospital, Mark finds out that Mare is pregnant and is surprised that she does not want him to tell Dan. Dan continues to seek evidence that will prove whether or not Mark is actually Luke, despite Mares confession that she does not want to know whether Marks story proves to be either true or false. At the police station, Dan finds out that the city in Idaho where Mark claims to have grown up - "Emerald City" - does not exist. Upset with Mark, Dan reluctantly saves his life when he almost falls off the roof. Mark then claims that Emerald City was a lie told to cover up his much worse past, and assures Dan of his credibility by showing him a birth mole that Luke also had. Shortly after, Mare rejects the latest of several marriage proposals from Dan, who has just found out through Mark about her pregnancy.

During an ice fishing trip, Dan confronts Mark yet again about the lies about his past. Feeling trapped, Mark pushes Dan into the water and lets him drown to death under the ice. Mark then returns to the house and cuts the phone wire and electricity cable, as well as disabling the motor in Mares car, thus ensuring that Mare has no way to call for help and no other place to go. Mare is unaware of his responsibility in all these occurrences, though, and is further manipulated by Marks claim that Dan fell in the water due to his fight with her over their marriage views. A visit from a police officer changes her views on Mark, upon finding out that he did not inform the officer about Dans death. Mark turns the confrontation by insisting that she has been a horrible mother to him. The police officer, meanwhile, finds out that there are flaws to Marks claims.

As both realize that Mark is a sociopathic killer, they unsuccessfully try to get away. Mark first nearly kills the officer with a hammer, and then turns to Mare. She attempts to throw him out, but he easily overpowers her; knocking her head through a mirror before handcuffing her. Later, he releases her and gives her another chance "to be a good mother", only to attack her when she claims to love him. When she admits that she is a bad mother, Mark responds that she should die for it. Mare, who does not believe that Mark is her own son anymore, attempts to shoot him to death with the policemans pistol, but runs out of bullets. She is then pushed into the basement and awakes next to the police officers nearly lifeless body. While Mark is distracted while digging a hole to bury the two, the officer manages to weakly move his arm enough to indicate his bullet-belt, and so Mare retrieves the gun and reloads it. Mark, meanwhile, admits that he is not her son, before starting to bury her alive. To save her life, Mare shoots him. Mark survives the shooting and is about to kill her with a hammer, when suddenly the police officers father appears behind Mare and saves her by shooting Mark to death. Mare awakes in the hospital, by her mothers side.

==Cast==
*Rick Schroder as Mark
*Kate Jackson as Mare Blackburn
*Chris Sarandon as Dan
*Clark Sandford as Captain Bender
*Peter Breitmayer as Phil Bender
*Pamela Danser as Emma
*Ross Swanson as Luke Blackburn
*Dale Dunham as Milo
*Oliver Osterberg as Joe
*Kelsey Rose as Jill
*Zachary Hunke as Jason
*Suzanne Egli as Policewoman
*James Harris as Doctor

==Reception==
The New York Times called The Stranger Within "a frightening, well-done, made-for-TV thriller with a fine performance by Rick Schroder.   Schroder shows remarkable range in this change-of-pace performance where he plays an unsympathetic and unappealing character with great energy and intelligence. The Stranger Within is a fine, frightening psychological thriller." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 