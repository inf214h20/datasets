Sundays and Cybele
{{Infobox film
| name         = Sundays and Cybele
| image        =Original poster of Sundays and Cybele..jpg
| caption      = Original French poster 1962
| writer       = Serge Bourguignon Antoine Tudal Bernard Eschassériaux
| starring     = Hardy Krüger Nicole Courcel Patricia Gozzi
| music = Maurice Jarre
| director     = Serge Bourguignon
| producer     = Romain Pinès
| distributor  = Davis-Royal Columbia Pictures
| released     =  
| runtime      = 110 minutes
| country      = France
| language     = French
| budget       =
}}
 French Indochina War. The film is based on a novel by  , who collaborated on the screenplay.

== Plot ==
Pierre suffers from amnesia after a war-time accident in which he might have killed a young Vietnamese girl while crash-landing his stricken plane. His nurse, Madeleine, lives with him in a low-key but potentially romantic relationship. When Pierre sees Cybèle, a young girl in distress as her obviously loveless father is dropping her off at an orphanage, he befriends her.  Each of the two is lonely, childlike, and in need of a supportive friend. Eventually, he pretends to be the girls father, which allows her to escape the locked orphanage for a day, and he shares every one of his Sundays with her for months.

Pierre conceals his friendship with Cybèle from Madeleine, but she eventually finds out, and tells Bernard, a doctor who has a romantic interest in her. Bernard assumes the girl to be in danger, and notifies the police, who adopt the same assumption.

Pierre has nothing to give Cybèle for Christmas, so he accepts her facetious challenge to bring her the metal rooster from the top of a Gothic church near the orphanage. While Cybèle falls asleep, awaiting Pierre for their Christmas together in the snow-covered parks gazebo, the former pilot musters the nerve to climb the 300-foot steeple. With his knife as a tool to unscrew the rooster, he brings it down.  As he returns to Cybèle, with the metal rooster and his knife in his hands, the police arrive and shoot him dead to "protect" the child, whom they imagine to be in danger. Cybèle awakens to the horror of seeing that her friend is dead.

==Cast==
*Hardy Krüger as Pierre
*Nicole Courcel as Madeleine
*Patricia Gozzi as Cybèle/Françoise
*Daniel Ivernel as Carlos
*Andre Oumansky as Bernard

==Awards==
Sundays and Cybele won the 1962 Academy Award for Best Foreign Language Film.   

{| class="wikitable"
! Award
! Category
! Recipient and nominees
! Result
|- 35th Academy Academy Awards Best Foreign Language Film
| 
|- Best Music, Scoring of Music, Adaptation or Treatment Maurice Jarre
| 
|- Best Writing, Screenplay Based on Material from Another Medium Serge Bourguignon Antoine Tudal
| 
|- Blue Ribbon Awards Best Foreign Language Film Serge Bourguignon
| 
|- 20th Golden Golden Globes Samuel Goldwyn International Award 
| 
|- National Board of Review Best Foreign Language Film NBR Award
| 
|}

==See also==
* List of submissions to the 35th Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*   at The Criterion Collection

 
 

 
 
 
 
 
 
 
 
 