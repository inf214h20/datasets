Sargavasantham
{{Infobox film
| name           = Sargavasantham
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Anil Das
| producer       = Rasheed Kattungal
| story          = Kakkanadan
| screenplay     = Babu Pallassery Chippy Siddique Siddique Narendra Prasad Baiju Vanitha Babu Namboothiri
| music          = Ouseppachan
| cinematography = Ramachandra Babu
| editing        = K. Rajagopal
| studio         = Kattungal Films
| distributor    = Naaz Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
Sargavasantham (  is an Indian Malayalam film directed by Anil Das. It was the directorial debut of Anil Das who later did the science fiction film Bharathan in 2007.

==Plot==
The film portrays a superstitious Hindu family.

==Cast== Chippy as Abhirami Siddique as Roy
* Narendra Prasad as Dr. Sarathchandra Varma Baiju as Chandran
* Vanitha Krishnachandran
* Babu Namboothiri
* Jagathy Sreekumar as Kunjunni
* Jose Pellissery
* Rizabawa Seetha as Sathi Sudheer
* Zainuddin
* T. R. Omana
* Shammi Thilakan

==Credits==
* Story: Kakkanadan (originally published as Vadakku Ninnum Vanna Pakshi)
* Screenplay, Dialogues: sab john
* Direction: Anil Das
* Producer: Rasheed Kattungal
* Cinematography: Ramachandra Babu
* Editing: K. Rajagopal
* Banner: Kattungal Films
* Distribution: Naaz Release
* Art Direction: Mani Suchitra
* Music: Ouseppachan
* Lyrics: Kaithapram
* Music label: Magnasound
* Effects: Murukesh
* PRO: Abraham Lincoln
* Ads: Saboo Colonia
* Associate Director: Lal Jose

==External links==
*  
*   at the Malayalam Movie Database

 
 
 


 