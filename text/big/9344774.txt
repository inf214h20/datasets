Un Oso Rojo
{{Infobox film
| name           = Un Oso Rojo
| image          = Osorojoposter.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Israel Adrián Caetano
| producer       = Lita Stantic
| screenplay     = Israel Adrián Caetano Graciela Speranza
| story          = Romina Lafranchini
| starring       = Julio Chávez Soledad Villamil Luis Machín Agostina Lage
| music          = Mariano Barrella Diego Grimblat
| cinematography = Willi Behnisch
| editing        = Santiago Ricci
| studio         = Lita Stantic Producciones
| distributor    = Cinema Tropical
| released       =  
| runtime        = 95 minutes
| country        = Argentina France Spain
| language       = Spanish
| budget         = 
|}} French drama film, directed by Israel Adrián Caetano. 

The film was produced by Lita Stantic, and the screenplay was written by Caetano and Graciela Speranza, from the story penned by Romina Lafranchini. The picture features Julio Chávez, Soledad Villamil, Luis Machín, and Agostina Lage, among others.

==Plot==
Oso (Julio Chávez) is sent to prison for a robbery and a murder. After seven years, he is released from prison, and in a flashback, the robbery is seen that led to his arrest.  His daughter, Alicia (Agostina Lage), was a year old on the day of the robbery. As a consequence, Alicia never got a chance to get to know her father.
 getaway driver on one last big job. For her part, Alicia seems fascinated with her father and makes him promise never to go away again.

==Cast==
* Julio Chávez as Oso
* Soledad Villamil as Natalia
* Luis Machín as Sergio
* Agostina Lage as Alicia
* Enrique Liporace as Güemes
* René Lavand as Turco
* Daniel Valenzuela as Alfarito
* Freddy Flores as Truck

==Distribution==
The film was first featured at the Cannes Film Festival on May 23, 2002. It opened in Argentina on October 3, 2002, making the picture Caetanos second film that opened in Argentina in 2002 (the last was Bolivia (film)|Bolivia in April).

The film was also shown at various film festivals, including: the Gothenburg Film Festival, Sweden; the Havana Film Festival, Cuba; the Latin America Film Festival, Poland; the Lleida Latin-American Film Festival, Spain; the Cartagena Film Festival, Colombia; the Film by the Sea Film Festival, Netherlands; and the Helsinki International Film Festival, Finland.

In the United States, the picture opened at the Sundance Film Festival on January 17, 2003.  It also screened at the New Directors/New Films Film Festival, New York in March 2003; and the Milwaukee International Film Festival and Los Angeles in October 2004.

==Critical reception==
Film critic Neil Young thought the film was an engaging mix of crime thriller and family drama, and wrote, "  is nevertheless an effective, if minor, foray into a dusty, neglected corner of modern-day Buenos Aires. We can feel Argentinas well-documented financial problems starting to sour the whole countrys atmosphere, exerting unbearable pressures on the likes of Sergio, Oso and Natalia."  Young also thought director Caetano elicited solid work from his actors. 

A.O. Scott, film critic for The New York Times, also liked the film and wrote, "Its combination of toughness and smooth, understated style makes it touching and absorbing..." 
 Argentine daily La Nación wrote, "Un Oso Rojo imposes itself as a piece of great dramatic power that ratifies a prolific directors narrative talent." 

==Awards==
;Wins
* Argentine Film Critics Association Awards: Silver Condor Award; Best Actor, Julio Chávez; 2003.
* Lleida Latin-American Film Festival: Best Actor, Julio Chávez; 2003.
* Havana Film Festival: Best Music, Diego Grimblat; Special Jury Prize, Adrián Caetano; Special Mention, Lita Stantic; 2002.
 
;Nominations
* Cartagena Film Festival: Golden India Catalina; Best Film, Adrián Caetano; 2004.
* Argentine Film Critics Association Awards: Silver Condor Award; Best Actress, Soledad Villamil; Best Art Direction, Graciela Oderigo; Best Cinematography, Willi Behnisch; Best Director, Adrián Caetano; Best Editing, Santiago Ricci; Best Film; Best Music, Diego Grimblat; Best New Actor, René Lavan; Best New Actress, Agostina Lage; Best Original Screenplay, Adrián Caetano and Gabriela Speranza; Best Sound, Marcos De Aguirre; 2003.

==References==
 

==External links==
*  
*  
*  
*   at cinenacional.com  
*  

 
 
 
 
 
 
 
 
 
 
 