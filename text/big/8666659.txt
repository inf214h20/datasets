Da Hip Hop Witch
{{Infobox Film
| name           = Da Hip Hop Witch
| image          = Da Hip Hop Witch.jpg
| image_size     = 175px
| caption        = 
| director       = Dale Resteghini
| producer       = Robert A. Flutie   Kim Resteghini Dale Resteghini
| writer         = Dale Resteghini
| narrator       =  Vitamin C
| music          = Terence Dudley Rob Gay L.G. Tony Prendatt
| cinematography = Martin Ahlgren  Dale Resteghini
| editing        = Namakula
| distributor    = A-Pix Entertainment  Artisan Entertainment Barnholtz Entertainment
| released       = 2000
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Vitamin C and Vanilla Ice. 

==Plot==
After learning about the "Hip Hop Witch", a powerful supernatural being that lurks in the ghettos and attacks upcoming rappers which makes their record sales go up, five suburban teenagers go on a quest to get their rap careers started by being attacked by the said witch. Filming their experience, they run into past hip hop stars that have already battled the Witch in person.

==Cast==
*Dale Resteghini as Will Hunting
*Stacii Jae Johnson as Dee Dee Washington
*Amy Dorris as Raven (aka Rave Girl)
*Parker Holt as Jerry (aka Da Retard)
*Jordan Ashley as The Whisk
* Elijah Rhoades as Big Z

=== As themselves ===
* Eminem as himself Havoc as  himself Prodigy as himself
*Pras as himself
*Vanilla Ice as himself
*Ja Rule as  himself
*Rah Digga as herself Vitamin C as herself (credited as Vitamin-C)
*Charli Baltimore as herself
*Spliff Starr as himself
* Killah Priest as himself
*Mia Tyler as herself
* Royce da 59" as himself (credited as Royce 59")
* Outsidaz as themselves (credited as The Outsidaz)
* Severe as himself
* Mobb Deep as themselves
* Chris Simmons as himself

==Release==
Preceding the films release, Eminems lawyers attempted to have his scenes removed from the film,  and tried to halt its distribution.  In 2003, Artisan Entertainment planned to reissue the film on VHS and DVD with artwork prominently advertising Eminems appearance in the film.    Before the films reissue, Artisan recalled copies featuring this artwork without official explanation. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 