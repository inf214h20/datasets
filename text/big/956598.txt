The Haunting (1999 film)
{{Infobox film
| name           = The Haunting
| image          = The Haunting film.jpg
| caption        = The Haunting film poster
| director       = Jan de Bont Colin Wilson Susan Arnold
| screenplay     = David Self Michael Tolkin
| based on       =  
| starring       = Liam Neeson Catherine Zeta-Jones Owen Wilson Lili Taylor
| music          = Jerry Goldsmith
| cinematography = Karl Walter Lindenlaub Michael Kahn
| distributor    = DreamWorks Pictures
| released       =  
| runtime        = 114 minutes
| country        = United States United Kingdom
| language       = English
| budget         = United States dollar|$80,000,000
| gross          = $177,311,151
}} of the same name. Both films are based on the novel The Haunting of Hill House by Shirley Jackson, published in 1959. The Haunting was directed by Jan de Bont and stars Liam Neeson, Catherine Zeta-Jones, Owen Wilson and Lili Taylor. It was released in the United States on July 23, 1999.

==Plot== Tom Irwin) evict her. Nell receives a phone call about an insomnia study, directed by Dr. David Marrow (Liam Neeson) at Hill House, a secluded manor in the Berkshires of western Massachusetts, and applies for it.  At the house, she meets Mr. and Mrs. Dudley (Bruce Dern, Marian Seldes), a strange pair of caretakers. Two other participants arrive, Luke Sanderson (Owen Wilson), and Theodora (Catherine Zeta-Jones), along with Dr. Marrow and his two research assistants. Unknown to the participants, Dr. Marrow’s true purpose is to study the psychological response to fear, intending to expose his subjects to increasing amounts of terror. Each night, the caretakers chain the gate outside Hill House, preventing anyone from getting in or out until morning. During their first night, Dr. Marrow relates the story of Hill House. The house was built by Hugh Crain (Charles Gunning)—a 19th century|19th-century textile tycoon.
 killed herself ghostly apparitions of young children in the window curtains and the bedsheets, but the others dont believe her. Hugh Crains wood portrait morphs into a skeletal face and is vandalized with the words "Welcome Home Eleanor" written in blood. Theo and Luke try to establish their innocence, but Nell tells them that they dont know her.
 haunted by the souls of people victimized by Crains cruelty. She learns that Crain took children from his cotton mills and murdered them, then burned the bodies in the fireplace, trapping their spirits and forcing them to remain with him, providing him with an eternal family. She also learns that Crain had a second wife named Carolyn, from whom she is descended. Dr. Marrow is skeptical of Eleanors claims, until he realizes he made a horrible mistake by bringing them to Hill House when a statue tries to drown him in a pool of water in a greenhouse. After several more terrifying events, Nell insists that she cannot leave the souls of the children to suffer for eternity at Crains hands. Trying to convince the obviously mentally-unbalanced Eleanor to leave the house with them, Theo offers to let Nell move in with her, but Nell reveals her relation to Carolyn and claims she must help the children "pass on".

Hugh Crains spirit seals up the house, trapping them all inside. A frustrated Luke defaces a portrait of Hugh Crain. Crains enraged spirit drags Luke to the fireplace where he is Decapitation|decapitated. Nell is able to lead Crains spirit towards an iron door. Avenging spirits pull Crain into the door, dragging him down to Hell. Nell is pulled with him, inflicting fatal trauma on her body, but the spirits gently release her on the ground. Her soul rises up to Heaven, accompanied by the ghosts of Crains victims. After Nells death, Theo and Dr. Marrow wait by the gate outside until the Dudleys come in the morning.
 traumatized psychiatrist does not give an answer, and neither does Theo. When the gate opens, the two silently walk out and down the road, leaving Hill House behind them.

==Cast==
* Lili Taylor as Eleanor "Nell" Lance
* Liam Neeson as Doctor David Marrow
* Catherine Zeta-Jones as Theodora "Theo"
* Owen Wilson as Luke Sanderson
* Marian Seldes as Mrs. Dudley
* Bruce Dern as Mr. Dudley
* Alix Koromzay as Mary Lambetta
* Todd Field as Todd Hackett
* Virginia Madsen as Jane Lance Tom Irwin as Lou
* Charles Gunning as Hugh Crain

==Production==
 
  Rose Red, a television miniseries that shares many elements with Jacksons source novel, The Haunting of Hill House, and the character of the real-life edifice Winchester Mystery House, in San Jose, California.
 What Dreams May Come - 1998) oversaw the set designs.

The CGI was done by Tippett Studio and Industrial Light and Magic.

==Filming==
Harlaxton Manor, in England, was used as the exterior of Hill House. The billiard room scene was filmed in the Great Hall of the manor, while many of the interior sets were built inside the dome-shaped hangar that once housed The Spruce Goose, near the permanently docked RMS Queen Mary steamship, in Long Beach, California. The kitchen scenes were filmed at Belvoir Castle.

==Critical reception==
The Haunting was panned upon its release, with most critics citing its weak screenplay, its overuse of horror clichés, and its overdone  , July 1, 1999 

The film was not a financial success, earning $91.2 million domestically and $85.9 million outside North America.    With only half of all box office receipts going back to the studio (the rest is given to theater owners),  the film made about $88.5 million. This covered its $80 million budget,  allowing the studio to barely break even on the budget,  but did not cover its $10 million domestic TV advertising campaign  or other costs (such as international advertising, non-TV advertising, percentage payments to actors or crew, or prints). Daily Variety noted that, as of 1999, The Haunting had the "dubious distinction of becoming the film with the biggest opening ever to gross less than $100 million domestically." 

===Razzie Awards===
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Nominee
! Category
! Result
|- Catherine Zeta-Jones Worst Actress
|  
|- Golden Raspberry Worst Screen Couple
|  
|-
| Lili Taylor
|  
|-
| David Self Worst Screenplay
|  
|-
| Jan de Bont Worst Director
|  
|-
| Donna Roth Golden Raspberry Worst Picture
|  
|- Colin Wilson
|  
|-
| Susan Arthur
|  
|-
|}

==See also==
* List of ghost films

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 