Fracchia contro Dracula
{{Infobox film
 | name = Fracchia contro Dracula
 | image =Fracchia contro Dracula.jpg
 | caption =
 | director =Neri Parenti
 | writer = Paolo Villaggio, Neri Parenti, Franco Marotta, Laura Toscano
 | starring = Paolo Villaggio Gigi Reder, Isabella Ferrari
 | music =Bruno Zambrini
 | cinematography = Luciano Tovoli
 | editing =   
 | language = Italian 
 | country = Italy
 | runtime = 88 min
 | released = 1985
 }}
 1985 Italian Horror film|horror-comedy film directed by Neri Parenti.    

== Plot summary ==
Giandomenico Fracchia, Villaggios "monstrously shy" character, is tasked to sell a piece of real estate in Transylvania, otherwise hell lose his job. The customer is the obtusely nagging and prickly accountant Arturo Filini, who suffers from heavy nearsightedness and who doesnt realize that the manor hes interested in is actually Count Draculas castle. Once on the spot Fracchia is terrified at the going-ons while Filini, in true Mister Magoo-style dismisses them as tricks to dissuade him from the estate deal. Meanwhile a young and attractive vampire hunter (Isabella Ferrari), arrives, determined to avenge the death of his brother, who perished trying to rid the world of Dracula and his cohorts. The events turn even more farcical when Draculas sister confesses her love for Fracchia to try to avoid being engaged to the Frankenstein Monster. In the end an ash-tipped umbrella seems to solve the situation but...was it all for real or just a horror-movie fueled nightmare??

== Cast ==
* Paolo Villaggio: Giandomenico Fracchia
* Edmund Purdom: Count Vlad / Dracula
* Gigi Reder: Ragionier Filini
* Ania Pieroni: Countess Oniria
* Giuseppe Cederna: Boris
* Isabella Ferrari: Luna
* Susanna Martinková: Catarina
* Romano Puppo: Frankenstein
* Filippo De Gara: Butler of Dracula 
* Federica Brion: Stefania Paul Muller: employer of Fracchia
* Lars Bloch: the doctor

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 


 
 