Ju-on: The Curse 2
{{Infobox film
| image                 = Ju-on The Curse 2 poster.jpg
| caption               =
| director              = Takashi Shimizu
| producer              = Takashige Ichise Kazuo Katô Masaaki Takashima
| writer                = Takashi Shimizu Takashi Matsuyama
| music                  = Gary Ashiya
| cinematography         = Nobuhito Kisuki
| editing                =
| distributor            =
| released               = 2000
| runtime                = 76 minutes
| language               = Japanese
| budget                 =
}}

 , also known as simply Ju-on 2, is a 2000 Japanese V-Cinema horror film and the second installment in the Ju-on (franchise)|Ju-on series. The film was released in Japan on March 25, 2000 and was later released on video on April 14, 2000. 

Much of the sequel is a recap of the first film, nearly 30 minutes of retelling out of 76 minutes. The rest of the movie introduces new information on those events, and new characters, as well as briefly introducing events which would play into the later theatrical films.

==Plot==
Like the first film, Ju-on 2 is split into six vignettes, the first two being taken from the first. The segments are presented in the following order: Kayako (伽椰子), Kyoko (響子), Tetsuya (達也), Kamio (神尾), Nobuyuki (信之), and Saori (沙織).

Shunsuke Kobayashi, a teacher, visits the Saeki household but finds the young Toshio Saeki alone at home. However, he quickly finds the corpse of Kayako Saeki hidden in the attic and receives a phone call from her husband Takeo Saeki who reveals he has killed Kobayashis pregnant wife Manami and butchered her unborn child, due to believing Kayako was being unfaithful upon discovering she has an obsessive crush on Kobayashi. Kayakos body rises as an Onryō and kills Kobayashi, before tracking down and killing the rampaging Takeo.

Sometime later, a woman named Kyoko helps her brother Tatsuya, a real estate agent, to examine the Saeki house and put it on the market. However, Kyoko is disturbed by the house and leaves, visiting her nephew Nobuyuki in the apartment once owned by Kobayashi. They witness a vision of Takeo murdering Manami and are affected by the Saeki curse. Tatsuya moves them to his parents house in the countryside, where Kyoko is seemingly possessed and rocks back and forth, whilst Nobuyuki has become a mute. Tatsuyas father believes the Saeki houses curse is responsible, Tatsuya heading off to investigate.

The current residents of the Saeki house, Yoshimi and Hiroshi Kitada, become affected by the curse. Yoshimi murders her husband by walloping him with a frying pan, and then kills Tatsuya when he visits. All of Tatsuyas family save Nobuyuki die from the curse. Around a month later, Nobuyuki is still a mute and is observed by police officers Kamio and Iizuka. Both visit another officer, Yoshikawa, who has been driven mad by his attempted investigation of the deaths surrounding the Saeki house, but he and his wife are killed by Kayako. Kamio is attacked by Kayako at work, shocking Iizuka and another officer who go to check his office. Kamio is killed seconds later by Kayako.

In the penultimate vignette, Nobuyuki is shown at school. He spots Kayako outside the window, and she suddenly opens and crawls in through it. Nobuyuki flees whilst pursued by the surprisingly agile Kayako despite having her ankle broken when she died forcing her to crawl. A second Kayako appears, while he makes an attempt to escape through the stairs. Both ghosts corner Nobuyuki in a science lab and kill him - the final shots of the vignette showing there is an army of Kayako replicas outside, scratching the windows, and still making their ominous death rattle, reflecting on the curses never-ending, spreading effects.

The film closes with a brief final segment consisting of a close-up of the Nerima house from outside, with voice-overs indicating a group of high school girls, one of whom is named Saori, sneaking into the house. They are exploring the second floor and the film ends right after they notice something in the attic. The segment serves as a teaser for the 2003 theatrical film,  ; it is likely a prelude to the "Izumi" segment of that film, as Saori is the name of one of Izumis friends who went missing in the house.

==Cast==
*Yūko Daike as Kyoko Suzuki
*Makoto Ashikawa as Tatsuya Suzuki
*Kahori Fujii as Yoshimi Kitada
*Yūrei Yanagi as Shunsuke Kobayashi
*Ryota Koyama as Toshio Saeki
*Takako Fuji as Kayako Saeki Takashi Matsuyama as Takeo Saeki
*Kaei Okina as Hiroshi Kitada
*Tomohiro Kaku as Nobuyuki Suzuki
*Taizo Mizumura as Taiji Suzuki
*Harumi Matsukaze as Fumi Suzuki
*Yue as Manami Kobayashi
*Denden as Yoshikawa
*Taro Suwa as Kamio

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 