Dheiva Thaai
{{Infobox film
| name = Dheiva Thaai
| image = Dheiva Thaai.jpg
| director = P. Madhavan
| writer = K. Balachander (dialog)
| starring = M. G. Ramachandran B.Saroja Devi Nagesh M.N.Nambiar S.A.Ashokan
| producer = R. M. Verappan
| music =Viswanathan Ramamoorthy
| studio = Sathya Movies
| distributor = Sathya movies
| released = 18 July 1964
| runtime = 175 minutes
| country = India Tamil
| budget =
}}

Dheiva Thaai ( ) is a Tamil language film starring M. G. Ramachandran in the lead role. The film was released in the 1964.The film was one of the biggest hit of 1964.

==Plot==
Some story-line of the movie is taken from Dr. No (film)|Dr. No, the first James Bond movie starring Sean Connery.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran ||
|-
| Saroja Devi ||
|-
| M. N. Nambiar ||
|-
| S. A. Ashokan ||
|-
| Nagesh
|}

==Soundtrack==
The music composed by Viswanathan Ramamoorthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 05:14
|-
| 2 || Karunai Mazhaiye || P. Susheela || 03:37
|-
| 3 || Kathalikkathe || P. Susheela || 04:02
|-
| 4 || Moondrezhuthil En || T. M. Soundararajan || 03:08
|-
| 5 || Oru Pennai Parthu || T. M. Soundararajan || 04:37
|-
| 6 || Paruvam Ponapaathaiyele || T. M. Soundararajan, P. Susheela || 04:32
|-
| 7 || Unmaikku Veliyithu || Seerkazhi Govindarajan || 04:37
|-
| 8 || Vannakkili || T. M. Soundararajan, P. Susheela || 03:40
|}

==References==
 

==External links==
 

 

 
 

 
 
 
 
 


 