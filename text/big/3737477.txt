Don't Lose Your Head
 
 
 
 
{{Infobox film
| name           = Dont Lose Your Head
| image          = Carry On- Dont Lose Your Head poster.jpg
| caption        = Theatrical release poster by Renato Fratini
| director       = Gerald Thomas
| producer       = Peter Rogers
| writer         = Talbot Rothwell
| narrator       = Patrick Allen Charles Hawtrey Joan Sims Dany Robin Eric Rogers
| cinematography = Alan Hume
| editing        = Rod Keys
| distributor    = Rank Organisation
| released       = December 1966
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = £200,000
}} the series Charles Hawtrey and Joan Sims. French actress Dany Robin makes her only Carry on appearance in Dont Lose Your Head. It was released in 1966. Set in France and England in 1789 during the French Revolution, it is a parody of Baroness Orczys The Scarlet Pimpernel.

==Plot==
 
 
It is the time of the French Revolution, and two bored English noblemen, Sir Rodney Ffing (pronounced "Effing") and his best friend Lord Darcy Pue (played by Sid James and Jim Dale respectively), decide to have some fun and save their French counterparts from beheading by the guillotine.

Enraged revolutionary leader Citizen Camembert (Kenneth Williams) and his toadying lackey, Citizen Bidet (Peter Butterworth), scour France and England for the elusive saviour of the French nobles, who has become known as The Black Fingernail. After abducting the Fingernails true love, Jacqueline (Dany Robin), Camembert and Bidet plot to lure the Fingernail to his death... oblivious that Desiree (Joan Sims), Camemberts flamboyant mistress, is herself in love with the hero and will do all she can to save him from the guillotine.

==Cast==
 
*Sid James as Sir Rodney Ffing/The Black Fingernail
*Kenneth Williams as Citizen Camembert
*Jim Dale as Lord Darcy Pue Charles Hawtrey as Duke de Pommefrite
*Joan Sims as Desiree Dubarry
*Peter Butterworth as Citizen Bidet
*Dany Robin as Jacqueline Citizen Robespierre
*Marianne Stone as Landlady Michael Ward as Henri
*Leon Greene as Malabonce
*Hugh Futcher as Guard
*Richard Shaw as Captain
*David Davenport as Sergeant
*Jennifer Clulow as 1st lady
*Valerie Van Ost as 2nd lady
 
*Jacqueline Pearce as 3rd lady
*Nikki van der Zyl as Messenger
*Julian Orchard as Rake
*Elspeth March as Lady Binder
*Joan Ingram as Bald dowager
*Michael Nightingale as "What locket?" man
*Diana MacNamara as Princess Stephanie
*Ronnie Brody as Little man
*Billy Cornelius as Soldier
*Patrick Allen as Narrator
*Monica Dietrich as Girl
*Anna Willoughby as Girl
*Penny Keen as Girl
*June Cooper as Girl
*Christine Pryor as Girl
*Karen Young as Girl
 

==Filming and locations==

*Filming dates – 12 September-28 October 1966

Interiors:
* Pinewood Studios, Buckinghamshire

Exteriors: Clandon Hall, Guildford, Surrey, England Claydon Park, Claydon, Buckinghamshire, England
* Cliveden, Buckinghamshire, England, UK
* Waddesdon Manor, Waddesdon, Buckinghamshire, England, UK
* Black Park, Buckinghamshire, England, UK

==Bibliography==
* 
* 
* 
* 
*Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (third edition) (2007) (Reynolds & Hearn Books)
* 
* 
* 
* 
* C

== External links ==
*  
* 

 
 


 
 
 
 
 
 
 
 
 
 
 
 
 
 