Young Lochinvar
 
 
{{Infobox film
| name           = Young Lochinvar
| image          =
| caption        =
| director       = W.P. Kellino 
| producer       = 
| writer         = Walter Scott (poem)  Alicia Ramsey
| starring       = Owen Nares   Gladys Jennings   Dick Webb   Cecil Morton York
| music          = 
| cinematography = Basil Emmott 
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = October 1923
| runtime        = 5,500 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent historical historical drama film directed by W.P. Kellino and starring Owen Nares, Gladys Jennings, and Dick Webb.  The screenplay was based on Canto V, XII of the poem Marmion (poem)|Marmion by Walter Scott. 

==Plot==
In Scotland, a young man insists on marrying the woman he loves in spite of the fact that she is betrothed to another.

==Cast==
* Owen Nares as Lochinvar
* Gladys Jennings as Helen Graeme
* Dick Webb as Musgrave
* Cecil Morton York as Johnstone
* Charles Barratt as Alick Johnstone
* Bertie Wright as Brookie
* Lionel Braham as Jamie the Ox
* Dorothy Harris as Cecilia Johnstone
* J. Nelson Ramsay as Graeme

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918–1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 