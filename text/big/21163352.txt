Master Harold...and the Boys (2010 film)
{{Infobox film
| name         = Master Harold... and the Boys
| image        = Lg_masterharold.jpg 
| caption      = Promotional poster
| runtime      = 87 minutes
| director     = Lonny Price
| producer     = Michael Auret Zaheer Goodman-Bhyat David Pupkewitz Morris Ruskin   
| story        = Athol Fugard (play)
| screenplay   = Nicky Rebello
| starring     = Freddie Highmore Ving Rhames Patrick Mofokeng
| cinematography = Lance Gewer
| editing      = Ronelle Loots
| music        = Philip Miller 
| budget       = $3 million
| country      =  
| language     = English
| distributor  = Focus Films Spier Films Shoreline Entertainment
| released     =  
}} of the same name by Athol Fugard, directed by director Lonny Price.     The cast includes Freddie Highmore  and Ving Rhames        with the release to be set in 2010. 

==Plot==
Set in Port Elizabeth, South Africa, during the early apartheid days. The story deals with the coming of age of seventeen-year-old Hally (played by Freddie Highmore). Hally, a white South African, has a bad relationship with his biological father and is torn between his father’s expectations and opinions of him and those of his surrogate fathers, black waiters named Sam (played by Ving Rhames) and Willie (played by Patrick Mofokeng). Young Hally is obliged to laugh at his father’s racist jokes and perform humiliating tasks like empty chamber pots. By contrast, Sam exposes Hally to many positive experiences. After being humiliated by his father, Sam shows Hally how to be proud of something he’s achieved by helping him build and fly his own kite.  

One day, Hally receives news that his real father, a violent alcoholic, is coming back home from a long stint in a hospital. Hally, distraught with this news, unleashes years of anger and pain on his two black friends.

==Production== red camera format. This film has a budget of just under $3m.    Apart from its two international actors (Freddie Highmore & Ving Rhames), Nugent and American director Lonny Price, this film is a fully South African film.  Waterfront Post will post-produce this film, with Spier Films owning copyright. All the HODs (Head of Department) are South African, including Director of Photography  Lance Gewer from the Oscar-winning Tsotsi, editor Ronelle Loots, production designer Tom Gubb and Pierre Viennings (wardrobe). Philip Miller is composing the music.

==DVD / Blu-ray release==
Master Harold and the Boys was released in the United States on July 5, 2011 to DVD and Blu-ray.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 