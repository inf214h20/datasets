God Gave Me Twenty Cents
{{Infobox film
| name           = God Gave Me Twenty Cents
| image          = 
| alt            = 
| caption        = 
| director       = Herbert Brenon
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Elizabeth Meehan John Russell Dixie Willson
| starring       = Lois Moran Lya De Putti Jack Mulhall William Collier, Jr. Adrienne DAmbricourt Leo Feodoroff Rosa Rosanova
| music          = 
| cinematography = Leo Tover
| editing        =  
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Herbert Brenon and written by Elizabeth Meehan, John Russell and Dixie Willson. The film stars Lois Moran, Lya De Putti, Jack Mulhall, William Collier, Jr., Adrienne DAmbricourt, Leo Feodoroff and Rosa Rosanova. The film was released on November 20, 1926, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Lois Moran as Mary
*Lya De Putti as Cassie Lang
*Jack Mulhall as Steve Doren
*William Collier, Jr. as Barney Tapman
*Adrienne DAmbricourt as Ma Tapman
*Leo Feodoroff as Andre Dufour
*Rosa Rosanova as Mrs. Dufour
*Claude Brooke as The Florist
*Tommy Madden as Thug
*Phil Bloom as Thug
*Eddie Kelly as Thug 
*John Sharkey as Thug 
*Harry Lewis as Thug

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 