Eroica (1949 film)
{{Infobox film
| name           = Eroica
| image          = 
| image size     = 
| caption        = 
| director       = Walter Kolm-Veltée
| producer       = Guido Bagier Walter Kolm-Veltée
| writer         = Walter Kolm-Veltée Franz Tassié
| narrator       = 
| starring       = 
| music          = Alois Melichar Ludwig van Beethoven
| cinematography = Günther Anders (cinematographer)|Günther Anders Hannes Staudinger
| editing        = 
| distributor    =  
| released       = 
| runtime        = 95 minutes
| country        = Austria
| language       = German
| budget         = 
| preceded by    = 
| followed by    = 
}}
 Austrian film depicting composer Ludwig van Beethovens life and work. The film is directed by Walter Kolm-Veltée, produced by Guido Bagier with Walter Kolm-Veltée and written by Walter Kolm-Veltée with Franz Tassié. It was entered into the 1949 Cannes Film Festival.   

== Plot == messenger rides Napoleon Bonaparte is approaching Vienna with his troops. The news spreads like wildfire in the city and also reaches Beethoven, who, together with his friends, is sitting in the tavern. Beethoven is very enthusiastic about the ideals embodied by Napoleon after the French Revolution. He hurries home to write a powerful and glorious symphony - which later becomes famous as Beethovens "Symphony No. 3 (Beethoven)|Eroica" - for Napoleon. After the symphony is successful, two messengers from Napoleon come to see Beethoven. They request Beethoven to participate in a reception given by the French emperor. Favouring the glamour, Napoleon has elaborated detailed clothing instructions for Beethoven. Beethoven is disappointed by his idols superficiality and deletes the dedication to Napoleon from the title page of the symphony.

For his safety, Beethoven travels to Hungary and finds quarters at the aristocratic home of his pupil Therese van Brunswik and her cousin Giulietta Guicciardi. Beethoven falls in love with Giulietta, who is even willing to leave her fiancé for Beethoven. This interpretation of Beethovens life is distorted, however, as it was in 1800, four years earlier, that Beethoven actually met Giulietta. In the film story, Therese thinks that, due to his talent, it is not Beethovens destiny to lead a fulfilling relationship with a woman.

Beethoven not only worries about his nephew Karl whom he thinks is leading a dissolute life and is under his mothers harmful influence but also becomes aware of a worsening of his hearing abilities. As Beethoven despairs of a reason why God would want to deprive him off his hearing, Beethovens friend Amenda replies that Beethovens destiny is to compose a type of music never heard before. It is saddening for Beethoven to learn during rehearsals for his opera "Fidelio" that the musicians cannot follow his "conducting", and in fact are actually following their music director instead. In a depressed mood, Beethoven goes home and once more speaks with God, as Amendas words come to his mind. When Therese comes to look after him, he comes to terms with his destiny and begins to compose again. 


==Cast==
*Ewald Balser — Ludwig van Beethoven
*Marianne Schönauer — Therese van Brunswick
*Judith Holzmeister — Giuliettia Guicciardi
*Oskar Werner — Karl, Beethovens nephew
*Dagny Servaes — Karls mother
*Iván Petrovich — Count Lichnowsky
*Ludmilla Hell — Countess Lichnovsky
*Auguste Pünkösdy — House keeper
*Hans Kraßnitzer — Amenda
*Alfred Neugebauer — Organist Albrechtsberger
*Richard Eybner — Schuppanzigh
*Karl Günther — Country doctor
*Gustav Waldau — Country parson
*Erik Frey — French Officer
*Franz Pfaudler — Theatre Director
*Julius Brandt — Painter
*Hans Hais — French Officer
*Helmut Janatsch — Austrian Cavalryman
*Karl Kalwoda — Caretaker

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 