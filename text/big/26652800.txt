Katha Parayum Theruvoram
{{Infobox film
| name           = Kadha Parayum Theruvoram
| image          = Katha Parayum Theruvoram.jpg
| alt            =  
| caption        = A still from the movie Sunil
| producer       = 
| writer         = Sarjulan
| starring       = Kalabhavan Mani  Padmapriya Punathil Kunhabdulla
| music          = Ramesh Narayanan
| cinematography = Jibu Jacob
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Kadha Parayum Theruvoram is a 2009 Malayalam film written and directed by Sunil (director)|Sunil. The star cast include Kalabhavan Mani, Padmapriya and novelist Punathil Kunhabdulla playing important roles. Three hundred children support the central characters including the characters played by Javed, Aakash, Abhinav, Amith and Mahek. Script and dialogues are by Sarjulan and the music is by Ramesh Narayan.

== Plot ==
Four children are brought up in the street. They were under the custody of a street gang. One day they escapes from the mafia who made them beg along the streets. The rest of the movie is how the kids survive on the streets by themselves.

== Cast ==
* Kalabhavan Mani
* Padmapriya
* Punathil Kunhabdulla
* Master Javed
* Master Aakash
* Master Abhinav
* Master Amith
* Master Mahek
* Ambika Mohan

== External links ==
* http://www.indiaglitz.com/channels/tamil/preview/11202.html
* http://popcorn.oneindia.in/title/3750/kadha-parayum-theruvoram.html
* http://www.my-kerala.com/movies/2009/05/katha-parayum-theruvoram.shtml
* http://kadhaparayumtheruvoram.blogspot.com/

 
 
 


 