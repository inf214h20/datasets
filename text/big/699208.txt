National Treasure (film)
{{Infobox film
|name=National Treasure
|image=Movie_national_treasure.JPG
|caption=Theatrical release poster
|director=Jon Turteltaub
|producer=Jerry Bruckheimer Jon Turteltaub Ted Elliott Cormac Wibberley Marianne Wibberley
|story=Jim Kouf Oren Aviv Charles Segars
|starring= Nicolas Cage Harvey Keitel Jon Voight Diane Kruger Sean Bean Justin Bartha Christopher Plummer
|music=Trevor Rabin
|cinematography=Caleb Deschanel
|editing=William Goldenberg studio = Jerry Bruckheimer Films Junction Entertainment Saturn Films distributor = Buena Vista Pictures
|released=  
|runtime=131 minutes
|country= United States
|language=English
|budget= $100 million 
|gross= $347.5 million
}} Ted Elliott, Terry Rossio, Cormac Wibberley, and Marianne Wibberley, produced by Jerry Bruckheimer, and directed by Jon Turteltaub. It is the first film in the National Treasure (franchise)|National Treasure franchise and stars Nicolas Cage, Harvey Keitel, Jon Voight, Diane Kruger, Sean Bean, Justin Bartha, and Christopher Plummer.
 millennia starting Declaration of Independence points to the location of the "national treasure", but Gates is not alone in his quest. Whoever can steal the Declaration and decode it first will find the greatest treasure in history.

==Plot==
  treasure hunters. Founding Fathers and the Freemasons during the American Revolutionary War.
 Declaration of Independence. When Ian suggests they steal the Declaration to get the map, Ben holds fast and wanting to acquire the document legally. They fight, and gunpowder in the hull is ignited; Ian and his team leave Ben and Riley to their fate, but the two are able to escape safely.
 FBI and National Archives gala while the Declaration is placed in storage. Abigail becomes aware of Bens actions, but as she approaches them during their escape, Ian and his men arrive. Ben, Abigail, and Riley escape in a car chase. The FBI, led by Peter Sadusky (Harvey Keitel), discover the missing Declaration and learn of Bens identity, and begin to track him down, while Ian and his men work to locate Ben.
 Ottendorf cipher USS Intrepid during which they engineer Bens escape.
 Trinity Church. Paul Reveres Ride—a clue pointing to the Old North Church in Boston. Ian and his men strand Ben, Abigail, Riley and Patrick and race to Boston; Ben soon reveals they lied to Ian, and finds a means of opening a secret door by an engraving of the all-seeing eye. Inside, they find a notch which the pipe from the Charlotte fits, opening onto a large chamber containing the national treasure, as well as a secondary passage to the surface. Once out of the church, Ben contacts Sadusky, and learns he is a Freemason; Ben returns the Declaration and the location of the treasure in exchange for their names being cleared of any wrongdoing. Ben also informs Sadusky of his bluff to Ian, and the FBI are able to capture Ian and his men when they arrive in Boston.

Later, Ben and Abigail have started a relationship, while Riley is somewhat upset that Ben turned down the 10% finders fee for the treasure and accepting a much smaller amount that still has netted them all significant wealth.

==Cast== Benjamin Franklin Gates
* Diane Kruger as Dr. Abigail Chase (Ph.D.)
* Justin Bartha as Riley Poole
* Jon Voight as Patrick Gates
* Christopher Plummer as John Adams Gates
* Sean Bean as Ian Howe
* David Dayan Fisher as Shaw
* Harvey Keitel as Agent Sadusky
* Stewart Finlay-McLennan as Powell
* Oleg Taktarov as Victor Shippen
* Stephen Pope as Phil McGreggor
* Annie Parisse as Agent Dawes
* Mark Pellegrino as Agent Johnson
* Armando Riesco as Agent Hendricks
* Erik King as Agent Colfax
* Jason Earles as Thomas Gates

==Production==

===Filming locations===
 National Treasure was filmed in the following locations: First Congregational Church, 540 S. Commonwealth Avenue, Los Angeles, California, USA
* Knotts Berry Farm, Independence Hall replica, 8039 Beach Boulevard, Buena Park, California, USA 
* Lincoln Memorial, National Mall, Washington, District of Columbia, USA 
* Memorial Continental Hall, 1776 D Street NW, Washington, District of Columbia, USA
* New York City, New York, USA 
* Philadelphia, Pennsylvania, USA  Trinity Church, 79 Broadway, Financial District, Manhattan, New York City, New York, USA
* USS Intrepid (CV-11), New York City, New York, USA 
* Utah, USA (Arctic scene)
* Washington, D.C., USA   

==Reception==

===Critical reception=== Disney childrens adventure movies,  and using it as the basis for an essay on scene transitions in classical Hollywood cinema. 

The film currently holds a 44% approval rating on Rotten Tomatoes. The sites consensus reads: "National Treasure is no treasure, but its a fun ride for those who can forgive its highly improbable plot." 

===Box office===
The film was a box office success, grossing over $173 million domestically and $174.5 million around the world to a total of $347.5 million worldwide.

==Home video releases==

===Collectors Edition DVD===
A special collectors edition, two-disc DVD set of the movie was released on December 18, 2007.

===Blu-ray Disc===
Walt Disney Studios Home Entertainment released Blu-ray Disc versions of National Treasure and its sequel, National Treasure 2: Book of Secrets, on May 20, 2008. 

==Soundtrack==
{{Infobox album  
| Name        = National Treasure
| Type        = film
| Artist      = Trevor Rabin
| Cover       = National Treasure Soundtrack.jpg
| Released    = November 16, 2004
| Recorded    = 2004
| Genre       =
| Length      = Hollywood
| Producer    = Trevor Rabin
}}

{{Track listing
| all_writing     = Trevor Rabin
| title1          = National Treasure Suite
| length1         = 3:17
| title2          = Ben
| length2         = 4:03
| title3          = Finding Charlotte
| length3         = 1:04
| title4          = Library of Congress
| length4         = 2:27
| title5          = Preparation Montage
| length5         = 4:53
| title6          = Arrival at National Archives
| length6         = 1:54
| title7          = The Chase
| length7         = 4:22
| title8          = Declaration of Independence
| length8         = 1:43
| title9          = Foot Chase
| length9         = 3:34
| title10         = Spectacle Discovery
| length10        = 3:18
| title11         = Interrogation
| length11        = 4:30
| title12         = Treasure
| length12        = 3:39
}}

==Sequels==

===National Treasure: Book of Secrets===
 
Although the DVD commentary stated that there were no plans for a sequel, the films box office gross of an unexpected $347.5 million worldwide warranted a second film, which was given the green light in 2005.  , on the DVD as National Treasure 2: Book of Secrets, was released on December 21, 2007.

===National Treasure 3===
Director Jon Turteltaub said that the filmmaking team will take its time on another National Treasure sequel,  but Disney has already registered the domains for NationalTreasure3.com and NationalTreasure4.com.  Though the second film ended with the question about page 47 of the Presidents book of secrets, the new movie may or may not be a sequel about the "Page 47". Turteltaub responded in a press interview that the idea was not set in stone as the basis for National Treasure 3. 

==See also==
 
* Arnold Cipher
* Beale ciphers
* Nicholas Dietrich, Baron de Ottendorf
* National Archives and Records Administration
* United States Declaration of Independence

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  .
*  .
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 