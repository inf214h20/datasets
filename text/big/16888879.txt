Sweeney!
 
{{Infobox film
| name           = Sweeney!
| image          = Sweeney! uk quad 320x240.jpg
| image_size     = 
| caption        = 
| director       = David Wickes
| producer       = Ted Childs
| writer         = Ranald Graham
| narrator       =  Barry Foster Diane Keen
| music          = Denis King
| cinematography = Dusty Miller
| editing        = Chris Burt
| studio         = Euston Films
| distributor    = EMI Films
| released       =  
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
}}
Sweeney! is a 1977 British crime film, made as a spin-off from the television show The Sweeney which ran from 1974 to 1978. It was released on Region 2 DVD in 2007. A sequel Sweeney 2 was released the following year.

== Plot ==
The film followed the adventures of DI Regan and DS Carter as they became embroiled in a deadly political scandal. One of the leading members of the government, Charles Baker (Ian Bannen), is about to secure a huge deal with OPEC stabilising the world oil market and boosting Britains position within it. Baker is the rising star of the government, regarded as a future prime minister, and he is closely controlled by his urbane, manipulative American press secretary Elliot McQueen (Barry Foster). 

When Regan investigates the mysterious death of a prostitute as a favour to one of his informants, he becomes aware that Baker and McQueen might be involved. A spate of killings follow - which sees Regan take on both the criminals and the hierarchy of the Metropolitan Police and British security services. The outcome of the film was similar to that of the Profumo affair, though with a typical Sweeney bittersweet ending.

==Cast==
* John Thaw as Detective Inspector Jack Regan
* Dennis Waterman as Detective Sergeant George Carter Barry Foster as Elliot McQueen
* Ian Bannen as Charles Baker MP
* Colin Welland as Frank Chadwick
* Diane Keen as Bianca Hamilton
* Michael Coles as Johnson
* Joe Melia as Ronnie Brent
* Brian Glover as Mac
* Lynda Bellingham as Janice Wyatt
* Morris Perry as Flying Squad Commander
* Michael Latimer as P.P.S.

==Reception==
Michael Deeley of EMI later said the film "was successful, so were helping fill the demand by making another one". British money is suddenly big in Hollywood,right up with Fox and Warne r.
Mills, Bart. The Guardian (1959-2003)   02 Sep 1977: 8.  

It was praised for capturing the spirit and setting of the original TV series. The film was successful enough for a sequel the following year Sweeney 2 which saw some of the action relocated to the Mediterranean.

==Production==
Sweeney! was made by Euston Films who also produced the television series. They had been planning a big screen outing for the show for a while. In the 1970s it was common for television shows to be given cinematic releases, amongst which were some of the biggest box office hits of the decade. 

The movie was one of a six-film £6 million program of films authorised by Nat Cohen at EMI Films. Boost for studios
The Guardian (1959-2003)   09 July 1975: 5. 

A number of the recurring minor characters of the film also appeared in the television series. But Garfield Morgan, who played Haskins in the TV shows, did not appear in the film. As with the television series, a large amount of the action took place outside with location footage. It was released in the United Kingdom with a X rating owing to the scenes of violence and nudity. 

The film followed fairly closely the events of the Profumo Scandal which had rocked British politics more than a decade before, though with a much more violent premise. The film was also notable concerning the topical subject of energy and oil usage, a major international issue at the time the film was made.

==References==
 
== External links ==
*  

 
   
 
   
 
 
 