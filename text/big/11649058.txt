Bugs Bunny's Easter Special
 
{{Infobox film
| name           = Bugs Bunnys Easter Special
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Gerry Chiniquy Friz Freleng Chuck Jones Robert McKimson
| producer       = David H. DePatie Friz Freleng 
| writer         = 
| screenplay     = 
| story          = David Detiege Friz Freleng
| based on       = 
| starring       = Mel Blanc June Foray
| narrator       = 
| music          = Milt Franklyn Doug Goodwin William Lava John Seely Carl W. Stalling
| cinematography = 
| editing        = Rick Steward
| studio         = Warner Bros. Television DePatie-Freleng Enterprises
| distributor    = Columbia Broadcasting System
| released       =  
| runtime        = 50 minutes
| country        =  
| language       = English
| budget         = 
| gross          = 
}}

Bugs Bunnys Easter Special (also known as The Bugs Bunny Easter Special and Bugs Bunnys Easter Funnies) is a List of Looney Tunes television specials|Looney Tunes television special featuring a number of Warner Bros. cartoons. It originally debuted on the CBS network on April 7, 1977 at 8 p.m.

==Cast== Daffy Duck/the Easter Bunny, Sylvester, Pepe Le Pew, Foghorn Leghorn, Yosemite Sam
*June Foray as Granny (Looney Tunes)|Granny.

==Plot==
The Easter bunny is ill, Granny needs to find a replacement for him, after watching several cartoons from the past. A very eager Daffy Duck keeps appearing, in the end he dresses up as the Easter bunny and Granny and Bugs have been cheated but find it funny.

==Credits==
*Produced by Hal Geer
*Supervised by Friz Freleng and David Detiege
*Directed by Gerry Chiniquy, Friz Freleng, Chuck Jones, Robert McKimson
*Written by Friz Freleng, David H. DePatie and David Detiege.

==Cartoons Featured==
*Birds Anonymous
*Bully For Bugs
*For Scent-imental Reasons
*Knighty Knight Bugs
*Sahara Hare
*Robin Hood Daffy
*Rabbit of Seville
*Hillbilly Hare
*Tweetys Circus
*Little Boy Boo

==Home Media==
This special was released on DVD in 2011, in time for the Easter holiday.

==External links==
*  

 
 
 
 
 
 

 