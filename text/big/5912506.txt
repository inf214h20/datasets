The Lump
 
{{Infobox film
| name           =  The Lump
| image          =
| image size     =
| caption        = John Weldon
| producer       =  David Verrall John Weldon
| narrator       =
| starring       = John Weldon
| cinematography =
| editing        =
| studio         = National Film Board of Canada (NFB)
| distributor    =
| released       =
| runtime        =  8 min. Canada
| English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

The Lump is a short animated film released in 1991. It tells the story of an unattractive and unpopular man named George. One day, a lump appears on his head that looks like an attractive face. By pretending the lump is his real face, he gains fame and fortune, but soon he gets into trouble when he enters into the company of several corrupt politicians.
 John Weldon. Harvey Atkin contributed the voice. It was nominated for a Genie Award in 1992 {{cite news
|url=http://imdb.com/title/tt0203659/awards
|publisher=Internet Movie Database
|title=Awards for the Lump
}} 
and won the Gordon Bruce Award for Humor at the Ottawa International Animation Festival in that year. {{cite news
|url=http://ottawa.awn.com/index.php?option=com_content&task=view&id=46&Itemid=311#gordon_bruce
|publisher=Oxford International Animation Festival
|title=Past Award Winners by Category
}} 

==References==
 

==External links==
* 
*Watch   at NFB.ca

 
 
 
 
 
 
 
 
 
 
 
 
 