White Rabbit (film)
{{Infobox film
| name           = White Rabbit
| image          = White_Rabbit_-_Theatrical_Movie_Poster.png
| image_size     = 240px
| border         = yes
| alt            = 
| caption        = Theatrical release poster Tim McCann
| producer       = {{Plain list |
*Shaun Sanghani
*Robert Yocum
*Jacky Lee Morgan
}}
| screenplay     = Anthony Di Pietro
| starring       = {{Plain list |
*Nick Krause
*Sam Trammell
*Britt Robertson Ryan Lee
*Dodie Brown
 
}}
| music          = {{Plain list |
*John Vincent McCauley
}}
| cinematography = Alan McIntyre Smith
| editing        = {{Plain list |
*Tim McCann
}}
| studio         = {{Plain list |
*Burning Sky Films
*SSS Entertainment
}}
| distributor    = Breaking Glass Pictures   
| released       =   
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $2 million   
| gross          = 
}}

 Tim McCann and written by Anthony Di Pietro about a mentally ill teen being bullied in high school, whose visions urge him to take revenge. The film stars Nick Krause, Sam Trammell and Britt Robertson. It was produced by Robert Yocum (Burning Sky Films), Shaun Sanghani (SSS Entertainment) and Jacky Lee Morgan. It had its world premiere at the Zurich Film Festival and is being distributed in the United States by Breaking Glass Pictures.   

==Plot Synopsis==
Harlon Mackey (Nick Krause) has been tormented by visions since his alcoholic father (Sam Trammell) forced him to kill an innocent rabbit while hunting as a boy. Now that Harlon is a bullied high school teen, his undiagnosed mental illness is getting worse. He begins to hear voices, and his imagination encourages him to carry out violent acts. Things begin to look up when Julie (Britt Robertson), a rebellious young girl, moves to town and befriends Harlon… But when she betrays him, the rabbit along with other imaginary comic book characters taunt him into committing one final act of revenge. 
The line between reality and Harlon’s imagination begin to blur in this intense dramatic thriller.

==Cast==
*Nick Krause as Harlon Mackey
*Sam Trammell as Darrell Mackey
*Britt Robertson as Julie Ryan Lee as Steve
*Dodie Brown as Ruby Mackey
*Zac Waggener as Thomas Dayton
*Josh Warren as Duane
*Robert Michael Szot as Harlon (Age 9) 
*Danielle Greenup as April
*Denise Williamson as Alice
*Carl Palmer as Mr. Eastman
*Maria McCann as Mrs. Eastman
*Lawrence Turner as Harvey
*Billy Slaughter as Dr. Clayton
*Aaron Saxton as Bouncer
==Production==
Principal photography of the film took place on August, 2012 in Louisiana, USA. Movie was shot with two digital cameras in 21 days.
===Music=== digitally on January 20th, 2015.

==Release==
White Rabbit premiered on September 30th, 2013 at the Zurich Film Festival.  It was released theatrically and VOD in North America on February 13th, 2015. 
==Accolades==
{| class="wikitable sortable" width="95%"
|-
! colspan="5" | Awards
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|- Catalina Film Festival
| rowspan="1" | September 28th, 2014 Best Film 
| Shaun Sanghani (SSS Entertainment)
|  
|- Boston Film Festival
| rowspan="3" | September 30th, 2014 Best Actor
| Nick Krause
|  
|-
| Best Supporting Actor
| Sam Trammell
|  
|-
| Best Supporting Actress
| Britt Robertson
|  
|-
| rowspan="2" | Chelsea Film Festival
| rowspan="2" | October 19th, 2014
| Best Supporting Actor
| Sam Trammell
|  
|-
| Best Cinematography
| Alan McIntyre Smith
|  
|}
==Trivia==
*One scene was filmed just a couple miles away from Ochsner hospital where the actor Sam Trammell was born.
*Many actors from the movie are locals from Louisiana.
*The script was written in 2004.
*The strip club scene was not originally in the script.
==See also==
 
==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 