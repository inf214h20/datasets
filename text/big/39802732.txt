102 Years in the Heart of Europe: A Portrait of Ernst Jünger
{{Infobox film
| name           = 102 Years in the Heart of Europe: A Portrait of Ernst Jünger
| image          = 
| caption        = 
| director       = Jesper Wachtmeister
| producer       = Cecilia Cederström
| writer         = Björn Cederberg
| starring       = Ernst Jünger Björn Cederberg Mikael Persbrandt
| cinematography = Per Källberg Jesper Wachtmeister
| editing        = Jesper Wachtmeister
| studio         = Martin & Co. Filmproduktion
| distributor    = 
| released       =  
| runtime        = 56 minutes
| country        = Sweden
| language       = German Swedish
| budget         = 
}}
102 Years in the Heart of Europe: A Portrait of Ernst Jünger ( ) is a Swedish documentary film from 1998 directed by Jesper Wachtmeister.  It consists of an interview by the journalist Björn Cederberg with the German writer, philosopher and war veteran Ernst Jünger (1895-1998). Jünger talks about his life, his authorship, his interests and ideas. The actor Mikael Persbrandt reads passages from some of Jüngers works, such as Storm of Steel, The Worker, On the Marble Cliffs and The Glass Bees.

Cederberg had interviewed Jünger eight years earlier but only in text. Jünger had declined to participate in the film project, but the film team still decided to travel to the village Wilflingen, where Jünger lived, and make an attempt. As a gift, they gave Jünger, known for his interest in botany and zoology, an 18th-century print of Carl Linnaeus Systema Naturae, and were granted an interview. 

==References==
 

 

 
 
 
 
 
 
 
 
 


 
 