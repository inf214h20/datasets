Jazz Rhythm
{{multiple issues|
 
 
}}
{{Infobox Hollywood cartoon|
| cartoon_name = Jazz Rhythm
| series = Krazy Kat
| image = 
| caption = 
| director = Manny Gould Ben Harrison
| story_artist = Ben Harrison
| animator = Manny Gould Allen Rose Harry Love
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures
| release_date = June 19, 1930
| color_process = Black and white
| runtime = 8:21 English
| preceded_by = Alaskan Knights
| followed_by = Honolulu Wiles
}}

Jazz Rhythm is a short animated film distributed by Columbia Pictures. The film is part of a series featuring the comic strip character Krazy Kat.

==Plot==
Sports aficionados from faraway come to a stadium to watch an event. Although the event features a boxing ring, the event is not a boxing match. Rather, its a contest between two pianists playing animated pianos. One of them is Krazy Kat who uses a standard piano. His opponent is a lion who uses an upright piano.

The lion is first to play. Despite getting some disturbance from a passing bee, the lion manages to perform without flaws.

Krazy is next to perform. Krazy also performs perfectly, thanks to not being disrupted.

After the pianists performed, they rest in their corners like boxers who been through a tough fight. Each of them has a cornerman who is a rat, and tries to keep them cool.

Moments later the third round begins. This time its a boxing match between the two pianos as the instruments trade punches. After some exchange, Krazys piano comes out the winner. Krazy celebrates by playing his piano again. The lion and the other piano, despite being bested, join Krazys play minutes later.

==External links==
*  at the Big Cartoon Database
* 

 

 
 
 
 
 
 
 
 


 