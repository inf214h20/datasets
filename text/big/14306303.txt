Intergirl
{{Infobox Film 
| name = Intergirl
| image = Intergirl.jpg
| image_size =
| caption = 
| director = Pyotr Todorovsky
| producer = 
| writer = 
| starring = Elena Yakovleva Vsevolod Shilovsky Zinovy Gerdt Lyubov Polishchuk Ingeborga Dapkunaite Larisa Malevannaya Anastasya Nemolyaeva
| music = 
| cinematography = 
| editing = 
| distributor = Mosfilm
| released = 1989
| runtime = 143 minutes
| country = Soviet Union
| language = Russian
| budget = 
}} Soviet drama the most popular Soviet film in 1989 (41.3 million viewers) and made a star of leading actress Elena Yakovleva.

It is the screen adaptation of the eponymous story by Vladimir Kunin (also author of Svolochi).

==Movie plot==
Swedish currency prostitute and client at the same time nurses from Leningrad Tanya Zaitseva suddenly makes her a marriage proposal. After another conversation with the police it with good news back home to his mother, who thinks that her daughter is just a nurse. Tanya did not hide the fact that she is getting married not for love, but because he wants to have an apartment, a car, money and dreams "to see the world with my own eyes." In a conversation with her mother she argues that prostitution is characteristic of all trades, "all sell themselves." However, the mother and can not imagine that Tatiana trades are in the truest sense of the word.

Former client and now fiance Thani Ed Larsen (in the book Edward Larsen) - a pass-Thani in the Western world of dreams. However, on its way gets the Soviet bureaucracy: to travel to Sweden Tanya needed help. The heaviest price she gets help from her father, whom she has not seen for 20 years. It requires 3,000 rubles - a lot of money in those days. Tanya has to re-engage in prostitution to get the money.

Sweden very quickly bored heroine. The only outlet for her becomes Russian truck drivers working in the "Sovtransavto", through which it sends gifts to his mother in Leningrad. Swedish "friends" not for a moment forget how Tanya earned in the USSR. Ed really loves his wife, but always making comments about her habits. Tanya - absolutely a stranger in a strange world. She wants to visit his mother. Meanwhile prostitute girlfriend Tanya says over the phone that the USSR at her opened case on "speculation" (for illegal currency transactions was another article, with very strict sanctions) for illegal currency exchange. Investigators come to Tanyas mother and reveals the secrets of her daughters high earnings. Morally broken mother-teacher commits suicide, household gas poisoning in his apartment. Skein, a neighbor Tanya, smell of gas at the site, bursts into the apartment, knocks out the window, pulls her mother to court and unsuccessfully tries to save her, raised the alarm. She struggled in vain knocking on the door to the neighbors. At this moment in Sweden Tanya turns as if to hear the knock. She feels that there was something terrible. In a panic, she jumps into the car, goes to the airport and breaks. The drama of the final episode reinforces the Russian folk song "Tramp" ("In the wild steppes of Transbaikalia ..."), which is the leitmotif of the film.

==Awards==
3 wins and 1 nomination. Elena Yakovleva has won the Best Actress award at Nika Award|Nika, 1990, and Tokyo International Film Festival, 1989.

==External links==
* 
* 
* 
* 
*  at Movie.ru  

 
 
 
 
 
 
 
 
 