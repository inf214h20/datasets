Burglar (film)
 
{{Infobox film
| name           = Burglar
| image          = Burglarposter.jpg
| caption        = Theatrical poster for Burglar Hugh Wilson Michael Hirsh Kevin McCormick
| eproducer      = Tom Jacobson
| aproducer      = Michael Green Hugh Wilson
| based on       = Lawrence Block (novel)
| starring       = Whoopi Goldberg Bob Goldthwait G. W. Bailey Lesley Ann Warren
| music          = Sylvester Levay
| cinematography = William A. Fraker
| editing        = Fredric Steinkamp William Steinkamp Nelvana Limited
| distributor    = Warner Bros.
| released       = March 20, 1987
| runtime        = 103 minutes
| country        = Canada United States
| language       = English
| budget         =
| gross          = $16,357,355 (sub-total)
}} comedy film Hugh Wilson and distributed by Warner Bros. The film stars Whoopi Goldberg and Bobcat Goldthwait.

==Plot==

Bernice "Bernie" Rhodenbarr, a former burglary|burglar, resumes her life of crime when a corrupt police officer named Ray Kirschman blackmails her.

A dentist, Dr. Cynthia Sheldrake, hires Bernice to break into her ex-husband Christophers home and steal back her jewelry. Things take a turn for the worse when Christopher is murdered while Bernie is robbing his home, and thanks to Sheldrake and her lawyer Carson, Bernie is the only suspect.

To clear her name Bernice and her friend Carl hop from bar to bar looking for someone who knew Christopher. They find out that Christopher had quite a few girlfriends ... and boyfriends. Bernice has three new suspects after an old flame of Christophers tells her about an artist, a bartender, and a mysterious man known only by his nickname: "Heeeeeeeres Johnny!"

Bernice investigates the artist and the bartender only to have them show up dead. With no clues or witnesses, Bernice waits for Dr. Sheldrake in her home to confront her. Demanding she tell her everything she knew about Christopher, she concludes that the doctor herself had been with her ex the night he was murdered.

During the conversation the TV flashes to an episode of The Tonight Show Starring Johnny Carson. Bernice realizes Carson knew the doctors ex. Bernice calls Carson to meet her in the park with the bag of jewelry Bernice was commissioned to steal in the first place. Bernie has also deduced that Carson was in love with Christopher himself. A scuffle ensues and Bernice, along with her friend Carl and Ray, capture Carson.

==Production==
The film was adapted from the 1978 novel The Burglar in the Closet by Lawrence Block; in Blocks book, Bernie Rhodenbarr is a white man. It was also the first R-rated and live-action production from the Canadian animation company, 
Nelvana. 

In a 2013 interview with Kevin Smith, screenwriter Jeph Loeb disclosed that Burglar was initially intended to be a serious vehicle for Bruce Willis with Whoopi Goldberg filling the role of the characters neighbor.  When Willis dropped out, Goldberg took on the lead role. 

==Cast==
* Whoopi Goldberg as Bernice Rhodenbarr

* Bobcat Goldthwait as Carl Hefler

* G. W. Bailey as Ray Kirschman

* Lesley Ann Warren as Dr. Cynthia Sheldrake

* James Handy as Carson Verrill

* Anne De Salvo as Detective Todras

* John Goodman as Detective Nyswander

==See also==
* List of American films of 1987
* List of Nelvana franchises

==References==
 

==External links==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 

 