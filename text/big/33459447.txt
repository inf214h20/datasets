My Ex 2: Haunted Lover
{{multiple issues|
 
 
}}

{{Infobox film
| name           = My Ex 2: haunted Lover
| image          = MyEx2HauntedLover2010Poster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Piyapan Choopetch
| producer       = 
| writer         = Adirek Uncle Watleela Piyapan Choopetch
| starring       = Ratchawin Wongviriya Marion Affolter Thongpoom Siripipat   Pete Thongjua
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 86 minutes
| country        = Thailand
| language       = Thai
| budget         = 
| gross          = 
}}

My Ex 2 : Haunted Lover (แฟนใหม่) is a 2010 Thai Horror film, a sequel to My Ex.

==Synopsis==
After watching My Ex (2010), Cee and her friends are chatting about the movie. Her older sister, Bowie, who is a famous actress, stated that a sequel was in the works. 
Afterwards, Cee catches her boyfriend with Ying, who was the daughter of a hotel owner, revealing that he is a playboy.
Aof and Cee later quarrels in front of Ying, who was shocked and leaves after Cee slaps Aof. 
Ying is later found dead in a pool of blood, discovered by Aof. She had apparently jumped from the building.

The next day Aof calls Cee about Yings death and tries to apologize to her but she calls off the relationship. 
Cee and Bowie meet Bowies producer friend, Karn, who is a rich resort owner and the producer of Cees debut film after she got a part in the horror film.

The sisters, and their friend go to the film set in Koh Chang to enjoy the holidays. When things started to go wrong when Cee sees Yings ghost and her friends is killed by a truck. Cee finds out that Aof is the prime suspect in Yings death. Cee and Bowie are caught in a love triangle with Karn.

A flashback shows that Cee was directly responsible for Yings death, and Aof was framed by Cee for Yings murder and Cee kills Aof with a knife and hit his head with a stone in the room.. Not long after, Bowie came back into their room and found Aofs corpse on the floor. 
Cee recalls that when she was a child, Bowie often tormented her, and now, Cee stated that her sister was trying to steal Karn from her too.  Cee killed Bowie with the stone. 

Karn returns to the hotel and saw that the power was down and told the staff to repair it. But when he goes to see Cee and Bowie in their room, Cee got a hallucination from Yings ghost, and mistaking Karn for Yings ghost, stabbed him with the knife to his chest.

There was a huge storm raging outside as Cee struggled to a nearby forest to dig a large grave to bury the three corpses, when she sees Ying coming for her. She wakes up and realised that she was just having a nightmare in the hotel room. However, she really did kill Aof, Bowie and Karn. She walks towards the grave site where it was surrounded by people and the police was already there digging out the bodies. She was horrified to see 4 bodies in the grave, and upon closer inspection, realised that she, herself had died in the grave as a tombstone fell and hit her unconscious. 

In the post-credits, a man becomes angry when he watched the news about the tragedy and breaks his baseball bat. It is revealed that he is Cees ex-boyfriend. 
She had dumped him for Aof, when the man leaves his home to call the police, Cees ghost watches with a guilty look.

==Cast==
*Ratchawin Wongviriya  ...  Cee
*Atthama Chiwanitchapan  ...  Bowie
*Thongpoom Siripipat  ...  Aof
*Marion Affolter  ...  Ying
*Pete Thongjua  ...  Karn
*Ananda Everingham ... Cees ex-boyfriend

==External links==
*  
* 

 
 
 
 
 
 


 
 