Somewhere in England (film)
Somewhere in England is a 1940 British comedy film directed by John E. Blakeley and starring Frank Randle, Harry Korris and Winki Turner.  It follows the adventures of an anti-authoritarian private stationed in a military camp in the North of England during the Second World War. It was the first in the Somewhere film series, followed by its sequel Somewhere in Camp in 1942.

==Cast==
* Frank Randle - Pte. Randle
* Harry Korris - Sgt. Korris
* Winki Turner - Irene Morant
* Dan Young - Pte. Young
* Robbie Vincent - Pte. Enoch
* Harry Kemble - Cpl. Jack Kenyon
* John Singer - Bert Smith
* Sydney Moncton - Adjutant
* Percival Mackey Orchestra - Themselves

==References==
 

==Bibliography==
* Rattigan, Neil. This is England: British film and the Peoples War, 1939-1945. Associated University Presses, 2001.

==External links==
* 

 
 
 
 
 
 
 


 
 