Sunburst (film)
{{Infobox film
| name           = Sunburst
| image_size     =
| image	=	Sunburst FilmPoster.jpeg
| caption        =
| director       = James Polakof
| producer       = H. Kaye Dyal (co-producer) Jim Moloney (associate producer) Reginald Olson (associate producer) Ronald Peck (executive producer) James Polakof (producer) Donald Winchester (associate producer)
| writer         = James Keach (writer) James Polakof (writer) David Pritchard (writer)
| narrator       =
| starring       =
| music          = Ed Bogas
| cinematography = Erik Daarstad
| editing        = John F. Link
| distributor    =
| released       = 1975
| runtime        = 74 minutes (USA)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Sunburst is a 1975 American film directed by James Polakof.

The film is also known as Slashed Dreams (American video title).

== Plot summary ==
A pair of students go on a trip up to the mountains to look for a friend who left school to find personal fulfillment apart from the normal of happenings of modern society finding a lost friend...

== Cast ==
*Peter Hooten as Robert
*Kathrine Baumann as Jenny
*Ric Carrott as Marshall Anne Lockhart as Tina
*Robert Englund as Michael Sutherland
*Rudy Vallee as Proprietor
*James Keach as Levon David Pritchard as Danker
*Randy Ralston as The Pledge
*Susan McCormick as Susan Peter Brown as The Professor

== Soundtrack ==
* Roberta Van Dere - "Pretty Things" (Words and Music by Ed Bogas)
* Roberta Van Dere - "Im Ready" (Words and Music by Ed Bogas)
* Roberta Van Dere - "Animals Are Clumsy Too" (Words and Music by Ed Bogas)
* Roberta Van Dere - "Take The Time" (Words and Music by Ed Bogas)
* Roberta Van Dere - "Mornin" (Words and Music by Ed Bogas)
* Roberta Van Dere - "Theme From Sunburst" (Words and Music by Ed Bogas)

== External links ==
* 
* 

 
 
 
 
 
 