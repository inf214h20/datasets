Turn the Key Softly
 
 
{{Infobox film
| name           = Turn the Key Softly
| image          = Turnthekeysoftly.jpg
| caption        = Jack Lee
| producer       = Maurice Cowan
| writer         = Jack Lee Maurice Cowan
| based on       =  
| starring       = Yvonne Mitchell Joan Collins Kathleen Harrison Terence Morgan
| cinematography = Geoffrey Unsworth
| music          = Mischa Spoliansky
| distributor    = General Film Distributors
| released       =  
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
}}
 Jack Lee and starring Yvonne Mitchell, Joan Collins, Kathleen Harrison and Terence Morgan. Lee and producer Maurice Cowan also wrote the screenplay, based on the novel of the same name by John Brophy, dealing with the first 24 hours of freedom for three women released on probation from prison on the same morning.

==Plot== Holloway Prison together at the end of their respective sentences. Monica Marsden (Mitchell) is a well-bred young woman, led into the world of crime by her smooth-talking but crooked lover David (Terence Morgan), who martyred herself by taking the fall for a crime he masterminded; Stella Jarvis (Collins) is a beautiful working-class girl who found her attractive appearance made West End prostitution a source of easy money before being imprisoned after disregarding numerous cautions for soliciting; Mrs. Quilliam (Harrison) is a kindly elderly widow living in poverty and jailed for repeat shoplifting offences. Monica proposes that the three should meet up later for dinner for which she will pay, to discuss how their first day of freedom has gone. Events before and after the dinner are explored as the film switches focus between each protagonist and how they set about picking up their lives.

During her incarceration, Monica has had time to reflect and initially is full of thoughts of vengeance towards David for allowing her to take the blame while he walked away scot free. When she meets up with him however, she finds her old feelings returning with a vengeance and starts once again to fall under his spell. Stella is full of good intentions to get back on track and turn her life around, away from the lure of her previous sordid but lucrative profession. Mrs. Quilliam, disregarded and ignored by her family, returns to her spartan home, consoled by the mongrel dog she loves and regards as the most important presence in her life. Over the course of 24 hours, each faces a struggle with herself to avoid a quick return to her recidivist ways. David still exerts a powerful hold over Monica; Stella, despite herself, is drawn back to her old haunts and their promise of maximum financial gain for least endeavour; Mrs. Quilliam has no money but somehow has to provide for herself and the dog. By the next day, two of the women have succeeded in resisting the temptations put in their path, but the third finds herself back behind bars in the most ironic of circumstances.

==Reception==
Turn the Key Softly received very positive reviews from contemporary critics, who noted with approval its realism and honesty; also its avoidance of the twin pitfalls in a storyline of this nature of either overly sentimentalising its characters or attempting to spice up proceedings with over-the-top melodrama or unnecessary plot twists and digressions. All three leading actresses were praised for their portrayals, with Harrison in particular singled out as giving a memorable and touching performance. Only a fall-back on coincidence as a plot device was mentioned as a minor weakness.

 , Henry Ward, said: "Turn the Key Softly is the kind of movie that apparently can only be made in Britain.  It is a warm, sympathetic sort of movie that is sentimental without being sticky or maudlin, a well-paced melodrama that never falls back on over-dramatics for effect."  He described Mitchell as "appealing", Collins as "excellent" and Harrison as "superb", concluding that the film "came to our town with a minimum of fanfare.  It doesnt need it.  It has a good story told with fine acting." 

==Cast==
* Yvonne Mitchell as Monica Marsden
* Terence Morgan as David
* Joan Collins as Stella Jarvis
* Kathleen Harrison as Mrs. Quilliam
* Thora Hird as Mrs. Rowan
* Dorothy Alison as Joan
* Glyn Houston as Bob
* Geoffrey Keen as Mr. Gregory
* Russell Waters as George Jenkins
* Clive Morton as Walters
* Richard Massingham as Bystander
* Hilda Fenemore as Mrs. Quilliams Daughter
* Simone Silva as Marie
* Toke Townley as Prison Guard

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 