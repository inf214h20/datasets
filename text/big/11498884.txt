Sparkling Red Star (2007 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name = Sparkling Red Star
| image = Sparkling Red Star poster.jpg
| caption =
| imdb_rating =
| director = Dante Lam
| producer =
| writer =
| starring =
| music =
| cinematography =
| editing =
| distributor = Asia Animation Ltd.
| released = October, 2007
| runtime = 90 mins China
| Mandarin
| budget =
| preceded_by =
| followed_by =
| mpaa_rating =
| tv_rating =
}}
 2007 Chinese Chinese animated film produced by Puzzle Animation Studio Limited.

==Background== film in 1974 during the Cultural Revolution.

==Story== Red Armys Long March. In Liuxi Village in Jiangxi, an innocent and cheerful child, Pan Dongzi (盘东子), spent his carefree childhood. However, after reaching 10 years of age, he began to experience the sorrows and joys of lifes partings and reunions.

Pans father was a Red Army sympathizer. He, along with other villagers, have been partitioning grain harvests out into two sections, one to be handed over to the land lord, the other to be saved for the Red Army, who was due to pass through their village. When Hu Hansan, the tyrannical landlord of the village, found out about this, Pans father was captured. The Red Guards of the Red Army show up just in time to save Pans father and since then, Pan worshiped Xiuzhu, the Captain of Red Guards. The villagers of Liuxi Village also enjoyed a period of happy days under the protection of the Red Guards.

However, good times did not last long. The Red Army had to set off on the Long March, and Pans father went with them. Before father and son parted, Pans father gave him a red star badge as a symbol of encouragement. After his father left, only a few members of the Red Guard were left stationed in Liuxi Village. Hu the traitor, hired a number of cold-blooded killers and returned to Liuxi Village. He occupied Liuxi Village again, and Pan had no choice but to flee with his mother by following the Red Guards.

Regaining his control over the village, Hus not only back to his old tricks of bullying the villagers, he also plans on making a profit by buying and selling weapons (presumably to the enemies of the Red Army, though its not clearly stated). The remaining Red Guards in Liuxi feel this poses a great threat to other Red Guards on the frontlines and decide they need to destroy Hus radio so he can longer contact his suppliers or buyers. Pan manages to sneak into Hus quarters and steal the radio but he inadvertently leads Hu and his henchmen to the Red Guard hideout.

A fight between the Red Guards and Hu and his men break out. Pans mother, joining the Red Guards in fight, is shot and killed. From that time, Pan grew up experiencing failures and frustrations with neither of his parents by his side. Pan constantly strove to become stronger, and gradually became a tough person. He had changed from a stubborn child into a brave and passionate young man.

==External links==
*  

 

 
 
 
 
 
 