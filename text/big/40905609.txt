Victoria L
 
{{Infobox film
| name           = Victoria L
| image          = 
| caption        = 
| director       = Petter Vennerød Svend Wam
| producer       = Petter Vennerød Svend Wam
| writer         = 
| starring       = Wenche Foss
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}
 Best Foreign Language Film at the 55th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Wenche Foss as Victoria Lund
* Pål Øverland as Carl
* Arne Bang-Hansen as Hilmar
* Monna Tandberg as Beatrice

==See also==
* List of submissions to the 55th Academy Awards for Best Foreign Language Film
* List of Norwegian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 