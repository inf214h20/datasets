The Jewess of Toledo (film)
{{Infobox film
| name           = The Jewess of Toledo 
| image          = 
| image_size     = 
| caption        = 
| director       = Otto Kreisler 
| producer       = 
| writer         = Franz Grillparzer (play)   Robert Land
| starring       = Franz Höbling   Ida Norden   Thea Rosenquist   Josef Viktora
| music          = 
| editing        =
| cinematography =
| studio         = Helios Film 
| distributor    =  Josef Rideg Film
| released       = 1 August 1919
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
}} German silent silent historical historical drama film directed by Otto Kreisler and starring Franz Höbling, Ida Norden and Thea Rosenquist. It is an adaptation of the 1872 play The Jewess of Toledo by Franz Grillparzer which was based on the relationship between Alfonso VIII of Castile and Rahel la Fermosa in 12th Century Spain. 

==Cast==
* Franz Höbling as Alfonso VIII of Castile 
* Ida Norden as Eleanor of England, Queen of Castile 
* Leopold Iwald as Count Manrique de Lara 
* Josef Viktora as Garzarrah
* Theodor Weiß as Isaak
* Emmy Flemmich as Esther 
* Thea Rosenquist as Rahel

==References==
 
==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 