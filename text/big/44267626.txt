Superbeast (film)
{{Infobox film
| name           = Superbeast
| image          = Superbeast & Daughters of Satan poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George Schenck
| producer       = George Schenck 
| writer         = George Schenck 
| starring       = Antoinette Bower Craig Littler Harry Lauter Vic Díaz Jose Romulo
| music          = Richard LaSalle
| cinematography = Nonong Rasca 
| editing        = Anthony DiMarco 
| studio         = A & S Productions
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Superbeast is a 1972 American horror film written and directed by George Schenck. The film stars Antoinette Bower, Craig Littler, Harry Lauter, Vic Díaz and Jose Romulo. The film was released on November 1, 1972, by United Artists.   It was released as a double feature with Daughters of Satan.

==Plot==
 

== Cast ==	 
*Antoinette Bower as Dr. Alix Pardee
*Craig Littler as Dr. Bill Fleming
*Harry Lauter as Stewart Victor
*Vic Díaz as Mondo Diaz
*Jose Romulo as Vigo
*John Garwood as Ray Cleaver
*Manny Ojeda as Dr. Raul Rojas
*Bruno Punzalan as Datu
*Alex Flores as Sloco
*Roderick Paulate as Pepe
*Ricardo Santos as Benny
*Nanita as Lupe

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 