A Jihad for Love
 
{{Infobox film
| name           = A Jihad for Love
| image          = A_Jihad_for_Love_Poster.jpg
| caption        = First Run Features poster for A Jihad for Love (US)
| director       = Parvez Sharma
| producer       = Sandi Simcha DuBowski, Shama Parvez Ioannis Mookas, assistant
| writer         =
| starring       =
| music          = Sussan Deyhim Richard Horowitz supervised by Ramsay Adams Abe Velez
| cinematography = Parvez
| editing        = Juliet Weber
| distributor    = First Run Features (U.S.)
| released       =  
| runtime        = 81 minutes 
| country        = United States
| language       = English, Arabic, Persian language|Persian, Urdu, Hindi, French, Turkish language|Turkish, etc.
| budget         =
| gross          = $105,651
}}
A Jihad for Love (also known by the working title In the Name of Allah) is a 2007 documentary film on the coexistence of Islam and homosexuality. {{Cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title =A Jihad for Love
  | work =
  | publisher =ajihadforlove.com
  | date =
  | url =http://www.ajihadforlove.com
  | format =
  | doi =
  | accessdate = June 13, 2007 }}  The film is directed by Parvez Sharma, and produced by Shama and Trembling Before G-d director Sandi DuBowski.

==Production== Logo (US) SBS (Australia).

The documentary was filmed in 12 different countries and in nine languages. {{Cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title =A Jihad for Love: Excerpts From A Work-In-Progress
  | work =
  | publisher =Miami Gay & Lesbian Film Festival
  | date =
  | url =http://www.mglff.com/16filmsbydate_loveisjihad.html
  | doi = archiveurl = archivedate = June 9, 2007}}  {{Cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title = A Jihad for Love
  | work =
  | publisher =Hartley Film Foundation
  | year =2007
  | url = http://hartleyfoundation.org/jihad-for-love
  | format =
  | doi =
  | accessdate = February 18, 2007 }}  Shama conducted interviews throughout North America, Europe, Africa, Asia and the Middle East. Countries included Saudi Arabia, Iran, Iraq, Pakistan, Egypt, Bangladesh, Turkey, France, India, South Africa, the United States and the United Kingdom.  He found many of his interviewees online, and received thousands of emails. {{Cite news
  | last = Hays
  | first = Matthew
  | coauthors =
  | title = Act of Faith: A Film on Gays and Islam
  | work =
  | pages =
  | publisher = The New York Times
  | date = November 2, 2004
  | url = http://www.nytimes.com/2004/11/02/movies/02alla.html?ex=1257138000&en=65bd923d140d39c0&ei=5090&partner=rssuserland
  | accessdate = February 5, 2007 }}
 

The film premiered at the Toronto International Film Festival in September 2007, and has been screened to great acclaim at several film festivals around the world. It was the Opening film for the prestigious Panorama Dokumente section of the Berlin Film Festival in February, 2008. The U.S. theatrical release was May 21, 2008 at the IFC Center in New York City. The film screened at the Frameline Film Festival in San Francisco on June 28, 2008, and the Tokyo International Lesbian & Gay Film Festival on July 13, 2008.

==Significance of the title==
The title A Jihad for Love refers to the Islamic concept of jihad, as a religious struggle. The film seeks to reclaim this concept of personal struggle, as it is used in the media almost exclusively to mean "holy war" and to refer to violent acts perpetrated by extremist Muslims.

The film has gone by several titles, beginning with the official working title, In the Name of Allah. {{Cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title =In the Name of Allah
  | work =
  | publisher =tremblingbeforeg-d.com
  | date =
  | url =http://www.ajihadforlove.com/
  | format =
  | doi =
  | accessdate = June 13, 2007}} 

Among Muslims, the phrase (bismillah in Arabic) may be used before beginning actions, speech, or writing. Its most notable use in Al-Fatiha, the opening passage of the Quran, which begins Bismillahi r-Rahmani r-Rahim. All  surahs of the Quran begin with "Bismillahi r-Rahmani r-Rahim," with the exception of the At-Tawba|ninth.
 as in Jewish tradition. Allah is the name of God in Islam and Arabic language|Arabic, and it is often used among Muslims residing in Muslim countries and monotheists in Arabic speaking countries.

==Controversy and problems==
Shamas making of the film has not been without criticism.

 

Shama refuses to associate homosexuality with shame, but recognizes the need to protect the safety and privacy of his sources, by filming them in silhouette or with their faces blurred. In one case, the family of an Afghan woman he interviewed "would undoubtedly kill her" if they found out she was lesbian. In another example, one of the associate producers, an Egyptian gay man, chose not to be listed in the credits for fear of possible consequences. 

The film was banned from screening at the 2008 Singapore International Film Festival "in view of the sensitive nature of the subject that features Muslim homosexuals in various countries and their struggle to reconcile religion and their lifestyle," Amy Chua, Singapore Board of Film Censors chairwoman was quoted as saying by The Straits Times. {{Cite web
  | last = Associated Press
  | first =
  | authorlink = Associated Press
  | coauthors =
  | title =Singapore censors ban films on terrorism, homosexual, fetish
  | work =
  | publisher = The China Post
  | date =
  | url =http://www.chinapost.com.tw/asia/singapore/2008/04/06/150637/Singapore-censors.htm
  | doi =
  | accessdate = March 8, 2008}} 

==Critical reception==
As of April 6, 2015, the review aggregator Rotten Tomatoes reported that 76% percent of critics gave the film positive reviews, based on 32 reviews.  Metacritic reported the film had an average score of 55 out of 100, based on six reviews — indicating mixed or average reviews. 

==See also==
 
* Islam and homosexuality
* Homosexuality and religion
* Gay Muslims (2006), a Channel 4 TV documentary about gay and lesbian Muslims in Britain
* Trembling Before G-d (2001), a documentary film directed Jihad for Love producer Sandi Simcha DuBowski, about Orthodox Jews who are gay or lesbian
* Fremde Haut  (2005), a film about an Iranian lesbian in Germany
* Love Jihad
* 2007 in film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  , Hartley Film Foundation
*  , article in The Guardian

 
 
 
 
 
 
 
 
 
 
 