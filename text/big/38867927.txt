Rise of the Zombie
 
 
{{Infobox film
| name=Rise Of The Zombie 
| image=
| image_size = 
| alt=
| caption=Theatrical release poster
| director=Devaki Singh & Luke Kenny
| producer=BSI Entertainment, Kenny Media
| story=
| based on=
| screenplay=
| starring=Luke Kenny Kirti Kulhari Ashwin Mushran Benjamin Gilani
| music=
| cinematography=Murzy Pagdiwala
| editing=Sanjeev Rathore, Parthasarthy Iyer
| studio=
| distributor=Reliance Entertainment
| released=  
| runtime=90 Mins approx
| country=India
| language=Hindi
}}
 2013 Hindi Horror film directed by Luke Kenny and Devaki Singh. The film starred Luke Kenny, Kirti Kulhari and Ashwin Mushran.

==Cast==
*Luke Kenny
*Kirti Kulhari
*Ashwin Mushran
*Benjamin Gilani

==Critical Reception==
Rise of the Zombie had a limited release, but received positive to mixed reviews from critics. Swati Deogire of in.com gave it a positive review, praising Luke Kennys performance.      On the other hand, Rohit Vats of IBNLive gave the film only two out of five, saying that it "could have become a better film if the makers would have devised a way of showing a connection between the gloomy environment of the mountains and the mystical origin of the zombie."     

==References==
 


==External links==
*  

 
 
 

 