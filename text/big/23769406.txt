Sex and the Single Girl (film)
{{Infobox film
| name           = Sex and the Single Girl
| image          = Poster of the movie Sex and the Single Girl.jpg
| caption        = original theatrical poster
| director       = Richard Quine
| producer       = William T. Orr
| writer         = Joseph Heller Helen Gurley Brown  (book) 
| starring       = Tony Curtis Natalie Wood Henry Fonda Lauren Bacall Mel Ferrer
| music          = Neal Hefti
| cinematography = Charles Lang
| editing        = David Wages
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $4,000,000  (rentals)  
}}
Sex and the Single Girl is a 1964 American comedy film directed by Richard Quine and starring Tony Curtis, Natalie Wood, Henry Fonda, Lauren Bacall, and Mel Ferrer.     

The film was inspired by the title of the non-fiction book Sex and the Single Girl (1962) by Helen Gurley Brown. 

==Plot==
Bob Weston works for STOP, a scandal magazine. His colleague has written a dirty article about Helen Gurley Brown, the author of a best-selling new book with advice to single women on how to deal with men, calling her a virgin. She is very offended. Due to this article, Helen has lost six appointments.  Weston wants to interview her, but she refuses to see him.

A friend of Bob, Frank Broderick, has marriage issues. Bob gets an idea that could benefit both of them.  He impersonates Frank to be qualified as a patient to find juicy material. He also attempts to seduce her to get more information. Helen is seen to return his affections, but resists, knowing hes married, and dates Rudy DeMeyer instead. Bob claims hes bound for a divorce, so Helen insists on meeting his wife.

By mistake, Bob persuades two women, secretary Susan and girlfriend Gretchen, to masquerade as Franks wife. When word gets to Franks actual wife, Sylvia, whats going on, she shows up in Helens office as well. Frank is arrested for bigamy.

Bob has juicy material for the magazine, but has fallen in love with Helen and refuses to write it, losing his job. A chase and a comedy of errors leads to happy endings all around, Rudy even ending up with Gretchen.

==Reaction== top 20 highest grossing films of 1964.  Tom Milne in the Time Out Film Guide 2009 describes the film as a " oyly leering comedy...graceless stuff, criminally wasting Bacall and Fonda as a couple with marital problems...with Quines moderate flair for comedy nowhere in evidence"  and with "noise substituting for wit and style" according to Halliwells Film & Video Guide. 

==Cast==
* Tony Curtis as Bob Weston
* Natalie Wood as Helen Brown
* Henry Fonda as Frank
* Lauren Bacall as Sylvia
* Mel Ferrer as Rudy
* Fran Jeffries as Gretchen
* Leslie Parrish as Susan
* Larry Storch as The Motorcycle Cop
* Edward Everett Horton as The Chief

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 