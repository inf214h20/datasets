Paws (film)
{{Infobox film
| name           = Paws
| image          =
| image_size     =
| caption        =
| director       = Karl Zwicky
| producer       = Rebel Penfold-Russell Andrena Finlay Vicki Watson
| writer         = Harry Cripps Karl Zwicky Emilie François Sandy Gore Heath Ledger
| music          = Mario Millo
| cinematography = Geoff Burton
| editing        = Nicholas Holmes
| distributor    = PolyGram Filmed Entertainment
| released       =    (Australia) ,    (UK) 
| runtime        = 83 minutes
| country        = Australia / UK
| language       = English
| budget         =
| gross          = £137,731 (UK, opening weekend) A$455,171 (Australia) 
| preceded_by    =  
| followed_by    =  
}}
 
Paws is an independent 1997 Australian family film that was released on 25 September 1997  in Australia and filmed in Sydney, New South Wales.

The film stars 15-year-old guitarist Nathan Cavaleri who has adventures with PC - a talking Jack Russell Terrier (voiced by comedian Billy Connolly).    The dog is computer literate - skills acquired from his former master allowing him to create a computer program that translates his words into English, with Zac subsequently designing a portable version that can be concealed in a bow tie- and the pair must a stop a valuable disk from falling into bad hands.   

==Cast==
*Billy Connolly - Voice of PC
*Nathan Cavaleri - Zac Emilie François - Samantha
*Sandy Gore - Anja
*Joe Petruzzi - Stephen
*Caroline Gillmer - Susie
*Rachael Blake - Amy
*Norman Kaye - Alex
*Freyja Meere - Binky
*Kevin Golsby - Commentator
*Rebel Penfold-Russell - Carla
*Alyssa-Jane Cook - Trish
*Daniel Kellie - Puck
*Matthew Krok - Bottom
*Gezelle Byrnes - Agnes Richard Carter - Cafe Owner
*Ben Connolly - Billy
*Charles Conway - Priest
*Julie Godfrey - Sister Deidre
*Denni Gordon - Dog hairdresser
*Heath Ledger - Oberon
*David Nettheim - Rabbi
*Gulliver Page - Fan Operator
*Nick White - Zacs father

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 


 
 