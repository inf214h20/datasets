Bangkok Traffic Love Story
 
{{Infobox film
| name           = Bangkok Traffic Love Story
| image          = Bangkok Traffic love Story poster.jpg
| caption        = Theatrical poster
| director       = Adisorn Tresirikasem
| producer       = Jira Maligool Chenchonnee Soonthornsarntool Suwimol Taechasupinun Wanruedee Pongsittisak
| writer         = Benjamaporn Srabua Navapol Thamrongruttanarit Adisorn Tresirikasem
| starring       =  Cris Horwang Theeradej Wongpuapan Ungsumalynn Sirapatsakmetha
| music          = Instinct
| cinematography = Somboon Piriyapakdeekul Jira Maligool
| editing        = Vichapat Gojew Thammarat Sumetsupachok Panayu Khunwallee
| distributor    = GMM Tai Hub
| released       =  
| runtime        =
| country        = Thailand
| language       = Thai language|Thai, Min Nan
| budget         =
| gross          =
}} Thai romantic comedy film released by GTH on 15 October 2009. It was directed by Adisorn Tresirikasem and written by Navapol Thamrongruttanarit.   The film tells the story of Mei Li (Cris Horwang), a thirty-year-old woman feeling desperate about being last among her friends to marry, and her relationship with Loong (Theeradej Wongpuapan), an engineer working on the BTS Skytrain system. The skytrain, which celebrated its tenth anniversary the same year and lends its name to the films Thai and English titles, is prominently featured throughout the story.

The film was criticized for its loose plot, but critics felt that young middle-class female Bangkokians could identify with the contemporary setting. The film was financially successful, earning 57 million baht on its opening weekend and over 140 million baht after four weeks.

==Plot==
On Peds (Panisara Pimpru) wedding night, her close friend, Mei li or Li (Cris Horwang), had drunk too much wine and had fallen asleep in Peds hotel room. Li woke up in the early hours, she drove back to her home however she had a car accident on her way and this was where she met Loong (Theeradej Wongpuapan). When Li arrives home, her family scolds her, and her father forbids her to drive, so Li has to go to work by public transport.

One night, Li wakes up in the middle of night and goes to the roof of her house. She accidentally catches her maid and her boyfriend, who are also on the roof. Her maids boyfriend calls his elder (responsible adult), who turns out to be Loong. Li next meets Loong again at a BTS Skytrain station. This time Li accidentally breaks Loongs sunglasses. Li buys a replacement pair of sunglasses and writes her number on the box. She arranges to bump into Loong to give him the new sunglasses. After waiting for Loong to call, Li goes to a video rental shop that Loong visits regularly. Lis neighbor, Plern (Ungsumalynn Sirapatsakmetha), uses a trick to get Loongs mobile number, but later goes to work at the same video store instead of giving Loongs number to Li. As revenge, Li uses Plerns mobile to send messages to Plerns three boyfriends, inviting them all to the store at the same time. Loong comes into the shop, and accidentally drops his laptop.

Feeling responsible, Li take Loongs laptop to Peds husband, but he cant fix it. Li goes to return it at the BTS office. When Loong finishes work, he finds Li, and they ride the BTS to get home. Loong decides to throw away the laptop and its bag. Li picks the bag up out of the garbage bin and takes it home. There are many things inside, including film negatives. Li has the film printed, and finds there are pictures of Loong with Kob Kavita (Taksaorn Paksukjareon), an actress in "Saints Tear", a popular television series. The photo printing shop owner posts the pictures on the internet, as they are of a famous actress.
 Songkran holidays, Loong asks Li to come celebrate by throwing water.

During the Songkran festival, Plern joins them. Li doesnt enjoy the festivities because of her. Li knows Loongs address, which is a guesthouse next to Chao Phraya River. Li changes her clothes and goes to see Loong, and finds him asleep. Li falls asleep next to him. After she wakes up, Loong asks her to travel around Bangkok. Loong asks Li to come to family day at the BTS, as he can take her into the depot. Loong takes pictures, but Li damages the camera.

On the family day visit, Li finds out that Loong is leaving in two days time to spend two years studying in Germany. They say goodbye on the Taksin Bridge. Loong sends Li a box when he arrives in Germany. Inside is the mirror from her car from when they first met, the damaged sunglasses, the broken laptop, Bangkok Planetarium tickets, and the damaged camera, with the memory card still inside. Li looks at the pictures. She rushes to Suvarnabhumi Airport to try to stop Loong, but she is too late. On that day, the comet orbits to the earth. Loong watches the comet from on board his plane, while Li watches it as well.

Two years later, while going to work one evening, Li accidentally meets Loong on a BTS Skytrain. Loong works a day shift and has been back in Thailand for a few months. Both get off the Skytrain at Siam station, which is the interchange station between the Sukhumvit Line and the Silom Line. Li goes downstairs to change to a different line and doesnt turn back to look at Loong. Li gets on her train, but the electricity goes out. Passengers call their friends or family to say the train has stopped. Lis phone rings; it is Loong. He asks Li to again celebrate Songkran. Li replies that she is free for the holidays. When electricity comes back on, Loong is on the same Skytrain, standing next to Li. He tells Li that she has his number now, and to record it.

==Production==
Bangkok Traffic Love Story was sponsored by the BTS, and the movie includes many scenes depicting maintenance work on the system and its infrastructure. The film was promoted as part of BTSs tenth anniversary celebrations.  The films Thai name, Rod fai fah.. Ma Ha Na Ther, translates as "Skytrain, coming to meet you", and is a word play on Rot Fai Fa Maha Nakhon ( ), which is the Thai name of the Bangkok Metro underground system. The English title is abbreviated BTS to coincide with that of the skytrain system.

==Reception==

===Critical response===
Critics mostly noted how Bangkok Traffic Love Story, despite its flaws, appropriately served and satisfied its target audience of Bangkoks young female adults. In Manager Daily, Aphinan Bunrueangphanao noted the films chick flick elements, and how the casting of Theeradej as the male lead helped boosted the films appeal. He noted that Cris skilfully represented the humorously exaggerated single urban female in her lead role, but criticized the film for its weak and loose plot.    Nantakwang Sirasoontorn observed in Kom Chad Luek that the films most distinct theme was that of the female fantasy of meeting the perfect man. He noted that images of contemporary Bangkok life and GTHs carefully planned marketing campaigns helped propel the film to success. He commended Criss acting and criticized the weak plot. 

===Box office===
Bangkok Traffic Love Story was a box office hit upon release, earning 15.1 million baht on its opening day, surpassing Phobia 2 s prior annual record of 14.9 million, and 57 million baht during the opening weekend.    At four weeks, the film totalled over 140 million baht in theatrical earnings, surpassing Fan Chans prior GTH record of 137 million, and becoming by far the highest-grossing film of 2009.   

==Awards==
{| class="wikitable"
|-
! Award !! Category !! Result
|-
|rowspan="3"| Peoples Choice Awards 2009
| Best Picture
|  
|-
| Best Actor (Theeradej Wongpuapan)
|  
|-
| Best Actress (Cris Horwang)
|  
|-
|rowspan="3"| 7th Kom Chad Luek Awards
| Best Picture
|  
|-
| Best Director
|  
|-
| Best Actress (Cris Horwang)
|  
|-
|rowspan="5"| Top Awards 2009
| Best Picture
|  
|-
| Best Director
|  
|-
| Best Actress (Cris Horwang)
|  
|-
| Best Actor (Theeradej Wongpuapan)
|  
|-
| Favourite Breakout Movie Actress (Ungsumalynn Sirapatsakmetha)
|  
|-
|rowspan="2"| 7th Starpics Thai Film Awards
| Best Picture
|  
|-
| Best Actress (Cris Horwang)
|  
|-
|rowspan="3"| 3rd Chalerm Thai Awards
| Best Picture of the Year
|  
|-
| Best Actor of the Year (Theeradej Wongpuapan)
|  
|-
| Best Actress of the Year (Cris Horwang)
|  
|-
|rowspan="6"| 18th Bangkok Critics Assembly Awards
| Best Director
|  
|-
| Best Actress (Cris Horwang)
|  
|-
| Best Film Directing
|  
|-
| Best Film Editing
|  
|-
| Best Art Direction
|  
|-
| The Highest-Grossing Film
|  
|-
|rowspan="3"| 19th Thai National Film Awards
| Best Actress (Cris Horwang)
|  
|-
| Best Costume Design
|  
|-
| Best Makeup
|  
|-
|rowspan="2"| 3rd Nine Entertain Awards
| Best Picture of the Year
|  
|-
| Best Actress (Cris Horwang)
|  
|-
|rowspan="7"| 7th Hamburger Awards
| Best Picture
|  
|-
| Best Director
|  
|-
| Best Actress (Cris Horwang)
|  
|-
| Best Supporting Actress (Ungsumalynn Sirapatsakmetha)
|  
|-
| Favourite Scene-Stealing Actor (Charlie Trairat)
|  
|-
| Favourite Scene-Stealing Actor (Sunny Suwanmethanont)
|  
|-
| Best Original Song
|  
|}

==References==
 

 
 
 
 
 
 