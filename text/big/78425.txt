Raise the Red Lantern
{{Infobox film
| name           = Raise the Red Lantern
| image          = Raise the Red Lantern DVD.jpg
| caption        = Front of DVD release
| director       = Zhang Yimou
| producer       = Hou Hsiao-hsien Chiu Fu-sheng Zhang Wenze
| writer         = Ni Zhen
| based on       =  
| starring       = Gong Li
| music          = Zhao Jiping
| cinematography = Zhao Fei
| editing        = Du Yuan
| distributor    = Orion Classics
| released       =  
| runtime        = 125 minutes
| country        = China Hong Kong Taiwan
| language       = Mandarin
| gross          = $2,603,061 (United&nbsp;States) 
}}
Raise the Red Lantern ( ) is a 1991 film directed by Zhang Yimou and starring Gong Li. It is an adaption by Ni Zhen of the 1990 novel Wives and Concubines by Su Tong. The film was later adapted into an acclaimed ballet of the same title by the National Ballet of China, also directed by Zhang.

Set in the 1920s, the film tells the story of a young woman who becomes one of the concubines of a wealthy man during the Warlord Era. It is noted for its opulent visuals and sumptuous use of colours.  The film was shot in the Qiao Family Compound near the ancient city of Pingyao, in Shanxi Province. Although the screenplay was approved by Chinese Censorship|censors,  the final version of the film was banned in China for a period.   Some film critics have interpreted the film as a veiled allegory against authoritarianism. 

==Plot==
The film is set in 1920s China during the Warlord Era, years before the Chinese Civil War. Nineteen-year-old Songlian (Sònglián, played by Gong Li), whose father has recently died and left the family bankrupt, marries into the wealthy Chen family, becoming the fourth wife or rather the third concubine or, as she is referred to, the Fourth Mistress (Sì Tàitai) of the household. Arriving at the palatial abode, she is at first treated like royalty, receiving sensuous foot massages and brightly lit red lanterns, as well as a visit from her husband, Master Chen (Ma Jingwu), the master of the house, whose face is never clearly shown.

Songlian soon discovers, however, that not all the concubines in the household receive the same luxurious treatment. In fact, the master decides on a daily basis the concubine with whom he will spend the night; whomever he chooses gets her lanterns lit, receives the foot massage, gets her choice of menu items at mealtime, and gets the most attention and respect from the servants. Pitted in constant competition against each other, the three concubines are continually vying for their husbands attention and affections.

The First Mistress, Yuru (Jin Shuyuan), appears to be nearly as old as the master himself. Having borne a son decades earlier, she seems resigned to live out her life as forgotten, always passed over in favor of the younger concubines. The Second Mistress, Zhuoyun (Zhuóyún, Cao Cuifen), befriends Songlian, complimenting her youth and beauty, and giving her expensive silk as a gift; she also warns her about the Third Mistress, Meishan (Méishan, He Caifei), a former opera singer who is spoiled and who becomes unable to cope with no longer being the youngest and most favored of the masters playthings. As time passes, though, Songlian learns that it is really Zhuoyun, the Second Mistress, who is not to be trusted; she is subsequently described as having the face of the Buddha, yet possessing the heart of a scorpion.

Songlian feigns pregnancy, attempting to garner the majority of the masters time and, at the same time, attempting to become actually pregnant. Zhuoyun, however, is in league with Songlians personal maid, Yaner (Yànér, played by Kong Lin) who finds and reveals a pair of bloodied undergarments, suggesting that Songlian had recently had her period, and discovers the pregnancy is a fraud.

Zhuoyun summons the family physician, feigning concern for Songlians "pregnancy". Doctor Gao (Gao-yisheng, Cui Zhigang), who is secretly having an illicit affair with Third Mistress Meishan, examines Songlian and determines the pregnancy to be a sham. Infuriated, the master orders Songlians lanterns covered with thick black canvas bags indefinitely. Blaming the sequence of events on Yaner, Songlian reveals to the house that Yaners room is filled with lit red lanterns, showing that Yaner dreams of becoming a Mistress instead of a lowly servant; it is suggested earlier that Yaner is in love with the Master and has even slept with him in the Fourth Mistress bed.

Yaner is punished by having the lanterns burned while she kneels in the snow, watching as they smolder. In an act of defiance, Yaner refuses to humble herself or apologize, and thus remains kneeling in the snow throughout the night until she collapses. Yaner falls sick and ultimately dies after being taken to the hospital. One of the servants tells Songlian that her former maid died with her mistresss name on her lips. Songlian, who had briefly attended university before the passing of her father and being forced into marriage, comes to the conclusion that she is happier in solitude; she eventually sees the competition between the concubines as a useless endeavor, as each woman is merely a "robe" that the master may wear and discard at his discretion.

As Songlian retreats further into her solitude, she begins speaking of suicide; she reasons that dying is a better fate than being a concubine in the Chen household. On her twentieth birthday, severely intoxicated and despondent over her bitter fate, Songlian inadvertently blurts out the details of the love affair between Meishan and Doctor Gao to Zhuoyun, who later catches the adulterous couple together. Following the old customs and traditions, Meishan is dragged to a lone room on the roof of the estate and hanged to death by the masters servants.

Songlian, already in agony due to the fruitlessness of her life, witnesses the entire episode and is emotionally traumatized. The following summer, after the Masters marriage to yet another concubine, Songlian is shown wandering the compound in her old schoolgirl clothes, having gone completely insane.

==Cast==
*Gong Li as Songlian (S:颂莲, T:頌蓮, P: Sònglián), the fourth mistress (四太太 Sì tàitai) He Caifei as Meishan (C:梅珊, P: Méishān), the third mistress (三太太 Sān tàitai)
*Cao Cuifen as Zhuoyun (S:卓云, T:卓雲, P: Zhuóyún), the second mistress  (二太太 Èr tàitai)
*Zhou Qi as housekeeper Chen Baishun (S: 陈百顺, T: 陳百順, P: Chén Bǎishùn)
*Lin Kong as Yaner (S: 燕儿, T: 燕兒, P: Yànér), Songlians young servant
*Jin Shuyuan as Yuru (C: 毓如, P: Yùrú), the first wife (大太太 dà tàitai)
*Ma Jingwu as Chen Zuoqian (S:陈佐千, T: 陳佐韆, Chén Zuǒqiān) or Master Chen
*Cui Zhihgang as Doctor Gao (S:高医生, T: 高醫生, P: Gāo-yīshēng)
*Xiao Chu as Feipu (S:飞浦, T: 飛浦, P: Fēipǔ), the masters eldest son
*Cao Zhengyin as Songlians old servant
*Ding Weimin as Songlians mother

==Soundtrack==
{{Infobox album  
| Name        = Raise the Red Lantern
| Type        = soundtrack
| Artist      = Zhao Jiping
| Cover       = RaiseOST.jpg
| Released    = 1994
| Recorded    =
| Genre       =
| Length      =
| Label       = Milan Records
| Producer    =
}}
All songs composed by Zhao Jiping.
# "Opening Credits/Prologue/Zhouyun/Lanterns"
# "First Night With Master/Alone on First Night Second Night Third Night"
# "Summer"
# "Flute Solo"
# "Record"
# "Autumn"
# "Births/The Peking Theme"
# "Pregnancy/Yaners Punishment"
# "Meishan Sings"
# "Young Master Returns Meishans Punishment"
# "Realization"
# "Winter"
# "Ghost"
# "Seasons"
# "Next Summer"
# "House of Death"
# "Fifth Mistress"
# "Songlians Madness/End Credits"

==Distribution==
Raise the Red Lantern has been distributed on VHS, Laserdisc and DVD by numerous different distributors, with many coming under criticism for their poor quality.

The Razor Digital Entertainment DVD release has been widely criticised. DVD Times states "Many other viewers will find this DVD release simply intolerable."  DVDTown criticised the same release, giving the video quality 1 out of 10 and the audio quality 6 out of 10, summarising that "the video is a disaster".  DVDFile adds to this stating "this horrible DVD is only recommended to those who love the movie so much, that they’ll put up with anything to own a Region 1 release."  The translation on this version has been also widely criticised for its numerous inaccuracies.   A release by Rajon Vision has also received poor commentary 
 Criterion edition with a new print or a full restoration, but in the absence of any likelihood of that, this Era Hong Kong edition is about as good as you could hope for."  DVDBeaver broadly agrees stating "Now, this is not Criterion image quality, but it is not bad at all. It is easily the best digital representation of this film currently available."    DVD Talk, though, believes that "This new version is a stunner". 

A new MGM release in 2007 has also received some positive feedback. 

==Reception==
Described as "one of the landmark films of the 1990s" by Jonathan Crow of Allmovie,  where it received 5 stars, since its release Raise the Red Lantern has been very well received. James Berardinelli named it his 7th best film of the 1990s.  It has a 96% certified fresh rating at Rotten Tomatoes    and TV Guide gave it 5 stars.  However, there was a small number of negative reviews. Hal Hinson of The Washington Post stated that "the story never amounts to much more than a rather tepid Chinese rendition of "The Women.""   The film ranked #28 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web
| title = The 100 Best Films Of World Cinema – 28. Raise the Red Lantern
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=28
| work = Empire
}}
 

The film has also been praised for its artistic merit. Desson Howe of The Washington Post states that "In purely aesthetic terms, "Raise the Red Lantern" is breathtaking"  and James Berardinelli states that "the appeal to the eye only heightens the movies emotional power".  John Hartl of Film.com describe it to be "a near-perfect movie that often recalls the visual purity and intensity of silent films." 

The film has been interpreted by some critics as a criticism of contemporary China, although Zhang Yimou himself has sternly denied this.  Jonathan Crow of Allmovie states that "the perpetual struggle for power that precludes any unity among the wives provides a depressingly apt metaphor for the fragmented civil society of post-Cultural Revolution China".  James Berardinelli makes a similar analogy in his review where he states that "Songlian is the individual, the master is the government, and the customs of the house are the laws of the country. Its an archaic system that rewards those who play within the rules and destroys those who violate them.".  Furthermore, in such a system, the innocent individual becomes the executer of new incoming victims, making ones outcome even more tragic, as it is analyzed in ThinkingChinese. 

Chinese journalist and activist Dai Qing has said that the film, along with many of Zhang Yimous earlier works, caters too much to Western taste; "this kind of film is really shot for the casual pleasures of foreigners". 

The films popularity has also been attributed to a resurgence in Chinese tourism after the government response to the Tiananmen Square Protests of 1989 due to its use of exotic locations. 
 25 movies you must see before you die.

==Awards and nominations==

===Wins===
*Silver Lion for Best Director - (1991 Venice International Film Festival)
*Best Foreign Language Film - (1992 New York Film Critics Circle)
*Best Foreign Language Film - (1992 David di Donatello for Best Foreign Film)
*Best Cinematography -  - Zhao Fei (1992 Los Angeles Film Critics Association) Grand Prix - (1993 Belgian Syndicate of Cinema Critics)
*Best Film Not in the English Language - (1993 British Academy Film Awards)
*Best Foreign Language Film - (1993 National Society of Film Critics)
*Best Foreign Language Film - (1993 London Film Critics Circle)
*Best Foreign Language Film - (1993 Kansas City Film Critics Circle)
*Best Cinematography - (1993 National Society of Film Critics)
*Best Actress (Gong Li) -  (1993 Hundred Flowers Awards)

===Nominations=== Academy Awards)
*Golden Lion for Best Film - (1991 Venice International Film Festival)
*Best Foreign Film - (1992 National Board of Review)
*Best Foreign Language Film - (1993 Independent Spirit Awards)
*Best Foreign Film - (28th Guldbagge Awards)   

==See also==
 
*List of films considered the best

==Further reading==
* "Chapter 2: Su Tong and Zhang Yimou: Womens Places in Raise the Red Lantern": Deppman, Hsiu-chuang. Adapted for the Screen: The Cultural Politics of Modern Chinese Fiction and Film. University of Hawaii Press, June 30, 2010. ISBN 0824833732, 9780824833732. p.&nbsp;32.
* Fried, Ellen J. - "Food, Sex, and Power at the Dining Room Table in Zhang Yimous Raise the Red Lantern." - In Bower, Anne Reel Food: Essays on Food and Film. Psychology Press, 2004. p.&nbsp;129-143. ISBN 0-415-97111-X, 9780415971119.
* Giskin, Howard and Bettye S. Walsh. An Introduction to Chinese Culture through the Family. SUNY Press, 2001. p.&nbsp;198-201.
* Hsiao, Li-ling. " ." ( ) Southeast Review of Asian Studies, University of North Carolina at Chapel Hill. Volume 32 (2010), pp.&nbsp;129–36.

==References==
 

==External links==
*  
*  
*  
* " ." The New York Times.
*  
* Howe, Desson. " ." The Washington Post. May 8, 1992.
* Ebert, Roger. " ." Chicago Sun-Times. April 27, 2003.

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 