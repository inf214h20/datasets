Those Who Dance (1924 film)
{{infobox film
| name           = Those Who Dance
| image          = Silver Sheet April 01 1924 - THOSE WHO DANCE.pdf
| imagesize      =
| caption        = Studio publication promoting film
| director       = Lambert Hillyer
| producer       = Thomas H. Ince
| story          = George Kibbe Turner
| writer         = Lambert Hillyer (adaptation) Arthur F. Slatter (adaptation)
| starring       = Blanche Sweet Bessie Love Warner Baxter
| cinematography = Sidney Hickox 
| editing        = Associated First National
| released       =  
| runtime        = 80 mins.
| country        = United States
| language       = Silent (English intertitles)
}}
 silent drama Associated First National, the film stars Blanche Sweet, Bessie Love, and Warner Baxter. It is based on a story by George Kibbe Turner.

 .

==Plot==
 
When a young lawyer (Warner Baxter)s sister is killed in a bootleg liquor-related accident, he seeks justice by joining the prohibition force. 

==Cast==
* Blanche Sweet as Rose Carney
* Bessie Love as Veda Anargas
* Warner Baxter as Bob Kane
* Robert Agnew as Matt Carney John Sainpolis as Monahan
* Lucille Ricksen as Ruth Kane Mathew Betz as Joe Anargas
* Lydia Knott as Mrs. Carney
* Charles Delaney as Tom Andrus
* W. S. McDunnough as Bob Kanes Father
* Jack Perrin as Frank Church
* Frank Campeau as "Slip" Blaney

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 