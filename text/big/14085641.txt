Brothers Five
 
 
{{Infobox film name = Brothers Five image = BrothersFive.jpg alt =  caption = Film poster traditional = 五虎屠龍 simplified = 五虎屠龙 pinyin = Wǔ Hǔ Tú Lóng}} director = Lo Wei producer = Runme Shaw writer = Ni Kuang Lo Wei starring =  music = Wang Fu-ling cinematography = Wu Cho-hua editing = Chiang Hsing-lung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 109 minutes country = Hong Kong language = Mandarin budget =  gross =
}}
Brothers Five is a 1970 Hong Kong wuxia film directed by Lo Wei and produced by the Shaw Brothers Studio. Cheng Pei-pei plays the role of Yen Lai, a woman who must reunite the Kao brothers to rid the Teng Lung Manor of killers whilst avenging the murder of their father.

==Cast==
*Cheng Pei-pei as Miss Yen Hsing-kung
*Lo Lieh as Kao Hsia
*Chang Yi as Scholar Kao Chih Yueh Hua as Valet Kao Wei Chin Han as Blacksmith Kao Hao
*Kao Yuen as Security chief Kao Yung
*Tien Feng as Master Lung Cheng-feng
*Unicorn Chan as Flying Fork Wang
*Wang Hsieh as Wang Liaoer
*Sammo Hung as Chu, escort service man
*Ku Feng as Lord Wan Bo-fu James Tien as Master Ting Zhi-shan
*Nam Wai-lit as Li Xiaosan
*Chin Chun as Wangs thug
*Lee Wan-chung as Butler Teng
*Lee Ka-ting as Master Yau
*Chow Siu-loi as Wang Fat
*Lee Sau-kei as Lungs servant
*Tsang Choh-lam as waiter
*Chu Gam as waiter
*Hao Li-jen as villager
*To Man-bo
*Someno Yukio as escort service man
*Ling Hon as servant
*Luk Chuen as Wans man
*Wu Chi-chin as Lungs gate guard
*Yeung Chak-lam as Lungs gate guard
*Kwan Yan as restaurant customer
*Wong Chi-ming as one of the Weird Eight
*Kei Ho-chiu as Lungs thug
*Hoh Wan as Lungs bodyguard
*Wong Kung-miu as villager
*Goo Chim-hung as villager
*Lam Yuen
*Gam Tin-chue
*Lam Ching-ying
*Sham Chin-bo
*Fuk Yan-cheng

== External links ==
* Brothers Five at  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 