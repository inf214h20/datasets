Sumurun
{{Infobox film
| name           = Sumurun
| image          = Sumurum-1920.jpg
| image_size     =
| caption        = Film poster
| director       = Ernst Lubitsch Paul Davidson
| writer         = {{plainlist|
* Hanns Kräly
* Ernst Lubitsch
}}
| narrator       =
| starring       = see below
| music          =
| cinematography = {{plainlist|
* Theodor Sparkuhl
* Fritz Arno Wagner
}}
| editing        =
| distributor    = UFA First National Pictures(U.S. release)
| released       =  
| runtime        = 103 minutes
| country        = Germany
| language       = German intertitles English intertitles(U.S. release)
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Sumurun (aka One Arabian Night) is a 1920 German silent film directed by Ernst Lubitsch based on a pantomime by Friedrich Freksa.

==Plot==
A company of travelling performers arrive at a fictional oriental city. It includes the beautiful dancer Janaia, the hunchback clown Yeggar who is lovesick for Janaia and the Old Lady who loves Yeggar. The Slave Trader Achmed wants to sell Janaia to the Sheik for his harem. At the Palace, the Sheik finds out that his favourite, Sumurun, is in love with Nur al Din, the handsome clothes merchant. He wants to condemn her to death but his son obtains her pardon. After seeing Janaia dancing, the Sheik is keen to buy her. Yeggar is desperate and takes a magic pill which make him look dead. His body is hidden in a chest. The women from the harem come to Nur al Dins shop and hide him in a chest so that he can be brought into the Palace. The chest containing Yeggars body is also brought to the Palace and the Old Lady manages to revive him. The Sheik finds Janaia making love to his son and kills both of them. He then finds Sumurun making love to Nur al Din and wants to kill them but he is stabbed in the back by Yagger. 

== Cast ==
*Paul Wegener as the Old Sheikh
*Carl Clewing as the Young Sheikh
*Jenny Hasselqvist as Sumurun
*Aud Egede Nissen as Haidee, a Servant
*Harry Liedtke as Nur-Al Din, the Cloth Merchant
*Paul Graetz as Puffti, a Servant
*Max Kronert as Muffti, a Servant
*Ernst Lubitsch as Yeggar, the Hunchback Beggar
*Margarete Kupfer as an Old Woman
*Pola Negri as Yannaia, a Dancer
*Paul Biensfeldt as Achmed, the Slave Trader
*Jakob Tiedtke as Head Eunuch

==Production==
The filming of Sumurun began at the Ufa studios Union Berlin Tempelhof on 13 March 1920. The monumental sets were realised by Kurt Richter and Erno Metzner. The costumes were designed by Ali Hubert. This is the last film in which Ernst Lubitsch was starring. Sumurun was classified by the Film Censors Office as not suitable for minors. The première took place on 1 September 1920 in the Ufa-Palast am Zoo in Berlin. 

==Reception==
In Germany, Sumurun was highly praised by contemporary critics and was described as "a cinematic journey into a universe of emotions and passions of great intensity and utter perfection, with a remarkable Ernst Lubitsch in one of the main roles." 

In America, the New-York Times wrote that "One Arabian Night" (the title under which Sumurun was released) gave added evidence that Ernst Lubitsch "is the superior of most directors anywhere, and that Pola Negri, a Polish-German actress, is one of the few real players of the screen who can make a character live and be something other than an actress playing a part." It concluded that, despite some shortcomings, it remained one of the years best pictures. 

==DVD releases== Kino Lorber as part of the box set "Lubitsch in Berlin" in 2007 with English intertitles. It was also released in the UK by Eurekas Masters of Cinema series as part of the box set "Lubitsch in Berlin: Fairy-Tales, Melodramas, and Sex Comedies" in 2010 with German intertitles and English subtitles.

==External links==
* 

==References==
 

 

 
 
 
 
 