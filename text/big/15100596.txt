A Farewell to Arms (1957 film)
{{Infobox film
| name           = A Farewell to Arms
| image          = Farewell to arms film poster small.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Charles Vidor
| producer       = David O. Selznick
| screenplay     = Ben Hecht
| based on       =  
| narrator       =  Jennifer Jones Elaine Stritch
| music          = Mario Nascimbene
| cinematography = Oswald Morris
| editing        = John M. Foley Gerard Wilson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 152 minutes
| country        = United States
| language       = English
| budget         = $4,100,000  or $4,353,000 David Thomson, Showman: The Life of David O. Selznick, Abacus, 1993 p 656 
| gross          = $20 million (worldwide) 
}}
 1929 semi-autobiographical A Farewell to Arms starred Gary Cooper and Helen Hayes. 

==Plot== Jennifer Jones), a Red Cross nurse whom earlier he had met, and with whom he had had a romantic encounter, near the front, and the two engage in an affair. Fredericks friend, the doctor, convinces the army that Fredericks knee is more severely wounded than it actually is and the two continue their romance but never get married.
 pregnant but after sneaking alcohol into the hospital for Frederick, the head nurse Miss Van Campen (Mercedes McCambridge) discovers the duplicity and separates them. She informs Fredericks superiors that he has fully recovered from his wounds and is ready for active duty. During their separation, Catherine comes to believe Frederick has abandoned her.

Following the Battle of Caporetto, Frederick and his close friend Major Alessandro Rinaldi (Vittorio De Sica) assist the locals in fleeing the invading German/Austrian armies. Along the forced march, several people die or are left behind due to exhaustion. When the two ambulance drivers are finally able to report to a local army base, the commandant assumes they are both deserters from the front. Rinaldi is executed by the Italian military; enraged, Frederick knocks out the kerosene lamps and flees, jumping into the river.

Wanted by the Italian army, Frederick evades capture and meets up with Catherine. They flee Milan to hide out on a lake on the Italian-Swiss border (Lake Lugano or Lake Maggiore). Fearing arrest by the police, Catherine persuades Frederick to flee to Switzerland by rowboat; after some adventures, they land successfully in Switzerland. Claiming to be tourists trying to evade the war, the two are allowed to remain in neutral Switzerland. Catherines pregnancy progresses but due to the conditions around them, the pregnancy becomes complicated and Catherine is hospitalized. Their child is Stillbirth|stillborn, and Catherine dies shortly afterward. Frederick leaves, shocked, and wanders the empty streets.

==Cast==
* Rock Hudson as Frederick Henry Jennifer Jones as Catherine Barkley
* Vittorio De Sica as Major Alessandro Rinaldi
* Oskar Homolka as Dr. Emerich
* Mercedes McCambridge as Miss Van Campen
* Elaine Stritch as Helen Ferguson
* Kurt Kasznar as Bonello
* Victor Francen as Colonel Valentini  
* Franco Interlenghi as Aymo  
* Leopoldo Trieste as Passini  
* José Nieto (actor)|José Nieto as Major Stampi (as Jose Nieto)  
* Georges Bréhat as Captain Bassi (as Georges Brehat)  
* Johanna Hofer as Mrs. Zimmerman  
* Eduard Linkers as Lieutenant Zimmerman  
* Eva Kotthaus as Delivery Room Nurse  
* Alberto Sordi as Father Galli
* Joan Shawlee as Blonde Nurse

==Production notes== A Star is Born, to which he owned the foreign rights. Without them, the studio could not release their intended remake with Judy Garland overseas. Selznick offered to relinquish his rights to Star in exchange for the rights to Farewell, and Warner Bros. agreed. 

On October 25, 1956, Selznick contacted director John Huston at the Blue Haven Hotel in Tobago and enthusiastically welcomed him to the project. He advised him his contract with 20th Century Fox called for severe financial penalties if the film went over schedule and/or budget, and urged him to concentrate wholly on the film until principal filming was completed.  Selznicks concerns increased as Huston began to tinker with the script and spend an inordinate amount of time on pre-production preparations, and on March 19, 1957, he sent the director a lengthy memo outlining the problems he foresaw arising from Hustons lack of cooperation.  Two days later, Huston announced he could not agree with Selznick on any of the issues he had raised and quit the project. Based on correspondence to Charles Vidor, it appears the producers relationship with Hustons replacement was acrimonious as well.  The producer later said the film was "not one of the jobs of which I am most proud." 
 Italian Alps, Venzone in the Province of Udine in the region of Friuli-Venezia Giulia, Lazio, and Rome. It was budgeted at $4,353,000, and grossed little more than that.

According to   was informed by Selznick that he would receive a $50,000 bonus from any profits the movie made. Unhappy at Selznicks nepotistic decision to cast his nearly 40-year old wife as a character intended to be in her early 20s, he wrote back "If, by some chance your movie, which features the 38-year old Mrs. Selznick as 24-year old Catherine Barkley, does succeed in earning $50,000, I suggest that you take all of that money down to the local bank, have it converted to nickel (United States coin)|nickels, and then shove them up your ass until they come out your mouth."

==Reception==

===Critical response===
Hemingways intuition proved correct as A Farewell To Arms opened to low box office receipts and negative reviews after it premiered in 1957 and would be forgotten by the moviegoing public as an epic in later years. In his review in The New York Times, Bosley Crowther noted, "Mr. Selznicks picture . . . lacks that all-important awareness of the inescapable presence and pressure of war. That key support to the structure of the theme has been largely removed by Ben Hechts script and by a clear elimination of subtle thematic overtones . . .   is a tedious account of a love affair between two persons who are strangely insistent upon keeping it informal . . . as a pure romance . . . it has shortcomings. The essential excitement of a violent love is strangely missing in the studied performances that Rock Hudson and Jennifer Jones give in the leading roles. Mr. Hudson is most noticeably unbending, as if he were cautious and shy, but Miss Jones plays the famous Catherine Barkley with bewildering nervous moves and grimaces. The show of devotion between two people is intensely acted, not realized. It is questionable, indeed, whether Mr. Hudson and Miss Jones have the right personalities for these roles." 

TV Guide calls it "an overblown Hollywood extravaganza that . . . hasnt improved with age . . . the chief virtue of this hollow epic is the stupendous color photography of the Italian Alps . . . also enjoyable is Vittorio De Sicas inspired performance as the wily Maj. Rinaldi, but its not enough to offset the flagrant overacting by Jones and the woodenness of Hudson." 

Time Out New York describes it as an "inflated remake" with "surplus production values and spectacle" and adds, "A padded Ben Hecht script and Selznicks invariable tendency to overkill are equally to blame." 
 Gone with the Wind . . . without compelling lovers at the heart of his grand-scale love story, its all just a meaningless protracted spectacle." 

After this film, David O. Selznick left the movies completely, producing no other films.

The movie earned an estimated $5 million in North American rentals  and by the end of 1958 had made worldwide rentals of $6.9 million.  Fox made some money on the movie but Selznick did not recover his costs. 

===Awards and nominations===
Vittorio De Sica was nominated for the Academy Award for Best Supporting Actor but lost to Red Buttons for Sayonara.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 