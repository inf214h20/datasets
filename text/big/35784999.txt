Sange Muzhangu
{{Infobox film
| name           = SANGE MUZHANGU              
| image          = Sange Muzhangu.jpg
| image_size     =
| caption        =
| director       = P. Neelakantan
| producer       = S. Ramakrishnan
| writer         = K. S. Gopalakrishnan
| narrator       = Lakshmi S. Helen V. K. Ramasamy (actor) |V. K. Ramasamy T. K. Bagavathy
| music          = M. S. Viswanathan
| cinematography = V. Ramamoorthy
| editing        = K. Narayanan
| studio         = Valli Films
| distributor    = Valli Films
| released       = 4 February 1972 
| runtime        = 156 mins
| country        =   India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Tamil Tamil film directed by Pa. Neelakantan, starring M. G. Ramachandran in the lead role, with S. A. Ashokan, Lakshmi (actress)|Lakshmi, Cho Ramaswamy, among others enacting supporting roles.

==Plot==
Murugan (MGR) & Sivagami, his sister adopted by Dayalan (V. S. Raghavan), a rich businessman.

When the adopted father murdered, Murugan was accused of murdering him for the fortune.
 
==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
|-
| M. G. Ramachandran || as Murugan, (after Mohammed and after Kirubal Singh)
|- Lakshmi || as Ladha, Murugan s lover
|-
| T.K.Bagavadhi|| as Pradhap Singh, Ladha s father
|-
| V. K. Ramasamy (actor) |V. K. Ramasamy || as Varagaswamy, an advocat and Nadharadjan s friend
|-
| S. A. Ashokan || as Nadharadjan, the director
|-
| V. S. Raghavan || as Dayalan, the jeweler, the foster father of Murugan and Sivagami
|-
| Cho Ramaswamy || as Sindhamani, Murugan s friend
|-
| Jaya Kausalya || as Sivagami, Murugan s sister
|-
| G. Sakunthala || as Vedhalan, Varagaswamy s wife
|-
| Sridevi |Baby Sridevi || as Sivagami (child)
|-
| Kumari Nirmala || as
|-
| Helen Jairag Richardson (Not mentioned) || as The cabaret dancer
|-
| K.D.Sandhanam || as Mogan
|-
| Kalapathre Nadaradjan || as Sekhar, Murugan s brother-in-law
|-
| S.V.Ramdass || as The aggressor of the airport
|-
| K.Kannan || as
|-
| Kundhu Karpaya || as
|-
| Master Babu || as Murugan (child)
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

MGR will take several get-up in this movie to find out the real murderer.

SANGE MUZHANGU is the second and last production under the banner of Valli Films (with MGR).

The first one being the famous KANAVAN of 1968.

==Songs==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:37
|-
| 2 || Kangalirandum Vizhi Vilakaga || T. M. Soundararajan, P. Susheela || 03:20
|-
| 3 || Naalu Perukku || T. M. Soundararajan || 03:30
|-
| 4 || Naam Solliththara || L. R. Eswari || 03:18
|-
| 5 || Pombala Sirichapochu || T. M. Soundararajan || 03:21
|-
| 6 || Silar Kudippathupole || T. M. Soundararajan, L. R. Eswari || 03:34
|-
| 7 || Thamizhil Athu Oru || T. M. Soundararajan, P. Susheela || 04:01
|-
| 8 || Ullathil Naalu Perukku || T. M. Soundararajan || 03:52
|}

==References==
 

==External links==
* 

 
 
 
 
 


 