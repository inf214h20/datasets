Le Bonheur de Pierre
{{Infobox film
| name           = Le Bonheur de Pierre
| image          = 
| caption        = 
| director       = Robert Ménard 
| producer       = Guy Bonner Claude Bonin
| writer         = Guy Bonner
| narrator       = Pierre Richard
| starring       = Pierre Richard Sylvie Testud Rémy Girard
| music          = Sébastien Souchois
| cinematography = Pierre Mignot
| editing        = Michel Arcand
| distributor    = 
| released       =   
| runtime        = 106 minutes
| country        = Quebec France  French
| budget         = 
| gross    = 
}}
Le Bonheur de Pierre is a 2009 Canadian-French comedy-drama directed by Robert Ménard. {{cite web|url=http://www.cinemamontreal.com/movies/29692/Le_Bonheur_de_Pierre.html
|title=Le Bonheur de Pierre|publisher=cinemamontreal.com|accessdate=2013-08-01}}     {{cite web|url=http://movies.yahoo.com/movie/le-bonheur-de-pierre/
|title=Le Bonheur de Pierre|publisher=movies.yahoo.com|accessdate=2013-08-01}}   New York Independent Film Festival in French films of 2009|2009.   {{cite web|url=http://www.myfrenchfilmfestival.com/en/movie?movie=29615
|title=Le Bonheur de Pierre|publisher=myfrenchfilmfestival.com|accessdate=2013-08-01}} 
The film is also known as A Happy Man.  

==Plot==
French professor Pierre Martin is a widower who lives with his single daughter Catherine. When his aunt Jeanne dies she bequeathes him her guest house in Canada on condition he lives there for a minimum of time. He loves to comply because he has fond childhood memories of this place. In order to make his daughter Catherine accompany him he tells her a great deal of money was also part of Jeannes heritage. So both of them hurry  to the rural little town Sainte Simone du Nord in Canada. The mayor Michel Dolbec knows Jeannes house will become property of their community if the two Martins dont hold out long enough. He sabotages them several times but they stay. Eventually Pierre Martin and Michel Dolbec become friends after all.
==Cast==
* Pierre Richard as Pierre Martin, 
* Rémy Girard as Michel Dolbec
* Sylvie Testud as Catherine Martin
* Louise Portal as Louise Dolbec, Michel Dolbecs wife
* Jean-Nicolas Verreault as Mario Vaillant
* Patrick Drolet as Steven Dolbec, Michel Dolbecs son
* Gaston Lepage as Ti-Guy, the plumber
* Diane Lavallée as Pauline
* Vincent Bilodeau as Raymond
==Reception==
The film received mixed reviews. It was praised as a "touching politically incorrect comedy". {{cite web|url=http://www.westdean.org.uk/Events/WestDeanFestival/WhatsOn/Film.aspx
|title=A Happy Man|publisher=westdean.org.uk|accessdate=2013-08-01}}    Yet its narrativity been blamed for a lack of rigour.
 
==DVD release==
A NTSC version of Le Bonheur de Pierre has been released on DVD in winter 2009. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 