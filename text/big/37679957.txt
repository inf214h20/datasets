Vaya con Dios (film)
{{Infobox film
| name           = Vaya con Dios
| image          = 
| alt            = 
| caption        = 
| director       = Zoltan Spirandelli
| producer       = 
| writer         = 
| starring       = Michael Gwisdek   Daniel Brühl   Traugott Buhre
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 106 min
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
Vaya con Dios is a 2002 German comedy film directed by Zoltan Spirandelli,  and takes the form of a road film. 
 

==Plot== Order of monks, who have until recently lived in an ancient Abbey in Brandenburg. The Cantorianer speak Latin, and maintain a religious life, in which hierarchy plays a minor role, and believe that the Holy Spirit in music, reveal particularly in the vocals. Because of this heretical doctrine, the Cantorianer have been persecuted by the Roman Catholic Church since 1693. Today only two monasteries survive Brandenburg and the mother monastery Montecerboli in Italy. German community Italian mother monastery.

Totally unprepared for the outside world the three monks travel on foot across Europe, until they meet in their travels a young journalist, Chiara(Chiara Schoras) who offers to give them a lift in her classic Mercedes-Benz convertible. The storey follows their journey where each of the monks face their own temptations. Arbo (Daniel Brühl), the youngest monk, who has lived in the monastery all his life falls in love with Chiara and struggles with betraying his vows, while the monk Tassilo(Matthias Brenner) is drawn to return to his parents farm which his widowed mother is now running alone.

==Music==
In the film songs of from different eras are:
•	A Bon Bard, composer "Perrinet" can be heard in the opening credits.
•	At the beginning of the film the monks in the monastery of Auerberg sing solus of Renaissance composer Josquin des Prez.
•	From the new music, a Pater Noster setting of Igor Strawinsky - is sung by the monks walking along the train tracks.
•	Before the overnight stay in the forest, the monks in a quarry sing the beginning of the Gospel of Matthew in Fauxbourdon style 
•	As Gloria song of the Holy Mass with the Jesuits in Karlsruhe, the three monks have a three-part set of Georg Neumark who makes only the love God from the 17th century can be heard exercise. This sentence was edited for the film by the Tobias Gravenhorst representing the organist.
•	At the end of the film, is Montecerboli, again
•	The actress Chiara Schoras sings music by D. F. Petersen, in the credits.

==Critique==
Music plays an important part of the storey line, with the three monks spontaneously breaking into song throughout the film, although it would be wrong to consider the film a musical film|musical, as the music does not advance the plot as in a musical. It is rather that story involved a group of monks who sing. Similarly although it has elements of both comedy and romance it would be wrong to call the film a romcom.
 
== Cast ==
* Michael Gwisdek - Benno
* Daniel Brühl - Arbo
* Traugott Buhre - Abt Stefan
* Chiara Schoras - Chiara
* Matthias Brenner - Tassilo
* Pamela Knaak - Frau Brenner
* Konstantin Graudus - Anwalt
* Remo della Rena - Italian Monk

==References==
 

==External links==
*  

 