The End of Innocence (film)
{{Infobox Film
| name           = The End of Innocence
| image          = The End of Innocence.jpg
| image_size     = 
| caption        = Original film poster
| director       = Dyan Cannon
| producer       = Leonard Rabinowitz Stanley Fimberg
| writer         = Dyan Cannon John Heard Stephen Meadows
| music          = Michael Convertino
| cinematography = Alex Nepomniaschy
| editing        = Bruce Cannon
| distributor    = Skouras Pictures
| released       = December 6, 1990
| runtime        = 102 minutes
| country        = United States English
| budget         = 
}}
The End of Innocence is a semi-autobiographical film from 1990 directed, written by and starring Dyan Cannon.

==Plot==
A young girl is spiritually torn apart by forces beyond her control. After suffering a nervous breakdown, she is placed in an asylum, where for the first time she is treated as a human being by a compassionate psychiatrist.

==Cast==
*Rebecca Schaeffer - Stephanie (18–25 years old)
*Dyan Cannon - Stephanie John Heard - Dean George Cole - Dad
*Billie Bird - Mrs. Yabledablov
*Michael Madsen - Earl
*Todd Field - Richard
*Stephen Meadows - Michael
*Dennis Burkley - Tiny
*Viveka Davis - Honey

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 