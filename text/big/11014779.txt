8 Mile (film)
{{Infobox film
| name = 8 Mile
| image = Eight mile ver2.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Curtis Hanson
| producer = Curtis Hanson Brian Grazer Jimmy Iovine
| writer = Scott Silver
| starring = Eminem Kim Basinger Brittany Murphy Mekhi Phifer 
| music = Various Artists
| cinematography = Rodrigo Prieto Jay Rabinowitz
| studio = Imagine Entertainment Universal Pictures
| released =  
| runtime = 110 minutes  
| country = United States
| language = English
| budget = $41 million 
| gross = $242,875,078 
}} biopic film written by Scott Silver, directed by Curtis Hanson, and starring Eminem, Mekhi Phifer, Brittany Murphy, Michael Shannon and Kim Basinger.
 8 Mile Oakland County suburbs.

Filmed mostly on location in Detroit and its surrounding areas, the film was a critical and financial success. Eminem won the Academy Award for Best Original Song for "Lose Yourself", the song which was iconic to this film. A decade after its release, Vibe (magazine)|Vibe magazine called the film a "hip-hop movie masterpiece." 

==Plot== rap battle one night at a club called The Shelter, and leave the stage humiliated.

Jimmy works at an automotive factory, but when he asks for extra shifts, his supervisor dismisses his request on account of his habitual tardiness. A woman named Alex shows up at the factory one day looking for her brother and meets Jimmy; they become friends. Jimmy comes to realize that his life has remained largely the same since high school. At first, he considers himself a victim of his circumstances and blames others for his problems. Over time, he begins to take more responsibility for the direction of his life. When he exhibits an improved attitude and performance at work, his supervisor grants him the extra shifts he requested.

Jimmys friendship with Wink (Eugene Byrd), a radio station employee with ties to a record label prompter, becomes strained after he finds out Wink does promotional work for Jimmys rivals, a gang of rappers known as the Leaders of the Free World. At one point, Jimmy and his friends get into a brawl with the Leaders, which is broken only when Jimmys friend Cheddar Bob pulls out a gun and accidentally shoots himself in the leg. He is hospitalized and relegated to crutches. 

At work one day, Jimmy sees a rap battle in which a worker named Mike insults a fellow co-worker, Paul, who is gay, with his lyrics. Jimmy joins the battle to defend Paul by insulting Mike. Alex witnesses this and is impressed; her relationship with Jimmy develops into a sexual one. He asks her out for that night, but they end up having intercourse in the factory. Wink arranges for Jimmy to meet with producers at a recording studio, but when Jimmy goes there, he finds Wink and Alex having sex on top of a mixing console. He beats up Wink and their friendship is ended. Wink and the Leaders of the Free World, in retaliation, attack Jimmy outside his mothers trailer and beat him badly. The leader of the gang, Papa Doc (Anthony Mackie), puts a gun to Jimmys face, threatening to kill him but Wink dissuades Doc. 

Jimmys best friend Future (Mekhi Phifer) pushes him to get his revenge by competing against the Leaders of the Free World at the next rap battle. However, Jimmys late-night shift conflicts with the next battle tournament. But a visit from Alex changes his mind about competing. Paul, the co-worker whom Jimmy had stood up for, agrees to cover for him at work, and Jimmy enters the battle.
 Bloomfield Hills, and grown up in a stable two-parent household.

With nothing to say in rebuttal, Clarence hands the microphone back to Future, feeling embarrassed that someone Papa Doc thought was below him beat him in a battle. After being congratulated by Alex and his friends, Future offers him a position hosting battles at The Shelter. Jimmy declines, saying he has to get back to work and to find success his own way. He walks back to work, with a renewed sense of confidence about his future.

==Cast==
* Eminem as Jimmy "B-Rabbit" Smith, Jr.
* Mekhi Phifer as David "Future" Porter, Jimmys closest friend and the rap battle host
* Brittany Murphy as Alex Latorno, Jimmys love interest
* Kim Basinger as Stephanie, Jimmy and Lilys mother
* Taryn Manning as Janeane, Jimmys ex-girlfriend
* Michael Shannon as Greg Buehl, Stephanies boyfriend Evan Jones as "Cheddar Bob", Jimmys friend
* Omar Benson Miller as "Sol George", Jimmys friend
* DeAngelo Wilson as "DJ IZ", Jimmys friend
* Eugene Byrd as "Wink"
* Anthony Mackie as "Papa Doc" (Clarence)
* Xzibit as Mike (Male Lunch Truck Rapper) Proof as "Lil Tic"
* Craig Chandler as Paul
* Obie Trice as Male parking lot rapper
* Chloe Greenfield as Lily, Jimmys sister
* John Singleton as Bouncer
*  Miz-Korona  as Vanessa (Female Lunch Truck Rapper)
* Brandon T. Jackson (uncredited) as a Chin Tiki club-goer

==Reception==

===Critical reception===
The film received generally positive reviews, with critics praising the music and Eminems performance. Review aggregator Rotten Tomatoes reports the film is "Certified Fresh", with 76% of 205 professional critics giving the film a positive review and a rating average of 6.8 out of 10. Narrowed down to Rotten Tomatoes "top critics", it holds a 88% approval rating based on 49 reviews, with an average of 7.3 out of 10. The sites consensus is that "Even though the story is overly familiar, theres enough here for an engaging ride."    On Metacritic, which assigns a weighted mean rating out of 100 reviews from film critics, the film has rating score of 77 based on 38 reviews, which indicates "generally favorable reviews".  CinemaScore polls conducted during the opening weekend revealed the average grade cinemagoers gave 8 Mile was B+ on an A+ to F scale, with the core under-21 demographics giving it an A. 

Roger Ebert gave the film 3 out of 4 stars. He said that we "are hardly started in "8 Mile," and already we see that this movie stands aside from routine debut films by pop stars" and that it is "a faithful reflection of his myth". He said that Eminem, as an actor, is "convincing without being too electric" and "survives the X-ray truth-telling of the movie camera". He praised Eminems approach to his role, saying that "The genius of Rabbit is to admit his own weaknesses." He complimented Basinger, saying that "Her performance finds the right note somewhere between love and exasperation; it cannot be easy to live with this sullen malcontent, whose face lights up only when he sees his baby sister, Lily." He said that criticism of Basinger for being "too attractive and glamorous to play Rabbits mother" were unfair: "Given the numbers of ugly people who live in big houses, why cant there be beautiful people living in trailers?" He called the film "a grungy version of a familiar formula, in which the would-be performer first fails at his art, then succeeds, is unhappy in romance but lucky in his friends, and comes from an unfortunate background. He even finds love, sort of, with Alex," but "What the movie is missing, however, is the third act in which the hero becomes a star," as it "avoids the rags-to-riches route and shows Rabbit moving from rags to slightly better rags." He said that he "would love to see a sequel in which Rabbit makes millions and becomes world famous, and we learn at last if it is possible for him to be happy." 
 At the Movies with Ebert and Roeper review, both Ebert and Richard Roeper gave the film a thumbs up; Roeper said that Eminem has a "winning screen presence" and "raw magic" to him. He was happy with Rabbits "tender side" presented through his relationship with the "adorable" Greenfield as his sister, but felt that Basinger was "really miscast". But as in his own review, Ebert felt that the dark, depressing atmosphere of 8 Mile would turn off some Eminem fans, while Roeper thought they would like it. Roeper said: "8 Mile probably wont win converts to rap, but it should thrill Eminem fans." 

Peter Travers gave the film 3.5 out of 4 stars. He said that 8 Mile "is a real movie, not a fast-buck package to exploit the fan base of a rap nonentity" that "qualifies as a cinematic event by tapping into the roots of Eminem and the fury and feeling that inform his rap. Hanson spares us the rags-to-riches cliches by leaving Rabbit on the edge of success. The film ends not with a blast but with the peace that comes to a rapper who finds his voice at last. That kind of class is a big risk for a novice stepping into the movie ring. Eminem wins by a knockout." He praised Hansons directing, stating that he "succeeds brilliantly at creating a world around Eminem that teems with hip-hop energy and truth" and "excels with actors." He hailed Eminems performance, saying that in 8 Mile, "Eminem is on fire" with an "electric" screen presence, "hold  the camera by natural right" and "read  lines with an offbeat freshness that makes his talk and his rap sound interchangeable," and sulk of "intensity to rival James Deans." He said that Murphy was "dynamite, "play  Alex with hot desperation and calloused vulnerability," while saying that "Basinger shines" in her role as well. "Hanson builds to a spectacular climax" with Rabbits last three battles, and compared his final battle with Papa Doc to fight between Rocky Balboa and Apollo Creed in Rocky. 

===Box office=== The Bodyguard (Whitney Houston).

The 8 Mile DVD, which was released on March 18, 2003, generated $75 million in sales and rentals in its first week, making it the biggest DVD debut ever for an R-rated movie and putting it in the all-time Top 10 for first week home video sales for a movie.
A VHS version was also released on the same date.  

===Top lists===
8 Mile has been named to various year-end and all-time top lists:
* 2nd – Billboard (magazine)|Billboard (Erika Ramirez): Top 10 Best Hip-Hop Movies Ever 
* 7th – The New York Observer (Andrew Sarris): The 10 Best English-Language Films of 2002 
* 9th – Time (magazine)|Time (Richard Schickel): Top 10 Movies of 2002 
* 10th – Rolling Stone (Peter Travers): The Best Movies of 2002 
* N/A – The Daily Californian: Best Films of 2002  100 Years...100 Songs

===Awards=== Luis Resto Best Original Song in 2002.  The film has been nominated for 32 awards, winning 11. 

{| class="collapsible collapsed" style="width:100%; border:1px solid #cedff2; background:#F5FAFF"
|-

! style="text-align:left;"| List of awards and nominations
|-
|  

{| class="wikitable" style="font-size:95%;"
|-
! Year
! Award
! Category
! Result
! Recipient
|- 2003
| Academy Award Best Original Song – Lose Yourself
|   Luis Resto and Jeff Bass
|- Black Reel Awards
| Best Original Soundtrack
|  
| 8 Mile
|- BMI
| Film Award for Music
|  
| Eminem
|-
| Most Performed Song from a Film – Lose Yourself
|  
| Eminem
|- Broadcast Film Critics Association Awards
| Critics Choice Award for Best Song – Lose Yourself
|  
| Eminem
|-
| CNOMA Awards
| Best Make-Up Artist for a Feature Film
|  
| Donald Mowat
|- Chicago Film Critics Association Awards
| Most Promising Performer
|  
| Eminem
|-
| European Film Awards
| Screen International Award
|  
| Curtis Hanson
|-
|  |publisher=[[Hollywood Foreign Press Association
|HFPA]]|year=2003|accessdate=February 17, 2014}}  Best Original Song – Lose Yourself
|  
| Eminem
|- Golden Trailer Awards   
| Best Music
|  
| 8 Mile
|-
| Best of Show
|  
| 8 Mile
|-
| Most Original
|  
| 8 Mile
|- Golden Reel Award
| Best Sound Editing in a Feature - Music - Musical
|  
| Carlton Kaller
|-
| Hollywood Makeup Artist and Hair Stylist Guild Awards
| Best Contemporary Makeup - Feature
|  
| Donald Mowat, Ronnie Specter, Matiki Anoff
|- MTV Movie Awards   
| Best Movie
|  
| 8 Mile
|-
| Best Male Performance
|  
| Eminem
|-
| Breakthrough Male Performance
|  
| Eminem
|- Online Film Critics Society Awards
| Best Breakthrough Performance
|  
| Eminem
|-
| Phoenix Film Critics Society Awards
| Best Original Song – Lose Yourself
|  
| Eminem
|- Satellite Awards
| Best Original Song – Lose Yourself
|  
| Eminem
|- Teen Choice Awards Choice Movie - Drama/Action Adventure
|  
| 8 Mile
|-
| Choice Movie Actor - Drama/Action Adventure
|  
| Eminem
|-
| Choice Movie Breakout Star - Male
|  
| Eminem
|-
| Choice Crossover Artist (Music/Acting)
|  
| Eminem
|-
| Choice Movie Liplock
|  
| Eminem and Brittany Murphy
|- World Soundtrack Awards
| Best Original Song Written for a Film – Lose Yourself
|  
| Eminem
|- 2004
| ASCAP Awards
| Most Performed Song from a Motion Picture – Lose Yourself
|  
| Eminem
|- Grammy Award   
| Grammy Award for Record of the Year – Lose Yourself
|  
| Eminem
|-
| Grammy Award for Song of the Year – Lose Yourself
|  
| Jeff Bass, Eminem & Luis Resto
|-
| Grammy Award for Best Rap Song – Lose Yourself
|  
| Jeff Bass, Eminem & Luis Resto
|-
| Grammy Award for Best Male Rap Solo Performance – Lose Yourself
|  
| Eminem
|-
| Grammy Award for Best Song Written for Visual Media – Lose Yourself
|  
| Jeff Bass, Eminem & Luis Resto
|-

|}
 
|}

==Music==
 
 
 UK Compilations Chart and the Australian ARIAnet Albums Chart. It also spawned a follow up soundtrack, More Music from 8 Mile, consisting of songs that appear in 8 Mile that were current singles during the films time setting of 1995. The album was also made in a clean edition removing most of the strong profanity and violent content.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
 
 
{{succession box
| before = "If I Didnt Have You" from Monsters, Inc. by Randy Newman
| title  = Academy Award for Best Original Song
| years  = 2002
| after  = "  by Fran Walsh, Howard Shore, and Annie Lennox
}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 