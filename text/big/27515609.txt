Monday or Tuesday (film)
{{Infobox film
| name           = Monday or Tuesday
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Vatroslav Mimica
| producer       = 
| writer         = Vatroslav Mimica Fedor Vidas
| narrator       = 
| starring       = Slobodan Dimitrijević Pavle Vuisić Gizela Huml Jagoda Kaloper Srđan Mimica Fabijan Šovagović Rudolf Kukić Renata Freiskorn Olivera Vučo
| music          = Miljenko Prohaska
| cinematography = Tomislav Pinter
| editing        = Katja Majer
| studio         = Jadran Film
| distributor    = 
| released       = 1966
| runtime        = 84 minutes Yugoslavia
| language       = Serbo-Croatian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1966 Cinema Yugoslav drama 1921 short story collection) in which she described her approach to writing:  "Examine for a moment an ordinary mind on an ordinary day. The mind receives a myriad impressions—trivial, fantastic, evanescent, or engraved with the sharpness of steel. From all sides they come, an incessant shower of innumerable atoms; and as they fall, as they shape themselves into the life of Monday or Tuesday." 

==Plot== flashbacks and fantastic imagery reflecting his inner life. These include his recollections of childhood, his feelings about the present and past, including memories of his first marriage, his current girlfriend Rajka (Jagoda Kaloper) and his father killed in World War II (Pavle Vuisić), as well as his fantasies and hopes about the future.

==Reception== stream of consciousness concept previously seen in his critically acclaimed 1964 film Prometheus of the Island.  The films visual structure (based on a collage of scenes set in the past and present mixed with fantastic imagery, which are indicated by changes in picture quality and colour) and the theme of contemporary social alienation (in part resulting from the trauma of War World II) have urged critics to draw comparisons to his animated film The Inspector Is Back! (Inspektor se vratio kući, 1957) and have been called "trademarks of Mimicas entire body of work". 
 concentration camps New Wave must have looked like when it first emerged as a trend, with its concentration on ordinary people and charmingly observed street scenes. And its this that makes the film endearing. Theres none of the removed coldness that formal experimentation can bring to a film as an unfortunate by-product."    
 Best Film Best Director Best Cinematography award for his work on both Monday or Tuesday and Rondo (film)|Rondo (directed by Zvonimir Berković).   

==References==
 

==External links==
* 
* 
*  at Filmski-programi.hr  

  

 
 
 
 
 
 