Night in London
{{Infobox film
| name           = Night in London
| image          = Night in London.jpg
| image_size     =
| caption        = Brij
| producer       = 
| writer         = Omar Khyam (story), Ramesh Pant (screenplay) 
| narrator       = Johnny Walker
| music          = Laxmikant-Pyarelal Anand Bakshi (lyrics)
| cinematography =
| editing        = 
| studio         =
| distributor    =
| released       =  1967 (India)
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}
 Johnny Walker and Helen (actress)|Helen.

==Plot==
An woman named Renu (Mala Sinha) is forced to lead a life of crime as her dad is being held hostage by the underworld. She eventually joins with a suspected criminal Jeevan (Biswajeet), both fall in love with each other, and team up against the bad guys.

==Cast==
* Biswajeet ... Jeevan 
* Mala Sinha ... Renu  Johnny Walker ... Bahadur Singh  Helen ... Jameela 
* Madhumati ... Dancer in Night in London Theme  Anwar Hussain ... 
* M._B. Shetty|M.B. Shetty .... Shetty
* Surya Kumar .... 

==Soundtrack==
The movie boasts one of the best performances of Mohd Rafi. Bahoshon Hawas Mein Deewana remained evergreen hit.Night in London marked the one of best music composed by duo Laxmikant Pyarelal and pro memoria lyrics of Anand Bakshi. Lata Mangeshkar also sang another cabaret song Mera Naam Hai Jamila.
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s) !! Picturized On
|-
| Baag Mein Phool Kisne
| Lata Mangeshkar, Mohd Rafi
| Biswajeet and Mala Sinha 
|-
| Bahoshon Hawas Mein Deewana
|  Mohd Rafi
| Biswajeet
|-
| Mera Naam Hai Jamila
| Lata Mangeshkar
| Helen
|-
| Nazar Na Lag Jaye
| Mohd Rafi
| Biswajeet and Mala Sinha
|-
| Night In London
| Lata Mangeshkar, Mohd Rafi
| Biswajeet and Mala Sinha
|-
| O Mere Yaar Tommy 
| Mohammad Rafi
| Biswajeet
|-
|-
| Sun Ae Bahar e Husn
| Mahendra Kapoor, Lata Mangeshkar
| Biswajeet and Mala Sinha
|-
|}

==References==
 

== External links ==
*  

 
 
 
 

 