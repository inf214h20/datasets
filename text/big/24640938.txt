If I Had to Do It All Over Again
{{Infobox film
| name           = If I Had to Do It All Over Again
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Claude Lelouch
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Catherine Deneuve
| music          = Francis Lai
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = 
| budget         = 
| gross          = $7,873,602 
}}

 
If I Had to Do It All Over Again (French: Si cétait à refaire) is a film directed by Claude Lelouch, released in 1976 in film|1976.

==Synopsis==
After a long time in prison, a woman discovers her son aged 14, studying under a scholarship.

==Starring==
*Catherine Deneuve as Catherine Berger
*Anouk Aimée as Sarah Gordon
*Charles Denner as Lavocat
*Francis Huster as Patrick
*Colette Baudot as Lucienne Lano
*Jean-Jacques Briot as Simon Berger
*Jean-Pierre Kalfon as Le bijoutier
*Valérie Lagrange
*Manuella Papatakis as la fille de Sarah
*Georges Staquet
*Jacques Villeret as Lagent immobilier
*Niels Arestrup as Henri Lano
*Albina du Boisrouvray
*Paul Bellaiche Robert Caron
*Zoé Chauveau
*Bernard-Pierre Donnadieu as Claude Blame
*Paul Deheuvels
*François Dalou
*Nicole Desailly
*Marie-Pierre de Gérando
*Paul Gianoli
*Martin Loeb
*Rita Maiden
*Alexandre Mnouchkine
*René Monard
*Chantal Mercier
*Monique Persicot
*Jean-François Rémi as le banquier
*Michel Ruhl
*Laurence Schuman
*Harry Walter
*Philippe Ziskind
*Françoise Hardy
*Elie Chouraqui
*Betty Mars

== References ==
 

==External links==
* 

 
 

 
 
 

 