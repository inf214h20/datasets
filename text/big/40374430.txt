Sisters in Arms (2010 film)
Sisters in Arms is a 2010 Canadian documentary film by created by Beth Freeman about three women who joined the Canadian military and took part in combat in Afghanistan.  . Look Out Newspaper  

== Synopsis ==
The film follows the experiences of three members of the Canadian military: Katie Hodges, an infantry soldier, Corporal Tamar Freeman, a medical professional, and Master Corporal Kimberly Ashton, a combat engineer. Revealing details of life on the front lines in Afghanistan, the film combines video diaries and recorded phone calls made by the soldiers with interviews by Freeman.  

== Release ==
Sisters in Arms premiered on November 9, 2010 on Knowledge Network, as part of its Remembrance Day programming.  It had its film festival debut at the 2011 Women in Film Festival in Vancouver, Canada  and went on to screen at Film North, winning the 2012 Best Documentary Award.  Sisters in Arms also screened at the Portland Oregon Women’s Film Festival the Other Venice Film Festival, the Yorkton Film Festival and had additional screenings in the United States and Canada. The film also aired on a number of other television networks including the Canadian Broadcasting Corporation (RDI). Sisters in Arms is distributed in the United States by Women Make Movies  and in Canada by Moving Images Distribution.

The film was screen at a celebration of International Womens Day in New York. 

Press coverage for the film includes: An in depth article on the history of women in combat in the New York Times  citing Sisters in Arms as a reference; A review of the issues in the Wall Street Journal  including extensive interviews with the films subjects; An article on women in combat in Canada in the Toronto Star  referencing Sisters in Arms and using images and quotes from the film’s interviews.

== References ==
 

== External links ==
* http://SistersinArms.ca
* http://www.imdb.com/title/tt1934394/
* https://www.facebook.com/pages/Sisters-in-Arms/163678543644951
* http://www.knowledge.ca/program/sisters-in-arms
* http://docspace.ca/film/sisters-in-arms

 
 
 
 
 
 