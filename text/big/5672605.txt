Su Excelencia
{{Infobox film
| name           = Su excelencia
| image          = Movie poster for the 1967 film, Su Excelencia, starring Mexican comedian Cantinflas.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Miguel M. Delgado 
| producer       = Jacques Gelman
| writer         = 
| screenplay     = Carlos León
| story          = Marco A. Almazán Cantinflas
| based on       =  
| narrator       = 
| starring       = Cantinflas Sonia Infante Guillermo Zetina
| music          = Sergio Guerrero
| cinematography = Rosalío Solano
| editing        = Jorge Bustos
| studio         = Posa Films
| distributor    = Columbia Pictures
| released       =   
| runtime        = 133 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} socialist and capitalist countries of the Cold War, which the film satirizes. Cantinflas portrays Lopitos, a chancellor stationed in his countrys (Republica de Los Cocos) embassy in the communist country of Pepeslavia who later ascends to the role of ambassador and has to decide the pivotal vote of joining the "Reds" or the "Greens", therefore deciding the fate of one hundred nations.

Su excelencia, the fourth Cantinflas film distributed by Columbia Pictures, boasted box-office success and is among Cantinflas most successful films.      

==Plot== bureaucrat from Communist bloc country "Pepeslavia" (a play on words of Joseph Stalin, the nickname for Joseph in Spanish (José) is "Pepe", and the inflection "-slavia" of Slavic peoples under the rule of the USSR). 
 superstition about coups détat in Los Cocos arrives throughout the meal, Lopitos becomes the official ambassador.

At a summit of world leaders, the representatives of the two world superpowers, "Dolaronia" (a play on words of the dollar, the currency of USA) and "Pepeslavia", court the allegiances of third-world diplomats to tilt the balance of global power in their favor. The last diplomat to remain unaligned, Lopitos instead harangues the superpowers for infringing on the rights of developing countries to self determination, talking to them with his point of view as a citizen not as ambassador because he arranged his demise as ambassador one day before his speech.

==Cast==
*Cantinflas &ndash; Lopitos, current ambassador of Los Cocos embassy.
*Sonia Infante &ndash; Lolita, personal secretary of Los Cocos embassy.
*Guillermo Zetina &ndash; Tirso de la Pompa y Pompa, counselor of Los Cocos embassy.		
*Tito Junco &ndash; General León Balarrasa, military attaché of Los Cocos embassy. Miguel Manzano &ndash; Serafín Templado, main secretary of Los Cocos embassy.		
*José Gálvez (actor)|José Gálvez &ndash; Osky Popovsky, bureaucrat of Pepeslavia and ardent supporter of the "Red" countries.		
*Víctor Alcocer &ndash; Admiral Neptuno Aguado, naval attaché of Los Cocos embassy.		
*Maura Monti &ndash; Tania Mangovna, Agent KGD007 of the Pepeslav government. Jack Kelly &ndash; Ambassador of Dolaronia and supporter of the "Green" countries.
*Eduardo Alcaraz &ndash; Salustio Menchaca, ex-ambassador of Los Cocos embassy. 		
*Fernando Wagner &ndash; Ambassador of Salchichonia who remains neutral.		
*Carlos Riquelme &ndash; President of Pepeslavia who greets Lopitos.		
*Quintín Bulnes &ndash; Petrovsky, butler of Los Cocos embassy.	
*Eduardo MacGregor &ndash; Vasily Vasilov, prime minister of Pepeslavia.	
*Luis Manuel Pelayo &ndash; Counselor of Pepeslavia

==References==
 

==External links==
*  

 
 
 
 