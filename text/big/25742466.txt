Standing Room Only (2003 film)
  2003 film directed by Deborra-Lee Furness and starring Hugh Jackman, Michael Gambon and Joanna Lumley. It has been distributed as part of the compilation film Stories of Lost Souls.

==Plot== William Ash), also interested in waiting by the door. It soon becomes apparent that the two men are very early patrons for a show by "The Man of a Thousand Faces". As both sit waiting for the time to slowly pass, their attention gets diverted by the stunning Maria, played by Mary Elizabeth Mastrantonio. After much gawking at her beauty, the men decide to have a game of chess, during which Roger (Hugh Jackman) exits a taxi and starts to make his way to the group, as Granny (Andy Serkis) turns a corner heading the same way. Determined not to be left in the cold, Granny all but runs to the forming line, nearly stepping on her dog. Alas, Roger beats her. Dismayed at her defeat, she scares away the newly advancing woman, who would be the last one in line. Having scared away the Granny and her dog, and seen the show, the trio regroups by the stage door, and Simon lines the group for a picture, which one of the performers offers to take for them. After the smiles and laughing fade from the picture taking, the dog reappears and follows the cast member down the street, and the patrons put two and two together about Hunter Jackson, the Man of a Thousand Faces.

==External links==
 
 
 
 


 