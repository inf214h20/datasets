All of Me (1934 film)
{{Infobox film
| name           = All of Me
| image_size     =
| image	         = All of Me FilmPoster.jpeg
| caption        =
| director       = James Flood
| producer       = Louis D. Lighton Thomas Mitchell Sidney Buchman
| starring       = Fredric March Miriam Hopkins George Raft
| music          = Karl Hajos
| cinematography = Victor Milner
| editing        = Otho Lovering
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States English
}} Thomas Mitchell and Sidney Buchman from Rose Porters play Chrysalis, and directed by James Flood.

==Cast==
*Fredric March as Don Ellis
*Miriam Hopkins as Lydia Darrow
*George Raft as Honey Rogers
*Helen Mack as Eve Haron
*Nella Walker as Mrs. Darrow
*William Collier, Sr. as Jerry Helman
*Gilbert Emery as Dean
*Blanche Friderici as Miss Haskell
*Edgar Kennedy as Guard
*Jason Robards, Sr. as Man in Speakeasy
*Barton MacLane as First Cop
*Kitty Kelly as Lorraine
*Mack Gray as Tough Guy

==Reception==
Reviews were poor and the film was a box office flop. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 45 
==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 

 