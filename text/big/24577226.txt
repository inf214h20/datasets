The Flaming Forest
__NOTOC__
{{Infobox film
| name           = The Flaming Forest
| image          = The Flaming Forest.jpg
| image_size     =
| caption        =
| director       = Reginald Barker
| writer         = Waldemar Young Lotta Woods (titles)
| based on       =  
| starring       = Antonio Moreno Renée Adorée
| music          =
| cinematography = Percy Hilburn
| editing        = Ben Lewis
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 silent drama film directed by Reginald Barker and starring Antonio Moreno and Renée Adorée. The film is based on the novel of the same name by James Oliver Curwood, and was produced by Cosmopolitan Productions and distributed by Metro-Goldwyn-Mayer.   A two-strip Technicolor sequence was shot for a climactic blaze sequence featured in the film. 

This is a preserved film at the Library of Congress.   

==Plot==
North-West Mounted Police sergeant David Carrigan (Antonio Moreno) fights Indians and wooes Jeanne-Marie (Renée Adorée).

==Cast==
* Antonio Moreno as Sergeant David Carrigan 
* Renée Adorée as Jeanne-Marie 
* Gardner James as Roger Audemard  William Austin as Alfred Wimbledon  Tom OBrien as Mike 
* Emile Chautard as André Audemard 
* Oscar Beregi, Sr. as Jules Lagarre 
* Clarence Geldart as Major Charles McVane 
* Frank Leigh as Lupin  Charles Ogle as Donald McTavish 
* Roy Coulson as François 
* DArcy McCoy as Bobbie 
* Claire McDowell as Mrs. McTavish 
* Bert Roach as Sloppy 
* Mary Jane Irving as Ruth McTavish

==See also==
*List of early color feature films

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 
 


 