Hotel Sorrento
 
 
 
{{Infobox film
| name           = Hotel Sorrento
| image          = Hotel Sorrento.jpg
| image_size     =
| caption        = Richard Franklin
| writer         =
| narrator       =
| starring       = Caroline Goodall
| music          =
| cinematography =
| editing        =
| distributor    = Umbrella Entertainment
| released       = 14 July 1995
| runtime        = 112 minutes
| country        = Australia
| language       = English
| budget         =
| gross          =
}}
 Richard Franklin. Sorrento after a ten-year hiatus.    One of the three has written a book called Melancholy which is a thinly disguised version of their lives. The film is an adaptation of Hannie Raysons play of the same name.   

"One of the films many fleeting reflections is an exploration of the word “melancholy” - a word that perfectly suits Hotel Sorrentos tone and pace."   

==Cast==
*Caroline Goodall as Meg Moynihan
*Caroline Gillmer as Hilary Moynihan 	
*Tara Morice as Pippa Moynihan
*Joan Plowright as Marge Morrisey
*Ray Barrett as Wal Moynihan
*Nicholas Bell as Edwin Ben Thomas as Troy Moynihan John Hargreaves as Dick Bennett
*Dave Barnett as Radio Announcer
*Peter OCallaghan as Radio Announcer
*Jane Edmanson as Radio Announcer
*Bill Howie as Radio Announcer
*Sam Newman as Football Commentator
*Shane Healy as Football Commentator
*Phillip Lee as Auctioneer (voice)

==Production==
Richard Franklin had worked for a number of years in the US, although he had lived in Australia since 1985. He was becoming frustrated with Hollywood and decided to make a film for the "art house market". He contacted his brother in law, Peter Fitzgerald, who had written a number of books on Australian theatre and asked him to recommend an Australian play which might make a good film. Fitzpatrick put forward Hotel Sorrento and Franklin loved it.  He made the movie having never seen a production of the play. 

==Critical reception==
The New York Times said that "The film is steeped in a homey provincial atmosphere that is at once comforting and stifling, and that gives some substance to the talk about the complacency and materialism of Australian society and its indifference to artists."  Cinephilia said "The play by Hannie Rayson, with its familiar typology of characters and Chekovian dialogue, no doubt provided pleasing entertainment in its original stage setting but as adapted by Franklin with Peter Fitzpatrick and transposed the big screen this story of a fraught family reunion of sorts looks like soapie material blown out of proportion (Meg’s line “I’m looking for Dick” is pure Number 96, albeit unintentionally so)."   

==Box office==
Hotel Sorrento grossed $1,215,478 at the box office in Australia. 

==Home media==
Hotel Sorrento was released on DVD by Umbrella Entertainment in September 2012. The DVD is compatible with all region codes and includes special features such as the trailer, audio commentary with Richard Franklin and a featurette titled Inside Hotel Sorrento.   

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 