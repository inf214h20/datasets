You'll Get Over It
 
{{Infobox film
| name           = Youll Get Over It
| image          = A-cause-dun-garcon.jpg
| caption        = Theatrical release poster (France)
| director       = Fabrice Cazeneuve
| producer       = Hervé Chabalier Claude Chelli Christophe Chevallier
| writer         = Vincent Molina
| starring       = Julien Baumgartner Jérémie Elkaïm Julia Maraval François Comar Bernard Blancan
| cinematography = Stephan Massis
| editing        = Jean-Pierre Bloc
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France  
| language       = French
| budget         = 
}} French title is Because of a Boy.

== Plot ==
Vincent (Baumgartner) is a shy boy who is on the swim team and is also a good student with his girlfriend Noémie (Maraval) and a best friend, Stéphane (Comar), life in school cant be better for him. But then, he suddenly starts to have encounters with the new boy, Bejamin (Elkaïm). They have a private meeting and then some boys write on a wall "Molina is a fag". Vincent starts getting bullied and is forced out of high school, changing his life and his relationships with family and friends in ways he will have to accept.

== Cast ==
 
* Julien Baumgartner as Vincent Molina
* Julia Maraval as Noémie
* Jérémie Elkaïm as Benjamin
* François Comar as Stéphane
* Patrick Bonnel as Bernard, the Father
* Christiane Millet as Sylvie
* Antoine Michel as Régis, the Brother
* Nils Ohlund as Bruno
* Bernard Blancan as Swimming Coach
* Eric Bonicatto as French Professor

== External links ==
*  

 
 
 
 
 


 
 