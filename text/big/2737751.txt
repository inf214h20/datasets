Peppermint Candy
 
{{Infobox film name        = Peppermint Candy image       = Peppermint Candy.jpg caption     = Theatrical poster director    = Lee Chang-dong producer  Makoto Ueda writer      = Lee Chang-dong starring    = Sol Kyung-gu Moon So-ri Kim Yeo-jin distributor = Shindo Films Cineclick Asia released    =   runtime     = 130 minutes country     = South Korea language    = Korean budget      =
| film name      = {{Film name hangul      =   hanja       =   rr          = Bakha Satang mr          = Pakha Satang}}
}}
Peppermint Candy ( ) is a 1999 film, the second from South Korean director Lee Chang-dong. The movie starts with the suicide of the protagonist and uses reverse chronology to depict some of the key events of the past 20 years of his life that led to his death. It was the ninth highest grossing domestic film of 2000 with 311,000 admissions in Seoul.   

It was received well, especially in film festivals. Spurred by the success of Lee Chang-dongs directorial debut, Green Fish, Peppermint Candy was chosen as the opening film for the Pusan International Film Festival in its first showing in 1999. It won multiple awards at the Karlovy Vary International Film Festival and won the South Korean cinema industrys Grand Bell Awards for best film of 2000.

==Plot==
At the beginning, the main character Yong-ho wanders to a reunion of his old student group. After causing some general mayhem with his deranged antics, he leaves and climbs atop a nearby train track. Facing an oncoming train, he exclaims "I want to go back again!" What follows is a series of prior events in the main characters life that show how he became the suicidal man portrayed in this scene.

The first flashback takes place only a few days before Yong-hos death. At this point he is already clearly suicidal, confronting his former business partner and ex-wife Hong-ja before the husband of his teenage crush Sun-im pays him a surprise visit. Yong-ho is taken to visit a comatose Sun-im in a hospital.

The next flashback shows Yong-hos life five years earlier. At first glance, he seems to be a rather successful businessman, but the problems in his life become clear when he confronts his wife, who is having an affair with her driving instructor. Yong-ho is unable to claim moral high ground, since he is also shown having an affair, with an assistant from his workplace. Finally, Yong-ho is shown with his wife at their new house, having dinner with his colleagues, where it becomes apparent that the marriage isnt working.
 torturing him for information about another mans whereabouts. This leads Yong-ho to Kunsan where he and his fellow police officers capture the wanted man. While in Kunsan, Yong-ho is distracted from his work by fruitlessly trying to search for Sun-im and instead ends up on a one-night stand with a woman.

The following flashback shows Yong-ho when he is just starting his career as a policeman and is pressured by his peers to torture a crime suspect, presumably a student demonstrator. Shortly afterward, he is visited by Sun-im. Yong-ho coldly and cruelly dismisses her by feigning interest in another woman, his future wife Hong-ja. At the final scene of this sequence, Yong-ho is shown sleeping with Hong-ja, whom it is shown he never truly cared about.

During the next flashback, its May 1980 and Yong-ho is performing his mandatory military service. While Sun-im is trying to visit him, his company is taken to quell the Gwangju Democratization Movement. Yong-ho gets shot in the leg and is told to stay behind. This leads to a scene where he confronts a harmless and presumably innocent student, whom he accidentally shoots and kills.

The last flashback shows Yong-ho as a part of the student group that reunited at the beginning of the movie. This is also where he meets Sun-im for the first time. The scene poignantly shows the innocence that Yong-ho had, before his country molded him into the violent and jaded man he is at the start of the film by pitting him against his friends.

==Analysis==
The events of Yong-hos life shown in the movie can be seen as representing some of the major events of Koreas recent history. The student demonstrations of the early 1980s leading to the Gwangju massacre is shown as Yong-ho becoming traumatized in the shooting incident.    The tightening grip on the country by the military government during the 1980s is mirrored by Yong-ho losing his innocence and becoming more and more cynical during his stint as a brutal policeman. Similarly, Yong-ho losing his job during the late 1990s mirrors the Asian financial crisis.   

Yong-hos life depicted the struggle between historiography and psychoanalysis. Despite his desperate desire to move on from his past, mnemic traces overpowered the psychoanalytical aspects of his life. These mnemic traces include the train, camera, and peppermint candy as well as Sun-im and her surrogates throughout the vignettes, which led the psychoanalysis of his life to triumph over historiography. The relationship between historiography and psychoanalysis can be seen in historicism and progressivism where Yong-ho chooses to look back on his past instead of looking solely on his future to move forward. The major and traumatic events that were historically imposed on him were so embedded in his life that he could not simply move on. However, finally reflecting back on his past allowed him to accept what happened and finally advance into the future. Unfortunately, this was moments before he committed suicide when he turned to face the train. The train was the symbol that guided the film in reverse chronology and his cry to return to the past signifies his tragically late recognition of the pasts significance on his life. 

Issues of masculinity in the South Korean culture arise in the film. Yong-Hos masculinity is broken during the Gwangju Massacre scene in which the militarized masculinity enforced by the Korean government &mdash; a required 26-month duty in the military, an order to kill innocent civilians, and a need to conform to the standards of the other soldiers around him &mdash; ultimately forces Yong-Ho to compensate later in life through interrogating the student protesters who inevitably were the reason he was put in that situation. Steve Choe,    This continues on with the way he treats women later on in his life, objectifying and mistreating his wife Hong-ja and ultimately losing his one link back to his innocence, Sun-im.    What results in the beginning of the film, what would be the end of Yong-Hos life, is an ultimate humiliation and lamentation for a lost innocence where personal history is connected with the history of South Korea. 

==Main cast==
*Sol Kyung-gu - Kim Yong-ho
*Moon So-ri - Yun Sun-im
*Kim Yeo-jin - Hong-ja

==Awards and nominations==
;2000 Baeksang Arts Awards 
* Best New Actor - Sol Kyung-gu

;2000 Grand Bell Awards
* Best Film
* Best Director - Lee Chang-dong
* Best Supporting Actress - Kim Yeo-jin
* Best Screenplay - Lee Chang-dong
* Best New Actor - Sol Kyung-gu

;2000 Blue Dragon Film Awards
* Best Actor - Sol Kyung-gu
* Best Screenplay - Lee Chang-dong

==References==

 

==External links==
*  
*  
* 
* 
*  at koreanfilm.org
* 

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 