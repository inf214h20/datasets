The Best of Times (film)
 The Best of Times}}
{{Infobox film
 | name           = The Best of Times
 | image          = Best of times poster.jpg
 | caption        = Theatrical release poster
 | director       = Roger Spottiswoode
 | producer       = Gordon Carroll
 | writer         = Ron Shelton
| starring = {{Plainlist|
* Robin Williams
* Kurt Russell
* Pamela Reed
* Donald Moffat
}} 
 | music          = Arthur B. Rubinstein
 | editing        = Garth Craven
 | cinematography = Charles F. Wheeler
 | studio         = Kings Road Entertainment
 | distributor    = Universal Studios
 | released       =   (USA)
 | runtime        = 104 min.
 | country        = United States
 | language       = English
 | budget         = 
 | gross          = $7,790,931 (USA)
}}
The Best of Times is a 1986 American comedy film directed by Roger Spottiswoode and written by Ron Shelton. It stars Robin Williams and Kurt Russell as two friends attempting to relive a high school football game.

== Plot ==
Jack Dundee (Williams) is a banker obsessed with what he considers the most shameful moment in his life: dropping a perfectly thrown pass in the final seconds of the 1972 high school football game between Taft and their arch rivals, Bakersfield, which ended in a scoreless tie.

Since that game, Jack has found it impossible to forget this event. He works for his father-in-law, The Colonel, Bakersfields biggest supporter, who reminds him of the event almost daily.

Thirteen years later, Jack coerces Reno (Russell), quarterback of the fateful game, and now a financially struggling garage owner in debt to Jacks bank, into helping him replay the game. He convinces supporters in both towns to re-stage the game and in the process revitalizes Taft, as well as his and Renos marriages.

== Cast ==
* Robin Williams as Jack Dundee
* Kurt Russell as Reno Hightower
* Pamela Reed as Gigi Hightower
* Holly Palance as Elly Dundee
* Donald Moffat as The Colonel
* Margaret Whitton as Darla
* M. Emmet Walsh as Charlie
* Donovan Scott as Eddie
* R. G. Armstrong as Schutte
* Dub Taylor as Mac
* Carl Ballantine as Arturo
* Kathleen Freeman as Rosie
* Tony Plana as Chico
* Kirk Cameron as Teddy
* Robyn Lively as Jacki
* Jeff Doucette as Olin
* Anne Haney as Marcy
* Linda Hart as Blender #1
* Tracey Gold as Jakis friend (uncredited)
*   Wayne Adderson as Bakersfield coach.

==Production==
Much of the film was shot in and around the actual Taft Union High School. The football scenes took place at Pierce Junior College in the San Fernando Valley. The night game was filmed at Moorpark Memorial High School, in Moorpark, CA.

==Reception==
The film received mixed reviews, with a 33% rotten rating on review aggregate Rotten Tomatoes.  Pauline Kael called the film "a small town comedy where the whole population is caught up in some glorious foolishness." 

==References==
 

== External links ==
*  
*   at Rotten Tomatoes
*  

 

 
 
 
 
 
 
 
 

 