The Jane Austen Book Club (film)
{{Infobox film
| name           = Jane Austen Book Club
| image          = Jane austen book club poster.jpg
| caption        = Original poster 
| alt            = 
| director       = Robin Swicord
| producer       = {{Plain list | 
* John Calley
* Julie Lynn
* Diana Napper
}}
| writer         = Robin Swicord
| based on       =  
| starring       = {{Plain list | 
* Maria Bello
* Emily Blunt
* Kathy Baker
* Amy Brenneman
* Maggie Grace
* Hugh Dancy
* Kevin Zegers
* Marc Blucas
* Jimmy Smits
* Lynn Redgrave
}}
| music          = Aaron Zigman
| cinematography = John Toon
| editing        = Maryann Brandon
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $6 million 
| gross          = $7,163,566 
}} novel of book club formed specifically to discuss the six novels written by Jane Austen. As they delve into Austens literature, the club members find themselves dealing with life experiences that parallel the themes of the books they are reading.

==Plot== French teacher fortysomething librarian who recently has separated from her philandering lawyer husband Daniel after more than two decades of marriage; Sylvias 20-something lesbian daughter Allegra; Jocelyn, a happily unmarried control freak and breeder of Rhodesian Ridgebacks who has been Sylvias friend since childhood; and Grigg, a science fiction fan whos roped into the group by Jocelyn with the hope he and Sylvia will prove to be a compatible match.
 1960s counterculture, finds herself desperately trying not to succumb to her feelings for her seductive student Trey. Allegra, who tends to meet her lovers while engaging in death-defying activities, feels betrayed when she discovers her current partner, aspiring writer Corinne, has used Allegras life as the basis for her short stories. Grigg is attracted to Jocelyn and mystified by her seeming lack of interest in him, marked by her failure to read the Ursula K. Le Guin novels he has hoped will catch her fancy. He also serves as the comedic foil to Jocelyn and Prudies very serious takes on the books.

==Cast==
* Maria Bello as Jocelyn
* Emily Blunt as Prudie
* Kathy Baker as Bernadette
* Hugh Dancy as Grigg
* Amy Brenneman as Sylvia
* Maggie Grace as Allegra
* Jimmy Smits as Daniel
* Marc Blucas as Dean
* Lynn Redgrave as Mama Sky
* Kevin Zegers as Trey
* Nancy Travis as Cat Harris
* Parisa Fitz-Henley as Corinne
* Gwendoline Yeo as Dr. Samantha Yep
* Myndy Crist as Lynne

==Production==
In The Book Club Deconstructed, a bonus feature on the DVD release of the film, screenwriter/director Robin Swicord explains how each of the six book club members is based on a character in one of Austens novels. Bernadette represents Mrs. Gardiner in Pride and Prejudice, Sylvia is patterned after Fanny Price in Mansfield Park, Jocelyn reflects the title character in Emma (novel)|Emma, Prudie is similar to Anne Elliot in Persuasion (novel)|Persuasion, Allegra is most like Marianne in Sense and Sensibility, and Grigg represents all of Austens misunderstood male characters.
 Long Beach, North Hollywood, Santa Clarita, Santa Monica, Van Nuys, Westlake Village.

==Music==
The soundtrack includes "New Shoes" by Paolo Nutini, "Youre All I Have" by Snow Patrol, "Save Me" by Aimee Mann, "So Sorry" by Feist (singer)|Feist, and "Getting Some Fun Out of Life" by Madeleine Peyroux.

==Release==
  
The film premiered at the Toronto Film Festival before going into limited release in the US. It opened on 25 screens on September 21, 2007 and earned $148,549 on its opening weekend. It went into wide release on October 5, expanding to 1,232 screens and earning an additional $1,343,596 that weekend. It eventually grossed $3,575,227 in the US and $3,542,527 in international markets for a worldwide box office of $7,163,566.   

==Critical reception==
On the review aggregator Rotten Tomatoes, 65% of critics gave the film positive reviews, based on 115 reviews,  while on Metacritic, the film has an average score of 61 out of 100, based on 28 reviews. 

Stephen Holden of The New York Times said the film "is such a well-acted, literate adaptation of Karen Joy Fowlers 2004 best seller that your impulse is to forgive it for being the formulaic, feel-good chick flick that it is ... Like the other movies and television projects in a Jane Austen boom that continues to gather momentum, it is an entertaining, carefully assembled piece of clockwork that imposes order on ever more complicated gender warfare." 

Roger Ebert of the Chicago Sun-Times called the film "a celebration of reading" and added, "oddly enough that works ... I settled down with this movie as with a comfortable book. I expected no earth-shaking revelations and got none, and everything turned out about right, in a clockwork ending that reminded me of the precision the Victorians always used to tidy up their loose ends." 

Ruthe Stein of the San Francisco Chronicle called the film "enjoyable if fairly predictable ... Its all a tad too neatly packaged, like a brand new set of Austen with the bindings unbroken. Still, a lively ensemble cast works hard ... Swicords gift as a screenwriter is that her catch-up summaries avoid sounding pedantic or like CliffsNotes. Shes less assured as a director. Her pacing is off, with some scenes going on longer than they need to and others whizzing by so fast you miss the nuances. Relationships arent always as clear as they should be. Still, Austen devotees are sure to lap up the central premise that her notions of love and friendship are as relevant today as ever. And if The Jane Austen Book Club gets people thinking about forming a club of their own, it will have served a more admirable purpose than most movies." 

Carina Chocano of the Los Angeles Times said it was nice to see "a movie so alive to the pleasures of reading and writing and sharing books, especially when the love feels sincere ... in parts, the story feels awkwardly truncated or too shallow to matter. But Swicord has a playful sense of humor and a good ear for dialogue, and the movie pleasantly accomplishes what it set out to accomplish." 

Dennis Harvey of Variety (magazine)|Variety stated, "While there are occasional forced notes ... Swicords direction proves as accomplished as her script at handling an incident-packed story with ease, capturing humor and drama sans cheap laughs or tearjerking." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 