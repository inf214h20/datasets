Village of Idiots
 
 
{{Infobox Film
| name           = Village of Idiots
| image          = Screenshot from the animated film Village of Idiots.jpg
| image_size     = 150px
| caption        = 
| director       = Eugene Fedorenko and Rose Newlove
| producer       = National Film Board of Canada
| writer         = John Lazarus
| narrator       = Nicholas Rice
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       = 1999 
| runtime        = 13 min.
| country        = Canada
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 folk tales Every Child.

==Summary==
“Outsiders call Chelm the village of idiots," Shmendrick explains, "but our rabbi said we were a city of natural geniuses, with our own way of figuring things out.”

With muted, mesmerizing illustrations and heavy accordion-based music, the film follows Shmendrick as he sets out on a journey away from home for the first time. But along his journey from Chełm to   tells us that the world everywhere is the same," he recalls.

==Awards==
Village of Idiots has won eight awards:
*Best Animated Short film at the Genie Awards 
*Two directing awards at the Montreal World Film Festival
*Won Animation at the Palm Springs International Short Film Festival
*Centaur Award at the St. Petersburg Message to Man Film Festival
*Best Animated Film at the Vancouver International Film Festival
*Won an award from the Writers Guild of Canada
*Second prize at the Zagreb World Festival of Animated Films

It was also included in the Animation show of shows.

==See also==
*The Witch from Melchet Street
*A Story about a Bad Dream
*Alan and Naomi

==References==
 

==External links==
*  
* Watch   at the National Film Board of Canada

 
 
 
 
 
 
 
 
 


 