The Forest (1982 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Forest
| image          =
| caption        =
| director       = Donald M. Jones
| producer       = Frank Evans Donald M. Jones
| writer         = Donald M. Jones
| starring       = Dean Russell Gary Kent Tomi Barrett John Batis Ann Wilkinson John Batis
| music          = Richard Hieronymus Alan Oldfield
| cinematography = Stuart Asbjornsen
| editing        = Robert Berk Donald M. Jones
| studio         = Wide World of Entertainment
| distributor    = Fury Films Ltd.
| released       =  
| runtime        = 85&nbsp;minutes
| country        = United States
| language       = English
}}
The Forest is a 1982 American slasher film directed, written, edited and produced by Donald M. Jones and starring Gary Kent, Tomi Barrett and John Batis. The film was shot in Sequoia National Park in California in 1981.

== Plot ==
 
Two American hikers, Steve (Dean Russell) and Charlie (John Batis), venture into the local forest along with their girlfriends, Sharon (Tomi Barrett) and Teddi (Ann Wilkinson). However, John (Gary Kent), a cannibalistic woodsman, is lurking in the forest putting these four friends at risk.

== Cast ==
*Dean Russell as Steve
*Gary Kent as John
*Tomi Barrett as Sharon
*John Batis as Charlie
*Ann Wilkinson as Teddi
*Jeanette Kelly as Mother
*Corky Pigeon as John Jr.
*Becki Burke as Jennifer
*Tony Gee as Plumber
*Stafford Morgan as Man
*Marilyn Anderson as Woman
*Jean Clark as Mechanic
*Donald M. Jones as Forest Ranger

== Release ==
 
The film was released theatrically in America. It also ran under the title Terror in the Forest, released in 1982 by Fury Films Ltds.
 Starmaker Video.

In 2006, Code Red released the film on DVD with an anamorphic widescreen transfer, a stills gallery, and commentary with director Donald. M. Jones and Gary Kent.

As a part of their Exploitation Cinema line, Code Red rereleased the film in 2009 as a double feature with Dont Go in the Woods (1981 film)|Dont Go In The Woods. This release did not include the commentary or stills gallery, but retained the widescreen transfer.

== Reception ==
 
The film received mixed reception by film critics.

Although DVD Talk reviewer Ian Jane praised Gary Kents acting ability, he wrote a negative review of the film stating that, "The Forest is definitely lower tier trash, even by the often times very low standards of the slasher and it really doesnt have a whole lot going for it aside from a few moments of unintentional humor."

Oh-The-Horror.com reviewer Brett Gallman praised the photography of the film but wrote a negative review calling it, "...an incoherent mess of a flick. There is almost no redeeming value to the film, as the acting is terrible, the music is generic, and the direction is mediocre."

Retro Slashers.net reviewer Daniel Perry wrote a positive review of the film stating that, "The Forest is actually a pretty enjoyable little flick. You can watch it for a good chuckle and also find that the storyline itself is pretty damn original. Don Jones gets an “A” for effort on this one."

== External links ==
*  
* 
* 
* 

 
 
 