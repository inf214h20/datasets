The Mistress of the World
{{Infobox film
| image          = File:The-mistress-of-the-world-saved-by-wireless.jpg
| caption        = American promotional poster, portraying events from Part V, Escape from Ophir
| name           = The Mistress of the World Joseph Klein Karl Gerhardt (Part 7)
| writer         = Karl Figdor Ruth Goetz Richard Hutter Joe May Wilhelm Roellinghoff  
| producer       = Joe May
| starring       = Mia May Michael Bohnen Henry Sze Hans Mierendorff
| music          = Ferdinand Hummel
| cinematography = Werner Brandes 
| editing        = 
| studio         = May-Film UFA
| based on       =  
| released       =  
| runtime        = 
| country        = Weimar Republic  German intertitles
}} German Expressionist cinema.

The story follows a young Danish woman on her globe-trotting adventures to find the legendary lost treasure of the Queen of Sheba that she hopes will enable her to bring revenge on the man that drew her father to commit suicide and led to her own social destruction.  The first film was released in Germany on 5 December 1919, and this was followed each week by the release of the subsequent episodes. 

==Development==
===Production===
Austrian film producer Joe May conceived the idea for filming The Mistress of the World, which he loosely based on the novel Die Herrin der Welt by author Karl Figdor, in early 1919.  May produced The Mistress of the World as a serial film, which Carjels argues was more influenced by the French serials of the time rather than the American cliff-hanger serials. Carjels (2011), pp.63-64  Whereas the popular American serials were filmed over two reels, making them short openers to a feature film, each part of The Mistress of the World was shot over six reels, making them feature length.  Another difference to the American serials was the lack of a cliffhanger ending. Each episode of Mistress was a self-contained story, but the end goal was still out of reach, drawing audiences back to find out how the story resolved rather than how the heroine escaped a dangerous situation. 

Normal prerelease hype for serials was often manufactured through movie tie-ins, serialization in other media and spot prizes given to audience members.  May attempted a different approach for The Mistress of the World, which at first appeared to be meager in comparison. He instead released a steady stream of information regarding the production of the film, which included stories around the films stars, the set design and the process of making the film. This in turn lead to massive audience anticipation, making The Mistress of the World one of the first   in Germany where a crew of 30,000 people fed by 100 cooks were utilised over the entire shoot and by the end of the production the film had cost six million Marks.  This steady stream of figures resulted in the series being described as "a completely new film experience", and even without the story being unveiled May had given the The Mistress of the World a feeling of importance and gravitas. 

===Cast and Crew===
*For the lead role of Maud Gregaards, Joe May cast his wife Mia May.       
*Michael Bohnen took the role of Consul Madsen, in parts I through IV. 
*Henry Sze played Dr. Kien-Lung, as one of Gregaards compaions. Sze was one of the few successful non-white actors in early German cinema, and played a sympathetic hero in parts I through to IV. 
  Paul Hansen as Allen Stanley in parts V through to VII. 
*Hans Mierendorff as the villainous Baron Murphy in parts I, II, IV, VII and VIII.
*Louis Brody as Simba the servant in parts I, V and VI and Mallkalle the Medicine Man in part IV.
*Ernst Hofmann as Credo Merville in part VIII.
*Rudolf Lettinger as Detective Hunt in part VIII.
*Wilhelm Diegelmann as Hannibal Harrison in part VI.
*Hermann Picha as Jonathan Fletcher in part VI. Paul Morgan as film maker Pius Gotthelf Karpeles in part VI.
*Victor Jansen as Bullbox in parts V and VI

As well as starring many stars of Weimar cinema, The Mistress of the World is also notable for bringing together many of the most important writers and production and set designers of early German Expressionism. Of note in parts II and III, acting as Assistant director, was Fritz Lang, who also acted as a script writer in part VIII. Working in the role of set and production design were Otto Hunte, Martin Jacoby-Boy, Erich Kettelhut and Karl Vollbrecht, all of whom would work in the years following with Lang on many of his iconic German films setting the tone of German Expressionist films in such classics as Dr. Mabuse the Gambler (1922), Die Nibelungen (1924) and Metropolis (1927 film)|Metropolis (1926).

==Reception==
The first part of The Mistress of the World was premiered on 5 December 1919 at the Tauentzienpalast in Berlin, along with other high-end cinemas. Carjels (2011) pp.68-69  The Tauentzienpalast was redecorated for the premier and the audience found themselves surrounded by tropical trees and bushes in an attempt to transport them to the exotic world of the film. Men dressed in Chinese servant uniforms handed out souvenirs, and large tapestries hung from the walls. 

Initial reviews to the film were very positive. Die illustrierte Filmwoche noted how no other film had created such anticipation and disappointed so little. Die Kinematograph saw it as a masterpiece of German film-making skill, while Der Film believed that "it will win over foreign markets". Carjels (2011) p.70  The more independent cultural publications were less positive, Berliner Börsen-Courier wrote that the serial did not rise above the average feature and that it lacked a sense of greatness, strength and depth.  George Gottholt, writing in the Freie Deutsche Bühne, was very negative in his views, stating "This film, and others like, it have a vulgarizing effect on the taste and a dumbing-down effect on the intellect of its audience." Carjels (2011) p.71  Carjels compares The Mistress of the World to a modern globe-trotting adventure story more akin to an Indiana Jones or Lara Croft film, rather than a more intellect take such as Madame Dubarry. 

Although receiving mixed reviews, the public responded positively and The Mistress of the World was the most commercially successful German film of the 1919-1920 season, outstripping The Cabinet of Dr. Caligari (1920) in popular appeal. 

==Plot==
===Part 1, The Girlfriend of the Yellow Man===
 

Young Danish adventuress Maud Gregaards (Mia May) answers an  advertisement to take up a position as a governess in China. There, she falls victim to the white slave-trade and is placed in a brothel. She is freed by her travel companion, Dr. Kien-Lung (Henry Sze), but the physician is then kidnapped by the devilish Hai-Fung, who also captures and tortures Maud. Chinese consul Madsen (Michael Bohnen) releases both Dr. Kein-Lung and Maud from the clutches of Hai-Fung. It is later revealed that Maud is harbouring a secret plot of revenge that brought her to China. Maud decides to tell her two trusted companions what that secret is.

===Part 2, The Race for Life===
 

The second part of this film is a prequel to the first part. Maud explains to her companions why she travelled to China. Mauds father was an archivist at the Foreign Office in Denmark. There he was blackmailed to hand over a Chinese secret agreement. Unable either to face the blackmailers or to betray his country, he commits suicide. Maud later falls in love with Baron Murphy (Hans Mierendorff) for whom she works as a translator, and they become engaged. Unknowingly, Maud translates the Chinese document for the Baron, after which he takes the secret and flees the country, leaving Maud to be arrested as a spy. In jail, Maud gives birth to the Barons child but the baby dies in prison. On being released from jail, Maud comes into the possession of information about the rabbi of Kuan-Fu, who is said to have knowledge of the hidden treasure of the Queen of Sheba. She decides to travel to China to find the treasure which she hopes will allow her to take revenge on her former lover. 

===Part 3, The City of Gold===
 

Maud Gregaards, Kien-Lung and Consul Madsen travel together to find the rabbi of Kuan-Fu, the last surviving member of an ancient Jewish settlement in Canton, who is said to be in possession of the secret to the fabled treasure of the Queen of Sheba. Halle, McCarthy (2003), p.18  The three companions enter the ancient ruins where the rabbi is believed to live, watched suspiciously by the local natives. On finding the old rabbi, Madsen reveals that he is Jewish and gains the trust of the holy man.  They are entrusted with the Gem of Astarte, which was once given to King Solomon by the Queen of Sheba, and within it the map to the treasure. Before he can reveal any further information the rabbi dies. On leaving the ruins Madsen is injured fighting with the natives, and is abandoned by Kien-Lung; the Gem of Astarte is briefly lost in the same encounter. As Madsen meets back up with Kien-Lung, a fight breaks out between the two men, only Mauds intervention separates the two.

===Part 4, King Macombe===
 

Maud, along with, Madsen and Kien-Lung, adventure into Central Africa, to search for the biblical city of Ophir where the treasure lies. There they enter the realm of King Makombe, at the heart of the Cult of Astarte. The tribes Medicine Man (Louis Brody) steals the Gem of Astarte to appease the natives against the invaders. Maud, Madsen and Dr. Kien-Lung flee but during the escape Kien-Lung is struck by a poisoned arrow and dies. Maud and Madsen find shelter in a cave, where they are hidden from their pursuers. In the final scene, Maud and Madsen reach the gates of Ophir.

===Part 5, Ophir, City of the Past ===
 
 
 Paul Hansen), whom he met in the slave city, and returns to rescue Maud. The three find the treasure and manage to telegraph the outside world for help. An American expedition hears their plea and sends a plane to rescue all three, but Madsen is killed in the escape. As they fly from the city an earthquake leaves the mysterious City of Ophir in ruins.

===Part 6, The Woman with the Billions===
 
 Paul Morgan), makes a movie about Mauds adventure. Although little more than fabrication, the movie makes her a superstar. Meanwhile Maud and Allan fall in love and leave for Europe.

===Part 7: The Benefactress of Mankind===
 

The billionaire Maud Fergusson, attempts to start a new life with Allan Stanley. The wealth gained from the Queen of Shebas treasure allows the "Mistress of the World" to live a carefree life in Denmark and the two plan to marry. Stanley insists that before they marry that Maud tells him the name of the man who is responsible for all her misfortunes. Maud, however, has put her past behind her and just wants to find peace. She plans to become a benefactor of mankind, sponsoring research and technology projects with their billions. Allan, meanwhile, develops a remote fusion machine that can melt metal. Since guns can be destroyed with this powerful invention, he soon falls into the sights of ruthless weapon manufacturers that want to stop its production. On the day of the inventions first public demonstration, there is a huge explosion, caused by Mauds nemesis Baron Murphy. Allan is killed in the explosion.

===Part 8: The Revenge of Maud Fergusson===
 

The death of Allan, shortly before their wedding, sends Maud on a quest for bloody revenge. Helped by a detective, Hunt, (Rudolf Lettinger), Maud discovers that Baron Murphy is behind her fiancés death and sparks a newspaper campaign against the Baron to expose him. Murphy loses his entire fortune and is forced to leave the country and dies while desperately battling through a snowstorm. Hunt also discovers that a young academic Credo Merville (Ernst Hofmann) is Murphy and Mauds son, who is supposed to have died shortly after his birth. In the end, mother and son fall into each others arms.

==1960 film==
In 1960 CCC Films released a two part Mistress of the World feature film directed by William Dieterle starring Martha Hyer as Karin Fergusson.

==References==
 

==Bibliography==
* 
* 
* 

==External links==
 
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 