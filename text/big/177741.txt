Min and Bill
{{Infobox film
| name           = Min and Bill
| image          = Min and bill 1930 poster.jpg
| image size     = 
| caption        = 1962 re-release poster
| director       = George W. Hill
| producer       = George W. Hill
| writer         = Frances Marion Marion Jackson
| based on       =  
| starring       = Marie Dressler Wallace Beery
| music          = 
| cinematography = Harold Wenstrom
| editing        = Basil Wrangell
| distributor    = Metro-Goldwyn-Mayer
| released       = November 29,  
| runtime        = 66 minutes
| country        = United States
| language       = English
| gross = $2 million 
}}
 
Min and Bill is a 1930 American comedy-drama film starring Marie Dressler and Wallace Beery and based on Lorna Moons novel Dark Star, adapted by Frances Marion and Marion Jackson.

The movie tells the story of dockside innkeeper Mins tribulations as she tries to protect the innocence of her adopted daughter Nancy, all while loving and fighting with boozy fisherman Bill, who resides at the inn.
 Dorothy Jordan (Nancy), and Marjorie Rambeau (Bella, Nancys ill-reputed mother), and was directed by George W. Hill. Dressler won the Academy Award for Best Actress in 1931 for her performance in this film.    

This film was such a runaway hit that it and its near-sequel Tugboat Annie, which reteamed Dressler and Beery in similar roles,  boosted both to superstar status. Dressler topped Quigley Publications annual Top Ten Money Making Stars Poll of movie exhibitors in 1933, and the two pairings with Dressler were primarily responsible for Beery becoming MGMs highest paid actor in the early 1930s, before Clark Gable took over that crown; Beery had a clause in his 1932 contract that he be paid a dollar per year more than any other actor on the lot.

 

 

==Cast==
*Marie Dressler as Min Divot
*Wallace Beery as Bill Dorothy Jordan as Nancy Smith
*Marjorie Rambeau as Bella Pringle
*Donald Dillaway as Dick Cameron
*DeWitt Jennings as Mr Groot
*Russell Hopton as Alec Johnson
*Frank McGlynn, Sr. as Mr Southard
*Gretta Gould as Mrs Southard

== Plot ==
Min Divot (Marie Dressler) runs a dockside inn.  She has been raising Nancy Smith (Dorothy Jordan) as her own since her prostitute mother, Bella (Majorie Rambeau) left her at the inn as an infant.  Min frequently argues with fisherman Bill (Wallace Beery).  Despite Bills near constant drinking, he and Min care for each other.  She and Bill are the only ones who know the real identity of Nancys real, still living, mother. 

Min does her best to raise Nancy and keep her from learning about the real activities of the people who live and work on the docks.  Despite not having much extra money or a home outside her inn, Min does her best to raise Nancy into a young lady.  She does everything she can to make sure Nancy is never around when Bella arrives for a visit.

Nancy loves Min as her own mother and frequently skips school to be with her.  After repeatedly dealing with the truant officer, Min uses the money she had hidden in her room to send Nancy to a fancy boarding school.  She hopes the school will teach Nancy better manners than what she had been picking up from Bill and the others on the docks.  The schooling works and Nancy returns to Min with good manners, an education, and the news that she is now engaged to a very wealthy man.  She wants Min to attend the wedding.  

Min is thrilled until she finds out that Bella has returned.  Seeing how happy Nancy is to be getting married (and the wedding will be taking place in a few days), Min deliberately argues with Nancy and says terrible things she doesnt mean in order for Nancy to immediately leave.  She is mad at herself for hurting Nancy, but is relived that she is gone by the time Bella arrives.  Min stalls Nancys mother, hoping that the wedding will take place and the couple leaves for their honeymoon before she can interfere.  

Bella arrives as the ceremony takes place.  She confronts Min in an upstairs room in her inn.  She has discovered her daughters identity, and that of her very wealthy new husband.  She taunts Min with the information and pledges that she will torment Nancy and her new husband until they give her money and take her into their new house.  

Min thinks about the wedding and Nancys happiness.  As Bella turns to leave, Min takes a hidden gun and shoots her dead.  Min drops the gun and flees the room.  Bill, knowing what was going on, tries to help Min, but she leaves the inn.  

Min wants to see Nancy one last time.  She sees the happy couple as they are about to board a boat to their honeymoon.  Min watches but then decides not to let Nancy know she is there and stays hidden in the crowd.  Two police officers quietly confront Min about the shooting in her inn.  Min doesnt say much.  She takes one final look at a smiling Nancy as she leaves with her husband.  Min turns back and smiles as she quietly walks away with the officers.

== In popular culture ==
Jack Kerouac, in On the Road, has his protagonist-narrator Sal Paradise compare Dean Moriarity and his second wife Camille to Min and Bill. Kerouac does not explain the reference, but it would be understood by contemporary readers  that he was signaling that the couple had a contentious but affectionate relationship, with Dean the weak, neer-do-well and Camille the heart and soul of the relationship.

At Disneys Hollywood Studios in Walt Disney World, homage to Min and Bill is paid in the form of a counter service restaurant.  Min and Bills Dockside Diner is in the shape of Bills fishing trawler, and "floats" in Echo Lake near the center of the park.

==Reception==
The film made a profit of $731,000. Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 191 
==References==
 

== External links ==
* 
*  

 

 

 
 
 
 
 
 
 
 
 

 