Chilling Visions: 5 Senses of Fear
{{Infobox film
| name           = Chilling Visions: 5 Senses of Fear
| image          = File:ChillingVisionsMoviePoster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      =  Eric England,  Nick Everhart,  Emily Hagins,  Jesse Holland,  Miko Hughes,  Andy Mitton
| producer       = 
| writer         = Eric England,  Nick Everhart,  Emily Hagins,  Jesse Holland,  Miko Hughes,  Andy Mitton,  Jack Daniel Stanley	
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Matthew Llewellyn
| cinematography = Bernard Evans (segment "Listen, My Children"),  Claudio Rietti,  Mike Testin (segment "Taste")
| editing        = Bryan Capri,  Nick Everhart,  Jesse Holland,  Andy Mitton
| studio         = Chiller Films, Synthetic Cinema International
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Eric England (Taste), Nick Everhart (Smell), Emily Hagins (Touch), Miko Hughes (See), Jesse Holland and Andy Mitton (Listen).  The anthology was originally broadcast on the Chiller (TV network)|Chiller network on May 31, 2013 and was released onto DVD and Blu-ray on October 22 of the same year. 

==Synopsis==
The film was composed of five short films and did not have a wraparound story, instead linking the shorts together with the mysterious company Watershed, a company with sinister intentions.

===Smell=== door to door saleslady offers him a cologne that can change everything for him, Seth takes her up on the offer. Her only warning is that he shouldnt use too much of it. Excited when the cologne starts making him more successful at work and love, Seth is soon horrified when the colognes side effects start to work on him.

===See===
See (directed by Miko Hughes) follows Dr. Tom (Ted Yudain), an optometrist that is able to see through the eyes of his patients. This is initially fun for him until he discovers that one of his patients is experiencing domestic abuse at the hand of her boyfriend (Lowell Byers). Tom tries to intervene by using his ability to cause the boyfriend to experience hellish visions, but this soon backfires.

===Touch===
Touch (directed by Emily Hagins) follows a young blind boy that must go search for help after his parents are wounded in a car accident. In his desperation he comes across a killer (Lowell Byers) living in a set of abandoned buildings that has an aversion to being touched.

===Taste===
Taste (directed by Eric England) is centered upon Aaron (Doug Roland), a hacker that finds himself in a large corporate building at the bequest of Watershed businesswoman Lacey (Symba Smith), who wants to interview him. Hes offered a lucrative job, but refuses and is instead treated to a deadly encounter.

===Listen=== found footage short that follows two men making a movie about Listen, My Children, a song that has the ability to kill. They manage to find footage of a doctor conducting experiments with the song and ultimately end up playing the song for others.

==Reception==
Critical reception for the film has been mixed.  Twitch Film commented that like many other anthologies, some of the shorts were "duds" but that overall the shorts were "fairly consistent".  Reviews from Bloody Disgusting were divided, with one reviewer criticizing the film as being "more predictable than terrifying" while the other reviewer stated that it was a "mixed bag, but a completely watchable one straight through."   Shock Till You Drop panned the first three shorts (Smell, See, and Touch) as they felt that viewers could "definitely skip the first three and not miss anything" while praising the final two (Taste and Listen). 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 