The Trail of the Lonesome Pine (1923 film)
 
{{Infobox film
| name           = The Trail of the Lonesome Pine
| caption        = Film poster
| image	         = The Trail of the Lonesome Pine FilmPoster.jpeg
| director       = Charles Maigne
| producer       = 
| writer         = Eugene Walter (adaptation)
| screenplay     = Will M. Ritchey
| based on       =  
| starring       = Mary Miles Minter James Howe
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}} silent drama of the same name, the film starred Mary Miles Minter in her final film role. The Trail of the Lonesome Pine film is now considered lost film|lost. Three other adaptions exist, including earlier 1914 and 1916 silent versions as well as a The Trail of the Lonesome Pine (1936 film)|1936, all color and sound film.

==Cast==
* Mary Miles Minter as June Tolliver
* Antonio Moreno as John Hale
* Ernest Torrence as Devil Jud Tolliver
* Ed Brady as Bad Rufe Tolliver (as Edwin J. Brady)
* Frances Warner as Ann
* J. S. Stembridge as Buck Falin
* Cullen Tate as Dave Tolliver

==Production==
The Trail of the Lonesome Pine was the second film on which cinematographer James Wong Howe earned his reputation leading him to become one of the most sought cinematographer of the era. Listed as first camera, essentially, Howe was considered the director of photography. Rainsberger  1982, p. 18.  Specifically, Howe made Mary Miles Minters blue eyes register on orthochromatic film with a filter.    With the success evident in Howes work, especially in lighting, "every blue-eyed actor and actress wanted him as their photographer. 

==Other adaptions== a second in 1936, and stars Sylvia Sidney and Fred MacMurray.

==References==

===Notes===
 

===Bibliography===
 
* Rainsberger, Todd. James Wong Howe, Cinematographer.  New Haven, Connecticut: A.S. Barnes & Co., 1982. ISBN 978-0-49802-405-4.
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 