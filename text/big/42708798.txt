Louise, Queen of Prussia (film)
{{Infobox film
| name =  Louise, Queen of Prussia 
| image =
| image_size =
| caption =
| director = Carl Froelich
| producer =  Wilhelm von Kaufmann    Henny Porten
| writer =  Walter von Molo  (novel)   Fred Hildenbrand Stuart   Friedrich Raff    Julius Urgiss   
| narrator =
| starring = Henny Porten   Gustaf Gründgens   Ekkehard Arendt   Vladimir Gajdarov
| music = Hanson Milde-Meissner   
| cinematography = Friedl Behn-Grund   
| editing =      
| studio = Henny Porten Filmproduktion 
| distributor = 
| released =  4 December 1931 
| runtime = 115 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama film directed by Carl Froelich and starring Henny Porten, Gustaf Gründgens and Ekkehard Arendt. The films art director was Franz Schroedter. 

It depicts the life of Louise of Mecklenburg-Strelitz (1776-1810), the wife of Frederick William III of Prussia. It forms part of the Prussian film genre.

The film was produced by Portens own production company, founded during the silent era when she has been a dominant German star. The failure of the film led to the financial ruin of Portens production company and she appeared in much fewer films after this point. 

==Cast==
* Henny Porten as Königin Luise 
* Gustaf Gründgens as König Friedrich Wilhelm III 
* Ekkehard Arendt as Prinz Louis Ferdiand 
* Vladimir Gajdarov as Zar Alexander 
* Friedrich Kayßler as Freiherr von Stein 
* Helene Fehdmer as Gräfin Voss  Paul Gunther as Napoleon
* Christian Grautof as Knabe  
* Inge Landgut as Mädchen  
*   Friedrich Ettel 
* Friedrich Gnaß 
* Bernhard Goetzke 
* Ferdinand Hart 
* Eva LArronge
* Aribert Mog 
* John Mylong 
* Veronika Nargo 
* Walter Steinbeck
* Hugo Werner-Kahle
* Paul Westermeier

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.
* Killy, Walther (ed.). Dictionary of German Biography: Volume VIII. Walter de Gruyter, 2005.

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 