A Friend's Betrayal
 
{{Infobox film
| name           = A Friends Betrayal
| image          = Stolen Youth DVD cover.jpg
| image size     =
| caption        = DVD cover
| director       = Christopher Leitch
| producer       = Jay Benson
| writer         = Hal Sitowitz J. B. White
| narrator       =
| starring       = Brian Austin Green Sharon Lawrence
| music          = W. G. Walden
| cinematography = Robert Primes
| editing        = Martin Nicholson
| studio         =
| distributor    = NBC
| released       = May 19, 1996
| runtime        = 94 minutes US
| English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1996 television television drama film directed by Christopher Leitch. The reviewers were mostly positive about Sharon Lawrences acting performance. 

== Plot ==
Paul Hewitt is an 18-year-old amateur photographer, leading an average life with his parents and sister Ella. He is a senior at high school and he is in the middle of making college decisions. His parents have always dreamed for him to become a doctor, but he isnt sure if he even wants go to college. One day, the family Hewitt receives a visit from mother Abbys friend Nina Talbert, whose mother recently died. Nina is a photographer as well, providing covers for music albums. Although being in a relationship with classmate Cindy, Paul immediately feels attracted to her. Nina, feeling lonely since trying to break up with her married boyfriend Robert, bonds with him. Abby notices this and convinces her to invite Paul to go with her to San Francisco, to look at a campus.

There, the tension between them grows and they eventually make love with each other. Upon their return, Nina, not wanting to face the Hewitt family, plans on returning to New York City. This upsets Paul, who has fallen in love with her and he shows no interest in Cindy any longer. While he is spending the night with Nina, Abby goes into his darkroom and is shocked to only find photos of Nina. She decides to go to her place to confront her about is, and she is shocked to find her kissing Paul. Furious, she blames it all on Nina, slapping her. She forbids Paul to see Nina and considers calling the police. Upset, Paul decides to run away from home. Nina is later confronted for a second time, but she shows no remorse, explaining that she loves him.

Paul is glad to hear that she will not leave to New York. When they decide to move in with each other, Abby is determined to do something about it. However, her husband Dennis tells her that trying to drive them apart will only make them wanting to see each other more. Soon, the news of their scandalous relationship is spread around his school, which makes Cindy breaking up with him. Meanwhile, Ellas sixteenth birthday is coming up. When she runs into Nina, she lies that she and Paul are welcome to visit her on her party. However, Abby is furious to see her and angrily sends her away. Ella feels that her party is ruined and tells her mother she hates her. She runs away from home, but is hit by a car. Although she recovers, the accident has a great deal of impact on Paul and Ninas relationship. She decides to leave him and return to New York, allowing him to reunite with his family. Abby eventually forgives Nina and accepts Pauls decision not to become a doctor.

==Cast==
*Brian Austin Green as Paul Hewitt
*Sharon Lawrence as Nina Talbert
*Harley Jane Kozak as Abby Hewitt
*John Getz as Dennis Hewitt
*Katie Wright as Cindy Gerard
*Ashleigh Aston Moore as Ella Hewitt
*Jeremy Renner as Pauls friend
*John Carroll Lynch as Mr. Franks

==References==
 

==External links==
* 

 
 
 
 
 
 