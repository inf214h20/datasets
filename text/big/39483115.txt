Big Game (2014 film)
 
{{Infobox film
| name = Big Game
| image = Big Game poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Jalmari Helander
| producer = {{Plainlist|
* Will Clarke
* Petri Jokiranta
* Andy Mayson
* Jens Meurer}}
| writer = {{Plainlist|
* Jalmari Helander
* Petri Jokiranta}}
| starring = {{Plainlist|
* Samuel L. Jackson
* Onni Tommila
* Felicity Huffman
* Victor Garber
* Ted Levine
* Jim Broadbent Ray Stevenson}}
| music = {{Plainlist|
* Juri Seppä
* Miska Seppä}}
| cinematography = Mika Orasmaa
| editing = Iikka Hesse
| production companies = {{Plainlist|
* Altitude Film Entertainment Ltd Bavaria Film Partners
* Egoli Tossell Film GmbH
* Film House Germany
* Head Gear Films
* Ketchup
* Metrol Technology
* Subzero Film Entertainment VisionPlus Fund I
* Waterstone}}
| distributor = {{Plainlist| Nordisk Film Distribution  
* Ascot Elite Entertainment Group  
* Entertainment One  
* EuropaCorp  }}
| released =  
| runtime = 90 minutes  
| country = {{Plainlist|
* Finland
* Germany
* United Kingdom}}
| language = {{Plainlist|
* English
* Finnish}}
| budget = €8.5 million 
| gross = $1.9 million 
}} action adventure Ray Stevenson.

The film premiered at the 2014 Toronto International Film Festival and was generally well received, with IGN citing it to be "a throwback to ’80s and ’90s adventure movie with a dash of comic book violence thrown in for good measure.".   

The films budget was €8.5 million, making it the most expensive ever produced in Finland.   

==Plot==
  terrorists leaving the President of the United States (Samuel L. Jackson) stranded in the wilderness of Finland, there is only one person around who can save him: a 13-year old boy called Oskari (Onni Tommila). In the forest on a hunting mission to prove his maturity to his kinsfolk, Oskari had been planning to track down a deer, but instead discovers the most powerful man on the planet in an escape pod. With the terrorists closing in to capture their own "Big Game" prize, the unlikely duo must team up to escape their hunters. As anxious Pentagon officials observe the action via satellite feed, it is up to Oskari and his new sidekick to prove themselves and survive the most extraordinary 24 hours of their lives.

==Cast==
* Samuel L. Jackson as President of the United States
* Onni Tommila as Oskari CIA Director
* Victor Garber as Vice President of the United States
* Ted Levine as General Underwood
* Jim Broadbent as Herbert  Ray Stevenson as Morris
* Mehmet Kurtuluş as Hazar
* Jaymes Butler as Otis

==Release==
===Box office===
Big Game opened in Finland on 20 March 2015 at number 4, taking in $324,321 from 113 screen. The following week it moved up two spots to number 2, but dropped -38% to finish the weekend with $199,996 from 103 screens.  The film has made $1,162,373 (€1,041,463) as of 26 April.

As of 26 April 2015, the film has a worldwide total of $1,906,206.   

===Critical reception===
The film received generally positive reviews from critics. On review aggregator website Rotten Tomatoes, the film has an 82% rating based on 11 reviews. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 