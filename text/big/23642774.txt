Darklands (film)
 
 
{{Infobox film
| name           = Darklands
| image          =
| caption        = Julian Richards
| producer       = Paul Brooks, Peter Edwards, Alan Martin, Clive Waldron   Julian Richards
| starring       = Craig Fairbrass, Jon Finch, Rowena King
| music          = David A. Hughes, John Murphy   
| cinematography = Zoran Djordjevic
| editing        = Mark Talbot-Butler   
| distributor    =
| released       = 26 November 1997
| runtime        =
| country        =   English
| budget         =₤500,000 Alexander Walker, Icons in the Fire: The Rise and Fall of Practically Everyone in the British Film Industry 1984-2000, Orion Books, 2005 p274-275 
| gross = ₤11,000 
| preceded_by    =
| followed_by    =
| website        =
}} Welsh horror Julian Richards, starring Craig Fairbrass, Jon Finch, Rowena King and released in 1997 in film|1997.  

Richards wrote the screenplay after attending the Beltane Fire Festival in Edinburgh and it was produced by Paul Brooks at Metrodome Films.

Dubbed "the Welsh Wicker Man" by the UK press, Darklands is possibly the first home grown Welsh horror film. 

==Plot==
Darklands follows journalist Frazer Truick as he investigates the mysterious death of the brother of trainee journalist Rachel Morris. Delving deeper, Truick becomes convinced that the tragedy was murder, committed by a bizarre religious cult. But as the evidence unfolds, things take on a more sinister and potentially lethal significance for the reporter, as he becomes embroiled in devil worship, witchcraft and ultimately human sacrifice.

==Release==
In 1998 Darklands was theatrically released in the UK by Metrodome Films, released on VHS by Pathé and broadcast by ITV (TV network)|ITV.

Darklands is available on DVD in Germany (Splended Films), France, Fox Film Corporation and Spain (Filmax).

On 20 November 2012 Darklands will be released on DVD in the USA and Canada by MVD Distribution.

==Awards==
*Critics Award - Fantasporto
*Méliès dArgent - Grand Prize of European Fantasy Film in Silver - Fantasporto
*Best Screenplay - Fantasporto
*Special Jury Award - Fantasporto
*Best Independent Feature Award - Festival of Fantastic Films (UK)
*Silver Remi Award - WorldFest Houston

==Festivals==
*Welsh International Film Festival - Aberystwyth, UK
*Fantasporto Film Festival - Portugal
*Brussels International Festival of Fantasy Films - Belgium
*Valenciennes Action & Adventure Film Festival - France
*Amsterdam Fantastic Film Festival - Netherlands
*Newport Beach Film Festival - USA
*WorldFest-Houston International Film Festival - USA
*Fantafestival - Rome, Italy
*Shots In The Dark Film Festival - Nottingham, UK
*Puchon Fantastic Film Festival - South Korea Espoo Ciné - Finland
*Festival of Fantastic Films (UK) - Manchester
*Lund Fantastisk Film Festival - Sweden
*Sitges Film Festival - Spain
*Leeds Film Festival - UK
*San Sebastián Film Festival - Spain
*Cinenygma Fantasy Festival - Luxemburg
*Scienceplusfiction Film Festival -Trieste, Italy
*Another Hole in the Head Genre Film Festival -San Francisco, USA

==Critical reaction==

Action horror with elements of The Wicker Man - Variety. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 