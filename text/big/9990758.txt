Tazza: The High Rollers
{{Infobox film name           = Tazza: The High Rollers image          = Tazza - The High Rollers.jpg caption        = Tazza: The High Rollers film poster film name      = {{Film name hangul         =   rr             = Tajja mr             = T‘acha}} director       = Choi Dong-hoon producer       = Cha Seung-jae Kim Mi-hee    writer         = Choi Dong-hoon based on       =   starring       = Jo Seung-woo Kim Hye-soo Yoo Hae-jin Baek Yoon-sik music          = Jang Young-gyu  cinematography = Choi Young-hwan  editing        = Shin Min-kyung  studio = Sidus FNH distributor    = CJ Entertainment released       =   runtime        = 139 minutes country        = South Korea language       = Korean budget         =   
| gross         =   
}} same name. Sidus FNH Hwatu ( South Koreas highest grossing films and winning numerous awards.

==Plot==
Goni has lost his entire savings, and money stolen from his family, after being swindled by professional cheat gamblers. In order to regain the money, Goni begins training under one of the best gamblers in the country, Mr. Pyeong. He becomes well-known, wandering about different gambling places throughout the country with Pyeong. Madam Jeong, who runs an illegal gambling operation, begins to show interest in Goni. Goni leaves Pyeong and begins working for Jeong, whom he also has a love tryst with. When Madam Jeong is arrested, Goni meets fellow pro Gwang and the two become partners.

==Cast==
*Jo Seung-woo as Kim Goni
*Kim Hye-soo as Madam Jeong
*Yoo Hae-jin as Ko Gwang-ryeol
*Baek Yoon-sik as Mr. Pyeong
*Kim Yoon-seok as Agui Kim Eung-soo as Kwak Cheol-yong Kim Sang-ho as Park Moo-seok
*Lee Soo-kyung as Hwa-ran
*Kim Jung-nan as Se-ran
*Huh Young-man (cameo appearance|cameo)

==Awards and nominations==
;2006 Blue Dragon Film Awards 
* Best Actress – Kim Hye-soo
* Best Cinematography – Choi Young-hwan
* Nomination – Best Film
* Nomination – Best Director – Choi Dong-hoon
* Nomination – Best Supporting Actor – Kim Yoon-seok
* Nomination – Best Visual Effects – Jeong Do-an

;2007 Asian Film Awards
* Nomination – Best Actress – Kim Hye-soo

;2007 Baeksang Arts Awards
* Grand Prize (Daesang)
* Best Director – Choi Dong-hoon
* Nomination – Best Film
* Nomination – Best Actor – Jo Seung-woo
* Nomination – Best Actress – Kim Hye-soo

;2007 Grand Bell Awards
* Best Supporting Actor – Kim Yoon-seok
* Best Costumes – Jo Sang-gyeong
* Nomination – Best Actress – Kim Hye-soo
* Nomination – Best Director – Choi Dong-hoon
* Nomination – Best Cinematography – Choi Young-hwan
* Nomination – Best Editing – Shin Min-kyung

;2007 Korea Movie Star Awards
* Best Actor – Jo Seung-woo
* Best Actress – Kim Hye-soo
* Best Supporting Actor – Kim Yoon-seok

;2007 Korean Film Awards
* Best Editing
* Nomination – Best Film
* Nomination – Best Actor – Jo Seung-woo
* Nomination – Best Actress – Kim Hye-soo
* Nomination – Best Director – Choi Dong-hoon
* Nomination – Best Cinematography – Choi Young-hwan

;2007 Newport Beach Film Festival 
* Best Director – Choi Dong-hoon
* Jury Award – Best Feature
* Jury Award – Best Actress – Kim Hye-soo
* Jury Award – Best Feature Actor – Jo Seung-woo

==DVD== 5 Points Pictures gave the film a two-disc DVD release in North America on September 18, 2012.  The film is subtitled and includes nearly 3 hours of bonus features, including the making of the film, a comparison between the film and its source manhwa, and gambling tricks explained by a former professional gambler.

==Sequel==
 
 Choi Seung-hyun (T.O.P), Shin Se-kyung, Kwak Do-won and Lee Ha-nui, with Yoo Hae-jin and Kim Yoon-seok reprising their roles. It began filming on January 2, 2014 and was released on September 3, 2014. 

==See also==
*List of Korean language films
*Cinema of Korea

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 