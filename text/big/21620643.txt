Halloween Night
{{Infobox film
| name           = Halloween Night
| image          = Halloween Night dvd.jpg
| alt            = 
| caption        = DVD cover
| director       = Mark Atkins
| producer       = David Michael Latt   Sherri Strain
| writer         = Michael Gingold
| story          = David Michael Latt
| starring       = Derek Osedach   Rebekah Kochan   Scot Nery
| music          = Mel Lewis
| cinematography = David Michael Latt
| editing        = Mark Atkins
| studio         = The Asylum
| distributor    = The Asylum
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} mockbuster film, for which The Asylum are infamous.

==Plot==
 insane asylum based largely on the fact that Chris had been found at home with the bodies by the police.
 insane and, to prove his innocence, he escapes from the asylum on October 31 to his home, which is now occupied by a new family who are holding a Halloween party.

As the night draws on, Chris attempts to search the house for any clues that will testify his innocence, but in an effort to conceal himself from the partygoers and the authorities who are searching for him, begins his own killing spree that leads to a massacre far worse than the original.

==Cast==

* Derek Osedach - David Baxter
* Rebekah Kochan - Shannon
* Scot Nery - Christopher Vale
* Sean Durrie - Larry
* Alicia Klein - Tracy
* Erica Roby - Angela
* Amanda Ward - Kendall
* Jared Michaels - Daryll
* Jay Costelo - Eddie
* Michael Schatz - The Troll
* Amelia Jackson-Gray - Jeanine
* Nicholas Daly Clark - Todd
* Tank Murdoch - Officer Connors
* Stephanie Christine Medina - Kim
* Jonathan Weber - Charlie

==See also==

* Halloween (2007 film)|Halloween - Another Halloween-themed film released around the same time

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 