The Idiot (1951 film)
{{Infobox film
| name           = The Idiot
| image          = Hakuchi poster.jpg
| image_size     =
| caption        = Original Japanese poster showing Toshirō Mifune (left), Masayuki Mori (centre) and Setsuko Hara (right)
| director       = Akira Kurosawa
| producer       = Takashi Koide
| writer         = Akira Kurosawa Eijirō Hisaita
| based on       = The Idiot by Fyodor Dostoevsky
| narrator       = Masayuki Mori Takashi Shimura  Noriko Sengoku
| music          = Fumio Hayasaka
| cinematography = Toshio Ubukata
| editing        = Akira Kurosawa
| studio         =  Shochiku
| distributor    = Shochiku
| released       = 23 May 1951  (Japan)  30 April 1963  (US) 
| runtime        = 166 minutes
| country        = Japan Japanese
| budget         =
}} novel of the same name by Fyodor Dostoevsky.

==Plot==
After narrowly escaping death during efforts in a recent war, Kameda returns to Japan from Okinawa, where he has been confined to an asylum. Because he is subject to seizures of epilepsy, he is considered to be mentally ill. During his journey home, he becomes acquainted with the wealthy Akama. Inadvertently, the two fall in love with the same woman, Taeko; in addition, Kameda is attracted to Ayako, another woman who returns his affection. Kameda soon realizes that he prefers Taeko and is disheartened to find that she is another mans mistress. He offers her money in return for her love, but she throws the money into a fire and gives herself to him. Akama learns of the affair, resulting in a quarrel between him and Kameda. Realizing that he has lost Taeko, Akama stabs her, and both men go mad with grief.

==Cast==
 
 Masayuki Mori as Kinji Kameda, the idiot
* Toshiro Mifune as Denkichi Akama
* Setsuko Hara as Taeko Nasu
* Yoshiko Kuga as Ayako
* Takashi Shimura as Ono, Ayakos father
* Chieko Higashiyama as Satoko, Ayakos mother
* Eijirō Yanagi as Tohata
* Minoru Chiaki as Mutsuo Kayama, the secretary
* Noriko Sengoku as Takako

==Production background==
The film is based on the novel   studio, after the previous years Scandal (1950 film)|Scandal. 

Originally intended to be a two-part film with a running time of 265 minutes, the film was severely cut at the request of the studio, against Kurosawas wishes, after a single poorly received screening of the full-length version. When the re-edited version was also deemed too long by the studio, Kurosawa sardonically suggested the film be cut lengthwise instead.    According to Japanese film scholar Donald Richie, there are no existing prints of the original 265-minute version. Kurosawa would return to Shochiku forty years later to make Rhapsody in August, and, according to Alex Cox, is said to have searched the Shochiku archives for the original cut of the film to no avail.
 Akira Kurosawa {{cite web url = http://www.eurekavideo.co.uk/moc/016.htm title = The Idiot accessdate = 2007-09-09 publisher = Masters of Cinema
}} }}

==See also==
*List of incomplete or partially lost films

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 

 