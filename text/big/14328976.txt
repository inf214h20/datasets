The Brotherhood of Satan
{{Infobox film
| name           = The Brotherhood of Satan
| image          = BrotherhoodofSatanposter.jpg
| image_size     =
| caption        = movie poster
| director       = Bernard McEveety
| producer       = L. Q. Jones Alvy Moore
| writer         = L. Q. Jones Sean MacGregor Charles Bateman Charles Robinson Alvy Moore Geri Reischl
| music          = Jaime Mendoza-Nava
| cinematography = John Arthur Morrill
| editing        = Marvin Walowitz
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 92 minutes
| country        = United States English
| budget         =
| gross          =
}}

The Brotherhood of Satan is a 1971 low-budget horror film written, produced and starring L. Q. Jones It was directed by Bernard McEveety and also stars Strother Martin.

==Plot== Charles Bateman), Charles Robinson).  These locals explain the unusual events in the town which involve several murders, the inability of the people to leave the town, and that many of the local children have gone missing.

A local coven of elderly witches have been taking the children and leading them to worship Satan in a plot to use their bodies as receptacles for their own souls.  They use their supernatural abilities to kill anyone who interferes by turning the childrens toys into instruments of murder.

The priest figures out that the coven is taking the children, and tells people, including Ben and Nicky, because K.T. has gone missing. However, he sees the murder of a man trying to find his son, Joey. He goes crazy, turning into a blubbering mess. 

The people searching for the children, Sheriff, Tobey, Nicky and  Ben, cant find Doc Duncan, (who is either Satan or the ringleader of the ceremonies. It is never said directly.) and search his house. They find the toy that came to life and killed Joeys father, Mike. It is a knight on horseback, and there is blood on the tiny sword.

They show the priest the toy and he starts to scream. They take the toy away and try to open a locked door. 

On the other side of the door, a bloody ceremony, in which the coven members allow themselves to be killed by hooded bearers of flaming swords in order to take over the bodies of the now zombie-like children, is taking place. The camera flashes from the searchers struggling to open the door and the covern members being willingly slain.

When they finally open the door, they see the children staring back at them. They are in a practically empty room, with the children, a table, dolls that resemble some of the black-cloaked coven members and a music box. The children continue to stare at the people and the camera slowly pans to an empty, black hole in a corner. The screen goes dark, and bright pink gothic words show up, saying, "Come in, children."

==Cast==
*Strother Martin as Doc Duncan
*L. Q. Jones as Sheriff Charles Bateman as Ben
*Ahna Capri as Nicky Charles Robinson as Priest
*Alvy Moore as Tobey
*Geri Reischl as KT

==Additional information==
*The Brotherhood of Satan was filmed in Albuquerque, New Mexico.

*A book adaptation, titled The Brotherhood of Satan, by L. Q. Jones was published by Universal-Award in 1980 (ISBN 0441083013).

*The Brotherhood of Satan was released on VHS in 1983 by RCA/Columbia Pictures Home Video ( ).  A DVD version was released in 2002 by Columbia TriStar Home Entertainment ISBN 0-7678-8253-9).

==References in Popular Culture==

The industrial dance group, My Life With The Thrill Kill Kult, sampled The Brotherhood of Satan in the song "Rivers of Blood, Years of Darkness" from their 1990 Confessions of a Knife album.

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 