The Killing of the Imam
 

{{Infobox film
| name           = The Killing of the Imam
| image          = 
| alt            =  
| caption        = 
| director       = Khalid Shamis
| producer       = Tubafilms
| screenplay     = Khalid Shamis
| starring       = 
| music          = Mark Roberts
| cinematography = Khalid Shamis
| editing        = Khalid Shamis
| studio         = 
| distributor    = 
| released       =   
| runtime        = 10 minutes
| country        = South Africa
| language       = 
| budget         = 
| gross          = 
}}

The Killing of the Imam  is a 2010 South African short documentary film.

== Synopsis ==
In 1969, Imam Abdullah Haron was incarcerated and killed in detention in Cape Town, South Africa. A much loved community leader, he was active within an inactive community in raising awareness of the plight of his compatriots living under apartheid. During the 60s, Imam Haron became more active and began travelling abroad to raise funds for impoverished families back home. Mixing animation, documentary and stock footage, this short film looks at the last few years of the Imams life and death. It is told by his grandson, the filmmaker, through the eyes of a child.

== Awards ==
* SAFTA 2011

== References ==
 

 
 
 
 
 
 
 
 


 
 