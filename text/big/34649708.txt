A Brother with Perfect Timing
 
 
{{Infobox film
| name           = A Brother with perfect timing
| image          = A_brother_With_Perfect_Timing.jpg 
| alt            = 
| caption        = 
| director       = Chris Austin (film director)
| producer       = 
| writer         = 
| starring       = 
| music          = 
| distributor    =  
| released       = 1987 
| runtime        = 90 minutes
| country        = South Africa
| language       = English
| budget         = 
| gross          = 
}} freedom with apartheid in South Africa.  

==Synopsis==
The documentary includes a live performance by Ibrahim and discussions about two of his compositions, "Anthem for a New Nation" and "Mannenberg".

==Reception==
The New York Times positively reviewed the film, calling it "a well-rounded view of a musician for whom exile means both pain and inspiration".  The film was reviewed in the book Jazz on Film where author Scott Yanow praised the movies "blend of interviews and music".  Coda (magazine)|Coda magazine also reviewed the documentary, commenting that Ibrahim "comes across as an admirable and dignified religious man". 

==References==
 

 
 
 
 

 