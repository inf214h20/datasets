Bart Got a Room
{{Infobox film
| name            = Bart Got a Room
| image           = Bart got a room.jpg
| caption         = Promotional film poster
| director        = Brian Hecker
| producer        = Plum Pictures
| writer          = Brian Hecker
| starring        = Steven Kaplan William H. Macy Cheryl Hines Alia Shawkat
| music           = Jamie Lawrence
| cinematography  = Hallvard Bræin
| editing         = Danny Rafic
| distributor     = Anchor Bay Entertainment
| released        =  
| runtime         = 80 minutes
| country         = United States
| language        = English
}} Steven Kaplan, Alia Shawkat, William H. Macy, and Cheryl Hines. Also appearing in the film are Ashley Benson, Brandon Hardesty, Kate Micucci, Jennifer Tilly, Dinah Manoff and Chad Jamian Williams as Bart. The film premiered at the Tribeca Film Festival on April 25, 2008. It had a limited US release in select theaters on April 3, 2009 and was released on DVD on July 28, 2009.

==Plot== Steven Kaplan) and his unsuccessful attempts to secure a prom date while his divorced father and mother (played by William H. Macy and Cheryl Hines respectively) are on their own unsuccessful quests to find love. The films name comes from the fact that the most unpopular kid in school, Bart Beeber (Chad Jamian Williams), not only secured a date for the prom, but got a hotel room after as well. This is a source of great anxiety for both Danny and his family. 

==Cast==
* Ashley Benson as Alice Steven Kaplan as Danny
* William H. Macy as Ernie
* Cheryl Hines as Beth Stein
* Alia Shawkat as Camille
* Brandon Hardesty as Craig
* Kate Micucci as Abby
* Jon Polito as Bob
* Jennifer Tilly as Melinda
* Katie McClellan as Gertie
* Dinah Manoff as Mrs Goodson
* Michael Mantell as Dr. Goodson
* Chad Jamian Williams as Bart
* Angelina Assereto as Marcie

==Production== State of Grace.

==Reception==
Bart Got a Room won "Best of Fest" awards at The Fort Lauderdale International Film Festival, The Asheville Film Festival, and The Chicago Gen Art Film Festival. The film currently has a 69% approval rating on Rotten Tomatoes.

==Soundtrack== Sing Sing Hollywood Beach Shell (theater)|Bandshell.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 