The Blue Squadron (1934 film)
{{Infobox film
| name           = The Blue Squadron
| image          =
| caption        =  George King
| producer       = Irving Asher Brock Williams John Stuart
| music          =
| cinematography = Basil Emmott
| editing        =  Warner Brothers-First First National Productions Società Anonima Pittaluga
| released       =  
| runtime        = 96 minutes Italy
| language       = English
}} George King John Stuart. Warner Brothers) and Kingdom of Italy (1861–1946)|Italys Pittaluga studios.  Although made under quota quickie conditions, the film seems to have enjoyed a rather more generous budget than was the norm with such productions and was able to include convincing location shots in the snowy Italian mountains.
 Italian Royal Air Force (Knight and Stuart) competing for the hand of the attractive Elena (Greta Hansen) and finding their comradeship under strain as a result.  Both try to impress her with daredevil aviation stunts which become increasingly reckless as they try to outdo each other in bravery.  Finally, Knight goes too far and crashes his plane on a mountainside.  Putting rivalry aside, Stuart courageously risks his own life to save his injured colleague and both realise that their friendship is more important than silly squabbling over a woman.  They decide to let Elena make her own choice.

The Blue Squadron is classed by the British Film Institute as a lost film. 

==Cast==
* Esmond Knight as Captain Carlo Banti John Stuart as Colonel Mario Spada
* Greta Hansen as Elena
* Cecil Parker as Bianchi
* Ralph Reader as Verdi

==References==
 

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 
 
 

 