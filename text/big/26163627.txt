True Women for Sale
 
 
{{Infobox film
| name           = True Women For Sale
| film name      =  
| image          = True Women For Sale poster.jpg
| caption        = Official poster of True Women For Sale
| director       = Herman Yau
| producer       = Ng Kin Hung
| writer         = Yeeshan Yang Herman Yau
| starring       = Prudence Liew Anthony Wong Chau Sang Race Wong Sammy Leung
| music          = Brother Hung Joe Chan
| editing        = Azreal Chung
| studio         = China Star Entertainment Group
| distributor    = Mei Ah Entertainment
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
 
True Women For Sale (性工作者2: 我不賣身．我賣子宮) is a 2008 drama film directed by Herman Yau and starring Prudence Liew and Anthony Wong Chau Sang.

==Plot==
The story revolves around two women in the grassroots of Hong Kong society, struggling to survive and haunted by demons in their respective pasts.  The literal translation of the Chinese film title is "I do not sell my body, I sell my uterus".  In fact it deals with one woman who indeed sells her body but strives to improve herself and one woman who views herself to be of higher status than the street walkers, but in reality is also selling herself by marrying someone and becoming pregnant just to become a Hong Kong citizen.  The two women live in the same apartment building and share some interactions on screen; however, their stories do not necessarily intermix.  The film takes place in the Sham Shui Po area of Kowloon, Hong Kong circa 2000.

Lai Chung-Chung (Prudence Liew) is a cocaine-addicted yet good-natured street walker who is fast approaching a "destiny call" made by a fortune teller advising her that in the year 2000, she would finally pay off all her debts from her previous life and will turn over a new leaf.  To prepare herself for the life-changing event, she works hard to save up for a dentistry process to clean her badly decaying teeth and gums from years of cocaine use.  After witnessing Lai heroically save a child from being hit by a van, freelance photographer Chi (Sammy Leung) thought that Lai would make a good human interest story for a magazine and starts to photograph Lai while having his girlfriend, journalist Elaine (Toby Leung) interview her.  During Elaines interview with Lai, it is revealed that Lai has had a troubled past with her mother and younger sister.

The other subject is Wong Lin-Fa ( .  Upon hearing Kin died in a work accident, Wong travels to Hong Kong, pregnant with Kins child in hopes of collecting an abundance of  .  She then moves into Kins apartment in a building that is full of prostitutes, including Lai.  Insurance broker Lau Fu-Yi (Anthony Wong Chau-Sang) notifies Wong that Kin had bought life insurance with her as the sole beneficiary. Lau later begins an unlikely friendship with Wong, helping her with her quest to give birth and gain residency in Hong Kong, all the while trying to sell her insurance.  Lau has a way of classifying people in his head by calculating their potential insurance coverage.  He does this for Lai and Chi and quickly dismisses them as having a potential of $0 while evaluating Wong at US$2 million. 

==Cast==
*Prudence Liew as Lai Chung-Chung
*Anthony Wong Chau-sang as Lau Fu-Yi
*Race Wong as Wong Lin-Fa
*Sammy Leung as Chi
*Toby Leung as Elaine
*Sherming Yiu as Yau Guen, Lais confidante and fellow street walker
*Apple Chow as Pink Pink, Lais friend and fellow street walker
*Jessie Meng as Dr. Lee, a Mainland Chinese doctor Lau is interested in
*Monie Tung as Sister Kot, a nun who empathises with the street walkers
*Fung Hak on as Keung, Lais regular client
*Chi-Kin Kwok as Leung, Wongs ex-boyfriend
*Jackie Ma as Lai Chung, Lais ex-husband
*Yuka Yu as Kwai, Keungs wife from Mainland China
*Colour as Ying Ying, Lai Chungs current wife

===Cameos=== Susan Shaw as Chungs mother
*Yumiko Cheng as Ob-Gyn Nurse
*Chapman To as Police Officer
*Louis Cheung as Social Worker
*Terence Siufay as Jacky, Laus co-worker

==Awards & nominations==
The film was selected as the opening film in the 2008 Hong Kong Asian Film Festival and was nominated for several awards.

{| class="wikitable"
|-
! Year
! Award
! Category
! Nominee
! Result
! Reference
|-
| 2008
| Golden Horse Awards
| Best Actress
| Prudence Liew
|  
| 
|- 2009
| 28th Hong Hong Kong Film Awards
| Best Actress
| Prudence Liew
|  
| rowspan="2" | 
|-
| Best Supporting Actress
| Race Wong
|  
|- Ming Pao Weekly Magazine Awards
| Most Outstanding Actress
| Prudence Liew
|  
| rowspan="2" | 
|}

==References==
 

==External links==
* 
* 
*  

 
 

 
 
 
 
 
 
 
 