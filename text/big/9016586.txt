The Living Sea
 
{{Infobox Film
| name           = The Living Sea
| image          = Thelivingseaimax.jpg
| caption        = 
| director       = Greg MacGillivray 
| producer       =  Tim Cahill
| narrator       = Meryl Streep 
| starring       =   Sting
| cinematography = Howard Hall
| editing        = Stephen Judson IMAX Corporation
| released       = 1995 
| runtime        = 40 min.  USA
| English 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 American documentary underwater imagery directed by filmmaker Greg MacGillivray. 

==Overview==
The film is a survey of the worlds oceans, emphasizing that it is a single interconnected ocean and the dependence of all life on the planet. The film shows researchers tracking whales, a Coast Guard rough-weather rescue squad, a deep-ocean research team, and the Palau Islands, which contain an unusual jellyfish habitat.
 Best Documentary Short    but lost to One Survivor Remembers.

==Soundtrack==
*  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 