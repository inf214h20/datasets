Anwar (2007 film)
{{Infobox film
| name                 = Anwar
| image                = Anwar 2007 film poster.jpg
| caption              = Theatrical release poster
| director             = Manish Jha
| producer             = Rajesh Singh
| story                = Manish Jha
| starring             = Siddharth Koirala Manisha Koirala Nauheed Cyrusi
| music                = Mithoon Pankaj Awasthi
| cinematography       = Kartik Vijay
| editing              = Amitabh Shukla
| distributor          = Dayal Creations Pvt. Ltd.
| released             =  
| runtime              =
| country              = India
| language             = Hindi
| budget               =
| gross                =
}}
Anwar ( ) is a 2007 Indian film  written and directed by Manish Jha, who is famous for his work in Matrubhoomi. The film stars Siddharth Koirala, Manisha Koirala, Rajpal Yadav and Nauheed Cyrusi.

==Plot==
Anwar, a Muslim, lives a middle-class lifestyle in Lucknow, India, with his mother, brother, and sister-in-law, Suraiya. Anwar is researching on ancient Hindu mandirs. The family rents out a room to a poor widow and her attractive daughter, Mehru, whom Anwar falls in love with.
 mandirs and notes on Lord Krishna, Devi Meera, and Mehru. This bag ends up with the police, who now believe that Anwar is a terrorist who is planning to detonate bombs in sacred Hindu temples. 
 American way of life, Anwar must now examine his options.

==Themes==
The film set in Lucknow, is about stereotyping of Muslims in the post-9/11 era. The film was inspired by the directors experience in New York two days after the 9/11 attacks when he was detained by the police and interrogated for five hours, who presumed him to be a Muslim, since he was unshaven and had long hair.   

==Production==
The film was shot in Kakori, Bakshi Ka Talab and Lucknow during April 2006. 

==Cast==
* Manisha Koirala as Anita
* Siddharth Koirala as Anwar
* Rajpal Yadav as Gopinath
* Vijay Raaz as Master Pasha Yashpal Sharma as S.P. Tiwari
* Hiten Tejwani as Udit
* Nauheed Cyrusi as Mehru
* Sudhir Pandey as Minister
* Pankaj Jha
* Lalit Tiwari
* Rasika Dugal
* Sanjay Mishra
* Prithvi Zutshi
* Sharat Sonu as Gardener

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = yes
| title1 = Maula Mere Maula
| extra1 = Roop Kumar Rathod
| music1 = Mithoon
| length1 = 6:04
| title2 = Javeda Zindagi
| extra2 = Kshitij Tarey, Shilpa Rao
| music2 = Mithoon
| length2 = 8:22
| title3 = Bangla Khula Khula
| extra3 = Megha Sriram
| music3 = Pankaj Awasthi
| length3 = 5:18
| title4 = Dilbar Mera
| extra4 = Pankaj Awasthi
| music4 = Pankaj Awasthi
| length4 = 4:53
| title5 = Jo Maine Aas Lagayi
| extra5 = Pankaj Awasthi
| music5 = Pankaj Awasthi
| length5 = 2:05
| title6 = Anwars Confession - Into The Black
| music6 = Pankaj Awasthi
| length6 = 3:17
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 