Prawo i pięść
Prawo Polish 1964 cult status history of Polish cinema. 
 Polish resistance and survivor of a German concentration camp. He is sent with a small group of men as government representatives to a small fictional town of Siwowo/Graustadt in the so-called Recovered Territories, the new Western Territories of Poland. Their task is to secure the property left there by the retreating Germans. The small town is mostly abandoned and the only remaining inhabitants are four women and a drunken waiter in the hotel. Upon reaching the town, Kenig discovers that the other members of the government delegation are not who they claim to be and that their only task is to loot as much of the property as they can for themselves. Kenig decides to fight against the gang of bandits alone. There is a shootout on the roofs and in the streets of the deserted town. 

The motive of a lone hero fighting against a group of villains resulted in the film being often described as a "Polish western movie|western". 

The film is also well-remembered because of the original score written by Krzysztof Komeda, and especially the highly-popular title song Nim wstanie dzień (Before the day breaks) with music by Komeda, lyrics written by Agnieszka Osiecka and performed by Edmund Fetting.

 
 
 
 
 
 
 
 


 