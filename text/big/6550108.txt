The Vampire Bat
{{Infobox film
| name           = The Vampire Bat
| image          = Vampirebat.jpg
| caption        = Theatrical film poster for The Vampire Bat
| director       = Frank R. Strayer
| producer       = Phil Goldstone Larry Darmour
| writer         = Edward T. Lowe Jr.
| starring       = Lionel Atwill Fay Wray Melvyn Douglas Dwight Frye Maude Eburne
| music          = Charles Dunworth
| cinematography = Ira H. Morgan
| editing        = Otis Garrett
| distributor    = Majestic Pictures
| released       =  
| runtime        = 63 min
| country        = United States
| language       = English language|English}}

The Vampire Bat (1933 in film|1933) is an American horror film starring Lionel Atwill, Fay Wray, Melvyn Douglas, and Dwight Frye.

==Plot outline==
When the villagers of Kleinschloss start dying of blood loss, the town fathers suspect a resurgence of vampirism, however police inspector Karl Breettschneider remains skeptical. Scientist Dr. Otto von Niemann, who cares for the victims, visits a patient who was attacked by a bat, Martha Mueller. Martha is visited by a mentally challenged man named Herman, who claims he likes bats because they are soft, like cats and nice. On the Doctors journey home, he meets Kringen, one of the townsfolk, who claims to have been attacked by the vampire in the form of a bat, but withheld his story from the town in order to not spread fear. Dr. von Niemann encourages Kringen to tell the townsfolk of his story. Kringen becomes suspicious that Herman Glieb may be the vampire due to his obsession with bats. Herman lives with bats and collects them off the street. 

Dr. von Niemann returns to his home, which also houses Breettschneiders love Ruth Bertin, Ruths hypochondriac aunt Gussie Schnappmann, and servants Emil Borst, and Gorgiana. Fear of the vampire and suspicion of Glieb quickly spreads around the town, and people start fearing him. Ms Mueller is killed that night. The analyses of Dr. von Niemann and another doctor, Dr. Haupt, conclude that the death is the same as all of the previous deaths; blood loss, with two punctures in the neck caused by needle sharp teeth. Gleib enters the examination and upon seeing the dead body, runs away screaming.

Next morning, Glieb enters Dr. von Niemanns garden, where Dr. von Niemann, Breettschneider, and Bertin are discussing vampires inside the house. The town fathers enter the house and announce that Kringen is dead and Gleib missing. An angry mob hunts down Gleib and chases him through the countryside and into a cave, where he falls to his death.

That night, Dr. von Niemann is seen telepathically controlling Emil Borst, as he picks up sleeping Gorgiana and takes her down to Dr. von Niemanns laboratory, where a strange organism is seen. They then drain her blood from her neck.

Schnappmann then discovers Gorgianas body in her bed. Dr. von Niemann and Breettschneider investigate and find Ms Muellers crucifix, which Glieb handled the night Dr. von Niemann visited her. Breettschneider is becoming more convinced of the presence of vampires in the village as no other plausible explanations for the deaths can be found. As Glieb was seen in the garden that morning, the two conclude he is guilty.

Upon hearing of Gliebs death, however, Breettschneiders conviction is erased. Dr. von Niemann tells Breettschneider to go home and take sleeping pills, but gives him poison instead, intent on draining his blood. Bertin discovers Dr. von Niemann telepathically controlling Borst, who is at Breettschneiders house. It is revealed that Dr. von Niemann has created life, and is using the blood to fuel his organism. He ties Bertin up in his lab. Borst supposedly enters with Breettschneiders body on a trolley. Dr. von Niemann walks over to Borst, who is revealed to be Breettschneider (who didnt take the pills) in costume, with the real Borst on the trolley. Breettschneider pulls a gun on Dr. von Niemann, and walks over to untie Bertin. Dr. von Niemann then wrestles Breettschneider, who drops the gun. As the two fight, Borst picks up the gun and shoots Dr. von Niemann.

==Production== Doctor X the previous year and had already wrapped up work on Mystery of the Wax Museum for Warner Bros. This was quite a large-scale release and would have a lengthy post-production process. Seeing a chance to exploit all the advance press, poverty row studio Majestic Pictures Inc. contracted Wray and Atwill for their own "quickie" horror film, rushing The Vampire Bat into production and releasing it in January 1933.
 Universal Pictures Frankenstein (1931) and the interior sets from his film The Old Dark House (1932), plus some location shooting at Bronson Caves. Completing the illusion that this was a film from a much bigger studio, Majestic hired actor Dwight Frye to populate scenes with Wray and Atwill. A stock musical theme by Charles Dunworth, "Stealthy Footsteps", was used to accompany the opening credits. 

The Vampire Bat ruse worked well for Majestic, which was able to rush the quickie film into theaters less than a month before Warners release of Mystery of the Wax Museum.
According to The Film Daily of January 10, 1933, the films running time was 63 minutes, like most extant prints.

==Cast==
* Lionel Atwill as Dr. Otto von Niemann
* Fay Wray as Ruth Bertin
* Melvyn Douglas as Karl Breettschneider
* Maude Eburne as Gussie Schnappmann
* George E. Stone as Kringen
* Dwight Frye as Herman Gleib
* Robert Frazer as Emil Borst
* Rita Carlisle as Martha Mueller
* Lionel Belmore as Bürgermeister Gustave Schoen
* William V. Mong as Sauer
* Stella Adams as Georgiana
* Paul Weigel as Dr. Holdstadt
* Harrison Greene as Weingarten William Humphrey as Dr. Haupt
* Ferm Emmett as Gertrude
* Carl Stockdale as Schmidt   
* Paul Panzer as Townsman

==See also==
* List of films in the public domain
* Vampire film

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 