Reptiloid (film)
 
{{Infobox film
| name           = Reptiloid
| image          = 
| caption        = scene from the film
| director       =  
| producer       = Kinoklub Zagreb
| writer         = Marin Mandir
| starring       =      
| music          = 
| cinematography = Josip Stipetic
| editing        = Maja Kadoic Raos
| distributor    = 
| released       =  
| runtime        = 29 minutes
| country        = Croatia
| language       = Croatian
| budget         =  imdb_id =3039814
}}
Reptiloid ( ) is a 2013 Croatian  . 

==Plot==
The story revolves around police officers Petar and Darko, who are sent to investigate a murder deep in a forest on a hill. As they park their car, Darko goes further inside the forest on foot, in search for clues, while Petar stays at the car. However, as the support crew fails to show up, and Darko disappeared with the car key, Petar is forced to spend the night in the forest. The next morning, Petar goes to search for Darko only to find him killed by a giant, Komodo dragon like lizard. The lizard then proceeds to hunt Petar across the forest, who tries to reach the city by foot.

==Cast==
*   - Petar
*   - Darko
*   - Barmen
*   - Poker-ace

==See also==
* List of films featuring dinosaurs
* Croatian science fiction

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 