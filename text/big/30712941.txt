We Were Here (film)
 
{{Infobox film
| name           = We Were Here
| image          = File:We_Were_Here_promotional_image.jpg
| alt            = 
| caption        = 
| directors       = David Weissman, Bill Weber
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = Holcombe Waller
| cinematography = Marsha Kahm
| editing        = Bill Weber
| studio         =  
| distributor    =  
| released       = September 2011 (USA)
| runtime        = 90 mins
| country        = USA
| language       = English
| budget         = 
| gross          =  
}}
 
We Were Here is a 2011 documentary film about the AIDS crisis in San Francisco.  The film, produced and directed by David Weissman with editor/ co-director and Bill Weber, premiered at the Sundance Film Festival in January 2011, The Berlin Film Festival in February 2011 and had its theatrical premiere at the Castro Theater in San Francisco on February 25, 2011.

We Were Here is the first documentary to take a deep and reflective look back at the arrival and impact of AIDS in San Francisco . It explores how the city’s inhabitants were affected by, and how they responded to, that calamitous epidemic. 

==Plot==
We Were Here documents the coming of what was called the “Gay Plague” in the early 1980s. It illuminates the profound personal and community issues raised by the AIDS epidemic as well as the broad political and social upheavals it unleashed.  

Early in the epidemic, San Francisco’s compassionate, multifaceted, and creative response to AIDS became known as “The San Francisco Model”. The city’s activist and progressive infrastructure that evolved out of the 1960s, combined with San Francisco’s highly politicized gay community centered around the Castro Street neighborhood, helped overcome the obstacles of a nation both homophobic and lacking in universal healthcare. In its suffering, San Francisco mirrors the experience of so many American cities during those years.  In its response, The San Francisco Model remains a standard to aspire to in seeking a healthier, more just, more humane society.

We Were Here focuses on 5 individuals – all of whom lived in San Francisco prior to the epidemic. Their lives changed in unimaginable ways when their beloved city changed from a hotbed of sexual freedom and social experimentation into the epicenter of a terrible sexually transmitted plague. From their different vantage points as caregivers, activists, researchers, as friends and lovers of the afflicted, and as people with AIDS themselves, the interviewees share stories which are not only intensely personal, but which also illuminate the much larger themes of that era: the political and sexual complexities, the terrible emotional toll, the role of women – particularly lesbians – in caring for and fighting for their gay brothers.

Archival imagery conveys an unusually personal and elegiac sense of San Francisco in the pre-AIDS years, and a window into the compassionate and courageous community response to the suffering and loss that followed.  And it also conveys in a very visceral sense the horrors of the disease itself.

==Interviewed People==
The film focuses on 5 different interviews of people that had a protagonist role during the epidemic. These people are, in order of appearance:

* Ed Wolf, http://www.edwolf.net
* Paul Boneberg
* Daniel Goldstein, http://www.danielgoldsteinstudio.com
* Guy Clark
* Eileen Glutzer

==Reception==
It holds a 100% certified fresh rating on review aggregator Rotten Tomatoes and a 94 rating on Metacritic, the highest among films of 2011.

==Awards==
*2011: Best Documentary Feature Film at the Mumbai Queer Film Festival

==See also== And The Band Played On

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 