The Nightmare (2015 film)
{{Infobox film
| name           = The Nightmare
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Rodney Ascher
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = Jonathan Snipes
| cinematography = Bridger Nielson
| editing        =  production companies = Zipper Bros Films Campfire distributors = Gravitas Ventures (USA) Altitude Film Distribution (UK)
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
The Nightmare is a 2015 American documentary film that was directed by Rodney Ascher.    The film had its world premiere on 26 January 2015 at the Sundance Film Festival and focuses on the topic of sleep paralysis. Ascher chose to focus on sleep paralysis as it had happened to him in the past.  The films crew initially began approaching participants via "message groups, YouTube videos, and a half dozen books that had been written", but found that participants began approaching them after the documentarys premise was announced.    

==Synopsis==
The documentary focuses on eight people suffering from sleep paralysis, a phenomenon where people find themselves temporarily unable to move, speak, or react to anything while they are falling asleep or awakening. Occasionally this paralysis will be accompanied by physical experiences or hallucinations that have the potential to terrify the individual. In the film Ascher interviews each participant and then tries to re-create their experiences on film with professional actors. 

==Reception==
Critical reception for The Nightmare has been mostly positive and the film holds a rating of 62% on Rotten Tomatoes, based on 13 reviews.   The film received praise from media outlets such as Indiewire, Screen Daily, and Variety (magazine)|Variety,   and Variety wrote that "Mixing talking heads, surreal bedtime re-creations and shamelessly assaultive scare tactics, Ascher’s playful, visually inventive sophomore feature isn’t at the same level as Room 237 but that "it shares with its predecessor a warped affection for eccentric storytellers and a desire to give vivid cinematic form to their darkest imaginings, if that is indeed what they are."  Shock Till You Drop remarked on how well the film was received at Sundance where one viewer "cried in gratitude of the film", and went on to state that although Ascher did not consult any professional scientists or doctors, the documentary was still effective in inciting terror. 

==References==
 

==External links==
*  

 
 
 
 