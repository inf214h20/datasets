Everybody Loves Sunshine
{{Infobox Film |
  name= Everybody Loves Sunshine|
  image=|
  writer=Andrew Goth|
  producer=Joanne Reay|
  starring=Rachel Shelley, David Bowie, Goldie|
  director=Andrew Goth |
  music=Nicky Matthew|
  distributor=|
  released=1999|
  runtime=97 min |
  language=English |
  movie_series_label=|
  movie_series=|
  budget          = |
  awards         = |
}} British independent independent film written and directed by Andrew Goth, and starring Rachel Shelley, David Bowie and Goldie.

==Plot== Pepperhill Estate Triad gangs and street gangs. Gang leaders Ray (Andrew Goth) and Terry (Goldie), who are cousins and lifelong friends, always trusting and relying on each other, have been in prison.  Ray doesnt want to be a gangster anymore, having also fallen for Clare (Rachel Shelley). But Terry, driven by an obsession beyond friendship, is determined to make sure that Ray never leaves the gang. During their time in prison, the Triads have grown stronger and more daring, eventually killing a member of Terry and Rays gang. Revenge is called for and the gang turns to them for direction. Bernie (David Bowie) is the aging gangster who struggles to keep the peace. 

==Critical reception==
Film Threat magazine, in its review, described the film as a "very British tale of vengeance and mayhem worth sitting through – though just barely," crediting the films "arresting visual presentation." The review notes that Bowies an "odd presence throughout" and "he often seems to be wandering in from a movie on another channel." 

==See also==
* Gangster No. 1

==References==
 

==External links==
*  

 
 
 
 
 


 
 