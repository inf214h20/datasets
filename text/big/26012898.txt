Cheppu
{{Infobox film
| name           = Cheppu
| image          = Cheppu.jpg
| caption        = Poster designed by Gayathri Ashokan
| director       = Priyadarshan
| writer         = Priyadarshan 
| screenplay = Dennis Joseph                        Lizy  Ganesh Kumar
| producer       = Thiruppathi Chettiyar
| music          = Reghu Kumar (Songs)  K. J. Joy (Background score)
| cinematography = S. Kumar ISC
| editing        = N. Gopalakrishnan
| distributor    = Evershine Release
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| gross          =
}}

Cheppu ( ) is a 1987 Malayalam language film made in Cinema of India|India, directed by Priyadarshan and starring Mohanlal.    The film is based on the 1982 Canadian film Class of 1984.

==Plot==
Ramachandran (Mohanlal) got a job as an English lecturer in T. K. P. Memorial College, only to find that the college is full of drugs and politics, led by young Ranjith Mathews (K. B. Ganesh Kumar|Ganesh), son of an M.P. (Prathapachandran). Ramachandran has helped the students, including a geek who was beaten by Ranjiths gang.

Ramachandran fights against Ranjith until he is murdered by his gang in the end. A student, whom Ramachandran had saved, later kills Ranjith for his honour.

==Cast==
*Mohanlal  as   Ramachandran 
*Cochin Haneefa  as   Inspector 
*K. B. Ganesh Kumar  as   Ranjith  Lizy  as   Mini 
*Prathapachandran  as   Mathews 
*Sankaradi as  Karunakaran Nair
*M. G. Soman  as   Principal 
*Sulakshana (actress) | Sulakshana  as   Lakshmi 
*Nedumudi Venu  as   Lecturer

==Soundtrack==
The music was composed by Raghu Kumar and lyrics was written by Poovachal Khader.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Free And Young || Brenda Lee || Poovachal Khader ||
|-
| 2 || Maarivillin Chirakode || K. J. Yesudas, Sujatha Mohan || Poovachal Khader ||
|}

==References==
 

==External links==
*  

 

 
 
 
 
 