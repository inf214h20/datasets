Wolf (1994 film)
{{Infobox film
| name           = Wolf
| image          = Wolf_movie_poster.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Mike Nichols
| producer       = Douglas Wick Neal A. Machlis
| screenplay     = Jim Harrison Wesley Strick Elaine May (uncredited)
| based on       =  
| starring       = Jack Nicholson Michelle Pfeiffer James Spader Kate Nelligan Richard Jenkins Christopher Plummer
| music          = Ennio Morricone
| cinematography = Giuseppe Rotunno
| editing        = Sam OSteen
| distributor    = Columbia Pictures
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $70 million
| gross          = $131,002,597    
}} romantic horror film directed by Mike Nichols and starring Jack Nicholson, Michelle Pfeiffer, James Spader, Kate Nelligan, Richard Jenkins, Christopher Plummer, Eileen Atkins, David Hyde Pierce and Om Puri. It was written by Jim Harrison and Wesley Strick, and an uncredited Elaine May. Music was composed by Ennio Morricone and cinematography by Giuseppe Rotunno. It is based on Harrisons 1971 debut novel Wolf: A False Memoir.

==Plot== wolf while driving home through Vermont after it was seemingly hit by his car. Soon after, he is demoted from editor-in-chief of a publishing house during a takeover by ruthless tycoon Raymond Alden (Christopher Plummer), who replaces him with Wills ambitious protégé Stewart Swinton (James Spader). Will begins experiencing physiological changes ranging from increased appetites and libido to hair regrowth and sharper-than-human sensory perceptions. Catching an unfamiliar scent on the clothing of his wife Charlotte (Kate Nelligan), Will rushes over to Stewarts house, bites Stewart during a brief physical altercation, and rushes upstairs to the bedroom where he finds evidence of Charlottes infidelity. Will leaves his wife, takes up residence at the Mayflower Hotel, and as the moon ripens, takes on increasingly bestial aggressive characteristics.
 estate where he partially transforms and hunts down a deer by moonlight. In the morning, Will finds himself on the bank of a stream, with blood all over his face and hands. Fearing the notice of this, Will hurriedly departs in his Volvo.

He visits Indian paranormal scholar Vijav Alezais (Om Puri), who gives him a silver amulet believed to be proof against lycanthropy. Alezais, who has recently been diagnosed with a terminal illness, asks Will to bite him preferring "damnation" to death. Will refuses, but keeps the amulet in the hopes that it will cure or forestall his condition. Returning to his hotel, Will calls Laura to explain his nocturnal disappearance and asks to see her again. Later that evening, Will as a wolfman breaks into the zoo, escaping two police officers and taking their handcuffs. Muggers attempt to steal his valuables, but he springs upon his assailants wounding them savagely. He wakes up in his hotel with no memory of the nights events.

Threatening to lead a mass writers exodus, Will successfully wrests his editor-in-chief position back from Alden, and negotiates for additional supervisory powers. Will surprises Stewart in the bathroom, notifies him of these office developments, fires him, and then urinates on Stewarts suede shoes so as to "mark his territory." While washing his hands, Will finds two of the muggers severed fingers in his jacket pocket, and realizes the beast is gaining control. Horrified by this realization, Will returns to his hotel, where Charlotte stops him in the lobby to beg his forgiveness. Will rebukes his now-estranged wife and tells her to stay away. Laura surreptitiously witnesses this argument. After Wills recent outing, Laura makes her way upstairs to Wills room where the two begin a sexual relationship.

Detective Bridger (Richard Jenkins) visits Wills hotel room the next day to inform him of Charlottes savage murder. Separately, Will and Laura both begin to harbor doubts concerning Wills possible involvement in the crime. Cutaway scenes between Stewart, Alden, and the police gradually inform the viewer that the former protégé is now trying to frame Will in an attempt to get his job back.

Disquieted by Wills superhuman hearing, dried mud on his shoes, and a phone call from Detective Bridger revealing that Charlottes blood and tissue samples were contaminated by canine DNA, Laura comes to believe that Will is a murderous monster. Will voluntarily submits to being locked in the Alden horse-barn while Laura goes to the police station. There, she encounters an uncharacteristically feral Stewart, whose amber eyes and increasingly-bestial manner suggest that, having previously been bitten by Will, he has also begun the werewolf transformation. Laura fends off Stewarts sexual advances and hurries back to the Alden estate, now convinced of Wills innocence and intent on leaving the country with him.

Stewart follows Laura back to her fathers estate, where he surprises and kills two security guards. A brief struggle in the barn ensues. Stewart tries to rape Laura, but Will intervenes, tearing off Alezais amulet, escaping his stable cell, and giving into the full-moon transformation. Both werewolves are brutally injured in the fight. Will eventually gains the upper hand over his opponent. Stewart tries to back stab him with a set of hedge-clippers, but Laura picks up a dead guards revolver and shoots him dead as Stewarts body regresses back to human form. Will, now quite lupine, shares a wordless moment with Laura before running into the forest.

Later that same evening, Laura is debriefed by police on the scene and feigns horrified surprise. Both the Aldens reconcile. Detective Bridger opines that Will was "too tame" to make a good match for Laura and is unnerved when Laura invites him in for a vodka tonic and notes that he has already had one this evening. As she walks away, Detective Bridger asks how Laura knew. From her position, Laura says "I can smell it a mile away" as she leaves for her room with Detective Bridger and Alden laughing about Lauras comment.

The final scene is a close-up of Lauras face who is now amber-eyed. It then transitions into Will howling at the moon as he completes his final metamorphosis into a full werewolf and then back to Laura where the screen fades out to her glowing no-longer-human gaze.

==Cast==
* Jack Nicholson as Will Randall
* Michelle Pfeiffer as Laura Alden
* James Spader as Stewart Swinton
* Kate Nelligan as Charlotte Skylar Randall
* Richard Jenkins as Detective Carl Bridger
* Christopher Plummer as Raymond Alden
* Eileen Atkins as Mary
* David Hyde Pierce as Roy MacAllister
* Om Puri as Dr. Vijav Alezais
* Ron Rifkin as Doctor Ralph
* Prunella Scales as Maude Waggins
* Brian Markinson as Detective Wade
* Peter Gerety as George
* Bradford English as Keyes
* Stewart J. Zully as Gary
* Thomas F. Duffy as Tom
* David Schwimmer as Cop
* Allison Janney as Party Guest 2

==Production==
Mia Farrow was an early contender for the role of Charlotte Randall, but was apparently considered too controversial a choice by the film company due to the then-current Woody Allen and Soon-Yi Previn affair.   

Sharon Stone turned down the role of Laura Alden, eventually played by Michelle Pfeiffer. 

The films release was delayed for six to eight months, in order to reshoot the poorly received ending. 

==Reaction==

===Box office===
Wolf grossed $65 million domestically and $66 million internationally, for a total of $131 million worldwide. 

===Critical response===
Wolf holds a score of 61% on Rotten Tomatoes based on 49 reviews. 

 s own killer instincts as an urbane social satirist are ideally suited to this milieu... Only later, when the wolf motif is allowed to become literal, does Wolf sink its paws into deep quicksand... Jack Nicholson|Mr. Nicholson, who actually totes a briefcase for this role and gives one of his subtlest performances in recent years, is well suited to the conversational savagery that marks Wolf at its best... Unlike Francis Ford Coppola, who revealed a surprising enthusiasm for horrific vampire tricks in Bram Stokers Dracula, Mr. Nichols shows no great gusto for the supernatural... there are admirable performances from James Spader|Mr. Spader, still turning the business of being despicable into a fine art, and Kate Nelligan, as Wills deceptively brisk and efficient wife... Michelle Pfeiffer|Ms. Pfeiffers role is underwritten, but her performance is expert enough to make even diffidence compelling. Christopher Plummer|Mr. Plummer, as he should, radiates a self-satisfaction so great it actually seems carnivorous."
 Rick Baker is convincing but wisely limited, and the movie looks great, but that doesnt cost a lot of money. What emerges is an effective attempt to place a werewolf story in an incongruous setting, with the closely observed details of that setting used to make the story seem more believable."
 Pfeiffer doesnt have nearly as much to work with, and so, ultimately, she lends more of her beauty than she does her talent. But with beauty like hers it would seem churlish to complain. Even so, she does bring a ring of true emotion to this bad girls jaded snarl. Chemistry between the two stars is essential here, and Pfeiffer makes us believe in this improbable love affair. Its Pfeiffers combination of compassion and terror that carries the last section of the film and gives it class."
 Spader - Nicholson is amazing, finding humor and poignancy in a role that could have slid into caricature. His scenes with Michelle Pfeiffer|Pfeiffer, who gives a luminous performance, have a welcome edge, aided by some uncredited scripting from Nichols former comedy partner Elaine May... a rapturous romantic thriller with a darkly comic subtext about what kills human values."
 Rick Baker Spader is creepily effective as the ladder-climbing opportunist. He ought to be the poster boy for that T-shirt slogan "Die Yuppie Scum." As for Jack, nobody does it better... Nichols has allowed Wolf to evolve from a well-mounted, supernatural drama to goofy camp."
 Spader is Nelligan has little to do as the unfaithful wife... Eileen Atkins and David Hyde Pierce as Wills loyal publishing underlings, are dead perfect."
 Time Out wrote:  "Quite frankly, its hard to fathom why exactly anyone would have wanted to make this slick, glossy, but utterly redundant werewolf movie... Overall, this is needlessly polished nonsense: not awful; just toothless, gutless and bloodless."

===Awards and nominations=== Best Horror Best Actor Best Actress Best Supporting Best Make-up Rick Baker).
 Grammy Award for Best Instrumental Composition Written for a Motion Picture or for Television.

{|class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! style="background:#B0C4DE;" | Awarding Body
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Nominee
! style="background:#B0C4DE;" | Result
|-
| Grammy Awards Best Instrumental Composition Written for a Motion Picture or Television
| Ennio Morricone
| nomination
|-
| rowspan="6" | Saturn Awards Best Horror Film
| 
| nomination
|- Best Actor
| Jack Nicholson
| nomination
|- Best Actress
| Michelle Pfeiffer
| nomination
|- Best Supporting Actor
| James Spader
| nomination
|- Best Writing
| Jim Harrison, Wesley Strick
| winner
|- Best Make-up Rick Baker
| nomination
|}

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 