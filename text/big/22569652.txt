It's a Wonderful Afterlife
 
 
{{Infobox film
| name           = Its a Wonderful Afterlife
| image          = Its-a-wonderful-afterlife-april-21-film-poster.jpg
| caption        = Theatrical release poster
| director       = Gurinder Chadha
| producer       =
| writer         =  
| narrator       =
| starring       =  
| music          = Sony Music  Dick Pope
| editing        = Oral Nottie Ottey
| distributor    = Icon Film Distribution
| released       =  
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} Punjabi dialogue.  The title is a reference to Chadhas personal attachment to Frank Capras film Its a Wonderful Life. Chadha also co-produced the film, and co-wrote the screenplay with her husband and producing partner, Paul Mayeda Berges. The lead role is played by newcomer Goldy Notay, joining Shabana Azmi, Shaheen Khan, Sendhil Ramamurthy and Sally Hawkins in the cast.

==Plot==
Mrs. Sethi (Shabana Azmi) is a widow living in Southall who wants to marry off her only daughter, for she is alone and unhappy. Her daughter, Roopi, (Goldy Notay) is a little plump and opinionated. Mrs. Sethi finds that all her matchmaking efforts are rudely rejected. She avenges this behaviour toward her daughter by murdering the failed dates using her culinary skills. A police hunt begins for a serial murderer using a killer curry. 

Mrs. Sethi does not feel guilty until the spirits of her victims come back to haunt her. They are unable to reincarnate until their murderer dies. Mrs. Sethi must kill herself to free the spirits, but vows to get her daughter married first.

The spirits realise that helping Roopi find a suitable husband before the police catch Mrs. Sethi is in their best interests, and everyone begins to work together. Meanwhile, Roopi catches the eye of the young Sergeant investigating the case.

==Cast==
* Shabana Azmi as Mrs. Sethi
* Goldy Notay as Roopi
* Sendhil Ramamurthy as Raj (D.S. Murthy)
* Sally Hawkins as Linda/Gitali
* Zoë Wanamaker as Mrs. Goldsmith
* Sanjeev Bhaskar as The Curry Man
* Catherine Balavage as Waitress
* Shaheen Khan as Manjeet Kahl/The Kebab Woman
* Adlyn Ross as Mrs. Chakra/The Rolling Pin Woman
* Ash Varrez as Mr. Chakra/The Naan Man
* Mark Addy as D.I. Smythe
* Preeya Kalidas as Karishma	
* Jimi Mistry as Dev
* Ray Panthaki as Jazz

==Production==
The film is a comedy which uses satirical and Ealing Comedies|Ealing-style humour. Depicting life amongst the Asian community in Britain, it is set in the west London suburb of Southall.

===Development===
Chadha conceived the film while watching The 100 Greatest Family Films on Channel 4 when narrator Bob Hoskins introduced a wedding scene from her earlier film Bend It Like Beckham at position 71.

“It was the Indian wedding scene and the party, which was inter-cut with the football,” says Chadha, “and immediately I remembered how much fun wed had shooting that scene. The wedding is so integral to our culture that I suddenly thought ‘How can I do another wedding scene without repeating myself?’ So I thought maybe I could do it with a horror spin, where everything goes awry. Much like the prom scene at the end of Carrie (1976 film)|Carrie.” 

Working with long-time collaborator and screenwriter Berges, Chadha spent two and half years writing the script. “I started seeing this crazy film, set in Ealing, in the world of Bend It Like Beckham and yet in a completely different genre,” continues Chadha. “We worked on the script, came up with the idea of the mum, the plump daughter and these spirits that return.”

Starting with the working title My Bloody Wedding, Chadha and Berges created the character of Roopi, a young British Indian woman, and Mrs Sethi, her meddling mother. “Really its an Ealing comedy about an Indian mum who lives with her daughter. The daughter is a little bit overweight, not exactly beautiful and has a broken engagement behind her,” explained Chadha. “People in the community have been really mean about this girl and the mother has had enough. So she devises all kinds of ways of killing people off, using Indian cooking methods. And of course, being Indian, we believe in reincarnation. The people she kills come back as spirits and these spirits cant work out why theyve not been reincarnated.”

Goldy Notay gained weight to play the role of Roopi, which she then promptly had to shed for her part in Sex and the City 2. 

===Funding===
Its a Wonderful Afterlife was co-produced by the AIM-listed The Indian Film Company (TIFC) and Bend It Films in association with Viacom 18 Motion Pictures. HanWay Films is handling worldwide sales and distribution, and pitched the film as My Big Fat Greek Wedding meets Shaun of the Dead. Icon Film Distribution is the UK distributor. 

===Filming=== SFX to create the effect of ghosts and supernatural phenomena.

==Music and sound==
 
{{Infobox album  
| Name        = Its a Wonderful Afterlife
| Type        = compilation
| Artist      = Various Artists
| Cover       =
| Released    = 9 April 2010
| Recorded    =
| Genre       = Punjabi language|Punjabi, blues-rock, Britpop, hip-hop, Bhangra (music)|Bhangra, indie rock
| Length      = 62:02
| Label       = Sony Music
| Producer    = Sony Music
| Reviews     =
}} Taz of Wonderful Life" Black features in its original form on the soundtrack. Promotional music videos feat. Stereo nation are choreographed by Rohit Chawla.

{{track listing
| headline        = Track listing
| extra_column    = Artist
| extra_credits   = yes
| title1          = Punjabi Soldiers A Team Theme
| note1           =
| extra1          = Punjabi MC
| length1         = 3:32
| title2          = Bhangra Chic / Bhangra Rap
| note2           =
| extra2          = Kidd Skilly
| length2         = 3:05
| title3          = So Real So Right
| note3           =
| extra3          = San-j Sanj featuring Natty A
| length3         = 4:01
| title4          = Larl Larl Buleeya
| note4           = Taz with Stereo Nation featuring Mumzy
| length4         = 3:35
| title5          = Do the Nach
| note5           =
| extra5          = Chillitown featuring 10shott
| length5         = 3:11 Wonderful Life
| note6           = Black
| length6         = 5:46
| title7          = Disco Bhangra
| note7           =
| extra7          = Bally Sagoo Featuring Niraj Shridhar
| length7         = 3:27
| title8          = Crazee
| note8           = Taz with Stereo Nation
| length8         = 3:42
| title9          = Nikkiye (Madhanian)
| note9           =
| extra9          = Parminder Chadha
| length9         = 2:20
| title10         = Ghum Suhm
| note10          =
| extra10         = Sukshinder Shinda and Rahet Fateh Ali Khan
| length10        = 3:22
| title11         = Stayin Alive Desified
| note11          =
| extra11         = Bally Sagoo and Stayin Alive UK
| length11        = 4:39
| title12         = Stayin Alive
| note12          =
| extra12         = Bee Gees
| length12        = 3:24
| title13         = Its Love
| note13          =
| extra13         = Mica Paris Andrew Griffiths, Syrona Marie and Bally Sagoo,Gurinder Chadha & Paul Mayeda Berges
| length13        = 4:16
| title14         = Ghosts of Ealing
| note14          =
| extra14         = Craig Pruess and Bally Sagoo
| length14        = 3:09
|
}}

==Reception==
The film received overwhelmingly negative reviews, with The Scotsman calling it an "exceptionally lazy effort, which, like the similarly weak Bride and Prejudice, is more of a pun in search of a story than an actual fully-fledged idea". 
The Radio Times suggested, "The gags are brash, the plot is messy and theres an element of mild horror (culminating in a send-up of Carrie (1976 film)|Carrie) that feels totally random."  The Express. 
Noyon Jyoti Parasara of AOL India stated "Every filmmaker has their bad days. And after watching ‘Its a Wonderful Afterlife’ I am confident that Gurinder Chadha is in one of hers." 

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 