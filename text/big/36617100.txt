Sohni Mahiwal (1984 film)
{{Infobox film
| name           = Sohni Mahiwal
| image          = SohniMahiwal1984film.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional Poster
| director       = Umesh Mehra Latif Faiziyev 
| producer       = F. C. Mehra
| writer         =
| screenplay     =  Shanti Prakash Bakshi Javed Siddiqui
| story          = 
| based on       =  
| narrator       =  Pran Zeenat Aman
| music          = Anu Malik Anand Bakshi (lyrics)
| cinematography = S. Pappu
| editing        = M. S. Shinde
| studio         = 
| distributor    = Eagle Films Uzbekfilm Sovinfilm
| released       =    
| runtime        = 142 min 
| country        = India, USSR Russian
| budget         = 
| gross          = 
}}
 Choreographer P. L. Raj. 

==Plot==
Shahjada Ijjat Beg comes to India with his caravan and settles in a town in Gujrat. Here he falls in love with Sohani, who keeps a shop in metal pots. Ijjat Beg buys pot from her with whatever money he had and they were attracted to each other. Sohni dispensed with her servant and kept Ijjat Beg instead. This gave them more opportunity to meet. This was a scandal in the town and Sohni was perforce married to Rehaman who was slightly off his head. Sohni continued her meeting Ijjat Beg who went out fishing. When the atmosphere became to hot for them they jointly took a water grave for their love

==Cast==
* Sunny Deol...Mirza Izzat Beg
* Poonam Dhillon...Sohni
* Pran (actor)|Pran...Tulla
* Tanuja...Tullas wife
* Shammi Kapoor... 	Peer Baba
* Zeenat Aman...Zarina
* Gulshan Grover...Noor
* Rakesh Bedi...Salamat
* Frunzik Mkrtchyan...Warrior
* Zakir Mukhamedzhanov (as Zucker Mohed Janou)...Izzats father
* Nabi Rakhimov...Karavan-baqi
* Zulkhumor Muminova  (as Z. Muminova)...Salamati 
* Gulchekhra Dzhamilova (as G. Dzhamilova)...Malkani
* Isamat Ergashev...Djadru 
* Natalya Krachkovskaya  (as N. Krachkovskaya)...Woman in the caravan
* Uchkur Rakhmanov  (as U. Rakhmanov)...Man in the caravan
* Tamar Hovhannisyan (as Tamara Oganesyan)...Izzats mother  
* Mazhar Khan...Rashid 
* Sanat Divanov (as S. Divanov)...Bodyguard

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Sohni Meri Sohni"
| Anwar, Asha Bhosle
|-
| 2
| "Sohni Chinab Di Kinare"
| Anupama Deshpande
|-
| 3
| "Mujhe Dulhe Ka Sehra"
| Asha Bhosle, Shabbir Kumar 
|-
| 4
| "Bol Do Mithe Bol Soneya"
| Asha Bhosle, Shabbir Kumar
|-
| 5
| "Chand Ruka Hai"
| Asha Bhosle 
|-
| 6
| "Sohni Chinab Di Kinare" (II)
| Anupama Deshpande
|}

==Awards==

*32nd Filmfare Awards
**  : Won 
**Best Editing: M. S. Shinde: Won     Best Sound: Brahmanand Sharma: Won   
**  : Nominated  {{cite web |title=The Nominations - 1984
 |url=http://filmfareawards.indiatimes.com/articleshow/articleshow/articleshow/articleshow/367137.cms |date= |publisher=Filmfare Awards }} 
**Best Lyrics: Anand Bakshi: "Sohni Chinab Di": Nominated

==See also==
* Sohni Mahiwal

==References==
 

==External links==
* 

 
 
 
 
 
 

 