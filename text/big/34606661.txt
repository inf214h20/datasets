Hope and a Little Sugar
{{Infobox film
| name           = Hope and a Little Sugar
| caption        =
| image	         =
| director       = Tanuja Chandra
| producer       = Scott Pardo and Glenn Russow
| writer         = Tanuja Chandra and Glenn Russow
| starring       = Anupam Kher Vikram Chatwal Amit Sial Mahima Chaudhry Suhasini Mulay
| music          = Wayne Sharpe (score) and Jasbir Jassi (lyrics)
| cinematography = Nirmal Jani
| editing        = Hilary Peabody
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = India United States
| language       = English
| budget         =
| gross          =
}}

Hope and a Little Sugar is a 2006 Indian English-language film shot in New York and directed by Tanuja Chandra. It premiered and won an award on October 6, 2006 at the South Asian International Film Festival in New York.  In addition, it was the opening film in 2008 for the Opening Night Gala at BAFTA, sponsored by the London Asian Film Festival by Tongues on Fire. 

==Plot==
Ali (Amit Sial) is a photographer and bike messenger who lives in New York. He develops a friendship and falls in love with a married woman, Saloni Oberoi (Mahima Chaudhry). When her husband, Harry Oberoi (Vikram Chatwal), is killed during the September 11 attacks, Harrys father, a retired Colonel (Anupam Kher), begins to take his aggressions out on Ali for being a Muslim. Although Mrs. Oberoi (Suhasini Mulay) tries to stop the Colonels behavior, the situation escalates as the Colonel, himself, becomes the target of social post-9/11 aggression directed towards him because he is a Sikh.     

==Cast==
*Anupam Kher as Colonel Oberoi
*Mahima Chaudhry as Saloni Oberoi
*Vikram Chatwal as Harry Oberoi
*Amit Sial as Ali Siddiqui
*Suhasini Mulay as Mrs. Oberoi

==Production==
The film was shot in 2004 but was delayed due to "an exhaustive post-production exchange" between director Chandra and independent American producers—Scott Pardo and Glenn Russow.  This included pre and post-production that was entirely done via the internet. Pre-production took over two months where Chandra "finalised the cast, locations and even set design sitting in Mumbai." The entire cast and crew consisted of 25 people, and the film was shot entirely in New York over a period of 25 days.   

==Notes==
 

==See also==
*List of cultural references to the September 11 attacks

==External links==
*  
*  - Tongues on Fire/London Asian Film Festival

 
 
 
 
 
 
 
 