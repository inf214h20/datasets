The Lightning Raider
 
{{Infobox film
| name           = The Lightning Raider
| image          = The Lightning Raider poster.jpg
| caption        = Film poster
| director       = George B. Seitz
| producer       =
| writer         = John B. Clymer Charles W. Goddard George B. Seitz May Yohé
| starring       = Pearl White Warner Oland Boris Karloff
| cinematography =
| editing        = Astra Films
| released       =  
| runtime        = 15 episodes 
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}} action film serial directed by George B. Seitz. It was the on-screen debut of Boris Karloff.    The film serial survives in an incomplete state with some reels preserved at the Library of Congress and other film archives. 

==Cast==
 
* Pearl White as The Lightning Raider
* Warner Oland as Wu Fang
* Henry G. Sell as Thomas Babbington North
* Ruby Hoffman as Lottie
* William P. Burt as The Wasp (as William Burt)
* Frank Redman as Hop Sing
* Nellie Burt as Sunbeam
* Sam Kim
* Henrietta Simpson
* Boris Karloff Billy Sullivan (as William A. Sullivan)
* Anita Brown

==Chapter titles==
# The Ebony Block
# The Counterplot
# Underworld Terrors
# Through Doors of Steel
# The Brass Key
# The Mystic Box
# Meshes of Evil
# Cave of Dread
# Falsely Accused
# The Baited Trap
# The Bars of Death
# Hurled Into Space
# The White Roses
# Cleared of Guilt
# Wu Fang Atones

==See also==
* List of film serials
* List of film serials by studio
* Boris Karloff filmography

==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 


 
 