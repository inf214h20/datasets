Subbu
{{Infobox film
| name           =Subbu
| image          =
| alt            =  
| caption        = Movie poster
| director       = Rudraraju Suresh Varma
| producer       = R Srinivas PM Hari Kumar
| writer         =  NTR Sonali Joshi
| music          = Mani Sharma
| released       =  
| cinematography = Vijaya Sri
| editing        = Marthand K. Venkatesh
| studio         = 
| distributor    = 
| runtime        = 145 minutes
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}
 Telugu film directed by Rudraraju Suresh Varma and produced by R. Srinivas and P.M. Hari Kumar.  It stars N. T. Rama Rao Jr.|Jr. NTR, succeeding his role in the hit Student No.1, and Sonali Joshi.

== Plot ==

Bala Subramanyam alias Subbu (N. T. Rama Rao Jr.|Jr. NTR) is a college student who is loved by each and everyone for his cool nature and lovable philosophy of keeping his head cool all the time. Neeraja (Sonali Joshi) is a Good Samaritan who keeps on donating money anonymously to fund 25 seats in the college in which Subbu is studying. Subbu is curious to know about the person who is donating that money.

Subbu falls in love with Neeraja, mainly for her positive attitude. There is a strong reason for Neeraja to donate money to the college. There is a stronger reason for Subbu to become a cool-headed guy.

In the flashback, Subbu rescues a girl who met with an accident and admits her in a hospital. She is a daughter of a rich industrialist GK Chowdary. GK Chowdary misunderstands Subbu as the other Bala Subramanyam who is responsible for his daughter to become pregnant. Without knowing truth, GK becomes a reason for the suicides of Subbus entire family. GK realizes the truth later. And incidentally Neeraja is the younger sister of the girl whom Subbu rescues.

By the time of interval the audience realize that Subbu and Neeraja are in search of each other. Rest of the film twirls around how this couple realizes that they are made for each other, in deed.

==cast ==
*N. T. Rama Rao Jr.|Jr. NTR
*Sonali Joshi
*Brahmanandam A V S
*M. S. Narayana
*Dharmavarapu Subramanyam
*Benerjee
*Kallu Chidambaram
*Krishna Reddy
*Jogi Naidu

==Critical reception==
Upon the release jeevi of idlebrain.com gave 3/5 stating NTR is improving from film to film. To put in a more frank way, people started accepting NTR as his number of films is increasing (remember the old adage - tinaga tinaga vemu teeyanundu).Telugu cinema.com stated The film is below the expectations in all the senses. Songs in newzealand are ok. Other songs are not up to the mark.  The Movie was an average grossers at Box office.

== Soundtrack ==
{{Infobox album
| Name =subbu
| Type     = Soundtrack
| Artist   = Mani Sharma
| Recorded = 2001 Feature film soundtrack Telugu
| Label    = Aditya Music
}}

The Soundtrack of the film was composed by Mani Sharma.  Songs gained good response up on release.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song Name  || Singers || Lyrics
|- KK || Ramajogayya Sastry
|-
| 2|| "Love Passayyanu" || Mallikarjun, Smita || Kandikonda
|-
| 3|| "Hari Hara" || Mano, Smita || Ramajogayya Sastry
|- Mano || Veturi
|- Chandrabose
|-
| 6|| "Mastu Mastu" || R. P. Patnaik, Ganga || Ramajogayya Sastry
|}

== References ==
 

== External links ==
* 

 
 