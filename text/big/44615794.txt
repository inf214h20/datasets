Dialogue with the Carmelites
{{Infobox film
 | name =Dialogue with the Carmelites
 | image = Dialogue with the Carmelites.jpg
 | director = Raymond Léopold Bruckberger   Philippe Agostini
 | writer =  
 | starring =Jeanne Moreau Alida Valli
 | music = Jean Françaix
 | cinematography = André Bac
 | editing = 
 | released = 1960
 | language = French
 }} historical drama film written and directed by Raymond Léopold Bruckberger and Philippe Agostini. It is based on the story of the Martyrs of Compiègne, Carmelite nuns who were guillotined in Paris in 1794 in the waning days of the Reign of Terror during the French Revolution, after refusing to renounce their vocation.      

== Cast ==

*  
* Alida Valli :  Mère Thérèse de Saint-Augustin  
* Madeleine Renaud : First Prioress
* Pascale Audret : Blanche de la Force
* Pierre Brasseur : Commissioner of the Revolution
* Jean-Louis Barrault : Mime
* Anne Doat : Sister Constance de Saint-Denis
* Georges Wilson : Chaplain of the Carmel
* Pascale de Boysson : Sister Cécile
* Hélène Dieudonné : Sister Jeanne de la Divine Enfance  Pierre Bertin : Marquis de la Force
* Claude Laydu : Chevalier de la Force
* Daniel Ceccaldi :  Officier
* Judith Magre : Rose Ducor

==References==
 

==External links==
* 

 
  
   
 
   
 
 
 
 
 

 
 
 