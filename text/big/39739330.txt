Ooops a Desi
{{Infobox film
| name= Ooops a Desi
| image=Ooops a Desi.jpg
| image_size = 
| alt=
| caption= English
| director=Jenner Jose
| producer= Jenner Entertainment
| story=Jenner Jose
| based on= 
| screenplay= Jenner
| starring=Buali Shah Adnan Khalid Pragathi Yadhati Gideon Samson Anil Sachdeva 
Manohar Pinnamraju Siddharth Gupta Javed Khalid Sonia Gadhok
| cinematography=Sri Ponnapati
| editing=Jenner Jose
| studio=Jenner Entertainment LLC
| distributor=Artistic Crab Entertainment
| released=  
| runtime= 2 hours
| country=India, USA
| 
| language=Hindi; English
}}
 2013 Hindi fast-paced drama thriller film directed by Jenner Jose. The film premiered at Times Square (US) followed by theatrical release on 23 August 2013 at major multiplexes across India. Movie received a very positive critical reception. The lack of marketing was its drawback. Plans are now in place for extended release as well as Punjabi- and Telugu-dubbed versions due to the films positive response. The film features Buali Shah, Adnan Khalid, Pragathi Yadhati and Gideon Samson as main characters.

==Box Office==
The film opened to 45% response in many metro multiplex theatres (limited release) and collected Rs. 4.2 Crores on its first weekend. The movie was made on a very small minimal budget with very limited crew, so it was in the profit territory by the first weekend itself. * .

==Synopsis==
Ooops a Desi is about Indians (Desi)living in the US on an invalid immigration status. The movie opens to a busy downtown street (in New York city). The passing pedestrians notice something very alarming. Its a Desi (Indian) holding a visible sign. Sign states ”BOMB here!!!”. Soon we are taken to the story that occurred before this incident. The guy with the Bomb Sign (Xavier), along with Dev do odd jobs because of their illegal immigration status in the USA. They have their own troubled past that is preventing them from returning to India. The roller-coaster begins when AJ, their room-mate is mysteriously abducted to which Sonia is a witness. Because of their illegal status, Dev and Xavier along with Sonia have no option but to take matters in their own hands.

==Cast==
*Buali Shah as Xavier
*Adnan Khalid as Dev
*Pragathi Yadhati as Sonia
*Gideon Samson as AJ
*Anil Sachdeva as Bhushan
*Manohar Pinnamraju as Swami
*Siddhartha Gupta as Nilesh
*Javed Khalid as Patiala
*Sonia Gadhok as Aarthi
* Srikanth Reddy Ponnapati (Sri ponnapati)

==Music==
*Music Directors: GUFY, Gaurav H. Singh, Hardik Dave, Divyajeet Sahoo, Saurabh Malhotra, Bharat Hans
*Singers: GUFY, Hardik Dave, Bharat Hans, Suryaveer Hooja
*Lyrics: Deepak Agrawal, Leeladhar Dhote, Divyajeet Sahoo, Gaurav H. Singh, Hardik Dave

==References==
 

==External links==
* .
* .
* 
* .

 
 
 
 
 