How I Got into College
{{Infobox film
| name           = How I Got Into College
| image          = howigotintocollege.jpg
| image_size     =
| caption        = Movie poster for How I Got Into College
| director       = Savage Steve Holland
| producer       = Elizabeth Cantillon Michael Shamberg 
| writer         = Terrel Seltzer
| narrator       =  Corey Parker
| music          = Joseph Vitarelli
| cinematography = Robert Elswit
| editing        = Kaja Fehr Sonya Sones
| studio         = 20th Century Fox	 	
| distributor    = 20th Century Fox
| released       = May 19, 1989
| runtime        = 86 min. USA
| English
| budget         = 
| gross           = $1,642,239 (USA)
| preceded_by    = 
| followed_by    = 
}}
 Corey Parker, and Lara Flynn Boyle. This is the film debut for then-future voice actor Tom Kenny.

==Plot== Corey Parker), a boy who tries to get into Ramsey to pursue Jessica, whom he is in love with.

==Cast==
*Anthony Edwards as  Kip Hammett Corey Parker as  Marlon Browne
*Lara Flynn Boyle as  Jessica Kailo
*Finn Carter as  Nina Sachie
*Charles Rocket as  Leo Whitman
*Brian Doyle-Murray as  Coach Evans
*Tichina Arnold as  Vera Cook
*Tom Kenny as "B"
*Bill Raymond as  Flutter
*Phillip Baker Hall as  Dean Patterson
*Nicolas Coster as  Dr. Phillip Jellinak, Sr.
*Richard Jenkins as  Bill Browne
*Phil Hartman as Bennedict
*Duane Davis as Ronny "Sure Hands" Rawlson
*Diane Franklin as Sharon Browne
*Robert Ridgely as  George Kailo
*Micole Mercurio as  Betty Kailo
*Rino Passaniti as Tuba Player 1
*Keith "Beaker" Burrus as Tuba Player 2
*Richard Steven Horvitz as Young Energizer
*Bruce Wagner as "A"
*Phill Lewis as Earnest Boy
*Curtis Armstrong as Arcadia Bible Academy Recruiter

==Reception==

===Box office===
The movie was not a box office success, making just $651,850 in its opening weekend from 743 theaters for an average of $877 per venue.  It ended its run with only $1,642,239 domestically.  

==Production notes==
Parts of the film used Pomona College in Claremont, California, as Ramsey College.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 