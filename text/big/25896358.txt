Happythankyoumoreplease
{{Infobox film
| name           = happythankyoumoreplease
| image          = Happythankyoumoreplease.jpg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = Josh Radnor
| producer       =
| writer         = Josh Radnor
| narrator       =
| starring       = Malin Åkerman Tony Hale Zoe Kazan Kate Mara Josh Radnor Pablo Schreiber
| music          =
| cinematography = Seamus Tierney
| editing        = Michael R. Miller
| studio         = Tom Sawyer Entertainment
| distributor    = Anchor Bay Films
| released       =  
| runtime        = 100 minutes 
| country        = United States
| language       = English
| budget         =
| gross          = $853,862
| preceded by    =
| followed by    =
}}

happythankyoumoreplease is a 2010 comedy-drama film written and directed by  Josh Radnor in his directorial debut. {{cite news |title=Sundance 10 Josh Radnor Steps Behind the Camera for "happythankyoumoreplease" |publisher=IndieWire |date=January 21, 2010
|url= http://www.indiewire.com/article/2010/01/20/sundance_10_josh_radnor_steps_behind_the_camera_for_happythankyoumoreplease/ |accessdate=January 21, 2010 }}  The film stars Radnor, Malin Åkerman, Kate Mara, Zoe Kazan, Michael Algieri, Pablo Schreiber, and Tony Hale, and it tells the story of a group of young New Yorkers, struggling to balance love, friendship, and their encroaching adulthoods. 

happythankyoumoreplease premiered at the 26th Sundance Film Festival in 2010, where it won the Audience Award and was further nominated for the Grand Jury Prize. On March 4, 2011 it was released in theatres throughout Los Angeles and New York.

==Plot==

A story of relationships, happythankyoumoreplease deals with the struggles facing several pairs trying to find their way. The film centers on Sam (Radnor), a writer, and Rasheen (Algieri), a foster care child, who meet each other when Rasheen is abandoned on the subway. The film comes to also involve Sams best friend Annie (Åkerman), an Alopecia patient trying to find a reason to be loved; his cousin Mary Catherine (Kazan) and her boyfriend Charlie (Schreiber), a couple facing the prospect of leaving New York; and Mississippi (Mara), a waitress/singer trying to make it in the city.

==Cast==
* Josh Radnor as Sam Wexler
* Malin Åkerman as Annie
* Kate Mara as Mississippi
* Pablo Schreiber as Charlie
* Zoe Kazan as Mary Catherine
* Michael Algieri as Rasheen
* Tony Hale as Sam #2
* Jakob Appelman as School Boy
* Bram Barouh as Spencer
* Dana Barron as The Gynecologist
* Sunah Bilsted as Receptionist
* Jimmy Gary Jr. as Officer Jones
* Richard Jenkins as Paul Gertmanian
* Marna Kohn as Melissa

==Production==
Radnor wrote the film while working on the first and second seasons of the CBS sitcom How I Met Your Mother. He then had actors read for roles, wrote revisions, and sought financing for two years. Radnor received financing in April 2009 and began shooting in July 2009 in New York, after six weeks of pre-production. The film was selected for the Sundance Film Festival, where it premiered on January 22, 2010. {{cite news |title="How I Met Your Mother" Star Josh Radnor on His Sundance Debut "happythankyoumoreplease" |author=Michelle Kung |newspaper=The Wall Street Journal |date=January 21, 2010
|url= http://blogs.wsj.com/speakeasy/2010/01/21/how-i-met-your-mother-star-josh-radnor-on-his-sundance-debut-happythankyoumoreplease/ |accessdate=January 21, 2010 }}     It won the audience award for favorite U.S. drama.   

Myriad Pictures bought the international distribution rights for the film.    The publisher Hannover House bought the North American distribution rights,    but they were later acquired by Anchor Bay Films. 

==Release==

===United States=== Gen Art Film Festival in New York City on April 7, 2010. {{cite news |title=Twentysomethings tale will open Gen Art |last=Kilday |first=Gregg |magazine=The Hollywood Reporter |date=March 8, 2010 Late Late Show with Craig Ferguson.
 opened in theaters in New York City and Los Angeles on March 4, 2011.  The movie grossed $216,110 in the United States. 

It was released by Anchor Bay Entertainment on DVD and Blu-ray disc on June 21, 2011.

===Foreign===
One month after its theatrical release in America, happythankyoumoreplease opened in Spain where it did comparatively better, grossing $551,472. 

Over the following months, it was released in Belgium, Poland, Turkey, and Greece.

==Reception== average rating of 5.1/10. 

==Music==
 
Fourteen of Jaymays songs were produced as score for the film by Michael Brake, music editor for How I Met Your Mother.  Andy Gowan was the music supervisor and is also the music supervisor for How I Met Your Mother.

The full track list is as below:
(artist-track)
* Shout Out Louds - "My Friend and The Ink on His Fingers"
* Friends of the Jitney (with Katrina Lenk, Nyles Lannon and Chris Phillips) - "Phosphorescent Green"
* The Brendan Hines - "Miss New York"
* Cloud Cult - "Please Remain Calm"
* The Generationals - "When They Fight, They Fight"
* Jaymay - "40 Hours Ago"
* Jaymay - "One May Die So Lonely"
* Jaymay - "Never Be Daunted"
* Jaymay - "Lullaby"
* Jaymay - "Have to Tell You"
* Jaymay - "Rock, Scissors, Paper"
* Jaymay - "All Souls"
* Blunt Mechanic - "Thrown Out at Third"
* Jaymay - "1 & !"
* The Go - "You Go Bangin On (remix)"
* Cloud Cult - "Chemicals Collide"
* Blind Pilot - "The Story I Heard" The War on Drugs - "Arms like Boulders"
* VA - "Sing Happy"
* Bear Lake - "Smile"
* Dr. Dog - "The World may Never Know"
* Throw Me The Statue - "Waving at the Shore"
* Jaymay - "Long Walk To Never"

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 