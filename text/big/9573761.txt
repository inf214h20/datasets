Lee (film)
{{multiple issues|
 
 
}}
{{Infobox film
| name = Lee 
| image = 
| director = Prabu Solomon
| writer =  Nila
| producer = 
| music = D.Imman 
| distributor = 
| released = 16 February 2007
| cinematography = Rajesh Yadav
| runtime = 
| country = India
| language = Tamil
| budget = 
}}
 Nila in the lead roles. The film was produced by noted actor and Sibirajs dad, Sathyaraj and directed by Prabu Solomon. The film was successful at the Box office.

== Plot ==
Leelatharan, Lee to his friends, hangs out with a bunch of merry youth who do practically any work and play football in other times. It is a strong-willed and thick-tied group. Chellam (WINGER), a worker in a facility for the mentally-challenged, is also kind of a groupie. Life is all merry and mirth till she espies Lee & co, attempting an assassination of sorts on a Minister. The bid fails, but Chellam is shocked and so are we. Why would this bunch want to take a crack at the murky world of politics and cricket.

Well, the answer unspools back into a college where Lee and his team are raring to go as footballers and cricketers. They have a committed mentor in Butthiran (player). Though a coach, he is a friend, philosopher and guide kind of omnibus figure for the youngsters. Just when the boys are making the right progress, problem erupts in the form of Rangabashyamicy (wing man). He is a scumbag, but is also the principal of the college and he wants his son to be in this all-conquering team.

The coach, with Chappellesque firmness, says no to this and wants to pick his team on merit. But the principal kicks out the team with its coach out of the college itself. So they gravitate towards another college and again start doing well as a team. And when they win a big and prestigious tournament, Rangabashyam throws up a major spanner in the works.

He scuttles the rise of Lee and co. The stifling is so bad that one member commits suicide and the team slowly disintegrates, while on the other hand, Rangabashyam rises up in the big bad world of politics. He ends up as a Minister.

Now the circle is complete. So the question, what happens to the rivalry of Rangabashyam and Lee and his team. And does Chellam understand the rationale behind Lee’s actions? and will they put him down? find out by watching the film.

== Cast ==
*Sibiraj as Leeladharan, aka Lee
*Nila as Chellam
*Prakash Raj as Buthiran
*Ahthi as Vijay
* Vidharth

== Songs ==
# Oru Kalavani payala
# Thandora kondakari

== Production ==
Veteran actor and a caring father, Satyaraj is producing Lee which stars his son Sibiraj in the lead role. Prabhu Solomon, who had done a racy action in Kokki earlier, is directing the film. Sibiraj plays Leeladharan alias Lee, a professional football player. Nila plays the female lead. Prakash Raj plays the football coach in the movie.

Says Sibiraj, “I prepared myself for the role with great difficulty. I ensured that I looked different and changed my mannerism a lot. I grew long hair and beard too.”

Since Sibiraj plays a footballer, he underwent football coaching at the YMCA every evening. Director Solomon shot a major portion of Lee in the busy roads of Chennai. The movie was shot in the busy roads without the public even being aware of it.

Though Sibi plays a footballer, the movie does not deal about the sport. “It is an action-thriller,” Solomon says and adds, “The whole crew has worked hard for the movies success and Prakash Raj especially, has done a fabulous job.”

Music by Imman is a major highlight. “The songs have come out really well,” he adds.

Cinemotography by Rajesh Yadav, has been excellent. Through his lens, we could see the unknown streets of Chennai, which really suits for this racy entertainer.

 

 
 
 
 
 