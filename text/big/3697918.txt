Chemmeen
 
{{Infobox film
| name           = PAPPAN (ANANDU)

| image          = Chemmeen.JPG
| image_size     = 
| caption        = Hand drawn poster featuring Sheela
| director       = Ramu Kariat
| producer       = Babu Ismail Sait (Chemmeen Babu)
| screenplay     = S. L. Puram Sadanandan
| based on       =   Sathyan
| music          = Salil Chowdhury
| cinematography = Marcus Bartley,  
| editing        = Hrishikesh Mukherjee, K. D. George
| studio         = Kanmani Films
| distributor    =
| released       =  
| runtime        =
| country        = India Malayalam
| budget         =
| gross          =
}} novel of the same name by Thakazhi Sivasankara Pillai and directed by Ramu Kariat from a screenplay by S. L. Puram Sadanandan. It was produced by Babu Ismail Sait.

The film tells the story of a pre-marital and later extra-marital relationship between Karuthamma, the daughter of an ambitious Hindu fisherman, and Pareekutty, the son of a Muslim trader. The theme of the film is based around the popular belief among the fishermen communities along the coastal   and K. D. George. The original score and songs were composed by Salil Chowdhury, with lyrics by Vayalar Ramavarma|Vayalar, and featuring the voices of K. J. Yesudas, P. Leela, Manna Dey and Santha P. Nair.

The film released on 19 August 1965. It received strongly positive critical reviews and was recognized as a technically and artistically brilliant film. It is usually cited as the first notable creative film in South India.  , p.?. "Chemmeen (1965), by Ramu Kariat in Kerala, is usually cited as Indian Presidents Gold Medal Cannes and Chicago festivals. The film was included in the list of 100 greatest Indian films by IBN Live.  Chemmeen was dubbed and released in Hindi as Chemmeen Lahren and in English as The Anger of the Sea.

==Plot==
Karuthamma (Sheela) is the daughter of an ambitious lower caste Hindu fisherman, Chembankunju(Kottarakkara Sreedharan Nair). She is in love with a young Muslim fish trader, Pareekutty (Madhu (actor)|Madhu). Chembankunjus only aim in life is to own a boat and net. Pareekutty finances Chembankunju to realise this dream. This is on a condition that the haul by the boat will be sold only to him. Karuthammas mother Chakki (Adoor Bhavani) comes to know about the love affair of her daughter with Pareekutty, and reminds her daughter about the life they lead within the boundaries of strict social tradition and warns her to keep away from such a relationship. The fisherfolks believe that a fisherwoman has to lead a life within the boundaries of strict social traditions and an affair or marriage with a person of another religion will subject the entire community to the wrath of the sea.

Karuthamma sacrifices her love for Pareekutty and marries Palani (Sathyaneshan Nadar|Sathyan), an orphan discovered by Chembankunju in the course of one of his fishing expeditions. Following the marriage, Karuthamma accompanies her husband to his village, despite her mothers sudden illness and her fathers requests to stay. In his fury, Chembankunju disowns her. On acquiring a boat and a net and subsequently adding one more, Chembankunju becomes more greedy and heartless. With his dishonesty, he drives Pareekutty to bankruptcy. After the death of his wife, Chembankunju marries Pappikunju (C. R. Rajakumari), the widow of the man from whom he had bought his first boat. Panchami (Lata), Chembankunjus younger daughter, leaves home to join Karuthama, on arrival of her step mother. Chembankunjus savings is manipulated by his second wife. The setbacks in life turns Chembankunju mad.

Meanwhile, Karuthamma has endeavoured to be a good wife and mother, but scandal about her old love for Pareekutty spreads in the village. Palanis friends ostracize him and refuse to take him fishing with them. By a stroke of fate, Karuthamma and Pareekutty meet one night and their old love is awakened. Palani, at sea alone and baiting a shark, is caught in a huge whirlpool and is swallowed by the sea. Next morning, Karuthamma and Parekutty, are also found dead hand in hand, washed ashore. At a distance, there lies a baited dead shark and Palani.


==Cast== Sathyan as Palani Madhu as Pareekkutty
*Sheela as Karuthamma
*Kottarakkara Sreedharan Nair as Chembankunju
*Adoor Bhavani as Chakki
*Latha Raju as Panchami
*Adoor Pankajam as Nalla Pennu
*S. P. Pillai as Achankunju
*Rajakumari Venu as Pappikkunju
*Philomina
*Paravoor Bharathan
*Nilambur Ayisha

==Production==

===Novel adaptation===
 
The novel fictionalised the myth among the fishermen communities along the coastal Kerala about chastity. 

Kariat bought the rights from Thakazhi for   8000, a comparatively large sum for a Malayalam novel then.  With adapting the novel, Ramu Kariat was taking a big risk as everybody in the film circle was almost certain that the film would be no patch  on the novel. 

===Filming===
Once Ramu Kariat had decided to make a film based on the novel, he approached several people including the Kerala State Government for funds to produce the film. On one of these journeys, he met Babu Ismail Sait (Kanmani Babu). Then in his early twenties, Kanmani Babu agreed to finance the film. The film was completely shot from Nattika beach, located about 25&nbsp;km from Thrissur.  In the 40th anniversary of the release of the film, Madhu described how the fisherfolk of Nattika cooperated by offering their homes and clothing for the filming. He also described how Sathyan escaped being drowned in the sea during the shooting.   

== Awards ==
* National Film Award for Best Feature Film {{cite news|url=http://articles.timesofindia.indiatimes.com/2013-04-04/did-you-know-/32253677_1_national-award-first-malayalam-film-best-feature-film|title=
Chemmeen won the National Award |accessdate=11-10-2012 | work=The Times Of India}} 
* Certificate of Merit at the Chicago International Film Festival
* Gold Medal at the Cannes Film Festival for Best Cinematography - Marcus Bartley 

== Significance ==

The film was included in the list of 100 greatest Indian films by  . May 12, 2013. Retrieved May 24, 2013.   .  . April 26, 2013. Retrieved May 24, 2013. 

==Soundtrack==
{{Infobox album|
 Name = Chemmeen|
 Type = Soundtrack |
 Artist = Salil Chowdhury | Vayalar |
 Cover = Chemmeencover.jpg|
 Background =  |
 Released = 1965 |
 Recorded = |
 Genre    = World Music|
 Length   = |
 Label    = HMV |
 Producer =Babu Ismail Sait |
 Last album =Chand Aur Suraj (1965) |
 This album = Chemmeen (1965) |
 Next album = Ezhuraathrikal (1968)|
}}

The music was composed by Salil Chowdhary and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kadalinakkare Ponore || K. J. Yesudas || Vayalar Ramavarma || 3:48
|-
| 2 || Maanasamaine Varoo || Manna Dey || Vayalar Ramavarma || 3:12
|-
| 3 || Pennaale Pennaale || K. J. Yesudas, P. Leela, Chorus || Vayalar Ramavarma ||  5:39
|-
| 4 || Puthan Valakkare || K. J. Yesudas, P. Leela, Chorus, KP Udayabhanu, Shantha P. Nair || Vayalar Ramavarma || 3:19
|-
| 5 || "Theme Music" || Instrumental ||  ||2:20
|}

==Footnotes==
 

==Bibliography==
*  

* {{cite news
| url = http://www.hindu.com/mp/2010/11/22/stories/2010112250310400.htm
| title = Chemmeen 1965 
| accessdate = 
| author = B. Vijayakumar
| date = November 22, 2010
| publisher = The Hindu
| location=Chennai, India
}}
* {{cite news
| url = http://www.hindu.com/2005/10/13/stories/2005101301370200.htm
| title = Celebrating 40 years of a movie classic 
| accessdate = 
| author = 
| date = October 13, 2005 
| publisher = The Hindu
| location=Chennai, India
}}

== External links ==

*  


 

 
 
 
 