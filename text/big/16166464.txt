Mitta Miraasu
{{Infobox film
| name           = Mitta Miraasu
| image          =
| caption        =
| director       =Kalanjiyam
| writer         =
| starring       = Prabhu Ganesan Roja Selvamani Vadivelu Kovai Sarala
| producer       = CR Karunanidhi N Rajendran
| cinematography =
| music          = Aslam Mustafa
| editor         =
| released       = October 10, 2001 
| runtime        =
| language       = Tamil
| country        = India
}}

Mitta Miraasu is a South Indian Tamil film directed by Kalanjiyam released in 2001.

==Plot==
Chellaiya has two missions in life. One, is to remove the taint on the family name and expose the man behind his familys misfortunes. Two is to make his younger sibling a lawyer, so that it will help him in achieving mission one. The villain of the piece is Chellaiyas own uncle Masilamani ( Alex) who had turned the villagers against Chellaiyas father Singha Perumal (Napoleon). Humiliated by the same people who had revered him once, Perumal dies of shock soon after (a laborious death scene). A few decades have passed since the incident, but Chellaiya seems nowhere near to fulfilling his mission, nor has Masilamani given up his wicked ways.

In fact Masilamani is stronger now, what with his two sons grown up and matching their father step tp step in his villainy. The trio have no redeeming qualities. Many cliched scenes later Chellaiya fulfills his mission and redeems his familys reputation.

==Cast== Prabhu as Chellaiya Roja as Meenakshi Alex as Masilamani Napolean as Singaperumal
* Mumtaj as Vijaya
* Vadivelu as Rangasamy
* Kovai Sarala as Bhagyam
* Manivannan
* Ravi Rahul

==Production==
Prabhu and Napoleon team together in Mitta Mirasu. And playing the female leads are Roja and Mumtaz. Manivannan, Kovai Sarala,Vadivelu, Ravi Rahul, Satya Prakash, Sunder, Alex,Chaplin Balu, and new face Shakthi form the supporting cast. The film is produced by C.R.Karunanidhi, N.Rajendran and M.Kalanjiyam. Kalanjiyam, who earlier directed films like Poomani, Poonthottam, Kizhakkum Merkkum and Nilave Mugham Kaattu, has written the story, screenplay and dialogues apart from directing the film.

Shooting is on at locations in Ooty, Gobichettypalayam, and Pollachi. The film has art design by Uma Shanker, dance choreography by Lalitha Mani, Stunt arrangements by Super Subbarayan, and editing by Lenin-Vijayan. Vairamuthu has done the lyrics, which are tuned by new music director, Aslam Mustafa. Mustafa has earlier sung some songs for A.R.Rehman and earned appreciation for his songs in Malli. 

==Soundtrack==
* Jal Jal - Shankar Mahadevan
* Mittamirasu - Mano
* Adiye - Chitra
* Kichu Kichu - Srinivas, Jaya
* Vannakiliye - Srinivas, Sujatha

==Awards==
* Tamil Nadu State Award for Best Villain - Alex. 

==Critical reception==
Critic wrote:"It is the same old storyline, same old incidents, practically the same old stars and the same old performances. The director tries to whip up excitement towards the end with some violent, bloody action scenes, but then it is a bit too late",  while another critic wrote:"Mitta Miraasu sets several things up and raises expectations but then fails to deliver on those expectations. Problems arise and issues are created but all of them end in a rather lame manner". 

==References==
 

 
 
 
 