In Heaven (film)
{{Infobox film
| name           = In Heaven
| image          = 
| caption        = Original release poster
| director       = Michael Bindlechner
| producer       = Michael Bindlechner
| writer         = TG Gergely Teglasy
| starring       = Sylvie Testud Xaver Hutter Merab Ninidze
| cinematography = Michael Kaufmann
| music          = Flora St. Loup
| editing        = Eliska Stibrova
| studio         = FRAMES filmproduction
| distributor    = Bavaria International
| released       =  
| runtime        = 102 minutes
| country        = Austria
| language       = German
| budget         = € 1,5 million
}}
In Heaven is a 1998 Austrian drama film directed by Michael Bindlechner. The plot of the film involves Csiwi, a 16 year old boy who learns about life through unruly girlfriend and chastened buddy. The film was entered into the 1999 International Film Festival Rotterdam.   

==Plot==
Csiwi (Xaver Hutter) is 14 and "borrows" his brothers car at night - thus not only driving around the suburban streets until dawn but also to find his way in life. When he meets Valeska (Sylvie Testud) – who seems to know her destination which she has identified on a postcard -  and Levi (Merab Ninidze) – who is tired of fighting and searching and wants to arrive only. Together they live through a summer of friendship at which end Csiw is alone again but finally knows his way.

==Cast==
* Sylvie Testud as Valeska
* Xaver Hutter as Civi
* Merab Ninidze as Levi

==Themes==

In Heaven is a coming of age story in which a rebellious young boy from the suburbs, Csiwi, encounters two people - Valeska and Levi - both in their desires and life goals pointing like signposts in a different direction. One summer, they experience the happiness of giving each other support and new hopes. But in the end each of them has to go his own way.
Bindlechner: What makes it worth while to tell the story of Csiwi, Levi and Valeska is that apparent happiness on the surface can hide the underlying drama right to the end - as so often in real life". 

==Critical reception==
In Heaven received generally positive reviews   and was invited to more than twenty film festivals world wide. In Heaven has a 6.3/10 rating at the International Movie Database. 

==Awards==
{| class="wikitable"
! style="background: #CCCCCC;" | Year
! style="background: #CCCCCC;" | Festival
! style="background: #CCCCCC;" | Result
! style="background: #CCCCCC;" | Award
! style="background: #CCCCCC;" | Recipient
|-
| 1999
| Max Ophüls Festival
| Winner 
| Best Young Actor
| Xaver Hutter
|- 1999
|Max Ophüls Festival Nominated    Best Film - Max Ophüls Award Michael Bindlechner
|- 1999
|Rotterdam International Film Festival Nominated  Best Film - Tiger Award Michael Bindlechner
|-
|}

==References==
 

==External links==
*  

 

 

 