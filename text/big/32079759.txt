Blue (2003 film)
{{Infobox film name           = Blue image          = Blue (2003) movie poster.jpg caption        = Theatrical Poster
| film name = {{Film name hangul         =   rr             = Beulru  mr             = Bŭllu}} director       = Lee Jeong-guk  producer       = Choe Hyeon-muk Choe Jin-hwa David Cho  writer         = Kim Hae-gon Kang Je-kyu  starring  Shin Hyun-joon Shin Eun-kyung Kim Yeong-ho  music          = Choe Tae-hwan cinematography = Choi Gi-youl Seo Geun-hui editing        = Park Gok-ji distributor    =  released       =   runtime        = 108 minutes country        = South Korea language       = Korean budget         =  gross          = $997,715 
}}
Blue ( ) is a 2003 South Korean war film directed by Lee Jeong-guk focusing on elite rescue divers of the South Korea Navy. The film attracted 61,223 admissions in the nations capital Seoul. 

==Plot==
Two friends in the Korean Navy, Lee and Kim are both part of an elite diving squad, specialising in emergency deep sea salvage dives. Lee is strait-laced and takes his duties seriously, while Kim treats the Navy as a lark. When Kang, a diving instructor and Kims former girlfriend, is posted to the unit, this creates tension between the friends as they compete for Kangs affections.  The tension is heightened when Lee is promoted ahead of Kim, creating a rivalry between the two.  Kims gung-ho approach to diving, and the danger he poses for himself (and his fellow divers), leads to further problems.  Matters come to a head when an incident at sea causes the sinking of a submarine, requiring the unit to attempt a dangerous salvage rescue of the sunken submarine.

==Cast== Shin Hyun-joon as Kim Jun
* Shin Eun-kyung as Kang Su-jin
* Kim Yeong-ho as Lee Tae-hyeon

== See also ==
* Cinema of Korea

== References ==
 

==External links==
*  
*  

 
 
 
 
 


 