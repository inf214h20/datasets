Tarka the Otter (film)
 
 
{{Infobox film
| name           = Tarka the Otter
| image size     =
| image          = Tarka the Otter FilmPoster.jpeg
| caption        =
| director       = David Cobham
| producer       = David Cobham
| writer         = Henry Williamson   Gerald Durrell   David Cobham
| narrator       = Peter Ustinov Spade
| music          = David Fanshawe John McCallum Charles Davies
| distributor    = Rank Film Distributors
| released       =  
| runtime        = 87 mins.
| country        =  
| books          = Tarka and me by Pete Talbot, handler English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 novel of the same name by Henry Williamson. Tarka the Otter was voted 98th in Channel 4’s poll of the 100 Greatest Family Films. {{cite web
| title      = 100 Greatest Family Films
| publisher  = Channel 4
| url        = http://www.channel4.com/film/newsfeatures/microsites/F/greatest-familymovies/results/100-96.html
| accessdate = 2008-03-24}} 

==Production==
The role of Tarka was played by an otter called Spade. 

==Ripple of Ancient Sunlight==
The principal animal handler for the film was Peter Talbot, who wrote a book about his time on the film called Tarka and me. It is now known by its original title Ripple of Ancient Sunlight. Talbot trained at the Otter Trust under Philip Wayre. In 1976 he was invited by film producers David Cobham and Bill Travers to hand rear a baby otter called Spade for the title role. 
The Authors note for Ripple of Ancient Sunlight explains...
...A little over ten years ago I was contacted by a Channel 4 researcher who had been tasked with investigating a classic old animal film called Tarka the Otter. I was puzzled why and he told me that film had just won an award in a family movie category but, finding nothing written anywhere about the animals in the film, he had noticed my name on the credits.
A year or so later I heard the sad news that another of my friends, from the Tarka period, had fallen silent and the shock prompted me into a decision to write it all down. The story belongs to them. It happened over a period of two years in the latter part of nineteen seventies whilst filming Henry Williamson’s classic novel. The film featured an otter called Spade as Tarka in real-time as he grew up and, for the duration as his minder, Tarka the Otter became a way of life for us both.

==Musical score== Argo label in 1979 (ZSW 613), and also included Peter Ustinovs narration.

==References==
 

==External links==
* 

 
 
 
 
 
 
 