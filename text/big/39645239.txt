Something Something (film)
{{Infobox film
| name = Something Something
| image = Something Something poster.jpg
| writer = Sundar C. Siddharth Hansika Motwani Brahmanandam
| director = Sundar C.
| cinematography = Gopi Amarnath
| producer = Subrahmanyam B. Suresh S.
| editing = Praveen K. L. N. B. Srikanth
| distributor = Lakshmi Ganapathi films
| country = India
| released =  
| runtime =
| language = Telugu
| music = C. Sathya
| awards =
| budget =
| gross =
}}
 Samantha together Santhanam replacing Brahmanandam in the Tamil version.  Music is composed by C. Sathya and Gopi Amarnath handled the Cinematography while award winning duo Praveen K. L. and N. B. Srikanth were the editors for the film. The film, along with the Tamil version, released on June 14, 2013 worldwide.

==Cast== Siddharth as Kumar Giri
* Hansika Motwani as Sanjana
* Brahmanandam as Premji
* Ganesh Venkatraman as George
* Rana Daggubati (cameo) Samantha (cameo)
* Kushboo (cameo)

==Production== Siddharth and Santhanam in Samantha were roped for cameo appearances together in both the versions.  The film was cleared by the Censor Board with a U/A Certificate and was released on June 14, 2013 worldwide.   

==Reception==
The film received mixed to positive reviews. 123telugu.com gave a review of 3/5 stating "Something Something has a very good first half and below par second half. Siddharth and Brahmanandam keep the film alive with their entertaining sequences. Poor music and a predictable storyline are liabilities. The movie ends up being just a decent romantic comedy."  idlebrain.com gave a review of rating 3/5 stating "Something Something is a romantic comedy that features Brahmanandam in a full length character. It is one of those films in which the character suits Siddharth very well. The scenes and thread featuring Brahmanandam is the lifeline of the movie. On a whole, Something Something works because of Brahmanandam."  Oneindia Entertainment gave a review stating "Expect a simple love story and loads of comedy. Woohoo Brahmi!!"  APHerald.com gave a review of rating 2.75/5 stating "Dont expect an intense love story or complete fun ride from starting frame to end in Something Something. But one thing is sure there will be entertaining comedy episodes between Brahmi-Sidd and for sure you will laugh every time Brahmi is on screen. Credit for fresh funny lines which appeals to todays audiences goes to Sundar C and Brahmis perfect execution made  Something Something movie to get above two rating." 

==References==
 

==External links==

 

 
 
 
 
 