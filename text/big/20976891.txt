Good Night, Paul
 
 
{{Infobox film
| name           = Good Night, Paul
| image          = File:Good Night Paul poster.jpg
| image_size     =
| caption        = Film poster Walter Edwards
| producer       = Lewis J. Selznick
| writer         = Charles Dickson(story) Roland Oliver(story) Julia Crawford Ivers(adaptation)
| music          = Harry B. Olsen
| Lyrics         = Charles Dickson Harrison Ford Constance Talmadge Norman Kerry
| music          =
| cinematography = James Van Trees
| distributor    = Select Pictures
| released       =   reels
| country        = United States Silent English intertitles
}}
 Walter Edwards. It was based on a successful stage play with book and lyrics by Roland Oliver and Charles Dickson, and music by Harry B. Olsen. Produced by Lewis J. Selznicks Select Pictures Corporation.

Its a farce that entails a wife willing to pose as a wife of her husband’s business partner as a scheme to hoodwink the business partner’s uncle out of money. But the uncle’s overextended visit forces schemers to keep up the charade leading to comical situations.
 no known print of this film.

==Cast==

*Constance Talmadge (Mrs. Richard) Harrison Ford (Paul Boudeaux)
*Zasu Pitts
*Norman Kerry (Richard Landers)
*Beatrice Van (Rose Hartley)
*John Steppling (Batiste Boudeaux)
*Rosita Marstini (Madame Julie)

==External links==
* 
* 
*  at New York Times website
* 

 
 
 
 
 
 
 


 
 