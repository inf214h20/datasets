Son of Dracula (1974 film)
 
{{Infobox film
 | name           = Son of Dracula
 | image          = Son of Dracula 1974 Apple.jpg
 | caption        = 1974 Movie Poster
 | director       = Freddie Francis
 | writer         = Jennifer Jayne
 | starring       = Harry Nilsson Ringo Starr Freddie Jones Suzanna Leigh Dennis Price
 | producer       = Jerry Gross Ringo Starr Tim Van Rellim
 | music          = Paul Buckmaster
 | cinematography = Anthony B. Richmond
 | distributor    = Cinemation Industries (US), Apple Films
 | released       = 19 April 1974 (U.S.)
 | runtime        = 90 mins English
 | budget         =
}}

Son of Dracula is a British musical comedy film, starring Harry Nilsson and Ringo Starr, produced by Starr and released in 1974 by Apple Films. It is also the title of a Harry Nilsson album released in conjunction with the movie. 

==Background==
Son of Dracula was made during a period when Starr, in between occasional single releases and session work, was concentrating on film-making and acting. Two movies in which he had starred, 200 Motels and Blindman, had been released at the end of 1971, and before starting on this one, he had just finished work on his directorial debut, the T.Rex documentary Born to Boogie.
 Merlin the Maureen brought him a copy, he didnt even know that Son of Schmilsson had already used a similar theme.

==Plot==

After the killing of his father (Count Dracula, the King of the Netherworld), by a mysterious assassin, Count Downe (Harry Nilsson) is summoned from his travels abroad by family advisor Merlin (Ringo Starr) in order to prepare him to take over the throne. Baron Frankenstein (Freddie Jones) is also on hand to help in any way he can. Problem is, Downe wants no part of this responsibility, and instead wishes to become human and mortal − especially after meeting a girl named Amber (Suzanna Leigh), with whom he falls in love. He approaches old family nemesis Dr Van Helsing (Dennis Price), who agrees to enable the Counts transformation, much to the dismay of the residents of the Netherworld. 

Despite the best efforts of a host of monsters, as well as one traitorous figure who is dealt with by the trusted Merlin, Van Helsing performs the operation and removes Downes fangs. He then informs the Count that he can now live out his days in the sunlight, with Amber at his side.
 Rolling Stones Jim Price. 

==Release==
 Bernard McKenna, were asked to write a whole new script to be dubbed over the films lacklustre dialogue, and they recorded an alternative, Pythonesque soundtrack, but the whole idea was then shelved. Later, attempts were made to market the movie, but as Ringo Starr later said, "No one would take it."

Showings over the years have been limited to midnight movies and similar outlets. No official home video release has ever been made, and reviewers such as Leonard Maltin have little positive to say about the film.
 Beatlefest convention, to be shown only at a special Friday night party that host Mark Lapidos was giving for the convention guests and vendors.  Most party attendees ignored the film. On YouTube, a version in 11 parts is available. According to journalist Peter Palmiere, Starr said in the late 1980s that he had a copy of the video lying on top of his TV set, but he couldnt bear to look at it.  

==Soundtrack album==
{{Infobox album |  
| Name        = Son of Dracula
| Type        = soundtrack
| Artist      = Harry Nilsson
| Cover       = Harry_Nilsson_Son_of_Dracula.jpg
| Released    = 1 April 1974 (US)   24 May 1974 (UK)
| Recorded    = 1971−72; September 1972   Trident Studios, London
| Genre       = Pop music
| Length      = 40:00
| Label       = Rapple (RCA Victor|RCA/Apple Records|Apple)
| Producer    = Harry Nilsson, Ringo Starr, Richard Perry
| Last album  = A Little Touch of Schmilsson in the Night   (1973)
| This album  = Son of Dracula   (1974)
| Next album  = Pussy Cats   (1974)
| Misc = {{Singles
  | Name           = Son of Dracula
  | Type           = soundtrack
  | Single 1       = Daybreak
  | Single 1 date  = 25 March 1974 (US); 7 June 1974 (UK)
  | Single 2       = 
  | Single 2 date  = 
  }}
}}{{Album ratings|title=Soundtrack
| rev1      = AllMusic
| rev1Score =   
| rev2 = The Essential Rock Discography
| rev2Score = 4/10 
}}
The Son of Dracula album includes Nilsson songs that were showcased in the film, as well as some instrumental tracks composed by Paul Buckmaster and portions of dialogue used as bridging sequences. All the song tracks bar one are from the previously released Nilsson Schmilsson and Son of Schmilsson albums.

The only new song, "Daybreak", was recorded in London sometime in September 72,  during a break in filming. Joining Nilsson and Starr on the sessions at Trident Studios were the likes of Voormann, Frampton, Keys and Price, once again, as well as George Harrison on cowbell.  Jim Price, along with pianist Gary Wright and orchestral arrangers Paul Buckmaster and Del Newman, also provided new, incidental music, some of which appeared in the movie only. 
 LP release of the soundtrack included a T-shirt iron-on advertising the movie, and a companion songbook included a reproduction of the movie poster. The single version of "Daybreak" edited out the words "its pissing me off" (referring to daylight), repeating the lyric "its making me cough" instead, and the fadeout is longer than on any LP or CD release of the song. The single peaked at number 21 on the Billboard Hot 100|Billboard Hot 100 but did not chart at all in the UK. The album itself fared even worse, non-charting in Britain and climbing no higher than number 106 in America.
 covered by Monkee Micky Dolenz.

==Album track listing==
#It is he who will be king (Paul Buckmaster) – 3:07
#"Daybreak" (Nilsson) – 2:43 John C. Moore) – 2:40
#Count Downe meets Merlin and Amber (Buckmaster) – 2:10
#"The Moonbeam Song" (Nilsson) – 3:20
#Perhaps this is all a dream (Buckmaster) – :47
#"Remember (Christmas)" (Nilsson) – 4:09 Without You" Tom Evans) – 3:47
#The Counts vulnerability (Buckmaster) – 2:10
#"Down" (Nilsson) – 3:07
#Frankenstein, Merlin and the operation (Taverner) – 3:20
#"Jump into the Fire" (Nilsson) – 3:16
#The abdication of Count Downe (Buckmaster) – 1:10
#The end (Moonbeam) – :49

==External links==
* 
* 

==References==
 

 
 

 
 
 
 
 
 
 
 
 