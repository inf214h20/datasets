Arthur Christmas
{{Infobox film
| name           = Arthur Christmas
| image          = Arthur Christmas Poster.jpg
| caption        = Theatrical release poster Sarah Smith
| producer       = Steve Pegram
| writer         = Peter Baynham Sarah Smith
| story          = Sarah Smith (uncredited)
| starring       = James McAvoy Hugh Laurie Bill Nighy Jim Broadbent Imelda Staunton Ashley Jensen Marc Wootton Laura Linney Eva Longoria Ramona Marquez Michael Palin
| music          = Harry Gregson-Williams
| cinematography = Jericca Cleland
| editing        = John Carnochan James Cooper
| studio         = Aardman Animations Sony Pictures Animation
| distributor    = Columbia Pictures
| released       =  
| country        = United Kingdom United States
| language       = English
| runtime        = 97 minutes 
| budget         = $100 million 
| gross          = $147,419,472   
}} American 3-D computer animated Christmas comedy film, produced by Aardman Animations and Sony Pictures Animation as their first collaborative project. The film was released on November 11, 2011, in the UK, and on November 23, 2011, in the USA. 
 Sarah Smith, Christmas Elf obsessed with wrapping gifts for children, and a team of eight strong, magical yet untrained reindeer.

Arthur Christmas was very well received by critics, who praised its animation and humorous, smart and heart-warming story.  The film earned $147 million at the box office on a $100 million budget. 

==Plot== Christmas elves panophobic younger brother Arthur answers the letters to Santa. During a delivery operation, when a child wakes up and almost sees Santa, a Christmas elf back in the S-1 inadvertently presses a button, causing a present to fall off a conveyor and go unnoticed.

Having completed his 70th mission, Santa is portrayed as far past his prime and whose role in field operations now is largely symbolic. Nonetheless, he is held in high esteem, and delivers a congratulatory speech to the enraptured elves. Much to Steves frustration, who has long anticipated succeeding his father, Santa announces he looks forward to his 71st. During their family Christmas dinner, Arthurs suggestion for the family to play a board game degenerates into a petty quarrel between Santa and Steve, while Grand-Santa, bored by retirement, resentfully criticizes their over-modernization. Distraught, the various family members leave the dinner table. When Arthur humbly compliments Steve that he believes he will be a great Santa Claus, Steve rudely dismisses Arthurs overture. Later, their father shares with Mrs. Claus his grave doubts about his self-identity should he retire.

Meanwhile, a Christmas elf named Bryony finds the missed present—a wrapped bicycle that has yet to be delivered—and alerts Steve and Peter to the problem. Arthur is alarmed when he recognizes the present as a gift for Gwen, a little girl whose letter he had personally replied to. Arthur alerts his father, who is at a loss as to how to handle the situation; Steve argues that one missed present out of billions is an acceptable error whose correction can wait a few days. Grand-Santa on the other hand, apparently learning of the dire situation, proposes delivering the gift using Eve (mispronounced as "evie"), his old wooden sleigh, and the great-great-grandchildren of the original eight reindeer, forcefully whisking away a reluctant Arthur and a stowaway Bryony. They get lost in three different continents, lose several of their reindeer, and land in danger several times, ultimately being mistaken for aliens and causing an international military incident. Through all this, Arthur eventually learns, to his compounding disappointment, that Grand-Santas true motive is to fulfill his ego, that Steve refuses to help them out of petty resentment and possibility of his brother being made hero overshadowing his work, and that his own father has gone to bed, apparently content even though a present was not delivered.

Finally, stranded in Cuba after losing the sleigh and the remaining reindeer, Arthur renews his sense of purpose—that it all comes down to having presents delivered, regardless of how it is done and who did it—and with Grand-Santas and Bryonys help manages to recover the sleigh. Meanwhile, the elves grow increasingly alarmed at rumors of this neglected delivery and the Clauses unthinkable indifference, sending them into a panic. In response, Santa, Mrs. Claus, and Steve take the high-tech sleigh-craft to deliver a superior present—to the wrong child.

In the meantime, Arthur and his party lose the remaining reindeer and a US Predator drone scrambled by Chief De Silva of UNFITA intercepts and opens fire on them thinking that they were aliens, causing Arthur to bail out of the sleigh, via parachute. Ultimately with Mrs. Claus and Bryonys help, all the male Clauses arrive at Gwens house before she awakens, only to have all but Arthur quarrel about who gets to actually place the gift. Noticing that only Arthur truly cares about the girls feelings, the elder Clauses collectively realize that he is the sole worthy successor. As a result, Santa gives Arthur the honor and Steve, upon learning of his own errors and acknowledges that his brother is the worthy Santa instead of him, forfeits his supposed birthright to his brother. In a fitting conclusion, Gwen glimpses a snow-bearded Arthur in a wind-buffeted sweater just before vanishing up into the S-1.

With the crisis resolved, Santa goes into a happy retirement with Mrs. Claus; he also becomes Grand-Santas much-desired new companion and plays Arthurs board game with him for many happy hours. Meanwhile, Steve finds true contentment as Chief Operating Officer while Bryony is promoted to Vice-President of Packing, Pacific Division. In a nod to traditionalism once neglected, the high-tech S-1 is re-christened EVIE in honor of Grand-Santas old sleigh and refitted to be pulled by a team of five thousand reindeer—led by the original eight, all of whom managed to return safely via innate navigational abilities. Finally, Arthur happily guides the entire enterprise in the proper spirit as the new Santa.

==Cast==
* James McAvoy as Arthur Claus, the good-natured but clumsy younger son of Malcolm and Margaret who works in the mail room.
* Hugh Laurie as Steven "Steve" Claus, Malcom and Margarets elder son and Arthurs cool and incredibly capable, but cynical, older brother.
* Bill Nighy as Grand-Santa, Malcoms 136 year old grumpy father and grandfather of Steve and Arthur who dislikes the modern world. Malcolm "Santa" Claus, the affable but incompetent man in charge at the North Pole, Grand-Santas son, Margarets husband, and Steves and Arthurs father.
* Imelda Staunton as Margaret Claus, Malcoms dedicated and talented wife, and Steves and Arthurs mother. 
* Ashley Jensen as Bryony, the enthusiastic Christmas Elf from the Giftwrap Battalion.
* Marc Wootton as Peter, Steves assistant Christmas Elf.
* Laura Linney as North Pole Computer
* Eva Longoria as Chief De Silva, the head of UNFITA (United Northern Federal International Treaty Alliance) 
* Ramona Marquez as Gwen Hines, the girl whose present Arthur must deliver.
* Michael Palin as Ernie Clicker, the elderly elf and former head of Polar communications for 46 missions during Grand-Santas time as Santa Claus.   

Lead elves voiced by Sanjeev Bhaskar, Robbie Coltrane, Joan Cusack, Rhys Darby, Jane Horrocks, Iain McKee, Andy Serkis, and Dominic West.

==Production==
Arthur Christmas was first announced in 2007, under the name Operation Rudolph.   It was the first film made by Aardman in partnership with Sony Pictures Entertainment and its subsidiaries. 
 Aardman and Sony Pictures Imageworks working together on animation. 
 Adam Cohen were going to compose the score. 

==Release==
The film was released on November 11, 2011 in the United Kingdom and on November 23, 2011 in the United States.   

===Home media===
Arthur Christmas was released on DVD, Blu-ray and Blu-ray 3D on November 6, 2012, in the US,  and November 19, 2012 in the UK. 

==Reception==

===Critical response===
Arthur Christmas has received positive reviews, praising its fresh take on the Christmas premise. Review aggregator,  , another review aggregator, the film holds a score of 69 based on 32 reviews. 

John Anderson from   also wrote a positive review, saying that it is "unexpectedly fresh, despite the familiar-sounding premise".  Neil Genzlinger of The New York Times wrote that "the plot may be a little too cluttered for the toddler crowd to follow, but the next age group up should be amused, and the script by Peter Baynham and Sarah Smith has plenty of sly jokes for grown-ups."  One of the few negative reviews came from Rene Rodriguez of The Miami Herald, who thought that "the movie fails utterly at coming up with a story that merits all the eye candy." 

===Box office===
Arthur Christmas has earned $46,462,469 in North America,  $33,334,089 in the UK,  and $67,622,914 in other countries, for a worldwide total of $147,419,472. 

In the United Kingdom the film opened in second place with a £2.5 million weekend gross, behind Immortals (2011 film)|Immortals. It topped the box office in its fourth week, by which time the cumulative gross was £11.5 million. The film returned to the top of the box office on week seven, during Christmas Week, grossing £2.05m and a total of £19.7m. 

In the United States and Canada the film earned $2.4 million on its opening day and $1.8 million on Thanksgiving Day. It would go on to gross $12.1 million over the three-day weekend and $16.3 million over the five-day. This was on par with studio expectations. The film went on to gross nearly $50 million domestically against a $100 million budget.  

===Accolades===
{| class="wikitable"
|-

! Award !! Category !! Recipient !! Result
|- Alliance of Women Film Journalists 
| Animated Film
|
| 
|- Annie Awards  Best Animated Feature
|
| 
|- Character Design in a Feature Production Peter de Sève
| 
|- Storyboarding in a Feature Production Kris Pearn
| 
|- Voice Acting in a Feature Production Ashley Jensen
| 
|- Bill Nighy
| 
|- Writing in a Feature Production Sarah Smith, Peter Baynham
| 
|- British Academy of Film and Television Arts  Animated Film
|
| 
|- Broadcast Film Critics Association Awards  Best Animated Feature
|
| 
|- Chicago Film Critics Association  Animated Film
|
| 
|- Golden Globe Award  Best Animated Feature Film
|
| 
|- Online Film Critics Society   Best Animated Feature
|
| 
|- Satellite Awards  Motion Picture, Animated or Mixed Media
|
|  
|- San Diego Film Critics Society  Best Animated Film
|
|  
|- Visual Effects Society  Outstanding Visual Effects in an Animated Feature Motion Picture Doug Ikeler, Chris Juen, Alan Short, Mandy Tankenson
| 
|- Outstanding Virtual Cinematography in an Animated Feature Motion Picture Michael Ford, David Morehead, Emi Tahira
| 
|- Women Film Critics Circle  Best Animated Females
|
| 
|}

==Soundtrack==
{{Infobox album  
| Name     = Arthur Christmas: Original Motion Picture Soundtrack
| Cover    = 
| Type     = Film
| Artist   = Harry Gregson-Williams
| Released = November 14, 2011
| Recorded =
| Genre    = Soundtrack
| Length   = 46:50
| Label    = Sony Classical, Madison Gate Records
| Producer =
| Reviews  =
}}

Arthur Christmas: Original Motion Picture Soundtrack is the soundtrack to the film of the same name. It was composed by Harry Gregson-Williams and released on November 14, 2011 by Sony Classical. 

The music video for Justin Biebers song Santa Claus is Coming to Town, which plays over the end credits, was exclusively shown in theatres before the film. 

===Track listing===
{{Tracklist
| total_length      = 46:50
| title1            = Trelew, Cornwall, England
| length1           = 1:48
| title2            = Operation Christmas
| length2           = 4:54
| title3            = Waker!
| length3           = 2:51
| title4            = Mission Control
| length4           = 2:55
| title5            = One Missed Child
| length5           = 3:00
| title6            = Bring Them Home
| length6           = 1:43
| title7            = Dash Away
| length7           = 3:46
| title8            = Paris Zoo?
| length8           = 2:29
| title9            = The Wrong Trelew
| length9           = 1:54
| title10           = Race to Gwens House
| length10          = 2:09
| title11           = Arthurs Sadness
| length11          = 2:22
| title12           = Serengeti Escape
| length12          = 2:24
| title13           = Worry Me!
| length13          = 1:37
| title14           = Space Travel
| length14          = 2:48
| title15           = Goodbye Evie
| length15          = 2:48
| title16           = Christmas Morning
| length16          = 4:00
| title17           = We Wish You A...
| length17          = 0:48
| title18           = Make Someone Happy
| note18            = Performed by Bill Nighy
| length18          = 2:34
}}

==Video game==
An   platform.  Released as a free and a premium version, the game allows players to play as delivery elves, who must quickly and quietly deliver gifts to children. Another iOS app based on the film is Arthur Christmas Movie Storybook, which was released on 30 November 2011. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 