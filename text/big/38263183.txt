PK (film)
 
 
{{Infobox film
| name           = PK
| image          = PK Theatrical Poster.jpg
| border         = yes
| alt            = Khan, as the title character PK, stands nude on the railroad tracks, looking into the camera while obscuring his genitals with a transistor radio.
| caption        = Theatrical release poster
| director       = Rajkumar Hirani
| producer       =  
| writer         =  
| starring       =  
| narrator       = Anushka Sharma
| music          = Songs: Shantanu Moitra Ajay-Atul  Ankit Tiwari  Background Score: Sanjay Wandrekar  Atul Raninga
| cinematography = C. K. Muraleedharan
| editing        = Rajkumar Hirani
| studio         =  
| distributor    = UTV Motion Pictures
| released       =  
| runtime        = 152 minutes 
| country        = India
| language       = Hindi
| budget         =   
| gross          = est.    –   
}}
 satirical science fiction comedy film.    The film was directed by Rajkumar Hirani, produced by Hirani and Vidhu Vinod Chopra, and written by Hirani and Abhijat Joshi.  The film stars Aamir Khan in the title role with Anushka Sharma, Sushant Singh Rajput, Boman Irani, Saurabh Shukla, and Sanjay Dutt in supporting roles. It tells the story of an alien who comes to Earth on a research mission. He befriends a television journalist and questions religious dogmas and superstitions.
 INR 6 billion worldwide.     

==Plot==
  alien (Aamir godman Tapasvi Maharaj (Saurabh Shukla) who predicts that Sarfaraz will betray Jaggu. Determined to prove them wrong, Jaggu proposes to Sarfaraz. She is heart-broken at the wedding chapel when she receives a letter calling off the marriage due to their differences. She returns to India where she becomes a television reporter.
 absorbing their memories through touch, but they chase him away when he tries, (wrongly) interpreting him to be a pervert. A passenger vehicle carrying bandmaster Bhairon Singh (Sanjay Dutt) strikes him and renders him unconscious. Bhairon takes him to a doctor who declares the alien to be suffering from amnesia and assumes that the amnesia is the result of the collision. Bhairon decides to take the alien along with his troop, befriending the alien over time. Interpreting the aliens attempts at hand-grabbing as sexual interest, he takes him to a brothel. There, the alien holds a prostitutes (Reema Debnath) hand for six hours and thus learns the Bhojpuri language.
 intoxicated or "tipsy" (tipsy translates to pee-kay in Hindi) and call him PK, a name he adopts. People tell him that only God can help him find his remote. PK then begins his search to find God, but is confused by Indias diverse religions and their confusing traditions. He later discovers that Tapasvi has his remote. However, Tapasvi falsely claims to have attained the object from God in the Himalayas and refuses to return it to PK. After hearing PKs story, Jaggu devises a plan to expose Tapasvi and recover PKs remote.

Bewildered, PK concludes that Tapasvi and other religious heads must be dialing a "wrong number" to communicate with God. As a result, spreading misunderstandings and advising the public to engage in meaningless rituals. Jaggu encourages thousands of people to send in videos of their own experiences with religious heads to her news channel calling them "wrong numbers". Meanwhile, PK contacts Bhairon who says he will come to Delhi with the thief who stole PKs remote, but dies in a bomb blast. After massive public appeals, Tapasvi is forced to come into the studio and confront PK on-air. Tapasvi claims he has a direct connection to God and refers to his prediction of Sarfarazs betrayal as proof. However, PK, who had earlier absorbed Jaggus memories, discovers that Sarfaraz did not write the letter she received. Jaggu contacts the Pakistan Embassy in Belgium where Sarfaraz worked part-time; the embassy tells her that Sarfaraz still loves her and calls them daily to inquire whether she has called. Jaggu and Sarfaraz reconnect and Tapasvi, exposed as a fraud, is forced to return PKs device.

In the course of the film, PK falls in love with Jaggu but refrains from telling her because she loves Sarfaraz. He records her voice and fills his suitcases with batteries so that he can listen on his home planet. Jaggu, despite knowing the truth, keeps quiet. She later publishes a book about PK.

One year later, PK returns to Earth on a new research mission with several other aliens.

==Cast==
{{columns-list|2|
* Aamir Khan as PK
* Anushka Sharma as Jagat "Jaggu" Janani Sahni
* Sushant Singh Rajput as Sarfaraz Yousuf
* Boman Irani as Cherry Bajwa
* Saurabh Shukla as Tapasvi Maharaj
* Sanjay Dutt as Bhairon Singh
* Parikshit Sahni as Jayprakash Sahni
* Sachin Parikh as Tapasvis assistant
* Ram Sethi as an old man
* Reema Debnath as Phuljhadiya
* Rohitash Gaud as a police inspector. 
* Rukhsaar Rehman as Pakistan embassy receptionist
* Brijendra Kala as idol seller at a temple
* Ranbir Kapoor as an alien (cameo)
}}

==Production==

===Filming=== FIR was lodged against makers of the film for allegedly hurting religious sentiments in October 2013.  The objection was regarding a scene in the film where a man dressed as the Hindu deity, Lord Shiva pulls the rickshaw with two burqa clad women as passengers. 

==Themes==
Firstpost compared PK to Hirani and Khans previous collaboration 3 Idiots (2009), as they both involve "A socially awkward and different young man – who walks and talks in a strange, enthusiastic childlike manner – observes the system, questions it, asks you to look at the many ludicrous things that inform it, and eventually brings about a minor revolution." 

==Release==
 Bollywood release in 2014.   The films release was later expanded to 6000 screens worldwide including 844 screens overseas.  PK was initially released in 4844 screens worldwide.     PK has been made tax-free in Uttar Pradesh and Bihar.   

===Marketing=== motion poster was also released on the same day on YouTube.  The second poster was launched on 20 August 2014 at an event in Mumbai.   This poster was also released as a motion poster on YouTube. The new poster featured Aamir Khan wearing traditional Rajasthani attire, also sporting dark glasses and holding onto a brass tenor. The promotional strategy for the movie was revealed by the makers at the launch of the second poster.

The third motion poster introduced Sanjay Dutt as Bhairon Singh. The fourth motion poster featuring Anushka Sharma along with Aamir Khan released on YouTube 16 October 2014. The poster introduced Anushka Sharma as Jagat Janani  Aamir Khan stated that PK will have a poster campaign in which a new poster would be introduced every two to three weeks. By the time of release, 8 to 10 posters would have been launched.   The teaser released on 23 October. 
 transistor covering PIL was Supreme Court dismissed the plea and gave the movie a green signal.  

A case was lodged against Aamir Khan and Rajkumar Hirani in Rajasthan.  

The films final scene shows PK returns to earth with another alien (Ranbir Kapoor). Talking about the ending, Khan said, "May be, Rajkumar Hirani is thinking of getting both Ranbir and me in the sequel."    In an interview with Hindustan Times the writer Abhijat Joshi expressed interest in working on a sequel to PK. 

Kamal Haasan is reported to feature in PK remake in Tamil and Telugu.  

===Distribution=== DNA reported "Being the first to use Rentrak, Aamir Khan has taken the first initiative to bring in a paradigm shift from the usual norms followed in Bollywood. He had clearly enforced the idea of Rentrak to be brought in to bring more accuracy when it comes to box office figures." 

==Soundtrack==
{{Infobox album
| Name       = PK
| Type       = Soundtrack
| Artist     =  
| Cover      = 
| Released   =  
| Recorded   =
| Length     =
 
| Label    = T-Series Hindi
| Producer   =
| Chronology = Shantanu Moitra
| Last album = Mardaani (2014)
| This album = PK (2014)
| Next album =
 {{Extra chronology
 | Artist = Ajay-Atul
 | Type = Soundtrack
 | Last album = Lai Bhaari (2014)
 | This album = PK (2014)
 | Next album = Shuddhi (2015)
}}
{{Extra chronology
 | Artist = Ankit Tiwari
 | Type = Soundtrack
 | Last album = Singham Returns (2014)
 | This album = PK (2014)
 | Next album = Alone (2015 film)|Alone (2015)
}}
{{Singles
  | Name           = PK
  | Type           = Soundtrack
  | Single 1       = Tharki Chokro
  | Single 1 date  = 8 November 2014 
  | Single 2       = Love Is a Waste of Time
  | Single 2 date  = 14 November 2014 
 }}
}}

The films soundtrack is composed by Shantanu Moitra, Ajay-Atul and Ankit Tiwari with lyrics written by Swanand Kirkire, Amitabh Varma and Manoj Muntashir. The full soundtrack was released on 17 November 2014.

The song "Tharki Chokro" is the first single, released on 8 November 2014. The video focuses on Aamir Khan and Sanjay Dutt,  with the song sung by Swaroop Khan and composed by Ajay-Atul with lyrics penned by Swanand Kirkire. 
 Shaan and Shreya Ghoshal, composed by Shantanu Moitra with lyrics by Swanand Kirkire.

=== Track listing ===
{{track listing
| headline =
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length =  
| music_credits = yes
| title1 = Tharki Chokro
| extra1 = Swaroop Khan
| music1 = Ajay-Atul
| lyrics1 = Swanand Kirkire
| length1 = 4:55
| title2 = Love Is a Waste of Time
| extra2 =  Sonu Nigam, Shreya Ghoshal
| music2 = Shantanu Moitra
| lyrics2 = Amitabh Varma
| length2 = 4:30
| title3 = Nanga Punga Dost
| extra3 = Shreya Ghoshal
| lyrics3 = Swanand Kirkire
| music3 = Shantanu Moitra
| length3 = 4:46
| title4 = Chaar Kadam
| extra4 = Shaan (singer)|Shaan, Shreya Ghoshal
| lyrics4 = Swanand Kirkire
| music4 = Shantanu Moitra
| length4 = 4:20
| title5 = Bhagwan Hai Kahan Re Tu
| extra5 = Sonu Nigam
| lyrics5 = Swanand Kirkire
| music5 = Shantanu Moitra
| length5 = 5:12
| title6 = Dil Darbadar
| extra6 = Ankit Tiwari
| lyrics6 = Manoj Muntashir
| music6 = Ankit Tiwari
| length6 = 5:31
| title7 = PK Dance Theme
| extra7 = Instrumental
| lyrics7 =
| music7 = Shantanu Moitra
| length7 = 2:23
}}

==Reception==

Srijana Mitra Das of The Times of India gave the movie 4 stars.  Rajeev Masand of CNN-IBN gave 3.5 stars to the movie quoting "Its a courageous film that sticks to Hiranis well-oiled formula".  NDTV gave the movie 5 stars calling it "PK is a winner all the way, a film that Raj Kapoor, Bimal Roy and Guru Dutt would have been proud of had they been alive. Rajkumar Hirani is without a doubt their most worthy standard-bearer." 
Bollywood Hungama described the film "a solid entertainer that will surely entertain the masses and classes alike" and gave it 4.5 out of 5 stars.  Raja Sen of Rediff gave PK 4 out of 5 stars and called it "a triumph" and argued that Aamir "soars high".  However, Sukanya Verma of the same publication called the film "a mixed bag of spunk and sentimentality", while still giving it 3.5 out of 5 stars.  Rohit Vats of Hindustan Times gave the film 4 out of 5 stars and said "Aamir Khan steals the show with his performance".  Rohit Khilnani of India Today gave 4.5 stars, and said "Go watch the film & watch it ASAP!" 

Activists of pro-Hindu organizations Vishwa Hindu Parishad and Bajrang Dal protested against certain scenes in the film, which they considered to be hurtful to the religious sentiments of the Hindu community. Subsequently, some theatres were vandalised by those activists,    who demanded a ban on the movie  and a Public Interest Litigation was filed against the film for the same.     Government officials, such as the Uttar Pradesh chief minister Akhilesh Yadav and the Bihar chief minister Jitan Ram Manjhi exempted the film from entertainment tax to encourage wider viewership. 

==Box office==
 
PK opted to have its box office figures tracked by Rentrak    a United States-based company that specializes in audience measurements. PK has become the first Bollywood film to earn   nett from online bookings. 

{| class="wikitable"  style="float:right; width:45%; text-align:center;"
|+ PK worldwide collections breakdown
|-
! Territory
! Territory wise Collections break-up
|-
| rowspan="3" | India
| Nett Gross:                
|-
| Distributor share:   
|-
| Entertainment tax:      
|-
| rowspan="2" | International   (Outside India) 
| US$25.8 million(Rs 1.62&nbsp;billion)  . Box Office India. Retrieved 15 April 2015.     
|-
| US$10,532,007   (United States-Canada)         
|-
||Worldwide
|      
|}

===Domestic===
PK earned   nett on its opening day.  The film showed growth on its second day, earning around   nett.  On its third day, the film brought in   nett, bringing its weekend take to   nett. 

PK had the highest collections for a Hindi film on its first Monday, earning around   nett.  Over the next two days the film grossed   and   nett respectively, taking its total to  .  On Christmas Day the film earned approximately   nett and taking its first week total to   nett. 
 highest grossing film in India in just thirteen days.   The film set a record second week figure of  , taking its total to   nett.    PK made an all-time record in the Mumbai circuit by grossing there more than   nett. 

The film netted around   in its third weekend, reaching   nett in seventeen days.  The film went on to nett   in India and grossed   overseas for a worldwide gross of   in three weeks.   The film earned a final domestic nett of  . 

===International===
The film opened in 22 international markets during its opening weekend ( 19–21 December) and grossed $28.7 million placing it at No. 3 at the worldwide box office behind   and Gone with the Bullets.   
 highest grossing Bollywood film of all time in international markets.    

===Records===
{| class="wikitable mw-collapsible " style="width:99%;"
|+ Box Office Records made by PK
|-
! Box office record !! Record details !! Previous record holder !!  
|-
| Number of screens (Domestic)|| 5,200 screens || Dhoom 3 (2013, 4,500 screens) ||   
|-
| First week nett (India) ||   || Dhoom 3 (2013,  )(only Hindi)||   
|-
| Lifetime nett gross (India) ||   || Dhoom 3 (2013,  )||     
|-
| Worldwide gross ||   || Dhoom 3 (2013,  )||  
|-
| Highest gross (North America) || $10,532,007 || Dhoom 3 (2013, $8,090,250) || 
|-
| Highest gross (Australia) || A$1,996,986 || Dhoom 3 (2013, A$1,752,845) || 
|-
| Highest gross (New Zealand) || NZ$540,182 || Dhoom 3 (2013, NZ$530,911 ) || 
|}

==Awards==
PK received two Awards out of eight Nominations at the 60th Filmfare Awards.  The film won five Star Guild Awards,   and two Life OK Screen Awards. 

==See also==
 
* List of highest-grossing Indian films – other high-grossing films

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 