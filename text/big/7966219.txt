Scooby-Doo! and the Loch Ness Monster
{{Infobox film
| name           = Scooby-Doo! and the Loch Ness Monster
| image          = Scooby-Doo and the Loch Ness Monster.jpg
| caption        = UK DVD cover
| director       = Scott Jeralds Joe Sichta
| producer       = Joe Sichta
| writer         = George Doty IV Ed Scharlach Joe Sichta Mark Turosz
| starring       = Frank Welker Casey Kasem Grey DeLisle Michael Bell Jeff Bennett John DiMaggio Phil LaMarr Sheena Easton
| music          = Thomas Chase
| cinematography = Joe Gall
| editing        = Harry Hinter
| distributor    = Warner Home Video
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
}}
Scooby-Doo! and the Loch Ness Monster is the seventh direct-to-video animated film based upon the Scooby-Doo Saturday morning cartoons. It was released on June 22, 2004, and it was produced by Warner Bros. Animation (although Warner Bros. had fully absorbed Hanna-Barbera|Hanna-Barbera Cartoons by this time, Hanna-Barbera was still credited as the copyright holder and the movie ended with an H-B logo). In it, the Mystery Inc. gang encounter the Loch Ness Monster. 

==Plot==
The Mystery, Inc. gang travel to Loch Ness in Scotland to see the famous Blake Castle, the home of Daphne Blakes Scottish ancestors as well as her cousin, Shannon. The Castle grounds are home to the first annual Highland Games, composed of many traditional Scottish sports. But when they arrive Shannon informs them that the Castle had recently been terrorized by the Loch Ness Monster. Shannon says she has seen the monster and it is indeed real, a position shared by Del Chillman, the Loch Ness Monster enthusiast and amateur Cryptozoology|cryptozoologist, and Professor Fiona Pembrooke, a scientist who has staked her whole career on proving the monster exists. Taking the opposite end of the argument are Colin and Angus Haggart, their father Lachlan, local competitors in the games, and Sir Ian Locksley, the head judge of the games (as well as director of the Scottish natural history museum). Locksley and Pembrooke share a mutual hatred for each other (she was Ians research assistant at his museum until he fired her for spending too much time on the Loch Ness Monsters trail).

That night, Scooby and Shaggy are chased by the monster, and destroy the playing field in the process. Velma discovers that the Loch Ness Monster tracks head into town instead of the loch. The next day, the gang and Shannon travel to Drumnadrochit. After enlisting the help of the Haggartys to rebuild the field, Fred, Daphne, Velma, and Shannon take Professor Pembrookes boat, filled up with out of date research equipment, to search for the Loch Ness Monster by sea, while Shaggy and Scooby take the Mystery Machine and search by land. While Shaggy is distracted, a hand switches a sign on the road leading to Shaggy getting lost. Both groups are attacked by the Loch Ness Monster, seemingly being in two places at once.

After returning the badly damaged boat back to Professor Pembrooke, the gang discovers Sir Ian has taken it upon himself to patrol the waters with a high-tech ship to prevent any further "peculiarities", as he is still not convinced of the monsters existence. On Locksleys ship, the gang and Shannon find something deep in the loch using sonar equipment. They take Locksleys midget submarine|mini-sub down to investigate. In the water, the gang is attacked by the Loch Ness Monster, but is saved by a large magnet claw on the ship (before reaching the surface, the Loch Ness Monster knocks the submarines sonar camera off of its hull). When they return to Blake Castle, they find Chillman sleeping in the Mystery Machine, who explains his van has been stolen. Later, the Loch Ness Monster chases the gang, Shannon, and Chillman into a bog, where it is revealed to simply be a canvas covering Chillman’s van. Fred deduces the Loch Ness Monster to be a decoy, and sets up a trap to catch the real one.

Fred sends Shaggy and Scooby out on the loch to act as bait, while he and Del prepare to use nets to surround the cove capture the Loch Ness Monster. A large fog appears, blocking visual contact with Shaggy and Scooby. Making matters worse, Locksley’s crew mutinies because they want to capture and sell the Loch Ness Monster, and capture Daphne and Shannon. The Loch Ness Monster attacks Shaggy and Scooby, chasing them out of the cove. Locksley’s ship attaches to the nets, dragging Chillman and Fred with it. The crew attempt to harpoon the Loch Ness Monster, but Daphne and Shannon distract them long enough to make them miss. Just as the Loch Ness Monster is about to attack Chillman and Fred, Daphne captures it by using the ship’s magnetic claw. All of a sudden, a second Loch Ness Monster appears and gives chase to Shaggy and Scooby, but falls into a previously set trap. This monster is revealed to be a huge puppet controlled by the Haggart brothers, and the one Daphne captured is revealed to be a home-made submarine operated by none other than Professor Pembrooke. Pembrooke used a secret entrance in her boat to go into the Loch Ness Monster and operate it. She also hired the Haggart brothers to man the second monster, but Angus and Colin reveal that they just wanted to do it as a prank. Velma explains that Pembrooke’s plan was to use her machine to convince Locksley the real monster existed, and enlist his aide in finding it.

The next day, the games begin on schedule; But Locksley calls everyone to his ship to look at new pictures of the monster that his mini-subs sunken (and yet obviously still working) camera had taken, at a depth well below what a ramshackle home-made submarine like Pembrookes could survive. These, plus three other photos that Pembrooke had taken several days earlier finally convince him the monster may actually exist. The film ends with the gang leaving Blake Castle, during which Velma admits that shes actually glad they didnt find out whether or not the Loch Ness Monster is real---her reason being, "Some mysteries are best left unsolved." The final scene shows Scooby briefly seeing what may possibly be the real Loch Ness Monster swimming by them in the water.

==Cast== Fred Jones, Lachlan Haggart
* Casey Kasem as Shaggy Rogers
* Mindy Cohn as Velma Dinkley
* Grey DeLisle as Daphne Blake, Shannon Blake Michael Bell as Duncan MacGubbin
* Jeff Bennett as Del Chillman, Sir Ian Locksley, Harpoon Gunner
* John DiMaggio as Colin Haggart, Volunteer #1
* Phil LaMarr as Angus Haggart, Volunteer #2
* Sheena Easton as Professor Fiona Pembrooke

==Trivia==
* This is the first Scooby-Doo direct-to-video movie that shows the Mystery Inc. gang in the same clothes they wear in Whats New, Scooby-Doo?.

* The Scooby films are now no-longer produced in the retro feel that the previous two films were, as from here until Scooby-Doo! and the Samurai Sword, the direct-to-video movies will be produced in the same animation as Whats New, Scooby-Doo?.

* This is the second Scooby-Doo direct-to-video movie that Daphne is voiced by Grey DeLisle since Scooby-Doo and the Cyber Chase and will remain as such from here on in. This is also Mindy Cohns first time voicing Velma in a direct-to-video movie.

* Del Chillman returns in Chill Out, Scooby-Doo! claiming that Nessie "was a no-show".

* Daphnes being danger-prone, a running gag in the old episodes, is revealed to be a trait that runs in her family.

* The gang discover Del sleeping in the Mystery Machine on Velmas poncho after his own van was stolen. This is the very same poncho that she wore in Scooby-Doo! and the Monster of Mexico.

* The Loch Ness Monster in this movie bears a strong resemblance to The Giant Behemoth.

* This movie is one of the few films to feature both a fake monster (actually three fakes) and a real one (the real Nessie is captured on film and seen by Scooby-Doo at the end).

* This is the third time Scooby has met the Loch Ness Monster. He also met her in Laff-A-Lympics, as the challenge was to get a picture of her, and once on The Scooby-Doo Show, in the episode "A Highland Fling with a Monstrous Thing".

* When Del Chillman first appears, he is listening to "Back on the Train" by Phish on his vans radio.
 Mir submersible(s).

* In one scene Shaggy states that the fog is thick enough to cut with a knife, in response Scooby uses one of his claws to cut out a circle in the fog and takes a bite out of, a reference to the episode of Scooby-Doo, Where Are You? called "Go Away Ghost Ship" where Shaggy makes the same comment and Scooby does the same thing in response.

==Novelization== American fantasy and science fiction author Suzanne Weyn.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 