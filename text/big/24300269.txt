Track 29
{{Infobox film
| name           = Track 29
| image          = Track 29.jpg
| image_size     = 220
| alt            =
| caption        = Movie Poster
| director       = Nicolas Roeg 
| producer       = George Harrison Rick McCallum
| writer         = Dennis Potter
| narrator       =
| starring       = Theresa Russell Gary Oldman Christopher Lloyd Colleen Camp Sandra Bernhard Seymour Cassel
| music          = Stanley Myers Alex Thomson
| editing        = Tony Lawson
| studio         = HandMade Films Island Pictures
| released       = 5 August 1988 (UK) 9 September 1988 (USA)
| runtime        = 91 min.
| country        = United Kingdom United States English
| budget         = $5,000,000 (estimated)
| gross          = $429,028 (USA)
| preceded_by    =
| followed_by    =
}}
 1988 film Wilmington and Wrightsville Beach, North Carolina

==Plot==
The wife of a small town doctor tires of his spending too much time playing with his model trains, and starts thinking about the son she gave up for adoption years before. While dining at a cafe, she meets a British hitchhiker, who believes he is her son. Years earlier, she was raped and gave up her son for adoption.  The son may be a figment of her imagination.  They start to get to know each other and the son starts to hate the husband. The wife begins to fear for her husbands safety.

==Main cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| Theresa Russell || Linda Henry
|-
| Gary Oldman || Martin
|-
| Christopher Lloyd || Henry Henry
|-
| Colleen Camp || Arlanda
|-
| Sandra Bernhard || Nurse Stein
|-
| Seymour Cassel || Dr. Bernard Fairmont
|}

==Critical reception==
Janet Maslin of The New York Times thought the film missed the mark:
 

However, Roger Ebert of the Chicago Sun-Times rated it 3 stars out of his 4 star rating system and found the film well done but painful:
 

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 