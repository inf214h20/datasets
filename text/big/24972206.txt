Like Asura
{{Infobox film
| name = Like Asura
| image =
| image_size =
| caption =
| director = Yoshimitsu Morita
| producer =
| writer =
| starring = Shinobu Otake Eri Fukatsu Kaoru Yachigusa Shidou Nakamura
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 135 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}} Japan Academy Prize it won three awards and received ten other nominations.

== Cast ==
* Shinobu Otake
* Eri Fukatsu
* Kaoru Yachigusa
* Shidou Nakamura

== Awards and nominations == Japan Academy Prize.   Best Director - Yoshimitsu Morita Best Screenplay - Tomomi Tsutsui
*Won: Best Actress in a Supporting Role - Eri Fukatsu
*Nominated: Best Picture Best Actress - Shinobu Otake
*Nominated: Best Actress in a Supporting Role - Kaoru Yachigusa
*Nominated: Best Actor in a Supporting Role - Shidou Nakamura
*Nominated: Best Music - Michiru Oshima
*Nominated: Best Cinematography - Nobuyasu Kita
*Nominated: Best Lighting Direction - Koichi Watanabe
*Nominated: Best Art Direction - Hidemitsu Yamasaki
*Nominated: Best Sound Recording - Fumio Hashimoto
*Nominated: Best Film Editing - Shinji Tanaka

== References ==
 

== External links ==
*  

 

 
 
 
 

 