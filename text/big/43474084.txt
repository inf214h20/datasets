Ehsaas: The Feeling
{{Infobox film
| name           = Ehsaas: The Feeling
| image          =
| caption        =
| director       = Mahesh Manjrekar
| producer       = Pravin Shah
| writer         = Mahesh Manjrekar
| starring       = Sunil Shetty Shabana Raza
| music          = Anand Raj Anand Milind Saagar Prajwal Anand
| cinematography = Vijay Arora
| editing        = V. N. Mayekar
| distributor    =
| released       = 30 Nov 2001
| runtime        = 117 mins
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
Ehsaas: The Feeling 2001  is a Bollywood film. It Stars Sunil Shetty and Shabana Raza. 

==Plot==
After the death of his wife, Aditi, Ravi Naik (Sunil Shetty) must look after his only child - a son - on his own. He brings up his son, Rohan (Mayank Tandon), in a very strict and disciplined atmosphere, so much so that his son starts to resent him and his mannerism. His immediate neighbors, Antra Pandit (Shabana Raza) and her mom (Kirron Kher) too are critical of the way Ravi handles his son, and suggest that he adapt a more lenient view. But Ravi ignores this advise. All he wants is to get Rohan excel as an athlete and win the forthcoming athletics event.

==Cast==
* Sunil Shetty as Ravi Naik
* Shabana Raza as Antra Pandit 
* Kiron Kher as Antra mother in law
* Mahesh Manjrekar as Michael 
* Rakhi Sawant as Maria
* Mayank Tandon as Rohan Naik 
* Shivaji Satam
* Shakti Kapoor as Principal 
* Kishore Nandlaskar
* Anand Raj Anand 
* Sunidhi Chauhan
* Sanjay Narvekar

==References==
 

== External links ==
*  

 
 
 

 