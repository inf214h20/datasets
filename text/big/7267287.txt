Changing Husbands
{{infobox film
| name           = Changing Husbands
| image          =
| imagesize      =
| caption        =
| director       = Paul Iribe Frank Urson Rupert Julian(uncredited)
| producer       = Adolph Zukor Jesse Lasky
| writer         = Sada Cowan Howard Higgins
| based on       =  
| starring       = Leatrice Joy
| music          =
| cinematography = Bert Glennon
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (6,799 feet)
| country        = USA
| language       = Silent (English intertitles)
}} silent comedy starring Leatrice Joy, and Victor Varconi, directed by Frank Urson and written by Sada Cowan and Howard Higgin.  The runtime is 70 minutes. It is preserved in the Library of Congress collection.   

==Cast==
*Leatrice Joy as Gwynne Evans/Ava Graham 
*Victor Varconi as Oliver Evans 
*Raymond Griffith as Bob Hamilton 
*Julia Faye as Mitzi 
*Zasu Pitts as Delia 
*Helen Dunbar as Mrs. Evans Sr  William Boyd as Conrad Bardshaw

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 