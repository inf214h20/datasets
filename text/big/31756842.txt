Elena Undone
 
{{Infobox film
| name           = Elena Undone
| image          = Elena undone.jpg
| caption        = 
| director       = Nicole Conn
| producer       = Jane Clark JD Disalvatore
| screenplay     = 
| story          = Nicole Conn
| based on       = 
| starring       = Necar Zadegan Traci Dinwiddie Gary Weeks
| music          = Mark Chait
| cinematography = Tal Lazar
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Elena Undone is a 2010 lesbian film written and directed by Nicole Conn.

==Plot==
The paths of Elena Winters (Necar Zadegan), a mother and pastors wife, and Peyton Lombard (Traci Dinwiddie), a well-known lesbian writer randomly cross. Instantly, they feel drawn toward one other and eventually fall in love.

The movie opens in two places - following the two protagonists - first, within a church, where Elenas son, Nash, refuses to support an anti-homosexual protest, and then within a memorial service for Peytons mother.

Elena and her husband Barry (Gary Weeks) are trying to conceive but it becomes clear that their efforts have been unsuccessful when Barry rebukes Elena after her period starts. Peyton starts cleaning out her mothers things, having flashbacks of her emotionally troubled mother as she works. Peyton finds a tape entitled Womens Glory Project with her things, and becomes obsessed with it for some time.

Elena visits her friend Tyler for advice, telling him that she and Barry are going to adopt instead of trying to have another child. He asks "whats in it for (her)," and insists (as hes apparently done in the past) that Barry is not her soulmate, or "twin flame." Even so, he pushes her to go to the adoption, saying that "someone is definitely coming into (her) life, in a big way."

Peyton and Elena meet at the adoption agency. They briefly speak as Peyton returns Elenas lost keys to her. They exchange business cards after Peyton mentions that she might have use for a photographer (which Elena was prior to marrying Barry).

Peytons best friend, Wave, drags her to a love guru (Tyler) where Peyton and Elena meet once again. Elena finds out that Peyton is gay as they talk about their marriages. Peyton calls Elena the next day, inquiring if Elena would help her with continuing her mothers Womens Glory Project, and make plans for Elena to visit the next day with her portfolio.

They meet once again, and its revealed that Peytons an agoraphobic with substantial difficulties trusting. They bond over the next few weeks - over picnics with Tyler, and late-night glasses of wine. Later, Elena takes better professional photographs of Peyton for her books, and they discuss their upbringings and how they knew they were straight and gay, respectively.

Peyton calls Wave asking for advice on her feelings for Elena, and her friend tells her to be honest with Elena about said feelings, unaware that Elena has some of her own. She visits Peyton later, after being avoided for some time, showing up at her door with the printed photographs. At the unexpected visit, Peyton discloses her feelings to Elena and suggests that they not spend time together. Elena violently rejects that idea, telling Peyton that shes her absolute best friend, that they need to be in each others lives, and that Peyton can have "all of (her) except for that (meaning physically romantic encounters)." Subsequently, however, Elena begins to rapidly examine her own feelings. Elena calls Peyton after exploring lesbian websites, saying that shes not attracted to them and doesnt think shes a lesbian, leaving the viewer aware that she is attracted to Peyton. This is followed by an uncharacteristically (up to this point) passionate statement from Elena asking if she and Peyton could agree to call her feelings a serious "crush." Her husband walks up in the middle of the call, causing her to quickly end it, which appears even more suspicious.

The following day, Elena comes over to Peytons house and makes out with her passionately. As time passes, Elena begins wanting more from her relationship with Peyton. She asks Peyton to make love to her; at first Peyton refuses, saying that "once   go there, theres no going back." Elena tells her that she doesnt want to go back. As theyre making love, Peyton asks Elena to stay with her, though its unclear whether she means physically or emotionally.

Elena and Peyton get into a bit of an inquisition when Peyton asks how Barry comes on to Elena and when and how often they have sex. Elena tells her that they dont have sex anymore, and also tells her that theyre going on an annual vacation to Hawaii together. Peyton writes intimate letters to Elena as shes away, and Nash is the one that finds them as hes looking for some aspirin in her suitcase.

The toll of having two partners starts to eat away at Elena, and she begins to get in more fights with both Barry and Peyton. Nash starts stealing and drinking from the stress of keeping Elenas affair with Peyton secret, and ends up getting caught by the police after getting drunk and returning to the store he stole from. When Elena comes back and confronts him about it, he tells her that he knows of her affair with Peyton and she explains (briefly) that she does not intend to stop seeing her.

Elena returns to Tyler out of desperation for answers as to why shes been set on this path, and meets Peyton later and kisses her in the park, oblivious to Millie seeing them together. Millie then calls Barry and tells him about Elenas affair. Peyton tells her that they should probably break up, because of all the complications arising for Elena. Elena insists that they dont, asking Peyton rhetorically if she ever saw a future of them together. They break up and Elena walks off as Millie goes to Barry and tells him about the affair to his face. Elena breaks up with Barry, telling him that Nash needs the real them, not the fighting couple that theyve become.

Six months later, Wave and Peyton are taking a walk in the park and run into Tori and Nash, also doing the same. Elena also comes up, walking with Tyler and his wife. Shes now pregnant, and at first Peyton freaks out, thinking that Elena played her and was sleeping with Barry the entire time they were together. Elena faints, and Nash gets Peyton and Elena to talk together at Tylers house so Elena can explain that she broke up and is getting divorced from Barry, but still wanted a child, to have some hope in her life and that she received IVF with Tylers contribution, and asks Peyton to try and understand. Peyton apologizes and they kiss.

The movie ends with another of Tylers picnics with Peyton and Elena together with their infant.

==Cast==
* Necar Zadegan as Elena
* Traci Dinwiddie as Peyton
* Gary Weeks as Barry
* Sam Harris as Tyler
* Connor Kramme as Nash
* Sabrina Fuster as Tori
* Mary Wells as Wave
* Erin Carufel as Millie
* Heather Howe as Lily

==Reception==
The film received mixed reviews. It is rated 58% on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 