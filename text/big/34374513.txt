Rok Sako To Rok Lo
{{Infobox film
| name           = Rok Sako To Rok Lo
| image          = Rok Sako To Rok Lo.jpg
| caption        = Film Poster
| director       = Arindam Chaudhuri
| producer       = Arindam Chaudhuri
| writer         = Arindam Chaudhuri
| narrator       =
| starring       = Yash Pandit Manjari Fadnis Sunny Deol Carran Kapur Aparna Kumar Ram Menon
| music          = Jatin-Lalit
| cinematography = Santosh Thundiyil
| editing        = Raviranjan Maitra
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
|}}
 sports drama film produced and directed by Arindam Chaudhuri, in his debut. The film stars Yash Pandit, Manjari Fadnis, Sunny Deol, Carran Kapur, Aparna Kumar and Ram Menon in the lead roles. The music was scored by Jatin Lalit. 

==Plot==
The film deals with the dispute between two neighbouring high schools - Bharti School, a peaceful and silent school and Valley High School - a violent one. Dev (Yash Pandit), Suhana (Manjari Fadnis), and their friends come from Bharti, and are often disturbed by their enemies who come from Valley High. The Valley High students also defeat the Bharti students in every single inter-school event that happens every year, which is the main reason why they make fun of Bharti, while giving it a new and insulting name. One day however, the Bharti students are saved from the Valley High students by a mysterious man Kabir Mukherjee (Sunny Deol), who they initially feared because of his rough looks, thereby the reason they always called him "Phantom". The Bharti students get close to Kabir and befriend him, and with time he also becomes more gentle and normal, thereby getting rid of his "Phantom" look.

Dev, however gets attached to Sanjana (Aparna Kumar), a girl from Valley High, which results in him dropping Suhana. But Sanjana quickly rejects him because he once made fun of her at the cafe and plus, he belongs to the rival school. One day while driving in an open road, Dev and his friends are again insulted and considered "slow"  by the Valley High students who are also driving on the same road. Dev however drives faster and overtakes them, but the car quickly meets with an accident and crashes. Everyone in the car however survives, except Kabir. This leaves the Bharti students dismayed and defenseless against the Valley High students, with whom they have an upcoming 100 metres race competition. Dev however is still determined, so he trains up and attends the race with full confidence. During the race, he gets injured by one of the runners, but doesnt lose hope and through last-minute strength, he eventually wins the race, thereby finally letting Bharti School get their revenge on Valley High School. Incidentally the prize for the winner is a bike similar to Kabirs, so Dev becomes the new "Phantom".

==Cast==
 
* Sunny Deol      - Kabir "Phantom" Mukherjee
* Yash Pandit     - Dev
* Manjari Fadnis  - Suhana
* " Namrata Shirodkar" - Sandra / Narrator
* Carran Kapur        - Ranveer Pratap Singh
* Aparna Kumar        - Sanjana
* Ram Menon
* Karun Punchi
* Abbas Saria
* Ajay Sheoparna
* Jehan Hillowalla
* Shreya Das	
* Dipti Bhatnagar	    - Devs Bhabhi
* Archana Puran Singh - Sweety
* Tinnu Anand	    - S.V.P.S, Balasubramaniam Iyer
* Rakesh Bedi         - Ghodbole
* Rajit Kapoor        - Sweetys husband
* Tiku Talsania       - Sweetys husband
* Anjaan Srivastav    - Ganguly

==Production==
Rok Sako To Rok Lo was the directorial debut for Arindam Chaudhuri. It is also said to be a rip-off of the 1992 Aamir Khan-starrer Joh Jeeta Wohi Sikander.  According to Chaudhuri, "The central theme is friendship". He also mentioned that it would show how "the principal of management can help reduce wastage of money in the film industry." The film was one out of 50 different stories which he worked on. The cast had mostly teenage debutants, while "well-known faces" like Tikku Talsania, Archana Puransingh, Anjan Srivastva, Rakesh Bedi, Tinu Anand and Rajit Kapoor played supporting roles. To choose a title for the film, Chaudhuri conducted "market research". 

==Soundtrack==
The films musical score was composed by Jatin Lalit, while the lyrics were penned by Prasoon Joshi. 

{{Track listing
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    =
| all_lyrics      = 
| title1          = Haan Mujhe Thaam Le
| extra1          = Babul Supriyo, Alka Yagnik
| length1         = 04:25

| title2          = Jaane Kise
| extra2          = Shaan (singer)|Shaan, Alka Yagnik
| length2         = 04:26

| title3          = Nazron Ka Yaarana
| extra3          = Shaan, Sunidhi Chauhan
| length3         = 05:10

| title4          = Rok Sako To Rok Lo
| extra4          = Shaan, Babul Supriyo, Shreya Ghoshal, Lalit Pandit, Ishaan
| length4         = 04:31

| title5          = Tera Gham
| extra5          = Sonu Nigam, Alka Yagnik
| length5         = 04:03

| title6          = Yaaron Sun Lo
| extra6          = Abhijeet Bhattacharya
| length6         = 05:08

| title7          = Yaaron Sun Lo (Sad)
| extra7          = Abhijeet Bhattacharya
| length7         = 01:55
}}

==Reception==
Rediff said, "Manjari has performed well, but Yash looks nervous in some scenes", and further wrote "I would recommend renting a Jo Jeeta Wohi Sikander DVD instead of spending money to watch this film." http://www.rediff.com/movies/2004/dec/10rok.htm  Subhash K. Jha of Nowrunning rated the film 2/5 and said, "Sadly, the films basic tenor is too flighty to hold up such lofty ideas. Thoughts that go beyond eye candy entertainment float in and out of the narrative without getting a chance to lodge themselves in the plot." 

==See also==
* Jo Jeeta Wohi Sikandar, an inspiration for Rok Sako To Rok Lo

==References==
 

==External links==
*  

 
 
 
 