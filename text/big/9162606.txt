O'Shaughnessy's Boy
{{Infobox film
| name           = OShaughnessys Boy
| image          =
| caption        =
| director       = Richard Boleslawski
| producer       = Phil Goldstone
| writer         = Harvey Gates Malcolm Stuart Boylan Leonard Praskins Wanda Tuchock Otis Garrett
| narrator       = Spanky McFarland
| music          = William Axt
| cinematography = James Wong Howe Frank Sullivan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 87 mins
| country        = United States English
| budget         =
}}
 1935 film starring Wallace Beery and Jackie Cooper. The movie was directed by Richard Boleslawski.

==Plot summary==
 

==Cast==
* Wallace Beery as Windy OShaughnessy
* Jackie Cooper as Stubby OShaughnessy
* George "Spanky" McFarland as Stubby OShaughnessy (child)
* Henry Stephenson as Major Bigelow
* Sara Haden as Aunt Martha Shields (billed as Sarah Haden)
* Leona Maricle as Cora OShaughnessy
* Willard Robinson as Dan Hastings
* Clarence Muse as Jeff

==References==
 	

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 


 