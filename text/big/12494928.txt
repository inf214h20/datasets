Stripped to Kill
{{Infobox film
| name           = Stripped to Kill
| image          = Stripped to kill poster.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Katt Shea
| producer       = Mark Byers executive Roger Corman
| writer         = Andy Ruben Katt Shea
| narrator       =
| starring       = Greg Evigan Kay Lenz Norman Fell Pia Kamakahi Tracy Crowder
| music          = John OKennedy
| cinematography = John LeBlanc
| editing        = Zach Staenberg Bruce Stubblefield	
| distributor    = Metro-Goldwyn-Mayer 1987
| runtime        = 88 min
| country        =   English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 1987 erotic thriller/sexploitation film, it was directed by Katt Shea, and stars Greg Evigan, Kay Lenz & Norman Fell. 

== Plot ==
The movie about a female detective who is forced to go undercover as a stripper in order to investigate a recent murder.

==Production==
The film was inspired by a visit Katt Shea and her husband and writing partner Andy Ruben made to a strip club. Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 199 

"I didnt want to go because I felt it was humiliating to women," recalls Shea. "But I finally got myself there. I sat down and began watching these acts and theyre performing as if they really cared." How Poison Ivy Got Its Sting: The studio wanted a teen-age Fatal Attraction. Katt Sheas movie may be more than that. Poison Ivy: Art or Exploitation?
By LAURIE HALPERN BENENSON. New York Times (1923-Current file)   03 May 1992: 70.  

Shea later elborated:
 Before I did STRIPPED TO KILL you had never seen a girl dancing on a pole, no one had ever seen that in a movie, to my knowledge. Girls swinging around on a pole--that had not been done yet.  So I think that was spectacular; it was crazy, it was wild. This is how it happened. I went to a strip club for the first time in my life and I saw a girl swinging around on a pole and I thought, ‘Oh my god this has got to be in a movie!’ I mean, nobody knows this goes on except a bunch of guys with dollar bills, so it just had to be exploited, I guess. I thought they were very artistic and I just loved the girls, they were real artists and they were just using this particular venue to explore their art.   accessed 21 April 2015  
She took the idea to Roger Corman for whom she had made a number of movies as an actor. Corman says he liked the basic idea but questioned the believability of a scene where a man went undercover as a stripper. Shea brought in a female impersonator to see Corman and had him describe to the producer who to pretend to be a stripper. "He   turned every shade," recalls Shea. "He was purple by the end. But then he said yes." 

== Criticism ==
Kay Lenz complained publicly about the films editing and "exploitative" ad campaign aimed at the print media. 

==Reception==
The film was a hit and led to a sequel, shot on the same set as Dance of the Damned. The sequel was also directed by Shea who took her name off because of Cormans editing interference. Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 201 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 