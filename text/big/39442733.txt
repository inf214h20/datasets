Svengali (2013 film)
 
{{Infobox film
| name           = Svengali
| image          =
| alt            =
| caption        =
| director       = John Hardwick
| producer       = Martin Root   Jonny Owen
| writer         = Jonny Owen
| starring       = Martin Freeman   Vicky McClure   Matt Berry   Michael Socha   Michael Smiley   Natasha OKeeffe   Jonny Owen
| music          = Tristin Norwell
| cinematography =
| editing        = Anthony Pants Boys   Kant Pan
| studio         = Root Films
| distributor    =
| released       =  
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
Svengali is a 2013 British film from Root Films, directed by John Hardwick and written by Jonny Owen. The film stars Martin Freeman,  Vicky McClure, Matt Berry, Michael Socha, Michael Smiley and Natasha OKeeffe.

== Synopsis ==

Svengali tells the story of  Dixie (Jonny Owen), a postman from South Wales, and a music fanatic. All his life hes dreamed of discovering a great band and then one day, trawling through YouTube, he finds them... The Premature Congratulations. He hunts them down and offers them his management services. They are young, arrogant, sexy and utterly magnificent. Putting their demo on a cassette tape, Dixie heads out onto the streets of London...
Innocent, wide-eyed Dixie embarks on a roller coaster ride through the most infamous industry of them all. His partner and his sanity through it all is his soulmate Michelle (Vicky McClure). Every day is a battle for Dixie, totally broke and working for nothing to get his beloved band gigs, their egos grow with their stardom. The more successful The Prems get, the greater the chances are of Dixie losing them.  He has to decide what are his real priorities in life… his love of music or his love for Michelle.

== Edinburgh Film Festival ==

Svengali has been selected to show at the 67th Edinburgh International Film Festival. It has also been nominated for the Michael Powell Award;   a prize which honours the best British feature film.  

== Cast ==

* Jonny Owen as Dixie
* Vicky McClure as Shell 
* Roger Evans as Horsey
* Martin Freeman as Don 
* Maxine Peake as Angie
* Matt Berry as Jeremy Braines
* Michael Socha as Tommy
* Michael Smiley as Irish Pierre
* Natasha OKeeffe as Natasha
* Morwenna Banks as Francine
* Joel Fry as Macca
* Dylan Edwards as Jake
* Jessica Ellerby as Alice
* Vauxhall Jermaine as Marcus
* Katy Brand as Katya
* Di Botcher as Mrs. Cooper
* Brian Hibbard as Dixies Dad
* Huw Stephens as Himself
* Alan McGee as Himself
* Max Rushden as Himself
* Carl Barat as Himself

== References ==
 

== External links ==

*  
*  
*  
*   at Edinburgh
*   at Edinburgh International Film Festival
*   talking about Svengali in the Big Issue

 
 
 
 
 

 
 