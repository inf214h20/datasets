The Idea (1932 film)
{{Infobox film name           = The Idea image          = Berthold Bartosch (1932) The Idea still.jpg image_size     = 250 alt            =  caption        = A woman, representing an idea, confronts a crowd of workers. director       = Berthold Bartosch writer         = Berthold Bartosch based on       =   music          = Arthur Honegger cinematography =  editing        =  studio         =  distributor    =  released       =   runtime        = 25 minutes country        = France language       = Silent budget         = 
}} the same Flemish artist Frans Masereel (1889–1972).  The film stars a naked woman, representing a thinkers idea; as she goes out into the world, the frightened authorities unsuccessfully try to cover up her nudity.  A man who stands up for her is executed, and a workers revolution she inspires is violently suppressed by big business.

Bartosch spent two years animating the film, initially in collaboration with Masereel. Bartosch used complicated techniques with multiple layers of superimposed animation to create the intricately detailed film.  The film features an electronic music score by composer Arthur Honegger (1892–1955). 

==Synopsis==

A thinker sits by a window, and an idea comes to him in the form of a doll-sized naked woman.  The thinker puts the woman in an envelope and sends her out into the world.  She finds herself in an office building, where the frightened authorities attempt to clothe her, but she soon sheds the clothing.  She becomes involved with a young working class man, and he appeals to the people on her behalf; he is captured and executed, and his coffin is carried through the streets by the people.  Another man presses her into a book, and delivers handbills of her to the frightened people.  She is captured by a businessman, and armed soldiers are sent to put down a revolution of the people; the people are suppressed, and the woman, now white-haired, becomes a star and drifts into the cosmos. 

==Production history==

 , an electronic instrument used in the score; likely the first instance of electronic music used in film]]
 The Idea (1919), but Masereel backed out of the production before it was finished.  In the end of Masereels version, the woman returns to the thinker; Bartoschs film ends with the defeat of the woman. 

Bartosch made the film over two years in a space over the Vieux Colombier Theatre.   The film required 45,000 frames of up to four levels of animation each, and up to eighteen camera superimpositions.  Bartosch combined drawings with hinged cardboard cutout characters.   Arthur Honegger provided the soundtrack; he used an ondes Martenot in what is likely the first instance of electronic music used in film. 

==Score==

The score was by composer Arthur Honegger in ten parts: 

 

#  ;
#  ;
#  ;
#  ;
#  ;
#  
#  ;
#  ;
#  ;
#  .

 

==Reception==

Film historian William Moritz called The Idea "the first   film created as an artwork with serious, even tragic, social and philosophical themes".   Historian Perry Willett wrote that the film is at times unclear, and was "something of a disappointment". 

==References==

 

===Works cited===

 

* {{cite book ref       = harv last      = Neupert first     = Richard title     = History of Animated Cinema url       = http://books.google.com/books?id=AvD7olaX74sC year      = 2011 publisher = John Wiley & Sons isbn      = 978-1-4443-9257-9}}
* {{cite book ref       = harv last      = Spratt first     = Geoffrey title     = The Music of Arthur Honegger url       = http://books.google.com/books?id=tmt3F29LYzEC year      = 1987 publisher = Cork University Press isbn      = 978-0-902561-34-2}}
* {{cite book ref       = harv last      = Wells first     = Paul title     = Animation: Genre and Authorship url       = http://books.google.com/books?id=Nj3hwD1Sk-QC year      = 2002 publisher = Wallflower Press isbn      = 978-1-903364-20-8}}
* {{cite book ref          = harv
|editor-last  = Donahue
|editor-first = Neil H. last         = Willett first        = Perry title        = A Companion to the Literature of German Expressionism url          = http://books.google.com/books?id=zjvV48n-ngUC&pg=PA111 year         = 2005 publisher    = Camden House isbn         = 978-1-57113-175-1 pages        = 111–134 chapter      = The Cutting Edge of German Expressionism: The Woodcut Novel of Frans Masereel and Its Influences}}
 

==External links==
* 

 
 
 