The Waiting City
 
The Waiting City is a 2009 Australian film directed by Claire McCarthy. 

The Waiting City stars Joel Edgerton and Radha Mitchell, and was an official selection at the 2009 Toronto International Film Festival.

==Synopsis==
Fiona (Radha Mitchell) and Ben (Joel Edgerton) are a couple from Australia—she is a successful, self-starting lawyer while he is a relaxed, easy-going musician. Fiona and Ben, who want to adopt a child, travel to India for what they imagine will be a quick and simple process. However, once they arrive in Calcutta (now Kolkata) they learn that little happens quickly in India, and for all the promises that have been made, they begin to doubt whether they will be able to complete the final stages of the adoption process.

While Fiona stays in touch with her clients at home via the internet and deals with the legal red tape slowing down the adoption, Ben finds himself wandering the streets of Calcutta and adjusting to the rhythms of the city. The stress of the waiting period seems to reinforce the differences between Fiona and Ben, and tension begins to grow into anger and resentment; adding to Bens dissatisfaction is his budding friendship with Scarlett (Isabel Lucas), an attractive fellow visitor who seems more compatible with his attitudes than his wife.

Fiona, who is an atheist finds herself at odds with the Indian people who are driven by religious tradition. As her stay in India progresses, the various unfamiliar sights evoke spiritual feelings in her. When Ben and Fiona finally go to the orphanage to meet their would-be daughter Lakshmi, they discover that she is chronically ill and likely unable to survive a flight to Sydney. Fiona and Ben are devastated when Lakshmi dies a few days later. Having lost all that they meant to find in India, the lovers decide once again that they need each other and engage in reconciliation.

==See also==
*Cinema of Australia

==References==
 

==External links==
* 

 
 
 
 


 