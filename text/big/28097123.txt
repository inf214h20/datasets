Vision – From the Life of Hildegard von Bingen
 
{{Infobox film
| name           = Vision - From the Life of Hildegard von Bingen
| image          = VISION - FROM THE LIFE OF HILDEGARD VON BINGEN Theatrical Poster - from Commons.jpg
| caption        = Theatrical release poster
| director       = Margarethe von Trotta
| producer       = Markus Zimmer, Hengameh Panahi, Richard Bolzline, Manfred Thurau
| writer         = Margarethe von Trotta
| narrator       =
| starring       = Barbara Sukowa   Heino Ferch   Hannah Herzsprung
| music          = Chris Heyne
| with original compositions by  = Hildegard von Bingen
| cinematography = Axel Block
| editing        = Corina Dietz
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 111 minutes
| country        = Germany
| language       = German  English
}}
Vision (original title: Aus dem Leben der Hildegard von Bingen) is a 2009 German film directed by Margarethe von Trotta.

==Plot==
In Vision, New German Cinema auteur Margarethe von Trotta (Marianne and Julianne, Rosa Luxemburg and Rosentrasse) tells the story of Hildegard of Bingen (Barbara Sukowa) the famed 12th century Benedictine nun, Christian mystic, composer, philosopher, playwright, physician, poet, naturalist, scientist and ecological activist.  Hildegard was a multi-talented, fully grounded, highly intelligent woman who was forced to hide her light.  The modern worlds first female rebel who re-transmitted her visions to the world for the greater glory of God and mankind. Regine Pernoud has called Hildegard “the inspired conscience of the 12th century,” the “Prophetissa Teutonica” and the “Jewel of Bingen.”  Pope John Paul II has called her “a light to her people and for her time, she continues to shine even more brightly today.” 

==Cast==
* Barbara Sukowa as Hildegard of Bingen
** Stella Holzapfel as Hildegard as a Child
* Heino Ferch as Brother Volmar
* Hannah Herzsprung as Sister Richardis
* im]]
* Sunnyi Melles as Marchioness Richardis von Stade
* Alexander Held as Abbot Kuno
* Lena Stolze as Sister Jutta
* Paula Kalenberg as Sister Clara
* Annemarie Duringer as Abbess Tengwich
* Devid Striesow as Emperor Frederick Barbarossa

==Production==
Integrally involved with the 1970s Women’s Movement, filmmaker Margarethe von Trotta has always been drawn to women whose story has been marginalized over time.  Von Trotta and others re-found Hildegard von Bingen in their search for historically forgotten (or misremembered) women.  While writing the screenplay for her 1983 film Rosa Luxemburg, von Trotta’s interest in Hildegard re-emerged and she wondered whether Hildegard’s life would be good material for a movie.  After writing a few scenes, von Trotta felt the film had a powerful message and potential resonance but didn’t feel she could find a producer ready to make the movie.  Thus, von Trotta shelved the idea until it came to cinematic fruition recently. 

The film reunites von Trotta with Barbara Sukowa (Zentropa, Berlin Alexanderplatz).  Sukowa portrays Hildegard’s fierce determination to expand the responsibilities of women within the Benedictine order, as she fends off outrage from the Church over the visions she claims to receive from God.  Shot in the original medieval cloisters in the German countryside, in Vision, von Trotta and Sukowa create a portrait of a woman who has emerged from the shadows of history as a forward-thinking pioneer of faith, change and enlightenment.   The film depicts Hildegards diplomatic (sometime manipulative) skills to understand men and their vanities in order to found her own convent. It captures Hildegard’s love of happiness, mankind and their connectedness to faith.

Vision made its European debut in 2009 and is being distributed in the U.S. by Zeitgeist Films  starting October 13, 2010.

==Accolades==
*Official Selection - Telluride Film Festival  2009
*Official Selection - Toronto International Film Festival  2009

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 