Ithu Pathiramanal
{{Infobox film name = Ithu Pathiramanal image = starring = Pradeep Rawat director = M. Padmakumar producer = Haseeb Haneef writer = Babu Janardhanan distributor =
|cinematography= Manoj Pillai editing = released =   runtime = language = Malayalam music = Afzal Yusuf budget = gross =
}}

Ithu Pathiramanal (  action film directed by M. Padmakumar starring Unni Mukundan in the lead role with Jayasurya and Ramya Nambeeshan . The shooting locations are in Marayur, Pathiramanal and Alapuzha. 

==Plot== Pradeep Rawat) during Eldhos childhood. His mother conceived from the rape and gave birth to a baby girl. Johnkutty (Jayasurya), Eldhos father is avenging his wifes death by going after the police constable and unfortunately dies.

After Eldho has grown up, he reaches Pathiramanal, a place in Kuttanad, in search of the constable to take revenge. There he meets Sara. He realizes that Sara is the daughter of the constable.

==Cast==
* Unni Mukundan as Eldho
* Jayasurya as Johnkutty
* Remya Nambeesan as Sara Pradeep Rawat as Shouri
* Bhagath Manuel as Murukan
* Shalu Menon as Ambika Kunchan as Chellapanashari
* Mahesh
* Master Siddharth
* Jyothykrishna as shallomi
* P.Balachandran as preast

==Production==
The shoot of the film  started at Alappuzha in May. The shooting was stopped for a while because of lead actor Jayasuryas injury during Vaadhyar film action sequence. Jayasurya was doing both the role of father and son in the film. But due to his injury, the sons role was filled by Unni Mukundan.

==Soundtrack==
The soundtrack features three songs composed by Afzal Yusuff with lyrics by Vayalar Sarath Chandra Varma and Beeyar Prasad.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)!! Duration
|-
| 1
| "Ekakiyay"
| K. J. Yesudas
| 5:57
|-
| 2
| "Eriveyil"
| Shreya Ghoshal
| 4:34
|-
| 3
| "Alolam"
| Najim Arshad, Mridula Warrier
| 4:35
|}

==References==
 

 
 
 


 
 