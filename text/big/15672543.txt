Carmen Comes Home
{{Infobox Film
| name           = Carmen Comes Home
| image          =
| image size     = 180px
| caption = Japanese movie poster
| director       = Keisuke Kinoshita Masaki Kobayashi (assistant director) Zenzo Matsuyama (assistant director)
| producer       = Shochiku
| writer         = Keisuke Kinoshita
| starring       = 
| cinematography = Hiroyuki Kusuda
| editing        = 
| music          = Chuji Kinoshita Toshiro Mayuzumi
| distributor    =  1951 
| runtime        = 86 minutes
| country        = Japan
| language       = Japanese
| budget         = 6800 millions Japanese yen
}}
  is a 1951 color Japanese film comedy directed by Keisuke Kinoshita. Filmed using Fujicolor, it was Japans first color film.

 , Toshiko Kobayashi and Yūko Mochizuki]]

== Cast ==
* Hideko Takamine as Lily Carmen, Aoyama Kin
* Shūji Sano as the blind man Haruo Taguchi
* Chishū Ryū as schoolmaster
* Kuniko Ikawa as Mitsuko, Haruos wife
* Takeshi Sakamoto as Shoichi, Kins father
* Bontarō Miyake as Maruju
* Keiji Sada as Mr. Ogawa, the school teacher
* Toshiko Kobayashi as Akemi Maya
* Koji Mitsui as Marujus man
* Yūko Mochizuki (aka Mieko Mochizuki) as Aoyama Yuki, Kins sister
* Yoshito Yamaji as village youth
* Akio Isono as Aoyama Ichiro
* Sumie Kuwabara
* Kuninori Takado
* Eiko Takamatsu
* Shoichi Kotoda
* Shusuke Agata
* Kiyoshi Koike as Aoyama Naokichi
* Isao Shirosawa as Taguchi Kiyoshi
* Yoichi Osugi
* Susumu Takase
* Jun Tanizaki
* Akira Noto
* Jun Yokoyama
* Shunsuke Kaneko
* Tami Yamamoto		
* Haruko Chichibu
* Sakae Ozawa as Maruno Juzo (uncredited)

== References ==
 

== External links ==
 
*  

 

 
 
 
 
 
 


 
 