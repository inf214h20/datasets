The Elusive Avengers
{{Infobox film
| name           = The Elusive Avengers
| image          = Elusive Avengers poster.jpg
| image_size     = 190px
| caption        = Film poster
| director       = Edmond Keosayan
| producer       = Mosfilm
| writer         = Story:   Sergei Yermolinsky
| narrator       =
| starring       = Viktor Kosykh Mikhail Metyolkin Vasili Vasilyev Valentina Kurdyukova Yefim Kopelyan Vladimir Treshchalov Boris Sichkin Inna Churikova Saveli Kramarov
| music          = Boris Mokrousov
| cinematography = Fyodor Dobronravov
| editing        = Lyudmila Yelyan
| distributor    = Mosfilm
| released       =  
| runtime        = 78 min.
| country        = Soviet Union
| language       = Russian
| budget         =
| gross          =
}}

The Elusive Avengers ( ,   under its original name. The film is an example of Ostern, set in Russian Civil War era Ukraine.

The film has spawned two sequels, The New Adventures of the Elusive Avengers (1968) and The Crown of the Russian Empire, or Once Again the Elusive Avengers (1971).

==Synopsis==
The film is a version of a story about four youngsters who become heroes in the Russian Civil War. Danka, orphaned son of a Red agent, whose father was tortured and executed by the warlord Lyuty before his eyes, and his sister Ksanka join Valerka, a former schoolboy, and Yashka, a devil-may-care Romani people|gypsy. They make a pledge of mutual assistance, determined to exact revenge on the bandits who are bringing so much suffering to peaceful villagers. The friends then embark on a series of daring adventures.
 Ataman Burnash. All his schemes seem to go wrong, sabotaged by unseen and unidentified enemies. The mischievous culprits always leave a note signed - the Elusive Avengers, and are of course the four friends, who succeed by never forgetting their pledge of mutual assistance. They are so effective, in fact, that reports of their deeds are reaching the local division of the Red Army.
 Westerns such as For a Few Dollars More and the Japanese Yojimbo (film)|Yojimbo, Danka uses his anonymity to infiltrate the outlaws gang and insinuate himself into Burnashs confidence, becoming his trusted right hand man. Unfortunately, Warlord Lyuty, whom Danka had thought he killed earlier, arrives at the Atamans camp and accuses Danka; and when Lyutys accusations are proven, it is up to the other three Avengers (well, mostly Yashka and Valerka) to get him out.

At the end of the movie, the Avengers are honored and acknowledged by the Red Army, which promptly appropriates the four of them as soldiers. The Elusive Avengers ride off into the rising sun, ready to answer their armys call.

==Differences from the original==
* In the book, the "ethnic minority" character was  ). The Elusive Avengers, filmed in Cold War era, by contrast, avoided references to the United States and replaced the obviously propagandistic black Tom with the more realistic gypsy Yashka.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 