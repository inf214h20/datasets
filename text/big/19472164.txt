Jonah and the Pink Whale
 
{{Infobox film
| name           = Jonah and the Pink Whale
| image          =
| caption        =
| director       = Juan Carlos Valdivia
| producer       = Luz María  Rojas Raquel Romero
| writer         = José Wolfango Montes Vannuci (book)
| starring       = Dino García María Renée Prudencio Julieta Egurrola Guillermo Gil
| music          = José Stephens
| cinematography = Henner Hofmann
| editing        = Sigfrido Barjau
| distributor    = IFA
| released       =  
| runtime        = 93 minutes
| country        = Bolivia Mexico
| awards         =
| language       = Spanish
| budget         =
}}
 Bolivian Film directed by Juan Carlos Valdivia. The film is based on the 1987 novel by José Wolfango Montes Vannuci  The film focuses on the relationship of Jonas, a photographer and his romantic relationship with his sister-in-law.
 Best Foreign Language Film competition in the 68th Academy Awards, but it did not receive an Oscar nomination. 

==See also==
* List of Bolivian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 