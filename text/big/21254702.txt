I Can Do Bad All by Myself (film)
{{Infobox film
| name = I Can Do Bad All By Myself
| image = I can do bad all by myself ver3.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Tyler Perry
| producer = Tyler Perry Reuben Cannon
| writer = Tyler Perry Brian White Mary J. Blige Gladys Knight Marvin Winans Tyler Perry
| music = Aaron Zigman
| cinematography = Alexander Gruszynski
| editing = Maysie Hoy
| studio = Tyler Perry Studios
| distributor = Lionsgate
| released =  
| runtime = 113 minutes
| country = United States
| language = English
| budget = $19 million 
| gross = $51,733,921    
}} romantic Musical musical comedy-drama adaptation of play of the same name; the two works have different storylines.

==Plot== Madea (Tyler Perry) and Joe Simmons (Perry) catches Jennifer (Hope Olaidè Wilson), Manny (Kwesi Boakye), and Byron (Freddy Siglar) breaking into their house. After hearing the children’s troubles, Madea welcomes and feeds them. Jennifer tells Madea that they’re living with their grandmother, whom they havent seen in four days. They tell Madea that their only other relative is their aunt April.
 Brian White), advances at Jennifer.
 brain aneurysm while riding on a bus. April is devastated by the news and seeks comfort from Randy; however, he is sleeping and shrugs her off. Later, Sandino comforts April as she tells him about her mothers death and the last time she spoke with her.

Depressed, Jennifer goes to Madea wanting to know how to pray. However, Madea, inexperienced with prayer, attempts to instruct her in a scene that plays out comically. The same night, Wilma sings "The Need to Be", an uplifting song for women, and Tanya (Mary J. Blige), the nightclub bartender, sings "I Can Do Bad". Before singing the song, Tanya is fed up with Aprils attitude and tries to help her friend, despite the fact that she cant help April if she can’t help herself.

Over time, Sandino and April become friends, and Sandino fixes a ruined bedroom in her house. This makes Manny and Byron happy but not Jennifer, who feels April doesnt want them there. While on a date, Sandino tells April he doesnt understand why she is with Randy and asks if she loves Randy. He tells her what true love is to him. One Sunday morning, Sandino eagerly knocks on Aprils bedroom door to get April ready for church, but Randy threatens to kill Sandino if he continues to spend time with April.

Late one night, Manny needs his insulin shot, and Jennifer gets it for him in the kitchen. As Jennifer gets her brothers insulin, Randy approaches and attempts to rape her, but Sandino fights him off. April walks in on the fight and Randy claims Jennifer offered him sex for money. April claims to believe him and sends Randy to take a bath. When he is in the tub, April threatens to electrocute him with a plugged-in radio. Sandino arrives and tries to stop her, but April is enraged, as she explains that she was sexually assaulted by her step-father and lied about it to her mother thus causing her to lose her faith in the people that cared about her. After saying Randy is no different from him, she drops the radio into the water, giving Randy a severe electric shock. Randy jumps out and Sandino orders him to leave.

April goes to the bar for a drink and blames herself for not seeing the signs. Sandino tries to stop her from drinking, but she pushes him away. She then asks Sandino if he is a child molester, because of all the attention he gives the children. Sandino tells April of his childhood as a child laborer and explains that he loves the children so much because he sees himself in them. Feeling hurt at her unfair accusations, Sandino says farewell to the children and leaves.

Jennifer and April begin to get along and connect after April tells Jennifer about her bad experience as a child. Jennifer tells April that she should recognize Sandino as a good man. Eventually, Sandino returns and April apologizes to him and admits that she loves him like a friend. Sandino tells her that she cant love anyone until she learns to love herself. He tells April that he is in love with her but he wants April to love him back the same way he loves her. He shows her by kissing her.

Eventually, April and Sandino get married. April and Sandino then hold a block party for their reception with Tanya singing "Good Woman Down", dedicated to April, then the new couple is shown embracing and sharing a passionate kiss.

==Cast==
* Taraji P. Henson as April
* Adam Rodriguez as Sandino  Brian White as Randy 
* Mary J. Blige as Tanya 
* Gladys Knight as Wilma 
* Marvin Winans as Pastor Brian 
* Hope Olaidé Wilson as Jennifer 
* Kwesi Boakye as Manny 
* Freddy Siglar as Byron Madea and Joe Simmons

==Differences from the stage play==
* Tyler Perrys famous characters, Mr. Brown and Cora, had main roles in the stage play; they are absent in the film. In the film, several characters refer to Cora, but she is not seen; Mr. Brown is neither seen nor mentioned.
* The original plot (in the stage play) focused on two sisters feuding because Maylee is engaged to her sister Viannes ex-husband, Anthony, and Anthony is holding her back from her parental responsibilities. In the film, Aprils sister is a drug addict and thief who neglected her children and is no longer in the picture. Her mother assumed custody of the children but has recently disappeared without explanation, and circumstances have forced April to reluctantly take her niece and nephews in.
* The stage play characters Vianne, Keisha, Bobby, Maylee, and Anthony (the antagonist in the play) dont exist in the film. Aprils married boyfriend Randy is the true main antagonist in the film.

==Music==
The film features 13 songs,  including two new songs by Blige. Perry was not able to produce a soundtrack album for the film due to the various record companies involved. 

* "Good Woman Down" (Robert F. Aries, Blige, Sean Garrett, Freddie Jackson, Melisa Morgan) – Mary J. Blige Shaffer Smith) – Mary J. Blige
* "Playboy" (Michael Akinlabi, Tasha Schumann) – Candy Coated Killahz Xavier Dphrepaulezz ) – Chocolate Butterfly
* "H.D.Y." (Ronnie Garrett, Herman (Pnut) Johnson) – Club Indigo Band
* "Indigo Blues" (Garrett, Johnson) – Club Indigo Band
* "Lovers Heat" (Garrett, Johnson) – Club Indigo Band
* "Tears of Pain" (Foster) – Ruthie Foster Rock Steady" (Aretha Franklin) – Taraji P. Henson
* "The Need to Be" (Jim Weatherly) – Gladys Knight Marvin L. Winans
* "Oh Lord I Want You to Help Me" (Traditional, arranged by Jerome Chambers & Edward ONeal) – Cheryl Pepsii Riley & Marvin L. Winans

==Reception==

===Critical reaction===
The film has received mixed to generally positive reviews from critics, making it Perrys most acclaimed film to date.    Rotten Tomatoes reported that 63% of critics gave positive reviews based on 43 reviews with an average score of 5.9/10, giving it a "Fresh" rating, with the general consensus being though somewhat formulaic and predictable, Perry succeeds in mixing broad humor with sincere sentimentality to palatable effect.  By comparison, Metacritic gave the film a 55% approval rating of critics based on 13 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   at MovieSet

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 