The Hole (1998 film)
 The Hole}}
{{Infobox film
| name        = The Hole
| image       = The Hole 1998 cover.jpg
| writer      = Tsai Ming-liang Yang Pi-ying
| starring    = Yang Kuei-Mei Lee Kang-sheng
| director    = Tsai Ming-liang
| producer    = Cheng Su-ming Chiu Shun-Ching Jiang Feng-Chyt
| distributor = Fox Lorber
| released    =  
| runtime     = 95 min. 69 min.  (Taiwan) 
| country     = Taiwan
| music       = Grace Chang Mandarin Taiwanese Taiwanese
| budget      =
}} musical film directed by Tsai Ming-liang. It stars Yang Kuei-Mei and Lee Kang-sheng.

==Plot==
A strange disease hits Taiwan just before the turn of the new millennium. Despite evacuation orders, tenants of a rundown apartment building stay put, including shop owner Hsiao-Kang (Kang-sheng Lee).

One day, a plumber arrives at Hsiao-Kangs apartment to check the pipes. He drills a small hole into the floor, which comes down through the ceiling of the woman downstairs (Kuei-Mei Yang).

The hole never gets repaired, and this leads to some tension between the two residents.

==Cast==
* Yang Kuei-Mei - the woman downstairs
* Lee Kang-sheng - the man upstairs
* Miao Tien - the customer at market
* Tong Hsiang-Chu - the plumber
* Lin Hui-Chin - a neighbor
* Lin Kun-huei - the kid

==Reception==
The Hole was entered into the 1998 Cannes Film Festival.    It was nominated for the Golden Palm award and won the FIPRESCI Prize (competition) "for its daring combination of realism and apocalyptic vision, desperation and joy, austerity and glamour." It also won three Silver Screen Awards at the Singapore International Film Festival - for best Asian feature film, best Asian director, and best Asian actress.

The film has an 80% fresh rating on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 