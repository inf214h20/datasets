The Puzzle of the Red Orchid
 
{{Infobox film
| name           = The Puzzle of the Red Orchid
| image          = 
| caption        = 
| director       = Helmut Ashley
| producer       = Horst Wendlandt
| writer         = Edgar Wallace Egon Eis
| starring       = Christopher Lee Peter Thomas
| cinematography =  
| editing        =  
| studio         = Rialto Film
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

The Puzzle of the Red Orchid (  and also known as The Secret of the Red Orchid) is a 1962 West German black-and-white crime film directed by Helmut Ashley and starring Christopher Lee.   

==Cast==
* Christopher Lee as Captain Allerman
* Adrian Hoven as Inspector Weston
* Marisa Mell as Lilian Ranger
* Pinkas Braun as Edwin
* Christiane Nielsen as Cora Minelli
* Eric Pohlmann as Kerkie Minelli
* Fritz Rasp as Tanner
*   as Chief Inspector Tetley
* Herbert A.E. Böhme as Oberst Drood
* Günther Jerschke as Mr. Shelby
*   as Mrs. Moore
* Hans Paetsch as Lord Arlington
*   as Babyface
* Klaus Kinski as Steve
* Eddi Arent as Parker

==Reception== FSK gave the film a rating of "12 and older" and found it not appropriate for screenings on public holidays.   

The film went on mass release on 1 March 1962.  

==See also==
* Christopher Lee filmography
* Klaus Kinski filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 