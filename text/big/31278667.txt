It's in the Bag (1936 film)
{{Infobox film
| name =  Irish for Luck
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Irving Asher  Brock Williams   Russell G. Medcraft 
| narrator =
| starring = Jimmy Nervo   Teddy Knox   Jack Barty   George Carney
| music = 
| cinematography = Basil Emmott
| editing = Bert Bates 
| studio = Warner Brothers 
| distributor = Warner Brothers
| released = September 1936
| runtime = 80 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Its in the Bag is a 1936 British comedy film directed by William Beaudine and starring Jimmy Nervo, Teddy Knox and Jack Barty.  Two low-level workers acquire some money and decide to open a nightclub together. It was made at Teddington Studios by the British subsidiary of Warner Brothers.  Art direction was by Peter Proud.

==Cast==
* Jimmy Nervo as Jimmy
* Teddy Knox as Teddy
* Jack Barty as Bert
* George Carney as Blumfield
* Rene Hunter as Ethel
* Ursula Hirst as Vi
* Aubrey Dexter as Peters
* Hal Gordon as Boss
* Ernest Sefton as Jerry Gee
* C. Denier Warren as Emery
* Glen Alyn as Fifi

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Marshall, Wendy L. William Beaudine: from silents to television. Scarecrow Press, 2005.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 