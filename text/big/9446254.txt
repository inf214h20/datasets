Up the River
 
{{Infobox film
| name           = Up the River
| image          = Up the River (film poster).jpg
| image_size     =
| caption        = Bogart featured on poster
| director       = John Ford William Fox
| writer         = Maurine Dallas Watkins
| narrator       =
| starring       = Spencer Tracy Claire Luce Warren Hymer Humphrey Bogart Joseph McCarthy
| cinematography = Joseph H. August
| editing        = Frank E. Hull
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 92 min
| country        = United States English
}}

Up the River (1930 in film|1930) is a Pre-Code comedy film about escaped convicts, directed by John Ford and starring Spencer Tracy and Humphrey Bogart in their feature film debuts.

==Plot==
Two convicts, St. Louis (Spencer Tracy) and Dannemora Dan (Warren Hymer) befriend another convict named Steve (Humphrey Bogart), who is in love with womans-prison inmate Judy (Claire Luce). Steve is paroled, promising Judy that he will wait for her release five months later. He returns to his hometown in New England and his mothers home.

However, he is followed there by Judys former "employer", the scam artist Frosby (Gaylord Pendleton). Frosby threatens to expose Steves prison record if the latter refuses to go along with a scheme to defraud his neighbors. Steve goes along with it until Frosby defrauds his mother. Fortunately, at this moment St. Louis and Dannemora Dan have broken out of prison and come to Steves aid, taking away a gun he planned to use on the fraudster, instead stealing back bonds stolen by Frosby. They return to prison in time for its annual baseball game against a rival penitentiary. The film closes with St. Louis on the pitchers mound with his catcher, Dannemora Dan, presumably ready to lead their team to victory.  

==Cast==
* Spencer Tracy as Saint Louis
* Claire Luce as Judy Fields
* Warren Hymer as Dannemora Dan
* Humphrey Bogart as Steve Jordan
* Gaylord Pendleton as Frosby
* William Collier, Sr. as Pop
* Joan Lawes as Jean

===Casting=== The Desperate Hours in 1955, but neither would consent to second billing, so the role intended for Tracy went to Fredric March instead. It was the only film Bogart made with director John Ford, and Tracy wouldnt work with Ford again until The Last Hurrah (1958).

Claire Luce (1903–1989) made very few films, but was on Broadway in many plays from 1923–1952. She should not be confused with author/playwright/political activist Clare Boothe Luce (1903–1987).
 remade by Up the Tony Martin respectively in the Tracy and Bogart roles.

==References==
 

==Bibliography==
*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 