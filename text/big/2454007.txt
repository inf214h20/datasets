Goldilocks and the Jivin' Bears
 
{{Infobox Hollywood cartoon cartoon_name = Goldilocks and the Jivin Bears image = 1944_goldilocks_and_the_jivin_bears.jpg caption = The December 1st, 1951 reissue card series = (Merrie Melodies) director = Friz Freleng story_artist = Tedd Pierce animator = Ken Champin layout_artist = Hawley Pratt background_artist = Paul Julian voice_actor = Mel Blanc Sara Berner (uncredited) musician = Carl Stalling producer = Eddie Selzer distributor = Warner Bros., Vitaphone release_date = September 2, 1944  color_process = Technicolor runtime = 7 minutes movie_language = English
}}
 short written by Tedd Pierce and directed by Friz Freleng. It was released on September 2, 1944, by Warner Bros. Pictures as part of its Merrie Melodies series.

Like most Merrie Melodies reissued during this time, the original closing was kept. This is the first cartoon to be produced by an uncredited Eddie Selzer.

The films story combines elements of Goldilocks and the Three Bears and Little Red Riding Hood. All of the characters are drawn in blackface style. This would be the last Warner Bros. animated short to feature an all-black cast excluding Chuck Jones Inki cartoons.

==Synopsis==
 

The Three Bears, a jazz trio, are enjoying a hot jam session when their instruments catch fire. After consulting a storybook, they find that they must go out for a walk to let the instruments cool off.
 Lockheed as a Rosie the Riveter|rivetater." The frustrated wolf looks out the window and sees Goldilocks entering the Three Bears house. (Unlike the other characters, Goldilocks is drawn as an attractive young woman.) Because there is a "food shortage" going on, the wolf decides to pursue Goldilocks.

Inside the Three Bears house, Goldilocks tries all the beds and lies down in the best one, only to find the wolf in bed with her. The wolf chases Goldilocks through the house until the Three Bears return. Finding Goldilocks and the wolf struggling in the living room, they shout "Jitterbugs!" and begin playing a dance tune. The wolf and Goldilocks dance the jitterbug until the wolf is exhausted and flees to Grandmas house.

Red Riding Hood returns to find the wolf in Grandmas bed, but the wolf is too tired to eat her. The Three Bears rush in, shout "Deres dat jitterbug!" and resume playing. This causes Grandma to burst out of a cupboard and jitterbug with the wolf, who turns to the audience and says, in a Jimmy Durante voice, "Everybody wants to get into the act!"

==Bans==
Because the film contains stereotyped portrayals of African-Americans, it is no longer available in any type of authorized release and is among the group of controversial cartoons known to animation buffs as the Censored Eleven.

==External links==
*  
*  
 
 
 
 
 
 
 
 
 
 
 