Wicked as They Come
{{Infobox film
| name           = Wicked as They Come
| image          = Wicked As They Come film poster.jpg
| caption        = Theatrical release poster
| director       = Ken Hughes
| producer       = M.J. Frankovich Maxwell Setton
| writer         = Ken Hughes Sigmund Miller Robert Westerby
| story          = Bill S. Ballinger (novel)
| starring       = Arlene Dahl Philip Carey Herbert Marshall
| music          = Malcolm Arnold	 	
| cinematography = Basil Emmott
| editing        = Max Benedict
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
}}

Wicked as They Come (Portrait in Smoke in the United States) is a 1956 British film starring Arlene Dahl, Philip Carey and Herbert Marshall. The film was directed by Ken Hughes. 

==Plot==

Poor girl from the slums Katherine Allenbourg trades on her looks. She enters a beauty contest, then charms the elderly gentleman running it, Sam Lewis, into fixing it so she will win first prize, a trip to Europe. She promptly abandons Sam.

Changing her name to Kathy Allen, she is attracted to Tim OBannion, a passenger on the plane to London who works for an ad agency. But shes determined to land someone wealthier and photographer Larry Buckham fills the bill. Invited to use his charge account at a department store for a wedding dress, Kathy makes many purchases, pawns the merchandise and leaves Larry without a word.

She gets a job at Tims advertising firm and seduces the married Stephen Collins, who runs it. Tim arouses more passion in her, but Kathys strictly out for herself. She demands Collins divorce his wife Virginia, whose father John Dowling owns the agency. Virginia tries to pay her off, but Kathy requests a transfer to the agencys Paris headquarters, where she immediately uses her wiles to get Dowling to marry her.

Anonymous threats begin by mail and phone. Someone in the shadows begins stalking her. Kathy picks up a gun and shoots, killing her husband instead. No one believes her tale of a prowler and Kathy is tried, convicted and sentenced to die.

Tim realizes that it is Larry who is behind all this. He reveals an explanation for Kathys cruel treatment of men, the fact that she was brutally assaulted as a girl. Larry has a change of heart and confesses to stalking her. Kathys prison sentence is reduced, and she hopes Tim will give her another chance once she gets out.

==Cast==
* Arlene Dahl as Kathleen Kathy Allen, née Allenbourg
* Philip Carey as Tim OBannion
* Herbert Marshall as Stephen Collins
* Michael Goodliffe as Larry Buckham
* Ralph Truman as John Dowling
* Sid James as Frank Allenbourg
* David Kossoff as Sam Lewis
* Faith Brook as Virginia Collins
* Frederick Valk as Mr. Reisner
* Marvin Kane as Mike Lewis
* Patrick Allen as Willie
* Gil Winfield as Chuck
* Jacques B. Brunius as Inspector Caron

==References==
 

==External links==
*  
*  

 

 
 
 

 