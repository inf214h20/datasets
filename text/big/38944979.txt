Aashiqui.in
{{Infobox film
| name           = Aashiqui.in
| image          = Aashiqui.in.jpg
| caption        = Theatrical release poster
| director       = Shankhadeep
| producer       =  
| writer         = Shankhadeep
| starring       =  
| music          =  
| cinematography = Bharat Parthasarthy
| editing        = Fazal Hussain
| studio         = 
| choreography   = Pradeep Kalekar
| distributor    =  
| released       =  
| runtime        = 109 minutes
| country        = India Hindi
| INR 22,500,000
| gross          = 
}} romantic film directed by Shankhadeep, starring Ankita Shrivastava and debutant Ishaan Manhaas in the lead roles. The music and the background score of the film are composed by Nitin kumar gupta and Prem Hariya, and the lyrics are penned by Gupta as well.    The film was released in cinemas on February 11, 2011, and the story follows a man who meets his love interest on the internet.

==Plot==
Cyrus (Ishaan Manhaas) is a college swim champ who has difficulty in setting his future. He aspires to becoming a writer, but as his father (Kamal Malik) wishes him to take his swimming skills to a national level, Cyrus hides his aspirations so as not disappoint his dad. Though popular in college, he feels out of place and seeks someone who can really understand him.

When a child, April (Ankita Shrivastava) lived an idyllic lifestyle with her widowed restaurant-owner father (Amit Dhamija).  He remarries Mona (Sharmila Joshi), who herself has two daughters, Saniya (Shubhi Ahuja) and Taniya (Priti Gandwani). Soon after the marriage, Aprils father passes away, and her stepmother and step sisters begin to treat her like a servant. Years later, and even while burdened by domestic duties, April manages to attend college and keep a part-time job to earn pocket money.  Her only confidant is her childhood friend Raj (Dheeraj Miglani).

Cyrus and April meet anonymously online, becoming close without revealing their real-world identities.  When they finally decide for a real-world meeting, April is able to discover Cyrus true identity beforehand, but he does not know hers. Feeling that he is too good for her, April avoids committing to a meeting. Cyrus, feeling he has finally found the one person who would understand him as both a friend and perhaps love, seeks her everywhere.

==Cast==
* Ishaan Manhaas as Cyrus
* Ankita Shrivastava as April
* Dheeraj Miglani as Raj Malhotra
* Tiya Gandwani as Soniya
* Shubhi Ahuja as Saniya
* Deepan Shah as Danny
* Sharmila Joshi as Mona
* Priti Gandwani as Taniya
* Ankitha as Riya
* Kamal Malik as Cyrus dad
* Amit Dhamija as Aprils dad
* Kalpesh as Robo

==Themes==
Inspired by the original Cinderella tale, the film involves a young woman being controlled by an overbearing step-mother and two step-sisters in a storyline which includes a hero protagonist mimicking the Cinderella Prince Charming, the two meeting at a costumed ball, the heroine rushing to leave before midnight, a glass shoe being left behind when she leaves the ball, and after the heros search for her, he and the heroine live "happily ever after."  

==Production==

 
the choreogapher is pradeep kalekar
 

==Release==
Originally intended for a January 2011 release,    the film was released to theaters on February 11, 2011. 

==Reception==
Rao Renuka of Daily News and Analysis offers that the film is a poor depiction of the Cinderella story. She writes that "It is unbelievable how at a time when audiences are discovering joyrides like Yeh Saali Zindagi and Dhobi Ghat, filmmaking debacles such as Aashiqui.in dare to even present themselves to viewers." She expands that the film has a poor plot, that actors seem to compete on screen for title of "worst performance", and that through its "storyline, over-the-top performances, and unsophisticated, rough editing" the film "makes a mockery of the Cinderella legend."   

== Soundtrack ==
{{Track listing
| extra_column = Performer(s)
| music_credits = yes
| lyrics_credits = yes
| title1 = Ruk Ke Jaana | extra1 = Kunal Ganjawala | music1 = Nitin kumar gupta | lyrics1 = Nitin kumar gupta Shaan | music2 = Nitin kumar gupta | lyrics2 = Nitin kumar gupta
| title3 = Ishq Bada Sangdil | extra3 = Sukhwinder Singh | music3 = Prem Hariya | lyrics3 = Taugique Palvi
| title4 = Lichu Lichu | extra4 = Sunidhi Chauhan and Amit Kumar | music4 = Nitin kumar gupta | lyrics4 = Taufique Palvi
| title5 = Rangon Bhari Raat | extra5 = Kunal Ganjawala | music5 = Nitin kumar gupta | lyrics5 = Nitin kumar gupta
| title6 = Cinderella Se Bhi Pyari | extra6 = Kunal Ganjawala | music6 = Nitin kumar gupta | lyrics6 = Nitin kumar gupta
| title7 = Ruk Ke Jaana | note7 = Everytime I See You Remix | extra7 = Nitin kumar gupta | music7 = Nitin kumar gupta | lyrics7 = Nitin kumar gupta
| title8 = Ishq Bada Sangdil | note8 = Pure Plugins Remix | extra8 = Sukhwinder Singh| music8 = Prem Hariya | lyrics8 = Taugique Palvi Shaan | music9 = Nitin kumar gupta | lyrics9 = Nitin kumar gupta
| title10 = Ruk Ke Jaana Reloaded! | note10 = Remix | extra10 = Kunal Ganjawala | music10 = Nitin kumar gupta | lyrics10 = Nitin kumar gupta
}}

==References==
 


==External links==
*  

*  
*  
*  
 
 
 
 
 
 