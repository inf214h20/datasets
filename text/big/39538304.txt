The Mule (2014 film)
 
{{Infobox film
| name           = The Mule
| image          = The_Mule_poster.jpg
| caption        = 
| director       = Tony Mahony  Angus Sampson
| story          = Jaime Brown
| screenplay     = Angus Sampson  Leigh Whannell
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}
The Mule is a 2014 film directed by Tony Mahony and Angus Sampson.  It was released directly to iTunes and other digital platforms simultaneously in Australia, United States, Canada and New Zealand on 21 November 2014. 

==Plot==
In 1983 Australia, television repairman Ray Jenkins (Angus Sampson) and his football team celebrate the end of their season by spending the weekend in Thailand. Rays best friend Gavin (Leigh Whannell), a small-time criminal working for local property owner/crime lord Pat Shepherd (John Noble), tags along on the trip. While wandering through the markets, Gavin goes to pick up half a kilogram of heroin to bring back to Pat. Before he leaves, he purchases an extra half kilogram to sell on his own. At the hotel, Gavin hides the heroin in condoms, which he forces Ray to swallow. Upon their arrival at Melbourne Airport, Ray begins to panic and is eventually detained by customs officials. Believing that Ray is a drug trafficker, he is arrested by AFP Agents Croft and Paris (Hugo Weaving and Ewen Leslie). Rays lawyer Jasmine Griffiths (Georgina Haig) tells Ray that he can only be held in a hotel room for four days.
 Geoff Morrell) that Ray has been arrested. They plan to head to the hotel to visit him, but John has a discussion with Gavin, revealing his participation in the drug scheme and to have Ray pinned for trafficking Pats drugs in order to get Pat to get rid of his debts. Judy and John head to the airport, where they give Ray his favourite meal cooked by Judy. When he refuses to eat it, Jasmine realises the drugs are located in Rays stomach. Suspecting the location of the drugs, Croft goes to a judge and extends Rays time in the hotel room for a whole week.

One day, Paris returns to the hotel room to find Ray being tormented by Croft and a police guard. He kicks them out of the room and comforts Ray, expressing his beliefs that Croft is working for Pat Shepherd. Gavin visits Pat, who tells him that Ray must be killed. He heads to the hotel, where he sneaks into the room while the guard is out of the room, intending to stab Ray, but finds he cannot bring himself to do so. He is caught by Paris, who follows him to the rooftop. Paris reveals himself to be working for Pat, while Gavin explains the whole situation. Having learnt that Gavin has purchased more heroin for himself, Pat sends his Russian guard to the hotel to kill Gavin. On the rooftop, a panicking Gavin threatens to testify against Pat and Paris, only for Paris to push him off the building, his body becomes wedged in the windshield of Pats bodyguards car. Having witnessed Gavin falling off the building, Ray realises that Paris is corrupt. He tries to tell Croft, but he refuses to believe his partner is corrupt.

Croft goes to the judge and extends the sentence to ten days, despite Jasmines pleas. She visits Ray and tells him that if he is ever released, he should testify against Croft and Paris for keeping him held against his will and their cruel behaviour. After Pat steals his car, John admits to Judy his part in the scheme and she kicks him out. John gets drunk and heads to a hall owned by Pat, where he threatens to kill him. Pat shoots John and hides his body in the hall. In the middle of the night, Ray is unable to stop himself from defecating in his bed. After making sure the guard is asleep, Ray digests the condoms, before he wakes the guard.

Still insisting that Ray is hiding the drugs, Croft goes to the judge and finally extends the sentence to the maximum of 14 days. On the twelfth night, Ray puts his plan into action. He excretes one of the condoms and places some of the heroin in the beer of the police officer guarding him, sending him to sleep. He then excretes the rest of the condoms, and then taking a coin from the police officers wallet, starts to unscrew the back panel of the television.

On the last night, the police officers are trying to watch the 1983 Americas Cup but the television is displaying static, so they call reception to have it repaired. On the morning of the last day, Ray finally goes to the toilet, and Paris discovers there are no drugs in his stomach contents. He attacks Ray, but Croft and another officer arrive, and he claims that Ray has assaulted him. An ambulance is called, and Ray hands Croft a photograph of Pat. A furious Paris trashes the hotel room looking for the drugs, and finds a hidden microphone in a flower vase. Paris realises his threats and confession to Ray have been recorded, revealing his corruption, and Croft enters the room and arrests him. Croft has also sent police to arrest Pat, but Pat is killed by his Thai chef as the police arrive. At the television repair shop where he works, Ray smiles as he looks at the television from the hotel room where he hid the drugs.

==Cast==
*Hugo Weaving as Tom Croft 
*Angus Sampson as Ray Jenkins
*Leigh Whannell as Gavin
*Ewen Leslie as Paris
*Georgina Haig as Jasmine Griffiths
*John Noble as Pat Shepherd Geoff Morrell as John
*Noni Hazlehurst as Judy Jenkins
*Chris Pang as Phuk

==Reception==
It has an 85% "fresh" rating on Rotten Tomatoes based on 26 reviews, with an average rating of 6.7/10. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 