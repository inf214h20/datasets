Ethan Frome (film)
{{Infobox film
|name=Ethan Frome Richard Nelson (screenplay) Edith Wharton (novel) John Madden
|starring=Liam Neeson Patricia Arquette Tate Donovan
|released=1993
|producer=
|language=English
|country=United Kingdom United States
}}
 John Madden and starring Liam Neeson, Patricia Arquette and Tate Donovan.  It was an adaptation of the novella Ethan Frome by Edith Wharton.

==Cast==
* Liam Neeson – Ethan Frome
* Patricia Arquette – Mattie Silver
* Tate Donovan – Reverend Smith
* Stephen Mendillo – Ned Hale
* Phil Garran – Mr. Howe Virginia Smith – Mrs. Howe
* Annie Nessen – Sarah Anne Howe
* Katharine Houghton – Mrs. Hale
* George Woodard – Jotham Powell
* Jay Goede – Denis Eady
* Joan Allen – Zenobia Zeena Frome

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 