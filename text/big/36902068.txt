Bab's Diary
{{infobox film
| name           = Babs Diary
| image          = Babs Diary 1917 scene.jpg
| imagesize      =
| caption        = Scene from the film.
| director       = J. Searle Dawley
| producer       =
| writer         = Martha D. Foster
| story          = Mary Roberts Rinehart
| starring       = Marguerite Clark
| music          =
| cinematography = Lewis W. Physioc
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 5 reels
| country        = United States
| language       = Silent English intertitles
}}
 silent romantic comedy film directed by J. Searle Dawley, and starring Marguerite Clark. The films scenario was written by Martha D. Foster, based on the screen story "Her Diary" by Mary Roberts Rinehart.  This was the first in a trilogy of Babs films, all starring Clark.

==Cast==
*Marguerite Clark - Bab Archibald
*Nigel Barrie - Carter Brooks
*Leone Morgan - Jane Gray
*Frank Losee - Mr. Archibald
*Isabel OMadigan - Mrs. Archibald
*Richard Barthelmess - Tommy Gray
*Helen Greene - Leila Archibald
*Guy Coombs - Harry
*John B. OBrien - Harold Valentine
*George Odell - The Butler

==Preservation status==
All three Babs films are now presumed Lost film|lost. 

==See also==
*List of lost films
*Babs Burglar
*Babs Matinee Idol

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 