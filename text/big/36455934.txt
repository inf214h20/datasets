The Only One (film)
 
{{Infobox film
| name           = The Only One
| image          = 
| caption        = 
| director       = Geoffrey Enthoven
| producer       = Mariano Vanhoof
| writer         = Jaak Boon Geoffrey Enthoven
| based on       = 
| starring       = 
| music          = 
| cinematography = Gerd Schelfhout
| editing        = Geoffrey Enthoven Philippe Ravoet
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Belgium
| language       = Dutch
| budget         = 
| gross          = 
}}

The Only One ( ) is a 2006 Belgian comedy-drama film. The film was directed by Geoffrey Enthoven and written by Jaak Boon and Enthoven. It was named best Belgian film of 2006 by the Belgian Film Critics Association winning the André Cavens Award, and received two awards at the International Filmfestival Mannheim-Heidelberg.

==Cast==
*Nand Buyl as Lucien Knops
*Marijke Pinoy as Sylvia
*Viviane de Muynck as Mathilde

==Plot==
Lucien (Nand Buyl) is a stubborn person in his eighties. After death of his wife, his daughter Gerda convinced him to move to her house. Of course, there are many strugglings. His way of life interferes with the rest of the residents. Furthermore, Lucien neglects many requests of Gerda as he does not want to accept orders from his daughter.

After the umpteenth incident, Gerda is almost overstrained and wants to put her father into a rest home. To her relief, Lucien announces he wont stay longer. The displeasure is he will move back to his own house, lying his new girlfriend will also move in. Gerda is frustrated as she thinks "the new girlfriend" will inherit and run away with the money Gerda already budgetted to buy a luxurious campervan to visit Spain. Gerda already tried to haggle money from Lucien in which she failed.

Lucien admits to his grandchild Julie he does not have a new girlfriend yet. Mathilde, the wife of his best friend Felix, will do the housekeeping. Once moved, it is clear Lucien did already had sexual affairs with Mathilde whilst his wife was still living. They even promised each other eternal love, but must keep it a secret until both of their partners are dead.

Living on his own seems to be tougher as he thought. Lucien is bored when Mathilde is not in. Only the visits of Julie please him, but one day she tells she will leave to France for her studies.

Things changes when Lucien gets a new lady next door named Sylvia (Marijke Pinoy). She is around 46 years old and intrigues Lucien. Thanks to her, Lucien revitalises. She teaches him how to do the housekeeping, gives him computer lessons. Gerda and Mathilde distrust Sylvia. When the friendship between Lucien and Sylvia is having its best time, Lucien takes a drastic decision and breaks with Sylvia in the advantage of his own family.

==References==

 

==External links==
* 
* 

 

 
 
 
 
 


 