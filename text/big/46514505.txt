Vascodigama
{{Infobox film
| name           = Vascodigama
| image          = 
| caption        = 
| director       = Madhu Chandra
| producer       = Ashwin Vijay Kumar
| writer         = Madhu Chandra
| screenplay     =  Kishore Parvathy Nair
| narrator       = 
| music          = Poornachandra Tejaswi
| cinematography = Karm Chawla
| editing        = Saikanth
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Kishore in the lead role alongside Parvathy Nair, who plays a pivotal role. The film deals with the education system in India. 

==Cast== Kishore
* Parvathy Nair
* Ashwin Vijay Kumar

==Soundtrack==
{{Infobox album
| Name        = Vascodigama
| Type        = Soundtrack
| Artist      = Poornachandra Tejaswi
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 9 April 2015
| Recorded    =  Feature film soundtrack
| Length      = 24:12
| Label       = Lahari Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Poornachandra Tejaswi composed the films background score and music for its soundtrack, with the lyrics written by Madhu Chandra, Shivkumara Swamy and Yogaraj Bhat. The soundtrack album consists of six tracks.  It was released officially on 9 April 2015 in Bangalore. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 24:12
| lyrics_credits = yes
| title1 = Sa Re Ga Ma
| lyrics1 = Madhu Chandra
| extra1 = Vijay Prakash, Naveen Sajju, Arun M. C.
| length1 = 4:39
| title2 = 20 20
| lyrics2 = Madhu Chandra
| extra2 = Nagalinge Gowda, Bappi Blossom
| length2 = 3:58
| title3 = Hello
| lyrics3 = Madhu Chandra, Shivkumara Swamy
| extra3 = Bappi Blossom, Naveen Sajju
| length3 = 4:06
| title4 = Once More
| lyrics4 = Yogaraj Bhat Tippu
| length4 = 4:27
| title5 = Goli Hodi
| lyrics5 = Madhu Chandra
| extra5 = Shankar Mahadevan, Naveen Sajju, Lawrence, Pancham
| length5 = 4:32
| title6 = Spider Man
| lyrics6 = Madhu Chandra
| extra6 = Bappi Blossom
| length6 = 2:30
}}

==References==
 

 
 
 

 