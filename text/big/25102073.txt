The Guard (2011 film)
 
 
{{Infobox film
| name           = The Guard
| image          = THEGUARDposter.JPG
| caption        = Theatrical release poster
| director       = John Michael McDonagh
| producer       = Chris Clark Flora Fernandez-Marengo Ed Guiney Andrew Lowe
| writer         = John Michael McDonagh
| starring       = Brendan Gleeson Don Cheadle Mark Strong Liam Cunningham Fionnula Flanagan Calexico
| cinematography = Larry Smith
| editing        = Chris Gill
| studio         = Reprisal Films Element Pictures Crescendo Productions Aegis Film Fund Prescience
| distributor    = Element Pictures   Sony Pictures Classics  
| released       =  
| runtime        = 92 minutes   96 minutes  
| country        = Ireland
| language       = English Irish
| budget         = $6 million   
| gross          = $19,560,274 
}}
The Guard is a 2011 English-language Irish comedy film written and directed by John Michael McDonagh, starring Brendan Gleeson, Don Cheadle, Mark Strong and Liam Cunningham. {{cite news
|url=http://www.reuters.com/article/filmNews/idUSTRE59T0KU20091030
|title=Cheadle & Gleeson Join Forces For The Guard’
|date=29 October 2009
|publisher=IFTN
|accessdate=18 November 2009}}  
{{cite web
|url=http://www.variety.com/article/VR1118010851.html?categoryId=19&cs=1&cache=false
|title=Gleeson, Cheadle join Guard
|last=Jaafar
|first=Ali
|date=4 November 2009 Variety
|accessdate=18 November 2009}}  The Wind that Shakes the Barley (2006) which previously held this status.   

==Plot==
Sergeant Gerry Boyle (Gleeson) is an officer of the Garda Síochána in Connemara in the west of Ireland. He is crass and confrontational, regularly indulging in drugs and alcohol even while on duty. He is also shown to have a softer side, showing concern for his ailing mother, Eileen (Flanagan).
 FBI agent Wendell Everett (Cheadle), sent to liaise with the Garda in hunting down four Irish drug traffickers, led by Francis Sheehy-Skeffington (Cunningham), who is believed to be awaiting a delivery of cocaine coming into Connemara by sea. Boyle recognises one of the men in Everetts presentation as the victim of the murder he and McBride had been investigating. Around the same time, McBride pulls over a car driven by Sheehy and his lieutenants Clive Cornell (Strong) and Liam OLeary (Wilmot) and is shot dead. McBrides wife, Gabriela (Čas), reports McBrides disappearance to Boyle, who promises to look into it.

The strait-laced Everett and the unorthodox Boyle are teamed up to track down Sheehy and his men but, while Everett makes the rounds, encountering language difficulties and uncooperative residents, Boyle has a sexual encounter with a pair of prostitutes (McElligott and Greene) at a hotel in town. On his way back from the hotel, Boyle spots McBrides Garda car at a "suicide hotspot" along the coast, but does not believe that McBride killed himself. Meeting Everett at a local bar, Boyle notices a CCTV camera and remembers that the original suspect in the murder case claimed to be frequenting the very same establishment at the time of the killing. Looking over the footage from the time of the murder, they see that the suspects alibi is valid – and Everett also spots Sheehy and Cornell at the bar at the same time. Meanwhile, Cornell delivers a payoff to the Garda inspectors to keep them off the case, but Sheehy believes that Boyle will not be so easily swayed, after he meets with Boyle to half-heartedly attempt blackmail and then to offer a bribe, which is refused.
 IRA and arranges its return to an IRA member. After having her last wish to hear a live pub band fulfilled, Boyles mother dies. Meeting at the bar again, Everett tells Boyle that Garda sources indicate Sheehys shipment will be coming into County Cork and that he is leaving to investigate. Returning home, Boyle is confronted in his living room by OLeary, sent by Sheehy to kill Boyle to prevent him from interfering with the shipment. Boyle pulls a gun (purloined from the cache) and kills OLeary, then calls Everett to tell him that he has learned that the Cork lead is a decoy arranged by corrupt officers. Boyle drives to the local dock where Sheehys vessel is berthed and Sheehys men are unloading the cocaine. Everett arrives and is persuaded to give Boyle covering fire as he moves to arrest Sheehy and Cornell. Boyle – taking a  flesh wound to the arm – kills Cornell before leaping onto the boat to deal with Sheehy. Everett’s gunfire sets the boat alight. Boyle shoots Sheehy and leaves him for dead in the main cabin as the boat explodes.

The next day, Everett looks out on the water where the boat sank, believing Boyle to be dead. Eugene, standing nearby, mentions that Boyle was a good swimmer, having claimed to have been placed fourth at the 1988 Summer Olympics in Seoul, a claim that Everett had incredulously dismissed. A young photographer (Kinlan) comments that it would be easy enough to look up. Everett smiles to himself as he remembers Boyle’s oblique remark that Sheehy’s backers would not forget Boyle’s actions, and that presumably, Boyle would have to disappear. The final song, John Denvers cover of "Leaving on a Jet Plane," reinforces this possibility, that Boyle left the area, and didnt die.

==Cast==
*Brendan Gleeson as Sergeant Gerry Boyle
*Don Cheadle as FBI Agent Wendell Everett
*Mark Strong as Clive Cornell
*Liam Cunningham as Francis Sheehy-Skeffington
*Fionnula Flanagan as Eileen Boyle
*Michael Og Lane as Eugene Moloney David Wilmot as Liam OLeary Sarah Greene as Sinéad Mulligan
*Rory Keenan as Garda Aidan McBride
*Dominique McElligott as Aoife OCarroll
*Katarina Čas as Gabriela McBride
*Dermot Healy as Jimmy Moody
*Pat Shortt as Colm Hennessy

==Production==

===Development=== Ned Kelly and as writer/director of the 2000 film The Second Death.  Cinematographer Larry Smith is known for his work on Eyes Wide Shut and production designer John-Paul Kelly for his work on Venus (film)|Venus.   

===Filming=== Leitir Móir Leitir Mealláin An Bord Scannán na hÉireann (Irish Film Board). 

International sales were handled by Metropolis Films and the film was released by Element Pictures Distribution in Ireland,  
{{cite news
|url=http://www.screendaily.com/news/production/uk-ireland/brendan-gleeson-joins-irish-comedy-thriller-the-guard/5007576.article
|title=Brendan Gleeson joins Irish comedy thriller The Guard
|last=Cooper
|first=Sarah
|date=2 November 2009
|publisher=Screen International
|accessdate=18 November 2009}}  
{{cite news
|url=http://www.braypeople.ie/entertainment/gleeson-and-cheadle-line-up-for-new-movie-1935324.html
|title=Gleeson and Cheadle line up for new movie
|last=Hayden
|first=Esther
|date=4 November 2009
|publisher=Bray People
|accessdate=18 November 2009}}  Optimum Releasing in the United Kingdom, Sony Pictures Classics in the United States and Alliance Films in Canada.

==Reception==
Reviews for The Guard were mainly positive.   On the review aggregator Metacritic, the film has a score of 78 out of 100, based on 29 critics, indicating "generally favorable reviews". 

In   policeman."   , David DArcy wrote: "As a director, McDonagh avoids the grand gesture and focuses on his web of odd characters that call to mind the comedies of Preston Sturges."  Justin Chang of Variety (magazine)|Variety wrote: "The film making crackles with energy, from Chris Gills crisp editing and Calexicos ever-inventive score to d.p. Larry Smiths dynamic camerawork, alternating between bright, almost candy-coloured interiors and shots of Galways grey, rugged landscape." 
 Ned Kelly screenwriter (and brother of In Bruges director Martin McDonagh|Martin), John Michael McDonagh... This confident film knows full well how funny it is, daring to provoke with unfettered unPCness a-plenty." 

===Accolades===
*  (Nominated)
*The Guardian: First Film award for 2012.  The Guard is the awards first non-British winner. 

==References==
 

==External links==
* 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 