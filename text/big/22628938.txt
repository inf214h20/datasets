The Rescue (1988 film)
{{Infobox_Film| name = The Rescue image = The_rescue_poster.jpg caption = Film Poster starring = Kevin Dillon Ned Vaughn Marc Price Charles Haid Christine Harnos Ian Giatti James Cromwell writer = Michael J. Henderson (uncredited) Jim Thomas John Thomas director = Ferdinand Fairfax producer = Laura Ziskin music = Bruce Broughton cinematography = Russell Boyd editing = David Holden Carroll Timothy OMeara studio = Touchstone Pictures Silver Screen Partners III distributor = Buena Vista Pictures released =  : 12 May 1989 runtime = 97 min preceded_by =  followed_by =  country = United States language = English budget =  gross = US$5,855,392
}}

The Rescue is a 1988 adventure film about a group of teenagers who infiltrate a North Korean prison to rescue their Navy SEAL fathers. It was written by Michael J. Henderson, Jim Thomas, and John Thomas, produced by Laura Ziskin, and directed by Ferdinand Fairfax. The film stars Kevin Dillon, Ned Vaughn, Marc Price, Charles Haid, Christine Harnos, Ian Giatti, and James Cromwell.

==Plot==
 
The film opens on a U.S. military base in South Korea. Personnel receives word that a U.S. submarine has become disabled in international waters near North Korea. A team of four Navy SEALS  travels to the underwater site of the sub, rescues the captain, and lays explosives. The captain is airlifted from the SEALS raft and taken to safety. Shortly after, the SEALS raft is intercepted by a North Korean helicopter just as the explosives detonate, destroying the submarine. The SEALS are captured and placed in prison cells in a North Korean fortress. Back home on the base, the SEALS children watch news of their fathers trial. The North Korean government alleges that the SEALS were engaged in espionage. Under North Korean law, if they are found guilty, they could be executed.

Max (Marc Price), the son of Admiral Rothman (James Cromwell), is an electronics whiz. By planting homemade bugs, he learns that a rescue mission is being planned. He shares this information with his friends, Shawn Howard (Ned Vaughn) and Adrian Phillips (Christine Harnos), whose fathers were among the captured SEALS. An official meeting is scheduled to take place on the base. Max plants a listening bug in the conference room so that he, Shawn, and Adrian can listen in via radio. In the meeting, a rescue mission is proposed, code-named "Operation Phoenix," involving infiltration of the North Korean base to rescue the SEALS. However, unwilling to risk aggravation of North Korea and its allies, the Secretary of the State insists that the operation is out of the question. Shawn Howard gives a tape of the meeting to J.J. Merrill (Kevin Dillon), a rebellious teenager whose father was also on the captured SEAL team. J.J. proposes that they carry out the mission themselves. Shawn and Max secretly copy the operation plans, and J.J. says he can get a boat from someone in the city. 

Dressed in drag, Max drives Adrians mothers car off the base past curfew, smuggling the three others in the back. At the harbor, J.J. learns that hes been ripped off, and they have no boat. He suggests they steal a motorboat docked next to a bar where its owners are busy getting drunk. Max bids the three farewell. The stealing of the boat goes sour, so Max creates a diversion by rigging the car to crash, but not before discovering that Shawns little brother, Bobby (Ian Giatti), was hiding in the trunk. Fearing for their safety, Max and Bobby get into the boat, and all five speed away under gunfire. The next morning, the five argue over whether or not to continue with their plan with Bobby and Max aboard, seeing as how Bobby is a liability, and Maxs father wasnt among the captured. Max convinces the group to continue onward and carry out the plan, him and Bobby included. 

Shortly after, the group evades a patrol boat and enters North Korean waters. In the village of Sang-Ri, near the fortress where the SEALS are held, the teens locate the hideout of Col. Kim Song (Melvin Wong) of South Korean Intelligence, who was intended to assist with Operation Phoenix. He is impressed with their bravery, but says that they have no chance without American Special Forces. He plans to escort them back home to safety. That night, the teens sneak out and continue on foot to the fortress. Travelling through a well and sewer system, they find a stash of smoke canisters and grenades. Going up through another well, they enter the base.

The group rigs fireworks on the base, originally intended for a "Workers Day" celebration, in order to create a diversion. J.J. steals a North Korean military uniform and enters the building where the SEALS are held. He gets into a brawl with the guard, but J.J.s father (Edward Albert) reaches through his cell bars and breaks the guards neck. J.J. sends a signal and Shawn and Max light the fireworks, now aimed downward at the North Korean base. J.J. frees the SEALS. Kim Song and his men show up at the base in disguise and aid the group in fighting its way out and back down the well. They slide down a long drain pipe and end up back in the village. They drive via truck to an airfield and secure a twin-engine cargo plane, which Adrians father (Timothy Carhart) pilots. Kim Song stays behind to hold off the North Koreans and give them time to take off. The aircraft is fired upon from North Korean anti-air guns, disabling one of its engines and causing a fuel leak. The plane crosses the border back into South Korea. The South Korean base scrambles fighter jets to intercept, fearing the plane is an intruder from the North. Bobby sticks out of the top hatch of the plane and flashes his Bruce Springsteen T-Shirt, identifying the passengers as American to the fighter jets. Approaching the runway at the base, the plane runs out of fuel, but Lt. Phillips is still able to make a safe landing. Triumphant, the group exits the plane and reunites with their families.

==Reception==
Due to not being a widely known film, it has no critic reviews on Rotten Tomatoes. However, it has received mixed to positive reviews from audiences. 

USA TODAYs Mike Clark gave it a one star out of four rating.

==Soundtrack==
In 2014, Intrada Records released a CD of the score composed and conducted by Bruce Broughton. 

# Main Title (4:31)
# Diving SEALS (1:02)
# Underwater Rescue (3:41)
# News (0:53)
# J.J. (1:18)
# Nightwork (2:28)
# Preparations (3:18)
# The Boat (2:53)
# Move It! (0:42)
# At The Border (0:55)
# Boat Chase (5:57)
# Into Korea More (3:55)
# The Monitor (0:57)
# Dejected (1:06)
# To The Prison (7:03)
# Spooling Around (0:53)
# The Rescue Begins (6:26)
# The Rescue (7:16)
# The Plane! The Plane! (6:42)
# The Landing And End Credits (5:03)
# J.J. (Alternate) (0:53)
# Armed Forces Radio (1:39)
# Almost Ready (Source) (2:23)
 
==References==
 

 
 
 
 
 
 