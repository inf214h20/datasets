Confessions of a Driving Instructor
{{Infobox film
| name           = Confessions of a Driving Instructor
| image_size     = 190px
| image	         = Confessions of a Driving Instructor FilmPoster.jpeg
| caption        = Theatrical release poster
| director       = Norman Cohen Greg Smith Michael Klinger
| writer         = Christopher Wood
| narrator       =
| starring       = Robin Askwith Antony Booth
| music          = Ed Welch
| cinematography = Ken Hodges
| editing        = Geoffrey Foot
| distributor    = Columbia pictures
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         =
}} Christopher Wood. 

==Premise==
This time Timothy joins his brother-in-laws driving school. Their school is soon in rivalry with a competing school, while Timothy finds himself involved in erotic adventures with his clients, secretary and landlady. His clients are a mix of the inept and the dangerous and, as usual, mayhem ensues. A rugby match is organised between the two schools, at which one of the rival schools instructors unknowingly swallows a powerful aphrodisiac and rampages around the field, an event that leads to the climactic car chase.

==Cast==
*Robin Askwith	.... 	Timothy Lea
*Antony Booth      .... 	Sidney Noggett
*Bill Maynard	.... 	Walter Lea
*Doris Hare	        .... 	Mrs Lea Sheila White .... Rosie Noggett
*Windsor Davies	.... 	Mr Truscott
*Liz Fraser	        .... 	Mrs Chalmers
*Irene Handl	.... 	Miss Slenderparts
*George Layton	.... 	Tony Bender
*Lynda Bellingham   ....    Mary Truscott
*Avril Angers       ....    Mrs. Truscott
*John Junkin        ....    Luigi
*Donald Hewlett     ....    Chief Examiner
*Sally Faulkner     .... Mrs. Dent
*Maxine Casson          ....    Avril Chalmers
*Chrissy Iddon          ....    Lady Snodley
*Ballard Berkeley	.... 	Lord Snodley
*Suzy Mandel	.... 	Mrs Hargreaves
*Damaris Hayman     ....    Tweedy Golfing Lady 
*Geoffrey Hughes    ....    Postman
*Lewis Collins      ....    Rugby Player

==Bibliography==
*  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 

 
 