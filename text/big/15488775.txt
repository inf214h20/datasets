Jannat (film)
 
 

{{Infobox film
| name           = Jannat
| image          = Jannat poster.jpg
| caption        = Theatrical release poster
| director       = Kunal Deshmukh
| producer       = Mahesh Bhatt  (Presenters)  Mukesh Bhatt
| story          = Vishesh Bhatt
| screenplay     = Kunal Deshmukh Vishesh Bhatt
| writer         = Sanjay Masoom  (dialogue) 
| starring       = Emraan Hashmi Sonal Chauhan
| music          = Original songs: Pritam Background Score Raju Singh lyricist        =  
| cinematography = Manoj Soni
| editing        = Deven Murudeshwar
| studio         = Vishesh Films
| distributor    = Percept Picture Company
| released       = 16 May 2008
| runtime        = 
| country        = India
| language       = Hindi
| budget         =  
| gross          =  (worldwide)
}}
  crime romance film directed by Kunal Deshmukh, and produced by Mukesh Bhatt. The film stars Emraan Hashmi opposite Sonal Chauhan in lead roles. It released on 16 May 2008, and received positive response from critics, and managed to do well at the box office and was a huge success worldwide.

==Plot==
The story revolves around a man who is caught in a quagmire of crime and consumerism as he struggles to find heaven on earth.

The story revolves around Arjun (Emraan Hashmi),  a street-smart young conman with an obsession for making a quick buck. He has a chance meeting with a girl at a mall and falls in love at first sight. Zoya was gazing at a ring placed inside showcase protected by glass and hoping to acquire this ring. He breaks the glass to grab the ring she was looking at, that led to catch him by the security guards patrolling in the mall. Zoya (Sonal Chauhan) gives him the reasons he was looking for to move out of his ordinary life and become rich for this girl. He steps up from playing small-time card games to becoming a bookie. Stuck in a triangle of sorts between the woman he loves and his addiction to make a quick buck, Arjun moves on from being a bookie to a runner for the mafia.

He steps into the world of Cricket match fixing, ensuring a match looks interesting, but the end result brings Arjun into limelight to the bigger, faster, better and more, until his dizzying rise attracts the attention of the police. Arjun has now to choose between the love of his life, and this new found success and power. As Arjun struggles to choose between the two, an underworld don (Javed Sheikh) offers the forbidden apple of limitless wealth in exchange for his soul, drawing him into his core entourage of money spinners. 
When Arjun declines his offer, the don has Arjun drawn into a plot where he is trapped as the man accused of killing a coach. With the police after him, Arjun manages to get into his best friends car. Both of them can now make off, but Arjun is stopped by Zoya who asks him to surrender. Convinced that he has not committed the crime, Zoya assures him they can fight the law. When the police arrive, Arjun raises his arms in a gesture of surrender. Zoya pleads with the inspector, who is a fair and just man, not to kill Arjun as he is ready to give himself up

Arjun is asked to drop his gun and when the gun is taken out and dropped, the zoyas ring is dropped along with it. Arjun, in a state of shock and blur, bends down to pick the ring, but the police mistakenly think he is reaching for the gun. They open fire, and Arjun is brutally killed. The film ends showing Zoya paying her shopping bill in a mall along with her child.

At the payment counter she reduces items from her shopping list as she cannot afford to pay for all the items. Just then her son willingly gives away an item for himself from the shopping list, explaining it can be bought some other time. She smiles at him; realising if Arjun wouldnt have been that greedy, he wouldve still been alive.

==Cast==

* Emraan Hashmi as Arjun Dixit
* Sameer Kochhar as ACP Ajay Raina
* Sonal Chauhan as Zoya mathur
* Vishal Malhotra as Vishal
* Javed Sheikh as Abu Ibrahim
* Vipin Sharma as Arjuns father
* Shakeel Khan as Shadaab
* Saif Devni as SD Group

==Soundtrack==
{{Infobox album |  
 Name = Jannat
| Type = Soundtrack
| Artist = Pritam
| Cover = Jannat.music.jpg
| Released = 2008
| Genre = Film soundtrack
| Length = 39:58
| Label = Sony BMG
| Producer = Vishesh Films Race 
(2008) Jannat 
(2008)
| Next album = Kismat Konnection (2008)
}}
The soundtrack of the film is composed by   by Egyptian artist Amr Diab).  

==Track listing==
Jannat soundtrack has been composed by Pritam & Kamran Ahmed. The soundtrack was highly popular after release with tracks Zara sa & Judai topping the charts. Songs are as follows.

{| class="wikitable"
|-
! Sr No !! Song !! Singer !! Music !! Length
|- KK || Pritam || 5:03
|-
| 2 || Judaai || Kamran Ahmed || Kamran Ahmed || 5:02
|-
| 3 || Haan Tu Hain || KK || Pritam || 5:25
|-
| 4 || Door Na Jaa || Rana Mazumdar || Pritam || 5:51
|-
| 5 || Jannat Jahan || Rupam Islam || Pritam || 3:43
|- Richa Sharma || Pritam || 4:48
|-
| 7 || Zara Sa (Power Ballad) || KK || Pritam || 5:28
|-
| 8 || Judaai (Kilogram Mix) || Kamran Ahmed || Pritam || 4:38
|}

Atta Khan of Planet Bollywood wrote about the soundtrack as In short Jannat has the hallmarks of Pritam at his very best; it is a vibrant, classy, and extremely enjoyable soundtrack that reaches out to the masses and therefore sells itself, just go and soak up its glorious rock infused sounds and then reflect on why we all love Pritam…talent is a word too easily used in this day and age but he clearly has it in abundance (controversy aside!) and gave an 8/10 rating.

== Reception ==
The film was received very well by all, specially by the critics & young audience, due to the soundtrack being at the top charts. The film collected in 5 weeks,  a considerable amount for its small budget and release. Noyon Jyoti Parasara of AOL India rated it 3.5 out of 5 and said, "When we have something in a movie that appeals to everyone, thats when we call it a masala entertainer – probably. Keeping this in mind, Jannat, with a decent measure of romance, thrill, comedy, tragedy and of course cricket is definitely a masala flick."  The film was eventually declared as a super hit, earning   worldwide.

==Sequel==
The sequel to Jannat was released on 4 May 2012, under the title of Jannat 2. The main direction/production crew remained the same. It was directed by Kunal Deshmukh, and produced by Mukesh Bhatt and Mahesh Bhatt once again. Emraan Hashmi also appeared as the male lead actor again, opposite Esha Gupta, with Randeep Hooda in a supporting role. The music for the sequel was composed by Pritam. It was also commercially successful and grossed  .

==References==
 

==External links==
*  
*  
*  
*  

 
 