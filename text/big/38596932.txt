Interrogating the Witnesses
{{Infobox film
| name           = Interrogating the Witnesses
| image          = 
| caption        = 
| director       = Gunther Scholz
| producer       = 
| writer         = Inge Meyer Gunther Scholz
| starring       = René Steinke
| music          = 
| cinematography = Claus Neumann
| editing        = 
| distributor    = 
| released       =  
| runtime        = 73 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Interrogating the Witnesses ( ) is a 1987 East German drama film directed by Gunther Scholz. It was entered into the 15th Moscow International Film Festival.   

==Cast==
* René Steinke as Max
* Mario Gericke as Rainer
* Anne Kasprik as Viola (as Anne Kasprzik)
* Christine Schorn as Beate Klapproth
* Franz Viehmann as Gunnar Strach
* Gudrun Okras as Oma Lotte

==References==
 

==External links==
*  

 
 
 
 
 
 
 