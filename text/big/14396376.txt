He Leads, Others Follow
 
{{Infobox film
| name           = He Leads, Others Follow
| image          = He Leads, Others Follow.jpg
| caption        = Theatrical poster to He Leads, Others Follow
| director       = Vincent P. Bryan Hal Roach
| producer       = Hal Roach
| writer         = H.M. Walker
| starring       = Harold Lloyd
| music          = 
| cinematography = Walter Lundin
| editing        = 
| studio         = Rolin Film Company
| distributor    = Pathe Exchange
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short comedy film starring Harold Lloyd.    It is presumed to be Lost film|lost. 

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels  
* Sammy Brooks
* Lige Conley (as Lige Cromley)
* Charles Inslee
* Dee Lampton
* Marie Mosquini
* Fred C. Newmeyer (as Fred Newmeyer)
* Billie Oakley
* H.L. OConnor Charles Stevenson (as Charles E. Stevenson)
* Noah Young

==See also==
*Harold Lloyd filmography
*List of lost films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 