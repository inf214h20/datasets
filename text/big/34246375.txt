Odd Squad (film)
 
 
{{Infobox film
 | name =   Odd Squad
 | image =  Odd Squad (film).png
 | caption =
 | director = Enzo Barboni
 | writer =
 | starring =  Johnny Dorelli Giuliano Gemma Vincent Gardenia
 | music =  Franco Micalizzi
 | cinematography =Romano Albani 	
 | editing =
| producer =Giovanni Di Clemente
 | distributor =
 | released =  1981
 | runtime =
 | awards =
 | country = Italy
 | language =
 | budget =
 }} 1981 Cinema Italian War war comedy film directed by Enzo Barboni.

== Cast ==
* 	Johnny Dorelli	: 	Lt. Federico Tocci
* 	Giuliano Gemma	: 	Lt. Joe Kirby
* 	Vincent Gardenia	: 	General Brigg
* 	Carmen Russo	: 	The Girl
* 	Eros Pagni	: 	Colonel
* 	Jackie Basehart	: 	Pvt. Kirk Jones
* 	Salvatore Borgese	: 	Salvatore Ficuzza
* 	Vincenzo Crocitti	: 	Soldier Meniconi Riccardo Garrone	: 	Lt. Rondi
* 	Jacques Herlin	:	French General
* 	Massimo Lopez	:	Italian soldier
* 	Riccardo Pizzuti	: 	Italian soldier
* 	Ivan Rassimov	:	Pvt. Russ Baxter

== Plot ==
After the Allied landing in Sicily, between the U.S. Army and the Italian Army there is an ancient Roman bridge that both want to blow up but they both do their best to not do it. 

== Reception ==
The film received mixed reviews. Morando Morandini praised the film as "pleasant and full of comical inventions".   Flaminio Di Biagi defined it as a "quite ramshackle little comedy". 

==References==
 

==External links==
* 
  

 
 
 
 
 
 
 


 
 