Winstanley (film)
{{Infobox film
| name           = Winstanley
| image          = Winstanley 1975 film poster.jpg
| image size     =
| caption        =
| director       = Kevin Brownlow Andrew Mollo
| producer       = Andrew Mollo Kevin Brownlow
| writer         = David Caute (novel) Kevin Brownlow
| narrator       =
| starring       = Miles Halliwell
| music          = Sergei Prokofiev
| cinematography = Ernest Vincze
| editing        = Sarah Ellis
| distributor    = 1975
| runtime        = 95 min.
| country        = United Kingdom
| language       = English
| budget         =
| preceded by    =
| followed by    =
}}
Winstanley is a film made in 1975 in the UK by Kevin Brownlow and Andrew Mollo, based on the 1961 David Caute novel Comrade Jacob.

This deals with some of the life story of the 17th Century social reformer and writer Gerrard Winstanley, who, along with a small band of followers known as the Diggers tried to establish a self-sufficient farming community on common land at Saint Georges Hill|St. Georges Hill ("Diggers Hill") near Cobham, Surrey. The community was one of the worlds first small-scale experiments in socialism/communism, and its ideas were copied elsewhere in England during the time of the Protectorate of Oliver Cromwell, but was quickly suppressed and in the end left only a legacy of ideas to inspire later generations of socialist theorists.

Great efforts were made to produce a film of high historical accuracy. Armour used was actual armour from the 1640s, borrowed from the Tower of London.   Real-life activist Sid Rawle played a Ranter (English Revolution period anarchist-type group).

The film was reissued on DVD and Blu-ray in 2009 by the British Film Institute (BFI), which funded the original project. 

== References ==
 

==External links==
* 
* 
*  by Kevin Brownlow (about the making of the film)

 
 
 
 
 
 
 
 


 