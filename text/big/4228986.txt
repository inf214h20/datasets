Hana and Alice
{{Infobox film
| name           = Hana & Alice (花とアリス)
| image          = Hanaandaliceposter.jpg
| writer         = Shunji Iwai
| starring       = Anne Suzuki Yû Aoi Tomohiro Kaku
| director       = Shunji Iwai
| producer       = Shunji Iwai
| distributor    = Toho Rockwell Eyes
| released       = March 13, 2004
| runtime        = 135 minutes Japanese
| music          = Shunji Iwai
}}
 Japanese teen HD digital high school.

Originally shot as a series of short films for the 30th anniversary of Kit Kat in Japan, it was later expanded into a feature film by Iwai and received theatrical release in Japan in 2004. It moved into theaters in other Asian territories later in 2004 and 2005, and into western film festivals, such as New York Asian Film Festival and Seattle International Film Festival.

A prequel, The Case of Hana & Alice, is released on February 2015. 

==Plot==
  dancing in Hana and Alice]]
When Alice develops a crush on a stranger at the train station, she offers her best friend, Hana, the strangers "half brother," Masashi. Hana declines, but after watching Masashi from a distance, she develops feelings for him. She stalks him by travelling on his regular train throughout the winter.

During the spring, Hana and Alice enrol Masashis high school. Hana learns that Masashi is a member of the story-telling club, which prompts her to join as a member. As she continues to track him secretly, she witnesses his crash into a garage door, which leaves him unconscious. As he awakes, he finds Hana leaning over him. She reveals that a blow to Masashis head has given him a case of amnesia and that she is his girlfriend. Hana and Masashi soon hang out as a couple while she continues deceiving him about their relationship. Alice becomes involved with Hanas lies by pretending she is Masashis ex-girlfriend.

Through a series of events, a love triangle unexpectedly develops between Hana, Alice and Masashi when Masashi falls in love with Alice whom he still believes is his ex. Masashi eventually learns Hanas lie about his amnesia and reacts accordingly, which tests Hana and Alices friendship and their relationships with people around them.

==Cast==
* Anne Suzuki - Hana Arai  
* Yū Aoi - Tetsuko "Alice" Arisugawa   
* Tomohiro Kaku - Masashi "Mr. Miya" Miyamoto  
* Shoko Aida - Kayo Arisugawa (Alices mother)   Hiroshi Abe - Boyfriend of Alices mother 
* Sei Hiraizumi - Kenji Kuroyanagi (Alices father) 
* Takao Osawa - Ryo Taguchi (Fashion Photographer) 
* Ryōko Hirosue - Fashion Shoot Coordinator
* Tae Kimura - Ballet Teacher   
* Ayumi Ito - Cameo   Mika Kano - Cameo (as herself)

==Awards==
* Best Actress: Yû Aoi, 2005 - Japanese Professional Movie Award

==Box office==
Box office gross:
*Japan: ¥2,490,000,000
*Opening Week Gross: ¥200,150,000
*In release: 12 Weeks

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 