Maazii
{{Infobox film
| name = Maazii
| image = Maazii.jpeg
| image_size = 220px
| caption = 
| director = Jaideep Chopra
| producer = Jaideep Chopra 
| writer = Sumeet Nijhawan, Shirish Sharma
| screenplay = Sumeet Nijhawan, Shirish Sharma
| story = 
| starring = Sumeet Nijhawan Mona Wasu
| music = Faizan Hussain and Agnel Roman
| cinematography = Surjodeep Ghosh
| editing = Rameshwar S Bhagat
| studio = Jaideep Chopra productions
| distributor = 
| released =  
| runtime = 130 minutes
| country = India
| language = Hindi
| budget =
| gross = 
}}

Maazii is an Indian bollywood film.,   directed by Jaideep Chopra and produced by Jaideep Chopra & Narendra Singh.  The title Maazii is an Urdu word meaning "the past".

==Cast==
*Sumeet Nijhawan  as Tarun Singh
*Mona Wasu as Shrishti Singh
*Manish Chaudhary as Gulab Singh
*Pankaj Tripathi as Satinder Rathi
*Mohammed Zeeshan Ayyub as Ashfaq
*Manav Kaushik as Satinder Bhati Zakir Hussain as Mahinder Singh
*Mukesh Rishi as Malhaan Singh
*Ashok Banthia as Satbeer Singh
*Devender Chaudhary as Rajinder Chaudhary Mohit Chauhan as Sushil Jain
*Pooja Bisht as Shanno
*Saanvi Sharma as Minal
*Vansh Chopra as Young Tarun

==Plot==
Maazii is essentially a thriller that travels from the hills of Mussoorie to the plains of western UP. It is a story about a couple Tarun (Sumit Nijhawan) and Shrishti (Mona Wasu) who have a near perfect relationship. However, by a twist of fate, an unfortunate accident occurs which brings Taruns life into the limelight and turns him into a hero overnight.
The events that follow not only shatter the peaceful life of the family but also put them in grave danger. But when his daughter, Minal (Saanvi Sharma) is kidnapped at the hands of an unknown assailant, Tarun will stop at nothing to get her back safely. He must now return to his roots and find out who is wreaking havoc in his life and what is the motive behind it.

==Filming== Rao Raj Jat Jagirdar of Kuchesar, near Meerut. The shooting was completed within the pre-decided schedule of 31 days from 29 October to 2 November 2012. The production budget was around Rs. Four Crores.

==Release==
The film was released across the country on 27 September 2013. However, being a debutorial venture without any big names attached to it, despite great critical acclaim, Maazii failed to get many screens. On 2 October, Besharams release wiped Maazii out of theaters, but since Besharam tanked, Maazii got another opportunity and was re-released in cinemas on 11 October 2013. 

==Critical acclaim==
Despite being Jaideep Chopras debut directorial, Maazii received good critical acclaim and his work as a director was appreciated. Renowned critic Subhash K. Jha, called it "the shocking surprise of the season." He also stated that Maazii "takes us back to the stylish thrillers of B.R. Chopra like Dhund and Ittefaq. Debutant Jaideep Chopras film is well-crafted and thoughtfully scripted. Its an original idea executed in a gripping language. Worth a dekko for its suspenseful aura."  

Rohit Vats from IBN7 gave Maazii a rating of 4/5 stars and was all praises for the film. He claimed that Maazii shows "a glimpse of the latent potential of the gangster genre" and was "not to be missed"    India Todays Faheem Ruhani mentioned in his review that "Director Jaideep Chopras debut directorial Maazii is that pleasant surprise which hits you once in a while."  

"Maazii" was also listed as one of the 10 "small films/non-star cast films that made it big" in 2013 in an article in DNA newspaper.  

==Controversies==
Despite good critic reviews, the film was not given many screens. In an interview, the producers of the film reported that several multiplex chains had demanded money in return for giving them shows.  This led to a wave of controversy with several other producers and directors coming forward to claim that they had faced similar extortion at the hands of multiplex chains.

==Music==
{{Infobox album
| Name = Maazii
| Type = Soundtrack
| Artist = Faizan Hussain-Agnel Roman
| Cover =
| Border =
| Alt =
| Caption =
| Released =  
| Recorded = 2012–2013 Feature film soundtrack
| Length = 24:52
| Language = Hindi
| Label = FilmyBox
}}

The music score of the film was composed by the duo Faizan Hussain-Agnel Roman, Maazii being their first commercial film album. Faizan Hussain is the nephew of tabla maestro Ustad Zakir Hussain. Lyrics for the album were written by Swanand Kirkire and Arun Kumar. The preparation for the songs and selection of music started as early as May 2012. Recording of the songs began in October 2012.

The music album was launched on 30 August 2013. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 24:52
| title1 = Maula
| extra1 = Rahat Fateh Ali Khan,
| length1 = 4:59
| title2 = Mora Jiya
| extra2 = Rekha Bhardwaj
| length2 = 4:24
| title3 = Zindagi
| extra3 = Shalmali Kholgade, Nikhil Paul George
| length3 = 4:13
| title4 = Totta
| extra4 = Mika, Manjeera Ganguly
| length4 = 4:27
| title5 = Mora Jiya Instrumental
| extra5 = Niladri Kumar, Rakesh Chaurasiya
| length5 = 3:33
| title6 = Totta Remix
| extra6 = Mika, Manjeera Ganguly
| length6 = 3:18}}

==Nominations and Awards==
{| class="wikitable" style
|-
! Award
! Category
! Recipients and Nominees
! Outcome
|-
| IBN LIVE Film Awards 
| Best Independent Film 2013 
| Jaideep Chopra for Maazii 
| Won
|-
| Dada Saheb Phalke Film Festival 2014
| Best Film Category
| Jaideep Chopra for Maazii
| Official Selection/Nomination
|-
| Sunset Film Festival Los Angeles 2014
| Foreign Film Category
| Jaideep Chopra for Maazii
| Won "Honorable Mention"
|-
| Indie Film Fest California 2014
| Foreign Independent Film Category
| Jaideep Chopra for Maazii
| Won "Award of Merit"
|-
| Indian Cine Film Festival 2014
| Best Film Category
| Jaideep Chopra for Maazii
| Special Festival Mention
|-
| Radio Mirchi Music Awards
| Best Raag Based Song 2013
| Rekha Bhardwaj for Mora Jiya
| Nominated
|}

==References==
 

==External links==
* 
*   at Bollywood Hungama

 
 
 