The Stag (film)
{{Infobox film
| name           = The Stag 
| image          = The Stag film.jpg
| caption        = 
| alt            = 
| director       = John Butler
| producer       = 
| writer         = John Butler   Peter McDonald
| starring       = 
| music          = 
| cinematography = Peter Robertson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Ireland
| language       = English
| budget         = €600,000 (estimated)
| gross          = 
}} Irish film directed by John Butler in his feature début and written by Butler and Peter McDonald. 
 
==Plot==
A stag weekend in the great outdoors in the west of Ireland takes some unexpected detours.
 stag do, the Hen. Ruth, the now concerned bride-to-be (Amy Huberman), promptly persuades the, marginally more-macho, best man (Andrew Scott) to organise one. Reluctantly, he agrees but proceeds to do everything he can to stop Ruths wildly infamous brother, known only as The Machine (Peter McDonald), coming along for their sober, walking-weekend, excuse for a stag party. But The Machine, not so easily foxed, tracks them down, and what follows is a hilarious few days in rural Ireland where the Stags find themselves lost, shot at, stoned and butt-naked. The Stag is a hilarious and heart-warming journey of friendship, fear, male bonding, and tightly fashioned squirrel skin!" 

==Cast== Andrew Scott as Davin
* Hugh OConor as Fionnán Peter McDonald as The Machine (Richard) Brian Gleeson as Simon Andrew Bennett as Large Kevin Michael Legge as Little Kevin
* Amy Huberman as Ruth

==Release==
After being included in the line-up at the Toronto Film Festival in September 2013,  the film was released in Ireland on 7 March 2014 and 14 March in the UK. It received its US première at the 2014 Tribeca Film Festival in April. For the US release the film was retitled The Bachelor Weekend. 

==Reception ==
Rotten Tomatoes gives the film a score of 81% based on reviews from 26 critics. 

Mark Kermode gave the film 3/5 stars. 

Ryan McNeil described it as an unexpected gem with some of the most honest performances youre likely to see, and more brains, heart, and courage than any Hollywood comedy dare put forward. 

==Awards==
The Stag has been nominated for best Irish film at the 11th Irish Film & Television Awards. 

==References==
 

==External links==
*  
*  

 
 
 
 
 