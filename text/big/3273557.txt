General Idi Amin Dada: A Self Portrait
 
{{Infobox film
| name           = General Idi Amin Dada: A Self Portrait
| image          = General Idi Amin Dada DVD.jpg
| image_size     =
| caption        =
| director       = Barbet Schroeder
| producer       = Jean-François Chauvel Charles-Henri Favrod Jean-Pierre Rassam
| writer         = Barbet Schroeder
| narrator       =
| starring       = Idi Amin
| music          = Idi Amin
| cinematography = Néstor Almendros
| editing        = Denise de Casabianca
| distributor    = Le Figaro Films Mara Films TV Recontre
| released       =  
| runtime        = 90 min
| country        = France Switzerland English French French
| budget         =
}} French director Barbet Schroeder with English dialogue. It was made with the support and participation of its subject, the Ugandan dictator Idi Amin. The film depicts Amin at the height of his power as the ruler of Uganda. 

==Plot synopsis==
The film is an extended character study of its subject. It follows Amin closely in a series of formal and informal settings, combined with several short interviews in which Amin expounds his unconventional theories of politics, economics, and international relations. Amin is seen supervising the Ugandan  .

Included in the film are many candid scenes of Amin and his military in action: the paratroopers practice their exercises on a slide similar to those that would be found in a childrens playground; a welcoming committee of villagers is forced to flee the dust and backdraft from Amins helicopter as it lands; a cabinet member picks his nose with the end of a pencil during one of Amins speeches in a cabinet meeting. In one sequence, Amin upbraids his cabinet ministers for their failure to represent Uganda "correctly" to the world. Even while remonstrating with his foreign minister for his public-relations failures, he is jocular and joking as always &mdash; two weeks later, the documentary points out, the foreign ministers body was found floating in the River Nile.

==Influence and participation of Idi Amin==
Director Barbet Schroeder has characterized the film as a "portrait|self-portrait" by Amin. While Schroeder and cameraman Nestor Almendros were given unprecedented access to Amins daily life, the documentary makes it plain that many of the events (including the residents of a garrison town turning out en masse to greet Amin) were staged for their benefit. In several sequences, Amin actively directs the cameraman to particular points of interest, at one point shouting to "film that helicopter!"

However, Amins influence as a "director" went beyond the actual filming of Idi Amin Dada. As per his agreement with Amin, Barbet Schroeder made two versions of his documentary: the first, an hour-long cut, was released in Uganda and delivered directly to Amin, who was apparently pleased with the result. The second version was released only outside Uganda and contained an additional half-hour of footage and narration. 
 Britain to transcript of its contents. Amin soon sent a letter to Schroeder requesting additional cuts to the film, but Schroeder refused. In response, Amin rounded up almost 200 French citizens living in Uganda and confined them to a hotel surrounded by the Ugandan army, supplying them with Schroeders home telephone number and explaining that their release was conditional on Schroeders acquiescence. In the face of this dilemma, Schroeder made the requested cuts, replacing the 2½ minutes of excised footage with title cards crediting the gaps to Amin. On Amins fall from power, Schroeder restored the missing material, and most versions seen today contain the full footage.

==External links==
*  
*  
*   on Google Video (Flash Video)
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 