The Wayward Cloud
{{Infobox film
|  name        = The Wayward Cloud
|  image       = The_Wayward_Cloud_Poster.jpg
|  caption     = The Wayward Cloud film poster
|  writer      = Tsai Ming-liang
|  starring    = Lee Kang-sheng Chen Shiang-chyi
|  director    = Tsai Ming-liang
|  producer    = Bruno Pésery
|  distributor = 20th Century Fox  (Taiwan)  Axiom Films  
|  released    =  
|  runtime     = 112 minutes
|  country     = Taiwan
|  language    = Mandarin
|  budget      = 
|  music       = 
| film name = {{Film name|  jianti      = 天边一朵云
 |  fanti       = 天邊一朵雲
 |  pinyin      = tiānbiān yī duǒ yún}}
}}
The Wayward Cloud ( ) is a 2005 Taiwanese film directed by Tsai Ming-liang. The cast includes Lee Kang-sheng and Chen Shiang-chyi.

==Plot==
There is a water shortage in Taiwan, and television programs are teaching various water-saving methods and encouraging the drinking of watermelon juice in place of water.

Hsiao-Kang (Lee) and Shiang-chyi (Chen), two characters from one of Tsais previous films, What Time Is It There?, meet again by chance and start a relationship. However, Shiang-chyi does not know that Hsiao-Kang is a pornographic film actor.

==Production==
The Wayward Cloud was filmed in Kaohsiung, Taiwan. The film was shot in various landmark locations in the area, including Dragon and Tiger Pagodas and Love River.

==Release==
The film grossed more than NT$20 million in its theatrical release in Taiwan;  it was a big commercial achievement for the Taiwan film industry since most Taiwan films ticket sales usually total under NT$1 million in recent years.  It has a 75% rating on Rotten Tomatoes. 

The film was Taiwans official entry for the 78th Academy Awards in the foreign-language category.

It was released on DVD by Strand Home Video in 2008. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 