Bury the Hatchet (film)
{{Infobox film
| name           = Bury the Hatchet
| image          =
| alt            =  
| caption        = Bury the Hatchet official poster
| director       = Aaron Walker
| producer       = George Ingmire Julie Gustafson
| writer         = Aaron Walker
| starring       = 
| music          = The Wild Tchoupitoulas Monk Boudreaux Preservation Hall Jazz Band  Dirty Dozen Brass Band Baby Dodds Trio George Winston  Jimmy Scott  Arvo Pärt  Donald Harrison Jr.
| cinematography = Aaron Walker
| editing        = Aaron Walker Tim Watson Amy Sanderson Joe Bini
| studio         = Cine-Marais Altaire Productions
| distributor    = 7th Art Releasing
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Bury the Hatchet is a documentary film directed by Aaron Walker.  The film is a portrait of the Mardi Gras Indians of New Orleans.

==Synopsis== Native Americans Native American influenced costumes that they sew over the course of the year.  When tribes meet instead of attacking each other with hatchets and knives, they battle over which Chief has the prettiest suit.

The film follows Big Chiefs Alfred Doucette, Victor Harris and Monk Boudreaux over the course of five years, both pre and post Hurricane Katrina, and is an exploration of their art and philosophies, as well as their struggles within their communities: harassment by the police, violence amongst themselves, gentrification of their neighborhoods, uninterested youth, old age and natural disaster.

Filmmaker Aaron Walker gained intimate entry into this often hidden New Orleans experience and discovered not only a fascinating and beautiful culture but endearing characters and a truly dramatic narrative.  The film is the story of the unique and endangered culture of New Orleans they represent—as bearers of tradition, artists, musicians, and warriors who have laid down their weapons, but not their determination to survive as a people.

With a celebratory soundtrack of New Orleans music and additional scoring by pianist George Winston, the film is an intimate entry into this often hidden New Orleans experience.

==Festivals== Hot Docs New Orleans Royal Anthropological Institute Festival of Ethnographic Film in Leeds, England.   The film has screened at numerous other festivals, including the Florida Film Festival  and the Margaret Mead Film Festival. 

== References ==
 
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 