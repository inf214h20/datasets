Wenn man baden geht auf Teneriffa
{{Infobox film
| name           = Wenn man baden geht auf Teneriffa
| image          = 
| image_size     = 
| caption        = 
| director       = Helmuth M. Backhaus
| producer       = Eva Rosskopf (executive producer)
| writer         = Helmuth M. Backhaus
| narrator       = 
| starring       = See below
| music          = Christian Bruhn
| cinematography = Gerhard Krüger
| editing        = Anneliese Artelt
| studio         = 
| distributor    = 
| released       = 1964
| runtime        = 
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Wenn man baden geht auf Teneriffa is a 1964 West German film directed by Helmuth M. Backhaus.

== Plot summary ==
 

== Cast ==
*Geneviève Cluny as Jutta
*Peter Kraus as Tom
*Gunnar Möller as Jens
*Corny Collins as Christa
*Richard Häussler as Erik Varnhagen
*Ursula Oberst as Bessy
*Helga Lehner as Bruni
*Karin Heske
*Hannes Stütz
*Ralph Persson
*Hans Elwenspoek
*Loni Heuser as Christas Mother
*Rolf Castell
*Horst Pasderski
*Katrin Teleky
*Heinz Erhardt as Tristan Wentzel

== Soundtrack ==
*Peter Kraus - "Wer Dich Sieht, Evelyn"

== External links ==
* 
* 

 
 
 
 
 


 
 