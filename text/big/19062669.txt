The Boy in the Striped Pyjamas (film)
{{Infobox film
| name = The Boy in the Striped Pajamas
| image = Theboyposter.jpg
| caption = Theatrical release poster
| director = Mark Herman
| producer = David Heyman
| screenplay = Mark Herman
| based on =  
| starring = {{Plainlist|
*Asa Butterfield
*Jack Scanlon
*David Thewlis
*Vera Farmiga
*Amber Beattie}}
| music = James Horner
| cinematography = Benoît Delhomme
| editing = Michael Ellis
| studio = {{Plainlist|
*Miramax Films
*BBC Films
*Heyday Films}} Walt Disney Studios Motion Pictures
| released =  
| runtime = 94 minutes
| country = United Kingdom
| language = English
| budget = $12.5 million
| gross = $40,416,563 
}} spelling differences) novel of the same name by Irish writer John Boyne.  Directed by Mark Herman, produced by Miramax Films, and distributed by Walt Disney Studios Motion Pictures, the film stars Asa Butterfield, Jack Scanlon, David Thewlis, Vera Farmiga, Amber Beattie and Rupert Friend.

This film is a Holocaust drama that explores the horror of a World War II Nazi extermination camp through the eyes of two 8-year-old boys; one the son of the camps Nazi commandant, the other a Jewish inmate.

==Plot==
In the films opening, set in Berlin, in Nazi Germany during the Holocaust, an 8-year-old boy named Bruno (Asa Butterfield) is seen playing with his three friends. After arriving home he learns that his father Ralf (David Thewlis) has been promoted. After a party to celebrate the promotion (at which Brunos paternal grandmother is shown to disapprove of Ralfs promotion and the move), Bruno, his father, his mother Elsa (Vera Farmiga), and his 12-year-old sister Gretel (Amber Beattie) relocate. Bruno hates his new home as there is nobody to play with and very little to explore. After commenting that he has spotted people working on what he thinks is a farm, he is also forbidden from playing in the back garden.
 Jim Norton), nationalist propaganda. German soldier. Bruno is confused about Nazi propaganda, because the Jews Bruno has seen, in particular the familys Jewish servant Pavel (David Hayman), do not resemble the caricatures in Liszts teachings.

Bruno one day disobeys his parents and sneaks off beyond the back garden. He eventually arrives at an electric, barbed wire fence surrounding a camp and befriends a boy his own age named Shmuel (Jack Scanlon), who lives on the inside and who asks for food. In the ensuing conversation, the pairs lack of knowledge as to the true nature of the camp is revealed, with Bruno thinking that the striped uniforms that Shmuel, Pavel, and the other prisoners all wear are "pyjamas" and Shmuel believing his grandparents had died from an illness contracted during their journey to the camp. Bruno starts meeting Shmuel regularly, sneaking him food and playing board games with him. Bruno eventually learns that Shmuel is a Jew and that he was brought to the camp along with his father.

 ]]
One day, Elsa discovers the reality of Ralfs assignment after Kotler lets slip that the source of the black smoke coming from the camps chimneys is the burning corpses of Jews. Elsa confronts and argues with Ralf and is disgusted and heartbroken. At dinner that night, after Bruno claims Herr Liszt wont let him read adventure books and that he mainly teaches him history, Kotler admits history was his favorite subject but that displeased his father, a professor of literature, who had moved to Switzerland. Ralf, upon hearing this, tells Kotler he should have informed the authorities of his fathers disagreement with the current political regime as it was his duty. The embarrassed Kotler then uses Pavels spilling of a wine glass as an excuse to beat the inmate to death. The next morning the maid, Maria, is seen cleaning up the blood stains.

Later that day Bruno sees Pavels replacement; Shmuel has been ordered to the house to clean glasses because of his small fingers. Bruno offers him some cake, and they start talking. Kotler appears, sees Shmuel chewing, and accuses him of stealing. Shmuel says Bruno offered him the cake, but fearful of Kotler, Bruno denies this, stating that he has never seen Shmuel before. Believing Bruno, Kotler orders Shmuel to finish cleaning the glasses and says they will then have a "little chat about what happens to rats who steal." Bruno goes to his room distraught and decides to apologize to Shmuel, but Shmuel has gone. Every day Bruno returns to the same spot by the camp but does not see Shmuel. Eventually Shmuel reappears behind the fence, sporting a black eye. Despite Brunos betrayal, Shmuel forgives him and renews his friendship.

After the funeral of Brunos grandmother, who was killed in Berlin by bombing, Ralf (after another argument with Elsa) decides that Bruno and Gretel are to stay with a relative while he "finishes his work" at the camp, accepting that it is no place for the children to live. Shmuel has problems of his own; his father has gone missing after those with whom he participated in a march did not return to the camp. Bruno decides to redeem himself by helping Shmuel find his father. The next day Bruno, who is due to leave that afternoon, arrives back at the camp, dons a striped prisoners outfit and a cap to cover his unshaven hair, and digs under the fence to join Shmuel in a search for Shmuels father. Bruno soon discovers the true nature of the camp after seeing many sick and weak-looking Jews. While searching one of the huts the boys are taken on a march with other inmates by Sonderkommandos.

At the house, Bruno being gone is noticed. After Gretel and Elsa discover the open window Bruno went through and the remains of a sandwich Bruno was taking for Shmuel, Elsa bursts into Ralfs meeting to alert him that Bruno is missing. Ralf and his men mount a search to find him. They find the boys discarded clothing outside the fence and the hole he dug, and enter the camp, searching for him. In the mean time, Bruno, Shmuel and the other inmates on the march are stopped inside a changing room and are told to remove their clothes for a "shower". They are packed into a gas chamber, where Bruno and Shmuel hold each others hands. An SS soldier pours some Zyklon B pellets into the chamber, and the prisoners start yelling and banging on the metal door. Ralf, still with his men, arrives at an empty dormitory, signalling to him that a gassing is taking place. Ralf cries out his sons name, and Elsa and Gretel fall to their knees, after discovering Brunos discarded clothes outside the gate. The film ends by showing the closed door of the now-silent gas chamber and the prisoners discarded clothing outside it indicating that the prisoners, Shmuel and Bruno are dead.

==Cast==
*Vera Farmiga as Elsa (Mother)
*David Thewlis as Ralf (Father)
*Amber Beattie as Gretel
*Asa Butterfield as Bruno
*Rupert Friend as Kurt Kotler
*David Hayman as Pavel
*Jack Scanlon as Shmuel
*Sheila Hancock as Nathalie (Grandma) Richard Johnson as Matthias (Grandpa)
*Cara Horgan as Maria (Housekeeper) Jim Norton as Herr Liszt

==Reception==

===Critical response===
The Boy in the Striped Pyjamas has a 63% with a 6.2/10 average rating on Rotten Tomatoes. James Christopher, of The Times, referred to the film as "a hugely affecting film. Important, too."  Manohla Dargis, of The New York Times, however, gave a negative review because it "trivialized, glossed over, kitsched up, commercially exploited and hijacked   for a tragedy about a Nazi family."   

===Historical accuracy===
Some critics have criticized the premise of the book and subsequent film. Reviewing the original book,   survivor friend that the book is "not just a lie and not just a fairytale, but a profanation." Blech acknowledges the objection that a "fable" need not be factually accurate; he counters that the book trivializes the conditions in and around the death camps and perpetuates the "myth that those   not directly involved can claim innocence," and thus undermines its moral authority. Students who read it, he warns, may believe the camps "werent that bad" if a boy could conduct a clandestine friendship with a Jewish captive of the same age, unaware of "the constant presence of death."   

But, according to statistics from the Labour Assignment Office, Auschwitz-Birkenau contained 619 living male children from one month to 14 years old on August 30, 1944. On January 14, 1945, 773 male children were registered as living at the camp. "The oldest children were 16, and 52 were less than 8 years of age. Some children were employed as camp messengers and were treated as a kind of curiosity, while every day an enormous number of children of all ages were killed in the gas chambers."  

Kathryn Hughes, whilst agreeing about the implausibility of the plot, argues that "Brunos innocence comes to stand for the willful refusal of all adult Germans to see what was going on under their noses."  American film critic, Roger Ebert, declared that the film is not attempting to be a forensic reconstruction of Germany during the war, but is "about a value system that survives like a virus." 

===Accolades===
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2008
| British Independent Film Award 
| Best Actress 
| Vera Farmiga
|  
|-
| Best Director
| Mark Herman
|  
|-
| Most Promising Newcomer
| Asa Butterfield
|  
|- 2009
| Premio Goya 
| Best European Film
| rowspan="2"|The Boy in the Striped Pyjamas
|  
|-
| Irish Film and Television Award 
| Best International Film
|  
|-
| Young Artist Award 
| Best Leading Performance (International Feature Film)
| Asa Butterfield & Jack Scanlon
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 