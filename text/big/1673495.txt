Stay Tuned (film)
 
{{Infobox film
| name           = Stay Tuned
| image          = Stay Tuned Poster.jpg
| director       = Peter Hyams Chuck Jones (animation)
| producer       = James G. Robinson
| writer         = Tom. S. Parker, Jim Jennewain and Richard Siegel (story) Tom S. Parker and Jim Jennewain (screenplay)
| starring       = John Ritter Pam Dawber Jeffrey Jones Eugene Levy
| music          = Bruce Broughton
| cinematography = Peter Hyams
| editing        = Peter E. Berger
| studio         = Morgan Creek Productions
| distributor    = Warner Bros. Pictures
| released       = August 14, 1992
| runtime        = 88 min.
| country        = USA English
| budget         =  
| gross          = $10,736,401 
}}
 adventure fantasy fantasy comedy film directed by Peter Hyams. It starred John Ritter, Pam Dawber, Jeffrey Jones, and Eugene Levy.  Tim Burton was originally chosen to be the director due to his art and style but left to direct Batman Returns. 

==Plot== 666 channels of programs one cannot view on the four big networks (CBS, NBC, American Broadcasting Company|ABC, and Fox Broadcasting Company|Fox). What Roy doesnt know is that Spike (later referred to as "Mephistopheles of the Cathode Ray") is an emissary from hell who wants to boost the influx of souls by arranging for TV junkies to be killed in the most gruesome and ironic situations imaginable. The candidates are sucked into a hellish television world, called Hell Vision, and put through a gauntlet where they must survive a number of satirical versions of sitcoms and movies. If they can survive for 24 hours they are free to go but if they get killed then their souls will become the property of Satan (the latter usually happens).

The dish eventually sucks Roy and Helen into this warped world. They are pursued by Spike, who enters some shows along with the Knables in order to halt their advance. Roy and Spike continue to fight throughout several shows, even in a cloak-and-dagger scenario where Roy displays his long-buried talent as a fencer. Through tenacity and sheer luck, the Knables keep surviving, and their young son Darryl (David Tom) recognizes his parents fighting for their lives on the TV set. He and his older sister Diane (Heather McComb) are able to provide important assistance from the real world. This infuriates Spike to the point that he makes good on Roys contract, releasing him but not Helen as she was not in the system under contract.

Roy re-enters the system to save Helen, bringing his own remote control with him, allowing them to control their journey. Roy confronts Spike in a Salt-N-Pepa music video, manages to get a hold of Spikes remote and uses it to save Helen from being run over by a train in a western movie. By pressing the "off" button on the remote, they are evicted from the TV set moments before it sucks their neighbors abusive Rottweiler into the TV and it destroys itself. In the end, Spike gets eliminated by the Rottweiler on the command of Crowley (Eugene Levy), a vengeful employee he banished to the system earlier and is then succeeded in his executive position by Pierce (Erik King), a younger upstart employee. Roy, who has learned a valuable lesson after his adventure, has dramatically cut back on his TV viewing, quit his job as a plumbing salesman, and opened his own fencing school.

==Cast==
*John Ritter as Roy Knable
*Pam Dawber as Helen Knable
*Jeffrey Jones as Spike
*David Tom as Darryl Knable
*Heather McComb as Diane Knable
*Bob Dishy as Murray Seidenbaum
*Eugene Levy as Crowley
*Erik King as Pierce
*Don Calfa as Wetzel
*Susan Blommaert as Ducker
*Don Pardo as Game Show Announcer
*Lou Albano as Ring Announcer George Gray as Mr. Gorgon
*Faith Minton as Mrs. Gorgon
*Laura Harris as Girlfriend #1
*Andrea Nemeth as Girlfriend #2
*Kristen Cloke as Velma
*Gianni Russo as Guido Dave Ward as Peasant Gene Davis as Frankensteinfeld
*Jerry Wasserman as Cop
*Shane Meier as Yogi Beer Kid
*Serge Houde as Yogi Beer Dad
*John Pyper-Ferguson as Inmate #2 Kevin McNulty as Inmate #3 Salt as Herself Pepa as Herself
*DJ Spinderella as Herself

==Reception==
The film was not screened for    The film received mixed reviews.

  

  

Variety (magazine)|Variety magazine said the film was "not diabolical enough for true black comedy, too scary and violent for kids lured by its PG rating and witless in its sendup of obsessive TV viewing...a picture with nothing for everybody"; it noted that the "six-minute cartoon interlude by the masterful Chuck Jones, with Ritter and Dawber portrayed as mice menaced by a robot cat...has a grace and depth sorely lacking in the rest of the movie." 
 Time Out called it "pointless satire" with the "emotional depth of a 30-second soap commercial." 

The film currently holds a 46% "Rotten" rating on Rotten Tomatoes. 

===Box Office===
The movie was not a box office success. 

==Parodies==
Some film and TV show parodies include:
*Threes Company – Reaps Company, At one point, Roy Knable stumbles through a channel onto the set of the television show that catapulted John Ritter to fame in the 1970s. Two women dressed as Chrissy Snow and Janet Wood enter and shout "Where have you been?". Roy screams in terror and changes the channel.
*Waynes World – Duanes Underworld in a twisted sketch show Saturday Night Dead (a parody of Saturday Night Live)  The Silence of the Lambs – Silencer of the Lambs commercial, a couple binds and gags their kids to keep them quiet during a car trip
*Three Men and a Baby, Rosemarys Baby (film)|Rosemarys Baby – Three Men and Rosemarys Baby 
*The Dukes of Hazzard – David Dukes of Hazzard
*Driving Miss Daisy – Driving Over Miss Daisy 
*Northern Exposure – Northern Overexposure 
*Lifestyles of the Rich and Famous – Autopsies of the Rich and Famous  The Exorcist – The Exorciseist
*Murder, She Wrote – Murder, She Likes
*Leave It to Beaver – Meet the Mansons
*Thirtysomething (TV series)|thirtysomething – thirtysomething-to-life  (in prison)
*Beverly Hills, 90210 – Beverly Hills, 90666
*I Love Lucy – I Love Lucifer 
*The Golden Girls – The Golden Ghouls
*Married... with Children – Unmarried With Children
*Fresh Prince of Bel-Air – Fresh Prince of Darkness The Facts of Life – Facts of Life Support
*My Three Sons – My Three Sons of Bitches
*Diffrent Strokes – Different Strokes (about two elderly men literally having strokes) World Wrestling Federation – Underworld Wrestling Foundation (includes a cameo by former professional wrestler/manager Lou Albano)
*  – Death Trek: The Next Generation (additional minor reference to  )
*  – This reference is made during a scene in Spikes control center. The center is modeled after the War Room and also features a Dr. Strangelove impersonator.
*Looney Tunes – Rooney Tunes, A cartoon, animated by Chuck Jones, depicting Roy and Helen as mice trying to evade a mechanical cat  (The name may also be a reference to the character played by Jeffrey Jones in Ferris Buellers Day Off, Principal Rooney.) Home Shopping Club – Home Shoplifting Channel

==Soundtrack==
{{Infobox album  
| Name        = Stay Tuned
| Type        = soundtrack
| Artist      = Various artists
| Cover       = Stay Tuned.jpg
| Released    = August 29, 1992
| Recorded    = 1992 Hip hop
| Length      = 35:39 Morgan Creek Hurby Luv Black Sheep, Jason Hunter, Ced Gee, LaVaba Mallison
}}
The soundtrack to the film is made up entirely of hip hop songs with the exception of the last two tracks, which were themes composed by Bruce Broughton. Tracks in bold are used in the movie.

===Track listing===
# Start Me Up - Salt-n-Pepa (4:45)  The Choice Black Sheep (3:22)
# Taste - Cherokee & Auto (4:07)
# Xodus - X-Clan (4:22) 
# Strobelite Honey - Black Sheep (3:07) 
# Message From the Boss - Ultramagnetic MCs (4:47) 
# The Mic Stalker - Doctor Ice (2:57)
# Bad, Bad, Bad - Kool Moe Dee (4:48)
# Darryls Dad - Bruce Broughton (1:17) 
# Stay Tuned (Main Theme) - Bruce Broughton (2:07)

===Score album===
Broughtons score was released in 2011 by Intrada.

# Main Title 2:57 
# Meet Darryl 1:03 
# The Dish 2:56 
# A Bumpy Ride 2:12 
# Sayonara, Mrs. Seidenbaum 0:33 
# Field Work 0:55 
# Gordon Bashing 2:04 
# It Ate My BMX 2:01 
# Wolf Attack 0:45 
# That’s My Bike! 2:53 
# Offering To Help 1:47 
# You Have Tits 1:35 
# Aim The Dish 0:30 
# Off With Your Wig 3:34 
# Darryl Breaks Through 0:52 
# Redemption 1:31 
# Roy Goes Back 1:10 
# The 3:10 To Yuma 1:55 
# Roy Gets Shot 0:53 
# Crashing In 0:32 
# The Big Sword Fight 1:19 
# Turn It Off! 1:50 
# So What Can I Tell You... 0:53 
# The Game Show 1:29 
# TV Theme Medley 3:32 
# Roy Knable, Private Dick 3:26 
# We’re Cartoons 6:42

==References==
 

==External links==
 
*  
*  
*  
*   from Los Angeles Times

 

 
 
 
 
 
 
 
 
 
 
 