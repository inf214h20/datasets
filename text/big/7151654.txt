Kurt Cobain: About a Son
 
{{Infobox film
| name           = Kurt Cobain: About a Son
| image          = Kurt Cobain About a Son cover.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = AJ Schnack
| producer       = Ravi Anne
| writer         = 
| narrator       = Kurt Cobain
| starring       = Kurt Cobain
| music          = Steve Fisk Ben Gibbard
| cinematography = Wyatt Troll
| editing        = AJ Schnack
| distributor    = Balcony Releasing
| released       =      
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Kurt Cobain: About a Son is a  , set over ambient cinematography of the places which Kurt Cobain called his home, mainly Aberdeen, Washington|Aberdeen, Olympia, Washington|Olympia, and Seattle, Washington|Seattle. The film played at numerous film festivals, and was nominated for the 2007 Independent Spirits Truer than Fiction Award. The DVD, which was released by Shout! Factory in February 2008, includes bonus interviews and commentary by Michael Azerrad and A.J. Schnack. Shout! Factory also put out the documentarys first Blu-ray edition on October 6, 2009.
==Soundtrack==
#Steve Fisk & Benjamin Gibbard – Overture
#"Motorcycle Song" - Arlo Guthrie Queen
#"Downed" - Cheap Trick
#"Eye Flys" - Melvins
#Audio: Punk Rock  MDC
#"Banned in D.C." - Bad Brains
#"Up Around the Bend" - Creedence Clearwater Revival
#"Kerosene" - Big Black
#"Put Some Sugar on It" - Half Japanese
#"Include Me Out" - Young Marble Giants 
#"Round Two" - Pasties Son of a Gun" - The Vaselines 
#"Graveyard" - Butthole Surfers
#Audio: Hardcore Was Dead 
#"Owners Lament" - Scratch Acid  Mudhoney 
#Audio: Car Radio  The Passenger" - Iggy Pop
#"Star Sign" - Teenage Fanclub
#"The Bourgeois Blues" - Lead Belly  New Orleans Instrumental No. 1" - R.E.M. 
#Audio: The Limelight 
#"The Man Who Sold the World" - David Bowie
Rock and Roll - Bother Nich
#"Museum" - Mark Lanegan
#"Indian Summer" - Ben Gibbard

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 


 