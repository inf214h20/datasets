Pecos Pest
 
 
{{Infobox Hollywood cartoon
|cartoon_name=Pecos Pest
|series=Tom and Jerry
|image=Pecostitle.jpg
|caption=Title Card
|director=William Hanna Joseph Barbera
|story_artist=William Hanna Joseph Barbera 
|animator= (uncredited)
|background_artist=Robert Gentle
|voice_actor=Shug Fisher
|musician=Scott Bradley 
|producer=Fred Quimby
|distributor=Metro-Goldwyn-Mayer
|release_date=November 11, 1955
|color_process=Technicolor
|runtime=6:40 English
|preceded_by=Smarty Cat
|followed_by=Thats My Mommy
}} animated Tom 1955 directed by William Hanna and Joseph Barbera scored by Scott Bradley and released in theaters on November 11, 1955 by Metro Goldwyn Mayer.

Pecos Pest was directed by Hanna and Barbera, and was the last Tom and Jerry cartoon released to be produced by Fred Quimby before he went into retirement. The cartoon was also the last Tom and Jerry cartoon produced in Academy format; all subsequent Tom and Jerry cartoons were released in CinemaScope format. It was animated by Ed Barge, Irven Spence, Lewis Marshall and Kenneth Muse with backgrounds by Robert Gentle. Uncle Pecos and his music were performed (uncredited) by Shug Fisher.

==Plot==
 
Jerry receives a telegram from his Uncle Pecos, and soon Uncle Pecos arrives. The mustached mouse (who stutters in a fashion similar to Porky Pig) gives Jerry a performance on his guitar, playing his new song, "Crambone" (his variation of the song "Frog Went A-Courting"), but his guitar string breaks.

Uncle Pecos asks Jerry if he has a guitar string, but Jerry shakes his head negatively. Pecos pokes his head out and spots a sleeping Tom, with whiskers that he can use to fix his guitar. He then walks up and plucks a whisker off the cats head, startling him awake and out of sleep. A shocked Jerry then comes to the rescue just in the nick of time, and quickly carries his reckless uncle away from Tom to his mousehole, all the while Pecos plays on his guitar with Tom in hot-pursuit. Jerry escapes into his mousehole, but accidentally bangs his uncles head on the wall just above the mousehole while doing so. Jerry quickly drags Uncle Pecos inside before Tom can get him. Uncle Pecos then thanks the cat for his "service" and Tom is confused. Tom then heads for the bathroom to look in the mirror when Pecos again strolls up to the cat and plucks a second whisker out from him after the first one snaps. Pecos plays more of his song until the whisker breaks. Tom, not wanting to be a cat without whiskers, flees in panic. He hides near a door and Pecos opens the door such that it turns back and hits Tom very hard, resulting in the door to break into pieces. Tom becomes noticeable when the door broke into pieces.  Pecos soon finds him, and tries to reason with the cat that he needs one of his whiskers to fix his guitar and also asking him to stay still, but Tom is intransigent in protecting his whiskers and tries to keep Jerrys uncle at bay. Pecos grabs a third whisker from Tom Cat despite Toms best efforts of self-defence, but it snaps after only a few notes. Tom frantically looks for somewhere to hide as Pecos searches for him, and Pecos soon plucks off Toms fourth whisker. Tom dashes in panic into a nearby closet and slams the door shut. Pecos then invites Jerry out to listen to his encore. Tom then steps outside of the closet and enjoys the music Pecos generates until the snap of a string is heard. Tom shrieks and retreats back inside the closet again, but Pecos attacks the door with an axe and saying that Tom knows that Pecos cannot perform without a guitar string, Tom finally surrenders with a white flag and then plucks his fifth whisker for Pecos as a sacrifice to keep him away.

Fortunately, Pecos has left for his performance. On the next day, Tom and Jerry watch his performance in their living room on TV. Pecos starts his performance, but while he is playing the guitar, the guitars string snaps again. A laughing Tom ended up aghast as he comically reaches out of the television screen, and pulls out Toms last whisker as Pecos gets to finale of his performance.

==Availability==
DVD
*Tom and Jerrys Greatest Chases, Vol. 3
*Tom and Jerry Spotlight Collection Vol. 1, Disc Two

==External links==
* 
* 

 

 
 
 
 
 
 