Ah Boys to Men 3: Frogmen
{{Infobox film
| name           = Ah Boys to Men 3: Frogmen free
| image          = AhBoysToMen3FrogmenPoster.jpg
| border         = Yes
| alt            =
| caption        = Theatrical release poster
| director       = Jack Neo
| producer       = 
| writer         = 
| starring       = Joshua Tan Wang Wei Liang Tosh Zhang Maxi Lim Wesley Wong
| music          = 
| cinematography =
| editing        = Yim Mun Chong
| studio         = J Team mm2 Entertainment
| distributor    = Golden Village Pictures
| released       =  
| runtime        = 148 minutes
| country        = Singapore
| language       = English Mandarin Hokkien Cantonese
| budget         = $2.85 Million
| gross          = 
}}
Ah Boys to Men 3: Frogmen (simplified Chinese: ,新兵正传III：蛙人传; traditional Chinese: 新兵正傳III：蛙人傳; pinyin: xīnbīng zhèngzhuàn III: wārénzhuàn; literally: "Recruits True Biography") is a 2015 Singaporean comedy film produced and directed by Jack Neo, and the third film in the Ah Boys to Men franchise. It was released in cinemas on 19 February 2015. The movie raked $2.83 million from box offices in four days, making it the Asian film with the highest ever box office takings in its opening weekend in Singapore. 

==Plot==
The film negates all happenings in the predecessors and explores what might have happened should the boys have been assigned to the Naval Diving Unit.

==Cast==
* Joshua Tan as Ken Chow
* Maxi Lim as Aloysius Jin Pseudonym|a.k.a. "Wayang King"
* Wang Wei Liang as Luo Bang aka Lobang
* Tosh Zhang as Warrant Alex Ong
* Charlie Goh as Wei Ming
* Wesley Wong as Hei Long
* Bunz as Sam
* Jaspers Lai as Handsome 
* Hanrey Low
* Justin Misson as Warrant Lum aka No. 2
* Rajid Ahamed as Balan
* Joey Leong as Lo Wei

==Production==
The film was shot at the Naval Diving Unit in Sembawang Camp.   
Directed by Jack Neo 梁智强
Screenplay by Jack Neo 梁智强 and Ivan Ho   夏友庆

==Reception==
The film is expected to be a hit with profits estimated to surpass $3,000,000 at the box office after release. 

==See also==
* List of Singaporean films of 2015
* Army Daze

==References==
 

==External links==
 
{{ external media
| align  = right
| width  = 210px
| image1 =   }}
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 