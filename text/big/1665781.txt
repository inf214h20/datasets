So Dear to My Heart
{{Infobox film
| name = So Dear to My Heart
| image = So Dear to My Heart poster.jpg
| caption = Theatrical release poster
| director = Harold D. Schuster Hamilton Luske
| producer = Walt Disney Perce Pearce Ken Anderson Marc Davis Bill Peet Maurice Rapf Ted Sears Novel Sterling North
| starring =Bobby Driscoll Luana Patten Beulah Bondi Burl Ives
| music = Eliot Daniel Irving Berlin
| cinematography = Winton C. Hoch Thomas Scott Walt Disney Productions RKO Radio Pictures, Inc.
| released =   |ref2= }}
| runtime = 82 minutes
| country = United States
| language = English
}}
So Dear to My Heart is a 1949 feature film produced by Walt Disney, whose world premiere was in Indianapolis on January 19, 1949, released by RKO Radio Pictures. Like 1946s Song of the South, the film combines animation and live action. It is based on the Sterling North book Midnight and Jeremiah.

==Plot==
Set in Indiana in 1903, the film tells the tale of Jeremiah Kincaid (Bobby Driscoll) and his determination to raise a black-wool lamb that had been rejected by its mother.  Jeremiah names the lamb Danny for the famed race horse, Dan Patch (who is also portrayed in the film). Jeremiahs dream of showing Danny at the Pike County Fair must overcome the obstinate objections of his loving—yet tough—grandmother Granny (Beulah Bondi). Jeremiahs confidant, Uncle Hiram (Burl Ives), is the boys steady ally. Inspired by the animated figures and stories, the boy perseveres. 

==Cast==
*Bobby Driscoll as Jeremiah Jerry Kincaid
*Luana Patten as Tildy
*Burl Ives as Uncle Hiram Douglas
*Beulah Bondi as Granny Kincaid Harry Carey
*Raymond Bond
*Walter Soderling
*Matt Willis
*Spelman B. Collins John Beal as Adult Jeremiah/Narrator
*Bob Stanton
*The Rhythmaires

===Voices=== Ken Carson as The Owl

==Awards==
The film was nominated for the Academy Award for Best Original Song for Burl Ivess version of the 17th-century English folk song "Lavender Blue," but lost to "Baby, Its Cold Outside" from Neptunes Daughter (1949 film)|Neptunes Daughter.
 Juvenile Award RKO melodrama The Window.)

==Production==
The train depot in the film was later relocated to Grizzly Flats Railroad. After the railroad closed, John Lasseter relocated it to his property.

==Release==
The film was re-released in 1964 and earned an estimated $1.5 million in rentals in North America. 

So Dear to My Heart was not released on home video until 1986. It was then re-released in 1992 and released on video in 1994 as part of the Walt Disney Masterpiece Collection. The film was originally planned for a US DVD release as part of the Walt Disney Gold Classic Collection, but was cancelled, with no particular reason given. Six years after seeing a region 2 DVD release, it was released in the US on DVD in July 2008 as a Disney Movie Club Exclusive.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 