The Crowd Roars (1938 film)
{{Infobox film
| name           = The Crowd Roars
| image_size     =
| image	         = File:The Crowd Roars lobby card.jpg
| caption        =Lobby card
| director       = Richard Thorpe
| producer       = Sam Zimbalist
| writer         = George Bruce (story and screenplay) Thomas Lennon George Oppenheimer
| narrator       = Robert Taylor Edward Arnold Frank Morgan
| music          =
| cinematography =
| editing        =
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Inc.
| released       =  
| runtime        = 87-92 minutes
| country        = United States
| language       = English
| budget         = $511,000  . 
| gross          = $2,032,000 
}} Robert Taylor as a boxer who gets entangled in the seamier side of the sport. It was remade in 1947 as Killer McCoy, featuring Mickey Rooney in the title role.

==Cast==
*Robert Taylor as Tommy "Killer" McCoy Edward Arnold as Jim Cain, aka James W. Carson
*Frank Morgan as Brian McCoy
*Maureen OSullivan as Sheila Carson
*William Gargan as Johnny Martin
*Lionel Stander as "Happy" Lane
*Jane Wyman as Vivian
*Nat Pendleton as "Pug" Walsh
*Charles D. Brown as Bill Thorne
*Gene Reynolds as Tommy McCoy as a boy
*Don "Red" Barry as Pete Mariola Donald Douglas as Murray
*Isabel Jewell as Mrs. Martin
*J. Farrell MacDonald as Father Patrick Ryan

==Reception==
According to MGM records the film earned $1,369,000 in the US and Canada and $663,000 elsewhere, resulting in a profit of $761,000. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 