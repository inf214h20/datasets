The Prizefighter and the Lady
{{Infobox film
| name           = The Prizefighter and the Lady
| image          = Poster of The Prizefighter and the Lady.jpg
| image_size     =
| caption        =
| director       = W. S. Van Dyke Howard Hawks (uncredited) David Townsend
| producer       = W. S. Van Dyke Hunt Stromberg John Meehan
| narrator       = Max Baer Primo Carnera Jack Dempsey
| music          =
| cinematography = Lester White
| editing        = Robert Kern MGM
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 boxers Max Max Baer, Primo Carnera, and Jack Dempsey.

==Production background== world heavyweight boxing champion at the time of the films release, while Baer took the title away from him the following year.

Howard Hawks was the initial director, but left the set when he found he would be working with non-actor Baer, not Clark Gable. MGM replaced Hawks with W. S. Van Dyke. 
 John Meehan Academy Award for Best Writing, Original Story.

==Plot==
While working as a barroom bouncer (doorman)|bouncer, sailor Steve Morgan (Max Baer) impresses alcoholic ex-boxing manager "the Professor" (Walter Huston) with his skills. The Professor talks Steve into entering a prize fight with an up-and-coming boxer to make money for both of them.

While out training on the road, Steve is nearly run over by a speeding car that crashes into a ditch. He carries nightclub singer Belle Mercer (Myrna Loy) out of the wreckage. Though she is attracted to him, she refuses to have anything to do with Steve. He learns where she lives and goes to see her anyway. He is too cocky to be concerned when she reveals that she is the girlfriend of well-known gangster Willie Ryan (Otto Kruger). When Willie finds out, Belle reassures him she is in control of her emotions. Willie is not so certain about that, but is too shrewd to have Steve killed out of hand by his bodyguard, whom he jokingly calls his "Adopted Son" (Robert McWade). It turns out that he had cause for concern; Steve persuades Belle to marry him. Deeply in love with Belle himself and still hoping to get her back, Willie lets Steve live.

Steve quickly rises through the boxing ranks. However, he cannot keep from fooling around with other women. When Belle catches him in a lie, she tells him that she loves him, but if he cheats on her once more, she will leave him. While waiting for a bout for the heavyweight championship of the world, Steve performs in a musical revue. When Belle unexpectedly goes to his dressing room, she finds a woman hiding there. It is the end of their marriage. She gets her old job back with Willie.

Anxious to see the overconfident Steve humiliated, Willie finds out what is holding up the match with the current champion, Primo Carnera (playing himself), and pays $25,000 to set it up. When the Professor tries to get Steve to train properly (without women and liquor), Steve gets angry and slaps him, ending their partnership.

The championship bout is refereed by boxing promoter and former champion Jack Dempsey (himself). Belle, Willie and the Professor are all in attendance. For most of the ten-round fight, Steve gets pummeled by the much heavier Carnera. Finally, a distraught Belle urges the Professor to forget his wounded pride and go to Steves corner to provide much needed advice. With his old friend and his ex-wife rooting him on, a heartened Steve makes a furious comeback in the final rounds. The match ends in a draw; Carnera retains his title.

Later, Willie enters Belles nightclub dressing room and tells her she is fired. Then he brings Steve in and leaves the couple alone to reconcile.

==Cast==
*Myrna Loy as Belle
*Max Baer as Steve
*Primo Carnera as Carnera
*Jack Dempsey as Promotor
*Walter Huston as Professor
*Otto Kruger as Willie Ryan
*Vince Barnett as Bugsie
*Robert McWade as Adopted Son
*Muriel Evans as Linda
*Jean Howard as Show Girl

The Three Stooges are reported to have been in a deleted scene. 

==Crew== David Townsend - Art Director

==DVD release==
This film was released on DVD on June 27, 2011 by the Warner Archive Collection.

==In real life== dubbed version, Nazi Minister of Propaganda Joseph Goebbels had the film banned in Germany because, as one of his underlings stated, "the chief character is a Jewish boxer"    (Baers grandfather was Jewish). Baer contended, however, that "They didnt ban the picture because I have Jewish blood. They banned it because I knocked out Max Schmeling"   on June 8, 1933.

Baer defeated Carnera during their real-life June 14, 1934 fight after knocking him down a record 11 times. He was supposedly able to do this after watching Carneras fighting style during the movies filming. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 