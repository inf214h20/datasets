The Humble Man and the Chanteuse
{{Infobox film
| name           = The Humble Man and the Chanteuse 
| image          = 
| image_size     = 
| caption        = 
| director       = Ewald André Dupont 
| producer       = 
| writer         = Felix Hollaender (novel)   Max Glass   Ewald André Dupont   
| narrator       = 
| starring       = Lil Dagover   Olga Limburg   Margarete Kupfer   Hans Mierendorff
| music          = 
| editing        =
| cinematography = Werner Brandes
| studio         = Terra Film
| distributor    = Terra Film
| released       = 2 April 1925
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent film directed by E.A. Dupont and starring Lil Dagover, Olga Limburg and Margarete Kupfer. It was based on a novel by Felix Hollaender. 

The films art direction was by Oscar Friedrich Werndorff.

==Cast==
* Lil Dagover as Toni Seidewitz 
* Olga Limburg as Trude Wessely 
* Margarete Kupfer as Frau von Bülow 
* Hans Mierendorff as Fabrikant Liesegang 
* Georg Baselt   
* Paul Bildt as Liesegangs Diener 
* Gertrud de Lalsky as Frau Professor Müller-Osten 
* Karl Elzer    Robert Garrison   
* Harry Halm as Prinz 
* Martin Kettner as Theateragent 
* Arnold Korff as Intendant 
* Eberhard Leithoff as Der Demütige / der Kapellmeister 
* Adolf E. Licho   
* Harald Paulsen
* Louis Ralph as Raimondi, Arzt 
* Hans Sternberg Varietédirektor Pullmann

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 

 
 
 
 
 
 


 