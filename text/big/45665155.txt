Hypnotized (1910 film)
 
{{Infobox film
| name           = Hypnotized
| image          = Thanhouser Hypnotized.jpg
| caption        = An advertisement for the film in The Moving Picture World
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        =
| country        = United States English inter-titles
}}
 silent short short drama William Russells role in the film, the production and cast credits are unknown. The film was released on December 30, 1910, it was met with positive reviews. The film is presumed lost film|lost. 

== Plot ==
Though the film is presumed lost film|lost, a synopsis survives in The Moving Picture World from December 31, 1910. It states, "May Smalley is a simple little country girl with whom Jack, a youth whom she has known since childhood, is very much in love. When a traveling show, consisting of a hypnotist and a Hindu magician comes to the opera house in her little town, the two young people are among the other interested spectators who flock to see the performance. Mays youth and beauty attract the hypnotist, who plans to lure her away from her home. He sends May a message that he has a communication for her from the spirit world. Against the protest of Jack, her escort, May goes behind the scenes after the performance to meet with the great hypnotist, who fascinates her with his wiles. The hypnotist is an unscrupulous villain, and seeing that May is thoroughly impressed with his few tricks and considers him quite superhuman, he induces her to follow him when he leaves the town. How Jack proves himself to be a youth of resource as well as courage, the important part he played in Mays deliverance by the Hindu fakir, is well told by the picture. Finally the hypnotist is shown in his true light. May is disillusioned, and comes to decide that Jack is just about the kind of protection she needs in a world of uncertainty."  According to a reviewer, Jack comes up to the stage where the Hindu magician is and knocks him down. Jack takes his costume and follows the hypnotist and May to the hotel where he rescues her. 

== Cast == William Russell as the hypnotist. 

== Production == William Russell George Middleton, Grace Moore, John W. Noble, Anna Rosemond, Mrs. George Walters. 

This film was an example of the use of the character names Jack and May, which were be repeatedly used by Lonergan in various productions.    Bowers writes, "It developed that Lloyd F. Lonergan,  ... liked these names, and during the years to come used them again and again. One can imagine that it must have become a studio joke to decide who was to play Jack and who was to play May. In actuality, names such as Jack and May were used in printed synopses to keep track of who was who, but such names were usually not mentioned in the films subtitles. Patrons watching the picture in a theatre had not the foggiest idea whether the hero was named Jack, Bertram, or Ezekiel."  The first usage of the two leading character roles was in Dots and Dashes. 

==Release and reception ==
The single reel drama, approximately 1,000 feet long, was released on December 30, 1910.    The film was released in Britain under the title A Quack Hypnotist on April 26, 1913.  The film likely had a wide national release, known advertising theaters include those in Arizona,  Pennsylvania,  and Kansas.  Some theaters did not provide the production credits,  complicating identification with Selig Polyscopes Hypnotized (1912 film)|Hypnotized.  The film received a positive review in the Moving Picture World which said, "It is a good story, well told, and the audience seems intensely interested in it."  The New York Dramatic Mirror offered a review that neither commended or criticized the production in its summary review of the film. 

== References ==
 

 
 
 
 
 
 
 
 