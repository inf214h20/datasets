6 (film)
 
{{Infobox film
| name           = 6
| image          = Six Candles.jpg
| image_size     =
| caption        = Promotional poster
| director       = V. Z. Durai
| producer       =
| story          = V. Z. Durai
| screenplay     =
| writer         = Jeyamohan
| starring       = Shaam Poonam Kaur
| music          = Srikanth Deva
| cinematography = Krishnasamy
| editing        = N. Arun Kumar
| studio         =
| distributor    = Studio 9
| runtime        = 129 minutes
| released       = 
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
6 Melugu Vathigal (also known as 6 Candles or 6) is a 2013 Tamil crime thriller film written and directed by V. Z. Durai. It stars Shaam and Poonam Kaur in the lead roles. This film sees the lead character sport six different looks and going around six different states to solve a mystery.  The film was released on 20 September 2013 with positive reviews.

==Synopsis==
Ram is a young software engineer who lives with his wife, Lizzy, and their son, Gautham in Chennai. They lead a normal life up until one day when Ram and Lizzy lose Gautham at a crowded beach. The police are not very helpful as Gautham is not the child of anyone with influential. However, after much pestering, the local inspector has one of his men take Ram and Lizzy to the slums in north Chennai to meet a petty criminal, Nayanar, who knows about all the crimes done by the dwellers in his area. Initially, he pretends to not know anything. However, after Lizzy begs at his feet, he finally reveals that Gautham might have been taken to Nagari in neighbouring Andhra Pradesh to be sold to a local pimp, Krishna Rao. Ram then leaves for the pimps brothel with two friends but finds that Rao only deals with grown women and not little children.

Back in Chennai, Ram goes to meet Nayanaar again, who accidentally let slip that Gautham might have been taken to another town in Andhra Pradesh although he is unsure of it. Coincidentally, the taxi driver, Rangan, who drove Ram and Lizzy to the slums earlier meets Ram and tells him he has driven a businessman from that town back to his home after meeting Nayanaar a few years ago. The driver reluctantly agrees to take Ram there. Back in Andhra Pradesh, Ram and Rangan meet the businessman, Nallama Reddy who claims to lead an honest living by selling rice. However, Ram realizes Reddy is lying as the temple board states he is a quarry dealer. Ram holds Reddys grandson at gunpoint and forces Reddy to reveal where Gautham is. Reddy then tells Ram his son might have been taken to a butchery at a nearby town.

At the butchery, the local gangsters already know that Ram is coming for them. Ram is prepared to face their attack. While struggling with the gang, its leader tells Ram that he already killed Gautham and sold the other children he abducted to a child trafficker in Bhopal named Nikhil Motwani after Reddy told them Ram is coming. Once the gangsters are defeated, Ram searches for Gauthams body but instead finds the corpse of another boy. Realizing that the gangsters killed the wrong boy, Ram is confident that Gautham is being taken to Bhopal and leaves immediately.

At Bhopal, Ram and Rangan manage to arrange a meeting with Motwani by posing as businessmen, but the human trafficker recognizes Ram from the news. He tells Ram that Gautham is being held by his henchman, Diwakar, and they can only release the boy if they are paid 1 crore. With no other choice, Ram calls up Lizzy to prepare the money and is taken to Diwakers hideout. Unable to control himself after seeing a young school girl almost being raped, Ram saves her by fighting off Diwakars men and taking her to the Times of Indias headquarters. When Ram and Rangan return to the hideout, they find it empty. As they explore the place, they discover that Gautham and many other abducted children have been kept prisoned there all along. Ram becomes hysterical when he finds Gauthams shirt on the floor along with his scribblings on the wall.

Once they are back on the road, Ram phones Diwakar and begs him to return Gautham but is instead taunted at. Meanwhile, Lizzy calls Rangan to tell him that she had borrowed money from friends and has banked it into Diwakars account. When she finds out it is no longer of any use, she is devastated and faints. As Ram and Rangan prepare to leave, they are shot at by an assassin hired by Motwani. Ram survives but Rangan is fatally shot. Before dying, Rangan reveals to Ram that he was the driver hired to drive Gauthams abductors around Chennai in search of victims. He then pleads to Ram to not give up and continue searching for his son.

Now left alone, Ram remembers his final conversation with Diwakar, who told him Gautham might be sold in Lucknow or Calcutta. He goes to Calcutta first and searches for Gautham at every place where child labourers and minor sex workers are being kept. As he comes to accept the fact that Gautham is not in the city, he phones Lizzy who he has not spoken to in months. She asks him to come home as there is nothing they can do to find their son now. Ram refuses to give up and heads for Lucknow. In Lucknow, Ram is found sleeping at the roadside by a kind Muslim, Bhai, who takes him in and helps him find Gautham. Through one of his contacts, Bhai learns about a local crime boss who houses dozens of child labourers. Once there, the crime boss claims he does not deal in child trafficking and instead asks Ram to look for Diwakar in Bhopal. We are then shown in flashbacks how Ram managed to track down Diwakar, who let slip his new hiding spot during their last conversation, and killed him in a fit of anger before taking back his money. Shocked, the crime boss finally shows Ram his child labourers. Ram is too overwhelmed by the number of abducted children when each of them beg him to take them away from this place.

As Ram and Bhai prepare to leave, Ram notices that he dropped Gauthams shirt and they go back to get it back. Ram then sees one of the boys, who is blind, holding the shirt. As Ram observes the boys face closely, he realizes it is Gautham, who he did not recognize earlier as his son has changed beyond recognition. The father and son embrace, as Bhai thanks the Lord for his blessing.

==Cast==
*Shaam as Ram
*Poonam Kaur as Lizzy
*Master Vivethan as Gautham
*Anil Murali as Diwakar
*Nagineedu as Nallama Reddy Anjathe Sridhar as Nayanaar
*Munnar Ramesh as Rangan Archana
*Narayan
*Chandra
*P. S. Selvam

==Production== Anthony were signed on as cinematographer, music composer and editor respectively.  A special medical team headed by Dr. Selvam and comprising a physio, dietitian, yoga expert, meditation guide and a physical trainer wa also set up to monitor the lead actor Shaams health during the shoot as the role required him to go through physical changes. Early reports suggested that Priyamani would play a role, but she was replaced by Poonam Kaur. Malayalam actor Jagathy Sreekumar was also roped in to play a supporting role in the film.

To attain a look with big eyebags, Shaam spent more than a dozen sleepless nights, which resulted in a big swelling below his eyes, so much so that the actor could not even be recognised. Although his effort gained notice, several contemporary actors criticized him for taking such a risk.  For another look, Shaam reduced his weight from 89&nbsp;kg to 72&nbsp;kg and grew long hair and beard. 

==Soundtrack==
{{Infobox album
| Name = 6
| Type = Soundtrack
| Artist = Srikanth Deva
| Cover  = 
| Released = 5 December 2012 Feature film soundtrack
| Length = Tamil	
| Label = Saregama
| Producer = Srikanth Deva
| Reviews =
| Last album = Machan (2012 film)|Machan (2012)
| This album = 6 (2012)
| Next album = Ragalaipuram (2012)
}}
Soundtrack is composed by Srikanth Deva collaborating with V. Z. Durai for second time after Nepali (film)|Nepali. V. Z. Durai made his debut as a lyricist and wrote all the 6 songs. The audio was launched in 5 December 2012, the function was attended by Sneha (actress)|Sneha, Ameer, Namitha, Abbas, Bharath, Vimala Raman, Shaam, Poonam Kaur, Kannada actor Sudeep and many others.  Behindwoods wrote:"Barring one song, the album has Srikanth Deva stepping out of his comfort zone and showing his capabilities at something different". 

* Boyfriend Girlfriend – Sathyan
* Aagaayam bhoomikellam – Sathyan, Soundarya
* Unnai Naan Velvatha – Roopa, Nancy Vincent
* Bigulu Bigulu – Soundarya, V. Z. Durai
* Thedukindrathe – Haricharan

==Release== Zee Thamizh. The film was given a "U" certificate by the Indian Censor Board. After many postponements, this film was released on 20 September 2013.

===Critical reception===
The film received positive reviews.    called the film "tedious" and wrote "The only silver lining is Shaams performance, as father struggling to come to terms between his love for his son and situations life throws up".  Baradwaj Rangan wrote "it should make for riveting drama, a welcome change from the endless comedies we are subjected to. But a number of factors conspire to derail the narrative, beginning with the cast".  Behindwoods have 2.75 stars out of 5 and wrote "A movie with good research by the director, V.Z.Dhorai and earnest hard work by actor Shaam, which could have been tightened as a better product". 

==References==
 

==External links==
*  

 

 
 
 