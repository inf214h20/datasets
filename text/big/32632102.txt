Jig (film)
{{Infobox film
| name = Jig
| image = Jig film.jpg
| alt = 
| caption = 
| director = Sue Bourne
| producer = Sue Bourne
| writer = 
| starring = 
| music = Patrick Doyle
| cinematography = Joe Russell
| editing = Colin Monie
| studio = BBC Scotland Creative Scotland Head Gear Films
| distributor = Screen Media Films
| released =  
| runtime = 99 minutes
| country = United Kingdom
| language = English
| budget = 
| gross = £194,515 
}}
Jig is a 2011 documentary produced and directed by Sue Bourne about the world of Irish dance and the fortieth Irish Dancing World Championships, held in March 2010 in Glasgow.

==Soundtrack==
The score for Jig was written by Academy Award nominated English composer Patrick Doyle.  The soundtrack was released through Varèse Sarabande 12 July 2011.

==Reception==
The film received a mixed reception from critics. It currently holds a 61% rating on Rotten Tomatoes  and a score of 53/100 based on fourteen reviews on Metacritic. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 