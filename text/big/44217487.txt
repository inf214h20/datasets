Oke Maata
{{Infobox film
| name           = Oke Matta
| image          =
| caption        =
| director       = Muthyala Subbaiah
| producer       = Ambhika Krishna
| writer         =
| screenplay     =
| starring       = Upendra Ramya Krishnan Nagendra Babu Babu Mohan
| music          = Koti
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Telugu
}}
 2000 Cinema Indian Telugu Telugu film,  directed by Muthyala Subbaiah and produced by Ambhika Krishna. The film stars Upendra, Ramya Krishnan, Nagendra Babu and Babu Mohan in lead roles.   

==Cast==
*Upendra
*Ramya Krishnan
*Nagendra Babu
*Babu Mohan
*M. S. Narayana

==Soundtrack==
The music was composed by Koti. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Raja Raja Chola || Devan, Sujatha || Ghantadi Krishna || 4.12
|-
| 2 || Kolo Kolo Koilallu || Sukhwinder Singh, Gopika Poornima || Sirivennela Seetharama Shastry || 5.09
|- Chithra || Ravikumar || 3.49
|-
| 4 || Maa Manchi || Sukhwinder Singh, Shivaleela || Samavedam Shanmukha Sharma || 4.15
|-
| 5 || Raja Raja Chola || Devan, Sujatha || Ghantadi Krishna || 4.12
|-
| 6 || Kiss Me Kiss Me || Devi Sri Prasad, Anuradha Sriram || Chandrabose || 4.52
|}

==References==
 

 
 
 
 

 