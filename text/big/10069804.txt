Bloody Murder
 

{{Infobox Film
| name           = Bloody Murder
| image          =
| image_size     =
| caption        =
| director       = Ralph S. Portillo
| producers      = Ralph S. Portillo Jamie Elliott
| writer         = John R. Stevenson
| narrator       =
| starring       = Jessica Morris Peter Guillemette Michael Stone Patrick Cavanaugh Christelle Ford Tracy Pacheco
| music          = Steve Stern
| cinematography = Keith Holland
| editing        = Carlos Puente
| recorded       = 1998
| released       = September 12, 2000
| runtime        = 82 mins.
| country        = United States
| language       = English
| budget         =
| gross          = 
| preceded_by    =
}}
 
  teen slasher film written by John R. Stevenson and directed by Ralph S. Portillo. It was released on September 12, 2000. The film has been highly criticized since its release for being a blatant rip-off of the Friday the 13th (franchise)|Friday the 13th series, yet it still has fans stretching from the US to the UK. 

==Plot==
A group of teenage friends travel to Camp Placid Pines to be camp counselors for the summer, Julie (Jessica Morris), Dean (Michael Stone), Whitney (Tracy Pacheco), Jason (Justin Martin) and Tobe (Patrick Cavanaugh). Upon arrival, they meet their boss Patrick (Peter Guillemette), another counselor named Drew (Christelle Ford), who is teamed up as a co-counselor with Julie, and a few other counselors. The teenagers get to work, bringing in food, cleaning, etc. Julie gets a warning from the groundskeeper, Henry, who claims there is danger in these woods. Julie questions Patrick about it, and he brushes it off, saying that Henry is crazy. Late that night, the group sits around a campfire and decides to play a game of "Bloody Murder". One person is "It" and the rest of the group try to find "It" and whoever finds "It" must scream Bloody Murder and then everyone must run back to base before "It" can tag them. Everyone joins in, with Jason being it, and Jason and Dean play a prank on one of the counselors Brad (Dave Smigelski). After the game, Dean witnesses Jason (who is Julies boyfriend) making out with Whitney. We later see Jason getting dressed and is confronted by a figure...

The next morning, Julie begins asking her fellow counselors about the whereabouts of Jason. Dean tells Julie that Jason said something about "taking off for a few days", but Julie still grows worried. The following night, the counselors gather in the mess hall for a movie, and Whitney goes to grab some food from the kitchen and is stabbed to death by a man in a hockey mask. The next day, the counselors inform Patrick that Jason and Whitney have gone missing, and they call the Sheriff who interrogates the group. Everyone suspects Dean, because he is Whitneys ex-boyfriend and he has been acting suspiciously. Dean is not provided with an alibi, so he is taken in for questioning. Julie encounters Henry again who tells her about her dad and a name of Nelson. She emails her Dad about it. Brad is killed next by the masked murderer and he goes missing, and Dean is off the hook for now. 

Julie gets an email reply from her dad, and he claims he doesnt remember a man named Nelson. Jason is suspected next, considering he had a past with Brad. Julie is attacked by the hockey-masked killer in the woods and she flees to the road, where  tire. She hurries back to camp, trying not to act scared, and Dean is killed by the murderer. Back at the camp, Julie finds a photo of her dad at the camp, with a kid named Nelson Hammond. She searches him, and discovers that Nelson Hammond was almost killed in an accident involving the game "Bloody Murder". He then came back a few years later and killed one of the counselors that caused the prank and was sent to a mental institution. That night, Julie is attack again by a man and he chases her to the mess hall and she locks him in the freezer. But the man is just Jason, who actually did take off, worried that Dean would tell about him cheating on Julie with Whitney, and he was worried because the police were after him. Jason is taken in by the police. 

The body count ups as another counselor, Doug is finished off. Julies father arrives after hearing about the police, and Drew and Juliesfather walk to the lake, as Julie retreats to her cabin to gather some things. There, she deduces the killer is Drew when she sees that Drews father was Bill Anderson, the man that Nelson Hammond killed for revenge and that Drew is out for revenge. She races to the missing, and she accuses Drew. But she is proven wrong when the killer appears and attacks them. Drew is knocked out and Julie flees. Julie runs into Patrick, and then Patrick reveals he is the killer. He is really Nelson Hammond and is seeking revenge for his accident that the other counselors caused. He attacks Julie with an axe, and chases her through the woods. She runs to the camp, and the other counselors, the police, Patrick and Julie gather. Patrick tries to make it seem, Julie hit her head and is delusional. But only Tobe believes Julie, and threatens to shoot Patrick, but finds out he has no bullets. Patrick swings at Julie, just as a shot rings out, hitting Patricks arm and it is revealed that Drew fired the shot. Patrick is taken in, Julies father is safe, and Julie starts having a love interest in Tobe. Once Sheriff Williams arrives at the police station with Patrick, he questions why he killed Doug, having nothing to do with Patricks scheme. Patrick then tells Sheriff Williams that he didnt kill Doug, and it must have been Trevor Moorehouse, hinting that the real Trevor is still out there.

Later, Julie and Jamie say their goodbyes and Julie breaks up with Jason in order to be with Tobe, angering Jason. As Jason is walking home alone, Trevor Moorehouse appears behind bushes wielding a chainsaw, and Jason screams in terror, before the screen cuts out.

==Reception==
Bloody Murder was generally criticized for the imitation and terrible blend of horror and comedy. It was ranked the 5th worst movie in history. "Bloody Murder does not have a single thing that should not be criticized." says New York Times critic." This movie is the most lousy movie I have ever saw in my lifetime." says Epoch times critic. It also has the 2nd lowest sales on movie history. A sequel titled   was released in 2003.

Another movie released in 2005 called The Graveyard takes place at Camp Placid Pines, but there is no appearance or mention of the Placid Pines killer Trevor Moorehouse.

==External links==
*  
*  

 
 
 
 
 
 