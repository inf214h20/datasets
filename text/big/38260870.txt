Brief Reunion
{{Infobox film
| name=Brief Reunion
| image=
| writer=John Daschbach
| starring= Joel de la Fuente Alexie Gilmore Scott Shepherd Quentin Mare Francie Swift
| director=John Daschbach
| producers=Ben Silberfarb  Andrew Lund
| distributor=Striped Entertainment
| released= 
| runtime=88 minutes
| language=English
| music=Michael Shaieb
| gross=$6,601 
}}

Brief Reunion is a 2011 American thriller film featuring Joel de la Fuente as Aaron, a successful New England entrepreneur who is haunted by the sudden arrival of a mysterious old friend from his past.  It was written and directed by John Daschbach and shot in New England.  It also stars Alexie Gilmore, Scott Shepherd, Francie Swift and Quentin Mare.

==Synopsis==
Aaron Clark lives a comfortable life in New England. He seems to have it all: financial security, a beautiful wife, and a close-knit circle of friends. But this peaceful existence is shattered by the sudden and unnerving arrival of Teddy, a former classmate and the proverbial snake. Teddy worms back into their lives, “befriending” Aaron’s wife and assistant.

He hijacks a 40th birthday surprise and systematically stalks Aaron — at home, at work, and in cyberspace. Bitter over Aaron’s success, Teddy pries into his business affairs, hinting at improprieties through a combination of extortion and revenge.

Relentlessly, he pushes toward destruction by unearthing and dissecting the past, until one day Aaron snaps. Now everybody asks, is Aaron
the man he appears to be? Or is it true that “Nobody really knows anybody, least of all themselves?”.

==References==

 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 


 