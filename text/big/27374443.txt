No Trifling with Love
 
{{Infobox film
| name           = No Trifling with Love
| image          = 
| caption        = 
| director       = Caroline Huppert
| producer       = Jean-Serge Breton
| writer         = Alfred de Musset
| starring       = Isabelle Huppert
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         = 
}}

No Trifling with Love ( ) is a 1977 French drama film directed by Caroline Huppert.    It is based on the theatrical work of Alfred de Musset of the same name.

==Cast==
* Isabelle Huppert - Camille
* Didier Haudepin - Perdican
* Sabine Haudepin - Rosette
* Jean Benguigui - Bridaine
* André Julien (actor)|André Julien - Le baron
* Evelyne Bouix - La choriste
* Yves Elliot - Blazius
* Monique Couturier - Dame Pluche
* Christian Pernot - Le choriste

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 