Pink Narcissus
 
{{Infobox film
| name           = Pink Narcissus
| image_size     =
| image	         = Pink Narcissus FilmPoster.jpeg
| caption        = James Bidgood
| producer       = James Bidgood
| writer         = James Bidgood
| cinematography = James Bidgood
| editing        = Martin Jay Sadoff
| music          = Martin Jay Sadoff Gary Goch
| starring       = Bobby Kendall
| distributor    = Sherpix Inc Strand Releasing (2003 re-release)
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = $27,000 (estimated)
}}
 James Bidgood visualizing the erotic fantasies of a gay male prostitute.

==Plot== Roman slave boy and the emperor who condemns him, and the keeper of a male harem for whom another male performs a belly dance.

==Production==
The movie is mostly shot on 8 mm film with bright, otherworldly lighting and intense colors. Aside from its last, climactic scene, which was shot in a downtown Manhattan loft, it was produced in its entirety (including outdoor scenes) in Bidgoods small New York apartment over a seven-year (from 1963 to 1970) period and ultimately released without the directors consent who therefore had himself credited as Anonymous.

==Provenance==
Because the name of the filmmaker was not widely known, there were rumors that Andy Warhol was behind it. In the mid-1990s, writer Bruce Benderson, who was obsessed with the film, began a search for its maker based on several leads and finally verified that it was James Bidgood, who was still living in Manhattan and was working on a film script.  In 1999, a book researched and written by Benderson was published by Taschen about Bidgoods body of photographic and filmic work.

Bidgoods unmistakably kitschy style has later been imitated and refined by artists such as Pierre et Gilles.

In 2003, the film was re-released by Strand Releasing.

== Music ==
*  
*  
*  

==References==
 

== External links ==
*  
*  

 
 
 
 
 

 