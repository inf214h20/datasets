Walk the Proud Land
{{Infobox film
  | name           = Walk the Proud Land
  | image          = Walk the Proud Land.jpg
  | caption        =
  | director       = Jesse Hibbs
  | producer       = Aaron Rosenberg
  | writer         = Gil Doud Jack Sher
| based on = Apache Agent: The Story of John P. Clum by Woodworth Clum
  | narrator       =
  | starring       = Audie Murphy Anne Bancroft Pat Crowley
  | music          =
  | cinematography = Harold Lipstein
  | editing        = Sherman Todd
  | distributor    = Universal-International (UI)
  | released       = September, 1956
  | runtime        = 89 min
  | country        = United States
  | language       = English
  | budget         =
  | gross          = $1.5 million (US)  
  | preceded_by    =
  | followed_by    =
}}
 1956 Western Western Technicolor CinemaScope film directed by Jesse Hibbs, starring Audie Murphy and future Academy Award winner Anne Bancroft. It was filmed at Old Tucson. 

==Plot==
 
This is the true story of Indian agent John Clum (Audie Murphy) as told by Clums son in the 1936 biography Apache Agent. The film begins in 1874, as Clum, an Eastern government representative, arrives in San Carlos, Arizona. He is sent to try a new approach to peace with Apaches based on respect for autonomy rather than submission to Army. He faces suspicions from the white settlers, the Army and the Indians, especially Geronimo. 

An Indian widow, Tianay (Anne Bancroft) falls in love with Clum, despite the fact he is engaged to Mary Dennison (Pat Crowley). Clum is helped by his Irish American friend, Tom Sweeney (Charles Drake).

==Cast==
* Audie Murphy as John Philip Clum
* Anne Bancroft as Tianay
* Pat Crowley as Mary Dennison
* Charles Drake as Tom Sweeny
* Tommy Rall as Taglito
* Robert Warwick as Chief Eskiminzin
* Jay Silverheels as Geronimo
* Eugene Mazzola as Tono Anthony Caruso as Disalin
* Victor Millan as Santos
* Ainslie Pryor as Capt. Larsen
* Eugene Iglesias as Chato
* Morris Ankrum as Gen. Wade
* Addison Richards as Gov. Safford
* Maurice Jara as Alchise

==Production==
The role of Mary Dennison, Clums fiancee, was originally offered to Piper Laurie but she turned it down so she could study at the Actors Studio in New York. Pat Crowley was cast instead.   accessed 15 June 2012 

==Reception==
The film was not a success at the box office, something attributed to the fact that Murphy played a pacifist rather than an action hero. This ended Murphys plans to make his dream project, a biopic of painter Charles Marion Russell. 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 