Aap Ke Saath
{{Infobox film
| name           = Aap Ke Saath
| image          = 
| caption        =
| director       = J. Om Prakash
| producer       = J. Om Prakash Sachin Bhawmik
| starring       = Anil Kapoor Vinod Mehra Smita Patil Rati Agnihotri Amrish Puri
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        =
| distributor    =  
| released       =  
| runtime        = 133 min 
| country        = India Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}
Aap Ke Saath is a 1986 Hindi language movie produced & directed by J. Om Prakash, starring Anil Kapoor, Vinod Mehra, Smita Patil, Rati Agnihotri, and Amrish Puri.

==Plot==
Ashok and Vimal are two grandsons of K.k. Vimal is womanizer. Ashok and Ganga are in love but when Ashok was about to propose Ganga for marriage, he finds that Ganga has complained against him in police station for sexual molestation. Ashok pays some money to Ganga and hope never to see her. K K bring home a girl named Deepa as K K want Deepa and Ashok to marry. But Deepa falls in love with Vimal. Vimal frequently visit a courtesan. When Ashok goes to courtesan to bring home Vimal, Ashok finds that courtesan is none other than Ganga.

==Cast==
*Anil Kapoor  as  Vimal
*Vinod Mehra  as  Ashok
*Smita Patil  as  Ganga
*Rati Agnihotri  as  Deepa/Salma
*Utpal Dutt  as  K K Sahib
*Amrish Puri  as  Persha

==Soundtrack==
{{Track listing
| extra_column = Singer(s)
| all_lyrics   = Anand Bakshi
| all_music    = Laxmikant–Pyarelal 
| title1       = Jind le gaya | extra1 = Lata Mangeshkar
}}title 2      - Mera naam Salma  -     (( Salma Agha ))
title  3         aap ke saath  -        (( shabbir kumar-anuradha paudwal))
title  4         naye saal ke  -        (( Shabbir kumar ))
title  5         chand chupta hai        (( shabbir kumar ))

==References==
 

== External links ==
*  

 
 
 
 


 