Death of a Superhero
{{Infobox film
| name           = Death of a Superhero
| image          = Death of a superhero.jpg
| alt            =  Ian FitzGibbon
| producer       = Astrid Kahmke Philipp Kreuzer, Michael Garland
| screenplay     = Anthony McCarten
| based on       =  
| starring       = Andy Serkis Thomas Sangster Aisling Loftus Michael McElhatton Sharon Horgan Jessica Schwarz
| music          = Marius Ruhland
| cinematography = Tom Fährmann
| editing        = Tony Cranstoun
| studio         = Bavaria Pictures Grand Pictures
| distributor    = Tribeca Film   NFP  
| released       =  
| runtime        = 99 minutes
| country        = Germany, Ireland
| language       = English

| gross          = $607
}} Ian FitzGibbon. The film stars Thomas Sangster alongside Andy Serkis. It tells the story of a dying 15-year-old boy who draws comic book stories of an invincible superhero as he struggles with his mortality.

==Plot==
After developing terminal cancer, 15 year old Donald (Sangster) slowly falls into a world occupied by his alter ego comic book superhero. Desperate to have sex at least once before he dies, Donald starts to see psychiatrist – Dr. Adrian King (Serkis).

==Cast==
* Andy Serkis as Dr. Adrian King Thomas Brodie-Sangster as Donald Clarke
* Sharon Horgan as Renata Clarke
* Aisling Loftus as Shelly
* Michael McElhatton as James Clarke
* Jessica Schwarz as Tanya
* Ben Harding as Michael 
* Killian Coyle as Hugo 
* Ronan Raftery as Jeff
* Jane Brennan as Dr. Rebecca Johnston

==Production==
Development of the project was announced in 2008, with it being reported that Anthony McCarten was to direct his adaptation of his own novel.    McCarten hoped the film would be shot in New Zealand after receiving German funding and also considered the big names it could draw in.     In February 2009, it was announced that Freddie Highmore was to join the cast as the central character and that filming was to begin later in the year.    After the involvement of Grand Pictures and the Irish Film Board, the films setting moved from New Zealand to Dublin, with Ian Fitzgibbon signing on as director.    Highmore left the role and was replaced by Thomas Sangster, with Andy Serkis also signing on. The film was shot throughout 2010 in Ireland, with principal photography wrapping in December 2010.    In October 2011, Tribeca Film purchased the North American distribution rights for the film during its world premiere at the Toronto Film Festival where it was an Official Selection.    The film will be released in 2012 after its US premiere at the Tribeca Film Festival. Prior to this the film won the Audience Award and Young Jury Award at the 2011 European Film Festival.    

==Reception==
The film received positive reviews, with Variety (magazine)|Variety praising the cast, specifically Sangster and Serkis. 
It won the Peoples Choice Award and the Young Jury Award at the 2011 Les Arcs European Film Festival.
The film has also won the Audience Award and ‘Special Mention’ of the Jury at the Mamer-en-Mars European Film Festival    

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 