11'09"01 September 11
 
{{multiple issues|
 
 
}}
{{Infobox film
| name = 1109"01 September 11
| image = September11Film.png
| caption =
| director =
| producer =
| writer =
| starring = See music See cinematography See editing See distributor
| released =  
| runtime = 135 minutes
| country = United Kingdom, France, Egypt, Japan, Mexico, United States, Iran
| language = Spanish, English, French, Arabic language|Arabic, Hebrew language|Hebrew, Persian language|Persian, French Sign Language
| budget =
| gross =
}} producer Alain Brigand. It has been released internationally with several different titles, depending on the language. It is listed in the Internet Movie Database as 1109"01 - September 11, while in French, it is known as 11 minutes 9 secondes 1 image and in Persian as 11-e-Septambr.

==Directors==
* Samira Makhmalbaf (segment "Iran")
*:In this segment, young Afghan schoolchildren discuss the Twin Towers collapse. Makhmalbaf has said, "When they asked me to talk about 11 September, I thought the whole world had representation except for Afghanistan, so I decided I would be their representative and tell it from their point of view. I didnt want to make it too judgemental. I wanted it to be innocent, through the eyes of the children." 
* Claude Lelouch (segment "France")
* Youssef Chahine (segment "Egypt")
* Danis Tanović (segment "Bosnia and Herzegovina|Bosnia-Herzegovina")
* Idrissa Ouedraogo (segment "Burkina Faso")
* Ken Loach (segment "United Kingdom")
*:This segment features Pablo, a Chilean singer-songwriter exiled in London, writing a letter to the American people condemning the terrorist attacks and telling the story of Salvador Allendes government and the tragic consequences of 1973 Chilean coup détat|Chiles own 9/11.
* Alejandro González Iñárritu (segment "Mexico") Amos Gitaï (segment "Israel")
* Mira Nair (segment "India")
* Sean Penn (segment "United States of America")
* Shōhei Imamura (segment "Japan")

==Awards== 2002 Venice Film Festival, the film received the UNESCO Award and Ken Loachs segment was the winner of the FIPRESCI Prize for Best Short Film.

==Producers==
* Alain Brigand (artistic producer)
* Jacques Perrin
* Nicolas Mauvernay
* Tania Zazulinsky (segment "France")
* Gabriel Khoury (segment "Egypt")
* Marianne Khoury (segment "Egypt")
* Čedomir Kolar (segment "Bosnia-Herzegovina")
* Nicolas Cand (segment "Burkina Faso")
* Rebecca OBrien (segment "United Kingdom")
* Alejandro González Iñárritu (segment "Mexico")
* Gustavo Santaolalla (segment "Mexico")
* Laurent Truchot (segment "Israel")
* Lydia Dean Pilcher (segment "India")
* Jon C. Scheide (segment "United States of America")
* Catherine Dussart (segment "Japan")
* Nobuyuki Kajikawa (segment "Japan")
* Masamichi Sawada (segment "Japan")
* Masato Shinada (segment "Japan")

==Writers==
* Samira Makhmalbaf (segment "Iran")
* Claude Lelouch (segment "France")
* Pierre Uytterhoeven (segment "France")
* Youssef Chahine (segment "Egypt")
* Danis Tanović (segment "Bosnia-Herzegovina")
* Idrissa Ouedraogo (segment "Burkina Faso")
* Paul Laverty (segment "United Kingdom")
* Ken Loach (segment "United Kingdom")
* Vladimir Vega (segment "United Kingdom")
* Alejandro González Iñárritu (segment "Mexico")
* Amos Gitaï (segment "Israel")
* Marie José Sanselme (segment "Israel")
* Sabrina Dhawan (segment "India")
* Sean Penn (segment "United States of America")
* Daisuke Tengan (segment "Japan")

==Music==
* Alexandre Desplat (title music)
* Mohammad-Reza Darvishi (segment "Iran")
* Manu Dibango (segment "Burkina Faso")
* Salif Keita (segment "Burkina Faso")
* Vladimir Vega (segment "United Kingdom")
* Osvaldo Golijov (segment "Mexico")
* Gustavo Santaolalla (segment "Mexico")
* Michael Brook (segment "United States of America")
* Heitor Pereira (segment "United States of America")
* Tarō Iwashiro (segment "Japan")

==Cinematography==
* Ebrahim Ghafori (segment "Iran")
* Pierre-William Glenn (segment "France")
* Mohsen Nasr (segment "Egypt")
* Mustafa Mustafić (segment "Bosnia-Herzegovina")
* Luc Drion (segment "Burkina Faso")
* Nigel Willoughby (segment "United Kingdom")
* Peter Hellmich (segment "United Kingdom")
* Jorge Müller Silva (segment "United Kingdom")
* Yoav Kosh (segment "Israel")
* Declan Quinn (segment "India")
* Samuel Bayer (segment "United States of America")
* Masakazu Oka (segment "Japan")
* Toshihiro Seino (segment "Japan")

==Editing==
* Mohsen Makhmalbaf (segment "Iran")
* Stéphane Mazalaigue (segment "France")
* Rashida Abdel Salam (segment "Egypt")
* Monique Rysselinck (segment "Bosnia-Herzegovina")
* Julia Gregory (segment "Burkina-Faso")
* Jonathan Morris (segment "United Kingdom")
* Alejandro González Iñárritu (segment "Mexico")
* Robert Duffy (segment "Mexico")
* Kim Bica (segment "Mexico")
* Kobi Netanel (segment "Israel")
* Allyson C. Johnson (segment "India")
* Jay Cassidy (segment "United States of America")
* Hajime Okayasu (segment "Japan")

==Distributors==
* Bac Films (2002) (France) (theatrical)
* BIM (2002) (Italy) (all media)
* Alfa Films (2003) (Argentina) (theatrical)
* Atrix Films (2002) (Germany) (all media)
* Bir Film (2003) (Turkey) (theatrical)
* Empire Pictures Inc. (2003) (USA) (all media)
* Europa Filmes (2003) (Brazil) (all media)
* Frenetic Films (2002) (Switzerland) (theatrical)
* Movienet (2002) (Germany) (theatrical)
* Scanbox Entertainment Finland Oy (2006) (Finland) (DVD)
* Tohokushinsha Film Corp. (2003) (Japan) (theatrical)

==References==
 

==External links==
* 
*  at Allmovie
* 
* 
* 
 
 
 
 
 
 
 
 
 
 
 
 