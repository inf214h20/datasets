The Screwdriver
 
{{Infobox Hollywood cartoon|
| cartoon_name        = The Screwdriver Woody Woodpecker
| image               = Screwdriver TITLE.jpg
| caption             =
| director            = Walter Lantz
| story_artist        = Ben Hardaway Jack Cosgriff
| animator            = Alex Lovy Ralph Somerville|R. Somerville
| voice_actor         = Mel Blanc
| musician            = Darrell Calker
| producer            = Walter Lantz
| studio              = Walter Lantz Productions Universal Pictures
| release_date        = August 11, 1941 (United States|U.S.)
| color_process       = Technicolor
| runtime             = 6 44"
| movie_language      = English Woody Woodpecker
| followed_by         = Pantry Panic
}}
 Universal Pictures.
 jalopy hes driving and his attempts to speed and to elude a persistent policeman.

== Notes ==
* Like most of the early 1940s Lantz "cartunes", The Screwdriver carried no directors credit. Lantz himself has claimed to have directed this film, which features animation by Alex Lovy and Ralph Somerville, a story by Ben Hardaway and Jack Cosgriff, and music by Darrell Calker.

* The song, performed by Mel Blanc at the beginning of the film, would make a reappearance in Hot Rod Huckster, performed by Grace Stafford.

:Everybody thinks Im screwy!
:Screwy as a loon, thats me!
:When I get on the driving spree,
:The coppers all get panicky!
:Honk my horn!
:HONK! HONK! HONK!
:Strip my gears!
:SCREECH! SCREECH! SCREECH!
:So Im screwy, what, what, what can I do?
:I ask you!

* The Screwdriver marks the last time Mel Blanc provided the voice for Woody. However, Woodys famous laugh and "Guess Who?!" signature line (provided by Blanc) would continue to be recycled in the succeeding shorts.

==Censorship==
The scene where Woody dressed up as a Chinese stereotype to trick the cop was removed.

== Quotes ==
(Woody ask a question about women to the cop.)

*WOODY::  Whats the color of Jeannies light brown hair? (Turns to audience) No coaching from the audience please

(The Cop is explaining what a speeder is)
*COP:: A speeder is a guy that goes *imitates speeding engine*
*WOODY:: You mean like this Pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt?
*COP:: Nah like this *imitates speeding engine*
*WOODY:: Ah I get it Pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-*Cuckoo*
*COP:: I didnt say Pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-*Cuckoo*. I said *imitates speeding engine*, and if you go Pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-pbbt-*Cuckoo* once more, Ill give you a ticket

== References ==
* Cooke, Jon, Komorowski, Thad, Shakarian, Pietro, and Tatay, Jack. " ". The Walter Lantz Cartune Encyclopedia. 

 
 
 
 
 
 
 