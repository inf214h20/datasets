Salute to the Marines
{{Infobox film
| name           = Salute to the Marines
| image          = Salmarpos.jpg
| image_size     =
| caption        = Original film poster
| director       = S. Sylvan Simon
| producer       =
| writer         =
| narrator       =
| starring       = Wallace Beery
| music          =
| cinematography =
| editing        =
| distributor    = MGM
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         =
}}
Salute to the Marines is a 1943 World War II film starring Wallace Beery.  The movie is set in the Philippines and shot in Technicolor (in Hollywood), and was directed by S. Sylvan Simon.
 NCO who, Ray Collins, Keye Luke, and Marilyn Maxwell.

==Cast==
*Wallace Beery as Sgt. Maj. William Bailey
*Fay Bainter as Jenny Bailey
*Reginald Owen as Henry Caspar Ray Collins as Col. John Mason
*Keye Luke as Flashy Logaz
*Marilyn Maxwell as Helen Bailey
*William Lundigan as Rufus Cleveland
*Donald Curtis as Randall James
*Noah Beery, Sr. as Adjutant
*Dick Curtis as Cpl. Moseley
*Russell Gleason as Pvt. Hanks
*Rose Hobart as Mrs. Carson Hugh Beaumont as Sergeant (uncredited) Robert Blake as Junior Carson (uncredited) Jim Davis as Pvt. Saunders (uncredited)
*Chester Gan as Japanese Officer

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 