List of Kannada films of 2001
 
  Kannada film industry in India in 2001 in film|2001, presented in alphabetical order.

==Top Grossing films of 2001==

* Huchha
* Chithra (film)|Chithra
* Kurigalu Saar Kurigalu
* Kothigalu Saar Kothigalu
* Nanna Preethiya Hudugi
* Kanasugara
* Baava Baamaida
* Jipuna Nanna Ganda
* Kotigobba
* Diggajaru
* Yuvaraja (film)|Yuvaraja
* Aunty Preethse

==Critically acclaimed films of 2001==
* Mathadana
* Asura (film)|Asura
* Shaapa
* Neela (film)|Neela

==List of released films==

{| class="wikitable"
|-
! Title
! Director
! Cast
! Music director
! Notes
|-
| Amma || D. Rajendra Babu || Ananth Nag, Lakshmi (actress)|Lakshmi, Jai Jagadish || M. M. Keeravani ||
|-
| Amma Nagamma || C. H. Balaji Singh Babu || Charan Raj, Damini, Ragasudha, Kumar Govind || Gopi Krishna || Oct 19 release
|-
| Amma Ninna Tholinalli || Naganna || Ramesh Aravind, Shruti (actress)|Shruti, Preethi, Bhavya, Avinash || Chaitanya || Sep 14 release
|-
| Anjali Geethanjali || S. Narayan || S. Narayan, Anu Prabhakar, Prema (actress)|Prema,  K. S. Ashwath || Prashanth Raj || April 20 release
|-
| Asura (2001 film)|Asura || S. Mahendar || Shivarajkumar, Damini, Ananth Nag, Raghuvaran, Doddanna || Gurukiran || Remake of Tamil film Amarkalam   March 23 release
|-
| Aunty Preethse || H. Vasu || Ananth Nag, Kushboo, Ramkumar, Anu Prabhakar, Balaraj, Anand || Chaitanya || Remake of Telugu film Aunty   May 25 release
|-
| Baanallu Neene Bhuviyallu Neene || S. Narayan || S. Narayan, Shashikumar, Divya Unni, Rekha Vedavyas || Prashanth Raj || Remake of Tamil film Veetla Visheshanga 
|-
| Baava Baamaida || Kishore Sarja || Shivarajkumar, Rambha (actress)|Rambha, Vinaya Prasad, Prakash Raj || Hamsalekha || Remake of Telugu film Bava Bavamaridi   August 23 release
|- Koti || Remake of Telugu film Chala Bagundi July 25 release
|-
| Chandana Chiguru || Rehman Pasha || Kumar Govind, Sudharani, Dattathreya, B. V. Radha || S. P. Chandrakanth || July 1 release
|-
| Chithra (film)|Chithra || Dinesh Baboo || Nagendra Prasad, Rekha Vedavyas, Ananth Nag || Gurukiran || Remake of Telugu film Chitram
|-
| Chitte || Dinesh Baboo || Aniruddh, Chaya Singh, Ananth Nag, Dwarakish, Damini || V. Manohar ||
|- Mansoor Ali Khan || Hamsalekha || Remake of Tamil film Natpukkaga   Jan 26 release
|- Kaalam Maari Pochu   March 30 release
|-
| Gatti Mela || S. Mahendar || S. Mahendar, Shruti (actress)|Shruti, Abhishek, Sharan (actor)|Sharan, Bhavana (Kannada actress)|Bhavana, Doddanna || Hamsalekha || March 2 release
|- Sai Kumar, Roja || Dhina || Nov 30 release
|-
| Haalu Sakkare || Yogish Hunsur || Devaraj, Shashikumar, Jaggesh, S. Narayan, Suhasini, Archana, Damini || L. N. Shastry ||
|-
| Hoo Anthiya Uhoo Anthiya || Praveen Kumar || Ramesh Aravind, Isha Koppikar, Anu Prabhakar || Karthik Raja ||
|-
| Huchha || Om Prakash Rao || Sudeep, Rekha Vedavyas, Bhavya, Avinash|| Rajesh Ramnath || Remake of Tamil film Sethu...   July 6 release
|-
| Jenu Goodu || S. Umesh || Devaraj, Kumar Govind, Shruti (actress)|Shruti, Sithara (actress)|Sithara, Umashree, Karibasavaiah || Prashanth Raj || Remake of Tamil film Koodi Vazhnthal Kodi Nanmai   August 10 release
|-
| Jipuna Nanna Ganda || J. G. Krishna || Jaggesh, Ravali (actress)|Ravali, Umashree, Komal Kumar || Sadhu Kokila || Remake of Tamil film Budget Padmanabhan   May 11 release
|- Deva || April 13 release
|-
| Jodi (2001 film)|Jodi || Kishore Sarja || Shivarajkumar, Jaggesh, Poonam, Doddanna, Mukhyamantri Chandru || Rajesh Ramanath || Remake of Malayalam film Darling Darling (2000 film)   Dec 7 release
|-
| Kalla Police ||  || Rajendra, Ruchita Prasad, Doddanna || Prashanth Raj
|- Srinath || Rajesh Ramnath || Remake of Tamil film Unnidathil Ennai Koduthen   August 3 release
|-
| Kanoonu || J G Krishna || Devaraj, Charan Raj, B. C. Patil, Jayanthi (actress)|Jayanthi, Anusha, Swarna || Sadhu Kokila || Sep 14 release
|- Prema || Hamsalekha || Dec 28 release
|- Deva || Remake of Tamil film Baashha   Nov 16 release
|-
| Kurigalu Saar Kurigalu || Rajendra Singh Babu || Ramesh Aravind, S. Narayan, Mohan Shankar, Ruchita Prasad, Bhavana (Kannada actress)|Bhavana, Ananth Nag, Umashree || Hamsalekha || March 23 release
|- Rami Reddy || Hamsalekha || Feb 23 release
|- Sharan || Koti - M. M. Srilekha || Remake of Telugu film Pellichesukundam   Feb 9 release
|-
| Mafia || Anand P Raju || Vinod Alva, Thriller Manju, Charan Raj, Abhijeeth, Ragasudha, Srilalitha || Gopi Krishna || 
|-
| Mahalakshmi || Karthik Raghunath || Ramesh Aravind, Kumar Govind, Shruti (actress)|Shruti, Srinivasa Murthy || N. Govardhan || Feb 16 release
|- Love Today August 31 release
|-
| Mathadana || T. N. Seetharam || Ananth Nag, Tara (actress)|Tara, Devaraj, Avinash || C. Ashwath, V. Manohar || Based on S. L. Bhyrappas novel   Jan 12 release
|-
| Maya Jinke || G K Purushottam || Shobharaj, Soujanya Hegde, Bank Janardhan || Naveen - Praveen || Jan 12 release
|-
| Mr. Harishchandra || R. C. Ranga || S. Narayan, Anu Prabhakar, Mohan Shankar, Darshan (actor)|Darshan, Daamini || Hamsalekha || August 31 release
|-
| Mysore Huli || Tiger Prabhakar || Tiger Prabhakar, Ragasudha, Joe Simon, Sathyajith || Tiger Prabhakar || Sep 21 release
|-
| Namma Samsara Ananda Sagara || J G Krishna || Shashikumar, Kumar Govind, Vinod Raj, Shwetha, Tara (Kannada actress)|Tara, Umashree, Archana || Prashanth Raj || Oct 26 release
|-
| Nanna Preethiya Hudugi || Nagathihalli Chandrashekar || Sameer Dattani|Dhyan, Deepali, Bhavya, Suresh Heblikar, Lokesh, Vijayalakshmi Singh || Mano Murthy || April 6 release
|-
| Narahanthaka || B R Keshav || Kumar Govind, Thriller Manju, Anusha, Abhishek || Maruthi Meerajkar || Sep 7 release
|-
| Neela (film)|Neela || T. S. Nagabharana || Gayatri Jayaraman, Naveen Mayur, Ananth Nag || Vijaya Bhaskar ||
|-
| Neelambari || Surya || Suman (actor)|Suman, Ramya Krishna, Prema (actress)|Prema, Charulatha, Devaraj || Rajesh Ramanath || 
|- Kasthuri || Mani Sharma || Remake of Telugu film Chiru Navvutho   Oct 5 release
|-
| Premi No.1 || Kodlu Ramakrishna || Ramesh Aravind, Prema (actress)|Prema, Srinivasa Murthy, Srinath (actor)|Srinath, Karibasavaiah || Gurukiran || Remake of Tamil film Darling, Darling, Darling   May 4 release
|- Sai Kumar, Vinod Raj, Bhavana (Kannada actress)|Bhavana, Manjula Sharma, Swarna || Sadhu Kokila || March 2 release
|-
| Rusthum || Om Sai Prakash || Jaggesh, Abhijeeth, Swathi, Lokesh, Komal Kumar || Sadhu Kokila || Oct 19 release
|-
| Satyameva Jayathe || H. Vasu || Devaraj, Archana, Shobharaj, Mukhyamantri Chandru || Gandharva  || Sep 12 release
|-
| Shaapa || Ashok Patil || Ramesh Aravind, Anu Prabhakar, B. C. Patil, K. S. Ashwath, Jai Jagadish || Hamsalekha || May 4 release
|- Jayanthi || Dhina || Oct 12 release
|-
| Shukradeshe || Vimal - Kathir || Jaggesh, Srilakshmi, Komal Kumar, Doddanna, Tennis Krishna || Vijay Anand || Jan 5 release
|-
| Sri Manjunatha || K. Raghavendra Rao || Arjun Sarja, Soundarya, Chiranjeevi, Meena Durairaj|Meena, Ambarish, Sumalatha, Vinod Raj, Kumar Govind, Abhijeeth, Sudharani, Dwarakish || Hamsalekha || A Kannada - Telugu bilingual   June 22 release
|-
| Sundara Kanda || M.S.Rajshekar || Shivarajkumar, Roja (actress)|Roja, Sujeetha, Ramesh Bhat, Shivaram || M. M. Keeravani || Remake of Tamil film Sundara Kandam   Oct 12 release
|-
| Supari || S. S. David || Charan Raj, Thriller Manju, Shobharaj, Hamsa, Lakshman || Shivamaya || 
|-
| Usire (film)|Usire || Prabhu Solomon|A. K. Prabhu || V. Ravichandran, Rachana Banerjee, Prakash Raj, Doddanna, Sadhu Kokila || Ilayaraja || Remake of Tamil film Bharathi Kannamma   April 6 release
|-
| Vaalee (film)|Vaalee || S. Mahendar || Sudeep, Poonam, Divyashree, Sadhu Kokila, Bank Janardhan || Rajesh Ramanath || Remake of Tamil film Vaali   Oct 19 release
|-
| Vishalakshmanna Ganda || Raj Kishore || S. Narayan, Anu Prabhakar, Pankaj, Doddanna || Prashanth Raj || Remake of Tamil film Vaettiya Madichu Kattu   Oct 26 release
|-
| Yarige Beda Duddu || Guruprasad || Abhijeeth, K. Shivaram, Tennis Krishna, Archana, Akhila || M N Krupakar || 
|- 
| Yuvaraja (film)|Yuvaraja || Puri Jagannadh || Shivarajkumar, Lisa Ray, Bhavna Pani, Kumar Govind, Avinash || Ramana Gogula || Remake of Telugu film Thammudu (film)|Thammudu   Nov 2 release
|}

==References==
*  

==External links==
*   at the Internet Movie Database

 
 

 
 
 
 
 