Ivan Oru Simham
{{Infobox film 
| name           = Ivan Oru Simham
| image          =
| caption        =
| director       = NP Suresh
| producer       = Purushan Alappuzha
| writer         = Alappuzha Karthikeyan Purushan Alappuzha (dialogues)
| screenplay     = Purushan Alappuzha
| starring       = Prem Nazir Srividya Sukumaran MG Soman
| music          = A. T. Ummer
| cinematography = J Williams
| editing        = NP Suresh
| studio         = Sreedevi Movies
| distributor    = Sreedevi Movies
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by NP Suresh and produced by Purushan Alappuzha. The film stars Prem Nazir, Srividya, Sukumaran and MG Soman in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prem Nazir
*Srividya
*Sukumaran
*MG Soman Reena
*Shanavas Sumithra
*Swapna

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kanmani Pookkaniyaay || S Janaki || Poovachal Khader || 
|-
| 2 || Kanmani Pookkaniyaay (Pathos) || S Janaki || Poovachal Khader || 
|-
| 3 || Radhike nin raasa nadanam || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 