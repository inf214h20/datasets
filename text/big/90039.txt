Flirting with Disaster (film)
{{Infobox film
| name           = Flirting with Disaster
| image          = Flirting with Disaster Poster.jpg
| caption        = Theatrical release poster
| director       = David O. Russell
| producer       = Dean Silvers
| writer         = David O. Russell
| starring       = Ben Stiller Patricia Arquette Téa Leoni Mary Tyler Moore George Segal Alan Alda Lily Tomlin
| music          = Stephen Endelman
| cinematography = Eric Alan Edwards
| editing        = Christopher Tellefsen
| distributor    = Miramax Films
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $14,702,438
}}

Flirting with Disaster is a 1996 American comedy film written and directed by David O. Russell about a young fathers search for his biological parents. The film stars Ben Stiller, Patricia Arquette, Téa Leoni, Mary Tyler Moore, George Segal, Alan Alda and Lily Tomlin. It was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.   

==Plot==
Mel Coplin and his wife, Nancy, live in New York, near Mels neurotic, Jewish, adoptive parents, Ed and Pearl Coplin.  Mel and Nancy have just had their first child, and Mel wont decide on a name for their son until he can discover the identity of his biological parents.  After an adoption agency employee locates his biological mothers name in a database, Mel decides to meet her personally.

Tina, the sexy but highly incompetent adoption agency employee, decides to accompany Mel, Nancy, and the newborn on a trip to San Diego to meet Mels biological mother.  The trip, of course, does not go as planned, and ends up becoming a tour of the United States.

First, Mel is introduced to Valerie, a blond Scandinavian woman with Confederate roots whose twin daughters are at least six inches taller than Mel. They quickly realize that Valerie is not Mels biological mother, and Tina scrambles to get the correct information from the agency database. Meanwhile, Nancy becomes jealous as Tina and Mel begin to flirt.

Next, the group heads to Battle Creek, Michigan with the hope of meeting the man whose name appears as the person who delivered infant Mel to the adoption agency. The man, Fritz Boudreau, turns out to be a trucker with a violent streak. However, when he discovers that Mel might be his son, he becomes instantly friendly and lets Mel drive his semi-trailer truck, which Mel immediately crashes into a Post Office building.
 ATF agents, gay and in a relationship with each other. It is discovered that Tony and Nancy went to high school together. Charges are dismissed, and Fritz Boudreau tells Mel that he is not Mels father, but only handled Mels adoption because Mels biological parents were indisposed.  Tina locates the current address of Mels biological parents, which turns out to be in rural New Mexico. Tony and Paul surprise everyone by deciding to tag along on the trip.
 adopted because they were in jail for making and distributing LSD in the late 1960s.  Not only that, but Richard and Mary continue to manufacture LSD, as becomes apparent when Lonnie, in an attempt to dose Mel with acid at dinner, accidentally doses Paul, the ATF agent.

In his drugged state Paul tries to arrest Richard and Mary but Lonnie knocks him out with a frying pan. They attempt to escape and decide to take Mels car, hiding their supply of acid in the trunk. Mels adoptive parents arrive but then change their minds and decide to leave, taking the wrong car. When they change their minds again and make a blind U-turn, the two families crash. Mels adoptive parents are arrested while his biological parents escape to Mexico. 

Not realizing what has happened Mel recounts the stories from dinner to Nancy and they agree to name the baby Jerry Garcia|Garcia. The next day Paul explains the situation and is able to get Mels parents released, and they are happy and reassured to hear Mel call them his parents. A montage of their relationships continues over the credits. They all still have their troubles but Mel and Nancy are happy together.

==Cast==
* Ben Stiller as Mel Coplin
* Patricia Arquette as Nancy Coplin
* Téa Leoni as Tina Kalb
* Mary Tyler Moore as Pearl Coplin
* George Segal as Ed Coplin
* Alan Alda as Richard Schlichting
* Lily Tomlin as Mary Schlichting
* Richard Jenkins as Paul Harmon
* Josh Brolin as Tony Kent
* Celia Weston as Valerie Swaney
* Glenn Fitzgerald as Lonnie Schlichting
* Beth Ostrosky as Jane
* David Patrick Kelly as Fritz Boudreau
* Nadia Dajani as Jill

==Reception==
 
Rotten Tomatoes rated the film as "Certified Fresh" giving it an 86% based on 51 reviews. 
American Film Institute recognition:
*AFIs 100 Years... 100 Laughs – Nominated 

==Soundtrack==
A soundtrack album was released on Geffen Records that includes the following tracks

#"Anything But Love" (Dr. John and Angela McCluskey) 
#"Somebody Elses Body" (Urge Overkill) 
#"Outasight" (G. Love and The Philly Cartel) 
#"Youre Not a Slut" (Ben Stiller And Celia Weston) 
#"Camel Walk" (Southern Culture on the Skids) 
#Lend Me Your Comb" (Carl Perkins) 
#"Acid Propaganda" (Lily Tomlin / Alan Alda / Ben Stiller) 
#"You Part the Waters" (Cake (band)|Cake) 
#"Lonnie Cooks Quali" (Glen Fitzgerald) 
#"Red Beans N Reverb" (Southern Culture on the Skids) 
#"Flirting with Disaster" (Dr. John and Angela McCluskey) 
#"Hypospadia" (Patricia Arquette / Josh Boslin / Tea Leoni / Ben Stiller) 
#"Melodie DAmour (Cha, Cha, Cha DAmour)" (Dean Martin)    
#"For Duty and Humanity" (Inch (band)|Inch) 
#"The Flirting Suit" (Stephen Endelman)

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 