Finder's Fee
 
{{Infobox film name           = Finders Fee image          = Finders Fee.jpg caption        = Finders Fee DVD cover director       = Jeff Probst producer       = Brad Van Arragon Katy Wallin Shawn Williamson eproducer      = James Shavick aproducer      = Jonathan Shore writer  story by Jim Gulian starring       = Erik Palladino James Earl Jones Ryan Reynolds Dash Mihok Matthew Lillard Robert Forster music          = Rob King B. C. Smith cinematography = Francis Kenny editing        = Brian Berdan distributor    = Lions Gate Entertainment Silverline Pictures released       =       runtime        = 100 minutes country        = United States language       = English budget         = 
}}
Finders Fee is a 2001 American film directed by Jeff Probst from his original screenplay.

==Plot==
The film takes place over the course of a single evening. Tepper, played by Erik Palladino, finds a wallet on his way home from work. He contacts the owner of the wallet by telephone, and then later discovers that the wallet contains the winning ticket in a $6 million lottery.

Complications arise when Teppers friends come over for their regular poker night. One of the conditions of the game is that everyone purchase a ticket for the lottery, to be thrown into the pot. The game is played as a Poker jargon#freezeout|freezeout, with the winner collecting all the tickets and any prizes they may be worth. When the owner of the wallet, played by James Earl Jones, arrives, he realizes that the winning ticket is in the pot, and stays to play in the game.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Ryan Reynolds || Quigley
|-
| Erik Palladino || Tepper
|-
| Matthew Lillard || Fishman
|-
| Dash Mihok || Bolan
|-
| James Earl Jones || Avery Phillips
|-
| Carly Pope || Carla
|-
| Frances Bay || Mrs. Darmsetter
|-
| Robert Forster || Officer Campbell
|}

==Reception==
The film received generally positive reviews from critics. The film holds a 60% fresh rating on Rotten Tomatoes gives the film a "Certified Fresh" rating with a score of 60% based on reviews from 5 critics.

==Awards==
Finders Fee won the Golden Space Needle Award, given to the audiences choice for Best Picture at the 2001 Seattle International Film Festival.

Jeff Probst won the Best Screenplay (Feature Film) at the 2001 Method Fest Independent Film Festival.
 DVDX Awards.

==External links==
*  

 
 
 
 
 
 


 