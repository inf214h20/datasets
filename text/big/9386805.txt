Cóndor Crux, la leyenda
{{Infobox Film name     =      Cóndor Crux, la leyenda image          = CondorCrux.jpg caption        = Poster director       = Juan Pablo Buscarini Swan Glecer  producer       = José Luis Garci  writer         = Juan Pablo Buscarini Swan Glecer starring       = Leticia Brédice music          = Juan Pablo Compaired Diego Grimblat cinematography =  editing        = César Custodio distributor    = Patagonik Film Group Tornasol Films S.A. Buena Vista International released       = January 6, 2000 runtime        = 88 minutes country        =Argentina Spain language       = Spanish  budget         = 
}}
Cóndor Crux, la leyenda (English: Condor Crux, the Legend) is a 2000  Argentine animated adventure film written and directed by Juan Pablo Buscarini and Swan Glecer. The film premiered on January 6, 2000 in Buenos Aires. The film won a Silver Condor for Best Animated film.

==Cast==
*Damián de Santo as Juan Condor Crux
*Arturo Maly as Phizar
*Pepe Soriano|José Soriano as Dr.Crux
*Favio Posca as Sigmund
*Leticia Brédice as Zonia
*Aldo Barbero as Amauta
*Max Berliner as Voice

==See also==
*List of animated feature-length films
*List of computer-animated films

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 