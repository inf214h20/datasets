41 (film)
{{Infobox film
| name           = 41
| image          = 41 film.jpg
| alt            =
| director       = Christian de Rezendes Christian ONeill
| producer       = Christian de Rezendes Christian ONeill
| writer         =
| narrator       =
| music          = Nick ONeill Michael Teoli
| cinematography = Christian de Rezendes Christian ONeill
| editing        = Christian de Rezendes
| studio         = Breaking Branches Pictures Nickys Counting Productions
| distributor    = Nehst
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = $20,000
| gross          =
}} Nicholas ONeill, play of the same name written by Nicholas a year before he passed.  The titular number, as described by the film, was of spiritual significance to Nicholas, although the reasons behind this are not fully known.  The film also details how his family and friends believe that Nicholas may have prophetically known that he would die at a young age, and that he continues to communicate with them as a spirit, often through "signs" involving the number 41. http://www.41themovie.com 
 Robert Brown, Maureen Hancock and Cindy Gilman, and AVID co-creator Tom Ohanian.  Both of the directors appear in the film and act as storytellers, relating their personal connection to the story. 

== History ==
Christian de Rezendes describes in the film itself how he was inspired to after he himself had an experience of the number 41 which he believed may have been a sign from Nicholas.  While working on the film version of They Walk Among Us, he proposed to Christian ONeill the idea of making a short companion film in which people could tell their own stories of their own alleged "signs."  As the two began to collaborate, the film grew into a feature length film that tells a much fuller story of Nicholas life and the way in which his family and friends responded to the tragedy, with paranormal elements such as EVPs (electronic voice phenomena) and Spirit Orbs interwoven into the story.

== Reception ==
To date, 41 has been played at nine film festivals across the United States and Canada, including the Bare Bones International Film Festival in Muskogee, Oklahoma, the Woods Hole Film Festival in Cape Cod, Massachusetts and the Blue Mountain Film Festival in Ontario, Canada, where it won the prize for Best Documentary (Narrative).   Reviews have been generally positive, including a five-star review from Film Threat.   In 2008, the film was picked up for distribution by Nehst Studios, a production and distribution company headed by Larry Meistrich (producer of Sling Blade).   Since that time, it has had two theatrical runs, both in its home state of Rhode Island.    In 2007, the filmmakers were the featured guests on Beyond Reality Radio with Jason Hawes and Grant Wilson from the television program Ghost Hunters,  which is based in Warwick, Rhode Island, close to the site of the Station fire.

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 