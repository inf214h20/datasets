Bad Asses on the Bayou
 
{{Infobox film
| name           = Bad Asses on the Bayou
| image          = Bad Asses on the Bayou.jpg
| caption        = 
| director       = Craig Moss
| producer       = Ash R. Shah Ben Feingold Jim Busfield Danny Glover Todd King Don Napoli Danny Trejo
| writer         = Craig Moss
| starring       = Danny Trejo Danny Glover John Amos Loni Love
| music          = Todd Haberman
| cinematography = Paul Marschall
| editing        = Josh Noyes
| studio         = Sense and Sensibility Ventures Silver Nitrate
| distributor    = 20th Century Fox Home Entertainment The Samuel Goldwyn Company
| released       = March 6, 2015
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Bad Asses on the Bayou (also known as Bad Ass 3: Bad Asses on the Bayou) is a 2015 action film starring Danny Trejo and Danny Glover, written and directed by Craig Moss. The film is the third part of the Bad Ass series.

==Plot synopsis==
Frank Vega and Bernie Pope return, this time to Louisiana in an attempt to find a kidnapped friend.

==Cast==
*Danny Trejo as Frank Vega
*Danny Glover as Bernie Pope
*John Amos as Earl
*Loni Love as Carmen
*Jimmy Bennett as Ronald
*Olga Wilhemine as Violinist
*Jaqueline Fleming as Katie
*Judd Lormand as Detective Williamson
*Sammi Rotibi as Geoffrey
*Rob Mello as Buford
*Al Vicente as Guillermo Gomez
*Miles Doleac as Talk Show host
*Deborah Ayorinde as Taryn
*Han Soto as Hung
*Brian Oerly as Jimbo
*Keith Loneker as Pierre
*Chelsea Bruland as Marissa
*Davi Jay as Chief Broussard
*Andrew Duplessie as AJ
*Carol Sutton as Lois Morgan
*Ross P. Cook as Ass Kicker
*Garrett Kruithof as Jawn
*Jeff Pope as Landry
*Aisa Palomares as Kelly
*Michael Patrick Rogers as Kidnapper
*Colby Arps as Kyle
*Garrett Saia as Bufords friend

== References ==
 

==External links==
 
 

 
 
 
 
 
 
 
 