Charodei
{{Infobox Film
| name           = Charodei
| image          = DVD-Charodei.jpg
| image_size     =
| caption        = Film DVD cover
| director       = Konstantin Bromberg
| producer       = 
| writer         = Arkady Strugatsky  Boris Strugatsky
| narrator       = 
| starring       = 
| music          = Yevgeni Krylatov
| cinematography = 
| editing        = 
| distributor    = 
| studio         = Odessa Film Studio
| released       = 1982
| runtime        = 160 min
| country        = Soviet Union Russian
| budget         = 
| gross          = 
}} 1982 Soviet Soviet romantic fantasy musical film directed by Konstantin Bromberg.

==Production== Sokurov "Days of Eclipse" (Dni zatmeniya).

Film became a classic Soviet New Years Eve romantic comedies, such as Irony of Fate (Ironiya sudby) and The Carnival Night (Karnavalnaya noch).

== Plot summary ==
Ivan Puhov (A. Abdulov) is in love with a young and pretty girl Alyona (A. Yakovleva). Little does know Ivan that Alyona is working as a witch. The marriage is already scheduled, but just one circumstance of "winter heart" magic induced by Kira Shemahanskaya (Ye. Vasilyeva), the institute director, who is convinced by Sataneev (V. Gaft), separates them. Her friends — magic wood masters, working in NUINU (Scientific Universal Institute of Extraordinary Services, a NIICHAVO subsidiary in Kitezhgrad; for NIICHAVO see Monday Begins on Saturday) - decides to interfere and help to save the love. They convince Puhov to return and save Alyona right now.

== Cast ==
* Aleksandra Yakovleva, Alyona Igorevna Sanina
* Aleksandr Abdulov, Ivan Sergeevich Puhov
* Yekaterina Vasilyeva, Kira Anatolyevna Shemahanskaya
* Valentin Gaft, Apollon Mitrofanovich Sataneev
* Yevgeny Vesnik
* Valery Zolotukhin, Ivan Kivrin
* Emmanuil Vitorgan, Kovrov
* Mikhail Svetin, Foma Ostapych Bryl
* Roman Filippov, Modest Matveevich Kamneyedov
* Anna Ashimova, Nina Puhova
* Semyon Farada, The Guest from South Leonid Kharitonov Amatin
* Ye. Abramova, Verochka
* I. Belger, Katenka
* V. Smolyanitskaya, as a master of a speaking cat
* Liliya Makeyeva, secretary

== Film soundtrack ==
Film soundtrack includes many classical Soviet songs (some of them romantic), written by Yevgeni Krylatov and Leonid Derbenyov, including:
* "A womans enigma" (Загадка женщины) performed by Irina Otieva
* "Three white horses" (Три белых коня) performed by Larisa Dolina
* "A song about a snowflake" (Песня о снежинке) performed by Olga Rozhdestvenskaya and Dobrie Molodtsy band
* "Witch-river" (Ведьма-речка) performed by Irina Otieva
* "A Song About a Suit" (Песенка про костюмчик) performed by Emmanuil Vitorgan and Mikhail Svetin
* "Imagine That" (Представь себе) performed by Aleksandr Abdulov
* "Time to Sleep" (Спать пора) performed by Mikhail Svetin
* "Serenade" (Серенада) performed by original cast members
* "Centaurs" (Кентавры) performed by Dobrie Molodtsy
* "By The Mirror" (Подойду я к зеркалу) performed by Zhanna Rozhdestvenskaya
* "You Cant Command Your Heart" (Только сердцу не прикажешь) performed by Zhanna Rozhdestvenskaya and Vladislav Lynkovskiy
* "Dont believe what they say" (Говорят, а ты не верь) performed by original cast members

Music performance by State Symphony Orchestra of Cinematography of the USSR.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 