Institute Benjamenta
{{Infobox film
| name           = Institute Benjamenta, or This Dream People Call Human Life
| image          = Institutebenjamenta.jpg
| image_size     =
| caption        =
| director       = Stephen Quay Timothy Quay Keith Griffiths
| writer         = Alan Passes
| narrator       =
| starring       =
| music          = Lech Jankowski
| cinematography = Nick Knowland
| editing        = Larry Sider
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 104 minutes
| country        = Germany Japan United Kingdom German English English
| budget         =
}}
 Robert Walser. It stars Mark Rylance, Alice Krige, and Gottfried John.

==Plot==
The plot follows Jakob, a young man who enters a school, run by brother and sister Herr and Lisa Benjamenta, which trains servants. The teachers emphasize to the students that they are unimportant people. Jakob finds the school to be an oppressive environment, and does not enjoy the lessons in subservience that he receives. He proceeds to challenge the Benjamentas and attempts to shift their perspectives. Lisa is attracted to Jakob and spends time with him, and shows him the secret labyrinth below the school. Lisa soon dies and after her death the institute closes. Herr Benjamenta and Jakob then leave together.

==Relation to the novel==
Though the film follows the same basic structure as the novel, its plot is more limited. The film does not depict the ending of the novel, in which Jakob travels to a nearby city and meets his brother. The film remains almost exclusively focused on the institute once Jakob arrives there. 
 parallel universe of the novel. The film sometimes makes figurative aspects of the novel into literal objects in the film. One reoccurring theme in the film is that many objects are seen vibrating, such as forks or bells. Parallels have been drawn between these vibrations and the frequent theme of music in Walsers writing. Also, in one passage of the book Jakob describes a staff member at the school as like a monkey, but in the film a literal monkey takes the role of that staff member. In addition, while Jakob wonders about the activities of Herr and Lisa Benjamenta in the novel, the film clearly displays an incestuous relationship. The conclusion of the film, however, differs from the book in a different manner. It shows Herr and Jakobs departure from the institute in surreal scene of the two in a fish bowl rather than Jakobs literal journey back to town in the book. 

==Themes== dream world.  Writing in the journal Adaptation, David Sorfa argues that many of the projects of the Brothers Quay discuss the idea that a "metaphysical interior" may exist. Sorfa argues that this film does not attempt to reveal a hidden meaning, but rather argue that a realm of hidden meaning, though unreachable, may exist nonetheless.   

Ariel Swartley of The New York Times has drawn a comparison between Jakobs efforts to "turn himself into a machine" as a servant and the animation of objects that the Brothers Quay have focused on in their previous stop-motion films.    (Institute Benjamenta was their first live-action film).    Stephen Quay has said that they sought to convey a similar sense of "otherness" through the actors as they had done through puppets in previous projects.    Laura Marks has argued that "non-sentient life seems to take precedence over human life" and that the film includes a "tide of non-human life". 

Sorfa contends that the number zero is used as a sort of parody of a MacGuffin in the film. 

==Reception== Expressionist film of the 1920s. Silke Horstkotte has argued that the gestures of the actors and surreal aspects of the school also recall silent film conventions. Horstkotte 2009, p. 186 

The imagery used in the film has been characterized as "befuddling",  "puzzling",  and "mysterious". 

Peter Stack of The San Francisco Chronicle said that several scenes which featured miming were particularly well made.    Phil Hall of Wired (magazine)|Wired praised what he described as the "marvelous tension" between Rylance and Kreige. He also applauded the focused and controlled performances by the rest of the cast.  David Sorfa praised Rylances portrayal of Jakobs emotions as "subtle". 
 Michael Atkinson praised the films originality, stating that at times it is comparable to the "inevitable grip of the best David Lynch". He did note that he found the film confusing at times, and characterized it as possessing "Freudian secrecy". He suggests that the film should be seen as a mood, rather than as a narrative. He also lamented that the film saw a limited release The film was shown at approximately 50 theaters in the United States in the spring and summer of 1996.  and would likely only be seen by those who already have an interest in art film.  Horstkotte noted that the films "unusual aesthetic" would be viewed as very unusual to individuals who typically watch Hollywood films. 

The Quays themselves later stated that they regretted the length of the film. They suggested that it would have been more effective as a short film or a very long film, rather than average length. 

== Notes ==
 

==References==
 

==Bibliography==
* 
* 
* 

==External links==
*  at Rotten Tomatoes
*  

 
 
 