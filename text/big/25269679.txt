Hotel (2004 film)
{{Infobox film
| name           = Hotel
| image          = 
| caption        = 
| director       = Jessica Hausner
| producer       = Bruno Wagner
| writer         = Jessica Hausner
| starring       = Franziska Weisz
| music          = 
| cinematography = Martin Gschlacht
| editing        = Karina Ressler
| distributor    = 
| released       = 17 May 2004
| runtime        = 83 minutes
| country        = Austria
| language       = German
| budget         = 
}}

Hotel is a 2004 Austrian drama film directed by Jessica Hausner. It was screened in the Un Certain Regard section at the 2004 Cannes Film Festival.   

==Cast==
* Franziska Weisz as Irene
* Birgit Minichmayr as Petra
* Marlene Streeruwitz as Mrs. Maschek
* Rosa Waissnix as Mrs. Liebig
* Christopher Schärf as Erik
* Peter Strauß as Mr. Kross
* Regina Fritsch as Mrs. Karin
* Alfred Worel as Mr. Liebig

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 