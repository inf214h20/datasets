Él (film)
{{Infobox film
| name           = Él
| image          = El-1953-film.jpg
| caption        = 
| director       = Luis Buñuel
| producer       = 
| writer         = based on the novel by Mercedes Pinto
| narrator       = 
| starring       = Arturo de Córdova

Delia Garcés
Carlos Martínez Baena
| music          = Luis Hernández Bretón
| cinematography = Gabriel Figueroa
| editing        = Carlos Savage
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

Él ( , is a Mexican film based upon the novel by Mercedes Pinto. It deals with many themes common to Buñuel’s cinema, including a May–December romance between a woman and her obsessively overprotective bourgeois husband, and touches of surrealism.  The film was entered into the 1953 Cannes Film Festival.   

==Plot== church where a man named Francisco sees an attractive young woman from across the room. They have seen each other many times before, and she seems reluctant to engage him. She leaves the church and escapes Francisco, despite his attempt to chase after her. Another day, Francisco finds her again in the church. He works up the courage to speak with her, but she seems uninterested, and insists that they can never speak to each other again. Francisco follows her to a restaurant and sees her meeting with Raul, a close friend of his.
 engaged to be married. Francisco conspires to woo Gloria away from Raul by throwing a party and arranging for the couple to attend. When Gloria finds out that Francisco is the host, she seems wary of this ruse, but ultimately falls for his charm.

The film jumps to the future, where Gloria and Francisco are married, and have been for quite some time. One day, Raul is driving through the city and finds Gloria. Though hesitant at first, she tells him of how horrible her marriage is, because Francisco has turned out to be a very jealous and domineering husband. For instance, Francisco had scolded her for being too familiar with other men, and even had an altercation with one of them.
 blanks in order to "teach her a lesson." But Gloria tells Raul that Francisco became more caring and forgiving after this episode.

Relations between husband and wife become better for a time, but then Francisco takes her alone up the spire of a church and (seemingly without pre-mediation), begins strangling her and dangles her  perilously over the railing. Gloria pulls herself from danger and runs away. It is only at this point that Gloria encounters Raul and tells him of her plight. Raul suggests that she leave her husband.

Gloria returns home willingly, but Francisco sees that someone brought her to the house, and demands to know who it was. He is devastated to learn that Gloria had been with Raul. The pattern of Franciscos jealously is unbroken and he contemplates divorce. But he seeks reconciliation after apparently realizing that Gloria has never in fact had an affair. Gloria confesses that "she was confused," but that she had to confide in somebody, and that somebody was Raul. When Francisco realizes that she had told Raul about their marital problems, he regards it as an utter betrayal, and says angrily that he cant forgive her for it.

That night, Francisco attempts to kill Gloria in her sleep with a  , and hallucinates that the entire congregation is laughing at him. He sees the priest, a good friend of his, laughing at him, and attempts to charge the altar and strangle him. He is detained.

Much later, Gloria, Raul, and a small child pay a visit to a monastery. It is revealed that Francisco has been taken in by the monks and has been taught in their ways. They meet with the head monk, but do not talk with Francisco, not wishing to reopen old wounds. Gloria and Raul have named their child "Francisco",and is implied that the child may not be Rauls. The head monk later tells Francisco of their visit, which he had already observed from afar. He confirms Franciscos suspicion that the child is the son of Gloria and Raul. Francisco affirms that, ultimately, "time has proven my point."  

 

==Cast==
*Arturo de Córdova - Francisco Galvan de Montemayor
*Delia Garcés - Gloria Milalta
*Luis Beristáin - Raul Conde

==Production==
After completing the initial filming of Adventures of Robinson Crusoe and its release being indefinitely delayed, Buñuel decided to adapt Mercedes Pintos novel Pensamientos about a paranoid husband. Buñuel also added personal memories of his sister Conchitas paranoid husband,  who once mistakenly thought he saw Buñuel making vulgar faces at him on the street and went home to get his gun until his family finally convinced him that Buñuel was living in Zaragoza at the time. Buñuel acknowledged autobiographical elements in the film and stated that "it may be the film I put the most of myself into. There is something of me in the protagonist." Baxter. pp. 228.  

Buñuel later complained about how fast he was forced to shoot the film and that he wanted to remake it. He stated that "I did what I did in most of my Mexican films. They proposed a subject to me and instead of it, I made a counter-offer which, though still commercial, seemed more propitious for examining the things that interested me."  Buñuels producer hired Yucatán-born Mexican actor Arturo de Córdova for the lead role of Francisco Galvan de Montemayor. De Córdova had previously been a Hollywood star in swashbuckling roles, but his heavy Bronx accent often hindered his performances. Buñuel playfully has a cameo in the films last scene as a priest. 

==Reception==
Él was a critical and financial disappointment, and many audience members in Mexico laughed during the film. Buñuel later stated that he was disappointed by the film overall, but proud that French psychoanalyst Jacques Lacan was known to screen the film for his students as an example of paranoia.  The film has been named one of the 100 essential films of all time by the French magazine Cahiers du Cinema.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 