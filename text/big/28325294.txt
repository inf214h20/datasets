Not Quite a Lady
{{Infobox film
| name           = Not Quite a Lady
| image          =
| image_size     =
| caption        =
| director       = Thomas Bentley
| producer       = 
| writer         = Eliot Stannard
| narrator       =
| starring       = Mabel Poulton   Janet Alexander   Barbara Gott   Maurice Braddell
| music          =  
| cinematography = 
| editing        = 
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 25 July 1928
| runtime        = 87 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} British silent silent comedy film directed by Thomas Bentley and starring Mabel Poulton, Janet Alexander and Barbara Gott.  Unhappy with her sons choice of fiancee a wealthy woman holds a boring house party to try to put her off marrying into the family.

==Cast==
* Mabel Poulton - Ethel Borridge
* Janet Alexander - Mrs.Cassilis
* Barbara Gott - Mrs. Borridge
* Maurice Braddell - Geoffrey Cassilis
* Dorothy Bartlam - Mabel Marchmont George Bellamy - Maj. Warrington
* Gibb McLaughlin - The Vicar
* Sam Wilkinson - Mr. Borridge

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 