Anton the Magician
{{Infobox film
| name           = Anton the Magician
| image          = 
| caption        = 
| director       = Günter Reisch
| producer       = 
| writer         = Karl-Georg Egel Günter Reisch
| starring       = Ulrich Thein
| music          = 
| cinematography = Günter Haubold
| editing        = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Anton the Magician ( ) is a 1978 East German drama film directed by Günter Reisch. It was entered into the 11th Moscow International Film Festival where Ulrich Thein won the award for Best Actor.   

==Cast==
* Ulrich Thein as Anton
* Anna Dymna as Liesel
* Erwin Geschonneck as Vater Grubske
* Barbara Dittus as Sabine
* Marina Krogull as Ilie
* Erik S. Klein as Schröder
* Marianne Wünscher as Rechtsanwältin
* Jessy Rameik as Bürgermeisterin
* Ralph Borgwardt as Leiter der Haftanstalt
* Gerry Wolff as Oberwachtmeister
* Werner Godemann as Franz Rostig
* Grigore Grigoriu as Sergeant (as Grigori Grigoriu)
* Dezsö Garas as Istvan

==References==
 

==External links==
*  

 
 
 
 
 
 
 