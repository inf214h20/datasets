Zero Kelvin (film)
{{Infobox film image       = Kjærlighetenskjøtere.jpg name        = Zero Kelvin director    = Hans Petter Moland writer      = Lars Bill Lundholm producer    = Esben Høilund Carlsen starring    = Stellan Skarsgård Gard B. Eidsvold Bjørn Sundquist Camilla Martens Paul-Ottar Haga Erik Øksnes music       = Terje Rypdal cinematography = Philip Øgaard editing     = Einar Egeland released    = 1995 runtime     = 118 min (113 min in USA) country     = Norway awards      = language    = Norwegian budget      =
}}

Zero Kelvin ( ) is a 1995 Norwegian film starring Stellan Skarsgård, an actor known to English language|English-speaking audiences from his roles in such films as Good Will Hunting.

In 1920s Oslo, Henrik Larsen (Gard B. Eidsvold), an aspiring poet, leaves his girlfriend (Camilla Martens) to spend a year as a trapper in Greenland, where he is teamed with a sailor (Stellan Skarsgård) and a scientist (Bjørn Sundquist). The men are trapped in a tiny hut, as the arctic winter sets in, and a complex and intense love/hate relationship develops between the poet and the sailor - both who are more similar to one another than either would like to admit.  Their conflict plays out in isolation amidst stunningly bleak arctic scenery, filmed in Svalbard.

==Cast==
*Stellan Skarsgård  as Randbæk
*Gard B. Eidsvold  as Henrik Larsen
*Bjørn Sundquist  as Jakob Holm
*Camilla Martens  as Gertrude
*Paul-Ottar Haga  as Officer
*Johannes Joner  as Company Man
*Erik Øksnes  as Captain
*Lars Andreas Larssen  as Judge
*Juni Dahr  as Woman in park
*Johan Rabaeus  as Man in park
*Frank Iversen  as Poetry Buyer
*Tinkas Qorfiq  as Jane

== External links ==
*  

 

 
 
 
 
 
 

 
 