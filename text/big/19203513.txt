Peggy Leads the Way
{{Infobox film
| name           = Peggy Leads the Way
| image          =
| caption        =
| director       = Lloyd Ingraham
| producer       =
| writer         = Charles T. Dazey Frank Mitchell Dazey
| starring       = Mary Miles Minter
| cinematography =
| editing        =
| distributor    = Mutual Film
| released       =  
| runtime        =
| country        = United States Silent English English intertitles
| budget         =
}}
 1917 silent film directed by Lloyd Ingraham. A copy of the film was first found at the Dutch Netherlands Filmmuseum|Filmmuseum. It was sold to the American Film Institute in 1991 and held at UCLA.   

==Plot==
Peggy Manners has just finished finishing school and returns home. She has always thought her father was a great and wealthy store owner, but finds out he is a grocery store owner who isnt making any profit. She tries to make a change and falls in love with the son of the wealthy Roland Gardiner. 

==Cast==
* Mary Miles Minter - Peggy Manners Andrew Arbuckle - H.E. Manners
* Carl Stockdale - Roland Gardiner
* Allan Forrest - Clyde Gardiner
* Emma Kluge - Mrs. Greenwood
* Margaret Shelby - Maude Greenwood

==References==
 

==External links==
* 

 
 
 
 
 
 


 