Somewhere in Berlin
{{Infobox film
| name           = Somewhere in Berlin
| image          = 
| image size     =
| caption        =
| director       = Gerhard Lamprecht
| producer       =
| writer         = Gerhard Lamprecht
| narrator       =
| starring       = Charles Brauer, Hans Trinkaus, Siegfried Utecht, Harry Hindemith, Hedda Sarnow
| music          = Erich Einegg
| cinematography = Werner Krien
| editing        =
| distributor    = 
| released       = 1946
| runtime        = 85 min.
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German DEFA film. It sold 4,179,651 tickets.  It was part of the group of rubble films made in the aftermath of the Second World War.

==Cast==
* Mady Rahl as Ilse Bock  
* Rotraut Richter as Edeltraud Panse  
* Carl-Heinz Schroth as Dr. Erich Horn, Rechtsanwalt  
* Ernst Waldow as Gustav Kluge, Bäckermeister  
* Grethe Weiser as Paula Kluge  
* Lotte Werkmeister as Witwe Bock, Plättstubenbesitzerin 
* Hans Stiebner as August Wudicke   Adolf Fischer as Emil, Bäckergeselle  
* Paul Westermeier as Panse, Portier 
* Kurt Seifert as Hermann Schultze  
* Ilse Fürstenberg as Irma Schultze  
* Ellen Bang as Mi  
* Kurt Waitzmann as Kuhlmann, Architekt  
* Ewald Wenck as Herr Semmelweiss 
* Walter Lieck as Roderich  
* Friedrich Langhammer as Willy, Bäckerlehrling  
* Margarete Kupfer as Frau Krawutschke 
* Wolfgang Kieling as Bürolehrling bei Dr. Horn

==References==
  

==Bibliography==
* Shandley, Robert. Rubble Films: German Cinema in the Shadow of the Third Reich. Temple University Press, 2010.

==External links==
*  

 

 
 
 
 
 

 