En sømand går i land
 
{{Infobox film
| name           = En sømand går i land
| image          = 
| caption        = 
| director       = Lau Lauritzen, Jr.
| producer       = Poul Bang
| writer         = Grete Frische
| starring       = Poul Reichhardt
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw
| distributor    = ASA Film
| released       =  
| runtime        = 101 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

En sømand går i land is a 1954 Danish comedy film directed by Lau Lauritzen, Jr. and starring Poul Reichhardt.

==Cast==
* Poul Reichhardt - Vladimir W. Olsen
* Lau Lauritzen, Jr. - Frederik Larsen
* Mogens Hermansen - Portneren i sømandshjemmet
* Carl Johan Hviid - Bestyrer Nielsen i sømandshjemmet
* Fernanda Movin - Fru Nielsen
* Jørn Jeppesen - Pastor Poulsen
* Knud Heglund - Hr. Jespersen
* Paul Hagen - Værtshusgæst
* Emil Hass Christensen - Politiassistenten
* Ib Schønberg - Bach / Beethoven
* Carl Ottosen - Herman
* Per Gundmann - Hermans ledsager
* Lisbeth Movin - Inger Knudsen
* Marie Brink - Fru Mortensen
* Birgitte Bruun - Esther
* Tove Grandjean - Dame på mødrehjælpens kontor
* Thorkil Lauritzen - Afdelingschef i stormagasin

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 