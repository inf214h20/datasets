The Night Before the Divorce
{{Infobox film
| name = The Night Before the Divorce
| image =
| image_size =
| caption =
| director = Robert Siodmak
| producer = Ralph Dietrich 
| writer =  Gina Kaus (play)    Ladislas Fodor (play)   Jerry Sackheim
| narrator =
| starring = Lynn Bari   Mary Beth Hughes  Joseph Allen
| music = Leigh Harline   Cyril J. Mockridge
| cinematography = J. Peverell Marley
| editing = John Brady 
| studio = Twentieth Century Fox 
| distributor = Twentieth Century Fox 
| released = March 6, 1942
| runtime = 67 minutes
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Night Before the Divorce is a 1942 American comedy film directed by Robert Siodmak and starring Lynn Bari, Mary Beth Hughes and Joseph Allen. 

==Main cast==
* Lynn Bari as Lynn Nordyke 
* Mary Beth Hughes as Lola May 
* Joseph Allen as George Nordyke (as Joseph Allen Jr.) 
* Nils Asther as Victor Roselle 
* Truman Bradley as Inspector Bruce Campbell 
* Kay Linaker as Hedda Smythe 
* Lyle Latell as Detective Brady 
* Mary Treen as Olga - the Maid 
* Thurston Hall as Bert Mousey Harriman 
* Spencer Charters as Small Town Judge 
* Leon Belasco as Leo - the Headwaiter 
* Tom Fadden as Captain Walt 
* Alec Craig as Jitters Noonan  

==References==
 

==Bibliography==
* Alpi, Deborah Lazaroff. Robert Siodmak: A Biography. McFarland, 1998.
* Greco, Joseph. The File on Robert Siodmak in Hollywood, 1941-1951. Universal-Publishers, 1999. 

==External links==
*  

 

 
 
 
 
 
 

 