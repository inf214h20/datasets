Il Fornaretto di Venezia
 
{{Infobox film
| name = Il Fornaretto di Venezia
| image = LE PROCES DES DOGES poster.jpg
| image_size =
| caption = French film poster
| director = Duccio Tessari
| producer =
| writer = Duccio Tessari Marcello Fondato
| narrator =
| starring = Jacques Perrin Michèle Morgan Enrico Maria Salerno
| music = Armando Trovajoli
| cinematography = Carlo Carlini
| editing = Franco Fraticelli Solange Moret
| studio = Lux Film Ultra Film Gaumont Film Company|Société des Etablissements L. Gaumont
| distributor =
| released =  
| runtime = 110 minutes
| country = Italy France
| language = Italian
| budget =
| gross =
}}  1963 Italian film directed by Duccio Tessari who co-wrote screenplay with Marcello Fondato, based on novel by Francesco DallOngaro.

It tells the story of 16th centurys Venice where a young worker is sentenced to death on the suspicion of attacking a noble.

==Main characters==
*Jacques Perrin as  Pietro, il fornaretto 
*Michèle Morgan as  Princess Sofia 
*Enrico Maria Salerno as  Lorenzo Barbo 
*Sylva Koscina as  Clemenza, Barbos Wife 
*Stefania Sandrelli as  Anella 
*Gastone Moschin as  Consigliere Garzone 
*Fred Williams as  Alvise  
*Mario Brega    
*Renato Terra

==External links==
* 
*  
* 
*  at AlloCiné   
*  at DvdToile  

 

 
 
 
 
 
 


 