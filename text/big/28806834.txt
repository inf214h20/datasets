The Mine with the Iron Door
 
{{Infobox film
| name           = The Mine with the Iron Door
| image          =
| caption        =
| director       = Sam Wood
| producer       = Sol Lesser
| writer         = Louis D. Lighton (adaptation) Hope Loring (adaptation) Mary Alice Scully Arthur F. Statter
| based on       =   Pat OMalley
| music          =
| cinematography = Glen MacWilliams
| editing        = 
| distributor    = Principal Pictures Corporation
| released       =  
| runtime        = 80 mins.
| country        = United States
| language       = Silent English intertitles
}}
 silent epic epic Western Western melodrama directed by Sam Wood and produced by Sol Lesser.  The film is based on the novel of the same name by American Author Harold Bell Wright published in 1923. 

Prints preserved in Gosfilmofond and Bois dArcy|France. 

==Cast==
* Dorothy Mackaill - Marta Hilgrove Pat OMalley - Hugh Edwards
* Raymond Hatton - The Lizard
* Bert Woodruff - Thad Grove Charles Murray - Bob Hill
* Mitchell Lewis - Sonora Jack
* Mary Carr - St. Jimmys Mother
* William Collier Jr. - Chico
* Creighton Hale - St. Jimmy
* Robert Frazer - Natachee
* Clarence Burton - The Sheriff
* Lillian Leighton 
* Marie Eagle Eye 
* Laura Winston 
* Fred Huntley

==Production notes==
The film was shot on location in the Tucson Arizona Valley, Oracle, Arizona, and Mt. Lemmon.  While shooting the film the cast and crew stayed at the Triangle L Ranch and the Wilson Ranch (Rancho Linda Vista) outside of Oracle, Arizona off Historic US Route 80.

==See also==
*List of rediscovered films

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 