Real Gone Cat
{{Infobox Film |
| name = Real Gone Cat
| image = Real Gone Cat.jpg
| caption = Promotional movie poster for the film
| writer = Scott Hopkins
| starring = Dennis Rowland Scott Hopkins Tracy Sucato Mark Gluckman Gary Imel Jesse McGuire
| director = Robert Sucato
| producer = Monsoon Films Hopcat Productions
| released = April, 2006 (USA)
| runtime = 19 Min
| budget = 
| language = English
}}
 2006 film directed by Robert Sucato and starring Dennis Rowland and Scott Hopkins. The film was an official selection of both the Sedona Film Festival and the Phoenix Film Festival.  Hopkins wrote and starred in the film with Rowland.

==Cast==
 
Dennis Rowland    -   Jimmy Baker 
Scott Hopkins     -   Picasso 
Tracy Sucato      -   Darlene 
Jesse McGuire     -   Ziggy Charles 
Mark Gluckman     -   Nick 
Gary Imel         -   Peter 
James Forsmo      -   Man at Bar

==Crew==

Directed by              -   Robert Sucato 
Written by               -   Scott Hopkins 
Produced by              -   Robert Sucato and Scott Hopkins 
Director of Photography  -   Steve Wargo 
Sound Editor             -   Danny Coltrane

==Film Festivals==

2006 Sedona Film Festival 
2006 Phoenix Film Festival 
2006 San Tan Short Film Festival 
2007 Kansas City Filmmakers Jubilee (Kansas City FilmFest)

==Trivia==
* Round Midnight (song) is the only song used in the film.
* There are three versions of Round Midnight (song) in the film.
* The version of Round Midnight (song) in the film was originally recorded by the films star and was on a Miles Davis tribute album.
* Leslie Nielsen was originally slated to play the role of "Peter".  He requested a line be added and it was made so.  Upon pulling out of the role for personal reasons, the line he had requested was left in the script.  "Wet birds never fly at night..."

==External links==
* 
* 

 
 
 