The Gay Dog
 
{{Infobox film
| name           = The Gay Dog
| image          = "The_Gay_Dog".jpg
| caption        = 
| director       = Maurice Elvey
| producer       = Ernest Gartside
| writer         = Peter Rogers Joseph Colton (play)
| starring       = Wilfred Pickles  Petula Clark  Megs Jenkins
| music          = Edwin Astley James Wilson
| editing        = Stanley Willis
| studio         = Coronet Films
| distributor    = Eros Films (UK)
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = 
}} Joe Henderson and Leslie Clark (Petulas father).   

==Plot==
A miners family acquire ownership of a greyhound, "Raving Beauty", which they plan to enter into a race. Jim Gay, the father, plans to get a better starting price for the dog by pretending it is ill, in order to lengthen its odds and clean up at the bookies. But keeping up the pretence with his wife, daughter, the whole street and the hardened gamblers played by Jon Pertwee and Peter Butterworth, is no easy task.  

==Cast==
* Wilfred Pickles - Jim Gay
* Petula Clark - Sally Gay
* Megs Jenkins - Maggie Gay Harold Goodwin - Bert Gay
* Nuna Duvey - Minnie Gay
* Miles Coleman - Gay Dog
* John Blythe - Peter Nightingale
* Margaret Barton - Peggy Gowland William Russell- Leslie Gowland
* Cyril Raymond - Vicar
* Peter Butterworth - Betting Man
* Jon Pertwee - Betting Man

==Critical reception==
*TV Guide wrote, "comic situations abound but fall far short in the execution, which is surprising coming from eminently competent director Elvey."  
*Monthly Film Bulletin wrote in 1954, "this unimaginative film version of Wilfred Pickles stage success has gained little from the transference, apart from the use of authentic locations which give an impression of realism. Wilfred Pickles presents an interesting character study of a hard-working miner, whilst retaining his customary mannerisms. Petula Clark gives a sensitive performance."    William Russell) she has a romantic partner with good looks, modest charm and considerable ability."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 