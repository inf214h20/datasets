L'Age d'Or
 
 
{{Infobox film
| name           =  L’Age d’or
| image          = LAge_dOr.jpg
| caption        = Cinema poster for L’Âge d’or
| director       = Luis Buñuel Vicomte Charles de Noailles Marie-Laure de Noailles
| writer         = Luis Buñuel Salvador Dalí
| starring       = Gaston Modot Lya Lys Caridad de Laberdesque Lionel Salem Max Ernst Germaine Noizet Josep Llorens Artigas Duchange Ibanez 
| music          = Luis Buñuel Georges van Parys 
| cinematography = Albert Duverger
| editing        = Luis Buñuel
| distributor    = Corinth Films (1979 U.S.&nbsp;release)
| released       = 29 November 1930
| runtime        = 63 minutes French
| budget         = 1 million francs
}} surrealist comedy sexual mores bourgeois society and the value system of the Roman Catholic Church. The screenplay is by Salvador Dalí and Buñuel.  LÂge dor was one of the first sound films made in France, along with Prix de Beauté and Under the Roofs of Paris.

==Plot== fellating the toe of a religious statue before finally French kissing her father. The mans penchant for committing social taboos like, making love in the mud during a religious ceremony, and slapping her mother seems to excite the woman. He stumbles away from the father and daughter kiss to her bedroom where he throws a burning tree, a bishop, a plow, the bishops staff, a giraffe statue and handfuls of pillow feathers out the window.

The final vignette is an allusion to the  , who comforts a young woman who has run out from the castle, before he takes her back inside. Afterwards, a woman’s scream is heard, and only the Duc re-emerges; and he is beardless. The concluding image is a crucifix festooned with the scalps of women; to the accompaniment of jovial music, the scalps sway in the wind.

==Cast==
* Gaston Modot as The Man
* Lya Lys as the Young Girl
* Caridad de Laberdesque as a Chambermaid and Little Girl
* Max Ernst as the Leader of men in cottage
* Josep Llorens Artigas as Governor
* Lionel Salem as Duke of Blangis
* Germaine Noizet as  Marquise
* Duchange as Conductor
* Valentine Penrose as a Spirit

==Production==
L’Âge d’or began as the second artistic collaboration between Luis Buñuel and Salvador Dalí, who had fallen out by the time of the film’s production. A neophyte cinéast, Buñuel overcame his ignorance of cinematic production technique by sequentially filming most of the screenplay; the 63-minute movie is composed of almost every meter of film exposed and dramatic sequence photographed.
 nobleman who, patroness of the arts and of artists, such as Dalí and Buñuel, Balthus, Jean Cocteau, Man Ray, Francis Poulenc, Jean Hugo, Jean-Michel Frank et alii.   entry in the Movie Diva website. 

LÂge dor included actors who were famous artists, such as Max Ernst and Josep Llorens Artigas.   

==Release==
 
==Reception==
Upon receiving a cinematic exhibition permit from the Board of Censors, L’Âge d’or had its premiere presentation at Studio 28, Paris, on 29 November 1930. Later, on 3 December 1930, the great popular success of the film provoked attacks by the right-wing Ligue des Patriotes (League of Patriots), whose angry viewers took umbrage at the story told by Buñuel and Dalí. The reactionary French Patriots interrupted the screening by throwing ink at the cinema screen and assaulting viewers who opposed them. They then went to the lobby and destroyed art works by Dalí, Joan Miró, Man Ray, Yves Tanguy, and others. On 10 December 1930, the Prefect of Police of Paris, Jean Chiappe, arranged to have the film banned from further public exhibition after the Board of Censors re-reviewed the film. 
 Roxie Cinema in San Francisco.
 psychological repression sexual mores predatory arthropod Ado Kyrou said that the five vignettes of the tale of LÂge dor (1930) correspond to the five sections of the tail of the scorpion.

==Legacy==
 
==References==
 

==External links==
*  
*  
*   at Rotten Tomatoes

 
 
 

 
 
 
 
 
 
 
 
 