The Career of Katherine Bush
{{infobox film
| title          = The Career of Katherine Bush
| image          = The Career of Katherine Bush (1919) - Calvert 2.jpg
| imagesize      =
| caption        =
| director       = Roy William Neill
| producer       = Adolph Zukor Jesse Lasky
| writer         = Elinor Glyn (novel)  Kathryn Stuart
| starring       = Catherine Calvert
| music          =
| cinematography =
| editing        =
| distributor    = Paramount Pictures - Artcraft
| released       = July 20, 1919
| runtime        = 50 minutes ; 
| country        = USA
| language       = Silent..(English intertitles)
}} lost  1919 silent film drama produced by Famous Players-Lasky and distributed by Paramount Pictures. Roy William Neill directed and Catherine Calvert starred. The film is based on an Elinor Glyn novel. 

==Starring==
*Catherine Calvert - Katherine Bush
*John Goldsworthy - Lord Algernon Fitz-Rufus
*Crauford Kent - Lord Gerald Strobridge
*Mathilde Brundage - Lady Garrubardine (*as Mrs. Mathilde Brundage)
*Helen Montrose - Lao Belemar
*Anne Dearing - Gladys Bush (*as Ann Dearing)
*Augusta Anderson - Matilda Bush
*Nora Reed - Slavey (*as Norah Reed)
*Claire Whitney - Lady Beatrice Strobridge
*Albert Hackett - Bert Bush
*Earl Lockwood - Fred Bush
*Walter Smith - Bob Hartley
*Robert Minot - Charlie Prodgers
*Edith Pierce - Ethel Bush
*Allan Simpson - Laos Sweetheart (*as Allen Simpson)

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 