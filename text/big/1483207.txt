The Train (1964 film)
{{Infobox film
| name = The Train
| image = the train poster.jpg
| image_size = 225px Frank McCarthy
| director = John Frankenheimer
| producer = Jules Bricken
| writer = Story & screenplay: Franklin Coen Frank Davis Uncredited: Walter Bernstein Howard Dimsdale Nedrick Young
| based on = Le front de lart by Rose Valland
| starring = Burt Lancaster Paul Scofield Jeanne Moreau Michel Simon
| music = Maurice Jarre
| cinematography = Jean Tournier Walter Wottitz
| editing = David Bretherton
| production companies = Les Productions Artistes Associés Les Films Ariane Dear Film Produzione
| distributor =  United Artists
| released = 1964 (UK) March 7, 1965 (U.S.)
| runtime = 140 minutes (UK) 133 minutes (U.S.)
| country = United States France Italy
| language = English
| budget = $5.8 million   The Numbers. Retrieved: January 22, 2013. 
| gross = $6,800,000 
}}
 Germans from museums and private art collections. It stars Burt Lancaster, Paul Scofield, and Jeanne Moreau.

Set in August 1944, the film sets French Resistance-member Paul Labiche (Lancaster) against German Colonel Franz von Waldheim (Scofield), who is attempting to move stolen art masterpieces by train to Germany. Inspiration for the scenes of the trains interception came from the real-life events surrounding train No. 40,044 as it was seized and examined by Lt. Alexandre Rosenberg of the Free French forces outside Paris.

==Plot== stolen by Jeu de Paume Museum, curator Mademoiselle Villard (Suzanne Flon) seeks help from the French Resistance.  Given the imminent liberation of Paris by the Allies of World War II|Allies, they need only delay the train for a few days—still, it is a dangerous operation and it must be done in such a way that does not risk damaging the priceless cargo.

Although Resistance cell leader and SNCF area inspector Paul Labiche (Burt Lancaster) initially rejects the plan, telling Mlle. Villard and senior Resistance leader Spinet (Paul Bonifas) "I wont waste lives on paintings", he has a change of heart after a cantankerous elderly engineer, Papa Boule (Michel Simon), is executed for trying to sabotage the train on his own. After that sacrifice, Labiche joins his Resistance teammates Didont (Albert Rémy) and Pesquet (Charles Millot)--who have been organizing their own plan to stop the train with the help of other SNCF Resistance members—in the effort to stop the theft. The Resistance devises an elaborate ruse to reroute the train, temporarily relabeling railway stations to make it appear to the German escort as if they are heading to Germany when they have actually turned back toward Paris. They then arrange a double collision in the small town of Rive-reine that will block the train without risking the cargo. Labiche, although shot in the leg, escapes on foot with the help of the widowed owner of a Rive-Reine hotel, Christine, (Jeanne Moreau) while other Resistance members involved in the collision plot are executed. 

The night after the Rive-Reine collision, Labiche and Didont meet Spinet again, along with young Robert (the nephew of Jacques, the executed Rive-Reine station master) and prepare an attempt to paint the tops of three boxcars white to warn off Allied aircraft from bombing the art train.  Robert recruits railroad workers and friends of his Uncle Jacques from nearby Montmirail, but the marking attempt is discovered, and Robert and Didont are both killed.

Now working alone, Labiche continues to delay the train after the tracks are cleared, to the mounting rage of von Waldheim, whose obsession with the paintings borders on madness. Finally, Labiche manages to derail the train without endangering civilian hostages that the colonel has placed on the engine to prevent it being blown up. Von Waldheim flags down a retreating army convoy and learns that a French armored division is not far behind. The colonel orders the train unloaded and attempts to commandeer the trucks, but the officer in charge refuses to obey. The trains small German contingent kills the hostages and joins the retreating convoy.

Von Waldheim remains behind with the abandoned train. Crates are strewn everywhere between the tracks and the road, labeled with the names of famous artists. Labiche appears and the colonel castigates him for having no real interest in the art he has saved:

  Labiche! Heres your prize, Labiche. Some of the greatest paintings in the world. Does it please you, Labiche? You feel a sense of excitement at just being near them? A painting means as much to you as a string of pearls to an ape. You won by sheer luck. You stopped me without knowing what you were doing or why. You are nothing, Labiche. A lump of flesh.

The paintings are mine. They always will be. Beauty belongs to the man who can appreciate it. They will always belong to me, or a man like me.

Now, this minute, you couldnt tell me why you did what you did.  

In response, Labiche turns and looks at the murdered hostages. Then, without a word, he turns back to von Waldheim and shoots him. He limps away, leaving the corpses and Frances greatest art treasures where they lie.

==Cast==
 
* Burt Lancaster as Paul Labiche
* Paul Scofield as Col. Franz von Waldheim  
* Jeanne Moreau as Christine
* Suzanne Flon as Mademoiselle Villard
* Michel Simon as Papa Boule
* Wolfgang Preiss as Maj. Herren
* Albert Rémy as Didont
* Charles Millot as Pesquet Richard Münch as Gen. von Lubitz
* Jacques Marin as Jacques
* Paul Bonifas as Spinet
* Arthur Brauss as Lt. Pilzer Jean Bouchard as Capt. Schmidt Donald OBrien as Sgt. Schwartz
* Howard Vernon as Capt. Dietrich
 

==Historical background== 1961 book Germans from museums and private art collections throughout France and were being sorted for shipment to Germany in World War II.

In contrast to the action and drama depicted in the film, the shipment of art that the Germans were attempting to take out of Paris on August 1, 1944 was held up by the French Resistance with an endless barrage of paperwork and red tape and made it no farther than a railyard a few miles outside Paris. 
 Paul Rosenberg, one of the worlds major Modern art dealers. 

German veterans organizations, including the SS veterans group HIAG, objected to Wehrmacht soldiers being depicted executing hostages and Resistance members in the film. They said that SS or uniformed Sicherheitspolizei (the Sicherheitsdienst and Gestapo) personnel should have been used for those scenes. 

==Production==
 , Michel Simon (background) & Burt Lancaster in The Train - trailer]]

John Frankenheimer took over the film from another director, Arthur Penn. The Train had already begun shooting in France when star Burt Lancaster had Penn fired and called in Frankenheimer to take over the film. Penn envisioned a more intimate film that would muse on the role art played in Lancasters character, and why he would risk his life to save the countrys great art from the Nazis. He did not intend to give much focus to the mechanics of the train operation itself. But Lancaster wanted more emphasis on action to ensure that the film would be a hit, after the failure of his film The Leopard (1963 film)|The Leopard. The production was shut down briefly while the script was rewritten, and the budget doubled under Frankenheimers direction. As he recounts in the Champlin book, Frankenheimer used the productions desperation to his advantage in negotiations. He demanded and got the following: his name was made part of the title, "John Frankenheimers The Train"; the French co-director, demanded by French tax laws, was not allowed to ever set foot on set; he was given total final cut; and a Ferrari.   Much of the film was shot on location.
 Spitfire attack scene that was inserted into the first third of the film. French Armée de lAir Douglas A-26 Invaders are also seen later in the film. 

The film includes a number of sequences involving long tracking shots and wide-angle lenses, with both foreground and background action in focus.  Noteworthy tracking shots include:

* Labiche attempting to flag down a train, then sliding down a ladder, running along the tracks, and jumping onto the moving locomotive&mdash;performed by Lancaster himself, not a stunt double;
* A scene in which the camera wanders around Nazi offices that are hastily being cleared, eventually focusing on von Waldheim and following him back through the office;
* A long dolly shot of von Waldheim travelling through a marshalling yard at high speed on a motorbike;
* Labiche rolling down a mountain and across a road, and staggering down to the track.  Frankenheimer noted on his DVD commentary that Lancaster performed the entire roll down the mountain himself, filmed by cameras at points along the hillside.
 History Channel, Frankenheimer revealed:
* The marshalling yard attacked during the Allied bombing raid sequence was demolished by special arrangement with the French railway, which had been looking to do it but had lacked funding.
* The sequence in which Labiche is shot and wounded by German soldiers while fleeing across a pedestrian bridge was necessitated by a knee injury Lancaster suffered during filming. Lancaster stepped in a hole while playing golf, spraining his knee so severely that he could not walk without limping.
* When told that Michel Simon would be unable to complete scenes scripted for his character as a result of prior contractual obligations, Frankenheimer devised the sequence wherein Papa Boule is executed by the Germans. Jacques Marins character was killed for similar reasons.
* Colonel von Waldheim (Paul Scofield) is told, at the scene of the last major train wreck, by Major Herren (Wolfgang Preiss), "This is a hell of a mess youve got here, Colonel."  This line became a metaphor for complicating disasters on Frankenheimer films thereafter.
* Colonel von Waldheim was originally to engage Labiche in a shootout at the films climax, but after Paul Scofield was cast in the role, at Lancasters suggestion Frankenheimer re-wrote the scene to provide Scofield a more suitable end—taunting Labiche into killing him.

Frankenheimer remarked on the DVD commentary, "Incidentally, I think this is the last big action picture ever made in black and white, and personally I am so grateful that it is in black and white.  I think the black and white adds tremendously to the movie."

Throughout the film, Frankenheimer often juxtaposed the value of art (or money) with the value of life. This may also be read as an allegorical commentary on patriotism and war in general. A brief montage ends the film, intercutting the crates full of paintings with the bloodied bodies of the hostages, before a final shot shows Labiche walking away. 

===Film locations===
Filming took place in several locations, including: Acquigny, Calvados; Saint-Ouen, Seine-Saint-Denis; and Vaires-sur-Marne|Vaires, Seine-et-Marne. The shots span from Paris to Metz. Much of the film is centered in the fictional town called "Rive-Reine." 

===The circular journey===
Actual train route: Paris, Vaires, Rive-Reine, Montmirail, Chalon-S-Marne, St Menehould, Verdun, Metz, Pont-a-Mousson, Sorcy(Level Crossing), Commercy, Vitry Le Francois, Rive-Reine. 

Planned route from Metz to Germany: Remilly, Teting(Level Crossing), St Avold, Zweibrücken.

===Locomotives used===
The locomotives used were the former Chemins de fer de lEst Series 11s 4-6-0s, which the SNCF classified as  . Identifiable locomotives include 1-230.B.739 with tender 22.A.739, 1-230.B.616, and 1-230.B.855 with tender 22.A.886; Papa Boules locomotive is 1-230.B.517. In the crash scene, an ancient "Bourbonnais" type 0-6-0 (N° 757) is used to block the line.

==Reception==
The Train earned $3 million in the US and $6 million elsewhere. Balio 1987, p. 279.  It had cost $6.7 million.  The film was one of the 13 most popular films in the UK in 1965. 

The Train holds a 100% "Fresh" rating on Rotten Tomatoes,  and a score of 7.9/10 on the Internet Movie Database. 

=== Awards and honors ===
*Nominated for the 1964 film award of the British Academy of Film and Television Arts. 
*Nominated for the 1965 Academy Award for Writing Original Screenplay (story and screenplay written directly for the screen). 
*Included in the second edition of The New York Times Guide to the Best 1,000 Movies Ever Made, published in 2004. 

==In Popular Culture== Season 5 Season 12 of Thomas & Friends, the brake sound effects from this film were used for the engines brakes.

There is a   for the Apple II and other 8- or 16-bit computers, based on the film.

==See also==
*The Monuments Men, a 2014 film about an Allied group tasked with saving pieces of art and culturally important items from destruction by Hitler during World War II.

==References==
===Notes===
 

===Bibliography===
 
* Armstrong, Stephen B. Pictures About Extremes: The Films of John Frankenheimer. Jefferson, North Carolina: McFarland, 2007. ISBN 978-0-78643-145-8.
* Balio, Tino. United Artists: The Company The Changed the Film Industry. Madison, Wisconsin: University of Wisconsin Press, 1987. ISBN 978-0-29911-440-4.
* Buford, Kate. Burt Lancaster: An American Life. New York: Da Capo, 2000. ISBN 0-306-81019-0.
* Champlin, Charles, ed. John Frankenheimer: A Conversation With Charles Champlin. Bristol, UK: Riverwood Press, 1995. ISBN 978-1-880756-09-6.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books Inc., 2000. ISBN 978-1-57488-263-6.
* Pratley, Gerald. The Cinema of John Frankenheimer (The International Film Guide Series). New York: Zwemmer/Barnes, 1969. ISBN 978-0-49807-413-4.
 

==External links==
 

* 
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 