Lulu (1996 film)
{{Infobox film
| name           = Lulu
| image          =
| image size     =
| caption        =
| director       = Srinivas Krishna
| producer       = Robert Bergman Srinivas Krishna Robert Armstrong
| narrator       =
| starring       = Kim Lieu
| music          =
| cinematography = Paul Sarossy
| editing        = Mike Munn
| distributor    =
| released       =  
| runtime        = 96 minutes
| country        = Canada
| language       = English
| budget         =
}}

Lulu is a 1996 Canadian drama film directed by Srinivas Krishna. It was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.   

==Cast==
* Kim Lieu - Khuyen
* Clark Johnson - Clive
* Michael Rhoades - Lucky
* Manuel Aranguiz
* Peter Breck
* Saeed Jaffrey
* Nghi Do
* Phuong Dan Nguten
* Richard Chevolleau
* Kay Tremblay
* Jack Jessop
* Dick Callahan
* Paul Persofsky
* Christofer Williamson
* Tony Meyler

==References==
 

==External links==
* 

 
 
 
 
 
 