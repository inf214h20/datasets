The Blood Beast Terror
{{Infobox film
| name           = The Blood Beast Terror
| image          = Das Blutbiest Originalposter.png
| caption        = British original poster
| budget         =
| director       = Vernon Sewell Peter Bryan
| starring       = Peter Cushing Robert Flemyng Wanda Ventham Vanessa Howard
| producer       = Arnold L. Miller Tony Tenser
| distributor    = Tigon British Film Productions
| released       = February 1968
| runtime        = 88 m English
}}

The Blood Beast Terror is a   it was released by Pacemaker Pictures on a double-bill with Slaughter of the Vampires as The Vampire-Beast Craves Blood.

==Story==
In 19th century England, a series of grisly murders are taking place in the countryside near London. The victims are good-looking young men, between the ages of twenty and thirty, and all have had their throats torn open and their blood drained. The witness of the latest murder, a coachman named Joe Trigger (Leslie Anderson), is driven insane when he catches a glimpse of the mysterious killer.

Investigating the deaths are Detective Inspector Quennell (Peter Cushing) of Scotland Yard and his assistant, Sergeant Allan (Glynn Edwards). Because Joe keeps ranting about a horrible winged creature with huge eyes, Quennell hatches a theory that perhaps a homicidal eagle is on the loose. At the scene of the latest killing, several shiny scales are discovered.

The two latest victims happen to be students of renowned entomology professor Dr. Carl Mallinger (Robert Flemying), who lives nearby with his daughter Clare (Wanda Ventham), and their butler, Granger (Kevin Stoney). When Quennell brings the scales to Mallinger for identification, Mallinger behaves suspiciously and tries to take all of them. He also puts forth his theory about a killer eagle, only to have Mallinger dismiss the notion. Quennell is unaware that the entomologist does actually have a pet eagle, which is tormented by sadistic Granger.

In the meantime, explorer and naturalist Frederick Britewell (William Wilde) returns from Africa with some moth   Frankenstein genre), but lives long enough to tell Quennell, "Deaths head!" when the inspector happens upon the dying explorer. Both Mallinger and Clare claim not to have known Britewell when questioned by Quennell.

Quennells superior suggests he takes a holiday and hands the case over to Sgt. Allan, but the Detective Inspector refuses. However, he reveals his intention to send his daughter Meg to stay with some relatives in Sussex until the investigation is over. As they leave for the railway station, Allan informs Quennell that Dr. Mallinger did in fact know Frederick Britewell, prompting Quennell to perform an immediate search of Mallingers home, only to find that the scientist and his daughter have left for Upper Higham. He also discovers a cellar filled with human bones, and the corpse of the dead butler, Granger.

Quennell informs his superior he will be taking leave after all: along with Meg, he goes to Upper Highham incognito as a vacationing banker named Thompson, where he discovers that Mallinger is also undercover as "Dr. Miles". But can he stop Mallinger, who is attempting to create a male were-moth to be a mate for his daughter?

===Cast===
{|
|- 
| Actor || Role
|-
| Peter Cushing || Inspector Quennell
|-
| Robert Flemyng || Dr. Carl Mallinger
|-
| Wanda Ventham || Clare Mallinger
|-
| Vanessa Howard || Meg Quennell
|-
| Glynn Edwards || Sgt. Allan
|-
| William Wilde || Frederick Britewell
|-
| Kevin Stoney || Granger
|- David Griffin || William Warrender
|- John Paul || Mr. Warrender
|-
| Leslie Anderson || Joe Trigger
|-
| Simon Cain || Clem Withers
|-
| Norman Pitt || Police Doctor
|-
| Roy Hudd || Smiler
|-
| Russell Napier || Landlord
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 