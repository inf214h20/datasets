Our Huge Adventure
{{Infobox Film|
  name=Little Einsteins: Our  Big  Huge Adventure |
  image=Our Big Huge Adventure.jpg |
  caption= Disney DVD cover |
  producer = Curious Pictures, Baby Einstein (companies) |
  starring= Natalia Wojcik   Jesse Schwartz   Aiden Pompey   Erica Huang |
  distributor= Walt Disney Home Entertainment |
  released= August 23, 2005 |
  runtime= 61 minutes  |
  language= English }}

Little Einsteins: Our  Big  Huge Adventure is a 2005 direct-to-DVD feature length movie from Walt Disney Home Entertainment. The film would later turn into the TV series Little Einsteins. 
==Plot==

In the film, the title group (comprising Leo, Annie, Quincy and June) meet a little caterpillar. The caterpillar sings a song, Leo conducts to it, while the Little Einsteins sing it. They then discover a green truck with a tree on it. When the caterpillar gets in the back of the truck, he waves goodbye to the Little Einsteins, as the caterpillars waving dies down, the truck gets bumped under a little hill, which causes the caterpillar to fly out of the back of the truck, Annie calls out to stop the truck, but the truck just drives away, leaving the caterpillar behind with the Little Einsteins. The caterpillar joins the Little Einsteins. They then later get some yellow leaves for the caterpillar, they then stop by the beach and watch the caterpillar eat the leaves. After the caterpillar finishes his leaves, a storm comes by. The caterpillar is then blown into the air and lands on one of the instruments out in the sea. Annie manages to rescue him by using the claw catcher. After a while, they finally found the truck with the other caterpillars inside, after they saw the truck, they went to the tree area, the Little Einsteins try to find the same tree like on the truck door, when they do, the caterpillar then gets on the tree, spins a cocoon, and turns into a butterfly. (The former part of this film was transformed into a regular episode entitled "A Brand New Outfit; the latter part was transformed into the episode "The Missing Invitation".) After when the caterpillar turns into a butterfly, they get invitations by a Mailman Butterfly, Leo, Annie, Quincy, June and Rocket get them except the butterfly. The butterfly becomes sad that he didnt get his invitation, Leo checks in the mailbox, but he says "Its empty." He asks the Mailman Butterfly where the invitation for the butterfly is. He shows the pictures of where they need to go: Niagara Falls, a butterfly garden, a Oklahoma cave and a cow place. After they searched for the invitation in 3 places, they finally find it on the cow place, but the cows are in the way. Leo conducts to the cows, the cows then move, and they get the invitation for the butterfly. After they get the invitation for butterfly, the Little Einsteins make it to the festival in Mexico. The Little Einsteins put their invitations in a box, so does butterfly and Rocket. The Little Einsteins and the swarm of butterflies make a music finale by singing the butterfly song. After they do, Leo calls out "Mission Completion!", then the curtains close the picture. They then do the Curtain Call before the film ends.

==Cast==

Natalia Wojcik as Annie

Jesse Schwartz as Leo

Aiden Pompey as Quincy

Erica Huang as June

==Rating==
The film received a G rating by the MPAA.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 

 
 