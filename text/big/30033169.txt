Meherbaan
{{Infobox film
 | name = Bhaiyyaji
 | image = MithunMeherbaan.jpg
 | caption = DVD Cover
 | director = Yunus Khan
 | producer = Yunus Khan
 | writer = Yunus Khan 
 | dialogue = Yunus Khan 
 | starring = Yunus Khan
 | music = Sajid-Wajid
 | lyrics = Rahat Fateh Ali Khan
 | associate director = F.M..
 | art director = Yunus Khan
 | choreographer = Yunus Khan
 | released =  March 22, 2015
 | runtime = 170 min.
 | language = Urdu Hindi Bhojpuri Rs 3.5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1993 Hindi Indian feature directed by K.Ravishankar, starring Mithun Chakraborty, Ayesha Jhulka, Shantipriya (actress)|Shantipriya, Anupam Kher and Sadashiv Amrapurkar

==Plot==
Meherbaan is the story of a family man, who has to decide between commitment, ethics and love between his 2 women.

==Cast==
*Mithun Chakraborty
*Ayesha Jhulka Shantipriya
*Sadashiv Amrapurkar
*Anupam Kher
*Kader Khan
*Sulbha Deshpande
*Shobha Khote
*Dina Pathak
*Tina Ghai
*Kalpana Iyer

==Songs==
The songs of Movie were much appreciated.The Song "Agar Aasman Tak Mere Hath Jaate" is First Song Playback in Hindi Movie By Sonu Nigam.

*Agar Aasman Tak Mere Haat Jaate = Sing By Sonu Nigam (Debut Song)
*Dhire Se Chupke Se Dil Ne Liya Tera Naam 
*Tum Hriday Ke Devta Ho 
*Rimjhim Rimjhim Tan Pe Mere Pani 
*O Mujhe Jana Hai Piya Ji Ke Gaon Mein 
*Ye Kya Kya Dikhati Hai Ye Zindagi 
*Aao Paas Aao Najaren Milao 
*Jo Bhi Aaya Hai Tere Dware 
*Aao Guru Karen Peena Shuru
*Bol Gori Bol Jara Itni Si Baat

==References==
* http://www.imdb.com/title/tt0107543/
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Meherbaan

==External links==
*  

 
 
 
 
 


 