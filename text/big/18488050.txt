The Heart of Me
{{Infobox film
| name           = The Heart of Me
| image          = The Heart of Me.jpg
| image_size     =
| director       = Thaddeus OSullivan
| writer         = Rosamond Lehmann Lucinda Coxon
| starring       = Helena Bonham Carter Olivia Williams Paul Bettany
| music          = Nicholas Hooper
| cinematography = Gyula Pados
| editing        = Alex Mackie
| distributor    = Paramount Pictures
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom Germany
| language       = English
}} British period drama   film directed by  Thaddeus OSullivan and starring Helena Bonham Carter, Paul Bettany and Olivia Williams. Set in London before and after World War II, it depicts the consequences of a womans torrid affair with her sisters husband. The film is an adaptation of Rosamond Lehmanns novel The Echoing Grove.

==Plot==
In 1934 after the death of her father, Madeleine, a proper and repressed well-off housewife, invites her free-spirited sister, Dinah, to stay with her and her husband Rickie in her elegant London home. The couple have a young son, Anthony. Madeleine has always been secretly jealous and resentful of Dinah, a raffish bohemian painter, who is the despair of her conservative sister and their mother, Mrs. Burkett. Madeleine at last contrives to get Dinah engaged to a respectable, well-off man. Dinah announces her engagement at a family dinner, but later that night Rickie, who has long harbored an attraction for her, tells her to end the engagement.

Rickie and Dinah fall in love and, during a New Years Day party, they become lovers. Rickie helps Dinah to settle in an apartment that becomes their love nest, but leave his marriage intact. Things get complicated when Dinah gets pregnant. She decides to leave London with her friend Bridie and await the birth and Rickies arrival in the south of England. During a snowstorm, Dinah gives birth to a stillborn daughter and almost dies from blood loss due to complications during the birth. Rickie, on his way to reach her, suffers a car accident and arrives too late. Grief-stricken, Dinah turns Rickie away and ends their affair. Rickie becomes lost in despair, but tries to hide it (unsuccessfully) from Madeleine. Months later, Madeleine receives a letter in the mail from Bridie who writes of the affair, leading Madeleine to the realization of what had been going on for quite some time. Rickie is adamant that the affair is finished, but he is unapologetic. Eventually Rickie meets unexpectedly with Dinah. She has a nervous breakdown at a restaurant and Rickie, still in love with her, tells Madeleine that he is leaving her.

Things, however, take a turn for the worse when Rickie collapses and is taken to the hospital, unknown to Dinah who is still home waiting for him. Dinah is prevented from seeing Rickie and is told by her mother and Madeleine that he has decided to return to his family, while they tell Rickie that she has gone back to France. Several more months pass and the lovers eventually meet again when Dinah is leaving the apartment that was being financed by Rickie, and he discovers that she had been there the entire time and not in France. Dinah, who is too hurt by things past, refuses to continue the affair and the lovers part for the last time.

During the war (World War II), Anthony, Rickies and Madeleines son, dies in  battle. Rickie is killed during an air raid while going to claim a bracelet for Dinah that he had ordered from a jewelry store with the engraving: "And throughout all eternity, I forgive you and you forgive me". The film ends with Madeleine and Dinah finally reconciled to the past and learning to forgive each other.

==Cast==
* Helena Bonham Carter – Dinah 
* Olivia Williams – Madeleine 
* Paul Bettany – Rickie 
* Eleanor Bron – Mrs. Burkett 
* Alison Reid – Bridie 
* Luke Newberry – Anthony
* Tom Ward – Jack 
* Gillian Hanna – Betty  
* Andrew Havill – Charles

==Reception==
The Heart of Me was filmed in   are used like clubs to beat the film. This is the same kind of thinking that led Jack Warner to tell his producers, "Dont give me any more pictures where they write with feathers." The movie is about the punishment of being trapped in a system where appearances are more important than reality.”

"...Quite well done, with a fine cast finding time between tears, anger, deception and recriminations to do some surprisingly strong and affecting acting..." — Los Angeles Times.
 
"...The actors demonstrate such unmatchable Englishness that the movie – a kind of The End of the Affair without the religious instruction – takes on the gleam of a cultural artifact..." — Entertainment Weekly 
"...A film for the mind..." — Total Film

==Awards and nominations==
*British Independent Film Awards
Won Olivia Williams

Nominated Helena Bonham Carter

*ALFS awards:
Won Helena Bonham Carter

Nominated Paul Bettany

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 