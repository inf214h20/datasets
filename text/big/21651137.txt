Dragon Ball: Ssawora Son Goku, Igyeora Son Goku
 
{{Infobox film
| name           = Dragon Ball: Ssawora Son Goku, Igyeora Son Goku
| image          = Korean Dragon Ball 1990 Film Poster.PNG
| caption        = 
| director       = Wang Ryong
| producer       = Ahn Hyun-Sik
| screenplay     = Yun Seok Hun
| story          = Akira Toriyama
| starring       = Heo Seong-Tae Lee Ju Hee Kim Jo Sef
| music          = Yi Nam Hong
| cinematography = 
| editing        = Hyeon Dong Chun
| distributor    = Dong Ir Movie
| released       =  
| runtime        = 100 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Dragon Ball: Ssawora Son Goku, Igyeora Son Goku ( ) (lit. Dragon Ball: Fight Son Goku, Win Son Goku) is an unofficial, unlicensed live-action Korean film adaptation of the manga series Dragon Ball by Akira Toriyama. It was directed by Ryong Wang and was released on December 12, 1990. 
The movie follows the original Dragon Ball story, and does so more closely than  . This live action adaption from Korea adopts the events of the Emperor Pilaf Saga and a little of the Saiyan Saga, as Nappa makes an appearance in this film.

== Availability ==

Currently, the only known method of acquiring or watching on VHS is through second-hand shops in South Korea, or through eBay. A copy of the original VHS tapes recently sold on eBay in the United States for $215.00.  Legally grey methods of viewing the movie also exist through YouTube and The Pirate Bay.

== References ==
 

 

 
 
 
 
 
 
 
 
 
 
 

 
 