Melanie (film)
{{Infobox film
| name = Melanie
| image =
| caption =
| director =
| producer =
| writer =
| starring = Glynnis OConnor Burton Cummings Don Johnson Paul Sorvino
| music = Burton Cummings
| cinematography =
| editing =
| distributor = 20th Century Fox
| released =  
| runtime =  104 minutes
| country = Canada
| language = English
| budget = $4 million ( ) 
}} 

Melanie is a 1982 Canadian film starring Glynnis OConnor, Burton Cummings and Don Johnson.

==Plot==
Melanie (OConnor) travels to Los Angeles in an effort to regain custody of her son from her ex-husband (Johnson). Her illiteracy poses a major obstacle. She meets a faded musician (Cummings) with whom she develops a relationship.

==Production==
Filming began in Toronto on 2 June 1980.    Additional filming was conducted in Los Angeles. 

==Reception==
Roger Ebert noted that Melanie was "an uneven and sometimes frustrating but very alive movie". {{cite news | first=Roger | last=Ebert | title=Parallel movies handling is contrasted | newspaper=Chicago Sun-Times | url=http://news.google.com/newspapers?id=GbQSAAAAIBAJ&pg=7246%2C3572824 | accessdate=20 April 2012 | date=29 March 1982 | page=8 | newspaper=Spokane Chronicle 
}} 

==Awards==

Melanie earned seven nominations for the 4th Genie Awards winning three of these: 

* OConnor won as Best Foreign Actress 
* "You Saved My Soul" by Cummings won for Best Song 
* Richard Paluk, Paul Guza Jr won for Best Adapted Screenplay

==Cast==
* Glynnis OConnor as Melanie
* Paul Sorvino as Walter
* Burton Cummings as Rick
* Trudy Young as Ronda
* Don Johnson as Carl
* Donann Cavin as Ginny
* Jamie Dick as Tyler
* Jodie Drake as Eula Lisa Dalbello as Marcie
* Yvonne Murray as Brandy
* Martha Gibson as Waitress
* Rocco Bellusci as Dana
* David Wills as Daryll Adrian

==References==
 

==External links==
*  
*  

 
 
 
 
 


 