Much Loved
{{Infobox film
| name           = Much Loved
| image          = 
| caption        =
| director       = Nabil Ayouch
| producer       = Saïd Hamich Eric Poulet Nabil Ayouch
| writer         = 
| starring       = Loubna Abidar Asmaa Lazrak
| music          = 
| cinematography = 
| editing        = 
| studio         = Celluloid Dreams
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Morocco France
| language       = Arabic
| budget         = 
| gross          = 
}}

Much Loved is a 2015 French-Moroccan  drama film directed by Nabil Ayouch. It has been selected to be screened in the Directors Fortnight section at the 2015 Cannes Film Festival. 

== Cast ==
* Loubna Abidar
* Asmaa Lazrak
* Halima Karaouane
* Sara Elmhamdi Elalaoui
* Abdellah Didane
 
== References ==
 

<!-- 
== External links ==
*   
-->

 
 
 
 
 
 
 
 
 

 