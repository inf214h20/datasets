Quiet Wedding
 
{{Infobox film
| name           = Quiet Wedding
| image          = "Quiet_Wedding"_(1941).jpg
| caption        =
| director       = Anthony Asquith
| producer       = Paul Soskin
| writer         = Esther McCracken (play) Anatole de Grunwald Terence Rattigan
| starring       = Margaret Lockwood Derek Farr Marjorie Fielding David Tomlinson
| music          = Nicholas Brodzsky
| cinematography = Bernard Knowles
| editing        = Reginald Beck Universal Pictures
| released       =  
| runtime        = 80 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 1941 Cinema British comedy Quiet Wedding by Esther McCracken which was later remade as Happy is the Bride.

==Plot==
A young couple become Engagement|engaged, but enjoy a number of comedic adventures before their wedding day. 

==Cast==
* Margaret Lockwood as Janet Royd
* Derek Farr as Dallas Chaytor
* Marjorie Fielding as Mildred Royd
* A. E. Matthews as Arthur Royd
* Athene Seyler as Aunt Mary
* Jean Cadell as Aunt Florence
* Margaretta Scott as Marcia
* David Tomlinson as John Royd
* Sidney King as Denys
* Peggy Ashcroft as Flower Lisle Frank Cellier as Mr. Clayton
* Roland Culver as Boofy Ponsonby
* Michael Shepley as Marcias Husband
* Muriel Pavlow as Miranda
* Margaret Halstan as Lady Yeldham
* Roddy Hughes as Vicar
* O. B. Clarence as First Magistrate
* Margaret Rutherford as Second Magistrate
* Wally Patch as Third Magistrate
* Martita Hunt as Madame Mirelle, the dressmaker Charles Carson as Johnson
* Bernard Miles as Constable
* Terry-Thomas (uncredited) appears as an extra

==Critical reception==
The New York Times wrote, "a foreword to the film states that its production was interrupted five times when Nazi bombs exploded on the studio, but all their destructive fury has left no visible mark on the quiet humor and the atmosphere of hearthside warmth that permeate this wisp of a tale about a young couple on the eve of their marriage...Anthony Asquith has directed with tender appreciation of his material this completely unpretentious and charming film, the component parts of which are as delicately balanced as the mechanism of a watch."  {{cite web|url=http://www.nytimes.com/movie/review?res=9901E0DB173DE333A2575AC2A9649D946093D6CF|title=Movie Review -
  Quiet Wedding - At the Little Carnegie - NYTimes.com|work=nytimes.com}} 

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 


 
 