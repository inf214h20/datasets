Magkakabaung
{{Infobox film
| name           = Magkakabaung
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Jason Paul Laxamana
| producer       = Cyrus Khan   
| writer         = Jason Paul Laxamana
| screenplay     = 
| story          = 
| based on       =  
| starring       = Allen Dizon Gladys Reyes Chanel Latorre Emilio Garcia
| narrator       = 
| music          = Diwa de Leo   
| cinematography = Rain Yamson   
| editing        = Jason Paul Laxamana
| studio         = ATD Entertainment Productions
| distributor    = Ferdinand Lapuz   
| released       = 29 August 2014   (Canada, premiere)   17 December 2014 (Philippines)
| runtime        = 
| country        = Philippines Kapampangan   
| budget         = 
| gross          =
}} drama independent independent film directed, written and edited by Jason Paul Laxamana.    The film is known for winning the Best Asian Film at the 3rd Hanoi International Film Festival in Vietnam on November 2014. The film was also an official entry at the New Wave Category of the 40th Metro Manila Film Festival

==Plot==
A single father accidentally kills his eight-year-old daughter by administering the wrong medication. He finds it challenging not only to have her body buried but also to bury his culpability.

==Cast==
*Allen Dizon as Randy
*Gladys Reyes as Mabel
*Chanel Latorre as Neri Emilio Garcia as Mr. Canda
*Felixia Crysten Dizon as Angeline

==Production== Kapampangan as the primary language accompanied with English subtitles.     

==Release==
The film was first released at the Montréal World Film Festival on 29 August 2014. Magkakabaung was first showed in the United States on 11 September 2014 at the Harlem International Film Festival. The Hong Kong premiere for the film was on 12 November 2014 at the Hong Kong Asian Film Festival and on Vietnam on 23 November 2014 at the Hanoi International Film Festival. The film was premiered at the Austin Film Society in the United States on 11 December 2014. Magkakabaung is released in the Philippines on 17 December 2014 as an official entry of the New Wave Category at the 40th Metro Manila Film Festival.

==Reception==
The Network for the Promotion of Asian Cinema describe the film, which was shown at the 3rd Hanoi International Film Festival, as "an emotionally rich journey that is free of sentimentality. By slowly and confidently unveiling a hero we believe in, we encounter the unseen and never doubt the truthfulness of this experience,”  
 Representative Winston Castelo, filed House Resolution No. 1744, which seeks to commend Magkakabaungs director Jason Paul Laxamana citing “Laxamana deserves all the praise and recognition for bringing pride, glory and honor to the country.”   
==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient(s) and nominee(s)
! width="10%"| Result
|-
| rowspan="6" align="center"| 2014 Harlem International Film Festival Awards   
| align="left"| Best Actor
| align="center"| Allen Dizon
|  
|-
| rowspan="2" align="left"| Network for the Promotion of Asian Cinema (NETPAC) Award       3rd Hanoi International Film Festival 
| align="left"| Best Asian Film
| align="center"| Magkakabaung
|  
|- Best Actor||align=center|Allen Dizon|| 
|- Metro Manila Film Festival Awards
| align="left"| New Wave Best Picture
| align="center"| Magkakabaung
|  
|- New Wave Jason Paul Laxamana|| 
|- New Wave Allen Dizon|| 
|}

==External Links==
 

==References==
 

 
 
 
 