Advance to the Rear
{{Infobox film
| name           = Advance to the Rear
| image          = Advance to the Rear poster.jpg
| image size     = 
| caption        = Australian film poster George Marshall
| producer       = Ted Richmond Robert Carson Samuel A. Peeples William Chamberlain Jack Schaefer (novel)
| narrator       = 
| starring       = Glenn Ford Stella Stevens Melvyn Douglas
| music          = Randy Sparks Hugo Montenegro performed by The New Christy Minstrels
| cinematography = Milton R. Krasner
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       = June 10, 1964
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,100,000 (US/ Canada) 
}}
 western comedy George Marshall.  The film is based on the 1957 novel Company of Cowards by Jack Schaefer, with the film having that title in pre-production and when released in the United Kingdom.  However, the novel had none of the comedic elements of the film which retained only the basic idea of a unit formed out of men who had been court-martialed for cowardice and sent out west as well as some character names.

==Plot summary== Union Colonel Confederate counterpart. They fire a few artillery rounds in each others general direction at precisely the same time each morning, then go back to contentedly waiting for the war to end.

Captain Jared Heath, however, disturbs the status quo one day by going out and capturing some of the enemy. The Confederates feel obliged to retaliate. One thing leads to another and a military fiasco results. As punishment, Brackenbury and Heath are demoted, placed in charge of all the misfits General Willoughby can find and shipped west, where they can (hopefully) do no further damage.

The rebels are suspicious, so they send a beautiful spy, Martha Lou Williams, to find out their "real" mission. After questioning Easy Jenny, a madam Martha Lou is traveling with, Heath sees through Martha Lous ruse. But he decides that he is going to marry her eventually, so Heath does his best to keep her out of mischief.

When the unit is sent to escort an important gold shipment, the soldiers are captured by Thin Elk, an Indian chief in league with Hugo Zattig of the Confederates. Zattigs men masquerade as Union soldiers (using uniforms taken from prisoners) and hijack the shipment. Thin Elk, meanwhile, recognizing Brackenbury as a fellow West Point graduate, lets his captives go, although without horses or guns.

Heath takes charge. He and the men steal horses from the Indians, retrieve the gold (and Martha Lou) and capture Zattigs gang.

==Cast==
 
*Glenn Ford as Captain/Lieutenant Jared Heath
*Stella Stevens as Martha Lou Williams
*Melvyn Douglas as Colonel/Captain Claude Brackenbury
*Jim Backus as General Willoughby
*Joan Blondell as Easy Jenny
*Andrew Prine as Private Owen Selous Jesse Pearson as Corporal Silas Geary
*Alan Hale, Jr. as Sergeant Beauregard Davis
*James Griffith as Hugo Zattig
*Whit Bissell as Captain Queeg
*Michael Pate as Thin Elk
 

==Musical soundtrack==
The score was composed by Randy Sparks with songs sung by The New Christy Minstrels and orchestral music arranged and conducted by Hugo Montenegro. The popular song "Today" (while the blossoms still cling to the vine), comes from this film.  The song was composed (both words and music) by Randy Sparks who was a member of The New Christy Minstrels and it was this vocal group that perhaps had the most commercially successful recording of the song.  The song has been recorded by several artists, including the late John Denver, but perhaps the most amusing aspect of this lovely ballad is that so many people assume it to be a centuries-old folk song and not part of a Hollywood soundtrack.  The film also features a title song under the name of the original title and UK title of the film "Company of Cowards" and another song called "Riverboat".

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 