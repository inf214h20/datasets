Dealin' with Idiots
{{Infobox film
| name           = Dealin with Idiots
| image          = Dealin With Idiots poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jeff Garlin
| producer       = Christine Vachon Erin OMalley
| writer         = Jeff Garlin
| starring       = Jeff Garlin Fred Willard Bob Odenkirk J. B. Smoove Timothy Olyphant Richard Kind Nia Vardalos Jamie Gertz Steve Agee Gina Gershon
| music          = Larry Goldings
| cinematography = James Laxton
| editing        = Jonathan Corn Nena Hsu Erb
| studio         = Killer Films
| distributor    = IFC Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  $16,757 
}}
 2013 American film written and directed by Jeff Garlin, who also stars.  It was distributed by IFC Films and released on July 12, 2013.   

==Synopsis==
According to a press release from IFC Films, the films distributor, Dealin with Idiots is about Max Morris, a famous comedian, who decides to get to know the colorful parents and coaches of his sons Little League Baseball team in an attempt to find the inspiration for his next movie.

==Cast==
*Jeff Garlin as Max Morris, a famous comedian   
*Max Wright  as Jack Morris
*Mindy Rickles  as Shelley Dave Sheridan  as Forrest
*Hope Dworaczyk  as Angela
*Kerri Kenney-Silver  as Caitlin
*Steve Agee  as Hezekiah
*Fred Willard  as Marty
*Alex Puccinelli  as Ambrose
*Bob Odenkirk  as Coach Jimbo
*J. B. Smoove  as Coach Ted
*Jami Gertz  as Rosie
*Gina Gershon  as Sophie
*Timothy Olyphant  as Maxs Dad
*Richard Kind  as Harold
*Nia Vardalos  as Ava Morris
*Ian Gomez  as Commissioner Gordon
*Ali Wong  as Katie
*Xolo Mariduena  as Manny Chris Williams  as Bengal Bob
*Deanna Brooks  as Jackie the Chocolatier
*Natasha Leggero  as Tipsy Jessica Ian Roberts  as Freddy
*Luenell  as Big Time Sara
*Bobby Dekeyser  as Hans
*Hiromi Oshima  as Andrea 
*Freddy Lockhart  as Umpire #2
*Sarayu Rao  as Opposing Team Parent

==Production==
Dealin with Idiots was written and directed by Garlin, inspired by his experiences with his sons youth baseball team. 

==References==
 

 
 
 
 
 
 
 