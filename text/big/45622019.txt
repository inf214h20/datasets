Rita of Cascia (film)
{{Infobox film
| name =   Rita of Cascia 
| image =
| image_size =
| caption =
| director = Antonio Leonviola
| producer = 
| writer = Celestino Spada    Antonio Leonviola 
| narrator =
| starring = Elena Zareschi   Ugo Sasso   Beatrice Mancini   Marcello Giorda
| music = Pietro Sassoli  
| cinematography = Giovanni Pucci 
| editing = Gisa Radicchi Levi 
| studio = Artisti Associati 
| distributor = Artisti Associati 
| released = 31 May 1943 
| runtime = 83 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Rita of Cascia (Italian:Rita da Cascia) is a 1943 Italian historical film directed by  Antonio Leonviola and starring  Elena Zareschi, Ugo Sasso and Beatrice Mancini.  It was made at the Titanus Studios in Rome. The film portrays the life of the Catholic saint Rita of Cascia.

==Cast==
*    Elena Zareschi as Rita da Cascia  
* Ugo Sasso as Paolo di Ferdinando  
* Beatrice Mancini as Ada  
* Marcello Giorda as Antonio, il padre di Rita  
* Laura Nucci as Jacoviella  
* Augusto Marcacci as Il barone di Collegiacone  
* Elodia Maresca as Amata 
* Teresa Franchini as La madre superiore  
* Stefano Sciaccaluga as Il pellegrino  
* Giulio Battiferri as Lampo, il servo spia del barone  
* Gian Paolo Rosmino as Frate Remigio 
* Luigi Garrone as Il taverniere 
* Umberto Spadaro as Il delatore nella taverna  
* Giovanni Onorato as Un cliente nella taverna  
* Amina Pirani Maggi as Una suora  
* Nera Bruni as Un altra suora  
* Vittoria Mongardi as Una conversa 
* Elio Marcuzzo as Gian Giacomo  
* Aleardo Ward as Paolo Maria  
* Umberto Leurini as Gian Giacomo da bambino 
* Amedeo Leurini as Paolo Maria da bambino 
* Adele Garavaglia 
* Lamberto Picasso

== References ==
 
 
==Bibliography==
* Nerenberg, Ellen Victoria. Prison Terms: Representing Confinement During and After Italian Fascism. University of Toronto Press, 2001. 

== External links ==
* 

 
 
 
 
 
 
 
 
 

 