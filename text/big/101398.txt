Hamlet (1996 film)
 
 
{{Infobox film
| name           = Hamlet
| image          = Hamlet_1996_poster.jpg
| caption        = Film poster
| director       = Kenneth Branagh David Barron
| screenplay     = Kenneth Branagh
| based on       =  
| starring       = Kenneth Branagh Julie Christie Billy Crystal Gérard Depardieu Charlton Heston Derek Jacobi Jack Lemmon Rufus Sewell Robin Williams Kate Winslet 
| music          = Patrick Doyle Alex Thomson
| editing        = Neil Farrell
| studio         = Castle Rock Entertainment
| distributor    = Columbia Pictures
| released       =  
| runtime        = 242 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $18 million    (adjusted by inflation: $ )   . Box Office Mojo. Retrieved 26 January 2012. 
| gross          = $4,739,189    (adjusted by inflation: $ )  
}} Queen Gertrude, Kate Winslet as Ophelia (character)|Ophelia, Michael Maloney as Laertes (Hamlet)|Laertes, Richard Briers as Polonius, and Nicholas Farrell as Horatio (character)|Horatio. Other notable appearances include Robin Williams, Gérard Depardieu, Jack Lemmon, Billy Crystal, Rufus Sewell, Charlton Heston, Richard Attenborough, Judi Dench, John Gielgud and Ken Dodd.

The film is notable as the first unabridged theatrical film version of the play, running just over four hours. The longest screen version of the play prior to the 1996 film was the 1980 BBC made-for-television version starring Derek Jacobi, which runs three-and-a-half hours.
 The Master.
 Best Art Tim Harvey), Best Costume Best Original Best Writing (Adapted Screenplay) (Kenneth Branagh).

==Plot== the original play.

==Cast==

===Main characters===
*Kenneth Branagh as Prince Hamlet, the storys protagonist and Prince of Denmark. He is the son of the late King Hamlet and heir to the throne of Denmark. At first, Hamlet is mournful of his fathers death and dissatisfied with his mothers swift remarriage to Claudius. However, Hamlet is later told by the ghost of his father King Hamlet that Claudius murdered him, usurping his title. Upon knowing this crime, Hamlet is sworn to avenge his fathers murder. Branaghs interpretation of the title role, by his own admission, was considerably less "neurotic" than others, removing the Oedipal fixation so prominently featured in Oliviers 1948 film among others. During the scenes in which Hamlet pretends to be insane, Branagh portrayed the Prince as Mania|manic.
*Derek Jacobi as King Claudius, the storys antagonist and brother of the late king. He murdered his brother Hamlet by pouring poison into his ear while he slept. He then quickly usurps his brothers title and quickly marries his widow. At first, believing Hamlet to be mad by the loss of his father and rejection from Ophelia, Claudius is persuaded by Polonius to spy on Hamlet. When Claudius later learns Hamlet knows of the murder, he tries to use Rosencrantz and Guildenstern, two of Hamlets schoolmates, to have his nephew murdered. Although Rosencrantz and Guildenstern are more than willing to serve Claudius, they have no idea that he wants Hamlet dead.
*Julie Christie as Gertrude (Hamlet)|Gertrude, Queen of Denmark and wife to both the late King Hamlet and King Claudius, whom she married swiftly following the formers passing—ignorant of the foul play that caused his death.
*Richard Briers as Polonius, the Lord Chamberlain. An impertinent busy-body, Polonius believes Hamlet to be mad and convinces Claudius to join him in spying on the prince. He is eventually murdered while eavesdropping by Hamlet, who mistakes him for Claudius.  Laertes to end their relationship. She is eventually driven mad by both Hamlets rejection and her fathers murder and drowns herself. Wittenberg University.
*Michael Maloney as Laertes (Hamlet)|Laertes, the son of Polonius and brother of Ophelia. After instructing his sister to have no further relations with Hamlet, he departs for Paris. Upon news of his fathers murder, Laertes returns to Denmark, leading a mob to storm the castle. Claudius explains to him who the real killer was and incites Laertes to kill Hamlet and avenge Polonius death. He later conspires with Claudius to murder Hamlet during a fencing duel.
*Rufus Sewell as Fortinbras, the Norwegian crown prince. Played mostly in flashback and frequently referenced throughout the film, Fortinbras and his army storm Elsinore castle during the final scene, assuming the vacant throne of Denmark.

===Supporting characters===
*Robin Williams as Characters in Hamlet#Osric|Osric, the Elsinore courtier sent by Claudius to invite Hamlet to participate in the duel with Laertes.
*Gérard Depardieu as Reynaldo, a servant to Polonius. He is sent by Polonius to Paris to check up on Laertes.
*Timothy Spall as Rosencrantz and Guildenstern|Rosencrantz, a courtier friend of Hamlet who is sent by Claudius to spy on Hamlet.
*Reece Dinsdale as Rosencrantz and Guildenstern|Guildenstern, a courtier friend of Hamlet who is sent by Claudius to spy on Hamlet.
*Jack Lemmon as Marcellus, a sentry at Elsinore who, with Barnardo, alerts Horatio of the appearance of King Hamlets Ghost.
*Ian McElhinney as Barnardo, a sentry at Elsinore who, with Marcellus, alerts Horatio of the appearance of King Hamlets Ghost.
*Ray Fearon as Francisco, a sentry at Elsinore and the first character to appear on screen. Ghost of Hamlets Father, an apparition in the form of the late King who informs Hamlet of his murder and Claudiuss usurpation of the throne. First Gravedigger, a sexton digging Ophelias grave who makes a case as to why she should not receive Christian burial before making quick dialogue with Hamlet. He later presents the skull of the Yorick to Hamlet, not knowing of Hamlets history with the jester. Second Gravedigger
*Don Warrington as Voltimand, an ambassador sent by King Claudius to Old King Norway.
*Ravil Isyanov as Cornelius, an ambassador sent by King Claudius to Old King Norway.
*Charlton Heston as the Player King
*Rosemary Harris as the Player Queen
*Richard Attenborough as the English Ambassador
*John Gielgud as Priam, the King of Troy, played in flashback during the Player Kings speech.
*Judi Dench as Hecuba, the Queen of Troy and wife of Priam, played in flashback during the Player Kings speech.
*John Mills as Old King Norway, uncle of Fortinbras, played in flashback reprimanding his nephew for claims against Denmark.
*Ken Dodd as Yorick, the Kings Jester, played in flashback entertaining the royals of Elsinore during the gravediggers scene.
*John Spencer-Churchill, 11th Duke of Marlborough, appears in a small cameo scene as the Norwegian Captain.

==Production==

===Origins===
Aspects of the films staging are based on Adrian Nobles recent Royal Shakespeare Company production of the play, in which Branagh had played the title role. 

===Text===
The film uses a conflated text based on the 1623 First Folio, with additions from the Second Quarto and amendments from other sources.  According to a note appended to the published screenplay, "The screenplay is based on the text of Hamlet as it appears in the First Folio – the edition of Shakespeare’s plays collected by his theatrical associated Heminges and Condell and published in 1623 by a syndicate of booksellers.  Nothing has been cut from this text, and some passages absent from it (including the soliloquy ‘How all occasions do inform against me…’) have been supplied from the Second Quarto (an edition of the play which exists in copies dated 1604 and 1605).  We have also incorporated some readings of words and phrases from this source and from other early printed texts, and in a few cases emendations from modern editors of the play.  Thus in I, 4, in the passage (from the Second Quarto) about the dram of eale, we use an emendation from the Oxford edition of the Complete Works (edited by Stanley Wells and Gary Taylor, 1988): doth all the noble substance over-daub  – rather than the originals of a doubt." 

===Style=== flashbacks to long single takes for numerous scenes.
 Ben Hur, The Ten Doctor Zhivago.  As J. Lawrence Guntner points out, comparisons with the latter film are heightened by the presence of Julie Christie (Zhivagos Lara) as Gertrude. 

===Filming=== Panavision Super Alex Thomson. The Master in 2012. 

===Music===
{{Infobox album|  
| Name        = William Shakespeares Hamlet: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Patrick Doyle
| Cover       =Hamlet_Doyle.jpg
| Recorded    = 1996
| Released    = 10 December 1996
| Genre       = Soundtrack
| Length      = 76:25
| Label       = Sony Classical Records
| Producer    = Patrick Doyle Maggie Rodford
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =    
| rev2 = Film Music on the Web
| rev2Score =    Filmtracks
| rev3Score =    
}}
The score to Hamlet was composed and co-produced by frequent Kenneth Branagh collaborator  . Retrieved 27 January 2012. 
 Best Original Score.

# In Pace (3:07) – performed by Plácido Domingo (this is heard in the film during the closing credits)
# Fanfare (0:48)
# "All that lives must die" (2:40)
# "To thine own self be true" (3:04)
# The Ghost (9:55)
# "Give me up the truth" (1:05)
# "What a piece of work is a man" (1:50)
# "What players are they" (1:33)
# "Out out thou strumpet fortune" (3:11)
# "To be or not to be" (1:53)
# "I loved you once" (3:27)
# "Oh, what a noble mind" (2:41)
# "If once a widow" (3:36)
# "Now could I drink hot blood" (6:57)
# "A foolish prating nave" (1:05)
# "Oh heavy deed" (0:56)
# "Oh here they come" (4:39)
# "My thoughts be bloody" (2:52)
# "The doors are broke" (1:20)
# "And will a not come again?" (1:59)
# "Alas poor Yorick" (2:49)
# "Sweets to the sweet – farewell" (4:39)
# "Give me your pardon sir" (1:24)
# "Part them they are incensed" (1:47)
# "Goodnight, sweet prince" (3:36)
# "Go bid the soldiers shoot" (2:52)

==Release==
Hamlet was screened out of competition at the 1997 Cannes Film Festival.    A shorter edit of the Branagh film, approximately two-and-a-half hours long, was also shown in some markets.  

===Home media===
A 2-Disc DVD was released in the US and Canada on 14 August 2007. It includes a full-length commentary by Branagh and Shakespeare scholar Russell Jackson. A Blu-ray Disc was released on 17 August 2010 in the US and Canada with similar additional features, including an introduction by Kenneth Branagh, the featurette "To Be on Camera: A History with Hamlet", the 1996 Cannes Film Festival Promo, and a Shakespeare Movies Trailer Gallery. 

==Reception==

===Box office===
Hamlet was not a success at the box office, mostly due to its limited release. The film earned just over $90,000 in its opening weekend playing on three screens. It made just over $30,000 in the Czech Republic (the films only foreign market) and ultimately played on fewer than 100 screens in the United States, bringing its total gross to just under $5 million on a budget of $18 million. 

===Critical response===
Hamlet received overwhelmingly positive reviews. It currently holds a 95% rating at Rotten Tomatoes with the consensus, "Kenneth Branaghs sprawling, finely textured adaptation of Shakespeares masterpiece lives up to its source material, using strong performances and a sharp cinematic focus to create a powerfully resonant film that wastes none of its 246 minutes."   
 lauded 1948 version, stating, "Branaghs Hamlet lacks the narcissistic intensity of Laurence Oliviers (in the 1948 Academy Award winner), but the film as a whole is better, placing Hamlet in the larger context of royal politics, and making him less a subject for pity."    Janet Maslin of The New York Times also praised both Branaghs direction and performance, writing, "This Hamlet, like Much Ado About Nothing (1993 film)|Branaghs version of Much Ado About Nothing, takes a frank, try-anything approach to sustaining its entertainment value, but its gambits are most often evidence of Branaghs solid showmanship. His own performance is the best evidence of all."  The New York Review of Books praised the attention given to Shakespeares language, "giving the meter of the verse a musicians respect";  Branagh himself said his aim was "telling the story with utmost clarity and simplicity." 

Some critics, notably Stanley Kauffmann, declared the film to be the finest motion picture version of Hamlet yet made. Noted online film critic James Berardinelli wrote the film a glowing four star review and went so far as to declare the Branagh Hamlet the finest Shakespeare adaptation ever, rating it as the best film of 1996, the fourth best film of the 90s, and one of his top 101 favourite films of all time, saying, "From the moment it was first announced that Branagh would attempt an unabridged Hamlet, I never doubted that it would be a worthy effort. After all, his previous forays into Shakespeare have been excellent. Nothing, however, prepared me for the power and impact of this motion picture. Hyperbole comes easily when describing this Hamlet, decidedly the most impressive motion picture of 1996. Nothing else this year has engaged my intellect, senses, and emotions in quite the same way. I have seen dozens of versions of this play (either on screen or on stage), and none has ever held me in such a grip of awe. This may be Branaghs dream, but it is our pleasure."   
 John Simon Alex Thomson, but stated that "Branagh essentially gives a stage performance that is nearly as over-the-top as some of his directorial touches." 
 Henry V (1989), which ranks in first place.   

===Accolades===
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Category
! Recipients and nominees Result
|-
| rowspan=4 | Academy Awards Best Art Direction Tim Harvey
|  
|- Best Costume Design
| Alexandra Byrne
|  
|- Best Original Score
| Patrick Doyle
|  
|- Best Writing (Adapted Screenplay)
| Kenneth Branagh
|  
|- Art Directors Guild Awards
| ADG Excellence in Production Design Award Tim Harvey, Desmond Crowe
|  
|-
| rowspan=2 | British Academy Film Awards Best Costume Design
| Alexandra Byrne
|  
|- Best Production Design Tim Harvey
|  
|-
| rowspan=2 | British Society of Cinematographers
| GBCT Operators Award
| Martin Kenzie
|  
|-
| Best Cinematography Award Alex Thomson
|  
|-
| Broadcast Film Critics Association Awards Best Film
| Kenneth Branagh
|  
|- The International Film Festival of the Art of Cinematography CAMERIMAGE
| Golden Frog Award for Best Cinematography Alex Thomson
|  
|- Chicago Film Critics Association Awards Best Actor
| Kenneth Branagh
|  
|- Empire Awards Best British Actress
| Kate Winslet
|  
|-
| Evening Standard British Film Awards
| Special Jury Award
| Kenneth Branagh
|  
|- San Diego Film Critics Society Awards Best Actor
| Kenneth Branagh
|  
|-
| rowspan=5 | Satellite Awards Best Art Direction and Production Design Tim Harvey
|  
|- Best Cinematography Alex Thomson
|  
|- Best Costume Design
| Alex Byrne
|  
|- Best Original Score
| Patrick Doyle
|  
|- Best Supporting Actress – Motion Picture
| Kate Winslet
|  
|-
|}

==See also==
*Hamlet in performance
*Hamlet on screen

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 