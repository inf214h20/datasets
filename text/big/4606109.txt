I Bury the Living
{{Infobox Film
| name           = I Bury the Living
| image          = IBuryTheLiving.jpg
| image_size     = 
| caption        = 
| director       = Albert Band
| producer       = Louis Garfinkle Albert Band
| writer         = Louis Garfinkle
| narrator       = 
| starring       = Richard Boone Theodore Bikel
| music          = Gerald Fried
| cinematography = Frederick Gately Frank Sullivan
| distributor    = United Artists
| released       =   July 1958
| runtime        = 76 min. USA
| English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
I Bury the Living (1958) is a horror film directed by famed B-movie director Albert Band, father of Charles Band, and starring Richard Boone and Theodore Bikel.

==Plot summary==

Robert Kraft (Richard Boone) is the newly appointed chairman of a committee that oversees a large cemetery. The cemetery caretaker, Andy MacKee (Theodore Bikel), keeps a map in the cemetery office displaying the grounds and each grave site. Filled graves are marked by black pins and unoccupied but sold graves are marked with white pins. New to the position and unobservant, Kraft accidentally places a pair of black pins where they dont belong, only to discover later that the young couple who had bought the grave sites in question died in an automobile accident soon afterwards. He believes that he marked them for death.

Hoping it will give him peace of mind, Robert replaces a random white pin with a black pin.  When that person dies later in the week, however, he becomes increasingly convinced that either he or the map has some kind of dark power.  Repeated experiments, undertaken upon the insistence of skeptical friends and co-workers, yield the same result.  Kraft slips into deep guilt and depression and believes he is cursed.

The police, who are initially skeptical, eventually begin to take notice and, in the hopes that it will reveal the cause of the deaths, ask Robert to place a black pin on the grave of a person who is known to be in France.  Although he does so, Robert continues his slide into despair.  That same night, he decides that if black pins give him the power of death, white pins might give him the power of life.  He replaces all of the recently placed black pins with white pins.  When he goes to the associated grave sites later that night, he discovers that they have all been dug up, with the bodies gone.

Upon returning to the cemetery office, Robert receives a call informing him of the death of the man in France.  As he hangs up the phone, the cemetery caretaker comes up behind him, covered in dirt.  He reveals that he has been killing all of the marked people as revenge for being forced to retire.  However, when Robert informs him of the passing of the man in France, the caretaker, who couldnt have killed the man, begins to lose his mind.  When the police arrive, they find the caretaker dead and tell Robert that the news of the mans death was all a ruse to flush out the cemetery caretaker.

==Cast==
*Richard Boone as Robert Kraft
*Theodore Bikel as Andy McKee
*Peggy Maurer as Ann Craig Howard Smith as George Kraft
*Herbert Anderson as Jess Jessup
*Robert Osterloh as Lt. Clayborne

==External links==
*  
*  
*  
*  

 
 
 
 