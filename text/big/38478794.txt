The Red Countess
{{Infobox film
| name           = The Red Countess
| image          = 
| caption        = 
| director       = András Kovács
| producer       = József Bajusz
| writer         = Mihályné Károlyi András Kovács
| starring       = Juli Básti
| music          = 
| cinematography = Miklós Bíró
| editing        = 
| distributor    = 
| released       =  
| runtime        = 180 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

The Red Countess ( ) is a 1985 Hungarian drama film directed by András Kovács. It was entered into the 14th Moscow International Film Festival.   

==Cast==
* Juli Básti as Katinka Andrássy|Katus, gróf Károlyi Mihályné, Andrássy Katinka
* Ferenc Bács as Mihály Károlyi|Gróf Károlyi Mihály
* Ferenc Kállai as Gyula Andrássy the Younger|Gróf Andrássy Gyula
* Hédi Temessy as Eleonóra Zichy|Gróf Andrássy Gyuláné
* Klári Tolnay as Geraldin, Károlyi nevelõanyja
* Ildikó Molnár as Klára Andrássy|Kája, Katus huga
* Gábor Reviczky as Pallavichini György
* András Bálint as Oszkár Jászi|Jászi Oszkár Tisza István
* Teri Tordai as Madeleine, Károlyi szeretõje
* László Tahi Tóth as Kéry Pál
* Edit Frajt as Ilona, Katuss Sister

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 