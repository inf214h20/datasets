Safari 3000
{{Infobox film
| name           = Safari 3000
| image          = Safari 3000.jpg
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Harry Hurwitz Arthur Gardner
| writer         = Arthur Gardner Michael Harreschou Jules V. Levy
| narrator       =
| starring       = David Carradine Stockard Channing Christopher Lee Ernest Gold
| cinematography = Adam Greenberg
| editing        = Samuel E. Beetley
| studio         = Levy-Gardner-Laven
| distributor    = Metro-Goldwyn-Mayer|MGM/UA Entertainment
| released       =
| runtime        = 92 minutes
| country        =   English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 1982 film directed by Harry Hurwitz. The film was shot on location in Africa. 

==Plot==
A reporter follows a stunt driver on a Safari Rally|3000-mile race across Africa.

==Principal cast==
{| class="wikitable" width="50%"
|- style="background:#CCCCCC;"
! Actor !! Role
|-
| David Carradine || Eddie Mills
|-
| Stockard Channing|| J.J. Dalton
|-
| Christopher Lee || Count Borgia
|-
| Hamilton Camp || Feodor
|-
| Ian Yule || Freddy Selkirk
|}

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 