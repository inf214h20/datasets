Au Revoir Taipei
 
{{Infobox film
| name           = Au Revoir Taipei
| image          = Au Revoir Taipei Poster.jpg
| image_size     = 
| caption        = 
| director       = Arvin Chen
| producer       = In-Ah Lee Wei-Jan Liu Michelle Cho Oi Leng Lui Wim Wenders (Executive Producer)
| writer         = Arvin Chen
| narrator       = 
| starring       = Jack Yao Amber Kuo Lawrence Ko Frankie Gao
| music          = Du-Che Tu
| cinematography = Michael Fimognari
| editing        = Justin Guerrieri
| distributor    = Atom Cinema Beta Cinema
| released       =  
| runtime        = 85 minutes
| country        = Taiwan Taiwanese Taiwanese Mandarin French French
| budget         = NTD$30,000,000
}} Arvin Chens Network for the Promotion of Asian Cinema" (NETPAC Prize) at the Berlin International Film Festival 2010 and was considered a box office success in Taiwan.

==Plot==
Kai, a lovesick young man, wants to leave Taipei in hopes of getting to Paris to be with his girlfriend. Kai spends long nights in a bookstore studying French, where Susie, a girl who works there, begins to take an interest in him. After one extra ordinary night, Kai finds the excitement and romance he was longing for are already right there in Taipei.

==Cast==
* Jack Yao as Kai
* Amber Kuo as Susie
* Lawrence Ko as Hong
* Joseph Chang as Jiyong
* Tseng Pei-yu as Yuan Yuan
* Tony Yang as Lei Meng
* Frankie Kao as Bao Ge
* Jack Kao as Kais father
* Chiang Kang-Che as Gao gao

==Awards==
Au Revoir Taipei won the Network for the Promotion of Asian Cinema (NETPAC) Prize at the Berlin International Film Festival 2010, the Jury Award at the Deauville Asian Film Festival in France, the Audience Award at the 2010 San Francisco Asian American International Film Festival,  the Golden Durian (Best Film) Award at the 2010 Barcelona Asian Film Festival, and Best Narrative Feature at the Asian Film Festival of Dallas.

Kuo was awarded Best New Actor at the 12th Taipei Film Festival in 2010 for her role as Susie. 

==References==
 

==External links==
* 
* 

 
 
 
 
 


 