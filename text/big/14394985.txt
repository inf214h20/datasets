Just Neighbors
 
{{Infobox film
| name           = Just Neighbors
| image          = 
| image size     = 
| caption        = 
| director       = Harold Lloyd Frank Terry
| producer       = Hal Roach
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = Fred Guiol
| editing        = 
| distributor    = 
| released       =  
| runtime        = 13 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd. Prints of the film survive in the film archives at George Eastman House, the UCLA Film and Television Archive, Filmoteca Española and the National Film, Television and Sound Archives of Canada.   

==Cast==
* Harold Lloyd - The Boy
* Bebe Daniels - The Bride
* Snub Pollard - The Neighbor
* Sammy Brooks - Short man in bank (uncredited)
* Helen Gilmore - Old woman with packages (uncredited)
* Margaret Joslin - Neighbors Wife (uncredited)
* Gus Leonard - Bearded bank teller / Vegetable vendor (uncredited)
* Gaylord Lloyd - Man in Line at Bank (uncredited)
* Marie Mosquini - (uncredited) Charles Stevenson - Postman (uncredited)
* Noah Young - (uncredited)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 