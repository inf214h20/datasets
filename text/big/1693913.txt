The Big Brass Ring
{{Infobox film
| image size     = 
| alt            = 
| image	=	The Big Brass Ring FilmPoster.jpeg
| caption        = Film Poster
| director       = George Hickenlooper
| producer       = Donald Zuckerman Andrew Pfeffer
| writer         = Orson Welles Oja Kodar F. X. Feeney George Hickenlooper
| starring       = 
| music          = Thomas Morse
| cinematography = Kramer Morgenthau
| editing        = Jim Makiej
| studio         = 
| distributor    = 
| released       = 1999
| runtime        = 104 min.
| country        = United States
| language       = English
| budget         = $7,000,000 (estimated)
| gross          = 
}}
The Big Brass Ring is a 1999 drama film, starring William Hurt, Nigel Hawthorne, Irene Jacob, Jefferson Mays, and Miranda Richardson (who was nominated for a Golden Globe Award for her performance).  The films script was heavily rewritten by George Hickenlooper and F. X. Feeney from a previous screenplay written by Orson Welles and Oja Kodar in the early 1980s; Hickenlooper also directed the film. 

==Plot==
The story concerns the darker side of a political campaign trail in Missouri. A gubernatorial candidate, Blake Pellarin, is making a campaign stop in St. Louis when his old mentor, Kim Minnaker, resurfaces. Minnaker left the country after a scandal, but now is working on a memoir and evidently possesses compromising photos of Pellarin that could end his hopes of becoming governor and, beyond that, President of the United States.

Pellarin is already juggling the pressures of a political race with a frayed relationship with his wife, Dinah, a wealthy woman with a drinking problem, and an affair with Cela, a worldly journalist. He endeavors to learn what kind of blackmail Minnaker has in store for him, as well as how it could affect his political future and his family.

==Cast==
*William Hurt as William Blake Pellarin
*Nigel Hawthorne as Kim Minnaker
*Irene Jacob as Cela
*Jefferson Mays as Garne Strickland
*Miranda Richardson as Dinah

==Production==
Welles had hoped to direct the film himself in the early 1980s, and to play the role of Minnaker (not least as he has been a "New Deal" Democrat close to Franklin Roosevelt), but he was unable to attract enough funding. He had been given a list of six "bankable" stars who could play Pellarin, including Clint Eastwood, Jack Nicholson, Robert Redford and Burt Reynolds, and was told by investors that if he could sign up one of them, then the film could be made; but all six actors refused for various reasons - according to Welless partner Oja Kodar, Redford was unavailable, Nicholson asked for too much money (he would not work for less than $2 million, which was the entire proposed budget of the film), while both Eastwood and Reynolds were uncomfortable with the films homosexual subtext, fearing its potential effect on their screen image, and Eastwood in particular feeling that it was at odds with his conservative values. 

However, Hickenlooper made substantial changes from the Welles script (published in 1991 by the University of Santa Barbara Press). Welless script concerned Senator Pellarin, a Democratic Presidential candidate in 1984 (closely modeled on Senator Gary Hart), and his troubled relationship with his disgraced homosexual mentor Kim Minnaker, a one-time Roosevelt New Deal Democrat who was now living in exile as advisor to the corrupt government of an unnamed African dictatorship. Hickenlooper retained the basic concept, but instead recast Pellarin as a candidate for Governor of Missouri, and Minnaker as living in Cuba, while much of the dialogue was rewritten and reinterpreted. None of Welless satire of Reagan-era politics was retained in the final film, while a number of key scenes, like a charged confrontation between Pellarin and Minnaker on a ferris wheel, were also omitted. 

==References==
 

== External links ==
*  
*   at Rotten Tomatoes

 
 

 
 
 
 
 
 
 
 
 


 