Shadow Man (1988 film)
{{Infobox Film|
  name=Shadow Man|
 
  size=220px|
  caption        = Theatrical film poster by Dana Andreev|
  director=Piotr Andrejew |
  producer=Guys Versluis|
  writer=Piotr Andrejew|
  starring=Tom Hulce Manouk van der Meulen Jeroen Krabbé Thom Hoffman |
  music=Edvard Grieg   Wim Mertens|
  cinematographer=Wit Dabal|
  distributor=RCV Hilversum New World Pictures| 1988 |
  runtime=93 min |
  language=English, Dutch |
  movie_series_label=|
  movie_series=|
  awards=|
  budget=|
  
}}
 1988 film about a Polish-Jewish refugee during a fictional war in Amsterdam. Like a `Walker between two worlds´ the famous Shadowman comics hero, created some years after this film, he partly belongs to the unseen world of darkness. Tom Hulce as the Shadow Man, created an interesting and moving performance. Also starring Jeroen Krabbé as Theo and Manouk van der Meulen as Monique, a Dutch couple provoked by the arrival of the Shadowman . 

Written and directed by Polish director Piotr Andrejew, recognized in Europe for his short movies, with the photography by Wit Dabal and the music by Edvard Grieg and Wim Mertens. 
A Dutch - British co-production in English, with some minor Dutch dialogue.    

Nominated Golden Leopard for the Best Film at the Locarno International Film Festival, Switzerland; 1989.
Opening Film National Dutch Film Festival, Utrecht, Netherlands; 1988.

==External links==
* 
*  

 
 


 