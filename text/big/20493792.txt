Alibi (1931 film)
{{Infobox film
| name           = Alibi
| image          = "Alibi"_(1931_film).jpg
| image_size     = 
| caption        = 
| director       = Leslie S. Hiscott
| producer       = Julius Hagen Michael Morton (play)  H. Fowler Mear
| starring       = Austin Trevor Franklin Dyall Elizabeth Allan John Deverell John Greenwood
| cinematography = Sydney Blythe
| editing        = 
| studio         = Twickenham Film Studios
| distributor    = Woolf & Freedman Film Service
| released       = 1931
| runtime        = 75 minutes
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
:For the 1929 US film, see Alibi (1929 film). British mystery mystery detective film directed by Leslie S. Hiscott and starring Austin Trevor, Franklin Dyall, and Elizabeth Allan.  
 Michael Morton which was in turn based on the Agatha Christie novel The Murder of Roger Ackroyd featuring her famous Belgian detective Hercule Poirot.
 Black Coffee Lord Edgware Dies in 1934, all starring Trevor as Poirot. He later starred in The Alphabet Murders, a 1965 Christie film, playing a minor role.

== Cast ==

* Austin Trevor  ...  Hercule Poirot  
* Franklin Dyall  ...  Sir Roger Ackroyd  
* Elizabeth Allan  ...  Ursula Browne  
* J.H. Roberts  ...  Dr. Sheppard  
* John Deverell  ...  Lord Halliford  
* Ronald Ward  ...  Ralph Ackroyd  
* Mary Jerrold  ...  Mrs. Ackroyd  
* Mercia Swinburne  ...  Caryll Sheppard  
* Harvey Braban  ...  Inspector Davis  
* Clare Greet    
* Diana Beaumont     Earl Grey

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 