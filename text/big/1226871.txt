No Retreat, No Surrender
 
{{Infobox film
| name           = No Retreat, No Surrender
| image          = NrnsPoster.jpg
| alt = 
| caption        = Film poster for No Retreat, No Surrender
| director       = Corey Yuen   
| producer       = Ng See-yuen 
| writer         = Keith W. Strandberg 
| story          = Corey Yuen Ng See-yuen 
| starring = {{Plainlist|
* Kurt McKinney
* J.W. Fails
* Ron Powell
* Kathie Sileno Peter "Sugarfoot" Cunningham
* Kent Lipham
* Jean-Claude Van Damme
}}
| music          = Paul Gilreath 
| cinematography = David Golia John Huneck 
| editing        = James Melkonia Mark Pierce Allan Poon 
| studio         = Seasonal Films Balcor Film Investors 
| distributor    = New World Pictures
| released       =  
| runtime        = 85 minutes   
| country        = United States    
| language       = English
| budget = 
| gross          = $4,662,137 
}}
No Retreat, No Surrender is a 1986 American martial arts film directed by Corey Yuen in his American directorial debut, and starring Kurt McKinney and Jean-Claude Van Damme. The film was released in the United States on May 2, 1986.   The film is about the American teenager named Jason Stillwell (Kurt McKinney) who learns martial arts from the spirit of Bruce Lee. Stillwell uses these lessons to defend his martial arts dojo against the Russian martial artist Ivan Kraschinsky (Jean-Claude Van Damme).
 The Karate Kid (1984).

==Plot== Sherman Oaks, organized crime syndicate looking to take over all the dojos in the country. After refusing to join the organization, Toms leg is broken by a Russian martial artist named Ivan Kraschinsky, one of the boss hired thugs.

The Stillwell family relocates to Seattle, where Jason meets R. J. Madison and they become good friends. Jason reunites with his old girlfriend Kelly Reilly, who lives in the neighborhood with her brother, local black belt Ian. Despite this, Jason has a hard time adjusting, as he and R. J. are constantly beaten and harassed by the local bullies led by an obese boy named Scott and arrogant martial artist Dean Ramsay. After getting beaten up and humiliated at Kellys birthday party by Scott and Dean, Jason visits the grave of Bruce Lee and beseeches him for aid. Later that night, Jason and Tom have a heated argument over Jasons involving himself in fights. When Jason calls his father a coward for running away from the syndicate, Tom destroys some of Jasons Bruce Lee memorabilia in the garage. Distraught, Jason consults with R. J., who suggests that Jason move all of his training gear into an abandoned house nearby. Exhausted from the move, Jason falls asleep at the house, but is suddenly awakened by the ghost of Bruce Lee, who appears to Jason and begins to train him. Under Lees tutelage, Jason goes from a below average fighter to a superior martial artist, at one point able to fend off several thugs who are assaulting his father in a parking lot.

Later on, Jason, Tom and R.J. attend an annual full-contact Kickboxing tournament featuring teams from Seattle and New York. Before the contest can get under way, however, the crime syndicate interrupts and makes a wager that none of the Seattle fighters can defeat Ivan. While Dean and Frank are easily dispatched by the Russian, Ivans last opponent, Ian, makes an impressive showing, forcing Ivan to resort to dirty tactics to defeat him. With Ian helplessly entangled in the ring ropes, Scott attempts to bite Ivan in the leg, but the Russian dispatches him with a headbutt. Kelly tries to stop Ivan by hitting him with a chair, but the Russian easily disarms her and grabs her by the hair. Spurred into action, Jason charges to the ring and attacks Ivan, much to the delight of the crowd. Utilizing his advanced training, Jason is finally able to conquer his nemesis and earn the respect of his peers.

==Cast==
  was cast as the Russian villain Ivan Kraschinsky.]]
* Kurt McKinney as Jason Stillwell
* Jean-Claude Van Damme as Ivan Kraschinsky the Russian
* J. W. Fails as R. J. Madison
* Kathie Sileno as Kelly Reilly Tai Chung Kim as the ghost of Bruce Lee
* Kent Lipham as Scott
* Ron Pohnel as Ian Reilly
* Dale Jacoby as Dean Ramsay Peter “Sugarfoot” Cunningham as Frank Peters
* Timothy D. Baker as Tom Stillwell
* Gloria Marziano as Mrs. Stillwell
*Paul Oswell as Trevor

==Production==
After living in Taiwan for a year in the early 1980s, screenwriter Keith W. Strandberg became interested in working in martial arts films as an actor.    Strandberg moved back to the United States and became a tour director in China, where he continued to stop by in Hong Kong to make contact with producers and screenwriters.  After being turned down by several studios including Shaw Brothers,  Strandberg read about Seasonal Film Corporation and got in contact with the studio head Ng See-yuen. Ng expressed an interest in making an American film and asked if Strandberg knew anything about screenplays. Strandberg stated that he had despite never seeing one before.  A year later, ng See-yuen contacted Strandberg in America stating that he wanted to write a script for them. Strandberg wrote a draft of what would become No Retreat, No Surrender. While production began on the film, Strandberg was on set and spent hours every night changing the script to improve its quality while filming. 
 Pete Cunningham which rendered him unconscious.  Actor and martial artist Timothy Baker stated that while working with Van Damme during the action scenes on the set, the production manager and director Corey Yuen instructed him to not make contact with the other actors and stuntmen.       Despite continuous warnings, Van Damme continued to make contacts with his kicks to Baker on the set.  Other actors and martial artists claimed that Van Damme had not been reckless with his physical contact with people during the fight scene including Ron Pohnel who said that "His control wasnt such as mine, but I had no complaints."  Van Damme originally had a two picture deal with screenwriter Keith W. Strandberg but broke his contract. 

==Release==
No Retreat, No Surrender was released on May 2, 1986.  The film was the 11th highest grossing film on its opening week in the American box office grossing $739,723.   The film gearned a total of $4,662,137.   

==Reception== Time Out Karate Kid, Black Belt gave the film a rating of one and a half out of five noting that Jean-Claude Van Damme does not have much screen time and that the film was derivative of The Karate Kid.   Patrick Goldstein of the Los Angeles Times called it "hilariously bad" and an "amateurish clunker" with poor action scenes. 
 Black Belt placed No Retreat, No Surrender at seventh on their list of top ten choreographed martial arts films. The magazine specifically praised Jean-Claude Van Dammes jump kicks while noting that Kurt McKinneys look "suspiciously quick" noting that "unlike the Hong Kong movie industry, American filmmakers have yet to master the technique of speeding up the film without "jumpy/fidgety" side effects". 

==See also==
* American films of 1986
* List of action films of the 1980s

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 