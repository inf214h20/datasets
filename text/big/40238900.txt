Road to Paloma
{{Infobox film
| name           = Road to Paloma
| image          = File:Road_to_Paloma.png
| alt            = 
| caption        = 
| director       = Jason Momoa
| producer       = Brian Andrew Mendoza Jason Momoa
| writer         = Jason Momoa Jonathan Hirschbein Robert Homer Mollohan
| starring       = Jason Momoa Sarah Shahi Lisa Bonet Michael Raymond-James Wes Studi Robert Homer Mollohan
| music          = 
| cinematography = Brian Andrew Mendoza
| editing        = Brian Andrew Mendoza Jennifer Tiexiera
| studio         = Boss Media Pride of Gypsies
| distributor    = Anchor Bay Entertainment (UK and US)   WWE Studios (US)
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $600,000
| gross          = 
}}
Road to Paloma is a 2014 American drama thriller film directed, produced, co-written by and starring Jason Momoa.  It co-stars Sarah Shahi, Lisa Bonet, Michael Raymond-James and Wes Studi. The film was released on July 11, 2014. 

==Plot==
 
After murdering his mothers rapist, Wolf (Jason Momoa), a Native American, flees from the law.6 months later he meets up with a drifter  called Cash (Robert Homer Mollohan) and heads north to his sisters property where he intends to go spread his mothers ashes, but with the law right behind him his dream to lay his mother to peace may come at a price.

== Cast ==
* Jason Momoa as Robert Wolf 
* Sarah Shahi as Eva 
* Lisa Bonet as Magdalena 
* Michael Raymond-James as Irish 
* Wes Studi as Numay 
* Jill Wagner as Sandy
* Timothy V. Murphy as Williams 
* Chris Browning as Schaeffer 
* James Harvey Ward as Billy
* Linden Chiles as Bob
* Steve Reevis as Totonka
* Tanoai Reed as Moose Robert Homer Mollohan as Cash
* Lance Henriksen as FBI Agent Kelly

==History==
=== Production ===
In October 2011 Jason Momoa stated that he was writing, directing and acting in an upcoming project, Road to Paloma.    Made for $600,000, filming began in February 2012 in Needles, California|Needles, California, where half of the filming was to be done. The rest took place in Los Angeles and Bishop, California|Bishop.    

Momoa set Shovels & Rope and The Rolling Stones to score the music for the film.   

=== Filming ===
Filming was scheduled to begin in February 2012 in Needles, California, where half of the filming was to be done with the rest set to take place in Los Angeles and Bishop, California|Bishop. 

=== Release ===
On August 13, 2013 it was announced that WWE Studios and Anchor Bay Entertainment had acquired distribution rights in North America, United Kingdom, Australia and New Zealand.       A late 2013 release was planned.   As of January 2014, the film will have a limited theatrical release on July 11, 2014, before its VOD release on July 15, 2014. The film has gained positive reviews.  
 2014 Sarasota Film Festival in April. 

WWE Studios and Anchor Bay have set the film for a July 11, 2014 release in Los Angeles.   

== References ==
 

== External links ==
*  
*  
*   at the ComingSoon.net

 
 
 
 
 
 
 
 
 
 
 
 
 
 