Hunting Humans
{{Infobox film
| name           = Hunting Humans
| image          = HuntingHumansDVD.jpg
| alt            = 
| caption        = DVD released by MTI Home Video
| film name      = 
| director       = Kevin Kangas
| producer       = Rick Ganz   Kevin Kangas
| writer         = Kevin Kangas
| screenplay     = 
| story          = 
| based on       = 
| starring       = Rick Ganz   Bubby Lewis
| narrator       = Rick Ganz Evan Evans
| cinematography = David Maurice Gil
| editing        = Harvey Glatman
| studio         = Marauder Productions
| distributor    = MTI Home Video   IFM World Releasing
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Hunting Humans is a 2002 horror film written and directed by Kevin Kangas. The film gained notoriety when a copy of it was found among the possessions of murderer Adam Leroy Lane.  

== Plot ==
 multiplex to murder a projectionist, who he is shocked to discover is already dead, with a note left on his body that reads "Ive Got Your Pattern". Enraged over the prospect of someone being on to him, Aric, in a fit of paranoia, shoots a suspicious dog walker. A few nights later, Aric is beaten unconscious in his home by the actual stalker, who identifies himself as "Dark". After Aric hires a private investigator named Marv to uncover information on Dark, his self-proclaimed rival challenges him to outdo his slaying of three picnickers, which Aric does by knifing four people in a theatre. Afterward, Aric learns from Marv that Dark is watching him through his own P.I., Frank Cooper.

Aric dismisses Marv, and picks up where he left off, intent on digging up everything he can on Dark through Frank. While doing so, Aric realizes that Dark is also spying on him through a co-worker named Barb, who Aric threatens into helping with his plan to get rid of Dark. Later, Aric breaks into Franks house again, and this time he is confronted by Marv, who is really Dark. The two serial killers try to one-up each other with various trump cards, ending with Dark gunning down Frank (who kept switching sides for monetary reasons) and running off when Aric reveals he has brought along a sniper rifle-wielding workmate named Doug. Aric chases and fights Dark in the woods, with Dark having the upper hand until Aric shoots him with a handgun, one of eighteen he had left in the area in anticipation that his and Darks battle would take place outside. Brad (who is unaware of Arics true nature) appears, and is murdered by Aric, who relocates to another city after eliminating the rest of his co-workers, and other loose ends.

== Cast ==

* Rick Ganz as Aric Blue
* Bubby Lewis as Aaron "Dark" Deckard
* Lisa Michele as Barb
* Trent as Frank Cooper
* Arthur Smith Jr. as Oliver Rust
* Jeff Kipers as Ken
* Catherine Granville as Carol Ann Stennings
* Joe Ripple as Police Detective
* Duke McClure as Brad
* James Fellows as Doug Fellows
* Rick Shipley as Corey Aaron Markbright
* Allison Klyn as Egg Girl
* Paul C. Kangas as Randy Jacob Green

== Reception ==
 Kangas has Michael Manns. For now, however, he has written a mean, smart, original script with Humans and has done an admirable job directing it as well".  Conversely, Evan Wade of Something Awful allotted the film a score of -37 out -50, and described it as "the kind of project that comes about when a bunch of people from the YMCA summer creative writing course decide to get together, overdose on Benadryl, and write a movie".  In a review of Kangass 2004 feature Fear of Clowns, DVD Talks Scott Weinberg dismissed Hunting Humans as something "you neednt rent anytime soon". 

== References ==

 

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 