Romance of Astree and Celadon
Romance of Astrea and Celadon ( ) is the final film directed by Éric Rohmer. It was released September 5, 2007.
* Director of Photography Diane Baratier
* Edited by Mary Stephen

== Cast ==
* Stéphanie de Crayencour (Astrée)
* Andy Gillet (Céladon)
* Cécile Cassel
* Jocelyn Quivrin

== Synopsis == Auvergne at Les Gorges de la Sioulle in May 2006.

== Awards ==
The film won a commendation in the Best Unreleased Film category (unreleased in Australia at the time of the awards) of the Australian Film Critics Association awards for 2008.

== External links ==
*  
*  
*   on European-films.net

 

 
 
 
 


 