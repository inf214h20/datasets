The Miller's Daughter (1905 film)
 
 
The Millers Daughter is a 1905 American silent film produced by Edison Manufacturing Company. Edison employees Wallace McCutcheon and Edwin S. Porter are generally credited as directors. Niver, Kemp (1967). Motion Pictures From The Library of Congress Paper Print Collection 1894-1912. University of California Press, ISBN 978-0520009479  The film is based on the melodrama Hazel Kirke by Steele MacKaye.

==Plot==
In the original play, Hazel Kirke, daughter of miller Dunstan Kirke, is expected to marry Aaron Rodney because he rescued their mill from insolvency. She instead marries Arthur Carrington, Lord Travers against her fathers wishes. Carringtons mother also disapproves, and tells Hazel the marriage is illegal because her son is already married. Despondent, Hazel tries to drown herself, but Carrington rescues her. Bordman, Gerald; Hischak, Thomas S. (2004). The Oxford Companion to American Theatre. Oxford University Press, ISBN 9780195169867 

In Porters adaptation, Carringford is a sophisticated urbanite and Rodney is a simple farmer. Both court Hazel, but she plans to marry Carringford against her fathers wishes. On the wedding day, Carringtons real wife appears with proof they are married. Hazel is disowned and goes to live in the city slums. She tries to make a living sewing, but her sewing machine is repossessed. She returns home to beg forgiveness but is again rejected by her father. She plans to commit suicide in the millrace, but Rodney saves her. After she marries and has a baby, she goes to her father with his new grandchild, and he forgives her.

Film historian Charles Musser writes of Porters adaptation, "  must learn the role of the dutiful daughter, wife, and mother. The father assumes a godlike role. The change in title from the womans name to her designated relationship to her father corresponds to this essential repositioning." Musser, Charles (1991). Before the Nickelodeon: Edwin S. Porter and the Edison Manufacturing Company. University of California Press, ISBN 9780520060807 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 