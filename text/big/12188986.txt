The Capture of Bigfoot
 

{{Infobox film
| name = The Capture of Bigfoot
| image = TromaBigfoot.jpg
| caption = VHS cover of The Capture of Bigfoot
| director = Bill Rebane
| producer =  
| writer =  
| starring =  
| music =
| cinematography =  
| editing =  
| studio = Studio Film Corp.
| distributor = Troma Entertainment
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget =
}}

The Capture of Bigfoot (aka The Legend of Bigfoot) is a 1979 horror film from Bill Rebane, the director of Monster A-Go-Go.    Produced and originally released by Studio Film Corp, the film was re-released in 2010 by Troma Entertainment.      

==Plot==
 
The creature known as Bigfoot has managed to elude capture for more than 25 years and a small town has made a cottage industry out of local Bigfoot sightings and merchandising.  What a businessman decides to trap Bigfoot once and for all so that he can benefit, the town may ultimately lose the tourist profits that have filled the towns coffers.

==Cast==
 
* Janus Raudkivi as The Legendary Creature of Arak
* Randolph Rebane as Little Bigfoot
* Otis Young as Jason
* George Buck Flower as Jake
* William Dexter as Hank
* Jeana Keough as Dancer
* Stafford Morgan as Garrett
* Katherine Hopkins as Karen
* Richard Kennedy as Olsen
* John F. Goff as Burt
* John Eimerman as Jimmy
* Randolph Scott as Randy
* Wally Flaherty as Sheriff Cooper
* Durwood McDonald as John
* Harry Youstos as Harry
* Verkina Flower as Linda
* Greg Gault as Kevin
* Nelson C. Sheppo as Daniels
* Mitzi Kress as Elsie
* Woody Jarvis as Woody
* William D. Cannon as Carlsen
 

==Reception==
 
 
In his book  , also directed by Bill Rebane).  Shot in Gleason, Wisconsin, the films closing credits attribute "wardrobe" to Kmart.

==References==
 

== External links ==
*  
*   at Rotten Tomatoes
*  

 

 
 
 
 
 
 