Avec mon mari
{{Infobox film
| name           = Avec mon mari
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kentarō Ōtani
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Yuka Itaya Hirofumi Kobayashi Kaori Tsuji Ren Ōsugi Gō Inoue Mayumi Terashima
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 95 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 romance comedy film directed by Kentarō Ōtani and starring Yuka Itaya, Hirofumi Kobayashi and Kaori Tsuji. It was released on 6 March 1999. 

==Cast==
*Yuka Itaya
*Hirofumi Kobayashi
*Kaori Tsuji
*Ren Ōsugi
*Gō Inoue
*Mayumi Terashima

==Reception==
It was chosen as the 9th best film at the 21st Yokohama Film Festival. 
{| class="wikitable sortable" width="90%"
|- style="background:#ccc; text-align:center;"
! Award
! Date
! Category
! Recipients and  nominees
! Result
|- Yokohama Film Festival 21st Yokohama 2000 
| Best New Director
| Kentarō Ōtani
|  
|-
| Best Newcomer
| Yuka Itaya
|  
|}

==References==
 

==External links==
* 

 

 
 
 
 


 
 