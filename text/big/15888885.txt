Heartworn Highways
{{Infobox Film
| name           = Heartworn Highways
| caption        = 
| image	=	Heartworn Highways FilmPoster.jpeg
| director       = James Szalapski
| producer       = Graham Leader
| writer         = James Szalapski Steve Young The Charlie Daniels Band Larry Jon Wilson
| music          = 
| cinematography = James Szalapski
| editing        = Phillip Schopper Warner Bros. Domestic Cable Distribution
| released       = May 13, 1981
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| followed_by    = 
}}
Heartworn Highways is a documentary film by James Szalapski whose vision captured some of the founders of the Outlaw Country movement in Texas and Tennessee in the last weeks of 1975 and the first weeks of 1976. AllMovie entry for  .  The film was not released theatrically until 1981. 

==Plot==
 Steve Young, and The Charlie Daniels Band. The movie features the first known recordings of Grammy award winners Steve Earle and Rodney Crowell who were quite young at the time and appear to be students of mentor Guy Clark. Steve Earle was also a big fan of Van Zandt at the time. 

The beginning of the movie shows Larry Jon Wilson in a recording studio shortly after he had been woken up for the movie after having been partying all night after a gig into the morning. The film maker goes to Austin and visits Townes Van Zandt at his trailer (At what is now 14th and Charlotte in the Clarksville neighborhood of downtown Austin) and his girlfriend Cindy, his dog Geraldine, Rex "Wrecks" Bell, and Uncle Seymour Washington (born 1896; died 1977) at his place, who is also called "The Walking Blacksmith", and who gives his great worldly advice to the viewers and represents a very important aspect of the atmosphere that these songwriters living in the South are surrounded by and involved in. 

The movie shows Charlie Daniels completely fill a big high school gymnasium. Then the camera man, sound recorder and director join David Allan Coe and film him playing a gig at the Tennessee State Prison where he admits to being a former inmate and tells a story of being there and seems to bring out friends of his onto the stage who still are inmates there and they perform a gospel number "Thank You Jesus" that they used to sing in the yard. The end of the movie shows a drinking party that starts Christmas Eve and ends sometime Christmas Day at Guy Clarks house in Nashville with Guy, Susanna Clark, Steve Young, Rodney Crowell, Steve Earle, Jim McGuire (playing the dobro), along with several other guests. Steve Young leads the group in a rendition of Hank Williams song "Im So Lonesome I Could Cry" and Rodney Crowell leads everyone in "Silent Night".

==Music==

A Complete List of Songs Performed in Movie:

* Guy Clark - "L.A. Freeway"
* Larry Jon Wilson - "Ohoopee River Bottomland"
* David Allan Coe - "Keep on Trucking"
* Big Mack McGowan & Glenn Stagner "The Doctors Blues"
* Guy Clark - "That Old Time Feeling"
* Townes Van Zandt - "Waitin Around to Die"
* David Allan Coe - "I Still Sing the Old Songs"
* Barefoot Jerry - "Two Mile Pike"
* Rodney Crowell - "Bluebird Wine"
* Steve Young (musician)| Steve Young - "Alabama Highway"
* Guy Clark - "Texas Cooking"
* Gamble Rogers - "Black Label Blues"
* Peggy Brooks - "Lets Go All the Way"
* The Charlie Daniels Band - "Texas"
* David Allan Coe - "Penitentiary Blues"
* David Allan Coe - "River"
* Steve Earle - "Elijahs Church" (partial)
* Ensemble - "Silent Night"
       
"Extras" Bonus Songs on DVD

* Guy Clark - "Desperadoes Waiting For a Train"
* Townes Van Zandt - "Pancho & Lefty"
* Richard Dobson - "Hard by the Highway"
* The Charlie Daniels Band - "Long Haired Country Boy"
* Guy Clark w/ Rodney Crowell - "Ballad of Laverne and Captain Flint"
* John Hiatt - "One For The One For Me"
* Steve Earle - "Darlin Commit Me"
* David Allan Coe - "Thank You, Jesus"
Party at Guy Clarks House
* Steve Earle - "Mercenary Song"
* Rodney Crowell - "Young Girls Hungry Smile"
* Richard Dobson - "Forever, For Always, For Certain"
* Billy Callery - "Question" 
* Steve Young - "Im So Lonesome I Could Cry"
* Steve Earle & Rodney Crowell - "Stay a Little Longer"
* Guy Clark - "Country Morning Music"

==References==
 

==External links==
* 
*  
* 

 
 
 
 