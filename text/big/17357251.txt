Shool
 
 
 Shool, Iran}}
{{Infobox film Shool
| image          = Shool 1999 - DVD Cover.jpg
| director       = E. Nivas
| producer       = Ram Gopal Varma Nitin Manmohan
| screenplay     = Ram Gopal Varma
| story          = Ram Gopal Varma E. Nivas
| writer         = Ram Gopal Varma E. Nivas Anurag Kashyap (dialogue)
| starring       = Manoj Bajpai Sayaji Shinde Raveena Tandon Sameer
| cinematography = Hari Nair
| editing        = Bhanodaya
| released       = 5 November 1999
| runtime        = 135 min
| country        = India
| language       = Hindi
}}
 Hindi crime drama film Hindi film|  written by Ram Gopal Varma and directed by E. Nivas. The film portrays the politician-criminal nexus and the criminalisation of politics in the state of Bihar, and its effect on the life of an honest police officer. The film starred Manoj Bajpai as Inspector Samar Pratap Singh and Sayaji Shinde as the borderline psychopath criminal-politician Bachhu Yadav. The film won the National Film Award for Best Feature Film in Hindi.

== Plot == the left, telling him jokingly that heart is on the left by reciting a famous Bollywood lyric.

Meanwhile, Inspector Samar Pratap Singh (Bajpai) arrives at Motihari, where he has been transferred, with his wife (Raveena Tandon) and daughter. At the railway station he confronts a coolie (Rajpal Yadav). The two have a tiff on Rs 30/- to be paid to the latter for which Singh refuses to pay, as he (rightfully) thinks he is being over-charged. As the situation goes to the verge of fisticuffs, a local police hawaldar intervenes. Not knowing that Singh too is a police officer,the hawaldar tries to manhandle Singh. Infuriated, Singh takes the matter to the police station to which he is posted. As Singh writes a complaint against the hawaldar for harassing a local (Singh), another inspector Hussain intervenes. Hussain asks for forgiving hawaldar, for which Singh doesnt relent. Singh later learns that the Motihari police station is ruled by local politician and criminal Bachhu Yadav and his henchmen. Singh is an idealist who respects the constitution and the law, and expects that everyone else should do the same. But no one follows the law in Motihari, especially the policemen who receive hafta (illicit weekly payments) from Yadav to do his bidding.

One day, the Deputy Superintendent of Police (Shrivallabh Vyas) asks Singh to break up a fight between two rival gangs, and arrest the people who attacked some of Yadavs men. Singh investigates and finds that Yadavs men were the real culprits. Among them were Sudhir Vinod (Nagesh Bhosle) and Lallan singh (Yashpal Sharma), and so Singh arrests them instead. When the D.S.P. orders him to release them, he refuses to do so, saying that he has already registered the case. This is the first time his superior learns of his real character and expresses concern over his future. Sub Inspector Hussain, who shamelessly admits subjection to Yadav, declares that Singh wont last long. Singh sadly learns the limit of his official prowess when the court releases Yadavs men (the men who were beaten up refuse to testify, understandably out of fear of Yadavs wrath).
 jai jawan, jai kisan".

Things begin worsening and Singh soon finds himself alone in his fight against a corrupt and rotten system. One day while buying vegetables in the local market, he sees three young men sitting on a wall teasing passing girls by singing lewd bhojpuri songs; when he confronts them they boldly react (he is in plainclothes), but on learning that he is the S.H.O., the two become defensive and tell him meekly that they are students of a certain college, but the third declares proudly that he is younger brother of an influential politician (not Yadav), hoping that Singh will get impressed, but Singh answers with a slap and forces him to apologise to the girl.

On his way home, Yadav decides to give interview to a female journalist who boldly asks him if he is a murderer. Yadav, understandably, gets annoyed and tries to confuse and terrify her, failing to do so he simply asks her to get out of his car.

Yadav get irritated by Singh, particularity because he arrested his men. He decides to annoy him and organises his own marriage anniversary and arranges a folk dance by hiring a beautiful dancer (Shilpa Shetty) late into night. Singh goes over there and asks for permission that is required in India for operating loudspeakers; failing to see the same, Singh seizes music system. Yadav confronts him and asks to be forgiven (in a teasing and satirical way). The D.S.P. is also present there (dead drunk), who tries to cool down Singh by making him aware that no one gives a damn to such "small rules" in a small town. Singh refuses to yield, which angers the D.S.P. to shout orders at him. Singh retains his stand by stating that he be given written orders. This gesture of rebellion costs Singh his job as next day, with the help of the corrupt sub-inspector Hussain, the D.S.P. falsely sues Singh for physical attack on his senior. Tiwari (Vineet Kumar) tries to help Singh but in vain. Singh gets suspended.

But real troubles still await Singh. One day he takes his daughter to a sweet-shop where they accidentally come across Yadavs goons seated at a table. They start passing disgustingly indecent comments that cause Singh to lose his temper and start beating them. One of the henchman comes with a heavy wooden club and starts beating Singh but misses him, and instead, hits the small girls head, killing her.

When badly injured Lallji goes in front of Yadav and tell him that Singh has beaten him badly, Yadav, who actually cares next to nothing even about his most loyal men, find it a golden opportunity to accuse Singh. He immediately takes a shot-gun from the wall and hits Lallji in head forcefully enough to kill him, and then orders his sidekicks to make complaint that Lallji actually died because of the beating by Singh, and puts the whole blame, in fact a murder-case, on Singh. The police waste no time and arrest Singh while he is still grieving over his daughters dead body. Singhs parents come to help him, and his father (Virendra Saxena) pleads with Yadav to get him released. When Singh realises that Yadav was behind his release, he insults Yadav.

A few days later, Singhs parents leave, and he has a big fight with his wife over their situation and her accusation that his idealism was to blame. He leaves in anger, and his wife consumes poison. His only friend in town, Tiwari, informs him about it and both rush to the hospital. Singh manages to speak a couple of sentences with her before she dies.
 criminalisation of politics that is rotting the entire system, he kills Yadav and yells "Jai Hind" twice. The film ends.

== Cast  ==
* Manoj Bajpai ... Samar Pratap Singh
* Raveena Tandon ... Manjari Singh
* Sayaji Shinde ... Bachhu Yadav
* Vineet Kumar ... Inspector Tiwari
* Ganesh Yadav ... Inspector Hussain
* Virendra Saxena ... Prabal Pratap Singh
* Nandu Madhav ... Laljee
* Sandeep Kulkarni ... Gopaljee
* Nagesh Bhosle ... Sudhir Vinod Yashpal Sharma ... Lallan Singh
* Shri Vallabh Vyas ... D.S.P
* Rajpal Yadav ... Coolie
* Shilpa Shetty ... Item Girl in a Song
* Pratima Kazmi ... Bhachhu Yadavs Mother
* Nawazuddin Siddiqui ... Waiter

== Awards ==
* National Film Award for Best Feature Film in Hindi, 2000. 
* Filmfare Critics Award for Best Performance, 2000 – Manoj Bajpai.
* Star Screen Award for Best Villain, 2000 – Sayaji Shinde.

==Soundtrack==
{{Infobox album
| Name = Shool
| Type = soundtrack
| Artist = Shankar-Ehsaan-Loy
| Cover =
| Released =   1999 (India)
| Recorded = Feature film soundtrack
| Length = Tips
| Producer = Ram Gopal Varma
| Reviews =
| Last album = Dus (1997 film)|Dus (1997)
| This album = Shool (1999)
| Next album = Rockford (film)|Rockford (1999)
}}
{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! Track # !! Song !! Singer(s)
|- Aaya Mere Kavita Krishnamurthy, Shankar Mahadevan, Baby Anagha 
|- Main Aayi Sapna Awasthi, Chetan Shashitaal 
|- Main Aayi Sapna Awasthi, Chetan Shashitaal 
|- Shankar Mahadevan
|- Shool Si Sukhwinder Singh
|- Aaj Raat Aayega Mazaa||K.S. Chitra
|-
|}
 Ehsaan of Shankar-Ehsaan-Loy remarked that the song "was something that contracted us as people" 

== References ==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 