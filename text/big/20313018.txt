Unthinkable
 
 
{{Infobox film
| name           = Unthinkable
| image          = unthinkable.jpg
| caption        = Original theatrical release poster
| image_size     = 250px
| director       = Gregor Jordan
| producer       = Marco Weber Caldecot Chubb Vanessa Coifman Bill Perkins
| writer         = Oren Moverman Peter Woodward
| starring       = Samuel L. Jackson Michael Sheen Carrie-Anne Moss Brandon Routh Gil Bellows Martin Donovan Stephen Root
| music          = Graeme Revell
| cinematography = Oliver Stapleton
| editing        = Scott Chestnut
| studio         = Lleju Productions Sidney Kimmel Entertainment Kimmel International ChubbCo Film Senator Entertainment Co.
| distributor    = Sony Pictures Home Entertainment Senator U.S.
| released       =  
| runtime        = 97 min.
| country        = United States
| language       = English
| budget         = $15,000,000
| gross          = $5,483,534 
}} suspense thriller film directed by Gregor Jordan and starring Samuel L. Jackson, Michael Sheen and Carrie-Anne Moss. It was released direct-to-video on June 14, 2010.  The film is noteworthy for the controversy it generated around its subject matter, the torture of a man who threatens to detonate three nuclear bombs in separate U.S. cities. 

== Plot == FBI Special nuclear bombs in separate U.S. cities if his demands are not met.
 puppet governments" dictatorships in Muslim countries and a withdrawal of American troops from all Muslim countries. The group immediately dismisses the possibility of his demands being met, citing the United States declared policy of not negotiating with terrorists.

When Brody accuses Yusuf of faking the bomb threat in order to make a point about the moral character of the United States government, he breaks down and agrees that it was all a ruse. He gives her an address to prove it. They find a room that matches the scene in the video tape and find evidence on the roof. A soldier removes a picture from an electrical switch which triggers a tremendous C-4 (explosive)|C-4 explosion at a nearby shopping mall visible from the roof. The explosion kills 53 people. Angry at the senseless deaths, Brody returns to Yusuf and cuts his chest with a scalpel. Yusuf is unafraid and demands she cut him. He justifies the deaths in the shopping mall, stating that the Americans kill that many people every day. Yusuf says he allowed himself to be caught so he could face his oppressors.
 mutilate her bleeds to New York, Los Angeles and Dallas), but H does not stop, forcing the others to intervene. Citing the amount of missing nuclear material Yusuf potentially had at his disposal (some 15–18&nbsp;lbs. were reported missing, with about 4½ lbs. needed per device), H insists that Yusuf has not admitted anything about a hence-unreferenced fourth bomb. H points out that everything Yusuf has done so far has been planned meticulously. He knew the torture would most likely break him, and he would have been certain to plant a fourth bomb, just in case.

Here it becomes clear that the purpose of the preceding torture was not to break Yusuf, but rather to make it clear to him what would happen to his children if he did not cooperate. The official in charge of the operation demands that H bring Yusufs children back in for further interrogation. H demands that Brody bring the children back in, because her decency will give him the moral approval that he needs to do the "unthinkable". When Brody refuses to retrieve the children for H, he unstraps Yusuf, sarcastically setting him free. The official draws his pistol and aims it at H to coerce him into further interrogation. Yusuf grabs the officials gun. He asks Brody to take care of his children and kills himself. Brody walks out of the building with Yusufs children.

=== Extended version ===
An FBI bomb disposal team arrives at one of the disclosed locations and resets the timer, to prevent the bomb from going off. As the FBI are celebrating however, behind a nearby crate, the originally unconfirmed fourth bombs timer counts down to zero. The screen immediately cuts to black and the credits roll.

== Cast ==
* Samuel L. Jackson - Henry Harold Humphries aka H, a black-ops officer assigned to interrogate Yusuf
* Michael Sheen - Yusuf Atta Mohammed / Steven Arthur Younger, an American Muslim, delta force operator, and bomb expert who has planted nuclear bombs in three U.S. cities
* Carrie-Anne Moss - Special Agent Helen Brody, the leader of an FBI counter-terrorism team assigned to interrogate Yusuf
* Brandon Routh - Special Agent DJ Jackson, a member of the FBI team Benito Martinez - Alvarez, assistant of H
* Gil Bellows - Special Agent Vincent, a member of the FBI team
* Joshua Harto - Special Agent Phillips
* Martin Donovan - Assistant Director in Charge Jack Saunders, FBI Los Angeles Division director who assigns the interrogation
* Stephen Root - Charles Thomson
* Necar Zadegan - Jehan Younger, Yusufs wife Michael Rose - Colonel Kerkmejian
* Holmes Osborne - General Paulson, the General leading the interrogation

== Reception ==
Policy adviser  Charles V. Peña opines that "Ultimately,   is about the age-old question, Do the ends justify the means?... In the end, Unthinkable doesn’t answer the question... but does provide plenty of food for thought".  Despite praising its dramatic value, film scholar Matthew Alford argues that "the aesthetic realism and apparent seriousness of Unthinkable is a mask for the absurdity of its content and reactionary politics" making it more than just a "nightmare scenario" but rather "a white paper from Freddy Krueger".  A similar point is raised by DVD columnist Sean Axmaker who calls the film "a clumsy polemic that bounces between the boundaries of stage-play debate and torture porn spectacle". 

Indian critic Rohit Rao recommends the movie with 3 and half stars, referring to Samuel L. Jacksons "powerhouse performance" in playing H as "brutally direct and mysterious at the same time". 

== References ==
 

== External links ==
 
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 