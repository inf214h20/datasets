Gloria (1980 film)
{{Infobox film
| name = Gloria
| image = gloria_1980_movie_poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = John Cassavetes
| producer = Sam Shaw
| writer = John Cassavetes
| starring = Gena Rowlands Julie Carmen Buck Henry John Adames
| music = Bill Conti
| cinematography = Fred Schuler
| editing = George C. Villaseñor
| distributor = Columbia Pictures
| released =  
| runtime = 121 minutes  
| country = United States
| language = English
}} crime Thriller thriller film written and directed by John Cassavetes. It tells the story of a Gun moll|gangsters girlfriend who goes on the run with a young boy who is being hunted by the mob for information he may or may not have. It stars Gena Rowlands, Julie Carmen, Buck Henry, and John Adames.

==Plot==
 
In the South Bronx, Jeri Dawn is heading home on the bus with bags of groceries. She gets of at her stop and accidentally drops all her bags. After picking them up, she heads to an apartment building. Once inside the lobby, she passes a man whose dress and appearance are out of place. The woman quickly boards the elevator and anxiously waits for it to reach her floor, where she then gets off and heads to a room far from the elevator.

She is met by her husband Jack Dawn, an accountant for a New York City mob family. There is a contract on Jack and his family, as he has been acting as an informant for the FBI. Suddenly, the familys neighbor, Gloria Swenson, rings their doorbell saying shes out of coffee. Jeri tells Gloria of the impending hit and implores Gloria to protect the children. Gloria, a former mobsters girlfriend, tells Jeri that she doesnt like kids but begrudgingly agrees. The Dawns daughter Carmen refuses to leave and locks herself in the bathroom, so Gloria takes only their young son Phil to her apartment – just narrowly missing the hit squad.

After hearing loud explosions from the Dawns apartment, a visibly shaken Gloria decides she and Phil must go into hiding. She quickly packs a bag, grabs her cat, and escapes the building with Phil, just as a police SWAT team are entering with heavy weapons. Meanwhile, a crowd of onlookers and news reporters has gathered in front of the building, and one cameraman captures a picture of Gloria leaving the building with Phil.

Gloria and Phil take a cab into Manhattan and hide out in an empty apartment belonging to a friend of hers. While Phil sleeps, Gloria has the TV on and hears a news report say that there was a mob hit in the South Bronx and that the name of the suspected abductor is Gloria Swenson.

The next morning, Gloria and Phil sneak out of the apartment just as a group of gangsters close in on them. The gangsters are old friends of Gloria and confront Gloria on the sidewalk outside, exhorting her to give up Phil and the ledger. In desperation, Gloria empties her revolver at the car of five gangsters, which takes off and flips over. As a result of her actions Gloria realizes that the fates of both her and Phil are now intertwined and that theyll have to escape from New York in order to survive.

Gloria goes to the bank to empty her safe deposit box and the two settle for the night at a flophouse. She confronts another group of gangsters at a restaurant, she asks for immunity in exchange for the ledger. "Only Mr. Tanzinni can agree to that," says one of the goons, so she takes some of their guns and flees.

The next day, Gloria tells Phil that she plans to send him away to a boarding school. Offended by her intentions Phil claims he is an independent grown man who can manage alone and Gloria decides to abandon him, and have a drink; but she is soon filled with guilt and rushes back to look for him; however he is captured by some wise-guys and Gloria is forced to rescue him, killing one thug in the process and then fleeing 2 other thugs in a taxi and the subway, where several by-standers help her escape from the two mobsters.

The two eventually make it to a hotel room, where Gloria laments the mafias strength and ubiquitous presence, explaining to Phil that she was once the mistress of Tanzinni himself. Thus she meets with her ex-lover, relinquishes the ledger, and then flees killing one gangster, as another shoots down upon her elevator car. Phil waits the three and a half hours, then flees to Pittsburgh via rail. At a cemetery Phil and Gloria, disguised as an old woman, reunite.

==Cast==
* Gena Rowlands as Gloria Swenson
* Julie Carmen as Jeri Dawn
* Buck Henry as Jack Dawn
* John Adames as Phil Dawn
* Lupe Garnica as Margarita Vargas
* John Finnegan as Frank
* Tom Noonan, J.C. Quinn, and Sonny Landham as Mob henchmen
* Lawrence Tierney as Broadway bartender

==Production==
John Cassavetes did not originally intend to direct his screenplay; he planned merely to sell the story to Columbia Pictures. However, once his wife, Gena Rowlands, was asked to play the title character in the film, she asked Cassavetes to direct it.

==Awards== Atlantic City. The Jazz Singer) for the Worst Supporting Actor Razzie award of 1980.

==Remakes and influences== under the same title with a screenplay by Steve Antin and directed by Sidney Lumet. It starred Sharon Stone and Jean-Luke Figueroa.

Other films inspired by Gloria include Ultraviolet (film)|Ultraviolet (2006), which uses the premise of a woman on the run with a little boy and transposes the story to a Dystopian futuristic setting,  and Erick Zoncas 2008 film Julia (2008 film)|Julia, starring Tilda Swinton.    Luc Bessons film Léon (film)|Léon also was inspired by Gloria, with actor Jean Reno playing the accidental guardian of a young girl (Natalie Portman) whose family was murdered by a corrupt DEA agent (Gary Oldman).  A 2009 Brazilian film titled Verônica has a similar plot, changing the main character from a gangsters girlfriend to a teacher, who tries to save a student from criminals who killed his parents and are now chasing after him.

==References==
 
* Cassavetes, John and Raymond Carney (2001). "Chpt 10: Gloria (1978-1980)" in  . Macmillan. ISBN 0-571-20157-1.
* Morris, George (1980). " ," Texas Monthly. Vol. 8, No. 10. ISSN 0148-7736.

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 