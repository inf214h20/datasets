U Turn (1997 film)
{{Infobox film
| name = U Turn
| image = U-Turnposter.jpg
| caption = Theatrical release poster
| director = Oliver Stone
| producer = Dan Halsted Clayton Townsend
| writer = John Ridley (book and screenplay) Oliver Stone
| starring = Sean Penn Nick Nolte Jennifer Lopez Joaquin Phoenix Powers Boothe Billy Bob Thornton Claire Danes Jon Voight
| music = Ennio Morricone Robert Richardson
| editing = Hank Corwin Thomas J. Nordberg
| studio = Phoenix Pictures
| distributor = TriStar Pictures  (Sony Pictures Entertainment) 
| released = August 27, 1997 (Telluride Film Festival) October 3, 1997 (U.S. release)
| runtime = 125 minutes
| country = France United States
| language = English
| budget = $19,000,000 (estimated)
| gross = $6,633,400 (USA)
}} romantic comedy crime thriller film directed by Oliver Stone, based on the book Stray Dogs by John Ridley. It stars Sean Penn, Billy Bob Thornton, Jennifer Lopez, Jon Voight, Powers Boothe, Joaquin Phoenix, Claire Danes and Nick Nolte.

==Plot==
A drifter named Bobby (Sean Penn) is on his way to pay off a large debt of $13,000 to a gangster in California when his car breaks down, forcing him to drive to the nearest town, Superior, Arizona. He takes the money with him but leaves his gun in the trunk of his car. While waiting for his car to be fixed by the town mechanic, Darrell (Billy Bob Thornton), he wanders around town where he meets Grace McKenna (Jennifer Lopez). Not realizing she is married, he hits on her and helps her carry some drapes to her car. She offers to take him back to her house where he can have a shower.

While in the shower, it is revealed that the "accident" that happened to his hand was in fact a punishment for the overdue debt – two of his fingers were cut off. After his shower he attempts to seduce Grace, who is cold to his advances. He goes to leave, saying he isnt interested in playing games. The two then kiss, and are caught by Graces husband Jake (Nick Nolte), who punches Bobby.

As Bobby is walking back to town Jake pulls up beside him and offers him a lift. After a casual conversation about Grace, Jake asks Bobby if he would kill her for a price. Bobby laughs this off and asks Jake if he is just trying to "rattle" him.

Later on when Bobby is in a convenience store, the store is held up. The robbers take his bag with all his money in it but the shop owner takes out a shotgun and kills them both, destroying most of the money in the process. Broke and stranded, Bobby frantically calls nearly everyone he knows trying to get money to fix his car. He even calls the gangster he owes money to asking him for money but the gangster angrily refuses. The gangster now knows where Bobby is and sends someone after him.

When Bobby is sitting in a diner ordering a beer, he is approached by Jenny (Claire Danes), which provokes the jealous rage of her boyfriend, Toby N. Tucker (Joaquin Phoenix). The resulting fight is stopped in time by the town Sheriff (Powers Boothe).

Bobby also goes back to the mechanic, where he finds that Darrell has done additional work and is asking a higher price. He also busted open the trunk of the car, meaning Bobby cannot access his gun. A confrontation breaks out which results in Darrell scratching up the hood of Bobbys car. Darrell says that he will continue working on the car and charge more and more for the work until Bobby has the means to pay him.
 sexually abused her from a young age and then married her after her mother died. Her mother was found dead at the bottom of a cliff but her death was ruled a suicide; her death is eerily similar to how Jake told Bobby to kill Grace. Grace then asks Bobby if he would kill Jake, meaning the two of them could steal his money. Bobby initially refuses.

Still broke and aware that the gangsters will send someone to find him, Bobby attempts to buy a ticket out of town. Although he doesnt have enough money, the clerk gives him the ticket anyway after he becomes aggressive and hostile. After buying the ticket he sees one of the gangsters driving towards him. However, the Sheriff stops the gangster and arrests him for concealing a gun. Thinking he is safe, Bobby is then attacked by Toby, who takes Bobbys ticket, rips it up and eats it. Bobby loses his temper and beats Toby to a pulp.
 tomahawk and apparently attacks Bobby when he walks into the room. However, she is really waiting for Jake to return so that she can kill him. Jake enters the room and finds Bobby lying on the floor. Bobby is playing dead and he attacks Jake with a golf club and they both fight. Grace then hits Jake in the back with the tomahawk and during a struggle between Bobby and Jake, Grace hits Jake in the chest with the tomakawk. Bobby grabs the tomahawk and kills Jake by striking him in the chest. Bobby and Grace unlock the safe to find $200,000. They then have sex in front of Jakes dead body.

Afterwards, Bobby goes to Darrell to get his car back. When he returns, Graces car is gone. Bobby thinks she has run off with the money but she merely hid the car in the garage. As they drive off however, they are pulled over by the Sheriff., with whom Grace has been having an affair. Grace appears to turn on Bobby, blaming him for Jakes death. However, she shoots the sheriff and puts him in the trunk with Jake.
 radiator hose to burst.

==Cast==
* Sean Penn as Bobby Cooper
* Jennifer Lopez as Grace McKenna
* Nick Nolte as Jake McKenna
* Powers Boothe as Sheriff Virgil Potter
* Claire Danes as Jenny
* Joaquin Phoenix as Toby N. Tucker a.k.a. TNT
* Jon Voight as Blind Indian
* Billy Bob Thornton as Darrell
* Abraham Benrubi as Biker #1
* Richard Rutowski as Biker #2
* Aida Linares as Jamilla
* Sean Stone as Boy in Grocery Store
* Ilia Volokh as Sergei
* Valeri Nikolayev as Mr. Arkady
* Brent Briscoe as Boyd
* Bo Hopkins as Ed
* Julie Hagerty as Flo
* Liv Tyler as Girl in Bus Station "cameo appearance"
* Laurie Metcalf as Bus Station ticket attendant "cameo appearance"

==Production==
U Turn was filmed in 1996 on location in  )  It was filmed entirely on Reversal stock, 5239, to give an extra harsh look to the hostile environment. 

==Reception==
===Critical response===
Reaction towards the movie was mostly mixed by critics. Roger Ebert gave the film 1½ stars out of four, deeming it a "repetitive, pointless exercise in genre filmmaking—the kind of film where you distract yourself by making a list of the sources".  Mick LaSalle of the San Francisco Chronicle wrote that it "demonstrates a filmmaker in complete command of his craft and with little control over his impulses".  U Turn currently holds a 60% rating on Rotten Tomatoes based on 50 reviews.
 The Postman) Worst Supporting Most Wanted; Double Team).

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 