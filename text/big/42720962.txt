R100 (film)
 
{{Infobox film
| name           = R100
| image          = R100 poster.jpg
| alt            = 
| caption        = Film poster
| director       = Hitoshi Matsumoto
| producer       = Keisuke Konishi Natsue Takemoto
| screenplay     = Hitoshi Matsumoto Mitsuyoshi Takasu Tomoji Hasegawa Kôji Ema Mitsuru Kuramoto	
| starring       = Nao Ōmori Shinobu Terajima Hitoshi Matsumoto Ai Tominaga Eriko Sato
| music          = Shûichi Sakamoto Shûichirô Toki
| cinematography = Kazunari Tanaka
| editing        = Yoshitaka Honda Yoshimoto Kogyo Company Drafthouse Films (US)  Warner Bros. (Japan) 
| released       =  
| runtime        = 94 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} dramedy film directed by Hitoshi Matsumoto.   The film had its world premiere at 2013 Toronto International Film Festival on September 12, 2013.  

==Plot==
Ordinary businessman Takafumi Katayama signs a contract to join a mysterious BDSM club where various dominatrixes, each with their own specialty skill, will attack and humiliate him in public. The contract lasts for one year and no cancellation is allowed. At first, Takafumi is greatly pleased by his membership, but when the clubs activities start to intrude into his home life, Takafumi must find a way to protect his family and himself from more than just humiliation.

==Cast==
*Nao Ōmori as Takafumi Katayama
*Shinobu Terajima as Whip Queen
*Hitoshi Matsumoto
*Ai Tominaga
*Eriko Sato
*Naomi Watanabe as Saliva Queen You
*Suzuki Matsuo
*Atsuro Watabe
*Gin Maeda
*Katagiri Hairi as Gobble Queen
*Lindsay Kay Hayward as CEO
*Mao Daichi as Voice Queen

==Reception==
R100 received positive reviews from critics. Review aggregator Rotten Tomatoes reports that 81% of 21 film critics have given the film a positive review, with a rating average of 6.7 out of 10. 

Rob Nelson of Variety (magazine)|Variety, said in his review that "If a dominatrix is one who takes total control of her passive partner, then "R100" is the cinematic equivalent of a kinky femme fatale in black leather and stiletto heels, cracking a whip and a smile."  Deborah Young in her review for The Hollywood Reporter praised the film by saying that "It’s hard to remember a film about S&M as funny as this one, or one as beautifully and weirdly imagined."  Colin Covert of Minneapolis Star Tribune gave the film three stars by saying that ""To call this Midnight Movie entry "not for everyone" is understating it. But connoisseurs of weird, twisted sex comedy will revel in its transgressive, audacious mischief."  Katie Rife of The A.V. Club gave the film a B+.   

==Release==
The film premiered on September 12, 2013 on the Toronto International Film Festival  and was released on March 3, 2015 on Blu-ray Disc in a 1080p video format  and DVD over Drafthouse Films. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 