Story of the Vulture Conqueror
 
 
{{Infobox film name = Story of the Vulture Conqueror film name = {{Infobox name module traditional = 射鵰英雄傳 simplified = 射雕英雄传
}} image = Story of the Vulture Conqueror newspaper ad.jpg image_size =  caption = 1958 newspaper ad for Part 1 director = Wu Pang producer = story = Louis Cha screenplay = Wu Pang Yue Fei Miu Ching narrator =  starring = Cho Tat-wah Yung Siu-yee music = cinematography = editing = studio = Emei Film Company distributor = released = 23 October 1958 (Part 1) 3 June 1959 (Part 2) runtime = country = Hong Kong language = Cantonese budget =  gross = preceded_by =  followed_by =
}} Louis Chas novel The Legend of the Condor Heroes. The first part was released in 1958 while the second part was released in the following year. The film was directed by Wu Pang and starred Cho Tat-wah and Yung Siu-yee in the leading roles.

==Cast==
: Note: Some of the characters names are in Cantonese romanisation.
 Kwok Ching Wong Yung / Fung Hang Yeung Hong Muk Nim-chi
* Lee Ching as Yeung Tit-sam
* Mui Yee as Pau Si-yeuk Chow Pak-tung
* Lee Heung-kam as Lee Ping Wong Yeuk-see Hung Tsat-kung
* Lam Lo-ngok as Ouyang Feng|Au-yeung Fung / Jebe
* Chan Lap-pan as Mui Chiu-fung
* Ho Siu-hung as Kau Chin-yan Wong Chung-yeung
* Wong Chor-san as Wong Chui-yat
* Siu Hon-sang as Or Chan-ngok
* Ng Yan-chi as Chu Chung
* Chan Yiu-lam as Hon Po-kui
* Chow Siu-loi as Nam Hei-yan
* Wu Ka as Hon Siu-ying
* Tsui Sung-hok as Chuen Kam-fat
* Tong Ka as Cheung Ah-sang
* Ho San as Yuen-ngan Hung-lit
* Yuen Siu-tien as Leung Tsi-yung
* Chu Chiu as Tuen Tin-tak
* Chan Kam-tong as Luk Sing-fung
* Mak Sin-shing as Luk Koon-ying

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 