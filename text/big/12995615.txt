The Zombie Diaries
{{Infobox film
| name = The Zombie Diaries
| image = Zombiediariesposter.jpg
| caption = Theatrical poster
| director = {{plainlist|
* Kevin Gates
* Michael Bartlett
}}
| producer = {{plainlist|
* Kevin Gates
* Michael Bartlett
}}
| writer = {{plainlist|
* Kevin Gates
* Michael Bartlett
}}
| starring = {{plainlist| Russell Jones
* Sophia Ellis James Fisher
}}
| music = Stephen Hoper
| cinematography = George Carpenter
| editing = {{plainlist|
* Kevin Gates
* Michael Bartlett
}}
| studio = {{plainlist|
* Off World Films
* Bleeding Edge Films
}}
| distributor = Revolver Entertainment
| released =  
| runtime = 81 minutes
| country = United Kingdom
| budget = £8,100 
| gross = $2 million   
}}
The Zombie Diaries is a 2006 British independent horror film written, produced and directed by Kevin Gates and first-time feature-filmmaker Michael Bartlett.
 documentary format on DV and split into three separate parts.

==Plot==
Soldiers of the British army clear a small collection of farmhouses with two scientists who take a tissue sample from a deceased civilian who appears to have been reanimated, then shot.

The films first chapter, "The Outbreak", begins in the city of London, emphasizing both the dismissive and paranoid reactions of the population to an unspecified disease outbreak that is gradually making its way to Britain. The film then moves to footage of a documentary crews travel to the countryside, where, in the process of filming material related to the virus, the characters encounter the zombie outbreak firsthand.  The story of these four individuals is revisited in the second half of the movie.
The second chapter, "The Scavengers", takes place one month later.  Two men (one of whom is American) and one woman travel around in a small car armed only with a rifle, in search of food and radio parts.

The final chapter, "The Survivors", tells the story of a larger group of uninfected people who have set up camp on a farm.  They divide their time doing reconnaissance of surrounding areas, holding off the endless stream of incoming zombies, and bickering amongst themselves.  In the opening scene of the chapter, the audience watches as "the survivors" calmly execute the approaching infected.  By the conclusion, in which the film goes back in time to the first nights of the documentary crew at the beginning of the first diary ("The Outbreak"), the word "survivors" becomes an ironic title, as all but one are killed by two uninfected psychopaths.

Both of the murderers also survive, one having disappeared after helping slaughter the film crew and the other kills off the "survivors".

==Cast==
Whilst the cast was made up of professional actors, few were well-known other than a brief appearance from Leonard Fenton, best known for playing Harold Legg|Dr. Harold Legg in EastEnders.

The principal cast consisted of: Russell Jones&nbsp;– Goke
* Craig Stovin&nbsp;– Andy
* Jonnie Hurn&nbsp;– John James Fisher&nbsp;– Geoff
* Anna Blades&nbsp;– Vanessa
* Imogen Church&nbsp;– Sue
* Sophia Ellis&nbsp;– Anna McKenzie
* Victoria Summer&nbsp;– Leeann

==Production==

The idea for the film was proposed by Michael Bartlett to Kevin Gates in Autumn 2004. Bartlett originally envisaged it as a short film, but Gates felt it was better suited to feature film length.     The filmmakers wanted to focus primarily on the survivors rather than the zombies themselves. 

Casting involved asking actors to improvise entire sequences from the film. Gates and Bartlett picked the largely unknown cast as a result of this process. Shooting took place at weekends between July and November 2005 and post-production was completed during 2006. 

==Release==

The film made a small-scale cinema debut on October 29, 2006 at Letchworth Garden Citys Broadway Cinema, followed by a debut in Leicester Square on August 27, 2007, as part of the UKs biggest horror film festival London FrightFest Film Festival (this showing was preceded by an attempt to break the world record for participants in a zombie walk).

The US DVD was released on November 18, 2008, by Dimension Films.

==Reception==
Steve Barton of Dread Central rated it 4/5 stars and wrote, "Though The Zombie Diaries doesn’t exactly break any new ground, it does provide viewers with an extremely visceral and at times bleak experience."   Beyond Hollywood wrote that the large cast makes empathizing with individual characters difficult, but the films atmosphere and slow-moving zombies make it a successful Romero-inspired horror film.   Justin Felix of DVD Talk rated it 2.5/5 stars and wrote that although the action sequences get become repetitive, the characters are interesting.   David Johnson of DVD Verdict wrote, "Its not a thrill ride or an epic tale of the undead, but Zombie Diaries tells an interesting story with a unique approach."   Writing in The Zombie Movie Encyclopedia, Volume 2, academic Peter Dendle wrote, "If the zombies themselves arent brilliant, they inhabit a truly effective zombie movie and a convincing undead apocalypse." 

==Sequel==
 
In March 2010, it was announced by the films producer, writer and director Michael Bartlett that he and his co-producer, writer and director Kevin Gates had come up with an idea to make a sequel to the film. It was also announced that the sequel had been approved by The Weinstein Company and that Bartlett and Gates will be returning to direct and a few of the cast members from the first film will be returning to star.

The sequel received a limited UK cinema release on June 24, 2011, followed by a DVD released on June 27. A US release followed in October.  The leads are played by Alix Wilton Regan (replacing Victoria Summer as Leeann), Philip Brodie and Vicky Aracio. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 