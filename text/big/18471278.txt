Veronika Decides to Die (film)
{{Infobox film
| name           = Veronika Decides to Die
| image          = VeronikaDecidesToDie_USPoster.jpg
| caption        = Emily Young
| producer       = Sriram Das
| writer         =Paulo Coelho (book)  Larry Gross
| narrator       = Sarah Michelle Gellar
| starring       = Sarah Michelle Gellar   Jonathan Tucker   Erika Christensen   Melissa Leo   David Thewlis
| music          = Murray Gold 
| cinematography =
| editing        =
| distributor    = First Look International  Entertainment One Films 
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = 
| gross          = $1,369,647 (foreign)   
}} Emily Young same name by Paulo Coelho.
 New York instead of the original location of the novel in Ljubljana, Slovenia.

==Plot==
 
After a frantic suicide attempt, Veronika (Sarah Michelle Gellar) awakens inside a mysterious mental asylum. Under the supervision of an unorthodox psychiatrist who specializes in controversial treatment, Veronika learns that she has only weeks to live.

==Cast==
*Sarah Michelle Gellar as Veronika
*Jonathan Tucker as Edward
*Erika Christensen as Claire
*Melissa Leo as Mari
*David Thewlis as Dr. Blake

==Production==
Filming of the movie began on May 12, 2008 in New York City and wrapped on June 21. The novel was adapted for film by Muse Productions, Das Films, and Velvet Steamroller Entertainment. 
It was reported that Kate Bosworth was previously attached to the project. 

==Music==
Key scenes of the film use piano pieces The Recital in the Night by Murray Gold (from the Piano Set collection) and Comptine dun autre été - Laprès-midi by Yann Tiersen.

==Release==
The film had a special screening in   on October 2, 2009,  in Sweden on October 30, 2009,  and in South Korea on November 19, 2009.  It was released in Argentina on July 1, 2010, in Austria on November 18, 2010, in Germany on September 30, 2010, in Lithuania on April 23, 2010, and in Mexico on September 10, 2010. 
  In Australia, it was released on a region free PAL DVD.

It was never officially released in the UK.   

The film will be released in select theaters & VOD January 20, 2015 in the USA 
and DVD on March 17, 2015

The film received positive reviews upon release and universal acclaim for Gellars performance. Some critics saying that if the movie was released earlier Gellar should have been considered for an Academy award nomination. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 