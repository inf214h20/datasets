Mirage (2004 film)
{{Infobox film
| name =Mirage
| image =MirageFilm.jpg
| caption =  US movie poster of Mirage
| director = Svetozar Ristovski
| producer = Svetozar Ristovski Harold Lee Tichenor
| writer = Svetozar Ristovski Grace Lea Troje
| starring = Mustafa Nadarević Vlado Jovanovski Nikola Đuričko Dejan Aćimović Marko Kovačević
| music          = Klaus Hundsbichler
| cinematography = Vladimir Samoilovski
| editing        = Atanas Georgiev Picture This!  (United States|US) 
| released =  

| runtime = 107 minutes
| country        = Macedonia Macedonian Albanian Albanian
| budget         =
| gross          = 
}} transliterated Iluzija) is a 2004 Macedonian drama film starring Vlado Jovanovski, Mustafa Nadarević, Nikola Đuričko, and Dejan Aćimović, with Marko Kovačević debuting in its lead role. It was directed by Svetozar Ristovski, who co-wrote the film with Grace Lea Troje. Taking place in the city of Veles (city)|Veles, the film is a coming of age|coming-of-age story about a talented but abused schoolboy who is betrayed by illusory hopes of a better future and transformed by harsh circumstances into a criminal. It offers a grim depiction of post-independence Macedonia, portraying it as a site of violence and corruption.
  feature debut realism and lead actors performance. It won Best Feature Film during the 2005 Anchorage International Film Festival and was nominated for the Tokyo Grand Prix during the 2004 Tokyo International Film Festival.

==Synopsis== Veles during Macedonian schoolboy whose harsh circumstances gradually transform him into a criminal. Two mentors offer Marko hopes of a better future, but they eventually fail him, leading to his catastrophic change. The illusory nature of these hopes is foreshadowed by the films epigraph, an aphorism from Friedrich Nietzsches Human, All Too Human: "Hope is the worst of evils, for it prolongs the torments of man." 
  bingo and Albanian police Dejan Acimovic). To escape his troubles, Marko often takes refuge in a local train graveyard, playing chess to pass time.
  Bosnian teacher Macedonian (Mustafa Independence Day celebrations. He fills Marko with hopes of escaping his hometown through literary achievement to Paris, the "city of art".
 
However, Markos efforts gain only indifference from his family and mockery from school bullies. Although a kind man, his teacher offers little help. He flees when he witnesses Levis gang beating up Marko outside his own apartment, and his attempts to banish Levi from his classes fail because of Blashkos intervention. Furthermore, Fanny begins an affair with a black Kosovo Force soldier, providing Markos racist bullies with more fuel.
 
Meanwhile, a scarred soldier (  church.
 
Marko is eventually caught peddling stolen perfumes. Taken into police custody, he is blackmailed by Blashko into becoming Levis tutor. Levi in turn blackmails Marko into joining his gang for a school break-in, threatening him with his fathers gun. Meanwhile, Markos teacher subjects his poetry to severe criticism, devastating the boy. At his wits end, Marko begs Paris to teach him how to shoot a gun. Dismissive at first, Paris finally agrees.
 
At night, Levis gang and Marko break into school. They vandalize its main office, setting student records on fire and locking Marko inside the burning room. Scarred in the face by a broken bottle and recognized by the night watchman, Marko escapes and seeks refuge in the train graveyard. There, he discovers that Paris has abandoned him, leaving behind his pawn bullet.
 
Summoned by the principal to account for the vandalism, Marko refuses to denounce Levis gang. He is condemned as a delinquent and expelled from school. Markos teacher offers him no help, instead reporting him for alcohol and tobacco abuse. He replaces Marko with classmate Jasmina (Marija Sikalovska) for their Independence Day poetry recital, ghostwriting a patriotic poem for her.
  Gnossienne No. 3. 

==Cast==
In order given by the films credits: Svetozar Ristovski conceived of Markos personality as a combination of outward fragility and inner toughness, choosing Kovacevic for his ability to project these dual qualities.  
* Jordanco Cevrevski as Neighbor. He appears in the films opening, quarreling with Lazo for disturbing the neighbourhood with his drunken antics.
* Elena Mosevska as Angja. Markos mother.
* Slavica Manaskova as Fanny. Markos sister.
* Mustafa Nadarevic as Bosnian Teacher.
* Martin Jovchevski as Levi. Blashkos son.
* Nikola Hejko as Chernobyl. He appears as one of Lazos cronies. Hejko was also casting director for the film.
* Marija Sikalovska as Jasmina. Dejan Acimovic as Blashko. Levis father.
* Petar Mircevski as Policeman. He appears as one of Blashkos assistants.
* Andrijana Ristovska as Bingo Announcer. She appears in Lazos bingo scenes. When a man takes over her task in one of these scenes, the bingo parlour responds with derision to her replacement.
*   for a month and learning Macedonian. {{cite news
  | title = Before Start of Filming of First Feature Film in 2003 - Years of Hope to start on May 6
  | work = Utrinski Vesnik
  | date = 14 April 2003
  | url = http://www.culture.in.mk/story.asp?id=6467 
  | accessdate = June 2008}} 
* Salaetin Bilal as Jeweller. He appears as a buyer for Markos stolen jewellery. KFOR Soldiers. They appear as customers in a cafe where Marko is peddling stolen perfumes. Troje was also co-writer with Ristovski for the film, while Angelovski and Boskov were location managers.
* Alexandar Georgiev as Night Watchman.
* Bajrush Mjaku as Principal.

==Major themes==
  
Svetozar Ristovski intended the film to be a Coming of age|coming-of-age story about a boys transition from childhood to adolescence in contemporary Macedonia.  As part of this coming-of-age process, the protagonist Marko spends much of the film searching for paternal figures such as Paris and his teacher to compensate for his fathers inadequacies and become alternative role models for him.  The mysterious Paris, in particular, functions as a wish-fulfilment "mirage" for Marko.  
  civil conflict in former Yugoslav territories.  Critics have similarly interpreted the film as an allegory of these social and regional concerns.  In this context, Markos transition from victimhood to criminality embodies the cycle of violence endemic in such conditions,   while his failed hopes serve as a cautionary warning against the dangers of empty idealism in such situations.  One critic saw Paris bullet in Markos chess set (see figure on the right) as a metaphor for the threat of violence ever-present in their world. 
  extensive travel restrictions during Macedonias post-independence years.  Ristovski saw Markos story as a dramatization of that predicament: "  see these trains that are going up and down, but they cant get on any of those trains. They cant really get out of their place, their country, their town."  One critic saw Markos train graveyard (see figure on the left) as a metaphor for the hopelessness of his particular story: "there are tracks everywhere but no hope of transport." {{cite web
  | last = LOfficial
  | first = Peter
  | title = Tracking Shots: Mirage
  | work = The Village Voice
  | date = 7 March 2006
  | url = http://www.villagevoice.com/film/0611,lofficial,72542,20.html 
  | accessdate = May 2008}} 

==Production==
 
Mirage was Svetozar Ristovskis first attempt at directing a feature film; his prior work included the short film Hunter (2000) and the documentary Joy of Life (2001). {{cite web
  | title = EastWest Film Distribution: Svetozar Ristovski
  | url = http://www.eastwest-distribution.com/director_ristovski.htm 
  | accessdate = May 2008}}  {{cite web
  | title = Macedonian Cinema Information Center: Ristevski Svetozar
  | url = http://www.maccinema.com/e_avtor_detali.asp?idsorabotnik=631  WKCR interview, shot (see figure on the right), which served as a focal point towards which he worked out the rest of the films events.  
 
Ristovski collaborated with Grace Lea Troje on the script. He credited his Canadian-born colleague with providing the film an "international" perspective and giving its story a more "universal" dimension,  thus steering it away from parochialism.  According to Variety (magazine)|Variety, Andrei Tarkovskys Ivans Childhood (1962) was an important influence on the co-writers.  
 
The casting director for the film was Nikola Hejko, {{cite web
  | title = IMDB: Nikola Hejko
  | url = http://www.imdb.com/name/nm0374793 
  | accessdate = June 2008}}   who was chosen primarily for his prior work with juvenile actors in movies such as Kolya (1996) and The Great Water (2004).  Hejkos tasks for the project included finding actors for the films juvenile roles with the help of his assistant Maja Mladenovska.  According to Ristovski, this involved searching various Macedonian schools for potential actors,   as well as months of test shooting and selection narrowing.  The films adult roles were largely filled by actors from Macedonia and other Balkan countries.  
 
In various interviews, Ristovski cited several difficulties in producing the film. According to a 2005 interview, the project met with disapproval from the Macedonian government during pre-production. {{cite news 
  | last = Weinberg
  | first = Scott
  | title = SXSW 05 Interview: Mirage Director Svetozar Ristovski
  | work = Hollywood Bitchslap
  | date = 12 March 2005
  | url = http://hollywoodbitchslap.com/feature.php?feature=1412 
  | accessdate = May 2008}}  Ristovski credited his cast and crew for their dedication towards the project despite this difficulty, likening their participation to "comradery in a battle".  In his WKCR interview, Ristovski also cited lack of technical resources such as film equipment and film laboratory services, which had to be sourced overseas. 
 
 , the films setting.]]Ristovskis production company, Small Moves, partnered with the Vienna-based Synchro Film to produce the film. {{cite news 
  | title = Macedonia with Three Candidates for Mostra– Film of Svetozar Ristevski Applies for Venice
  | work = Vest
  | date = 13 July 2004
  | url = http://www.culture.in.mk/story.asp?id=10660 
  | accessdate = June 2008}}  The project had a budget of about 600,000 euros,                  to which the Macedonian Ministry of Culture contributed 500,000  and Synchro Film about 10%. {{cite news 
  | title = Film - Shooting of Years of Hope by director Svetozar Ristovski Under Way
  | work = Dnevnik
  | date = 6 May 2003
  | url = http://www.culture.in.mk/story.asp?id=6680 
  | accessdate = June 2008}}    During production, the films working title was Godini na nadez, The films post-production took place in Austria. 

The film was shot on location in Veles during May and June 2003.  {{cite news 
  | title = Macedonia in Great Festivals in North America and Asia - Mirage of Festivals in Toronto, Vancouver and Tokyo
  | work = Utrinski Vesnik
  | date = 13 August 2004
  | url = http://www.culture.in.mk/story.asp?id=10944 
  | accessdate = June 2008}}  Veles had been Ristovskis birthplace and childhood home,  and his familiarity with the place served as an advantage for finding filming locations.            He also had prior working experience with the films cinematographer, Vladimir Samoilovski.            According to his WKCR interview, he and Samoilovski aimed for a visual style that combined both "aesthetic beauty" and documentary-style "grittiness" in the films depiction of Veles. 

==Distribution== South by Southwest Film Festival. {{cite web
  | title = SXSW 2005 Film Screenings: Mirage
  | url = http://2005.sxsw.com/film/festival/screenings/?a=show&q=545 
  | accessdate = May 2008}}  The film saw its first US theatrical release in New York City during March 2006, followed by a DVD release in July.
 
Mirage is distributed by EastWest Filmdistribution located in Vienna (Austria). The films European premiere took place on 21 November 2004 during the 45th Thessaloniki International Film Festival. {{cite news 
  | title = 45th Thessaloniki Film Festival – Illusion with Three Projections in Thessaloniki
  | work = Utrinski Vesnik
  | date = 23 November 2004
  | url = http://www.culture.in.mk/story.asp?id=11588
  | accessdate = June 2008}}  In Macedonia, it premiered on 11 March 2005 during the 8th Skopje Film Festival, {{cite news 
  | title = Cultural Roundup
  | work = Southeast European Times
  | date = 9 March 2005
  | url = http://www.setimes.com/cocoon/setimes/xhtml/en_GB/features/setimes/roundup/2005/03/09/roundup-03
  | accessdate = June 2008}}  {{cite news 
  | title = Skopje Film Festival Begins on Friday
  | work = Macedonian Information Agency
  | date = 10 March 2005
  | url = http://groups.yahoo.com/group/Macedonian_News_Service/message/6156
  | accessdate = June 2008}}  with a theatrical release following in April. {{cite news 
  | title = Cinema Millennium – Premiere of Mirage of Svetozar Ristovski
  | work = Utrinski Vesnik
  | date = 8 April 2005
  | url = http://www.culture.in.mk/story.asp?id=12067 
  | accessdate = June 2008}} The Friday mentioned in the article is 8 April 2005. Times and venues for the theatrical screenings are available at http://www.culture.in.mk for the periods of  ,   and  . 

==Reception==
===Box office===
In the United States, the film opened in a single theatre on March 17, 2006 and grossed $1,511 during its one week of screening. {{cite web
  | title = Box Office Mojo: Mirage
  | url = http://www.boxofficemojo.com/movies/?id=mirage.htm 
  | accessdate = May 2008}}  Overall, the film has grossed $2,241 in the United States. 

===Critical reception===
The film was well-received by most film critics. On Rotten Tomatoes, it earned an aggregate review score of 82%, deeming it "Fresh" based on 11 reviews. {{cite web
  | title = Rotten Tomatoes: Mirage
  | url = http://www.rottentomatoes.com/m/mirage 
  | accessdate = May 2008}}  In Metacritic, the film earned a generally favourable metascore of 61% based on 8 reviews. {{cite web
  | title = Metacritic: Mirage
  | url = http://www.metacritic.com/film/titles/mirage?q=Mirage 
  | accessdate = May 2008}} 
 
In general, critics have praised the film for its uncompromising Realism (arts)|realism. After its Toronto premiere, Variety (magazine)|Variety lauded the film as "a modest triumph of fearless acting and pointed social commentary". {{cite news
  | last = Cockrell
  | first = Eddie
  | title = Toronto: Mirage (Macedonia)
  | work = Variety
  | date = 13 September 2004
  | url = http://www.variety.com/review/VE1117924885.html?categoryid=31&cs=1
  | accessdate = May 2008}}  Its release in the United States drew similar praise. "Ristovski needs us to feel his nations torment, and he succeeds," {{cite news
  | last = Weitzman
  | first = Elizabeth
  | title = Movie Digest: Mirage
  | work = New York Daily News
  | date = 17 March 2006
  | url = http://www.nydailynews.com/archives/entertainment/2006/03/17/2006-03-17_movie_digest.html 
  | accessdate = May 2008}}  wrote the New York Daily News. The Hollywood Reporter found the films treatment of Markos story "unremittingly grim and powerful", {{cite web
  | last = Scheck
  | first = Frank
  | title = Mirage review
  | work = The Hollywood Reporter
  | date = 19 May 2006
  | url = http://www.allbusiness.com/services/motion-pictures/4790444-1.html
  | accessdate = May 2008}}  while TV Guide praised it for being "tense   gripping" as well as "starkly beautiful". {{cite web
  | last = Fox
  | first = Ken
  | title = Mirage (2006, Movie)
  | work = TV Guide
  | date = 17 March 2006
  | url = http://www.tvguide.com/movies/mirage/review/278991  Balkan country". {{cite news
  | last = Genzlinger
  | first = Neil
  | title = Hard Times for a Macedonian Teen
  | work = The New York Times
  | date = 17 March 2006
  | url = http://movies.nytimes.com/2006/03/17/movies/17mira.html 
  | accessdate = May 2008}}  The Village Voice characterized the films portrayal of youthful misery as a "wicked evocation of hopelessness".   
 
However, the film also received a few negative reviews. The New York Post summed up the film as "drab, despairing and pointless",  while Slant Magazine found its treatment of Markos story "shrill" and "unreal", likening the film to "drag  the corpse of Billy Elliott     through sewer water". {{cite web
  | last = Gonzalez
  | first = Ed
  | title = Mirage review
  | work = Slant Magazine
  | date = 8 March 2006
  | url = http://www.slantmagazine.com/film/film_review.asp?ID=2113  Utrinski Vesnik praised the film for its realism and imagery, but concluded by criticizing its emphasis on misery for being one-sided. 
 
Critics have singled out Marko Kovacevic in his lead role for praise. His performance was lauded as "remarkable" by Variety  and "superb" by The Hollywood Reporter,  while The Village Voice commended him for "channel  gentle and ferocious with equal ease"  in his depiction of the protagonist.

===Awards===
{| class="wikitable"
|-
!Event !! Award !! Winner/Nominee !! Result
|-valign="top" 2004 Tokyo International Film Festival Tokyo Grand Prix Svetozar Ristovski Nominated
|-valign="top" 2005 Anchorage International Film Festival {{cite web
  | title = Anchorage International Film Festival 2005
  | url = http://137.229.184.12/2005/index.asp 
  | accessdate = May 2008}} More information at {{cite web
  | title = Iluzija (Mirage)
  | url = http://137.229.184.12/2005/guide/entry.asp?id=0346 
  | accessdate = May 2008}}   Best Feature Svetozar Ristovski Won
|-valign="top" 2005 Zlín International Film Festival for Children and Youth {{cite web
  | title = Film Festival Zlín: International Film Festival for Children and Youth - 45th festival
  | url = http://www.zlinfest.cz/vysledkove_listiny.php?zobrazArchiv=2005 
  | accessdate = May 2008}}  Best European Debut Film Svetozar Ristovski  Won
|-valign="top" 2006 Avanca Film Festival {{cite web
  | title = Festival Avanca 2006: International cinema and video competition
  | url = http://www.avanca.com/festival_2006_premios.htm 
  | accessdate = May 2008}}  Feature Film (Special Mention) Svetozar Ristovski  Won
|-valign="top" Best Actor (Special Mention) Marko Kovacevic  Won
|-valign="top" Cinematography
|Vladimir Samoilovski  Won
|}

==Notes==
 

==References==
===Reviews===
 
* {{cite news
  | last = Cockrell
  | first = Eddie
  | title = Toronto: Mirage (Macedonia) Variety
  | date = 13 September 2004
  | url = http://www.variety.com/review/VE1117924885.html?categoryid=31&cs=1
  | accessdate = May 2008}}
* {{cite web
  | last = Fox
  | first = Ken
  | title = Mirage (2006, Movie)
  | work = TV Guide
  | date = 17 March 2006
  | url = http://www.tvguide.com/movies/mirage/review/278991 
  | accessdate = May 2008}}
* {{cite news
  | last = Genzlinger
  | first = Neil
  | authorlink = Neil Genzlinger
  | title = Hard Times for a Macedonian Teen
  | work = The New York Times
  | date = 17 March 2006
  | url = http://movies.nytimes.com/2006/03/17/movies/17mira.html 
  | accessdate = May 2008}}
* {{cite web
  | last = Gonzalez
  | first = Ed
  | title = Mirage review
  | work = Slant Magazine
  | date = 8 March 2006
  | url = http://www.slantmagazine.com/film/film_review.asp?ID=2113 
  | accessdate = May 2008}}
* {{cite web
  | last = LOfficial
  | first = Peter
  | title = Tracking Shots: Mirage
  | work = The Village Voice
  | date = 7 March 2006
  | url = http://www.villagevoice.com/film/0611,lofficial,72542,20.html 
  | accessdate = May 2008}}
* {{cite web
  | last = Scheck
  | first = Frank
  | title = Mirage review
  | work = The Hollywood Reporter
  | date = 19 May 2006
  | url = http://www.allbusiness.com/services/motion-pictures/4790444-1.html
  | accessdate = May 2008}}
* Kyle Smith|Smith, Kyle (17 Mar 2006). "The pain in Spain creates familiar tears". New York Post: New York Pulse, p.&nbsp;58.
* {{cite news
  | last = Weitzman
  | first = Elizabeth
  | title = Movie Digest: Mirage
  | work = New York Daily News
  | date = 17 March 2006
  | url = http://www.nydailynews.com/archives/entertainment/2006/03/17/2006-03-17_movie_digest.html 
  | accessdate = May 2008}}

===Interviews===
* {{cite web
  | last = Hanrahan   
  | first = Brian
  | title = Art Attack / Friday 17 March: 9:00-10:00 pm / Part II: 9:30-10:00 / Director Svetozar Ristovski
  | work = 
  | publisher = WKCR-FM
  | date = 17 March 2006
  | url = http://www.columbia.edu/cu/wkcr/arts/programs.html 
  | accessdate = May 2008}}  

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 