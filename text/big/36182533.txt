Max Havoc: Ring of Fire
{{Infobox film
| name           = Max Havoc: Ring of Fire
| image          = Max Havoc Ring of Fire.jpg
| caption        = 
| director       = Terry Ingram
| producer       = Christian Arnold-Beutel   John F.S. Laing
| writer         = Donald Martin   Michael Stokes
| narrator       = 
| starring       = Mickey Hardt
| music          = John Sereda   Paul Michael Thomas
| cinematography = Anthony Metchie 
| editing        = David Czerwinski
| distributor    = Westlake Entertainment 
| released       =  
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}

Max Havoc: Ring of Fire is a 2006    (2004) Mickey Hardt played Max Havoc.

==Plot== mission in a no-go area. 

Sister Caroline informs Max about a street gang that systematically frightens off old-established shopkeepers. As Max learns Emile once started stealing because his parents (also shopkeepers) had been killed as a result of arson. While Max is still present, the street gang appears and threatens Sister Caroline because she is reluctant to pay protection money. Max fights against the gangsters but spares a member named Ramon for he is Emiles big brother.

The next day Emile witnesses how his brother Ramon is executed for alleged cowardice. Roger Tarso, the owner of the hotel where Max and Suzy and her mother currently stay, has decided to clear the slums by all means because he wants to add the land to his premises. In order to keep all this a secret he has Emile chased by his henchmen. But Max and Suzy discover his scheme anyway and try to find Emile first. In the end Max has to fight against an enemy who seems to know his fighting style better than Max himself.

==Cast==
*Mickey Hardt as Max Havoc
*Christina Cox as Suzy Blaine
*Linda Thorson as Denise Blaine
*Dean Cain as Roger Tarso
*Rae Dawn Chong as Sister Caroline
*Samuel Patrick Chu as Emile
*Martin Kove as Lt. Reynolds

==Reception==
Critics werent overly impressed by this films dialogues or its storyline but it was recommended for movie fans who enjoy martial arts. {{Cite news|url= http://www.mattmovieguy.com/2009/12/max-havoc-ring-of-fire-2006.html|title= The dialog is atrocious and the plot is pretty standard, so youre really only in this for Mickey Hardt beating the crap out of people
|accessdate=2012-06-19 |location=Kittery, Maine
|work= Direct to Video Connoisseur|first=Matt|last=Poirier}} 

==References==
 

==External links==
*  
*  

 
 
 
 