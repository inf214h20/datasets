The Devil's Hand
 
 
 
{{Infobox film
| name           = The Devils Hand
| image          = The Devils Hand.jpg
| image_size     =
| caption        =
| director       = William J. Hole Jr.
| producer       = Alvin K. Bubis (producer) Dave Carney (associate producer) Harris Gilbert (associate producer) Pierre Groleau (associate producer) Jack Miles (executive producer) Nick Newberry (associate producer) Rex Carlton (producer)  (uncredited)
| writer         = Jo Heims (screenplay)
| narrator       =
| starring       = See below
| music          = Allyn Ferguson Michael Terr
| cinematography = Meredith M. Nicholson
| editing        = Howard Epstein
| distributor    = Crown International Pictures
| released       =
| runtime        = 71 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
The Devils Hand is a 1962 American film directed by William J. Hole Jr..

==Plot==
 
Rick Turner is engaged to Donna Trent and is having nightmares of a beautiful blonde woman who appears to be dancing in the sky. One night, he is mysteriously driven to enter a doll shop, and in the next morning he returns to the place with Donna. He finds a doll that resembles his fiancé, but the owner Francis Lamont delivers another doll to him, with the face of the woman of his dreams, Bianca Milan. Rick looks for Bianca and is seduced and convinced by her to join a sect that worships the diabolic "Devil-god of Evil" Gamba, while the health of Donna is threatened by Francis and Bianca. Francis Lamont, the "High Executioner" of the sect, threatens various members of the cult via his voodoo powers, which are also used upon a journalist who infiltrates the sect. Ultimately Rick Turner escapes the influence of the beautiful witch Bianca Milan and helps Donna to escape the burning temple of Gamba.

==Cast==
*Linda Christian as Bianca Milan
*Robert Alda as Rick Turner
*Ariadne Welter as Donna Trent Neil Hamilton as Francis "Frank" Lamont
*Gene Craft
*Jeanne Carmen as The Blonde Cultist
*Julie Scott
*Diana Spears
*Gertrude Astor as The Elderly Cultist
*Bruno VeSota as Lindells Sponsor
*Dick Lee
*Jim Knight
*Coleen Vico Roy Wright as Doctor
*Romona Ravez

==Soundtrack==
 

==Reception==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 