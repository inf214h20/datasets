A.LI.CE
{{Infobox film
| name           = A.LI.CE
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kenichi Maejima
| producer       = 
| writer         = 
| screenplay     = Masahiro Yoshimoto
| story          = 
| based on       =  
| narrator       =  Kaori Shimizu   Ryō Horikawa
| music          = Akira Murata
| cinematography = 
| editing        = 
| studio         = GAGA Communications
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} CG science fiction comedy-drama anime film directed by Kenichi Maejima.  

==Plot==

In the year 2000, shortly after her best friends sudden suicide, a Japanese schoolgirl named Alice wins a competition to be the youngest person ever to be sent into space, on a semi-commercial space airline. However, something goes wrong during the flight, and the shuttle crashes, killing all on board except for Alice and the stewardess robot, SS1X. Finding themselves in a desolate, snowy landscape, they set out to find help.

A young man named Yuan aids them, and reveals that they are in Lapland, the year is 2030, and the worlds population is only one billion. At first he does not believe their claims to be from 30 years in the past. Yuan helps Alice and SS1X avoid the army which appears and begins pursuing her. Together they find enough old newspapers to discover what has happened in the years they have missed. A dictator known as Nero, and his computer named SS10X, have apparently been systematically capturing people and removing them to concentration camps, thus erasing all but one billion people. Most towns are empty and technology is old and decaying.

It is revealed that the pursuing army is the Liberation Army, who directly oppose Nero. Alice, Yuan, and SS1X are captured and taken to the armys centre of operations. There, it is revealed that there is a mysterious connection between Alice and Neros computer, and that, as a result, only Alice is able to hack into the computer and disable Neros security systems. The Liberation Army plan to disable all the security and storm Neros base to assassinate him and end his dictatorship. Alice agrees to hack the computer for the Liberation Army, despite being warned that it will be at great risk to herself. She uses a virtual reality device to float through the workings of the computer, disabling much of its workings but causing her mind great pain. With the automated security turned off, the Liberation Army sends all their troops to Neros base for their final assault.

Yuan and SS1X, meanwhile, escape from their imprisonment and race to stop Alice from hacking any further before she causes herself serious harm. Alice, aware that her connection to Neros computer, SS10X, is not a coincidence, says that she feels she needs to finally meet Nero. They all travel to Neros base, narrowly avoiding the security systems which are just beginning to reactivate. The entirety of the Liberation Army are killed by the security robots, except for its leader, Kaspar.

Kaspar reveals that it was he who sabotagued Alices space flight by replacing her space shuttle with a duplicate which contained a hidden time machine but was otherwise identical to the original. Kaspar had realised that Nero was the only person other than Kaspar himself potentially capable of building a time machine of his own, so he brought the only person compatible with Neros SS10X computer, Alice, from the past to destroy Nero before he ever had the chance to build his own time machine. By removing Nero, his only obstacle, Kaspar intends to use his time machine to insert himself into key moments of history so that he is ultimately seen as a god.

Neros security system manages to activate enough to impale and kill the megalomaniac Kaspar, however, not before Kaspar fires shots into Neros huge computer, causing the beginning of its destruction. Alice finally approaches Nero himself, who reveals that he is her son. He explains that Alice fell into a coma shortly after his birth, and that despite his genius intellect, he only ever wanted nothing more than to communicate with her. He built his computer, based on the SS1X model Alice had known as the stewardess on her space flight, in an effort to see into the mind of the comatose Alice. This is why Alice is the only person able to hack the computer: it is tuned only to her mind.

Unfortunately, in her coma, the older Alice only mentally repeats the last words of her best friend who had committed suicide in the year 2000. Her friend had mused that the sky was not as blue as it should be, due to the pollution caused by human overpopulation. Acting on this, the SS10X computer began to decrease the human population to bring about a natural blue sky. Nero was never the dictator responsible for the wasteland of 2030, but the unconscious and unaware Alice was.

As the massive computer begins to fall apart, Alice begs Nero to escape with her. However, he says that having finally spoken with his mother, he is happy at last. He realises that his attempt to reach his mother inadvertently brought about a dictatorship and mass destruction, so he chooses to die rather than escape. Alice flees the base with Yuan and SS1X just as the destruction of SS10X reaches its peak. In the heart of the enormous computer, the suspended, comatose body of an older Alice is glimpsed.

Alice uses the space shuttle, with its time machine, to travel back to 2000. She intends to prevent the terrible future she has seen. Meanwhile, SS1X reveals to Yuan that she found data log files in Neros base that reveal where the missing seven billion people are being kept, and that together they can rescue them all, including Yuans parents. As Alice flies back into the past, she reflects on all she has experienced before passing out from the high forces of atmospheric re-entry. The shuttle lands safely in the ocean, and a rescue crew manage to break into the hull. As she awakens, the first thing Alice sees is a young member of the rescue crew, holding his hand out to her, wearing the same necklace that Nero wore. The film ends as she smiles and reaches for his hand. 

==Cast== Kaori Shimizu
*Ryō Horikawa

==Reception==
Joseph Savitski of BeyondHollywood.com gave the film a positive review. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 