The Last Flight of Noah's Ark
{{Infobox film
| name           = The Last Flight of Noahs Ark
| image          = Poster of the movie The Last Flight of Noahs Ark.jpg Ron Miller Jan Williams
| director       = Charles Jarrott
| writer         = George Arthur Bloom Steven W. Carabatsos Ernest K. Gann (story) Sandy Glass
| starring       = Elliott Gould Geneviève Bujold Ricky Schroder
| music          = Richard Bowden Maurice Jarre
| cinematography = Charles F. Wheeler
| editing        = Gordon D. Brenner Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| gross          = $11,000,000
}} Disney film released by Buena Vista Distribution on June 25, 1980. The film stars Elliott Gould, Geneviève Bujold and Ricky Schroder.

==Plot== pilot named Noah Dugan (Gould) is unemployed and owes a large amount of money due to his gambling. He goes to an old friend, Stoney (Vincent Gardenia), who owns an airfield. He is offered a job flying a cargo of animals to a remote South Pacific island aboard a B-29 bomber. Bernadette Lafleur (Bujold) is the prim missionary who accompanies him. Bernadette has raised the animals at an orphanage and is close to two of the orphans, Bobby and Julie (Schroder and Tammy Lauren).

The two children cannot bear to part with their beloved animals and stow away aboard the bomber as it takes off. During the flight, the plane goes off course and Dugan is forced to crash land on an uncharted island. While on the island, the group meets two elderly Japanese holdout soldiers who have lived there alone for 35 years. At first they treat them as enemies as the soldiers are unaware that World War II is over. However, Bernadette wins their friendship and trust, and they propose to turn the plane into a boat to sail back to civilization.
 Cutter

==Cast==
*Elliott Gould as Noah Dugan
*Geneviève Bujold as Bernadette Lafleur
*Ricky Schroder as Bobby
*Vincent Gardenia as Stoney 
*Tammy Lauren as Julie 
*John Fujioka as "Cleveland"
*Yuki Shimoda as Hiro
*Dana Elcar as Benchley

==Background and production== The High and the Mighty and Fate Is the Hunter). Tammy Lauren was his stepdaughter.

Elliott Gould has said that this was the finest film he ever did and the one of which he was proudest. 

Four B-29 airplanes were used in the film production.

The feature film was released to many drive-in theaters on a double bill with One Hundred and One Dalmatians (a Disney classic). The films promotional slogan was "treat your family to a Disney summer".

==Reception==

The feature received a mixed reception from critics. 

==See also==
*List of surviving Boeing B-29 Superfortresses

==References==
 

==External links==
*  
*  
*  
*   at UltimateDisney.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 