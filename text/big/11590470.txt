Offending Angels
{{Infobox film
| name           = Offending Angels
| image          = 
| caption        = 
| director       = Andrew Rajan	
| producer       = Andrew Rajan	
| writer         = Tim Moyler Moyler Andrew Rajan
| starring       = Susannah Harker Shaun Parkes Andrew Lincoln Andrew Rajan
| music          = Martin Ward
| cinematography = Alvin Leong
| editing        = Roger Burgess Catherine Fletcher	
| distributor    = Guerilla Films
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = £89 or £79
| preceded_by    = 
| followed_by    =
}}

Offending Angels is a 2000 British romantic comedy film directed by Andrew Rajan.

==Plot==
Sam and Baggy are two non-committal slackers who while their time away with nonsensical affairs while dreaming of greater things in life. Paris and Zeke are two guardian angels who confront them with plans for change. Paris is a former dolphin while Zeke is a former squirrel.

==Cast==
*Susannah Harker.....Paris
*Shaun Parkes.....Zeke
*Andrew Lincoln.....Sam
*Andrew Rajan.....Baggy
*Jack Davenport.....Rory
*Stephen Mangan.....Fergus
*Michael Cochrane.....Mentor

==Reception==
The film became notorious because it took £89  {{cite news
  | last = logboy
  | first = 
  | title = Offending Angels. £70k Budget, £89 Box Office. 8 DVD Sales to Double its Takings
  | work = Twitch.net
  | date=  2006-02-03
  | url = http://www.twitchfilm.net/archives/005008.html Prince Edward exited the industry, which proved to be terrible timing for the film release in the UK.

===Critics===
The film split the critics; panned by some including the BBC, who called it a "truly awful pile of garbage", {{cite news
  | last = Russell
  | first = Jamie
  | authorlink = Jamie Russell
  | title = Offending Angels (2002)
  | work = BBC
  | date=  2002-04-10
  | url = http://www.bbc.co.uk/films/2002/04/10/offending_angels_2002_review.shtml
  | accessdate = 2007-01-16 }}  but Film Review, called it "A rather heartwarming story of the need to remember to live your life". {{cite news
  | last = Cameron Wilson
  | first = James
  | title = Offending Angels film review
  | work = Film Review
  | date=  May 2002
  | url = http://www.visimag.com/filmreview/index.htm
  | accessdate = 2010-08-30 }} 

==Awards and nominations==
Emden International Film Festival
*Nominated, "Emden Film Award" – Andrew Rajan

==References==
 

==External links==
*  
*  
* http://www.pantsproductions.com/5667.html

 
 
 
 
 
 


 
 