When the Game Stands Tall
{{Infobox film
| name           = When the Game Stands Tall
| image          = When the Game Stands Tall poster.jpg
| caption        = Theatrical release poster Thomas Carter
| writer         = Scott Marshall Smith
| producer       = David Zelon
| starring       = Jim Caviezel Laura Dern Michael Chiklis Alexander Ludwig
| music          = John Paesano
| cinematography = Michael Lohmann
| editing        = Scott Richter
| studio         = Affirm Films Mandalay Pictures 
| distributor    = TriStar Pictures 
| released       =  
| runtime        = 115 minutes 
| country        = United States
| language       = English
| budget         = $15 million   
| gross          = $30.1 million 
}} De La Salle High School of Concord, California.       The film is an adaptation of the 2003 book of the same name by Neil Hayes, published by North Atlantic Books. De La Salle head coach Bob Ladouceur retired in January 2013 after winning his last Open Division state championship in December 2012.    Filming began April 22, 2013 and lasted until June 15, 2013.  The film was released on August 22, 2014.

==Plot== De La Salle Spartans, watches his team win the championship of their undefeated 2003 season. Following tradition, the seniors share their stories about what the team means to them. The juniors, including Ladouceurs son Danny, celebrate with team captain Chris Ryan who has a chance of breaking the California state record for the highest career number of touchdowns. During an argument about whether to accept a coaching offer at the college level, Ladouceurs wife Beverly tells him that he does not spend enough time with his family. A division meeting between coaches arranges to have De La Salle play against Long Beach Polytechnic High School, a team with a similar reputation.

Ladouceur engages the team members in a passage from the Gospel of Luke which states that all good deeds will be rewarded. This provokes a mixed reaction from players who come from disadvantaged households. Reminded of team alumnus Cameron Colvin, Ladouceur pays him a visit in Richmond, California and discovers that his mother is dying. Upon returning home, Ladouceur suffers a heart attack, impacting his ability to coach in the 2004-2005 season. Colvin is approached by his best friend Terrance Kelly who urges him to enroll at the University of Oregon so that they can play together. Not wanting to involve friends in the downward spiral he has found himself in, Colvin protests but eventually agrees. Days later, Kelly is gunned down while trying to pick up a friend and the Spartan team attends his funeral.
 winning streak comes to an end when they lose a game against Bellevue, Washington. After the game, Mickey Ryan hits his son and threatens further abuse if he does not break the touchdown record. Coach Ladouceur takes the team to a veterans rehabilitation center, to deconstruct their respective ideas of brotherhood. To prepare for the game against Long Beach, the teammates watch videos of previous games and see that all of the Poly players are faster and more physically imposing. The gruelling game becomes a victory for De La Salle after Danny knocks down a pass by Poly into the endzone to help the Spartans hold Poly on all 4 attempts on goal. The Spartans enter the final 2005 game with a renewed sense of confidence and hope that Ryan can make three touchdowns to break the record.

A number of touchdowns during the game, including two by Chris, place De La Salle in a comfortable lead. Mickey cheers that there is enough time left for him to break the record. When setting up their final play, Ryan gathers his teammates and tells them that it would be wrong for him to end his time with the team by chasing a personal victory. He instead gives up the ball and raises his helmet to Ladouceur. As the audience members do the same, the game ends with a tribute to the coach that brought them there.

==Cast==
*Jim Caviezel as Coach Bob Ladouceur
*Laura Dern as Bev Ladouceur
*Michael Chiklis as assistant coach Terry Eidson
*Alexander Ludwig as running back Chris Ryan
*Gavin Casalegno as Michael Ladouceur
*Clancy Brown as Mickey Ryan  Cam Colvin 
*Stephan James as Terrance G. "T.K." Kelly 
*Matthew Daddario as Danny Ladouceur 
*Joe Massingill as Joe Beaser 
*Jessie Usher as Tayshon Lanear 
*Matthew Frias as Arturo Garcia 
*LaJessie Smith as Jamal 
*Richard Kohnke as Rick Salinas 
*Chase Boltin as Manny Gonzales 
*Adella Gautier as T-Gram 
*Terence Rosemore as Landrin 
*Deneen Tyler as Veronica 
*Anna Margaret as Laurie 
*James DuMont as Gordy Wilock 
*David DeSantos as Mike Blasquez 
*Ricky Wayne as Marty
*Tre Tureaud as T.Ks Killer

==Production== Thomas Carter, Friday Night Lights, "  the football scenes".  Hayes, a Contra Costa Times journalist at the time the book was released, was part of the creative team.  Zelon said the intrigue of this film was that Ladouceur "placed little value on winning, instead focusing his players on giving a perfect effort in life".  SPWA president Steve Bersch noted that the themes in the film transcended sports and cinema: "At its core, this is a timeless and universal story about character, hard work and love". 

==Reception==
When the Game Stands Tall has received mixed to negative reviews from critics. On Rotten Tomatoes, the film has a rating of 18%, based on 62 reviews. The sites critical consensus reads, "An uneasy blend of solid game sequences and threadbare inspirational sports drama clichés, When the Game Stands Tall is overshadowed by better players in a crowded field."  On Metacritic, the film holds a score of 41 out of 100, based on 24 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*  
*  
*   at History vs. Hollywood
* 
* 
*  
*  
*  
* 
* 
 

 
 
 
 
 
 
 
 
 
 