Heathcliff: The Movie
{{Infobox Film
| name = Heathcliff: The Movie
| image = Heathcliffmovie.jpg
| alt =
| caption = Theatrical release poster Bruno Bianchi
| producer = Jean Chalopin Andy Heyward  Tetsuo Katayama
| writer = George Gately
| based on =  
| starring = Mel Blanc Donna Christie Jeannie Elias
| music = Shuki Levy Haim Saban LBS Communications McNaught Syndicate Clubhouse Pictures  
| released =  
| runtime = 70 minutes
| country = Canada France United States
| language = English
| budget = 
| gross = $2,610,686
}}
Heathcliff: The Movie is a 1986 animated film from   in 2006.

==Plot== Heathcliff (Mel the TV series.

===Stories===
# "Cat Food for Thought" - Heathcliff becomes a TV star after getting rid of his competition.
# "Heathcliffs Double" - Theres a new cat in town called Henry who looks exactly like Heathcliff, and everybody mistakes him for Heathcliff. Siamese Twins" - There are two new cats in town that are ruining Heathcliffs reputation, making everyone think Heathcliff is the cause of their troubles.
# "An officer and an Alley Cat" - To win a lifetime supply of free cat food, Heathcliff goes to obedience school to be worthy for the contest.
# "The Catfather" - In this parody of The Godfather, Heathcliff collects gifts for the Catfather, oblivious to the fact that the Catfather is the scare of the town. wrestler Boom cheats to win matches.
# "Pop on Parole" - Heathcliffs father has gotten parole for jail time and Heathcliff believes he broke out and the cops are chasing him.

==Cast==
* Mel Blanc as Heathcliff / Spike
* Donna Christie as Iggy Nutmeg
* Jeannie Elias as Marcy
* Marilyn Lighstone as Sonja / Grandma
* Ted Zeigler as Mungo / Grandpa Stan Jones as Wordsworth
* Danny Mann as Hector / Fish Shop Proprietor
* Peter Cullen as Pop
* Marilyn Schreffler as One
* Derek McGrath as Two
* Danny Wells as General and Announcer

==Release==
The film was released theatrically on January 17, 1986 and grossed $2,610,686 domestically by the end of its run. 

==See also==
* List of animated feature-length films

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 