Angie (1994 film)
{{Infobox film
| name           = Angie
| image          = Angieposter.jpg
| caption        = Theatrical release poster
| director       = Martha Coolidge
| producer       = Larry Brezner Patrick McCormick
| writer         = Todd Graff Novel: Avra Wing
| starring       = Geena Davis Stephen Rea James Gandolfini
| music          = Jerry Goldsmith
| cinematography = Johnny E. Jensen
| editing        = Steven Cohen
| studio         = Hollywood Pictures Caravan Pictures Buena Vista Pictures
| released       = March 4, 1994
| runtime        = 107 minutes
| country        = United States
| awards         =
| language       = English
| budget         = $26 million
| gross          = $9,398,308
| preceded by    =
| followed by    =
}}
 romantic comedy-drama film directed by Martha Coolidge, and starring Geena Davis as the titular character. It is based on the 1991 novel Angie, I Says by Avra Wing, which was a New York Times Notable Book of 1991.

==Plot==
Angie (Geena Davis) is an office worker who lives in the Bensonhurst section of Brooklyn, New York and dreams of a better life. After learning that she is pregnant by her boyfriend Vinnie (James Gandolfini), she decides that she will have the baby, but not Vinnie as a husband.

This turns the entire neighborhood upside down and starts her on a journey of self-discovery, including a love affair with a man named Noel (Stephen Rea) who she meets at an art museum. Even her best friend Tina (Aida Turturro) has trouble understanding her.

==Reception==

The film opened to mixed reviews but did not make as much money as was hoped. In addition, Geena Davis, who won an Oscar six years before for   was to play the title character, but dropped out at the last
minute. The film was famous for introducing three actors who would star on the TV show  , Aida Turturro, and Michael Rispoli.

===Awards===
*The movie was nominated an Artios for Best Casting for Feature Film, Comedy by the Casting Society of America.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 