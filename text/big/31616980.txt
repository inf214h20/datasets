Udhyanapalakan
{{Infobox film
| name           = Udhyanapalakan
| image          = Udhyanapalakan.jpg
| caption        = CD Cover Hari Kumar
| producer       = G. P. Vijayakumar
| writer         = Lohithadas Kaveri Rekha Mohan Nedumudi Venu Oduvil Unnikrishnan Kalabhavan Mani Johnson
| Venu
| editing        = G. Murali
| art            = Premachandran
| studio         = Seven Arts
| distributor    = Seven Arts Release
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Hari Kumar. Kaveri in the lead roles, and Rekha Mohan, Nedumudi Venu, Oduvil Unnikrishnan and Kalabhavan Mani in other pivotal roles.

==Plot==
Mammootty plays the lead role of the protagonist, Sudhakaran Nair, an ex-army man who injured his leg while in service. He is now running a shop in the town. He maintains a beautiful garden in front of his house. The films plot is centred on the romance between the mid-aged Sudhakaran Nair and Indu, a young girl in the neighbourhood.

==Cast==
* Mammootty as Sudhakaran Nair Kaveri as Indu
* Rekha Mohan as Suma
* Nedumudi Venu as Gopalettan
* Oduvil Unnikrishnan as Govinda Menon
* Kalabhavan Mani as Jose
* Biju Menon as Mohan
* Rizabawa as Indus father
* Valsala Menon as Indus mother
* Ravi Vallathol as Indus uncle
* Bindu Panikkar as Gopalettans wife
* Mamukkoya as marriage broker
* Ponnamma Babu as Shantha, Sudhakaran Nairs sister
* Cochin Haneefa as Gangadharan, Shanthas husband
* Krishna Prasad as Shanthas son
* Chandni as Sumithra
* Sivaji as Sumithras father
* Lakshmi Krishnamurthy
* Cherthala Lalitha

==Soundtrack==
{{Infobox album|  
| Name = Udhyanapalakan
| Type = soundtrack Johnson
| Cover =  Feature film soundtrack
| Label = Big B
| Last album = 
| This album = 
| Next album = 
}} Johnson with lyrics by Kaithapram Damodaran Namboothiri.
{{Track listing
| extra_column = Singer(s)
| length1 = 
| title1 = Ekantha Ravin Pinvathil 
| extra1 = K. J. Yesudas
| length2 = 
| title2 = Kurunnu Thamarakkuruvi 
| extra2 = K. S. Chithra
| length3 = 
| title3 = Mayyazhippuzhayozhuki
| extra3 =  K. S. Chithra
| length4 = 
| title4 = Mayyazhippuzhayozhuki
| extra4 = K. J. Yesudas
| length5 = 
| title5 = Panineerppoovithalil
| extra5 = K. J. Yesudas
}}

==External links==
*  

 
 
 


 