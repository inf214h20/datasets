Paradise (1928 film)
{{Infobox film
| name           = Paradise
| image          =
| caption        =
| director       = Denison Clift 
| producer       = 
| writer         = Philip Gibbs   Violet E. Powell
| starring       = Betty Balfour  Joseph Striker   Alexander DArcy   Winter Hall
| music          =
| cinematography = René Guissart (director)|René Guissart
| editing        = Emile de Ruelle
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 10 September 1928
| runtime        = 7,246 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama film directed by Denison Clift and starring Betty Balfour, Joseph Striker and Alexander DArcy. A clergymans daughter wins £500, and decides to take a holiday on the French Riviera. There she becomes ensared by a foreign fortune hunter, but her true love comes and rescues her. 

==Cast==
* Betty Balfour as Kitty Cranston 
* Joseph Striker as Dr. John Halliday 
* Alexander DArcy as Spirdoff 
* Winter Hall as Reverend Cranston 
* Barbara Gott as Lady Liverage 
* Dino Galvani as Manager 
* Boris Ranevsky as Commissionaire 
* Albert Brouett as Detective 
* Ina De La Haye as Douchka

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 