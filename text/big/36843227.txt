Polite People
 
 
{{Infobox film
| name           = Polite People
| image          = Polite People film poster.jpg
| caption        = Film poster
| director       = Olaf de Fleur Johannesson
| producer       = 
| writer         = Olaf de Fleur Johannesson Hrafnkell Stefansson
| starring       = Stefán Karl Stefánsson
| music          = 
| cinematography = Bjarni Felix Bjarnason
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 
}}

Polite People ( ) is a 2011 Icelandic comedy film directed by Olaf de Fleur Johannesson.    

==Cast==
* Stefán Karl Stefánsson as Lárus Skjaldarson
* Ágústa Eva Erlendsdóttir as Margrét
* Hilmir Snær Guðnason as Hrafnkell
* Eggert Þorleifsson as Markell
* Ragnhildur Steinunn Jónsdóttir as Hanna
* Halldóra Geirharðsdóttir as Elín
* Benedikt Erlingsson as Þorgeir
* Gunnar Hansson as Jónatan
* Ingvar Eggert Sigurðsson as Þorsteinn
* Friðrik Friðriksson as Danskur ljósmyndari
* Jóhann G. Jóhannsson as Askur

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 