For the Sake of My Intemperate Youth
 
{{Infobox film
| name           = For the Sake of My Intemperate Youth
| image          = For the Sake of My Intemperate Youth.jpg
| caption        = Film poster
| director       = Arne Mattsson
| producer       = Rune Lindström
| narrator       =
| starring       = Aino Taube
| music          =
| cinematography = Sven Thermænius
| editing        = Carl-Olov Skeppstedt
| distributor    =
| released       =  
| runtime        = 115 minutes
| country        = Sweden
| language       = Swedish
| budget         =
}}

For the Sake of My Intemperate Youth ( ) is a 1952 Swedish drama film directed by Arne Mattsson. It was entered into the 1953 Cannes Film Festival.     

==Cast==
* Aino Taube - Greta Arvidsson
* Georg Rydeberg - Karl Arvidsson
* Nils Hallberg - Kuno Andersson
* Ulla-Bella Fridh - Maj (as Ulla-Britt Fridh)
* Ester Roeck Hansen - Berit Altman
* Erik Bullen Berglund - Altman (as Erik Berglund)
* Ib Schønberg - Madsen (as Ib Schönberg)
* Naima Wifstrand - Vendela Påhlman
* Margareta Fahlén - Elaine
* Ragnvi Lindbladh - Marianne (as Ragnvi Lindblad)
* Wiktor Andersson - School Janitor (as Kulörten Andersson)
* Else Jarlbak - Maid (as Elsie Jarlback)
* Hanny Schedin - Majs Mother
* Birgitta Olzon - Majs Schoolmate (as Birgitta Olsson)
* Axel Högel - Train Conductor
* Mats Björne - Jens
* Lars Egge - Dr. Knut Hegel
* Erik Hell - Blind Musician
* Maj-Britt Nilsson - Ingrid Ninni Arvidsson
* Folke Sundquist - Torben Altman

==References==
 

==External links==
* 

 
 
 
 
 
 
 