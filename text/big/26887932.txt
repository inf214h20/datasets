Dhwani
 
{{Infobox film
| name           = Dhwani
| image          = Dhwani.jpg
| director       = A. T. Abu
| producer       = Amjad Ali
| writer         = P. R. Nathan
| screenplay     = P. R. Nathan
| narrator       = 
| starring       = Prem Nazir Jayabharathi Jayaram Shobhana Vaikom Muhammad Basheer Suresh Gopi
| music          = Naushad
| cinematography = Venu CE Babu
| editing        = G Venkittaraman
| studio         = Mak Productions
| distributor    = Mak Productions
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| awards         = 
}} musical romance film directed by A. T. Abu. It was the last completed film of veteran actor and evergreen hero of Malayalam cinema Prem Nazir who died on 16 January 1989.  {{cite book
 |author=
 |title=India Today
 |volume= Volume 14 part =  
 |publisher=Living Media India Pvt. Ltd
 |year=1989
 |pages=45–48
 |isbn=
}}  The  film has music composed by legendary  . Retrieved April 28, 2011. 

==Cast==
 
*Prem Nazir as Rajasekharan Nair
*Jayaram as Shabari
*Shobhana as Devi
*Jayabharathi as Malathi
*Suresh Gopi as Dinesh
*Karamana Janardanan Nair as Kuttisankaran
*Thilakan as Vettukuzhy
*Sukumari as Thankamani
*Nedumudi Venu as Shekharan Innocent as Rappayi
*Jagathi Sreekumar ...  Manikanta Pillai
*K. P. Ummer ...  Omalloor Sadasivan
*Sabitha Anand ...  Kanakam
*Haneefa Ambadi ...  Sakhaavu Bhaskran
*Balan K. Nair ...  Bahuleyan
*Kunjandi
*K. P. A. C. Sunny Rohini ...  Sunitha
*Vaikom Muhammad Basheer ... the visitor at the hospital (special appearance)
 

==Soundtrack==
{{Infobox album   
| Name = Dhwani
| Type = soundtrack 
| Artist = Naushad 
| Cover = 
| Released = 1988
| Recorded =  Feature film soundtrack 
| Length = 
| Label =  
| Producer = 
| Music Director = Naushad 
| Reviews = 
| Last album = Love and God (1986)
| This album = Dhwani (1988)
| Next album = Teri Payal Mere Geet (1989)
}}
The score and soundtrack for the movie are composed by renowned musician Naushad. It remains as the only Malayalam movie for which Naushad had composed music for. The lyrics are written by poet Yusuf Ali Kechery in Malayalam and Sanskrit. The soundtrack is highly regarded as one of the best and classic songs of all times in Malayalam film music. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s) !! Raga
|-
| "Aankuyile Thenkuyile"
| K. J. Yesudas
|
|-
| "Anuraaga Lola"
| K. J. Yesudas, P. Susheela
| Gowri manohari
|-
| "Maanasa Nilayil"
| K. J. Yesudas
| Abheri
|-
| "Oru Ragamaala Korthu"
| K. J. Yesudas
|
|-
| "Jaanaki Jaane"
| K. J. Yesudas
| Yamuna Kalyani
|-
| "Jaanaki Jaane"
| P. Susheela
| Yamuna Kalyani
|-
| "Rathi Sukha Saaramayi"
| K. J. Yesudas
| Sindhu Bhairavi
|}

==Awards==
* Kerala State Film Award for Second Best Actor - Thilakan 


==References==
 

==External links==
*  

 
 
 
 
 