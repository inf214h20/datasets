Jimmy the Kid
{{Infobox film
| name           = Jimmy the Kid
| image          = Jimmy The Kid.jpg
| caption        =  Gary Nelson
| producer       = Ronald Jacobs
| writer         = Sam Bobrick (writer), Donald E. Westlake (1974 novel)
| starring       = Gary Coleman John Cameron
| cinematography = Dennis Dalzell
| editing        = Richard C. Meyer	
| distributor    = New World Pictures
| studio         = Zephyr Productions
| released       = November 12, 1982  , Retrieved December 10, 2010   , Lawrence Journal-World, Retrieved December 10, 2010 (national newspaper advertisement used in November 1982 to advertise release of film) 
| runtime        = 85 minutes
| country        = United States
| language       = English 
| gross          = $5 million Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 222 
}}
 Gary Nelson, produced by Ronald Jacobs, and released in November 1982 by New World Pictures.  Following 1981s On the Right Track, it was second theatrical film release starring Coleman. 

==Background==
The film was based on the 1974 novel of the same name by  , Retrieved December 10, 2010 

==Cast==
* Gary Coleman as Jimmy
* Paul Le Mat as John Dortmunder
* Ruth Gordon as Bernice
* Dee Wallace as May
* Cleavon Little as Herb
* Don Adams as Harry Walker
* Pat Morita as Maurice
* Fay Hauser as Nina
* Avery Schreiber as Dr. Stevens
* Walter Olkewicz as Kelp

==Reception==
Overall, critical reception of the family-friendly comedy was on the negative side.  Even  , Retrieved December 10, 2010 ("AS ONE OF FEW Americans who publicly declared his affection for "On the Right Track," Gary Colemans first feature film, lets just say that "Jimmy the Kid," Colemans second film, is definitely on the wrong track, preferring the screeching of car tires to character development.")  Colwell, Carter (21 November 1982).  ,  , Retrieved December 10, 2010 (""Jimmy the Kid" proves a longstanding cinema law: Any movie calling itself a "comedy crime caper" is likely to be a misdemeanor against good taste.")  (28 May 1983).  ,   (1982) 

==References==
 

==External links==
*  
*  
*  

 

 
 