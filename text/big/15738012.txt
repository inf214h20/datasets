Kurosagi (film)
{{Infobox Film name = Kurosagi writer = Eriko Shinozaki starring = Tomohisa Yamashita Maki Horikita director =  producer =  cinematography =  editing =  released = March 8, 2008  runtime = 126 min. language = Japanese
}}

Kurosagi is a film based on the 2006 popular TV series Kurosagi.
 TBS received many questions about whether there would be a new season or a film. TBS decided to shoot a film, beginning in August 2007.

On March 8, 2008, the cast appeared on stage at TOHO Cinemas Roppongi Hills. At the end of the conference, a guard with a suit case filled with 3,800,000,000 yen (since it was March 8) appeared on stage. Yamashita said he would make another Kurosagi film, shooting it overseas. 

The official theme song is Taiyō no Namida (太陽のナミダ). It is performed by NEWS (band)|NEWS, a 6-member band. Tomohisa Yamashita, who also plays the lead role, is in the band.

==Plot synopsis==
A young man named Kurosaki had his family destroyed by con artists. When he grows up, he becomes a swindler to avenge his family. He swindles other con artists and returns the money to their victims.

==Cast==
 
* Tomohisa Yamashita as Kurosagi (Kurosaki)
* Maki Horikita as Yoshikawa Tsurara
* Yui Ichikawa as Mishima Yukari
* Mao Daichi as Sakura
* Naoto Takenaka as Ishigaki Tetsu
* Renji Ishibashi
 

==References==
 

==External links==
*  
*  

 
 


 