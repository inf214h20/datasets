Antha (film)
{{Infobox film
| name           = Antha
| image          = Antha poster.jpg
| image size     = 
| caption        = 
| director       = S. V. Rajendra Singh Babu
| producer       = H.N. Maruthi Venugopal
| story          = H.K. Anantha Rao
| screenplay     = H.V. Subba Rao S. V. Rajendra Singh Babu
| narrator       = Lakshmi Latha
| music          = G. K. Venkatesh
| cinematography = P. S. Prakash
| editing        = K. Balu
| studio         = Parimala Arts
| distributor    = Amee Films
| released       =  
| runtime        = 129 min
| country        = India
| language       = Kannada
}}
 Kannada film Lakshmi and Latha in lead roles. The film went on become a huge success and is viewed as a milestone in Ambareeshs career. It was remade in Telugu as Kadidi Aarambam and in Hindi as Meri Aawaz Suno.

==Cast==
* Ambareesh Lakshmi
* Latha
* Jayamala
* Pandari Bai
* Vatsala
* Baby Sindhu
* M. S. Umesh
* Vajramuni
* Tiger Prabhakar
* Shaktiprasad
* Sundar Krishna Urs

==Soundtrack==
The music of the film was composed by G. K. Venkatesh with lyrics penned by Chi. Udaya Shankar, R. N. Jayagopal and Geethapriya.
{{Infobox album	
| Name = Antha
| Longtype = to Antha
| Type = Soundtrack	
| Artist = G. K. Venkatesh	
| Cover = 
| Border = Yes	
| Alt = Yes	
| Caption = 
| Released = 
| Recorded = 
| Length =  Kannada 
| Label = Sa Re Ga Ma
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Baaramma Illi Baaramma"
| S. P. Balasubramanyam, S. Janaki
|-
| 2
| "Deepaveke Beku"
| S. Janaki
|-
| 3
| "Naanu Yaaru Yaava Ooru"
| S. P. Balasubramanyam
|-
| 4
| "Premavide"
| S. Janaki
|}

==Awards==
* Karnataka State Film Awards - Special Award - Ambareesh For Best Screenplay - Rajendra Singh Babu For Best Cinematographer - P. S. Prakash

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 

 