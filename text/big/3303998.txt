How Much Wood Would a Woodchuck Chuck (film)
{{Infobox film
| name           = How Much Wood Would a Woodchuck Chuck
| image          = How Much Wood Would a Woodchuck Chuck 1976 film.jpg
| caption        = Title card
| director       = Werner Herzog
| producer       = Werner Herzog
| writer         = Werner Herzog
| narrator       = Werner Herzog
| starring       = Werner Herzog   Steve Liptay   Ralph Wade   Alan Ball   Abe Diffenbach
| music          = 
| cinematography = Thomas Mauch
| editing        = Beate Mainka-Jellinghaus
| studio         = Werner Herzog Filmproduktion   Süddeutscher Rundfunk
| distributor    = Werner Herzog Filmproduktion
| released       = February 14, 1977 (West Germany)
| runtime        = 45 minutes
| country        = West Germany English  German
| budget         = 
| preceded_by    = 
| followed_by    = 
}} German director Werner Herzog, produced by Werner Herzog Filmproduktion. It is a 44 minute film documenting the World Livestock Auctioneer Championship held in New Holland, Pennsylvania. Herzog has said that he believes auctioneering to be "the last poetry possible, the poetry of capitalism." 

Herzog describes the auctioneering as an "extreme language ... frightening but quite beautiful at the same time." {{cite book
  | last = Herzog
  | first = Werner
  | title = Herzog on Herzog
  | publisher = Faber and Faber
  | year = 2001
  | isbn = 0-571-20708-1
  | page = 140 }}  Herzog used two of the featured auctioneers as actors in his later film Stroszek.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 