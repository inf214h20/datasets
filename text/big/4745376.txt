Enakku 20 Unakku 18
{{Infobox film name = Enakku 20 Unakku 18 image = caption = director = Jyothi Krishna cameo = Tamannaah Bhatia producer = A. M. Rathnam writer = Jyothi Krishna starring = Tarun Shriya Vivek
|music = A. R. Rahman cinematography = R. Ganesh editing = Kola Bashkar distributor = Sri Surya Movies released =   runtime = country = India language = Tamil
}} Tamil romantic Vivek and Manivannan play other pivotal roles. The film was also simultaneously shot and released in Telugu with a slightly different cast as Nee Manasu Naaku Telusu. The films score and soundtrack are composed by A. R. Rahman.

==Plot==
Sridhar (Tarun (actor)|Tarun) is a final year degree student and gets selected in the campus selections for studies abroad. He goes to Mumbai for an interview and in his return finds Preeti (Trisha Krishnan|Trisha) and loses his heart to her. Sridhar comes to know that Preeti was about to join in degree and Preeti learns that he was a final year degree student. Sridhar is an ardent cricket fan, so is Preeti, while Sridhar is also a good football player. The principal of the college appoints a coach called Reshma (Shriya Saran|Shriya), who is also a student, for the football team led by Sridhar. Sridhar tries his best to trace Preeti by visiting several womens colleges, while Preeti also watches for Sridhar at the mens college. Accidentally, Preeti helps Sridhars mother, when she had a heart attack. In order to find Preeti at any cost, Sridhar goes to the LB Stadium, where a cricket match is in progress and gets injuries in a bomb blast in the stadium. Just before the blast, both Preeti and Sridhar find each other but could not meet due to the blast.

Later Preetis parents convince her to agree for a marriage and tell her to forget about the boy she was searching. At the same time, Sridhars mother also tells him to concentrate on studies and go abroad to complete his studies. At this juncture, Sridhar comes to know that Preeti was staying just behind his house, when her parents come to his house to inform that she is getting married. Sridhars gang participates in an inter-college football match and wins the cup. Before his return, Sridhars mother suffers yet another heart attack and Preetis father admits her in the hospital. Then they come to know that Sridhars brother-in-law was behind bars for no fault of his and Preeti helps him to come out of the case. Meanwhile, Sridhars mother passes away. Later Sridhars brother-in-law gets another job and the family leaves for Bangalore. Sridhar feels lonely so, he decides to go abroad. After three years, Sridhar returns from abroad to find Preeti in the same compartment. To his surprise, Preeti is still unmarried and they unite.

 
*Tarun Kumar as Sridhar
*Trisha Krishnan as Preeti
*Shriya Saran as Reshma Vivek as Kapali (Kapil)
*Manivannan as Preetis Father
*Tamannaah Bhatia as Preetis Friend
*Riyaz Khan as Preetis Brother
*Devadarshini as Sridhars Sister Anand as Sridhars brother-in-law
*Archana Puran Singh as Preetis Mother
*Kalairani as Sridhars Mother Swarna as Preetis Sister in law
*Reemma Sen as Priyanka (Guest Appearance) Mano as himself (Guest Appearance)
*R. Sundarrajan as Train Passenger (Guest Appearance)
*Vaiyapuri as Train Passenger (Guest Appearance)
*Janaki Sabesh as Train Passenger (Guest Appearance)

==Production==
Trisha Krishnan had signed the film before any of her other films had released.  For one song,  260 shots were taken and picturised at 90 locations in Chennai and Hyderabad. 

==Release==
The film opened to mixed reviews, with a critic from The Hindu citing that "certain scenes have appreciable depth while some are downright superficial and predictable" and that it was "absorbing in parts, spontaneous in spurts, natural at times and clichéd now and then".  However another critic labelled the film as "strictly for youth" and a "perfect choice". 

==Soundtrack==
{{Infobox album 
|  Name        = Enakku 20 Unakku 18
|  Type        =  Soundtrack
|  Artist      = A. R. Rahman
|  Cover       = 
|  Background  =  2003
|  Recorded    =  Panchathan Record Inn
|  Genre       = Film soundtrack
|  Length      = 
|  Label       = Five Star Audio
|  Producer    =  A.R. Rahman
|  Reviews     = 
}}

All lyrics written by Pa.Vijay

The soundtrack features 6 songs composed by A. R. Rahman.
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Artist(s)
|-
| "Askava"
| Surjo Bhattacharya, Shreya Ghoshal 
|-
| "Sandhipoma"
| Unni Menon, Anupama (singer)|Anupama, Chinmayee
|-
| "Kama Kama"
| Anupama, Aparna, Kunal Ganjawala, Blaaze, George Peter, Benny Dayal
|-
| "Oru Nanban Irundhal"
| S. P. B. Charan, Venkat Prabhu, Chinmayee
|-
| "Yedho Yedho"
| Karthik (singer)|Karthik, Gopika Poornima 
|-
| "Asathura"
| Srinivas (singer)|Srinivas, Chitra Sivaraman, Mathangi, George Peter 
|}

==References==
 

== External links ==
*  

 
 
 
 
 
 