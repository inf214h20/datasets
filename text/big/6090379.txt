The Golem: How He Came into the World
 
 
{{Infobox film name           = The Golem: How He Came into the World image          = Golem 1920 Poster.jpg writer         = Henrik Galeen Paul Wegener starring       = Paul Wegener Albert Steinrück Lyda Salmonova Ernst Deutsch Lothar Müthel director       = Paul Wegener Carl Boese producer  Paul Davidson cinematography = Karl Freund distributor    =  released       =   runtime        = 85 minutes language  German intertitles country        = Weimar Republic budget         = music          = awards         =
}}
 The Golem The Golem (1915) and the short comedy The Golem and the Dancing Girl (1917), in which Wegener dons the Golem make-up in order to frighten a young lady he is infatuated with. It is a prequel to The Golem and is the best known of the series, largely because it is the only one of the three films that has not been lost film|lost.

==Plot== Rabbi Loew, the head of the citys Jewish community, reading the stars. Loew predicts disaster for his people and brings his assistant to inform the elders of the community. The next day the Holy Roman Emperor signs a royal decree declaring that the Jews must leave the city before the new moon. The Emperor sends the knight Florian to deliver the decree. Loew meanwhile begins to devise a way of defending the Jews.

Upon arriving at the ghetto, the arrogant Florian falls in love with Miriam, Loews daughter for whom his assistant shares affection. Loew talks Florian into reminding the Emperor that it is he who predicts disasters and tells the horoscopes of the Emperor, and requests an audience with him. Having courted with Miriam, Florian leaves. Loew begins to create a huge monster out of clay by praying with God first, the Golem, which he will bring to life to defend his people. Florian returns later with a request from the Emperor for Loew to attend the Rose Festival at the palace. He shares a romantic moment with Miriam while Loew reveals to his assistant that he has secretly created the Golem, and requires his assistance to animate it. In an elaborate magical procedure, Loew and the assistant summon the spirit Astaroth and compel him, as per the ancient texts, to say the magic word to bring life. This word is written on paper by Loew which is then enclosed in an amulet and inserted onto the Golems chest. The Golem awakes. Loews assistant then tames the Golem, and the Rabbi uses it as a household servant.

When Loew is summoned to the palace for the festival, he brings the Golem with him to impress the audience. Florian meanwhile slips away from the court to meet Miriam, whose house is being guarded by Loews assistant under Loews instruction. Back at the palace the court is both terrified and intrigued by the arrival of the Golem. Impressed, the Emperor asks to see more supernatural feats by Loew. Loew projects a magical screen showing the history of the Jews, instructing his audience not to make a noise. Upon the arrival of Ahasuerus, the Wandering Jew, the court begins to laugh and the palace suddenly crumbles. As the building collapses around them, the Golem intervenes and props up the falling ceiling, saving the court. As a sign of gratitude the Emperor pardons the Jews and allows them to stay in the city.

Loew and the Golem return to the ghetto, spreading the news that the Jews are saved. Loew returns to his house and begins to notice erratic behaviour in the Golem. He reads that upcoming astrological movements will cause Astaroth to possess the Golem and attack its creators. Loew removes the amulet and is called down by his assistant to join in the celebrations in the street. As the community rejoices, the assistant goes to inform Miriam and bring her to the synagogue but finds her in bed with Florian. Devastated, he reanimates the Golem and orders it to remove Florian from the building. But the Golem, now under Astaroths influence, literally does so by throwing Florian from the roof of the house, killing him. Horrified, the assistant and Miriam flee, but the Golem sets fire to the building and Miriam falls unconscious.

Loews assistant rushes to the synagogue to alert the praying Jews of the disaster, but upon their arrival at Loews house they find that it is burning and both the Golem and Miriam are missing. Despaired, the community begs to Loew to save them from the rampaging Golem. Loew performs a spell that removes Astaroth from the Golem. Promptly, the Golem, who is wandering the ghetto causing destruction, leaves Miriam, whom he has been dragging by the hair through the streets, lying on a stone surface and heads towards the ghetto gate. He breaks the gate open and sees a group of girls playing. They all flee except for one, whom he picks up, having developed a docile nature following the removal of Astaroth. Out of curiosity she removes the amulet from the Golem. It drops her and collapses. Loew meanwhile finds Miriam, who awakes shortly after. Happily reunited, they are awkwardly joined by Loews assistant, who informs him that the Jews are waiting for him by the gate. Loew having left, the assistant promises to Miriam that he will never tell anyone of her forbidden affair with Florian, and asks in return for forgiveness for his actions. The Jews meanwhile gather at the gate to find the dead Golem. Rejoicing and praying, they carry it back into the ghetto, the Star of David appearing on the screen as the film ends.

==Production==
  and English]] The Student of Prague (1913). 
 Jewish ghetto expressionist imagery.  The cinematography of Karl Freund, in collaboration with Poelzig and Wegener, is cited as one of the most outstanding examples of German Expressionism.
 remade in 1936 as Le Golem, which was directed by Julien Duvivier on location in Prague. 

==Cast==
* Albert Steinrück as Rabbi Loew
* Paul Wegener as The Golem
* Lyda Salmonova as Miriam
* Ernst Deutsch as Loews assistant
* Lothar Müthel as Knight Florian
* Otto Gebühr as Emperor
* Hans Stürm as Rabbi Jehuda
* Max Kronert as The Gatekeeper
* Greta Schröder as A Lady of the Court
* Loni Nest as Little Girl
* Fritz Feld as a jester

==See also==
* List of films made in Weimar Germany

==References==
 

==External links==
*  
*  
*  
*   at Rotten Tomatoes

==Citations==
*  
* Matei Chihaia: Der Golem-Effekt. Orientierung und phantastische Immersion im Zeitalter des Kinos, transcript, Bielefeld 2011 ISBN 978-3-8376-1714-6
* {{Citation
  | title = The Overlook Film Encyclopedia
  | url = http://books.google.com/books?id=3-ehQgAACAAJ&dq=overlook+film+encyclopedia
  | editor-last = Hardy
  | editor-first = Phil
  | editor-link = Phil Hardy (journalist)
  | volume = 3
  | publisher = Overlook Press
  | year = 1995 
  | isbn=0-87951-624-0 }}

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 