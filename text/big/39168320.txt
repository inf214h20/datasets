Miele (film)
 
{{Infobox film
| name           = Miele
| image          = Miele (film).jpg
| caption        = Film poster
| director       = Valeria Golino
| producer       = Viola Prestieri Riccardo Scamarcio
| writer         = Francesca Marciano Valeria Golino Valia Santella
| based on       = novel A nome tuo by Mauro Covacich
| starring       = Jasmine Trinca Carlo Cecchi
| music          = 
| cinematography = Gergely Pohárnok
| editing        = Giogiò Franchini
| distributor    = BIM Distribuzione (Italy)
| released       =  
| runtime        = 96 minutes
| country        = Italy
| language       = Italian
| budget         = €1.650.000
}}

Miele is a 2013 Italian drama film directed by Valeria Golino. It was screened in the Un Certain Regard section at the 2013 Cannes Film Festival    where it won a commendation from the Ecumenical Jury.    It was also nominated for the 2013 Lux Prize.

==Cast==
* Jasmine Trinca as Irene/Miele
* Carlo Cecchi as Carlo Grimaldi
* Libero De Rienzo as Rocco
* Vinicio Marchioni as Stefano
* Iaia Forte as Clelia
* Roberto De Francesco as Filippo
* Barbara Ronchi as Sandra
* Massimiliano Iacolucci as Irenes father
* Claudio Guain as Ennio
* Valeria Bilello as Irenes mother

==Accolades==
 
 
{| class="wikitable"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of Accolades
|- style="text-align:center;"
! style="background:#ccc;" width="40%"| Award / Film Festival
! style="background:#ccc;" width="30%"| Category
! style="background:#ccc;" width="25%"| Recipient(s)
! style="background:#ccc;" width="15%"| Result
|- style="border-top:2px solid gray;"
|- Nastro dArgento|67th Nastri dArgento Best New Director
| Valeria Golino
|  
|- Best Producer
| Riccardo Scamarcio and Viola Prestieri
|  
|- Best Actress
| Jasmine Trinca
|  
|- Best Supporting Actor
| Carlo Cecchi
|  
|- Best Editing
| Giogiò Franchini
|  
|- Best Sound
| Emanuele Cecere
|  
|- 66th Cannes Film Festival
| Un Certain Regard Award
| Valeria Golino
|  
|-
| Prize of the Ecumenical Jury - Special Mention
| Valeria Golino
|  
|- David di 59th David di Donatello Awards
|- Best New Director
| Valeria Golino
|  
|- Best Script
| Francesca Marciano, Valia Santella & Valeria Golino
|  
|- Best Producer
| Riccardo Scamarcio and Viola Prestieri
|  
|- Best Actor
| Carlo Cecchi
|  
|- Best Actress
| Jasmine Trinca
|  
|- Best Cinematography
| Gergely Poharnok
|  
|- Best Editing
| Giogiò Franchini
|  
|- Globo doro|53rd Italian Golden Globe
|- Best First Feature
| Valeria Golino
|  
|- Best Actress
| Jasmine Trinca
|  
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 