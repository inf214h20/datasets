The Tesseract (film)
 
 
{{Infobox film 
| name = The Tesseract
| image = The Tesseract movie poster.jpg
| caption = Promotional poster for the Sundance Channel. Oxide Pang
| producer = Takashi Kusube Naoki Kai Soo-Jun Bae Jun Hara Koichi Shibuya
| based on =  
| screenplay = Oxide Pang Patrick Neate
| starring = Jonathan Rhys-Meyers Saskia Reeves
| music = James Iha
| cinematography = Decha Srimantra Oxide Pang Piyapan Chooppetch
| distributor = Momentum Pictures
| released = 
| runtime = 92 minutes
| country = Cinema of Japan|Japan/Cinema of Thailand|Thailand/Cinema of England Thai
| budget = 
}}
 2003 thriller the novel Oxide Pang.
 English drug Thai assassination|assassin, and an abused 13-year old boy demonstrate that life is so complex that even the smallest events can have enormous, even fatal consequences (i.e. the butterfly effect).

==Plot==
Sean, a runner for a drug gang, has checked into room 303 at the seedy, rundown Heaven Hotel in Bangkok, to await arrival of a package of heroin. Another guest is Rosa, psychologist who is researching slum children, on the floor below (room 202). In the next room, 203, is Lita, a female assassin who is waiting to intercept the package Sean is waiting for. Tying them all together, is the 13-year-old bellboy, Wit, a streetwise, light-fingered kid.

==Cast==
* Jonathan Rhys Meyers as Sean
* Saskia Reeves as Rosa
* Alexander Rendell as Wit
* Carlo Nanni as Roy
* Lena Christenchen as Lita

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 