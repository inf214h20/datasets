Love Hurts (2009 film)
{{Infobox film
| name           = Love Hurts
| image          = Love_Hurts_(2009).JPG
| alt            =  
| caption        = Film poster
| director       = Barra Grant
| producer       = Brian Reilly
| writer         = Barra Grant
| starring       = Richard E. Grant Carrie-Anne Moss Johnny Pacar
| music          = 
| cinematography = Alan Caso
| editing        = Roger Bondelli
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Love Hurts is a 2009 romantic comedy film with Richard E. Grant, Carrie-Anne Moss, Johnny Pacar, Jenna Elfman, Janeane Garofalo, and Camryn Manheim. It was written and directed by Barra Grant.

==Synopsis==
Richard E. Grant plays Dr. Ben Bingham, a middle-aged ear, nose, and throat doctor. Carrie-Anne Moss plays Amanda, his vibrant wife. Amanda leaves Ben when the idea of sharing an empty nest with her inattentive husband becomes unbearable. Ben becomes depressed and tries to drown himself in alcohol. He walks around in  pajamas until his son Justin (Johnny Pacar) gives his dad a makeover and introduces him to the social scene. Before long, Ben becomes so popular that he is pursued by his nurse, his personal trainer, and karaoke-loving twin sisters. However, things quickly change when Justin finds himself in love. It is now Ben’s turn to teach his son the art of romance and in the process recapture his own wifes love.

==Cast==
* Richard E. Grant as Ben Bingham
* Carrie-Anne Moss as Amanda Bingham
* Johnny Pacar as Justin Bingham
* Jenna Elfman as Darlene
* Janeane Garofalo as Hannah Rosenbloom
* Camryn Manheim as Gloria
* Julia Voth as Young Amanda
* Angela Sarafyan as Layla
* Jeffrey Nordling as Curtis
* Yvonne Zima as Andrea
* Olga Fonda as Valeriya
* Rita Rudner as Dr. Lisa Levanthorp Jim Turner as Doctor
* Cameron Van Hoy as Young Ben

== External links ==
*  
*  
*   at the Cleveland International Film Festival
*  
*   at Metacritic
*  
*  

 
 
 
 
 


 

 