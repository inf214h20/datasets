Tol'able David
{{Infobox film
| name           = Tolable David
| image          = Tolable David-Poster.JPG
| caption        = Theatrical release poster Henry King
| producer       = Henry King
| writer         = Edmund Goulding Henry King
| based on       =  
| starring       = Richard Barthelmess Gladys Hulette Walter P. Lewis Ernest Torrence
| cinematography = Henry Cronjager
| editing        = W. Duncan Mansfield
| studio         = Inspiration Pictures
| distributor    =
| released       =  
| runtime        = 99 minutes
| country        = United States Silent (English intertitles)
| budget         =
}} 1921 United American silent Henry King for Inspiration Pictures.
 Photoplay Magazine 1921 "medal of honor" and is seen by critics and viewers as one of the classics of silent film. 

In 2007, Tolable David was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Cast==
*Richard Barthelmess as David Kinemon
*Gladys Hulette as Esther Hatburn
*Walter P. Lewis as Iscah Hatburn
*Ernest Torrence as Luke Hatburn
*Ralph Yearsley as Saul "Little Buzzard" Hatburn
*Forrest Robinson as Grandpa Hatburn

==Plot==
Young David Kinemon, son of West Virginia tenant farmers, longs to be treated like a man by his family and neighbors, especially Esther Hatburn, the pretty girl who lives with her grandfather on a nearby farm.  However, he is continually reminded that he is still a boy, "tolable" enough, but no man.  

He eventually gets a chance to prove himself when outlaw Iscah Hatburn and his sons Luke and "Little Buzzard," distant cousins of the Kinemons Hatburn neighbors,  move into the Hatburn farm, against the will of Esther and her grandfather.  Esther initially tells David not to interfere, saying hes no match for her cousins.  Later, the cousins kill Davids pet dog and cripple his older brother while the latter is delivering mail and taking passengers to town in his "hack" wagon.  Davids father sets out to administer vigilante justice on the Hatburn cousins (the sheriff doesnt have the means to deal with the outlaws himself), but has a heart attack.  David is determined to go after the Hatburns in his fathers place, but his mother talks him out of it, arguing that with his father dead and brother crippled, the household, including his brothers wife and infant son, depends on him.  The family is then turned out of the farm and are forced to move into a small house in town.  David asks for his brothers old job of driving the hack but is told he is too young.  He does find work at the general store though.  Later, when the hacks regular driver is fired for drunkenness, David finally has a chance to drive the hack.  He loses the mailbag near the Hatburn farm, where it is found by Luke.  David goes to the Hatburn farm to demand the mailbag.  He is refused and gets into an argument with the cousins, during which he is shot in the arm.  David then shoots Iscah and the younger son and later, after a prolonged fight with the older brother (meant to recall the story of David and Goliath), emerges victorious.  Esther flees for help and makes it to the village, telling that David has been killed.  As a crowd prepares to go look for David, he, although injured, arrives in the hack with the bag of mail.  It is clear to all that David, no longer merely "tolable," is a real man and a hero.

==Other adaptations==
*Harold Lloyds The Kid Brother (1927 in film|1927) was a highly regarded comedy film that used a similar plot, and also featured Tolable David actor Ralph Yearsley.  Lloyd said in later years that it was his favorite of his own films. 1930 sound Richard Cromwell, Noah Beery, and Joan Peers. 

==Additional info==
Some of the climactic scenes in the 1959 horror movie The Tingler take place in a specialty theater during a showing of Tolable David.

==References==
 

==External links==
 
*  
*  
*  
*   at Virtual History

 
 

 
 
 
 
 
 
 
 
   
   
 