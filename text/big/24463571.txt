La Valentina (1966 film)
{{Infobox film
| name           = La Valentina 
| image          = La Valentina MoviePoster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Rogelio A. González
| producer       = Gregorio Walerstein
| writer         = 
| screenplay     = José María Fernández Unsáin
| story          = José María Fernández Unsáin Gregorio Walerstein Eulalio González 
| based on       = "La Valentina" (public domain)
| narrator       = 
| starring       = María Félix Eulalio González
| music          = Manuel Esperón
| cinematography = Rosalío Solano
| editing        = Rafael Ceballos Estudios San Ángel Cima Films
| distributor    = Azteca Films
| released       =   
| runtime        = 99 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} romantic comedy film directed by Rogelio A. González, produced by Gregorio Walerstein, and starring María Félix and Eulalio González in the leading roles.    The supporting cast features José Elías Moreno, José Venegas "El Bronco"|José Venegas, and Raúl Meraz. The film is a dramatization of the Mexican Revolution corrido of the same name. 

==Plot==
In early 20th century Mexico, in the midst of the Mexican Revolution, the beautiful yet feisty Valentina Zúñiga marries an unnamed serviceman. Though she is part of the rural upper class, Valentina is a fervent supporter of the Revolution. At the same time but in another place, a smuggler named Genovevo Cruz García sells defective arms to the federal Mexican army.

During Valentinas wedding night, gunshots are heard outside her bedroom window. This frustrates her, as she believes it is a waste of ammunition. Her newlywed husband, however, believes that the guns are shot in celebration to their wedding. As he approaches the bedroom balcony to stop the gunshots, Valentinas husband is shot multiple times and killed. The next day, at the burial of her husband, Valentina desires vengeance.

When Genovevos deceit becomes known, since his faulty arms were the cause of multiple casualties, the federals decide to execute him by a firing squad. As he is executed and falls to the ground, an elderly couple ask for his body so that they may give him a Christian burial. Genovevos body is driven away on the couples donkey and it is later known that the execution was false. Federal military captain Luis Benítez, the man who headed the firing squad, tells Genovevo that he was shot with rubber bullets and saved from death for a reason. Benítez starts to explain that he has fallen in love with Valentina Zúñiga, ordering Genovevo to kidnap her for him. Genovevo agrees but bursts into laughter. Responding to Genovevos laughter, Benítez states that Valentina is worth the dangerous attempt of kidnapping, as she is beautiful, affectionate, and sweet.

Genovevo therefore travels to Valentinas village, concocting a clever plan to kidnap her. As Valentina and her father and brothers are short of ammunition, they send for Genovevo, who quickly agrees to sell them arms on the condition of taking Valentina with him to the place where his merchandise is located. Her father refuses but Valentina decides to go and she is accompanied by her two brothers. Upon arrival at the location of Genovevos merchandise, Valentinas brothers fall into Genovevos trap and he successfully kidnaps her.

==Cast==
 
 
*María Félix as Valentina Zúñiga
*Eulalio González as Genovevo Cruz García
*José Elías Moreno as Don Juan Zúñiga
*José Venegas "El Bronco"|José Venegas as Epigmenio Zúñiga
*Raúl Meraz as Captain Luis Benítez
*Graciela Lara as Lupita
*Carlos Agostí as Valentinas husband
*Víctor Alcocer as the federal Colonel
*Ricardo Carreón as the federal Sergeant
*Carlos León as Melitón Zúñiga
*Juan Ferrara as a federal soldier
*Jorge Lavat as Erasmo
*Victor Sorel as Bedulo
*Hugo Avendaño as a revolutionary singer
*Graziella Garza as a revolutionary singer
*Manuel Dondé as the man giving the eulogy (uncredited)
*Manuel Vergara as the prisoner at the betting (uncredited)
*Manuel Alvarado as the bartender (uncredited)
*Mercedes Carreño as the maid of the Zuiñga household (uncredited)
*Agustín Isunza as the corrido vendor (uncredited)
*Roberto Meyer as Don Quintín (uncredited)

==Production==
 s municipal cemetery]]
According to Eulalio González, María Félix wanted to star with him in a comedy film, and therefore, the film came to be.    The films script was exclusively written by González so his "Piporro" character could alternate with María Félix.  The films background is the Mexican Revolution, an armed conflict between the government and the peasantry during the early 20th century. Hints of the storys setting in the film suggest that the film took place in the state of Jalisco, since Félixs character remarks her hometown as Tepatitlán,  and a song in the film mentions Guadalajara. 

==Release== Million Dollar United Artists theaters in Los Angeles, California. 

===Critical response===
One contemporary critic, writing for the Mexican Política magazine, criticized the film as "tragically stupid and revealing of the hallucinating level of our commercial cinema."   

==Accolades==
 
{| class="wikitable"
|-
! Award !! Category !! Name !! Outcome
|-
| Silver Goddess Awards    || Best Actress in a Minor Role (Mejor actriz de cuadro)|| Graciela Lara ||  
|}

==Music==
*"La Valentina" (public domain) performed by Hugo Avendaño and Graziella Garza.
*"Tengo ganas" written and performed by Eulalio González.
*"El güero aventao" written and performed by Eulalio González and María Félix.
*"La norteña" written by Eduardo Vigil y Robles and performed by Hugo Avendaño and Graziella Garza.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 