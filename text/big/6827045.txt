Vasantam
For the Singaporean television channel please see:MediaCorp Vasantham
{{Infobox film
| name           = Vasantam
| image          = 
| image_size     = 494 × 480 pixels
| caption        =
| director       = Vikraman
| producer       = N V Prasad S. Naga Ashok Kumar
| writer         = Vikraman  
| story          = Chintapalli Ramana
| screenplay     = Vikraman
| narrator       = Venkatesh Aarti Kalyani
| music          = S. A. Rajkumar 
| cinematography = B. Balamurugan
| editing        = Marthand K. Venkatesh
| studio         = Sri Sai Deva Productions
| distributor    =
| released       = 11 July 2003
| runtime        = 164 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood film Kalyani in lead roles and music is composed by S. A. Rajkumar.  The film opened simultaneously alongside the Tamil version of the film, Priyamaana Thozhi.

==Plot==
Ashok (Venkatesh) and Julie (Kalyani) are childhood friends. Ashok is like a family member of Julies family. They are pretty close and they believe that friendship is above love. They do not have any love feelings towards each other. After they grow up Julies father (Chandra Mohan) dies and Ashok brings Julie to his house and she stays along with his family. Nandini (Arti Agarwal) is from a rich family and Ashok falls in love with her and they get married. The rest of film revolves around how Ashok and Nandini get Julie married.

==Cast==
{{columns-list|3| Venkatesh as Ashok
* Aarti Agarwal as Nandini / Nandu Kalyani as Julie
* V.V.S.Laxman as himself Akash as Michel Sunil as Ramesh Chandra Mohan as Peter 
* Tanikella Bharani 
* Dharmavarapu Subramanyam 
* L. B. Sriram  Kondavalasa  Surya 
* Siva Reddy Pruthvi Raj 
* Ahuti Prasad 
* Prasad Babu 
* Vizag Prasad  Ananth as Panthulu
* Gundu Hanumantha Rao 
* Junior Relangi
* Chitti Hema as Hema
* Delhi Rajeswari 
* Rajasri
* Deepa 
* Devisri 
* Banda Jyothi 
* Master Teja 
* Master Shashak 
* Baby Akshaya Jayaram 
* Baby Srihitha
}}

==Soundtrack==
{{Infobox album
| Name        = Malleswari
| Tagline     = 
| Type        = film
| Artist      = S. A. Rajkumar
| Cover       = 
| Released    = 2003
| Recorded    = 
| Genre       = Soundtrack
| Length      = 37:16
| Label       = Aditya Music
| Producer    = S. A. Rajkumar
| Reviews     =
| Last album  = Siva Rama Raju   (2002)  
| This album  = Vasantham   (2003)
| Next album  = Tiger Harischandra Prasad   (2003)
}}

Music composed by S. A. Rajkumar. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 37:16
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ammo Ammayena
| lyrics1 = Kulasekhar Sujatha
| length1 = 4:47

| title2  = Gaali Chirugaali Sirivennela Sitarama Sastry  Chitra
| length2 = 4:34

| title3  = Ninnu Chudaka
| lyrics3 = Kulasekhar Hariharan
| length3 = 5:13

| title4  = Godaralle Ponge
| lyrics4 = Kulasekhar SP Balu
| length4 = 4:25

| title5  = O Lolly Popki Chandrabose 
| extra5  = Shankar Mahadevan
| length5 = 5:37

| title6  = Jampanduve
| lyrics6 = Veturi Sundararama Murthy   Sujatha
| length6 = 4:58

| title7  = O Jabili
| lyrics7 = Kulasekhar  Chitra
| length7 = 5:16

| title8  = Ammo Ammayena (Bit)
| lyrics8 = Kulasekhar Hariharan 
| length8 = 0:52

| title9  = Gaali Chirugaali (Bit) Sirivennela Sitarama Sastry  Chitra
| length9 = 1:28
}}

==Special Appearance==
V.V.S.Laxman,a former India cricketer had played a guest role in this movie.In a scene Laxman appears as himself,an already existing player for Indian national team giving advices and tips to the Andhra Ranji team player Ashok(Venkatesh) during practice sessions and who aspires to play for the India team. 

==Box-office performance==
* The film had a 50-day run in 139 centres and also completed 100 days in 71 centers.Its big hit of 2003.

==Awards==
* Nandi Special Jury Award - N.V.Prasad
* Nandi Award for Best Costume Designer - P.Rambabu

==References==
 

==External links==
*  

 

 
 
 


 