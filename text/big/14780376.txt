Enteng Kabisote: Okay ka, Fairy Ko: The Legend
{{Infobox Film
| name           = Enteng Kabisote: Okay Ka, Fairy Ko: The Legend
| image          = Enteng kabisote.jpg
| caption        = DVD cover
| director       = Tony Y. Reyes
| producer       = Orly R. Ilacad                               
| writers        = Isabel Da Rosa and Bibeth Orteza
| starring       = Vic Sotto  Kristine Hermosa    
| music          = Jan K. Ilacad
| cinematography = 
| dustributor       = OctoArts Films
| studio    = OctoArts Films and M-Zet Productions
| released       = December 25, 2004
| runtime        = 115 minutes
| country        = Philippines
| awards         =  English Tagalog Tagalog Filipino Filipino
| budget         = P 35 million 
| gross          = P 101.6 million(2 WEEK MMFF) P 135 million (4 weeks)
}}
Enteng Kabisote: Okay Ka, Fairy Ko: The Legend is the first sequel of Enteng Kabisote films. a   (2006), and   (2007).

== Plot == Queen of Engkantasya, an enchanted kingdom. The couple has two children, Aiza (Aiza Seguerra) and Benok (Oyo Boy Sotto). The family is living peacefully and happily until the day Satana (Bing Loyzaga), the evil queen of Kadiliman brings chaos to Earth by sending Romero (Jeffrey Quizon), Lucy (Nikki Arriola), and Fer (Levi Inacio) to poison the water of the dams. But Venuz (Leila Kuzma) and Aries (Patrick Alvarez), armor fairies sent by Ina Magenta stopped them. 

Satana turns her ire to Enteng’s family. She sends Itim (Michael V.), to spy on Enteng and Faye. But because of the family’s goodness to him, Itim deceived and lied to Satana. In turn, Satana transforms herself into a young girl named Tanny (Nadine Samonte), who seduces and possesses Benok but Enteng manages to stop her evil deed in time. Burning with anger, Satana kidnaps Faye. She demands that Ina Magenta surrenders her good powers so she can rule the earth. Instead, Ina sends Enteng and Benok together with Itim (transformed into a talking flying horse) to fight Satana. After a numerous adventures and comic fights, they manage to rescue Faye.

== Cast ==
* Vic Sotto as Enteng
* Kristine Hermosa as Faye
* Giselle Toengi as Ina Magenta
* Aiza Seguerra as Aiza
* Oyo Boy Sotto as Benok
* Bing Loyzaga as Satana/Amarillo
* Bayani Casimiro II as Prinsipe K
* Joey de Leon as Mulawit/Pugo
* Jose Manalo as Jose
* Jeffrey Quizon as Romero
* Ruby Rodriguez as Amy

==References==
 

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- Enteng Kabisote 2: Okay Ka Fairy Ko: The Legend Continues
|- 2005
| rowspan="1" align="left"| Metro Manila Film Festival Best Supporting Actor
| align="center"| Jose Manalo
|  
|}

==References==
 

==External links==
*  
* 

 

 
 
 
 
 