World in My Corner
{{Infobox film
| name           = World in My Corner
| image          =
| caption        =
| director       = Jesse Hibbs
| producer       = Aaron Rosenberg
| writer         = Jack Sher Joseph Stone
| narrator       =
| starring       = Audie Murphy Barbara Rush
| music          = Henry Mancini Heinz Roemheld
| cinematography = Maury Gertsman
| editing        = Milton Carruth
| studio         = Universal International
| distributor    =
| released       =  
| runtime        = 82 mins
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
World in My Corner is a 1956 film starring Audie Murphy as a boxer. It was one of the few non-Westerns Murphy made in his career. 

==Plot==
Tommy Shea (Audie Murphy), a boxer from Jersey City, is sponsored by millionaire Robert Mallinson (Jeff Morrow). He falls for Mallinsons daughter, Dorothy (Barbara Rush) and decides to work for crooked fight promoter Harry Cram to earn the money to keep her in the style to which she has become accustomed.

==Cast==
 
 
*Audie Murphy as Tommy Shea
*Barbara Rush as Dorothy Mallinson
*Jeff Morrow as Robert T. Mallinson
*John McIntire as Dave Bernstein
*Tommy Rall as Ray Kacsmerek
*Howard St. John as Harry Cram
 
*Chico Vejar as Al Carelli
*Steve Ellis as TV announcer
*Art Aragon as fighter
*Dani Crayne as Doris
*James F. Lennon as ring announcer
*Cisco Andrade as Parker
 

==Production== To Hell and Back (1955) and used the same producer and director as that film. Murphy fights with several real life boxers on screen, including Chico Vejar, Art Aragon and Cisco Andrade. Audie Murphys a Prize Fighter in New Loop Movie, 
Chicago Daily Tribune (1923-1963)   Mar 11, 1956: j11.   Andrade later praised Murphy as being "the first actor I ever saw who wasnt afraid of getting hit hard in a prize fight scene." Don Graham, No Name on the Bullet, N.Y.: Viking, 1989 p263 

==Response==
According to Murphys biographer, the film "didnt do anything at the box office". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 