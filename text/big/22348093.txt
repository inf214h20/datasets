Nelly's Folly
 
{{Infobox Hollywood cartoon
|cartoon_name=Nellys Folly
|series=Merrie Melodies 150px
|caption=Screenshot of Nellys Folly title card
|director=Chuck Jones Co-Direction Abe Levitow Maurice Noble Richard Thompson Ben Washam
|story=Chuck Jones David Detiege
|background_artist=Philip DeGuard
|voice_actor=Gloria Wood (Nelly) Mel Blanc (Turtle, Male Giraffe as he calls Nelly a "has-been") Ed Prentiss (Narrator & Agent, uncredited)
|musician=Milt Franklyn
|producer=Warner Bros. Pictures
|studio=Warner Bros. Cartoons
|distributor=Warner Bros. Pictures, 1961 Warner Home Video, 1992
|release_date=December 30, 1961 (USA)
|color_process=Color
|runtime=7:23
|movie_language=English
}}
Nellys Folly is a Merrie Melodies cartoon short, released in 1961, which was written and directed by Chuck Jones. A singing giraffe leaves the jungle to pursue a singing career, but finds herself lonely and out of work following an affair.

==Plot==
The cartoon opens as a camera pans across the continent of Africa where the narrator describes how dark and terrifying it is, amid jungle sounds and roars. As the camera pans across the darkest area, we hear the melodious sound of someone singing. A second later the camera opens on a singing giraffe named Nelly, who is performing for her animal friends. A hunter appears from out of the bush, surprised at the sight of this singing giraffe and immediately has her sign a contract offering her fame and fortune.

Nelly waves a tearful goodbye to her friends in the jungle as she leaves for civilization, captivated by the idea of show business. Once she arrives in New York City, she is put to work singing jingles for "Algonquin Rutabaga Tonic" - a cure for ailments, puts on live stage shows, and produces a line of giraffe-neck sweaters. The camera closes in on a turtle reading a magazine article on the giraffe-neck clothing. He turns to the camera and says, "Well, thats show business".

Nelly releases several albums, but over time becomes lonely with fame and longs for male companionship. One day she wanders into the Zoo and falls in love with a male giraffe, but she finds out hes already married (albeit unhappily, as the "wife" catches him looking at her).  Scandal ensues and her agent tells her shes ruining her career (reduced to performing in empty opera houses and taking roles in foreign movies). She leaves show business to go back to her lover, who is back together with his wife and wants nothing to do with a has-been celebrity. A devastated Nelly decides to return to Africa.

Back in Africa, Nelly is seen singing a beautiful love song, her sad reflection in a pond, tears dripping from her eyes, and from the eyes of her jungle friends. Moments later, another male giraffe begins singing along with her. The two fall in love and the cartoon ends.

==Trivia==
* Nominated for an Academy Award in 1962, for Best Short Subject, Cartoons.
* Ed Prentiss, the narrator of this film, used to be "Captain Midnight" on radio in the 1940s.
* Ed Prentiss also narrated Chuck Jones "Martian Through Georgia" 1 year later.
* Gloria Wood sang the original "Rice-A-Roni, The San Francisco Treat" TV jingle.
* Gloria Wood was also the vocalist for Kay Kysers "The Woody Woodpecker Song" and was dubbed the singing voice of Marilyn Monroe, Vera Ellen, and Betty Grable.
* Nellys Folly did not end with the familiar "Thats all, folks!" title card, but rather, with a slide that read: "Merrie Melodies: A Warner Bros. Cartoon. A Vitaphone Release". These words appeared multi-colored against a black background. No music accompanies this.

==Music==
* "Auld Lang Syne"
* "The Flower of Gower Gulch" from Drip-Along Daffy, written by Michael Maltese Johann Strauss
* "Aloha Oe", by Queen Liliuokalani
* "Columbia, Gem of the Ocean", aka "The Red, White and Blue"
* "Then Youll Remember Me", from Balfes opera "The Bohemian Girl"

==Home Video==
"Nellys Folly" is included on Looney Tunes Platinum Collection Volume Three.

==External links==
*  

 
 
 
 
 
 
 
 
 