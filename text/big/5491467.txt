L'Arbre, le maire et la médiathèque
{{Infobox film
  | name = LArbre, le maire et la médiathèque
  | image = 
  | caption = 
  | director = Éric Rohmer
  | producer = Françoise Etchegaray
  | writer = Éric Rohmer
  | starring  = Pascal Greggory Arielle Dombasle Fabrice Luchini
  | music = Sébastien Erms
  | cinematography = Diane Baratier
  | editing = Mary Stephen
  | distributor = 
  | released = 1993
  | runtime = 105 min. French
  | budget = 
}}

LArbre, le maire et la médiathèque ou les sept hasards is a French film directed by Éric Rohmer.  It was shown at the 1993 Montreal World Film Festival where it received the FIPRESCI prize.  It is known variously by the alternate title Les Sept hasards and in English translation as The Tree, the Mayor and the Mediatheque.

==Synopsis==
In Saint-Juire-Champgillon, a village in Vendée, a young socialist mayor decides to build a media library.

==External links==
* 

 

 
 
 
 

 