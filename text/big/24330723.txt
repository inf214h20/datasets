The Disappearance of Alice Creed
{{Infobox film
| name           = The Disappearance of Alice Creed
| image          = Disappearance of alice creed UK poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = J Blakeson
| producer       = Adrian Sturges
| writer         = J Blakeson
| starring       = Gemma Arterton Martin Compston Eddie Marsan
| music          = Marc Canham
| cinematography = Philipp Blaubach
| editing        = Mark Eckersley
| studio         = Isle of Man Film CinemaNX
| distributor    = West End Films
| released       =   
| runtime        = 96 minutes 
| country        = United Kingdom
| language       = English
| budget         = $1,500,000 
| gross          = £554,832 
}} thriller film about the kidnapping of a young woman by two ex-convicts. The film is written and directed by J Blakeson and stars Gemma Arterton as the captured Alice Creed, with Martin Compston and Eddie Marsan as Danny and Vic, the kidnappers.    The film was shot on the Isle of Man.

==Plot==
Victor and Danny are lovers who met in prison and hatched a plan to kidnap and demand ransom for the only child of a rich family. The film opens with scenes in which they make meticulous preparations to carry out the kidnap. They take their victim, Alice, her head hooded, to a soundproofed room. They cut off her clothing and tie her to a bed with a ball-gag buckled into her mouth. They force her to look at a camera as she is photographed naked. They then dress her in a tracksuit, put a hood on her head and leave her. They bag up both her and their clothes for disposal and Vic sends the pictures to her father as first step towards making the ransom demand. In continuing graphic scenes, they explain to Alice that she needs to sign when she needs the toilet and strip her lower half in order that she use a bed pan whilst still tied up. Neither shows any emotion during her humiliation but Vic, who is dominant, questions Dannys resolve now that the formerly hypothetical girl has become real.

Vic is the one who leaves the place where they are holding Alice in order to make preparations to obtain the ransom, whilst Danny is left guarding Alice.  In his absence, Alice, signing that she needs to do a number two. persuades Danny to untie her and turn his back. She attacks him with her poo bucket and grabs his pistol, which she fires during the struggle between them, hitting the wall. Believing he is about to die, Danny reveals his identity to Alice. They were lovers before his time in jail and he says he met Vic while in jail and chose her as the kidnap victim with a view to getting money from the father she had told him she hated. He says his plan was to double cross Vic and to start a new life, sharing the money with Alice. They hear Vic returning and Danny points out that even if she were to shoot him she would not get past Vic. Alice agrees to play along with Dannys plan and lets him tie her back up. Later, while Vic is away again, Alice gets Danny to untie her and seduces him, managing to handcuff Danny to the bed. She tries to leave but finds the front door bolted from the inside and cant get out. She sees a mobile phone on the table and dials 999 but cannot tell the operator where she is. She then notices Dannys gun and decides to use it to get him to tell her where the keys to the front door are. Danny tells her they are in his trouser pocket, which is close enough to the bed for him to overpower her whilst she is attempting to retrieve them. He reties the unconscious Alice to the bed. Vic returns and says the exchange is on. Vic is left alone with Alice while Danny prepares the van. As Vic starts to get Alice ready for the trip, the mobile phone she managed to obtain falls out of her pocket onto the bed. Vic checks it and finds it shows a 999 call. Then he spots the bullet in the wall, ungags Alice and threatens her. She screams for Danny, proving she knows him. She then tells Vic that Danny intended to double cross him and that she had done a deal with Danny to play along as kidnap victim in exchange for a share of the money.

Vic is shocked at Dannys betrayal. When Danny returns, Vic gives Danny a chance to admit something was amiss by saying he feels something is not right. But Danny does not reveal anything to Vic. They inject Alice with something to put her temporarily to sleep and move her to a deserted, rural warehouse, where they chain her up in a back room. Vic asks Danny for his set of keys to the locks and then drives him to to a forest where he says they are to pick up the ransom. There, Vic confronts Danny about his betrayal, saying he has consigned both himself and the Alice to death through it. He says he now intends the hole they had dug for the ransom to be for Dannys body. Danny flees and Vic shoots at him, wounding him. Danny manages to get away and hide.  Vic then retrieves the ransom elsewhere and returns to get Alice. He tries to give her an injection, as he has done previously in order to render her unconscious whilst moving her, but the wounded Danny returns as he is doing so, overcomes him and gets his gun. In a stand off between the two men, Vic reveals to Alice that they were lovers. He attempts to use the bond between them to persuade the fatally wounded Danny to put the gun down and be taken to hospital. Danny shoots Vic point blank and then, to Alices horror, leaves the room, switching off the light on his way out. She is still handcuffed to railings and Vic appears to be dead. However, he revives sufficiently to throw keys to Alice. She manages to unlock her fetters and stagger out through the deserted warehouse. Outside, she finds Danny has not got far. The car he was leaving in is a short distance up the road, the ransom money on the passenger seat and Dannys dead body in the drivers seat. Alice opens the car door and drags his body out. The radio is still playing. She drives away.

==Cast==
* Gemma Arterton as Alice Creed
* Martin Compston as Danny
* Eddie Marsan as Vic

==Release==
The film was screened at the 2009   in 2010.
 Southampton University Students Union won the event, which took place on 20 April 2010. 

===Critical reception===
Review aggregator   (TIFF). Cameron Bailey, co-director of TIFF,  praises J Blakesons directorial style, claiming that "Not since Reservoir Dogs has a hostage standoff been handled with such intelligence". 

The film was nominated for the Raindance Award at the 2009 British Independent Film Awards. 

==Home media==
The film was released on DVD in the UK on 4 October 2010. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 