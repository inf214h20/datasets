Boots! Boots!
  British comedy George Formby, Beryl Formby and Arthur Kingsley. It was made by Blakeleys Productions, Ltd. (later Mancunian Films) at the Albany Studios in London.  The premiere of the film was in Burslem, Stoke-on-Trent.      

Producer John E. Blakeley had no prior experience in film production; he had seen comedian George Formby doing his stage act and approached him to star in a feature film. Blakeleys modest studio was a one-room loft space above a taxi garage. The makeshift stage was not soundproofed, so whenever the crew members wanted to film a scene, they had to signal the garage to stop its noisy work below. The studio also had pictorial limitations, and couldnt replicate much more than simple room interiors (many scenes were staged in cramped corners). Thus the nightclub scenes in Boots! Boots! were filmed in near-darkness, hiding the absence of set decorations, with a single spotlight trained on the performer being photographed.

The film is a patchwork of songs and jokes tied to the misadventures of bumbling John Willie, played by Formby. ("John Willie" was a character made famous by Formbys father, George Formby, Sr., in music halls of the early 1900s.) Despite the crude photography and recording, and the minuscule budget of £3000, Boots! Boots! became an enormous hit. It was reissued (in shortened form, in 1938) to capitalize on Formbys later fame; for six decades this 55-minute version was the only one that circulated, until an uncut, 80-minute print was located and restored for DVD release.

==Cast== George Formby - John Willie Beryl Formby - Snooky
* Arthur Kingsley - Hotel Manager
* Tonie Forde - Chambermaid
* Lilian Keyes - Lady Royston
* Donald Read - Sir Alfred Royston
* Constance Fletcher - Mrs Clifford
* Betty Driver - Betty
* Wallace Bosco - Mr Clifford
* Myfanwy Southern - Reception Clerk
* Bert Tracey - The Chef
* Harry Hudson and his Orchestra - Themselves

==References==
 

==Bibliography==
* Richards, Jeffrey. The Age of the Dream Palace. Routledge & Kegan, 1984.

==External links==
* 

 
 
 
 
 


 