Young Goethe in Love
{{Infobox film
| name = Young Goethe in Love
| image = YoungGoetheInLove2010Poster.jpg
| alt = 
| caption = Film poster
| director =  
| producer = Christoph Müller Helge Sasse
| writer = Philipp Stölzl Alexander Dydyna Christoph Müller
| starring = Alexander Fehling Miriam Stein Moritz Bleibtreu
| music = Ingo L. Frenzel
| cinematography = Kolja Brandt
| editing = Sven Budelmann
| studio = Seven Pictures Warner Bros. Film Productions Germany
| released =  
| runtime = 102 minutes
| country = Germany
| language = German
| budget = €3 million
| gross = 
}}
Young Goethe in Love (originally titled Goethe!) is a 2010 German historical drama film directed by   and starring Alexander Fehling, Miriam Stein, and Moritz Bleibtreu. It is a fictionalized version of the early years of the poet Johann Wolfgang von Goethe and the events forming the basis of his novel The Sorrows of Young Werther.

==Cast==
* Alexander Fehling - Johann Wolfgang Goethe
* Miriam Stein - Charlotte Buff
* Moritz Bleibtreu - Albert Kestner
* Volker Bruch - Wilhelm Jerusalem
* Burghart Klaußner - Vater Buff
* Henry Hübchen - Johann Kaspar Goethe - Vater
* Hans-Michael Rehberg - Gerichtspräsident Kammermeier

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 