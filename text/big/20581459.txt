Hari Darshan
{{Infobox film
| name           = Hari Darshan
| image          = 
| image_size     = 
| caption        = 
| director       = Chandrakan
| producer       = 
| writer         =
| narrator       = 
| starring       = Dara Singh  
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    =  1972
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1972 Bollywood religious film directed by Chandrakan. The film stars Dara Singh & other artists of Indian film Industry, who portray different characters during the time of Bhakt Prahlad.
The story revolves around the devotee Prahlad. It also show cases different Avtars (incarnations) of lord Shri Hari Vishnuji.

==Cast==
*Saroja Devi B. ...  Kayadu 
*Randhawa ...  Lord Hiranyakaship  Master Satyajeet ...  Prahlad  Mehmood   
*Mehmood Jr. ...  Jamure 
*Jayshree Gadkar ...  Devi Lakshmi 
*Dara Singh ...  Bhagwan Shiv  Sujata ...  Holika 
*Abhi Bhattacharya ...  Bhagwan Vishnu

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jai Jai Narayan"
| Lata Mangeshkar
|-
| 2 
| "Apna Hari Hai Hazar Haathwala"
| Lata Mangeshkar
|-
| 3
| "Idhar Bhi Ishwar Udhar Bhi"
| Sushma Shrestha|Poornima, Hemlata, Kamal Barot  
|-
| 4
| "Jai Jai Narayan"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 5
| "Karo Hari Darshan"
| Mahendra Kapoor
|-
| 6
| "Maarnewala Hai Bhagwan"
| Lata Mangeshkar
|-
| 7
| "Prabhu Ke Bharose"
| Lata Mangeshkar
|-
| 8
| "Hari Naam Dhun"
| Lata Mangeshkar
|}
==External links==
*  
 
 

 
 
 
 
 