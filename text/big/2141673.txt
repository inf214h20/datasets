The Man Who Saw Tomorrow
{{Infobox film | name = The Man Who Saw Tomorrow
  | image =The Man Who Saw Tomorrow.jpg
  | caption =
  | director = Robert Guenette	
  | producer = Paul Drane Alan Goland (associate producer) Robert Guenette Lee Kramer David L. Wolper (executive producer) Peter Wood (associate producer)					
  | writer = Nostradamus (book) Robert Guenette Alan Hopgood
  | starring =Orson Welles (Presenter/Narrator)
  | music = William Loose Jack Tillar
  | cinematography =
  | editing = Scott McLennan Peter Wood
  | distributor = Warner Bros. 1981 (U.S. release)
  | runtime = 90 min
  | language = English
  | budget =
  }}
  French astrologer and physician Michel de Notredame (Nostradamus).

The Man Who Saw Tomorrow is narrated (one might also say "hosted") by Orson Welles. The film depicts many of Nostradamus predictions as evidence of Nostradamus predicting ability, though as with other works, nothing is offered which conclusively proves his accuracy.  The last quarter of the film discusses his Nostradamus supposed prediction for the then future of the 1980s, 1990s and beyond. There are no scientifically testable predictions directly included in this film, only suggestions and allusions.

== Welles view ==

Welles, though he agreed to host the film, was not a believer in the subject matter presented.  Welles main objection to the generally accepted translations of Nostradamus quatrains (so called because Nostradamus organized all his works into a series of four lined prose, which were then collected into "centuries", or groups of 100 such works) relates in part to the translation efforts.  While many skilled linguists have worked on the problem of translating the works of Nostradamus, all have struggled with the format the author used.
 Italian and Greek language|Greek).  Not content with such obfuscation, Nostradamus is also said to have used anagrams to further confuse potential inquisitors (particularly with respect to names and places).
 other projects more interesting to him personally.

==Alleged Nostradamus predictions in The Man Who Saw Tomorrow==
 King Henry Henry II of France (1559).
* The French Revolution (1789–1799). rise and fall of Napoleon Bonaparte (1799–1815).
* The American Revolution (1775–1783). assassination of President of the United States|U.S. President Abraham Lincoln (1865). rise and fall of Adolf Hitler (1933–1945).
* World War II (1939–1945).
* The Holocaust. Nagasaki (1945). conspiracy to assassinate U.S. President John F. Kennedy (1963). assassination of Robert F. Kennedy (1968).
* Islamic Revolution of Iran (1979).
* The 1980s could be the time for Ted Kennedy to run to the U.S. Presidency, after Chappaquiddick incident (1969).  Eventually, Kennedy ran for the Presidency in 1980, when he tried to defeat incumbent President Jimmy Carter in the Democratic Party primaries. Inventions and technological advances. rivalry between the Soviet Union and the United States ("One day, the two great masters would be friends...The eastern ruler would be vanquished" but which could just as easily have referred to the Holy Roman Empire and the Ottoman Empire).
* A major earthquake striking Los Angeles in May 1988. Greater Arabia during the late 1990s, wage war around the world and spread the influence of Islamic fundamentalism, along with the decreasing  influence of Christianity. Nostradamus claims that the "King of Terror" would form an alliance with Russia.  According to Nostradamus, the "King of Terror" and Russia would wage World War III against the Western world (United States, United Kingdom and France), starting with a nuclear strike on New York City ("the sky will burn at 45 degrees, fire approaches the great new city").  Nostradamus claims that World War III would last about 27 years, and the war would destroy cities and kill millions.  The "King of Terror" would be defeated.
* After World War III, there will be a "peace of a thousand years". End of the world would be the year 3797 A.D.

== 1991 Remake ==

On February 20, 1991, during the Gulf War, NBC aired a remake of this film, hosted and narrated by Charlton Heston.  Much of the same footage, voice acting, and musical score was retained from the original movie.  Much of Hestons narration constituted a verbatim reprise of Welles original presentation.  There were, however, some significant differences between the 1991 remake and the original film:

* Much of the religious content was omitted.  The three Antichrists became three "great despotic tyrants", and the religious dimensions of the third tyrants war were eliminated.
* The remake was shortened to one hour, leading to the omission of all references to the Robert F. Kennedy assassination, Francisco Franco, the Edward VIII abdication crisis,  Jeanne Dixon, and to allegations surrounding the disinterment of Nostradamus in 1791.  Discussion of the remaining predictions was abridged. Edward Kennedy presidency in 1984, or any time thereafter, references to his possible presidency were eliminated.
* The third tyrants war, and the tragedies preceding that war, were made to seem less severe than in Welles original documentary.
* Saddam Hussein was implied to be the third tyrant.
* The remake discussed the 1989 Loma Prieta earthquake, alleging that this earthquake may have been the one predicted in Welles movie.
* Discussion of Nostradamus alleged predictions was rearranged, in that the remake included discussion of Napoleon with the other two tyrants, rather than with the French Revolution. end of the world were eliminated.

The nature of the overall changes served the purpose of brevity, reducing an eighty-eight minute film to a one-hour television broadcast with commercial interruptions.  Moreover, the predictions relating to the third tyrant were adapted to serve U.S. propaganda purposes during the then-ongoing Gulf War.  The severity of the natural disasters preceding the tyrants emergence was reduced, as a worldwide famine leading to cannibalism, and earthquakes and flooding in various European cities, had clearly not taken place.  Similarly, the severity of the third tyrants war was diminished, omitting references to the conquest of Europe, nuclear war, and the destruction of New York City, as well as the prediction that the war would begin in 1994.  Significantly, negative references to Islam, and references to the religious nature of the tyrant (calling him an Antichrist and a "strong master of Mohammedan law") and his war were eliminated, avoiding offense to Muslims, as well as the portrayal of the then-current conflict as a religious war.

== External links ==

* 

 
 
 
 
 
 