The Caine Mutiny (film)
 
{{Infobox film 
|  name               = The Caine Mutiny 
|  image              = Mutiny_0.jpg 
|  caption            = original film poster
|  director           = Edward Dmytryk 
|  producer           = Stanley Kramer 
| based on       =    Stanley Roberts Michael Blankfort Robert Francis
|  music              = Max Steiner
|  cinematography     = Franz Planer
|  editing            = Henry Batista William A. Lyon
|  distributor        = Columbia Pictures 
|  released           =   
|  runtime            = 124 minutes 
|  country            = United States
|  language           = English
|  budget             = $2 million Charles Tranberg, Fred MacMurray: A Biography, Bear Manor Media, 2014 
|  gross              = $21,800,000 
}} 1951 Pulitzer Pulitzer Prize-winning novel written by Herman Wouk. The film depicts the events on board a fictitious World War II U.S. Navy destroyer minesweeper, the USS Caine (DMS-18), and the subsequent court-martial of two officers.
 Oscar nominations for Best Picture, Best Actor (Humphrey Bogart), Best Supporting Actor (Tom Tully), Best Screenplay, Best Sound Recording (John P. Livadary), Best Film Editing, and Best Dramatic Score (Max Steiner).    It was the second highest-grossing film in the United States in 1954. The Top Box-Office Hits of 1954, Variety Weekly, January 5, 1955 
 Directors Guild Award for Outstanding Directorial Achievement in Motion Pictures.

==Plot== Robert Francis), reports for his first assignment aboard the Caine, which is homeported in Pearl Harbor. "Willie" as he known by his fellow officers, is disappointed to find that the Caine is a small, battle-scarred destroyer-minesweeper. Its gruff captain, Lieutenant Commander William H. DeVriess (Tom Tully), has almost completely given up on discipline, and the crew has become slovenly and superficially undisciplined – although their performance is excellent. Keith has already met the executive officer, Lieutenant Stephen Maryk (Van Johnson), and is introduced to the communications officer, Lieutenant Thomas Keefer (Fred MacMurray), a novelist in civilian life. Keith has an opportunity to transfer from the ship to a more glamorous billet as an Admirals aide, but decides to stay aboard. However, Keith remains frustrated with the ships lack of discipline, despite his own shortcomings.

The captain is soon replaced by Lieutenant Commander Phillip Queeg (Humphrey Bogart), a no-nonsense career veteran who immediately attempts to instill discipline into the crew. At first, Keith is relived to have a strict commander but he soon gets concerned about Queegs erratic behavior which includes fumbling with a pair of steel balls. 

The next day, the Caine is assigned to tow a target for gunnery practice. Queeg is distracted berating Keith and Keefer over a crewmans appearance after giving an order to turn, and he cuts off the helmsmans attempt to warn him that they are turning back on their own towline. After the Caine continues in a circle and cuts the towline, Queeg tries to cover up his responsibility for the incident.

Other incidents serve to undermine Queegs authority.  Under enemy fire, Queeg abandons escorting a group of landing craft during an amphibious assault long before they reach the fiercely defended shore, instead dropping a yellow dye marker in the water and leaving the landing craft to fend for themselves, much to the crews disgust. Afterward, Queeg speaks to his officers, not explicitly apologizing, but bending enough to ask for their support. His disgusted subordinates do not respond.

When strawberries go missing from the officers mess, the captain, once again clicking the steel balls, goes to absurd lengths to hunt down the culprit. Despite being told by one of his officers that the mess staff had eaten the fruit, Queeg insists otherwise and proudly tells Maryk and Keefer that as an ensign he was commended for unmasking a cheese thief.

Keefer begins trying to convince Maryk that he should relieve Queeg on the basis of mental illness under Article 184 of Navy Regulations. At first Maryk displays loyalty to Queeg, even threatening Keefer with a charge of insubordination. Maryk begins keeping a journal, documenting Queegs behavior. Keefer convinces Maryk and Keith to join him in presenting the case to Admiral William F. Halsey, Jr.. While aboard Halseys flagship, Keefer realizes that Queegs documented actions could be interpreted as reasonable attempts to instill discipline, leaving the officers open to a charge of conspiring to mutiny. When Halseys aide tells the Caine officers that Halsey will see them, Keefer talks Maryk and Keith out of making the complaint.

Matters come to a head during a violent typhoon. Maryk urgently recommends that they steer into the waves and take on ballast, but Queeg refuses to deviate from the fleet-ordered heading and declines Maryks request for ballast. He fears that it would foul the fuel lines with salt water. When Queeg appears to become paralyzed in action, Maryk relieves him, with Keiths support.
 
Upon returning to port, Maryk and Keith face a court-martial for mutiny. After questioning them and Keefer, Lieutenant Barney Greenwald (José Ferrer) reluctantly accepts the job of Maryks defense counsel, which a number of other lawyers have already turned down.

The proceedings do not go well, as Keefer has carefully covered himself and denies any complicity. Navy psychiatrist Dr. Dixon (Whit Bissell) testifies that Queeg is not mentally ill, but when Queeg is called to testify, he exhibits paranoid behavior and begins to click the ball bearings together in his hand under Greenwalds tough cross-examination. Maryk is acquitted, and Keith is spared any charges.

The Caine officers celebrate the trials results at a hotel. Keefer shows up, telling Maryk privately he did not have the guts not to. Greenwald appears and clears his "guilty conscience". Apparently drunk, he berates the officers for not appreciating the years of danger and hardship endured by Queeg, a career Navy man. He lambastes Maryk, Keith, and finally Keefer, for not supporting their captain when he most needed it. Under his attack, Maryk and Keith admit that if they had given Queeg the support he had asked for, he might not have "frozen" during the typhoon.

Greenwald turns to Keefer, denouncing him as the real "author" of the mutiny, who "hated the Navy" and manipulated the others while keeping his own hands officially clean. The lawyer exposes Keefers double-dealing to the other officers, throws a glassful of champagne in his face and tells Keefer that if he wishes to do anything about the drink in the face, the two could fight outside. Keefer does not respond to the challenge and the other officers depart, leaving him alone in the room.

A few days later, Keith has received his promotion to Lieutenant, junior grade and reports to his new ship. He is surprised to find he is serving again under DeVriess, who has been promoted to Commander. DeVriess lets Keith know that he will start with a clean slate.

==Cast==
 
*Humphrey Bogart as Lieutenant Commander Philip Francis Queeg
*José Ferrer as Lieutenant Barney Greenwald
*Van Johnson as Lieutenant Steve Maryk
*Fred MacMurray as Lieutenant Tom Keefer Robert Francis as Ensign (later Lieutenant, junior grade) Willis Seward "Willie" Keith
*May Wynn as May Wynn
*Tom Tully as Lieutenant Commander (later Commander) (William H.) DeVriess
*E. G. Marshall as Lieutenant Commander (John) Challee, the prosecutor
*Arthur Franz as Lieutenant, junior grade, H. Paynter Jr.
*Lee Marvin as Seaman / Petty Officer "Meatball" 
*Warner Anderson as Captain Blakely, president of the court-martial
*Claude Akins as Seaman "Horrible" (Everett Black)
*Katherine Warren as Mrs. Keith, Ensign Keiths mother
*Jerry Paris as Ensign Barney Harding
*Whit Bissell as Navy psychiatrist Lieutenant Commander Dickson, Medical Corps Kenneth MacDonald as a court martial board member (uncredited)
*James Best as Lieutenant Jorgensen (uncredited) Johnny Duncan as radioman
*Todd Karns as Petty Officer Stillwell, the helmsman  (uncredited)
*Don Dubbins as Seaman 1st Class Urban (uncredited)
* May Wynns song "I Cant Believe That Youre In Love With Me" sung by Jo Ann Greer
 
Cast notes Burbank airport in California.

==Production==

===Casting and director=== Grant or Clark Gable|Gable, but always to me." Scott McGee    During shooting, Bogart was already suffering from the earliest symptoms of the throat cancer that would eventually kill him. Bogart had served in World War I as a Petty Officer in the US Navy, as Chief Quartermaster of the USS Leviathan. 

Lee Marvin was cast as one of the sailors, not only for his acting, but also because of his knowledge of ships at sea. Marvin had served in the U.S. Marines from the beginning of American involvement in World War II through the Battle of Saipan, in which he was wounded. As a result, he became an unofficial technical adviser for the film. 
 Cyrano de Ship of Fools, the only one directed by Kramer.
 American Communist Hollywood Ten and was blacklisted. In 1951 Dmytryk chose to testify to HUAC about his brief Party past, as well as pressure by other party members to put Communist propaganda into his films. In a second appearance before the House Committee, he identified 26 Party members. This allowed him to work again in the film industry.

===Script=== Stanley Roberts, an experienced screenwriter. Roberts later quit the production after being told to cut the screenplay so the film could be kept to two hours. The 50 pages worth of cuts were made by Michael Blankfort, who received an "additional dialog" credit. 

====Differences from novel====
 
The character and background of Ensign Keith are more thoroughly explored in the novel, including midshipman school and his early relationship with his girlfriend May Wynn. After the court-martial, he returns to the Caine. He is seen to develop into a mature, competent Naval officer, which the film only suggests.

In the novel, Captain Queeg is roughly thirty years old at the time of the mutiny. Bogart  was fifty-five at the time of filming, and the script gave him backstory to try to fill in the years. In the original novel and stage play, Greenwald is characterized as Jewish, who more than the other officers believed in the Navys importance in keeping the German Nazis far from America. His beliefs contributed to his sympathy for Queeg and contempt for the junior officers who simply signed on for the duration of the war.

===Navy involvement=== Navy initially objected both to the portrayal of a mentally unbalanced man as the captain of one of its ships and the word "mutiny" in the films title. After the script was altered, the Navy cooperated with Columbia Pictures by providing ships, planes, combat boats, and access to Pearl Harbor and the port of San Francisco for filming. Following the opening credits, the epigraph states that the films story is non-factual. It notes there has not been a ship named USS Caine, and no Navy captain has been relieved of command at sea under Articles 184–186: "There has never been a mutiny in a ship of the United States Navy. The truths of this film lie not in its incidents, but in the way a few men meet the crisis of their lives."
 Admiral Halseys fighter planes were placed atop the flight deck for the filming.   (the USS Jones of the Caine s commissioning plaque) had a similar fate in World War I. The Jacob Jones commanding officer, David Worth Bagley survived the sinking of his ship. The ship that Willis Keith conns out of port at the end of the film was the  .

===Shooting===
Studios did not want to purchase the film rights to Wouks novel until cooperation of the U.S. Navy was settled. TCM    Independent producer Stanley Kramer purchased the rights himself for an estimated $60,000 – $70,000. The Navys reluctance to cooperate led to an unusually long pre-production period of fifteen months. The Caine Mutiny went into production from 3 June to 24 August 1953, under the initial working title of Authority and Rebellion. TCM  , Turner Classics Movies (TCM) 
 Naval Station Treasure Island in San Francisco; Pearl Harbor, on Oahu in Hawaii, and at Yosemite National Park in California. The latter was the setting of the Yosemite Firefall and Keiths romantic interlude with May Wynn while on leave.

===Music=== Charles Gerhardt and the National Philharmonic Orchestra.

The lyrics of the song, "Yellowstain Blues," which mocked Queegs perceived cowardice during the landing incident, were written by Herman Wouk. They were drawn from The Caine Mutiny, the novel on which the film was based.

===Soundtrack=== LP release Broadway play dealing with the court-martial. He threatened to prohibit Columbia Pictures from making any further adaptations of his work. According to Wouk, "  Cohn looked into the matter, called me back, and said in his tough gravelly voice, Ive got you beat on the legalities, but Ive listened to the record and its no goddamn good, so Im yanking it." 
In October 2012 a copy sold on eBay for over $6,000. 

==Reception== theatrical rentals White Christmas, which earned $12 million in rentals. 

Director Edward Dmytryk felt The Caine Mutiny could have been better than it was. He thought the movie should have been three and a half to four hours long to fully portray all the characters and complex story, but Columbias Harry Cohn had insisted on a two-hour limit. 

In the 54 Best Legal Films of all-time,  The Caine Mutiny received one vote.

==Awards and honors== Oscar nominations for Best Picture, Best Actor (Humphrey Bogart, losing to Marlon Brando for On the Waterfront), Best Supporting Actor (Tom Tully), Best Screenplay, Best Sound Recording (John P. Livadary), Best Film Editing, and Best Dramatic Score (Max Steiner). 
 Directors Guild Award for Outstanding Directorial Achievement in Motion Pictures.

;American Film Institute Lists:
*AFIs 100 Years...100 Movies – Nominated 
*AFIs 100 Years...100 Heroes and Villains:
**Lt. Commander Philip Francis Queeg – Nominated Villain 
*AFIs 100 Years...100 Movie Quotes:
**"Ah, but the strawberries!  Thats, thats where I had them." – Nominated 
*AFIs 100 Years...100 Movies (10th Anniversary Edition) – Nominated 
*AFIs 10 Top 10 – Nominated Courtroom Drama 

==Influence==

===On actors===
*When Maurice Micklewhite first became an actor he adopted the stage name "Michael Scott". He was later told by his agent that another actor was already using the same name, and that he had to come up with a new one immediately. Speaking to his agent from a telephone box in Leicester Square in London, Micklewhite looked around for inspiration, noted that The Caine Mutiny was being shown at the Odeon Cinema, and adopted the name Michael Caine. Caine has often joked in interviews that, had he looked the other way, he would have ended up as "Michael One Hundred and One Dalmatians." 

===In television===
* Vince Gilligan used a clip of the film in Breaking Bad episode 5.02 ("Madrigal (Breaking Bad)|Madrigal", 2012), and has stated that The Caine Mutiny was one of his favorite movies as a child. 
 one episode, Holly is apparently replaced by a back-up computer called Queeg. Whereas Holly is sloppy and easy-going, Queeg is ruthless, authoritarian and by-the-book, bringing misery to the lives of the crew, in ways similar to Bogarts character.
 USS Jimmy Terminator that has been named Queeg by the crew. 

==See also==
*Trial movies
*Typhoon Cobra (1944), an actual typhoon that threatened U.S. warships under circumstances similar to those in the book.

==References==
 

==External links==
 
 
*  
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 