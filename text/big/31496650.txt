The Devil's Agent
The Devils Agent is a 1962 drama film directed by John Paddy Carstairs and starring Peter van Eyck, Marianne Koch, Christopher Lee and Macdonald Carey.  It was a co-production between Britain, West Germany and the Republic of Ireland and was an English-language version of the German film Im Namen des Teufels. It was based on a novel by Hans Habe. It is set in East Germany during the Cold War

==Partial cast==
* Peter van Eyck - Droste
* Marianne Koch - Nora
* Christopher Lee - Baron von Staub
* Macdonald Carey - Mr Smith
* Albert Lieven - Inspector Huebring
* Billie Whitelaw - Piroska
* David Knight - Father Zambory
* Marius Goring - General Greenhahn
* Helen Cherry - Countess Cosimano
* Colin Gordon - Count Dezsepalvy
* Niall MacGinnis - Paul Vass
* Eric Pohlmann - Bloch
* Peter Vaughan - Chief of Hungarian Police Michael Brennan - Horvat
* Jeremy Bulloch - Johnny Droste

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 