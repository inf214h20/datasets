The Green Berets (film)
{{Infobox film
| name           = The Green Berets
| image          = Green berets post.jpg Frank McCarthy
| border         = yes
| director       = {{plainlist|
* John Wayne
* Ray Kellogg
* Mervyn LeRoy (uncredited)
}}
| producer       = Michael Wayne
| screenplay     = James Lee Barrett
| based on       =  
| starring       = {{plainlist|
* John Wayne
* David Janssen
* Jim Hutton
* Aldo Ray
* Jack Soo
* Luke Askew Mike Henry
}}
| music          = Miklós Rózsa
| cinematography = Winton C. Hoch
| editing        = Otho Lovering
| studio         = Batjac Productions
| distributor    = Warner Bros.-Seven Arts (Worldwide, Theatrical) National Broadcasting Company (1972, TV) Warner-Columbia Film (1974, 1980) (Sweden, Finland) (theatrical) Fazer Musiikki Oy/Fazer Video (1984) (Finland) (VHS) Warner Home Video (Worldwide, DVD & VHS) Audio Visual Enterprises (1985) (Greece) (VHS) Scanvideo (Finland) (VHS)
| released       =  
| runtime        = 141 minutes
| country        = United States
| language       = {{plainlist|
* English
* Vietnamese
}}
| budget         = $7 million
| gross          = $21,707,027 
}} eponymous 1965 book by Robin Moore, though the screenplay has little relation to the book.
 President Johnson. To please the Pentagon, who were attempting to prosecute Robin Moore for revealing classified information, Wayne bought Moore out for $35,000 and 5 percent of undefined profits of the film. Moore, Robin Introduction to 1999 edition The Green Berets The Green Berets: The Amazing Story of the U.S. Armys Elite Special Forces Unit 2007 Skyhorse Publishing Inc. 

==Plot==
  Fort Bragg, Special Forces Green Beret" soldier killed in Vietnam), includes a demonstration and explanation of the whys and wherefores of participating in the Vietnam War.
 Communist Czechoslovakia, Communist China. Despite that, Beckworth remains skeptical about the value of intervening in Vietnams civil war. When asked by Green Beret Colonel Mike Kirby (John Wayne) if he had ever been to Southeast Asia, reporter Beckworth replies that he had not, prompting a discourteous acknowledgement of his opinion. Realizing his ignorance, Beckworth decides to go in-country to report on what he finds there so he may better his argument that America needs to stop participating in this unwinnable war.
 Montagnard soldiers while the other A-Team is to form a counter guerrilla Mike force. While selecting his teams, Kirby intercepts a Spc. Petersen (Hutton) from another unit who is scrounging supplies from Kirbys supply depot. Realizing Petersens skills, Kirby promotes him and brings him onto his SF team.

Arriving In South Vietnam, they meet Beckworth whom Kirby allows to join them at the basecamp where he witnesses the humanitarian aspect (irrigation ditches, bandages, candy for children) of the Special Forces mission. Still, he remains skeptical of the U.S.s need to be there. During that first evening, the unseen Viet Cong launch a harassing attack against the base camp by lobbing a few mortar shells before withdrawing, which results in little damage, but also results in the death of the American base camp commander Captain Coleman whom was one day from leaving back for the USA. During this period, Petersen befriends a young native boy named Ham Chuck, a war orphan who has no family other than his pet dog and the soldiers at the basecamp.

Also introduced is the ARVN base camp strike force leader Captain Nim (George Takei) whom was a former Viet Minh officer from Hanoi during the previous war and is now fighting for the anti-communist South Vietnamese government. He obsesses with having to "kill all the stinking Viet Cong" to win this war. He also claims that there is a spy network within the camp and ARVN strike force.

One day, Muldoon, while supervising a group of U.S. Seabees clearing part of the jungle around the base camp and evacuating the civilians in preparation for a potential Viet Cong attack, he notices an ARVN soldier pacing unusually outside the team house and mess hall and slugs him out. Upon interrogation by Captain Nim, the ARVN soldier denies being a Viet Cong spy, until Nim discovers a silver cigarette lighter in the ARVN soldiers possession, which belonged to a Green Beret medical specialist who was recently murdered by the VC, and whom was a friend of Kirbys. After Beckworth witnesses Nim beat and torture the Viet Cong suspect to get a confession from him, he confronts Kirby about it, which the Colonel justifies the interrogation by telling Beckworth about the cigarette lighter the Viet Cong suspect had and how the VC are ruthless killers who deserve no protection of any kind in this new kind of war.

Another few days later, Beckworth accompanies Kirby and his team on a patrol to a local village in the nearby mountains. It is here that Beckworth changes his mind about the American involvement in the Vietnam war after first witnessing the aftermath of a Viet Cong terror attack on a nearby Montagnard village in which the young grand-daughter of the village Chief he had befriended earlier, as well as the Chief and most of the male villagers are tortured and executed by the VC for cooperating with the Americans.

Another evening or so later, the Special Forces camp is attacked in a massed nighttime attack by thousands of enemy Viet Cong and North Vietnamese troops. Kirby and Muldoon fly out to assess the situation, but their helicopter is shot down by enemy fire, but they are soon rescued by a patrol where they secure a field as a LZ for US and ARVN reinforcements as a Mike Force to aid the besieged camp.

Meanwhile, the ferocious North Vietnamese Army attack upon the SF camp continues relentlessly. Beckworth is forced to don a rifle from a fallen ARVN sergeant and fights alongside the Green Berets as well as helping move the local villagers into the camp to protect them from the enemy onslaught.

As the battle rages, Ham Chunks pet dog is killed and the young boy tearfully buries his faithful companion. Symbolically, the boy uses the stick he had used to dig the dogs grave as the tombstone. He is found by Petersen who takes him to safety with the other refugees. As ARVN soldiers rush to their defensive positions, the stick is knocked away, leaving an unmarked grave.

At this time, the perimeter of the camp is breached by enemy sappers who blow up holes in the barbed wire fences around the camp, and the Green Berets and ARVN soldiers are forced to fall back to the inner perimeter. Just then, Kirby and Muldoon arrive with the Mike Force reinforcements, which are supported by a US airstrike which A-1 Skyraiders drop napalm on the attacking enemy troops with little success. Nim is killed by enemy artillery as he is detonating claymore mines to kill more attacking enemy soldiers within the camps lines.

By dawn, with the enemy attack still continuing, Kirby orders the troops to fall back and withdraw from the camp which is then taken by the enemy. At a nearby LZ, more US Army helicopters arrive to evacuate the refugees and Petersen puts Ham Chuck on one of the helicopters and promises to return for him in Da Nang. With the base in VC/NVA hands, Kirby orders an airstrike of a C-47 on the camp which kills more enemy troops forcing them to withdraw later that day. When the enemy departs, Kirby and his team re-occupy the destroyed camp.

Afterwords, Kirby has a talk with Beckworth where the reporter admits that he probably will be fired from the newspaper where he works for filing a story supporting the American war. He then thanks Kirby for the experience and then returns to Da Nang with the Mike Force reinforcements.
 honey trap to lure General Ti to a guarded former French colonial mansion located in a well guarded valley deep in North Vietnam.

Kirby, Muldoon, Peterson, and a handful of Green Berets, Montagnards (Degar), and ARVN soldiers are selected by Cai for this secret mission, and who will be personally accompanying them. At nightfall, they are airlifted in a C-130 transport and parachuted into the North Vietnam jungle. After Kirbys point man, Kolowski, is killed by a patrol of local enemy militia, after he kills all of them single-highhandedly, the group continues on. Muldoon and the medical specialist, Doc McGee and two of Cais men are to stay behind at a local bridge over a river to set explosive to blow it up to prevent the team from being chased by the NVA forces.

At nightfall, Kirby and the group arrive outside the guarded plantation where they witness the enemy general arrive at his plantation with Lin. After Kirby and Cai and their men kill all the sentries around the mansion, they quietly enter and subdue the enemy general with Lins help and hoist him outside where they put him in the trunk of his car and Kirby, Cai, Petersen, and Lin drive off, but the rest of the team is overwhelmed and killed in a hail of bullets by the North Vietnamese guards while attempting to escape.
 Skyhook device.

While Kirby and the group advance through the woods to the LZ for the helicopters to pick them up, Petersen is killed by an enemy booby-trap when he is gorily impaled to a trap of punji sticks. Kirby and his team are forced to leave his dead body behind.

Back at Da Nang Air Force base, Beckworth watches as Ham Chuck awaits the return of the helicopters carrying the survivors of the raid. He realizes the toll of the war as Ham Chuck runs crying from helicopter to helicopter, searching for Petersen who is not there. Beckworth then accompanies a group of US soldiers whom have arrived in the country and are sent to the war zone area. Kirby, in a touching moment, walks over to the boy and tells him the sad news. Ham Chuck asks plaintively, "What will happen to me now?" Kirby places Petersens green beret on him and says, "You let me worry about that, Green Beret. Youre what this things all about." The two walk holding hands along the beach into the sunset.

==Cast==
* John Wayne as COL Mike Kirby (Group Commander)
* David Janssen as George Beckworth
* Jim Hutton as SGT Petersen 
* Aldo Ray as MSG Muldoon (Senior NCO)
* Raymond St. Jacques as SFC "Doc" McGee (Medical Sergeant)
* Bruce Cabot as COL Morgan
* Jack Soo as Colonel Cai (ARVN)
* George Takei as Captain Nim (ARVN)
* Patrick Wayne as LT Jamison (USN)
* Luke Askew as SGT Provo (Heavy Weapons "but not if he can find a light one")
* Irene Tsu as Lin
* Edward Faulkner as CPT MacDaniel
* Jason Evers as CPT Coleman Mike Henry as SGT Kowalski
* Craig Jue as Ham Chuck Richard "Cactus" Pryor as Collier (often mistakenly credited as Richard Pryor) 

==Production notes== The Devils Brigade,  an account of the World War II 1st Special Service Force in 1965, and produced that film instead.
* The films origins began in 1965 with a trip by John Wayne to South Vietnam, and his subsequent decision to produce a film about the   to do so. Skyhook recovery for use in the film.  The Army also provided authentic uniforms for use by the actors, including the OG-107 green and "Tiger Stripe" Tropical Combat Uniform (jungle fatigues), with correct Vietnam War subdued insignia and name tapes.  Some of the "Vietnamese village" sets were so realistic they were left intact, and were later used by the Army for training troops destined for Vietnam.  The commander of the United States Army Airborne School at Fort Benning can be seen shooting trap with John Wayne in the film.  He can be identified as the only soldier wearing the Vietnam-era "baseball" fatigue cap; the rest wear green berets.  The soldiers exercising on the drill field which Wayne shouts to were Army airborne soldiers in training.  CIDG camp Captain Roger C. Donlon was the first American to receive the Medal of Honor during the Vietnam War.  Australian Warrant Officer Kevin Conway was the first Australian to be killed in action in the Vietnam War during the battle.  The A-107 camp scene used in the film was realistically constructed on an isolated, hilly area of Fort Benning, complete with barbed wire trenches, punji sticks, sandbagged bunkers, mortar pits, towers, support buildings and hooches for the combined strike force.  The camp set was largely destroyed by the producers using several tons of dynamite and black powder during the filming of the battle sequence. 
* George Takei missed working on the " " series to work on this movie.  The Fugitive" aired. 
*  The famous supposed-goof at the end of the movie where Ham Chuck and Kirby walk along the beach into the sunset and watch the sun set over the ocean in the east is not necessarily impossible.  Since no previous scenes take place on the coast, this scene could have taken place, as Vietnam does have a west coast, albeit a short one. There are also west-facing beaches around Vũng Tàu with approximately 25 miles to the opposite shore or it could be a sunrise that was filmed instead of a sunset.   .

==Realism==
Although The Green Berets portrays the Vietcong and North Vietnamese Army as sadistic tyrants, it also depicts them as a capable and willing enemy. The film shows the war as one with no front lines, meaning that the enemy can show up and attack at almost any position, anywhere. It shows the sophisticated spy ring of the VC and NVA that provided information about their adversaries. Like A Yank in Viet-Nam it gave a positive view of the South Vietnamese military forces.

The US Army objected to James Lee Barretts initial script in several ways. The first was that the Army wanted to show that South Vietnamese soldiers were involved in defending the base camp. That was rectified. Secondly, the Army objected to the portrayal of the raid with the mission of kidnapping a general because in the original script this involved crossing the border into North Vietnam. 
 The Alamo was heavily criticised for too much dialogue. Scenes shot with Vera Miles as the wife of Waynes character were jettisoned. 

==Critical reception== cowboys and indians", and being a "heavy-handed, remarkably old-fashioned film."  It is on his "Most Hated" list. In The New York Times, Renata Adler wrote, "It is vile and insane. On top of that, it is dull."  Oliver Stones acclaimed anti-war film Platoon (film)|Platoon was written partially as a reaction to The Green Berets.    It is mocked in the Gustav Hasford novel The Short-Timers in a scene where Joker and Rafter Man find the Lusthog Squad watching it at a movie theater.

Film commentator Emanuel Levy noted in his review that Wayne was not attempting to promote the cause of the Vietnam War as much as he was trying to portray the Special Forces in their unique role in the military: "Wayne said his motive was to glorify American soldiers as the finest fighting men without going into why we are there, or if they should be there. His compulsion to do the movie was based on his pride of the Special Forces, determined to show what a magnificent job this still little-known branch of service is doing. ... I wasnt trying to send a message out to anybody, he reasoned, or debating whether it is right or wrong for the United States to be in this war."

Levy also notes that Wayne acknowledged war is generally not popular but the soldiers are in a role of sacrifice – often against their personal will or judgment. Levy quotes Wayne: “What war was ever popular for Gods sake. Those men dont want to be in Vietnam anymore than anyone else. Once you go over there, you wont be middle-of-the-road."   

Despite the poor reviews, it went on to be a commercial success, which Wayne attributed in part to the negative reviews from the press, which he saw as representing criticism of the war rather than the film.  The Green Berets earned rentals of $8.7 million in North America during 1968. 

The journalist John Pilger describes his reaction to The Green Berets in a 2007 speech he gave criticising the media for its coverage of the Vietnam war. "I had just come back from Vietnam, and I couldn’t believe how absurd this movie was. So I laughed out loud, and I laughed and laughed. And it wasn’t long before the atmosphere around me grew very cold. My companion, who had been a Freedom Rider in the South, said, Let’s get the hell out of here and run like hell." 

==Music==
The original choice for scoring the film, Elmer Bernstein, a friend and frequent collaborator with John Wayne, turned the assignment down due to his political beliefs. As a second choice, the producers contacted Miklós Rózsa then in Rome.  When asked to do The Green Berets for John Wayne, Rózsa replied  "I dont do Westerns". Rozsa was told "Its not a Western, its an Eastern".   As a title song, the producers used a Ken Darby choral arrangement of Barry Sadlers hit song  Ballad of the Green Berets. Rozsa provided a strong and varied musical score including a night club vocal by a Vietnamese singer Bạch Yến;  however, bits of Onward Christian Soldiers were deleted from the final film.

==See also==
 
* John Wayne filmography
* White savior narrative in film

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 