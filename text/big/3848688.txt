Saamy
{{Infobox film
| name           = Saamy
| image          = Saamy poster.jpg
| caption        = Movie Poster Hari
| producer       = Pushpa Kandasamy Hari
| cinematography = Priyan
| editing        = V. T. Vijayan Vikram  Trisha  Vivek   Kota Srinivasa Rao   Ramesh Khanna
| studio         = Kavithalaya Productions
| distributor    = Lakshmi Venkat Sai Films
| released       =  
| runtime        = 161 minutes
| country        = India
| language       = Tamil
| music          = Harris Jayaraj
| gross           =   
}}
 Tamil action Ayya Starring Darshan and Trisha and Kota Srinivasa Rao in the lead. The films music was composed by Harris Jayaraj. Upon release, the film was highly successful at the box office. Later the movie was remade into Hindi as Policegiri starring Sanjay Dutt.
 Hari this Tamil such Tamil as a villain in this film.

==Plot==
Aarusaamy  (Vikram (actor)|Vikram) is the Deputy Commissioner of Police of Tirunelveli who efficiently brings the city under control. Aarusaamy arrives at Tirunelveli after being in exile for some years after being wrongly accused of bribery by corrupt politicians. In an early scene in the film, he eats idli with beer and performs some drunken antics, which had a negative impact on the viewing public. At the outset, he pretends to be a corrupt cop by accepting bribes from the very influential Annachi. Later Saamy starts to rebel against him and the rest is about how he succeeds in overcoming the corrupt politicians.

Aarusaamys father (Vijayakumar (actor)|Vijayakumar) who wanted to become a cop was unable to become due to corruption and takes care of agriculture for his living. But he wanted to make his son a cop. Vikram too passes the IPS examinations in merit, but was asked for bribe. His father mortgages his properties and makes him get the desired job. Being an honest cop, Aarusaamy is honoured with transfers all over Tamil Nadu due to political pressure. Finally, in Trichy, he is accused of bribery by a politician and he gets suspended. It takes him six months to prove himself and he is posted in Tirunelveli now.

In Tirunelveli, he adapts a new policy of adjusting with the local goons so that he can serve the people in an effective way. Perumal Pichai (Kota Srinivasa Rao), is an underworld don who has the total control of Southern Tamil Nadu, both in politics as well as rowdyism but he is less known in the media. He bribes Aarusaamy so that he will not disturb his business because both Perumal Pichai and Aarusaamy belongs to his caste. Saamy also accepts it but requests him to make some changes which he in turn accepts. As a result, the city is under the control of Law and Order. He is always accompanied by "Punctuality" Paramasivam (Ramesh Khanna), who is an inspector.

Saamy falls in love with a college-going Brahmin girl, Bhuvana (Trisha Krishnan) and they get engaged. Bhuvanas father, (Delhi Ganesh) is a straightforward government officer who never got bribe and leads a noble life. Saamy and Bhuvana meet each other when Saamy goes in search of a home for rent to Bhuvanas home. Bhuvana misinterprets Saamy and Paramasivam as thieves and locks them in a room.

The ruling party has called of for a one-day strike all over the state and Perumal Pichai is handed with the responsibility of Tirunelveli on the eve of Pongal festival. But Saamy takes steps to maintain law and order and the strike becomes a failure. This makes Perumal Pichai angry. He was waiting for a chance to take revenge on Saamy and they attack the market on the day of Saamys marriage since all the policemen would be attending the wedding. Besides this, Saamy also seals the petrol bunk (gas station) of Perumal Pichai because the workers at the petrol bunk assaulted a woman and others when they challenged very less distribution of petrol than being promised. This incident was the last straw that broke the camels back and started the revolution of Saami against Perumal Pichai.

This incident marks the start of direct clash between Saamy and Perumal Pichai. Both challenge to get rid of the other in 7 days time. Perumal Pichai uses his influences and gets Saamy transferred but Saamy is given a time of 7 days to take charge. Saamy also loses his father in a bomb blast in his home which was targeted to kill him. He plans accordingly and takes revenge on Perumal Pichai by the eve of 7th day.

==Cast==
  Vikram as Aarusaamy Trisha as Bhuvana Aarusaamy
* Kota Srinivasa Rao as Ilaiya perumal (Perumal Pichai) Vivek as Venkataraman Iyengar
* Ramesh Khanna as Sub Inspector Paramasivam Vijayakumar as Saamys Father
* Delhi Ganesh as Bhuvanas Father Sumithra as Bhuvanas Mother
* Venniradai Moorthy as Bhuvanas Grandfather Manorama as Bhuvanas Grandmother Ponnambalam as Annachis Henchman
* Bala Singh as Annachis henchman Thyagu as M.L.A
* Singamuthu
* Cell Murugan as Traffic inspector
* Benjamin as Kudairepair
* Keerthana as Venkatramans wife
* Ilavarasu
* Crane Manohar Rajesh
* Bayilwan Ranganathan MRK
* Besant Ravi
* Sethuvinayagam
* Chaplin Balu as Bridegroom of donkey
 

==Production== Hari to Vikram and Trisha were selected to play lead pair. Telugu actor Kota Srinivasa Rao was selected to play negative role making his debut in Tamil.

The films shooting schedule took place at Karaikudi, Some fight scenes were shot at the busy lanes in Karaikudi where Vikram chased some rowdies, The fight scene was shot for five days, with Priyan canning the shots and Super Subbarayan choreographing the fights.   Vikram worked on his body for the film, sporting a thick waist to show notable differences from his other police film, Dhill and also put on eight kilograms. 

==Soundtrack==
{{Infobox album  
| Name         = Saamy
| Type         = Soundtrack
| Artist       = Harris Jayaraj
| Cover        = Saamy Audio Cover.jpg
| Border       = yes
| Alt          =
| Caption      = Front CD Cover
| Released     = 2003
| Recorded     =
| Genre        = Film soundtrack
| Length       = 25:27 Tamil
| Label        = Star Music Ayngaran Music An Ak Audio
| Producer     = Harris Jayaraj
| Last album   = Lesa Lesa   (2003)
| This album   = Saamy   (2003)
| Next album   = Kovil (film)|Kovil   (2003)
}}
 Filmfare Best Music Director award.

Tamil Tracklist
{{track listing
| extra_column     = Singer(s)
| lyrics_credits   = yes
| total_length     = 25:27

| title1        = Aarumuga Saamy
| extra1        = Sriram Parthasarathy
| lyrics1       = Snehan
| length1       = 04:36

| title2        = Idhuthaanaa
| extra2        = K. S. Chithra
| lyrics2       = Thamarai
| length2       = 05:19

| title3        = Kalyaanam Thaan Kattikittu
| extra3        = KK (singer)|KK, Yugendran, Srilekha Parthasarathy
| lyrics3       = Snehan
| length3       = 05:02

| title4        = Pudichirukku
| extra4        = Hariharan (singer)|Hariharan, Komal Ramesh, Mahathi
| lyrics4       = Na. Muthukumar
| length4       = 05:03

| title5        = Veppamaram Tippu
| lyrics5       = Na. Muthukumar
| length5       = 05:27
}}

Telugu Tracklist
{{track listing
| extra_column     = Singer(s)
| all_lyrics       = Siva Ganesh
| total_length     = 25:27

| title1        = Thapeswaram Kajaaraa
| extra1        = Naveen
| length1       = 04:36

| title2        = Ayyayyo Ayyayyo
| extra2        = Koushik, Shravya, Komal Ramesh
| length2       = 05:03

| title3        = Yedalona Yedalona
| extra3        = Harini
| length3       = 05:19

| title4        = Pelli Pelli
| extra4        = Tippu (singer)|Tippu, Srilekha Parthasarathy
| length4       = 05:02

| title5        = Vepachettu
| extra5        = Tippu
| length5       = 05:27
}}

==Release==
===Critical reception===
Sify wrote:"The patchy storyline merely serves as a pretext to spark off several skirmishes and bombastic dialogues. Ultimately it is Vikram who dominates this action movie. In fact he is the mainstay of the picture   Director Hari packs in a sting, but Saamy is strictly for the no holds barred action addicts".  Bizhat called it:"taut, fully engaging actioner".  Hindu wrote:"Kavithalayas "Saami" should follow the "Dhil", "Dhool" line. Vikrams daredevilry and macho appeal ought to go down well with the masses. His presence of mind and intelligence are bound to make an impression on those who expect something more". 

===Box office===
Saamy was released approximately in 100 screens in Tamil Nadu. The film had a huge opening as it was a summer vacation for the Tamil audiences. Theatres in Chennai had almost 100% occupancies and the film recovered all its budget within 4 to 5 days. 

==Remakes==
The film was remade into Telugu as Lakshmi Narasimha with Balakrishna and Asin in lead roles though the original version was dubbed and released in Telugu as Swamy IPS.  It was also remade in Bengali as Barood_(2004 film)|Barood (বারূদ) with Mithun Chakraborty and in Hindi as Policegiri by the notable director K.S.Ravikumar with Sanjay Dutt and Prachi Desai. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 