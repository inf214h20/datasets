Who Framed Roger Rabbit
 
 
{{Infobox film
| name = Who Framed Roger Rabbit
| image = Movie poster who framed roger rabbit.jpg
| alt = 
| caption = Theatrical release poster by Steven Chorney
| director = Robert Zemeckis Frank Marshall Robert Watts Jeffrey Price Peter S. Seaman
| based on =  
| starring = Bob Hoskins Christopher Lloyd Charles Fleischer Stubby Kaye Joanna Cassidy 
| music = Alan Silvestri
| cinematography = Dean Cundey Arthur Schmidt
| studio = Touchstone Pictures Amblin Entertainment  Buena Vista Pictures Distribution, Inc.
| released =  
| runtime = 103 minutes 
| country = United States
| language = English
| budget = $70 million
| gross = $329.8 million 
}} animated Fantasy film|fantasy-comedy film  directed by Robert Zemeckis. The screenplay by Jeffrey Price and Peter S. Seaman is based on Gary K. Wolfs 1981 novel Who Censored Roger Rabbit?, which depicts a world in which cartoon characters interact directly with human beings and animals.

Who Framed Roger Rabbit stars Bob Hoskins as private detective Eddie Valiant, who investigates a murder involving Roger Rabbit, a second banana|second-banana cartoon character. The film co-stars Charles Fleischer as the eponymous characters voice; Christopher Lloyd as Judge Doom, the villain; Kathleen Turner as the voice of Jessica Rabbit, Rogers cartoon wife; and Joanna Cassidy as Dolores, the detectives girlfriend.
 Walt Disney Richard Williams was hired to supervise the animation sequences. Production was moved from Los Angeles to Elstree Studios in England to accommodate Williams and his group of animators. While filming, the production budget began to rapidly expand and the shooting schedule ran longer than expected.
 modern era of American animation, especially the Disney Renaissance. 

==Plot==
 

In 1947, cartoon characters, commonly called "toons", are living beings who act out cartoons in the same way that human actors make live-action production. Toons interact freely with humans and animals and live in Toontown, an area near Hollywood, California. R. K. Maroon is the human owner of Maroon Cartoon studios; Roger Rabbit is a fun-loving toon rabbit, one of Maroons stars; Rogers wife Jessica Rabbit is a toon woman, and Baby Herman is Rogers co-star, a 50-year-old toon who looks like an infant. Marvin Acme is the practical joke-loving owner of Toontown and the Acme Corporation.

Maroon hires private detective Eddie Valiant to investigate rumors that Jessica is having an extramarital affair. Eddie and his brother Teddy used to be friends of the toon community, but Eddie has hated them, and has been drinking heavily, since Teddy was killed by a toon who dropped a piano on his head some years earlier while investigating a Toontown bank robbery. Eddie goes to Maroons offices and shows Roger photographs of Jessica "cheating" on him by playing Pat-a-cake, pat-a-cake, bakers man|patty-cake with Acme, Maroon and Eddie suggests that Roger and Jessica should separate, but Roger becomes distraught and runs away. This makes him the prime suspect when Acme is found murdered the next day by the Los Angeles Police Department|LAPD. At the crime scene, Eddie meets Judge Doom and his Toon Patrol of weasel henchmen. Although toons are virtually impervious to physical harm, Doom has discovered that they can be killed by being submerged in a mixture of Turpentine, Acetone, and Benzene that he refers to as "Dip". He demonstrates it by sampling an innocent cartoon shoe which quickly dissolves in the dip leaving red paint, much to Eddies shock while LAPD Lt. Santino looks away, unable to watch Dooms brand of "toon justice."

Baby Herman insists that Acmes Will (law)|will, which is missing, bequeaths Toontown to the toons. If the will is not found by midnight, Toontown will be sold to Cloverleaf Industries, which recently bought the Pacific Electric system of trolley cars. One of Eddies photos shows the will in Acmes pocket, proving Baby Hermans claim. After Roger shows up at his office professing his innocence, Eddie investigates the case with help from his girlfriend Dolores while hiding Roger from the Toon Patrol. Jessica tells Eddie that Maroon blackmailed her into compromising Acme, and Eddie learns that Maroon is selling his studio to Cloverleaf. Maroon explains to Eddie that Cloverleaf will not buy his studio unless they can also buy Acmes gag-making factory. His plan was to use the photos to blackmail Acme into selling. Before he can say more, he is killed by an unseen assassin and Eddie sees Jessica fleeing the scene. Thinking that she is the killer, Eddie pursues her into Toontown. When he finds her, she explains that Doom killed Maroon and Acme in an attempt to take over Toontown.
 dismantling the die of laughter, while the leader, Smart-Ass, is subjected to the Dip, and confronts Doom. Doom survives being run over by a steamroller, revealing that he himself is a toon (disguised, wearing a rubber mask), and that he killed Teddy. Eddie eventually dissolves Doom in the Dip by opening the drain on the Dip machine, avenging his brothers death.

As toons and the police arrive, Eddie discovers that an apparently blank piece of paper on which Roger wrote a love poem to Jessica is actually Acmes will, written in Invisible ink|disappearing/reappearing ink. Eddie kisses Roger — proving that he has regained his sense of humor — and the toons celebrate their victory. As everybody heads back to Toontown, Porky Pig and Tinker Bell end the film with their usual splutter of "Thats all folks!" and spark of pixie dust.

==Cast==
 
 
* Bob Hoskins as Eddie Valiant, an alcoholic private investigator who holds a grudge against Toons. Executive producer Spielbergs first choice for the role was Harrison Ford, but Fords price was too high. Bill Murray was also considered for the role; however, due to his method of receiving offers for roles, he missed out.   
*   
*  , both being overly evil characters which he considered being "fun to play".  Lloyd avoided blinking his eyes while on camera in order to perfectly portray the character. 
* Kathleen Turner provides the uncredited voice of Jessica Rabbit, Roger Rabbits beautiful and flirtatious Toon wife.    She loves Roger because, as she says, "he makes me laugh." Amy Irving supplied the singing voice, while Betsy Brantley served as the stand-in.
* Joanna Cassidy as Dolores, Eddies on-off girlfriend who works as a waitress.
* Alan Tilvern as R. K. Maroon, the short-tempered and manipulative owner of "Maroon Cartoon" studios. This was Tilverns final theatrical performance.
* Stubby Kaye as Marvin Acme, Jester|prankster-like owner of the Acme Corporation. This was Kayes final film performance.
* Lou Hirsch provides the voice of Baby Herman, Rogers middle-aged, foul-mouthed, cigar-chomping co-star in Maroon Cartoons. Williams said Baby Herman was a mixture of "Elmer Fudd and Tweety crashed together".  April Winchell provides the voice of Mrs. Herman and the "baby noises".
* David Lander provides the voice of Smart Ass, the leader of the weasels.
 Fred Newman voiced Stupid and June Foray voiced Wheezy. Foray also voiced Lena Hyena, a hag Toon woman who resembles Jessica Rabbit and provides a comical role which shows her falling for Eddie and pursuing him. She shares her name with the character from Lil Abner, but its unclear if its the same character Al Capp created.
 Richard Williams The Big Bad Wolf, Russi Taylor voiced Minnie Mouse and some birds, Cherry Davis voiced Woody Woodpecker, Tony Anselmo voiced Donald Duck (with an archival recording of Clarence Nash, the original voice of Donald, used at the beginning of the scene ), Frank Welker voiced Dumbo, Mae Questel reprised her role as Betty Boop, Pat Buttram, Jim Cummings & Jim Gallant voiced Valiants animated bullets, Les Perkins voiced Mr. Toad, Mary Radford voiced Hyacinth Hippo from Fantasia (1940 film)|Fantasia, Nancy Cartwright voiced the Dipped shoe, and Peter Westy voiced Pinocchio.

==Production==

===Development=== I Wanna Frank Marshall, Frank Marshall Kathleen Kennedy, were approached to produce Who Framed Roger Rabbit alongside Disney. The original budget was projected at $50 million, which Disney felt was too expensive. 
 Walt Disney animation department. Felix the Universal Pictures/Walter Lantz Productions to "lend" their characters to appear in the film with (in some cases) stipulations on how those characters were portrayed; for example, Disneys Donald Duck and Warners Daffy Duck appear as equally-talented dueling pianists, and Mickey Mouse and Bugs Bunny also share a scene. Apart from this agreement, Warner Bros. and the various other companies were not involved in the production of Roger Rabbit. However, the producers did not have time to acquire the rights to use Popeye, Tom and Jerry, Little Lulu, Casper the Friendly Ghost or the Terrytoons for appearances from their respective owners (King Features, Turner, Western Publishing, Harvey Comics and Viacom (original)|Viacom).  
 Peter Schneider, Waking Sleeping Beauty DVD commentary, 2010, Walt Disney Studios Home Entertainment 

===Writing=== urban and teamed up against the Pacific Electric Railway system and bought them out of business. Where the freeway runs in Los Angeles is where the Red Car used to be."  In Wolfs novel Who Censored Roger Rabbit?, the Toons were comic strip characters rather than movie stars. 
 act as joeys pop Snow White Harlem Cotton The Seven Dwarfs in cameo appearances. However, the scene was cut for pacing reasons and never made it past the storyboard stage.  Before finally agreeing on Who Framed Roger Rabbit as the films title, working titles included Murder in Toontown, Toons, Dead Toons Dont Pay Bills, The Toontown Trial, Trouble in Toontown, and Eddie Goes to Toontown. 

===Filming=== Richard Williams James Baxter, David Bowers, Chris Jenkins, Phil Nibbelink, Nik Ranieri, and Simon Wells. The animation production, headed by associate producer Don Hahn, was split between Richard Williams London studio and a specialized unit in Los Angeles, set up by Walt Disney Feature Animation and supervised by Dale Baer.  The production budget continued to escalate while the shooting schedule lapsed longer than expected. When the budget reached $40 million, Disney president Michael Eisner seriously considered shutting down production, but Jeffrey Katzenberg talked him out of it. Stewart, p.87  Despite the escalating budget (from the original $30 million to the final $70 million), Disney moved forward on production because they were enthusiastic to work with Spielberg. 

 s, puppeteers, mannequins and robotic arms were commonly used during filming to help the actors interact with "open air and imaginative cartoon characters". ]] Filming began blue screen Desilu Studios served as the fictional Maroon Cartoon Studio lot. 

===Animation and post production=== optical compositing.  First, the animators and lay-out artists were given black and white printouts of the live action scenes (known as "photo stats"), and they placed their animation paper on top of them. The artists then drew the animated characters in relationship to the live action footage. Due to Zemeckis dynamic camera moves, the animators had to confront the challenge of ensuring the characters were not "slipping and slipping all over the place."   After rough animation was complete, it would run through the normal process of traditional animation until the cels were shot on the rostrum camera with no background. The animated footage was then sent to ILM for compositing, where technicians would animate three lighting layers (shadows, highlights and tone mattes) separately, in order to make the cartoon characters look three-dimensional and give the illusion of the characters being affected by the lighting on set.  Finally, the lighting effects were optically composited on to the cartoon characters, who were, in turn, composited into the live-action footage. One of the most difficult effects in the film was Jessicas dress in the night club scene, because it had flashing sequins, an effect accomplished by filtering light through a plastic bag scratched with steel wool. 

===Music===
{{Infobox album  
| Name     = Who Framed Roger Rabbit (Soundtrack from the Motion Picture)
| Type     = Soundtrack
| Artist   = Alan Silvestri and the London Symphony Orchestra
| Cover    =  
| Released = June 22, 1988
| Recorded = 1988
| Genre    = Soundtrack
| Length   = 45:57 Buena Vista
}} music themes improvised by the LSO. The work of American composer Carl Stalling heavily influenced Silvestris work on Who Framed Roger Rabbit.   The films soundtrack was originally released by Buena Vista Records on June 22, 1988, and reissued by Walt Disney Records on CD on April 16, 2002. 

{{Track listing
| extra_column = Performer(s)
| title1       = Maroon Logo
| length1      = 0:19
| extra1       = Alan Silvestri
| title2       = Maroon Cartoon
| length2      = 3:25
| extra2       = Silvestri
| title3       = Valiant & Valiant
| length3      = 4:22
| extra3       = Silvestri
| title4       = The Weasels
| length4      = 2:08
| extra4       = Silvestri
| title5       = Hungarian Rhapsody (Dueling Pianos)
| length5      = 1:53
| extra5       = Tony Anselmo, Mel Blanc
| title6       = Judge Doom
| length6      = 3:47
| extra6       = Silvestri
| title7       = Why Dont You Do Right?
| length7      = 3:07
| extra7       = Amy Irving
| title8       = No Justice for Toons
| length8      = 2:45
| extra8       = Silvestri
| title9       = The Merry-Go-Round Broke Down (Rogers Song)
| length9      = 0:47
| extra9       = Charles Fleischer
| title10      = Jessicas Theme
| length10     = 2:03
| extra10      = Silvestri
| title11      = Toontown
| length11     = 1:57
| extra11      = Silvestri
| title12      = Eddies Theme
| length12     = 5:22
| extra12      = Silvestri
| title13      = The Gag Factory
| length13     = 3:48
| extra13      = Silvestri
| title14      = The Will
| length14     = 1:10
| extra14      = Silvestri
| title15      = Smile, Darn Ya, Smile!/Thats All Folks
| length15     = 1:17
| extra15      = Toon Chorus
| title16      = End Title (Who Framed Roger Rabbit)
| length16     = 4:56
| extra16      = Silvestri
}}

==Release==
Michael Eisner, then CEO, and Roy E. Disney, Vice Chairman of the Walt Disney Company, felt Who Framed Roger Rabbit was too risqué with sexual references.  Eisner and Zemeckis disagreed over elements with the film, but since Zemeckis had final cut privilege, he refused to make alterations.  Roy E. Disney, head of Feature Animation along with studio chief Jeffrey Katzenberg, felt it was appropriate to release the film under their Touchstone Pictures banner instead of the traditional Walt Disney Pictures banner. 

Who Framed Roger Rabbit opened on June 24, 1988, in America, grossing $11,226,239 in 1,045 theaters during its opening weekend, ranking first place in the domestic box office.  The film went on to gross $156,452,370 in North America and $173,351,588 internationally, coming to a worldwide total of $329,803,958. At the time of release, Roger Rabbit was the twentieth highest-grossing film of all time.  The film was also the second highest grossing film of 1988, behind only Rain Man. 

Zemeckis has revealed a 3D reissue could be possible. 

===Home media releases===
Who Framed Roger Rabbit was first released on VHS on October 12, 1989. A Laserdisc edition was also released. A DVD version was first available on September 28, 1999.

On March 25, 2003,  , the "pig head" sequence; the three Roger Rabbit shorts, Tummy Trouble, Roller Coaster Rabbit, and Trail Mix-Up; as well as a booklet and interactive games. The only short on the 2003 VHS release was Tummy Trouble.
 Touchstone Home digitally restored by Disney for its 25th Anniversary. Frame-by-frame digital restoration was done by Prasad Studios removed dirt, tears, scratches and other defects.  

==Critical reception== Siskel & Ebert episode in which they reviewed the film analyzing the films painstaking filmmaking. Siskel also praised the film, and ranked it #2 on his top ten films list for 1988, while Ebert ranked it as #8 on a similar list.   Janet Maslin of The New York Times commented that "although this isnt the first time that cartoon characters have shared the screen with live actors, its the first time theyve done it on their own terms and make it look real".  Desson Thomson of The Washington Post considered Roger Rabbit to be "a definitive collaboration of pure talent. Zemeckis had Walt Disney Pictures enthusiastic backing, producer Steven Spielbergs pull, Warner Bros.s blessing, Canadian animator Richard Williams ink and paint, Mel Blancs voice, Jeffrey Prices and Peter S. Seamans witty, frenetic screenplay, George Lucas Industrial Light & Magic, and Bob Hoskins comical performance as the burliest, shaggiest private eye." {{cite news|author=Desson Thomson|url=
http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/whoframedrogerrabbitpghowe_a0b16f.htm | title = Who Framed Roger Rabbit | work = The Washington Post | date = June 24, 1988 | accessdate = November 1, 2008}}  Today Show also praised the film, calling it "one of the most extraordinary movies ever made". 
 homages to the Golden Age of American animation.  Animation legend Chuck Jones made a rather scathing attack on the film in his book Chuck Jones Conversations. Among his complaints, Jones accused Robert Zemeckis of robbing Richard Williams of any creative input and ruining the piano duel that both he and Williams storyboarded.  

The film received generally positive reviews.  , 60 reviews collected by review aggregator   calculated an average score of 83, based on 15 reviews. 

===Accolades=== Mary Poppins Best Sound Best Visual Best Film Best Art Peter Howitt), Best Cinematography Best Sound John Boyd, Special Achievement Best Direction Special Visual Golden Globe for Best Motion Picture (Musical or Comedy), while Hoskins was also nominated for his performance.  The film also won the Hugo Award for Best Dramatic Presentation  and the Kids Choice Award for Favorite Movie.

;American Film Institute Lists
* AFIs 100 Years...100 Laughs—Nominated
* AFIs 100 Years...100 Movie Quotes:
** "Im not bad. Im just drawn that way."—Nominated
* AFIs 10 Top 10—Nominated Fantasy Film

===Top Ten lists===
5th  (in 1988)  - Cahiers du cinéma 

==Legacy==
 
 s Mickey Mouse and Warner Bros. Bugs Bunny appeared on screen together.]] theatrical animated Dick Tracy two Hare PC games, 1989 game 1991 game released on the Game Boy. 

===Controversy===
With the films Laserdisc release, Variety (magazine)|Variety first reported in March 1994 that observers uncovered several scenes of antics from the animators that supposedly featured brief nudity of the Jessica Rabbit character. While undetectable when played at the usual rate of 24 film frames per second, the Laserdisc player allowed the viewer to advance frame-by-frame to uncover these visuals. Whether or not they were actually intended to depict the nudity of the character remains unknown.     Many retailers said that within minutes of the Laserdisc debut, their entire inventory was sold out. The run was fueled by media reports about the controversy, including stories on CNN and various newspapers.  A Disney executive responded to Variety that "people need to get a life than to notice stuff like that. We were never aware of it, it was just a stupid gimmick the animators pulled on us and we didnt notice it. At the same time, people also need to develop a sense of humor with these things." 
 extends his middle finger as he passes under a womans dress and re-emerges with drool on his lip.     There is also controversy over the scene where Daffy Duck and Donald Duck are playing a piano duel, and, during his trademark ranting gibberish, it is claimed that Donald calls Daffy a "goddamn stupid nigger"; however, this is a misinterpretation, with the line from the script being "doggone stubborn little--."     

===Legal issue===
Gary K. Wolf, author of the novel Who Censored Roger Rabbit?, filed a lawsuit in 2001 against The Walt Disney Company. Wolf claimed he was owed royalties based on the value of "gross receipts" and merchandising sales. In 2002, the trial court in the case ruled that these only referred to actual cash receipts Disney collected and denied Wolfs claim. In its January 2004 ruling, the California Court of Appeal disagreed, finding that expert testimony introduced by Wolf regarding the customary use of "gross receipts" in the entertainment business could support a broader reading of the term. The ruling vacated the trial courts order in favor of Disney and remanded the case for further proceedings.  In a March 2005 hearing, Wolf estimated he was owed $7 million. Disneys attorneys not only disputed the claim but said Wolf actually owed Disney $500,000–$1 million because of an accounting error discovered in preparing for the lawsuit.  Wolf won the decision in 2005, receiving between $180,000 and $400,000 in damages. 

===Sequel===
With the films critical and financial success, Disney and Spielberg felt it was obviously time to plan a second installment.  . It began with Roger Rabbits early years, living on a farm in the  .    
 Broadway and Eric Goldberg was set to be the new animation director, and began to redesign Rogers new character appearance. 

Spielberg had no interest in the project because he was establishing DreamWorks, although Frank Marshall and Kathleen Kennedy decided to stay on as producers. Test footage for Who Discovered Roger Rabbit was shot sometime in 1998 at the Disney animation unit in Lake Buena Vista, Florida; the results were an unwieldy mix of CGI, traditional animation and live-action that did not please Disney. A second test had the Toons completely converted to computer-generated imagery|CGI; but this was dropped as the films projected budget escalated well past $100 million. Eisner felt it was best to cancel the film.  In March 2003, producer Don Hahn was doubtful over of a sequel being made, arguing that public tastes had changed since the 1990s with the rise of computer animation. "There was something very special about that time when animation was not as much in the forefront as it is now." 

In December 2007, Marshall admitted he was still "open" to the idea,    and in April 2009, Zemeckis revealed he was still interested.  According to a 2009 MTV News story, Jeffrey Price and Peter S. Seaman were writing a new script for the project, and the cartoon characters will be in traditional 2D, while the rest will be in motion capture.  However, in 2010, Zemeckis said that the sequel will remain hand-drawn animated and live-action sequences will be filmed, just like in the original film, but the lighting effects on the cartoon characters and some of the props that the toons handle will be done digitally.  Also in 2010, Don Hahn, who was the films original associate producer, confirmed the sequels development in an interview with Empire (magazine)|Empire magazine. He stated, "Yeah, I couldnt possibly comment. I deny completely, but yeah... if youre a fan, pretty soon youre going to be very, very, very happy."  In 2010, Bob Hoskins stated he was interested in the project, reprising his role as Eddie Valiant.  However, he retired from acting in 2012 after being diagnosed with Parkinsons disease a year earlier (and died from those complications in 2014).  Marshall has confirmed that the film is a prequel, similar to earlier drafts, and that the writing was almost complete.  During an interview at the premiere of Flight (2012 film)|Flight, Zemeckis stated that the sequel is still possible, despite Hoskins’ absence, and the script for the sequel was sent to Disney for approval from studio executives. 
 the 1952 film of the same name. The proposed film is set to a prequel, taking place five years before Who Framed Roger Rabbit and part of the story is about how Roger met Jessica, his future wife. Wolf has stated the film is currently wending its way through Disney. 

===Roger Rabbit dance=== the Running Man, but done by skipping backwards with arms performing a flapping gesture as if hooking ones thumbs on suspenders.

==Real world parallels==
One of the themes in the film pertains to the dismantling of public transportation systems by private companies who would profit from an automobile transportation system and freeway infrastructure. Near the end of the film, Judge Doom reveals his plot to destroy Toon Town to make way for the new freeway system. This is an indirect historical reference to the dismantling of public transportation trolley lines by   in 1922, reorganized in 1936 into a holding company—for the express purpose of acquiring local transit systems throughout the United States. "Once   purchased a transit company, electric trolley service was immediately discontinued, the tracks quickly pulled up, the wires dismantled ..." and General Motors buses replaced the trolleys. 

==References==
 

;Further reading
*  
*  
*  
*  

==External links==
 
 
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

{{Navboxes
|list1= 
 
 
 
 
 
 
 
}}
 

 
 
 
 
 
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 