My First Mister
{{Infobox film
| name           = My First Mister
| image          = My First Mister.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Christine Lahti
| producer       = Carol Baum Sukee Chew Jane Goldenring Anne Kurtzman Mitchell Solomon
| writer         = Jill Franklyn
| starring       = Albert Brooks Leelee Sobieski John Goodman Carol Kane Desmond Harrington
| music          = Steve Porcaro
| cinematography = Jeffrey Jur
| editing        = Wendy Greene Bricmont
| distributor    = Paramount Classics
| released       =  
| runtime        = 109 minutes
| country        = United States Germany
| language       = English
| budget         = $5.25 million
| gross          = $595,005
}}
My First Mister is a 2001 film written by Jill Franklyn and directed by Christine Lahti. The film is the story of an alienated teen (Leelee Sobieski) who forms an unlikely friendship with a lonely clothing store manager (Albert Brooks). The film co-stars Carol Kane, Michael McKean, John Goodman, and Desmond Harrington. 

==Plot== Century City Mall in Los Angeles, Jennifer (Sobieski), an 18-year-old "Goth subculture|goth-punk subculture|punk" girl who just graduated from High School, makes a nuisance of herself at a clothing store run by 49-year-old Randall Harris (Brooks), who eventually hires her on a trial basis as a stockroom clerk. Jennifer refers to herself simply as "J", and thus asks Randall if its okay if she calls him "R", to which he accedes.
 outfit and promotes her to saleswoman.

Feeling isolated from the other people in her life, J finds she is attracted to Randall. After an incident that makes him question whether he can continue to trust her, J demonstrates her trust in him by revealing that she engages in self harm. The two thus strike up an unlikely friendship as they realize that neither has anyone close with whom they can confide. Made aware that J is unhappy living with her mother (who seems to pay more attention to her two pugs than her daughter) and stepfather, Randall offers her an advance on her salary so she can afford her own place, then helps her find an apartment.

As their friendship progresses, Randall consents to getting a (very small) tattoo at Js urging, only to realize at the last possible moment that he cant go through with it. In a fit of despair he declares that they cant continue as friends. Confronted by J at his home a short time later, Randall confides his many phobias, which endears him to J even more. Their friendship restored, Randall reluctantly accompanies J to a cemetery to lie on the graves of the deceased to feel their "energy", something she does regularly. Due to the lateness of the hour, they go back to Randalls where they bond over tea and J spends the night on the couch.

The following morning J discovers Randall collapsed in the street after having told her he was going "for a running|run." She learns that Randall has had leukemia for many years and doesnt have long to live, and is initially very angry that he didnt share this with her.
 Albuquerque only cynical young man who tells her that his mother died in a car accident six months earlier, and had told him that his father had died before he was born. Although he initially refuses to drive to L.A. to see the father hes never known before he dies, he ultimately does so. Because of Js intervention, Randall and his son have a brief time to get to know one another, for which Randall is very grateful.

Js friendship with Randall inspires her to seek a closer relationship with her family, especially her mother. In Randalls final days, Jennifer organizes a dinner at which his son and Jennifers family come together to celebrate his life.

==Cast==
* Albert Brooks as Randall R Harris
* Leelee Sobieski as Jennifer "J" Wilson
* John Goodman as Benjamin Wilson
* Carol Kane as Mrs. Benson
* Michael McKean as Bob Benson
* Henry Brown as Jack Taylor, Salesman
* Desmond Harrington as Randy Harris, Jr.
* Mary Kay Place as Nurse Patty

==Production and casting==
In the DVD commentary, director Christine Lahti notes that Albert Brooks campaigned vigorously for the role of Randall, and that although initially doubtful about whether he could handle the heavy dramatic aspects of the character, she was won over at their first meeting. Similarly, when Sobieski was considered for the part of "J", she was initially skeptical that "such a beautiful face" could capture the feeling and look of isolation that was needed for the character, but changed her mind after meeting Sobieski.

Lahti expresses deep disappointment that the film was rated R (for language), despairing that it would likely not be seen by many teenagers who would like and relate to the characters. She also laments that the shooting schedule, constrained by the films budget, didnt give her the opportunity to shoot more "coverage" (camera perspectives, such as "close-ups"), and thus more editing choices to convey the emotional content of some scenes.

==Critical reception==
Film critic Roger Ebert praised the film, writing, "These two characters are so particular and sympathetic that the whole movie could simply observe them...The bravest thing about the movie is the way it doesnt cave in to teenage multiplex demographics with another story about dumb adults and cool kids. My First Mister is about reaching out, about seeing the other person, about having something to say and being able to listen. So what if the ending is in autopilot? At least its a flight worth taking." 

==References==
 

==External links==
*  
*  
*  
*   film info at Leelee Sobieski website

 
 
 
 