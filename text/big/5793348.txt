Thursday's Child (1943 film)
 
 
{{Infobox film
| name           = Thursdays Child
| image          = Thursdayschild.jpeg
| image_size     = 
| alt            = Theatrical release poster
| director       = Rodney Ackland
| producer       = John Argyle
| writer         = Rodney Ackland   Donald Macardle
| narrator       =  Wilfrid Lawson Charles Williams
| cinematography = Desmond Dickinson
| editing        = Flora Newton
| studio         = 
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 directed by Wilfrid Lawson. produced by John Argyle and Associated British Picture Corporation.

==Synopsis==
A young girl, Fennis Wilson (Sally Ann Howes), is cast in a film, launching her career to stardom, the very thing her older sister desperately wants.  Stardom is the furthest thing from 12-year-old Fennis goals in life, as shes more introspective and intellectual.  When the hit - film falls in her lap, it creates tension in the family that threatens to tear the family apart, while Fennis just wants everyone to be happy, especially herself. As usual, Ronald Shiners character (Joe) plays a decisive role

==Casting==
Thursdays Child was the first film for Howes. It was written and directed by Rodney Ackland, who was a neighbor of hers. Howes auditioned and was chosen for the part after over two hundred auditions of other girls.

==Cast==
* Sally Ann Howes as Fennis Wilson Wilfrid Lawson as Frank Wilson
* Kathleen ORegan as Ellen Wilson
* Stewart Granger as David Penley
* Eileen Bennett as Phoebe Wilson
* Marianne Davis as Gloria Dewey
* Gerhard Kempinski as Rudi Kauffmann
* Felix Aylmer as Mr. Keith
* Margaret Yarde as Mrs. Chard
* Vera Bogetti as Madame Felicia
* Percy Walsh as Charles Lennox
* Michael Allen as Jim Wilson
* Margaret Drummond as Wendy Keith
* Ronald Shiner as Joe Anthony Holles as Roy Todd

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 

 
 