Guru (2012 film)
{{Infobox film
| name           = Guru
| image          = Guru Kannada film (2012) poster.jpg
| caption        = Theatrical release poster
| director       = Jaggesh
| producer       = Parimala Jaggesh
| screenplay     = Jaggesh
| starring       = Gururaj Rashmi Gautham Yatiraj Sudharani Shobaraj Srinivasa Murthy
| music          = Vinay Chandra
| cinematography = Ramesh Babu
| editing        = K. M. Prakash
| studio         = Gururaj Films
| distributor    = 
| released       =  
| runtime        = 130 minutes 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}} Kannada film directed by Jaggesh. The film starred his sons Gururaj and Yathiraj as male leads and Rashmi Gautham as the female lead. The film was Jaggeshs first film as a director.  The film, as Jaggesh said, is a remake of a Korean film that deals with drug trafficking. 

==Cast==
* Gururaj as Guru
* Rashmi Gautham as Ankitha
* Yatiraj as Yathi
* Sudharani as Tejaswini
* Shobhraj as ACP Bopaiah
* Srinivasa Murthy Abhijit
* Jaggesh

==Production==
The film was actor Jaggeshs debut as a director and said that he took up the project on his sons insistence as a launchpad to the latter. The film was produced under the then-launched banner by Jaggesh, Gururaj Films.   

The story for the film was chosen by Jaggesh from politician Karunanidhis camp, receiving it from his son M. K. Tamailarasu. Jaggesh also wrote the script for the film. Speaking of the film, he said, "Guru will be a commercial film, but will not have a routine story. You will get the retro feel with this film. Such incidents would have occurred in everybodys life". To prepare for his role in the film, Gururaj underwent training in kickboxing and other martial arts for a period of five and a half months in Cambodia.  The film was launched in Mantralayam in May 2012, before filming that began in June 2012. 

==Soundtrack==
{{Infobox album
| Name        = Guru
| Type        = Soundtrack
| Artist      = Vinay Chandra
| Cover       = 2012 Kannada film Guru album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 25 September 2012
| Recorded    =  Feature film soundtrack
| Length      = 16:14
| Label       = Ashwini Audio
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Vinay Chandra composed the music for the soundtracks, and lyrics written by Ramnarayan and Kaviraj (lyricist)|Kaviraj. The album consists of four soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 16:14
| lyrics_credits = yes
| title1 = Ankitha
| lyrics1 = Ramnarayan
| extra1 = Karthik (singer)|Karthik, Anuradha Bhat
| length1 = 4:16
| title2 = Enu Papa Madide Kaviraj
| extra2 = Rajesh Krishnan
| length2 = 3:31
| title3 = Guru
| lyrics3 = Ramnarayan
| extra3 = Jaggesh
| length3 = 3:44
| title4 = Manase
| lyrics4 = Kaviraj
| extra4 = Vinay Chandra
| length4 = 4:43
}}

==Critical response==
Guru received positive to mixed response from critics upon its release. G. S. Kumar of The Times of India gave the film a rating of 3.5/5 and wrote of the direction, "Jaggeshs directorial debut has proved that he too can join the list of talented directors who have contributed some excellent movies to Sandalwood." He further wrote praising the "good script", "...   matured performance with expression and dialogue delivery", snd the musc and cinematography in the film.    Srikanth Srinivasa of Rediff.com|Rediff gave the movie 2.5/5, commenting that "Guru is worth a watch for the breezy narrative and some surprisingly well shot scenes and slick editing."  However NewstrackIndia rated the film 2/5 and said that the film did not live up to the expectations, but is watchable once. 

==References==
 

 
 
 
 
 