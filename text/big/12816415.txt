On the Town (film)
{{Infobox film
| name           = On the Town
| image          = On the Town poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Gene Kelly Stanley Donen
| producer       = Arthur Freed Roger Edens
| screenplay     = Adolph Green Betty Comden
| based on       =  
| starring       = Gene Kelly Vera Ellen Frank Sinatra Betty Garrett Ann Miller Jules Munshin
| music          = Leonard Bernstein Roger Edens Adolph Green Betty Comden Conrad Salinger  
| cinematography = Harold Rosson
| editing        = Ralph E. Winters
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $2,133,000  . 
| gross          = $4,428,000 
}} Broadway stage musical of Fancy Free which was also produced in 1944),    although many changes in script and score were made from the original stage version; for instance, most of Bernsteins music was dropped in favor of new songs by Edens, who disliked the majority of the Bernstein score, for being too complex and too operatic.  This caused Bernstein to boycott the film.

The film was directed by Gene Kelly and Stanley Donen, and  stars Kelly, Frank Sinatra, Ann Miller, Betty Garrett, Jules Munshin, and Vera-Ellen. It also features Alice Pearce and in  small, bit part, Bea Benaderet. It was a product of producer Arthur Freeds Unit at MGM, and is notable for its combination of studio and location filming, as a result of Gene Kellys insistence that some scenes be shot in New York City itself, including at the American Museum of Natural History, the Brooklyn Bridge, and Rockefeller Center.

The film was an instant success and won the Academy Award for Best Music, Scoring of a Musical Picture, and was nominated for a Golden Globe Award for Best Cinematography (Color). Screenwriters Comden and Green won the Writers Guild of America Award for Best Written American Musical.

Judy Holliday was uncredited as the voice of Daisy Simkins. 
 list of best musicals.

==Plot== New York, New York").
 Museum of Symphonic Hall.  Later, Chip sincerely falls for Hildy telling her "Youre Awful"&nbsp;– that is, awful nice to be with. That evening, all the couples meet at the top of the Empire State Building to celebrate a night "On the Town".
 cooch dancer, the friends tell a despondent Gabey, "You Can Count on Me", joined by Hildys hilarious roommate, Lucy Schmeeler. They have a number of adventures reuniting with Ivy at Coney Island before their 24-hour leave ends and they must return to their ship to head off to sea. Although their future is uncertain, the boys and girls share one last kiss on the pier as a new crew of sailors heads out into the city for their leave ("New York, New York" reprise).

==Main cast==
* Gene Kelly as Gabey
* Frank Sinatra as Chip
* Jules Munshin as Ozzie
* Ann Miller as Claire Huddesen
* Betty Garrett as Brunhilde "Hildy" Esterhazy
* Vera-Ellen as Ivy Smith
* Florence Bates as Madame Dilyovska
* Alice Pearce as Lucy Schmeeler
* George Meader as Professor
* Hans Conried as François (head waiter)

;Cast notes
* Carol Haney, Gene Kellys assistant, performed with Kelly in the Day in New York ballet sequence, but was not credited.  This was Carols screen debut 
* Bea Benaderet has a small, uncredited role as a girl from Brooklyn on the subway.  Bea also made her film debut in this film. 
* Bern Hoffman has an uncredited role as a shipyard singer.
* Alice Pearce was the only original member of the Broadway cast to reprise her role.

==Musical numbers==
 , Alice Pearce, Jules Munshin, Gene Kelly, Betty Garrett and Ann Miller perform "Count on Me"]]
# "I Feel Like Im Not Out of Bed Yet"&nbsp;– Shipyard builder New York, New York"&nbsp;– Gabey, Chip, and Ozzie (Original to Bernsteins score)
# "Miss Turnstiles Ballet" (instrumental)&nbsp;– Ivy and ensemble (Original to Bernsteins score)
# "Prehistoric Man"&nbsp;– Claire, Ozzie, Gabey, Chip, and Hildy
# "Come Up to My Place"&nbsp;– Hildy and Chip  (Original to Bernsteins score)
# "Main Street"&nbsp;– Gabey and Ivy
# "Youre Awful"&nbsp;–  Chip and Hildy
# "On the Town"&nbsp;– Gabey, Ivy, Chip, Hildy, Ozzie, and Claire
# "Count on Me"&nbsp;– Gabey, Chip, Ozzie, Hildy, Claire, and Lucy
# "A Day in New York" (instrumental)&nbsp;– Gabey, Ivy, and dream cast (Original to Bernsteins score)
# "New York, New York" (Reprise) &nbsp;– Shipyard builders, three new sailors, and chorus

 

==Reception==

===Box office===
According to MGM records the film earned $2,934,000 in the US and Canada and $1,494,000 overseas, resulting in a profit to the studio of $474,000. 

The film was also a critical success, receiving good reviews in various publications, including Variety and the New York Times.  

===Awards===
* Academy Awards, Best Music, 1950 (won)
* BAFTA Awards, Best Film, 1951 (nominated)
* Golden Globes, Best Cinematography&nbsp;– Color, 1950 (nominated)
* Writers Guild of America, Best Written American Musical, 1950 (won)

==Notes==

The musical numbers staged on location in New York were the first time a major studio had accomplished this.  The location shots in New York took nine days. 

The Breen Office of the MPAA refused to allow the use of the word "helluva" in the song "New York, New York", and so it was changed to "wonderful". 

==See also==
* Arthur Freed
* USS USS Swanson (DD-443)|Swanson, DD-443, the three sailors ship, which appears in the opening and closing scenes.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 