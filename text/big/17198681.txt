Loose Women (film)
{{Infobox Film
| name           = Loose Women
| image          = 
| image_size     = 
| caption        = 
| director       = Paul F. Bernard
| producer       = Paul F. Bernard Chris Matonti
| writer         = Sherry Ham
| narrator       = 
| starring       = Sherry Ham Melissa Errico
| music          = Pat Irwin
| cinematography = Peter Reiners
| editing        = Marie-Pierre Renaud
| distributor    = 
| released       = 18 July 1997 (Germany video premiere)
| runtime        = 91 min.
| country        = USA English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1996 romance/drama film directed by Paul F. Bernard. Sherry Ham-Bernard wrote and starred in the production. Charlie Sheen, Renee Estevez, Keith David, Giancarlo Esposito and Stephan Lang made cameo appearances.

== Cast ==
*Sherry Ham – Rachel
*Melissa Errico – Gail
*Marialisa Costanzo – Tracy
*Corey Glover – Jack
*Tom Verica – Det. Laurent
*Amy Alexandra Lloyd – Ann
*Robin Strasser – Mrs. Hayes
*Renee Estevez – Make-up lady
*Charlie Sheen – Barbie Lover Bartender
*Keith David – Stylist

== External links ==
*  

 
 
 
 
 


 