Death in High Heels
 
 
Death in High Heels is a 1947 British crime film directed by Lionel Tomlinson and starring Don Stannard, Elsa Tee and Veronica Rose.  It was based on a novel of the same title by Christianna Brand. It was a very early Hammer Films (here Marylebone-Hammer) production and was released through Exclusive Films, Hammers original incarnation. Its running time was just over 50 minutes. It was last shown on British television on Channel 4 in 1991.

==Plot==
The police are called in when a woman is murdered at a luxury Bond Street shop.

==Cast==
* Don Stannard - Detective Charlesworth
* Elsa Tee - Victoria David
* Veronica Rose - Agnes Gregory
* Denise Anthony - Aileen
* Patricia Laffan - Magda Doon
* Diana Wong - Miss Almond Blossom
* Nora Gordon - Miss Arris
* Bill Hodge - Mr Cecil
* Kenneth Warrington - Frank Bevan
* Leslie Spurling - Sergeant Bedd

==References==
 

==External links==
*  
 

 
 
 
 
 


 
 