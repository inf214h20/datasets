Cape Karma
{{Infobox film
| name = Cape Karma
| image = 
| image_size = 
| alt = 
| caption = Pankaj Advani
| producer = Brian Brake Pankaj Advani
| starring = Rahul Dev Audrie Woodhouse Tisca Chopra Gulshan Grover 
| music = 
| cinematography = 
| editing = 
| studio = 
| distributor = 
| released =  
| runtime = 79 minutes 
| country = United States
| language = English
| budget = US$1 million
| gross = 
}} Pankaj Advani, with a “dark secrets” plot, shot in Scotland. Surprisingly for an Indian production it contains topless nude scenes.

==Plot==
The film is about a man who comes to be in danger after he falls for the wife of a wealthy man.

==Cast==
*Rahul Dev	 ...	Manav
* Audrie Woodhouse	 ...Maya J. Mundhra
* Gulshan Grover	 ...J. Mundhra
* Tisca Chopra	 ...	Kamini
* Rajendra Gupta	 ...	Kaminis Dad
* Shri Vallabh Vyas	 ...Manavs Dad (as Shrivallabh Vyas)
* Vineet Sharma

==Production==
Shripal Morakia of iDream Production  gave Pankaj Advani a chance to make his stylized, surreal, psychological thriller Cape Karma.    Cape Karma, shot in the winter of 2005 in Dundee, Scotland with its non-linear story telling and undercurrents of violence and disconcerting sexual theme proved to be much ahead of its times for an Indian audience and is one of Pankajs least-seen films.

==Reception==
The film received negative reviews. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 

 