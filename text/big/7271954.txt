Blood & Chocolate (film)
 
{{Infobox film
| name = Blood & Chocolate 
| image = Blood and chocolateposter.jpg
| alt = 
| caption = Theatrical release poster
| director = Katja von Garnier
| producer = {{Plainlist|
* Wolfgang Esenwein
* Hawk Koch
* Gary Lucchesi
* Tom Rosenberg
* Richard S. Wright}}
| screenplay = {{Plainlist|
* Ehren Kruger Christopher Landon}}
| based on =  
| starring = {{Plainlist|
* Agnes Bruckner
* Hugh Dancy
* Olivier Martinez
* Katja Riemann
* Bryan Dick}}
| music = {{Plainlist|
* Reinhold Heil
* Johnny Klimek}}
| cinematography = Brendan Galvin
| editing = {{Plainlist| David Gamble
* Emma E. Hickox}}
| production companies = {{Plainlist|
* Daniel Bobker Productions
* Lakeshore Entertainment}}
| distributor = {{Plainlist|
* Entertainment Film Distributors  
* Metro-Goldwyn-Mayer  }}
| released =  
| runtime = 98 minutes  
| country = {{Plainlist|
* United States
* Germany
* Romania
* United Kingdom}}
| language = {{Plainlist|
* English
* Romanian}}
| budget = $15 million
| gross = $6.3 million 
}} Christopher Landon, 1997 Young-adult young adult novel of the same name. 
 United States, United Kingdom, Blood & Chocolate was both a commercial and critical failure.

==Plot==
Vivian (Agnes Bruckner) is a nineteen-year-old werewolf born in Bucharest, Romania to American parents who then moved back to America. When Vivian was nine years old, her parents and two siblings were killed by two hunters  who then proceeded to burn down their house. She then moved back to Bucharest to live with her aunt Astrid (Katja Riemann), who was the mate of the packs leader, Gabriel (Olivier Martinez) at that time. To Astrids distress, Gabriel left her after seven years in accordance with pack law to choose a new mate. The culmination of another seven years is only a few months away and Gabriel wants the reluctant Vivian as his.
 Tom Harper), John Kerr), and Willem (Jack Wilson), together known as The Five. Believing that she is telling him all their secrets- as seen by a drawing he did of her and wolves because he knew her as "The Wolf Girl"- and may grow to be a danger to their pack, Rafe tells Gabriel of them. Gabriel then tells Rafe that Aiden must leave or he must be dealt with.

Rafe lures Aiden to an abandoned church with the ruse that Vivian wanting to reconnect and attempts to scare him away. When this doesnt work, Rafe attacks and underestimates Aiden who defends himself and forces him back into a table where he cuts himself. Aiden, who did not know prior what Vivian and her friends were, sees the golden glow of the Loups-Garoux and realizes what hes been dating. The two fight, with Aiden attacking Rafe with a silver pendant and Rafe turning to a wolf, until Aiden eventually gains the upper hand and sends both over the rail, killing Rafe.

Afterwards, Aiden confronts Vivian about what she is, tempting her with his blood. She does not give in but is hurt that Aiden would think she was such a monster. Not long after, Aiden is captured by the pack to answer for killing Rafe, Gabriels son. He is made to run through the forest while being chased by the pack. If the pack catches him, he dies. If he makes it to the river and crosses it, he lives. Vivian is scared for him and changes into her wolf form, a white wolf, to save him from the rest of the pack. Aiden makes it to the river by confusing the pack, using his blood to spread his scent and make it harder for the pack to track him. Gabriel, however, is angry that Aiden made it to the river and attempts to follow him anyway, to kill him. Vivian helps to protect Aiden by throwing Gabriel off. Aiden, not realizing that the white wolf is Vivian, strikes her with a silver knife causing her to slowly die unless she gets an antidote. 

After hiding from the pack, Aiden and Vivian find the pharmacist who has the antidote for the silver poisoning and steals it from him, but not before he calls the rest of the pack. After being chased, Vivian tells Aiden to save himself and is captured by the pack. She is held in a cage and taunted by the rest of the five while Gabriel attempts to curve her to his way of thinking. Aiden comes to Vivians rescue and in the end Vivian has to kill Gabriel.

Aiden and Vivian go towards the age of hope. Driving past other Loups-Garous, the wolves are shown to bare their necks in respect to Vivian and Aiden, showing Vivian to possibly be the new leader of the pack.

==Cast==
 
* Agnes Bruckner as Vivian
* Hugh Dancy as Aiden
* Olivier Martinez as Gabriel
* Katja Riemann as Astrid
* Bryan Dick as Rafe
* Chris Geere as Ulf Tom Harper as Gregor John Kerr as Finn
* Jack Wilson as Willem
* Vitalie Ursu as Constani
* Bogdan Voda as Albu
* Kata Dobó as Beatrice
* Maria Dinulescu as Girl in Red
* Sandu Mihai Gruia as Pharmacist
 

==Production== Christopher Landon, whose father Michael Landon had a leading role in the film I Was a Teenage Werewolf (1957). 

Author Annette Curtis Klause was not kept up to date by the producers of the film. She had to find the information about the filming on the Web. 

Principal photography was set in Bucharest historic part of the city and at MediaPro Studios in Buftea. However, as many of the American films based in Bucharest, the film failed to be accurate in presenting the real places in city, for example the Piata Romana (Romana Square) is actually the Curtea Veche yard (Old Court, a destroyed old palace), or Biserica Silvestru (Silvestru Church, in downtown Bucharest) is actually a church in Stirbey Palace, Buftea, a few tens of kilometres west of Bucharest.

===Music===
 
The Film score|films score was composed by Reinhold Heil and Johnny Klimek. The soundtrack consists of fifteen songs, none of which are featured in the film.

;Songs featured in the film
# "Garab" - Rachid Taha
# "Let Yourself Go Wild" - Jasmin Tabatabai
# "Velvet Hills" - Katja Riemann
# "You Know the Truth" - Aurah
# "Cash Machine" - Hard-Fi
# "Amor Fati" - Aurah
# "Silence Summons You" - The Sofa Club
# "Eu Te Iubesc Prea Mult" - Nicolae Guta
# "Stand My Ground" - Within Temptation

==Release==
===Box office===
Blood & Chocolate opened on January 26, 2007 in 1,200 theaters and earned $2,074,300 in its opening weekend, ranking number 16 in the domestic box office.  By the end of its run, a little over two months later, the film had grossed $3,526,847 domestically and $2,784,270 overseas for a worldwide total of $6,311,117.   

===Critical reception===
The film was panned by critics. On review aggregator website Rotten Tomatoes, the film has an 11% rating based on 63 reviews.  On Metacritic, the film has a 33 out of 100 rating from 16 critics, indicating "generally unfavorable reviews". 

==Footnotes==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 