Day Trip (film)
{{Infobox film name           = Day Trip  image          = Day_Trip_poster.jpg director       = Park Chan-wook    Park Chan-kyong  producer       = Park Chan-wook   Park Chan-kyong writer         = Park Bae-il   Park Chan-wook   Park Chan-kyong starring       = Song Kang-ho   Jeon Hyo-jung  music          =   cinematography =    editing        = Kim Sang-beom   Kim Jae-beom distributor    =  released       =   runtime        = 18 minutes country        = South Korea language       = Korean budget         =  gross          =  film name      = {{Film name hangul         =    hanja          =   rr             = Cheongchul-eoram mr             = Chŏngchul-ŏram}}
}}
Day Trip ( ) is a 2012 South Korean short film co-directed by Park Chan-wook and Park Chan-kyong. The film features a pansori master (Song Kang-ho) and his student (Jeon Hyo-jung). 

The 18-minute short was released online on December 28, 2012 on the website of outdoor clothing brand Kolon Sport, and was funded by the company to mark its 40th anniversary.
 Night Fishing, Berlin International Film Festival in 2011. 

==Plot==
A master (Song Kang-ho) and his student (Jeon Hyo-jung) visit a mountain to practice pansori following a music competition that left the trainee disappointed.

==Production== Mount Namsan Korean folk music that harbors a range of emotions, including han (cultural)|han, repressed bitterness and powerlessness that many say lingers in the Korean people. Day Trip highlights the traditional musics harmonious nature with the surrounding scenery by featuring a version of Sae Taryeong ("Song of Birds").   

Outdoor clothing brand Kolon Sport had commissioned and funded the short for its 40th anniversary "Way to Nature Film Project." But the short has almost no promotional elements because the company gave the Parks directorial autonomy. Rather than rendering typical outdoor images, they focused on presenting pansori in harmony with the surrounding nature and civilization. 

The characters trip to the mountain appears to be part of a 100-day practice session required for pansori singers in which they train their voices in the unique acoustic setting of the waterfalls, cliffs and caves. They sing so loudly their voices usually go hoarse and their bodies swell until they feel blood in their throats. In recent years, this kind of training may be seen as a way to permanently injure vocal cords, but they pushed themselves in isolation for 100 days believing that the efforts would ultimately makes ones voice suitable for pansori. 

The short was the debut film for 15-year-old Jeon Hyo-jung. The actress, a pansori major at the National Middle School of Traditional Arts, was selected for the role through a competitive audition. In order to play a man much older than himself, prior to each shoot actor Song Kang-ho had to undergo intense make-up sessions under make-up artist Song Jong-hee, who was previously recognized for her work on the film A Muse|Eungyo. 

==References==
 

==External links==
*   at Kolon Sport  
*  
*  
*  

 

 
 
 
 