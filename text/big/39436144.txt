Kill the Messenger (2014 film)
 
{{Infobox film
| name           = Kill the Messenger
| image          = Kill the Messenger poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Cuesta
| producer       = {{Plainlist|
* Pamela Abdy
* Naomi Despres
* Jeremy Renner
* Scott Stuber}}
| writer         = Peter Landesman
| based on       =  
| starring       = {{Plainlist|
* Jeremy Renner
* Rosemarie DeWitt
* Ray Liotta
* Tim Blake Nelson
* Barry Pepper
* Oliver Platt
* Michael Sheen
* Michael K. Williams
* Mary Elizabeth Winstead
* Andy García}} Nathan Johnson
| cinematography = Sean Bobbitt
| editing        = Brian A. Kates
| studio         = {{Plainlist|
* Bluegrass Films
* The Combine}}
| distributor    = Focus Features
| released       =  
| runtime        = 112 minutes  
| country        = United States
| language       = English
| budget         = $5 million 
| gross          = $2.5 million   
}} crime Thriller thriller film book of the same name by Nick Schou and the book Dark Alliance by Gary Webb. The film stars Jeremy Renner (in his first film as a producer), Michael Sheen, Andy Garcia, Ray Liotta, Barry Pepper,  Mary Elizabeth Winstead, Rosemarie DeWitt, Paz Vega, Oliver Platt, Richard Schiff, and Michael K. Williams. The film was released on October 10, 2014. 

==Plot==
Based on the true story of journalist Gary Webb, the film takes place in the mid-1990s. Webb uncovered the CIAs alleged role in importing crack cocaine into the U.S. to secretly fund the Nicaraguan contra rebels. Despite enormous pressure to stay away, Webb chose to pursue the story and went public with his evidence, publishing the series called "Dark Alliance". He then experienced a vicious smear campaign fueled by the CIA, during which he found himself defending his integrity, his family and his life. 

==Cast==
 
* Jeremy Renner as Gary Webb
* Rosemarie DeWitt as Susan Webb
* Ray Liotta as John Cullen
* Barry Pepper as Russell Dodson
* Mary Elizabeth Winstead as Anna Simons
* Paz Vega as Coral Baca
* Oliver Platt as Jerry Ceppos
* Michael Sheen as Fred Weil
* Richard Schiff as Walter Pincus
* Andy García as Norwin Meneses
* Robert Patrick as Ronny Quail
* Michael K. Williams as "Freeway" Rick Ross
* Jena Sims as Little Hottie
* Joshua Close as Rich Kline Danilo Blandon
* Robert Pralgo as Sheriff Nelson
* Lucas Hedges as Ian Webb
* Michael Rose as Jonathan Yarnold
* Matthew Lintz as Eric Webb
* Michael H. Cole as Pete Carey
* David Lee Garver as Douglas Farah
* Andrew Masset as Johnathan Krim
 

==Production==
On March 5, 2014, Focus Features announced that the film would be released on October 10, 2014.   

===Filming=== Georgia locations, Cobb County and Decatur, Georgia|Decatur.  

===Music=== Nathan Johnson score for the film,  and Back Lot Music release a soundtrack album on October 7, 2014. 

==Release==
Kill the Messenger received a regional theatrical release on October 10, 2014.  

===Critical reception===
The film has received positive reviews from critics. On Rotten Tomatoes, the film holds a "Certified Fresh" rating of 77%, based on 95 reviews, with an average rating of 6.8/10. The sites consensus reads, "Kill the Messengers potent fury over the tale of its real-life subject overrides its factual inaccuracies and occasional narrative stumbles."  On Metacritic, the film currently has a rating of 60 out of 100, based on 33 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at History vs. Hollywood

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 