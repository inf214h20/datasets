Chintamani (1937 film)
 
{{Infobox film
| name           = Chintamani
| image          = Chintamani_1937_film.jpg
| caption        =  Chintamani (Aswathamma), Lord Krishna (Serugulathur Sama) and Bilwamangal(M. K. Thyagaraja Bhagavathar)
| director       = Y. V. Rao
| writer         = Screenplay:Y. V. Rao Dialogue: Somayajulu, Serugalathur Sama
| starring       = Aswathamma M. K. Thyagaraja Bhagavathar Serugulathur Sama Y. V. Rao L. Narayana Rao
| producer       = Royal Talkies
| distributor    = Royal Talkies
| music          = Papanasam Sivan
| cinematography =  B. Washgar
| editing        =
| released       = 12 March 1937
| runtime        = 215 min. (19,501 Feet) Tamil
}}

Chintamani ( ) is a 1937 Tamil cinema|Tamil-language film directed by Y. V. Rao starring M. K. Thyagaraja Bhagavathar, Serugulathur Sama and Aswathamma.       It was the first Tamil film to run for a year in a single theatre.             

==Production==
Chintamani was a popular play which had been performed in many languages. First, a silent film was made based on the play, then talkies based on it were made in Bengali, Hindi and Telugu. In 1937, a Tamil version of the film was directed by film-maker Y. V. Rao under the banner of Royal Talkies, owned by yarn merchants of Madurai. 

===Casting===
Initially, the director Y. V. Rao, wanted to play Bilwamangals role himself.  However, he changed his mind and acted as Bilwamangals companion Manoharan. Serugulathur Sama was another contender for the main role. But, Rao rejected him in favor of M. K. Thyagaraja Bhagavathar who was signed to play the part. In the initial stages, more publicity was given to the Kannada actress Aswathamma who played Chintamanis role than M. K. Thyagaraja Bhagavathar. Her name was mentioned above that of Bhagavathars in the credits.

==Plot==
Chintamani was based on the legendary story of a Sanskrit poet and devotee of Lord Krishna named Bilwamangal (M. K. Thyragaraja Bhagavathar). Bilwamangal, a resident of Varanasi, was a Sanskrit scholar, who gets infatuated towards a courtesan called Chintamani (Aswathamma), a woman of ill-fame.   As a result he deserts his wife.  However, Chintamani is an ardent devotee of Lord Krishna (Serugalathur Sama) and spends most of her time singing bhajans in praise of Lord Krishna.  His attraction towards Chintamani eventually draws Bilwamangal closer towards Lord Krishna and transforms his life forever. Bilwangal, himself, becomes a devotee of Lord Krishna and pens a monumental Sanskrit work Sri Krishna Karnamritam.  

==Cast and crew==
 
* Aswathamma ... Chintamani
* M. K. Thyagaraja Bhagavathar ... Bilwamangal
* L. Narayana Rao ... Alwar Chetty
* S. S. Rajamani
* Serugalathur Sama ... Actor (as Lord Krishna) and Dialogue
* Y. V. Rao ... Actor (as Manoharan), Director and Script writer
* Somayajulu - Dialogue
* Papanasam Sivan - Music, Lyrics
* B. Washgar - Cinematographer
* M. Varma - Art Director
* Movi Guha - Still Photographer   

==Release and reception==
 

Chintamani was released on 12 March 1937  and became one of the most acclaimed films of early Tamil cinema. Though Bhagavathars first film Pavalakkodi (1934 film)|Pavalakkodi had achieved some success, it was Chintamani that made Bhagavathar into a successful actor. It had an uninterrupted theatrical run of more than a year.  It was one of the two films of Bhagavathar, released in 1937 (the other one was Ambikapathy (1937 film)|Ambikapathy) which ran for more than a year. It also marked the debut in Tamil for Kannada actress Aswathamma who played the title role. Aswathamma acted in one more Tamil movie before her untimely death in 1939 due to tuberculosis. 
 Kalki wrote that the film has made such an impact on the viewers that the housewife would sing the song Mayaprapanchattil from the movie while preparing coffee in the morning and her husband would sing Rathey unakku kobam in order to please his sweetheart. 
 Sinhala film music to this day. 

Writing in Eelakesari magazine in April 1938, Pudumaipithan praised the film as follows:

  }}

==Soundtrack==
Partial list of Songs from Chintamani:
* Radhae Unakku Kobam Aagathadi
* Gnana kann
* Krishna Krishna
* Divya Darishanam
* Maya prapanchathil
* Nadagame Ulagam

==References==
 

 
 
 
 
 
 