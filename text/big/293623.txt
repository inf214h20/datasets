Secret Agent (1936 film)
{{Infobox film 
| name           = Secret Agent 
| image          = Secret Agent (1936 film) poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Alfred Hitchcock
| producer       = Michael Balcon Ivor Montagu Charles Bennett Ian Hay
| story          = W. Somerset Maugham Robert Young Lilli Palmer
| cinematography = Bernard Knowles
| editing        = Charles Frend 
| released       = May 1936 (U.K.) June 15, 1936 (U.S.)
| runtime        = 86 minutes 
| country        = United Kingdom
| language       = English
}}
 Robert Young. The Lady Vanishes two years later. This was also Michael Rennies film debut (uncredited).

Typical Hitchcockian themes used here include mistaken identity and murder.

==Plot== First World War, only to discover his obituary in the newspaper. He is brought to a man identifying himself only as "R", who asks him to undertake a secret mission: to identify and eliminate a German agent on his way to Arabia to stir up trouble in the Middle East. Upon agreeing, Brodie is given a new identity (Richard Ashenden), a fake death, and the assistance of a killer known variously as "the Hairless Mexican" and "the General" (Lorre), though he is neither bald, Mexican or a general. 

Brodies late "predecessor" thought that the enemy agent is staying at the Hotel Excelsior in neutral Switzerland. When "Ashenden" arrives there, he is surprised to find that "R" has also provided him with an attractive wife, Elsa Carrington (Carroll). Entering their suite, he also encounters her new admirer, fellow hotel guest Robert Marvin (Young), who is only slightly deterred by the arrival of her husband (and continues to flirt with Elsa for much of the film). When they are alone, Ashenden is displeased when Elsa reveals she insisted upon the assignment for the thrill of it.

Ashenden and the General go to contact a double agent, the church organist, only to find him dead. In his hand, however, they find a button, evidently torn off in the struggle. When they go to the casino to meet Elsa, the button is accidentally dropped onto a gambling table. Since it looks the same as his own buttons, Caypor assumes it is his.

The agents persuade experienced mountaineer Caypor to help them settle a concocted bet: which one of them can climb higher on a nearby mountain. As the moment approaches, Ashenden finds he is unable to commit cold-blooded murder, but the General has no such qualms and pushes the unsuspecting Caypor off a cliff. 

However, a coded telegram informs them that Caypor is not their target. The General finds it very funny, but Elsa becomes terribly distraught when they are told. She decides to quit, despite having told Ashenden that she fell in love with him at first sight. In the lobby, she encounters Marvin. With no destination in mind, she persuades him to take her along with him. Meanwhile, the other two bribe a worker at a chocolate factory (the secret "German spy post office") to show them a very important message received the day before. They discover that it is addressed to none other than Marvin. 

They set out in pursuit, taking the same train as Marvin and Elsa. Before they can arrange anything, they cross the border into Turkey - enemy territory - and a large number of soldiers board. Despite this, they manage to get Marvin alone in his compartment. Elsa tries to persuade them not to kill Marvin. Before Ashenden can do anything, one way or the other, the train is attacked and derailed by airplanes sent by "R". Marvin is pinned in the wreckage, but manages to fatally shoot the General before dying. The "Ashendens" quit the spy business.

==Cast==
* John Gielgud as Captain   Brodie/Richard Ashenden
* Peter Lorre as The General
* Madeleine Carroll as Elsa   Robert Young as Robert Marvin
* Percy Marmont as Caypor Florence Kahn as Mrs. Caypor Charles Carson as "R"
* Lilli Palmer as Lilli

==Reception==
The film was voted the fifth best British film of 1936. 

==References==
 

==External links==
*  
*  
* Elisabeth Weis, 1982, The Silent Scream — Alfred Hitchcocks Sound Track (1982),   — a discussion of Hitchcoocks use of sound in the film.
* Movie Diva,  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 