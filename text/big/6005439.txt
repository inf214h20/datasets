Nenunnanu
{{Infobox film
| name           = Nenunnanu
| image          = nenunnanudvd.jpg
| caption        =
| writer         = Paruchuri Brothers  
| story          = Bhupathi Raja
| screenplay     = V. N. Aditya 
| producer       = D. Sivaprasad Reddy
| director       = V. N. Aditya Nagarjuna Akkineni Shriya Saran Arti Agarwal
| music          = M. M. Keeravani
| cinematography = J. Siva Kumar
| editing        = Marthand K. Venkatesh
| studio         = Kamakshi Movies
| released       =  
| runtime        = 153 minutes
| country        =   India
| language       = Telugu
| budget         =
| gross          =
}}  
 Telugu film Nagarjuna Akkineni, Shriya Saran, Arti Agarwal in the lead roles and music by M. M. Keeravani was a big asset to the film. This movie was then dubbed in Tamil as Chandramadhi and in Hindi as Vishwa - The He Man. The film recorded as Super Hit at box-office.

== Plot == Nagarjuna Akkineni) is an orphan and a contractor at Vizag port. Anu (Shriya Saran) is a student in classical singing. Sruthi (Arti Agarwal) is Anus friend. Anu makes an attempt to elope with her boyfriend Arun. Arun is the son of a Business tycoon JP (Mukesh Rishi). JP sends police across to nab Anu. As police nab Arun, Venu rescues Anu. Anus father disassociates himself from his daughter. Then Venu takes Anu to his place and gives assurance. Venu finds out where Arun is and gets Anu married to him. JP tells his son that Venu and Anu have an affair as they live together in the same house. Arun gets suspicious about Anus character and leaves the marriage venue immediately after the marriage. Anu is back in Venus place. Venu is in the mission of locating Arun and convince him to come back to Anu. As Anu and Venu spend more time together, they get closer. Venu never takes advantage of it and treats Anu like a good friend. Anu pines for Arun but comes to know that Arun is getting married to the Ministers daughter. Venu goes to the engagement venue and tries to stop Arun from getting engaged. Anu comes and slaps Arun, saying that their marriage is over. She goes for a singing competition with Venu accompanying her. She wins the competition as well as her fathers love. Arun, meanwhile tries to kill Venu but accidentally gets an electric shock. He gets paralysed for life. JP seeks revenge. Sruthi falls for Venu. Anu goes back to her fathers house and now, being unmarried, thinks over her relationship with Venu. She realises that she loves Venu. Venu, too, reveals among his friends that he loves Anu, but will never tell her. Sruthi overhears both Anu and Venus feelings and decides to sacrifice her love for Venu. Her parents decide to get her married to Venu. Sruthi refuses to get married to Venu and tells them everything. Sruthis mother gets angry and asks Anu to arrange her daughters and Venus wedding. Anu gets heartbroken but thinks that its better for everyone. Sruthi goes to Venu and tells him that Anu too loves him. Anu is kidnapped by JP and his goons.

Venu goes to save Anu. After a big fight, he is able to defeat JP and save Anu but Anu is stabbed. Sruthi comes and she takes Anu to the hospital. Venu comes to the hospital later. Anu asks the doctors to allow her to meet Venu before treating her. Anu and Venu confess their love for each other. Anu is successfully operated and finally united with Venu, with everyones, including Sruthis mothers wishes.

==Cast==
{{columns-list|3| Nagarjuna Akkineni as Venu Madhav
* Shriya Saran as Anu
* Arti Agarwal as Sruthi Anitha as Special Appearance
* Mukesh Rishi as JP
* Subbaraju as Arun
* Brahmanandam  Sunil
* Ali 
* Tanikella Bharani  Paruchuri Venkateswara Rao 
* M. S. Narayana 
* Dharmavarapu Subramanyam 
* Ravi Babu 
* Siva Reddy 
* Pasupati 
* Ananth
* Hema Sundar 
* Naidu Gopi
* Jenny 
* Sudha 
* Siva Parvathi 
* Neharika 
* Likhitha Yamini 
* Swathi 
* Deepthi 
* Dolly 
* Master Anand Vardhan as Young Venu 
* Baby Sri Vibha as Young Sruthi
* Baby Nishiptha as Young Anu
}}

==Soundtrack==
{{Infobox album
| Name        = Nenunnanu
| Tagline     = 
| Type        = film
| Artist      = M. M. Keeravani
| Cover       = 
| Released    = 2004
| Recorded    = 
| Genre       = Soundtrack
| Length      = 33:32
| Label       = Aditya Music
| Producer    = M. M. Keeravani
| Reviews     =
| Last album  = Naa Autograph   (2004)
| This album  = Nenunnanu   (2004)
| Next album  = Sye (2004 film)|Sye   (2004)
}}
The music was composed by M. M. Keeravani. All songs are blockbusters. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 33:32
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ettago Unnadi Sirivennela Seetarama Sastry  Chitra 
| length1 = 4:51

| title2  = E Shwasalo Sirivennela Seetarama Sastry  Chitra
| length2 = 5:08

| title3  = Nee Kosam Sirivennela Seetarama Sastry
| extra3  = KK (singer)|KK, Shreya Ghoshal
| length3 = 5:30

| title4  = Nenunnanani Chandrabose  Sunitha
| length4 = 3:31

| title5  = Ryali Ravulupadu Chandrabose  Sunitha
| length5 = 5:33

| title6  = Intha Dhooramochinaka Chandrabose
| extra6  = Tippu (singer)|Tippu, Shreya Ghoshal
| length6 = 4:33

| title7  = Nuziveedu Chandrabose 
| extra7  = Arnod Chakravarthy, Shreya Ghoshal
| length7 = 4:20
}}

==Box-office performance==
* The film had a 50-day run in 125 centres and a 100 day run in 42 centres.It was one of the highest grosser in nagarjunas career while major business was from B and C centers  

==References==
 

==External links==
*  

 

 
 
 