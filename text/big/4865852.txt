Double, Double, Toil and Trouble
{{Infobox film name           = Double, Double, Toil and Trouble image          = Double, Double, Toil and Trouble.jpg caption        = DVD cover director       = Stuart Margolin producer       = writer         = Jurgen Wolff starring       = Mary-Kate Olsen Ashley Olsen music          = Richard Bellis cinematography = editing        = distributor  ABC
|released       =   runtime        = country        = United States language       = English budget         =
}}
Double, Double, Toil and Trouble is a 1993 Halloween made-for-television childrens film. It stars Mary-Kate and Ashley Olsen as two adventurous little girls who discover that their Great Aunt Sophia has been trapped and cursed by her evil twin sister Agatha. On the 7th year of her imprisonment, Sophia will be doomed to the netherworld unless the spell is broken by the magical spell of twins. The films title is part of the famous line spoken by the three witches in William Shakespeare|Shakespeares Macbeth (Act IV, Scene I): "Double, double toil and trouble; Fire burn and cauldron bubble."

==Plot==

Don Farmer and his wife Christine are deeply in debt. They have utilized all of their sources but are still in danger of losing their home. They have twin daughters, Kelly and Lynn (played by Mary-Kate Olsen|Mary-Kate and Ashley Olsen respectively) who are tired of being twins and want to be looked upon as individuals. The Farmers decided to ask Christine’s Aunt Agatha, Christines fathers sister, who is a cruel and cold woman, for a loan, which is refused. Agatha too, has a twin sister; her name is Sophia, but she has been missing for many years with no clue of her whereabouts. (Both sisters are portrayed by Cloris Leachman.)

Upon the visit to Agatha’s home, Mr. Grave Digger, a man employed for Agatha, reveals a story about Agatha and her sister. He explains to the girls that Agatha’s home once belonged to a powerful witch who, before being burned at the stake 200 years before, had hidden her moonstone, the gem which gave her power, in the home. As children, Agatha and Sophia, tired of being twins, heard the tale and began looking for the stone, in hopes of using its power to no longer be twins. Agatha found a moonstone and upon finding it, hides it from her sister and begins using the magic it possesses to make her sister’s life miserable. Years later, on Halloween, Sophia and her fiancé George, Agathas butler, prepare to elope and begin their life together. Agatha, out of jealously and rage, casts a spell that banishes her sister into the netherworld through a mirror, which she keeps hidden in the attic. Sophia’s only form of communication or link to the outside world was through a mirror that Agatha kept hidden in an old attic. On the 7th year, at midnight, the spell will become permanent, and there will be no way for Sophia to be rescued.

As the Farmer twins learn of their parents financial problems and Aunt Sophia, they begin a rescue mission. The spell can only be broken by twins who have possession of the moonstone and Kelly and Lynn’s ultimate goal is to apprehend it. During their journey they are joined on by a clown named Oscar who wishes to be taller, the cowardly gravedigger, and a poor man named Mr. N who would do anything for money. The girls also come into the possession of a toy magic wand that has unexplained genuine magical powers.

The cruel Aunt Agatha does everything in her power to get rid of the twins. She is threatened by their presence because she knows that the power of twins combined is superior to her own. Eventually she attempts to poison them with jealousy and resentment toward one another. Agatha attempts to persuade Lynn into betraying her sister. However, in the end, Lynn decides her sister is the most important person in her young life. After the twins get hold of the moonstone and return to Agathas mansion, they attempt to free Sophia, but the magic doesnt work. Lynn and Kelly tell each other that they still love each other and want to be sisters; the power of love and loyalty transcends all, and Sophia is finally freed. Enraged, Agatha attempts to push her sister back into the mirror. The twins and their friends fight back, and Agatha falls into the mirror herself. All of Agathas evil magic is undone and the mirror is shattered, thus dooming her to spend the rest of her life in solitude in the netherworld.

After discovering the girls have gone missing, Don and Christine return to the mansion to find Sophia safe and happily returned. Everyone keeps what happened secret, preferring to tell Don and Christine that Agatha went on a long trip to "reflect". Aunt Sophia gladly agrees to give the Farmers the money they need to save their home. Some time later, Lynn and Kelly are cleaning up the broken mirror in the attic, and they see Aunt Agatha in one of the broken pieces. She asks for help, but the twins say, "No chance" and walk out of the attic while holding hands.

The movie ends with Aunt Agatha shouting, "I hate Halloween!"

==Cast==

* Mary-Kate Olsen ... Kelly Farmer/Young Aunt Sophia
* Ashley Olsen ... Lynn Farmer/Young Aunt Agatha
* Cloris Leachman ... Aunt Agatha/Aunt Sophia
* Phil Fondacaro ... Oscar
* Eric McCormack ... Don Farmer
* Kelli Fox ... Christine Farmer
* Wayne Robson ... Gravedigger Matthew Walker ... George
* Meshach Taylor ... Mr. N
* Denalda Williams ... Hostess Gary Jones ... Bernard Brewster
* Babs Chula ... Madame Lulu
* Bill Meilen ... Chairperson
* Nora McLellan ... Female cop
* Alex Green ... Pumpkin driver
* Alek Diakun ... Doorman
* Claire Kaplan, Karin Konoval, Glynis Leyshon ... Witches
* Mitch Kosterman ... Cop
* Gary McAteer ... Fred
* Eliza Centenera ... Girl
* Christopher Anderson  ... Boy
* Lynda Boyd ... Singer
* Ian Bagg ... Fat man
* Freda Perry ... Girls mother
* Shawn Michaels ... Fred Hilton

 Internet Movie Database 

==Release==
 UK September 20, 2004.
The movie was also released in other countries in which it was renamed:
::Abracadabra	              (Spain)
::Bruxinhas ao Ataque	          (Portugal)
::Czary-mary	              (Poland)
::Dia das Bruxinhas - Feitiço das Gêmeas	(Brazil)
::Doubles jumelles, doubles problèmes	(France)
::Feitiço das Gêmeas	     (Brazil)
::Halloween Twins - Jetzt hexen sie doppelt	(Germany)
::Két tojás száz bajt csinál	     (Hungary)
::Uma Confusão das Bruxas	    Brazil) (cable TV title)
::Doble Doble Hechizo (Mexico)

 

==Awards and nominations==
The movie had positive reviews with its 1993 debut. It was nominated for two awards. In 1994, there was an Emmy Award nomination for Outstanding Individual Achievement in Music Composition for a Miniseries or a Special (Dramatic Underscore) for composer Richard Bellis. In the same year, Mary-Kate and Ashley Olsen ware also nominated and won the Young Artist Award for Best Youth Actress in a TV Mini-Series, M.O.W. or Special. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 