The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies
 
{{Infobox film
| name           = The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies
| image          = Incrediblystrangecreatures.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Ray Dennis Steckler	
| producer       = Ray Dennis Steckler
| writer         = 
| screenplay     = Gene Pollock Robert Silliphant
| story          = E.M. Kevke
| starring       = Cash Flagg Carolyn Brandt Brett OHara Atlas King Sharon Walsh Madison Clarke
| music          = André Brummer Libby Quinn
| cinematography = Joseph V. Mascelli
| editing        = Don Schneider
| studio         = 
| distributor    = Fairway International Pictures
| released       =  
| runtime        = 82 mins.
| country        = United States
| language       = English
| budget         = $38,000
| gross          =
}}

The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies (sometimes "!!?" is appended to the title) is a 1964 American monster movie written and directed by Ray Dennis Steckler. Steckler also starred in the film, billed under the pseudonym "Cash Flagg".
 carnival and stumble into a group of occultists and disfigured monsters.  Produced on a $38,000 budget, much of it takes place at The Pike amusement park in Long Beach, California, which resembles Brooklyns Coney Island. The film was billed as the first "monster musical film|musical", beating out The Horror of Party Beach by a mere month in release date.

== Plot ==
Jerry (Steckler as "Flagg"), his girlfriend Angela (Sharon Walsh), and his buddy Harold (Atlas King) head out for a day at the carnival.  In one venue, a dance number is performed by Marge (Carolyn Brandt), an alcoholic who drinks before and between shows, and her partner, Bill Ward, for a small audience.  There Jerry sees stripper Carmelita (Erina Enyo) who hypnotizes him with her icy stare and he is compelled to see her act. Carmelita is the young sister of powerful fortune-teller Estrella (Brett OHara), and Estrella turns Jerry into a zombie by hypnotizing him with a spiraling wheel.   He then goes on a rampage, killing Marge and fatally wounding Bill.  Later, Jerry  attempts to strangle his girlfriend Angela as well. It develops that Estrella, with her henchman Ortega (Jack Brady), has been busy turning various patrons into zombies, apparently by throwing acid on their faces.  

Interspersed through the film are several song-and-dance production numbers in the carnivals nightclub, with songs like "Choo Choo ChBoogie" and "Shook out of Shape". The titular zombies only make an appearance in the final act, where they escape and immediately kill Estrella, Carmelita, Ortega and several performers before being shot by police. Jerry, himself partially disfigured but not a zombie, escapes the carnival and is pursued to the shoreline, where the police shoot him dead in front of Angela and Harold.

==Production==
 
=== Title ===
At the time of release, The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies was the second longest titled film in the horror genre (Roger Cormans The Saga of the Viking Women and Their Voyage to the Waters of the Great Sea Serpent being the first). 

This was not, however, the originally intended title of the film.  As Steckler relates, the film was supposed to be titled The Incredibly Strange Creatures, or Why I Stopped Living and Became a Mixed-up Zombie, but was changed in response to  , which was under production at the time. {{cite video
  | people = Commentary by Ray Dennis Steckler date = 2006
  | title = The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies
  | medium = DVD
  | publisher = Media Blasters
  | time = 
  | isbn = 1-58655-550-2
  | oclc = 213513846
}} 
 roadshowed the picture across the US.  In order to get repeat customers, Steckler re-titled the film numerous times, with titles such as The Incredibly Mixed-Up Zombie, Diabolical Dr. Voodoo and The Teenage Psycho Meets Bloody Mary. 

=== Notable cast and crew ===
Brett OHara was usually a stand-in for Susan Hayward. Madame Estrella was the only "real" role of her career. 

Sharon Walsh was not originally meant to play Angela. Bonita Jade was given the role, but when it was time for her scene, she said she had to leave to meet her boyfriend, because he was performing and she always went to his gigs. Steckler was furious, and he pulled Walsh out of the chorus line, telling her she was now the female lead. Sharon had already appeared in several dance numbers during the movie and they had to "disguise" her with a new hairstyle. 
 Academy Award for his work on Close Encounters of the Third Kind;  and László Kovács (cinematographer)|László Kovács (listed as Leslie Kovacs). 

=== Studio === midway scenes IATSE union agents, who found it difficult to climb the stairs to the seventh floor main stage. 

===Budget===
During the filming of the movie, Steckler was in terrible need of funds, both for the movie and for rent, food and basic needs. Atlas King, who had grown close to Steckler, gave him three hundred dollars out of his own pocket.  The station wagon Jerry drives in this movie was the Steckler family car. 

== Reception ==
 
In some screenings, employees in monster masks, sometimes including Steckler himself, would run into the theater to scare the audience (The gimmick was billed as "Hallucinogenic Hypnovision" on the films posters).  

The 2004 DVD The 50 Worst Movies Ever Made listed this film as the worst film of all time.  
 camp or rock critic Lester Bangs wrote an appreciative 1973 essay about Incredibly Strange Creatures in which he tries to explain and justify the movies value:
{{Cquote|...this flick doesnt just rebel against, or even disregard, standards of taste and art. In the universe inhabited by The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies, such things as standards and responsibility have never been heard of. It is this lunar purity which largely imparts to the film its classic stature. Like Beyond the Valley of the Dolls and a very few others, it will remain as an artifact in years to come to which scholars and searchers for truth can turn and say, "This was trash!" {{cite book
  | author =Lester Bangs
  | authorlink = Lester Bangs
  | editor = Greil Marcus
  | title = Psychotic Reactions and Carburetor Dung
  | year = 1987
  | publisher = Random House
  | location = New York
  | isbn = 0-394-53896-X
  | chapter = The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies, or, The Day The Airwaves Erupted
  | page = 122
}} 
}}

==DVD release==
The DVD release of Incredibly Strange Creatures features a commentary track by "drive-in movie critic" Joe Bob Briggs. {{cite video
  | people = Commentary by Joe Bob Briggs date = 2006
  | title = The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies
  | medium = DVD
  | publisher = Media Blasters
  | time = 
  | isbn = 1-58655-550-2
  | oclc = 213513846
}} 

==In popular culture==
A key group of the 1970s Melbourne post-punk little band scene named themselves after the film. 

The film was lampooned in 1997 by Mystery Science Theater 3000.

==Notes==
 

==References==
* {{cite video
  | people = Other unspecified commentary and interviews date = 2006
  | title = The Incredibly Strange Creatures Who Stopped Living and Became Mixed-Up Zombies
  | medium = DVD
  | publisher = Media Blasters
  | time = 
  | isbn = 1-58655-550-2
  | oclc = 213513846
}}

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 