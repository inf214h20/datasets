498A: The Wedding Gift
 
{{Infobox film
| name           = 498A: The Wedding Gift
| image          = 
| alt            = 
| caption        = 
| director       = Suhaib Ilyasi
| producer       = 
| writer         = Suhaib Ilyasi
| starring       = Farida Jalal Alok Nath Sushma Seth Reema Lagoo Srishti Gautam Harsh Naagar
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =
| runtime        = 
| country        = India
| language       = 
| budget         = 
| gross          = 
}}

498A: The Wedding Gift is a 2013 drama film set in New Delhi, India. Written and directed by Suhaib Ilyasi, this motion picture features veteran actor Farida Jalal, Alok Nath, Sushma Seth and Reema Lagoo. Colgate Ad boy Harsh Naagar and a fresh actor from Delhi, Srishti Gautam, are in lead roles. The cast also includes Deepak Tijori in his first negative character.
 Section 498(A).  Section 498a IPC was enacted to empower women and make it easier for the wife to seek redress from potential harassment by the husbands family. However, this law on dowry prohibition has come under severe criticism as this has been allegedly misused by unscrupulous women for vested interests in India and abroad. 

Ghulam Ali khan, a veteran Ghazal singer, has reportedly acted in 498A: The Wedding Gift as well. The film first screened in 2013. 

==Plot inspiration==
 
Producers of 498A: The Wedding Gift maintain that the story of the film is not based on any specific true incident, however the motion picture 498A: Wedding Gift is said to be inspired by the death of Syed Makhdoom in 2009. Makhdoom (Canadian NRI), computer engineer by profession, ended his life on 5 April 2009 in Bangalore, leaving four suicide notes and a dying video message on his mobile.

==References==
 

==External links==
*  
*  

 
 
 


 