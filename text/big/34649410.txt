Fishing Without Nets (2012 film)
{{Infobox film
| name = Fishing Without Nets
| image = 
| image_size = 
| caption = 
| director = Cutter Hodierne
| producer = Cutter Hodierne, John Hibey, Raphael Swann
| writer = Cutter Hodierne, John Hibey, Raphael Swann
| starring = 
| music = 
| cinematography = 
| editing = 
| distributor = 
| released = 2012
| runtime = 
| language = English
| country = United States
| rating =
| budget =
| preceded_by =
| followed_by =
}}
 Sundance winning the Grand Jury prize followed by European Premier at the Dawn Breakers International Film Festival.
 feature film based on this short. The film premiered in-competition in the US Dramatic Category at 2014 Sundance Film Festival on January 17, 2014. 

==Plot==
The plot tells the story of a group of fishermen in Somalia who are lured into Piracy in Somalia|piracy. 

==Production==
After becoming obsessed with the subject of piracy in the Indian Ocean, director Cutter Hodierne (then 24 years old) convinced producing partners John Hibey (then 28) and Raphael Swann (then 24) to embark on a journey to do in-field research on the subject. What began as a trailer turned into a short film, written over a few weeks.   

In October 2012, on their first night on location in Mombasa, Kenya in the African Great Lakes region, the filmmakers were accused of trespassing by armed men in police and military uniforms. They were subsequently handcuffed and forced to swim out into the ocean. The cineastes were eventually released after paying a $150 bribe, the first of several such payments made to local officials. They were also along the way robbed by Kenyan policemen,    and briefly imprisoned on gun permit-related charges. 

The film was shot over a period of three and a half months, though the original shooting schedule was intended to last five weeks. Somali immigrants and local Kenyans were cast in the acting roles. Financing for the project was raised through proceeds that Hodierne had earned while previously working on U2s 360 tour. 

==Awards==
Fishing Without Nets was awarded the Grand Jury Prize in Short Filmmaking at the 2012 Sundance Film Festival.   

==References==
 

==External links==
* 
* 

 
 