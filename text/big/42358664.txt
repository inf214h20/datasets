George: A Zombie Intervention
{{Infobox film
| image          =
| alt            =
| caption        =
| director       = J. T. Seaton
| producer       = Brad Hodson David Nicholson J. T. Seaton
| screenplay     = Brad Hodson J. T. Seaton
| starring       = Peter Stickles Michelle Tomlinson Carlos Larkin Shannon Hodson Eric Dean Vincent Cusimano Adam Fox Lynn Lowry
| music          = Joel J. Richard
| cinematography = Jason Raswant
| editing        = Tyler Earring
| studio         = Cat Scare Films
| distributor    = Tarvix Pictures Vicious Circle Films Breaking Glass Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $60,000   
| gross          =
}}
George: A Zombie Intervention (also Georges Intervention) is a 2009 American zombie comedy directed by J. T. Seaton, written by Seaton and Brad Hodson, and starring Carlos Larkin as a zombie who undergoes an invention by his friends, who are concerned that he is eating humans.

== Plot ==
After spores cause the dead to rise as zombies, the undead become a natural and accepted part of society. However, Georges friends become worried about his behavior after his transformation into a zombie. Convinced that hes been eating humans, they decide to stage an intervention, wherein they will attempt to convince him to attend a zombie rehab that teaches zombies how to control their appetite for human flesh. Sarah, Georges ex-girlfriend, recruits a professional interventionist, Barbra, and she and several of Georges friends stage a practice run before confronting him directly. George, however, sees nothing wrong with his behavior and resists their demands. As tempers flare, George finds his hunger increasingly difficult to control, and he begins to give in to his urges.

== Cast ==
* Peter Stickles as Ben
* Michelle Tomlinson as Sarah
* Carlos Larkin as George
* Shannon Hodson as Francine
* Eric Dean as Steve
* Vincent Cusimano as Roger
* Adam Fox as Tom
* Lynn Lowry as Barbra

Brinke Stevens and Lloyd Kaufman appear in cameos.
 Night of the Living Dead film series.  

== Production ==
Shooting took place in Los Angeles    in late 2008; re-shooting took place in early 2009.   Seaton, a horror fan, wanted to make an independent film that genre fans could enjoy without feeling exploited. The cast watched episodes of Intervention (TV series)|Intervention in order to prepare.   Ben was not originally a gay character, but Seaton wanted to insert a plot twist that invalidated a hinted-at romantic subplot between Ben and Sarah.   Seaton, a friend of Brinke Stevens, cast her in the end sequence. Lloyd Kaufman was cast after he requested a part.   The part of Barbra was written for Lynn Lowry, whom Seaton had met on the set another film. British farces were an influence on Seaton for this film, but he also credits H. P. Lovecraft as a general influence. 

== Release ==
Seaton, who had earlier experienced resistance against his films gay characters, said that he was able to successfully screen George: A Zombie Intervention at multiple festivals with no issue.   The film premiered at the 2009 Dragon Con under its original name of Georges Intervention. It was released on DVD in October 2011. 

== Reception ==
The Louisville Eccentric Observer called it "a fun, no-budget love letter to George Romero".   Mark L. Miller of Aint It Cool News wrote that the film is overlong and the acting is "somewhat amateurish", but it is funny and original.   Rohit Rao of DVD Talk rated it 2.5/5 stars and wrote, "J.T. Seaton builds his horror-comedy around a clever premise but neglects to maintain a consistent tone in his urge to rush to the gory climax."   Peter Dendle called it "clever and eminently watchable". 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 