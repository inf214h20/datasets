Poo Viriyum Pulari
{{Infobox film
| name           = Poo Viriyum Pulari
| image          =
| caption        =
| director       = G Premkumar
| producer       = G Premkumar TS Roy Prasad Chandran
| writer         = G Premkumar Pappanamkodu Lakshmanan (dialogues)
| screenplay     = Pappanamkodu Lakshmanan
| starring       = Sukumari Mammootty Rajalakshmi Captain Raju
| music          = Jerry Amaldev
| cinematography = J Williams
| editing        = R Santharam
| studio         = Cherry Enterprises
| distributor    = Cherry Enterprises
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed and produced by G Premkumar. The film stars Sukumari, Mammootty, Rajalakshmi and Captain Raju in lead roles. The film had musical score by Jerry Amaldev.   

==Cast==
 
*Sukumari
*Mammootty
*Rajalakshmi
*Captain Raju Shankar
*Prathapachandran Ambika
*Kuthiravattam Pappu
*Mala Aravindan
*Nellikode Bhaskaran
*Raj Kumar
*Sreenath
 

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Iniyumethu theeram || P Jayachandran || Poovachal Khader || 
|-
| 2 || Kandu ninne sundarippenne || P Jayachandran, Vani Jairam || Poovachal Khader || 
|-
| 3 || Manathaaril Mevun   || Vani Jairam || Poovachal Khader || 
|-
| 4 || Mullappanthal pooppanthal || Vani Jairam || Poovachal Khader || 
|-
| 5 || Premathin maniveenayil || P Jayachandran, Vani Jairam || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 