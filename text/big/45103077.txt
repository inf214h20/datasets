Rodencia y el Diente de la Princesa
{{Infobox film
| name           = Rodencia y el Diente de la Princesa
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = David Bisbano
| producer       = Milton Guerrero   Álvaro Urtizberea
| writer         = David Bisbano   Raquel Faraoni
| screenplay     = 
| story          = 
| based on       =  
| starring       = Hernán Bravo   Natalia Rosminati   Ricardo Alanis   Enrique Porcellana
| narrator       =  
| music          = Panchi Quesada
| cinematography = 
| editing        = David Bisbano
| studio         = Red Post Studio   National Institute of Cinema and Audiovisual Arts   Vista Sur Films S.r.l.
| distributor    = H2O Films   Distribution Company   United International Pictures (UIP)
| released       =  
| runtime        = 87 minutes
| country        = Peru   Argentina Spanish  Portuguese
| budget         = 
| gross          =  
}}
 animated adventure adventure film, directed by David Bisbano and produced by Red Post Studio.

==Plot==
An old legend says that in a vast and wild forest, there is a fantastic kingdom Rodencia, a place inhabited by marvelous creatures and powerful wizards. "Rodencia y el Diente de la Princesa" follows the adventures of the little Edam, an awkward wizards apprentice, along with the beautiful and safe little mouse Brie, accompanied by the greatest warriors of the kingdom, they begin an incredible journey, where they will face the most surprising dangers for a magical and legendary power and thus defeat the dark forces led by the evil wizard Rotex-Texor of the rats, which threatens to invade Rodencia.

==Voice cast==

;Spanish cast
* Hernán Bravo as Edam
* Natalia Rosminati as Brie
* Ricardo Alanis as Roquefort
* Enrique Porcellana as Gruyere
* Sergio Bermejo as Rotex-Texor

;Portuguese cast
* Philippe Maia as Edam
* Erika Menezes as Brie
* Luiz Carlos Persy as Rotex-Texor

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 