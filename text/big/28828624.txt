Aurora (1984 film)
{{Infobox film
| name           = Aurora
| image size     = 
| image	=	Aurora FilmPoster.jpeg
| border         = 
| alt            = 
| caption        = 
| director       = Maurizio Ponzi
| producer       = 
| writer         = Sergio Citti (original story)
| screenplay     = Sergio Citti, John McGreevey, Franco Ferrini, Maurizio Ponzi, Gianni Menon
| story          = 
| based on       =  
| narrator       = 
| starring       = Sophia Loren   Edoardo Ponti   Philippe Noiret   Daniel J. Travanti
| music          = Georges Delerue
| cinematography = Roberto Gerardi
| editing        = Michael Brown
| studio         = 
| distributor    = New Team
| released       =  1984 1984
| runtime        = 95 min
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}

Aurora is a 1984 Italian drama film directed by Maurizio Ponzi and starring Sophia Loren, Edoardo Ponti and Daniel J. Travanti.  In order to raise money for an operation for her son, a woman tells various former wealthy lovers that they are his father. Its Italian title is Qualcosa di biondo.

==Plot==
A desperate woman (Loren), in crisis and poverty, is in search of the cruel husband (Noiret) who abandoned after giving her a son. The boy has now become almost an adult has a serious eye problem and is likely to become completely blind. The mother then choose to leave to go in search of his father, who in the meantime is starting a new life. When the mother finds out where he is, also learns that the cruel man has remade a family by marrying a young girl, who became the mother of a twenty-year-old son who has had the cruel man from another relationship.

==Cast==
* Sophia Loren as Aurora
* Edoardo Ponti as Auroras Son
* Daniel J. Travanti as David Ackermann
* Angela Goodwin as Nurse
* Ricky Tognazzi as Michele Orcini
* Marisa Merlini as Teresa
* Anna Strasberg as Angela Feretti
* Franco Fabrizi as Guelfo
* Philippe Noiret as Dr. André Feretti
* Antonio Allocca as Taxi Driver David Cameron as Floyd
* Vittorio Duse as Mechanic
* Jorge Krimer as Priest
* Gianfranco Amoroso as Gasoline Attendant
* Alessandra Mussolini as Bride

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 