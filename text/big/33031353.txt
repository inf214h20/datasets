Love Always, Carolyn
 
{{Infobox film
| name           = Love Always, Carolyn
| image          = Love always carolyn poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Malin Korkeasalo,  Maria Ramström
| producer       =  Margarete Jangård,      Fredrik Gertten,  Lina Bertilsson
| writer         = Malin Korkeasalo,  Maria Ramström
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Jan Strand (composer) 
| cinematography = Maria Ramström, Malin Korkeasalo 
| editing        =  Bernhard Winkler, Stefan Sundlöf 
| studio         = WG Film 
| distributor    = 
| released       =  
| runtime        = 70 minutes    
| country        = Sweden 
| language       = English 
| budget         = 
| gross          = 
}} Swedish documentary film written and directed by Malin Korkeasalo and Maria Ramström.     The film is about Carolyn Cassadys recollection of life with husband Neal Cassady and Jack Kerouac, and her concern that the truth about these men is being lost in their mythos.     It features interviews with Carolyn Cassady and her children as well as archive footage. The film premiered at the 2011 Tribeca Film Festival.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 
 