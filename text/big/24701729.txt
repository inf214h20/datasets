The Libertine (2000 film)
{{Infobox film
| name           = The Libertine
| image          = 
| alt            = 
| caption        = 
| director       = Gabriel Aghion
| writer         = Éric-Emmanuel Schmitt Gabriel Aghion
| producer       = Raphaël Cohen Gaspard de Chavagnac Pascal Houzelot
| starring       = Vincent Pérez Fanny Ardant Josiane Balasko Michel Serrault Audrey Tautou Arielle Dombasle
| music          = Bruno Coulais
| cinematography = Jean-Marie Dreujou
| editing        = Luc Barnier
| based on       = 
| released       =  
| runtime        = 100 min
| language       = French
| budget         = $9 million 
| gross          = $4,726,485  
}}
 
Le Libertin (The Libertine) is a French comedy film directed by Gabriel Aghion and released in 2000.  It is an adaptation of a 1997 play by Éric-Emmanuel Schmitt.

==Synopsis==

The philosopher Denis Diderot, one of the modernists of the French 18th-century Age of Enlightenment movement, is a guest at the château of the Baron dHolbach. The film depicts the Baron (in reality a major sponsor of Diderot) as a benign host and inventor of amusing machines, including a piganino. Diderots banned Encyclopédie is being secretly set up, printed and bound in a crypt beneath the chapel, where the noise of the presses is masked by the playing of an organ.

Madame Therbouche, a purported portrait painter and  , arrives from Berlin. She has made a painting of Diderots idol, Voltaire, and convinces him to be more daring and to pose for her in the nude, leading to an animated row with his wife Antoinette. This unsavoury event is witnessed by a feared visitor, Baron dHolbachs brother, the Cardinal, who is hunting for the illegal Encyclopédie printers. To divert him from entering the chapel, the Baroness confesses to him her real and imagined sins and sends every woman in the castle to do the same. Most notably her guest, the depraved Madame de Jerfeuil, is having a lesbian affair with her cousin. The couple is later joined spontaneously by her husband the Chevalier de Jerfeuil, who has been seduced by two other male guests.

Diderots romantic adventures oblige him to constantly revise his article on the subject of morality. Eventually, secret agenda are disclosed and the painter is revealed as a spy in the Cardinals service.  However, she finally defects to Diderots material and sensual causes, and the Cardinal fails to find the clandestine publishing operation which has been transplanted to Amsterdam.

==Cast==

* Denis Diderot - Vincent Pérez
* Antoinette Diderot - Françoise Lépine
* Madame Therbouche - Fanny Ardant
* Julie dHolbach - Audrey Tautou 	
* Baron dHolbach - François Lalande
* Baroness dHolbach - Josiane Balasko
* The Cardinal - Michel Serrault
* Chevalier de Jerfeuil - Christian Charmetant
* Madame de Jerfeuil - Arielle Dombasle
* Marquis de Cambrol - Bruno Todeschini
* Marquis de Lutz - Arnaud Lemaire
* The cousin of Madame de Jerfeuil - Véronique Vella
* Abraham - Yan Duffas

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 