Calling Philo Vance
__NOTOC__
{{infobox film
| name           = Calling Philo Vance
| image          = File:Calling Philo Vance poster.jpg
| image_size     = 225px
| caption        = theatrical poster William Clemens
| starring       = James Stephenson Margot Stevenson
| producer       = Brian Foy (assoc. prod; uncredited)
| screenplay     = Tom Reed
| based on       =  
| cinematography = L. Wm. OConnell
| editing        = Benjamin Liss Louis Lindsay
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       = February 3, 1940 (US)
| runtime        = 62 minutes
| country        = United States
| language       = English
| gross          =
| budget         =
}}
 comedy released William Clemens Tom Reed film in 1933, starring William Powell and Mary Astor.

For this adaptation of the story, Vance is on international assignment from the United States government to investigate traffic in wartime aircraft designs. The original story dealt with art world double-dealing, but the solution to the mystery is the same in each film.

== Cast ==
  
* James Stephenson as Philo Vance
* Margot Stevenson as Hilda Lake
* Henry ONeill as J.P. Markham
* Edward Brophy as Ryan
* Sheila Bromley as Doris Delafield
* Ralph Forbes as Tom McDonald
* Don Douglas as Philip Wrede
* Martin Kosleck as Gamble
 
* Jimmy Conlin as Dr. Doremus 
* Edward Raquello as Eduardo Grassi
* Creighton Hale as Du Bois
* Harry Strang as Markhams assistant
* Richard Kipling as Archer Coe
* Wedgwood Nowell as Brisbane Coe
* Bo Ling as Ling Toy
 

Cast notes:
*Warner Bros. intended to revitalize the Philo Vance series with British stage actor James Stephenson, but Stephenson never played the part again &ndash; he died of a heart attack in 1941. 
*Actors    The Wizard of Oz. 

==Production==
Calling Philo Vance had the working titles "Philo Vance Comes Back" and "Philo Vance Returns".   on TCM.com 

==References==
Notes
 

== External links ==
*   
*  

 
 
 
 
 
 
 

 