491 (film)
 {{Infobox Film
| name           = 491
| image          = 491 poster.jpg
| caption        = Theatrical release poster
| director       = Vilgot Sjöman
| producer       = Lars-Owe Carlberg
| writer         = Lars Görling (also novel) Vilgot Sjöman	 	
| narrator       = 
| starring       = Lars Lind Georg Riedel
| cinematography = Gunnar Fischer
| editing        = Lennart Wallén
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 101 min. Sweden
| Swedish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1964 Cinema Swedish black-and-white drama film directed by Vilgot Sjöman, based on a novel by Lars Görling. The story is about a group of youth criminals who are chosen to participate in a social experiment, where they are assigned to live together in an apartment while being supervised by two forgiving social workers. The tagline is: It is written that 490 times you can sin and be forgiven. This motion picture is about the 491st.
 homosexual rape banned in Sweden and Malaysia, but released after reediting, a censored version was later released in Malaysia. Amongst other things, a scene in which a woman is raped by a dog was cut. The film was also banned in Norway until 1971. Christian Democratic Party was founded in 1964.

==Cast==
* Lars Lind as Krister
* Leif Nymark as Nisse
* Stig Törnblom as Egon
* Lars Hansson as Pyret
* Sven Algotsson as Jingis
* Torleif Cederstrand as Butcher
* Bo Andersson as Fisken
* Lena Nyman as Steva
* Frank Sundström as Inspector
* Åke Grönberg as Reverend Mild

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 