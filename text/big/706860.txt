Waiting for Guffman
{{Infobox film
| name           = Waiting for Guffman
| image          = Waiting for Guffman.jpg
| director       = Christopher Guest
| producer       = Karen Murphy
| writer         = Christopher Guest Eugene Levy
| starring       = Christopher Guest Eugene Levy Catherine OHara Parker Posey Fred Willard Bob Balaban
| cinematography = Roberto Schaefer
| editing        = Andy Blumenthal
| producer       = Karen Murphy
| studio         = Castle Rock Entertainment
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $4 million
| gross          = $2,892,582 (USA)
}}
Waiting for Guffman is a comedy in the documentary style starring, co-written and directed by Christopher Guest that was released in 1997. Its cast included Catherine OHara, Eugene Levy, Fred Willard, and Parker Posey.

The title of the film is a reference to the Samuel Beckett play (theatre)|play, Waiting for Godot. As in the other mockumentaries created by Guest, the majority of the dialogue is improvised. Because the film is about the production of a stage musical, it contains several original musical numbers.

==Plot==
In the fictional small town of Blaine, Missouri, a handful of utterly delusional residents prepare to put on a community theater production led by eccentric director Corky St. Clair (Christopher Guest). The show, a musical chronicling the towns history titled Red, White and Blaine, is to be performed as part of the towns 150th anniversary celebration.

Cast in the leads are Ron and Sheila Albertson (Catherine OHara and Fred Willard), a pair of married travel agents who are also regular amateur performers; Libby Mae Brown (Parker Posey), a perky Dairy Queen employee; Clifford Wooley (Lewis Arquette), a "long time Blaineian" and retired taxidermist who is Red, White and Blaines narrator; Johnny Savage (Matt Keeslar), a handsome and oblivious mechanic, who Corky goes out of his way to get into the play; and Dr. Allan Pearl (Eugene Levy), a tragically square dentist determined to discover his inner entertainer. High school teacher Lloyd Miller (Bob Balaban) is the shows increasingly frustrated musical director.
 Broadway producer, to critique Red, White and Blaine. Corky leads the cast to believe that a positive review from Guffman could mean their show might go all the way to Broadway.
 Pacific coastline. We also learn why the town obtusely refers to itself as "the stool capital of the United States." The music is a series of poorly performed songs such as "Nothing Ever Happens on Mars" a reference to the towns supposed visit by a unidentified flying object|UFO, and "Stool Boom". (The DVD contains "This Bulging River" and "Nothing Ever Happens in Blaine", which were edited from the cinema release.) 
 stereotypically gay mannerisms. He supposedly has a wife called Bonnie, whom no one in Blaine has ever met or seen. He uses her to explain his habit of shopping for womens clothing and shoes.
 stage rouge and eyeliner.

Corky is also faced with creating his magic on a shoestring budget, at one point quitting the show after storming out of a meeting with the City Council, which turns down his request for $100,000 to finance the production. But the distraught cast and persuasive city fathers convince Corky to return. At the shows performance, Guffmans seat is seen to be empty, much to the dismay of the cast. Corky reassures them that Broadway producers always arrive a bit late for the show, and sure enough a man (Paul Benedict) soon takes Guffmans reserved seat. The show is well received by the audience, whereupon Corky invites the assumed Guffman backstage to talk to the actors.

The man is actually Roy Loomis, who came to Blaine to witness the birth of his nieces baby, but he did enjoy the show. Corky then reads a telegram stating that Guffmans plane was grounded by snowstorms in New York City, meaning that, like the "Godot" being spoofed, the real Guffman himself is destined never to arrive.
 Brat Pack The Remains of the Day lunch boxes.

==Cast==
*Christopher Guest as Corky St. Clair
*Eugene Levy as Dr. Alan Pearl
*Fred Willard as Ron Albertson
*Catherine OHara as Sheila Albertson
*Parker Posey as Libby Mae Brown
*Lewis Arquette as Clifford Wooley
*Bob Balaban as Lloyd Miller
*Matt Keeslar as Johnny Savage
*David Cross as UFO Expert
*Linda Kash as Mrs. Pearl
*Brian Doyle-Murray as Red Savage
*Paul Benedict as Roy Loomis
*Paul Dooley as UFO Abductee

==Reception==
Waiting for Guffman received acclaim from critics. Based on 54 reviews collected by Rotten Tomatoes, the film received a 91% approval rating from critics, with an average score of 7.8/10.    
By comparison, Metacritic, which assigns a rating in the 0–100 range based on reviews from top mainstream critics, calculated an average score of 71, based on 19 reviews.     During opening weekend in 1997, the film made $37,990.     With a budget of $4 million, the film earned less than $3 million worldwide. 

American Film Institute recognition:
*AFIs 100 Years... 100 Laughs - Nominated 

==References==
 

==External links ==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 