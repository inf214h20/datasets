The Misleading Widow
{{infobox film
| name           = The Misleading Widow
| image          = File:Misleading Widow poster.jpg
| imagesize      =
| caption        = Film poster
| director        = John S. Robertson
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = F. Tennyson Jesse (play)  H. M. Hardwood (play) Frances Marion (scenario)
| starring       = Billie Burke
| music          =
| cinematography = Roy Overbaugh
| editing        =
| studio         = Famous Players-Lasky/Artcraft
| distributor    = Paramount Pictures
| released       = July 27, 1919 (Los Angeles) August 31, 1919 (New York City) September 7, 1919 (USA)
| runtime        = 5 reels
| country        = United States
| language       = Silent film (English intertitles)
}} silent film comedy directed by John S. Robertson and starring Billie Burke. The film is based on the play Billeted (play)|Billeted by F. Tennyson Jesse and H. M. Harwood and was produced by Famous Players-Lasky and distributed by Paramount Pictures.        

As the survival status of The Misleading Widow is listed as unknown,    it is likely that it, similar to most of Burkes silent films, is a lost film. 

==Plot==
 
As summarized in an adaptation published in the September 1919 issue of Shadowlands,    Betty Taradine, who lives in a British village near a army base, was abandoned by her husband for her spendthrift ways. She reports that he is dead to obtain insurance money. Later, British officer Captain Peter Rymill is assigned to be billeted at her house, but he turns out to be her husband living under an assumed name. There are various romantic triangles involving other villagers, and the identity of the missing husband and existence of the marriage is revealed after a dinner with the guests gathered in the widows bedroom.
 Third Amendment to the United States Constitution prohibits the quartering of soldiers in a persons home without their consent.

==Cast==
*Billie Burke - Betty Taradine
*James Crane - Captain Peter Rymill
*Frank Mills - Colonel Preedy
*Madelyn Clare - Penelope Moon (billed  Madeline Clare)
*Fred Hearn - Reverend Ambrose Liptrott
*Mrs. Priestly Morrison - Tabitha Liptrott
*Fred Esmelton - Mr. MacFarland
*Dorothy Waters - Rose

==See also==
*List of lost films

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 