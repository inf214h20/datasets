Firecreek
 
{{Infobox film
| name           = Firecreek
| image          = 1968.firecreek.jpg
| caption        = Theatrical release poster
| producer       = Philip Leacock John Mantley
| director       = Vincent McEveety
| writer         = Calvin Clements Sr.
| starring       = James Stewart Henry Fonda Alfred Newman
| cinematography = William H. Clothier
| editing        = William H. Ziegler
| distributor    = Warner Bros.-Seven Arts
| released       =  
| runtime        = 104 minutes English
| gross = $1,100,000 (US/ Canada) 
}}
 western movie  directed by Vincent McEveety and starring James Stewart and Henry Fonda in his second role as an antagonist that year. The film is similar to High Noon in that it features an entire town of cowards refusing to help a peace officer against outlaws. Stewart plays an unlikely hero, forced into action when his conscience will not permit evil to continue.  
 How the West Was Won but had no scenes together despite playing best friends.

==Plot==
After years of backing away from criminals and gunfights, one resident of the small western town of Firecreek decides to fight back. Part-time sheriff Johnny Cobb (James Stewart) decides to avenge the death of a young man against gunmen led by Bob Larkin (Henry Fonda).

Cobb has a lot on his mind, particularly with his wife Henrietta (Jacqueline Scott) about to give birth. He is a peace-loving farmer whose childishly made sheriffs badge is practically an honorary one.

Larkins men ride into town and disrupt the peace. Earl (Gary Lockwood), Norman (Jack Elam), and Drew (James Best) run roughshod over the local citizens and Larkin has no inclination to stop it, despite Cobbs requests. Larkin is more interested in getting to know an attractive widow named Evelyn (Inger Stevens).

The only person in town willing to help Cobb is a slow-witted stable boy named Arthur (J. Robert Porter (actor)|J. Robert Porter). When the boy is murdered by Larkins men, a terrified and outgunned Cobb decides to stand up to them alone.

==Cast==
* James Stewart as Johnny Cobb
* Henry Fonda as Bob Larkin
* Inger Stevens as Evelyn Pittman
* Jacqueline Scott as Henrietta Cobb
* Gary Lockwood as Earl
* Dean Jagger as Whittier
* Ed Begley as Preacher Broyles
* Jay C. Flippen as Mr. Pittman
* Jack Elam as Norman
* James Best as Drew
* Barbara Luna as Meli
* Brooke Bundy as Leah
* J. Robert Porter (actor)|J. Robert Porter as Arthur
* Morgan Woodward as Willard
* John Qualen as Hall
* Louise Latham as Dulcie
* Athena Lorde as Mrs. Littlejohn

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 