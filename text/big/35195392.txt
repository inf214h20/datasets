Everybody Loves Alice
{{Infobox film
| name           = Everybody Loves Alice
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Richard Hobert
| producer       = Peter Possne
| writer         = Richard Hobert
| narrator       = 
| starring       = 
| music          = Björn Hallman
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sveriges Television   Film i Väst   Sonet Film
| released       =  
| runtime        = 118 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Everybody Loves Alice ( ) is a 2002 Swedish drama film.

==Plot==
Alice lives in a house with her brother,Pontus and parents, Johan and Lotta. It is revealed that Lotta doesnt love Johan and has also been having an affair. He later moves and lives with his "new" lover, Anna, who has a son called Patrik, a classmate of Alice.

The film includes Alices problems dealing with Anna and her feelings of isolation.

==Cast==
*Natalie Björk as Alice
*Bisse Unger as Pontus
*Marie Richardson as Lotta
*Mikael Persbrandt as Johan
*Marie Göranzon as Sonya, Lottas mother
*Sverre Anker Ousdal as Henry, Lottas father
*Hege Schøyen as Tina
*Anastasios Soulis as Patrik
*Lena Endre as Anna, Patriks mother Per Svensson as Erik, teacher
*Mathilda Lundholm as Hanna
*Li Lundholm as Moa
*Marcus Ardai-Blomberg as Anton
*Pernilla August as the psychologists voice

==External links==
*  
*  
*   on Filmpunktens website

 
 
 
 
 

 
 