8213: Gacy House
 
{{Infobox film

 | name = 8213: Gacy House/Paranormal Entity 2
 | caption = DVD cover Anthony Fankhauser
 | producer =  
 | writer = Anthony Fankhauser
 | starring =  
 | music = Chris Ridenhour (trailer only)
 | cinematography = Matt Hoelfer
 | distributor = The Asylum
 | released =  
 | runtime = 90 minutes
 | country = United States
 | language = English
 | budget = 
 | image = 8213- Gacy House VideoCover.jpg
}} American horror film by The Asylum. The film is directed by Anthony Fankhauser and serves as a loose prequel to The Asylums 2009 film Paranormal Entity, and a mockbuster of the film Paranormal Activity 2.

==Plot==
A group of paranormal investigators enter the abandoned home of serial killer John Gacy, hoping to find evidence of paranormal activity. Upon entering the house they set up cameras throughout the abandoned house while going room to room with hand-held cameras, performing séances and asking for John Gacy to come forward. As the evening progresses it seems the investigators are not prepared for the horror still within the house. In the end all hell breaks loose, Gary finds Mikes dead body in the basement, Gary is dragged away, Franklin disappears and Robbie is chased through the house. After the rest of the cameras cut to static, an apparition of Gacy appears, Robbie is then taken and his camera cuts to static. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 