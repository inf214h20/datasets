The Strange Night
 
{{Infobox film
| name           = The Strange Night
| image          = The Strange Night.jpg
| caption        = Film poster
| director       = Alfredo Angeli
| producer       = Alvaro Mancori
| writer         = Alfredo Angeli Marco Guglielmi Giulio Paradisi Bruno Rasia
| starring       = Sandra Milo
| music          = 
| cinematography = Marcello Gatti
| editing        = Giulio Paradisi
| distributor    = 
| released       = 13 April 1967
| runtime        = 113 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Strange Night ( ) is a 1967 Italian film directed by Alfredo Angeli. It was entered into the 17th Berlin International Film Festival.   

==Cast==
* Sandra Milo as Debora
* Enrico Maria Salerno
* Giulio Platone
* Lidia Alfonsi
* Massimo Serato
* Evi Maltagliati
* Ettore Manni
* Antonella Steni
* Giorgio Capecchi
* Elvira Cortese
* Annie Gorassini
* Adriano Micantoni
* Mirella Pamphili

==References==
 

==External links==
* 

 
 
 
 
 
 