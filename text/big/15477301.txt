Kala Malam Bulan Mengambang
 
 
 
{{Infobox film
| name           = Kala Malam Bulan Mengambang
| image          =
| caption        =
| director       =
| producer       = Mamat Khalid
| starring       = Rosyam Nor Umie Aida Corrien Adrienne Avaa Vanjaa Bront Palarae Farid Kamil
| music          =
| distributor    = Tayangan Unggul Sdn Bhd
| released       = 2008
| runtime        = 110 minutes
| country        = Malaysia
| awards         =
| language       = Malay
| budget         = MYR 1,400,000 (estimated)
| followed_by    =
| website        =
}}

Kala Malam Bulan Mengambang ( n Film noir and being released on 15 January 2008.

==Plot==
Saleh (Rosyam Nor), a reporter, finds himself stranded in a village after his car tire is punctured by a strange keris. During his stay, he meets the beautiful and mysterious Cik Putih (Umie Aida), sister to the mechanic Jongkidin, and is immediately smitten. At the same time, he also finds out that when there is a full moon, a man would disappear from the village. Rumours start to spread about a pontianak who is killing their men for their blood. Saleh decides to stay a little longer to solve the mystery, even after being advised to leave the village by Doreen (Corrien Adrienne). Who has been kidnapping the men? What is the secret of the village?

==Cast==
* Rosyam Nor as Salleh
* Umie Aida  as Cik Putih
* Avaa Vanjaa as Miss Rohayah
* Corrien Adrienne as Doreen Chua
* Farid Kamil as Jongkidin
* Kuswadinata as Dr. Rushdi
* Bront Palarae as Mahinder Singh
* David Teo as Hotel Owner

==External links==

 
 

 
 
 
 