Leprechaun (film)
 
{{Infobox film
| name           = Leprechaun
| image          = Leprechaunposter.jpg
| alt            = 
| caption        = Theatrical release poster Mark Jones
| producer       = Jeffrey B. Mallian
| writer         = Mark Jones
| starring       = {{plainlist|
* Warwick Davis
* Jennifer Aniston
}}
| music          = {{plainlist|
* Kevin Kiner
* Robert J. Walsh
}}
| cinematography = Levie Isaacks
| editing        = Christopher Roth
| studio         = Trimark Pictures
| distributor    = Trimark Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $900,000
| gross          = $8.6 million   
}} cult American Mark Jones. It stars Warwick Davis in the title role and Jennifer Aniston in her film debut. The film was shot in Saugus, Santa Clarita, California.

==Plot== Leprechaun had stowed away in one of his suitcases, killing his wife by pushing her down the basement stairs. After burying the gold, Daniel discovers the leprechaun and tries to kill it by showing him a four-leaf clover, the Leprechauns weakness. He manages to trap him inside a crate and attempts to burn it and the house to the ground, but suffers a stroke, leaving the leprechaun inside, guarded by the magic of the four leaf clover.

10 years later, in 1993, J.D. Redding and his teenage daughter Tory rent the OGrady farmhouse for the summer when they meet Nathan Murphy, his little brother Alex, and their dimwitted friend Ozzie Jones, who are re-painting the farmhouse. Ozzie is looking around the basement when he hears the Leprechauns cry for help, mistaking him for a little child. He brushes the old four-leaf clover off the crate, letting the Leprechaun break free.

After trying to convince the others that he met a Leprechaun (which fails horribly due to his former ludicrous stories), Ozzie spots a rainbow and chases it, believing that there will be a pot of gold at the end. Alex accompanies him for fear Ozzie might hurt himself when they come across an old truck, with the bag of one hundred gold pieces magically appearing. After testing to see if its real gold (where Ozzie bit the gold piece and accidentally swallowed it), they plot to keep it for themselves, hoping to fix Ozzies brain.

The Leprechaun lures J.D. into a trap by imitating a cat, biting and injuring his hand. Tory and the others rush him to the hospital, followed by the Leprechaun, who travelled there on a tricycle. Alex and Ozzie go to a pawn shop to see if the gold is pure while Nathan and Tory are out, waiting on J.D.s results. The Leprechaun attacks the pawn shop owner, killing him by crushing his chest with a pogo stick. After terrorizing and killing a policeman, the Leprechaun returns to the farmhouse, searching for his gold, while shining every shoe in the house. Everyone (minus J.D.) returns home, finding the house ransacked. Nathan goes out to see what is outside when he is injured by a bear trap set up by the Leprechaun.

After shooting the Leprechaun several times, they try to leave the farmhouse when the truck breaks down, due to the Leprechaun biting all the cords. After ramming the truck with his specially-made car, the Leprechaun terrorizes the gang until Ozzie reveals that he and Alex found the pot of gold. Tory recovers the bag from the old well, and gives it to the Leprechaun. Believing the worst to be over, they try to head out to the hospital. The Leprechaun is counting his gold when he discovers that he is missing one gold piece (the one Ozzie swallowed) and thinks that they have tricked him, leading him to terrorize them until Ozzie tells them about OGrady, who was taken to a nursing home after his stroke. Tory decides to head out to the home to find out how to kill the Leprechaun.

Tory arrives at the nursing home, searching until she finds OGrady, who is actually the Leprechaun, who chases Tory to the elevator. Tory escapes, while the bloodied body of OGrady crashes through, managing to tell her that the only way to kill him is by a four-leaf clover, before dying. Tory returns home, and automatically starts searching for a clover until she is chased by the Leprechaun, who almost kills her until she is saved by Nathan and Ozzie. Alex tries to set a trap but is attacked by the Leprechaun, almost killing him but Ozzie tells him that he swallowed the last gold coin, and is critically wounded by the Leprechaun. Before the Leprechaun can kill Ozzie, Alex takes the four-leaf clover from Tory, sticks it to a wad of gum and shoots it into the Leprechauns mouth, taking away his power. The skeleton of the Leprechaun appears out of the well until Nathan hits him down and pours gas inside the well, blowing up the well and killing the Leprechaun.

The police arrive where Tory is reunited with her father as the police investigate the remains of the well, where the Leprechaun vows he will not rest until he recovers every last piece of his gold.

== Cast ==

* Warwick Davis as The Leprechaun
* Jennifer Aniston as Tory Redding
* Ken Olandt as Nathan Murphy
* Mark Holton as Ozzie Jones
* Robert Hy Gorman as Alex Murphy
* David Permenter as Deputy Tripet William Newman as Sheriff Cronin
* Shay Duffin as Dan OGrady
* Pamela Mant as Mrs. OGrady
* John Sanderford as J. D. Redding

== Production ==
Mark Jones, the writer-director, had a career in American television shows.  Desiring to make a film, he decided that a low budget horror film was his best opportunity.  Jones was inspired by the Lucky Charms commercials to create a leprechaun character, only his twist was to turn the character into an antagonist.  Jones was further influenced by Critters (film)|Critters, which featured a small antagonist.  Jones brought the concept to Trimark, who were looking to get into film production and distribution.  Leprechaun became the first theatrically released film to be produced by Trimark.  Davis, who had experienced a dry spell after playing the protagonist in Willow (film)|Willow, liked the script and was excited to play against type.  Aniston, who was an unknown at the time, impressed Jones, and he fought to have her cast.  The film was initially more of a straightforward horror film, but Davis sought to add more comedic elements.  Jones agreed with this tonal shift, and they shot it as a horror comedy.   

Gabe Bartoalos performed the make-up effects.  Trimark contacted Bartalos to produce a sample.  Bartalos early efforts were not to his liking, and he began to push the design in a more grotesque, rage-filled direction, as that was what he wanted to see on the screen as a horror fan.  Bartalos design impressed Trimark, and he got the account.  Applying the make-up took three hours, and taking it off took another 40 minutes. 

Entertainment Weekly quoted the budget at "just under $1 million". 

== Release ==
Leprechaun opened in 620 theatres and took in $2,493,020 its opening week, ultimately earning $8,556,940 in the United States.   Vidmark released it on VHS in April 1993  and on DVD in August 1998.   Lionsgate released a triple feature collection on March 11, 2008.   All seven films were released on Blu Ray in a collection in September 2014. 

==Reception==
On release, critical reception for the film was  negative.  Rotten Tomatoes, a review aggregator, reports that 25% of 12 surveyed critics gave the film a positive review; the average rating is 4/10.   Vincent Canby of The New York Times called it "neither scary nor funny".   Michael Wilmington of the Los Angeles Times wrote, "Even if youre in the mood for a low-budget horror movie about a maniacal leprechaun in bloody quest of a crock of gold, youd do well to pass on Leprechaun".   Richard Harrington of The Washington Times wrote that the film "has major continuity and credibility problems" and is only interesting while Davis is on screen.   Chris Hicks of The Deseret News described the plot as "by-the-numbers killings with no rhyme or reason" and said that the film should have gone direct-to-video.   Marc Savlov of The Austin Chronicle called it cliched and uninteresting.   James Berardinelli wrote, " How is the audience supposed to sympathize with a group of morons who act like they flunked kindergarten?"   Ron Weiskind of the Pittsburgh Post-Gazette wrote, "Forget about the proverbial pot of gold.  The movie Leprechaun is a crock."   Matt Bourjaily of the Chicago Tribune wrote that the film "has brought new meaning to the term bad".   Jeff Makos of the Chicago Sun-Times wrote that audience feedback would be more entertaining than the film itself.   Robert Strauss of the Los Angeles Daily News called it "as witless and worthless a horror film as could possibly be conjured". 

Despite the negative reviews, the film and its sequels have become cult films.   In 2009, Tanya Gold of The Guardian selected it as one of the top ten scariest films for Halloween,   and it is also watched on Saint Patricks Day. 

== References ==
 

== External links ==
*  
*  
*   digital comic books from Devils Due Digital.

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 