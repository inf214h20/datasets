Naked Evil
{{Infobox film
| name           = Naked Evil
| image          =
| image_size     =
| caption        =
| director       = Stanley Goulder
| producer       = Michael F. Johnson Steven Pallos
| writer         = Stanley Goulder based on        = TV play by Jon Manchip White
| narrator       =
| starring       = Basil Dignam Anthony Ainley Suzanne Neve
| music          = Bernard Ebbinghouse
| cinematography = Geoffrey Faithfull
| editing        = Peter Musgrave
| studio         = Protelco
| distributor    = Columbia
| released       = 1966
| runtime        = 85 mins
| country        = United Kingdom
| language       = English budget = £60,000   
}}

Naked Evil is a 1966 British horror film, written and directed by Stanley Goulder.  It is based on the play The Obi written by Jon Manchip White. An American version of the film was released with the title Exorcism at Midnight.

==Cast== 
* Basil Dignam as Jim Benson 
* Anthony Ainley as Dick Alderson 
* Suzanne Neve as Janet Tuttle 
* Richard Coleman as Inspector Hollis 
* Olaf Pooley as Father Goodman 
* John Ashley Hamilton as Danny (credited as George A. Saunders) 
* Carmen Munroe as Beverley (credited as Carmen Monroe) 
* Brylo Forde as Amizan
* Bari Jonson as Spadey
* Dan Jackson as Lloyd 
* Oscar James as Dupree  
* Ronald Bridges as Wilkins

==Production== Richard Gordon. 

==References==
 

==External links==
*  

 
 
 
 

 