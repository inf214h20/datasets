Man with the Screaming Brain
 
{{Infobox film
| name           = Man with the Screaming Brain
| image          = Man with the screaming brain.jpg
| caption        = Theatrical release poster
| director       = Bruce Campbell
| producer       = Bruce Campbell Jeff Franklin David Goodman Bob Perkis
| writer         = Bruce Campbell
| story          = Bruce Campbell David Goodman
| narrator       =
| starring       = Bruce Campbell Tamara Gorski Vladimir Kolev Antoinette Byron Stacy Keach Ted Raimi
| music          = Joseph LoDuca
| cinematography = David Worth
| editing        = Shawn Paper
| distributor    = Syfy
| released       =  
| runtime        = 90 minutes
| country        = Germany United States
| language       = English
| budget         = $3,000,000 
}} 2005 science science fiction/slapstick film co-written, produced, directed by and starring Bruce Campbell.  It is Campbells feature film directorial debut.  The film was co-written by David Goodman and co-stars Ted Raimi.

== Plot == stereotypical Ugly ugly American who constantly complains about the lack of Americanization of the former communist country. Theyre driven to a hotel by a taxi driver, and former KGB agent, named Yegor Stragov (Vladimir Kolev), in which Yegor gives William a ring to give to Jackie. While William is at the construction of a subway, Jackie secretly cheats on William with Yegor.

William gets back to the hotel and bumps into the hotel maid and gypsy, Tatoya (Tamara Gorski), who kills men that date and dump her. Jackie then comes in, catches William kissing Tatoya and dumps him. William chases Tatoya, who had taken Williams money and ring (which is revealed to be Tatoyas that she gave to Yegor when they dated), and Tatoya knocks him in the head with a pipe outside the hotel. Yegor witnesses this and so Tatoya kills Yegor with his own gun.

A vengeful Jackie has Coles life support plug pulled in hospital, and then goes to Gypsy Town where Tatoya lives and attempts to kill her, only to have Tatoya kill her by throwing her down a flight of stairs. Meanwhile, William wakes up in the warehouse of a Russian scientist named Dr. Ivan Ivanovich Ivanov (Stacy Keach), and his idiotic assistant Pavel (Ted Raimi), who had taken damaged parts of Williams brain and replaced it with healthy tissue from Yegors. When William runs out of the warehouse, he discovers he can hear Yegors voice in his head, and they both plan to "get the woman that killed us both". Jackie, who had also been picked up by Dr. Ivanov and Pavel, has her brain put inside a robot, and so she escapes and also plans to exact revenge on Tatoya.

William/Yegor and Robo-Jackie chase Tatoya around town. William gets involved in a car crash with his foot underneath a car and Tatoya makes another attempt to kill William by setting the leaking car gasoline alight. Jackie saves him and is presumed dead in the explosion. After avoiding some bar punks that believe William "raped Tatoya on her wedding day", William/Yegor begin suffering brain damage due to their cells not able to coexist in the same head. Jackie, who had survived the explosion, appears and attempts to kill Tatoya by throwing her off a bridge, until Jackie stabs her brain, causing her to malfunction, and has Jackie thrown off the bridge. William chases Tatoya through the subway construction and the sewer and finally kills Tatoya by dropping her in sewer river, after obtaining the ring back. William and Jackie then confess their love for each other before Jackies batteries finally die, as does William due to the brain cells of him and Yegor. Pavel brings William, Jackie and Tatoyas body back to Dr. Ivanov to fix them, as he had earlier found a way to make William and Yegors brain cells coexist in the same head.

The movie ends with William back in the U.S. six months later, still sharing his body with Yegors brain. He goes to a brain trauma benefit with Jackie, whose brain had been transferred into Tatoyas body.

== Cast ==
* Bruce Campbell as William Cole
* Tamara Gorski as Tatoya
* Ted Raimi as Pavel
* Antoinette Byron as Jackie Cole
* Stacy Keach as Dr. Ivan Ivanovich Ivanov
* Vladimir Kolev as Yegor Stragov
* Valentine Glasbeily as Uri
* Velizar Binev as Mayor
* Raicho Vasilev as Bartender
* Jonas Talkington as Larry
* Mihail Elanov as Punk 1
* Neda Sokolovska as Waitress
* Remington Franklin as Bar Punk

== Production ==
The story for Man with the Screaming Brain has existed in various forms since the mid-1980s. It was originally supposed to take place in East L.A., but after the fall of communism it was shot in Bulgaria instead, in order to save production costs. 

== Release == Sci Fi Channel, where it was aired for the first time on television on September 25, 2005.  It premiered on April 3, 2005 at the IHouse in Philadelphia, Pennsylvania. It was supposed to premiere that same night at the Broadway Theater in Pitman, New Jersey; however, that theater went bankrupt earlier in the week and a new venue was found (the IHouse). 

Campbell then took the film with him on his book tour and it was shown at a limited number of theaters throughout the summer of 2005.

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 33% of 12 surveyed critics gave the film a positive review; the average rating was 4.9/10.   Joshua Siebalt of Dread Central rated it 3/5 stars and wrote, "The story is pretty ridiculous from start to finish, but thats not necessarily a bad thing since the film doesnt take itself seriously at all."   Rob Gonsalves of eFilmCritic.com rated it 4/5 stars and called it "a decent and diverting piece of work from perhaps the hardest-working man in movies." 

Negative reviews came from Dennis Harvey of Variety (magazine)|Variety, who called it "a comedy that doesnt build, lacks structural integrity, and often falls flat. But its also winningly loopy, with bizarre incidental ideas and performance riffing making for a series of parts that almost make up for the faults of the whole",  and Mark de la Viña of San Jose Mercury News, who called it "an unapologetically sloppy jumble of Roger Corman-style antics that could only hope to inspire their own drinking game." 

==Adaptations==
Dark Horse Comics published a four-issue comic book series based on the film.

==References==
 

==External links==
* 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 