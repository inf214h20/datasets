Um das Menschenrecht
{{Infobox film
| name           = Um das Menschenrecht
| image          =
| image_size     =
| caption        =
| director       = Hans Zöberlein Ludwig Schmid-Wildy
| producer       = Hans Zöberlein (executive producer)
| writer         = Hans Zöberlein (manuscript and screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Ludwig Zahn
| editing        =
| distributor    =
| released       = 1934
| runtime        =
| country        = Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Um das Menschenrecht is a 1934 German film directed by Hans Zöberlein and Ludwig Schmid-Wildy.

== Plot summary ==
 

== Cast ==
*Hans Schlenck as Hans, Frontkamerad
*Kurt Holm as Fritz, Frontkamerad
*Ernst Martens as Max, Frontkamerad
*Beppo Brem as Girgl, Frontkamerad
*Ludwig Ten Cloot as der Hauptmann
*Hans Erich Pfleger as Paul, Freikorpskamerad
*Paul Schaidler as Christian, Freikorpskamerad
*Franz Loskarn as Höllein, Freikorpskamerad
*Leopold Kerscher as Martin, Freikorpskamerad
*Werner Scharf as Anführer der Roten
*Trude Haefelin as Petratka, politische Agentin
*Katja Specht as Natascha, politische Agentin
*Ludwig Körösy as ein Unterführer
*Hans Pössenbacher as a Spartacist
*Ludwig Schmid-Wildy as der alte Krafft
*Lydia Alexandra as Berta Schön, Wirtstochter
*Rose Kugler as Girgls wife
*Hilde Horst as Fritzs wife

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 


 
 