Simshar
 
{{Infobox film
| name           = Simshar
| image          = Simshar poster.jpg
| caption        = Film poster
| director       = Rebecca Cremona
| producer       = 
| writer         = David Grech Rebecca Cremona
| starring       = Lotfi Abdelli
| music          = 
| cinematography = 
| editing        = Daniel Lapira
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Malta
| language       = Maltese
| budget         = 
}}
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.       It was the first time that Malta submitted a film for the Best Foreign Language Oscar.   

==Cast==
* Lotfi Abdelli as Simon
* Jimi Busuttil as Karmenu
* Sékouba Doucouré as Moussa
* Chrysander Agius as John
* Adrian Farrugia as Theo
* Clare Agius as Sharin
* Mark Mifsud as Alex

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Maltese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 