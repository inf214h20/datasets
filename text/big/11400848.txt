The Adventures of Tintin (film)
 
 
{{Infobox film
| name = The Adventures of Tintin
| image = The Adventures of Tintin - Secret of the Unicorn.jpg
| caption = US release poster
| director = Steven Spielberg Kathleen Kennedy Joe Cornish
| based on =  
| starring = Jamie Bell Andy Serkis Daniel Craig Nick Frost Simon Pegg Daniel Mays Mackenzie Crook Toby Jones Gad Elmaleh
| music = John Williams
| cinematography = Janusz Kamiński Michael Kahn
| studio = Nickelodeon Movies Amblin Entertainment The Kennedy/Marshall Company WingNut Films Hemisphere Media Capital
| distributor = Paramount Pictures (North America/Asia Pacific/United Kingdom) Columbia Pictures    (International)
| released =  
| runtime = 107 minutes 
| country =  United States New Zealand
| language = English
| budget = $135 million   
| gross = $374 million 
}}
The Adventures of Tintin, also known as The Adventures of Tintin: The Secret of the Unicorn, is a 2011   (1941), The Secret of the Unicorn (1943), and Red Rackhams Treasure (1944).  The cast includes Jamie Bell, Andy Serkis, Daniel Craig, Nick Frost and Simon Pegg.
 Universal opted Sony chose to co-produce the film. The delay resulted in Thomas Sangster, who had been originally cast as Tintin, departing from the project. Producer Peter Jackson, whose company Weta Digital provided the computer animation, intends to direct a sequel. Spielberg and Jackson also hope to co-direct a third film.   The world première took place on 22 October 2011 in Brussels.  The film was released in the UK and other European countries on 26 October 2011, and in the USA on 21 December 2011, in Digital 3D and IMAX 3D|IMAX.   
 Best Animated Best Director Best Music Teenage Mutant Ninja Turtles surpassed its worldwide gross.

==Plot== Tintin and Snowy are Barnaby and Ivan Ivanovitch Sakharine, who both unsuccessfully try to buy the model from Tintin. Tintin takes the ship home, but it is accidentally broken, resulting in a parchment scroll slipping out of the model and rolling under a piece of furniture. Meanwhile, detectives Thomson and Thompson are on the trail of a pickpocket, Aristides Silk. Tintin, later finds that the Unicorn has been stolen. He then visits Sakharine in Marlinspike Hall and accuses him of the theft when he sees a miniature model of the Unicorn, but when he notices that Sakharines model is not broken, he realizes that there are two Unicorn models. Once Tintin returns home, Snowy shows him the scroll and after reading an old message written on it, Tintin puts the scroll in his wallet, but it is stolen by Silk, the next morning.
 Moroccan port of Settings in The Adventures of Tintin|Bagghar, but the seaplane crashes in the desert, due to low fuel.

While trekking through the desert, Haddock hallucinates and remembers facts about an ancestor of his,   and after his eventual surrender, Sir Francis sank the Unicorn, and most of the treasure, to prevent it from falling into Rackhams possession. It transpires that there were three Unicorn models, each containing a scroll; together, the scrolls can reveal coordinates to the location of the sunken Unicorn and its treasure.

The third model is in Bagghar, possessed by Omar ben Salaad. Sakharine causes a distraction in a concert that results in him successfully stealing the third scroll. After a chase, he gains all the scrolls by having his gang toss Captain Haddock and Snowy in the water to force Tintin to go after him instead of saving the scrolls. After the boat leaves, Tintin is ready to give up but is persuaded by Haddock to continue. With help from officers Thompson and Thomson, Tintin and Haddock track Sakharine down, who is revealed to be a descendant of Red Rackham. They head back to their starting point, and set up a trap, but Sakharine uses his pistol to resist arrest. When his gang fails to save him, Sakharine challenges Haddock to a final showdown. Sakharine and Haddock sword-duel with cranes and swords eventually resulting in Sakharine being defeated and pushed overboard the ship by Haddock. When climbing ashore, Sakharine is arrested by Thomson and Thompson. Guided by the three scrolls indicating the location of Marlinspike Hall, Tintin and Haddock find there some of the treasure and a clue to the Unicorn s location. The film ends with both men agreeing to continue their search of the shipwreck.

==Cast==
  pictured at the films premiere in 2011]] King Kong remake.  Pythonesque quality.  The actor researched about seaman|seamen, and gave Haddock a Scottish accent as he felt the character had "a rawness, an emotional availability, a more Celtic kind of feel". 
* Daniel Craig as Ivan Ivanovitch Sakharine and Red Rackham, Sakharine being the descendant of Red Rackham, the pirate who attacked the Unicorn (ship)|Unicorn, the ship captained by Sir Francis Haddock.  Spielberg described Sakharine as a "champagne villain, cruel when he has to be but with a certain elegance to him." Jackson and Spielberg decided to promote Sakharine from a relatively minor character to the main antagonist, and while considering an "interesting actor" to portray him Spielberg came up with Craig, with whom he had worked on Munich (film)|Munich.  Craig joked that he followed "the English tradition of playing bad guys".  How to Burke & Hare, in 2010.
* Enn Reitel as Nestor (comics)|Nestor, Captain Haddocks butler; and Mr. Crabtree, a vendor who sells the Unicorn to Tintin.
* Tony Curran as Lieutenant Delcourt, an ally of Tintin. 
* Toby Jones as Aristides Silk, a pickpocket.  
* Gad Elmaleh as Omar ben Salaad, an Arab potentate.  Elmaleh stated that his accent was "the childhood coming back". 
* Daniel Mays as Allan Thompson (comics)|Allan, Captain Haddocks first mate. 
* Mackenzie Crook as Tom, a thug on the Karaboudjan. Barnaby Dawes, an Interpol agent who tries to warn Tintin about purchasing the Unicorn and winds up shot by Sakharines thugs on Tintins doorstep.
* Kim Stengel as Bianca Castafiore, a comical opera singer. While Castafiore was absent from the three stories, Jackson stated she was added for her status as an "iconic character" and because she would be a fun element of the plot. 
* Sonje Fortag as Mrs. Finch (comics)|Mrs. Finch, Tintins landlady.
* Cary Elwes and Phillip Rhys as seaplane pilots.
* Ron Bottitta as Unicorn Lookout.
* Mark Ivanir as Afgar Outpost Soldier/Secretary.
* Sebastian Roché as Pedro/1st Mate.
* Nathan Meister as a market artist who bears the resemblance of Hergé.
* Sana Etoile as Press Reporter.

==Production==

===Development===
Spielberg had been an avid fan of The Adventures of Tintin comic books, which he discovered in 1981 when a review compared   of Amblin Entertainment were scheduled to meet with Hergé in 1983 while filming Indiana Jones and the Temple of Doom in London. Hergé died that week, but his widow decided to give them the rights.  A three-year-long option to film the comics was finalized in 1984,  with Universal as distributor. 

Spielberg commissioned E.T. the Extra-Terrestrial writer Melissa Mathison to script a film where Tintin battles ivory hunters in Africa.  Spielberg saw Tintin as "Indiana Jones for kids" and wanted Jack Nicholson to play Haddock.  Unsatisfied with the script, Spielberg continued with production on Indiana Jones and the Last Crusade. The rights returned to the Hergé Foundation. Claude Berri and Roman Polanski became interested in filming the property, while Warner Bros. negotiated for the rights, but they could not guarantee the "creative integrity" that the Foundation found in Spielberg.  In 2001, Spielberg revealed his interest in depicting Tintin with computer animation.  In November 2002, his studio DreamWorks reestablished the option to film the series.  Spielberg said he would just produce the film.    In 2004, the French magazine Capital (French magazine)|Capital reported Spielberg was intending a trilogy based on The Secret of the Unicorn / Red Rackhams Treasure, The Seven Crystal Balls / Prisoners of the Sun and The Blue Lotus / Tintin in Tibet (which are separate stories, but both feature Chang Chong-Chen).  By then, Spielberg had reverted to his idea of a live-action adaptation, and called Peter Jackson to ask if Weta Digital would create a computer-generated Snowy.   

 
 The Lord King Kong. He suggested that a live action adaptation would not do justice to the comic books and motion capture was the best way of representing Hergés world of Tintin.  A week of filming took place in November 2006 in Playa Vista, Los Angeles, California, on the stage where James Cameron shot Avatar (2009 film)|Avatar.    Andy Serkis had been cast, while Jackson stood in for Tintin.  Cameron and Robert Zemeckis were present during the shoot.  The footage was transmitted to Weta Digital,  who produced a twenty-minute test reel that demonstrated a photorealistic depiction of the characters.  Spielberg said he would not mind filming it digitally because he saw it as an animated film, and reiterated his live action work would always be filmed traditionally.  Lead designer Chris Guise visited Brussels to see the inspiration for Hergés sceneries. 
 The Lovely love bombed" Joe Cornish, a fan of Tintin with whom Wright was working at the time, worked on it with him. 

More filming took place in March 2008.    However, in August 2008, a month before   to co-finance and distribute the first film by the end of October.        Sony only agreed to finance two films, though Jackson said a third film may still happen. 

===Filming and visual effects===
Filming began on 26 January 2009, and the release date was moved from 2010 to 2011.  Spielberg finished his film—after 32 days of shooting—in March 2009. Jackson was present for the first week of filming and supervised the rest of the shoot via a bespoke videoconferencing program.  Simon Pegg said Jacksons voice would "be coming over the Tannoy like God."  During filming, various directors including Guillermo del Toro, Stephen Daldry and David Fincher visited. Spielberg would try to treat the film like live-action, moving his camera around.  He revealed, "Every movie I made, up until Tintin, I always kept one eye closed when Ive been framing a shot," because he wanted to see the movie in 2-D, the way viewers would. "On Tintin, I have both of my eyes open."  Jackson took the hands-on approach to directing Weta Digital during post-production, which Spielberg supervised through video conferencing. Jackson will also begin development for the second film, for which he will be officially credited as director.    Spielberg says "there will be no cell phones, no TV sets, no modern cars. Just timeless Europe."  His cinematographer Janusz Kamiński serves as lighting consultant for Weta, and Jackson said the film will look "film noirish, very atmospheric." Spielberg finished six weeks of additional motion-capture filming in mid-July 2009.   Post production was finished on September 2011. 
 ray tracing software called PantaRay, which required 100 to 1000 times more computation than traditional shadow-map based solutions.  For the performance of "Snowy", various models served as a reference for actors on-set, manipulated by property master Brad Elliott. Later, a dogs motion was captured digitally, so the animators had inspiration for realistic movements. His vocal effects were taken from various breeds of dogs. 

===Music===
{{Infobox album  
| Name        = Music from the Motion Picture: The Adventures of Tintin : The Secret of the Unicorn
| Type        = film John Williams
| Cover       =  The Adventures of Tintin (soundtrack).jpg
| Released    =  
| Recorded    =
| Genre       = Soundtrack
| Length      =   Sony Classical Indiana Jones and the Kingdom of the Crystal Skull (2008)
| This album  = The Adventures of Tintin (2011) War Horse (2011)
}}
{{Album ratings rev1 =   rev1score =   rev2 =   rev2score =   rev3 =   rev3score =   rev4 =   rev4score =   rev5 =   rev5score =   rev6 =   rev6score =   rev7 =   rev7score =   rev8 =   rev8score =  
}}
John Williams composed the musical score for The Adventures of Tintin. It was Williams first film score since 2008s Indiana Jones and the Kingdom of the Crystal Skull,  as well as his first animated film. Most of the score was written while the films animation was still in the early stages, with Williams attempting to employ "the old Disney technique of doing music first and have the animators trying to follow what the music is doing". Eventually several cues had to be revised when the film was edited. The composer decided to employ various musical styles, with "1920s, 1930s European jazz" for the opening credits, or "pirate music" for the battle at sea.  It was released on 21 October 2011 through Sony Classical Records. 

The score received very positive reviews from critics.

;Track listing
{{Track listing
| title1       = The Adventures of Tintin
| length1      = 3:07
| title2       = Snowys Theme
| length2      = 2:09
| title3       = The Secret of the Scrolls
| length3      = 3:12
| title4       = Introducing the Thompsons and Snowy’s Chase
| length4      = 4:08
| title5       = Marlinspike Hall
| length5      = 3:58
| title6       = Escape from the Karaboudjan
| length6      = 3:20
| title7       = Sir Francis and the Unicorn
| length7      = 5:05
| title8       = Captain Haddock Takes the Oars
| length8      = 2:17
| title9       = Red Rackham’s Curse and the Treasure
| length9      = 6:10
| title10      = Capturing Mr. Silk
| length10     = 2:57
| title11      = The Flight to Bagghar
| length11     = 3:33
| title12      = The Milanese Nightingale
| length12     = 1:29
| title13      = Presenting Bianca Castafiore
| length13     = 3:27
| title14      = The Pursuit of the Falcon
| length14     = 5:43
| title15      = The Captain’s Counsel
| length15     = 2:10
| title16      = The Clash of the Cranes
| length16     = 3:48
| title17      = The Return to Marlinspike Hall and Finale
| length17     = 5:51
| title18      = The Adventure Continues
| length18     = 2:58
}}

==Differences from the source material==
The film mainly draws its story from The Secret of the Unicorn (1943) and The Crab with the Golden Claws (1941), and to a much lesser degree from Red Rackhams Treasure (1944). There are major differences from the source material, most notably with regard to the antagonists. In the book, Ivan Sakharine is a minor character, neither a villain nor the descendant of Red Rackham, and the main villains are instead the Bird brothers, who are absent from the film adaptation (save for a small "cameo" in the initial sequence at the market). As a result many events occur that bear no relation to events in the books involving Sakharines character.    As in other adaptations Snowys "voice" is not used.

==Distribution==

===Video game===
  iOS devices to coincide with the films European launch. 

===Theatrical release===
  at the films premiere in Paris, 22 October 2011.]] Princess Astrid Princess Luisa Princess Laetitia Maria; with the Paris première later the same day.    Sony later released the film during late October and early November 2011 in Europe, Latin America, and India. The film was released in Quebec on 9 December 2011.  Paramount distributed the film in Asia, New Zealand, the U.K., and all other English-speaking territories. They released the film in the United States on 21 December 2011.  

===Home media===
On 13 March 2012, Paramount Home Entertainment released The Adventures of Tintin on DVD and Blu-ray.  Both formats of the film were also released in a Blu-ray + DVD + Digital Copy combo pack and a Blu-ray 3D + Blu-ray + DVD + Digital Copy combo pack, with each pack including 11 behind-the-scenes featurettes. 

During its first week available on home video, The Adventures of Tintin Blu-ray was the number one selling HD movie after selling 504,000 units and generating $11.09 million in sales.  The film was also the second highest selling home media seller during its first week, with 50% of its profits coming from its Blu-ray market. 

==Reception==

===Critical response===
 
The Adventures of Tintin received positive reviews from critics. Based on 197 reviews collected by review aggregate site Rotten Tomatoes, the film scored a 75% "Certified Fresh" approval rating, with an average rating of 7/10. The sites critical consensus is, "Drawing deep from the classic Raiders of the Lost Ark playbook, Steven Spielberg has crafted another spirited, thrilling adventure in the form of Tintin."    Metacritic, another review aggregator which assigns a weighted mean rating out of 100 to reviews from mainstream critics, calculated an average score of 68, based on 40 reviews, which indicates "generally favorable reviews".   

Colin Covert of Star Tribune gave the film 4 out of 4 stars and said that Spielbergs first venture into animation was his most delightful dose of pure entertainment since Raiders of the Lost Ark.  Amy Biancolli of the San Francisco Chronicle wrote, "Such are the timeless joys of the books (and now the movie), this sparkling absurdity and knack for buckling swash under the worst of circumstances. The boy may have the worlds strangest cowlick, but he sure can roll with the punches." 

Roger Ebert, writing for Chicago Sun-Times, labeled the film as "an ambitious and lively caper, miles smarter than your average 3-D family film." He praised the setting of the film, stating its similarity to the original Tintin comic strips, and was also pleased with the 3-D used in the film, saying that Spielberg employed it as an enhancement to 2-D instead of an attention-grabbing gimmick. He gave it 3.5 out of 4 stars. 

Peter Travers of Rolling Stone gave the film 3.5 out of 4 stars and wrote, "The movie comes at you in a whoosh, like a volcano of creative ideas in full eruption. Presented as the first part of a trilogy produced by Spielberg and Peter Jackson, The Adventures of Tintin hits home for the kid in all of us who wants to bust out and run free."  Kenneth Turan of Los Angeles Times said, "Think of "The Adventures of Tintin" as a song of innocence and experience, able to combine a sweet sense of childlike wonder and pureness of heart with the most worldly and sophisticated of modern technology. More than anything, its just a whole lot of fun." 

Giving the film 3.5 out of 4 stars, Lou Lumenick of New York Post wrote, "Spielberg and an army of collaborators — deploying motion capture and 3-D more skillfully than in any film since "Avatar" — turn this unlikely material into one of the year’s most pleasurable, family-friendly experiences, a grand thrill ride of a treasure hunt."  Richard Corliss of Time (magazine)|Time wrote, "Motion capture, which transforms actors into cartoon characters in a vividly animated landscape, is the technique Spielberg has been waiting for - the Christmas gift ... that hes dreamed of since his movie childhood." 

Jordan Mintzer of  ". 

Belgian newspaper Le Soir s film critics Daniel Couvreur and Nicolas Crousse called the film "a great popular adventure movie," stating "  enthusiasm and childhood spirit are unreservedly infectious."  Le Figaro praised the film, considering it to be "crammed with action, humor and suspense."  Leslie Felperin of Variety (magazine)|Variety wrote, "Clearly rejuvenated by his collaboration with producer Peter Jackson, and blessed with a smart script and the best craftsmanship money can buy, Spielberg has fashioned a whiz-bang thrill ride thats largely faithful to the wholesome spirit of his source but still appealing to younger, Tintin-challenged Audience|auds." 

The film was named in New York (magazine)|New York magazines David Edelsteins Top 10 List for 2011.  It was also included in HitFixs top 10 films of 2011. 

La Libre Belgique was, however, a little less enthusiastic; its film critic Alain Lorfèvre called the film "a technical success,   a Tintin vivid as it should be   a somewhat excessive Haddock."  The Guardian s Xan Brooks gave the film two stars out of five, stating: "while the big set pieces are often exuberantly handled, the human details are sorely wanting. How curious that Hergé achieved more expression with his use of ink-spot eyes and humble line drawings than a bank of computers and an army of animators were able to achieve." 

Blog Critics writer Ross Miller said, "author Hergés wonderfully bold and diverse array of characters are a mixed bag when it comes to how theyve been translated to the big-screen" and that while the mystery might be "perfectly serviceable" for the film, "the execution of it at times feels languid and stodgy, like its stumbling along from one eye-catching setpiece to the next." However, he summed it up as, "an enjoyable watch with some spectacular set-pieces, lavish visuals and some fine motion-capture performances." 
 Rambo blasting his way through the British Raj|Raj." 

Steve Rose from The Guardian wrote about one of the movies major criticisms: that The Adventures of Tintin, much like The Polar Express, crossed into the uncanny valley, thereby rendering Tintin "too human and not human at all."  Nicholas Lezard, also from The Guardian wrote: 
{{cquote|
As it is, the film has turned a subtle, intricate and beautiful work of art into the typical bombast of the modern blockbuster, Tintin for morons, and the nicest things one can say about it are that theres a pleasing cameo of Hergé himself in the opening scene, the cars look lovely, indeed it is as a whole visually sumptuous, and (after 20 minutes or so of more or less acceptable fidelity; and the 3D motion-capturing transference of the original drawings is by far the least of the films problems) it usefully places in plain view all the cretinous arrogance of modern mass-market, script-conference-driven film-making, confirming in passing that, as a director, Spielberg is a burned-out sun. A duel between dockyard cranes? Give me a break.  }}

Manohla Dargis, one of the chief critics of the New York Times, called the movie "a marvel of gee-wizardry and a nights entertainment that can feel like a lifetime." The simplicity of the comic strip, she wrote, is a crucial part of the success of Tintin, who is "an avatar for armchair adventurers." Dargis noted that Tintins appearance in the film "resembled Hergés creation, yet was eerily different as if, like Pinocchio, his transformation into human form had been prematurely interrupted." Another major fault in the film, Dargis points out, is how it is so wildly overworked; she writes that there is "hardly a moment of downtime, a chance to catch your breath or contemplate the tension between the animated Expressionism and the photo-realist flourishes." Nevertheless, she singles out some of the "interludes of cinematic delight," approving of the visual imagination employed within the movies numerous exciting scenes. Dargis, Manohla.   The New York Times. (20 Dec. 2011) 

===Box office===
The film grossed $77,591,831 in North America and $296,402,120 in other territories, for a worldwide total of $373,993,951.   
 CIS ($4.81 million).  

The film grossed   on its opening weekend ( 11–13 November 2011) in India, an all-time record for a Spielberg film and for an animated feature in India. The film was released with 351 prints, the largest ever release for an animated film.    In four weeks, it became the highest-grossing animated film of all time in the country with  .  In the United States, it is one of only twelve feature films to be released in over 3,000 theaters and still improve on its box office performance in its second weekend, increasing 17.6% from $9,720,993 to $11,436,160. 

===Accolades=== Best Animated Feature Film.  It also received two nominations at the 65th British Academy Film Awards in the categories of Best Animated Film and Best Special Visual Effects. 

{| class="collapsible collapsed" style="width:100%; border:1px solid #cedff2; background:#F5FAFF"
|+ List of awards and nominations
|-
! Award !! Category !! Recipients and nominees !! Result
|-
| Academy Awards  Best Original Score
| John Williams
|  
|-
| Alliance of Women Film Journalists  Best Animated Film
|  
|-
| rowspan="4" | Annie Award  Annie Award Best Animated Feature
|  
|-
| Best Animated Effects in an Animated Production
| Kevin Romond
|  
|-
| Best Music in a Feature
| John Williams
|  
|- Best Writing in a Feature Production Joe Cornish
|  
|-
| Art Directors Guild  Fantasy Film
|  
|- BAFTA Awards    Best Animated Film
| Steven Spielberg
|  
|- Best Special Effects
| Joe Letteri
|  
|- BMI Film & TV Awards 
| Film Music Award
| John Williams
|  
|-
| Chicago Film Critics Association
|colspan=2| Best Animated Film
|  
|-
| Critics Choice Movie Awards  Best Animated Feature
|  
|-
| Dallas-Fort Worth Film Critics Best Animated Film
|  
|-
| Empire Awards  The Art of 3D
|  
|-
| Florida Film Critics Circle  Florida Film Best Animated Film
|  
|- Golden Globe Awards    Best Animated Feature Film
| Steven Spielberg
|  
|- Golden Trailer Awards 
| colspan=2| Best Animation/Family
|  
|- Best Pre-show Theatrical Advertising
|  
|- Grammy Awards  Best Score Soundtrack For Visual Media
| John Williams
|  
|-
| rowspan="2" | Houston Film Critics Society  Best Animated Film
|  
|-
| Best Original Score
| John Williams
|  
|- IGN Best of 2011  Best Animated Movie 
| 
|-
| Best Movie Actor
| Andy Serkis 
| 
|-
| Los Angeles Film Critics Association  Los Angeles Best Animation
|  
|-
| New York Film Critics Online Best Animated Film
|  
|- Oklahoma Film Critics Circle Oklahoma Film Best Animated Feature
|  
|-
| Online Film Critics Society  Online Film Best Animated Feature
|  
|-
| Phoenix Film Critics Society   Best Animated Film
|  
|-
| Producers Guild of America Award 
| Outstanding Producer of Animated Theatrical Motion Picture Kathleen Kennedy and Steven Spielberg
|  
|-
| rowspan="2" | Satellite Awards  Satellite Award Best Motion Picture, Animated or Mixed Media
|  
|- Best Adapted Screenplay
| Steven Moffat, Edgar Wright and Joe Cornish
|  
|- Saturn Awards  Saturn Award Best Animated Film
|  
|- Best Director
| Steven Spielberg
|  
|- Best Music
| John Williams
|  
|-
| Best Production Design
| Kim Sinclair
|  
|- Best Editing Michael Kahn
|  
|- Best Special Effects Matt Aitken, Joe Letteri, Matthias Menz and Keith Miller
| 
|-
| St. Louis Gateway Film Critics Association Awards Best Animated Film
|  
|-
| Toronto Film Critics Association Best Animated Film
|  
|-
| Utah Film Critics Association Best Animated Feature
|  
|-
| rowspan="6" | Visual Effects Society   Outstanding Visual Effects in an Animated Feature Motion Picture
| Jamie Beard, Joe Letteri, Meredith Meyer-Nichols, Eileen Moran
|  
|-
| Outstanding Animated Character in an Animated Feature Motion Picture Tintin — Gino Acevedo, Gustav Ahren, Jamie Beard, Simon Clutterbuck
|  
|- Outstanding Created Environment in an Animated Feature Motion Picture
| Bagghar — Hamish Beachman, Adam King, Wayne Stables, Mark Tait
|  
|-
| Docks — Matt Aitken, Jeff Capogreco, Jason Lazaroff, Alessandro Mozzato
|  
|-
| Pirate Battle — Phil Barrenger, Keith F. Miller, Alessandro Saponi, Christoph Sprenger
|  
|-
| Outstanding Virtual Cinematography in an Animated Feature Motion Picture 
| Matt Aitken, Matthias Menz, Keith F. Miller, Wayne Stables
|  
|-
| Washington D.C. Area Film Critics Association  Best Animated Feature
|  
|-
| Women Film Critics Circle  Best Family Film
| 
|- World Soundtrack World Soundtrack Academy  World Soundtrack Best Original Soundtrack of the Year
|rowspan=2| John Williams
| 
|- World Soundtrack Soundtrack Composer of the Year
| 
 
|}

==Sequels==
Originally the second film was going to be based on The Seven Crystal Balls and Prisoners of the Sun.    however screenwriter Anthony Horowitz later stated that those books would be the second sequel and another story would become the first sequel.   
 Destination Moon and Explorers on the Moon as the basis for a third or fourth film in the series. 

In December 2011, Spielberg said the book that would form the sequel had been chosen, and that the Thompson and Thomson detectives would "have a much bigger role".    The sequel would be produced by Spielberg and directed by Jackson.  Kathleen Kennedy said the script might be done by February or March 2012 and motion-captured in summer 2012, so that the film would be on track to be released by Christmas 2014 or mid-2015. 

In February 2012, Spielberg said that a story outline for the sequel had been completed and that it was based on two books.  In May 2012, Horowitz   in December 2012, Jackson said he intended to shoot performance-capture in 2013, aiming for a release date in 2015. 

On 12 March 2013, Spielberg said, "Dont hold me to it, but were hoping the film will come out around Christmas-time in 2015. We know which books were making, we cant share that now but were combining two books which were always intended to be combined by Hergé." He refused to confirm the names of the books, but said The Blue Lotus would probably be the third Tintin film. 
 New Zealand films before that. 

==See also==
* The Adventures of Tintin
* The Adventures of Tintin (TV series)|The Adventures of Tintin (TV series)
* Tintin and the Golden Fleece|Tintin and Golden Fleece (1961 film)
* Tintin and the Blue Oranges|Tintin and the Blue Oranges (1964 film)
* Tintin and the Lake of Sharks|Tintin and the Lake of Sharks (1972 animated film)
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   at Tintinologist.org
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 