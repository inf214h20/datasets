Big Bullet
 
 
{{Infobox film
| name           = Big Bullet
| image          = Big-bullet-poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Hong Kong poster for Big Bullet Benny Chan
| producer       = Benny Chan Joe Ma Susan Chan Benny Chan
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Lau Ching-wan Anthony Wong Yu Rongguang Lam Sheung Yee
| music          = Clarence Hui Peter Kam
| cinematography = Arthur Wong
| editing        = Cheung Kar-fai Golden Harvest
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$ 9,771,575
}}
 Benny Chan. The film won awards for film editing at the 1996 Golden Horse Film Festival and the 16th Hong Kong Film Awards.

==Plot==

After assaulting his tactical commander during a raid gone sour, a dedicated but temperamental cop Sergeant Bill Chu is demoted to the Emergency Unit, long considered to be the police forces dumping ground for problem cops.

==Release==
Big Bullet was released in Hong Kong on 26 July 1996. It grossed a total of HK$ 9,771,575.  At the 16th  ) and Best Original Music (Peter Kam). Zhang Yaozong and Zhang Jiahui won the Best Editing award for their work on Big Bullet. 

At the 1996 Golden Horse Film Festival, Big Bullet was nominated for Best Action Choreography (Ma Yuk Sing) and won the award for Best Film Editing (Peter Cheung and Cheung Ka Fai). 

==Cast==
* Lau Ching-wan
* Jordan Chan
* Theresa Lee
* Cheung Tat-Ming
* Francis Ng
* Anthony Wong
* Yu Rongguang
* Lam Sheung Yee
* Vincent Kok
* Dayo Wong

 
 

==Notes==
 

==See also==
 
* Hong Kong films of 1996
* List of action films of the 1990s

==External links==
*  
*  

 

 
 
 
 
 

 
 