Imps*
{{Infobox film
| name           = Imps*
| image          = Imps-film.jpg
| alt            =  
| caption        = 
| director       = Scott Mansfield
| producer       = Jere Rae
| writer         = Scott Mansfield
| starring       = Linda Blair Colleen Camp Julia Duffy Erika Eleniak Michael McKean Miguel A. Núñez, Jr. Jennifer Tilly Fred Willard
| music          = Bill Elliot
| cinematography = Alec Hirschfeld
| editing        = Stu Eisenberg Barbara Noble
| studio         = 
| distributor    = Imps Company Monterey Media
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Imps* is a comedy film released in 2009. The film stars an ensemble cast and is divided into several segments.

IMPS is an acronym for "Immoral Minority Picture Show".

==Production==

Although released in 2009, Imps* was actually filmed in the 1980s and shelved. In 2009, faced with an advertising budget of $500 and culturally irrelevant subject matter,  , she replied without a doubt. 

==Description== sketches and Marquessa de Sade, and a horror heroine.

==Filming==
The film was shot in Los Angeles, California.

==Cast==
* Linda Blair as Jamie
* Colleen Camp as Young Lady
* Julia Duffy as Marjorie
* Erika Eleniak as Brooke
* Michael McKean as Fritz #2
* Miguel A. Núñez, Jr. as Bopper
* Jennifer Tilly as Joni
* Fred Willard as Dad

==Release==
The DVD premiered on February 10, 2009.

==References==
 

==External links==
* 
*  at Rotten Tomatoes

 
 