Vicky Daada
{{Infobox film
| name           = Vicky Daada
| image          =
| caption        =
| director       = A. Kodandarami Reddy
| producer       = D. Sivaprasad Reddy
| writer         = Ganesh Patro  
| story          = Yandamuri Veerendranath
| screenplay     = Paruchuri Brothers Radha Juhi Chawla
| music          = Raj-Koti
| cinematography = N. Sudhakar Reddy
| editing        = D. Venkataratnam
| studio         = Kamakshi Art Movies
| distributor    =
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
Vicky Daada is a 1989 Telugu cinema|Telugu, crime film produced by D. Sivaprasad Reddy on Kamakshi Art Movies banner, directed by A. Kodandarami Reddy. Starring Akkineni Nagarjuna, Radha (actress)|Radha, Juhi Chawla lead roles and music is composed by Raj-Koti. The film recorded Super Hit at box-office.    
==Cast==
 
*Akkineni Nagarjuna Radha
*Juhi Chawla  Kannada Prabhakar
*Gollapudi Maruthi Rao
*Giri Babu Ranganath
*Kota Srinivasa Rao Sudhakar 
*Prasad Babu 
*Vinod
*Vankayala
*Bhimeswara Rao
*Dham
*Srividya
*Vara Lakshmi
*Kalpana Rai
 

==Soundtrack==
{{Infobox album
| Name        = Vicky Daada
| Tagline     = 
| Type        = film
| Artist      = Raj-Koti
| Cover       = 
| Released    = 1989
| Recorded    = 
| Genre       = Soundtrack
| Length      = 21:55
| Label       = AMC Audio
| Producer    = Raj-Koti
| Reviews     =
| Last album  = Gunda Rajyam   (1989)
| This album  = Vicky Dada   (1989)
| Next album  = Bala Gopaludu   (1989)
}}

The film songs composed by Raj-Koti. Lyrics written by Veturi Sundararama Murthy. All the songs are hit tracks. Music released on AMC Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 21:55
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =


| title1  = O Baby Neemeeda
| extra1 = Mano (singer)|Mano, S. Janaki
| length1 = 4:23

| title2  = Abba Takku Tikku SP Balu, P. Susheela
| length2 = 4:07

| title3  = Beauty Beauty SP Balu, S. Janaki
| length3 = 4:57

| title4  = Ganta Gantaki Motaguntadi SP Balu, S. Janaki
| length4 = 4:27

| title5  = Jarindammo Jarindammo SP Balu, S. Janaki
| length5 = 4:01
}}

==References==
 

 
 
 
 
 
 

 