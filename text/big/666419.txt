Mr. Duck Steps Out
{{Infobox Hollywood cartoon
|cartoon_name=Mr. Duck Steps Out
|series=Donald Duck 
|image=Mr. Duck Steps Out.png
|caption=Theatrical release poster Jack King
|story_artist=Carl Barks Don Towsley, Ken Muse, Dick Lundy, Phil Duncan, Larry Strickland, Judge Whitaker, Jim Armstrong
|layout_artist=Bill Herwig
|voice_actor=Clarence Nash
|musician=Charles Wolcott
|producer=Walt Disney RKO Radio Pictures Walt Disney Productions
|release_date=June 7, 1940
|color_process=Technicolor
|runtime=8:12 minutes
|country=United States
|movie_language=English
}}
 Jack King and scripted by Carl Barks.

==Plot==
Donald visits the house of his new love interest for their first known date. At first Daisy acts shy and has her back turned to her visitor. But Donald soon notices her tailfeathers taking the form of a hand and signaling for him to come closer. But their time alone is soon interrupted by Huey, Dewey and Louie who have followed their uncle and clearly compete with him for the attention of Daisy.

Uncle and nephews take turns dancing the jitterbug with her while trying to get rid of each other. In their final effort the three younger Ducks feed their uncle maize in the process of becoming popcorn. The process is completed within Donald himself who continues to move wildly around the house while maintaining the appearance of dancing. The short ends with an impressed Daisy showering her new lover with kisses. 

==History==
The short stands out among other Donald shorts of the period for its use of modern music and surreal situations throughout. After this short, the idea of a permanent love interest for Donald was well established. However, Daisy did not appear as regularly as Donald himself.

==Availability==
*1940 &ndash; Theatrical release Walt Disneys Wonderful World of Color, episode #8.6: "Inside Donald Duck" (TV)
*1977 &ndash; Donald and His Duckling Gang (theatrical)
*c. 1983 &ndash; Good Morning, Mickey!, episode #61 (TV)
*1984 &ndash; "From Disney With Love" (TV)
*1984 &ndash; "Cartoon Classics - Limited Gold Edition: Daisy" (VHS)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #31 (TV)
*c. 1992 &ndash; Donalds Quack Attack, episode #45 (TV)
*1994 &ndash; "Love Tales" (VHS) The Ink and Paint Club, episode #1.20: "Huey, Dewey & Louie" (TV) The Ink and Paint Club, episode #1.40: "Crazy Over Daisy" (TV)
*2004 &ndash; "Mickey and Minnies Sweetheart Stories" (DVD)
*2004 &ndash; " " (DVD)
*2006 &ndash; " " (DVD)

==References==
 

==External links==
* 
* 
 
 
 
 
 
 
 
 

 