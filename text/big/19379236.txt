Wendy and Lucy
{{Infobox film
| name     = Wendy and Lucy
| image    = Wendy and lucy.jpg
| caption  = Promotional poster
| director = Kelly Reichardt
| writer   = Kelly Reichardt Jonathan Raymond
| producer = Larry Fassenden Neil Kopp Anish Savjani Michelle Williams Will Patton
| released =  
| distributor = Oscilloscope Laboratories
| runtime  = 80 minutes
| country  = United States
| language = English
| gross    = $1,192,655   
| budget   = $300,000   
}}
 2008 Cinema American drama Michelle Williams and Will Patton. It had its world premiere at the 2008 Cannes Film Festival and was screened at several additional film festivals before receiving a limited theatrical release in the United States on December 10, 2008.

==Plot== Michelle Williams) is a young woman who sets her sights on Alaska in hopes of starting a new life with her dog Lucy, travelling in her car with limited supplies and a straining budget of a little over $500 left for the trip. Stranded in Oregon when her car breaks down and lacking the funds to repair it, Wendy faces yet another challenge when she is apprehended for shoplifting and Lucy disappears while she is in police custody.

==Cast== Michelle Williams as Wendy Carroll
*Will Patton as Mechanic Walter Dalton as Security Guard
*Will Oldham as Icky John Robinson as Andy  Deirdre OConnell as Deb
*Lucy as Herself
*Larry Fessenden as Man in Park

==Critical reception== review aggregation rating average of 7,3 out of 10.    12th Toronto Film Critics Association Awards.  Wendy and Lucy was placed at 87 on Slant Magazines best films of the 2000s. 
===Box office===
In its opening weekend, the film grossed  $18,218 in 2 theaters in the United States, ranking #54 at the box office. By the end of its run, Wendy and Lucy grossed $865,695 domestically and  $326,960 internationally for a worldwide total of $1,192,655. 
===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008,      including those of the Chicago Reader, New York Post, Newsweek, The Austin Chronicle, LA Weekly, The Philadelphia Inquirer, the Seattle Post-Intelligencer, Entertainment Weekly, The New York Times, The Oregonian, Slate (magazine)|Slate, The Village Voice, and The Christian Science Monitor.
==Home media==
Wendy and Lucy was released on DVD on May 5, 2009.

==References==
 

==External links==
*  
*  
*  
*   movie review by Jonathan Raban from The New York Review of Books

 

 
 
 
 
 
 
 
 