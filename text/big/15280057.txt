The Bribe
 
{{Infobox film
| name           = The Bribe
| image          = The Bribe poster.jpg
| image_size     = 225px
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Z. Leonard
| producer       = Pandro S. Berman
| screenplay     = Marguerite Roberts
| based on       =  
| narrator       = Robert Taylor Ava Gardner Charles Laughton
| music          = Miklós Rózsa
| cinematography = Joseph Ruttenberg
| editing        = Gene Ruggiero
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $1,984,000  . 
| gross          = $2,510,000 
}} Robert Taylor, Ava Gardner, Charles Laughton, and Vincent Price. 

==Plot==
Federal agent Rigby (Taylor) travels to Los Trancos on the island of Carlota (somewhere off the coast of Central America) to break up a war-surplus aircraft engine racket and finds himself tempted by corruption, namely Elizabeth Hintten (Gardner), a café singer married to Tug Hintten (Hodiak), a drunken ex-pilot.

Carwood (Price) is the brains of the outfit, aided and abetted by J.J. Bealer (Laughton) and Hintten (Hodiak).

==Cast== Robert Taylor as Rigby
* Ava Gardner as Elizabeth Hintten
* Charles Laughton as J.J. Bealer
* Vincent Price as Carwood
* John Hodiak as Tugwell Tug Hintten
* Samuel S. Hinds as Dr. Warren
* John Hoyt as Gibbs
* Martin Garralaga as Pablo Gomez

==Reception==
===Box Office===
According to MGM records the movie earned $1,559,000 in the US and Canada and $951,000 overseas, resulting in a loss to the studio of $322,000.  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 401 

===Critical reception===
Film critic Bosley Crowther lambasted the drama in his film review, writing, "If you plan to put down your money to see the Capitols The Bribe, we suggest that you be prepared to write off this extravagance as a folly and nothing more. For The Bribe is the sort of temptation which Hollywood put in the way of gullible moviegoers about twenty years ago. Its a piece of pure romantic fiction, as lurid as it is absurd. And if it didnt have several big names in it, it would be low-man on a grind house triple-bill...The only hint which the director, Robert Z. Leonard, gives that he may have meant it all as pure nonsense comes at the very end, when he blows up the place with pyrotechnics. Thats the one appropriate move in the whole show." 
 Time Out film guide included the following in their review: "Price and Laughton make a formidable pair of heavies in this otherwise feeble thriller shot on a cheaply rigged-up corner of the MGM backlot. Taylor isnt up to moral dilemma as a US government agent sent to crack illicit aircraft engine trading in the Caribbean, yet tempted by a lucrative cash offer and the irresistible charm of café chanteuse Gardner." 

Critic Leslie Halliwell wrote in his film guide, "Steamy melodrama with pretensions but only moderate entertainment value despite high gloss. The rogues gallery, however, are impressive." 

In the book Cult Movies by Karl French and Philip French, they write, "In classic noir style, the chain smoking Rigby (he has no Christian name) tells most of the story in flashbacks that begin as visions he sees on the rain-lashed window of his hotel room.
His voiceover narration continues as he battles with his conscience and tries to retain his honour in a world reeking of corruption. 
Laughton and Price are splendidly hammy villains and Gardners nightclub singer is an innocent femme fatale in the manner of Rita Hayworths Gilda." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 