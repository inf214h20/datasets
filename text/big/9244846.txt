How to Play Golf
  short animated Walt Disney Productions film directed by Jack Kinney. Eight minutes long, it was distributed by RKO, and was a part of a series where Goofy learned to play various sports.

Upon release, The Film Daily called it "highly hilarious", and gave the following review:

 
"This, another of Walt Disneys Technicolor cartoons featuring the Goof, is a howl. While the narrator explains how properly to play golf, the Goof attempts to demonstrate. Naturally he does everything wrong, with superb results from a comedy point of view. The technique and animation are noteworthy indeed. Mark this down as a topflight booking of its kind."
 

==References==
* "Reviews of the New Films." Film Daily. 5/10/44. p13.

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 