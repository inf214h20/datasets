Sergeant Murphy
{{Infobox film
| name           = Sergeant Murphy
| image          = 
| caption        = 
| director       = B. Reeves Eason
| producer       =  William Jacobs
| starring       = Ronald Reagan
| music          = 
| cinematography = Ted D. McCord
| editing        = 
| distributor    = 
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = 
}}

Sergeant Murphy is a 1938 American comedy film directed by B. Reeves Eason and starring Ronald Reagan.   

==Cast==
* Ronald Reagan  as Pvt. Dennis Reilley
* Mary Maguire  as Miss Mary Lou Carruthers
* Donald Crisp  as Col. Todd Carruthers
* Ben Hendricks Jr.  as Cpl. Kane (as Ben Hendricks)
* William B. Davidson  as Maj. Gruff (as William Davidson)
* Max Hoffman Jr.  as Sgt. Buck Connors
* Robert Paige  as Lt. Duncan (as David Newell)
* Emmett Vogan  as Maj. Smythe
* Tracy Layne  as Texas (as Tracey Lane)
* Edmund Cobb  as Gruffs Adjutant Janet Shaw  as Joan Furse (as Ellen Clancy)
* Rosella Towne  as Alice Valentine
* Joan Valerie  as Bess Merrill (as Helen Valkis)
* Sam McDaniel  as Henry H. Henry (as Sam McDaniels)
* Sergeant Murphy as Sergeant Murphy - a Horse

==See also==
* Ronald Reagan filmography

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 