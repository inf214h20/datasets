Arsho
 
{{Infobox film
| name           = Arsho
| image          = 
| alt            =
| caption        =
| director       = Shapinder Pal Singh
| producer       = Dakssh Ajit Singh
| writer         =  
| starring       =  
| music          =  
| cinematography = Naren Gedia
| editing        =
| studio         =
| released       =  
| runtime        =
| country        = India
| language       = Punjabi
| budget         =
| gross          =
}}
 Indian romance film directed by Shapinder Pal Singh and produced by Dakssh Ajit Singh. The film released June 13, 2014, and cast includes  Mannat Singh, Dakssh Ajit Singh, Sonia Gill, Shakti Anand, and Komal,   and was shot in Chandigarh and surrounding areas. 

==Plot==
 
The film deals with the romance of duet singers Arsh (Mannat Singh) and Deep (Dakssh Ajit Singh) and the ongoing day-to-day struggles faced by such artists.

==Cast==
 
* Mannat Singh as Arsh
* Dakssh Ajit Singh as Deep
* Sonia Gill as Raavi
* Shakti Anand as Gammy
* Komal
* Razia Sukhbir
* Pavel Sandhu
* Harjeet Bhullar
* Manish Bisla
* Magie Mathur
* Jaswant Jass
* Gurinder Makna
* Satwant Ball
 

==Reception==
 
When the films trailer was released, it was not well received due to its lead female character of Arsho being seen on screen drinking openly in front of her on-screen father. Actress Mannat Singh defended the scene by stating she was in actuality drinking apple juice.   

In praising the film, Jasmine Singh of Tribune India wrote that a film does not require, "a battery of high-riding actors and comedians, endless nonsensical and repetitive gags or garishly dressed up villains to put together a good or lets say a watchable film,"  and offered that Arsho iacts as an example of how a good script can pave the way for a good film, further noting that by successfully writing, producing, acting, and singing in the film, director "Dakssh breaks every notion that a single person cannot multi-task in a film; lets be more precise, in a Punjabi film."   The director did well against other similar romances by "not exaggerating a single scene,"  and this acknowledgement also stands in praise of the films editor.  Further was made toward the films backdrop and locations and pacing. The screenplay and dialogues are "crisp as well as believable to a major extent."   Both Dakssh Ajit Signh and Mannat Singh gave commendable performances. The reviewer felt that the skills of television actor Shakti Anand were wasted in his small and little clumsy role.   The only flaw found in the film was "probably the bombardment of songs", but not overly so as the "background music by Aadesh Srivastva is a refreshing change."   

==Awards==

PTC Punjabi Film Awards 2015

Pending
*PTC Punjabi Film Award for Best Story - Shapinder Pal Singh & Dakssh Ajit Singh

==References==
 

==External links==
*   at the Internet Movie Database
*  
*  

 
 
 