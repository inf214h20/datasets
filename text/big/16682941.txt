Dans Paris
{{Infobox Film
| name           = Dans Paris 
| image          = Dans paris poster.jpg
| caption        = Movie poster for Dans Paris
| director       = Christophe Honoré
| producer       = 
| writer         = Christophe Honoré
| narrator       = 
| starring       = Romain Duris, Louis Garrel, Guy Marchand, Marie-France Pisier, Joanna Preiss
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 4 October 2006
| runtime        = 
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Dans Paris ( , Louis Garrel, Guy Marchand, Marie-France Pisier, and Joana Preiss.  It concerns two brothers, Paul (Duris) and Jonathan (Garrel), who attempt to help one another out of their respective troubles and worries while living with their divorced father, Mirko (Marchand).  Paul has broken up with Anna (Priess), while Jonathan is a womanizer. 

The New York Times reviewer praised the films "playful, liberatory style", which she found reminiscent of the best films of the French New Wave 

==Awards and nominations==
*César Awards (France) Best Actor &ndash; Supporting Role (Guy Marchand)

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 