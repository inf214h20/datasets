Chase (film)
 The Chase}}
 
{{Infobox film
| name = Chase
| image = Chase film.jpg
| image_size =
| caption =
| director = Jagmohan Mundhra
| producer = Anuj Saxena
| writer =
| starring = Anuj Saxena Udita Goswami Sameer Kochhar Aditya Raj Kapoor Ashok Banthia Tareena Patel Rajesh Khattar Shweta Menon
| music =
| cinematography = Uday Tiwari
| editing = Ram Singh
| distributor = Maverick Productions
| released =  
| runtime = 103 minutes
| country = India
| language = Hindi
| budget =
}}
Chase is a 2010 Bollywood action film directed by Jagmohan Mundhra, who has previously directed films such as Shoot on Sight and Provoked (film)|Provoked. The film stars Anuj Saxena, Udita Goswami, Sameer Kochhar and Tareena Patel in the lead roles, while Gulshan Grover makes a special appearance.  The film was released on 30 April 2010.   

==Cast==
* Anuj Saxena as Sohail Ansari
* Udita Goswami as Nupur Pradhan
* Sameer Kochhar as Inspector Siddharth
* Aditya Raj Kapoor as Mr. Khanna
* Ashok Banthia as Vishwajeet Rana - Udyog Mantri
* Tareena Patel as  Surbhee
* Rajesh Khattar as Ranveer Tyagi
* Geeta Khanna as Rosy Aunty
* Gulshan Grover as Anthony DCosta (Special appearance)
* Shweta Menon (Special appearance)

==Plot==
 
The main players in Chase to get to the "truth" are Sohail Ansari, a man on the run. DIG Ranveer Tyagi who has vested interest in getting to the bottom of a murder in which Sohail is involved. Insp. Siddharth, right hand of the DIG who is monitoring Sohails actions. In this scenario enters Nupur Chauhan who is Involved with Sohail closely. The other players of this Chase are Surabhee, Sohails love interest and industrialist Mr. Khanna to whom money is the solution of all problems. Added to this there is Anthony DCosta who is also in search for the truth. The hunt for Sohail and search for truth takes us through the streets of Mumbai in an exciting and thrilling Chase. The truth has the potential of blowing up the entire political system apart. The line between truth and lies gets blurred. Who is the victim and who is the criminal? What you see may not be the truth. Do the criminals get caught? Does the victim come up the winner? Who is Sohail Ansari and what is the role of Anthony DCosta? This Chase for the truth ends in nail biting, tense and unusual climax which will leave you spell bound. It is said that you are innocent till proven guilty, but what happens when the law is interested in punishing the accused without verifying his innocence or guilt? What happens when the law upholder are ruthless and have no scruples?

==Box office==
Upon release, the film failed miserably at the box office and could only collect a total net gross of Rs.   during its first two weeks. It was declared a disaster by the box office India. 

==References==
 

==External links==
*  

 
 
 
 


 