Mag-ingat Ka Sa... Kulam
{{Infobox film
| name             = Mag-ingat Ka Sa... Kulam
| image            = 
| caption          = 
| director         = Jun Lana
| producer         = Lily Monteverde Roselle Monteverde-Teo Mhalouh Crisologo
| writer           = Jun Lana Elmer L. Gatchalian Renato Custodio Jr.
| starring         =  
| cinematography   = Mo Zee
| editing          = Renewin Alano Ria De Guzman Mikael Angelo Pestaño	 
| distributor      = Regal Films Regal Multimedia
| released         =  
| runtime          = 
| country          = Philippines
| language         =  
| budget           =
| gross            = P87,490,918
}}
 Filipino horror film starring Judy Ann Santos and Dennis Trillo. It is produced and released by Regal Films as part of their year-long 48th anniversary celebration. The film was released domestically on October 1, 2008. 
The film is about a married woman who starts behaving unpredictably as she recovers from a terrible car accident. 

==Plot==
Ever since Mira (Judy Ann Santos) woke up from a car accident, she started seeing things that she could not explain. She felt paranormal activity is occurring inside their house. Although she could not remember anything that happened before the accident, Dave came to her house and explained to her that she was supposed to leave her husband, Paul and elope with him. Dave told her that she loved him and she trusted a secret to him. Mira learned that Paul had another sexmate before which caused their relationship to deteriorate. Even her blind child, Sophie seemed aloof and is scared of her. She started to patch things up between her and her family but she could not forget what Dave told her about trusting him her secret.
Mira called up Dave to meet him and Dave explained to her the secret she seemed to have forgotten already because of the accident. Dave told her how she had a twin sister named Maria that she left in a mental institute because she had been insane since their mothers death. Mira and Dave went to the mental institute to see Maria but the doctor said that Maria had died a month back and since no one claimed her body, they took the corpse to a University for study. She told Paul about this which surprised him, all along not knowing that Mira ever had a twin sister. Paul called the university to claim the corpse and then had it cremated.

Mira and Pauls daughter, Sophie, underwent an eye transplant. Sophie was so excited to see the eclipse as this is her first time to see again since she got blind. The moment she opened her eyes, she saw a woman standing behind her mother and the people around were wondering who she was seeing since no other people were there.

Paul and Mira could no longer take the paranormal activity so they decided to consult a medium to help them. The old man told Mira and Paul that a powerful black magic is due to acquire a physical body on the eclipse when all spirits become more powerful. The medium found a doll which he called "antigua", a powerful device that the bad spirit used to let itself remain in this world.
On the night of the eclipse, Sophie and Paul were watching the phenomenon when Paul caught a glimpse of something white approaching them. It was Marias body. He took Sophie inside the house and in her room, he found a tape which when he played in Sophies video cam recorder, he saw it was recorded by Mira for him. The video showed Mira videotaping herself before the car accident she went through. Mira was saying in the tape how Paul could fight Maria. In the room where Mira was, her body became that of Maria. The twist was then revealed. It was Maria who came back from the car accident and it was Mira who died in the asylum and got cremated. Mira had been dead all along and it was Maria who used her body to come back to the world and take vengeance against Mira through her family.

Mira recalled that their mother was a mangkukulam and tried to transfer her powers to the twins when they were young. Being the braver one, Mira tried to contradict their mothers chants as she did not want to learn about her evil doings and voodoo magic. When the twin grew up, Mira decided to run away from home, leaving Maria behind. Their mother died soon after. Mira attended the burial and realized that Maria had gone insane, claiming she could talk to things that Mira could not see. Mira then told her twin that she would bring her with her to Manila. It turned out though that Mira sent her to a mental institute. Maria was left heartbroken. One night, she stayed up late and did a powerful black magic so that her and Miras body could switch. The chant was effective although when Marias soul transferred to Miras body who was on the way to the mental asylum to stop Maria from doing any harm to her family, the car got into a horrible accident. Maria, who woke up in Miras body could no longer remember anything while Mira who found herself in Marias body woke up from the trance already in the asylum.

In the present, Paul replayed the video tape that Mira left for him. Mira said that by the time that Paul got to watch the video, she would have been dead. Paul needed to trap Maria into a circle made of Miras bodys dust. Paul did this but Maria discovered the trick. She was pushed though by Sophie from behind and there, she was trapped in the circle of dust. Paul stabbed the "antigua" with a piece of wood and Marias soul burned and disappeared. Miras soul showed up to the father and daughter to bid her final farewell. It is hinted at the end that Miras Daughter Sophie saw the book that is used in "kulam"

==Cast==
*Judy Ann Santos as Mira/Maria
*Dennis Trillo as Paul
*TJ Trinidad as Dave
*Sharlene San Pedro as Sophie
*Kris Bernal as Maggie
*Mart Escudero as Neil
*Ces Quesada as Design
*Cris Daluz as Albularyo

==Reception==

===Critical response===
The film received an "A" rating from the Cinema Evaluation Board. 
Film critics praised Judy Ann Santoss performance in this film.

===Box Office===
The film was a box office success. The film grossed P43.3 million on its opening weekend.
The film grossed P87.4 million on its entire theatrical run. 

==Awards and nominations==

* 6th Golden Screen Awards  
*: Best Visual Effects: Roadrunner Network, Inc.

* 25th PMPC Star Awards for Movies  
*: Movie of the Year
*: Movie Director of the Year: Jun Lana
*: Movie Actress of the Year: Judy Ann Santos
*: Movie Child Performer of the Year: Sharlene San Pedro
*: Original Movie Screenplay of the Year: Jun Lana, Elmer Gatchalian and Renato Custodio
*: Movie Cinematographer of the Year: Moises Zee
*: Movie Editor of the Year: Ria De Guzman, Renewin Alano & Mikael Angelo Pestano
*: Movie Production Designer of the Year: Mario Lipit & Edgar Martin Littaua
*: Movie Sound Engineer of the Year: Bebet Casas

* 11th Gawad Pasado Awards  
*: Pinakapasadong Editing (Best Editing): Ria de Guzman, Renewin Alano at Angelo Pestano
*: Pinakapasadong Tunog (Best Sound): Bebet Casas

* 27th FAP Luna Awards  
*: Best Musical Scoring: Von de Guzman

==References==
 

==External links==
*  

 
 
 
 
 
 
 