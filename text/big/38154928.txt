Veera (2013 film)
{{Infobox film
| name           = Veera
| image          = 2013 Kannada film Veera poster.JPG
| caption        = Film poster
| director       = Ayyappa P. Sharma
| producer       = Ramu
| writer         = Anil Kumar
| screenplay     = Ayyappa P. Sharma
| starring       = Malashri Komal Kumar Rahul Dev Ashish Vidyarthi
| music          = Hamsalekha
| cinematography = Rajesh Khatta
| editing        = Lakshman Reddy
| studio         = Ramu Enterprises
| distributor    = 
| released       =  
| runtime        = 132 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 action film directed by Ayyappa P. Sharma and starring Malashri, Rahul Dev and Ashish Vidyarthi in the lead roles. Produced by Ramu Enterprises, the films soundtrack and lyrics are written by Hamsalekha. The supporting cast features C. R. Simha, Komal, Mukul Dev and Raju Talikote.

== Cast ==
* Malashri as Veeralakshmi "Veera"
* Ashish Vidyarthi as Police commissioner Garudachar
* Komal
* Rahul Dev as RD
* Mukul Dev
* Raju Talikote
* C. R. Simha as Home Minister Chunchanagatti
* M. N. Lakshmi Devi
* Anantha Velu
* Sathyadev

==Production==

===Development===
Veera was announced in December 2009, with Malashri returning after a six-month hiatus, following her previous release, Kannadada Kiran Bedi. It was announced that Veera would be produced by her husband Ramu, under his banner Ramu Enterprises. Reports said filming would begin in the first week of December. 

===Casting===
However, the film went under production only in July 2012, following the signing of Ashish Vidyarthi and Komal Kumar to play pivotal roles.  C. R. Simha was signed to play a supporting role in the film, who returned to acting after a sabbatical. Sathyadev was cast to play one of the negative leads. 

===Filming=== dubbing was done at the Akash Studio and Hamsalekha Studio in Bangalore. 

==Soundtrack==
{{Infobox album
| Name        = Veera
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Anand Audio
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Hamsalekha composed the films background score and music for the soundtracks who also penned its lyrics. The album consists of six tracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Dosthi Endare Dhokha
| lyrics1 = Hamsalekha
| extra1 = Hemanth
| length1 = 
| title2 = Indiyana
| lyrics2 = Hamsalekha
| extra2 = Vijay Yesudas
| length2 = 
| title3 = Kabadiya
| lyrics3 = Hamsalekha
| extra3 = Raksha
| length3 = 
| title4 = Mahalu
| lyrics4 = Hamsalekha
| extra4 = Raghu Dixit
| length4 = 
| title5 = Veera
| lyrics5 = Hamsalekha
| extra5 = Chandan
| length5 = 
| title6 = Asha Kirana
| lyrics6 = Hamsalekha
| extra6 = Badri Prasad
| length6 = 
}}

==Release and reception==
The film was given the "U/A" (Parental Guidance) certificate by the Regional Censor Board in early-March 2013. The film released theatrically on 29 March 2013, in over 200 prints across Karnataka, the highest for a Malashri film. 

Upon release, it opened to mixed response from critics. G. S. Kumar of The Times of India rated the film 3.5/5 and wrote, "Armed with a good script, director Aiyyappa P Sharma has churned out a commercial movie packed with action." He concluded writing, "Malashri has given a neat performance. Rahu Dev, Mukul Dev, Ashish Vidyarthi, CR Simha have done justice to their roles. Komal gives an excellent comic touch. Hamsalekha has contributed a couple of catchy tunes. Rajesh Kata shines with his cinematography."  Muralidhara Khajane of The Hindu felt that Malashri who "has been creating a niche for herself with action roles" doesnt disappoint with Veera.  Y. Maheswara Reddy of Daily News and Analysis|DNA reviewed the film giving it a score of 35% and wrote, "Veera for all its hype disappoints big time. In fact, going by the number of action scenes, dialogues and the pedestrian screenplay, it would seem that director Ayyappa P Prasad has taken the Kannada film-viewing audience for granted." He added, "the film also lacks any semblance of romance or humour." 

==References==
 

 
 
 
 
 
 
 
 