Guys and Dolls (film)
{{Infobox film
| name           = Guys and Dolls 
| image          = Guys and dolls movieposter.jpg
| image size     = 
| alt            = 
| caption        = theatrical poster 
| director       = Joseph L. Mankiewicz
| producer       = Samuel Goldwyn
| writer         = 
| screenplay     = Joseph L. Mankiewicz Ben Hecht
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Frank Loesser
| cinematography = Harry Stradling 
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 150 minutes
| country        = United States
| language       = English
| budget         = $5.5 million
| gross          = $20,000,000
}}
 1950 Broadway musical by composer and lyricist Frank Loesser, with a book by Jo Swerling and Abe Burrows based on "The Idyll Of Miss Sarah Brown" and "Blood Pressure", two short stories by Damon Runyon.  Dances were choreographed by Michael Kidd, who had also staged the dances for the Broadway production.

At Samuel Goldwyn and Joseph L. Mankiewiczs request, Frank Loesser wrote three new songs for the film: "Pet Me Poppa", "(Your Eyes Are the Eyes of) A Woman in Love", and "Adelaide", the last written specifically for Sinatra. Five songs in the stage musical were omitted from the movie: "A Bushel and a Peck", "My Time of Day" (although these are heard instrumentally as background music), "Ive Never Been In Love Before", "More I Cannot Wish You"  and "Marry the Man Today".

==Plot== stage and movie versions, New York criminals and gamblers in the late 1940s.
 crap game Robert Keith), are "putting on the heat". All the places where Nathan usually holds his games refuse him entry due to Brannigans intimidating pressure. The owner of the Biltmore garage does agree to host the game provided Nathan pays him $1000 in cash in advance. The garage owner will not even accept a "marker" or IOU (debt)|IOU, he insists on having the money itself. Adding to Nathans problems, his fiancée, Miss Adelaide (Vivian Blaine), a nightclub singer, wants to bring an end to their 14-year engagement and actually tie the knot. She also wants him to go straight, but organizing illegal gambling is the only thing hes good at.
 Mission (based on the Salvation Army) which opposes gambling.
 Broadway branch drunks or gamblers have come in to confess or reform. To approach Sarah, Sky pretends that he is a gambler who wants to change. Sarah sees how expensively dressed he is and she is suspicious: "Its just so unusual for a successful sinner to be unhappy about sin."

Seeing that the Mission is and has been empty and unsuccessful, "a store full of repentance and no customers", Sky suggests a bargain. He will get a dozen sinners into the Mission for her Thursday night meeting in return for her having dinner with him in Havana. With General Matilda Cartwright (Kathryn Givney) threatening to close the Broadway branch for lack of participation, Sarah has little choice left, and agrees to the date.

Meanwhile, confident that he will win his bet with Sky, Nathan has gathered together all the gamblers, including a visitor that tough-guy Harry the Horse ( ), a Chicago mobster. When Lieutenant Brannigan appears and notices this gathering of "senior delinquents", all gathered at Mindys, Nathans sidekick, Benny Southstreet (Johnny Silver), covers it up by claiming that they are celebrating the fact that Nathan is getting married to Adelaide. Nathan is shocked by this, but is forced to play along. Later, when he notices the Save a Soul Mission band passing by and sees that Sarah is not among them, he collapses on the realization that he has lost his bet with Sky. He has no money and nowhere to house the crap game, and, since Adelaide was present at the "wedding announcement" Benny Southstreet dreamed up, he is now apparently committed to actually marrying Adelaide. He does love Adelaide, but is uneasy about going straight, either maritally or lawfully.

Over the course of their short stay in Cuba, Sky manages to break down Sarahs social inhibitions, partly through disguised alcoholic drinks (several glasses of milk with Bacardi added "as a preservative"), and they begin to fall in love with one another. He even confesses that the whole date was part of a bet, but she forgives him as she realizes that his love for her is sincere.

They return to Broadway at dawn and meet the Save a Soul Mission band which, on Skys advice, has been parading all night. At that moment police sirens can be heard, and before they know it the gamblers led by Nathan Detroit are hurrying out of a back room of the Mission, where they took advantage of the empty premises to hold the crap game.

The police arrive too late to make any arrests, but Lieutenant Brannigan finds the absence of Sarah and the other Save a Soul members too convenient to have been a coincidence. He implies that it was all Skys doing: "Masterson, I had you in my big-time book. Now I suppose Ill have to reclassify you &mdash; under shills and decoys". Sarah is equally suspicious that Sky has had something to do with organizing the crap game at the Mission and she angrily takes her leave of him, refusing to accept his denials.

But Sky still has to make good his arrangement with Sarah to provide sinners to the Mission. Sarah would rather forget the whole thing, but Uncle Arvide Abernathy (Regis Toomey), who acts as a kind of father figure to her, warns Sky that "If you dont make that marker good, Im going to buzz it all over town youre a welcher."

Nathan has continued the crap game in a sanitary sewer|sewer. With his revolver visible in its shoulder holster, Big Jule, who has lost all his money, forces Nathan to play against him while he cheats, cleaning Nathan out. Sky enters and knocks Big Jule down and removes his pistol. Sky, who has been stung and devastated by Sarahs rejection, lies to Nathan that he lost the bet about taking her to Havana, and pays Nathan the $1000. Nathan tells Big Jule he now has money to play him again, but Harry the Horse says that Big Jule cant play without cheating because "he cannot make a pass to save his soul". Sky overhears this, and the phrasing inspires him to make a bold bet: He will roll the dice, and if he loses he will give all the other gamblers $1000 each; if he wins they are all to attend a prayer meeting at the Mission.

The Mission is near to closing when suddenly the gamblers come parading in, taking up most of the room. Sky won the roll. They grudgingly confess their sins, though they show little sign of repentance: "Well ... I was always a bad guy. I was even a bad gambler. I would like to be a good guy and a good gambler. I thank you." Even Big Jule declares: "I used to be bad when I was a kid. But ever since then Ive gone straight, as I can prove by my record &mdash; 33 arrests and no convictions." Nicely-Nicely Johnson (Stubby Kaye) however, recalling a dream he had the night before, seems to have an authentic connection to the Missions aim, and this satisfies everyone.

When Nathan tells Sarah that Sky lost the Cuba bet, which she knows he won, she hurries off in order to make up with him.

It all ends with a double wedding in the middle of Times Square, with Sky marrying Sarah, and Nathan marrying Adelaide, who is given away by Lieutenant Brannigan. Arvide Abernathy performs the dual ceremony. Nicely-Nicely has joined the Save a Soul Mission, and he and Sarahs assistant are sweet on each other. As the film closes, the two newlywed couples are escorted from the wedding to their respective love nests inside police cars, with lights festively flashing and sirens blaring.

==Cast==
*Marlon Brando as Sky Masterson
*Jean Simmons as Sister Sarah Brown
*Frank Sinatra as Nathan Detroit
*Vivian Blaine as Miss Adelaide
*Stubby Kaye as Nicely-Nicely Johnson Robert Keith as Lieutenant Brannigan
*Sheldon Leonard as Harry the Horse
*Regis Toomey as Arvide Abernathy
*B.S. Pully as Big Jule
*Johnny Silver as Benny Southstreet The Goldwyn Pat Sheehan and Larri Thomas.   

==Production==
 ;  both roles went to Brando.

Since Betty Grable was not available to play Miss Adelaide, Goldwyn cast Vivian Blaine, who had originated the role onstage.  Marilyn Monroe had wanted the part of Adelaide but a telephone request from her did not influence Joe Mankiewicz, who wanted Blaine from the original production. 

Goldwyn wanted Grace Kelly for Sarah Brown, the Save-a-Soul sister. When she turned the part down because of other commitments, Goldwyn tried Deborah Kerr, who was also unavailable. The third choice was Jean Simmons who had recently played opposite Brando in Désirée (film)|Désirée. Goldwyn was surprised by Simmons sweet voice and strong acting and ultimately believed the love story worked better in the film than onstage. Im so happy he said after seeing the rushes one day that I couldnt get Grace Kelly.  Director Joe Mankiewicz called Simmons "the dream ... a fantastically talented and enormously underestimated girl.  In terms of talent, Jean Simmons is so many heads and shoulders above most of her contemporaries, one wonders why she didnt become the great star she could have been."  

The musical numbers performed by Jean Simmons and Marlon Brando were sung by the actors themselves, without dubbing by professional singers.         

Stubby Kaye, B.S. Pully, and Johnny Silver all repeated their Broadway roles in the film.

==Awards and honors==
* Academy Awards   
** Nominated for  , Joseph C. Wright, Howard Bristol; 
**Nominated for   
**Nominated for   
**Nominated for  .
* BAFTA Awards
** Nominated for Best Film from any Source
** Nominated for Best Foreign Actress: Jean Simmons
* Golden Globe Awards Best Motion Picture - Musical/Comedy Best Motion Picture Actress - Musical/Comedy: Jean Simmons
 AFI ranked list of best musicals.

==Reception==
Guys and Dolls opened on November 3, 1955 to mostly good reviews. Review aggregator Rotten Tomatoes reports that 90% out of 29 critics have given the film a positive review, with a rating average of 7.6/10.  Casting Marlon Brando has long been somewhat controversial, although Variety (magazine)|Variety wrote "The casting is good all the way."  This was the only Samuel Goldwyn film released through MGM.  With an estimated budget of over $5 million, it garnered rentals in excess of $13 million.  Variety (magazine)|Variety ranked it as the #1 moneymaking film of 1956;  when a film is released late in a calendar year (October to December), its income is reported in the following years compendium, unless the film made a particularly fast impact. Guys and Dolls went on to gross $1.1 million in the UK, $1 million in Japan, and over $20 million globally. 

According to MGM records the film earned $6,801,000 in the U.S. and Canada and $2,262,000 elsewhere, resulting in a total of $9,063,000.  . 

==References==
Notes
 

==External links==
* 
* 
* 
* 
*  

 

{{Navboxes
|title=Articles and topics related to Guys and Dolls (film)
|state=collapsed
|list1=
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 