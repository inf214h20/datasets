Fatty and the Broadway Stars
 
{{Infobox film
| name           = Fatty and the Broadway Stars
| image          = Fatty and the Broadway Stars - 1916 - newspaper.jpg
| caption        = Scene from the film. Fatty Arbuckle
| producer       = Mack Sennett
| writer         = 
| narrator       =  Fatty Arbuckle
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 20 minutes
| country        = United States  Silent English English intertitles
| budget         = 
}}
 short comedy Fatty Arbuckle.

==Cast== Roscoe Fatty Arbuckle
* Ivy Crosthwaite
* Mack Sennett
* Joe Weber
* Lew Fields
* Sam Bernard
* William Collier Sr.
* Joe Jackson
* Brett Clark
* Harry Booker
* Mae Busch - Actress
* Glen Cavender - Actor
* Chester Conklin
* Alice Davenport
* Minta Durfee - Actress
* Lewis Hippe - (as Lew Hippe)
* Tom Kennedy
* Fred Mace - Actor
* Hank Mann
* Polly Moran Charles Murray Al St. John
* Slim Summerville
* Mack Swain
* Wayland Trask
* Bobby Vernon - Actor
* Harry Gribbon
* Edgar Kennedy
* Keystone Kops - Police Force
* Ford Sterling

==See also==
* List of American films of 1915
* Fatty Arbuckle filmography

==External links==
* 

 
 
 
 
 
 
 
 

 