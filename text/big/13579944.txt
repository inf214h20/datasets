Maha-Sangram
{{Infobox film
| name           = Maha-Sangram
| image          = Maha-Sangram.jpg
| image_size     =
| caption        = Promotional Poster
| director       = Mukul Anand
| producer       = Nitin Manmohan
| writer         = 
| narrator       =  Govinda
| music          = Anand-Milind
| cinematography = 
| editing        = 
| distributor    = Neha Arts Pvt.Ltd
| released       = 12 January 1990 
| runtime        = 149 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
|}} 1990 Bollywood Govinda and Madhuri Dixit.

==Plot==
Uttar Pradesh-based Vishal gets a telegram from Santa Cruz Police Station that his younger collegian brother, Arjun, is dead. Distraught he travels to Bombay, collects his brothers ashes, and finds out that Arjun met a violent death. With the aid of a street-smart con-woman and her mentor, Babu Kasai Hyderabadi, he then sets out to find who killed his brother - not knowing that soon he will be drawn into the dark world of Godha and Vishwaraj.

==Cast==
*Vinod Khanna as Vishal
*Madhuri Dixit as Jhumri    Govinda as Arjun aka Munna 
*Shaheen as Pooja
*Gulshan Grover as Police Officer  
*Shakti Kapoor  as Babu (Kasai)Hydrabadi 
*Amjad Khan as Bada Godha
*Aditya Pancholi as Suraj aka Chota Godha 
*Sonu Walia as Neelam
*Sumeet Saigal as Prakash
*Kiran Kumar as Vishwa
*Ali Khan as Pakya
*Shammi as Mary

==Tracks list==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "I Love You"
| Alka Yagnik
|- 
| 2
| "Do Dooni Chaar"
| Amit Kumar, Anuradha Paudwal
|-
| 3
| "Aa Bahon Mein Aa"
| Amit Kumar, Anuradha Paudwal
|-
| 4
| "Chhod Ke Tujh Ko"
| Mohammad Aziz, Suresh Wadkar
|-
| 5
| "Aaya Main Aaya"
| Udit Narayan, Amit Kumar
|-
| 6
| "Dhak Dhak Dhak"
| Aditya Pancholi, Alisha Chinai
|}

== External links ==
*  

 
 
 
 


 
 