Daybreak (1931 film)
{{Infobox film
| name           = Daybreak
| image          = 
| alt            = 
| caption        =
| director       = Jacques Feyder
| producer       = 
| screenplay     = Cyril Hume Ruth Cummings
| based on       =  
| starring       = Ramon Novarro Helen Chandler Jean Hersholt C. Aubrey Smith William Bakewell Karen Morley
| music          = 
| cinematography = Merritt B. Gerstad
| editing        = Tom Held
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Daybreak is a 1931 American drama film directed by Jacques Feyder and written by Cyril Hume and Ruth Cummings. The film stars Ramon Novarro, Helen Chandler, Jean Hersholt, C. Aubrey Smith, William Bakewell and Karen Morley. The film was released on May 2, 1931, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==	 
*Ramon Novarro as Willi
*Helen Chandler as Laura
*Jean Hersholt as Herr Schnabel
*C. Aubrey Smith as General von Hertz
*William Bakewell as Otto
*Karen Morley as Emily Kessner
*Douglass Montgomery as Von Lear 
*Glenn Tryon as Franz Clyde Cook as Josef
*Sumner Getchell as Emil
*Clara Blandick as Frau Hoffman
*Edwin Maxwell as Herr Hoffman
*Jackie Searl as August 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 