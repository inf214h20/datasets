Kulavadhu
{{Infobox film 
| name           = Kulavadhu
| image          =  
| caption        = 
| director       = T. V. Singh Takur
| producer       = A C Narasimha Murthy &amp; Friends
| writer         = Krishnamurthy Puranika (Based on Novel Kulavadhu)
| screenplay     = Shilashree Sahithya Vrunda Rajkumar Balakrishna Balakrishna Narasimharaju Narasimharaju K. S. Ashwath
| music          = G. K. Venkatesh
| cinematography = B Dorairaj
| editing        = Venkatram Raghupathi
| studio         = Shilashree Productions
| distributor    = Shilashree Productions
| released       =  
| runtime        = 132 min
| country        = India Kannada
}}
 1963 Cinema Indian Kannada Kannada film, Narasimharaju and K. S. Ashwath in lead roles. The film had musical score by G. K. Venkatesh famous song "Yuga yugadi kaledaru" is a still hit song from this movie.  

==Cast==
  Rajkumar
*Balakrishna Balakrishna
*Narasimharaju Narasimharaju
*K. S. Ashwath
*Venkatasubbaiah
*Ashwath Narayana
*Aadishesh
*Shenoy
*Kuppuraj
*Raman
*Bhujanga Rao
*Basappa
*Master Udayashankar Leelavathi
*B. Jayashree
*Papamma
*Sharadamma
*Sadhana (Revathidevi)
 

==Soundtrack==
The music was composed by GK. Venkatesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Seetharamayya || 03.44
|- Janaki || M. Govinda Pai || 03.12
|-
| 3 || Bangaradodave Beke || PB. Srinivas, Latha || GV. Iyer || 01.49
|-
| 4 || Hagalu Irulu || PB. Srinivas || GV. Iyer || 01.28
|- Janaki || GV. Iyer || 03.23
|-
| 6 || Kannadada Makkalella || GK. Venkatesh || GV. Iyer || 03.16
|-
| 7 || Ninagidu Nyayave || PB. Srinivas || GV. Iyer || 01.26
|-
| 8 || Sigadhanna || PB. Srinivas || GV. Iyer || 01.38
|-
| 9 || Yedavidare Naa || S. Janaki|Janaki, Rajeshwari || GV. Iyer || 04.49
|- Janaki || DR. Bendre || 02.45
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 