Sgt. Bilko
 
{{Infobox film
| name=Sgt. Bilko 
| image=Sgt bilko poster.jpg 
| caption = Theatrical release poster 
| writer=Nat Hiken (premise)   Andy Breckman  (screenplay)
| starring=Steve Martin   Dan Aykroyd   Phil Hartman 
| director=Jonathan Lynn 
| music=Alan Silvestri 
| cinematography=Peter Sova
| editing=Tony Lombardo
| studio= Imagine Entertainment	 
| distributor= Universal Pictures
| released=  
| runtime=94 minutes
| country = United States|
| language=English 
| movie_series=
| awards=|
| producer=Brian Grazer 
| budget= $39 million 
| gross= $37,956,793
}}

Sgt. Bilko is a 1996 American comedy film directed by Jonathan Lynn and written by Andy Breckman.  It is an adaptation of the iconic 1950s television series The Phil Silvers Show, often informally called Sgt. Bilko, or simply Bilko, and stars Steve Martin, Dan Aykroyd and Phil Hartman.

==Plot==
Master Sergeant Ernest G. Bilko is in charge of the motor pool at Fort Baxter, a small United States Army base that develops new military technology. Exploiting this position, he directs a number of scams, ranging from gambling to renting out military vehicles. His commanding officer, Colonel John Hall, overlooks Bilkos money-making schemes, as he is more concerned with problems in the hovertank that the base is designing.

Major Colin Thorn, an officer from the U.S. Army Inspector Generals office, arrives at the camp and begins to scrutinize Bilkos record. Officially, Thorn is at Fort Baxter to conduct a general inspection and determine if the base should remain open in light of recent defense cutbacks. He is also determined to get revenge on Bilko to settle an old score the two have from Fort Dix, where Thorn was nearly court-martialled after a fixed boxing match resulted in Thorn being shipped to Greenland.

Bitter and unprincipled, Thorn is not above breaking the law to ruin Bilko. He attempts to steal Bilkos long-time fiancée Rita, whom Bilko has stood up at the altar more than a dozen times. Rita is tired of waiting and gives Bilko 30 days to win her back or lose her for good.

Bilko, with the help of newly assigned Private First Class Wally Holbrook, devises a means of avoiding Thorns attempt to transfer him to Greenland:  He rigs a demonstration of the bases malfunctioning hovertank, staged before a four-star general and numerous dignitaries. Since Thorn had deliberately tried to sabotage the tank the previous night, he confronts Bilko, Hall, and the general, loudly insulting Bilko and Hall. While ranting he confesses to sabotaging the hovertank. Thorn is sent off again to Greenland.
 favorite songs. Looking out, she sees Bilko and his platoon. Bilko asks Rita to marry him, and she accepts.

==Cast==
*Steve Martin as Master Sergeant Ernest G. Bilko
*Dan Aykroyd as Col. John T. Hall
*Phil Hartman as Maj. Colin Thorn
*Glenne Headly as Rita Robbins
*John Marshall Jones as Sgt. Henshaw
*Pamela Adlon as Sgt. Raquel Barbella
*Austin Pendleton as Maj. Ebersole
*Chris Rock as 1st Lt. Oster
*Cathy Silvers as 1st Lt. Monday Steve Park as Capt. Moon
*Debra Jo Rupp as Mrs. Hall
*Richard Herd as Gen. Tennyson
*Dan Ferro as Spc. Tony Morales
*John Ortiz as Spc. Luis Clemente
*Max Casella as Spc. Dino Paparelli
*Daryl Mitchell as Pfc. Walter "Wally" T. Holbrook
*Mitchell Whitfield as Pfc. Mickey Zimmerman
*Brian Leckner as Pfc. Sam Fender
*Eric Edwards as Pvt. Duane Doberman
*Charles C. Stevenson as Minister
*Travis Tritt as himself
*Steve Kehela as Master Sergeant Stan Sowicki

== Production ==
Both Albert Brooks and Michael Keaton turned down the role of Bilko, and Robin Williams and Billy Crystal expressed interest in it before the role finally went to Martin. 

== Reception ==
The film received mixed to negative reviews from critics,    and has a rotten rating of 32% at review aggregator Rotten Tomatoes.  It also failed at the box office.

==References==
  

==External links==
* 
* 
*  at Rotten Tomatoes

 
 

 
 
 
 
 
 
 
 
 
 
 
 