Pinocchio (2012 film)
{{Infobox film
 | name = Pinocchio
 | image = Pinocchio (2012 film).jpg
 | caption =
 | director = Enzo DAlò
 | writer = Carlo Collodi (novel) Enzo DAlò
 | starring =  
 | music =   Lucio Dalla
 | based on       =  
 | cinematography =  
 | editing =    Gianluca Cristofari
 | released =  
| country        = Italy France Belgium Luxemburg
| language       = Italian
| runtime = 84 min
 }}
Pinocchio is a 2012 Italian animation film directed by Enzo DAlò. It is based on the novel The Adventures of Pinocchio by Carlo Collodi.   

The film had a budget of about 8 million euros.    
It was screened out of competition at the 70th Venice International Film Festival.   
 Paolo Ruffini (Candlewick (Pinocchio character)|Candlewick), Andy Luotto and the same Lucio Dalla. 

==Plot==
In a small village in Tuscany, the poor carpenter Geppetto decides to fabricate a wooden puppet with socket because he feels himself alone. The puppet is called Pinocchio, and that magically comes to life and begins to make jokes of all kinds to Geppetto and the villagers. Soon Pinocchio escapes, although his father wants him to go to study in school like a normal child; the puppet is captured by the puppeter Mangiafoco, who wants to roast him, but Pinocchio moves him, and manages to save his life. Pinocchio runs into other adventures, encountering swindlers: the Cat and the Fox, then the good Fairy, who protects him, and  a the end Lucignolo, a rascal who leads Pinocchio in the fantastic Wonderland.

==References==
 
 
==External links==
* 

 
 
 
 
 
 
 
  
 
 
 
 
 

 
 