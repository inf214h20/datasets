Big Rig (film)
{{Infobox film
| name           = Big Rig
| image          = bigrigdvd.jpg
| caption        = DVD release cover
| director       = Doug Pray
| producer       = Brad Blondheim
| writer         = Doug Pray Brad Blondheim
| starring       =
| music          =
| cinematography = Doug Pray
| editing        = Doug Pray
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
}}

Big Rig is a 2007 documentary film by Doug Pray about long-haul truck drivers. The film consists of a series of interviews with different drivers, focusing on both their personal life stories and also the life and culture of truck drivers in the United States.

==Production==
The film was shot with a crew of three people: director and cameraman Doug Pray, producer Brad Blondheim, and lighting, sound, and general production assistant Jim Dziura. http://bigrigmovie.com/bigrig/INFO5-about.html, official production notes 

Filming took place over four different two-week roadtrips.  The crew of three did not schedule interviews with truckers: they would drive into a truck stop in their RV and approach the truckers for interviews.  If the driver agreed, Pray would ride as the passenger and interview the driver for the day, while Blondheim followed in the RV. Most of the interviewees were very skeptical of the crew at first, and the crew was repeatedly thrown out of truck stops for soliciting interviews, which is typically not allowed. 

Director Pray and producer Blondheim became interested in a trucker documentary after a series of road trips to various concerts during the filming of Prays Scratch (2001 film)|Scratch. Doug Pray, Brad Blondheim, audio commentary on Big Rig DVD  In 2001, Pray and Blondheim shot and edited a 10-minute film as a proof of concept for the films production process. 

For the films music, producer Blondheim was inspired by the roots music featured in the film O Brother, Where Art Thou?.  One of the music producers from Scratch (2001 film)|Scratch interested them in the music of Buck 65. The final cut of the film uses several contributions from Buck 65; about one third of this was composed specifically for the film. 

The film was shot on a Panasonic Varicam and edited by the director using Final Cut Pro. 

In July 2012 it was announced by several in the truck driving community and by a source inside Ocule Films that there will be a sequel to the film, No time frame for when filming or production will begin on the sequel.

==Reception==
The film was an official selection of the Seattle International Film Festival and AFI Fest, and was featured at a handful of other small film festivals.   It was released on DVD in late 2008.
The film was and remains a big hit with the trucking community and truck drivers. 

==References==
 

==External links==
* 
*  at the Big Rig official site

 
 
 
 
 
 
 