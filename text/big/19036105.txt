Fame Is the Spur (film)
{{Infobox Film
| name           = Fame is the Spur
| image          = "Fame_Is_the_Spur"_(1947).jpg
| image_size     = 
| caption        = 
| director       = Roy Boulting
| producer       = John Boulting
| music          = John Wooldridge
| cinematography = Günther Krampf  Richard Best
| writer         = Nigel Balchin   Howard Spring (novel)
| narrator       = 
| starring       = Michael Redgrave Rosamund John Bernard Miles  David Tomlinson
| distributor    = General Film Distributors
| released       =  
| runtime        = 116 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Fame Is Labour Party politician Ramsay MacDonald. 

==Plot== Labour Member of parliament|M.P., but is seduced by the trappings of power, and finds himself the type of politician he originally despised.

==Cast==
* Michael Redgrave - Hamer Radshaw
* Rosamund John - Ann
* Bernard Miles - Tom Hannaway
* Carla Lehmann - Lady Lettice
* Hugh Burden - Arnold Ryerson
* Marjorie Fielding - Aunt Lizzie
* Seymour Hicks - Old Buck
* Tony Wager - Hamer as a boy
* Brian Weske - Ryerson as a boy
* Gerald Fox - Hannaway as a boy
* Jean Shepeard - Mrs Radshaw
* Guy Verney - Grandpa
* Percy Walsh - Suddaby
* David Tomlinson - Lord Liskead Charles Wood - Dai
* Milton Rosmer - Magistrate
* Wylie Watson - Pendleton Ronald Adam - Radshaws Doctor 
* Honor Blackman - Emma
* Campbell Cotts - Meeting chairman
* Maurice Denham - Prison doctor
* Kenneth Griffith - Wartime Miners Representative 
* Roddy Hughes - Wartime Miners Spokesman 
* Vi Kaley - Old Woman in Election Crowd 
* Laurence Kitchin - Radshawss secretary
* Philip Ray - Doctor
* Gerald Sim - Reporter
* Harry Terry - Man in Election Crowd
* Iris Vandeleur - Woman Who Opens Front Door
* H Victor Weske - Wartime Miners Representative  Ben Williams - Radical Orator

==Critical reception== Labor and Conservative factions are at loggerheads with each other in Great Britain" ;  while The New York Times wrote, "this John and Roy Boulting film has vivid authority and fascination...But, unfortunately, a full comprehension of the principal character in this tale is missed in the broad and extended panorama of his life that is displayed...Mr. Redgrave is glib and photogenic; he acts the "lost leader" in a handsome style. But he does not bring anything out about him that is not stated arbitrarily";  {{cite web|url=http://www.nytimes.com/movie/review?res=9F02E5DB1E3BE23BBC4053DFB7678382659EDE|title=Movie Review -
  Fame Is the Spur - THE SCREEN IN REVIEW;  Fame Is the Spur, British Film Based on Novel by Spring, Opens at Little CineMet - NYTimes.com|work=nytimes.com}}  while the Radio Times praised Redgraves "powerhouse performance, with his gradual shedding of heartfelt beliefs as vanity replaces commitment having a chillingly convincing ring. But such is Redgraves dominance that theres little room for other characters to develop or for any cogent social agenda."  

==External links==
* 

==References==
 

 
 

 
 
 
 
 
 