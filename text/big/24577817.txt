A Certain Young Man
{{Infobox film
| name           = A Certain Young Man
| image          =
| caption        =
| director       = Hobart Henley Edmund Goulding (uncredited)
| producer       =
| writer         = Donna Barrell Bela Sekely
| based on       =  
| starring       = Ramon Novarro Marceline Day Renée Adorée Carmel Myers Bert Roach
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 58 minutes
| country        = United States Silent English intertitles
| budget         =
}}
A Certain Young Man is a 1928 comedy film directed by Hobart Henley. The film stars Ramon Novarro, Marceline Day, Renée Adorée, Carmel Myers and Bert Roach. The film is considered lost.  

A trailer for the film is preserved at the Library of Congress.

==Synopsis==
An English Lord is over-fond of married women.

==Cast==
* Ramon Novarro - Lord Gerald Brinsley
* Marceline Day - Phyllis
* Renée Adorée - Henriette
* Carmel Myers - Mrs. Crutchley
* Bert Roach - Mr. Crutchley
* Huntley Gordon - Mr. Hammond
* Ernest Wood - Hubert

==Production==
The film was shot in 1926, but was not released until 1928. Sally ONeil was originally cast as Phyllis, but was replaced by Marceline Day.  Joan Crawford, still an ingenue at the time, was considered for that same role, but she was deemed unsuitable.  Cast member Willard Louis died a few months after filming wrapped, and his name was removed from the credits. 

==Release==
A Certain Young Man opened to reviews that were lukewarm at best, and was a financial failure at the box office. Some screenings were preceded by the Technicolor short The Czarinas Secret featuring Sally Rand and Olga Baclanova.  The films release followed on the heels of A Gentleman of Paris with Adolph Menjou, which was based on the same source material and was considered a better adaptation. Novarros performance was unfavorably compared with that of Menjou by several critics,  including Mordaunt Hall of the New York Times, who found the overall film "only mildly amusing and very shallow," though he found Myers "charming" and work by supporting actors Huntley Gordon and Bert Roach "favorable." 

Personally, Novarro hated the film and his performance. In an interview in the April 1931 issue of Modern Screen, he said "In acting, in directing, in everything - I want to be definite. Even when I am later proven wrong - it will at least have been so definite that I myself know it. I am responsible for the worst performance that has ever been given on the screen. It was in A Certain Young Man and it was terrible. I am mortally ashamed of it - yet Id rather have been bad than just fair." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 