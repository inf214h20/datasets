Relentless (1989 film)
{{Infobox Film
| name= Relentless
| image= Relentless_poster.jpg
| caption= Theatrical Release Poster
| writer=Phil Alden Robinson
| starring=Judd Nelson Robert Loggia Leo Rossi Meg Foster
| director=William Lustig
| cinematography=James Lemmo
| music=Jay Chattaway
| distributor=CineTel Films
| released= 
| runtime=92 min.
| country=United States
| language=English
| awards=
| budget=
| gross=$6,985,999 (domestic)
| producer=Paul Hertzberg Lisa M. Hansen Howard W. Koch
}}
 LAPD officers on a hunt for an ex-cop turned serial killer.

Relentless was the first in a series of four films starring Leo Rossi as detective Sam Dietz trying to stop a serial killer. The three sequels were all filmed and released within three consecutive years from 1992 to 1994.

== Plot == Los Angeles phone book and skillfully covering up his tracks by using his skills and knowledge that he learned while on the force. While in pursuit of Taylor, both Dietz and Malloy become his next planned targets for murder.

==Cast==
* Judd Nelson as Arthur "Buck" Taylor
* Robert Loggia as Bill Malloy
* Leo Rossi as Sam Dietz
* Meg Foster as Carol Dietz
* Patrick OBryan as Todd Arthur
* Ken Lerner as Arthur
* Mindy Seeger as Francine
* Angel Tompkins as Carmen
* Beau Starr as Ike Taylor Ron Taylor as Captain Blakely
* Roy Brocksmith as Coroner

== Sequels ==
*  (1992)
*Relentless 3 (1993)
*  (1994)

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 