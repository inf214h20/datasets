Billion Dollar Brain
 
 
 

{{Infobox film
| name           = Billion Dollar Brain
| image          = Billion Dollar Brain poster.jpg
| image_size     =
| caption        = original film poster
| director       = Ken Russell
| producer       = Harry Saltzman
| writer         = Len Deighton (novel) John McGrath
| starring       = Michael Caine Karl Malden Ed Begley Oscar Homolka Françoise Dorléac
| music          = Richard Rodney Bennett Billy Williams
| editing        = Alan Osbiston
| studio         = Jovera S.A. Lowndes Productions Limited
| distributor    = United Artists
| released       = 20 December 1967 (US)
| country        = United Kingdom
| language       = English
| runtime        = 111 minutes
| budget         =
| gross          = $1,500,000 (US/ Canada) 
}}
 espionage film novel of the same name by Len Deighton. The film features Michael Caine as secret agent Harry Palmer, the anti-hero protagonist. The "brain" of the title is a sophisticated computer  with which an ultra-right-wing organisation controls its worldwide anti-Soviet spy network.
 The Ipcress Funeral in Berlin (1966). It is the only film in which Ken Russell worked as a mainstream director-for-hire, and the last film to feature actress Françoise Dorléac.

A fourth film in the series, an adaptation of Horse Under Water, also to be released by United Artists, was tentatively planned  but never made. However, Caine played Palmer in two later films, Bullet to Beijing and Midnight in Saint Petersburg.

==Plot==
Harry Palmer (Michael Caine), who has left MI5 to work as a private investigator, is told by a mechanical voice on the phone to take a package to Helsinki. The package contains six virus-laden eggs that have been stolen from the British governments research facility at Porton Down. In Helsinki, he is met by Anya (Françoise Dorléac) who takes him to meet her handler, Harrys old friend Leo Newbigen (Karl Malden). Leo is in love with Anya, but Harry knows that she is only pretending to reciprocate. Leo takes Harry to a secret room where a computer issues daily instructions to Leo and Anya. The computer speaks in the same voice as the one which summoned Harry to Helsinki.
 Latvia where he embeds with some rebels to obtain intelligence for Leos operation. After being captured and left for dead, Harry is extracted from Russia by Colonel Stok (Oskar Homolka), an old acquaintance from the KGB. Back in Helsinki, Anya tries to kill Harry while seducing him, then confesses that the computer told her to kill him. Harry locks her in a room and waits for Leo at the computers location. Leo offers to pay off Harry for his trouble, but Harry insists on half of the money Leo is getting from whatever the conspiracy is all about.

The pair go to Texas, where Harry meets oil tycoon General Midwinter (Ed Begley). The General proudly displays his billion-dollar brain, a room full of computers that dispenses orders to his agents around the world. The General is in the midst of planning a rebellion in Latvia which he thinks will trigger the fall of the Soviet Union. His plan is to infect the Red Army with the viruses, while using his Latvian agents to begin a rebellion as his own private army invades. Meanwhile, Leo subverts the Generals computer orders and escapes with the eggs. The General realises Harry is a double agent, but Harry convinces him that he can track Leo down.

Back in Helsinki, Leo and Anya board a train for the Soviet Union with the eggs, but Harry, accompanied by two of Midwinters men, intercepts them and escorts Leo off the train with the eggs. Anya shoots Harrys bodyguards as the train pulls away from the station. Leo runs after the train and hands the eggs to Anya. As he tries to pull himself up, Anya pushes him off the train and shrugs as he looks at her in bewilderment. "She used me," Leo tells Harry. He then offers to help Harry stop the Generals insane plan, which could trigger World War III.

In personnel carriers made from oil tanker trucks from his company, the General leads his private army across the frozen Baltic Sea into Latvia. Harry and Leo attempt to catch up with the General, but he orders their car to be fired upon and Leo is killed. Meanwhile, Col. Stok is fully aware of the invasion and orders jets to intercept the convoy. Rather than firing directly on the convoy, the jets simply fire at the ice in the conveys path, breaking it. The entire convoy plunges into the freezing water, and all the vehicles and soldiers—including the General himself—sink below the ice to a cold, watery, Baltic grave.

Harry awakes alone on an ice floe. Col. Stok arrives in a helicopter with Anya and the eggs. He gives the eggs to Harry. "We dont need them," he says, "We have our own ideas." Stok confirms that Anya is one of his spies. Back in London, Harry delivers the eggs to Colonel Ross, who agrees to reward Harry with a promotion. However, when he opens the package to inspect the eggs, he finds they have hatched and the box is full of baby chicks.

==Cast==
 
* Michael Caine as Harry Palmer
* Karl Malden as Leo Newbigen
* Ed Begley as General Midwinter
* Oskar Homolka as Colonel Stok
* Françoise Dorléac as Anya
* Guy Doleman as Colonel Ross
* Vladek Sheybal as Doctor Eiwort
* Milo Sperber as Basil  
 

 

Cast notes:
* Donald Sutherland has a very small appearance as the computer technician who asks Karl Malden "Whats going on?"; Sutherland also appears as the mechanical voice on the phone at the beginning of the film. Susan George makes an early appearance as a young Latvian girl on a train who offers her copy of Isvestia to Michael Caine.

==Production== Helsinki and other parts of Finland, including Turku. The Riga scenes were filmed in Porvoo. The remainder of the film was shot at Pinewood Studios. Scenes involving "The Brain" were filmed in Honeywell facilities and featured a Honeywell 200 mini-computer.

Otto Heller – who had photographed the first two Harry Palmer films – was supposed to shoot the film but would not submit to a medical examination and so the production could not hire him. 

Principal Photography occurred from 30 January to the tail-end of May 1967.  Approximately five weeks later, on 26 June, Françoise Dorléac was killed in an automobile accident in Nice, France. It is unclear whether or not her voice was dubbed by another actress, due to her death.

==Reception==
The film has 60% approval on Rotten Tomatoes. Author and critic Anne Billson calls this "by far" the best film of the series, noting that critics and audiences did not like it on first release. 

==Soundtrack== brass and percussion  including three pianos, which are featured prominently in the main theme, and later, together with the percussion, create sonorities similar to Stravinskys Les Noces. The score is basically monothematic, constantly varying the main theme. For more romantic moods, it features the ondes Martenot, an early electronic instrument, played by its most prominent soloist, Jeanne Loriod. Thus, even the tender moments have an eerie undertone.
 11th Symphony "The Year 1905". Yet, music from the "Leningrad" symphony is featured later on during Midwinters speech to his soldiers in Finland and during the final battle on the ice.

==Miscellany== Alexander Nevsky (1938).  short (North long (European, older British) Names of large numbers|scales. In the short scale it would be one quintillion and in the long scale it would be one trillion.

==References==
 

==External links==
*  
*  
*  
*  at Trailers from Hell
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 