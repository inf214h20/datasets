Kilippattu (film)
{{Infobox film
| name           = Kilippattu
| image          =
| caption        = Raghavan
| producer       = Raghavan (dialogues) Raghavan
| Menaka Sukumaran
| music          = MB Sreenivasan
| cinematography = Vipin Das
| editing        =
| studio         = Revathichithra
| distributor    = Revathichithra
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Menaka and Sukumaran in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
*Adoor Bhasi
*Nedumudi Venu Menaka
*Sukumaran
*Balan K Nair
*K. P. Ummer
*Sabitha Anand

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by KM Raghavan Nambiar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aarodum Parayaruthe || K. J. Yesudas || KM Raghavan Nambiar || 
|-
| 2 || Aattavum Paattum || CO Anto, Latha Raju, Malathi || KM Raghavan Nambiar || 
|-
| 3 || Panchavarnnakkili || K. J. Yesudas || KM Raghavan Nambiar || 
|-
| 4 || Raavil Unarnnu || K. J. Yesudas || KM Raghavan Nambiar || 
|}

==References==
 

==External links==
*  

 
 
 

 