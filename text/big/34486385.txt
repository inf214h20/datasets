Fourth Commandment (1926 film)
{{infobox film
| name           = Fourth Commandment
| image          =
| imagesize      =
| caption        =
| director       = Emory Johnson
| producer       = Carl Laemmle
| writer         = Emory Johnson (adaptation) Carroll Owen (titles)
| starring       = Belle Bennett
| cinematography = Arthur L. Todd
| editing        =
| distributor    = Universal Pictures
| released       =   reels (2,091.84 meters)
| country        = United States
| language       = Silent film (English intertitles)
}}
Fourth Commandment is a 1926 silent film drama produced and distributed by Universal Pictures and directed by Emory Johnson. Johnson was an actor who was primarily in front of the camera in films such as The Sea Lion (1921). The picture stars Belle Bennett, Henry Victor, June Marlowe, and Mary Carr.

An incomplete print exists at British Film and Television (BFI) London.   

==Cast==
*Henry Victor - Gordon Graham
*June Marlowe - Marjorie Miller
*Belle Bennett - Virginia
*Leigh Willard - Edmund Graham
*Mary Carr - Mrs. Graham
*Brady Kline - Ray Miller (billed as Brady Cline)
*Catherine Wallace - Mrs. Miller Frank Elliott - Frederick Stoneman
*Knute Erickson - John Malloy
*Kathleen Myers - Mrs. Smith
*Robert Agnew - Sonny
*Wendell Phillips Franklin - Sonny, as a child
*Lorraine Rivero - Marjorie, as a child
*Malcolm Jones - Gordon, as a child
*Stanley Taylor - Count Douglas Von Rosen

==References==
 

==External links==
* 
* 

 
 
 
 
 


 