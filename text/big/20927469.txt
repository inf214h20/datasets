Girl No. 217
 
{{Infobox film
| name           = Girl No. 217
| image          = 
| caption        = 
| director       = Mikhail Romm
| producer       = 
| writer         = Mikhail Romm Yevgeny Gabrilovich Yelena Kuzmina
| music          = Aram Khachaturian
| cinematography = Boris Volchek Era Savelyeva
| editing        = 
| studio         = Mosfilm
| distributor    = 
| released       =  
| runtime        = 
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

Girl No. 217 ( , Transliteration|translit.&nbsp;Chelovek No. 217) is a 1945 Soviet drama film directed by Mikhail Romm. It was entered into the 1946 Cannes Film Festival.     

An anti-Nazi film, it depicted a Russian girl enslaved to an inhuman German family. Anthony Rhodes, Propaganda: The art of persuasion: World War II, p219 1976, Chelsea House Publishers, New York   She is even robbed of her name and forced to answer to "No. 217". " "   Subplots depict abuse directed at other POWs.   This reflected the use by Nazis of OST-Arbeiter as slave labour, including as family servants.

==Cast== Yelena Kuzmina - Tanya Krylova - Nr. 217
* Vladimir Balashov
* Tatyana Barysheva
* Gregori Greif - Kurt Kahger
* Anastasiya Lissianskaya - Klava
* Grigory Mikhaylov - Prisoner Nr. 225
* Lyudmila Sukharevskaya - Lotta
* Peter Suthanov - Rudolph Peschke
* Vasili Zajchikov - Scientist

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 
 