Der weisse Rausch
{{Infobox film
| name           = Der weisse Rausch
| image          = Der wei rauschposter.jpg
| border         = yes
| alt            = 
| caption        = German theatrical release poster
| director       = Arnold Fanck
| producer       = Harry R. Sokal
| writer         = Arnold Fanck
| starring       = {{Plainlist|
* Hannes Schneider
* Leni Riefenstahl
* Guzzi Lantschner
* Walter Riml
}}
| music          = Paul Dessau
| cinematography = {{Plainlist|
* Richard Angst
* Hans Karl Gottschalk
* Bruno Leubner
* Kurt Neubert
}}
| editing        = Arnold Fanck
| studio         = Sokal-Film GmbH
| distributor    = Aafa-Film AG
| released       =  
| runtime        = 70 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}

Der weisse Rausch (The White Ecstasy) is a 1931 German mountain film written and directed by Arnold Fanck and starring Hannes Schneider, Leni Riefenstahl, Guzzi Lantschner, and Walter Riml. The film is about the skiing exploits of a young village girl (played by Riefenstahl), and her attempts to master the sport of skiing and ski-jumping aided by the local ski expert (played by Schneider). Filmed on location in Sankt Anton am Arlberg, the film was one of the first to use and develop outdoor film-making techniques and featured several innovative action-skiing scenes.

Two Tyrolean skiing stars, Walter Riml and Guzzi Lantschner, have important parts in this movie. They play two Hamburger carpenters in their traditional outfits. They come to the Arlberg and try to learn how to ski with the aid of two different skiing books. A Weisse Rausch downhill race, based on the film, is held every April in St. Anton. 

==Cast==
* Hannes Schneider as Hannes
* Leni Riefenstahl as Leni
* Rudi Matt as Rudi
* Lothar Ebersberg as Der kleine Lothar
* Guzzi Lantschner as Guzzi, the Hamburger carpenter
* Walter Riml as Walter, the Hamburger carpenter 
* Otto Lantschner
* Harald Reinl
* David Zogg
* Josef Gumboldt
* Hans Kogler
* Luggi Foeger
* Benno Leubner
* Hans Salcher
* Kurt Reinl

==References==
;Notes
 
;Bibliography
 
*   (20 December 1931) Berlin
* Sadoul, Georges; Morris, Peter (1972).   University of California Press ISBN 978-0-520-02151-8 pg 79
* Hinton, David B. (2000)   Scarecrow Press ISBN 9781461635062 pg 9-10
* Richards, Jeffrey (2013) Visions of Yesterday ISBN 131792861X pg 294
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 