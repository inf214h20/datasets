Girls for the Summer
{{Infobox film
| name           = Racconti destate
| image          = 1958 Racconti d estate poster.jpg
| image_size     = 
| caption        = 
| director       = Gianni Franciolini
| producer       = Mario Cecchi Gori  Franz de Blasi    
| writer         = Gianni Franciolini Alberto Moravia   Sergio Amidei    René Barjavel  Ennio Flaiano  Rodolfo Sonego Alberto Sordi  Edoardo Anton
| narrator       = 
| starring       = 
| music          = Piero Piccioni 
| cinematography = Enzo Serafin 
| editing        = Adriana Novelli 
| distributor    = Ultra Pictures Corporation (USA) 
| released       = 1958
| runtime        = 
| country        = Italy / France
| language       = Italian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Girls for the Summer (  or US title: Love on the Riviera) is a 1958 Italian romantic comedy drama film directed by Gianni Franciolini,    based on story by Alberto Moravia. The film stars Alberto Sordi, Michèle Morgan, Marcello Mastroianni, Sylva Koscina, Gabriele Ferzetti, Dorian Gray, Franca Marzi, Franco Fabrizi and Jorge Mistral.  

It tells the five romantic and funny stories in the Tigullio Gulf (Liguria, Italy).

==Cast==
* Alberto Sordi - Aristarco Battistini
* Michèle Morgan - Micheline
* Marcello Mastroianni - Marcello Mazzoni
* Sylva Koscina - Renata Morandi
* Gabriele Ferzetti - Giulio Ferrari Dorian Gray - Dorina
* Franca Marzi - Clara
* Lorella De Luca - Lina
* Franco Fabrizi - Sandro Morandi
* Ennio Girolami - Walter (as Enio Girolami)
* Jorge Mistral - Romualdo (as Jeorge Mistral)
* Dany Carrel - Jacqueline

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 
 