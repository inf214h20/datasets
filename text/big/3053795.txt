The Suburbans
 
{{Infobox film
| name        = The Suburbans
| caption     = DVD cover for The Suburbans
| image	      = The Suburbans FilmPoster.jpeg
| alt         = 
| writer      = Donal Lardner Ward Tony Guma
| starring    = {{Plain list | 
* Jennifer Love Hewitt
* Donal Lardner Ward
* Amy Brenneman
* Tony Guma
* Craig Bierko
* Will Ferrell
}}
| director    = Donal Lardner Ward Michael Burns Leanna Creel Brad Krevoy
| distributor = Tri-Star Pictures
| released    =  
| runtime     = 81 minutes
| language    = English
| music       = Robbie Konder
| awards      =
| budget      =
}}
The Suburbans is a 1999 comedy-drama that satirizes the 1980s revival hype around the turn of the 21st century. It stars Jennifer Love Hewitt and Donal Lardner Ward, who also co-wrote the movie with Tony Guma and directed the movie.

The Suburbans premiered at the Sundance Film Festival on January 25, 1999. It was released on a very limited number of screens (11) on October 29 of the same year, and grossing $11,130, is considered to have failed commercially.  Of ten reviews counted at Rotten Tomatoes, all ten are negative. 

== Plot ==
In 1998 Danny, Mitch, Gil and Rory, formerly known as long-forgotten, early 80s one-hit wonder The Suburbans, reunite to perform their only hit single at one of the band members wedding. After the gig Cate, an up-and-coming record company executive, approaches them and suggests to shoot a pay per view reunion show that would eventually re-establish the bands claim to fame. The four, more reluctantly than not, agree and subsequently face the ramifications on their personal lives as the shows production contrasts their former rock n roll image with their now middle-class, suburban life style. It soon becomes evident that Cate is probably the only remaining fan of the band, who, out of a personal interest in the matter, put her own career at stake.

== Cast ==
* Donal Lardner Ward as lead singer Danny
* Jennifer Love Hewitt as record company executive Cate
* Amy Brenneman as Dannys girlfriend Grace
* Craig Bierko as lead guitarist Mitch
* Will Ferrell as bass player Gil
* Tony Guma as drummer Rory
* Bridgette Wilson as Rorys girlfriend Lara
* Ben Stiller as record company owner Jay Rose
* Perrey Reeves as Amanda
* Jerry Stiller as record company owner Speedo Silverberg
* Dick Clark as himself, hosting a fictional episode of American Bandstand
* Kurt Loder as himself, an MTV interviewer
* A Flock of Seagulls as themselves in a cameo appearance

== See also == Sugar Town, another "rock-and-roll and relationships"  film released a month earlier, and called by Janet Maslin&mdash;in her review of The Suburbans&mdash;a "better and more ambitious recent film that   had no luck in finding an audience"   

== References ==
 

== External links ==
*  
*  
*  
*   for  

 

 
 
 
 
 
 
 
 