Drunken Master
 
 
 
{{Infobox film name = Drunken Master image = DrunkenMasterMoviePoster.jpg caption = Original Hong Kong movie poster traditional = 醉拳 jyutping = Zeoi3 Kyun4}} director = Yuen Woo-ping producer = Ng See-yuen writer = Lung Hsiao Ng See-yuen Yuen Woo-ping starring = Jackie Chan Yuen Siu-tien Hwang Jang Lee Dean Shek music = Chow Fu-liang cinematography = Chang Hui editing = Pan Hsiung distributor = Seasonal Film Corporation released =   runtime = 110 minutes country = Hong Kong language = Cantonese budget = gross = HKD$ 6,763,793 
}} comedic Chinese kung fu genre for which Jackie Chan became famous. The film popularised the Zui Quan (醉拳, "drunken fist") fighting style. Ranked number 3 on totalfilm.coms 50 greatest kung fu movies of all time.

==Background==
The films protagonist Wong Fei-hung was a Chinese martial artist, a traditional Chinese medicine practitioner and a revolutionary who lived towards the end of the Qing Dynasty. He became a Chinese folk hero and the subject of several Hong Kong television programmes and films. Beggar So, who plays a supporting role in the film, is also another character from Chinese folklore and one of the Ten Tigers of Canton. The Beggar So character is often cast as an associate of Wong Fei-hung or Wongs uncle.

==Plot==
The plot centers on a young and mischievous Wong Fei-hung (sometimes dubbed as "Freddie Wong"). Wong runs into a series of troubles. Firstly, he teaches an overbearing assistant martial arts teacher a lesson. Next, he makes advances on a woman to impress his friends, and is soundly thrashed by her older female guardian as a result; his shame is compounded when these two are later revealed to be his visiting aunt and cousin, whom he had not met before. Lastly, he beats up a hooligan who is the son of an influential man in town. His father decides to punish him for his behavior by making him train harder in martial arts.

Wongs father arranges for Beggar So to train his son in martial arts. Beggar So has a reputation for crippling his students during training so Wong flees from home in an attempt to escape his punishment. Penniless, he stops at a restaurant and tries to con a fellow patron into offering him a free meal. As he was about to leave after his meal, he discovers that the man is actually the owner of the restaurant. He fights with the owners lackeys in an attempt to escape. An old drunkard nearby is drawn into the fight and helps him escape. The drunkard turns out to be Beggar So, the Drunken Master. (Beggar So is known in some versions of the film as Sam Seed, So Hi or Su Hua-chi)

Beggar So forces Wong into his brutal and rigorous training programme. Wong flees again to avoid the torturous training and runs into the notorious killer Yan Ti San (known in some versions as Thunderfoot or Thunderleg) by accident. Yan is known for his "Devils Kick", a swift and deadly kicking style which has never been defeated. Wong provokes and challenges him to a fight and is soundly defeated and humiliated. He makes his way back to Beggar So and decides to commit himself to the Drunken Masters training program.
 Drunken Boxing The Eight xian that Drunken Miss Hos as he feels that her style of fighting is too feminine.

Meanwhile, Yan Ti San is contracted by a business rival to kill Wongs father. Wongs father fights with Yan and is defeated and injured by him. Wong and Beggar So arrive on the scene on time and Wong continues the fight with Yan. Beggar So promises not to interfere in the fight. Wong employs the new skills he has learned and outmatches Yans kicking style. Yan then resorts to his secret technique, the Devils Shadowless Hand, which is too fast for Wong to defeat. Wong confesses that he did not master the last style so Beggar So tells him to combine the seven styles and create his own version of the last style. Wong follows the instruction and discovers his own unique style of Drunken Miss Ho, which he uses to overcome the Shadowless Hand and finally defeat Yan.

==Cast==
*Jackie Chan as Wong Fei-hung / Freddy Wong
*Yuen Siu-tien (or Simon Yuen) as Beggar Su Hua Chi / Sam Seed
*Hwang Jang Lee as Thunderleg / Thunderfoot
*Lam Kau as Wong Kei-ying / Robert Wong
*Fung Ging Man as Mr Li
*Hsu Hsia as King of Bamboo Hsu Ching Tien
*Linda Lin as Wong Fei-hungs aunt
*Dean Shek as Professor Kai-hsien
*Yuen Shun-yi as Chen Ko-wai / Charlie Wei
*Tong Jing as Wong Fei-hungs cousin
*Tino Wong as Jerry Li

==Production==
According to his book I Am Jackie Chan: My Life in Action, Chan nearly lost an eye after his brow ridge was injured.   

==Fight scenes and martial arts== Lama Pai Black Tiger, Snake systems Monkey style kung fu, popular in Southern Chinese martial arts performances, is also shown briefly.
 Drunken Boxing" Choi Lei Taoist Eight Immortals are popular staples of Chinese culture and art. However, the "Eight Drunken Immortals" forms depicted in this film are likely the creation of director and choreographer Yuen Woo-ping and based on routines found in other systems.

The primary villain in Drunken Master is played by Hwang Jang Lee, a Korean martial artist specialising in Taekwondo and known for his high-flying kicks, which are prominently displayed in the film. The systems of "Devils Kick" and "Devils Shadowless Hands" employed by Thunderleg are entirely fictitious.

==Box office==
Drunken Master earned an impressive HK $6,763,793 at the Hong Kong box office. 

==Sequels and spinoffs==
*Drunken Master was a semi-sequel to the 1978 film Snake in the Eagles Shadow, which featured the same cast and director.
*Drunken Master II (1994) did star Jackie Chan, and is often considered a sequel. However the film is not an exact storyline sequel despite Chan portraying the same character. The US release of the film in 2000 was entitled The Legend Of Drunken Master.
*In 1979 Yuen Siu-tien reprised the role of Beggar So in the film Dance of the Drunk Mantis, which is entitled Drunken Master Part 2 (not to be confused with Drunken Master II noted above) in some releases. The film, which was again directed by his son, Yuen Woo-ping, does not feature Jackie Chan, focusing instead on the drunken beggar character rather than on Wong Fei-hung. It is therefore generally considered to be a spinoff rather than a true sequel.
*Yuen played this same role again in the films Story of Drunken Master and World of the Drunken Master.
*In 2010 Yuen Woo-ping returned to directing with True Legend which could be called prequel to Drunken Master as it explains why Beggar Su (played by Vincent Zhao) turns to drinking.

==Imitators==
  
As with many successful Hong Kong action films, several films were released in the wake of Drunken Master (and its sequel) that could be considered to trade on the fame of the original films. These had less in common with the original films than the spinoffs starring Yuen Siu Tien.
They include:
*5 Superfighters (aka The Drunken Fighter) (1978)
*Drunken Swordsman (aka Drunken Dragon Strikes Back) (1979)
*The Shaolin Drunken Monk (starring Gordon Liu) (1982)
*Drunken Tai Chi (directed by Yuen Woo-ping and starring Donnie Yen) (1984)
*Revenge of the Drunken Master (1984), starring Johnny Chan,  whose name allowed him to trade off his more successful namesake in other low-budget martial arts films including Golden Dragon, Silver Snake (1979) and The Eagles Killer (1978)
*Drunken Master III (aka Drunken Master Killer) - starring Andy Lau (1994)
*The Little Drunken Masters (1995)
 Drunken Monkey (2002) may feature a drunken style of kung fu, and in the case of The Forbidden Kingdom (2008), the same principal star, but they have a fundamentally different plot and sufficiently different title to separate them from Drunken Master.

==Home media==
 
*On 24 April 2000,   soundtrack with dubtitles. However, it has a number of additional features including a deleted scene and an interview with producer Ng See-yuen.
*On 2 April 2002,  s. Theres an audio commentary by Ric Meyers and Jeff Yang. 
*On 18 March 2004, HKVideo released a "Wong Fei Hung" DVD boxset in France containing this film (French title: "Le maître chinois") and two others. It contains a full 2:35:1 image and the Cantonese soundtrack. However, it contains slightly poorer image quality and no English subtitles.
*On 30 April 2004, Mei Ah Entertainment released a remastered DVD in Hong Kong (pictured right). It contains a 2:35:1 image, Cantonese Dolby Digital 5.1 track, original Cantonese Dolby Digital 2.0 mono track and Mandarin Dolby Digital 2.0 mono track. Subtitles include Traditional Chinese, Simplified Chinese and English. The missing Cantonese for the opening has been re-dubbed in Cantonese and the other missing Cantonese scenes as extended footage in Mandarin due to trouble of re-dubbing with new voice actors. Many short lines missing Cantonese had been removed. Special features include Extended footage, accessed during the film by selecting the wine jug icon when it appears on the right top corner, Mastering the Drunken Master, a 35 second music video with clips of Jackie Chan practicing the 8 Drunken Gods from the film, film synopsis and cast & crew.
 bootleg DVD that contains the complete Cantonese track.

==Influence on popular culture==
  Son Goku. Author Akira Toriyama said that Drunken Master was one of his major inspirations for the Dragon Ball series.
*The PlayStation game Jackie Chan Stuntmaster includes a bonus level in which he wears his traditional Drunken Master dress and drinks wine while fighting. He even gives the Drunken Punch as his charge punch throughout the game. Guild Wars, there is a stance-skill called "Drunken Master" which temporarily increases movement and attack speed. This effect is doubled if character is drunk. Chin Gentsai was modeled after Su Hua Chi.
*Jamaican musicians Sly Dunbar, Robbie Shakespeare, and The Revolutionaries recorded a reggae song called Drunken Master which was released in 1981 by Island on an album called  .
* In the Naruto series, one of the characters Rock Lee is seen performing similar fighting styles after consuming alcohol. Known as the Drunken Fist in the series Japanese version and the Loopy Fist in the English.
*The   released in 2012, Lei Wulongs "Ultimate Stance" is "Drunken Fist" based on his performance in the 1978 original and the 1994 sequel.
*UK Dubstep artist FuntCase used speech samples taken from the film for his song  .

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 