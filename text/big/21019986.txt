Rhinoceros (film)
{{Infobox film
| name           = Rhinoceros
| image          = Poster of Rhinoceros (film).jpg
| image_size     =
| caption        =
| director       = Tom OHorgan
| producer       = Ely Landau
| writer         = Julian Barry Eugène Ionesco (play)
| narrator       =
| starring       = Gene Wilder Zero Mostel Karen Black
| music          = Galt MacDermot
| release        =
| cinematography = Jim Crabe
| editing        = Bud Smith
| released       = January 21, 1974 (USA)
| runtime        = 101 minutes United States of America
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| distributor    = American Film Theatre
}}
 1974 comedy the play by Eugène Ionesco. The film was produced and released as part of the American Film Theatre, which adapted theatrical works for a subscription-driven cinema series.

==Plot==
The residents of a large town are inexplicably turning into rhinoceroses. Stanley (Gene Wilder), a mild-mannered office clerk, watches the bizarre transformations from a bemused distance. But soon the strange occurrences invade his personal space, as his neighbor and best friend John (Zero Mostel) and his girlfriend Daisy (Karen Black) become part of the human-into-rhinoceros metamorphosis that is taking place. Eventually, Stanley realizes that he may be the only human left amidst the new rhinoceros majority.   

==Production==
In adapting Ionesco’s play, several changes were made to the original text. The setting was switched from   was created for the film and a dream sequence was added to the story. 
 Broadway production of the play, recreated his role as the man who turns into a Rhinoceros. Mostel created a minor brouhaha during the production when he refused to smash any props during the rehearsal of his transformation scene – the actor claimed he had an aversion to destroying property. 

Although OHorgan considered using a live animal to dramatize the transformation, no rhinoceros is ever seen on camera during the film – shadows and POV camera angles are used to suggest the presence of the animals. 

==Release==
Rhinoceros was poorly received when it had its theatrical release as part of the American Film Theatre series. Jay Cocks, reviewing the film for Time magazine|Time, faulted the film for its “upbeat, frantic vulgarization” of the Ionesco text, arguing that O’Horgan “removed not only the politics but the resonance as well. What remains is a squeaky sermon on the virtues of nonconformity.”  Vincent Canby, writing in The New York Times, dismissed the film as “an unreliable mouthpiece in an unreliable metaphor so grossly overdirected by Tom OHorgan that you might get the idea Mr. OHorgan thought he was making a movie for an audience made up entirely of rhinoceroses instead of people.”

==References==
 

==External links==
*  at the Internet Movie Database

 
 
 
 
 
 