The Lady and the Mouse
 
{{Infobox film
| name           = The Lady and the Mouse
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith
| starring       = Lillian Gish
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short drama film directed by  D.W. Griffith. A print of the film survives.   

==Cast==
* Lillian Gish - The Young Woman
* Lionel Barrymore - The Young Womans Father
* Harry Hyde - The Rich Man / Tramp
* Dorothy Gish - The Sick Sister
* Kate Toncray - The Aunt
* Robert Harron - The Young Friend
* Adolph Lestina - The Doctor
* Henry B. Walthall - At Garden Party
* Viola Barry - At Garden Party
* J. Jiquel Lanoe - At Garden Party
* Mae Marsh - Undetermined Role (unconfirmed) Joseph McDermott - Creditor
* Frank Opperman - Creditor
* W. C. Robinson - Creditor

==See also==
* D. W. Griffith filmography
* Lillian Gish filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 