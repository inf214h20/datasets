Chakravalam Chuvannappol
{{Infobox film
| name           = Chakravalam Chuvannappol
| image          =
| caption        =
| director       = J. Sasikumar
| producer       =
| writer         = Ranka Dr Pavithran (dialogues)
| screenplay     = Dr Pavithran
| starring       = Prem Nazir Mohanlal Mammootty Sumalatha
| music          = M. K. Arjunan
| cinematography = RR Rajkumar
| editing        = K Sankunni
| studio         = Soorya Productions
| distributor    = Soorya Productions
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar. The film stars Prem Nazir, Mohanlal, Mammootty and Sumalatha in lead roles. The film had musical score by M. K. Arjunan.    

==Cast==
 
*Prem Nazir
*Mohanlal as Surendran
*Mammootty as Vasu
*Sumalatha
*Vanitha Krishnachandran
*Jagathy Sreekumar
*Baby Reena
*Bindulekha
*C. I. Paul
*Kaval Surendran
*Radhadevi
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Chirayinkeezhu Ramakrishnan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kuttathippenne || Balagopalan Thampi || Chirayinkeezhu Ramakrishnan Nair || 
|-
| 2 || Ore veenathan || K. J. Yesudas || Chirayinkeezhu Ramakrishnan Nair || 
|-
| 3 || Panineer Thalikkunna || Vani Jairam || Chirayinkeezhu Ramakrishnan Nair || 
|-
| 4 || Thaamarappoykaye || K. J. Yesudas, Vani Jairam, Chorus || Chirayinkeezhu Ramakrishnan Nair || 
|}

==References==
 

==External links==
*  

 
 
 

 