Born Lucky (film)
{{Infobox film
| name           = Born Lucky
| image          = 
| caption        = 
| director       = Michael Powell Jerome Jackson
| writer         = Ralph Smart Rene Ray John Longden
| music          = 
| cinematography = Geoffrey Faithfull
| editing        = 
| distributor    = Westminster Films
| released       =  
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
}}
 Rene Ray and John Longden.  The screenplay was adapted from the 1928 novel  Mops by Marguerite Florence Barclay.

Born Lucky is one of eleven quota quickies directed by Powell between 1931 and 1936 of which no print is known to survive.  The film is not held in the BFI National Archive (nor in this case do they even hold any stills or publicity material), and is classed as "missing, believed lost". This was the first film that the great cinematographer Oswald Morris worked on as a clapper boy. 

==Plot==
Mops (Ray), so called because of her striking curly hair, is an orphan living in the East End of London with her guardian Turnips (Talbot OFarrell), whose nickname derives from his craft of carving flowers out of vegetables, which he sells to earn a few extra coppers to augment his income as a lighting-man at the local music hall.  Mops performs there and earns a living wage, but has to contend with the unwanted advances of the manager.  When he tries to force himself on her, Turnips beats him up and both he and Mops are sacked.
 in service.

Having taken the advice, Mops finds a place as kitchen-maid with Lady Chard.  As the lowest in the servants pecking-order she is given the most menial jobs and is bullied by more senior domestics.  Early one morning when she is alone in the kitchen she hears a knock, and opens the door to find the man she befriended on the road.  She helps him out of a predicament, and they begin walking out together after he successfully applies for a position with playwright Frank Dale.  Some time later Mops is dismissed from her post after being blamed for starting a fire in the house.  She goes to visit her beau, and finds him smartly-dressed and in conversation with a theatrical impresario.  He admits that he is really Frank Dale, and all along he has been using her as research material for his new play.  On being told of Mops music hall background, Frank and the impresario offer her the lead role in the play.  Both the play and Mops are overnight sensations, and she is welcomed by society.  After Frank has rid himself of his grasping fiancée, and Mops has exacted her revenge on the magistrate who imprisoned Turnips, the couple are married and the future looks bright for them and the newly released Turnips.

==Cast== Rene Ray as Mops
* John Longden as Frank Dale
* Talbot OFarrell as Turnips
* Ben Welden as Harriman
* Barbara Gott as Cook
* Helen Ferrers as Lady Chard
* Roland Gillett as John Chard
* Paddy Brown as Patty

==Reception==
Surviving contemporary reviews show a muted critical response to the film.  Kine Weekly wrote: "The treatment shows some imagination, if the stars shine but dimly", adding that the hop-picking sequences were "picturesque and original".   Picturegoer Weekly found less to impress, stating: "It is all very naïve and the continuity is rather ragged owing to an excess of varied detail which makes for lack of cohesion". 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 