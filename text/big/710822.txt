The Adventures of Pinocchio (1996 film)
{{Infobox film
| name           = The Adventures of Pinocchio
| image          = Adventures of pinocchio ver1.jpg
| image_size     = 240
| caption        = Theatrical release poster
| director       = Steve Barron
| producer       = Heinz Bibo Raju Patel Jeffrey M. Sneller
| writer         = Sherry Mills Steve Barron Tom Benedek Barry Berman
| based on     =   David Doyle David Doyle Geneviève Bujold
| music          = Rachel Portman
| cinematography = Juan Ruiz Anchía
| editing        = Sean Barton
| studio         = The Kushner-Locke Company Savoy Pictures
| distributor    = New Line Cinema
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom Italy France Germany Malaysia
| language       = Italian English
| budget         = $25 million
| gross          = $15,094,530 
}} original novel of the same name, directed by Steve Barron and starring Jonathan Taylor Thomas, Martin Landau and Udo Kier.

==Plot== Volpe and Lorenzini (Udo Kier) who owns a luxurious puppet theatre. Felinet tells him about Pinocchio, and Lorenzini appears interested, so he visits Geppetto that afternoon to barter for Pinocchio. However, although Pinocchio is somewhat taken to Lorenzini, Geppetto refuses to sell Pinocchio and instead sells some of his own marionettes.
 wise and David Doyle). He promises to help Pinocchio to become a real boy, if he will start behaving properly. The next day, Geppetto and Pinocchio stand before the judge (voiced by Jerry Hadley) as the bakers wife sways the court in her favor. The judge rules that unless Geppetto can pay for the damages, he will be sent to a debtors prison for three years. Lorenzini steps in and offers to pay off the debt, on the condition that Pinocchio be handed over to his custody. Geppetto strongly refuses but eventually gives in, believing that perhaps the puppet will be better off that way.

Pinocchio comes to enjoy the theater and also comes to believe that Lorenzini loves him as much as his father did, especially after receiving few golden coins from him. Though Pepe tells him that Lorenzini is just using him for riches and fame. Pinocchio comes to realize this as he performs in Lorenzinis play, and manages to save several puppets from being burned by the cruel Lorenzini. As he escapes, he accidentally sets the theater aflame. He floats away down the river, passing through the woodlands to a quiet monastery. Volpe and Felinet catch up with him and manage to swindle him out of the golden coins by telling him that if he buries them in the ground, they will grow into a tree of miracles that will turn him into a real boy. Pepe scolds Pinocchio and tells him that miracles are actually made in the heart. As expected, Volpe and Felinet steal the coins while Pinocchio is occupied.
 Terra Magica, a hidden funfair for boys where visitors can engage in all sorts of activity, including bad behavior without repercussions. While going past the waterfall entrance, Pinocchios hat falls off and washes up on the beach. Geppetto finds it and assumes Pinocchio is lost at sea. Finally realizing how much he loves Pinocchio like a son, Geppetto decides to venture out to find him.

Meanwhile, while riding a roller coaster, Pinocchio discovers that the water of Terra Magica is a cursed fluid which upon consumption turns humans into creatures symbolic of their behavior (in this case, the badly behaving boys are transformed into "donkey|jackasses"). Lampwick and several other boys turn into donkeys while Pinocchio only ends up with wooden donkey ears. Pinocchio when realizes that the entire trip is a scheme by Lorenzini, who is seeking to regain his fortune by turning the boys into donkeys and selling them off. Pinocchio releases Lampwick and the donkeys and in the ensuing fight, Lorenzini falls into a fountain filled with the water which horribly mutates him as he flees diving into the sea. Pinocchio, the boys and the donkeys escape Terra Magica, though Lampwick remains with Pinocchio to keep him company.
 sea monster. From the strong smell of rotten chili peppers, Pinocchio realizes that it is actually the fully transformed Lorenzini, remembering his fondness for them. Pinocchio eventually finds Geppetto and shares a warm reunion with him. As they try to climb through Lorenzinis throat, Pinocchio lies about hating Geppetto and his nose grows so long that it puts pressure on Lorenzinis throat, causing him to cough so much that Pinocchio and Geppetto are rocketed out of his mouth and sent back to shore.

As Pinocchio and Geppetto embrace, the same mysterious force appears again, this time turning Pinocchio into a human child. While returning home with Geppetto, Leona and Lampwick, Pinocchio spots Volpe and Felinet and decides to pay them back for the trouble they caused him. He tells them about Terra Magica and the cursed water, but adds that if they drink it while holding a rock, it will turn to gold. They fall for it and end up turning into a real fox and cat, and are captured by a farmer.

Later on, Lampwick and the other donkeys turn back into boys for reforming. One day while playing, Pinocchio and Lampwick accidentally knock over a cart of logs. Pinocchio takes one to Geppetto and asks him to carve him a girlfriend, much to Geppettos surprise.

==Cast==
* Martin Landau as Geppetto, an impoverished puppet maker who accidentally gives Pinocchio life after carving him from an enchanted log. He initially refuses to accept the puppet as his son, but warms up to him once he loses him.
* Jonathan Taylor Thomas as Pinocchio, the eponymous character and main protagonist of the film. He seeks to learn about right and wrong so that one day he will become a real boy. He was puppeteered by Mak Wilson, Robert Tygner, Michelan Sisti, Bruce Lanoil, William Todd-Jones, and Ian Tregonning. David Doyle as the voice of Talking Cricket|Pepe, Pinocchios spiritual conscience. In the trailer, Wallace Shawn was cast as Pepe until the role was recast to Doyle. This was Doyles final performance before his death in 1997 next year.
* Geneviève Bujold as Leona, a friend of Geppettos who Geppetto is secretly in love with, a love which is actually mutual. She serves as the Blue Fairys stand-in in the film but is absent from the sequel when Geppetto briefly mentions her name when he talks about the Wooden Heart he carved onto Pinocchio when Pinocchio was still inside the Tree.
* Udo Kier as Lorenzini, an original character created for the film. He is an amalgamation of Mangiafuoco, The Coachman and The Terrible Dogfish, and serves as the films main antagonist. His fondness for chilli peppers, which give him his somewhat fiery breath, is an homage to Mangiafuoco.
* Bebe Neuwirth as The Fox and the Cat|Felinet, a scheming con artist always looking for the next profit.
* Rob Schneider as The Fox and the Cat|Volpe, Felinets dimwitted partner. His name is the Italian word for "Fox". He and Felinet are based on the Fox and the Cat from the original novel.
* Corey Carrier as Candlewick (Pinocchio character)|Lampwick. Unlike in the book and the 1940 Disney version, Lampwick truly becomes Pinocchios best friend, and affectionately calls him "Woody" even after they have both become real boys.
* Dawn French as the Bakers Wife. Though she does more damage to her shop than Pinocchio, she sways the court judge to rule in her favour.
* Richard Claxton as Saleo, Lampwicks companion and friend who kicks Pinocchio in class at school. He is turned into a donkey, along with Lampwick and another boy (Joe Swash), after drinking cursed water on Terra Magicas roller coaster. Unlike Lampwick, Saleo does not appear in the sequel.
* John Sessions as the Professor, an irritable teacher who Pinocchio inadvertently annoys while attending one of his classes.
* Jerry Hadley as the voice of the Judge, a court official who threatens to send Geppetto to a debtors prison for Pinocchios irresponsible behavior.

==Development== Disney with this idea, but Disney turned down the project. Years later, producer Peter Locke sent Barron a script for a film based on the Carlo Collodi novel. Barron heavily rewrote the script. The project then finally got off the ground.

==Production== David Doyle as the voice of Pepe the talking cricket. However Shawns voice as Pepe can still be heard in the trailer for the film and he is even credited in the trailer.
 David Roach.

==Box office==
The Adventures of Pinocchio was made on a budget of $25 million. Because it only generated $15 million revenue, the film was a box office bomb.

==Critical Reception==
Critically, the film has received mixed to negative reviews from critics, with a rotten 27% rating on Rotten Tomatoes. On the popular television review series Siskel & Ebert, Roger Ebert was disappointed with the film, while Gene Siskel was more lenient, praising the special effects. 

In her seminar "The Persistent Puppet: Pinocchios Heirs in Contemporary Fiction and Film," Rebecca West finds the film to be relatively faithful to the original novel, although she notes major differences such as the replacement of the Blue Fairy by the character of Leona. 

==Soundtrack==
{{Infobox album
| Name = The Adventures of Pinocchio: Original Motion Picture Soundtrack
| Type = Soundtrack Sissel (songs) and Rachel Portman (score)
| Cover = Theadventuresofpinocchiooriginalmotionpicturesoundtrack.jpg
| Released = July 26, 1996 Score
| Length = 64:38 Decca
}}

{{Track listing
| writing_credits = yes
| total_length = 64:38
| extra_column = Performer(s) Just William
| title1 = II Colosso
| writer1 = Brian May, Lee Holdridge
| length1 = 7:36
| extra2 = Jerry Hadley
| title2 = Luigis Welcome
| writer2 = Spencer Proffer, David Goldsmith (lyricist), Holdridge
| length2 = 2:33
| extra3 = The Morling School Ensemble with Jonathan Shell
| title3 = All For One
| length3 = 2:27
| writer3 = Craig Taubman
| extra4 = Stevie Wonder
| title4 = Kiss Lonely Good-Bye (with Orchestra) 
| writer4 = Stevie Wonder
| length4 = 4:39
| extra5 = Stevie Wonder
| title5 = Hold On To Your Dream (with Orchestra)
| writer5 = Wonder
| length5 = 4:21
| extra6 = 
| title6 = Theme From Pinocchio 
| length6 = 7:17
| writer6 = Rachel Portman
| extra7 = 
| title7 = Lorenzini
| length7 = 3:22
| writer7 = Portman
| extra8 = 
| title8 = Terra Magica
| length8 = 3:56
| writer8 = Portman
| extra9 = 
| title9 = Pinnocchio Becomes A Real Boy
| length9 = 5:10
| writer9 = Portman
| extra10 = Stevie Wonder
| title10 = Kiss Lonely Good-Bye (Harmonica With Orchestra)
| length10 = 4:39
| writer10 = Wonder
| extra11 = Geppettos Workshop
| title11 = Pinnocchios Evolution
| length11 = 3:46
| writer11 = Wonder
| extra12 = Brian May, Sissel
| title12 = What Are We Made Of
| length12 = 3:41
| writer12 = May
| extra13 = Stevie Wonder
| title13 = Hold On To Your Dream
| writer13 = Wonder
| length13 = 6:00
| extra14 = Stevie Wonder
| title14 = Kiss Lonely Good-Bye
| length14 = 5:02
| writer14 = Wonder
}}

==Sequel== The New Luxembourg City, Luxembourg. Like the first movie, the animatronics effects were made by Jim Hensons Creature Shop.

==References==
 

==External links==
 
*  
*   at Rotten Tomatoes
*   at AllRovi

 

 

 

 
 

 
 
 
 
 
 
 
 