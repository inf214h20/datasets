Oh, to Be on the Bandwagon!
{{Infobox film
| name           = Oh, to Be on the Bandwagon!
| image          = 
| caption        = 
| director       = Henning Carlsen
| producer       = Henning Carlsen
| writer         = Henning Carlsen Benny Andersen
| starring       = Karl Stegger
| music          = 
| cinematography = Henning Kristiansen
| editing        = Henning Carlsen
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}
 Best Foreign Language Film at the 45th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Karl Stegger - Søren
* Birgitte Bruun - Caja (as Birgitte Price)
* Otto Brandenburg - Lasse
* Jesper Langberg - Svend
* Ingolf David - Ib
* Lone Lindorff - Elly
* Gyrd Løfquist - Café-ejeren
* Lene Maimu - Annie
* Lene Vedsegård - Ragnhild
* Ellen Margrethe Stein - Ibs mor
* Birgit Conradi - Lasses kone Lone
* Hans W. Petersen - Hr. Hansen
* Ebba Amfeldt - Ældre Dame
* Elin Reimer - Sørens kone Gerda
* Martin Lichtenber - Søren og Gerdas søn Jens

==See also==
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 