List of lesbian, gay, bisexual or transgender-related films of 1999
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1999. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1999==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|$30|| 1999 ||   ||  || Short, comedy||
|- 24 Nights|| 1999 ||   ||  || Comedy||
|-
|After Stonewall|| 1999 ||   ||  || Documentary||
|-
|Aimée & Jaguar|| 1999 ||   ||  || Romantic, war, drama||
|-
|All About My Mother|| 1999 ||   ||    || Drama||
|-
|Amor nello specchio|| 1999 ||   ||   || Drama||
|-
|Amic/Amat|| 1999 ||   ||   || Drama||
|-
| || 1999 ||   ||   || Comedy, Science fiction||
|-
|Beau travail|| 1999 ||   ||  || Drama||
|-
|Being John Malkovich|| 1999 ||   ||  || Fantasy, comedy, drama||
|-
|Better Than Chocolate|| 1999 ||   ||   || Romantic, comedy||
|-
| || 1999 ||   ||  || Drama||
|-
| || 1999 ||   ||    || Crime, drama||
|- Boys Dont Cry|| 1999 ||   ||  || Crime, drama||
|-
|Burlesk King|| 1999 ||   ||   || Drama||
|-
|But Im a Cheerleader|| 1999 ||   ||  || Comedy, drama||
|-
|Chutney Popcorn|| 1999 ||   ||  || Comedy, drama||
|- Chill Out|| 1999 ||   ||  || Drama||
|-
|Cruel Intentions|| 1999 ||   ||  || Drama, thriller||
|-
|Le derrière|| 1999 ||   ||  || Comedy||
|-
| || 1999 ||   ||     || Historical drama||
|-
|Emporte-moi|| 1999 ||   ||      || Drama||
|-
|Flawless (1999 film)|Flawless|| 1999 ||   ||  || Comedy, drama||
|-
|Floating (film)|Floating|| 1999 ||   ||  || Romantic, drama||
|- Full Blast|| 1999 ||   ||  || Drama, music||
|-
|Gendernauts - Eine Reise durch die Geschlechter|| 1999 ||   ||    || Documentary ||
|-
|Go (1999 film)|Go!|| 1999 ||   ||  || Comedy, Crime||
|-
|Happy, Texas (film)|Happy, Texas|| 1999 ||   ||  || Crime, Comedy||
|-
|Hit and Runway|| 1999 ||   ||  || Comedy||
|-
|Lola and Billy the Kid|| 1999 ||   ||     || Drama||
|- Memento Mori|| 1999 ||  , Min Kyu-dong ||   || Horror, Romantic, drama||
|-
|Oi! Warning|| 1999 ||  , Dominik Reding ||  || Drama||
|- Rites of Passage|| 1999 ||   ||  || Drama, thriller||
|- Second Skin|| 1999 ||   ||  || Romantic, drama||
|-
|See You in Hell, My Darling|| 1999 ||   ||  ||Drama||
|-
|Skin Gang|| 1999 ||   ||         || Drama, adult || aka Skin Flick
|-
|Sleeping Beauties|| 1999 ||   ||  || Short||
|-
|Speedway Junky|| 1999 ||   ||  || Drama||
|-
|Taboo (1999 film)|Taboo|| 1999 ||   ||       || Drama|| aka Gohatto
|-
| || 1999 ||   ||  || Crime, Drama, Thriller||
|-
|Trick (film)|Trick|| 1999 ||   ||  || Romantic, comedy||
|- Why Not Me?|| 1999 ||   ||       || Comedy||
|}

 

 
 
 