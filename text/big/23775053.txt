Trailer Park Boys: Countdown to Liquor Day
{{Infobox film
| name           = Trailer Park Boys: Countdown to Liquor Day
| image          = Tpb2movie.jpg
| caption        = Promotional poster
| director       = Mike Clattenburg
| producer       = Mike Clattenburg Barrie Dunn Michael Volpe Mike Smith Timm Hannebohm
| based on       =  
| starring       = Robb Wells John Paul Tremblay Mike Smith John Dunsworth Jonathan Torrens Patrick Roach Lucy DeCoutere Sarah E. Dunsworth Barrie Dunn Tyrone Parsons Jeanna Harrison  
| music          = Blain Morris
| cinematography = Ted McInnes 
| editing        = Roger Mattiussi
| studio         = Trailer Park Productions Topsail Productions
| distributor    = Alliance Films
| released       =  
| runtime        = 102 minutes
| country        = Canada
| language       = English
| budget         = Canadian dollar|C$6.6 million   
| gross          = Canadian dollar|C$3,037,011 
}} Say Goodnight Mike Smith) as they return to a life of crime after being released from prison.

Trailer Park Boys: Countdown to Liquor Day premiered in   nomination for  , which was released in Canada on April 18, 2014.

==Plot== Say Goodnight Mike Smith) are released from prison.  Upon returning to Sunnyvale Trailer Park, Bubbles finds that all of his kittens have been sent to an animal shelter and that he will need to pay thousands of dollars in order to have them released; secondly, Ricky and Julian successfully rob a liquor store, but Julian uses all the money to buy a car and refurbish his trailer into an Automobile repair shop|auto-body shop. Rickys plan is to pass his Grade 12 exam, which he feels will open doors of opportunity for him.

Jim Lahey (John Dunsworth) is sober and has opened up a posh new trailer park while attempting to demolish Sunnyvale. His only problem is that his new sewer line will have to run through Julians lot; he tries to convince Julian to move.  One sip of beer causes him to revert to heavy drinking. After Randy (Patrick Roach) leaves him and moves out of their trailer and on to Julians deck, Lahey goes on a liquor-fueled rampage. Meanwhile, the boys experience failure: Julians auto-body business is failing, Ricky fails his Grade 12 exam, and Bubbles kittens remain in the animal shelter after a failed attempt to rescue them which results in Tyrones (Tyrone Parsons) arrest.
 cannabis growing operation and also demolishes Julians trailer in an attempt to get Julian to move. However, Laheys plan backfires as Julian has insured his trailer for $28,000. With nothing left to live for and his dream of operating his new posh trailer park ruined by Julians refusal to move, Lahey loses his sanity.

The boys decide to rob a bank disguised as security guards making an armored-car transport as a last resort while Julian waits for the insurance check to save Bubbles kittens. They succeed in obtaining the money from the vault but they are foiled by Lahey showing up at the last minute. Lahey threatens to commit suicide by threatening to jump off the roof of the bank. After Julian prevents this, the real security guards show up. Ricky, Julian, and Bubbles are arrested and sent back to prison after a car chase with the police and security guards. They receive a short two-week sentence due to their arresting officer being drunk but request not to be filmed anymore.  Bubbles kittens are saved through a therapy animal program at the prison where the cats are released and spend time with the inmates. Bubbles secures a date with the animal control worker who saved his kittens, and the boys presumably return to Sunnyvale, with Julian building a new trailer with the insurance money.

Meanwhile, Lahey is drunk in Cuba, having absconded with the stolen money. Even though he is now rich, he is shown to still be miserable, because he has nobody to share his wealth. J-Roc (Jonathan Torrens) is also shown to have finally reached his goal of becoming a famous rapper, as he has a released new record, and is on stage in front of a large, enthusiastic crowd.

==Cast==
 
 
* Robb Wells as Ricky
* John Paul Tremblay as Julian Mike Smith as Bubbles
* John Dunsworth as Jim Lahey
* Patrick Roach as Randy
* Jonathan Torrens as J-Roc
* Lucy DeCoutere as Lucy
* Sarah E. Dunsworth as Sarah
* Barrie Dunn as Ray
* Jeanna Harrison as Trinity
* Tyrone Parsons as Tyrone
* Mary Fay Coady as Jenny
* Jacob Rolfe as Jacob Collins Richard Collins as Philadelphia Collins
* Alex Lifeson as Undercover Prostitute #1
* Brian Vollmer as Randys $15 John
* Sam Tarasco as Sam Losco
* Bernard Robichaud as Cyrus
* Jim Swansburg as Detective Ted Johnson
* George Green as Officer George Green Nobu Adilman as Dennis Mio Adilman as Terry
 

==Production== Say Goodnight to the Bad Guys", the final television special that concluded the series.    As with the previous film, Countdown to Liquor Day sees the cast from the television series reprise their respective roles.

Clattenburg officially announced plans to end the Trailer Park Boys franchise in November 2008.  He initially wanted to end the series after the fifth season. Due to popular demand from fans of the series, Clattenburg decided to produce two more seasons for the show.  When asked why he chose to end the franchise, Clattenburg stated, "There comes a time Id like to do more new stuff and develop in different ways."     Filming took place on location in Nova Scotia, Canada.   

==Release==
Trailer Park Boys: Countdown to Liquor Day was released in Canadian theatres on September 25, 2009. Prior to its theatrical release, the film held its world premiere in Halifax Regional Municipality|Halifax, Nova Scotia at the 29th Atlantic Film Festival, where it won an award for Best Sound Design.   

===Reception===
Prior to its domestic theatrical release, Countdown to Liquor Day divided film critics.  Linda Bernard of the Toronto Star wrote that the film was "a celluloid celebration of everything that boys love about being boys; from running around in their underpants, to setting stuff on fire, demolishing structures with heavy machinery and eating fried chicken for breakfast."     Stephen Cole of The Globe and Mail awarded the film three stars out of four, writing, "Trailer Park Boys succeeded because they were good company. So they are again in their second movie."    Steve Newton of Straight.com stated the film was "the funniest Canadian movie ever made."   
 Now Magazine wrote in his negative review, "Even the fans might wonder where the laughs are this time around. Heads get shaved, stuff gets smashed and drinks get drunk, but nothing has much of a payoff."    Jay Stone of The StarPhoenix wrote that the films "shenanigans run out of steam in a hurry, a problem that Countdown to Liquor Day tries to counteract with an episodic structure that flies through several stories, each of them cruder than the last."   

===Box office===
Trailer Park Boys: Countdown to Liquor Day grossed $1,396,229 on its opening weekend, placing 12th place at the box office.     The film dropped 65.33 percent during its second week, earning only $484,072.    The film dropped an additional 45.85 percent in its third week, grossing only $262,129 at thirty-second place.   In total, Countdown to Liquor Day has grossed $2,941,985 in Canada.   

===Home video releases===
In Canada, Countdown to Liquor Day was released in DVD and Blu-ray disc formats on December 22, 2009.  Special features for the film include deleted scenes, extended takes, lost interviews, and behind-the-scenes footage.    In the United States, the film was released in the same home video formats on February 23, 2010.  

==Sequel==
In May 2012, Mike Clattenberg announced on his  , will be released in Canada on April 18, 2014.   

==Accolades==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 2px #aaa solid; border-collapse: collapse; font-size: 90%;" 
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center" 
! Award
! Category
! Recipient
! Result
|-
|rowspan=1|30th Genie Awards     Genie Award Best Performance by an Actor in a Supporting Role John Dunsworth Nominated
|-
|rowspan=1| 29th Atlantic Film Festival  Best Sound Design Brian Power Won
|-
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 