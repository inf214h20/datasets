One in the Chamber
{{Infobox film
| name = One in the Chamber
| image = OneInTheChamber.MoviePoster.jpg
| image_size =
| caption = Official movie poster
| director = William Kaufman
| producer = Justin Bursch Brad Krevoy Patrick Newall
| writer = Derek Kolstad Benjamin Shahrabani
| starring = Cuba Gooding Jr. Dolph Lundgren
| music = John Roome
| cinematography = Mark Rutledge
| editing = Russell White Jason Yanuzzi
| studio = Mediapro Studios Motion Picture Corporation of America
| distributor = Anchor Bay Entertainment
| released =  
| runtime = 91 minutes
| country = United States
| language = English
| budget = $6,000,000 
| gross =
}} The Hit List. The film was released on direct-to-video|direct-to-DVD in the United States on August 21, 2012.

==Plot==
After the fall of Communism, criminals from the USA flooded into Eastern Europe where they established a crime syndicate in a land where authorities were powerless and the laws were replaced by the crime. Dealers of weapons and drugs have turned Prague into their headquarters where they could make millions of dollars by selling weapons to various gangs.

Ray Carver (Cuba Gooding Jr.) is an assassin who works for two rival mafia families. After Carver fails to assassinate Demyan Ivanov (Louis Mandylor), one of the crime bosses he frequently works for, he decided to kill the brother of the other crime boss, Mikhail Suverov. Mikhail is angry and wants to kill Ray Carver to avenge his brothers death, so he calls Aleksey "The Wolf" Andreev (Dolph Lundgren), a legendary Russian hitman who is rumoured to be fictional. Aleksey is out for Carver, but soon they both realize that theyre in the middle of a huge gang war. So they decide to team up and kill every member of the mafia in the Pragues criminal underworld.

==Cast==
* Cuba Gooding Jr. as Ray Carver
* Dolph Lundgren as Aleksey "The Wolf" Andreev
* Claudia Bassols as Janice Knowles
* Andrew Bicknell as Mikhail Suverov
* Catalin Babliuc as Live
* Louis Mandylor as Demyan Ivanov
* Leo Gregory as Bobby Suverov
* Lia Sinchevici as Mila
* George Remes as Gregori
* Alin Panc as Vlad Tavanian Billy Murray as Leo Crosby
* Florin Roata as Junior
* Alexandra Murarus as Nadia
* Aaron McPherson as Peter
* Andrei Ciopec as Waiter
* Bogdan Uritescu as Nicholai
* Jimmy Townsend as Ivan
* David Menina as Matous
* Bogdan Farkas as Goon #1
* Justin Bursch as Crony #1
* Patricia Poienaru as Juliana
* Slavi Slavov as Goon #2
* Annalee Gooding as Bus Passenger
* Zane Jarcu as Crony #2

==Development==
Billy Murray was confirmed to joined the cast on July 4, 2011.  The trailer for the film was released on June 27, 2012.  The first clip from the film was released on July 23, 2012. 

==Reception==
The film is received mildly positive reviews. Movie Ramblings said "One In The Chamber is a perfectly serviceable thriller – just don’t expect too much."  Very Aware said "It’s certainly not the worst thing you’ll ever put in your Blu-ray player, but put it in your Blu-ray player you should."  The Other View said "At best, give it a rental before you buy, but I think it’s worth it."  Weve Got This Covered said "is a good example of how to make a good straight-to-DVD film." 

==Production==
It is set and filmed in the Czech Republic, Eastern Europe and Brasov, Romania in 25 days on July 7 and August 1, 2011.

==Home media== Region 1 on August 21, 2012 and Region 2 on August 27, 2012, it was distributed by Anchor Bay Entertainment.

==References==
 

 
 
 
 
 
 
 
 
 
 
 