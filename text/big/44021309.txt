Manyasree Viswamithran
{{Infobox film 
| name           = Manyasree Viswamithran
| image          =
| caption        = Madhu
| producer       = Madhu
| writer         = Kainikkara Kumarapillai
| screenplay     = Kainikkara Kumarapillai Madhu Sheela Jayabharathi Kaviyoor Ponnamma Shyam
| cinematography = U Rajagopal
| editing        = G Venkittaraman
| studio         = Uma Arts
| distributor    = Uma Arts
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, Madhu . The film stars Madhu (actor)|Madhu, Sheela, Jayabharathi and Kaviyoor Ponnamma in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
  Madhu as Marthandan Thampi
*Sheela as Kusumam
*Jayabharathi as Padmam
*Kaviyoor Ponnamma as Bhagiradhiyamma
*KPAC Lalitha as Naani
*Adoor Bhasi as Sankaran
*Sam
*Sankaradi as Vakkeel
*KR Suresh
*Bahadoor as Balachandran
*Kovai Rajan
*MG Soman as Ramesh Meena as Aluvalia
*Narendran
*Sudevan
*Usharani
 

==Soundtrack== Shyam and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadaan Varoo Vegam || LR Eeswari, KP Brahmanandan, ST Sasidharan, Kumari Jayalakshmi || P. Bhaskaran || 
|-
| 2 || Ha Sangeethamadhura Naadam || P Jayachandran, ST Sasidharan, Kumari Jayalakshmi || P. Bhaskaran || 
|-
| 3 || Kanavu Neythoru || S Janaki, KP Brahmanandan || P. Bhaskaran || 
|-
| 4 || Kettille Kottayathoru || P. Madhuri || P. Bhaskaran || 
|-
| 5 || Pandoru Naalil || P Susheela || P. Bhaskaran || 
|-
| 6 || Saarasaayi Madanaa || LR Eeswari || P. Bhaskaran || 
|-
| 7 || Vaadiveena Poomala || P. Madhuri || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 