Without a Trace (film)
{{Infobox Film
  | name     = Without a Trace
  | image    = Without a Trace.jpg
  | director = Stanley R. Jaffe		
  | producer = Stanley R. Jaffe
  | writer   = Beth Gutcheon, based upon her novel Still Missing
| starring = {{plainlist|
* Kate Nelligan
* Judd Hirsch
* David Dukes
* Stockard Channing
}} 
  | music    = Jack Nitzsche John Bailey
  | editing  = Cynthia Scheider
  | distributor = 20th Century Fox
  | released = February 4, 1983 (United States) July 5, 1986 (Japan)               
  | runtime  = 120 min
  | country = United States
  | language = English 
  | gross    =  $9,632,062 
  }}
Without a Trace is a 1983 dramatic film.  It is based on the novel Still Missing by Beth Gutcheon. The film stars Kate Nelligan, Judd Hirsch, David Dukes and Stockard Channing.

==Plot== English professor Brooklyn brownstone with her six-year-old son Alex (Danny Corkill). One March morning, Susan sees Alex off to school, which is only two blocks away. Alex turns to wave to his mother, then disappears around the corner.

Susan returns home after a day of teaching, and becomes increasingly alarmed when Alex is late coming home. She calls her friend and neighbor Jocelyn Norris (Channing), whose daughter is a classmate of Alexs, and finds out that Alex never got to school. The New York City Police Department is immediately called and officers descend on the townhouse, led by Lieutenant Al Menetti (Hirsch). Susan is questioned closely on all aspects of her life and her sons, and police zero in on Susans estranged husband Graham (Dukes), a professor at New York University who hasnt been seen for hours. When Graham finally turns up, he produces an alibi, ruling him out as a suspect.

Susans case generates a lot of attention from the New York City media, with citizens helping in the search by distributing posters. Susan is initially criticized for allowing her son to walk to school by himself. Susan also takes a polygraph test that clears her as a suspect. Numerous leads are checked out, including several reports that Alex may have been seen in the back seat of a blue 1965 Chevy. A psychic is also called in, but each lead fizzles.

The investigation drags on, and Graham is at odds with Menetti after budget cuts force Menetti to dismantle the command center in Susans apartment and run the case from the precinct. Menettis attention is soon diverted to other cases, but the Selky case is always a priority. At one point, Graham takes matters into his own hands after he gets a ransom call. He heads to a location the caller directs him to, but is cornered and beaten, prompting a hospital stay.
 gay Philippe was picked up with a 14 year old male prostitute. Susan visits Philippe in jail, and he tells her that the bloody underpants came about when he used them to stop bleeding after he cut himself washing dishes in Susans house. Convinced Philippe is innocent, Susan tries to persuade Menetti to drop the charges, but he refuses, citing physical evidence he wont discuss.

The renewed media coverage generated by Philippes arrest dies down, and Susan is facing increased pressure to drop the matter and accept that Alex could be dead. Susans feelings come to a boiling point when a magazine cancels an article she wrote about Alex (because a gay man was arrested) and even her friend Jocelyn tells her its time to give up.

Susan tries to resume her normal routine, although she never loses faith that her son is alive. One day, she receives a phone call from a woman in Bridgeport, Connecticut named Malvina Robbins (Louise Stubbs), who says Alex is alive and living with neighbors. Menetti tells Susan that he has also heard from Robbins, but Bridgeport police told him the woman is just a crank, or in Menettis words, "a lonely old booby". The investigation is closed, he says, and Philippe goes on trial within weeks.

On a day off, Menetti is taking a drive with his son (David Simon). When he sees a sign for Bridgeport, Connecticut, he decides to check out the lead personally. He recruits his young son as his partner on the case. Once he is sure that the lead is false, Menetti hopes to browbeat Robbins from ever disturbing Selky ever again. When Menetti arrives at Robbins address, he is shocked to see a blue Chevy (in which witnesses had reported seeing Alex) parked in the driveway of the neighboring house. Realizing that Robbins was telling the truth, he uses her phone to contact the Bridgeport police. They find Alex alive and unharmed. His kidnapper wanted the boy to care for his disabled sister who lives in the house.

Menetti drives Alex back to New York with a huge police escort (which grows with each jurisdiction it passes through), and the New York media is tipped off that hes been found, converging on Susans Brooklyn house. Susan returns from grocery shopping in time to see Alex stepping out of Menettis car. In front of delighted bystanders and reporters, mother and child are reunited.

==Production==
The movies screenplay was written by novelist and screenwriter Beth Gutcheon, who kept the film relatively faithful to her novel Still Missing, a work of fiction.  The one glaring difference between the book and the film is that the book was set in Boston, Massachusetts|Boston, while the film was set and filmed in New York.  The film was originally supposed to be titled Still Missing, but was changed by the studio to avoid confusion with the 1982 film Missing (film)|Missing.

The film was released in North America on February 4, 1983.

==References==


 

==External links==
*  
*  

 
 
 
 
 
 
 
 