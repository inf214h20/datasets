Return to Treasure Island (1954 film)
{{Infobox film
| name           = Return to Treasure Island
| image_size     = 
| image	=	Return to Treasure Island FilmPoster.jpeg
| caption        = 
| director       = Ewald André Dupont
| producer       = Jack Pollexfen (producer) Aubrey Wisberg (producer)
| writer         = Jack Pollexfen (writer) Robert Louis Stevenson (characters) Aubrey Wisberg (writer)
| narrator       = 
| starring       = See below
| music          = Paul Sawtell William Bradford
| editing        = Fred R. Feitshans Jr.
| studio = World Pictures
| distributor    = United Artists Eclipse Films (2000) (USA) BFS Video (2003) (USA) Alpha Video Distributors (2004) (USA)
| released       = 1954
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Return to Treasure Island is a 1954 American film directed by Ewald André Dupont.

== Cast ==
*Tab Hunter as Clive Stone
*Dawn Addams as Jamesina "Jamie" Hawkins
*Porter Hall as Maximillian "Maxie" Harris
*James Seay as Felix Newman
*Harry Lauter as Parker
*William Cottrell as Cookie
*Lane Chandler as Capt. Cardigan Henry Rowland as Williams
*Dayton Lummis as Capt. Flint Robert Long as Long John Silver
*Ken Terrell as Thompson

== See also ==
* Long John Silver (film)|Long John Silver (film), a 1954 Australian film directed by Byron Haskin starring Robert Newton as Long John Silver

== External links ==
* 
* 

 
 
 

 
 
 
 
 
 

 