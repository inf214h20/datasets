On the Line (2011 film)
On Frank Wolf that investigates the risks and consequences associated with the proposed Enbridge Northern Gateway Pipelines project. The film is set in the context of a 2,400&nbsp;km biking, hiking, rafting and kayaking journey that two friends embark on from the Alberta oil sands to the British Columbia coast.  As the pair travel the proposed pipeline route,  they encounter a broad cross-section of people who voice their perspective on the issues associated with the project. It won the Spirit of Action Prize at the 2012 Santa Cruz Film Festival  and was chosen for the VIFF Selects section at the 2011 Vancouver International Film Festival.  It airs on CBCs documentary (TV channel) in Canada multiple times through 2015.  The film features music by Peirson Ross,  Terry Jacks, Chilliwack (band)|Chilliwack, Kola Collective, Phontaine,Tessa Amy, and Eric Stanger.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 
 