What's the Matador?
{{Infobox Film |
  | name           = Whats the Matador?
  | image          = Matadorlobbycard.jpg
  | caption        = 
  | director       = Jules White
  | producer      = Jules White Jack White Harry Burns John Tyrrell Cy Schindell Eddie Laughton Don Zelaya
  | cinematography = L. William OConnell 
  | editing        = Jerome Thoms
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 16"
  | country        = United States
  | language       = English
}}

Whats the Matador? is the 62nd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

== Plot ==
  Harry Burns). In an act of revenge on Curly for flirting with Dolores, José pays the bullring attendants to release a live bull into the ring. Moe and Larry flee the ring, but Curly is unaware of the switch. He eventually head-butts the wild animal, and is paraded out of the ring to the rousing cheers of "Olé, Americano!"

== Production notes ==
Whats the Matador? was filmed on August 14-18, 1941.    It was remade in 1959 as Sappy Bull Fighters, using minimal stock footage from the original. 
 Blood and Sand.    While bullfighting is the reference, the two stories otherwise have nothing in common.

The Stooges have a frustrating exchange with an old Mexican local (Don Zelaya) when they ask if he has seen Dolores. Though his lengthy Spanish response seems unintelligible to them, he actually says the following:
*"Go down the street three blocks, turn right and go two more blocks, turn right, cross the square, and turn right. Walk down that street until you find an alley, but keep walking. Go down that street until you find another alley, but do not enter that alley. Turn right. There you will find a river. Do me a favor: jump into the river and drown yourself!" 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 