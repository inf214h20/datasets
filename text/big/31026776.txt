Simón Bolívar (1942 film)
{{Infobox film
| name           =Simón Bolívar 
| image          = 
| image size     =
| caption        =
| director       = Miguel Contreras Torres
| producer       = Miguel Contreras Torres   Jesús Grovas
| writer         = Miguel Contreras Torres    
| narrator       =
| starring       = Julián Soler   Marina Tamayo   Carlos Orellana   Margarita Mora
| music          = Federico Ruiz
| cinematography = Jack Draper Mario González
| distributor    = 
| released       = July 15, 1942
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Mexican historical historical drama biopic of the revolutionary Simón Bolívar who fought to end Spanish rule over much of Latin America.

==Cast==
* Julián Soler - Simón Bolívar
* Marina Tamayo - Manuela Sáenz
* Carlos Orellana - General José Antonio Páez
* Margarita Mora - Josefina Machado
* Anita Blanch - Fanny du Villars
* Domingo Soler - General Jacinto Lara
* Pedro Armendáriz - General Briceño Méndez
* Julio Villarreal - General Pablo Morillo
* Carlos López Moctezuma - General José Tomás Boves
* Francisco Jambrina - Mariscal Antonio José de Sucre
* Miguel Inclán - Sargento Pérez Carmen Molina - María Teresa del Toro
* Tito Junco - General Francisco de Paula Santander
* Alberto G. Vázquez - General José de San Martín
* Víctor Manuel Mendoza - La Mar
* Víctor Urruchúa - General José María Córdova
* Miguel Tamayo - Generalisimo Francisco de Miranda
* Eduardo González Pliego - General Carlos Soublette
* Felipe Montoya - General Mariano Montilla
* Ricardo Beltri - Canónigo Córtes de Madariega
* Alberto Espinoza - General Jose Félix Rivas
* Manuel Dondé - General Manuel Piar
* Manuel Fábregas - Fernando Bolívar
* Francisco Laboriel - Pedro Camejo
* Carlos L. Cabello - General William Miller
* Arturo Soto Rangel - Marqués y Coronel del Toro Max Meyer - Barón Alejandro de Humboldt
* Feliciano Rueda - Doctor Prospero Reverand
* Ricardo Carti - Bacuero
* Ernesto Finance - Obispo Estevez
* Luis G. Barreiro - General Barreiro
* José Pidal - General Domingo Monteverde
* Ramón G. Larrea - La Torre
* Victorio Blanco - Renovales
* Consuelo de Alba - Anita L. Blanchard
* Margarita Cortés - María Antonia Bolívar
* Mercedes Cortés - Juana Bolívar
* Conchita Gentil Arcos - Señora del Toro

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 