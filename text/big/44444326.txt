A Man - That's All
 
 
{{Infobox film
  | name     = A Man - Thats All
  | image    = 
  | caption  =  Alfred Rolfe		
  | producer =   
  | writer   = 
  | based on = 
  | starring = Charles Villiers
  | music    = 
  | cinematography = 
  | editing  = 
  | studio = Australasian Films
  | distributor = 
  | released = 31 January 1916 (Melbourne) 
  | runtime  = Four reels 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
A Man - Thats All (1916) is an Australian war film directed by Alfred Rolfe for Australasian Films.

==Plot==
Captain Dudley West ("the Man") is engaged to Doreen Drummond ("the Girl"). While out motoring, he saves an escaped convict, Jim Slade, who has busted out of prison to visit his dying wife.

The Girls brother Lt Cyril Drummond cheats at cards in a game at a gambling den and the Man takes the blame in order to save him. He loses his position in society and Doreen breaks off with him. He takes up drinking and gambling and emigrates to Australia and becomes a drunk. He runs into Jim Slade in a Chinese gambling den and together they decide to enlist in the army.

The Man takes part in a mission behind enemy lines which results in him saving the life of Jim and Cyril, before being wounded. He escapes through the help of an Australian raiding party.

The Man is sent to Egypt where he meets the Girl who is working as a nurse in hospital. Jim tells the Girl the truth before dying. The Man and the Girl are reunited.    

The chapter headings were:
*boy starts at cards
*on the downward path
*no money, no friends
*chance meeting with convict at Sydney
*decides to enlist
*the landing at Gallipoli
*in the trenches
*the race for life
*the last cartridge
*reconciliation 

==Cast==
*Robert Inman as the Man 
*Maisie Carter as the Girl Charles Villiers as the Crook
*Percy Walshe as the Girls Father

==Production==
The film was made with the co operation of the Department of Defence.  Filming took place in October 1915. 

No writer was credited on the film.   

==Reception==
The film appears to have premiered in Melbourne on 31 January 1916. It was screened until the end of the war.  

The film was used as a recruiting tool, with army officers speaking at screenings.   It was one of four films the Department of Defence made available to councils for recruiting purposes, the others being Will They Never Come?, The Hero of the Dardenelles and Australians in Egypt. 

===Critical===
A critic from the Mirror described the film as "a good picture, and is nearer to the American style of production than any other Australian film I have seen. The settings, though plain, look well and add to the general value." 

The Motion Picture News said the author "deserves credit for developing an old plot in an entirely new way... the battle scenes, though not elaborate, are convincing and are perhaps the most realistic of any subject yet made in Australia. The settings, direction and photography are of high class order." 

The Moving Picture World called it "a very poor production, and not up  to the standard set by some home-made productions lately. The chief fault is in the story, as the acting, photography  and direction leave little to be desired. The settings are also particularly good." 

Punch said that "the Australian parts of the photo play are slight" but the war sequences were "realistic". 

==References==
 
 

 
 
 
 