Isadora
 Isidore}}
 
 
{{Infobox film
| name           = Isadora
| image          = Isadora 1968 film poster.jpg
| caption        = Film poster by Reynold Brown
| director       = Karel Reisz Raymond Hakim Robert Hakim
| writer         = Melvyn Bragg (adaptation & screenplay) Clive Exton (screenplay) Margaret Drabble (additional dialogue) Isadora Duncan (book, My Life) Sewell Stokes (book, Isadora Duncan: An Intimate Portrait John Fraser
| music          = Anthony Bowles (dance music) Maurice Jarre
| cinematography = Larry Pizer
| editing        =
| distributor    = Universal Pictures
| released       = 1968
| runtime        = Original Version  168 Min Directors Cut  153 Min Edited Version 128 Min  131 Min
| country        = UK / France NSFC Awards  for Best Actress (Vanessa Redgrave)
| language       = English
| budget         = $1.7 million Alexander Walker, Hollywood, England, Stein and Day, 1974 p345 
| gross = $1.25 million (US/ Canada rentals) 
| preceded_by    =
| followed_by    =
|}}
Isadora (also known as The Loves of Isadora) is a 1968 biographical film which tells the story of celebrated American dancer Isadora Duncan. It stars Vanessa Redgrave, James Fox, and Jason Robards.

The film was adapted by Melvyn Bragg, Margaret Drabble, and Clive Exton from the books My Life by Isadora and Isadora, an Intimate Portrait by Sewell Stokes. It was directed by Karel Reisz.
 Best Actress.   

==Plot==
In 1927, Isadora Duncan has become a legend as the innovator of modern dance, a temperamental bohemian, and an outspoken advocate of free love. Now past 40, she lives in poverty in a small hotel on the French Riviera with her companion Mary Desti and her secretary Roger, to whom she is dictating her memoirs. As a young girl in California, Isadora first demonstrates her disdain for accepted social standards by burning her parents marriage certificate and pledging her dedication to the pursuit of art and beauty. In 1896, she performs under the name of Peppy Dora in a rowdy music hall in Chicago and publicly embarrasses the theatre manager into paying her $300 so that she can take her family to England. Modeling her free-form style of dance and costume after Greek classicism, she rapidly acquires international acclaim.

In Berlin, she meets her first love, Gordon Craig, a young stage designer who promises her that together they will create a new world of theatre. After bearing the already-married Craig a daughter, Isadora moves to Paris and meets Paris Singer, a millionaire who lavishes gifts upon her and later buys her an enormous estate for her to open a School for Life, where only beauty and simplicity are taught.

Following the birth of a son, Isadora returns to England with Singer but becomes bored with her quiet life and enters into an affair with her pianist, Armand. A short time later, both of her children are drowned when their chauffeur-driven car plunges off a bridge into the Seine. Broken by the tragedy, Isadora leaves Singer and wanders about Europe until in 1921 she receives an offer to open a dancing school in the Soviet Union.

Unaffected by the countrys poverty, she develops a strong rapport with the peasantry and has a passionate affair with Sergei Essenin, a volatile poet whom she marries so that he can obtain a visa to accompany her to the United States. Essenins outrageous behaviour turns a press conference into a shambles, however, and US anti-Bolshevist sentiment turns to open hostility when Isadora bares her breasts during a dance recital in Boston. Following the disintegration of her marriage, she returns to Nice to write her memoirs. Impulsively selling her possessions to open a new school in Paris, Isadora goes to a local cafe to celebrate and spots Bugatti, a handsome Italian whom she has been admiring for several days. She goes for a drive with him in his sports car, and as they roar along a road by the sea, Isadoras long chiffon scarf catches in the spokes of a wheel and strangles her.

==Cast==
* Vanessa Redgrave as Isadora Duncan John Fraser as Roger
* James Fox as Gordon Craig
* Jason Robards as Singer
* Zvonimir Crnko (billed as Ivan Tchenko) as Sergey Esenin
* Vladimir Leskovar as Bugatti
* Cynthia Harris as Mary Desti
* Bessie Love as Mrs. Duncan
* Tony Vogel as Raymond Duncan
* Libby Glenn as Elizabeth Duncan
* Ronnie Gilbert as Miss Chase
* Wallas Eaton as Archer
* Nicholas Pennell as Bedford
* John Quentin as Pim
* Christian Duvaleix as Armand
* Pauline Collins as extra sitting behind Isadora at outside café (uncredited)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 