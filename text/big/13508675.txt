Blood Rage
 
 
{{Infobox film
| name           = Blood Rage
| caption        = 
| image	=	Blood Rage FilmPoster.jpeg
| director       = John Grissmer
| producer       = Marianne Kanter
| writer         = Bruce Rubin
| starring       = Louise Lasser Mark Soper
| music          = 
| cinematography = 
| editing        = 
| distributor    =  1987
| runtime        = 82 minutes
| country        = United States English
| budget         = 
}}

Blood Rage (Nightmare at Shadow Woods) is a 1987 slasher film written by Bruce Rubin and directed by John Grissmer. It was produced by Marianne Kanter and starred Louise Lasser.

==Plot==
Todd and Terry are twins. They are blonde, cute, bright and identical in every respect, with one exception. One of them is a murderer. This starts one night at a drive-in theater when a teenager was slaughtered in the back seat of his car while his girlfriend watched. Todd is found guilty for the heinous crime and is locked away in an Psychiatric hospital|asylum.

Years passed and Terry lives happily with his mother (Louise Lasser), who smothers him with enough love for two sons. All is fine until one Thanksgiving when they receive news that Todd escaped. Terry goes on a killing spree to ensure that Todd goes back to the asylum. His first kill is his mothers fiancée Brad, when he chops off his hand with a machete, before stabbing him to death.

Meanwhile, Dr. Berman and her assistant, Jackie, go out in search for Todd. Jackie meets a sticky end, when he is stabbed by Terry. Dr. Berman also suffers the same fate. Whilst in the woods looking for Todd, she comes across Terry, who cuts her in half with the machete, leaving her to die.

After the killings, Terry changes from his bloody T-shirt in to a vest. He then proceeds to the place where his new neighbour, Andrea, is babysitting a young newborn baby. They then begin a mini relationship, until the mother Julie and her date Bill come home. Whilst that was happening, a friend of Terrys, Karen (who Terry has feelings for), bumps into Todd thinking he is Terry. So when Todd announces himself to her, she panics and runs away, telling her friends.

Karen later tells Terry about seeing his brother. He then disappears into the night in search for him, whilst Karen, Gregg, Artie go back to Andreas house for a party. Terry tells Maddie that Todd has escaped. She starts to panic. Meanwhile Todd comes across Dr. Bermans split body and breakdown over it, trying to move the body back together. Back at the young born babys house, Julie and her date are looking forward to a night together - until Terry knocks on the door. Bill answers the door and is decapitated. Julie goes to check where Bill is and discovers his head hanging from a rope. Suddenly, Terry jumps out of nowhere and kills her.

Later on, Andrea and Gregg leave the party to play tennis. They are unaware that Terry is watching them. They both leave soon after and have sex on the diving board by the swimming pool. Terry surprises them and kills them. He later meets up with Artie and Karen. Artie goes off to find the now dead Gregg and Andrea. When getting in the car, he meets Todd, who asks him to help him get Terry as he is killing people. Artie doesnt believe him, that is until Todd holds a gun up at his head. Karen and Terry meanwhile have a sexual moment together. Artie knocks at the door and Terry answers it and sees Todd holding a gun at him. Todd later disappears. Artie and Terry go running off to find Todd, but Artie meets his maker when Terry stabs him in the neck with a carving fork.

Karen finds Terry and discovers his secret, when he attempts to kill her. He chases her all over the place, and in the process, she discovers Brads body. She runs to the young babys house and discovers the mothers corpse. She takes the baby and runs for her live when Terry enters the house. Whilst all that is happening, Maddie finally discovers Terrys secret when she finds his bloodied T-shirt in the bin. She goes in search of her fiancée Brad, only to discover his head split in two.

The final showdown between brothers comes at the swimming pool and they both battle for survival. They both tumble into the water, Todd becomes weak and almost drowns. Maddie then appears and shoots Terry - killing him. Maddie and Todd embrace. Although Maddie thinks she has killed Todd, and when Todd tell her who he really is, she picks up a gun and shoots herself in the head. Karen, shocked, runs away, whilst Todds fate is unknown as the police arrive making the audience wonder what will happen to him now.

==Cast==
*Louise Lasser: Maddy Simmons
*Mark Soper: Todd and Terry Simmons
*Marianne Kanter: Doctor Berman
*Julie Gordon: Karen Reed
*Jayne Bentzen: Julie
*Bill Cakmis: Maddys Date James Farrell: Artie Ed French: Bill
*William Fuller: Brad King
*Gerry Lou: Beth
*Chad Montgomery: Gregg Ramsey
*Lisa Randall: Andrea
*Doug Weiser: Jackie
*Dana Drescher: Little Girl
*Brad Leland: Drive-in Boy
*Rebecca Thorp: Drive-in Girl
*Ted Raimi: Condom Salesman

==Release==
Although the film was shot in 1983, it was given only a limited release theatrically in the United States by the Film Concept Group under the title Nightmare at Shadow Woods in 1987.  It was released on VHS by Prism Entertainment the same year under the title Blood Rage. 

===Alternate versions===
Nightmare at Shadow Woods (which was also the cable television title for this film) was heavily edited, abbreviating much of the gore, but it contained a swimming pool scene not found in the 1987 VHS Blood Rage version by Prism Entertainment.  The latter contains all of the gore and includes an early scene, missing from the Nightmare at Shadow Woods version, where Maddy visits Todd at the mental hospital.  The Nightmare at Shadow Woods version had a budget DVD release in 2004 by Legacy Entertainment, but as of September 2011 is out of print. A high definition release on Blu-Ray is planned for 2015 by Arrow Video in the UK/US. 

==References==
 

==External links==
*  
*  
*  
* http://www.bloodragethefilm.com/

 
 
 
 
 
 
 
 
 