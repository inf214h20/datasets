La bête lumineuse
 
{{Infobox film
| name           = La bête lumineuse
| image          = 
| caption        = 
| director       = Pierre Perrault
| producer       = Jacques Bobet
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Martin Leclerc
| editing        = Suzanne Allard
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 128 minutes
| country        = Canada
| language       = French
| budget         = 
}}

La bête lumineuse (English: The Shimmering Beast) is a 1982 Canadian documentary film directed by Pierre Perrault, about a group of hunters who gather annually to hunt moose near Maniwaki, Quebec. It was screened in the Un Certain Regard section at the 1983 Cannes Film Festival.   

==Cast==
* Louis-Philippe Lécuyer as Canotier
* Philippe Cross as Canotier
* Stéphane-Albert Boulais as Archer
* Maurice Chaillot as Archer
* Bernard LHeureux as Orignal
* Michel Guyot as Orignal
* Maurice Aumont as Chasseur dours
* Claude Lauriault as Chasseur dours

==References==
 

==External links==
* 
* , National Film Board of Canada (in French)

 
 
 
 
 
 
 
 
 
 

 
 