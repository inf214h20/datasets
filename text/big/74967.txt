Motion Painting No. 1
Motion Brandenburg Concerto no. 3, BWV 1048.  It is a film of a painting (oil on acrylic glass); Fischinger filmed each brushstroke over the course of 9 months. In 1997, this film was selected for inclusion in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==External links==
*  
* Excerpts from Fischingers writings about this film are at the Center for Visual Musics  . See their Fischinger bibliography for many other texts.
*  

==Further References==
* The original acrylic glass panels are at the Deutsches Filmmuseum in Frankfurt, Germany.
* The film itself can be purchased on CVMs  
*  , 2004)

 
 
 
 
 


 