Chronicle of a Disappearance
{{Infobox film
| name           = Chronicle of a Disappearance
| image          = Chronicle of a Disappearance film.jpg
| image_size     =
| alt            =
| caption        = Chronicle of a Disappearance DVD cover
| director       = Elia Suleiman
| producer       = Elia Suleiman
| writer         = Elia Suleiman
| narrator       =
| starring       = Elia Suleiman
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = United States:  International Film Circuit
| released       =  
| runtime        = 88 minutes
| country        = Israel, Palestine Russian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Chronicle of a Disappearance ( . Accessed October 12, 2009.  is a  . Published 1997. Retrieved August 22, 2009.  Dhat Productions produced the film.  The film features no real storyline or character arc. Suleiman plays himself returning to Israel and the West Bank after a long absence which is followed by a series of barely connected vignettes and sketches, which are intended to convey the feelings of restlessness and uncertainly from Palestinian statelessness. The films tone varies through these scenes such as "Nazareth Personal Diary", which has a light and domestic tone, and "Jerusalem Political Diary", which has a more ideological tone. 
 1996 Venice Film Festival, where it won the award for Best First Film Prize. 

== Plot ==
 Palestinians given their Palestinian state|statelessness. 
 vignettes include tap and fails to keep a cheap camel statuette from falling over on his shelves.  E.S. and the shop owner spend time sitting in front of the stop waiting futilely for tourists to stop by. A boat full of Arab men fish, as one of the men bashes various Palestinian families that his friend does not belong to while praising the one that his friend does belong to. Suleiman also interviews a Russian Orthodox cleric who rails against the tourists polluting the Sea of Galilee. 
 Palestinian film feeding back and he leaves the podium. The last section, "Jerusalem Political Diary, has a quicker narrative pace and a more overtly ideological message.  Absurd humor is evoked alongside feelings of anti-Israeli paranoia in the characters; for example, what first appears to be a terrorists hand grenade held by a Palestinian turns out to be a cigarette lighter. 

Suleiman discovers an  . Published October 2, 2006. Retrieved August 22, 2009.  The end comes after a long shot of Sulimans parents sleeping, with all the lights off and Israeli material playing on their television. 

==Cast==
* Elia Suleiman as E.S.
* Ola Tabari as Adan
* Nazira Suleiman as Mother
* Fuad Suleiman as Father
* Jamel Daher as Jamal, owner of the Holyland
* Juliet Mazzawi as The aunt
* Fawaz Eilemi as Abu Adnan
* Leonid Alexeenko as Priest
* Iaha Mouhamad as The writer

== Production ==
  immigrated to New York Canadian filmmaker Jay Salloum. Suleiman went on to direct two short films before moving to Jerusalem in 1994, working for Bir Zeit University. Gabriel, Judith.  . Al Jadid Magazine, Vol. 8, No. 41 (Fall 2002). Accessed October 12, 2009. 
 Russian languages are all spoken in the film. 

The cast includes Elia Suleiman himself as well as Fuad Suleiman, Nazira Suleiman, Ula Tabari, James Daher, Juliet Mazzawi, Fawaz Eilemi, Leonid Alexeenko, and Iaha Mouhamad.  Much of the cast are related to him. Adams, Sam.  . Philadelphia City Paper. Published February 12–19, 1998. Accessed October 12, 2009.  The company Dhat Productions produced the film along with assistance from the European Union Media Project Production Company, the Centre national de la cinématographie, the Fund for the Promotion of Israeli Quality Film, and the Independent Television Service. Assaf Ami, of Norma Productions, served as the executive producer. 

== Reception == Sundance and Museum of 1996 and Palestinian films to receive national release in the United States, which occurred in fall 1997. 
 artistic minimalism and called it a "shrewdly soft-voiced argument for peace".  Janet Maslin of The New York Times called it "quite remote" as well as "schematic and abstract". She also stated, " or every astute or revealing detail about a culture full of frustrations, there is liable to be a glimpse of someone falling asleep on a sofa or staring disconsolately into space."  Cinematic scholar Gönül Dönmez-Colin dedicated a chapter to the film in her 2007 book The Cinema of North Africa and the Middle East. She commented, "Much of Palestinian film deals with the liminality of loss and disappearance- of country, of the people, and of the self. In other Palestinian film, however, are these processes of love disappearance more beautifully captured than in Chronicle of a Disappearance."   

Richard Brody of The New Yorker labeled it "witty" and lauded its "graceful artistry and rhetoric". He also stated that Suleiman "constructs his film disingenuously around the politics that he omits".  All Movie Guide commented that " n his fragmented, personal, self-critical, and low-key way, Suleiman makes his point that the disappearance hes chronicling is that of the identity of his people."  Sam Adams of The Philadelphia City Paper stated that the film "succeeds because of aesthetics, not politics and avoids ideological commentary on the Israel-Palestinian conflict|conflict.  Critic Dennis Schwartz of Ozus World Movie Reviews panned the film. In particular, he cited its portrayal of both Nazareth and Jerusalem as part of Palestine despite the sovereign Israeli history in those cites as inappropriate and provocative. He also criticized its disjointed, non-linear structure. 

When asked about the international critical praise for the film by IndieWire, Suleiman commented:
 

===Home video===
 

== See also ==
*  
* List of Palestinian films Artistic minimalism Notable films in 1996

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 