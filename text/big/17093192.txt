Ways to Strength and Beauty
{{Infobox film
| name           = Ways to Strength and Beauty
| image          = Wege zu Kraft und Schönheitposter.jpg
| border         = 
| alt            = 
| caption        = German theatrical release poster
| director       = {{Plainlist|
* Nicholas Kaufmann
* Wilhelm Prager 
}}
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Jack Dempsey
* Jenny Hasselqvist
* Josef Holub
* Niddy Impekoven
* Tamara Karsavina La Jana
* Leni Riefenstahl
* Hertha von Walther
* Johnny Weissmuller
}}
| music          = Giuseppe Becce
| cinematography = {{Plainlist|
* Eugen Herich
* Friedrich Paulmann
* Friedrich Weinmann
}}
| editing        = 
| studio         = {{Plainlist|
* Kulturabteilung Universum Film (UFA)
}}
| distributor    = Oefa-Film Verleih
| released       =  
| runtime        = 100 minutes
| country        = Germany
| language       = Silent film, German intertitles
| budget         = 
| gross          = 
}}

Ways to Strength and Beauty  ( ) is a 1925 German silent film directed by Nicholas Kaufmann and Wilhelm Prager. The film was produced by Universum Film AG|Ufa-Kulturfilmabteilung of Weimar Germany.

The action was an idealized, somewhat naive approximation to the health and beauty in conformity with nature. The film offered a contrast to the rather hopeless living in the city of Berlin and other large cities of Germany during the twenties and became an immediate success quite from the beginning. Finally it  became the most popular and most important German kulturfilm of this period.

The film is best known as the first film to feature Leni Riefenstahl.

==Cast==
* Jack Dempsey  
* Jenny Hasselqvist
* Josef Holub
* Niddy Impekoven
* Tamara Karsavina
* La Jana
* Leni Riefenstahl
* Hertha von Walther
* Johnny Weissmuller  

== External links ==
*  

 
 
 
 


 