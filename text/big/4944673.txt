Lock Up (film)
{{Infobox film
| name           = Lock Up
| image          = Lock_up.jpg
| caption        = Theatrical release poster John Flynn Charles Gordon Lawrence Gordon Jeb Stuart Henry Rosenbaum
| starring       = Sylvester Stallone Donald Sutherland John Amos Darlanne Fluegel Frank McRae Sonny Landham
| music          = Bill Conti Gordon Company White Eagle Pictures Carolco Pictures
| distributor    = TriStar Pictures StudioCanal
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| cinematography = Donald E. Thorin
| editing        = Don Brochu Robert A. Ferretti Michael N. Knue Barry B. Leirer
| budget         = $24 million 
| gross          = $22,099,847 (US) 
}} prison  John Flynn. The film stars Sylvester Stallone and Donald Sutherland. The film was released in the United States on August 4, 1989.

==Plot==
  Gateway Prison run by Warden Drumgoole. Drumgoole explains that Leone will serve hard time—Leone was the only person to escape from Treadmore and did so on Drumgooles watch. Leone escaped because his mentor and friend was dying; Leone was refused even one hour to see him. Leone went to the press about the wardens treatment of his prisoners, resulting in Drumgooles transfer to Gateway and Leone serving in minimum security before his transfer.

Leone befriends fellow prisoners Dallas, Eclipse, and First-Base. The foursome refurbish a Ford Mustang, which Eclipse nicknames "Maybelline". After Leone reluctantly allows First-Base to start the car he refuses to turn it off and drives the Mustang out of the garage. Drumgoole has them watch as inmates destroy the car and Leone is subsequently sent to solitary confinement for six weeks. The warden orders that the letters sent to him from his girlfriend Melissa be stashed away but as Leone is released Braden, an honest prison guard, secretly delivers them to Leone.

Upon release, the warden wants an excuse to slap Leone with more time, so he allows prisoner Chink Weber and his bullying friends to kill First-Base in the gym. Leone fights and defeats Chink, but he spares Chinks life because he knows thats precisely what Drumgoole wants. Since Leone didnt kill Chink one of Chinks friends stabs Leone from behind with a Shiv (weapon)|shank. Whilst Leone recovers in the prison infirmary, one night a stranger, an inmate who claims to be an old friend from Treadmore, says that Chink has paid him to rape and murder Melissa. Dallas offers to help Leone escape to warn Melissa but instead delivers Leone to the utility basement right into Drumgooles hands where he finds out that Drumgoole wanted him to try to escape so he would receive a mandatory 10-year sentence for the second escape attempt and the inmate who was going to rape Melissa was just a corrupt prison guard called Mastrone who was part of the wardens scheme to set Leone up.

Dallas agreed with the warden to lead Leone into a trap only if the warden would give Dallas an early release from prison. But the warden betrays Dallas telling him he doesnt make deals with escaped prisoners. He then tells Dallas hell be sent back to the prisons general population, and since they now know hes a snitch the inmates will kill him. Dallas tries to attack the warden but the corrupt sadistic prison guards beat him up and push him into a pool of water.  The warden then leaves the sadistic guards to physically abuse Leone by pushing his face into a cloud of hot steam from the pipes. Leone breaks free burning Wiley, one of the guards, in the steam and knocking out Mastrone he then fights the sadistic guard Manly and beats him up and pushes him into the same pool of water Dallas fell into. Dallas, badly beaten up, apologizes to Leone and as Leone tries to help him Manly attacks Leone from the water and Dallas then rips an electric cable off the railing and electrocutes Manly in the water, taking his own life as well. Leone, who wasnt in the water, survives.

Enraged, Leone breaks into Drumgooles office and takes him to the execution chamber and straps him to the electric chair. Leone activates the generator and secures his hand to the switch. The guards organize into a response team and secure heavy weaponry. The prison guards break into the execution chamber and point their weapons at Leone, but if he is shot he will trip the switch and kill Drumgoole. The warden finally confesses to his plot to increase Leones jail time. Leone pulls the switch but nothing happens; he then reveals he took one of the fuses out to trick the warden into confessing. Captain Meissner and his men cuff Frank whom the warden orders to be taken to the hole; however, Meissner and his men dont. They take Drumgoole into custody for the confession though the warden insists that he merely played along to save his life.

A judicial inquiry is made into the matter about the wardens corrupt behavior and Leone serves only the jail time required of him in the first place. A few weeks later Frank leaves prison to the cheers of his fellow inmates. He meets up with Eclipse one last time, and receives a Cuban cigar which supposedly was given to Eclipse by Fidel Castro. He tells the captain who walks him to the exit that hell miss his incredible smile and Captain Meissner shakes his hand and says his farewell. Frank then exits Gateway and embraces the waiting Melissa.

==Cast==
* Sylvester Stallone as Frank Leone
* Donald Sutherland as Warden Drumgoole
* John Amos as Captain Meissner
* Sonny Landham as Chink Weber
* Tom Sizemore as Dallas
* Frank McRae as Eclipse
* Darlanne Fluegel as Melissa
* William Allen Young as Braden
* Larry Romano as First Base
* Jordan Lund as Manly
* John Lilla as Wiley
* Dean Duval as Ernie
* Jerry Strivelli as Louie Munafo
* David Anthony Marshall as Mastrone
* Kurek Ashley as Chinks Gang Member
* Michael Petroni as Chinks Gang Member
* Danny Trejo as Chinks Gang Member
* Frank Pesce as Johnson

==Production==
John Flynn later recalled how the movie was made:
 Stallone had a “window,” which means the guy was available for a certain window of time. Larry Gordon had a terrible script set in a prison. Stallone calls James Woods and asks if I’m any good as a director. Woods says yeah, he’s a good director and you ought to work with him. So we have a director and a star, but no script. All we have is a theme - a guy escaping from prison. So we hire Jeb Stuart, who was then one of the hottest writers in Hollywood, to rewrite the script and we go off looking for prison locations. Now we have a star, a theme, a shooting date, a budget, a studio, but we still have no script. So we all go back to New York and move into a hotel where Larry “tortures” Jeb and Henry Rosenbaum into writing a script in record time. Meanwhile, I’m going around scouting prisons. We finally found one in Rahway, New Jersey. Jeb and Henry were writing the script as we were making the movie. New pages would come in every day. There was one day when I was on the third tier of a cellblock in Rahway Penitentiary and I had nothing to shoot. I had my movie star, all these extras and a great location - and the pages were on their way. So we sat around and bullshitted with the prisoners. Stallone is a smart guy and a very underrated actor. If I ever needed a better line, he’d come up with one. Stallone is a really hard worker. I had no problem whatsoever with him.    

==Reception==

===Box Office===
Lock Up did poorly at the American box office, making $22,099,847 on a budget of $24 million. 

===Critical===
Rotten Tomatoes, a review aggregator, reports that 17% of twelve surveyed critics gave the film a positive review; the average rating is 4.2/10.   Kevin Thomas of the Los Angeles Times wrote that Stallone "defies credibility to the point of inviting unintended laughter."   Hal Hinson of The Washington Post wrote, "Lock Up bears the unmistakable mark of a vanity production". 
 Razzie Awards including Worst Picture, Worst Actor for Sylvester Stallone and Worst Supporting Actor for Donald Sutherland, but failed to win any of those categories. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 