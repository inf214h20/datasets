Spork (film)
{{Infobox film
| name           = Spork
| image          = Spork film.jpg
| alt            = 
| caption        = 
| director       = J.B. Ghuman Jr.
| producer       = Christopher Racster Chad Allen Honey Labrador Geric Frost
| writer         = J.B. Ghuman Jr. Sydney Park Rachel G. Fox Michael Arnold Oana Gregory Rodney Eastman Beth Grant Yeardley Smith Keith David Elaine Hendrix Richard Riehle
| music          = Casey James and The Staypuft Kid
| cinematography = Bradley Stonesifer
| editing        = Phillip Bartell
| studio         = Neca Films Last Bastion Entertainment Bent Film 11:11 Entertainment
| distributor    = Underhill Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} musical comedy Sydney Park, Rachel G. Fox, Michael William Arnold, Oana Gregory, Beth Grant, Elaine Hendrix, Yeardley Smith, Rodney Eastman, Keith David and Richard Riehle. The screenwriter was J.B. Ghuman Jr and the production was by Christopher Racster, Chad Allen, Honey Labrador and Geric Frost.

== Plot ==
A 14-year-old girl nicknamed "Spork" is unpopular, mistreated by her classmates, and very soft-spoken. Her next-door neighbor and best friend, known as "Tootsie Roll", is planning on entering the school Dance-Off to win $236 which she would use to visit her father in prison. During a hair-product-related dancing accident, Tootsie Roll injures her ankle and can no longer compete in the competition. Spork rises to the occasion and surprises the whole school by signing up for the Dance-Off. 

Spork and Tootsie Roll listen to hip-hop songs from the early 1990s and wear 1990s fashion, yet the antagonist, Betsy Byotch, and her friends wear 1980s garb and listening to 80s music (though they are also fans of Britney Spears). The character of Charlie is obsessed with Justin Timberlake, whose career began in the mid–1990s. There is also a mention by "Besty Beyotch" that there are pictures from a few years ago of "Loosie Goosie" circa 1998.
 The Wiz, The Wizard of Oz.

==Cast==
* Savannah Stehlin as Spork Sydney Park as Tootsie Roll
* Rachel G. Fox as Betsy Byotch
* Michael William Arnold as Charlie
* Oana Gregory as Loosie Goosie
* Beth Grant as Principal Tulip
* Elaine Hendrix as Felicia
* Yeardley Smith as Ms. Danahy
* Rodney Eastman as Spit
* Keith David as Coach Jenkins
* Richard Riehle as Clyde

== Critical reception ==
Spork received mixed reviews from film critics. It received a score of 50% from Rotten Tomatoes.   

==External links==
*  
*  
*  
*  

== References ==
 

 
 
 
 
 