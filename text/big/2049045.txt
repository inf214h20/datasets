Mr. Arkadin
{{Infobox film
| name           = Mr. Arkadin AKA Confidential Report
| image          = Poster3 Orson Welles Mr. Arkadin Confidential Report.jpg
| caption        = Theatrical release poster.
| writer         = Orson Welles The Lives of Harry Lime originally produced by Harry Alan Towers (uncredited)
| starring       = Orson Welles Robert Arden Paola Mori Akim Tamiroff Michael Redgrave
| director       = Orson Welles
| producer       = Louis Dolivet Orson Welles
| distributor    = Filmorsa/Cervantes Films/Sevilla
| music          = Paul Misraki
| editing        = Renzo Lucidi 
| released       = 20 October 1955 (Madrid)
| runtime        = 99 minutes ("Corinth" version)  93 minutes (Spanish version)  95 minutes (public domain version)  98 minutes (Confidential Report)  106 minutes (2006 edit)
| country        = France Spain Switzerland English Spanish Spanish
| budget         = gross = 517,788 admissions (France)   at Box Office Story 
| awards         =
}}

Mr. Arkadin (first released in Spain, 1955), known in Britain as Confidential Report, is a French-Spanish-Swiss coproduction film, written and directed by Orson Welles and shot in several Spanish locations, including Costa Brava, Segovia, Valladolid and Madrid. Filming took place throughout Europe in 1954, and scenes shot outside Spain include locations in London, Munich, Paris, the French Riviera, and the Château de Chillon in Switzerland.

==Plot==
Guy Van Stratten, a small-time American smuggler working in Europe, is at the scene of the murder of a man named Bracco. The dying man whispers two names that he claims are very valuable, one of which is Gregory Arkadin. Using this small bit of information and lots of bluffing, Van Stratten manages to meet the apparent multi-millionaire business magnate and socialite Arkadin, and Arkadin then hires Van Stratten to research his own past, of which he claims to have no memory before 1927.

Traveling across the world, Van Stratten pieces together Arkadins past from the few remaining people who knew Arkadin as a gangster in post-World War I Europe, but in each case the individuals he speaks to end up dead.  Van Stratten ultimately discovers the truth about Arkadins past, leading to a climactic race to Spain between the two, with disastrous consequences.

==Cast==
* Orson Welles as Gregory Arkadin
* Robert Arden as Guy Van Stratten
* Patricia Medina as Mily
* Paola Mori as Raina Arkadin
* Akim Tamiroff as Jakob Zouk
* Gregoire Aslan as Bracco
* Jack Watling as Bob, the Marquess of Rutleigh
* Mischa Auer as the Professor
* Peter Van Eyck as Thaddeus
* Michael Redgrave as Burgomil Trebitsch
* Suzanne Flon as Baroness Nagel
* Frederick OBrady as Oskar
* Katina Paxinou as Sophie Radzweickz Martinez Manuel Requena as Jesus Martinez
* Tamara Shayne as the woman who hides Zouk
* Terence Langdon as Mr. Arkadins Secretary
* Gert Frobe as a Munich Detective Eduard Linker as a Munich Policeman
* Gordon Heath as the pianist in the Cannes bar
* Annabel as the woman with a baguette in Paris
* Irene López Heredia as Sofía (only in Spanish version)
* Amparo Rivelles as Baroness Nagel (only in Spanish version)

==Production== radio series The Lives of Harry Lime, which in turn was based on the character Welles portrayed in The Third Man.  The main inspiration for the plot was the episode Man of Mystery. Most of the key elements for Arkadins character come from real life arms dealer, Basil Zaharoff, the mysterious birthplace, the French Riviera property and the Spanish castle for example.

In addition, several different versions of the film were released. Welles missed an editing deadline, and so Producer Louis Dovilet took the film out of his hands and released several edits of the film, none of which were approved by Welles. Jonathan Rosenbaums essay "The Seven Arkadins" is an attempt to detail the different versions including the novel and radio play. Adding to the confusion is a novel of the same title that was credited to Welles; Welles claimed that he was unaware of the books existence until he saw a copy in a bookshop.

In 1982 Welles described Mr. Arkadin as the "biggest disaster" of his life, due to his loss of creative control,   not released in the United States until 1962.   Some consolation for Welles came in the form of Paola Mori who played the role of his daughter. In private life Countess Paola Di Girfalco, she would become his third wife.   In addition the shooting started Welless longtime relationship with Spain, where he lived for several periods in his life.

Released in some parts of Europe as Confidential Report, this film shares themes and stylistic devices with The Third Man. Like many of Welless other films, Mr. Arkadin was heavily edited without his input.  The Criterion Collection produced a three-DVD box set that includes three separate versions of Mr. Arkadin including a comprehensive re-edit that combines material taken from all the known versions of the film. Though even the creators of this "restored" version express their doubts as to the "correctness" of altering another artists work, this new version is far and away the most comprehensible and easy to follow of the known versions. Also included are three of the Harry Lime radio plays reported to have been written by Welles and which he certainly based the Arkadin screenplay on. A copy of the Arkadin novelization — which may have been adapted by Welles or a ghost writer — is also included. The Criterion release also includes commentary tracks from Welles film scholars Jonathan Rosenbaum and James Naremore.

==Multiple versions of Mr. Arkadin==
In his 1991 essay, "The Seven Arkadins", film historian Jonathan Rosenbaum identified seven different versions of the story, and since its initial publication, a further two versions have emerged. 

===Pre-film versions===
1. Three episodes of the radio series The Lives of Harry Lime, written, directed by and starring Welles. The basic plot of a wealthy Mr. Arkadian (spelt with three As in this version) commissioning a confidential report on his former life can be found in the episode "Man of Mystery" (first broadcast 11 April 1952), while the episode "Murder on the Riviera" (first broadcast 23 May 1952) and the final episode "Greek Meets Greek" (first broadcast 25 July 1952) both contain plot elements repeated in the film. Note that in the film, the popular Harry Lime character from The Third Man is replaced by the less sympathetic Guy Van Stratten, since Welles did not own the copyright to the Lime character (who was a creation of Graham Greenes).

2. Masquerade, an early version of the screenplay of what would eventually become Mr. Arkadin, has substantial differences from the film versions. The screenplay follows a strictly chronological structure rather than the back-and-forth structure of the film. Many of the scenes in the film are set in different countries, and a lengthy sequence in Mexico is entirely missing from the final film.

===Different edits of the film released in Welles lifetime===
Crucially, none of the versions available before 2006 contained all the footage found in the others; each had some elements missing from other versions, and each has substantial editing differences from the others.

3. The main Spanish-language version of Mr. Arkadin. (93 mins) This was filmed back-to-back with the English-language version and was the first to be released, premiering in Madrid in March 1955. Although the cast and crew were largely the same, at least two characters were played by Spanish actors - Amparo Rivelles plays Baroness Nagel, and Irene Lopez Heredia plays Sophie Radzweickz Martinez. (Additionally, Robert Arden is credited as "Bob Harden.") Some scenes had Spanish dubbing over English dialogue, although most were performed in Spanish.

4. There is a second Spanish-language cut of Mr. Arkadin, which was unknown to Rosenbaum at the time he wrote The Seven Arkadins. (He confessed in the essay to having seen only brief clips of one version.) This one credits Robert Arden as "Mark Sharpe". 

5. "Confidential Report" (98 mins) - the most common European release print of Mr. Arkadin, which premiered in London in August 1955. Differences to this version include the presence of off-screen narration from Van Stratten. Rosenbaum speculates that the editing of this version was based on an early draft of Welles, screenplay, since its exposition is far simpler than the "Corinth" version.

6. The "Corinth" version of Mr. Arkadin. (99 mins) - named after Corinth Films, the initial US distributor of the film.    Until the 2006 re-edit, it was believed to be the closest version to Welles conception. Peter Bogdanovich discovered its existence in 1961, and secured its first US release in 1962, seven years after alternative versions of the film came out in Europe.

7. The most widely seen version of Mr. Arkadin, which was made for television/ home video release and is now in the public domain. (95 mins) It entirely removes the films flashback structure and presents a simpler, linear narrative. Rosenbaum describes it as "the least satisfactory version", which is a "clumsily truncated" edit of the "Corinth" version, often editing out half-sentences, making some dialogue incomprehensible.  As this version is in the public domain, the vast majority of DVD releases are of this version, often in poor-quality prints.

===Novelization===
8. The novel Mr. Arkadin was first published in French, in Paris in 1955; and then in English in 1956, both in London and New York. Welles was credited as author, and the books dustjacket boasted "It is perhaps surprising that Orson Welles … has not written a novel before."

"I didnt write one word of that novel. Nor have I ever read it," Welles told Peter Bogdanovich. "Somebody wrote it in French to be published in serial form in the newspapers. You know — to promote the picture. I dont know how it got under hardcovers, or who got paid for that." 

Welles always denied authorship of the book, and French actor-writer Maurice Bessy was long rumoured to be the author. Rosenbaum suggested that the book was written in French and then translated into English, since lines from the script were approximations that seemed to have been translated from English to French to English again. Research by film scholar François Thomas in the papers of Louis Dolivet has uncovered documentary proof that Bessy was indeed the author. 

===Criterion edit (2006)=== Munich Film Museum and Claude Bertemes of the Cinémathèque municipale de Luxembourg, with both Peter Bogdanovich and Jonathan Rosenbaum giving technical assistance. It uses all available English-language footage, and attempts to follow Welles planned structure and editing style as closely as possible, incorporating his comments over the years on where the other edits went wrong. However, it still only remains an approximation - for instance, Welles remarked that his version of the film began with a womans body (Mily) on a beach, including a close-up which makes her identity apparent to the audience. Whilst the Criterion edit restores the film opening on a womans body on the beach, only a long shot exists (taken from the Corinth version) in which it is unclear whose body it is; and so no close-up of Mily could be used as the footage no longer exists. 

"The Criterion edit" (106 mins) was released in a 3-disc DVD set which included:
*the "Corinth" version of the film
*the "Confidential Report" version of the film
*a copy of the novel
*clips from one of the Spanish-language versions
*the three Harry Lime radio episodes on which the film was based

==Reception==
Japanese film director Shinji Aoyama listed Confidential Report as one of the Greatest Films of All Time in 2012. He said, "No other movie is destructive as Confidential Report, which gives me different emotions every time I see it. Achieving this kind of indetermination in a film is the highest goal that I always hope for, but can never achieve." 

==See also== The Scorpion and the Frog

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 