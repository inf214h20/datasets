Kannaki (film)
{{Infobox film
| name           =Kannaki
| image          = 
| image size     =1024 × 768
| caption        =ending scene where Kannaki lets the snake to bite her to die alongside Manickam.
| director       = Jayaraj
| producer       = Mahesh Raj
| writer         = Sajeev Kilikulam
| narrator       =
| starring       = Lal (actor)|Lal, Nandita Das, Siddique (actor)|Siddique, Kalpana (Malayalam actress)|Kalpana, Geetu Mohandas, Manoj. K. Jayan, Cochin Hanifa
| music          = Kaithapram Vishwanathan	 	 
| cinematography = M.J. Radhakrishnan	  
| editing        = N.P. Sathish
| distributor    = 
| released       = 2001
| runtime        =
| country        = India Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Kannaki  is a Malayalam language film. It was released in 2001. It is directed by Jayaraj. It is loosely based on Shakespeare story of Antony & Cleopatra   

==Plot==
Kannaki revolves around Manickam (Lal) and Choman (Siddique) who are best friends in a local village. Manickam excels in the local game of ‘Cock Fight’ and has total mastery of the game and the bird. Manickam is actually fighting for Choman in his regular cockfights with the Gounder (Manoj K.Jayan).

The local beauty is Kannaki (Nandita Das), who has some magical powers and half the village is lusting after her. Choman and the Gounder had fallen out with Kannaki, but Kannaki likes Manickam. She likes him for his courage and macho image. Manickam has another admirer in Choman’s sister Kumudam (Geetu Mohandas).

Kannaki and Manickam become very close and he takes a decision to marry her. When Choman comes to know of this, he is furious. He wants Manickam to marry his sister and goes to the extent of announcing their wedding at a public function. Manickam meets Kumudam and explains to her that he has a sisterly affection to her and not of that of a lover. Kumudam is heart broken, but still maintains a level of loyalty in hope that, Manickam will realize her love. Choman is angered by all this and develops a hurt feeling towards his best friend.

Kannaki meanwhile, out of love, tries to get Manickam out of the local game and out of the clutches of Kumudam. A local fortune teller Kanakamma (Kalpana) keeps feeding ideas and false news into Kannakis ears about Manickam. On her idea, Kannaki gives an untrained cock on the pretense that she had trained the cock to fight, to Manickam. Manickam takes the cock to fight, against Chomans cock. Manickam loses the fight and comes home to tell Kannaki has cheated him. There she tells him she did this to have Manickam all to herself and away from Choman and the Gounder.

Choman and Gounder pair up to defeat Manickam at a local festival. They know that Manickam will come to the cock fight to take part. As Choman and Gounder plan, kumudam comes to say that her brother is wrong in fighting against Manickam,to which the Gounder says if she wants Manickam, they have to separate Kannaki from him. And their separation is only possible if this fight takes place.

Later, Kanakamma comes to Manickam and says Kumudam wants to meet him in private. After he goes to speak to Kumudam, Kanakammagoes goes inside and tell Kannaki that Manickam is cheating on her, and as proof she can go outside and see Manickam and Kumudam speaking. Meanwhile at a distance Manickam is telling Kumudam that he still sees her as a sister, and nothing else. Kumudam says she still loves him, and they depart. Kannaki then approaches Kumudam and says, Kumudam shouldnt meet Manickam again. Kumudam lies to her that she cannot stop herself from seeing her childs father who is Manickam, and that they have met like this many times before. Kannaki is shocked and asks Kumudam what she should do, to which Kumudam says she should let Manickam go back to Kumudam.

Kannaki then sees a local snake seller, and gets the most poisonous snake from him. Later Manickam is seen gifting Kannaki with a wedding saree to wear for their wedding. he says they will marry right after the game, which he is sure he will win.
Kannaki dresses up as a bride, and wishes Manickam the best for the game.As soon as he leaves, she tells Ravunni to tell Manickam that she left from that place, and that Manickam should forget her and marry Kumudam. He also tells Ravunni that she will be in the nearby Sarpakaavu, waiting for Ravunni, and until he tells her that Manickam left to stay with Kumudam, she will not come out.

Manickam wins the game, seriously injuring Chomans cock, and a fight disrupts. the Gounder runs away, and at the end of the fight, Manickam and Choman become friends. Manickam returns home announcing his victory, but Ravunnis bad news is waiting for him. Manickam then enters the house with his cock, full of grief and shock on why Kannaki did this to him. he locks the house, and does the same cock fight with him and his cock. He lets the cock wound and scratch him, till the cock pecks at his main vein on the neck. he falls down dead. In the meanwhile Ravunni who watched and tried to stop this from outside the house, goes and calls Kannaki. Initially she does not come, but when Ravunni tells her what Manickam did to himself, she comes out and goes to see him. When she finds him dead, she lets out a deep wail and cry. She then takes out the snake she bought earlier, and lets the snake bite her. She also dies besides Manickams body.

==Cast== Lal as Manickam
* Nandita Das as Kannaki
* Geetu Mohandas as Kumudam Siddique as Choman
* Cochin Haneefa as Ravunni
* Manoj K. Jayan as Gounder Kalpana as Kanakamma
* Sajan palluruthi as friend of manickam

==References==
 

 
 
 