Fakta Ladh Mhana
{{Infobox film
| name           = Fakta Ladh Mhana
| film name = फक्त लढ म्हणा
| image          = Fakta Ladh Mhana.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sanjay Jadhav
| producer   = Great Maratha Entertainment Company  Twinkle Group & Mirah Entertainment 
| writer   = Mahesh Manjrekar
| based on  = Action and Story of Suicides of Farmers in Maharshtra
| starring = Bharat Jadhav, Sachin Khedekar, Siddharth Jadhav, Sanjay Narvekar
| music = Ajit - Sameer
| cinematography = Sanjay Jadhav
| editing   = Rahul Bhatnakar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Marathi
| budget         =  4,00,00,000 
| gross          = 
}} Marathi action film with lead roles by Aniket Vishwasrao, Siddharth Jadhav, Sanjay Narvekar, Santosh Juvekar, Bharat Jadhav, Mahesh Manjrekar, Sachin Khedekar and Vaibhav Mangle. The film is written and produced by Mahesh Manjrekar and is directed by Sanjay Jadhav.  It is notable for being one of the costliest Marathi film.   

==Plot== hearing problems, a Salim (Santosh Juvekar), Alex (Aniket Vishwasrao), an aggressive guy, Paani Cum a small boy and Baba Bhai (Mahesh Manjarekar) leader of the gang. The story of film about the suicides of the farmers.

Aptegaon a progressive village in Maharashtra is suddenly in news thanks to its impending Special Economic Zone or SEZ status. As a result, Land prices are to zoom, but the villagers are still unaware of their villages impending SEZ status
.
Industry Minister Madhusudan Patil, the MLA from Aptegaon, a clean image, but highly corrupt politician controls Aptegaon village through his aide Kulkarni, who loans huge amounts of money to farmers and thus keeping them at his mercy, since most of them have no other option but to sell him their land.  The piece of land, Madhu is after, is a 7-acre plot adjoining the river, which belongs to a war hero Gangadhar. Madhusudan has already pre-sold this land at a fancy price to an industrialist Veer Dhawal. 

The school teacher, Milind gets wind of the SEZ status and cautions the villagers, resulting in him being killed by Madhusudans brother Bhasker. Bhasker, Madhusudan& Kulkarni run riots as a result Gangadhar loses his life too. Gangadhars son Vithobais is sure its murder, but all the evidence available points towards suicide, which the villagers don’t agree to. Vithobas last option is to turn to his cousin Tukaram who is part of the underworld in Mumbai, controlled by the goon Bababhai.  

Tukaram is furious when he learns about his uncle Gangadhars killing and leaves for Aptegaon with his accomplices Alex, West Indies, Salim, Kanfatya, & their chaiwala Panikam. On reaching there they realize they are fighting an enemy bigger than they have ever faced in their life. Blinding fury rises in each one of them & they decide to fight.  Even at the cost of their own lives. 

==Cast==
* Sachin Khedekar as Madhusudan Patil
* Bharat Jadhav as Tukaram
* Sanjay Narvekar as Kanfatya
* Siddarth Jadhav as West Indies
* Vaibhav Mangle as Sawkar Kulkarni
* Santosh Juvekar as Salim
* Aniket Viswasrao as Alex
* Umesh Tonpe as Sam
* Kranti Redkar as Tukarams girlfriend
* Sanjay Khapre as Bhaskarrao Patil
* Mahesh Manjrekar as Bababhai
* Satish Pulekar as Gangadhar
* Amruta Khanvilkar as Special Appearance in song
* Mansi Naik as Special Appearance in song 

==Production and promotion==
The films promotion caught audiences as it aroused curiosity through poster. The poster only showed five men holding another person at Coercion#Physical|gunpoint. The actors were not seen in this poster.  The film also caught many eyes for being one of the most costliest Marathi film. The budge of the film went above  4 crore. 

==Soundtrack==
The music of Fakta Ladh Mhana is composed by the duo of Ajit-Sameer while the lyrics are penned by Guru Thakur,Kaushal Inamdar, Pravin Kunvar & Jeetendra Joshi.
{| class="wikitable"

! No. !! Title !! Singer(s) !! Length
|-
| 1 || Fakta Ladh Mhana Title Song || Ajit Parab, Mahesh Manjrekar, Chorus || 3:52
|-
| 2 || Aan De || Avdhoot Gupte, Ajit Parab || 4:21
|-
| 3 || Tu Manat Tu (Kawawali)|| Neha Rajpal, Swapnil Bandodkar, Ajit Parab, Chorus || 5:44
|-
| 4 || Daav Ishqacha (Laavni) || Urmila Dhangar, Chorus || 5:20
|}

==References==
 

==External links==
*  

 
 