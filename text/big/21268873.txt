The Martyr of Calvary
 
{{Infobox film
| name           = The Martyr of Calvary
| image          = 
| caption        = 
| director       = Miguel Morayta
| producer       = Gonzalo Elvira Raúl Mendizábal
| writer         = Gonzalo Elvira Sánchez de Aparicio Miguel Morayta
| starring       = Enrique Rambal
| music          = 
| cinematography = Jorge Stahl Jr.
| editing        = José W. Bustos
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

The Martyr of Calvary ( ) is a 1952 Mexican drama film directed by Miguel Morayta about the life of Christ. It was entered into the 1954 Cannes Film Festival.   

==Cast==
* Enrique Rambal - Jesús (as Enrique Rambal Jr.)
* Manuel Fábregas - Judas (as Manolo Fabregas)
* Consuelo Frank - María Madre
* Alicia Palacios - Magdalena
* Miguel Ángel Ferriz - Pedro Carmen Molina - Marta
* José María Linares-Rivas - Caifás (as Jose Mª. Linares Rivas)
* Felipe de Alba - Andrés
* Luis Beristáin - Jefe Sinagoga
* Miguel Arenas - José de Arimatea
* Lupe Llaca - Verónica
* Alberto Mariscal - Anás el Joven
* Alfonso Mejía - Marcos
* José Baviera - Poncio Pilatos
* Fernando Casanova - Centurión

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 