Phir Bhi Dil Hai Hindustani
 
 
{{Infobox film
| name           = Phir Bhi Dil Hai Hindustani फिर भी दिल है हिंदुस्तानी  
| image          = Phir Bhi Dil Hai Hindustani .jpg
| caption        = 
| director       = Aziz Mirza
| producer       = Shahrukh Khan Juhi Chawla Aziz Mirza
| writer         = Sanjay Chhel Raaj Kumar Dahima Manoj Lalwani
| starring       = Shahrukh Khan Juhi Chawla Johnny Lever Paresh Rawal
| music          = Jatin-Lalit
| cinematography = Santosh Sivan
| editing        =
| distributor    = Dreamz Unlimited
| released       = 21 January 2000
| runtime        = 166 minutes
| country        = India
| language       = Hindi
| budget         = 150&nbsp;million
| gross          =   
}}

Phir Bhi Dil Hai Hindustani ( ,  }}, "Yet the heart is still Indian") is a Bollywood comedy-drama film released by Dreamz Unlimited on 21 January 2000. The film was directed by Aziz Mirza and stars Shahrukh Khan and Juhi Chawla focuses on the media war being fought in the TV newsrooms. Ajay Bakshi and Ria Banerjee, respectively. Two arch rival Television reporters for rival news channels, who try to save the life of a man who has been fixed by a politicians from the death of his brother, from a death sentence.
 Yes Boss and Raju Ban Gaya Gentleman.

==Plot==

Ajay Bakshi (Shahrukh Khan) is a successful loudmouthed reporter, working for a reputed news channel. His father was a freedom fighter living on a measly pension. Ajay thinks that his fathers ideal & sacrifices have given him nothing. He has no respect for his fathers ideals, which his father still sticks to. The rival news channel ropes in Ria Banerjee (Juhi Chawla) as his answer.
Ria is the antithesis of Ajay & uses her charm to get her work done.

Pappu Junior alias Choti ( ) brother (Mahavir Shah) on national TV. Choti will earn respect & Ajays channel will gain TRP. Little does Ajay know that the plan is going to turn too real.

The ministers brother is gunned down by an assailant named Mohan Joshi (Paresh Rawal). When Ajay learns that he was not Chotis man, he panics. Ria learns about Ajay & Chotis deal, but they decide to help each other nevertheless. Minster Ramakant, takes advantage for his brothers death to gain vote and sympathy he organises a pogrom. In the turn of events, Mohan is arrested, but does not open his mouth even once during police torture. To avoid any public unrest, the ACP (Anjan Srivastav) declares that Mohan is a terrorist working for unknown terror organisations. Meanwhile, Mohan somehow escapes out of jail.

Ajay & Ria are fighting over the matter in their car, unaware that Mohan is hiding in the same car. Mohan accosts them, where Ajay accuses him of being a terrorist. Mohan has an angry outburst & tells him that he is not a terrorist. Mohan goes on to narrate his story. Mohan has an ailing wife (Neena Kulkarni) & a now-dead daughter. Mohan says that his daughter went to an interview for a secretarial job at the victims office, where the latter raped and beaten her badly and she dies by the trauma & Mohan was left helpless & running from door to door for justice. Hence hopeless, he took the law into his own hands.

Ajay & Ria are shaken by his confession & decide to help Mohan. Ajay hands over the video tape of this confession to his boss and Ajays uncle Kaka (Satish Shah). Unfortunately, the minister has joined hands with his opposition rival minister Mushran (Govind Namdeo) as he fears that their secrets might come out if Mohan surrenders. Similarly, Ajays boss forms an alliance with Rias boss (Dalip Tahil) after knowing about the tape. The ministers & channel heads come together and trick Ajay & Ria to give the tape to them.

Only after they give the tape do they realise what the truth is. Ajay is angry at first, but comes up with a plan to retrieve the tape back from the politicians and their respective bosses. With the help of Ria & Choti, he succeeds in obtaining the tape. Here Mohan has been arrested by police & is sent to be publicly hanged. Ajay succeeds in broadcasting Mohans confession just an hour before the execution & tearfully requests the nation to stop this injustice.

The ministers and policemen try to stop the protesters from coming to the execution ground by making the police barricade the entrance and policemen and head inspector (Vishwajeet Pradhan) beats up Ajay. The ACP decides to join Ajay & Ria, thus neutralising the police barricades. In a blatant mockery of medias TRP hogger attitude, Mohan is made to wear a T-shirt containing logos of various companies & is prepared for execution. Just seconds before the execution, Ajay & the protesters succeed in saving Mohan and beats up the politicians by public.

Mohans execution is called off. Ajays father tells him that his ideals may not have given him money, but they gave him something far more important – Ajay. Ajay proposes to Ria in front of all the protesters; Ria accepts after playful funny banter with him.

==Cast==
* Shahrukh Khan as Ajay Bakhshi
* Juhi Chawla as Ria Banerjee
* Paresh Rawal as Mohan Joshi
* Johnny Lever as Choti/Pappu Junior
* Atul Parchure as Shahid
* Sanjai Mishra as Bomb Defuser
* Sharat Saxena as Pappus boss
* Neena Kulkarni as Mohans wife
* Dalip Tahil as Rias boss
* Satish Shah as Ajays boss
* Govind Namdeo as Minister Mushran
* Shakti Kapoor as Minister Ramakant Dua
* Mahavir Shah as Ramakants brother
* Bharti Achrekar as Rias mother
* Smita Jaykar as Ajays mother
* Vishwajeet Pradhan as Head Police inspector
* Dilip Joshi as Sapney (Chotis aide)
* Mona Ambegaonkar as Ajays crush Hyder Ali as Ajays father

==Music==
{{Infobox Album |  
 Name = Phir Bhi Dil Hai Hindustani
| Type = Album
| Artist = Jatin Lalit
| Cover = 
| Released = 2000
| Recorded = Feature film soundtrack
| Length = 33:26
| Label = Sony Music
| Producer = Jatin-Lalit
| Last album = Mohabbatein (2000)
| This album = Phir Bhi Dil Hai Hindustani (2000)
| Next album = Raja Ko Rani Se Pyar Ho Gaya (2000)
}}
{{Album ratings
| rev1 = Planet Bollywood
| rev1score =     
}}

The music of the film was appreciated. Manish Dhamija of Planet Bollywood gave 8.5 stars stating, "Overall, the album is a pleasant surprise from Jatin-Lalit". 
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CC99FF" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
|"Phir Bhi Dil Hai Hindustani" Udit Narayan
| 04:00
|-
| 2
| "I Am The Best – Male" Abhijeet Bhattacharya|Abhijeet
| 04:18
|-
| 3
| "I Am The Best – Female" Jaspinder Narula
| 04:18
|-
| 4
|"Banke Tera Jogi" Sonu Nigam and Alka Yagnik
| 04:43
|-
| 5
|"Vande Mataram" Shankar Mahadevan Ehsaan
| 04:39
|-
| 6
|"Kuch To Bata" Abhijeet Bhattacharya|Abhijeet and Alka Yagnik
| 04:32
|-
| 7
|"Aur Kya" Abhijeet Bhattacharya|Abhijeet and Alka Yagnik
| 05:03
|-
| 8
| "Aao Na Aao Na"
| Jatin Pandit
| 01:53
|}

==See also==
* Mera Joota Hai Japani

==References==
 

==External links==
 
 
 
 
 
 
 