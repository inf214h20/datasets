Bait 3D
 
{{Infobox film
| name           = Bait
| image          = Bait3D poster.jpg
| caption        = Theatrical Release Poster
| director       = Kimble Rendall
| producer       = Gary Hamilton Todd Fellman Peter Barber
| writer         = Russell Mulcahy John Kim
| based on       =  
| starring       = {{plainlist|
*Phoebe Tonkin
*Xavier Samuel
*Julian McMahon
*Sharni Vinson
*Adrian Pang
*Qi Yuwu Alex Russell
*Alice Parkinson
*Martin Sacks
*Cariba Heine
*Lincoln Lewis
*Dan Wyllie
*Richard Brancatisano}}
| music          = Joe Ng Alex Oh
| cinematography = Ross Emery
| editing        = Rodrigo Balart
| studio         = Darclight Films
| distributor    = Paramount Pictures
| released =  
| runtime        = 93&nbsp;minutes
| country        = Australia Singapore
| language       = English
| budget         = $20&nbsp;million 
| gross          = $29,538,160 
}}
 horror Thriller thriller film Alex Russell, Lincoln Lewis, Alice Parkinson, and Dan Wyllie. The film was released on 20 September 2012 in Australia.

==Plot==
The movie opens with a hungover lifeguard, Josh (Xavier Samuel), being woken up by friend and fellow lifeguard Rory (Richard Brancatisano). Rory tells Josh that he shouldnt have proposed to his sister, Tina (Sharni Vinson), then offers to set a buoy for Josh. Josh visits Tina, who discusses their upcoming move to Singapore. As Rory boards into the ocean to set a buoy, a shark appears and kills a man in the water. Alerted to the danger, Josh quickly takes a jet ski and goes to get Rory, but is too late. Rory is knocked into the water, and Josh is unable to save him before he is devoured.
 Alex Russell), who also works at the store. The store manager, Jessup (Adrian Pang), catches up with her, fires Ryan, and calls the police. The arresting officer, Todd (Martin Sacks), arrives, and is revealed to be Jaimes dad. After this, Jessup is held at gun point by a robber, Doyle (Julian McMahon). Things go south , and Doyles partner appears and shoots assistant manager Julie (Rhiannon Dannielle Pettett).

At the height of the commotion, a tsunami wave descends on the city and floods the supermarket. Doyles partner is killed by the flood, and the survivors injured. As the survivors try to find a way out the security guard is dragged under water. It becomes apparent that a great white shark has been washed into the store by the tsunami. Additionally, a broken wire threatens to electrocute them all. Steven volunteers to shut off the power, and the others dress him in crude armor made of shopping carts and shelves to protect him from the shark. He succeeds but loses his oxygen tube and drowns. Despite their previous conflicts, the survivors work together to get Jessup into a ventilation shaft to go for help. A flood of quarter size  crabs slides out of the vent, startling Jessup and causing him to fall back, where the shark kills him.

In the parking garage, Kyle, Heather, and Ryan have been cut off from the others. Ryan helps the couple escape from their drowned car, but a second great white shark is revealed. Kyle ditches the dog, Bully at the last second and they manage to get to temporary safety. After several unsuccessful attempts at luring the shark away, Ryan decides to join them on top of a flipped van, but the shark gives chase. Ryan successfully gets into the flipped van but Kyle falls and is devoured.

Inside the supermarket, the remaining group make a plan to catch their shark so they can swim to safety. Jaime manages to swim to the butcher section and grab a hook with meat to use as bait. The shark does not go for the bait, so fellow survivor, Kirby (Dan Wyllie) grabs a hook and puts it through Naomis shirt, using her as bait. Kirby is revealed to be Doyles partner, who changed out of his clothes and mask so the others wouldnt know he was the murderer. Doyle stabs Kirby with a makeshift harpoon, and throws him into the water with the hook. Naomi is pulled from the water as the shark devours Kirby, catching its jaw on the hook and ensnaring itself in the trap. Josh apologizes to Tina, feeling guilt over her brothers death, but Tina reassures him and kisses him.

Below, Bully is found alive. Heathers newfound hope inspires Ryan to start banging on the pipes, calling for help. Jaime hears Ryan, and with Josh goes to rescue him. Below, they are alerted to the second sharks presence. Josh and Jaime find her dads car, in which are a shotgun and taser. Josh kills the shark with the shotgun, and the four of them get back into the supermarket. A tremor strikes as they are all swimming to the entrance, breaking the first shark loose. Josh kills the shark with the taser as Doyle wires a truck jammed in the entrance to explode, breaking a hole in the debris and freeing them. The survivors leave the supermarket, reaching the severely damage city, and Tina asks Josh what to do now. At sea, a seagull swoops low over the water, and a shark jumps out and devours it.

==Cast==
*Phoebe Tonkin as Jaime, the protagonist
*Xavier Samuel as Joshua "Josh", Rorys friend, and Tinas fiancé
*Sharni Vinson as Tina, Rorys sister, and Joshs fiancé
*Julian McMahon as Doyle, the robber of the supermarket
*Richard Brancatisano as Rory, Tinas brother
*Dan Wyllie as Kirby, Doyles partner
*Alice Parkinson as Naomi, who also works at the supermarket
*Damien Garvey as Colins
*Lincoln Lewis as Kylerick "Kyle", Heathers boyfriend
*Cariba Heine as Heather, Kyles girlfriend Alex Russell as Ryan, Jaimes boyfriend
*Adrian Pang as Jessup, who is the manager of the supermarket
*Qi Yuwu as Steven, Tinas new best friend
*Martin Sacks as Todd, Jaimes father
*Chris Betts as Lockie
*Simon Edds as Lifeguard
*Miranda Deakin as TV Reporter
*Rhiannon Dannielle Pettett as Julie, who is the assistant manager of the supermarket
*Skye Fellman as Young Girl
*Nicholas McCallum as Neil
*Gypsy as Bully the dog, Heathers dog

==Production==
The film began shooting on 29 November 2011. 

==Release==
Bait had its premiere at the Venice Film Festival where it was shown out of competition on 1 September 2012. The film was released theatrically on 20 September 2012 Australia and on 29 November 2012 in Singapore.   

The film was released on DVD in Australia on 16 January 2013 without the "3D" subtitle as Bait; it was only released in 3D on Blu-ray Disc|Blu-ray.

==Reception==
The film received a 47% "Rotten" score on Rotten Tomatoes based on 32 reviews. Margaret Pomeranz acclaimed the film, saying it would appeal to a specific genre audience, while David Stratton dismissed the film for not investing the audience in its characters. Others, such as Jake Wilson of The Age, saw it as a film that is somewhat enjoyable on the basis of being an "awful" film. 

==Sequel==
Production of a sequel called Deep Water, about a plane en route from China to Australia crashing in the Pacific Ocean, was suspended in March 2014 due to "uncomfortable similarities" to the disappearance of Malaysia Airlines Flight 370.  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 