Comic Book: The Movie
{{Infobox film
| name         = Comic Book: The Movie
| image        =
| caption      =
| director     = Mark Hamill Billy West Scott Zakarin
| writer       =
| starring     = Mark Hamill Billy West Donna DErrico Roger Rose Jess Harnell
| narrator     = Billy West
| music        = Billy West
| cinematography = Jason Cooley
| editing       = Matt Mariska Nimrod Erez Lawrence Hutchins E.S. Josephs
| distributor  = Miramax
| released     =  
| runtime      = 106 minutes
| country      = United States
| language     = English
| budget       =
}}
Comic Book: The Movie is a 2004 Direct-to-video|direct-to-DVD mockumentary starring and directed by Mark Hamill.

==Plot==
The story revolves around comic book fan Don Swan and his battle against a fictional film studio which is about to announce a film based on his favorite superhero, Commander Courage.

==Production==
Most of the movie was filmed at a San Diego Comic-Con International|Comic-Con. All the interviews with attendees while Hamill remained in-character were unscripted, for a realistic look. The film also was critical of "crowding out" in voice acting where celebrities have been cast to voice part instead of experienced voice actors; this film did the opposite by casting voice actors in live-action roles.

==Cast==

* Mark Hamill as Donald Swan Billy West as Leo Matuzik
* Donna DErrico as Liberty Lass / Papaya Smith
* Roger Rose as Taylor Donohue
* Jess Harnell as Ricky
* Lori Alan as Anita Levine
* Daran Norris as Commander Courage / Bruce Easly
* Jim Cummings as Dr. Cedric
* Jill Talley as Jill Sprang
* Tara Strong as Hotel Maid
* Arleen Sorkin as Ms. Q
* James Arnold Taylor as J.T.
* Debi Derryberry as Debby Newman
* Tom Kenny as Derek Sprang
* Sid Caesar as Old Army Buddy
* Jonathan Winters as Wally (Army Buddy #2)
 Peter Mayhew (who played Chewbacca alongside Hamill in Star Wars), actor Jeremy Bulloch (who played Boba Fett alongside Hamill in The Empire Strikes Back and Return Of The Jedi), actor and voiceover performer Edd Hall, special effects designer Greg Nicotero, and Matt Groening (creator of the comic Life in Hell and animated TV series The Simpsons and Futurama).

==Awards==

*DVD Exclusive Awards
**Best Live-Action DVD Premiere Movie
**Best Actor (in a DVD Premiere Movie) - Mark Hamill
**Best Supporting Actor (in a DVD Premiere Movie) - Sid Caesar and Jonathan Winters
**Best Director (in a DVD Premiere Movie) - Mark Hamill

==External links==
*  

 
 
 
 
 
 
 


 
 