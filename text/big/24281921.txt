Dalaal
{{Infobox film
| name           = Dalaal
| image          = Dalaalfilm.jpg
| director       = Partho Ghosh
| producer       = Prakash Mehra
| writer         = Kaushal Bharati (story) Tarun Ghosh (screenplay)  Anwar Khan
| starring       = Mithun Chakraborty Raj Babbar Ayesha Jhulka
| music          = Bappi Lahiri
| cinematography = N. Satyen
| editing        = Shyam Gupte
| genre          = Social
| distributor    = Eros Entertainment
| released       = October 29, 1993
| runtime        = 
| country        = India
| language       = Hindi
| awards         = 
| budget         = 
}}
 1993 Hindi Indian feature directed by eighth highest grossing Bollywood film of 1993. 

== Synopsis ==

Dalaal is the story of illiterate Bhola Nath who escorts young women to meet with their brothers. Bhola is enthusiastic as he works diligently to earn the respect of everyone around him. But one day during the course of his duties, he meets a beautiful woman named Roopali, who makes him understand the true nature of his duties - that a pimp. Will Bhola realize his mistake to redeem himself forms the climax.

== Cast ==

* Mithun Chakraborty ...  Bhola Nath
* Raj Babbar ...  Jagganath Tripathi
* Ayesha Jhulka ...  Roopali
* Ravi Behl ...  Inder
* Indrani Banerjee ...  Chutki / Radha
* Tinnu Anand ...  Chaku Singh
* Tarun Ghosh ...  Bengalese
* Deb Mukherjee ...  Girdhari
* Mangal Dhillon ...  Jagga Seth.
* Satyendra Kapoor ...  Chatriprasad
* Rita Bhaduri ...  Mrs. Djun-Djun Wala
* Suresh Chatwal ...  Inspector Tambe
* Vikas Anand ...  Bholas Father
* C. S. Dubey ...  Netas man
* Shakti Kapoor ...  Seth Djun-Djun Wala

==Soundtrack==
The Music of this movie was well appriciated with hit numbers like Gutur Gutur and was one of the final hit albums composed by Bappi Lahiri for a Mithun Chakraborty movie. 

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Thahre Huye Paani Mein"
| Kumar Sanu
|-  
| 2
| "Gutur Gutur"
| Kumar Sanu, Bappi Lahiri, Alka Yagnik, Ila Arun
|- 
| 3
| "Chori Chori Maine Bhi To"
| Kumar Sanu, Kavita Krishnamurthy
|- 
| 4
| "Mar Gaye Mar Gaye"
| Udit Narayan, Alka Yagnik
|-  
| 5
| "Na Unees Se Kam"
| Kumar Sanu
|- 
| 6
| "Thahre Huye Paani Mein"
| Sadhana Sargam
|- 
| 7
| "Mere Ramji Mere Bhagwanji"
| Kumar Sanu, Alka Yagnik
|}

==Reception==
Dalaal received 100% initial collection as the songs, especially "Gutur Gutur", became a rage. Dalaal is among the Superhit films of 1993.

==References==
 
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Dalaal

== External links ==
*  

 
 
 
 
 