Red Sien
{{Infobox film
| name           = Rooie Sien
| image          = 
| caption        = 
| director       = Frans Weisz
| producer       = Rob du Mée
| writer         = 
| starring       = 
| music          = Ruud Bos  
| cinematography = 
| editing        = 
| distributor    = Actueel film
| released       =  
| runtime        = 108 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}
Red Sien ( ) is a 1975 Dutch comedy film directed by Frans Weisz, produced by Rob du Mée. The story is based on the play with the same name, created by Marius Spree at the start of the 20th century. Beppie Nooij jr. played the role of Rooie Sien ("Red Sien") almost 1500 times on the stage. In the film Willeke Alberti plays the role of the young Sien, while Nooij moved on the role of Siens mother. The film was entered into the 9th Moscow International Film Festival.   

==Plot==
Rooie Sien starts in 1912, when Rooie Sien is murdered by her husband Ko Breman, because she doesnt want to follow him to Rotterdam. Sien works as a prostitute in Amsterdam, where she works in café De Kikker, with a pimp called Mooie Frans. Sien sings, entertains customers, and in some cases follows them to bed. Her daughter, Sientje, is raised in Rotterdam by her grandparents, the father and mother of Ko.

1923. Sientje is almost an adult and has an adventurous temperament. The son of her neighbour, Gerrit van Buren, has a crush on Sientje, but she craves more excitement. She meets the artist Jan Meiren and is fascinated by his art and smooth manners, and decided to have him teach her to sing and dance, and to perform together. She ends up in the etablissement of Belze Marie where she quickly becomes a celebrated star. Jan en Sien keep performing in Belzes café, but their relation doesnt improve, especially when Sientje gets pregnant.  Sientjes father, Ko, visits them in the café, who sees a lot of similarities between Sientje and her mother; he predicts she will end up in the gutter, but Sientje doesnt listen.

1932. Belze Marie and her husband have a smooth-running cabaret in Den Haag, and Jan Sien follow them there. But there relation is struggling. Jan is showing an interest in the blond dancer Angelique, and when these two start a relation, Sientje flees from the café. At New Years Eve 1933 Sientje and Jan break up for good. Sientjes father shows up, and she follows him back to Rotterdam to take care of her grandparents.

==Cast==
* Sien Breman – Beppie Nooij jr
* Sien junior – Willeke Alberti
* Oma Breman – Myra Ward
* Opa Breman – Wim van den Brink
* Mooie Frans – Cor van Rijn
* Keesbaas, client of Sien sr. – Wim Kouwenhoven
* Ko Breman, Siens father – Kees Brusse
* Van Buren,- Sacco van der Made
* Gerrit van Buren (until 11 years old) – Alf van Beem
* Gerrit – Peter Faber
* Jan Meier – Jules Hamel
* Tootje de Vries – Mimi Kok
* Tonio – Gerard Thoolen
* Belze Marie – Rosy Parrish
* Accordionist Belze Marie – John Woodhouse
* Angelique – Geert de Jong
* Baron – Guus Oster
* Tiny Tino – Toby Rix

==References==
 

== External links ==
*  

 
 
 
 
 
 