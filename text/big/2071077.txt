Christine (1983 film)
{{Infobox film
| name           = Christine
| image          = ChristinePoster.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = John Carpenter
| producer       = Richard Kobritz Larry J. Franco
| screenplay     = Bill Phillips
| based on       = Christine (novel)|Christine&nbsp;by  
| starring = {{Plainlist|
* Keith Gordon John Stockwell
* Alexandra Paul
* Robert Prosky
* Harry Dean Stanton
}}
| music          = John Carpenter Alan Howarth
| cinematography = Donald M. Morgan
| editing        = Marion Rothman
| studio         = Columbia Pictures Delphi Premier Productions Polar Film
| distributor    = Columbia Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $9.7 million
| gross          = $21 million   
}} horror thriller John Stockwell, homonymous novel by Stephen King, published in 1983. The story, set in 1978, follows a sentient and violent automobile named "Christine", and its effects on Christines teenaged owner.

==Plot== John Stockwell). Arnies life begins to change when he buys a used red-and-white 1958 Plymouth Fury, nicknamed "Christine", in need of extensive repairs. Arnie begins to restore Christine, but as he spends more of his time repairing her, he begins to change. Formerly shy, Arnie develops a cocky arrogance. Dennis and Arnies new girlfriend, Leigh Cabot (Alexandra Paul), discover the cars previous owner, Roland LeBay, became obsessed with Christine and later died in it from carbon monoxide poisoning. 

A group of bullies at school—angry with Arnie after a shop class confrontation results in Buddy Repperton (William Ostrander) getting expelled—vandalize Christine, leaving her ruined. Arnie is left devastated and determined to fix Christine again. As he looks the wreck over and hears the creaking of metal, he sees the engine is now fully restored. Arnie tells the car "show me", and Christine restores herself to showroom quality and then seeks out the members of the gang who attempted to destroy her, killing them one at a time. 

On New Years Eve, Dennis and Leigh reason that the only way to stop Christine and save Arnie is to destroy the car. Dennis scratches "Darnells Tonight"—referencing the name of a local junkyard—into Christines hood, then makes his way there with Leigh. Dennis waits in a bulldozer while Leigh heads to the office so that she can shut the door after Christine arrives, trapping the car. Christine, who has been lying in wait the entire time, shines the headlights from under a pile of garbage and the car charges after Leigh. Christine crashes into Darnells office in an attempt to kill Leigh, and Arnie is thrown through Christines windshield. He is impaled on a shard of glass and survives just long enough to admire Christine one last time. Christine continues to attack Dennis and Leigh, sustaining damage and regenerating. Dennis pulls Leigh into the cab of the bulldozer; the two then crush Christine with the bulldozer, compacting her into a cube.

Dennis and Leigh survive and leave behind the remains of the car. The closing shot of the film is of the crushed cube that was Christine while a piece of the grill slowly begins to unbend.

==Cast==
* Keith Gordon as Arnold "Arnie" Cunningham John Stockwell as Dennis Guilder
* Alexandra Paul as Leigh Cabot
* Robert Prosky as Will Darnell
* Harry Dean Stanton as Detective Rudolph "Rudy" Junkins
* Christine Belford as Regina Cunningham
* Roberts Blossom as George LeBay
* Kelly Preston as Roseanne
* William Ostrander as Clarence "Buddy" Repperton
* Malcolm Danare as Peter "Moochie" Welch
* Steven Tash as Richard "Richie" Trelawney
* Stuart Charno as Donald "Don" Vandenberg
* David Spielberg as Mr. Casey

==Production==
 
Kings novel, the source material for Carpenters film, made it clear that the car was possessed by the evil spirit of its previous owner Roland D. LeBay, whereas the film version of the story shows that the  evil spirit surrounding the car was present on the day it was built.
 Belvedere and the Plymouth Savoy|Savoy, were also used to portray the malevolent automobile onscreen. Total production for the 1958 Plymouth Fury was only 5,303, and they were difficult to find and expensive to buy at the time. In addition, the real-life Furys only came in one color, "Buckskin Beige", seen on the other Furies on the assembly line during the initial scenes of the movie. Several vehicles were destroyed during filming, but most of the cars were Savoy and Belvedere models dressed to look like the Fury. Of the twenty cars used in the film, only two still exist; One is a stunt vehicle with a manual transmission and now resides in the hands of a private California collector   and the other vehicle was rescued from a junkyard and restored by collector Bill Gibson of Pensacola, Florida.
 

==Release==
Christine was released in North America on December 9, 1983 to 1,045 theaters.

===Box office===
In its opening weekend Christine brought in $3,408,904 landing at #4. The film dropped 39.6% in its second weekend, grossing $2,058,517 slipping from fourth to eighth place. In its third weekend, it grossed $1,851,909 dropping to #9. The film remained at #9 its fourth weekend, grossing $2,736,782. In its fifth weekend, it returned to #8, grossing $2,015,922. Bringing in $1,316,835 it its sixth weekend, the film dropped out of the box office top ten to twelfth place. In its seventh and final weekend, the film brought in $819,972 landing at #14, bringing the total gross for Christine to $21,017,849. 

===Critical reception===
Based on 23 reviews collected by Rotten Tomatoes, Christine has an overall 65% approval rating from critics, with an average score of 5.7 out of 10.   

==Soundtrack (original score)  ==
{{Infobox album  
| Name       = Christine: Music from the Motion Picture
| Type       = Film Alan Howarth
| Cover      = ChristineSoundtrack.jpg
| Released   = June 1, 1990
| Recorded   =
| Genre      = Soundtrack
| Length     = 33:14
| Label      = Varèse Sarabande
| Producer   = John Carpenter and Alan Howarth
}}

Two soundtracks were released, one consisting purely of the music written and composed by John Carpenter and Alan Howarth, the other consisting of the contemporary pop songs used in the film.   
{{Track listing
| headline        = Christine: Music from the Motion Picture (by John Carpenter and Alan Howarth)
| title1          = Arnies Love Theme
| length1         = 1:15
| title2          = Obsessed with the Car
| length2         = 2:07
| title3          = Football Run/Kill Your Kids
| length3         = 2:42
| title4          = The Rape
| length4         = 1:10
| title5          = The Discovery
| length5         = 1:30
| title6          = Show Me
| length6         = 2:36
| title7          = Moochies Death
| length7         = 2:25
| title8          = Junkins
| length8         = 3:33
| title9          = Buddys Death
| length9         = 1:27
| title10         = Nobodys Home/Restored
| length10        = 1:44
| title11         = Car Obsession Reprise
| length11        = 1:53
| title12         = Christine Attacks (Plymouth Fury)
| length12        = 2:30
| title13         = Talk on the Couch
| length13        = 1:23
| title14         = Regeneration
| length14        = 1:25
| title15         = Darnells Tonight
| length15        = 0:13
| title16         = Arnie
| length16        = 1:01
| title17         = Undented
| length17        = 1:54
| title18         = Moochie Mix Four
| length18        = 2:26
}}

==Soundtrack (songs used in the film)==
The soundtrack album containing songs used in the film was entitled Christine: Original Motion Picture Soundtrack and was released on LP and cassette on Motown Records.  It contained 10 (of the 15) songs listed in the films credits, plus one track from John Carpenter and Alan Howarths own score. The track listing was as follows:
# George Thorogood and the Destroyers - Bad to the Bone Buddy Holly Not Fade Away
# Johnny Ace - Pledging My Love We Belong Together
# Little Richard - Keep A-Knockin
# Dion and The Belmonts - I Wonder Why The Viscounts - Harlem Nocturne
# Thurston Harris - Little Bitty Pretty One
# Danny & The Juniors - Rock n Roll is Here to Stay
# John Carpenter & Alan Howarth - Christine Attacks (Plymouth Fury)
# Larry Williams - Bony Moronie

The following tracks were not included on this LP release, but were used in the film and listed in the films credits:
 The Name of the Game
* Bonnie Raitt - Runaway
* Richie Valens - Come on Lets Go Not Fade Away Beast of Burden

==See also==
*Two Black Cadillacs, a music video by Carrie Underwood, in which two cheated women team up to murder their loved one with the help of a car with a mind of its own.
* "A Thing about Machines", a 1960 episode of The Twilight Zone
* "You Drive", a 1964 episode of The Twilight Zone in which the car of a hit-and-run driver hounds him to confession
*My Mother the Car, a 1965 television sit-com series anthropomorphic 1963 Beetle racecar same name by Theodore Sturgeon anthropomorphic customized 1971 Lincoln Continental Mark III
*The Hearse, a 1980 horror movie about a possessed hearse Knight Rider, a franchise—begun in 1982—featuring an artificially intelligent third generation Pontiac Firebird Trans Am named KITT (Knight Industries Two-Thousand)
*Nightmares (1983 film)|Nightmares, a 1983 movie consisting of four separate story segments; the third segment, "The Benediction", features a traveling priest (played by Lance Henriksen) attacked on the highway by a demonic pickup truck
*Maximum Overdrive, a 1986 horror movie; and Trucks (film)|Trucks, a 1997 made-for-TV remake film; both based on the short story Trucks (short story)|Trucks by Stephen King
*The Wraith, a 1986 film starring Charlie Sheen, who plays a man murdered by a gang of car thieves who gets revenge upon his killers by returning as a phantom car and driver set out to eliminate them
*"The Honking", a 2000 Futurama episode in which the robot character, Bender, is possessed by a were-virus, transforming him into a murderous car every night at midnight. The curse could only be lifted by destroying the originator of the virus, a project-Satan car located at the "Anti-Chrysler" building. The car that hits Bender is actually a 1958 Plymouth, just like the one in Christine. Supernatural episode about a driverless black Dodge utiline truck in Cape Girardeau, Missouri killing everyone related to its owners past. Road Kill, a 2010 Australian supernatural thriller about a group of teenagers menaced by a driver-less road-train in the harsh Australian outback.
*Super Hybrid, a 2011 Science Fiction Horror thriller film about a malicious shape shifting sentient car that devours its victims by tricking them into its cab.
*"Christrina", a Grojband episode where Kins dream catching camera messed up and released Trinas soul from her body and into her car, bringing her car to life. Trina became angry about this and went around on a rampage destroying everything in her car body.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 