Saturday Morning (1922 film)
{{Infobox film
| name           = Saturday Morning
| image          =
| image_size     =
| caption        = Tom McNamara Robert F. McGowan
| producer       = Charley Chase Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       = Jack Davis William Gillespie Ernie Morrison Sr.
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 20 minutes
| country        = United States English intertitles
| budget         =
}}

Saturday Morning is the fifth Our Gang short subject comedy released. The Our Gang series (later known as "The Little Rascals") was created by Hal Roach in 1922 in film|1922, and continued production until 1944 in film|1944.

==Synopsis==
The various boys of the gang each manage to escape their responsibilities for the day, meeting up to build a raft for their pirate game.

==Notes==
When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into television syndication as "Mischief Makers" in 1960 under the title Music Lesson. About two-thirds of the original film was included. Most of scenes from the first half of the film that showed the various gang members at home were cut; all of the original inter-titles were also cut.  The film was also released for home viewing as "Hooray for Holidays".

==Cast==

===The Gang===
* Jackie Condon as Jackie
* Mickey Daniels as Mickey Jack Davis as Waldemar
* Allen Hoskins as Maple
* Ernest Morrison as Sorghum
* Dinah the Mule as Herself

===Additional cast=== William Gillespie as Waldemars father
* Joseph Morrison as Aunty Jackson
* Katherine Grant as The maid
* Richard Daniels as The cello teacher

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 