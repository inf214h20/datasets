Just Add Water (film)
{{Infobox Film
| name           =  Just Add Water
| image          = Just add water.jpg
| caption        = Promotional poster|
| director       = Hart Bochner
| writer         = Hart Bochner
| starring       = Dylan Walsh Tracy Middendorf Danny DeVito Penny Balfour Justin Long Anika Noni Rose Jonah Hill Brad Hunt
| producer       =Clifford Werber
| producer       = Robin Bissell
| distributor    = 
| released       = June 17, 2008
| rating         =  
| runtime        =
| language       = English
}}
Just Add Water is a comedy film released on June 17, 2008. The film stars Dylan Walsh as a hardworking man living in the same small town in which he grew up, Danny DeVito as a gas station owner, and Justin Long as a meth dealer.

== Plot ==
This film is an offbeat romantic comedy about Ray Tuckby (Dylan Walsh), a decent guy with a dead-end life in the dead-end town of Trona, San Bernardino County, California|Trona, California. After discovering that his wife has had an affair with his brother Mark and that his son Eddie (Jonah Hill) is actually Marks son, he decides to start his life all over again.
 meth baron, Dirk (Will Rothhaar).
 Chevron gas station operator, Ray begins to dream again.

Ray musters the nerve to pursue his childhood love, Nora (Tracy Middendorf), and after Dirk shuts down the towns electricity and water supplies, Ray and his neighbors finally plot a plan to take back his community by toppling Dirk.

Ray then goes on to marry Nora and opens a restaurant with Nora, using his mothers lemon meringue pie as the signature dish. Eddie works as his chef while Spoonie, Denny and his wife work as waiters. Nora is also shown to be expecting a baby.

==Cast==
*Dylan Walsh as Ray Tuckby
*Tracy Middendorf as Nora
*Danny DeVito as Merl Stryker
*Penny Balfour as Charlene
*Will Rothhaar as Dirk
*Justin Long as Spoonie
*Melissa McCarthy as Selma
*Anika Noni Rose as Rchlle
*Jonah Hill as Eddie Tuckby
*June Squibb as Mother
*Brad Hunt as Denny
*Lindsey Axelsson as Chrisy
*Chelsea Field as Jeanne

==External links==
* 

 
 