Kauravar
 
{{Infobox film
| name           = Kauravar
| image          = Kauravar-5798.jpg
| image_size     =
| alt            =
| caption        = Movie poster
| director       = Joshi
| writer         = A. K. Lohithadas
| narrator       = Vishnuvardhan Thilakan Anju
| music          = S P Venkatesh
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 149 min
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}

Kauravar (  directed by Joshiy, written by A. K. Lohithadas|Lohithadas, and starring Mammootty, Vishnuvardhan (actor)|Vishnuvardhan, Thilakan, Murali (Malayalam actor)|Murali, Anju (actress)|Anju, Babu Antony and Bheeman Raghu.

The film was remade in Telugu as Khaidi Garu with Mohan Babu. It was also remade into Kannada in 2001 by the title Devasura and starring Devraj and B. C. Patil.

==Plot==
A gang led by Aliyar (Thilakan) holds a grudge against Police Officer Haridas (Vishnuvardhan (actor)|Vishnuvardhan) who was responsible for the death of Antonys (Mammootty) wife and daughter 12 years ago and sent Antony to jail. When Antony is finally freed, he reunites with his old gang and they plan to kill Haridas. Antony then discovers that his daughter who he thought had died is actually still alive and was raised by Haridas as one of his daughters. Antony meets his long-lost daughter but Aliyar and gang are not ready to leave Haridas. Antony pleads to the gang to leave Haridas alone but fails to stop them. During the struggle, Antony gets shot by Aliyar and in his defence kills Aliyar.

==Reception==
The movie was a big hit among family audience. It ran for 150 days. The film dubbed in Telugu as Kankanam and became a big hit in Andhra Pradesh.
The film dubbed in Tamil as Shatriya Vamsam and became a big hit in Tamil Nadu. 

== Cast ==
* Mammootty as  Antony Vishnuvardhan as  Haridas I.P.S.
* Thilakan as  Aliyar
* Babu Antony as Hamsa Murali as Rajagopal (Commissioner of Police)
* Shanthi Krishna as Commissioners wife
* Bheeman Raghu as Ramayyan Devan as goergemathew MLA Rudra
* Azeez  as kannannair Santhosh
* Kanakalatha Anju as seetha
* Kuthiravattom Pappu as Pillai
* Vinu Chakravarthy as police inspector Kunchan as police
* K. P. A. C. Sunny 
==Soundtrack==
The films soundtrack was composed by S P Venkatesh. Lyrics were penned by Kaithapram.

{{tracklist
| headline     =
| extra_column = Artist(s)
| total_length =
| title1       = Kanaka Nilaave
| extra1       = Dr. K. J. Yesudas, K. S. Chitra
| length1      = 4:53
| title2       = Muthu Mani Thooval 
| extra2       = Dr. K. J. Yesudas
| length2      = 4:43
| title3       = Maari Kuliril 
| extra3       = Dr. K. J. Yesudas
| length3      = 5:14
| title4       = Maari Kuliril
| extra4       = K. S. Chitra
| length4      = 1:49
}}

==References==
 

==External links==
*  

 
 
 
 
 
 