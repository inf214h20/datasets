Backlash (1947 film)
{{Infobox film
| name           = Backlash
| image          = Backlash Poster.jpg
| image size     = 
| alt            =
| caption        = Theatrical release poster
| director       = Eugene Forde
| producer       = Sol M. Wurtzel
| screenplay     = Irving Elman
| narrator       = Richard Travis John Eldredge
| music          = Darrell Calker
| cinematography = Benjamin H. Klinei
| editing        = William F. Claxton
| studio         = Sol M. Wurtzel Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
}}
 John Eldredge Richard Travis. 

==Plot== John Eldredge) to tell him of client Red Baileys (Douglas Fowley) jail escape. Shortly thereafter, an armed Bailey flags down Morlands car, which police find wrecked, its drivers face beyond recognition.
 Richard Travis) have been having a secret affair, while his doctor says Morland once swallowed poison that his wife tried to give him.
 Leonard Strong). The evidence of an affair grows until Catherine is arrested and Conroy resigns as DA.

Bailey, meantime, resurfaces with Marian Gordon (Louise Currie), a girlfriend. McMullen has a phone tapped and follows them. He learns that Bailey was hidden by Morland at the cabin, but then they had a fight and Bailey knocked him cold. 

Catherine is released. She finds ONeil dead at the lodge. Her husband, Morland, is alive. He has been behind this all along, going so far as to fake being poisoned to frame his wife. The body in the car is the caretakers. It is a diabolical plan, but McMullens on the case.

==Cast==
* Jean Rogers as Catherine Morland Richard Travis as Richard Conroy
* Larry J. Blake as Det. Lt. Jerry McMullen John Eldredge as John Morland Leonard Strong as Willis, the caretaker
* Robert Shayne as James ONeil
* Louise Currie as Marian Gordon
* Douglas Fowley as Red Bailey
* Sara Berner as Dorothy, the maid
* Richard Benedict as Det. Sgt. Tom Carey
* Wynne Larke as Patricia McMullen
* Susan Klimist as McMullen girl Billy Gray as Denny

==Reception==
When the film was released, The New York Times film critic, Thomas M. Pryor, panned the film, writing, "When a movie company lets two of its feature pictures be sold first-run for the price of one, you can bet your last dollar that even the studio has little faith in the product. With that in mind, we can move on to a fast appraisal of Backlash and Jewels of Brandenburg, the twin bill sponsored by Twentieth Century-Fox and now showing at the Rialto. Both are melodramas and, to get the unpleasantness over with as quickly as possible, they are the type which reflect absolutely no credit upon anyone connected with them, except, possibly, the studio cutter, who pared them down to a little over sixty minutes each." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 