Brand upon the Brain!
{{Infobox film
| name           = Brand upon the Brain!
| image          = Brand upon the brain.jpg
| caption        = Theatrical film poster
| director       = Guy Maddin
| producer       = The Film Company
| writer         = Guy Maddin George Toles
| starring       = Sullivan Brown Gretchen Krich Maya Lawson Erik Steffen Maahs Katherine E. Scharhon
| music          = Jason Staczek
| cinematography = Benjamin Kasulke
| editing        = John Gurdebeke
| studio          = The Film Company
| distributor    = The Film Company Vitagraph Pictures
| released       =  
| runtime        = 99 minutes
| country        = Canada United States Silent
| budget         = $40,000
}}

Brand upon the Brain! (2006) is a feature film directed by Guy Maddin and produced by The Film Company, a Seattle-based not-for-profit film production company that offered Maddin a budget to make any film he wanted, with complete freedom as long as he shot it in Seattle with local actors.  Maddin directed the film from a script co-written by Toles, shooting over nine days and editing over three months,  on an estimated budget of $40,000. 

==Plot==
Guy Maddin (played by Erik Steffen Maahs as an adult, and Sullivan Brown as a child), returns to Black Notch, a deserted island on which stands a lighthouse that was his family home, an orphanage run by his parents, to slap a fresh coat of paint on the lighthouse. The film is divided into twelve chapters, each of which is a flashback that Guys ancestral house-painting has brought to the fore of his memory. 

Guy, twelve years old in his memory, attends a secret meeting of orphans run by Savage Tom, a believer in pagan rituals. Tom says he will cut out the heart of Guys friend Neddie but Guys mother interrupts through the use of her "aerophone," a radio/loudspeaker that Guys domineering mother uses to communicate across the entirety of the island and so keep control of her children, whom she also spies on with the help of a telescope mounted with the lighthouses revolving light. In the orphanage/lighthouse, mother delights in repressing the orphans desires as fully as possible, especially the sexual yearnings of Sis. Mother relates that she herself was an orphan because Maddins grandmother was bald and scalped her sister for her hair, while her sister was so jealous of the pregnancy that Maddins mother was literally cut out of her own mothers stomach. Maddins father, little-seen, spends his time in a basement laboratory while Mother oversees all else.

In the woods one day Guy meets a young girl, Wendy Hale, a famous teen detective investigating why orphans adopted from the island all have holes bored in the backs of their heads. Guy falls for Wendy and the two join Sis and Neddie for a game of spin the bottle. Wendy falls in love with Sis and impersonates her twin brother Chance in order to pursue her. Guy develops a "Boy crush" on the disguised Wendy/Chance, who moves into the orphanage to further her/his investigation. Guy helps Wendy/Chance investigate and they discover that Father is using a sharp signet ring to drill into the skulls and draw nectar from their brains of the orphans (and his own children). The nectar is harvested and shipped to the mainland, and also used to extend Mothers youth. She becomes twenty years younger, and hopes to eventually return to infancy, but the effects are daily reversed by the age-ifying efforts of keeping Sis and the other children in line and properly repressed. Sis being the biggest problem, Mother sends her for additional nectar-harvesting, but the over-harvesting causes Sis to murder Father in self-defense. 

Father is buried near the water and the orphans have to jump on the coffin so that it will sink into the flooded grave. Mother attempts suicide by dramatically taking poison and calling the orphans to her bedside to witness the lengths to which theyve driven her. Sis has discovered that Chance is Wendy but nevertheless plans to marry the girl. Mother is enraged by the marriage and threatens to tell Father. To accomplish this, she exhumes the corpse and "boosts" it back to life using jumper cables connected to her own racing, nectar-infused heart. The zombie Father resumes his normal activities. Mother curses Sis further and becomes frenzied with hunger for more nectar. Guy stumbles upon Mother in the woods, eating through Neddies skull. The crime compels Sis to force Mother, Father, and Savage Tom from the island in a rowboat. Guy, left on the island, and his Mother exchange calls of love over the water. Guy is soon sent off the island himself and into foster care. 

Present-day Guy finishes painting the lighthouse, and encounters the ghost of Wendy, who tells him that Sis took over his Mothers place, to become just as tyrannical. She continued to harvest nectar from the orphans, and finally Wendy/Chance abandoned her and fled the island. This drove Sis to madness and to combusting in the lighthouses lamp. Mother returns to the island, now blind, with the undead Father in tow. She attempts to restore her past regime, with Guy (her lone remaining child) her sole focus. Guy resists but life is less dramatic than before, until Father is murdered by sailors who were formerly orphans he victimized (they stuff him in a trash can and set him on fire). Mother soon readies to die, and Guy readies to catch her dying breath in a glass bottle. However, the ghost of Wendy/Chance distracts him and Mother dies furious with him for his inattention. Guy is left alone on the island, torn between the past and the future, contemplating suicide.

==Cast==
* Gretchen Krich as Mother
* Cathleen OMalley as Young Mother
* Susan Corzatte as Old Mother
* Sullivan Brown as Young Guy Maddin
* Erik Steffen Maahs as Older Guy Maddin
* Maya Lawson as Sis
* Katherine E. Scharhon as Chance Hale/Wendy Hale
* Todd Moore as Father
* Clayton Corzatte as Old Father
* Andrew Loviska as Savage Tom
* Kellan Larson as Neddie

==Release== Foley artists. The film was toured across North America in a similar fashion, with a host of celebrity narrators including Crispin Glover and John Ashbery. The films normal theatrical run featured narration by Isabella Rossellini. Brand Upon the Brain! Dir. Guy Maddin. Produced by The Film Company, 2006. DVD distributed by Criterion Collection, 2008.  At the New York Film Festival in October 2006, the film was narrated by Isabella Rossellini and accompanied by the Sospeso Collective. Rossellini also narrated at the Berlin International Film Festival in February 2007, where the score was played by the Volkswagen Orchestra. All live orchestra performances were conducted by the scores composer, Jason Staczek.

Brand Upon the Brain! was released on DVD in 2008 by The Criterion Collection and features narration tracks by Isabella Rossellini, Laurie Anderson, John Ashbery, Louis Negin, Crispin Glover, Eli Wallach, and Maddin himself. 

==Critical reception==
Brand Upon the Brain! was well-received, with a 92% approval rating at  .   at Metacritic, retrieved May 1, 2008 

Roger Ebert described the film as "a phantasmagoric story that could be a collaboration between Edgar Allan Poe and Salvador Dalí",    and wrote, of the film and Maddins work in general, that "Maddin seems to penetrate to the hidden layers beneath the surface of the movies, revealing a surrealistic underworld of fears, fantasies and obsessions."  Carrie Rickey emphasized the films connection to the horror genre, calling it "a feverishly imaginative Freudian vampire film."  Andrew Sarris, in the New York Observer, called the film "one of the most compelling avant-garde excursions into the narrative cinema ever." 

==References==
 

==External links==
*  
*  at The Film Company
* 
* 
*  at Rotten Tomatoes
*  at MetaCritic
* 

 

 

 

 
 
 
 
 
 