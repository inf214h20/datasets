Ghost in Love
{{Infobox film
| name           = Ghost in Love
| image          = Ghost in Love film poster.jpg
| caption        = Theatrical poster
| director       = Lee Kwang-hoon
| producer       = Kang Woo-suk
| writer         = Li Hong-zhou
| starring       = Kim Hee-sun Lee Sung-jae Cha Seung-won Yu Hye-jeong Jang Jin-young
| music          = Oh Jin-woo 
| cinematography = Park Hyeon-cheol
| editing        = Lee Hyeon-mi
| distributor    = Cinema Service
| released       =  
| runtime        = 91 minutes
| country        = South Korea
| language       = Korean
| budget         = 
}}
Ghost in Love ( ; aka Suicide Ghost Club) is a 1999 South Korean film written by Li Hong-zhou and directed by Lee Kwang-hoon. The film stars Kim Hee-sun in the title role as the girlfriend of a man she suspects of cheating on her.  She throws herself underneath an oncoming train (with some help from nearby ghosts), and discovers that in the afterlife she can roam as a ghost and take revenge, if she wants to, on her former boyfriend, who has quickly moved on. Lee Sung-jae also stars as Kantorates, a ghost who befriends the protagonist. The film was released on August 14, 1999.

== Cast ==
* Kim Hee-sun ... Jin Chae-byul
* Lee Sung-jae ... Kantorates
* Cha Seung-won ... Na Han-su
* Yu Hye-jeong ... Baek
* Jang Jin-young ... Lee Young-eun
* Lee Yeong-ja ... Deity
* Myeong Gye-nam ... Sales manager (1)
* Park Kwang-jeong ... Sales manager (2)
* Jeong Won-jung ... Messenger of Death (1)
* Jang Se-jin ... Messenger of Death (2)
* Kim Si-won ... Hyun-ju
* Jae Hee ... Kantoratess younger brother
* Kim Myeong-su

== External links ==
*  

 
 
 
 
 
 
 


 