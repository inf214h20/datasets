Supercross (film)
{{Infobox film
| name           = Supercross
| image          = Supercross film.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Steve Boyum Steve Austin Richard Gabai J. Todd Harris
| screenplay    = Ken Solarz Bart Baker
| story             = Bart Baker Keith Alan Bernstein Steve Howey Mike Vogel Sophia Bush Cameron Richardson Channing Tatum
| music          = Jasper Randall
| cinematography = William Wages
| editing        = Alan Cody Brett Hedlund
| studio         = Tag Entertainment
| distributor    = 20th Century Fox
| released       =      
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $3,344,431 
}}
 Steve Howey and Mike Vogel. The movie is a mixture of youthful relationships set in the intense world of professional Supercross.

==Plot synopsis==
K.C. Carlyle (Howey) and Trip (Vogel) are brothers who compete in a supercross, a race involving off-the-road motorcycles on an artificial dirt track.  K.C. accepts a lucrative deal to race on a factory team and leaves behind his brother to fund his own racing.  When an accident disables Trip, K.C. resolves his differences with him, and Trip helps coach K.C. to win the supercross championship. 

==Cast== Steve Howey as K.C. Carlyle
*Mike Vogel as Trip Carlyle
*Cameron Richardson as Piper Cole
*Sophia Bush as Zoe Lang
*Channing Tatum as Rowdy Sparks
*Robert Carradine as Clay Sparks
*Robert Patrick as Earl Cole
*Aaron Carter as Owen Cole
*J. D. Pardo as Chuy
*Jamie Little as Herself
*Ricky Carmichael as Himself

==Production==

Supercross: The Movie is the full title of the 35mm color motion picture, first presented in Brentwood during a motorcycle event on the evening of August 17, 2005, then on August 19 released into full worldwide theatrical distribution. The director of photography for the dramatic segments was William Wages. Talk is that he was asked to re-create a sort of "olden times" look to the movie, by purposely overexposing the acting segment footage. The grainy and at times thin film is in contrast with the deep and clear cycle racing photography. Among the very finest action film makers are to be credited with the high quality look during actual motorcycle race events, that features the work of a sky train cinema camera helped along by top notch film production experts. Corporate team work created for the movie some of the best imagery that Clear Channel has to offer. Also, Steve Boyum excelled during his strong points of motorcycle race and stunt work photography. To make the filming story line, actors and actor double cycle racers David Castillo and Rich Taylor had to qualify for the cycle racing competition events up through and including the World Supercross Championship final race. During the finals, there was also a ten minute break between the racing events, so that the stunt doubles could complete some work needed to film specific contest action during the movies final minutes. Modeled sound effects mix it up with an IMAX style combination of extreme close-up and fine focus film footage during most of the actual Motocross and Supercross live action events. The Las Vegas Supercross final events live action filmed using an over-the-stadium cable camera, brings a new dimension to motorcycle racing cinematography. 

==Soundtrack==

Supercross is the soundtrack to the motocross film "Supercross". It has not been released yet but here are the tracks listed in the film.

;Track listing
#Saturday Night         - 4:01 - (Ozomatli)
#Pirates                    - 2:20 - (Bullets and Octane)
#California Records   -         - (Longbeach Shortcuts)
#Make Them Believe - 3:47 - (Fu Manchu)
#Thats the Way It Is         - 3:12 - (Powerman 5000)
#Tear Up This Town - 3:27 - (Leif Garrett)
#Chemical                - 3:48 - (Joseph Arthur) (aka Start Trouble)
#Get Out Alive         - 3:27 - (Socialburn)
#Days of My Life    - 4:00 - (City of London)
#Ride of Your Life   - 3:00 - (John Gregory)
#Things Ive Done    - 5:00 - (Natural)
#Everytime              - 4:00 - (Rusty Truck)
#Every Second        - 3:00 - (Change of Pace)

==References==
 

==External links==
* 
* 


 

 
 
 
 
 