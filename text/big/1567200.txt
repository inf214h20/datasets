The Saint (film)
{{Infobox film
| name           = The Saint
| image          = The Saint 1997 poster.jpg
| image_size     =
| caption        = original theatrical poster
| alt            =
| director       = Phillip Noyce David Brown Robert Evans William J. MacDonald Mace Neufeld
| writer         = Characters:   Wesley Strick
| narrator       =
| starring       = Val Kilmer Elisabeth Shue Rade Šerbedžija
| music          = Graeme Revell
| cinematography = Phil Meheux
| editing        = Terry Rawlings
| studio         = Mace Neufeld Productions Rysher Entertainment
| distributor    = Paramount Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $68,000,000 (est.)
| gross          = $169.4 million
}}
 espionage thriller thriller film, starring Val Kilmer in the title role, with Elisabeth Shue and Rade Šerbedžija, directed by Phillip Noyce and written by Jonathan Hensleigh and Wesley Strick. The title character is a high tech thief and master of disguise that becomes the anti-hero while using the moniker of various saints while paradoxically living in the underworld of international industrial theft. The film was a financial success with a worldwide box office of $169.4 million, rentals of $28.2 million, and continuous DVD sales. http://www.imdb.com/title/tt0120053/business     
 radio series British The television series of the 1960s which starred Roger Moore, and a 1970s series starring Ian Ogilvy.

==Plot==
 Saint Ignatius John Rossi refers to himself as "Simon Templar" and leads a group of fellow orphans as they attempt to run away to escape their harsh treatment. When Simon is caught by the head priest, he witnesses the tragic death of a girl he had taken a liking to when she accidentally falls from a balcony.
 oligarch that Russian President. electrochemist Emma Russell (Elisabeth Shue). He wishes to acquire Emmas formula—which creates clean, inexpensive energy—so he can monopolize the energy market during a severe oil shortage in Russia.
 Inspector Teal Russian police, loyal to Tretiak, arrest Simon and Emma. However, they manage to escape from the police van as they are being brought to Tretiaks mansion.
 fence who sells them the directions through an underground sewer system that lead to the American embassy. Simon and Emma exit the sewer tunnel only to find Ilya and his men waiting for them among a gathering of protestors outside the embassys front gates. Emma safely makes it to the embassy for political asylum, while Simon allows himself to be caught by Ilya as a distraction. He escapes after rigging a car bomb that severely burns Ilya.

Simon plants a listening device in Tretiaks office and learns he plans to perform a coup détat by selling the cold fusion formula to Russian President Karpov to frame him for wasting billions on useless technology. Tretiak then plans to use the political fallout to install himself as President. Emma finishes the equations to complete the formula, and Simon delivers the information to Tretiaks physicist, Dr. Lev Botkin (Henry Goodman), who builds an apparatus which proves the formula works. Simon infiltrates the Presidents Kremlin residence and informs him of Tretiaks conspiracy just before Tretiak loyalists detain him. In front of a massive gathering in Red Square, Tretiak makes public accusations against President Karpov, but when the cold fusion reactor is successfully initiated, Tretiak is exposed as a fraud and arrested. He is also revealed to have caused the heating oil shortage in Moscow by illegally stockpiling vast amounts of heating oil underneath his mansion.

Sometime later, at a news conference at the University of Oxford, Emma presents her cold fusion formula to the world. Simon attends the conference in disguise and once again avoids being captured by Inspectors Teal and Rabineau when they spot him in the crowd. As he drives away, he listens to a news radio broadcast (voiced by Roger Moore) reporting that $3 billion was recently donated to the Red Cross, Salvation Army and the United Nations Childrens Fund. It is implied that Simon, who had access to Tretiaks accounts, donated the money anonymously. Furthermore, a non-profit foundation led by Dr. Botkin is being established to develop the cold fusion technology.

==Cast==
* Val Kilmer as Simon Templar
* Elisabeth Shue as Dr. Emma Russell
* Rade Šerbedžija as Ivan Petrovich Tretiak
* Valery Nikolaev as Ilya Tretiak
* Henry Goodman as Dr. Lev Naumovich Botkin Inspector Teal Michael Byrne as Vereshagin
* Yevgeni Lazarev as President Karpov
* Irina Apeksimova as Aleksa "Frankie" Frankeyevich
* Lev Prygunov as General Leo Sklarov
* Charlotte Cornwell as Inspector Rabineau Tommy Flanagan as Scarface
* Egor Pazenko as Scratchface Adam Smith as Young Simon Templar
* Roger Moore as Radio Announcer Voice David Schneider as Bar Waiter William Hope as State Department Official

== Production   == Andrew Clarke; and a set of feature-length made-for-television adventures produced in Australia in 1989 starring Simon Dutton. Of these, the Moore series remained the definitive television adaptation.

In the mid-1980s, tabloid gossip newspapers such as the National Enquirer reported that Moore was planning to produce a new Saint movie, with Pierce Brosnan (then known for playing the Templar-influenced character Remington Steele on TV) being considered for the role, though nothing came of this project.

The reference work The Saint: A Complete History by Burl Barer (McFarland 1992) was written at a time when another set of plans were under way to launch a new Saint film series, which would have been faithful to the original writings of Leslie Charteris and feature characters from the original books. This project also failed.
 Robert Evans Quiz Show &mdash; was offered $1 million for the lead, but eventually passed. In a 1994 interview for Premiere (magazine)|Premiere magazine, Fiennes said the screenplay &mdash; racing fast cars, breaking into Swiss banks &mdash; was nothing he hadnt seen before.
 David Brown Upstate New York, Saint Petersburg|St. Petersburg, and Moscow. Setpieces included Dr. Russell skydiving while strapped into a wheelchair and a plane landing in Red Square. Darwin Mayflower described it as one of the top unproduced screenplays. {{cite web
| date = 2000-08-08
| author = Darwin Mayflower
| title = Script Reviews: TOP TEN UNPRODUCED SCRIPTS
| url = http://www.screenwritersutopia.com/modules.php?name=Content&pa=showpage&pid=2667
}}
  Phillip Noyce was hired to direct.
 The Saint TV series and the later Return of the Saint revival of the 1970s, Robert S. Baker, the producer of both series, was brought in an executive producer of the film.
 ITV show, Batman & Robin and the script was rewritten by Wesley Strick to suit his style.

Stricks rewrite relocated the action to   franchise. The Saint, as devised by Charteris in the 1930s, used crude disguises instead of the sophisticated ones shown in this film.
 Willis or Mel Gibson), this Saint refrained from killing and even the main villains live to stand trial. Charteris version had no qualms about taking another life.

In the original version of the film &mdash; as in the original Jonathan Hensleigh script &mdash; Emma, having escaped Russia via the American embassy, collapses while giving a lecture and dies in Simon Templars arms. Watching the videotape back, he sees Ilya Tretiak stabbing her in the leg with the tip of his cane. The final half-hour has Simon returning to Moscow to destroy the villains plans and avenge her death. With Dr. Botvins help, he switches the formulas around and humiliates Ivan Tretiak during his show trial of the Russian president. The Tretiaks shoot their way out of the crowd and escape back to their mansion, with Simon and the Russian army in pursuit. Ivan shoots the treacherous Dr. Botvin. Simon arrives and finds the bodies of Botvin and Ivan Tretiak, killed by his own son. Simon battles Ilya on the stairwell as Russian tanks pound the mansion walls, exposing and setting fire to the vast stockpile of heating oil in the basement. With the stairwell disintegrating around them, the fight spills out on to the chandelier, suspended above the blazing oil. Simon teases Ilya with the disc containing the formula for cold fusion. As he reaches out for it, Simon cuts the rope and Ilya plummets to a fiery death. Returning to Emmas home, Simon finds a letter from her, a tear fills his eye and he vows from now on to use his skills only for good. 

Test audiences did not like that Emma died three-quarters of the way into the film; it was confusing as to what had happened to her. The novelization features an alternate version in which Emma lives and Simon and Ilya still battle on the chandelier. In the end the producers decided to cut Emmas death scene, chopped off the action-packed climax, inserted footage of the Tretiaks being arrested and filmed a new epilogue at Oxford. (Footage from the original ending features prominently in the films trailer.)  

The film featured the Volvo C70, a nod to the Volvo P1800 of the original series.

Fort Amherst starred as a filming location for The Saint in 1997. The tunnels were used for the scene in which Simon receives a map in the Kremlin tunnel in Moscow. 

==Novelization==
 
 
A novelization based upon the film script was written by Burl Barer.

==Soundtrack==
 

The Saint won the 1998 BMI Film Music Award. 
 Out of singles to promote the movie.

The films soundtrack album,   included many songs from the electronica age. Aside from Duran Duran and the Sneaker Pimps, recording artists included Orbital (band)|Orbital, Moby, Fluke (band)|Fluke, Luscious Jackson, The Chemical Brothers, Underworld (band)|Underworld, Daft Punk, David Bowie, Dreadzone, Duncan Sheik, The Smashing Pumpkins, Everything but the Girl and the theme "Polaroid Millenium" by British musician Su Goodacre (alias "Superior") which also played during the final credits.

== Reception ==

=== Box office ===
The Saint was the #2 film for its opening weekend, earning $16,278,873 at 2,307 theaters in the United States.  With a domestic gross of $61,363,304, it ranked 28th of 303 movies for 1997  Internationally the film earned
$108 million, with a worldwide total of $169.4 million   (equivalent to $245.3 million in 2012 dollars ).

=== Critical response   ===
Critical response for the film was mixed to negative, with a Metascore of 50/100, based on 22 professional film critic reviews provided by Metacritic.    It has a 30% rating on Rotten Tomatoes based on 44 reviews with the consensus stating: "The Saint is watchable thanks to Val Kilmer and Elisabeth Shue, but the films muddled screenplay stretches credulity." 
 Bond film GoldenEye. It brings back the humour and sangfroid that makes the genre work."  Todd McCarthy of Variety (magazine)|Variety called it a "suspenser that doesnt taste bad at first bite but becomes increasingly hard to swallow". 
 The Postman.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 