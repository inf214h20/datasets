The Secret (1990 film)
{{Infobox film
| name           = The Secret
| image          = Ilsegreto1990.jpg
| caption        = Film poster
| director       = Francesco Maselli
| producer       = Giuseppe Giovannini
| writer         = Francesco Maselli
| starring       = Nastassja Kinski
| music          = Giovanna Marini
| cinematography = Pier Luigi Santi
| editing        = Carla Simoncelli
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Italian drama film directed by Francesco Maselli. It was entered into the 40th Berlin International Film Festival.   

==Cast==
* Nastassja Kinski as Lucia
* Stefano Dionisi as Carlo
* Franco Citti as Franco
* Chiara Caselli as Lilli
* Alessandra Marson
* Franca Scagnetti
* Raffaela Davi
* Enzo Saturini
* Antonio de Giorgi
* Michela Bruni
* Luigi Diberti
* Maria Giovanna Delfino

==References==
 

==External links==
* 

 
 
 
 
 
 
 