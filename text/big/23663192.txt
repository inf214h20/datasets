The Glass Menagerie (1987 film)
 
 
{{Infobox film
| name           = The Glass Menagerie
| image          = MenageriePoster.jpg
| caption        = Promotional poster
| director       = Paul Newman
| producer       = Burtt Harris
| writer         = Tennessee Williams
| starring       = Joanne Woodward John Malkovich Karen Allen James Naughton
| music          = Henry Mancini
| cinematography = Michael Ballhaus
| editing        = David Ray
| distributor    = Cineplex Odeon Films
| released       =  
| runtime        = 134 minutes
| country        = United States
| language       = English
| budget         = 
}}
 play of the same title that originated at the Williamstown Theatre Festival and then transferred to the Long Wharf Theatre in New Haven, Connecticut.    
 1950 feature film and television movies made in 1966 and 1973. It was shown at the 1987 Cannes Film Festival  and the Toronto International Film Festival before opening in New York City on October 23, 1987.

==Plot==
Introduced by Tom Wingfield as a memory play, it is based on his recollection of his disillusioned and delusional mother Amanda and her shy, crippled daughter Laura. Amandas husband abandoned the family long ago, and her memory of her days as a genteel Southern belle surrounded by devoted beaux may be more romanticized than real. Tom is an aspiring writer who works in a warehouse to support his family, and the banality and boredom of everyday life leads him to spend most of his spare time watching movies in local cinemas at all hours of the night. Amanda is obsessed with finding a proper "gentleman caller" for Laura, who spends most of her time with her collection of glass animal figurines. To appease his mother, Tom eventually brings Jim OConnor home for dinner, but complications arise when Laura realizes he is the man she loved in high school and has thought of ever since. He dashes her hopes of a future together when he announces he is engaged. Infuriated, Amanda lashes out at her son for raising his sisters hopes and Tom leaves, never to return to his family.

==Cast==
* Joanne Woodward as Amanda Wingfield
* John Malkovich as Tom Wingfield
* Karen Allen as Laura Wingfield
* James Naughton as Jim OConnor

==Critical reception==
Janet Maslin of The New York Times called the film "a serious and respectful adaptation, but never an incendiary one, perhaps because the odds against its capturing the plays real genius are simply too great. In any case, this Glass Menagerie catches more of the dramas closeness and narrowness than its fire . . .   starts out stiffly and gets better as it goes along . . . But quiet reverence is its prevailing tone, and in the end that seems thoroughly at odds with anything Williams ever intended." 

Desson Howe of the Washington Post observed, "Acting is definitely the trouble in Menagerie. Theres an awful lot of it here. And there are many words - fine words by Tennessee Williams. But before that no-nonsense lens, and as emoted by Malkovich and Woodward, they seem time-consuming, inflated, dated and theatrical. The films few good moments happen when mouths are firmly shut. Which is why Karen Allen, one of the screens great underrated actresses, comes off best. As frail and softspoken daughter Laura, awaiting gentleman callers who never come, shes the best film performer here . . . Woodward, she with the longest resumé, is the disappointment. Apparently understating, she speaks in a low, squeaky tone - a kind of laryngitic falsetto. Its so irritating it makes her moments of hysteria a relief. She is also at her best when wordless . . . Malkovich, as the pivotal Tom, is certainly watchable . . . But as the son bearing his mothers pushiness and the brother tethered to his sisters social infirmity, his actions are obvious and broad. They smell of the stage. Which seems to have been director Newmans intention. But by filming this play in straightforward manner . . . Newman emphasizes the artificiality of theater and distances you from the play." 

Variety (magazine)|Variety called it "a reverent record" of the Williams play "one watches with a kind of distant dreaminess rather than an intense emotional involvement" and cited the "brilliant performances . . . well defined by Newmans direction." 

It currently holds a 67% approval rating on Rotten Tomatoes.

==Availability==
Sometime after the films theatrical run, the film was released on videocassette in the United States in 1988 by MCA Home Video and in Canada that same year by Cineplex Odeon. The film has never been released on DVD and as of January 4, 2010, Universal Studios Home Entertainment has yet to announce any plans for a Region 1 DVD release. As of July 6, 2013, it is currently available through AT&T U-Verse Screen Pack.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 