Swing! (film)
{{Infobox film
| name           = Swing!
| image          =File:Swing poster.jpg
| caption        =Film poster
| director       = Oscar Micheaux
| producer       = Oscar Micheaux (producer)
| writer         = Oscar Micheaux (screenplay) Oscar Micheaux (story "Mandy")
| narrator       =
| starring       = Cora Green Hazel Diaz Dorothy Van Engle
| music          =
| cinematography = Lester Lang
| editing        = Patricia Rooney
| studio         =
| distributor    = Micheaux Film Corporation
| released       =  
| runtime        = 69 minutes
| country        = United States English
| budget         =
| gross          =
| website        =
}}
 1938 American race film directed, produced and written by Oscar Micheaux.

==Plot==
Mandy Jenkins (Cora Green), an African American cook for a wealthy white family in Birmingham, Alabama, discovers her husband Cornell is having an affair with Eloise Jackson (Hazel Diaz). When she confronts her husband and Eloise at a nightclub, a violent fight ensues.  Eloise leaves Birmingham and relocates to the Harlem section of New York City, where she gets a job as a cabaret vocalist under the false name of Cora Smith. She is followed to Harlem by her husband, Lem, who becomes mixed up in the local crime scene.  Mandy also arrives in New York, having left Cornell.  She gets a job as the wardrobe mistress at the cabaret where Eloise is performing. When Eloise breaks her leg during a drunken fall, Mandy is recruited as the last-minute substitute and becomes a musical star as the revue is transferred to Broadway theatre|Broadway. Cornell, who is now destitute, reunites with Mandy and agrees never to cheat on her again. 

=== Differences from source ===
 

== Cast ==
*Cora Green as Amanda "Mandy" Jenkins
*Larry Seymour as Cornell Jenkins
*Hazel Diaz as Eloise Jackson / Cora Smith
*Alec Lovejoy as Lem Jackson / Big Jones
*Amanda Randolph as Liza Freeman
*Trixie Smith as Lucy
*Carman Newsome as Ted Gregory
*Nat Reed as Ted, Gregorys Assistant
*Sammy Gardiner as Sammy, Gregorys Assistant
*Dorothy Van Engle as Lena Powell Doli Armena as Miss Watkins, a Trumpet Player
*Columbus Jackson as A Hustler
*George R. Taylor as Mr. Becker, Theatrical Backer

== Soundtrack == Bei Mir Bist di Schön" (Music by Sholom Secunda, English lyrics by Sammy Cahn and Saul Chaplin)
*Played by Leon Grosss orchestra and sung by Cora Green - "Heaven Help this Heart of Mine" (Music and lyrics by Leonard Whitcup, Walter G. Samuels and Teddy Powell)
*Hazel Diaz - "Once I Did"
*Tyler twins - "I Got Rhythm, Boy"

==Production==
Green performs the Yiddish tune Bei Mir Bistu Shein for her star-making musical sequence. 

Actress Dorothy Van Engle, who had a supporting role as an assistant producer, is credited for inventing a key scene in Swing!, where her character and  Mandy are sewing together. Van Engle, who was also a seamstress, created her own clothing for the film. 

Elvera Sanchez Davis, the mother of entertainer Sammy Davis, Jr., had a small role in Swing! as a tap dancer. 

Swing!, which is a public domain title, has been frequently shown in film festivals and retrospective series celebrating the creative output of Oscar Micheaux, a pioneering African American filmmaker,  and it has also been broadcast on U.S. television in programming devoted to the history of African American cinema. 

==See also==
* List of films in the public domain

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 