Plan (film)
{{Infobox film
| name = Plan
| image = Plan Flim.jpg 
| caption=Poster Sanjay Gupta
| screenplay= Yash-Vinay
| starring = Sanjay Dutt Priyanka Chopra Dino Morea Sanjay Suri Sameera Reddy Riya Sen
| director = Hriday Shetty Sanjay Gupta Dharam Oberoi
| distributor = White Feather Films
| music = Anand Raj Anand Vishal Dadlani Shekhar Ravjiani
| released =  
| runtime =
| country = India
| language = Hindi
| overseas gross =
| total gross =
| budget =
| awards =
}}
 actionAction thriller|thriller film that Sanjay Gupta and directed by Hriday Shetty.  This film is loosely based on the 1997 Hollywood comedy Suicide Kings. 

==Plot==
Four individuals, from different walks of life, meet on a train destined towards Mumbai. They are Bobby (Dino Morea), Omi (Rohit Roy), Lucky (Sanjay Suri) and Jai (Bikram Saluja). They immediately become friends as they are alone, and only Jai has someone to live with.

Bobby dreams of becoming an Actor like Amitabh Bachchan. Omi needs to get back some money from a person who borrowed it from his father. Lucky was an expert gambler in his village, and intends to test his luck in Mumbai. Jai wants to regain his lost love.

It turns out that Omi had in fact made a plan with the borrower for 50% cut of the money. While Lucky starts having luck in gambling, Bobby faces hard luck in the film industry, and Jai finds out that his love, Shalini (Riya Sen), had intentionally left him, and doesnt want him back. To make Jai forget Shalini, his friends plan and hook him up with Tanya (Payal Rohatgi), a Night-Club Prostitute, and he falls for her later. Soon after, the boys decide not to return to their village, and instead enjoy the life of the city.

They enjoy the life in Mumbai, but ultimately find out that they are running short of cash. Thus Lucky decides to play a big gamble. All others pitch in whats left with them. However, the person playing opposite to Lucky cheats, resulting in Lucky owing him Rs. 700,000. They are warned to return the money in 2 weeks time, and not to leave the city.

The only solution to their problems is kidnapping a Business tycoon, who Lucky has recently seen in nightclubs. After successfully kidnapping him, they find out that he is actually Musa Bhai (Sanjay Dutt). Musa Bhai heads the Underworld Criminal Activities of Mumbai, and is the most feared gangster. However, luck favors them when Musa Bhai finds out that had they not kidnapped him, he would have died, because his men had been bribed by his rival Sultan (Mahesh Manjrekar) to kill him.

The boys help Musa Bhai eliminate his disloyal men, and in return Musa Bhai helps Bobby get a film with his love interest Sapna (Sameera Reddy), and makes a deal with winner of the bet that if his boys win another game, the amount is off. This time when the opponent tries to cheat, Musa prevents him from doing so. As a result, the boys are freed from the burden of handling in Rs 700,000. Though Jai expresses his feeling for Tanya, she tells him that she is not suitable for such a nice boy like him, and that he should return to his village.

When it comes to fighting Sultan, Musa Bhai orders the boys to go back to their villages, stating that he wishes to do so himself, but cannot, as his crimes are far too high. However the boys return to help Musa Bhai in beating Sultan, and Musa Bhai kills him. In the end, all boys depart back to their villages, with Musa Bhai bidding them farewell, and advising them to lead an honest life.

A minor sub-plot involves Rani (Priyanka Chopra), who is a bar-dancer by profession, and is in love Musa Bhai, and wants to marry him. Musa Bhai loves Rani too, but refuses to marry, instead suggesting that they should follow a non-marital affair, to which Rani refuses. In the end, Musa Bhai agrees to marry her.

== Cast ==
* Sanjay Dutt as Musa Bhai, the most feared Underworld Criminal in Mumbai.
* Priyanka Chopra as Rani, Musa Bhais love interest.
* Dino Morea as Bobby, a boy who wishes to become an actor.
* Sameera Reddy as Sapna, Bobbys love interest, and his film heroine.
* Sanjay Suri as Lucky, a local gambler, who wishes to make it big in Mumbai.
* Rohit Roy as Omi, who has bluffed his father into lending money to a friend.
* Bikram Saluja as Jai, who travels to Mumbai to claim his love.
* Riya Sen as Shalini, Jais initial love interest, later rejecting him.
* Payal Rohatgi as Tanya, a Call-Girl in a nightclub, who hooks-up with Jai, and becomes Jais love interest.
* Sanjay Mishra as Jaggi the Pimp, to whom Jai looks forward too for finding an accommodation.
* Mahesh Manjrekar as Sultan, Musa Bhais rival.
* Razak Khan as Film Director Atma, who is forced by Musa Bhai to hire Bobby in a film .
* Dinyar Contractor as Laxmi Seth
* Mukesh Khanna as Ali Bhai, the head of the Underworld in Mumbai; Musa and Sultans boss.

== Soundtrack ==
{| class=wikitable 
|-
!Song
!Singer(s)
!Composer
!Lyricist
|- Pyar Aya Anand Raj Anand, Alisha Chinoy Anand Raj Anand Dev Kohli
|- Hota Hai Hota Hai  Kumar Sanu, Sunidhi Chauhan
|Vishal-Shekhar Kumar
|- Kal Raat Se  Kumar Sanu, Shreya Ghoshal Anand Raj Anand Parveen Bhardwaj
|- Aane Wala Pal - I Udit Narayan, Abhijeet Bhattacharya, Anand Raj Anand, Babul Supriyo, Zubeen Garg Anand Raj Anand Dev Kohli
|- Aane Wala Pal - II Udit Narayan, Abhijeet Bhattacharya, Anand Raj Anand, Babul Supriyo, Zubeen Garg Anand Raj Anand Dev Kohli
|- Aim Kaim Sanjay Dutt, Anand Raj Anand, Shaan (singer)|Shaan, Anand Raj Anand Dev Kohli
|- Mehboob Mere Alisha Chinoy Anand Raj Anand Dev Kohli
|- Kaise Kaise Adnan Sami, Sunidhi Chauhan Anand Raj Anand Dev Kohli
|}

==References==
 

==External links==
*  

 
 
 
 