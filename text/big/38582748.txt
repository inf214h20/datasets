Megh Roddur
{{Infobox film
| name           = Megh Roddur
| image          = Megh Roddur.jpg
| alt            = 
| caption        = 
| director       = Surajit Dhar Sudarshan Basu
| producer       = DK Entertainment
| writer         = Surajit Dhar
| starring       = Subhashree Ganguly Palash Ganguly Biswajit Chakraborty Anindya Banerjee Biswanath Basu Debolina Dutta Biplab Dasgupta Pathasarathi Chakraborty Kumar Rupsa
| music          = Rishi Chanda
| cinematography = Nayanmoni Ghosh
| editing        = Amit Debnath
| studio         = DK Entertainment
| distributor    = DK Entertainment
| released       =  
| runtime        = 185 minutes
| country        = India
| language       = Bengali
| budget         = 90 lac
| gross          = 81 lac
}}
Megh Roddur ( ) (  and Sudarhan Basu and produced under the banner of DK Entertainment. Music of the film has been composed by Rishi Chanda. The film was released on February 15, 2013.   

==Plot==
Madhuja Sen (Subhashree Ganguly), an actress comes to Shillong to shoot her film and happens to meet Arpan (Palash Ganguly), a bookshop owner. She eventually falls in love with Arpan. In the meantime, a sudden terrorist attack takes place in the town and has an influence in their lives.
 Notting Hill (1999), it is not an absolute copy of the same. 

==Cast==
* Subhashree Ganguly as Madhuja Sen
* Palash Ganguly as Arpan
* Kumar as Tathagata
* Debolina Dutta as Paromita
* Rupsa as Sheli
* Biplab Dasgupta
* Biswajit Chakraborty
* Anindya Banerjee
* Biswanath Basu
* Parthasarathi Chakraborty

==Soundtrack==
{{Infobox album  
| Name       = Megh Roddur
| Type       = Soundtrack
| Artist     = Rishi Chanda
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = 
| Producer   = DK Entertainment
| Last album = 
| This album = 
| Next album = 
}}
The soundtrack of Megh Roddur is composed by Rishi Chanda. The film has 6 original songs. Megh Roddur has received positive remarks by critics regarding its soundtrack. 

===Tracklist===
{{Track listing extra_column = Singer(s) lyrics_credits = yes total_length = 23:08 title1 = Megh Roddur extra1 = Rishi Chanda lyrics1 = Priyo Chatterjee, Gautam Susmit length1 = 2:48 title2 = Ki Boli Na Boli extra2 = Shaan (singer)|Shaan, Somchanda Bhattacharya lyrics2 = Priyo Chatterjee, Gautam Susmit length2 = 4:23 title3 = Mon Amor extra3 = Kunal Ganjawala lyrics3 = Priyo Chatterjee, Gautam Susmit length3 = 4:22 title4 = Jole Mon extra4 = Javed Ali, Anwesha Datta Gupta lyrics4 = Priyo Chatterjee, Gautam Susmit length4 = 3:27 title5 = Aaj Abar Ami Eka extra5 = Zubeen Garg lyrics5 = Priyo Chatterjee, Gautam Susmit length5 = 4:46 title6 = Kaasa Kai extra6 = June Banerjee lyrics6 = Priyo Chatterjee, Gautam Susmit length6 = 3:22
}}

==Critical Reception==
{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|-
|  
|  
|-
|   
|  
|-
|  
|  
|-
|  
|  
|-
|}

Megh Roddur received mixed reviews from critics. However, the soundtrack has received many positive remarks. 
 Notting Hill Welsh and, of course, Thackerays roommate, look like a thorough gentleman in comparison.

For someone who is used to working for a certain banner in Cinema of West Bengal|Tollywood, Megh Roddur was Subhashrees biggest chance of proving her mettle as a character actress. Shes a star, the heroine, the diva. Every diva has her own demons to fight, her own dilemmas to resolve. That sense of conflict, that angst and unrest was lacking in her acting. She doesnt talk about the pains it took her to reach this level - the surgeries, the failed relationships - think about that dinner scene in Notting Hill. She looks beautiful, no doubt, but extremely sanitized for the role.

Palash has potential. For a debut opposite an established actress like Subhashree, he manages to hold his own and even shines in certain scenes.

Shillong, as the location for the plot to unfold, was indeed a bold choice. The camera flits between the busy town and the serene outskirts of the rainy setting. We get the Megh bit here. But sadly, its a lot of Megh (Clouds) and very little of Roddur (Sunlight). The director adds a twist to the tale by portraying the political unrest in the North East, weaving the strife into the lives of the protagonists. Yet, thats too little sunshine to make a rip-off such as this look spotless.

==See also==
*  
* Loveria
* Namte Namte Bicycle Kick
* Rupe Tomay Bholabo Na
* Damadol

==References==
 

 
 
 