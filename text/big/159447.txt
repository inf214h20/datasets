The Negotiator
 
{{Infobox film
| name           = The Negotiator
| image          = Negotiatorposter.jpg
| caption        = Theatrical release poster
| director       = F. Gary Gray
| producer       = David Hoberman Arnon Milchan
| writer         = James DeMonaco Kevin Fox David Morse John Spencer J.T. Walsh
| music          = Graeme Revell
| cinematography = Russell Carpenter
| editing        = Christian Wagner
| studio         = Regency Enterprises Mandeville Films New Regency Monarchy Enterprises Taurus Films Warner Bros. Pictures
| released       =  
| runtime        = 140 minutes
| country        = Germany United States
| language       = English
| budget         = $50 million
| gross          = Domestic: $44,547,681 
}} action Thriller thriller film directed by F. Gary Gray, starring Samuel L. Jackson and Kevin Spacey. It takes place in Chicago and was released on July 29, 1998. The original music score was composed by Graeme Revell.

==Plot==
Lieutenant Danny Roman (Samuel L. Jackson), a top Chicago Police Department hostage negotiator, is approached by colleague Nate Roenick (Paul Guilfoyle) who warns him that large sums of money are being embezzled from the departments disability fund, for which Danny is a board member, and members of their own unit are involved. Nate claims to have an informant whom he refuses to name. When Danny goes to meet with him again he finds Nate murdered seconds before other police arrive, pinning him as the prime suspect.
 Internal Affairs Siobhan Fallon), police commander Grant Frost (Ron Rifkin), and two-bit con man Rudy Timmons (Paul Giamatti) as hostages.

With the building evacuated and placed under siege by police (including his own unit) and the FBI, Danny issues his conditions, which include finding Nates informant and summoning police Lt. Chris Sabian (Kevin Spacey), another top negotiator. Danny wants Sabian because he is from another side of the city and therefore unconnected to the pension fund matter, has a reputation for negotiating as long as possible before using force, and should be one of the few people Danny can trust. Despite temporarily putting Sabian in charge after an attempted raid on Danny goes poorly, the police doubt Sabians methods.
 bugs and wiretaps, including his last conversation with Nate. Sabian tries to bluff Danny and the police with an unrelated man who claims to be Nates informant but Niebaums files reveal that Nate himself was the actual informant and had passed his evidence on to the IAD. Niebaum finally admits that he took bribes from the guilty parties to cover up their crimes, and he implicates many of Dannys squad mates in the conspiracy but does not know who the ringleader is. The same conspirators have entered the room via the air vents under the pretext of being part of a team to take Danny out in case he started killing hostages; upon hearing Niebaums confession, they open fire and kill Niebaum before he can reveal where he has hidden corroborating evidence of their guilt. Danny single-handedly fights them off using the flashbangs he confiscated from the officers in the previous failed raid.

Believing that Sabian and the police have lost control of the situation, especially after Sabians bluff with the informant, the FBI relieve Sabian and order a full breach. Sabian now believes Danny and gives him a chance to prove his innocence, helping him to sneak out of the building during the FBI SWAT assault by wearing a confiscated police uniform while the police save the hostages. Danny and Sabian proceed to Niebaums house, but cannot find the evidence there. The police arrive and the dirty members of Dannys squad surround the house but they back off as Frost enters the house to try to talk Danny down while Frost secretly loads his weapon. Sabian realizes Frost is the ringleader and Nates killer.

In front of Frost, Sabian suddenly shoots Danny and offers to destroy the evidence they have uncovered in return for "a piece of the pie" from Frost. Frost agrees and effectively makes a full admission to his crimes, but when he leaves the house, he finds the whole area surrounded by police. Danny had feigned death and Frosts admission of guilt was broadcast over the police radio. Frost attempts to kill himself but is disarmed and arrested. As Danny is loaded into an ambulance, Sabian gives him back his badge and departs.

==Cast==
 
*Samuel L. Jackson as Lieutenant Danny Roman
*Kevin Spacey as Lieutenant Chris Sabian
*Paul Guilfoyle as Nathan Nate Roenick David Morse as Commander Adam Beck
*Ron Rifkin as Commander Grant Frost John Spencer as Chief Al Travis
*J.T. Walsh as Inspector Terence Niebaum Siobhan Fallon as Maggie
*Paul Giamatti as Rudy Timmons
*Regina Taylor as Karen Roman
*Bruce Beatty as Markus
*Michael Cudlitz as Palermo Carlos Gómez as Eagle Tim Kelleher as Argento
*Dean Norris as Scott
*Nestor Serrano as Hellman
*Leonard Thomas as Allen Stephen Lee as Farley
*Robert David Hall as Cale Wangro
 

==Production notes==
The Negotiator was dedicated to J.T. Walsh, who died several months before the films release.

The building used for the IAD office is 77 West Wacker Drive, the headquarters of United Airlines.

When it was made, The Negotiators $50 million budget was the highest ever given to an African-American director. 

==Factual basis==
This film is loosely based on the pension fund scandal in the St. Louis Police Department in the late 1980s and early 1990s. 

==Reception==
 

===Critical response===
The film received a generally positive critical response and a score of 75% on   thriller."

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Saturn Award Saturn Award Best Action or Adventure Film David Hoberman & Arnon Milchan
| 
|- American Black Film Festival Black Film Award for Best Film
| 
|- Black Film Award for Best Director
|F. Gary Gray
| 
|- Black Film Award for Best Actor Samuel L. Jackson
| 
|- Blockbuster Entertainment Blockbuster Entertainment Award Favorite Actor - Action/Adventure
| 
|- NAACP Image Award NAACP Image Outstanding Actor in a Motion Picture
| 
|-
|}

==References==
{{reflist|refs=
  . boxofficemojo.com. Retrieved 2012-11-16. }}

==External links==
 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 