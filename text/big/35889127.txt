The Other Side (2012 film)
{{Infobox film
| name           = The Other Side
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Muhammad Danish Qasim
| producer       = Atiqullah
| writer         = Muhammad Danish Qasim
| screenplay     = 
| Motion Graphic = Aamir Bhagat 
| based on       =  
| narrator       = 
| starring       = 
| music          = Ahsan Raza Naqvi
| cinematography = Ali Raza Mukhtar Ali
| editing        = Waqas Waheed Awan
| studio         = 
| distributor    = 
| released       =  
| runtime        = 20 minutes 
| country        = Pakistan
| language       = 
| budget         = 
| gross          = 
}}
The Other Side is a 2012 short film directed by Muhammad Danish Qasim. It is about drone attacks in Pakistan by the United States.

==Plot==
The film is about a child in Miranshah, Pakistan, whose neighborhood gets bombed by US Unmanned aerial vehicle|drones. He then becomes part of a terrorist group.   

The Film is raising the voice of thousands of innocent families which have been a target of 
the brutality of drone attacks. After the demise of their families, the hatred towards America 
ignites the feeling of revenge in the other family members, which is further exploited by the 
established terrorist groups. The film also depicts the contradiction that though Pakistan is 
blamed for patronizing the terrorists, it is yet the biggest victim of terrorism.

==Awards==
The film received the 2012 Audience Award for International Showcase at the National Film Festival for Talented Youth.  However, Qasim was denied a visa to the United States and hence could not attend the award ceremony.        Qasim said he believed that "the most probable reason for the visa denial was the sensitive subject of my film".  

==See also==

* List of films featuring drones

==References==
 

 
 
 
 