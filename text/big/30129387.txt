Deep River: The Island
 
{{Infobox film name           = Deep River: The Island image          = DeepRiverTheIsland2008.jpg image_size     =  caption        = Theatrical poster producer       = Ben Bachelder Clayton Moore Susan Stuckert-Bachelder George Stuckert director       = Ben Bachelder writer         = George Stuckert starring       = Kristopher Bowman Maia Kaufhold music          = Cassidy Bisher Casino Richard DeHove Drop Drop Lee Fitzsimmons cinematography = Clayton Moore	  editing        = Ben Bachelder				 distributor    =  released       =   runtime        = 76 minutes country        = Canada United States language       = English
| budget = $35,000 (estimated)  
| gross = 
}} Canadian zombie comedy film released in 2009. It was written by George Stuckert and directed by Ben Bachelder. The movie was produced by Ben Bachelder, Clayton Moore, Susan Stuckert-Bachelder, and George Stuckert. The music score was written by Cassidy Bisher, Casino, Richard DeHove, Drop Drop, and Lee Fitzsimmons. It stars Kristopher Bowman, Maia Kaufhold and Jo-Ellen Size. The movie was filmed in Deep River, Ontario, Canada. 

The movie was filmed between July 28, 2008 - October 2, 2008. It was released July 28, 2010 at the Action On Film International Film Festival  and October 2010 at the Eerie Horror Film Festival.

==Summary==
Six friends from a small rural Canadian high school, who reunite for the summer, must fight for survival against the undead. 

==Cast==
*Kristopher Bowman  as Sean
*Maia Kaufhold  as Jen
*Jo-Ellen Size  as Emily
*Mark Von Sell  as Aaron
*Robin Dwyer  as Jerm
*Andrew Nosek  as Trevor
*R.J. Frost  as Zombie
*Jessica Pettigrew  as Bikini Zombie

===Additional Zombies===
* J. Barton
* Gordon Bell
* Katie Bell
* Nicholas Brown
* Dylan Cameron
* Katy Celovsky
* William Clifford
* Jackie McCarthy
* Sean Stanley
*Tiffany-Lee Holmes
*Tyler Holmes

==See also==
* List of zombie films

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 