The Overland Limited (1925 film)
{{infobox_film
| name           = The Overland Limited
| image          =
| imagesize      =
| caption        =
| director       = Frank ONeill
| producer       = Sam Sax 
| based on       = 
| writer         = James J. Tynan
| cinematography = Jack MacKenzie
| editing        =
| distributor    = Gotham
| released       =   reels 
| country        = United States
| language       = Silent film (English intertitles)
}}
The Overland Limited is a 1925 American silent film directed by Frank ONeill and produced by Sam Sax with cinematography by Jack MacKenzie. The story was written by James J. Tynan. The film was released July 14, 1925 in New York. Ralph Lewis as the railway engineer who ultimately saves a trainload of passengers from the dangerous bridge. The picture concludes with a model set of a steam locomotive breaking through the steel girders and plunging into the river. The films final scene was promoted with the tagline: "Like a steel comet, the mighty locomotive was hurled into the foaming waters below! The crashing climax of the greatest railroad photo-play ever made". The film was released in Italy as Nastro dAcciaio. 
==References==
 

==External links==
*  

 
 

 