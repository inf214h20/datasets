Crying Ladies
{{Infobox film
| image                 =
| caption               =
| name                  = Crying Ladies
| director              = Mark Meily 
| producer              =
| music                 =
| cinematography        =
| story                 =
| screenplay            =
| writer                = Mark Meily   Melvin Lee
| editing               =
| starring              = Sharon Cuneta   Hilda Koronel   Angel Aquino 
| studio                =
| distributor           =
| released              =  
| country               = Philippines
| language              =  
| runtime               =
| budget                =
| gross                 = $108,043 (USA) (27 February 2004) Drama
}}
 Filipino film directed by Mark Meily, and winner of Best Picture in the 2003 Metro Manila Film Festival. It was the Philippines submission to the 77th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

==Casts==
*Sharon Cuneta as Stella Mate
*Hilda Koronel as Rhoda Aling Doray Rivera
*Angel Aquino as Choleng
*Eric Quizon as Wilson Chua
*Ricky Davao as Guido
*Julio Pacheco as Bong
*Shamaine Buencamino as Cecile
*Sherry Lara as Mrs.Chua
*Gilleth Sandico as Becky
*Joan Bitagcol as Grace
*Edgar Mortiz as Mang Gusting
*Raymond Bagatsing as Ipe
*Bella Flores as Lost Lady
*Lou Veloso as Brgy.Chairman

==Awards==
 
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2003
| rowspan="5" align="left"| Metro Manila Film Festival   Best Picture
| align="center"| Crying Ladies
|  
|- Best Director
| align="center"| Mark Meily
|  
|- Best Actor
| align="center"| Eric Quizon
|  
|- Best Supporting Actress
| align="center"| Hilda Koronel
|  
|- Best Child Performer
| align="center"| Julio Pacheco
|  
|}

==See also==
 
*Cinema of the Philippines
*List of submissions to the 77th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 

 