Life and Times of Allen Ginsberg
 1993 film by Jerry Aronson chronicling the poet Allen Ginsbergs life from his birth and early childhood to his thoughts about death at the age of 66.  The film has been completed and released a number of times due to changing technologies and world events.  The first release of the film was in 1993 at the Sundance Film Festival after which it enjoyed an international festival run and USA theatrical run.  At the time, Allen was still alive.  When Mr. Aronson showed the film to Ginsberg, the poet is reported to have nodded his head thoughtfully and said, "So, thats Allen Ginsberg."

When Allen passed in 1997, Jerry Aronson decided to update the ending of the film to include the poets passing, one shot of Ginsbergs headstone in New Jersey and a new recording of Paul Simon singing Allens "New Stanzas for Amazing Grace" for closing credits.

The new DVD, released on July 2007 by New Yorker Video, includes interviews with Bono, Paul McCartney, Yoko Ono, Johnny Depp, Hunter S. Thompson, Andy Warhol, Patti Smith, Joan Baez, Michael McClure, Norman Mailer, Amiri Baraka, Ken Kesey, William S. Burroughs, Anne Waldman and Timothy Leary - all of whom considered Allen a good friend.

In addition to the documentary, the DVD release contains a number of bonus features. These include behind-the-scenes footage with the likes of Paul McCartney, Bob Dylan, and Neal Cassady; excerpts from a Ginsberg memorial; and extended and updated interviews.

==External links==
* 
* 

 

 
 
 
 


 