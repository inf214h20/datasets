The Pink Phink
{{Infobox Hollywood cartoon
| cartoon_name = The Pink Phink
| series = Pink Panther
| image = Pinkphink.jpg
| caption =
| director = Friz Freleng Hawley Pratt John W. Dunn Don Williams Bob Matz Norm McCabe LaVerne Harding
| background_artist = Tom OLoughlin
| musician = William Lava Henry Mancini
| producer = David H. DePatie Friz Freleng
| studio = DePatie-Freleng Enterprises UA Entertainment UA Communications Co. (1986-1990) MGM-Pathé Communications (1990-1992) Metro-Goldwyn-Mayer (1992-present)
| release_date = December 18, 1964 Deluxe
| runtime = 6 46"
| movie_language = Not language specific
| preceded_by =
| followed_by = Pink Pajamas
}} animated short short comedy Pink Panther.

==Plot==
  the "Little Man") compete over whether a house should be painted blue or pink. Each time the painter attempts to paint something blue, the panther thwarts him in a new way. At the end, the painter inadvertently turns the house and everything around it pink and the panther moves in. But just before he moves in, he paints the white man completely pink. The painter gets upset and bangs his head against a mailbox. The Pink Panther then walks into the house as the sun sets.   

==Academy Award== Pink Panther animated short produced by DePatie-Freleng Enterprises and by winning the 1964 Academy Award for Animated Short Film, it marked the first time that a studio won an Academy Award with its very first animated short. 

==Credits==
* " 
* Producers: David H. DePatie, Friz Freleng
* Director: Friz Freleng
* Co-Director: Hawley Pratt John W. Dunn
* Animation: Don Williams, Bob Matz, Norm McCabe, LaVerne Harding
* Layout: Dick Ung
* Backgrounds: Tom OLoughlin
* Film Editor: Lee Gunther
* Production Supervision: Bill Orcutt
* Music score: William Lava

==Laugh track==
  Boomerang TV channel, and the France Channel Gulli.

==Popular culture==
* An episode of the animated series Dexters Laboratory entitled "A Silent Cartoon" is a homage to this short; the short features Dexter (filling the role of the painter) trying to construct a blue laboratory, while an all-pink version of his sister Dee Dee finds clever ways to turn the blue lab into a completely pink lab.
* In the 2009 series Pink Panther and Pals, a scene from "A Pinker Tomorrow" in which the Pink Panther tricks the Little Man (Big Nose) to cover the outside of the house in paint, is homage to the original short.
==See also== List of The Pink Panther cartoons

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 