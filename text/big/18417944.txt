Man's Lust for Gold
 
{{Infobox film
| name           = Mans Lust for Gold
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = George Hennessy
| starring       = Blanche Sweet Robert Harron
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short silent silent drama film directed by D. W. Griffith and starring Blanche Sweet.   

==Cast==
* Blanche Sweet - The Prospectors Daughter
* Robert Harron - The Prospectors Son
* Frank Opperman - The Claim Jumper
* Charles Hill Mailes - The Mexican
* William J. Butler - The Prospector
* William A. Carroll - Among the Indians (as William Carroll) David Miles (unconfirmed)
* Jack Pickford - Among the Indians

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 