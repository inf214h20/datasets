The Last of the Mohicans (1920 German film)
 
{{Infobox film
| name           = The Last of the Mohicans
| image          =
| caption        =
| director       = Arthur Wellin
| producer       = Arthur Wellin
| writer         = James Fenimore Cooper Robert Heymann
| starring       = Bela Lugosi
| music          =
| cinematography = Ernest Plhak
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Weimar Republic Silent
| budget         =
| gross          =
}} novel of the same name. The first part was The Deerslayer and Chingachgook (Der Wildtöter und Chingachgook).

A print of Lederstrumpf, in its heavily edited U.S. version titled The Deerslayer, was discovered in the 1990s.

==Cast==
* Charles Barley as Harry
* Edward Eyseneck as Worley
* Herta Heden as Judith Hutter
* Gottfried Kraus as Tom Hutter
* Béla Lugosi as Chingachgook
* Emil Mamelok as Deerslayer
* Erna Rehberger as Heddy Hutter
* Kurt Rottenburg as Magua
* Egon Söhnlein as Col. Munro
* Margot Sokolowska as Wah-ta-Wah
* Heddy Sven as Cora Munro

==See also==
* Bela Lugosi filmography

==External links==
* 

 

 
 
 
 
 
 
 


 
 