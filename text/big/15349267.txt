Avalanche (1923 film)
 
{{Infobox film
| name           = Avalanche
| image          =
| director       = Michael Curtiz
| producer       = Arnold Pressburger
| writer         = Ladislaus Vajda
| starring       = Victor Varconi
| cinematography = Gustav Ucicky
| editing        =
| distributor    =
| released       =  
| runtime        = 71 minutes
| country        = Austria
| language       = Silent
| budget         =
| gross          =
}}

Avalanche ( ) is a 1923 Austrian silent film directed by Michael Curtiz, and produced by Arnold Pressburger.

==Cast==
* Victor Varconi as George Vandeau
* Mary Kid as Marie Vandeau
* Walter Marischka as The Child
* Lilly Marischka as Kitty
* Mathilde Danegger as Jeanne Vandeau

==See also==
* Michael Curtiz filmography

==External links==
* 

 

 
 
 
 
 
 
 
 
 