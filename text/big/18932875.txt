Thunder Rock (film)
 
 
{{Infobox film
| name           = Thunder Rock
| image          = Thunderrock.jpg
| caption        =
| director       = Roy Boulting
| producer       = John Boulting
| writer         = Bernard Miles Jeffrey Dell Robert Ardrey (play)
| starring       = Michael Redgrave Barbara Mullen James Mason Lilli Palmer
| cinematography = Mutz Greenbaum
| music          = Hans May
| editing        = Roy Boulting
| studio         = Charter Film Productions
| distributor   = Metro-Goldwyn-Mayer (UK) English Films (US)
| released       = 4 December 1942 (UK) 16 September 1944 (US)
| runtime        = 112 minutes (UK) 90 minutes (US release)
| country        = United Kingdom
| language       = English
}}

Thunder Rock is a 1942 British drama film with supernatural elements, directed by Roy Boulting and starring Michael Redgrave and Barbara Mullen, with James Mason and Lilli Palmer in supporting roles.

==Background== Thunder Rock montage sequence flashback sequences detailing the histories of the various characters in Charlestons imagination, in the process serving to give a heightened propagandist tone to the material.

Critical opinion of the time in Britain was divided as to whether the additional material brought new depths to the story, or made too explicit things which Ardrey had preferred to leave to the audiences imagination and intelligence. The film was however almost universally admired by North American critics and became a huge popular success. Ironically, it ran to packed houses in New York for over three months, where the play had folded in less than three weeks. 

==Plot==
During the late 1930s, David Charleston (Redgrave) is an ambitious campaigning newspaper journalist, a fierce opponent of fascism and the British policy of Appeasement#The conduct of appeasement, 1937–39|appeasement. He wishes to alert his readers to the dangers of German rearmament and the folly of ignoring what is going on in Europe, but the reports he submits are censored by the editor of his newspaper. He subsequently quits his job and sets off on a speaking tour around the country under the slogan "Britain, Awake!"  The lack of interest and response indicates that Britain is happy to keep slumbering. The final straw comes when Charleston is at the cinema, and the newsreel feature comes on the screen detailing the German occupation of the Sudetenland. The audience show themselves completely uninterested in the newsreel, taking the opportunity to chat among themselves or go in search of refreshments. In despair at the way his countrymen seem totally oblivious to the ever-more impending doom which is about to engulf them, and appear to be content to go about their daily business as normal while all the time sleepwalking towards disaster, he decides to turn his back on Britain and find a far-flung location where he can withdraw from the world and all its contemporary woes.

He crosses the Atlantic, and finds exactly what he is looking for when he successfully lands a job as a lone lighthouse-keeper on Lake Michigan, which will provide him with the solitude he craves. The lighthouse rock carries a commemorative tablet, listing the names of a group of immigrants from Europe who perished 90 years earlier when the ship carrying them to a new life in America foundered off-shore in a violent storm. As weeks turn into months in his self-imposed isolation, Charleston becomes fixated on the names on the tablet, and begins to conjure up ghostly visions of the lost souls, who start to relate to him their sad stories of sorrow, escape and unfulfilled dreams, in what seems an uncanny parallel to Charlestons own situation. The ships captain Stuart (Finlay Currie), who appears to be the only ghost aware that he is dead and that it is no longer 1850, acts as mediator between Charleston and the other spirits as they tell their tales. Charleston discovers the story of proto-feminist Ellen (Mullen), repeatedly persecuted and imprisoned for her progressive views, and becomes particularly emotionally involved with the Kurtz family, progressive medical man Stefan (Frederick Valk) and his sad daughter Melanie (Palmer), who seems to harbour a strange ghostly attraction towards Charleston, which he reciprocates.

Charlestons lonely existence is broken by the arrival of an old colleague Streeter (Mason), who is worried about him after finding out from Charlestons employers that his pay cheques have not been cashed for many months. Streeter is nonplussed and not a little concerned as he starts to realise Charlestons mental state. Stuart meanwhile becomes exasperated by the way in which Charlestons imagination is forcing the others into unrealistic behaviour. Charleston agrees to let them have more freedom of action, but then finds them all starting to question where they are and what time they are in. He finally allows Melanie to read the tablet describing their deaths, and tells them all that the civilisation they knew is coming to an imminent end, and he has withdrawn to avoid being witness to its demise. He adds that now he has told them the truth, as figments of his imagination they no longer need to appear to him.

To his consternation, they do not disappear. Stefan confronts him sternly, pointing out that running away is cowardly and that it is always better to stand up and fight for what is good and right, regardless of the consequences. Moreover none of the spirits have any intention of leaving him until he faces up to what he has to do. Finally convinced, Charleston realises he must return to Europe and carry on his fight for truth and justice against the evil which threatens the continent.

==Reception==
On its British release in 1942, Thunder Rock received mixed reviews, with critics eager to compare the screen version to the stage play, not always to the formers advantage. The   called the film "a glowing fantasy that lights up the dark corners of many current issues...it manages to be high-class without being highbrow".  Dorothy Kilgallen, writing in her Voice of Broadway column, urged any of her out-of-town readers planning a visit to New York to "drop in at the World Theatre...and see the film Thunder Rock...youll remember it a long time, and it may not play your town."  Herbert Whittaker, film critic for the Montreal Gazette, chose the film as one of the ten best of 1944, observing "it translate(s) Robert Ardreys deep and philosophical drama to the screen with brilliance".  The Los Angeles Times described it as "highly imaginative", "noteworthy" and "outstanding". 

Modern critical assessments of Thunder Rock tend to be equally assertive of the films lasting merit. A BBC reviewer comments that the film "succeeds in creating an atmosphere that is at once haunting, mournful and inspiring. As the writer disillusioned by the worlds complacent response to fascism, Michael Redgrave gives one of his most complex and tormented performances, as he regains the crusading spirit from his encounters with the victims of a shipwreck that occurred years before on the rocks near the lighthouse he now tends. With a bullish contribution from James Mason and truly touching support from ghostly emigrée Lilli Palmer, this is one of the Boulting Brothers finest achievements."  The Time Out Film Guide says: "The film effortlessly transcends its theatrical origins, merging drama and reality, past and present, propaganda and psychological insight, to complex and intelligent effect.  Beautifully performed, closer in tone and style to Powell and Pressburger than to the British mainstream, its weird and unusually gripping." 

==DVD==
The film was released on a Region 2 DVD (now out-of-print) in Europe, but has, as of 2013, not been made available on a Region 1 DVD in the United States.

==Cast==
 
 
* Michael Redgrave as David Charleston
* James Mason as Streeter
* Lilli Palmer as Melanie Kurtz
* Barbara Mullen as Ellen Kirby
* Finlay Currie as Capt. Joshua Stuart
* Frederick Valk as Dr. Stefan Kurtz
* Sybille Binder as Anne-Marie Kurtz
* Barry Morse as Robert
* George Carney as Harry
* Frederick Cooper as Ted Briggs
* Jean Shepherd as Millie Briggs
 
* Miles Malleson as Chairman of Directors
* A. E. Matthews as Mr. Kirby
* James Pirrie as Jim Sales
* Olive Sloane as Woman Director Tommy Duggan as Office Clerk
* Tony Quinn as Office Clerk
* Harold Anstruther as British Consul
* Alfred Sangster as Director
* Victor Beaumont as Hans (uncredited)
* Gerard Heinz as Hans Harma (uncredited)
* Milo Sperber as Hirohiti (uncredited) 
 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 