Dance with Me (film)
 
{{Infobox film
| name           = Dance with Me
| image          = Dance With Me2.jpg
| image_size     =
| caption        =
| director       = Randa Haines
| producer       = Shinya Egawa Randa Haines Lauren Weissman Ted Zachary  (executive producer) 
| writer         = Daryl Matthews
| starring       = Vanessa L. Williams Chayanne Kris Kristofferson Joan Plowright Jane Krakowski
| music          = Michael Convertino
| cinematography = Fred Murphy
| editing        = Lisa Fruchtman William S. Scharf
| studio         = Mandalay Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 126 minutes
| country        = United States English
| budget         =
| gross          = $15,814,088 (United States Dollar|USD)
}} Puerto Rican singer Chayanne.

==Plot==
After burying his mother, Rafael Infante (Chayanne) comes from Santiago, Cuba to Houston, Texas to work for a man named John Burnett (Kris Kristofferson) as a handyman in Burnetts dance studio. It soon becomes clear to the audience that Burnett is the father Rafael never knew. While there he finds himself falling for a dancer and instructor Ruby Sinclair (Williams), who incidentally brought him to the studio. 
 Las Vegas and that Ruby would be taking part as well. Rafael gets close to Ruby and their attraction to each other grows, but she is not willing to commit herself to a relationship as she seems more interested in her dancing.

Meanwhile, Rafaels arrival and persona wins him the friendship of an older dancer Bea Johnson (Joan Plowright) as well as the studio receptionist Lovejoy (Beth Grant), but it also causes some discomfort to Burnett, who suddenly begins to withdraw into himself and takes less interest in the preparations for the competition, much to the chagrin of his own partner Patricia Black (Jane Krakowski). 

While visiting Burnett at his home, Rafael notices and offers to repair his old and broken-down truck. He and Ruby go downtown to get the parts needed to repair the truck and are invited to an engagement party by a Cuban man whose daughter was getting engaged. While there, both of them discover more about each other as he tells her that his mother had died but that he never knew his father.

Later, Rafael invites Ruby to a dance party at a club in the city and they agree to go on the following Saturday night. Before leaving, Rafael helps Patricia learn a dance lift she had been practicing, explaining that his mother made him take a little ballet. The following Saturday, he and Ruby go to the party in which they perform, along with other dancers, the Latin Salsa dancing to a song by Albita Rodriguez. 

After the party, he takes Ruby home where he discovers that she has a 7-year-old son Peter (Chaz Oswill) who is "looked after" by Bea in Rubys absence and who happens to be fathered by Rubys former dance partner, Julian Marshall (Rick Valenzuela). While there, he comes up to Ruby and they share a passionate kiss, but she eventually breaks it up.

Later on Rafael comes to Burnetts home to show him the refurbished truck, to the latters delight. while there, Patricia comes to the house and has a discussion with John asking him to explain why he suddenly lost interest in their choreography preparations. John then tells her that she can dance with Rafael if she wants and both she and Rafael start their practice to the amazement of the other dancers at the studio. 

After the rehearsal, Rafael overhears John telling Lovejoy that Ruby would be in Las Vegas without them and also asking her to switch his name with Rafaels when Patricias dance comes up. 

Sensing that she wants to reunite with Julian, Rafael goes to see Ruby who explains that she wanted Peter to see his father often and that she did not want to be in love. While at a fishing trip with Burnett, Rafael is shocked when Burnett tells him to go back to Cuba after the competition and that he did not have a son. Devastated by the double rejection, he decides to return to Cuba after his dance with Patricia.

At Las Vegas, Rafael meets Ruby and tells her that he was returning to Cuba after the competition. Burnett (who did not go) reflects on his cruel rejection of Rafael and decides to come to the dance to apologize to Rafael and persuade him not to go back to Cuba. Rafael dances with Patricia and while watching them, Ruby realized that she was in love with Rafael and felt a stab of jealousy seeing them together. 

Just as Rafael and Patricia leave the stage, Bea comes in, saying, "I wanna do that too" and she and Rafael perform a very humorous dance to the amusement and delight of the crowd. After the dance, Burnett meets and apologizes to Rafael, eventually convinces him to stay back and also commending his dancing.

The main dances begin and Ruby and her partner Julian are involved. Despite the slight tension between them, they win the competition although Ruby almost all the time seems to keep her eyes on Rafael, who is in the audience and who is watching her, however he leaves and when Ruby looked around without seeing him, she breaks down in tears. 

Later at a dance party for all the contestants, Ruby is met by a man who wishes to promote her. However, to her relief, Rafael appears again and leads her to the dance floor for a final dance scene that has the rest of the dancers watching with admiration. 

The movie ends at the studio with the entire studio members and some new dancers dancing to the theme song "You Are My Home" by Chayanne and Williams themselves.

==Cast==
*Vanessa L. Williams as Ruby Sinclair
*Chayanne as Rafael Infante
*Kris Kristofferson as John Burnett
*Joan Plowright as Bea Johnson
*Jane Krakowski as Patricia Black
*Beth Grant as Lovejoy
*Harry Groener as Michael

==Film soundtrack==
For the article for the soundtrack, see  

Track Listing:

1.	Magalenha   (03:39) 
Sérgio Mendes 
2.	Heavens What I Feel   (05:09) 
Gloria Estefan 
3.	You Are My Home (05:10)  Vanessa Williams; Chayanne 
4.	Jibaro   (04:36) 
Electra 
5.	Fiesta Pa Los Rumberos (05:03) 
Albita 
6.	Want You, Miss You, Love You (04:01) 
Jon Secada 
7.	Jazz Machine (03:31) 
Black Machine 
8.	Echa Pa Lante (Spanish Cha-Cha Mix) (NOTE:  This song is not to be confused with the original;  it is a completely different song in theme from the original.) (03:53) 
Thalía 
9.	Atrévete   (04:11)  DLG 
10.	Eres Todo en Mí   (05:13) 
Ana Gabriel 
11.	Refugio de Amor   (Salsa) (05:30)  Vanessa Williams; Chayanne 
12.	Tres Deseos     (05:00) 
Gloria Estefan 
13.	Patria (04:10) 
Ruben Blades

Bonus Tracks:

14.	Pantera en Libertad (03:26) 
Monica Naranjo  Suavemente   (04:17) 
Elvis Crespo

==Awards==
Nomination
*Satellite Awards: Best Supporting Actress in a Comedy or Musical- Joan Plowright

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 