Namma Ooru Raasa
{{Infobox film
| name           = Namma Ooru Raasa
| image          =
| image_size     =
| caption        =
| director       = Ramarajan Nalini Ramarajan
| writer         = 
| screenplay     = 
| starring       = Ramarajan Sangita
| music          = Sirpy
| cinematography = 
| editing        = 
| studio         =  Nalini Cini Arts
| distributor    =  Nalini Cini Arts
| released       = 1996
| country        = India Tamil
}}
 1996 Cinema Indian Tamil Tamil film, Nalini Ramarajan. The film stars Ramarajan, Sangita, Manivannan and Senthil in lead roles. The film had musical score by Sirpy.  

==Cast==
*Ramarajan
*Sangita

==Soundtrack==
The music was composed by Sirpy.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Chithra || Kalidasan || 4.57 
|- 
| 2 || Andankakka || Mano (singer)|Mano, Sirphi || Kalidasan || 4.43 
|- 
| 3 || Ennudaiya Maadapura || Mano (singer)|Mano, Sujatha || Kalidasan || 
|- 
| 4 || Amma Amma Mariyamma || Sujatha || Muthulingam || 
|- 
| 5 || Kaadu Vetti || Mano (singer)|Mano, Sangeetha || Muthulingam || 4.40 
|-  Mano || Muthulingam || 1.22 
|-  Mano || Kalidasan || 4.27 
|}

==References==
 

==External links==

 
 
 
 


 