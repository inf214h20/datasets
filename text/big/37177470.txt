Just Suppose
{{infobox film
| name           = Just Suppose
| image          =
| imagesize      =
| caption        =
| director       = Kenneth Webb
| producer       = Richard Barthelmess
| writer         = Albert Ellsworth Thomas(play:Just Suppose) Violet E. Powell(adaptation) C. Graham Baker(scenario)
| starring       = Richard Barthelmess Lois Moran
| music          =
| cinematography = Stuart Kelson
| editing        =
| distributor    = First National Pictures
| released       = January 10, 1926
| runtime        = 70 minutes; 7 reels(6,270 feet)
| country        = USA
| language       = Silent(English intertitles)

}}
Just Suppose is a surviving  1926 film produced by and starring Richard Barthelmess with distribution through First National Pictures. Kenneth Webb directed Barthelmess and young Lois Moran star. The film is based on a 1920 Broadway play, Just Suppose, by Albert E. Thomas.   

==Cast==
*Richard Barthelmess - Prince Rupert of Koronia
*Lois Moran - Linda Lee Stafford
*Geoffrey Kerr - Count Anton Teschy
*Henry Vibart - Baron Karnaby
*George Spelvin - King
*Harry Short - Crown Prince
*Bijou Fernandez - Mrs. Stafford
*Prince Rokneddin - Private Secretary

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 
 