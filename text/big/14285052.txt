Mad Detective
 
 
{{Infobox film
| name           = Mad Detective
| image          = Mad detective.jpg
| caption        = Promotional poster
| director       = Johnnie To Wai Ka-Fai
| producer       = Johnnie To Wai Ka-Fai
| writer         = Wai Ka-Fai Au Kin-Yee
| starring       = Sean Lau Lam Ka-Tung Andy On Kelly Lin
| music          = Xavier Jamaux
| cinematography = Cheng Siu-Keung
| editing        = Tina Baz
| studio         = One Hundred Years of Film Milkyway Image
| distributor    = China Star Entertainment Group
| released       =  
| runtime        = 89 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = $2,160,790 
}}
Mad Detective (Chinese: 神探) is a 2007 Hong Kong psychological thriller film produced and directed by Johnnie To and Wai Ka-Fai. 
Mad Detective was first screened at the 64th Venice International Film Festival, and later premiered at the 2007 Toronto International Film Festival, before being released in Hong Kong on 29 November 2007.  The films screenplay won "Best Screenplay" awards at various Asian film ceremonies.

==Plot==
 
Chan Kwai-Bun (Sean Lau) is a brilliant detective with a supernatural gift seeing a persons "inner personalities," or hidden ghosts. However, he is forced into retirement after severing his ear and presenting it to his retiring boss. 
 AWOL for 18 months, and his gun has been used in a series of armed robberies. Ho turns to Bun, who now lives in seclusion with his imaginary wife, May Cheung (Kelly Lin).

Bun comes out of retirement and discovers that rather than being one man, Chi-Wai is a seven-spirit collective, with each perhaps representing an aspect of the seven deadly sins with the head, or brain, being a business-like woman. He also discovers that Chi-Wai had his gun stolen by an Non-resident Indian and Person of Indian Origin|Indian, named Naresh Sherma, so he killed Wong to steal his gun.
 SMS to Ho, telling him that after Chi-Wai gets his old gun back again by killing Naresh, he would kill Ho, but the latter does not believe him. 

Chi-Wai and Ho go to a warehouse and find Naresh, with Bun in pursuit. Bun tries to warn Ho of what will happen again, but Ho, not believing Bun and believing that Bun is the enemy, calls him to reveal his position. A shootout occurs, ultimately leading to Naresh and Bun pointing their guns at Chi-Wai and Ho pointing his girlfriends gun at Bun. Chi-Wai kills Naresh while Ho shoots Bun. Chi-Wai turns and shoots Ho, as predicted by Bun. Bun, however, begins to limp towards Chi-Wai, who shoots and fatally wounds him. Bun ultimately kills Chi-Wai. 

Just as Bun dies, he sees Hos inner personality, the scared boy, being led by a business-like woman, which looks eerily similar to the woman that led Chi-Wai. Ho then begins to endlessly arrange and rearrange everyones guns to fabricate his own story, like Chi-Wai did.

==Cast==
* Sean Lau as Inspector Chan Kwai-Bun, a former police detective.
* Lam Ka-Tung as Officer Ko Chi-Wai, a policeman, who is considered a suspect in the disappearance of another officer, Wong Kwok-Chu. 
* Andy On as Inspector Ho Ka-On, a detective trying solve the disappearance of Police Officer Wong Kwok-Chu.
* Kelly Lin as May Cheung, Buns imaginary wife.
** Flora Chan also plays May Cheung as seen through Buns eyes.
* Lee Kwok-Lun as Officer Wong Kwok-Chu, the missing police officer.
* Jo Kuk as a cunning woman, who appears as Ho Ka-Ons inner personality
* Jay Lau as a calculating woman, who appears as one of seven of Kos inner personalities.
* Lam Suet as Fatso, one of Kos seven inner personalities.
* Cheung Siu-fai as a violent man who is one of Kos seven inner personalities.
* Eddy Ko as a retiring police chief.

==Reception== Category III rating, an 18+ restriction rating in Hong Kong.  Prior to the films release, Wai Ka-Fai discussed the films rating, saying that the rating was based on one exceptionally violent scene in the movie and since he felt the scene was crucial to the story he and his partner, Johnnie To, refused to delete it to get a Category IIB rating. 

Mad Detective was screened at the 2007 Toronto International Film Festivals Special Presentations, a showcase for daring and artistic films with high-profile stars or directors. It also premiered in at the 64th Venice International Film Festival where it was nominated for a Golden Lion Award.

===Critical response===
The film currently holds an 83% "fresh" rating at Rotten Tomatoes based on 18 reviews.    Another review aggretator, Metacritic, gave the film a 68/100 approval rating based on 7 reviews following under the "generally favorable reviews" category.   

  of the Boston Globe wrote that Mad Detective "is equal parts gonzo inspiration and overwrought indecision", and nicknamed the film "The Lunatic From Kowloon."   

===Box office=== 30 Days The Heartbreak China Star, Category III rating in Hong Kong. 

===Awards and nominations===
{| class="wikitable"
|-
!Award !! Category !! Winner/Nominee !! Result
|- 27th Hong Kong Film Awards Best Screenplay Wai Ka-Fai, Au Kin-Yee Won
|- Best Actor Sean Lau Nominated
|- Best Directors Johnnie To, Wai Ka-Fai Nominated
|-
|- Best Cinematography Cheng Siu-Keung Nominated
|- Best Film Editing Tina Baz Nominated
|- Best Costume and Makeup Design Stanley Cheung Nominated
|- Best Visual Effects Raymond Man Nominated
|- 2nd Asian Film Awards Best Screenplay Wai Ka-Fai, Au Kin-Yee Won
|- Hong Kong Film Critics Society Awards Best Screenplay Wai Ka-Fai, Au Kin-Yee Won
|- 8th Chinese Film Media Awards    Best Screenplay Wai Ka-Fai, Au Kin-Yee Won
|- 64th Venice International Film Festival Golden Lion Johnnie To, Wai Ka-Fai Nominated
|}

==Distribution== VOD on 18 July 2008, the same day as part of its First Take program. 

Eureka Entertainment acquired the distribution rights for the United Kingdom, opening theatrically on 18 July 2008 at the Institute of Contemporary Arts (ICA) London and nationwide after that, with DVD and Blu-ray Disc editions released on 20 October 2008 as part of their Masters of Cinema series.

==See also==
* Johnnie To filmography

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 