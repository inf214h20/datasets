Holi Ayee Re
{{Infobox film
| name           = Holi Ayee Re
| image          = 
| image_size     = 
| caption        = 
| director       = Harsukh Jagneshwar Bhatt 
| producer       = 
| writer         =
| narrator       = 
| starring       =Shatrughan Sinha  Mala Sinha
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Holi Ayee Re (meaning:"The Festival of Colors Has Come") is a 1970 Bollywood romance film directed by Harsukh Jagneshwar Bhatt. The film stars Shatrughan Sinha and Mala Sinha.

==Cast==
*Shatrughan Sinha   
*Mala Sinha   
*Balraj Sahni   
*Premendra
*Kumud Chuggani   
*Rajendra Nath   
*Kanhaiyalal 
*Jankidas    Dulari   
*Praveen Paul

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chal Chal Re Raahi Chal Re"
| Mahendra Kapoor
|-
| 2
| "Holi Aaee Re"
| Mahendra Kapoor, Lata Mangeshkar, Usha Khanna
|-
| 3
| "Jawaniya Ke Din Dui Char Rasiya"
| Hansa Dave
|- 
| 4
| "Meri Lottery Lag Jane Wali Hai"
| Kishore Kumar
|-
| 5
| "Meri Tamannaon Ki Taqdeer Tum" Mukesh
|-
| 6
| "Meri Tamannaon Ki Taqdeer"
| Lata Mangeshkar
|-
| 7
| "Tere Haseen Badan Mein"
| Mukesh
|}

==External links==
*  

 
 
 
 
 


 
 