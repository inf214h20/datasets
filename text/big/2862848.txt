Moon Warriors
 
 
{{Infobox film name = Moon Warriors image = MoonWarriors DVDcover.jpg alt = caption = DVD cover art traditional = 戰神傳說 simplified = 战神传说 pinyin = Zhàn Shén Chuán Shuō jyutping = Zin3 San4 Cyun4 Syut3}} director = Sammo Hung producer = Jessica Chan writer = Alex Law starring = Andy Lau Kenny Bee Anita Mui Maggie Cheung music = James Wong Sherman Chow cinematography = Arthur Wong Cheung Man-po Tam Chi-wai editing = Kam Ma studio = Teamwork Production House Limited distributor = Newport Entertainment Ltd (Hong Kong) released =   runtime = country = Hong Kong language = Cantonese budget = gross = HK$11,159,986.00
}}
Moon Warriors  is a 1992 Hong Kong wuxia film directed by Sammo Hung, written by Alex Law and starring Andy Lau, Kenny Bee, Anita Mui and Maggie Cheung.

==Plot==
Fei is a simple fisherman, who still possesses great sword-fighting prowess. He ends up foiling an assassination attempt against 13th Prince and helping him in an attempt to regain the throne from his evil brother. Yuet and Hsien appear as the princes wife-to-be and aide, while adding their talents to the films numerous swordplay sequences. The film ending was also a tragic situation.

==Cast==
*Andy Lau as Fei
*Kenny Bee as 13th Prince
*Anita Mui as Yuet
*Maggie Cheung as Hsien
*Kelvin Wong as 14th Prince
*Chang Yi as Lord Lanling
*Chin Kar-lok as 13th Princes bodyguard
*Heung Lui
*Tam Wai
*Ng Biu-chuen
*Wong Man-kit
*Lam Wai-kong
*Sam Mei-yiu
*Law Yiu-hung
*Lui Tat
*Mak Wai-cheung Hsiao Ho
*Chang Kin-ming
*Hon Ping
*Cheung Wing-cheung

 {{Cite web |url=http://www.imdb.com/title/tt0108650/ |title=Moon Warriors 
 |accessdate=30 July 2010 |publisher=imdb.com}} 
   

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 