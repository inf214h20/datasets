Homework (1989 film)
{{Infobox film
| name           = Homework
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = مشق شب
| director       = Abbas Kiarostami
| producer       = Ali Reza Zarrin
| writer         = Abbas Kiarostami
| narrator       = 
| starring       = Pupils at Shahid Masumi School Two pupils fathers Abbas Kiarostami Iraj Safavi
| music          = Mohammad Reza Aligholi
| cinematography = Iraj Safavi Ali Asghar Mirzai
| editing        = Abbas Kiarostami Kanoon
| distributor    = 
| released       =  
| runtime        = 86 min.
| country        = Iran Persian
| budget         = 
| gross          = 
}}
 1989 Iranian narrative documentary film written, directed and edited by Abbas Kiarostami.
 16mm in late January and/or early February 1988 at Tehrans Shahid Masumi primary school. 

==Plot==
The films consists almost exclusively of interviews with a number of pupils and two fathers of pupils at Shahid Masumi school who are asked to give their opinion on the traditional teaching practice of assigning homework. Issues such as some parents illiteracy and their inability to help their children with the homework are raised. The children dont always succeed in hiding the more embarrassing aspects of their family life (corporal punishment, poverty, etc.).

==References==
 

==Bibliography==
*Deborah Young, "Mashgh-e shab (Homework)", Variety (magazine)|Variety, no. 338, 7 March 1990, p. 32
*Peter Matthews, "A Little Learning", Sight & Sound, vol. 12, no. 6, June 2002, pp. 30–32
*Sonia Giardina, "Another Look at Homework by Abbas Kiarostami: An Investigation of a Pedagogic and Social Drama", Film International, no. 35, Summer 2002, p. 33
*François Niney, "Devoirs de maison", Cahiers du cinéma, no. 449, November 1991, pp. 62–63
*Danièle Parra, "Devoirs du soir",  , no. 477, December 1991, p. 41
*François Niney, "Devoirs de soir", Cahiers du cinéma, no. 493, July/August 1995, pp. 107–108
*Israel Diego Aragón, "Un documental de terror psicológico: Los deberes", in  , no. 7, 2003, pp. 76–77

==See also==
*List of Iranian films

==External links==
* 
*  

 

 
 
 
 
 
 
 
 