The Journey of a Young Composer
 
{{Infobox film
| name           = The Journey of a Young Composer
| image          = 
| caption        = 
| director       = Georgiy Shengelaya
| producer       = 
| writer         = Georgiy Shengelaya Erlom Akhvlediani Otar Chkeidze
| starring       = Gia Peradze
| music          = 
| cinematography = Levan Paatashvili
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Georgia
| language       = Georgian
| budget         = 
}}

The Journey of a Young Composer ( , Transliteration|translit.&nbsp;Akhalgazrda kompozitoris mogzauroba,  ) is a 1985 Georgian drama film directed by Georgiy Shengelaya. It was entered into the 36th Berlin International Film Festival where Shengelaya won the Silver Bear for Best Director.   

==Cast==
* Gia Peradze as Leko Tatasheli
* Levan Abashidze as Nikusha Chachanidze
* Zura Kipshidze as Elizbar Tsereteli
* Rusudan Kvlividze as Tekla Tsereteli
* Ruslan Miqaberidze as Shalva Tsereteli
* Lili Ioseliani as Epemia Tsereteli
* Teimuraz Dzhaparidze as Giorgi Otskheli
* Ketevan Orakhelashvili as Gurandukhti
* Zinaida Kverenchkhiladze as Gulkani
* Chabua Amiredjibi as Davit Itrieli
* Teimuraz Bichiashvili as Rostomi

==References==
 

==External links==
* 

 
 
 
 
 
 
 