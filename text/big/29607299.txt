Goppinti Alludu
 
{{Infobox film
| name           = Goppinti Alludu
| image          =
| caption        =
| writer         = Janardhan Maharshi  
| story          =
| screenplay     = E V V Satyanarayana 
| producer       = Nandamuri Rama Krishna
| director       = E V V Satyanarayana Simran Sanghavi Sadhika
| Koti
| cinematography = Nandamuri Mohana Krishna
| editing        = Kotagiri Venkateswara Rao
| studio         = Ramakrishna Horticulture Cine Studios
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}  
 Sadhika in Hindi Movie Hero No. 1 (1997).  The film recorded as Average at box office.

==Plot==
Murali Manohar(Balayya) is son of the industrialist SVR(SPB). He arrives India from Switzerland after finishing his graduation. SVR fixes a girl(Sadhika) for him and asks him to marry her. To get rid of unwanted marriage, Maohar escapes back to Switzerland.

Sowmya(Simran) is the granddaughter of Achyuta Ramayya(Satyanarayana). Achyuta Ramayyas family consists of 3 of his daughters and their husbands and their kids along with Sowmya and Jalandhara(Sanghavi). Jalandhara is the straight daughter of son of Achuta Ramayya and Sowmya is the daughter of the keep of son of Achuta Ramayya. Hence everyone in the family(except for Achuta Ramayya) demeans and harass Sowmya that she is the daughter of a keep. Jalandhara has lot of hatred towards Sowmya and she tries to be diplomatic with Sowmya, but backstabs her without Soumyas knowledge every time

Sowmya gets a job offer as a bank Manager (thankfully not as a software consultant!) in Switzerland. Here the stage is set for the introduction between manohar and Sowmya at airport followed by Manohar teasing her in the flight. As Mr. Subba Rao (Chalapathi Rao) did not come to the airport to receive Sowmya, she is forced to take the help of Manohar.

SVR and his cook comes to Switzerland to search manohar. Sowmya shifts her accommodation to her boss (Prithvi) guest house. Then her boss tries to rape her and Sowmya escapes from him and starts running on the road. Here comes the 15 minutes guided tour of Switzerland with SVR and his cook running down the roads for Manohar, Subba Rao searching for Sowmya, Boss running after Sowmya, Manohar chasing the boss to help Sowmya. In these chasing sequence Manohar expresses his love to her and Sowmya accepts. At the end of 15 minutes of running senseless, Boss is sent to jail, SVR accepts the love of Sowmya and Manohar.

SVR decides to go to Achyuta Ramayyas house and ask the hand of Sowmya for his son. On the way due to some unforeseen accident SVR runs Achuta Ramayya down with his car and fractures his legs. Achyuta Ramayya refuses to get Sowmya married to Manohar.

Sowmya decides to elope with manohar. But Manohar convinces her that they should get married with the consent of their families as it will be useful for everybody in the long run. And the next thing you see is Manohar joining Achyuta Ramayyas house as a cook and winning the heart of everybody. But jalandhara comes to know that the cook is none but the Manohar. She takes every opportunity to disturb the discreet romance of the love birds and seduce manohar.

As Jalandhara starts vying for the blood of Sowmya desperately, manohar tries sincerely to win the hearts of the Sowmya family members. To know how all ended well, you gotta watch this film on the silver screen.

==Cast==
 
* Nandamuri Balakrishna ... Murali Manohar / Bheemudu Simran ... Soumya
* Sanghavi ... Jaladhara Sadhika ... Jiddu Balamani Satyanarayana ... Achyutha Rammaiha SP Balasubramanyam ... SVR
* Kota Srinivasa Rao ... Jiddu Bharadwaja
* Tanikella Bharani ... Achyutha Rammaihas Elder Son-in-law
* Chalapathi Rao ... Subba Rao
* Jaya Prakash Reddy ... Swaminaidu Prudhvi Raj ... Paramahamsa
* Surya ... Achyutha Rammaihas Third Son-in-law
* Ahuti Prasad... Achyutha Rammaihas Second Son-in-law Mallikarjuna Rao ... Pandu (Cook)
* L.B. Sriram ... Servant
* Gundu Hanumantha Rao ...
* Sudha ... Achyutha Rammaihas Elder Daughter
* Sana ... Achyutha Rammaihas Second Daughter Hema ... Achyutha Rammaihas Third Daughter
* Madhumani ... Widow
* Kalpana Rai ... Servant
 

==Soundtrack==
{{Infobox album
| Name        = Goppinti Alludu
| Tagline     = 
| Type        = film Koti
| Cover       = 
| Released    =  
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:24
| Label       = Supreme Music Koti
| Reviews     =
| Last album  = Vamsoddarakudu   (2000)  
| This album  = Goppinti Alludu   (2000)
| Next album  = Nuvve Kavali   (2000)
}}

Music composed by Saluri Koteswara Rao|Koti. Music released on Supreme Music Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:24
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =


| title1  = Nee Height India Gatu
| lyrics1 = Bhuvanachandra
| extra1  = Sukhwinder Singh,Poornima 
| length1 = 4:01

| title2  = Naachere Naachere
| lyrics2 = Samaveda Shanmukha Sarma
| extra2  = Sukhwinder Singh,Manasa
| length2 = 4:30

| title3  = Muddoche Gopala
| lyrics3 = Surendra Krishna Chitra
| length3 = 3:55

| title4  = Premiste Entho Greatu
| lyrics4 = Bhuvanachandra
| extra4  = Devan,Chitra 
| length4 = 4:17

| title5  = Vachestundo Chestundo
| lyrics5 = Ravikumar SP Balu, Chitra
| length5 = 4:17

| title6  = Ammagariki Pessarattu  
| lyrics6 = Bhuvanachandra  
| extra6  = SP Balu
| length6 = 4:24
}}
|}

==Critical response==
Upon release, the film received positive reviews from critics. Jeevi from Idlebrain gave a three commented "EVV exploited and played with the unexposed comic timing of Balayya in this film. He has done a wonderful job ". 

==Box office==
The Movie was a moderate success at Boxoffice. Goppinti Alludu Movie Review http://www.idlebrain.com/movie/archive/mr-ga.html 

==Others== Hyderabad

==References==
 

==External links==
*  
 

 
 
 
 
 