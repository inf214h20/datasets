The Old Man of the Mountain (film)
{{Infobox Hollywood cartoon|
| cartoon_name = The Old Man of the Mountain
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist = Bernard Wolf Thomas Johnson
| voice_actor = Mae Questel (uncredited) Cab Calloway Bonnie Poe
| musician = Cab Calloway and his orchestra
| producer = Max Fleischer
| studio = Fleischer Studios Paramount Productions
| release_date = August 4, 1933
| color_process = Black-and-white
| runtime = 7 mins
| country =   English
}}
  1933 animated Paramount Productions. Calloway, who voices all of the characters in the cartoon save for Betty herself (voiced by Mae Questel) performs all of the music in the cartoon, including three of his own songs.

==Synopsis==
The short begins with a live-action introduction of Calloway and his orchestra, who perform a short chorus of "Minnie the Moocher" before performing a vamp of the title song, "The Old Man of the Mountain".

As the cartoon proper begins, a lion on roller skates (made of rabbits) rushes from his guard post atop a mountain, racing into a nearby village crying "Look out! The Old Man of the Mountain!" The lions warning sparks a mass exodus of the other animals who pack up their things and start to flee as the lion continues to warn "Look out! The Old Man of the Mountain!"

In time, Betty Boop emerges from a guest house in order to find out what is going on. She confronts a passing owl, who in song describes the Old Man of the Mountain, a predatory hermit who threatens the livelihood of the villagers, particularly the women. Despite the owls warnings, Betty is curious and declares, "Im going up to see that old man of the mountain", and starts a trek up the mountainside.  She passes several people fleeing from the Old Man, including a woman pushing a carriage with her triplets—who look suspiciously like the Old Man of the Mountain.

When Betty gets to the top of the mountain, the Old Man of the Mountain emerges from behind a rock. Over twice as tall as Betty, the Old Man backs the girl into his cave and, as Betty fights off his advances, begins to sing with her a duet of (Calloways) "Youve Got to Hi-De-Hi." Betty loosens up and joins in, and the two begin to flirt with each other. After his first verse, the Old Man looms menacingly over Betty.

"Whatcha gonna do now?" Betty asks, frightened.

"Gonna do the best I can," the Old Man replies, launching into a jazzy dance routine.  The Old Man and Betty continue to dance together, but when the song is over, the Old Man makes a lustful grab for Betty, who runs for her life back down the mountainside.

The Old Man makes chase, and grabs Betty just long enough to catch hold of her dress, which Betty jumps out of. As Betty finds refuge behind a large tree in her underwear, her dress comes to life and slaps the Old Man before running back to its owner. Betty climbs the tree to apparent safety, but as the Old Man comes over and attempts to coax her down with (Calloways) "The Scat Song", he picks the tree up and bounces it on the ground, causing Betty to slide down.

Before he can have his way with her, however, the animals from the village rally to Bettys aid and surround the Old Man, tying his arms and legs together by a tree.  They then proceed to beat him up, tickle and humiliate him, thus exacting revenge for all the times he had made their lives a misery, with Betty watching with glee.

== Controversy ==
According to film historian Christopher Lehman, the sexually suggestive nature of this film caused "some Americans at the time, especially Catholics," to complain to exhibitors who then pressured Paramount Studios (distributor of the Betty Boop series) to tone down the Betty Boop character, which subsequently pressured Fleischer Studios to do the same thing. This can be seen when an old man sees Betty and acts crazy, a fish starts to follow her before getting hit by his wife, and Bettys dress even is removed in one scene.  According to Lehman, "In dispensing with the African-American entertainers and their music after limiting the Betty Boop series sexual references,   Fleischer thus acknowledged the widely assumed connection between raciness and blackness."  After 1934, African-American jazz music would no longer appear in Betty Boop cartoons, and she metamorphosized into a more conservative, mature, domestic character who often played only a supporting role. 

== Notes == Minnie the Moocher and Snow White (1933 film)|Snow-White. As in the other two cartoons, film footage of Cab Calloway was Rotoscoping|rotoscoped, or traced into animation, to provide the dance steps for the Old Man during his duet of "Youve Got to Hi-De-Hi". As with many other Boop shorts, The Old Man of the Mountain is now in the public domain.

*The dialogue between Betty and the Old Man ("Whatcha gonna do now?" "Gonna do the best I can!") is mirrored almost exactly in a scene between Santa Claus and Oogie Boogie in the 1993 film The Nightmare Before Christmas. The scene also features music very similar to Calloways "Minnie the Moocher" and dance steps close to Calloways own.

==References==
 

== External links ==
*  
*   on YouTube
*   (public domain, MPEG4, 8.2MB)
*   at Heptune.com
 

 
 
 
 
 
 
 
 
 
 
 