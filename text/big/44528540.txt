Happy Journey (2014 Marathi film)
{{Infobox film
| name           = Happy Journey
| image          = Happy Journey Marathi film poster.jpg
| alt            =
| caption        = Theatrical release poster
| film name      = 
| director       = Sachin Kundalkar
| producer       = Sanjay Chhabria
| writer         = 
| screenplay     = Sachin Kundalkar
| story          = Sachin Kundalkar
| based on       = 
| starring       = Atul Kulkarni  Priya Bapat
| narrator       = 
| music          = Karan Kulkarni
| cinematography = Rangarajan Ramabadran
| editing        = Jayant Jathar
| studio         = Everest Entertainment
| distributor    = 
| released       =  
| runtime        = 129 minutes
| country        = India
| language       = Marathi
| budget         = 
| gross          =   
}} Marathi drama film directed by Sachin Kundalkar and produced by Sanjay Chhabria under the banner of Everest Entertainment. It features actors Atul Kulkarni and Priya Bapat in the lead roles.  
 
The story is about love of brother and sister,A sister who hardly has seen her brother in her life. So after her death her ghost shares her experiences and helps her brother and finally disappears.

== Cast ==
* Atul Kulkarni as Niranjan
* Pallavi Subhash as Alice
* Priya Bapat as Janaki
* Chitra Palekar as Ansuya (Alices mother)
* Madhav Abhyankar as Niranjan & Janakis father
* Shiv Kumar Subramaniam as  Andrew (Alices father)
* Siddharth Menon as Ajinkya
* Suhita Thatte as Janakis mother

== Music ==
{{Infobox album  
| Name       = Happy Journey
| Type       = soundtrack
| Artist     = Karan Kulkarni
| Cover      =
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Everest Entertainment
| Producer   = 
| Last album = That Day After Everyday (2013)
| This album = Happy Journey (2014)
| Next album = 
}}
The soundtrack of the film has been given by Karan Kulkarni and lyrics have been penned by Omkar Kulkarni, Kshitij Patwardhan and Tejas Modak.   The first single "Fresh", sung by Shalmali Kholgade released on 8 July 2014. 

{{Track listing
| collapsed       = 
| headline        = Happy Journey (Original Motion Picture Soundtrack)
| extra_column    = Singer(s)
| total_length    = 12:50
| all_music       = Karan Kulkarni
| lyrics_credits  = yes
| title1          = Fresh
| extra1          = Shalmali Kholgade
| lyrics1         = Omkar Kulkarni
| length1         = 3:57
| title2          = Ka Saang Na
| lyrics2         = Kshitij Patwardhan
| extra2          = Shreya Ghoshal, Swapnil Bandodkar
| length2         = 5:00
| title3          = Aakash Jhale
| note3           = Lullaby
| lyrics3         = Tejas Modak
| extra3          = Shalmali Kholgade
| length3         = 3:53
}}

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 

 