Veede
{{Infobox film
| name           = Veede
| image          = Veede poster.jpg Dharani
| starring       = Ravi Teja Reema Sen Arti Agarwal Sayaji Shinde
| director       = Ravi Raja Pinisetty
| producer       = Singanamala Ramesh Chakri
| released       =  
| length         = 180 minutes
| country        = India
| language       = Telugu
| budget         =
}} Vikram and Jyothika in lead role.

==Plot==

Edu Kondalu (Ravi Teja) is an uneducated, head strong youth in Bobbarlanka village. He and his village heads decide to close down the nearby factory by appealing to politicians as its letting out a lot of pollutants into the river which is the main source of food, water and livelihood for them. He and another girl Manga Thaayaru (Aarti Agarwal) are sent to Hyderabad to appeal to MLA of their constituency - Byragi Naidu (Sayaji Shinde). They stay at a Friends place (Ali (actor)|Alis house) and try to get their work done by the minister. In the process many hurdles come across them. Edu Kondalu roughs up a few of the MLAs goons and is always at loggerheads with his henchmen. After all this, Byragi Naidu begins to show his true colors and plans to butcher Edu Kondalu as he is forming as an obstruction to his underground activities, unknowingly. Then, the protagonist of the story decides to teach the MLA a lesson, get his job done of closing down the factory. Swapna (Reema Sen) is a journalist who stays in Edu Kondalus locality and has a heavy crush on him. She also helps him in pinning down the minister. The remaining plot is how intelligently Edu Kondalu outclasses the minister and earns good name. Also in the plot, the hero makes use of Shakeelas films to defame the minister and she herself makes an appearance in the film.

==Cast==
  was selected as lead heroine marking her first collaboration with Ravi Teja.]]
* Ravi Teja .... Edu Kondalu
* Aarti Agarwal .... Manga Thaayaru Sen .... Swapna
* Sayaji Shinde .... MLA Byragi Naidu Ali .... Sunny (Edu Kondalus Friend)
* Manoj K. Jayan .... Venkiteshwara Rao
* Nalini .... Lady Don Swarnakka
* M S Narayana Venu Madhav
* Dharmavarapu Subramanyam .... Personal Assistant of Byragi Naidu
* Telangana Sakuntala .... Manga Thaayarus Grandmother Archana
* Ravi Varma

== External links ==
*  

 

 
 
 
 

 