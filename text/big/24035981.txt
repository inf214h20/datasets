Doll Face
 
{{Infobox film
| name           = Doll Face
| image          = Poster - Doll Face (1945).jpg
| image_size     =
| caption        = Film poster
| director       = Lewis Seiler
| producer       = Bryan Foy
| writer         = Harold Buchman (adaptation) Gypsy Rose Lee (play The Naked Genius) Leonard Praskins (writer)
| cinematography = Joseph LaShelle
| editing        = Norman Colbert
| distributor    =
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 
 .]]

Doll Face is a 1945 American film directed by Lewis Seiler starring Vivian Blaine as "Doll Face" Carroll.

The film is also known as Come Back to Me in the United Kingdom.

==Plot summary== dedicate the book to Mike with "For the love of Mike".

Another performer in the burlesque show, Chita Chula, remarks that if the book is a success and Doll Face leaves the show it will probably have to close down. Mike then decides to produce a Broadway show of his own with the financial aid of the performers themselves. Frederick offers to put up any money missing. Chita Chula (portrayed by Carmen Miranda) is sceptical she can pull it off, but Mike assures her shell "probably wound up being another Carmen Miranda!", something Chita Chula perceives as an insult.

Mike leaks word on the book to the press and, riding the publicity, argues the show got all the press it needs and that the book, although all but finished, needs not to be published. Doll Face, however, decides to go through with it and goes to Jamaica with Frederick for some final touch-ups. Boat engine trouble leaves them marooned on an island and when Mike finds them he misreads the situation and breaks up with her. Without "Doll Face" as headliner, the Gayety Theatre struggles and Mike is forced to finally shut it down.

Doll Face releases her book The Genius DeMilo and when Mike sees she dedicated the book to Frederick instead of him, he regrets leaving her. After Doll Face refuses to talk to Mike he sends a lawyer to stop her show in the middle of opening night since she is under contract not to appear in any show not produced by him. She agrees to see him and he asks her forgiveness. They reunite, she tricks the producer of her show to give Mike a 25% share and co-producer credit so the show can continue.

===Differences from play===
The film is based on the 1943 play The Naked Genius written by Gypsy Rose Lee.
 

==Cast==
*Vivian Blaine as Mary Elizabeth (Maybeth) "Doll Face" Carroll
*Dennis OKeefe as Michael Francis "Mike" Hannegan
*Perry Como as Nicky Ricci
*Carmen Miranda as Chita Chula Martha Stewart as Frankie Porter Stephen Dunne as Frederick Manly Gerard (credited as Michael Dunne)
*Reed Hadley as Flo Hartman
*Stanley Prager as Flos aide
*Charles Tannen as Flos aide
*George E. Stone as stage manager
*Frank Orth as Peters
*Donald MacBride as Ferguson (lawyer)
*Robert Mitchum as passenger (uncredited)

  Vivian Blaine as "Doll Face" Carroll Dennis OKeefe as Mike Hannegan Perry Como as Nicky Ricci Carmen Miranda as Chita Chula Martha Stewart as Frankie Porter Stephen Dunne as Frederick Manly Gerard
 

==Soundtrack==
*Vivian Blaine - "Somebodys Walking in My Dream" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*Perry Como and Martha Stewart - "Somebodys Walking in My Dream" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*Perry Como and chorus girls - "Red Hot and Beautiful" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*Vivian Blaine and male quartet - "Red Hot and Beautiful" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*Perry Como - "Here Comes Heaven Again" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*Perry Como and Vivian Blaine - "Here Comes Heaven Again" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*Perry Como and Martha Stewart - "Dig You Later (A-Hubba Hubba Hubba)" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*Carmen Miranda, Bando da Lua and chorus - "Chico Chico" (Music by Jimmy McHugh, Lyrics by Harold Adamson)
*"The Parisian Trot" (Music by Lionel Newman, lyrics by Charles E. Henderson)

==Production and filming==
  and Dennis OKeefe in in movie scene.]]
 
The working titles of this film were The Naked Genius and Heres a Kiss. Playwright and renowned stripper Gypsy Rose Lee is credited onscreen as Louise Hovick, which was her real name. Noting the billing, the Var reviewer commented: "Pic could be exploited by use of the fact that Gypsy Rose Lee wrote the stage play, The Naked Genius, on which the script is based, and also by the fact that film has an autobiographical tinge in its story of a burlesque queen who writes the story of her life. But Twentieth Century-Fox, presumably fearing a boomerang, credits neither play nor authoress w.k.   stage-name, merely listing pic as based on a play by Louise Hovick." Although a 6 Apr 1944 HR news item noted that producer George Jessel offered Lee a role in the picture, she does not appear in the finished film.

In Jun 1944, HR announced that Carole Landis would star in the film, and that Jackie Gleason would have the "comedy lead." According to Jul 1945 HR news items, William Eythe was scheduled to play the "romantic lead," and fifteen-year-old singer Hazel Dawn had been included in the cast. Dawns appearance in the completed picture has not been confirmed, however. Dennis OKeefe was borrowed from Edward Smalls company for the film, which marked the screen debuts of Martha Stewart and Lex Barker. According to a 3 Aug 1945 DV news item, producer Bryan Foy filled in for director Lewis Seiler for three days while Seiler was ill. According to information in the MPAA/PCA Collection at the AMPAS Library and the Twentieth Century-Fox Records of the Legal Department, located at the UCLA Arts--Special Collections Library, the Breen Office refused to allow the studio to use The Naked Genius as either the title of the film or of "Doll Faces" autobiography. The PCA also strongly protested the depiction of Doll Face as a stripper and disapproved several screenplays submitted by the studio. In late Jul 1945, PCA head Joseph I. Breen cautioned studio public relations head Jason S. Joy: "Please have in mind that any time you undertake to identify a character as a strip tease artist, you run the risk of giving enormous offense everywhere. People, pretty generally, look up   the business of the burlesque shows--and, more importantly, the strip tease--as, possibly, the very lowest form of public entertainment, and this same viewpoint is reflected in the reaction of the Censor Boards." In Aug 1945, Joy replied to Breen, in regard to a conference concerning the film attended by studio and PCA officials: "All of us distinctly understood that the strip tease flavor in Doll Face was not considered by you to be a violation of the Code, but that you were merely (and earnestly) warning us that any reference to strip tease would get us into an awful lot of trouble." Although the PCA continued to protest the films burlesque setting, it was eventually given a production code seal and encountered few problems with state or city censor boards. The Breen Office also disapproved the lyrics for "Chico Chico (From Porto Rico)," stating that it constituted "a burlesque of the Latin-American character, and hence as such would unquestionably give offense to Latin Americans generally." The song lyrics were changed slightly and later approved.

According to the studio records, Jimmy McHugh and Harold Adamson submitted the song "True to the Navy" for inclusion in the film, and a production number featuring it was filmed. McHugh and Adamson had previously submitted the song to Paramount Pictures|Paramount, however, which used it in their 1945 release Bring on the Girls. Paramount refused to license the song for use in Doll Face, and the number, which cost between $60,000 and $75,000 to film, had to be cut. In a Dec 1945 letter to Twentieth Century-Fox studio president Spyros Skouras, studio attorney George Wasson speculated that Paramount refused to license the song because Twentieth Century-Fox had obtained the distribution rights to Tales of Manhattan, which Paramount had desired, and because Twentieth Century-Fox had succeeded in getting clearance for the use of the title Sentimental Journey , which Paramount also wanted. The legal records also reveal that Irving Weissman filed suit against the studio, claiming that the song "Dig You Later (A Hubba-Hubba-Hubba)" had been plagiarized from one of his compositions. The case was dismissed in Sep 1948 by a federal court judge, but Weissman again filed suit through a state court. The disposition of the second suit has not been determined.  

==Reviews==
The studio Twentieth Century-Fox reportedly paid Louise Hovick (Gypsy Rose Lee) much money for the rights to The Naked Genius. Bosley Crowther of The New York Times wrote that "some one (not Miss Hovick) made a terrible deal.   the only distinction in its writing is a persistence in grammatical mistakes. The only remarkability about its pattern is a monotonous fidelity to form."  and "Forget the plot, and concentrate on the production numbers performed with gusto by Blaine, Como, and Carmen Miranda." 

"A very surprising choice, I think, is Carmen Miranda for "Doll Face". Not that she isnt one of out best dancers, and certainly one of our most persuasive personalities, but she is so different from Joan Blondell, who created the role on the stage in "Naked Genius" from which "Doll Face" is taken" said Louella Parsons. 

The newspaper   makes a handsome, hard-hitting manager and performs with great sincerity. Perry Como sings in an even more attractive manner than hitherto, and Vivian Blaine is more than adequately attractive, if a trifle too polished, as the "burleycue" blonde. Carmen Miranda appears in a straight part with only one singing number. The innovation is not a success, but the fault is the directors not Carmens." 

==DVD release==
The film was released on DVD in June 2008 as part of Foxs "The Carmen Miranda Collection."  

==Gallery==
{{Gallery
|title=
|footer=
|width=175
|height=130
|lines=3 Nicky performs "Red Hot and Beautiful" at the burlesque Gayety Theatre. Doll Face perform "Red Hot and Beautiful" to an enthusiastic crowd. Nicky performing "Here Comes Heaven Again" Doll Face on her way to make the final ajustments to her autobiography. Frankie and Nicky perform "Somebodys Walking in My Dream". Frankie performing "Somebodys Walking in My Dream" Frankie and Nicky perform "Somebodys Walking in My Dream". A worried Mike searching for Doll Face. Mike finds Doll Face and Frederick. Nicky and Frankie rehearses "Dig You Later (A-Hubba Hubba Hubba)". Frederick suspects Doll Face is unhappy. Chita suggests he should know. Rehearsing Nickys "Here Comes Heaven Again" proves too much for Doll Face.
}}

==References==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 