La Belle captive
{{Infobox film
| name           = The Beautiful Prisoner  La Belle captive 
| caption        = Film poster
| image	         = La Belle captive FilmPoster.jpeg
| director       = Alain Robbe-Grillet
| producer       = Anatole Dauman
| writer         = Alain Robbe-Grillet Frank Verpillat
| starring       = Daniel Mesguich
| music          = 
| cinematography = Henri Alekan
| editing        = Bob Wade
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}
 horror and mystery film directed by Alain Robbe-Grillet. It was entered into the 33rd Berlin International Film Festival.   

==Cast==
* Daniel Mesguich as Walter Raim
* Cyrielle Clair as Sara Zeitgeist
* Daniel Emilfork as Inspector Francis
* François Chaumette as Dr. Morgentodt
* Gabrielle Lazure as Marie-Ange van de Reeves
* Gilles Arbona as Le barman
* Arielle Dombasle as La femme hystérique
* Jean-Claude Leguay as Le cycliste
* Nancy Van Slyke as La serveuse
* Denis Fouqueray as Le valet (as Denis Foucray)
* Michel Auclair as La voix de Walter, off (voice)
* Roland Dubillard as Prof van de Reeves

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 