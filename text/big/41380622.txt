Jackpot (1992 film)
{{Infobox film
 | name = Jackpot 
 | image = Jackpot (1992 film).jpg
 | caption =
 | director = Mario Orfini
 | writer =  Adriano Celentano Mario Orfini Grazia Giardiello
 | starring =  Adriano Celentano Salvatore Cascio Christopher Lee
 | music = Anthony Marinelli Giorgio Moroder
 | cinematography =  Luciano Tovoli
 | editing =   Pietro Scalia
 | country = Italy
 | released =  
 | runtime = 110 minutes
 | language =  English - Italian
 }}
Jackpot (also known as Cyber Eden) is a 1992 Italian Sci-fi film|sci-fi-adventure film directed by Mario Orfini.

The film was a box office bomb, grossing just 158 millions lire at the Italian box office in spite of a budget of 18 billions lire.    

== Plot ==
In Italy an old female billionaire has created a multinational company called Financial Youth Foundation, which has taken seven prodigies children who work with new computers. The purpose of the foundation is to restore youth to old with artificial products. The gardener of the villa of the old billionaire, Furio, hdiscovers that this effect of youth, apparently beneficial, is a plan to exploit people with technology. So Furio is opposed to this system, so that the seven children and the younger generation can savor the beauty of nature, fighting the false and malicious technology.

== Cast ==

* Adriano Celentano: Furio
* Kate Vernon: Prudunce
* Salvatore Cascio: Cosimo 
* Carroll Baker: Madame
* Scott Magensen: Vladimir
* Christopher Lee: Cedric 
* Johnny Melville: Synthetic man

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 