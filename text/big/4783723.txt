The Lucky Dog
 
{{Infobox film
| name           = The Lucky Dog
| image          = L&H_Lucky_Dog_1919.jpg
| image size     = 190px
| caption        = Theatrical poster
| director       = Jess Robbins
| producer       = Broncho Billy Anderson
| writer         =
| starring       = Stan Laurel Oliver Hardy Florence Gillet Jack Lloyd
| cinematography = Irving G. Ries
| editing        =
| studio         = Sun-Lite Pictures
| distributor    = Metro Pictures (as Reelcraft Pictures)
| released       =  
| runtime        = 24 minutes 17 minutes (cut edition) English (original intertitles)
| country        = United States
| budget         =
}} comedy duo of Stan Laurel and Oliver Hardy, later known as Laurel and Hardy and is the first occasion that they worked together.    
Though they appear in scenes together, they play independent of each other and not as the comedic team that they would later become.
The film was shot as two reels,    but some versions end abruptly after the first reel where Stan is robbed by Ollie. 

==Plot==
  scene, Stan Laurel (left), is attacked by Oliver Hardy (above) as Jack Lloyd spots that Stans faithful dog has retrieved Olivers lighted stick of dynamite.]]
Stan plays the hapless hero, who after being thrown out onto the street for not paying his rent, is befriended by a stray dog. The dog and Stan then bump into Oliver (playing a robber) who is holding someone up. Oliver, who in the process has accidentally placed his victim’s money into Stans back pocket, turns from his first victim (who runs off) to rob Stan. Oliver then steals the money he had already stolen, from a very bemused Stan who had thought he was broke.
 dog show. When his entry is refused, Stan sneaks in anyway, but is quickly thrown out, followed by all the dogs in the show.
Stan spots the poodle’s owner outside looking for her dog and offers his dog in its place. She accepts and in turn offers him a lift to her home. This scene is witnessed by her jealous boyfriend, who happens to bump into Oliver and together the two plot their revenge on Stan.

At the ladys house, Stan is introduced to the boyfriend and Oliver (in disguise as the Count de Chease of Switzerland). The boyfriend proposes and is refused while Oliver attempts to shoot Stan only to have the gun jam. The boyfriend chases the lady around the house while Ollie tries to blow up Stan with a stick of dynamite. The dog comes to the rescue, chasing Ollie and the boyfriend into the garden with the dynamite and leaving them to be blown up.

==Production and release date==
 license plate in one shot of the complete film, the most likely date filming took place is in the latter part of 1920 and into early 1921. Mitchell, 2010 p. 181  The film was released for distribution in late 1921 by Reelcraft. 

The films production cost was thought to have been about $3000. 

==Cast & Crew==
 
*Stan Laurel - Young man
*Oliver Hardy - Bandit
*Florence Gillet - Girl
*Jack Lloyd - Boyfriend
*Gilbert M. Broncho Billy Anderson - Producer
*Irving G. Ries - Cinematography

==References==
;Notes
 
;Bibliography
* Mitchell, Glenn. The Laurel & Hardy Encyclopedia. New York: Batsford, 2010, First edition 1995. ISBN 978-1-905287-71-0.

==External links==
 
* 
* 

 
 

 
 
 
 
 
 
 
 
 