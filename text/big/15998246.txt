Gold Diggers in Paris
{{Infobox film
| name           = Gold Diggers in Paris
| image          = Gold Diggers in Paris.jpg
| image_size     = 287px
| caption        =
| director       = Ray Enright Busby Berkeley
| producer       = Hal B. Wallis (exec. prod.) Samuel Bischoff (both uncredited)
| story          = Story idea: Jerry Horwin   Richard Macaulay Maurice Leo
| writer         =  Peter Milne
| narrator       = Rosemary Lane Hugh Herbert Allen Jenkins. Freddie Fisher George Barnes (musical numbers)
| editing        = George Amy
| distributor    = Warner Bros.
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 movie musical Rosemary Lane, Hugh Herbert and Allen Jenkins.

==Plot== Rosemary Lane) to teach their girls ballet on the boat crossing the Atlantic.  Terry finds Kay very attractive, but things are complicated when his ex-wife, Mona (Gloria Dickson), invites herself along, rooming with Kay.

Meanwhile, the head of the real ballet company, Padrinsky (Curt Bois), finds out whats happened and cables Giraud aboard ship, then heads to Paris with his patron, a ballet-loving gangster named Mike Coogan (Edward Brophy), who intends to rub out Terry and Duke. Giraud is upset about being hoaxed, but is mollified when a "talking dog" (a ventriloquist hired by Terry and Duke) convinces him that Padrinsky is the liar.

After they arrive in Paris, a representative of the exposition, Pierre Le Brec (Melville Cooper), wants to watch the groups rehearsals, and Duke tells his new friend Coogan, the gangster, that Le Brec is causing him trouble. Coogan goes to "take care" of the problem, but by mistake knocks out Leoni instead of Le Brec.  Padrinsky shows up and arranges for the imposters to be deported on the day of the contest, but Mona manages to change the order so that Coogan and Padrinsky are shipped out instead, which allows the company to perform and win the grand prize.  

==Cast==
*Rudy Vallee as Terry Moore Rosemary Lane as Kay Morrow
*Hugh Herbert as Maurice Giraud
*Allen Jenkins as Duke Dukie Dennis
*Gloria Dickson as Mona
*Melville Cooper as Pierre LeBrec
*Mabel Todd as Leticia
*Fritz Feld as Luis Leoni
*Curt Bois as Padrinsky
*Edward Brophy as Mike Coogan
*Eddie Rochester Anderson as Doorman
*The Schnickelfritz Band as themselves

Cast notes:
*Carole Landis, Peggy Moran and Diana Lewis appear as Gold Diggers

==Production==
Gold Diggers in Paris was the fifth and last in Warner Bros. series of "Gold Digger" films, following Gold Diggers of Broadway (1929), which is now lost; Gold Diggers of 1933, which was a remake of the earlier film, and the first to feature Busby Berkeleys extravagant production numbers; Gold Diggers of 1935; and Gold Diggers of 1937.   Majestic Pictures attempted to cash in on the "Gold Diggers" concept by naming a feature Gold Diggers of Paris, however Warner Bros. prevented this through legal action, and the filming and release of Gold Diggers in Paris may have been a part of the effort to protect what Warners considered to be their trademark.
 Burbank  from January to March 1938.   It premiered in New York City on June 1, 1938, and went into general release on June 11.   The film was known as The Gay Impostors in the U.K.

==Songs and music==
As usual for a Warner Bros. musical of this period, the extravagant musical numbers were created, designed, staged, choreographed and directed by Busby Berkeley.

The majority of the songs in Gold Diggers in Paris were written by the team of Harry Warren (music) and Al Dubin (lyrics), who contributed many of the songs in the Gold Diggers series and other Warner Bros. musicals.  "I Wanna Go Back to Bali",  "Latin Quarter" (a song which was later used frequently in Warner Bros. cartoons featuring Pepe Le Pew), "Lets Drink to a Dream", "Put That Down in Writing", "Stranger in Paree" and "Waltz of the Flowers" were their creations in this film.  In addition, Harry Warren wrote two other songs, "My Adventure" and "Daydreaming All Night Long", but with lyrics by Johnny Mercer. TCM    

 

===The Schnickelfritz Band===
 
The Schnickelfritz Band, ("snicklefritz|schnickelfritz" supposedly being German slang for "silly fellow"), a comedy musical group somewhat reminiscent of Spike Jones (who came later), performs novelty songs in the film. Led by Freddie Fisher, who played woodwinds, sang and also composed the song "Colonel Corn" for the band, the band consisted of Stanley Fritts (trombone, drums, jug, washboard), Nels Laakso (cornet, trumpet), Paul Cooper (piano, arrangements), Kenneth Trisko (drums), and Charles Koenig (string bass / tuba).  Original trumpeter Nels Laakso left to join The Korn Kobblers and was  replaced by trumpet player George Rock, who later became a key member of Spike Joness City Slickers.  The group, which was billed as "Americas Most Unsophisticated Band!", recorded for Decca Records, and were brought to Hollywood by Rudy Vallee after his agent saw them in St. Paul, Minnesota.

According to one source, Gold Diggers in Paris was almost complete at the time the band arrived in Hollywood, so the bands segments were inserted into the film with short intros and reaction shots used to connect them to the rest of the action.  The band broke up shortly after doing the film, with Fritts taking some of the members east to become the "Korn Kobblers", and Fisher staying in Hollywood to open a nightclub, where he appeared billed as "The Original Colonel of Corn".  Although the Schnickelfritz Band never appeared in another film, Fisher appeared in several others as a band leader.      

==See also== The Gold Diggers
*Gold Diggers of Broadway
*Gold Diggers of 1933
*Gold Diggers of 1935
*Gold Diggers of 1937
* Pre-Code Hollywood

==References==
;Notes
 

==External links==
* 
* 
* 

 

 
 

 
 
 
 
 
 
 
 
 