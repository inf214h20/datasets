All About Hash
{{Infobox film
| name           = All About Hash         
| image          = 
| image size     = 
| caption        =  Edward Cahn
| producer       = Jack Chertok Richard Goldstone
| writer         = Hal Law Robert A. McGowan
| narrator       =  Mickey Gubitosi George McFarland Carl Switzer Darla Hood Billie Thomas
| music          = 
| cinematography = Clyde De Vinna
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 minutes
| country        = United States
| language       = English
| budget         = 
}} short comedy Edward Cahn.  It was the 189th Our Gang short (190th episode, 101st talking short, 102nd talking episode, and 21st MGM produced episode) that was released.

==Plot== hash made from the Sunday-dinner leftovers, and Mickeys dad hates hash. To teach the two adults a lesson, the Our Gang kids stage a skit on a local radio program, ending with a heartfelt plea by Mickey to stop the quarrelling. {{cite web
|url=http://movies.nytimes.com/movie/226198/All-About-Hash/overview |title=New York Times: All-About-Hash|accessdate=2008-10-08|work=NY Times}} 

==Notes==
Janet Burston makes her Our Gang debut as a young radio contestest Mary Swivens singing "Tippi Tippi Tin".

==Cast==
===The Gang=== Mickey Gubitosi as Mickey Henry
* Darla Hood as Darla
* George McFarland as Spanky
* Carl Switzer as Alfalfa
* Billie Thomas as Buckwheat
* Leonard Landy as Leonard

===Additional cast===
* Janet Burston as Mary Swivens Barbara Bedford as Martha, Alfalfas mother
* Louis Jean Heydt as Bob Henry, Mickeys father William Newell as Alfalfas father
* Peggy Shannon as Edith Henry, Mickeys mother
* Ferris Taylor as Radio announcer
* Tommy McFarland as Extra
* Jo-Jo La Savio as Extra
* Harold Switzer as Extra

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 