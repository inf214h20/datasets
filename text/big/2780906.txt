Everything's Gone Green (film)
{{Infobox film
| name           = Everythings Gone Green
| image          = Everythings_gone_green.jpg
| caption        = Promotional poster for Everythings Gone Green
| producer       = Henrik Meyer, Chris Nanos & Elizabeth Yake
| director       = Paul Fox
| writer         = Douglas Coupland
| starring       = Paulo Costanzo Steph Song JR Bourne
| cinematography = David Frazee
| editing        = Gareth C. Scales
| distributor    = THINKFilm
| released       =  
| runtime        = 
| budget         = 
| country        = Canada
| language = English
}}
Everythings Gone Green is a 2006 Canadian comedy film directed by Paul Fox and written by Douglas Coupland. It was produced by Radke Films and True West Films. The distributor is ThinkFilm in Canada, and Shoreline Entertainment elsewhere and won the award for best Canadian feature film at the 2006 Vancouver International Film Festival.  

==Plot== IT corporation. dollars on the BC lottery, he trashes his office space, and resigns. Unfortunately, when he calls the lottery "Win Line" he discovers they havent actually won anything. By happy accident, Ryan is offered a job with the lottery bureau interviewing and photographing lottery winners. En route to the job interview he stops to see a beached whale and meets Ming, a set designer in a relationship with golf-course designer and scam artist Bryce.
 marijuana grow-op in the family basement, and when he re-visits lottery winners to discover that they are often worse off than they were before winning.

==Cast==
* Paulo Costanzo ... as Ryan
* Steph Song ... as Ming Yu
* JR Bourne	... as Bryce
* Aidan Devine ... as Alan Susan Hogan ... as Mom Tom Butler ... as Dad
* Peter Kelamis ... as Kevin
* Gordon Michael Woolvett ... as Spike
* Katharine Isabelle ... as Heather Tara Wilson ... as Marcia
* Chiu-Lin Tam ... as Granny
* Camyar Chai ... as Surjinder
* Jennifer Kitchen ... as Linda
* Alexus Dumont ... as Wendy
* Don Thompson ... as Mr. Connor
* Chang Tseng ... as Mr. Ho
* Mark Gibbon ... as Rory
* Melanie Blackwell	... as Receptionist
* Kit Koon ... as Ms. Hamada
* Steven Cree Molison ... as Biker

==Soundtrack== MTV Live to promote its release.
===Songs=== Black Mountain
# Hangover Days - Jason Collett
# Birdsong - The Golden Dogs Final Fantasy
# Fire - Jason Collett Sloan
# Andre Ethier Hawaii
# Violet Light - Raised by Swans Caribou
# 97 and 02 - Circlesquare
# I Gotta Plan (for Saturday Night) - The Deadly Snakes
# Monkey Mask - The Meligrove Band
# Small Town Murder Scene - The Fembots

==References==
 

==External links==
*  
*  
*   at Reel Film Reviews.

 
 
 
 
 
 