Little Moscow (film)
{{Infobox film
| name           = Little Moscow
| film name      =  
| image          = 
| director       = Waldemar Krzystek 
| starring       = Swietłana Chodczenkowa, Lesław Żurek, Dimitrij Uljanow
| released       =  
| runtime        = 114 minutes
| country        = Poland
| language       = Polish/Russian
}}
Little Moscow ( ) is a Polish-Russian co-production directed by Waldemar Krzystek and released in 2008.

==Plot==
It is 1967, the middle of the Cold War in Legnica, south western Poland. The Red Army have turned the town into the largest Soviet garrison on foreign soil due to Legnicas proximity to Czechoslovakia and East Germany. Wiera is the wife of the crack Soviet pilot Jura, but after attending a cultural event to ease Polish-Soviet tensions falls head over heels in love with Michał, a Polish officer. The forbidden love takes many twists and turns, and the tale begins and ends in post-Soviet Legnica in 2008 as both Jura and his angry daughter Wiera Junior try to make peace with the past.

== Cast ==
* Swietłana Chodczenkowa – Wiera Swietłowa jr/Wiera Swietłowa sr
* Lesław Żurek –  Michał Janicki
* Dimitrij Uljanow – Jura Swietłow, Wieras husband
* Weronika Książkiewicz

== Awards ==
At the 33rd annual Polish Film Festival in 2008 the category of best actress was won by Swietłana Chodczenkowa and Golden Lion by director Waldemar Krzystek. 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 

 