The Satin Woman
 
{{Infobox film
| name           = The Satin Woman
| image          = 
| caption        = 
| director       = Walter Lang
| producer       = Walter Lang Dorothy Davenport
| writer         = Walter Lang
| starring       = Dorothy Davenport
| cinematography = Ray June
| editing        = Edith Wakeling
| studio         = Gotham Productions
| distributor    = Lumas Film
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent
| budget         = 
}}
 silent drama film directed by Walter Lang and starring Dorothy Davenport also known as Mrs. Wallace Reid.     

==Cast==
* Dorothy Davenport as Mrs. Jean Taylor (as Mrs. Wallace Reid)
* Rockliffe Fellowes as George Taylor
* Alice White as Jean Taylor Jr.
* John Miljan as Maurice
* Winter Blossom as Maria (as Laska Winter)
* Charles A. Post as Monsieur Francis (as Charles Buddy Post)
* Ruth Stonehouse as Claire
* Gladys Brockwell as Mae
* Ethel Wales as Countess Debris

==Preservation status==
The film is preserved in the Library of Congress collection. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 