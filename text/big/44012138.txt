Jeevan Lata
{{Infobox film
| name           = Jeevan Lata 
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sagar Movietone
| writer         = Waqif 
| narrator       =  Motilal Gulzar Bhudo Advani
| music          = Pransukh Nayak
| cinematography = Faredoon Irani
| editing        = 
| distributor    =
| studio         = Sagar Movietone
| released       = 1936
| runtime        = 174 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1936 Hindi Motilal paired in several films right from Motilals first Shaher Ka Jadoo (1934), with Badami using the pair for many of the films he directed for Sagar Movietone.    This was Motilal’s fourth film.    The film starred Sabita Devi, Motilal, Gulzar, Sankatha Prasad, Bhudo Advani and Mehdi Raza. 

==Cast==
* Motilal
* Sabita Devi
* Bhudo Advani
* Sushila
* Pande
* Sankatha
* Mehdi Raja

==Music==
The music was composed by Pransukh Nayak. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-  
| 1
| Mero Dil Chheen Liyo Re 
| Sabita Devi
|-
| 2
| Mohe Prem Ke Jhoole
| Sabita Devi
|-
| 3
| Prem Ki Raah Mein Chalna Koi Aasan Nahin
|
|-
| 4
| Shabe Vasal Dulhan Bani 
|
|-
| 5
| Param Prem Paribrahm Jagat Mein
|
|}
 

==References==
 

==External links==
* 

 


 

 
 
 