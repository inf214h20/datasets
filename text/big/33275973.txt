Vana Yuddham
 
 

{{Infobox film
| name           = Vana Yuddham
| image          = Vana yuddham.jpg
| alt            =  
| caption        = 
| director       = A. M. R. Ramesh
| producer       = A. M. R. Ramesh V. Srinivas Jagadeesh
| writer         = A. M. R. Ramesh, A.S.ajayan bala Arjun Kishore Kishore Vijayalakshmi Vijayalakshmi
| music          = Sandeep Chowta
| cinematography = S.D. Vijay Milton Anthony
| studio         = Akshaya Creations Sai Sri Cinemas S Lad Entertainment
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Kishore plays Arjun and Vijayalakshmi in the lead. It is made as a bilingual, being simultaneously shot in Kannada as Attahasa. The movie released on 14 February 2013 with good & positive reviews.     http://www.deccanchronicle.com/channels/showbiz/others/veerappan’s-attahasa-rings-out-loud-724  

==Cast== Arjun as D.G.P K. Vijay Kumar Kishore as Veerappan
*Lakshmi Rai as Vijayata Vashisht Vijayalakshmi as Muthulakshmi Veerappan Rajkumar
*Ravi Kale as Senthamarai Kannan
*Jayachitra as Jayalalitha Sulakshana as Parvathamma Rajkumar Shikha as Chandhini
*A. M. R. Ramesh as Gurunath
* Vijay Anand as TV photographer Prakash
*V. I. S Jayabalan
* Yogi Devaraj as Veerappan Father
*Suma Guha

==Production== Special Task Vijayalakshmi was Sulakshana would play his wife Parvathamma.  Divya Spandana was initially supposed to enact the role of a journalist,  however the actress too dismissed the reports,  following which Nikita Thukral was approached for the role.  Lakshmi Rai then confirmed that she was offered the role.  Bhavana Rao, known under her stage name Shikha in Tamil cinema, revealed that she was playing a character called Chandhini, who was considered as "Veerappans right-hand".  Former Asst Director Nagappa Maradagi, who also was abducted by Veerappan, and Shivakumar Mugilan, who was part of Veerappans gang, were involved in the making of the film. 
 Anthony were confirmed as the cinematographer and editor, respectively,  while Sandeep Chowta was signed on to compose the films score, although initial reports suggested that Yuvan Shankar Raja would be the music director. 

==Release== Sun TV. The film was given a "U" certificate by the Indian Censor Board.

==Controversy==
Mysore based writer T. Gururaj, who wrote a book on the late forest brigand Veerappan, titled Rudhra Narthana, has filed a complaint against filmmaker A.M.R. Ramesh, for using his content in the film without his permission. 

Veerapans wife Muthulakshmi had filed a case against to ban the film as she feel that her husband has been portrayed in bad light.  The Supreme court of India asked the producers to pay Rs. 2.5&nbsp;million to the widow. The film was then released during the course of the day. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 