One Step Away (film)
{{Infobox film
| name           = One Step Away
| image          = One Step Away film poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Zhao Baogang 
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Shanghai Film Group Co.,Ltd China Film Group Corporation
| distributor    = 
| released       =   
| runtime        = 
| country        = China
| language       = Mandarin
| budget         = 
| gross          = chinese yuan|¥69.87 million (China)
}}
 suspense romance film directed by Zhao Baogang. It was released on September 19, 2014. 

==Cast==
*Sun Honglei
*Gwei Lun-Mei Alex Fong
*Xu Jinglei
*Huang Lei
*Jiang Qinqin
*Ada Choi
*Xi Meijuan
*Fang Jun
*Duanmu Chonghui
*Li Hua
*Tan Kai

==Theme song==
* "Ai Bu Ke Ji" (爱不可及)
** Lyrics: Lin Xi
** Music: Zhang Yadong
** Singer: Faye Wong

==Reception==
By September 28, it had earned chinese yuan|¥69.87 million at the Chinese box office. 

==References==
 

 
 
 
 
 


 
 