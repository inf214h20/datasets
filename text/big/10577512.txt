Skokie (film)
{{Infobox television film
| bgcolour =
| name = Skokie
| image =Poster of the movie Skokie.jpg
| image_sized =
| imagesize = 240px
| caption = 
| genre = Drama
| creator = 
| director = Herbert Wise Robert Berger Herbert Brodkin Bernard Sofronski (Executive Producer)
| writer = Ernest Kinoy
| starring = Danny Kaye Eli Wallach Carl Reiner George Dzundza Brian Dennehy Ed Flanders
| music = Ralph Berliner
| editing = Stephen A. Rotter
| distributor = CBS
| studio = Titus Productions
| runtime = 125 minutes
| country =  
| language = English language
| network = CBS
| released = November 17, 1981
}} NSPA Controversy of Skokie, Illinois, which involved the National Socialist Party of America.
 German television on March 3, 1997.

==Plot== concentration camp prisoners.
 
The Jewish community decides to stand against the rally at all cost to make sure that the Holocaust will never be forgotten or allowed to happen again.

Moderate leaders Bert Silverman (Eli Wallach) and Abbot Rosen (Carl Reiner) advise the Jewish community to ignore the neo-Nazis; the strategy they put forward is “quarantine”, isolating the meeting by totally ignoring the neo-Nazi presence and refusing to be provoked. The logic is simple: if the Jewish community refuses to acknowledge the rally and thus refuses to feed the media any publicity, the meeting will be futile and eventually forgotten.
 First Amendment in the United States of America.

==Analysis and background==
The film intermixes real and fictional characters and events, including fictionalizing aspects of some of the main characters. For example, American Civil Liberties Union
(ACLU) lawyer "Herb Lewisohn" (played by actor John Rubinstein) is fictional, apparently based on attorney (and later law professor) David A. Goldberger who argued the case in real life, {{cite web
| url = http://moritzlaw.osu.edu/faculty/bios.php?ID=25
| title = David A. Goldberger, Isadore and Ida Topper Professor Emeritus of Law
| publisher = Moritz College of Law, Ohio State University
| accessdate = 2010-09-19
}}  while ACLU national lawyer Aryeh Neier (played by actor Stephen D. Newman) is a real person. Similarly, Holocaust survivor "Max Feldman" is fictional, while Holocaust survivor Sol Goldstein (played by actor David Hurst) is a real person. {{cite book
| title = When the Nazis Came to Skokie: Freedom for Speech We Hate
| first = Philippa
| last = Strum
| series = Landmark Law Cases and American Society
| publisher = University Press of Kansas
| url = http://www.kansaspress.ku.edu/strwhe.html
| isbn = 978-0-7006-0941-3
| year = 1999
}}  Although filmed after it became public knowledge that the neo-Nazi leader Frank Collin was actually an ethnic Jew, the film makes no mention of this fact. Nor is any mention made of Collins 1979 conviction for child molestation.

==See also==
*American Civil Liberties Union
*Hecklers veto
*Illinois Holocaust Museum and Education Center
*Jewish Defense League
*National Socialist Party of America v. Village of Skokie

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 