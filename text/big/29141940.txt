A Virgin Among the Living Dead
{{Infobox film
| name     = A Virgin Among the Living Dead 
| image = 
| caption = 
| director = Jesús Franco
| producer = {{plainlist|
* Franco Gaudenzi
* Marius Lesoeur
}}
| writer = Jesús Franco
| starring = {{plainlist|
* Christina von Blanc
* Britt Nichols
* Howard Vernon
* Ann Libert
* Rose Kiekens Paul Muller
}}
| studio = Brux International Pictures
| distributor = Andrés Salvador Molina
| released =  
| runtime = 78 minutes
| country = {{plainlist|
* Spain 
* France 
}}
| language = French
| budget = $250,000  
}}
A Virgin Among the Living Dead is a 1971 Spanish-French Gothic horror film that was widely marketed as a zombie film. 

It was released in France and Italy in 1973 as Christina, princesse de lérotisme and I desideri erotici di Christine with new inserts featuring Alice Arno added to the film.  Jess Franco has said that the original shooting title of the film was The Night of the Shooting Stars / La nuit des étoiles filantes. 

Producer Marius Lesoeur later had horror director Jean Rollin direct some newly filmed zombie footage that was added to Francos film for its 1981 re-release. At this time, the English dub was also recorded.

==Plot==
A woman arrives from England to visit her estranged relatives in a small castle for the reading of her dead fathers will, she eventually discovers that they are all undead and her decision to live with them turns into a nightmare. She learns that the Queen of the Night has claimed her fathers eternal soul because he had committed suicide. Jess Franco himself played a co-starring role in this film as a mindless cretin named Basilio who walks the corridors talking in gibberish to a severed chickens head.

==Cast==
*Christina von Blanc as Christina Benson
*Britt Nichols as Carmencé (as Britt Nickols)
*Rosa Palomar as Aunt Abigail
*Anne Libert as The Queen of the Night
*Howard Vernon as Uncle Howard Paul Muller as Ernesto Pablo Reiner, Christinas father
*Jesús Franco as Basilio (as Jesús Manera)
*Nicole Guettard as Female Doctor (as Nicole Franco)
*Alice Arno as Princess of Eroticism (erotic inserts only)

==Home media releases==
The film was released on VHS in the United States by Wizard Video. 

The film was released on DVD in the United States by Image Entertainment in 2003 as part of their EuroShock horror line.   Some DVD releases features an extended version of the film that runs 13 minutes longer than the US and UK releases, because it contains the extra Jean Rollin footage added to the film eight years after its original production.

On August 20, 2013, a Blu-ray Disc edition, containing both cuts, was made available by Redemption. 
  

Alternate titles include: A comme apocalypse; Among the Living Dead; Christina chez les morts vivants; Christina, princesse de lerotisme; Christina, Sex Princess; Le Labyrinthe; La nuit des etoiles filantes; Los suenos eroticos de Christine; Zombie 4; and Zombie Holocaust. 

==Reception== Chris Alexander of Fangoria called the original Franco cut "a loose, atmospheric masterpiece of pure cinema".   Gordon Sullivan of DVD Verdict wrote that the Kino-Redemption release is "a triumph for Franco fans, though unlikely to appeal outside that demographic".   Bill Gibron of DVD Talk rated it 3.5/5 stars and called it "a truly unnerving experience representing Franco at his most visually arresting".   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle called it "an atrophied psychological horror, which is over-stylish and impressionistic to the point of incoherence".   

==References==
 

==See also==
*List of zombie films

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 