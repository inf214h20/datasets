Vellaripravinte Changathi
{{Infobox film
| name           = Vellaripravinte Changathi
| image          = Vellaripravinte_changathi.jpg
| image size     =
| border         =
thumbnail
| alt            =
| caption        =
| director       = Akku Akbar
| producer       = Arun Ghosh   Shijoy
| writer         =Ajay J
| screenplay     = G S Anil
| story          =
| based on       =  
| narrator       = Dileep   Kavya Madhavan
| music          = Mohan Sithara
| cinematography = Vipin Mohan  Samir Haq
| editing        =
| studio         = Chandvi Creations
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Dileep and Kavya Madhavan in the lead roles.    The film is loosely based on the famous Sulekha murder case. 

==Plot== Sheela and others. Unluckily Augustine Joseph was not able to get the film released. Later he commits suicide due to the debts from the production of the film.
After years his son revels the original prints of the film from Gemini Lab and sees it. Later he goes on with releasing the film and tries to find the people who did the characters. The film is well appreciated for the way of narration and picturisation which makes the viewer feels that they are actually watching an old Black and white movie of the 60-70s.

==Cast== Dileep as Mukkom  Shajahan / Ravi

* Kavya Madhavan as Mary Vargese / Sulekha 
* Manoj K Jayan as Krishnan / Basheer Indrajith as Manikkunuju Saikumar as Varikkodan Mash / Thangaluppappa Lal as Himself Vijayaraghavan Editor K.Sankunni
* Maniyanpilla Raju as Preman Mamukoya as Moonnaan
* Suraj Venjaramoodu as Himself Ramu as Augustien Joseph,father of Manikkunju
* Anil Murali as Moosa
* Sadiq
* Zeenath
* Cherthala Lalitha

==Childhood Cast==
* Sandra as Mary Vargese / Sulekha 
* Dhananjay as Mukkom  Shajahan / Ravi
* Thamanna as Radha / Ravis sister
* Sabith as Krishnan / Basheer

==Production==
The film was earlier titled Ithaano Valiya Karyam but later renamed to the present title.    It was shot in locations at Thodupuzha, Ernakulam, Pollachi and Kanyakumari.  Kavya Madhavan played the female lead in the film.

== Awards == Kerala state Best actor 2011

==Music==
The film music album has 4 songs (including a repeat track) written by Vayalar Sarath Chandra Varma and composed by Mohan Sithara.

===Track List===
# – Pathinezhinte  
# – Naanam Chaalicha  
# – Thekko Thekkorikkal  
# – Pathinezhinte Poonkaralil  

==References==
 

https://www.academia.edu/9204740/When_Ghosts_Come_Calling_Re-projecting_the_Disappeared_Muses_of_Malayalam_Cinema==External links==
*  
*  

 
 
 
 
 
 