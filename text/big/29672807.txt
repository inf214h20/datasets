Chore Chore Mastuto Bhai
{{Infobox Film | name = Chore Chore Mastuto Bhai
 | image = chore chore mastuto bhai.jpg
 | caption = DVD Cover
 | director = Anup Sengupta
 | producer = 
 | camera=Venkatesh
 | writer = Shankar Dasgupta
 | dialogue =  Jeet Jisshu Sengupta
 | music = Ashok Bhadra
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  2005
 | runtime = 130 min.
| country  = India Bengali
 | budget = 
 |   gross = 
 | preceded_by = 
 | followed_by = 
 }}
 2005 Bengali Indian feature directed by Anup Sengupta, starring Mithun Chakraborty, Chiranjit, Jisshu Sengupta, Koel Mallickand Dipankar Dey in the main roles.

==Plot==
Nagraj (Deepankar Dey) sends John to steal a rare Diamond worth crores from the museum. John betrays Nagraj and runs away with Diamond, but is killed by Ronnie, who leaves the Diamond with a taxi triver Yadav Das. Therefore Yadav is jailed in Johns murder case. His daughter Madhuri (Koel Mullick) disguises herself as a boy to run the taxi to support her family. Now Manik and Chand turn up at Madhuris house and claim to be her long lost uncles from Africa. Madhuri is in love with Rahul, a Nagrajs henchman. As the Diamond is the prime attraction, everyone wants its possession. In the climax, Manik and Chand rescue Rahul, Madhuri and Madhabi from Nagraj and the police arrest Nagraj.

==Cast==
*Mithun Chakraborty as Manik
*Chiranjit Chakraborty as Chand
*Jisshu Sengupta as Rahul
*Koel Mallick as Madhuri
*Dipankar Dey as Nagraj, the main antagonist
*Subhashish Mukherjee
*Shankar Chakraborty as Nagrajs henchman
*Anamika Saha
*Joy Badlani Jeet in special appearance

==References==
*http://www.induna.com/1000001721-productdetails/?oY6Sbg==
*http://www.gomolo.in/Movie/Movie.aspx?mid=15383
*http://www.servinghistory.com/topics/Chore_Chore_Mastuto_Bhai

 
 
 


 