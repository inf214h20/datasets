Aaghaat
{{Infobox film
| name           = Aaghaat
| image          = Aaghaat.jpg
| image_size     = 
| caption        =  
| director       = Vikram Gokhale
| producer       = Mohan Damle Sanjay Sathaye Shriram Dandekar
| writer         = Dr Nitin Lavangare
| cinematographer = Bhavesh Rawal
| starring       = Vikram Gokhale Mukta Barve Kadambari Kadam
| distributor    = Saransh Distributors Milind Lele
| released       = 24 Dec 2010
| runtime        = 130 minutes Marathi
| budget         = 
}}
Aaghat is a 2010 Marathi film directed by veteran movie and stage actor Vikram Gokhale. Produced by Sprint Arts Creations Pvt. Ltd. and producers Mohan Damle, Sanjay Sathaye and Shriram Dandekar, the film is based on a story written by Dr Nitin Lavangare.    The cast of the film includes actors Vikram Gokhale, Mukta Barve, Kadambari Kadam   and Dr Amol Kolhe. The film was shot in Pune and was released in theatres on December 24, 2010.   

The film deals with the bureaucratic structure in major private hospitals, the administrations approach towards patients and their relatives and ego issues of reputed medical practitioners.   

==Plot== consent form. malignant carcinoma, and no verdict is passed on the second ovary. Dr. Khurana orders Smita to remove both the ovaries, an order she refuses to comply with citing new research in medical journals. This enrages Dr. Khurana and he orders assistant Dr. Budhkar (Shashank Shende) to complete the surgery. 
 kidney racket. It is implied that the hearings end in Smitas favor, and Dr. Khurana is shown at home in distress.

==Cast==
{| class="wikitable" Vikram Gokhale|| Dr. Khurana
|- Mukta Barve|| Dr. Smita Deshmukh
|-
|Dr. Amol Kolhe|| Dr. Suren
|- Kadambari Kadam|| Sangeeta Pradhan
|- Aniket Vishwasrao|| Amol
|- Suhas Joshi|| Dr. Meera Desai
|- Shashank Shende|| Dr. Vilas Budhkar
|- Smita Tambe|| Mrs. Kalpana Budhkar
|- Manoj Joshi Manoj Joshi|| Doctor
|- Kiran Kumar|| Doctor
|- Deepa Shriram|| Doctor
|- Surekha Kudachi|| Maya
|- Kiran Karmarkar|| Dr. Deshpande
|- Vrushali Gokhale|| Mrs. Khurana
|}

==Reception== Marathi and Bollywood films, and said that Vikram Gokhale had managed to maintain the sensitiveness of the topic while still making the film entertaining to watch.    Despite appearing in the busy holiday season, the film was popular with the audiences. 

== References ==
 

 
 
 
 