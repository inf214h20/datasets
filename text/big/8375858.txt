Dance with Me, Henry
 
{{Infobox film
| name      = Dance with Me, Henry
| image     = dancewithmehenry.jpg
| caption   = Theatrical release poster Charles Barton
| producer  = Bob Goldstein
| writer    = Devery Freeman
| starring  = Bud Abbott Lou Costello Gigi Perreau Rusty Hamer Mary Wickes
| music     = Paul Dunlap
| editing   = Robert Golden
| distributor = United Artists
| released  =  
| runtime   = 80 min.
| language  = English
| budget    = $450,000 
}}
Dance with Me, Henry is a 1956 film starring the comedy team of Abbott and Costello. It is the final film that they starred in together, although Costello went on to star in one more film before his death, The 30 Foot Bride of Candy Rock.

==Plot== Richard Reeves), at Kiddyland to pick up the money and a plane ticket. Lou, however, informs District Attorney Proctor (Robert Shayne) of the plan and he shows up at Kiddyland during Bud and Mushies meeting. Mushie sees the DA and hides the money just before he murders Proctor and frames Lou for it. Miss Mayberry uses Lous arrest to take the children from his home.

Bud informs Mushie that he knows that he really killed Proctor, and Mushie threatens to kill him.  However Big Frank and Dutch (Paul Sorensen) kill Mushie.  They kidnap Bud and demand that he tell them where the money is hidden.  Meanwhile Lou is released by the police, with the intent that he will lead them to Bud.  Dutch then kidnaps Lou and takes him to their hideout, where Bud is also being held.  Bud lies and tells Big Frank that he knows where the money is and they all head to Kiddyland, with the police following them every step of the way.  Bud then tricks Big Frank into confessing to everything while they are inside the parks recording booth, then Lou grabs the recording and escapes into the park.  Shelly and Duffer have also escaped from Miss Mayberry and are now inside the park playing when they see Lou being chased.  They return to the orphanage to get help from the other children, and they all head back to Kiddyland.  The children then wreak havoc in the park, foiling the gangsters at every turn.  The police capture them, and the reward money that Bud and Lou receive is donated to the orphanage.  Miss Mayberry, seeing what a good role model Lou really is, returns custody of the orphans to him.   

==Cast==
===Main===
* Bud Abbott as Bud Flick
* Lou Costello as Lou Henry
* Gigi Perreau as Shelley
* Rusty Hamer as Duffer
* Mary Wickes as Miss Mayberry

===Supporting===
* Ted de Corsia as Big Frank
* Ron Hargrave as Ernie
* Frank Wilcox as Father Mullahy
* Sherry Alberoni as Bootsie
* Eddie Marr as Garvey Richard Reeves as Mushie
* Robert Shayne as Proctor
* Walter Reed as Drake
* Paul Sorensen as Dutch

===Cameo/Uncredited===
* Robert Bice as Policeman
* John Cliff as Knucks
* Phil Garris as Mickey
* Jess Kirkpatrick as Policeman
*David McMahon as Savoldi
* Gilman Rankin as McKay
* Rod Williams as Janitor

==Production==
Dance with Me, Henry was filmed from May 23 through June 22, 1956. It was their thirty-sixth feature film and their first after being dropped by Universal Pictures in 1955 following the completion of Abbott and Costello Meet the Mummy.  Independent film producer Bob Goldstein hired the duo for this film, which was released through United Artists. It turned out to be the last film that Abbott and Costello made together as a team, as they ended their partnership in July, 1957.  

The films title was taken from the 1955 song "The Wallflower (Dance with Me, Henry)", although the films plot has nothing to do with the song. 

During filming, Abbott and Costellos routine, Whos On First? was inducted into the National Baseball Hall of Fame in Cooperstown, NY, where a clip still runs continuously. 

A bit part in the film went to the "just released from contract" Mouseketeer Sherry Allen, who was advised by Lou Costello to return to the use (professionally) of her true surname, "Alberoni".  She took his advice, and has been known professionally since that time by her birth name Sherry Alberoni.

==Release==
Dance With Me, Henry received mixed reviews when it was theatrically released in December 1956. A.H. Weiler, reviewing the film for The New York Times, complained "it is perfectly clear that any attempt to lend dramatic dimension to the simple and egregious fantasy expected of an Abbott and Costello venture can be fraught with the makings of a loud backfire."  The New York Herald Tribune was more conciliatory, noting "this time, the team is more sedate" while praising Costello for eschewing slapstick comedy and "developing along the lines of a Chaplinesque character." 

During their appearance on Ralph Edwards This Is Your Life on NBC, they mentioned the release of this film. 

==DVD release==
Dance with Me, Henry was released on DVD in June 2005 by MGM Entertainment. It was paired in its release with another independently produced Abbott and Costello film, The Noose Hangs High (1948). 

==References==
  

==Further reading==
* Stephen Cox and John Lofflin. The Abbott and Costello Story. Cumberland House Publishing, 1997.

==External links==
* 

 
 

 
 
 
 
 
 
 