The Black Road
 
{{Infobox film
| name           = The Black Road
| image          = Blackroad-poster.jpg
| image_size    =
| caption        = The Black Road promotional poster
| director       = William Nessen
| producer       = Andrew Ogilvie
| writer         = Willian Nessen Lawrie Silvestrin
| music          = Tim Count Keith Van Geyzel
| editing        = Lawrie Silvestrin
| released       =  
| runtime        = 52 minutes
| country        = Australia
| language       = English
}} SBS in Australia, it has since been shown around the world at film festivals and presentations on the subject. The film is critically acclaimed and has received several awards, both Australian and international. The Black Road was among four films on the subject of separatism that the Indonesian Film Censorship Institute banned in the country.
__TOC__

==Plot== conflict which objective journalist to a supporter of the Free Aceh Movement (Gerakan Aceh Merdeka, or GAM).   

While the 2004 tsunami made Aceh, an Indonesian Province on the northern tip of Sumatra, well known across the world, most people remain ignorant of its 27-year struggle for independence.  
 General Bambang Darmono, the leader of the Indonesian military in Aceh. Gaining the trust of General Darmono, Nessen was able to obtain information and shooting opportunities that would have been unavailable to most other journalists.     During this period he fell in love with a trusted military translator, Sya’diah Syeh Marhaban, who was a spy for the separatist movement. They worked together, continuing to extract sensitive information from General Darmono.  Nessen, as his collection of footage grew, thought of making a film. This idea would later evolve into The Black Road.

Nessen and Marhaban were married in Aceh. Only a few days after the wedding Nessen’s best man, a human rights activist, was kidnapped and murdered by Indonesian security forces.    It became clear to Nessen that the independence movement was supported by the majority of the Acehnese. As his personal experiences began to influence him, he too began to support the Free Aceh Movement|GAM. He went to live on the front lines with the rebels many times, travelling between General Darmono and the guerrillas in secret. In total, Nessen spent more than a year with the GAM before the military realised what he was doing. He was hunted and almost killed by the Indonesian military, who accused him of espionage. After being ordered to stop reporting from rebel held areas, Nessen spent weeks running from the authorities. After many near-death experiences, he turned himself into the military and was then imprisoned for forty days. Following this he was deported to Singapore and banned from entering Indonesia for one year, a ban that has been renewed each year since 2004.   

==Production==
Between 2001 and 2003, Nessen spent almost a year in Aceh.  During this time, Nessen was repeatedly ordered to stop reporting from rebel-held areas.  Upon giving himself up to Indonesian authorities, Nessen was imprisoned for 40 days. The Indonesian military originally accused him of espionage, and threatened the death penalty,  but due to U.S. Embassy involvement in the case Nessen was found to be innocent, and was only charged with failing to inform officials of an address change and failing to report to martial law authorities.  Following this he was expelled from Indonesia and was denied entry for a year. 
 
A year after the films release, Nessen was invited to visit his wifes relatives in Aceh. However, upon arrival at the Polonia Airport on 19 April 2006, Nessen was denied entry to Indonesia. No reason was given to him, but his wife told newspapers that the government still considers him a threat. 

==Distribution== SBS and was aired on 22 August 2006. It has been shown at a range of other venues, including film festivals and presentations that William Nessen has given on the subject. {{cite web
|title=Event: Film Screening The Black Road: On the Front Line of Acehs War date =20 June 2006
|publisher=Aid Watch
|url=http://www.aidwatch.org.au/index.php?current=68&display=aw00928&display_item=1
|accessdate=23 April 2008}}  {{cite press release
|title=Kathy Bates conformed for Aspen Filmfest date =1 September 2005
|publisher=Aspen Filmfest
|format=PDF
|url=http://www.aspenfilm.org/docs/AFF05_BATES_RELEASE_9-1.pdf
|accessdate=23 April 2008}}    

==Reception==
Along with three similar films, The Black Road was banned from being shown at an international film festival by the Indonesian Film Censorship Institute.  This decision was attacked as a breach of free speech and freedom of the press. 

===Critical reception===

The film received good reviews from newspapers around the globe. 

===Awards===
* William Nessen was awarded Best Documentary for The Black Road at the 2006 Mumbai International Film Festival. 
* Andrew Ogilvie was awarded "Best Film of the Festival" for The Black Road at the 2006 Mumbai International Film Festival. 
* The Black Road won Screenrights Best Made-for-TV Documentary at the DOCNZ Film Festival. 
* The Black Road was given a Special Mention at the DOCNZ Film Festival.  AFI Award in 2005 for "Best Editing in a Documentary" by the Australian Film Institute. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 