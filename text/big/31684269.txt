Phantom of the Mall: Eric's Revenge
{{Infobox Film
| name           = Phantom of the Mall: Erics Revenge
| image          = PhantomOfTheMall-EricsRevenge-300.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Richard Friedman    
| executive producer=Charles W. Fries Paul Little Robert King 
| narrator       = 
| starring       = Derek Rydall Jonathan Goldsmith Rob Estes Pauly Shore Kari Whitman Morgan Fairchild   .  IMDB Full Credits ("verified as complete"). 
| music          = Stacy Widelitz
| cinematography = Harry Mathias
| editing        = Gregory F. Plotts Amy Tompkins
| distributor    = Fries Distribution Company
| released       = December 1, 1989
| runtime        = 91 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1989 horror film about a young man who apparently dies in a suspicious house fire after saving his girlfriend, Melody;  a year later, at the new mall built over the site of the burned-out house,  thefts and murders begin to occur as a mysterious figure secretly prowls around the shopping center and takes a keen interest in watching over and protecting Melody. The film was directed by Richard Friedman , and stars Derek Rydall, Jonathan Goldsmith, Rob Estes, Pauly Shore, Kari Whitman, Ken Foree and Morgan Fairchild.

==Synopsis==
In an unlit store, a shadowy figure steals clothes and a crossbow; when a mall guard approaches, the figure stabs him to death. Melody and her friend Susie get jobs at the new mall, just before its scheduled grand opening. The shadowy Eric, his face half scarred, excitedly sees Melody, his former girlfriend, from hidden vantage points around the mall, and leaves flowers and gifts for her. He breaks open a mannequin head to fashion a half-mask. When a mall maintenance worker spots Eric in the ventilation ducts, Eric kills him by forcing his head into a running fan. Eric attacks anyone who threatens Melody; he kills a guard, who spies on women in the dressing room via mall security video, by crushing him into an electrical panel using a forklift. A masked mugger attacks Melody in the parking lot, but Eric shoots him with the crossbow. Later the mugger is revealed to be the mall pianist; Eric kills him on the toilet with a poisonous snake.  The mall owners son, Justin, harasses Melodys friend Susie; Eric kills him with a lasso pulled into the mall escalator. Reporter Peter Baldwin becomes interested in Melody, investigates the suspicious fire, and with Melody, Susie and pal Buzz, tries to learn if Eric is still alive. A year earlier, Eric Matthews house was destroyed in a suspicious fire which apparently trapped and killed him, just after he saved his girlfriend Melody.  Now, a year later, in the mall built over the site of his home, Eric plants a time-delay bomb beneath the mall, timed to coincide with the malls grand opening.

Mall guard Christopher Volker attacks Melody and Peter at gunpoint as they talk, in a car, about the fire; he boasts that he is the arsonist. As they escape and Volker chases them with his car, Eric leaps onto the roof, distracting him and causing a crash. Later, Volker attacks Melody again, knocking her unconscious. Eric fights him, then kills him using the automatic box crusher, and carries Melody to his lair.
 full moon) tricks a guard away from the video surveillance booth  Tatum, Charles (March 29, 2003).  . eFilmCritic.com.   so he and Susie can search for Melody. Melody awakens, and talks with Eric. She is glad he is alive, but confesses she does not love him anymore; he angrily declares that he has planted the bomb, so she will die and be with him forever anyway.

As Peter searches for Melody in the tunnels, Eric deploys the snake to kill him, for being interested in Melody. When Peter retreats, then later finds them anyway, Eric tries to fight and kill him, but Eric is stunned by Melodys shout that she loves Peter. Peter takes the chance to knock Eric out, so the pair escape and are able to warn Buzz and Susie of the bomb. Eric revives and kills those involved in the arson, the coverup, and the construction of the mall: mall owner Harv Posner and the complicit Mayor Karen Wilton.  Melody, Peter, Buzz and Susie, and all of the mall patrons escape, as Erics subterranean bomb explodes, destroying the mall.

==Cast==
*Derek Rydall as Eric Matthews, The Phantom of the Mall
*Jonathan Goldsmith as Harv Posner, Mall Owner 
*Rob Estes as Peter Baldwin, Reporter
*Pauly Shore as Buzz, Yogurt Clerk
*Kimber Sissons as Susie, Fashion Clerk 
*Gregory Scott Cummins as Christopher Volker, Security Guard
*Tom Fridley as Justin, Posners Son
*Kari Whitman as Melody Austin
*Ken Foree as Acardi
*Morgan Fairchild as Mayor Karen Wilton
*Terrence Evans as Security Guard
*Dante DAndre as Piano man  
 

==Production==
The film was shot in southern California at Sherman Oaks Galleria, Promenade Mall (now Westfield Promenade) and Valencia Studios, "an old warehouse converted for the film."    Alhambra, California was also a location shoot.

==Release==
The film was released December 1, 1989. It was made available on video by Fries Entertainment in February 1990.  A DVD was released by Echo Bridge Home Entertainment in 2006.   

==Reception==
The Los Angeles Times was bluntly negative in its assessment of the film, comparing it with other Phantom of the Opera (adaptations)#Movies|Phantom of the Opera film adaptations: "this schlock-slasher version of Lerouxs shocker--with the Phantom turned into a burned, vengeful teen prowling the air-tunnels of a posh suburban shopping mall--is, hands down, the most inept, pointless, puerile and inane." The review continued: "scarcely a scene isnt gross or ridiculous, scarcely a performance isnt forced or shallow, scarcely a line of dialogue isnt a burbling, awkward cliche. Theres a perfection of awfulness here that almost commands respect; it cant have been easy to keep going on this picture after a look or two at the rushes."   

Charles Tatum of Australian review site EFilmCritic.com gave the film one star (of four), "Worse than Montezumas Revenge", writing "you probably did not need a plot sketch since the entire story is in the title. Someone named Eric is taking revenge against people as a phantom of a mall. This also means there is no suspense. We know Eric is behind this, but we still have to see Estes and Cute Girl go through the motions of a silly investigation." 

In his "Final Verdict," Andrew Smith of UK review site PopcornPictures.co.uk gave the film one star, stating "You should know the 80s slasher drill by now. No scares, suspense, story, acting or characters - just novelty death scenes and a psychotic villain. When a film fails to deliver on the latter two AND the rest, then its really bottom of the barrel stuff." Smith, Andrew (2000).  . PopcornPictures.co.uk. 

==References==
 

==External links==
*  
*   review. Ross Horsley, Anchorwoman In Peril blog, January 26, 2008.
*   reviews. Finalgirl blog, August 18, 2008.

 

 
 
 
 
 