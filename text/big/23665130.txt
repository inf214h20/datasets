A Man in Love (1987 film)
 
{{Infobox film
| name           = A Man in Love
| image          = AManInLove1987Poster.jpg
| caption        = French theatrical poster
| director       = Diane Kurys
| producer       = Diane Kurys
| writer         = Diane Kurys Israel Horovitz Olivier Schatzky
| screenplay     = 
| story          = 
| based on       =  
| starring       = Peter Coyote Greta Scacchi
| music          = Georges Delerue
| cinematography = Bernard Zitzermann
| editing        = Joële Van Effenterre
| studio         = 
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = France Italy
| language       = English
| budget         = 
| gross          = 
}}

A Man in Love ( ,  ) is a 1987 French-Italian drama film directed by Diane Kurys. Her first English language film, it was entered into the 1987 Cannes Film Festival.   

==Synopsis==
Jane Steiner (Greta Scacchi) is an actress on the verge of giving up on her unfulfilling career who is offered a small part in an American movie set in the 1950s about Cesare Pavese, in which Steve Elliot (Peter Coyote) is playing the lead. Steve is a charming American movie star who is passionate about the writer he is playing although his work unknown in America. He is married to Susan (Jamie Lee Curtis) who is at home in New York, but falls in love with his co-star, Jane on the set. Jane is playing the role of Gabriella, Paveses love just prior to his suicide. After meeting Steve, Jane abandons her relationship with lover and friend (Vincent Lindon). The story is told through Janes eyes, and explores the illusions, passions and dramas of the creation of cinema, as well as one womans journey from actress to writer, mirroring Kurys own.  

==Cast==
* Peter Coyote as Steve Elliott
* Greta Scacchi as Jane Steiner
* Jamie Lee Curtis as Susan Elliott
* Claudia Cardinale as Julia Steiner
* Peter Riegert as Michael Pozner John Berry as Harry Steiner
* Vincent Lindon as Bruno Schlosser
* Jean Pigozzi as Dante Pizani
* Elia Katz as Sam
* Constantin Alexandrov as De Vitta
* Jean-Claude de Goros as Sandro
* Michele Melega as Paolo
* Jole Silvani as Olga

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 