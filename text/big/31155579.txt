Thilothama
{{Infobox film 
| name           = Thilothama
| image          =
| caption        =
| director       = Kunchacko
| producer       = M Kunchacko
| writer         = Vaikkom Chandrasekharan Nair (dialogues)
| screenplay     = Vaikkom Chandrasekharan Nair Sathyan Madhu Madhu Sharada Sharada
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film, Madhu and Sharada in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir Sathyan
*Madhu Madhu
*Sharada Sharada
*Thikkurissi Sukumaran Nair
*Jijo
*K. R. Vijaya
*Kottarakkara Sreedharan Nair
*Philomina

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhaagyaheenakal || P. Leela || Vayalar Ramavarma || 
|-
| 2 || Chanchala Chanchala || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Devakumaara || S Janaki || Vayalar Ramavarma || 
|-
| 4 || Ezhara Veluppinu || P Susheela || Vayalar Ramavarma || 
|-
| 5 || Indeevaranayane || P Susheela, P. Leela || Vayalar Ramavarma || 
|-
| 6 || Poovittu Poovittu || P Susheela || Vayalar Ramavarma || 
|-
| 7 || Priye Pranayini || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 