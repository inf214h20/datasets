The Perfect Circle
 
{{Infobox film
| name           = The Perfect Circle  (Savršeni krug)
| image          = Savršeni krug (1997 film).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Ademir Kenović
| producer       = 
| writer         =
| screenplay     = Ademir Kenović  Abdulah Sidran  Pjer Žalica
| narrator       = 
| starring       = Mustafa Nadarević
| music          = Esad Arnautalić  Ranko Rihtman
| cinematography = Milenko Uherka
| scenography    = Kemal Hrustanović
| editing        = Christel Tanović
| studio         = 
| distributor    = 
| released       =    (Montreal World Film Festival) 
| runtime        = 113 minutes
| country        = Bosnia and Herzegovina Bosnian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Bosnian film siege of 1992-1996. It was written by Kenović with Pjer Žalica and the famous Bosnian poet Abdulah Sidran. The set designer was Kemal Hrustanović. The title derives from the ability of "Hamza" (played by Mustafa Nadarević) to draw perfect circles on paper.

==Plot==
A Bosnian poet (Hamza) lived with his family in Sarajevo during the hard times in the horrific siege of the city. The war in Bosnia was raging all around them. After sending his wife (Gospoda) and daughter (Miranda) to Croatia, he found at home two orphans Adis and Kerim, who escaped a massacre in their own village. They escaped from their village and came to Sarajevo in search of their aunt, which lived in the neighborhood called Bistrik. Hamza decided to help the boys by shielding them from the horrors of war. Together they fight for survival in this horrible war and the ongoing complete siege of the city. After a long search, Hamza discovers that the aunt of Adis and Kerim was a refugee in Germany. Upon learning this Hamza tries to save the kids by sending them out of the war zone. But the only way out was through the Sarajevo International Airport, which is a very dangerous passage occupied and monitored by Chetniks and their death squads as well as snipers.

In many scenes Hamza is seen with the two kids speaking monologues, while observing photos of his wife and daughter. All the poetry cited in the film by Hamza are verses written by Abdulah Sidran, renowned Bosnian writer and poet.

==Cast==
*Mustafa Nadarević - Hamza
*Almedin Leleta - Adis
*Almir Podgorica - Kerim
*Jasna Diklić - Gospoda
*Mirela Lambić - Miranda
*Ljubica Lohajner-Znidarić - an old woman
*Dragan Marinković
*Mira Avram - Hamzina majka
*Sabina Bambur
*Senad Bašić - Strazar
*Amina Begović - Gordana
*Vedrana Bozinović
*Bozidar Bunjevać - Grobar
*Ines Fančović - Baka
*Admir Glamocak - Staka
*Zaim Muzaferija - Asaf

==Awards== 1997 Cannes Film Festival  Tokyo Sakura Grand Prix" at 1997 Tokyo International Film Festival 

==See also==
 
*Bosnian War
*Siege of Sarajevo
*List of Bosnia and Herzegovina films

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 