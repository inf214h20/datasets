My Father the Hero (1994 film)
  
{{Infobox film
| name           = My Father the Hero
| image          = My_father_the_hero.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Steve Miner
| producer       = Jacques Bar Jean-Louis Livi
| screenplay         = Francis Veber Charlie Peters
| based on = Mon père, ce héros by Gérard Lauzier
| narrator       =
| starring       = Gérard Depardieu Katherine Heigl Dalton James Lauren Hutton Faith Prince Stephen Tobolowsky David Newman
| cinematography = Daryn Okada
| editing        = Marshall Harvey
| studio         = Touchstone Pictures Buena Vista Pictures
| released       = February 4, 1994
| runtime        = 90 minutes
| country        = France / U.S.A. French / English
| budget         =
| gross          = $25,479,558 (USA) 
| preceded_by    =
| followed_by    =
| website        =
}} 1994 English 1991 French Mon père, ce héros.  The remake was directed by Steve Miner and released by Touchstone Pictures.

== Plot ==
André (Gérard Depardieu), a Frenchman divorced from his wife, takes his teenage daughter, Nicole (Katherine Heigl), on vacation with him to The Bahamas. She is desperate to appear as a woman and not a girl, so in order to impress a local boy Ben (Dalton James), she makes up more and more ridiculous stories, starting with André being her lover and leading to some bizarre assumptions by the rest of the community.

André is desperate to make Nicole happy (especially as she is increasingly upset by his relationship with girlfriend Isobel) and so plays along with her crazy games, and the stories they make up get increasingly bizarre.

== Cast ==
* Gérard Depardieu as André Arnel, the father of the title character
* Katherine Heigl as Nicole Arnel, Andrés daughter
* Dalton James as Ben, the boy Nicole wants to impress
* Lauren Hutton as Megan Arnel, Andrés ex-wife and Nicoles Mother
* Faith Prince as Diana
* Stephen Tobolowsky as Mike
* Ann Hearn as Stella
* Robyn Peterson as Doris
* Frank Renzulli as Fred
* Manny Jacobs as Raymond Jeffrey Chea as Pablo
* Stephen Burrows as Hakim Michael Robinson as Tom
* Robert Miner as Mr. Porter
* Betty Miner as Mrs. Porter
* Emma Thompson as Isobel, Andrés girlfriend (uncredited)
* Roberto Escobar as Alberto

=== Minor Cast ===
*Dorian Jones as David
*Yusef Bulos as the cab driver
*Stacey Williamson as the airport bartender
*Malou Corrigan as Bens girlfriend
*Robin Bolter as Raymonds girlfriend
*Steve Wise as Bens father
*Judy Clayton as Bens mother
*Bonnie Byfield as Guest
*Jennifer Roberts as Girl #1
*Felicity Ingraham as Girl #2
*Noa Meldy as Pablos girlfriend
*Sid Raymond as the elderly guest
*Anthony Delaney as the hotel bartender
*Michelle Riu as the girl on TV
*Dave Corey as the father at the beach
*Tom Bahr as a guest Juan and Eileen Santillan as the tango dancers
*The then-current Baha Men: Isaiah Taylor, Nehemiah Hield, Colyn Grant, Fred Ferguson, Herschel Small, and Jeffery Chea

==Reception==

The movie received a negative reception.    It holds a 15% rating on Rotten Tomatoes.

===Box office===
The movie debuted at No. 4 behind  , Mrs. Doubtfire and Philadelphia (film)|Philadelphia. 

== Soundtrack ==
The film featured music and appearances by the Baha Men.  The groups songs create the movies island soundtrack. 

* "Back to the Island"
* "Mo Junkanoo"
* "Gin and Coconut Water (Traditional)"
* "Land of the Sea and Sun"
* "Oh, Father"
* "Island Boy"

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 