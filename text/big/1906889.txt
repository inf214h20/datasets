Threesome (film)
 
{{Infobox film
| name     = Threesome
| image    = Threesome ver1.jpg
| caption  = theatrical release poster
| director = Andrew Fleming
| producer = Brad Krevoy
| writer   = Andrew Fleming
| starring = Lara Flynn Boyle Stephen Baldwin Josh Charles Alexis Arquette Martha Gehman
| music    = Thomas Newman
| cinematography = Alexander Gruszynski
| editing  = William C. Carruth
| studio   = Motion Picture Corporation of America
| distributor = TriStar Pictures
| released =  
| country  = United States
| runtime  = 93 minutes
| language = English
| budget   = $10 million
| gross    = $14,815,317 
}}
Threesome is a 1994 American comedy-drama film, written and directed by Andrew Fleming. The film is an autobiographical comedy mixed in with some social commentary, and is based on the college memories of Fleming. It was given an R rating by the Motion Picture Association of America. The movie stars Lara Flynn Boyle, Stephen Baldwin and Josh Charles. 

==Plot==
Due to an administrative error two male college students, the shy and intellectual Eddy (Josh Charles) and the All-American jock Stuart (Stephen Baldwin), end up with a female roommate.  The university thought that Alex (Lara Flynn Boyle) was a man (based on her name) and thus the three students are forced to live with each other until the university can move Alex to a female residence hall.

Alex falls in love and tries unsuccessfully to seduce Eddy; Stuart is in love with Alex, and Eddy falls in love with Stuart.  The trio become good friends and scare off anyone who tries to seduce the other.  Eventually, Alex, Stuart and Eddy agree to have an actual threesome that seems to destroy the friendship, and raises the possibility that Alex might have become pregnant.

After the threesome, they start to drift apart. Three weeks later the semester ends and Alex moves to an apartment and Eddy gets a single dorm. Eddy (who acts as the films narrator) eventually finds a boyfriend, Stuart finds happiness in a monogamous relationship with a woman, and Alex remains single. While they now only see each other for lunch occasionally, they do not seem to regret the friendship they had while in college.

==Cast==

* Josh Charles as Eddy
* Lara Flynn Boyle as Alex
* Stephen Baldwin as Stuart
* Alexis Arquette as Dick
* Martha Gehman as Renay Mark Arnold as Larry
* Michele Matheson as Kristen
* Joanne Baron as Curt Woman

==Reception== Jules and Jim. Eddy, you wish."  Roger Ebert gave the film three stars and wrote "The dialogue is really the films strongest element. The three actors are all smart, and able to reflect the way kids sometimes use words, even very bold words, as a mask for uncertainty and shyness." 

==Home media==
In 2001, a DVD of the film was released with some special features: a directors audio commentary, an alternate ending, various language subtitles and cast talent files.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 