Shavukar
{{Infobox film
| name           = Shavukar
| image          = Shavukaru.jpg
| image_size     =
| caption        =
| director       = L. V. Prasad
| producer       = Alur Chakrapani B. Nagi Reddy
| writer         = Alur Chakrapani
| narrator       = Vangara Vallabhajosyula Sivaram
| music          = Ghantasala Venkateswara Rao
| cinematography = Marcus Bartley
| editing        =
| studio         =
| distributor    =
| released       = 1950
| runtime        = 177 minutes
| country        = India Telugu
| budget         =
}} Nagireddy and Alur Chakrapani|Chakrapani. Veteran South Indian actress Janaki has been named after the film as Sowcar Janaki.

==Plot==
Changayya (Subba Rao) is a wealthy Sowkar (businessman) whose son Satyam (N.T. Rama Rao) is studying in the city. His neighbour is Ramayya (Srivatsa). Changayya is affectionate towards his daughter Subbulu (Janaki) and wants to make her his daughter-in-law. Satyam also likes this idea. But due to some conflict in the village courtesy Bangarayya, Ramayya provides the police witness against Changayya citing his hand present in the messy affair. The two families are thereafter estranged. They start off with petty fights, getting worse every day culminating with Satyam and Ramayyas son Narayana (Sivaram) ending up in jail after being framed. The local goonda Sunnapu Rangadu (S.V. Ranga Rao) plans to rob Changayyas place, but his plans are spoiled by Subbulu and Ramayya. Both the families end their differences, and become friendly once again. At the end, Satyam and Subbulu marry and live happily.

==Credits==
===Cast===
* Sowcar Janaki	 ... 	Subbulu
* Govindarajula Subba Rao	... 	Showkaru Changaiah
* Nandamuri Taraka Rama Rao	... 	Satyam
* S. V. Ranga Rao	... 	Sunnapu Rangadu
* P. Santha Kumari  ...    Santhi
* B. Padmanabham  ... Polai
* Vallabhajosyula Sivaram   ...    Narayana Vangara ... Pantulu
* Kanakam	... 	Raami
* Sreevatsa	... 	Ramaiah
* Madhavapeddi Satyam
* Moparru Daasu
* Relangi Venkata Ramaiah  ...  Varalu (General store)

===Crew===
* Director: L. V. Prasad
* Assistant Director: Tatineni Prakash Rao
* Producers: B. Nagi Reddy and Alur Chakrapani
* Production company: Vijaya Pictures
* Writer: Alur Chakrapani
* Music Director: Ghantasala Venkateswara Rao
* Cinematography: Marcus Bartley
* Art Director: Madhavapeddi Gokhale
* Choreography: Pasumarti Krishnamurthi Samudrala
* Playback singers: Jikki Krishnaveni, M. S. Rama Rao, Pithapuram Nageswara Rao, Raavu Balasaraswathi, Ghantasala Venkateswara Rao and Madhavapeddi Satyam

==External links==
*  

 
 
 