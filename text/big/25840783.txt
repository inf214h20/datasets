Born to Ride (film)
{{Infobox film
| name           = Born to Ride
| image          =
| image_size     =
| caption        = Graham Baker
| writer         =
| narrator       =
| starring       = John Stamos
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}} Graham Baker.  The film was released on May 3, 1991 and starred John Stamos as a biker turned military Corporal. 

==Synopsis== John Stockwell), who dislikes both Gradys unorthodox methods and his interest in his daughter Beryl Ann (Teri Polo). But when Grady discovers that his unit is ill equipped to launch a rescue mission in Spain, he decides to accompany his unit in the hopes of increasing their chance of succeeding in the dangerous mission.

==Cast==
*John Stamos as Corporal Grady Westfall John Stockwell as Captain Jack Hassler
*Teri Polo as Beryl Ann Devers
*Sandy McPeak as Colonel James E. Devers (CO, 36th Div.)
*Kris Kamm as Bobby Novak
*Keith Cooke as Broadwater
*Dean Yacalis as Cartucci
*Salvator Xuereb as Levon
*Justin Lazard as Brooks
*Thom Mathews as Willis
*Garrick Hagon as Jim Bridges, State Department Official
*Matko Raguz as Esteban
*Lisa Orgolini as Claire Tate
*Ed Bishop as Dr. Tate
*Slavko Juraga as Capt. Rosario

==Reception==
The film was largely panned upon its release,  and Entertainment Weekly gave the movie a D+ rating and commented that it was "a big failure on the most basic dramatic level."  In a 2009 article PopCrunch listed Stamos on their "20 Worst Action Film Stars of All Time" article for his role in Born to Ride. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 