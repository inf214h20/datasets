Poru Telangana
 

{{Infobox film
| name           = Poru Telangana
| image          = 
| image_size     =
| caption        =
| director       = R. Narayana Murthy
| producer       = R. Narayana Murthy
| writer         = R. Narayana Murthy
| narrator       =
| starring       = R. Narayana Murthy
| music          = 
| cinematography = 
| editing        = 
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Poru Telangana is a 2011 Telugu-language film directed by R. Narayana Murthy.  The star cast includes R. Narayana Murthy.

==Synopsis==
Poru Telangana is a film on the 2009&ndash;11 Telangana movement.

==Songs==
{{Track listing
| lyrics_credits = yes
| extra_column = Singer(s)
| title1 = Chuda Telangana | extra1 = Mitra | lyrics1 = Ande Sri | length1 = 4:49
| title2 = Raathi Bommallona Koluvaina Shivuda | extra2 = Nitya Sanshini | lyrics2 = Mittapally Surendar | length2 = 6:40
| title3 = Osmania Campuslo | extra3 = Vandemataram Srinivas | lyrics3 = Abhinaya Srinivas | length3 = 4:35
| title4 = Raajiga Ori Raajiga | extra4 = Vandemataram Srinivas | lyrics4 = Guda Anjaiah | length4 = 4:35
| title5 = Ayyoniva Nuvvu Avvoniva Telanganoniki Thoti Paaloniva | extra5 = Telu Vijaya | lyrics5 = Guda Anjaiah | length5 = 6:00
| title6 = Gnanam Okadi Sottu | extra6 = Vandemataram Srinivas | lyrics6 = Vangapandu Prasad Rao | length6 = 6:00
| title7 = Entho Sahasamaindhi | extra7 = Mitra | lyrics7 = Abhinaya Srinivas | length7 = 5:15
| title8 = Telangana Vachedaka | extra8 = Vandemataram Srinivas | lyrics8 = Suddala Rajaiah | length8 = 5:15
}}

==References==
 

 
 
 
 


 