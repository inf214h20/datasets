The Well-Digger's Daughter (1940 film)
  
{{Infobox film
| name           = The Well-Diggers Daughter
| image          = WellDiggersDaughter.JPG
| image_size     = 
| caption        = Theatrical release poster
| director       = Marcel Pagnol
| producer       = Marcel Pagnol
| writer         = Marcel Pagnol Herman G. Weinberg (English titles)
| narrator       = 
| starring       =  
| music          = Vincent Scotto Willy
| editing        = Jeannette Ginestet
| distributor    = Siritzky International Pictures Corporation (USA)
| released       =  
| runtime        = 170 minutes
| country        = France
| language       = French
| budget         = 
}} French romantic comedy drama film directed by Marcel Pagnol.

==Plot==
Patricia, a peasant girl, becomes pregnant by Jacques, a military pilot from a local town family. He serves in World War I, while his family refuses to support the child and her father expels her from their home. Patricia stays with her aunt and gives birth to a boy. Jacques is reported killed in the war, and both families wish to meet his son, their new grandchild. Jacques is not dead, however; he returns and Patricia agrees to marry him. 

==Selected cast==
*Raimu as Pascal Amoretti
*Fernandel as Félipe Rambert
*Josette Day as Patricia Amoretti
*Line Noro as Marie Mazel
*Georges Grey as Jacques Mazel
*Fernand Charpin as Monsieur Mazel
*Milly Mathis as Nathalie Clairette as Amanda Amoretti
*Roberte Arnaud as Roberte Amoretti
*Raymonde as Éléonore Amoretti

==Reception== armistice in Vichy film". Most of the filming occurred before the defeat of the French Third Republic, however, with the scene with the speech added later.   

French actor Daniel Auteuil wrote, directed, and starred in a remake of this film in 2011. 

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 

 
 