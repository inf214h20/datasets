Circle Track Summer
 

{{Infobox film
| name           = Circle Track Summer
| image          = Circle Track Summer.jpg
| caption        = DVD coverart
| film name      = 
| director       = Steve Pallotta
| producer       = Steve Pallotta
| writer         =  
| starring       =  
| music          = Major Buzz and the 60 Cycle Hum
| cinematography = Chris Parker
| editing        = Guido Huckball
| studio         = Fools Gold Entertainment
| distributor    = CustomFlix
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $5000 (est)
| gross          = 
}}
 2005 film WNIR also stars in the movie.     The film is considered a "local cult favorite".   

==Production== Panasonic DVX100 digital video camera.      The film was edited in a room in the directors house and music was provided by musician friends of the director. 

Circle Track Summer is Steve Pallottas third full length feature, following his first two movies The Forbidden Closet and Lake Evermore. With this project, cinematographer Chris Parke, takes his production level "up several notches".   

The project was shot entirely on location in the Akron, Ohio area, with more than 200 locals involved in the one-year filming. The projects costume design was done by Susie Smith, known for her award-winning work on St. Elsewhere.   

The filmed debuted July 29,    and had a DVD release by CustomFlix on May 19, 2006.   
 Nina Rawls, wife of Lou Rawls. 

==Plot==
The owner of a failing race track seeks to reverse his fortune by holding a series of promotion events where the cars are driven by women.  Becky (JodyMarie Spiech) is a waitress who unknowingly puts her life in danger while attempting to save the family farm.  She had rediscovered her lost high-school sweetheart, reporter Scoop Hendrickson (Tony Rio), and must decide whether or not to tell him of what ended their romance.  To raise money to save the farm, she and three other financially strapped but voluptuous local girls, Su Shi (Jenna Christie), Tammy Lay (Heather Ley) and Jette Black (Lindsay Robertson), sign up for a new Powder-Puff auto racing event at the Barberton Speedway in hopes of winning cold, hard cash. The promotional catch developed by the track owner is that the women must wear bikinis as they race, and must drive the cars backwards. 

==Cast==
 
 
* JodyMarie Spiech as Becky Smith
* Tony Rio as Scoop Hendrickson
* Lindsay Robertson as Jette Black
* Heather Ley as Tammy Lay 
* Jenna Christie as Su Shi
* Tom Jenny as Kooter
* Thor Syvertsen as Bud
* Fig Jankowski as Chain Drive
* Doug Cusit as Uncle Bob
* Roger Samblanet as Uncle Dan
* Phil Skoff as Metal Gate 
* Paul Sherman as Anthony
 
* Dave Zimmerman as Cookie
* Tami Bowman as Caprice Black
* Barbara Evans as Mary
* Sherman Montgomery Jr. as Anthony
* Jacquelane Nespo as Summer
* Ada Carolina Ortiz as Djanga
* Tom Erickson as Track Announcer
* Richard Rolenz as Rocco
* Tom Rush as Deputy Sheriff
* Rick Montgomery as Hairdressers ghost
* Steve Pallotta as himself
 

==Release==
The film was released on DVD on March 19, 2006,  and screened April 14, 2007 at the Akron Independent Film Festival. 

==Response== Lost In Translation it’s not) and tolerance for ethnic jokes, sexual innuendo and nudity. The strengths lie mostly in three things: the willingness of the filmmakers to take a chance by poking fun at every known stereotype of Ohioans, and especially of natives of Barberton; Pallotta’s ability to avoid crossing the fine line between localizing a story by using well-known landmarks or inside jokes and providing free advertising by referring to everything by its full name and location - something natives of an area never do; and the supporting players." 

==References==
 

==External links==
*  
*   at the Internet Movie Database
*   at the AllMovie
*  

 
 
 
 
 
 
 