Les grandes vacances (film)
:For the unrelated 1940 novel by Francis Ambrière see Les grandes vacances
{{Infobox Film
| name           = Les grandes vacances
| image          = 
| caption        = 
| director       = Jean Girault
| producer       = Gaumont Film Company
| writer         = Jean Girault Jacques Vilfrid
| starring       = Louis de Funès
| music          = Raymond Lefevre
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1 December 1967
| runtime        = 85 min.
| country        = France/Italy
| awards         =  English
| budget         = 
| gross          = $52,401,877  
| preceded_by    = 
| followed_by    = 
}}
 Italian comedy movie from 1967, directed by Jean Girault, written by Jean Girault, and starring by Louis de Funès.  

== Plot ==
Charles Bosquier is the dictatorial headmaster of a French school. One of his own sons miserably failed his exams, so he sends him to England as exchange student.

== Cast ==

* Louis de Funès :  M. Charles Bosquier
* Claude Gensac : Mme Isabelle Bosquier
* Ferdy Mayne : Mac Farrell
* Martine Kelly : Shirley Mac Farrell
* Olivier de Funès : Gérard Bosquier
* François Leccia : Philippe Bosquier
* Maurice Risch : Stéphane Michonnet
* Jean-Pierre Bertrand : Christian, a friend of Philippe
* René Bouloc : Bargin, the pupil who leaves with Philippe
* Jacques Dublin : Claude, a friend of Philippe
* Dominique Maurin : Michel, a friend of Philippe

== References ==
 

==External links==

*  

 
 
 
 
 
 
 
 