İki Genç Kız
{{Infobox Film name = İki Genç Kız image = image_size = caption = director = Kutluğ Ataman producer = Murat Çelikkan
Gülen Guler Hurley writer = Kutluğ Ataman
Perihan Mağden narrator = starring = Feride Çetin
Vildan Atasever
Hülya Avşar music = Replikas cinematography = Emre Erkmen editing = Aziz İmamoğlu
Lewo distributor = Yalan Dünya, Filmpot released = 29 April 2005 runtime = 115 min. country = Turkey language = Turkish budget = gross = preceded_by = followed_by =
}}
 Golden Orange Turkish film by Kutluğ Ataman, produced by Gulen Guler Hurley, starring Feride Çetin, Vildan Atasever and Hülya Avşar. The book is based on Perihan Mağdens novel İki Genç Kızın Romanı (translated into English as 2 Girls), and it follows the novels plot closely although it diverges from it at some points.

The film is about two teenage girls, Behiye (Feride Çetin) and Handan (Vildan Atasever), with contrasting characteristics and backgrounds, forming a close relationship with sexual implications. As they become closer and closer the relationship becomes more fragile, and the impossibility of the survival of their relationship becomes more evident over time. Economic, social, psychological, and sexual problems come in the way.

Replikas, an experimental band provided the music for the film, and the music was published in 2006 with Film Müzikleri album.

==Awards==
The movie won 14 prizes in 2005 (Best Actress for Vildan Atasever, Best Cinematography for Emre Erkmen, and Best Director for Kutluğ Ataman) at Antalya Golden Orange Film Festival which gives out the most prestigious film awards in Turkey. Kutluğ Ataman was also awarded the Best Director in Istanbul International Film Festival for this film. 

==References==
 

==External links==
*  
*  
*   distributor page for the film

 

 

{{succession box |
  | before = Meleğin Düşüşü
  | after = Takva Golden Orange Dr. Avni Tolunay Jury Special Award for Best Picture
  | years = 2005
|}}

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 