Breaking the Waves
 
{{Infobox film
| name           = Breaking the Waves
| image          = Breaking the waves us poster.jpg
| caption        = Theatrical release poster
| director       = Lars von Trier
| producer       = Peter Aalbæk Jensen Vibeke Windeløv
| writer         = Lars von Trier Peter Asmussen
| starring       = Emily Watson Stellan Skarsgård Katrin Cartlidge Jean-Marc Barr Udo Kier
| music          = 
| cinematography = Robby Müller
| editing        = Anders Refn
| distributor    = October Films (US)
| released       =  
| runtime        = 158 minutes   
| country        = Denmark
| language       = English
| budget         = $7.5 million 
| gross          = $3,803,298 (USA) 
}}
Breaking the Waves is a 1996 film directed by Lars von Trier and starring Emily Watson. Set in the Scottish Highlands in the early 1970s, it is about an unusual young woman, Bess McNeill, and of the love she has for Jan, her husband, who asks her to have sex with other men when he becomes immobilized from a work accident. The film is an international co-production led by Lars von Triers Danish company Zentropa. It is the first film in Triers Golden Heart Trilogy which also includes The Idiots (1998) and Dancer in the Dark (2000).

==Plot== oil rig Calvinist church. Bess is steadfast and pure of heart, but extremely simple and childlike in her beliefs. During her frequent visits to the church, she prays to God and carries on conversations with Him using her own voice, believing that He is responding directly through her.

Bess has difficulty living without Jan when he is away on the oil platform. Jan makes occasional phone calls to Bess in which they express their love and sexual desires. Bess grows needy and prays for his immediate return. The next day, Jan is severely injured in an industrial accident and is flown back to the mainland. Bess believes her prayer was the reason the accident occurred, that God was punishing her for her selfishness in asking for him to neglect his job and come back to her. No longer able to perform sexually and mentally affected by the paralysis, Jan asks Bess to find a lover. Bess is devastated and storms out. Jan then attempts to commit suicide and fails. He falls unconscious and is readmitted to hospital.

Jans condition deteriorates. He urges Bess to find another lover and tell him the details, as it will be as if they are together and will revitalize his spirits. Though her sister-in-law Dodo constantly reassures her that nothing she does will affect his recovery, Bess begins to believe these suggestions are the will of God and in accordance with loving Jan wholly. Despite her repulsion and inner turmoil to be with other men, she perseveres in her own sexual debasement as she believes it will cure her husband. Bess throws herself at Jans doctor, but when he rebuffs her, she takes to picking up men off the street and allowing herself to be brutalized in increasingly cruel sexual encounters. The entire village is scandalized by these doings, and Bess becomes excommunicated. In the face of being cast out from her church, she proclaims "you cannot love words. You cannot be in love the Word. You can only love a human being."

Dodo and Jans doctor agree the only way to keep Bess safe from herself is to have her committed, and as far away from her husband as possible. It is then that Bess decides to make what she thinks is the ultimate sacrifice for Jan: she unflinchingly goes out to a derelict ship full of barbarous sailors, who rape and murder her. The church refuses to give a funeral for her and damns her soul to hell. Jan is later shown burying her in the ocean, in deep grief but fully restored to health. The film ends as church bells ring in the sky.

==Cast==
* Emily Watson as Bess McNeill
* Stellan Skarsgård as Jan Nyman
* Katrin Cartlidge as Dodo McNeill
* Jean-Marc Barr as Terry
* Adrian Rawlins as Dr. Richardson
* Jonathan Hackett as Priest
* Sandra Voe as Mother
* Udo Kier as Sadistic Sailor
* Mikkel Gaup as Pits
* Roef Ragas as Pim
* Phil McCall as Grandfather Robert Robertson as Chairman

==Style== realist Dogme 95 movement, of which von Trier was a founding member, and its grainy images and hand-held photography give it the superficial aesthetic of a Dogme film. However, the Dogme rules demand the use of real locations, whereas many of the locations in Breaking the Waves were constructed in a studio.  In addition, the film is set in the past and contains dubbed music, as well as a brief scene featuring CGI, none of which is permitted by the Dogme rules.

The film was made using Panavision equipment. The low-res look of the scenes was obtained by transferring the film to video, and then back to film again. According to von Trier, "what we did was take a style and lay it like a filter over the story. It’s like decoding a television signal when you pay to see a film. Here we encoded the film, and the audience has to decode it. The raw, documentary style that I imposed on the film, which actually dissolves and contradicts it, means that we can accept the story as it is. 

==Production== Emily Watsons audition, even though she was a complete unknown in the film industry at the time.

The exterior scenes were shot in Scotland: the graveyard was built for the film on Isle of Skye; the church is in Lochailort, the harbour in Mallaig, and the beach in Morar.  The interiors were shot at Det Danske Filmstudie, Lyngby, Denmark.

The helicopter used in the movie, G-BBHM, a Sikorsky S-61-N, was later involved in an emergency landing and fire that destroyed the aircraft but none of the four crew was injured. This occurred at Poole, Dorset on 15 July 2002.  

==Reception==

===Critical response===
During a show where film personalities listed their top movies of the 1990s, Breaking the Waves was named one of the ten best films of the decade by both critic Roger Ebert and director Martin Scorsese. 

===Box office===
Released on November 13, 1996, the film has grossed just over $4 million in the US. 

===Awards=== Grand Prix at the 1996 Cannes Film Festival,    the César Award for Best Foreign Film, and three awards at the 1996 European Film Awards, including Film of the Year, International Film Journalists Award, and European Actress of the Year (Watson). Emily Watson was also nominated for the 1996 Academy Award for Best Actress, the 1996 British Academy of Film and Television Arts award and the National Society of Film Critics prize.

==Operatic adaptation==
Composer Missy Mazzoli and librettist Royce Vavrek have been commissioned by Opera Philadelphia to adapt the work into a chamber opera.  The first aria, "His Name is Jan," was presented at the Brooklyn Academy of Music as part of the 2013 Next Wave Festival. 

 
==References==
 

==Bibliography==
*  
* Ebbe Villadsen: Danish Erotic Film Classics (2005)
* Georg Tiefenbach: Drama und Regie (Writing and Directing): Lars von Triers Breaking the Waves, Dancer in the Dark, Dogville. Würzburg: Königshausen & Neumann 2010. ISBN 978-3-8260-4096-2.

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 