Two (1964 film)
{{Infobox film
| name           = Two
| image          = Two (short film, 1964) title card.JPG
| image_size     = 235px
| border         = 1px
| alt            = Two (short film, 1964) title card
| caption        = Title card of the film
| director       = Satyajit Ray
| producer       = Esso World Theater
| writer         = 
| screenplay     = Satyajit Ray
| narrator       = 
| starring       = Ravi Kiran, Street Kid
| music          = Satyajit Ray
| cinematography = Soumendu Roy
| editing        = Dulal Dutta
| studio         = 
| distributor    = Esso World Theater (in public domain)
| released       =  
| runtime        = 15 minutes
| country        = India
| language       = No dialogue
}} Ballet troupe Bengali cinema, Bengali setting, however Ray being an admirer of silent film decided to make a film without any dialogue as a tribute to the genre.   

The short film shows an encounter between a child of a rich family and a street child, through rich kids window. The film is made without any dialogue and displays attempts of One-upmanship between kids in their successive display of their toys. The film portrays the child-rivalry with the help of world of noise and that of music.  The film is among less known films of Ray but experts rated the film as one of Rays best. It is often regarded as a prelude to another Ray film, Goopy Gyne Bagha Byne (1969). Made during the Vietnam War, experts believe that the film makes "a strong anti-war statement"    as it ends with street kids flute sound overpowering sound of expensive toys.

Academy Film Archive, part of the Academy Foundation, took an initiative to restore Satyajit Rays films and could successfully restore 19 Ray films but Two is yet to be restored as its present location of elements of the film are unknown.  The films original script was included in a book named Original English Film Scripts Satyajit Ray, put together by Rays son Sandip Ray. 

==Plot==
The film begins with a rich kid (Ravi Kiran) shown to be playing with his toys and enjoying the bottled soft drink. While playing, he overhears a sound and curiously overlooks the window to see a slum kid playing a flute. In order to show his toys, rich kid takes out his toy trumpet to make loud sound. Street kid then goes back to his hut and starts playing a small drum to which rich kid shows his battery-powered drum. When street kid comes out home-made mask and bow and arrow, rich kid wears various masks including one of a native villager and  cowboy. Disappointed street kid returns to his hut and rich kid also goes back to play with his toys.

While playing, rich kid notices a kite flying in the sky, through the window. Curious to know who is flying the kite, rich kid readily runs to the window to see slum kid holding kite string, Manja (kite)|Manja. Furious to see street kid happily flying the kite higher, rich kid gets his slingshot to attack the kite. Unable to do so, he then gets his toy-rifle and successfully shoots down the kite. Street kid then returns to his hut with a torn kite and gives up on becoming a friend with rich kid.

Rich kid then comes back to his toys and starts playing all of them, with each making its own sound. The film ends when rich kid could still hear the flute sound through the window in spite of loud noise of his toys and ponders over his deeds.

==Credits==

===Cast===
* Ravi Kiran as rich kid
* Un-credited street kid

===Crew===
* Editor: Dulal Dutta
* Art direction: Bansi Chandragupta
* Sound designer: Sujit Sarkar
* Cinematographer: Soumendu Roy
* Music direction: Satyajit Ray

==Restoration== honorary Academy Award in 1992 for Lifetime Achievement,  Academy Film Archive, part of the Academy Foundation, which mainly works with objectives as "preservation, restoration, documentation, exhibition and study of motion pictures" took an initiative to restore and preserve Rays films.  Josef Lindner was appointed as a preservation officer and Academy could successfully restore 19 Ray titles so far. However, Academy is yet to restore Two as present location of elements of the film are unknown and believed that it could be with Standard Oil in the USA.   

==In media== Rabindranath Tagore, Sukumar Ray on May 7, 2009.  The films original script was included in a book named Original English Film Scripts Satyajit Ray, put together by Rays son Sandip Ray along with an ex-CEO of Ray Society, Aditinath Sarkar, which also included original scripts of Rays other films.    

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 