High Steppers
{{infobox film
| name           = High Steppers
| imagesize      =
| image	=	
| caption        = 
| director       = Edwin Carewe
| producer       = Edwin Carewe
| writer         = Finis Fox 
| based on       =  
| starring       = Lloyd Hughes Mary Astor Dolores del Río
| music          = 
| cinematography = Robert Kurrle
| editing        =
| distributor    = Edwin Carewe Productions
| released       =  
| runtime        = 70 mins.
| country        = United States
| language       = Silent film (English intertitles)
| gross = 
}}
 silent drama film produced and distributed by Edwin Carewe Productions and directed by Edwin Carewe. The film is based on the novel Heirs apparent by Philip Gibbs.

== Plot ==
Julian Perryam (Lloyd Hughes) gets thrown out of Oxford University and returns to the family estate outside of London. He discovers that his sister and his mother are caught up in the "jazz" life and their father, whos the editor of a tabloid scandal rag, is too busy to notice. He also discovers that his sister is in love with the scoundrel son of his fathers publisher, Victor Buckland. Learning that Buckland is actually an embezzler, Julian gets a job as a reporter on a muckraking publication and sets out to expose Buckland. 

== Cast ==
* Lloyd Hughes as Julian Perryam
* Mary Astor as Audrey Nye
* Dolores del Río as Evelyn Iffield
* Rita Carewe as Janette Perryam
* John T. Murray as Cyril Buckland
* Edwards Davis	 as Victor Buckland

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 