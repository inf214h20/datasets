Three Girls About Town
{{Infobox Film
| name           = Three Girls About Town
| image          = ThreeGirlsAboutTownMoviePoster.jpg
| caption        = Theatrical Poster
| writer         = Richard Carroll
| starring       = Joan Blondell Binnie Barnes Janet Blair
| director       = Leigh Jason
| producer       = Samuel Bischoff
| music          = 
| cinematography = Franz Planer Charles Nelson
| distributor    = Columbia Pictures
| released       = October 23, 1941
| runtime        = 75 minutes
| country        = United States English
| budget         = 
|}} 1941 Columbia Columbia comedy film directed by Leigh Jason.  The story is written by Richard Carroll and stars Joan Blondell, Binnie Barnes and Janet Blair (in her film debut).

==Plot==
The Merchants Hotel is hosting a convention for morticians and a mediation meeting between aircraft manufacturers and their workers. This makes hotel manager Puddle worry about an article in the papers criticizing the hotels policy on employing hostesses.

The article upsets more people on the staff, and Tommy Hopkins, who is in charge of press information at the hotel is scolded by his fiancé Hope Banner, who is also head hostess. Tommy has tried to get Hope to get another job with "regular" hours, but he hasnt spread this information to the press.

Hope works the long and uncomfortable hours to send her younger sister, Charity, to a private school. However, right after Hope and Tommys argument, Charity announces that she will quit school to be a hostess like her older sister. Hope is shocked and tries to convince her otherwise, together with another hostess, Faith.

Meanwhile, a dead body is found in another part of the hotel in one of the rooms, by housekeeper Maggie and some of the maids. After telling Puddle, he decides to dump the body in an alley for someone else to find. As they try to do so, they are interrupted by the chief of police and a group of women outraged by the article about the legality of the hiring of hostesses in the hotel. Hope manages to fend them off by telling them there is no illegal hiring.

Tommy realizes that the dead body is in fact the labor mediator who has ye to show up at the mediation meeting. He phones a newspaper and tell them about this. When Hope and Faith finds out, they move the body to not ruin the hotels reputation. But when the chief of police comes near, they have to hide it in a guests room. Unfortunately the body is discovered by the guest and heruns off to call the police.

Meanwhile, hope and Faith retrieve the body and hide it on a laundry cart. Tommy takes the body off the cart and uses it as an extra player in a game of cards, calling it "sleepy Joe". The corpse wins the forst round, and the others call it a "lucky stiff". Hope and Faith find the corpse at the card game and puts on a show, scolding the stiff, claiming it is Hopes lost husband. They manage to carry "Joe" out of there, but again bumps into the chief of police in the hallway.

Desperate to hide the body, they dump it in a coffin in mortician Josephus Wiegels room. The coffin is then hauled away into the morticians convention room. The editor who Tommy told about the missing mediator, Fred Chambers, arrives to the hotel press room, and finds out the body is now lost. He is furious, and tells Tommy that the story is already printed and that they have to find the body.

Tommy asks Charity to tell him where the body is, and she tells him it is on display in the convention room. After talking to Tommy, she gets an evil idea in her head, and tells Hope that Tommy just proposed to her.

When the newspapers arrive in the morning, and when the chief of police reads the story, he decides to find Tommy. Tommy tries to hide at the mediation meeting, posing as the dead mediator himself. He talks for a long while of patriotism and manages to prevent the aircraft workers from going on a strike.

Soon the chief of police find him and arrest him. Tommy loses his job and Hope regrets that she put the corpse in the coffin in the first place. She decides to make things right by bringing the corpse back, and involves Puddle in the plan. They all get arrested for wrongful handling of the dead body, but Tommy is promoted when the editor sees the corpse.

When the body is carried out through the hotel lobby, a passing man recognizes the body and with a clap of his hands brings the man back to life. It turns out he had just paralyzed the man with hypnosis. Everyone arrested is released again, and Tommy announces he is to get married. Hope believes it is Charity he intends to tie the knot with, but Tommy proposes to Hope. Charity is spanked for her mean prank, by everyone involved. 

==Cast==
* Joan Blondell &ndash; Hope Banner
* Binnie Barnes &ndash; Faith Banner
* Janet Blair &ndash; Charity Banner
* John Howard &ndash; Tommy Hopkins
* Robert Benchley &ndash; Wilburforce Puddle, hotel manager
* Hugh OConnell &ndash; Chief of police
* Frank McGlynn Sr. &ndash; Josephus Wiegel, senior mortician
* Eric Blore &ndash; Charlemagne, looking for Charlie Paul Harvey &ndash; Fred Chambers, editor Una OConnor &ndash; Maggie OCallahan, scrubwoman
* Almira Sessions &ndash; Tessie Conarchy, scrubwoman
* Dorothy Vaughan &ndash; Mrs. McDougall, scrubwoman Charles Lane &ndash; Mortician
* Bess Flowers &ndash; Morticians wife

==External links==
* 

==References==
 

 
 
 
 
 
 
 
 