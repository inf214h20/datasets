Kill Kill Faster Faster
{{Infobox film
| name           = Kill Kill Faster Faster
| image          = Kill Kill Faster Faster.jpg
| image size     = 
| alt            = 
| caption        = DVD cover
| director       = Gareth Maxwell Roberts
| producer       = Gil Bellows Carlo Dusi John Pantages Gareth Maxwell Roberts
| writer         = 
| screenplay     = Gareth Maxwell Roberts Joel Rose
| story          = 
| based on       =  
| narrator       = 
| starring       = Gil Bellows Lisa Ray Esai Morales Shaun Parkes  
| music          = Mike Benn
| cinematography = Ruzbeh Babol
| editing        = Raimondo Aiello Rachel Tunnard
| studio         = Aria Films
| distributor    = Galloping Films Kaleidoscope (UK)
| released       =  
| runtime        = 97 minutes
| country        = United States Netherlands
| language       = 
| budget         = 
| gross          =
}}
 London Independent Film Festival.  It had its DVD release on July 28, 2009, which was followed by screening at the Helsinki International Film Festival September 18, 2009.

==Background==
The project is set in New York City and was filmed there and in Rotterdam, the Netherlands. The film follows the life of a former drug addict and murderer who is released from prison.  Filming was completed July 2007.  The film received attention due to former Bollywood actress Lisa Rays several aesthetically shot nude scenes.      

==Plot== arm candy.  Learning she had been a fan of his for some time, the two begin a torrid affair.  Joe then searches for a way to reconcile the crime for which he had never forgiven himself.

==Partial cast==
* Gil Bellows as Joey
* Lisa Ray as Fleur
* Esai Morales as Markie
* Shaun Parkes as Clinique
* Doña Croll as Birdie
* Moneca Delain as Kimba
* Stephen Lord as Jarvis
* Jane Peachey as Serena
* Saskia Troccoli as Sasha
* Viv Weatherall as Nicky Roman
* Liza Sips as Lolita
* Godwin Logye as Dobbins
* Jason Anthony Griffith as Cassius(uncredited)

==Recognition==

===Critical response===
Independent Film Reviews wrote that the film "had some real mixed reviews and not a lot of publicity."  The anonymous reviewer noted that the films use of a prison theme is one that has been used many times.  He noted that while Shaun Parkes role as Joeys cell mate was cliché, it was "enjoyable to watch", and also noted that Gil Bellows performance was "very reminiscent of Mickey Rourkes character in Bullet (1996 film)|Bullet".  He granted that there were some scenes that provided nice, artistic visuals, but that they could not overcome drab writing "trying to be philosophical but coming off as dribble and out of place". The reviewer offered that if a viewer did not have high expectations, it might be worth renting, but wrote "overall the theme is predictable and the story pretty much tells its ending all throughout which gives the viewer a bit of a letdown." 

TV Kult makes note that the film uses the device of frequent flashbacks to maintain tension and provide context without a need to overly develop the backstory, and through the performances of the supporting cast their characters are portrayed convincingly.  The reviewer noted that while the scenes depicting Joes 18 years of incarceration included sex and violence, they were contextually well integrated into the film without appearing overdone. While writing that the film was not great cinema, he noted that its strength lay with a story that holds a viewers interest. In conclusion, it is " in durchaus sehenswerter Film für alle Thrillerfans, die mehr Wert auf eine gute Story als auf Effekten und großen Namen legen," or "a very watchable film for all thriller fans who attach more importance to a good story than on effects and big names."   

===Awards and nominations===
* 2008, won Best International Feature at London Independent Film Festival  
* 2008, won Best Editing in a HD Feature Film at HD Fest   
* 2008, 2nd place, Best High-Definition Feature HD Fest 
* 2008, won Best Independent Feature Film at Charity Erotic Awards. 

==DVD release==
The DVD extras include the original trailer and a trailer show, a photo gallery with pictures from the movie and the filming, and a one-hour interview with the director Gareth Maxwell Roberts. 

== References ==
 

== External links==
*   at the Internet Movie Database

 
 
 
 
 
 
 
 
 
 
 
 
 
 