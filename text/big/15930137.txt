Nobody Will Speak of Us When We're Dead
 
{{Infobox film
| name           = Nadie hablará de nosotras cuando hayamos muerto
| image          = NobodyWillSpeakOfUsWhenWereDead.jpg
| image_size     =
| caption        =
| director       = Agustín Díaz Yanes
| producer       = Edmundo Gil
| writer         = Agustín Díaz Yanes Alejandro Pose
| narrator       =
| starring       = Victoria Abril Pilar Bardem Federico Luppi Bruno Bichir Demián Bichir
| music          = Bernardo Bonezzi
| cinematography = Paco Femenia
| distributor    =
| released       =  
| runtime        = 104 minutes
| country        = Spain Spanish
| budget         =
}}
 1995 Spain|Spanish noir drama film written and directed by Agustín Díaz Yanes.

==Cast==
*Gloria Duque - Victoria Abril
*Doña Julia - Pilar Bardem
*Eduardo - Federico Luppi
*Doña Amelia - Ana Ofelia Murguía
*Oswaldo - Daniel Giménez Cacho
*Juan - Ángel Alcázar
*Ramiro - Saturnino García
*María Luisa - Marta Aura
*Evaristo - Guillermo Gil

==Awards==
* 8 Goya Awards: including Best Film, Best Actress (Victoria Abril), Best Supporting Actress (Pilar Bardem) and Best New Director
* San Sebastian Film Festival: Best Actress (Victoria Abril) and Special Prize of the Jury
* 2 Fotogramas de Plata
* 3 Premio Ondas

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 