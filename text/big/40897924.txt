A Second Knock at the Door
 
A Second Knock at the Door is a documentary  on friendly fire in Iraq and Afghanistan.  
The film follows military families after they are told their family member died in a "fratricide" incident. 
All the families profiled in the film only learned their family member was killed by a comrade, not an enemy, months after they first learned of their death. 

Director Christopher Grimes has described being inspired by the official coverup that former sport star Pat Tillman was killed by his comrades. 

The film was screened at the Flyway Film Festival in 2011, and the East Lansing Film Festival in 2012.  

==Notes==
 
 
{{cite news 
| url         = http://asp.militarygear.com/2012/03/28/a-second-knock-at-the-door/
| title       = A Second Knock at the Door
| publisher   = A Soldiers Perspective
| author      = 
| date        = 2012-03-28
| page        = 
| location    = 
| isbn        = 
| archiveurl  = https://web.archive.org/web/20140212073617/http://asp.militarygear.com/2012/03/28/a-second-knock-at-the-door/
| archivedate = 2014-02-13
| accessdate  = 2014-02-12
| deadurl     = No 
| quote       = For Christopher E. Grimes, then a graduate student planning his Master’s thesis in Public Policy at Northwestern University, the case provoked the question: how many other cases like Pat Tillman’s are there? The results of Grimes’ thesis research is the award-winning documentary feature A SECOND KNOCK AT THE DOOR, which shares the heart-breaking stories of four families who have lost loved ones due to friendly fire. 
}}
 
 
{{cite news 
| url         = http://qctimes.com/entertainment/columnists/linda-cook/documentaries-to-help-pass-a-cabin-fever-weekend/article_1990fa8c-8b25-5c19-8777-c0181b8d0029.html
| title       = 33 documentaries to help pass a cabin fever weekend
| publisher   = Quad City Times
| author      = Linda Cook
| date        = 2014-02-01
| page        = 
| location    = 
| isbn        = 
| archiveurl  = http://www.webcitation.org/query?url=http%3A%2F%2Fqctimes.com%2Fentertainment%2Fcolumnists%2Flinda-cook%2Fdocumentaries-to-help-pass-a-cabin-fever-weekend%2Farticle_1990fa8c-8b25-5c19-8777-c0181b8d0029.html&date=2014-02-12
| archivedate = 2014-02-12
| accessdate  = 2014-02-12
| deadurl     = No 
| quote       = Loved ones of those in the military who lost their lives to “friendly fire” are interviewed about what really happened to cause the deaths.
}}
 
 
{{cite news 
| url         = http://www.pretentiousfilm.com/2012/02/showcase-spotlight-second-knock-at-door.html
| title       =  Showcase Spotlight: "A Second Knock at the Door," with Q & A, Screening Saturday, 2/18 
| publisher   = Pretentious Film Society
| author      = 
| date        = 2012-02-11
| page        = 
| location    = 
| isbn        = 
| archiveurl  = https://web.archive.org/web/20121204175625/http://www.pretentiousfilm.com/2012/02/showcase-spotlight-second-knock-at-door.html
| archivedate = 2012-12-04
| accessdate  = 2014-02-12
| deadurl     = No 
| quote       = When I saw A SECOND KNOCK AT THE DOOR, at last years Flyway Film Festival, I was completely knocked off my feet.  This is a powerful documentary which focuses on the issue of American troop fatalities caused by "friendly fire," which is the inadvertent firing on ones own soldiers while engaging with an enemy.  
}}
 
 
{{cite news 
| url         = http://live.hollywoodjesus.com/?p=9892
| title       = Friendly Fire, Unfriendly Lies
| publisher   = Hollywood Jesus
| author      = Brian Dedmon
| date        = 2012-03-29
| page        = 
| location    = 
| isbn        = 
| archiveurl  = https://web.archive.org/web/20120404131501/http://live.hollywoodjesus.com/?p=9892
| archivedate = 2012-04-04
| accessdate  = 2014-02-12
| deadurl     = No 
| quote       = The title, A Second Knock At The Door, is a tell-tale application of the emotions involved in these family’s experiences. After being told that their soldier was killed by mortar fire from an enemy or a car accident they discover, most often months later, that their kin was killed by friendly fire. In some cases the army went to great lengths to cover up the exact details of how the   was killed. It is alarming to consider that the U.S. Army would lie about a soldier’s death to save face, but they’re only human.
}}
 
 
{{cite news 
| url         = http://www.lansingcitypulse.com/lansing/article-6572-east-lansing-film-festival-special.html
| title       = East Lansing Film Festival Special 
| publisher   = East Lansing City Pulse
| author      = 
| date        = 2012-05-31
| page        = 
| location    = 
| isbn        = 
| archiveurl  = https://web.archive.org/web/20130419224204/http://www.lansingcitypulse.com/lansing/article-6572-east-lansing-film-festival-special.html
| archivedate = 2013-04-19
| accessdate  = 2014-02-12
| deadurl     = No 
| quote       = To the government, friendly fire incidents are embarrassing blunders that needn’t be dwelled upon; to the families, the fog and secrecy surrounding their loved ones’ deaths make the pain of loss even worse.
}}
 
 

==External links==
* 

 
 
 
 
 


 