Looking for Angelina
{{Infobox film
| name           = Looking for Angelina
| image          = Looking for Angelina (2005 film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Sergio Navarretta
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2005 
| runtime        = 
| country        = Canada Italian
| budget         = 
| gross          = 
}}
 2005 Cinema Canadian drama film based on the murder case involving Angelina Napolitano.    Napolitano allegedly murdered her husband with an axe and was sentenced to be executed.   

In 2003,   and Frank Canino, took inspiration from Canino’s play "The Angelina Project". Platinum Image Film press release New Film About Italian Immigrant, March 13, 2006. Accessed June 2008 via     , accessed June 2008  Lina Giornofelice starred as Angelina, with Alvaro D’Antonio playing Pietro.   For authenticity, large parts of the film are in Neapolitan language|period-correct Italian with English subtitles.  
 English and 2009 under the title Buscando a Angelina.    It was shown at the Montreal World Film Festival, Cinéfest in Greater Sudbury|Sudbury, Quitus Italian Film Festival in Montreal, Shadows of the Mind Festival in Sault Ste Marie, the International Film Festival of India, Cimameriche Film Festival in Genoa and the Mumbai International Film Festival.   “In general,” said director Navaretta, “audiences have responded to the film on an emotional level, empathizing with the journey of  .”  "Looking For Angelina" won three awards: A Special Recognition at the Cimameriche Film Festival and Best Feature (Drama) and Quitus Award of Distinction at the Quitus Film Festival in Montreal. 

==References==
 

 
 
 
 
 