The Student Teachers
{{Infobox film
| name           = The Student Teachers
| image          = The_Student_Teachers.jpg
| caption        =
| director       = Jonathan Kaplan
| producer       = Julie Corman
| writer         = Jonathan Kaplan Danny Opatoshu
| narrator       =
| starring       = Susan Damante Chuck Norris James Millhollin
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 90 min.
| country        = United States
| language       = English
| studio         = New World Pictures
| budget         = under $100,000 
| gross          = $1,078,000 (US/ Canada rentals) 
}}
The Student Teachers is a 1973 film directed by Jonathan Kaplan. It was inspired by the "nurse" cycle of pictures starting with The Student Nurses (1970). Roger Corman says it was one of the best of the cycle. 

==Plot==
Three new high school teachers use unconventional methods to get through to their students. Rachel teaches after-school sex education; Tracey gets involved with nude photography; Jody recruits a former drop out to help with a half-way house and gets involved with a drug ring.

==Production==
A draft was written by Kaplan and Danny Opatoshu, but Corman asked for it to be rewritten.

It was shot in 15 days for under $100,000, including three days shooting at the Paramount Ranch. Corman removed a number of jokes from the final chase sequence so it was played straighter. 

===Casting===
The lead was written for Patti Byrne who was in Kaplans earlier   

==References==
 

==External links==
* 

 
 
 

 
 
 
 
 
 
 
 