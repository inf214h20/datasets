Son of the Guardsman
{{Infobox film
| name           = Son of the Guardsman
| image          = Poster of the movie Son of the Guardsman.jpg
| image_size     = 
| caption        = 
| director       = Derwin Abrahams
| producer       = Sam Katzman Melville De Lay (associate)
| writer         = Lewis Clay Harry L. Fraser George H. Plympton
| narrator       = Knox Manning Robert Shaw Charles King
| music          = 
| cinematography = Ira H. Morgan Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Columbia Serial film serial.  It was the 31st of the 57 serials produced by that studio.

Son of the Guardsman is a rare serial with a period setting, in this case 12th century England.  The serial is largely based on the Robin Hood legends, to the extent of including outlaws from Sherwood Forest, but it does not include or reference Robin Hood himself.
 Robert Shaw Charles King as his villainous uncle Sir Edgar Bullard.

==Plot== usurped by the regent Lord Hampton.

==Cast== Robert Shaw as David Trent, nobleman turned outlaw
* Daun Kennedy as Lady Louise Markham, daughter of Lord Markham
* Robert Buzz Henry as "Roger Mowbry", really Prince Richard in disguise
* Jim Diehl as Allan Hawk, leader of the Sherwood Forest outlaws
* Hugh Prosser as Red Robert
* Leonard Penn as Mark Crowell
* Jock Mahoney as Captain Kenley Charles King as Sir Edgar Bullard, David Trents evil uncle
* John Merton as Lord Hampton, the evil regent

==Production==
Son of the Guardsman is based on the Robin Hood legends, although it does not include Robin Hood, just the period and Sherwood Forest setting. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 329
 | chapter = 13. The Classics "You Say What Dost Thou Mean By That? and Push Him Off the Cliff" Amortising the costs of all the productions involved. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 29
 | chapter = 3. The Six Faces of Adventure
 }}  Costume drama serials were rare productions for any producer.  The serials subitle was "Gallant Fighter of the Greenwood."

===Stunts===
* Jock Mahoney

==Chapter titles==
# Outlaws of Sherwood Forest
# Perils of the Forest
# Blazing Barrier
# The Siege of Bullard Hall
# A Dagger in the Dark
# A Fight for Freedom
# Trial by Torture
# Mark Crowells Treachery
# Crushed to Earth
# A Throne at Stake
# Double Danger
# The Secret of the Treasure
# Into the Depths
# The Lost Heritage
# Free Men Triumph
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 244
 | chapter = Filmography
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*  
*  

 
{{succession box  Columbia Serial Serial 
| before=Chick Carter, Detective (1946)
| years=Son of the Guardsman (1946) Jack Armstrong (1947)}}
 

 
 

 
 
 
 
 
 
 
 