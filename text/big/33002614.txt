Aazmayish
 
{{Infobox film
| name           =Aazmayish
| image          = Aazmayishfilm.jpg
| image_size     = 
| caption        = Promotional Poster Sachin
| producer       = Mohan Kumar
| writer         =
| narrator       = 
| starring       = Dharmendra Rohit Kumar Anjali Jathar Prem chopra Ashok Saraf Mohnish Bahl Prem Chopra
| music          = Anand-Milind
| lyrics         = Anand Bakshi
| cinematography = 
| editing        = 
| distributor    = 
| released       = 25 April 1995
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Aazmayish is a 1995 Bollywood film starring Dharmendra, Anjali Jathar and producer Mohan Kumars son Rohit Kumar in his debut.  It is a story of a father and son  directed by actor Sachin (actor)|Sachin.


==Plot==

It is Rohit Kumars first film, who plays a role of Dharmendras son. Anjali Jathar belongs to a rich family and Prem Chopra plays a role of her father.

==Summary==

Prem Chopra is in a negative role who has his own construction company. Dharmendra is a worker in Prem Chopras company.
Dharmendra and all the labors working with the  company  stay in slums. Prem chopras father had promised to build cement houses for all the workers but his life ends before he could fulfill his promise.When Prem Chopra takes over the control of the company after his father, Dharmendra advocates the demand to get cement houses for all the workers on behalf of the entire labor community. Prem Chopra, a very business minded person and hence Dharmendras demanding nature creates an enmity between Dharmendra and Prem chopra. The story takes a twist when Rohit Kumar and Anjali Jathar fall in love and their relationship is opposed by Prem Chopra while it is supported by Dharmendra.

==Cast==

*Dharmendra
*Rohit Kumar
*Anjali Jathar
*Prem Chopra
*Ashok Saraf
*Mohnish Bahl

==Soundtrack==

Mohan Kumar opted for Anand-Milind over regulars Laxmikant-Pyarelal to compose the music of the film. He however retained lyricist Anand Bakshi. Although the film failed at the box-office, one song Choodiyaan Banti Hain, rendered by the then struggling singer Sonu Nigam and Bela Sulakhe, was popular. Sonu Nigam sang all, but one song in this film, his first major break. Director Sachin is said to have recommended Sonu Nigam to Anand-Milind.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "O My Daddy"
| Kumar Sanu, Sonu Nigam
|-
| 2
| "Chooiyaan Banti Hain"
| Sonu Nigam, Bela Sulakhe
|-
| 3
| "Mera Dil Kho Gaya"
| Sonu Nigam, Alka Yagnik
|-
| 4
| "Yaar Mat Ja"
| Sonu Nigam, Alka Yagnik
|-
| 5
| "Yeh Roti Yeh Dal"
| Abhijeet
|}

==References==
  

==External links==
*  

 
 
 
 
 