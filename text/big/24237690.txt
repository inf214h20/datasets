Bran Nue Dae (film)
 
 
{{Infobox film
| name = Bran Nue Dae
| image = Bran Nue Dae poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Rachel Perkins
| producer = {{Plainlist|
* Robyn Kershaw
* Graeme Isaac}}
| screenplay = {{Plainlist|
* Reg Cribb
* Rachel Perkins}}
| based on =  
| starring = {{Plainlist| 
* Rocky McKenzie
* Jessica Mauboy
* Ernie Dingo
* Missy Higgins
* Geoffrey Rush}}
| music = Cezary Skubiszewski
| cinematography = Andrew Lesnie
| editing = Rochelle Oshlack
| production companies = {{Plainlist|
* Film Victoria
* Mayfan
* Omnilab Media Robyn Kershaw Productions
* Screen Australia}} Roadshow Films Freestyle Releasing 
| released =  
| runtime = 88 minutes 
| country = Australia
| language = English
| budget =   Production information  , ScreenDaily.com. Retrieved 26 September 2009. 
| gross =     – By Brendan Swift – if.com – Retrieved 8 January 2012. 
}} musical comedy-drama film directed by Rachel Perkins and written by Perkins and Reg Cribb. A feature film adaptation of the 1990 stage musical Bran Nue Dae by Jimmy Chi, the film tells the story of the coming of age of an Aboriginal Australian teenager on a road trip in the late 1960s.

==Plot== Broome in Perth to Kombi van, hoping that the two hippies inside will help him. Not realising how far it will be to Broome, the hippies, Slippery the German (Tom Budge) and Annie (Missy Higgins), his girlfriend, agree to drive them.
 roadhouse where pointing a bone, and the van promptly breaks down.

Willie then gets a ride with a passing truck carrying the members of a football team. They end up in Port Hedland where he meets flirty Roxanne (Deborah Mailman), who takes him to the condom tree and offers to show him a good time, but her boyfriend turns up and a fight ensues. Willie is rescued by Tadpole, who says that all young men end up there at some point. The next morning, they are driving along a desert road when a hung-over Roxanne emerges from the back seat, startling everyone. While smoking some pot, they are discovered by police and arrested, despite Annies attempts at stopping the police from arresting them. At the police station, Slippery reveals that his real name is Wolfgang Benedictus. The police then put them in a jail cell for a night.
 temperance march, which invites everyone to the beach to testify. Willie tells Rosie he loves her, and they kiss. At the beach, Willies mother reveals that she had a son to another man, who turns out to be Father Benedictus. Wolfgang is their son. Tadpole is spotted by Willies mother, and she tells Willie that he is Tadpoles son.

==Cast== Perth to return to his home in Broome, Western Australia|Broome. 
*  . Retrieved 26 September 2009. 
*  . Retrieved 26 September 2009. 
* Missy Higgins as Annie, Slipperys spiritual girlfriend who is travelling with him to Perth.  This is Higgins acting debut.   Lets Sing It. Retrieved 26 September 2009. 
*  . Retrieved 26 September 2009.  Kombi van. 
* Deborah Mailman as Roxanne 
* Ningali Lawford as Theresa, Willies mother. Ningali is one of the cast members from the original 1989 stage production of Bran Nue Dae. 
* Stephen "Baamba" Albert as Pastor Flaggon. Stephen is also one of the cast members from the original 1989 stage production of Bran Nue Dae. 
* Dan Sultan as Lester, the local band leader who finds himself in love with Rosie. 
* Magda Szubanski as Roadhouse Betty, the frisky owner of a Roadhouse.   Film Ink 

==Release==
Bran Nue Dae  . Retrieved 25 September 2009.  The film made its international debut at the   in 2010.    . Retrieved 7 December 2009.  In the lead up to the premiere on the day, stars of the film Mauboy, Sultan, Higgins, and Dingo put on a public performance with the Kuckles band in Broomes Chinatown. 

===Box office===
The film was theatrically released in Australia on 14 January 2010   at Cinematic Intelligence Agency   and had an opening weekend rank of #6, averaging $6,977 at 231 screens for a gross of $1.6 million,  $3.7 million in its first two weeks  - Retrieved 8 January 2012.  and eventually grossing more than $7.5 million.  The film "... has since become one of the Top 50 Australian films of all time at the local box office." 

===Critical reception===
The film received generally mixed reviews. Review aggregator website Rotten Tomatoes reports that 57% of critics have given the film a positive review based on 60 reviews, with an average score of 6/10. The critical consensus is: "Its original and high-spirited, but Bran Nue Dae is also uneven and sometimes overly kitschy." 

Dennis Harvey of  . Retrieved 25 September 2009.   . Retrieved 25 September 2009.   . Retrieved 25 September 2009.  for Rush who "brings an idiosyncratic physical energy and an extravagant German accent to the role" and newcomer Jessica Mauboy brought "sweetness and confidence to the role of Rosie". 

===Accolades===
The following is a list of awards that Bran Nue Dae or the cast have been nominated for or won:

;Wins
*  . Retrieved 25 September 2009. 
** Audience award for Best Feature

;Nominations
* 2009 Toronto International Film Festival   tiff.net. Retrieved 26 September 2009. 
** Peoples Choice Award

==Soundtrack==
{{Infobox album
| Name        = Bran Nue Dae-Official Soundtrack
| Type        = soundtrack
| Artist      = Various Artists
| Cover       = BranNueDaeSoundtrack.jpg
| Released    =  
| Recorded    = Pop Rock Rock and Roll
| Length      = 58:25
| Label       = Sony Music Australia
| Producer    = David Bridie Cezary Skubiszewski
| Chronology = 
| Last album  =
| This album  =
| Next album  =
| Misc        = {{Extra album cover
  | Upper caption =
  | Type =
  | Cover =
  | Lower caption =
 }}
}}
The official soundtrack was released on 15 January 2010.  , Retrieved on 24 January 2010. 
{{Track listing
| extra_column    = Performer(s)
| all_writing     = Jimmy Chi, Kuckles, and Cezary Skubiszewski
| title1          = Bran Nue Dae
| extra1          = Dan Sultan
| length1         = 2:06
| title2          = All The Way Jesus
| extra2          = Jessica Mauboy
| length2         = 2:34
| title3          = Seeds That You Might Sow
| extra3          = Dan Sultan
| length3         = 2:19
| title4          = Feel Like Going Back Home
| extra4          = Ernie Dingo & Missy Higgins
| length4         = 2:19
| title5          = Light a Light
| extra5          = Jessica Mauboy & Brendon Boney
| length5         = 2:46
| title6          = Nothing I Would Rather Be
| extra6          = Bran Nue Dae Cast
| length6         = 1:55
| title7          = Nyul Nyul Girl
| extra7          = Dan Sultan
| length7         = 2:42
| title8          = Broome Love Theme
| extra8          = Bran Nue Dae Gypsy Orchestra
| length8         = 2:06
| title9          = Long Way Away from My Country
| extra9          = Ernie Dingo
| length9         = 2:34
| title10         = Is You Mah Baby
| extra10         = Ernie Dingo
| length10        = 2:37
| title11         = Six White Boomers
| extra11         = Rolf Harris
| length11        = 3:22
| title12         = Zorbas Dance
| extra12         = David Bridie
| length12        = 2:12
| title13         = Afterglow
| extra13         = Missy Higgins
| length13        = 3:13
| title14         = Listen to the News
| extra14         = Ernie Dingo
| length14        = 4:43
| title15         = Black Girl
| extra15         = Dan Sultan
| length15        = 4:10
| title16         = Stand by Your Man
| extra16         = Jessica Mauboy
| length16        = 2:56
| title17         = Nothing I Would Rather Be
| extra17         = Brendon Boney and Geoffrey Rush
| length17        = 1:52
| title18         = Road Movie Medley
| extra18         = Bran Nue Dae Gypsy Orchestra
| length18        = 1:54
| title19         = Child of Glory
| extra19         = Bob Faggetter
| length19        = 1:58
| title20         = Going Back Home
| extra20         = Stephen Pigram
| length20        = 3:42
| title21         = Bran Nue Dae (Millya Rumarra Recording)
| extra21         = Jimmy Chi
| length21        = 4:25
}}

;Chart performance
{| class="wikitable"
|-
!Chart (2010)
!Peak position
|- ARIA Charts|Australian ARIA Albums Chart  , Retrieved on 24 January 2010.  29
|}

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 