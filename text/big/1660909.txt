Operation Dumbo Drop
{{Infobox film
| name           = Operation Dumbo Drop
| image          = Operation dumbo drop.jpg
| caption        = Theatrical release poster
| director       = Simon Wincer
| producer       = Diane Nabatoff David Madden
| story          = Jim Morris
| screenplay     = Gene Quintano Jim Kouf
| starring       = Danny Glover Ray Liotta Denis Leary Doug E. Doug Corin Nemec David Newman
| cinematography = Russell Boyd
| editing        = O. Nicholas Brown
| studio         = Walt Disney Pictures Interscope Communications PolyGram Filmed Entertainment Buena Vista Pictures
| released       =  
| runtime        = 108 minutes
| language       = English Vietnamese
| budget         =
| gross          = $24,670,346   
}}
 war comedy Green Berets during the Vietnam War in 1968, who attempt to transport an elephant through jungle terrain to a local South Vietnamese village which in turn helps American forces monitor Viet Cong activity. Actors Denis Leary, Doug E. Doug and Corin Nemec also star in principal roles. A joint collective effort to commit to the films production was made by Interscope Communications and PolyGram Filmed Entertainment.  As a backdrop for Vietnam, primary shooting and photography took place in Thailand.

It was commercially distributed by Walt Disney Pictures theatrically, and by Buena Vista Home Entertainment for home media.

Operation Dumbo Drop premiered in theaters nationwide in the United States on July 28, 1995 grossing $24,670,346 in domestic ticket receipts. The film was a moderate financial success after its theatrical run, but was generally met with negative critical reviews and failed to garner any award nominations from mainstream motion picture organizations.

==Plot== Montagnard Vietnamese NVA know of their cooperation with the local villagers. In punishment, Brigadier Nguyen (Hoang Ly) of the NVA, at the disagreeance of his subordinate, Captain Quang (Vo Trung Anh), orders him to kill the villagers elephant, right before a spiritual festival. To aid the villagers, Cahill promises to replace the slain elephant before their upcoming ceremony.

At camp, Major Pederson (Marshall Bell), assigns Cahill and Doyle, with Doyle in command, to secure and deliver a new elephant to the villagers, as well as two soldiers, Specialist 4 Harvey Ashford (Doug E. Doug) and Specialist 5 Lawrence Farley (Corin Nemec). Cahill blackmails Chief Warrant Officer 3 Davis Poole (Denis Leary) in to helping as well. They purchase an elephant from a Vietnamese trader in a village. They also agree to take along the elephants handler, Linh (Dinh Thien Le), who has experience with verbal commands in guiding the elephant. Along the way, NVA soldiers attempt to halt their advancement with the elephant towards Dak Nhe. Following a failed air transport, the soldiers use a combination of methods to reach Pleiku Air Base before their final journey towards Dak Nhe.

At Pleiku Air Base, Major Pederson notifies the captains that the mission to deliver the elephant as a favor, has been cancelled. Pederson informs the soldiers that the enemy supply route has changed direction, and they no longer need the support of the local village. Against regulations, they commandeer a cargo aircraft with the elephant on board to deliver to the villagers as promised. The aircraft comes under enemy fire, forcing them, along with the elephant aboard a crate, parachute out early. The company land unharmed in and around the village, but Ashford, gets stuck in a tree and becomes separated from the rest. NVA forces suddenly appear to disrupt the operation, threatening to take the remaining soldiers hostage and kill the elephant. Ashford however, is able to free himself and create a diversion long enough to distract and incapacitate the NVA troops. They complete their original intended mission, and to the delight of the U.S. Army, capture high-ranking enemy combatants in the process.

==Cast==
 
{| cellpadding="0" cellspacing="0"
| Danny Glover
| &nbsp;... Captain Sam Cahill
|-
| Ray Liotta
| &nbsp;... Captain T.C. Doyle
|-
| Denis Leary
| &nbsp;... Chief Warrant Officer 3 David Poole
|-
| Doug E. Doug
| &nbsp;... Specialist 4 Harvey Ashford
|-
| Corin Nemec
| &nbsp;... Specialist 5 Lawrence Farley
|-
| Dinh Thien Le
| &nbsp;... Linh
|-
| James Hong
| &nbsp;... Y Bham
|-
| Tchéky Karyo
| &nbsp;... Goddard
|-
| Hoang Ly
| &nbsp;... Brigadier Nguyen
|-
| Vo Trung Anh
| &nbsp;... Captain Quang
|-
| Marshall Bell
| &nbsp;... Major Pederson
|- Tim Kelleher
| &nbsp;... Major Morris
|-
| Raymond Cruz
| &nbsp;... Staff Sergeant Adams
|- Tai
| &nbsp;... Bo Tat
|}
 

==Production==

===Development===
 
The premise of Operation Dumbo Drop is based on the true story relating to the cooperation of South Vietnamese villagers and the U.S. Army during the Vietnam War in the late 1960s.    The U.S. Army viewed many villages as having a strategic value due to their proximity to enemy weapons supply routes, such as the Ho Chi Minh trail.  Elephants found in villages were typically the primary source of farm labor. To appease hostile villagers, the U.S. offered elephants as a token of appreciation. According to actor Glover, one such operation took place specifically on April 4, 1968; but received fairly little coverage due to the death on the same day of a Vietnamese military leader and also, the assassination of Martin Luther King, Jr. in Memphis   The film is based on a story depicted by retired United States Army Major Jim Morris; who related his experiences surrounding the elephant air-droppings during the ongoing war.
 Tai was used for the part of Bo Tat. The female elephant was chosen for the part because of her calm demeanor and friendly disposition, which allowed her to be placid and relaxed during scenes with simulated gunfire.     Many depictions in the film such as the elephant being sedated and lying down or following that, moving her legs and standing up were actually accomplished with an animal trainer. 

===Set design and filming===
Principal filming took place primarily on location in Thailand.  Other filming locations included film studios in Los Angeles, California and Miami, Florida. 
 
  similar to one used during filming]]
Certain scenes where the elephant was shown aboard a marine boat actually had I-beams under the deck to support the animal. Additionally, ballast was added to the boat to keep it afloat.  One of the later scenes where the elephant was aboard the aircraft with gunfire and missiles being shot at it; was filmed in cuts with both fake elephants and a mechanical elephant being used in the jump. The real elephant was used only for close-ups. 

During the village food cart scene, a trainer as an extra, ran alongside the elephant telling her to keep moving. There was also another trainer in front of her encouraging her constant movement and ensuring that nothing got in her path. Wire was attached to crates and tables which were pulled over as the elephant ran by, making it appear as though she were knocking everything aside.  In a flashback scene, the character of Linh sees an elephant being shot, as he tells of how his parents were killed. The film crew accomplished this feat by instructing the trainer to tell the elephant to lie down, while then shooting the scene in slow motion.  To ensure the health of the elephant, her food and water, including her bathing water, was shipped from the U.S. to Thailand throughout the production. Furthermore, she was bathed in purified water every day. Young native Thai men were hired to hold umbrellas over the elephant when the cameras werent conducting shooting. And throughout filming, almost everything the elephant walked on was reinforced with timber and steel.  Other animals used as a background were local cattle, chickens, and goats. Twelve elephants were also used as extras in the jungle mountain scenes. 

According to Denis Leary, production on the film was terrible, claiming that the film took such a long time to shoot.  Leary said he, along with co-stars Ray Liotta and Danny Glover had done the movie for money to buy property that they all wanted.  Denis Leary has also (jokingly) stated that it wasnt him who played Poole in the film at all, and it was actually Willem Dafoe pretending to be him. Willem Dafoe denies this.

===Soundtrack=== David Newman; while being edited by Tom Villano.    The sound effects in the film were supervised by William Jacobs. The mixing of the sound elements was orchestrated by Doc Kane.  "When I See an Elephant Fly" is taken from the Disney animated film, Dumbo, with the music composed by Oliver Wallace.

==Reception==

===Critical response===
Among mainstream critics in the U.S., the film received mostly negative reviews. Rotten Tomatoes reported that 31% of 26 sampled critics gave the film a positive review, with an average score of 4.0 out of 10. 

Hal Hinson, writing in  , critic Chris Hicks reserved a mild compliment for some of the lead acting and directing saying, "Glover and Liotta play against each other pretty well, though there is none of the chemistry Glover has with Mel Gibson in the " . Retrieved 2010-08-18. 

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"I am not asking that "Operation Dumbo Drop" be hard-edged realism. Its not that kind of movie. Im not even very bothered by the scene where the elephants chute doesnt open, and Liotta goes into free-fall to save it. (No, he doesnt grab the elephant and open his own chute so they can ride down together.) What bothers me is that a chapter of our history is being rewritten and trivialized, as we win in the movie theaters a war that did not, in fact, turn out very well for us."
|-
|style="text-align: left;"|—Roger Ebert, writing in the Chicago Sun-Times Ebert, Roger (July 28, 1995).  . Chicago Sun-Times. Retrieved 2010-08-18. 
|}
 . Retrieved 2010-08-18.  In a mostly positive review, Joe Leydon writing in Variety (magazine)|Variety, felt Operation Dumbo Drop was "a well-crafted and entertaining pic with broad, cross-generational appeal." and that "Glover is well cast and establishes an effectively edgy give-and-take with Liotta." He also reserved praise for "Russell Boyds splendid cinematography and Rick Lazzarinis convincing animatronics."  Film critic Gene Siskel of the Chicago Tribune gave the film a thumbs down review gruffly calling it, "preposterous" and saying "Im not buying it all the way through." He also ridiculed the outdoor market scene as "the worlds most dangerous profession in the movies; selling fruit on a city street."  Lisa Schwarzbaum writing for Entertainment Weekly gave the film a positive "B Grade" rating and viewed the film as a "concept, supposedly based on a true story, is weird — this is what Vietnam movies have come to? But at least the Disney quadruped has the grace to say nothing, and Leary, still an interesting motormouth, knows enough not to smoke or swear when there are elephants around."  Peter Stack writing for the San Francisco Chronicle, saw the film as "not the terrible movie that its ubiquitous trailers would indicate. But it is an odd one," and that "Glover and Liotta, though not exactly great comic actors, play off each other with real spark, the two vying for command of the outlandish mission to deliver the elephant across 200 miles of impossible jungle terrain to the mountain village." 

 
Gary Kamiya of   of the  . Retrieved 2010-08-18.  Left unimpressed, critic Leonard Maltin wrote that the film was "Surprisingly flat until the climax" and thought the events relating to the Vietnam War was "an odd choice of setting and subject for a Disney family film ... which explains the lack of swearing and the toning-down of Learys conniving character." 

Actor Denis Leary was also disappointed with the movie, admitting in 2005, "The movie was so painstakingly terrible — because it took a long time to shoot — that all of us actually had pictures of the things that we were gonna buy with our money to keep us going. I had a picture of this property in Connecticut. Ray Liotta had a picture of a house that he was building outside L.A., and Danny Glover had a picture of a property in San Francisco he was gonna buy. Thats how we would get through it." 

===Box office===
The film premiered in cinemas on July 28, 1995 in wide release throughout the U.S.. During its opening weekend, the film opened in a distant 6th place grossing $6,392,155 in business showing at 2,980 locations.    The film Waterworld soundly beat its competition during that weekend opening in first place with $21,171,780.    The films revenue dropped by 33.2% in its second week of release, earning $4,271,252. For that particular weekend, the film fell to 9th place screening in 2,158 theaters but still holding on to a top ten position. Waterworld, remained in first place grossing $13,452,035 in box office revenue.    The film went on to top out domestically at $24,670,346 in total ticket sales through a 21-week theatrical run.  For 1995 as a whole, the film would cumulatively rank at a box office performance position of 67. 

===Home media===
The film was initially released in VHS video format on March 19, 1996.  The   release.

==Bibliography==
* 
* 
* 

==References==
 

==External links==
 
 
*  
*  
*  
*   at Rotten Tomatoes
*   at the Movie Review Query Engine
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 