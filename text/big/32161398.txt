Varusham Padhinaaru
{{Infobox film
| name           = Varusham Padhinaaru
| image          = VarushamPadhinaaru.JPG
| caption        = Fazil
| producer       = Ganga Chithra Productions
| writer         = Gokul Krishna (dialogues) Fazil
| Fazil
| Karthik Kushboo Sukumari V. K. Ramaswamy (actor)|V. K. Ramaswamy Janagaraj Vadivukkarasi
| music          = Ilaiyaraaja
| cinematography = Anandakuttan
| editing        = T. R. Sekar
| studio         = Ganga Chithra Productions
| distributor    = Ganga Chithra Productions
| released       = 17 February 1989
| runtime        =
| country        = India
| awards         = Tamil
| budget         =
| gross          =
| website        =
| preceded by   =
| followed by    =
}}
 1989 Tamil Indian feature directed by Fazil (director)|Fazil, starring Karthik Muthuraman|Karthik, Kushboo Sundar|Kushboo, Sukumari, V. K. Ramaswamy (actor)|V. K. Ramaswamy, Janagaraj and Vadivukkarasi. 
The film won the Filmfare Award for Best Actor for Karthik Muthuraman|Karthik. The film went on to become a super-grosser in Cinemas and completed 100 day run in many parts of the State.  

==Plot==

Varusham Padhinaaru has the story line, where family values gets conflicted due to unethical thoughts of some members, but the result was disastrous leaving a sad note to everyone.

==Cast==
 Karthik - Kannan Kushboo - Radhika
* Sukumari
* V. K. Ramaswamy (actor)|V.K.Ramaswamy
* Poornam Viswanathan
* Janagaraj - Rajamani
* Vadivukkarasi - Maheshwari
* Jayabharathi - Kannans mother
* Charle - Narayana
* Vijay Menon

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 05:40
|-
| 2 || Hey Aiyasamy || S. P. Balasubrahmanyam, K. S. Chithra || 04:31
|-
| 3 || Karayatha Manamum || K. J. Yesudas, K. S. Chithra || 04:29
|-
| 4 || Pazhamuthir Cholai || K. J. Yesudas || 04:36
|-
| 5 || Poo Pookum Masam || P. Susheela || 04:45
|}

==Box-Office==

Varusham Padhinaaru was the remake of Fazils own Malayalam film, Ennennum Kannettante and was a huge hit in Tamil Nadu. Interestingly Ennennum Kannettante was a flop in Kerala.

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 