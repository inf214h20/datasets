Harley Davidson and the Marlboro Man
{{Infobox film
| name            = Harley Davidson and the Marlboro Man
| image           = harley davidson and the marlboro man movie poster.jpg
| caption         = Theatrical release poster
| writer          = Don Michael Paul Vanessa Williams Tom Sizemore
| director        = Simon Wincer
| producer        = Jere Henshaw
| cinematography  = David Eggby
| editing         = Corky Ehlers Pathe Entertainment
| distributor     = Metro-Goldwyn-Mayer
| released        =  
| runtime         = 98 minutes
| language        = English
| country         = United States
| music           = Basil Poledouris
| budget          = $23,000,000   
| gross           = $7,434,726 (United States) 
}} action biker film starring Mickey Rourke and Don Johnson. The film was written by Don Michael Paul and directed by Simon Wincer.  Loosely based on Tony Zuzios life biking around New Jersey.

The film was a critical and financial failure, earning only $7 million at the domestic box office (the budget was estimated at $23 million). It became  a cult classic following its release to video. It promoted a "male Outlaw motorcycle club|biker" stereotype. 

== Plot ==
The film is set in the then-future of 1996 in Los Angeles. Harley Davidson (Mickey Rourke) is in a motel in Texas when he hears about a dangerous new street drug named "Crystal Dream" on the radio. The significance of this street drug does not arise again until later in the film. Harley then meets a lifelong friend, a cowboy who is nicknamed The Marlboro Man (Don Johnson) and they later plan a bank robbery to help save their friends bar from being foreclosed and replaced with a skyscraper. However, after they rob a banks armored car, they discover the cargo they stole is the designer drug "Crystal Dream", not money. Chance Wilder (Tom Sizemore), who is a bank president involved in drug dealing, demands the return of the drugs. A series of increasingly deadly encounters ensue as heavily armed assassins (who work for the bank) hunt for Harley and Marlboro. Much of it was actually filmed in and around Tucson AZ and the "Boneyard" at Davis-Monthan Air Force Base.

== Cast ==
* Mickey Rourke as Harley Davidson
* Don Johnson as The Marlboro Man (Robert Lee Anderson) Virginia Slim
* Daniel Baldwin as Alexander
* Giancarlo Esposito as Jimmy Jiles Vanessa Williams as Lulu Daniels
* Tom Sizemore as Chance Wilder
* Tia Carrere as Kimiko Jack Daniels
* Julius Harris as "The Old Man" Jiles
* Eloy Casados as Jose Cuervo 
* Kelly Hu as Suzi
* Robert Ginty as Thom
* Branscombe Richmond as Big Indian
* Sean "Hollywood" Hamilton as himself (Radio Personality)

== Soundtrack ==
{{Infobox album  
| Name = Harley Davidson and the Marlboro Man
| Type = soundtrack
| Artist = various artists
| Cover =
| Released = 1991
| Recorded =
| Genre = Rock music|Rock, Hard rock
| Length = 42:46
| Label = Mercury Records
| Producer =
| Last album =
| This album =
| Next album =
}}

{{Track listing
| extra_column    = performer
| total_length    = 42:46
| writing_credits = yes
| title1          = Long Way from Home
| writer1         = Loyd Neil Carswell
| extra1          = Copperhead
| length1         = 7:15  
| title2          = The Bigger They Come
| writer2         = Frampton, Marriott, John Regan
| extra2          = Peter Frampton, Steve Marriott
| length2         = 4:26  
| title3          = Tower of Love
| writer3         = Paul Jackson, Richard Day, Frank Noon
| note3           = from the album Roadhouse, 1991 Roadhouse
| length3         = 3:56  
| title4          = I Mess Around
| note4           = from the album Shooting Gallery, 1991
| writer4         = Billy G. Bang, Andy McCoy Shooting Gallery
| length4         = 4:19  
| title5          = Wild Obsession Steve Riley
| note5           = from the album Hollywood Vampires, 1991
| extra5          = L.A. Guns
| length5         = 4:14  
| title6          = Cmon
| writer6         = Dave Gleeson, Richard Lara All for One, 1991
| extra6          = The Screaming Jets
| length6         = 2:48  
| title7          = Lets Work Together
| writer7         = Wilbert Harrison
| extra7          = The Kentucky Headhunters
| length7         = 2:14  
| title8          = Hardline
| writer8         = Tom Kimmel, Dennis Morgan
| extra8          =  Waylon Jennings
| length8         = 4:09  
| title9          = Ride with Me
| writer9         = Dean Davidson
| extra9          = Blackeyed Susan 
| length9         = 5:10  
| title10          = What Will I Tell My Heart?
| writer10         = Peter Tinturin, Irving Gordon, Jack Lawrence The Comfort Zone, 1991 Vanessa Williams
| length10         = 4:15
}} Wanted Dead or Alive" by Bon Jovi, and "Work to Do" and "The Better Part of Me" by Vanessa Williams. An early scene around a pool table seems to have originally featured AC/DCs "Hells Bells," but has been replaced by a sound-alike.

== Reception == Time Out London called it "utter rubbish, and badly dressed at that."   Kim Newman of Empire (film magazine)|Empire wrote, "For a while, its crassness is amusing, but as the plot sets in, it gradually turns into a stultifying bore." 

== See also ==
* List of American films of 1991

== References ==
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 