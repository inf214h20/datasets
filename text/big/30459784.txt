Anuvahood
{{Infobox film
| name           = Anuvahood
| image          = AnuvaHood.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = Adam Deacon Daniel Toland Najama Dahir 
| producer       = Nick Taussig  Paul Van Carter  Daniel Toland  Terry Stone
| writer         = Michael Vu Adam Deacon
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Adam Deacon Jazzie Zonzolo Femi Oyeniran Ollie Barbieri Wil Johnson Ashley Walters
| music          = Chad Hobson
| cinematography = Felix Wiedemann
| editing        = Seth Bergstrom
| studio         = Gunslinger
| distributor    = Revolver Entertainment
| released       =  
| runtime        = 89 Minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £2,207,877
}} British urban comedy film directed by Adam Deacon, who also plays the films lead character. It also stars Paul Kaye, Wil Johnson, Ollie Barbieri, Femi Oyeniran, Jocelyn Jee Esien, and Ashley Walters. Anuvahood was released on 18 March 2011; though critics received it negatively, it had a strong box-office opening and ended up a moderate success.

Anuvahood is a parody of films in the vein of urban movies such as Kidulthood, Adulthood (film)|Adulthood, and Shank (2010 film)|Shank.     

==Plot== grime MC, and has already created his debut mixtape, Feel The Pain. However, nobody has bought a single copy and Kenneth works, for now, at local supermarket Sainsburys|Laimsburys to help pay his familys rent. When his boss insults him at work for trying to be a rapper, he quits and his mother berates him for failing to pay the house rent and his family is soon threatened by bailiffs. Kenneth cannot take seeing his mother hassled by the bailiffs, so he begins to sell illegal drugs with his friends Bookie (Femi Oyeniran), Enrique (Ollie Barbieri), Lesoi (Michael Vu), and TJ (Jazzie Zonzolo). When local badman Tyrone (Richie Campbell) investigates Kenneth, he steals Kenneths and his friends accessories. His friends leave him and his family do not support him, so Kenneth slyly breaks into Tyrones house to steal back his stuff. While Tyrone cheats on his babys mother in the other room, Kenneth manages to steal everyones stuff back, but Tyrone finds out and comes after him. Tyrone attacks him, and his friends try to help him, but Tyrone manages to scare them away, making it a one-on-one fight. Kenneth shockingly fights back and takes Tyrone down. After the humiliation, Tyrones boss arrives and witnesses Tyrone hitting kids, therefore sacks him and insults him in front of the entire hood. To make matters worse Tyrones babys mothers brother appears on the scene to punish him further for cheating on his sister, and Tyrone flees in humiliation. Kenneth gets his job back at Laimsburys and helps pay his familys rent.

==Cast==
 
* Adam Deacon as Kenneth "Kay" Fletcher
* Femi Oyeniran as Bookie
* Ollie Barbieri as Enrique
* Jazzie Zonzolo as T.J.
* Michael Vu as Lesoi Richie Campbell as Tyrone
* Jaime Winstone as Yasmin
* Paul Kaye as Tony
* Ashley Walters as Cracks
* Terry Stone as Terry
* Eddie Kadi as Tunde
* Perry Benson as Brian
* Linda Robson as Pauline
* Richard Blackwood as Russell
* Wil Johnson as Mike
* Jason Maza as Darren
* Carmell Roche as Kesha
* Jocelyn Jee Esien as Tasha
* Ashley Chin as Mo
* Michael Maris as Big T
* Alex Macqueen as Edward
* Doon Mackichan as Patricia
* Aisleyne Horgan-Wallace as Maria
* Levi Roots as Himself (special appearance during intro credits)
 

==Release==
The film released worldwide on 18 March 2011. A few months after the release, the cast and crew attended the Anuvahood Tour in which the team met up with several fans and performed sketches and dances from the movie. The tour lasted from 1–4 July 2011.

== Reception==
 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 