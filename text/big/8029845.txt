Yellow (1998 film)
 
{{Infobox film
| name           = Yellow
| image          =Yellow (1998 film).jpg image size      = 130px
| caption        =
| director       = Chris Chan Lee
| producer       = Chris Chan Lee David Yang
| writer         = Chris Chan Lee
| narrator       =
| starring       = John Cho Burt Bulos Jason Tobin
| music          = Lance Hahn
| cinematography = Ted Cohen
| editing        = Kenn Kashima
| distributor    = Phaedra Cinema
| released       = 1998
| runtime        = 90 min.
| country        = USA
| language       = English Korean
| budget         =
| preceded by    =
| followed by    =
}}
Yellow is a 1998 film directed by Chris Chan Lee. The film is about the harrowing graduation night of eight Korean-American high school youths in Los Angeles that culminates in a violent crime that will forever change their lives.

Yellow was invited to over a dozen film festivals, including the Los Angeles Film Festival, the Singapore International Film Festival and the Slamdance Film Festival. The film received a U.S. release by Phaedra Cinema and is sold worldwide through Cinema Arts. The film stars John Cho and Jason Tobin.

==External links==
* 

 
 
 
 
 
 


 