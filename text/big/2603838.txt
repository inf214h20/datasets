From Beijing with Love
 
 
{{Infobox film
| name           = From Beijing with Love
| image          = From Beijing With Love.jpg
| image_size     = 
| caption        = Hong Kong film poster
| director       = Lee Lik-Chi Stephen Chow
| producer       = 
| writer         = Stephen Chow   Roman Cheung   Vincent Kok   Lee Lik-Chi
| narrator       =  traditional = 國產凌 凌漆
| jyutping = gwok3 caan2 ling4 ling4 cat1
| pinyin =}}
| starring       = Stephen Chow Anita Yuen Law Ka Ying   Lee Lik-Chi
| music          = William Hu
| cinematography = Lee Kin-Keung Tom Lau
| editing        = Ma Chung-Yiu
| studio         = Wins Entertainment, Ltd.|Wins Movie Production Ltd. Golden Harvest
| released       =  
| runtime        = 94 min
| country        = Hong Kong Cantonese
| budget         = 
| gross          = Hong Kong dollar|HK$37,523,850.00 
| preceded_by    = 
| followed_by    = 
}} 1994 Cinema Hong Kong action and James Bond movies and stars Stephen Chow, Anita Yuen and Law Ka-Ying.

== Synopsis ==

Golden Gun steals the cranium of Chinas only dinosaur fossil. Chow, starring as a hawker-cum-secret-agent 007, is sent to Hong Kong by a high-ranking government official to recapture the cranium. When he arrives in Hong Kong, he meets Siu Kam (Anita Yuen), who proposes to help him in his endeavor. However, Siu Kam turns out to be a subordinate of Golden Gun. Golden Gun is in actuality the government official who directs Chow to find the cranium.
 Jaws from James Bond) and a mysterious woman who are out to kill 007. Taking this opportunity, Siu Kam shoots Chow several times, including once in the leg with 007 thinking another assassin has shot him. 007 (who was wearing a bulletproof vest but not bulletproof trousers) escapes, grabbing three white roses on the way out. Siu Kam is touched by this gesture and saves his life. She decides to defect from Golden Gun. Together, the two destroy the organization that is behind the theft of the cranium. Chow wins over Yuen and is rewarded with a meat cleaver emblazoned with the calligraphy of Deng Xiaoping.

== Cast ==
* Stephen Chow - Ling Ling Qi (007)
* Anita Yuen - Li Xiang Qin (the name of a Hong Kong famous actress in Cantonese opera, featuring mistress of the Emperor)
* Law Kar-ying - Da Wen Xi (Leonardo da Vinci) Pauline Chan - Mystery Woman
* Joe Cheng - Killer with metal mouth
* Lee Lik-Chi - executed Martial arts master
* Wong Kam-kong - Golden Gun, posed as official
* Wong Yat-Fei
* Yu Rongguang - agent at the beginning of the film, killed by Golden Gun
* Spencer Lam
* Indra Leech
* Lee Kin-yan
* Leung Hak-Shun
* Leung See-Ho
* Tsang Sau-Ming

 {{Cite web |url=http://www.imdb.com/title/tt0109962/ |title=From Beijing with Love 
 |accessdate=29 June 2010 |publisher=imdb.com}} 
   

== Salutation to other films ==
 
* The name of the film in Chinese means "the domestically-produced 007". blue movie.
* The scene where the camera pans slowly around a room of various James Bond posters to focus on Stephen Chow combing his hair and admiring himself in the mirror is a parody of the last scene of Days of Being Wild, in which Tony Leung prepares to go out.  The music used is the same.
* The Universe Laser DVD cover (pictured on right) of the movie parodies that of the 1987 James Bond movie The Living Daylights. The Man with the Golden Gun. Unlike the one from the James Bond series, this one shoots out extremely powerful explosive bullets instead of a one-hit fatal fragmentation bullet.

==Music==
The song Stephen Chow sang while playing the piano is 李香蘭 by Jacky Cheung.

==Box office==
It grossed a huge HK $37,523,850 in Hong Kong.

==References==
 
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 