Salsa (2000 film)
 
{{Infobox film| name           = ¡Salsa!
| image          = 
| image_size     = 
| caption        = 
| director       = Joyce Buñuel EN GRINGO
| producer       = Aïssa Djabri Farid Lahouassa Manuel Munz 
| writer         = Joyce Buñuel Jean-Claude Carrière
| starring       = Christianne Gout Vincent Lecoeur Catherine Samie Michel Aumont Roland Blanche
| music          = Yuri Buenaventura Sierra Maestra Jean-Marie Sénia 
| cinematography = 
| editing        = 
| distributor    = United International Pictures , France
| released       = January 22, 2000
| runtime        = 100 min
| country        = Spain, France
| language       = French
}}
 Spanish romance film. The film was directed by Joyce Buñuel, and stars Vincent Lecoeur, Christianne Gout, and Catherine Samie.


==Synopsis==
A brilliant classical pianist, 24-year-old Rémi Bonnet, renounces his career for his true passion: salsa. In Paris he takes dance lessons from an old salsa master, and decides to teach salsa himself in order to be accepted in a Cuban music band. By artificially darkening his skin with UV light treatments in a local tanning salon and faking a Latin accent he tries to "become" a genuine Cuban.

He meets the beautiful young Nathalie who becomes his student, and the results are romantically inevitable. She finds Rémi very seductive, much against the wishes of her father and her fiancé.  But the deception goes wrong when she finds out who the man she thinks is her true love really is.

The group, La Sierra Maestra, is the master of Cuban salsa and their music permeates the film. The vocal harmonies, the fabulous brass, and the incredible percussion are pure magic.

==Cast==
* Christianne Gout - Nathalie
* Vincent Lecoeur - Rémi
* Catherine Samie - Letty
* Michel Aumont - Monsieur Redele
* Roland Blanche - Henry
* Alexis Valdés - Félipe
* Elisa Maillot - Françoise (as Eliza Maillot)
* Aurora Basnuevo - La Goya
* Estéban Socrates Cobas Puente - Barreto
* Christiane Cohendy - Madame Redele
* Naím Thomas - Stéphane
* Pierre-Arnaud Juin	- Jean-Charles
* Olivier Bernard - Lappariteur
* Sylvain Corthay - Le directeur du Conservatoire
* Isabelle Coutançais	 - La pianiste
* Michel Debrane - Blondin (as Michel Debrannes)
* Annabelle Février - La mère
* Jean-Luc Horvais - Le père (as Jean-Luc Horvay)
* Marie-Christine Leclaire - Femme U.V. (as Marie Christine Leclaire)
* Elisabeth Margoni - Mme de Stael
* Jéhan Pages -voix Stéphane (voice)
* Stéphane Slima - Linspecteur de police
* Brigitte Tourtchaninoff- Assistante du Directeur du Conservatoire

==External links==
*  

== References ==
 

 
 
 
 
 
 
 


 
 
 