The Argyle Secrets
{{Infobox film
| name           = The Argyle Secrets
| image          = "The_Argyle_Secrets"_(1948).jpg
| caption        = Lobby card
| director       = Cy Endfield
| producer       = Sam X. Abarbanel Alan H. Posner
| screenplay     = Cy Endfield
| narrator       =
| starring       = William Gargan Marjorie Lord
| music          = Raoul Kraushaar (as Ralph Stanley)
| cinematography = Mack Stengler
| editing        = Gregg C. Tallas
|  studio         = Eronel Productions
| distributor    = Film Classics  (USA)
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         =
}}
The Arygle Secrets is a 1948 film directed by Cy Endfield.  It was based on a half-hour radio play by Endfield, originally heard on CBS Suspense.     The film was made for the micro-budget of $100,000 and shot in eight days. 

==Plot==
Reporter Harry Mitchell tracks down incriminating papers showing that some leading Americans collaborated with the Nazis during the war.   

==Cast==
*Harry Mitchell -	William Gargan
*Maria -	Marjorie Lord	
*Lt. Samuel Sampson - Ralph Byrd	
*Panama Archie - Jack Reitzen	
*Winter - John Banner	
*Elizabeth Court -  Barbara Billingsley	
*Jor McBrod - Alex Frazer	
*Scanlon - Peter Brocco	 George Anderson	
*Gil Hobrey - Mickey Simpson	
*Pinky Pincus - Alvin Hammer	
*The Nurse - Carole Donne	
*Mrs. Rubin - Mary Tarcai	
*Melvyn Rubin - Cop - Robert Kellard	
*Gerald Rubin - Kenneth Greenwald

==Critical reception== Variety called the film "a particularly interesting B movie in its suggestion that the U.S. government secretly brought Nazis into the country to work for the military." 

==References==
 

==External links==
*  at IMDB

 

 
 
 