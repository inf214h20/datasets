If I Had a Million
 
{{Infobox film
|name= If I Had A Million
|image= If I Had a Million poster.jpg
|image_size= 
|caption= Theatrical poster Stephen Roberts Norman Z. McLeod James Cruze William A. Seiter H. Bruce Humberstone Lothar Mendes (uncredited)
|producer= Emanuel Cohen
|writer= Robert Hardy Andrews (story) multiple writers 
|narrator=  Richard Bennett
|music= John Leipold Charles Edgar Schoenbaum Gilbert Warrenton Alvin Wyckoff (all uncredited)
|editing= LeRoy Stone
|distributor= Paramount Pictures
|released= December 2, 1932
|runtime= 88 minutes
|country= United States
|language= English
|budget= 
|gross= 
}}
 Stephen Roberts, Norman Z. McLeod, James Cruze, William A. Seiter, and H. Bruce Humberstone  . Lubitsch, Cruze, Seiter, and Humberstone were each responsible for a single vignette, Roberts and McLeod directed two each, and Taurog was in charge of the prologue and epilogue. The screenplays were scripted by many different writers, with Joseph L. Mankiewicz making a large contribution. If I Had a Million is based on a novel by Robert Hardy Andrews.  

A wealthy dying businessman decides to leave his money to eight complete strangers. Gary Cooper, Charles Laughton, George Raft, May Robson, Charles Ruggles, and Gene Raymond play some of the lucky beneficiaries.
 The Millionaire was based on a similar concept. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 33 

==Plot== Richard Bennett) cannot decide what to do with his wealth. He despises his money-hungry relatives and believes none of his employees is capable of running his various companies. Finally, he decides to give a million dollars each to eight people picked at random from a telephone directory before he passes away, so as to avoid his will being contested. (The first name selected is John D. Rockefeller, which is swiftly rejected.)

===China Shop===
* Directed by Norman Z. McLeod

Henry Peabody (Charles Ruggles) is unhappy, both at work and at home. A bookkeeper promoted to salesman in a china shop, Henry keeps breaking the merchandise, meaning his "raise" results in his bringing home less money than before, something his nagging wife (Mary Boland) is quick to notice. After Glidden gives him a certified check, Henry shows up late for work and then proceeds to gleefully wreak destruction on the wares.

===Violet===
* Directed by Stephen Roberts

Barroom prostitute Violet Smith (Wynne Gibson) checks into the most expensive hotel suite she can find and goes to bed ... alone.

===The Forger===
* Directed by H. Bruce Humberstone

Eddie Jackson (George Raft) narrowly avoids arrest for trying to cash a forged check. With his prior record, if he is caught, it will mean a life sentence in prison. When Glidden presents him with his check, Eddie is delighted ... at first. However, he does not dare show his face in a bank, and none of his criminal associates believes the check is genuine. Frantic to leave town and desperately needing to sleep, the penniless man gives the check as security for a 10 cent bed in a flophouse. The manager secretly calls the police to take away what he thinks is a lunatic, and uses the check to light his cigar.

===Road Hogs===
* Directed bt Norman Z. McLeod

Ex-vaudeville performer Emily La Rue (Alison Skipworth) is very content with her life, running her tea room with the help of her partner, ex-juggler Rollo (W. C. Fields). Only one thing is lacking to make her satisfaction complete, and it is delivered that very day: a brand new car. However, when they take it out for a drive, it is wrecked when another driver ignores a stop signal. The heartbroken woman returns to her tea room, where Glidden finds her. 

She comes up with an inventive way to spend part of her great windfall. She and Rollo purchase eight used cars and hire drivers. They all take to the road in a long procession. When they encounter an inconsiderate road hog, Emily and Rollo immediately set off in pursuit and crash into the offenders automobile. They then switch to one of their spare cars and repeat the process, until they run out of automobiles. At the end of the day, Emily purchases another new car, but it too is destroyed in a collision with a truck. No matter. Emily tells Rollo it has been "a glorious day".

===Death Cell===
* Directed by James Cruze

Prisoner John Wallace (Gene Raymond) has been condemned to the electric chair for killing someone during a robbery. After a tearful conversation with his wife Mary (Frances Dee), he is visited in his cell by Glidden. John is certain that his new-found wealth will save him, but it is too late. He is executed that same day, despite his protests.

===The Clerk===
* Directed by Ernst Lubitsch
 blows a raspberry at his former boss and leaves.

===The Three Marines===
* Directed by William A. Seiter

Glidden finds United States Marine Corps|U.S. Marine Steve Gallagher (Gary Cooper) and his good buddies Mulligan (Jack Oakie) and OBrien (Roscoe Karns) in the stockade for striking their sergeant. However, when Glidden gives Gallagher the check, Gallagher notices it is April Fools Day and assumes it is a joke. 

When the three men are released, they immediately head for a nearby lunch stand to see Marie (Joyce Compton), the pretty waitress. They all want to take her to the carnival, but none of them has any money. Then Gallagher remembers his check and that Zeb, the stands owner, is illiterate. He tells Zeb that the check is for $10 and gets Zeb to cash it. He and Marie head off to the carnival, but Gallagher cannot shake his pals. Then Mulligan becomes embroiled in a fight, his comrades join in, and the trio end up right back in the stockade. Through the bars, they watch dumbfounded as a fancily dressed Zeb steps out of a limousine, escorting an equally well-garbed Marie.

===Grandma===
* Directed by Stephen Roberts

The last beneficiary is Mary Walker (May Robson), one of many unhappy elderly women consigned to a rest home run by Mrs. Garvey (an uncredited Blanche Friderici). Mrs. Garvey is a petty tyrant who enforces her rules rigorously, to the displeasure of her charges, especially the spirited, defiant Mary. Mary uses her money to turn the tables. She pays Mrs. Garvey and the rest of the staff just to sit in rocking chairs while she and the other residents have a wonderful time partying and dancing with their gentleman friends. 

Marys spirit even reinvigorates John Glidden. Glidden ignores his doctor and looks forward to spending time with Mary.

==Cast (in credits order)==
*Gary Cooper as Steve Gallagher
*Charles Laughton as Phineas V. Lambert
*George Raft as Eddie Jackson
*Jack Oakie as Private Mulligan Richard Bennett as John Glidden
*Charles Ruggles as Henry Peabody
*Alison Skipworth as Emily La Rue
*W. C. Fields as Rollo La Rue
*Mary Boland as Mrs Peabody
*Roscoe Karns as Private OBrien
*May Robson as Mrs Mary Walker
*Wynne Gibson as Violet Smith
*Gene Raymond as John Wallace
*Frances Dee as Mary Wallace
*Lucien Littlefield as Zeb - Hamburger Stand Owner
*Joyce Compton as Marie - Waitress
*Cecil Cunningham as friend of Emily La Rues

==Notes==
 

==References==
 
 

==External links==
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 