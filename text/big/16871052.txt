Aur Ek Prem Kahani
 
{{Infobox film
| name           = Aur Ek Priem Kahani
| image          = Aur Ek Prem Kahani (video cover art).jpg
| image_size     =
| caption        = DVD Cover
| director       = Balu Mahendra
| producer       = Mahesh Bhatt Amit Khanna
| writer         = Balu Mahendra
| narrator       =
| starring       = Ramesh Aravind Revathi Menon Heera Rajgopal Sudhir Ahuja
| music          = Ilayaraaja
| cinematography = Balu Mahendra
| editing        =
| distributor    = Eros
| released       = 1996
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1996 Hindi Indian feature directed by famed Cinematographer, Balu Mahendra. The film became the debut of several Southern stars, Ramesh Aravind, Sudhir Ahuja, and Heera Rajgopal. Co-starred the film were Revathi Menon and Akshay Anand. The film is a remake of Balu Mahendras Kannada debut film Kokila (film)|Kokila.

==Story==

Aur Ek Prem Kahani has Ranganathan (Sudhir Ahuja), a middle aged engineer who lives in a middle class home in Madras with his wife Kamala (Sushma) and daughter Kokila (Heera Rajgopal), who is a medical student. Manga (Revathy) a maid lives with them and is treated like a member of the family. As Ranganathan travels a lot and his wife is not well, the family decides to keep a paying guest. Sathyamoorthy a bank executive (Arvind Ramesh) comes to live in the Ranganathan household. Kokila and Sathyamoorthy fall in love and plan to get married with her parents consent. Meanwhile one night perchance Sathya and the maid Manga are alone in the house. One thing leads to another and before they realise they end up making love. Kokila remains unaware of this incident even as Manga knows about Kokilas affair with Sathya. Sathya soon forgets the Manga episode and continues his romance with Kokila. One day while Kokila is away on a college tour Satya is informed by Manga that she is pregnant and hopes he will not let her down. Sathya suggests that Manga abort the child. Manga is shattered and leaves the Ranganathan house. Kokila comes back from the college tour and finds Sathya missing. He has vacated the room, says her mother and all attempts by Kokila to trace him remain futile. Kokila and Sathya meet each other accidentally, after few years in a village where she has gone as a doctor. She discovers Satya has married Manga and they have a young daughter whom they have named Kokila.

==Cast==

*Ramesh Aravind ...  Sathyamoorthy
*Heera Rajgopal ...  Kokila
*Revathy ...  Manga
*Sudhir Ahuja ...  Shankar
*Sushma Ahuja ...  Kamla
*Akshay Anand ...  Govindram Dinanath Vijaykumar Shastri

==Soundtrack==

Music is provided by Maestro Ilaiyaraja. He has recycled his own tunes from Johnny, Alaigal Oivathillai, Manvasanai, Olangal

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! #
! Track
! Singer(s)
! Original Movie
! Original Song
! Language
! Picturised On
|-
| 1
| Naina Bole Naina
|Asha Bhosle
|Johnny (1980 film)|Johnny
| Kaatril... enthan geetham
|Tamil cinema|Tamil
|Rajinikanth, Sridevi
|-
| 2
| Hona Hai Tho
| Mano (singer)|Mano, Asha Bhosle
|Mann Vasanai
| Pothi Vacha Malliga
|Tamil cinema|Tamil
|Paandiyan, Revathi
|-
| 3
| Monday Tho Utkar
| Mano (singer)|Mano, Preeti Uttam Singh
|Olangal
| Thumbi Vaa..
|Malayalam cinema|Malayalam
|Amol Palekar, Poornima Jayaram
|-
| 4
| Meri Zindagi
|Asha Bhosle
|Alaigal Oivathillai
| Kaadhal Oviyam...
|Tamil cinema|Tamil
|Karthik Muthuraman, Radha (actress)|Radha
|}

==References==
* http://www.ibosnetwork.com/asp/filmbodetails.asp?id=Aur+Ek+Prem+Kahani

==External links==
*  

 

 
 
 
 
 