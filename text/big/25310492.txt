Cold Prey 3
 
 
{{Infobox film
| name           = Cold Prey 3
| image          = Fritt Vilt III.JPG
| alt            =  
| caption        = Poster of "Fritt vilt III"
| director       = Mikkel Brænne Sandemose
| producer       = Martin Sundland Kristian Sinkerud
| writer         = Peder Fuglerud Lars Gudmestad
| starring       = Ida Marie Bakkerud Kim S. Falck-Jørgensen Nils Johnson
| music          = Magnus Beite
| cinematography = Ari Willey
| editing        = Wibecke Rønseth
| studio         = Fantefilm
| distributor    = 
| released       =  
| runtime        = 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          = 
}}
Cold Prey 3 ( ) is a 2010 Norwegian slasher film, it is the prequel to the highly successful Cold Prey (Fritt vilt), and Cold Prey 2 (Fritt vilt II).  It is directed by Mikkel Brænne Sandemose and starred Nils Johnson in the leading role. 

==Plot==
 
A group of young friends take a trip into the national park near Jotunheimen. They plan on hiking into the area where an abandoned hotel sits in the mountains; it was the site of several disappearances including a young boy who vanished into thin air. After opting to camp in the woods instead of stay at the old hotel, the friends soon discover there is a hulking killer amongst the woods, and worst of all he may not be alone. In Flashback (narrative)|flashbacks centers the killers childhood and youth in the 80s. 

==Cast==
* Ida Marie Bakkerud as Hedda
* Kim S. Falck-Jørgensen as Anders
* Pål Stokka as Magne
* Julie Rusti as Siri
* Arthur Berning as Simen
* Sturla Rui as Knut
* Terje Ranes as Einar
* Nils Johnson as Jon
* Hallvard Holmen as Bjørn Brath
* Trine Wiggen as Magny Brath

==Production==
 
  
Cold Prey 3 is set to be a prequel to the first two movies.  It was completely shot in Jotunheimen, Norway. 

==Release==
 
The film premiered on 15 October 2010 in Norway. 

==Reception==
 
==References==
 

==External links==
* 


 
 
 
 
 
 
 
 
 
 
 


 
 