Mabel's Strange Predicament
{{Infobox film
| name      = Mabels Strange Predicament
| image     = MabelsStrangePredicament-1.jpg
| caption   = Mabel Normand in a scene from the movie
| director  = Mabel Normand
| producer  = Mack Sennett
| writer    = Henry Lehrman
| starring  = {{plainlist|
* Mabel Normand
* Charles Chaplin
* Chester Conklin
* Alice Davenport
* Harry McCoy
* Hank Mann
* Al St. John
}}
| cinematography = Hans Koenekamp|H.F. Koenekamp 
| distributor = Keystone Studios
| released    =  
| runtime     = 17 minutes
| language    = Silent film English (Original titles)
| country     = United States
}}
 
 ]]
Mabels Strange Predicament is a 1914 American film starring Mabel Normand and Charles Chaplin, notable for being the first film for which Chaplin donned The Tramp costume. 

==Plot== tramp runs into an elegant lady, Mabel, who gets tied up in her dogs leash, and falls down. He later runs into her in the hotel corridor, locked out of her room. They run through various rooms. Mabel ends up in the room of an elderly husband where she hides under the bed. Enter the jealous wife, who soon attacks Mabel, her husband, and Mabels lover, not to mention the staggeringly drunken tramp.

==Cast==
*Charles Chaplin as The Tramp
*Mabel Normand as Mabel
*Chester Conklin as Husband
*Alice Davenport as Wife
*Harry McCoy as Lover
*Hank Mann as Hotel Guest
*Al St. John as Bellboy

==First "Tramp" appearance filmed==
The Tramp was first presented to the public in Chaplins second film Kid Auto Races at Venice (released February&nbsp;7, 1914) though Mabels Strange Predicament, his third film in order of release, (released February&nbsp;9, 1914) was produced a few days before. It was for this film that Chaplin first conceived of and played the Tramp. As Chaplin recalled in his autobiography:
{{quote|text=I had no idea what makeup to put on. I did not like my get-up as the press reporter  . However on the way to the wardrobe I thought I would dress in baggy pants, big shoes, a cane and a derby hat. I wanted everything to be a contradiction: the pants baggy, the coat tight, the hat small and the shoes large. I was undecided whether to look old or young, but remembering Sennet had expected me to be a much older man, I added a small mustache, which I reasoned, would add age without hiding my expression.
I had no idea of the character. But the moment I was dressed, the clothes and the makeup made me feel the person he was. I began to know him, and by the time I walked on stage he was fully born.|sign=Chaplin |source=My Autobiography, p. 154}}

Mabels Strange Predicament is one of more than a dozen early films that writer/director/comedienne Mabel Normand made with Chaplin; Normand, who had written and directed films before Chaplin, mentored the young comedian. Chaplins Tramp is shown swigging from a flask toward the beginning of the film and subsequently becoming so drunk that he staggers when he walks and falls down repeatedly near the end. His portrayal of drunkenness remains convincingly realistic. The Tramp also keeps his derby cocked throughout the proceedings, a touch that Chaplin abandoned later in his career.

==See also==
* Charlie Chaplin filmography
* List of American films of 1914

==References==
{{reflist|refs=
   
}}

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 