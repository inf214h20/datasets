Creep (2004 film)
 
{{Infobox film
| name           = Creep
| image          = Creep_movie_poster.jpg
| caption        =  Theatrical release poster Christopher Smith
| producer       = Julie Baines Martin Hagemann Barry Hanson Robert Jones Kai Künnemann Jason Newmark
| writer         = Christopher Smith Ken Campbell Vas Blackwood Jeremy Sheffield Sean Harris
| music          = The Insects
| cinematography = Danny Cohen
| editing        = Kate Evans
| studio         = UK Film Council Filmstiftung NRW Dan Films Zero West
| released       =  
| runtime        = 81 minutes
| country        = United Kingdom Germany
| language       = English GBP £1,728,375
}} horror film Christopher Smith, about a woman locked in overnight on the London Underground who finds herself being stalked by a hideously deformed killer living in the sewers below. The film was first shown at the Frankfurt Fantasy Filmfest in Germany on 10 August 2004.

== Plot ==
 
 Ken Campbell) and George (Vas Blackwood), who discover a tunnel in one of the walls that neither of them is familiar with. Arthur enters and George follows. He soon discovers Arthur, injured and in a state of shock. Moments later, a similarly injured young woman jumps out in front of them, crying for help, only to be pulled back into the darkness.

The focus then shifts to a young German woman, Kate (Franka Potente), at a party. After hearing that her friend, Gemma, went to another party with George Clooney without her even though they planned to go together, she decides to travel there. She heads to Charing Cross tube station, but soon falls asleep on the platform while waiting for the train. When she awakens, she is alone and finds the entire station locked up for the night. Another empty train arrives and she boards it, but it stops abruptly. Confused, Kate makes her way to the driverss compartment but cannot find the reason for the stoppage; unbeknownst to her, the driver has been killed. Kate soon encounters Guy (Jeremy Sheffield), an obsessive acquaintance from the previous party. Guy, intoxicated by cocaine, crudely attempts to seduce her, but Kate is not interested and tries to leave the train. Guy then sexually assaults her, but is dragged off her and out of the train by an unseen attacker.

Kate flees from the train and runs into a homeless couple, Jimmy (Paul Rattray) and Mandy (Kelly Scott), and their dog, a Jack Russell terrier called Ray. Kate explains what has happened and Jimmy reluctantly agrees to help her after she pays him. Meanwhile, Mandy is also attacked while alone. Kate and Jimmy find Guy lying on the railway track with his back mutilated. After they pull him onto the platform, Ray appears with blood smeared on his fur. Jimmy, immediately thinking of Mandy, goes with Ray back to their shelter where she is nowhere to be found. Kate attempts to contact a watchman via speakers to get help. Suspicious of her, the watchman demands Guy to be dragged in front of the security cameras to prove that she is not lying. When she does so, Guy dies and the watchman is also killed in his office by having his throat slit by the stalker. Kate runs back to a despondent Jimmy who has shot himself full of drugs. She eventually persuades him to help her find Mandy and a way out of the station. After exploring a tunnel together, another train pulls up in front of them. Jimmy enters and is killed by the stalker, who was lurking on the roof. Kate flees into the sewer system below the station, but is soon captured by the killer; a hideously deformed, mentally ill hermit named "Craig" AKA The Creep (Sean Harris).
 being stored as food for Craig. She also meets George, who is still alive and also trapped. However, before Craig can harm either of them, Kate manages to escape, to temporarily incapacitate Craig, and to release George. Together, they run through several dark corridors and end up in a secret, deserted abortion clinic, where they find an unconscious Mandy strapped on an operating chair and mistakenly presume her to be dead. However, before they can investigate further, Craig appears and they are forced to flee and leave Mandy alone with him, who kills her in a twisted imitation of an abortion.

George and Kate eventually find themselves in an abandoned platform and Craig soon catches up to them once again. George attacks Craig, but is impaled through the head with a serrated blade protruding out of a wall. Kate attempts to escape, but is soon cornered by Craig. She breaks down until she spots a large hook on a long chain, with which she stabs Craigs throat. She then hears the distant sounds of an approaching train and she throws the other end of the chain over the tunnel, in an attempt to electrocute him. Her plan fails, and although severely wounded, Craig rises and attempts to crush Kate with a barrel. The train then passes through the tunnel and smashes into the chain, which tears out his throat. Craig finally dies, and Kate makes her way through the tunnel and finds herself back in Charing Cross Station. Tattered and filthy, she collapses on a platform and Ray appears, curling onto her lap. The film ends when a man waiting for a train puts a coin next to her, thinking she is a beggar, and she breaks into hysterical giggles and tears.

== Cast ==

* Franka Potente as Kate
* Vas Blackwood as George
* Ken Campbell as Arthur
* Jeremy Sheffield as Guy
* Paul Rattray as Jimmy
* Kelly Scott as Mandy Joe Anderson as Male Model
* Sean Harris as Craig, the "Creep" Morgan Jones as the Night Watchman

== Production ==
  cannibalistic killer. Director Smith, who had not seen Death Line, attributes his inspiration to a scene in An American Werewolf in London set in the London Underground. 

== Critical reception ==
 
Creep has received generally mixed reviews from critics. It currently holds a 46% rotten rating on movie review aggregator website Rotten Tomatoes based on thirteen reviews. 
 Time Out gave the film a negative review, writing, "this London Underground-set slasher squanders a promising premise   for trashy shocks, old-school nastiness and a fistful of genre clichés. " 

AllRovi|AllMovies review was favourable, writing, "this pared-down shocker might be light on plot, but its packed with creepy frights and psychopathic attitude from its mean main monster. " 

Matthew Turner ViewLondon said "The performances are good, particularly Potente, who avoids scream queen clichés by making her character surprisingly unlikeable - Kate is rude and arrogant in her early scenes and the fact that shes German is, of course, a coincidence." 

== See also ==
* List of London Underground-related fiction

== References ==
 

== External links ==

*  

 

 
 
 
 
 
 
 
 
 
 