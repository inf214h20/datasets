45 Years
 
{{Infobox film
| name           = 45 Years
| image          = 
| caption        = 
| director       = Andrew Haigh
| producer       = Tristan Goligher
| writer         = David Constantine Andrew Haigh
| starring       = Charlotte Rampling
| music          = 
| cinematography = Lol Crawley
| editing        = Jonathan Alberts
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}

45 Years is a 2015 British drama film directed by Andrew Haigh. It was screened in the main competition section of the 65th Berlin International Film Festival.    Charlotte Rampling won the Silver Bear for Best Actress and Tom Courtenay won the Silver Bear for Best Actor.   

==Cast==
* Charlotte Rampling as Kate Mercer
* Dolly Wells as Charlotte
* Tom Courtenay as Geoff Mercer
* Geraldine James as Lena
* Max Rudd as Maître d David Sibley as George Sam Alexander as Chris the postman
* Hannah Chalmers Richard Cunningham as Mr. Watkins
* Kevin Matadeen as Waiter

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 