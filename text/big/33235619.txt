L'aventure, c'est l'aventure
{{Infobox film
| name           = Laventure, cest laventure
| image          = Laventure, cest laventure.jpg
| image_size     =
| caption        =
| director       = Claude Lelouch
| producer       = Georges Dancigers Alexandre Mnouchkine
| writer         = Claude Lelouch Pierre Uytterhoeven
| narrator       =
| starring       = Lino Ventura Jacques Brel Charles Denner
| music          = Francis Lai José Padilla
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 120 min
| country        = France
| language       = French
| budget         =
| gross          =
}}
Laventure, cest laventure is a 1972 French film directed by Claude Lelouch. The film was screened at the 1972 Cannes Film Festival, but wasnt entered into the main competition.   

== Plot ==
Five incorrigible crooks, frustrated by their disappointing career as bank robbers, decide to switch from bank heists to kidnapping European celebrities. Pretending to be terrorists, they kidnap their first hostage, singer Johnny Hallyday, and demand a huge sum of money to prevent further trouble. They receive their ransom money. They go on to kidnap other celebrities, and grow more successful with each kidnapping. Eventually they expand their targets and hijack planes, kidnap ambassadors, and even abduct the Pope.  

== Cast ==
* Lino Ventura – Lino Massaro
* Jacques Brel – Jacques
* Charles Denner – Simon Duroc
* Johnny Hallyday – Himself
* Charles Gérard – Charlot
* Aldo Maccione – Aldo
* Nicole Courcel – Nicole
* Yves Robert – Lavocat de la défense
* André Falcon – The ambassador
* Juan Luis Buñuel – Ernesto Juarez
* Gordon Heath	
* Prudence Harrington – The ambassadors wife
* Maddly Bamy – Une vacancière (credited as Madly Bamy)
* Sophie Boudet – Une vacancière
* Annie Ho – Une vacancière (credited as Annie Hau)
* Annie Kerani – Une vacancière
* Xavier Gélin – Linos son
* Sevim Joyce		
* Jean Berger		
* Henry Czarniak – Le motard
* Gérard Sire – Lavocat général
* Michele Alet (uncredited)
* Catherine Allégret – Une militante (uncredited)
* Elie Chouraqui – A revolutionary (uncredited)
* Jean Collomb – The Pope (uncredited)
* Georges Cravenne – Le président du tribunal (uncredited)
* Eva Damien – Lexperte en politique (uncredited)
* Michel Drucker – Himself (uncredited)
* El Kebir – Un militant (uncredited)
* Arlette Gordon – The flight attendant (uncredited)
* Sylvain Joubert – Un militant (uncredited)
* Pierre Kast (uncredited)
* Alexandre Mnouchkine – Davis (uncredited)
* Jacques Paoli – Himself (uncredited)
* Roger Rudel – An official in the car (uncredited)
* Lionel Vitrant – Man on a bicycle (uncredited)

== References ==
 

== External links ==
* 

 

 
 
 
 
 

 