Christmas Rathri
{{Infobox film
| name = Christmas Rathri
| image =Christmas rathri.jpg
| caption =
| director = P. Subramaniam
| producer = P Subramaniam
| writer = Muttathu Varkey
| screenplay = TN Gopinathan Nair
| starring = T. K. Balachandran Thikkurissi Sukumaran Nair Soman Adoor Pankajam
| music = Br Lakshmanan
| cinematography = NS Mani
| editing = KD George
| studio = Neela Productions
| distributor = Neela Productions
| released =  
| country = India Malayalam
}}
 1961 Cinema Indian Malayalam Malayalam film, directed and produced by P. Subramaniam and was filmed at Merryland Studio. The film stars T. K. Balachandran, Thikkurissi Sukumaran Nair, Soman and Adoor Pankajam in lead roles. The film had musical score by Br Lakshmanan.    The plot revolves around Advocate George (played by Thikkurissi) and Annie (played by Miss Kumari). While "Christmas" is in the title, the film has very little to do with the holiday except that the climax of the film occurs on Christmas Eve. 

==Cast==
  
*T. K. Balachandran as Dr Mathews
*Thikkurissi Sukumaran Nair as George
*Soman 
*Adoor Pankajam as Mariya
*Aranmula Ponnamma as Annamma Ambika as Gracy 
*Bahadoor as Kurian
*Kannamma
*Kottarakkara Sreedharan Nair as Valyedathu Vareechan
*Miss Kumari as Annie
*N. Govindankutty as Philip
*Pankajavalli as Thresiamma
*Paravoor Bharathan as Porinchu
*S. P. Pillai as Thoma
*Benjamin F
 

==Soundtrack==
The music was composed by Br Lakshmanan and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aattummanammele   || P. Leela, Kamukara, AP Komala || P. Bhaskaran || 
|- 
| 2 || Appozhe Njan || Kamukara, AP Komala || P. Bhaskaran || 
|- 
| 3 || Enthininiyum || PB Sreenivas || P. Bhaskaran || 
|- 
| 4 || Kanmani Karayalle || P. Leela || P. Bhaskaran || 
|- 
| 5 || Karinkaaru Nerthallo || Kamukara || P. Bhaskaran || 
|- 
| 6 || Kinaavinte || AP Komala || P. Bhaskaran || 
|- 
| 7 || Lelam Kale || AP Komala || P. Bhaskaran || 
|- 
| 8 || Mishihaanaadhan || TS Kumaresh || P. Bhaskaran || 
|- 
| 9 || Nanmaniranjoramme || P. Leela || P. Bhaskaran || 
|- 
| 10 || Varanondu Laathi || TS Kumaresh || P. Bhaskaran || 
|- 
| 11 || Vinnil Ninnum || P. Leela, Chorus || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 