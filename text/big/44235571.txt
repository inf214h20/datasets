Sabarimala Sree Ayyappan
{{Infobox film
| name           = Sabarimala Sree Ayyappan
| image          =
| caption        =
| director       = Renuka Sharma
| producer       = Subramaniam Kumar V Swaminathan
| writer         = Sreekumaran Thampi (dialogues)
| screenplay     = Rajkumar Geetha Geetha Master Sanjay
| music          = K. V. Mahadevan
| cinematography = SV Sreekanth
| editing        = BV Krishnan VM Victor
| studio         = VSN Productions
| distributor    = VSN Productions
| released       =  
| country        = India Kannada
}} 1990 Cinema Indian Kannada Kannada film, Geetha and Master Sanjay in lead roles. The film had musical score by K. V. Mahadevan.   

==Cast== Rajkumar
*Geetha Geetha
*Master Sanjay

==Soundtrack==
The music was composed by K. V. Mahadevan.

The following are the list of songs in Malayalam dubbed version and lyrics was written by Sreekumaran Thampi and Kumbakudi Kulathur Iyer.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Daivam neeye ayyappa || K. S. Chithra || Sreekumaran Thampi || 
|-
| 2 || Ganapathiye || K. S. Chithra || Sreekumaran Thampi || 
|-
| 3 || Harivaraasanam || K. J. Yesudas, Chorus || Kumbakudi Kulathur Iyer || 
|-
| 4 || Poorna Chandran Vannu ||  || Sreekumaran Thampi || 
|-
| 5 || Shankara Shashidhara || K. J. Yesudas, Chorus || Sreekumaran Thampi || 
|-
| 6 || Sree Hari Rakshakan || K. J. Yesudas, Chorus || Sreekumaran Thampi || 
|-
| 7 || Swami Ayyappa || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 8 || Swami Ayyappa Group || K. J. Yesudas, Chorus || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 
 


 