Under the Flag of the Rising Sun
{{Infobox film
| name           = Under the Flag of the Rising Sun
| image          = Fukasaku Under the Flag R1 DVD.jpg
| caption        = Cover to the Region 1 DVD release of Under the Flag of the Rising Sun
| director       = Kinji Fukasaku
| producer       = Seishi Matsumaru Eigasha Shinsei Shohei Tokizane
| writer         = Shoji Yuki (novel) Kinji Fukasaku Norio Osada Kaneto Shindō
| starring       = Sachiko Hidari Tetsuro Tamba
| music          = Hikaru Hayashi
| cinematography = Hiroshi Segawa
| editing        = Keiichi Uraoka
| distributor    = Toho
| released       =  
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
 Best Foreign Language Film at the 45th Academy Awards, but was not accepted as a nominee.  It was also one of the films selected for a Los Angeles tribute to Fukasaku, after he died.

==Cast==
{| class="wikitable"
! Actor 
! Role
|-
|  Tetsuro Tamba
|  Katsuo Tomikashi
|-
|  Sachiko Hidari
|  Sakie
|-
|  Yumiko Fujita
|  Tomoko
|-
|  Noboru Mitani
|  Terada
|-
|  Paul Maki
|  Paul
|-
|  Kanemon Nakamura
|  Tesinjara
|-
|  Shinjiro Ehara
|  Ochi
|-
|  Isao Natsuyagi
|  Teacher
|-
|  Koichi Yamamoto
|  Actor
|}

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==Sources==
*  
*  
*  
*  

== External links ==
*  
*  

 

 
 
 
 
 


 