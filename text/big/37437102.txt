Lai Shi, China's Last Eunuch
 
 
 
{{Infobox film
| name           = Lai Shi, Chinas Last Eunuch
| image          = ChinasLastEunuch.jpg
| alt            = 
| caption        = Film poster
| film name = {{Film name| traditional    = 中國最後一個太監
| simplified     = 中国最后一个太监
| pinyin         = Zhōngguó Zuìhòu Yīge Tàijiān
| jyutping       = Zung1 Gwok3 Zeoi3 Hau6 Jat1 Go3 Taai3 Gaam3}}
| director       = Jacob Cheung
| producer       = Sammo Hung
| screenplay     = Eddie Fong
| story          = Ni Kuang Jacon Cheung Eddie Fong Chan Wai
| starring       = Max Mok Irene Wan Sammo Hung Andy Lau Wu Ma Lam Ching-ying
| music          = Joseph Chan Sherman Chow
| cinematography = Tom Lau
| editing        = Peter Cheung Bo Ho Films Paragon Films Golden Harvest
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$15,624,171
}}
Lai Shi, Chinas Last Eunuch, also known as Last Eunuch in China, is a 1988 Hong Kong historical drama film directed by Jacob Cheung in his directorial debut and starring Max Mok in the title role of Liu Lai Shi. The film is based on Ni Kuangs novel about eunuch Sun Yaoting.

==Plot== Xuantong Emperor was expelled from the Forbidden City and Liu also began his wandering career.

==Cast==
*Max Mok as Liu Lai Shi, the main protagonist, Chinas last eunuch
*Irene Wan as Chiu Tai, Liu Lai Shis childhood friend
*Sammo Hung as Liu Lai Shis teacher, the opera troupe leader
*Andy Lau as Han Ming, Chiu Tais husband and a revolutionary
*Wu Ma as Lord Ting, the head eunuch
*Lam Ching-ying as Liu Chang Fu, Liu Lai Shis father
*Kuei Ya-lei as Liu Lai Shis mother
*Pauline Wong as Sister Hung, Ching Lau lady
*Manfred Wong as Eunuch Lee
*Peter Mak
*Alfred Cheung as District chief Anthony Chan as Japanese interpreter James Tien as General Lei
*Sit Hon as Lord Chao
*Sihung Lung as Fus landlord
*Ng Min Kan as opera troupe member
*Yuen Miu as opera troupe member
*Chow Kam Kong as opera troupe member
*Pang Yun Cheung as opera troupe member
*Lee Chi Kit as Japanese soldier
*Vincent Chiao as Adjutant

==Box office==
The film grossed HK$15,624,171 at the Hong Kong box office during its theatrical run from 4 March to 31 March 1988 in Hong Kong.

==Award nominations==
*8th Hong Kong Film Awards Best Director (Jacob Cheung) Best Actor (Max Mok) Best Supporting Actor) (Wu Ma)

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 