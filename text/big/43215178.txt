Estrada de Palha
{{Infobox film
| name           = Estrada de Palha
| image          = 
| alt            = 
| caption        = 
| director       = Rodrigo Areias
| producer       = 
| writer         = 
| screenplay     = Rodrigo Areias Nuno Melo
| music          = The Legendary Tigerman Rita Redshoes
| cinematography = 
| editing        = Tomás Baltazar
| studio         = 
| distributor    = 
| released       =   
| runtime        = 120 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
| gross          = 
}} action adventure adventure western film directed by Rodrigo Areias.     

==Plot==
The film is set in the 1900s (decade)|1900s. 

==Cast== Nuno Melo

==Reception==

===Critical response===
On Público (Portugal)|Público, Vasco Câmara gave the film a rating of one out of five stars,  Jorge Mourinha gave it three out of five,  and Luís Miguel Oliveira gave it two out of five. 

===Accolades===
{| class="wikitable sortable" width="90%"
|- style="background:#ccc; text-align:center;"
! Award
! Date
! Category
! Recipients and  nominees
! Result
|-
| rowspan="6"| Sophia Awards October 6, 2013  
| Best Supporting Actor Nuno Melo
|  
|-
| Best Original Screenplay
| Rodrigo Areias
|  
|-
| Best Director
| Rodrigo Areias
|  
|-
| Best Wardrobe
| Susana Abreu
|  
|-
| Best Film Editing
| Tomás Baltazar
|  
|-
| Best Music
| The Legendary Tigerman and Rita Redshoes
|  
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 