Pigen og greven
{{Infobox Film
| name           = Pigen og greven
| image          = 
| image size     = 
| caption        = 
| director       = Finn Henriksen
| producer       = Dirch Passer Henrik Sandberg
| writer         = Finn Henriksen Carl Ottosen
| narrator       = 
| starring       = Dirch Passer
| music          = 
| cinematography = Henning Kristiansen
| editing        = 
| distributor    = 
| released       = 25 November 1966
| runtime        = 96 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Pigen og greven is a 1966 Danish family film directed by Finn Henriksen and starring Dirch Passer. 

==Cast==
* Dirch Passer - Andreas Lillelys
* Josephine Passer - Lulu Hansen
* Lene Tiemroth - Susanne Sus Hansen
* Peter Steen - Grev Ditmar Gyldenstjerne
* Karin Nellemose - Grevinde Constance Gyldenstjerne
* Malene Schwartz - Irene Gyldenstjerne
* Cleo Jensen - Doris
* Karl Stegger - Søren
* Ove Sprogøe - Nielsen
* Sigrid Horne-Rasmussen - Kokkepigen Putte
* Carl Ottosen - Godsforvalter Lauritsen
* Paul Hagen - Bastian Gyldenstjerne
* Preben Mahrt - Theodor Gyldenstjerne
* Preben Kaas - Skuespiller
* Poul Bundgaard - Skuespiller
* Daimi - Skuespiller
* Bjørn Spiro - Sælger af mejetærsker

==External links==
* 

 
 
 
 
 