Tokyo Nagarathile Viseshangal
{{Infobox film 
| name           = Tokyo Nagarathile Viseshangal
| image          =
| caption        =
| director       = Jose Thomas
| producer       =
| writer         = Udaykrishna Sibi K Thomas Udaykrishna Sibi K Thomas
| screenplay     = Udaykrishna Sibi K Thomas Mukesh Prem Kumar Tony
| music          = S. P. Venkatesh Sunny Stephen Violin Jacob
| cinematography = Venugopal
| editing        = K Rajagopal
| studio         = Akai Films
| distributor    = Akai Films
| released       =  
| country        = India Malayalam
}}
 1999 Cinema Indian Malayalam Malayalam film,  directed by Jose Thomas and produced by . The film stars Jagathy Sreekumar, Mukesh (actor)|Mukesh, Prem Kumar and Tony in lead roles. The film had musical score by S. P. Venkatesh, Sunny Stephen and Violin Jacob.   

==Cast==
 
*Jagathy Sreekumar Mukesh
*Prem Kumar
*Tony
*Bindu Panicker
*C. I. Paul
*Harishree Ashokan
*Maathu
*Oduvil Unnikrishnan
*Priyanka
*Salim Kumar Vijayaraghavan
 

==Soundtrack==
The music was composed by S. P. Venkatesh, Sunny Stephen and Violin Jacob and lyrics was written by Balu Kiriyath, Gireesh Puthenchery and . 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chenkalcheriye Unnam Vacha || KG Markose || Balu Kiriyath || 
|-
| 2 || Enthininnum || Madhu Balakrishnan, H Rajesh || Gireesh Puthenchery || 
|-
| 3 || Minnaaminni Ponnum Muthe || KS Chithra, KG Markose || Gireesh Puthenchery || 
|-
| 4 || Sur Barsaaye Theri Sarod Se || Madhu Balakrishnan, H Rajesh || Gireesh Puthenchery || 
|-
| 5 || Swarnameda || Biju Narayanan || Gireesh Puthenchery || 
|-
| 6 || Thrilling Incidents (Theme) ||  ||  || 
|-
| 7 || Venpraavukal || KG Markose, Radhika Thilak || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 

 