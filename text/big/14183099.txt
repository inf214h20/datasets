Yaariyan (2008 film)

 

{{Infobox film
| name = Yaariyan
| image = Yaariyan poster.jpg
| caption = Theatrical poster
| director = Deepak Grewal
| producer = Pinky Basrao
| cinematography = Kapil K.Gautam
| starring = Gurdas Mann Bhumika Chawla Om Puri Gulshan Grover Asrani
| music = Onkar  Aadesh Shrivastava Sachin Ahuja Jaidev Kumar
| distributor = Pinky Basrao Films
| released  =  
| runtime =
| country = India Punjabi
}} Punjabi film, produced by Pinky Basrao starring Gurdas Maan as the male lead and Bhumika Chawla playing his love interest. Directed by Deepak Grewal, the film also stars Om Puri and Gulshan Grover who have their roots in Punjab, India|Punjab. There is also a special appearance by Asrani.

==Music== Universal .
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Lyrics
|-
|Yaariyan
| Gurdas Mann & Sukhwinder Singh
|
|-
|Wajda Tunak Tunak Ik Tara
| Gurdas Mann
|
|-
| Bada Kuchh Kehna Hai
| Sonu Nigam & Tarannum
|
|-
|Watno Door Lage Ne Mele
| Gurdas Mann & Feroz Khan
|
|-
|Ki Aye Kise Da Kasoor
| Sadhna Sargam
|
|-
| Cmon Cmon (bonus Track)
| Sonu Nigam & Alisha Chinoy
|
|-
| Yaariyan (remix)
| Gurdas Mann & Sukhwinder Singh
|
|-
| Time Chakna
|
|
|-
|}

==External links==
*  

 
 
 


 