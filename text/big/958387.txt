Final Justice (1984 film)
{{Infobox film
| name = Final Justice
| image = Final Justice.jpg
| caption = Poster.
| producer = Greydon Clark
| director = Greydon Clark
| writer = Greydon Clark
| starring = Joe Don Baker Rossano Brazzi Venantino Venantini Patrizia Pellegrino Bill McKinney Helena Abella Lino Grech Tony Ellul David Bell
| cinematography = Nicholas Josef von Sternberg
| editing = Larry Bock
| distributor = Arista Films
| released = May 1985 (USA)
| runtime = 90 min. English
| budget = Unknown
| gross = Unknown
}} mobster who killed his partner. The film was lampooned in a 1999 episode of Mystery Science Theater 3000. Now infamous for its poor quality, it is among the lowest rated movies on IMDb. 

==Plot==

Joe Don Baker plays Texas sheriff Thomas Jefferson Geronimo III. His partner, the former sheriff, is killed by an Italian mobster. He finds the criminal, a man named Joseph Palermo, and escorts him back to Europe, only to lose him in the capital city of Valletta in Malta. He rejects the assistance of local law enforcement and pursues Palermo himself. A cat-and-mouse game follows, with car chases, gunfights, fistfights and boat chases.

==Mystery Science Theater 3000==

The film was featured in the eighth episode of MST3Ks tenth and final season. It is the second Joe Don Baker film to be riffed on MST3K, following Mitchell (film)|Mitchell, to which a few references were made in this episode.
 editing mistake. Early in the film when the sheriff (played by director Greydon Clark) dies, there is a shot of the partner getting killed and collapsing to the ground. Moments later, the exact shot is repeated. This error only appears in a television print, and the original Vestron release does not contain this mistake.

== References == 
 

==External links==
*  

===Mystery Science Theater 3000===
*  
*  

 

 
 
 
 
 
 


 