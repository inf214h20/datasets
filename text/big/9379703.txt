My Sassy Girl (2008 film)
{{Infobox film
| name = My Sassy Girl
| image = Sassygirl.jpg
| image_size =
| caption = Film poster
| director = Yann Samuell
| producer = Paul Brooks Mark Morgan Roy Lee
| writer = Victor Levin
| narrator =
| starring = Elisha Cuthbert Jesse Bradford
| music = David Kitay
| cinematography =
| editing = Anita Brandt Burgoyne Fox Home Entertainment
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget = 
| gross =
}}
 remake of the 2001 South Korean romantic comedy film|romantic-comedy My Sassy Girl. It stars Elisha Cuthbert and Jesse Bradford and was directed by Yann Samuell. Both movies are based on a true story told in a series of blog posts written by Kim Ho-sik, who later adapted them into a fictional novel.
 Entertainment in Video. 

==Plot==
Charlie Bellow, a polite, kind-hearted young man, hopes he will one day secure a managerial position with the Tiller King agricultural company, where his father works as a maintenance mechanic. When he starts business school in New York City, he hears news that his cousin has suddenly died. His friend suggests that they play a game in which they say whether they would sleep with any of the women they see while sitting in the park, which is where Charlie sees Jordan for the first time, but leaves to meet his grieving aunt who wants to set him up with someone. On the subway platform, Charlie sees Jordan drunkenly leaning over the guard rail and saves her from being hit by an oncoming train. Taking responsibility for the drunken, blacked-out girl, he sobers her up and begins a relationship with her over several weeks. While becoming fascinated with Jordan, he soon comes face-to-face with her volatile personality and learns more about her past through wild, drunken dates. Among other things, Jordan tells Charlie that her fiancé recently left her, tells a Tiller King representative she is pregnant with Charlies child, sabotages Charlies job interview with another Tiller King representative, and gives a piano recital where she asks him to bring her a single red rose and pushes him into the sea – and then saves him. Despite this, Charlie begins to fall in love with Jordan but clashes with her father, who believes Charlie is responsible for her erratic behavior. Because of this, Jordan begins to drift away from Charlie.

A few months later, Jordan asks Charlie to meet her in Central Park to exchange unopened love letters to bury in a time capsule, and to meet at the same place on the same day of the following year to read them. After parting ways at a train station, Charlie spends the next year preparing for when they will meet again but when the day arrives, Jordan does not meet him at the tree. Reading her letter, he learns that on the day they met, her fiancé suddenly committed suicide with little explanation and that he reminded Jordan of him. The letter further explains that Charlie and Jordans time together was a prolonged reenactment of her relationship with her fiancé. She writes that her absence at the tree means she has not yet healed from her loss, but it does not mean that she doesnt love him. Charlies letter in turn tells Jordan she is the only woman he will ever love and says that he believes he is destined to be with her.

Some time later, Jordan meets with her ex-fiancés mother at a restaurant, who she remained close with after his death. The mother explains that she has been trying to set Jordan up with another young man for quite some time, and she has arranged for the two to meet today. As she begins to describe him, Charlie walks into the restaurant, revealing that Charlies cousin was Jordans fiancé. Charlie and Jordan share their first kiss, and he narrates that we all need to help destiny in shaping our lives.

==Synopsis==
The film is set in New York Citys Central Park and Upper East Side. Samuell, the director, states "Its a fable about destiny, in the end." whilst Bradford, who plays the protagonist, described the movie as "a romantic comedy about how they pull each other to a more healthy place by virtue of their relationship."  
 

==Cast==
* Elisha Cuthbert as Jordan Roark, a wealthy, fun-loving girl who lives in New York City
* Jesse Bradford as Charlie Bellow, a business-school student from Indiana
* Austin Basis as Leo, Charlies friend
* Chris Sarandon as Dr. Roark, Jordans father. Dr. Roark disapproves of Jordans relationship with Charlie.
* Jay Patterson as Roger Bellow
* Tom Aldredge as the old man in the park. He helps Jordan sort out her feelings toward Charlie.
* Louis Mustillo as Jordans doorman. He carries Jordan into her apartment when Charlie brings her home drunk.
* Brian Reddy as Mr. Phipps
* Stark Sands as Soldier
* Joanna Gleason as Kitty / Aunt Sally
* Jeremiah Sird as New Yorker

==Soundtrack listing==
 
 
* "Im in Love (Ella Leya song)|Im in Love" by Ella Leya
* "Yadnus" by !!! Phobos
* "Thought About You" by The Beautiful Girls
* "Hip Joint Vs. Pachelbels Canon" by Hipjoint
* "I Can Always Dream" by Beverly Valera
 
* "Shaken Not Stirred" by John Gintz
* "Contradictions" by Nick Howard Daniel May
* "Put A Record On" by Unkle Bob
* "Feel Good About It" by Marching Band
* "Occams Razor" by Ocha la Rocha
 

==See also==
* 2008 in film
* Ugly Aur Pagli

==References==
* http://www.gazette.net/stories/08282008/enteres115509_32474.shtml
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 