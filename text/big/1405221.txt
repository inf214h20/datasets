The Rules of Attraction (film)
 
{{Infobox film
| name = The Rules of Attraction
| image = rulesofa.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster
| director = Roger Avary
| producer = Roger Avary Greg Shapiro
| screenplay = Roger Avary
| based on =  
| starring = James van der Beek Shannyn Sossamon Ian Somerhalder Jessica Biel Kip Pardue
| music = tomandandy
| cinematography = Robert Brinkmann
| editing = Sharon Rutter Roger Avary Filmproduktion Lions Gate Films
| released =  
| runtime = 110 minutes  
| country = United States Germany
| language = English German
| budget = $4 million 
| gross = $11,819,244 
}} the novel of the same name by Bret Easton Ellis. It stars James van der Beek, Shannyn Sossamon, Ian Somerhalder, Jessica Biel, and Kip Pardue.

==Plot== townie while bashed when it turns out he is deeply closeted; additionally, a bruised Sean tears up a purple letter, before approaching and having sex with a blonde girl at the party.

The plot then moves backwards several months and explores the love triangle between Lauren, Paul, and Sean. Misinterpreting Seans friendliness, Paul mistakes him for a homosexual and makes several advances that Sean is apparently oblivious to. Concurrently, Lauren also finds herself attracted to Sean despite saving her virginity for her traveling boyfriend, Victor. Sean reciprocates her feelings, and assumes the anonymous, purple love letters he has started receiving are from her.

While Paul is visiting his friend Dick, Sean is seduced by Laurens roommate Lara, who tells him that Lauren isnt interested in him because she has a boyfriend; despite having sex with Lara, however, Sean regrets it and realizes that he is in love with Lauren. It is then revealed that another, unnamed girl is the author of Seans love letters; after seeing him leave the party with Lara, she sends him a suicide note before cutting her wrists in the dorm bathtub. Lauren, finding Sean with Lara, runs to the girls bathroom in anger, only to find the unnamed girls corpse, leaving Lauren extremely distressed. Sean, still believing Lauren wrote the purple letters, misinterprets the suicide note and assumes Lauren never wants to be with him.

After numerous failed attempts at suicide, Sean then fakes his death and, unaware that Lauren recently found a corpse, unintentionally upsets her further when she finds him pretending to be dead. After stealing drugs from dealer Rupert, Sean tries to speak to Lauren again, only for her to brush him off angrily. Finally happy now that Victor has returned to Camden College, Lauren is completely distraught to find Victor has no idea who she is.

Paul, upon finding a drunk Sean, tries to talk to him, only for Sean to reject him, causing Paul to run off upset. Sean then finds that the love notes have stopped, and is beaten by Rupert and Ruperts Jamaican partner, Guest. The protagonists then attend the "End of the World" party and the plot returns to the introduction. After seeing Lauren heading upstairs with the filmmaker, Sean finally accepts he cannot be with her, and tears up one of the purple letters he believes to be from her. Its then revealed that, rather than having sex with the blonde girl as he does in the intro, Sean was merely fantasizing, and he instead leaves his drink and exits. Paul and Lauren then meet on the house porch and reflect on the events of the movie and on Sean, who is seen leaving on a motorcycle. Sean then begins narrating his thoughts, only for them to end prematurely.

==Cast==
* James van der Beek as Sean Bateman, a drug dealer. The character is in fact the younger brother of Patrick Bateman, the antihero of American Psycho.
* Shannyn Sossamon as Lauren Hynde, a virgin who is saving herself for Victor, her ex-boyfriend, who is traveling through Europe. She develops feelings for Sean, which dissipate when she discovers him in bed with her roommate. Eventually she is raped by a "town and gown|townie" at a party.
* Ian Somerhalder as Paul Denton, Laurens gay ex-boyfriend. He develops a sexual attraction to Sean, who eventually rejects him.
* Jessica Biel as Lara Holleran, Laurens promiscuous roommate.
* Kip Pardue as Victor Johnson, Laurens promiscuous ex-boyfriend.
* Clifton Collins, Jr. as Rupert Guest, a high-strung drug dealer who is owed a large debt by Sean.
* Thomas Ian Nicholas as Mitchell Allen, a weaselly cohort who seems to idolize brutish Victor. fuck buddy of Pauls. His mother is a friend of Pauls mother.
* Faye Dunaway as Mrs. Eve Denton, Pauls mother.
* Eric Stoltz as Mr. Lance Lawson, a college teacher who tries to seduce Lauren.
* Fred Savage as Marc, a heroin-addicted student who owes Sean money for drugs.
* Theresa Wayman as "Food Service Girl", an unnamed character who commits suicide after writing Sean love notes that he mistakenly believes come from Lauren.
* Kate Bosworth as Kelly, a girl from the party whom Sean takes back to his room.
* Jay Baruchel as Harry, a French exchange student and friend of Paul.
* Joel Michaely as Raymond
* Sara Dallin as herself
* Keren Woodward as herself
* Clare Kramer as Candice
* Swoosie Kurtz as Mimi Jared, Richards uptight mother Ron Jeremy Hyatt as the piano player Paul Williams as doctor

==Production==
The film was shot at the University of Redlands in California. 
 24fps could be achieved with an Commercial off-the-shelf|off-the-shelf product. Roger Avary, the films director, became a spokesperson for FCP, appearing in print ads worldwide.  

==Music== Love and The Rapture, Milla Jovovich, Der Wolf, and Serge Gainsbourg.

==Releases and versions== ratings in the United States|U.S. and other areas.
 Lions Gate R rating, sexual content, drug use, Profanity|language, and violent images".

The Australian version of the film is uncut, retaining 22 seconds that were removed in the R-rated US version. 
 Special Edition entitled Les Lois de LAttraction is the longest known version available.  It contains a small number of scenes not shown in the US and UK DVDs and also includes more footage of the suicide scene (including the girl actually cutting into her wrists, instead of just seeing her reaction). It also includes more content in commentary tracks than the other DVDs available.

The uncut version was shown at UK cinemas. However, the British Board of Film Classification|BBFC, under its power as censor under the Video Recordings Act 1984, shortened the suicide scene, even at the highest (18+) rating. 

==Reception==

===Critical reception===
The Rules of Attraction received mixed reviews—getting 44% "rotten" reviews from   of collegiate craving...Sex, drugs and rack n ruin; pretty people doing nasty things to one another...honestly, what more could you want in a movie?". 

===Authors reception=== Less Than Zero didnt." 

===Box office===
The film grossed $11,819,244 worldwide on a budget of $4 million, thus making the film a minor box office success.   

===Awards=== The Hours.

===Cult following===
Though the film has inspired mixed critical reaction, it has become something of a cult classic, which was covered by The A.V. Club for their "New Cult Canon" feature.  In 2012 Entertainment Weekly cited the film as one of the "50 Best Movies Youve Never Seen".   In an April 2009 interview, author Ellis stated that the film adaptation of The Rules of Attraction came closest of all the movies based on his books to capturing his sensibility and recreating the world he created in his novels.  On Friday April 24, 2009, the film was shown on Film4 as part of the "Great Adaptations" series. 

==Home media== trailers and an audio commentary by Carrot Top, despite having nothing to do with the making of the film. He often comments on the attractiveness of each actress, begs Eric Stoltz for work every time he is on screen, and even occasionally sings along with the songs in the film, all the while making a number of Self-deprecation|self-deprecating jokes. The DVD also features other commentaries.

==See also==
* Less Than Zero (novel)|Less Than Zero (novel)
* Less Than Zero (film)|Less Than Zero (film)
* American Psycho|American Psycho (novel)
* American Psycho (film)|American Psycho (film)
* Glamorama
* Glitterati (film)|Glitterati

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 