Dark Delusion
{{Infobox film
| name           = Dark Delusion
| image	         =
| caption        =
| director       = Willis Goldbeck Carey Wilson
| writer         = Jack Andrews Harry Ruskin Max Brand (characters)
| based on       = James Craig Lionel Barrymore Lucille Bremer David Snell
| cinematography = Charles Rosher
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States English
| budget         = $875,000  . 
| gross          = $718,000 
}} James Craig, Lionel Barrymore, and Lucille Bremer. The film was the last in the Dr. Kildare film series released by MGM.

==Plot==
Dr. Gillespie (Barrymore) asks a young surgeon, Dr. Tommy Coalt (Craig), to go to the small town of Bayhurst to replace a local doctor while he is on assignment to the Occupation effort in post-WWII Europe. There, Coalt is asked to sign mental-health commitment papers on a beautiful young socialite, Cynthia Grace (Bremer). Coalt thinks there is something amiss, and begins his own investigation. 

==Reception==
According to MGM records, the movie was not a hit, earning $475,000 in the US and Canada and $243,000 elsewhere, making a loss to the studio of $448,000. 

==References==
 

==External links==
*  at TCMDB
* 

 
 
 
 
 
 
 
 

 

 
 