Wacky Blackout
{{Infobox Hollywood cartoon
| series = Looney Tunes Robert Clampett
| producer = Leon Schlesinger
| story artist = Warren Foster
| narrator = Mel Blanc
| voice actor = Mel Blanc Sara Berner Kent Rogers (all uncredited)
| musician = Carl Stalling Milt Franklyn (orchestrator) (uncredited)
| animator = Syd Sutherland Rod Scribner (uncredited)
| studio = Leon Schlesinger Studios Warner Bros. Pictures, Inc.
| release date = July 11, 1942 (USA)
| color process = Black-and-white
| runtime = 8 minutes
| language = English
| proceeded by = none
| succeeded by = A Tale of Two Kitties
}} 1942 Warner Bros. cartoon in the Looney Tunes series. It was directed by Bob Clampett, animated by Syd Sutherland and an uncredited Rod Scribner, and musical direction by Carl Stalling.

==Plot synopsis== the war. We see a farmer trying to put out a fire, and the narrator says that the farmer has trained his dog to put out fires. The narrator says that the dog is a "full-blooded spitz", as the dog puts out the fires by spitting.
*The narrator then says that a cow has increased her production and she has given 5000 quarts of milk a day. The cow (voiced by Sara Berner) then says that they come in and take it from her, and cries. Meanwhile, a baby cow says, "What a performance.".
*A turkey is eating food, and the narrator says that when the turkey reaches 20 pounds, they put him in the oven. The turkey then hears that, and goes to an exercising machine, reading a book on how to lose weight in 18 days. the Road Runners beeping.
*Back on the farm, a dog tries to ask his lover, Marie-Alana, something. The dog wishes there was a blackout. He sees a lightswitch, and yells "BLACKOUT!". After a short blackout, the dog has lipstick stains on his face, and he turns off the lights again, yelling "BLACKOUT!".
*Glum caterpillars appear, as the narrator has no idea why they are glum. Just then, a happy caterpillar comes crawling on-screen. The caterpillar says that hes happy because he just got a retread. He laughs, and rolls off.
*The narrator says that fireflies are gonna stage a practice blackout. The fireflies then turn off their lights. Meanwhile, the narrator tells a turtle to go into his shell because its a blackout, but the turtle doesnt want to. The turtle then climbs into his shell, and the narrator asks him why he didnt want to go into his shell. The turtle then says that hes afraid of the dark. The narrator tells the fireflies that the blackout is over, and all the fireflies turn their lights back on... except for one. The firefly notices and asks the others who stole his bulb, and the one behind him gives his bulb back.
*A mother bird is teaching her son how to fly. After she shows him an example, the bird says that he wants to be a dive bomber. The bird flies around, mimicking a plane engine, and the mother bird shrugs to the audience. The mother birds son would be the appearance of Tweety in another Clampett cartoon, A Tale of Two Kitties.
*The narrator says that the only living things to not be affected by the war are the famous swallows of Capistrano. The narrator also says that the birds return to a mission on a certain day each year. He also says that they are just in time for their return. Just then, a delivery person comes and says that there is a telegram for the audience. The message says: "We are out over the ocean. We cant even get close to land. We cant fly to Capistrano, past the Fourth Interceptor Command.  Signed, the Swallows".
*Elderly carrier pigeons are in their home. The narrator says that they give their sons to the service during each war. Pa says, "Well, Ma...", then the two pigeons start singing "We did it before and we can do it again". Then, they watch their sons fly in the sky, heading to war.
*A running gag in this cartoon is a woodpecker (voiced by Kent Rogers, doing a Red Skelton voice) pecking a cat named Old Toms tail. In gag 5, he says that he pecked the cats tail again. In the final gag, Old Tom says that he ate the woodpecker, and the woodpecker pecks through his stomach.

==Cast==
*Mel Blanc as the Narrator, Baby Cow, Turkey, Turtle, Caterpillar, Firefly, Baby Bird, Delivery Person, Pa Pigeon, Old Tom
*Sara Berner as the Cow, Mother Bird, Ma Pigeon
*Kent Rogers as the Dog, the Woodpecker, the Turtle

==Crew== Robert Clampett
*Producer: Leon Schlesinger
*Writer: Warren Foster Carl W. Stalling
*Orchestrator: Milt Franklyn
*Film Editor: Treg Brown
*Sound Effects: Treg Brown
*Animation: Sidney Sutherland and Rod Scribner

==See also==
* Looney Tunes and Merrie Melodies filmography (1940-1949)

==External links==
*  

 
 
 
 
 
 
 
 
 