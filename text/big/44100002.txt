Nishane Baazi
 
{{Infobox film
| name           =Nishaane Baaz
| image          = 
| image_size     = 
| caption        = 
| director       = V N Menon	 
| producer       = Mohan T Gehani
| writer         =
| narrator       = 
| starring       =Sumeet Saigal Sriprada
| music          = Ravindra Jain
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1989
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

 Nishaane Baaz  is a 1989 Bollywood film directed by V N Menon and starring Sumeet Saigal, Sriprada, Shakti Kapoor, Kader Khan and Anupam Kher. The movie was released on 1st Jan 1989.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hum to banjaare hai"
| Anuradha Paudwal Suresh Wadkar
|}


 
 
 

 