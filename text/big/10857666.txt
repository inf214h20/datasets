Raga (film)
 
 
{{Infobox film name           = Raga image          = Raga 1971 UK film poster.jpg image size          = 185px caption        = director       =Howard Worth producer       =Howard Worth writer         = Nancy Bacal starring       = Ravi Shankar Allauddin Khan Yehudi Menuhin George Harrison Alla Rakha Kamala Chakravarty Lakshmi Shankar music          =Ravi Shankar cinematography = James Allen editing        = Merle Worth studio         =Apple Films distributor    =Apple Films released       =23 November 1971 runtime        =96 mins country        =United States language       = English, Hindi budget         = preceded_by    = followed_by    =
}}
Raga is a 1971 documentary film about the life and music of Indian sitarist Ravi Shankar, produced and directed by Howard Worth. It includes scenes featuring Western musicians Yehudi Menuhin and George Harrison, as well as footage of Shankar returning to Maihar in central India, where as a young man he trained under the mentorship of Allauddin Khan. The film also features a portion of Shankar and tabla player Alla Rakhas acclaimed performance at the 1967 Monterey Pop Festival.

The majority of the documentary was shot in the late 1960s, during a period when Shankars growing popularity saw Indian classical music embraced by rock and pop musicians and their audiences. Financial problems then delayed production until Harrison provided assistance through the Beatles company Apple Films. In addition to actively promoting Raga, Harrison produced the soundtrack album – a project that led directly to he and Shankar staging the Concert for Bangladesh in August 1971.

The films working title was alternately East Meets West and Messenger Out of the East. In 2010, to coincide with celebrations for Shankars 90th birthday, East Meets West Music released a fully remastered version on DVD, titled Raga: A Film Journey into the Soul of India. The expanded soundtrack album was also made available, via digital download.

==Production==
 
New York film-maker   of the Beatles,  this phenomenon resulted in Shankar achieving  , 9 March 1968 (retrieved 15 December 2013).  

Speaking in 2010 of his involvement in Raga, Worth recalled that he disliked Indian music initially, but soon changed his view.  At the request of Canadian television producer Nancy Bacal, he attended a private recital by Shankar, in the company of singers Judy Collins and Leonard Cohen, a performance that convinced Worth that he wanted to direct the planned Shankar documentary after all.   (retrieved 1 November 2013).  Worth also served as producer, Castleman & Podrazik, p. 320.  and he and Bacal worked on a script at Collinss house in California. 

The film was originally called East Meets West, according to author Peter Lavezzoli; Lavezzoli, p. 184.  Messenger Out of the East was an alternative working title. Clayson, p. 308.  The first of these titles referenced West Meets East, Shankars 1966 album with American violinist Yehudi Menuhin, and the winner of the 1967 Grammy Award for Best Chamber Music Performance. 
 

===Filming===
 
Much of Raga was shot during the first half of 1968 in India, particularly Bombay,  home to Shankars Kinnara School of Music since 1963.  Among the scenes filmed in India, Shankar directs musicians such as Shivkumar Sharma, Hariprasad Chaurasia and Kartick Kumar in a Bombay studio and, in a scene titled "Vinus House", enjoys a casual musical get-together with singer Vinay Bharat Ram and violinist Satyadev Pawar. 

Early in the film, Shankar travels by train to the   tradition,  reflecting Shankars early career as a dancer with elder brother   (retrieved 24 November 2013).   , East Meets West Music (retrieved 1 November 2013). 

According to Worth, the emotional highpoint of filming was when Shankar visited his spiritual guru,  named Tat Baba.   In his own teaching activities, Shankar is shown mentoring students at Kinnara,  adhering to the strict Guru-shishya tradition|guru-shishya tradition he had experienced under Allauddin Khan.  Shankar later reflects on the comparative rush to master the intricacies of Indian music by his Western students in Los Angeles,  where he opened a branch of the Kinnara School in May 1967. 
 United Nations building in New York. Lavezzoli, pp. 7–8.  

Another milestone for the popularity of Indian music was the June 1967 release of the Beatles  ".  Harrison joined Shankar in  , 9 December 2001 (retrieved 1 December 2013).  Harrison would later cite this visit to Esalen as presaging the end of his commitment to the sitar.   

California was also the location for the films penultimate scene, in which Shankar, looking out over a windswept beach, questions the validity of his attempts to bring Indian culture to America. Shankar, Raga Mala, pp. 210–11.  In his narration for the scene, he reads out a passage adapted from My Music, My Life, reaffirming his belief in Nada Brahma. 

===Apple Films involvement=== Raga Mala, Shankar says that he financed the film himself, adding: "which was rather sad because it cost a large amount and I only realised this later!" Shankar, Raga Mala, p. 210. 
 A Hard Yellow Submarine.  Late in 1970,  Harrison attended a special screening of the assembled footage and was so moved, according to Worth, that within days he offered the services of the Beatles own Apple Films as a distributor.  Worth credits Harrison with saving the production and thereby "chang  my life". 

==Soundtrack==
{{Infobox album|  
| Name = Raga
| Type = soundtrack
| Artist = Ravi Shankar
| Cover = Raga1971album.jpg
| Released = 7 December 1971 (US)
| Recorded = April–July 1968; June–July 1971 Hindustani classical
| Length = 39:34 Apple
| Producer = George Harrison
| Last album  = 
| This album  = 
| Next album  = 
| Misc        =
}} Zakir Hussain (both tabla), and singer Jitendra Abhisheki.  For a scene that Shankar describes in Raga Mala as "reflect  all the distortions in that period – Indian music mixed up with rock, hippies and drugs", Walcott created a piece titled "Frenzy and Distortion", using "a profusion of electronic sounds". 
 atrocities being Concert for Bangladesh, held at Madison Square Garden, New York, on 1 August.   Work on the Raga soundtrack was completed in mid July,  around the time of sessions for Shankars Apple Records EP Joi Bangla. 

==Release== The David Howard Thompson described Raga as "quietly penetrating" and "beautifully made", adding: "Everything about it is admirable." 

On 7 December 1971, Apple Records released the soundtrack album (as Apple SWAO 3384) – like the film, in America only.  Billboard (magazine)|Billboard s album reviewer commented on the packagings "superb photo folio showing the sitarists career" but suggested that, due to the fact that only portions of ragas were present, the soundtracks "greatest attractiveness may be to those who see the movie or are Shankar collectors".  In August 1972, Harrison screened the film for select guests at a cinema in Mayfair, London, to coincide with Shankars upcoming appearance at Southwark Cathedral. 

==Reissue==
Raga was released on home video in 1991, distributed by Mystic Fire Video. Madinger & Easter, p. 422.  The Shankar-affiliated East Meets West Music (EMWMusic) remastered the film and released it on DVD in October 2010, as part of the celebrations for his 90th birthday. 

On 1 November 2010, the film was screened at the New York headquarters of the  , 1 November 2011 (retrieved 12 August 2014).  which had promoted Shankars first US appearances in 1957  and now honoured the artist with its Cultural Legacy Award. The event was introduced by composer Philip Glass and attended by Anoushka Shankar (representing her father, who was too sick to attend),  along with people involved in the original production such as Worth, Gary Haber and Merle Worth.  Writing in Songlines (magazine)|Songlines magazine, Jeff Kaliss gave the Raga DVD a five-star review and described the film as an "honest, entertaining portrait of a maestro" that was "  satisfying musically as it is visually". 

Replacing the 1971 promotional image for Raga, which showed a silhouette of a cow against a backdrop of a sunset, the new cover consisted of a still of Shankar playing sitar during the 1960s.  This photo, taken by Canadian portrait photographer   that Shankar had popularised over the more traditional six-string model favoured by musicians such as Vilayat Khan.  

For the 2010 reissue, EMWMusic expanded the soundtrack album from thirteen selections  to seventeen, with all recordings fully remastered.  The Raga soundtrack remains available via digital download with the documentary film. 

==Album track listing==

===Original 1971 release===

All songs by Ravi Shankar, except where noted.

Side one
# "Dawn to Dusk" – 3:38
# "Vedic Hymns" (PD) – 1:30
# "Baba Teaching" (PD) – 1:08
# "Birth to Death" – 3:10
# "Vinus House" – 2:37
# "Guru Bramha" – 1:10
# "United Nations" (Shankar, Yehudi Menuhin) – 4:33

Side two
#  "Medley: Raga Parameshwari / Rangeswhari" – 4:31
# "Banaras Ghat" – 2:45
# "Bombay Studio" – 1:51
# "Kinnara School" – 1:51
# "Frenzy and Distortion" – 1:06
# "Raga Desh" –- 8:50

===2010 digital download version===

# "East/West Introductions" – 3:08
# "Dawn to Dusk" – 3:41
# "Vedic Hymns" – 1:36
# "Baba Teaching" – 1:17
# "Birth to Death" – 3:15
# "Vinus House" – 2:41
# "Gurur Bramha" – 1:15
# "United Nations" – 4:41
# "Medley: Raga Parameshwari / Raga Rangeshwari" – 2:54
# "Banaras Ghat" – 1:49
# "Bombay Studio" – 2:48
# "Kinnara School" – 1:34
# "Frenzy and Distortion" – 1:56
# "Raga Desh" – 9:02
# "The Spirit of the Raga" – 2:33
# "What Is a Raga?" – 1:41
# "The Seriousness of It" – 3:15

 

==Notes==
 

==Citations==
 

==Sources==
 
* Keith Badman, The Beatles Diary Volume 2: After the Break-Up 1970–2001, Omnibus Press (London, 2001; ISBN 0-7119-8307-0).
* Harry Castleman & Walter J. Podrazik, All Together Now: The First Complete Beatles Discography 1961–1975, Ballantine Books (New York, NY, 1976; ISBN 0-345-25680-8).
* Alan Clayson, George Harrison, Sanctuary (London, 2003; ISBN 1-86074-489-3).
* George Harrison, I Me Mine, Chronicle Books (San Francisco, CA, 2002; ISBN 0-8118-3793-9).
* Olivia Harrison, George Harrison: Living in the Material World, Abrams (New York, NY, 2011; ISBN 978-1-4197-0220-4).
* Peter Lavezzoli, The Dawn of Indian Music in the West, Continuum (New York, NY, 2006; ISBN 0-8264-2819-3).
* Simon Leng, While My Guitar Gently Weeps: The Music of George Harrison, Hal Leonard (Milwaukee, WI, 2006; ISBN 1-4234-0609-5).
* Chip Madinger & Mark Easter, Eight Arms to Hold You: The Solo Beatles Compendium, 44.1 Productions (Chesterfield, MO, 2000; ISBN 0-615-11724-4).
* Barry Miles, The Beatles Diary Volume 1: The Beatles Years, Omnibus Press (London, 2001; ISBN 0-7119-8308-9). East Meets West/Apple Films, 2010 (produced and directed by Howard Worth; reissue produced by Shyama Priya & Cat Celebrezze).
* Robert Rodriguez, Fab Four FAQ 2.0: The Beatles Solo Years, 1970–1980, Backbeat Books (Milwaukee, WI, 2010; ISBN 978-1-4165-9093-4).
* Ravi Shankar, My Music, My Life, Mandala Publishing (San Rafael, CA, 2007; ISBN 978-1-60109-005-8).
* Ravi Shankar, Raga Mala: The Autobiography of Ravi Shankar, Welcome Rain (New York, NY, 1999; ISBN 1-56649-104-5).
* Bruce Spizer, The Beatles Solo on Apple Records, 498 Productions (New Orleans, LA, 2005; ISBN 0-9662649-5-9).
* World Music: The Rough Guide (Volume 2: Latin and North America, Caribbean, India, Asia and Pacific), Rough Guides/Penguin (London, 2000; ISBN 1-85828-636-0).
 

==External links==
*  
*  

{{navboxes
|title=Navigation boxes related to Ravi Shankar
|state=collapsed
|list1=
 
 
 
 
 
 
 
 
 
{{Navbox
|name=Khan family title = Khan family listclass = hlist
|liststyle=background:transparent;
 group1 = 1st generation list1  = 
* Allauddin Khan
* Ayet Ali Khan
 group2 = 2nd generation list2  = 
* Ali Akbar Khan
* Annapurna Devi
* Ravi Shankar
* Raja Hossain Khan
* Bahadur Khan
* Mobarak Hossain Khan
 group3 = 3rd generation list3  =  Aashish Khan Debsharma
* Bidyut Khan
* Kirit Khan
* Shahadat Hossain Khan
* Shubhendra Shankar
* Reenat Fauzia

}} 

}}
 

 
 
 
 
 
 
 
  
 
 
 
 