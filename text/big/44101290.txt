Oru Naal Innoru Naal
{{Infobox film 
| name           = Oru Naal Innoru Naal
| image          =
| caption        =
| director       = TS Suresh Babu
| producer       = Sreevigneswara Filims
| writer         = A. Sheriff
| screenplay     = A. Sheriff
| starring       = Prem Nazir Sukumari Shobhana Nedumudi Venu
| music          = MG Radhakrishnan
| cinematography = C Ramachandra Menon
| editing        = G Murali
| studio         = Sreevigneswara Filims
| distributor    = Sreevigneswara Filims
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film,  directed by TS Suresh Babu and produced by Sreevigneswara Filims. The film stars Prem Nazir, Sukumari, Shobhana and Nedumudi Venu in lead roles. The film had musical score by MG Radhakrishnan.   

==Cast==
 
*Prem Nazir
*Sukumari
*Shobhana
*Nedumudi Venu
*Thikkurissi Sukumaran Nair
*Ratheesh Shankar
*Bheeman Raghu
*Jagadish
*Jagannatha Varma
*Jayaprabha
*K. P. Ummer
*Karamana Janardanan Nair
*Poojappura Ravi
*Santhakumari
*Vettoor Purushan
*Gomathi
 

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Panchavarnnakkili || MG Sreekumar, G Gadha || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 