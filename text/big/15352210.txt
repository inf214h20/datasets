W. (film)
{{For|other films of  s W (disambiguation)#Music and entertainment W (disambiguation)}}
 
 
{{Infobox film
| name           = W.
| image          = w ver4.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Oliver Stone
| producer       = Moritz Borman Jon Kilik Bill Block Paul Hanson Eric Kopeloff
| writer         = Stanley Weiser
| narrator       = Jeffrey Wright Scott Glenn Bruce McGill Jennifer Sipes Noah Wyle Ioan Gruffudd
| music          = Paul Cantelon
| cinematography = Phedon Papamichael Jr.
| editing        = Julie Monroe Joe Hutshing Alexis Chavez
| studio         = Global Entertainment Group QED International Emperor Motion Pictures Millbrook Pictures Onda Entertainment Lionsgate
| released       =  
| runtime        = 129 minutes
| country        = United States

| language       = English
| budget         = $25.1 million
| gross          = $29,506,464 
}} biographical drama presidency of  Jeffrey Wright, Scott Glenn, and Richard Dreyfuss. Filming began on May 12, 2008, in Louisiana and the film was released on October 17.   

== Plot ==
 his family Jeb (Jason Ritter) intervenes and stops the fight.
 Laura Lane Democratic opponent, Republican candidate in the states history. Angered by his loss, Bush declares that he will "never be out-Texasd or out-Christianed again".
 1988 presidential Texas Rangers Persian Gulf 1992 presidential election to Bill Clinton, Bush blames the loss on his decision not to depose Saddam.
 Mission Accomplished" weapons of mass destruction within Iraq, Bush learns that the responsibility for finding them had been relegated far down the chain of command. Bush also discovers that Saddam gambled his regime and his life on the assumption that Bush was bluffing. Bush is asked in a White House press conference what mistakes he made as President, a question that leaves him flustered and speechless. That night, Bush has a nightmare in which his father accuses him of ruining his familys legacy, which the elder Bush claims was intended for Jeb. In the final scene of the film, Bush dreams of playing center field at a baseball game. Bush attempts to catch a pop fly, but it quickly disappears.

== Cast ==
 methodical actor and he has to have the makeup thing work for him."    Brolin, cast at the "last minute",  spent months working on Bushs distinctive vocal style, calling hotels in Texas and talking to the people at the front desk, listening to their accents. The actor also watched videos of Bush walking. Brolin said, "It changes over the years, how he walks in his 30s, how he walks in foreign lands, before September 11 attacks|9/11 and afterward. People hold their emotions in their bodies. They cant fake it. Especially him." 
* Elizabeth Banks as Laura Bush: Banks said she would not do an impression of the First Lady. "I just want to honor her voice, her stillness, and her hairstyle". 
* James Cromwell as George H. W. Bush, Bushs father and the 41st President of the United States.
* Ellen Burstyn as Barbara Bush
*   as Cheney, but Duvall turned down the role.    Dreyfuss and Stone did not get along well during filming, and Dreyfuss was harshly critical of the director during the films press junket (noting Stone had attacked him right before that tour began). Dreyfuss also told Nathan Rabin in an interview that he did not empathize with the former Vice President, calling him "true to himself", and saying that the real villain of W. was the American public, because it never responded with outrage to the Bush Administrations actions. Jeffrey Wright as U.S. Secretary of State Gen. Colin Powell
* Scott Glenn as U.S. Secretary of Defense Donald Rumsfeld
* Thandie Newton as U.S. National Security Advisor Condoleezza Rice
* Toby Jones as Deputy White House Chief of Staff Karl Rove
* Bruce McGill as Central Intelligence Agency Director George Tenet
* Ioan Gruffudd as British Prime Minister Tony Blair.  Much of Gruffudds intended scene was cut. The scene depicted Blair revealing his intentions to convert to Catholicism, and his desire to give the United Nations and British and US allies more time and greater involvement before the Iraq invasion.
* Noah Wyle as U.S. Secretary of Commerce Donald Evans
* Rob Corddry as White House Press Secretary Ari Fleischer
* Dennis Boutsikaris as U.S. Deputy Secretary of Defense Paul Wolfowitz Paul Bremer, head of the Coalition Provisional Authority
* Jason Ritter as Jeb Bush
* Michael Gaston as General Tommy Franks
* Tom Kemp as David Kay
* Paul Rae as Congressman Kent Hance James Robison, Tony Evans, T. D. Jakes, and Ed Young.  
* Jesse Bradford as Thatcher, Bushs college buddy
* Marley Shelton as Fran, one of Bushs girlfriends
* Anne Pressly as Anchorwoman
* Sayed Badreya as Saddam Hussein, President of Iraq
* William Chan as Hospital Doctor Teresa Cheung as Miss China
* Colin Hanks as David Frum
* Bryan Massey as Skeeter
* Brent Sexton as Joe ONeill
* Randal Reeder as Oil Rig Foreman

== Production ==
 

W was Oliver Stones third film in a trilogy he made about the Presidency, set in the time from the 1960s to today: the set began with   distributed the film.  invasion of The Queen, chamber piece, and not as dark in tone."  He described the structure of W. as a three-act film starting with Bush as a young man "with a missed life", followed by his transformation and "an assertion of will which was amazingly powerful" as he came out from his fathers shadow, and finally his invasion of Iraq.   

The film, originally titled Bush,  was re-titled W.     Filming began on May 12, 2008, in Shreveport, Louisiana,    and completed filming on July 11, 2008.  On May 13, 2008, the New York Post published excerpts from an early draft of the script. The column, written by Cindy Adams, stated “Pro-Bushies will hate it, antis will love it.”   
 presidential election. Republican Party presidential nominee John McCains ads in the fall of 2008. 

== Reception ==

=== Critical response ===

  average score normalized rating out of 100 to reviews from mainstream critics, the film has received an average score of 56, based on 36 reviews.   

Giving the film four stars in his review, Roger Ebert wrote that it was "fascinating" and praised all the actors, noting that Richard Dreyfuss, in particular, was "not so much a double as an embodiment" of Dick Cheney.  In contrast, Ann Hornaday of The Washington Post called the film "a rushed, wildly uneven, tonally jumbled caricature."   
Film critic James Berardinelli negatively compared the film with Saturday Night Live skits, saying of the actors that "None of them are as dead-on as Tina Fey as Sarah Palin."
 Governor Jeb Bush, who is portrayed in the film, called the sibling rivalry portrayed in the film "high-grade, unadulterated hooey" and said that Stones exploration of the family dynamic could have benefited from actual conversations with the Bush family.  Slate Magazine s Timothy Noah, however, noted that "most   the films more ludicrous details" are actually directly taken from non-fiction sources, and argued that the film was too kind to Bush in omitting certain historically recorded dramatic events, most notably Bushs alleged mocking of murderer Karla Faye Tucker, a woman put to death during his tenure of the Texan governorship.  However, the incident is disputed by Bush himself, and as such is also unconfirmed. In a March 2010 "Screen Test" interview with The New York Times  Lynne Hirschberg, Josh Brolin claims Bush did in fact watch the film.  Brolin said Oliver Stone met with Bill Clinton in China and Clinton told Stone hed lent his copy of W. to Bush.  Reportedly, Bush himself "liked it very much" and "thought there were sad moments." 
 New York Daily News named it the eighth best film of 2008,  and Roger Ebert of the Chicago Sun-Times named it on his top 20 list (he did not assign rankings).   

=== Box office ===
 The Secret Max Payne, respectively with $10,505,668 from 2,030 theaters with a $5,175 average.  The film had a budget of $25.1 million and grossed $25,534,493 in North America, and $3,401,242 internationally. 

== References ==
 

 

== External links ==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 