Lagos Cougars
{{Infobox film
| name           = Lagos Cougars
| image size     = 
| image	         = Lagos Cougars poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Desmond Elliot
| producer       = Emem Isong
| writer         = 
| screenplay     =  
| story          = 
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = Royal Arts Academy
| distributor    =  
| released       =  
| runtime        = 
| country        = Nigeria
| language       = English
| budget         =
| gross          =
}}
 Victoria Island. 
The film revolves around the life of 3 corporate women; Elsie (Monalisa Chinda), Aret (Uche Jombo) and Joke (Daniella Okeke) in their quest to find love and explore their fantasies.

==Cast==
*Monalisa Chinda as Elsie 
*Uche Jombo as Aret
*Daniella Okeke as Joke
*Benjamin Touitou as Lawrence
*Alex Ekubo as Chigo

==Reception==
The film was panned by critics. Nollywood Reinvented gave it an 18% rating, and explained that the storyline and directing was poor and very unoriginal.   

YNaija played down the love chemistry between Monalisa Chinda & her young lover Benjamin Touitou, it was also very critical of the uneven directing of the film. It titled its review and concluded that "Lagos Cougars was funny but not always for the right reasons". 

Sodas and Popcorn in its review gave a final verdict that the story was "...predictable and cliche ridden...".  

==References==
 

 
 

 