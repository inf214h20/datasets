Embrace of the Vampire
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name = Embrace of the Vampire
| image = Embrace-of-the-vampire.jpg
| caption = Film poster for Embrace of the Vampire
| director = Anne Goursaud
| producer = Alan Mruvka Marilyn Vance Ladd Vance Matt Ferro
| writer = Rick Bitzelberger Nicole Coady Martin Kemp Rebecca Ferratti Glori Gold Seana Ryan Sabrina Allen Harold Pruett Jennifer Tilly
| distributor = New Line Cinema
| budget =
| released =  
| runtime = 92 minutes
| country = United States
| language = English
}} vampire horror remake was released by Anchor Bay Home Entertainment starring Sharon Hinnendael as Charlotte Wells to universally negative reviews.

==Plot==
 
Charlotte is a "Chastity|chaste" good girl, who is having some very bad dreams about sex. Her dreams are the only place where her dream lover is a dark, handsome vampire; however, that is a bit of a dilemma for her real-life boyfriend, who is not quite as fascinating as the vampire dream boy. Charlotte is given the choice between staying with her life as she knows it, or becoming a part of the vampires world. There is also the usual struggle between such opposing entities, as Charlotte sees that not only does she have to choose between her boyfriend and her mysterious nighttime visitor, but also between light and dark, and good and evil. In the end, after briefly kissing Milo and being interrupted by Eliza, the campus slut (who was killed by the vampire by banging her head against the wall, then licking the blood from the wall), she ends up having the dream one last time. This time, the vampire is telling her to come to him, as he believes there is nothing left for her here (on earth). After some dream scenes, finally, Charlotte is in the tower with the vampire. Chris (her boyfriend) is there, but the vampire pushes Chris away, and is about to put the bite on her when she utters Chriss name; he at first tells her not to think of Chris and instead think of himself and her as a couple. Then, after Charlotte utters Chriss name again, the vampire then says that she cannot take his life to the eternal life, and disappears. Charlotte and Chris wake up in the morning and kiss, and the vampire is destroyed while laying in a sunbeam.

==Cast==
* Alyssa Milano as Charlotte Wells Martin Kemp as The Vampire
* Harold Pruett as Chris (credited as Harrison Pruett)
* Jordan Ladd as Eliza
* Rachel True as Nicole
* Charlotte Lewis as Sarah
* Jennifer Tilly as Marika
* Rebecca Ferratti as Princess
* Glori Gold as Nymph I                                                                     
* Seana "Shawna" Ryan as Nymph II
* Sabrina Allen as Nymph III
* Robbin Julien as Rob
* Christopher Utesch as Guy in Hallway
* David Portlock as Peter
* Gregg Vance as Jonathan
* John Riedlinger as Milo
* Ladd Vance as Mark
* Lynn Philip Seibel as The Professor

==See also==
*Vampire film

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 