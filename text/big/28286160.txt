Vinyan
 
 
{{Infobox film
| image = Vinyan_poster.jpg
| caption = Theatrical Poster
| director = Fabrice du Welz
| producer =  
| writer =  
| starring =  
| music = François-Eudes Chanfrault
| cinematography = Benoît Debie
| editing = Colin Monie
| studio = The Film
| distributor =  
| released =  
| runtime = 96 minutes
| country =  
| language = English
}} horror themes directed and co-written by Fabrice du Welz. The film was du Welzs second as a director. The film premiered at the Venice Film Festival on 30 August 2008.

Reviews towards the film were slightly positive, receiving an aggregated score of 56% from Rotten Tomatoes.

==Plot==
Jeanne and Paul are a wealthy couple who were in Thailand helping to establish an orphanage when the 2004 tsunami leveled the island. Jeanne and Paul had a young son who disappeared in the storm, and since his body has never been found, Jeanne holds out hope that he might still be alive, a hope that becomes a desperate concern when she sees a video of children being held by kidnappers in Burma which shows a child who looks like her boy. Eager to find out the truth, Paul pays a hefty fee to local outlaw Mr. Gao to escort him and Jeanne into a forbidden zone known only to Thailands criminal underclass near the Burmese border. Jeanne and Paul soon find themselves out of their depth in a strange land they do not understand where dangerous men commune with the spirits of the dead.

==Cast==
* Emmanuelle Béart as Jeanne Bellmer
* Rufus Sewell as "Paul Bellmer"
* Petch Osathanugrah as Thaksin Gao
* Julie Dreyfus as "Kim"

==Release==
The film first appeared in North America at the Toronto International Film Festival, where it premiered on 5 September 2008.

== External links ==
*   
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 