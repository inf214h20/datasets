Frequencies (film)
 
{{Infobox film
| name           = Frequencies
| image          = 
| border         =
| alt            = 
| caption        = 
| director       = Darren Paul Fisher
| producer       = Darren Paul Fisher
| writer         = Darren Paul Fisher
| starring       = {{plainlist|
* Daniel Fraser
* Eleanor Wyld
}}
| music          = Blair Mowat
| cinematography = James Watson
| editing        = Darren Paul Fisher
| studio         = Incurably Curious Productions
| distributor    = FilmBuff
| released       =  
| runtime        = 105 minutes
| country        = United Kingdom
| budget         = 
| gross          = 
| language       = English
}} British science fiction romance film directed and written by Darren Paul Fisher. The film stars Daniel Fraser, Eleanor Wyld, and Owen Pugh. The film takes place in a world where human worth and emotional connections are determined by set "frequencies".

The film was produced by Darren Paul Fisher and Alice Hazel Henley, and was released on 24 July 2013.

==Plot==

In a world where relationship, connections, and life worth is determined by predestined "frequencies", Isaac-Newton Midgeley, known as Zak, is a low born that attempts to change his fate by attempting to start a relationship with high born savant, Marie-Curie Fortune.

Despite teachers and parents who tell Zak that Marie and he are opposites which will never attract, Zak attempts throughout his youth to court Marie with no success. Marie, being of high frequency, is unable to feel emotion; however, her goal is to feel love. Zaks friend, Theo, attempts to help Zak raise his frequency, a feat claimed to be impossible. During his teenage years, Zak uses magnets and other methods with no avail.

Upon returning as a young adult to Maries birthday, he claims to be able to raise his frequency and eventually manages a kiss from Marie. The two end up spending the night together. Zak discovers with Theo that sound waves, when combined with gibberish two syllable words, are able to temporarily raise ones frequency. They create a cell phone device which, based on the environment, is able to determine which words can raise ones frequency.

However, Zak and Marie discover that the words actually have mind controlling properties which  caused their love. A secret government organization detains Zak and his associates, revealing that this phenomenon had been known throughout history and was slowly forgotten. By 1760, this phenomenon lost much of its power. Unable to contact Theo, Zak uses the words to paralyze his captors and escape.

He escapes to Theos house, where Theos father reveals that music, specifically that of Mozart, was able to balance everyones frequencies and nullify the mind controlling properties of these words. Theo is able to calculate an equation based on music and discover that fate exists. He is able to predict the future and the destinies of others. Zak and Marie realize their love was caused by fate not choice. Finding this irrelevant, the two hold hands while Theo watches and realizes he can perfect the equation.

==Cast==
*Daniel Fraser as Zak
*Eleanor Wyld as Marie
*Dylan Llewellyn as Zak (Teen)
*Georgina Minter-Brown as Marie (Teen)
*Owen Pugh as Theo

==Reception==
The film currently holds a 100% on Rotten Tomatoes, a review aggregator, based on six reviews.  Daniel Gold of  The New York Times wrote, "While the detached, deadpan tone and occasionally stilted acting might leave some viewers flat, there’s no doubting the fierce intelligence behind this admirable puzzle box of a movie.".  Alan Scherstuhl of the The Village Voice said the film was "an uncommonly ambitious science-fiction romance ... sparkling and unsettling at once".  MaryAnn Johanson of Flick Filosopher praised the film for its "incredibly ambitious and profoundly provocative science fiction drama about ideas that require no FX to sell them".   John DeFore of The Hollywood Reporter wrote, "Though the explanations Fisher comes up with for his sci-fi contrivances may not be fully satisfying in the end, the conceits themselves offer much to play with, bringing the film into that pleasing area where an imaginary reality has interesting things to say about our own."   Richard Whittaker of The Austin Chronicle wrote, "What raises Fishers script to the upper echelons of the current wave of intellectually challenging indie sci-fi ... is that it truly weaves its concept into its nature."   Kurt Halfyard of Twitch Film wrote that it "is chock-a-block full of knowledge, destiny and imagination, but suffers a bit from a lack of heart and soul." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 