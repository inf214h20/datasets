Joseph and the Dreamer
{{Infobox film name = Joseph the Dreamer image = caption = director = Yoram Gross producer = Yoram Gross writer = Natan Gross starring = music =  cinematography = editing =  studio =  distributor = released = 1961 runtime = 60 mins country = Israel language = English budget = gross =
}} Joseph from the Bible. 

==Production==
The movie was made with home-made puppets from a script by Gross brother. Part of the budget came from the Israel Film Commission. Gross later recalled:
 We had a crew of five people. We receive permission from the municipality to use a storage room as a studio, but in those days the studio lights were so hot that we couldnt film during the day. It was so hot we could only shoot at night, with open windows and doors.   accessed 4 March 2014  

==Reception==
The movie screed at the Cannes Film Festival.

According to Yoram Gross, the film was widely seen but because the majority of the audience were schoolchildren, who only paid a quarter of the normal childrens ticket price, he could not recover his exhibition costs. 

It was re-released in Australia in 2002 dubbed into English. The Hebrew title was Baal Hahalomot. 

==References==
 

==External links==
*  at IMDB
*  at New York Times
 

 
 
 
 


 