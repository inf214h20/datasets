Tumko Na Bhool Paayenge
{{Infobox film 
| name           = Tumko Na Bhool Payenge
| image          = Tumko.jpg
| image_size     = 200px
| director       = Pankaj Parashar
| producer       = Gordhan Tanwani
| writer         = Rumi Jafery
| narrator       = 
| starring       = Salman Khan Sushmita Sen Diya Mirza Inder Kumar
| music          = Sajid-Wajid Daboo Malik
| cinematography = Thomas A. Xavier
| editing        = Srinivas Patro
| studio         =
| distributor    =
| released       = 21 February 2002
| runtime        = 165 minutes
| country        = India
| language       = Hindi
| awards         = 
| budget         = 
}}
 romantic action action thriller thriller film It Was Raining That Night (2005).

== Synopsis ==

Veer Thakur (Salman Khan) is an eligible young bachelor living in a small community with his parents, Thakur Kunal Singh (Sharat Saxena), and mom Thakurain Geeta (Nishigandha Wad). He is in love with Muskaan(Dia Mirza), his friend & daughter of another Thakur. Their parents give their support to the union, but Veer suddenly starts getting visions of events that he cant remember ever happening to him & finds that he is an expert in fighting techniques no pehelwaan, including his father, have ever seen before. Nobody has any explanation for these facts.

One day, as the Chief Minister (Anjan Srivastav) is giving a speech at a certain function in his village, Veer suddenly spots a sniper trying to aim at the CM. Veer lunges to save the CM, only to find that the sniper and the building are both missing. Veer grows restless, but nobody notices that the CM has grown restless on seeing Veer. Some days later, at Veers wedding, some goons attack the party and try to kill Veer. Veer kills them single-handedly. Convinced that everyone is hiding something important from him, he demands answers. The Thakur relents and tells him that he is neither Veer nor their son.
 Arbaaz Khan) was a soldier who died in the line of duty. After immersing his ashes, he found the bullet-ridden body of an unknown man. When he realized that this man had no memories of his past, he told him that he is Veer & concoct a past for him, because they feel the need to have a son. The hero decides to go on a quest to find his real identity. He goes to Mumbai to find that both police & goons are baying for his blood. He meets a teenager who calls him Ali bhaiyya, but the teenager dies while trying to save Ali from an assassin. The hero does not see the face of the assassin, but assumes that his name is Ali. He gets visions of a girl(Sushmita Sen) whom he has never seen.

The mystery starts unwinding when he meets a guy named Inder(Inder Kumar). As Ali starts getting his past memories, it is confirmed that his name is indeed Ali. Ali & the teenager were orphaned brothers, while Inder was Alis friend. An old man named Rahim Chacha(Alok Nath) was their guardian. The mysterious girl Ali saw was Mehak, his love. Ali & Inder had won medals for shooting during many contests, although Ali was always the best. The marksmanship skills of Ali & Inder are seen by an Inspector(Mukesh Rishi), who makes a proposal to them: masquerade as goons of a gang, kill goons of their rival gang & trick both the gangs to destroy each other in gang wars. Ali refuses flatly, but after some goons kill Rahim Chacha, Ali & Inder decide to take the offer.

Mehak gives both the guys portable video recorders, so that they can prove their innocence if anything goes wrong. Soon, the Inspector takes them to the Chief Minister(Sadashiv Amrapurkar) & his aide(Anjan Srivastav). They plan to enact an attack on the CM, making the opposition look dirty in eyes of people & garnering sympathy votes for CM. However, when Ali is trying to fake the shooting, somebody really kills the CM. The police start chasing Ali thinking him to be the killer & Ali flees.

After recovering his memory, Ali realizes that the CMs aide took advantage of the plan to become CM himself. Meanwhile, Ali learns that Inder made Mehak his fiancee to save her from harassment. Ali tries to tell the truth to the Inspector, but realizes that nobody believes him. In an attack when Ali goes aboard a local, the remaining memories come to him. He remembers boarding the train same way on the day of assassination where Inder met him & confessed to killing the CM. Thereafter, Inder shot Ali to hide the truth & threw his body in a river. Suddenly, the Inspector confronts him, but Ali convinces the Inspector by telling the truth.

Ali goes to Mehak & tells her everything. He realizes that there must be some incriminating evidence in Inders tapes. As he plays a tape, Mehak witnesses in horror Inder striking the deal with CMs aide. Ali notifies Inder that he remembers everything now. Inder, along with his cronies, comes to kill Ali, but Mehak dies in the process. Ali kills the goons & demands Inder an explanation.

Inder reveals that he was always second best with Ali around & that even Mehak, whom he secretly loved, chose Ali over him. Also, Ali always got money & fame more easily than Inder. Inder reveals that he had sent the goons to kill Rahim Chacha, thus manipulating Ali to take the offer. Also, when Ali came back to Bombay, Inder saw him. Inder was the sniper whom Alis brother saw. Ali kills Inder in a hand-to-hand combat. Ali broadcasts Inders tape over the cable TV network, thus freeing himself from his charges & putting the present CM in the dock. He returns to the village and marries Muskaan, just as planned.

== Cast ==
* Salman Khan ... Ali/Veer Singh Thauker
* Dia Mirza ... Muskaan
* Sushmita Sen ... Mehak
* Inder Kumar ...  Inder Saxena
* Nishigandha Wad ...  Geeta Singh Thakur
* Sharat Saxena ... Thakur Kunal Singh
* Mukesh Rishi ... Commissioner Arvind Raj
* Rajpal Yadav ...  Lallan
* Anjaan Srivastav ... Chief Minister
* Pankaj Dheer ...  Shivpratap Singh (Muskaans Father)
* Alok Nath ... Rahim Chacha
* Razak Khan ...  Dilbar Khan
* Johnny Lever ...  Pakhandee Baba Arbaaz Khan ...  Veer Singh Thakur (special appearance)

==Box office/ Reception== Raaz which went on to be a hit while this movie was an average. 

==Soundtrack==
{{Infobox album  
| Name = Tumko Na Bhool Paayenge
| Type = Soundtrack
| Artist = Sajid-Wajid
| Cover =
| Released = 2002
| Recorded = Feature film soundtrack
| Length =
| Label =  T-Series
| Producer = Sajid-Wajid
| Reviews =
| Last album = Kya Yehi Pyaar Hai  (2002)
| This album = Tumko Na Bhool Paayenge (2002) Maa Tujhhe Salaam (2002)
}}

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
!# !! Title !! Singer(s)!! Music!! Length

|-
| 1
| Bindiya Chamke, Choodi Khanke
| Alka Yagnik, Sonu Nigam
| Daboo Malik
| 05:12
|-
| 2
| Kya Hua Tujhe
| Alka Yagnik, Sonu Nigam
| Daboo Malik
| 05:05
|-
| 3
| Yeh Bekhudi Deewangi
| Sonu Nigam
| Sajid-Wajid
| 06:20
|-
| 4
| Mubarak Eid Mubarak 
| Sonu Nigam, Arvinder Singh, Sneha Pant
| Sajid-Wajid
| 06:22
|-
| 5
| Kyon Khanke Teri Choodi
| Alka Yagnik, Kamaal Khan
| Sajid-Wajid
| 05:11
|-
| 6
| Mehendi Hai Lagi Mere Hathon Mein
| Sonu Nigam, Jaspinder Narula
| Daboo Malik
| 05:40
|-
| 7
| Main To Ladki Kanwari 
| Sonu Nigam, Sneha Pant
| Sajid-Wajid
| 06:11
|-
| 8 Kyon Khanke Teri Choodi (Club Mix)  Alka Yagnik, Kamaal Khan
| Sajid-Wajid
| 04:29
|}

==References==
 

==External links==
* 

 
 
 