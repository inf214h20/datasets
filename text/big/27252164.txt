The Impassive Footman
{{Infobox film
| name = The Impassive Footman
| image =
| image_size =
| caption =
| director = Basil Dean
| producer = Basil Dean 
| writer = Herman C. McNeile  (play)   Harold Dearden   John Farrow   John Paddy Carstairs 
| narrator = George Curzon 
| music =  Ernest Irving  Robert Martin Otto Ludwig   Ernest Aldridge
| studio = Associated Talking Pictures 
| distributor = RKO Pictures  
| released = July 1932
| runtime = 70 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} quota quickie" George Curzon.  The film was made at Ealing Studios with sets designed by Edward Carrick. It was also released under the alternative title Woman in Bondage.

==Plot== hypochondriac husband. This leads to a series of violent quarrels, all witnessed by the familys footman who is the only one who knows entirely what is going on. 
==Cast==
* Owen Nares as Bryan Daventry
* Betty Stockfeld as Grace Marwood
* Allan Jeayes as John Marwood George Curzon as Simpson
* Aubrey Mather as Doctor Bartlett
* Frances Rose Campbell as Mrs Angers
* Florence Harwood as Mrs Hoggs

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 