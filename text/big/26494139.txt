Millennium Stars
 
{{Infobox film
| name           = Millennium Stars
| image          = Millennium Stars.jpg
| image_size     =
| alt            = 
| caption        = 
| director       = Jayaraj
| producer       =N. Krishnakumar (Kireedam Unni)
| writer         = 
| narrator       = 
| starring       = Suresh Gopi Jayaram  Biju Menon Vidyasagar
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1 January 2000
| runtime        = 
| country        =  
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Millennium Stars is a 2000 Malayalam film by Jayaraj starring Suresh Gopi, Jayaram and Biju Menon in lead roles. Many consider the soundtrack of the film to be one of Vidyasagar (music director)|Vidyasagars finest works.

== Plot ==
When they were children, Shiva and Shankar went to Mumbai in search of a living. Circumstances puts them in jail, where they meet a bully named Karunan. They escape from jail. Several years later, Shankar and Shiva dream of becoming singers. Karunan is still active as a criminal. Through their hard work,the Shiva-Shankar duo become successful singers and start being called "millennium stars". Then the villain enters and tries to separate the two. Karunan convinces them not to separate.

==Cast==
*Suresh Gopi ...  Karunan 
*Jayaram ...  Shankar 
*Biju Menon ...  Shivan 
*Abhirami
*Harisree Asokan Devan
*Kalabhavan Mani   Manorama
*Laxmi Rattan
*Jagathy Sreekumar

== Box office ==
Since this film was taken and released with huge expectations,this film failed at the box office

==Soundtrack==

The films music was composed by Vidyasagar (music director)|Vidyasagar, with lyrics by Girish Puthenchery.
{|class="wikitable" width="70%"
! Song Title !! Singers 
|-
| "Maha Ganapathim" ||  Hariharan (singer)|Hariharan, K. J. Yesudas, Srinivas (singer)|Srinivas, Vijay Yesudas 
|-
| "Krishna Krishna" || K. J. Yesudas, Vijay Yesudas 
|-
| "O Mumbai" || K. J. Yesudas, Vijay Yesudas 
|-
| "Shravan Gange" || K. J. Yesudas, Hariharan (singer)|Hariharan, Vijay Yesudas 
|-
| "Parayaan Njan Marannu" || K. J. Yesudas, Hariharan (singer)|Hariharan, Vijay Yesudas 
|-
| "Kuku Thee Vandi" || Harikrishna, Anand 
|-
|}

==External links==
*  

 
 
 
 


 