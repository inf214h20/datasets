Pilot ♯5
 
{{Infobox film
| name           = Pilot #5
| image          = Pilot No.5.jpg
| image_size     = 200 px
| caption        = Theatrical poster
| director       = George Sidney
| producer       = B.P. Fineman David Hertz
| narrator       = Marsha Hunt Gene Kelly Van Johnson
| music          = Lennie Hayton
| cinematography = Paul Vogel George White
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =$486,000  . 
| gross = $969,000 
}}
 Marsha Hunt, Gene Kelly and Van Johnson.  It was directed by George Sidney. Told in flashback, Pilot #5 marked Gene Kellys dramatic debut. It was also known by the titles Destination Tokyo, Skyway to Glory and The Story of Number Five. 

==Plot==
In 1942, a small group of Allied soldiers and airmen stationed on Java are being bombed by Japanese aircraft daily. With only one working fighter of their own, and five pilots who volunteer to fly a dangerous mission, the Dutch commander, Major Eichel (Steven Geray) chooses George Collins (Franchot Tone) to bomb the Japanese aircraft carrier lying offshore. As the flight progresses, Eichel asks the other pilots to tell him about George. As they recount his rise from brilliant law student, it is apparent that his involvement in a scandal with the states Governor, has led to attempts to redeem himself, especially for Freddie (Marsha Hunt), his long-time love. With the promise that his mission is "for his country," Collins sacrifices himself in a final dive on the carrier.

==Cast==
As appearing in Pilot #5 (main roles and screen credits identified):   IMDb. Retrieved: May 8, 2012. 
* Franchot Tone as George Braynor Collins Marsha Hunt as Freddie Andrews
* Gene Kelly as Vito S. Alessandro
* Van Johnson as Everett Arnold Alan Baxter as Winston Davis
* Dick Simmons as Henry Willoughby Claven
* Steven Geray as Major Eichel (as Steve Geray)
* Howard Freeman as Governor Hank Durban
* Frank Puglia as Nikola Alessandro
* William Tannen as American Soldier 

Casting included Peter Lawford who had an uncredited role as a British soldier.

 

==Production==
The facilities at Cal-Aero Aviation Training School, Riverside, California were used for the wartime sequence. A Republic P-43 Lancer was prominently featured as the sole remaining fighter aircraft in Java. 
 Jim Davis, Marilyn Maxwell, Marie Windsor and Frances Rafferty as cast members. Ava Gardner has been listed in a modern source as uncredited.   Turner Classic Movies. Retrieved: May 8, 2012. 

==Reception==
Not given any major promotion and considered a "B" feature in the MGM lot, Pilot #5 was mainly a forgettable flagwaver, typical of the period.  It didnt fare well with audiences or critics.  More recent reviews have noted it provides a historical context, but mainly remains a curio.  Leonard Maltin described the film more favorably, "Good cast uplifts so-so curio; its intriguing to see Kelly in a supporting part, as a morally bankrupt hothead. Watch for Peter Lawford at the opening, and see if you can spot Ava Gardner. 
===Box Office===
According to MGM records the film earned $669,000 in the US and Canada and $300,000 elsewhere resulting in a profit of $174,000. 
==References==
;Notes
 
;Bibliography
 
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5. 
 

==Further reading==
* 

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 