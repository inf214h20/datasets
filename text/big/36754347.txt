Last of the Pagans
{{Infobox film
| name           = Last of the Pagans
| image          =Last of the Pagans (1935) trailer 1.jpg 
| image_size     = 
| alt            =film tailer image 
| caption        =Ray Mala and Lotus Long 
| director       = Richard Thorpe
| producer       =  Phil Goldstone
| screenplay     =John Farrow
| narrator       = 
| starring       = Ray Mala, Lotus Long
| music          = 
| cinematography = 
| editing        = 
| studio         = MGM
| distributor    = 
| released       = 1935
| runtime        = 
| country        = United States, filmed on location in Tahiti
| language       = English
| budget         = 
}}
Last of the Pagans is a 1935 MGM film. It was based on the Herman Melville novel: Typee.

==External links==
*  at IMDB
*  at TCMDB
 
 
 