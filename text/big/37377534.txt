Jigsaw (1989 film)
{{Infobox film
| name           = Jigsaw
| image          = 
| image size     =
| caption        = 
| director       = Marc Gracie
| producer       = Rosa Colosimo
| writer         = Marc Gracie Chris Thompson
| based on = an idea by Rosa Colosimo
| narrator       =
| starring       = Rebecca Gibney Dominic Sweeney Nic Lathouris
| music          = 
| cinematography = Jaems Grant
| editing        = 
| studio = Rosa Colosimo Films
| distributor    = Video Entertainment (video)
| released       = 1989
| runtime        = 86 mins
| country        = Australia
| language       = English
| budget         =
| gross = 
| preceded by    =
| followed by    =
}}
Jigsaw is a 1989 thriller film starring Rebecca Gibney. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p117 

==Plot==
Virginia Yorks husband is killed on the first day of their honeymoon and she is the prime suspect. Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p91 

==Cast==
*Rebecca Gibney as Virginia York
*Dominic Sweeney as Detective Constable Broulle
*Gary Day as Gordon Carroll Terence Donovan as Jack McClusky
*Michael Coard as Aaron York
*James Wright as Ray Carpenter
*David Bradshaw as Alex York
*John Flaus as Oliver
*Brenda Addie as Jean
*Anthony Fletcher as Tony
*Nick Lathouris as Ted Minter

==References==
 

==External links==
*  at IMDB
*  at Screen Australia
*  at TCMDB

 
 
 


 