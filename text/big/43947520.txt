Bheekara Nimishangal
{{Infobox film 
| name           = Bheekara Nimishangal
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = P Arunachalam
| writer         = Thamil Story Jagathy NK Achari (dialogues)
| screenplay     = Jagathy NK Achari Sathyan Madhu Madhu Sheela Adoor Bhasi
| music          = MS Baburaj
| cinematography = Dathu
| editing        = VP Krishnan
| studio         = Savithri Pictures
| distributor    = Savithri Pictures
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by P Arunachalam. The film stars Sathyan (actor)|Sathyan, Madhu (actor)|Madhu, Sheela and Adoor Bhasi in lead roles. The film had musical score by MS Baburaj.   

==Cast==
  Sathyan as Mancheri Raghavan Madhu as Murali
*Sheela as Savithry
*Adoor Bhasi as Ranger James
*Sankaradi
*T. S. Muthaiah as Vikraman Thampi
*Abbas
*Baby Savitha
*Bahadoor
*Geetha Khadeeja
*Kuttan Pillai
*N. Govindankutty as Velu
*Paravoor Bharathan
*Ushakumari as Indira Vincent as Murali.
*Hema 
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anjalippoo || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Pirannaal Innu Pirannaal || LR Eeswari || Vayalar Ramavarma || 
|-
| 3 || Thulasi Devi || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Vaisaakha Poojaykku || K. J. Yesudas, S Janaki || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 