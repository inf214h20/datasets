Flying Disc Man from Mars
{{Infobox Film
| name           = Flying Disc Man from Mars
| director       = Fred C. Brannon
| image	=	Flying Disc Man from Mars FilmPoster.jpeg
| producer       = Franklin Adreon
| writer         = Ronald Davidson Walter Reed James Craven Harry Lauter Richard Irving Stanley Wilson
| cinematography = Walter Strenge
| distributor    = Republic Pictures
| released       =   Mathis 1995, pp. 3, 10, 122–123. 
| runtime         = 12 chapters / 167 minutes (serial) 
| country         = United States
| language        = English
| budget          = $152,640 (negative cost: $157,439) 
}} Republic Serial film serial.   This is considered a weak example of the serial medium, even compared to other post-World War II serials.

==Plot== atomic technology. American scientist, James Craven) into assisting him and hires some criminals to be his henchmen.
 Walter Reed), the private pilot who shot down Mota with Dr. Bryants ray gun, gets caught up in these events while working security for atomic industrial sites.

==Chapter titles==
# Menace from Mars (20min)
# The Volcanos Secret (13min 20s)
# Death Rides the Stratosphere (13min 20s)
# Execution by Fire (13min 20s)
# The Living Projectile (13min 20s)
# Perilous Mission (13min 20s)
# Descending Doom (13min 20s)
# Suicidal Sacrifice (13min 20s)
# The Funeral Pyre (13min 20s)
# Weapons of Hate (13min 20s) - a clipshow|re-cap chapter
# Disaster on the Highway (13min 20s)
# Volcanic Vengeance (13min 20s)
 Source:   
===Cliffhangers===
Walter Reed survives the serial by bailing out of whatever vehicle he was in. 

==Cast== Walter Reed as Kent Fowler, pilot
* Lois Collier as Helen Hall
* Gregory Gaye as Mota, martian invader James Craven Nazi
* Harry Lauter as Drake, one of Motas henchmen
* Richard Irving as Ryan, one of Motas henchmen
* Sandy Sanders as Steve, Kents sidekick
* Michael Carr as Trent, disc-ship pilot-henchman

==Production==
Flying Disc Man from Mars was budgeted at $152,640 although the final negative cost was $157,439 (a $4,799, or 3.1%, overspend).  It was the most expensive Republic serial of 1950. 

It was filmed between August 21 and September 12, 1950 under several working titles: Atom Man from Mars, Disc Man from Mars, Disc Men of the Skies, Flying Planet Men and Jet Man from Mars.   The serials production number was 1709. 

This is a sequel to earlier serial The Purple Monster Strikes.  The villain Mota reuses the Purple Monster costume from that serial. Harmon and Glut 1973, pp. 55–56. 

===Special effects===
The Flying Disc from King of the Mounties is reused for this serial.  The Japanese logo is still visible on its side. In some shots the flying wing is footage from the Republic serial Spy Smasher (used in chapter three of Flying Disc Man), where the tail fin is missing (The wing was built for Spy Smasher, and the tail-fin with rising sun was added for the King of the Mounties serial). 

Stock footage from several earlier serials was used to pad out the serial and keep costs down.  This includes the Rocket crash from The Purple Monster Strikes, a car chase from Secret Service in Darkest Africa and various pieces from G-Men vs. the Black Dragon. 

All effects in this serial were produced by the Lydecker brothers, the in-house duo who worked on most of Republics serials.

===Stunts===
*Dale Van Sickel Kent Fowler/Watchman (doubling Walter Reed) David Sharpe Henchman Ryan/Technician (doubling Richard Irving) Tom Steele as Henchman Drake/Trent/Taylor (doubling Harry Lauter & Michael Carr)
*  Carey Loftin as Truck Loader Thug

==Release==
===Theatrical===
Flying Disc Man from Marss official release date is 25 October 1950, although this is actually the date the sixth chapter was made available to film exchanges. 
 The Tiger Woman, re-titled as Perils of the Darkest Jungle, instead of a new serial.  The next new serial, Don Daredevil Rides Again, followed in spring of 1951. 

A 75-minute feature film version, created by editing the serial footage together, was released on March 28, 1958 under the new title Missile Monsters.  It was one of 14 feature films Republic made from their serials. 

==References==
===Notes===
 
===Bibliography===
 
* Cline, William C. ""Filmography". In the Nick of Time. New York: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Harmon, Jim and Donald F. Glut. "2. "We Come from Earth, Dont You Understand?". The Great Movie Serials: Their Sound and Fury. New York: Routledge Publishing, 1973. ISBN 978-0-7130-0097-9.
* Mathis, Jack. Valley of the Cliffhangers Supplement. South Barrington, Illinois: Jack Mathis Advertising, 1995. ISBN 0-9632878-1-8. 
* Stedman, Raymond William. "5. Shazam and Good-by". Serials: Suspense and Drama By Installment. Norman, Oklahoma: University of Oklahoma Press, 1971. ISBN 978-0-8061-0927-5.
* Weiss, Ken and Ed Goodgold. To be Continued ...: A Complete Guide to Motion Picture Serials. New York: Bonanza Books, 1973. ISBN 0-517-166259.
 

==External links==
*  
*  
*    
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 