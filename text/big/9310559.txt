Ten (2002 film)
 
{{Infobox film
| name = Ten
| image = Ten DVD.jpg
| director = Abbas Kiarostami
| writer = Abbas Kiarostami
| starring = Mania Akbari Amin Maher
| released =  
| country = Iran
| language = Persian
| runtime = 89 minutes
| distributor =
}} Iranian film directed by Abbas Kiarostami and starring Mania Akbari. It was nominated for the Palme dOr at the 2002 Cannes Film Festival    and ranks at number 447 on Empire magazines 2008 list of the 500 greatest movies of all time.  The film ranked No.&nbsp;47 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010.  The French film magazine Cahiers du cinéma ranked the film as 10th place in its list of best films of the décade 2000-2009. 

== Plot ==
The film is divided into ten scenes, each of which depict a conversation between an unchanging female driver (played by Mania Akbari) and a variety of passengers as she drives around Tehran. Her passengers include her young son (played by Akbaris real life son, Amin Maher), her sister, a bride, a prostitute, and a woman on her way to prayer. One of the major plots during the film is the drivers divorce from her (barely seen) husband, and the conflict that this causes between mother and son.

== Film details ==
Many of the cast were untrained as actors, and the film has an improvisatory element. Elements of the characters were based on the actual life of the main actress and her son. The film was recorded on two digital cameras, one attached to each side of a moving car, showing the driver and passenger respectively.

The film explores personal social problems arising in Iranian society, particularly the problems of women.

== Bibliography ==
*Andrew, Geoff, 10 (London: British Film Institute, 2005).

== See also ==
* Docufiction
* List of docufiction films

==References==
 

==External links==
*  
*  
*   Daniel Ross,  

 

 
 
 
 
 
 
 
 
 
 
 

 