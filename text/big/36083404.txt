Mera Jawab
 
{{Infobox film
| name           = Mera Jawab
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = N.S. Raj Bharath
| producer       = P. Anand Rao
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jackie Shroff Meenakshi Seshadri
| music          = Laxmikant-Pyarelal
| cinematography = P. Deva Raj
| editing        = Vellaiswamy	 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by N.S. Raj Bharath and produced by P. Anand Rao. It stars Jackie Shroff and Meenakshi Seshadri in pivotal roles.

==Cast==
* Jackie Shroff...Suresh / Solanki Patwardhan Lal
* Meenakshi Seshadri...Poonam
* Shakti Kapoor...Danny 
* Gulshan Grover...Kukoo
* Satyendra Kapoor...DSouza
* Kader Khan...Inspector Ajay

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Main Usse Itna Pyar Karta"
| Manhar Udhas, Anuradha Paudwal
|-
| 2
| "Mere Liye Zindagi"
| Manhar Udhas, Anuradha Paudwal
|-
| 3
| "Aa Baitha Hoon Dar Pe Tere"
| Manhar Udhas, Anuradha Paudwal
|-
| 4
| "Main Hoon Hasina"
| Laxmikant-Pyarelal|Laxmikant, Alka Yagnik
|-
| 5
| "Mere Liye Zindagi (Female)"
| Anuradha Paudwal
|}
==External links==
* 

 
 
 
 

 