He Was a Quiet Man
{{Infobox film 
| name           = He Was a Quiet Man
| image          = He Was a Quiet Man film poster.jpg
| image_size     = 
| caption        = He Was a Quiet Man film poster
| alt            = A man clutches a briefcase to his chase. He is against a wood panelled wall, he is wearing a brown suit, he has brown receding hair, a moustache, and wears glasses. 
| director       = Frank Cappello
| producer       = Jason Hallock
| producer       = Frank Cappello
| writer         = Frank Cappello
| starring       = Christian Slater Elisha Cuthbert William H. Macy Sascha Knopf John Gulager Jamison Jones
| music          = Jeff Beal Frank Cappello Robert Cosio
| cinematography = Brandon Trost
| editing        = Kirk M. Morri
| distributor    = Bleiberg Entertainment
| released       = 2007
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $83,440    
|}}
 Mike Leahy.  The film stars Christian Slater, Elisha Cuthbert, Jamison Jones and William H. Macy.

== Plot == goes postal" and shoots up the office before Bob does. Bob stops the shooters plans by killing him with the gun he planned to use on the others, and in the process saves Venessas (Elisha Cuthbert) life. The former invisible nobody is suddenly thrown into the spotlight of public notice, he is considered a hero by those he wished to murder, promoted by a grateful boss to "VP of Creative Thinking" and given all the perks of higher management. Meanwhile, he saves the object of his desire only to have her ask him to end her life; her injuries have left her a quadriplegia|quadriplegic. 

Venessa asks Bob to let her roll down a subway platform in front of an oncoming train. Bob debates whether or not to go through with it, scrawling "should I finish what Coleman started?", on a piece of paper. Bob doesnt go through with it and he stops her just before she reaches the train. They then discover that she can wiggle her little finger, providing hope that she may recover. Bob and Venessa become romantically involved. Bob is still trapped by the demons of his past, and has insecurities that as soon as Venessa recovers, she will leave him.

Finally it is revealed that Bob has been hallucinating all of the events since just before the initial shooting. This time, he is in the same position as his coworker was, only instead of killing his coworkers, he shoots himself in front of Venessa. The last scenes show police searching his house to find a note that reads "you may ask why I did what I did... but what choice did you give me? How else could I have gotten your attention?" In the news with reporters interviewing his neighbors, they say that "he was a quiet man."

== Cast ==
* Christian Slater as Bob Maconel
* Elisha Cuthbert as Venessa Parks
* William H. Macy as Gene Shelby
* Sascha Knopf as Paula Metzler
* Jamison Jones as Scott Harper
* Michael DeLuise as Detective Sorenson
* Anzu Lawson as Nancy Felt
* John Gulager as Goldie / Maurice Gregory Frankie Lou Thorn as Jessica Light
* Randolph Mantooth as Dr. Willis
* Greg Baker as Copy Boy

== Awards ==
  
* May 1, 2007. Best Cinematography at the Newport Film Festival
* June 11, 2007. Best Director at the Jackson Hole Film Festival
* June 5, 2007. Best Feature at Seattles True Independent Film Festival
* October 9, 2009. Best Actor at Goa True Film Festival

== Reception ==
Rotten Tomatoes gives the film a score of 79% based on reviews from 19 critics. 

==Home media== 
  
The DVD version contains two alternate endings of the story.

In a first alternate ending, the lead-up to the shooting reveals that Bob is indeed the shooter and intends to shoot Venessa (due to his frustration that she does not know he exists), but before he can fire a shot, Bob is himself shot several times in the chest. The scene reveals Coleman to be the hero in this ending, having shot Bob through the cubicle wall. As Bob lies on the floor and his vision fades to black, he sees coworkers standing over him, with Venessa being the last coworker, mouthing the words "I love you" before he dies.

In a second alternate ending, all events of the first alternate ending come to pass. As Bobs vision fades to black and he sees Venessa mouth the words "I love you," a faint beeping sound is heard. The scene then flashes to Bob, sitting in his cubicle, going through a normal day with the shooting having never taken place. Venessa walks by his cubicle and comments on the hula-dancing figurine on Bobs desk. Bob removes a notebook from his desk drawer, and notes that today, Venessa loved his hula girl. The image pans out to reveal a meticulously detailed log of all inconsequential events and interactions with Venessa over the course of months (or even years). The book then closes to the end credits.

== References ==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 