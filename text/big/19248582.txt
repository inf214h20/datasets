Hornets' Nest
 Hornets Nest}}
 
{{Infobox film
| name           = Hornets’ Nest
| image          = Hornetpost.jpg
| alt            = 
| caption        = Original American film poster
| film name      = 
| director       = Phil Karlson Franco Cirino 
| producer       = Stanley S. Canter
| writer         = Stanley Colbert (story) S.S. Schweitzer
| starring       = Rock Hudson Mark Colleano Sylva Koscina Sergio Fantoni Giacomo Rossi-Stuart Jacques Sernas
| music          = Ennio Morricone
| cinematography = Gábor Pogány
| editing        = J. Terry Williams
| distributor    = United Artists
| released       = September 9, 1970
| runtime        = 110 minutes
| country        = USA Italy
| awards         =  German Italian Italian 
| budget         = 
}}
 American war film directed by Phil Karlson and starring Rock Hudson, Sylva Koscina, and Sergio Fantoni.

==Plot==
In Northern Italy in 1944, the entire population of the village of Reanoto is massacred by the SS under the command of the cruel Sturmbannführer Taussig (Jacques Sernas) for helping the Italian resistance movement.  The only survivors are a group of young boys in ages from 7 to 14 led by Aldo (Mark Colleano, son of Bonar Colleano and Susan Shaw ) who witness the mass execution and vow revenge.
 detachment of partisans prior Captain Turner (Rock Hudson), who is rendered unconscious and goes unnoticed by the Germans when he lands in the branches of a tree. The Germans capture the demolition equipment from the dead Americans.

Aldo and his friends rescue Turner by spiriting him away from the ambush.  Realising that Turner needs medical attention, they kidnap German doctor Bianca Freedling (Sylva Koscina) to nurse him to health, and keeping her captive even after Turners recovery. In order to avenge the massacre of Reanoto, Aldo wants Turner to train him and his friends in the use of military weapons and tactics.  Turner uses the opportunity as a second chance to complete his sabotage mission, using the boys instead of his late command.  He has the boys steal the captured American demolition gear from von Hechts headquarters, killing SS Rottenführer Gunther in the process, but Aldo hides the detonators until Turner leads them in their revenge. He trains them to use German MP40 guns.

In response to the killing of Gunther, Standartenführer Jannings makes the hunt for Turner and his "Dead End Kids" an SS-led affair, placing von Hecht under the command of Taussig and reassigning his regular Wehrmacht troops to guard a tunnel. Not long after this, Turner and the boys steal a truck and drive to SS headquarters, slaughtering all of Taussigs men in a surprise attack. Killing the SS was enough for most of the boys, but not for Aldo, who is becoming emotionally and mentally unhinged. He wants to keep killing Germans.

Von Hecht, after an argument, shoots and kills Taussig in the aftermath of the attack on SS headquarters, and goes after Turner and the boys solo. While Turner and a few of the boys are planting Composition C charges on the dam, Aldo and a few of the others go against orders and directly engage the German guards on top of the dam, which very nearly costs the entire mission. Taking over an MG42, Aldo becomes blood-drunk as he mows down Germans, even going so far as to shoot down his own friend Carlo when Carlo blocks his shot. Despite interference from von Hecht, Turner and the kids successfully plant the explosives and blow up the dam, flooding the valley.

With the dam destroyed and the Germans routed, the American troops begin moving in. When Turner learns of Carlos death, as well as that of another boy who died protecting Bianca and some of the younger children, he regrets having involved the children and breaks all of the guns. Aldo however refuses to hand his over, and runs off, encountering von Hecht, who survived the destruction of the dam. Aldo shoots and wounds him just as Turner catches up to him, and after failing to talk him out of finishing the German officer off, prevents him by force, taking his gun away. He takes von Hecht prisoner as von Hecht expresses his admiration for Turners success.

Turner, Bianca, von Hecht and all of the surviving boys except for Aldo go down to meet the approaching column of American soldiers. Aldo lingers. After seeing to it that the others are safely aboard a truck, Turner goes back for Aldo, who angrily throws rocks at him because he stopped him from killing von Hecht. But finally he is overcome with remorse for having killed Carlo, and begs Turners forgiveness. Turner gives it, and scoops the defeated and frightened boy up, and carries him to the waiting truck.

==Cast==
 
* Rock Hudson as Capt. Turner 
* Sylva Koscina as Dr. Bianca Freedling
* Sergio Fantoni as Capt. Friedrich von Hecht
* Giacomo Rossi-Stuart as Schwalberg 
* Jacques Sernas as Maj. Taussig 
* Mark Colleano as Aldo 
* Mauro Gravina as Carlo 
* John Fordyce as Dino 
* Giuseppe Cassuto as Franco 
* Amedeo Castracane as Tonio 
* Giancarlo Colombaioni as Romeo 
* Ronald Colombaioni as Mikko 
* Valerio Colombaioni as Arturo 
* Giuseppe Coppola as Rico 
* Daniel Dempsey as Giorgio 
* Luigi Criscuolo as Paolo 
* Gaetano Danaro as Umberto
* Vincenzo Danaro as Silvio Daniel Dempsey as Giorgio
* Anna-Luisa Giacinti as Maria
* Daniel Keller as Tekko
* Mauro Orsi as Luigi
* Maurizio-Fabrizio Tempio as Mario
* Jean Valmont as Scarpi
* Rod Dana as US Colonel
* Tom Felleghy as Col. Jannings
* Andrea Bosic as Gen. von Kleber
* Andrea Esterhazy as Gen. Dohrmann
* Mino Doro as Italian Doctor
* Gérard Herter as Capt. Kreuger
* Jacques Stany as Ehrlich
* Hardy Stuart as Cpl. Gunther
* Marco Gobbi as Hermann
* Max Turilli as Col. Weede
* Alan Chammas as 1st German Sentry
* Amos Davoli as 2nd German Sentry
* Goffredo Unger as Merkel
* Larry Dolgin as Pilot William Conroy as German Soldier (uncredited)
 

==Production==
Hudson had previously had success with several military roles, such as Ice Station Zebra.  After the failure of the large budgeted Darling Lili, he thought an action war film geared to the youth market directed by the experienced Phil Karlson had potential.  The female lead was originally to have been played by Sophia Loren, but she dropped out at the last moment and was replaced by Koscina.  Koscina, who had a childhood in World War II Yugoslavia, felt that the idea of the film of war destroying mentally as well as physically was an interesting one.  Her role as a female German doctor captured by partisans was similar to the lead in the acclaimed The Last Bridge.
 fatigues and Dirty Dozen Reach Puberty’”.  Ennio Morricones score included a whistling theme that was used in the film by the children themselves. 

Michael Avallone wrote a novelisation of the film as a tie-in.  An exploitation guide for cinema owners suggested dressing up boys under 15 in army uniforms and putting actual hornets nests (hopefully without their makers) in shop windows to promote the film. 

==Reception==
In what would be the last year of film productions about war, this film opened in both Europe and the U.S. to controversy. Critics attacked the film for its depiction of children learning violence and going to war with an American leading them: there was even a sexual assault by both the boys and later Turner on Koscinas German nurse. Many felt that such depictions did not make a good war film and felt this particular film went to unnecessary lows. This, plus the anachronistic touches in sets and costumes and resentment over the ongoing Vietnam War happening at the time, led the film to fail at the box office, along with other war films like Catch 22, Murphys War, and Waterloo (1970 film)|Waterloo. After this film, Rock Hudson turned to TV and the show McMillan and Wife.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 