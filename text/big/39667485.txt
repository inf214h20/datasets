Wolf Totem (film)
{{Infobox film
| name           = Wolf Totem
| image          = Wolf Totem film poster.jpg
| alt            = Two mens heads are seen, with a wolfs head seen above and between their heads.
| caption        = Chinese film poster
| director       = Jean-Jacques Annaud
| producer       = {{Plainlist |
* Jean-Jacques Annaud
* Xavier Castano
* La Peikang
* Bill Kong
}}
| screenplay     = {{Plainlist |
* Jean-Jacques Annaud
* Alain Godard
* John Collee
* Lu Wei
}}
| based on       =  
| starring       = {{Plainlist |
* Feng Shaofeng
* Shawn Dou
* Ankhnyam Ragchaa Basen Zhabu
* Yin Zhusheng
}}
| music          = James Horner
| cinematography = Jean-Marie Dreujou
| editing        = Reynald Bertrand
| production companies = {{Plainlist |
* China Film Group
* Beijing Forbidden City Film Corporation
* Reperage  (France) 
* China Movie Channel
* Beijing Phoenix Entertainment Co.
}}
| distributor    = {{Plainlist |
* China Film Group  (China) 
* Mars Distribution  (France) 
}}
| released       =  
| runtime        = 121 minutes
| country        = {{Plainlist |
* China
* France
}}
| language       = {{Plainlist |
* Mandarin Chinese Mongolian
}}
| budget         = US$38 million    - $40 million   
| gross          = US$110.95 million (China) 
}}
Wolf Totem ( ,   by Lu Jiamin. Directed by French director Jean-Jacques Annaud, the Chinese-French international co-production|co-production features a Chinese student who is sent to Inner Mongolia to teach shepherds and instead learns about the wolf population, which is under threat by a government apparatchik.
 Seven Years in Tibet is banned in China, was hired despite the history. The film was produced under China Film Group and French-based Reperage. The French director, who had worked with animals on other films, acquired a dozen wolf pups in China and had them trained for several years by a Canadian animal trainer. With a production budget of  , Annaud filmed Wolf Totem in Inner Mongolia, where the book is set, for over a year.

The film premiered at the European Film Market on  , 2015. It was released in China on  , 2015, for the start of the Chinese New Year, and it was released in France on  , 2015.

==Premise==
In 1969, student Chen Zhen ( ) is sent from Peking to the Chinese region of Inner Mongolia to teach shepherds. Instead, he learns about the shepherds and the bond they share with the wolves, a bond that is threatened by a government apparatchik. 

==Cast==
 
* Feng Shaofeng as Chen Zhen
* Shawn Dou as Yang Ke
* Ankhnyam Ragchaa as Gasma Basen Zhabu as Bilig
* Yin Zhusheng as Bao Shungui
* Baoyingexige
* Tumenbayar
* Xilindule
* Bao Hailong
 

==Production== a history Life of Pi (2012),    and he instead signed a contract with Qiang.   
 

By August 2009, Annaud began developing the project and scouting locations in China with Jiamin,  whom he had befriended. The filmmakers acquired wolves to raise and train in preparation for filming. The director worked on the first outline with writing partner Alain Godard, who died before they finished it. Annaud brought a draft to China in mid-2012. Chinese screenwriter Lu Wei wrote the second and third drafts of the screenplay.  The draft was translated to French for Annaud to give feedback, and it was subsequently translated back to Chinese for Jiamin to revise.  Preliminary filming of Wolf Totem began in July 2012.   
 William Feng Shaofeng and Shawn Dou were cast in the leading roles. The film marked the first Chinese production by a non-Chinese director.  For filming, 420 Chinese and seven French crew members were hired.    Annaud filmed Wolf Totem in Inner Mongolia, the region of China where the book is set,    near the town of Wulugai.  Filming in Inner Mongolia lasted for over a year.  The website China.org.cn reported, "The director and his team had to overcome harsh difficulties in the wild, such as low temperatures, extremely bad weather,   mosquito swarm attacks."  To preserve the grasslands, Annaud had his crew walk to locations with the equipment wherever possible, avoiding use of vehicles, despite the slower process making production more costly. {{cite news | last=Yu | first=Fu | url=http://english.cri.cn/12394/2014/12/05/2941s855410.htm | title= grips pushed the horse away from the wolves so they would chase it.    Some footage was also filmed in Beijing.  Annaud filmed Wolf Totem in 3D.  The production budget totaled  . 

===Animal training===
  Eurasian wolves The Bear (1988) and  Two Brothers (2004),  working with bears and tigers respectively.    He said dogs were traditionally used to depict wolves in film but that he sought to use actual wolves to show authentically their hunting method. 
 Andrew Simpson to raise and train the wolves,    which ultimately numbered 35.  Since China has a dwindling wolf population, the government did not allow any wolves to leave. Simpson moved from his ranch in Canada to China to train the wolves to sit, snarl, and fight on cue.  Four bases were built in Inner Mongolia and in Beijing for raising and training the wolves.  The wolves were trained for over four years to be used in the film.  Training revolved around feeding the wolves. They had a diet of dried dog food and chopped chicken, but during training, Simpson fed them "ruby red cubes" of fresh meat.  The wolves were kept under control behind long, double fences and were trained not to avoid the cameras.  During filming, the crew permitted the wolves to rest every hour. Despite precautions, actor Feng Shaofeng was injured by a wolf.    While Annaud filmed live footage of the wolves, he plans to use technology in post-production to create scenes that would normally be impossible to film.  After filming, the wolves were ultimately relocated to Canada since they only understood commands in English. 

Other animals were also prepared for filming. The Mongolian gazelle was difficult to find in Inner Mongolia, so filmmakers had to travel to the neighboring country of Mongolia to acquire gazelles. 

==Directors relationship with China==
  Seven Years Jetsun Pema, the sister of the exiled 14th Dalai Lama, was cast in the film.  Annaud was able to have his personal ban lifted,  though Seven Years in Tibet is still banned in China to date.  The Wall Street Journal said that Annauds hiring was "a surprise to some" due to this history.  Variety (magazine)|Variety said it was ironic for China Film Group, which is state-backed, to produce Wolf Totem while Seven Years in Tibet was still banned. 

Annaud said in 2012 that he was mistaken in assuming that it was acceptable to cover historical conflicts in retrospect, like with France, Algeria, and the Algerian War. The director said, "My mistake was to think that it was the same in China regarding Tibet. I realize now that it was seen as something very intrusive, which was not my intention." He said he did not have to apologize for directing the film.  He said, "I offended China with Seven Years and its quite something that after this we have decided not to speak about it. Im very grateful; it says a lot about China today."  The director described Wolf Totem as "much more complicated, fascinating, amusing" than others realize. The director said he would not have made the film if Chinese authorities did not like it.  The Associated Press said in 2009, "Annaud will have to make an apolitical interpretation of the novel to pass Chinese film censorship." It reported that Beijing Forbidden City Film Co. avoided the books political message and instead described it as "an environmental protection-themed novel about the relationship between man and nature, man and animal". 

In the weeks leading up to Wolf Totem s release, Annaud said that Chinese censors did not modify his screenplay. Reuters said, "  deals with conservation themes head on, though it largely avoids the books more subtle political issues."    The Economist said Annaud originally said that he did not have to apologize for Seven Years in Tibet but that he had apologized in December 2009, "In an open letter circulated in Chinese online (a liberal but generally fair translation, he says), Mr. Annaud declared he had never supported Tibet’s independence and had no personal relationship with the Dalai Lama." The Economist said Annauds self-criticism likely helped protect the film against Chinese critics who did not support its production. 

==Chinese-French relations in film==

In January 2015, Entgroup cited Annauds involvement with Wolf Totem as part of a trend that European directors were turning to China instead of Hollywood.  Yibada reported, "The French filmmaker told reporters that he is aware that he might have been an exception in the realm of censorship, as the film is being promoted as an exemplar of Sino-French cultural relations."    The Hollywood Reporter said with Wolf Totem s debut, "Execs are keen to learn about Frances expertise in pacting with the worlds second largest film market." A co-production treaty between China and France was signed in 2010, and Wolf Totem was among the first eight official co-productions to date. 

==Release==

===Theatrical screenings===
The film premiered at the European Film Market on  , 2015. It was released in China on  , 2015, the start of the Chinese New Year. It was released in France on  , 2015.    Alibaba Pictures acquired rights to distribute Wolf Totem in territories outside of the United States. 
 Wild Bunch acquired European sales rights for Wolf Totem at the European Film Market in February 2013.  Wild Bunch held a private screening of the film at the 7th UniFrance Rendez-vous for distributors who bought rights and for interested German buyers. Variety (magazine)|Variety reported, "Buzz... is that director Jean-Jacques Annaud is back." 

Wolf Totem had preview screenings in China starting on  , 2015. The film grossed   in preview screenings. The film was officially released in China on  , 2015 for the start of the Chinese New Year.     

With the presence of Ankhnyam Ragchaa, the movie premiered in Ulaanbaatar, Mongolia on 27 February 2015.    A Mongolian-language version of the film will also be released so audiences unfamiliar with Mandarin Chinese could see it. 

===Critical reception===

Maggie Lee, reviewing for Variety (magazine)|Variety, said, "Despite its magnificent natural vistas and some pulse-pounding action in stunning 3D, Wolf Totem boils down to a familiar environmentalist allegory that doesnt move or provoke too deeply." Lee said Jean-Marie Dreujous cinematography "rivetingly conveys" the wolves primal behavior but that the film failed to authentically dramatize the friction between humans and animals. She found the film to lack any "new perspective to environmental themes long expounded on in the West" and that the screenplay "considerably softened" the devastation and led to a weak conclusion. The critic also found the books "thought-provoking cultural-political subtext" missing from the film and that the films character development was weak with a "too muted" potential romance introduced late in the film. Lee commended the visual effects of the wolves in motion and composer James Horners score for its "strong emotional sweep" in non-dialogue scenes. 

Besides, Positif, a monthly and highly renowned dedicated-to-cinema magazine, indicates that "Rarement fiction naura su rendre aussi palpable la présence dun animal sauvage. Le Dernier loup est dabord un modèle de mise en scène en relief." And, Le Point, a weekly information magazine, said that "Annaud revient enfin à son meilleur: un film tendu, palpitant, parfois à couper le souffle. Un tour de force."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 