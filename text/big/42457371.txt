On the Other Side of the Tracks
{{Infobox film name          = On the Other Side of the Tracks image         =  alt           =  caption       =  director      = David Charhon producer      = Eric Altmayer Nicolas Altmayer story         = Eric Altmayer Nicolas Altmayer Alexis Dolivet Laurent Lafitte screenplay    = David Charhon Remy Four Julien War starring      = Omar Sy Laurent Lafitte Lionel Abelanski Zabou Breitman music         = Ludovic Bource
|cinematography= Alain Duplantier editing       = Stéphane Pereira studio        =  distributor   = The Weinstein Company released      =   runtime       = 96 minutes country       = France language      = French budget        = $8,4 millions gross         = $46,748,897 
}}
On the Other Side of the Tracks ( ) is a French comedy released in France on December 19, 2012, and picked up for US distribution by The Weinstein Company. It was released in the US on April 4, 2014. On the Other Side of the Tracks is the story of two very different police officers who team up after a business moguls wife is murdered.

== Plot ==

When bad boy police officer Ousmane is involved in a car accident, he is mistaken for the assailant by his fellow police officers. The next day, the wife of a business mogul turns up nearby Ousmanes housing project. Ousmane draws connections between the two crimes and believes he is able to solve both crimes. In order to be heard by fellow police officers, Ousmane is forced to team up with visiting Parisian investigator Francois Monge, who has much more departmental clout. Despite coming from and working in very different neighborhoods, the pair find common ground through their policing style, including manhandling suspects and kicking butt.

== Cast ==

*Omar Sy as Ousmane Diakhité
*Laurent Lafitte	 as François Monge
*Sabrina Ouazani as Yasmine
*Lionel Abelanski as Daniel Cardinet
*Youssef Hajdi	as Giovanni / Nabil
*Maxime Motte	as Van Gogh
*Léo Léothier as Gérard
*André Marcon as Chaligny
*Zabou Breitman as Commissaire Morland
*Patrick Bonnel as Balard
*Roch Leibovici as Patrick
*Rebecca Azan as Laurence
*Tchewk Essafi as Samir (as Tchewik Essafi)
*Patrick Kodjo Topou as Tyson
*Samuel Goreini as Sam

== Release ==
The film was released in the United States on April 4, 2014, opening on 50 screens.

== References ==
 

== External links ==
* 
* 
* 
* 
* 

 
 
 
 