Hum Hindustani
{{Infobox Film
| name           = Hum Hindustani
| image          =  
| image_size     = 
| caption        = 
| director       = Ram Mukherjee
| producer       =  Sashadhar Mukherjee
| writer         = 
| narrator       =  Helen Joy Mukherjee
| music          = Usha Khanna
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1960
| runtime        = 
| country        =   India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Translation We, Sanjeev Kumar in his debut.     The film is a remake of "Basu Parivar" (1952). The film about clash of Indian feudalism and modern youth during in the Nehruvian era, symbolished by the song, "Chodo Kal Ki Baatein" (Let go old stories) sung by Mukesh (singer)|Mukesh,     with music by Usha Khanna and written by Indian Peoples Theatre Association| IPTA poet, Prem Dhawan.    
 Helen said that because the film "flopped," she wasnt offered character roles after this film, until later in career and was typecast as a dancer.   

==Cast==
* Sunil Dutt as Suken
* Joy Mukherjee as Satyen
* Asha Parekh
* Jagirdar Helen as Kalpana
* Leela Chitnis Agha
* Prem Chopra  Sanjeev Kumar
==Music==

01. Neeli Neeli Ghata, O Bhigi Bhigi Hava  - Mukesh, Asha Bhosle

02. Raat Nikhari Hui, Zulf Bikhari Hui  - Mukesh

03. Chhodo Kal Ki Baaten Kal Ki Baat Puraani  - Mukesh

04. Hum Jab Chale To Ye Jahaa Jhume - Mohammed Rafi

05. Maajhi Meri Qismat Ke Ji Chaahe Jahaan Le Chal  - Lata Mangeshkar

06. Chori Chori Tori Aayi Hai Radha  - Lata Mangeshkar

07. Tu Lage Mora Baalam Ye Kaise Kahu Mai - Usha Khanna, Geeta Dutt

08. Baalamaa Re Haay, Mori Lat Sulajhaa De - Asha Bhosle

09. Chhedo Na Mohe Kanha Ja Ja - Lata Mangeshkar.
This film is also remembered for having introduced Sanjeev Kumar and Prem Chopra.

==References==
 

== External links ==
*  
*  

 
 
 
 


 