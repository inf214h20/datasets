7 Lives
 
 
{{Infobox film
| name           = 7 Lives
| image          = 7lives movie poster.jpg
| border         = 
| alt            = 
| caption        = Movie poster
| director       = Paul Wilkins
| producer       = 
| writer         = Paul Wilkins
| screenplay     = 
| starring       = Danny Dyer Kate Ashfield Martin Compston
| music          = Michael Price
| cinematography = James Friend Nick Gordon Smith
| editing        = Nigel Galt
| studio         = Starfish Films
| sound design   = 
| distributor    = Revolver (UK)
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| gross          = 
}}
7 Lives is a 2011 British film directed by Paul Wilkins starring Danny Dyer, Kate Ashfield and Martin Compston.  

==Plot==
Tom, a married man with kids, is struggling at work when a client tries to seduce him with promises of a ‘more exciting life’. On his way home one night he gets attacked by a gang of hoodies and falls into a parallel world where he lives 5 other lives including a Rock-Star, a Homeless person and the ‘hoody’ that attacked him. These lives help him to re-evaluate his priorities and values but in order to get home he must face some of his deepest desires and fears. Will he make it home or is the grass greener on the other side?

==Cast==
* Danny Dyer as Tom
* Kate Ashfield as Cynthia
* Nick Brimble as Ted
* Martin Compston as Rory
* Craig Conway as Keith
* Michael Elwyn as Brian
* Helen George as Valerie
* Tom Goodman-Hill as Peter
* Julien Ball as Doctor
* Theo Barklem-Biggs as Kid

==Critical reception==
GASHE.com gave the film a rating of 2 1/2 stars saying, "While the plot is dark and uncomfortable at times, it moves along at a decent pace and the unique use of different lead actors for each story works." 

==Release==
The film was released on 7 October 2011. The film was released on DVD and Blu-ray on 10 October 2011.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 