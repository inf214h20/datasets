Kodambakkam (film)
 
{{Infobox film
| name           = Kodambakkam
| image          = Kodambakkam.jpg
| caption        = 
| director       = 
| producer       = K.Seveal Nandha Diya Tejashree  Craig Brown
| music          = Sirpy
| cinematography = U.K Senthilkumar
| distributor    = AAA Productions
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
}}
Kodambakkam ( ) is a Tamil film directed by K. P. Jegannath.

The star of the film Nandha made his debut in the film Mounam Pesiyadhe as Surya Sivakumars friend. The film received many awards including best debut actor for Nandha.

==Cast== Nandha
* Diya
* Tejashree

==Production==

=== Development ===
The producers of this film is V.S Satheesan and K.Seveal. The film became a big hit. He used AAA Productions to help him. The director Jagan had already directed a movie. In addition he directed Joseph Vijay`s movie Puthiya Geethai.

== Team ==
The team which was used in this film were people who had been in other big movies. The cinematographer was U.K Senthilkumar who had also been involved in Rajnikanths movie Arunachalam. Jagan assisted with Cheran (director)|Cherans films such as Vetri Kodi Kattu. The producer K.Seveal had been involved in cinema for 4 year only and this was his third film. Other producer V.S Satheesan, who currently lives in the UK appeared as a cameo role in one of the minor scenes. Because of this the film became a major hit in London as V.S Satheesan encouraged all his relatives to support his film.

== References ==
 

 

 
 
 
 
 
 


 