The Pride of the Fancy
{{Infobox film
| name           = The Pride of the Fancy
| image          = 
| image_size     = 
| caption        =  Albert Ward
| producer       = 
| writer         = 
| narrator       = 
| starring       = Rex Davis Daisy Burrell 
| music          = 
| cinematography = 
| editing        = 
| studio         = G. B. Samuelson Productions
| distributor    =  
| released       = 1920 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British silent silent motion Albert Ward, Tom Reynolds. A drama, it was based on a novel by George Edgar.

==Plot==
Phil Moran is a young boxer who becomes a champion. 

==Cast==
* Rex Davis – Phil Moran 
* Daisy Burrell – Kitty Ruston Tom Reynolds – Professor Ruston Fred Morgan – Ireton  Dorothy Fane – Hilda Douglas 
* Wyndham Guise – Sir Rufus Douglas 
* F. Pope-Stamper –  Oswald Gordon
* Kid Gordon – James Croon

==Notes==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 


 