Class Action (film)
{{Infobox film
| name           = Class Action
| image          = Class_Action_film.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Apted
| producer       = Ted Field Scott Kroopf Robert W. Cort
| writer         = Carolyn Shelby Christopher Ames Samantha Shad
| starring       = Gene Hackman Mary Elizabeth Mastrantonio
| music          = James Horner
| editing        = Ian Crafford
| cinematography = Conrad Hall
| studio         = Interscope Communications
| distributor    = 20th Century Fox
| runtime        = 110 minutes
| released       =  
| country        = United States
| language       = English
| budget         = 
| gross          = $28,277,918 
}} drama Thriller thriller film directed by Michael Apted. Gene Hackman and Mary Elizabeth Mastrantonio star; Laurence Fishburne, Colin Friels, Fred Dalton Thompson, and Donald Moffat are also featured. The film was entered into the 17th Moscow International Film Festival.   

==Plot==
The story is about a lawsuit concerning injuries caused by a defective automobile. The suit takes on a personal dimension because the injured plaintiffs attorney, Jedediah Tucker Ward (Gene Hackman) discovers that the automobile manufacturers attorney Maggie Ward (Mary Elizabeth Mastrantonio) is his estranged daughter. 

Jedediah Ward is a liberal civil rights lawyer who has based his career on helping people avoid being taken for a ride by the rich and powerful; hes pursued principle at the expense of profit, though he has a bad habit of not following up on his clients after their cases are settled. 

Jeds daughter, Maggie, has had a bad relationship with her father ever since she discovered that he was cheating on her mother, Estelle (Joanna Merlin), and while she also has made a career in law, she has taken a very different professional route by working for a high-powered corporate law firm and has adopted a self-interested political agenda.

Jed is hired to help field a lawsuit against a major auto manufacturer whose station wagons have a dangerous propensity to explode on impact while making a left turn, but while his research indicates he has an all but airtight case against them, the case becomes more complicated for him when he discovers that Maggie is representing the firm hes suing.

The auto manufacturer in the film also utilizes a "bean-counting" approach to risk management, whereby the projections of actuaries for probable deaths and injured car-owners is weighed against the cost of re-tooling and re-manufacturing the car without the defect (exploding gas tanks) with the resulting decision to keep the car as-is to positively benefit short term profitability.

==Cast==
* Gene Hackman as Jedediah Tucker Ward
* Mary Elizabeth Mastrantonio as Maggie Ward
* Colin Friels as Michael Grazier
* Joanna Merlin as Estelle Ward Larry Fishburne as Nick Holbrook
* Donald Moffat as Quinn
* Jan Rubes as Pavel Matt Clark as Judge Symes
* Fred Dalton Thompson as Dr. Getchell
* Jonathan Silverman as Brian
* Joan McMurtrey as Ann Anne Elizabeth Ramsay as Deborah
* David Byron as Carl
* Tim Hopper as Howie
* Robert David Hall as Steven Kellen

==Production notes== fuel tank Mother Jones, allegded Ford was aware of the design flaw, refused to pay for a redesign, and decided it would be cheaper to pay off possible lawsuits. The magazine obtained a cost-benefit analysis that it said Ford had used to compare the cost of repairs (Ford estimated the cost to be $11 per car) against the cost of settlements for deaths, injuries, and vehicle burnouts. The document became known as the Ford Pinto Memo.       

==Reception==
Class Action opened at #4 in its opening weekend with $4,207,923 and ended with a domestic gross of $24,277,858; a worldwide total of $28,277,918 was made and the film was a moderate box office success.   

The film received generally positive reviews; it currently holds a 75% fresh rating on Rotten Tomatoes.  It holds a 58/100 on Metacritic, indicating "mixed or average reviews". 

==See also==
Grimshaw v. Ford Motor Co.

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at Abnormal Use

 

 
 
 
 
 
 
 
 
 
 
 
 
 