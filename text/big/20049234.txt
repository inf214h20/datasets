Rottweiler (film)
{{Infobox film
| name           = Rottweiler
| image          = Brian-Yuzna-Rottweiler.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Brian Yuzna
| producer       = Julio Fernández Brian Yuzna
| writer         = Miguel Tejada-Flores (screenplay) Alberto Vázquez Figueroa (story)
| narrator       = Paulina Gálvez Paul Naschy Ivana Baquero
| music          = Mark Thomas
| cinematography = Javier Salmones
| editing        = Andy Horvitch
| distributor    = Castelao Producciones
| released       =  
| runtime        = 95 minutes
| country        = Spain
| language       = English
| budget         =
}} science fiction Paulina Gálvez, Paul Naschy and Ivana Baquero.

== Plot ==
In the near future (2018), a prisoner named Dante (William Miller) escapes from jail after having been arrested for illegally entering Spain. Forced to kill a prison guard, he is hunted down by the prisons dog, a monstrous Rottweiler police dog that sadistic prison warden Kufard (Paul Naschy) had revived and cybernetically enhanced after a fatal injury. Believing his Spanish girlfriend Ula (Irene Montala) was sent to work as a prostitute in Puerto Angel as punishment, Dante looks for her, but is exhausted by the chase and wounded by the Rottweiller. As a result, he starts having hallucinations and being haunted by the repressed memories of his and Ulas arrest. As he reaches Puerto Angel and cant find her at the brothel, he finally remembers that Ula got killed when Kufard let his dog loose on her, which led Dante to shoot the dog, which was then turned into a cyborg. Dante kills Kufard as the Rottweiler catches up with him, and they fight to the death among the burning remains of Kufards helicopter. The morning after, firemen find the skeletons of Dante, Ula and the Rottweiler on the beach.

== Cast ==
 
* William Miller - Dante
* Irene Montalà - Ula Paulina Gálvez - Alyah
* Cornell John - Dongoro
* Lluís Homar -	Guard Borg
* Paul Naschy - Warden Kufard
* Ivana Baquero - Esperanza

== Critical reception == Allmovie called the film "a killer cyborg dog flick thats filled with more sleeping pills than chilling thrills" and "an obvious misstep for Yunza  , whose past successes are fastly fading in time. Do yourself a favor and leave this dog bone of a mess alone – youll be happy that you did." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 