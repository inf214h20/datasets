Café. Waiting. Love
{{Infobox film
| name           = Café. Waiting. Love
| image          = Cafe Waiting Love poster.jpg
| alt            = 
| caption        = 
| director       = Chiang Chin-lin
| producer       = 
| writer         = 
| starring       = Megan Lai   Sung Yuhua   Bruce   Marcus C   Pauline Lan   Lee Luo   Vivian Chow
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 120 minutes
| country        = Taiwan
| language       = Mandarin
| budget         = 
| gross          = New Taiwan dollar|NT$37.1 million (Taipei) Hong Kong dollar|HK$9.3 million (Hong Kong)
}}

Café. Waiting. Love (等一個人咖啡) is a 2014 Taiwanese romantic comedy film directed by Chiang Chin-lin.   

==Promo Slogan==
  (English translation: Everyone is waiting for a certain someone.)

==Plot==
Si-ying (Sung Yuhua) is a university freshman who works at a cafe. Here, she met A Bu-si (Megan Lai), a professional coffee maker who can make any type of coffee drinks according to a customers order, the shops proprietress (Vivian Chow) who is often quiet and alone, seated at a corner of her cafe most of the time, as well as Ze-Yu (Marcus Chang).

One day, Senior A-Tuo (Bruce), a senior of Si-yings whos pretty legendary in the university, came to the coffee shop with his friends where he met a reportedly lesbian who stole his girlfriend - A bu-si, by coincidence. As the friends kept teasing A-Tuo, Si-ying, full of helping and justice heart, helped A-Tuo out of the difficult situation. The two of them eventually became friends after several encounters. Senior A-Tuo is an optimistic person with a happy-go-lucky personality. Besides working part-time at a roadside stall, he also works for Bao (Lee Luo), who was a movie director and now a mediator for gangs, as a cook at his restaurant. There, he got to know Aunt Jin-dao, Baos wife, (Pauline Lan), and learned to cook a noodle dish from her. After Bao and his wife fell out due to a petty argument, she started up her own dry-cleaning shop which was where Si-ying first met with Aunt Jin-Dao.

Started out as acquaintances, Si-ying and A-Tuo become good friends with each other after hanging out for some time. A-Tuo started having feelings for Si-ying, but Si-ying only treated him as a friend whom she can confess any thinking in her heart openly, as she likes Ze-Yu. Later, Senior A-Tuo went backpacking overseas. It was during this period that Si-Ying realised that A-Tuo is the one whom she has been waiting for all along.

==Cast==
* Vivian Sung aka Sung Yuhua as Si-ying
* Bruce as Senior A-Tuo
* Megan Lai as A Bu-si 
* Vivian Chow as Proprietress
*Katie Chen as Young proprietress
* Marcus C as Ze-yu
*Hong Yan Xiang as young Ze-yu
* Lee Luo as Bao
* Pauline Lan as Aunt Jin-dao
* Yuri Gao as A-zu

==Reception==
It has grossed New Taiwan dollar|NT$37.1 million in Taipei and Hong Kong dollar|HK$9.3 million in Hong Kong. 

==Production Crew==
*Executive Producer: Giddens Ko, Angie Chai
*Director: Chiang Chin-lin
*Original Screenplay: Giddens Ko
*Productions: K4s Motion Studio, VieVision Pictures, Amazing Film Studio, Star Ritz Productions Co., LTD.

==References==
 

==External links==
* 

 
 
 
 


 
 