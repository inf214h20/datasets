Bitters and Blue Ruin
{{Infobox film
| name           = Bitters and Blue Ruin
| image          =
| image size     =
| caption        =
| director       = Sean Kelley
| producer       = Scott Elwell
| writer         = Scott Elwell 
Sean Kelley
| narrator       =
| starring       =
| music          = Cermit Fluffi
| cinematography = Krista Mazzeo
| editing        = Sean Kelley
| distributor    =
| released       =
| runtime        = 140 minutes
| country        = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Bitters and Blue Ruin (2002 in film|2002) is a black-and-white neo-noir film shot in Philadelphia which centers on the ills of absinthe Substance dependence|addiction.

==Plot==
Written by director  . Though his last book was a big hit, things dont look so rosy this time around; Roddy has writers block.

Things take a further nosedive when Roddy loses control and decks graduate student Sal Unmarino, who slinks back to his advisor and Roddys rival Professor Slavko P. Slavko (John Bravo). Sal spills his guts, painting a picture of a Roddy whos not only desperate to complete the book, but who has lapsed back into his old ways, hitting the cups hard.

Slavco senses that something is not quite right, but even he is shocked when a clandestine search through Roddys office reveals that Roddy has been blowing Sal off and running with a former Hollywood pretty boy, now full-time nobody named Chester Trunche. What in blue blazes could Roddy be up to?...

What follows is the story of a desperate man searching to regain his former inspiration, but instead finds himself sinking deeper and deeper into the pit of dependency and hedonism as he becomes hooked on a potent and mysterious beverage known only as "The Blue Muse".

Roddys quest for the Muse takes him from atop the lofty ivory towers of academia down to the lowest gutters of the seedy under-culture of Philadelphia. But whether hes at a swanky urbane soiree or in the company of garrulous prostitutes, Roddy always finds himself a man out at sea. His newly found Muse has quickly become his only true love.
 dean of the school, a guy who himself has more than a few screws loose. The jig is truly up when Halbfass decides to commit Roddy to an insane asylum. This touches off a chain of events that brings everything crashing down on their heads.

==History==
The film takes its name from a line in the Tom Waits song "9th and Hennepin" which goes as follows:
"And the clock ticks out like a dripping faucet/
til youre full of rag water and bitters and blue ruin"

The film had a world premiere on December 14, 2001, at International House at the University of Pennsylvania.

According to the Kelley and Elwell, an edited and colorized version of the film will likely be released commercially in 2008 under a new title.  Initial reports are that the films title will be changed to "The Third Degree."  Additionally, filming will commence soon on Kelley and Elwells long-rumored comedy "The Luxury Problem."

==External links==
*  
* 

 
 
 
 

 