Vengeance Is Mine (1916 film)
{{Infobox film
| name           = Vengeance Is Mine 
| image          = 
| image_size     = 
| caption        = 
| director       = Rudolf Meinert
| producer       = Rudolf Meinert
| writer         = Ewald André Dupont
| narrator       = 
| starring       = Hans Mierendorff Herr Forstner Marie von Buelow Ernst Pittschau
| music          = 
| editing        = 
| cinematography = 
| studio         = Meinert Film
| distributor    = 
| released       = 1916
| runtime        = 
| country        = Germany Silent German German intertitles
| budget         = 
| gross          = 
}} German silent silent crime film directed by Rudolf Meinert and starring Hans Mierendorff, Herr Forstner and Marie von Buelow. It was part of a popular series of films featuring the fictional detective Harry Higgs. Bock & Bergfelder p. 106  It was the first screenplay written by Ewald André Dupont, who later went on to be a leading filmmaker. 

==Cast==
*  Hans Mierendorff - Harry Higgs
* Herr Forstner - Graf Löwe
* Marie von Buelow
* Ernst Pittschau
* Erna Flemming
* Alice Ferron
* Stefanie Hantzsch
* Franz Ramharter
* Franz Verdier Johannes Müller

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 

 