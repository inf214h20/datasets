A Merry Friggin' Christmas
 
{{Infobox film
| name           = A Merry Friggin Christmas
| image          = A Merry Friggin Christmas poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Tristram Shapeero Anthony Russo Joe Russo Tom Rice
| writer         = Phil Johnston
| starring       = Joel McHale Lauren Graham Clark Duke Oliver Platt Wendi McLendon-Covey Tim Heidecker Candice Bergen Robin Williams
| music          = Randy Edelman
| cinematography = Giovani Lampassi
| editing        = Christian Kinnard
| studio         = Sycamore Pictures
| distributor    = Phase 4 Films Entertainment One Films
| released       =  
| runtime        = 88 minutes 
| country        = United States
| language       = English
}}
A Merry Friggin Christmas is a 2014 American black comedy film directed by Tristram Shapeero and written by Phil Johnston. The film stars an ensemble cast featuring Joel McHale, Lauren Graham, Clark Duke, Oliver Platt, Wendi McLendon-Covey, Tim Heidecker, Candice Bergen and Robin Williams. The film was released on November 7, 2014, by Phase 4 Films.   

==Plot==
Boyd Mitchler and his family must spend Christmas with his estranged family of misfits. Upon realizing that he left all his sons gifts at home, he hits the road with his dad in an attempt to make the 8-hour round trip before sunrise.

==Cast==
 
* Joel McHale as Boyd Mitchler
* Robin Williams as Mitch Mitchler
* Lauren Graham as Luann Mitchler
* Candice Bergen as Donna Mitchler 
* Clark Duke as Nelson Mitchler
* Oliver Platt as Hobo Santa
* Wendi McLendon-Covey as Shauna
* Tim Heidecker as Dave Ryan Lee as Rance
* Pierce Gagnon as Douglas
* Amara Miller as Pam
* Amir Arison as Farhad
* Mark Proksch as Trooper Zblocki Gene Jones as Glen
 

==Production==
Principal photography began in April 2013 in Atlanta, Georgia (U.S. state)|Georgia.  It was one of the last films Williams filmed before his death on August 11, 2014. 

==Release==
The film was released on November 7, 2014 by Phase 4 Films. 

==Reception==
A Merry Friggin Christmas received negative reviews from critics. On Rotten Tomatoes, the film has a rating of 17%, based on 12 reviews, with an average rating of 3.1/10.  On Metacritic, the film has a rating of 26 out of 100, based on 7 critics, indicating "generally unfavorable reviews". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 