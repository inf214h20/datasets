Titan A.E.
 
{{Infobox film
| name        = Titan A.E.
| image       = Titan_AE_One_Sheet.jpg
| caption     = Theatrical release poster
| director    = Don Bluth   Gary Goldman
| producer    = Don Bluth   Gary Goldman   David Kirschner
| screenplay  = Ben Edlund John August Joss Whedon
| story       = Hans Bauer Randall McCormick
| starring    = Matt Damon Bill Pullman John Leguizamo Nathan Lane Janeane Garofalo Drew Barrymore
| music       = Graeme Revell
| studio      = Fox Animation Studios
| distributor = 20th Century Fox
| released    =  
| runtime     = 94 minutes
| language    = English
| country     = United States
| budget      = $75–$90 million
| gross       = $36,754,634
}} animated post-apocalyptic science fiction traditional hand-drawn animation and extensive use of computer generated imagery. Its working title was Planet Ice. 

==Plot==
 
In 3028 A.D., humanity has mastered deep space travel and interacted with several alien species. A human invention called "Project Titan" alarms the Drej, a pure energy-based alien species who attack Earth after breaching the Global Defense System. As the Drej start to attack Earth, Professor Sam Tucker, the lead researcher for "Project Titan", sends his son Cale on one of the evacuation ships with his alien friend Tek while Tucker and other members of his team fly the Titan spacecraft into hyperspace (science fiction)|hyperspace. With Earth destroyed by the energy beam fired by the Drej mothership with the explosion also breaking the Moon, the surviving humans become nomads, generally ridiculed by other alien species.
 salvage yard in an asteroid belt at Tau 14. He encounters Captain Joseph Korso, his female human pilot Akima and the alien crew members of the spaceship Valkyrie named Preed, Gune, and Stith. Korso reveals that Professor Tucker encoded a map in Cales ring to the Titan, humanitys chance of recovery. When the Drej attack the salvage yard, Tek convinces Cale to go with Korso as Cale and Korso escape from the Drej. Cale realizes the Drej want him dead before he can find the Titan. On the planet Sesharrim, the bat-like Gaoul interpret the map and discover the Titan is hidden in the Andali Nebula. Drej fighters arrive and capture Cale and Akima. The Drej eventually discard Akima and extract the Titans map from Cale.

Korsos crew rescues Akima while Cale eventually escapes in a Drej ship and rejoins the group. While visiting the human space station called New Bangkok for repairs, Cale and Akima discover that Korso and his assistant Preed are working with the Drej to destroy the Titan in exchange for money. Cale and Akima are caught by Preed, and it is mentioned by Korso that the Drej have killed Sam Tucker. After escaping from Korso, Cale and Akima are stranded on New Bangkok. With the help of New Bangkoks colonists, Cale and Akima salvage a small spaceship named Phoenix and set out to find Titan.
 create an Earth-like planet including the native animal and plant life thanks to stored DNA samples but has lost the energy necessary for the reactor to start the process after its initial escape from Earth. Cale also finds a recording made by his father in the event that the Drej had gotten to him first and in the event that Cale found the Titan. The recording urges Cale to finish the job of preserving the human race. After killing Preed (who was earlier bribed by the Drej against him), Korso attempts but fails to get Cales ring. Moments later, the Drej attack the Titan, but are diverted by the Valkyries remaining crew. Cale modifies the Titan to absorb the energy beam fired by the Drej mothership, aided by a repentant Korso who sacrifices his life in the process. The Drej mothership is vaporized after being fired upon and the now active Titan molds the asteroid belt into a new planet named "Bob" jokingly by Cale and "New Earth" by Akima.

While on "New Earth", Cale and Akima witness the planet in action. Stith and Gune leave on Valkyrie to another planet as they wave goodbye to their human comrades. Human colony ships (including the New Bangkok) approach the new planet to start life anew. The final shot of the planet is labeled "New Earth (Planet Bob)."

==Cast==
* Matt Damon as Cale Tucker, a male yard-salvager who carries the map to Titan in his ring.
** Alex D. Linz as Young Cale Tucker
* Drew Barrymore as Akima Kunimoto, the pilot of the Valkyrie and Cales love interest.
* Bill Pullman as Captain Joseph Korso, the captain of the Valkyrie and old colleague of Sam Tucker.
* John Leguizamo as Gune, an amphibian-like Grepoan and Korsos chief scientist. fruit bat-like Akrennian and Korsos first mate.
* Janeane Garofalo as Stith, a kangaroo-like Mantrin and the Valkyries munitions officer.
* Ron Perlman as Professor Sam Tucker, Cales father and a researcher who helped to develop Project Titan.
* Tone Loc as Tek, Sam Tuckers alien friend who raises Cale while Sam is away. At some point during the fifteen years between the destruction of Earth and the events of the movie, he has become Blindness|blind. Following the Drejs attack on Tau 14, Tek gives Cale to Korso to look over.
* Jim Breuer as The Cook (profession)|Cook, an anthropomorphic cockroach at Tau 14 who disdains Cale and is killed during the Drejs invasion.
* Christopher Scarabosio as Queen Drej, the leader of the Drej armies who plans to destroy all human species. overseer at Tau 14s salvage yard.
* Charles Rocket as Firrikash, an alien salvage yard worker at Tau 14 who bullies Cale.
** Charles Rocket also voices a Slave Trader Guard, an alien who surprises Preed with his unexpected intelligence.
* Ken Hudson Campbell as Po, an alien salvage yard worker at Tau 14 who bullies Cale. Tsai Chin as Old Woman	
* Crystal Scales as Drifter Woman
* David Lander as Mayor
* Thomas A. Chantler as Male Announcer
* Elaine A. Clark as Citizen
* Roy Conrad as Second Human, a human salvage yard worker at Tau 14
* Julian B. Wilson as Fleet Commander
* Leslie Hedger as First Human, a human salvage yard worker at Tau 14
* Roger L. Jackson as First Alien, an alien salvage yard worker at Tau 14
* Shannon Orrock as Female Announcer
* Alex Pels as Soldier
* Eric Schniewind as Alien
* Stephen Stanton as Alien Prisoner, Colonist

==Digital screening==
Titan A.E. became the first major motion picture screened in end-to-end digital cinema. On June 6, 2000, ten days before the movie was released, at the SuperComm 2000 trade show, the movie was projected simultaneously at the trade show in Atlanta, Georgia as well as a screen in Los Angeles, California. It was sent to both screens from the 20th Century Fox production facilities in Los Angeles via a virtual private network|VPN. 

==Reception==
Titan A.E.received a mixed response. Though it received an Annie Award nomination for Best Animated Feature (which it lost to Toy Story 2),  the film was a box office bomb. After the films failure, Fox Animation Studios was shut down. 

The film opened at #5, with only $9,376,845 for an average of only $3,429 from 2,734 theaters.  The film then lost 60% of its audience in its second weekend, dropping to #8, with a gross of just $3,735,300 for an average of $1,346 from 2,775 theaters.  The film ended up grossing a mere $36,754,634 worldwide ($22,753,426 in the United States and Canada, and $14,001,208 in international markets).  The films budget is estimated at between $75 million    and $90 million.    According to Chris Meledandri, the supervisor of the film, Titan A.E. lost $100 million for 20th Century Fox. 

Film critic Roger Ebert enjoyed it, giving it 3.5/4 stars for its "rousing story", "largeness of spirit", and "lush galactic visuals   are beautiful in the same way photos by the Hubble Space Telescope are beautiful".  He cited the Ice Rings sequence as "a perfect examine   of what animation can do and live action cannot". 
 Best Science Fiction Film at 27th Saturn Awards,  but lost to X-Men (film)|X-Men, another film from 20th Century Fox.

==Prequel novels==
To tie in with the film, a series of prequel novels was released, as well as a comic book mini-series.

* Cales Story - The adventures of Cale, ending with the beginning of the film. The book chronicles Cale growing up on Vusstra (Teks home planet) for ten years and having to move to a different place every time the Drej attack. It also reveals how Cale became resentful of his fathers disappearance and how he came to despise "drifter colonies".

* Akimas Story - The adventures of Akima, ending with the beginning of the film. The book chronicles Akimas life aboard drifter colonies, and reveals whence Akima learned her karate skills, her friendship with Stith, and her reason to find the Titan.

* Sams Story - A Dark Horse Comics prequel comic telling the story of Sam Tucker and his crew, and their quest to hide the Titan.

==Home media release==
Twentieth Century Fox Home Entertainment - R1 (America) - DVD 
* Picture format: 2.35:1 (Anamorphic)  
* Soundtrack(s): English (DTS and Dolby Digital 5.1 / 2.0 Surround) French (Dolby Digital 2.0 Surround)
* Subtitles: English and Spanish
* Extras:
* Audio Commentary with Director-Producers Don Bluth and Gary Goldman
* The Quest for Titan (Fox TV special)
* 2 Deleted Scenes
* 2 Extended / Alternate scenes
* 2 Theatrical Trailers
* 2 TV Spots
* Gallery of Production Artwork
* Over My Head (Music Video) by Lit
* THX Optimizer
* DVD-ROM: Including weblinks and game link
* Case type: Keep Case
* Notes: THX Certified

==Soundtrack==
The soundtrack album was issued by Capitol Records.
 Lit
# "The End is Over" — Powerman 5000
# "Cosmic Castaway" — Electrasy
# "Everything Under the Stars" — Fun Lovin Criminals
# "Its My Turn to Fly" — The Urge Texas
# "Not Quite Paradise" — Bliss 66
# "Everybodys Going to the Moon" — Jamiroquai Splashdown
# "Renegade Survivor" — The Wailing Souls
# "Down to Earth" — Luscious Jackson

===Score===
The movies underscore was composed by Graeme Revell. In 2014 La-La Land Records released a limited edition score album of 1500 copies, containing most of what he wrote.

# Prologue/Drej Attack - 6:33
# Wow - 0:44
# You’ve Been a Little Uppity - 0:52
# The Human Race Still Matters - 0:41
# We’ve Got Company - 0:37
# The Ring Is the Key - 0:56
# Start Running, Keep Running - 2:48
# Exhale - 0:54
# Do You Know What This Means? - 0:27
# Fight the Good Fight Precious - 3:12
# I See Your Father - 1:21
# The Broken Moon - 3:09
# The Guaol - 0:40
# The Dreaded Drej - 2:55
# Hydrogen Forest Chase - 2:31
# Earth Is Never Lost/Mother Drej - 3:56
# Stith Kicks Butt/Captive/What Kept You? - 3:41
# The Map Evolves - 1:01
# I Miss Him/Nightmare of the Drej - 1:08
# Korsos Double Cross/You Got a Problem With That? - 2:46
# Recovery - 2:47
# Launch/Hall of Mirrors - 2:55
# Don’t Lose ‘Em - 3:47
# I Remember - 1:10
# Mistrust - 1:21
# Its Up to You - 1:21
# Face It Cale, You’ve Lost - 1:04
# Sleep Tight, Little Friend - 2:33
# Power Struggle - 6:39
# Creation/Bob - 3:58
# Bonus Track: Creation (orchestra only) - 2:09
# Bonus Track: Prologue (with alternate opening) - 6:19

==See also==
* List of films featuring space stations

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 