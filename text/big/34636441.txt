Django 2
{{Infobox film
 | name =
 | image =   Django 2 dvd.jpg
 | director = Nello Rossati
 | writer =
 | starring = Franco Nero Donald Pleasence
 | music =  Gianfranco Plenizio
 | cinematography = Sandro Mancori
 | editing =
 | producer =
 | distributor =
 | released =
 | runtime =
 | awards =
 | country =
 | language =  Italian
 | budget =
 }} 1987 Cinema Italian spaghetti western film directed by Nello Rossati. It is the only official sequel of Django (1966 film)|Django.   

==Plot==
Twenty years after the event in the first Django (film)|Django, the title character has left the violent world of gunslinger to become a monk. Living in a monastery, he wants no more of the violent actions he perpetrated. Later, he learns that along the way, he had a daughter who is working for ruthless criminals in a mine, for which they hope to get rich from the spoils. Determined to find his daughter and nail the bad guys, Django gets some arms and goes on the warpath.

== Cast == Django
* Donald Pleasence as Gunn Christopher Connelly as El Diablo Orlowsky
* Licinia Lentini as Countess William Berger as Old Timer

== Production ==
The project was born in parallel with Duccio Tessaris Tex e il signore degli abissi, in view of a commercial revival of the Italo-western cinema. After the commercial failure of Tex, Sergio Corbucci, who had initially accepted the direction of the sequel and had just written the story of the film, eventually refused to shoot it. 
 Christopher Connelly, who died of cancer the following year. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 