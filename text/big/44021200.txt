Khilona (1942 film)
{{Infobox film
| name           = Khilona
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Amar Pictures
| writer         =  
| narrator       =  Kanhaiyalal 
| music          = Khemchand Prakash
| cinematography = 
| editing        = 
| distributor    =
| studio         = Ranjit Studios
| released       = 1942
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1942 social Kanhaiyalal and Pratima Devi. 

==Cast==
* Snehprabha Pradhan as Asha
* P. Jairaj as Amar
* Prabha as Maya
* Satish as Kishore Kanhaiyalal 
* Pratima Devi as Lady Mazumdar
* Baburao Sansare as Doctor
* Nagendra as tailor
* Pesi Patel

==Music== Calcutta to Bombay, where he mainly worked for Ranjit Movietone.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1
| Bindiya Mori Chamakan Lagi 
| Snehaprabha Pradhan
|-
| 2
| Mile Jule Sab Rang 
| Sumati Trilokekar, Khan Mastana
|-
| 3
| Dil Unko Dhundta Hai Hum Dil Ko Dhundte Hain
| Snehaprabha Pradhan, Khan Mastana
|-
| 4
| Jamuna Kinare Mera Baagh Malaniya Radheshyam Ki
| Snehaprabha Pradhan
|-
| 5
| Mai Phiru Bajariya Saari Re 
| Snehaprabha Pradhan
|-
| 6
| Ham Jinke Mehaman Bane Hain
| Snehaprabha Pradhan
|-
| 7
| Bhor Bhaye Ghar Aaye Balam More 
|
|-
| 8
| Khilauna Hai Tu 
| Snehprabha Pradhan
|}

==References==
 

==External links==
* 

 

 
 
 

 