JLA Adventures: Trapped in Time
 
{{Infobox film
| name = JLA Adventures: Trapped in Time 220px
| alt = 
| caption = DVD cover
| narrator = 
| director = Giancarlo Volpe
| producer = Giancarlo Volpe 
| screenplay =  Michael Ryan
| based on =  Laura Bailey Grey DeLisle-Griffin Peter Jessop Fred Tatasciore
| music = Frederik Wiedmann
| cinematography =
| editing = Bruce A. King
| studio = Warner Bros. Animation DC Entertainment
| distributor = Warner Home Video
| released =  
| runtime = 52 minute
| country = United States
| language = English
| budget =
| gross =
}} Justice League of America. It was first released on January 21, 2014, and features Diedrich Bader, Travis Willingham and Kevin Michael Richardson, reprising their roles as Batman, Gorilla Grodd and Black Manta.   

==Plot== Legion of cryogenic rays from orbital satellites to expand the Earths polar ice caps in the Arctic Ocean, causing a massive drop in sea levels and creating new islands they intend to rule. Superman and the Justice League confront the Legion, dividing their forces to fight the Legion and destroy the satellites. In the ensuing space battle, Robin purposely crashes his spacecraft into the satellite array. Refusing to accept defeat, Captain Cold overloads the remaining satellite, which falls back to Earth and entombs Luthor in an ice sheet shortly before the satellite explodes. The League is triumphant and believes Luthor to be dead.
 31st century, Karate Kid are visiting a museum dedicated to the exploits of the Justice League. They find that Luthor has been in suspended animation for nearly a millennium after being discovered and excavated in the 29th century and placed on display. Karate Kid accidentally releases Luthor from the ice. Luthor awakes nearly 1,000 years into the future; he explores the museum, discovers Supermans secret identity and locates an ancient item called the Eternity Glass, which allows its user to manipulate the fabric of time and space. Taking Captain Colds weapon, he freezes Dawnstar and Karate Kid. He then uses the Eternity Glass to unleash Time Trapper as his servant, allowing him to travel to the point when he last fought the League. Vengeful, Luthor plans to return and destroy Superman. The young heroes break free and follow.
 Kent family from adopting Kal-El in Smallville. The young heroes enlist the Justice Leagues aid to stop them, and travel to the Hall of Justice in Washington, D.C.   After a misunderstanding, they explain their story and verify its validity through Wonder Womans Lasso of Truth. The Flash, Aquaman, and Cyborg travel into the past to prevent Bizarro, Toyman, Cheetah and Solomon Grundy from sending the baby Kal-El back into space, while Superman and the rest of the League attempt to stall Luthors Legion in the present. 

Despite the Justice Leagues efforts, the Legion sends Kal-Els ship away from Earth. Without Superman, there is nobody left in the past to inspire individuals with extraordinary abilities to become superheroes. This would cause a temporal paradox, enabling Time Trapper to erase the Justice League in the present. Unwilling to risk being erased from existence, Dawnstar and Karate Kid retreat. In the absence of the Justice League, the Legion pillages the world, robbing banks and depositories, and leaving Washington D.C. in ruins. The young heroes realize that they can induce their own paradox by ensuring Lex Luthors past counterpart never traveled to the 31st century so he could not steal the Eternity Glass and return to the past. The young heroes discover Luthors tomb with Dawnstars tracking ability, where Karate Kid breaks the ice and frees Luthors earlier self. However, the Time Trapper is immune to the paradox because he exists outside the timeline, and with Luthors counterpart freed, Time Trapper is free to do as he pleases because Luthor is no longer his master.
 
The Time Trapper banishes the future Luthor from existence and tries to remake the current world in his image. Superman and the Justice League, who exist again because of the latters absence, confront the villain. Time Trapper is too powerful for the League to fight. Karate Kid discovers that Time Trapper is composed entirely of dark matter. Karate Kid realizes that only Dawnstars light-based powers can defeat Time Trapper. Dawnstar charges Time Trapper and the League defeat him, reverting the Eternity Glass, which imprisons Time Trapper once more. The Justice League thanks the young heroes and offers them the opportunity to stay. Karate Kid and Dawnstar decline, stating they must return to their time. They use the Eternity Glass to return to the 31st century. Superman recovers Luthor and sends him to Blackgate Prison. Gorilla Grodd assumes control of the Legion of Doom. Returning to their own time, Karate Kid and Dawnstar discover that a statue of Luthor has replaced a statue of Superman. Believing they have altered history in some unforeseen fashion, they immediately return to the 21st century to help the Justice League once more.

==Cast==
 
* Diedrich Bader as Batman Laura Bailey as Dawnstar  Karate Kid
* Corey Burton as Time Trapper, Captain Cold Grey DeLisle-Griffin Superbaby
* Peter Jessop as Superman
* Fred Tatasciore as Lex Luthor Robin
* Michael David Donovan as Bizarro Jonathan Kent  Martha Kent
* Liam OBrien as Aquaman, Batwing Computer Solomon Grundy The Flash, Taxi Driver Avery Kidd Cyborg
* Travis Willingham as Gorilla Grodd
 

==Release==
The film was first released on DVD exclusively at Target stores on January 21, 2014.  The DVD, and digital download, was later available generally on May 20, 2014. 

==Reception==
 
Chris Sims of comicsalliance.com wrote, "Trapped In Time might not have the pedigree of being inspired by a best-selling comic, but it’s exactly the kind of project DC’s animated movies should be focusing on".  Cliff Wheatley from IGN gave the film 8/10, saying "JLA Adventures: Trapped in Time is a call back to the glory days of DC’s animated offerings. It might not have the refined animation of the Bruce Timm era, but it’s certainly a ton of fun despite its flaws." 

==References==
 

== External links ==
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  
 
 
 