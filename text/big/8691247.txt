The Baker's Wife (film)
 
{{Infobox Film
| name           = The Bakers Wife
| image          = Bakers Wife Film.jpg
| caption        = 
| director       = Marcel Pagnol
| producer       = Charles Pons
| writer         = Jean Giono Marcel Pagnol
| starring       = Raimu Ginette Leclerc Fernand Charpin
| music          = Vincent Scotto
| cinematography = Georges Benoît
| editing        = Suzanne de Troeye
| distributor    = 
| released       = 7 September 1938
| runtime        = 133 minutes
| country        = France
| language       = French
| budget         = 
}} French comedy American musical The Bakers Wife.

==Plot==
The peacefulness of a village is shattered when the wife of the local baker runs off with a handsome shepherd.  In his despair, the baker becomes heartbroken and no longer bakes bread.  The villages organize forces to bring the wife back to her husband, and back to their daily bread.

== Cast ==
* Raimu as Aimable Castanier
* Ginette Leclerc as Aurélie Castanier
* Fernand Charpin as Le marquis Castan de Venelles
* Robert Vattier as Le Curé
* Charles Blavette as Antonin
* Robert Bassac as Linstituteur
* Marcel Maupi as Barnabé
* Alida Rouffe as Céleste
* Odette Roger as Miette
* Yvette Fournier as Hermine
* Maximilienne as Melle Angèle
* Charblay as Le boucher
* Julien Maffre as Pétugue
* Adrien Legros as Barthelemy
* Jean Castan as Esprit

== Awards == National Board of Review Awards (1940), Best Foreign Film New York Film Critics Circle Awards (1940), Best Foreign Film

== External links ==
* 
*  

 

 
 
 
 
 
 
 
 
 


 
 