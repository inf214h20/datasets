The Sons of Eilaboun
{{Infobox film
| name = The Sons of Eilaboun
| image = The Sons of Eilaboun.jpg
| caption =original movie dvd cover and poster
| director = Hisham Zreiq
| producer = Hisham Zreiq
| writer = Hisham Zreiq 
| music = Duncan Patterson Marcel Khalife
| cinematography = Amir Reshpon
| editing = Hisham Zreiq
| distributor = 
| released =  
| runtime = 24 minutes
| country = Germany Palestinian territories Israel
| language = Arabic, English
| budget = 
}} Nakba in Palestinian exodus of 1948 in Eilaboun, a village in the Northern Galilee between Nazareth and the Sea of Galilee. In the incident, fourteen men were killed and twelve of them were executed. The villagers were expelled to Lebanon and became refugees for few months, before managing to return clandestinely.   

The film is the story of the film makers family, and specially his fathers story. Hisham Zreiq explained why he made the film when he said "He choked and his eyes were full of tears, and with a trembling voice he said I remember it as if it has just happened -- this is the way he ended the story, the story of a nine-year-old boy from a small village called Eilaboun, in Palestine 1948, the story of my father, when he was a refugee". 

Hisham Zreiq was honored by Ramiz Jaraisy the mayor of Nazareth and by Dr. Hana Sweid is an Israeli Arab politician and member of the Knesset from Eilaboun,  where Ramiz Jaraisy described the film as important work, that tells the Palestinian story in a contrast with the dominant Israeli version.  
Gilad Atzmon, an Israeli-born British, political activist and writer, wrote in an article: "Zreiq manages to deliver a very deep and authentic reading of Palestinian history. He also manages to portray the intense emotional impact of the Nakba on those who survived the horror."   

==Synopsis==
The film starts with Melia Zreiq, an old woman from Eilaboun, saying: "I hope God will bring peace to this land, and let the peoples live together - a good life. I hope there will be peace". Historian Ilan Pappe talks about Plan Dalet, a plan that David Ben-Gurion and the Haganah leaders in Palestine worked out during autumn 1947 to spring 1948. Pappe discusses the details of the plan, and how was it carried out. On October 30, 1948, the Israeli army entered Eilaboun at approximately 5 AM. They then forced the villagers together in the main square of the village. They chose seventeen young men. Five of them were taken as human shield, and the rest of the  twelve were killed, each in a different location. This all happened after the expulsion of the rest of the village to Lebanon, where they became refugees after a five days forced march to Lebanon. After a United Nations peace keeper observed and reported Israel was forced to allow the people back. 

== Awards and festivals ==
The film won the Al-Awda award in Palestine 2008,  and was screened in several festivals and events, such  as:

* Sixth Annual International Al-Awda Convention 2008, California, USA  
* Boston Palestine Film Festival 2008, USA  
* International İzmir Short Film Festival 2008, Izmir, Turkey  
* Amal The International Euro-Arab film Festival 2008, Spain  
*  , Tunisia  
* Regards Palestiniens, Montreal, Canada  
* Chicago Palestine Film Festival, Chicago, USA  
* 13th Annual Arab Film Festival, Los Angeles, USA  
* Sixth Twin Cities Arab Film Festival, Minnesota, USA   
* Palestine Film Festival in Madrid, 2010, Spain   
* Al Ard Doc Film Festival, 2011, Cagliari, Italy  
* Toronto Palestine Film Festival, 2012, Toronto, Canada 

==References==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 