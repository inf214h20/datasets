Geheimakte W.B.1
Geheimakte German drama film directed by Herbert Selpin and starring Alexander Golling, Eva Immermann and Richard Häussler. The film portrays Wilhelm Bauer and his work on developing the submarine.  It was based on the novel Der Eiserne Seehund by Hans Arthur Thies.

==Cast==
* Alexander Golling - Wilhelm Bauer
* Eva Immermann - Sophie Hösly
* Richard Häussler - Großfürst Konstantin
* Herbert Hübner - Admiral Brommy
* Wilhelm P. Krüger - Vater Hösly
* Günther Lüders - Schiffsbauer Karl Hösly
* Willi Rose - Werftmeister Schultze
* Gustav Waldau - König Maximilian
* Justus Paris - Vorsitzender des Gerichts
* Theo Shall - Mr. Wood
* Walter Holten - General
* Andrews Engelmann - Russischer Intrigant Trotzky
* Karl Meixner - Senator
* Viktor Afritsch - von Klamm
* Albert Arid - Offizier der russischen Hafenwache
* Karl Hanft - Tony
* Michl Lang - Oberhofen
* Richard Ludwig - Major der russischen Wache
* Philipp Manning - Holm
* Friedrich Ulmer - Dr. Hoffmann
* Paul Wagner - Begleiter des König Maximilian
* Aruth Wartan - Kenwolsky
* Dolf Zenzen - Begleiter Trotzkys am bayerischen Hof

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Hadley, Michael L. Count Not the Dead: The Popular Image of the German Submarine. McGill-Queens University Press, 1995.

==External links==
* 

 

 
 
 
 
 
 


 