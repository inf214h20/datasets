Ghilli
 
 
 

{{Infobox film
| name           = Ghilli
| image          = Ghilli308.jpg
| caption        = Film poster Dharani
| Dharani Barathan  (Dialogues ) 
| producer       = A. M. Rathnam
| writer         = Gunasekhar Vijay Trisha Trisha Prakash Raj Ashish Vidyarthi Dhamu Nagendra Prasad Vidyasagar
| cinematography = Gopinath
| editing        = V. T. Vijayan
| art director   = Maniraj Sri Surya Movies
| released       =  
| runtime        = 167 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =   
}}
Ghilli (English: Risk taker) is a 2004   over the years.    It was dubbed in Hindi as Keertimaan.

==Plot== as Ghilli (Vijay (actor)|Vijay), is a wannabe kabbadi champion who is the son of Assistant Commissioner of Police Sivasubramanian (Ashish Vidyarthi). Sivasubramanian is not fond of his son, constantly chiding him for his lack of interest in studies and his love for kabaddi. His mother (Janaki Sabesh), on the other hand, dotes on him and his younger sister Bhuvana (Baby Jennifer) is the typical sharp and inquisitive schoolgirl, constantly getting Velu into trouble with his father, but nevertheless still adores him.

In Madurai, there is a ruthless factionalist leader Muthupandy (Prakash Raj), who is obsessed with a beautiful girl Dhanalakshmi (Trisha Krishnan) and would do anything to marry her. Muthupandi kills Dhanalakshmis elder brother as he rejects his offer to marry Dhanalakshmi. Then Dhanalakshmis younger brother is also killed by Muthupandi when he tries to avenge his brothers murder. Dhanalakshmis father is a meek person who gets horrified by Muthupandis acts and asks Dhanalakshmi to go away from the place and lead a peaceful life at her uncles place in the USA by giving her the necessary certificates and money. Muthupandi catches her when she starts fleeing because by coincidence Dhanalakshmi gets into one of the lorries owned by him. At this juncture, Velu- who is in Madurai to play in a kabaddi tournament- rescues Dhanalakshmi from the hands of Muthupandi and takes her to Chennai.
 airport in Punjab in the final of the National League. Sivasubramanian, enraged that his son is a wanted criminal and yet is playing in a kabaddi match, goes to the stadium to arrest Velu. By now, Velu too has fallen in love with Dhanalakshmi and begins to miss her, only to spot her in the stadium during the match. Velus lack of focus in the game is replaced by his best on seeing Dhanalakshmi, winning Tamil Nadu the championship. After winning the championship, Velu is arrested by his father, but is then stopped by Muthupandi, who wants to fight Velu, having been incited by Dhanalakshmi to do so to prove his worth. During the fight, Muthupandi accidentally falls on a floodlight, killing him. The movie ends with Velu and Dhanalakshmi finally united.

==Cast== Vijay as Saravanavelu, the protagonist
* Trisha Krishnan as Dhanalakshmi, Saravanavelus love interest
* Prakash Raj as Muthupandi, the antagonist Ghilli (DVD): opening credits from 1.05 to 1.25 
* Ashish Vidyarthi as Sivasubramanian, Saravanavelus father 
* Tanikella Bharani as Muthupandis father 
* Janaki Sabesh as Saravanavelus mother Ghilli (DVD): closing credits from 157.25 to 157.30 
* Baby Jennifer as Bhuvana 
* Dhamu as Otteri Nari 
* Vinod (actor Vikram (actor)|Vikrams father) as Dhanalakshmis father 
* T. K. Kala as Muthupandis mother 
* Nagendra Prasad as Prasad
* Aadukalam Murugadoss as Aadhivasi 
* Mayilsamy as Narayana
* Brahmanandam as priest, his comedy sequence is one of the most popular in Tamil cinema.
* Appukutty as Brahmanandhams assistant Ponnambalam as Arivumathi
* Nandha Saravanan as Saravanavelus enemy (opposite team leader)  Paandu
* Karate Raja as Muthupandis henchman 
* Ammu
* Vimal as Saravanavelus team mate (uncredited role)
* Pandi (actor) as road side seller

==Production==
Vijay expressed interest in starring in a Tamil remake of the successful Telugu action flick Okkadu and prompted producer A. M. Rathnam to purchase the remake rights of the film. Dharani was finalized as the director, whose previous film Dhool under Rathnams production had been a financial success. Dharani made minor changes to the script to suit Vijays image and Tamil audiences taste. Dharanis regular crew members including cinematographer Gopinath and music director Vidyasagar joined the film, while Rocky Rajesh and Raju Sundaram were chosen to choreograph the stunts and dances, respectively.

Simran Bagga was roped to play the female lead opposite Vijay, But due to her wedding arrangements Trisha Krishnan was replaced to play the female lead, while Prakash Raj was signed on to reprise the villains character from the original. Dhamu and Prabhu Devas brother, Nagendra Prasad were recruited to essay supporting roles. Ashish Vidyarthi, Janaki Sabesh and Baby Jennifer were selected to portray Vijays parents and sister, respectively. Playback singer T. K. Kala made her acting debut with this film.  Vimal who went on to act in films like Pasanga (2009) and Kalavani (2010) appeared in a small role as one of Vijays teammates and also worked as "unofficial" assistant director. 

Filming began in mid-2003, after Vijay had completed his action flick with director Ramana; the film was completed by early 2004. Filming took place in Chennai surrounding the coastal areas like Mylapore and Besant Nagar. The films introduction fight scene and a song were shot in a costly set in Prasad studios.  A lighthouse set was also erected. Other action and chasing sequences were canned near the Meenakshi Amman Temple in Madurai. The climax scene was shot in a crowd of one hundred thousand people on a Vinayagar Chaturthi occasion.   

==Soundtrack==
{{Infobox album|  
| Name        = Ghilli
| Longtype    =
| Type        = Soundtrack Vidyasagar
| Cover       =
| Caption     =
| Released    =
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Five Star Audio Vidyasagar
| Last album  = Madhurey (2004)
| This album  = Ghilli (2004)
| Next album  = Sullan (2004)
}}
 Kabilan and Hindi movie Boss (2013 Hindi film)|Boss starring  Akshay Kumar.

{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| title1 = Kabadi
| extra1 = Maran, Jayamoorty
| length1 = 01:44
| title2 = Arjunaru Villu
| extra2 = Sukhwinder Singh
| length2 = 04:27
| title3 = Sha La La
| extra3 = Sunidhi Chauhan
| length3 = 04:30
| title4 = Appadi Podu
| extra4 = Krishna Kumar Kunnath|KK, Anuradha Sriram
| length4 = 05:53
| title5 = Soora Thenga Tippu
| length5 = 04:03
| title6 = Kokkarakko
| extra6 = Udit Narayan, Sujatha Mohan
| length6 = 05:00
}}

==Release==
Ghilli was due for release on 9 April but later got postponed for a week and opened in over 150 theaters on 16 April. Though the reason for the postponement was not given out, rumours were that producer A M Rathnams creditors put pressure on him to settle his accounts before release. Another reason was that Vijay got cold feet after the Udhaya debacle as the film had not even taken an opening. 

===Reception===
The film opened to highly positive reviews, and became the biggest commercial success of Vijays career then. Sify appreciated the film for its fast moving screenplay and said as a racy entertainer giving a rating of 3.5/5 stars.  Nowrunning.com stated that "Gilli, story wise, is neither fresh popcorn nor spicy samosa found in theaters.. but the screenplay and overall treatment is as fresh and appetizing as full meals after a long day. and gave an overall rating of 3/5 stars  Oneindia gave the film 4/5 stars and stated the film as a "Blockbuster".  The Hindu stated that "Vijay, the hero whom the masses today identify with, and Prakashraj, the inimitable villain in tow, this remake of the Telugu flick, "Okkadu," comes a clear winner". 
Indiaglitz called the film that Gilli is an out and out entertainer and commented that "Vijay and Trisha on track with another blockbuster".  Rediff stated that, "Dont miss Gilli. 

===Accolades===
*  National award for best dialogue "chellam" Prakash Raj₹
* Filmfare Award for Best Dance Choreographer – South – Raju Sundaram Vijay
* Dinakaran Best Actor Award – Vijay
* Film Today Best Actor Award – Vijay
* Dinakaran Best Villain Award – Prakash Raj

==Remakes and character map==

{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" Ajay (2006) Jor (2008) (cinema of West Bengal|Bengali) || Tevar (2015) (Bollywood|Hindi) 
|- Vijay || Puneeth Rajkumar || Jeetendra Madnani || Arjun Kapoor 
|-
| Bhumika Chawla || Trisha Krishnan || Anuradha Mehta || Barsa Priyadarshini || Sonakshi Sinha 
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 