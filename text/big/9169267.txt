Amu (film)
{{Infobox Film|
  name     = Amu|
  image          =Amu poster small.jpg  |
  director       = Shonali Bose|
  writer         = Shonali Bose|
  starring       = Konkona Sen Sharma  Brinda Karat Ankur Khanna|
  producer       = Shonali Bose Bedabrata Pain|
  distributor    =  |
  cinematography = Lourdes Ambrose|
  editing = Bob Brooks|	 
  released   = 2005|
  runtime        = 102 minutes |
  language = English  |
  country = India|
  music = Nandlal Nayak
}}

Amu is a critically acclaimed 2005 film directed by Shonali Bose, based on her own novel by the same name.  It stars Konkona Sen Sharma, Brinda Karat, and Ankur Khanna. The film premiered at the Berlin Film Festival and the Toronto Film Festival in 2005.  

==Plot==
Amu is the journey of Kajori Roy (Konkona Sen Sharma), a 21-year-old Indian American woman who has lived in the US since the age of 3. After graduating from UCLA Kaju goes to India to visit her relatives. There she meets Kabir (Ankur Khanna), a college student from an upper-class family who is disdainful of Kaju’s wide-eyed wonder at discovering the "real India". Undeterred, Kaju visits the slums, crowded markets and roadside cafes of Delhi. In one slum she is struck by an odd feeling of déjà vu. Soon after she starts having nightmares. Kabir gets drawn into the mystery of why this is happening, particularly when he discovers that she is adopted.

Meanwhile Kaju’s adoptive mother – Keya Roy, a single parent and civil rights activist in LA, arrives unannounced in Delhi. She is shocked to discover that Kaju has been visiting the slums. Although Kaju mistakes her mother’s response to a typical Indian over-protectiveness, Keya’s fears are more deeply rooted.
 the massacre assassination of Indira Gandhi, the Prime Minister of India. Kabir learns that his father was instrumental in organizing the riots, as well as guilty of failing to stop Kajus father from being killed. Kabir confronts his father who tries to justify his actions. Keya finally tells Kaju the truth; her birth name is Amu Singh and her Sikh father and younger brother were killed in the riots while her mother hanged herself in a refugee camp.

== Cast ==

*Konkona Sen Sharma as  Kajori Roy
*Brinda Karat as Keya
*Ankur Khanna as Kabir 
* Kuljeet Singh as Gurbachan Singh
*Bharat Kapoor as Arun Sehgal
*Lushin Dubey as Meera Sehgal

==Production==
The films production was marred by obstacles like a reputed production house backing out in the last moment, and threats from local goons during the shooting of the  , 6 August 2008. 

==Reception== Time Out, New York, May 24–30, 2007.  Further a Rediff review states, "If Fahrenheit 9/11 can, so can Amu."  According to the Indiatimes, "What sets Amu  apart is its historical astuteness and its creator’s unblinking regard for the past, no matter how brutal." 

==Censorship== censor board in India,  which cleared it only with 6 politically motivated cuts, and with an "Adult|A" certificate.  Since — according to Indian law — this made the movie ineligible to be telecast on Indian television, the producers later reapplied for a UA censor certificate. This was when a 10-minute cut was suggested by censors, including removal of all verbal references to the 1984 anti-Sikh riots|riots. Subsequently, the producers decided to forgo the lower certification, and released the movie directly to DVD.  

==Awards==
* 2005:     
* 2005: FIPRESCI Critics Award.
* 2005:  , 25 March 2005. 
* 2005: Teenage Choice Award, Torino, Italy (Cine donne Film Festival).
* 2005: Jury Award, Torino, Italy (Cine donne Film Festival). 
* 2006: Star Screen Award – Best English Film (India)

==References==
 

==External links==
*  
*  
*  
*  , Nirali Magazine, May 2007
*  

 
 
 
 
 
 
 
 
 
 
 
 