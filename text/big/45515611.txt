Hum Hain Kamaal Ke
 
 
{{Infobox film
| name           = Hum Hain Kamaal Ke
| image          = 
| alt            = 
| caption        =
| film name      =  
| director       = Vijay Reddy
| producer       = Ashok Adnani
| writer         = Anwar Khan
| screenplay     =
| story          = Anwar Khan
| based on       =  
| starring       = Sheeba Akashdeep Kader Khan Anupam Kher Aruna Irani Raza Murad Sadashiv Amrapurkar Sujoy Mukherjee Rucha Gujarathi
| narrator       =  
| music          = Naresh Sharma & Raju Singh
| cinematography = Ashok Rao
| editing        = V.N. Mayekar
| studio         = Evershine Studio
| distributor    =  
| released       =  
| runtime        = 
| country        =  
| language       = Hindi
| budget         = 
| gross          =
}}
 comedy film released on 21 September 1993. The film had a multi star cast; Sheeba Akashdeep, Kader Khan, Anupam Kher, Aruna Irani, Raza Murad, Sadashiv Amrapurkar, Sujoy Mukherjee and Rucha Gujarathi. This movie was produced by Ashok Adnani, directed by Vijay Reddy and story was written by Anwar Khan.      

==Plot== blind since birth. Both friends developed special skills and also good coordination between themselves, due to which others fail to realize that they are differently abled. Jebago (Raza Murad) is head of some gangsters. Nilambar and Pitamber attempt to solve a murder mystery with series of comic mishaps.    

==Cast==
{| class="wikitable sortable"
|-
! # !! Actor !! Role

|-

| 01 || Sheeba Akashdeep || Shallu
|-

| 02 || Kader Khan || Pitamber
|-

| 03 || Anupam Kher || Nilamber
|-
| 04 || Aruna Irani || Mrs. Lado Godbode
|-
| 05 || Raza Murad || Sibago
|-
| 06 || Sadashiv Amrapurkar || Police Inspector Godbode
|-
| 07 || Sujoy Mukherjee || Vishal       (as Sujoy Mukerji)
|-
| 08 || Rucha Gujarathi || Baby Rucha (Sheeba Akashdeep|Sheebas sister)
|-





|}

==See also==
 
* Cinema of India
* Bollywood

== References ==
 
 
 
 

 