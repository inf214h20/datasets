È pericoloso sporgersi
{{Infobox film
| name           = È pericoloso sporgersi
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Jaco Van Dormael
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Walther van den Ende
| editing        = Susana Rossberg
| studio         = 
| distributor    = 
| released       =  
| runtime        = 12 minutes
| country        = Belgium French
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} 1984 Belgium|Belgian short film written and directed by Jaco Van Dormael. The short film was shot in 1984 in Belgium. The film starred Mathieu Chemin, Joëlle Waterkeyn and Dirk Pauwels. It was produced by Iblis Films (Brussels).    È pericoloso sporgersi tells the story of a child who has been thrust into a position where he must make an impossible decision. 

The film won the Grand Prix in international competition at the Clermont-Ferrand#Culture|Clermont-Ferrand International Short Film Festival and received the Audience Award for Best Belgian Short Film at the 1985 Brussels International Fantastic Film Festival.  It was also screened at the Osaka European Film Festival in 1985.  In 2011, it appeared at the Sottodiciotto Filmfestival held in Turin in the retrospective dedicated to Van Dormael. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 