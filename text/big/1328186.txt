Female Trouble
 
 
{{Infobox film
| name           = Female Trouble
| image          = Femaletroubleposter.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = John Waters
| producer       = John Waters 
| writer         = John Waters  Divine David Susan Walsh Michael Potter
| music          = John Waters Bob Harvey
| cinematography = John Waters
| editing        = John Waters Charles Roggero Dreamland Saliva Films
| distributor    = New Line Cinema
| released       =  
| runtime        = 97 minutes   92 minutes   89 minutes  
| country        = United States
| language       = English
| budget         = $25,000
}} Susan Walsh.

The film is dedicated to Manson Family member Charles "Tex" Watson. Waters prison visits to Watson inspired the "crime is beauty" theme of the film and  in the films opening credits, Waters includes a wooden toy helicopter that Watson made for him.

==Plot==
Dawn Davenport, a regular troublemaker at her all-girls school, receives a failing Geography grade and a sentence of writing lines for fighting, lying, cheating, and eating in class. 

After encouragement from the Dashers, Dawn cuts off Idas hand. Taffy comes home and, after becoming unhinged at the sight of a grown woman in a bird cage with a bloody stump, pleads with her mother to reveal the identity of her real father, which she reluctantly does.
 Hare Krishna movement. Dawn warns her she will kill her if she does. Dawn, now with grotesque hair, make-up, and outfits provided by the Dashers, creates a nightclub act. 

Police allow the Dashers to leave after Donald and Donna claim they are upright citizens caught in a bloody rampage. Dawn flees into a forest but is soon arrested by the police.

==Cast== Divine as Dawn Davenport / Earl Peterson
* David Lochary as Donald Dasher
* Mary Vivian Pearce as Donna Dasher
* Mink Stole as Taffy Davenport
* Hilary Taylor as Young Taffy
* Edith Massey as Ida Nelson
* Cookie Mueller as Concetta Susan Walsh as Chiclet Fryer
* Michael Potter as Gator Nelson
* Ed Peranio as Wink
* Paul Swift as Butterfly
* George Figgs as Dribbles
* Susan Lowe as Vikki
* Channing Wilroy as Prosecutor
* Elizabeth Coffey as Ernestine

==Theme song== title song Waters and set to a pre-existing piece of music.

==Production notes== Dreamlander Vincent Peranio, who created Dawns apartment in a condemned suite above a friends store. 
* Divine chose to perform his own stunts, the most difficult of which involved doing flips on a trampoline during his nightclub act. Waters took Divine to a YMCA, where he took lessons until the act was perfected. Dreamlander Susan prophylactics filled with liver, while the baby (Ramsey McLean) was doused in fake blood. The scene created quite a scandal for Lowes mother-in-law, who arrived on the set in a state of confusion.    executed at US capital punishment was suspended from 1972 to 1976 due to the Supreme Courts ruling in the case of Furman v. Georgia. Maryland didnt formally reinstate capital punishment until July 1, 1975 and its constitutionality wasnt passed until 1976. Furthermore, asphyxiation in the gas chamber was the authorized method of execution, not Electric chair|electrocution.
* On the 2004 DVD Directors Special Comments, Waters states that the original working title of the film was "Rotten Mind, Rotten Face". 

==Reception==
The film has a 79% "Fresh" rating on review aggregator website Rotten Tomatoes. 

 

==Alternate versions== 16mm release 35mm and shown theatrically, it was cut to 89 minutes. This version was the only version seen in the United States for many years. However, a recent restoration was done of the original cut, which runs 97 minutes; it has played at this 97-minute length in Europe, however, since its initial release.
 double in the junkyard sex scene between Dawn Davenport and Earl Peterson)

The film was shown in the 89-minute cut when re-released in 2002.

The 97-minute version is now available on DVD and includes an audio commentary by Waters.

== Legacy ==
Baltimore writer/director Erik Kristopher Myers saw Female Trouble as a fourteen-year-old. "It completely warped my brain," he said. He became an independent filmmaker because of the movie, and went so far as to cast George Stover (the chaplain who walks Divine to the electric chair) in his 2013 thriller Roulette (film). 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 