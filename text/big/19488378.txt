Ronia, the Robber's Daughter (film)
 
{{Infobox film
| name           = Ronia, the Robbers Daughter
| image          = Ronia german poster.jpg
| alt            = 
| caption        = German theatrical poster
| director       = Tage Danielsson
| producer       = Waldemar Bergendahl
| writer         = Astrid Lindgren
| starring       = Hanna Zetterberg Dan Håfström Börje Ahlstedt Lena Nyman
| music          = Björn Isfält
| cinematography = Rune Ericson Mischa Gavrjusjov Ole Fredrik Haug
| editing        = Jan Persson
| studio         = 
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = Sweden
| language       = Swedish
| budget         = SEK 18,000,000 (estimated)
| gross          = SEK 49,396,838 (Sweden)
}} novel of the same title by Astrid Lindgren, and adapted for the screen by Lindgren herself.
 Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Plot==
Ronja, daughter of robber-chief Mattis becomes friends with Birk Borkasson. His father, robber-chief Borka, is the main rival and fiercest enemy of Ronjas father.

==Cast==
*Hanna Zetterberg as Ronja
*Dan Håfström as Birk (also as Dick Håfström)
*Börje Ahlstedt as Mattis
*Lena Nyman as Lovis
*Per Oscarsson as Borka
*Med Reventberg as Undis
*Allan Edwall as Skalle-Per
*Ulf Isenborg as Fjosok
*Henry Ottenby as Knotas
*Björn Wallde as Sturkas
*Tommy Körberg as Lill-Klippen

==Reception==
The film was a major success, becoming the highest-grossing 1984 film in Sweden, {{cite journal
  | last = Holmlund
  | first = Christine
  | title = Pippi and Her Pals
  | journal = Cinema Journal
  | volume = 42.2
  | issue = Winter 2003
  | pages = 4
  | year = 2003}}  More than 1.5 million people attended its screenings in Sweden. {{cite web
  | title = Box office / business for Ronja Rövardotter (1984)
  | url = http://www.imdb.com/title/tt0088015/business
  | accessdate = 2008-07-08}} 

==Awards and honours==
The film won Reader Jury of the "Berliner Morgenpost" and the Silver Berlin Bear ("For a movie of extraordinary fantasy") at the 35th Berlin International Film Festival in 1985. The film was also nominated for a Golden Bear.    

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 