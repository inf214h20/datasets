Nosferatu the Vampyre
 
{{Infobox film
| name           = Nosferatu the Vampyre
| image          = Nosferatu Phantom der Nacht.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Werner Herzog
| producer       = Michael Gruskoff Werner Herzog
| screenplay     = Werner Herzog
| based on       =  
| starring       = Klaus Kinski Isabelle Adjani Bruno Ganz Popol Vuh
| cinematography = Jörg Schmidt-Reitwein
| editing        = Beate Mainka-Jellinghaus Werner Herzog Filmproduktion, München Zweites Deutsches Fernsehen Gaumont   20th Century Fox  
| released       =  
| runtime        = 107 minutes  
| country        = West Germany
| language       = German English Romanian 
| budget         = German mark|DEM2.5 million
| gross          = 
}}
Nosferatu the Vampyre is a 1979 West German  , Germany and Transylvania, and was conceived as a stylistic remake of the 1922 German Dracula adaptation, Nosferatu, eine Symphonie des Grauens. It stars Klaus Kinski as Count Dracula, Isabelle Adjani as Lucy Harker, Bruno Ganz as Jonathan Harker, and French artist-writer Roland Topor as Renfield. There are two different versions of the film, one in which the actors speak English, and one in which they speak German.

Herzogs production of Nosferatu was very well received by critics and enjoyed a comfortable degree of commercial success.    The film also marks the second of five collaborations between director Herzog and actor Kinski,    immediately followed by 1979s Woyzeck (1979 film)|Woyzeck. The film had 1,000,000 admissions in Germany and grossed ITL 53,870,000 in Italy. http://www.imdb.com/title/tt0079641/business  The film was also a modest success in Adjanis home country, taking in 933,533 admissions in France. http://www.jpbox-office.com/fichfilm.php?id=7650 

==Plot==
Jonathan Harker is an estate agent in Wismar, Germany. His boss, Renfield, informs him that a nobleman named Count Dracula wishes to buy a property in Wismar, and assigns Harker to visit the count and complete the lucrative deal. Leaving his young wife Lucy behind in Wismar, Harker travels for four weeks to Transylvania, to the castle of Count Dracula. He carries with him the deeds and documents needed to sell the house to the Count. On his journey, Jonathan stops at a village, where locals plead for him to stay clear of the accursed castle, providing him with details of Draculas vampirism. Harker ignores the villagers’ pleas as superstition, and continues his journey unassisted ascending the Borgo Pass. Harker arrives at Draculas castle, where he meets the Count, a strange, ancient, almost rodent-like man, with large ears, pale skin, sharp teeth, and long fingernails.
 asylum after gypsy boy playing a violin. He is eventually sent to a hospital and raves about ‘black coffins’ to doctors, who then assume that the sickness is affecting his mind.
 log that drink her blood.

Lucys beauty and purity distract Dracula from the call of the rooster, and at the first light of day, he collapses to the floor, dead. Van Helsing arrives to discover Lucy, dead but victorious. He then drives a stake through the heart of the Count to make sure Lucy’s sacrifice was not in vain. In a final, chilling twist, Jonathan Harker awakes from his sickness, now a vampire, and arranges for Van Helsing’s arrest. He is last seen traveling away on horseback, stating enigmatically that he has much to do.

==Cast==
* Klaus Kinski as Count Dracula
* Isabelle Adjani as Lucy Harker
* Bruno Ganz as Jonathan Harker
* Roland Topor as Renfield
* Walter Ladengast as Dr Abraham Van Helsing
* Dan van Husen as Warden
* Jan Groth as Harbormaster
* Carsten Bodinus as Schrader
* Martje Grohmann as Mina
* Rijk de Gooyer as Town official
* Clemens Scheitz as Clerk
* John Leddy as Coachman
* Tim Beekman as Coffin bearer
* Lo van Hensbergen
* Margiet van Hartingsveld

==Production==

===Background===
While Nosferatu the Vampyre s basic story is derived from   silent film Nosferatu, eine Symphonie des Grauens (1922), which differs somewhat from Stokers original work. The makers of the earlier film could not obtain the rights for a film adaptation of Dracula, so they changed a number of minor details and character names in an unsuccessful attempt to avoid copyright infringement on the intellectual property owned (at the time) by Stokers Florence Balcombe|widow. A lawsuit was filed, resulting in an order for the destruction of all prints of the film. Some prints survived, and were restored after Florence Stoker had died and the copyright had expired.    By the 1960s and early 1970s, the original silent returned and was enjoyed by a new generation of movie goers.

Herzog considered Murnaus Nosferatu to be the greatest film ever to come out of Germany,    and was eager to make his own version of the film, with Klaus Kinski in the leading role. In 1979, by which time the copyright for Dracula had entered the public domain, Herzog proceeded with his updated version of the classic German film, which could now include the original character names.

===Filming=== Gaumont and ZDF. As was common for German films during the 1970s, Nosferatu the Vampyre was filmed on a minimal budget, and with a crew of just 16 people. Herzog could not film in Wismar, where the original Murnau film was shot, so he relocated production to Delft, Netherlands.  Parts of the film were shot in nearby Schiedam, after Delft authorities refused to allow Herzog to release 11,000 rats for a scene in the film.  Draculas home is represented by locations in Czechoslovakia.
 dubbed dialogue by voice actors) could be included in the English version of the film. Herzog himself said in 2014 that the German version was more "authentic." 
 mummified bodies of the victims of an 1833 cholera epidemic are on public display. Herzog had first seen the Guanajuato mummies while visiting in the 1960s. On his return in the 1970s he took the corpses out of the glass cases in which they are normally stored. To film them, he propped them against a wall, arranging them in a sequence running roughly from childhood to old age. 

Kinskis Dracula make-up, with black costume, bald head, rat-like teeth and long fingernails, is an imitation of Max Schrecks makeup in the 1922 original. The makeup artist who worked on Kinski was Japanese artist Reiko Kruk. Although he fought with Herzog and others during the making of other films, Kinski got along with Kruk and the four-hour makeup sessions went on with no outbursts from Kinski himself. A number of shots in the film are faithful recreations of iconic shots from Murnaus original film, some almost perfectly identical to their counterparts, intended as an homage to Murnau.    

===Music===
 
 Popol Vuh, Georgian folk song Tsintskaro, sung by Vocal Ensemble Gordela. 

==Release and reception== Silver Bear for an outstanding single achievement.   

Film review aggregator Rotten Tomatoes reports a 95% approval critic response based on 42 reviews, with a "Certified Fresh" and an average score of 7.9/10. 

In contemporary reviews, the film is noted for maintaining an element of horror, with numerous deaths and a grim atmosphere, but it features a more expanded plot than many Dracula productions, with a greater emphasis on the vampires tragic loneliness.    Dracula is still a ghastly figure, but with a greater sense of pathos; weary, unloved, and doomed to immortality. Reviewer John J. Puccio of MovieMet considers it a faithful homage to Murnaus original film, significantly updating the original material, and avoiding the danger of being overly derivative.   

In 2011, Roger Ebert added the film to his "Great Movies Collection". Concluding his four star review, Ebert said:
 "One striking quality of the film is its beauty. Herzogs pictorial eye is not often enough credited. His films always upstage it with their themes. We are focused on what happens, and there are few beauty shots. Look here at his control of the color palette, his off-center compositions, of the dramatic counterpoint of light and dark. Here is a film that does honor to the seriousness of vampires. No, I dont believe in them. But if they were real, here is how they must look."  

==Animal cruelty== Dutch behavioral biologist Maarten t Hart, hired by Herzog for his expertise of laboratory rats, revealed that, after witnessing the inhumane way in which the rats were treated, he no longer wished to cooperate. Apart from travelling conditions that were so poor that the rats, imported from Hungary, had started to eat each other upon arrival in the Netherlands, Herzog insisted the plain white rats be dyed gray. In order to do so, according to t Hart, the cages containing the rats needed to be submerged in boiling water for several seconds, causing another half of them to die. The surviving rats proceeded to lick themselves clean of the dye immediately, as Hart had predicted they would. Hart also implies sheep and horses that appear in the movie were treated very poorly, but does not specify this any further. 

==References==
 

==External links==
*  
*  
*  
* Differences between the US and German versions:   at schnittberichte.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 