Kill!
{{Infobox film   name     = Kiru image          = Kill 313.jpg
| director       = Kihachi Okamoto 
| writer         = Shūgorō Yamamoto  Akira Murao Kihachi Okamoto starring       = Tatsuya Nakadai Etsushi Takahashi Naoko Kubo Shigeru Kōyama
| producer         = Tomoyuki Tanaka
| music         = Masaru Satō cinematography = Rokuro Nishigaki distributor    = Toho Company Ltd.   released   = June 22, 1968 (Japanese release) runtime        = 115 min.  language = Japanese 
}}

  is a 1968 film directed by Kihachi Okamoto,  written by Akira Murao, Kihachi Okamoto, and Shūgorō Yamamoto and starring Tatsuya Nakadai.

== Cast ==
*Tatsuya Nakadai - Genta (Hyōdō Yagenta)
*Etsushi Takahashi - Hanji (Hanjirō Tabata)
*Yuriko Hoshi - Chino Kajii
*Naoko Kubo - Tetsutarō Oikawa
*Shigeru Kōyama - Ayuzawa
*Akira Kubo - Monnosuke Takei
*Seishirō Kuno - Daijirō Masataka
*Tadao Nakamaru - Shōda Magobei
*Eijirō Tōno - Moriuchi Hyōgo
*Shin Kishida - Jurota Arao
*Atsuo Nakamura - Tetsutaro
*Isao Hashimoto - Konosuke Fujii
*Yoshio Tsuchiya - Matsuo Shiroku
*Hideyo Amamoto - Shimada Gendaiu

== Synopsis ==
Tatsuya Nakadai stars as Genta, a former samurai who became disillusioned with the samurai lifestyle and left it behind to become a wandering yakuza (gang) member. He meets Hanjirō Tabata (Etsushi Takahashi) a farmer who wants to become a samurai to escape his powerless existence. Genta and Tabata wind up on opposite sides of clan intrigue when seven members of a local clan assassinate their chancellor. Although the seven, led by Tetsutarō Oikawa (Naoko Kubo) rebelled with the support of their superior, Ayuzawa (Shigeru Kōyama), he turns on them and sends members of the clan to kill them as outlaws.

The film is a comically exaggerated exploration of what it is to be a samurai.  The characters in the movie either give up samurai status or fight to attain it, and samurai are seen behaving both honorably and very badly. The film has a parodic tone, with numerous references to earlier samurai films. 

==Trivia==
*Kill! is based on Shūgorō Yamamotos novel Peaceful Days, the same source as for Akira Kurosawas Sanjuro.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 