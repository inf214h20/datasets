Princess Cinderella
{{Infobox film
| name = Princess Cinderella
| image =Princess Cinderella.jpg
| image_size = 255px
| director = Sergio Tofano
| writer =Edoardo Anton   Vittorio Metz Sergio Tofano 
| starring = Renzo Rossellini
| cinematography = Manfredo Bertini
| editing =
| producer =
| released =  
| country = Italy Italian
}}

Princess Cinderella ( ) is a 1941 Italian fantasy-comedy film written and directed by Sergio Tofano.It is based on the characters of the popular comic strip series Signor Bonaventura, created in 1917 by the same Tofano for the children magazine Corriere dei Piccoli.      

== Plot == Bonaventura promises him to go in search of the girl and bring her back. After fighting bitter enemies such as Barbariccia and the Ogre,  helped by the loyal Cecè and by the Little Fairy he will finally succeed.

== Cast ==

*Paolo Stoppa as Bonaventura
*Silvana Jachino as   Cinderella 
*Roberto Villa as Prince Charming
*Sergio Tofano as the Doctor
*Guglielmo Barnabò as the King
*Rosetta Tofano as  Pasqualina
*Mercedes Brignone as the Queen
*Piero Carnabuci as Barbariccia
*Mario Pisu as  Cecè 
*Camillo Pilotto as the Ogre
*Renato Chiantoni as Gambamoscia
*Amelia Chellini as the Witch

==References==
 

==External links==
* 
 

 
 
  
 
 
 
 
 
 
 


 
 