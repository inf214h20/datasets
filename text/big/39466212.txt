Outcast (2014 film)
{{Infobox film
| name = Outcast
| image = Outcast poster.jpg
| director = Nick Powell
| producer = Jeremy Bolt   Alan Zhang   Karine Martin   Léonard Glowinski
| writer = James Dormer
| starring = Hayden Christensen Nicolas Cage Liu Yifei Ji Ke Jun Yi Andy On
| music = 
| studio = Actra Montreal 
| distributor = Telefilm Canada  Arclight Films
| released =  
| runtime = 99 minutes
| country = United States China Canada
| language = English   Mandarin
| budget = 
| gross =
}}

Outcast is an 2014 American-Chinese-Canadian   action film directed by Nick Powell and written by James Dormer.  It stars Nicolas Cage, Hayden Christensen, Liu Yifei, Ji Ke Jun Yi and Andy On. The film was slated for release on September 26, 2014 in China but was postponed to April 3, 2015.      

==Plot==
When the heir of the imperial throne becomes the target of assassination by his despised older brother, the young princes only hope is the protection of his sister, and the reluctant aid of a war-weary Crusader, Jacob (Hayden Christensen), who must overcome his own personal demons and rally the assistance of the legendary Crusader-turned-bandit, Gallain (Nicolas Cage), to return the prince to his rightful place on the throne. 

==Cast==
* Hayden Christensen as Jacob
* Nicolas Cage as Gallain
* Liu Yifei as Lian
* Ji Ke Jun Yi as Mei
* Andy On as Shing

==Production==
In 2013, the film was officially announced on the Arclight films website. Principal photography started in April in the Yunnan province of China. In July, the official poster was released on the Arclight films website and Hayden Christensen Fan site. James Dormer wrote the script for the movie. Nick Powell will make his directorial debut.

== Sequel ==
On April 15, 2014, producer Jeremy Bolt announced plans for the sequel to the film. 

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 