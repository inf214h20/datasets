Thamizh Padam
{{Infobox film
| name = Thamizh Padam
| image = Tamil-padam.jpg
| image_size =
| caption =
| director = C. S. Amudhan
| producer = Dhayanidhi Alagiri
| writer = C. S. Amudhan &  K.Chandru Shiva Disha Pandey Paravai Muniyamma Kannan
| cinematography = Nirav Shah
| editing = T. S. Suresh
| Dialogue writer = K. Chandru
| studio = Cloud Nine Movies
| distributor = Y NOT Studios
| released =  
| runtime = 120 minutes
| country = India
| language = Tamil
| budget =   
| gross =      
| preceded_by =
| followed_by =
}} Shiva and Disha Pandey in the lead roles. The films plot parodies contemporary commercial films in Tamil cinema, mocking the stereotypical scenes.   The film was distributed by Dhayanidhi Alagiris Cloud Nine Movies, and it was released on 29 January 2010 and won critical acclaim and commercial success at the box office. The film was later remade to Telugu as Sudigadu.

==Plot==
The story opens in a village where male infanticide is predominant, and all male babies are required to be killed immediately after birth. One such baby is headed for such a fate, until he "speaks" to his caretaker-grandmother (Paravai Muniyamma) and asks to be sent to Chennai on a goods train, where he plans to grow into a hero. The old woman complies, and takes the baby to Chennai and raises him herself, living in the citys poorer section.
 costumed associate. gang of friends, composed of Nakul (M. S. Bhaskar), Bharath (Venniradai Moorthy) and Siddarth (Manobala).

Shiva runs into a headstrong girl named Priya (Disha Pandey) whom he falls in love with. After learning of her hatred for men, he realizes that she has dedicated her life to classical dance, and learns Bharathanatyam over the course of a night; and performs an exaggerated dance sequence for her. She reciprocates his feelings, and the two start a relationship. Shiva is then challenged by Priyas rich and powerful father Kodeeswaran, who refuses to give his daughter to a poor man. Shiva swears to become a billionaire, and promptly does- during the course of a 2.50 minute song.

Kodeeswaran accepts Shiva as a suitor, and fixes his engagement to Priya. During the ceremony, Shiva hears a passing comment that he does not know his own father. Offended, he travels to his birthplace Cinemapatti village, accompanied by Bharath, to learn his roots. After encountering a host of Tamil-cinema stereotypes of several decades ago, he succeeds in uniting with his family when a woman (revealed to be Shivas fathers concubine) leads him to his father, mother and sister. He finds them when they sing their "family song" (the Michael Learns to Rock number, "Someday (Michael Learns to Rock song)|Someday, Someway"). 
 Apoorva Sagodharargal); and kills his final victim with the bad odour of his sock. The police discover that he is the killer, and it is revealed that Shiva is actually an undercover officer and was killing the lawbreakers under direct orders from not only the Commissioner, but also from the President of the United States.

The ganglord who commandeered the slain criminals, a mysterious person simply called "D", organizes the kidnapping of Priya, and has Shiva beaten up. Shiva recovers, saves his beloved by fighting off thugs using exaggerated stunts, and comes face to face with "D", revealed to be his grandmother. She explains that she did it to increase the fame of her grandson, and a heartbroken Shiva is forced to arrest her.

At the trial, Shiva accidentally kills an assassin who targets his grandmother, and is put on trial himself. He is saved, however, when a man who had helped him in Cinemapatti earlier testifies that Shiva was a victim of circumstance. Both Shiva and "D" are pardoned, and Shiva who has been promoted to Director General of Police|DGP, unites happily with Priya, his family and friends.

==Cast== Shiva as Shiva
* Disha Pandey as Priya
* M. S. Bhaskar as Nakul
* Venniradai Moorthy as Bharath
* Manobala as Siddharth
* Delhi Ganesh as "Heroin" Kumar Ponnambalam as Nattamai
* Paravai Muniyamma as Grandmother (D)
* Mahanathi Shankar as Drugs Ravi Kasthuri in an item number
* Periyar Dasan as Mokkai (Shivas father)
* Seenu as Devaraj
* Azhagu as Kodeeswaran (Priyas father)
* Halwa Vasu as Kodeeswarans assistant
* V. S. Raghavan as Judge
* Meesai Rajendran as Inspector Sundaram
* Sathish as Pandiya
* Ammu as Sheela
* Nellai Siva as Minister
* Ayyappan as raped victim
* Shanmuga Sundaram as Shanmugam
* Benjamin as Railway Guard

==Release==
The satellite rights of the film were sold to Kalaignar TV|Kalaignar. The film was given a "U" certificate by the Indian Censor Board.

==Reception==
The film had a wide expectation based on the viral marketing campaign that the producers undertook with popular film stills being parodied as a part of the promotion strategy. This film released along with Goa (film)|Goa and it was a box office success.  The film garnered very best ratings on the user ratings. 

==Soundtrack==
{{Infobox album|  
| Name           = Thamizh Padam
| Type           = Soundtrack
| Artist         = Kannan
| Cover          = 
| Image_size     = 
| Released       = 
| Recorded       =  Feature film soundtrack
| Length         = 
| Label          = Think Music
| Producer       = Kannan
| Certification  = 
| Last album     = 
| This album     = Thamizh Padam   (2010)
| Next album     = Chaplin Saamandhi   (2011)
}}

The films soundtrack was composed by debutant Kannan and it was received well by the critics. The soundtrack contains five songs.

{{Track listing
| headline = Tracklist
| extra_column = Singer(s) / Notes
| total_length = 
| lyrics_credits = yes
| nextra_column = Notes
| title1 = Kuthu Vilakku Kasthuri
| lyrics1 = Thiyaru
| length1 = 
| title2 = O Maha Zeeya
| extra2 = Hariharan (singer)|Hariharan, Swetha Mohan / A romantic duet featuring Shiva and Disha Pandey with gibberish words
| lyrics2 = 
| length2 = 
| title3 = Oru Sooravali
| extra3 = Shankar Mahadevan / Depicts Shivas transformation into a billionaire.
| lyrics3 = Chandru
| length3 = 
| title4 = Pacha Manja
| extra4= Mukesh and chorus
| lyrics4 = Chandru
| length4 = 
| title5 = Theme Music
}}

==List of notable spoofs==
===Films===
* Karuththamma - The opening scene shows Periyar Dasan as the father of a baby who is about to be killed. Dasan played the same role in Karuththamma as well.
* Boys (2003 film)|Boys -  Sivas friends are named after the actors in the film. Their introduction scene, as well as Siddarths failed attempt to impress his girlfriend, are based on this film.
* Thalapathi - The scene where Devaraj visits his brother and asks for forgiveness.
* Priya (film)|Priya - The Scene where Devarajs Gang Asks siva To meet devaraj, sivas eyes is closed and he tried to located the place while sensing sounds while hes in car Polaroid photo of her.
* Minsara Kanavu - A small part of the dance in the song Oh Maha Zeeya is the same as what Prabhu Deva does in Minsara Kanavu in the song Vennilavae Vennilavae.
* Enga Ooru Paattukaran - A similar character is shown as a resident of Cinemapatti.
* Kizhakke Pogum Rail - At Cinemapatti railway station, a woman is shown sending messages at the back of the train compartment.
* Pokkiri - The hero is revealed to be an undercover police officer.
* Baasha (film)|Baasha - Sivas meeting with Devaraj is heavily based on the encounter between Rajinikanth and Raghuvaran in Baasha. Sivas fight in market - circle of rowdies around him and walking is based on Rajinis walk in Baasha when he hits Anand Raj. Villain D shoots the henchman when the henchman says "India lost the cricket match". In Baasha, Raghuvaran shoots his henchman when he conveys a bad news.
* Run (2002 film)|Run - Siva is chased by thugs and when he is not visible to them he sticks a small fake birthmark on his face. So when the thugs find him they are unable to recognize him though hes in the same dress. Even his girlfriend finds it difficult to recognize him.
* Ramana (film)|Ramana - Devraj is shown to accidentally meet Ramana before his brother (and asks forgiveness to ramana ) The protagonist of the film is famous for his dialogue " Tamilla enakku pidikyatha varuthai...Mannippu " ( meaning Sorry is the only word I hate in Tamil )
* Annamalai (film)|Annamalai - Siva challenges his lovers father and claims that he will become a millionaire (and subsequently does).
* Sivaji (film)|Sivaji - Siva meets a minister to report about the lack of water. His unique way of signing documents is also based on Sivaji.
* Billa (2007 film)|Billa - A transvestite has a Billa tattoo that says "Swarna". Apoorva Sagodharargal - Siva appears as a dwarf and attempts to kill a criminal using a Rube Goldberg machine. Delhi Ganesh reprises his role as the villain.
* Anniyan - Siva tries to force buffaloes to stampede and kill a criminal. In the same sequence, he tries to imitate Anniyans voice with no success.
* Nayagan - The people look to Siva as their savior and ask for his help in repairing the water pipe.
* Citizen (film)|Citizen - In the climax court scene, the judge mention the name of Athippatti Citizen, who is a character in this film.
* Captain Prabhakaran - In the climax court scene, the judge mention the name of Captain Prabhakaran, who is the  titular character in this film.
* Dhool - The character of Swarna is directly lifted from this film.
* Mouna Ragam - Siva proposes to Priya using the college intercom.
* Nattamai - The character of the same name appears in the film. Ponnambalam who appeared as villain in that film played as Naattamai.
* Chinna Thambi - A scene depicting three brothers protecting their sister by making her walk on their palms.
* Kaakha Kaakha - The hero and heroine travel in an open jeep to a retreat in Pondicherry (city)|Pondicherry, where the hero is beaten up and the heroine gets kidnapped. Pandiya, villain character of that film is also imitated. An instrumental version of the song Ennai Konjam is part of the background score. (Siva even mentions that a certain Mr. Anbuselvam is travelling ahead of them on the same route)
* Muthirai - Sivas cheri home turns out to be very posh within.
* Dasavathaaram - The appearance of the Tamil Nadu Chief Minister, Indian Prime Minister Manmohan Singh and the President of the United States.
* Thirupachi - The villains tie up the heroine in an abandoned warehouse, and the hero arrives in the nick of time to save her.
* Virumaandi - The title character appears in the film. Bharath remarks that Virumaandi means well, but is always misunderstood.
* Vettaiyadu Vilayadu - A cop who imitates Kamal Hassan asks for permission to go to America.
* Kanthaswamy (film)|Kanthaswamy - A man wearing Kanthaswamys costume is an associate of Sivas. The parodied version is named Kuppuswamy.
* Mozhi (film)|Mozhi - Siva first encounters Priya as she is beating up another woman. Bharaths "signals" of love, and the heroines friend named Sheela, are all based on Mozhi.
* Kadhalan - Siva goes to Priyas house and performs "classical dance" to win her love.
* Kadhalukku Mariyadhai - Siva and a girl simultaneously take the same book "Love and Love Only".
* Rasigan - The "Pacha Manja" song features a caption which says that the hero sang the song.
* Deva (film)|Deva - The "Pacha Manja" song features a caption which says that the hero sang the song.
* Vishnu (1995 film)|Vishnu - The "Pacha Manja" song features a caption which says that the hero sang the song.
* Maanbumigu Maanavan - The "Pacha Manja" song features a caption which says that the hero sang the song. Once More -  The "Pacha Manja" song features a caption which says that the hero sang the song.
* Palayathu Amman - When the hero enters Cinemapatti, they encounter film sets similar to all Rama Narayanan films, especially Palayathu Amman.
* En Rasavin Manasile - In Cinemapatti village, a person on the roadside is shown eating mutton.
* Sethupathi IPS - A policeman resembling Vijayakanth blames the murders on Pakistani terrorists. Chidambara Rahasiyam - The "mystery villain" D is portrayed similar to Delhi Ganeshs portrayal of the Black Cat in this film.
* Veerasamy - Priya, a fan of T. Rajendar, holds a DVD of this film and declares it an "epic". (same scene was parodied in Sonna Puriyathu)
* Chandramukhi  - Sivas introduction scene shows him stretching his leg (the scene from Chandramukhi is parodied when it is revealed that Siva has a split seam in his pants).
* Mappillai (1989 film)|Mappillai - When Siva punches and kicks the giant, the giant shows no sign of pain.
* Chennai 600028- A goon working for Kodeeswaran tells that he has seen Shiva in Chennai 600028
* 7G Rainbow Colony - Bharaths father scolding him, like Vijayan does in Rainbow Colony

===People===
* Kamal Haasan - When a conversation mentions that Virumaandi is a good man and all that he does gets misconstrued. Vijay - At the beginning of the film, there are references to his punch dialogues and also imitates the slogan saying that particular hero has sung the song. The Nattamai also states that boys from cinemapatti are starting political parties after doing 50 films. This is in direct reference to the now formed political party of Vijay which he announced on the eve of his 50th films release. Later, Siva tenses his muscles like Vijay did in Vettaikaaran (2009 film)|Vettaikaran. 2009 Academy Awards).
* Nayantara - During the song Kuthu Vilakku, a man dances in a bikini with a tattoo on his back, similar to Nayantaras tattoo in Billa (2007 film)|Billa.
* Fatima Babu/Shobana Ravi - The TV reporter says her name is Fathima Ravi - a combination of the two famous newsreaders from Doordarshan TV.
* Silambarasan - The Nattamai character declares that anyone who disobeys his law will be forced to watch Silambarasans films a hundred times.
* Sivaji Ganesan - Siva deflects the thugs who come searching for him by doing a getup change by sticking a small birthmark on his face.
* Trisha Krishnan - The heroine introduction scene in which the girl who abuses her husband mentioning that she want to go to pub with her friend Trisha to have drinks.
* Vikraman - Siva becoming a richman within one song is similar to the scenes of Vikraman directed films.

===Others=== Hutch - When Priya calls Sivas mobile phone, his ring tone is set to Hutchs signature tune and a dog follows him as depicted in the Hutch advertisement. He eventually gives brandy to the dog to stop it from following him.
*ZooZoos from Vodafone - After Siva beats up the rowdies to save Priya, the ZooZoos attempt to defeat Siva in the name of BooBoos.
*Santoor Soap - When Siva finds a girl being a young mother of a small girl, he utters "Mummy" similar to the advertisement.

==References==
 

==External links==
*  
* 
 

 
 
 
 
 
 
 
 