Swanthamevide Bandhamevide
 
{{Infobox film
| name           = Swanthamevide? Bandhamevide?
| image          = Swanthamevide Bandhamevide.jpg
| alt            = 
| caption        = Poster designed by Gayathri Ashokan Sasikumar
| producer       = 
| writer         = S. L Puram Sadanandan
| narrator       = 
| starring       =   Johnson
| cinematography = 
| editing        = G. Venkittaraman
| studio         = Royal Pictures
| distributor    = Royal & Cherupushpam Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Swanthamevide? Bandhamevide? is a Malayalam film released in 1984. Directed by J. Sasikumar|Sasikumar, this film is about the issues cropping up  inside the family after the marriage of two sons. Mohanlal, Jose Prakash, Swapna (actress)|Swapna, Lalu Alex, Menaka (actress)|Menaka, and Adoor Bhasi appeared in leading roles.   


==Plot==
Madhavan Nair (Jose Prakash), a lorry driver is loyal and honest to his master Keshava Panicker (Adoor Bhasi). In return, Panicker offers him a lorry, and a house in which to stay along with his family consisting of Lakshmi (Kaviyoor Ponnamma), his wife and two sons. With time, Madhavan Nair turns into a rich businessman who owns several buses, automobile workshops, and trucks. Balachandran (Lalu Alex), his elder son runs his business, while Rajendran (Mohanlal), younger son is of carefree nature, who is also in his  final years in college. Rajendran fells in love with Usha (Swapna (actress)|Swapna), Panickers daughter, while Balachandran is in love with Indu (Menaka (actress)|Menaka), daughter of Varma (M. S Thrippunithara), an old feudal family, who had lost all his wealth with time. With the consent of parents both Rajendran and Balachandran marry their heart throbes. But life turns into more troublesome after the marriage. Both Indu and Usha gets into petty quarrels leading to serious clashes inside the house. Things even went out of hands with both the brothers getting into physical fights in front of the parents. In such a fight between Rajendran and Balan, Madhavan Nair and Lakshmi interferes, but accidentally, the blow hits on Lakshmis head leading to her death, shocking everyone.

==Cast== 
  
*Mohanlal as Rajendran  Menaka as Indulekha 
*Lalu Alex as Balachandran  Swapna as Usha 
*Jagathy Sreekumar 
*Kaviyoor Ponnamma 
*Adoor Bhasi 
*Jose Prakash as Madhavan 
*Kundara Johny 
*M. S. Thripunithura  Meena 
*Kollam GK Pillai 
 

==Soundtrack== Johnson and lyrics were by Poovachal Khader.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Artist(s)
|-
| 1 || Amritham Kulirum || K. J. Yesudas
|-
| 2 || Odi Odi Odi || J. M. Raju, Vani Jairam
|-
| 3 || Oro Thazhvaravum || P. Jayachandran, Vani Jairam
|-
| 4 || Shapamo Ee Bhavanam || K. J. Yesudas
|}

==References==
 

==External links==
* 

 
 
 


 