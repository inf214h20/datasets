David and Bathsheba (film)
{{Infobox film
| name = David and Bathsheba
| image = David Bathsheba.jpg
| caption = Original film poster Henry King
| producer = Darryl F. Zanuck Philip Dunne
| starring = Gregory Peck Susan Hayward Raymond Massey Kieron Moore James Robertson Justice Alfred Newman Edward Powell
| cinematography = Leon Shamroy
| editing = Barbara McLean
| distributor = 20th Century-Fox
| released =  
| runtime = 123 minutes
| country = United States English
| budget = $2.17 million Sheldon Hall, Epics, Spectacles, and Blockbusters: A Hollywood History Wayne State University Press, 2010 p 137 
| gross = $7.1 million (est. US/ Canada rentals) 
}}
 historical Technicolor Henry King, Philip Dunne. Alfred Newman Samuel from Lithuanian wrestler named Walter Talun.

==Cast== King David
* Susan Hayward - Bathsheba
* Kieron Moore - Uriah the Hittite|Uriah
* Raymond Massey - Nathan (Prophet)|Nathan
* James Robertson Justice - Abishai (Bible)|Abishai
* Jayne Meadows - Michal John Sutton - Ira
* Dennis Hoey -Joab
* Walter Talun - Goliath (Bible)|Goliath
* Francis X. Bushman - Saul the King
* Leo Pessin - Young David
* Paul Newlan - Samuel
* Holmes Herbert - Jesse
* George Zucco - Egyptian Ambassador (uncredited)

==Production==
  and Susan Hayward]]

While Twentieth Century-Fox Film Corp. owned the rights to the 1943 book David written by Duff Cooper, the film is not based on that book. It was, though, the inspiration that led the studio to this film project. The production of the film started on November 24, 1950 and was completed in January 1951 (with some additional material shot in February 1951). The film premiered first in New York City August 14, and later in Los Angeles August 30, before opening wide in September 1951. {{cite web
| last =
| first =
| authorlink =
| coauthors =
| year =
| url = http://www.afi.com/members/catalog/DetailView.aspx?s=&Movie=50071
| title = David and Bathsheba
| format =
| work =
| pages =
| publisher =
| accessdate = 2006-04-27
}}
 

==Reception==
The film earned an estimated $7 million at the US box office in 1951, making it the most popular movie of the year. 

==Awards==
The film was nominated for five Academy Awards:    Best Art George Davis, Thomas Little, Paul S. Fox) Best Cinematography (Leon Shamroy) Best Costume Edward Stevenson) Best Music (Alfred Newman) Best Writing (Philip Dunne)

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 