The Bullfighter and the Lady
{{Infobox film
| name           = Bullfighter and the Lady
| image          = Bullfighterand01.jpg
| image_size     =
| caption        = Movie poster
| director       = Budd Boetticher
| producer       = Budd Boetticher John Wayne
| writer         = Budd Boetticher Ray Nazarro (story) James Edward Grant (screenplay)
| starring       = Robert Stack Joy Page Gilbert Roland
| music          = Victor Young
| cinematography = Jack Draper
| editing        = Richard L. Van Enger
| distributor    = Republic Pictures
| released       =  
| runtime        = 87/124 mins.
| country        = United States English
}}
 1951 drama film directed and written by Budd Boetticher. Filmed on location in Mexico, the film focused on the realities of the dangerous sport of bullfighting. During production, one stunt man died. Boetticher, who had experience in bullfighting, used a semidocumentary approach in filming the sport and the lives of matadors.

== Plot ==
Chuck Regan (Robert Stack), a young American film producer travels to Mexico, where he takes up bullfighting to impress a local beauty, Anita de la Vega (Joy Page). Manolo Estrada (Gilbert Roland), an aging matador, reluctantly agrees to teach the brash, self-centered Regan.

== Cast ==
*Robert Stack as John "Chuck" Regan
*Joy Page as Anita de la Vega
*Gilbert Roland as Manolo Estrada
*Virginia Grey as Lisbeth Flood John Hubbard as Barney Flood
*Katy Jurado as Chelo Estrada
*Ismael Pérez as Panchito
*Rodolfo Acosta as Juan
*Ruben Padilla as Dr. Sierra
*Darío Ramírez as Pepe Mora
*Paul Fix as Joseph Jamison (uncredited) 
*Ward Bond as Narrator (voice) (uncredited)

== Reception == Best Story, which he shared with co-writer Ray Nazarro. Together with Seven Men from Now, Boetticher regarded Bullfighter as one of "the two best films I ever made."  

The complete 124 minute version of the  The Bullfighter and the Lady  was released on DVD and Blu-ray Disc on July 30, 2013.

== Alternate Versions == American theatrical double bill. UCLA Film Archive recently restored the film to its full 124 minute length.  

== External links ==
 
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 