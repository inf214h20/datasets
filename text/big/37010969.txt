Out the Gate (film)
{{Infobox film
| name           = Out the Gate
| image          = "Out_the_Gate,_Official_Release_Poster,_otg_poster.jpg".jpg
| caption        = Release Poster
| director       = R. Steven Johnson and Qmillion Everton Dennis Susanne Lovejoy  Philippo Francinni R. Steven Johnson Cynthia Enriquez Everton Dennis Paul Campbell Everton Dennis
| music          = Qmillion
| cinematography = Livingston Siwell
| editing        = Norem Furia
| studio         = Far I Films
| distributor    = Far I Films (Theatrical)
| released       =  
| runtime        = 104 minutes
| language       = English Jamaican Patois
| country        = Jamaica United States
| budget         = 
}} Everton Dennis Paul Campbell, Everton Dennis, Everton Dennis and directed by R. Steven Johnson and Qmillion . The picture had its official limited release in the United States and Jamaica Distributed by Far I Films in 2011 followed by the DVD in 2012. The film was released to theaters in Los Angeles, New York, Atlanta, Toronto, as well as Jamaica.   Jamaica Gleaner  Newspaper Atlanta Daily World wrote the movie "appears to be on its way to becoming a classic".

==Plot== Everton Dennis) Paul Campbell) demands more than he can deliver.

==Cast== Paul Campbell as "Badz"
* Oliver Samuels as "Uncle Willie"
* Shelli Boone as "Tamika" Everton Dennis as "E-Dee"

== Soundtrack ==
"Ghetto Yutes Rise" feat. I-Octane by E-Dee 
"Bruk Whyne" by E-Dee 
"Hot Like Fiyah" by Junior P. 
"Everyday" by Mr. Lexx 
"Bongce Along" feat E-Dee by Ms. Triniti 
"Wi Party" feat Akapello by E-Dee 
"Bottle Service" feat. Jadakiss by Duane Darock 
"Time Fi Move Up" feat. Vannichi and E-Dee by Qmillion 
"Cant Stop Wi" feat Busy Signal by Karl Morrison 
"No Other Girl" by E-Dee

==Release dates==
*US Theatrical Release (Limited) May 13, 2011 
*US Soundtrack Release - June 5, 2012 
*US Video/DVD Release - Dec 11, 2012

== See also ==
*The Harder They Come
*Dancehall Queen
*Shottas
*Third World Cop

==References==
{{Reflist|refs=
   
   }}

== External links ==
*  
*  
*  
*  
* http://www.fandango.com/outthegate_141096/movieoverview

 
 
 
 
 
 
 
 
 
 
 