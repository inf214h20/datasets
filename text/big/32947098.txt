Bis fünf nach zwölf – Adolf Hitler und das 3. Reich
 

{{Infobox film
| name           = Bis fünf nach zwölf - Adolf Hitler und das 3. Reich
| image          =
| image_size     =
| caption        =
| director       = Gerhard Grindel
| producer       = Wolf C. Hartwig (producer)
| writer         = Gerhard Grindel (writer)
| narrator       =
| starring       = See below
| music          = Rudolf Perak
| cinematography =
| editing        = Bert Rudolf
| studio         =
| distributor    =
| released       = 1953
| runtime        =
| country        = West Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Bis fünf nach zwölf – Adolf Hitler und das 3. Reich is a 1953 West German documentary film directed by Gerhard Grindel.

The film is also known as Adolf Hitler - Ein Volk, ein Reich, ein Führer: Dokumente der Zeitgeschichte in West Germany, Adolf Hitler und das 3. Reich - Sein Untergang (German DVD box title) and Bis 5 nach 12 (German DVD title).

== Plot summary ==
 

== Cast ==
*Martin Bormann as Himself (archive footage)
*Eva Braun as Herself (archive footage)
*Winston Churchill as Himself (archive footage)
*Karl Dönitz as Himself (archive footage)
*Hans Frank as Himself (archive footage)
*Roland Freisler as Himself (archive footage)
*Wilhelm Frick as Himself (archive footage)
*Hans Fritzsche as Himself (archive footage)
*Walther Funk as Himself (archive footage)
*Josef Goebbels as Himself (archive footage)
*Carl Friedrich Goerdeler as Himself (archive footage)
*Hermann Göring as Himself (archive footage)
*Rudolf Hess as Himself (archive footage)
*Reinhard Heydrich as Himself (archive footage)
*Heinrich Himmler as Himself (archive footage)
*Adolf Hitler as Himself (archive footage)
*Miklós Horthy as Himself (archive footage)
*Friedrich Hoßbach as Himself (archive footage)
*Carola Höhn as Herself
*Alfred Jodl as Himself (archive footage)
*Kaiser Wilhelm II as Himself (archive footage)
*Ernst Kaltenbrunner as Himself (archive footage)
*Wilhelm Keitel as Himself (archive footage)
*Fritz Lafontaine as Himself
*Theodor Morell as Himself (archive footage)
*Benito Mussolini as Himself (archive footage)
*Werner Mölders as Himself (archive footage)
*Friedrich Paulus as Himself (archive footage)
*Erich Raeder as Himself (archive footage)
*Walther Rathenau as Himself (archive footage)
*Otto Ernst Remer as Himself (archive footage)
*Alfred Rosenberg as Himself (archive footage)
*Ernst Röhm as Himself (archive footage)
*Fritz Sauckel as Himself (archive footage)
*Hjalmar Schacht as Himself (archive footage)
*Ferdinand Schörner as Himself (archive footage)
*Arthur Seyss-Inquart as Himself (archive footage)
*Albert Speer as Himself (archive footage)
*Joseph Stalin as Himself (archive footage)
*Julius Streicher as Himself (archive footage)
*Gustav Stresemann as Himself (archive footage)
*Harry S. Truman as Himself (archive footage)
*Tsar Nicholas II as Himself (archive footage)
*Ulrich von Hassel as Himself (archive footage)
*Paul von Hindenburg as Himself (archive footage) Ewald von Kleist as Himself (archive footage)
*Konstantin von Neurath as Himself (archive footage)
*Franz von Papen as Himself (archive footage)
*Joachim von Ribbentrop as Himself (archive footage)
*Gerd von Rundstedt as Himself (archive footage)
*Baldur von Schirach as Himself (archive footage)
*Erwin von Witzleben as Himself (archive footage)
*Georgi Zhukov as Himself

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 