My Girl and I
{{Infobox film
| name           = My Girl and I
| image          = My Girl and I film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | hanja          = 파랑 
 | rr             = Parang Juuibo
 | mr             = Parang Chuŭibo}}
| director       = Jeon Yun-su
| producer       = Jeon Hoon-tak
| writer         = Kwak Jae-young
| starring       = Cha Tae-hyun Song Hye-kyo
| music          =
| cinematography =
| editing        = Park Gok-ji
| distributor    = iLove Cinema
| released       =  
| runtime        = 95 minutes
| country        = South Korea
| language       = Korean
| budget         =
| gross          = $2,293,477  
}} Japanese film Crying Out Love, In the Center of the World, adapted from the novel Socrates in Love by Kyoichi Katayama.

==Plot==
 
The film starts in modern day Korea where we see Kim Su-ho (Cha Tae-hyun) return to his hometown after ten years to attend a reunion with his high school friends. He sadly strolls as he hears a female voice in his head calling out his name.

The next scene shows us Su-hos three friends celebrating and talking about Su-ho. They doubt that hell attend their reunion because he apparently hasnt gotten over some girl. It is said that on that day of their reunion, it is the death anniversary of a girl named Bae Su-eun (Song Hye-kyo). To everyones surprise, Su-ho enters and warmly greets his friends.

After celebrating, the four friends go to a lighthouse where Su-hos friend shouts, "Bae Su-eun, my friends brain is small but is filled with endless thoughts of you. Please let my friend go." Su-ho starts to cry.

The next scene is a flashback wherein a younger Su-ho is rescued from drowning by a pretty girl. Her pager gets lost in the process of rescuing him but she just walks away. Su-ho then wakes up and thinks that his friends saved him from drowning.

The next scene leads us to a music class. Su-ho and his classmates sing in chorus. He sang normally just like the other students but was distracted when his classmate, Bae Su-eun started staring at him. Su-ho is confused. Su-hos friend says Su-eun always stares at him in class. Su-ho starts to think that Su-eun may actually be interested in him but was not.

Su-ho and Su-eun pass by in the hallway. Su-ho tries to walk away when Su-eun called him. She asks him to buy her a croquette which surprises Su-ho. He says that if he and Su-eun ate together, other people might think that they are dating and cause trouble. Still they eat together, and the other students notice. 

Soon the captain of the judo club who had his eyes set on Su-eun finds Su-ho. He threatens to hurt Su-ho who immediately denies dating her. Su-eun passes by and when the captain asks, she quickly replies that they are indeed dating. The captain throws Su-ho down. Su-hos friends arrived to rescue him and they escape by bike.

Su-ho and Su-eun stay by the boardwalk near a lighthouse. Su-eun hints to Su-ho that she saved him but he fails to understand. She asks him to replace her pager. They watch the sunset together.

Su-ho then walks Su-eun home and she writes her beeper number on a pomegranate for she was afraid her strict father would find out. Although her beeper was lost, she could still receive and leave messages. That night, they confess their feelings for each other.

The next day, Su-ho is called out of class because his grandfather supposedly collapses. He quickly goes to his grandfathers but finds him well. It turns our he faked illness so that he could talk to Su-ho. Su-eun arrives bringing Su-hos bag. Su-hos grandfather shares his story.

When he was young, Su-hos grandfather joined the army. He was separated from his first love, Soon-im. He left her a necklace to remember him by.

Years pass and he becomes an undertaker. He prepares a funeral for Soon-ims husband. They could not even say a word to each other and soon he married another woman, Su-hos grandmother.

Su-ho then realizes that he loves Su-eun and he wishes for her to be his destiny so that they would not suffer the same fate as his grandfather. Using a payphone, Su-ho left a message on Su-euns pager. In it he said that he wished she could be his destiny, completely unaware that she was behind him until she replied, "Me too."

From that day on, Su-ho and Su-eun officially became a couple.

Su-hos friends propose an overnight stay in Fog Island. As Su-ho and Su-eun arrived together at the pier, he finds out that his friends tricked him so that he and Su-eun would have some time alone. Su-eun seemed mad but then she herself bought two tickets to Fog Island.

On the island their love deepens even more. They share their first kiss.

The next day, Su-eun faints. Su-ho rushes her to the hospital, visiting her every day. What he thought was just simple anemia turns out to be terminal leukemia. Her last wish is to return to their island.

Another woman also had a last wish before she died—Soon-im. She wished to be reunited with her first love, Su-hos grandfather. He even prepared her funeral and was reunited with his first love, the woman he had loved for 50 years.

One day, when Su-ho visited the hospital, Su-eun expresses her wish and he shows her two ferry tickets. On the day of their departure, a typhoon prevents the ferries from running. Su-ho desperately tries to convince the authorities to let them travel.

As he argues with the management, Su-eun knows it is her time. As he apologizes to her, she collapses onto him.

In the present, Su-ho returns to Fog Island and finds Su-euns bag. Her diary entry reveals that she had planted seeds on the hill during their visit. The flowers will be her gift to him.

Su-ho goes to the hill, finding it covered with purple flowers.

==Cast==
* Song Hye-kyo as Bae Su-eun
* Cha Tae-hyun as Kim Su-ho
* Kim Hae-sook as Su-hos mother
* Lee Soon-jae as Kim Man-geum, Su-hos grandfather
* Song Chang-eui as Park Jong-goo, Su-hos friend
* Park Hyo-jun as Oh Sung-jin, Su-hos friend
* Kim Young-joon as Kim Hye-sung, Su-hos friend
* Kim Shin-young as Su-hos sister
* Song Yool-kyu as Young-goo, judo captain
* Han Myeong-ku as Su-euns father
* Moon Jung-hee as Soon-im / Soon-ims daughter
* Yoon Hee-won as young Kim Man-geum

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 