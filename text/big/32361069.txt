Cielito Lindo (film)
{{Infobox film
| name           = Cielito Lindo "Beautiful Heaven"
| image          = Cielito-lindo-film.jpg
| caption        = Theatrical release poster
| producer       = Alejandro Alcondez
| director       = Alejandro Alcondez
| writer         = Alejandro Alcondez
| starring       = Alejandro Alcondez Nicole Paggi Ilia Volok Nestor Serrano   Adam Rodriguez 
| music          = Cesar Benito
| cinematography = Horacio Marquínez   David Harges
| editing        = Alejandro Alcondez   Alan Roberts
| studio         = Alejandro Alcondez Pictures
| distributor    = Alejandro Alcondez Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = 
| followed by    = 
}}
Cielito Lindo also known by its English title Beautiful Heaven is a 2010   American film produced, written and directed by Alejandro Alcondez. He wrote it especially for himself where he himself plays the starring role of Pablo Pastor in the film.  It was released by his production and distribution company Alejandro Alcondez Pictures.

==Plot==
This is the life of Pablo Pastor (Alejandro Alcondez). Corrupt and deceitful people have stolen a precious stone know by the indigenous tribe as the Cielito Lindo, which is believed to bring the much needed rain back to the desert village of the Tahuromara Tribe.  Pablo Pastor is forced to abandon his quest to reunite with his family, and thrown into the center of the chaos around  Cielito Lindo . Outwitting and outsmarting the corrupt assassins Pablo finds a way to return the precious stone back to its people and with it hope and life. Because Pablo suffered from post-traumatic amnesia it is only towards the end that he remembers the horrible accident where both his wife and son were killed in the tragic car accident.  

==Cast==
*Alejandro Alcondez as Pablo Pastor, who is involved in a car accident and suffers from post-traumatic amnesia, only to come to the realization that his son and wife perished in a car accident. Thrown in the chaos of the stolen Cielito Lindo  Pablo finds a way to return the precious stone back to its people and with it hope and life.
*Nicole Paggi as Nicole, Married Borowski to get to the Cielito Lindo precious stone.
*Ilia Volok as Borowski, Mastermind and Villain that steals a precious stone from the Indigenous Tribe.
*Nestor Serrano as Matador, is a bullfighter that kills and tortures human beings as if they were animals and enjoys the kill.
*Adam Rodriguez as Leonardo, bodyguard planning to double cross Borowski by stealing  the precious stone known as the Cielito Lindo.
*Bernardo Peña as Moreno, Corrupt Police agent with a separate agenda on recovering the precious stone for themselves.
*David Castro as Olmedo, Corrupt Police agent and ranking officer for Moreno.
*Mariela Santos as Lucretia, Nurse who cared for and helped Pablo Pastor escape from the mental institution.
*Alex Bovicelli as Perla, gay crossdress who helped Nicole hide from being captured by Gigante.
*Antonio Costa as Don Manuco, Local store owner and Lucretias brother who loaned the Papaya truck.
*Rodrigo Patino as Spider, Meeting Nicole to make the trade off of the Cielito Lindo .
*Luis Fernandez Reneo as Carlitos, Papaya truck drive that was beat up by his wife.
*Valdez Cavazos Salvador as Drunk on Train.
*Jair Paz as Gomez, Police Agent.
*Joel Roman as Gigante, Bodyguard muscleman  for Borowski.

==Production==
The film was produced in its entirety by Alejandro Alcondez owner of Alejandro Alcondez Pictures. Production took place in the city of Chihuahua, Chihuahua Mexico and the surroundings landscapes including the use of the local train station that had to be shut down delaying some departures. Local abandoned copper mines Las Minas de Aldama were also part of the filming locations. It took approximately 3 months to complete the filming of Cielito Lindo. Many locals were used as extras including the participation of native Tarahumara Indigenous people using their original garments in the scenes.    

==Release==
The film was scheduled for release in 2009 and was determined that Alejandro Alcondez Pictures would be the distributing company. The film premiered at the famous Grauman’s Chinese Theater on May 27, 2010   and was well received by the public as well as different news publications, it was released in theaters everywhere.  

==Awards==
Alejandro Alcondez received the “Jalisco Latino Pride Award” presented by the Jalisco Foundation for the State of Jalisco, Mexico, for his long career in the cinematography as director, producer, writer and actor, which included his most recent American film Cielito Lindo “Beautiful Heaven”.  

==See also==
*Tlaloc
*Tarahumara people

==References==
 

==External links==
* 
* 
* 

 
 