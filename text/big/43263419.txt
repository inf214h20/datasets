Money Ratnam
{{Infobox film
| name           = Money Rathnam
| image          = MoneyRatnam.jpg
| alt            = ManiRatnam film poster
| director       = Santhosh Nair
| producer       = Raju Mathew
| writer         = Anil Narayanan Ajith C.Lokesh
| starring       = Fahadh Faasil Niveda Thomas Renji Panicker
| music          = Prashant Pillai
| cinematography = Neil D`Cunha
| editing        = Manoj
| studio         = 
| distributor    = Century Films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}

Money Ratnam (English translation: Money Diamond) is a 2014 Indian Malayalam comedy thriller film, directed by debutant Santhosh Nair starring Fahadh Faasil  and Niveda Thomas.  Touted to be a road movie, it narrates a story that happen in a 24-hour time frame during a journey from Munnar to Marayur.  Scripted by Anil Narayanan and Ajith C. Lokesh, the music department was handled by Prashanth Pillai, while the camera was helmed by Neel D. Kunha.  Money Ratnam is the 100th film of the production house Century Films.  The films locations include Udumalpet, Pollachi, Ernakulam, and Sholayar.  It released on 26 September 2014 as an Onam release. 

==Plot==
Neil John Samuel is a showroom executive in Ernakulam. After the New Year party, fortunately Neel gets a bag of money. With the money, Neil reaches a village in Tamil Nadu. Four people with an intention to buy diamonds also arrive at the same spot. All are connected with the cash which is in the hands of Neil.

==Cast==
*Fahadh Faasil as Neil John Samuel
*Niveda Thomas as Pia Mammen
*Renji Panicker as Isaac Anakkadan
* Joju George as Makudi Das
* Ambika Mohan as a nun
* Sunil Sukhada
* Naveen
* Leema Babu as Divya
* Kochu Preman
* Chembil Ashokan as Joppa
* Dinesh Nair as Cleetus
* Balu Varghese as Joppas son
* Sasi Kalinga as Karim
* Reena Basheer as Isaac Anakkadans wife Aarthi

==Home Video==
Manorama Music  released the VCD, DVD and Blu-ray of the movie in 5 December 2014

==Critical reception==
The Hindu wrote, "Money Ratnam has its moments, offers a few laughs, does not give you one of those splitting headaches and is your money’s worth".  The Times of India gave 3 stars out of 5 and wrote, "The film is not logically perfect, but it sure brings out the slapstick comedian in Fahad, hitherto untapped...the director shows good promise and the film is worth a watch, solely for the humour".  Rediff gave 2.5 stars  out of 5 and called the film "an above-average, unpretentious entertainer".  The New Indian Express wrote, "Despite...minor hiccups, it is a comedy film, entertaining and engaging throughout". 

==References==
 

==External links==
*  

 
 
 