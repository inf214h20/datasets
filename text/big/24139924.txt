Take a Girl Like You (film)
 
 
{{Infobox film
| name           = Take a Girl Like You
| image          = 
| caption        = 
| director       = Jonathan Miller
| producer       = Hal E. Chester
| writer         = George Melly
| based on       =  
| starring       = Haley Mills Oliver Reed Noel Harrison
| music          = Stanley Myers
| cinematography = Dick Bush Jack Harris Rex Pyke 
| distributor    = Columbia Pictures
| released       =   (USA)
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British comedy television adaptation of the story was broadcast in 2000.

==Plot==
In one of a small set of adult roles she did during the 1970s and 1980s, Mills plays Jenny, a good girl who comes to a small British town to start a simple life. Soon she becomes the center of male attention all around the town. Leading the pack is the honest but gruff Patrick who shares one thing along with the other men: have sex with the right girl, and who he and the others consider the right girl is 
Jenny.

==Cast==
* Hayley Mills as Jenny Bunn
* Oliver Reed as Patrick Standish
* Noel Harrison as Julian Ormerod John Bird as Dick Thompson
* Sheila Hancock as Martha Thompson
* Aimi MacDonald as Wendy
* Geraldine Sherman as Anna Le Page
* Ronald Lacey as Graham McClintoch
* John Fortune as Sir Gerald
* Imogen Hassall as Samantha
* Pippa Steel as Ted
* Penelope Keith as Tory Lady
* Nicholas Courtney as Panel Chairman George Woodbridge as Publican Jimmy Gardner as Voter
* Nerys Hughes as Teacher
* Jean Marlow as Mother
* Howard Goorney as Labour Agent

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 
 