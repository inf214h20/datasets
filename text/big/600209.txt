Kalifornia
 
{{Infobox film
| name           = Kalifornia
| image          = Kaliforniaposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Dominic Sena
| producer       = Jonathan Demme Peter Saraf Edward Saxon
| screenplay     = Stephen Levy Tim Metcalfe
| story          = Tim Metcalfe
| starring       = Brad Pitt Juliette Lewis David Duchovny Michelle Forbes
| music          = Carter Burwell
| cinematography = Bojan Bazelli
| editing        = Martin Hunter Viacom Pictures
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 117 minutes 118 minutes  
| country        = United States
| language       = English
| budget         = $8.5 million 
| gross          = $2,395,231
}} road thriller thriller film directed by Dominic Sena and starring Brad Pitt, Juliette Lewis, David Duchovny, and Michelle Forbes. The film focuses on a graduate student (Duchovny) and his photographer girlfriend (Forbes) traveling cross-country to research serial killings, who unwittingly carpool with a serial killer (Pitt) and his childlike girlfriend (Lewis).

The film was released in September 1993 in the United States, and received generally positive reviews from critics. 

==Plot==
Brian Kessler (David Duchovny) is a graduate student in psychology and a journalist, who has written an article about serial killers, which draws interest from a publisher that offers him a book deal.  After the book deal advance is spent, Brian realizes that he needs to start working on finishing his book.  His photographer girlfriend Carrie Laughlin (Michelle Forbes) persuades him to move to California, and they decide to drive from Pittsburgh, Pennsylvania to California and visit infamous murder sites along the way. Short on funds, Brian posts a ride-share ad.

Meanwhile, psychopathic parolee Early Grayce (Brad Pitt) has just lost his job. His parole officer (Judson Vaughn) learns of this  and comes to the trailer park where Early lives with his young girlfriend Adele Corners (Juliette Lewis). Early refuses the officers offer of a job as a college janitor, saying he wants to leave the state.  The officer informs him that if he does not keep the appointment, he will be returned to prison. Early decides to go to the job interview. However, when he is on his way out, he is confronted by the landlord over non-payment of rent. Early grows violent and spins out in his car, chasing the man all over the park.

Early spies the ride-share ad at the college and calls Brian, who agrees to meet him the following day. Early sends Adele ahead then murders the landlord before joining Adele to wait for Brian and Carrie. Carries first response to seeing the rough-hewn couple is to suggest Brian turn the car around and leave, but Brian asks her to give the plan a chance, and she reluctantly agrees.

On the road, unbeknownst to his companions, Early murders a man in a gas station bathroom and steals his money. When they arrive at the first hotel, he cuts Adeles hair to match Carries. At another hotel, Early invites Brian out to play pool, leaving Adele and Carrie alone together. Adele reveals to Carrie that she is a rape victim and that she views Early as her protector, even though her mother did not approve because Early had just been released from prison. Carrie is alarmed by Brians growing fascination with Early and nonchalant response to the news that Early has been in prison. She gives him an ultimatum: either they rid themselves of Early and Adele, or she will leave.

At the next gas station, Carrie glimpses a newscast with footage of Early and the announcement he is a suspected murderer. Early kills the gas station attendant and continues the trip with the couple as hostages. They encounter two police officers, whom Early shoots. They next come to the home of an elderly couple. Early beats the man to death, but Adele allows the woman to flee. As Early rushes to find the woman, Adele confronts him and says she wants nothing more to do with him. Early fatally shoots Adele, strikes Brian on the head, and kidnaps Carrie.

Brian regains consciousness, and the elderly woman gives him the keys to her truck. Brian arrives at an abandoned nuclear testing site and surprises Early, hitting him in the head with a shovel. Brian finds Carrie bloodied and handcuffed to a bed, having been sexually assaulted. Early, who was only stunned, attacks Brian and they struggle until Early is hit over the head by Carrie. When Early continues the attack, Brian kills him.

The film ends with Brian and Carrie in a California beach house. Carrie, who now sports long hair (eschewing the short style that Early had Adele emulate during the trip), tells Brian that a gallery is interested in her art, and he suggests they go out to celebrate.

==Cast==
* Brad Pitt as Early Grayce
* Juliette Lewis as Adele Corners
* David Duchovny as Brian Kessler
* Michelle Forbes as Carrie Laughlin
* Brett Rice as Police Officer
* Loanne Bishop as Female Officer

==Development==
Originally titled California, the script was written by Tim Metcalfe with Stephen Levy in 1987. Metcalfe later commented their intentions were "to scare an audience, to comment on our national obsession with true crime stories, and to punish myself for my morbid preoccupation with the subject of murder and murderers."  . Accessed 03-10-12.  The script was optioned in November 1990 by Propaganda Films, by request of director Dominic Sena.  . Accessed 03-10-12.  Sena would go on to mention his positive impression on the script was mainly based on the premise and the character of Early Grayce.

Between November 1990 and March 1991, Metcalfe completed two rewrites of the script to implement changes requested by Sena and Propaganda Films.  The characters of Brian and Carrie were given professions as a writer and a photographer, respectively, while retaining the original premise to share a ride with a serial killer. Metcalfe disagreed on the direction the script was being developed,  while Sena and the producers found his rewrites "uninspiring".  In March 1991, Metcalfe was fired from the project.

Without the budget to hire another writer, Sena, along with his two producers, spent another year writing ten subsequent drafts of the script.  Their contributions included the voice-over narration of the character of Brian, along with a change of tone from a black comedy to a more violent thriller.  Sena maintained the rewrites helped them to secure the cast, as well as an increase to the originally proposed budget of $4.5 million.

==Soundtrack==
{{Infobox album 
| Name        = Kalifornia - Original Soundtrack
| Type        = Soundtrack
| Artist      = Various
| Cover       = 
| Released    = August 3, 1993
| Genre       = Various
| Length      = 55:42 Polydor
| Producer    = Various
| Misc        =
}}
{{Album ratings
| rev1        = Allmusic
| rev1score   =   
}}
{{tracklist
| extra_column = Artist(s)
| writing_credits = yes
| title1 = Do You Need Some?
| writer1 = Matt Mercado
| extra1 = Mind Bomb
| length1 = 6:25
| title2 = Unfulfilled Quicksand
| Quicksand
| length2 = 3:23
| title3 = Deep
| writer3 = Tony Mortimer
| extra3 = East 17
| length3 = 4:04
| title4 = When You Come Back
| writer4 = Kevn Kinney, Tim Nielson, Jeff Sullivan, Buren Fowler
| extra4 = Drivin N Cryin
| length4 = 3:00
| title5 = No One Said It Would Be Easy Kevin Gilbert, Dan Schwartz
| extra5 = Sheryl Crow
| length5 = 5:29
| title6 = I Love the World
| writer6 = Angelique Bianca
| extra6 = The Indians
| length6 = 5:36
| title7 = Lettuce and Vodka John Doe, Duke McVinnie X
| length7 = 5:05
| title8 = Accelerator Andrew Cairns, Fyfe Ewing, Michael McKeegan
| extra8 = Therapy?
| length8 = 2:08
| title9 = Born for Love
| writer9 = David Baerwald
| extra9 = David Baerwald
| length9 = 6:17
| title10 = Dive Bomber
| writer10 = Sean Dickson
| extra10 = Soup Dragons
| length10 = 2:44
| title11 = Look Up to the Sky
| writer11 = Angelique Bianca
| extra11 = The Indians
| length11 = 7:20
| title12 = Kalifornia/Cactus Girl
| writer12 = Carter Burwell
| extra12 = Carter Burwell
| length12 = 4:11
}}
 Hugh Harris, "Playin in the Dirt" by Heather Myles, "Strong Enough" by Sheryl Crow, "Come Home" by Pere Ubu, "(Get Your Kicks on) Route 66" by Asleep at the Wheel, "Symphony No. 8 in F Major Op. 93" by Ludwig van Beethoven and Brad Pitt partially singing part of Free Bird by Lynyrd Skynyrd. 

==Comic book adaptation==
Following the completion of the film,  . Fegredo has speculated this was due to the demise of Gramercy Pictures, a production company involved handling the rights. An uncolored version, complete with Fegredos original cover art, has since been uploaded to the Internet. 

==Reception==
Kalifornia received positive reviews; it currently holds a 67% rating on Rotten Tomatoes.    It was a box office bomb, only grossing $2,395,231  based on a supposed $9 million budget. The film would go out to win various awards at international film festivals, including one for best screenplay, credited to Tim Metcalfe.  . Accessed 03-10-12. 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 