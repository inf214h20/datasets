813 (film)
{{Infobox film
| name           = 813
| image          =
| image_size     =
| caption        = Theatrical release poster
| director       = Charles Christie Scott Sidney
| producer       = Al Christie
| screenplay     =
| story          =
| starring       = {{Plainlist |
*Wedgwood Nowell Ralph Lewis
*Wallace Beery
*J. P. Lockney
*William V. Mong Colling kenny
*Milton Ross
* 
}}
| cinematography =
| editing        =
| studio         = Christie Film Company
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = 
| budget         =
| gross          =
}}
 Scott Darling Ralph Lewis,  Wallace Beery and Laura La Plante. 

The survival status of 813 is classified as unknown,  suggesting that it is a lost film.

==Plot==
As summarized in a film publication,    Robert Castleback (Lewis) has plans for worldwide power through a mysterious secret that he possesses. Arsene Lupin (Nowell), master thief but loyal Frenchman, knows of the secret and is attempting to obtain state papers held by Castleback. Two other persons in the employ of the Kaiser are attempting the same thing. Castleback is murdered and some suspect Lupin, who announces his intention to catch the real killer. Disguised as the chief of police, he works fearlessly alongside the police. Soon he comes into contact with another master criminal, Ribeira (Beery), who is mascarading as Maj. Parbury, and Lupin suspects that he is complicit in the crime. Lupin falls in love with Dolores Castleback (Adams), widow of the murdered man. When Ribeira, to get rid of Lupin, steals his daughter and informs Lupin that he will have to go alone to a deserted house to get her back, Lupin goes, foils the plot to kill him, and escapes through an underground tunnel that comes out in the home of Delores. As he turns from the mantelpiece where he has discovered the hiding place of the state papers, he sees a mysterious man that he has been trailing. To Lupins horror he finds that the man is really Delores, who is in reality a German criminal. She kills herself and Lupin escapes.

==Cast==
 
Wedgwood Nowell	 ...	
Arsene Lupin  Ralph Lewis	 ...	
Robert Castleback 
Wallace Beery	 ...	
Maj. Parbury / Ribeira 
J.P. Lockney	 ...	
Formerie 
William V. Mong	 ...	
Chapman  Colin Kenny	 ...	
Gerard Beaupre 
Milton Ross	 ...	
Gourel 
Thornton Edwards	 ...	
Doudeville 
Frederick Vroom	 ...	
Prefect of Police 
Mark Fenton	 ...	
Marco 
Kathryn Adams	 ...	
Dolores Castleback 
Laura La Plante	 ...	
Genevieve 
Vera Steadman	 ...	
Vashti Seminoff 
 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 
 