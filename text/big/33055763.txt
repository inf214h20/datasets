The Black Panther's Cub
{{infobox film
| name           = The Black Panthers Cub
| image          = The Black Panthers Cub 1922 newspaperad.jpg
| imagesize      =
| caption        = Newspaper advertisement.
| director       = Emile Chautard
| producer       = William F. Ziegfeld
| writer         = Algernon Swinburne (poem: Faustine) Ethel Donoher (story) Philip Bartholomae (adaptation) Norman Trevor Henry Stephenson Tyrone Power, Sr.
| music          =
| cinematography = Jacques Monteran - ( ) Alfred Ortlieb
| studio         = Ziegfeld Cinema Corporation
| distributor    = Equity Pictures Corporation
| released       = May 15, 1921 July 16, 1926
| runtime        = unknown
| country        = United States Silent (English intertitles)
}}
  lost film.  

==Plot==
As summarized in a film publication,    when the law closes the Black Panthers (Reed) house, she gives her daughter into the keeping of her old friend Clive (Stephenson). Clive dies and the Cub (Reed), now a young lady, learns who her mother was. Lord Maudsley (Foxe), Clives son, is in financial difficulty. He makes the Cub believe that dead benefactor has left large debts, and persuades her to reopen her mothers establishment to obtain the money. She does, and the former admirers of the Black Panther marvel at the way she has retained her youth. Eventually the Cub meets her mother (Reed), now an old woman, in a dive to which the Cub has fled with an admirer to get away from the man she loved, but feared to face in her new existence. The place is raided and the mother is shot. Later Maudsley admits that it was he who needed the money, and the lover forgives the Cub and they are happy together.

==Cast==
*Florence Reed - The Black Panther / Mary Maudsley / Faustine Norman Trevor - Sir Marling Grayham
*Henry Stephenson - Clive, Earl of Maudsley
*Paul Doucet - Victim of Chance (*Paul Ducet)
*Don Merrifield - Sir Charles Beresford
*Henry Carvill - Lord Whitford
*Louis R. Grisel - Butler (*Louis Grisel)
*Earle Foxe - Lord Maudsley
*William Roselle - Hampton Grayham
*Paula Shay - Evelyn Grayham
*Halbert Brown - Mr. Laird
*Charles Jackson - Stable Boy
*Ernest Lambart - Money Lender
*Frank DeVernon - Philanthropist
*Tyrone Power, Sr. - Count Boris Orliff (*as Tyrone Power)

==Reception== film censors would just cut it? 

==References==
 

==External links==
 
*  
* 

 
 
 
 
 
 