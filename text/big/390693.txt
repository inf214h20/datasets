Born in Flames
{{Infobox film
| name = Born in Flames
| image = Born in flames poster.jpg
| image_size =
| caption = Lizzie Borden
| producer = Lizzie Borden
| writer = Lizzie Borden
| narrator =
| starring = {{plainlist|
* Honey
* Adele Bertei
* Kathryn Bigelow
}}
| music = Ibis
| cinematography = {{plainlist|
* Ed Bowes
* Al Santana Michael Oblowitz
* Lizzie Borden
}}
| video camera = DeeDee Halleck
| editing = Lizzie Borden
| distributor = First Run Features
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget =
| gross =
}} Lizzie Borden alternative United socialist democracy.   

==Plot==
 Flo Kennedy) Pat Murphy, Kathryn Bigelow) for a socialist newspaper run by screenwriter Ed Bowes, who go so far they get fired.

The story involves several different women coming from different perspectives and attempts to show several examples of how sexism plays out, and how it can be dealt with through direct action. A famous scene is one during which two men are attacking a woman on the street and dozens of women on bicycles with whistles come to chase the men away and comfort the woman. The women in the movie have different ideas about what can and should be done, but all know that it is up to them, because the government will not take care of it. The movie shows women organizing in meetings, doing radio shows, creating art, wheatpasting, putting a condom on a human penis|penis, wrapping raw chicken at a processing plant, etc. The film portrays a world rife with violence against women, high female unemployment, and government oppression. The women in the film start to get together to make a bigger impact, by means that some would call terrorism.

Ultimately, after both radio stations are suspiciously burned down, Honey and Isabel team up and broadcast "Phoenix Ragazza Radio" from stolen moving vans. They also join the Womens Army, which sends a group of terrorists to interrupt a broadcast of the President of the United States proposing that women be paid to do housework, followed by bombing the antenna on top of the World Trade Center to prevent additional such destructive messages from the mainstream.

==Cast== Honey as Honey
* Adele Bertei as Isabel
* Jean Satterfield as Adelaide Norris
* Florynce Kennedy (credited as "Flo Kennedy") as Zella Wylie 
* Becky Johnston as Newspaper Editor
* Pat Murphy as Newspaper Editor
* Kathryn Bigelow as Newspaper Editor
* Hillary Hurst as Leader of Womens Army Hillary Hurst
* Sheila McLaughlin as Other Leader
* Marty Pottenger as Other Leader/Woman at Site
* Bell Chevigny as the Talk Show Host
* Joel Kovel as the Talk Show Guest
* Ron Vawter as FBI Agent
* John Coplans as Chief
* John Rudolph as TV Newscaster
* Warner Schreiner as TV Newscaster
* Valerie Smaldone as TV Newscaster Hal Miller as Detective

This film marks the first screen appearance of Eric Bogosian.  He plays a technician at a TV station who is forced at gunpoint to run a videotape on the network feed.  The movie also features a rare acting appearance by Academy Award-winning film director Kathryn Bigelow. 

==Awards==
In 1983, the film won the Reader Jury prize at the Berlin International Film Festival and the Grand Prix at the Créteil International Womens Film Festival.

==Reception== Time Out London wrote that Borden "  her story with audacity and make  even the driest argument crackle with humour, while the more poignant moments burn with a fierce white heat."   TV Guide rated it 2/4 stars and wrote, "This feminist film wins laurels for close attention to detail in a radical filmmaking effort."   Nathan Rabin of The A.V. Club called it "interesting as a historical curio but nearly unwatchable as entertainment".   Greg Baise of the Metro Times called it "an early 80s landmark of indie and queer cinema". 

==Influence==
The film is discussed in Christina Lanes book Feminist Hollywood: From "Born in Flames" to "Point Break". 

The film features the Red Krayola song "Born In Flames", released as a single in 1981. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 