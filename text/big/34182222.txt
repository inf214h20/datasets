Springtime Serenade
{{Infobox Hollywood cartoon|
| cartoon_name = Springtime Serenade
| series = Oswald the Lucky Rabbit
| image = SpringtimeSerenade.jpg
| caption = Oswald and his sister open their inn for Spring.
| director = Walter Lantz Bill Nolan
| animator = Manuel Moreno Lester Kline Fred Kopietz Bill Mason La Verne Harding
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = May 27, 1935
| color_process = Technicolor
| runtime = 8:08 English
| preceded_by = Elmer the Great Dane
| followed_by = Towne Hall Follies
}}

Springtime Serenade is a short cartoon made by Walter Lantz Productions, and features Oswald the Lucky Rabbit. While most shorts in the series are shot in black and white, this one is among the very few that are in color.

==Plot==
After a long winter in the countryside, the snow finally melted and the animals came out of hybernation. A squirrel merrily runs around and heads to the groundhogs house to tell the latter of the good news.
 see his shadow on the wall of his house. He then warns the squirrel and the other animals nearby that if his shadow is visible, more days of snow await. The other animals, however, were doubtful of his advice and therefore took it as a joke.

On another part of the countryside, Oswald and an unnamed sister of his are working on their inn after it had been closed for the winter. They dust the furniture and shake the dirt off the carpets. After completing their tasks, the inn was ready for service. Oswald and his sister went outside for a little stroll.

As the two inn operators are at the open and spending time with some animals, the groundhog approached and warn them once more about more winter days coming. Again, they all thought it was just a bluff and began to laugh. But they would stop laughing when snowflakes fall to the ground. Finally realizing the truth of the groundhogs warning, everybody hurried back indoors.

==Availability==
The short is available on   DVD box set.

==See also==
*Oswald the Lucky Rabbit filmography

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 


 