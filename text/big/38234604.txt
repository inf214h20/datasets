The Thoroughbred (1928 film)
{{Infobox film
| name           = The Thoroughbred 
| image          =
| caption        =
| director       = Sidney Morgan 
| producer       = Sidney Morgan
| writer         = Sidney Morgan Ian Hunter Richard Barclay   Harry Agar Lyons
| music          = 
| cinematography = 
| editing        = 
| studio         = London Screenplays
| distributor    = Gaumont British Distributors 
| released       = December 1928
| runtime        = 5,608 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Ian Hunter, Richard Barclay. It was made at Twickenham Studios. The wife of a Member of Parliament pressures a jockey to throw the Epsom Derby. 

==Cast== Ian Hunter   
* Louise Prussing    Richard Barclay   
* Harry Agar Lyons 

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 