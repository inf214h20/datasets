Kurishuyudham
{{Infobox film
| name           = Kurishuyudham
| image          =
| caption        = Baby
| producer       = C Radhamani
| writer         = Pushparajan Pappanamkodu Lakshmanan (dialogues)
| screenplay     = Pappanamkodu Lakshmanan Madhu Srividya Mohanlal
| music          = KJ Joy
| cinematography = KB Dayalan
| editing        = G Murali
| studio         = Pushpa Movie Productions
| distributor    = Pushpa Movie Productions
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Baby and produced by C Radhamani. The film stars Prem Nazir, Madhu (actor)|Madhu, Srividya and Mohanlal in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Prem Nazir as James Madhu as Mathew Cheriyachan
*Srividya as Rosamma
*Mohanlal as Johny Madhavi as Susie, Daisy (double role)
*TG Ravi as Issac John
*CI Paul as Paili
*Jose Prakash as Father Fernandez
*Kollam GK Pilla as Paappi
*Prathapachandran as Doctor
*Jagannatha Varma as D.I.G
*Captain Raju as Magician Dsouza/Lorence Anuradha as Dancer Santhakumari as Annamma Santhosh as Gunda

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhoomiyil poomazhayaayi || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Koodaaram vediyumee || K. J. Yesudas || Poovachal Khader || 
|-
| 3 || Yudham kurisuyudham || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 