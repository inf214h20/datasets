A Faithful Soldier of Pancho Villa
 
{{Infobox film
| name           = A Faithful Soldier of Pancho Villa
| image          = A Faithful Soldier of Pancho Villa.jpg
| caption        = Film poster
| director       = Emilio Fernández Antonio del Castillo Emilio Fernández
| writer         = Emilio Fernández
| starring       = Emilio Fernández
| music          = 
| cinematography = José Ortiz Ramos
| editing        = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

A Faithful Soldier of Pancho Villa ( ) is a 1967 Mexican drama film written, directed by and starring Emilio Fernández. It was entered into the 5th Moscow International Film Festival.   

==Cast==
* Emilio Fernández as Aurelio Pérez
* Maricruz Olivier as Amalia Espinosa
* Carlos López Moctezuma as Gonzalo de los Monteros
* Sonia Amelio as María Dolores
* José Eduardo Pérez as Comandante Pérez
* José Trinidad Villa
* Jorge Pérez Hernández
* Aurora Cortés
* Celia Viveros
* Margarita Cortés
* Leonor Gómez
* Margarito Luna
* Salvador Godínez

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 