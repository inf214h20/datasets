Jatta (film)
{{Infobox film
| name           = Jatta
| image          = Jatta poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = B. M. Giriraj
| producer       = N. S. Rajkumar
| writer         = B. M. Giriraj Kishore Sukratha Wagle
| music          = Ashley Mendonca and Abhilash Lakra
| cinematography = Kiran Hampapur
| editing        = K. M. Prakash
| studio         = 
| distributor    = Omkar Movies
| released       =  
| runtime        = 134 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}} thriller drama Kishore and Sukratha Wagle in lead roles. Kishore plays the role of a forest guard in the film whose wife elopes with a tourist and he involves in a clash of beliefs and ideologies with a feminist who he chains in his isolated forest home. The film won critical acclaim for its story and the portrayal of characters from critics and filmgoers.     

==Cast== Kishore as Jatta
* Sukratha Wagle as Sagarika
* Pavana as Belli
* Prem Kumar as Bheemakumar
* B. Suresha
* Vinod Kumbar as Rajesh

==Production==
In 2010, B. M. Giriraj made an experimental film film Naviladavaru with a budget of  35,000. Being recognized for this, he was offered to direct Advaita by film producer N. M. Suresh in the same year, a film that did not release. He was then offered Jatta by producer N. S. Rajkumar. 

==Soundtrack==
The music of the film was composed by the duo Ashley Mendonca and Abhilash Lakra with lyrics penned by B. M. Giriraj.
{{Infobox album	
| Name = Jatta
| Longtype = to Jatta
| Type = Soundtrack	
| Artist = Ashley Mendonca and Abhilash Lakra
| Cover = 
| Border = Yes	
| Alt = Yes	
| Caption = 
| Released = 2013
| Recorded = 
| Length =  Kannada 
| Label = 
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Huch Munde"
| Chaitra H. G.
|-
| 2
| "Aarada Ondu Gaya" Hariharan
|-
| 3
| "Kurudu Baavali"
| Sujay N. Harthi
|-
| 4
| | "Preeti Onde Prarthane"
| Rithisha Padmanabh
|}

==Reception==
Jatta received largely positive response from critics upon its release. G. S. Kumar of The Times of India gave the film a four star rating out of five and wrote, "Full marks to director BM Giriraj who has worked wonders with a story that captures human values, conflicts, dilemmas and tries to find a solution to them. With a good script and excellent narration, the director has made the movie interesting not only for the class but also for the mass audience."  Shyam Prasad. S of Bangalore Mirror gave the film a 4.5/5 rating and wrote, "With just a handful of characters, Jatta dabbles with and intertwines various topics like feminism, male chauvinism, religious intolerance, adultery, Ambedkar-isms, corruption and cultural beliefs; all of which are dealt with in a simple yet engrossing narrative." He concluded by praising the role of all the departments in the film including directing, acting, cinematography and music.

==References==
 

==External links==
*  

 
 
 
 