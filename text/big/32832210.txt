Get the Gringo
{{Infobox film
| name           = Get the Gringo
| image          = Get_The_Gringo_Key_Art.jpg
| caption        = Key Art
| director       = Adrian Grunberg
| producer       = Mel Gibson Bruce Davey Stacy Perskie
| writer         = Mel Gibson Stacy Perskie Adrian Grunberg
| starring       = Mel Gibson  Kevin Hernandez Antonio Pinto
| cinematography = Benoît Debie
| editing        = Steven Rosenblum
| studio         = Icon Productions Airborne Productions
| distributor    = 20th Century Fox Home Entertainment   
| released       = May 1, 2012 
| runtime        = 96 minutes   
| country        = United States
| language       = English Spanish 
| budget         = 
| gross          = $5,793,167   
}}
Get the Gringo (also known as How I Spent My Summer Vacation)  is a 2012 American action thriller film directed by Adrian Grunberg, produced, co-written by and starring Mel Gibson. The film has received largely positive reviews, gaining an 81% Fresh rating on Rotten Tomatoes. 

==Plot== El Pueblito prison under false charges, keep the cash for themselves, and cremate the accomplice, who by then has died. As one of the only Americans incarcerated there, the driver becomes known as "the Gringo". El Pueblito proves surprising, operating more like a small ghetto than a prison. The Gringo quickly manages to work out the prisons criminal hierarchy and engages in petty thefts and robberies from some of the prisons less reputable businesses. One of these thefts is witnessed by an unnamed kid (Kevin Hernandez) who is living with his incarcerated mother (Dolores Heredia) and is protected by the prisons criminals. Curious, the Gringo presses him to explain why the criminals protect him, but the Kid refuses.

Later, the Gringo stops the Kid from an ill-fated assassination attempt on Javi ( ), who easily identifies him as a career criminal. Unconcerned, the Gringo and the Kid work towards bringing down Javi, and the Gringo grows closer to the Kids mother. The Gringo ingratiates himself to Javi by saving Javis brother, Caracas (Jesús Ochoa (actor)|Jesús Ochoa) and revealing the money stolen by Vasquez and Romero. Thugs working for criminal boss Frank (Peter Stormare) have already located Vasquez and Romero and are torturing them to find the location of an additional $2 million. Javis men kill everyone and take the money, enraging Frank. With the aid of the Embassy guy, Frank sends assassins into El Pueblito to kill Javi and the Gringo.

The resulting shootout results in the Mexican authorities planning a raid on the prison. Knowing time is short, Javi hires the Gringo to kill Frank and sets up an immediate transplant operation. In the US, the Gringo lures Frank out of hiding by arranging a meeting between him and shipping magnate Warren Kaufmann (Bob Gunton). The Gringo sets up his ex-partner, who betrayed him, and kills Frank. When the Gringo ambushes the Embassy guy, he learns of the transplant operation and rushes to save the Kid, who has unsuccessfully attempted to stab himself in the liver. Using the Embassy guys credentials, the Gringo infiltrates the raid on the prison and interrupts Javis operation. Threatening to kill Javi, the Gringo forces Caracas to retrieve the kids mother, who Javi has tortured. Caracas returns with several thugs, but the Gringo kills them all. A nurse helps the Gringo by pretending to capture him. When Caracas relaxes, the nurse shoots him. They grab the money, and the nurse helps them escape from the prison in an ambulance. In the epilogue, the Gringo recovers the missing $2 million hidden in the escape car, and the Gringo, the Kid, and his mother retire to an idyllic beach, while Kauffman kills the Gringos ex-partner.

==Cast==
 
*Mel Gibson as Driver
*Kevin Hernandez as Kid
*Dolores Heredia as Kids Mom
*Daniel Giménez Cacho as Javi Huerta
*Jesús Ochoa (actor)|Jesús Ochoa as Caracas
*Roberto Sosa as Carnal
*Tenoch Huerta as Carlos
*Peter Gerety as Embassy Guy
*Peter Stormare as Frank Fowler
*Bob Gunton as Warren Kaufmann Scott Cohen as Jackson
*Patrick Bauchau as Surgeon
*Mayra Serbulo as Nurse
*Stephanie Lemelin as Franks lawyers secretary
*Tom Schanley as Gregor
*Mario Zaragoza as Vazquez
*Dean Norris as Bill
 

==Production== script was written by Mel Gibson.  The film is directed by Adrian Grunberg,  who worked as a first assistant director with Gibson on Apocalypto. The film was produced by Gibson, Bruce Davey and Stacy Perskie.  Executive Producers included Mark Gooder, Vicki Christianson, Ann Ruark, Len Blavatnik.    Filming began in March 2010 in San Diego, Brownsville, Texas, and Veracruz, Veracruz|Veracruz, Mexico.  Most of the filming took place at the Ignacio Allende Prison.    Benoît Debie was the cinematographer on the film. 

==Release==
The films theatrical release began in Israel in March 2012 before reaching 22 other countries over the next six months.  In the UK, the film was released under its original title of "How I Spent My Summer Vacation." As of the July, the film had taken in $4.5 million at the box office.  The films US premiere coincided with a ten-city, same-day tour with Mel Gibson appearing at Austins Alamo Drafthouse Cinema with co-star Kevin Hernandez and director Adrian Grunberg on April 18, 2012.  The other nine cities screened the film and received a satellite feed of the Q&A.  The film did not have a regular US theatrical release instead appearing on video on demand (VOD). At the event, Gibson said of the VOD release, "Were just in a different era. Many people just like to see things in their homes....I think its the future."  The film was first publicly released exclusively through video-on-demand services in the United States.         In Icon’s deal with Fox, Get the Gringo did have an exclusive preview window on DirecTV May 1 before it was released on other VOD services a month later. Its release date on Blu-ray Disc was set as July 17, 2012, by 20th Century Fox Home Entertainment in the US. 

==Reception==
Get the Gringo has received largely positive reviews.  On Rotten Tomatoes, a review aggregator, the film received positive reviews by 81% of the 52 surveyed critics; the average rating was 6.3/10.   
 Time Out London rated the film 3/5 stars and called it "superbly constructed, pithily scripted, and absurdly entertaining" despite the racist stereotypes and viciousness.   Todd McCarthy of The Hollywood Reporter likened the film to 1970s exploitation films, such as Bring Me the Head of Alfredo Garcia, composed of efficient and effective scenes.  Variety s Peter Debruge also compared the film to Sam Peckinpahs films, and states that the film is an ideal showcase for director Grunberg. 

==See also==
 
*Mel Gibson filmography

 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 