Love & Debate
Love & Debate  is a drama/romance film released in 2006 based on decisions everyday teenage girls have to make. 

It is a story of a young girl who has become the star in the debate club in her high school. 
Her love for debate was ultimately inspired by watching politicians on television. 
Her main role-model is Hillary Clinton (before the 2008 elections) and so she follows her foot-steps by joining the Harvard Debate Team.

==Plot==

Jordan Landa (Gina Philips) is in her senior year of high school when she joins the debate team on the recommendation of a teacher (Sean Astin) who is in charge of the debate team. She quickly shows promise and advances through competition and decides that she wants to pursue a career the field. However, during the debating championship, Jordan is raped by another debater (Austin Nichols). Despite the trauma, Jordan is undeterred from her goals, and is invited to join the debate team at Harvard University.

Soon into her college experience, Jordan meets and falls in love with Chris (Bryan Greenberg), a fellow student and singer. Her Latino-Jewish family quickly disapproves because he does not share her heritage. Jordan adopts her boyfriends partying, pot-smoking, heavy drinking lifestyle, and her constant absences from practices eventually cause her to lose her spot on the Harvard debate team. Jordan continues to date Chris, despite her friends and familys concerns. Eventually they break up because of Chris unfaithfulness.

The summer before her senior year of college, Jordan develops a relationship with Elias (Alex Rodriguez), a Latino-Jewish boy who her parents set her up with when she was still in high school.  At the beginning of the academic year, Jordan approaches Sajan (Sendhil Ramamurthy), her former partner, about reuniting for that years debate season. He is initially reluctant, but Jordan convinces him. That year brings debating success for Jordan as she falls in love with Elias. 

At the National Finals competition for debate, Jordan finds out that she will have to debate Alex, her rapist, to win the championship. Elias is resistant, but Jordan decides that the final tournament is too important to skip. At first, Jordan stumbles and struggles. However, she regains her confidence and gives a stirring speech at the conclusions which leads Jordan and Sajan to victory. That night, Elias tells Jordan that he was accepted into a medical residency program in Miami and he proposes. Jordan refuses because she does not want to turn out like her mother, but Elias tells her that he will wait. That same night, family tragedy strikes and Jordan decides what she wants for her future.

==Cast==

*Gina Philips - Jordan
*Sean Astin - Coach Amal
*Adam Rodriguez - Elias
*Rachel Miner - Sophia
*Bryan Greenberg - Chris
*Sendhil Ramamurthy - Sajan

==External links==
* 

 
 