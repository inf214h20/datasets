Dead Men Don't Wear Plaid
 
{{Infobox film
| name           = Dead Men Dont Wear Plaid
| image          = Deadmenplaidposter.jpg
| image_size     = 
| caption        = Theatrical poster
| alt            = 
| director       = Carl Reiner
| producer       = William E. McEuen Richard McWhorter David V. Picker
| writer         = Carl Reiner George Gipe Steve Martin
| narrator       = Steve Martin
| starring = {{plainlist|
* Steve Martin
* Rachel Ward
* Reni Santoni
* Carl Reiner
}}
| music          = Miklós Rózsa Steve Goodman Michael Chapman
| editing        = Bud Molin
| studio         =  Aspen Film Society
| distributor    = Universal Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $9 million
| gross          = $18,196,170
}} homage to, pulp detective movies of the 1940s.

Edited by Bud Molin, Dead Men Dont Wear Plaid is partly a collage film, incorporating clips from 18 different vintage films. They are combined with more recent footage of Martin and other actors similarly shot in black-and-white, with the result that the original dialogue and acting of the classic films have now become part of a completely different story.

Among the actors who appear from classic films are Ingrid Bergman, Humphrey Bogart, James Cagney, Joan Crawford,  Bette Davis, Brian Donlevy, Kirk Douglas,  Ava Gardner, Cary Grant, Alan Ladd, Veronica Lake,  Burt Lancaster, Charles Laughton, Fred MacMurray, Ray Milland, Edmund OBrien, Vincent Price, Barbara Stanwyck, and Lana Turner.

This was the last film for both costume designer Edith Head and composer Miklós Rózsa.

==Plot== Keeper of the Flame). In the next scene, private investigator Rigby Reardon (Steve Martin) is reading a newspaper when Forrests daughter, Juliet (Rachel Ward), enters his office and faints when the papers headline reminds her of her fathers death. Upon coming to, she hires Rigby to investigate the death, which she thinks was murder. In Dr. Forrests lab, Rigby finds two lists, one titled "Friends of Carlotta" and the other "Enemies of Carlotta", as well as an affectionately autographed photo of singer Kitty Collins, whose name appears on one of the lists. His search is interrupted by a man posing as an exterminator (Alan Ladd, in This Gun for Hire), who shoots Rigby in the arm and frisks the lists from the seemingly dead investigator.

Rigby manages to find his way to Juliets house, where she sucks out the bullet, snakebite-style, and points Rigby to the club at which Kitty sings. Juliet also reveals a note to her father from her alcoholic brother-in-law, Sam Hastings, which in turn reveals that Dr. Forrest gave him a dollar bill "for safekeeping". Despite warnings that the mentally disturbed Leona will not be of much use, Rigby calls Leona, who after a rambling discussion, hangs up (Barbara Stanwyck, in Sorry, Wrong Number). On the way out, Juliet asks Rigby to leave further news with her butler or cleaning woman.  Mention of the latter causes Rigby to go berserk due to his own father running off with the cleaning woman and his mother dying of a broken heart.
 Lost Weekend) The Killers) at the Brentwood Room.  He asks if shes one of Carlottas friends, which causes her to leave abruptly. He trails her to a restaurant, where she ditches her brooch into her soup.  Rigby subsequently retrieves the brooch, which contains an "EOC" (Enemies of Carlotta) list, on which all names are crossed out, except Swede Andersons. Rigby visits Swede (Burt Lancaster, from The Killers) but while Rigby prepares a "Java coffee|java", Swede is killed.
 The Big Year of the Rat. Upon exiting, she asks Rigby to call with any progress. Marlowe arrives, and picks up the EOC list to check against unsolved murders.

Rigby goes to the train station to collect the contents of locker 1936, which contains more lists. A "handsome" guy (Cary Grant, from Suspicion (1941 film)|Suspicion) follows him onto a train but, Rigby puts him to sleep with the help of his harmonica. Rigby finds F.X. Huberman, whose name he found on one of the lists and who turns out to be a "classy dame," throwing a party (Ingrid Bergman, from Notorious (1946 film)|Notorious). She flirts with Rigby (represented by Cary Grants silhouette), then drugs his drink and steals the locker key.

Rigby wakes up back at his office, where Juliet informs him that Sam Hastings fell out of a window to his death.  She also has a New York Times reference for him from her fathers office.  The reference is to an article about a South American cruise ship called Immer Essen (German for always eating) on whose last voyage Sam Hastings was a passenger. When Marlowe (Bogart, from The Big Sleep) calls, Rigby questions him about Walter Neff, the ships owner, and learns that Neff cruises supermarkets looking for blondes.
 The Glass The Postman Edward Arnold, from Johnny Eager) by giving him a puppy. He then is beaten up by four thugs (Kirk Douglas and others from I Walk Alone). 
 Double Indemnity). Rigby drugs him and finds documents about the Immer Essen, including a passenger list identical to an EOC list, and articles about the ships imprisoned captain, Cody Jarrett, who refuses to talk to anyone about it but his mother. Rigby then dresses up as Jarretts mother to visit Jarrett in prison without arousing the prison guards suspicion (James Cagney from White Heat). He tries to win Jarretts confidence by explaining the Friends of Carlotta are after him. Rigby doesnt learn anything from Jarrett though, so he cashes in a favor with the warden to act as a prisoner for a few days. Jarrett turns out to be a Friend of Carlotta after all, kidnaps Rigby on a jail break, and shoots him while hes still in the trunk of the getaway car.  

After sucking out a third bullet, Juliet leaves for the drugstore for medicine. On her way out, a call comes in from an old flame (Joan Crawford, in Humoresque (film)|Humoresque).  Juliet overhears parts of it, takes it to be a double dating by Rigby and closes the case. While Rigby is drinking, thinking himself betrayed by Juliet, Marlowe calls and tips Rigby off that Carlotta is an island off Peru. At a cafe, Rigby finds Kitty Collins (Ava Gardner, from The Bribe) there. Carlos Rodriguez (Reni Santoni), a local policeman from Rigbys gun-running past, warns Rigby of the locals, including Kittys new boyfriend, Rice. The next day, one of the characters Rodriguez warned Rigby of (Charles Laughton, from The Bribe) approaches him and tries to bribe Rigby into leaving the island.

Next, Kitty drops by Reardons room. Carlos calls to tell him Rice is in town with a group of Germans when the telephone line is cut. Kitty then drugs Rigbys drink, causing him to pass out. He wakes up to see Rice (Vincent Price, from The Bribe) trying to suffocate him. After exchanging shots and chasing through the "Fiesta de Carlotta" fireworks celebration (much of it archived footage from The Bribe), Rigby shoots Rice and frisks the corpse for instructions leading him to a hideout where he finds Juliet, her father (actually still alive), and her butler, who introduces himself as Field Marshal Wilfried von Kluck (Carl Reiner).

Rigby and the Field Marshal compete about the right to explain what happened. It turns out that Dr. Forrest had been tricked into divulging a secret cheese mold by Nazis posing as a humanitarian organization. Once he discovered their true intent, to use the molds corrosive properties to destroy America and make a comeback, he assembled a list of Nazi agents, the "Friends of Carlotta." Before he could divulge the names to the FBI, he was abducted and his death faked to prevent a police investigation. The Immer Essen, a cruise ship passing by, witnessed the corrosive effects of the mold tests, making all passengers "Enemies of Carlotta" and targets for murder. Rigby is captured but Juliet gets the Field Marshal to say "cleaning woman," causing Rigby to go berserk, break his chains and overpower the Nazis. While Juliet gets Rodriguez, the Field Marshal manages to pull one of the switches, destroying Terre Haute, Indiana, before being shot dead by Rigby. Rodriguez rounds up the other Nazis while Rigby shares a long kiss with Juliet.

==Production notes==
In the summer of 1980, comedian Steve Martin was having lunch with director Carl Reiner and screenwriter George Gipe. {{cite news
 | last = 
 | first = 
 | coauthors =
 | title = Dead Men Dont Wear Plaid Production Notes
 | work =
 | pages =
 | language =
 | publisher = Universal Pictures
 | year = 1982
 | url =
 | accessdate = }}  They were also discussing a screenplay Martin had written when he suggested that they use a clip from an old film. From this suggestion came the idea of using all sorts of clips from films throughout the entire feature. The three men left the lunch thinking about how they could incorporate all of these old clips into a story. Reiner planned to work Martin into the old footage via over-the-shoulder shots so that it looked like the comedian was talking to these vintage actors, a strategy used effectively several times in the film. In one scene, trick photography makes it appear that Martin is in the same shot (not over-the-shoulder) as Cary Grant in a clip from Suspicion. Reiner and Gipe spent countless hours looking through classic films for specific shots and "listening for a line that was ambiguous enough but had enough meat in it to contribute a line".  They took lines of dialogue from clips they wanted to use and juxtaposed them while also trying to write a story based on them. Reiner and Gipe finally worked out a story and then met with Martin who contributed some funny material of his own. 

Martin purposely chose not to watch any classic film noirs because he "didnt want to act like Humphrey Bogart ... I didnt want to be influenced".  The filmmakers enlisted some of the people that helped define many of the classic films from the 1940s. Costume designer Edith Head created over 20 suits for Martin in similar fashion to those worn by Cary Grant or James Stewart. Production designer John De Cuir, a veteran with 40 years of experience, designed 85 sets for the ten-week shooting schedule. Director of photography Michael Chapman studied the angles and lighting popular among 40s film noir, conducting six months of research with Technicolor to try to match the old film clips with his new footage. 
 Laird International Studios in Culver City and three exterior locations shot in and around Los Angeles. Martin usually acted opposite actors dressed exactly like the classic movie stars he was interacting with so that he had someone to talk to and would respond to his lines. 

The gag used in the film that employs Rigby going berserk whenever the term "cleaning woman" is heard is an homage to the old Slowly I Turned vaudeville routine.

==Films used==
The following films were used in Dead Men Dont Wear Plaid. Five films were already owned by Universal, and the rest were licensed from other studios. Note that some of the film license owners have changed since the original release of the films.

=== Universal ===
* This Gun for Hire (1942) The Glass Key (1942) Double Indemnity (1944) The Lost Weekend (1945) The Killers (1946)

=== Warner Bros.* ===
* Deception (1946 film)|Deception (1946)
* Humoresque (film)|Humoresque (1946) The Big Sleep (1946) Dark Passage (1947)
* White Heat (1949)
   * At the time of release, United Artists owned these films.  

=== MGM ===
* Johnny Eager (1941) Keeper of the Flame (1942) (uncredited) The Postman Always Rings Twice (1946)
* The Bribe (1949)

=== RKO Pictures ===
* Suspicion (1941 film)|Suspicion (1941)
* Notorious (1946 film)|Notorious (1946)

=== Paramount Pictures ===
* I Walk Alone (1947)
* Sorry, Wrong Number (1948)

=== Columbia Pictures ===
* In a Lonely Place (1950)

==Critical reception== Pennies from Heaven." {{cite news
 | last = Ansen
 | first = David
 | coauthors =
 | title = This Film for Hire
 | work = Newsweek
 | pages =
 | language =
 | publisher = 
 | date = May 24, 1982
 | url = 
 | accessdate = }}  Vincent Canbys review for The New York Times praised Martins performance: "the film has an actor whos one of Americas best sketch artists, a man blessed with a great sense of timing, who is also self-effacing enough to meet the most cockeyed demands of the material." {{cite news
 | last = Canby
 | first = Vincent
 | coauthors =
 | title = Steve Martin Stars in Reiner Comedy
 | work = The New York Times
 | pages =
 | language =
 | publisher = 
 | date = May 21, 1982
 | url = http://movies.nytimes.com/movie/review?_r=3&res=990CEEDB143BF932A15756C0A964948260&scp=9&sq=dead+men+don%27t+wear+plaid&st=nyt&oref=login
 | accessdate = 2009-11-06}}  Time (magazine)|Time magazines Richard Corliss wrote, "The gag works for a while, as Martin weaves his own plot-web into the 18 old movies, but pretty soon hes traveling on old good will and flop sweat". {{cite news
 | last = Corliss
 | first = Richard
 | coauthors =
 | title = White Meat Time
 | pages =
 | language =
 | publisher = 
 | date = May 17, 1982
 | url = http://www.time.com/time/magazine/article/0,9171,921236,00.html
 | accessdate = 2009-11-06}} 

==References==
 

==External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 