Ghulam (film)
 
 
 
{{Infobox film
| name           = Ghulam
| image          = Ghulam(Movie) Poster.jpg
| caption        = Theatrical poster Ghulam
| director       = Vikram Bhatt
| producer       = Mukesh Bhatt
| story          = Anjum Rajabali
| narrator       = 
| starring       = Aamir Khan Rani Mukerji Deepak Tijori Sharat Saxena
| music          = Jatin-Lalit Teja
| editing        = Waman Bhonsle
| distributor    = Vishesh Films
| released       = 19 June 1998
| runtime        = 
| country        = India
| language       = Hindi
| awards         =  
| budget         = 
}}

Ghulam ( : غلام, translation: Slave) is a 1998 Indian action crime drama film directed by Vikram Bhatt and starring Aamir Khan. The film was inspired by Elia Kazans On the Waterfront (1954). It is the second remake of On the Waterfront after Kabzaa starring Sanjay Dutt.  Ghulam did moderately well at the box office and was declared as Above Average .   

== Plot ==
 
Siddharth "Siddhu "Marathe (Aamir Khan) is a Mumbai tapori, a boxing champion. His brother, Jaidev "Jai" (Rajit Kapur), is the accounts manager and right hand man for Raunak "Ronnie" Singh (Sharat Saxena), a former boxing champion who ostensibly runs a travel agency but in reality rules the local community by terrorising people and stealing money from innocent merchants. Other than his boxing practice, Siddhu leads a relatively aimless and wanderlust life. In his spare time, he hangs out with friends, occasionally stealing money from rich people. Siddhu is financially dependent on his elder brother Jai, who lives in Raunak Singhs house but occasionally visits Siddhu and tops up his finances.

The opening scene features a female lawyer Fatima Madam (Mita Vashisht) defending Siddhu in court against accusations of small-time theft. The lawyer tries to convince the judge to show leniency towards Siddhu on account of his disadvantaged background as an orphan. The judge points out that Siddhu has already been shown leniency four times, and is greeted with laughter by everybody, including Siddhu. In the commotion, Siddhu steals Rs. 400 from his own lawyers purse. Siddhu is let free. Later, when he asks the lawyer what her fees are, she asks him for a fees of Rs. 400, and he is thus forced to part with the money he has just stolen. The lawyer gives no indication of awareness that Siddhu had stolen an equal amount of money. Later, Siddhu, while joking around with friends, gets angry when one of his friends suggests that the lawyer may be receiving sexual favours from Siddhu in exchange for defending him in court.

A few days later, Raunak Singh hires Siddhu to deliver a letter to a local cricket player, and to beat up the player if he shows signs of resisting. Unknown to Siddhu, the letter contains instructions to the player to get out after scoring a certain number of runs, so that Ronnie can win a bet. Siddhu delivers the letter and intimidates the cricket player into agreeing to follow the instructions, breaking the players bat in the process. Later, while returning, he gets into a motorcycle speed race with a motorcycle gang led by Charlie (Deepak Tijori), which escalates into a dare game to run towards a moving train at night. Siddhu beats Charlies past record in the game. Charlie chooses to run again, but falls on the train tracks, and seems to be headed for death as the train approaches him. Siddhu rescues Charlie at considerable personal risk. In the process, Siddhu becomes friends with Alisha Mafatlal (Rani Mukerji), also part of the gang, and their friendship blossoms into love. The famous song Aati Kya Khandala is situated during this period, when Siddhu is trying to cheer Alisha up after a fight with her father.

It is revealed that Siddhu saw his own fathers death when he was a child which mentally affected him throughout his life. His fathers words – "anybody can swim with the tide, but the truly brave person is he who swims against the tide"—remained with him, though he had only been with his father for a little while.

Back in his local community, Siddhu witnesses an incident of some of Ronnies men beating up a local restaurateur for not paying extortion money to Ronnies gang. The restaurateur runs for his life as Ronnies men chase him, but none of the other community people come to his aid. Harihar Mafatlal (Akshay Anand), a social worker, coaxes a police constable to stop the fight. The restaurateur and his two assailants land up on the roof where Siddhu is doing boxing practice, with Hari and the constable following. The constable breaks up the fight and scolds the restaurateur, and Hari is angry at the constable for blaming the victim. The assailants leave, greeting Siddhu on the way out. Siddhu offers some water to the restaurateur and introduces himself to Hari, suggesting to both to avoid getting on the wrong side of Ronnie. Hari speaks of principle and self-respect, and Siddhu is reminded of things that his father told him long ago.

Ronnie is furious to hear about this, and wishes to kill the restaurateur and Hari immediately. Ronnie explains the logic of terror to his henchmen: if even a few people stop acceding to his demands, then that will instill rebelliousness against Ronnies demands in others. Fear is essential for the kind of respect Ronnie enjoys, and being lenient against a few people who stand up to him based on a short-term cost–benefit analysis will have long-term negative consequences for Ronnie. Jai, however, dissuades Ronnie from taking rash action. In the process, it is revealed that Jai and Ronnie are in the process of bribing politicians and government officials to make Ronnie the owner of a large construction project.

Hari later canvasses some villagers together, trying to get one of them to sign a complaint against Ronnie that can be filed with the police so that official action can be taken against him. Siddhu attends the meeting at Jais request, but does not inform Jai or Ronnie about it immediately. Instead, Siddhu offers Hari a friendly warning to  . Hari reveals the principle guiding his own actions: whenever he sees himself in the mirror, he should not feel ashamed of what he sees.

Ronnie nonetheless comes to know of the events during the meeting, and is angry. He tells Siddhu to arrange a meeting between Hari and himself (Ronnie) so that Ronnie can dissuade Hari from these activities. Siddhu agrees, and calls Hari over to a bridge, on the pretext that he himself (Siddhu) needs to talk to Hari. Hari arrives at the bridge, expecting to meet Siddhu. Ronnie arrives along with his men, beats up Hari, and throws him from the bridge after which he is crushed under a moving train. Siddhu is furious at Jai and Ronnie, and attacks Ronnie physically, but Ronnies men control him, and he is allowed to go on account of the fact that he is Jais brother and he was the one who managed to inadvertently help Ronnie kill Hari. Siddhu, returning home, cannot bear the sight of himself in the mirror, and breaks it. It is also revealed that Hari was the brother of Alisha (Siddhus romantic interest), something Siddhu had been unaware of because he was living separately. Siddhu confesses everything to Fatima Madam, the female lawyer who had defended him in the beginning of the movie, but refuses to testify against Ronnie in court, for fear of implicating his elder brother Jai. The lawyer tries to talk him into testifying, but fails. She challenges him to reveal the truth to Alisha, which he does, and they break up.

In a subsequent boxing match (against boxing champion "Kala Tiger"), that Siddhu has been preparing for several months, Siddhu is told by his brother, in the midst of the game, to throw away the match, since Ronnie has bet money on Siddhu losing. Siddhu throws away the game, allowing the other boxer to beat him unconscious. He is furious at his brother, and they have an oral confrontation. In the process, Siddhu is forced to confront that his father had betrayed five of his friends in the Indian independence movement to the British out of fear of being tortured, causing all of them to be killed. Siddhu realises that his father was a good man, but, like everybody else, was a coward and lacked the strength to fight injustice. Yet, he knows that his father wanted to instill these values in him, and he vows to bring Haris killers to justice and complete the task that Hari attempted to begin. He tells Fatime Madam (the lawyer) that he is willing to testify publicly against Ronnie as well as against his own brother Jai, and also confesses to her his theft of Rs. 400 (shown at the beginning of the movie). She expresses pride in Siddhu, revealing that she knew all along about the theft and also knew that Siddhu would reveal it to her some day of his own accord. She tries to get Alisha to forgive Siddhu, but fails. Siddhu, now a reformed man, meets up with the cricketer he had beaten up a while back, presents him with a new cricket bat, and seeks forgiveness.

When Ronnie discovers that Siddhu is the person who has filed a complaint against him, he is ready to kill Siddhu. Jai dissuades Ronnie, reassuring him that Jai himself will dissuade Siddhu from testifying publicly against Ronnie. In an emotional scene, Siddhu, when approached by Jai, confronts Jai instead, accusing Jai of neglecting his duties as an elder brother by encouraging Siddhu to follow himself in a life of crime. Jai realises his error and apologises. Upon returning home, Siddhu discovers Alisha waiting for him, and they embrace and reconcile. Later that night, Ronnie kills Jai and his men also try to kill Siddhu, but the motorcycle gang led by Charlie (whose life Siddhu had saved earlier) intervenes to save Siddhus life. Siddhu wants to kill Ronnie in person to avenge his brothers death, but the female lawyer arrives in time to dissuade him.

The next day, in court, Siddhu gives his testimony regarding Ronnies murder of Hari. The court is adjourned until Monday. Coming out of court, Ronnie orders for a local bandh and forces all the shops to close down. He also has Siddhus home broken into and his belongings thrown on the street. Siddhu, upon seeing this, walks up to Ronnies house and challenges him to come out and duel alone, rather than hiding behind henchmen. Ronnie agrees, and all the people in the area come out to watch. The two boxers have a long and bloody boxing match, with nobody interfering. As the local people see one man with the courage to fight Raunak Singh, they awaken to the possibility that they too can resist his extortionary demands. Ronnie loses the fight, but he then orders his henchmen to kill Siddhu. The local people, who are much more numerous than Ronnies henchmen, block the attempt. Together, they beat up Ronnie and his henchmen and force them to flee the area.

==Cast==
*Aamir Khan...... Siddharth Marathe (Siddhu)
*Rani Mukerji...... Alisha (Mona Ghosh Shetty for the dubbing voice)
*Deepak Tijori...... Charlie
*Sharat Saxena...... Raunak Singh (Ronnie)
*Akshay Anand...... Hari
*Rajit Kapoor...... Jai, Siddharths brother
*Mita Vashisht...... Siddharths lawyer Fatima Madam
*Dalip Tahil...... Siddharths father
*Raju Kher ...... Alishas Father
*Ashutosh Rana...... Shyamsundar Agrawal
*Kamlesh Oza...... Avinash (Haris Brother)
*Amin Hajee...... Black Tiger (Champion Boxer) Prithvi

==Soundtrack==
{{Infobox album |  
|  Name        = Ghulam |
|  Type        = Album |
|  Artist      = Jatin-Lalit |
|  Cover       = Ghulam CD Cover.jpg|
|  Alt            = Feature film soundtrack |
|  Length     =  
|  Label       =  Tips Music | Hindi
| Last album = Dhoondte Reh Jaaoge (1998)
| This album = Ghulam (1998)
| Next album = Jab Pyaar Kisise Hota Hai (1998)
}}

The music were composed by Jatin-Lalit. Lyrics handled by Indeevar, Nitin Raikwar, Sameer, Vinod Mahendra.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! Song !! Singer(s) !! Duration!! Picturised on
|-
|"Aankhon Se Tune Yeh Kya"
| Kumar Sanu, Alka Yagnik
| 5:05
| Aamir Khan, Rani Mukherjee
|-
|"Aati Kya Khandala"
| Aamir Khan, Alka Yagnik
| 4:12
| Aamir Khan, Rani Mukherjee 
|-
|"Jadu Hai Tera Hi Jadu"
| Kumar Sanu, Alka Yagnik
| 7:43
| Aamir Khan, Rani Mukherjee 
|-
|"Ab Naam Mohabbat Ke"
|  Udit Narayan, Alka Yagnik
| 5:19
| Aamir Khan, Rani Mukherjee 
|-
|"Saath Jo Tera Mil Gaya"
| Udit Narayan, Alka Yagnik
| 5:26
| 
|-
|"Tujko Kya"
| Udit Narayan, Jojo, Surjeet
| 6:07
| Aamir Khan
|}

==Reception==
The film was declared a Above Average and its total gross was 200&nbsp;million. 

==Awards==
;Filmfare Awards
*Nominated—Best Film
*Nominated—Best Actor (Male) – Aamir Khan

;Zee Cine Awards
*Nominated—Best Actor (Male) – Aamir Khan
*Nominated—Best Actor (Female) – Rani Mukerji

==Trivia==

===Stunts===

A sequence in the film shows running on rail tracks towards an oncoming train, which misses him by a few feet as he jumps off the tracks. 1.3 seconds was the only difference between Aamir and the train. This stunt was actually performed, and by Aamir himself. It won the Best Scene Award at the 44th Filmfare Awards, but he later criticised himself for taking such an unnecessary risk. 

===Dubbing===
 Mona Shetty,  who had a much more high-pitched voice. When asked if the directors decision to not use her voice in the film affected her, she said that her voice was dubbed as it "did not suit the character".  

===Remake===

It was remade in Tamil as Sudhandhiram (2000)

==References==
 

==External links==
*  

 

 
 
 
 
 