Youngistaan
 
 
{{Infobox film
| name = Youngistaan
| image = File:Youngistaan.jpg
| caption = Theatrical release poster
| director = Syed Ahmad Afzal
| starring = Jackky Bhagnani Neha Sharma Farooq Sheikh Kayoze Irani
| Music = Jeet Gannguli Sneha Khanwalkar Shiraz Uppal Shree Isshq released =  
| country = India Hindi
| gross =   
| Awards = Nominated for Academy Awards
}}
Youngistaan is a Bollywood film directed by Syed Ahmad Afzal released on 28 March 2014. It stars Jackky Bhagnani, Neha Sharma, Farooq Sheikh, Deepankar De and Kayoze Irani in lead roles. It is a remake of the Telugu film Leader.    The film is a love story set against the backdrop of Indian politics.         

Sheikhs performance as Akbar Patel was notably praised. 
 
 
 
 

It has made it to the list of 87th Academy Awards best foreign film category as an independent entry. 

==Plot==

Youngistaan is a love story set in the backdrop of Indian politics. It is the story of Abhimanyu Kaul & the love of his life, Anwita Chauhan. Abhimanyu Kaul, a young boy living an ordinary life in Japan, finds himself in the political spotlight due to the sudden death of his father, the Prime Minister of India. Travel with Abhimanyu, as he struggles to balance his complicated personal relationships with the political resistance against him from within his own party. Being a public figure, by reluctantly accepting to represent the governing party, much against his own wishes and at the cost of his private life, is a double-edged sword that Abhimanyu must walk on.

Thought of as an amateur and incapable of handling the issues at large by one and all, (except the ever faithful Akbar Patel, Secretary to the P.M.), the film closes as a victorious Abhimanyu changes the course of events and turns the tide his way, through his hard work, honestly, and above all, a political legacy – a sharp, leading mind that not everyone inherits.

==Cast==

*Jackky Bhagnani as Abhimanyu Kaul/Abhi
*Neha Sharma as Anwita Chauhan
*Farooq Sheikh as Akbar Uncle
*Boman Irani as Dashrat Kaul
*Deepankar De as Shubhodeep Ganguly popularly known as Shubho Da (President of ABKP Party,Interim Prime Minister & later President)
*Kayoze Irani as Ajay Doshi

==Production and promotion==
While the first schedule of the film took place in Indore, Lucknow  and overseas—the second schedule was held at the Taj Mahal in Agra, where a campaign titled Yo Youngistan Go Youngistan was launched.      

The first trailer of Youngistaan was unveiled at a suburban multiplex in Mumbai on 1 February 2014.         

==Critical reception==

Shubha Shetty-Saha of Mid-Day gave the film 2.5 out of 5 stars stating, "While the film has a very interesting premise, it is totally diluted by lazy scriptwriting and sketchy direction." 
Shubhra Gupta of The Indian Express rated the film 2 out of 5 stars and stated "The film, despite its efforts, becomes muddled, and dull." 
Anupama Chopra of Hindustan Times rated the film 1 out of 5 stars saying "Youngistaan is brain-dead and insufferable." 
Paloma Sharma of Rediff.com rated the film 1 out of 5 stars saying "Youngistaan neither says something new nor does it reinforce time-tested wisdom in a way that you actually want to pay attention to it." 

==Box office==
The film opened to a "low" occupancy of 5–10% on the first day of its release—across 1000 theatres in India—with the other two releases of the day: Dishkiyaoon and O Teri.   

According to exhibitor Rajesh Thadani, "Youngistaan raked in Rs 40&nbsp;million during the first weekend."  On its 1st Monday, the film saw a sharp decline in its gross collection earning in the range of 7.5&nbsp;million, thus taking the films domestic total up to a cumulative of 48.0&nbsp;million at the box office. 

==Music==
{{Infobox album |  
| Name = Youngistaan
| Type = Soundtrack
| Artist = 
| Cover = 
| Released = 
| Recorded = Feature film soundtrack
| Length = 
| Lyrics = 
| Label = Big Music
| Producer = 
| Reviews =
| Chronology = 
| Last album = 
| This album = 
| Next album = 
}}


Music has been composed by Jeet Ganguly, Sneha Khanwalkar, Shiraz Uppal & Shree Isshq whilst the background score were composed by Salim-Sulaiman. Lyrics have been penned by Sanamjeet, Syed Ahmad Afzal, Hard Kaur, Sneha Khanwalkar, Jackky Bhagnani, Kausar Munir & Sonny Ravan. 

{{tracklist
| headline        = Tracklist
| music_credits   = Jeet Ganguly
| extra_column    = Artist(s)
| title1          = Suno Na Sangemarmar
| extra1          = Arijit Singh
| length1         = 3:22
| title2          = Mere Khuda
| extra2          = Shiraz Uppal
| length2         = 4:09
| title3          = Tanki (Mika Version)
| extra3          = Mika Singh, Bhavin Dhanak, Apeksha Dandekar, Sneha Khanwalkar
| length3         = 5:27
| title4          = Daata Di Diwani (Qawwali)
| extra4          = Rafaqat Ali Khan & Shiraz Uppal
| length4         = 5:00
| title5          = Tanki (Bhaven Version) 
| extra5          = Hard Kaur, Apeksha Dandekar & Sneha Khanwalkar
| length5         = 5:00
| title6          = Youngistaan Anthem
| extra6          = Shree D & Ishq Bector
| length6         = 4:45
| title7          = Suno Na Sangemarmar (Remix)
| extra7          = Arijit Singh
| length7         = 5:23
| title8          = Youngistaan Anthem (Remix)
| extra8          = Shree D & Ishq Bector
| length8         = 4:48
}}

==Sequel==

The makers have announced a sequel to the film titled Youngistaan dobara, which would narrate the story after Jackky Bhagnanis character becomes Prime Minister. 

==References==
 

==External links==
*  

 
 
 
 
 