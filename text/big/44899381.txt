How Could You, Caroline?
{{Infobox film
| name           = How Could You, Caroline?
| image          =
| alt            =
| caption        =
| director       = Frederick A. Thomson
| producer       =
| writer         =
| screenplay     = Agnes Christine Johnston
| story          =
| based on       =
| starring       = Bessie Love James W. Morrison Dudley Hawley
| cinematography =
| editing        =
| studio         = Pathé Exchange 
| distributor    = Pathé Exchange 
| released       =   reels 
| country        = United States Silent (English intertitles)
| budget         =
| gross          =
}}
How Could You, Caroline? is a 1918 silent comedy-drama film directed by Frederick A. Thomson, with a screenplay by Agnes Christine Johnston.   It stars Bessie Love, James W. Morrison, and Dudley Hawley. 

==Plot==
Caroline (Love), a student at a boarding school, leaves her boyfriend to elope with Reginald, a chauffeur (Hawley). When the chauffeur is exposed as a thief, Caroline returns to her boyfriend Bob (Morrison).  

==Cast==
*Bessie Love as Caroline   
*James W. Morrison as Bob 
*Dudley Hawley as Reginald 
*Henry Hallam as Mr. Rodgers 
*Edna Earle as Ethel
*Amelia Summerville as Mrs. Rodgers

==Reception==
A contemporaneous review called the plot "rather frail and considerably padded at the end", but praised the performances of Love and Hawley.   

Screenwriter Agnes Christine Johnstons depiction of the female lead character has been praised as efficiently creating complex female identities.   

==References==
 

==External links==
*  
*  

 
 