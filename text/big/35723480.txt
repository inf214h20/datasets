Six Lessons from Madame La Zonga
 
{{Infobox film
| name            = Six Lessons from Madame La Zonga
| image           = Poster - Six Lessons from Madame La Zonga.jpg
| image_size      =
| caption         = Promotional poster for the film. John Rawlins
| producer        = Joseph Gershenson
| writer          = Ben Chapman and Larry Rhine
| narrator        =
| starring        = Lupe Vélez Leon Errol
| music           = Everett Carter Milton Rosen
| cinematography  = John W. Boyle
| editing         = Edward Curtiss
| distributor     = Universal Pictures
| released        =  
| runtime         = 62 minutes
| country         = United States
| language        = English
}}

Six Lessons from Madame La Zonga is a 1941 American comedy film starring Lupe Vélez.  The film was inspired by the same-name song interpreted by Helen OConnell and Jimmy Dorsey Orchestra.

==Plot==
 

==Cast==

* Lupe Vélez as Madame La Zonga
* Leon Errol as Señor Alvarez/Mike Clancy
* William Frawley as Beheegan
* Helen Parrish as Rosita Alvarez

==References==
 

==External links==
*  

 
 
 
 