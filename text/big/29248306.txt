The New Year Parade
{{Infobox film
| name           = The New Year Parade
| image          = The New Year Parade.jpg
| alt            = 
| caption        =  Tom Quinn
| producer       = Steve Beal Tom Quinn
| writer         = Tom Quinn
| starring       = 
| music          = 
| cinematography = Tom Quinn
| editing        = Tom Quinn
| studio         = Two Street Pictures
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The New Year Parade is a 2008 drama film filmed and directed by Tom Quinn with lighting and audio work by Mark Doyle. The film won the Grand Jury Prize for Best Narrative at the 2008 Slamdance Film Festival and the award for Best Acting Ensemble at the 2008 Ashland Independent Film Festival. At the 2008 BendFilm Festival, Quinn won the award for Best Director, while Jennifer Welsh won the award for Best Supporting Actress.  The film was nominated for an IFP Gotham Award in 2008 for "Best Film Not At A Theater Near You" while Quinn and co-Producer Steve Beal were nominated for a 2010 Independent Spirit Award in the John Cassavetes Award section for their work on the film.

==Cast== Andrew Conway as Mike
* Mary Ann McDonald as Lisa
* Greg Lyons as Jack Jennifer Welsh as Kat

==References==
 

==External links==
* 
* 
* 

 
 
 