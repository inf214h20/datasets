The Tell-Tale Heart (1960 film)
 
 
{{Infobox film
| name           = The Tell-Tale Heart
| image          = 1960TellTaleHeart.jpg
| image_size     = 150px
| alt            = 
| caption        = DVD cover
| director       = Ernest Morris
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         = Brian Clemens Eldon Howard based on = the short story by Edgar Allan Poe
| narrator       = 
| starring       = Laurence Payne Adrienne Corri Dermot Walsh
| music          = Tony Crombie Bill LeSage James Wilson
| editing        = Derek Parsons
| studio         = 
| distributor    = Associated British Picture Corporation|Warner-Pathé Distributors 
| released       = December 1960
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 British horror short story of the same title by Edgar Allan Poe. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 119-122  

==Plot==
Edgar Marsh, a shy librarian obsessed with erotica, becomes infatuated with his neighbor Betty Clare when he sees her undressing in her bedroom. He invites her to dinner, and although she clearly is uncomfortable with the attention he pays her, he showers her with jewelry and fantasizes about their future. Complications arise when he introduces her to his friend Carl Loomis, whom Betty finds far more attractive and appealing. In order to eliminate the competition, Edgar bludgeons Carl to death with a poker and buries him beneath the floorboards in his piano room. His overwhelming guilt leads him to believe a ticking metronome and the incessant dripping of a faucet actually are the sound of his victims heart still beating.   

==Cast==
*Laurence Payne ..... Edgar Marsh
*Adrienne Corri ..... Betty Clare
*Dermot Walsh ..... Carl Loomis
==Production==
Around this time the typical budget of the Danzigers feature film was ₤15,000. This cost a little more due to its period setting and necessitated shooting in black and white. 
==References==
 
==External links==
* 

 

 
 
 
 
 
 
 

 