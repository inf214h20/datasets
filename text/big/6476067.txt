Hands over the City
{{Infobox film
| name           = Hands over the City
| image          = Hands over the City.jpg
| caption        = Film poster for Hands over the City
| director       = Francesco Rosi
| producer       = Lionello Santi
| writer         = Francesco Rosi Raffaele La Capria
| narrator       = 
| starring       = Rod Steiger Salvo Randone Guido Alberti Marcello Cannavale|
| music          = Piero Piccioni
| cinematography = Gianni Di Venanzo
| editing        = Mario Serandrei
| distributor    = Warner Bros. Pictures
| released       = September, 1963 (Italy) September 17, 1964 (United States|U.S.)
| runtime        = 105 min.
| country        = Italy France Italian
| budget         = 
}} 1963 drama film directed by Francesco Rosi. It is a story of political corruption in post-World War II Italy. {{cite web|url=http://www.rottentomatoes.com/m/hands_over_the_city/|title=Le mani sulla città (Hands Over the City) (1963)
|publisher=Rotten Tomatoes|accessdate=2014-01-25}} 

== Plot == Neapolitan land developer and elected city councilman, Edoardo Nottola (Rod Steiger), manages to use political power to make personal profit in a large scale suburban real estate deal. However, after the collapse of a residential building, the Communist councilman De Vita (Carlo Fermariello) initiates an inquiry on Nottolas possible connection to the accident.

==Cast==
* Rod Steiger as entrepreneur Edoardo Nottola
* Salvo Randone as De Angeli
* Guido Alberti as Maglione
* Angelo DAlessandro as Balsamo
* Carlo Fermariello as De Vita
* Marcello Cannavale as Nottolas friend
* Alberto Canocchia as Nottolas friend
* Gaetano Grimaldi Filioli as Nottolas friend
* Dante Di Pinto as the president at the commission of enquiry
* Dany Paris as Magliones lover
* Alberto Amato as a counsellor
* Franco Rigamonti as a counsellor
* Terenzio Cordova as the inspector
* Vincenzo Metafora as the trade unionist

== Awards ==
The film won the Golden Lion award of the Venice Film Festival in 1963.

==References==
 

== External links ==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 