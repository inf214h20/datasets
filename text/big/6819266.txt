Mechanic Alludu
 
{{Infobox film
| name           = Mechanic Alludu
| image          = 
| caption        = 
| director       = B. Gopal
| producer       = Allu Aravind
| writer         = Satyanand
| starring       = Chiranjeevi   Vijayashanti  Akkineni Nageswara Rao
| music          = Raj-Koti
| cinematography = 
| editing        = 
| distributor    = 
| country        = India
| language       = Telugu
| released       = May 27, 1993
}}
 Tollywood action film|action-comedy film which is directed by B. Gopal and produced by Allu Aravind. This film stars Chiranjeevi, Vijayashanti, and Akkineni Nageswara Rao.

== Plot ==
Story opens with Chiru showingup on multiple TV screens as an announcer. After a clash, he is fired from work, and he joins ANRs garage as a mechanic. Here, he tries every possible trick up his sleeve to tease his bosss daughter(Vijayasanthi) and she too tries the same. After few comic scenes also involving brahmanandam, both fall in love . ANR realizing that chiru is his enemys son, rejects their love. Chiru manages to persuade ANR to take revenge on his father(Satyanarayana) by acting as his son and entering into satyanarayanas house. Both Chiru and Vijayasanthi confuse everyone there about their relationship, as both enter the house as siblings, but behave as lovers. After few songs and a fight in the climax, everyone unites and thus ends the movie.

==Cast==
* Chiranjeevi ....  Ravi
* Akkineni Nageshwara Rao....  Jagannatham
* Vijayashanti .... Chitti 
* Kaikala Satyanarayana ....  Narayana
* Allu Ramalingaiah   Sharada  ....  Lakshmi
* Kota Srinivasa Rao ....  Kotappa Annapoorna ....  Lakshmi Shubha ....  Parvathi
* Brahmanandam  
* Sudhakar
* Narayan Rao  
* Ali  
* Prasad Babu

==Crew==
* Director: B. Gopal
* Writers: Satyanand
* Producer: Allu Aravind
* Music: Raj-Koti
* Lyrics: Veturi Sundararama Murthy and Bhuvana Chandra Nagur Babu
* Choreography: Tara & Prabhu Deva

==Production & other companies==
* Production Company: Geetha Arts
* VCD Release by: Sri Balaji Videos

==Songs==
* "Chekka Chekka"
* "Guntalakkidi Gundammo"
* "Guruva Guruva"
* "Jhummane"
* "Premiste Pranamistha"
* "Amba Palikindi"

==Box office & Trivia==
* The movie Proved to be a Hit at the box office.
* First film from Allu Aravind to have under performed.
* Spent 2.6 million rupees on the song "Jhummane" which was a lot of money those days.
* First film to have Akkineni Nageshwara Rao and Chiranjeevi together.

==External links==
*  

 
 
 