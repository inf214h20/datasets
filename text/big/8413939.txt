Aunt Rose
{{Infobox film
| name           = Aunt Rose
| image          = Aunt Rose.jpg
| caption        = Official promo poster
| director       = James Tucker
| producer       = Joshua Nelson
| writer         = Joshua Nelson
| starring       = Raine Brown Kevin T. Collins Marty Gargle
| music          = Duane Peery
| cinematography = Brian Fass
| editing        = James Tucker
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
}}
Aunt Rose is a 2005 horror film directed by James Tucker.

==Plot==
A spoiled, teenage girl named Debbie is the center of the plot.  As her parents rapidly become sick of Debbies attitude, they discover that she is lesbian.  Complications arise when three criminals seek shelter, but they are unaware of the beast that lurks in the attic. 

==Cast==
* Raine Brown - Toni
* Joshua Nelson	- Johnny
* Elizabeth Cooke - Debbie
* Velocity Chyaldd - Robin
* Kevin T. Collins - Stewie
* Frank Franconeri - Peter
* Christine DAmato - Anna
* Marty Gargle - Aunt Rose

==Release==
The DVD releasing was distributed by Anchor Bay Entertainment in the USA on 12 September 2006. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 