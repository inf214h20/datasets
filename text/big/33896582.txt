Oomai Vizhigal
{{Infobox film
| name           = Oomai Vizhigal
| image          = Oomai Vizhigal.jpg
| image_size     =
| caption        = Poster
| director       = R. Aravindraj
| producer       = Aabhavanan
| writer         = Aabhavanan Chandrasekhar Jaishankar Karthik
| music          = Manoj - Gyan Aabhavanan
| cinematography = A. Ramesh Kumar
| editing        = G. Jayachandran
| distributor    = Thirai Chirpi
| studio         = Thirai Chirpi
| released       = 15 August 1986
| runtime        = 176 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        = 
}}
 1986 Tamil language drama film directed by R. Aravindraj. The film features Vijayakanth, Arun Pandian, Karthik (actor)|Karthik, Chandrasekhar and Jaishankar in lead roles. The film was released on 15 August 1986 and completed 100 days in theaters. The film became a blockbuster and was a profitable venture.   

<!---==Plot==
A group of college girls come in a light blue matador to Chola Picnic Village.They take a room in a hotel at that Picnic Spot. At night they go out the beach,lit a bonfire and enjoy by singing and dancing. After the song gets over,all the girls return to the Hotel. One of them is missing, that is Vasanthi. She comes across a bell hung in a "Pie symbol" based structure. She goes near it and rings it for 3 times. At that time PRK came and looks into her eyes and Vasanthi starts running. At one point of time, PRK gets hold her hair and takes her along with him.

The next scene shows Raja entering into the Chola Picnic Village. He takes some photos. He comes across a "Pie Symbol" based structure hung with a bell. A beldam is found to sit below it.He takes a photo snap of her. Then he sees a father and introduces himself as Assistant Editor in Dhinamurasu Magazine. He enquires by saying that four to five days before a girls body was found floating in the Picnic Villages Beach. He says that police reported the murder to be a suicide,regrading that he requires some clarifications. The father refuses to answer and raja takes a snap of him. That time Lakshmana,the younger brother of PRK arrives there with smoking cigarette in his mouth. Raja moves away from there.

The Chandran is the Editor of the Dhinamurasu Magazine. He and raja wash the photos and Chandran enquires to raja,how he is getting such sensational news.For that raja replies that he has a known person in alleged suspects house and her name is Uma and she is a steno.The alleged suspect is none other than MLA Saanthanathan.Phone rings in the MLAs house and Velu asks her to pick the phone.She picks up the phone and says that a person name PRK wants to talk with MLA.Uma overhears their conversation through her phone.PRK says to Sattanathan that an Editor came to Picnic village to enquire about the girls death that took place last week and MLA says that he would take care of that issue.

The next morning chandran while working at his office receives a phone call from an inspector who states that why does he unnecessarily investigates  the girls case. For that chandran replied him to mind his words. And Vijay, the friend of the Raja also joined in magazine. Raja and Vijay were staying in the Devis house for rent. The next day Raja brings Umas blindfather and leaves in her house. There Devi is found seating.He says to her that in two-three days he wants some clarifications regarding the picnic village. Vijay-Devi and Raja-Uma were fall in love together.
When the birth day of Uma, she was killed by MLA in front of Raja. And the case sent to DSP Deenathayalan for investigation.

Then the scene shifts to a newly married couple,Ramesh and shanthi,coming for honeymoon to chola picnic village.They spend the time merrily,but during that time they are being watched by the beldam.By the time Maamarathu poo song finishes,it becomes night and when the couples are invloved in romance in the hotel room,PRK comes and makes karthik fall down unconscious.Then shanthi escapes from the village,where she is being helped by passerby whose car suddenly stops.When PRK opens the gate of the village and approaches her to kill her the car starts and that passerby admit her at a hospital.He also informs vijay about a girl who was coming undressed from the village and he has admitted her in a hospital.By the time raja arrives there,one of Sattanathans people go there to kill Shanthi. He hide in shanthis room to kill her. Raja comes there in time and he is stabbed by that thug. Raja also kills that thug and brings Shanthi to DSPs house and dies there. Vijay gets disheartened on seeing this.then peter,ramesh,vijay all join hands in order to know the reason why prk does this.after this prk gets everyone and chains them.he also reveals that he loved someone,especially her eyes but she cheated,so he wanted take eyes frm the girls he see,actually beldam is his step mother who gives him info abt girls.atlastdsp gets to knw about this and punishes everyone.ramesh and shanthi rejoin thus making the end of silent eyes.-->

==Cast==
 DSP Deenathayalan
*Arun Pandian as Vijay Chandrasekhar as Raja
*Jaishankar as Chandran Ravichandran as P.R.K Karthik as Ramesh (guest appearance)
*Srividya as Chandrans wife
*Saritha as Sumathi
*Kokila  as Devi, Vijays lover MLA Sattanathan Senthil
*Ilavarasi Ilavarasi as Selvi Uma, Rajas lover
*Kishmuth as Kothandaraman
*Visu as Rathnasabapathy
*Thengai Srinivasan as Devis father
*Sachu as Devis mother Thyagu as Velu
*Kumarimuthu

*Loose Mohan as Samarasam Sasikala as Shanti, Rameshs wife (guest appearance)
*Disco Shanti as an item number as Vasanthi

==Soundtrack==

{{Infobox album |  
  Name        = Oomai Vizhigal |
  Type        = soundtrack |
  Artist      = Manoj - Gyan |
  Cover       = |
  Released    = 1986 |
  Recorded    = 1986 | Feature film soundtrack |
  Length      = 26:27 |
  Label       = |
  Producer    = Manoj - Gyan |  
  Reviews     = |
}}

The film score and the soundtrack were composed by film composer Manoj - Gyan and additional music composed by Aabhavanan. The soundtrack, released in 1986, features 6 tracks with lyrics written by Aabhavanan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration 
|-  1 || Kanmani Nillu || S. N. Surendar, Sasirekha  || 4:46
|- 2 || Maamarathu Poo || S. N. Surendar, Sasirekha || 4:30
|- 3 || Rathiri Nerathu || Sasirekha || 4:35
|- 4 || Tholvi Nilayenna || P. B. Srinivas, Aabhavanan  || 4:07
|- 5 || Nilaimaarum Ulagil || K. J. Yesudas || 4:32
|- 6 || Kudukuduppai || Aabhavanan, S. N. Surendar  || 3:57
|}

==References==
 

==External links==
* 

 
 
 
 