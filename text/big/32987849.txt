The Loyal Rebel
 
{{Infobox film
  | name     = The Loyal Rebel
  | image    = The_Loyal_Rebel.jpg
  | caption  = Poster for film Alfred Rolfe		
  | producer =  Arthur Wright
  | based on = 
  | starring = 
  | music    = 
  | cinematography = 
  | editing  = 
  | studio = Australasian Films
  | distributor = 
  | released = 27 September 1915 
  | runtime  = 5,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Alfred Rolfe set against the background of the Eureka Rebellion.

It is considered a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p. 55 

==Plot==
In 1854, a young farmer, Stanley Gifford, leaves his girlfriend Violet in England and goes to seek his fortune in the goldfields. Stanleys letters to Violet are intercepted by the villainous Pellew Owen. Violets father, Major Howard, is blackmailed by Pellew Owen into giving him Violets hand in marriage after Howard shoots a man in a quarrel over cards.

Pellew tires of Violent and abandons her, so she goes with her father to Ballarat to find Stanley. Her father dies of exhaustion and Violet is kidnapped by Pellew after interrupting a bank robbery. She is recused by police and Pellew is arrested, but set free after he agrees to be a police spy.

The Eureka rebellion takes place in which a miner is killed, Bentley acquitted, and the hotel burnt down. Miners take refuge in the Eureka Stockade and Pellew is killed. Stanley is wounded, but he manages to escape with Violet and they are united.  

==Cast==
*Reynolds Denniston as Stanley Gifford
*Maisie Carte as Violet Howard    Charles Villiers as Pellew Owen
*Percy Walshe was Major Howard
*Leslie Victor as Peter Lalor
*Jena Williams as Mrs Gifford
*Wynn Davies as soldier

==Production== Arthur Wright, won first prize of £30 in a competition held by Australasian Films.   

The movie was filmed on location near Sydney and in the Rushcutters Bay studio. The Bakery Hill scenes were shot at a railway deviation camp outside Sydney. 

The film used some of the original records stored in the Mitchell Library, including the proclamation concerning the imposition of a monthly tax on the miners, the licences which were issued, and the first Australian flag, consisting of a piece of blue hunting and five stars of the Southern Cross.  

Most of the cast came from the stage, including Reynolds Denniston, who was a well-known theatre star.  

==Reception==
Arthur Wright says that director Alfired Rolfe "made a fine job of it, judged on the standard of the day, but though it was a good effort it did not pull big business. The title was against it for one thing." 

===Critical===
A critic from the Sydney Morning Herald said that "while free use is made of a love romance to point a moral and adorn a tale, the most realistic of the scenes are those showing the rising of the miners on the Ballarat goldfield of 1854 and the fierce fight behind the stockade."  The Referee called the film:
 The first historical photo play produced in Sydney, and the result is very creditable. Mr. Arthur Wright... has weaved in a story of love and adventure, and has done the work very well. The play is full of life, and, considering the large number of people who figure in the action simultaneously, the staging is excellent and the acting very satisfactory... One of the most striking features of the film is the faithful presentation of dress, goldfields, and life generally as they were 60 years ago, in the era of the top hat, the crinoline, the Wellington boot, and the Crimean shirt.  

==Short story==
Arthur Wright later published a short story under the same title. However the contents of the story appear to be different from the plot of the book. 

==References==
 

==External links==
*   Trove
*   at National Film and Sound Archive
*  at AustLit
*  at AustLit
*  reprinted in Sunday Times in 1915
 
 

 
 
 
 
 
 
 


 