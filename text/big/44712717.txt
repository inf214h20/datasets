Nagaram Nidrapotunna Vela
{{Infobox film
| name           = Nagaram Nidrapotunna Vela
| image          =
| caption        =
| writer         = Prem Raj  
| producer       = Nandi Srihari
| director       = Prem Raj
| starring       = Jagapati Babu Charmee Kaur
| music          = Yasho Krishna
| cinematography = Lakshmi Narasimhan
| editing        = Marthand K. Venkatesh
| studio         = Gurudeva Creations Pvt.Ltd    
| released       =  
| runtime        = 133 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}

 Nagaram Nidrapotunna Vela  is a 2009 Telugu Philosophical film produced by Nandi Srihari on Gurudeva Creations Pvt.Ltd banner, directed by Prem Raj . Starring Jagapati Babu, Charmee Kaur in lead roles and music was composed by Yasho Krishna. The film recorded as flop at box office.         

==Plot==
Niharika (Charmee Kaur) is a young journalist working in a Telugu News Channel. After being chided by her boss for bringing ‘boring’ news, she starts looking for some controversial material. For this purpose she ventures out alone in the night into Hyderabad. Unknowingly, Niharika has a pen camera, which happens to capture a secret talk of a politician, which if it could come out, will dismantle his status. He sends his men after her. Niharika in the mean time starts looking at Hyderabad’s dark secrets during the night. She also meets Sivarama Prasad (Jagapathi Babu) who is a drunkard with a good heart, and along with him tries to solve the political problem. How things turn completely against her and how she overcomes the difficulties form the rest of the story.

==Cast==
*Jagapati Babu as Sivarama Prasad 
*Charmee Kaur as Niharika
*Ahuti Prasad

==Soundtrack==
{{Infobox album
| Name        = Nagaram Nidrapotunna Vela
| Tagline     = 
| Type        = film
| Artist      = Yasho Krishna
| Cover       = 
| Released    = 2011
| Recorded    = 
| Genre       = Soundtrack
| Length      = 31:58
| Label       = Aditya Music
| Producer    = Yasho Krishna
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Yasho Krishna. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 31:58
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Nataraju Pooja 
| lyrics1 = Suddala Ashok Teja
| extra1  = Shankar Mahadevan
| length1 = 4:42

| title2  = Nidra Pothunnadi
| lyrics2 = Suddala Ashok Teja SP Balu
| length2 = 5:50

| title3  = Samara Simha 
| lyrics3 = Goreti Venkanna
| extra3  = Goreti Venkanna
| length3 = 5:50

| title4  = Ekkadi Dakka 
| lyrics4 = Ananta Sriram
| extra4  = Hariharan (singer)|Hariharan,Harini
| length4 = 5:45

| title5  = Ma Mummy 
| lyrics5 = Bhaskarbhatla 
| extra5  = Ranjith (singer)|Ranjith,Suchitra
| length5 = 5:06

| title6  = Aksharaniki   
| lyrics6 = Suddala Ashok Teja 
| extra6  = Vandemataram Srinivas
| length6 = 4:31
}}
   

==References==
 

 
 


 