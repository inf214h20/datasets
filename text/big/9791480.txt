Let Them Chirp Awhile
{{Infobox film
| name           = Let Them Chirp Awhile
| image          = Let them chirp awhile.jpg
| caption        = Promotional film poster
| director       = Jonathan Blitstein
| writer         = Jonathan Blitstein
| producer       = Jonathan Blitstein Anouk Frosch
| starring       = Justin Rice Brendan Sexton III Laura Breckenridge Zach Galligan Pepper Binkley Charlotte af Geijerstam Anthony Rapp
| music          = Giulio Carmassi
| cinematography = Andrew Shulkind
| editor         = Jonathan Blitstein
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
}} New York during October 2006.  The film was produced by Anouk Frosch and Jonathan Blitstein.  It was photographed by Andrew Shulkind. The release began December 5, 2008 in select theaters.   It received positive reviews in Variety,  the Village Voice and TimeOut NY.  Opening weekend in NYC had a sold out screening on Saturday December 6, 2008 with the cast in attendance for Q+A.

The film had its World Premiere at the 2007 Woodstock Film Festival in Woodstock, NY where it received acclaim from both audiences and critics.  The director of the festival, Meira Blaustein called the film "a hidden gem" and praised Blitsteins "unique eye" during her introduction prior to the first screening,  as the film had been accepted through regular submission.

==Plot==
The basic storyline of the film follows Bobby and Scott, two young men in their late twenties who are struggling to make careers out of their artistic dreams but their competitiveness with each other gets in the way. Bobby is a screenwriter and Scott is a musician but neither of them are productive because they dont believe in themselves and they share incredibly high standards for their work.  Bobby meets an old lover, Deirdre, who is headed to Los Angeles and she begs Bobby to take care of her dog while shes gone.  Bobby agrees but only in exchange for a sexual favor. Later, when Bobby makes the mistake of sharing an idea for the opening of his new screenplay with Scott, Scott tells their mutual friend, Hart, a playwright, and Hart steals the idea, incorporating it in his new play "Death of a Banker", a campy morality tale about the September 11, 2001 attacks set to be performed off-Broadway later that month.  Bobby loses Deirdres dog and tries to figure out a way to get out of the situation.

==Production==
Blitstein, was the youngest independent filmmaker ever to write, direct, produce, and edit a 35mm color feature-length film which was ultimately released in theaters.  He wrapped shooting on October 24, 2006 when he was still twenty-three years old.  After months of thinking and planning his script concept, Blitstein wrote the first draft script in 27 days and proceeded to assemble an indie crew composed mostly of friends he met at NYU film school.  The total projected budget of the film was nearly $600,000, but because of donated 35mm film, equipment, and services from New York City rental houses and friends of Blitstein and Frosch, the film was completed on the last day of shooting with only $99,000.  The film was funded by friends, family members, and Blitsteins own personal savings from his job as a waiter.  Blitstein also "maxed out" more than seven credit cards.  Locations used included Blitsteins East Village apartment, the apartments of his friends, and restaurants and local shops within walking distance.  Blitstein convinced companies like Fender USA and Pabst Blue Ribbon to allow him to show their logos in the film to lend realism instead of using fake logos or covering logos which happens in many low-budget independent films.  Blitstein cast the film by cold-calling agents of actors he was interested in working with.  By convincing the agents assistants to read his script, he got a foot in the door, and agents and actors began to express interest based on the script, even though Blitstein was an unknown filmmaker.  The shoot was 18 days long and most of the film was shot in 1 or 2 takes because neither Blitstein or his production team knew how much film was going to arrive on set each day.  Sometimes shots were done in a single take in order to save time and film.  On the 12th day of the shoot, Blitstein collapsed due to stress and lack of sleep.  He was admitted to the emergency room at Beth Israel Hospital nearby.  Doctors discovered an unrelated non-life-threatening kidney abnormality (Blitstein was born with a single kidney), and Blitstein quickly recovered, returning to set the next morning to complete the shoot.  Blitstein edited the film in 47 days using a 12" mac laptop.

==Theme==
The film builds on the cinematic style of 1970s New Hollywood filmmakers like Hal Ashby, whose "The Last Detail" and "Harold and Maude" begin with slapstick comedy and slowly shift to straight drama as the characters begin to look inward and make self-discoveries, ultimately returning to a blend of comedy-drama at the films denouement. Let Them Chirp Awhile attempts to capitalize on this style while also maintaining a sense of heightened realism in order to lend the film an originality distinguishing it from other indie films of the late 2000s.

Aaron Graham of Uptown Magazine wrote that the film "  much more to the early, sprightly comedies of Brian De Palma (Greetings, Hi, Mom!) than to overplayed Mumblecore".  The film has been repeatedly compared to mumblecore films such as Mutual Appreciation because of star Justin Rice although Blitstein has stated that he is not influenced by or associated with the mumblecore genre. 

The result is a coming of age tale disguised within the indie genre. Because of its comedy-drama blend, it has been compared at talkbacks and forums at film festivals to films by Woody Allen, Wes Anderson, Todd Solondz and Richard Linklater.

"Let Them Chirp Awhile is a mix of realism and romanticism.   Scoring the struggles of todays self-obsessed young people with the vintage sounds of melodramatic orchestral music, the film celebrates and satirizes their hopes, fears and dreams.  The East Village becomes a place where everyday conflicts like having a meaningless idea appropriated by a friend, or a breakup are heightened to meet the characters inflated visions of themselves.  Let Them Chirp Awhile is as much a throwback to old films as it is a look forward into the destiny-anxiety that afflicts all of us in the digital age." 

==Cast==
  (left) and Brendan Sexton III.]]

* Justin Rice — Bobby
* Brendan Sexton III — Scott
* Laura Breckenridge — Dara
* Zach Galligan — Hart Carlton
* Pepper Binkley — Michelle
* Charlotte af Geijerstam — Charlotte
* Amy Chow — Ariel
* Anthony Rapp — Himself

==Screenings==
World Premiere at the 2007 Woodstock Film Festival - October 11, 12, 2007 (Opening Night Screening)
* Fort Lauderdale International Film Festival - October 26, November 1, 2007
* Santa Fe Film Festival - November 28 - December 2, 2007 (Opening Night Screening)
* CINE-WORLD Festival - Florida 2007
* East Lansing Film Festival - March 12–20, 2008
* Filma Madrid Spain - March 28 - April 5 (European Premiere)
* Sonoma Valley Film Festival - 9 -13, 2008
* Atlanta Film Festival - April 10–19, 2008
* Rhode Island International Film Festival (Centerpiece Film) - August 5–10, 2008
* Mar del Plata International Film Film Festival Buenos Aires (South American Premiere) November 5–21, 2008

==Awards==
* Winner - "Best Feature Film" - East Lansing Film Festival 2008
* Finalst - "Independent Spirits Award" - Santa Fe Film Festival 2007
* Finalist - NYUs "Richard Vague Award Foundation" - 2007 

==References==
  
* http://www.cinemavillage.com/chc/cv/show_movie.asp?movieid=1417
* http://www.variety.com/review/VE1117939148.html?categoryid=31&cs=1&query=let+them+chirp+awhile
* http://www.woodstockfilmfestival.com
* http://archive.is/20121210141716/http://www.factory630.com/files/vague_press_release.pdf

==External links==
*  
*  
*  
*   From 12.13.2007
*   From 03.23.07
*   From 11.06.06
*   From 08.31.06
*  

 
 
 
 