Venus and Serena (film)
{{Infobox film
| name           = Venus and Serena
| image          = Venus and Serena Official movie Poster.jpg
| caption        = 
| director       = Maiken Baird Michelle Major
| producer       = Maiken Baird Michelle Major
| writer         = 
| starring       = Venus Williams Serena Williams
| music          = Wyclef Jean
| cinematography = 
| editing        = Sam Pollard
| studio         = 
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $10,981 
}}

Venus and Serena is a documentary film that takes and inside look at Venus Williams and Serena Williams lives and careers. Venus and Serena was directed by Maiken Baird and Michelle Major. It was the official selection at the 2013 Miami International Film Festival, 2012 Toronto Film Festival, 2012 Tribeca Film Festival and 2012 Bermuda Docs Film Festival. Venus and Serena was released by Magnolia Pictures May 10, 2013. 

==Cast==
* Venus Williams as Herself
* Serena Williams as Herself Richard Williams as Himself
* Oracene Price as Herself
* Billy Jean King as Herself

==References==
 
   
 

==External links==
*  
* 

 
 
 
 
 