Football Under Cover
{{Infobox Film
| name           = Football Under Cover
| image          = FootballUnderCover.jpg
| image_size     = 
| caption        = 
| director       = David Assmann Ayat Najafi
| producer       = Helge Albers Roshanak Behesht Nedjad Patrick Merkle
| writer         = Corinna Assmann David Assmann Marlene Assmann Valerie Assmann Ayat Najafi
| narrator       = 
| starring       = 
| music          = Niko Schabel
| cinematography = Niclas Reed Middleton Anne Misselwitz
| editing        = Sylke Rohrlach
| distributor    = 
| released       = February 10, 2008 (Berlin International Film Festival)
| runtime        = 86 mins.
| country        = Germany Iran German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German Marlene football match between Assmans team BSV Aldersimspor and the Iranian national womans team. {{Citation
  | last = Simon
  | first = Alissa
  | author-link = 
  | title = Football Undercover Review Variety
  | date = 2008-04-23
  | url =http://www.variety.com/review/VE1117936870.html?categoryid=31&cs=1&query=football+under+cover
  | accessdate =2008-08-20 }} 

==Plot== 

Football Under Cover is a documentary directed Ayat Najafi and David Assmann about the real soccer game between the Iranian women’s international football (soccer) team and an amateur female soccer team from Berlin, BSV AL-Dersimspor. Marlene, a member of the Berlin team learns about the Iranian female national team through an Iranian friend named Ayat. From this point on, Ayat and Marlene take on the difficult task of organizing a soccer match between the two teams. Due to the strict rules of Iranian government and society, the Iranian women’s team had never played a game against another team before. Therefore, Marlene convinces her team to travel to Iran with her to help change this.
 
Marlene travels to Tehran, Iran to help get things organized along with Ayat and another female member of the Iranian team; however, they run into challenges along the way. These challenges include not being able to find a sponsor, not being able to receive visas, difficulty in finding long-sleeved jerseys and pants for the female athletes to wear in order to abide by the Iranian dress code for women, troubles with cooperation between embassies, lack of willingness to advertise the match, among other difficulties. However, the team from Berlin was able to make it to Iran to play the game, under cover. All female participants had to abide by Iranian rules and stay covered during play. Despite political challenges, cultural differences, and other struggles, the love for soccer brings these two teams together to create a historical event in Iranian history as well as break taboos within their society. 

==Reception==
At the 2008 Berlin International Film Festival, Football Under Cover won the Teddy Award for Best Documentary Film and the Teddy Audience Award. 

==Company Credits==
Production Companies: Flying Moon Filmproduktion, Assmann Filmproduktion, Rundfunk Berlin-Brandenburg (RBB)
Other Companies: Medienboard Berlin-Brandenburg (funding), Sander4 (on-line editing facilities), Studio 1141 (sound post-production)

==Music==
===Songs===
* Night Pt. 1 (Written by Niko Schabel, Performed by Radio Citizen, Courtesy of Ubiquity Records)
* Night Pt. 2 (Written by Niko Schabel, Performed by Radio Citizen, Courtesy of Ubiquity Records
* Mondlicht (Written by Niko Schabel, Performed by Radio Citizen, Courtesy of Ubiquity Records) 
* In Balin (Written by P.R. Kantate / Kraans De Lutin, Performed by P. R. Kantate, Courtesy of EMI Music Publishing Germany)
* Gharibeh (Written by Hassan Shamaizadeh, Performed by Narmila Fathi)
* FIFA Anthem (Written by Lambert / Loew, Courtesy of Kobalt Music)
* Afsoongar (Written and Performed by Arian Band, Courtesy of Taraneh Sharghee Co.)
* Straßen von Teheran (Written and Performed by Nariman Hodjaty and Mohamadreza Mortazavi)
* Nilu (Written and Performed by Nariman Hodjaty and Mohamadreza Mortazavi)
* Gharibeh (Written by Hassan Shamaizadeh, Performed by Cymin Samawatie, Niko Schabel, Julian Waiblinger, Moritz Ecker, Klaus Janek)

==References==
 

==External links==
* 
* 
*  
*  

 
 
 
 
 
 
 
 
 
 


 