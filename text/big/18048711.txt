The Natural History of Parking Lots
{{Infobox Film
| name           = The Natural History of Parking Lots
| image          = Nhplvhs.jpg
| caption        = VHS cover
| director       = Everett Lewis
| producer       = Aziz Ghazal
| writer         = Everett Lewis Charlie Bean Charles Taylor
| music          = Johannes Hammers
| cinematography = Hisham Abed   Roy Unger
| editing        = Everett Lewis
| distributor    = Strand Releasing 1990
| runtime        = 89 minutes
| country        = USA
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    =  1990 United American independent film written and directed by Everett Lewis. It tells the story of two estranged brothers who seek to reconcile their relationship against a background of criminal activity and violence. The film is Lewis feature film debut.

==Plot== Charlie Bean) Charles Taylor) (who insists his sons call him "Sam" and not "Dad") arranges for him to be bailed into the custody of Chriss older brother, Lance (B. Wyatt). The brothers seem to bond, but there is always the suspicion that Lance is merely using his newly-domestic situation as a cover for his real business, gun-running.

==Awards and nominations== Best First Feature. {{cite web
  | title = 1991 Independent Spirit Award nominees
  | publisher = Film Independent
  | url = http://filmindependent.org/spiritawards/past_nominees_filmbyyear.php archiveurl = archivedate = 2008-03-12}} 

==Notes==
 

==External links==
*  
*   at The Internet Movie Database
*   at Rotten Tomatoes

 
 
 
 