Marriage in Trouble
{{Infobox film
| name           = Marriage in Trouble
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald Leo Meyer   Seymour Nebenzal
| writer         = Georges Antequil (novel)   Herbert Juttke   Georg C. Klaren
| narrator       = 
| starring       = Elga Brink   Walter Rilla   Evelyn Holt   Alfred Abel
| music          = 
| editing        = 
| cinematography = Friedl Behn-Grund
| studio         = Nero Film
| distributor    = Vereinigte Star-Film
| released       = 6 December 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Richard Oswald and starring Elga Brink, Walter Rilla and Evelyn Holt. The films art direction was by Franz Schroedter. A man considers leaving his wife for another woman, but eventually decides against it.  The film was based on a French novel by Georges Antequil.

==Cast==
* Elga Brink as Frau
* Walter Rilla as Mann
* Evelyn Holt as Mädchen Hannele Meierzak as Kind
* Alfred Abel as Rechtsanwalt
* Fritz Kampers as Cafétier
* Otto Wallburg
* Willy Rosen
* Trude Berliner
* Elsa Wagner

==References==
 

==Bibliography==
*Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 


 
 