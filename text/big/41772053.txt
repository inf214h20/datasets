Stay More: The World of Donald Harington
  }}
{{Infobox film
| name = Stay More: The World of Donald Harington
| director = Brian Walter
| studio = Brian Walter Productions
| runtime = 95 minutes
| released =  
| country = United States
| language = English

}} Donald Harington, produced by Brian Walter based on interviews that Walter conducted with Harington and his wife in 2006–07.    

==Production==
 

==Synopsis==
In this documentary, Donald Harington defines the background of Stay More, the fictional Ozark village where Harington’s novels are based. Harington shares his childhood memories of the small town of Drakes Creek (the town that was the model for Stay More) and looked back on his uneasy relationships with his parents and the upsetting loss of his hearing at the age of 12. Likewise, Harington reflects the dubious delights of getting his initial novels into print, with their sales on no occasion matching the favorable notices, blending these stories with the humorous perceptions on the writers life (particularly the writerly penchant for liquor, religion, and sex). What came of this is a bittersweet portrait of an inspired but haunted artist, a writer genuinely rooted in American folk traditions whose triumphs seemed always to be matched by deep disappointments, a novelist who not only blends comic relief into his tragedies but poignant tragic relief into his comedies.

==Cast==
 

==Reception==
 

==References==
 

== External links ==
*   at University of Arkansas Press 
*  
*   in Arkansas Times

 
 

 