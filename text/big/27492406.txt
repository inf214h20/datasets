Amok (1944 film)
{{Infobox film

 | name = Amok
 | caption = 
 | director = Antonio Momplet
 | producer = Gonzalo Elvira
 | writer = Stefan Zweig Antonio Momplet
 | narrator = 
 | starring = María Félix Julián Soler Stella Inda
 | music = Manuel Esperón Agustín Lara
 | cinematography = 
 | editing = Jorge Bustos
 | distributor = Clasa Films Mundiales
 | released =  
 | runtime = 137 minutes
 | country = Mexico
 | language = Spanish
 | budget = 
 | preceded_by = 
 | followed_by = 
}}
Amok is a 1944 Mexican romantic drama film directed by Antonio Momplet and starring María Félix and Julián Soler. The film is based on the novel Der Amoklaufer by  author Stefan Zweig.

==Plot==

A doctor (Julián Soler) embezzles the proceeds of his Parisian clinic in order to better support the manipulative woman  (María Félix) with whom he is having an affair. After losing all the money while gambling, he is forced to flee to an undeveloped region of India. There, he tries to mitigate the onslaught of a disease the natives term "Amok", while his past mistakes still plague him.

==Cast==
* María Félix as Mrs. Travis / Mrs. Belmont
* Julián Soler as Dr. Jorge Martell
* Stella Inda as Tara
* Miguel Ángel Ferríz as Governor

==External links==
* 
*  

 
 
 
 
 
 
 
 


 
 