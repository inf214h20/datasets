The Tony Blair Witch Project
The 2000 horror spoof of The Blair Witch Project which also pokes fun at then Prime Minister of the United Kingdom, Tony Blair.

== Synopsis ==
Five British politicians (including Tony Blair himself, performed by director Mike A. Martinez wearing a paper mask) travel to West Virginia searching for a legend called The Tony Blair Witch.  They begin by interviewing the locals and then bumble around a ghost town breaking windows and doors before a gang of local bumpkins kills them off.  The film is presented as a mockumentary much like the film it spoofs. 

==Reception==
The film has been very poorly received by audiences, and holds a rating of 1.8/10 on IMDb  (as voted by users), and is #23 on the websites Bottom 100 films, a list of the worst reviewed films on the site. 

==See also==
* Cultural depictions of Tony Blair

==References==

 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 