Wise Guys (1986 film)
{{Infobox film
| name           = Wise Guys
| image          = Wiseguysposter.jpg
| caption        = Theatrical release poster
| director       = Brian De Palma
| producer       = Aaron Russo
| writer         = George Gallo Norman Steinberg
| starring = {{Plainlist|
* Danny DeVito
* Joe Piscopo
* Harvey Keitel
* Ray Sharkey Captain Lou Albano
}}
| music          = Ira Newborn
| cinematography = Fred Schuler
| editing        = Gerald B. Greenberg
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 100 minutes
| country        = United States English
| budget         = $13 million
| gross          = $8,475,466
}}
 1986 feature film directed by Brian De Palma and starring Danny DeVito and Joe Piscopo. A comedy revolving around two small-time mobsters from Newark, New Jersey, it also features Harvey Keitel, Ray Sharkey, Lou Albano, Dan Hedaya, and Frank Vincent.

==Plot==
Italian American Harry Valentini and his Jewish friend and next-door neighbor Moe Dickstein occupy the bottom rung of Newark Mafia boss Anthony Castelos gang. Making a living by doing Castelos lowest jobs (such as looking after his goldfish, testing out bullet-proof jackets, or checking the bosss car for bombs) the two men dream of opening the worlds first Jewish-Italian delicatessen. However, they get little to no respect from their boss or his subordinates, who frequently ridicule them.

They accompany Frank "The Fixer" Acavano, one of Castelos top men and a violent, heavyset psychopath, to Meadowlands Racetrack to place a bet on Castelos behalf. Valentini changes horses at the last minute because his boss usually bets on the wrong one. However, this time Castelo had fixed the race, meaning that Harry and Moe now owe their boss thousands. After a night of torture, both are forced to agree to kill each other.

Unaware that each has made a deal and frightened following the murder of Harrys cousin Marco, they steal Acavanos Cadillac and travel to Atlantic City to see Harrys uncle Mike, a retired mobster who started Castelo in the crime business. After using Acavanos credit cards to pay for a luxury stay in a hotel owned by their old friend Bobby DiLea, the two go to Uncle Mikes house to ask for help. They find only Uncle Mikes ashes, leading to Moe leaving in disgust. Grandma Valentini, however, is able to give Harry the money he owes.

Harry tries to get DiLea to sort things out with Castelo. As he and Moe leave the hotel, their limo is being driven by Acavano, after DiLea appears to double-cross the two. Harry luckily spies Castelos hitmen and decides to stay behind and gamble the money. After a chase through the hotel casino, Moe catches up to Harry and accidentally shoots him. Harry is pronounced dead and Moe flees.

Back in Newark, Moe hides out of sight at Harrys funeral. He is spotted by the huge Acavano (who is eating a sandwich during the burial service) and Castelo resolves to kill Moe after the service. Moe returns to his house and prepares to hang himself. Before doing so, sees a vision of Harry at the foot of the stairs. He quickly realizes that it is actually Harry, who arranged the whole thing with DiLea. Moe is thrilled, although he is so shocked that he is almost hanged anyway until Harry intervenes.

Harry provides a skeleton for Moe and they write a suicide note before turning on the gas and setting fire to the curtains. As the two leave Moes house, however, the door slams shut and puts the fire out. Castelo and his men enter to find a bizarre scene. Castelo takes out a cigarette, prompting his stooges to routinely spark their lighters for him. Acavano asks "Who farted?", prompting Castelo to realize the house is filled with gas just before the house explodes, with the crew inside it.

Harry and Moe return to Atlantic City, where Moe bemoans the fact that they didnt keep the money. Harry informs him that he did save the money, but has invested it. Moe seems perturbed, but the film ends with their dream realized as the two stand in their Jewish-Italian delicatessen.

==Reception==
The film received mixed reviews from critics. A positive review came from The New York Times, with Walter Goodman calling it amusing and fresh before concluding that "Everything works."  Roger Ebert was similarly enthusiastic, writing "Wise Guys is an abundant movie, filled with ideas and gags and great characters. It never runs dry." 

As of October 2010, the film has a 33% "Rotten" rating on Rotten Tomatoes. 

==Cast==
 
*Danny DeVito as Harry Valentini
*Joe Piscopo as Moe Dickstein
*Harvey Keitel as Bobby DiLea
*Ray Sharkey as Marco
*Dan Hedaya as Anthony Castelo
*Lou Albano as Frank Acavano
*Julie Bovasso as Lil Dickstein
*Patti LuPone as Wanda Valentini
*Antonia Rey as Aunt Sadie
*Mimi Cecchini as Grandma Valentini
*Matthew Kaye as Harry Valentini Jr.
*Tony Munafo as Santo Ravallo
*Tony Rizzoli as Joey Siclione
*Frank Vincent as Louie Fontucci
*Rick Petrucelli as Al Anthony Holland as Karl
*Marcelino Rivera as Bellhop
*Joseph Cipriano as Parking Valet
*Julius Cristinzio as Roulette Operator
*Dan Resin as Maitre D
*Alessandro Falcini as Priest
*Jill Larson as Mrs Fixer
*Maria Pitillo as Masseuse
*Christine Poor as Masseuse
*Stephanie Quinn as Masseuse
*Cecilia I. Battaglini as Luggage Salesperson
*Frank D. Formica as Pit Boss
*Deborah Groen as Roulette Dealer
*Bradley Neilson as Clothing Salesman
*Maryellen Nugent as Jewellery Saleswoman
*Frank Ferrara as Thug
*Gaetano Lisi as Hood
*Vince Pacimeo as Hood Henry Stewart as Tailor
*Carol Cass as Birthday Guest
*Mary Engel as Birthday Guest
*Bruce Katzman as Birthday Guest
*Dayna Lee as Birthday Guest
*Louisiana as Birthday Guest
*Myles OConnor as Birthday Guest
*Don R. Richardson as Birthday Guest
*Johnny George Sarno as Birthday Guest
*Reuben Schafer as Birthday Guest
*Catherine Scorsese as Birthday Guest
*Charles Scorsese as Birthday Guest
*Gary Cookson as Race Track Bettor
*Kiya Ann Joyce as Race Track Bettor
*Willow Hale as Race Track Bettor
*Bob OConnell as Race Track Bettor
*Joe Schmieg as Race Track Bettor
*Richardson Taylor as Race Track Bettor
*Larry Guardino as Race Track Bettor
*Kim Delgado as FBI Agent
 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 