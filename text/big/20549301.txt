Paramesha Panwala
{{Infobox Film
| name = Paramesha Panwala
| image = Paramesha Panwala.jpg
| caption = movie poster
| director = Mahesh Babu
| story = Janardhana Maharshi
| screenplay = Mahesh Babu
| producer = Aditya Babu Sonu
| music = V Harikrishna
| cinematography = Venus Moorthy
| editing = 
| released =  
| runtime =  
| language = Kannada  
| country = India
}}
 Kannada language film directed by Mahesh Babu, and produced by Aditya Babu, whilst the music was composed by V. Harikrishna. The film stars Shivrajkumar and Surveen Chawla in the lead roles. It was released nationwide on 5 December 2008.  

==Cast==

*Shivrajkumar as Paramesha
*Surveen Chawla as  Sonu
* Srinivasamurthy
* Ashish Vidyarthi
* Chitra Shenoy
* Akul Balaji
* Rekha 
* Sadhu Kokila 
* Sharan

==Reception==
This movie also suffered the same fate as of other recent movies by Shivaraj Kumar. Even though the film was hyped as usual, it did not fare well in box office. 

==Music==
The film has five songs composed by V. Harikrishna with the lyrics penned by V. Nagendra Prasad and Kaviraj.The audio of the film released on 10 November 2008, at Green House, Bangalore. Album was released by Raghavendra Rajkumar and journalist P.G. Srinivasamurthy.  

{{Infobox album |   Name        = Paramesha Panwala Type        = Album Artist      = V. Harikrishna Cover       = PP Audio.jpg Released    = 10 November 2008 Recorded    =  Genre  Feature film soundtrack Length      = 24:53 Label       = Ashwini Audio Producer    = Aditya Babu Reviews     =  Last album  =  This album  = Paramesha Panwala (2008)  Next album  = 
}}

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Singers || Length (m:ss) ||Lyrics ||Notes
|-
| 1 || Dil Dhadak Dhadak || Sonu Nigam, Anuradha Bhat || 5:14 || V. Nagendra Prasad ||
|-
| 2 || Saavira Saavira Janma || Kunal Ganjawala, Shravya || 4:25 || Kaviraj ||
|-
| 3 || Sum Sumke || S. P. Balasubrahmanyam, Shamita Malnad || 4:54 || V. Nagendra Prasad ||
|-
| 4 || Madaraasu Aisa || Shankar Mahadevan || 5:38 || V. Nagendra Prasad ||
|-
| 5 || Tuntaraama Puttaraama || Udit Narayan, K. S. Chithra || 4:42 || V. Nagendra Prasad ||
|}

==References==
 

==External links==
* 
* 

 
 
 
 


 