Sagla Karun Bhagle
 

{{Infobox film
| name           = Sagla Karun Bhagle - Marathi Movie
| image          = Sagla Karun Bhagle.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Vijay Patkar
| producer       = Saurabh Kulkarni
| screenplay     = 
| story          = 
| starring       = Makrand Anaspure Deepali Saiyyed Sweta Mehendale Madhavi Nimkar Chetan Dalvi 
| music          = Nitin Hivarkar
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Sagla Karun Bhagle is a Marathi movie released on 2 December 2011.  Produced by Saurabh Kulkarni and directed by Vijay Patkar. The movie is laughter riot based on alcoholism and its addiction.

== Synopsis ==
In Bhokarwadi a young man, ‘Arjun’ (Makarand Anaspure) starts making efforts to eradicate alcoholism from his village but uses different methods to achieve success in this noble thought.

== Cast ==

The cast includes the ace comedian actor Makrand Anaspure, with Deepali Saiyyed, Sweta Mehendale, Madhavi Nimkar, Chetan Dalvi & Others

==Soundtrack==
The music is provided by Nitin Hivarkar.

== References ==
 
 

== External links ==
*  
*  

 
 
 


 