What the Daisy Said
{{Infobox film
| name           = What the Daisy Said
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Stanner E.V. Taylor
| starring       = Clara T. Bracy
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 12 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}

What the Daisy Said is a one-reel film (about 12 minutes) made by D. W. Griffith for Biograph in 1910.   

==Plot== palm read by a woman fortuneteller.  There, a mustachioed gypsy catches her eye, and he tells her a fortune in which he "plans her future to his liking".  The pair run off together, crossing a brook into which he saves her from falling.  They arrive at a waterfall where he "induces her to believe his prophecy must be true".  After that brief exchange, Martha jubilantly skips home, passing the lanky farmhand who pays her no heed.

Arriving at the homestead, she immediately resolves to return to the waterfall.  At their second meeting, the gypsy greets Martha with open arms and after some animated entreaties she meets his embrace.  In a brief Cutaway (filmmaking)|cutaway, Millie looks for Martha at the orchard, but cant find her.  Martha reluctantly bids the gypsy adieu—after she is gone, he laughs and struts confidently.  Marthas stealthy return escapes the notice of Father and Millie who are sitting on the porch of the homestead.  They seem to accept the explanation of her absence when she motions to the second story of the house.  Martha then talks Millie into getting her own fortune read and they slip away, hand in hand, over a rough-hewn fence, through the field of daisies, and into town.  While the fortuneteller is examining Millies palm, Martha is distracted by one of the townswomen.  The crafty gypsy then swoops in and charms Millie into accompanying him to the romantic waterfall.  When Martha returns to the fortunetellers tent, Millie is nowhere to be found.

Millie, in the meantime, has bounced home and back to the waterfall, where the gypsy bids her to sit on a conveniently-placed bench.  Martha wanders back to the waterfall (via the orchard), where she is horrified to find the gypsy with his arm around Millie and observes them long enough to witness him kiss Millie on the cheek.  Heartbroken, Martha returns to the orchard to weep.  To complicate things, Father departs the homestead and his route takes him by the waterfall where he discovers his daughter in close personal contact with the gypsy.  In a fit of rage, the old man tears the gypsy from his daughter and reproves him wildly.  Father raises his cane to strike the gypsy, but the gypsy impulsively fells him with a two quick blows to the torso.  Aghast at what hes done, the gypsy escapes just as two passersby arrive on the scene.

The gypsy flees through the field of daisies as a growing mob of farmhands set out looking for him.  After passing through an overgrown field and over the brook, the gypsy arrives at the orchard.  Martha, unaware of his wrongdoing, yields to his pleading and successfully conceals him in a barrel underneath potatoes she empties out of a bushel basket.  After the farmhand posse passes, and just as Father is recovering with the help of Millie and a passerby at the waterfall, the gypsy emerges from the barrel and bids the jilted Martha a hasty and perfunctory farewell.  The posse eventually catch up with the gypsy back at the fortunetellers wagon, where they warn him in no uncertain terms to leave town, which he does with a bindle over his shoulder.  The dejected Martha, sitting on some wooden steps leading up from the road, looks up to find the posse marching the gypsy out of town.  She wanders off to the vegetable patch where she finds solace in the arms of the lanky farmhand she had rejected earlier.  The film concludes in the field of daisies where Millie abandons another round of petal plucking to walk off arm-in-arm with a strapping farmhand who appears out of the blue.

==Cast==
* Clara T. Bracy - A Gypsy (uncredited)
* Kate Bruce - The Spinster (uncredited)
* Verner Clarges - The Father (uncredited) John T. Dillon - A Farmhand (uncredited)
* Frank Evans - A Farmhand (uncredited)
* Francis J. Grandon - A Farmer (uncredited)
* Joseph Graybill - The Gypsy (uncredited)
* Owen Moore - (uncredited) George Nichols - (uncredited)
* Anthony OSullivan - A Gypsy/A Farmhand (uncredited)
* Alfred Paget - A Farmhand (uncredited)
* Mary Pickford - Martha (uncredited)
* Gertrude Robinson - Milly (uncredited)
* Mack Sennett - (uncredited)
* Charles West - A Farmer (uncredited)

==See also==
* 1910 in film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 