A Valentine for You
{{multiple issues|
 
 
}}

{{Infobox television film
| name           = Winnie the Pooh: A Valentine for You
| image          = Winnie the Pooh-A Valentine For You.jpg
| caption        = 
| director       = Keith Ingham
| writer         = Carter Crocker
| producer       = The Walt Disney Company David Warner Michael Gough Andre Stojka Carl Johnson Walt Disney Television Animation Buena Vista Television
| released       =  
| runtime        = 30 minutes
| country        = United States
| language       = English
}} special based Disney television series The New Adventures of Winnie the Pooh, originally broadcast on February 13, 1999. This is the final role of Paul Winchell as Tigger before his retirement from the role in the same year and his natural causes-related death in 2005. It was released on DVD in 2004 and 2010.

==Cast==
* Jim Cummings as Winnie the Pooh and Tigger (singing)
* Paul Winchell as the final role of Tigger Piglet
* Peter Cullen as Eeyore
* Brady Bluhm as Christopher Robin
**Frankie J. Galasso as Christopher Robin (singing voice) Rabbit
* Michael Gough Gopher
* Owl
* David Warner

== Notes ==
* Kanga and Roo do not appear in this show but they only show up as toy animals.

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 