Little Laura and Big John
{{Infobox film
| name           = 
| image          = "Little_Laura_and_Big_John"_(1973).jpg
| image_size     =
| alt            =
| caption        = 
| director       = Luke Moberly Bob Woodburn
| producer       = Lou Wiethe
| writer         = Luke Moberly Bob Woodburn based on = story by Phillip Weilding AUTHOR REMEMBERS SMALL-TOWN FLAVOR OF FORT LAUDERDALE:  
MacENULTY, PAT. Sun Sentinel   04 Apr 1990: 9. 
| narrator       = Fabian Forte Karen Black Paul Gleason Bill Walker
| cinematography = H. Edmund Gibson
| editing        = Tom Woodburn
| studio         = Louis Wiethe Productions
| distributor    = Crown International
| released       = 11 May 1973
| runtime        = 82 mins
| country        = United States
| language       = English
| budget         = $200,000   accessed 5 July 2012 
| gross          = 
| preceded_by    =
| followed_by    =
}} Ashley gang in the Florida everglades in the 1910s and 1920s. 

==Plot==
Lauras mother tells the story of her daughter and John Ashley. John goes into a life of crime after he accidentally shoots an Indian.

==Cast== Fabian Forte John Ashley
*Karen Black as Laura
*Ivy Thayer as Lauras mother
*Ken Miller as Hanford Mobley
*Paul Gleason as Sheiff Bob Baker
*Cliff Frates as Young John
*Ivy Thayer as Lauras mother
*Jerry Rhodes as Bob Ashley
*Ray Barrett as Cates

==Production==
Plans to make the film were announced in 1968 by Luke Moberly, who owned a studio near Fort Lauderdale.  Costumes were borrowed from the Martin County Historical Society. By February 1969 it was announced that Fabian and Karen Black were to play the leads. Filming began 10 March 1969 on location in Stuart, Florida, in and around Martin County, and at Moberly Film Studios in Fort Lauderdale.  The budget had been raised by Lou Wiethe. 

Additional footage was later shot, including filming a nude scene at the public beach at the House of Refuge in Martin County. 

The original title of the film was The True Story of the Ashley-Mobley Gang. Then it was Too Soon to Laugh, Too Late to Cry before becoming Little Laura and Big John. The film scene
The Christian Science Monitor (1908-Current file)   21 Apr 1969: 4. 

==Release==
Crown International bought the rights to the movie which was not released until 1973. MOVIE CALL SHEET: Rights Acquired to Novels
Murphy, Mary. Los Angeles Times (1923-Current File)   09 Oct 1972: d15. 

The film marked the first time Fabian was credited as "Fabian Forte." 

==References==
 

==External links==
*  at IMDB
*  at Fabianforte.net
*  at TCMDB
*  at New York Times BFI

 
 
 
 
 
 
 


 