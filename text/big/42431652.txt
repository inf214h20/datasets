Roswolsky's Mistress
{{Infobox film
| name           = Roswolskys Mistress
| image          =
| caption        =
| director       = Felix Basch 
| producer       = 
| writer         = George Froeschel (novel)   Henrik Galeen   Hans Janowitz
| starring       = Asta Nielsen   Paul Wegener   Wilhelm Diegelmann   Ferdinand von Alten
| music          = Bruno Schulz 
| cinematography = Carl Drews   Adolf Lieberenz
| editing        = 
| studio         = Messter Film  UFA
| released       = 2 September 1921 
| runtime        = 
| country        = Germany
| awards         =
| language       = Silent   German intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by Felix Basch and starring Asta Nielsen, Paul Wegener and Wilhelm Diegelmann. It was based on a novel by George Froeschel. The film was shot at the Tempelhof Studios in Berlin, with sets designed by art directors Robert Neppach and Jack Winter. According to one estimate, the star Asta Nielsen wore thirty six different costumes during the course of the film. 

==Synopsis==
A working class girl is mistakenly believed to have become the mistress of a billionaire.

==Cast==
* Asta Nielsen as Mary Verhag  
* Paul Wegener as Eugen Roswolsky  
* Wilhelm Diegelmann as Sekretaer  
* Ferdinand von Alten as Lico Mussafin  
* Marga von Kierska as Fernande Raway  
* Guido Herzfeld as Flügelmann, Geldverleiher  
* Arnold Korff as Untersuchungsrichter 
* Carl Bayer as Juwelier  
* Adolphe Engers as Jean Meyer  
* Ernst Gronau as Layton  
* Max Landa as Baron Albich  
* Adolf E. Licho as Theaterdirektor  
* Maria Peterson as Zimmervermieterin 
* Emil Rameau as Kapellmeister  
* Gertrud Wolle as Martha Verhag, Schwester von Mary

== References ==
 

==Bibliography==
* Ganeva, Mila. Women in Weimar Fashion: Discourses and Displays in German Culture, 1918-1933. Camden House, 2008.
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

== External links ==
*  

 
 
 
 
 
 
 
 


 
 