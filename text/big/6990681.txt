Ariel (film)
{{Infobox film
| name = Ariel
| image = Ariel poster.jpg
| director = Aki Kaurismäki
| producer = Aki Kaurismäki
| writer = Aki Kaurismäki
| starring = Turo Pajala Susanna Haavisto Matti Pellonpää
| music = Esko Rahkonen Rauli Somerjoki Taisto Tammi
| cinematography = Timo Salminen
| editing = Raija Talvio
| released       =   
| runtime = 74 minutes   
| country = Finland
| language = Finnish
|}}

Ariel is a 1988 Finnish drama film directed and written by Aki Kaurismäki. The film tells the story of Taisto Kasurinen (Turo Pajala), a Finnish coal miner who must find a way to live in the big city after the mine closes and his father, also a miner, commits suicide.

Taistos friend is played by Matti Pellonpää, an actor who appeared in many of Kaurismäkis early films.
 Eclipse box-sets.  The film is included in the 1001 Movies You Must See Before You Die list. The film was entered into the 16th Moscow International Film Festival where Turo Pajala won the Bronze St. George for Best Actor.   

==Cast==
* Turo Pajala as Taisto Kasurinen
* Susanna Haavisto as Irmeli Pihlaja
* Matti Pellonpää as Mikkonen
* Eetu Hilkamo as Riku
* Erkki Pajala as Kaivosmies
* Matti Jaaranen as Pahoinpitelijä
* Hannu Viholainen as Apuri
* Jorma Markkula as Prikkamies
* Tarja Keinänen as Nainen Satamassa
* Eino Kuusela as Mies rannalla

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 
 