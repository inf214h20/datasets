The Twins' Tea Party
{{Infobox film
| name           = The Twins Tea Party
| image          = TheTwinsTeaParty.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = Robert W. Paul
| producer       = Robert W. Paul
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 20 secs
| country        = United Kingdom Silent
| budget         =
}}
 1896 UK|British short  silent actuality film, produced and directed by Robert W. Paul, featuring twin girls squabbling over a piece of cake at a tea party. The film, "was one of the very first facials," which according to Michael Brooke of BFI Screenonline was, "a popular genre in early British cinema that exploited what to 1896 audiences was the astonishing novelty of being able to see moving images of recognisable people in medium close-up as they reacted to a particular situation." John Barnes, author of The Beginnings of the Cinema in England, adds that, "this charming one-shot film of two infant girls reluctantly sharing tea was one of the most popular items exhibited in R.W. Pauls programmes at the Alhambra Theatre in 1896." It was remade in 1898.   

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 