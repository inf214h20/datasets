The Snow Queen's Revenge
{{Infobox film
| name     = The Snow Queens Revenge
| image    = The Snow Queen 2DVD.jpg
| caption  = Czech home release cover art
| director = Martin Gates
| producer = Martin Gates
| writer   = Sue Radley Martin Gates
| starring = Julia McKenzie Ellie Beaven Hugh Laurie
| composer = Toby Allington
| released =  
| runtime  = 65 minutes
| studio   = Martin Gates Productions Carrington Productions
| country  = United Kingdom
| language = English
}}
 The Snow Queen and has some of the voice cast changed, including Julia McKenzie cast in the titular role of the Snow Queen. Vanquished in the first film, the evil Snow Queen returns to life, setting out to seek revenge on those who ruined her plans to freeze and rule the world and it is up to young Ellie and her best friends to stop her again.

==Plot== the previous film left off. After the evil Snow Queen was defeated and frozen solid, Dimly the flying reindeer arrives back at the village with Ellie, her brother Tom, and Peeps the sparrow. By now it is almost spring. Back at the Queens palace, her now uncontrolled three trolls, Eric, Baggy and Wardrobe, prepare for Dimly to take Freda back to the flying school and return for the trolls. As the trolls try to figure out where to go, the Snow Queens bats take her magic staff and place it in her hand, setting her free moments after Dimly returns. The furious Queen decides to kidnap Dimly so Ellie will come to her and she can get her revenge. She moves with Dimly and the trolls to the South Pole, as it is now too warm in the North Pole. 

The Snow Queen contacts Ellie, telling her that she has captured Dimly and daring her to rescue him. Ellie does not know where the Queen is now, so Peeps takes her to Brenda, a bird that is said to know everything. Meanwhile, the Queen reaches her palace set on a frozen volcano and locks Dimly away in a cell next to her ferocious reindeer, which attempt to break into his cell and eat him. Brenda takes Ellie and Peeps toward the South Pole. They stop at a restaurant for food, where the proprietor (a greedy humanoid pig) and her minions capture Brenda and try to cook her to serve as food. Ellie and Peeps stop them and escape, destroying the restaurant.

The Snow Queen begins work on the final part of her plan, creating the Iceosaurus, an ice pterodactyl. Meanwhile, Ellie falls asleep and she and Peeps fall off Brenda and into the ocean, where they are picked up by a humanoid walrus, Clive and his partner Rowena, on a ship named the S.S. Quagmire. When Ellie and Peeps describe how they are on their way to the Queens palace, they realize that she set the whole thing up to lure Ellie there. Clive and Rowena are revealed to be bounty hunters who plan to give Ellie to the Queen for a big reward, and imprison her and Peeps. When Brenda realizes that they have fallen off, she comes back and rescues them.

Brenda, Ellie, and Peeps arrive at the South Pole, but the Iceosaurus freezes Brenda. Ellie and Peeps discover a magic talisman that turns into a salt shaker which Ellie uses to unfreeze Brenda. Brenda separates from Ellie to find a high place to take off. Ellie and Peeps encounter Pearl and Elspeth, two humanoid penguins who clean the palace for the Snow Queen, and Ellie traps them inside a bubble using the device. They find Dimly and release him from his cell by turning the device into a key, and locking the Queens reindeer inside. The group attempts to escape, but Dimly is too weak to fly at the moment. The Queen and the Iceosaurus attack them, but Fredas device turns into a kind of shield that Ellie uses to deflect the ice beams the Queen shoots at her, causing one to hit the Iceosaurus. It falls and crashes into the ground, creating a massive volcanic eruption. Brenda escapes the flood of lava and gets Ellie, Peeps, and Dimly to safety. The panicked Snow Queen attempts to flee on feet, but is unable to escape her crumbling palace and falls down into the lava.

As Brenda takes Ellie, Peeps, and Dimly back home, Eric, Baggy, Wardrobe, Pearl, and Elspeth watch as the Snow Queens castle is destroyed, and walk off once the eruption is over. The final scene shows the Queen drifting through the river of molten magma, her body semingly intact but now turned to stone, and still holding her magic staff. Her eyes glow onimously before the credits roll.

==Cast==
*Julia McKenzie as the Snow Queen and Freda
*Ellie Beaven as Ellie 
*Gary Martin as Dimly
*Hugh Laurie as Peeps
*Elizabeth Spriggs as Brenda Tim Healy as Eric
*Colin Marsh as Baggy
*Russell Floyd as Wardrobe
*Patrick Barlow as Clive
*Imelda Staunton as Rowena and Elspeth
*Alison Steadman as Pearl

==Release== Warner Brothers Feature Animation in 1998.

==See also==
*A Snow White Christmas

==Sources==
* 
* 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 