Escapement (film)
{{Infobox film
| name           = Escapement
| image          = "Escapement"_(1958).jpg
| image_size     = 
| caption        = U.S. lobby card
| director       = Montgomery Tully Richard Gordon
| writer         =  based on        = novel Escapement by Charles Eric Maine
| narrator       =  Rod Cameron Mary Murphy Meredith Edwards
| music          = 
| cinematography = 
| editing        = 
| studio         = Amalgamated Productions
| distributor    = Anglo-Amalgamated (UK)
| released       = 1958
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| website        = 
| amg_id         = 
}}
Escapement is a 1958 black and white British science fiction film, known in the U.S. as The Electronic Monster.  It was based on the novel Escapement by Charles Eric Maine (London, 1956).  

==Production==
Producer Richard Gordon later said there were major problems with the films special effects. He also said that he had a dispute with Anglo Amalgamated, who did not want the movie to get an X Certificate in England whereas Gordon wanted more horror for the US. Tom Weaver, The Horror Hits of Richard Gordon, Bear Manor Media 2011 p 19 

==Plot==
Inquiring into the mysterious death of a Hollywood star, insurance investigator Jeff Keenan uncovers an exclusive psychiatric clinic on the French Riviera. Here, patients who want to escape the stresses of life are hypnotized, then laid out in morgue like drawers and left to dream for several weeks. It turns out that Dr. Zakon, the clinics ex-Nazi owner, is using  a "dream machine" to alter the sleepers dreams, and to impose his will on theirs.

==Cast== Rod Cameron Mary Murphy Meredith Edwards
*Paul Zakon - 	Peter Illing
*Doctor Hoff - 	Carl Jaffe
*Laura Maxwell - 	Kay Callard
*Blore - 	Carl Duering
*Verna Berteaux - 	Roberta Huby
*Commissaire - 	Felix Felton
*Brad Somers - 	 Larry Cross
*Signore Kallini - 	 Carlo Borelli
*Claude Denver - 	 John McCarthy
*French Doctor - 	 Jacques Cey
*French Farmer - 	 Armande Guinle
*Receptionist (clinic) - 	 Malou Pantera
*Receptionist (studios) - 	 Pat Clavin
*Wayne - 	 Alan Gifford

==Critical reception==
Leonard Maltin called it a "blah sci-fi programmer" ;   while TV Guide noted, "an intriguing feature in that it was among the first to examine the possibilities of psychological manipulation and brainwashing."  

==References==
 

==External links==
*  at IMDB
*  at TCMDB BFI

 

 
 