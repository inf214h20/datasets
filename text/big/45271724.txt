Patterns of Evidence
 
 Exodus of the Hebrew slaves from Egypt as depicted in the Book of Exodus.    It is directed by Tim Mahoney. 

==Content==
The film describes numerous findings, including possible archaeological findings of evidence of Hebrew habitation in ancient Egypt, and new historical findings as to the timeline of the events of Exodus, as well as contemporary events in ancient Egypt. It also describes some existing artifacts and documents from Ancient Egypt which have been the subject of debate amongst experts for a number of years.     
 New Chronology developed by Rohl.  Their evidence is arranged in chronological order, which they claim matches the biblical and Assyriology chronology.    , by Steve Lipman,. Interview with Galit Dayan, 3/31/15.   

The film features interviews with archaeologists, historians, and Biblical scholars; these experts provide commentary on some of the archaeological findings depicted in the film. They also provide commentary on existing findings, providing some analysis to discuss how those findings might provide the authenticity of the Biblical narrative of the Exodus.

One significant theory presented in the film is that the current predominant analysis that asserts that the Exodus occurred around 1250 BC is incorrect. The film presents numerous pieces of evidence which claim that the era for the Exodus can be set at around 1450 BC, based on both Biblical sources. The film presents numerous items of archaeological significance to offer corroboration for this hypothesis. This would place these events approximately around the era of the reign of Amenhotep II.

The last half hour featured a panel of non-experts in biblical or historical studies who attempted to evaluate the argument.

==Reception and review==

The film has prompted discussion by various reviewers and commentators on some of the historical topics and hypotheses which are presented in the film.   

==References==
 

==External links==
*  
*   

==See also==
* Book of Exodus

 
 