La Reine Margot (1994 film)
 
 
{{Infobox film
| name           = La Reine Margot
| image          = Reine_margot1.jpg
| caption        = Theatrical release poster
| director       = Patrice Chéreau
| producer       = Claude Berri
| screenplay     = Danièle Thompson Patrice Chéreau
| story          = Alexandre Dumas, père
| starring       = Isabelle Adjani Daniel Auteuil Virna Lisi Vincent Pérez
| music          = Goran Bregović
| cinematography = Philippe Rousselot
| editing        = François Gédigier Hélène Viard
| distributor    = AMLF
| released       =  
| runtime        = 162 minutes
| country        = France
| language       = French Italian
| budget         = DEM 42,000,000
| gross          = $2,017,346 (US)
}} La Reine Margot by Alexandre Dumas. It stars Isabelle Adjani, Daniel Auteuil, Virna Lisi and Vincent Pérez. An abridged version of the film was released as Queen Margot in North America, and in the United Kingdom under its original French title.

The film was a box-office success, grossing $2,017,346 in the United States when given limited theatrical release as well as in other countries such as Germany where it gained 260,000 admissions and Argentina where it received 530,800.  , IMDb.  The film also had a total of 2,002,915 admissions in France.   at JPs Box-Office. 
 Jury Prize Best Actress Award at the 1994 Cannes Film Festival, as well as five César Awards. It was later shown as part of the Cannes Classics section of the 2013 Cannes Film Festival. 

==Plot== King Charles Margot (Isabelle Henri de King of La Môle Henry III.

==Cast==
* Isabelle Adjani as Margaret of Valois, "Queen Margot" Henry IV Charles IX La Môle
* Virna Lisi as Catherine de Medici
* Dominique Blanc as Henriette de Nevers Henry III
* Miguel Bosé as Henry I, Duke of Guise
* Asia Argento as Charlotte de Sauve Admiral de Coligny Duke of Alençon
* Thomas Kretschmann as Nançay
* Otto Tausig as Mendès
* Hélène de Fougerolles as a courtesan

==Production==
The film was an international coproduction made by several companies based in France, Germany, and Italy, with the additional participation of StudioCanal and the American company Miramax and the support of Eurimages.
Among the locations were the Mafra Palace in Portugal, the Basilica at Saint-Quentin, Aisne, and the Château de Maulnes, Cruzy-le-Châtel in France.

==Alternative versions and marketing==
 
The films original running time was 161 minutes in its premiere at the 1994 Cannes Film Festival and in its French theatrical release. However, its American distributor, Miramax, asked the director to re-edit the movie to 145 minutes, and this version was the version seen in cinemas outside France and later on video. The full-length version was available for a limited period in the United Kingdom on VHS in a collectors edition box set in 1995, but all further releases, including the DVD, have used the 145 minute cut.

The re-edited version not only removed scenes, it also added a scene between Margot and La Môle, in which they stand outdoors wrapped in a red cloak. The director had cut this scene from the original full-length version. The scene was re-inserted because Miramax insisted that the relationship between the two characters be more substantial, as the romance was to become the focal point for the American marketing campaign. The red cloak scene appears on the US DVD cover. In contrast the Region 2 European DVD cover uses the original poster, showing a shocked Margot bespattered with blood.

==Accolades==
{| class="wikitable"
|- style="text-align:center;"
! colspan="4" style="background:#B0C4DE;" | List of Accolades
|- style="text-align:center;"
! style="background:#ccc;" width="40%"| Award / Film Festival
! style="background:#ccc;" width="30%"| Category
! style="background:#ccc;" width="25%"| Recipient(s)
! style="background:#ccc;" width="15%"| Result
|- style="border-top:2px solid gray;"
|-
|rowspan="1"| 67th Academy Awards Academy Award Costume Design Moidele Bickel
| 
|-
|rowspan="1"| 52nd Golden Globe Awards Golden Globe Best Foreign Language Film France
| 
|- Nastro dArgento|50th Silver Ribbon Awards Nastro dArgento Best Supporting Actress Virna Lisi 
| 
|- 49th British Academy Film Awards BAFTA Award Best Film Not in the English Language France
| 
|- 1994 Cannes Cannes Film Festival Best Actress Best Actress  Virna Lisi 
| 
|- Jury Prize Jury Prize  Patrice Cheréau
|  
|- Palme dOr  Patrice Cheréau
|  
|- David di 40th David di Donatello Awards Best Supporting Actress
| Virna Lisi
|  
|- Best Costumes
| Moidele Bickel
|  
|- 20th César Awards Best Film
|
| 
|- Best Director Patrice Chéreau
| 
|- Best Actress Isabelle Adjani
| 
|- Best Supporting Actor
|Jean-Hugues Anglade
| 
|- Best Actress in a Supporting Role Virna Lisi
| 
|- Best Actress in a Supporting Role Dominique Blanc
| 
|- Best Adapted Screenplay Patrice Chéreau and Danièle Thompson
| 
|- Best Cinematography Philippe Rousselot
| 
|- Best Production Design Richard Peduzzi and Olivier Radot
| 
|- Best Costume Design Moidele Bickel
| 
|- Best Editing
|François Gédigier and Hélène Viard
| 
|- Best Music Written for a Film Goran Bregovic
| 
|-
|}

==See also==
* La reine Margot – Soundtrack, album by Goran Bregović, with the music that he composed for the film
* French Wars of Religion
* La Reine Margot (1954 film), an earlier film adaption of the novel

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 