Jayam (2003 film)
{{Infobox film
| name = Jayam
| image = Jayam (2003 film).jpg
| director = M. Raja Teja Prasanna Kumar
| based on =   Gopichand
| producer = M. Varalakshmi
| music = R. P. Patnaik
| cinematography = R. Rathnavelu
| editing = A. Mohan
| distributor = M. L. Movie Aarts
| released = 20 June 2003
| runtime =
| country = India
| language = Tamil
}}
 Tamil action Ravi and Telugu Jayam film with the same title, Jayam became a commercial and critical success at the box office.

==Plot==
The story is centered in a village where the parents of first cousins Sujatha (Sadha) and Raghu (T. Gopichand|Gopichand) decide that they will be married to each other when they become adults. As a child, Sujathas uncle gives her anklets, which she wears as she grows up. As they grow older, Raghu is caught smuggling money from his house. At school, Raghu beats up a kid for talking to Sujatha. They quarrel, and soon Raghus family leaves for another village.

Sujatha grows to be an attractive college student, while the villain Raghu becomes a good-for-nothing character. On her way to college, Sujatha meets Ravi (Jayam Ravi), a poor, handsome, charming, enthusiastic young man. He asks her to wear her anklets to college again, and soon after their quarrels they fall in love. Meanwhile, Raghu’s parents, hoping that he will become a responsible person after marriage, decide to carry out his marriage with Sujatha. The whole story takes a turn when Sujatha’s parents learn about her love affair with Ravi. On the day of her marriage, Sujatha elopes with Ravi and the action heats up as they are chased by Raghu and his thugs.

Finally, the villain Raghu is defeated in a face-to-face combat with Ravi, and Ravi marries Sujatha.

==Cast==
* Jayam Ravi as Ravi
* Sadha as Sujatha Rajeev  Pragathi  Gopichand as Raghu Senthil
* Suman Setty
* Ramesh Khanna
* Shakeela
* Mayilsamy
* Poornitha 
* Reshmi Menon as Sujatha

==Soundtrack==
Songs and background score is handled by R.P. Patnaik 
# "Kanamoochi" - Shankar Mahadevan
# "Kavithayae Theriyuma" - Harini, Manicka Vinayagam, R.P. Patnaik
# "Thiruvizhannu Vantha" - Tippu (singer)|Tippu, Gowri, Raja, Ravi
# "Vandi Vandi Railu Vandi" - Tippu, Manicka Vinayagam Karthik
# "Kaathal Thandha Vali" - Karthik, Ganga
# "Kodi Kodi Minnalgal" - Vijay Yesudas

==References==
 

==External links==
*  
 

 
 
 
 
 