Dead or Alive 2: Birds
{{Infobox film name            = Dead or Alive 2: Birds image           = DeadOrAlive2BirdsFilm.jpg caption         = DVD cover director        = Takashi Miike producer        = Yoshihiro Masuda Makoto Okada writer          = Masa Nakamura starring        = Show Aikawa Riki Takeuchi music           = Chu Ishikawa cinematography  = Kazunari Tanaka   distributor     = released        =   November 11, 2001  (United Kingdom)  runtime         = 97 minutes country         = Japan language        = Japanese budget          =
}}

  is a 2000 Japanese   (1999) or   (2002) except that all three films have Show Aikawa and Riki Takeuchi in them, and they are all directed by Takashi Miike. 

==Plot==
Two contract killers cross paths on the same job and realize they are childhood friends. Together they take a break from killing and visit the small island they once called home. After reflecting on their past lives they decide to team up and use their talents in killing for good... upsetting the crime syndicates they used to work for.

==Cast==
*Show Aikawa as Mizuki Okamoto
*Riki Takeuchi as Shuuichi Sawada
*Noriko Aota as Kōheis wife
*Edison Chen as Boo
*Kenichi Endo as Kōhei
*Hiroko Isayama Masato as Hoo
*Yuichi Minato as Head of orphanage
*Ren Osugi (credited as Red Ohsugi) as Mizukis stepfather
*Manzō Shinra
*Tomorowo Taguchi as Man with telescope
*Teah as Woo
*Toru Tezuka
*Shinya Tsukamoto as Magician Higashino
*Yoshiyuki Yamaguchi

==Other credits==
*Produced by:
**Toshiki Kimura: associate producer for Excellent Film
**Mitsuru Kurosawa: executive producer for Toei Video
**Yoshihiro Masuda: producer for Daiei
**Makoto Okada: producer for Toei Video
**Tsutomu Tsuchikawa: executive producer for Daiei
*Film Editing by: Yasushi Shimamura
*Production Design by: Akira Ishige
*Sound Department: Mitsugu Shiratori (sound)

== External links ==
*  
*  

 

 
 
 
 
 
 


 