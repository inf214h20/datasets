Stage to Thunder Rock
{{Infobox film
| name           = Stage to Thunder Rock
| image          = Stage to Thunder Rock poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William F. Claxton 	
| producer       = A. C. Lyles
| writer         = Charles A. Wallace  Barry Sullivan Anne Seymour John Agar Wanda Hendrix
| music          = Paul Dunlap
| cinematography = W. Wallace Kelley 
| editing        = Jodie Copelan 
| studio         = A.C. Lyles Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film Barry Sullivan, Anne Seymour, John Agar and Wanda Hendrix. The film was released on November 10, 1964, by Paramount Pictures.  

==Plot==
 

== Cast ==	 Barry Sullivan as Sheriff Horne
*Marilyn Maxwell as Leah Parker
*Scott Brady as Sam Swope
*Lon Chaney Jr. as Henry Parker  Anne Seymour as Myra Parker
*John Agar as Dan Carrouthers
*Wanda Hendrix as Mrs. Sarah Swope
*Ralph Taeger as Reese Sawyer
*Keenan Wynn as Ross Sawyer Allan Jones as Mayor Ted Dollar
*Laurel Goodwin as Julie Parker Robert Strauss as Bob Acres Robert Lowery as Deputy Sheriff Seth Barrington
*Rex Bell Jr. as Shotgun Rex
*Argentina Brunetti as Sarita
*Morgan Brittany as Sandy
*Paul E. Burns as Joe Withers
*Wayne Peters as Toby Sawyer
*Roy Jenson as Harkins 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 