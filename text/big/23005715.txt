The Princess and the Marine
{{Infobox film
| name = The Princess and the Marine
| image =  

| writer = Ronni Kern
| director = Mike Robe
| starring = Mark-Paul Gosselaar Marisol Nichols
| producer = NBC
| distributor = NBC
| released = February 18, 2001
| awards =  ; Outstanding made for television movie/miniseries.
}}
 American made American Marine Jason Johnson and Bahrain Princess Meriam Al-Khalifa, with stars Mark-Paul Gosselaar and Marisol Nichols in the roles of Jason and Meriam.

==Plot==
Meriam Al Khalifa (Marisol Nichols) is a Bahraini royal who is not content to be in an arranged marriage, even though her strict Muslim parents would never allow a union with a non-Muslim. In the movie, Meriam is allowed to go to the local mall and watch and listen to American pop culture. One day, she desperately makes some random calls to strangers, including a Marine stationed at the U.S. embassy named Jason Johnson (Mark-Paul Gosselaar). After meeting, the two become friends and later fall in love, but Meriam doesnt tell anyone because of her parents. After being caught kissing at the Tree of Life, Bahrain, her mother forbids any more contact between the two. Meriam and Jason exchange letters with the help of a jeweler in the mall, and plan to run away to America with a fake passport for Meriam and pass her off as a fellow Marine.

Once in America, Meriam is taken in by the local authorities for being an illegal immigrant and is separated from Jason. Meriam is released after asking for political asylum|asylum, saying shed be disowned or even killed if she returned to Bahrain. Meriam and Jason marry in Las Vegas, and he is stripped of his insignia and rank to Private in the Marines.

The film concludes with Meriam and Jason stationed at a base, looking outside at an American flag, while a Marine holding a copy of the Quran salutes. It is then that Meriam tells Jason that this is what she believes in (America - freedom).

At the movies end, it notes the divorce of Jason Johnson and Meriam Al Khalifa, which was finalized on the fifth wedding anniversary.

==Awards/Nominations==
Marisol Nichols was nominated for ALMA Awards-Outstanding Actress for her role in this movie. 

==See also==
 The real Meriam Al Khalifa and Jason Johnson ]] -->
*Meriam Al Khalifa
*House of Khalifa
*Bahrain
*Tree of Life, Bahrain

==References==
 

==Cast==
 Marisol Nichols and Mark-Paul Gosselaar in The Princess and the Marine]] -->
*Mark-Paul Gosselaar as Jason Johnson
*Marisol Nichols as Meriam Al-Khalifa
*Luck Hari as Meriams mother
*Alexis Lopes as Latifa Keith Robinson as Trucker
*Sheetal Sheth as Layla

==External links==
* 
* 

 
 
 
 
 
 