Heartbeeps
 
{{Infobox film
| name           = Heartbeeps
| image          = Heartbeeps (1981).jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Allan Arkush Michael Phillips John Hill Kenneth McMillan Christopher Guest Melanie Mayron Richard B. Shull Dick Miller John Williams
| cinematography = Charles Rosher Jr.
| editing        = Tina Hirsch
| distributor    = Universal Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = $10 million
| gross          = $2,154,696 
}}  romantic sci-fi comedy film about two robots who fall in love and decide to strike out on their own. It was directed by Allan Arkush, and starred Andy Kaufman and Bernadette Peters as the robots.

Stan Winstons make-up work for Heartbeeps made him one of the nominees for the inaugural Academy Award for Best Makeup  in 1982, losing to An American Werewolf in London. 

== Plot == 
Val Com 17485 (Andy Kaufman), a robot designed to be a valet with a specialty in lumber commodities, meets Aqua Com 89045 (Bernadette Peters), a hostess companion robot whose primary function is to assist at poolside parties. At a factory awaiting repairs, they fall in love and decide to escape, stealing a van from the company to do so. 

They embark on a quest to find a place to live, as well as satisfy their more immediate need for a fresh electrical supply. They assemble a small robot, Phil, built out of spare parts, whom they treat as their child, and are joined by Catskill, a mechanical standup comic (which is seen sitting the entire movie). 

A malfunctioning law-enforcement robot, the Crimebuster, overhears the orders of the repair workers to get the robots back and goes after the fugitives. With the help of humans who run a junkyard, and using Catskills battery pack, the robots are able to save Phil before running out of power and being returned to the factory. Brought back to the factory the robots are repeatedly repaired and their memories cleared. Because they continue to malfunction they are junked. They are found by the humans who run the junk yard and reassembled. In the junkyard they live happily and build a robot daughter. The film ends with Crimebuster, after only pretending to have his mind erased, continues to malfunction, going on another mission to recover the fugitive robots.  

== Cast ==
*Andy Kaufman &ndash; ValCom-17485
*Bernadette Peters &ndash; AquaCom-89045
*Randy Quaid &ndash; Charlie Kenneth McMillan &ndash; Max
*Christopher Guest &ndash; Calvin
*Melanie Mayron &ndash; Susan
*Richard B. Shull &ndash; Factory Boss
*Dick Miller &ndash; Factory Watchman
*Kathleen Freeman &ndash; Helicopter Pilot Jack Carter (voice) &ndash; Catskill-55602
*Jerry Garcia of the Grateful Dead (voice) &ndash; Phil
*Ron Gans (voice) &ndash; Crimebuster
*Mary Woronov &ndash; Party House Owner
*Paul Bartel &ndash; Party Guest
*Wally Ann Wharton &ndash; Party Guest (as Anne Wharton)
*Barry Manilow &ndash; Party Guest
*Barry Diamond &ndash; Firing Range Technician and Catskil Performer
*Stephanie Faulkner &ndash; Firing Range Technician
*Jeffrey Kramer &ndash; Party Butler Robot
*David LeBell &ndash; Robot Forklift Driver

== Production == 
Sigourney Weaver was offered a role and was interested in the film, as she wanted to work with Andy Kaufman, but Weavers agent persuaded her to turn down the film. 

Because of a strike by the Screen Actors Guild, filming was shut down in July 1980 (along with numerous other motion picture and television series). The strike ended at the beginning of October 1980 (filming had started in June).  The film was aimed at children & was a failed experiment: Universal Pictures gave Andy Kaufman a blank check to make this film after focus group testing indicated that children liked robots, apparently in the wake of R2-D2 and C-3PO.  

Bob Zmuda, in his book Andy Kaufman: Revealed, wrote that Kaufman and Zmuda had "pitched" the screenplay of Kaufmans The Tony Clifton Story, a movie about the life and times of his alter-ego Tony Clifton, to Universal Studios. The Universal executives were concerned that Kaufman had not acted in films, except for a small role, and arranged for him to star in Heartbeeps to test whether he could carry a movie. Because the movie was "a box office disaster", plans for making the Clifton movie were cancelled.   

John Hill adapted the screenplay into a novel, Heartbeeps, published by Jove Publications in December 1981 (ISBN 0-515-06183-2).

== Reception == 
Reviews of the film were negative. Film website Rotten Tomatoes, which compiles reviews from a wide range of critics, gives the film a score of 0%.  

Vincent Canby wrote, in a negative review in The New York Times, that it was "unbearable" and a "dreadfully coy story."  Gary Arnold, writing in The Washington Post (December 23, 1981): "Its unlikely that Kaufman or Peters face serious career setbacks from a minor fiasco only a handful of people will ever see."  

Kaufman felt that the movie was so bad that he personally apologized for it on Late Night with David Letterman, and as a joke promised to refund the money of everyone who paid to see it (which didnt involve many people). Lettermans response was that if Kaufman wanted to issue such refunds, Kaufman had "better have change for a 20 (dollar bill)".  

=== Box office ===  USD $2,154,696, with an estimated budget of $10,000,000.  

=== Accolades === 
{| class="wikitable"
|-
! Award
! Category
! Recipient(s)
! Outcome
|- Saturn Award
|-
| Best Science Fiction Film
| Douglas Green
| 
|-
| Best Make-Up
| Stan Winston
| 
|-
| Academy Awards
| Best Make-Up
| Stan Winston
| 
|- Stinkers Bad Movie Awards 
|-
| Worst Picture
| Michael Phillips
|  
|-
| Most Painfully Unfunny Comedy
| Michael Phillips
|  
|-
| Worst Screenplay
| John Hill
|  
|-
| Worst Actor 
| Andy Kaufman
|  
|-
| Worst On-Screen Couple 
| Andy Kaufman and Bernadette Peters
|  
|- Most Annoying Fake Accent - Male
| Andy Kaufman
|  
|-
|}

== References ==
  

== External links ==
* 
* 
* 
*  on TheMakeupGallery

 

 
 
 
 
 
 
 
 
 