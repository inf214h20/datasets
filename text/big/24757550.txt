Hariyali Aur Rasta
{{Infobox film
| name           = Hariyali Aur Rasta
| image          = Hariyali Aur Rasta, 1962 Hindi film.jpg
| image_size     = 190px
| alt            = 
| caption        = 
| director       = Vijay Bhatt
| producer       = Shankerbhai Bhatt
| writer         = Dhruva Chatterjee (Story & screenplay) Qamar Jalalabadi (dialogue) Moosa Kaleem (dialogue director)
| narrator       = 
| starring       = Manoj Kumar Mala Sinha Shashikala Om Prakash Shailendra (lyrics) Hasrat Jaipuri(lyrics)
| cinematography = Bipin Gajjar	
| editing        = Pratap Dave	 	 
| studio         = Shri Prakash Pictures
| distributor    = 
| released       =  
| runtime        = 168 min  
| country        = India
| language       = Hindi
| budget         = 
| gross          = Rs. 1,90,00,000  
}}
Hariyali Aur Rasta (English: The Greenery and the Road) (Hindi: हरियाली और रास्ता) is a 1962 Hindi film produced and directed by Vijay Bhatt. It had Manoj Kumar and Mala Sinha as leads. The film has music by Shankar Jaikishan.
 highest grosser of the year and declared a Hit at the Indian Box Office    

==Plot==
Hariyali Aur Rasta is a triangular love story among Shankar (Manoj Kumar), Shobhana (Mala Sinha) and Rita (Shashikala). Marriage of Rita and Sankar has been fixed in their childhood, though when they grow up, Shankar falls in love with Shobhana, though eventually ends up marrying Rita as per their familys desires. But they both cannot reconcile to the ill fitting marriage, though situation deteriorate further when ex-flame Shobhana comes into their lives once again.  

==Cast==
* Manoj Kumar - Shanker
* Mala Sinha - Shobna / Kamla
* Shashikala -  Rita
* Om Prakash - Joseph Helen - Dancer Dolly
* Manmohan Krishna	- Shivnath

==Production and Crew==
The film was shot extensively in Darjeeling, especially in Ging Tea Estate and Rangneet Tea Estate. 

* Production: Gamanlal Bhatt
* Assistant Director: Arun Bhatt, Haren Ghosh
* Art Direction: Shri Krishna Achrekar, Kanu Desai
* Choreography: Satyanarayan
* Costume: Gaffar Behl

== Soundtrack == Shailendra and Hasrat Jaipuri, together they creating songs like "Ibteda-e-Ishk Mein Hum Saari Raat Jaage", Allah Jaane Kya Hoga Aage" song by Mukesh and Lata, plus a perennial Diwali hit, "Lakhon Taare Aasman Mein Ek Magar Dhunde Na Mila", "Dekh Ke Duniya Ki Diwali Dil Mera Chup Chap Jala" again by Lata and Mukesh   
{{Infobox album  
| Name        = Hariyali Aur Rasta
| Type        = Soundtrack
| Artist      = Shankar Jaikishan
| Cover       = Hariyali Aur Rasta, 1962 Hindi film soundtrack album cover.jpg
| Released    = 1962 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = Sa Re Ga Ma (HMV)
| Producer    = Shankar Jaikishan
| Reviews     =  Professor     (1962)
| This album  = Hariyali Aur Rasta  (1962)
| Next album  = Dil Tera Diwana  (1962)        
|}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer (s)
|-
| "Teri Yaad Dil Se Bhulaane Chala Hoon" (Shailendra)  Mukesh (singer)|Mukesh 
|-
|"Allah Jaane Kya Hoga Aage" (Hasrat Jaipuri) Mukesh 
|-
|"Yeh Hariyali Aur Yeh Rasta" Lata Mangeshkar
|-
| "Parwano Ki Raah Mein" Asha Bhosle 
|-
|"Kho Gaya Hai Mera Pyar" (Hasrat Jaipuri)  Mahendra Kapoor 
|-
|"Ek Tha Raja Ek Thi Rani" (Shailendra) Lata Mangeshkar
|-
| "Dil Mera Chup Chap Jala"  Lata Mangeshkar, Mukesh 
|-
| "Bol Meri Taqdeer Mein Kya Hai- I"  (Shailendra)  Lata Mangeshkar
|-
| "Bol Meri Taqdeer Mein Kya Hai - II" (Shailendra)  Lata Mangeshkar, Mukesh 
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 