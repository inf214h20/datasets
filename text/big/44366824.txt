Burlesque Fairytales
 
 
{{Infobox film
| name     = Burlesque Fairytales
| image    =  Poster_for_lucianis_burlesque_fairytales.jpg
| caption= Theatrical release poster
| director = Susan Luciani
| producer = Lindsay McFarlane
| writer   = Susan Luciani 
| starring =  
| cinematography = Derek Suter
| music = Nicholas Singer
| editing = Claus Wehlisch
| studio = Double Barrel Productions
| country = United Kingdom
| language = English
| runtime = 72 minutes 
| released =  
| budget = £35,000 
}}
 Jim Carter.  The films premiere was held at  Seattles True Independent Film Festival on 7 June 2009. 

== Cast==
* Anna Andresen as The Mermaid 
* Esmé Bianco as Mother 
* Stephen Campbell Moore as Peter Blythe-Smith  Jim Carter as The Compere 
* Benedict Cumberbatch as Henry Clark 
* Lindsay Duncan as Ice Queen 
* Barbara Flynn as Mrs Argyle 
* Mona Hammond as Deaths Wife
* Kevin Howarth as Jimmy Harrison 
* Sophie Hunter as Annabel Blythe-Smith 

==Production==
Produced by Double Barrel Productions, the film is Susan Lucianis directorial debut and was shot in 19 days at Pinewood Studios. 

==References==
{{reflist|refs=
   
   
   
}}

== External links ==
* 

 
 
 
 
 
 