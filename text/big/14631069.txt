Richard Burton's Hamlet
Richard Broadway production tragedy that played from April 9 to August 8, 1964 at the Lunt-Fontanne Theatre, and for the filmed record of it that has been released theatrically and on home video.

==Theatre==

===Background===
The production took place because of a lighthearted agreement between Richard Burton and Peter OToole while they were filming Becket (1964 film)|Becket. O’Toole decreed that they should each play Hamlet afterwards under the direction of John Gielgud and Laurence Olivier in either London or New York, with a coin toss deciding who would be assigned which director and which city. O’Toole won London and Olivier in the toss, with Burton being assigned Gielgud and New York.  O’Toole kept his part of the agreement, appearing as Hamlet under Oliviers direction in the premiere production of the Royal National Theatre later that year, and Burton approached producer Alexander H. Cohen and Gielgud about mounting a New York production. 

===Concept===
Because Burton disliked wearing period costumes, and for aesthetic reasons of his own, Gielgud conceived of a production performed in a “rehearsal” setting with an incomplete set and the actors wearing what appeared to be street clothes (although the costumes were actually the result of continuous trial-and-error in rehearsals, with the actors bringing in countless variations of attire for Gielgud to consider).  Gielgud also opted to depict the Ghost as a shadow against the back of the stage wall, voicing the character himself on tape (since he was unavailable while the production was in performance).

===Reception=== Maurice Evanss Laurence Oliviers Oscar-winning 1948 film version. Less favorably received were Linda Marsh as Ophelia and Alfred Drake as King Claudius, whom Gielgud had considered replacing with respectively Sarah Miles and either Harry Andrews or himself in rehearsals. 
 William Redfield and Richard L. Sterne, who went to the length of hiding a tape recorder in a briefcase at rehearsals to get accurate transcriptions of what was said. 

{{Infobox film|
  name          = Richard Burton’s Hamlet|
  director      = Bill Colleran (film) John Gielgud (stage)|
  writer        = William Shakespeare|
  starring      = Richard Burton Hume Cronyn Eileen Herlie Alfred Drake|
  producer      = Alexander Cohen Alfred W. Crown John Heyman|
  cinematography= Nobby Cross|
  distributor   = Theatrofilm|
  released      = 23 September 1964|
  runtime       = 191 min. |
  country = USA |
  language      = English |
gross = $3,100,000 (US/ Canada)  |
  preceded_by   = |
  followed_by   = |
}}

==Film==
A filmed record was created by recording three live performances on camera from June 30 to July 1  using a process called Electronovision  and then editing them into a single film.

===Theatrical release===
This film was produced by Horace William Sargent, Jr. (as Bill Sargent - often incorrectly attributed as William Sargent, Jr.), who was also the creator and patent holder of Electronovision, and whose credits include  , and Give em Hell, Harry!. Hamlet played for only two days in theatres to lukewarm reviews. William Redfield wrote that “the film version played four performances in a thousand theatres and has grossed (to date) a total of $4,000,000. The financial details of this venture involved a mass screwing of the acting company so excruciatingly delicious that only a separate letter could do the tale justice.” 

===Home video===
By contractual agreement, all prints of the film were to have been destroyed after its theatrical run. However, by chance, a single print was discovered in Burtons garage following his death, and his widow allowed it to be distributed on VHS, and later on DVD.  The film was originally titled Hamlet, but the VHS and DVD covers read Richard Burtons Hamlet.

==Columbia Masterworks LP album set==
A four-record Columbia Masterworks LP album set of the production was made in 1964, with its original cast. However, the recording was not made directly from the soundtrack of the film. The productions cast recorded it in the recording studio, and the album was released in both mono and stereo, but has so far not appeared on compact disc. The film, by contrast, was released only in mono sound.

After the films release on DVD, a true soundtrack album of the motion picture actually was issued, but it remained in print for an extremely short time. 

==Cast==
 
*Hamlet - Richard Burton
*Polonius - Hume Cronyn
*Claudius - Alfred Drake
*Gertrude - Eileen Herlie William Redfield George Rose
*Player King - George Voskovec
*Voltemand - Philip Coolidge
*Laertes - John Cullum
*Francisco/ Fortinbras - Michael Ebert
*Reynaldo/Osric - Dillon Evans
*Rosencrantz - Clement Fowler
*Lucianus - Geoff Garland
*Marcellus/Priest - Barnard Hughes
*Ophelia - Linda Marsh
*Horatio - Robert Milli Hugh Alexander
*Bernardo/Ensemble - Robert Burr Christopher Culkin
*Ensemble - Alex Giannini
*Ghost - John Gielgud
*Ensemble - Claude Harz John Hetherington
*Ensemble - Gerome Ragni
*Ensemble - Linda Seff
*Gentleman - Richard L. Sterne
*Ensemble - Carol Teitel
*Ensemble - Frederick Young
 

==References==
 

==External links==
*  
 

 
 
 
 
 
 