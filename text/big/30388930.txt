1919 (film)
 {{Infobox film
| name           = 1919
| image          = 
| caption        = 
| director       = Hugh Brody
| producer       = Nita Amy
| writer         = Hugh Brody Michael Ignatieff
| starring       = Paul Scofield
| music          = 
| cinematography = Ivan Strasburg
| editing        = David Gladwell
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
1919 is a 1985 British drama film directed by Hugh Brody and written by Michael Ignatieff together with Brody. It was entered into the 35th Berlin International Film Festival.   

==Cast==
* Paul Scofield as Alexander Scherbatov
* Maria Schell as Sophie Rubin
* Frank Finlay as Sigmund Freud (voice)
* Diana Quick as Anna
* Clare Higgins as Young Sophie
* Colin Firth as Young Alexander
* Sandra Berkin as Nina
* Alan Tilvern as Sophies father
* Bridget Amies as Childs Nurse
* Willy Bowman
* Norman Chancer

==References==
 

==External links==

* 

 
 
 
 
 
 
 
 
 
 