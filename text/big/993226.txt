Ray (film)
{{Infobox film
| name = Ray
| image = Ray poster.jpg
| caption = Theatrical release poster
| director = Taylor Hackford
| screenplay = James L. White
| story = Taylor Hackford James L. White
| starring = Jamie Foxx Kerry Washington Clifton Powell Harry Lennix Terrence Howard Larenz Tate Richard Schiff Regina King
| producer = Taylor Hackford Stuart Benjamin Howard Baldwin Karen Baldwin
| music =  
| cinematography = Paweł Edelman Paul Hirsch
| studio = Bristol Bay Productions Anvil Films Baldwin Entertainment Group
| distributor = Universal Studios
| released =  
| runtime = 152 minutes
| country = United States
| language = English
| budget = $40 million  . Box Office Mojo. IMDb. Retrieved March 15, 2014. 
| gross  = $124,731,534 
}} independently produced Best Actor Golden Globe, Screen Actors second actor to win all five lead actor awards for the same performance.

Charles was set to attend an opening of the completed film, but died of liver disease in June, several months before its premiere.   
 

== Plot ==
Raised on a sharecropping plantation in Northern Florida (U.S. state)|Florida, Ray Charles Robinson went blind at the age of seven, shortly after witnessing his younger brother drown. Inspired by a fiercely independent mother who insisted he make his own way in the world, Charles found his calling and his gift behind a piano keyboard. Touring across the chitlin circuit, the soulful singer gained a reputation before exploding onto the worldwide stage when he pioneered the incorporation of rhythm and blues, rock and roll, gospel, Country Music|country, jazz and orchestral influences into his inimitable style.

== Cast ==
 
* Jamie Foxx as Ray Charles
** C. J. Sanders as Young Ray Robinson
* Sharon Warren as Ray Charles mother, Aretha Robinson
* Kerry Washington as Della Bea Robinson Margie Hendricks
* Renee Wilson as Pat Lyle
* Larenz Tate as Quincy Jones
* Harry Lennix as Joe Adams
* Clifton Powell as Jeff Brown
* Curtis Armstrong as Ahmet Ertegun
* Richard Schiff as Jerry Wexler
* Kurt Fuller as Sam Clark
* Richard A. Smith as Til
* Patrick Bauchau as Dr. Hacker Terrence Dashon Howard as Gossie McKee
* Chris Thomas King as Lowell Fulson
* Wendell Pierce as Wilbur Brassfield
* Bokeem Woodbine as David "Fathead" Newman
* Aunjanue Ellis as Mary Ann Fisher
* Denise Dowse as Marlene Andres
* Warwick Davis as Oberon
* David Krumholtz as Milt Shaw
* Johnny ONeal as Art Tatum
 

== Production ==
The films production was entirely financed by Philip Anschutz, through his Bristol Bay Productions company. Taylor Hackford said in a DVD bonus feature that it took 15 years to make the film; or more specifically, as he later clarified in the liner notes of the soundtrack album, this is how long it took him to secure the financing. It was made on a budget of $40 million.

Charles was given a Braille copy of the films original script; he objected only to a scene showing him taking up piano grudgingly, and a scene implying that Charles had shown mistress and lead "Raelette" Margie Hendricks how to shoot heroin. 

Ray debuted at the 2004 Toronto International Film Festival.

== Soundtrack ==
 

The following songs were used in the film:

* "Every Day I Have the Blues"
* "Baby, Let Me Hold Your Hand"
* "Mess Around"
* "I Got a Woman"
* "Hallelujah, I Love Her So"
* "Drown in My Own Tears"
* "Mary Ann"
* "Leave My Woman Alone"
* "What Kind of Man Are You?"
* "Night Time Is the Right Time"
* "I Believe To My Soul"
* "Whatd I Say (song)|Whatd I Say"
* "Georgia on My Mind"
* "Hit the Road Jack" Unchain My Heart" You Dont Know Me"
* "I Cant Stop Loving You" Bye Bye Love"
* "Born to Lose"
* "Hard Times (No One Knows Better Than I)"

== Reception ==

=== Box office ===
Ray was released in theaters on October 29, 2004. The film went on to become a box-office hit, earning $75 million in the U.S. with an additional $50 million internationally, bringing its worldwide gross to $125 million. 

=== Critical reaction ===
The film received mostly positive reviews, with most of the praise going to Jamie Foxxs performance for which he was awarded the  . 

According to music critic Robert Christgau, "Foxx does the impossible—radiates something approaching the charisma of the artist hes portraying... thats the only time an actor has ever brought a pop icon fully to life on-screen." 

=== Awards ===
 

=== Differences from noted events ===

The films credits state that Ray is based on true events, but includes some characters, names, locations, and events which have been changed and others which have been "fictionalized for dramatization purposes."  Examples of the fictionalized scenes include:

*The films portrayal of Charles brother Georges death in 1935 shows him drowning to death in a metal tub after Ray doesnt attempt to rescue him because he assumes he is just playing; Rays mother then discovers George drowning when calling the boys in for dinner. Though George did drown to death in a metal tub, Ray did try to pull him out, but was unable to do so due to Georges large body weight; http://www.historylink.org/index.cfm?DisplayPage=output.cfm&file_id=5707  Ray then ran inside to tell his mother what had happened.     
* In the studio scene where Charles is taught the "Mess Around," he is told it is in the "key of G", but the "Mess Around" is actually in the key of E flat.
* In the film, when he arrives in Seattle at the club where he is going to audition, Ray meets a teenage Quincy Jones that very night. This event is only partly true, as Ray actually met Quincy a few days after arriving.
* Throughout the film, it is suggested that Rays depression and heroin addiction were fueled by nervous breakdowns he had over the deaths of both George and his mother, as well as his blindness. In reality, the death of his mother did give him a nervous breakdown and was thought to be a leading cause of his depression,  but the death of George and his blindness did not lead to nervous breakdowns. http://www.slate.com/articles/news_and_politics/life_and_art/2004/10/its_a_shame_about_ray.html 
* It is true that Charles kicked his heroin addiction after undergoing treatment in a psychiatric hospital during 1965, as stated towards the end of the film, but it is not mentioned that he would often use gin and marijuana as substitutes for heroin throughout much of the remaining years of his life.   Fender Rhodes electric piano, but in reality, he used a Wurlitzer electric piano on the original recording and begun using it on tour in 1956, because he didnt trust the tuning and quality of the pianos provided to him at every venue. Evans, p. 109. 
* In the film, when his backing singer and mistress Margie Hendricks informs Ray she is pregnant with his child, Ray suggests she should have an abortion, out of loyalty to Della; Margie decides to keep the baby and soon leaves Ray to pursue a separate singing career after he refuses to abandon his family, move in with her and welcome the baby into his life.  In reality, Hendricks did conceive a child with Charles and abandoned him after he refused to leave Della, but Charles never asked her to have an abortion, and welcomed any child he conceived, whether from Della or any mistress, into his personal life. 
* The film shows Ray having a party in Los Angeles sometime in 1965 when he is informed that Margie has died of a drug overdose, and he is devastated by the tragedy.  In reality, her death from an alleged drug overdose did reportedly upset Charles, but it did not occur until 1973.   
* In the scene where Charles is about to enter a segregated music hall in Augusta, Georgia, in 1962, a group of civil rights activists protesting just outside the hall successfully persuade him not to perform; Charles then declares that he will no longer perform in segregated public facilities and in response, the Georgia state legislature passes a resolution banning Charles from ever performing again in the state. In reality, a group of civil rights activists did successfully persuade Charles to reject this invitation, but the advice came in the form of a telegram rather than a street protest;  Charles also did make up for the gig later, and was never banned from performing in Georgia and still accepted invitations to perform at segregated public facilities. http://www.stfrancis.edu/content/historyinthemovies/ray.htm 
* During the final scene in the film where Charles version of "Georgia on My Mind" becomes Georgias state song, Charles is congratulated by his wife Della, and a resolution is also passed to lift the lifetime ban he had received in 1962 after he declared he would no longer perform at segregated public facilities. In reality, by the time "Georgia on My Mind" became Georgias state song in 1979, Charles and Delia had already divorced, so she wasnt present when Charles performed at the Georgia State Legislature;  and since he had never been banned from performing in Georgia in the first place, no such resolution was ever passed. 

==References==
 

== External links ==
 
 
*  
*  
*  
*  
*  
*  
*  , an October 2004 article from the Washington Post
*  , a review at Slate Magazine that lists factual inaccuracies

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 