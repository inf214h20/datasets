In Secret
 
{{Infobox film
| name           = In Secret
| image          = In Secret.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Charlie Stratton
| producer       = Mickey Liddell Pete Shilaimon William Horberg
| screenplay     = Charlie Stratton 
| based on       =  
| starring       = Elizabeth Olsen Tom Felton Oscar Isaac Jessica Lange
| music          = Gabriel Yared
| cinematography = Florian Hoffmeister Leslie Jones Paul Tothill
| studio         = Exclusive Media Group LD Entertainment
| distributor    = Roadside Attractions
| released       =  
| runtime        = 106 minutes  
| country        = United States
| language       = English
| budget         = $2 million
| gross          = $444,179 
}} classic novel Thérèse Raquin,  the film stars Elizabeth Olsen, Tom Felton, Oscar Isaac and Jessica Lange. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.       The film received a regional release on February 21, 2014.

==Plot==
In the lower echelons of 1860s Paris, Thérèse Raquin is a sexually repressed beautiful young woman trapped in a loveless marriage to her sickly cousin, Camille, having been forced into it by her domineering aunt, Madame Raquin. Thérèse spends her days confined behind the counter of a small shop and her evenings watching Madame play dominoes with an eclectic group. After she meets her husbands alluring friend Laurent LeClaire, she embarks on an illicit affair that leads to tragic consequences. Camille soon dies during an outing on the lake with Laurent and Therese, which is revealed to be their doing as Laurent physically beat Camille while he was in the water, causing him to drown. Madame Raquin finds it difficult to come to terms with her sons death and is incapacitated by a stroke. To escape being sentenced for murder, Laurent and Therese choose to take their own lives and they drink poison mixed with champagne, dying in front of Madame Raquin.

==Cast==
* Elizabeth Olsen as Thérèse Raquin (character)|Thérèse Raquin, Camilles wife and first cousin.
** Lily Laight as young Thérèse
* Tom Felton as Camille Raquin 
** Dimitrije Bogdanov as young Camille
* Oscar Isaac as Laurent LeClaire, a childhood friend and co-worker of Camille who seduces his wife, Thérèse.
* Jessica Lange as Madame Raquin, Camilles mother and Thérèses aunt.
* Matt Lucas as Olivier
* Shirley Henderson as Suzanne
* Mackenzie Crook as Grivet John Kavanagh as Inspector Michaud

==Production==

===Casting===
Kate Winslet was attached for a long time to star in the lead role of Thérèse Raquin. Jessica Biel then replaced her, with Gerard Butler as Laurent. In fall 2011, Elizabeth Olsen was announced as a replacement in the lead role. Glenn Close was originally cast as Madame Raquin, but dropped out and was replaced by Jessica Lange. 

===Filming===
On May 9, 2012, principal photography began in Belgrade, Serbia and Budapest, Hungary. 

==Reception==
In Secret received mixed reviews. On review aggregation website Rotten Tomatoes, the film holds a 47% rating, with an average score of 5.5/10, based on reviews from 64 critics. The consensus states: "Although it benefits from a strong cast, In Secret s stars cant totally compensate for the movies sodden pacing and overly familiar story." 

Despite mixed reviews, Langes performance has been critically praised. Avi Offer from NYC Movie Guru proclaimed Lange as "...one of the greatest actresses of our time,   delivers a mesmerizing, magnificent performance and sinks her teeth into her role quite smoothly." David Lee Dallas from   described her ability to "  a looming presence that shifts from despicable to sympathetic and back again." USA Today described her work as her "...most fully dimensional performance." Variety (magazine)|Variety mentioned that Lange "...relishes what becomes the most dramatically potent role."

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 