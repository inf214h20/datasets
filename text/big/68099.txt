Kiss Me Deadly
 
{{Infobox film
| name           = Kiss Me Deadly
| image          = KissMeDeadly.jpg
| caption        = Theatrical release poster
| alt            = In the upper half of the poster, theres a torso painting of a man embracing a woman. Hes kissing her neck just below her left ear. The man is wearing a business suit. The woman is in a strapless gown; her skin is bare above her chest. Shes leaning away from the man, with her eyes open and a quizzical expression; shes holding a small pistol in her right hand, which is dangling loosely. In the upper right corner, the words "Blood Red Kisses!" are lettered in red. In the middle of the poster, and just below the right corner of the painting of the couple, the phrase "Mickey Spillanes Latest H-Bomb" is lettered. Below the left corner: "White Hot Thrills" is lettered. Below the center is a painting of parted red lips with "Kiss Me Deadly" lettered on them; in smaller letters above the lips is "Parklane Pictures presents". Theres a small billing block at the lower left of the poster: "starring Ralph Meeker/ with Albert Dekker - Paul Stewart - Juano Hernandez/ Produced and directed by Robert Aldrich / screenplay by A. I. Bezzerides/ Released through United Artists". There are several small paintings of scenes from the film scattered around the poster.
| director       = Robert Aldrich
| producer       = Robert Aldrich
| screenplay     = A. I. Bezzerides
| based on       =   Paul Stewart Juano Hernandez
| music          = Frank DeVol
| cinematography = Ernest Laszlo
| editing        = Michael Luciano
| studio         = Parklane Pictures
| distributor    = United Artists
| released       =  
| runtime        = 106 minutes 104 minutes (USA)
| country        = United States
| language       = English
| budget         = $410,000 Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 238 
| gross = $726,000 (USA/Canada) $226,000 (foreign) 436,699 admissions (France)   at Box Office Story 
}} drama produced and directed by Robert Aldrich starring Ralph Meeker. The screenplay was written by A.I. Bezzerides, based on the Mickey Spillane Mike Hammer mystery novel Kiss Me, Deadly.  Kiss Me Deadly is often considered a classic of the noir genre. The film grossed $726,000 in the United States and a total of $226,000 overseas. It also withstood scrutiny from the Kefauver Commission, which called it a film designed to ruin young viewers, leading director Aldrich to protest the Commissions conclusions.

Kiss Me Deadly marked the film debuts of both actresses Cloris Leachman and Maxine Cooper.   

In 1999, Kiss Me Deadly was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."
==Plot== private eye who is almost as brutal and corrupt as the crooks he chases. Mike and his assistant/secretary/lover, Velda (Maxine Cooper), usually work on "penny-ante divorce cases."

One evening on a lonely country road, Hammer gives a ride to Christina (Cloris Leachman), an attractive hitchhiker wearing nothing but a trench coat.  She has escaped from a nearby mental institution. Thugs waylay them and Hammer awakens in some unknown location where he hears Christina screaming and being tortured to death. The thugs then push Hammers car off a cliff with Christinas body and an unconscious Hammer inside. Hammer next awakens in a hospital with Velda by his bedside.  He decides to pursue the case, both for vengeance and because "she (Christina) must be connected with something big" behind it all.

 
The twisting plot takes Hammer to the apartment of Lily Carver (Gaby Rodgers), a sexy, waif-like blond who is posing as Christinas ex-roommate.  Lily tells Hammer she has gone into hiding and asks Hammer to protect her. It turns out that she is after a mysterious box that, she believes, has contents worth a fortune.
 Repo Man, Richard Kelly pays homage to the film, showing the main characters watching the beginning on their television and later the opening of the case is shown on screens on board the mega-Zeppelin.
 criticality when the box is fully opened.  Horrifying sounds emit from the nuclear material as Gabrielle and the house burst into flames just as Hammer and Velda escape.

===Alternative ending===
The original American release of the film shows Hammer and Velda escaping from the burning house at the end, running into the ocean as the words "The End" come over them on the screen. Sometime after its first release, the ending was altered on the films original negative, removing over a minutes worth of shots where Hammer and Velda escape and superimposing the words "The End" over the burning house. This implied that Hammer and Velda perished in the atomic blaze, and was often interpreted to represent the apocalypse. In 1997, the original conclusion was restored, where Velda and Mike survive. The DVD release has the original ending, and offers the truncated ending as an extra.

The movie is described as "the definitive, apocalyptic, nihilism|nihilistic, science-fiction film noir of all time – at the close of the classic noir period."   

==Cast==
 
 
* Ralph Meeker as Mike Hammer
* Albert Dekker as  Dr. G.E. Soberin Paul Stewart as Carl Evello
* Juano Hernandez as Eddie Yeager
* Wesley Addy as Lt. Pat Murphy
* Marian Carr as Friday
* Maxine Cooper as Velda
* Cloris Leachman as Christina Bailey
* Gaby Rodgers as Gabrielle (Lilly Carver)
* Nick Dennis as Nick Jack Lambert as Sugar Smallhouse
* Jack Elam as Charlie Max
* Jerry Zinneman as Sammy
* Leigh Snowden as Cheesecake
 
* Percy Helton as Doc Kennedy
* Mady Comfort as jazz singer
* Strother Martin as Harvey Wallace
* Kitty White as Vocalist in club
* James Seay as FBI agent
* Bing Russell as Police Detective Paul Richards as Paul Richards
* Sam Balter as Radio Announcer (voice)
* Eddie Beal as Sideman
* Marjorie Bennett as Manager
* Fortunio Bonanova as Carmen Trivago Ben Morris as Radio Announcer
* Leonard Mudie as Athletic Club Clerk
 

==Background==

===Los Angeles locations=== Bunker Hill (Italian opera singers home)
* The Donigan Castle, a Victorian mansion at 325 S. Bunker Hill Avenue (where Cloris Leachmans character lived; it was used for interiors and exteriors). Wilshire Blvd, NW corner of Wilshire and Beverly Glen (Hammers apartment building; still standing)
* Carl Evellos Mansion, 603 Doheny Road, Beverly Hills, California
* Clay Street, an alley beneath Angels Flight incline railway, on Bunker Hill, where Hammer parks his Corvette and then takes the back steps up to the Hill Crest Hotel, but when we cut to him approaching the hotels large porch, hes on the Third Street steps opposite Angels Flight.
* Club Pigalle, 4800 block of Figueroa Avenue (the black jazz nightclub where Hammer hangs out)
* Hollywood Athletic Club, 6525 W. Sunset Blvd. (where Hammer finds the radioactive box; still standing)
* Kiss Me Deadly remains one of the great time capsules of Los Angeles; the Bunker Hill locations were all destroyed when the downtown neighborhood was razed in the late 1960s.

==Reception==

===Critical response===
Critical commentary generally views it as a metaphor for the paranoia and nuclear fears of the Cold War era in which it was filmed. 
 leftist at the time of the Hollywood blacklist, Bezzerides denied any conscious intention for this meaning in his script.  About the topic, he said, "I was having fun with it. I wanted to make every scene, every character, interesting." 

Film critic Nick Schager wrote, "Never was Mike Hammers name more fitting than in Kiss Me Deadly, Robert Aldrichs blisteringly nihilistic noir in which star Ralph Meeker embodies Mickey Spillanes legendary P.I. with brute force savagery...The gumshoes subsequent investigation into the womans death doubles as a lacerating indictment of modern societys dissolution into physical/moral/spiritual degeneracy – a reversion that ultimately leads to nuclear apocalypse and mans return to the primordial sea – with the directors knuckle-sandwich cynicism pummeling the genres romantic fatalism into a bloody pulp. Remember me? Aldrichs sadistic, fatalistic masterpiece is impossible to forget." 

The review aggregator Rotten Tomatoes reported that 97% of critics gave the film a positive review, based on 37 reviews. 

===Accolades===
In 1999, Kiss Me Deadly was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."

American Film Institute
*AFIs 100 Years...100 Thrills – Nominated
*AFIs 100 Years...100 Movie Quotes:
**CHRISTINA: "Get me to that bus stop and forget you ever saw me. If we dont make that bus stop…" MIKE HAMMER: "We will." CHRISTINA: "If we don’t, remember me." – Nominated
*AFIs 10 Top 10 – Nominated mystery film

==Differences from the novel==
The original novel, while providing much of the plot, is about a mafia conspiracy and does not feature espionage and the nuclear suitcase, elements added to the film version by the scriptwriter, A.I. Bezzerides.
 narcissistic bully, the darkest anti-hero private detective in the film noir genre. He apparently makes most of his living by blackmailing adulterous husbands and wives, and he takes an obvious sadistic pleasure in violence, whether hes beating up thugs sent to kill him, breaking an informants treasured record collection, or roughing up a coroner whos slow to part with a piece of information. He also apparently has no compunction about engaging in nefarious acts such as pimping his secretary. Bezzerides wrote of the script: "I wrote it fast because I had contempt for it ... I tell you Spillane didnt like what I did with his book. I ran into him at a restaurant and, boy, he didnt like me." 

==Home media==
A digitally restored version of the film was released on DVD and Blu-ray by The Criterion Collection in June 2011 and has the alternate ending as a bonus feature. 

==References==
 

==External links==
 
*  
*  
*  
*  
*   article by Alain Silver ("Evidence of a Style")
*   essay J. Hoberman for The Criterion Collection ("Kiss Me Deadly: The Thriller of Tomorrow")
*   photos from the set of at The Criterion Collection
*   article by Glenn Erickson ("The Kiss Me Mangled Mystery") 
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 