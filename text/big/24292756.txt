Eyes of a Stranger (1981 film)
{{Infobox film
| name        = Eyes of a Stranger
| image       = Eyes-of-a-Stranger-poster.jpg
| caption     =
| director    = Ken Wiederhorn| Mark Jackson
| producer    = Ronald Zerra
| starring    = Jennifer Jason Leigh Lauren Tewes John DiSanti
| distributor = Warner Bros. Pictures (1981 Theatrical) Warner-Columbia (1981 Austrian Theatrical Release) Warner Home Video (1994 VHS and 2007 DVD)
| released    =  
| runtime     = 85 min.
| country     = United States
| language    = English
| music       = Richard Einhorn
}}
Eyes of a Stranger is a 1981 slasher film directed by Ken Wiederhorn.  It features makeup effects by Tom Savini and stars Jennifer Jason Leigh in one of her earliest roles.

==Plot== rapist and murderer who stalks his victims and then calls them repeatedly before raping and killing them.  A feminist TV anchor becomes suspicious and investigates one of her neighbors, who she believes is committing the crimes. 

==Release==
Eyes of a Stranger was released in 180 theaters in the United States by Warner Bros. on March 27, 1981, earning $546,724 during its opening weekend. It eventually grossed an estimated  $1.1 million.

==Edited versions==
The film was originally cut for an R rating, removing many instances of violence including a decapitation from the film, leaving only the final head shot uncut. As a result many of Tom Savinis gore effects were cut out or edited. The uncut version was released on DVD as part of Warners Twisted Terror Collection with an R-rating on the packaging.

== Critical reception ==
Allmovie wrote, "this tired, unimaginative slasher-thriller plays like a sleazy TV movie-of-the-week punctuated with gory murder scenes". 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 