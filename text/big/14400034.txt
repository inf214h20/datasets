Captain Kidd's Kids
(Not Mary Pickfords Captain Kidd, Jr., also from 1919)
 
{{Infobox film
| name           = Captain Kidds Kids
| image          = Captain Kidds Kids.jpg
| caption        = Theatrical poster to Captain Kidds Kids
| director       = Hal Roach
| producer       = Hal Roach
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = 
| editing        = 
| distributor    = Rolin Films
| released       =  
| runtime        = 20 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd. Prints of this film exist in the film archives of the UCLA Film and Television Archive and Filmoteca Española.   

==Cast==
* Harold Lloyd - The Boy
* Bebe Daniels - The Girl
* Snub Pollard - The Valet
* Fred C. Newmeyer - Ah Nix (Chinese Cook) (as Fred Newmeyer)
* Helen Gilmore - The Girls mother Charles Stevenson - Servant (as Charles E. Stevenson)
* Noah Young - Big pirate
* Marie Mosquini - Pirate girl
* Sammy Brooks - Small pirate

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 