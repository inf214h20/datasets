The Leopard (1918 film)
{{Infobox film
| name           = The Leopard
| image          = 
| caption        = 
| director       = Alfréd Deésy
| producer       = 
| writer         = Alphonse Daudet
| narrator       = 
| starring       = Béla Lugosi
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hungary Silent
| budget         = 
| gross          = 
}}
The Leopard is a 1918 Hungarian film directed by Alfréd Deésy and featuring Béla Lugosi.

==Cast==
* Annie Góth as Viktória hercegnõ
* Péter Konrády
* Ila Lóth as Postáskisasszony
* Béla Lugosi as Orlay Pál,építész (as Arisztid Olt)
* Klára Peterdy as Matild bárónõ
* Gusztáv Turán

==See also==
* Béla Lugosi filmography

==External links==
* 

 
 
 
 
 
 
 
 
 