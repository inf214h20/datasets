Heaven's Postman
{{Infobox film
| name           = Heavens Postman 
| image          = File:Heavens_Postman_poster.jpg
| director       = Lee Hyung-min
| producer       = Shin Hyun-taek   Go Dae-jung 
| writer         = Eriko Kitagawa
| starring       = Kim Jaejoong   Han Hyo-joo
| music          = 
| cinematography = Jin Jin
| editing        = 
| distributor    = CJ Entertainment   Toho
| released       =  
| runtime        = 107 minutes
| country        = South Korea Japan
| language       = Korean
| budget         =  
| admissions     = 94,694  
| gross          =    
}}

Heavens Postman, also known as Postman to Heaven ( ;  ) is a 2009 South Korean-Japanese film starring Kim Jaejoong and Han Hyo-joo. A young CEO quits his job and becomes a kind of supernatural postman, delivering letters from grieving families and loved ones to the dead in heaven. 
 CGV theaters SBS (South Korea) on September 25, 2010, and TV Asahi (Japan) in 2010.

==Plot== IT company, until he unexpectedly becomes a postman. He delivers the letters grieving people have written to their loved ones in heaven. One day, he comes across Hana, who writes a letter full of resentment to the dead man that she used to love, and reveals his presence to her. Jae-joon proposes that Hana delivers responses which come back from heaven and the two think up various ways to give peace and happiness to those who are alive and left behind, sometimes by writing the responses themselves. But a human being and a postman from heaven cannot spend unlimited time together. As they start to grow feelings for each other, Jae-joon tries to pull himself away from Hana and the two, for the last time, deliver a response to an owner of a teahouse who had been agonizing for a long time over the loss of his son. 

==Cast==
*Kim Jae-joong as Shin Jae-joon / Yuu
*Han Hyo-joo as Jo Hana / Saki
*Shin Goo as Choi Geun-bae
*Kim Chang-wan as Lee Moon-gyo
*Yook Mi-ra as Moon-gyos wife
*Joo Jin-mo as Yoon Jeon-soo
*Lee Doo-il as Goo Dae-bong
*Jang Jung-hee as Woo-subs mother

==See also==
*The Relation of Face, Mind and Love
*19-Nineteen
*Triangle (2009 South Korean film)|Triangle
*Paradise After the Banquet
*A Dream Comes True

==References==
 

==External links==
* http://www.telecinema7.jp/  
* http://cafe.naver.com/telecine7/  
* http://www.tv-asahi.co.jp/nikkandrama/  
*  
*  
*  
 

 
 
 
 
 
 
 