The Loves of Pharaoh
{{Infobox film
| name           = The Loves of Pharaoh
| image          = The Loves of Pharaoh 1922 newspaperad.jpg
| alt            =  
| caption        = A contemporary newspaper advertisement for the American release of the film.
| director       = Ernst Lubitsch
| producer       =
| writer         = Norbert Falk Hanns Kräly
| starring       = Emil Jannings Paul Biensfeldt Friedrich Kühne Albert Bassermann Harry Liedtke Paul Wegener
| music          = Eduard Künneke (Original) Hugo Riesenfeld (US version)
| cinematography = Alfred Hansen Theodor Sparkuhl
| editing        = 
| studio         = Ernst Lubitsch-Film  Europäische Film-Allianz
| distributor    = 
| released       =  
| runtime        = 110 minutes 100 minutes  
| country        = Weimar Republic
| language       = Silent film
| budget         = 
| gross          = 
}}
 historical epic film directed by Ernst Lubitsch. It starred Emil Jannings.

A complete version of the film had been considered lost for years.  A digitally restored and  reconstructed version premièred on 17 September 2011.  The restored film includes the original music by composer Eduard Künneke that had been commissioned for the film by Lubitsch.   
 Hollywood that he could make an epic. The Loves of Pharaoh was his last German feature before he migrated to Hollywood in 1923. 

==Restoration== Munich Film Museum did the reconstruction. The complete version of The Loves of Pharaoh had been lost, so restoration had to be done from parts of the film that had been found in various countries. The largest part came from a 35mm tinted nitrate print the German Federal Archives had acquired from a film archive in Russia in the 1970s. The Russian footage lasted only 55 minutes and was missing all the scenes dealing with love and emotion. This version revolved around the massive combat sequences. The restoration extended the Russian version with footage from an Italian nitrate print of Pharaoh that the George Eastman House in Rochester, New York, had acquired in 1998. The Italian print had been fragmented but contained the missing love scenes. To this was added other footage, and the title cards, which had turned up during the Munich Film Museums restoration work on another movie.  According to the introduction of the restoration, about   of the original   of film is still missing.

==Plot==
Pharaoh Amenes (Emil Jannings) receives glad tidings: King Samlak of Ethiopia proposes an alliance, to be cemented by the marriage of Amenes to Samlaks daughter, Makeda. Sothis, Ameness master builder, reports there has been an accident at the construction site of the treasury and begs for more time for his workers sake, but Amenes is unmoved.

As Samlak and Makeda trek to Amenes, Ramphis, the son of Sothis, spots Makedas despised Greek slave, Theonis. Entranced, he takes her home with him. When Ramphis tries to kiss Theonis, she playfully runs away toward the treasury, unaware that the penalty to approach the place is death. Ramphis chases after her, but they are caught and brought before Amenes. 

Pharaoh sentences them both to be executed at dawn. Theonis throws herself at his feet and begs him to spare Ramphis, as it was all her fault. Amenes immediately falls under her spell. He offers to let Ramphis live in return for her. Theonis rejects him, but seeing Ramphis about to be crushed underneath a gigantic stone slab, she gives in. Amenes commutes Ramphiss sentence to a life working in the quarries; the prisoner is told that Theonis has been executed.

Amenes decides to make Theonis his queen, mortally offending King Samlak. Samlak raises his army and invades the country. Meanwhile, Ramphis triggers a rebellion at the quarry when he goes to the aid of a stricken fellow prisoner. He escapes in the confusion when the news of the invasion breaks.

Back in Ameness city, Pharaoh prepares to lead out his army. Before he leaves, however, he demands that Theonis swear an oath not to take another man if he is killed in battle. When she refuses, he orders that she be sealed within the treasury. He has Sothis show him the secret entrance to the treasury, then has the builder blinded.

Samlak launches a surprise attack on Ameness camp, routing the defenders. He personally shoots an arrow into the back of Amenes, causing him to fall from his fleeing chariot. Before he succumbs, Amenes asks Samlak not to harm Theonis. 

Ramphis makes his way home. When he learns what has happened to his father and Theonis, he enters the treasury, intent on killing Theonis, blaming her for his fathers blinding. However, when he sees her, he cannot go through with it.

Samlak marches on the city. He gives the terrified inhabitants a choice: give up their queen or he will sack the city. They break into the treasury. Theonis, apprised of the situation, decides to give herself up, but Ramphis rallies the soldiers and prepares an ambush. It works. Samlaks army is driven away.

Queen Theonis chooses Ramphis as her king, to the delight of the soldiers. Then Amenes shows up, haggard but still alive. The chief priest tells him he has lost his throne, but that Theonis is still his wife. No one dares challenge the law of the gods. In desperation, Ramphis offers him back the throne in return for Theonis. When the couple leave the palace, the mob turns on them, stoning them to death, despite Ameness attempt to stop it. Distraught, Amenes returns to his throne room, then falls to the floor dead.

==Cast==
*Emil Jannings as Pharao Amenes 
*Paul Biensfeldt as Menon, Ameness governor 
*Friedrich Kühne as Oberpriester
*Albert Bassermann as Sothis
*Harry Liedtke as Ramphis 
*Paul Wegener as Samlak
*Lyda Salmonova as Makeda
*Dagny Servaes as Theonis   

==See also==
*List of historical drama films
* List of rediscovered films

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 