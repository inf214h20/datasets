Oculus (film)
 
{{Infobox film
| name           = Oculus
| image          = Oculus.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster Mike Flanagan
| producer       = {{plainlist|
* Marc D. Evans
* Trevor Macy
* Jason Blum
}}
| writer         = {{plainlist|
* Mike Flanagan
* Jeff Howard
}}
| based on       =  
| starring       = {{plainlist|
* Karen Gillan
* Brenton Thwaites
* Rory Cochrane
* Katee Sackhoff
}}
| music          = The Newton Brothers
| cinematography = Michael Fimognari
| editing        = Mike Flanagan
| studio         = {{plainlist|
* Blumhouse Productions
* WWE Studios
* Intrepid Pictures
}}
| distributor    = Relativity Media
| released       =  
| runtime        = 103 minutes  
| country        = United States
| language       = English
| budget         = $5 million 
| gross          = $44 million   
}} Mike Flanagan. wide theatrical release on April 11, 2014.  The film stars Karen Gillan as a young woman who is convinced that an antique mirror is responsible for the death and misfortune that her family suffered. The film is based upon an earlier short film by Flanagan, Oculus: Chapter 3 – The Man with the Plan. 

==Plot==
The film takes place in two different times: the present and 11 years earlier. The two plotlines are told in parallel through Flashback (narrative)|flashbacks. In 2002, software engineer Alan Russell moves into a new house with his wife Marie, 10-year-old son Tim, and 12-year-old daughter Kaylie. Alan purchases an antique mirror to decorate his office. Unknown to them, the mirror is malevolent and supernaturally induces hallucinations in both adults; Marie is haunted by visions of her own body decaying, while Alan is seduced by a ghostly woman named Marisol, who has mirrors in place of eyes.

Over time, the parents become psychotic; Alan increasingly isolates himself in his office, and Marie becomes withdrawn and paranoid. During the same period, all of the plants in the house die, and the family dog disappears after being locked in the office with the mirror. After Kaylie sees Alan interact with Marisol and tells her mother, Marie goes insane and attempts to kill her children. Alan overpowers her and chains her to their bedroom wall. When the family runs out of food, the children attempt to seek help from their neighbors, who disbelieve their stories. Attempting to contact doctors or the authorities, Kaylie discovers that all of her phone calls are answered by the same man, who admonishes her to speak with her father.

One night, Alan unchains Marie, and both parents attack the children. Marie briefly comes to her senses, only to be shot dead by Alan. Alan corners the children in his office but also experiences a moment of lucidity, during which he forces Tim to shoot him to death. The police arrive and take Tim into custody. Before the siblings are separated, they promise to reunite as adults and destroy the mirror. As Tim is taken away in the back of a squad car he sees the ghosts of his parents watching him from the house.

Eleven years later, Tim is discharged from a psychiatric hospital, having come to believe that there were no supernatural events involved in his parents deaths. Kaylie, meanwhile, has spent most of her young adulthood researching the history of the mirror. Using her position as an employee of an auction house, Kaylie obtains access to the mirror and has it transported to the family home, where she places it in a room filled with surveillance cameras and a "kill switch"—an anchor weighted to the ceiling and set to a timer. Kaylie intends to destroy the mirror but first wants to document its powers, prove its supernatural nature, and thus vindicate her family.

Tim joins Kaylie at the house and attempts to convince her that shes rationalized their parents deaths as being caused by an external force. The siblings argue until the houseplants begin to wilt and cameras document behavior they can not recall. Tim finally accepts that the mirror has some diabolical power and attempts to escape the house with Kaylie, only for the pair to be repeatedly drawn back by the mirrors influence. Trying to call the police for help, they are only able to reach the same voice who spoke to them on the phone as children. Kaylie accidentally kills her fiancé, who she mistakes for a hallucination of her deceased mother, and later sees his ghostly figure. The pair begin to hallucinate and experience visions of everyone killed by the mirror.

As the two timelines merge, Kaylie and Tim begin to interact with their younger selves. Younger Kaylie is drawn to the mirror by an image of her mother beckoning to her; at the same time, Tim as an adult hallucinates that he is alone in the room with the mirror. Tim activates the kill switch, causes the anchor to descend, and fatally impales a mirror-obscured Kaylie. The police arrive and arrest a hysterical Tim, just as they had arrested him when he was younger. As both a boy and adult, Tim claims the mirror is responsible. As he is taken away, the older Tim sees Kaylies ghost in the house with his parents and remembers their promise that they will destroy the mirror no matter what.

==Cast==
* Karen Gillan as Kaylie Russell
** Annalise Basso as 12-year-old Kaylie
* Brenton Thwaites as Tim Russell
** Garrett Ryan as 10-year-old Tim
* Rory Cochrane as Alan Russell
* Katee Sackhoff as Marie Russell
* James Lafferty as Michael Dumont
* Miguel Sandoval as Dr. Graham
* Katie Parker as Phone Store Clerk
* Kate Siegel as Marisol Chavez

==Production==
The film is based on an earlier 2005 short horror film, also called Oculus. The short contained only one setting, a single actor, and a mirror.    The short became highly acclaimed, and interest quickly arose regarding the adaptation of the short into a feature.  Initially, studios were interested in making the film in accordance with the found footage genre;    however, Flanagan was opposed to this and passed on such offers.  Eventually, Intrepid Pictures expressed interest in producing the film under the condition that "as long as you dont do it found footage."

Expanding the premise to a feature-length screenplay proved challenging, as Flanagan felt like he had "pushed the limit" of what could be done with the premise in the short. The solution Flanagan came across was to combine two storylines, past and present, intercut with one another. The idea was to "create a sense of distortion and disorientation that would be similar for the viewer as it was for Tim and Kaylie in the room."  In early drafts, it was difficult to distinguish between the two timelines, until the team hit upon the idea of writing all of the scenes from the past in italics. 
 Lovecraftian literature often seemed to be an "alien force that if you even were to try to comprehend it completely it would drive you mad."  He also expanded: "Evil in the world doesnt have an answer." 

On October 27, 2012, filming wrapped in Alabama, after three weeks. 

==Release==
The film was first released on September 5, 2013, at the 2013 TIFF,  and received a worldwide theatrical release on April 11, 2014. 

The film was released on DVD and Blu-ray Disc on August 5, 2014. 

==Soundtrack==
{{Infobox album  
| Name        = Oculus
| Type        = soundtrack
| Artist      = The Newton Brothers
| Cover       = 
| Caption     = 
| Released    = April 15, 2014
| Recorded    = 
| Genre       = Classical, stage and screen
| Length      = 1:11:13
| Label       = Varèse Sarabande
| Producer    = Bryon Davis
| Last album  = 
| This album  = 
| Next album  = 
}} digitally on CD on April 15, 2014. 

{{Track listing
| collapsed       = yes
| total_length    = 71:13 

| title1          = Recurring Dream
| length1         = 1:34

| title2          = The Auction
| length2         = 3:20

| title3          = Moving IN
| length3         = 1:47

| title4          = You Promised Me
| length4         = 1:20

| title5          = Juice Box
| length5         = 0:33

| title6          = Kaylies Nightmare
| length6         = 0:57

| title7          = Graphic Photos
| length7         = 1:48

| title8          = The Mirror
| length8         = 1:02

| title9          = Grotesque Cow
| length9         = 0:16

| title10         = The Other Woman
| length10        = 0:16

| title11         = Memories Surface
| length11        = 1:14

| title12         = Dead Plants
| length12        = 0:47

| title13         = We Have a Gun to Its Head
| length13        = 1:41

| title14         = History of the Mirror
| length14        = 1:55

| title15         = Fuzzy-Trace Theory
| length15        = 2:30

| title16         = Mason Was Sick
| length16        = 0:27

| title17         = Fingernails
| length17        = 1:41

| title18         = Book Stacking
| length18        = 1:14

| title19         = Whispers in the Glass
| length19        = 0:19

| title20         = Who Are You Talking To?
| length20        = 1:04

| title21         = The Reveal
| length21        = 2:55

| title22         = Marisol, Marisol, Marisol
| length22        = 2:53

| title23         = Shifting Glass
| length23        = 0:48

| title24         = Maries Breakdown
| length24        = 3:40

| title25         = Lightbulbs & Apples
| length25        = 2:30

| title26         = Seeing Things
| length26        = 0:57

| title27         = Broken Plates
| length27        = 0:58

| title28         = She Needs a Doctor
| length28        = 1:20

| title29         = This Isnt Real
| length29        = 4:59

| title30         = Staring Eyes
| length30        = 5:12

| title31         = It Wont Let Us
| length31        = 0:56

| title32         = Ive Seen the Devil and He Is ME
| length32        = 2:54

| title33         = A Mothers Embrace
| length33        = 3:17

| title34         = Oculus
| length34        = 4:41

| title35         = Oculus
| note35          = Remix) (featuring Paul Oakenfold
| length35        = 2:49

| title36         = Oculus of Glass
| note36          = featuring Paul Oakenfold
| length36        = 4:39
}}

==Reception==

===Critical reception===
Critical reception for Oculus has been positive and the film holds a rating of 73% on   gave Oculus a positive review, stating that it was "smart and scary stuff".  Film School Rejects gave a mixed review and stated that it was "well-acted, looks quite good, and manages some moments of entertainment, but as the minutes tick by it grows weaker and weaker until its final cheat is designed to allow for a shocker ending." 

===Box office===
Released in the United States on April 11, 2014, Oculus grossed $4,966,899 on its opening day, nearly equaling its production budget of $5 million. As of October 3, 2014, the film has taken in an estimated $27,695,246 at the North American box office and another $16,335,000 internationally for a worldwide total of $44,030,246. 

==Remake== Huma Qureshi The Best of Me and 3 Days to Kill, along with Oculus. 

==See also==
* List of ghost films

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 