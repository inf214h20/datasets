The Postman Always Rings Twice (1946 film)
{{Infobox film
| name            = The Postman Always Rings Twice
| image           = PostmanAlwaysPoster.jpg
| caption         = Theatrical release poster
| alt             =
| director        = Tay Garnett
| producer        = Carey Wilson
| screenplay      = Harry Ruskin Niven Busch
| based on        =  
| starring        = Lana Turner John Garfield Cecil Kellaway Hume Cronyn
| music           = George Bassman Erich Zeisl
| cinematography  = Sidney Wagner George White
| distributor     = Metro-Goldwyn-Mayer
| released        =  
| runtime         = 113 minutes
| country         = United States
| language        = English
| budget          = $1,683,000  .  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 380 
| gross           = $5,086,000  
}}
 novel of Leon Ames, and Audrey Totter. It was directed by Tay Garnett, with a score written by George Bassman and Erich Zeisl (the latter uncredited). 

This version was the third filming of The Postman Always Rings Twice, but the first under the novels original title and the first in English.  Previously, the novel had been filmed as Le Dernier Tournant (The Last Turning) in France in 1939, and as Ossessione (Obsession) in Italy in 1943.

==Plot==
  from the trailer for the film]]

Frank Chambers (John Garfield) is a drifter who stops at a rural diner for a meal, and ends up working there. The diner is operated by a beautiful young woman, Cora Smith (Lana Turner), and her much older husband, Nick (Cecil Kellaway).

Frank and Cora start to have an affair soon after they meet. Cora is tired of her situation, married to a man she does not love, and working at a diner that she wishes to own. She and Frank scheme to murder Nick in order to start a new life together without her losing the diner.  Their first attempt at the murder is a failure, but they eventually succeed.
 Leon Ames), suspects what has occurred, but doesnt have enough evidence to prove it. As a tactic intended to get Cora and Frank to turn on one another, he tries only Cora for the crime. Although they do turn against each other, a clever ploy from Coras lawyer (Hume Cronyn) prevents Coras full confession from coming into the hands of the prosecutor. With the tactic having failed to generate any new evidence for the prosecution, Cora benefits from a plea bargain in which she pleads guilty to manslaughter and receives probation.

Frank and Cora eventually patch together their tumultuous relationship, and now plan for a future together. But as they seem to be prepared finally to live "happily ever after", Cora dies in a car accident, while Frank is driving. Although it was truly an accident, the circumstances seem suspicious enough that Frank is accused of having staged the crash. He is convicted of murdering Cora and is sentenced to death.

When informed that his last chance at a reprieve from his death sentence has been denied, and thus his execution is now at hand, Frank is at first incredulous that he will be put to death for a crime of which he is innocent. But when informed that authorities have recently discovered irrefutable evidence of his guilt in the murder of Nick, Frank decides that his impending death is actually his overdue punishment for that crime, despite his official conviction being for the murder of Cora.

Frank contemplates that when a person is expecting to receive a letter, it is of no concern if at first he does not hear the postman ring the doorbell, because the postman will always ring a second time, and that second ring will invariably be heard. After they escaped legal punishment for Nicks murder, but nonetheless with Cora now dead and Frank on his way to the death chamber, he notes that the postman has indeed rung a second time for each of them.

==Cast==
* Lana Turner as Cora Smith
* John Garfield as Frank Chambers
* Cecil Kellaway as Nick Smith
* Hume Cronyn as Arthur Keats Leon Ames as Kyle Sackett
* Audrey Totter as Madge Gorland
* Alan Reed as Ezra Liam Kennedy
* Jeff York as Blair

==Production== film adaptation Double Indemnity, which included much the same moral taboos. 

==Reception==
The film was a big hit, earning $3,741,000 in the US and Canada and $1,345,000 elsewhere, recording a profit of $1,626,000.  Despite this Louis B Mayer of MGM hated the movie. 

===Critical response===
  from the trailer for the film]]

Bosley Crowther, film critic of the New York Times, gave the film a positive review and lauded the acting and direction of film, writing, "Too much cannot be said for the principals. Mr. Garfield reflects to the life the crude and confused young hobo who stumbles aimlessly into a fatal trap. And Miss Turner is remarkably effective as the cheap and uncertain blonde who has a pathetic ambition to be somebody and a pitiful notion that she can realize it through crime. Cecil Kellaway is just a bit too cozy and clean as Miss Turners middle-aged spouse. He is the only one not a Cain character, and throws a few scenes a shade out of key. But Hume Cronyn is slyly sharp and sleazy as an unscrupulous criminal lawyer, Leon Ames is tough as a district attorney and Alan Reed plays a gum-shoe role well." 
 Peyton Place Madame X that traded on her personal tragedies, but Postman, which predates all that, is a stunner—a cruel and desperate and gritty James Cain vehicle that sorely tests Lanas skills. But she succeeds marvelously, and from the first glimpse of her standing in the doorway in her white pumps, as the camera travels up her tanned legs, she becomes a character so enticingly beautiful and insidiously evil that the audience is riveted." 

The review aggregator Rotten Tomatoes reported that 95% of critics gave the film a positive review, based on eighteen reviews. 

==Adaptations==
* Le Dernier Tournant (1939 French film) directed by Pierre Chenal
* Ossessione (1943 Italian film) directed by Luchino Visconti The Postman 1981 film) directed by Bob Rafelson The Postman Always Rings Twice (1982 opera)
*Szenvedély (1997 Hungary film) directed by Fehér György Christian Petzold (in German)

==See also== 1932 film adaptation, which explore a similar theme.

==References==
 

==External links ==
 
 
*  
*  
*  
*  
*  
*  

===Streaming audio===
*   on Screen Guild Theater: June 16, 1947
*   on Hollywood Sound Stage: January 24, 1952

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 