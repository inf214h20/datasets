Suvarilladha Chiththirangal
{{Infobox film
| name = Suvarilldha chithirangal
| image = Suvarilladha Chiththirangal DVD Cover.jpg
| caption = DVD Cover
| director = K. Bhagyaraj 
| writer = K. Bhagyaraj Sudhakar K. Sumathi
| producer = K. Gopinathan
| music = Gangai Amaran
| cinematography = B. S. Pasavaraj
| editing = R. Baskaran
| studio = Bagavathy Creations
| distributor = Bagavathy Creations
| released =  
| runtime =
| country = India Tamil
| budget =
}}
 Sudhakar and Sumathi played the lead roles. The film was remade by V. Madhusudhan Rao in Telugu as Pedala Brathukulu with Sudhakar and Sumathi reprising their roles.

The Story revolves around a poor girl living with widowed mother and siblings. Poverty and the family situation gets her mother to prostitute herself for the welfare of the family. Director Bhagyaraj, treats this subject from the perspective of the girl who sees the wrong of her mother; but in the later teases the thought of right and wrong with the viewers considering the situation in which the mother gets into that.

Bhagyaraj, himself plays the neighbour who first falls in love with the girl, later to realize that she is already in a relationship with another.

==Cast== Sudhakar as Murthy
* K. Bhagyaraj as Azhagappan Sumathi as Saroja
* Goundamani as Kaliyannan
* S. Varalakshmi as Parvathy
* Kanthimathi
* Kallapatti Singaram
* Sangili Murugan
* Charulatha
* C. R. Saraswathi Chandrasekar
* Haja Shareef

==Soundtrack==
The music composed by Gangai Amaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadidum Odamaai || S. P. Balasubrahmanyam, S. P. Sailaja || Muthulingam || 3:42
|-
| 2 || Kadhal Vaibhogame || Malaysia Vasudevan, S. Janaki || Kannadasan || 3:45
|-
| 3 || Welcome || S. P. Balasubrahmanyam || Gangai Amaran || 4:52
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 