San Antonio Rose (film)
{{Infobox film
| name           = San Antonio Rose
| image          =
| image_size     =
| caption        =
| director       = Charles Lamont
| producer       = Ken Goldsmith
| writer         = Jack Lait Jr. (story) Paul Girard Smith Howard Snyder Hugh Wedlock Jr.
| narrator       =
| starring       = Jane Frazee Robert Paige Eve Arden Lon Chaney Jr.
| music          = Charles Previn
| cinematography = Stanley Cortez
| editor         = Milton Carruth
| distributor    = Universal Pictures
| released       = 20 June 1941
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
}}
 1941 United American black-and-white musical film starring Jane Frazee and featuring Lon Chaney, Jr. and Shemp Howard as a faux Abbott and Costello; it was also designed as a showcase for the then-popular vocal group The Merry Macs. The plot involves two rival groups of entertainers converging on an abandoned roadhouse with the intent to reopen it, unaware that a gangster is eyeing the property for his own scheme.

==Cast==
*Jane Frazee - Hope Holloway
*Robert Paige - Con Conway
*Eve Arden - Gabby Trent
*Lon Chaney Jr. - Jigsaw Kennedy
*Shemp Howard - Benny the Bounce
*The Merry Macs - Themselves, The Merry Macs
**Mary Lou Cook - Mona Mitchell (as Mary Lou Cook - The Merry Macs)
**Joe McMichael - Harry (as Joe McMichael - The Merry Macs)
**Ted McMichael - Ted (as Ted McMichael - The Merry Macs)
**Judd McMichael - Phil (as Judd McMichael - The Merry Macs) Richard Lane - Charles J. Willoughby
*Elaine Condos - Elaine (Dancer)
*Louis Da Pron - Alex (Dancer)
*Charles Lang - Ralph
*Riley Hill - Jimmy (as Roy Harris)
*Peter Sullivan - Don Richard Davies - Eddie
*Luis Alberni - Nick Ferris

==Soundtrack==
*"Mexican Jumping Bean"
:Music by Gene de Paul
:Lyrics by Don Raye
:Sung by The Merry Macs
*"Youre Everything Wonderful"
:Written by Henry Russell
:Sung by Jane Frazee and Eve Arden
*"Gee But Its Tough to Be a Glamour Girl"
:Written by Henry Russell
:Sung by Jane Frazee and Eve Arden
*"The Hut-Sut Song (A Swedish Rhapsody)"
:(uncredited)
:Written by Ted McMichael, Jack Owens and Leo Killion
:Sung by The Merry Macs
*"Youve Got What It Takes"
:Music by Gene de Paul
:Lyrics by Don Raye
*"Bugle Woogie Boy"
:Written by Henry Russell
*"San Antonio Rose"
:Written by Bob Wills
*"Hi, Neighbor"
:(uncredited)
:Written by Jack Owens
:Sung by Jane Frazee
*"Once Upon a Summertime"
:(uncredited)
:Lyrics by Jack Brooks
:Music by Norman Berens
*"The Old Oaken Bucket"
:(uncredited)
:Lyrics by Samuel Woodworth
:Music by George Kiallmark

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 