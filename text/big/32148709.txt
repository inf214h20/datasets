Dhan Dhana Dhan
{{Infobox film
| name = Dhan Dhana Dhan
| image =
| caption =
| director = Ramnath
| producer = Santhosh Singh  Wasim. D
| writer = Prem Kumar Sharmila Mandre Ananth Nag
| music = Sai Karthik
| cinematography =
| editing =
| distributor =
| released =  
| runtime =
| country = India
| language = Kannada
| budget =
| gross =
}} Kannada film romance genre Prem Kumar and Sharmila Mandre in the lead roles. The film is directed by Ramnath Rigvedi. K. Sai Karthik is the music director of the film. Santhosh Singh has jointly produced the film with Wasim. D. 

"Dhan Dhana Dhan" is a remake of David Dhawan directed Hindi comedy "Hum Kisise Kum Nahin" (HKKN), which had Amitabh Bachchan, Ajay Devgan, Sanjay Dutt and Aishwarya Rai in the cast.

==Cast== Prem Kumar
* Sharmila Mandre
* Ananth Nag
* Rangayana Raghu
* Ravishankar
* Adi Lokesh

==Soundtrack==
K. Sai Karthik has set the tunes to the lyrics of Kaviraj and Ramanarayan

{{Track listing
| lyrics_credits =
| extra_column = Performer(s)
| title1 = Kaavana || extra1 = Santhosh
| title2 = Ninna Kanda || extra2 = Sai Karthik
| title3 = Full Bottlu || extra3 = L. N. Shastry, Santhosh
| title4 = Dhan Dhana Dhan || extra4 = Shaan (singer)|Shaan, Anuradha Bhat
| title5 = Kavana || extra5 = Santhosh, Anuradha Bhat
}}
 

==References==
 

 
 
 


 