Their Night Out
{{Infobox film
| name =  Their Night Out
| image =
| image_size =
| caption = Harry Hughes
| producer = Harry Hughes
| writer = Arthur H. Miller  (play)   George Arthurs  (play)   Harry Hughes
| narrator =
| starring = Claude Hulbert   Renee Houston  Gus McNaughton   Binnie Barnes
| music = 
| cinematography = Walter J. Harvey 
| editing = Walter Stokvis 
| studio = British International Pictures 
| distributor = Wardour Films
| released = March 1933
| runtime = 74 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Harry Hughes and starring Claude Hulbert, Renee Houston and Gus McNaughton. It was made by British International Pictures at Elstree Studios.  The films sets were designed by the art director Duncan Sutherland.

==Cast==
* Claude Hulbert as Jimmy Oliphant 
* Renee Houston as Maggie Oliphant 
* Gus McNaughton as Fred Simpson 
* Binnie Barnes as Lola 
* Jimmy Godden as Archibald Bunting 
* Amy Veness as Gertrude Bunting 
* Judy Kelly as Betty Oliphant 
* Ben Welden as Crook 
* Hal Gordon as Sgt. Bert Simpson 
* Marie Ault as Cook  

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 

 