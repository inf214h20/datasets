Crazy Rook (2014 film)
 
{{Infobox film
| name           =  Crazy Rook 
| image          = Crazy Castle 2014 film.jpg
| image_size     = 
| caption        = 
| director       = Abolhassan Davoudi
| producer       = Bita Mansouri
| writer         = Mohammad Reza Gohari
| music          = Karen Homayounfar Sahar Hashemi  Farnoush Al-e Ahmad
| distributor    = 
| released       = 2014
| runtime        = 90 minutes
| country        =   Persian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Crazy Rook (Persian title: Rokh-e Divaneh-  )  is a 2014 Iranian film directed by Abolhassan Davoudi and starring Tannaz Tabatabei, Bizhan Emkanian, Saber Abar, Nazanin Bayati and Gohar Kheirandish. 

The film won the Best Director and the Best Film awards in the 33rd Fajr International Film Festival. 

==Cast==
*Tannaz Tabatabaei
*Saber Abar
*Amir Jadidi
*Saed Soheili
*Nazanin Bayati
*Gohar Kheirandish
*Bijan Emkanian
*Sahar Hashemi
*Amirmohammad Zand
*Reza Ahadi

==References==
 

 
 
 


 