Tattooed Flower Vase
{{Infobox film
| name           = Tattooed Flower Vase
| image          = Tattooed_Flower_Vase-1976.jpg
| caption        = Poster to Tattooed Flower Vase (1976 in film|1976)
| director       = Masaru Konuma
| producer       = 
| writer         = Kiyoharu Matsuoka
| starring       = Naomi Tani Takako Kitagawa
| music          = Yasuo Higuchi
| cinematography = Masaru Mori
| editing        = 
| distributor    = Nikkatsu
| released       = September 25, 1976 (Japan)
| runtime        = 74 minutes
| rating         =
| country        = Japan
| awards         =
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

  (1976 in film|1976) is a Japanese pink film in the Roman Porno series, starring Naomi Tani, directed by Masaru Konuma and produced by Nikkatsu. 

==Synopsis==
A traffic accident brings together the widow Michiyo and her step daughter Takako with Hideo, the young man who caused the accident. Hideo and Takako start a relationship but Michiyo finds out that Hideo is the son of the man who raped her many years earlier. After this discovery, Michiyos feelings alternate between guilt and excitement and she secretly spies on the young lovers. As atonement she decides to have her "most sensitive areas" tattooed but the pain brings her pleasure. The denouement has Hideo lusting after Michiyo and raping her as his father had done.

==Cast==
* Naomi Tani as Michiyo
* Takako Kitagawa as Takako
* Shin Nakamaru as Hideo
* Genshu Hanayagi
* Mami Yuki

==Availability== Kino International region 1 DVD release of Tattooed Flower Vase simultaneously with three other Masaru Konuma Roman Porno films, scheduled for November 6, 2007. 

==See also==
* List of Nikkatsu Roman Porno films

==Notes==
 

== Sources ==
*  
* 
*  
*  
*  

== External links ==
*  

 

<!--
 
-->

 
 
 
 
 


 
 