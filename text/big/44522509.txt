I Don't Know Much, But I'll Say Everything
{{Infobox film
| name           = I Dont Know Much, But Ill Say Everything 
| image          = 
| border         = 
| caption        = 
| director       = Pierre Richard
| producer       = Henri Brichetti
| writer         = 
| screenplay     = Pierre Richard Didier Kaminka
| starring       = Pierre Richard Bernard Blier Georges Beller Daniel Prévost Pierre Tornade
| music          = Michel Fugain 
| cinematography = Pierre Lhomme
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
I Dont Know Much, But Ill Say Everything ( ) is a 1973 French comedy film directed by Pierre Richard.

== Plot ==
Pierre Gastié-Leroy (Pierre Richard) is the son of a wealthy director of a factory of weapon manufacturing (Bernard Blier). Despite his parents, two generous uncles and a bishop godfather who try to inculcate him the rigid values of his social level, Pierre is a dreamer, antimilitaristic, social educator who dreams of saving three thugs, his "little guys" at the limit of delinquency. After several resounding failures that sent him to prison, Pierre is ordered by his father to join his factory to direct the social service. Tired of the venality of his father and the foolishness of the "little guys", Pierre hires them at the factory. They will have fun making mischief and being overzealous to convince the supervisors on increasing the working rhythms, denouncing the trade union leaders, battling a strike and finally, stealing 500 tanks to sell them back to the black market. A demonstration of new remote-controlled missiles attended by the Minister for Defence turns into fiasco. Injured in his pride, the father Gastié-Leroy wants to show the reliability of his product by pointing the fire at his own factory.

== Cast ==
* Pierre Richard as Pierre Gastié-Leroy
* Bernard Blier as Monsieur Gastié-Leroy
* Didier Kaminka as Didier
* Luis Rego as Luis
* Georges Beller as Georges
* Pierre Tornade as the police commissioner
* Daniel Prévost as Morel
* Danièle Minazzoli as Danou, the nurse
* Nicole Jamet as Nicole
* Hélène Duc as Madame Gastié-Leroy
* Francis Lax as Antoine
* Pierre Repp as Vernier, the factory director
* Jean Obé as Oncle Léon, godfather of Philippe
* André Thorent as Oncle Jean
* Michel Delahaye as Oncle Paul
* Xavier Depraz as Général Deglane
* Jean Saudray as Morin
* François Cadet as Félix
* Victor Lanoux as a laborer
* Teddy Vrignault as Staflikevitch, the Bulgarian
* André Gaillard as the Social Security employee
* France Rumilly as Laurence Deglane

== External links ==
* 

 
 
 
 


 
 