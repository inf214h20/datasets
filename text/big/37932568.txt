Archanai Pookal
 
 
{{Infobox film
| name           = Archanai Pookal
| image          =
| caption        =
| director       = Gokula Krishnan
| producer       = K. Gopinathan
| writer         = Gokula Krishnan
| starring       = Chandrasekar, Mohan (actor)|Chandrasekar, Mohan Rajalakshmi, Subathra (actress)|Rajalakshmi, Subathra Sangili Murugan Poornam Viswanathan Manorama Senthil Master khaja sherif S.Ramadoss Anu Mohan
| music          = Ilaiyaraaja
| cinematography = Jeyanan
| editing        = T.Sekar
| studio         = Bagavathy Creations
| distributor    = Bagavathy Creations
| released       =  
| country        = India Tamil
}}
   1982 Cinema Indian Tamil Tamil film, directed by Gokula Krishnan, starring Chandrasekar, Mohan, Rajalakshmi and Subathra in lead roles. The film had musical score by Ilaiyaraaja and was released on 1982.  

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aavaram Kaatukul || Malaysia Vasudevan, Uma Ramanan || Kannadasan || 04:16
|-
| 2 || Kaveriye || S. P. Balasubrahmanyam, S. Janaki || Gangai Amaran || 04:17
|- Vaali (poet)|Vaali || 04:43
|-
| 4 || Vazhimel Vizhiyaal || S. Janaki || 04:23
|}

==References==
 

 
 
 
 


 