Kentucky Blue Streak
{{Infobox film
| name           = Kentucky Blue Streak
| image          =
| image_size     =
| caption        =
| director       = Bernard B. Ray
| producer       = C.C. Burr
| writer         = Rose Gordon (story and screenplay) Edward OBrien (special dialogue)
| narrator       =
| starring       = See below Ben Carter Lee Zahler
| cinematography = Irvin Akers
| editing        = Tony Martinelli
| distributor    =
| released       = 1 May 1935
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Kentucky Blue Streak is a 1935 American film directed by Bernard B. Ray.

The film is also known as The Blue Streak in the United Kingdom.

== Plot summary ==
 

== Cast ==
*Edward J. Nugent as Martin Marion
*Frank Coghlan Jr. as Johnny Bradley Patricia Scott as Mary Bradley
*Cornelius Keefe as District Attorney Barton Pierce
*Margaret Mann as Mrs. Martha Bradley
*Roy DArcy as Harry Johnson
*Ben Holmes as Bellhop ODonnell
*Roy Watson as Colonel Seymour Harry Harvey as Barker / Voice of Radio Announcer Roger Williams as Deputy
*Joseph W. Girard as Warden Carlson
*Walter Downing as Editor Ben Carter as Waiter / Colored Octette leader

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 