Black Moon Rising
{{Infobox film
| name           = Black Moon Rising
| image          = Black Moon Rising (1986 film) poster art.jpg
| image size     =
| caption        = Theatrical release poster by Steven Chorney
| director       = Harley Cokeliss
| producer       = Douglas Curtis
| writer         = John Carpenter  William Gray  Desmond Nakano
| starring = {{Plainlist|
* Tommy Lee Jones
* Linda Hamilton
* Robert Vaughn
* Richard Jaeckel
* Bubba Smith
}}
| music          = Lalo Schifrin
| cinematography = Misha Suslov
| editing        = Todd C. Ramsay	
| distributor    = New World Pictures
| released       =  
| runtime        = 100 min.
| country        = United States English
| gross          = $6,500,000
}}
Black Moon Rising is an action motion picture made in 1986 directed by Harley Cokeliss, written by John Carpenter and starring Tommy Lee Jones, Linda Hamilton and Robert Vaughn, plus Keenan Wynn in his final screen role. The focus of the film was the theft of a prototype vehicle called the Black Moon.

==Plot== cassette with vital information, Sam Quint, a former CIA agent (Jones), runs into Marvin Ringer (Lee Ving), another hitman he replaced, who is after the same cassette. In a desperate attempt to evade Ringer and get the tape back to a government official (played by Bubba Smith), Quint hides the cassette in a 300-MPH prototype vehicle, the Black Moon, which is on a transporter destined for a public presentation in Los Angeles.

Before Quint can recover the cassette, Nina (Hamilton) steals the Black Moon, with Quint on her tail, and returns it to a warehouse, the headquarters for the car thief ring headed by Ed Ryland (Vaughn). Quint must lure Nina into helping him recover the Black Moon, with the help of its driver and his crew, and make it out alive with the cassette and the car before the next day.

==Cast==
* Tommy Lee Jones as Quint
* Linda Hamilton as Nina
* Robert Vaughn as Ryland
* Bubba Smith as Johnson
* Keenan Wynn as Iron John
* Lee Ving as Ringer

==Car==
The Black Moon was based on the 1980 Wingho Concordia II designed by Bernard Beaujardins and Clyde Kwok, made by Wingho Auto Classique in Montreal. {{cite web|url=http://www.928spyder.com/En/ContentCD.html
|accessdate=2008-03-28
|title=Design Section
|publisher=Wingho Auto Classique, Inc.
|date=2007-02-22
}}  {{cite web|url=http://www.mcgill.ca/news/2001/summer/newsbites/
|accessdate=2008-03-28
|year=2001
|title=Newsbites - The Most Beautiful Car in the World
|publisher=McGill University
}}  Only one of these had been built, so in the movie, a copy of the car cast from a mold was used for stunts, as well as a third replica of the interior only.<!-- this info appears in the 1986 Behind the Scenes promotional clip which appear on youtube; however, the origin of the footage is needed to cite it. 
por gentileza traduza esta pagina. agradeço a oprtunidade -->

==See also==
* List of American films of 1986

== References ==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 

 
 