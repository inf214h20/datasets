Quick (2011 film)
{{Infobox film
| name           = Quick
| image          = Quick-poster.jpg
| caption        = Promotion poster for Quick
| film name      = {{Film name
| hangul         =   
| rr             = Qwik 
| mr             = Kwik}}
| director       = Jo Beom-goo  
| producer       = Lee Sang-yong   Yoon Je-kyoon    Gil Young-min   Lee Han-seung
| writer         = Park Su-jin
| starring       = Lee Min-ki Kang Ye-won  Kim In-kwon
| music          = Dalparan
| cinematography = Kim Young-ho
| editing        = Shin Min-kyung
| studio         = JK Film
| distributor    = CJ Entertainment
| released       =    
| runtime        = 115 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
Quick ( ) is a 2011 South Korean contemporary action comedy film.  

==Plot==
Seoul, 2004. A group of bikers are joy-riding through the streets and while their leader the teenage Han Ki-su (Lee Min-ki) is tearfully berated by girlfriend Chun-shim (Kang Ye-won) for scorning her. The biker Kim Myung-shik (Kim In-kwon) is attracted to Chun-shim watches dolefully. Following some heavy traffic, Ki-su executes a perfect bike jump over it. 
 NPCC team leader Kim (Ju Jin-mo) examine the CCTV tape in the building that exploded and believe that Ki-su is potentially the bomber. Ki-su delivers Chun-shim to the concert just in time where she performs in the helmet. The two of them start to make the deliveries, while being hunted by the police and trying to solve figure out who is responsible for the bombings while driving between Seoul and Incheon.

==Cast==
* Lee Min-ki - Han Ki-su 
* Kang Ye-won - Chun-shim / Ah-rom
* Kim In-kwon - Kim Myung-shik 
* Ko Chang-seok - Detective Seo
* Yoon Je-moon - Jung In-hyuk
* Ju Jin-mo - Team leader Kim
* Ma Dong-seok - Kim Joo-chul
* Song Jae-ho - Kwak Han-soo
* Oh Jung-se - Park Dal-yong  Kim Tae-woo - Junichi Watanabe 
* Jeon Kuk-hwan - Masaaki Aikawa
* Choi Jae-sup - Manager

==Reception==
The film ranked third and grossed over   in its first week of release.  and grossed a total over   after seven weeks of screening. 

Film Business Asia gave the film a seven out of ten rating, comparing it to the action comedy films produced by Luc Besson, opining that it was an "action movie thats fast, furious and (thankfully) funny".   

==References==
 

==External links==
*    
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 
 
 