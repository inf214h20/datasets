Tangiers (1982 film)
Tangier Moroccan thriller film directed by Michael E. Briant and starring Ronny Cox, Billie Whitelaw and Glynis Barber. 

"Take the disappearance of a key British Intelligence Officer in Gibraltar, loaded with top secrets. Add a tough, down-on-his-luck ex-CIA agent with a murky past. Try a drop of blackmail. Garnish with two beautiful women. Sprinkle liberally with murder, treachery and mayhem. Stir well till it all fizzes with danger and excitement that is Tangier. The result is a thriller that will grip you down to the last explosive moment. Its the thriller of the year. Its Tangier."
* from the cover of the Linked Ring VHS release 1982.

==Cast==
* Ronny Cox ...  Bob Steele 
* Billie Whitelaw ...  Louise 
* Glynis Barber ...  Beth 
* Ronald Lacey ...  Wedderburn 
* Oscar Quitak ...  Velatti  Jack Watson ...  Donovan  Ronald Fraser ...  Jenkins 
* Adel Frej ...  Ahmed 
* Benjamin Feitelson ...  Fisher 
* Peter Arne ...  Malen 
* David Collings ...  Major Greville 
* Connie Mason ...  Marsha 
* Nicholson Donnelly ...  Major Crawley Ronald Fraser ...  Guest appearance

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 