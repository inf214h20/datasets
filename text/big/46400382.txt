Robin des bois, la véritable histoire
 
{{Infobox film
| name           = Robin des bois, la véritable histoire
| image          = 
| border         = 
| caption        = 
| director       = Anthony Marciano
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    =
| released       = 15 april 2015
| runtime        = 
| country        = France French
| budget         =
}}

Robin des bois, la véritable histoire  (literally Robin Hood, the true story) is a 2015 French comedy film. 

==Plot==
Robin Hood is a bad guy. He and his accomplice Tuck have a clear ethical in life : they fly as the poor, women or older. The rest ? Too risky. But even the bad guys have dreams and theirs is to buy the brothel run most of the city, the Pussycat. Robin, who stop at nothing when it comes to get rich, then decided to get the money where it is located and plans to rob the cash taxes Nottingham. But his meeting with Sherwood gang of vigilantes who rob them the rich to give to the poor, will thwart his plans. Little John, Marianne and their friends have in fact had exactly the same idea as him rob the Sheriff of Nottingham. The - true - story of Robin Hood can finally begin !

==Cast==
* )  {{cite web|url=http://www.allocine.fr/film/fichefilm-229993/casting/
|title=Casting La Véritable Histoire de Robin des Bois
|website=AlloCiné
|accessdate=18 December 2014
}} 
*  
* )
*Patrick Timsit : Alfred
* )
*Géraldine Nakache : Marianne
*Jaouen Gouévic : Raoul
*Benjamin Blanchy : lemployé de Robin   
*Antoine Khorsand : Petit Prince 
*Éric et Quentin : Gaston et Firmin
*M. Pokora: Robin des Bois (Caméo)

==Reception==
In France, the film received generally negative reviews. It achieves a score of 1.9 / 5 on AlloCiné.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 