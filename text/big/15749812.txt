The Kautokeino Rebellion
 
{{Infobox film
| name           = The Kautokeino Rebellion
| image          = The Kautokeino Rebellion.jpg
| caption        = Norwegian theatrical release poster
| director       = Nils Gaup
| producer       = Tove Kløvvik
| writer         = Nils Isak Eira, Nils Gaup
| starring       = Nikolaj Coster-Waldau Mikael Persbrandt Michael Nyqvist Nils Gaup Mikkel Gaup Anni-Kristiina Juuso
| music          = Mari Boine Svein Schultz Herman Rundberg
| cinematography = Philip Øgaard
| editing        = Jan-Olof Svarvar
| studio         = Rubicon
| released       = 18 January 2008
| runtime        = 96 minutes
| country        = Norway Sami  Norwegian  Swedish
| NOK
}}
 Kautokeino riots in Kautokeino, Norway in 1852 in response to the Norwegian exploitation of the Sami community at that time.  It is directed by  Nils Gaup and was released in January, 2008. Music to this movie was mostly composed by Sami musician - Mari Boine.

==Cast==
* Mikael Persbrandt as Carl Johan Ruth
* Michael Nyqvist as Lars Levi Læstadius
* Nils Peder Isaksen Gaup as Mons Somby
* Mikkel Gaup as Aslak Hætta
* Anni-Kristiina Juuso as Ellen Aslaksdatter Skum
* Jørgen Langhelle Pastor Stockfleth
* Stig Henrik Hoff Peter Andersson as Lars Johan Bucht
* Silje Holtet as Anne Elise Blix
* Eirik Junge Eliassen as Prästen Zetliz Mathis Hætta
* Inger Utsi as Inger Andersdatter Spein
* Ole Nicklas Guttorm as Litle Aslak (son)
* Inga Juuso as Grandmother Rasmus Spein Lars Hætta
* Nikolaj Coster Waldau as Bishop Juell

==Soundtrack==
A four track soundtrack CD with music by Mari Boine, Svein Schultz and Herman Rundberg was released by Sony/ATV Music Publishing Scandinavia.

===Track list===
#Elen Skum
#Válddi vuoigna - The Spirit of Power
#Deaivideapmi - Confrontation
#Doaivut ja vuoimmehuvvat - Hope and Defeat

==See also==
* Sami revolt in Guovdageaidnu

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 

 