Make Mine Laughs
{{Infobox film
| name           = Make Mine Laughs
| image	         = Make Mine Laughs FilmPoster.jpeg
| image_size     = 
| alt            =
| caption        = Theatrical release poster
| director       = Richard Fleischer
| writer         = 
| narrator       = 
| starring       = Joan Davis
| music          = 
| cinematography = Robert De Grasse
| editing        = 
| distributor    = RKO Radio
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Make Mine Laughs is a 1949 film directed by Richard Fleischer. The film was a compilation of comic scenes and musical numbers from older and newer RKO films.

Make Mine Laughs was the second of three RKO Radio "pastiche" films, composed largely of musical and comedy highlights from previous RKO productions. Comedian Gil Lamb hosts the proceedings, finding time to make satirical comments about the opening credits and to perform his "swallowing the harmonica" specialty. The filmclips include two short subjects, Leon Errols Beware of Redheads and one of RKOs Flicker Flashbacks entries, both presented in their entirety. Also featured are Frances Langford singing "Moonlight Over the Islands" (from Bamboo Blonde), Anne Shirley and Dennis Day duetting on "If You Happen to Find a Heart" (from Music in Manhattan), and specialties performed by the likes of orchestra leaders Frankie Carle and Freddie "Schnickelfritz" Fisher, ventriloquist Robert Lamouret, and dance teams Manuel & Marita Viera and Rosario & Antonio. Make Mine Laughs was withdrawn from distribution after Ray Bolger and Jack Haley brought suit against RKO for unauthorized use of clips from Bolgers boxing pantomime in Four Jacks and Jill (1944) and Haleys "Who Killed Vaudeville?" number from George Whites Scandals (1945).

After problems with the copyright of some of the material used in the movie, RKO removed the movie from general release and shelved it.

==References==
 

==External links==
*  

 

 
 
 
 
 


 