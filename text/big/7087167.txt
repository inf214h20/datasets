The Country Girl (1954 film)
{{Infobox film
| name           = The Country Girl
| image          = The_country_girl.jpg
| image_size     = 225px
| border         = yes
| caption        = theatrical release poster
| director       = George Seaton
| producer       = William Perlberg
| writer         = George Seaton the play by Clifford Odets
| starring       = {{Plainlist|
* Bing Crosby
* Grace Kelly
* William Holden
}}
| music          = Victor Young
| cinematography = John F. Warren
| editing        = Ellsworth Hoagland   
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 104 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = $6.5 million (est. US/ Canada rentals) 
}}
 of the Academy Award for Best Writing, Adapted Screenplay. It was entered in the 1955 Cannes Film Festival.   
 Oscar for Broadway production.  The role, a non-glamorous departure for Kelly, was as the alcoholic actors long-suffering wife.
 A Star Is Born. NBC even sent a camera crew to Garlands hospital room, where she was recuperating from the birth of her son, in order to conduct a live interview with her if she won. The win by Kelly instead famously prompted Groucho Marx to send Garland a telegram stating it was "the biggest robbery since Great Brinks Robbery|Brinks."

Given the period of its production, the film is notable for its realistic, frank dialogue and honest treatments of the surreptitious side of alcoholism and post-divorce misogyny. 

==Plot==
In a theatre where auditions are being held for a new musical production, the director, Bernie Dodd, watches a number performed by fading star Frank Elgin and suggests he be cast. This is met with strong opposition from Cook, the shows producer.

Bernie insists on the down-on-his-luck Frank Elgin, who is living in a modest apartment with his wife Georgie. They are grateful, though not entirely certain Frank can handle the work.

Initially Frank leads Bernie to believe that Georgie is the reason for his career decline. Bernie strongly criticizes her, first behind her back and eventually to her face. What he doesnt know is that the real reason Franks career has ended is his insecurity. When their five-year-old son Johnny was hit by a car while in his care, Frank was devastated by the death and, partly using that as an excuse to cover up his insecurity, reduced to a suicidal alcoholic. 
 suicidal and a drunk, when it is actually Frank who is both.

Humiliated when he learns the truth, Bernie realizes that behind his hatred of Georgie was a strong attraction to her. He kisses her and falls in love.

Elgin succeeds in the role on opening night. Afterward he demands respect from the producer that he and his wife had not been given previously. At a party to celebrate, Bernie believes that now that Elgin has recovered his self-respect and stature, Georgie will be free to leave him. But she stands by her husband instead.

==Cast==
  
* Bing Crosby as Frank Elgin
* Grace Kelly as Georgie Elgin
* William Holden as Bernie Dodd  
* Anthony Ross as Philip Cook
* Gene Reynolds as Larry
 
* Jacqueline Fontaine as Lounge singer
* Eddie Ryder as Ed Robert Kent as Paul Unger
* John W. Reynolds as Henry Johnson
* Victor Young as Recording studio conductor
 

==Awards and honors== Academy Awards===
The Country Girl  won two Academy Awards.   
{| class="wikitable" border="1"
|-
! Award !! Result !! Winner
|- Best Motion Picture ||   || William Perlberg  Winner was Sam Spiegel On the Waterfront 
|- Best Director ||   || George Seaton   Winner was Elia Kazan On the Waterfront 
|- Best Actor ||   || Bing Crosby  Winner was Marlon Brando On the Waterfront 
|- Best Actress ||   || Grace Kelly
|- Best Writing, Screenplay ||   || George Seaton
|- Best Art Richard Day On the Waterfront 
|- Best Cinematography (Black-and-White) ||   || John F. Warren  Winner was Boris Kaufman On the Waterfront 
|-
|}

==In popular culture== Mika song, Grace Kelly (song)|"Grace Kelly".

==References==
Notes
 

==External links==
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 