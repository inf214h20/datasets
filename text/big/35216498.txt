High Street (film)
{{Infobox film
| name           = High Street
| image          = 
| caption        = 
| director       = André Ernotte
| producer       = Pierre Drouot Alain C. Guilleaume
| writer         = André Ernotte Elliot Tiber
| starring       = Annie Cordy
| music          = 
| cinematography = Walther van den Ende
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Belgium
| language       = French
| budget         = 
}}
 Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Annie Cordy as Mimi
* Mort Shuman as Painter David Reinhardt
* Bert Struys as Lhomme
* Guy Verda as Gérard
* Anne Marisse as Sandra
* Elliot Tiber as Mike
* Nadia Gary as Valérie
* Raymond Peira as Le docteur

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Belgian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 