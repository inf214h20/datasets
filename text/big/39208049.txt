Wild Honey (film)
for the 1918 film of the same title, see Wild Honey (1918 film).
{{infobox film
| name           = Wild Honey
| image          =Wild Honey (1922) - 1.jpg
| imagesize      =190px
| caption        =Robert Ellis, Priscilla Dean, and Wallace Beery
| director       = Wesley Ruggles
| producer       = Carl Laemmle
| based on       =  
| writer         = Lucien Hubbard (scenario)
| starring       = {{plainlist|*Priscilla Dean
*Noah Beery, Sr.
*Wallace Beery}}
| music          =
| cinematography = Harry Thorpe
| editing        =
| distributor    = Universal Film Manufacturing Company
| released       =   (New York) 
| runtime        = 71 minute
| country        = United States
| language       = Silent (English intertitles)
}}
Wild Honey is a 1922 American silent film produced and distributed by the Universal Film Manufacturing Company and directed by Wesley Ruggles. Based on a book of the same title by Cynthia Stockley and starring Priscilla Dean,  the film also features Noah Beery, Sr. and Wallace Beery in supporting roles. It is notable for the first use of a traveling matte special effect.

Its survival status is classified as unknown, suggesting that it is a lost film.  

==Synopsis==
Despite her fathers debt to him, Lady Vivienne (Priscilla Dean) refuses to marry the wealthy but villainous Henry Porthen (Noah Beery). Porthen devises a plot to lure Vivienne to his country home using her weak-willed friend, Freddy (Lloyd Whitlock). In the course of events, Vivienne faints, Porthen is killed by his secretary Joan (Helen Raymond), and Freddy runs away for fear that he will be blamed.
 Transvaal to investigate some problem property she owns. She is rescued from bandits by homesteader Kerry Burgess (George Ellis) and the two fall in love. More intrigue brought about by Viviennes rejection of another suitor, Wolf Montague (Landers Stevens), leads to the sabotage of a dam and a destructive flood. Vivienne tries to warn the settlers in the floods path and is herself swept up in it. Burgess rescues her again and they are united.

==Cast==
 
*Priscilla Dean – Lady Vivienne
*Noah Beery, Sr. – Henry Porthen
*Lloyd Whitlock – Freddy Sutherland
*Raymond Blathwayt – Sir Hugh
*Percy Challenger – Ebenezer Learnish
*Helen Raymond – Joan Rudd
*Landers Stevens – Wolf Montague Robert Ellis – Kerry Burgess
*Wallace Beery – Buck Roper
*Carl Stockdale – Liverpool Blondie
*Christian J. Frank – Repington
*Harry DeRoy – Koos
 

==Production==
Cynthia Stockleys novel Wild Honey was purchased by Universal in 1921 with Priscilla Dean already in mind.  Brothers Wallace and Noah Beery appeared for the first time in the same feature film. 
 matte process its inventor) was used. The action of the players was filmed against a black screen, and a scene in miniature of a bursting dam and consequent flood was filmed separately, then the two were combined by the process. 

==Release== Central Theatre on February 27, 1922. 

Reviews were mostly negative, but many critics singled out the flood scene as impressive and some regarded it as worth the price of admission.  The Variety review expressed the opinion that the movie was cheaply made and that, except for the flood scene, the production suffered as a result.  Reviewing the film for Life, Robert E. Sherwood called it "a pitifully weak piece of work".  The capsule review in Photoplay labeled it "as dull an evenings entertainment as you can find anywhere". 

==References==
{{reflist|30em|refs=
   
   
   
   
   
   
   
   
   
   
}}

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 