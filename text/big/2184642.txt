Into the Blue (2005 film)
 
{{Infobox film
| name     = Into the Blue
| image    = Into the Blue poster.jpg
| caption  = Theatrical release poster John Stockwell
| producer = David Zelon
| writer   = Matt Johnson
| starring = Paul Walker Jessica Alba
| music    = Paul Haslinger
| cinematography = Shane Hurlbut Peter Zuccarini
| editing  = Nicolas De Toth Dennis Virkler
| studio = Mandalay Pictures
| distributor = Metro-Goldwyn-Mayer Columbia Pictures 
| released =  
| runtime  = 110 minutes
| language = English
| budget   = $50 million  
| country  = United States
| gross    = $44,434,439    
}} John Stockwell, and distributed by Metro-Goldwyn-Mayer and Columbia Pictures.

==Plot==
Jared (Paul Walker) and Sam (Jessica Alba) are two lovers living a rustic life in a trailer, next to the beach in the Bahamas. Sam works as a guide in the local aquatic theme park, "Atlantis", and Jared has worked a number of odd jobs in his field of passion, diving. His real dream, though, is finding one of many merchant and pirate ships lost in the waters around the Bahamas, many of them with large amounts of gold on board. Derek Bates has similar dreams and a better boat, but Jared is not interested in working for him.

Jareds friend Bryce (Scott Caan) and his girlfriend Amanda (Ashley Scott) come to visit, as Bryce, a lawyer in New York City, has acquired the use of a luxury vacation house from a client he defended. While snorkeling, Jared comes across artifacts that seem to stem from a ship wreck. The four of them investigate and find several other pieces that turn out to be the remains of a legendary French pirate ship, the Zephyr, lost as the pirate made his getaway. They also discover a crashed plane and its cargo of cocaine.

Without Jareds knowledge, Bryce and Amanda come up with a plan to retrieve some of the cocaine from the sunken plane and sell it to local night club owner Primo (Tyson Beckford). Unfortunately, Primo turns out to be an associate of drug lord Reyes (James Frain) to whom the cocaine belonged in the first place. Reyes, wanting his cocaine back, threatens Jared, Amanda and Bryce at gunpoint to bring him his cocaine or die.

Back on land, the trio breaks the bad news to Sam, who is heartbroken that Jared would violate his principles and work for drug lords. He tries to convince her that its okay and that its a one-time thing, but she leaves him, saying that they are over.

As they are moving the cocaine packs from the plane to their surface boat, Amanda is attacked by a tiger shark. She is brought to the hospital where she dies due to a huge bite out of her left leg. Hearing of the tragedy, Sam comes to the hospital and she and Jared are reunited.

Meanwhile, Sam insists on going to the police, and goes to the private home of one of their friends, a local cop named Roy (Dwayne Adway). Bryce drops her off, but thinking theyll get busted, heads off to meet Jared.

The Sea Robin heads for the location where Jared directs them, but as they approach the area, they see a white substance floating on the surface. Bryce has switched places with one of the dead pilots, and they use this to mount a surprise attack. An underwater struggle follows, killing all of Bates men and wounding Bryce.

Below, Jared and Bates are the only ones left. Jared uses an air tank as a missile by hammering off the valve. Bates escapes it, but as it hits the fuel tank at the back of the plane, it causes the entire plane to explode, killing Bates. Sam jumps into the water, fearing for Jareds life.

Six weeks later, the trio is salvaging the Zephyr. After trying to bring an old cannon atop fails and the cannon sinks back down to the bottom, breaking the part of ship. Jared is ready to call it a night, but Bryce dives in again and shouts that he has found gold.

==Cast==
*Paul Walker as Jared
*Jessica Alba as Sam
*Scott Caan as Bryce
*Ashley Scott as Amanda
*Josh Brolin as Derek Bates
*James Frain as Reyes
*Tyson Beckford as Primo

==Filming== fins and a dive mask flung to him by Alba. During filming he missed the mask. It was never seen again after hitting the water and the DVD suggests that it was taken by a shark.

==Reception==

===Box office===
Into the Blue was a box office flop. Produced on a budget of $50 million, the film made just $18 million in North America and $26 million internationally for a total of $44 million worldwide. 

===Critical response===
The film received negative reviews from critics.   lists a rating of 45 out of 100 based on 28 reviews, which indicates "mixed or average reviews". 

===Awards=== Fantastic Four), Dirty Love. 

==Home media==
Into the Blue was released on VHS and DVD-Video on December 26, 2005 (it was the final MGM film released on VHS). It was released on Blu-Ray on August 15, 2006.

== Soundtrack ==
*"Good Old Days" – Ziggy Marley
*"I Will" – Holly Palmer
*"Ill Be" – O S Xperience
*"Time of Our Lives (Swiss-American Federation Remix)" – Paul van Dyk feat. Vega 4
*"Think It Matters" – Paul Haslinger & Dan di Prima
*"Clav Dub" – Rhombus
*"No Trouble" – Shawn Barry
*"Whoa Now" – Louque
*"VIP" – D Bo
*"J.O.D.D." – Trick Daddy feat. Khia & Tampa Tony
*"Papi" – The Jackson 2
*"Its Alright" – Guerilla Black
*"Of Course Nigga You Can" – Billy Steel
*"Perique" – Louque
*"Wonderful World, Beautiful People" – Jimmy Cliff
*"Remember" – Abdel Wright

==Sequel==
A sequel,  , has been released on direct to DVD status. The cast consists of Chris Carmack, Laura Vandervoort, Audrina Patridge and Mircea Monroe.

==References==
 

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 