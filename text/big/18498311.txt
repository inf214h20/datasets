Wonder of It All (1974 film)
 
{{Infobox Film
| name           = Wonder of It All
| image          = Wonder of It All 1974 film.png
| image_size     = 150px
| caption        = Wonder of It All theatrical poster
| director       = Arthur R. Dubs
| producer       = 
| writer         = James T. Flocker 
| starring       = Les Biegel (narrator)
| music          = 
| cinematography = 
| editing        = 
| distributor    = Pacific International Enterprises
| released       = January 1974
| runtime        = 95 min. 
| country        = U.S.A. English
| budget         = 
| followed_by    = 
}}

Wonder of It All is a 1974 nature documentary film that was produced by Pacific International Enterprises.

==Film description==
The film takes the viewer around the world to show the animals of the world in their natural habitats showing them in their day-to-day activities. This includes not only with their own kind  but interacting and coexisting with other species. The film is directed by Arthur R. Dubs with narration by Les Biegel in this family film that involved seven years of filming. 

 
==DVD details==
*Release date: January 1, 2003 
*Full Screen 
*Region: 1
*Aspect Ratio: 1.33:1 
*Audio tracks: English  
*Subtitles: English, Spanish 
*Running time: 95 minutes

==References==
 

==See also==
* True Life Adventures
* Grand Canyon (1958 film)
* Pacific International Enterprises

==External links==
*  
*  
*  
*  

 
 
 
 
 

 