9 (2005 film)
{{Infobox film
 | name           = 9
 | image          = 9 poster.jpg
 | caption        = Poster for the short film
 | director       = Shane Acker
 | producer       = Shane Acker
 | writer         = Shane Acker
 | narrator       = 
 | starring       = 
 | music          = Earganic
 | cinematography = 
 | editing        =
 | studio         = UCLAs Animation Workshop
 | distributor    = Thinkart Films
 | released       =  
 | runtime        = 11 minutes
 | country        = United States
 | language       = 
 | budget         = 
 | gross          = 
 }}
9 is a 2005  , although it did win a Student Academy Award for Best Animation.

==Plot== sentient rag armature of talisman which it holds in its claws. Sitting quietly, 9 stares into the mirrored surface of his own strange talisman and has a flashback.

In the flashback, 9 searches the ruins with his mentor 5, a one-eyed rag doll. They gather useful bits and pieces and store them in the cloth linings of their own skin. With 9s help, 5 is able to salvage a light bulb and operate it using pieces of wire. 5 gives the functional bulb to 9 for safe-keeping.

Almost immediately after, 5 draws the mirrored talisman from inside his chest.  It glows green, warning of danger.  5 gives the talisman to 9 then pushes him into cover. Extending a homemade folding spear, 5 steps out to confront the threat.  But the Cat Beast circles behind him and snatches him up with a metal pincer, shearing off his right arm.  Though he struggles, 5 can not break free, and the Cat Beast sucks 5s soul out through his mouth using its talisman leaving 5 lifeless. Horrified, 9 escapes in panic, almost giving away his location to the Cat Beast by scraping the light bulb against a rock.

9 is woken from the memory by the warning green glow of the mirrored talisman. Taking the light bulb, which he has attached to the end of a staff, 9 flees into a ruined house. The Cat Beast follows and we see that it has not just taken the other rag dolls souls: it also wears their numbered skins like a garment. The number 5 shows prominently on its back.

The Beast pounces on what it thinks is 9, but which turns out to be a marionette&nbsp;– the dummy created by 9 at the beginning of the film. The Beasts claws stick in the tar and 9 is able to hop onto its back and steal the Beasts talisman. 9 leads the Beast on a panicked chase through the house and finally runs out onto the end of a broken plank, which extends over a several-story drop.  Thinking it has 9 at bay, the Beast walks out onto the plank.  But it has fallen into 9s trap.

Nearby is the metal armature, and the upright book.  9 leaps off the plank onto the armature, which swings him around to the book.  He kicks it and it falls into the pit.  The string, tied at one end to the book, has its other end tied to the plank.  The weight of the falling book drags the plank into the pit, and the Beast with it.  The Beast crashes through a hole in the cellar floor and is presumably impaled by the sharp end of the falling plank. This all reveals that all the things that 9 has built was a cleverly made trap for the Beast.

Now free from fear, 9 salvages the skins of the other rag dolls and prepares them to be ceremonially burned. As he looks sadly at the skin of 5, the two talismans begin to glow. 9 realizes that they are two halves of a whole and puts them together. A beam of green light erupts from the united talisman, and the spirits of the eight slain rag dolls 1, 2, 3, 4, 5, 6, 7 and 8 emerge, returning to their rag doll skins to be at peace. Before disappearing, 5s soul turns to 9 and nods in approval.

In the morning, when the ashes have cooled, 9&nbsp;– bearing the light bulb staff, a symbol of the persistent light of knowledge and learning&nbsp;– walks off into the wasteland, leaving the empty talisman behind in the sand.

==Production== Maya 1.5&ndash;5.5 Photoshop for After Effects Premiere for rendered at lighters that worked on the film. The music was provided by Eric Olsen and his band the Earganic.

==Awards==
;Awarded 
* Student Academy Award&nbsp;– Gold Award for Animation
* SIGGRAPH&nbsp;– Best in Show
* Animex International Festival of Animation and Computer Games|Animex&nbsp;– First Prize, 3D Character Animation
* Academy of Television Arts & Sciences Foundation College Awards&nbsp;– First Prize, Non-traditional Animation
* Florida Film Festival, Newport Beach Film Festival&nbsp;– Best Animated Short

;Nominated 
*  

==Feature film==
 
9 is a computer animated feature film (which the short film was released with the DVD release of) adapted from the short film. It was produced in part by Tim Burton, Timur Bekmambetov, and Jim Lemley, and released on September 9, 2009 (09/09/09) by Focus Features.

Shane Acker directed the film and wrote the original story. Pamela Pettler wrote the script. The lead voice actors are Elijah Wood, John C. Reilly, Jennifer Connelly, Crispin Glover, Martin Landau, Christopher Plummer, and Fred Tatasciore.  

==See also==
*List of animated feature films
*List of science-fiction films

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 