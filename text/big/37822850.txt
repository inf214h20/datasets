The Labour Leader
{{Infobox film
| name           = The Labour Leader
| image          =
| caption        =
| director       = Thomas Bentley
| producer       = 
| writer         = Kenelm Foss Fred Groves Fay Compton Owen Nares
| music          =
| cinematography = 
| editing        = 
| studio         = British Actors Film Company
| distributor    = International Exclusives
| released       = June 1917 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Fred Groves, Labour Party Member of Parliament. The film was based on an original screenplay by Kenelm Foss. 

==Cast== Fred Groves as John Webster
* Fay Compton as Diana Hazlitt
* Owen Nares as Gilbert Hazlitt
* Christine Silver as Nell Slade
* Lauri de Frece as Bert Slade
* Frederick Volpe as Sir George Hazlitt

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1914-1918. Routledge, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 