The Rover (2014 film)
 
 
{{Infobox film
| name           = The Rover
| image          = The Rover-2013.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = David Michôd
| producer       = David Linde Liz Watts David Michôd
| screenplay     = David Michôd
| story          = David Michôd Joel Edgerton
| starring       = Guy Pearce Robert Pattinson Scoot McNairy
| music          = Antony Partos   
| cinematography = Natasha Braier
| editing        = Peter Sciberras
| studio         = Porchlight Films Lava Bear Films Screen Australia
| distributor    = United States:  
| released       =   }}
| runtime        = 102 minutes  
| country        = Australia
| language       = English
| budget         = $12 million   
| gross          = $2.3 million   
}}
The Rover is a 2014 Australian dystopian drama film written and directed by David Michôd and based on a story by Michôd and Joel Edgerton.  {{cite web|title=FilmNation nabs Rover Australian outback, Anthony Hayes, David Field and Tawanda Manyimo.   It premiered out of competition in the Midnight Screenings section at the 2014 Cannes Film Festival on 18 May 2014.  
 expanding wide on 20 June 2014 in the United States.     
 Best Sound.   

==Plot== David Field), Caleb (Tawanda Manyimo) and Henry (Scoot McNairy) flee, leaving behind Henrys injured brother Rey (Robert Pattinson). While driving away, Archie mocks Rey and Henry attacks him, causing Caleb to crash. When they cannot maneuver the vehicle out of debris, Archie steals the car belonging to mysterious loner Eric (Guy Pearce). Eric manages to start the getaway vehicle and follows them. After a brief chase, Archie stops and Eric confronts them. When Eric tries to attack Archie, Henry knocks him unconscious with a shotgun.

Eric wakes up and drives his new car into town, where he wanders into several establishments, asking if they have seen the men. He goes to an opium den, where he finds a dwarf and two Chinese acrobats from a traveling circus in the backroom. Eric follows the dwarf to his trailer, where he offers Eric a gun for US$300. Eric doesnt have $300, so he abruptly shoots the dwarf in the head and leaves with a gun. After another confrontation with the opium dens owner, he walks back to his truck and finds Rey, who asks why he is in Henrys car. Eric asks Rey where Henry is, but Rey faints.

After seeking help from a shopkeeper, Eric takes Rey to a doctor (Susan Prior), who performs surgery on him. The doctor cares for abandoned dogs, which seems to interest Eric. The next day, Eric sees two cars approaching in the distance and takes the doctors rifle. The occupants of the car turn out to be the traveling circus members seeking revenge for Erics murder of the dwarf. They kill the doctors companion without warning when he comes out to investigate. Eric opens fire and kills the acrobats. Eric then leaves with Rey.

Eric and Rey stay at a motel in an almost-abandoned town. While Eric is away from the room, Rey loads a revolver, then sees an Army vehicle driving down the street. He takes cover behind a bed and hears someone attempting to enter the room from outside. He shoots through the door and is shocked to find he has killed the daughter of the motels owner. He is then shot at by a soldier (Nash Edgerton). Eric comes to the rescue, killing the soldier and driving away from the scene.
 Anthony Hayes). At a small Army base nearby, he learns he is being transported to Sydney. Eric tells the soldier that he killed his wife after learning she had an affair and is angry that he was able to get away with it without anyone noticing. The soldier ignores him. When the soldier hears gunfire, he goes to investigate and is killed by Rey, who has broken into the base to rescue Eric.

Eric and Rey arrive at the town where Henry and the gang are hiding. They find Erics car outside a house and break in. Eric holds Archie and Caleb at gunpoint, while Rey goes to confront Henry. After an argument, they shoot at each other; Rey misses, but Henry shoots Rey in the throat. Eric kills Archie and Caleb, before walking into Henrys room and finding Reys corpse. He shoots Henry in the chest and burns the bodies. Later, Eric pulls to the side of the road in his car. It is revealed that he was obsessed with finding the car because his dogs corpse was in the trunk. The film ends with Eric preparing to bury the dog.

==Cast==
{{multiple image
| footer    = Guy Pearce (left) plays Eric, Robert Pattinson (right) plays Reynolds.
| image1    = GuyPearceJan11.jpg
| alt1      = Guy Pearce
| width1    =  
| image2    = Robert Pattinson 4, 2011.jpg
| alt2      = Robert Pattinson
| width2    =  
}}
* Guy Pearce as Eric, a violent and bitter former Australian soldier who has lost his farm and his family. Michôd said that "I wanted the character to be a guy who had seen that world collapse, remembered a time when things were different and was carrying around a jaded resentment that was bubbling in a really murderous and dangerous way."  
* Robert Pattinson as Reynolds, a simple and naive southern American youngster. Rey is described by Pattinson as "a dependent who has been protected by people his entire life, but he has also burdened them, and he thinks that he can’t really live as an independent person. He’s a little slow, and very, very needy, and he feels like he needs people to look after him all the time." 
* Scoot McNairy as Henry, brother of Reynolds and a member of the criminal group who stole Erics car 
* Gillian Jones as Grandma, owner of an opium den David Field as Archie, another member of the group who stole Erics car
* Tawanda Manyimo as Caleb, another member of group who stole Erics car Anthony Hayes as Sgt. Rickofferson
* Susan Prior as Dorothy Peeples
* Nash Edgerton as Town soldier
* Jamie Fallon as Colin 	
* Samuel F. Lee as Chinese acrobat

==Production==

===Development===
  }} Animal Kingdom, David Michôd started working on his next screenplay, based on a story he conceived with Joel Edgerton. The story is set in the near future, in Australia a decade after the collapse of the western economy where people from all over the world come to work in the mines.   

The setting and plot of the film will draw comparisons with Mad Max. Clarifying those comparisons, Michôd said that, "You put cars in the desert in Australia and people are going to think of Mad Max, and with all due respect to that film — and I stress that — I think The Rover is going to be way more chillingly authentic and menacing." 

Talking about the characters in the film, Michôd said, "There is a strong sort of Asian flavour in the film, but I wanted it to feel like people have come from everywhere, from all corners of the world. Rey is a southern American who has travelled with his older brother Henry to work in the Australian mines. Robert Pattinson and Scoot McNairy are the only American accented characters in the film, but a lot of other accents join them, including Standard Chinese|Mandarin, Cambodian language|Cambodian, and of course, Australian."   

===Casting===
It is the second collaboration between David Michôd and Guy Pearce after Animal Kingdom; Michôd wrote the character of Eric for Pearce.    In May 2012, it was announced that Pearce would appear as Eric in the film.  Initially Joel Edgerton, who co-wrote the story of the film with Michôd tried for the role of Eric but it did not work out, according to Edgerton "David   and I even at one point were like, because I said to David, “Should I be in this movie? I don’t know that I’m right for it.” And he’s like, “I don’t know if you’re right for it either.” Because it wasn’t just some complicated character, Guy  ’s character. We even decided to go out one day to shoot a scene together just to work it out for ourselves. It was pretty clear after doing it I’m not the right guy for this movie." 

Pattinson won the role of Reynolds after audition, over other actors.  Michôd commented on the casting of Pattinson that, "(he is) really smart, and not the sort of pretty boy I was expecting. As soon as it was time to start testing… he was my first choice, by a long way." 

In February 2013, McNairy joined the cast of the film as Henry (Reynolds brother).    Gillian Jones was cast in the role of the matriarch, who runs an opium den.   

Michôd used amateur actors from the Australian outback in the film. Talking about that he said, "You cant meet these people and not think, Oh, theres got be a way of getting you in this movie." 

===Pre-production===
Production began on 26 November 2012 in Adelaide, South Australia. Producer Liz Watts called The Rover "a dirty and dangerous near-future western set in the Australian desert," and added that shooting would begin in January 2013. 

Pattinson said in August 2012, "Its very existential. Its really interesting. I couldnt really explain to you what its about but its sort of about how much pain can the world take and how much disgust and cruelty before love dies. I think thats kind of what its about." 

 .]]
Producers Watts and Linde commended the enthusiasm of the cast; they said of the shoot, "The South Australian desert environment can be a tough one to work in, particularly for those coming from a Northern Hemisphere winter but both the cast and crew have thrown themselves into the shoot. We are shooting in some of the most haunting and stunning landscapes in the world, and are fortunate to have such an exceptional cast and a truly talented crew." 

===Filming===
Principal photography commenced on 28 January 2013 in Southern Flinders Ranges, Australia.   Filming continued over seven weeks in Electoral district of Hammond|Hammond, Quorn, South Australia|Quorn, Copley, South Australia|Copley, and Leigh Creek, ending on 16 March 2013 in Marree, north of Adelaide, Australia.  

===Post-production===
After filming finished in mid-March, post-production took place in Sydney, Australia.    

==Music==
{{Infobox album
| Name       = The Rover: Original Motion Picture Soundtrack 
| Type       = film
| Longtype   = / Soundtrack album
| Artist     = various artists
| Cover      = 
| Released   = 7 October 2014
| Recorded   =  electronic
| Length     = 61:33  (Score Length)  34:55  (Soundtrack Length) 
| Label      = Lakeshore Records
| Producer   = Antony Partos
| Chronology = Antony Partos
| Last album = 33 Postcards (2011)
| This album = The Rover (2014)
| Next album = 99 Homes (2014)
}}
Antony Partos composed the score for the film. He previously collaborated with Michôd on his 2010 film Animal Kingdom.  Musician Sam Petty, who previously worked with Michôd on Animal Kingdom and Hesher (film)|Hesher selected the soundtracks and music pieces for the film.  The soundtrack album was released by Lakeshore Records in a digital format on 7 October 2014 and will be release in physical format on 11 November 2014.    

"Gone" by Clearside was featured in the first teaser trailer of the film. 

===Background===
Talking about the score Partos said that "My task was to build the trust and love between the two main characters despite their circumstances. I think there is a subtle yet tangible shift that develops two thirds of the way through the story and the score does change in this regard to become more harmonically based compared to the textures that are present in the first half of the film."   

===Score listing===
{{Track listing
| extra_column = Artist(s)

| title1= Four Day Interval Tortoise
| length1= 4:44

| title2 = Arrival
| extra2= Anthony Partos
| length2 = 2:36

| title3 = (No) Vacancy
| extra3 = Sam Petty
| length3= 4:32

| title4= Crossfire 
| extra4= Sam Petty
| length4= 2:47

| title5= Campfire 
| extra5= Antony Partos
| length5= 2:21

| title6 = Groundswell
| extra6 = Colin Stetson
| length6= 1:40

| title7 = Pit Stop
| extra7= Antony Partos
| length7 = 1:53

| title8= Deja-Vu
| extra8= Sam Petty
| length8= 3:40

| title9 = Homecoming
| extra9= Anthony Partos
| length9 = 3:51

| title10 = Bonfire
| extra10= Anthony Partos
| length10 = 4:15

| title11= Crystal Waters
| extra11= Matthias Loibner
| length11= 3:55

| title12= Two Themes for Rey
| extra12= Sam Petty
| length12= 1:16

| title13 = Motel
| extra13= Antony Partos
| length13 = 2:54

| title14 = Variation VI
| extra14 = William Basinski
| length14= 9:18

| title15 = Variation V
| extra15 = William Basinski
| length15= 15:11
 
| total_length= 61:33
}}

===Soundtrack===
Michôd wanted to use saxophonist Colin Stetson pieces in the film. Originally he also selected "Dont Cha" by The Pussycat Dolls to use in a crucial scene in the film involving Rey (played by Pattinson) but later changed it to "Pretty Girl Rock" by Keri Hilson. 

===Soundtrack listing===
{{Track listing
| collapsed = yes
| extra_column = Artist(s)

| title1 = Meak Mer Nov Odor Meanchhey
| extra1 = Savy Heng and James Cecil

| title2 = Backpack 
| extra2 = Gabby La La

| title3 = Trilogy- The Three Ages of Man: Memories
| extra3 = Frances-Marie Uitti

| title4 = Time is Advancing with Fitful Irregularity
| extra4 = Colin Stetson

| title5 = As a Bird or Branch
| extra5 = Colin Stetson

| title6 = Awake on Foreign Shores
| extra6 = Colin Stetson

| title7 = Ko-Tha- Three Dances Of Shiva: Transcription For Double Bass By Fernando Grillo - I Robert Black

| title8 = Do I Worry?
| extra8 = The Ink Spots

| title9 = Djed Tortoise

| title10= I Heard the Voice of Jesus Say
| extra10= Pattie Rosemon with Frank & Odie Rosemon

| title11= Pretty Girl Rock
| extra11= Keri Hilson

| total_length= 34:55
}}

==Distribution==

===Marketing and Promotion===
The first images of Pearce and Pattinson in the film were released on 13 March 2013.   On 10 April 2014, a still of Pattinson from the film was released.  The film was presented as a part of upcoming highlights at 2013 Toronto International Film Festival. 
 teaser trailer was released, along with the poster of the film.   The full teaser, along with a sneak peek from the film, premiered on TV on 30 January 2014 during the 3rd AACTA Awards broadcast on Network Ten. 

With the announcement of the films premiere at the Cannes Film Festival, a full-length official trailer, along with two film posters featuring Pearce and Pattinson, were released on 17 April 2014.  

===Screening===
AACTA host a screening of the film and Q&A session with Michôd on 7 July 2014.  On 6 August 2014, BFI Southbank hold a screening of film and a Q&A session,  additionally on 7 August, Apple Store, London also hosted a Q&A session with Pearce, Pattinson and Michôd. 

===Release=== wider release 2014 New Zealand International Film Festival in Thrill section on 20 July 2014.  It also served as the opening night film at Fantasy Film Fest, Germany on 27 August 2014.  It had a theatrical release in UK on 15 August 2014.  

===Home media=== digital download amazon on 23 August 2014 in United States.  It was released on DVD in UK on 5 January 2015. 

==Reception==

===Box office=== The Railway Man for the lowest opening weekend of a film in 600 or more theatres during 2014. 

The film has earned a worldwide total of $2,297,553. 

===Critical response===
 ]]
Upon its  , which assigns a weighted mean rating out of 100 reviews from film critics, the film holds an average score of 64, based on 38 reviews, indicating a generally favorable response. 
 The Playlist, grade the film B+ by saying that "Bleak, brutal and unrelentingly nihilist, and with only sporadic flashes of the blackest, most mordant humour to lighten the load, it feels parched, like the story has simply boiled away in the desert heat and all that’s left are its desiccated bones. In a good way." 
 Variety said The Guardian noted that "After a terrific start, the film begins to meander, to lose its way, and its grip."  Drew McWeeny of HitFix called the film "glacially paced and intentionally minimalistic." 
 Animal Kingdom, David Michôd proves himself to be the most uncompromising director of his generation." 

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2014
|rowspan="13"|AACTA Awards  (4th AACTA Awards|4th)  Best Direction
| David Michôd
|   
|- Best Lead Actor
| Guy Pearce
|   
|- Best Supporting Actor
| Robert Pattinson
|   
|- Best Supporting Actress
| Susan Prior
|   
|- Best Production Design
| Josephine Ford
|   
|- AACTA Award Best Original Music Score
| Anthony Partos
|   
|-
| Sam Petty
|   
|- AACTA Award Best Sound
| Sam Petty
|   
|-
| Des Kenneally
|   
|-
|  Justine Angus
|   
|-
| Brooke Trezise
|   
|-
| Francis Ward Lindsay
|   
|-
| Robert Mackenzie
|   
|- Australian Film Critics Association Best Director David Michôd
|    
|- Best Actor Guy Pearce
|  
|- Best Supporting Actor Robert Pattinson
|  
|- Best Supporting Actress Susan Prior
|  
|- Gillian Jones
|  
|- Best Cinematography Natasha Braier
|  
|- Australian Production and Design Guild Awards Docklands Studios Melbourne Award for Design on a feature film Josephine Ford
|  
|- Australian Screen Australian Screen Sound Guild Awards Best Film Sound Recording Des Kenneally
|    
|- Marco Arlotta 
|  
|- Rob Nokes
|  
|- 2015
|rowspan="6"|Film Critics Circle of Australia Best Director David Michôd
|    
|- Best Actor in a Leading Role Guy Pearce
|  
|- Best Actor in a Supporting Role Robert Pattinson 
|  
|- Best Actress in a Supporting Role Susan Prior 
|  
|- Best Production Design Josephine Ford 
|  
|- Best Music Score Antony Partos 
|  
|- 2014
|Screen Screen Producers Australia Awards Feature Film Production PorchLight Entertainment|PorchLight Films
|  
|-
| Sydney Film Festival
| Official Competition Award: Best Film
| David Michôd
|   
|}
 

==See also==
 
* Cinema of Australia
* South Australian Film Corporation

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 