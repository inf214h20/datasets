Randy and the Mob
 
{{Infobox film name = Randy and the Mob image = Poster of the movie Randy and The Mob.jpg producer = Lisa Blount Walton Goggins Benjy Griffith David Koplan D. Scott Lumpkin Phil Walden   director = Ray McKinnon writer = Ray McKinnon  starring = Ray McKinnon Lisa Blount Walton Goggins Bill Nunn Burt Reynolds. music = John Swihart cinematography = Jonathan Sela editing = Jim Makiej     distributor = Vivendi Entertainment Lightyear Entertainment released =   runtime =  country = United States language = English budget = 
}} Ray McKinnon. It also stars Lisa Blount, Walton Goggins and Bill Nunn, with a cameo by Burt Reynolds.

Randy and the Mob was filmed in August 2005 in several locations in and around Atlanta, Georgia (U.S. state)|Georgia, mostly in Villa Rica, Georgia. The film won the Audience Choice Award at the Nashville Film Festival.

== Plot ==
Good ol’ boy Randy Pearson can’t seem to help getting in over his head with his various businesses. His latest scheme to keep his businesses afloat goes awry when a long-standing debt becomes due to a couple of low level Italian mobsters. And the IRS wants to talk to Randy too! Randy’s forced to swing into action, but his only hope is a helping hand from his carpal-tunneled, baton-teaching wife, Charlotte; his estranged, gay twin brother, Cecil, and Tino Armani, a mysterious modern day prophet with a knack for high fashion, Italian cooking and clogging. They serve as a powerful reminder of the strength of Southern roots, where love and family can usually be counted on to trump all adversities.

== Cast == Ray McKinnon as Randy and Cecil Pearson
*Walton Goggins as Tino Armani
*Lisa Blount as Charlotte Pearson
*Tim DeKay as Bill
*Bill Nunn as Wardlowe
*Brent Briscoe as Officier Griff Postell
*Paul Ben-Victor as Franco
*Sam Frihart as Four
*Burt Reynolds as Elmore Culpepper

== External links==
*  

 
 
 
 
 
 
 
 
 


 