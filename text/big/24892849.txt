Pugachev (film)
{{Infobox film
| name           = Pugachev
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Aleksei Saltykov
| producer       = Aleksei Saltykov
| writer         = Eduard Volodarsky
| narrator       = 
| starring       = Yevgeny Matveyev Vija Artmane
| music          = Andrei Eshpai
| cinematography = Igor Chernykh
| editing        = 
| studio         = Mosfilm
| distributor    = Mosfilm 1978
| runtime        = 156 min.
| country        =   USSR Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Pugachev ( ) is a 1978 film, directed by Aleksei Saltykov and starring Yevgeny Matveyev and Vija Artmane. This film was honored with a special prize at All-Union Film Festival in 1979. 

==Plot==
Pugachev is historical drama about revolt under direction of Yemelyan Pugachev playing by Yevgeny Matveyev   during the reign of  Catherine II of Russia playing by Vija Artmane.
 

==Main cast==
*Yevgeny Matveyev - Yemelyan Pugachyov
*Vija Artmane -     Catherine II of Russia	
*Tamara Syomina- 	Sofiya Pugachyeva
*Olga Prokhorova - 	Ustinya Pugachyeva
*Pyotr Glebov -	Stepan Fedulov
*Boris Kudryavtsev -Maksim Shigayev
*Grigore Grigoriu -	Chika Zarubin
*Viktor Pavlov-     Mitka Lysov

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 



 
 

 