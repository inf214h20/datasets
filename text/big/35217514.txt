End Play
 
{{Infobox film
| name           = End Play
| image          = 
| image_size     = 
| caption        = 
| director       = Tim Burstall
| producer       = Tim Burstall Alan Finney (associate)
| writer         = Tim Burstall
| based on = novel by Russell Braddon
| narrator       =  George Mallaby John Waters Ken Goodlet   Delvene Delaney Peter Best
| editing        = David Bilcock
| cinematography = Robin Copping 
| studio         = Hexagon Productions distributor = Roadshow
| released       = 1 January 1976
| runtime        = 114 mins
| country        = Australia
| language       = English
| budget         = AU$294,000 
| gross =AU$800,000 (Australia) David Stratton, The Last New Wave, Angus & Robertson, 1980 p 34 
| preceded_by    = 
| followed_by    = 
}} Australian thriller George Mallaby, John Waters and Ken Goodlet. It was an adaptation of the 1972 novel End Play by Russell Braddon. It was made by Hexagon Productions. 

==Plot==
Hitchhiker Janine Talbort is picked up and murdered by an unseen assailant. Mark Gifford, a merchant sailor on leave, then disposes of the body, attracting the suspicion of his wheelchair bound brother Robert. The police become suspicious of both brothers, who are rivals over their half-cousin, Margaret.

==Cast== George Mallaby - Robert Gifford  John Waters - Mark Gifford 
* Ken Goodlet - Superintendent Cheadle 
* Delvene Delaney - Janine Talbort 
* Charles Tingwell - Doctor Fairburn 
* Belinda Giblin - Margaret Gifford 
* Robert Hewett - Sergeant Robinson 
* Kevin Miles - Charlie Bricknall  Walter Pym - Stanley Lipton 
* Sheila Florance - Mavis Lipton 
* Reg Gorman - TV Reporter 
* Adrian Wright - Andrew Gifford 
* Jan Friedl - Policewoman 
* Vicki Raymond - Robbies Mother 
* Elspeth Ballantyne - Welfare Officer 
* Terry Gill - Ticket Collector

==Production== Eliza Fraser (1976). The movie was budgeted at $244,000 but eventually cost $294,000. Scott Murray, Tim Burstall, Cinema Papers Sept-Oct 1979 p495, 576  Shooting commenced in January 1975.

The two leads, George Mallaby and John Waters, were familiar faces on Australian television at the time. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p34 

==Reception==
The film performed reasonably at the box-office and in 1979 reported that it had just broken even. It also rated highly on television, the rights for which earned Heaxgon $70,000.  Burstall admitted the film might have been more effective as a TV movie but says it would have been harder to make a profit that way. 

==References==
 

==Bibliography==
* Moran, Albert & Viethm, Errol. Historical Dictionary of Australian and New Zealand Cinema. Scarecrow Press, 2005.

==External links==
* 
*  at Oz Movies
 

 
 
 
 
 
 
 


 