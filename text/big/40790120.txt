Lima: Breaking the Silence
{{multiple issues|
 
 
}}

{{Infobox film
| name = Lima: Breaking the Silence
| image =
| caption =
| director = Menahem Golan
| producer = Martien Holdings A.V.V.
| writer = Menahem Golan Vadim Sokolovsky
| starring = Joe Lara Billy Drago
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 90 min
| country = United States
| language = English
}} crime Drama drama film directed and written by Menahem Golan, starring Joe Lara and Billy Drago. The scenario is based on the true story of Peruvian rebels who in 1997 kidnapped several hundred diplomats. 

== Cast ==
* Joe Lara as Victor
* Billy Drago as General Monticito Frantacino
* Christopher Atkins as Jeff
* Bentley Mitchum as Bruce Nelson
* Julie St. Claire as Elena

== See also ==
* Japanese embassy hostage crisis
* Breaking the Silence (film)

== References ==
 

== External links ==
*  
*  

 
 
 

 