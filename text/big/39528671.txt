Eugenia Grandet
{{Infobox film
 | name =
 | image = Eugenia Grandet.jpg
 | caption =
 | director = Mario Soldati
 | producer = Ferruccio De Martino
 | writer =  Honoré de Balzac (novel)   Aldo De Benedetti   Mario Soldati
 | starring = Alida Valli   Gualtiero Tumiati   Giorgio De Lullo     Giuditta Rissone Renzo Rossellini Roman Vlad
 | cinematography = Václav Vích  
 | editing =   Eraldo Da Roma
 | studio = Excelsa Film 
 | distributor = Minerva Film 
 | released = 10 September 1946
 | runtime = 100 minutes
 | awards =
 | country = 
 | language =
 | budget = 
 }} Italian drama film directed by Mario Soldati. It is based on the novel Eugénie Grandet by Honoré de Balzac.    The novel has been adapted into films on a number of occasions. The films sets were designed by art director Gastone Medin.
 7th Venice Best Actress.  The film also won the Nastro dArgento for Best Scenography.   

== Cast ==
*Alida Valli: Eugenia Grandet
*Giorgio De Lullo: Charles Grandet
*Gualtiero Tumiati: Felix Grandet
*Giuditta Rissone: Eugenias mother
*Maria Bodi: Madame Des Grassins
*Giuseppe Varni: Mr. Des Grassins 
*Pina Gallini: Nanon
*Lina Gennari: Marquise DAubrion 
*Enzo Biliotti: Notary Cruchet 
*Liana Del Balzo

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 