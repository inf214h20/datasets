The Attic (2007 film)
{{Infobox film
| name           = The Attic
| image          = Poster of The Attic (2008 film).jpg
| caption        = 
| director       = Mary Lambert
| producer       = {{plainlist|
* Tom Malloy
* Isen Robbins
* Aimee Schoof
* Russ Terlecki
}}
| screenplay     = {{plainlist|
* Tom Malloy
* Bob Reitano
}}
| story          = Tom Malloy
| starring       = {{plainlist|
* Elisabeth Moss Jason Lewis
* Tom Malloy
}}
| music          = 
| cinematography = James Callanan
| editing        = Jack Haigis
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
}} Jason Lewis, Tom Malloy, and Catherine Mary Stewart. 

==Plot==
Emma (Moss) has a strong aversion towards her familys new house, especially the attic. After moving in, she becomes miserable and reclusive. The rest of her family also seems unhappy and unsettled. The situation escalates one day when Emma is in the attic alone. All of a sudden someone who looks exactly like Emma attacks her viciously. 

Emma is convinced that someone or something is haunting her, and she refuses to leave her house until she can piece the puzzle together, with the assistance of John Trevor (Lewis), a sympathetic detective. Eventually, Emma suspects her parents of hiding skeletons in the closet from the familys past and practicing magical rituals using symbols Wicca seemingly stole from satanism. As the clues pile up, she discovers that she once had a sister named Beth, who died twelve days after Emma was born. Emma realizes that this identical apparition may actually be Beth returning to life.

Emma witnesses Beth kill her brother, and the police suspect it was really Emma. John Trevor comes to the rescue when Beth is trying to strangle Emma to death and leaves her with a gun. She suspects that her father was to blame and murders both her parents. When the police arrive, Emma threatens "John Trevor", who now claims to be a paramedic, with the gun he gave her. As she points the gun at Trevor, she is really pointing the gun at herself, and when she pulls the trigger, she kills herself.

The police later discuss about the incident, and Emmas former psychologist explains that Beth and Trevor were only in her mind. Another officer mentions that the previous owner of the house mysteriously died. The psychologist says that houses do not kill people, but in this case it did. The film ends as new family looks to buy the house. As a young girl about Emmas age explores the attic, "John Trevor", now a real estate agent called Ron, appears behind her and says they will be seeing a lot of each other.

==Cast==
* Elisabeth Moss as Emma Callan Jason Lewis as John Trevor
* Tom Malloy as Frankie Callan John Savage as Graham Callan
* Catherine Mary Stewart as Kim Callan
* Thomas Jay Ryan as Dr. Perry
* Alexandra Daddario as Ava Strauss
* Gil Deeble as Detective Carter
* Clark Middleton as Dr. Cofi
* Lynn Marie Stetson as Beth
* Tara Thompson as Allison
* Nick Gregory as Detective
* Sara Maraffino as Demon (voice)
* Russell Terlecki as paramedic

==Release==
It was released on DVD in Germany in 2007.   Allumination FilmWorks released it on DVD in the US on January 15, 2008. 

==Reception==
Joshua Siebalt of Dread Central rated it 1/5 stars and wrote, "The Attic is not the Mary Lambert comeback fans, myself included, were likely hoping for. Instead it’s just another predictable psychological horror movie posing as a ghost story and very much not worth your time."   Justin Felix of DVD Talk rated it 1.5/5 stars and wrote, "The Attic is fairly transparent and youll figure out the answers long before the heroine does."   Jim Thomas of DVD Verdict wrote, "This is a bad movie. Avoid it at all costs." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 