Bonnie and Clyde (film)
{{Infobox film
| name           = Bonnie and Clyde
| image          = Bonnie and Clyde.JPG
| image_size     =
| caption        = Film poster by Bill Gold
| director       = Arthur Penn
| producer       = Warren Beatty
| writer         =  
| starring       = Warren Beatty Faye Dunaway Michael J. Pollard  Gene Hackman Estelle Parsons
| music          = Charles Strouse
| cinematography = Burnett Guffey
| editing        = Dede Allen
| distributor    = Warner Bros.-Seven Arts
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $2.5 million
| gross          = $50,700,000    $70,000,000   
}} American biographical directed by Clyde Barrow David Newman and Robert Benton. Robert Towne and Beatty provided uncredited contributions to the script; Beatty also produced the film. The soundtrack was composed by Charles Strouse.

Bonnie and Clyde is considered a landmark film, and is regarded as one of the first films of the New Hollywood era, since it broke many cinematic taboos and was popular with the younger generation. For some members of the counterculture of the 1960s|counterculture, the film was considered to be a "rallying cry."  Its success prompted other filmmakers to be more open in presenting sex and violence in their films. The films ending also became iconic as "one of the bloodiest death scenes in cinematic history". 
 Best Supporting Best Cinematography (Burnett Guffey).    It was among the first 100 films selected for preservation in the United States National Film Registry.   

==Plot==
In the middle of the Great Depression, Clyde Barrow (Warren Beatty) and Bonnie Parker (Faye Dunaway) meet when Clyde tries to steal Bonnies mothers car. Bonnie, who is bored by her job as a waitress, is intrigued with Clyde, and decides to take up with him and become his partner in crime. They pull off some holdups, but their amateur efforts, while exciting, are not very lucrative.

The duos crime spree shifts into high gear once they hook up with a dim-witted gas station attendant, C.W. Moss (Michael J. Pollard), then with Clydes older brother Buck (Gene Hackman) and his wife, Blanche (Estelle Parsons), a preachers daughter. The women dislike each other on first sight, and their feud only escalates from there: shrill Blanche has nothing but disdain for Bonnie, Clyde and C.W., while gun-moll Bonnie sees Blanches flighty presence as a constant danger to the gangs well-being.

Bonnie and Clyde turn from pulling small-time heists to robbing banks. Their exploits also become more violent. When C.W. botches a bank robbery by parallel parking the getaway car, Clyde shoots the bank manager in the face after he jumps onto the slow-moving cars running board. The gang is pursued by law enforcement, including Texas Ranger Frank Hamer (Denver Pyle), who is captured and humiliated by the outlaws, then set free. A raid later catches the outlaws off guard, mortally wounding Buck with a gruesome shot to his head and injuring Blanche. Bonnie, Clyde and C.W. barely escape with their lives. With Blanche sightless and in police custody, Hamer tricks her into revealing C.W.s name, who was up until now still only an "unidentified suspect."

Hamer locates Bonnie, Clyde and C.W. hiding at the house of C.W.s father Ivan Moss (Dub Taylor), who thinks the couple—and an ornate tattoo—have corrupted his son. The elder Moss strikes a bargain with Hamer: In exchange for leniency for the boy, he helps set a trap for the outlaws. When Bonnie and Clyde stop on the side of the road to help Mr. Moss fix a flat tire, the police in the bushes open fire and riddle them with bullets. Hamer and his posse then come out of hiding, looking pensively at the couples bodies.

==Cast== Clyde Barrow Bonnie Parker
*Michael J. Pollard as C.W. Moss
*Gene Hackman as Buck Barrow
*Estelle Parsons as Blanche Barrow
*Denver Pyle as Frank Hamer
*Dub Taylor as Ivan Moss
*Gene Wilder as Eugene Grizzard
*Evans Evans as Velma Davis
*Mabel Cavitt as Mrs. Parker
 

===Cast notes===
Actor Gene Wilder portrayed Eugene Grizzard, one of Bonnie and Clydes hostages. His girlfriend Velma Davis was played by Evans Evans, who was the wife of film director John Frankenheimer.

The family gathering scene was filmed in Red Oak, Texas. Several local residents gathered to watch the film being shot. When the filmmakers noticed Mabel Cavitt, a local school teacher, among the people gathered, she was cast as Bonnie Parkers mother.    

==Production and style==
The film was intended as a romantic and comic version of the violent gangster films of the 1930s, updated with modern filmmaking techniques.  Arthur Penn portrayed some of the violent scenes with a comic tone, sometimes reminiscent of Keystone Kops-style slapstick films, then shifted disconcertingly into horrific and graphic violence.    The film was strongly influenced by the French New Wave directors, both in its rapid shifts of tone, and in its choppy editing, which is particularly noticeable in the films closing sequence. 
{{multiple image
|direction=vertical
|width=220
|footer=
|image1=BonnieClyde67TrailerWBCredit.JPG
|image2=BonnieClyde67TrailerFayeCredit.JPG
}}
 The Chase Fahrenheit 451.  But at Truffauts suggestion the writers, much excited, the then-producers less so, approached Jean-Luc Godard. Some sources claim Godard didnt trust Hollywood and refused; Robert Benton claimed that Godard wanted to shoot the film in New Jersey in January (winter) and took offense when would-be producer Norah Wright objected that that was unreasonable considering the story took place in Texas with its year-round warm environment.  while her partner, Elinor Jones,  claimed they did not believe Godard was right for the project in the first place. Godards retort: « Je vous parle de cinéma, vous me parlez de météo. Au revoir »    

Soon after the failed negotiations for production, Warren Beatty was visiting Paris and learned of the project and its perambulations through Truffaut. On returning to Hollywood, Beatty requested the script and bought the rights. A meeting with Godard was not productive. Beatty then changed compass and convinced the writers that while the script at first reading was very much of the French  New Wave style, an American director was necessary for the subject. 

Beatty offered the position to names like George Stevens, William Wyler, Karel Reisz, John Schlesinger, Brian G. Hutton, and Sydney Pollack, all of whom turned down the opportunity. Arthur Penn actually turned down the directors position again further times before Beatty finally convinced him to helm the film. 

When Warren Beatty was on board as producer only, his sister Shirley MacLaine was a strong possibility to play Bonnie, but when Beatty decided to play Clyde, obviously a different actress was needed. Those considered for the role were Jane Fonda, Tuesday Weld, Ann-Margret, Leslie Caron, Carol Lynley and Sue Lyon. Cher auditioned for the part, while Warren Beatty begged Natalie Wood to play the role. Wood declined the role to concentrate more on her therapy at the time, and acknowledged that working with Beatty before was "difficult." Faye Dunaway stated that she won the part "by the skin of her teeth!" 

The film is forthright in its handling of sexuality, but that theme was toned down from its conception. Originally, Benton and Newman wrote Clyde as bisexual and he and Bonnie were to have a three-way sexual relationship with their male getaway driver. However, Arthur Penn persuaded the writers that the relationships emotional complexity was underwritten, it dissipated the passion of the title characters and it would harm the audiences sympathy for the characters who would write them off as sexual deviants because they were criminals. Others claimed that Beatty was not willing to have his character display that kind of sexuality and that the Production Code would never have allowed such content in the first place.  Instead, Clyde is portrayed as unambiguously heterosexual, if Erectile dysfunction|impotent. When Clyde brandishes his gun to display his manhood, Bonnie suggestively strokes the phallic symbol. Like the 1950 film Gun Crazy, Bonnie and Clyde portrays crime as alluring and intertwined with sex. Because Clyde is impotent, his attempts to physically woo Bonnie are frustrating and anti-climactic.
 squibs – small explosive charges, often mounted with bags of stage blood, that are detonated inside an actors clothes to simulate bullet hits. Released in an era where shootings were generally depicted as bloodless and painless, the Bonnie and Clyde death scene was one of the first in mainstream American cinema to be depicted with graphic realism. 
 PT 109 at his behest and was insolent enough to defy his favorite gesture of authority of showing the studio water tower with the WB logo on it by responding "Well, its got your name, but its got my initials."  In addition, Warner complained about the films extensive location shooting in Texas which exceeded its production schedule and budget, until he ordered the crew back to the studio backlot, where it was planned to be anyway for final process shots. 
 Reflections in a Golden Eye (they had changed the coloration scheme at considerable expense), their neglect of his film, which was getting excellent press, suggested a conflict of interest; he threatened to sue the company. Warner Brothers gave Beattys film a general release. Much to Warner Brothers management surprise, the film eventually became a major box office success. 

===Music===
The instrumental banjo piece "Foggy Mountain Breakdown" by Flatt and Scruggs was introduced to a worldwide audience as a result of its frequent use in the movie. Its use is strictly anachronistic as the Bluegrass music|bluegrass-style of music dates from the mid-1940s rather than the 1930s, but the functionally similar Old-time music genre was long established and widely recorded at the period in which the film is set.  Long out of print in vinyl and cassette formats, the film soundtrack album was finally released on CD in 2009. 

==Historical accuracy==
 , March 1933]]
The film considerably simplifies the lives of   and Henry Methvin.   

The Gene Wilder-Evans Evans sequence is based on the kidnappings of the undertaker H.D. Darby and his acquaintance Sophia Stone, near Ruston, Louisiana on April 27, 1933.  In the film, Eugene and Velma are romantically involved; Darby and Stone were not.
 Texas Ranger Frank Hamer (played by Denver Pyle) as a vengeful bungler who had been captured, humiliated, and released by Bonnie and Clyde. Hamer was already a legendary Texas Ranger when he was coaxed out of semi-retirement to hunt down the duo; he had never seen them before he and his posse ambushed and killed them near Gibsland, Louisiana on May 23, 1934.  In 1968, Hamers widow and son sued the movie producers for defamation of character over his portrayal. They were awarded an out-of-court settlement in 1971. 

The film portrays an unarmed and unsuspecting Clyde walking away from the car to investigate the broken down truck when he was ambushed. It suggests that Bonnie, still in their car, may also have been unarmed. The real couple remained in the vehicle and had weapons at the ready in the front seat; the back seat contained a dozen guns and thousands of rounds of ammunition.  Neither outlaw got out of the car alive.
{{multiple image
|footer=Bonnie and Faye: Infamous 1933 cigar photo branded Bonnie as a gun moll; 1966 publicity reenactment with Faye Dunaway
|direction=horizontal
|width=150
|image1=BonnieParkerCigar1933.jpg
|image2=DunawayAsBonniePromo.jpg
}} hastily abandoned hideout in Joplin, Missouri. In one, Bonnie holds a gun in her hand and a cigar between her teeth. Its publication nationwide typed her as a dramatic gun moll. The film portrays the taking of this playful photo. It implies the gang sent photos—and poetry—to the press, but this is untrue. The police found most of the gangs items in the Joplin cache. Bonnies final poem, read aloud by her in the movie, was publicized only posthumously by her mother.   (39 pages, with apartment plans, map, newspaper clippings and 11 photos) 
 John Tolands Algren refers to Dillinger Days as "a volume of conjectures, surmises and easy assumptions", and states, "one can only marvel at this writers presumption. Clyde Barrow might have been a latent heterosexual without even his mother knowing"? 

The only two members of the Barrow Gang who were alive at the time of the films release were Blanche Barrow and W. D. Jones. While Blanche Barrow approved the depiction of her in the original version of the script, she objected to the later re-writes. At the films release, she complained loudly about Estelle Parsonss portrayal of her, saying, "That film made me look like a screaming horses ass!" 

In 1968, W.D. Jones outlined his time with the Barrows in a Playboy (magazine)|Playboy magazine article.  That same year, he filed a lawsuit against Warner Brothers-Seven Arts, claiming that the film Bonnie and Clyde "maligned and brought shame and disrepute" on him and damaged his character by implying that he was complicit in the betrayal of his old partners. He repeated what he had said at his arrest in 1933, that far from being a willing member of the gang, he had tried to escape several times.   There is no record that his petition was ever heard by a court.

The movie was partly filmed in and around Dallas, Texas, in some cases using locations of banks that Bonnie and Clyde were reputed to have robbed at gunpoint. 

==Reception==
 
 glorification of murderers, and for its level of graphic violence, which was unprecedented at the time. Bosley Crowther of The New York Times was so appalled that he began to campaign against the increasing brutality of American films.  Dave Kaufman of Variety (magazine)|Variety criticized the film for uneven direction and for portraying Bonnie and Clyde as bumbling fools.  Joe Morgenstern for Newsweek initially panned the film as a "squalid shoot-em-up for the moron trade." After seeing the film a second time and noticing the enthusiastic audience, he wrote a second article saying he had misjudged it and praised the film. Warner Brothers took advantage of this, marketing the film as having made a major critic change his mind about its virtues. 

Roger Ebert gave Bonnie and Clyde a largely positive review, giving it four stars out of a possible four. He called the film "a milestone in the history of American movies, a work of truth and brilliance." More than 30 years later, he added the film to his The Great Movies list. Film critics Dave Kehr and James Berardinelli have also praised the film in the years since.

The fierce debate about the film is discussed at length in  . This 2009 documentary film chronicles what occurred as a result: The New York Times fired Bosley Crowther because his negative review seemed so out of touch with the public, and Pauline Kael, who wrote a lengthy freelance essay in The New Yorker in praise of the film, became the magazines new staff-critic.
 theatrical rentals, My Fair one of the top five grossing films of 1967 with $50,700,000 in US sales, and $70,000,000 worldwide. 
 artistic merit, Bonnie and Clyde is still sometimes criticized for opening the floodgates for violence in cinema.  It currently holds a 90% "Certified Fresh" rating on Rotten Tomatoes,  with 45/50 reviews positive.

==Awards and honors==

===Academy Awards===
Estelle Parsons won an Academy Award for Best Supporting Actress for her portrayal of Blanche Barrow, and Burnett Guffey won an Academy Award for Best Cinematography.

The film was also nominated for:
 Best Picture – Warren Beatty Best Director – Arthur Penn Best Writing David Newman and Robert Benton Best Actor in a Leading Role – Warren Beatty Best Actress in a Leading Role – Faye Dunaway Best Actor in a Supporting Role – Gene Hackman Best Actor in a Supporting Role – Michael J. Pollard Best Costume Design – Theadora Van Runkle

===Others===
*In 1992, Bonnie and Clyde was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."
*1998 – AFIs 100 Years...100 Movies – #27
*2001 – AFIs 100 Years...100 Thrills – #13
*2002 – AFIs 100 Years...100 Passions – #65
*2003 – AFIs 100 Years...100 Heroes and Villains
**Clyde Barrow & Bonnie Parker – #32 Villains
*2005 – AFIs 100 Years...100 Movie Quotes – #41
**"We rob banks." – #41
*2007 – AFIs 100 Years...100 Movies (10th Anniversary Edition) – #42 Gangster Film 

==Influence==
 .]]
Some critics cite Joseph H. Lewiss Gun Crazy, a 1950 film noir about a bank-robbing couple (also based loosely on the real Bonnie and Clyde), as a major influence on this film.  Forty years after its premiere, Bonnie and Clyde has been cited as a major influence for such disparate films as The Wild Bunch, The Godfather, The Departed,    and Natural Born Killers.  Bonnie and Clyde were also the subject of a popular 1967 French pop song performed by Serge Gainsbourg and Brigitte Bardot. Some aspects of the Bollywood movie Bunty aur Babli are inspired by this movie.

==Cultural References==

The "Storage Jars" skit of episode 33 of Monty Pythons Flying Circus features a brief still shot of Warren Beatty as Clyde Barrow firing a Thompson submachine gun as he escapes from the Red Crown Tourist Court; the shot is accompanied by a jarring chord and an announcement by Eric Idle that "On tonights programme, Mikos Antoniarkis, the Greek rebel leader who seized power in Athens this morning, tells us what he keeps in storage jars."  

==Notes==
 

==Further reading==
* 
* 

==External links==
 
 
*  
*  
*  
* Bosley Crowthers  , the New York Times, 14 April 1967, and his   of 3 September 1967.
* Stephen Hunter, in Commentary (magazine)|Commentary, on the films   about Barrow, Parker and Hamer.
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 