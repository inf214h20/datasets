Alluri Seetarama Raju (film)
{{Infobox film
  | name = Alluri Seetarama Raju
  | image = Alluri Seetharamaraju.jpg
  | caption =
  | director = V. Ramachandra Rao
  | producer = G.Hanumanthu Rao, G.Aadiseshagiri Rao
  | writer = Tripuraneni Maharadhi
  | starring =Ghattamaneni Krishna Vijaya Nirmala Sreedhar Surapaneni
  | music = P. Adinarayana Rao
  | cinematography =
  | editing =
  | studio = Padmalaya Studios
  | distributor =
  | released = 1 May 1974
  | runtime = 187 min.
  | language =  Telugu
  | budget =
  | preceded_by =
  | followed_by =
    }} Telugu film based on the life of Indian freedom fighter Alluri Sitaramaraju and produced by Padmalaya Studios. It is the 100th film of Ghattamaneni Krishna. The film is directed by V. Ramachandra Rao and written by Maharadhi Tripuraneni. It is South Indias first cinema scope film.

==Plot==
Cinematic version of the life of the legendary freedom fighter, Alluri Sitaramaraju who raised voice against the cruelity of British rulers and fought for the tribals.

==Cast==
* Gattamaneni Krishna ...  Alluri Seetarama Raju
* Kongara Jaggayya ...  Rutherford
* Vijaya Nirmala ...  Seeta Chandra Mohan 
* Sridhar 
* M. Balaiah ...  AGGI RAJU Kanta Rao ...      Padalu
* Pandari Bai 
* Dr. Prabhakar Reddy GAM MALLU DORA Rajababu  Manjula 
* Rajasree
* K. V. Chalam 
* Gummadi Venkateswara Rao GAM GANTAM DORA
* Tyagaraaju (SEBASTIAN)
* Rao Gopal Rao

==Awards== National Film Awards
* National Film Award for Best Lyrics - Srirangam Srinivasa Rao

;Nandi Awards
* Nandi Award for Best Feature Film (Golden Nandi) - Hanumantha Rao Gh

==Box-office==
* The film had a 100-day run in 19-centres    in Andhra Pradesh and Bangalore.

==Popularity==
This movie became one of the biggest hits of Telugu Cinema during that time and performance of Krishna in title role was highly appreciated.Tripuraneni Maharadhi scripted a masterpiece after taking this script as a challenge, initially N.T Rama Rao asked Tripuraneni Maharadhi to write this script for him but Maharadhi refused because he had already given word to Krishna. Against a strong opposition in the Induatry Hanumantha rao, Krishna and Maharadhi embarked upon this venture and Maharadhi scripted a master piece and rest is history. Maharadhi is a legendary writer who used to command the entire set, No director had the guts to make any changes to his script, such was the command of Maharadhi at that time. NT Rama Rao upon watching this movie said that "No body can do or make this film  better than Krishna and nobody can write this better than Maharadhi " (In Telugu: Eee cinemaani inthaku minchi inkevvaru cheyyaleeru, inkevvaru rayaleru"). The song "Telugu Veera Levera" is a national award winning (for lyrics), highly inspirational patriotic song which is remembered by Telugu people even today. Also the duet song "Vastaadu naa raju ee roju" was highly popular song. The dialogues written by Maharathi in this movie became very popular.

After watching movie legendary actor NT Rama Rao told to Krishna that "He wants to make this movie and you made better one". 

==Trivia==
* Its the first colour film to be made in Cinemascope. 

==References==
 

==External links==
*  

 
 
 
 
 
 