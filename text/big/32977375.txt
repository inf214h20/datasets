Environment (1927 film)
 
{{Infobox film
| name           = Environment
| image          = 
| image_size     =
| caption        = 
| director       = Gerald M. Hayle
| producer       = Vaughan C. Marshall
| writer         = Gerald M. Hayle
| based on       = 
| narrator       =
| starring       =  Beth Darvall Hal Percy
| music          =
| cinematography = Tasman Higgins
| editing        =
| studio = Advance Films 
| distributor    = 
| released       = 23 July 1927 
| runtime        = 6,000 feet
| country        = Australia English intertitles
| budget         = ₤4,000  
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Environment is a 1927 Australian silent film about a woman who poses for a revealing painting. It was one of two films produced by Vaughan C. Marshall, the other one being Caught in the Net (1928).

Unlike many Australian silent films, a copy of it survives today. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 140. 

==Plot==
Mary Garval is forced by poverty into posing semi-nude for a painting, LEnvironment. The painters assistant, Arthur, tries to seduce her but she runs away after finding out he is married.

Mary seeks refuge in the country and falls for a farmer, Jimmy. They get married but Arthur, seeking revenge, sends a Jewish friend to spy on them. He sends Jimmy a copy of the painting as a wedding present. Jimmy eventually forgives Mary and decides to destroy the painting, but discovers a lost will in the frame, which reveals Mary to be the heiress to a lost fortune.

==Cast==
*Beth Darvall as Mary Garval
*Hal Percy as James Denison
*Colin Campbell as Arthur Huston
*Alf Scarlett as James Masterton
*Arthur Bruce as James Garval
*Jim Joyce as Wilfred Garval
*Dorothy May as Mrs Huston
*Max Sorelle as Mr Eltham
*Kitty Mostyn as Mrs Eltham
*Viva Vawden as Mrs Harrop
*Charles Brown as Henry Harrop
*George Gilbert as Hal Hawkins
*Edward Landor as Abe Halstein
*Phyllis Best as Biddy ORooke

==Production==
The movie was shot in early 1927 in and around Melbourne. Marshall tried to woo Melbourne society while making the film, looking for investment. The director, Gerald Haye, made industrial films in Melbourne for several years. 

==Reception==
The film appears to have only been screened in Victoria. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 


 