Flawed
 
  Halifax filmmaker Andrea Dorfman about body image, combining stop-motion animation and hand-painted images. Flawed was produced in Halifax by Annette Clarke for the National Film Board of Canada.   

According to Dorfman, the film was created to resemble a storyboard because it was based on a series of postcards that she has sent to a boyfriend while they were in a long-distance relationship. While Flawed is an autobiographical work, she also believes that it is universal because "I believe everyone has felt flawed at some point in their lives." 

In July 2012, the film version of Flawed received a nomination in the category New Approaches to News and Documentary Programming: Documentaries at the 33rd annual News & Documentary Emmy Awards. The film was nominated following its August 23, 2011 premiere on the PBS series POV (TV series)|POV.  

The films awards also included an audience choice award at the New York City Short Film Festival.  Festival selections included a premiere at the 2010 Hot Docs Canadian International Documentary Festival. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 