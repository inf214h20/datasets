The Inn of the Sixth Happiness
{{Infobox film
| name           = The Inn of the Sixth Happiness
| image          = Inn Of Sixth Happiness 02(1958).jpeg
| caption        = Original film poster
| director       = Mark Robson
| producer       = Buddy Adler
| writer         = Alan Burgess
| based on       =  
| screenplay     = Isobel Lennart
| starring       = {{Plainlist|
* Ingrid Bergman
* Curd Jürgens
* Robert Donat
}}
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       = 23 November 1958 (World Premiere, London)
| runtime        = 158 minutes
| country        = United Kingdom
| language       = {{Plainlist|
* English
* Mandarin
* Japanese
* Russian
}}
| music          = Malcolm Arnold
| cinematography = Freddie Young
| editing        = Ernest Walter
| budget = $3,570,000 
| gross = $4.4 million (est. US/ Canada rentals) 
}}
 British maid, love interest, Dutch father. Robert Donat, who played the mandarin of the town in which Aylward lived, died before the film was released. The musical score was composed and conducted by Malcolm Arnold. The cinematography was by Freddie Young. 
 Chinese communities in Europe.  

== Plot ==
The story begins with Aylward (Ingrid Bergman) being rejected as a potential missionary to China because of her lack of education. Dr. Robinson (Moultrie Kelsall), the senior missionary, feels sorry for her and secures her a position in the home of a veteran explorer with contacts in China. Over the next few months, Aylward saves her money to purchase a ticket on the Trans-Siberian railway, choosing the more dangerous overland route to the East because it is less expensive. 
 Yang Cheng, where she secures a post as assistant to a veteran missionary, Jeannie Lawson (Athene Seyler). Lawson has set up an inn for traveling merchants, where they can get a hot meal and hear stories from the Bible. The film follows Aylwards acculturation, culminating in her taking over the inn when Lawson dies in an accident. 
 Mandarin (Robert China is being invaded by Japan, and Aylward is encouraged by Lin (Curt Jürgens) to leave. She refuses, and as the town of Yang Cheng comes under attack, she finds that she has fifty orphans in her care. 

As the population prepares to evacuate the town, the Mandarin announces that he is converting to Christianity to honour Aylward and her work (she is rather taken aback by this, as she would have preferred him to convert through religious conviction). She is now left alone with the children, aided by Li (Burt Kwouk), the former leader of the prison revolt she helped to resolve. Lin tells her that the only hope for safety is to take the children to the next province, where trucks will evacuate them to a safer area, but they must get there within three weeks, or the trucks will leave without them. 

Just as they are preparing to leave, another fifty orphans appear from a neighbouring town, so Aylward and Li have no choice but to lead one hundred children on a trek across the countryside. Although it should only have taken them a week, the roads are infested with Japanese patrols, and the group has to cut across the mountains. After a long, difficult journey, they all arrive safely (except for Li, who died to save them from a Japanese patrol) on the day the trucks are to leave. Aylward is greeted by Dr. Robinson, and she reminds him of how he rejected her as a missionary years before.

The film culminates with the column of children, led by Aylward, marching into the town, singing the song "This Old Man" to keep up their spirits.

==Cast==
 
* Ingrid Bergman as Gladys Aylward
* Curt Jürgens as Captain Lin Nan
* Robert Donat as The Mandarin of Yang Cheng Michael David as Hok-A
* Athene Seyler as Jeannie Lawson
* Ronald Squire as Sir Francis Jamison
* Moultrie Kelsall as Dr. Robinson
* Richard Wattis as Mr. Murfin Peter Chong as Yang Tsai Chin as Sui-Lan
* Edith Sharpe as Secretary at China Inland Mission 
* Joan Young as Sir Francis cook 
* Lian-Shin Yang as Woman with Baby 
* Noel Hood as Miss Thompson 
* Burt Kwouk as Li
* André Mikhelson as Russian Commissar 
 

==Production==
 
For the production of The Inn of the Sixth Happiness 20th Century Fox rented space at MGM British Studios Borehamwood, where the Chinese villages were built on the backlot, with location scenes filmed in Nantmor, near Beddgelert in North Wales.
A gold-painted statue of Buddha that was used on a set for the film is now located in the Italianate village of Portmeirion, North Wales. Sean Connery was considered for the role of Colonel Lin. The screen tests from this can be seen on the DVD.
 British actor Robert Donat and Austrian actor Curt Jurgens were not even Chinese.

== Historical accuracy ==
The film was based on the 1957 novel The Small Woman by Alan Burgess. 

The real Gladys Aylward (1902–1970) was born in London. She was a former domestic turned missionary in China and best known for her work with children. Aylward became a Chinese citizen in 1936. Four years later, despite being in ill health herself, she led more than 100 children over the mountains to safety at the height of the Second Sino-Japanese War. 

In 1958, the year this film was released, she founded a childrens home in Taiwan, which she continued to run until her death. Known in China as "Ai-weh-deh" (艾偉德), or "Virtuous One", she continues to be regarded as a national heroine.

Gladys Aylward was deeply upset by the inaccuracy of the movie. Although she found herself a figure of international interest thanks to the popularity of the movie and television and media interviews, Aylward was mortified by her depiction in the film and the many liberties it took. The tall, Swedish Ingrid Bergman was inconsistent with Aylwards small stature, dark hair and cockney accent. The struggles of Aylward and her family to effect her initial trip to China were skipped over in favor of the plot device of her employer "condescending to write to his old friend Jeannie Lawson," and Aylwards dangerous, complicated travels across Russia and China were reduced to "a few rude soldiers," after which "Hollywoods train delivered her neatly to Tsientsin." Sam Wellman. Gladys Aylward: Missionary to China, Barbour Publishing Inc., 1998, page 197. 

The names of many characters and places were changed, even when the names had significant meanings, such as those of Aylwards adopted children and of her inn, named for the Chinese belief in the number 8 as an auspicious number. Colonel Lin Nan was portrayed as half-European, a change which Aylward found insulting to his Chinese lineage. She also felt her reputation damaged by the Hollywood-embellished love scenes in the film; not only had she never kissed any man, but also the films ending portrayed her character abandoning the orphans in order to join the colonel elsewhere, Wellman, page 198.  even though in reality she did not retire from working with orphans until she was sixty years old. Wellman, page 201.  In real life, Aylward and Lin Nan were not reunited—he was lost in the war and she never knew what happened to him. 

==Reception==
The film was the second most popular movie at the British box office in 1959. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 