Toxi
{{Infobox film
| name           = Toxi
| image          =
| image_size     =
| caption        =
| director       = Robert A. Stemmle
| producer       = Werner Ludwig   Hermann Schwerin
| writer         = Maria von der Osten-Sacken   Peter Francke   Robert A. Stemmle
| narrator       =
| starring       = Elfie Fiegert   Paul Bildt   Johanna Hofer   Ingeborg Körner
| music          = Michael Jary
| cinematography = Igor Oberberg
| editing        = Alice Ludwig
| studio         = Fono Film
| distributor    = Allianz Filmverleih
| released       = 15 August 1952
| runtime        = 89 minutes
| country        = West Germany German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} black Allied-occupied Allied serviceman white German mothers entered school. 

Elfie Fiegert was selected to play Toxi after a mass audition held in Munich. Publicity for the film emphasised the similarities between her own story and that of Toxi.  The film was the eighth most popular release at the West German box office in 1952. 

==Synopsis==
The melodrama, film begins with a young Afro-German girl being left at the doorsteps of a middle-class, white, German family. Initially, most family members treat the young girl with relatively welcome arms as they believe she is a gift of performance for the night from a family member. The family later discovers a suitcase that was left on the doorsteps and they learn that the young girl,Toxi, has in fact been abandoned. Once the family learns that Toxi has been abandoned there is a shift in feelings regarding their acceptance of Toxi; the possibility of Toxi spending more time at the home than was expected forces members of the family to confront their racism. 
One character in particular, Uncle Theodore, is very unsettled by the idea of having Toxi stay in his white, household, as he does not want Toxi interacting with his children. Unlike the other children featured in the film, Toxi acts very differently. Toxi is constantly on performing perfect behavior as her manners and maturity level are well beyond her age as she feels a need to compensate for her blackness. Due to Toxis mature behavior Uncle Theodore realizes that he has made a mistake with his discrimination towards her. 
By the end of the film, the entire family has approved of Toxi. However, Toxi does not stay with the family as her father, an American soldier comes into the home to find Toxi and take her back to the States with him.

==Significance of Uncle Theodore==

Out of all of the characters within Toxi, Uncle Theodore is one of the most overtly racist figures for much of the film. This becomes clear in his strong objections to the idea of his children growing up around Toxi and his genuine attempts to put her out of the house. Additionally, his constant mention of 
This antagonistic approach towards Toxi directly contrasts the heartwarming relationships Toxi has with Grandfather Rose and Robert Peters. Where Uncle Theodore spends much of his time attempting to find a way to rid himself of Toxi, Grandfather Rose and Robert reach out to Toxi, engage with her and work to establish a more genuine relationship.
Through the juxtaposition of these relationships, the film demonstrates its intention of characterizing racial intolerance and the problematic ways in which different generations interacted with communities of color as issues unique the Nazi generation. Theodor’s struggle with his racism and references to the Rassenprobleme further accentuate the linkage of racial intolerance with the Nazi era and isolate this to his generation. Such isolation ultimately proved that others, such as the Jenrich children and other members of the younger generations, had the ability to overcome the racism and discrimination inherent in the previous generations. Furthering this suggestively post-racial agenda, Herta and Robert’s consideration of the possibility of adoption serves as a subtle push that reasserts the suggestion that racism was confined to the Nazi era, and as such, a thing of the past.

==Significance of the end of the film==

At the end of Toxi, Toxi and Theodore’s children put on a nativity play for the family. Although Toxi is originally supposed to be the Moor King, one of Theodore’s children wants the role. Thus they paint the other girls face black and paint Toxi’s face white in order to symbolize a post-racial era and support the idea of the film that race is not something that people should discriminate and differentiate on. 
However, while the nativity play is in progress Toxi’s grandmother brings Toxi’s black American father into the house. With her face covered in white paint Toxi hugs her father for the first time and the movie ends. Of course, the assumption is that Toxi’s father will take her “home” to America—Toxi will wipe the white paint from her face, and thus her Germaneness, and return to country in which she belongs. This is problematic, because it shows that after all Toxi has been through to prove herself lovable to Germans, it actually means nothing, because she still does not belong. The movie still has to end with Toxi returning to America, showing that Toxi is not in fact German even though it has been her home her entire life.

Another important implication of the ending of the film is that Toxis case mirrors the case of the "Brown Babies" (war children) from Germany after World War II. Biracial children were born as a result of the bringing of American and French troops into Germany during World War II. They were often referred to as "Rhineland B*stards" as well. These "Brown Babies" could not be assimilated into the white German nation, so they were excluded from German society. Consequently, Black Americans were more receptive to these biracial children so the African-American community sought to adopt some of these babies. Toxis case is very rare, but existent in Black Diaspora histories. 

==Historical Context== Allied Forces and conceived of their society as "post-racial" despite conditions on the ground which indicated a still very racialized society. Despite German roots through their mothers, many popular discourses described these children as "not belonging and at risk in Germany" due to their skin color and the circumstances surrounding their conception.  

Two distinct dialogues emerged in Germany regarding these "Brown Babies" and their prospects for living in Germany. The first posited that these occupation babies should be integrated into German society, and given the opportunity for equal rights and security in their homeland. The second position held that the children could not possibly be guaranteed a safe future in Germany and should thus be removed to the United States or somewhere else where their blackness would be more socially acceptable. 

==Instances of Racism in the Film==
In the beginning when Toxi makes an entrance into the house, one of the ladies asks "Whats this?" to which another replies "A chocolate girl!" This demonstrates the objectification of Black Germans in white German society. 
Moreover, there is a scene in the movie where Toxi and Uncle Theodores daughters are eating Mohrenkopf treats. Black Germans were sometimes referred to as this, since the treat has a dark chocolate covering.
When Toxi meets the Hamburgs, the family assumes that she is a humorous birthday. The assumption that a young black girl is a prank is an example of German racist views of black people. Comments about Toxi exhibit racist ideology in German. Uncle Theodor states that he does not want Toxi around his children assuming that she will give them a disease. Another family member calls Toxi a "child of shame."

==Symbolism in Toxi==

The white family in Toxi is a microcosm of Germany and Toxi represents the Afro-German population. They are brought to a home out of their will (because of slavery) and surrounded by those who are different from them, many of whom don’t want her to be in their home. Yet one realizes that placing Toxi in a place where she felt she should belong--an orphanage) wasn’t her real home, which is the case we find with many Afro-Germans. They may go to a place where they feel they belong but they end up not finding who they really are. This places them in a place of limbo because they are not accepted in either their new or original homelands. They are lost. Toxi is later embraced by her new community, but seems to be really at home when her father arrives.

==Genre and Audience==

Toxi is considered a melodrama as it delves into female suffering (specifically Toxi’s suffering) to appeal to emotion. It also proves true to be a melodrama because it presents overreacted emotions and big exxageration as a way to use emotion. Such emotions are an attempt to prove that blacks are human through sadness, depression and suffering. However, melodramas, and Toxi specifically, enforce the idea that black people are exceptionally strong given the help of white people. Even though this is an Afro-German narrative,  this story is told by a white German director, Robert Stemmle, which may explain why Whites appear to be the heroes in this movie. Because Whites are initially responsible for “saving” Toxi and they all eventually embrace Toxi, it seems the intended audience for this is generally White.
 

Specific examples of the melodramatic elements in the film include the scene when Toxi is seen singing for her food after she arrives at the new household. Its as if she needs to entertain her new white family in order to be fed. Another melodramatic scene occurs when Toxi is seen doing a very similar action when she is taken by the caravan family. She is seen singing and dancing before they feed her and send her to bed. Finally, when Toxis African-American father arrives, Toxi leaps into his arms and begins to count in English - that being the only English she knows how to speak. After her adventurous day with Uncle Theodor, it seems Toxi can do no wrong and deserves to go home to her actual family, although in reality, her assimilation to America would have been a difficult one as her father did not speak German and Toxis English was minimal and barely useful for actual communication. Toxi is portrayed as the perfect child every time she is rewarded with something good or exciting.

==Cast==
* Elfie Fiegert as Toxi
* Paul Bildt as Grossvater Rose 
* Johanna Hofer as Grossmutter Helene 
* Ingeborg Körner as Herta Rose 
* Carola Höhn as Charlotte Jenrich 
* Wilfried Seyferth as Theodor Jenrich 
* Sylvia Hermann as Ilse 
* Karin Purschke as Susi 
* Elisabeth Flickenschildt as Tante Wally 
* Rainer Penkert as Robert Peters 
* Ernst Waldow as Ubelhack 
* Erika von Thellmann as Frau Übelhack 
* Willy Maertens as Krim-Inspektor Plaukart 
* Lotte Brackebusch as Frau Berstel 
* Al Hoosmann as James R. Spencer 
* Gustl Busch as Anna 
* Julia Fjorsen as Fanny 
* Katharina Brauren as Vorsteherin 
* Gertrud Prey as Fursorgeschwester 
* Ursula V. Bose as Krankenschwester 
* Leila Negra as Singer 
* Renate Feuereisen as Frau im Wohnwagen

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Biess, Frank & Roseman, Mark & Schissler, Hanna. Conflict, Catastrophe and Continuity: Essays on Modern German History. Berghahn Books, 2007.
* Davidson, John & Hake, Sabine. Framing the Fifties: Cinema in a Divided Germany. Berghahn Books, 2008.
* Fenner, Angelica. Race Under Reconstruction in German Cinema: Robert Stemmles Toxi. University of Toronto Press,  2011.

==External links==
* 

 

 
 
 
 
 
 
 
 


 