The Abyss of Repentance
{{Infobox film
| name           = The Abyss of Repentance
| image          =
| image_size     = 
| caption        = 
| director       = Wiktor Bieganski
| producer       = 
| writer         = Wiktor Bieganski
| narrator       = 
| starring       = Antoni Piekarski   Ryszard Sobiszewski   Halina Maciejowska   Jerzy Starczewski
| music          = 
| editing        = 
| cinematography = Zbigniew Gniazdowski   Stanislaw Sebel
| studio         = Kinostudia 
| distributor    = 
| released       = 7 January 1923
| runtime        = 
| country        = Poland
| language       = Silent   Polish intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Polish silent silent drama film directed by Wiktor Bieganski and starring Antoni Piekarski, Ryszard Sobiszewski and Halina Maciejowska. The film was shot and set in the Tatra Mountains. 

==Cast==
* Antoni Piekarski as Lesniczy 
* Ryszard Sobiszewski as Molski, father 
* Halina Maciejowska as Maria Mary Molska 
* Wiktor Bieganski as Ryszard Rysio Molski, Marys brother 
* Jerzy Starczewski as Narzeczony Marii 

==References==
 

==Bibliography==
* Haltof, Marek. Polish National Cinema. Berghahn Books, 2002.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 