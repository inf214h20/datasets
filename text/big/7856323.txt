Dr. Jekyll and Mr. Hyde (1912 film)
{{Infobox film
| name = Dr. Jekyll and Mr. Hyde
| image =
| image_size =
| caption =
| director = Lucius Henderson
| producer = Thanhouser Company
| writer = Robert Louis Stevenson (novel)
| based on       =  
| narrator =
| starring = James Cruze, Florence La Badie
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 12 minutes (One-Reel film)
| country = United States
| language = Silent film  English
| budget =
| preceded_by =
| followed_by =
}}
  1912 horror play version (1887) of Thomas Russell Sullivan. Directed by Lucius Henderson, the film stars actor (later noted film director) James Cruze as the dual role of Jekyll/Hyde.

==Plot summary== taloned beast now appears in the chair. After repeated use, Jekylls evil alter ego emerges at will, causing Jekyll to murder his sweethearts father. The evil personality scuttles back to the laboratory only to discover that the antidote is finished and that he will be as Mr. Hyde forever. A burly policeman breaks down Jekylls door to find that the kindly doctor is dead after taking poison.

==Production==
This film was produced by Thanhouser Company. In a 1963 interview, Harry Benham revealed that while Cruze played both Jekyll and Hyde, he and Cruze shared the role of Hyde, with Benham appearing as Hyde in some scenes.

==Copyright Status==
The film is in the public domain, along with all films released before 1928.

==Cast==
*James Cruze as Dr. Jekyll and Mr. Hyde (character)|Dr. Jekyll/Mr. Hyde
*Florence La Badie as Jekylls sweetheart
*Marie Eline as Little girl knocked down by Hyde
*Jane Gail (Extra)
*Marguerite Snow (Extra)

===Uncredited===
* Harry Benham as Mr. Hyde (some scenes) (source: IMDB)

==See also==
*List of rediscovered films

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 