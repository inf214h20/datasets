St Trinian's (film)
 
 
{{Infobox film
| name           = St Trinians
| image          = St Trinians (2007 film).jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Oliver Parker Barnaby Thompson
| producer       = Oliver Parker Barnaby Thompson
| screenplay     = Piers Ashworth Nick Moorcroft Additional material Jamie Minoprio Jonathan M. Stern
| based on       =  
| starring       = Rupert Everett Colin Firth Russell Brand Talulah Riley Lena Headey Gemma Arterton
| music          = Charlie Mole
| cinematography = Gavin Finney
| editing        = Alex Mackie
| studio         = Ealing Studios Fragile Films UK Film Council
| distributor    = Entertainment Film Distributors
| released       =  
| runtime        = 97 minutes 100 minutes  
| country        = United Kingdom
| language       = English
| budget         = £7 million (US$11.4 million)
| gross          = $29,066,483
}} rebooting of St Trinians anarchic school for uncontrollable girls run by eccentric headmistress Camilla Dagey Fritton (the reboot continues the tradition, established by Alastair Sim in the original film, of casting a male actor to play the female headmistress, with Rupert Everett inheriting the role).

==Plot==
  St Trinians, Headmistress is head girl cliques – Posh Totty, Chavs, Emos, Geeks, and First Years.

In her room, a booby trap results in gunge and feathers being dropped on Annabelle, who attempts to clean up in the shower, but falls victim to another trap when the girls set up cameras directly connected to YouTube, steal her clothes and towels, and turn down the water temperature, forcing her to run through the halls naked. Once dressed, Annabelle phones and demands to be taken home. Carnaby, however, pretends to lose reception as he is enjoying his freedom without Annabelle. In frustration, Annabelle hits her mobile phone with a hockey stick across the hall and smashes a marble bust into pieces. Miss Cleaver, the Sports Teacher, sees this and instantly drafts her into the school hockey team.
 Flash Harry pays a visit to the school to deal bootleg goods, and makes a deal with the girls for some lab-made vodka, that may or may not cause blindness and death after two glasses.

A few days later, St Trinian hosts a hockey match against Annabelles old school – their rivals Cheltenham Ladies College, captained by Verity Thwaites, the school bully; Annabelle was her favourite victim. The girls give a hostile and violent welcome to their visitors and the match ends in a brawl; the St Trinians matron acts as the referee and does not know the rules, so the girls cheat excessively. Eventually, there is a tie and the final shot is left up to Annabelle who is intimidated by Verity. Kelly takes her place and scores, knocking Verity out in the process.
 a girl trapped in a tank of formaldehyde in the art rooms and the illegal vodka, which is so strong that he instantly becomes tipsy after licking the tiny amount on his finger. He accidentally knocks over a tank of biting ants, which crawl all over his shoes and up his trousers. Geoffrey is about to escape the school when he hears the Posh Totties coming in. He hides from them in their dressing room. Still in hiding Geoffrey tries to remove his trousers to prevent him from being bitten by the ants when his phone rings, causing the Posh Totty to discover him, pants down, and throw him out the window into a fountain. Drenched and humiliated, he declares war on Miss Fritton and the school.

Celebrating their win over Cheltenham, the St Trinians girls have a wild party. The next morning, a banker named Steve Furst stops by to inform Miss Fritton that St Trinians owes the bank over half a million pounds, and if they dont pay the money in four weeks the school will be shut down. Kelly and the head of the Geeks, Polly, learn of this and Kelly tells the girls they must earn the money or else they will have to go to normal schools. Some days later through electronic spy cameras, the girls and especially Annabelle witness Carnaby attempting to broker a business deal with Miss Fritton. Carnaby wants to sell the school; that way they can repay the debt and split the profits. She refuses, inquiring about Annabelle. Carnaby reveals his true feelings about Annabelle – he does not like her and is not sure if he is really her father.
 Scarlett Johansson" National Art Gallery, where it is being held, to scope out the scene. After seeing how difficult it is going to be, they give up, until the new English teacher Miss Dickinson casually mentions that the finals of the TV quiz show School Challenge competition are to be filmed in the grand hall of the gallery.

As they make their plans, Geoffrey decides to show the media exactly what St Trinians is like. However, the girls spot them and change around the classrooms until they are in perfect order. The only classroom that isnt changed is the one where preparations for the heist are taking place. As the press are about to enter, Annabelle manages to distract them by revealing that Verity was the school bully at her old school. Miss Fritton arrives down the stairs dressed as Queen Elizabeth I. In the midst of the conversation between them, Miss Frittons dog Mr.Darcy once again tries to mate with Geoffreys leg. Irritated, Geoffrey kicks him, sending him flying out the window, crashlanding in the lawnmower and instantly killing him.

In the middle of the night, Annabelle is woken up by the girls and, as thanks for standing up for the school, is given a confidence-boosting makeover. Afterwards, when Kelly asks Annabelle how she feels, she replies "Like a St Trinian!". While still not fully accepted, she feels much stronger in herself.
 Eton team is rendered clueless by their seduction).

During the final, Kelly, Taylor and Andrea foil the security for the painting that theyre after and eventually retrieve it. On the way back the cable that they are using snaps just after Taylor and Andrea get over to the other side. Kelly gets stuck with no way to get to other side. Miss Fritton sees that Kelly is trapped and goes to help, having to also distract Geoffrey through a combination of liquor and seduction. Meanwhile, Annabelle takes care of Veritys threat to foil the girls escape route by shooting her walkie-talkie with her hockey stick at Verity, knocking her out. At the same time, the girls win School Challenge.

The school successfully gets the painting and pretend to sell it to Carnaby who fails to realise until he buys it that it is only a copy painted by Miss Fritton and not the genuine article, which has been returned to the police under the cover of St Trinians finding it. He faints and Flash, dressed as Gerhardt to give him the art, races to the car that is waiting for him which is driven by Kelly. They receive a reward of £50,000 for returning the real painting, which they supposedly found. With the cash from Carnaby they pay back the money owed to the bank and St Trinians stays in business.

==Cast==
* Gemma Arterton as Kelly Opossum Jones, the Head Girl
* Rupert Everett as Miss Camilla Dagey Fritton, St Trinians Headmistress
** Everett also plays Carnaby Fritton, Camillas brother, Annabelles father
* Colin Firth as Geoffrey Thwaites, the Education Minister Flash Harry, the spiv
* Talulah Riley as Annabelle Lealla Fritton, the new girl
* Tamsin Egerton as Chelsea Parker, Posh Totty #1
* Antonia Bernath as Chloe, Posh Totty #2
* Amara Karan as Peaches, Posh Totty #3
* Paloma Faith as Andrea, the Emo
* Juno Temple as Celia, the "Trustafarian" one
* Kathryn Drysdale as Taylor, the Chav
* Lily Cole as Polly, the Geek
* Cloe and Holly Mackie as Tara & Tania, the Twins
* Lena Headey as Miss Dickinson, the English Teacher
* Fenella Woolgar as Miss Cleaver, the Sports Teacher
* Caterina Murino as Miss Maupassant, the Foreign Languages Teacher
* Jodie Whittaker as Beverly, the receptionist
* Toby Jones as St Trinians Bursar
* Celia Imrie as St Trinians Matron
* Stephen Fry as Himself, the School Challenge presenter
* Anna Chancellor as Miss Bagstock, Cheltenhams Headmistress school bully
* Mischa Barton as JJ French, the PR Guru, and previous Headgirl
* Steve Furst as Bank Manager
* Dolly the Dog as Mr Darcy, the schools dog

The members of Girls Aloud (Nicola Roberts, Kimberley Walsh, Sarah Harding, Nadine Coyle and Cheryl Fernandez-Versini) all make cameo appearances as the members of St Trinians school band, and cameos in the film itself. Zöe Salmon also makes a cameo appearance as an emo girl, while Nathaniel Parker, the directors real-life brother, makes a short appearance as the Chairman of the National Gallery. Jeremy Thompson also briefly appears, as himself.

==Release== premiered in London on 10 December 2007 and in wide release on 21 December 2007, it was then released in Australia on 27 March 2008 and in New Zealand on 17 April 2008. Other European releases were planned for Belgium on 9 July 2008, and Germany on 7 August 2008.   The film was given a limited release in North America on 9 October 2009. 
 I Am The Golden Compass. It ranks in the top grossing independent British films of the past decade. 

The film was released on DVD in March 2008; and in the US on 26 January 2010. The DVD was released in Canada on 11 August 2009. 

==Critical reception==
St Trinians received mixed reviews. Empire (magazine)|Empire wrote that the film "fuse  an understanding of what made the originals great with a modern feel – the writers have fulfilled their end of the bargain, even tweaking some of the weaker points of the original story."  The Observer wrote that it "is raucous, leering, crude and, to my mind, largely misjudged, with Rupert Everett playing Miss Fritton as a coquettish transvestite with the manners of a Mayfair madam. The attempts to shock us fail, though Cheltenham Ladies College may well be affronted to hear one of its teachers say between you and I. But the preview was packed with girls aged from seven to 14 who found it hilarious, and especially enjoyed Russell Brand." 

Derek Malcolm in the Evening Standard wrote: "Structurally, the new movie is a mess, and it doesnt look too convincing either, with cinematography that uses all sorts of old-fashioned dodges to raise a laugh", and "when you look at it again, the old film was not only superior but rather more radical. This St Trinians looks as if it is aiming at the lowest common denominator, and finding it too often." 

On the film-critics aggregate website Rotten Tomatoes, St Trinians holds a 31% positive rating, with the consensus "Both naughtier and campier than Ronald Searles original postwar series, this St. Trinians leans on high jinks instead of performances or witty dialogue." 

==Soundtrack==
{{Infobox album |  
 | Name        = St. Trinians
 | Type        = Soundtrack
 | Artist      = Various Artists
 | Cover       =
 | Released    =  
 | Recorded    = 
 | Genre       = Pop music|Pop, Dance-pop
 | Length      = 
 | Label       = Universal Music Group
 | Producer    = 
 | Chronology  = St. Trinians original soundtrack
 | Last album  = 
 | This album  = St. Trinians: The Soundtrack (2007)
 | Next album  =   (2009)
}}
{{Album ratings rev1 = BBC Music rev1score = (negative)  rev2 = Digital Spy rev2score = (positive)  rev3 = InTheNews.co.uk rev3score = (3/10) 
}}  Theme to Love Is in the Air". A number of popular singles or current album tracks by artists, such as Mark Ronson, Lily Allen, Noisettes, Gabriella Cilmi, and Sugababes, were included on the soundtrack.

===Track listing===
{{tracklist
| collapsed       = no
| extra_column    = Artist(s)
| total_length    = 
| writing_credits = no

| title1          = Theme to St. Trinians
| extra1         = Girls Aloud
| length1         = 4:29

| title2          = Trouble Shampoo cover
| extra2          = Cast of St Trinians
| length2         = 3:33
 Oh My God
| extra3         = Mark Ronson featuring Lily Allen
| length3        = 3:40
 Love Is in the Air
| extra4         = Rupert Everett and Colin Firth
| length4        = 3:50

| title5          = Dont Give Up (Noisettes song)|Dont Give Up
| extra5         = Noisettes
| length5         = 2:31
 Nine2Five
| extra6          = The Ordinary Boys vs. Lady Sovereign
| length6         = 3:04

| title7          = If I Cant Dance
| extra7         = Sophie Ellis-Bextor
| length7         = 3:24

| title8          = Teenage Kicks
| extra8          = Remi Nicole
| length8         = 2:27
 Sanctuary
| extra9         = Gabriella Cilmi
| length9         = 3:29
 Love Is a Many-Splendored Thing
| extra10          = The Four Aces
| length10         = 2:59
 3 Spoons of Suga
| extra11         = Sugababes
| length11         = 3:51

| title12          = On My Way to Satisfaction
| extra12          = Girls Aloud
| length12         = 4:06

| title13          = The St Trinians School Song
| extra13          = Cast of St Trinians
| length13         = 3:47
}}

==Sequels==
It was announced at the 2008  , also directed by Oliver Parker and Barnaby Thompson, would be released in 2009.  Filming began on 6 July 2009,  and on 7 July 2009, it was announced that David Tennant, Sarah Harding and Montserrat Lombard had all signed on to appear in the sequel.  In December 2009, it was announced that there will be a St Trinians 3.

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 