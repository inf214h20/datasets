Spaced Invaders
{{Infobox Film |
|  name           = Spaced Invaders|
|  image          = Spaced_invaders_poster.jpg|
|  caption        = Theatrical Release Poster
|  writer         = Patrick Read Johnson Scott Lawrence Alexander Fred Applegate Wayne Alexander J.J. Anderson Patrika Darbo Tonya Lee Williams|
|  director       = Patrick Read Johnson|
|  music          = David Russo|
|  editing        = Seth Gaven Daniel Gross|
|  cinematography = James L. Carter|
|  producer       = Luigi Cingolani|
|  studio         = Touchstone Pictures | Buena Vista Pictures |
|  released       =   |
|  runtime        = 100 minutes|
|  language       = English |
|  budget         = $5,000,000 (estimated) |
|  gross          = $15,369,573
}}

Spaced Invaders is a 1990 science fiction comedy directed by Patrick Read Johnson and starring Douglas Barr, Royal Dano and Ariana Richards.

==Plot==
The space armada from Mars, known as the Imperial Atomic Space Navy (Battle Group Seven), fights an interstellar war against their long-time enemy, the Arcturans. The armada is forced into battle by Enforcer Droids, tasked to keep the Martian soldiers in line, despite objections by some that it wont work. Meanwhile, an incompetent crew of a small Martian spaceship, from the Civilian Asteroid Patrol, intercept a distress signal from the fleet, followed by a Halloween rebroadcast of Orson Welles 1938 The War of the Worlds (radio)|The War of the Worlds radio dramatization. 

Mistaking this for a real invasion and not wanting to miss out on the glory, they land their ship in the tiny community of Big Bean, Illinois and begin their invasion of Earth. The ships smart-mouthed pilot, Blaznee, with more common sense than the others, doesnt think its a good idea, but he is ignored by the rest of the crew: Captain Bipto, the overzealous optimist of the group; Lieutenant Giggywig, the ambitious, know-it-all hothead; Dr. Ziplock, the careful and calculating scientist; and Corporal Pez, who is overeager, yet timid. They search for the invasion fleet they think has already landed. Because its Halloween, everyone assumes they are just kids in very good costumes. Eventually, though, a few locals realize the truth.  Among them is the town sheriff (Barr), his daughter (Richards) and an elderly farmer named Wrenchmuller (Dano), on whose farm the Martians have crash-landed. The sheriff finds out about the aliens when his deputy records their ship doing 3,000&nbsp;mph. 

The deputy tracks down the ship to give the occupants tickets for having no license, no registration, no headlights, no taillight, no wheels, and going 2945 miles over the posted limit. The sheriffs daughter, Kathy, discovers the aliens when they join a group of trick-or-treating kids. She befriends the Martians "Scout-in-a-Can", a small robot that folds up into a sphere and is considered "smart, efficient, easy to use and expendable." Mister Wrenchmuller tries to cash in on the Martians existence in order to save his farm. Captain Bipto gets hit by a truck and turns a gas station attendant named Vern into his robotic slave. Giggywig, Ziplock and Pez try to blow up the towns Co-Op and instead just heat up a silo of corn kernals, creating a gigantic hot air popcorn popper. Kathys new friend, Brian (the Duck) captures Blaznee by hitting him with a trashcan lid. He then tries to help the alien repair his ship. Wrenchmuller tries to blow the ship up and gets trapped in a paralyzing beam. The desperate Martians try to blow the Earth up using the D.O.D. (Doughnut Of Destruction), but it falls apart instead. The Martians finally realize they made a horrible mistake. 

Things get worse when the ships "hyperdriver" starts to go into meltdown, threatening to create a black-hole. Their ships Enforcer Drone wont let them leave, making things even worse. The humans manage to destroy the Enforcer Drone with dynamite and help the grateful "invaders" return to space. As an unintentional gift the Martians jettison their ships sewage tank while flying over Wrenchmullers field to lighten the load on their ship so that they can reach gravitational escape velocity. 

The alien manure rejuvenates the drought-stricken farmland and turns the regular green beans (for which the town is famous) into gigantic, 6 foot tall pods, enabling Wrenchmuller to save the town from greedy real-estate developers. As the Martians head home Captain Bipto suggests they go to Arcturus to "help torture prisoners", which is shot down by the rest of the crew.

==Cast==
*Douglas Barr as Sheriff Sam Hoxly
*Royal Dano as Mr. Wrenchmuller
*Ariana Richards as Kathy Hoxly
*Gregg Berger as Steve W. Klembecker Fred Applegate as Deputy Russell Pillsbury Wayne Alexander as Vern
*J.J. Anderson as Brian Hampton (Duck)
*Patrika Darbo as Mrs. Vanderspool
*Tonya Lee Williams as Ernestine
*Kevin Thompson as Blaznee
*Jimmy Briscoe as Captain Bipto Tony Cox as Corporal Pez
*Debbie Lee Carrington as Dr. Ziplock
*Tommy Madden as Lieutenant Giggywig

===Voices===
*Kevin Thompson as Blaznee
*Jeff Winkless as Captain Bipto
*Tony Pope as Corporal Pez
*Joe Alaskey as Dr. Ziplock
*Bruce Lanoil as Lieutenant Giggywig
*Patrick Johnson as Commander/Enforcer Drone
*Kirk Thatcher as Shortstuff

== Allusions==
*During the end title music, there is an homage to the TV series My Favorite Martian as the first bars of the shows theme song are sung by one of the Invaders.

*During the scene where martians accidentally launch the mortar-like "Scout in a Can" Captain Ziploc exclaims "What in the name of Uncle Martin is that?"  Also a nod to the TV series My Favorite Martian.

*For most of the movie the deputy has a half sunburned face.  A tribute to what happened to "Roy" (Richard Dreyfuss) in Close Encounters of the Third Kind.

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 