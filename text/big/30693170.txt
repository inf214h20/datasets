Harumi: The Beauty of Spring
{{multiple issues|
 
 
 
}} 
{{Infobox film
| name           = Harumi: The Beauty of Spring
| image          = 
| director       = Ghislaine Heger
| producer       = Ghislaine Heger
| writer         = Ghislaine Heger
| cinematography = Ghislaine Heger Patrick Tresch
| editing        = Prune Jaillet
| distributor    =  (CH) 
| released       =  
| runtime        = 14:06
| language       = French
}} documentary written, directed and produced by Ghislaine Heger. The film showed at the New York United Film Festival on 22 October 2010. 

This short documentary is a poetic portrait of jewelry designer Harumi Klossowska, daughter of the late painter Balthus and Japanese artist Setsuko. Set in and around the Grand Chalet in Rossiniere, Switzerland, it sketches Harumis profile through her choices and the words of people close to her.

==References==
 

==External links==
*  

 
 
 
 
 


 