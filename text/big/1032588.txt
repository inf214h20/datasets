Ladder 49
{{Infobox film
| name = Ladder 49
| image = Ladder_49_poster.JPG
| caption = Theatrical release poster
| director = Jay Russell
| producer = Casey Silver
| writer = Lewis Colick
| starring = Joaquin Phoenix John Travolta Jacinda Barrett Morris Chestnut Robert Patrick William Ross
| cinematography = James L. Carter
| editing = Bud S. Smith M. Scott Smith Casey Silver Productions Buena Vista Pictures
| released =  
| runtime = 115 minutes
| country = United States
| language = English
| awards =
| budget = $55 million 
| gross = $100.5 million 
}}
 Baltimore firefighter Jack Morrison, who is trapped inside a warehouse fire, and his recollection of the events that got him to that point. The movie is a celebration of the firefighting profession and the lifestyle associated with it. The film starred Joaquin Phoenix and John Travolta.

==Plot== Baltimore firefighter Canton waterfront flashbacks showing how Jack joined the fire department, his first meeting (at a supermarket) with the woman who would eventually become his wife (Jacinda Barrett), his relationship with his children, and the bonds he formed and the trials and tribulations he endured with his fellow firefighters.

After graduating from the fire academy, Jack is sent to work on Baltimore City Fire Department (BCFD) Engine Company 33, in the busiest firehouse in the city. Quartered with Engine 33 is Ladder Company 49. On Engine 33, Jack learns the ropes of firefighting. He quickly becomes close friends with his fellow firefighters, including Mike Kennedy, his Captain at the time. Jacks first fire takes place at a burning vacant rowhouse. Engine 33 and Ladder 49 respond and are the first companies on the scene. Jack and Mike enter the building with a hose line and tackle the blaze, with Jack on the nozzle of the hose. They quickly and triumphantly extinguish it.
 Billy Burke), dies after falling through a roof of the building. Jack decides, although it is more dangerous, to take his friends position as a "truckie", a search and rescue member on Ladder 49 by transferring to the Truck.

As the years go by, Jack suffers some traumatic experiences, including rescuing a man from the ledge of a burning high-rise building in Downtown Baltimore, and witnessing another friend and fellow firefighter from Ladder 49, Tommy Drake (Morris Chestnut), burned beyond recognition following a steam explosion at an industrial building who continues firefighting even after sustaining such a terrible injury. He finds the work rewarding, but his wife initially worries about his safety and opposes the change. However, she eventually accepts his new role and even talks him out of taking an administrative position that Mike, who has now become a Deputy Chief, offers him.
 Medal of Valor for their actions.

Back at the grain building fire that opened the film, Jacks fellow firefighters become extremely determined to rescue him, and Jack does his best to reach the only possible safe area Mike tells him about. However, upon reaching that room he sees that the only exit is cut off by raging flames, and out of breathing air with the heat intesifying, Jack realizes his situation is hopeless. He radios Mike to pull his men back, so no one else will be hurt while trying to rescue him. Mike reluctantly agrees, and Jack accepts his fate to die in the fire, devastating Mike.

At Jacks funeral, Mike delivers an emotional eulogy in celebration of Jacks life, which inspires a standing ovation from friends and family in attendance. Jacks body is then carried to his resting place, with full honors, on the back of Engine 33 in a typical firemans funeral procession. The film ends  with flashbacks of Jack and his fellow firefighters going to fires and a final shot of Mike and Jack coming out of Jacks first ever burning building in triumph.

==Songs==
Robbie Robertson contributed the films theme song, "Shine Your Light". He also composed an adagio for the end credits. The film also features "Love Sneakin Up On You" by Bonnie Raitt, among others. 

==Cast==
*Joaquin Phoenix as Firefighter Jack Morrison, Ladder 49 (formerly Engine 33)
*John Travolta as Deputy Chief (formerly Captain) Mike Kennedy, Deputy Chief 1 (formerly Engine 33)
*Robert Patrick as Firefighter Leonard "Lenny" Richter, Ladder 49
*Jacinda Barrett as Linda Morrison
*Morris Chestnut as Firefighter Tommy Drake, Ladder 49 Tillerman
*Balthazar Getty as Firefighter Ray Gauquin, Ladder 49 Billy Burke as Firefighter Dennis Gauquin, Ladder 49
*Tim Guinee as Captain Tony Corrigan, Ladder 49
*Kevin Chapman as Frank McKinney, Engine 33
*Jay Hernandez as Probationary Firefighter Keith Perez, Engine 33
*Kevin Daniels as Firefighter Engineer Don Miller, Engine 33
*Steve Maye as Firefighter Pete Lamb, Engine 33
*Robert Logan Lewis as Firefighter Ed Reilly, Ladder 49
*Spencer Berglund as Nicky Morrison
*Brooke Hamlin as Katie Morrison
*Sam Stockdale as Himself
*Paul Novak, Jr. as the Dispatcher
*Mayor of Baltimore Martin OMalley as himself

==Reception==
Ladder 49 grossed $74,463,263    at the US box office and $102,332,848 worldwide,    and has generally received mixed reviews. It received a rating of 3.5 out of 4 stars from Roger Ebert,  and it has received a rating of 40% on Rotten Tomatoes based on 159 reviews and an overall rating of 47/100 from Metacritic, based on 32 reviews, resulting in "Mixed or Average Reviews."   

==Trivia==
 
There is an actual Engine Company 33 in the Baltimore City Fire Department|B.C.F.D; however, there is no Ladder 49. Also, Ladder Companies are referred to as "Truck" Companies in Baltimore; thus Ladder 49 would really be designated as "Truck 49". The highest Truck Company number in the BCFD is Truck 30. Also, the fire department dispatching heard during the film accurately portrays the fire dispatch protocol used by the BCFD.

The firehouse used in the film was the former quarters of Engine 33, a real company in the BCFD, located at 1751 Gorsuch Ave. The firehouse was closed in 2000 when Engine 33 moved to a new firehouse several blocks away. However, the building was refurbished back to its original condition for the filming.

==See also==
*List of firefighting films
*List of films shot in Baltimore
*Worcester Cold Storage and Warehouse fire

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 