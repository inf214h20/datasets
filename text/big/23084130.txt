Ben and Arthur
{{Infobox film
| name           = Ben and Arthur
| image          = 
| image_size     = 
| caption        = 
| director       = Sam Mraovich
| producer       = Sam Mraovich
| writer         = Sam Mraovich
| starring       = Sam Mraovich Jamie Brett Gabel Michael Haboush Bill Hindley Julie Belknap Gina Aguilar
| music          = Phil Garcia Michael Haboush Chris Mraovich Robert Mraovich Sam Mraovich
| cinematography = Michael Haboush Sam Mraovich
| editing        = Chris Mraovich Sam Mraovich
| distributor    = Ariztical Entertainment
| released       = September 9, 2002 
| runtime        = 85 min
| country        = United States
| language       = English
| budget         = 
| gross          = $40,000
}}
 one of the worst movies ever made.

==Plot== comes out to her, and asks her for a divorce.

After the disappointment of their near-wedding, Ben and Arthur resume their daily life, working in a small diner in Los Angeles, where Ben is a dishwasher and Arthur is a waiter. Although Ben—a former nurse who quit to pursue a music career—enjoys the manual labor and hours, Arthur has grown impatient with servitude and putting up with needy customers. One night, Arthur decides to quit and go back to college, so that he can earn an MBA and open up his own sex shop. Although the loss of income to the household means that Ben will have to quit and return to being a nurse, he agrees to do so in order to help Arthur pursue his dream.
 turn straight, he nonetheless offers to give Arthur money for college if he will bring Ben by the apartment and allow him to evangelize.

While Arthur considers Victors offer, he and Ben hire an attorney (Gina Aguilar) to consult for advice on getting married. Despite Bens still being legally married to Tammy, the attorney counsels them to travel to Vermont, be wed in a civil union, and then return to California and attempt to be recognized as members of a domestic partnership. The two take her advice, and are wed in a private ceremony in Vermont. 

Suspicious of Arthurs lack of response to his monetary offer, Victor hires a private investigator to tail Ben and Arthur. The PI tells Victor about the mens marriage and their attempts to get their union recognized in California. In response, Victor follows the attorney home one evening and shoots her to death in her apartments parking garage. At the same time, Tammy arrives at Arthurs apartment and tries to force Ben to take her back at gunpoint, but Ben successfully disarms her and throws her out.
 exorcise the excommunicated because bad karma negative energy. A dejected Victor reaches out to Stan for help, who helps Victor come to an agreement with Father Rabin that Victor will be permitted to rejoin the church if he successfully murders Ben and Arthur. To this end, they hire a hitman named Scott (Nick Bennet), whom Father Rabin has apparently used to kill gay people in the past.
 gay bashed taps his phone. After intercepting a call implicating Victor and Father Rabin, Arthur goes to Victors church, chloroforms Father Rabin, and then murders him by burning the church down with him still inside.

After Ben has sufficiently recovered, Arthur takes him back home to their apartment. Deciding that the next attempt on the mens lives must be more drastic, Victor and Scott go Ben and Arthurs apartment with guns; at the last minute, Victor tells Scott that he wants to kill them himself and sends him away. Victor rings the apartments doorbell, and when Ben answers, he fatally shoots him. He then forces Arthur to strip naked at gunpoint and performs an impromptu baptism in the bathtub.

While Victor contemplates what hes done, Arthur slips away and gets the gun that Ben had earlier confiscated from Tammy. Dressed in a bathrobe and briefs, a hysterical Arthur—reenacting one of the final scenes of Scarface (1983 film)|Scarface—propositions Victor while holding him at gunpoint, accusing him of lashing out to try to combat his own repressed homosexuality. When Arthur fires a warning shot, Victor pulls out his own gun and shoots Arthur in the chest and back several times. In turn, Arthur manages to fire off a single shot which hits Victor in the forehead and instantly kills him, before Arthur dies of his own wounds.

==Production and release==
 The Entertainer" Canon in D, which play over the opening and closing credits, respectively.  

The original script featured a female antagonist named Victoria, as opposed to a male antagonist named Victor. Some of Victors lines are still listed as belonging to "Victoria" in the films final shooting script, although any references as to her relationship with Arthur have been excised.  Following the films release, Sam Mraovich received numerous complaints that, although ostensibly a homophobe, the character of Victor is not only played by a gay actor but is portrayed as flamboyantly gay in the film (Michael Haboush, who plays Victor, is in fact openly gay and a veteran of gay pornography).  Mraovich responded saying that the casting and direction were intentional, and meant to convey that Victor is a self-loathing gay man in the vein of James McGreevy:

 James McGreevey could not deny it. Gov. James McGreevey was totally against gay marriage and didnt even support civil unions or domestic partnerships. Also, look at Mayor Jim West, hes totally gay yet against gay rights and says bad things about us but privately he was giving his gay lovers jobs... the bottom line is the character in my movie, Victor, is an example of a gay man who hates himself for being gay."   

The film premiered at the Sunset 5 theater in West Hollywood. It was released on Region 1 DVD in the United States in early 2003. As of 2011, the film has only earned $40,000, making it a box office bomb. 

==Reception==
 The Room is the over-wrought, melodramatic and self-pitying heterosexual camp classic of choice, then Sam Mraovichs Ben & Arthur is its gay equivalent." Rotten Tomatoes went on to cite the poor production values and Arthurs "hissy fits", concluding that the quality of the film was so poor that "Mraovich might as well have shot his story of gay persecution and fightback on a cell phone".  Pop culture review site insert-disc likewise compared it to The Room, stating "The Room was better than this. The acting, special effects, music, and writing are bad even compared to The Room.    Total Film included Ben and Arthur in their list of the 66 worst films of all time. 

The film also took particularly criticism from the gay community: The gay pop culture site Queerty called it "the worst gay movie ever", only to later retract the "gay" qualifier and simply declare it "worst. movie. ever."  The gay movie review site Cinemaqueer likewise indicated that it was the worst film to have ever been featured on the site, suggesting that the film was too bad to even be parodied on Mystery Science Theater 3000. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 