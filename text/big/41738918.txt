Ankhon Dekhi
 
 
{{Infobox film
| name           = Aankhon Dekhi
| image          = Ankhon Dekhi – Poster 1.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Rajat Kapoor
| producer       = Manish Mundra
| writer         = Rajat Kapoor Sanjay Mishra Seema Pahwa Rajat Kapoor
| music          = Sagar Desai
| sound design   = Sujay Jinturkar  Sujay Jinturkar
| cinematography = Rafey Mahmood
| editing        = Suresh Pai
| studio         = Mithya Talkies
| distributor    =  
| released       =  
| runtime        = 108 minutes 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Hindi drama Sanjay Mishra Best Supporting Best Story and Best Ensemble Cast. 

== Plot == Sanjay Mishra) is a man in his late fifties, living out a dreary but eventful life in a small house in old
Delhi- where he lives with his extended family.
A random incident is going to change his life in a dramatic way- though he does not realise at
the moment. Baujis daughter has been seeing a boy of ill repute. When that fact is revealed to
the family, after much deliberation they decide to do the obvious- lock up the girl and go beat
the wilful boy. It is a funny old journey of this man, this old fool- who is both Lear and the fool. 

==Cast==
*  Sanjay Mishra (actor) as Bauji
*  Rajat Kapoor as Rishi Chacha
*  Seema Pahwa as Amma
*  Namit Das as Ajju
*  Shripad Raorane as Sharma ji
*  Brijendra Kala as Shibbo Babu
*  Manu Rishi as Sharma ji
*  Maya Sarao as Rita
*  Taranjit Kaur as Chachi
*  Chandrachoor Rai as Shammi
*  Alka Chawla as Sarup Bua
*  Mahesh Sharma as Bauaa
*  Anil Chaudhary as Chaudhary Saab
*  Shrikant Varma as Master Ji
*  Manish Karnatak as Jeevan
*  Dhruv Singh as Dhruv
*  Saurabh Shukla as Boss
*  Danish Hussain as Gopi
*  Yogendra Tiku as Pandit
*  Chaitanya Mahawar as Ashok
*  Chetan Sharma as Anil
*  Shivam Sethi as Arun
*  Ranvir Shorey as the tourist photographer
*  Yadvinder Singh Brar as Goon

==Critical Response==
Aankhon Dekhi opened to immense critical appreciation. Most critics praised all the performances in the film, and the deeply rich philosophical undertones of the movies script.

Rajeev Masand of CNN-IBN gave the film 3.5/5 stars stating, "Through the wonderfully whimsical Ankhon Dekhi, writer-director Rajat Kapoor shows us how the journey could be more meaningful if we lived life the way we choose to."

Anupama Chopra of Hindustan Times gave the film 3.5/5 and mentioned, "the strands come together with such a heave of emotion that I found myself wiping away tears. Ankhon Dekhi is a lovely respite from the formulaic fare that clutters our multiplexes week after week."

Aniruddha Basu of Dear Cinema was very positive about the film particularly Sanjay Mishras lead performance, stating "As the quixotic patriarch, Sanjay Mishra hits just the right notes, making Bauji consistently unpredictable and engaging but never wholly sympathetic." 

Independent film scholar Bharadwaj Rangan praised the acting and casting of the film stating "Watching Rajat Kapoors marvellous Ankhon Dekhi, you may find yourself wishing that we had one of those "Outstanding Performance by a Cast in a Motion Picture" awards. The casting is perfect, the performances exquisite. Its a cliché to say that an actor has "lived" his role – but that sense of not-acting-but-being is all-pervasive here." 

Sukanya Varma of Rediff went with 4/5 stars, saying "For all its existentialism crisis, Aankhon Dekhis heart lies in Kapoors affectionate depiction of humdrum living, the tender father-daughter relationship between Bauji and Rita (Maya Sarao), the unspoken attachment between him and his younger brother and the concerned anxiety of his rock solid wife."

Shubra Gupta of Indian Express usually a very harsh critic, gave the film a surprising 4/5, mentioning in regards to the central character, "He is fool, clown and man, all in one. He is us. I have seen it with my own eyes, and I can tell you that Aankhon Dekhi is a fine , fine film."

Soham Gadre of film blog ExtrasensoryFilms gave a positive review stating "Its definitely a movie to ponder over, and it will take some time to really appreciate what Rajat Kapoor is trying to say. I certainly havent fully digested the films message yet, but like I said, its a very strange movie, and I guess when compared with the rest of Bollywoods cinema (almost all of it tawdry and nonsensical), that alone is enough to recommend it to all." 

One of the few differing voices on the film was Madhureetha Mukherjee of India Today who gave 2.5/5 stars stating "What doesnt work for the film is the fact that it borders on abstract at regular intervals and lacks continuity, making a few scenes and conversations seem out-of-context." However, she still recommended that people watch the film.

==Awards==
;Filmfare Awards
 Filmfare Award for Best Film (Critics) 2014   
 Filmfare Award Sanjay Mishra 

*Won- Filmfare Award for Best Story 2014 – Rajat Kapoor 

;Screen Awards

* Won – Star Screen Award for Best Story – Rajat Kapoor 
 Tabu who won it for Haider (film)|Haider] 

==Music==
Ankhon Dekhi features a very unique soundtrack combining Indian classical melodies with modern rhythms. The score was composed by Saagar Desai, and the lyrics written by Varun Grover.  The soundtrack was well received with many calling it a breath of fresh air due to the melodies being composed with classical instruments. 

{| class="wikitable"
|-
! Song !! Singer !! Composer !! Lyricist
|-
| "Aaj Laagi Laagi Nai Dhoop" || Kailash Kher || Saagar Desai || Varun Grover
|-
| "Aayi Bahaar" || Kailash Kher || Saagar Desai || Varun Grover
|-
| "Kaise Sukh Soyein" || Ronkini Gupta || Saagar Desai || Varun Grover
|-
| "Yaad Saari Baari Baari" || Kailash Kher || Saagar Desai || Varun Grover
|- Shaan || Saagar Desai || Varun Grover
|-
| "Dheeme Re Re" || Mansheel Gujral || Saagar Desai || Varun Grover
|}

==References==
 

==External links==
*  
* 
*  
*  

  

 
 
 
 