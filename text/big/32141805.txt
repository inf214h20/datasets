Desert Gold (1926 film)
 
{{Infobox film
| name           = Desert Gold
| image          = 
| caption        = 
| director       = George B. Seitz
| producer       = Jesse L. Lasky Adolph Zukor
| writer         = Zane Grey Neil Hamilton
| cinematography = Charles Edgar Schoenbaum
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent
| budget         = 
}}

Desert Gold is a 1926 silent American Western film directed by George B. Seitz. According to silentera.com the film survives while Arne Andersen Lost Film Files has it as a lost film.       Portions of the film were shot near Palm Springs, California. {{Cite book
 | last = Niemann
 | first = Greg
 | title = Palm Springs Legends: creation of a desert oasis
 | publisher = Sunbelt Publications
 | year = 2006
 | location = San Diego, CA
 | pages = 286
 | url =  http://books.google.com/books?id=RwXQGTuL1M0C&pg=PA169&lpg=PA169&dq=palm+springs+film+locations&source=bl&ots=N8AgHRYZbg&sig=RMIwy_g3fEV3KoK1aa7P4VxuJWM&hl=en#v=onepage&q=palm%20springs%20film%20locations&f=false
 | doi =
 | id =
 | isbn = 978-0-932653-74-1
 | mr =
 | jfm =
|oclc=61211290}}  ( )  

==Cast== Neil Hamilton as George Thorne Shirley Mason as Mercedes Castanada
* Robert Frazer as Dick Gale
* William Powell as Snake Landree
* Josef Swickard as Sebastian Castaneda George Irving as Richard Stanton Gale
* Eddie Gribbon as One-Found Kelley
* Frank Lackteen as Yaqui Richard Howard as Sergeant Bernard Siegel as Goat Herder
* Aline Goodwin as Alarcons Wife
* Ralph Yearsley as Halfwit
* George Regas as Verd

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 