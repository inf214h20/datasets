The Enemy Below
{{Infobox film
| name           = The Enemy Below
| image          = Enemybelowposter.jpg
| caption        = Movie Poster
| director       = Dick Powell
| producer       = Dick Powell
| based on       =  
| screenplay     = Wendell Mayes
| starring       = Robert Mitchum Curt Jürgens
| music          = Leigh Harline
| cinematography = Harold Rosson
| editing        = Stuart Gilmore
| distributor    = 20th Century Fox
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $1,910,000 
}}
 American destroyer German U-boat during World War II.  The movie stars Robert Mitchum and Curt Jürgens and was directed and produced by Dick Powell. The film was based on a novel by Denys Rayner, a British naval officer involved in anti-submarine warfare throughout the Battle of the Atlantic.
 Academy Award Best Special Effects.    

==Plot== Naval Reserve, has recently taken command of the Haynes, even though he is still recovering from injuries incurred in the sinking of his previous ship. Before the U-boat is first spotted, one sailor questions the new captains fitness and ability. However, as the battle begins, Murrell shows himself to be a match for wily U-boat Kapitän von Stolberg (Curt Jürgens) (portrayed as not being enamoured with the Nazi regime) in a prolonged and deadly battle of wits that tests both men and their crews. Each man grows to respect his opponent.
 torpedoing the executive officer, ram it. With his boat crippled, Von Stolberg orders his crew to set the scuttling charges and abandon ship.
 traditional ceremony, as the American crew respectfully observes.

== Changes from the book ==
The movie script differs substantially from the original book. The ship is changed from British to American. More importantly, the final scenes of mutual respect and potential friendship between the protagonists is not at all how the book ends. In the book the destroyer captain hates the German captain so much he takes a swing at him while theyre in the lifeboat. The movie also vaguely alludes to the "enemy" being evil (or the devil), not particularly the Nazis ("You cut off one head and it grows another..."). This gives the title "The Enemy Below" a double meaning not at all present in the book.

==Cast==
* Robert Mitchum as Captain Murrell
* Curt Jürgens as Kapitän von Stolberg. Jürgens was imprisoned in 1944 in an internment camp in Hungary by order of Nazi propaganda minister Joseph Goebbels during World War II. Contrary to some reports, it was not a death camp. He was released when the war ended.
* Theodore Bikel as Korvettenkapitän Heinie Schwaffer, von Stolbergs second in command. Bikel is an immigrant Austrian Jew who was born in Vienna, Austria, in 1924. He and his family fled to America by way of Palestine in 1937. Al Hedison as Lieutenant Ware, the executive officer of the Haynes
* Russell Collins as Doctor, USS Haynes
* Kurt Kreuger as Von Holem
* Ralph Manza as Lieutenant Bonelli
* Frank Albertson as Lieutenant, Junior Grade Crain, USS Haynes
* Biff Elliot as Quartermaster, USS Haynes
* Doug McClure, in his film debut, as Ensign Merry, USS Haynes

==Production==
The destroyer escort USS Haynes was portrayed by   Walter Smith, played the engineering officer. He is the man seen reading comics (Little Orphan Annie) during the lull before the action.

==Music==
The tune sung by the U-boat crew on the ocean floor between depth charge attacks is from an 18th-century march called "Der Dessauer Marsch". As a more popular song, its also known by the first line of lyrics as "So leben wir" ("Thats how we live").

=="Remakes"== Balance of USS Enterprise cast as the destroyer and the Romulan vessel, using a cloaking device, as the U-boat. 
 Voyage to the Bottom of the Sea episode "Killers of the Deep" was not only based on this movie, it also re-used substantial amounts of footage from it. Also David Hedison (then Al Hedison) who played Lieutenant Ware, the executive officer of the Haynes,  played  Commander Lee Crane.

==In popular culture== Crimson Tide (Tony Scott, 1995), the crew of the USS Alabama goes on board and talks about submarine movies, citing The Enemy Below.

==References==
 
*Rayner, D.A., The Enemy Below, London: Collins 1956

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 