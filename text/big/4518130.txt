Support Your Local Gunfighter
{{Infobox film
| name           = Support Your Local Gunfighter
| image          = Support Your Local Gunfighter-1971-poster.jpg
| image_size     = 280
| caption        = Theatrical release poster
| director       = Burt Kennedy
| producer       = Bill Finnegan Burt Kennedy
| writer         = James Edward Grant and, uncredited, Burt Kennedy 
| starring       = James Garner Suzanne Pleshette Harry Morgan Jack Elam John Dehner Marie Windsor Joan Blondell Jack Elliot  Allyn Ferguson
| cinematography = Harry Stradling Jr.
| editing        = William B. Gulick
| distributor    = United Artists
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
}}

Support Your Local Gunfighter is a 1971 comic western film starring James Garner, directed by Burt Kennedy, and written by James Edward Grant. The film shares many cast and crew members and plot elements with the earlier Support Your Local Sheriff! but is not a sequel. It actually parodies Yojimbo (film)|Yojimbo and its remake A Fistful of Dollars, using the basic storyline of a stranger who wanders into a feuding town and pretends to work as an enforcer for both sides.

==Plot==
Latigo Smith (Garner), a gambler and confidence man, is traveling by train in frontier-era Colorado  with the rich and powerful Goldie (Marie Windsor).  Goldie is besotted with Latigo and wants desperately to marry him, a fate that he wants no part of.  He manages to slip off the train at Purgatory, a jerkwater mining town.  Assessing the situation, he discovers that two rival companies of miners, led by Taylor Barton (Harry Morgan) and Colonel Ames (John Dehner), are racing each other to find a "mother lode" of gold buried somewhere nearby.  Massive dynamite blasts periodically rock the town to its foundations, creating or embellishing various moments of comic relief throughout the film.

Latigo consults the town doctor ( ) impersonating Swifty, he schemes to pay off his debts and skip town before the real Swifty makes his appearance.  In the process, Latigo attracts the attention of Patience "The Sidewinder" Barton (Suzanne Pleshette), the hot-tempered daughter of Taylor, who desperately wants to escape this backwater frontier existence, attend "Miss Hunters College on the Hudson River, New York, For Young Ladies of Good Families", and live a life of refinement in New York City.  Latigo falls hard for Patience, and when he and Jug side with the Bartons in a dispute, Ames sends a telegram to Swifty informing him of the situation.

Swifty arrives in town and immediately challenges the hapless Jug to a gunfight; but at the appointed time and place, Latigo is there in place of Jug, sitting atop a donkey loaded with crates of dynamite in an attempt to bluff the gunfighter.  Swifty calls Latigos bluff; but as he draws his six-shooter, a particularly massive mining blast startles him, and he shoots himself in the foot.  The blast also panics the donkey, who charges into the Bartons saloon with Latigo aboard, blowing the building to smithereens; but the blast uncovers the mother lode, which conveniently sits beneath the Bartons land.  The resulting inferno also fortuitously burns off Latigos troublesome tattoo, while miraculously leaving him uninjured.
 Italian westerns.

==Cast==
* James Garner as Latigo Smith
* Suzanne Pleshette as Patience Barton
* Harry Morgan as Taylor Barton
* Jack Elam as Jug May
* John Dehner as Col. Ames
* Marie Windsor as Goldie
* Dick Curtis  as Bud Barton
* Dub Taylor as Doc Shultz
* Joan Blondell as Jenny
* Ellen Corby  as Abigail Ames
* Kathleen Freeman as Mrs. Martha Perkins
* Virginia Capers as Effie Henry Jones as Ez
* Ben Cooper as Colorado
* Grady Sutton  as Storekeeper
* Chuck Connors as "Swifty" Morgan (uncredited)

==References==
 	

==External links==
* 
*  
*  
*  DEAD LINK

 
 

 
 
 
 
 
 
 
 
 
 