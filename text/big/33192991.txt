Rock the Shaadi
{{Infobox film
| name           = Rock the Shaadi
| image          =
| caption        =  Navdeep Singh
| producer       = Amit Kapoor Sidhartha M. Jain
| story          = 
| screenplay     = 
| starring       = Abhay Deol Genelia DSouza Preeti Desai
| music          = Ram Sampath
| cinematography = 
| editing        = 
| distributor    = Balaji Motion Pictures iRock Films
| released       =  
| country        = India
| language       = Hindi
| runtime        = 
| budget         = 
| gross          = 
}} Navdeep Singh, and starring Abhay Deol and Genelia in the lead roles. The movie has been delayed until further notice. 

Preeti Desai is also seen in a Cameo role in Rock The Shaadi. 

==Cast==
* Abhay Deol as Rohan
* Genelia DSouza as Simran
* Preeti Desai in a cameo
* Kushal Punjabi

==Production==
The film was initially titled Shaadi of the Dead.  But as per sources, producer was not satisfied about "Shaadi Of The Dead" and he was keen to have a better title before the movies shooting starts. So for this producer threw open the challenge to the entire team asking if anyone could come up with a new title. Finally, Abhay had come out with apt suggestion, since he is deeply involved with the movie and its concept, the project has finally titled as Rock the Shaadi. 

Abhay Deol’s girlfriend Preeti Desai has joined the cast of the movie in a cameo role.

"The movie should be a blast because Punjabi weddings are fun, flamboyant and colourful. And this one has zombies too. No, I’ve never met one but loads of people have been trying to chew on my brains for years", laughs Genelia.

Abhay is equally kicked about this Zombie- meets-Punjabi wedding. "Tandoori Bheja Fry will be the main dish. Youre often told to leave you brains behind when you go for a madcap comedy but if you do so in this case, you may end up with the zombies feasting on them", he chuckles. "Rock the Shaadi is one of the funniest scripts Ive read. I wouldnt mind having a big fat Pun-Zom wedding myself." 

==References==
 

==External links==
*   at Bollywood Hungama

 
 
 

 