Arabian Love
{{Infobox film
| name           = Arabian Love
| image          = 
| caption        = 
| director       = Jerome Storm
| producer       =
| writer         = Jules Furthman John Gilbert Barbara Bedford Barbara La Marr
| music          = 
| cinematography = Joseph H. August
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 50 minutes
| country        = United States Silent (English intertitles)
| budget         = 
}}
Arabian Love is a 1922 American silent drama film directed by Jerome Storm. Its survival status is classified as being unknown,  which suggests that it is a lost film.

==Plot==
Shortly after marrying a man, Nadine Fortier travels through the desert to a distant city to visit her dying mother. On her way, she is kidnapped by a group of bandits, who use her when gambling. Nadine eventually becomes the property of Norman Stone, an American criminal who is on the run from the police. Norman helps her to safety and they plan on crossing ways. Nadine, however, contacts him to find her husbands murderer.

Themar, the daughter of a sheik, is jealous of Normans interest in Nadine and she tells Nadine that Norman is responsible for her fathers death. Upon confronting him, Norman admits that her husband had several clandestine meetings with his sister and that he was accidentally shot to death in his presence. Although she is initially mad, their love for each other proves to be more powerful. They eventually become a couple and leave the country for America. 

==Cast== John Gilbert as Norman Stone Barbara Bedford as Nadine Fortier
*Barbara La Marr as Themar
*Herschel Mayall as The Sheik
*Bob Kortman as Ahmed Bey
*William Orlamond as Dr. Lagorio

==Production and release== The Sheik John Gilbert made at Fox Film Corporation flopped, but Arabian Love became a great success. Gilbert was praised for his portrayal of a sheik, but the actor himself loathed it and made sure he later would not appear again in that sort of character.  Barbara La Marr was praised by the critics too, with the film magazine Moving Picture World stating that "the forlorn lovesickness of the sheik’s daughter   unusually effective". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 