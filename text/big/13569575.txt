The Spider Returns
{{Infobox film
| name           = The Spider Returns
| image          = The Spider Returns.jpg
| image_size     =
| caption        =
| director       = James W. Horne
| producer       = Larry Darmour Lawrence Taylor John Cutting Harry L. Fraser Jesse Duffy George H. Plympton Screenplay and history based on the pulp magazine character created by Norvell Page
| narrator       = Dave OBrien Joseph W. Girard Kenne Duncan Corbet Harris
| music          = Lee Zahler
| cinematography = James S. Brown Jr. Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 chapters 300 minutes
| country        = United States English
| budget         =
| gross          =
}} 1941 15-chapter Columbia Serial movie serial 1938 serial The Spiders Web. The first episode runs 32 minutes, while the other 14 are approximately 17 minutes each.

==Plot==
Amateur criminologist Richard Wentworth, formerly the masked vigilante, The Spider, brings his  former alter ego out of retirement for 15 action-packed chapters to help his old friend, police commissioner Kirk (Kirkpatrick in the pulp novels), battle a dangerous, power-obsessed maniac called The Gargoyle. This mysterious crime lord and his henchmen threaten the world with acts of sabotage and wholesale murder in an effort to wreck the U. S. national defense. 

==Production==

Columbia Pictures used their original serial The Spiders Web as the basic template for many of its early serials: the daring hero and his assistants adopt disguises to battle an exotic, secretive villain and his lawless gang. In The Spider Returns, The Gargoyle wears robes which would not look out of place being worn by Flash Gordons longtime nemesis Ming the Merciless. 

Both serials feature a dramatic wardrobe enhancement to the Spiders original magazine appearance: his simple black cape and head mask are over-printed with a white spiders web pattern and then matched with his usual plain black fedora. This striking addition gave the silver screen Spider an appearance more like that of a traditional superhero, like other pulp and comics heroes being adapted for the eras movie serials; it also made the serial Spider look less like the very popular Street and Smith pulp hero The Shadow, which also had been produced by Columbia and starred Victor Jory.

James W. Horne, who had co-directed the first Spider serial, was in complete charge of the sequel. By this time, Horne was filling his serials with tongue-in-cheek melodramatics, ludicrous fight scenes (in which the hero fights six or more men, and wins), as well as ridiculous-looking machines. For this reason, action fans often dismissed The Spider Returns as an inferior serial; but others consider it one of Hornes best, and a worthy sequel. While The Spider does take on half-a-dozen henchmen at a time, he doesnt always come off the clear winner. Horne keeps the action fairly straight until the last chapter, when he inserts some obvious humor (two henchmen, exhausted from their fist-fight, haphazardly swing at each other and then collapse).

The action-filled screenplay employs a typical serial formula of fist-fights, gun battles, explosions, and car chases, not forgetting secret weapons, death traps, and hairbreadth escapes as The Gargoyle tries to steal some top secret plans. The Spider serials are unique in that The Spider is also sought by the police with the same vigor that he is sought by criminals. The one real difference between this and the first serial is the police know Wentworth goes undercover at times in disguise as petty criminal Blinky McQuade; they work with him following the leads he uncovers as McQuade.
 Dave OBrien, who had performed The Spiders acrobatic stunts in The Spiders Web, is now a full-fledged second lead playing the role of Wentworths assistant. This appearance led to a starring role in Columbias later serial, Captain Midnight. Only three of the main participants in The Spiders Web (Warren Hull, Kenne Duncan, and Dave OBrien) are on hand for this sequel.

==Cast==
* Warren Hull as The Spider, his secret identity Richard Wentworth, and Blinky McQuade
* Mary Ainslee as Nita Van Sloan, Richard Wentworths fiancée Dave OBrien as Jackson, Wentworths assistant 
* Joseph W. Girard as Police Commissioner Kirk Kenneth Duncan as Ram Singh, Sikh warrior and Wentworths bodyguard.
* Corbet Harris as McLeod 
* Bryant Washburn as Westfall 
* Charles F. Miller as Mr. Van Sloan
* Anthony Warde as Trigger, one of the Gargoyles henchmen Harry Harvey as Stephan
* Forrest Taylor  (uncredited) Voice of The Gargoyle

==Production==
===Stunts===
*Chuck Hamilton
*George Magrill
*Ken Terrell
*Dale Van Sickel

==Chapter titles==
 
# The Stolen Plans
# The Fatal Time-Bomb
# The Secret Meeting
# The Smoke Dream
# The Gargoyles Trail
# The X-Ray Eye
# The Radio Boomerang
# The Mysterious Message
# The Cup of Doom
# The X-Ray Belt
# Lips Sealed by Murder
# A Money Bomb
# Almost a Confession
# Suspicious Telegrams
# The Payoff
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 230
 | chapter = Filmography
 }} 
 

==See also==
*List of film serials by year
*List of film serials by studio

==References==
 

==External links==
*  
*  
*   at Movie Serial Experience

 
{{succession box  Columbia Serial Serial  White Eagle (1941)
| years=The Spider Returns (1941) The Iron Claw (1941)}}
 

 

 
 
 
 
 
 
 
 
 
 
 
 