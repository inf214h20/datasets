Hoop-La
{{Infobox film
| name           = Hoop-La
| image          = Hoop-la.jpg
| image_size     = 
| caption        = 
| director       = Frank Lloyd
| producer       = 
| screenplay     = Bradley King Joseph Moncure March|J.M. March
| based on       =   Richard Cromwell Minna Gombell
| music          = Louis De Francesco (uncredited) Ernest Palmer
| editing        = 
| distributor    = Fox Film Corporation
| released       = November 30, 1933
| runtime        = 85 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
 Richard Cromwell, filmed in 1928 under the same title as the play. 
 Classic Film Festival in Hollywood during the spring. 

==Synopsis==
Bow plays a carnival performer who sets out to seduce the boss son in order to win a bet. 

==Cast==
*Clara Bow - Lou
*Preston Foster - Nifty Miller Richard Cromwell - Chris Miller
*Herbert Mundin - Hap Spissel
*James Gleason - Jerry
*Minna Gombell - Carrie
*Roger Imhof - Colonel Gowdy
*Florence Roberts - Ma Benson

unbilled
*Damores - Colonel Gowdy (French version)
*Erville Alderson - The Sheriff
*Bob Burns - Barker
*Dick Dickinson - Carnival Spectator
*Ethel Loreen Greer - Fat Lady
*Chuck Hamilton - Side-Show Troublemaker
*Otis Harlan - Town Councilman, Side Show Customer
*John Irwin - Roustabout
*William Le Maire - Tall Gum-chewing spectator Frank Mills - Barker
*Frank Moran - Side Show Craps Player
*George Offerman Jr. - Billy
*Harvey Parry - Roustabout
*Charles Sellon - The Colonel, Billys Father
*Harry Wilson - Roustabout
*Harry Woods - Side Show Troublemaker

==Remake==
The same story was remade in 1945 as Billy Roses Diamond Horseshoe starring Betty Grable.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 

 