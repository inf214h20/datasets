The Glad Eye (1927 film)
{{Infobox film
| name           = The Glad Eye 
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = Victor Saville   Maurice Elvey
| writer         = Jose G. Levy (play)   Nicolas Nancey (play)   Paul Armont (play)   Gareth Gundrey   Victor Saville   Maurice Elvey
| starring       = Estelle Brody Mabel Poulton Jeanne de Casalis   Aubrey Fitzgerald
| music          =
| cinematography = Percy Strong   Basil Emmott 
| editing        = 
| studio         = Gaumont British Picture Corporation
| distributor    = Gaumont British Distributors
| released       = July 1927
| runtime        = 7,700 feet  
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}}
  silent comedy The Glad Eye, a 1920 film based on the play Le Zebre by Paul Armont. It was made at Twickenham Studios.

==Cast==
* Estelle Brody - Kiki
* Mabel Poulton - Suzanne
* Jeanne de Casalis - Lucienne
* Hal Sherman - Chausette
* Aubrey Fitzgerald - The Footman
* A. Bromley Davenport - Galipau
* John Longden - Floquet John Stuart - Maurice
* Humberston Wright - Gaston

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.
* Wood, Linda. British Films 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 