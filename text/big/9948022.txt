Muni (film)
{{Infobox film
| name           = Muni
| image          =http://image1.indiaglitz.com/tamil/wallpaper/MOVIES/muni/muni3_800_260606.jpg
| caption        = Muni Theatrical Poster Raghava Lawrence Saran
| writer         = Ramesh Khanna  (Dialogues)
| screenplay     = Raghava Lawrence
| story          = Raghava Lawrence
| starring       = Raghava Lawrence Vedhicka Rajkiran Bianca Desai|
| cinematography = K. V. Guhan
| music          = Bharathwaj
| editing        = Suresh Urs
| studio         = Gemini Productions
| distributor    = Gemini Productions
| released       = 9 March 2007
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
Muni ( ) is a 2007   was released by Raghava Lawrence. It was the first film in the installment of Muni (film series).

==Plot==
Ganesh (Raghava Lawrence), a young man with a deep fear of ghosts who refuses to even go out after 6pm, moves into a new house with his mother, father and wife not knowing that it is the residence where Muni met his death and where his spirit now resides. Muni enter Ganeshs body and Ganesh starts behaving in a rude manner, his family couldnt understand why his behaviour is weird so they seek the help of a priest (Nasser). Priest asks the ghost about his whereabouts. Ghost says that he is Muniyaandi and starts saying his flashback. Muniyaandi was a kind-hearted poor man living in the slums with his daughter and other people. Munis friend is Marakka Dhandapani MLA who uses Muni to make him win in elections and promising that he would give lands to poor people. Dhandapani wins the election but he cheats Muni. Muni fights with Dhandapaani but he kills both him and his daughter. Dhandapaani lies to poor people saying that Muni and his Daughter had fled with the money he had given Muni for the welfare of the village residents. Ganesh accepts Muni and he enters his body. Ganesh enters Dhandapaanis household and terrorizes him and his assistants. Dhandapaani comes to know that the spirit of Muni resides in Ganeshs body so he sets a contract killer (Rahul Dev) to finish him. In the climax, at Ayyanaar temple Dhandapaani confesses to people that he only killed Muni. Muni talks to people and eats the feast prepared by his people. Muni then resides again in Ganesh and finishes off Dhandapaani. Ganesh comes back to normal.

==Cast== Raghava Lawrence as Ganesh
* Rajkiran as Muniyandi
* Vedhicka as Priya
* Vinu Chakravarthy
* Kovai Sarala as Ganeshs mother
* Kadhal Dandhapani
* Rahul Dev
* Nassar
* Bianca Desai
*Kavithan Rajamohan

==Critical Reception==
Muni received generally positive reviews from critics. IndiaGlitz.com called the movie a visual treat from Lawrence. Lawrence as Ganesh in the film walks away with all honours. His electrifying performance lends solidity to the film. Vedicka as Lawrences wife chips in with her best. She has shown immense promise.  

==Sequels==
 , the sequel of Muni, released on 15 July 2011 in Andhra Pradesh and 22 July 2011 in Tamil Nadu with a different cast except   was released in 16th April 2015 and also became a Super-Hit

==References==
 

==External links==

 
 

 
 
 
 
 
 
 
 
 


 
 