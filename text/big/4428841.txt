Death of a Scoundrel
{{Infobox film
| name           = Death of a Scoundrel
| image          = Death of a Scoundrel poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Charles Martin
| producer       = Charles Martin
| screenplay     = Charles Martin
| story          = 
| narrator       = 
| starring       = George Sanders Yvonne De Carlo Zsa Zsa Gabor
| music          = Max Steiner
| cinematography = James Wong Howe
| editing        = Conrad A. Nervig
| studio         = 
| distributor    = RKO Pictures
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}Death of a Scoundrel is a 1956 film starring George Sanders, Yvonne De Carlo and Zsa Zsa Gabor. It was written and directed by Charles Martin.

Death of a Scoundrel is a fictionalized adaptation of the life and mysterious death of Serge Rubenstein.

This film and The Falcons Brother are the only two films to feature real-life brothers George Sanders and Tom Conway. They play brothers in both films.

==Plot==
Clementi Sabourin is found dead. Police come to investigate, and when they question Bridget Kelly, who found the body, she tells them everything she knows about his past.

A Czech refugee, missing and believed dead, Sabourin one day turns up to find his love Zina is now married to his brother. Out of spite, he betrays his brother to the police.

Sabourin sets sail for America. At the port in New York he observes the shady Miss Kelly as she makes off with ship passenger Leonard Wilsons wallet. Sabourin makes a romantic play for Kelly, only to steal the wallet when her back is turned. Kellys estranged husband pursues and shoots Sabourin in the street, but Sabourin still gets away.

On a tip from the doctor who removes the bullet, Sabourin invests in a company that manufactures the drug penicillin. He fraudulently uses a $20,000 cashiers check from inside the stolen wallet to purchase the stock. Encountering the wealthy Mrs. Ryan, widow of a prominent businessman, Sabourin earns a considerable sum of money for her as well by tipping her to the stock, then covers his own bad check with a $20,000 loan from the grateful Mrs. Ryan.

Sabourin becomes a great business success, with Miss Kelly and his broker, OHara, coming to work for him. He and his lawyer Bauman unscrupulously make deals to attain wealth while Sabourin begins courting a number of women romantically, including Mrs. Ryans young secretary, Stephanie North, financing her ambition to be an actress, as well as the married, affluent Edith van Rensselaer. Also resurfacing is Zina, to whom Sabourin pledges his love and help, lying through his teeth.

As everyone in his life comes to understand just what a scoundrel Sabourin is, each has a reason to kill him, until someone ultimately does.

==Cast==
* George Sanders as Sabourin
* Yvonne DeCarlo as Miss Kelly
* Zsa Zsa Gabor as Mrs. Ryan
* Victor Jory as Leonard Wilson
* Nancy Gates as Stephanie North
* Coleen Gray as Edith van Renssalaer
* John Hoyt as OHara
* Lisa Ferraday as Zina Monte
* Tom Conway as Gerry Monte Sabourin
* Celia Lovsky as Mrs. Sabourin
* Werner Klemperer as Herbert Bauman
* Justice Watson as Henry

==Reaction==
"Mr. Sanders and company make Death of a Scoundrel a slick and sometimes fascinating fiction. It only casually tries to probe the hearts and minds of its principals."  

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 