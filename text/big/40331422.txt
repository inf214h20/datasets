Yennamo Yedho
 
{{Infobox film
| name           = Yennamo Yedho
| image          = Yennamo Yedho poster.jpg
| caption        = Film poster
| director       = Ravi Tyagarajan
| producer       = P. Ravikumar P. V. Prasad
| writer         = Nandini Reddy
| starring       = Gautham Karthik Rakul Preet Singh Prabhu Ganesan Nikesha Patel
| music          = D. Imman
| cinematography = Gopi Jagadeeswaran
| editing        = I J Alen
| studio         = Ravi Prasad Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Tamil romantic comedy film directed by Ravi Thyagarajan starring Gautham Karthik and Rakul Preet Singh. A remake of the Telugu film Ala Modalaindi,  the film was produced by Ravi Prasad Productions and has music by D. Imman. Gopi Jagadeeswaran was the cinematographer, while Vishwaroopam fame Lalgudi N Illayaraja was the art director.  The film released on 25 April 2014 to mixed reviews.

==Cast==
* Gautham Karthik as Gautham
* Rakul Preet Singh as Nithya
* Nikesha Patel as Kavya
* Prabhu Ganesan as Chakravarthi
* Azhagam Perumal as Narayanan, Nithyas father
* Anupama Kumar as Lakshmi, Gauthams Mother
* Surekha Vani as Nithyas Mother
* Sai Prashanth as Gauthams Brother-in-law
* Manobala as Guruji
* Madhan Bob as Madhan, Nithyas Uncle
* Shakeela as Kujli Plus TV Anchor
* "Lollu Sabha" Manohar
* Sadhana as Anu Aunty
* "Yogi" Babu
* Snigdha as Gauthams Sister
* Ravi Raj as Dr. Ravishankar
* Satheesh Natrajan

==Production==
Ravi Prasad Outdoor Unit, pioneers in the south for film shooting equipment bought the remake rights of Ala Modalaindi and ventured into film production with this film under the banner of Raviprasad Productions.  Ravi Thyagarajan, son of fight master Thyagarajan and a former assistant to the director Priyadarshan, was selected to direct the film and make his directorial debut. 

The shooting began on May 23, 2013.  The second schedule began on June 27, 2013 in Hyderabad, India|Hyderabad. 

==Soundtrack==
The music was composed by D. Imman and lyrics were written by Madhan Karky. 

*Mosale Mosale - Deepak, Pooja AV
*Muttalai Muttalai - D. Imman, Maria Roe Vincent
*Nee Enna Periya Appatucker - Anirudh Ravichander, Harshita Krishnan
*Pudhiya Ulagai - Vaikom Vijayalakshmi
*Shut Up Your Mouth - Shruti Haasan, Deepak

==Release==
The satellite rights of the film were sold to Jaya TV.

==Critical reception==
The film received negative reviews from critics. The Times of India gave the film 2 stars out of 5 and wrote, "It is a convoluted plot that just feels inane on paper but one that could pass off as a comedy of the absurd on screen. But, because of its uneven tone, the film comes across as something that is severely disjointed and unfocussed.  The Hindu wrote, "there’s no end to the tiresome sequence of romance and break-ups in Yennamo Yedho. It has everything going wrong for it. A convoluted plot, unimpresive performances and unfunny comedy".  Sify wrote "the plot is hackneyed, romance is half baked and lead pair is unimpressive", calling the film "a pale version of the original" and describing it as "heartbreakingly disappointing".  Deccan Chronicle wrote, "Despite being the remake of Ala Modalaindi, which was a hit in Telugu, Yennamo Yedho fails to impress".  Behindwoods.com gave 1.75 stars out of 5 and wrote, "Yennamo Yedho has very few gripping scenes and doesn’t make the connect with the viewer. Its pace is laborious and the romance that is portrayed is nothing new to the viewer. The so-called twists are also predictable and artificial. Inspite of the refreshing lead pair, the movie leaves you weary". 

The New Indian Express wrote, "The film may not be the best of romantic comedies. But it’s a fairly enjoyable watch".  Indiaglitz stated, "A little bit of action, a lot of comedy - mostly situational and full of romance, Yennamo Yedho is a fairly clean entertainer and a lighthearted movie experience to chill out on, with friends and family".  Oneindia.in called it "an enjoyable movie". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 