The Sellout (film)
 
{{Infobox film
| name           = The Sellout
| image          = The Sellout film poster.jpeg
| caption        = Theatrical release poster
| director       = Gerald Mayer
| producer       = Nicholas Nayfack Matthew Rapf Charles Palmer
| story          = Matthew Rapf
| starring       = Walter Pidgeon John Hodiak Audrey Totter Paula Raymond
| music          = David Buttolph	 	
| cinematography = Paul Vogel George White
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $596,000  . 
| gross          = $645,000 
}}

The Sellout is a 1952 American film noir directed by Gerald Mayer, starring Walter Pidgeon, John Hodiak, Audrey Totter and Paula Raymond. 

==Plot==
Big-city newspaper Editor Haven D. Allridge (Pidgeon) starts a crusade to smash corrupt small-town sheriff Burke (Gomez). After Allridge is suddenly intimidated into silence, state attorney Chick Johnson (Hodiak) continues the fight for right. He discovers that the sheriff is keeping Allridge quiet by threatening to reveal the criminal activities of Allridges son-in-law (Mitchell).

==Cast==
* Walter Pidgeon as Haven D. Allridge  
* John Hodiak as Chick Johnson 
* Audrey Totter as Cleo Bethel  
* Paula Raymond as Peggy Stauton  
* Thomas Gomez as Kellwin C. Burke   Cameron Mitchell as Randy Stauton  
* Karl Malden as Captain Buck Maxwell  
* Everett Sloane as Nelson S. Tarsson  
* Jonathan Cott as Ned Grayton  
* Frank Cady as Bennie Amboy  
* Hugh Sanders as Judge Neeler  
* Griff Barnett as Attorney General Morrisson  
* Burt Mustin as Elk M. Ludens  
* Whit Bissell as Wilfred Jackson  
* Roy Engel as Sam F. Slaper

==Reception==
According to MGM records the film made $434,000 in the US and Canada and $211,000 elsewhere, resulting in a loss of $227,000. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 