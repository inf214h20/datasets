How Do You Know
{{Infobox film
| name           = How Do You Know 
| image          = How Do You Know Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = James L. Brooks
| producer       = Julie Ansell James L. Brooks Paula Weinstein
| writer         = James L. Brooks
| starring       = Reese Witherspoon Paul Rudd Owen Wilson Jack Nicholson
| music          = Hans Zimmer
| cinematography = Janusz Kamiński
| editing        = Richard Marks
| distributor    = Columbia Pictures
| studio         = Gracie Films
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $120 million  
| gross          = $48.7 million   
}}
 romantic comedy drama film directed, written and produced by James L. Brooks. It stars Reese Witherspoon, Paul Rudd, Owen Wilson and Jack Nicholson.

The film was shot in Philadelphia and Washington, D.C. It was released on December 12, 2010. This marks the third film to feature Witherspoon and Rudd following 2009s Monsters vs. Aliens and 1998s Overnight Delivery.

The film was both a critical and financial failure. The film grossed more than $48 million, which is less than half of its enormous $120 million budget, and was therefore considered a financial flop. 

==Plot== Team USA roster. Unsure what to do next, Lisa begins dating Matty Reynolds (Owen Wilson), a pitcher for the Washington Nationals. She also receives an intriguing phone call from a young executive, George Madison (Paul Rudd), who was advised by a friend of Lisas to give her a call. George calls out of politeness because he wants to explain that his relationship with his girlfriend has just become more serious. But life takes an abrupt turn for the worse for George when he suddenly finds himself the target of a federal criminal investigation for corporate malfeasance at a company run by his father, Charles Madison (Jack Nicholson). George is fired from his job and abandoned by the company, with the exception of his father and his pregnant secretary, Annie (Kathryn Hahn).

Still reeling from this blow, George goes to his girlfriend for sympathy and is stunned when she immediately breaks up with him. On a whim, George calls again to invite Lisa to dinner and she accepts. It turns out to be a disaster; George is so overwhelmed with his troubles that Lisa eventually asks that they just eat in silence, and they part ways not expecting to see one another again. Soon, Lisa moves in with Matty, who has a penthouse in the same upscale building where Georges father lives. Matty is rich, well-meaning and fun, but is also immature and insensitive, and continues to have casual affairs with other women.

George is indicted and could face prison time. Annie is so loyal that she tries to give him inside information in advance, but he urges her not to lose her own job. Matty tries to do better in Lisas eyes, promising to consider seeing fewer women on the side. He inadvertently offends her, so Lisa moves out and spends a pleasant, tipsy evening at Georges modest new apartment. Georges father then drops one last bombshell on his son: It was he who committed the illegal act for which George is being charged. Due to a previous conviction, Charles would spend at least 25 years &mdash; basically, the rest of his life &mdash; in prison, whereas George would only do three years at most.

On the night Annies baby is born and her boyfriend proposes, Lisa begins to reconsider her previous reluctance to settle down. George is clearly smitten with her, but Matty pleads for another chance and she accepts. George makes a proposition to his father: He will take one more shot at persuading Lisa to be with him; If she will, Charles must go to jail, and if she wont, George will take the rap for his dad. At a birthday party that Matty throws for her, George confesses his feelings for Lisa and  goes outside to give her time to think it over, Charles looking on from above. Finally, Lisa says goodbye to Matty and joins George outside. At the end of the movie, they are seen boarding a bus together.

==Cast==
 
* Reese Witherspoon as Lisa Jorgenson
* Paul Rudd as George Madison
* Owen Wilson as Matty Reynolds
* Jack Nicholson as Charles Madison
* Dean Norris as Tom
* Kathryn Hahn as Annie
* Yuki Matsuzaki as Tori Andrew Wilson as Mattys Teammate
* Shelley Conn as Terry
* Tony Shalhoub as Psychiatrist
* Domenick Lombardozzi as Bullpen Pitcher
* Ron McLarty as Georges Lawyer
* Lenny Venito as Al
* Mark Linn-Baker as Ron
* Molly Price as Coach Sally
* Joseph Clampitt as Physics student
 

==Production==
Brooks began work on the film in 2005, wishing to create a film about a young female athlete. While interviewing numerous women for hundreds of hours in his research for the film, he also became interested in "the dilemmas of contemporary business executives, who are sometimes held accountable by the law for corporate behavior of which they may not even be aware." He created Paul Rudds and Jack Nicholsons characters for this concept.  Filming finished in November 2009,  although Brooks later reshot the films opening and ending. 
 tax rebates salaries for the director Brooks (about $10 million) and the four major stars Witherspoon ($15 million), Nicholson ($12 million), Wilson ($10 million) and Rudd ($3 million) totaled about $50 million. Brooks "slow and meticulous" production and post-production also explained the size of the budget. {{cite news
| date=2010-12-10
| first=Kim
| last=Masters
| title=EXCLUSIVE: How Do You Know Price Tag: $120 Million, $50 Million Just for Talent
| url=http://www.hollywoodreporter.com/news/price-tag-120-million-50-58410
| work=The Hollywood Reporter
| accessdate=2010-12-19
}} 

==Release==

===Box office===
The film opened at $7.6 million in the  ,  . By December 22, it was #11 in the box office.

How Do You Know grossed a total of $48,668,907 worldwide, failing to recoup its $120 million budget.  

In 2014, the LA Times listed the film as one of the most expensive box office flops of all time. 

===Critical response  ===
The film received generally negative reviews. Rotten Tomatoes gives the film a score of 32% based on 144 reviews, making it "Rotten". {{cite web
| url=http://www.rottentomatoes.com/m/everything_youve_got
| title=How Do You Know Movie Reviews, Pictures
| work=Rotten Tomatoes
| publisher=Flixster
| accessdate=2010-12-31
}}  The sites consensus is: "How Do You Know boasts a quartet of likeable leads – and they deserve better than this glib, overlong misfire from writer/director James L. Brooks".

==References==
 

==External links==
*  
*  
*  
*   at Metacritic
*  
* http://gifts4pts.net/?ref=Xe1EbH5ab
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 