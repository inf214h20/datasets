Bella (film)
{{Infobox film
| name = Bella
| image = Bella cover.jpg
| caption =  Theatrical release poster Jason Jones
| director = Alejandro Gomez Monteverde
| writer = Alejandro Gomez Monteverde  Patrick Million Leo Severino
| starring = Eduardo Verástegui Tammy Blanchard Manny Perez Ali Landry Ewa Da Cruz
| cinematography = Andrew Cadelago
| editing = Joseph Gutowski Fernando Villena
| music          = Stephan Altman
| distributor = Roadside Attractions
| released =  
| runtime = 91 minutes
| country = United States
| language = English Spanish
| budget = $3.3 million 
| gross = $12,083,296   
}}
Bella is a 2006 film directed by Alejandro Gomez Monteverde starring  Eduardo Verastegui and Tammy Blanchard. Set in New York City, the film is about the events of one day and the impact on the characters lives.

==Plot==
Bella tells the story of Nina, a New York City waitress, and her co-worker José, a cook.

The kitchen of a Mexican restaurant in Manhattan is getting ready for the noon rush. Nina arrives late for the second day in a row. Manny, the restaurant owner and Joses brother, fires her.

As Nina leaves, José follows her outside. She tells him she is pregnant but says she is not ready for a baby and is seriously considering abortion. He takes her to his parents house and introduces her to his family. He then takes her into the garage and shows her his old car and also tells her that a few years ago he had been driving his car when he accidentally hit and killed a little girl. He was sentenced to four years in prison. After being released, he tried unsuccessfully multiple times to get in touch with the girls mother.

José and Nina have dinner at his parents house during which Nina finds out that Manny was adopted. Josés parents tell Nina she is always welcome to stay at their house. José takes Nina to the beach, which is near the house.  Nina tells José of how her fathers death when she was twelve caused her and her mother severe emotional pain.  Since Nina had no siblings and spent her childhood taking care of her emotionally crippled mother, she tells José how fortunate he is to have a loving family.  The next day, before they each go their own way, Nina says she needs a friend to be there for her the next week.

José walks back to the restaurant and reconciles with Manny.

Several years later, José is playing on a beach with a young girl, Bella, Ninas daughter and Josés adopted daughter. Nina pulls up in a taxi and meets Bella for the first time. The two exchange gifts. The movie ends as Nina, Bella, and José walk down the beach together. 

== Cast ==
* Eduardo Verástegui as José
* Tammy Blanchard as Nina
* Manny Perez as Manny
* Ali Landry as Celia
* Ramón Rodríguez (actor)|Ramón Rodríguez as Eduardo
* Angélica Aragón as Josés mother
* Ewa Da Cruz as Veronica
* Alexa Gerasimovich as Luchi
* Sophie Nyweide as Bella
* Herbie Lovelle plays as a blind homeless person

==Production== Manuel Perez, Jason Jones.  Executive producers were J. Eustace Wolfington, Sean Wolfington, Ana Wolfington and Stephen McEveety. It was financed by producers Sean Wolfington and Eustace Wolfington.

Stephen McEveety, producer of Braveheart and The Passion of the Christ, consulted on the script; after the film was finished, he signed on as an executive producer to help market it. Bella is McEveetys first release under his new company Mpower Pictures.

Bella was produced by Metanoia Films. Lionsgate and Roadside Attractions acquired United States distribution rights to the film and released the film on October 26, 2007, according to The Hollywood Reporter.   

== Reception ==
The film received mixed critical reviews, scoring 44% at review site Rotten Tomatoes and 47 at Metacritic   but fared better with audiences, winning the Golden Tomato award from Rotten Tomatoes with a user rating of 96.5, the highest of any film released in 2007. 

Bella resonated with adoption and pro-life organizations, who gave the movie high marks for its pro-adoption themes. 

==Awards and honors==
Bella took the "Peoples Choice Award" at the 2006 Toronto International Film Festival.   

Bella won the Heartland Film Festivals Grand Prize Award Winner for Best Dramatic Feature and the Crystal Heart Awards for Monteverde as writer/director/producer.   

Bellas filmmakers received the Smithsonian Institutions "Legacy Award" for the films positive contribution to Latino art and culture.       "This movie depicts the culture but also transcends it," said Pilar OLeary, executive director of the Smithsonian Institutions Latino Center. "It has universal appeal." William Triplett (May 7, 2007).   . Variety.  Retrieved on 2007-10-11. 

Bella received the Tony Bennett Media Excellence Award.  Bennett said Bella is "a perfect film, an artistic masterpiece that will live in peoples hearts forever."   

The United States Conference of Catholic Bishops Office for Film and Broadcasting rated Bella as the second-best film of 2007 (with Juno (film)|Juno)  noting that Bella presents an "affirmative pro-life message," along with "themes of self-forgiveness, reconciliation and redemption that should resonate deeply." 

The director of the Department of Citizenship gave the director of Bella, Alejandro Monteverde, the "American by Choice" Award at a White House reception for Bellas positive contribution to Latino art and culture in the United States.     Monteverde was also invited to join the First Lady Laura Bush in her private box to watch the State of the Union address.   

The Mexican Embassy honored the film and gave Bella a screening at their annual Cinco De Mayo celebration. 

Bella broke the record for a Latino-themed film in total box office earnings and box office average per screen for films released in 2007. It was the top-rated movie on the New York Times Readers Poll, Yahoo and Fandango (ticket service)|Fandango.  The Wall Street Journal said Bella was "the falls biggest surprise" and stated that "after only four weeks in release Bella has total sales of $5.2 million."    Bella ended its U.S. theatrical release with more than $10 million in domestic box office, finishing the year in the top 10-grossing independent films of 2007. 

==References==
This article incorporates text from  , licensed under GNU Free Documentation license.
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 