Spun
 
{{Infobox film
| name           = Spun 
| image          = Spun film poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Jonas Åkerlund 
| producer       = Chris Hanley Fernando Sulichin Timothy Wyane Peternel Danny Vinik
| screenplay     = William De Los Santos Creighton Vero Deborah Harry Eric Roberts Chloe Hunter Nicholas Gonzalez Brittany Murphy Mickey Rourke
| music          = Billy Corgan
| cinematography = Eric Broms
| editing        = Jonas Åkerlund
| studio         = Silver Nitrate Films Brink Films
| distributor    = Newmarket Capital Group
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $411,119
}}
 crime Black dark comedy-drama Deborah Harry, Eric Roberts, Chloe Hunter, Nicholas Gonzalez, Brittany Murphy and Mickey Rourke. 
 debut as a feature film director, having already become known for his work in music videos. The film was shot in 22 days, and centers on various people involved in a methamphetamine drug ring. The film blends elements of dark comedy and drama in its storytelling. Its title is a reference to the slang term for the way users feel after going multiple days without sleep while on a methamphetamine binge. The characters take a combined total of 23 "hits" during the course of the movie.

==Plot==
Ross (Jason Schwartzman) is a customer of Spider Mike (John Leguizamo), a methamphetamine dealer. Spider Mike and his girlfriend Cookie (Mena Suvari) are constantly arguing, and Ross strikes up a friendship with Nikki (Brittany Murphy), a fellow addict. Nikki takes Ross to meet her boyfriend, "The Cook" (Mickey Rourke), who supplies Spider Mike with drugs from a meth lab he has set up in a motel room. The Cook gives a small amount of meth to Ross in exchange for bringing Nikki home, and says that he will get in touch with Ross if he needs a driver.

Back at his apartment, Ross gets messages from his mother and his former girlfriend, Amy, wishing him a happy birthday; Amy is also demanding that he pay back $450 that he owes her. Ross, assuming she still loves him, calls her and leaves a message. He then goes to the local strip club while high, leading to an intense pornographic hallucination. He takes one of the dancers, April (Chloe Hunter), home and has sex with her in a variety of positions, the last of which leaves her tied to the bed naked. As they finish, the Cook calls with an emergency regarding Nikkis dog. April tells him to untie her but Ross, still high, duct-tapes Aprils eyes and mouth shut to keep her quiet and leaves without releasing her, playing music to cover her gagged screams. While Ross and Nikki take the dog to the veterinarian, policemen and a TV crew raid the trailer where Frisbee (Patrick Fugit), another one of Spider Mikes customers, lives, falsely believing that a meth lab is located there. They take Frisbee and his overweight mother into custody, where they threaten him into cooperation in a drug bust.  
 Las Vegas. Deborah Harry), who beats up the same liquor store customer when he disrespects the clerks again.

While Ross and Nikki are out, Frisbee is coerced by the cops to wear a wire and buy some meth from Spider Mike so they can arrest him. When he enters, Cookie attempts to make love to him, her revenge on Spider Mike for his use of a phone sex line. She finds the wire, and the cops rush in to make the drug bust. Spider Mike, furious at Frisbees betrayal, shoots him in the testicles, and Spider Mike and Cookie are arrested. Meanwhile, the Cooks meth lab catches fire and destroys the motel room. He takes off to the adult film store, where he is arrested after the owner (Rob Halford) calls the police. Once the Cook makes bail, he calls Ross asking for a ride to another dealers house in the city after he drops Nikki off at the station. Ross learns of everyone elses arrests, and agrees to drive him there, as well as visit Amy, who also lives in the city.  

The dealer (Eric Roberts) provides the Cook with cash, some meth, and the equipment to start a new lab. Ross calls Amy again, and leaves a message asking to see her and that he has her money with him. The Cook promises six months worth of meth to Ross in exchange for being his chauffeur; he agrees on the condition that he can see Amy first. Amy, who has gotten her life together and found work in the city, leaves him in the park after seeing that he is still using drugs. He gives her $100 of the money he owes her, and leaves with the Cook. Finally, after several days of nonstop activity fueled by drug use, the main characters all go to sleep except for the Cook. As Ross naps in his car, the Cook starts up a new lab in an old trailer, but blows it and himself up in the process.

==Cast==
* Jason Schwartzman as Ross
* John Leguizamo as Spider Mike
* Mena Suvari as Cookie
* Patrick Fugit as Frisbee
* Peter Stormare as Mullet Cop
* Alexis Arquette as Moustache Cop Deborah Harry as The "Neighbor"
* Eric Roberts as The Man
* Chloe Hunter as April Love
* Nicholas Gonzalez as Angel
* Brittany Murphy as Nikki
* Mickey Rourke as The Cook
* Charlotte Ayanna as Amy

;Cameos
* Larry Drake as Dr. K
* Billy Corgan as the doctor examining Frisbees injury
* China Chow as the prostitute ordered by the Cook
* Rob Halford as the clerk in the sex shop Tony Kaye as the announcer in the strip-bar
* Ron Jeremy as the bartender
* Josh Peck as Fat Boy

==Reception== drug movies. Time Out London was particularly harsh, accusing the film of "smug Amoralism|amoralism", and claiming that Åkerlund simply re-uses other peoples ideas and techniques. 
 pornographic tape fast cutscenes.

==Soundtrack== UFO (performed The Djali Bathory (for Per Gessle Mother North" appears in the movie.

==References==
 

==External links==
*  , which automatically launches the films IMDb page
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 