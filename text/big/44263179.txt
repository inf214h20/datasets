The Bridge in the Jungle
{{Infobox film
| name           = The Bridge in the Jungle
| image          = 
| alt            = 
| caption        =
| director       = Pancho Kohner 
| producer       = Pancho Kohner 
| writer         = Pancho Kohner 
| based on       =  
| starring       = John Huston Katy Jurado Elizabeth Guadalupe Chauvet José Ángel Espinosa Ferrusquilla Enrique Lucero Jorge Martínez de Hoyos
| music          = LeRoy Holmes
| cinematography = Xavier Cruz 
| editing        = 
| studio         = Capricorn Productions
| distributor    = United Artists
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Bridge in the Jungle is a 1971 American adventure film written and directed by Pancho Kohner. The film stars John Huston, Katy Jurado, Elizabeth Guadalupe Chauvet, José Ángel Espinosa Ferrusquilla, Enrique Lucero and Jorge Martínez de Hoyos. The film was released in January 1971, by United Artists.  

==Plot==
 

== Cast ==	 
*John Huston as Sleigh
*Katy Jurado as Angela
*Elizabeth Guadalupe Chauvet as Carmelita 
*José Ángel Espinosa Ferrusquilla as Garcia 
*Enrique Lucero as Perez
*Jorge Martínez de Hoyos as Augustin 
*Xavier Marc 		
*Chano Urueta as Funeral Singer
*Aurora Clavel as Aurelia
*Teddy Stauffer as Warner
*Eduardo López Rojas as Ignacio 
*José Chávez as Hide Trader 
*Sergio Calderón as Pedro 
*Enrique de Peña as Dancer 
*Ramiro Ramírez as Dancer
*Carlos Berriochea as First Boy
*Juan Antonio Edwards as Second Boy
*Elizabeth Dupeyrón as Joaquina 
*Gilberto Ramos Atayde as Carlos 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 