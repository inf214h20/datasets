The Princess and the Warrior
{{Infobox film
| name           = The Princess and the Warrior
| image          = Princess and the warrior.jpg
| image_size     =
| caption        = German-language poster
| director       = Tom Tykwer
| producer       = Stefan Arndt Katja De Bock Gebhard Henke Maria Köpf
| writer         = Tom Tykwer
| narrator       =
| starring       = Franka Potente Benno Fürmann
| music          = Reinhold Heil Johnny Klimek Tom Tykwer
| cinematography = Frank Griebe
| editing        = Mathilde Bonnefoy
| distributor    = Sony Pictures Classics
| released       = Venice Film Festival: 2 September 2000
| runtime        = 135 minutes
| country        = Germany German
| budget         =
| gross          = $871,058 (USA)
}} 2000 Cinema German drama drama film written and directed by Tom Tykwer with Franka Potente, star of his previous movie Run Lola Run (Lola rennt), in a leading role. It follows the life of Sissi, a psychiatric hospital nurse and Bodo (Benno Fürmann), an anguished former soldier who lapses into criminality, but allows other characters (such as Bodos brother Walter and select patients from the hospital) equal time for development. We see how Sissis routine life is skewed by a near-death experience and her subsequent relationship with Bodo.

==Plot==

The film begins with a letter to Sissi, a nurse in a psychiatric hospital, asking her for help in wrapping up the affairs of a friends dead mother. Sissi cares for her patients to the effect that they appear to be her extended family (in fact, she states that her own father is a patient in the hospital). As such, she has little experience of life outside the hospital. Meanwhile, Bodo, living with his brother, applies for a funerary job but is quickly dismissed because of his inability to control his emotions. He later robs a grocery store and during the ensuing chase indirectly causes a truck to hit Sissi. Taking cover from the police underneath the truck, Bodo finds Sissi, who cannot speak or breathe. In order to save her life, he performs an emergency tracheotomy. Once Sissi is in medical care, they are separated without Sissi ever learning his name.

The circumstances of her accident prevent Sissi from re-adjusting to her mundane life at the hospital, as she obsesses about tracking down her saviour. One of her patient friends, who had accompanied her on the day of her accident, helps her do so. She manages to track down Bodo, who is not interested in maintaining contact with her or any other woman. We see several times how Bodo, in a semi-conscious state, embraces a hot stove, having to be restrained by his brother Walter. It later becomes clear that Bodo is hallucinating (or perhaps dreaming) about his deceased wife, having never fully recovered from her death.  Walter tells Sissi that Bodos wife was killed in an explosion at a filling station, while Bodo was in the washroom. After Walter finishes his explanation, Bodo arrives and throws Sissi out.
 breakdown upon learning of his brothers death on TV. This incident brings Bodo to the attention of the head doctor at the institution, and he is treated and kept as a patient. It is during this period that Bodo explains to Sissi the true nature of his wifes death; a flashback shows Bodo and his wife engaged in a serious argument at the filling station. After heading to the washroom, Bodo witnesses his wife engulfed in a huge explosion caused by her deliberate dropping   of a cigarette into a pool of gasoline.

Sissi makes the decision to leave and asks Bodo to come with her, telling him of her dream in which they were "brother and sister, father and mother, husband and wife". Meanwhile, Steini, one of the patients, who recognized Bodos identity and is jealous of him spending time with Sissi, calls the police and in a delusion tries to kill Bodo by electrocution (by throwing a toaster into Bodos bathtub). The delusion, really a Hallucinogen Persisting Perception Disorder|flashback, reveals that Steini had killed Sissis mother the same way. Bodo however catches the toaster out of the air before it can fall into the tub and chases Steini into the attic. Meanwhile, the police arrive, and Sissi realizes that her mother was in fact murdered and did not commit suicide. She follows Steini to the roof, who offers to jump to atone for his actions. She declines, saying "Youre not going to jump anyway", grabs Bodos hand, and together they jump from the roof of the building, into a small pond.

The final scene (taking place at the scene of Bodos wifes accident) strays into a bit of surrealism: Bodos past personality, unkempt and finally emerging from the gas station restroom, takes a seat behind the wheel, while the real Bodo gets in the back of the car. As they drive off, Sissi touches (old) Bodos face to wipe away his tears, but he stops her and does so himself, pushing her away yet again. This visibly stirs (real) Bodo, who leans forward and covers his former selfs eyes, forcing him to brake. "Real" Bodo tells "old" Bodo to get out and leaves him standing in the middle of the road thus symbolically taking over "old" Bodos life; perhaps a visual metaphor for beginning anew.

In the international version of the movie, a small scene commences with the abandoned "old" Bodo as a more resolved ending to this surrealistic character and moment. Turning, he notices a sign indicating a bus stop in the field off the road; he waits by it, and an empty bus, driven by Walter, soon arrives to pick up this "dead" version of Bodo. The brothers do not speak, and they drive off a short distance before disappearing.

The film ends with Bodos redemption through his acceptance of Sissi, as he is shown at last content and dry-eyed as the couple arrive at the friends seaside house on the edge of a cliff (the friend from the beginning of the movie).

==Cast==
* Franka Potente as Simone, "Sissi"
* Benno Fürmann as Bodo
* Joachim Król as Walter
* Lars Rudolph as Steini
* Melchior Beslon as Otto
* Ludger Pistor as Werner
* Natja Brunckhorst as Meike

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 