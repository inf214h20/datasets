Chicken Little (2005 film)
 
{{Infobox film
| name           = Chicken Little
| image          = Chickenlittlemcgiposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Mark Dindal
| producer       = Randy Fullmer
| screenplay     = {{Plainlist|
* Steve Bencich
* Ron J. Friedman
* Ron Anderson }}
| story          = {{Plainlist|
* Mark Dindal
* Mark Kennedy }}
| starring       = {{Plainlist|
* Zach Braff
* Garry Marshall
* Joan Cusack
* Steve Zahn
* Dan Molina
* Amy Sedaris Mark Walton }}
| music          = John Debney
| editing        = Dan Molina
| studio         = {{Plainlist|
* Walt Disney Pictures Walt Disney Feature Animation }} Buena Vista Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $150 million   
| gross          = $314.4 million 
}}
 3D Computer-generated Walt Disney fable of Walt Disney Animated Classics series, it was directed by Mark Dindal with screenplay by Steve Bencich, Ron J. Friedman, and Ron Anderson and story by Mark Kennedy and Dindal.
 1943 cartoon made during World War II. The film is also the last Disney animated film made before John Lasseter was named chief creative officer of Disney Animation.

Though it received mixed reviews upon release, the film was a box office success, grossing $314 million worldwide.

==Plot==
In the small town of Oakey Oaks, Chicken Little rings the school bell and warns everyone to run for their lives. This sends the whole town into a frenzied panic. Eventually, the Head of the Fire Department calms down enough to ask him whats going on, and he explains that a piece of the sky shaped like a stop sign had fallen on his head when he was sitting under the big oak tree in the town square; however, he is unable to find the piece. His father, Buck Cluck, assumes that this "piece of sky" was just an acorn that had fallen off the tree and had hit him on the head, making Chicken Little the laughing stock of the town.

A year later, Chicken Little has become infamous in the town for being crazy. His only friends are outcasts like himself: Abby Mallard (who has a crush on him), Runt of the Litter (who ironically is extremely large), and Fish Out of Water (who wears a helmet full of tap water).

Trying to help, Abby encourages Chicken Little to talk to his father, but he really only wants to make his dad proud of him. As a result, he joins his schools baseball team in an attempt to recover his reputation and his fathers pride, but is made last until the ninth inning of the last game. Chicken Little is reluctantly called to bat by the coach (even though the coach is certain that he will lose the game for them). Little is able to hit the ball and make it past first, second, and third bases, but is met at home plate by the outfielders. He tries sliding onto home plate but is touched by the ball. While its presumed he lost the game, the umpire brushes away the dust to reveal Chicken Littles foot barely touching home plate, thus declaring Little safe and the game won; Little is hailed as a hero for winning the pennant.

Later that night back at home, he is hit on the head by the same "piece of the sky" — only to find out that it is not a piece of the sky, but a device which blends into the background (which would thereby explain why Chicken Little was unable to find it last time). He calls his friends over to help figure out what it is.

When Fish pushes a button on the back of the hexagon, it flies into the sky, taking Fish with it. It turns out to be part of the camouflage of an invisible UFO. Chicken Little manages to ring the bell to warn everyone, but the aliens see the crowds coming and manage to escape, leaving an orange alien child behind. No one believes the story of the alien invasion, and Chicken Little is ridiculed yet again... until the next day. He and his friends discover the orange alien, and a few minutes later a whole fleet of alien ships descends on the town and start what appears to be an invasion. The invasion is actually a misunderstanding, as the two aliens are looking for their lost child and attack only out of concern. As the aliens rampage throughout Oakey Oaks, vaporizing everything in their path, Little realizes he must return the alien to his parents to save the planet. First, though, he must confront his father and regain his trust. Chicken Little then finally tells his father what Abby had wanted him to do in the first place. As Chicken Little is talking about what his father was doing and that was he emotionally let Chicken Little down by not being there for him and not listening to him enough. As he and his father reconcile, Chicken Little goes to Abby and tells her that he always found her attractive as he and Abby share a kiss.

In the invasion, Buck, now regaining his pride and trust in his son, defends him from the aliens until they get vaporized. It is then discovered that the aliens werent vaporizing people, but the ray guns had teleported them aboard the UFO. Afterwards, the aliens return everything to normal (except Foxy Loxy, whose brain got scrambled, turning her into a Southern belle, and as a result, Runt falls for her), and everyone is grateful for Chicken Littles efforts to save the town.

==Cast==
* Zach Braff as Chicken Little, a young (and extremely small) rooster who suffers under a reputation for being crazy since he caused a panic saying the sky was falling.
* Joan Cusack as Abigail "Abby" Mallard (also known as the Ugly Duckling), a female duck (implied swan) with buckteeth. She is accustomed to being teased for her appearance, and takes a generally optimistic approach to life. She is Chicken Littles best friend and, near the end, presumed his girlfriend.
* Dan Molina as Fish Out of Water, a goldfish who wears a scuba helmet filled with water and lives on the surface. He is unable to speak properly, instead making gurgling sounds and acting out what he feels. He isnt very shy around others and he will perform brave stunts without fear.
* Steve Zahn as Runt of the Litter, a large pig with a huge heart who is much larger than the other children, but is far smaller than the other massive members of his family. Runt is easily frightened and prone to panic. 1943 short film, Foxy Loxy is a male fox. Mark Walton as Goosey Loosey, a goose, and Foxy Loxys best friend.
* Garry Marshall as Buck "Ace" Cluck, Chicken Littles widowed father, a former high school baseball star. turkey who is the mayor of Oakey Oaks. He is sensible, but not very bright.
* Sean Elmore, Matthew Michael Joston, and Evan Dunn as Kirby, an energetic and hyper alien child.
* Fred Willard as Melvin, Kirbys father and Tinas husband.
* Catherine OHara as Tina, Kirbys mother and Melvins wife.
* Mark Dindal as Morkubine Porcupine, one of the cool kids. Dindal also provides the voice of Coach in the film.
* Patrick Stewart as Mr. Woolensworth, the class sheep language teacher.
* Wallace Shawn as Principal Fetchit, the schools main principal.
* Patrick Warburton as Alien Cop
* Adam West as Ace - Hollywood Chicken Little
* Harry Shearer as Dog Announcer, the baseball announcer at Chicken Littles school.

==Production==
 
When the project started in 2001, it was originally meant as a movie about "a young girl who went to summer camp to build confidence so she wouldnt overreact". Other changes included Abby being a boy. When David Stainton became Disneys new president of Walt Disney Feature Animation in early 2003, he decided the story needed a different approach and told the director the script had to be revised, and during the next three months it was rewritten into a tale of a boy trying to save his town from space aliens. 

New software and hardware tools were introduced for the production of the film:   
* "Chicken Wire", a geometric wire frame model of the characters that the animators can stretch and squeeze as they please.
* "Shelf Control", which makes it possible to see the whole model on the screen while having a direct access to any chosen area of the character.
* New electronic tablet screens that allow the artists to draw digital sketches of the characters to rough out their movements, which is then transferred to the 3D characters.
 Pixar Animation Studios was set to expire with the release of Cars (film)|Cars in 2006. The end result of the contentious negotiations between Disney and Pixar was viewed to depend heavily on how Chicken Little performed at the box office. If successful, the film would have given Disney leverage in its negotiations for a new contract to distribute Pixars films. A failure would have allowed Pixar to argue that Disney could not produce CGI films without aid from Pixar. Discussions to renew the deal in 2005 were held off until both sides could access Chicken Little s performance at the box office.

It is not known how the two sides regarded Chicken Little s modest success. While it underperformed compared to Pixars product, it was more successful than Disneys recent output and was much more profitable for the company, since they did not need to share the revenue. Regardless, both sides decided that they were better off with each other than separate. However, instead of negotiating a new contract, on January 24, 2006, Disney announced their intent to purchase Pixar in an all-stock transaction worth $7.4 billion. The purchase was completed on May 5, 2006. 

==Reception==

===Box office===
In its opening weekend, Chicken Little debuted at #1, the first Disney animated film to do so since Dinosaur (film)|Dinosaur (2000), taking $40 million and tying with The Lion King (1994) as the largest opener for a Disney animated film.  It also managed to claim #1 again in its second week of release, earning $31.7 million, beating Sonys sci-fi family film, Zathura (film)|Zathura.  The film grossed $135,386,665 in North America, and $179,046,172 in other countries, for a worldwide total of $314,432,837. 
 Home on the Range (2004). However, these films received better critical reception.  

===Critical reception===
Chicken Little received generally unfavorable reviews from critics. Critical response aggregator Rotten Tomatoes reported that 36% of critics gave positive reviews based on 159 reviews with an average score of 5.5/10. The critical consensus states "In its first non-Pixar CGI venture, Disney expends more effort in the technical presentation than in crafting an original storyline."  Another review aggretator, Metacritic gave the film an average score of 48 based on 32 critics, indicating "mixed or average reviews". 
 Ebert & Roeper gave the film a "Thumbs Down" rating saying "I dont care whether the film is 2-D, 3-D, CGI, or hand-drawn, it all goes back to the story."  A.O. Scott of the New York Times stated the film is "a hectic, uninspired pastiche of catchphrases and clichés, with very little wit, inspiration or originality to bring its frantically moving images to genuine life."  However, Ty Burr of the Boston Globe gave the film a positive review saying the film was "shiny and peppy, with some solid laughs and dandy vocal performances".  Angel Cohn of TV Guide gave the film 3 stars alluding the film that would "delight younger children with its bright colors and constant chaos, while adults are likely to be charmed by the witty banter, subtle one-liners and a sweet father-son relationship." 

===Home media===
Chicken Little was first released on DVD on March 21, 2006, only in a single-disc edition.  The first Blu-ray Disc|Blu-ray release of Chicken Little was on March 20, 2007, and contained new features that were not included in the 2006 DVD. A 3D Blu-ray version was released on November 8, 2011. 

==Soundtrack==
{{Infobox album  
| Name        = Chicken Little
| Type        = soundtrack
| Artist      = Various artists
| Cover       = Chickenlittlesoundtrack.jpg
| Border      = yes
| Released    = November 1, 2005
| Recorded    =
| Genre       = Rock, Pop, R&B, film soundtrack
| Length      = 39:05 Walt Disney
| Producer    = John Debney
| Chronology  = Walt Disney Animation Studios Home on the Range (2004)
| This album  = Chicken Little (2005) Meet the Robinsons (2007)
}} covers of classic popular songs, such as Elton John and Kiki Dees "Dont Go Breaking My Heart," Carole Kings "Its Too Late (Carole King song)|Its Too Late," and the Spice Girls signature hit "Wannabe (song)|Wannabe." The soundtrack was released on November 1, 2005.

===Track listing===
{{tracklist
| extra_column    = Artist
| total_length    = 39:05
| title1          = Stir It Up
| length1         = 3:42
| extra1          = Joss Stone and Patti LaBelle
| title2          = One Little Slip
| length2         = 2:53
| extra2          = Barenaked Ladies
| title3          = Shake a Tail Feather
| length3         = 3:05 The Cheetah Girls
| title4          = All I Know
| length4         = 3:25
| extra4          = Five for Fighting
| title5          = Aint No Mountain High Enough
| length5         = 3:28
| extra5          = Diana Ross
| title6          = Its the End of the World as We Know It (And I Feel Fine)
| length6         = 4:04
| extra6          = R.E.M.
| title7          = We Are the Champions
| length7         = 0:38
| extra7          = Zach Braff Wannabe
| length8         = 0:50
| extra8          = Joan Cusack
| title9          = Dont Go Breaking My Heart
| length9         = 1:53
| extra9          = The Chicken Little Cast
| title10         = The Sky Is Falling
| note10          = score
| length10        = 2:49
| extra10         = John Debney
| title11         = The Big Game
| note11          = score
| length11        = 4:04
| extra11         = John Debney
| title12         = Dad Apologizes
| note12          = score
| length12        = 3:14
| extra12         = John Debney
| title13         = Chase to Cornfield
| note13          = score
| length13        = 2:00
| extra13         = John Debney
| title14         = Dodgeball
| note14          = score
| length14        = 1:15
| extra14         = John Debney
| title15         = Driving with Dad
| note15          = score
| length15        = 1:45
| extra15         = John Debney
}}

==Video games==
  Xbox on Buena Vista Games. Two days later it was released for PlayStation 2, Nintendo GameCube and Game Boy Advance (October 20, 2005), and later Microsoft Windows (November 2, 2005). Chicken Little for Game Boy Advance was developed by Behaviour Interactive|A2M, while BVGs recently acquired development studio, Avalanche Software, developed the game for the consoles. 

The second video game, Disneys Chicken Little: Ace in Action, is a multi-platform video game, for the Wii, Nintendo DS, Xbox (console)|Xbox, Nintendo GameCube, and PlayStation 2 inspired by the "superhero movie within the movie" finale of the film. It features Ace, the superhero alter ego of Chicken Little, and the Hollywood versions of his misfit band of friends: Runt, Abby and Fish-Out-of-Water. The crew of the intergalactic Battle Barn faces off against Foxy Loxy and her evil Amazonian sidekick, Goosey Loosey, who have an evil plan to take over Earth. Battle evil alien robots through multiple levels across the solar system and combat your foes in one of three distinct game play modes: Ace on foot as a soldier, Runt as the driver of an armored tank, or Abby as the pilot of a spaceship. The original Chicken Little and his friends Abby, Runt, and Fish from the film are featured in cut scenes throughout the game.

Chicken Little also appears as a summon gem in the video game, Kingdom Hearts II. 

 

===Reception===
{{Video game reviews
| title = Disneys Chicken Little
| width = 26m
| GR = (PC) 73.40%    (GBA) 71.67%    (GC) 68.45%    (Xbox) 68.44%    (PS2) 65.35%   
| MC = (PC) 71/100    (GBA) 71/100    (Xbox) 68/100    (GC) 67/100    (PS2) 60/100   
| GI = 7/10 
| GSpy =   
| GameZone = (GBA) 7.5/10  (PS2) 6/10 
| IGN = (GBA) 7.8/10  7/10  (PS2) 6.9/10 
| NP = 6/10 
| OPM =   
| OXM = 7.5/10 
| PALGN = 4/10 
| PCGUS = 74% 
| XPlay =   
| rev1 = The Times
| rev1Score =   
}}

The first Chicken Little game was met with average to mixed reception upon release.  GameRankings and Metacritic gave it a score of 73.40% and 71 out of 100 for the PC version;   71.67% and 71 out of 100 for the Game Boy Advance version;   68.45% and 67 out of 100 for the GameCube version;   68.44% and 68 out of 100 for the Xbox version;   and 65.35% and 60 out of 100 for the PlayStation 2 version.  

 

{{Video game reviews
| title = Disneys Chicken Little: Ace in Action
| width = 26m
| GR = (PS2) 71.40%    (Wii) 70.77%    (DS) 67.67%   
| MC = (Wii) 72/100    (PS2) 69/100    (DS) 66/100   
| EuroG = 7/10 
| GI = 7.25/10 
| GameZone = (PS2) 7.2/10  (Wii) 7/10  (DS) 6.9/10 
| IGN = (Wii) 7.6/10  (PS2) 7.5/10  (DS) 7/10 
| NP = (Wii) 7/10  (DS) 6.5/10 
| OPM = 7.5/10 
| PALGN = 7/10  
| VG = 6/10 
| XPlay =   
}}Ace in Action was also met with average to mixed reception upon release.  GameRankings and Metacritic gave it a score of 71.40% and 69 out of 100 for the PS2 version;   70.77% and 72 out of 100 for the Wii version;   and 67.67% and 66 out of 100 for the DS version.  

==Cancelled sequel==
  became Walt Disney Animation Studios new chief creative officer, he called all sequels and future sequels that DisneyToon had planned cancelled, along with a sequel to Meet the Robinsons and The Aristocats.   

 
==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 