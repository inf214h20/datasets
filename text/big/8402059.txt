Accident 703
{{Infobox film
|  name     =  Accidente 703 image          = Accidente703.jpg caption        = director       = José María Forqué producer       = Julio Irigoyen writer         = Vicente Coello   José María Forqué starring       = Manuel Alexandre   Ángel Álvarez   Carlos Ballesteros   Vicente Bañó   Maite Blasco  Frank Braña music          = Adolfo Waitzman cinematography = Juan Mariné editing        = Pedro del Rey distributor    = As Films S.A. (Spain) released       = 6 August 1962 runtime        = 84 minutes country        = Spain   Argentina
|  language       = Spanish
|  budget         =
|  followed_by    =
}} 1962 cinema Spanish drama film directed by José María Forqué and written by Vicente Coello.

==Release==
The film was released on August 6, 1962 in Spain and was released later in Argentina.

==Cast==
*Manuel Alexandre
*Ángel Álvarez
*Carlos Ballesteros
*Vicente Bañó
*Maite Blasco
*Frank Braña
*Ángela Bravo
*José María Caffarel
*Susana Campos
*Ricardo Canales
*Antonio Casas
*Antonio Cerro
*Enrique Closas
*Carlos Cores
*Francisco Cornet
*Miguel Ángel de la Iglesia ....  child
*Antonio Delgado
*Alejo del Peral
*Hebe Donay
*Enrique Echevarría
*Irán Eory
*Carlos Estrada
*Pedro Fenollar
*Lola Gálvez
*Gemma García
*Tito García
*Manolo Gómez Bur
*Julia Gutiérrez Caba
*Jesús Guzmán
*Lolita Herrera
*Guillermo Hidalgo
*Maribel Hidalgo
*Rufino Inglés
*María Luisa Lamata
*José Luis López Vázquez
*Carmen Lozano
*Jacinto Martín
*Maribel Martín ....  child
*José Morales
*Guadalupe Muñoz Sampedro
*José Orjas
*Rosa Palomar  (as Rosita Palomar)
*Erasmo Pascual
*Jesús Puente
*Elisa Romay
*Concha Sánchez
*Ángel Terrón
*Nuria Torray
*Ana María Ventura
*José Villasante

==External links==
*  

 
 
 
 
 


 
 