Los Tarantos
{{Infobox film
| name           = Los Tarantos
| image          =
| caption        =
| director       = Francisco Rovira Beleta
| writer       = Francisco Rovira Beleta Alfredo Mañas
| starring       = Antonio Gades Carmen Amaya
| music          = Andrés Batista Fernando García Morcillo Emilio Pujol José Solá
| cinematography = Massimo Dallamano
| editing        = Emilio Rodríguez
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}
 Best Foreign Film category.   

The film is based on the play La historia de los Tarantos written by Alfredo Mañas, and inspired by Romeo and Juliet by  William Shakespeare.

==Plot==
The love between two gipsies, Juana La Zoronga and Rafael El Taranto, from different families in Barcelona is thwarted by the enmity between their respective parents. Rafael sees Juana dance at a gipsy wedding, and is captivated by her beauty and charm, and they fall in love, aided by their younger siblings who are secretly friends and sympathetic to the young lovers.

Juana earns the respect of Rafaels formidable mother, Angustias, through her spirit and grace at flamenco, but her father Rosendo, an old beau of Rafaels mother, remains obstinate, despite the pleas of Juana, Rafael and Angustias. Juanas father offers her to his colleague, Curro, to make her forget about her romance with Rafael, but neither Juana nor Rafael can forget their love. Curro becomes arrogant, killing Rafaels friend Mojigondo, and beating Juana when he suspects she has been meeting with Rafael. Desperate, Juana seeks Rafael out in his dovecote and they make love, planning to elope the following day. But Curro, incited by Juanas brother Sancho, finds them together and kills them both. Rafaels brother subsequently hunts Curro down in his stables, and kills him.

Angustias and Rosendo are united in their grief, and Juanas younger brother comforts Rafaels younger sister, showing that the feud will not continue any further.

==Cast==
*Carmen Amaya	 ... 	Angustias
*Sara Lezana	... 	Juana
*Daniel Martín	... 	Rafael
*Antonio Gades	... 	Mojigondo
*Antonio Prieto	... 	Rosendo
*José Manuel Martín	... 	Curro (as J. Manuel Martín)
*Margarita Lozano	... 	Isabel
*Juan Manuel Soriano
*Antonia Singla	... 	Sole (as Antonia La Singla)
*Aurelio Galán El Estampío	... 	Jero (as A. Galán El Estampío)
*Peret... 	Guitarist
*Andrés Batista	... 	Guitarist
*Emilio de Diego	... 	Guitarist
*Pucherete	... 	Guitarist
*Blay	... 	Guitarist
*El Chocolate	... 	Cantaor
*La Mueque	... 	Cantaor
*Morita	... 	Cantaor (as Morità)
*Enrique Cádiz	... 	Cantaor
*El Viti	... 	Cantaor
*J. Toledo	... 	Cantaor
*Antonio Escudero El Gato	... 	Juan/Bailaor (as A. Escudero El Gato)
*D. Bargas	... 	Bailaor (as D. Bargas Lulula)
*Amapola	... 	Antonia/Bailaora
*El Guisa	... 	Bailaor
*Antonio Lavilla	... 	Sancho
*Francisco Batista
*Carlos Villafranca	... 	Salvador
*Josefina Tapias

==See also==
* List of submissions to the 36th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 