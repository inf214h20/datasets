Forward, Comrades
 

{{Infobox film
| name = Forward, Comrades
| image = Forward, comrades.jpg
| caption = Film poster
| film name = {{Film name| simplified = 前进，达瓦里希
| pinyin = Qiánjìn, dáwǎlǐxī }}
| director = Wang Yilin
| producer =  
| writer = Wang Yilin
| released =  
| runtime = 8 minutes 21 seconds
| country = China
| language = Mandarin, Russian, English
}}
Forward, Comrades ( ;  ) is a 2013 Chinese animated short film by Wang Liyin of the Beijing Film Academy. The film focuses on the fall of the Soviet Union as its main theme, told from the perspective of a young girl. As an original net animation with a strong political backdrop, the film has triggered strong reactions from various audiences.

==Plot== Comrade Vladimir Comrade Felix Comrade Beriya (贝利亚同志). Her mother is described as a schoolteacher "within our socialist motherland", who informs her that although their work is not supported by everybody, they cannot negate the merits of such work, and that their belief will never be effaced.

One day, the girl punishes Beriya for crimes against socialism and the people, having stolen one of her toy blocks, and gives Felix the most important command for the final decisive battle on a piece of paper, for him to safeguard. Her mother returns from work early, and the girl is surprised. On the radio, a broadcast in Russian informs the people of an anti-constitution coup détat instigated by right-wing reactionary forces. The adults become very unhappy, however the girl is unable to comprehend the reasons behind it. Her animals pass away one after another, many of her house possessions, such as books, are put out for sale by her mother, and Beriya is sold to a restaurant. Eventually she is told that they will need to leave their bungalow and move to the skyrise buildings; the girl remains unassured, fearing that the Americans might bomb their building.

Her mother throws away all of her old construction toys, and gives her brand new American dolls and other western toys, stating that "other children all like these toys". As the colour television displays   in the form of animals. As she sheds a tear and salutes, Felix, long dead and in military attire, passes her the note with the important command, to which she unfolds and reads: "Вперед, товарищи" (forward, comrades).

==Reception==
Response to the film has largely been polarized. Commentary in China for both sides in favour and against the film have hotly debated the imagery and symbology, which is largely considered to reflect the disintegration of the Soviet Union and the fall of socialism. Many of those praising the film regard it as a nostalgic revisit of the past, and interpret the plot as an analogy for Chinas independence from Soviet communism and formation of its own socialist development path;  critics on the other hand have slammed the films hypocritical pretensions, accusing it of beautifying the Red Terror, and ignoring the past misdeeds that occurred during the reign of the Soviet Union.  

The film has been uploaded onto YouTube and various domestic Russian websites with Russian subtitles, sparking a heated debate amongst various Russian internet users reflecting on the Soviet Unions legacy. 

==References==
 

==External links==
* 
* 
* 

 
 
 