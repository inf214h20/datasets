Porky's Pet
 
{{Infobox Hollywood cartoon
|cartoon_name=Porkys Pet
|series=Looney Tunes (Porky Pig)
|image=
|caption= Jack King
|story_artist=
|animator=Cal Dalton Sandy Walker
|background_artist=
|layout_artist=
|voice_actor=Joe Dougherty
|musician=Norman Spencer
|producer=Leon Schlesinger
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|studio=Warner Bros. Cartoons
|release_date=July 11, 1936
|color_process=Black-and-white
|runtime=6:35
|movie_language=English
}} 1936 Looney Looney Tune Jack King, animated by Cal Dalton and Sandy Walker, and its music was directed by Norman Spencer. Its main star is Porky Pig.

==Plot==
A mailman heads to Porky Pigs house and delivers a telegram to Porky. When Porky reads the telegram, he sees it is an offer from a big shot producer in Broadway theatre|Broadway, New York City, who wants Porky and his pet ostrich, Lulu in his show, offering $75 a day. Porky wants the job and, so he tells Lulu the good news and takes on a leash to the train station.

Once there a passenger train speeds right past the station and Porky has to change the signal to stop the second train. Porky and Lulu get on board, but the conductor kicks them off, on account of a "no pets" rule. Porky then tells Lulu to go down to the tracks so he can pick her up when the train passes by. Porky gets on board and the train departs. When it passes by Lulu, Porky grabs her and pulls her in. Realising what will happen if the conductor finds out, Porky shoves Lulu under his seat, but Lulu insists on poking her head out. She then squeezes out and swallows passengers personal belongings.

Just then, the conductor comes asking the passengers for tickets. Porky sees him, shoves the noisy accordion down Lulus throat to her stomach, and stuffs her inside a guitar case and trimming her sticky out tail feathers. When the conductor comes up to Porky, Lulu blows her cover by squawking, pushing her legs out, and taking the conductor on a wild ride to the other side of the coach. Angered, the conductor throws Lulu out and throws Porky out of the train from the observation car. Porky spots a handcar in a siding and a cow grazing. He and Lulu hop on the handcar, and Porky grabs the cows tail. The cow happily takes them down the track, and even outruns the train, much to the conductors shock.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 