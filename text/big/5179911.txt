Fortress 2: Re-Entry
{{Infobox film
| name           = Fortress 2: Re-Entry
| image          = fortress 2-re-entry.jpg
| image_size     =
| writer         = Troy Neighbors Steven Feinberg
| starring       = Christopher Lambert Aidan Rea David Roberson Liz May Brice Beth Toussaint Nick Brimble
| director       = Geoff Murphy John Davis John Flock
| cinematography = Hiro Narita
| distributor    = Columbia TriStar
| released       =  
| runtime        = 92 min.
| language       = English
| budget         = $11,000,000 (estimated)
| music          =
}}
Fortress 2: Re-Entry, directed by Geoff Murphy, is the sequel to the 1992 film Fortress (1993 film)|Fortress. In the film, the principal actor Christopher Lambert reprises his role as John Henry Brennick, still on the run from the MenTel Corporation. Lambert was the only original actor from Fortress, as his on-screen wife was re-cast and all his on-screen cell mates and the prison director perished in the original film.

==Plot==
 

Fortress 2 takes place about 10 years later. John Brennick is somewhere in North America, apparently still on the run since he takes a shotgun with him everywhere. His son Danny tells him that his wife Karen wants John to come home immediately. When they arrive there are three people waiting for them. They ask John to help them destroy Men-Tels new power station, saying that the company is on the verge of collapse and "without their power, they have no power". John refuses, wanting to protect his family, so the trio leave on a boat. As John waves goodbye, two Men-Tel helicopters appear and John scrambles his familys escape plan. He sends Danny and Karen through an underground passage while he leads the soldiers on a wild goose chase. The battle ends, though, with one helicopter destroyed, but Brennicks Jeep is overturned.

John is then knocked out and captured. He wakes up in a room with a disembodied voice telling him that he is in prison again and has been sentenced to death. He has been implanted with a behavior modification device which causes headaches of various intensity when prisoners enter prohibited areas. He also finds one of the men who visited him, a former Men-Tel vice president, who is now brain-damaged because of an improperly planted device. Another of Johns visitors, a former soldier, is also in the jail and friends with one of the guards.
 solar array.

Brennick tries to escape in a water-delivery shuttle but is caught and sent to "The Hole" - an exposed area of the ship where John is bombarded with solar radiation while the station faces the sun and extreme cold when its orbit takes it behind the Earth. When Men-Tels president arrives he tries to kill John by jettisoning him without a spacesuit. John manages to hold his breath and propel himself towards another airlock and back into the prison. Due to the sudden decompression, the computerized warden, Zed, begins to malfunction and cannot perform its duties. John uses a prison gun to destroy the computer and Teller is subsequently electrocuted. John and all his friends board the Shuttle and head back to Earth, where John reunites with his family.

==Critical reaction==
Almar Haflidason of the  , it does boast some reasonable CGI effects and is slightly more entertaining in a cheap kind of way."  Kim Newman of the British Film Institutes Sight & Sound said the film "lacks   wit and grit, though director Geoff Murphy makes it a decent enough ride." 
 the original." 

==See also==
* List of films featuring space stations

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 