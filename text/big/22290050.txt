Curiosity (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Curiosity
| image          = CuriosityPoster_200.png
| alt            =  
| caption        = Promotional poster
| director       = Toby Spanton
| producer       = 
| writer         = Toby Spanton Tom Riley
| music          = 
| cinematography = Craig Bloor
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 10 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Curiosity is a 2009 British short film written and directed by Toby Spanton.

==Plot==
Mike and Emma, after witnessing a murderer moving a body bag, enter into a game of cat and mouse when the killer realizes he has been seen.

==Cast== Tom Riley - Mike
*Emily Blunt - Emma

==Background==
Shot in June 2008 in Wandsworth, London, was produced by Pollibee Pictures. The film, now complete, is being entered into film festivals worldwide.

==External links==
*  
*  

 
 
 
 
 
 
 


 