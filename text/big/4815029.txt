The War Within (film)
{{Infobox film
| name           = The War Within  
| image          = The War Within.JPG  
| caption        = DVD cover for The War Within  
| director       = Joseph Castelo  
| producer       = Tom Glynn Jason Kliot Joana Vicente  Mark Cuban
| writer         = Ayad Akhtar Joseph Castelo Tom Glynn
| starring       = Ayad Akhtar Firdous Bamji Nandana Sen  Samrat Chakrabarti   Anjeli Chapman David Holmes, Stephen Hilton
| cinematography = Lisa Rinzler
| editing        = Malcolm Jamieson	 
| distributor    = Magnolia Pictures  
| released       =  
| runtime        = 119 minutes
}}
The War Within is a 2005 American drama film directed by Joseph Castelo and written by Ayad Akhtar, Joseph Castelo, and Tom Glynn. Distributed by HDNet Films and released by Magnolia Pictures, the film stars Ayad Akhtar, Firdous Bamji, Nandana Sen and Sarita Choudhury. The War Within premiered at the 2005 Toronto International Film Festival.  

==Plot== American intelligence terrorist activities. After his interrogation, Hassan undergoes a radical transformation and embarks upon a terrorist mission, surreptitiously entering the United States to join a cell based in New York City. After meticulous planning for an event of maximum devastation, the members of the cell are arrested, except for Hassan, Khalid and their cell leader Izzy. 

With no alternative and nowhere else to turn, Hassan must rely on the hospitality of his former best friend Sayeed, who is living the American dream with his family in New Jersey. To go forward and carry out his own attack, Hassan takes advantage of Sayeeds generosity while plotting his strategy and amassing materials to create explosives. Eventually, Hassans skewed religious fervor clashes with his feelings for Sayeed and his family, especially Sayeeds young son Ali, Sayeeds eight-year-old daughter Rasheeda, and Sayeeds sister Duri, whom Hassan begins to fall in love with. 

When Izzy is arrested Khalid and Hassan decide to use the explosives in a suicide attack on Grand Central Station instead. Duri discovers Hassan mixing the explosives in her brothers house. When Sayeed tries to stop him he knocks him out and runs away. Duri follows Hassan to stop the attack. At the last minute, Khalid loses his nerve and Hassan goes to the target alone. Duri arrives at Grand Central Station just before Hassan detonates his explosive belt. After the attack, Sayeed is held by the police who believe that he helped Hassan.

==Cast== 
  
*Ayad Akhtar as Hassan

*Firdous Bamji as Sayeed

*Nandana Sen as Duri

*Sarita Choudhury as Farida

*Charles Daniel Sandoval as Khalid

*Varun Sriram as Ali

*Anjeli Chapman as Rasheeda

*John Ventimilgia as Gabe

*Aasif Mandvi as Abdul

*Ajay Naidu as Naveed

*Kamal Marayti as the Imam

*Samrat Chakrabarti as Interrogator

*Wayman Ezeil as Izzy

*Mike McGlone as Mike OReilly

*Christopher Castelo as Steven

*James Rana as Saudi Man

*Christine Commesso as News Anchorwoman

*John Zibell as Officer Carroll

*Angel Desai as Reporter

==Production notes==
 Columbia Universitys Palestinian suicide bomber.  They approached both Miramax Films and Fine Line Features to finance the film, but both companies refused, citing that the subject matter was too controversial for American audiences. 

The film was shot on location in New York City and Jersey City. Sameer Bajar and Afia Nathaniel provided the Urdu dialogues for the film.

==Reception==
The War Within received mainly positive reviews from critics. It has an aggregate rating of 72% on Rotten Tomatoes   and 61 out of 100 on Metacritic.  

==DVD release==
The DVD for The War Within was released on January 17, 2006. It features commentary by Joseph Castelo and Ayad Akhtar, 8 deleted scenes and an alternate beginning. 

==Awards==
*2005 Nomination for Satellite Award for Best Film – Drama
*2005 Nomination for Satellite Award for Best Original Screenplay
*2006 Nomination for Independent Spirit Award for Best Screenplay
*2006 Nomination for Independent Spirit Award for Best Supporting Male - Firdous Bamji 
 
==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 