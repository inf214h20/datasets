Olympia (1938 film)
{{Infobox film
| name           = Olympia
| image          = Olympia_Poster.jpg
| caption        = VHS cover art
| director       = Leni Riefenstahl
| producer       = Leni Riefenstahl
| writer         = Leni Riefenstahl
| narrator       =
| starring       =
| music          = Herbert Windt Walter Gronostay
| cinematography = Paul Holzki
| editing        = Leni Riefenstahl
| studio         = Olympia-Film
| distributor    = Tobis Müller Taurus (video)
| released       =  
| runtime        = 126 minutes (part I) 100 minutes (part II) 226 minutes (combined)
| country        = Germany
| language       = German
}}
 
Olympia is a 1938   techniques, which later became industry standards but which were groundbreaking at the time, were employed —including unusual camera angles, smash cuts, extreme close-ups, placing tracking shot rails within the bleachers, and the like. The techniques employed are almost universally admired, but the film is controversial due to its political context. Nevertheless, the film appears on many lists of the greatest films of all-time, including Time (magazine)|Time magazines "All-Time 100 Movies." 

Olympia set the precedent for future films documenting and glorifying the Olympic Games, particularly the Summer Games. The 1936 Summer Olympics torch relay was devised by the German sports official Dr. Carl Diem for these Olympic Games in Berlin. Riefenstahl later staged the torch relay for this film, as with competitive events of the Games.

==Versions== French and English language|English.  There are slight differences between each version, extending to which portions were included and their sequence within the entire film.  The French version is known by the alternate title Les Dieux du Stade.

It appeared to be Riefenstahls habit to re-edit the film upon re-release, so that there are multiple versions of each language version of the film. For example, as originally released, the famous diving sequence (the penultimate sequence of the entire film) ran about four minutes. Riefenstahl subsequently reduced it by about 50 seconds. (The entire sequence could be seen in prints of the film circulated by the collector Raymond Rohauer.) 

==Reception==
The film had an immensely strong reaction in Germany and was received with acclaim and accolades around the world.  In 1960, her peers voted the film as one of the 10 best films of all time. The Daily Telegraph recognised the film as "even more technically dazzling" than Triumph of the Will.  The Times described the film as "visually ravishing...A number of sequences in the supposedly documentary Olympia, notably that devoted to the high-diving competition, become less and less concerned with record and more and more abstract: some of the divers hit the water, as the visual interest of patterns of movement takes over."   The Times. 10 September 2003 
 Wagnerian deity... But that was in 1934–35. In   Riefenstahl gave the same heroic treatment to Jesse Owens..."    

The film won a number of prestigious film awards but fell from grace, particularly in the United States when, in November 1938, the world learned of the pogrom against the Jews. Riefenstahl was touring the U.S. to promote the film at that time and was immediately asked to leave the country. 

===Awards===
The film won several awards; 

*National Film Prize (1937–1938) Venice International Film Festival (1938) — Coppa Mussolini (Best Film)
*Swedish Polar Prize (1938)
*Greek Sports Prize (1938)
*Olympic Gold Medal of the Comité International Olympique (1939)
*Lausanne International Film Festival (1948) — Olympic Diploma

==Re-release==
There had been few screenings of Olympia in English-speaking countries upon its original release. In 1955 Riefenstahl agreed to remove three minutes of Hitler footage for screening at the Museum of Modern Art in New York. The same version was also screened on West German television and in cinemas around the world.   

==References==
 

==Further reading==
* Rossol, Nadine. "Performing the Nation: Sports, Spectacles, and Aesthetics in Germany, 1926-1936," Central European History Dec 2010, Vol. 43 Issue 4, pp 616–638
* McFee, Graham and Alan Tomlinson. "Riefenstahls Olympia: Ideology and Aesthetics in the Shaping of the Aryan Athletic Body," International Journal of the History of Sport, Feb 1999, Vol. 16 Issue 2, pp 86–106
*Mackenzie, Michael, “From Athens to Berlin: The 1936 Olympics and Leni Riefenstahl’s Olympia,” in: Critical Inquiry, Vol. 29 (Winter 2003)
* Rippon, Anton. Hitlers Olympics: The Story of the 1936 Nazi Games 2006

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 