Chicago (2002 film)
 
{{Infobox film name           = Chicago image          = Chicagopostercast.jpg caption        = Theatrical release poster director       = Rob Marshall producer  Martin Richards screenplay     = Bill Condon based on       =   starring       = Renée Zellweger Catherine Zeta-Jones Richard Gere Queen Latifah music      = John Kander (music) Fred Ebb (lyrics) Danny Elfman (original score) cinematography = Dion Beebe editing  Martin Walsh studio      = Producer Circle Co. Storyline Entertainment distributor    = Miramax Films released       =   runtime        = 113 minutes   country        = United States language       = English Hungarian budget         = $45 million    gross          = $306.8 million 
}} musical comedy comedy crime adapted from satirical Musical stage musical of the same name, exploring the themes of celebrity, scandal, and corruption in Jazz Age Chicago.  The film stars Catherine Zeta-Jones, Renée Zellweger and Richard Gere, and also features John C. Reilly, Queen Latifah, Christine Baranski, Taye Diggs, Lucy Liu, Colm Feore, and Mýa|Mýa Harrison.

Chicago centers on Velma Kelly (Zeta-Jones) and Roxie Hart (Zellweger), two murderesses who find themselves in jail together awaiting trial in 1920s Chicago. Velma, a vaudevillian, and Roxie, a housewife, fight for the fame that will keep them from the gallows.
 Directed and choreographed by Best Picture. The film was critically lauded, and was the first musical to win Best Picture since Oliver! (film)|Oliver! in 1969.

==Plot==
In Chicago, 1924, naïve Roxie Hart visits a nightclub where star Velma Kelly performs ("All That Jazz"). Roxie begins an affair with Fred Casely, whom she believes will make her a vaudeville star. After the show, Velma is arrested for killing her husband and sister after finding them in bed together. A month passes and Casely, when Roxie becomes too clingy for his taste, admits that he lied about his connections so she would sleep with him. Enraged, Roxie fatally shoots him with a gun and convinces her husband Amos to take the blame, telling him she has killed a burglar and that he is likely to be released on self-defense. As he confesses to the detective, Roxie fantasizes that she is singing a song devoted  to her husband ("Funny Honey"). However, when the detective brings up evidence that Roxie knew Casely and they were having an affair, Amos comes clean and Roxie furiously admits what happened and is sent to Cook County Jail. Ambitious District Attorney Harrison informs the press he intends to seek the death penalty.

Upon her arrival Roxie is sent to Murderess Row, under the care of the corrupt Matron "Mama" Morton, ("When Youre Good to Mama"). Roxie meets Velma and learns the backstories of the other women in Murderess Row ("Cell Block Tango"). She attempts to befriend Velma, whom she idolizes, but is rudely rebuffed. On Mortons advice, Roxie decides to engage Velmas lawyer, the brilliant Billy Flynn ("All I Care About"). Flynn and Roxie manipulate the press at a press conference, reinventing Roxies identity as an originally virtuous woman turned bad by the fast life of the city; she claims to have had an affair with Casely because Amos was always working, but wanted to reform herself and start a family with Amos, which made Casely jealous. The press believe the story and turn her into a tragic heroine praised by the public ("We Both Reached for the Gun"). Roxie becomes an overnight sensation ("Roxie"), which infuriates Velma as it takes away the attention from herself. Velma tries to convince Roxie to doing a double-act, replacing the sister that she murdered ("I Cant Do It Alone"), but Roxie, now the more popular one of the two, snubs her like Velma originally did and the two begin a rivalry.

Roxies fame dwindles when Kitty Baxter, a wealthy heiress, is arrested for the murder of her husband and his two lovers, and the press and Flynn pay more attention to Kitty. Roxie, to Velmas surprise, quickly steals back the fame when she pretends to be pregnant. However, Amos is ignored by the press ("Mister Cellophane"), and Flynn, to create more sympathy for Roxie, convinces Amos that the child is not his and that he should divorce Roxie in the middle of her predicament. Roxies fame makes her arrogant and over-confident and refuses Flynns command to wear a very modest dress for her trial, and fires him when she believes she can win on her own. However, when she sees one of the women in Murderess Row hanged, a Hungarian heavily implied to be innocent, she realizes the gravity of the situation and re-hires Flynn.

Roxies trial begins and Billy turns it into a media spectacle ("Razzle Dazzle") with the help of the sensationalist reports of newspaper reporter and radio personality, Mary Sunshine. Billy discredits witnesses, manipulates evidence, and even stages a reunion between Amos and Roxie when she admits that the child is his, and they publicly reconcile. The trial seems to be going in Roxies way until Velma appears with Roxies diary, where she reads incriminating diary entries in exchange for amnesty. Billy discredits the diary, implying that the prosecuting attorney was the one who planted the evidence ("A Tap Dance"). Roxie is acquitted, but her fame dies a few seconds later when a woman shoots her husband just outside the court. Flynn tells her to accept it, and admits that he tampered with her diary and gave it to Mama, who gave it to Velma, in order to both incriminate the district attorney and free two clients at once. Amos remains loyal and excited to be a father, and she cruelly rejects him and reveals her fake pregnancy, and he finally leaves her.

Roxie eventually becomes a vaudeville actress, but is very unsuccessful ("Nowadays"). Velma approaches her, implied to be just as unsuccessful, and suggests that a pair of murderesses in vaudeville would become a famous act. Roxie refuses at first, but accepts when they realize that they can perform together despite their resentment for each other. The two stage a spectacular performance that earns them the love of the audience and the press ("Nowadays / Hot Honey Rag"). The film concludes with Roxie and Velma receiving a standing ovation from an enthusiastic audience, and, as flashbulbs pop, proclaiming that "We couldnt have done it without you".

==Cast==
*Catherine Zeta-Jones as Velma Kelly, a showgirl who is arrested for the murders of her husband, Charlie, and her sister, Veronica. Roxanne "Roxie" Hart, a housewife who aspires to be a Vaudeville|vaudevillian, and is arrested for the murder of her deceitful lover.
*Richard Gere as Billy Flynn, a duplicitous, smooth-talking lawyer who turns his clients into celebrities to gain public support for them.
*John C. Reilly as Amos Hart, Roxies naïve, simple-minded but devoted husband. matron of the Cook County Jail.
*Christine Baranski as Mary Sunshine, a sensationalist reporter.
*Taye Diggs as The Bandleader, a shadowy, mystical master of ceremonies who introduces each song.
*Lucy Liu as Kitty Baxter, a millionaire heiress who briefly outshines Velma and Roxie when she kills her husband and his two mistresses.
*Colm Feore as Harrison, the prosecutor in both Roxie and Velmas court cases.
*Mýa|Mýa Harrison as Mona, a prisoner on murderers row.
*Dominic West as Fred Casely, Roxies deceitful lover and murder victim.
*Jayne Eastwood as Mrs. Borusewicz, the Harts neighbor from across the hall.
*Chita Rivera as Nicky, a prostitute.
*Susan Misner as Liz, a prisoner on murderers row.
*Denise Faye as Annie, a prisoner on murderers row .
*Ekaterina Chtchelkanova as The Hunyak (Katalin Helinszki), a Hungarian prisoner on murderers row who does not speak English except for two words: "not guilty".
*Conrad Dunn as Doctor
*Deidre Goodwin as June, a prisoner on murderers row.

==Musical numbers==
 
#"Overture / All That Jazz" – Velma, Company
#"Funny Honey" – Roxie
#"When Youre Good to Mama" – Mama
#"Cell Block Tango" – Velma, Cell Block Girls
#"All I Care About" – Billy, Chorus Girls
#"We Both Reached for the Gun" – Billy, Roxie, Mary, Reporters
#"Roxie" – Roxie, Chorus Boys
#"I Cant Do It Alone" – Velma
#"Mister Cellophane" – Amos
#"Razzle Dazzle" – Billy, Company
#"A Tap Dance" - Billy
#"Class" – Velma and Mama (cut from film; included in DVD and 2005 broadcast premiere on NBC, and film soundtrack album)
#"Nowadays" – Roxie
#"Nowadays / Hot Honey Rag" – Roxie, Velma
#"I Move On" – Roxie and Velma (over the end credits)
#"All That Jazz (reprise)" – Velma, Company

==Production and development== Oscar for his direction of the film version of Cabaret (1972 film)|Cabaret (1972). Although he died before realizing his version, Fosses distinctive jazz choreography style is evident throughout the 2003 film, and he is thanked in the credits.
 cutaway scenes in the mind of the Roxie character, while scenes in "real life" are filmed with a hard-edged grittiness. (This construct is the reason given by director Marshall why "Class," performed by Velma & Mama, was cut from the film.) 
 play by film in which Gaertner herself had a cameo.
 Gooderham and Elgin Theatre, Union Station, Danforth Music Old City Hall.  All vocal coaching for the film was led by Toronto-based Elaine Overholt, whom Richard Gere thanked personally during his Golden Globe acceptance speech.

==Release==

===Critical response===
On the review aggregate website  , the film averaged a critical score of 82 (indicating "universal acclaim").  The cast received widespread universal acclaim for their performances. 

Tim Robey, writer for   called it "Big, brassy fun". 

However, other reviews claimed that there were issues with the film being too streamlined, and minor complaints were made toward Marshalls directing influences. AMC critic Sean OConnell explains in his review of the film that "All That Jazz", "Funny Honey", and "Cell Block Tango" play out much like youd expect them to on stage, with little enhancement (or subsequent interference) from the camera. But by the time "Razzle Dazzle" comes around, all of these concerns are diminished. 

===Box office=== Mamma Mia!.

===Legacy=== Mamma Mia! Les Misérables, Rock of Sunshine on Into The West End Dundee Reps production).

===Home media=== deleted musical number called "Class", performed by Zeta-Jones and Queen Latifah.

==Awards and nominations==
 
{|class="wikitable" 
|-
! Category
! Nominee
! Result
|-
! colspan="3"| Academy Awards    
|- Best Picture Martin Richards
|  
|- Best Actress
| Renée Zellweger
|  
|- Best Supporting Actor
| John C. Reilly
|  
|- Best Supporting Actress
| Catherine Zeta-Jones
|  
|-
| Queen Latifah
|  
|- Best Director
| Rob Marshall
|  
|- Best Adapted Screenplay
| Bill Condon
|  
|- Best Cinematography
| Dion Beebe
|  
|- Best Art Direction
| John Myhre and Gordon Sim
|  
|- Best Costume Design
| Colleen Atwood
|  
|- Best Film Editing Martin Walsh
|  
|- Best Sound Mixing David Lee
|  
|- Best Original Song
| John Kander (for "I Move On")
|  
|-
! colspan="3"| BAFTA Awards 
|- Best Film
|
|  
|- Best Actress
| Renée Zellweger
|  
|- Best Supporting Actress
| Catherine Zeta-Jones
|  
|-
| Queen Latifah
|  
|- David Lean Award for Direction
| Rob Marshall
|  
|- Best Cinematography
| Dion Beebe
|  
|- Best Production Design
| John Myhre
|  
|- Best Costume Design
| Colleen Atwood
|  
|- Best Make Up and Hair
| Judi Cooper-Sealy
|  
|- Best Editing
| Martin Walsh
|  
|- Best Sound
| Michael Minkler, David Lee and Dominick Tavella
|  
|- Anthony Asquith Award for Film Music
| Danny Elfman
|  
|-
! colspan="3"| Golden Globes 
|- Best Motion Picture – Musical or Comedy
|
|  
|- Best Actor – Musical or Comedy
| Richard Gere
|  
|- Best Actress – Musical or Comedy
| Renée Zellweger
|  
|-
| Catherine Zeta-Jones
|  
|- Best Supporting Actor
| John C. Reilly
|  
|- Best Supporting Actress
| Queen Latifah
|  
|- Best Director
| Rob Marshall
|  
|- Best Screenplay
| Bill Condon
|  
|-
! colspan="3"| Critics Choice Movie Awards 
|- Best Picture
|
|  
|- Best Supporting Actress
| Catherine Zeta-Jones
|  
|- Best Acting Ensemble
|
|  
|-
! colspan="3"| Chicago Film Critics Association Award
|- Best Actress
| Renée Zellweger
|  
|-
! colspan="3"| Dallas-Fort Worth Film Critics Association Award
|- Best Picture
|
|  
|-
! colspan="3"| Directors Guild of America Awards
|- Outstanding Directing
| Rob Marshall
|  
|-
! colspan="3"| Evening Standard British Film Awards
|-
| Best Actress
| Catherine Zeta-Jones
|  
|-
! colspan="3"| Florida Film Critics Circle
|- Best Song
| "Cell Block Tango"
|  
|-
! colspan="3"| National Board of Review of Motion Pictures
|-
| Best Directorial Debut
| Rob Marshall
|  
|-
! colspan="3"| Online Film Critics Society Awards 
|- Best Supporting Actress
| Catherine Zeta-Jones
|  
|- Best Ensemble
|
|  
|- Best Breakthrough Filmmaker
| Rob Marshall
|  
|- Best Costume Design
| Colleen Atwood
|  
|- Best Editing
| Martin Walsh
|  
|-
! colspan="3"| Phoenix Film Critics Society
|-
| Best Picture
|
|  
|-
| Best Actress
| Renée Zellweger
|  
|-
| Best Supporting Actress
| Catherine Zeta-Jones
|  
|-
| Best Acting Ensemble
|
|  
|-
| Best Director
| Rob Marshall
|  
|-
| Best Cinematography
| Dion Beebe
|  
|-
| Best Costume Design
| Colleen Atwood
|  
|-
| Best Film Editing
| Martin Walsh
|  
|-
| Best Newcomer
| Rob Marshall
|  
|-
! colspan="3"| Producers Guild of America Award
|- Best Picture
| Martin Richards
|  
|-
! colspan="3"| Screen Actors Guild Awards 
|- Best Actress
| Renée Zellweger
|  
|- Best Actor
| Richard Gere
|  
|- Best Supporting Actress
| Catherine Zeta-Jones
|  
|-
| Queen Latifah
|  
|- Best Acting Ensemble
|
|  
|-
! colspan="3"| Writers Guild of America Award
|- Best Adapted Screenplay
| Bill Condon
|  
|}

==References==
 
* http://www.altfg.com/blog/classics/chicago-1927-trivia/

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 