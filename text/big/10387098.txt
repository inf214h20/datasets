Guest Wife
{{Infobox film
| name           = Guest Wife
| image          = Guestwife1945.jpg
| image_size     =
| caption        =
| director       = Sam Wood
| producer       = Jack H. Skirball
| writer         = John D. Klorer Bruce Manning
| starring       = Claudette Colbert Don Ameche Dick Foran
| music          = Daniele Amfitheatrof
| cinematography = Joseph A. Valentine William Morgan
| studio         = Greentree Productions
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
}}
Guest Wife is a 1945 American comedy film directed by Sam Wood, written by Bruce Manning and John Klorer, and starring Claudette Colbert, Don Ameche and Dick Foran. It is also known as What Every Woman Wants.
 Academy Award for Best Music, Scoring of a Dramatic or Comedy Picture (Daniele Amfitheatrof).

==Plot==
Banker Christopher Price (Dick Foran) from the humdrum town of Keetoosen, Ohio, is happily married to Mary (Claudette Colbert), and the couple are just about to go to New York on their second honeymoon. However, Chris old childhood friend Joe Parker (Don Ameche), a known newspaper reporter who has been stationed abroad, sends him a message just before they begin their journey. Joe arrives in Keetoosen before the couple leaves, and explains that he has lied to his boss about being martied to Mary, to get a longer vacation in the past. Now, he is to work in New York, and needs to "borrow" Mary to pretend that she is his wife, to save his career. Mary wants nothing to do with this, but Chris agrees to help out, lending Joe his wife. Joe and Mary go ahead to New York, but Chris is delayed because the trains are full. When they arrive to New York, Joe and Mary are taken to a press conference immediately, and their picture end up all over the papers. It turns out Joes lies were a tad larger than he first said, since he has faked letters from his loving wife - letters that his boss, Arthur Truesdale Worth (Charles Dingle), has read. Chris experiences some large bumps on his way to New York, as his boss Arnold (Edward Fielding) sees the pictures of Joe and Mary in the papers, and believes Chris is an adulterer. Arnold forces Chris to stay on in Keetoosen a few days longer to fend off a scandal. Another person from Keetoosen recognizes Mary clubbing with Joe in New York. When Chris eventually makes it to New York the evening after, Worth and other people get suspicious of his interest in "Mrs. Parker". Eventually Mary pretends to be in love with Joe, and even tells her friend Suzy (Marlo Dwyer) about this. On invitation from Worth, she comes out to Long Island to his house, with Joe as company. Chris finds out where they are and sneaks into the house. He finds Joe and Mary under a romantic night sky on a balcony, and believes Mary has fallen for the reporter. The truth is that Mary pretends to be suicidal, threatening to jump off the balcony because Joe doesnt return her feelings for him. Chris knocks out Joe and takes Mary away in his car. Joe takes the opportunity to play the devastated husband who has been left by his wife, and gets sympathy from Worth. Mary is quite happy with Chris actions and that he finally stood up to his friend. 

==Cast==
*Claudette Colbert as Mary Price 
*Don Ameche as Joseph Jefferson Parker 
*Dick Foran as Christopher Price 
*Charles Dingle as Arthur Truesdale Worth  Grant Mitchell as House Detective 
*Marlo Dwyer as Susy (as Wilma Francis) 
*Chester Clute as Urban Nichols 
*Irving Bacon as Nosey Stationmaster 
*Hal K. Dawson as Dennis 
*Edward Fielding as Arnold

==External links==
* 
*  
*  
* 

==References==
 

 

 
 
 
 
 
 
 