Dementia (1955 film)
{{Infobox Film | name =Dementia
 | image = Dementiamovie.jpg
 | caption =DVD cover John Parker
 | producer = John Parker Ben Roseman Bruno VeSota (as Bruno Ve Sota)
 | writer = John Parker
 | starring = Adrienne Barrett Bruno VeSota (as Bruno Ve Sota) Ben Roseman Angelo Rossitto
 | cinematography = William C. Thompson
 | music = George Antheil Shorty Rogers	
 | editing = Joseph Gluck	
 | distributor = 
 | released = December 22, 1955 (U.S. release)
 | runtime = 58 min 
 | language = English
 | budget = 
 }} John Parker, expressionist film.  It was produced in 1953, but not released until 1955. Info on DVD release by "Kino Video", 2000. 

==Synopsis==
A young woman awakens from a nightmare in a run down hotel. She leaves the building and wanders through the night, passing a newspaper man. The news headline "Mysterious stabbing" catches her eye, and she quickly leaves. In a dark alley, a wino approaches and grabs her. A policeman rescues her and beats up the drunken man. Shortly later, another man approaches her and talks her into escorting a rich man in a limousine. While they cruise the night, she remembers her unhappy youth with an abusive father, whom she stabbed to death with a switchblade after he had killed her unfaithful mother. The rich man takes her to various clubs and then to her noble apartment. As he ignores her while having an extensive meal, she tries to tempt him. When he advances her, she stabs him with her knife and pushes the dying man out of the window. Before his fall, he grabs her pendant. The woman runs down onto the street and, as the dead mans hand wont relieve her pendant, cuts off the hand while being watched by faceless passersby. Again, the patrol policeman shows up and follows her. She flees and hides the hand in a flower girls basket. The pimp shows up again and drags her into a night club, where an excited audience watches a jazz band playing. The policeman enters the club, while the rich man, lying at the window, points out his murderess with his bloody stump. The crowd encircles the woman, laughing frantically. The woman wakes up in her hotel room, her encounters have supposedly been a nightmare. In one of her drawers, she discovers her pendant, clutched by the fingers of a severed hand. The camera leaves the hotel room and moves out into the streets, while a desperate cry can be heard.

==Production==
Dementia was shot in the studio in Hollywood and on location in Venice, Los Angeles|Venice, California. Production, including editing, ended in 1953. 

The original film had no dialogue, only music and some sound effects, such as doors slamming, dubbed laughter etc. The films musical score is by avant-garde composer George Antheil, vocalized by Marni Nixon–there are no lyrics as such. Jazz musician Shorty Rogers can be seen and heard performing in a night club scene.

Producer–writer–director John Parker is only credited as producer in the titles. In later years, actor and associate producer Bruno VeSota claimed to have co-written and co-directed the film with Parker. 

==Release==
In the US, Dementia premiered in New York City in 1955 with four cuts demanded by the censors. This version was picked up by Jack H. Harris and released as Daughter of Horror. Harris version also has music without dialogue, but with an added narration read by actor Ed McMahon. 

In the United Kingdom, the British Board of Film Classification denied Dementia (in the alternate Daughter of Horror version) a classification in 1957. In 1970, Dementia was finally passed without cuts.  

==Reception== Variety Quoted by DVD release by "Kino Video", 2000. 

"A piece of film juvenilia   despite its good intentions   An understanding of Mr. Parkers desire to say something new cannot reconcile one to the lack of poetic sense, analytical skill and cinematic experience exhibited here." – The New York Times 

"To what degree this film is a work of art, we are not certain but, in any case, it is strong stuff." – Cahiers du cinéma 

"The movie spends an hour exploring a lonely woman’s sexual paranoia through a torrent of expressionistic distortions which would look avant-garde if the vulgar Freudian ‘message’ weren’t so reminiscent of ’50s B features." – Time Out Film Guide 

==Legacy==
Dementia is perhaps most famous for its appearance in The Blob (1958), where it is the movie playing in the movie theater when the Blob strikes.

The film has been compared to The Cabinet of Dr. Caligari (1920) as being a portrait of an insane mind from the "inside out". 

== References ==
 

==Further reading==
* Re/Search No. 10: Incredibly Strange Films. Re/Search Publications, San Francisco 1986, ISBN 0-940642-09-3, p.&nbsp;179–180

==External links==
*  
*  
*  
*  
*   at Rotten Tomatoes

 
 
 
 
 
 
 