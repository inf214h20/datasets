Conjurer (film)
 
{{Infobox Film
| name           = Conjurer
| image_size     = 
| image          = Conjurer FilmPoster.jpeg
| caption        = 
| director       = Clint Hutchison
| producer       = Richard Mix Clint Hutchison Lance W. Dreesen
| writer         = Clint Hutchison David Yarbrough John Schneider
| music          = Dana Niu
| cinematography = Ken Blakey
| editing        = Otto Arsenault
| studio         = Red Five Entertainment
| distributor    = Monarch Home Video
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Conjurer is a 2008 supernatural horror film directed by Clint Hutchison and written by Hutchison and David Yarbrough.  The film had its world premiere on 11 September 2008 at the SoCal Independent Film Festival and was released to DVD on 25 November of the same year. It stars Andrew Bowen as a photographer caught up in a haunting.

==Plot== John Schneider), but hes willing to do whatever he can if it will give Helen solace. Frank promises that hell build the two of them a brand new house, but until the construction is done they must stay in an older house with a decrepit cabin in the back yard. Shortly after they move in, Helen becomes pregnant again.

Shawn is fascinated when he discovers via some neighbors that the cabin is reported to be haunted by the ghost of a witch that curses anyone trying to get pregnant, seeking revenge against a husband that murdered her own child years ago. While investigating the legend Shawn injures himself on a tooth left in the cabin and develops a severe infection. As the infection worsens Shawn begins to experience strange visions and events, unsure if they are real or delusions triggered by the infection and a possible latent mental illness, as his own father murdered his wife and then killed himself. This worries Shawn as either way this poses a potential threat to Helen, either by the witchs hand or by Shawns possible mental illness, and he decides that he will get Helen out of that place. This puts him at odds with Frank, who believes the land to be completely safe. 

As things grow more strange and Shawn becomes more unstable, things culminate in a chase scene that ends with Shawn firing a gun at his wife. Authorities are called to the scene and Shawn tries to explain the story of the witch, only to be told that there is no witch and that the neighbors (who had told him the story) never existed. Hes then taken to a mental institution, leaving Helen to live in the house by herself. The film ends with Helen listening to a phone message from Shawn and then turning to the camera with a malevolent look, leaving it up to the viewer to decide if the events in the film are the result of Shawns psychosis or if there actually is a witch and that she has possessed Helen at some point during the movie.

== Cast ==
* Andrew Bowen as Shawn Burnett
* Maxine Bahns as Helen Burnett John Schneider as Frank Higgins
* Tom Nowicki as Marlin Dreyer
*Brett Rice as Fulton Moss
*Edith Ivey as Mrs. Dreyer
*Terrence Gibney as Dr. Letson
*Dolan Wilson as Sheriff Clayton
*Liz McGeever as Hattie
*Nicholaus Teall as Moss Boy
*James Donadio as Tony Stegman
*Donna Biscoe as Dr. McKenzie
*Ryan Puszewski as Young Shawn

== Reception ==
Critical reception for Conjurer has been positive.  Dread Central gave the film three out of five blades and praised the movies acting and directing.  LA Weekly also praised the movie, which they stated had "a thick mood of gothic dread". 

===Awards===
*Best Horror Film at the Action on Film International Film Festival (2008, won) 
*Best Feature Film at the Dixie Film Festival (2008, won) 
*Honorable Mention at the Louisville Fright Night Film Fest (2008, won)
*Directorial Discovery Award at the Rhode Island International Horror Film Festival (2008, won) 

== See also ==
* List of ghost films

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 