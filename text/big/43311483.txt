What a Lie
  }}

{{Infobox film
| name           = What A Lie!
| director       = Muḥammad an-Najār
| producer       =  
| screenplay     = Nuhá al-‘Amrūsī Aḥmad ‘Abdullāh
| starring       = Aḥmad Ādam Muḥammad Sharf Mīsrah Amīra Fatḥī Amīra al-‘Āyadī Luṭfī Labīb
| music          = ‘Aṣām Kārīkā Hashām Jabar
| distributor    =  
| released       =  
| runtime        = 104 minutes
| country        = Egypt
| language       = Egyptian Arabic
}}
What A Lie! (  /   produced in 2007.

==Synopsis==
The film follows the story of Na‘nā‘ (played by Aḥmad Ādam), who lives in the house he inherited from his mother with his father (Sa‘īd Tarābīk) and his step mother (Mīsrah). Na‘nā‘ is music teacher who lost his eyesight at the age of three in a car accident. When it’s revealed that the piece of land he inherited has doubled in value, he is duped into marrying Layl (played by Amīra Fatḥī), who pretends to be a nurse. Over the course of time his eyesight returns and he begins to discover the truth. However, he is convinced by his best friend to keep it a secret, so that he can see for himself how exploitative Egyptian society has become of vulnerable people.

==References==
 

 