Bulldog Drummond Escapes
{{Infobox film
| name           = Bulldog Drummond Escapes
| image          =

| image_size     =
| caption        = James P. Hogan Stuart Walker (associate producer)
| writer         = Gerard Fairlie (play Bulldog Drummond Again) Edward T. Lowe Jr. (writer) Herman C. McNeile (play Bulldog Drummond Again)
| narrator       =
| starring       = Ray Milland
| music          =
| cinematography = Victor Milner William Shea
| distributor    =
| released       =
| runtime        = 67 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 James P. Hugh "Bulldog" Drummond.

==Plot==
Captain Hugh Bulldog Drummond has just returned to England. As he is driving home in the dark, a young woman jumps out in front of his car. He misses her, but she falls to the ground. As he tries to revive her, he hears a shout for help, then gunshots. As he goes to investigate, the woman drives away with Drummonds car. He is soon able to trace her to nearby Greystone Manor, and when he goes there to meet her, she urges him to help her get out of a desperate situation

===Differences from play===
 

==Cast==
*Ray Milland as Capt. Hugh Bulldog Drummond|"Bulldog" Drummond
*Guy Standing as 	Inspector Col. Sir Reginald Nielson Heather Angel as Phyllis Clavering Reginald Denny as Algy Longworth
*Porter Hall as Norman Merridew
*Fay Holden as Natalie Merridew Seldon
*E.E. Clive as "Tenny" Tennison
*Walter Kingsford as Professor Stanton
*P.J. Kelly as Stiles - the butler
*Charles McNaughton as Chief Constable Higgins
*Clyde Cook as Constable Alf Frank Elliott as Bailey David Clyde as Gower
*Doris Lloyd as Nurse
*Barry Macollum as Blodgson (uncredited)

==Soundtrack==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 