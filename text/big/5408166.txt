Sällskapsresan
{{Infobox film
| name           = Sällskapsresan
| image          = Sällskapsresan.jpg
| caption        = Swedish Blu-ray cover
| director       = Lasse Åberg, Peter Hald Bo Jonsson
| writer         = Bo Jonsson, Lasse Åberg 
| starring       = Lasse Åberg, Jon Skolmen, Lottie Ejebrant, Kim Anderzon, Roland Jansson, Magnus Härenstam
| music          = Bengt Palmers
| cinematography = 
| editing        = 
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 107 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
Sällskapsresan, released in English as The Charter Trip, is a 1980 Swedish comedy film and the first in a series directed by Lasse Åberg. The film has reached cult status in Sweden and a vast number of the Swedish people have seen it, and many of the lines in the movie are known and referenced frequently by many Swedes. It was entered into the 12th Moscow International Film Festival.   

==Plot==
The plot follows a stuffy, nerdy Swede, Stig-Helmer Olsson (Lasse Åberg), who travels to the fictional town of Nueva Estocolmo in  ) by telling him that he has an aunt at Nueva Estocolmo and gives Stig-Helmer the money in a christmas present. (Unknown to Stig-Helmer, the money is stuffed in a bread loaf.) Among the other travellers are two middle-aged women, Maj-Britt (Lottie Ejebrant) and Siv (Kim Anderzon); the alcoholic duo Berra Ohlsson (Sven Melander) and Robban Söderberg (Weiron Holmberg) who looks for a liquor store named Pepes Bodega throughout the movie; and the Storch couple who are laughed at by Berra and Robban because of the similar pronouncing of Storch and stork.

== Cast ==
*Lasse Åberg - Stig-Helmer Olsson
*Jon Skolmen - Ole Bramserud
*Lottie Ejebrandt - Maj-Britt Lindberg
*Kim Anderzon - Siv Åman
*Ted Åström - Lasse Lundberg
*Weiron Holmberg - Robban Söderberg
*Sven Melander - Berra Ohlsson
*Svante Grundberg - Herr Storch
*Eva Örn - Fru Storch
*Roland Jansson - Gösta Angerud
*Magnus Härenstam - Dr B. A:son Levander
*Gösta Ekman (uncredited) - Hotel Maid

==Reception==
The film has become a classic in Sweden, though it was not well received by Swedish critics at the time of release. Jan Aghed from Sydsvenska Dagbladet wrote "There are many funny gags in the beginning, and the film is very funny until the travellers has reached their destination. The film then goes on very slowly in poor humor and boredom." However, just like Åbergs previous film Repmånad the film became a box-office success with over 2,5 million people watching the film, making it the biggest cinema success in Sweden to that date.

The success of the film spawned a series of films about the character Stig-Helmer, mostly making fun of Swedish vacation activities:

* 1985 Sällskapsresan 2 - Snowroller (skiing)
* 1988 S.O.S. - En segelsällskapsresa (sailing)
* 1991 Den ofrivillige golfaren (golfing)
* 1999 Hälsoresan - En smal film av stor vikt (health spas)
* 2011 The Stig-Helmer Story (Chronicles about the young life of Stig-Helmer)

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 