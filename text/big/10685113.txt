Night of the Scarecrow
 
 
{{Infobox film
| name           = Night of the Scarecrow
| image          = Nightofthescarecrow.jpg
| caption        = Film poster
| director       = Jeff Burr
| writer         = Reed Steiner Dan Mazur
| starring       = Elizabeth Barondes John Mese Stephen Root Bruce Glover Dirk Blocker
| music          = Jim Manzie
| cinematography = Thomas L. Callaway
| editing        =
| distributor    = Republic Pictures
| released       =  
| runtime        = 85 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Night of the Scarecrow is a 1995 American horror film directed by Jeff Burr.

== Plot ==
 
In order for a town to prosper and have a successful harvest, the citizens made a deal with a warlock. When the people found out that the warlock was sexually preying on the towns women, they murdered him and buried his bones, which resulted in his ghost being confined to a scarecrow.

== Cast ==
* Elizabeth Barondes: Claire Goodman
* John Mese: Dillon
* Stephen Root: Uncle Frank/The Sherriff
* Bruce Glover: Uncle Thaddeus
* Dirk Blocker: Uncle George
* Howard Swain: The Scarecrow
* Gary Lockwood: Mayor Goodman John Hawkes: Danny Thompson
* William Joseph Barker: Kyle
* Martine Beswick: Barbara
* Cristi Harris: Stephanie

== Production ==
 

== Reception ==
 
While Variety wrote that the director "has more luck with physical scenes than he does with coaxing inspired performances out of his actors", Blu-ray.com wrote that the director "stages things very effectively and gets some good performances out of a game cast."       A reviewer for DVD Talk  did not like the "inept circa-1995 CGI".   

Jeffrey Kaufman of Blu-ray.com compared a violent scene in the film to a scene in another horror film with a murderous scarecrow, the television film Dark Night of the Scarecrow. 
Eric Hansen of Variety (magazine)|Variety wrote of the film and its original VHS release, "A moderately exciting, average monster movie with good production values and a few good ideas, “Night of the Scarecrow” looks to have its longest life on homevideo."  A review for TV Guide says, "A healthy dose of directorial style and energy makes this prosaic low-budget chiller a tense, entertaining diversion." 

Adam Tyner, writing for DVD Talk, said "Its kind of interesting seeing some of the familiar faces in the cast, and Night of the Scarecrow does trot out a few really nice looking setpieces, but none of thats enough to salvage this limp, lifeless, uninspired, instantly forgettable slasher flick. Skip It." 

== Home media ==
The original video release was in 1995 on VHS, distributed by Republic Pictures.  The film was released on DVD and Blu-ray by Olive Films in 2013, with the same special features&nbsp;– an audio commentary with director Jeff Burr, a making of video, and a picture gallery. 

== See also ==
* The Night of the Scarecrow
* Dark Night of the Scarecrow
* Scarecrow (2002 film)|Scarecrow
* Scarecrows (1988 film)|Scarecrows

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 