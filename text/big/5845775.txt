Last Best Chance
Last President Charles Ross.

The name of the film is a reference to a   by Abraham Lincoln.

==Premiere==

Running at forty-five minutes in length, Last Best Chance premiered in the fall of 2005 at the lavish East Side mansion that is home to the Council on Foreign Relations in New York City. Among the attendees were diplomats, military personnel, international bankers, and lawyers.  

Speakers at the event following the film included:

*Peter George Peterson| Pete Peterson, chairman of the Council on Foreign Relations 
*Ted Turner, founder of CNN
*Warren Buffett, investor and philanthropist 
*Richard Lugar| Sen. Richard Lugar, chairman of the Senate Foreign Relations Committee Nuclear Threat Initiative

The film was produced and funded by Nunn’s Nuclear Threat Initiative, the Carnegie Corporation of New York, and the MacArthur Foundation.

==See also==
*List of films about nuclear issues
*Nuclear Tipping Point

==References==
 

==External links==
* 
* 

 
 

 