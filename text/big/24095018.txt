Texas, Brooklyn and Heaven
{{Infobox film
| name           = Texas, Brooklyn and Heaven
| image          = Poster of the movie Texas, Brooklyn and Heaven.jpg
| image_size     =
| caption        =
| director       = William Castle
| producer       = Robert Golden (producer) Lewis J. Rachmil (associate producer)
| writer         = Barry Benefield (story "Eddie and the Archangel Mike") Lewis Meltzer (writer)
| narrator       =
| starring       = Guy Madison Diana Lynn
| music          = Arthur Lange
| cinematography = William C. Mellor
| editing        = James E. Newcom
| distributor    =
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = nearly $1 million HOLLYWOOD DEALS: Prospects Brighten for United Artists -Budget Runs Wild and Other Matters
By THOMAS F. BRADYHOLLYWOOD.. New York Times (1923-Current file)   01 Feb 1948: X5.  
| gross          =
}}
Texas, Brooklyn and Heaven (  film directed by William Castle and starring Guy Madison and Diana Lynn.

== Plot ==
Eddie Tayloe (Madison) is a reporter assigned to the Fort Worth, Texas|Ft. Worth desk of a Dallas newspaper, and as the two neighboring cities are feuding, therefore has nothing to do. He dreams of becoming a New York playwright, and a small inheritance from his grandfather gives him his chance. Quitting his job, he begins the long drive. Picking up hitchhiker Perry Denklin (Lynn), also looking for fame and fortune in New York, he shares with her encounters with various eccentric characters. The big city does not work out for either of them, and when Eddie finds Perry working in a Coney Island girlie show, he pulls her out and they find happiness together, buying a ranch back in Texas. 

== Cast ==
 
*Guy Madison as Eddie Tayloe
*Diana Lynn as Perry Dunklin James Dunn as Mike
*Lionel Stander as Bellhop
*Florence Bates as Mandy
*Michael Chekhov as Gaboolian Margaret Hamilton as Ruby Cheever
*Moyna Macgill (credit: Moyna Magill) as Pearl Cheever
*Irene Ryan as Opal Cheever Colin Campbell as MacWirther
*Clem Bevans as Capt. Bjorn
*William Frawley as Agent
*Alvin Hammer as Bernie
*Roscoe Karns as Carmody
*Erskine Sanford as Dr. Danson
*John Gallaudet as McGonical Audie Murphy as the copy boy (his first film role)  
 

==Production==
  Western star Audie Murphy. His similarly small role in Beyond Glory had been filmed previously, but that production did not see release until September.  Mary Grant designed the films costumes.

==References==
 

== External links ==
 
* 
* 
*  at TCMDB
*  at Audie Murphy Memorial Site

 

 

 
 
 
 
 
 
 
 
 
 