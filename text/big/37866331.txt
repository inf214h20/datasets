D' Kilabots Pogi Brothers Weh?!
{{Infobox film
| name = 
| image = 
| caption = D Kilabots Pogi Brothers Weh?!
| director = Soxy Topacio, DGPI Allan Quilantang
| writer = G. Phil Noble Marvic C. Sotto
| starring = Jose Manalo Wally Bayola Pokwang
| music =
| cinematography =
| editing =
| studio =  APT Entertainment  M-Zet Productions
| distributor = APT Entertainment  M-Zet Productions
| released =  
| runtime =
| country = Philippines
| language = Filipino English
| budget =
| gross = PHP 42,392,978 
}}

D Kilabots Pogi Brothers Weh?! is a 2012  , Wally Bayola, Solenn Heussaff, Pokwang, Paolo Ballesteros and Gina Pareno. It was made after Jose Manalo and Wally Bayola hit it big with a sold-out concert.

==Synopsis==
Two brothers become rivals in the affections of the same woman. Their rivalry tears them apart, and causes one of them to be heartbroken. But the two are forced to reunite in order to protect their family from a greedy businessman.

==Cast==
===Main cast===
* Jose Manalo as Justine Kilabot
* Wally Bayola as Bruno Kilabot
* Pokwang as Kitty Kilabot
* Solenn Heussaff as Lulu
* Paolo Ballesteros as Tweety

===Supporting cast===
* Gina Pareño as Sophia Kilabot
* Jimmy Santos as Jai-ho
* Mosang as J-Lo
* Diego	Llorico as Ngengio	
* Tirso Cruz III as Donald Trang
* Michael de Mesa as Mr. Lucio

===Special Participation===
* Allan K. as Security Guard
* Maricel Soriano as Female Partner
* Roderick Paulate as Male Partner
* Vic Sotto as  Bossing Chairman
* German Moreno as Kuya Hermano
* Victor Basa as Police Officer
* Michael V. as MMDA Traffic Enforcer
* Aljur Abrenica as Police Officer
* Eddie Garcia as Police Superintendent
* Niña Jose as Justines suitor
* Nyoy Volante as Guitarist
* Janica Pareño as Donalds maid

==Reception==
{{Album ratings
| rev1 = Click The City
| rev1Score =   
| rev2 = Pisara
| rev2Score =     
}}

===Critical Response===

Philbert Ortiz Dy of ClickTheCity.com rated the film 1.5 out of 5.    He stated, "The end credits of DKilabots Pogi Brothers, Weh? share space with little vignettes of the two lead actors thanking the various guest stars of the movie for showing up. Many of these stars end up making the same joke: that they aren’t getting paid. These vignettes sum up the problem with the movie as a whole. For one thing, much of the movie seems to have been designed around these cameo appearance. And secondly, the movie has a tendency to repeat its humor. These problems are compounded by an odd production that can’t seem to decide what the name of one of the characters is."

===Trivia===
* The main characters are derived from name of artists, singer, song, politicians, businessmen and toys
* This is also second movie of Jose Manalo and Wally Bayola under APT Entertainment and M-Zet TV Production Inc. after the success of their first movie Scaregiver(2008) TV5

===Rating===
It was graded "PG-13" by the Cinema Evaluation Board of the Philippines. 

==References==
 

 
 
 