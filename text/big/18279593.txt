The Cap (film)
{{Infobox Film
| name           =  The Cap
| image          = 
| image_size     = 
| caption        = 
| director       =   Andy Thomson Robert Verrall
| writer         =  writer Robert A. Duncan story Morley Callaghan
| narrator       =
| starring       =  Michael Ironside Nicholas Podbrey
| music          =  Chris Crilly
| cinematography =  Andreas Poulsson
| editing        =  Ginny Stikeman Atlantis Films
| distributor    =  WonderWorks
| released       =  
| runtime        =   24 min.
| country        =  Canada English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Atlantis Films Limited and the National Film Board of Canada.

==Plot==
The plot centers on a young boy from Montreal named Steve (Nicholas Podbrey) who is given a baseball cap by his idol, Andre Dawson of the Montreal Expos. One day, Steve loses the cap and soon after discovers that it was found by the boy of a wealthy businessman. Steve along with his unemployed father (Michael Ironside), go to the wealthy boys house to discuss the matter. After a long and heated debate, Steve and his father leave empty-handed.

==Cast==
* Michael Ironside
* Nicholas Podbrey
* Jennifer Dale
* David Connor
* Steven Bednarski
* Roger Michael
* Andre Dawson
* Duke Snider
* Dave Van Horne
* Tim Raines

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 