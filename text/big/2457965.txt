A Century of Cinema
{{Infobox film
| name           = A Century of Cinema
| image          = 
| alt            =  
| caption        = 
| director       = Caroline Thomas
| producer       = British Film Institute (BFI) Bob Thomas
| starring       = 
| music          = 
| cinematography =  William Cole
| studio         = 
| distributor    = Miramax Films
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 documentary directed by Caroline Thomas about the art of filmmaking (coinciding with cinemas 100th anniversary), containing numerous interviews with some of the most influential film personalities of the 20th century.

==Celebrities interviewed==
 
*Richard Attenborough
*Dan Aykroyd
*Kim Basinger
*Milton Berle
*George Burns
*Tim Burton
*Nicolas Cage
*Marge Champion
*Geraldine Chaplin
*Chevy Chase
*Roger Corman
*Kevin Costner
*Billy Crystal
*Tony Curtis
*Joe Dante
*Kirk Douglas
*Robert Downey Jr.
*Clint Eastwood
*Alice Faye
*Sally Field
*Jane Fonda
*Harrison Ford
*Morgan Freeman
*Kathryn Grayson
*Jack Haley, Jr.
*Charlton Heston
*Bob Hope
*Anthony Hopkins
*Bob Hoskins
 
*Norman Jewison
*Jessica Lange
*Spike Lee
*Shirley MacLaine
*Bette Midler
*Ann Miller
*Liza Minnelli
*Demi Moore
*Maureen OHara
*Sidney Poitier
*Richard Pryor
*Dennis Quaid
*Burt Reynolds
*Julia Roberts
*Cesar Romero
*Mickey Rooney
*Meg Ryan
*Mark Rydell
*Arnold Schwarzenegger
*Steven Spielberg
*Sylvester Stallone
*James Stewart
*Meryl Streep
*Donald Sutherland
*Jessica Tandy
*Paul Verhoeven
*Denzel Washington
*Shelley Winters
*Robert Wise
 

==See also==
* Cinema of the world

==External links==
* 

 
 
 
 


 