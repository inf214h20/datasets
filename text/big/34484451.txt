The Ghost Patrol
 
{{infobox film
| name           = The Ghost Patrol
| image          = The Ghost Patrol (1923) - Ad 1.jpg
| image_size     = 190px
| caption        = Ad for film
| director       = Nat Ross
| producer       = Carl Laemmle
| writer         = Raymond L. Schrock (scenario)
| based on       =  
| starring       = Ralph Graves Bessie Love
| music          =
| cinematography = Ben F. Reynolds
| editing        =
| distributor    = Universal Pictures
| released       =   reels (4,228 feet)
| country        = United States Silent (English intertitles)
}}
The Ghost Patrol is a 1923 silent film drama directed by Nat Ross from a short story by Sinclair Lewis, produced and distributed by Universal Pictures. It starred Ralph Graves and Bessie Love and is now considered lost film|lost. 

==Cast==
*Ralph Graves as Terry Rafferty
*Bessie Love as Effie Kugler George Nichols as Donald Patrick Dorgan
*George B. Williams as Rudolph Kugler
*Max Davidson as Raspushkin
*Wade Boteler as Michael McManus
*Melbourne MacDowell as Commissioner Manning

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 

 