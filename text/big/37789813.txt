Dino Time
{{Infobox film
| name           = Dino Time
| image          = Dino-time.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Yoon-suk Choi John Kafka
| producer       = Robert Abramoff Joonbum Heo David Lovegren Jae Y. Moh Jae Woo Park Sharath Sury
| screenplay     = James Greco Zachary Rosenblatt Adam Beechen Jae Woo Park
| story          = Yoon-suk Choi John Kafka
| starring       = Pamela Adlon Tara Strong Jane Lynch Yuri Lowenthal Fred Tatasciore Melanie Griffith Rob Schneider
| music          = Stephen Barton Loren Gold
| cinematography = Yoon-suk Choi John Kafka
| editing        = David B. Baron Billy Jones
| studio         = CJ Entertainment Myriad Pictures Toiion Clarius Entertainment
| distributor    = Clarius Entertainment CJ Entertainment Distribution Company Koch Media Eagle Films Signature Entertainment Tanweer Films
| released       =  
| runtime        = 88 minutes
| country        = United States South Korea
| language       = English
| budget         = 
| gross          = 
}} 2012 3D 3D South Korean-American Computer-animated film|computer-animated family fantasy comedy adventure film produced by CJ Entertainment, Robert Abramoff, Joonbum Heo, David Lovegren, Jae Y. Moh and Jae Woo Park, distributed by Clarius Entertainment, written by James Greco, Zachary Rosenblatt, Adam Beechen and Jae Woo Park and it stars Pamela Adlon, Tara Strong, Jane Lynch, Yuri Lowenthal, Fred Tatasciore, Melanie Griffith, Rob Schneider, Stephen Baldwin, William Baldwin, Nolan North, Tom Kenny and John DiMaggio. It is directed by Yoon-Suk Choi and John Kafka, and was released on November 30, 2012 in South Korea.

==Plot==
Ernie Fitzpatrick (Pamela Adlon) is a daredevil kid who lives in Terra Dino with his best friend Max Santiago (Yuri Lowenthal) and his trouble-making sister Julia (Tara Strong) who likes to bust him. Ernie and Julia live with their mother Sue (Jane Lynch) who has been chosen as the mother of the year, but she is overprotective of them and often makes them leave notes. Ernie is told to go to the store after school to keep an eye on it, but he disobeys orders and goes with Max to the Terra Dino Museum to sneak into a forbidden part of the area still under construction to see the bones of the ferocious Sarcosuchus. But Julia enters as well and uses a quarter from Ernie to set off the alarm. Julia is able to escape but Ernie and Max get caught by the guards, and Sue grounds Ernie for three weeks. When Ernie finds out Julia did it, he gets sick and tired of how he never gets his own way, but he decides to get his revenge by sneaking out again, but Julia discovers this and follows.

Maxs punishment is helping out in the garage for a month with his dad, Dr. Santiago (Fred Tatasciore), who is an inventor working on a time machine, which has not been working for four years. While Ernie and Max admire it, Julia reveals her appearance and prepares to leave, but Ernie tries to stop her, which results in him spilling soda on the control panel of the time machine, which activates it and sends the kids back in time 65 million years ago, to the time of the dinosaurs. When they emerge, they find a friendly Tyrannosaurus Rex named Tyra (Melanie Griffith) who takes them in as her own. She also runs the orphanage for the dinosaurs without parents, and one of them (Rob Schneider) quickly befriends the kids, but the dinosaurs do not think that the kids can protect them from the evil Sarco Brothers, Surly (Stephen Baldwin) and Sarco (William Baldwin) who are Sarcosuchuses in the Lower Valleys who plan to kill Tyra and take over the Upper Valley, but the Sarco Brothers three henchmen, Morris (Nolan North), Borace (Tom Kenny) and Horace (John DiMaggio) find out about Tyras "newborn" babies and report back to the Sarco Brothers. But, because they mistake the time machine for a real egg, they have Borace and his gang go to steal it so they can lure Tyra to come into their lair, thinking she will be looking for it. Sue and Dr. Santiago discover their kids disappearances and find out that Tyras real egg switched places with the time machine.

Ernie, wanting to explore the land before they can leave, hides the power key of the time machine and tells a lie that its missing, so the kids sneak away from Tyra early in the morning to "look" for it, but they are accidentally sent into a river by the same dinosaur they befriended. He is able to help the kids dodge boulders during a surfing race, but the force of the water causes the power key to jump out of Ernies pocket. The kids find out that the dinosaur can talk by repeating what they say, so Julia names the dinosaur Dodger because of how he dodged the boulders, and Tyra takes them back to the nest shortly after. Borace and his gang find the power key, and Horace swallows it. The kids and Dodger decide to have some fun while Ernie is still busy looking for the power key, and at the same time, Sue and Dr. Santiago start building their own time machine out of Sues car.

The next day, Tyra and the others find tracks from Borace and his gang, but they trick them by hiding behind a bush when Tyra starts following the tracks backwards, and they steal the time machine. When they get back, Ernie finds the power key and reveals he lied, angering Julia and Max when they find out the time machine is gone. But Ernie gets the idea to make a landmark that are actually instructions to powering the time machine. Hell write it on a place in Terra Dino so itll be shown in the present time, and when their parents see it, theyll discover spilling soda on the controls powers the time machine. They decide to do it on Mystery Rock, the center of Terra Dino. But before Ernie and Dodger can do it, Julia is captured by Borace and his gang to lure Tyra to the Lower Valleys due to how the time machine didnt lure her to their lair. Max hitches a ride by riding a Gallimimus to the Lower Valleys, and Ernie is able to get inside the caves by avoiding a fleet of Pterodactylus. Tyra confronts the Sarco Brothers and battles them by knocking them unconscious, and she, Max, and Julia try to escape before they wake up. Ernie ends up being chased by Borace and his gang and ends up running into a dead end. Before they can eat him, Dodger is able to scare them, and Borace and his gang fall into a tar pit.

Meanwhile, the dinosaur egg from the past hatches in Dr. Santiagos house and it escapes and starts causing mayhem. Sue and Dr. Santiago chase it all over Terra Dino and lure it with hamburgers from the Burgersaurus restaurant. Their adventures make Sue realize how overprotective she was of her kids and how she was wrong to not trust Ernie, but they still fail to activate the time machine. Sue figures out they have to spill soda on it after she sees the landmark and spilling it by accident. Ernie and the others reunite, but they end up getting lost and run into the Sarco Brothers again. Ernie manages to kill Surly by throwing his rocket-powered skateboard into his mouth and activating it after Julia blinds him, with the ignition from the skateboard blowing Surly into the lava. Sarco goes for another attack but is stopped by Tyra, who is badly weakened. When Sue and Dr. Santiago arrive in the Lower Valleys, Tyra regains her strength and kills Sarco by dumping him in a tar pit. Sue thinks Tyra is an enemy but Ernie and others tell her the truth. Before they leave, Dr. Santiago returns her real baby. They depart from the Upper Valleys, but as they head back home, Ernie tells Sue that he did not write the landmark because he went to save Julia. Dodger is revealed to be the one who did it, and he hitches a ride back to the present with his allies.

==Cast==
* Pamela Adlon as Ernie Fitzpatrick
* Tara Strong as Julia "Jules" Fitzpatrick
* Jane Lynch as Sue Fitzpatrick
* Yuri Lowenthal as Max Santiago
* Fred Tatasciore as Dr. Santiago
* Melanie Griffith as Tyra
* Rob Schneider as Dodger
* Stephen Baldwin as Surly
* William Baldwin as Sarco
* Nolan North as Morris
* Tom Kenny as Borace
* John DiMaggio as Horace
===Other Cast===
* Roger Craig Smith
* Dee Bradley Baker
* Kari Wahlgren
* Dawnn Lewis
* Jessica DiCicco
* Ben Diskin
* Mary Elizabeth McGlynn
* Melanie Abramoff
* Jamie Simone
* Megan Lovegren Grey Griffin

==Storyline==
Three curious kids accidentally trip an egg-shaped time machine into operation and find themselves back 65 million years in the middle of a nest of dinosaur eggs. The first thing they see is a giant T-Rex staring down at them in happy wonder. Theyre not food, theyre family! Now the kids have just until the real eggs hatch to find their way back to the present, facing other prehistoric monsters and dangers along the way. The story of a daredevil kid named Ernie, his little sister Julia, and his best friend Max, who, while horsing around in Maxs inventor fathers workshop, accidentally trip a time-machine into operation and find themselves transported back in time 65 million years, where theyre adopted by doting dinosaur mom Tyra (Melanie Griffith) and a rambunctious dinosaur "brother" named Dodger (Rob Schneider). The three kids explore the excitement of the prehistoric world - trying to steer clear of Tyras evil dinosaur rivals (William Baldwin and Stephen Baldwin) - while, back in the present day, Maxs dad and Ernie and Julias overprotective mom (Jane Lynch) plot their rescue. Three kids who travel back in time to 65 million years ago, where they are taken in by a dinosaur.

==Crew==
Aziz Acar, Deb Adair, Gregg Barbanell, Mark Ettel, Suzanne Goldish, James Lafferty, Mark Lanza, Mary Elizabeth McGlynn, Stephen P. Robinson, Lucy Sustar, and Joseph Tsai are the makers for the film in Sound Department, Dominique Monfery is the storyboard artist for the film, Mark Griffith is the digital intermediate colorist for the film, Hanung Lee, Kunwoo Lee and Dominique Monfery are the animators for the film, Margaret Whitman for the film is special thanks and Lindsay Fellows, Isobel Griffiths, Jamie Luker and Malcolm Luker for the film are Music Department.

==Release== MPAA for "some scary action and mild rude humor".  It was released in Australia on August 3, 2013. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 