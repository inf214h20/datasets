Phantom (2013 film)
{{Infobox film
| name           = Phantom
| image          = Phantom Ed Harris.jpg
| caption        = Film poster. Todd Robinson  John Watson Julian Adams Pen Densham
| writer         = Todd Robinson
| starring       = Ed Harris David Duchovny William Fichtner Lance Henriksen Johnathon Schaech Julian Adams
| music          = Jeff Rona
| cinematography = Byron Werner
| editing        = Dan Lebental Terel Gibson
| studio         = RCR Media Group Trilogy Entertainment Solar Filmworks
| distributor    = RCR Distribution
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $18,000,000
| gross          = $1,177,589   
}}
 Todd Robinson wrote and directed the film. It stars Ed Harris, David Duchovny and William Fichtner.

The film tells the story of a Soviet Navy submarine captain attempting to prevent a war. It is loosely based on the real-life events involving the K-129 crisis of 1968.

==Plot==
Demi, a veteran Soviet Navy captain finishing up a career that failed to live up to the legacy of his legendary father, is given an assignment by Markov to lead a top secret mission and given the command of his old ship. Due to mistakes he made during his service, involving the deaths of many of his past crew in a fire, this will be his last assignment and his only opportunity to command any ship at all. Demi interrupts a party involving his crew, including Alex Kozlov, his up-and-coming executive officer headed for great success. However, as Demi leaves his home to lead Alex and the crew of a Soviet submarine given a shadowy mission, the presence of KGB agent Bruni and ominous portends begin to alter the objectives of the mission, concerning Demi and Kozlov. Furthermore, Markov commits suicide as the submarine departs.

Demi and Kozlov are wary of Bruni and his men and soon discover his mission: to use a device known as the Phantom to alter their ships acoustic signature to make them appear as civilian merchants, or a number of other ships. After a demonstration, Demi questions Bruni about the device and the mission objectives and not long after, Brunis men hold Demi at gunpoint. Bruni informs Demi that his plans are to start a war between the United States and China by disguising the ship as a Chinese vessel and launching a nuclear missile at the American fleet. Those loyal to Demi are taken with him and locked away in the ship, where Demi explains his past failures and explains how him smacking his head on the ship wall gave him seizures and brain trauma.

Demi, Kozlov and the loyal crew plot and execute a plan that eventually takes back the ship and alerts another Russian sub that they are in distress. Two other members of the crew, Tyrtov and Sasha, attempt to disable the nuclear missile. Eventually, fighting ensues, causing the deaths of many on Demis crew and of Brunis men. The sub is soon badly damaged by Russian torpedoes and sinks to the ocean floor. The crew begin breathing in poisonous fumes as Alex is sent on a rescue attempt for the remaining crew. Bruni explains to Demi that he was on his crew when the fire took place and that he was the man ordered to trap the men to their deaths. 

Eventually, the sub is recovered and the remaining crew, including Demi and his men, are seen standing atop the ship. Demis wife and daughter praise his actions while Alex, now a Captain, salutes the sub, paying respects to his former commanding officer. It is then revealed that Demi and his men, watching from afar, are ghosts.  

==Cast==
*Ed Harris as Demi
*David Duchovny as Bruni
*William Fichtner as Alex Kozlov
*Lance Henriksen as Markov
*Johnathon Schaech as Pavlov
*Jason Beghe as Semak
*Sean Patrick Flanery as Tyrtov
*Jason Gray-Stanford as Sasha
*Julian Adams as Bavenod
*Derek Magyar as Garin

==Production==
Filming took place in California in Long Beach; the Naval Training Center, San Diego; the City of San Diego; and Sun Valley, Los Angeles, California, USA. 

==Soundtrack==
The soundtrack to Phantom was released on February 26, 2013.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 66:15 

| title1          = The Early Dawn
| length1         = 3:03
| extra1          = Jeff Rona

| title2          = This Is Not a Drill
| length2         = 1:18
| extra2          = Jeff Rona

| title3          = Can We Be Redeemed For the Things We’ve Done?
| length3         = 1:14
| extra3          = Jeff Rona

| title4          = Time for Farewells
| length4         = 1:53
| extra4          = Jeff Rona

| title5          = We All Go on the Same Way on a Boat
| length5         = 2:51
| extra5          = Jeff Rona

| title6          = Welcome Aboard
| length6         = 1:05
| extra6          = Jeff Rona

| title7          = We Sail at Dawn
| length7         = 2:54
| extra7          = Jeff Rona

| title8          = True Zealots
| length8         = 1:24
| extra8          = Jeff Rona

| title9          = Twenty Ton Screws
| length9         = 2:30
| extra9          = Jeff Rona

| title10         = These Government Drugs Are Shit
| length10        = 2:14
| extra10         = Jeff Rona

| title11         = Like a Thousand Snowflakes
| length11        = 1:10
| extra11         = Jeff Rona

| title12         = Engage the Phantom
| length12        = 0:54
| extra12         = Jeff Rona

| title13         = Only Two Reasons
| length13        = 2:06
| extra13         = Jeff Rona

| title14         = My Father
| length14        = 3:22
| extra14         = Jeff Rona

| title15         = You Should Be Flattered
| length15        = 0:55
| extra15         = Jeff Rona

| title16         = Sending a Signal
| length16        = 2:22
| extra16         = Jeff Rona

| title17         = Go Below
| length17        = 3:27
| extra17         = Jeff Rona

| title18         = They Already Have the Codes
| length18        = 2:20
| extra18         = Jeff Rona

| title19         = If They So Much as Blink
| length19        = 2:39
| extra19         = Jeff Rona

| title20         = Torpedoes in the Water
| length20        = 2:28
| extra20         = Jeff Rona

| title21         = This Is Your Captain
| length21        = 3:04
| extra21         = Jeff Rona

| title22         = Arming the Warhead
| length22        = 3:11
| extra22         = Jeff Rona

| title23         = We’re On the Bottom
| length23        = 4:03
| extra23         = Jeff Rona

| title24         = Give Her a Message
| length24        = 4:32
| extra24         = Jeff Rona

| title25         = To Stay Here With You
| length25        = 1:26
| extra25         = Jeff Rona

| title26         = I Wish He Knew
| length26        = 4:07
| extra26         = Jeff Rona

| title27         = An Ocean Away
| length27        = 3:43
| extra27         = Rachel Fannan (Carmen Rizzo Mix)

}}The score was co-produced by Jeff Rona and Nathan Rightnour.

==Reception==
The film has received negative reviews from critics, as it currently holds a 25% rating on Rotten Tomatoes, based on over 53 reviews, with the consensus: "A cast of solid actors do what they can to elevate the material, but Phantom s script is too clunky and devoid of tension to bear comparison to its thematic predecessors".    gave it a B score declaring it solidly entertaining if uneven "solid acting all around and nice flashbacks that peel off the layer of intrigue for the viewer ... despite a less than grandiose feast of special effects ... offset mostly by skilled acting from Harris, Duchovny and Fichtner." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 