The Whoopee Party
 
{{Infobox Hollywood cartoon
| cartoon name = The Whoopee Party
| series =
| image =
| image size =
| alt =
| caption =
| director = Wilfred Jackson
| producer = Walt Disney
| story artist =
| narrator =
| voice actor = Walt Disney Marcellite Garner Pinto Colvig
| musician = Maude Nugent David Hand
| layout artist =
| background artist =
| studio = Walt Disney Productions
| distributor = Metro-Goldwyn-Mayer
| release date = September 17, 1932
| color process = Black and white
| runtime = 7 minutes
| country = United States
| language = English
| preceded by =
| followed by =
}}

The Whoopee Party is a Mickey Mouse short animated film first released on September 17, 1932.

==Plot==
In this short Mickey Mouse and friends have a party which in Minnie Mouse is playing the piano while Mickey, Goofy (then Dippy Dog), and Horace Horsecollar are preparing some snacks. This short was also featured in the House of Mouse episode "Dennis the Duck".

==External links==
*  

 

 
 
 
 
 
 
 
 


 