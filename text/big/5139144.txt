Torture Garden (film)
{{Infobox film
| name = Torture Garden
| image = Torturegardenposter.jpg
| caption = Theatrical release poster
| director = Freddie Francis
| producer = Max Rosenberg Milton Subotsky
| writer = Robert Bloch based on = short stories by Bloch, "Enoch", "The Man Who Collected Poe", "Terror Over Hollywood", "Mr Steinway"
| starring = Jack Palance Burgess Meredith Beverly Adams Peter Cushing James Bernard
| cinematography = Norman Warwick
| editing =
| studio = Amicus Productions
| distributor = Columbia Pictures
| released =
| runtime =
| country = United Kingdom
| language = English
| budget =
}} horror film Michael Bryant Hammer horror James Bernard and Don Banks.

It is one of producer Milton Subotskys trademark "portmanteau" films, an omnibus of short stories linked by a single narrative.

==Plot==

Five people visit a fairground sideshow run by showman Dr. Diabolo (Meredith). Having shown them a handful of haunted-house-style attractions, he promises them a genuinely scary experience if they will pay extra. Their curiosity gets the better of them, and the small crowd follows him behind a curtain, where they each view their fate through the shears of an effigy of the female deity Atropos (Clytie Jessop).

* In Enoch, a greedy playboy (Bryant) takes advantage of his dying uncle (Denham), and falls under the spell of a man-eating cat.
* In Terror Over Hollywood, a Hollywood starlet (Adams) discovers her co-stars are androids.
* In Mr. Steinway, a possessed grand piano by the name of Euterpe becomes jealous of its owner (Standing)s new lover (Ewing) and takes revenge.
* In The Man Who Collected Poe, a Poe collector (Palance) murders another collector (Cushing) over a collectable he refuses to show him, only to find it is Edgar Allan Poe himself.

In an epilogue, the fifth patron (Ripper) goes berserk and uses the shears of Atropos to kill Dr. Diabolo in front of the others causing them to panic and flee.  It is then shown that he is working for Diablo and the whole thing was faked.  As they congratulate each other for their acting, Palances character also commends their performance, revealing he had not run off like the others.  He shares a brief exchange with Diabolo and lights a cigarette for him, then leaves.  Diabolo puts the shears back into the hand of Atropos and breaks the fourth wall by addressing the audience, revealing himself to actually be the devil as the movie ends.

==Cast==
* Jack Palance as Ronald Wyatt
* Burgess Meredith as Dr. Diabolo
* Beverly Adams as Carla Hayes
* Peter Cushing as Lancelot Canning Michael Bryant as Colin Williams
* Barbara Ewing as Dorothy Endicott
* John Standing as Leo John Phillips as Storm
* Michael Ripper as Gordon Roberts
* Bernard Kay as Dr. Heim
* Maurice Denham as Uncle Roger
* Ursula Howells as Miss Chambers David Bauer as Charles
* Niall MacGinnis as Doctor
==Production==
The film was meant to star Peter Cushing and Christopher Lee but Columbia, who were providing the budget, wanted two American names, leading to Palance and Merediths casting. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 50-55 
== Critical reception ==
AllRovi|Allmovies review of the film was mixed, writing, "Torture Garden lacks the strength and inventiveness to qualify as a top-tier horror anthology but it offers enough spooky thrills to qualify as a Saturday afternoon diversion." 

== References ==
 

==External links==
* 
*  

 
 
 

 
 
 
 
 
 
 
 
 