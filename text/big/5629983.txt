The Hour of the Pig
{{Infobox Film
| name           = The Hour of the Pig
| image          = Theadvocateposter.jpg
| image_size     = 
| caption        = American theatrical release poster
| director       = Leslie Megahey
| producer       = David M. Thompson
| writer         = Leslie Megahey
| starring       = Colin Firth Ian Holm Donald Pleasence Amina Annabi Nicol Williamson Joanna Dunham
| music          = Alexandre Desplat
| cinematography = Denis Lenoir
| released       = 25 September 1993  (France, Dinard Festival of British Cinema)  21 January 1994  (UK)  24 August 1994  (USA) 
| runtime        = 112m 16s  (UK)  BBC CiBy 2000
| country        = United Kingdom France English
| budget         = 
| website        = 
}}
 French film Jim Carter mystery or a black comedy.
 15 certificate, rated R, primarily due to its nudity and sexual content.

==Plot==
The Hour of the Pig is set in  s were used to determine if animals were the perpetrators of  , 1403–1596.
 law in what they believe to be a quiet rural village, Abbeville, in the province of Ponthieu, then part of Burgundy rather than France. Courtois quickly becomes involved in a number of back-logged cases.

For his first case, he defends a farmer who is accused of killing his wifes lover. Courtois gets him acquitted (the farmer mutters, "I should have done him years ago" as he leaves and offers to help Courtois anytime). In his next case, Courtois fails to save Jeannine, a woman accused of witchcraft. He asks for rats to be called as witnesses to testify that she did not bribe them to infect her neighbour; when the rats do not appear the following day as summoned, this charge is struck off. However, Courtois is unfamiliar with the  difference between the Roman law and the Ponthieu customary law and she is sentenced to be hanged anyway. As she is led away she tells Courtois, "There is darkness all about you, you can bring the light. Look to the boy, maître. Look to the boy." At her execution, Jeannine says she will not curse the town but blesses it, saying a fine knight will arrive and deliver them from their lying and evil.

Courtois takes on a case defending a pig that is accused of killing a young Jewish boy. The pig, however, belongs to a band of Moors (alternatively/first thought to be Romani people|Gypsies, being referred to as coming from "Little Egypt") passing through town. Two of the Moors, Mahmoud (Sami Bouajila) and his sister Samira (Annabi), appeal to Courtois to save the pig, as it is their only source of food for the coming winter. Courtois declines. Samira later enters Courtoiss room at night and quietly strips naked, offering her body in exchange for his services, but he refuses this. On the next day he offers her enough money to purchase two pigs, but she does not accept this.

As Courtois delves deeper into the case and becomes more involved with Samira, he discovers that there is more at work than a simple murder. His work is brought to the attention of Seigneur Jehan dAuferre (Williamson), who has his own designs on Courtois. Soon, Courtois finds that he is being used as a pawn in a complicated game of sociopolitical intrigue that extends beyond mere racism and corruption. The Seigneur subtly offers to bribe Courtois, also hinting that his daughter Filette is available in marriage. The Seigneurs son and daughter are eccentric to the point of insanity. The sons main hobby seems to be torturing birds.

Courtoiss relationship with Samira becomes common knowledge. The Seigneur decides to sit in at the court and uses this knowledge to threaten Courtois into letting the pig be executed. Just as the case seems to be over, the Advent festival begins and the case is adjourned.

The prosecutor Pincheon (Pleasence) tells Courtois that he moved from Paris to Ponthieu as Courtois did, in order to shine in a village in a way he could not in Paris. He urges Courtois to go back to Paris and not waste his life among ignorant, superstitious peasants.

The skeleton of another Jewish boy who went missing over a year ago is found while Courtoiss house is being built. Courtois now suspects a human serial killer is at large and the pig has been framed.

On Christmas Day, Samira performs for a gathering of notables at the Seigneurs chateau. She is almost arrested for drawing a knife on the Seigneurs son after he pours wine down her blouse. Courtois boldly leads her away. That night, he rescues a boy from a masked horseman wielding an axe.

Courtois confronts the Seigneur, telling him his son is the killer. The Seigneur does not deny it and reveals that his son has left for England to be treated. At the trial, the pig is acquitted when Valliere, the farmer Courtois saved in his first case at Abbeville, brings in a replica pig which he claims absconded at the time of the killing.

As Courtois leaves, he sees a Knight arriving just as Jeannine had foretold. After he has left, the Knight goes to a room and takes off his armor to reveal that he bears the characteristic buboes of the Black Death.

==U.S. Version== Miramax as The Advocate.

==U.K. Version== PAL speed-up).  Only the shorter (R-rated) version of the film was released to VHS or DVD in North America. The full version has been released on DVD and screened on television in some European countries.

==List of changes==

Besides the titles, there are many differences between The Advocate (U.S) and The Hour Of The Pig (U.K.). The list of changes include:

-different voice takes for some scenes.
-different music or no music for some scenes.
-Amina Annabis voice is dubbed in the U.S. version.

==References and notes==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 