Semper Fi: Always Faithful
{{Infobox film
| name = Semper Fi: Always Faithful
| image = Semper Fi Always Faithful.jpg
| caption = Promotional poster photo of Jerry Ensminger graduation day from Drill Instructor School, 19 December 1975 Parris Island, S. C.

| alt =
| director = Tony Hardmon and Rachel Libert
| producer = Jedd Wider, and Todd Wider
| starring = Jerry Ensminger, Master Sergeant USMC (retired), Tom Townsend, Major USMC (retired), Mike Partain, Denita McCall (deceased)
| writer =
| music = 
| editing = Purcell Carson
| studio = Wider Film Projects
| distributor = 
| released =  
| runtime = 76 minutes
| country = United States
| language = English
| budget =
}} short list The Ridenhour Documentary Film Prize 2012.  The Society of Professional Journalists presented it with its Sigma Delta Chi Award for Best Television Documentary (Network).

== Plot ==
  and force them to live up to their motto to the thousands of Marines and their families exposed to toxic chemicals. His fight reveals a grave injustice at North Carolinas Camp Lejeune and a looming environmental crisis at military sites across the country.

== Accolades ==
Semper Fi: Always Faithful has won/nominated the following awards: 
* Nomination for News & Documentary Emmy Awards (2013)
* Won Best Editing for the Tribeca Film Festival (2011)
* Won Best Documentary for United Nations Association Film Festival (2012)
* Woodstock Film Festival (2011)
** Won Audience Award
** Won Jury Prize
** Runners Up for Best Editting

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 