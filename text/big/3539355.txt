The Empress Dowager
 
 
{{Infobox film name = The Empress Dowager image =  alt =  caption =  traditional = 傾國傾城 simplified = 倾国倾城 pinyin = Qīng Guó Qīng Chéng jyutping = King1 Gwok3 King1 Sing4}} director = Li Han-hsiang producer = Runme Shaw writer = Li Han-hsiang starring = Lisa Lu Ti Lung music = Frankie Chan cinematography = Lam Chiu editing = Chiang Hsing-lung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 165 minutes country = Hong Kong language = Mandarin budget =  gross = 
}}
The Empress Dowager is a 1975 Hong Kong historical film directed by Li Han-hsiang and produced by the Shaw Brothers Studio, starring Lisa Lu as Empress Dowager Cixi.

==Plot== Empress Dowager Ching Dynasty Emperor Kuang-hsu Empress Chin Concubine Chen and a young eunuch who wholeheartedly supports him.

==Cast== Empress Dowager Tzu-hsi Emperor Kuang-hsu Concubine Chen Li Lien-ying
*David Chiang as Eunuch Ko Lien-tsai Empress Chin Feng
*Tanny Tien as Li Chieh
*Chen Ping as Consort Jin
*Shum Lo as Eunuch Wang Shang Weng Tung-ho Li Hung-chang Prince Kung
*Lee Pang-fei as Prince Shun
*Cheng Miu as Official Yung Lu
*Yeung Chi-hing as Official Hsu Tung
*Wang Hsieh as Official Kang Li
*Chiang Nan as Official Tai Yi
*Tin Ching as Cheng Yueh Lou
*Ou-yang Sha-fei as concubine
*Law Bing-ching as Kiang governors daughter
*Chan Si-gai as Kiang governors daughter
*Wong San as court official
*Liu Wai as court official
*Wang Han-chen as court official
*Kong Yeung as court official
*Wong Ching-ho as eunuch in charge of conjugal record
*Tung Wai as eunuch
*Chan Mei-hua as lady in waiting
*Lau Nga-ying as lady in waiting
*Ofelia Yau as lady in waiting
*Yuen Man-tzu as lady in waiting
*Ling Hon as court official
*Gam Yam as court official
*Kwan Yan as court official
*Fuk Yan-cheng as court official
*Lui Hung as lady in waiting
*Yeung Kei as lady in waiting
*On Ching-man
*Lau Wai-ling
*So Chan
*Dana
*Woo Hei-hung
*Lee Hing-kwan
*Got Heung-ting

==Awards==
*1975 Golden Horse Awards
**Best Actress: Lisa Lu 
**Best Supporting Actress: Ivy Ling Po

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 