Will to Live
{{Infobox film
| name           = Will to Live
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Kaneto Shindo
| producer       = Jiro Shindo
| writer         = Kaneto Shindo
| screenplay     = 
| story          = 
| based on       =  
| starring       = Rentarō Mikuni, Shinobu Otake
| music          = 
| cinematography = 
| editing        = 
| studio         = Kindai Eiga Kyokai
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} The Ballad FIPRESCI Prize at the 21st Moscow International Film Festival.   

==Plot==
Yasukichi (Rentarō Mikuni) visits "Ubasuteyama", a mountain where, in the past, old people were left to die. He is a regular at a bar. While at the bar he defecates in his clothes. The bar owner (Naoko Otani) literally kicks him out of the bar. Lying on the pavement, he is run over by a man on a bicycle, who turns out to be a doctor. His daughter, Tokuko (Shinobu Otake), is awakened by a phone call from the hospital asking her to collect Yasukichi. She tries to decline, saying she has bipolar disorder and cannot look after her father, but eventually is forced to take him in. Yasukichi has stolen a book from the hospital about Obasuteyama and begins reading it to Tokuko. The story of Ubasuteyama is told on the screen, in a black and white film.

Tokukos sister comes to visit. Yasukichi again loses control of his bowels before she arrives. Yasukichi visits a park and talks to the doctor again. He visits the bar again and begins arguing with the regulars about treatment of the elderly. He then loses control of his bowels and collapses, again, and returns to the hospital. At the hospital, his son visits and announces his marriage.

After some persuasion, Yasukichi accepts that he must go into the old peoples home. He goes into the old peoples home but then Tokuko decides that she is lonely without him and brings him back out again.

==Cast==
{| class=wikitable
! Role
! Actor
|- Yasukichi Yamamoto
|Rentarō Mikuni
|- Tokuko Yamamoto Shinobu Otake
|-
| Hideko Yoshida
|-
| Akira Emoto
|-
| Masayuki Shionoya
|- Bar owner Naoko Otani
|-
| Yoshiko Miyazaki
|-
| Masahiko Tsugawa
|-
| Hideo Kanze
|}

==References==
 

==External links==
* 
*  at the Japanese Movie Database (in Japanese)

 
 

 
 
 
 

 