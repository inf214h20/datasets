Short Cuts
 
{{Infobox film
| name           = Short Cuts
| image          = Shortcutsfilm.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Altman
| producer       = Cary Brokaw
| writer         = Robert Altman Frank Barhydt
| based on       =   see below
| music          = Mark Isham
| cinematography = Walt Lloyd
| editing        = Geraldine Peroni Suzy Elmiger
| studio         = Spelling Pictures International Avenue Pictures
| distributor    = Fine Line Features  
| released       =  
| runtime        = 188 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $6,110,979 
}} short stories and a poem by Raymond Carver. Substituting a Los Angeles setting for the Pacific Northwest backdrop of Carvers stories, the film traces the actions of 22 principal characters, both in parallel and at occasional loose points of connection. The role of chance and luck is central to the film, and many of the stories concern death and infidelity.

The film features an ensemble cast including Matthew Modine, Julianne Moore, Fred Ward, Anne Archer, Jennifer Jason Leigh, Robert Downey, Jr., Chris Penn, Jack Lemmon, Frances McDormand, Annie Ross, Lori Singer, Andie MacDowell, Buck Henry, Lily Tomlin, and musicians Huey Lewis, Lyle Lovett and Tom Waits.

==Plot==
A fleet of helicopters sprays for medfly|medflies, revealing all the characters along the path of their flight. Dr. Ralph Wyman and his wife, Marian, meet another couple, Stuart and Claire Kane, at Zoe Trainers cello concert and make a spontaneous Sunday dinner date. Marians sister Sherri is married to philandering cop Gene, who makes up unbelievable stories to hide his affair with Betty Weathers. Betty is in the process of divorcing one of the helicopter pilots, Stormy. Waitress Doreen Piggot is married to an alcoholic limo driver named Earl. TV commentator Howard Finnigan lives with his wife Anne and their young family next door to Zoe and her mother, cabaret singer Tess Trainer. Their pool cleaner is Jerry Kaiser, whose wife, Lois, works from home as a phone sex operator, tending to the children while she talks off strange men. Jerry and Lois are friends with Doreens daughter, Honey and her husband Bill, who works as a makeup artist.

The day before Casey Finnigans 8th birthday, Doreen hits him with her car. Casey appears fine, and refuses Doreens offer of a ride home, because she is a stranger. His mother comes home from ordering his birthday cake to find Casey slumped lethargically on the couch. Howard convinces her to take Casey to the hospital, where he remains unconscious. The baker calls the next day to inform Ann that the cake is ready, but Howard, wanting to keep the line free, briskly ends the conversation. The baker immediately calls back, incensed at being hung up on. While the Finnigans maintain their vigil, the baker continues to call and harass the couple. Howards estranged father turns up at the hospital and recalls that Caseys hospitalization is reminiscent of the day that Howard had an accident as a boy. When Howards mother went to her sisters house, she found her in bed with her husband, whom she had seduced. That lead to the estrangement between father and son.

Stuart and his two friends, Gordon and Vern, harass Doreen at the diner before they head out on their three day fishing trip. On the first day, they find a young womans body submerged near some rocks. After some debate, they decide to tie her to the rocks, continue fishing, and report the body when they are done. When he comes home, he eventually admits what they had done to Claire, and she is disgusted that they could fish for days with the womans body nearby. The body is identified as a 23-year-old woman, and Claire goes to her funeral out of a sense of guilt.

Stormy visits Bettys house, ostensibly to pick up his mothers clock, but instead, he spends the day destroying her belongings. Bill and Honey entertain themselves in the apartment that they are watching while its owners are on vacation by taking some pictures of Honey where Bill has made her up to look like she has been brutally beaten. Gene abandons the family dog on a strange street because he cannot endure its barking, but after several days of his distraught childrens inquiries, he returns to the neighborhood and retrieves the dog. The Wymans get into a massive argument just before their dinner party with the Kanes. Marian admits to sleeping with another man. Both couples alleviate their stress by drinking copiously, and the party lasts all night long.

One day, Caseys eyes begin to flutter. Anns excitement grows, but just as he appears to be fully waking, he suddenly dies. Seeing this and being overwhelmed, Howards father, Paul Finnigan, leaves the hospital while the distraught couple returns home and informs Zoe of Caseys death. The next day, they go to the bakery to shame the baker over his abuse of them. When he learns why they never picked up the cake, he asks them to stay and gives them baked goods. Zoe, worn to the breaking point by her mothers alcoholism and her isolation, commits suicide by starting a car engine inside her garage, playing the cello as she asphyxiates. Later that day, her mother discovers that Zoes dead and is bewildered.

When Honey picks up the pictures from the developer, they are mixed up with Gordons. Gordon is horrified to see the pictures of Honey beaten so badly, while she is horrified of the pictures he took of the submerged body on his fishing trip. They each walk away from each other memorizing the other persons license plates. Honey and Bill are on their way to a picnic with Jerry and Lois. In the park, Jerry and Bill try to hook up with two young women they encountered earlier, and Bill quickly makes an excuse to divvy up into couples. As he and one of the girls walk away from Jerry and the other, they hear her scream. They turn around to see Jerry hitting her in the head with a rock, just as a major earthquake strikes.

In the aftermath, Jerrys murder of the girl is attributed to a falling rock during the earthquake, and the implication is that he may also be responsible for the submerged body in the river.

==Cast==
* Matthew Modine as Dr. Ralph Wyman
* Julianne Moore as Marian Wyman
* Fred Ward as Stuart Kane
* Anne Archer as Claire Kane
* Buck Henry as Gordon Johnson
* Huey Lewis as Vern Miller
* Lily Tomlin as Doreen Piggot
* Tom Waits as Earl Piggot
* Zane Cassidy as Casey Finnigan
* Bruce Davison as Howard Finnigan
* Andie MacDowell as Ann Finnigan
* Lyle Lovett as Andy Bitkower
* Jack Lemmon as Paul Finnigan
* Lili Taylor as Honey Piggot Bush
* Robert Downey, Jr. as Bill Bush
* Jennifer Jason Leigh as Lois Kaiser
* Chris Penn as Jerry Kaiser
* Tim Robbins as Gene Shepard
* Madeleine Stowe as Sherri Shepard
* Lori Singer as Zoe Trainer 
* Annie Ross as Tess Trainer
* Peter Gallagher as Stormy Weathers
* Frances McDormand as Betty Weathers
* Jarrett Lennon as Chad Weathers
* Dirk Blocker as Diner customer

==Production== Los Angeles, California. Principal photography began on July 26, 1992, and ended on October 1, 1992. 

==Book==
A book was released to accompany the film, compiling the nine short stories and one poem that inspired it. Altman wrote an introduction to this collection, which featured insights into the making of the film and his own thoughts about Carvers stories.
# "Neighbors (short story)|Neighbors"
# "Theyre Not Your Husband"
# "Vitamins (short story)|Vitamins"
# "Will You Please Be Quiet, Please?"
# "So Much Water So Close to Home"
# "A Small, Good Thing"
# "Jerry and Molly and Sam"
# "Collectors"
# "Tell the Women Were Going"
# "Lemonade" (poem)

==Release==
The film was distributed by Fine Line Features, and released in the United States on October 3, 1993, to strong critical acclaim. A special DVD edition was released by the Criterion Collection in 2004, containing two discs, the collection of Carvers short stories and an essay booklet on the film.

==Awards and nominations== Academy Award Best Director Golden Globe Best Screenplay with Barhydt (lost to Steven Zaillian for Schindlers List). The cast won a Special Golden Globe Award for their ensemble acting. The film also won the prestigious Golden Lion and the Volpi Cup for Best Ensemble Cast at the Venice Film Festival.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 