Spirited Away
 
 
{{Infobox film
| name = Spirited Away
| film name = {{Film name| kanji = 千と千尋の神隠し
| romaji = Sen to Chihiro no Kamikakushi}}
| image = Spirited_Away_poster.JPG
| alt = A young girl dressed in work clothes is standing in front of an image containing a group of pigs and the city behind her. Text below reveal the title and film credits, with the tagline to the girls right.
| caption = Japanese release poster
| director = Hayao Miyazaki Toshio Suzuki
| writer = Hayao Miyazaki
| starring = {{Plainlist|
*Rumi Hiiragi
*Miyu Irino
*Mari Natsuki
*Takeshi Naito
*Yasuko Sawaguchi
*Tsunehiko Kamijō
*Takehiko Ono
*Bunta Sugawara}}
| music = Joe Hisaishi
| cinematography = Atsushi Okui
| editing = Takeshi Seyama
| studio = {{Plainlist|
* Tokuma Shoten
* Studio Ghibli
* Nippon Television Network
* Dentsu Buena Vista Home Entertainment
* Tohokushinsha Film Mitsubishi Commercial Affairs}}
| distributor = Toho
| released =    
| runtime = 124 minutes  
| country = Japan
| language = Japanese
| budget = {{Plainlist| 
* Japanese yen|¥1.9 billion
* (United States dollar|US$19 million)  }}
| gross = $330 million  
|}} bathhouse to find a way to free herself and her parents and return to the human world.

Miyazaki wrote the script after he decided the film would be based on his friend, associate producer Seiji Okudas ten-year-old daughter, who came to visit his house each summer.  At the time, Miyazaki was developing two personal projects, but they were rejected. With a budget of United States dollar|US$19 million, production of Spirited Away began in 2000. During production, Miyazaki realized the film would be over three hours long and decided to cut out several parts of the story. Pixar director John Lasseter, a fan of Miyazaki, was approached by Walt Disney Pictures to supervise an English-language translation for the films North American release. Lasseter hired Kirk Wise as director and Donald W. Ernst as producer of the adaptation. Screenwriters Cindy Davis Hewitt and Donald H. Hewitt wrote the English language|English-language dialogue, which they wrote to match the characters original Japanese language|Japanese-language lip movements.   
 greatest animated Bloody Sunday) and is among the top ten in the BFI list of the 50 films you should see by the age of 14.

==Plot==
  bathhouse and meets a young boy named Haku who warns her to return across the river before sunset. However, Chihiro discovers too late that her parents have turned into actual pigs and she is unable to cross the flooded river, becoming trapped in the spirit world.
 tipping extensively. As the workers swarm him hoping to be tipped, he swallows yet another two greedy workers.
 seal from her, and warns Sen that it carries a deadly curse. After Haku dives to the boiler room with Sen and Boh on his back, she feeds him part of the dumpling, causing him to vomit both the seal and a black slug, which Sen crushes with her foot.

With Haku unconscious, Sen resolves to return the seal and apologize for Haku. Before she leaves the bathhouse, Sen confronts No-Face, who is now massive and feeds him the rest of the dumpling. No-Face chases Sen out of the bathhouse, steadily vomiting out those he has eaten. Sen, No-Face and Boh travel to see Zeniba. Enraged at the damage caused by No-Face, Yubaba blames Sen for inviting him in and orders that her parents be slaughtered. After Haku reveals that Boh is missing, he promises to retrieve Boh in exchange for Yubaba freeing Sen and her parents.
 spinner for Zeniba and accepts her proposal to stay as a worker. On the way back, Sen recalls a memory from her youth in which she had fallen into the Kohaku River but was washed safely ashore. After correctly guessing that Haku is the spirit of the Kohaku River (and thus revealing his real name), Haku is completely freed from Yubabas control. When they arrive at the bathhouse, Yubaba tells Sen that in order to break the curse on her parents, she must identify them from among a group of pigs. After Sen correctly states that none of the pigs are her parents, she is given back her real name Chihiro. Haku takes her to the now dry riverbed and vows to meet her again. Chihiro crosses the river and reunites with her restored parents, who do not remember what happened. They walk back to their car and drive away.

==Voice cast==
{| class="wikitable"
|-
! Character name
! Japanese voice actor
! English voice actor
|-
|  /  || Rumi Hiiragi  || Daveigh Chase
|-
|  /  || Miyu Irino || Jason Marsden
|-
|   ||rowspan="2"| Mari Natsuki ||rowspan="2"| Suzanne Pleshette
|-
|  
|-
|   || Bunta Sugawara || David Ogden Stiers
|-
|   ||   || Susan Egan
|-
|   ||   || Paul Eiding
|-
|  /Assistant Manager ||   || John Ratzenberger
|-
|   ||   ||rowspan="2"| Bob Bergen
|-
|   ||  
|-
|  /Foreman || Yo Oizumi|Yō Ōizumi || Rodger Bumpass
|-
|   (Baby) || Ryunosuke Kamiki|Ryūnosuke Kamiki || Tara Strong
|-
|  , Chihiros father ||   || Michael Chiklis
|-
|  , Chihiros mother || Yasuko Sawaguchi || Lauren Holly
|-
|   ||   ||  
|-
|   ||   || Jack Angel
|}

==Production==
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#white; color:black; width:30em; max-width: 40%;" cellspacing="5" I created a heroine who is an ordinary girl, someone with whom the audience can sympathize. Its not a story in which the characters grow up, but a story in which they draw on something already inside them, brought out by the particular circumstances. I want my young friends to live like that, and I think they, too, have such a wish.
|-
|style="text-align: left;" |— Hayao Miyazaki 
|}
Every summer, Hayao Miyazaki spent his vacation at a mountain cabin with his family and five girls who were friends of the family. The idea for Spirited Away came about when he wanted to make a film for these friends. Miyazaki had previously directed films such as My Neighbor Totoro and Kikis Delivery Service, which were for small children and teenagers, but he had not created a film for ten-year-old girls. For inspiration, he read shōjo manga magazines like  Nakayoshi and Ribon the girls had left at the cabin, but felt they only offered subjects on "crushes" and romance. When looking at his young friends, Miyazaki felt this was not what they "held dear in their hearts" and decided to produce the film about a girl heroine whom they could look up to instead.  . Nausicaa.net (July 11, 2001). 

  used shōjo manga magazines for inspiration to direct Spirited Away.]]
Miyazaki wanted to produce a new film for years, he previously wrote two project proposals, but they were rejected. The first one was based on the Japanese book Kirino Mukouno Fushigina Machi, and the second one was about a teenage heroine. Miyazakis third proposal, which ended up becoming Sen and Chihiro Spirited Away, was more successful. The three stories revolved around a bathhouse that was based on a bathhouse in Miyazakis hometown. Miyazaki thought the bathhouse was a mysterious place, and there was a small door next to one of the bathtubs in the bathhouse. Miyazaki was always curious to what was behind it, and he made up several stories about it; one of which was the inspiration for the bathhouse in Spirited Away. 

 , a town in Taiwan, is believed to have served as an inspiration for the spirit worlds design. ]]
Production of Spirited Away commenced in 2000 on a budget of Japanese yen|¥1.9 billion (US$19 million).  Disney invested 10% of the cost for the right of first refusal for American distribution.    As with Princess Mononoke, Miyazaki and the Studio Ghibli staff experimented with computer animation. With the use of more computers and programs such as Autodesk Softimage|Softimage, the staff learned the software, but kept the technology at a level to enhance the story, not to "steal the show". Each character was mostly hand-drawn, with Miyazaki working alongside his animators to see they were getting it just right.  . Jimhillmedia.com.  The biggest difficulty in making the film was to reduce its length. When production started, Miyazaki realized it would be more than three hours long if he made it according to his plot. He had to delete many scenes from the story, and tried to reduce the "eye-candy" in the film because he wanted it to be simple. Miyazaki did not want to make the hero a "pretty girl." At the beginning, he was frustrated at how she looked "dull" and thought, "She isnt cute. Isnt there something we can do?" As the film neared the end, however, he was relieved to feel "she will be a charming woman." 

  residence in the   located in Yamagata Prefecture, famous for its exquisite architecture and ornamental features.  The old gold town of Jiufen in Taiwan also served as an inspirational model for Miyazakis film.  The Dōgo Onsen is also often said to be a key inspiration for the Spirited Away onsen/bathhouse. 

===Music===
The film score of Spirited Away was composed and conducted by Miyazakis regular collaborator Joe Hisaishi, and performed by the New Japan Philharmonic.    The soundtrack received awards at the 56th Mainichi Film Competition Award for Best Music, the Tokyo International Anime Fair 2001 Best Music Award in the Theater Movie category, and the 17th Japan Gold Disk Award for Animation Album of the Year.    Later, Hisaishi added lyrics to "One Summers Day" and named the new version   which was performed by Ayaka Hirahara. 

The closing song,   was written and performed by Youmi Kimura, a composer and lyre-player from Osaka.  The lyrics were written by Kimuras friend Wakako Kaku. The song was intended to be used for  , a different Miyazaki film which was never released.    In the special features of Japanese DVD, Hayao Miyazaki explains how the song in fact inspired him to create Spirited Away.  The song itself would be recognized as Gold at the 43rd Japan Record Awards. 
 image album, titled  , that contains 10 tracks. 

===English adaptation===
Both   co-director   dialogue, which they wrote to match the characters original Japanese language|Japanese-language lip movements. 

The cast of the film consisted of Daveigh Chase, Susan Egan, David Ogden Stiers and John Ratzenberger. Advertising was limited, and Spirited Away was only mentioned in a small scrolling section of their film page on Disneys official website. Disney had sidelined their official website for Spirited Away and it remained hidden.  The promotion of the film was given a worse treatment than Disneys own B-movies by comparison.  Marc Hairston argues this was a justified response to Ghiblis retention of the merchandising rights to the film and characters, which imposed a limitation on Disney that did not validate the marketing costs. 

==Themes== Pleasure Island were transformed into donkeys. Upon gaining employment at the bathhouse, Yubabas seizure of Chihiros true name symbolically kills the child,  who must then assume adulthood. She then undergoes a rite of passage according to the monomyth format; to recover continuity with her past, Chihiro must create a new identity. 
 Japanese culture and fable in the amalgam of the bathhouse.
 Japanese society.

Additional themes are expressed through the No-Face, who reflects the characters which surround him, learning by example and taking the traits of whomever he consumes. This nature results in No-Faces monstrous rampage through the bath house. After Chihiro saves No-Face with the emetic dumpling, he becomes timid once more. At the end of the film, Zeniba decides to take care of No-Face so it can develop without the negative influence of the bathhouse. 

==Release==

===Box office===
Spirited Away was released theatrically in Japan on July 20, 2001 by distributor Toho, grossing ¥30.4 billion to become the List of highest-grossing films in Japan|highest-grossing film in Japanese history, according to the Motion Picture Producers Association of Japan.    It was also the first film to earn $200 million at the worldwide box office before opening in the United States.  The film was dubbed into English by Walt Disney Pictures, under the supervision of Pixars John Lasseter. The dubbed version premiered at the Toronto International Film Festival on September 7, 2002  and was later released in North America on September 20, 2002. Spirited Away had very little marketing, less than Disneys other B-films, with at most, 151 theaters showing the film in 2002.  After the 2003 Oscars, it expanded to as many as 714 theaters. The film grossed US$449,839 in its opening weekend and ultimately grossed around $10 million by September 2003.    In addition to its North American earnings of $10 million and Japanese earnings of $290 million, it grossed a further $29 million from other countries for a worldwide total of about $330 million. Gross
* 
::North American gross: $10,055,859
::Japanese gross: $229,607,878 (March 31, 2002)
::Other territories: $ }}
Japanese gross
* 
::End of 2001: $227 million
* 
::Across 2001 and 2002: $270 million
* 
::As of 2008: $290 million 

===Critical reception===
Spirited Away received widespread critical acclaim  from film critics. The film holds a 97% approval rating on the  , the film achieved a weighted average score of 94 out of 100 based on 37 reviews, signifying "universal acclaim". 

Roger Ebert of the Chicago Sun-Times gave the film a full four stars, praising the film and Miyazakis direction. Ebert also said that Spirited Away was one of "the years best films."  Elvis Mitchell of The New York Times positively reviewed the film and praised the animation sequences. Mitchell also drew a favorable comparison to Lewis Carrolls Through the Looking-Glass and also said that his movies are about "moodiness as mood" and the characters "heightens the   tension."    Derek Elley of Variety (magazine)|Variety said that Spirited Away "can be enjoyed by sprigs and adults alike" and praised the animation and music.    Kenneth Turan of the Los Angeles Times praised the voice acting and said the film is the "product of a fierce and fearless imagination whose creations are unlike anything a person has seen before". Turan also praised Miyazakis direction.  Orlando Sentinels critic Jay Boyar also praised Miyazakis direction and said the film is "the perfect choice for a child who has moved into a new home." 

Rotten Tomatoes ranked Spirited Away as the thirteenth-best animated film on the site.  In 2005, it was ranked as the twelfth-best animated film of all time by IGN.  The film is also ranked No. 9 of the highest-rated movies of all time on Metacritic, being the highest rated traditionally animated film on the site. The film ranked number 10 in Empire (film magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. 

In his book Otaku,   forms and markets quite rapidly won social recognition in Japan", and cites Miyazakis win at the Academy Awards for Spirited Away among his examples. 

===Accolades===
{| class="wikitable"
|-
! Year
! Award
! Category
! Result
! Recipient
|- 2002
| Japan Academy Award
| Best Film
|  
| Spirited Away   
|-
| Best Song
|  
| Spirited Away 
|-
| 52nd Berlin International Film Festival
| Golden Bear
|  
| Spirited Away     
|-
| Cinekid Festival
| Cinekid Film Award
|  
| Spirited Away     
|-
| 21st Hong Kong Film Awards
| Best Asian Film
|  
| Spirited Away 
|- 2003
| 75th Academy Awards Best Animated Feature
|  
| Spirited Away 
|}

===Home media=== Buena Vista Home Entertainment on April 15, 2003. The attention brought by the Oscar win resulted in the film becoming a strong seller.  The bonus features include Japanese trailers, a making-of documentary which originally aired on Nippon Television, interviews with the North American voice actors, a select storyboard-to-scene comparison and The Art of Spirited Away, a documentary narrated by actor Jason Marsden. 

The film was released nationwide in the UK on September 12, 2003.  It was released on DVD in the UK on March 29, 2004.  In 2005, it was re-released by Optimum Releasing.  The film was released on Blu-ray format in Japan and the UK in 2014, and it is expected to be released in North America on June 16, 2015.   

==See also==
*List of films considered the best

==References==
 

==Further reading==
*  

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
*  
*   at the Japanese Movie Database  
*  

 
 
{{Navboxes title = Awards for Spirited Away list =
 
 
 
 
 
 
 
 
 
 
 
 
}}
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 