The Wicked Lady (1983 film)
 
 
{{Infobox film
| name           = The Wicked Lady
| image size     =
| image	=	The Wicked Lady FilmPoster.jpeg
| caption        =
| director       = Michael Winner
| producer       = Yoram Globus Menahem Golan
| writer         = Leslie Arliss Gordon Glennon Magdalen King-Hall Aimée Stuart Michael Winner
| narrator       =
| starring       = Faye Dunaway Alan Bates John Gielgud Denholm Elliott Hugh Millais Tony Banks
| cinematography = Jack Cardiff
| editing        = Michael Winner
| studio         = The Cannon Group
| distributor    = Metro-Goldwyn-Mayer|MGM/UA (U.S.) Guild Home Video (U.K. home video)
| released       = 22 July 1983
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
| budget         = $8 million (est.) Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p43-45 
| gross          = $724,912 
| preceded by    =
| followed by    =
}} 1983 cinema British drama 1945 film of the same name, which was one of the popular series of Gainsborough melodramas.

== Plot ==
Caroline is to be wed to Sir Ralph and invites her sister Barbara to be her bridesmaid. Barbara seduces Ralph, and marries him herself, but, despite her new wealthy situation, she gets bored and turns to highway robbery for thrills.

While on the road she meets a famous highwayman (Jerry Jackson) and they continue as a team, but some people begin suspecting her identity and she risks death if she continues her nefarious activities.

== Cast ==
* Faye Dunaway as Lady Barbara Skelton
* Alan Bates as Jerry Jackson
* John Gielgud as Hogarth
* Denholm Elliott as Sir Ralph Skelton
* Prunella Scales as Lady Kingsclere
* Oliver Tobias as Kit Locksby
* Glynis Barber as Caroline
* Joan Hickson as Aunt Agatha
* Helena McCarthy as Moll Skelton
* Mollie Maureen as Doll Skelton
* Derek Francis as Lord Kingsclere
* Marina Sirtis as Jacksons Girl
* Nicholas Gecks as Ned Cotterell
* Hugh Millais as Uncle Martin
* John Savident as Squire Thornton
* Marc Sinden as Lord Dolman  Mark Burns as King Charles II 

==Production== Mark Burns Equity union minimum fee. Burns told him to make a donation to the Police Memorial Trust, which was run by Winner. Years later when Burns appeared at a magistrates court on a charge of speeding, Winner, appearing as a character witness, told the bench that the actor had given "his entire fee" for a major film to the fund and Burns was subsequently discharged. 
 Worst Actress. 

==Soundtrack== soundtrack for Genesis keyboardist Tony Banks.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 

 