Paris by Night (1988 film)
Paris British thriller David Hare and starring Charlotte Rampling, Michael Gambon and Iain Glen.  A British politician spends some time in Paris, but gets caught up in a murder.

The film was released on DVD by Televista in 2007, but it is considered to be of inferior quality.

==Cast==
* Charlotte Rampling ...  Clara Paige 
* Michael Gambon ...  Gerald Paige 
* Iain Glen ...  Wallace Sharp 
* Robert Hardy ...  Adam Gillvray 
* Jane Asher ...  Pauline 
* Niamh Cusack ...  Jenny Swanton 
* Julian Firth ...  Lawrence 
* Linda Bassett ...  Janet Swanton 
* Robert Flemyng ...  Jack Sidmouth 
* Juliet Harmer ...  Delia
* Alain Fromager ...  Paul Zinyafski
* Christopher Baines ...  Young Organizer 
* Annabel Brooks ...  Girl at Gillvrays Office 
* Edward Clayton ...  Birmingham Chairman 
* Brian Cobby ...  Foreign Secretary 
* Bradley Cole ...  Young Man
* Reg Gadney ...  British Diplomat 
* François Greze ...  Young Hotel Clerk 
* Czeslaw Grocholski ...  Foreign Lecturer
  Peter Whitbread ... English Lecturer http://m.imdb.com/name/nm0924336/filmotype/actor

==References==
 

==External links==
* 

 
 
 
 
 


 
 