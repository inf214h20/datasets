Raamanam
{{Infobox film
| name           = Raamanam
| image          = 
| caption        = 
| director       = M. P. Sukumaran Nair
| producer       = M. P. Sukumaran Nair
| based on       = Smarakasilakal by Punathil Kunhabdulla
| starring       = Jagathy Sreekumar  Indrans Mamukkoya
| music          = K. G. Jayan
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Raamanam ( ,  ) is a 2009 Malayalam film directed and produced by M. P. Sukumaran Nair. Jagathy Sreekumar plays the lead character in this film. The film is a re-depiction of Smarakasilakal (Memorial Stones), a celebrated Malayalam novel by Punathil Kunhabdulla.   {{cite news
|url=http://www.hindu.com/fr/2009/10/09/stories/2009100950970200.htm
|title= Reinterpreting history on the screen 
|newspaper=The Hindu
|author=C. S. Venkiteswaran
|date=9 October 2009
|accessdate=28 June 2013
}}  {{cite news
|url=http://www.hindu.com/fr/2009/05/22/stories/2009052251180200.htm
|title= Beyond religions
|newspaper=The Hindu
|author=
|date=22 May 2009
|accessdate=28 June 2013
}} 

==Plot==
A progressive thinking man, Pookoya Thangal (Jagathy Sreekumar) does not believe in the caste system or creed. One day a woman named Neeli was ostracized from the Hindu society because she became pregnant before marriage. Pookoya Thangal gave her a place to stay in his servants house. Thangals wife, Aatta Beevi (Margi Sati), was also pregnant.

Neeli died during childbirth of a baby boy. Aatta Beevi gave birth to a baby girl. Neelis son was named Kunhali and Thangals daughters name was Pookkunhi Beevi. Kunhali stayed in Thangals house. Thangals wife hated the presence of Kunhali and he was sent to Pathus house where he was raised up by Pathu.

Both Kunhali and Pookkunhi reached in the stage of higher studies. Kunhali joined for M.B.B.S and Pookkunni Beevi for Degree. At this time Thangal made his daughter to learn Nangyar Koothu, a Hindu ritual art form. After she learned Koothu, a performance was organised in a temple. Some members of the Hindu community did not tolerate the act and opposed.

The film also mentions the split within Muslim League, followed by two other incidents, namely the Emergency and the Babri Masjid incident.

==Cast==
* Jagathy Sreekumar as Pookkoya Thangal
* Indrans as Eramullaan
* Manju Pillai
* Mamukkoya
* Anoop Chandran
* Margi Sati as Aatta Beevi
* Jayakrishnan
* Revathi Menon

==Awards==
* Kerala State Film Award for Second Best Film  
* Kerala State Film Award (Special Jury Award) – Jagathy Sreekumar  

==References==
 

==External links==
* http://www.indiaglitz.com/channels/malayalam/preview/11065.html
* http://popcorn.oneindia.in/title/2260/raamaanam.html

 
 
 


 