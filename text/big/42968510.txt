The Bank Messenger Mystery
 
 
{{Infobox film
| name =  The Bank Messenger Mystery
| image =
| image_size =
| caption =
| director = Lawrence Huntington
| producer =  Will Hammer 
| writer =  Lawrence Huntington 
| narrator = Paul Neville
| music =  Eric Cross
| editing = 
| studio = Hammer Film Productions
| distributor = Renown Pictures
| released = 1936
| runtime = 56 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Paul Neville. Hammer Films.  A bank cashier believes he has been wrongly fired and teams up with some criminals to rob the bank.

==Cast==
*   George Mozart as George Brown 
* Francesca Bahrle as Miss Brown   Paul Neville as Harper  
* Marilyn Love 
* Frank Tickle
* Kenneth Kove 

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 

 