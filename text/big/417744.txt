Sexmission
{{Infobox film
| name           = Sexmission
| image          = Sexmissionposter.jpg
| producer       = Juliusz Machulski
| director       = Juliusz Machulski
| writer         = Juliusz Machulski, Jolanta Hartwig, Pavel Hajný
| starring       = Jerzy Stuhr, Olgierd Łukaszewicz, Beata Tyszkiewicz
| music          = Henryk Kuźniak
| cinematography = Jerzy Łukaszewicz
| editing        = Miroslawa Garlicka
| distributor    = 
| released       = 1984
| runtime        = 117 min. Poland
| Polish
| budget         =
}} cult Poland|Polish comedy science fiction action film. It also contains a hidden political satire layer specific to the time and place of its production.

==Plot== Olgierd Łukaszewicz, respectively, submit themselves in 1991 to the first human hibernation experiment. Instead of being awakened a few years later as planned, they wake up in the year 2044, in a Post-apocalyptic science fiction|post-nuclear world. By then, humans have retreated to underground living facilities, and, as a result of subjection to a specific kind of radiation, all males have died out. Women reproduce through parthenogenesis, living in an oppressive feminist society, where the apparatchiks teach that women suffered under males until males were removed from the world. 

The cold-shoulder treatment Max and Albert receive from the women, their character differences and specific realities of future life serve as background of many humorous encounters. The plot thickens when it turns out that the females have no interest in the rebirth of men, and that for the good of society, the two males are to be killed or "naturalised", i.e. undergo a sex-change. While trying to break away, Max and Albert find out the impact of their masculinity on women. With one of the scientists on their side, the men choose freedom and prefer to escape and die outside. In doing so, they discover the truth: radiation was just a feminist lie to keep women underground and the surviving male population were "naturalised" into women by the feminists when they took power in the post-war period. As a result of discovering the truth, both Max and Albert begin thinking of bringing the world back to normal.

 

==Political and social satire==
The film contains numerous subtle allusions to the realities of the Eastern Bloc|communist-bloc society, particularly to that of the Peoples Republic of Poland just before the fall of communism, perhaps in the anticipation of the major events to come; the fall of communism and the rise of political liberty.   When Max and Albert escape, they jump through the wall, which then starts to shake (often associated with later Lech Wałęsas jumping over the wall of the Gdańsk shipyard, and also with the subsequent fall of the Berlin wall). The secret meeting of the women league  apparatchiks and their lies to the women parallels the communist government of Poland. This dimension of the movie appears to typically escape the viewer more removed from the context. Some sections of this kind were left out from the version shown in Polish theaters by the government censors , but many passed through.

The movie can also be viewed as a satire directed at intergender conflict (wrong-headed feminism or wrong-headed  masculism), prudery, or totalitarianism.

==Reception==
The film has been very popular in Poland. It was proclaimed to be the best Polish film of the last 30 years in a 2005 joint poll by readers of three popular film magazines.    However, this assessment by the audience was considered to be a surprise as it disagreed with the historical rankings of Polish movies by the professional film critics.  It received the Złota Kaczka award for the best Polish movie of 1984. The movie was also fairly popular in Hungary when shown a couple of years later.

==Cast==
* Olgierd Łukaszewicz as Albert Starski
* Jerzy Stuhr as Maximilian Max Paradys
* Bożena Stryjkówna as Lamia Reno
* Bogusława Pawelec as Emma Dax
* Hanna Stankówna as Tekla
* Beata Tyszkiewicz as Berna
* Ryszarda Hanin as Dr. Jadwiga Yanda
* Barbara Ludwiżanka as Julia Novack
* Mirosława Marcheluk as Secretary
* Hanna Mikuć as  Linda
* Elżbieta Zającówna as Zająconna
* Dorota Stalińska as TV Reporter
* Ewa Szykulska as Instructor
* Janusz Michałowski as Professor Wiktor Kuppelweiser
* Wiesław Michnikowski as Her Excellency

==See also==
*Idiocracy
* 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 