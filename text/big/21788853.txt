Mantis in Lace
{{Infobox Film name           = Mantis in Lace image          = Mantis in lace poster.jpg caption        =   director       = William Rotsler producer       = Harry Novak writer         = Sanford White starring  Stuart Lancaster music          =   cinematography = Laszlo Kovacs (credited as Leslie Kovaks) editing        =   distributor    = Boxoffice International Pictures (BIP)  released       = 1968 runtime        =   rating         = R country        = United States awards         = language  English
|budget         = 
}}

Mantis in Lace is a 1968  ), the movie was released under the title, Lila, which coincides with the movies opening scene also crediting the films title as Lila.  

==Plot== psychedelic bad trips in which she visualizes a balding, half-naked old man clutching wads of cash in one hand and a bunch of bananas in the other. These psychotic episodes cause her to murder her partners by stabbing them with a screwdriver and dismembering them with a rusty meat cleaver (or in one case, a garden hoe) while imagining that she is cutting up cantaloupes and watermelons.     

As pieces of the victims bodies are discovered in cardboard boxes, she is pursued by a pair of Los Angeles Police Department detectives played by Steven Vincent and M.K. Evans. The narrative is interrupted by long sequences of topless dancing, softcore pornography, and recreational drug use.  

==Cast== Stuart Lancaster, Janu Wine, John Caroll, John LaSalle, Hinton Pope, Bethel Buckalew, Lyn Armondo, Norton Halper, Judith Crane (dancer), and Cheryl Trepton (dancer). 

==Taglines==
One tagline for the film was "Let her show you the heat of desire — the face of sin!"  The movie poster includes two additional taglines. The first, in large typeface, says "SHE LOVED THEM..and loved them and loved them TO DEATH."  The second tagline refers to the sexually cannibalistic tendencies of mantises and warns "Like a female MANTIS...to love her was to DIE!" 

==Reviews==
Mantis in Lace has been described as "a truly bizarre movie" and "a very strange trip ...(with) lots of tits and little sense";  the review further warns:
 
The acting is either terrible or nonexistent. The same can be said for the direction and editing. The music score, relying chiefly on an electric guitarist who seems to be making up stuff as he goes along — none of it worthy of the term "improvisation", mind you — lurches from quirky kitsch to agonizing irritant very quickly. The drippy "Lila" theme song, used a gazillion times, should require a warning label on the DVD to prevent mental injury. 
 
Another reviewer warns that after watching this "less than riveting tale" the viewers "biggest question will be whether you should categorize what youve just seen as a bad trip or merely a bummer" 

==References==
 

==External links==
* 

 
 
 