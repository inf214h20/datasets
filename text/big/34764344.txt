The Eyes of Thailand
{{Infobox film
| name           = The Eyes of Thailand
| image          = 
| caption        = Theatrical poster
| director       = Windy Borman
| narrator       = Ashley Judd
| producer       = Windy Borman Tim VandeSteeg
| associate producer = Jesisca DeLine
| writer         = Tim OBrien Windy Borman
| music          = Steve Horner
| editing        = Gary Schillinger
| distributor    = 
| released       =  
| country        = United States
| language       = English
| budget         =
}}
The Eyes of Thailand is a 2012 documentary film directed and produced by Windy Borman and produced by Tim VandeSteeg.  The film chronicles the work of Soraida Salwala, who opened the worlds first elephant hospital (Friends of Asian Elephants Hospital) in Lampang, Thailand and together with her team, created the worlds first elephant prosthesis.
  

==Plot==
The Eyes of Thailand tells the true story of Soraida Salwalas 10-year quest to help two elephant landmine survivors, Motala and Baby Mosha, walk again after losing their legs in landmine accidents.  Along with Soraidas efforts to care for the injured elephants and ultimately help them to walk again, the film also highlights the dangers posed by landmines. 

==Production==
Director/Producer Windy Borman started making this film in 2007.  The film has gone through several revisions due to ongoing changes in the story of Motala and Mosha.  Ashley Judd lends her voice as the films narrator, saying about the film - "I hope it will raise awareness to protect Asian elephants—and all beings—from the terror of landmines."  

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipient(s)
! Result
|-
| Ace Documentary Grant 
| December 30, 2011
| Documentary
|
|  
|}

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 


 