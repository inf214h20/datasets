Agentti 000 ja kuoleman kurvit
{{Infobox film
| name           = Agentti 000 ja kuoleman kurvit
| image          = Agentti000.jpg
| caption        = VHS cover
| image_size     =
| director       = Visa Mäkinen
| producer       = Visa Mäkinen
| writer         = Ismo Sajakorpi John Wood John Williams
| cinematography = Pertti Aherva 
| editing        = Christian Achander 
| distributor    = Kinosto
| released       = 16 September 1983
| runtime        = 89 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| followed_by    = 
}} 1983 Cinema Finnish spy spy comedy comedy film John Wood also makes an appearance as Agent 009, and his serious, wooden portrayal of the character stands in contrast to the outrageous antics of the other members of the cast contributing to further humour. Finnish actress Kielo Tommila stars as the reporter Ulla Salla and the main love interest of Agent 000. "000" or "nolla-nolla-nolla" as it was advertised, was then the Finnish national emergency call number.

==Plot== spoof the evil genius villain, car chases, helicopters, gadgets and beautiful women.
 spoof of SPECTRE) from turning the members of Finnish establishment into nudists, drunks or suicide candidates with a mind-control device. The parody content of the film is similar to that of Casino Royale (1967 film) starring David Niven and Peter Sellers.

==Release==
The film premiered on 16 September 1983. It was produced by the Finnish film company Tuotanto Visa Mäkinen and was distributed by Kinosto. The company produced a number of other popular Finnish films in the 1980s and early 1990s such as Pirtua, Pirtua in 1991 in film|1991. The rights to the film are owned by director Visa Mäkinen.

The cinema version of the film had a running time of 89 minutes although the uncut version released on VHS in 1984 ran 4 minutes longer at 92 minutes.

Initially for the big screen, Agentti 000 ja kuoleman kurvit was awarded the Finland:S certificate but the uncut video version was given a Finland:K-12 labelling. A testimony to the lasting significance of the film it was re-released on DVD in the late 1990s.

==Cast==
*Ilmari Saarelainen as Joonas G. Breitenfeldt (Agent 000)
*Tenho Saurén as  Bank Robber John Wood as Agent 009
*Kielo Tommila as Reporter Ulla Salla
*Matti Ruohola
*Jukka Sipilä as Inventor Doctor
*Pekka Elomaa
*Titta Jokinen as Agent
*Seppo Kolehmainen
*Matti Mäntylä
*Martti Pennanen as X
*Rita Polster as Col. Karonov
*Allan Tuppurainen as Agent 0

==Cinematography==
*Pertti Aherva
*Keijo Mäkinen
*Tapani Sillanpää

==References==
 

==External links==
*  

 
 
 
 
 
 
 