Suddenly, It's Spring
{{Infobox film
| name           = Suddenly, Its Spring
| image          = Suddenly Its Spring - 1947 Poster.png
| image_size     = 
| caption        = 1947 Theatrical Poster
| director       = Mitchell Leisen
| writer         = P. J. Wolfson Claude Binyon
| narrator       = 
| starring       = Paulette Goddard
| music          = Victor Young
| cinematography = Daniel L. Fapp
| editing        = Alma Macrorie	 	
| distributor    =  
| released       = February 13, 1947
| runtime        = 87 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Suddenly, Its Spring is a 1947 comedy film directed by Mitchell Leisen. It stars Paulette Goddard and Fred MacMurray.  

==Cast==
*Paulette Goddard as Mary Morely
*Fred MacMurray as Peter Morely
*Macdonald Carey as Jack Lindsay
*Arleen Whelan as Gloria Fay
*Lillian Fontaine as Marys mother

==References==
 

==External links==
* 

 

 
 
 