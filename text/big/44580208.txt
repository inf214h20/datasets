Mind Games (film)
{{Infobox film
| name           = Mind Games
| image          = Mind Games poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Bob Yari
| producer       = Louie Lawless Randolf Turrow 
| writer         = Kenneth Dorward
| starring       = Maxwell Caulfield Edward Albert Shawn Weatherly Matt Norero David Campbell
| cinematography = Arnie Sirlin 
| editing        = Robert Gordon 	
| studio         = MTA Persik Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Mind Games is a 1989 American thriller film directed by Bob Yari and written by Kenneth Dorward. The film stars Maxwell Caulfield, Edward Albert, Shawn Weatherly and Matt Norero. The film was released on March 3, 1989, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Maxwell Caulfield as Eric Garrison
*Edward Albert as Dana Lund
*Shawn Weatherly as Rita Lund
*Matt Norero as Kevin Lund

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 