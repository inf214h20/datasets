Angels (2014 film)
{{Infobox film
| name           = Angels
| image          = 
| caption        = 
| director       = Jean Markose
| producer       =  
| writer         =  
| starring       =  
| music          = Jakes Bejoy
| studio         = Cloud4Cinema
| cinematography = Sujith Sarang
| editing        = Sreejith Sarang
| released       =   
| country        = India
| language       = Malayalam
| budget = 1.90 Cr
| gross = 2.50 Cr (14 Days)
}}
 Malayalam "social Baiju and Dinesh Panicker. Story and screenplay were written by Jean Markose and Toni Tomy, and dialogues were co-written by Shabu Kilithatil.   

==Production==
Filming took place at Chitranjali Studios in Thiruvananthapuram. The film is Produced by Linu Issac, Hisham Basheer, Saju Azad, Maya Kartha under the banner of Cloud 4 Cinemas. Music is composed by Jakes Bejoy, with Sujith Sarang as cinematographer and Sreejith Sarang as editor. Principal filming completed on June 5, 2014.   During filming, there was a shoot day when director Jean Markose was incapacitated due to a high fever, and Indrajith stepped in to temporarily direct in his stead.   Film producer Linu Issac stated "Indran (Indrajith) rocked it."   

Jean Markose is a radio jockey from Dubai and has directed several short films in Dubai. He narrated the script to Indrajith, his wifes cousin, who "immediately signed the deal with a handshake" as he liked the "powerful social message" it had.   According to director Markose, the film is a "social thriller that revolves around ‘Views 24x7,’ a popular crime and investigative show".   Indrajiths role is that of a police officer who was tagged as a "loser" and hence different from other traditional cop roles.    Asha Sarath was signed to play a journalist named Haritha, with the actress stating that Haritha was "as unscrupulous as Geetha", her character in Drishyam. 

==Plot==
Hameem Haider (Indrajith) is a brilliant police officer in the Special Armed Police Department. He does his work efficiently but is an egoist to some extent. Due to external pressures, he is forced to step aside from a case he has been dealing with. He is offended by this and wants to take revenge on those behind forcing his recusal.

His earlier crime investigation becomes the focus of the reality crime show Views 24x7, and it is at this juncture that priest Varghese Punnyalan (Joy Mathew) and Journalist Haritha Menon (Asha Sarath) enter his life. Both of them want to know about Haider and his earlier investigations, but their interests are unrelated and have different motivations.  In their attempts to attain their goal, each becomes aware of the other and the resulting conflict builds the rest of the story line.

==Cast==
 
* Indrajith Sukumaran as S P Hameem Hyder
* Asha Sarath as Haritha Menon
* Joy Mathew as Father Varghese Punyaalan. Baiju as SP Ashok Kumar
* Prem Prakash as Head of Production Vijayakumar as Inspector Poly
* Lakshmi Priyaa Chandramouli as Zaina
* Parvathy Menon
* Dinesh Panicker as DIG Rajan Ponnose
* Aneesh G Menon as Rishi
* Baby Anni as Child
* Tharakalyan
 

==References==
 
 

==External links==
*   at the Internet Movie Database



 
 
 