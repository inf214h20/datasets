Daughters, Daughters
{{Infobox film
| name           = Daughters, Daughters
| image	=	Daughters, Daughters FilmPoster.jpeg
| caption        = 
| director       = Moshé Mizrahi
| producer       = Yoram Globus Menahem Golan
| writer         = Moshé Mizrahi Shaike Ophir
| starring       = Shaike Ophir
| music          = 
| cinematography = Adam Greenberg
| editing        = Dov Hoenig
| distributor    = 
| released       = 1973
| runtime        = 93 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}
Daughters, Daughters ( ) is a 1973 Israeli film directed by Moshé Mizrahi. It was entered into the 1974 Cannes Film Festival.   

==Cast==
* Shaike Ophir - Sabbatai Alfandari
* Zaharira Harifai - Bianca Alfandari
* Joseph Shiloach - Joseph Omri
* Michal Bat-Adam - Esther Alfandari
* Gideon Singer - Dr. Mazor
* Avner Hizkiyahu - Casarola
* Baruch David
* Naomi Greenbaum

==References==
 

==External links==
* 

 
 
 
 
 
 
 