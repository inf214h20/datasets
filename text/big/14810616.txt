Barefooted Youth
{{Infobox film
| name           = Barefooted Youth
| image          = Barefooted Youth.jpg
| caption        = Poster to Barefooted Youth
| film name = {{Film name
 | hangul         =  
 | hanja          = 맨발의  
 | rr             = Maenbaleui cheongchun
 | mr             = Maenbal-ŭi ch‘ŏngch‘un }} Kim Ki-duk 
| producer       = Cha Tae-jin
| writer         = Seo Yun-seong
| starring       = Shin Seong-il Um Aing-ran
| music          = Lee Bong-jo
| cinematography = Byeon In-jib
| editing        = Go Yeong-nam
| distributor    = Keuk Dong Entertainment
| released       =  
| runtime        = 116 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} Kim Ki-duk. It is one of the best-known examples of the "adolescent film" genre that was popular in South Korea during the 1960s. 
it was adapted as 1997 television series with Bae Yong-joon and Ko So-young

==Synopsis==
A good-hearted young gangster falls in love with a diplomats daughter. When their romance is opposed by her mother, they commit suicide together. 

==Notes==
 

==Bibliography==
*  
*  
*  
*  
*  

 
 
 


 