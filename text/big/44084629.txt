Lovely (1979 film)
{{Infobox film 
| name           = Lovely
| image          =
| caption        =
| director       = N Sankaran Nair
| producer       = Sherif Kottarakkara
| writer         = Sherif Kottarakkara TV Gopalakrishnan (dialogues)
| screenplay     = TV Gopalakrishnan
| starring       = Thikkurissi Sukumaran Nair Kottayam Santha Krishnachandran Manavalan Joseph
| music          = M. K. Arjunan Ashok Kumar
| editing        = T Sasikumar
| studio         = Geetha Movies
| distributor    = Geetha Movies
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by N Sankaran Nair and produced by Sherif Kottarakkara. The film stars Thikkurissi Sukumaran Nair, Kottayam Santha, Krishnachandran and Manavalan Joseph in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Thikkurissi Sukumaran Nair
*Kottayam Santha
*Krishnachandran
*Manavalan Joseph
*Paul Vengola
*Sukumaran Baby Sumathi
*Baby Vengola Khadeeja
*Lolitha
*MG Soman
*R. S. Manohar
*P. K. Abraham
*Pala Thankam Sudheer
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by TV Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Asthamanakkadalinte || K. J. Yesudas, Jency || TV Gopalakrishnan || 
|-
| 2 || Ella dukhavum enikku || K. J. Yesudas || TV Gopalakrishnan || 
|-
| 3 || Innathe raathrikku || S Janaki || TV Gopalakrishnan || 
|-
| 4 || Raathri sisira raathri || S Janaki || TV Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 