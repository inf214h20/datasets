Eagle's Wing
 
{{Infobox film
| name           = Eagles Wing
| image          = Eagles Wing poster.jpg
| image_size     =
| alt            =
| caption        =
| director       = Anthony Harvey
| producer       = Ben Arbeid
| writer         = Michael Syson (story) John Briley (screenplay)
| narrator       =
| starring       = Martin Sheen Sam Waterston Harvey Keitel Stéphane Audran
| music          = Marc Wilkinson Billy Williams
| editing        = Lesley Walker
| studio         = Peter Shaw Productions
| distributor    = The International Picture Show Company
| released       =  
| runtime        = 111 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}

Eagles Wing is a Euro-Western Eastmancolor film made in 1979.  It stars Martin Sheen, Sam Waterston and Harvey Keitel.  It was directed by Anthony Harvey, with a story by Michael Syson and a screenplay by John Briley.  It won the British Society of Cinematographers Best Cinematography Award for 1979.

==Plot==
The story has three themes which run concurrently through the film. A stagecoach carrying a rich widow home to her familys hacienda.  A war party of Indians, returning to their village; and two fur traders waiting to meet a different group of Indians with whom they trade.  The war party attacks the other Indians, and kills their leader, who owns a magnificent white stallion.  White Bull (Waterston) attempts to capture the horse, but it is too quick, and makes off carrying the dead chief.    Pike (Sheen) and Henry (Keitel) wait in vain for the traders, and are then attacked themselves by the war party.  Henry is killed, the Indians take the traders horses, and Pike is left alone with only a mule.

Travelling alone, he comes across the ritual funeral of the dead chief.  He saves the white stallion from ritual slaughter, abandons his mule, and  continues his travels. The Medicine Man conducting the ritual is accidentally killed while Pike is taking the horse.  The war party finds the stage coach, attacks it, kills the driver, guard, and one of the passengers, and then leaves White Bull to ransack the coach and passengers of all valuables. He gathers a hoard of jewels and other valuable items, takes a white girl for himself, and leaves the other survivors standing in the desert.  One of the survivors,a priest, takes a coach horse and rides off to alert the hacienda.

The story then becomes a four-way chase.  After gaining the white stallion from Pike, White Bull, the girl, the treasure and the stallion continue towards his village; Pike goes after the stallion; a posse from the hacienda sets out to recover the coach passengers and the girl, and members of the Medicine Mans tribe seek to avenge his death.  After a series of to-and-fro adventures, the film ends with White Bull riding off alone with the stallion; Pike standing watching him go - utterly defeated; the girl behind him, still waiting to be rescued.

==Cast==
* Martin Sheen as Pike
* Sam Waterston as White Bull
* Harvey Keitel as Henry Stephane Audran as The Widow
* John Castle as The Priest
* Caroline Langrishe as Judith
* Jorge Russek as Gonzalo
* Manuel Ojeda as Miguel
* Jorge Luke as Red Sky
* Pedro Damian as Jose
* Claudio Brook as Sanchez
* José Carlos Ruiz as Lame Wolf
* Farnesio de Bernal as The Monk
* Cecilia Camacho as The Young Girl
* Enrique Lucero as The Shaman

==Reception==
The film was not a success at the box office although it received some good reviews, The Observer calling it "dazzling"  and The Guardian saying it was "well worth seeing". 

It was one of the last movies financed by the Rank Organisation. John Huxley. "Losses of £1.6m sound the knell for cinema production." Times   7 June 1980: 17. The Times Digital Archive. Web. 16 April 2014.
 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 