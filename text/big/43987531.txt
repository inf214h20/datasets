Shiksha (film)
{{Infobox film 
| name           = Shiksha
| image          =
| caption        =
| director       = N Prakash
| producer       = M Azim
| writer         = Thoppil Bhasi
| screenplay     = Thoppil Bhasi Sathyan Sheela Kaviyoor Ponnamma
| music          = G. Devarajan
| cinematography = SJ Thomas
| editing        = K Narayanan
| studio         = Azeem Company
| distributor    = Azeem Company
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by N Prakash and produced by M Azim. The film stars Prem Nazir, Sathyan (actor)|Sathyan, Sheela and Kaviyoor Ponnamma in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Shiva Sankaran  Sathyan as Surendran
*Sheela as Shobha
*Kaviyoor Ponnamma as Meenakshi
*Adoor Bhasi as Krishna Pilla
*T. R. Omana as Thankamma
*T. S. Muthaiah as Ramunni Menon
*K. P. Ummer as Prabha Sadhana as Jayamala
*Vijayachandrika as Dancer
*Vijayasree as Dancer
*N. Govindankutty as Prathapan
*Paravoor Bharathan as Pankan Pilla
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mallike Mallike || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Pranayakalahamo || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Rahasyam Ithu Rahasyam || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Swapnamennoru Chithralekha || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Velliyaazhcha Naal || LR Eeswari || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 