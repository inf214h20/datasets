Gus (1976 film)
{{Infobox film
| name           = Gus
| image          = Gus (1976 film) poster.jpg
| caption        = Theatrical release poster
| director       = Vincent McEveety
| producer       = Ron W. Miller
| story          = Ted Key Don Nelson
| starring       = Don Knotts Edward Asner Gary Grimes Tim Conway Harold Gould Tom Bosley Louise Williams Dick Butkus
| music          = Robert F. Brunner
| cinematography = Frank V. Phillips Robert Stafford Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $21,873,000
}} Walt Disney Productions. Its center character is Gus, a football-playing mule.  

==Plot==
Gus is a film about a football-kicking mule ("Gus") and his trainer "Andy" (Gary Grimes).

The film opens with a soccer game, and the Petrovic family watching their son Stepjan win the soccer game. Their other son Andy Petrovic works on his farm in Yugoslavia, and cant play soccer at all. A soccer ball is behind his mule, Gus. After saying that he never wants to see a soccer ball again, Gus kicks the soccer ball a long distance. Andy tries it with him and he says, "Oyage!" and Gus kicks the ball.   

Meanwhile, the California Atoms are a team that cannot do anything right. Debbie Kovac, a woman with Yugoslavian parents gets the Yugoslavian papers, and once Hank Cooper and Coach Venner find out about Gus, they want him over. So with that, Andy and Gus fly over to California and Gus kicking of the football gets them to agree to let them join the team.

==Cast==
* Edward Asner as Hank Cooper
* Don Knotts as Coach Venner
* Gary Grimes as Andy Petrovic
* Tim Conway as Crankcase
* Louise Williams as Debbie Kovac
* Dick Van Patten as Cal Wilson
* Ronnie Schell as Joe Barnsdale
* Bob Crane as Pepper
* Johnny Unitas as Himself
* Dick Butkus as Rob Cargil
* Harold Gould as Charles Gwynn
* Tom Bosley as Spinner
* Titos Vandis as Seth Petrovis
* Hanna Hertelendy Molly Petrovis
* Liam Dunn as Dr. Morgan

==Film information==
The film did well and was released on home video in 1981. The movie is remembered for two sequences involving a hotel and a supermarket.

This is the only one of their five films together where Don Knotts and Tim Conway do not share any scenes.

Johnny Unitas appears as a commentator with Bob Crane (in his last feature film appearance) supplying the play-by-play during the football broadcasts.  Dick Enberg did the play-by-play for the local games.
 The Love Dean Jones in the film Herbie Goes to Monte Carlo.

Gus would be the last feature film in the short career of then 20-year old Grimes, and the final film appearance of Virginia OBrien.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 