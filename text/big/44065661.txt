Kid-Thing
{{Infobox film
| name           = Kid-Thing
| image          = 
| alt            = 
| caption        = 
| director       = David Zellner
| producer       = Nathan Zellner
| writer         = David Zellner
| starring       = Sydney Aguirre Susan Tyrrell David Zellner Nathan Zellner
| music          = The Octopus Project
| cinematography = Nathan Zellner
| editing        = Melba Jodorowsky
| studio         = Zellner Bros.
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
}}
Kid-Thing is a 2012 American drama film written and directed by David Zellner. Producer was his brother Nathan Zellner, who was responsible for the camera. The film was shown in 2012 at various festivals, including the Sundance Film Festival and Berlin International Film Festival. In the USA, the film was released on May 22, 2013 for cinemas.

==Plot==
Ten-year-old Annie lives with her father Marvin on a goat farm somewhere in Texas. Annie is often boring, she has no pleasure for school and her alcoholic addict father does not care about her. Since Marvin spends time with Demolition Derby and his goats, Annie has to deal all the time alone. Out of sheer boredom makes daytime forays through the house, rioted in fathers store room or makes a telephone prank at an auto repair shop. The additional daily routine consists of only forays into the countryside, where Annie lives out her lust for destruction. In this process, she destroyed some objects lying around in the area, the tree trunks are chopped, crushed maggots, toilet bowls and a birthday cake one sitting in the wheel-chair girl smashed with a baseball bat and took her gift, a large colorful lollipop is smashed or shot at bullshit and cow carcasses with a paintball gun. Also bananas are not spared from it and blasted with firecrackers.

When Annie is traveling in the nearby forest she heard calling near a womans voice for help. She attentively followed, where the voice is coming from and finds a dry well in the ground. The woman in the fountain Annie noticed and called to her, but that she wants to please get help to save her, because shes been a while down there. She introduces herself as Esther before but Annie reacts skeptical and believes it is the devil who wants to outsmart her. Frightened, she runs away and goes home. Arriving there Marvin her how to hypnotize a chicken and wants to show her with what is unconditional love. But Annie only replied that the animals just love him because he always gives them food.

The next day Annie returns to the hole back in the woods and brings Esther homemade sandwiches, Capri Sun, toilet paper and a walkie-talkie. Esther thanks indeed but begs Annie at the same time also to help her and to get adult help. But Annie remains skeptical and cannot be persuaded to do the right thing. She makes feel better again on her wanderings, without thinking further about Esther. One evening she radioed Esther with the walkie-talkie and asks how shes down there so goes. Esther is a poorer and angrier, because she could not reach Annie and she denied her the help to come out of the hole. She is so angry that she referred to Annie as bad person. Also Annie is therefore angry and insulted Esther as a wicked witch and also very bad person. The voice of Esther silenced out and can no longer even respond to persuade so much as it tries Annie. The next morning she again tries to spark Esther, but the walkie-talkie is only a faint hiss. She slaps her a deal to get her out when Esther takes her afterwards, no matter where she goes out, but the walkie-talkie is still silent. The Annie moved now to go into the grocery store to buy drinks and bananas for Esther, who then brings it to the hole and throws down. But no voice resounds more out of the hole. Annie starts one last attempt and ignites a New Years Eve Pops, it casts down into the hole, but it does not cause a reaction. She gets on once nosebleeds and therefore makes his way home. Sad and angry Annie is looking at the hideout, where she stashed the stolen gift and opens it. In it she finds a Barbie doll, which her tear off the arms, legs and the head.

Once home Annie watched as Marvin the goats feeding has a heart attack, but she gets no help but look ahead. Then she goes back to the fountain, sits down on the edge, leaves legs dangling into the hole, staring down into the darkness. She decides to one last final step into the unknown and jump down.

==Production==
The shooting of the film took place in a suburb of Austin, Texas. It was produced and directed by the Zellner Bros. Part of the costs were from a Kickstarter funding. This lavish sound design is by Nathan Zellner. For the role of Annie, the Zellner brothers occupied the daughter of acquaintances.

==Reception==
Spearheaded by phenomenal pint-sized lead Sydney Aguirre, this challenging third feature from the Zellner Brothers retains much of their provocative trademark idiocy but navigates darker waters. 

Ms. Aguirre captures Annie’s bottled-up anger nicely, though the role asks a limited amount of her, since it has little dialogue. The film is, if nothing else, an interesting meditation on how a child who grows up without guidance might react to a situation that requires judgment. You can even take a sliver of hope from it. This girl raised by wolves, as it were, may not make the best decisions, but she at least knows instinctively that she should be doing something. 

==References==
 

==External links==
*  
*  
*  

 
 
 