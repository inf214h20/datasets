Yu (film)
{{Infobox film
| name           = Yu
| image          = 
| caption        = 
| director       = Franz Novotny
| producer       = 
| writer         = Michael Grimm Franz Novotny
| starring       = André Eisermann
| music          = 
| cinematography = Andreas Hutter
| editing        = 
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = Austria
| language       = German
| budget         = 
}}

Yu is a 2003 Austrian drama film directed by Franz Novotny. It was entered into the 25th Moscow International Film Festival.   

==Cast==
* André Eisermann as Chris
* Gedeon Burkhard as Tom
* Marina Bukvicki
* Nikola Djuricko
* Vanja Ejdus
* Dejan Lutkic
* Ana Maljevic as Sonja
* Ivana Mrvaljevic
* Ljubisa Samardzic
* David Scheller as Alex
* Ana Stefanovic as Jelena

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 