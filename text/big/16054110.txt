Mere Baap Pehle Aap
{{Infobox film| name           = Mere Baap Pehle Aap image          = Mere baap pehle aap poster.jpg caption        = Promotional poster for Mere Baap Pehle Aap writer         =  starring       = Akshaye Khanna Genelia DSouza Paresh Rawal Om Puri Rajpal Yadav director       = Priyadarshan producer       = Shemaroo Films distributor    = released       = 13 June 2008 runtime        = country        = India language  Hindi
|music Vidyasagar
|editing        = Arun Kumar awards         = budget         = followed_by    = story = Kalavoor Ravikumar}}

Mere Baap Pehle Aap (   . 17 April 2008.  It was released on 13 June 2008.

==Synopsis==
Janaradhan Wishvanbhar Rane is a widower has spent his life bringing up his two kids, Chirag and Gaurav. Janaradhan has done everything to bring them up in the best possible manner, ever since they were toddlers. 

Now the duo has grown up, and Gaurav takes up the responsibility for his father. They manage the household chores and their business to the best of their abilities. The younger son treats his father like his son. Gaurav is on guard with the vigilance of a disciplined parent. He shouts, threatens, fights, and even locks up his father occasionally so that his prankster best friend Madhav Mathur &mdash; who is a divorcé and desperate to get married &mdash; does not spoil him. Madhav and Janaradhan who are always in search of a bride for Madhav forever land up in trouble and every time Gaurav has to bail them out and face the embarrassment. 

Gaurav, busy managing his business, starts getting prank calls from a girl who turns out to be his old college friend Sheekha Kapoor. Sheekha is staying with her guardian Anuradha who was Janardhans first love. Gaurav and Sheekha notice changes in the behavior of Janaradhan and Anuradha when they come face to face after many years. They learn about their past relationship. Now Gaurav wants his father to get married to his lost love. Gaurav and Sheekha embark upon a rib-tickling journey to arrange his fathers love marriage and, in the process, find soul mates in each other. Of course, the path is not so smooth: there are obstacles. But then, Madhavs desperate desire for a bride comes handy.

==Cast==
*Akshaye Khanna as Gaurav Rane
*Genelia DSouza as Sheekha Kapoor
*Paresh Rawal as Janaradhan Wishambhar Rane
*Archana Puran Singh as Inspector Bhavani Bhagvat
*Om Puri as Madhav Mathur
*Rajpal Yadav as Mannu
*Shobana as Anuradha Joshi (Annu) Manoj Joshi as Chirag Rane
*Naseeruddin Shah as Nirmal Kapoor (Sheekhas father)
*Arjun Hansda as Gourav Kumar
* Arzoo Gowitrikar as Anjana Anju Rao
*Sunil Barve
*Mumaith Khan as item number

==Soundtrack==
The soundtrack features songs composed by Vidyasagar (music director)|Vidyasagar. Guest composer Tauseef Akhtar composed one song Ishq Subhan Allah and its remix. 
{{Infobox album |  
| Name       = Mere Baap Pehle Aap
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2008
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      =  Vidyasagar
| Last album = Raman Thediya Seethai (2008)
| This album = Mere Baap Pehle Aap (2008)
| Next album = Mahesh, Saranya Matrum Palar (2008)
}}

{| class="wikitable" width="70%"
! Song Title !! Singers || Composer
|- Vidyasagar
|-
|"Ishq Subhan Allah"  || Alisha Chinai, Neeraj Shridhar, Rap by Bob || Tauseef Akhtar
|- Vidyasagar
|- Vidyasagar
|- Vidyasagar
|-
|"Ishq Subhan Allah (Remix)" || Alisha Chinai, Neeraj Shridhar, Rap by Bob || Tauseef Akhtar
|-
|}

==Reception==
After a poor opening in India and the UK, the film performed below average at the box office eventually grossing   21,14,00,000. 

==References==
 

==External links==
* 
* 
 
 

 
 
 