Bride and Gloom (film)
 
{{Infobox film
| name           = Bride and Gloom
| image          = Bride and Gloom (film).jpg
| caption        = Theatrical poster to Bride and Gloom
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film starring Harold Lloyd.   

==Cast==
* Harold Lloyd as Groom
* Bebe Daniels as Bride
* Snub Pollard
* William Gillespie
* Helen Gilmore
* Lew Harvey
* James Parrott Charles Stevenson (as Charles E. Stevenson)

==See also==
* List of American films of 1918
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 