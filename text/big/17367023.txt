RFK Must Die
{{Infobox film
| name           = RFK Must Die
| image          = RFK Must Die Cover.jpg
| caption        = DVD Cover
| director       = Shane OSullivan
| producer       = Shane OSullivan
| starring       = Robert Kennedy Sirhan Sirhan Munir Sirhan Sandra Serrano Paul Schrade Evan Freed Vincent Di Pierro
| music          = Pyratek
| cinematographer = George Dougherty
| editing        = Shane OSullivan
| distributor    = Soda Pictures
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
}} Shane OSullivan released in 2007. The film expands on his earlier reports for BBC Newsnight and the Guardian and explores alternative theories of what happened the night Bobby Kennedy was shot at the Ambassador Hotel in Los Angeles on June 5, 1968. The title comes from a page of "free writing" found in Sirhan Sirhans notebook after the shooting, in which he wrote "R.F.K. must die - RFK must be killed Robert F. Kennedy must be assassinated… before June 5 68."

Running on an anti-war ticket, Kennedy had just won the California Democratic primary and was confident he would challenge Richard Nixon for the White House. As he walked through the hotel kitchen pantry, shots rang out and he fell to the floor, fatally wounded by a bullet to the brain, fired from an inch behind his right ear.

24-year old Palestinian  Sirhan Sirhan was convicted of murder as the "lone assassin" but witnesses placed Sirhan several feet in front of Kennedy    and for forty years he claims he has never been able to remember the shooting. Defense psychiatrist Bernard L. Diamond claimed Sirhan was in a hypnotic state at the time of the shooting and FBI agent William Bailey saw extra bullet holes in the pantry suggesting a second gunman may have been involved.

Kennedy volunteer Sandra Serrano stated that she saw a woman in a polka-dot dress run down a fire escape exclaiming "We shot him! We shot Kennedy!".  The LAPD could not find her and Serrano retracted her statement under interrogation. The LAPD concluded the girl did not exist but in her first filmed interview since the night of the shooting, Sandra Serrano still sticks to her story.

In his first filmed interview for almost forty years, Sirhans younger brother Munir - Sirhans only surviving family member - describes Sirhans upbringing and what he perceives as the many injustices in the case. Sirhan is one of Americas last surviving political assassins, imprisoned for a crime he claims that he cannot remember. Since 9-11, some revisionist historians have cast him as "the first Arab terrorist."

After a three-year investigation, RFK Must Die was theatrically released in the UK on May 16, 2008 and in New York on June 5, 2008, the fortieth anniversary of the assassination.

The films release comes as new forensic analysis of the only known audio recording of the shooting concluded there were thirteen shots fired from two guns in the Ambassador kitchen pantry and that Sirhan Sirhans Iver Johnson revolver did not fire the bullet that killed Robert Kennedy, prompting new calls for the case to be reopened.  The sound tape, known as the Pruszynski recording, is examined in RFK Must Die: Epilogue. 

OSullivans accompanying book on the case, Who Killed Bobby? The Unsolved Murder of Robert F. Kennedy, was published by Union Square Press on June 3, 2008.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 