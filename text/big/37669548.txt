The Clean Machine
{{Infobox film
| name           = The Clean Machine
| image          = 
| image size     =
| caption        = 
| director       = Ken Cameron Doug Mitchell George Miller
| writer         = Ken Cameron Terry Hayes Richard Mortlock
| based on = 
| narrator       =
| starring       = Steve Bisley Grigor Taylor Ed Devereuax
| music          = 
| cinematography = Dean Semler
| editing        = 
| studio = Kennedy Miller
| distributor    = Network Ten
| released       = 22 May 1988
| runtime        = 90 mins
| country        = Australia English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}}
The Clean Machine is a 1988 Australian tele movie about police corruption starring Steve Bisley. It was one of four telemovies made by Kennedy Miller around this time. 

==Plot==
Inspector Eddie Riordan is appointed to head a new anti-corruption squad.

==Production==
The director was Ken Cameron:
 They asked me did I want to make it on 35mm. Now, Ive always wondered whether I made a big mistake by not doing it on 35mm. But I dont think it would have been a success in the cinema. It wouldnt have had the density that it had on television. In terms of big screen, I could not have had the production values; the money wouldnt have stretched that far. So I dont know. Theres a turning point. You never know what these turning points mean. But I knew one of the factors was that we didnt have Mel Gibson in the lead. I think Steves terrific in it, but to release it as a movie in that genre, you almost needed Mel or a star.   accessed 18 November 2012  
Cameron did say doing the movie revived his career after the box office failure of The Umbrella Woman. 

==References==
 

==External links==
* 

 
 
 
 


 