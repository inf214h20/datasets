Holiday: A Soldier Is Never Off Duty
 
 
{{Infobox film
| name           = Holiday: A Soldier Is Never Off Duty
| image          = Holiday - A Soldier Is Never Off Duty (poster).jpg
| caption        = Theatrical release poster
| alt            =
| director       = A.R. Murugadoss Aruna Bhatia   Twinkle Khanna   Vipul Amrutlal Shah
| writer         = A.R. Murugadoss
| narrator       = Govinda   Sumeet Raghavan
| music          = Pritam Kaushik Dutta Background Score: Prasad Sasthe Sunshine Pictures
| cinematography = Natarajan Subramaniam
| editing        = Amitabh Shukla
| distributor    = Reliance Entertainment
| released       =  
| runtime        = 170 minutes
| country        = India
| language       = Hindi
| budget         =   
| gross          =  
}} action thriller Tamil film Thuppakki  which was also directed by A. R. Murugadoss.    The film released on 6 June 2014,    becoming the biggest nett grosser of the first half of 2014.    

== Plot == Captain in Defence Intelligence Agency (India)|D.I.A., a wing of Indian Army, returns to his home in Mumbai on a holiday. On his arrival, his parents rush him to see Saiba Thapar (Sonakshi Sinha), who they wanted him to marry. But Virat rejects her with an excuse that she is old fashioned and not his type. On the contrary, Saiba is a professional boxer, and is completely modern in her outlook. Virat notices her in a boxing match and falls for her instantly.

One day, while traveling in a bus with his friend sub-inspector Mukund "Makhiya" Deshmukh (Sumeet Raghavan), Virat witnesses a bomb  explosion killing innocent people. Virat manages to capture the man who planted the bomb but he escapes from the  hospital with the help of a police officer. Virat kidnaps the terrorist again and also forces corrupt police officer Ashok Gaikwad (Gireesh Sahedev) to commit suicide. He later discovers that a terrorist group has planned, serial blasts in Mumbai in a couple of days with the help of 12 sleeper agents. Along with his fellow Army officers and Mukund, Virat manages to track these bombers and kills them before they could trigger the bombs.
 sleeper cells (Freddy Daruwala) finds out about the team of officers involved in the failure of the terrorist attack, he begins to target their families by kidnapping someone close to them. When Virat realizes the plan, he substitutes one of the girl to be kidnapped, with his younger sister Priti. Using his pet dog Rocky and his sisters dupatta, he manages to reach the terrorists hideout. He eliminates all the terrorists and rescues all the victims including his sister, who was about to be killed after Virats bluff was exposed. Virat also captures Asif Ali (Dipendra Sharma), who was the leader of the group but later kills him on realizing that Asif is just the second-in-command of the sleeper cell.
 Zakir Hussain), who is also amember of this terrorist group. Virat had a one on one fight with the leader and escapes on a boat before the ship explodes, also later killing the leader. The film ends with Virat forcing Alvin Dsouza to commit suicide and later returning to border along with his team.

==Cast== Captain in Defence Intelligence Agency (India)|D.I.A., a wing of Indian Army.   
* Sonakshi Sinha as Saiba Thapar, a boxer.   
* Sumeet Raghavan as Mukund Deshmukh, Virats friend also an Sub Inspector, Mumbai Police.
* Freddy Daruwala as The leader of the sleeper cells.    Govinda as Pratap, Virat Bakshis Senior Commanding Officer (Cameo Appearance). Zakir Hussain as  Alvin Dsouza.
* Gireesh Sahedev  as ACP Ashok Gaikwad (cameo)
* Dipendra Sharma as Asif Ali
* Maroof Raza in special appearance.

==Production==
The films production commenced in 2013 with the working title of Pistol under the banner of Hari Om Entertainment and Sunshine Pictures. The soundtrack is composed by Pritam Chakraborty and the stunt coordinator of the film is Greg Powell. 

The production budget of Holiday is around   without Akshay Kumar remuneration, print and advertising costs.  Army officer for which Akshay lost some weight to fit in the role and to look 12&nbsp;years younger from his real biological age. Kumar also sported a Crew cut|crew-cut hairstyle for his role. 
Whilst Sonakshi will play a role of boxer and Virat (Kumars) love interest Saiba. 
Model and debutant Freddy Daruwala will play the main antagonist pitted against Kumar. 
 Action Jackson.  However, Action Jackson released on December 5, 2014, while Holiday released on June 6, 2014.

==Music==
{{Infobox album
| Name = Holiday: A Soldier Is Never Off Duty
| Type = Soundtrack
| Artist = Pritam Chakraborty
| Cover = Holiday (Original Motion Picture Soundtrack).jpg
| Released =  
| Length =   Feature Film Soundtrack Zee Music Company
| Chronology = Pritam Chakraborty
 | Last album = Shaadi Ke Side Effects (2014)
 | This album = Holiday: A Soldier Is Never Off Duty (2014)
 | Next album =
}}

The soundtrack was composed by Pritam and Kaushik Dutta and The lyrics were written by Irshad Kamil and Sukumar Dutta.

===Track listings===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length =  
| title1 = Shaayraana
| extra1 = Arijit Singh
| lyrics1 = Irshad Kamil
| length1 = 4:25

| title2 = Tu Hi Toh Hai (Film Version)
| extra2 = Kunal Ganjawala
| lyrics2 = Irshad Kamil
| length2 = 3:55

| title3 = Ashq Na Ho
| extra3 = Arijit Singh
| lyrics3 = Irshad Kamil
| length3 = 5:53

| title4 = Blame The Night
| extra4 = Arijit Singh, Aditi Singh Sharma, Piyush Kapoor
| lyrics4 = Irshad Kamil
| length4 = 4:39

| title5 = Palang Tod Naina
| note5 = Music by: Kaushik Dutta
| extra5 = Mika Singh, Ritu Pathak
| lyrics5 = Sukumar Dutta
| length5 = 3:47

| title6 = Tu Hi Toh Hai
| extra6 = Benny Dayal
| lyrics6 = Irshad Kamil
| length6 = 4:15

| title7 = Palang Tod Naina (Version 2)
| note7 = Music by: Kaushik Dutta
| extra7 = Mika Singh, Ritu Pathak
| lyrics7 = Sukumar Dutta
| length7 = 4:10
}}

==Critical reception==
ABP News similarly rated it 4.5/5,calling it a"fast paced,exhilarating roller coaster ride"  Taran Adarsh gave 4/5 and states that it is a slick action-thriller that keeps you engrossed, enthralled and captivated all through, thanks to its fascinating premise and a watertight, razor-sharp screenplay.  
The Times Of India  gave the movie 3.5 rating describing it as This one applauds the jawans who live in the jaws of death, and is a wake up call for all the sleepers that abound. Bravo!""   Rachel Saltz of The New York Times  stated The action sequences mostly have tension and punch, even if the movie is old-school long — 2 hours 41 minutes — and the plot doesn’t bear too much scrutiny.""   The romantic angle of the movie was criticised.  Hindustan Times  praised the performance of Akshay Kumar.  Filmfare critic praised the movie as , "Genuinely smart action thriller"  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 