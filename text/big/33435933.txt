Galiyon Ka Badshah (1989 film)
{{Infobox film
 | name = Galiyon Ka Badshah
 | image = GaliyonKaBadshah.jpg
 | caption = DVD Cover
 | director = Sher Jung Singh
 | producer = K Yogi
 | writer = 
 | dialogue = 
 | starring = Raaj Kumar Mithun Chakraborty Hema Malini Smita Patil Poonam Dhillon Amrita Singh Danny Denzongpa Iftekhar
 | music = Kalyanji Anandji
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =   
 | runtime = 135 min.
 | country = India
 | language = Hindi
 | budget =  6 Crores
 | preceded_by = 
 | followed_by = 
}} 1989 Hindi Indian feature directed by Sher Jung Singh, starring Raaj Kumar, Mithun Chakraborty, Hema Malini, Smita Patil, Poonam Dhillon, Amrita Singh, Danny Denzongpa, Iftekhar and Om Shivpuri.

==Plot==

Galiyon Ka Badshah is an action thriller, featuring Raaj Kumar and Mithun Chakraborty in lead roles, well supported by Hema Malini, Smita Patil, Amrita Singh, Danny Denzongpa and Iftekhar.

==Cast==
*Raaj Kumar  as  Raja/Ram 
*Mithun Chakraborty  as  Sikander 
*Hema Malini  as  Billoo 
*Smita Patil  as  Tulsi 
*Poonam Dhillon  as  Madhu 
*Amrita Singh  as  Film heroine 
*Danny Denzongpa  as  Inspector Vijay 
*Iftekhar  as  Commissioner of Police 
*Aruna Irani  as  Ranisahiba 
*Shreeram Lagoo  as  Uncle Abdul
*Ranjeet  as  Tiger 
*Parikshat Sahni  as  Rana 
*Om Shivpuri  as  Seth Vinod Kumar
*Leela Mishra  as  Mausi
*S Dubey
*Dev Kumar

==References==
* http://www.bollywoodhungama.com/movies/cast/5274/index.html

==External links==
*  

 
 
 
 
 

 