Deathstalker and the Warriors from Hell
 
 

{{Infobox film
| name = Deathstalker and the   Warriors from Hell
| image = Deathstalker3.jpg
| director = Alfonso "Poncho" Corona
| writer = Howard R. Cohen
| starring = John Allen Nelson Carla Herd Thom Christopher
| producer = Robert North Roger Corman Alfonso Corona
| distributor = Concorde Pictures
| released = 1988
| runtime = 86 min.
| language = English
| budget =
}}

Deathstalker and the Warriors from Hell, also known as Deathstalker III: The Warriors from Hell, is a 1988 sword and sorcery fantasy film. It is the third film in the Deathstalker (film)|Deathstalker tetralogy.

==Plot summary==
The film, which takes place in a fantasy setting, opens at a festival featuring Deathstalker and the wizard Nicias. Deathstalker once saved Nicias and the two go from village to village obtaining money by Nicias foretelling the future and showing his magic. During the festival, a hooded woman arrives to see Nicias. She is actually the princess Carissa bringing a magical stone hoping that Nicias has the other one, which when united, will at long last uncover the magical and rich city of Arandor of whom Nicias is the last of the city’s descendants. Nicias does not possess the second stone, but knows it is south in Southland which is ruled by the evil sorcerer Troxartes. Troxartes has the second stone and wants the first so he can harness its power and rule more.

The festival is attacked by Troxartes’s black-clad right-hand man Makut and his horse soldiers looking for the stone. Amid the slaughter and chaos, Nicias teleports away while the princess is saved from capture by Deathstalker and the two escape. She is nonetheless killed by a few of the unknowing soldiers and passes the stone and knowledge on to Deathstalker. He travels to the hot and wooded Southland where he meets the twin sister of Carissa, the feisty Princess Elizena who was sent from the North to marry Troxartes. Makut is searching for Deathstalker now and finds him again so Deathstalker hides in Elizena’s tent but is alerted by her after she sees he held her with a twig instead of a knife. He escapes into an impenetrable valley where he is given shelter by two wild women, Marinda and her mother. Marinda has sex with Deathstalker and then lead him to their horses so he can escape since Makut has entered the valley. The mother, outraged at Marinda’s absence, leads Makut to the horses, but Deathstalker has escaped. Learning that he is up against Deathstalker, Troxartes uses his power to awaken all the dead foes he defeated to catch the “legend.”

Elizena’s guards were killed by Makut after he thought they were aiding Deathstalker. She accidentally meets Deathstalker who is camping in the woods. In the morning she leaves and is found by Troxartes who takes her back to his castle as his bride. Deathstalker trails them and infiltrates the castle by night, but is found by Troxartes himself who asks for the stone until Deathstalker is knocked unconscious and the stone retrieved. Troxartes figures out there is actually a third stone needed to harness the power so he puts his mistress to torture Deathstalker for the knowledge, but he escapes and ties her up. Heading for the stones, Nicias unknowingly teleports right near Troxartes in the castle who jovially captures him and intends to put him in his army if his magic cannot find the third stone.

In the woods at night, Deathstalker finds Marinda and runs into a few of the undead warriors near a camp fire recognizing Gragas who was killed in a fair fight between Deathstalker earlier. They are forced to do Troxartes’s bidding because their souls are kept secure in jars so Deathstalker makes a deal to get the jars if they will help him against Troxartes. He also tells Marinda to go alert the northern band to come help in the fight against the castle. Elizena learns she is just being kept alive until the third stone is found so she leads Deathstalker to where Nicias is being kept. The third stone is accidentally discovered to have been hidden in the castle all along. The northern band arrives and the souls are released by Deathstalker so the undead warriors turn on Troxartes and his band. In the ensuing battle, Makut is killed by an arrow during a duel between Deathstalker. Troxartes kills Marinda and is then killed by Deathstalker during the fight. The three stones are united at last and it reveals the secret city of Arandor and peace is brought to the land. Deathstalker rides off into the sunset for further adventures.

== Cast ==
*John Allen Nelson as Deathstalker
*Carla Herd as Carissa / Elizena
*Terri Treas as Camisarde
*Thom Christopher as Troxartas
*Aarón Hernán as Nicias
*Roger Cudney as Inaros
*Agustín Salvat as Makut
*Claudia Inchaurregui as Marinda
*Mario Iván Martínez as Preacher
*Carlos Romano as Gragas
*Erika Carlsson as Khorsa
*Alejandro Bracho as Dead Warrior
*Lizetta Romo as Dead Warrior
*Antonio Zubiaga as Soldier
*Manuel Benítez as Soldier

== Production == The Raven for some of the exterior shots of Troxartes’s castle turrets.

== Reception ==

=== Influence === Mike Nelson, Lord of the Rings comments that compare his appearance to Gandalf, Saruman, and Radagast the Brown.  The films lame attempt at a battle scene prompts Nelson to say, "This is one of the most ambitiously bad movies we have ever done."

The episodes stinger (following the end credits) is Marindas mother angrily declaring "Potatoes are what we eat!"

==Soundtrack== Space Raiders and Sorceress (1982 film)|Sorceress. The film also includes an excerpt of Brian Enos "Prophecy Theme" from the soundtrack of the 1984 David Lynch version of Dune (film)|Dune. 

== References ==
 

== External links ==
*  

 
{| style="margin:0 auto" align=center id=toc width=auto Deathstalker Series
|-
|---
| align="center" |  
|}

 
 
 
 
 
 
 
 