The Young Diana
{{Infobox film
| name           = The Young Diana
| image          = The Young Diana poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Albert Capellani Robert G. Vignola
| producer       = 
| screenplay     = Luther Reed
| based on       = The Young Diana by Marie Corelli
| starring       = Marion Davies Macklyn Arbuckle Forrest Stanley Gypsy OBrien Pedro de Cordoba
| music          = 
| cinematography = Harold Wenstrom
| editing        = 
| studio         = Cosmopolitan Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent drama film directed by Albert Capellani and Robert G. Vignola and written by Luther Reed. The film stars Marion Davies, Macklyn Arbuckle, Forrest Stanley, Gypsy OBrien, and Pedro de Cordoba. It is based on the novel The Young Diana by Marie Corelli. The film was released on August 27, 1922, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Marion Davies as Diana May
*Macklyn Arbuckle as James P. May
*Forrest Stanley as Commander Cleeve
*Gypsy OBrien as Lady Anne
*Pedro de Cordoba as Dr. Dimitrius 

== References ==
 

== External links ==
 
*  
 
 
 
 
 
 
 
 
 
 
 
 