A Far Off Place
 
{{Infobox Film
|  name=A Far Off Place
|  image= Faroffplace.jpg
|  caption = Theatrical release poster
|  writer=Laurens Van der Post Robert Caswell Jonathan Hensleigh Sally Robinson David M. Evans (uncredited) Ethan Randall Jack Thompson Maximilian Schell
|  director=Mikael Salomon
|  music=James Horner
|  cinematography=Juan Ruiz Anchía
|  editing=Ray Lovejoy
|  studio=Walt Disney Pictures Amblin Entertainment Touchwood Pacific Partners |	  Buena Vista Pictures
|  released=March 12, 1993
|  runtime=100 min.
|  country=United States
| language=English
|  movie_series=
|  awards= Frank Marshall Kathleen Kennedy Gerald R. Molen
|  budget= 
| gross = $12,890,752 }} adventure drama Ethan Randall, Jack Thompson and Maximilian Schell. The filming locations were in Namibia and Zimbabwe.

==Plot== bushman Xhabbo. On the months-long journey ahead they not only become good friends against their differences, but also realize that every one of them has strength and skills that are required to survive.

==Cast==
* Reese Witherspoon as Nonnie Parker Ethan Randall as Harry Winslow  Jack Thompson as John Ricketts
* Sarel Bok as Xhabbo 
* Robert John Burke as Paul Parker 
* Patricia Kalember as Elizabeth Parker 
* Daniel Gerroll as John Winslow 
* Maximilian Schell as Col. Mopani Theron 
* Miles Anderson as Jardin
* Taffy Chihota as Warden Robert 
* Magdalene Damas as Nuin-Tara

==See also==
*African Cats 
*The Last Lions
*Duma (film)|Duma 
*Cheetah (1989 film)|Cheetah
*Born Free

==References==
 

==External links==
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 