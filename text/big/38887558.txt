Paarthaal Pasi Theerum
{{Infobox film
| name           = Paarthaal Pasi Theerum பார்த்தால் பசி தீரும் 
| image          = Paarthal Pasi Theerum.jpg
| director       = A. Bhimsingh
| producer       = C. R. Basavaraju
| writer         = Aarur Dass
| screenplay     = A. Bhimsingh
| story          = A. C. Tirulokchandar Savitri Sowcar Janaki B. Saroja Devi Kamal Haasan K. A. Thangavelu
| music          = Viswanathan–Ramamoorthy
| cinematography = G. Vittal Rao
| editing        = A. Bhimsingh A. Paul Duraisingam R. Thirumalai
| studio         = AVM Productions G. K. Productions
| distributor    = AVM Productions
| released       =   
| runtime        = 159 mins
| country        =   India Tamil
}}

 Paarthaal Pasi Theerum ( ) is a Tamil language film directed by A. Bhimsingh.  The film features Sivaji Ganesan, Gemini Ganesan, Savitri (actress)|Savitri, Sowcar Janaki and B. Saroja Devi in lead roles. 
The film, produced by C. R. Basavaraju, had musical score by Viswanathan–Ramamoorthy and was released on 14 January 1962.  The film released in Telugu as Pavithra Prema.

==Plot==
Balu (Sivaji Ganesan) and Velu (Gemini Ganesan) work in British Indian Air Force and are fighting with allied forces in World War II. Their plane crashes due to bombs dropped by Japanese. Velu is grievously injured. Balu carries him 50 miles to a village in assam. There Velu regains consciousness and his health starts improving under the care of Indroma(Savithri) and her father, in whose house they are staying. Japanese soldiers come in search of these two. Balu hides Velu and when Japanese are about to discover Velu, he gives himself up to save Velu. Velu recovers, and marries Indroma, teaches her Tamil and calls her Indra. velu is found my members of British Indian army the day after his wedding and has to leave with them, leaving his wife behind. Indra is pregnant and gives birth to a child, however, their village is bombed and she loses her eyesight. When Velu comes in search of his wife, he only sees the ruins of the former village and grieves thinking his wife is dead and goes to Chennai to his maternal uncle.

After 5 years, Balu, who had joined Indian National Army under Netaji Subhash Chandra Bose when Bose had come to Japan, is acquitted in a court and released in Delhi. There, in a refugee camp, he sees a blind Indra and her son and comes to know that Indras father had just died. He promises Indra that he will help her find Velu and unite them. So, Balu takes Indra and her son and travels to Chennai. Indras son starts calling Balu as appa in spite of Indra telling him not to. They find a place to stay in Chennai and the house owner promises Balu that he will help him get a job in his company. On going to his houseowners office, Balu sees that the owner of that company is Velu. He gets very happy and goes to Veluss house,only to see that Velu has married his Maternal uncles daughter Janaki(Sowcar Janaki), who has heart problems and has a son also. Also, Balu also comes to know that Velu thinks that Indra is dead. Balu does not tell him the truth fearing Janaki would not be alive on knowing the truth. Velu gives Balu a very high position in his company. Janakis sister Saro( Saroja Devi) falls in love with Balu and Balu also reciprocates. Meanwhile, Velu sees Indra and her son in Balus house and gets shocked. Balu then tells him the truth about Indra, her blindness and Velus son and cautions him not to talk with her as it might cause both Janaki and Indra to die. Janaki comes to know that Balu  has a son and Saro loves Balu. She thinks that Balu is a married person ashamed of his blind wife and hence trying to cheat her sister. She hates him and cautions Saro against him. Saro comes to Balus house, sees indras son Babu, calling Balu as his father and thinks that Balu has cheated her. Indra, on hearing this comes to Velus house to meet Saro and clear the confusion. Velu, however, on seeing his blind wife pleading for Balus innocence cannot contain himself and tells the truth to everyone. Janaki on hearing that Indra is her husbands first wife, cannot take the shock and dies. She unites Balu and Saro before dying and apologises to Balu for having suspected him and thanks him for all the sacrifices he has made for her husband and family.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Sivaji Ganesan ||  Balu
|-
| Gemini Ganesan ||  Velu
|- Savitri ||  Indhra
|-
| Sowcar Janaki ||  Janaki
|-
| B. Saroja Devi ||  Saroja
|-
| K. A. Thangavelu ||  Sakkarabani
|-
| M. Saroja ||  Sandhana Lakshmi
|-
| Kamal Haasan || Babu & Kumar
|-
|}

==Crew==
*Producer: C. R. Basavaraju
*Production Company: G. K. Productions
*Director: A. Bhimsingh
*Music: Viswanathan–Ramamoorthy
*Lyrics: Kannadasan
*Story: A. C. Tirulokchandar
*Screenplay: A. Bhimsingh
*Dialogues: Aarur Dass
*Art Direction: H. Santharam
*Editing: A. Bhimsingh, A. Paul Duraisingam & R. Thirumalai
*Choreography: B. Hiralal, Chinni & Sampath
*Cinematography:  G. Vittal Rao
*Stunt: None
*Audiography: J. J. Manickam & V. S. M. Gopalram
*Dance: None

==Soundtrack==

{{Infobox album |  
 Name        = Paarthaal Pasi Theerum|
 Type        = soundtrack |
 Artist      = |
 Cover       = | 
 Released    = 1962| 
 Recorded    = |  Feature film soundtrack |
 Label       = | 
 Producer    = Viswanathan–Ramamoorthy| 
}}

The music composed by Viswanathan–Ramamoorthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 07:05
|-
| 2 || Andru Oomai Pennallo   || P. Susheela || 03:46
|-
| 3 || Kodi Asainthathum || T. M. Soundararajan, P. Susheela || 03:30
|-
| 4 || Paarthal Pasi Theerum || P. Susheela || 03:22
|-
| 5 || Pillaikku Thandhai Oruvan || T. M. Soundararajan || 03:01
|-
| 6 || Ullam Yenbadhu || T. M. Soundararajan || 03:22
|-
| 7 || Yaarukku Maapilai || P. Susheela || 03:32
|}

==References==
 

==External links==
 

 
 

 
 
 
 
 
 