The Dismantling
 
 
{{Infobox film
| name           = The Dismantling (Le Démantèlement)
| image          = 
| caption        = 
| director       = Sébastien Pilote
| producer       = 
| writer         = Sébastien Pilote
| starring       = Gabriel Arcand
| music          = 
| cinematography = Michel La Veaux
| editing        = Stéphane Lafleur
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Canada
| language       = French
| budget         = 
}}

The Dismantling ( ), also released in the United States under the title The Auction, is a 2013 Canadian drama film written and directed by Sébastien Pilote. It was screened in the Contemporary World Cinema section at the 2013 Toronto International Film Festival.      

The film is a shortlisted nominee for  , 13 January 2014. 

==Cast==
* Gabriel Arcand as Gaby Gagnon
* Pierre-Luc Brillant as Caretaker
* Normand Carrière as Léo Simard
* Claude Desjardins as Auctioneer
* Sophie Desmarais as Frédérique Gagnon
* Éric Laprise as Bank employee
* Lucie Laurier as Marie Gagnon
* Dominique Leduc as Neighbor
* Gilles Renaud as Accountant / Friend
* Johanne-Marie Tremblay as Françoise

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 