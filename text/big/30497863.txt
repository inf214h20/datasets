Tomorrow, the World!
{{Infobox film
| name           = Tomorrow, the World!
| image          = TomorrowTheWorld.jpg
| caption        = Theatrical release poster
| director       = Leslie Fenton
| producer       = Lester Cowan David Hall (associate producer)
| screenplay   = Ring Lardner, Jr. Leopold Atlas
| based on     =  
| starring       = Fredric March Betty Field Agnes Moorehead Skip Homeier
| music          = Louis Applebaum
| cinematography = Henry Sharp
| editing        = Anne Bauchens
| studio         = 
| distributor    = United Artists
| released       = December 29, 1944
| runtime        = 86 min (82 min in the Video-Cinema Films Inc. print)
| language       = English and German
| country        = USA
| budget         =
| gross          =
}}
 Broadway play of the same name.

The title comes from Hitlers famous threat: "Today, Germany; tomorrow, the world."   Accessed January 16, 2011.  

==Background==
The play Tomorrow, the World! opened on Broadway in New York City, New York, USA on 14 April 1943 and closed 17 June 1944 after 500 performances. The opening-night cast included Skip Homeier as Emil and Edit Angold as Frieda, both of whom originated their movie roles in the play, and Ralph Bellamy as Mike Frame, Shirley Booth as Leona Richards and Kathryn Givney as Jessie Frame.   

Producer Lester Cowan bought the rights to the play for $75,000 plus 25% of the gross, not to exceed $350,000. He wanted to change the title of the movie to "The Intruder," but a poll of exhibitors voted him down. 

==Synopsis== Nazi propaganda from his years in the Hitler Youth.   Accessed January 16, 2011. 
 Third Reich, traitor and Jewish fiancée (Field).   Accessed January 16, 2011. 
 The Seventh Cross (1944), that sought to depict the everyday German. Instead of focusing on cruel Nazi stereotypes, the film concerned itself with those who were misguided or victimized by Hitlers brutal regime." 

==Reception==
Rotten Tomatoes refers to the movie as a "critically acclaimed World War II classic." 

The movie is one of the films highlighted in the 2004 television documentary,  , examining the way Hollywood films dealt with the sensitive topics of the Holocaust and anti-semitism.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 