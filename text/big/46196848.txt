My Bromance
{{Infobox film
| name             = My Bromance
| image            = This_is_a_poster_for_My_Bromance.jpg
| caption          = Theatrical movie poster
| director         = Nitchapoom Chaianun
| producer         = Navaraj Kanagaraj (Executive Producer)   Nantuchaporn Samerpong (Production Manager)
| writer           = 
| screenplay       = 
| starring         = Teerapat Lohanan Pongsatorn Sripinta. 
| cinematography   = 	
| editing          = 
| music            = Piyatut Hemstapat (Music Composer)   Bill Piyatut H. (OST)   Harmonica Sunrise (OST)
| studio           = Navaraj Studio
| distributor      = WayuFilm
| released         = 20 February 2014 (Thailand)
| runtime          = 1 hour 58 minutes 53 seconds
| country          = Thailand
| language         = Thai
| budget           =
| gross            = $102,015 
}}

My Bromance also known as Pee Chai or Phi Chai and Pi Chai   ( ), is a 2014 Thai movie starring Teerapat Lohanan and Pongsatorn Sripinta. The movies director is Nitchapoom Chaianun.  The film was filmed in 2013 and premiered on February 20, 2014. The movie was categorized as Drama and was filmed primarily in Chiang Mai, Thailand.

The show main sponsors are: Changs Drinking Water, Holiday Inn Chiang Mai, Paradise Clinic and The Greenery Villa.

On 13 December 2014, it was revealed that the show production is undergoing filming on a new sequel named: My Bromance: The Series.  The sequel was later renamed to  .

==Plot==
"Golf" is an aggressive and hot-tempered boy who has grown up in a broken family who deprives the attention of his father. "Bank" on the other hand is a gentle, considerate and sweet-natured lovely boy who hopes that the new family he is going into accepts him. But he wasnt welcomed by his brother. Before they accepts each other as "Brothers", they have to learn the art in living together to overcome the obstacles of the surroundings and gradually fall in love.

==Story Synopsis==
Golf (played by Teerapat Lohanan) whom is referred by his friends as aggressive, hot-tempered, a playboy and foul-mouth 18 year old high school student. He comes from a broken family, often living in the semi-detached home alone, often depriving the love and attention from his father, but the latter showered him with money.

One day, Golf was thrown in a ruthless world of family politics when Golfs father brought a woman (Thara) and Bank (played by Pongsatorn Sripinta) home, confused as it is, Golf slowly learns that, that Thara would be his new step-mother, and also had to learn that he has a step-brother who is just 4 months his junior. Golf, who has never been an older brother and doesnt wish to have a younger brother was forced to accept the sudden change with new additions in the family. Golf began to ostracized Bank when he tries to make contact with the new older brother.

Soon after, initially belonging to another class, Bank asked the school principal to transfer him to Golfs class to be close to him, Golfs chanced the opportunity on Bank by bullying him. as he hates to see him, taking any opportunity that comes his way, resulting dampening the relationship with his friends (especially Jieb). And the concerned group of classmates of theirs stepped in, making Golfs in realizing what he did to Bank was wrong, When the jealous Golf discovers that Bank who is willing to do anything and everything for his older brother. Banks actions slowly earn and won the respect of Golf, and slowly accepts him as brother. However love start to dazzle and blossom in the two unknown brothers.

Things gets sticky when Thom (played by Withawat Thaokhamlue) a popular school singer tries his ways to woo Bank, by sending him lunchbox, flowers and snacks. Also, he went as far as to buying Banks contact number from Banks classmate and friend (Tar), knowing his family is poor. Affected by what Golfs saw in his own eyes, that Bank communicated with Thom, he turned jealous, and started a heated argument with Bank, and end up forcing Bank to confess his true feelings for his older brother. Golf later confessed his true feelings too and in return, gave Bank a couple ring which Bank wanted to buy earlier on.

Things does not look so good as it seems, apart from having the approval by their classmates (Jieb, Paan, Tar and Tued), the duo relationship start to crack when Golfs aunt suspected and eventually caught the audacious act by the two step-brothers laying on top of each other as they return home from a trip. Horrified and notified by the action, Golfs father called for a family meeting and eventually sent Golf to America to study, in hopes that the two brothers relationship will break, leaving both boys sad and withered like flowers.

Six months has passed, Golf returned home, a happy Bank was shocked and couldnt accept that his older brother has a new girlfriend (Kaem) whom they knew in America, and to make matters worst, Golf is about to get engaged to Kaem, leaving a heart-broken Bank to date Thom, however Banks steadfast love for Golf did not change. Being confronted by Golfs firm love to Bank and in the state of a heated argument proving who loves who, both brothers was involved in a car accident leaving the two brothers seriously injured.

A shocked Golf slowly understands that his brother life was left hanging when the doctors had to remove one of his kidneys, and the latters health did not improve, as Banks other kidney did not function properly, a sad Golf decides to donate his kidney to Bank, with several disapproval from his father. After pleading, Golf eventually donates his kidney to his younger brother, a pact they have done before that none of the brothers will leave each other.

Fast forward to one year later, during Banks 20th birthday, unbeknownst to Bank about his brother Golfs passing. Bank unwraps the last present and a letter given to him, but was left in tears when he learnt that his brother had died of a brain tumor (a condition that was discovered after the duo had the car accident), and the truth that Golf has donated one of the kidneys to Bank, telling him his love to his younger brother, leaving Bank even more distraught.

As Bank visited Golfs grave, he thanked the older brother and offered the ring to him, with his finger wearing one, proving Banks everlasting and steadfast love to his brother. To a boy who has suffered so much, Bank took on his brother legacy by doing what he likes most, painting and collecting plastic toy fixtures. (To the viewers, it was shown that Banks mother had born a son). And Bank fully understands and is able to with-hold the love of his older brother, Bank later affirmed the love of his brother by visiting a bridge area which the brothers frequently visited.

==Cast==

===Main Cast===
{| class="wikitable sortable"
|-
! Character !! Portrayed by !! Remarks
|-
| Kanthitat Atsawametanon || Teerapat Lohanan || Better known as Golf   Vuts Biological son   Tharas older step-son   Banks older step-brother   Banks love interest   Thoms love rival   Kaems unmarried fiancé   Born on 1 August 1994   Deceased on 9 May 2013   Being referred as a playboy, likes playing truant, bad tempered, a person who doesnt reflect own words and easily jealous   Died of brain tumor   Donated kidney to Bank   Ended up being with Bank (although he has passed away)   In Grade 11 Class 1
|-
| Baworananan || Pongsatorn Sripinta || Better known as Bank   Tharas Biological son   Vuts younger step-son   Golfs younger step-brother   Golfs love interest   Thoms love interest   Kaems love rival   Born on December 1994/January 1995.    Being referred as a sweet natured, gentle and considerate   Recipient of Golfs kidney   Ended up being with Golf (although Golf has passed away)   In Grade 11 Class 1
|-
|} 

===Supporting Cast===
{| class="wikitable sortable"
|-
! Character !! Portrayed by !! Remarks
|-
| Thom || Withawat Thaokhamlue || Golfs love rival   Banks love interest   Jiebs super fan   School Boy-Band Member
|-
| Vut ||   || Golfs biological father   Mais brother   Married Banks Mother Thara   Banks step-father   Works in a plastic toy company.    Father of a later born child 
|-
| Mai ||   || Golfs Aunt   Vuts Sister   A Divorcee
|-
| Thara || Phantphin T Chiangmai || Also known as Tara   Banks Biological Mother   Golfs step-mother   An ex-widow   Married Golfs Father   Had a baby in the end
|-
| Tar || Worakamon Nokkaew || Bank and Golfs classmate   Paans love interest   Panns boyfriend   In Grade 11 Class 1
|-
| Jieb || Wachiraporn Attawut || Bank and Golfs classmate   Thoms admirer   In Grade 11 Class 1
|-
| Paan || Varatchaya Comemamoon || Bank and Golfs classmate   Tars love interest   Tars girlfriend   In Grade 11 Class 1
|-
| Tued || Naradon Namboonjit || Bank and Golfs classmate   In Grade 11 Class 1
|-
| Mr Nikom ||   || Grade 11 Class 1 Form Teacher   Golf, Bank, Jieb, Tar, Paan and Tued Form Teacher
|-
| Sutthisanan ||   || Also known as Kaem   Golfs unmarried fiancée   Banks love rival   Did not marry Golf in the end
|-
|} 


==Trivia==

* During Banks visit to Golfs grave, while he was placing the bouquet of flowers, it was not known whether or not it was intentionally or unintentionally showing the ring in Banks fingers. As this is supposed to be shown when Bank is giving the ring to the already deceased Golf, as part of the love they had. 

==Reception==

Ratings shown were based on the latest statistics as of 24 March 2015.

{| class="wikitable" style="text-align:center;"
|-
! No.
! Website
! Rating
! Ref
|-
| 1 
| IMDb
| 6.6/10
|  
|-
| 2 
| AsianWiki
| 95/100
|  {{cite web|url=http://asianwiki.com/My_Bromance_-_Thai_Movie|title=My Bromance - AsianWiki|publisher=AsianWiki
|date=April 12, 2015|accessdate=April 12, 2015}} 
|-
| 3 
| SiamZone
| 8.33/10
|  {{cite web|url=http://www.siamzone.com/movie/m/7073/%E0%B8%9E%E0%B8%B5%E0%B9%88%E0%B8%8A%E0%B8%B2%E0%B8%A2|title=My Bromance - Siam Zone|publisher=SiamZone
|date=April 12, 2015|accessdate=April 12, 2015}} 
|-

|}

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 