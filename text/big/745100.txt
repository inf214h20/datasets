James and the Giant Peach (film)
 
{{Infobox film
| name           = James and the Giant Peach
| image          = James and the giant peach.jpg
| caption        = Theatrical release poster
| director       = Henry Selick
| producer       = Denise Di Novi Tim Burton Jonathan Roberts Steve Bloom
| based on       =  
| narrator       = Pete Postlethwaite Paul Terry Simon Callow Richard Dreyfuss Susan Sarandon Jane Leeves Miriam Margolyes David Thewlis Joanna Lumley
| music          = Randy Newman
| cinematography = Pete Kozachik Hiro Narita
| editing        = Stan Webb
| studio         = Walt Disney Pictures Allied Filmmakers Skellington Productions Buena Vista Pictures
| released       =  
| runtime        = 76 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $38 million
| gross          = $37,734,758 
}} musical fantasy novel of Paul Terry as James. The film is a combination of live action and Stop motion|stop-motion animation. Co-stars Joanna Lumley and Miriam Margolyes played Jamess aunts in the live-action segments, and Simon Callow, Richard Dreyfuss, Susan Sarandon, Jane Leeves, and David Thewlis voiced his insect friends in the animation sequences.

==Plot==
 
James Henry Trotter is a young boy who lives with his parents by the sea in England. On James birthday, they plan to go to New York City and visit the Empire State Building, the tallest building in the world. However, before that happens, his parents are killed by a ghostly rhinoceros from the sky and James finds himself living with his two neglectful aunts, Spiker and Sponge.

He is forced to work all day and they threaten him with beatings to keep him in line and taunt him about the mysterious rhino and other hazards if he tries to leave. While rescuing a spider from being squashed by his aunts, James meets a mysterious man with a bag of magic green "crocodile tongues", which he gives to James to make his life better. The soldier warns him not to lose the "tongues" and disappears. When James is returning to the house, he trips and the "tongues" escape into the ground. 

This turns a peach on a withered old tree into a gigantic size. Spiker and Sponge sell tickets to view the giant peach. James crawls inside a large hole he inadvertently creates in the peach, and he finds and befriends a group of life-size anthropomorphic bugs (Mr. Grasshopper, Mr. Centipede, Earthworm, Miss Spider, Mrs. Ladybug, and Glowworm). As they hear the aunts search for James, Mr. Centipede cuts the stem connecting the giant peach to the tree and the peach rolls away to the Atlantic Ocean.

Remembering his dream to visit New York City, James and the insects decide to go there with Mr. Centipede steering the peach. They use Miss Spiders silk to capture and tie a hundred seagulls to the peach stem, while battling against a giant robotic shark. Miss Spider reveals to James that she was the spider he saved from Spiker and Sponge. The next day, James and his friends find themselves in The Arctic; the Centipede has fallen asleep while keeping watch. After hearing Mr. Grasshoper wishing they had a compass, Mr. Centipede jumps off the peach into the icy water below and searches a sunken ship for a compass but is taken prisoner by skeletal pirates. James and Miss Spider rescue him and the journey continues.

As they reach New York City, a storm appears, along with the ghostly rhino. James is frightened but challenges the rhino and gets his friends to safety before the rhino strikes the peach with lightning; James and the peach fall to the city below, landing on top of the Empire State Building. After he is rescued by police officers, firefighters, and the largest crane in New York City, Spiker and Sponge arrive and attempt to claim James and the peach. James reveals Spiker and Sponges abusive behavior towards him to the crowd, who gasp in shock at the revelation. Spiker and Sponge become enraged by James betrayal and attempt to kill him. The bugs arrive and tie up Spiker and Sponge with Miss Spiders silk and the two aunts are taken away. James introduces his friends to the New Yorkers and allows the children to eat up the peach.

The peach pit is made into a house in Central Park, where James lives happily with the bugs, who form his new family and also take important jobs in the city. James celebrates his 9th birthday with his new family.

In a post-credits scene, a new arcade game called "Spike the Aunts" is shown, featuring the rhino.

==Cast== Paul Terry as James Henry Trotter
* Miriam Margolyes as aunt Sponge
* Joanna Lumley as aunt Spiker
* Pete Postlethwaite as Narrator/the Magic Man

===Voices===
* Simon Callow as Mr. Grasshopper
* Richard Dreyfuss as Mr. Centipede
* Jeff Bennett as Mr. Centipede (singing voice)
* Jane Leeves as Mrs. Ladybug
* Susan Sarandon as Miss Spider
* David Thewlis as Earthworm
* Miriam Margolyes as Glowworm

==Songs==
* My Name Is James - Paul Terry
* Thats the Life For Me - Jeff Bennett, Susan Sarandon, Jane Leeves, Miriam Margolyes, Simon Callow, and David Thewlis
* Eating the Peach - Jeff Bennett, Susan Sarandon, Jane Leeves, Miriam Margolyes, Simon Callow, David Thewlis, and Paul Terry
* Family - Simon Callow,  Jeff Bennett, Jane Leeves, David Thewlis, Susan Sarandon, Miriam Margolyes, and Paul Terry
* Good News - Randy Newman

==Production==

The film begins with normal live-action for the first 20 minutes,    but becomes stop-motion animation after James enters the peach, and then live-action when James enters New York City (although the arthropod characters remained in stop-motion). Selick had originally planned James to be a real actor through the entire film, then later considered doing the whole film in stop-motion; but ultimately settled on entirely live-action and entirely stop-motion sequences, to keep lower costs.  Unlike the novel, James aunts are not killed by the rolling peach (though his parents deaths also occur like the novel) but follow him to New York. 

==Reception== film version of James and the Giant Peach produced during his lifetime, his widow, Liccy, approved an offer to have a live action version produced. She thinks Roald "would have been delighted with what they did with James. It is a wonderful film."   

James and the Giant Peach received near-universal acclaim from film critics. Review aggregator Rotten Tomatoes gives the film a score of 93% based on reviews from 69 critics, with a "Certified Fresh" rating and an average score of 7.2/10. The sites consensus states: "The arresting and dynamic visuals, offbeat details and light-as-air storytelling make James and the Giant Peach solid family entertainment". 

Owen Gleiberman of Entertainment Weekly gave the film a positive review, praising the animated part, but calling the live-action segments "crude."  Writing in The New York Times, Janet Maslin called the film "a technological marvel, arch and innovative with a daringly offbeat visual conception"  and "a strenuously artful film with a macabre edge." 

===Awards=== Best Music, Original Musical or Comedy Score, by Randy Newman. It won Best Animated Feature Film at the Annecy International Animated Film Festival.

==Home media==
A digitally restored Blu-ray Disc|Blu-ray/DVD combo pack was released by Walt Disney Studios Home Entertainment on August 3, 2010 in the United States. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 