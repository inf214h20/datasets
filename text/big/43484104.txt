Anukshanam
{{Infobox film
| name           = Anukshanam 
| image          = Anukshanam_Movie_First_Look_Poster.jpg
| alt            = 
| caption        =
| director       = Ram Gopal Varma
| producer       = Vishnu Manchu
| writer         = Ram Gopal Varma
| starring       = {{Plainlist|
* Vishnu Manchu  
* Revathi
* Surya
* Kota Srinivasa Rao
* Brahmanandam
* Navdeep
* Tejaswi Madivada
* Madhu Shalini}}
| music          = 
| cinematography = Nani Chamidisetty
| editing        = Kamal R Santosh Bammidi
| studio         = 24 Frames Factory
| distributor    = 24 Frames Factory (worldwide)  
| released       =  
| runtime        = 
| country        = India
| language       =Telugu
| budget         = 
| gross          = 
}}
Anukshanam ( , slasher film written and directed by Ram Gopal Varma, starring Vishnu Manchu and Revathi in lead roles.    Ram Gopal Varma has started a new distribution system with this film by auctioning this movie through a website.   Upon release the edge of the seat thriller received very positive reviews,    and became an instant hit at the Indian and United states box office.  

==Plot==
Inspired by a real life incident, the film is based on a serial killer who sends a wave of panic across Hyderabad.

==Cast==
* Vishnu Manchu - Gautham (Deputy Commissioner of Police)
* Surya - Psychopath Killer
* Revathi - Shailaja (Psychologist)
* Kota Srinivasa Rao - Harish Reddy (Home minister)
* Brahmanandam - Shailajas Brother
* Navdeep 
* Tejaswi Madivada - Satya (Gauthams wife)
* Madhu Shalini - Asha (TV 6 News Reporter)

== References ==
 

==External links==
* 
 
* http://www.india-daily.com/

 
 
 
 
 
 
 
 
 

 