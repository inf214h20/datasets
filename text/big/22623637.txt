Oute gata oute zimia
{{Infobox film
| name           = Oute gata oute zimia Ούτε γάτα ούτε ζημιά
| image          = 
| image_size     = 
| caption        = 
| director       = Alekos Sakellarios
| producer       = 
| writer         = Christos Giannakopoulos
| starring       = Vasilis Logothetidis Ilya Livykou Lampros Konstantaras Mimis Fotopoulos Rena Stratigou Dina Stathatou Charis Kamili Margarita Lambrinou
| music          = 
| cinematography = 
| photographer   = 
| editing        = 
| distributor    = Finos Films
| released       =  
| runtime        = 90 minutes
| country        = Greece
| language       = Greek
}}
Oute gata oute zimia ( ) is a 1954 Greek black-and-white film directed by Alekos Sakellarios and Christos Giannakopoulos.


==Plot==

Businessman Lalakis Makrykostas (Vasilis Logothetidis) is married to beautiful and much younger Popi (Ilya Livykou), whos being courted by Nikos Koutroumbas (Lampros Konstantaras). Popi is becoming suspicious of Lalakis frequent business trips that take him away from Athens. 

While Lalakis is away on yet another supposedly business trip, it is revealed by a series of coincidences that hes in fact on a romantic getaway to Thessaloníki with his mistress. Popi decides to give him a taste of his own medicine and agrees to go to a romantic getaway of her own with Nikos. Nikos takes her to a village in Boeotia called Thymaria, where the station-master Stelios Molfetas (Mimis Fotopoulos) is an old friend of Nikos.  

By coincidence, Lalakis and his mistress, Lolota (Rena Stratigou) have missed their train to Thessaloníki and are spending the night at the station-masters home as well. Both illicit couples, the station-master and his wife (Margarita Lamprinou) sit down to a very uncomfortable dinner; Popi pretends shes Nikos wife, nearly convincing her own husband that shes just his wifes doppelgänger. The couples eventually retire for the night; Popi and Nikos drive back to Athens in the middle of the night whereas Lalakis, eager to return to Athens in order to confront Popi about her infidelity, is forced to make his way back by horse and carriage and public transport.

Popi arrives home first and establishes an "alibi". Lalakis arrives shortly after her but cant prove anything. Right when Popi is starting to ask him questions on his stay in Thymaria theres a knock on the door; its the station master returning Lalakis wallet, which had fallen out of his pocket the night before. The station-master also mentions that Popi and Nikos also left early (the implication being that they never slept together) and that Popi left her watch behind. Lalakis, now having concrete proof of his wifes infidelity, offers to return it to  her and takes it from the station-master. However, instead of confronting Popi, he just tells her that the jewellery shop has returned her watch. After a moment of awkwardness, they both start laughing, essentially agreeing to forget the whole thing.

==References==
*Finos Films
*Greek Cinematic Centre/Center

==See also==
*List of Greek films

==External links==
* 

 

 
 
 