Nadodi Pattukkaran
{{Infobox film
| name           = Nadodi Pattukkaran
| image          = 
| image_size     =
| caption        = 
| director       = N. K. Viswanathan
| producer       = Sangili Murugan
| writer         = Sangili Murugan Jaya Rajendran  (dialogues) 
| starring       =  
| music          = Ilaiyaraaja
| cinematography = N. K. Viswanathan
| editing        = V. Rajagopal B.K. Mohan
| distributor    =
| studio         = Murugan Cine Arts
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1992 Tamil Tamil drama Karthik and Mohini in lead roles. The film, produced by Sangili Murugan, had musical score by Ilaiyaraaja and was released on 15 May 1992. 

==Plot==

Sundaram (Karthik (actor)|Karthik), Thevar Ayya (M. N. Nambiar), Periya Madurai (Thyagu), Seedan (Charle), Vadivelu (Chinni Jayanth) and Annamalai (Manangatti Subramaniam) form a music troup. Sundaram is a jobless graduate who prefers making his living by singing than being unemployed. The group travel from village to village leaving their family. One day, the troup enters in a village where the robbers spreads terror amongs the villagers. The troup confront them and the police arrests the robbers. The troup becomes quickly popular. Geetha (Mohini (Tamil actress)|Mohini), the daughter of the village head Sivathaya (Jaishankar), falls under the spell of Sundaram. Later, Geetha and Sundaram fall in love with each other, on the other side Geethas family objects their love. What transpires later forms the crux of the story.

==Cast==
 Karthik as Sundaram Mohini as Geetha
*Jaishankar as Sivathaya
*M. N. Nambiar as Thevar Ayya
*S. S. Chandran as Sakkarai
*Rocky as Periya Pandi Senthil as Uyaram Vivek
*Charle as Seedan
*Chinni Jayanth as Vadivelu Thyagu as Periya Madurai
*Manangatti Subramaniam as Annamalai
*Senthamarai as DSP
*V. Gopalakrishnan
*Peeli Sivam
*O.A.K. Sundar as Chinna Pandi
*Karuppu Subaiah as Suna Pana
*Kullamani
*Master Kannan
*Master Arun
*Ganthimathi as Chella Thayi
*Y. Vijaya as Vadivu
*S.N. Parvathy Annapoorna as Sundarams mother
*Vijaya Chandrika
*Thalapathy Dinesh

==Soundtrack==

{{Infobox Album |  
| Name        = Nadodi Pattukkaran
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1992
| Recorded    = 1992 Feature film soundtrack |
| Length      = 28:21
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1992, features 7 tracks with lyrics written by Vaali (poet)|Vaali, Muthulingam, Gangai Amaran, Na. Kamarasan, Piraisoodan and Parinaman.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Aagaya Thamarai || Ilaiyaraaja, S. Janaki || 4:57
|- 2 || Kathalukku Kangalillai || S. P. Balasubrahmanyam, Swarnalatha || 5:00
|- 3 || Mannaiyam Ponnaiyum || S. P. Balasubrahmanyam, S. Janaki || 5:48
|- 4 || Mano || 5:09
|- 5 || Vanamellam Shenbagapoo  (solo)  || S. P. Balasubrahmanyam || 5:11
|- 6 || Vanamellam Shenbagapoo  (duet)  || S. P. Balasubrahmanyam, P. Susheela || 4:21
|- 7 || Thenpandi Cheemai || Gangai Amaran || 4:34
|}

==Reception==
N. Krishnaswamy of The New Indian Express said "Ilayaraja has a few melodious numbers in this film, that sees Karthik emoting well. Directed and photographed by N. K. Viswanathan, the film is one that has possibilities that are wasted because of his preference for the pedestrian". 

==References==
 

==External links==
* 

 
 
 
 
 