Sound of My Voice
{{Infobox film
| name           = Sound of My Voice
| image          = Sound of my Voice poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Zal Batmanglij
| producer       = Hans Ritter Brit Marling Shelley Surpin
| writer         = Zal Batmanglij Brit Marling
| starring       = Christopher Denham Nicole Vicius Brit Marling
| music          = Rostam Batmanglij
| cinematography = Rachel Morrison
| editing        = Tamara Meem
| studio         = Skyscraper Films
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 85 minutes  
| country        = United States
| language       = English
| budget         = $135,000 
| gross          = $408,015 
}}
Sound of My Voice is a 2011 American psychological thriller directed by Zal Batmanglij and starring Christopher Denham, Nicole Vicius and Brit Marling. The plot focuses on two documentary filmmakers who attempt to expose a cult led by a charismatic leader (Marling) who claims to be from the future. The film was written by Batmanglij and Marling. It premiered at the 2011 Sundance Film Festival. It was also selected to close the 2011 SXSW Film Festival. The film was released by Fox Searchlight Pictures on April 27, 2012. 

==Synopsis==
In Los Angeles, substitute schoolteacher Peter and aspiring writer Lorna are a couple in their twenties making a film documentary. Their subject is a small cult led by the mysterious Maggie, whom they plan to expose as a fraud.

When the cult considers Peter and Lorna ready to meet Maggie, they are made to shower thoroughly and dress in white surgical gowns. Then they are driven blindfolded to a secret basement location and received by Klaus, with whom they exchange a distinctive, intricate handshake. They join eight other members and meet Maggie, who uses an oxygen tank and implies that the showering and clothing requirements are to avoid aggravating her illness.

Maggie claims to be a time-traveler from the year 2054. She describes the future as riddled with war, famine and struggle, and has come back to select a special band of chosen people to prepare for what lies ahead. She leads the group in a series of intense psychological exercises and tells them about herself and the future, never proving nor disproving her extraordinary claim. Maggies charismatic manner is powerful, and both Lorna and Peter have moments in which they waver between skepticism and belief. Lorna is especially concerned when she notices that Peter, who was initially adamant that Maggie is a fraud, seems to be intrigued by Maggie and attracted to her.

After several group meetings, Maggie instructs Peter to bring her the eccentric eight-year-old Abigail Pritchett, one of his students. Maggie insists Abigail is her mother, and that Peter and Lorna will be banned from the group if he fails to comply. When Peter admits that he is considering following Maggies orders, Lorna is outraged and accuses him of falling for Maggies deception. After they argue, Lorna is privately approached by Carol, a woman who identifies herself as a Justice Department agent. Carol tells Lorna that Maggie is wanted for a variety of felonies. Lorna agrees to set Maggie up to be captured and to hide this plan from Peter.

Peter arranges for Maggie to meet the student in public, at the LaBrea Tar Pits, during a class field trip. When Maggie meets the little girl, Peter is amazed to see them perform the cults special handshake. Abigail asks how Maggie knew her secret handshake, and Maggie reverently responds: "You taught it to me." Men in police uniforms burst into the room and seize Maggie. As cult members angrily accuse Peter of betraying Maggie, he exchanges a glance with Lorna who smiles slightly, affirming her role in Maggies capture. Abigail asks Peter who Maggie was, and he responds, stricken, that he does not know.

==Cast==
 
* Christopher Denham as Peter Aitken
* Nicole Vicius as Lorna Michaelson
* Brit Marling as Maggie
* Davenia McFadden as Carol Briggs
* Kandice Stroh as Joanne Richard Wharton as Klaus
* Christy Myers as Mel
* Alvin Lam as Lam
* Constance Wu as Christine
* Matthew Carey as Lyle
* Jacob Price as PJ
* David Haley as OShea
* James Urbaniak as Mr. Pritchett
* Avery Pohl as Abigail Pritchett
* Kyle Hacker as Lucas
 

 

==Reception==
Sound of My Voice was named among the Top Indie Films at Festivals in 2011 on IndieWires criticWIRE. 

Manohla Dargis wrote in her review in the New York Times: "Nobody is gutted in Sound of My Voice, a smart, effectively unsettling movie about the need to believe and the hard, cruel arts of persuasion. But over time the men and women who meet in a mysterious house in an anonymous Los Angeles neighborhood – where they shed their clothes and cleanse their bodies in a ritual – are opened up bit by bit, wound by wound, until they’re sobbing and laughing, their insides smeared across the carpet." 

Brent Simon, a film critic, summed up Sound of My Voice as, "a delicate, mesmeric thing that dances darkly along the edges of psychology, religion and science-fiction, raising questions about faith, identity, self-betterment and romantic connection." 

In September 2012, the film won the Octopus d’Or for the best international feature film at the Strasbourg European Fantastic Film Festival. 

In his round-up of 2012s cinematic standouts, Variety magazine|Variety film critic Peter Debruge admitted to having watched Sound of My Voice four times and called it an "ingenious low-budget puzzler." 

==Possible follow-ups==
The film was originally intended to be the first instalment of a trilogy. 

==See also==
 The East, a 2013 American corporate espionage thriller about a private intelligence firm that tries to take down a radical environmental activist collective- which drives their agent into a profound state of moral ambiguity. The film is also directed by Zal Batmanglij, also stars Brit Marling and is also written by Batmanglij and Marling.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 