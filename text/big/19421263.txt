Father's Doing Fine
{{Infobox film
| name           = Fathers Doing Fine
| image_size     = 
| image	=	Fathers Doing Fine FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Henry Cass
| producer       = Victor Skutezky
| writer         = Noel Langley (play)   Anne Burbnaby
| narrator       =  Noel Purcell   Virginia McKenna Philip Green   Harold Smart
| cinematography = Erwin Hillier
| editing        = Edward B. Jarvis
| studio         = Marble Arch Productions
| distributor    = Pathé   Stratford Pictures Corporation
| released       = August 1952
| runtime        = 83 minutes
| country        = United Kingdom English
| budget         = 
| gross          = £127,822 (UK) 
| preceded_by    = 
| followed_by    = 
}} Noel Purcell and Sid James.  It was based on the 1950 play Little Lambs Eat Ivy by Noel Langley.

==Cast==
* Richard Attenborough as Dougall
* Heather Thatcher as Lady Buckering Noel Purcell as Shaughnessy
* George Thorpe as Dr. Drew
* Diane Hart as Doreen
* Susan Stephen as Bicky
* Mary Germaine as Gerda
* Virginia McKenna as Catherine
* Jack Watling as Clifford Magill Peter Hammond as Roly Brian Worth as Wilfred
* Sid James as Taxi Driver
* Ambrosine Phillpotts as Nurse Pynegar
* Wensley Pithey as Police Constable
* Jonathan Field as Zookeeper
* Harry Locke as Father in Zoo

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 