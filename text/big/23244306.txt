Evil Town
{{Infobox film
| name           = Evil Town
| image_size     = 
| image	=	Evil Town FilmPoster.jpeg
| caption        =  Edward Collins Larry Spiegel Peter S. Traynor Mardi Rustam
| producer       = Joan Kasha William D. Sklar Peter S. Traynor
| writer         = Royce D. Applegate Richard Benson Larry Spiegel
| narrator       =  Michele Marsh Christie Houser Hope Summers Lynda Wiesmeier
| music          = Michael Linn
| cinematography = Bob Ioniccio Bill Mann
| editing        = David Blangsted Jess Mancilla Peter Parasheles
| studio         = Production Company: Mars Productions 
| distributor    = New World Pictures Starmaker Video Trans World Entertainment (TWE)
| released       = June 2, 1987
| runtime        = 88 min.
| country        = United States
| language       = English
}} American zombie zombie horror Edward Collins, Mardi Rustam, Larry Spiegel, and Peter S. Traynor. 
 {{cite web
 |url=http://www.tcm.com/tcmdb/title.jsp?stid=503924&category=Full%20Credits
 |title=Evil Town (1987)
 |publisher=Turner Classic Movies
 |accessdate=2009-07-08}}  
 {{cite web
 |url=http://www.citwf.com/film107067.htm
 |title=Evil Town|publisher=Complete Index to World Film
 |accessdate=2009-07-08}}  
 {{cite book
 |others=Jay R. Nash, Stanley Ralph Ross, James J. Mulay, Daniel Curran, Jeffrey H. Wallenfeldt
 |title=The Motion Picture Guide 1988 Annual: The Films of 1987
 |editor=Jay R. Nash, Stanley Ralph Ross
 |publisher=Cinebooks
 |year=1988
 |edition=illustrated
 |page=page 86
 |isbn=0-933997-16-7
 |oclc=9780933997165
 |url=http://books.google.com/books?id=BYkqAAAAYAAJ&q=%22Evil+Town%22,+cult+film,+1987&dq=%22Evil+Town%22,+cult+film,+1987&ei=2uZTSoz0LITOlQTB-piSBw}}  
 {{cite book
 |others=R.R. Bowker Company
 |title=Bowkers Complete Video Directory 2002
 |publisher=R.R. Bowker
 |page=page 458
 |year=2002
 |volume=1
 |isbn=0-8352-4478-4
 |oclc=9780835244787
 |url=http://books.google.com/books?id=KBJlAAAAMAAJ&q=%22Evil+Town%22,+cult+film,+1987&dq=%22Evil+Town%22,+cult+film,+1987&ei=2uZTSoz0LITOlQTB-piSBw}}  Evil Town was the last film with the actor Dean Jagger. 

==Synopsis==
The film depicts an evil scientists (Dean Jagger) campaign to achieve eternal youth, through synthesizing a drug derived from human pituitary fluid.  In extracting the fluid, he creates mindless zombies from the donors. Because the local town residents are in on the plot, to achieve immortality, they help the scientist, by abducting visitors who come through town. 
 {{cite book
 |authors=Mick Martin, Marsha Porter
 |title=Video Movie Guide 1995
 |publisher=Ballantine Books
 |year=1994
 |edition=revised
 |isbn=0-345-39196-9
 |oclc=9780345391964}} 

==Cast==
*James Keach as Dr. Chris Fuller
*Dean Jagger as Dr. Schaeffer
*Robert Walker Jr. as  Mike
*Doria Cook-Nelson as Linda
*Lynda Wiesmeier as Dianne
*Hope Summers as Mrs. Wylie (archive footage) Michele Marsh as Julie
*Christie Houser as  (as Christie Hauser)
*Dabbs Greer Keith Hefner Scott Hunter Jillian Kesner
*Paul McCauley
*Regis Toomey
*Lurene Tuttle
*Noelle Harling
*Greg Finley

==Production== Jillian Kesner and nude scenes with Playboy Playmate Lynda Wiesmeier. 
 {{cite book
 |last=Weldon
 |first=Michael
 |title=The Psychotronic Video guide Macmillan
 |year=1996
 |edition=illustrated
 |page=page 191
 |isbn=0-312-13149-6|oclc=9780312131494
 |url=http://books.google.com/books?id=nhjsnWfFoiAC&pg=PA191&dq=%22Evil+Town%22,+cult+film,+1987&ei=vt1TSouON4jAlQTVivyUBw}} 

===Pre spin-off===
When beginning work on Evil Town in 1984, director Mardi Rustam liked the story enough to make his own version which he released as Evils of the Night in 1985,  
 {{cite book
 |authors=Mick Martin, Marsha Porter
 |title=Video Movie Guide 1991
 |editor=Derrick Bang
 |publisher=Random House 
 |year=1990
 |edition=6, revised
 |isbn=0-345-36945-9
 |oclc=9780345369451}}  two years before the release of Evil Town.

==Reception==
Cavett Binion of All Movie Guide called it a "silly horror film" and noted that it was an assemblage of parts of earlier films, including an unfinished one from the 70s, and that it was "spiced up with some gratuitous nudity courtesy of former Playboy playmate Lynda Wiesmeier."  While remarking that the editors efforts to maintain continuity were comendable, he concluded that "the end result seems hardly worth the effort". 
 {{cite web
 |url=http://www.allmovie.com/work/evil-town-16260
 |title=Evil Town 1987
 |last=Binion
 |first=Cavett
 |publisher=All Movie Guide
 |accessdate=2009-07-08}} 

==Release==
The film was scheduled for release on June 3, 1987, but due to the high level of anticipation for the movie, many theaters began showing it on the evening of June 2, 1987 . It was released in USA on VHS in November 1987. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 