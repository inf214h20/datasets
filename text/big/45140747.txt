Bedi Bandavalu
{{Infobox film 
| name           = Bedi Bandavalu
| image          =  
| caption        = 
| director       = C. Srinivasan
| producer       = T N Srinivasan
| writer         = Smt Neeladevi (Based on Novel)
| screenplay     = 
| starring       = Kalyan Kumar Dwarakish Rama Rao Rajaram Giriyan
| music          = R. Sudarshanam
| cinematography = Vijaya Nanjappa
| editing        = G Veluswamy
| studio         = Srinivas Art Productions
| distributor    = Srinivas Art Productions
| released       =  
| runtime        = 154 min
| country        = India Kannada
}}
 1968 Cinema Indian Kannada Kannada film, directed by C. Srinivasan and produced by T N Srinivasan. The film stars Kalyan Kumar, Dwarakish, Rama Rao and Rajaram Giriyan in lead roles. The film had musical score by R. Sudarshanam.  

==Cast==
 
*Kalyan Kumar
*Dwarakish
*Rama Rao
*Rajaram Giriyan
*Shyamsundar
*Sahyadri
*Master Gopal
*Srirangamurthy
*Lakshman Rao
*Venkataram
*Chandrakala
*Shailashree
*Kamalamma
*Malathamma
*Indrani
*A. Lalitha
*Radha
*Rama
*Kavitha
*Baby Sunanda
*Baby Roja Ramani
*Baby Padmashree
*Baby Prema
*Baby Jayanthi
*B. Jayamma in Guest Appearance
*Ranga in Guest Appearance
 

==Soundtrack==
The music was composed by R. Sudarsanam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || R. N. Jayagopal || 03.42
|- Susheela || R. N. Jayagopal || 03.30
|}

==References==
 

==External links==
*  
*  

 
 
 


 