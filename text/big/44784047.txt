Ibbara Naduve Muddina Aata
{{Infobox film|
| name = Ibbara Naduve Muddina Aata
| image = 
| caption =
| director = Relangi Narasimha Rao
| based on =  Kasthuri
| producer = T. M. Venkataswamy
| music = Sadhu Kokila
| cinematography = Nagendra Kumar
| editing = Shashikumar
| studio = S V Productions
| released =  
| runtime = 117 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada comedy comedy drama Kasthuri and Swarna in the lead roles.  The films score and soundtrack is composed by Sadhu Kokila.

== Cast ==
* Shivarajkumar 
* Raghavendra Rajkumar Kasthuri
* Swarna Tara
* Srinivasa Murthy
* Mukhyamantri Chandru
* Bank Janardhan
* Umashri
* B. V. Radha
* Girija Lokesh
* Kunigal Nagabhushan

== Soundtrack ==
The soundtrack of the film was composed by Sadhu Kokila.

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Aao Pyar Kare
| extra1 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics1 = 
| length1 = 
| title2 = Kattiruve Kacchittu
| extra2 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics2 = 
| length2 = 
| title3 = Minichinatha Kannavanu
| extra3 =  K. S. Chithra
| lyrics3 = 
| length3 = 
| title4 = O Mama
| extra4 = S. P. Balasubrahmanyam 
| lyrics4 = 
| length4 = 
}}

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 

 