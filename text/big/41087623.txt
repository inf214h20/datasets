Cuban Fury
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Cuban Fury
| image          = Cuban Fury.jpg
| alt            = 
| caption        = UK theatrical release poster James Griffiths
| producer       = James Biddle Nira Park
| writer         = Jon Brown
| starring       = Nick Frost Rashida Jones Chris ODowd Olivia Colman
| music          = Daniel Pemberton Dick Pope
| editing        = Jonathan Amos Film4
| distributor    = StudioCanal
| released       =  
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
| budget         = £1 million 
| gross          = £3,286,786 
}}

Cuban Fury is a 2014 British romantic comedy film starring Nick Frost, Rashida Jones and Chris ODowd.   

==Plot== salsa champion Bruce Garrett (Nick Frost) is now an engineer. Bruce gave up dancing after he was brutally bullied by older boys.  When he finds out that his new boss, Julia, (Rashida Jones) is passionate about salsa dancing, he decides that the only way he can win her over is by re-mastering the art of dance. He seeks out his old teacher Ron (Ian McShane), who forces him to confront the reasons he quit dancing in the first place.  He struggles with low self-esteem, as well as a bullying coworker and rival, Drew (Chris ODowd), who constantly dominates the attention of their boss Julia who theyre both interested in seducing.  With the help of his salsa classmates, teacher, and his former dancing partner, his sister Sam (Olivia Colman), Bruce gets up the courage to relearn all his rusty dance steps and to recapture his lost "corazón" (heart), not only for the dance but for his life. When ready, his friends convince him to enter the local nightclubs salsa dance competition, with the idea that hell invite Julia to be his dance partner. But when he goes round to hers to ask her out to the dance, he is tricked into believing that hes interrupting an intimate evening shes spending with Drew, so leaves before asking, disillusioned. Julia, meanwhile, discovers what Drew is up to and outright rejects his advances, then kicks him out whilst threatening his position at work. Julia follows Bruce to the nightclub, where hes been doing quite well with his old partner (his sister) and an old routine, and is about to enter the final heat/round of the competition. When he notices Julia had followed him to the club, hes elated and finally plucks up the courage to ask her to dance. They dance the last round of the competition, where Bruce goes on to lose the competition, but regains his true self and finally wins the girl...

==Cast==
* Nick Frost as Bruce Garrett
* Rashida Jones as Julia
* Chris ODowd as Drew
* Olivia Colman as Sam, Bruces sister.
* Ian McShane as Ron
* Wendi McLendon-Covey as Carly 
* Alexandra Roach as Helen
* Rory Kinnear as Gary
* Kayvan Novak as Bejan
* Simon Pegg as Ginger Mondeo driver (cameo appearance|cameo)
* The Cuban Brothers as MCs during dance contest

==Reception==

===Critical response===
The film has a 51% rating on Rotten Tomatoes and a score of 52 out of 100 at Metacritic which indicates "average reviews". 

==References==
 

==External links==
*  – official site
*  – official site
* 
* 
* 


 
 
 
 
 
 
 
 
 
 
 


 