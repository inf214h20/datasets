Cash (2010 film)
 
{{Infobox film
| name           = Cash
| image          = Cash.jpg
| caption        = Promotional film poster
| director       = Stephen Milburn Anderson
| producer       = Naveen Chathappuram Stephen Milburn Anderson Prema Thekkek
| writer         = Stephen Milburn Anderson Mike Starr Michael Mantell Glenn Plummer Antony Thekkek
| music          = Jesse Voccia
| cinematography = John R. Leonetti Robert Primes
| editing        = Mark Conte
| distributor    = Roadside Attractions
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
}}
 independent crime film|crime-thriller film directed by Stephen Milburn Anderson that stars Sean Bean and Chris Hemsworth.

== Plot ==
When a suitcase is thrown out of a car which is involved in a car chase and lands on the car of Sam Phelan (Chris Hemsworth), he initially curses his luck. But then he finds the suitcase is loaded with money. After bringing the money to his home, he convinces his wife Leslie (Victoria Profeta), that they should use the money.

Meanwhile Pyke Kubic (Sean Bean) visits his twin brother Reese (also Bean) in jail, who tells him that he threw a suitcase with about half a million dollars from his car, when he was being chased by the police. Pyke decides to go and find the money.

After Pyke finds the Phelans, he asks for the money back. They return whatever money they have left after they bought a new car, furniture and other minor expenses. Pyke forces the Phelans to rob stores, in order to get him the amount of money the Phelans spent, which they do, reluctantly at first, but later with more of a taste for it. After robbing over ten stores in the course of a few days, they are still short on the money, and Sam proposes to rob a bank. There he changes the gun without bullets which Pyke gave him with that of the guards, and Leslie shoots Pyke in a struggle. Pykes car and his corpse are sent to a junkyard, where a worker is bribed into destroying the car and the body.

The Phelans again keep the money, but return all they stole, adding damages for people who got hurt in the course of the robberies. Unknown to them, Reese, who originally threw the cash out of his car, has been released from prison.

== Cast ==
* Sean Bean as Pyke Kubic / Reese Kubic
* Chris Hemsworth as Sam Phelan
* Victoria Profeta as Leslie Phelan Mike Starr as Melvin Goldberg
* Glenn Plummer as Glen the Plumber
* Michael Mantell as Mr. Dale
* Antony Thekkek as Bahadurjit Tejeenderpeet Singh
* Tim Kazurinsky as Chunky Chicken Salesman
* Robert C. Goodwin as Bartender

== Soundtrack ==
The film features music written and recorded by musician Jim Bianco from his album Sing (Jim Bianco album)|Sing. The film opens with "Ive Got a Thing for You" and closes with "To Hell With the Devil." During the film, "Get On" and the remix of "Ive Got a Thing for You" are used.

== External links ==
*  
*  
*  
*  

 
 
 
 
 