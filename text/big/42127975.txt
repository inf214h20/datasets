Wasteland (film)
 Wasteland}}
 
 
}}
{{Infobox film
| name           = Wasteland
| image          = 
| alt            = 
| caption        = 
| genre          = Drama
| director       = Graham Travis Patrick Collins
| writer         = 
| starring       = Lily Carter Lily LaBeau Liza Del Sierra Sarah Shevon
| music          = 
| cinematography = Alex Ladd
| editing        = Graham Travis
| studio         = 
| distributor    = Elegant Angel
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Wasteland is a 2012 pornographic film. It stars Lily Carter and Lily LaBeau in the lead roles, and is written and directed by Graham Travis. The movie is highly praised in the adult business and has won several awards including the AVN Award for Best Movie in 2013.

==Plot==
Anna (Lily Carter) and Jacky (Lily LaBeau) were best friends in high school in Tucson, Arizona but havent seen each other in five years. Anna is shy, awkward, and withdrawn while Jacky is outgoing, hedonistic, but unsettled. They both formed an unlikely friendship because they both felt different, but it was this feeling that drove them apart as Jacky moved to Los Angeles. Anna travels by bus to Los Angeles to visit her for a day and the two spend the day together. The two head to a nightclub later that night where Jacky has sex with a man in a bathroom stall, which Anna witnesses. Later on Jacky performs a handjob on a random man in an alley. She gets Anna to do the same even though she is uncomfortable with the situation.

When they return home Jacky apologizes. Anna begins kissing her and the two engage in sexual acts. They then pass the time by playing Go Fish, drinking Tequila and slow-dancing. They discuss how they missed each other but its clear that Jacky had a much easier time moving on with her life. Anna says that she would always be Jackys friend, but Jacky dismisses that claim as unrealistic. Anna is put off. The two wind up heading up to the bedroom, making love again. Jacky expresses that she sometimes feels as if something is wrong with her.

The two women spend time in a pool, but afterwards their next conversation becomes heated. Anna admits that she was somewhat pleased when Jacky moved away due to her volatility. Jacky is angry with Anna for not talking to her for five years and accuses her of not being around when Jacky needed her.

Via flashback, it is shown that Jacky previously had sex with someone that Anna was interested in, which pained Anna. Jacky asks Anna why she came to visit her. Its revealed that Annas parents died in a fire when she was 3 years old and was raised by her grandmother ever since, but she is now dying and Anna is afraid that once she dies, shell be alone in the world. Jacky is the only person in the world she has really connected with.

Jacky takes Anna to a sex club where she works. Jacky leaves Anna to her own devices at first, telling her to come find her in a few minutes. Anna has a torrid encounter with a dancer (Liza del Sierra), then finds Jacky in a room full of men, where she participates in a gang bang.
 Union Station.

==Cast==
* Lily Carter as Anna
* Lily Labeau as Jacky
* Ellis McIntyre as Alice
* Barbara as Grandmother
* Alec Knight as Steve
* Manuel Ferrara as Manuel Ferrara
* Carlos Carrera as George
* Xander Corvus as Eric

also featuring:
Liza Del Sierra, Sarah Shevon, Proxy Paige, Tiffany Doll, Sparky Sin Claire, Mick Blue, David Perry, Sparky, Toni Ribas, Ramon Nomar, Eric John, Brian Street Team

==Reception==
The drama was very well received and earned a number of awards due to its stimulating mix of long close-ups and sweeping exterior shots that convey the pulse of the city at night    

==Awards and nominations==
 
{| class="infobox" style="width: 25em; text-align: left; font-size: 90%; vertical-align: middle;"
|+  Accolades received by Wasteland 
|-
|-
| colspan=3 |
{| class="collapsible collapsed" width=100%
! colspan=3 style="background-color: #D9E8FF; text-align: center;" | Awards & nominations
|-
|-  style="background:#d9e8ff; text-align:center;"
| style="text-align:center;" | Award
|  style="text-align:center; background:#cec; text-size:0.9em; width:50px;"| Won
|  style="text-align:center; background:#fcd; text-size:0.9em; width:50px;"| Nominated
|-
| style="text-align:center;"|
;AVN Awards
| 
| 
|-
| style="text-align:center;"|
;Erotic Lounge Awards
| 
| 
|-
| style="text-align:center;"|
;Los Angeles International Underground Film Festival Awards
| 
| 
|-
| style="text-align:center;"|
;NightMoves Awards
| 
| 
|-
| style="text-align:center;"|
;Orgazmik Awards
| 
| 
|-
| style="text-align:center;"|
;TLA RAW Awards
| 
| 
|-
| style="text-align:center;"|
;XBIZ Awards
| 
| 
|-
| style="text-align:center;"|
;XCritic Fans Choice Awards
| 
| 
|-
| style="text-align:center;"|
;XRCO Awards
| 
| 
|}
|- style="background:#d9e8ff;"
| style="text-align:center;" colspan="3"|
;Total number of wins and nominations
|-
| 
| 
| 
|- style="background:#d9e8ff;" References
|}
{| class="wikitable"
|-
! Year
! Ceremony
! Result
! Award
! Recipient(s)
|-
|rowspan="3"| 2012
|rowspan="2"| Los Angeles International Underground Film Festival Award
|   Best Narrative Feature   
|  
|-
|   Best Actress  Lily Carter
|-
| Orgazmik Award
|   Best Film (Feature) 
|  
|-
|rowspan="35"| 2013
|rowspan="16"| AVN Award
|   Best Actress    Lily Carter
|-
|   Best Actress    Lily LaBeau
|-
|   Best Cinematography  Alex Ladd, Carlos D. & Mason
|-
|   Best Director – Feature  Graham Travis
|-
|   Best Drama 
|  
|-
|   Best DVD Extras 
|  
|-
|   Best Editing  Graham Travis
|-
|   Best Girl/Girl Sex Scene  Lily Carter & Lily LaBeau
|-
|   Best Group Sex Scene  Lily Carter, Lily LaBeau, Mick Blue, David Perry, Ramon Nomar & Toni Ribas
|-
|   Best Music Soundtrack 
|  
|-
|   Best Oral Sex Scene  Lily Carter & Lily LaBeau
|-
|   Best Original Song for "Hear Me Out"  Erlisha Tamplin
|-
|   Best Overall Marketing Campaign – Individual Project 
|  
|-
|   Best Packaging 
|  
|-
|   Best Screenplay  Graham Travis
|-
|   Movie of the Year 
|  
|-
| Erotic Lounge Award
|   Best Feature Film 
|  
|-
| NightMoves Award
|   Best Feature Production (Editors Choice) 
|  
|-
| TLA RAW Award
|   Best Porn Movie 
|  
|-
|rowspan="10"| XBIZ Award
|   Feature Movie of the Year   
|  
|-
|   Director of the Year - Feature Release  Graham Travis
|-
|   XBIZ Award Best Actress&mdash;Feature Movie  Lily Carter
|-
|   Best Actress&mdash;Feature Movie  Lily LaBeau
|-
|   Best Scene - Feature Movie  Lily Carter, Lily LaBeau, Mick Blue, Ramon Nomar, David Perry & Toni Ribas
|-
|   Screenplay of the Year    Graham Travis
|-
|   Best Cinematography  Alex Ladd, Carlos Dee & Mason
|-
|   Best Art Direction 
|  
|-
|   Best Music 
|  
|-
|   Best Editing  Graham Travis
|-
|rowspan="3"| XCritic Fans Choice Award
|   Feature Movie of the Year   
|  
|-
|   Best Director: Feature  Graham Travis
|-
|   Best Actress: Feature  Lily Carter
|-
|rowspan="3"| XRCO Award
|   Best Release   
|  
|-
|   Best Actress  Lily Carter
|-
|   Best Actress  Lily LaBeau
|}

==References==
 

==External links==
* 
* 
* 

 
 
 
 