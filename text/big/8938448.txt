Ángel, la Diva y Yo
{{Infobox film
| name           = Ángel, la diva y yo
| image          = Angelladivayo.jpg
| alt            = 
| caption        = Screenshot from the film
| director       = Pablo Nisenson
| producer       = Ricardo Belén Gastón Ocampo
| screenplay     = Pablo Nisenson José Pablo Feinmann
| narrator       =
| starring       = José Soriano Esther Goris
| music          = Alberto Quercia Lagos
| cinematography = Eduardo Pinto
| editing        = Sergio Zottola
| studio         = INCAA LAFS & TIERS
| distributor    = Primer Plano Film Group
| released       =  
| runtime        = 93 minutes
| country        = Argentina Spanish
| budget         = 
| gross          = 
}}
Ángel, la diva y yo is a 1999 Argentine drama film directed Pablo Nisenson, and written by Nisenson and José Pablo Feinmann.  The film stars José Soriano, Esther Goris, and others.  The film was partly funded by INCAA.

==Plot==
In Buenos Aires at the end of the century, a documentarian, anxious about his future, is about to realize his "last act" when a mysterious package is delivered to him: a rusty 35&nbsp;mm film canister, an envelope stuffed with cash, and a request that he rescue the greatest Argentine film director in history from oblivion. Together with his crew, the filmmaker tries to piece together the puzzle.

==Cast==
 
 
* José Soriano as Angel Ferreyros
* Esther Goris as Diva
* Boy Olmi as Julián Armendáriz
* Florencia Peña as Ana
* Ricardo Sendra as Miguel
* Osvaldo Bayer as Escritor
* Max Berliner as Merayo
* Alberto Busaid as Funes
* Claudio España as Crítico
* José Pablo Feinmann as Filósofo
 
* Miguel Fontes as Payaso
* Celina Fux as Vecina
* Patricia Hart as Santa
* Diana Ingro as Actriz famosa
* Augusto Larreta as Loco
* Miguel Padilla as Policia
* Jorge Román
* Salvador Sammaritano as Esayista
* Andrés Turnes as Empresario
* Emilio Vieyra as Mendigo
 

==Distribution==
The film was first presented at the Mar del Plata Film Festival in November 1999 and was acclaimed as one of the best Ibero-American films of that year. It opened in wide in Argentina on September 21, 2000.

==Awards==
Wins
* Mar del Plata Film Festival: Best Ibero-American Film, Pablo Nisenson; Best Screenplay, Pablo Nisenson and José Pablo Feinmann; 1999.

Nominations
* Argentine Film Critics Association Awards: Silver Condor; Best Original Screenplay, José Pablo Feinmann and Pablo Nisenson; 2001.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*  

 
 
 
 
 
 


 