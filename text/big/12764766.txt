Tom and Jerry: Blast Off to Mars
{{Infobox film
| name           = Tom and Jerry: Blast Off to Mars
| image          = Tom and Jerry Blast Off to Mars cover.jpg
| caption        = 
| director       = Bill Kopp
| producer       = Tom Minton
| writer         = Bill Kopp
| based on       = Tom and Jerry by William Hanna & Joseph Barbera Steven Bernstein Billy West
| studio         = Turner Entertainment Toon City Warner Bros. Animation
| distributor    = Warner Home Video
| released       =   
| country        = United States
| runtime        = 70 minutes
| language       = English
| budget         = 
| gross          = 
}}
Tom and Jerry: Blast Off to Mars is a 2005 Tom and Jerry outer-space-themed direct-to-video film produced by Warner Bros. Animation. It was released on DVD and VHS on January 18, 2005,    and on Blu-ray on October 16, 2012. 

==Plot== Billy West) are going to Mars. In the process, Jerry and Tom are caught during the speech (first misunderstood as aliens due to Tom getting hit by green paint backstage), and they try to capture them, but only Tom is caught and thrown out. During the dehydrated food testing, Jerry knocks over a cup in the process, and all the food comes to life, and as a result, the food goes all over the place in an explosion. Soon the staff tries to catch Jerry, but figure only Tom can catch him, so they bring him back to the base and give him a mission to "eliminate" Jerry, and at the end of the chase, they are chased into a rocket ending up at Mars.  They discover a giant black wall, similar to the black monolith in the Space Odyssey series. A green female alien named Peep (voiced by Kathryn Fiore) arrives with an alien dog, Ubu and two more Martians arrive. Jerry is mistaken for the “Great Gloop”. After much calamity and a discovery that Jerry is not the Gloop, Tom, Jerry and Peep (since she knows this was all a misunderstanding and trusted Jerry from the very beginning)  steal a flying saucer, so they can get back to Earth and warn everyone about a potential attack by the Martians. They manage to stop them, but the “Invince-a-tron” eventually arrives on Earth and begins to suck everyone up with its vacuum. Tom, Jerry, and Peep stop Invince-a-tron, using a bone to get Spike into his brain and make it malfunction, much to Spikes nervous breakdown in the end. At the end they are thanked and awarded with a Hummer by Arnold Schwarzenegger (uncredited) as the U.S. president. Before they could even drive it, they are attacked by the Invince-a-tron again but this time controlled by Spike (mad for the destruction of his precious bone) who chases after them. Peep comes with the ship and rescues Jerry, but leaves Tom behind (for no apparent reason). Peep kisses Jerry with him being blushed as the iris closes in. At the ending scene, Biff and Buzz are seen cleaning the mess as punishment for lying that there is no life on Mars, soon they start to argue about it and fight as Tom is being chased by the Spike controlled Invince-a-tron in the background.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 