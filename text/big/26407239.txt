Rehmat Ali
{{Infobox film
| name           = Rehmat Ali
| image          = Rehmat_Ali_New.jpg
| alt            =  
| caption        = CD Cover of Rehmat Ali
| director       = Partho Ghosh
| producer       = Raj Behl Geeta Behl
| writer         = 
| screenplay     = 
| story          = 
| starring       =  
| music          = Bappi Lahiri
| cinematography = N Natarajan Subramaniam
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 145 minutes
| country        = India Bengali
| Rs 2 Crores
| gross          = 
}} 
 2010 Cinema Indian feature directed by Partho Ghosh for producers Raj Behl and Geeta Behl, starring Mithun Chakraborty, Rituparna Sengupta and Rupa Ganguly in lead roles.  It marked the return of Mithun Chakraborty to the Bengali film industry after a long gap. It is a remake of the 1997 Bollywood film Ghulam-e-Mustafa.   

== Cast ==
*Mithun Chakraborty
*Rituparna Sengupta
*Rupa Ganguly
*Biswajit Chakraborty
*Rajatava Dutta
*Biswanath
*Jayanta Dutta Burman
*Sanchita Nandi

==Songs==
Lyrics have been written by Lipi and Bijoy Bhakat.
 Shaan
*"Moja Dekhabo" – Bappi Lahiri
*"Saiyaan Bina Ghar Suna" – Sunidhi Chauhan
*"Tumi Amar Ami Tomar" – Shaan, Bappi Lahiri, Alka Yagnik
*"Joy Kali Joy Kali" – Sadhana Sargam, Chinton
*"Saiyaan Bina Ghar Suna (Remix)" – Madhushree

==References==
 

 
 
 
 

 