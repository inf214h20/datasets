The Two Waters
{{Infobox film
| name = A dos aguas
| image = Adosaguas.jpg
| caption = Film Poster
| director = Carlos Olguin-Trelawny
| producer = Carlos Olguin-Trelawny Jorge Estrada Mora
| writer = Martha Gavensky / Carlos Olguin
| starring = Miguel Ángel Solá and Bárbara Mújica
| music = Rodolfo Mederos
| cinematography = Rodolfo Denevi
| editing = Armando Blanco
| distributor = Metropolis Films, Zurich, CH
| released = 28 April 1988
| runtime = 78 minutes
| country = Argentina
| language = Spanish
| budget =
| followed_by =
}} 1988 cinema Argentine drama film directed by Carlos Olguin-Trelawny and written  Martha Gavensky starring Miguel Ángel Solá and Bárbara Mújica.

==Plot==
Buenos Aires, 1983. Christmas Eve. Rey (Miguel Ángel Solá) and Isabel (Bárbara Mujica), two old college friends, bump into each other at a restaurant. It has been fifteen years since they last saw each other. Isabel has just returned from exile; Rey had just wanted to be alone, wrestling with his own personal demons.

This by-chance encounter with Isabel takes Rey back to his college days when he was secretly in love with her. "A dos aguas" (The Entire Life) is a look at the effects of years spent living under a brutal dictatorship and peoples desperate rush to recover lost time. Perhaps more importantly, it depicts the pain of being an orphan in both a physical and spiritual sense.

==Release and acclaim==
Praised by critics as the first post-modern Latin American film.  
"A dos aguas" premiered on April 28th, 1988 in Argentina and in various European countries most notably France in October 1988.

"A dos aguas" was produced by Avica Producciones and Jorge Estrada Mora Producciones (JEMPSA) and distributed worldwide by Metropolis Films (Zürich, CH).

Director Carlos Olguin-Trelawny won a Special Mention at the 40th Locarno International Film Festival of 1987.

==Cast==
* Miguel Ángel Solá as Rey
* Bárbara Mujica as Isabel
* Sandra Ballesteros as "La mujer felina"
* Rubén Ballester
* Jorge Baza de Candia
* Aldo Braga as "Patricio/Weintraub"
* Mónica Lacoste
* Cipe Lincovsky as "María"
* Miguel Ruiz Díaz
* Jorge Sassi as "Reys Alter ego"
* Osvaldo Tesser as "Reys father"
* Antonio Ugo as "Bartender"

==External links==
*  

 
 
 
 
 
 
 