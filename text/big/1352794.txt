The Chronicles of Riddick: Dark Fury
 
{{Infobox film
| name = The Chronicles of Riddick: Dark Fury
| image = Dark fury cover.jpg
| caption = 
| director = Peter Chung
| producer = John Kafka Jae Y. Moh
| writer = Characters: Jim Wheat Ken Wheat Story:  
| starring = Vin Diesel Rhiana Griffith Keith David Nick Chinlund Tress MacNeille Roger L. Jackson
| music = 
| cinematography = 
| editing = 
| distributor = Universal Studios Home Entertainment
| released = June 15, 2004
| runtime = 35 minutes
| language = English
| budget = 
}}
 science fiction Richard B. Riddick.

==Plot== Pitch Black, Riddick, Jack, and the Imam are picked up by a Mercenary spacecraft. Although Riddick attempts to conceal his identity from the mercenaries by impersonating William J. Johns (in Pitch Black) over the intercom, they quickly voice-print and identify him.

Captured by the mercenaries, the trio of survivors discover that their captors have unusual plans for them.  The ships owner, Antonia Chillingsworth (Tress MacNeille), collects criminals, whom she freezes and keeps as statues that are, in her view, art. Although the criminals are frozen, they are alive and conscious.  To her Riddick is the ultimate "masterpiece" for her collection. Riddick, Jack, and Imam must fight their way through the army of human and alien creatures at her disposal or they will meet a fate crueler than death.

Riddick is pursued much of the story by Toombs and his mercs/bounty hunters.  Mercs are kept in suspended animation until they are needed. They are released to confront Riddick and company aboard the ship.

Jack has important character development, as she discovers her violent side by shooting the ships owner just before she can kill Riddick. This discovery is clearly a source of worry for Riddick and Imam as the three escape from the mercenary ship. Riddick decides to deliver both Jack and Imam to New Mecca, where theyll be safe.

==Cast== Richard B. Riddick
* Rhiana Griffith as Jack
* Keith David as Imam Abu al-Walid
* Roger L. Jackson as Junner
* Tress MacNeille as Antonia Chillingsworth
* Nick Chinlund as Toombs
* Dwight Schultz as Skiff A.I.
* Sarge as Escort Merc
* Julia Fletcher as Merc Squad Leader
* Hedy Burress as Lab Tech
* Andrew Philpot as Tech
* Rick Gomez as Lead Merc

==Critical Response==
Dark Fury received mixed to negative reviews.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 