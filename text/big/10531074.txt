BraveStarr: The Movie
{{Infobox film| name =BraveStarr: The Movie
| image =BraveStarr The Movie DVD cover.jpg
| caption =
| producer = Lou Scheimer
| writer = Bob Forward Steve Hayes
| starring = Charlie Adler Susan Blu Pat Fraley Ed Gilbert Alan Oppenheimer
| director = Tom Tataranowicz
| studio = Filmation
| distributor = Taurus Entertainment
| released =March 18, 1988
| runtime = 91 min. English
| music = Frank W. Becker
| editing = Ludmilla P. Saskova
| budget =
| box office =
| preceded_by =
| followed_by =
}}

BraveStarr: The Movie (released in Europe as BraveStarr: The Legend) is an animated  -based European version of the movie has been released to Region 1 DVD as a bonus feature in the July 3, 2007 release of The Best of BraveStarr from BCI Eclipse.  The movie received its own single DVD release on May 6, 2008.

Unlike The Secret of the Sword (which was later edited into the first five She-Ra episodes), the BraveStarr movie was produced and released following the conclusion of the TV series.
 Happily Ever After, did not premiere until 1993.

==Plot==
The final Filmation show got its start with this little-seen feature. Far out in space on New Texas, a single marshal protects a frontier people from the evil machinations of Stampede and his lackey, Tex Hex.

==Cast==

*Charlie Adler as Deputy Fuzz / Tex Hex
*Susan Blu	as Judge J.B.
*Pat Fraley	as Marshall Bravestarr / Thunder Stick
*Ed Gilbert	as Shaman / Thirty-thirty
*Alan Oppenheimer as Stampede / Outlaw Skuzz

==Reception==

The movie gained a positive response from critics despite flopping at the box office.  

==References==
*Beck, Jerry (2005), pp.&nbsp;41–2. The Animated Movie Guide. ISBN 1-55652-591-5. Chicago Reader Press. Accessed April 8, 2007.

== Notes ==
 

==External links==
* 
*  
* 

 
 
 
 
 
 
 
 
 
 
 
 


 