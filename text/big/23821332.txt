Chimère (film)
 
{{Infobox Film
| name           = Chimère
| image          = 
| image_size     = 
| caption        = 
| director       = Claire Devers
| producer       = Jean Achache Philippe Carcassonne
| writer         = Claire Devers Arlette Langmann
| narrator       = 
| starring       = Béatrice Dalle
| music          = 
| cinematography = Renato Berta
| editing        = Hélène Viard
| distributor    = 
| released       = 31 May, 1989
| runtime        = 94 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Chimère is a 1989 French drama film directed by Claire Devers. It was entered into the 1989 Cannes Film Festival.   

==Cast==
* Béatrice Dalle - Alice
* Wadeck Stanczak - Léo
* Francis Frappat - Fred
* Julie Bataille - Mimi
* Adriana Asti - La mère dAlice
* Pierre Grunstein - Le père dAlice
* Christophe Odent - Paul
* Toni Cecchinato - Gino
* Robert Deslandes - Le chasseur
* Isabelle Candelier - La collègue au centre météo
* Marilyne Even - La médecin
* Isabelle Renauld - Linfirmière

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 