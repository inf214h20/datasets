Inescapable (film)
 
{{Infobox film
| name           = Inescapable
| image          = Inescapable -- movie poster.jpg
| image_size     = 220
| alt            = 
| caption        = Movie poster
| director       = Ruba Nadda
| producer       = 
| writer         = Ruba Nadda
| starring       = Alexander Siddig Marisa Tomei Joshua Jackson Oded Fehr
| music          = Geo Höhn Jim Petrak
| cinematography = Luc Montpellier
| editing        = Teresa Hannigan
| studio         = Foundry Films Out of Africa Entertainment
| distributor    = Alliance Films   IFC Films   Myriad Pictures
| released       =   
| runtime        = 93 minutes   
| country        = Canada 
| language       = {{Plainlist|
*English
*Arabic}} CAD $4,000,000 (estimated) 
| gross          = $4,327 
}}
 drama thriller thriller film written and directed by Ruba Nadda, starring Alexander Siddig, Marisa Tomei, and Joshua Jackson about a former Syrian intelligence officer Adib (Alexander Siddig) who becomes embroiled in a cat and mouse chase in locating his photographer daughter arrested on suspicion of being an Israeli spy while visiting Damascus while evading capture by corrupt government agents.

==Synopsis==
 
Adib (Alexander Siddig), was an officer in the Syrian military police twenty-five years ago, when he suddenly defected to Canada under suspicious circumstances, abandoning his fiancee Fatima (Marisa Tomei). With his wife, two daughters, and a steady job in Toronto, Adib is confident hes put his past behind him. But when his daughter Muna suddenly disappears in Damascus, his past threatens to violently catch up to him. Adib teams up with a Canadian emissary (Joshua Jackson), Adib evades former colleagues that are pursuing him. Both a mystery drama and a spy thriller, Inescapable is an action-packed thriller driven by a fathers selfless dedication to his daughter. 

==Cast==
* Alexander Siddig as Adib Abdel Kareem
* Joshua Jackson as Paul
* Marisa Tomei as Fatima
* Oded Fehr as Sayid
* Saad Siddiqui as Halim
* Fadia Nadda as Lingerie Girl
* Bonnie Lee Bouman as Emily

===Release===
The film premiered on September 11, 2012 at the Toronto International Film Festival.

===Critical reception===
The film received negative reviews by critics. It holds a 19% rating on Rotten Tomatoes based on 21 reviews and reports a rating average of 4.2 out of 10  At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 37 based on 11 reviews, indicating "generally unfavorable reviews". 

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 