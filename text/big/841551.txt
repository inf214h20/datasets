Invasion of the Bee Girls
{{Infobox film
| name =Invasion of the Bee Girls
| image    = Invasion of the bee girls.jpg
| caption  = Theatrical release poster
| director = Denis Sanders
| producer =
| writer   = Nicholas Meyer Sylvia Schneble William Smith Victoria Vetri Anitra Ford Charles Bernstein
| cinematography = Gary Graver
| editing  = Dimension Pictures
| released =  
| runtime  = 85 min
| language = English
| country = United States
| budget   =
}}

Invasion of the Bee Girls (UK video title: Graveyard Tramps) is a 1973 science fiction film. {{cite book
 | author=The Staff and Friends of Scarecrow Video
 | year=2004 | title=The Scarecrow Movie Guide
 | page=732 | publisher=Sasquatch Books William Smith, Anitra Ford and Victoria Vetri. Meyer almost didnt put his name to the project after he saw it but was later convinced by his manager at the time.

==Synopsis==
The premise of the movie is that a mad scientist (played by Anitra Ford) has created an army of beauties who seduce men to death.
One by one the male victims are killed before the local police catch on to the plans of the infested females.

==Plot== congestive heart failure caused by sexual exhaustion.

Faced with a rapidly escalating body count, the local sheriff, Captain Peters, holds a town meeting at which the laboratory’s leading sex researcher, Henry Murger, urges the town populace to practice sexual abstinence – an idea greeted with derision by the locals. Neil and Julie arrange a meeting with Murger afterwards to discuss his theories as to the cause of his deaths, only to see him chased down and run over by a car with an unseen driver. While investigating Murger’s home in search of clues, Neil discovers a secret room concealing sexual paraphernalia and Murger’s gay lover, Joe, who informs Neil that he saw Murger driving off with an unknown woman prior to his death.
 entomologist working on bees. Though described by the men as an “iceberg,” she flirts with Kline and invites him over for dinner. That night, as they engage in sex, Kline suffers a fatal thrombosis and Harris reveals black compound eyes suggesting that she is more than she appears.

 Meanwhile, convinced there is a similar pattern at work, Neil begins studying the sexual patterns of insects. Seeking information about the mating habits of honeybee|bees, he interrogates Dr. Harris at her lab; when he departs, she resumes her project – the transformation of Kline’s wife Nora into a Bee Girl through a process of controlled mutation. With the aid of other previously mutated women, she cocoons Nora and places her in a chamber where she is swarmed by mutated bees; when she emerges, she is bombarded with radiation and awakens with the same black compound eyes and drive to mate that the other transformed women possess. The next morning, Herb Kline’s body is discovered; when Captain Peters goes to inform Nora, she attempts unsuccessfully to seduce him.

Having worked out much of what has taken place, Neil summons the remaining department chairs and presents his theory. Though the geneticist, Stan Williams, scorns the idea, the head of the diagnostics department, Aldo Ferrara, is more receptive. At Grubowskys funeral, Neil and Julie bring a radiation detector, which picks up the gamma radiation coming from the women in attendance who have been subjected to the treatment. Aware that they have been detected, the transformed women move to eliminate the remaining scientists; Williams is killed by his now-mutated wife and Ferrara dies during a visit by Dr. Harris, who then lures Julie to her lab to be transformed.

When Neil discovers Ferrara’s body, he realizes what the women are up to. Racing to the lab, he interrupts the women as they begin the process of turning Julie into one of their number. With Harris threatening to kill Julie unless he leaves, he moves to depart but pulls out a revolver instead and shoots the machinery. Neil then rescues Julie and bars the door, leaving the mutated women to die amidst a cascade of exploding machinery.

==Cast== William Smith as Neil Agar
*Anitra Ford as Dr. Susan Harris
*Victoria Vetri as Julie Zorn
*Cliff Osmond as Captain Peters
*Wright King as Dr. Murger
*Ben Hammer as Herb Kline
*Anna Aries as Nora Kline
*Andre Philippe as Aldo Ferrara
*Sid Kaiser as Stan Williams
*Katie Saylor as Gretchen Grubowsky (as Katie A. Saylor)
*Beverly Powers as Harriet Williams

==Sequel==
A semi-sequel to the movie, the play Beyond the Invasion of the Bee Girls was produced by House of Dames in 1998 in Seattle, Washington. 

==Release==
This movie has also been released on DVD as Graveyard Tramps and featured the Compilation Stephen Romano presents Shock Festival. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 