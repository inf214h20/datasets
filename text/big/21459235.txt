Fancypants
 Fancy pants}}
{{Infobox film
| name           = Fancypants
| image          = Fancypants Poster1med.JPG
| alt            =  
| caption        = 
| director       = Joshua Russell
| producer       = Daniel Hanson
| writer         = Joshua Russell
| starring       = Patrick Gleason Roddy Piper Robert Carradine Jackson Dunn Amy Hendricks Patrick Zielinski Kirby OConnell Richard Kind Nick Turturro
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Patrick Gleason, Roddy Piper, and Robert Carradine. Featuring Nick Turturro, Richard Kind, and introducing Jackson Dunn.

It is a comedy/drama about a professional wrestler who is afraid of conflict in real life.  He is nearing the end of his career and finds out he has one fan left in an 8-year old boy, who hides a secret that will change Leos life forever.  The film was shot entirely on location in Chicago. 

The film won Best Feature at the 2011 Sunscreen Film Festival in St. Petersburg, Florida,  and was an official selection at the 2011 Seattle True Independent Film Festival  and the 2012 Treasure Coast Film Festival in Fort Pierce, Florida.   The movie premiered in Chicago on September 29, 2011.  It had a limited theatrical release, then was released nationwide on February 10, 2012 via Comcast On Demand, and March 13, 2012 through Time Warner, Cox, Charter, and all other cable providers.  It is distributed in the US by Lightning Entertainment and Entertainment Content Management.

==References==
 

==External links==
*  
*   New York Times  
* Chicago Sun Times   Daily Herald  
* Chicago Reader  
* Chicago Tribune  
* Chicago Tribune   TimeOut Chicago  

 
 
 
 
 


 