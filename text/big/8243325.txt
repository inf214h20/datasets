S.O.S. Eisberg
{{Infobox film
| name           = S.O.S. Eisberg
| image          = Eisburgsos-poster.jpg
| border         = 
| alt            = 
| caption        = German theatrical release poster
| director       = Arnold Fanck
| producer       = {{Plainlist|
* Carl Laemmle Jr.
* Paul Kohner
}}
| writer         = Edwin H. Knopf
| screenplay     = Tom Reed
| story          = Arnold Fanck
| starring       = {{Plainlist|
* Gustav Diessl
* Leni Riefenstahl
* Sepp Rist
* Gibson Gowland
* Ernst Udet
* Rod La Rocque
* Walter Riml
}}
| music          = Paul Dessau
| cinematography = {{Plainlist|
* Richard Angst
* Hans Schneeberger
}}
| editing        = {{Plainlist|
* Hermann Haller
* Andrew Marton
}}
| studio         = Deutsche Universal-Film
| distributor    = {{Plainlist|
* Deutsche Universal-Film
* Universal Pictures
}}
| released       =  
| runtime        = 90 minutes
| country        = Germany, USA
| language       = German, English
| budget         = 
| gross          = 
}}

S.O.S. Eisberg (S.O.S. Iceberg) is a 1933 German-US drama film directed by Arnold Fanck and starring Gustav Diessl, Leni Riefenstahl, Sepp Rist, Gibson Gowland, Rod La Roque, and Ernst Udet. Written by Tom Reed based on a story by Arnold Fanck, the film is about an Arctic expedition that goes in search of a party that was lost the previous year. S.O.S. Eisberg was filmed on location in Umanak, on the west coast of Greenland, in Iceland, and in the Bernina Alps, on the border between Italy and Switzerland.  It was filmed simultaneously in German and English, and released by Universal Studios in both Germany and the United States. The film premiered on 30 August 1933 in Berlin. 
 The Blue Light (1932), and would go on to direct Triumph of the Will (1934), Olympia (1938 film)|Olympia (1938), and Tiefland (film)|Tiefland (1954).  Riefenstahl co-starred with Gustav Diessl and Ernst Udet in the German version S.O.S. Eisberg, and with Gibson Gowland and Rod La Rocque in the English version S.O.S. Iceberg.

==Cast==
;German version
* Gustav Diessl as Dr. Karl Lorenz
* Leni Riefenstahl as Hella
* Sepp Rist as Dr. Johannes Krafft
* Ernst Udet as Ernst Udet
* Gibson Gowland as John Dragan
* Max Holzboer as Dr. Jan Matuschek 
* Walter Riml as Fritz Kümmel 
* Arthur Grosse
* Tommy Thomas
* Nakinak as Hund

;English version
* Rod La Roque as Dr. Carl Lawrence
* Leni Riefenstahl as Ellen Lawrence
* Sepp Rist as Dr. Johannes Brand
* Ernst Udet as Ernst Udet, flier
* Gibson Gowland as John Dragan
* Max Holzboer as Dr. Jan Matuschek
* Walter Riml as Fritz Kümmel
* Nakinak as Nakinak, the Eskimo dog

==Production==
S.O.S. Eisberg was filmed on location in Umanak, on the west coast of Greenland, in Iceland, and in the Bernina Alps, on the border between Italy and Switzerland.   

==See also==
* List of German films 1933-1945

==References==
;Notes
 
;Bibliography
 
* Fanck, Arnold (1933). S.O.S. Eisberg. Mit Dr. Fanck u. Ernst Udet in Groenland. Die Groenland-Expedition des Universal-Films S.O.S. Eisberg. München: F. BruckmannAG OCLC 219890420.
* Riefenstahl, Leni (1933) Kampf in Schee und Eis. Leipzig: Hesse und Bacher Verlag.
* Sorge, Ernst (1935). With Plane, Boat, and Camera in Greenland: An Account of the Universal Dr. Fanck Greenland expedition. London: Hurst & Blackett, Ltd.
* Riefenstahl, Leni (1995). A Memoir. New York: Picador, ISBN 0-312-11926-7, pp. 104, 108-122, 131, 133-134.
 

==External links==
*    
*    
*  

 

 
 
 
 
 
 
 
 


 