Vidhyarambham
{{Infobox film 
| name           = Vidhyarambham
| image          =
| caption        =
| director       = Jayaraj
| producer       = GP Vijayakumar Sreenivasan
| Sreenivasan
| Murali Sankaradi
| music          = Bombay Ravi
| cinematography = AV Thomas
| editing        = L Bhoominathan
| studio         = Seven Arts
| distributor    = Seven Arts
| released       =  
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, Murali and Sankaradi in lead roles. The film had musical score by Bombay Ravi.   

==Cast==
 
*KPAC Lalitha
*Nedumudi Venu Murali
*Sankaradi Sreenivasan
*Maniyanpilla Raju
*Prathapachandran
*Alummoodan
*Bobby Kottarakkara
*Gouthami
*Jagadish
*Mamukkoya
*Oduvil Unnikrishnan
*Paravoor Bharathan
*Philomina
*KK Jacob
 

==Soundtrack==
The music was composed by Bombay Ravi and lyrics was written by Kaithapram. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Paathiraakkombil || K. J. Yesudas || Kaithapram || 
|-
| 2 || Poovarambin || KS Chithra || Kaithapram || 
|-
| 3 || Uthraalikkaavile || K. J. Yesudas || Kaithapram || 
|}

==References==
 

==External links==
*  

 
 
 

 