Pudgy Picks a Fight!
{{Infobox Hollywood cartoon|
| cartoon_name = Pudgy Picks a Fight!
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist =
| animator = Lillian Friedman Myron Waldman
| voice_actor = Mae Questel
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = 14 May 1937
| color_process = Black-and-white
| runtime = 7 mins English
}}
Pudgy Picks a Fight! is a 1937 Fleischer Studios animated short film starring Betty Boop and Pudgy the Puppy.

==Synopsis==
Betty Boop is so delighted with her new fox fur. Pudgy the Puppy, thinking the fox is a live animal, jealously handles it, and when it does not move in response, thinks he killed the creature. Pudgy is then plagued by nightmarish illusions accusing him of guilt (including seeing the shadow of a part of a lamp which he thinks is a noose) and when Betty returns and tells Pudgy that the fox is not alive, he furiously tears the fur to smithereens.
 

 
 
 
 
 


 