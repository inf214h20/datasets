Altitude (film)
 
{{Infobox film
| name        = Altitude
| image       = AltitudePoster-1.jpg
| caption     = Teaser poster
| director    = Kaare Andrews
| producer    = Ian Birkett
| writer      = Paul A. Birkett
| starring    = Mike Dopud Jessica Lowndes Julianna Guill Ryan Donowho Landon Liboiron Jake Weary Chelah Horsdal
| editing     = Chris Bizzocchi
| distributor = 
| released    =   
| runtime     = 
| country     = Canada
| language    = English
| budget      = 
| gross       = 
| music       = Jeff Tymoschuk
| awards      = 
}}
Altitude is a Canadian horror film|horror, television and  "direct-to-video" film directed by Canadian comic book writer and artist Kaare Andrews.   Anchor Bay Entertainment is set to distribute the film in North America, U.K., Australia, and New Zealand.  
The trailer for Altitude premiered at the 2010 San Diego Comic Con.  

==Plot==
In the prologue, a small aircraft piloted by the mother of Sara (Jessica Lowndes), is transporting a family of three (two parents and their child). The child is extremely nervous and starts hyperventilating. Wondering why he is so afraid, the passengers suddenly see an out-of-control aircraft that crashes into them.

Years later, Sara, who has recently received her pilots license, is planning to fly to a concert with her friends, Sal (Jake Weary) and his girlfriend, Saras best friend Mel (Julianna Guill), her cousin Cory (Ryan Donowho) and her boyfriend Bruce Parker (Landon Liboiron). While in the air, Bruces nerves draw ridicule from the others and Sara invites him to take the controls. They hit some turbulence and Bruce loses control, taking them into a steep climb.

Sara tries to regain control, but a loose bolt has jammed the elevator. Only able to climb, they fly into a storm and lose radio contact. Sara explains that with the elevator jammed, they will keep climbing until they run out of fuel or reach the aircrafts ceiling. They have less than an hours worth of fuel left; Bruce has a panic attack and is put to sleep with a choke hold by Sal. In an effort to save fuel, they jettison everything overboard. The only way to unjam the tail is to climb outside and manually remove the obstacle. Cory, who has experience as a climber, volunteers. He has climbing gear with him and a rope for to use as an anchor. Sal wraps the rope around himself and after some difficulty, Cory makes it to the tail and removes the errant bolt. Sal then sees a horrifyingly giant tentacle among the clouds and loses control of the rope. Cory slips and Sal is almost pulled out of the aircraft. Panicking, he cuts the rope and Cory falls, only to be caught by the monster tentacle.

When Bruce awakens, he finds he has been tied up and learns Cory is dead, along with a monster outside. Bruce tells Sara that he was in the crash that killed her mother and his parents. Sara tries the radio again and hears a strange noise. Sal recognizes it as the monster that took Cory. Suddenly, the aircraft crashes into the monsters open mouth. Bruce looks at a page of his comic which shows a blond woman being grabbed by tentacles. Immediately, a large tentacle grabs Mel. Bruce starts flicking through the comic book, as if he has discovered something. Sal threatens to kill Bruce for causing Mels death and tries to throw him out, but Sara intervenes and in the ensuing confusion, Sal falls out the door.

Bruce tells Sara he is causing all this; that his mind is recreating the comic book, something that happens when he gets very scared. The creature starts attacking the aircraft, and Sara demands that Bruce prove he is doing it by ending it all. His attempts just make things worse, until Sara kisses him, but is grabbed by the monster. She tells him that if he can do all this, then he can bring his parents back. After a struggle, the monster suddenly disappears and she falls back into the aircraft. As they fly out of the storm, they see another aircraft heading straight for them, carrying Bruce, his parents and Saras mother. They manage to take control of their aircraft, and dont crash into the other.

In the altered past Saras mother and Bruces family have arrived at their destination intact. Saras mother says "Everybody gets one near miss, right?", and Bruces mom asks, "Do you think they made it?" to which Saras mother replies "I hope so." The young Sara and Bruce are introduced to one another, holding hands and looking out into the sky.

==Cast==
*Jessica Lowndes as Sara
*Julianna Guill as Mel
*Mike Dopud as The Colonel
*Ryan Donowho as Cory
*Landon Liboiron as Bruce Parker
*Jake Weary as Sal
*Ryan Grantham  as Young Boy
*Chelah Horsdal  as Mrs. Taylor

==Production==
Originally Kaare Andrews teamed up with producer Ian Birkett, and his brother, writer Paul A. Birkett, who had a preliminary script. After spending a day at a small airfield outside of Vancouver, the trio shot "...a fake trailer for no money, you know, like Machete, to raise some money and it immediately got interest." With a "micro-budget" secured, further financing came from Darclight and Telefilm to raise an operating budget to over $3.5  million, enough to make a credible product. The concept of a sky-creature was part of an homage to the imagery evoked by H. P. Lovecraft. 
 Piper Chieftain CGI work. Smith, Zack.    Newsarama, October 26, 2010. Retrieved: November 6, 2011. 

==Reception==
Despite its modest budget and unpretentious use of special effects, many critics found Altitude a refreshing departure from the cookie-cutter school of horror films. Richard Scheib called it "... a film that never insults its audience’s intelligence or opts for easy cliche dramatics."  Other reviews focused on the main dilemma of the film, trying to keep interest in a "confinement thriller" offshoot of the horror genre. Todd Rigney commented: "... interesting short film padded into a daunting 90-minute assault on your patience." 

==Release==
Altitude was released on DVD and Blu-ray on October 26, 2010.   Alliance Films is to release Altitude in Canada. The film has been screened also at the 28th Turin Film Festival (from November 26 – December 4, 2011).

==References==
;Notes
 
;Bibliography
 
* Schaefer, Glen.   The Province, October 4, 2010.
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 