The Piano Tuner of Earthquakes
{{Infobox film
| name           = The Piano Tuner of Earthquakes
| image          = Pianoturnerofearthquakes.jpg
| image_size     =
| caption        =
| director       = Stephen Quay Timothy Quay Keith Griffiths Hengameh Panahi Alexander Ris
| writer         = Alan Passes
| narrator       =
| starring       =
| music          = Trevor Duncan Christopher Slaski
| cinematography = Nick Knowland
| editing        = Simon Laurie
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 99 minutes
| country        = Germany France United Kingdom Portuguese English English
| budget         =
}}

The Piano Tuner of Earthquakes, released in 2005, was the second feature-length film by the Brothers Quay and their first film in over ten years. It features Amira Casar, Gottfried John and Assumpta Serna.

==Plot==

A 19th-century opera singer is murdered on-stage shortly before her forthcoming wedding. Soon after being slain by the nefarious Dr. Emmanuel Droz during a live performance, Malvina van Stille is spirited away to the inventors remote villa to be reanimated and forced to play the lead in a grim production staged to recreate her abduction. As the time for the performance draws near, piano tuner of earthquakes Felisberto sets out to activate the seven essential automata who dot the dreaded doctors landscape and make sure all the essential elements are in place. Once again instilled with life after her brief stay in the afterworld, amnesiac Malvina is soon drawn to the mysterious Felisberto as a result of his uncanny resemblance to her one-time fiancé Adolfo.

==See also==
*List of stop-motion films

==External links==
*  at Metacritic
*  at Rotten Tomatoes
*  at MovieScore Media
*  
*  

 
 
 
 
 
 
 
 
 
 

 