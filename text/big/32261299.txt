Muni 2: Kanchana
{{Infobox film name           = Kanchana image          = Kanchana_poster.jpg caption        = Kanchana Theatrical Poster director       = Raghava Lawrence producer       = Raghava Lawrence writer         = Raghava Lawrence starring  Sarathkumar Lakshmi Sriman
|music          = Thaman cinematography =  Vetri]]
                  Krishnasamy editing        = Kishore Te. studio         = Raghavendra Productions distributor    = Sri Thenandal Films released       =   runtime        = 170 mins country        = India language       = Tamil budget         =       gross          = 
}}
 Tamil comedy Sarathkumar in titular character and Lakshmi Rai in other roles. The film released on 22 July 2011,  while a dubbed same-titled Telugu version was released a week earlier in Andhra Pradesh. In spite of mixed reviews from critics, the film became a commercial success in both languages.    Now it is also going to remake in Sinhala in Sri Lanka and the unusual lead character is going to be portrayed by Ranjan Ramanayake.

==Plot==
Raghava (Raghava Lawrence) is a typical jobless youth who spends his days playing cricket with friends. He suffers from an irrational fear of ghosts, and retreats to the safety of his home after sunset. So great is his fear, that he prefers to sleep with his mother (Kovai Sarala) and have her accompany him to the bathroom at night. This creates major annoyance in the household, including Raghavas brother (Sriman), sister-in-law (Devadarshini) and their children.

One day, Raghava and his friends are forced to abandon their usual cricket ground and find a new one; they unwittingly select an abandoned ground which is rumored to be haunted. A bizarre weather change scares them away. Raghava brings home his cricket stumps, which have been stained with blood from a buried corpse in the ground. He focuses on wooing Priya (Lakshmi Rai), the sister of his sister-in-law. In the following days, his mother and sister-in-law are witness to several paranormal phenomena at night; prominently a ghost haunting the hallways. On consulting a priest, they perform 3 rituals to ascertain if the house is haunted:

1. They keep a coconut on a rangoli and pray to Lord Shiva. The coconut rotates on its own.

2. They make a cow eat food. The cow runs out of the house without eating the food. 

3. They leave a lit lamp and two drops of blood and leave the house. A ghost of a woman appears before them and licks the blood.

Scared senseless, Raghavas mother and sister-in-law hire two priests ( , who successfully drives the spirit away from Raghavas body. The ghost of the woman, trapped, reveals her story.
 MLA Shankar (Devan (actor)|Devan). Kanchana angrily confronts the MLA, who cunningly kills her. He also kills Babu Antony and his son. Before she died, she vowed to kill Shankar, his wife, and his henchmen. The bodies are then buried in Kanchanas own ground.
 symbiotically in Raghavas body to help him out when the need rises.

==Cast==
* Raghava Lawrence as Raghava
* Sarath Kumar as Kanchana
* Lakshmi Rai as Priya Sriman as Raghavas brother Devan as MLA Shankar
* Babu Antony as Bhai
* Kovai Sarala as Raghava´s mother
* Devadarshini as Kamakshi
* Priya as Geetha
* Manobala as priest (Cameo)
* Mayilsamy as priest (Cameo)

==Soundtrack==
The films original soundtrack has been composed by S. Thaman. 

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Nillu Nillu Nillu Nillu
| lyrics1         = Raghava Lawrence Tippu
| length1         = 
| title2          = Sangili Bungili
| lyrics2         = Vivega
| extra2          = Velmurugan
| length2         = 
| title3          = Karuppu Perazaga
| lyrics3         = Vivega
| extra3          = Suchith Suresan, Darshana KT
| length3         = 
| title4          = Kodiavanin Kadhaya
| lyrics4         = Vivega
| extra4          = Sriram, MLR.Karthikeyan and Malathy Lakshman
| length4        = 
}}

==Reception==

===Critical response===
Rediff wrote:"It is torturous and tedious to watch, the chills and thrills are not spine-chilling and a soundtrack that is supposed to be eerie is anything but. Theres quite a bit of unintended comedy too".  Greatandhra wrote:"As for Lawrence, his intention was to target the mass audience and he has been fairly successful in his attempt as compared to his prequel by infusing good depth and emotional intensity".  Sify wrote:"On the whole, Kanchana is an entertaining affair and can be watched once".    

===Box office===
According to Sify, Kanchana emerged 2011s most successful Tamil film based on return on investment.  Its publicity cost was  1.5 crore, while its Telugu dubbing rights were sold to Bellamkonda Suresh for  4 crore. The film grossed a share of  20 crore at the end of its run.  The film completed 50-day run at the box office.  the film eventually completed 100 days and became a super hit

==Awards==
;Vijay Awards Best Supporting Actor - R. Sarathkumar Best Female Comedian - Kovai Sarala

;1st South Indian International Movie Awards
* Best Actor in a Supporting Role - Sarathkumar

==Prequel== Muni is the prequel of Muni:2 Kanchana this film released on March 9, 2007.

==Sequel and remakes==
In early 2012, sources claimed that Raghava Lawrence was planning to make a third part of Muni.      Furthermore, it was suggested that Lawrences brother might play the lead role in the sequel. Instead, he danced with his brother in a cameo for the film. Eventually the third installment Kanchana 2  was released on 17 April 2015. It was also a commercial success. 
 Hindi remake Sai Kumar Sinhalese remake of Kanchana, Ranjan Ramanayake is playing the role initially portrayed by Sarath Kumar as Kanchana. Also been remade in Philippines as Da Possessed.

==See also==
*List of ghost films

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 