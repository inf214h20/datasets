Wara Wara
{{Infobox film
| name           = Wara Wara
| image          = 
| caption        = 
| director       = José María Velasco Maidana
| producer       = 
| writer         = 
| starring       = Juanita Taillansier Martha de Velasco Arturo Borda Emmo Reyes   
| music          = 
| cinematography = 
| editing        = 
| distributor    = Urania Film 
| released       = 1930
| runtime        = 69 minutes  (restored version, 24 frames per second|fps)  
| country        = Bolivia silent
| budget         = 
| gross          = 
}} historical drama and romance film|romance. The film was described as a "superproduction" by the press at the time.

==Preservation==
Long thought to be a lost film, it was rediscovered in 1989, restored, and screened for a new "premiere" in September 2010.    It is "the only known surviving work from Bolivias silent-film era".   

==Plot== Spanish conquest Aymara territories happily ever after". The films closing scene show "a final prude kiss against the backdrop of a sunset on the edge of the Incas sacred lake", Lake Titicaca.     

==Production background==
Velasco Maidana had previously directed The Prophecy of the Lake (La profecía del lago), a 1925 film and love story between an Aymara man and the daughter of a white landowner. The film was censored for its "social critique", and never shown. For Wara Wara, he inverted the gender roles (an indigenous woman falling in love with a white man), and changed the setting. The Prophecy of the Lake had been set in his own time, while Wara Wara was set four centuries earlier, so as to appear less shocking and avoid censorship.    Wara Wara was inspired by Antonio Diaz Villamils novel La voz de la quena. 

The film was shot in Bolivia, between La Paz and Lake Titicaca.  It premiered at the Teatro Princesa in La Paz on January 9, 1930, and was shown thirty-two times. No copies were subsequently thought to have been kept, and Wara Wara became a lost film. 

==Rediscovery==
In 1989, the directors grandson inherited some of his belongings, and discovered film reels among them, mostly in very good condition. They did not contain the film in its final form, but a jumble of shots, leaving little indication as to their proper order, save for references to the plot in press coverage at the time. The film thus had to be not just restored, but reconstructed. In addition, the precise original musical accompaniment was unknown. During the films original release, music had been played live. Cinemateca Boliviana, in charge of restoring the film, decided to add a soundtrack - taken partly from Velsaco Maidanas 1940 ballet Amerindia.  Certain reels were quite badly damaged, and thus took a long time to restore. This was the case, in particular, of the films ending, which only became viewable in 2009. 

The restored films premiere was on September 23, 2010 at the Cinemateca Bolivianas centre in La Paz. 

The restoration was the topic of a book, Wara Wara. La reconstrucción de una película perdida, by filmmaker Fernando Vargas Villazon. 

==Context and significance== indigenist project" orientalised and played by non-indigenous actors and actresses. 

Le Courrier described it as a "universal fairy tale, reminiscent of Romeo and Juliets balcony, but which remains closer to Pocahontas"; it depicts the triumph of love over inter-ethnic hatred.  It also "depicts a homogenous society, which has succeeded in assimilating its indigenous people". Yet, the Courrier argues, Velasco Maidana was progressive for his era. Even though the film is "imbued with the colonial ethnic blending which was popular at the time, Velasco Maidana denounces, in his film, the condition of Indians in Bolivia and is concerned with the suffering of indigenous peoples. He raises the question of their place in society, and defies his societys racist prejudice". 
 plurinational state. 

==See also==
*List of rediscovered films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 