Daredevils of the Red Circle
{{Infobox film
| name           = Daredevils of the Red Circle
| image          = daredevilsredcircle.jpg
| image_size     =
| caption        = John English
| producer       = Robert M. Beche
| writer         = Barry Shipman Franklin Adreon Rex Taylor Ronald Davidson Sol Shor
| narrator       = Herman Brix David Sharpe Charles Middleton William Nobles
| distributor    = Republic Pictures
| music          = William Lava William Thompson
| released       = {{Film date|1939|06|10|U.S. serial|ref1= {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 38-.39
 | chapter = 
 }} |1953|03| |Austria|2002|01|26|France}}
| runtime        = 12 chapters / 211 minutes (serial)  6 26½-minute episodes (TV) 
| country        = United States
| language       = English
| budget         = $126,855 (negative cost: $126,118) 
}} Republic Serial Movie Serial David Sharpe, Herman Brix Charles Middleton. John English and is often considered one of the better serials produced by Republic.  The serial was the fourteenth of the sixty-six serials produced by the studio.

==Plot== philanthropist Horace acrobats is Funfair|performing.  The daredevils, Gene, Bert, and Tiny escape but Genes kid brother is badly wounded in the blaze, and later dies of his injuries.  Seeking revenge they take jobs as private investigators for the man they believe to be Horace Granville. Through a series of deadly traps, and with the help of a mysterious cloaked figure, known only as "The Red Circle", the daredevils begin to unravel the truth.

In the first chapter, we are introduced to all the above facts, and even shown the secret room within the Granville estates where 39013 is keeping the real Granville. He is kept in a cell, the exact duplicate of the one in which 39013 resided for his abruptly ended sentence. This room is trapped, so that in the event that 39013 does not return, a dripping reservoir will run dry. The loss of weight will tip the scale, causing deadly gas capsules to break upon the floor, killing Granville in a very short time. This causes him to spout the characteristic line, "You best hope I continue to live, Granville."

==Cast==
===Main cast=== high diver, one of the Daredevils of the Red Circle Herman Brix as Tiny Dawson, Strongman (strength athlete)|strongman, one of the Daredevils of the Red Circle David Sharpe escape artist, one of the Daredevils of the Red Circle Granddaughter of Horace Granville philanthropist victim, and ex-partner, of 39013 Charles Middleton as Harry Crowel/39013, escaped criminal with a vendetta against Horace Granville

===Supporting cast=== doctor
* Ben Taggart as Dixon, Horace Granvilles manager William Pagan as Landon, police chief henchmen
* Raymond Bailey as Stanley, Horace Granvilles secretary and one of 39013s henchmen Snowflake as Snowflake, black, comic-relief servant of Horace Granville
* George Chesebro as Sheffield, one of 39013s henchmen Ray Miller as Jeff, Horace Granvilles nurse brother
* Stanley Price as Prof. Selden (uncredited)
* "Tuffie", the dog

==Production==
Daredevils of the Red Circle was budgeted at $126,855 although the final negative cost was $126,118 (a $737, or 0.6%, under spend).  It was the cheapest Republic serial of 1939 and one of only three pre-war serials to be made under budget.  The other two were The Fighting Devil Dogs (1938) and Mysterious Doctor Satan (1940). 

It was filmed between 28 March and 28 April 1939.   The serials production number was 897. 

Ironically, David Sharpe, who is generally considered to have been one of the greatest stuntmen in the movies, had to be doubled in action scenes by Jimmy Fawcett because he was playing a leading role, and the studio could not risk any production delays were Sharpe to suffer injury.

==Release==
===Theatrical===
Daredevils of the Red Circles official release date is 10 June 1939, although this is actually the date the sixth chapter was made available to film exchanges. 

===Television===
In the early 1950s, Daredevils of the Red Circle was one of fourteen Republic serials edited into a television series.  It was broadcast in six 26½-minute episodes. 

==Chapter titles==
#The Monstrous Plot (27 min 48s)
#The Mysterious Friend (16 min 41s)
#The Executioner (16 min 45s)
#Sabotage (16 min 38s)
#The Ray of Death (16 min 39s)
#Thirty Seconds to Live (16 min 39s)
#The Flooded Mine (16 min 42s)
#S.O.S. (16 min 40s)
#Ladder of Peril (16 min 39s)
#The Infernal Machine (16 min 36s)
#The Red Circle Speaks (16 min 39s)  -- Clip show|Re-Cap Chapter
#Flight to Doom (16 min 40s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 224–225
 | chapter = Filmography
 }} 

Note: This was one of two 12-chapter serials released by Republic in 1939.  The other was Zorros Fighting Legion.  Republic also released two 15-chapter serials in this year.

<!--
==Cliffhangers==
===Cliffhangers===
#The Monstrous Plot:
#The Mysterious Friend:
#The Executioner:
#Sabotage:
#The Ray of Death:
#Thirty Seconds to Live:
#The Flooded Mine:
#S.O.S.:
#Ladder of Peril:
#The Infernal Machine:
#The Red Circle Speaks:

===Solutions===
#The Monstrous Plot:
#The Mysterious Friend:
#The Executioner:
#Sabotage:
#The Ray of Death:
#Thirty Seconds to Live:
#The Flooded Mine:
#S.O.S.:
#Ladder of Peril:
#The Infernal Machine:
#The Red Circle Speaks:
#Flight to Doom:
-->

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*  
*  
*   at Todd Gaults Movie Serial Experience

 
{{succession box  Republic Serial Serial 
| before=The Lone Ranger Rides Again (1939)
| years=Daredevils of the Red Circle (1939)
| after=Dick Tracys G-Men (1939)}}
{{succession box  English Serial Serial 
| before=The Lone Ranger Rides Again (1939)
| years=Daredevils of the Red Circle (1939)
| after=Dick Tracys G-Men (1939)}}
 

 
 

 
 
 
 
 
 
 
 
 