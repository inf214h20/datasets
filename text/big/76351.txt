Shane (film)
 
{{Infobox film
| name           = Shane
| image          = Shaneposter.png
| caption        = theatrical poster
| director       = George Stevens
| producer       = George Stevens
| cinematography = Loyal Griggs
| editing        = William Hornbeck Tom McAdoo
| music          = Victor Young
| based on       =  
| screenplay     = A.B. Guthrie Jr. Jack Sher
| starring       = Alan Ladd Jean Arthur Van Heflin Brandon deWilde Jack Palance
| distributor    = Paramount Pictures
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $3.1 million
| gross          = $20,000,000 
}}
 Western film novel of Ben Johnson.

Shane was listed No. 45 in the 2007 edition of AFIs 100 Years…100 Movies (10th Anniversary Edition)|AFIs 100 Years…100 Movies list and No. 3 on AFIs 10 Top 10 in the category Western.

==Plot==
  homesteader Joe Starrett (Van Heflin) and his wife, Marian (Jean Arthur), he learns of an ongoing conflict between the valleys homesteaders and the ruthless cattle baron Rufus Ryker (Emile Meyer), who is trying to seize their land.  
 Ben Johnson), one of Rykers men, throws a shot of whiskey on Shanes shirt. "Smell like a man!" he taunts.  Shane doesnt rise to the bait; but at their next encounter Shane orders two shots of whiskey, pours one on Calloways shirt and throws the other in his face, then knocks him to the ground. A brawl ensues; Shane prevails, with Starretts help. Ryker declares that the next time they meet, "the air will be filled with gunsmoke."

Starretts son Joey (Brandon deWilde) is drawn to Shane and his gun. Shane shows him how to wear a holster and demonstrates his shooting skills; but Marian interrupts the lesson. Guns, she says, are not going to be a part of her sons life. There is an obvious, mysterious attraction between Shane and Marian. Shane counters that a gun is a tool, no better nor worse than an axe, shovel, or any other tool. A gun, he says, is as good or as bad as the man using it. Marian retorts that everyone would be better off if there werent any guns, including Shanes, in the valley. 

Jack Wilson (Jack Palance), an unscrupulous, psychopathic gunfighter who works for Ryker, taunts Frank "Stonewall" Torrey (Elisha Cook, Jr.), a hot-tempered ex-Confederate homesteader. Stonewall Jackson, Robert E. Lee, and "all the rest of them rebs" are trash, Wilson says. Torrey calls Wilson a "low-down Yankee liar". "Prove it," Wilson replies—and when the inexperienced farmer goes for his gun, shoots him dead.

Fear spreads through the valley. At Torreys funeral many ranchers talk of leaving; but after they unite to fight a fire set by Rykers men, they find new determination, and resolve to continue the fight against Rykers evil ambitions.

Ryker invites Starrett to a meeting at the saloon to negotiate a settlement—and then orders Wilson to kill him when he arrives. Calloway, unable to tolerate Rykers treachery any longer, warns Shane of the double-cross. Starrett says he will shoot it out with Wilson if he has to, and asks Shane to look after Marian and Joey if he dies. Shane says he must go instead, because Starrett is no match for Wilson. Starrett is adamant, and Shane is forced to knock him unconscious, to Marians dismay.  Is he doing this for her, she asks? Yes, replies Shane; and for Joey, and for all the decent people who want a chance to live in peace in the valley.
 Old West, he says, but Ryker hasnt realized it yet. Then he turns to Wilson; "I hear that youre a low-down Yankee liar," he says. Wilson again replies, "Prove it." Shane kills Wilson, and then Ryker as he draws a hidden gun. Rykers brother Morgan, overhead in a dark balcony, has Shane in his rifle sight; but Joey, who followed Shane into town, shouts a warning, and Shane shoots Morgan as well.

The battle is over, the settlers have won, and Shane tells Joey that he must move on.  "Now you run on home to your mother," he says, "and tell her ... tell her everythings all right. There arent any more guns in the valley." As Joey reaches out, blood drips onto his hands; Shanes left arm hangs limply at his side as he mounts his horse. He rides out of town, past the grave markers on Cemetery Hill and toward the mountains, his body slumped forward in the saddle, ignoring Joeys desperate cries of "Shane! Come back!"

==Cast==
 
* Alan Ladd as Shane
* Jean Arthur as Marian Starrett
* Van Heflin as Joe Starrett
* Brandon deWilde as Joey Starrett
* Jack Palance (credited as Walter Jack Palance) as Jack Wilson Ben Johnson as Chris Calloway
* Edgar Buchanan as Fred Lewis
* Emile Meyer as Rufus Ryker
* Elisha Cook, Jr. as Frank Stonewall Torrey
* Douglas Spencer as Axel Swede Shipstead
* John Dierkes as Morgan Ryker
* Ellen Corby as Mrs. Liz Torrey
* Paul McVey as Sam Grafton
* John Miller as Will Atkey, bartender
* Edith Evanson as Mrs. Shipstead Leonard Strong as Ernie Wright
* Nancy Kulp as Mrs. Howells
 
Jack Palance was the last living top billed cast member when he died in 2006. Beverly Washburn, who played
one of the homesteader children though uncredited, may be the last living cast member. 

==Production notes==
 Although the film is fiction, elements of the setting are derived from Wyomings Johnson County War (1892).   The physical setting is the high plains near Jackson Hole, Wyoming, and many shots feature the Grand Teton massif looming in the near distance. Other filming took place at Paramount Studios in Hollywood, California.
 The Talk Oscar nomination. Shane marked her last film appearance (when the film was shot she was 50 years old, significantly older than her two male co-stars), although she later appeared in theater and a short-lived television series.
 RKO Radio Pictures. Hughes offer made Paramount reconsider the film for a major release.

Jack Palance had problems with horses and Alan Ladd with guns. The scene where Shane practices shooting in front of Joey required 116 takes.   A scene where Jack Palance (credited as Walter Jack Palance) mounts his horse was actually a shot of him dismounting, but played in reverse. As well, the original planned introduction of Wilson galloping into town was replaced with him simply walking in on his horse, which was noted as improving the entrance by making him seem more threatening. In the bar scene where Alan Ladd shoots Jack Palance twice, he noticeably blinks as his gun fires.

===Technical details===
Shane was the first film to be projected in a "flat"  , Paramount picked Shane to debut their new wide-screen system because it was composed largely of long and medium shots that would not be compromised by cropping the image. Using a newly cut aperture plate in the movie projector, as well as a wider-angle lens, the film was exhibited in its first-run venues at an aspect ratio of 1.66:1. Just before the premiere, Paramount announced that all of their films would be shot for this ratio from then on.  This was changed in 1954, when the studio changed their house aspect ratio to 1.85:1.
 optical soundtrack House of Wax.
 Bonnie and Clyde. 

In addition, Shane was one of the first films in which actors were attached to hidden wires that yanked them backwards when they were shot from the front. Director George Stevens also used a small cannon and fired it into a garbage can to create the loud report of the pistol for maximum effect. Stevens was in World War II and saw what a single bullet can do to a man. 
 Welsh television HTV Cymru/Wales broadcast a version dubbed into the Welsh language. 

Assistant producer was Ivan Moffat who provides commentary on the DVD release of Shane, along with George Stevens, Jr.

==Reception==
The film opened in New York City at Radio City Music Hall on April 23, 1953.   According to Motion Picture Daily, "opening day business at the Music Hall was close to capacity. The audience at the first performance applauded at the end of a fight sequence and again at the end of the picture. 

Bosley Crowther, after attending the premiere, called the film a "rich and dramatic mobile painting of the American frontier scene" and noted:
 
Crowther called "the concept and the presence" of Joey, the little boy played by Brandon deWilde, "key to permit  a refreshing viewpoint on material thats not exactly new. For its this youngsters frank enthusiasms and naive reactions that are made the solvent of all the crashing drama in A. B. Guthrie Jr.s script." 

Shane ended its run at Radio City Music Hall on May 20, 1953, racking up $114,000 in four weeks at Radio City.  It earned $8 million in North America during its initial run. 
 The Treasure Double Indemnity, The Informer The Hill. Shane, he wrote, "...is a great movie and can hold its own with any film, whether its a western or not." 

==Awards and honors==
Awards
* Academy Award for Best Cinematography, Color, Loyal Griggs; 1954

Nominations
* Academy Awards:  
** Best Actor in a Supporting Role, Brandon deWilde
** Best Actor in a Supporting Role, Jack Palance
** Best Director, George Stevens
** Best Picture, George Stevens
** Best Writing, Screenplay, A.B. Guthrie Jr.; 1954

Other
* In 1993, Shane was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically or aesthetically significant".
 the list was revisited in 2007, it rose to No. 45.

* In June 2008, AFI revealed its "Ten top Ten"—the best ten films in ten "classic" American film genres—after polling over 1,500 people from the creative community. Shane was listed as the third best film in the western genre.  

American Film Institute recognition
* AFIs 100 Years... 100 Movies No. 69
* AFIs 100 Years... 100 Heroes and Villains:
** Shane, Hero No. 16
* AFIs 100 Years... 100 Movie Quotes No. 47
** "Shane. Shane. Come back!"
* AFIs 100 Years... 100 Cheers No. 53
* AFIs 100 Years... 100 Movies (10th Anniversary Edition) No. 45
* AFIs 10 Top 10 No. 3 Western

==Copyright status in Japan==
In 2006, Shane was the subject of a major legal case in Japan involving the expiration of its copyright in Japan. First Trading Corporation had been selling budget-priced copies of public domain films, including Shane, as Japanese law only protected cinematographic works for 50 years from the year it was published—which meant that Shane fell into the public domain in 2003. In a lawsuit filed by Paramount, it was contested that Shane was not in the public domain in Japan due to an amendment which extended the copyright term for these works from 50 to 70 years, and came into effect on January 1, 2004. It was later ruled that the new law was not retroactive, and any film produced during or before 1953 was not eligible for the extension.   

==References==
  20. The Making of Shane, Walt Farmer, Jackson, WY, 2001

==External links==
 
 

*  
*  
*  
*  
*  
*   at Filmsite.org
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 