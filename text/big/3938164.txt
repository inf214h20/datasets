At the Earth's Core (film)
 
 
{{Infobox film
|  | name = At the Earths Core
  | image = Earths core film.jpg
  | caption = film poster by Tom Chantrell Kevin Connor
  | producer = John Dark Max Rosenberg Milton Subotsky
  | writer = Edgar Rice Burroughs (novel) Milton Subotsky
  | starring = Peter Cushing Doug McClure Caroline Munro
  | music = Mike Vickers
  | cinematography = Alan Hume
  | editing =
| studio = American International Pictures Amicus Productions
  | distributor = American International Pictures British Lion Films (UK)  (Sony Pictures Entertainment) 
  | released = July 1976
  | runtime = 89 min.
| budget = $1.5 million 
  | country = United Kingdom
  | language = English
}}

At the Earths Core is a 1976 fantasy-science fiction film produced by Britains Amicus Productions. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 16 
 directed by Kevin Connor At the Earths Core, by Edgar Rice Burroughs, the first book of his Pellucidar series, in token of which the film is also known as Edgar Rice Burroughs At the Earths Core. The original music score was composed by Mike Vickers.

==Plot summary== Victorian period Welsh mountain, but end up in a strange underground labyrinth ruled by a species of giant telepathic flying reptiles, the Mahars, and full of prehistoric monsters and cavemen. They are captured by the Mahars, who keep primitive humans as their slaves through mind control. David falls for the beautiful slave girl Princess Dia (Munro) but when she is chosen as a sacrificial victim in the Mahar city, David and Perry must rally the surviving human slaves to rebel and not only save her but also win their freedom.

==Cast==
* Peter Cushing as Dr. Abner Perry
* Doug McClure as David Innes
* Caroline Munro as Princess Dia
* Cy Grant as Ra
* Godfrey James as Ghak
* Keith Barron as Dowsett

==Production== The Land That Time Forgot. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 150 

==Reception==
The movie was popular, becoming the 18th most profitable British film of 1976. 

==See also==
* The People That Time Forgot (disambiguation)
* Journey to the Center of the Earth (1959 film)
* Journey to the Center of the Earth (2008 Asylum film) – A direct-to-DVD American film sharing similarities with this film

==References==
 

==External links==
 
*  
*  
*  
*  
*  at BFI Screenonline
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 