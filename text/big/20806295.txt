Remember Last Night?
{{Infobox Film
| name           = Remember Last Night?
| image          = Remember.JPG
| caption        = Theatrical poster
| director       = James Whale
| producer       = Carl Laemmle, Jr.
| writer         = Adam Hobhouse (novel, The Hangover Murders)   Harry Clork   Doris Malloy   Dan Totheroh (screenplay) Edward Arnold Robert Young
| music          = Franz Waxman
| cinematography = Joseph Valentine
| editing        = Ted Kent
| distributor    = Universal Studios
| released       = November 4, 1935
| runtime        = 81 min.
| country        = United States
| language       = English
| budget         = $460,000
| gross          = 
}} mystery comedy Edward Arnold, Robert Young.
 Production Code because of the focus on excessive drinking. The novels original title was also unsuitable because of the word "hangover". Following revisions, the film was approved and was released on November 4, 1935 to mixed reviews and poor box office results.

==Plot== hung over to find Vic dead from a gunshot through the heart and his wife Bette missing. Tony calls his friend, district attorney Danny Harrison to investigate. Bette arrives with Billy Arliss at whose home she had slept. Because of their excessive drinking, no one can remember anything about what had happened the night before. As circumstantial evidence mounts against Tony, he calls in hypnotist Professor Karl Jones to help everyone try to recover their memories. Just as the professor is about to reveal the murderer, he is murdered. 

Next to be killed is restaurateur Faronea. After Tony and Carlotta eavesdrop on him conferring with an accomplice at his restaurant, Faronea discovers them. Tony bluffs that he knows about the kidnapping plot and the accomplice murders Faronea. The couple returns home to find Bouclier murdered in his quarters.  Friend Jake Whitridge responds to a frantic telephone call from Billy. Tony and Danny arrive, as they had planned with Billy, moments after Jake. Jake attacks Billy and knocks him out. When he regains consciousness Billy attempts to shoot Jake but Tony saves him. After the various spouses arrive, Tony announces he has solved the mystery.

Billy borrowed money from Vic on behalf of Jake, using a false name. Jake altered the check to be for $150,000 instead of $50,000 and Vic forced Billy to reveal he had borrowed the money for Jake. Jake shot Vic at Jakes home and brought his body to the party, where everyone assumed he was just passed out. Jake paid Bouclier to remain quiet, which is why Bouclier had to kill Professor Jones. Bouclier, Faroneas accomplice, killed Faronea after Tony spoke to him about the kidnapping plot.  Jake then shot Bouclier. Danny places Jake under arrest and extracts a pledge from Tony and Carlotta to quit drinking. They agree and drink a toast to it. {{cite web
  | title = Overview for Remember Last Night? (1935)
  | publisher = Turner Classic Movies
  | url = http://www.tcm.com/tcmdb/title.jsp?stid=87952
  | accessdate = 2008-12-22}} 

==Cast== Edward Arnold as Danny Harrison Robert Young as Tony Milburn
* Constance Cummings as Carlotta Milburn, Tonys wife
* George Meeker as Vic Huling
* Sally Eilers as Bette Huling, Vics wife Reginald Denny as Jake Whitridge Louise Henry as Penny Whitridge, Jakes wife
* Gregory Ratoff as Faronea Robert Armstrong as Flannagan, the Milburns mechanic
* Monroe Owsley as Billy Arliss
* Jack La Rue as Baptiste Bouclier, Vics chauffeur (as Jack LaRue)
* Edward Brophy as Maxie, Harrisons assistant
* Gustav von Seyffertitz as Professor Karl Jones
* Rafaela Ottiano as Mme. Bouclier (as Rafael Ottiano)
* Arthur Treacher as Clarence Phelps, the butler
* E. E. Clive as Coroners Photographer

==Production== Magnificent Obsession Show Boat. The Thin Man as evidence that a picture based on the novel would be a success. Curtis, p. 254  Laemmle agreed to buy the rights for $5,000, only after extracting a promise from Whale that he would direct Draculas Daughter next. Curtis, p. 255 
  Production Code – and Whale promised to keep the novels ending in which Tony and Carlotta agree to quit drinking. 

Harry Clork and Doris Malloy put together a 34-page treatment which Laemmle approved in April. The pair completed their draft on May 20, 1935. Whale had Dan Totheroh re-write the dialogue and the draft was ready for submission to the PCA on July 15. Curtis, p. 256  When Breen reviewed the draft, his objections centered around the excessive alcohol use. "We take this opportunity of pointing out to you, in regard to the matter of the treatment of drinking in this story, that, generally speaking, it is presented in a light, facetious, acceptable, amusing, and desirable mode of behavior. It is upon this that we feel rejection may be reasonably based."  A revised script with the drinking toned down slightly was submitted on July 24, the same day Whale started shooting. Remember Last Night? was budgeted at $385,000.   Whale inserted lines that made fun of horror pictures, a genre with which he no longer wished to be associated. Carlotta is shown jumping on a diving board flapping a towel and exclaiming "Look, Im Draculas Daughter!" and in another scene she says "I feel like the Bride of Frankenstein!"  Shooting wrapped on September 14. Whale was nine days over schedule and $75,000 over budget. 

==Release and reception==
Remember Last Night? was cleared by the PCA on September 24, 1935 and following previews in October, opened on November 4. Financially the film was a failure that according to Laemmle lost money for the studio.  Critical reception was mixed. The Hollywood Reporter called the film "a murder mystery to kid all murder mysteries" {{cite web
  | last = Miller
  | first = John M
  | title = Remember Last Night?
  | publisher = Turner Classic Movies
  | url = http://www.tcm.com/thismonth/article.jsp?cid=118171&mainArticleId=118168
  | accessdate = 2008-12-22}}  and "a riot of comedy spots superimposed on a riot of crime detecting". Quoted in Curtis, p. 258  Whale, the reviewer found, "let himself go in a riotous directorial splurge".  Although less effusive, The New York Times praised the film as "good minor fun" and noted the likeable pairing of Young and Cummings. Ed Brophy, Edward Arnold and Arthur Treacher were also singled out for praise. However, the Times concluded that Remember Last Night? should be enjoyed "in moderation" as the "halfwit behavior of the roisterers in the film" may make the viewer come away "with the feeling that one or two additional murders among the madcap principals would have made Long Island a still better place to live in". {{cite news 
  | last = Sennwald
  | first = Andre
  | title = Murder With a Smile in Remember Last Night?, the New Comedy Melodrama at the Roxy Theatre.
  | work = The New York Times
  | date = 1935-11-21
  | url = http://movies.nytimes.com/movie/review?_r=1&res=9E02E7D61238E13ABC4951DFB767838E629EDE
  | accessdate = 2008-12-22 }}  Variety (magazine)|Variety was strongly disapproving of the film. "The women are more blotto than the men, and two of the wives are on the make. Its all faintly unwholesome." 

Local censorship boards made numerous cuts to the film. The long drinking party scene was cut, as was part of a 30-second kiss between Tony and Carlotta that opened the film. Censors also cut a line of dialogue delivered by Louise Henry in response to Carlottas declaration that the   New York dubbed the film a "Delightful screwball parody of the detective thriller...Whales use of elisions, non-sequiturs and unexpected stresses creates what is virtually a blueprint for the style developed by Robert Altman in and after MASH." {{cite web
  | last = Milne
  | first = Tom
  | title = Remember Last Night? (1935)
  | publisher = Time Out New York
  | url = https://www.timeout.com/film/newyork/reviews/76573/remember-last-night.html
  | accessdate = 2008-12-22 }}  The Los Angeles Times, reviewing the film for a 1999 retrospective of Whales work, found it to be "an amusing trifle, tossed off with considerable wit and skill by Whale" and "pretty good fun if you’re in the mood for a chic, brittle period piece". {{cite news 
  | last = Thomas
  | first = Kevin
  | title = Remember Whales Comedy
  | work = The Los Angeles Times
  | page = F-15
  | date = 1999-01-28
  | url = http://articles.latimes.com/1999/jan/28/entertainment/ca-2329
  | accessdate = 2008-12-22}} 

==Notes==
 

==References==
* Curtis, James (1998). James Whale: A New World of Gods and Monsters. Boston, Faber and Faber. ISBN 0-571-19285-8.

==External links==
*  
*  
*  
*  


 
 

 
 
 
 
 
 
 
 
 
 
 