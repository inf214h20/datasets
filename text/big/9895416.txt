The Student of Prague (1913 film)
 
{{Infobox film
| name           = The Student of Prague
| image          = Paul Wegener als Student von Prag, Filmplakat 1913.jpg
| image_size     = 
| caption        = 
| director       = Hanns Heinz Ewers Stellan Rye
| producer       = 
| writer         = Hanns Heinz Ewers
| starring       = Paul Wegener  John Gottowt  Grete Berger
| music          = Josef Weiss
| cinematography = Guido Seeber
| editing        = 
| distributor    = 
| released       =  
| runtime        = 85 min.
| country        = German Empire German intertitles
| budget         = 
}}
  silent horror The Student of Prague. Other remakes were produced in 1935 and 2004. It is generally deemed to be the first independent film in history. 

==Plot==
A poor student rescues a beautiful countess and soon becomes obsessed with her. A sorcerer makes a deal with the young man to give him fabulous wealth and anything he wants, if he will sign his name to a contract (i.e. make a Deal with the Devil). The student hurriedly signs the contract, but doesnt know what hes in for.

A long plot summary is given by psychologist Otto Rank in The Double (1971; originally "Der Doppelgänger" in Imago III.2, 1914, 97-164).

The film is referenced in the detective story "The Image in the Mirror" by Dorothy Sayers, in which Lord Peter Wimsey helps clear Mr. Duckworthy, a man wrongly suspected of murder. Among other things Duckworthy tells:  sold himself to the devil, and one day his reflection came stalking out of the mirror on its own, and went about committing dreadful crimes, so that everybody thought it was him." 
 
(In the story, Mr. Duckworthy had what seemed a similar experience - but Wimsey eventually proves that it had a rational explanation involving no supernatural agency).

==Cast==
* Paul Wegener as Balduin
* John Gottowt as Scapinelli
* Grete Berger as Countess Margit
* Lyda Salmonova as Lyduschka
* Lothar Körner as Count von Schwarzenberg
* Fritz Weidemann as Baron Waldis-Schwarzenberg

==External links ==
*  
*  

==References==
 

 
 

 
 
 
 
 
 
 
 
 


 