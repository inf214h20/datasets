Princess of the Nile
{{Infobox film
| name           = Princess of the Nile
| image          = Princess of the Nile.jpg
| image_size     =
| caption        =
| director       = Harmon Jones
| producer       =
| writer         = Gerald Drayson Adams
| based on       =
| starring       = Debra Paget Jeffrey Hunter Michael Rennie
| music          = Lionel Newman (uncredited)
| cinematography = Lloyd Ahern
| editing        = George A. Gittens
| studio         = Panoramic Productions
| distributor    = 20th Century-Fox
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $475,000. 
| gross          =
}}

Princess of the Nile is a 1954 American adventure film starring Debra Paget.

==Plot==
Egypt, 1249: The father of Princess Shalimar has fallen under the spell of the sinister Shaman, who drugs him and tries to keep daughter Shalimar a prisoner. She knows a secret passage, however, and slips away at night to entertain the oppressed villagers of Hanwan by disguising herself as Taura, a popular dancer in the Tambourine Tavern.

Prince Haidi, the son of the caliph of Bagdad, rides into town accompanied by Captain Hussein, his close friend. At the same time, the menacing Rama Khan and his powerful army arrive. Rama Khan is conspiring with the Shaman to overthrow the Hanwan rulers.

Hussein is killed by Khan, and in the confusion, Taura the dancing girl stabs Prince Haidi with a dagger, unaware he is a potential ally. Haidis wounds are not fatal. As he consults Princess Shalimars father about how to conquer the invading horde, he inquires about the dancer Taura who stabbed him, unaware she and Shalimar are one and the same.

Rama Khan wants the princess for himself. He threatens to kill villagers unless she gives herself to him. A battle ensues, in which Haidi, who now realizes her true identity, overcomes Khan, while the Shaman also endures a well-deserved death.

==Cast==
*Debra Paget as Princess Shalimar / Taura 
*Jeffrey Hunter as Prince Haidi
*Michael Rennie as Rama Khan
*Dona Drake as Mirva
*Michael Ansara as Capt. Kral
*Edgar Barrier as Shaman
*Wally Cassell as Goghi
*Jack Elam as Basra
*Lisa Daniels as Handmaiden
*Phyllis Winger as Handmaiden
*Merry Anders as Handmaiden
*Honey Harlow as Handmaiden

==References==
 

==External links==
*  at IMDB

 
 
 
 
 
 
 