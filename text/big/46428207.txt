The Woman in the Cupboard
{{Infobox film
| name = The Woman in the Cupboard
| image =
| caption =
| director = Rudolf Biebrach
| producer =
| writer = Octave Mirbeau (play) Soulié Dussieux de Chennevières (play) Bobby E. Lüthge
| starring = Kaethe Consee Willy Fritsch
| music = Gustav Gold
| cinematography = Werner Brandes
| editing = UFA
| distributor = UFA
| released =  
| runtime =
| country = Germany German intertitles
| budget =
| gross =
}}
The Woman in the Cupboard (German:Die Frau im Schrank) is a 1927 German silent comedy film directed by Rudolf Biebrach and starring Kaethe Consee and Willy Fritsch.  The films art direction was by Erich Czerwonski.

==Cast==
* Rudolf Biebrach 
* Kaethe Consee 
* Willy Fritsch as Dr. Richard Marchal  
* Harry Hardt as Solicitor Thibault  
* Arnold Korff as Col. Gaston Belfort  
* Olga Limburg
* Fee Malten as Claire Labori  
* Imre Ráday 
* Gyula Szöreghy 
* Ruth Weyher as Lucie  

== References ==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

== External links ==
*  

 
 
 
 
 
 
 
 

 