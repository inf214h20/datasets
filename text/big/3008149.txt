Far Out Man
{{Infobox film
| name           = Far Out Man
| image          = Far Out Man.jpg
| caption        = Theatrical release poster.
| director       = Tommy Chong
| producer       = Lisa M. Hansen John Pare
| writer         = Tommy Chong
| starring       = Tommy Chong C. Thomas Howell Rae Dawn Chong Shelby Chong Paris Chong Martin Mull Judd Nelson Michael Winslow Cheech Marin Paul Bartel
| music          = Jay Chattaway
| cinematography = Greg Gardiner Eric Woster Stephen Myers Gilberto Costa Nunes
| distributor    = New Line Cinema
| released       =  
| runtime        = 81 minutes
| country        = United States English
| budget         =
| gross          =
}}

Far Out Man was a 1990 comedy film written, directed by and starring Tommy Chong.  It was filmed in Los Angeles, California, USA. Cinetel Films produced the movie and it was distributed in USA theaters by New Line Cinema, Sony Video (VHS), Platinum Disc (DVD), and RCA/Columbia Pictures Home Video (VHS). It was distributed in Germany by Ascot Video (VHS) and in Brazil by Odyssey (VHS). It was distributed in Canadian theaters by Alliance.

==Plot==
An aging hippie goes on a road trip in search of his long lost family. He meets up with his son (Paris Chong, Tommys real-life son). Together they go off to see America. A majority of Tommy’s real life family have roles; daughter Rae Dawn and wife Shelby both have lines.

==Cast==
*Tommy Chong as Far Out Man
*C. Thomas Howell as himself
*Rae Dawn Chong as herself
*Shelby Chong as Tree
*Paris Chong as Kyle
*Martin Mull as Dr. Leddledick Bobby Taylor as Bobby
*Reynaldo Rey as Lou
*Peggy McIntaggart as Misty (Peggy F. Sands)
*Al Mancini as Fresno detective
*Judd Nelson as himself
*Cheech Marin as Cheech
*Michael Winslow as airport cop
*Lisa M. Hansen as police radio dispatcher
*Rae Allen as Holly
*Paul Bartel as Weebee Cool
*Paul Hertzberg as Drunk man with wine
*Rob Perlis

Floyd Sneed, former drummer of the rock group Three Dog Night, made a small cameo in the film as a drummer.

==References==
{{Reflist|refs=
 {{Citation
 | last1     = Bloom
 | first1    = Steve
 | last2     = Halperin
 | first2    = Shirley
 | title     = Reefer Movie Madness: The Ultimate Stoner Film Guide
 | publisher = Abrams
 | page      = 113
 | year      = 2010
 | isbn      = 1613120168
 | url       = http://books.google.com/books?id=2WMt_bUB6SkC&pg=PT113
 | postscript= .
}} 
}}

==External links==
* 
*  

 
 
 
 

 