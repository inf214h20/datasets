Happy You and Merry Me
{{Infobox Hollywood cartoon|
| cartoon_name = Youre Not Built That Way
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist =
| animator = Edward Nolan Myron Waldman
| voice_actor = Mae Questel
| musician = Sammy Timberg (uncredited)
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = August 21, 1936
| color_process = Black-and-white
| runtime = 6 mins
| movie_language = English
}}

Happy You and Merry Me is a 1936 Fleischer Studios animated short film featuring Betty Boop and Pudgy the Pup.

==Plot summary==
A stray kitten called Myron wanders into Betty Boops house, gets sick on candy, and is cured with catnip by Betty and Pudgy the pup.

==External links==
* http://www.imdb.com/title/tt0027717/
 

 
 
 
 
 


 