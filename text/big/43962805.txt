Frida Still Life
 
{{Infobox film
| name           = Frida Still Life
| image          = 
| caption        =  Paul Leduc
| producer       = 
| writer         = José Joaquín Blanco Paul Leduc
| starring       = Ofelia Medina
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
 Paul Leduc. Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Ofelia Medina as Frida Kahlo
* Juan José Gurrola as Diego Rivera
* Max Kerlow as Leon Trotsky
* Claudio Brook as Guillermo Kahlo Salvador Sánchez as David Alfaro Siqueiros
* Cecilia Toussaint as Fridas Sister
* Ziwta Kerlow as Trotskys Wife

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Mexican submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 