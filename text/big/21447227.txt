Don Quixote (1957 film)
 
{{Infobox film name = Don Quixote image = Don Quixote 1957 poster.jpg caption = 1957 film poster by Vladimir Kononov director = Grigori Kozintsev producer =  writer = Miguel de Cervantes Yevgeni Shvarts starring = Nikolai Cherkasov music = Gara Garayev cinematography = Apollinari Dudko Andrei Moskvin editing = Ye. Makhankova distributor = Lenfilm released =   runtime = 110 minutes country = Soviet Union language = Russian budget = 
}}
 same name. It was entered into the 1957 Cannes Film Festival.     It opened in the United States in 1961, beginning its U.S. run on January 20. 

The film was exhibited in the mid-1960s by Australian University film clubs receiving the productions of Sovexportfilm. It was the first film version of Don Quixote to be filmed in both widescreen and color.

==Changes to the storyline==
Although largely faithful to the storyline of the Cervantes novel, the film makes several significant changes and adopts an overall more outwardly serious tone than does the original book. The order of Quixotes adventures is completely changed, with the famous windmill scene occurring towards the end, just after Don Quixote has been completely humiliated and mocked by Altisidora, the "damsel" at the Ducal Palace who pretended to be dead out of love for him. Quixote then angrily leaves the palace, where he and Sancho have been repeatedly treated" to practical jokes, and at the same time, Sancho, who has been mockingly made governor of a nonexistent island, is "overthrown" by a staged revolt. The separated knight and squire encounter each other again on the road, and soon come upon the windmills. Immediately after tilting against them and being injured by being thrown to the ground by the sails, they encounter the Knight of the White Moon, who is really Sanson Carrasco in disguise. He challenges Quixote, who is now too weak to fight, to a joust and easily defeats him. Carrasco then reveals his true identity to Quixote and Sancho. The three of them make their way back to Don Quixotes village, where the old man is soon at deaths door. As in several adaptations (but not as in the novel), Aldonza, the wench whom Don Quixote believes to be his lady Dulcinea, actually makes an appearance in the film. She appears at the beginning, when Don Quixote is first preparing to sally forth as a knight and "christens" her Dulcinea, and at the end, grieving with the knights family at his deathbed.

==Cast==
* Nikolai Cherkasov as Don Quixote de la Mancha / Alonso Quixano
* Yuri Tolubeyev as Sancho Panza
* Serafima Birman as The Housekeeper
* Lyudmila Kasyanova as Aldonsa (as L. Kasyanova)
* Svetlana Grigoryeva as The Niece Vladimir Maksimov as The Priest
* Viktor Kolpakov as The Barber
* Tamilla Agamirova as Lady Altisidora (as T. Agamirova)
* Georgiy Vitsin as Sanson Carrasco
* Bruno Freindlich as The Duke (as V. Freindlich)
* Lidiya Vertinskaya as The Duchess
* Galina Volchek as Maritornes (as G. Volchek)
* Olga Vikland as Peasant girl
* Aleksandr Benyaminov as Shepherd
* S. Tsomayev as Andres, the shepherd boy

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 