Murder with Music
 

{{Infobox film
| name           = Murder with Music
| image          =
| image_size     =
| caption        =
| director       = George P. Quigley
| producer       = Augustus Smith (writer) Victor Vicas (writer)
| narrator       =
| starring       = See below
| music          = George Webber
| editing        =
| distributor    =
| released       = 1941
| runtime        = 59 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Murder with Music is a 1941 American film directed by George P. Quigley.

The film is also known as Mistaken Identity (American video title).

== Plot summary ==
 

== Cast == Bob Howard as Editor
*Milton Williams as Ted
*Nellie Hill as Lola George Oliver as Hal
*Bill Dillard as Mike
*Marjorie Oliver as Secretary
*Ruth Cobbs as Mary Smith
*Ken Renard as Bill Smith
*Andrew Maize as Jerry
*Pinky Williams as Lewis
*Skippy Williams as Band leader Alston as Dancer
*Johnson as Specialty dancer
*Noble Sissle as Themselves Young as Dancer

== Soundtrack == Augustus Smith)
* Bob Howard, accompanied by Noble Sissle and his Orchestra - "Too Late Baby" (Written by Sidney Easton and Augustus Smith)
* Noble Sissle, accompanied by his orchestra - "Hello Happiness" (Written by Sidney Easton and Augustus Smith)
* Skippy Williams and his band - "Jam Session" (Written by Skippy Williams)
* Noble Sissle and by his orchestra - "Running Around" (Written by Sidney Easton and Augustus Smith)
* Nellie Hill - "Cant Help It" (Written by Skippy Williams)
* Nellie Hill accompanied by Skippy Williams and his band - "Cant Help It" (reprise)
* Played on piano and danced by Johnson and Johnson - "Thats the Cheese You Got To Squeeze"

== External links ==
* 
* 

 
 
 
 
 
 
 


 