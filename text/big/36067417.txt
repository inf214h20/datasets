Stars of Eger (1968 film)
{{Infobox film
| name           = Stars of Eger
| image          = 
| image_size     = 
| caption        = 
| director       = Zoltán Várkonyi
| producer       = András Németh
| writer         = Géza Gárdonyi (novel)   István Nemeskürty
| narrator       = 
| starring       = Imre Sinkovits   György Bárdy   István Kovács (actor)|István Kovács   Tibor Bitskey
| music          = Ferenc Farkas 
| editing        = Ferencné Szécsényi
| cinematography = Ferenc Szécsényi 
| studio         = Mafilm
| distributor    = 
| released       = 19 December 1968
| runtime        = 157&nbsp;minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Hungarian historical Stars of Eger was made in 1923.

==Partial cast==
* Imre Sinkovits - István Dobó
* György Bárdy - Jumurdzsák
* István Kovács (actor)|István Kovács - Gergely Bornemissza
* Tibor Bitskey - Mekcsey István
* Gábor Agárdy - Sárközy
* Vera Venczel - Éva Cecey
* Éva Ruttkai - Queen Izabella
* Hilda Gobbi - Baloghné
* Vera Szemere - Ceceyné
* Péter Benkő - János Török
* Rudolf Somogyvári - István Hegedûs
* Gyula Benkő - Veli bég
* László Tahi Tóth - Kobzos Ádám
* Géza Tordy - Miklós
* Tamás Major - Sultan Suleiman I
* Gábor Mádi Szabó - Cecey
* Zoltán Latinovits - Imre Varsányi
* Gábor Koncz - János
* Samu Balázs - Bishop
* Géza Polgár - Bojki Tamás
* Antal Farkas - Debrõi, innkeeper
* József Fonyó - Emperor Ferdinands interpreter
* László György - Woodsmith
* József Horváth (actor)|József Horváth - Sukán
* László Inke - Turkish leader
* György Korga - Bornemissza Jancsika
* Tibor Molnár - Friar Márton
* Lajos Pándy - Fügedy
* János Rajz - Friar
* Levente Sipeki - Fürjes Ádám
* Nándor Tomanek - Preacher
* István Velenczei - Father György
* Ferenc Zenthe - Josef, the German mercenary
* László Bánhidi - Jumurdzsáks prisoner
* Ferenc Bessenyei - Bálint Török
* Péter Blaskó - Hungarian soldier
* András Kern - Cook
* Zoltán Várkonyi - Emperor Ferdinand I

==External links==
* 
 
 
 
 
 
 
 

 