One Day in the Future
{{Infobox film
| name           = One Day in the Future
| image          = GelecektenBirGunFilmPoster.jpg
| image size     =
| alt            = 
| caption        = Film Poster
| director       = Boğaçhan Dündar
| producer       = Cüneyt Ceylan
| writer         = Cüneyt Ceylan
| narrator       = 
| starring       =  
| music          = Selim Gülgören
| cinematography = Aras Demiray
| editing        = 
| studio         = Film Bahçesi
| distributor    = Medyavizyon
| released       =  
| runtime        = 95 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $296,103
| preceded by    = 
| followed by    = 
}}
One Day in the Future ( ) is a 2010 Turkish comedy film, directed by Boğaçhan Dündar, featuring Hayrettin Karaoğuz as a man who is shown his unrealised future by two angels after committing suicide. The film went on general release across the country on  .

==Plot==
Tolga (Hayrettin Karaoğuz), having struggled with bad luck all his life, believes he is a total loser and the unluckiest man in the whole world. Following an unsuccessful presentation of his new project at work, he is fired. Believing he has ruined his last chance to win over the love of his life (Hande Subaşı), he commits suicide. After he dies, he is met by two angels (Rasim Öztekin and Arda Kural), who, as a punishment, show Tolga scenes from how his life would have been in the future if he had not committed suicide.

==Cast==
* Hayrettin Karaoğuz as Tolga
* Hande Subaşı as Ebru
* Rasim Öztekin as Canal
* Arda Kural as Felek
* Neco as Timur
* Işın Karaca as Filiz
* Murat Serezli as Salim
* Bektaş Erdoğan as Metin
* Hümeyra Aydoğdu as Berna
* Yeşim Dalgıçer as Rüya

==Release==
The film opened across  Turkey on   at number six in the box office chart with an opening weekend gross of $135,914.   

==Reception==
===Box office===
The movie has made a total gross of $296,103. 

==References==
 

==External links==
*   (Turkish)
*   at Beyazperde (Turkish)
*  
*  

 
 
 
 
 
 
 
 

 
 