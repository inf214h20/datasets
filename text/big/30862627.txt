Ashaant
{{Infobox film
| name = Ashaant
| image = 
| writer = 
| starring = Akshay Kumar Vishnuvardhan (actor)|Dr.Vishnuvardhan Ashwini Bhave  Mamta Kulkarni
| director = Keshu Ramsay
| producer = 
| music = Jatin Lalit
| lyrics = 
| released = 1 January 1993
| language = Hindi
| country = India
}}
 Hindi and Kannada language|Kannada. In Kannada, the film was called Vishnu Vijaya. The Hindi film was considered a flop while the Kannada version became a Super Hit Blockbuster. It is to be remembered that, Akshay Kumar, when Super Star Vishnuvardhan (actor)|Dr.Vishnuvardhan died, had said that “We did a Hindi and a Kannada version of Ashaant. The Kannada version was a super hit. The Hindi version was a flop. I realised Vishnuji was a much bigger star than I could ever be." 

==Plot==
Ashaant is a story about effort made by the whole Police Force of the country, especially two police officers A.C.P.
Vijay Bombay Police (Akshay Kumar) and A.C.P. Vishnu Bangalore Police (Vishnuvardhan) to bring peace i.e. Shanti
to country.
The wage a war against a Mafia who deals in printing fake currency notes and have become threat to country´s and security.
These Mafia dons Kaka (Jai Kalgutkar) and Rana (Punit) are operating from Bangalore city.
But when the pressure of Police Officer Vishnu mounts on them, the decide to shift their roots to Bombay. As they enter Bombay, Rana is nabbed by ACP Vijay and is arrested.
Vijay starts his investigation to reach the roots of this Mafia through Rana, when orders come from home ministry to shift Rana from Bombay custody to Bangalore court for many previous serious crimes.

Vijay along with his friend ACP Amit (Pankaj Dheer) take join them to re-arrest Rana.
Vishnu, who is in charge of Rana´s case gets them permission on two conditions that Vijay and Amit can´t use their weapons and police uniform. Vishnus wife Anita (Ashwini Bhave)and Vijay are ex lovers. Their relationship ended as Anitas father Ex-Chief Minister Niranjan Das refused to let her daughter marry an Ordinary Police Officer. Also to take revenge on Vijay he got Vijays Sister Ritu (Amits Wife) killed. Vijay then misused his power as Police Officer and got Niranjas Das forced to Resign as Chief Minister. Anita also took an oath that she would only marry a Police Officer and Married Vishnu.

On the other hand Rana splits away from Kaka and forms his own Mafia which gradually becomes more powerful and ruthless.
Vijay and Amit along with Vishnu make a team to capture Rana and now Kaka also.
Sonali (Mamta Kulkarni) a dancer in Kaka´s hotel, joins this team of three against the two mafia operating in city.

Kaka manages to create misunderstanding between Vijay and Vishnu to break their unity and divide their power.
Vishnu starts suspecting illegitimate relationships between his wife Anita (Ahwini Bhave) and Vijay.
The degree of suspicion rises but Amit finally wipes the misunderstanding between these two friends Vishnu and Vijay.
They join each other with equal faith and then manages to smash both the Mafia.
Rana and Kaka are destroyed but at the cost of sacrificing their dear friend Amit.

==Cast==
* Akshay Kumar ... Vijay
* Vishnuvardhan (actor)|Dr.Vishnuvardhan ... Vishnu
* Ashwini Bhave ... Anu / Anita 
* Pankaj Dheer ...Amit 
* Mamta Kulkarni ... Sonali
* Sharat Saxena ... Chief Minister Niranjan Das

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Ki Ghadi Are Ghadi Ghadi"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 2
| "Tu Bhi Sharabi Main Bhi Sharabi" Abhijeet 
|-
| 3
| "Hum To Sanam Sadiyon Se"
| Abhijeet, Kavita Krishnamurthy
|-
| 4
| "Deewane Tu Hai Jahan"
| Kumar Sanu, Alka Yagnik
|-
| 5
| "Bheege Bheege Jo Sawan"
| Kavita Krishnamurthy
|-
| 6
| "Jaani Jaani Jaani"
| Chithra
|}

==References==
 

==External links==
*  

 
 
 
 

 