La cambiale
{{Infobox film
 | name = La cambiale
 | image =  La cambiale.jpg
 | caption =
 | director = Camillo Mastrocinque
 | writer = Luigi Magni, Vittorio Metz, Roberto Gianviti
 | starring =  Totò, Peppino De Filippo, Ugo Tognazzi, Vittorio Gassman, Aroldo Tieri, Sandra Mondaini, Raimondo Vianello
 | music =  Carlo Innocenzi
 | cinematography =  Alvaro Mancori
 | editing =  
 | producer = Franco Palaggi
 | distributor =
 | released = 1959
 | runtime = 105 min
 | awards =
 | country = Italy
 | language =  Italian
 | budget =
 }} 1959 Cinema Italian comedy film directed by Camillo Mastrocinque.  

== Cast ==
*Totò: Antonio Posalaquaglia 
*Peppino De Filippo: Peppino Posalaquaglia 
*Erminio Macario|Macario: Tommaso La Candida 
*Vittorio Gassman: Michele, the "coiffeur pour chien"
*Sylva Koscina: Odette Mercury 
*Ugo Tognazzi: Alfredo Balzarini  Georgia Moll: Maria, Ottavios sister  
*Raimondo Vianello: Olimpio  Paolo Ferrari: Ottavio 
*Aroldo Tieri: Commendator Pierluigi Bruscatelli
*Lia Zoppelli: la proprietaria della Ilaria boutique
*Luigi Pavese: Cav. Temistocle Bisogni 
*Toni Ucci:  Manager of Ursus 
*Andrea Bosic: Prince Alessio 
*Olimpia Cavalli: Enrichetta 
*Gina Rovere: Lola Capponi 
*Mario Castellani: Lawyer Incarta 
*Eduardo Passarelli: il pretore
*Giacomo Furia: il cancelliere

==References==
 

==External links==
* 

 
 
 
 
 