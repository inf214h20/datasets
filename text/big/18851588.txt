Divergence (film)
 
 
{{Infobox film
| name           = Divergence
| image          = DivergencePoster2005.jpg
| caption        = Theatrical poster in Mandarin Benny Chan
| producer       = Benny Chan
| eproducer      = Daneil Lam Chiu Suet-Ying
| aproducer      =
| writer         = Ivy Ho
| starring       = Aaron Kwok Ekin Cheng Daniel Wu
| music          = Anthony Chue
| cinematography = Anthony Pun Yiu-Ming
| editing        = Yau Chi-Wai
| distributor    = Universe Films Distribution Co. Ltd. (Hong Kong) Tartan Films (U.S.)
| released       =  
| runtime        = 101 mins.
| country        = Hong Kong
| awards         =  Mandarin
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 2005 Cinema Hong Kong Benny Chan, from a screenplay by Ivy Ho. The film stars Aaron Kwok, Ekin Cheng and Daniel Wu.

==Plot==
It is about three people (a cop, a lawyer and a killer) who cross paths after the murder of a federal witness and a kidnapping of a pop star.

CID Shing (Aaron Kwok) arrests the accountant of a money launderer. However, the accountant is assassinated upon his arrival in the Hong Kong airport. The killer Coke (Daniel Wu) escapes without leaving any clues. While the masterminded laundry head (Gallen Lo) is happy about the soon resumption of his frozen assets for the death of his unfavorable witness, his fond son Xia is kidnapped suddenly. 

Shing is an ill-fated CID. He cant forget his loving girlfriend (Angelica Lee) who disappeared 10 years ago and has not yet been found. During investigation, he finds a woman (Angelica Lee) who looks very much alike with his missing girlfriend. The woman is the wife of the lawyer To (Ekin Cheng) who represents the laundry head. It raises his interest in the couple. 

Although the killer Coke has completed his job, he cant help feeling great interest in the case and it violates the code of killers. He does it because years ago Shing was the host of the Police Call which leave him great impression. And he knows why Shings girlfriend disappeared 10 years ago. 

The lawyer To is very successful in his field. He always wins cases in the court and helps the suspects escape from prison terms. Will there be a hidden side for an arrogant person like him? Everyone in the story seems to possess some qualities that dont suit their identity. Will it engulf them into the whirlpool? 

Simultaneously, there are many other crimes around the city. Unknown motivated murders and disappearances are plentiful. Will the cases related to the 3 characters of the story?

==Cast==
* Aaron Kwok as Suen Siu-Yan
* Ekin Cheng Yee-Kin as To Hou-San
* Daniel Wu as Koo/Coke
* Gallen Lo as Yiu Tin-Chung
* Angelica Lee as Siu-Fong / Amy
* Ning Jing as Ting
* Yu Rongguang as Inspector Mok
* Tommy Yuen as Yiu Ha
* Samuel Pang as So Fu-On
* Jan Lamb as Detective Chu
* Eric Tsang as Uncle Choi Sam Lee as Leung Tak
* Lam Suet as Mou Wai-Bun 

==Awards==
The film won the 42nd Golden Horse Awards for best actor in leading role (Aaron Kwok), best cinematography  and best editing.

== External links ==
*  
*  

 

 
 
 
 
 
 
 