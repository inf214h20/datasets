Days of Waiting (1987 film)
{{Infobox film
| name           = Days of Waiting
| image          = 
| writer         = Hossein Torabi
| starring       = Firouz Behjat Mohammadi, Mohammad Motee, Majid Mozaffari, Ahmad Hashemi, Esmail Mohammadi, Iraj Tahmasb, Jamshid Layegh, Asghar Hemat, Gohar Kheirandish, Fereshteh Sadre Orafaiy
| director       = Asghar Hashemi
| producer       = Asghar Hashemi, Cinematographic Department of the Mostazafan Foundation
| music 	 = Farhad Fakhredini
| cinematography = Mahmoud Kalari
| editing        = Ruhallah Emami
| released       =  1987
| runtime        = 94 minutes Persian
| budget         = 
}}
Days of Waiting, or Ruzhay-e Entezar ( ), is an Iranian film and was directed by Asghar Hashemi. This film was Hashemi´s first feature as director and the first Iranian film happening in the desert in winter-time.

==Plot==
Fed up with the unfair prices of a speculator who buys their crop in advance only to sell them their own harvest at multiple prices, a number of tent-dwellers living amid the central desert of Iran decide to go to other sellers to procure fodder for their starving flocks. But he has exerted enough intimidation to make his colleagues to think twice before trying to break his prices.
The pinched tent-dwellers assign a representative to go buy barley in the nearby town. Later when the delegate is to carry the fodder to their place, he finds out that lorry drivers refuse to take the risk of the hazardous trip to the snow-covered area with too many deadly moors. 

==Interview with director==
The main part of the film depicts man´s battle with a harsh nature, says Hashemi about his film: “This is the first Iranian film happening in the desert in winter-time – a situation few would dare to take a shooting crew to work at. I have been for a realistic approach, far from heroes, hyperbole and adventure. Although the story has an underlying theme of adventure, I have preferred to concentrate on the human honour and dignity, though a remote concept to the urban population, which drives the natives of arid deserts to keep on struggling for survival. I have been trying to throw light on concepts like honour and selflessness in the milieu of tribal life. Although I could easily create adventure and suspense, I dwelled on a course that begins with life and ends with death. This is the message of my film. To me, it is the solidarity and sincerity of the people that matters. That is why my leading character kneels to accomplish the mission he has undertaken before his blood knis.” 

==References==
 

== External links ==
 
*  
*   at the Iranian Movie Database



 
 