There Is Another Sun
{{Infobox film
| name           = There Is Another Sun
| image          = There is Another Sun DVD cover.jpg
| image_size     = 
| caption        = DVD cover
| director       = Lewis Gilbert
| producer       = Ernest G. Roy Guy Morgan
| narrator       = 
| starring       = Maxwell Reed Laurence Harvey
| music          = Wilfred Burns
| cinematography = Wilkie Cooper Dudley Lovell
| editing        = Charles Hasse
| distributor    = Butchers Film Service (UK) Realart Pictures (USA)
| released       = 17 May 1952 (USA)	
| runtime        = 89 mins.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1951 Cinema British drama film directed by Lewis Gilbert and produced by Ernest G. Roy.

==Cast==
* Maxwell Reed as Eddie Racer Peskett 
* Laurence Harvey as Mag Maguire 
* Susan Shaw as Lillian 
* Leslie Dwyer as Mick Foley  Meredith Edwards as Detective Sergeant Bratcher 
* Hermione Baddeley as Gypsy Sarah, fortune-teller  Robert Adair as Sarno 
* Leslie Bradley as Racetrack Manager 
* Eric Pohlmann as Markie, club owner 
* Nosher Powell as Teddy Green, champ boxer  Earl Cameron as Ginger Jones, carnival boxer 
* Dennis Vance as Len Tyldesley 
* Laurence Naismith as Riley, Greens trainer  Charles Farrell as Mr. Simmons, fight promoter
* Wilfred Burns as Pianist 
* Harry Fowler as First Novice Biker 
* Jennifer Jayne as Dora, Lils Friend at Markies  
* Arthur Mullard as Harry, Boxing Booth Contestant
* Hal Osmond as Mannock
* J.H. Messham as 1st Wall of Death Rider 
* Jim Kynaston as 2nd Wall of Death Rider
* Tom Messham as 3rd Wall of Death Rider

==External links==
* 
* 
* 
* 

 

 
 
 
 
 


 