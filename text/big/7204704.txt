Naagarahaavu
 
{{Infobox film
| name           = Nagarahaavu
| image          = Naagarahaavu.jpg
| caption        = Film poster
| director       = Puttanna Kanagal
| producer       = N. Veeraswamy
| writer         = T. R. Subba Rao
| based on       =  
| screenplay     = Puttanna Kanagal Vishnuvardhan Aarathi Leelavathi Ambarish
| music          = Vijaya Bhaskar
| cinematography = Chittibabu
| editing        = P. Bhakthavathsalam
| studio         = Sri Eshwari Productions
| released       =  
| runtime        = 169 minutes
| country        = India Kannada
| budget         =
}}

Naagarahaavu ( ,  ) is a 1972 Indian Kannada language film directed by Puttanna Kanagal, based on T. R. Subba Raos three novels Nagarahavu, Ondu Gandu Eradu Hennu and Sarpa Mathsara, and starring Vishnuvardhan (actor)|Vishnuvardhan, Aarathi, K. S. Ashwath in lead roles. The supporting cast features Leelavathi (actress)|Leelavathi, Ambareesh|Ambrish, Shivaram, Dheerendra Gopal, Lokanath and Vajramuni.This is the 1st Kannada movie to complete 100 days in 3 centres.
 Vishnuvardhan and Kannada cinema. Lakshmi Devi have come in for their share of applause.

The film is about the teacher who miserably fails in blending his daily educative chores on society interfering  philosophically in guiding a tough lad. Although Vishnuvardhans character cannot be called a Cobra (having venom) but he is too tough a guy who doesnt listen to anybody but has an obvious anger towards the age old caste system and inefficiency of urban society in holding up culture which makes his first love a prostitute under a corrupt husband . At the climax the deaths of Chamayya and Ramachari are also portrayed magnificently on a hill.

The conversation between the Guru (Chamayya) and Shishya (Ramachari) is the highlight of movie all through. They both efficiently portray their parts with high energy and confidence. Although the student seems to be subdue under masters advice at the end master gets defeated by his beliefs towards society. Probably the society is the silent rhetoric for the title of the movie which spreads venom - a cobra.

== Plot details ==
The story revolves around a short tempered, yet affable college student Ramachari. Ramachari is unpopular in the whole town of Chitradurga as an ill-tempered boy and is disliked by many people in his college.

=== First few years of Ramachari in College ===
The story begins with Ramachari being caught in a classroom while trying to copy in an examination. The college principal (Loknath) suspends him for copying in the exam. Ramachari is humiliated and angry at this action and later at night Ramachari throws stones at the principals house and when the principal wakes up and comes out of his house, Ramachari ties the half-naked principal to a pole and runs away satisfied.

Then the plot focuses on Ramacharis home. Ramachari is brought up in a pious and religious Brahmin environment by his parents Madhwacharya and Sonabai. His father doesnt like him because he is unpopular in the whole town as a ruffian. His mother is worried about his future. One person who truly (seems to) understand Ramachari is his primary school teacher, Chamayya (K.S.Ashwath) master. Ramachari finds the company of his primary school teacher Chamayya and his wife Tunga (Leelavathi), more interesting than that of his parents and considers them as his parents. Ramachari has very great regards for his Chamayya master even though he is no longer his academic teacher and would do whatever his teacher would say without thinking.

The next part of the story is about the two women Almelu (Aarathi) and Margaret (Shubha) who come into Ramacharis life and how the teacher Chamayya influences Ramacharis decisions in these relationships. The teacher tries to bring Ramachari in the "conventional way of life", but he errs and Ramacharis life is destroyed.

=== Almelu ===
In college, Ramachari hangs out with Varada (Shivaram) and other friends. Varada has a very beautiful sister, by the name Almelu. Almelus beauty has maddened a neighbourhood hooligan, Jaleela (Ambareesh) who keeps stalking her. Varada wants to put an end to this eve-teasing but he is a timid, pusillanimous person. He asks Ramachari for help. Ramachari agrees based on the condition that he should marry Almelu. Ramachari is tempted and he fixes Jaleela and drives him away out of Almelus life. Almelu and Ramachari fall in love.

At this point, when Almelu and Ramachari are truly in love, Almelus marriage is fixed with a boy of her caste. When Almelu tries to refuse the proposal by saying she is in love with Ramachari and asks for Varadas support, Varada says that he never gave Ramachari any promise and deceives Ramachari.

At this point, the teacher Chamayya intervenes in Ramacharis life and says to give up hope on Almelu. He convinces Ramachari by making him understand that sacrifice is a greater act than selfish love.

Ramachari, confused, agrees to his teacher and sacrifices his love.

Years later, Ramachari meets her in a five star hotel as a prostitute. Almelus husband sold her into flesh trade and she had turned into a prostitute.

=== Margaret ===
The second lady in Ramacharis life is Margaret, a peppy Christian girl who has moved into the town who has young men like Tukaram (Dheerendra Gopal) swaying to her tunes. Ramachari kisses Margaret on an ego tassle between the two and they soon fall in love. However due to the intense pressure in the society which denounces the marriage between a Brahmin boy and a Christian girl, they decide to run away. Once again the teacher Chamayya tries to stop Ramachari and says that by sacrificing his love to Margaret, he will be holding his religion and tradition before love. But this time, Ramachari confronts his teacher and says that he wont budge because the last time he sacrificed his love for Almelu, it went horribly wrong and Almelu was a high society prostitute. Chamayya master, accused now of ruining Almelus life, realizes (perhaps for the first time) that Ramachari was right and he was wrong.

=== Climax ===
The movie ends with Chamayya master falling to his death from the hill when Ramachari pushes him in a fit of rage when his master tells "if you want to marry her, you have to witness my death before it" . He will not have any motive to kill his master,but it shows his respect for his master since he dont want to hear about his masters death. Ramachari, shocked by this incident asks Margaret to join him in a bid to accompany his beloved teacher in death, to which she happily agrees. They both commit suicide by jumping off a cliff.

Ramachari, the hero is compared to a King Cobra. Dangerous, yet respected but a misfit in the society.

==Cast== Vishnuvardhan as Ramachari
* K. S. Ashwath as Chamayya Meshtru
* Aarathi as Alamelu
* Shubha as Margaret Leelavathi as Thunga
* Ambareesh as Jaleela
* M.N Lakshmi Devi as Mary
* M. P. Shankar as Pailwan
* Shivaram as Varadha
* Lokanath
* Dheerendra Gopal as Thukaram
* Vajramuni

== Soundtrack ==
The music of the film was composed by Vijaya Bhaskar with lyrics penned by Vijaya Narasimha, Chi. Udaya Shankar and R. N. Jayagopal.

=== Track List ===
{{Track listing
| lyrics_credits = yes
| extra_column = Singer(s)
| title1 = Haavina Dwesha Hanneradu Varusha http://www.kannadamovielyrics.com/Songs/Nagarahavu.aspx#Haavina Dwesha
| title2 = Kannada Naadina Veera Ramaniya http://www.kannadamovielyrics.com/Songs/Nagarahavu.aspx#Kannada Naadina
| title3 = Karpoorada Bombe Naanu http://www.kannadamovielyrics.com/Songs/Nagarahavu.aspx#Karpoorada Bombe
| title4 = Baare Baare Chendada Cheluvina Taare http://www.kannadamovielyrics.com/Songs/Nagarahavu.aspx#Baare Baare
| title5 = Sangama Sangama http://www.kannadamovielyrics.com/Songs/Nagarahavu.aspx#Sangama Sangama
| title6 = Kathe Heluve http://www.kannadamovielyrics.com/Songs/Nagarahavu.aspx#Kathe Heluve Nanna Kathe
| lyrics1 = Vijaya Narasimha
| lyrics2 = Chi. Udaya Shankar
| lyrics3 = R. N. Jayagopal
| lyrics4 = Vijaya Narasimha
| lyrics5 = Vijaya Narasimha
| lyrics6 = Chi. Udaya Shankar
| extra1 = S. P. Balasubramanyam
| extra2 = P. B. Srinivas
| extra3 = P. Susheela
| extra4 = P. B. Srinivas
| extra5 = P. B. Srinivas, P. Susheela
| extra6 = P. Susheela
}}

==Awards==
Filmfare Award South 1973
* Best Film Kannada
Karnataka State Film Awards 1972-73 
* Second Best Film 
* Best Actor – Vishnuvardhan 
* Best Actress – Aarathi 
* Best Supporting Actor – K S Ashwath 
* Best Supporting Actress – Shubha 
* Best Story Writer – Tha Ra Su 
* Best Screenplay – S R Puttanna Kanagal 
* Best Dialogue Writer – Chi Udayashankar

== Criticism ==
After watching the film, T. R. Subba Rao remarked that Puttanna Kanagal has turned Naagarahaavu (the cobra) into Kerehaavu (meaning rat snake). The implied meaning is that the characterisation of Ramachari in the novel and the movie is strikingly different and less effective. Nevertheless Naagarahaavu is an all-time favorite movie and remains popular in all its reruns. 

== Remakes to other Language ==
* This movie was remade in Hindi titled as Zehreela Insaan, directed by Puttanna Kanagal himself.
* The film was remade in Tamil as Raaja Naagam with Sreekanth playing the lead role.
* This movie was remade into Telugu titled Kode Nagu starring Sobhan Babu, Chandrakala and Lakshmi and noted lyricist Aacharya Athreya playing Chamayya teacher. This was directed by K.S. Prakasha Rao. It was a big hit in Telugu.

== References ==
 

#  

== External links ==
*  

 
 
 
 
 
 
 
 