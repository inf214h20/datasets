Sita Sings the Blues
{{Infobox film
| name           = Sita Sings the Blues
| image          = Sita STB Poster.jpg
| caption        = Poster for Sita Sings the Blues
| director       = Nina Paley
| producer       = Nina Paley
| writer         = Nina Paley
| narrator       = Aseem Chhabra Bhavana Nagulapally Manish Acharya
| starring       = Sanjiv Jhaveri Nina Paley Deepti Gupta Debargo Sanyal Reena Shah Pooja Kumar Aladdin Ullah
| music          = Annette Hanshaw et al.
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $290,000 Largely for music, copyright clearance and distribution. Paley, Nina.  . December 28, 2008. Accessed on: January 31, 2009. 
}}
Sita Sings the Blues is a 2008 animated film written, directed, produced and animated entirely by American artist Nina Paley (with the exception of some fight animation by Jake Friedman in the "Battle of Lanka" scene), 
   primarily using 2D computer graphics and Flash Animation.
 shadow puppets, musical interludes voiced with tracks by Annette Hanshaw and scenes from the artists own life. The ancient mythological and modern biographical plot are parallel tales, sharing numerous themes.

==Plot==

===The Ramayana===
The film uses a pared-down adaptation of the legend that retains many of its finer details while adopting a perspective sympathetic towards Sita; in the directors words, the film is "a tale of truth, justice and a woman’s cry for equal treatment." Director Nina Paleys   from the press section at  , retrieved May 4, 2008 

The plot joins the legend at the exile of prince Rama from his fathers court, at the behest of his fathers favorite queen, Kaikeyi. Having earned the right to any single favor by saving the kings life, Kaikeyi attempts to secure her own sons inheritance over the eldest and favorite, Rama, by ordering him banished from the court. Sita, Ramas wife, determines to accompany her beloved husband, although the woods are dangerous and overrun with demons and evil spirits. The demon king Ravana, encouraged by his spiteful ogress sister, hears of Sitas beauty and determines to kidnap her. He sends a golden hind past their dwelling to distract Rama, who tries to impress Sita by hunting the hind into the woods. In his absence, Ravana abducts Sita and demands that she submit to him on pain of death. Sita remains staunchly devoted to Rama and refuses to entertain the idea; Ravana sets a deadline for the ultimatum and Sita waits faithfully for Rama to rescue her.

Aided by the monkey prince Hanuman, Rama eventually discovers Sitas location and brings the monkey army to assist in her rescue. Ravana is slain and Sita restored to her husband, although he expresses serious doubts concerning her fidelity during her confinement. She submits to a trial by fire, a test of her purity; upon throwing herself into the flames, she is immediately rescued by the gods, who all proclaim her devotion and fidelity.

She accompanies Rama back to the palace, and soon falls pregnant. Lingering doubts still play on Ramas mind, however, and after overhearing one of his subjects beating and ejecting an unfaithful consort (claiming he is no Rama to accept and forgive her unfaithfulness), he orders his reluctant brother Lakshman to abandon Sita in the forest. In the company of ascetics she gives birth to her sons and raises them to love and praise their absent father. Years later, Rama overhears their hymns of adoration to their father and locates their dwelling. Distressed and disappointed by Ramas continuing doubt on her purity during her reunion with Rama, Sita prays to the earth to swallow her as final proof of her purity and devotion and the prayer is duly answered, despite the pleas of Rama and Lakshman.

===Contemporary parallel===
In an episode taken from the directors own life,  animator Nina Paley starts the film living happily in a San Francisco apartment with her husband and cat. Her husband then accepts the offer of a six-month contract working in Trivandrum, India, and moves there alone to take up the position. After a month of no contact, he calls to inform his wife that the contract has been extended another year.

Bewildered by his callous indifference to their separation, Nina sublets their apartment, leaves their beloved cat behind and joins her husband in India. Upon her arrival he appears deeply unenthusiastic to be reunited and demonstrates neither affection nor sexual interest. A while later, Nina flies to a meeting in New York, where she receives a brief e-mail from her husband telling her that their relationship is over. Sad and alone, she stays in New York, finding comfort in a new cat and her study of the Ramayana.

==Style and narrative==
The film uses several different styles of animation to separate and identify the parallel narratives.

  approaches Sita during her captivity.]]

===Episodes from the Ramayana===
Episodes with dialogue from the Ramayana are enacted using painted figures of the characters in profile, which strongly resemble the 18th-century Indian tradition of Rajput painting|Rajput painting. The Rajput style of brush painting was principally enacted on manuscripts and commonly employed in the telling of epics such as the Ramayana. In the film they serve as a more traditional style of dramatic narrative, although the dialogue is frequently ironic, inappropriately modern or otherwise humorous.

The background in each scene is usually static and the poses of each character are kept minimal, with movement achieved by simply transporting the character across the screen in its set position. Speech is enacted by alternating the set pose of the face with a slightly liquified version where the jaw is lower.

 s discuss Ramas attitude towards Sita after her trial by fire.]]

===Narration and discourse on the Ramayana===
 , published by Asia Book, Bangkok in 2000, page retrieved May 4, 2008. 

The voices are clearly contemporary and somewhat irreverent, unlike their visualisations, which further establishes the theme of contrast between "ancient tragedy and modern comedy";   of the official website, SitaSingstheBlues.com  Chhaya Natak shadow theatre, for example, was commonly used in retellings of the Ramayana.   feature on shadow puppetry, retrieved May 4, 2008. 

During these sections, the ideas and contradictions raised over the course of the puppets discussions are visualised in animated photographic compositions in the background.

  mourns her privation from her husband and his callous behaviour towards her.]]

===Musical episodes from the Ramayana=== vector graphic animation.

The slick, bold style – driven by digital animation software – is at odds with the somewhat rustic quality of the old musical recordings but allows close synchronisation with the vocals. The smooth, repetitive, side-scrolling movement it effects assists in suspending the musical episode from the more consistent narrative plot.

 , Ninas husband seems distant.]]

===Contemporary parallel===
The modern, more personal element to the contemporary part of the story is narrated using the rough, energetic Squigglevision technique of traditional animation. It conveys the kind of restlessness inherent in the story and achieves a more light-hearted, universal tone with its simple, highly stylised renderings of character and environments.

==Production==
 rotoscoped by Paley for the animation.

===Copyright problems===
The film uses a number of 1920s Annette Hanshaw recordings. Although the filmmaker initially made sure these recordings were not covered by US copyright law,  a number of other copyright issues surfaced, including state laws prior to US federal copyright law on recordings, rights to the compositions and the right to synchronize the recordings with images. These recordings were protected by state commerce and business laws passed at the time in the absence of applicable federal laws and were never truly "public domain".   In addition, the musical composition itself, including aspects such as the lyrics to the songs, the musical notation, and products derived from using those things, is still under copyright. 

Without a distributor, Nina Paley was unable to pay the approximately $220,000 that the copyright holders originally demanded. Eventually, a fee of $50,000 was negotiated. Paley took out a loan to license the music in early 2009. 
 

In July 2011, Nina Paley made a protest video regarding the films deletion from YouTube in Germany due to what she regards as fraudulent take-down notice under the aegis of Gesellschaft für musikalische Aufführungs- und mechanische Vervielfältigungsrechte|GEMA, Germanys major performance rights or music collection organization,  but which may be an instance of a larger on-going conflict regarding copyright and royalties between YouTube and GEMA.  

On January 18, 2013, Paley announced that she has changed the Creative Commons license for the film from "CC-BY-SA" (the Creative Commons Attribution-Share-alike 3.0 Unported license) to "CC-0" (public domain); she made the ownership rights change in response to the continual red tape of rights procurement, even under the share-alike license.    

==Unorthodox distribution==
Due to terms of the music license, one limited DVD pressing of 4,999 copies was printed.  The film was released for   starting in early March, 2009 "at all resolutions, including broadcast-quality, HD, and film-quality image sequences", at the time licensed under "CC-BY-SA".  The freely downloaded files counted as "promotional copies" and was exempt from payments to the copyright holders of the songs. 

The full film can also be   in low-resolution streaming video on the web site for WNET, a PBS member station in New York City. WNET broadcast the film on  .
  ancillary products, sponsorships, the aforementioned limited DVD sales, and possibly other methods. 

A cornerstone of the distribution model is the "creator-endorsed" logo, developed by Nina Paley in cooperation with QuestionCopyright.org. Although anyone is free to distribute the film, distributors who do so while giving a part of the profits to the artist can get the artists endorsement and use the "creator-endorsed" logo on their promotional materials.   Exclusive right to distribute 35mm and HDCam prints of the film is split between GKIDS  for all theaters East of the Mississippi River, and Shadow Distribution  for all theaters west of the Mississippi River.

The film can also be rented via DVD on Netflix as of March 17, 2010.  When asked by a media provider on behalf of Netflix in April 2010 if she would also offer the film via the companys on-demand streaming service in exchange for a limited amount of money, Nina requested that the film be streamed either digital restrictions management|DRM-free or with an addendum telling viewers where the film was available for download. When the internet television service refused to meet these conditions due to a "No Bumper (broadcasting)|Bumpers" policy, Nina refused to accept their offer, citing her desire to uphold her principled opposition to DRM. 

Paley has qualified this distribution strategy as a financial success, citing that it earned $132,000 from March 2009 to March 2010. 

==Reception==

===Critical reaction===
Since its release, Sita Sings the Blues has attracted limited but consistent acclaim from critics. Rotten Tomatoes reports that 100% of critics have given the film a positive review, based on 33&nbsp;reviews, with an 8.3/10 review average.  The film also holds a score of 94 on the review aggregator website Metacritic  . 

Film critic Roger Ebert on the Chicago Sun-Times enthused, "I am enchanted. I am swept away. I am smiling from one end of the film to the other. It is astonishingly original. It brings together four entirely separate elements and combines them into a great whimsical chord... To get any film made is a miracle. To conceive of a film like this is a greater miracle." 

The New York Times too praised the films ingenuity, commenting that "  evokes painting, collage, underground comic books, Mumbai musicals and Yellow Submarine (for starters)," and praised Paley for her use of 2D animation:  "A Pixar or DreamWorks extravaganza typically concludes with a phone book’s worth of technical credits. Ms. Paley did everything in “Sita” — an amazingly eclectic, 82-minute tour de force — by herself." 

Although The Hollywood Reporter declared the film to be "a rather rarified effort that will probably appeal more to adults than to children," it described it as "charming" and commented that, "Arriving amidst a tidal wave of overblown and frequently charmless big studio efforts, "Sita Sings the Blues" is a welcome reminder that when it comes to animation bigger isnt necessarily better." 

===Controversy===
The films distribution sparked an intense debate regarding the portrayal of Hindu culture in Western media and was met with outrage by some Hindus  and, according to the film-maker, a number of academics also.  In April 2009, a group called the Hindu Janajagruti Samiti started a petition calling for a complete ban on the movie and initiation of legal action against all those who have been involved in its production and marketing, believing its portrayal of the Ramayana to be offensive, with some members going so far as to call it "a derogatory act against the entire Hindu community."  Nina Paley expressed surprise at the adverse reaction, saying "I thought it might be a bit controversial, but I wasn’t fully aware of how art and artists are major targets of some right-wing nationalist groups in India. I always imagine an audience of smart, compassionate people I’d enjoy spending time with." Kohn, Eric.  . The Forward. May 29, 2009.  Aseem Chhabra, who was one of the shadow puppets in the film, said "In the last two decades, the right-wing religious forces in India — Hindu and Muslim — have become very strong, vocal and sometimes violent. But I also know that there are enough sane, balanced, liberal people in India who take art for what it is." 

Nina Paley has said that some academics have also been critical of the film, describing their position as "any white person doing a project like this is by definition racist, and its an example of more neocolonialism." Di Justo, Patrick. . Wired. April 25, 2008. 

===Awards===
Sita Sings the Blues has won a number of awards.   IMDB 

* Berlinale (Berlin International Film Festival), Feb. 2008,  , Germany
* Annecy, June 2008,  , France
* Avignon, June 2008,  , France
* Athens International Film Festival, Sept. 2008, Best Script Award, Greece
* Ottawa International Animation Festival, Sept. 2008,  , Canada
* Montreals Festival du nouveau cinéma, Oct. 2008,  , Canada
* Expotoons, Oct. 2008,  , Buenos Aires, Argentina
* Leeds International Film Festival, Nov. 2008, Golden Owl Competition - Special Mention, UK
* Asheville Film Festival, Nov. 2008,  , NC, USA
* Starz Denver Film Festival, Nov 2008,  , CO, USA
* Gotham Independent Film Awards, Dec 2008,  , NYC, NY, USA
* Les Nuits Magiques, Dec 2008,  , Begles, France
* Santa Fe Film Festival, Dec 2008,  , NM, USA
* Boulder International Film Festival, Feb 2009,  , CO, USA
* Film Independent’s Spirit Awards, Feb 2009,  , Los Angeles, CA, USA
* Fargo Film Festival, March 2009,   and  , ND, USA
* Festival Monstra, March 2009  , Lisbon, Portugal
* Cairo International Film Festival for Children, March 2009, Jury’s Special Mention, Cairo, Egypt
* Tiburon International Film Festival, March 2009,  , Tiburon, CA, USA
* Big Cartoon Festival, March 2009,  , Krasnoyarsk, Russia
* Animabasaui5, March 2009,  , Bilbao, Spain
* Akron Film Festival, April 2009,  , Akron, OH, USA
* Philadelphia CineFest, April 2009,  , Philadelphia, PA, USA
* Salem Film Festival, April 2009,  , Salem, OR, USA
* Indian Film Festival of Los Angeles, April 2009,  , Los Angeles CA, USA
* Talking Pictures Festival, May 2009,  , Evanston, IL, USA
* Connecticut Film Festival, June 2009,  , Danbury, CT, USA
* Festival Internacional de Cine DerHumALC, June 2009,  , Buenos Aires, Argentina
* Indianapolis International Film Festival, July 2009,  , Indianapolis, IN, USA

===Cultural impact===
In April 2009, the film inspired a Bangkok high fashion line designed by Roj Singhakul, titled "Sita Sings the Blues". 

==See also==
* Versions of Ramayana

==Footnotes and references==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 