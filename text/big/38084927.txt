Immanuel (film)
{{Infobox film
| name           = Immanuel
| image          = 
| caption        = Theatrical 100 days poster
| director       = Lal Jose
| producer       = S. George
| writer         = A.C. Vijeesh
| starring       = Mammooty Fahadh Faasil Reenu Mathews Salim Kumar Guinnes Pakru
| music          = Afzal Yusuf
| cinematography = Pradeep Nair
| editing        = Ranjan Abraham
| studio         = Cyn-Cyl Celluloid
| distributor    = Play House, Tricolor Entertainment (Aus/NZ/Singapore) 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 4 crores
| box office     = 9 crores
}} Malayalam drama film, directed by Lal Jose and produced by S. George.    The film stars Mammootty in the title role  with Reenu Mathews and Fahadh Faasil.  Lal Jose has stated that it is a "soft film sans hullabaloo" and "essentially tells the story of Immanuel’s intrinsic goodness even in the face of adversity."   


==Synopsis==
The film follows the character of Immanuel who works in a book publishing firm. When the company is closed down due to unexpected reasons, Immanuel and his family find it difficult to make both ends meet.  He then gets a job in a private insurance company, where he finds it very hard to meet targets set by his manager. The movie gives an unvarnished portrayal of the ruthless ways in which private insurance companies work, with scant regard for humanity. Clients are repeatedly denied their claims on flimsy grounds while the company makes profit. The hardships and harsh realities he faces forces him to see himself and his life in a new perspective that is often outside his personal comfort zone

==Cast==
*Mammootty as Immanuel
*Fahadh Faasil as Jeevan Raj 
*Reenu Mathews as Annie
* Suja Menon as Saritha Master Gaurisankar as Robin
*Sukumari as Khadeejumma
*Salim Kumar as Suku (Peon)
*Guinness Pakru as Kannadi Kavi Shivan
*Sunil Sukhada as Joseph
*Ramesh Pisharody as Venkatesh
*P. Balachandran as Gopinathan Nair
*Bijukuttan as Driver Abu Salim as Chandy
*Ponnamma Babu s Chandys wife
*Devi Ajith as Sandy Wilson
*Shivaji Guruvayoor as Simon
*Balachandran Chullikkadu as Madhavettan
*Nedumudi Venu as Jabbar Mukesh (cameo) as Rajasekharan Devan (cameo) as Kuwait Kumaran Muktha (cameo) as Jenniffer
*Anil Murali (cameo) as Velayuthan
*Nandhu as Dr. Ramakrishnan
*Deepika Mohan as Immanuels co-worker at DTP Center

==Development== John Abraham and Balachandra Menon rumored to be taking part in the film.    Jose denied rumors that Abraham would be acting in the film and Menon also denied that he would perform due to conflicting schedules with his personal life.   Actor Mammootty was confirmed as the lead actor, with Fahadh Faasil also confirmed as performing in the film.  Faasil was initially said to be performing a "negative role" in Immanuel, which Jose later stated was untrue and that Faasil would be portraying a company executive.    In early January 2013, Jose announced that he had cast, a newcomer, Reenu Mathews, as Mammoottys wife in the film.  

Filming began in Kochi in January 2013. 

==Critical reception== OneIndia gave the movie 4/5 stars and stated, "Immanuel is a touching, heartwarming and poignant tale of human values, endurance and goodness in people" and that the movie "is worth watching this weekend and seems to have all ingredients that might make it a superhit."     

Aswin J Kumar of The Times of India gave the movie 3/5 stars, and said that "Immanuel is the latest embodiment among the newly-developed breed of characters who absorb all pain, rage and agony with an enduring smile," but stated that "the problem with Immanuel is that whatever he (Immanuel) does in the film looks more divine than human."    

Now running.com rated Immanuel above average. Veeyen said that Emmanuel is a film that talks of the trying times that we live in. its a delicate film with modest intentions, but which nevertheless remains a miniature gem with a distinct shine. He also praised the performance of mammooty. Mammootty is incredibly good as Emmanuel Veeyen wrote. 
 Best Actor in 2010. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 