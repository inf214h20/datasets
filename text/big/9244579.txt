Battles Without Honor and Humanity: Police Tactics
{{Infobox film
| name           = Battles Without Honor and Humanity: Police Tactics
| image          = BattlesWithoutHonorAndHumanityPoliceTacticsDVDCover.jpg
| image size     = 175px
| caption        = North American DVD cover
| director       = Kinji Fukasaku
| producer       =
| writer         = Kazuo Kasahara Kōichi Iiboshi  (original story)  Takeshi Katō
| narrator       = Tetsu Sakai
| music          = Toshiaki Tsushima
| cinematography = Sadaji Yoshida
| editing        =
| distributor    = Toei Company
| released       = January 15, 1974
| runtime        = 101 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}
  is a 1974 Japanese yakuza film directed by Kinji Fukasaku. It is the fourth film in a five-part series that Fukasaku made in a span of just two years.

==Plot==
In fall 1963, the police crack down on yakuza activities nationwide due to public outcry and in preparation for the upcoming 1964 Summer Olympics. However, the war between the Yamamori family and Shinwa Group versus the Uchimoto, Hirono, and Akashi families wages on. Noburo Uchimoto, Shozo Hirono and Shinichi Iwai recruit Hidemitsu Kawada and Tomoji Okajima, the usually neutral leader of the Gisei Group, to their side. When Masakichi Makiharas men kill a member of Hironos family he wants to go after Yamamori himself, however retired yakuza and Hironos adviser Kenichi Okubo stops him. Akira Takeda threatened Okubo to keep Hirono in Kure, Hiroshima|Kure, as Yamamori has fled to Hiroshima City while Makiharas men stay to fight Hironos.

The police, knowing that supporting all the  visiting reinforcements is taking a monetary toll, strictly watch the gangs illegal business collections. When a Uchimoto member accidentally kills a civilian, the public demands further action and the media begin photographing yakuza in brawls. The police put a constant stakeout on Hironos base, effectively paralyzing him, Uchimoto refuses to take action, and his backers the Akashi get entangled in resistance in Tokyo. When Hirono learns that Yamamori will be in Kobe, he secretly leaves his base and plans to kill him then. However, during the trip, his men leave him stranded and intend to perform the hit themselves. But the Akashi stop them, not wanting the murder to happen on their turf, instead Iwai plans a large memorial service for Hironos killed man, using it as an excuse to send him a large number of reinforcements for an attack.

In the meantime, Uchimoto is kidnapped by Takeda and Yamamori and forced to reveal the plan to them. Yamamori tips the police off to a year-old crime Hirono committed to have him arrested. Without him, the Akashi attack never happens and Yamamori and Makihara are able to return to Kure. Due to Takeda tricking Okajimas girlfriend, Yamamori is able to have Okajima killed much to Takedas anger, as he believes it hurt their image with the public. Gisei Group member Shoichi Fujita retaliates by bombing Shoichi Edas office and Uchimoto rats on his own men who planned to attack Yamamori as a favor for Takeda releasing him. When Uchimotos men learn this they launch a deadly gunfight in public, which leads police to arrest Uchimoto, Eda, and Yamamori. Iwai and his men immediately fly to Kure to rebuild the Gisei Group, while Takeda recruits further allies including his former enemy Otomo. Takeda has Boss Akashis house in Kobe bombed, and the Akashi assume it was the Shinwa Group and retaliate accordingly before further violence follows in Hiroshima.

Kawada then has one of his men kill his supposed ally Fujita, feeling that the Gisei are taking his turf. Iwai visits Hirono in jail and explains to him what has happened and that he has to return to Kobe because the Akashi have made peace with the Shinwa thanks to police mediation. Hirono is sentenced to over seven years in prison, Makihara gets about three, Eda five, Yamamori a year and a half, and Uchimoto is let go on probation for formally dissolving his family. While both waiting to be booked into prison at the films end, Takeda tells Hirono that Takeda has to turn his yakuza family into a political committee to survive.

==Cast==
 
*Bunta Sugawara as Shozo Hirono
*Akira Kobayashi as Akira Takeda Takeshi Katō as Noburo Uchimoto
*Tatsuo Umemiya as Shinichi Iwai
*Hiroki Matsukata as Shoichi Fujita
*Nobuo Kaneko as Yoshio Yamamori
*Hideo Murota as Hideo Hayakawa
*Shingo Yamashiro as Shoichi Eda
*Kunie Tanaka as Masakichi Makihara
*Shinichiro Mikami as Hidemitsu Kawada
*Ichiro Ogura as Hiroshi Yazaki
*Asao Koike as Tomoji Okajima
 
*Asao Uchida as Kenichi Okubo
*Harumi Sone as Toshio Ueda
*Tatsuo Endo as Shigeo Aihara
*Toshio Kurosawa as Shigeru Takemoto
*Nobuo Yana as Isamu Kasai
*Akio Hasegawa as Yasuki Fukuda
*Yukio Miyagi as Ryuji Matsui
*Mayumi Nagisa as Mieko
*Isao Natsuyagi as Hiroshi Sugimoto
*Toshie Kimura as Rika Yamamori
*Mitsuko Aoi as Akemi
*Kinji Nakamura as Kenjiro Ishigami
*Seizō Fukumoto as Tsunehiko Yamazaki
*Mitsue Horikoshi as Aiko Hikarigawa
*Sanae Nakahara as Kikue
*Akira Shioji as jailer
 

==Release==
Battles Without Honor and Humanity: Police Tactics has been released on home video and aired on television, the latter with some scenes cut. A Blu-ray box set compiling all five films in the series was released on March 21, 2013 to celebrate its 40th anniversary. 
 David Kaplan, Kenta Fukasaku, Kiyoshi Kurosawa, a Toei producer and a biographer among others.   

==References==
 

==External links==
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 