The Appointments of Dennis Jennings
{{Infobox film
| name           = The Appointments of Dennis Jennings
| image          =
| image_size     = 
| caption        = 
| director       = Dean Parisot
| writer         = Mike Armstrong Steven Wright 
| producer       = Dean Parisot Steven Wright
| narrator       = 
| starring       = Steven Wright Rowan Atkinson Laurie Metcalf
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1988
| runtime        = 29 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Appointments of Dennis Jennings is a 1988 American short comedy film, starring and co-written by Steven Wright, which won the Academy Award for Live Action Short Film at the 61st Academy Awards in 1988.   

==Plot==
Dennis Jennings (Steven Wright) is an introvert, showing symptoms of obsessive-compulsive disorder, anxiety, paranoia, and a troubled youth. He works as a waiter and has an indifferent girlfriend, Emma, who only seems to patronize him. The "appointments" are with his psychiatrist (Rowan Atkinson), who is annoyed with him and uninterested in what he has to say. After finding his doctor sharing his (Denniss) intimate secrets with a group of fellow psychiatrists at a bar, and then finding that his girlfriend is cheating on him with the doctor, Dennis decides he has had enough.

==References==
 

==External links ==
*  

 

 
 
 
 
 
 
 
 