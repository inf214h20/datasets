Distant Lights (2003 film)
{{Infobox film
| name           = Distant Lights
| image          = LichterDVDCover.jpg
| director       = Hans-Christian Schmid
| writer         = Michael Gutmann, Hans-Christian Schmid Michael Gerber Herbert Knaup August Diehl Henry Hübchen
| producer       = Jakob Claussen, Thomas Wöbke
| music          = The Notwist
| cinematography = Bogumil Godfrejów
| editing        = Bernd Schlegel, Hansjörg Weißbrich Universal
| released       = 31 July 2003
| runtime        = 105 minutes
| language       = German, Polish, Russian, Ukrainian
| budget         =
}}
Distant Lights is a 2003 Germany film directed by Hans-Christian Schmid. The film takes place on the Polish-German border at Słubice and Frankfurt (Oder). It features an ensemble cast, and the various threads illustrate the daily life between the two countries. Its original German title is Lichter, which means "Lights".

== Plot ==
Unlike many ensemble films, the subplots of the film mostly do not interconnect with each other, and the film ends without a finale. Instead, the story threads illustrate life on the border between two countries; what appears to be poverty and desperation to some is a promised land for others, worth risking their lives to reach. The threads are:

*Kolya, a Ukrainian, has paid to be smuggled into Germany illegally; he finds he has been left on the Polish side of the border. When he is caught, a German interpreter agrees to smuggle him herself; he reaches Berlin.
*Ingo, a German mattress salesman, finds business in Frankfurt to be minimal and the people self-interested. He is humiliated and becomes desperate, but may have found love in Simone. communion dress homage to All or Nothing.)
*Philip, a young German architect, runs into his Polish former girlfriend Beata while working on a building project in Słubice. Neither the romance nor the project works out.
*Anna and Dimitri, a young Ukrainian couple, are swindled out of their money and try with Antonis help to cross the river Oder, almost drowning in the attempt.
*Andreas, a young German orphan who smuggles cigarettes, attempts to take the money and run away with a girl from the childrens home.

==Cast==
*Iwan Shwedoff as Kolya
*Devid Striesow as Ingo
*August Diehl as Philip
*Julia Krynke as Beata
*Alice Dwyer as Katharina
*Zbigniew Zamachowski as Antoni
*Maria Simon as Sonja Michael Gerber as Rainer Petzold
*Herbert Knaup as Klaus Fengler
*Henry Hübchen as Werner Wilke
*Anna Fischer

==Reception==
*The film won the FIPRESCI Prize at the 2003 Berlinale
*2003 Findling Award
*2002 Bundesfilmpreis in silver
*Best Direction, Best Script, 2003 Bavarian Film Prize

==External links==
*   
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 