I Will Walk Like a Crazy Horse
{{Infobox film
| name           = I Will Walk Like a Crazy Horse
| image          = I_Will_Walk_Like_a_Crazy_Horse_Poster.jpg
| caption        =
| director       = Fernando Arrabal
| producer       = Bernard Legargeant
| writer         = Fernando Arrabal
| starring       = Emmanuelle Riva George Shannon Hachemi Marzouk Marie France
| music          =
| cinematography = Bernard Auroux Georges Barsky Ramón F. Suárez Alain Thiollet
| editing        = Laurence Leininger
| studio         = Babylone Films Société Générale de Production
| distributor    = Luso-France
| released       =  
| country        = France
| runtime        = 100 minutes
| language       = French
| budget         =
}} George Shannon epileptic boy who, falsely suspected of murdering his mother, flees to the desert where he meets a hermit and brings him back to the city where the hermit becomes a circus performer.  

Since its release the movie has been shown at some film festivals such as the 2013 Psych Out film festival in Newcastle upon Tyne.  

==Synopsis== George Shannon) flees to the desert in order to avoid any police questioning, as they believe that he was responsible for his mothers death. It is in there that Aden meets the savage yet noble Marvel (Hachemi Marzouk). While the two men bond over their travels, Aden tries to convince Marvel that civilization is much more desirable than the wilderness, although Marvel appears to disagree. During all of this the police continue their relentless pursuit of Aden.

==Cast==
*Emmanuelle Riva as La mère (Madame Rey) George Shannon as Aden Rey
*Hachemi Marzouk as Marvel
*Marco Perrin as Oscar Tabak
*François Chatelet as Le prédicateur
*Marie-France Garcia|Marie-France as Bijou-Love (as Marie-France Garcia)
*Gerard Borlant as Le concierge
*Jean Chalon as Le maître dhôtel
*Raoul Curet as Commissaire Falcon
*Luc Guérin as Aden enfant
*Antoine Marin as Le boucher
*Pedro Meca as Le 1er mangeur
*Gilles Meyer as Cascadeur
*Myriam Mézières as Dell
*Camilo Otero as Le 2ème mangeur

==Reception==
DVD Talk gave a mixed review, saying that the movie would appeal most to fans of surrealist cinema and that "Some of its graphic nature may have dulled over time, some not, and it is a bit heavy-handed, but it still holds up as an interesting work for those open to the distinctive surrealist storytelling style." 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 