Rise of the Planet of the Apes
{{Infobox film
| image          = Rise of the Planet of the Apes Poster.jpg
| caption        = Theatrical release poster
| director       = Rupert Wyatt Rick Jaffa Amanda Silver
| writer         = Rick Jaffa Amanda Silver
| based on       =   Brian Cox Tom Felton David Oyelowo Andy Serkis
| music          = Patrick Doyle
| cinematography = Andrew Lesnie
| editing        = Conrad Buff Mark Goldblatt Ingenious Film Partners
| distributor    = 20th Century Fox
| released       =  
| runtime        = 105 minutes
| country        = United States   
| language       = English
| budget         = $93 million 
| gross          = $481,801,049 
}}
Rise of the Planet of the Apes is a 2011 American   (1972), but it is not a direct remake of that film.
 Best Visual Best Director Best Writing Best Science Best Supporting Best Special Caesar was performance capture as traditional acting. A sequel to the film, Dawn of the Planet of the Apes, was released on July 11, 2014.

==Plot==
  chimpanzees to Julius Caesar, from which he can, despite his deteriorating mental condition, cite long passages from memory. Will learns that Caesar has inherited his mothers high intelligence (the 112 virus passing to him in utero) and decides to raise him, working from home and observing his behavior in hopes that he can get the project restarted. Three years later, Will introduces Caesar to the redwood forest at Muir Woods National Monument. Meanwhile, with Charles condition rapidly deteriorating, Will treats him with ALZ-112 and he is restored to better-than-original cognitive ability.
 treated cruelly by the other chimps and the chief guard, Dodge Landon. Caesar learns how to unlock his cage, gaining free access to the common area. With the assistance of Buck, a gorilla, he confronts the sanctuarys alpha chimp and claims that position.
 bribing the head of the shelter to release him), but the chimp refuses to go home with him. Instead, he escapes from the facility and returns to Wills house, where he takes canisters of the ALZ-113.
 electrocutes the cattle prod-wielding Dodge by spraying him with water, unintentionally killing him. The apes flee the facility, release the remaining apes from Gen-Sys, and free the other apes from the San Francisco Zoo.

A battle ensues as the ape army fight their way past a police blockade on the Golden Gate Bridge to escape into the redwood forest. Buck sacrifices himself to save Caesar by jumping into the helicopter in which Jacobs is riding. The helicopter crashes onto the bridge, trapping Jacobs in the wreckage. Jacobs is then killed by Koba. As the apes find their way into the forest, Will arrives and warns Caesar that the humans will hunt them down, and begs him to return home. In response, Caesar hugs him and says that "Caesar is home." Will, realizing that this is indeed their last farewell, respects Caesars wishes.

During the credits, Hunsiker (having been infected by Franklin) leaves his house for work as an airline pilot, arriving at San Francisco International Airport for his flight to Paris. His nose begins to drip blood onto the floor. A graphic traces the spread of the humanity-killing virus to Europe and then around the globe via international airline flight routes.

==Cast==
 

===Humans===
* James Franco as Dr. William "Will" Rodman, a scientist who is trying to discover a cure for his fathers Alzheimers disease by testing ALZ-112 on chimps. He is a father figure to Caesar. James Franco was cast after talks with Tobey Maguire broke down.  
* Freida Pinto as Caroline Aranha, a primatologist who starts a relationship with Will and grows attached to Caesar.
* John Lithgow as Charles Rodman, Wills Alzheimers-afflicted father and a former music teacher who improves after Will gives him the ALZ-112 and forms a strong bond with Caesar. Brian Cox Planet of the Apes. Planet of the Apes.
* David Oyelowo as Steven Jacobs, Wills greedy boss. His last name is a reference to Arthur P. Jacobs, the producer of the original Planet of the Apes (franchise)|Planet of the Apes series.
* Tyler Labine as Robert Franklin, a chimp handler at Gen-Sys and one of Wills friends.
* Jamie Harris as Rodney, a caretaker and a nightwatchman who is much kinder to the apes at the sanctuary and is regularly victimized by Dodge for this.
* David Hewlett as Douglas Hunsiker, Wills hot headed neighbor.
* Chelah Horsdal as Irena, a nurse who is looking after Charles.

===Apes=== Caesar from Conquest of the Planet of the Apes and Battle for the Planet of the Apes of the original series. Maurice Evans, Planet of the Apes (1968) and Beneath the Planet of the Apes (1970). Konoval also cameos as the court clerk whom Will briefly argues with about his appeal.
* Terry Notary as Rocket, the dominant chimpanzee at the ape sanctuary, until Caesar overthrows him. His name references the set decorator of Planet of the Apes, Norman Rockett. Notary also plays Bright Eyes, Caesars mother who was captured in Africa. Her name is the nickname given to Charlton Hestons human character by Zira in the 1968 film.  
* Richard Ridings as Buck, a western lowland gorilla who pledges his allegiance to Caesar after he is freed by him. His name is a reference to Buck Kartalian, who played the gorilla Julius in the 1968 film and the gorilla Frank in Conquest of the Planet of the Apes.
* Devyn Dalton as Cornelia, a female chimpanzee in the ape sanctuary. Her name is based on that of Cornelius (Planet of the Apes)|Cornelius, played by Roddy McDowall in the original Planet of the Apes. dominant male chimpanzee of Bright Eyes troop and Caesars father.
* Christopher Gordon as Koba, a scarred bonobo who has spent most of his life in laboratories and holds a grudge against humans. He is named Koba after an alias often used by Joseph Stalin.

==Production==

===Development and writing===
In 2006, screenwriter-producer Rick Jaffa was searching for a script idea. As Jaffa searched a newspaper articles  s treatment as a captive in the original film.  Rebecca Keegan (August 11, 2011).  . Los Angeles Times. Retrieved 2011-11-06.  

In a segment of a video blog post, director  ."  In a 2009 interview, Wyatt said, "Weve incorporated elements from Conquest of the Planet of the Apes, in terms of how the apes begin to revolt, but this is primarily a prequel to the 1968 film...Caesar is a revolutionary figure who will be talked about by his fellow apes for centuries...This is just the first step in the evolution of the apes, and theres a lot more stories to tell after this. I imagine the next film will be about the all-out war between the apes and humans." 

===Filming===
Filming began in July 2010 in Vancouver, British Columbia.    Filming also happened in San Francisco, California (the primary setting of the film),  and around Oahu, Hawaii, which doubled for the African jungle as the schedule and budget did not allow for location shooting in Africa. Rupert Wyatt audio commentary, Rise of the Planet of the Apes Blu-ray 

===Visual effects=== anthropomorphic ones of the original Apes film franchise, the producers decided not to use actors in make-up or ape suits. After considering real apes, instead Weta Digital created the apes digitally in almost every case through performance capture. "A New Generation of Apes," Rise of the Planet of the Apes Blu-ray  Advances in the technology allowed the use of performance capture in an exterior environment, affording the film-makers the freedom to shoot much of the film on location with other actors, as opposed to the confines of a soundstage.   The main breakthrough was a camera that enabled viewing the motion capture dots in daylight, employed mostly for the Golden Gate Bridge battle. A maximum of six actors could have their movements captured, with larger ape crowds using fully digital animals animated using Wetas move library. The Golden Gate Bridge set used both a physical set which was extended digitally, and a fully computer-generated model of the bridge that also included the ocean and nearby hills. 
 pupil dilation, and light refraction.   While Andy Serkis was the primary performer for Caesar, as the effects team considered that at times "Andy overcame the character," other motion capture team actors were also used, especially Devyn Dalton, whose height matched that of a chimpanzee. Along with that, they used Notary to play Caesar in stunt-filled scenes such as the Golden Gate Bridge scene. 

===Music===
The score for the film was written by Patrick Doyle and performed by the Hollywood Studio Symphony conducted by James Shearman.  The main concern was to have the music help progress the plot in the scenes without dialogue, for instance, conveying the emotions of Caesars relationships with Will and Charles. To turn the score into a "driving force that keeps audiences paying attention," Doyle employed an African-American chorus and focused on percussion and "low and deep" orchestra sounds. Doyle collaborated closely with the sound department to make the music complement the sound effects, including writing a recurring theme based on their recording of a chimpanzee. 

==Reception==

===Critical response===
Reviews for Rise of the Planet of the Apes have been positive, with review aggregator  , reports a score of 68 based on 39 reviews. 
 Daily News labelled Rise of the Planet of the Apes as the summers best popcorn flick.  Nick Pinkerton of The Village Voice wrote, "Caesars prison conversion to charismatic pan-ape revolutionist is near-silent filmmaking, with simple and precise images illustrating Caesars General-like divining of personalities and his organization of a group from chaos to order."  Roger Moore of Orlando Sentinel wrote, "Audacious, violent and disquieting, "Rise of the Planet of the Apes" is a summer sequel thats better than it has any right to be." He gave the film 3.5 out of 4 stars.  Manohla Dargis of The New York Times praised the film by saying, "Precisely the kind of summer diversion that the studios have such a hard time making now. Its good, canny-dumb fun." She also gave it 3.5 out of 4 stars. 

===Box office===
Rise of the Planet of the Apes was a surprise hit upon release. The film made its debut in the United States and Canada on roughly 5,400 screens within 3,648 theaters.  It grossed $19,534,699 on opening day and $54,806,191 in its entire opening weekend, making it #1 for that weekend as well as the fourth highest-grossing August opening ever.  The film held on to the #1 spot in its second weekend, dropping 49.2%, and grossing $27,832,307.  Rise of the Planet of the Apes crossed the $150 million mark in the United States and Canada on its 26th day of release. Entertainment Weekly said that this was quite an accomplishment for the film since the month of August is a difficult time for films to make money. 

The film ended its run at the box office on December 15, 2011, with a gross of $176,760,185 in the U.S. and Canada as well as $305,040,864 internationally, for a total of $481,801,049 worldwide.   

=== Home media ===
Rise of the Planet of the Apes was released on Blu-ray Disc, DVD, and Digital Copy on December 13, 2011. 

===Awards===
{| class="wikitable"
|-
!Award !! Category !! Recipient !! Result
|-
| 84th Academy Awards Best Visual Effects Daniel Barrett
|  
|-
| Alliance of Women Film Journalists 
| Best Supporting Actor
| Andy Serkis
|  
|-
| Annie Awards 
| Character Animation in a Live Action Production
| Eric Reynolds
| 
|-
| rowspan="3" | Broadcast Film Critics Association  Best Supporting Actor
| Andy Serkis
|  
|- Best Visual Effects
|  
|- Best Action Film
|  
|- Empire Awards  Empire Award Best Film
|  
|- Empire Award Best Sci-Fi/Fantasy
|  
|- Best Director
| Rupert Wyatt
|  
|- Best Actor
| Andy Serkis
|  
|-
| Genesis Awards Best Feature Film  Rick Jaffa and Amanda Silver
|   
|- Houston Film Critics Society 
| Best Supporting Actor
| Andy Serkis
|  
|-
|colspan=2| Technical Achievement
|  
|- IGN Best of 2011 
|colspan=2| Best Movie
| 
|-
|colspan=2| Best Sci-Fi Movie
| 
|-
| Best Movie Actor The Adventures of Tintin) 
| 
|-
| Best Movie Director
| Rupert Wyatt
| 
|-
| rowspan="7" | IGN Summer Movie Awards 
|- Best Summer Movie
|  
|-
| Funniest Line
| "Why cookie Rocket?"
|  
|-
| Best All-Out Brawl
| Apes vs. Humans on the Golden Gate Bridge
|  
|-
| Favorite Kill
| Helicopter Pushed Over the Golden Gate Bridge
|  
|-
| Coolest Creature Caesar
|  
|-
| Favorite Hero
| Caesar – Andy Serkis
|  
|-
| Las Vegas Film Critics Society 
|colspan=2| Best Visual Effects
|  
|-
| London Film Critics Circle
| Technical Achievement
| Joe Letteri
|  
|-
| Phoenix Film Critics Society Best Visual Effects
|  
|-
| San Diego Film Critics Society 
| Best Supporting Actor
| Andy Serkis
|  
|-
| rowspan="2" | Satellite Awards  Best Supporting Actor – Motion Picture
| Andy Serkis
|  
|- Best Visual Effects
| Jeff Capogreco, Joe Letteri, R. Christopher White
|  
|- 38th Saturn Saturn Awards  Saturn Award Best Science Fiction Film
| 
|- Saturn Award Best Supporting Actor Andy Serkis
| 
|- Saturn Award Best Director Rupert Wyatt
| 
|- Saturn Award Best Writing Rick Jaffa and Amanda Silver
| 
|- Saturn Award Best Special Effects Dan Lemmon, Joe Letteri, R. Christopher White, and Daniel Barrett
| 
|-
| rowspan="4" | Visual Effects Society  Outstanding Visual Effects in a Visual Effects-Driven Feature Motion Picture
| Dan Lemmon, Joe Letteri, Cyndi Ochs, Kurt Williams
|  
|-
| Outstanding Animated Character in a Live Action Feature Motion Picture
| Caesar – Daniel Barrett, Florian Fernandez, Matthew Muntean, Eric Reynolds
|  
|-
| Outstanding Virtual Cinematography in a Live Action Feature Motion Picture
| Thelvin Cabezas, Mike Perry, R. Christopher White, Erik Winquist
|  
|-
| Outstanding Compositing in a Feature Motion Picture
| Jean-Luc Azzis, Quentin Hema, Simon Jung, Christoph Salzmann 
|  
|-
| Washington D.C. Area Film Critics Association  Best Supporting Actor
| Andy Serkis
|  
|-
|}

==Sequel==
 
Regarding the story setting up possible sequels, director Rupert Wyatt commented: "I think were ending with certain questions, which is quite exciting. To me, I can think of all sorts of sequels to this film, but this is just the beginning."  Screenwriter and producer Rick Jaffa also stated that Rise of the Planet of the Apes would feature several clues as to future sequels: "I hope that were building a platform for future films. Were trying to plant a lot of the seeds for a lot of the things you are talking about in terms of the different apes and so forth." 

On May 31, 2012, 20th Century Fox announced that the sequel would be named Dawn of the Planet of the Apes.  Reports said that Wyatt was leaving the sequel due to his concern that a May 2014 release date would not give him enough time to properly make the film;  he was replaced by Cloverfield director Matt Reeves.  Jaffa and Silver returned as producers and to pen the screenplay, with rewrites from Scott Z. Burns  and Mark Bomback. 

Taking place ten years after Rise, Dawn follows Caesars growing nation of evolved apes threatened by human survivors that put both sides in the brink of war.    Andy Serkis, Terry Notary and Karin Konoval reprised their roles as Caesar, Rocket and Maurice.  James Franco returned as Will Rodman in a "cameo via video".  Judy Greer and Toby Kebbell replace Devyn Dalton and Christopher Gordon as Cornelia and Koba.  Filming of Dawn of the Planet of the Apes started on North Vancouver Island in April 2013.  The film was released July 11, 2014.

== See also ==
 

== Notes and references ==
 

==External links==
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 