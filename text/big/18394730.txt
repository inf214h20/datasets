How to Train Your Dragon (film)
 
 
{{Infobox film name = How to Train Your Dragon image = How to Train Your Dragon Poster.jpg caption = Theatrical release poster director =   producer = Bonnie Arnold screenplay =   based on =   narrator = starring =   music = John Powell editing =   studio = DreamWorks Animation distributor = Paramount Pictures released =   runtime = 98&nbsp;minutes country = United States language = English budget = $165&nbsp;million  gross = $494.8 million   
}} 3D computer book series Chris Sanders and Dean DeBlois, the duo who directed Walt Disney Animation Studios|Disneys Lilo & Stitch. It stars the voices of Jay Baruchel, Gerard Butler, Craig Ferguson, America Ferrera, Jonah Hill, T.J. Miller, Kristen Wiig, and Christopher Mintz-Plasse.
 Viking world where a young Viking teenager named Hiccup aspires to follow his tribes tradition of becoming a dragon slayer. After finally capturing his first dragon, and with his chance at at last gaining the tribes acceptance, he finds that he no longer wants to kill it and instead befriends it.
 Best Original Best Animated Feature.
 video game TV series.

==Plot==
  prosthetic fin that allows him to guide the dragon in free flight.  By studying Toothless behavior, Hiccup becomes proficient in subduing the captive dragons kept for training. Stoicks fleet arrives home unsuccessful, but he is cheered by Hiccups success.

Astrid, a tough Viking girl who Hiccup has a crush on, discovers Hiccup is training Toothless but before she can tell the village, Hiccup takes her for a ride to demonstrate his relationship with the dragon.  Toothless unexpectedly takes the pair to the dragons nest where they discover a gigantic dragon named the Red Death   that feeds on the smaller dragons unless they bring it stolen livestock.

Back at the village, Hiccup subdues a captive dragon in front of his father but Stoick mistakenly angers the dragon. Toothless attempts to protect Hiccup in the ensuing panic but is instead captured by the Vikings.  Hiccup accidentally reveals to Stoick that Toothless is capable of locating the dragons nest.  Stoick disowns his son and sets off for the nest with Toothless as a guide. The Vikings expel most of the dragons but are overwhelmed by the Red Death until Hiccup, Astrid and their fellow-pupils fly in riding the training dragons from the academy and provide cover fire.  Hiccup almost drowns trying to break Toothless free from a sinking ship but Stoick saves them both and then reconciles with his son. Toothless and Hiccup destroy the Red Death but Hiccups left foot is torn off in the fight. Hiccup regains consciousness on Berk where his foot has been replaced by a prosthesis.  The Vikings and the dragons now live in peace.

==Voice cast==
* Jay Baruchel as Hiccup Horrendous Haddock III: protagonist, and companion of Toothless, a Night Fury.
::Producer Bonnie Arnold declared that Hiccups appeal emerges from Baruchels voice acting and the characters mannerisms making him a "slightly offbeat character." "Viking-Sized Cast", How to Train Your Dragon Blu-ray 

* Gerard Butler as Stoick the Vast, the chieftain of the Viking tribe and Hiccups father. A fierce, immensely strong and utterly fearless, warrior.
::Director Chris Sanders described Stoick as "representing everything thats Viking", and thus a very important character in how he intimidates his son Hiccup. Butler considered that "half my career led me into playing a role like this," particularly Beowulf & Grendel, which is set in the Viking age, and was trying to avoid depicting Stoick as villainous, instead aiming for "a character that we can all identify, hes not just big and tough, also vulnerable." 

* Craig Ferguson as Gobber the Belch: blacksmith, a close friend of Stoicks, and training master of the young warriors. He is the mediator between Hiccup and Stoick. He is missing his right foot and his left hand, the latter of which he has replaced with a variety of specialized prosthetics. 
::Director Dean Deblois said Gobbers characterization drew a lot from how Fergusons comedic routine "takes a really dark situation and phrases it in a way theres always a punchline." 

* America Ferrera as Astrid Hofferson, Hiccups fellow-student.
::DreamWorks had long been interested in having Ferrera voicing one of their movies and, once invited, the actress picked up  Cressida Cowells book, which made her accept the job. Bonnie Arnold complimented how Ferrera "has a strong voice, but also a lot of heart in it" that helped make Astrid sympathetic even as she continued being rough to Hiccup. 

*   terms.  
::Bonnie Arnold declared Mintz-Plasses voice made them immediately think of Fishlegs, as the characters big frame needed something to contrast, and the actors "squeaky, small voice" was perfect to complement a "dragon nerd". 

* Jonah Hill as Snotlout Jorgenson, one of Hiccups dragon-training classmates. Snotlout is brash, overconfident, and fairly unintelligent but, at times, reliable.

* T. J. Miller and Kristen Wiig as Tuffnut and Ruffnut Thorston, a pair of quarrelsome twins. 

* David Tennant as Spitelout. A Viking who is not named in the film, he appears to be Stoicks second-in-command and Snotlouts father. Tennant has previously narrated a series of Hiccup adventures on audio book. 
* Robin Atkin Downes as Ack
* Philip McGrade as Starkard
* Kieron Elliot as Hoark the Haggard
* Ashley Jensen as Phlegma the Fierce

==Production==
  Over the Hedge, producer Bonnie Arnold shortly became interested in the newly acquired property. She kept focusing on the project as time went on, and when DreamWorks Animation co-president of production Bill Damaschke asked her what she wanted to work on next, she chose “How to Train Your Dragon”. 

In initial development, the plot followed the original novel closely, but about halfway through production   (known for frequently collaborating with the Coen brothers) as a visual consultant to help them with lighting and overall look of the film and to "add a live-action feel".  Extensive research was done to depict both flight, as the directors knew they would be the biggest draw of the films 3D effects, and fire, given animation could break away from the limitations seen in live-action films, where propane flames are usual due to being easier to extinguish. The dragons design made sure to create animals that were comical and also innovative compared to other dragon fiction. Toothless in particular tried to combine various dragon traits in a black panther-inspired design, that also had large ears and eyes to convey emotion better. "The Technical Artistry of Dragon", How to Train Your Dragon Blu-ray 

The directors made sure to cash in the improvisation abilities of the secondary cast&nbsp;— Christopher Mintz-Plasse, Jonah Hill, Kristen Wiig and T.J Miller&nbsp;— by frequently bringing them together in the recording sessions. 

 

==Music==
  John Powell returned to DreamWorks Animation to score How to Train Your Dragon, making it his sixth collaboration with the studio, following his previous score for Kung Fu Panda (which he scored with Hans Zimmer). Powell composed an orchestral score, combining bombastic brass with loud percussion and soothing strings, while also using exotic, Scottish and Irish tones with instruments like the penny whistle and bagpipes. Additionally, Icelandic singer Jón Þór Birgisson|Jónsi wrote and performed the song "Sticks & Stones" for the film. The score was released by Varèse Sarabande on March 23, 2010.
 Academy Award nomination for his work on the film, ultimately losing to Trent Reznor and Atticus Ross for their score for The Social Network.

==Release== IMAX 3D, and released to 186 North American IMAX theatres, and approximately 80 IMAX theatres outside North America. 

===Competition for 3D screens=== Clash of the Titans from 2D to 3D, then to release it one week after How to Train Your Dragon.  Entertainment reporter Kim Masters described the 3D release schedule around March 2010 as a "traffic jam", and speculated that the lack of 3D screen availability could hurt Katzenbergs prospects despite his support of the 3D format. 
 Alice in Wonderland. As theater multiplexes often had just one 3D screen, theaters were unable to accommodate more than one 3D presentation at a time. 

===Box-office=== 10th highest-grossing movie of 2010. 

===Critical reception===
How To Train Your Dragon received universal acclaim upon its release. Review aggregator Rotten Tomatoes gives the film a score of 98% based on 183 reviews from professional critics, with an overall rating average of 7.9 out of 10.    The sites general consensus is that "Boasting dazzling animation, a script with surprising dramatic depth, and thrilling 3-D sequences, How to Train Your Dragon soars."  By "Rotten Tomatoes#Description|Tomatometer" score, the film is DreamWorks Animations most critically successful film ever. On Metacritic, which assigns a weighted mean rating out of 0–100 reviews from film critics, the film has a rating score of 74 based on 33 reviews, indicating Generally favorable reviews.   CinemaScore polls conducted during the opening weekend revealed the average grade cinemagoers gave How to Train Your Dragon was A on an A+ to F scale. 
 Rolling Stone At The Movies felt the characters and the story were not strong points, but loved the cinematography and said, "that swooping and soaring, they are worth the price of a ticket, so go see it."      Village Voice film critic Ella Taylor panned the film describing it as an "adequate but unremarkable animated tale".    Film critic James Berardinelli of ReelViews praised the film and its story, giving it 3.5 out of 4 stars he wrote, "Technically proficient and featuring a witty, intelligent, surprisingly insightful script, How to Train Your Dragon comes close to the level of Pixar|Pixars recent output while easily exceeding the juvenilia DreamWorks has released in the last nine years."  Entertainment Weekly film critic Owen Gleiberman praised the film giving it an A- and wrote, "How to Train Your Dragon rouses you in conventional ways, but its also the rare animated film that uses 3-D for its breathtaking spatial and emotional possibilities." 

===Accolades===
{| class="wikitable"
|- style="background:#ccc; text-align:center;"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Name
! Outcome
|- Academy Awards 
| Academy Award for Best Animated Feature Chris Sanders Dean DeBlois
|rowspan=3 align="center  
|-
| Academy Award for Best Original Score John Powell
|-
| rowspan=2|Alliance of Women Film Journalists 
| Best Animated Feature
|
|-
| Best Animated Female
| America Ferrera (Astrid)
| rowspan=3 align="center"  
|- Annie Awards  
| Annie Award for Best Animated Feature Chris Sanders Dean DeBlois
|- Annie Award for Best Animated Effects in an Animated Production
| Brett Miller
|-
| Jason Mayer
|  
|- Annie Award for Best Character Animation in a Feature Production
| Gabe Hordos
|  
|-
| Jakob Hjort Jensen
|rowspan=2 align="center  
|-
| David Torres
|- Annie Award for Best Character Design in an Animated Feature Production
| Nico Marlet
|rowspan=5 align="center  
|- Annie Award for Best Directing in an Animated Feature Production
| Chris Sanders Dean DeBlois
|- Annie Award for Best Music in an Animated Feature Production John Powell
|- Annie Award for Production Design in an Animated Feature Production
| Pierre-Olivier Vincent
|- Annie Award for Best Storyboarding in an Animated Feature Production
| Tom Owens
|-
| Alessandro Carloni
|  
|- Annie Award for Best Voice Acting in an Animated Feature Production
| Jay Baruchel (Hiccup)
|  
|-
| Gerard Butler (Stoick)
|  
|- Annie Award for Best Writing in an Animated Feature Production William Davies Chris Sanders Dean DeBlois
|  
|- British Academy Film Awards 
| BAFTA Award for Best Animated Film
|
|rowspan=4 align="center  
|-
| BAFTA Award for Best Film Music John Powell
|- Chicago Film Critics Association Awards  Best Animated Film
| Chris Sanders Dean DeBlois
|-
||16th Critics Choice Awards|Critics Choice Awards  Best Animated Feature Film
|
|-
||Genesis Awards  Best Feature Film
|
| 
|- Golden Globe Awards  Golden Globe Award for Best Animated Feature Film
|
|  
|- Golden Reel Awards  Best Sound Editing in an Animated Feature Film
|
|rowspan=3 align="center  
|-
|rowspan=4|International Film Music Critics Association  
| Film Score of the Year John Powell
|-
| Best Original Score for an Animated Feature John Powell
|- Film Music Composition of the Year John Powell&nbsp;– "Forbidden Friendship"
|rowspan=10 align="center  
|- John Powell&nbsp;– "Test Drive"
|-
|Kids Choice Awards 
| Favorite Animated Movie
|
|- Online Film Critics Society Awards  Best Animated Feature
| Chris Sanders Dean DeBlois
|-
||37th Peoples Choice Awards|Peoples Choice Awards  Favorite Family Movie
|
|-
||Satellite Awards  Motion Picture (Animated or Mixed)
|
|- Saturn Awards  Saturn Award for Best Music John Powell John Powell
|- Saturn Award for Best Production Design Kathy Altieri
|- Saturn Award for Best Animated Film Chris Sanders Chris Sanders Dean DeBlois
|-
|Teen Choice Awards  Choice Animated Movie
|
|-
||Toronto Film Critics Association  Toronto Film Critics Association Award for Best Animated Film
|
|rowspan=5 align="center  
|-
||Venice Film Festival  Most Creative 3D Film of the Year Chris Sanders and Dean DeBlois Tied with James Cameron for Avatar (2009 film)|Avatar
|-
|style="border-top:10px black" rowspan=3| Visual Effects Society 
| Visual Effects Society Award for Outstanding Animation in an Animated Feature Motion Picture
| Simon Otto Craig Ring Bonnie Arnold
|-
| Visual Effects Society Award for Outstanding Animated Character in an Animated Feature Motion Picture
| Gabe Hordos Cassidy Curtis Mariette Marinus Brent Watkins
|-
| Visual Effects Society Award for Outstanding Effects Animation in an Animated Feature Motion Picture
| Andy Hayes Laurent Kermel Jason Mayer Brett Miller
|- Washington D.C. Area Film Critics Association Awards  Best Animated Feature
| Chris Sanders Dean DeBlois
|rowspan=3 align="center  
|-
|rowspan=2|World Soundtrack Academy  World Soundtrack Award for Soundtrack Composer of the Year John Powell John Powell
|- World Soundtrack Award for Best Original Song Written Directly for a Film
|Jón Þór Birgisson
|}

===Home media===
How to Train Your Dragon was released on single-disc DVD, 2-disc Double DVD Pack and Blu-ray/DVD combo pack in Canada and the United States on Friday, October 15, 2010. Among the features available in the 2-disc DVD edition is an original sequel short film, Legend of the Boneknapper Dragon. As of July 18, 2012, units sold for the DVD stand at more than 6.5 million copies and has grossed $121,663,692. 

Samsung signed a deal with DreamWorks to gain exclusive distribution rights to a Blu-ray 3D version of the film. The exclusive agreement ended on September 11, 2011 when How to Train Your Dragon - 3D was released in a Two-Disc Blu-ray combo pack.
 Paramount Home Entertainment on Blu-ray and DVD on May 27, 2014. 

==Sequels==
 

A sequel, How to Train Your Dragon 2, was confirmed on April 27, 2010.  The film was directed and written by Dean DeBlois, the co-director of the first film. Bonnie Arnold, the producer of the first film, also returned.  The film was released on June 13, 2014, to critical acclaim.       The entire original voice cast&nbsp;– Baruchel, Butler, Ferguson, Ferrera, Hill, Mintz-Plasse, Miller and Wiig&nbsp;– returned for the sequel. 

A third film, How to Train Your Dragon 3, is in development. The film will also be directed and written by DeBlois, produced by Bonnie Arnold, and exec-produced by Chris Sanders, with all the main cast set to return. It is scheduled to be released on June 29, 2018. 

==Other media==
 

===Short films===
Four post-movie   (2010), Book of Dragons (2011), Gift of the Night Fury (2011) and Dawn of the Dragon Racers (2014). 

===Television series===
 
A television series based on the film  premiered on Cartoon Network in Autumn 2012. Jay Baruchel, America Ferrera, Christopher Mintz-Plasse and T. J. Miller  reprise their roles as Hiccup, Astrid, Fishlegs and Tuffnut. The series follows Hiccup and his friends as they learn more about dragons, discover new ones, teach others to feel comfortable around them, adapt traditions within the village to fit their new friends and battle against enemies as they explore new worlds. Hiccup has been made head of Berk Dragon Academy.   

===Video games=== action adventure How to Train Your Dragon, was released for the Wii, Xbox 360, PS3 and Nintendo DS gaming consoles. It is loosely based on the film and was released on March 23, 2010.
 Android and iOS. 

===Ice show===
 
A Broadway-style production named How To Train Your Dragon ON ICE is on Royal Caribbeans Allure of the Seas. 

===Arena show===
 
How To Train Your Dragon Arena Spectacular is an arena show adaptation of the first film featuring 24 animatronic dragons, acrobats and projections. It premiered on March 2, 2012, in Melbourne, Australia. 

==See also==
 
* List of animated feature-length films
* List of computer-animated films

==Notes and references==

===Notes===
 

===References===

 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 