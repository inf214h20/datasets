Chayam (1973 film)
{{Infobox film
| name           = Chayam
| image          =
| caption        = PN Menon
| producer       = SK Nair
| writer         = Malayattoor Ramakrishnan
| screenplay     = Malayattoor Ramakrishnan Raghavan Adoor Pankajam
| music          = G. Devarajan
| cinematography = Ashok Kumar
| editing        = Ravi
| studio         = New India Films
| distributor    = New India Films
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, PN Menon Raghavan and Adoor Pankajam in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Sheela
*Sankaradi Raghavan
*Adoor Pankajam
*CA Balan
*Roja Ramani Sudheer
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar and Kannadasan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme Amme || Ayiroor Sadasivan || Vayalar ||
|-
| 2 || Chaayam karutha chaayam || P. Madhuri || Vayalar ||
|-
| 3 || Gokulaashtami Naal || P. Madhuri || Vayalar ||
|-
| 4 || Maariyamma Thaaye || P. Madhuri, TM Soundararajan || Kannadasan ||
|-
| 5 || Oshaakali || Adoor Bhasi, Chorus || Vayalar ||
|-
| 6 || Sreevalsam Maaril || Ayiroor Sadasivan || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 


 