The Substitute
 
{{Infobox film
| name           = The Substitute
| image          = Substituteposter.jpg
| caption        = Theatrical release poster
| director       = Robert Mandel
| producer       =
| writer         = Roy Frumkes   Rocco Simonelli Richard Brooks William Forsythe
| music          =
| cinematography =
| editing        =
| distributor    = Orion Pictures Live Entertainment
| released       =  
| runtime        = 114 minutes
| country        = United States English
| gross          = $14,818,176
}}
 thriller film William Forsythe, Raymond Cruz and Luis Guzmán.

==Plot==
Shale (Berenger) is a mercenary and a Vietnam veteran who returns home to Miami after a botched covert operation in Cuba in which three men from his platoon were killed. He surprises his girlfriend, Jane Hetzko (Diane Venora) at her apartment and is warmly welcomed. On the outside, Jane is a schoolteacher at inner-city Columbus High School, an institution with a gang problem, complete with metal detectors and gates protecting its boundaries. She also clashes with Juan Lacas (Anthony), leader of the KOD ("Kings of Destruction") gang and has her leg broken in a beating while attacked on a morning jog. Jane and Shale believe this to be related to the KOD, which prompts the latter to go undercover as an Ivy League-educated, government-affiliated substitute teacher for his girlfriends class.

Shale arrives at Columbus High School and is, at first, taken back by the lowly conditions. He is unable to control his class of poorly-educated students on the first day, but decides to use his street-smarts and military tactics to gain the upper hand. Soon enough, he is able to take command of the students by displaying his combat self-defence techniques when students attack him. He is warned not to use such methods by Principal Claude Rolle (Hudson), but gains the respect of his students when he bonds with them over the similarities between his early gang and Vietnam War experiences and their involvement in petty crime and street gangs. During this time, he befriends fellow schoolteacher Darrell Sherman (Plummer) and also crosses paths with Lacas, one of his students.

Suspicious of odd conditions within the high school, Shale sets up surveillance cameras throughout the building. He discovers that Lacas orchestrated the attack on Jane. He also discovers that Lacas is secretly working with Rolle to distribute cocaine around Miami for a major narcotics ring. Shale and his team raid a drug deal, using the stolen money to buy music and sports equipment in the form of a "school donation." While Sherman initially denies Shales discovery, Sherman and a female student inadvertently witness the drugs being loaded into one of the school buses later that day. Sherman tells the student to warn Shale and Hetzko, and sacrifices himself by creating a distraction. Rolle, who at this point is aware of Shales interference orders a "car accident" for Shale, and sends Lacas after Hetzko. With the help of another student, Shale saves Hetzko, learning the full story from the female witness. Shale and his team garrison the school grounds to enter combat against the remaining K.O.D. members, a rival mercenary company led by Janus, and Rolle himself. Ultimately, Shale and Joey Six end up as the sole survivors of the battle, walking away from the school grounds discussing future operations as substitute teachers.

==Cast==
*Tom Berenger as Jonathan Shale/Mr. James Smith
*Ernie Hudson as Principal Claude Rolle
*Diane Venora as Jane Hetzko
*Marc Anthony as Juan Lacas
*Glenn Plummer as Mr. Darrell Sherman
*Cliff De Young as Matt Wolfson William Forsythe as Hollan
*Raymond Cruz as Joey Six
* Sharron Corley as Jerome Brown
*Luis Guzmán as Rem Richard Brooks as Wellman
* David Spates as Michael Davis

==Soundtrack==
 
A soundtrack containing hip hop music was released on April 9, 1996 by Priority Records. It peaked at #90 on the Billboard 200|Billboard 200 and #18 on the Top R&B/Hip-Hop Albums.

==Reception==
The Substitute holds a rating of 41% on Rotten Tomatoes from 22 critics. 

  gave the film two stars out of four, writing: "The Substitute has its moments, all of which fall in the realm of high camp. ... Nevertheless, aside from a lot of only moderately-satisfying violence, The Substitute comes across as rather lame. Its not boring, but that dubious qualification isnt enough to earn the movie a passing grade." 
 The AV Club stated: "There have been plenty of movies about white people coming into inner-city schools and whipping the students into shape, but nothing quite like The Substitute, which brings the subtly racist, paternalistic elements of those films right to the surface." 

A more positive review came from   wrote: "The Substitute is a guilty pleasure, but its not garbage. Berenger brings to the role an appealing ruggedness and world-weariness, and Ernie Hudson, as the corrupt principal, is sleazy and elegant. The script isnt bad, either. The first meeting between Shale and the principal, in which they size each other up, is superb, and throughout the outlandish premise is handled with straight-faced intelligence." 

==Sequels==
Three direct-to-video|direct-to-DVD sequels were made with Treat Williams replacing Tom Berenger:

*  (1998)
*  (1999)
*  (2001)

==Home media==
The movie was first released on DVD in 1997 by LIVE Entertainment. It was re-released on DVD and bundled with The Substitute 3: Winner Takes All in 2000.

==References==
 

==External links==
*  
*  
*  at Facemelting Films

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 