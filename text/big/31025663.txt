El Desalmado
{{Infobox film
| name           =El Desalmado
| image          = 
| image size     =
| caption        =
| director       =Chano Urueta
| producer       = Pedro Galindo	
| writer         =  Alfonso Lapeña (story), Ernesto Rosas (story)
| narrator       =
| starring       = David Silva, Lilia Prado, Crox Alvarado
| music          = Jorge Pérez
| cinematography = Agustín Jiménez
| editing        = Jorge Bustos
| distributor    = 
| released       = 1950
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1950 Mexico|Mexican film. It was directed by 
Chano Urueta.

==Cast==

* 	 David Silva	 ...	Enrique Vidal
* 	 Lilia Prado	 ...	Aurora Cervantes La Jarochita
* 	 Crox Alvarado	 ...	Mayor Rosales, Comandante de policía
* 	 Dalia Íñiguez	 ...	Madre de Vidal
* 	 Fernando Casanova	 ...	Dedos de Seda
* 	 Joaquín Roche		
* 	 Julio Ahuet		
* 	 Mario Castillo		
* 	 Ernesto Rosas	 ...	Mayor de policía (as Mayor Ernesto Rosas Ruiz)
* 	 Bernardo Yllanes Valdés	 ...	Comandante de policía
* 	 José Arredondo	 ...	Agent de policía
* 	 Jorge Martínez Harlow	 ...	Agent de policía
* 	 Augustín Álvarez	 ...	Agent de policía
* 	Luis Green	 ...	Agent de policía

==External links==
*  

 
 
 
 
 


 