Riley the Cop
 
{{Infobox film
| name           = Riley the Cop
| image          = Poster of the movie Riley the Cop.jpg
| caption        = Film poster
| director       = John Ford (uncredited)
| producer       = John Ford
| writer         = Fred Stanley James Gruen
| starring       = J. Farrell MacDonald Nancy Drexel
| music          = 
| cinematography = Charles G. Clarke
| editing        = Alex Troffey Fox Film Corporation
| released       =  
| runtime        = 68 minutes 
| country        = United States  Silent English intertitles
| budget         = 
}}

Riley the Cop is a 1928 American comedy film directed by John Ford. It was a silent film with a synchronized music track.   

==Cast==
* J. Farrell MacDonald as James "Aloysius" Riley (as Farrell Macdonald)
* Nancy Drexel as Mary Coronelli
* David Rollins as David "Davy" Collins
* Louise Fazenda as Lena Krausmeyer
* Billy Bevan as Paris Cabman (uncredited)
* Mildred Boyd as Caroline (uncredited)
* Mike Donlin as Crook (uncredited)
* Otto Fries as Munich Cabman (uncredited)
* Dell Henderson as Judge Coronelli (uncredited)
* Isabelle Keith as French Woman on Pier (uncredited)
* Robert Parrish as Boy (uncredited)
* Russ Powell as Mr. Kuchendorf (uncredited)
* Harry Schultz as Hans "Eitel" Krausmeyer (uncredited)
* Ferdinand SchumannasHeink as Julius Kuchendorf (uncredited)
* Rolfe Sedan as French Restaurant Patron (uncredited)
* Harry Semels as French Policeman (uncredited) Tom Wilson as Sergeant (uncredited)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 