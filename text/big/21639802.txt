The Knife (film)
{{Infobox film
| name           = The Knife
| image          = 
| image_size     = 
| caption        = 
| director       = Fons Rademakers
| producer       = Joop Landre Karel Logher
| writer         = Hugo Claus
| narrator       = 
| starring       = Marie-Louise Videc
| music          = 
| cinematography = Eduard van der Enden
| editing        = Han Rust
| distributor    = 
| released       = 2 March 1961
| runtime        = 89 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Knife ( ) is a 1961 Dutch drama film directed by Fons Rademakers. It was entered into the 1961 Cannes Film Festival.     

==Cast==
* Marie-Louise Videc as Toni
* Reitze van der Linden as Thomas
* Ellen Vogel as Thomas Moeder
* Paul Cammermans as Oscar
* Guus Hermus as Thomas Vader
* Cor Witschge as Ratte, de soldaat
* Hetty Beck as Marie
* Mia Goossen as Tonis Moeder
* John Kuipers
* Piet van der Meulen
* Lei Pijls
* Ellen Van Stekelenburg as Waarzegster
* Janine van Wely

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 