Sundarakanda (1992 film)
{{Infobox film name           = Sundarakanda image          = Sundarakanda-M.JPG caption        =  director       = Kovelamudi Raghavendra Rao|K.Raghavendra Rao producer       = K.V.V.Satyanarayana screenplay     = K.Raghavendra Rao writer         = Paruchuri Brothers  (dialogues)  story          = K. Bhagyaraj starring  Venkatesh Meena Meena Aparna music          = M. M. Keeravani  cinematography = K. Ravindra Babu	 editing        = Kotagiri Venkateswara Rao studio         = Saudhamini Creations	 	 distributor    =  released       =    country        = India runtime        = 2:07:59 language       = Telugu budget         =  gross          =
}}
 Meena and Aparna played the lead roles with original soundtrack by M. M. Keeravani.  The film was a remake of Tamil film Sundara Kandam. The film was Super Hit at the box office. 

==Plot==
Venkateswarlu (Venkatesh) arrives to the town and gets appointed as a Telugu lecturer in the junior college where he completed his education. He is fooled by the students headed by a brilliant but mischievous student Roja (Aparna). Venteswarulu is irritated by her indicipline and finds fault for whatever she does. But this does not stop Roja to crack pranks on him. Thukaram (Brahmanandam) classmate of Venkateswarlu who is still studying as student for so long years and some other students create a fake love letter on name of Roja and keeps it in Venkateswarlus desk. Venkateswarlu misunderstands that, it is also one of the pranks of Roja and warns her and gives the letter to her. But Roja considers that he wrote the love letter and falls in love with him. Rojas classmates repeat the mischief again and again and Venkateswarlu misunderstands it as Rojas mischief and complaints to Principal (Kota Srinivasa Rao) because of which Roja is suspended without any chance to defend herself.

Later on, Venkateswarlu understands that there is no fault with Roja and apologizes her. Roja who also finds the truth insists since both their honours has been spoiled in the college it is better both can get into love and marry. Scared by her acts Venkateswarlu keeps away from her. But Roja does not lose her heart and she chases to win his love. But Venkateswarlu insists that it is not a good ethic for a teacher to fall in love with his student and reveals that he is not willing to marry her. Roja seeks TC from her college and discontinues her studies and pressures that she is not his student anymore and he can marry her. Yet Venkateswarlu does not yield to her. Roja shifts to her friends home as a paying guest which is next to Venkateswarlus house and makes her full-time job to love him. Unable to resist her torture Venkateswarlu marries a girl named Nanchari (Meena) from an orphanage which shocks Roja.

Venkateswarlu has a great dream about his wife but Nanchari does not have any qualities of his dream girl yet she manages somewhat to live to her husbands expectation. Roja understands her situation and helps Nanchari without her knowledge. But when Nanchari finds the truth she gets furious that Roja tries to snatch away her husband from her and warns her that if she ever turns her face towards her husband she would go vigorous. But later Nanchari finds that Roja was his student and feels bad for her acts. Roja and Nanchari become good friends. Roja helps Nanchari to cook, to speak in English and all the ways to attract her husband. But Roja & Nancharis relationship is not liked by Venkateswarlu and he insists Roja to keep away from their life, but eventually Rojas parents dies in an accident, because she became an orphan Nanchari brings and keeps Roja her house. 

Roja on one side helps Nanchari and on other hand makes her attempt to win Venkateswarlus love and become his wife before her death. He gives a final warning to her that Nanchari will not keep quiet if she ever comes to know Rojas attempt on him. Nanchari hears their conversation but does not react to that instead she drags to hospital for check-up. When Venkateswarlu wonders, Nanchari explains that she a got a hand-kerchief of Roja with blood stains. Finally both come to know Roja is battling with her life with cancer and counting her days. Nanchari insists Venkateswarlu to marry Roja so that her last wish is fulfilled. But Roja dies in her college, in her classroom, in the place where she sat wearing her school uniform. In her last speech recorded in a tape, she praises Venkateswarlu for being stubborn that a teacher should not marry his student at any cost. She presents her jewels to Nanchari and tells she will be born again as their child. At the end Venkateswarlu and Nanchari wear the jewel to their baby considering Roja is born again.

==Cast== Venkatesh as Venteswarulu Meena as Nanchari
*Aparna as Roja 
*Gollapudi Maruthi Rao as Single Puri Sharma
*Kota Srinivasa Rao as Principal
*Bramhanandam as Thukaram

==Soundtrack==
{{Infobox album
| Name        = Sundarakanda 
| Tagline     = 
| Type        = soundtrack
| Artist      = M. M. Keeravani
| Cover       = 
| Released    = 1992
| Recorded    = 
| Genre       = Soundtrack
| Length      = 26:43
| Label       = Surya Audio
| Producer    = M. M. Keeravani
| Reviews     =
| Last album  = Gharana Mogudu   (1992)
| This album  = Sundarakanda   (1992)
| Next album  = President Gari Pellam   (1992)
}}
Music composed by M. M. Keeravani. Lyrics written by Veturi Sundararama Murthy. All songs are blockbusters. Music released on SURYA Audio Company.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 26:43
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Sundara Kaandaku SP Balu, Chitra
| length1 = 4:33

| title2  = Ulikipadaku
| extra2  = SP Balu, Chitra
| length2 = 4:23

| title3  = Aakasana Suryudundadu-I
| extra3  = SP Balu
| length3 = 4:18

| title4  = Kokilamma 
| extra4  = SP Balu, Chitra
| length4 = 4:41

| title5  = Inka Inka  
| extra5  = SP Balu, Chitra
| length5 = 3:52

| title6  = Aakasana Suryudundadu-II 
| extra6  = Chitra
| length6 = 4:21
}}

==References==
 

==External links==
* 

 
 
 
 
 
 


 