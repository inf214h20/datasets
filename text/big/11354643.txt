Shikar (1968 film)
{{Infobox film
| name           = Shikar
| image          = Shikar1968.jpg
| image_size     =
| caption        = LP cover
| director       = Atma Ram
| producer       = Atma Ram
| writer         = Abrar Alvi, Dhruva Chatterjee
| narrator       = 
| starring       = Dharmendra Asha Parekh Sanjeev Kumar (actor)| Sanjeev Kumar
| music          = Shankar Jaikishan
| cinematography = V.K. Murthy
| editing        = Y.G. Chawhan
| distributor    = 
| released       = 1968
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Johnny Walker.
The films music is by Shankar Jaikishan and its songs were penned by Hasrat Jaipuri. Asha Bhosle won the Filmfare award for best Female Playback singer, for the arabic tuned song Parde Mein Rehne do, while Lata Mangeshkar and Asha Bhosle sang the duet song Jabse Laagi Tose Najariya.

==Plot==
Manager Ajay Singh find out that a man by the name of Naresh is murdered and inform police inspector Rai, but all evidences are tampered with at the crime scene and as a result the murderer became difficult to trace. From here on Ajays life takes a new turn in trying to find the culprit who have murdered Naresh, in the process he meets a young woman, Kiran who have some crucial evidences that might lead Ajay to the culprit who have murdered alcoholic and womanizer Naresh.

==Cast==
* Asha Parekh        	 ...	Kiran
* Dharmendra       	 ...	Ajay Singh
* Sanjeev Kumar  	 ...	Police Inspector Rai
* Rehman         	 ...	Sharma
* Helen         	 ...	Veera
* Bela Bose     	 ...	Mahua
* Johnny Walker 	 ...	Teju
* Manmohan      	 ...	Robbie
* Ramesh Deo      	 ...	Naresh Mathur
		
==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Haae Mere Paas To Aa"
| Asha Bhosle
|-
| 2
| "Jabse Laagi Tose Najariya"
| Asha Bhosle, Lata Mangeshkar
|-
| 3
| "Main Albeli Pyar Jata Kar"
| Asha Bhosle
|-
| 4
| "Mere Sarkar Meri Aahon Ka Asar Dekh"
| Mahendra Kapoor, Krishna Kalle 
|-
| 5
| "Parde Mein Rahne Do"
| Asha Bhosle
|-
| 6
| "Shikar Karne Ko Aaye"
| Mohammed Rafi
|-
| 7
| "Tumhare pyaar mein beqaraare"
| Mohammed Rafi
|}

==Awards and nominations==
*1969 Filmfare Awards  Best Supporting Actor - Won - Sanjeev Kumar (actor)| Sanjeev Kumar Best Female Playback Singer - Won - Asha Bhosle singing "Parde Mein Rahne Do" Best Comedian - Won - Johnny Walker (actor)| Johnny Walker Best Sound Design - Won - P. Thakkersey Best Supporting Actress - Nominated - Helen Jairag Richardson| Helen

==Trivia==
* This is the second hit movie of 1968 for Dharmendra. His other hit movie in the same year is Aankhen (1968 film)|Aankhen.
* This movie went on to be a Golden Jubilee Hit movie.
* Director Atma Ram is Guru Dutts elder brother.

==References==
 

== External links ==
*  

 
 
 
 