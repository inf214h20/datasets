Martin & Orloff
 
  Matt Walsh Ian Roberts (Walsh & Roberts are best known as half of the Upright Citizens Brigade comedy troupe) along with Ians wife Katie Roberts.  The film was produced and directed by Lawrence Blume and features an ensemble cast of alternative comedians including H. Jon Benjamin, David Cross, Andy Richter, Matt Besser, Amy Poehler, Tina Fey, Janeane Garofalo and Rachel Dratch, as well as actress Kim Raver as Orloffs girlfriend.

Martin and Orloff is available on DVD through Anchor Bay Entertainment and made its television debut on Comedy Central (Summer 2006).

The film has won numerous awards including:
*Texas Chainsaw Massacre Film Festival (Winner: Director’s Prize)
*Sarasota Film Festival ( “Best of Fest” Award)
*East Lansing Film Festival (Winner: Audience Award: Best Feature)
*The Art Institute of Chicago (Winner: Christopher Wetzel Award for Independent Comedy)
*High Times Magazine Stoney Award: Best Unreleased film of 2003 (nominee)
*IFP/West Independent Spirit Award: Motorola Producer of the Year
*Top Ten Films of 2003: Chicago Reader
*Hollywood Reporter: 2002 Ten Rising Stars of Comedy: Lawrence Blume

==External links==
* 
* 

 
 
 
 
 
 


 