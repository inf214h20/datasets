Hellgate (1952 film)
{{Infobox film
| name           = Hellgate
| image          = 
| caption        = 
| director       = Charles Marquis Warren
| producer       = John C. Champion
| writer         = John C. Champion Charles Marquis Warren
| starring       = Sterling Hayden
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Western film directed by Charles Marquis Warren and starring Sterling Hayden.   

==Plot== Civil War soldier, is falsely accused and convicted of a crime. He is sentenced to this hellish place.

He immediately gets on the wrong side of Voorhees, a vicious guard, and Redfield, a mean convict. Hanley will need to fight his way out, particularly when the prisoners are afflicted with an epidemic of a spreading plague (disease)|plague.

==Cast==
* Sterling Hayden as Gilman S. Hanley
* Joan Leslie as Ellen Hanley
* Ward Bond as Lt. Tod Voorhees
* James Arness as George Redfield Peter Coe as Jumper Hall John Pickard as Gundy Boyd
* Robert J. Wilke as Sgt. Maj. Kearn (as Robert Wilkie) James Anderson as Vern Brechene (as Kyle James)
* Richard Emory as Dan Mott
* Dick Paxton as George Nye (as Richard Paxton)
* William Hamel as Lt. Col. Woods (as William R. Hamel)

==See also==
* Sterling Hayden filmography

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 