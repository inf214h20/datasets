Behold the Earth
 
{{Infobox film
| name     = Behold the Earth
| image    =
| caption  = David Conover
| producer = David Conover
| writer   =
| starring = E. O. Wilson Cal DeWitt Richard Louv Theo Colborn Tim Eriksen
| music    = Dirk Powell Tim Eriksen
| cinematography =
| editing  =
| distributor =
| released =  
| runtime  =
| country  = United States
| language = English
| budget   =
}} David Conover.

==Cast==
*E. O. Wilson—renowned biologist, Pulitzer Prize-winning author, and Pellegrino University Research Professor in Entomology for the Department of Organismic and Evolutionary Biology at Harvard University. He is the author of The Creation: An Appeal to Save Life on Earth. Creation Care movement.
*Theo Colborn—co-authored the 1996 book, Our Stolen Future, which brought worldwide attention to the fact that common contaminants can interfere with human fetal development.
* . Louv is chairman of The Children & Nature Network, and has written extensively on the need to reconnect children and nature.

==Directors statement==
Director David Conover described the evolution of the film idea as follows:   

 
Many of us Americans share a vision of the past, which goes something like this...

Once upon a time, we lived in close proximity to what biologist E. O. Wilson and many others refer to as the Creation or, alternatively, as the natural world.  Food was grown in nearby fields, hunted in nearby woods, or fished from nearby waters. Children played outdoors.  A rich bounty of birds, mammals, plants, fish, and insects invited curious minds to observe, organize, and understand what life is.  The open landscape inspired dreams of what our own lives, and those of our descendants, could be.

Today, many Americans share unease about our relationship to the natural world.  Our children, and we, seem to spend less time outside and more time with indoor virtual amusements.  We look about and within our own day-to-day activities and feel distress about the food we eat, the air we breathe, the water we drink, and the accelerating pace required just to get by.  We’re disturbed by the degraded bounty of life on earth, a result of imbalances that we fear we’ve introduced.  Understandably, much attention is focused on SNAPSHOTS of these degradations.  This film takes a different tack.

Behold The Earth provides an opportunity to hold steady within the BIG PICTURE of American identity and the natural world.  It asks the big questions of thought leaders like E. O. Wilson, Cal Dewitt, Richard Louv, Theo Colborn, and others.  Where have we come from?  Where are we going?  How do we know what we know?  Rather quickly, these questions cut to the core of what Americans believe and know about Creation.

At this core are the two most powerful forces in the world today; religion and science.  Often they co-exist in a distrustful incompatibility.  Indeed, in several core areas, I believe the respective worldviews will certainly continue to disagree.  But not all core areas. And this film is not about the areas of disagreement, in any case.

In light of addressing the earth’s most pressing degradations, this film explores those areas of common cause.  Parents, educators, scientists, Christians, environmentalists, and any combination of the above are invited to join the inquiry.  Evocative sights and sounds of the natural world combine with a distinctive blend of neo-traditional music and talent.
 

==See also== David Conover
*Compass Light
*Sunrise Earth

==References==
 

==External links==
*  

 
 
 
 
 