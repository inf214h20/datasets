It Isn't Done
 
{{Infobox film
| name           = It Isnt Done
| image          = It_Isnt_Done.jpg
| image_size     = 
| caption        = 
| director       = Ken G. Hall
| producer       = Ken G. Hall Frank Harvey   Carl Dudley
| based on       = original story by Cecil Kellaway
| narrator       = 
| starring       = Cecil Kellaway   Shirley Ann Richards
| music          = Hamilton Webber George Heath 
| editing        = William Shepherd
| studio         = Cinesound Productions
| distributor = British Empire Films (Aust) MGM (UK)
| released       = February 1937 (Australia) 1938 (UK)
| runtime        = 90 minutes (Australia) 77 mins (UK)
| country        = Australia
| language       = English
}}
It Isnt Done is a 1937 Australian comedy film about a grazier (Cecil Kellaway) who inherits a barony in England.

==Synopsis==
Hubert Blaydon, an Australian farmer, inherits a baronial estate and moves to England with his wife and daughter Patricia to collect it. He finds it difficult to adapt to upper class customs and faces snobbishness from Lord Denvee and difficulties with his butler Jarms. Patricia falls for a writer, Peter Ashton, who is next in line for the title and the estate.

Hubert misses Australia. He eventually contrives evidence that Peter is the legal heir and bonds with Lord Denvee over the fact that both their sons died on the same day in World War I. Hubert and his wife return to Australia with Jarms while Patricia and Peter are married.

==Cast==
*Cecil Kellaway as Hubert Blaydon
*Shirley Ann Richards as Patricia Blaydon
*John Longden as Peter Ashton Frank Harvey as Lord Denvee
*Harvey Adams as Jarms
*Nellie Ferguson as Mrs Blaydon 
*Campbell Copelin as Ronald Dudley
*Bobby Hunt as Lady Denvee 
*Leslie Victor as Potter
*Harold Meade as Lady Addersley
*Rita Paucefort as Mrs Dudley
*Douglas Channell as Harry Blaydon
*Sylvia Kellaway as Elsie Blaydon
*Hilda Dorrington as Mrs Ashton
*Ronald Whelan as Perroni

==Production==

===Development===
The film was based on an original story by Cecil Kellaway which he had written in between acts while performing in White Horse Inn on stage. Hall liked the basic idea but said Kellaway was unable to put it down to paper. 
 Frank Harvey, who had recently joined Cinesound as a dialogue director.  Harvey  went on to write all of Ken G. Halls films for Cinesound. Hall says he had considerable input to the film as well. 

"If anything, the English will get more knocks than the Australians", said Hall at the time. "But there will be hits at both sides-nothing malicious; just a good-humoured conflict of ideas." 
Cecil Kellaway later claimed the lead character was based on a real grazier from New South Wales:
 Ive enjoyed portraying this role, because I know him so thoroughly. Ive stayed on his property, Ive studied his mannerisms. He is the jovial, lovable person who is symbolic of the democratic carefree spirit of a sunny land.... To me, he is typical of so many of our countrymen. In many of his scenes I have endeavoured to give something that hopes for a laugh and faintly suggests a tear. I sincerely hope I have succeeded.  

===Casting===
The film marked the feature debut of Shirley Ann Richards who was a graduate of Cinesounds Talent School, run by Harvey and George Parker. Although she was not very experienced, she proved a natural and was enormously popular. She was signed to a long-term contract with Cinesound and went on to appear in several of their films.  

===Shooting===
Although mostly set in England, the film was entirely shot in Australia, at Cinesounds Bondi studios and Camden, New South Wales|Camden. Shooting took place in October and November 1936. Hall used rear projection equipment to show English backgrounds. The backgrounds were filmed for Cinesound by British International Pictures. 

Sets were designed by Eric Thompson, who had returned to Australia after several years working in Hollywood. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 176-177 

==Reception==
Reviews were positive  and the film was a big hit at the box office, being released in the US and UK.

A representative from RKO in Hollywood saw the film and offered Kellaway a long-term contract, which he accepted.  Kellaway did return to Australia for one more film, Mr. Chedworth Steps Out, but spent the rest of his career in America.

After completing the film, John Longden returned to England after spending four years in Australia.

In the film, Shirley Ann Richards plays a woman whose brother was killed in World War I. Richards brother in real life died in a Japanese POW camp during World War II. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at Australian Screen Online
*  at Oz Movies
 

 
 
 