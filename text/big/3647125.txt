Elephant Walk
 

{{Infobox film
| name           = Elephant Walk 
| image          = Elephant Walk 1954.jpg
| image size     =
| caption        = Theatrical release poster
| director       = William Dieterle
| producer       = Irving Asher Robert Standish
| starring       = Elizabeth Taylor Dana Andrews Peter Finch Abraham Sofaer Abner Biberman
| music          = Franz Waxman
| cinematography = Loyal Griggs
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $3 million (US) 
}}

Elephant Walk is a 1954 Paramount Pictures film, directed by William Dieterle, and starring Elizabeth Taylor, Dana Andrews, Peter Finch and Abraham Sofaer.

It is based upon the novel Elephant Walk by "Robert Standish", the pseudonym of the English novelist Digby George Gerahty (1898–1981).
 The Beggars Opera (1953). Leigh was enthusiastic about the role and continued in her husbands absence, but she was forced to withdraw from production shortly after filming began in Colombo, Sri Lanka|Ceylon, as a result of bipolar disorder.   According to Leonard Maltins annual Movie Guide book, Leigh can be seen in some long shots that were not re-filmed after Elizabeth Taylor replaced her.

==Plot==
Colonial tea planter John Wiley, visiting England at the end of World War II, wins and weds lovely English rose Ruth and takes her home to Elephant Walk, Ceylon, where the local elephants have a grudge against the plantation because it blocks their migrating path. Ruths delight with the tropical wealth and luxury of her new home is tempered by isolation as the only white woman in the district; by her husbands occasional imperious arrogance; by a mutual physical attraction with plantation manager Dick Carver; and by the hovering, ominous menace of the hostile elephants. The elephants end up destroying the plantation in a stampede along with a fire.

==Critical reception==
Maltin gave the film 2 stars out of 4, and made one of his pithier critiques: "Pachyderm stampede climax comes none too soon."  A major plot element in the film is that the tea plantations manor, where the films action occurs, had been built in the middle of a path that migrating Indian elephants had previously used.

==Cast==
* Elizabeth Taylor as Ruth Wiley
* Dana Andrews as Dick Carver, the manager of the plantation
* Peter Finch as John Wiley, Ruths husband
* Abraham Sofaer as Appuhamy, handyman of Johns father Tom
* Noel Drayton as Chief Planter Atkinson
* Abner Biberman as Doctor Pereira
* Rosalind Ivan as Mrs. Lakin
* Edward Ashley-Cooper as Planter Gordon Gregory
* Barry Bernard as Planter Strawson
* Philip Tonge as Planter John Ralph
* Leo Britt as Planter Chisholm
* Mylee Haulani as Rayna

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 