The Brave Little Toaster to the Rescue
{{Infobox film name = The Brave Little Toaster To The Rescue
|image=The Brave Little Toaster to the Rescue.jpg image size = 185px caption = DVD cover. director = Robert C. Ramirez producer = Peter Locke (executive producer) writer = Original   (book),   music = Alexander Janko (score), William Finn and Ellen Fitzhugh (songs) starring = Deanna Oliver Tim Stack Thurl Ravenscroft studio = Hyperion Animation The Kushner-Locke Company distributor = Walt Disney Home Video released =   runtime = 74 minutes country = United States language = English
}} The Brave Hyperion Animation and The Kushner-Locke Company, it was originally released in 1999 in North America by Walt Disney Home Video. It was also released the same year in the United Kingdom and premiered on television on BBC Two, but there no DVD release. The film (along with Goes To Mars) is now available for purchase and rent on iTunes, but the first film has yet to be released.

==Cast==
* Deanna Oliver as Toaster
* Timothy Stack as Lampy
* Roger Kabler as Radio
* Eric Lloyd as Blanky
* Thurl Ravenscroft as Kirby
* Brian Doyle-Murray as Wittgenstein Chris Young as Rob
* Jessica Tuck as Chris
* Alfre Woodard as Maisie
* Andy Milder as Ratso
* Jonathan Benair as Jim Bob
* Eddie Bracken as Sebastian
* Andrew Daly as Murgatroid
* Eddie Deezen as Charlie
* Patti Edwards as Lab Computer
* Victoria Jackson as Mouse
* Marc Allen Lewis as Security Guard
* Ross Mapletoft as Modem
* Kevin Meaney as Computer
* Jay Mohr as Mack
* Danny Nucci  as Alberto

==Plot==
Rob McGoarthy, the owner of the appliances and whom they refer to as "the master", is working in a veterinary clinic where he tends to injured animals. One night, while working on a thesis, his computer accidentally crashes caused by a terrible computer virus from an old TLW-728 supercomputer named Wittgenstein. The appliances, along with the rat Ratso who found Wittgenstein, then seek to help Rob by finding Wittgenstein to reverse the effects of his computer virus, hence recovering the masters thesis. Meanwhile, in a dual plot of the film, Mack, Robs lab assistant, plots to sell the injured animals Rob had been tending, to a place called "Tartaras Laboratories", the same place that Sebastian, an old monkey Rob is tending to, was sent to when he was just a baby. When the appliances find Wittgenstein, they discover him abandoned, all alone and run-down and broken in the basement due to be infected by a computer virus. The miserable supercomputer reveals that he is living on one rare tube, named the "WFC 11-12-55". The appliances learn that unless they find a replacement quickly, Wittgensteins tube will blow and lead to his apparent death.
 is a goner". Ratso then blames Radio, which causes Radio himself to give up his own tube which turns out to be the very rare tube they had been looking for, thus leaving himself as a lifeless appliance. Apparently, knowing that they were given a final chance to save the animals, the appliances replaced the tube in the nick of time; with the boosted power of the new tube, Wittgenstein wakes up, miraculously regenerates the other smashed tubes connected to himself and destroying the computer viruses within him and is completely revived to as good as new. By the end of the film, the appliances restore Robs thesis and stop Mack from selling the injured animals, Radios tube is replaced with a new one (hence his revival), Wittgenstein is sold to a museum, Rob proposes to his girlfriend Chris, and all is well.

==Notes==
Despite being the third and final film released, it appears to be the second in plot sequence. This is mentioned in  The Brave Little Toaster Goes to Mars by the fact that the group already knows the supercomputer Wittgenstein, and by the fact that he is referred to as "our old college buddy." Also, Rob proposes to his girlfriend in this movie, while in the second movie the two are married with a baby. This is because both films were in production at the same time, and Goes To Mars was the first to be released.

==Reception==
The movie received mixed to positive reviews. Fans of the first movie called this one a "relief" after hating Goes To Mars when it was released. Critics said that "this movie explains everything that was missing between the original and the sequel"
 

==Animal Characters==
*Sebastian - Monkey
*Maisie - Cat
*Ratso - Rat
*Murgatroid - Snake Chihuahua

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 