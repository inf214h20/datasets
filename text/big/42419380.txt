Adventure in Warsaw
{{Infobox film
| name = Adventure in Warsaw
| image =
| image_size =
| caption =
| director = Carl Boese
| producer = 
| writer =  Bobby E. Lüthge   Konrad Tom
| narrator =
| starring = Georg Alexander   Paul Klinger   Jadwiga Kenda   Hedda Björnson
| music = Michael Jary
| cinematography = Georg Krause
| editing = 
| studio = Nerthus Film   Polski Tobis
| distributor = Tobis Film
| released = 1937
| runtime = 
| country = Germany   Poland German
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Augustus the Strong (1936). 

A separate Polish version A Diplomatic Wife was released.

==Cast==
*Georg Alexander as Exzellenz Bernardo de Rossi - Gesandter in Warschau
*Paul Klinger as Henry de Fontana - Gesandtschaftsrat
*Jadwiga Kenda as Jadwiga Janowska - seine Frau
*Hedda Björnson as Ines Costello - eine junge Witwe
*Baby Gray as Wanda - eine angehende Soubrette
*Robert Dorsay as Jan - Operettenbuffo
*Richard Romanowsky as Stanislaus Bilinski - Theaterdirektor
*Rudolf Carl as Kupka - sein Sekretär
*Mieczyslawa Cwiklinska as Apollonia, komische Alte
*Eugen Wolff as Mit seinen Tanzorchester

== References ==
 

== Bibliography ==
*Bock, Hans-Michael & Bergfelder, Tim. The Concise Cinegraph: Encyclopaedia of German Cinema. Berghahn Books, 2009.
*Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918–1945. University of California Press, 1999.
*Skaff, Sheila. The Law of the Looking Glass: Cinema in Poland, 1896–1939. Ohio University Press, 2008.

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 