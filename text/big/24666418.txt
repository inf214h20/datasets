The Disappearance of Haruhi Suzumiya
 
{{Infobox film
| name           = The Disappearance of Haruhi Suzumiya
| image          = The Disappearance of Haruhi Suzumiya BD limited edition cover.jpg
| caption        = Blu-ray Disc release cover art
| film name      = {{Infobox name module
| kanji          = 涼宮ハルヒの消失
| romaji         = Suzumiya Haruhi no Shōshitsu
}}
| director       = {{plainlist|
* Tatsuya Ishihara  
* Yasuhiro Takemoto
}}
| producer       = {{plainlist|
* Atsushi Itō
* Hideaki Hatta
}}
| screenplay     = Fumihiko Shimo
| based on       =  
| starring       = {{plainlist|
* Aya Hirano
* Tomokazu Sugita
* Minori Chihara
* Yūko Gotō
* Daisuke Ono
* Natsuko Kuwatani
* Yuki Matsuoka
* Minoru Shiraishi
* Megumi Matsumoto Sayaka Aoki
}}
| music          = {{plainlist|
* Satoru Kōsaki
* Ryuichi Takada
}}
| cinematography = Ryūta Nakagami
| editing        = Kengo Shigemura
| studio         = {{plainlist|
* Kadokawa Shoten
* Kadokawa Pictures
* Kyoto Animation Lantis
* KlockWorx
}}
| distributor    = {{plainlist|
* Kadokawa Shoten
* KlockWorx
}}
| released       =  
| country        = Japan
| language       = Japanese
| runtime        = 164 minutes
| gross          = ¥830 million 
}}
 the same name written by Nagaru Tanigawa. It is produced by Kyoto Animation and directed by Tatsuya Ishihara and Yasuhiro Takemoto. It was released in Japanese theaters on February 6, 2010 and on DVD and Blu-ray Disc on December 18, 2010. The film has been licensed by Bandai Entertainment in North America and Manga Entertainment in the UK.

==Plot==
  The Melancholy cultural festival. Haruhi Suzumiya nabe party Taniguchi (Minoru Shiraishi) that Haruhi was at another high school the whole time, along with Itsuki and others formerly from his school. By revealing his identity to her as John Smith, an alias he had used when he travelled back in time to assist a young Haruhi, Kyon manages to convince Haruhi to believe his story. With her assistance they gather the SOS Brigade together in the club room, thus bringing the keys necessary for a program built by alien Yuki.

Wanting to go back to his interesting life, Kyon activates the program and goes back in time to the Tanabata of three years ago. After meeting up with the future Mikuru, he obtains an uninstall program from the pasts Yuki, which needs to be shot at the culprit right after the change in the early hours of December 18. Returning to the present, they find the culprit, Yuki, who had borrowed Haruhis power to change everyones memories except Kyons, giving him the choice of which world he would rather live in. Kyon questions himself about his choice and thinks to himself that a normal world without the SOS Brigade and Haruhi Suzumiya would be calm and peaceful and thinks Yuki was tired of everything she had to do like monitoring Haruhis behavior and protecting Kyon, but finally decides that his original world was more interesting and fun. Kyon tries to install the program into Yuki but is stabbed by Ryoko, who had retained her psychotic behavior. Before Ryoko can finish him off, he is rescued by future counterparts of Yuki, Mikuru and himself. He wakes up a few days later in a hospital, where the world is back to normal, but almost everyone believes Kyon had been in a coma since December 18 after falling down the stairs. When Yuki mentions to Kyon how the Data Integration Thought Entity would punish her for her actions, Kyon tells her to let them know that if they ever try such a thing, he can tell Haruhi about him being John Smith and have her alter reality so the organization would cease to exist. As December 24 comes and his everyday life returns, Kyon decides there is still time before he has to go back in time to save himself and decides to join in on the Christmas party.

==Production and release==
On December 18, 2007, the official website of The Melancholy of Haruhi Suzumiya anime series, haruhi.tv, was replaced by a fake 404 error with five form input fields, a reference to the pivotal date in The Disappearance of Haruhi Suzumiya, the fourth volume in the light novel series.  The story of The Disappearance of Haruhi Suzumiya did not appear in the 2009 re-airing of the anime series The Melancholy of Haruhi Suzumiya, which included previously un-aired episodes adapted from the second, third and fifth novels. However, at the close of the 2009 season on October 8, 2009, a 30-second teaser trailer showing Yuki Nagato was aired, revealing that The Disappearance of Haruhi Suzumiya would actually be a film,  set for a February 6, 2010 release.  A one-minute promotional video was released in December 2009.  The film was released on Blu-ray Disc|BD/DVD, in regular and limited editions on December 18, 2010 in Japan.  

The film has been licensed for North America distribution by Bandai Entertainment. English-subtitled screenings began running in San Franciscos Viz Theater from May 21, 2010, and were followed by a screening at the Laemmles Sunset theater in Hollywood on June 24, 2010  and a theatrical run in Hawaii in June 2010 through Consolidated Theatres and Artisan Gateway as part of their Spotlight Asia Films program.  An English-language version has been co-produced by Bang Zoom! Entertainment and was released on DVD and Blu-ray Disc in North America on September 20, 2011.        Manga Entertainment released it in the UK on DVD on November 7, 2011,    though a planned 2012 Blu-ray release has been cancelled.    Madman Entertainment released the film on DVD and Blu-ray in Australia and New Zealand on November 16, 2011.   The film had its European premiere on October 17, 2010 at the Scotland Loves Anime event in Edinburgh.  Animax Asia will air the film. 

===Music=== single of which was released on February 24, 2010.  The opening theme is "List of Haruhi Suzumiya albums#Bōken Desho Desho?|Bōken Desho Desho?" by Aya Hirano. The films original soundtrack was released on January 27, 2010.  The soundtrack is performed by the Eminence Symphony Orchestra and was produced by Satoru Kōsaki.

{{Tracklist
| music_credits   = yes
| collapsed       = yes
| headline        = Original soundtrack disc 1
| title1          = Itsumo no Fūkei kara Hajimaru Monogatari
| note1           =   The Story Begins With the Usual Scenery
| music1          = Satoru Kōsaki
| length1         = 3:51
| title2          = SOS-dan Christmas Party
| note2           =   SOS Brigade Christmas Party
| music2          = Satoru Kōsaki
| length2         = 2:24
| title3          = Dotabata Time
| note3           =   Noisy Time
| music3          = Satoru Kōsaki
| length3         = 1:03
| title4          = Nichijō no Saki ni Machiukeru Mono
| note4           =   Everyday Things That Lie Ahead
| music4          = Keigo Hoashi
| length4         = 0:51
| title5          = Asakura Ryōko to Iu Josei
| note5           =   The Woman Named Ryoko Asakara
| music5          = Ryūichi Takada
| length5         = 2:59
| title6          = Fuan kara Kyōfu e
| note6           =   Fear from Anxiety
| music6          = Ryūichi Takada
| length6         = 1:44
| title7          = Uragirareta Kitai
| note7           =   Betrayed Expectations
| music7          = Keigo Hoashi
| length7         = 2:48
| title8          = Kodoku Sekai no Hirogari
| note8           =   Lonely World’s Spread
| music8          = Keigo Hoashi
| length8         = 3:14
| title9          = Kankyō Henka no Zehi
| note9           =   Pros and Cons of Environmental Changes
| music9          = Satoru Kōsaki
| length9         = 2:56
| title10          = Suzumiya Haruhi no Tegakari
| note10           =   The Trail of Haruhi Suzumiya
| music10          = Kakeru Ishihama
| length10         = 1:27
| title11          = Hayaru Kokoro to Mae ni Denai Ashi
| note11           =   Popular Spirit and Feet That Wont Leave
| music11          = Satoru Kōsaki
| length11         = 1:14
| title12          = Tsunagatta Kioku
| note12           =   Memories Tied Together
| music12          = Satoru Kōsaki
| length12         = 2:33
| title13          = SOS-dan Futatabi
| note13           =   SOS Brigade Once Again
| music13          = Satoru Kōsaki
| length13         = 1:56
| title14          = Ready?
| note14           = 
| music14          = Ryūichi Takada
| length14         = 4:13
| title15          = Ano Hi no Kioku o Oikakete
| note15           =   Chasing the Memory of That Day
| music15          = Satoru Kōsaki
| length15         = 1:27
| title16          = Michibiku Josei no Kataru Kotoba
| note16           =   Having Words With the Female Guide
| music16          = Keigo Hoashi
| length16         = 2:26
| title17          = Mirai e no Ashiato
| note17           =   Footprints From the Future
| music17          = Keigo Hoashi
| length17         = 1:53
| title18          = Gymnopédies (Satie)|Gymnopédie No. 2
| note18           =   Jimunopedi Dai Ni-ban
| music18          = Erik Satie
| length18         = 2:55
| title19          = Nagato Yuki no Kokoro ni Aru Mono
| note19           =   In the Heart of Yuki Nagato
| music19          = Keigo Hoashi
| length19         = 2:88
| title20          = Jikoishiki no Kakunin
| note20           =   Awakening Self-Consciousness
| music20          = Keigo Hoashi
| length20         = 2:44
| title21          = Rekishi no Tenkanten
| note21           =   Converging Point of History
| music21          = Ryūichi Takada
| length21         = 3:18
| title22          = Futatabi Deaeta Danintachi
| note22           =   Meeting Brigade Members Once Again
| music22          = Satoru Kōsaki
| length22         = 5:01
| title23          = Itsumo no Fūkei de Owaru Monogatari
| note23           =   The Story Ends With the Usual Scenery
| music23          = Satoru Kōsaki
| length23         = 3:16

}}

{{Tracklist
| music_credits   = yes
| collapsed       = yes
| headline        = Original soundtrack disc 2
| total_length    = 
| title1          = Gymnopédie No. 1
| note1           =   Jimunopedi Dai Ichi-ban
| music1          = Erik Satie
| length1         = 3:17
| title2          = Gymnopédie No. 2
| note2           =   Jimunopedi Dai Ni-ban
| music2          = Erik Satie
| length2         = 2:50
| title3          = Gymnopédie No. 3
| note3           =   Jimunopedi Dai San-ban
| music3          = Erik Satie
| length3         = 2:27 Gnossienne No. 1
| note4           =   Gunoshiennu Dai Ichi-ban
| music4          = Erik Satie
| length4         = 3:24
| title5          = Gnossienne No. 2
| note5           =   Gunoshiennu Dai Ni-ban
| music5          = Erik Satie
| length5         = 2:17
| title6          = Gnossienne No. 3
| note6           =   Gunoshiennu Dai San-ban
| music6          = Erik Satie
| length6         = 2:56
| title7          = Je te veux
| note7           =   I Want You
| music7          = Erik Satie
| length7         = 5:15

}}

==Related media==
A Spin-off (media)|spin-off manga titled   is illustrated by Puyo and started serialization in Kadokawa Shotens Young Ace in July 2009. An anime adaptation by Satelight began airing in April 2015.  A visual novel video game titled   was released on May 12, 2011 by Bandai Namco Games for the PlayStation 3 and PlayStation Portable. The games story takes place shortly after the events of the film.

==Reception==
  yen in its first week.  The film won the Best Theatrical Film award at the 2010 Animation Kobe Awards.  The BD version sold over 77,000 copies in its first week, topping the Oricon charts, while placing fourth in the DVD charts with 19,667 copies sold.  Minori Chihara won the Best Singing Award at the fifth annual Seiyu Awards held in 2011 in Tokyo for her performance of the "Yasashii Bōkyaku" ending theme song. 
 nationwide floods to be screened only at the Lido Theatre, Siam Square, Bangkok, and only for one day, November 6, 2011. However, it is reported that the tickets were immediately sold out on the first day of booking. After the showing, Rose Media & Entertainment, the Thai Haruhi Suzumiya franchisee, also held an auction of the Haruhi goods, including limited BDs and DVDs, and donated all the earnings to the flood relief efforts.  

==References==
 

==External links==
*   
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 