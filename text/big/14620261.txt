Tarzan and the Slave Girl
{{Infobox film
| name           = Tarzan and the Slave Girl
| image          = "Tarzan_and_the_Slave_Girl"_(1950).jpg
| image_size     =
| caption        = Italian theatrical poster
| director       = Lee Sholem
| producer       = Sol Lesser
| writer         = Arnold Belgard Hans Jacoby 
| based on       =  
| starring       = Lex Barker Vanessa Brown Robert Alda
| music          = Paul Sawtell
| cinematography = Russell Harlan
| editing        = Christian Nyby
| distributor    = RKO Pictures
| released       =   Essoe, p. 131. 
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         =
}}

Tarzan and the Slave Girl is a 1950 film starring Lex Barker as Tarzan, Vanessa Brown as Jane Porter (Tarzan)|Jane, and Robert Alda as big game hunter Neil. It was directed by Lee Sholem. The plot involves a lost civilization in Africa, a strange illness, and an evil counselor manipulating a prince into kidnapping large numbers of local women.

The film was Barkers second portrayal of Tarzan, and Vanessa Browns only outing as Jane.

==Plot==
Tarzan and Jane are spending some time by a river when they hear a scream. A local tribal girl has gone missing, and the tribes people believe this is due to some evil spirit. Tarzan and Jane quickly realize the girl has been kidnapped. The kidnappers are Lionians, a "lost" culture of Caucasians who have a culture similar to ancient Egypt and who worship lions. The Lionians are kidnapping girls throughout the region to bring back to their city deep in the jungle. But they have brought a terrible disease with them which can kill within hours. Tarzan seeks the help of Dr. Campbell, who has a serum that can both cure the disease as well as vaccinate against it. After saving the local tribe, Dr. Campbell and Tarzan (with the help of Neil, a drunken big game hunter) head for the Lionian city.  Meanwhile, Dr. Campbells native assistant, the buxom and blonde Lola, has fallen for Tarzan. Jane and Lola have a fight, after which both women are captured by a Lionian raiding party.

Tarzan and the others are repeatedly attacked by other tribes and the Lionians as they search for the Lionian city.  Neil suffers an injured leg, and is left behind.  Dr. Campbell unknowingly drops his bottle of serum, and although Neil discovers it later as he follows Tarzan and Campbell.

Meanwhile, Jane and Lola are taken to the Lionian capital. The Lionian king has recently died of the horrible disease, leaving the Prince in charge. He is easily swayed by the evil counselor, Sengo, who has persuaded the Prince to indulge every lust for food, drink, and women to assuage his grief. Furthermore, the illness has killed many Lionian women, leading the Lionians to capture local beauties as concubines. When the Lionian High Priest challenges Sengo, Sengo convinces the Prince that the priest is a rebel and should be fed to the lions. Sengo takes on the duties of the High Priest. The Prince admires Lola but leaves to see his sick son. Lola taunts Sengo that he will suffer when she is Queen. He has her whipped and, in a scuffle, Jane stabs him in the arm with his own knife and the two girls flee into the dead Queens tomb (which is in the dead kings stone mausoleum) where Sengo discovers them and entombs them alive. 

Tarzan arrives at the Lionian city with Campbell. The Princes son has fallen ill with the disease, and Sengo blames Tarzan and Neil. Their deaths are ordered, but Tarzan escapes and leads the Lionians on a merry chase through their own city.  Tarzan hides inside the dead kings sarcophagus, but becomes entombed in the stone mausoleum as well. Luckily, Tarzan discovers where Jane and Lola have been sealed up as well, and frees them. Neil arrives with the serum (which Cheetah finds along the way) and they begin to treat the Princes son. Whilst Sengo prepares to throw the old High Priest to the lions, Tarzan calls for help, and an elephant breaks down the tombs door to free Tarzan, Jane, and Lola.  Tarzan holds off the Lionians, and manages to throw Sengo into the pit with the lions.  Meanwhile, the Princes son is cured.  The Prince, realizing how wrong he has been, orders the High Priest, Tarzan, all of Tarzans friends, and all the slave girls freed.

==Cast==
* Lex Barker as Tarzan Jane
* Robert Alda as Neil
* Hurd Hatfield as Prince of the Lionians
* Arthur Shields as Dr. E.E. Campbell Anthony Caruso as Sengo (billed as Tony Caruso)
* Denise Darcel as Lola
* Robert Warwick as High Priest
*Alfonso Pedroza as Nagasi Chief (uncredited)
*Satini Pualoa as Medicine Man (uncredited)
*Tito Renaldo as Chiefs Son (uncredited)
*Phil Harron as Lionian (uncredited)
* Peter Mamakos as Lionian Henchman (uncredited)

==Production==
Production of the film was announced on June 23, 1949, after producer Sol Lesser signed a new distribution agreement for his "Tarzan" pictures with 
  (who had recently appeared in William Wellmans World War II picture Battleground (film)|Battleground, was cast as the slave girl.  Vanessa Brown was signed to play Jane two weeks later. 

Hans Jacoby, who had scripted the highly popular Tarzan and the Amazons, turned in the screenplay for the film. Reid, p. 233.  He would also script the Lex Barker feature Tarzans Savage Fury. 
 Iverson Movie Ranch. Schneider, p. 4.  But most of the filming was done on the RKO Forty Acres backlot.  (On January 7, 1950, Lesser announced that the next Tarzan film would be made entirely in South Africa.)  

The film marked actress Vanessa Browns only outing as Jane.  According to director Lee Sholem, producer Sol Lesser was looking to cast a new "Jane" to replace actress  . May 29, 1999.] Accessed 2011-07-08.  Signed by 20th Century Fox, shed been loaned out to RKO several times.  But Fox had cancelled her contract in early 1950.  She took the role in RKOs Tarzan and the Slave Girl because she needed the money.  She later recalled, "My intellectual friends said, My God, what you wont do for money. I needed a job, I had to pay the rent."  (Later that year, shed become a Broadway star after Katharine Hepburn picked her to play Celia in As You Like It.)  Lesser picked Brown because of her Quiz Kid background.  But director Sholem found her pompous:
:There was a situation one day where she had about three words to say, and she asked, "What is the underlying meaning of this?" In a Tarzan picture  ! "What is my feeling here? What is my attitude?" Oh, you never heard such shit! 

The slave girl in the title is Lola, played by Denise Darcel. Although previous films had made it clear that Tarzan and Jane were husband and wife, this film depicted Jane as Tarzans girlfriend —which allowed Lola to compete for Tarzans affections without implying that she was an adulterer.

Mary Ellen Kay has an uncredited role as the slave girl who is engaged to the Prince.  Eva Gabor has a nonspeaking background role as one of the slave girls as well. 

During the production, Tarzan creator Edgar Rice Burroughs visited the set. Suffering from Parkinsons disease and having already had several heart attacks, Burroughs visited the set of Tarzan and the Slave Girl during its production. Etter, p. 233.  It was one of his last public appearances, according to Burroughs daughter, Joan.  Burroughs died on March 19, 1950, just four days after the films release on March 15. 

==Critical reception== New York Times called the film "painful" to watch, and said, "About the only novelty the picture offers is Cheetas encounter with a bottle of whisky, and even that isnt very funny." 

==References==
;Notes
 

;Bibliography
*American Film Institute. The American Film Institute Catalog of Motion Pictures Produced in the United States. Vol. F4. Berkeley, Calif.: University of California Press, 1999.
*"Barker, Lex." Dictionary of American Biography, 1971–1975. New York: Scribner, 1994.
*Belanger, Camyl Sosa. Eva Gabor, an Amazing Woman. New York: iUniverse, 2005.
*Essoe, Gabe. Tarzan of the Movies: A Pictorial History of More Than Fifty Years of Edgar Rice Burroughs Legendary Hero. New York: Citadel Press, 1968.
*Etter, Jonathan. Quinn Martin, Producer: A Behind-the-Scenes History of QM Productions and Its Founder. Jefferson, N.C.: McFarland & Co., 2003.
*Fitzgerald, Michael G. and Magers, Boyd. Ladies of the Western: Interviews With 25 Actresses From the Silent Era to the Television Westerns of the 1950s and 1960s. Jefferson, N.C.: McFarland, 2008.
*Reid, John Howard. Success in the Cinema: Money-Making Movies and Critics Choices. Morrisville, N.C.: John Howard Reid, 2006.
*Schneider, Jerry L. Edgar Rice Burroughs and the Silver Screen. Vol. IV: The Locations. Miami: ERBville Press, 2008.
*Vernon, Alex. On Tarzan. Athens, Ga.: University of Georgia Press, 2008.
*Weaver, Tom. Return of the B Science Fiction and Horror Heroes: The Mutant Melding of Two Volumes of Classic Interviews. Jefferson, N.C.: McFarland, 2000.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 