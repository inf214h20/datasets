You Can't Buy Everything
{{Infobox film
| name           = You Cant Buy Everything
| image          = 
| image_size     = 
| caption        = 
| director       = Charles Reisner Sandy Roth
| writer         = Dudley Nichols Lamar Trotti Zelda Sears Eve Greene
| narrator       = 
| starring       = May Robson Jean Parker Lewis Stone
| music          = William Axt Leonard Smith Ben Lewis
| studio         = 
| distributor    = MGM
| released       = January 26, 1934
| runtime        = 82 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
You Cant Buy Everything (1934) is an American movie released by Metro-Goldwyn-Mayer. It stars May Robson, Jean Parker and Lewis Stone. Working titles of the film were Rich Widow and Old Hannibal. According to Motion Picture Herald, the principal character of Hannah Bell (played by May Robson) was modeled after Hetty Green, famous as the miserly "Witch of Wall Street." 

==Plot==
In 1893 New York, Mrs. Hannah Bell (May Robson) takes her son Donny to a charitable medical clinic, where she gives a false name and information in order to avoid paying (Hetty Green notoriously tried to do the same thing for her son Edward Howland Robinson Green|Edward). However, her friend Kate Farley (Mary Forbes) visits the clinic (which she generously supports) and recognizes Donny. She makes Hannah pay for the boys treatment. 

Later, Hannah reads in the newspaper that John Burton (Lewis Stone) has been named vice president of the Knickerbocker Bank. Furious, she goes to see her longtime friend and banker Asa Cabot to withdraw all of her money immediately. He is unable to find out why she hates Burton, but refuses to accept his offered resignation. It is later revealed that Burton abandoned Hannah without explanation just before their wedding. She later married a man she did not love who she knew was only after her wealth, just to salvage her pride. Her husband squandered her money, leaving her in desperate financial straits. She painstakingly made herself rich, all for her sons sake, and became a miser just like her father.
 
In 1904, Donny is the valedictorian of his graduating class at Princeton University. He wants to become a writer, but Hannah insists he work at the bank where she has entrusted her now immense wealth. 

In 1907, Kate learns something about Hannahs relationship to John Burton, and tries to secretly arrange a meeting between them. It does not work, but does unintentionally bring together Donny (played by William Bakewell as a man) and Burtons daughter Elizabeth (Jean Parker). They fall in love. However, Elizabeth at first refuses to marry Donny because she feels that he cannot stand up to his domineering mother. When Hannah finds out about the relationship, she storms into Burtons office and accuses him of trying to get her money through his daughter. He denies plotting against her, but refuses to interfere with the couple. Donny and Elizabeth get married without her approval. She does not even attend the wedding (though she watches from in hiding as the happy newlyweds leave the church).
 demand loan (on which she can demand repayment at any time). Just after Burton receives his share of the loan to satisfy his bank clients, Hannah notifies him that she wants the loan paid back. Instead of returning the money, he decides to forfeit his stock rather than abandon his depositors. Hannah is delighted to finally avenge herself on her former fiance, having wrested control of the railroad away from him. 

Donny, just returned from his honeymoon in Europe, gets Burtons side of the story. Then he denounces his mother, accusing her of never loving him, but rather treating him as just another of her possessions. He informs her that Burton left her at the altar because her father tried to get him to sign an agreement never to touch her money. Burton assumed she knew and approved of the stipulation, whereas she never did until now. Stunned by the revelation, she goes outside to the park to think. 

She catches pneumonia, but recovers. Donny comes to see her, and they are reconciled. She also embraces her daughter-in-law. When Burton shows up (having received the railroad stocks back), that vendetta is also ended.

==Cast==
*May Robson as Mrs. Hannah Bell	   
*Jean Parker as Elizabeth "Beth" Burton Bell	   
*Lewis Stone as John Burton	   
*Mary Forbes as Kate Farley	   
*Reginald Mason as Dr. Lorimer	   
*William Bakewell as Donny "Don" Bell as a man	   
*Tad Alexander as Donny Bell as a boy	    Walter Walker as Josiah Flagg	   
*Reginald Barlow as Tom Sparks	   
*Claude Gillingwater as Asa Cabot	   			   
*Bruce Bennett as Bank Clerk (uncredited)	   
*Margaret Bert as Miss Austin - Burtons Secretary (uncredited)	   
*Eddie Borden as Train Conductor (uncredited)	   
*Walter Brennan as Train Vendor (uncredited)	   
*Kate Campbell as Betsy, Kates Maid (uncredited)	   
*Nick Copeland as Thomas, Bank Messenger (uncredited)	   
*Charles Crockett as Committee Man (uncredited)	   
*Eva Dennison as Woman at Graduation (uncredited)	    Donald Douglas as Intern at Clinic (uncredited)	   
*Bobby Dunn as News Vendor (uncredited)	   
*Lillian Harmer as Tillie, Scrubwoman in Bank (uncredited)	   
*Forrester Harvey as Tramp in Park (uncredited)	   
*Tom Herbert as Salesman Sleeping on Train (uncredited)	   
*Samuel S. Hinds as Henry, Banking Clerk (uncredited)	   
*Art Howard as Angry Bank Customer (uncredited)	   
*Arthur Stuart Hull as Man at Graduation (uncredited)	   
*Isabelle Keith as Nurse at Clinic (uncredited)	   
*Kathleen Kerrigan as Mrs. Stillman (uncredited)	    Fred Lee as President Wilson at Princeton (uncredited)	   
*Kendall McComas as Freckled Boy (uncredited)	   
*Tom McGuire as Train Conductor #2 (uncredited)	   
*Alex Melesh as Ragman (uncredited)	    Dave Morris as Minor Role (uncredited)	   
*Robert Paige as Wedding extra (uncredited)	   
*Lee Phelps as Railway Clerk (uncredited)	   
*Phillips Smalley as Committee Man (uncredited)	   
*Jerry Tucker as Jerry, Young Clinic Patient (uncredited)	    William Worthington as Teller (uncredited)	

==Crew==
*Lucien Hubbard - Producer David Townsend - Art Director
*Edwin B. Willis - Set Designer
*Douglas Shearer - Recording Director
*Dolly Tree - Wardrobe
*Maurice De Packh - Orchestrator (uncredited) 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 