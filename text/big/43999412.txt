Ithiri Neram Othiri Karyam
{{Infobox film
| name = Ithiri Neram Othiri Karyam
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer = OM John
| writer = Balachandra Menon
| screenplay = Balachandra Menon
| starring = Sukumari Srividya Venu Nagavally Balachandra Menon Johnson
| cinematography = Anandakkuttan
| editing = G Venkittaraman
| studio = St.Joseph Cine Arts
| distributor = St.Joseph Cine Arts
| released =  
| country = India Malayalam
}}
  1982 Cinema Indian Malayalam Malayalam film, directed by Balachandra Menon and produced by OM John. The film stars Sukumari, Srividya, Venu Nagavally and Balachandra Menon in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
  
*Sukumari 
*Srividya 
*Venu Nagavally 
*Balachandra Menon 
*Jose Prakash  Shubha  Lissy 
*Maniyanpilla Raju 
*Prathapachandran 
*Bheeman Raghu 
*Jalaja 
*Janardanan 
*Lalu Alex 
*P. K. Abraham 
*Poornima Jayaram 
*Santhakumari 
 
 
==Soundtrack==
The music was composed by Johnson (composer)|Johnson. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Akkare Ikkare || K. J. Yesudas, Chorus || Madhu Alappuzha || 
|- 
| 2 || Ithalazhinju vasantham || K. J. Yesudas, Shailaja M Ashok || Madhu Alappuzha || 
|- 
| 3 || Ithiri neram aha (Jyothi Rathnangal) || K. J. Yesudas, Chorus, Lathika || Madhu Alappuzha || 
|-  Janaki || Madhu Alappuzha || 
|}

==References==
 
 
==External links==
*   
*  

 
 
 


 