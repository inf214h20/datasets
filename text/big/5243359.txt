Heart of America (film)
 
{{Infobox film
 | name = Heart of America
 | image = BLIT1463.jpg
 | caption = DVD release cover
 | alt = 
 | director = Uwe Boll
 | producer = Shawn Williamson  Wolfgang Herold
 | country = Canada Germany
 | writer = Robert Dean Klein
 | starring = Jürgen Prochnow  María Conchita Alonso  Clint Howard  Miles Meadows  Brendan Fletcher  Lochlyn Munro  Will Sanderson  Maeve Quinlan  G. Michael Gray  Michael Belyea  with Patrick Muldoon   and Michael Pare
 | music = Reinhard Besser
 | editing = David M. Richardson
 | language = English
 | released =  
 | runtime = 87 minutes
 | budget = $5 million (estimated)
 }} Home Room) is a 2002 drama film by German director Uwe Boll about a fictional school shooting in a suburban high school. The film also addresses the issue of school bullying.

==Plot  ==
The last day of school contains many problems for both teachers and students of Riverton High School, and there are multiple storylines about this.
 English teacher William Pratt, who has let his professional frustrations get the better of him, while guidance counselor Vanessa Jones attempts to get through to student drug dealer Wex Presley, who is ruining her students.
 bully Ricky Herman begins to recognize the consequences of his actions.

Unbeknownst to these people, two bullied students, Daniel Lynn and Barry Shultz, are about to shoot up the school as an act of revenge against their tormentors. Barry, however, is having second thoughts, while the seething Daniel prepares to unleash his rage. Daniel decides to attack his high school on the last day, insisting that he does not care if he lives or dies, tired of constant harshness and abuse from his uncaring father, Artie, and from bullies at school; Ricky amongst them. On the last day, Daniel carries out his plan, aided by Dara, who spontaneously joins him. Daniel confronts three of his past tormentors: Paul, Ricky, and Jeff, and kills them. Kevin is shot and killed when he and Robin come into the line of fire, much to Daniels regret, while Robin is spared. Meanwhile, Dara walks into her English class and kills Mr. Pratt, and Karen, who had been a rival for the attentions of Tommy. Dara is then subdued by bully Donny Pritzee, who was presumbably in love with Karen, and is turned over to the police. Barry walks away from the school, having bailed out on the plan, believing there are other responses to the abuse he and Daniel suffered in the past.

As the film ends, a reporter informs the public of the school shooting, revealing that Daniel committed suicide, and the camera turns away from the TV to show Barrys and Daniels shocked parents watching. At Daniels home, the phone begins to ring, but Artie cannot bring himself to pick it up.

==Cast==
;Killers
*Kett Turton as Daniel Lynne, the ringleader of the shootings.
*Elisabeth Rosen as Dara McDermott, a meth-addicted outcast. 
;Kids
*Michael Belyea as Barry Shultz, Daniels best friend
*Elisabeth Moss as Robin Walters, a pregnant girl who hopes to become a waitress. 
*G. Michael Gray as Wex Presley, a student drug dealer who sells meth to Dara. 
*Kevin Mundy as Tommy Bruno, Karyns boyfriend and a jock. 
*Spencer Achtymichuk as James Pratt, Wills toddler-aged son, who has a habit of repeating the curse words Will lets slip.
*Lily Shavick as Paula, Karyns best friend.
*Michaela Mann as Slow White
*Chris Jovick as Kenny
*Justin Stillwell as Joe
*Mike Savage as Dave

;Bullies
*Will Sanderson as Frank Herman, the head bully.
*Matthew MacCaull as Donald "Donny" Pritzi, a jock/bully.
*Christopher Marvrikos as Lenny, the least intelligent of the bullies.

;Adults
*Jurgen Prochnow as Harold Lewis, the principal of Riverside High School. 
*Maria Conchita Alonso as Vanessa Jones, the schools guidance counselor.
*Clint Howard as Arthur "Artie" Lynne, Daniels abusive, uncaring father.
*Maeve Quinlan as Becky Schultz, Barrys mother. 
*Chilton Crane as Helena Pratt, Wills wife and James mother.
*Lesley Ewin and Brigit Stein as Ms. Weinberg and Mrs. Hall, the schools office administrators.
*Patrick Muldoon as Ryan Kirkland, a friend of Wills who works at the school.

;Victims
*Alejandro Rae as Paul
*Brendan Fletcher as Ricky Herman
*Steve Byers as Jeff, another bully
*Miles Meadows as Kevin Rhodes, Robins boyfriend Dave Sanders, as both were the only teachers killed in their respective shootings. 
*Stephanie MacGillivray as Karyn Lewis, the principals daughter.

;Miscellaneous
*Benjamin Derrick as the mailman
*Lochlyn Munro as the reporter
*Brad Loree as a man with a mask
*Brent Connolly, Richard Kahan, and John Kralt as additional students
*Nick Allen, David Ferguson, Gary Ferguson, and Dustin Brooks as "small students"

==Reception==
The film holds no score based on critics reviews at the review aggregator website Rotten Tomatoes though 43% of users liked it. 

==See also==
* School Violence
* Elephant (2003 film)|Elephant - A film inspired by the Columbine High School massacre Zero Day, Another 2003 film inspired by the Columbine High School massacre The Only Way, a 2004 American independent film inspired by the Columbine High School massacre
* Duck! The Carbine High Massacre - Yet another film inspired by the Columbine High School massacre
*  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 