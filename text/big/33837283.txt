In Absentia (film)
{{Infobox film
| name           = In Absentia
| image          =
| caption        =
| director       = Stephen Quay Timothy Quay Keith Griffiths
| writer         =
| narrator       =
| starring       = Marlene Kaminsky
| music          = Karlheinz Stockhausen
| cinematography =
| editing        =
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 20 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}

In Absentia, a short film commissioned by the BBC as a part of a series called “Sound on Film International”, was a collaboration with the filmmakers The Brothers Quay and musical composer Karlheinz Stockhausen, elder statesman of the twentieth-century musical avant-garde. Keith Griffiths produced the film with production companies, Illuminations Films and Koninck. Marlene Kaminsky plays the woman in the film.   

==Plot==
A seated woman, alone in a chair at a table in a room on one of the top floors of an asylum, repeatedly writes on a piece of paper and sharpens pencils. The pencil point often breaks under her fingers force. She places the broken points outside the window on the sill. A satanic figure is somewhere nearby, animated and made of straw or clay, not flesh. A spotlight lights up her window randomly. She finishes her writing, tears the paper from the pad, folds it, places it in an envelope, and slips it through a slot that contains many more letters. Great emphasis is placed on extreme close-ups of the objects central to her existence: the pencils, the sharpener, the paper, her cramped, clenching hands, blackened fingernails, endless stubs of broken-off lead, and finally the letters themselves, packaged up and posted uselessly into a grandfather clock.   

==Influence==
 . The image was so powerful of letters written to her husband that were deeply disturbed writing, where she would write over the top of the original letter again and again until it became a graphite blur of imagery. So we said ‘this is what the film would be about.’”    The film is dedicated to "E.H. who lived and wrote to her husband from an asylum."    The Brothers Quay later learned that composer Karlheinz Stockhausen found their film compelling on a more intimate level. They later said, “The surprising thing is that when Stockhausen saw the film at an avant-premiere he was moved to tears. We only later learned that his mother was imprisoned by the Nazis in an asylum, where she later died. Even for us this was a very moving moment, especially because we directed the film without knowing any of this." 

==Release details==
The black and white film, with some parts color, was originally created in 35mm format and is twenty minutes long. It was released by Zeitgeist Films in 2000. 

==References==
 

==External links==
* 

 
 