The Lucky Ones (film)
{{Infobox Film
| name        = The Lucky Ones
| image       = Lucky ones poster.jpg
| caption     = Theatrical release poster
| director    = Neil Burger
| producer    = Neil Burger Brian Koppelman David Levien Rick Schwartz
| writer      = Neil Burger  Dirk Wittenborn
| starring    = {{Plain list |
* Tim Robbins 
* Rachel McAdams
* Michael Peña
}}
| music       = Rolfe Kent
| cinematography = Declan Quinn
| editor      = Naomi Geraghty
| studio      = Koppelman & Levien Productions  Overnight Productions   QED International Lionsgate  Roadside Attractions
| released    = September 26, 2008
| runtime     = 115 minutes
| country     = United States
| language    = English
| budget      = $14 million    
| gross       = $266,967   
}}

The Lucky Ones is a 2008 American comedy-drama directed by Neil Burger. The screenplay by Burger and Dirk Wittenborn focuses on three United States Army soldiers who find themselves drawn together by unforeseen circumstances.

==Plot== Las Vegas. shrapnel injury before he reunites with his girlfriend.
 blackout that ended just prior to their arrival. Rather than face a potentially long wait before normal flight schedules resume, they rent a minivan and begin to drive westward.

Upon arrival in St. Louis, Cheaver is thrilled to learn his son Scott has been accepted at Stanford University but stunned to learn his wife Pat wants a divorce. He decides to visit his brother in Salt Lake City but first drives Colee and T.K. to the airport so they can fly to Vegas. When they see how devastated Cheaver is, they fear his state of mind will put him at risk on the open road and decide to continue with him. As their journey progresses and they open themselves up to each other, the three gradually become closer and find themselves sharing unexpected adventures. At a revival meeting conducted by Pastor Jerry Nolan, a member of the congregation invites them to a birthday party in his palatial home, where they must endure anti-war sentiments expressed by the mans adult children and Cheaver is seduced by a guest who expects him to participate in a threesome with her and her husband Bob. Later, Colee and T.K. leave Cheaver at a campsite while they go in search of food, and while driving they are forced to flee an approaching tornado and take shelter in a drainage ditch. As they cling closely to each other, T.K. discovers he might not need the sex surrogate after all.

Scotts scholarship will pay for only part of his tuition and he needs to pay the $20,000 balance immediately in order to secure his place at the university in the upcoming fall semester. Cheaver decides to bypass Salt Lake City and travel to Vegas to try his luck in the casinos. When Colee discovers that a guitar similar to the one she is returning to her boyfriends parents recently sold at an online auction for $22,000, she is tempted to give it to Cheaver, but he encourages her to complete her mission.

Colee is welcomed warmly by her boyfriends parents, Tom and Jeanie Klinger, but quickly discovers that not only nothing he had told her about himself and his past was true, but he had omitted some important details as well. Neither of the Klingers recognize the guitar, supposedly a family heirloom, and living with them are Shannon and the baby she had after a one-night stand with their son. The Klingers invite Colee to spend the remainder of her leave with them. Disillusioned, she declines, but she asks if she can keep the guitar, and they readily agree.

Colee, Cheaver, and T.K. are reunited at the local police station, where T.K. has been brought after confessing to a casino robbery Colees boyfriend had claimed he committed before joining the army. T.K.s plan to avoid returning to the Middle East by being sentenced to a prison term backfires when he learns the crime was yet another fabrication. Colee insists Cheaver take the guitar but he tells her he already has the $20,000 he needs. His friends are stunned he won the money so quickly, but Cheaver confesses he received it as a bonus for rejoining the Army. They go their separate ways, but three weeks later meet again at the airport as they prepare to return to battle.

==Cast== USA
* USA
* USA
* Molly Hagan as Pat Cheaver
* Mark L. Young as Scott Cheaver
* Spencer Garrett as Pastor Jerry Nolan
* Howard Platt as Stan Tilson
* Arden Myrin as Barbara Tilson John Heard as Bob
* Jennifer Joan Taylor as Bobs wife
* Katherine LaNasa as Janet
* Scott Jaeck as Guitar Store Owner John Diehl as Tom Klinger
* Annie Corley as Jeanie Klinger

==Production== The Illusionist in Prague, where the Iraq War frequently was debated. He also drew inspiration from The Last Detail and its seriocomic mood, episodic structure, and such plot details as an odd religious detour and a meeting with prostitutes.  In A Look Inside The Lucky Ones, a bonus feature included in the films DVD release, he explained he intentionally avoided mentioning Iraq because he wanted the audience to concentrate on the heart of the characters rather than focus on the specific war they were fighting. The film, which takes no position on the war, is the first Iraq War-related Hollywood project the Army has supported by providing personnel and technical help. 
 Arlington Heights, Park Ridge Chesterfield and Grand Junction in Colorado, and Las Vegas.

After seeing a rough cut of the film in September 2007, Lions Gate Entertainment executives considered rushing it into theaters in order to qualify for Academy Award consideration. But when the similarly war-themed films In the Valley of Elah, Rendition (film)|Rendition, and Lions for Lambs opened and failed to generate much box office revenue, the studio and Neil Burger decided to schedule it for Spring 2008. 

When the films future remained in limbo, producer Rick Schwartz acknowledged the difficulty in marketing a war-related film to an audience that recently had rejected three, although The Lucky Ones primarily is a road movie, and its sole combat scene lasts only forty seconds. "What should we do with this one?," Schwartz wondered. "Do you do a trailer that’s more light and comedic that hides the fact that it’s really about three soldiers, or do you try to stay as true to the spirit of the film as possible?"  Burger considered either trimming the already short combat scene or removing it completely "to somehow mitigate or minimize the problem, or the perceived problem," but ultimately decided, "To not have the more serious scenes to balance out the humor feels somehow disrespectful to these characters." 
 ShoWest on March 10, 2008 and was shown at the Toronto International Film Festival before opening on 425 screens in the United States and Canada on September 26. It earned $183,088 on its opening weekend, ranking #33 at the box office. 

==Critical reception==
The Lucky Ones received negative reviews from critics. Review aggregation website Rotten Tomatoes gives the film a score of 36% based on reviews from 72 critics. The sites consensus states, "The Lucky Ones features heartfelt performances, but is undone by the plots overwrought parade of coincidence and contrivance." 

Laura Kern of the New York Times noted the film "has little interest in making bold pro- or antiwar proclamations. With a smooth, light touch (though not without bumps and awkward moments) it focuses instead on the idea that the present and the people who factor into it are all we really have. Aside from the opening, the only combat scenes to be found here are strictly domestic, and, in terms of its lead characters, internal . . . Their journey is jampacked with misadventures and personal epiphanies, some touching, others laughably contrived . . . But because the lead actors work so well together, adding depth and levels of vulnerability to fairly underwritten roles, the emotional consequences of the sense of displacement these lucky characters — lucky to be alive, lucky to have met one another — must deal with always ring true." 

Roger Ebert of the Chicago Sun-Times observed, "What makes "The Lucky Ones" so gratifying to me is anything but gravitas; these three characters are simply likable, warm, sincere and often funny. The performances are so good, they carry the film right along . . . I believe audiences will be moved by the characters. I was." 

Owen Gleiberman of Entertainment Weekly graded the film B− and noted, "Its all very facile — wars domestic fallout made into feel-good fodder — but The Lucky Ones isnt dull, and the actors do quite nicely, especially McAdams, whos feisty, gorgeous, and as mercurial as a mood ring." 

Todd McCarthy of Variety (magazine)|Variety thought, "Its hard to find the genuine heartfelt moments . . . under the clutter of narrative contrivances and coincidences . . . The daily life-and-death crises these characters faced in Iraq are replaced on their trip by the less dire but still pressing matters facing them at home, the subtext being that Iraq represents a distraction from domestic matters, which consequently suffer from neglect. There are moments when the sought-after poignancy born of this dilemma is felt, but they are all too fleeting and dominated by exaggerated dramatics and broad comedy, especially as they relate to intimate matters. Tone overall is ill managed." 

Ann Hornaday of the Washington Post said the film "is drenched not just in contrivance but condescension, for its protagonists and the clueless civilians they encounter. Surely all those constituencies deserve better." 
 Arrested Development episode, and the final 15 minutes are particularly frustrating   All of the actors make the best of well-intentioned plot turns that often dont feel true. The Lucky Ones has plenty of heart and courage. If it only had a brain ..." 

Marjorie Baumgarten of the Austin Chronicle rated the film 2½ out of four stars and commented, "So far, the box office has not been terribly kind to movies about the Iraq war or the soldiers who wage it. The Lucky Ones is not likely to alter that situation, although the solid threesome of actors at the films core and the storyline set entirely on American soil may earn the film greater props from moviegoers than some of the previous films. In many ways, The Lucky Ones is a universal story about rudderless Americans, not just this particular pack of Army veterans. Yet, the story   contains too many coincidences and convergences to wholly ring true   Robbins is quietly effective as the elder of the bunch, while Peña shows us something of the growth of a cocky kid into sobering adulthood." 

==Home media==
  
The film was released on Region 1 DVD on January 27, 2009. It is in anamorphic widescreen format with an English audio track and English and Spanish subtitles. The sole bonus feature is A Look Inside The Lucky Ones, which includes interviews with cast and crew members.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 