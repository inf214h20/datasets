Two Smart People
 
{{Infobox film
| name           = Two Smart People
| image          = Twosmartpeople1946.jpg
| caption        = Theatrical release poster
| director       = Jules Dassin
| producer       = Ralph Wheelwright
| screenplay     = Leslie Charteris Ethel Hill
| story          = Ralph Wheelwright Allan Kenward
| starring       = Lucille Ball John Hodiak Lloyd Nolan
| music          = George Bassman
| cinematography = Karl Freund Chester W. Schaeffer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $1,074,000  . 
| gross          = $1,199,000 
}}

Two Smart People is a 1946 American film directed by Jules Dassin, and starring Lucille Ball and John Hodiak, Lloyd Nolan and Hugo Haas. 

==Plot==
Ace Connors (John Hodiak) is a con man and hes got half a million dollars in bonds hidden in a cookbook. When he tries to sell a bogus oil investment to Dwight Chadwick (Lloyd Corrigan)at a Beverly Hills hotel, Dwights attractive friend, Ricki Woodner (Lucille Ball), intervenes with a scam of her own.

Ace is about to go to prison for his part in the theft of the bonds . He cuts a deal to reduce his sentence by testifying. This doesnt please his former partner in crime, Fly Feletti (Elisha Cook, Jr.).

A cop, Bob Simms (Lloyd Nolan), is assigned to accompany Ace on the train from Los Angeles to New York. The passengers include Ricki, who is falling for Ace and wants to help, and Fly, who wants Ace not to get to New York.

Along the way, Ace and Ricki manage to get off the train in New Orleans to enjoy Mardi Gras together. When they do, Ace leaves the book at a costume shop, confident no one will notice it until he returns for it. During a romantic moment around midnight, Ace reveals to Ricki where hes hidden the bonds. Fly makes his move, but Simms is able to beat him to the draw. Ace fears that con artist Ricki has taken it on the lam with his dough, but she turns up in the end, ready to wait for Ace till hes out of Sing Sing.

==Cast==
* Lucille Ball as Ricki Woodner
* John Hodiak as Ace Connors
* Lloyd Nolan as Bob Simms
* Hugo Haas as Seïnor Rodriquez, Dept. of Agriculture
* Lenore Ulric as Maria Ynez, Inn of the 4 Winds
* Elisha Cook, Jr. as Fly Feletti
* Lloyd Corrigan as Dwight Chadwick
* Vladimir Sokoloff as Monsieur Jacques Dufour
* David Cota as Jose
* Clarence Muse as Porter

==Reception==
The film earned $871,000 in the US and Canada and $328,000 elsewhere causing MGM a loss of $252,000. 

===Critical response===
When the film was released the film critic for The New York Times panned the film writing, "Except for a lively and colorful series of Mardi Gras sequences in New Orleans, which are introduced quite late in the picture, Two Smart People is an otherwise dreadfully boring hodgepodge about love and the confidence racket ... John Hodiak and Lucille Ball are the principals and they are painfully defeated by the script at almost every turn. Lloyd Nolan as the patient sleuth fares a little better, however. But in addition to its pedestrian plot, Two Smart People also suffers from lack of competent direction." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 