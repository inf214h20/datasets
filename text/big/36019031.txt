Ruri no Kishi
 
{{Infobox film
| name           = Ruri no kishi
| image          = 
| caption        = 
| director       = Kaneto Shindo
| producer       = Tengo Yamada
| writer         = Kaneto Shindo
| starring       = Mie Kitahara
| music          = 
| cinematography = Takeo Itō
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 1956 Japanese drama film written and directed by Kaneto Shindo.   

==Cast==
* Mie Kitahara as Chiho Teraoka
* Nobuko Otowa as Hagiyo, Chihos mother
* Rentarō Mikuni as Ryūkichi Fukase
* Ranko Akagi as Yoshie, Ryūkichis mother
* Sen Hara as Sama (as Senko Hara)
* Nobuo Kaneko as Takakura
* Sachiko Murase as Uta, Chihos grand mother
* Terumi Niki
* Ichirō Sugai as Sōkichi, Ryūkichis father
* Yoshiko Tsubouchi as Kuniko (as Mieko Tsubouchi)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 