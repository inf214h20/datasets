El cónsul de Sodoma
{{Infobox film
| name           = The Consul of Sodom
| image          = El consul de sodom.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Sigfrid Monleón
| producer       = Andrés Vicente Gómez
| writer         = Miguel Dalmau
| screenplay     = Joaquín Górriz   Miguel A Fernández   Sigfrid Monleón
| based on       = the biography by Miguel Dalmau
| starring       = Jordi Mollà   Bimba Bosé   Àlex Brendemühl   Josep Linuesa  Isaac de los Reyes 
| music          = Joan Valvent 
| cinematography = Jose David Garcia Montero
| editing        = Pablo Blanco
| studio         = 
| distributor    = Rodeo Media 
| released       =  
| runtime        = 112 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 335.848,24 €  
}}
The Consul of Sodom ( )  is a 2009 Spanish film directed by Sigfrid Monleón and starring Jordi Mollà. It is a biopic of the Catalan poet Jaime Gil de Biedma, based on a biography written by Miguel Dalmau. 

==Plot==
In 1959, Jaime Gil de Biedma, a wealthy poet from Barcelona, is visiting Manila on business trip as director of the Philippine Tobacco Company. At night, the poet gives free rein to his homosexuality. He meets Johnny, a young man who works in an erotic nightclub, and they have sex. The poverty of Manila makes a deep impression on Jaime and heightens his social conscience. 

Back in Barcelona, the Spanish police interrogate Jaime about some of his subversive friends who are still dreaming of regime change in Spain. Ironically Jaime is refused membership in the Communist Party because he is gay. He visits his friend and editor Carlos Barral, and meets Juan Marsé, a young writer about to publish his first novel. Jaime is trying to save his relationship with Luis, his lover, but although in love with him, Jaime treats Luis, who is of humble background, with contempt. After a heated argument Luis leaves him for good. Describing himself as "a Sunday poet with a Monday conscience", Jaime mixes his weekly working days for his familys company with a bohemian lifestyle on weekends. Don Luis, Jaimes father, takes care of Jaimes troubles with the police, but he warns his son he has to sort his life out because he is putting his family and the business in jeopardy. 

By the mid 1960s, Jaime favorite spot is the Bocaccio nightclub where he meets the sexy and enigmatic Bel, a divorced woman with two kids. They quickly establish a relationship. Bel is entangled in a bitter battle with her ex-husband for the custody of her children. Jaime buys an apartment and asks Bel to marry him, but she turns him down. They are two free spirits, getting married would condemn their relationship to failure. Overwhelmed by the events, Jaime loses himself in the night and gets drunk. That same night, Bel dies in a tragic accident. When he hears the news, Jaime, in despair, tries to take his own life. He manages to get back on his feet with the support of friends and family. However, Jaime will never write poetry again. At the beginning of the 1970s, Jaime goes to the Philippines and has to deal with the economic changes which the company is going through under Ferdinand Marcoss dictatorship.

On his return, Jaime meets Toni, a young assistant of photography of humble background, and they begin a sentimental relationship. In spite of his class awareness, he seems to be attracted to men of lower background. Toni, for his part, insists on learning all he can from Jaime and asks him to introduce him into his sophisticated world. One day, at the beach with Tony and some friends, Jaime is move to tears watching the freshness of a girl dancing with Tony. As a middle-aged poet, Jaime is painfully aware of the passage of time, his misspent youth, and the death that patiently awaits him. He writes in a poem: "What do you want now, youth, you impudent delight of life?,"  "What brings you to the beach? We old ones were content until you came along to wound us by reviving the most fearful of impossible dreams. You come to rummage through our imaginations." 

Jaimes father dies and events pile up. The growing tension between Jaime and Toni leads to a violent confrontation in the country house which Jaime has bought as their love nest. Toni throws him out, and Jaime trips and is injured in the snow. He almost dies. "The fact that life was to be taken seriously we understand only later", he narrates in another poem, "Like all young people, I was going to change the world. I wanted to make my mark and withdraw to applause. Growing old, dying, it was all a question of the size of the theater. But time has passed and I see the unpleasant truth. Growing old, dying, is the plays only plot."

Years later, old and tired, Jaime lives out his days with a young stage actor. He finds out that he has AIDS. The passing of time and illness have left their mark on him. His friends, who know that Jaime is dying, organize a poetry recital at Madrids famous Students Residence, which turns into a public acclamation of the poet. Although old and approaching death, he still yearns for youth and beauty. He hires a young male prostitute, but in a hotel room he can only passively observe the young naked man dancing to The Pet Shop Boyss song Always on My Mind.

==Cast==
*Jordi Mollà -  Jaime Gil de Biedma 
*Bimba Bosé - Bel
*Àlex Brendemühl - Juan Marsé
*Josep Linuesa  - Carlos Barral
*Isaac de los Reyes - Toni
*Juli Mira - Jaimes father
*Alfonso Bergara - Luis
*Marc Martínez  - Víctor Anglada
*Isabelle Stoffel - Colita
*Vicky Peña - Jaimes mother

==Reception==
The Consul of Sodom was shot on location in Madrid, Barcelona and Manila. Shooting took place between 19 January 2009 and 3 April 2009. The film premiered on 11 December 2009.
The film generally received good reviews. It was nominated to six Goya Awards including best actor for Jordi Mollà, but did not win any.

The film caused a stir in Spain, with the real-life Juan Marse, now a revered novelist, expressing his strong indignation in the national press. He referred to The Consul of Sodom as "grotesque, ridiculous, phony, absurd, dirty, pedantic, directed by an incompetent and ignorant fool, badly acted, with deplorable dialogue. Its a shameless film, with an infamous title and produced by unscrupulous people." 

==External links==
*  

==References==
 

 
 
 
 