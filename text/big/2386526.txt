Fraternity Vacation
 
{{Infobox film
| name = Fraternity Vacation
| image = FraternityVacation.jpg
| caption = Fraternity Vacation DVD cover
| director = James Frawley
| writer = Lindsay Harrison
| producer = Robert C. Peters Larry A. Thompson (executive producer)
| starring = {{Plainlist|
* Stephen Geoffreys
* Sheree J. Wilson
* Cameron Dye
* Tim Robbins
* Leigh McCloskey Matt McCoy
* John Vernon
}}
| music = Brad Fiedel Paul Ryan
| editing = 
| studio = New World Pictures
| distributor = New World Pictures Anchor Bay Entertainment (DVD)
| released = February, 1985
| runtime = 84 min.
| awards =
| preceded_by =
| followed_by = United States English
}} Iowa State, with Tim Robbins and Cameron Dye as Theta Pi Gamma frat boys (or, as they are known to their Iowa State frat rivals, "Theta Pigs").  On spring break in Palm Springs, California, several boys  compete for the affections of a sophisticated co-ed, played by Sheree J. Wilson.

==Cast==
*Stephen Geoffreys ... Wendell Tvedt
*Sheree J. Wilson ... Ashley Taylor
*Cameron Dye ... Joe Gillespie
*Leigh McCloskey ... Charles Chas Lawlor III
*Tim Robbins ... Larry Mother Tucker Matt McCoy ... J.C. Springer
*Amanda Bearse ... Nicole Ferret
*John Vernon ... Chief Ferret
*Nita Talbot ... Mrs. Ferret
*Barbara Crampton ... Chrissie
*Kathleen Kinmont ... Marianne
*Max Wright ... Millard Tvedt Julie Payne ... Naomi Tvedt
*Franklin Ajaye ... Harry

==Reception==
Roger Ebert gave the film one star, his lowest rating:
 Dont get me wrong. I have nothing against dumb sex comedies. All I object to is the fact that "Fraternity Vacation" is playing with half a deck - the male half. The men are the characters and the women are the objects.  
 The Tech (MIT) said that the film was a poor example of its genre, and "not worth seeing unless youre really in the mood for this type of movie". 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 

 