The House of Mirth
 
 
{{Infobox book
| name          = The House of Mirth
| image         = File:The House of Mirth.JPG
| imagesize     = 200px
| caption       = The House of Mirth, Penguin Books, 1993
| author        = Edith Wharton
| cover_artist  =
| country       = United States
| language      = English
| genre         = Novel
| publisher     = Charles Scribners Sons
| release_date  = October 14, 1905
| media_type    = print
| isbn          = NA   
| preceded_by   =  
| followed_by   =  }}

<!--  
 
 
 
 
 
 
 
  -->
 high society of New York City, who was raised and educated to become wife to a rich man, a hothouse flower for conspicuous consumption. As an unmarried woman with gambling debts and an uncertain future, Lily is destroyed by the society who created her.  
 American literary naturalism. 

==Title==
 
 Book of Ecclesiastes, 7th chapter, verse 4:

  
 social stratum to which she belonged by birth, education, and Etiquette|breeding. 
 Romantic imagery of the first stanza of the poem “She was a Phantom of Delight” (1804), by William Wordsworth (1770–1850), which describes an ideal of beauty of the type that narrowly circumscribed the life of Lily Bart:

 

==Plot==
In The House of Mirth (1905), the story of Lily Bart begins with her visiting the apartment of Lawrence Selden, a man for whom she has romantic feelings, but, for the sake of a high social-standing, decides that she must marry Percy Gryce, a young, timid millionaire who is wealthier than Selden. Despite the people of her social circle being convinced that Percy will propose marriage to Lily when they next meet, Lily changes her mind, and withdraws from their relationship, on account of an unexpected visit from Lawrence Selden. Despite feeling convinced that he loves Lily, Selden does not yet want to risk marriage; meanwhile, Gryce marries another girl, from within their social circle.

 
 social standing begins to erode when Gus Trenor, husband of friend Judy, gives Lily a large sum of money, accepted in the innocent belief that it represents profits from investments Gus had made in her behalf. The rumors that arise from that transaction, worsened by her injudicious and mysterious visit to Guss townhouse in the city, further erode Lilys social standing. In the event, Lily one day receives a note from Selden, who asks to meet with her; certain that he shall propose marriage, Lily accepts to meet him the next day. Surprised by her acceptance, Selden becomes frightened by Lilys apparent change-of-heart, because earlier, Lily had escaped his attempt to kiss her; the fearful Selden flees New York City, first to Havana and then to Europe, cravenly leaving Lily with no notice.

To escape the rumors arisen from the gossip caused by her financial dealings with Gus Trenor, and also disappointed by Seldens emotional cowardice, Lily accepts Bertha Dorsets invitation to join her and her husband, George, on a cruise of Europe aboard their yacht, the Sabrina. Unfortunately, whilst at sea, Bertha falsely accuses Lily of adultery with George, in order to thwart the attention and suspicion of their social circle away from Berthas infidelity with the poet Ned Silverton. The ensuing social scandal ruins the personal reputation of Lily Bart, which then causes her abandonment by friends and disinheritance by Aunt Peniston.

Undeterred by such misfortunes, Lily fights to regain her place in high society, by befriending Mr and Mrs Gormer, but her enemy, the malicious Bertha Dorset, gradually communicates to them the “scandalous” personal background of Lily Bart, and, so, undermines the friedship with the Gormers, by way of whose sponsorship Lily hoped to become socially rehabilitated. Only two friends remain for Lily: Gerty Farish (a cousin of Lawrence Selden) and Carrie Fisher, who help her cope with the social ignominy of a degraded social-status, whilst continually advising Lily to marry as soon as reasonably possible.
 social strata of the high society of New York City. She obtains a job as personal secretary of Mrs. Hatch, who is a disreputable woman; and Lily resigns after Lawrence Selden returns to rescue her from complete infamy. Lily then finds a job in a Hatmaking|milliners shop; yet, unaccustomed to the rigors of working class  manual labour, her rate of production is low and the quality of her workmanship is poor, and is fired at the end of the New York social season, when the demand for fashion hats has ceased.

Meanwhile, Simon Rosedale, a Jewish suitor who earlier had proposed marriage to Lily, when she was higher in the scale of social classes, returns to her life and tries to rescue her, but Lily is unwilling to meet his terms: Simon wants Lily to use love letters, which she bought from her servant, to confirm the occurrence, years earlier, of a love affair between Lawrence Selden and Bertha Dorset. For the sake of Seldens reputation, Lily does not act upon Rosedales request, and secretly burns the love letters when she visits Selden, one last time.

In the event, Lily Bart receives a ten-thousand-dollar inheritance, from her Aunt Peniston, with which she repays Gus Trenor. Distraught by her misfortunate life, Lily had begun regularly using a sleeping draught of chloral hydrate to escape the pain of poverty and social ostracism; yet one day, Lily overdoses the sleeping draught, and kills herself. Hours later, Lawrence Selden arrives to her quarters, to finally propose marriage, but finds Lily Bart dead; only then was he capable of being close to her in the way he had proved incapable when she was alive.

==Critical reception==
In the contemporary book review “New York Society Held up to Scorn in three New Books” (15 October 1905) The New York Times critic said that  The House of Mirth is “a novel of remarkable power” and that “its varied elements are harmoniously blended, and   the discriminating reader who has completed the whole story in a protracted sitting, or two, must rise from it with the conviction that there are no parts of it which do not properly and essentially belong to the whole. Its descriptive passages have verity and charm, it has the saving grace of humor, its multitude of personages, as we have said, all have the semblance of life.” 
 leisure class. 

==Adaptations==
The novel The House of Mirth (1905) has been adapted to the stage and the cinema.

* The Play of the novel The House of Mirth (1906), by Edith Wharton and Clyde Fitch.   La Maison du Brouillard (1918), directed by Albert Capellani, featured Katherine Harris Barrymore as Lily Bart; a French silent film.
* The House of Mirth (1956), directed by John Drew Barrymore. Matinee Theatre: Season 2, Episode 56. (4 December 1956)
* The House of Mirth (1981), directed by Adrian Hall. A television film for the Public Broadcasting System in the U.S. The House of Mirth (2000), directed by Terence Davies, featured Gillian Anderson as Lily Bart. 

==References==
 

==Sources==
*   in  

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 