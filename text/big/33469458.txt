Answer This!
{{Infobox film
| name = Answer This!
| image = Answer This! Movie Poster.jpg
| alt = Answer This! Theatrical release poster
| caption = Theatrical release poster
| director = Christopher Farah 
| producer = Mike Farah Anna Wenger
| screenplay = Christopher Farah 
| starring = Christopher Gorham Arielle Kebbel Chris Parnell Nelson Franklin Evan Jones Kip Pardue Kali Hawk Ralph Williams
| music = John Paesano and James Renald
| cinematography = Christian Sprenger
| editing = Wendy Nomiyama
| distributor = Wrekin Hill and Neca Films
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget   = $2,000,000
| gross    = $5,567,890 
}}
 American 2011 comedy film written and directed by Christopher Farah, starring Christopher Gorham, Arielle Kebbel, and Chris Parnell.  The film was primarily filmed in Ann Arbor, Michigan and is set at the University of Michigan; it is the first film to have been filmed closely in cooperation with the University. 

==Cast==
 
* Christopher Gorham as Paul Tarson
* Arielle Kebbel as Naomi
* Chris Parnell as Brian Collins
* Kip Pardue as Lucas Brannstrom
* Nelson Franklin as James Koogly Evan Jones as Izzy Ice Dasselway
* Kali Hawk as Shelly
* Ralph Williams   as Dr. Elliot Tarson
* Michael Sinterniklaas as Umlatt the Flunkee
 

==Production==

===Filming=== University of the Barton Hills area, Washtenaw Dairy, Espresso Royale, Eight Ball Saloon, and the Michigan Union.

== References ==
 

==External links==
*  
*  
*  
*  

 
 
 