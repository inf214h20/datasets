The Jolly Boys' Last Stand
The Jolly Boys Last Stand is a 2000 low budget British comedy feature starring Andy Serkis and Milo Twomey. British comedian Sacha Baron Cohen appears in a minor role.  The film was written and directed by Chris Payne.

==Plot==

When Spider (played by Serkis), the President of a young mens drinking club becomes engaged, his oldest friend and best man, Des, (Twomey) decides to film the run up to the wedding as a gift to the betrothed – secretly hoping the on‐screen carnage will de‐rail the marriage.

==DVD release==

The DVD is distributed by Spirit Level Film. It includes a look behind the scenes at the original casting sessions held at Ealing Studios as director Chris Payne puts the cast through improvised auditions, plus an audio commentary from director Chris Payne, Andy Serkis, Milo Twomey, and Rebecca Craig.

==External links==
*  

 
 
 
 
 
 


 
 