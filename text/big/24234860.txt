The Girl from Monterrey
{{Infobox film
| name           = The Girl from Monterrey
| image          = Girl from Monterrey.jpg
| image_size     =
| caption        =
| director       = Wallace Fox
| producer       = Harry D. Edwards (associate producer) Jack Schwarz (producer) Robert Gordon (story) George Green (story) Arthur Hoerl (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Marcel Le Picard
| editing        = Robert O. Crandall
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
 PRCs version of the Mexican Spitfire. 

The film is also known as The Girl from Monterey (American alternative spelling).

==Plot summary==
In a Mexican nightclub, some American fight promoters witness Alberto Baby Valdez, the brother of Lita Valdez knock out a champion fighter.  At first Lita is angered that her brother has quit his law studies to become a fighter, but the two move to the United States.  Lita literally bumps into reigning champion Jerry OLeary with the three becoming inseparable friends.  However the American fight promoters force Alberto and Jerry to fight each other or face suspension.
  
==Cast==
*Armida Vendrell as Lita Valdez
*Edgar Kennedy as Doc Hogan, Fight Promoter
*Veda Ann Borg as Flossie Rankin
*Jack La Rue as Al Johnson Terry Frost as Jerry OLeary Anthony Caruso as Alberto Baby Valdez Charles Williams as Harry Hollis
*Bryant Washburn as Fight Commissioner Bogart
*Guy Zanette as Tony Perrone
*Wheeler Oakman as Fight Announcer
*Jay Silverheels as Fighter Tito Flores
*Renee Helms as Hat Check Girl

==Soundtrack==
* Armida - "Jive, Brother, Jive" (Written by Lou Herscher and Harold Raymond)
* Armida - "Last Nights All Over" (Written by Lou Herscher and Harold Raymond)
* Armida - "The Girl from Monterrey" (Written by Lou Herscher and Harold Raymond)

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 



 