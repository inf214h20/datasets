Rio Grande (film)
{{Infobox film
| name           = Rio Grande
| image          = RioGrande.jpg
| border         = yes
| caption        = Theatrical poster
| director       = John Ford
| producer       =  
| screenplay     = James Kevin McGuinness
| based on       =  
| starring       = {{plainlist|
* John Wayne
* Maureen OHara
}}
| music          = Victor Young
| cinematography = Bert Glennon Jack Murray
| studio         = Argosy Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| gross          = $2.25 million (US rentals) 
}}

Rio Grande is a 1950   (1948 in film|1948) and She Wore a Yellow Ribbon (1949).

John Wayne plays the lead in all three films, as Captain Kirby York in Fort Apache, then as Captain of Cavalry Nathan Cutting Brittles in She Wore a Yellow Ribbon, and finally as a promoted Lieutenant Colonel Kirby Yorke in Rio Grande (scripts and production billing spell the York  characters last name differently in Fort Apache and Rio Grande).  
 Ben Johnson, Claude Jarman, Jr., Harry Carey, Jr., and Chill Wills.
==Plot==
In Rio Grande, Lt. Col. Kirby Yorke (John Wayne) is posted on the Texas frontier to defend settlers against depredations of marauding Apaches. Col. Yorke is under considerable stress between the Apaches using Mexico as a sanctuary from pursuit and by a serious shortage of troops of his command. The action of the movie is set in the summer of 1879 ("fifteen years after the Valley Campaigns of 1864|Shenandoah").
 Tension is Trooper Jeff West Point Ben Johnson) (who is on the run from the law) and Daniel "Sandy" Boone (Harry Carey, Jr.), who take him under their wings.

With the arrival of Yorkes estranged wife, Kathleen (Maureen OHara), who has come to take the under-age Yorke home with her, further tension is added. During the war, Yorke had been forced by circumstances to burn Bridesdale, his wifes plantation home in the Shenandoah valley. Sgt. Quincannon (Victor McLaglen), who put the torch to Bridesdale, is still with Yorke and provides a constant reminder to Kathleen of the episode. In a showdown with his mother, Jeff refuses her attempt by reminding her that not only the commanders signature is required to discharge him, but his own as well, and he chooses to stay in the Army. The tension brought about in the struggle over their sons future (and possibly the attentions shown to her by Yorkes junior officers) rekindles the romance the couple once felt for each other.
 Civil War commander, Philip Sheridan (J. Carrol Naish), now commanding general of his department. Sheridan has decided to order Yorke to cross the Rio Grande into Mexico in pursuit of the Apaches, an action with serious political implications since it violates the sovereignty of another nation.

If Yorke fails in his mission to destroy the Apache threat he faces the threat of court-martial. Sheridan, in a quiet act of acknowledgment of what he is asking Yorke to risk, promises that the members of the court will be men "who rode down the Shenandoah" with them during the Civil War. Yorke accepts the mission. Now Col. Yorke must fight to save, and put back together, his family and his honor.

Yorke leads his men toward Mexico, only to learn that a wagonload of children from his fort, who were being taken to Fort Bliss, Texas|Ft. Bliss for safety, has been captured by the Apaches.  After permitting three troopers&mdash;Tyree, Boone and Jeff&mdash;to infiltrate the church in the Mexican village where the Indians have taken the children, Yorke leads his cavalry in a full-scale attack, rescuing all of the children unharmed, though he himself is wounded.  He is taken back to the fort by his victorious troops, where Kathleen meets him and holds his hand as he is carried on a travois into the post.  After Yorke recovers, Tyree, Boone, Jeff and two others receive medals.  At the ceremony, Tyree is given furlough to continue his run from the law, stealing Sheridans horse for the purpose.  As the troops pass in review, the movie closes.

== Cast ==
 
 
 
* John Wayne as Lt. Col. Kirby Yorke
* Maureen OHara as Kathleen Yorke
  Ben Johnson as Trooper Tyree
* Claude Jarman, Jr. as Trooper Jeff Yorke
* Harry Carey, Jr. as Trooper Daniel "Sandy" Boone
* Chill Wills as Dr. Wilkins
 
* J. Carrol Naish as Gen. Philip Sheridan
* Victor McLaglen as Sgt. Maj. Quincannon
* Grant Withers as Deputy Marshal
* Sons of the Pioneers as the Regimental Singers
  Peter Ortiz as Capt. St. Jacques
* Steve Pendleton as Capt. Prescott
* Karolyn Grimes as Margaret Mary
* Albert Morin as Lieutenant
* Stan Jones as Sergeant
* Fred Kennedy as Trooper Heinze

==Music==
The film contains folk songs led by the Sons of the Pioneers, one of which is Ken Curtis (Fords son-in-law and best known for his role as Festus Haggen on Gunsmoke).

==Production==
Ford wanted to make The Quiet Man first, but Republic Pictures studio president Herbert Yates insisted that Ford make Rio Grande beforehand, using the same combination of Wayne and Maureen OHara; Yates did not feel that the script of The Quiet Man was very good, and wanted Rio Grande to be released first to pay for The Quiet Man. To Yatess surprise The Quiet Man, on its eventual release in 1952, would become Republics number one film in terms of box office receipts. 
 Big Jake (1971). The first three were directed by John Ford.

This was the film debut of Patrick Wayne. 

The film was shot in  . - Retrieved 2008-07-21 

==Basis==
Some aspects of the story, notably the regiments crossing into Mexico, and undertaking a campaign there, loosely resemble the expedition conducted by the 4th Cavalry Regiment (United States) under Colonel Ranald S. Mackenzie in 1873. 

==See also==
* John Wayne filmography

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 