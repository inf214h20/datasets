Power Dive (film)
{{Infobox film
| name           = Power Dive
| image_size     = 150px
| image	=	Power Dive FilmPoster.jpeg
| caption        = James P. Hogan
| producer       = William H. Pine (producer) John W. Rogers (associate producer) William C. Thomas (producer)
| writer         = Edward Churchill (writer) Paul Franklin (story) Maxwell Shane (writer)
| narrator       =
| starring       = Richard Arlen Jean Parker Helen Mack
| music          = C. Bakaleinikoff
| cinematography = John Alton
| editing        = Robert O. Crandall
| studio         = Picture Corporation of America Paramount Pictures, Inc.
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = $86,000 ITS BEEN 14 YEARS AND FILMS CHANGE: So Pine-Thomas (We Want to Make a Million) Switch to Million-Dollar Movies
By THOMAS M. PRYORSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   25 Feb 1954: 25.  
| gross          = $1 million 
| preceded_by    =
| followed_by    =
| website        =
}}
 James P. Hogan. The film stars Richard Arlen, Jean Parker and Helen Mack. 

It was the first film from the producing team of Pine and Thomas, former press agents who had a producing unit at Paramount.. 
==Plot==
Ace test pilot Bradley Farrell (Richard Arlen), flying for McMasters Aviation Corp., breaks his leg when an overweight prototype crashes. Brads younger brother Douglas (Don Castle), a recent graduate in aeronautical engineering thinks Dougs flying is too dangerous, and is hired as a design engineer at McMasters. Carol Blake (Jean Parker) wants to interest Brad in her fathers design for an aircraft made of plastic. Doug pretends to be Brad because he is attracted to her but Brad meets Carol and takes her out flying. She introduces him to her blind father, Professor Blake (Thomas W. Ross), resulting in Brad becoming immersed in the professors new designs. 

Brads friend, Johnny Coles (Louis Jean Heydt), loses his life test flying his own, similar design,  that breaks apart in the air, leaving behind his wife and child. Despite his friends death, Brad convinces the company to build Blakes Geodetic airframe|"geodetic" aircraft design, with his brother put in charge of the project. 

After Brad returns from setting a new cross-country speed record, he proposes to Carol, but she is in love with Doug. Doug doesnt know Carols true feelings and with the test of the professors aircraft imminent, he is at odds with Brad over the new aircrafts design. Brad has to fly the aircraft for US Army officials but is worried that the heavy test equipment will make the aircraft dangerous to fly. Doug will fly with him on the test and when a g-force|9-G power dive is scheduled, Doug passes out. The test equipment breaks free, jamming the rudder. Brad forces Doug to parachute to safety, and then cuts the rudder wires, grabbing them with his bare hands. He manages to land the aircraft safely although his hands are cut badly. With the aircraft accepted, Brad gives up test flying to become a  vice-president of McMasters Aviation. Doug and Carol find happiness and marry.

==Cast==
* Richard Arlen as Brad Farrell
* Jean Parker as Carol Blake
* Helen Mack as Betty Coles
* Don Castle as Doug Farrell
* Cliff Edwards as Squid Watkins
* Roger Pryor as Dan McMasters, company president
* Thomas W. Ross as Professor Blake
* Billy Lee as Brad Coles
* Louis Jean Heydt as Johnny Coles
* Alan Baldwin as Young Reporter
* Pat West as Burly Mechanic
* Ralph Byrd as Jackson, fellow draftsman
* Tom Dugan as The Waiter
* Helen Lynd as Giggly Blond
* James Seay as Army Radio Operator

==Production==
 
Power Dive was the first release by Picture Corp. of America, an independent production company formed in December 1940 headed by William Pine and William C. Thomas, former press agents and then associate producers at Paramount. Principal photography took place from January 23 to mid-February 1941 with some scenes shot on location at the Metropolitan Airport in Van Nuys, California. 

Opening credits include the following statement: "Creative acknowledgement for technical assistance in use of Geodetic plane to the Pleweve Aircraft Corporation."  A Phillips 1-B Aeroneer (NX16075) and Player/CT-6A Plxweve/Greenleaf CT-6A (NX 19994) made by Plxweve aircraft were featured in the film.  Reviews noted that writer Edward Churchill had an aviation background and that star Richard Arlen was running a flyers training school near Hollywood. 

==Reception==
Power Dive was primarily a B film. Aviation Film Historian James H. Farmer characterized the film as fast-paced, low-budget formula film ..." Farmer 1984, p. 324.  Variety (magazine)|Variety called it "... good program entertainment, taking full advantage of present interest in aviation and national preparedness." 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 