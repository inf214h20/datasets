All for Love (2012 film)
{{Infobox film name       = All for Love image      = All For Love 2012 film poster.jpg caption    =  traditional = 三個未婚媽媽 simplified = 三个未婚妈妈 pinyin     = Sāngè Weìhūn Māmā }} director   = Jiang Ping producer   = Jin Zhongqiang Wu Weijian
| writer    = Zhu Ping Xu Yiwen
| based on  =  starring   = Ariel Aisin-Gioro Che Yongli Alec Su Ju Wenpei music      = Li Ge cinematography = Long Shensong Chen Youliang editing    = Zhan Haihong An Xiaoyu Li Guangtian studio     = SMI CORPORATION China Broadcasting Art Troupe 
|distributor=  released =   runtime = 92 minutes country = China language = Mandarin budget   =  gross    = 4.72 million 
}}
All for Love is a 2012 Chinese romantic comedy film directed by Jiang Ping and written by Zhu Ping and Xu Yiwen, starring Ariel Aisin-Gioro, Che Yongli, Alec Su, and Ju Wenpei.   All For Love was released in China on 26 October 2012.

==Cast==
* Ariel Aisin-Gioro as Ye Xiaomeng, the urban female white-collar.
* Che Yongli as Mei Ling, Liu Erbiaos lover.
* Alec Su as Gu Donghai, Ye Xiaomengs boyfriend.
* Ju Wenpei as Dong A Ping, a woman entrepreneur.

===Guest===
* Chen Maolin as Ye Xioamengs father.
* Zheng Yuzhi as Ye Xiaomengs mother.
* Zhang Jiayi as Liu Erbiao.
* Huang Lei as Officer Liu.
* Yang Mi as Xiao Ma.
* Tong Dawei as The taxi driver. Huang Yi as Wei Wei.
* Zhu Xijuan as Liu Erbiaos mother.
* Yan Bingyan as Liu Erbiaos wife.
* Siqin Gaowa as Grandmother Wang.
* Zhu Xu
* Wen Zhang
* Lu Chuan
* Purba Rgyal
* Sha Yi
* Yvonne Yung as The head nurse.
* Feng Gong
* Gong Hanlin Lu Qi
* Zhang Guangbei
* Jiang Hongbo
* Zhong Xinghuo
* Hu Ke
* Huang Xiaoli

==Production==
The film began production in April 2012 and finished filming on May 3, 2012. 

The film shot the scene in Nantong, Jiangsu, China. 

==Released==
It had its world premiere at the Huabin Opera House ( ) on October 23, 2012, and it was released on October 26, 2012 in China. 

The film also screened in the 4th China Image Film Festival and 8th Chinese American Film Festival. 

The film was only moderately successful with critics and at the box office.

==Award==
{| class="wikitable"
|-
! Award !! Category !! Name !! Outcome  !! Notes
|-
| rowspan="1"| 8th Paris Chinese Film Festival || Best Actress || Che Yongli ||   || 
|}

==References==
 

 
 
 
 
 
 
 
 

 