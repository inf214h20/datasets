Dr. Pomerantz
{{Infobox film
| name           = Dr. Pomerantz
| image          = 
| caption        = Film poster
| director       = Assi Dayan
| producer       = Eyal Shiray
| writer         = Assi Dayan
| starring       = Assi Dayan Michael Hanegbi Rivka Michaeli
| music          = Amit Poznansky
| cinematography = Boaz Yehonatan Yaacov
| editing        = Zohar M. Sela
| distributor    = 
| released       =  
| runtime        = 88 minutes
| rating         =
| country        = Israel
| language       = Hebrew
| budget         = $250,000   
}}
Dr. Pomerantz ( ) is a 2011 Israeli film directed by and starring Assi Dayan. It premiered in 2011 at the 27th Haifa International Film Festival and was theatrically released in Israel in February 23, 2012. The film was nominated for Best Screenplay at the Ophir Awards in September 2012.  The film became Dayans final as a director as well as an actor, before his death in May 2014.

In the film, Dayan plays a clinical psychologist named Dr. Yoel Pomerantz. Pomerantzs life is one big mess, both personally and professionally. His wife committed suicide because she thought their 5-year-old son was mentally retarded. The son, Yoav (Michael Hanegbi), who is now 30 years old, actually has Asperger syndrome. He works as a traffic inspector and more than anything else, loves to affix traffic tickets to car windshields.    

The film was met with mostly favorable reviews, though did very poor business at the Israeli Box office. The film initially remained unreleased on DVD, and was only available through Video on demand|VOD. But shortly after Dayans death, the film saw a first DVD release in June 2014.

==Cast==
*Assi Dayan &ndash; Dr. Yoel Pomerantz
*Michael Hanegbi &ndash; Yoav Pomerantz
*Rivka Michaeli &ndash; Rebecca Zimmer
*Shlomo Vishinsky &ndash; Izhak Bar-Ner
*Gavri Banai &ndash; Himself
*Shlomo Bar-Shavit &ndash; Vladek Nusbaum
*Shmil Ben Ari &ndash; Yossi Sherman

==References==
 

==External links==
* 

 
 

 
 
 
 


 
 