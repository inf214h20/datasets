Periya Idathu Mappillai
{{Infobox film
| name           = Periya Idathu Mappillai
| image          = 
| image_size     =
| caption        = 
| director       = Guru Dhanapal
| producer       = K. Murugan
| writer         = Guru Dhanapal (dialogues)
| screenplay     = Guru Dhanapal
| story          = Rafi Mecartin
| starring       =  
| music          = Sirpy
| cinematography = R. H. Ashok
| editing        = P. Sai Suresh
| distributor    =
| studio         = Murugan Movies
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1997 Tamil Tamil comedy Devayani and Mantra in lead roles. The film, produced by K. Murugan, had musical score by Sirpy and was released on 22 August 1997. The film is a remake of the Malayalam film Aniyan Bava Chetan Bava. 
   

==Plot==

Periya Thambi (Vijayakumar (actor)|Vijayakumar) and Chinna Thambi (Rajan P. Dev) are brothers and owners of a company. Gopalakrishnan (Jayaram) is from a poor family, despite being a gratuate, he cannot find a job. To support his family, he becomes the driver of Periya Thambi and Chinna Thambi. Periya Thambis daughter Lakshmi (Devayani (actress)|Devayani) and Chinna Thambis daughter Priya (Raasi (actress)|Mantra) fall in love with Gopalakrishnan. What transpires later forms the crux of the story.

==Cast==

 
*Jayaram as Gopalakrishnan
*Goundamani as Kaali
*Manivannan as Manian Gounder Devayani as Lakshmi Mantra as Priya Vijayakumar as Periya Thambi
*Rajan P. Dev as Chinna Thambi
*R. Sundarrajan (director)|R. Sundarrajan as Gopalakrishnans father Pandiyan as Pandiyan
*Ponvannan as Chellappa Vivek as Ramu
*Halwa Vasu as Vasu
*Kalaignanam as Gopalakrishnans uncle
*Chokkalinga Bhagavathar as Gopalakrishnans grandfather
*LIC Narasimhan
*Kalaranjini as Chinnavars wife
*Vijaya Chandrika as Gopalakrishnans mother
*Janaki as Gopalakrishnans aunt
*Radhabhai as Gopalakrishnans grandmother
 

==Soundtrack==

{{Infobox album |  
| Name        = Periya Idathu Mappillai
| Type        = soundtrack
| Artist      = Sirpy
| Cover       = 
| Released    = 1997
| Recorded    = 1997 Feature film soundtrack |
| Length      = 16:50
| Label       = 
| Producer    = Sirpy
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sirpy. The soundtrack, released in 1997, features 4 tracks with lyrics written by Kalidasan and Palani Bharathi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Mano || 4:05
|- 2 || Chik Chik || Mano, Swarnalatha || 4:37
|- 3 || Kadhalin Formula || Mano, K. S. Chithra || 4:38
|- 4 || Hariharan || 3:30
|}

==References==
 

 
 
 
 
 
 