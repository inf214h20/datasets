Epic (1984 film)
{{Infobox film name = Epic image = caption = director = Yoram Gross producer = Yoram Gross writer =  starring =John Huston Ross Higgins music =  cinematography = editing =  studio = Yoram Gross Studios distributor = released =1984 runtime =  country = Australia language = English budget = gross =
}}
Epic is a 1984 Australian animated feature by Yoram Gross,  who later called it "a rather Australian film - I cant say very successful, a little bit too much experimental film, too much abstract story." 

==Storyline==
During a massive flood, two children are rescued by a family of dingoes, which subsequently raises them as their own. When the children come of age, they must go out into the world and collect the "secrets of life", before becoming the new king and queen of the dingoes. 

==Voice cast==
*John Huston - Narrator
*Ross Higgins
*Robyn Moore
*Benita Collings Keith Scott

==References==
 

==External links==
*  at IMDB

 

 
 

 
 