Long Vacations of 36
{{Infobox Film 
| name = Long Vacations of 36
| image = Las largas vacaciones del 36.jpg
| caption = Spanish film poster
| director = Jaime Camino
| producer = José Frade
| writer = Jaime Camino Manuel Gutiérrez Aragón 
| narrator = 
| starring =  Concha Velasco José Sacristán Francisco Rabal Ángela Molina
| music = Xavier Montsalvatge
| cinematography = Fernando Arribas
| editing = Teresa Alcocer
| distributor = 
| released = October 28, 1976, Spain
| runtime = 102 minutes
| country = Spain
| language = Spanish
| budget = 
| preceded_by = 
| followed_by = 
}}
 Spanish drama film directed by Jaime Camino dealing with the effects of Spanish Civil War on a bourgeois family trapped by the conflict in a tourist village near Barcelona.

The film won  three awards at the  , Interfilm Award - Recommendation, and UNICRIT Award - Honorable Mention.   

The original ending (Francos cavalry entry in the village, intentionally showed as a blurred image) was cut by Spanish censorship of that time. The film ends with all the Republicans marching to exile, and the fascist couple waiting at home.

==Plot==
In the summer of 1936, the beginning of the Spanish Civil War, a bourgeois family with many children, spending their holiday near Barcelona, tries to remain neutral between the Republicans and the supporters of General Francisco Franco. But the war change the lives of all.

==Cast==
*Analía Gadé	 ... 	Virginia
*Ismael Merlo	... 	El Abuelo
*Ángela Molina	... 	Encarna
*Vicente Parra	... 	Paco
*Francisco Rabal	... 	El Maestro
*José Sacristán	... 	Jorge
*Charo Soriano	... 	Rosita
*Concha Velasco	... 	Mercedes
*José Vivó	... 	Alberto

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 