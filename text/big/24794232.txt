Yathumaagi
{{Infobox film
| name     = Yaathumagi
| image    = yaathumagi.jpg
| director = S. Balakumar
| producer = Sakthi Sangavi Mohana Sangavi
| starring = Sunaina Sachin Riaz Khan Aniruthan
| music    = James Vasanthan
| studio   = Chola Creations
| released =  
| language = Tamil
| country  = India
}}
Yathumaagi is a 2010 Indian Tamil-language romance film written and directed by S. Balakumar that stars newcomer Sachin and Sunaina in lead roles. The films score and soundtrack composed by James Vasanthan. The film, produced by Sakthi Sangavi and Mohana Sangavi under the Chola Creations banner, was released on 12 March 2010 to critical acclaim while becoming a successful box office venture.   

==Cast==
* Sachin as Anand
* Sunaina as Annalakshmi
* Riyaz Khan
* Aniruthan
* Azhagan Thamizhmani

==Plot==
Anand (Sachin) is an advertisement photographer. Quite typical to such genre of films, he comes across Annalakshmi (Sunaina), who is conservative in her looks. After a few initial encounters, the girl develops love for him.

But Anand has other plans. The mystery is soon unraveled and is made known that Anand hails from a family of doctors. When everyone thinks Anand will tie the knot to Annalakshmi, the twist occurs in the form of Anand accepting an arranged marriage. What transpires between the lead couple forms the rest of the story which ends in an interesting climax.

==Reception==
The film received critical acclaim from critics. Indiaglitz praised Sachins and Sunainas performance. They stated that Sachin was a "promising newcomer".  Behindwoods praised Sachin, claiming that he performed well and Sunaina was also praised for her performance and looks. 

==Soundtrack==
The films soundtrack is composed by James Vasanthan, which features 6 tracks. The music of the album is very well famous become the yougsters 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!!Notes
|-
| "Parthadum" || Vijay Yesudas, K. S. Chitra ||
|-
| "Yathumaagi" || Benny Dayal || 
|-
| "Pesum Minsaram" || Benny Dayal ||
|-
| "Thigatta Thigatta" || Deepa Miriam ||
|-
| "Yaaradhu Yaro Yaro" || Belli Raj, Srimathumitha ||
|-
| "Koothadichivada" || Prasad, Ramkumar, Vijay Uma ||
|}

==References==
 

==External links==
*  

 
 
 
 
 
 


 