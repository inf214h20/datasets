The Dark Stairway (1953 film)
 
 
{{Infobox film
| name           = The Dark Stairway
| image          = 
| caption        = 
| director       = Ken Hughes Stuart Levy
| writer         = Ken Hughes
| starring       = Russell Napier Vincent Ball
| music          =  narrator = Edgar Lustgarten
| cinematography =  J. M. Burgoyne-Johnson Ron Bicker
| editing        = Derek Holding
| studio         = Merton Park Studios
| distributor    = Anglo-Amalgamated
| released       = March 1954
| runtime        = 32 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Scotland Yard. 

It was also known as The Greek Street Murder. 
==Plot==
A blind man, George Benson, witnesses the murder of Harry Carpenter by Joe Lloyd. Benson finds himself accused of the murder. Inspector Jack Hammond finds the murder weapon and discovers Carpenter was murdered because he betrayed Lloyd to the police. Benson manages to identify Lloyd by his rin, voice and aftershave smell.
==Cast==
*Russell Napier as Inspector Harmer
*Vincent Ball as Sergeant Gifford
*George Manship George Benson
*Joe Lloyd as Edwin Richfield

==References==
 

==External links==
*  BFI
 

 
 
 
 
 
 

 
 