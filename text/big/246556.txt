The Morning After (1986 film)
 
{{Infobox film
| name           = The Morning After
| image          = Morning_after.jpg
| image_size     =
| caption        = original film poster
| writer         = James Hicks David Rayfiel (uncredited)
| starring       = Jane Fonda Jeff Bridges Raúl Juliá
| director       = Sidney Lumet
| producer       = Bruce Gilbert
| music         = Paul Chihara
| cinematography = Andrzej Bartkowiak
| editing        = Joel Goodman
| studio         = Lorimar Motion Pictures
| distributor    = 20th Century Fox (theatrical)
| released       =  
| runtime        = 103 minutes
| language       = English
| budget         = $13 million
| gross          = $25,147,055
}}
The Morning After is a 1986 mystery film directed by Sidney Lumet and starring Jane Fonda, Jeff Bridges and Raul Julia. It was nominated for the Academy Award for Best Actress (Jane Fonda). Kathy Bates has a very small role as a bystander.

==Plot== alcoholic actress, Alex (played by Fonda), wakes up on Thanksgiving Day next to a murdered man, a sleazy photographer. She feels sick and remembers nothing from the night before. She calls her friend Jackie about what happened the night before. (We find out later that he is her husband but they are separated.) He informs her that she missed out on getting what could have been her best gig in a while the night before because she was drunk and rude. She tells him about the body. He tells her to call the cops and she says that she is scared but he insists, telling her it will be alright. She retorts saying "ya wanna bet?" She leaves the apartment, and it is implied that she has not called the police. While trying to flee at the airport, Alex encounters an ex-policeman, Turner (Jeff Bridges), who believes in her innocence. Raúl Juliá plays Jackie, a hairdresser who wants to divorce her, as he has fallen in love with socialite Isabel Harding (Diane Salinger).

==Cast==
*Jane Fonda as Alex Sternbergen
*Jeff Bridges as Turner Kendall
*Raul Julia as Joaquin Manero ("Jackie")
*Diane Salinger as Isabel Harding
*Richard Foronjy as Sgt. Greenbaum
*Fran Bennett as Airline Clerk
*Michael Flanagan as Airline Supervisor
*Bruce Vilanch as Bartender Geoffrey Scott as Bobby Korshack
*James Haake as Frankie
*Kathleen Wilhoite as Red

==Reception==
The film holds a 71% "fresh" rating on Rotten Tomatoes. 

===Awards===
Jane Fonda was nominated for the Academy Award for Best Actress in a Leading Role.  

==References==
 

==External Links==
*  at The Internet Movie Database
*  at Rotten Tomatoes

 

 

 
 
 
 
 
 
 
 
 