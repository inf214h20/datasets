Arctic Flight
{{Infobox film
| name           =Arctic Flight
| image          =Arctic-flight-movie-poster-1952.jpg
| image size     =150px Theatrical poster
| director       =Lew Landers
| producer       =Lindsley Parsons
| based on       = 
| writer         =Robert Hill George Bricker
| narrator       = Wayne Morris Alan Hale Jr. Lola Albright
| music          =Edward J. Kay
| cinematography =Jack Russell
| editing        =Ace Herman Monogram Pictures Corp.
| distributor    = Monogram Pictures Corp.
| released       =   
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Wayne Morris, Soviet intrigue. 

==Plot== aircraft short of the Inuit village of Little Diomede, and transports Martha by dog sled, over the short distance remaining on the frozen Bering Strait. A romance between the two is kindled.
 Kenneth MacDonald) and local resident Miksook (Anthony Garson). She is replacing the teacher who had wandered too close to the International Date Line that separates the two islands and was shot and killed. Flying to Nome, Alaska|Nome, Mike learns he has another job, flying businessman John W. Wetherby (Alan Hale Jr.) on a polar bear hunt. Bad weather delays the hunt and Wetherby expresses an interest in visiting Little Diomede. A native girl Saranna (Carol Thurston) tells Mike that his friend Dave Karluck (Thomas Richards Sr.), has been mauled in a bear attack. Mike and Wetherby find the polar bear and Wetherby kills the animal, and proceeds to skin him. About to leave, Wetherbys wallet drops out and Mike sees that a pass to go to Soviet territory is inside the wallet. Knicked by a skinning knife wielded by Wetherby, the wounded pilot is flown back by the businessman to Little Diomede where Martha treats the wound. 

Mike confides in Martha that his client did not stab him by accident, and is not who he is claiming. Martha is afraid that Mike is delirious but finding Wetherbys identification card, leads to a confrontation where Mike, coming to her rescue, is knocked out. In his haste to head out over the ice to the Soviet base on Big Diomede, Wetherby loses a packet of papers, including microfilms of defense installations in the United States and his identification card. When he tries to enter the base without an entry card, he is shot and killed by the sentries. Martha and Mike realize that Wetherby was a spy and their efforts have stopped his plan to deliver military secrets to an enemy power.

==Cast==
   Wayne Morris as Mike Wein
* Alan Hale Jr. as John W. Wetherby  
* Lola Albright as Martha Raymond 
* Carol Thurston as Saranna Koonuk 
* Phil Tead as "Squid" Tucker
   
* Thomas Richards Sr. as Dave Karluck (credited as Tom Richards)
* Anthony Garson as  Miksook  Kenneth MacDonald as Father François 
* Paul Bryar as "Happy" Hogan
* Dale Van Sickel as Joe Dorgan
 

==Production==
  workhorse used in Arctic Flight.]] 
Principal photography for Arctic Flight took place from late February to early April 1952 at Little Diomede Island in Alaska and at KTTV Studios in Los Angeles. Writer Ewing Scott directed most of the Alaskan footage, but was replaced by Lew Landers after a flare-up of an old leg injury.  A Cessna 170B (N1470D) appeared as the bush plane the lead character flew. 

==Reception== Hollywood cinematographer Richard H. Kline considered Landers "... the most prolific of all directors", adept in many genres.  

==References==
Notes
 
Citations
 
Bibliography
 
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
* Weaver, Tom. A Sci-Fi Swarm and Horror Horde: Interviews with 62 Filmmakers. Jefferson, North Carolina: McFarland & Company, 2010. ISBN 978-0-78644-658-2.
 

==External links==
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 