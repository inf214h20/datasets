Let's Get a Divorce
{{infobox film
| title          = Lets Get a Divorce
| image          =
| imagesize      =
| caption        =
| director       = Charles Giblyn
| producer       = Adolph Zukor Jesse L. Lasky John Emerson (scenario) Anita Loos (scenario)
| starring       = Billie Burke
| music          =
| cinematography = Hal Young
| editing        =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       = April 28, 1918
| runtime        = 50 minutes
| country        = USA
| language       = Silent film (English intertitles)
}} John Emerson and Anita Loos. The film was produced by the Famous Players-Lasky company and distributed through Paramount Pictures.

The film is based on the popular stage play Divorcons by Victorien Sardou and Émile de Najac.   

==Cast==
*Billie Burke - Madame Cyprienne Marcey
*John Miltern - Henri de Prunelles
*Pinna Nesbit - Yvonne de Prunelles
*Armand Kaliz - Adhemar
*Rod La Rocque - Chauffeur
*Helen Tracy - Mother Superior
*John Merkyl - Calvignac (*billed Wilmuth Merkyl)
*Cesare Gravina - Head Waiter

==Preservation status==
This film is currently considered a lost film. 

==See also== Kiss Me Again (1925) That Uncertain Feeling (1941)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 