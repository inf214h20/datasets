The 81st Blow
 
{{Infobox film
| name           = The 81st Blow
| image          = 
| caption        = 
| director       = David Bergman  Jacques "Jacquo" Ehrlich Haim Gouri
| producer       = David Bergman Jacques Ehrlich Haim Gouri
| writer         = Haim Gouri
| starring       = 
| music          = 
| cinematography = 
| editing        = Jacques Ehrlich
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Israel 
| language       = Yiddish Hebrew
| budget         = 
}}

The 81st Blow (  and also known as The Eighty-First Blow) is a 1974 Israeli documentary film directed by Haim Gouri. The film covers the oppression of Jews under the Nazis and features rare historical footage of concentration camps. It was nominated for an Academy Award for Documentary Feature.    The title is derived from a comment by a witness at Adolf Eichmanns trial. According to his testimony, he was whipped 80 times by the Nazis, but was not believed by Israelis after the war; this final doubt of his own people was the "81st blow". The 81st Blow is the first film in the Israeli Holocaust Trilogy by Bergman, Ehrlich and Gouri.  It was followed by The Last Sea (1980) and Flames in the Ashes (1985).

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 
 