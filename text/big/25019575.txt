Tanging Yaman
 
 
{{Infobox film
| name           = Tanging Yaman
| image          = Tanging Yaman DVD cover.jpg
| caption        = DVD cover of the film
| director       = Laurice Guillen
| producer       = Star Cinema
| eproducer      = 
| aproducer      = 
| writer         =  Gloria Romero Hilda Koronel  Johnny Delgado Edu Manzano Dina Bonnevie  Joel Torre   Cherry Pie Picache    Marvin Agustin  Jericho Rosales  CJ Ramos  Shaina Magdayao  Dominic Ochoa  John Prats  Carol Banawa   Janette McBride
| music          = 
| cinematography = 
| editing        = 
| distributor    = Star Cinema
| released       = December 25, 2000
| run_time        = 112 minutes
| country        = Philippines
| awards         =  Filipino  Tagalog  English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2000 Filipino Filipino religious-family drama film produced by Star Cinema. Directed by Laurice Guillen, the film garnered several awards, especially at the 2000 Metro Manila Film Festival, including Best Picture, Best Actor, and Best Actress.

The movies title came from a liturgical composition by Manoling Francisco, SJ and sung by Carol Banawa.

The success of this religious-family drama film was followed ten years later by "Sa Yo Lamang", also directed by Laurice Guillen. The film is set to be restored by ABS-CBN Film Archive.

==Plot==
Separated by both physical and emotional distance, siblings Danny (Johnny Delgado), Art (Edu Manzano) and Grace (Dina Bonnevie) settle a land dispute whilst their mother Dolores "Loleng" Rosales (Gloria Romero) succumbs to debilitating disease. Old resentments begin to surface and spill over to the next generation as they cope with Lolengs sickness and the tract of land left to them by their deceased father.    

==Cast== Gloria Romero as Lola Loleng
* Hilda Koronel as Celine
* Dina Bonnevie as Grace
* Edu Manzano as Arturo
* Johnny Delgado as Danny
* Joel Torre as Francis
* Cherry Pie Picache as Nanette
* Marvin Agustin as Boyet
* Jericho Rosales as Rommel
* Maria Elaine Palomo as Lilibeth
* CJ Ramos as John-John
* Shaina Magdayao as Carina
* John Prats as Andrew
* Carol Banawa as Chona
* Dominic Ochoa as Joel
* Janette McBride as Madeleine
* Rolaine Kaye Surposa as Opera

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2000
| rowspan="9" align="left"| Metro Manila Film Festival   Best Picture
| align="center"| Tanging Yaman
|  
|- Best Actor
| align="center"| Johnny Delgado
|  
|- Best Actress Gloria Romero
|  
|- Best Director Laurice Guillen
|  
|- Best Screenplay
| align="center"| Laurice Guillen, Shaira Mella Salvador, and Raymond Lee
|  
|- Best Original Story
| align="center"| Laurice Guillen
|  
|- Best Cinematography Videlle Meily
|  
|- Best Musical Score
| align="center"| Nonong Buencamino
|  
|-
| align="left"| Gatpuno Antonio J. Villegas Cultural Awards
| align="center"| Tanging Yaman
|  
|}
==References==
 

==External links==
* 

 
 
 
 
 
 
 

 

 