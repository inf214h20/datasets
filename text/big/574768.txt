Roxanne (film)
{{Infobox film
| name = Roxanne
| image = Roxanne1987.jpg
| caption = Theatrical release poster
| director = Fred Schepisi
| writer = Steve Martin
| producer = Michael I. Rachmil Daniel Melnick
| starring = Steve Martin Daryl Hannah Rick Rossovich Shelley Duvall
| music = Bruce Smeaton Ian Baker
| editing = John Scott
| released =  
| runtime = 107 minutes
| distributor = Columbia Pictures
| awards =
| country = United States
| language = English
| gross =  $40,050,884 (domestic) {{cite web
| title = Roxanne
| work = Box Office Mojo
| publisher = Internet Movie Database
| url = http://www.boxofficemojo.com/movies/?id=roxanne.htm
| accessdate = October 4, 2010
}}
 
}} play Cyrano Cyrano de Bergerac, adapted by Steve Martin and starring Martin and Daryl Hannah.

== Plot ==
C.D. "Charlie" Bales (Steve Martin), the fire chief of a small American ski town in Washington (state)|Washington, is witty, acrobatic and skilled at many things, but he has a very large nose. He is attracted to newcomer Roxanne Kowalski (Daryl Hannah), an astronomer searching for a new comet, but she becomes infatuated with Chris (Rick Rossovich), a handsome but dim fireman who has just joined Baless unit. As in the play, Bales is touchy about his most prominent feature, which he cannot have surgically altered because of a dangerous allergy to anesthetics.

When Chris bungles his budding relationship with Roxanne, he asks Bales for help. Somewhat reluctantly, Bales writes romantic love letters for Chris to pass off as his own. They prove irresistible, and Roxanne invites Chris to spend the night. Chris, however, eventually becomes uncomfortable trying to meet Roxannes intellectual expectations of him. He eventually sends her a letter telling her that he has left town with another woman.

Baless friend Dixie (Shelley Duvall) then reveals to Roxanne who actually wrote the letters. When Bales arrives at Roxannes home in response to her call, she confronts him. Bales and Roxanne argue, she claiming that he was leading her on, while Bales says that she wants the perfect man who is both emotionally and physically beautiful. In the end, Bales and Roxanne forgive one another and Roxanne confesses her love for him. She says that flat-nosed people are too boring and bland, and that his nose gives him character.

== Cast ==
*Steve Martin as  Charlie Bales (C.D.)
*Daryl Hannah as Roxanne Kowalski
*Rick Rossovich as Chris McConnell
*Shelley Duvall as Dixie
*John Kapelos as Chuck
*Fred Willard as Mayor Deebs Max Alexander as Dean
*Michael J. Pollard as Andy
*Steve Mittleman as Ralston
*Damon Wayans as Jerry
*Matt Lattanzi as Trent
*Ritch Shydner and Kevin Nealon as two drunk men that Charlie meets on the street
*Brian George as Dr. Dave Schepsi, a plastic surgeon Maureen Murphy as Cindy, the cosmetics girl
*Heidi Sorenson as Trudy, the Mayors love interest
*Thom Curley as Jim, the darts player C.D. takes to task in the bar after the "Big Nose" comment
*Shandra Beri as Sandy, the bartender
*Jean Sincere as Nina

== Production ==
Roxanne was filmed in the summer of 1986 in the town of Nelson, British Columbia. Steve Martin chose to use the local fire hall on Ward Street as a primary set.

== Reception ==

=== Critical response ===
Roxanne received mostly positive reviews. It currently holds an 88% approval rating on Rotten Tomatoes, with the consensus being: "Though its sweetness borders on sappiness, Roxanne is an unabashedly romantic comedy that remains one of Steve Martins funniest". 

Roger Ebert hailed the film as a "gentle, whimsical comedy", giving it a 3 and half stars of four, also stating: "What makes "Roxanne" so wonderful is not this fairly straightforward comedy, however, but the way the movie creates a certain ineffable spirit". 

It is number 71# on Bravo (US TV channel)|Bravos "100 Funniest Movies".

=== Accolades ===
It has also won and has been nominated for a number of awards, including: 

* Golden Globe Award: Nominated: Best Performance by an Actor in a Motion Picture - Comedy/Musical: Steve Martin The Witches of Eastwick (1987).
* NSFC Award: Won: Best Actor: Steve Martin WGA Award (Screen): Won: Best Screenplay Based on Material from Another Medium: Steve Martin

== References to the play == Cyrano de Bergerac wrote of a journey to the Moon and to the Sun, and Roxanne alludes to this in a scene where C.D. jokes about UFOs and Extraterrestrial life in fiction|aliens. Additionally, that scene mirrors one in the play where Cyrano pretends to fall out of a tree to distract another wooer of Roxanne.

* The names of all three main characters are based on their counterparts in the play. C.D. Bales has the same initials as Cyrano de Bergerac, Roxannes name is a slight alteration of Roxane, and Chris is a diminutive form of Christian.

* The "20 Nose Insults" speech in the film mirrors a similar speech in the play.

* C.D.s position in the firefighters is similar to Cyranos leadership of the Gascon Cadets.

== See also ==
 
*List of firefighting films

== References ==
 

== External links ==
 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 