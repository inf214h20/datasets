Anandhapurathu Veedu
 
{{Infobox film
| name           =  Veedu
| image          = 
| caption        =  Naga
| producer       = S. Shankar Aryan Megh Varn Pant
| music          = Ramesh Krishna
| cinematography = Arun Mani Palani
| editing        = Kishore Te.
| studio         = S Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Indian Tamil Tamil supernatural Chidambara Rahasiyam. Nandha and Chaya Singh in lead roles along with child artist Aryan, making his debut. Written by Naga, Sharath Haridasan and Indra Soundar Rajan and produced by director S. Shankars S Pictures, Anandhapurathu Veedu was released on 9 July 2010.  The film was commercial success at the box office.

== Cast == Nandha as Bala
* Chaya Singh as Revathy Bala Master Aryan as Anand Bala
* Krishna as Jeeva
* Kalairani as Mayilamma
* Lavanya as Radhika
* Megh Varn Pant as Sashikanth
* Ganesh Babu as Rathinam
* Samson T. Willson as Natwarlal

==Plot==
15 years after an accident in which Balas parents both died, Bala returns with wife, Revathi, and son, Anand, to his hometown. They decide to stay a couple of nights in the massive house that he grew up in. As he spends a few days in the house and recalls his memories, Anand- who has a disability to speak- sees movement around the house. But he cannot tell anyone what he witnesses. Soon Revathi who suffers from claustrophobia, sees that the house is haunted and so tells Bala that they have to go back to Chennai. Anand enjoys being in the presence of the ghosts and doesnt find any problem. Problems come up between the couple that they had never thought of when they had love marriage. Soon Revathi discovers that they did not come on holiday but in fact, came to escape some rowdies that Bala owes money to. This is when Balas classmate and business partner, Jeeva, arrives to stay with them. His company had borrowed 40 lakhs from a rowdy and over the months it had increased to 50 lakhs but he is unable to pay back any money as his agent, who was supposed to give him 2 crores, runs away and is nowhere to be found. With the help of ghosts, Bala and his family find a way to escape from being kept under house arrest.Balas partner who has been jealous of him had the money the whole time to pay the rowdies.

==References==
 


==External links==
*  

 

 
 
 
 
 


 