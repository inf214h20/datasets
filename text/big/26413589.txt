Bab'Aziz
{{Infobox film
| director = Nacer Khemir
| writer = Tonino Guerra Nacer Khemir
| starring = Parviz Shahinkhou Maryam Hamid Hossein Panahi Nessim Khaloul Mohamed Graïaa Maryam Mohaid Golshifteh Farahani
| music = Armand Amar Abacus Consult Bulgarian Symphony Orchestra Naïve Records|Naïve SIF309 Film & Music Productions
| studio = Behnegar
| distributor= Bavaria Film International Typecast Releasing  Trigon-Film
| released =  
| runtime = 98 minutes
| country = Iran Tunisia Persian
| gross = $263,447
}}
 Tunisian writer and director Nacer Khemir. It stars Parviz Shahinkhou, Maryam Hamid, Hossein Panahi, Nessim Khaloul, Mohamed Graïaa, Maryam Mohaid and Golshifteh Farahani. It was filmed in Iran and Tunisia.

==Summary and themes==
 Sufi gathering — encounter several strangers who relate the stories of their own mysterious and spiritual quests.
 Wanderers of the desert) and 1991   (The doves lost necklace).    The three films share structural elements and themes drawn from Islamic mysticism and classical Arab culture, as well as an isolated desert setting. Khemir has said: 
 
"The desert… evokes the Arabic language, which bears the memory of its origins. In every Arabic word, there is a bit of flowing sand. It is also one of the main sources of Arabic love poetry. In all three of my movies… the desert is a character in itself."
 
BabAziz is particularly concerned with Sufi themes. Khemir has stated that he wished to show, in the film, "an open, tolerant and friendly Islamic culture, full of love and wisdom . . . an Islam that is different from the one depicted by the media in the aftermath of 9/11",  and that the unusual structure of the film was a deliberate attempt to imitate the structure of Sufi visions and dances, aimed at allowing the spectator "forget about his own ego and to put it aside in order to open up to the reality of the world". 

==Cast==

* Parviz Shahinkhou as BabAziz
* Maryam Hamid as Ishtar
* Hossein Panahi as red dervish
* Nessim Khaloul as Zaid
* Mohamed Graïaa as Osman
* Golshifteh Farahani as Nour
* Soren Mehrabiar as dervish

==Reception==

===Box office===
BabAziz has grossed $263,447 worldwide.   

===Critical response===

BabAziz received mixed reviews from critics. Review aggregator Rotten Tomatoes reports that 58% of 24 critics have given the film a positive review.  Boston Globe critic Michael Hardy found fault with Khemirs "well-meaning attempt to correct Western misconceptions of Islam", complaining that the film "is set in the present, but resolutely ignores current events in favor of pervasive nostalgia for the glorious past".  However, Matt Zoller Seitz of the New York Times praised it as "a structurally audacious fairy tale that imparts moral lessons and shows how narratives reflect and shape life". 

==References==

 
*  

==External links==
*  
*  
 

 
 
 
 
 
 