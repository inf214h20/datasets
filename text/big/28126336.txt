Världens bästa Karlsson
{{Infobox film
| name           = Världens bästa Karlsson
| image          = 
| alt            = 
| caption        = 
| director       = Olle Hellbom
| producer       = Olle Nordemar
| writer         = Astrid Lindgren
| starring       = Lars Söderdahl Mats Wikström Georg Riedel
| cinematography = 
| editing        = 
| studio         = 
| distributor    = AB Svensk Filmindustri
| released       =  
| runtime        = 99 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Världens bästa Karlsson is a 1974 Swedish family film directed by Olle Hellbom. It is based on a book about Karlsson-on-the-Roof by Astrid Lindgren.

==Cast==
*Lars Söderdahl as Svante Lillebror Svantesson
*Mats Wikström as Karlsson
*Jan Nygren as Karlsson (voice)
*Catrin Westerlund as Lillebrors Mother
*Stig Ossian Ericson as Lillebrors Father
*Staffan Hallerstam as Bosse Svantesson
*Britt Marie Näsholm as Bettan Svantesson
*Nils Lagergren as Krister
*Maria Selander as Gunilla
*Pär Kjellin as Pelle Janne Loffe Carlsson as Fille
*Gösta Wälivaara as Rulle
*Catti Edfeldt as Woman with flowers

== External links ==
* 
* 

 

 
 
 
 
 
 
 

 