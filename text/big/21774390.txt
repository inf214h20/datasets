Three Sailors and a Girl
{{Infobox film
| name           = Three Sailors and a Girl
| image          = Three Sailors and a Girl poster.jpg
| image_size     =
| caption        =
| director       = Roy Del Ruth
| producer       = Sammy Cahn
| writer         = Devery Freeman Roland Kibbee George S. Kaufman (play)
| narrator       =
| starring       = Jane Powell Gordon MacRae Gene Nelson
| music          =
| cinematography = Carl E. Guthrie
| editing        =
| studio         = Warner Bros.
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Three Sailors and a Girl is a 1953 musical film made by Warner Bros.. It was directed by Roy Del Ruth, and written by Devery Freeman and Roland Kibbee, based on the George S. Kaufman play The Butter and Egg Man. Ray Heindorf is the Musical Director, orchestrations by Gus Levene, and vocal arrangements by Norman Luboff. Choreography by LeRoy Prinz. The Soundtrack features original songs with music composed by Sammy Fain and lyrics by Sammy Cahn.

Soundtrack recording: As was the practice at the time, the soundtrack album was a studio recording  .  The Capitol Records album was released early in 1954, and featured eight of the songs from the Fein/Cahn songwriting team. Jane Powell and Gordon MacRae are the featured vocalists. George Greeley conducted the Orchestra and Chorus. The album was re-issued and released on CD in 2006: However it contained 12 more songs by Gordon MacRae. 

==Plot==
While their submarine is docked in New York City, three sailors on liberty invest the money theyve earned at sea into a broadway musical and its up and coming star.

==Cast==
*Jane Powell as  Penny Weston 
*Gordon MacRae as  "Choirboy" Jones 
*Gene Nelson as  Twitch 
*Sam Levene as  Joe Woods 
*Jack E. Leonard as  Porky 
* George Givot as  Emilio Rossi 
*Veda Ann Borg as  Faye Foss 
* Archer MacDonald as  Webster 
* Raymond Greenleaf as  Morrow 
* Henry Slate as  Hank the Sailor

Burt Lancaster made an uncredited cameo appearance at the end, playing a Marine who hesitantly asks about taking over the starring role in the musical after Jones has to return to the Navy. Joe Woods brushes him off. When a woman asks why he was so brusque, Joe tells her that the Marine looked too much like Burt Lancaster. Merv Griffin also appears (uncredited) as one of the sailors.

==Soundtrack songs==
* "Face to Face" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "Youre But Oh Right" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "There Must Be a Reason" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "When Its Love" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "I Get Butterflies" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "Kiss Me or Ill Scream" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "The Lately Song" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "Show Me a Happy Woman (and Ill Show you a Miserable Man)" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "Its Going to Be a Big Hit" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "Home Is Where the Heart Is" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "My Heart Is a Singing Heart" - Music by Sammy Fain; Lyrics by Sammy Cahn
* "Embraceable You" - Music by George Gershwin; Lyrics by Ira Gershwin
* "The Japanese Sandman" - Words and Music by Richard a Whiting (background in nightclub scene)
* "The Marines Hymn" - Music by Jacques Offenbach

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 