House Hunting
{{Infobox film
| name           = House Hunting
| image          = House Hunting.jpg
| caption        = 
| director       = Eric Hurt
| producer       = {{plainlist|
* Erica Arvold
* Pat Cassidy
}}
| writer         = Eric Hurt
| starring       = {{plainlist|
* Marc Singer
* Art LaFleur
* Hayley DuMond
}}
| music          = {{plainlist|
* Jaysen Lewis
* Cody James
}}
| cinematography = {{plainlist|
* Todd Free
* Eric Hurt
}}
| editing        = {{plainlist|
* Jay Thomas
* Eric Hurt
}}
| studio         = Pillage and Plunder Pictures
| distributor    = Paramount Pictures
| released       =   }}
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} thriller film VOD release through Phase 4 Films on March 5, 2013.  The film stars Marc Singer and Art LaFleur and follows two families that are trapped within a deserted farmhouse.

==Plot==
Charlie Hays, his daughter Emmy, and his second wife Susan follow a lead on a foreclosed house Charlie wants to buy. Its a seemingly perfect and beautiful home on 70 acres of private land. He also wants to use this as an opportunity for Emmy and Susan to get along, as neither likes one another. Meanwhile, another family – Don Thomson, his wife Leslie, and their live-in son Jason, who has suffered a broken leg in a car accident – meets a man in a red hat who gives Don an advertisement for the same house the Hayses are interested in. Finding it too good to be true, they too make the trip out to the house. Once both families arrive, they find the house abandoned. Charlie and his family go to the road to retrieve the realtors phone number and, on the way back, nearly strike a distraught girl named Hanna running across the road. She is severely traumatized, and her tongue has been cut out. The families decide to take her to the hospital, but to their shock find themselves coming back to the house again and again. Don refuses to give up, but by nightfall, his car runs out of gas. They decide to stay in the house, despite Hannas pleading not to enter.

Once inside, they find firewood and cans of stew that account for each one of them: seven cans for seven people. Despite the arguments that ensue between Leslie and Susan, the families decide to stay and wait for help. They remain for one month, while recordings on the house tell them that only one family will claim the house. Cabin fever and close quarters have everyone on edge until Susan spurns a cheerful Leslie for being too happy. As the tension mounts, the house inexplicably begins providing only six cans. Reminded of her daughter Lizzy, who had previously died, Leslie falls into a deep depression and kills herself. After this, Susan learns that the house had been foreclosed on by Charlie himself, and Emmy begins seeing visions of a man who had killed his son. Soon after, Charlie and Emmy begin seeing visions of her mother, appearing before them with her throat slashed. Charlie confides in Emmy that her mother had left them long ago, but Susan seems to know more. Following clues by the mute girl Hanna and a puzzle, Emmy nearly makes it back to the road by walking backwards, but she is interrupted by Jason, who tries to rape her. Hanna strikes him with a log, and he impales himself on a branch; Don, who has slowly begun to fall into madness, rescues him, but Jason begins to suffer shock from blood loss.

After being terrified by a vision, Jason admits that he killed a woman, later revealed to be an occupant of the house, in a hit and run accident nearby. Don leaves the Hayses bound and attempts to leave the property on his own. When he returns that night, he admits he found the road and starts to free them, but Susan, fearing him, attacks and kills him with an axe. Afterward, the house stops providing food for them, and Charlie becomes erratic. At the house, the ghost of the woman Jason killed attacks him and chokes him to death. Charlie snaps after a vision of himself lies to him about how Susan told Emmy about his responsibility for her mothers murder. After suspecting Susan of stealing their dwindling food supplies, he attacks and kills her. He chases Emmy and Hanna, saying that he believes that murdering Hanna will set them free. Charlie kills Hanna, but he is in turn killed by Emmy. Running to the road, Emmy comes across another family. They bring her into the car and drive back to the house. Emmy panics when she sees the family chase after the ghosts that had haunted her own family, but she is pulled into the car and her tongue is cut out, which starts the cycle again.

==Cast==
*Marc Singer as Charlie Hays
*Art LaFleur as Don Thomson
* Hayley DuMond as Susan Hays
* Janey Gioiosa as Emmy Hays
* Paul McGill as Jason Thomson
* Rebekah Kennedy as Hanna
* Victoria Vance as Leslie Thomson
* Jon Cobb as the Realtor
*Emma Rayne Lyle as Lizzy Thomson

==Production==
Filming took place in Charlottesville, Virginia over a 22 day period.  Producer Erica Arvold became involved with the production and casting of the film after reading the first 30 pages of Hurts script for House Hunting.    Actress Janey Gioiosa was chosen to portray Emmy Hays, as she had previously performed in one of Hurts earlier films and had impressed Hurt.  Gioiosa sustained an injury during the filming of the movie, as she had burst a blood vessel in her eye during a scene that required she scream. 

==Reception==
Aint It Cool News named the movie one of their "best horror films on AICN HORROR since last Halloween", as they felt that it was "original and well made".  Starburst (magazine)|Starburst gave the movie 7 out of 10 stars and remarked that while the movie was "definitely not perfect" and would not appeal to all tastes, it was also "well made, compelling and interesting."  Matthew Lee of Twitch Film wrote, "You have to forgive a lot to want to buy into The Wrong House, but its still one of the best deals of 2012."  In January 2013, Lee marked the film as one of his favorite films of 2012.  In contrast, HorrorNews.net was more critical in their review and gave it a C+, praising the acting and directing while stating that the films surrealism detracted from their viewing experience. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 