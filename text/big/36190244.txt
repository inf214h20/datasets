Mac & Devin Go to High School
 
{{Infobox film
| name = Mac & Devin Go to High School
| image = Mac & Devin Go To High School.jpg
| alt =
| caption = DVD/Blu-Ray cover
| director = Dylan C. Brown
| producer = Dylan C. Brown Lucy Brown 
| screenplay = Herschel Faber Jamieson Stern
| story  = Jarrett Golding
| starring = Snoop Dogg Wiz Khalifa Mike Epps Teairra Marí Andy Milonakis Luenell
| music = Snoop Dogg Wiz Khalifa
| cinematography = Luis Panch Perez
| editing =
| studio = Yard Entertainment Doggystyle Records
| distributor = Anchor Bay Films
| released =  
| runtime = 75 minutes
| country = United States
| language = English
| budget =
| gross = $1,616,650   
}}
 American direct-to-DVD stoner film directed by Dylan Brown. The film stars rappers Snoop Dogg and Wiz Khalifa in the title roles, along with Mike Epps, Teairra Mari, Andy Milonakis, Luenell in supporting roles and the voice of Mystikal in a guest appearance. The story follows two high school students, geeky Devin, and badman Mac who is a stoner. He befriends Devin, and introduces him to cannabis. The high schools name "N.Hale" is based on Nate Doggs real name Nathaniel Hale and is also a play on the word "inhale".

The film was released on DVD and Blu-ray Disc|Blu-ray on July 3, 2012. It marks the film debut of American rapper Wiz Khalifa. The soundtrack for the film was released on December 13, 2011, worldwide in stores.

==Cast==
* Snoop Dogg as Mac Johnson
* Wiz Khalifa as Devin Overstreet
* Mike Epps as Mr. Armstrong
* Teairra Mari as Ms. Huck
* Luenell as Principal Cummings
* Paul Iacono as Mahatma Chang Greenberg
* Andy Milonakis as Knees Down
* Mystikal as Slow Burn
* Far East Movement as Detention Students
* Samantha Cole as Jasmine Bishop Don Magic Juan as Student Service Guy
* Winston Francis as Massage Parlor Bouncer
* Derek D as Assistant Principal Skinfloot
* Teni Panosian as Ashley
* Kendré Berry as Nerdy Chemistry Student
* Alicia Monet as Jubilance
* Affion Crockett as Captain Kush
* Eunice Kiss as Mamasan
* Andray Johnson as Bail Officer
* Roz Wilson as Study Center Matron
* Carla Howe as Masseuce
* Jamieson Stern as Police Sergeant
* Melissa Howe as Tattoo Parlor Receptionist
* Kelly Pantaleoni as Jenny Billings
* Shvona Lavette Chung as M.I.L.F.
* Cordelle Braudus as M.I.L.F.s Son
* Jennifer Andrade as Munchie Machine Girl
* Young Pilot as A Freshman
* Raul Delante as Tattoo Artist
* Nasia Aissaoui as Foreign Exchange Student
* Dennis George Brown Jr. as Teacher

===Students=== YG (Smoker #2)
* Tyga
* Julian (The Rangers)
* Langston (The Rangers)
* Day Day (The Rangers)

===Hemptathalon=== Ty Dolla $ign as Smoker 1 YG as Smoker 2
* Kendré Berry as Smoker 3
* Tiffany Hughes as Tooted and Pooted Girl

==Release==

===Soundtrack=== Mac & Devin Go to High School by Snoop Dogg and Wiz Khalifa. The albums song "Young, Wild & Free" featuring Bruno Mars turned out to be a huge success worldwide, and was the most popular song from the whole soundtrack.  In its first week, the track sold 159,000 digital copies,  debuting at number ten on the US Billboard Hot 100|Billboard Hot 100, and forty four on the Canadian Hot 100. Therefore, Snoop Dogg revealed in an interview that the soundtracks success has given him the inspiration of coming up with a movie based on the song Young, Wild & Free. Soon enough, in March 2012, it was announced that Snoop Dogg and Wiz Khalifa would star in the spin-off Mac & Devin Go to High School: The Movie. Production and filming began straight after the announcement of the film.

===Announcement===
Snoop Dogg announced plans for the release of a film, and soundtrack alongside with Wiz Khalifa in January 2011, with the release of the song "Dat Good", originally intended to be the soundtracks lead single.  Wiz Khalifa spoke on the soundtrack saying "Its a real big deal because nobodys done it like that as far as a veteran in the game, an OG, a pioneer and then the newest, youngest, most exciting dude in rap coming through, and really just giving people a complete project," Wiz said. "Im a fan of it, separate myself from making it,   a huge fan of it. Cant wait." As far as the musical vibe of the soundtrack, Snoop described it. "Its something to relax you and get you through the day; its some real good music. The music is quality, I dont even have no title for it, as far as what kinda music is it, its centric, its ...," Snoop said searching for the right words before his partner-in-rhyme lent a hand. 

===DVD release===
The film was set for a Direct-to-video|straight-to-DVD release on April 20, 2012, however was then pushed back to July 3, 2012. The film has been seen by critics, and received mixed response from most critics who have already reviewed it.  As of August 2012, the DVD has sold 54,641 units in the United States. 

==Location controversy== Manhattan Beach, California, after the Manhattan Beach Unified School District granted The Yard Entertainment a facilities use permit.    Two days of filming were finished over the weekend of May 7, 2011, but production was halted when it was reported that individuals (some involved with the film, some not) were smoking marijuana on school campus.  A student present at the shoot claimed to have seen "their entourage smoking blunts."  Theft of school classrooms was also reported by teachers once classes began the next week, and The Yard reportedly reimbursed them for this. 

The controversy was quelled quickly, as the school district reportedly revoked its facilities use permit.  The school board claimed at the time to have returned all revenue received for the use of its school and demanded that all footage be thrown out, but as the footage was eventually used an additional, unpublished agreement must have been reached.

==Critical response==
Mac & Devin Go to High School was panned by critics. JP DelaCuesta of AllHipHop gave the film a 3/10, saying "There’s no other way to say this except Mac and Devin Go to High School is bad – plain and simple. The on-screen collaboration between these two Hip-Hop heavyweights is a joke, and for their sakes, hopefully a joke that they and everyone involved with Mac and Devin were in on. At the end of the day, the only thing that Mac and Devin Go to High School proves is that we need How High 2, and we need it bad!"  Nathan Rabin of The A.V. Club gave the film a negative review, saying "The protégé completes his evolution when he uses his high-school valedictorian speech to perform “Young, Wild & Free,” the hit single from Mac & Devin Go To High School. “Young, Wild & Free” is everything Mac & Devin Go To High School should be but isn’t: fun, light, goofy, entertaining, and young. In moments like this, the movie possesses a strange, disarming innocence, but it forces audiences to endure a punishing gauntlet of misogyny and non-starting comedy to get to that middling moment of moderate enjoyment." 

==References==
 

==External links==
*   at Internet Movie Database

 
 

 
 
 
 