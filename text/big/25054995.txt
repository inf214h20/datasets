FBI Code 98
{{Infobox film
| name           = FBI Code 98
| caption        =
| image	         = FBI Code 98 FilmPoster.jpeg
| director       = Leslie H. Martinson
| producer       = Stanley Niss
| writer         = Stanley Niss Jack Kelly William Reynolds Ray Danton Merry Anders Andrew Duggan Philip Carey Peggy McCay Howard Jackson
| narrator       = William Woodson Robert Hoffman
| editing        = Leo H. Shreve
| art director   = William L. Campbell
| distributor    = Warner Bros.
| released       =  
| runtime        = 104 min.
| country        = United States English
}} Jack Kelly, William Reynolds.

==Plot==

The president of an electronics company, Alan Nichols (Andrew Duggan), and his two vice presidents, Robert Cannon (Jack Kelly) and Fred Vitale (Ray Danton), are required at Cape Canaveral to oversee the test launching of a missile which their company developed. But before they are able to board the plane to take them there one of their suitcases is switched for one containing a bomb. Cannon opens his luggage when the men are in mid air and discovers the bomb, and his colleague Vitale manages to disarm it. The FBI is called in to determine whether this is a case of attempting to murder Cannon, whose suitcase contained the bomb, or an attempt to sabotage the air plane.

The investigation soon proves that electronics project engineer Petersen made and planted the bomb in the suitcase. Petersens motivation for doing this is that his son was fired by CEO Nichols, and when failing to blow up the plane he instead tries to blow up Nichols yacht, with the wife and her lover on it. The task for the FBI is to stop this endeavour. 

==Cast== Jack Kelly
:Fred Vitale - Ray Danton
:Alan W. Nichols - Andrew Duggan
:Inspector Leroy Gifford - Philip Carey William Reynolds
:Deborah Cannon - Peggy McCay
:Marian Nichols - Kathleen Crowley
:Grace McLean - Merry Anders
:Walter Macklin - Jack Cassidy Vaughn Taylor
:Lloyd Kinsel - Eddie Ryder
:Special Agent in Charge Gibson White - Ken Lynch Charles Cooper
:Special Agent Philip Vaccaro - Paul Comi Robert Hogan
:Anita Davidson - Laura Shelton
:Carl Rush - Robert Ridgely Francis De Sales
:Special Agent Alan Woodward - Bill Quinn
:Special Agent Vernon Lockhart - Ross Elliott
:Narrator - William Woodson

==Production==
The Federal Bureau of Investigation cooperated in the filming of FBI Code 98,    with sequences filmed in Washington, D.C. and Quantico, Virginia.

The working title of the film was Headquarters F.B.I. with screenwriter and producer Stanley Niss having a novelization of the screenplay published under that title.  Niss, a former police reporter,  had written episodes for the radio shows Gangbusters and Counterspy (radio series)|Counterspy and episodes of several Warner Bros. Television shows. Niss also visited the set to smooth difficulties between Ray Danton and director Leslie H. Martinson. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 