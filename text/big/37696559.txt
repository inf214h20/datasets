Opera House (1961 film)
 

{{Infobox film
| name = Opera House
| image = 
| image_size = 
| caption = 
| director = P.L. Santoshi
| producer = A.A. Nadiadwala 
| writer = 
| narrator =  Ajit B. Saroja Devi K.N. Singh Lalita Pawar Chitragupta
| cinematography = 
| editing = Dharamvir
| distributor = 
| released = 1961
| runtime = 
| country = India
| language = Hindi
| budget = 
}}

Opera House is a 1961 Hindi film produced by A.A. Nadiadwala and directed by P.L. Santoshi. It is a murder mystery film. The film stars Ajit Khan|Ajit, B. Saroja Devi, K.N. Singh and Lalita Pawar among others.

==Plot==

Saroj (B. Saroja Devi) lives a poor lifestyle in Nagpur along with her widowed mom, Leela Sharma (Lalita Pawar) and a younger sister, Nanhi. She gets employed as a singer/dancer in Bombay and relocates there. Once there, she meets with Ajit Rai (Ajit), and the couple fall in love. Circumstances compel her to re-locate to Nagpur and seek employment with another dance/drama company run by Chunilal. Ajit decides to follow her to Nagpur, and visits her mom and sister. It is from here he locates the dance/drama company and does meet Saroj - only to find out that she has changed her name to Mary DSouza. Ajit will eventually discover that Saroj had witnessed the murder of Chunilal and the killer(s) are now out to silence her - and whoever else dares to come in their way...

==Cast==

{|class="wikitable"

|-

!Character!!Actor

|-
 Ajit Rai||Ajit Ajit

|-
 Saroj Sharma/Mary DSouza||B. Saroja Devi

|-

|Daniel||K.N. Singh

|-
 Leela Sharma||Lalita Pawar

|-
 Bela Bose

|-
 Maruti Rao

|-
 Mumtaz Begum Mumtaz Begum 

|}

==Soundtrack==
 Chitragupta and Lyricist is Majrooh Sultanpuri.

{|class="wikitable"

|-

!#!!Title!!Singer

|-
 Mukesh (singer)|Mukesh, Lata Mangeshkar

|-
 Lata Mangeshkar

|}

==External links==
*  

 
 
 
 

 