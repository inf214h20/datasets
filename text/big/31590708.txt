Ki Jadu Korila
{{Infobox Film
| name           = Ki Jadu Korila
| image          = Ki Jadu Korila.jpg
| image_size     = 200px
| caption        = VCD Cover
| director       = Chandan Chowdhury
| producer       = Anowar Hossain Mintu
| writer         = Chandan Chowdhury Riaz Popy Mir Sabbir Ratna Sagorika Humayun Faridi Dolly Johur Rehana Jolly
| music          = Alam Khan 
| cinematography = Mahfuzur Rahman Khan
| editing        = Sahidul Haque
| distributor    = Sadia Hossain Kothachitra
| released       =  
| runtime        = 143 minutes
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
| imdb_id        = 
}}
Ki Jadu Korila also ( ) is a Bangladeshi Bengali language film. The film directed by Chandan Chowdhury, he was worked as assistant director with Bangladeshi famous film maker Abdullah Al Mamun. This was Chandan Chowdhury’s film direction debut. Release was 2008 in Eid ul-Fitr. 

==Cast==
{|
|- bgcolor="#CCCCCC"
! Actor/Actress !! Roles
|- Riaz || Sagar
|-
| Popy || Jhinuk
|-
| Mir Sabbir || Akash
|-
| Ratna || Dana
|-
| Sagorika || Parul
|-
| Humayun Faridi || Kamal Chairman
|-
| Dolly Johur || Akash’s Mother 
|-
| Rehana Jolly  || Sagar’s Mother
|-
| Kabila  || Sagar’s Maternal Uncle
|-
| Sirin Alam  || Jhinuk’s Aunt
|-
|}

==Crew==
* Director: Chandan Chowdhury
* Producer: Anowar Hossain Mintu
* Story: Chandan Chowdhury
* Music: Alam Khan
* Cinematography: Mahfuzur Rahman Khan
* Editing: Sahidul Haque
* Distributor: Sadia Hossain Kothachitra

==Music==
{{Infobox album
| Name = Ki Jadu Korila
| Type = Album
| Artist = Alam Khan
| Cover = 
| Background = Gainsboro |
| Released = 2008 (Bangladesh)
| Recorded = 2008
| Genre = Soundtrack/Filmi
| Length =
| Label =
| Producer =Eglle Music
| Reviews =
| Last album = 
| This album = Ki Jadu Korila (2008)
| Next album = 
|}}

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track’s !! Title’s !! Singer’s !! Performer’s!! Note’s
|- 1
|Prem Koro Mon Khati Manush Chine Andrew Kishore Riaz (Actor)|Riaz
|
|- 2
|Chokkhu Duita Kajol Kalo Andrew Kishore Riaz (Actor)|Riaz and Popy
|
|- 3
|Ki Jadu Korila Piriti Shikhaia Sabina Yasmin and Andrew Kishore Riaz (Actor)|Riaz and Popy
|- 4
|Tomar Sathey Bhab Koritey Runa Laila   Riaz (Actor)|Riaz and Sagorika
|
|- 5
|Tomar Akash Bukey Mon Pakhi Runa Laila and S I Tutul
| Mir Sabbir and Ratna
|
|- 6
|Januk Januk Sakoley Dekhuk Sabina Yasmin and S I Tutul
| Mir Sabbir and Popy
|
|-
 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 