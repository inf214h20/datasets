Nightmare (1964 film)
 
 
{{Infobox film
| name           = Nightmare
| image          = nightmare 1964.jpg
| image_size     =
| caption        = Film poster for Nightmare
| director       = Freddie Francis
| producer       = Jimmy Sangster
| writer         = Jimmy Sangster
| narrator       =
| starring       = David Knight Moira Redmond Jennie Linden
| music          = Don Banks
| cinematography = John Wilcox
| editing        = James Needs
| studio         = Hammer Films Rank (U.K.) Universal (U.S.)
| released       =  
| runtime        = 83 mins
| country        = United Kingdom English
| budget         =
| preceded_by    =
| followed_by    =
}} 1964 horror/suspense film from Hammer Films. The film was directed by Freddie Francis and written by Hammer Films regular Jimmy Sangster. The British Film Institute has the only 35mm print in the UK.

==Plot==
The films protagonist, Janet, is a young student attending boarding school.  After a number of nightmares concerning her mother, whom she saw kill her father when she was young, the girl is sent home to her guardian, Attorney Henry Baxter. At home, she is assigned a nurse. Janet begins having more nightmares this time concerning an unknown woman with a scar and a birthday cake. The dreams get worse and worse. Finally, her guardian brings home his wife, whom Janet has never met. Janet is introduced to the woman at her birthday celebration. The cake and woman from her dreams with the scar appearing at once is enough to make Janet snap.  She kills the woman by stabbing her - the same way her mother killed her father.  Janet is committed. Meanwhile, her guardian Henry and the nurse, who was disguised to look like the woman with a scar to drive Janet mad, celebrate the loss of Janet. However, the two do not go unpunished.

== Cast ==
*David Knight as Henry Baxter
*Moira Redmond as Grace Maddox
*Jennie Linden as Janet
*Brenda Bruce as Mary Lewis
*George A. Cooper as John
*Clytie Jessop as Woman in White
*Irene Richmond as Mrs. Gibbs  John Welsh as Doctor
*Timothy Bateson as Barman

Jennie Linden was an 11th hour casting choice replacing Julie Christie who dropped out to do the film Billy Liar (film)|Billy Liar.
This was the final film performance of  American actor David Knight who subsequently focused on theatre work.

== Critical reception ==
 AllMovie called the film an "effective little chiller that packs a surprising punch for a film of its age." 

== References ==

 

== External links ==

*  

 
 
 

 
 
 
 
 
 
 
 
 