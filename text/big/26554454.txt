Ten Tiny Love Stories
{{Infobox film
| name           = Ten Tiny Love Stories
| image          = 
| image_size     = 
| caption        =  Rodrigo García
| producer    = Alexis Alexanian Daniel Hassid Gary Winick
| writer         = Rodrigo García
| starring       = Lisa Gay Hamilton Radha Mitchell Alicia Witt Deborah Kara Unger
| music          = Mark Carroll
| cinematography = Rodrigo Prieto
| editing        = Luis Cámara Lionsgate
| released       = 2001
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Rodrigo García. It stars Lisa Gay Hamilton and Radha Mitchell.  The film is a series of ten monologues about love in its different forms.

==Cast==
*Lisa Gay Hamilton as Three
*Radha Mitchell as One
*Alicia Witt as Two
*Deborah Kara Unger as Seven
*Rebecca Tilney as Four
*Kimberly Williams-Paisley as Five
*Susan Traylor as Eight
*Elizabeth Peña as Nine
*Debi Mazar as Six
*Kathy Baker as Ten

==References==
 

==External links==
* 

 

 
 
 

 