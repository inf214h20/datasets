Winter Visitor
Winter Spanish film, written and directed by Sergio Esquenazi.  The film was produced by Esteban Mentasti and Christian Koruk. The picture features Santiago Predero, Sandra Ballesteros, Diego Alonso Gómez, Ana Cuerdo, among others. Is the first Argentine horror film in reach theatrical release in Argentina in 20 years.

==Plot==
A 20 years old Ariel Lambert, after a suicide attempt, is sent with her mother to a small summer town in the state of Buenos Aires. Ariel arrives in winter, a time of the year in which the village is almost deserted. From his room and with the help of a telescope, Ariel witness a man taking children into his house, children that will not ever come out again. Ariel tries to call the police, but given his precarious mental state, they will not take him seriously. Determined to prove what he saw is true, he set forth in search for the evidence required to prove that what he saw is not only part of his imagination but a horrible truth.

==Cast==
* Santiago Predero as Ariel Lambert
* Sandra Ballesteros as Viviana Lambert
* Ana Cuerdo as La gallega
* Diego Alonso Gómez as Pichi
* Pepe Novoa as Dr. Silva
* Rolly Serrano as Miranda
* Catalina Artusi as Julieta Lambert
* Jorge Varas as Ramirez
* Alicia Vidal as Nurse
* Alejandro El Abed as Tino
* Alfredo Castellani as Lito
* Anahí Martella as Clarisa
* Ariel Staltari as Berto
* Diego Sampallo as Pelado

==Background==
Director Sergio Esquenazi has said in media interviews that the story is based on his travels in winter to Pinamar. Forest surrounded by fog and a town with deserted streets and houses were the source his inspiration.

==Filming Location==
The film was shot entirely in Pinamar, Buenos Aires Province, Argentina.

==Distribution==
The film was first featured at the Fantasporto International Film Festival. It opened in Argentina on March, 2008.

The film was also shown at various film festivals, including: the Toronto Film Festival; the New York Film Festival and the Havana Film Festival, Cuba.

In the United States, it was presented at the New York Film Festival on October 2, 2008.  It also opened in Los Angeles on October 12, 2008.

==Critical reception==
Ryan Doom (The Joble Movie Network) Winter Visitor is an effective, intelligent, and superbly written movie that deserves viewership worldwide. It’s a good kick in the ass for horror, showing you don’t have to drop the intelligence in order to create something scary. (www.joblo.com)

Catalina Adlugi Channel 13 (Argentina) (National Television) A great idea and a great achievement. A valuable film.

Esteban Castromán from Haciendo Cine (Film Magazine) (4 Starts ****) ¨Somebodys Watching¨ (1988) directed by Horacio Maldonado and Gustavo Cova, was the last Argentine horror film released in theaters in Argentina.  Winter Visitor break this streak of 20 years and is probably responsible for attracting public attention. And Winter Visitor does through a careful reading
of the best horror film from North American. (www.haciendocine.com)

Diego Trerotola from El Amante (FIlm Magazine) said: Winter Visitor weaves between moody thriller and Gore, between suspense and graphic violence. The film does not delights with any of these topics. Moving skillfully between the two, the film manages to concentrate an essence of horror is worth developing. We hope that this is the beginning of horror genre in Argentina. (www.elamantecine.com)

Hugo Zapata (Cinesargentinos) I can write a long list mentioning Hollywood horror films and compare to Winter Visitor, the Argentine film wins.

Enrique Martínez Terrorifilo (Uruguay)) said: "Winter Visitor" is a horror film made in Argentina that caused a great impression on the author of these lines.
The reasons for such a good concept is due to the suspense and tension masterfully created and also the sensitivity the director takes in developing the characters. (www.terrorifilo.com)

Jorge Carnevale Noticias (Magazine) (4 stars ****)  Argentine cinema face horror genre with the best grades. The film, shot in a misty-blue tone becomes more and
more disturbing. Dont miss it.

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*   (The Joblo Movie Network)

==Critical reception==

 
 