Alias Mr. Twilight
 
{{Infobox film name          = Alias Mr. Twilight| image         = caption       = director      = John Sturges writer        = Malcolm Stuart Boylan Arthur E. Orloff Brenda Weisberg starring      = producer      = John Haggott music         = Vincent Farrar distributor   = Columbia Pictures released      =   runtime       = 69 min. language  English
|followed_by   = budget = approx $100,000 Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008 p38 
}}
Alias Mr. Twilight is a 1946 film by John Sturges.

==Plot==
Five-year-old Susan Holden celebrates her birthday with a group of people, including her loving grandfather Geoffrey Holden, who is also her guardian ever since her parents were killed in a car accident a few years ago. Present at the party is also Susans nurse, the very pretty Miss Corcoran "Corky", and her police investigator boyfriend, Tim Quaine. Tim is specialized in crimes involving confidence men. Geoffrey, who hasnt met Corkys boyfriend before, is a bit discouraged when he learns about Tims profession, since he and his partner in crime, Sam Havemayer, travel the country as con men, specializing in swindling jewelry stores. 

But when Tim sees how fond Corky and Susan are of the young policeman, he changes his opinion of him slightly towards the better. Right after the birthday party, Geoffrey and Sam leave for San Diego to pull a scam on the jewelry stores over there. He has told the family that he is going to San Francisco. When they arrive in San Diego, Geoffrey and Sam immediately pull a job worth over fifteen hundred dollars. Afterwards they rest at their hotel, and Geoffrey decides to tell Sam about his worries for the future. He suspects that he wont be able to hide his shady profession for Susan in all eternity, especially since the people around them increase the risk of her finding out he is up to no good. Geoffrey suggests to Sam that they make a last big hit on a jewelry store before retiring for good. The hit would make them both financially secure for the rest of their lives. Sam promises to consider this as an option as they part roads. 

Arriving home to the family, Geoffrey is met by his highly irritating cousin, Elizabeth Christens. She knows a little of what he is up to and has been blackmailing him for years. Elizabeth threatens to reveal Geoffreys true business, in order to obtain the custody of Susan. Just as Geoffrey and Elizabeth argue, Corky and Tim arrives to the scene with Susan, interrupting them. Geoffrey normally gives Susan and Corky gifts when he returns from his "business trips". This time is no different, and Corky gets a beautiful wallet. Tim examines the wallet, and finds a business card from a San Diego hotel in it, which raises his suspicions. 

Tim is in charge of investigating a series of swindles, who coincidentally have occurred in places where Geoffrey has visited on business. Geoffrey gets nervous, thinking that he is about to be discovered, and consults his house attorney for advice in regard of his guardianship of Susan. The attorney tells him that he could be replaced as guardian and lose custody of Susan if he would be deemed unfit of taking care of the girl. Elizabeth contacts Geoffrey and demands a sum of $25,000 in cash to keep quiet about Geoffreys dealings. Geoffrey doesnt have that kind of money, but he decides to go and do a job on his own, a bank robbery in Sacramento, to get the money. He robs the fancy Duval et Cie jewelry store. 

Tim receives information about the robbery in Sacramento, and reads that a witness has given a description of the robber that fits Geoffreys appearance spot on. Tim confronts Corky about the information, and asks her to quit her job as Susans nurse. Corky refuses to do this, claiming that Susan needs her and she must stay with her. When Geoffrey gets back home, Corky openly confronts him about his "business trips" and tells him about Tims suspicions. She adds that the witness to the robbery will arrive the next evening. 

Geoffrey feels the noose tightening around his neck, realizing that he only has a day to get everything ready for Susans future. He also needs to get Elizabeth of their backs. Geoffrey contacts Sam, who tells him that he has knowledge about a shipload of bills, counterfeit currency, in the harbor. Its an acquaintance of his who has hidden the money there awaiting transport. Geoffrey comes up with a plan to take the money from the ship. He will disguise himself as a construction manager and set up a construction site in the dock beside the ship. He is unaware that he is followed by Tim and his men, who register his doings. Tim believes that Geoffrey is making the effort to get the jewels from his latest robbery at Duval et Cie on the boat and out of the country. 

Geoffrey gets the money, and then tricks Elizabeth of taking it from him. Elizabeth is then arrested for possession of the illegal money, but Tim also comes after Geoffrey. When he does, Geoffrey happily gives him the stolen jewels, but only with the condition that Tim agrees to marry Corky, and that the couple adopt Susan as their daughter. The last scene shows Geoffrey writing a new birthday card to Susan, for her sixth birthday this time, telling her how much he loves her, and that he is happy to have been able to give her new parents. The camera pulls back to reveal that Geoffrey and Sam are sitting playing cards on a prison transport train. Beside them sits Geoffreys cousin Elizabeth. 

==Cast==
* Lloyd Corrigan as Geoffrey Holden
* Trudy Marshall as Corky Corcoran
* Gigi Perreau as Susan
*Michael Duane as Tim Quaine
*Rosalind Ivan as Elizabeth Christens
*Alan Bridge as Sam Havemayer

==References==
 

==External links==
*  

 

 
 
 
 