September (1987 film)
{{Infobox film
| name =September
| image = Septemberpost.jpeg
| caption = Theatrical release poster
| director = Woody Allen
| producer = Robert Greenhut
| writer = Woody Allen
| starring = Mia Farrow Sam Waterston Dianne Wiest
| cinematography = Carlo Di Palma
| music = 
| editing = Susan E. Morse |
| distributor = Orion Pictures Corporation
| released =  
| runtime = 82 minutes
| country = United States
| language = English
| budget = 
| gross = $486,434
}}
September is a 1987 film written and directed by Woody Allen.  Allens intention for September was that it be like "a play on film," hence the great number of long takes and few camera effects.

The movie does not use Allen as an actor, and is one of his straightforwardly dramatic films. The cast includes Mia Farrow, Sam Waterston, Dianne Wiest, Elaine Stritch, Jack Warden, and Denholm Elliott.

The plot centers on Lane (Farrow), who is recovering from a suicide attempt at her house in the country at the end of summer. Local widower Howard (Elliott) has befriended her. Her friend Stephanie (Wiest) is spending the month with her, and her mother, Diane (Stritch), and stepfather (Warden) come to visit. It is a story of unrequited love, betrayal, selfishness, and loneliness.

The film is modeled on Anton Chekhovs play Uncle Vanya, though the gender roles are often subverted. 

==Plot==
After a failed suicide attempt, Lane has moved into her country house to recuperate. Her best friend, Stephanie, has come to join her for the summer to have some time away from her husband. Lanes brassy, tactless mother, Diane, has recently arrived with her physicist husband, Lanes stepfather. Lane is close to two neighbors: Peter, a struggling writer, and Howard, a French teacher. Howard is in love with Lane, Lane is in love with Peter, and Peter is in love with Stephanie.

Diane, once a well-known actress, wants Peter to write her biography, primarily because, many years earlier, a teenage Lane supposedly shot her mothers abusive lover. Lane does not want this painful event to go back in the spotlight, but Peter thinks it would make a great story.

One evening, Diane decides to host a party, ruining Lanes plans with Peter. Peter arrives early and confesses to Stephanie that he has wanted to be alone with her for a long time. Outside, there is an electrical storm, and the lights go out. Candles and piano music create a romantic setting. Diane finds her old Ouija board and talks to the spirits of her previous lovers. A very drunk Howard finally reveals his feelings to Lane, who does not return them. Peter tells Lane that he does not share her feelings. Lane seems to take the rejection well. When everyone else has gone to bed, Peter tries seducing Stephanie, but she is conflicted, later following him back to his house.

The next morning, a real estate agent is showing a couple around the house; Lane is counting on the money from the sale to move back to New York  Lane is feeling depressed: she has not taken Peters rejection well after all, exacerbating Stephanies guilt. Soon after, Peter arrives and kisses Stephanie, just as Lane opens the door to show the room to prospective buyers, and Lane is shocked. Stephanie insists that it meant nothing, while Peter tells Lane that the two of them have deep feelings for each other. Diane comes downstairs, announcing that she and her husband are going to move into the house permanently. Lane becomes even more distraught, insisting that Diane gave Lane the property a long time ago. Diane dismisses it as one of her own drunken whims. Lane experiences a breakdown, accusing her mother of being fake and insensitive.

The films climax comes when an anguished Lane cries, "Youre the one who pulled the trigger! I just said what the lawyers told me to say", thus revealing that Diane was actually the one who shot her abusive lover. Presumably Dianes lawyers thought it would be better if Lane took the fall, as she would be treated leniently. The ordeal has obviously been hugely detrimental to Lane. Diane finally concedes that if she could go back, she would behave differently.

Everyone leaves except Stephanie and Lane. Lane has a lot of paperwork to do for the sale of the house. The film ends with Stephanie encouraging Lane to "keep busy".

==Cast==
* Denholm Elliott as Howard
* Dianne Wiest as Stephanie
* Mia Farrow as Lane
* Elaine Stritch as Diane
* Sam Waterston as Peter
* Jack Warden as Lloyd
* Rosemary Murphy as Mrs. Mason

==Filming==
Allen shot the film twice. It originally starred Sam Shepard as Peter (after Christopher Walken shot a few scenes, but was determined not to be right for the role), Maureen OSullivan as Diane, and Charles Durning as Howard. After editing the film he decided to rewrite it, recast it, and reshoot it. He has since stated he would like to redo it again. 

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
  
 
 
 
 