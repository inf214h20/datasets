A Million A Minute
{{infobox film
| name           = A Million A Minute
| image          =
| imagesize      =
| caption        =
| director       = John W. Noble Bill Bailey (asst director)
| producer       = Quality Pictures Corporation
| writer         = Robert Aitken (novel) Howard Irving Young (scenario)
| starring       = Francis X. Bushman Beverly Bayne
| music          =
| cinematography =
| editing        =
| distributor    = Metro Pictures
| released       = May 8, 1916
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}} lost 1916 American silent romance drama film released by Metro Pictures and starring then off screen lovers Francis X. Bushman and Beverly Bayne. The film is based on a novel, A Million a Minute: A Romance of Modern New York and Paris by Robert Aitken. John W. Noble, a regular director for Metro releases, did directing honors. 

==Cast==
*Francis X. Bushman - Stephen Quaintance
*Beverly Bayne - Dagmar Lorraine
*Robert Cummings - Timothy OFarrell William Bailey - Mark Seager
*Helen Dunbar - Fanchette John Davidson - Duke de Reves
*Charles Prince - Jules, His Valet
*Mrs. Allen Walker - Mrs Smith (billed as Mrs. Walker)
*Carlton Brickert - Stephen Quaintance Sr. (billed as Carl Brickert)
*Mary Moore - Ellen Sheridan
*Jerome N. Wilson - Miles Quaintance (billed as Jerome Wilson)

==See also==
*Francis X. Bushman filmography

==References==
 

==External links==
*  
*  
*  (*if page dont load then click->   link then return and click)

 
 
 
 
 
 
 


 
 