The Substitute (2007 film)
{{Infobox film
| name     = The Substitute
| image    = The Substitute 2007.jpg
| director = Ole Bornedal
| producer = 
| writer   = 
| starring = Paprika Steen   Ulrich Thomsen   Jonas Wandschneider
| cinematography = 
| music = 
| country = Denmark
| language = Danish
| runtime = 93 minutes
| released =  
}}
The Substitute ( ) is a 2007 Danish Science Fiction Horror film directed by Ole Bornedal. The story takes place in a small village in Denmark where an alien comes to Earth to learn about human emotions, but instead is thwarted by a young boy and his friends, who find out that she is part of an intergalatic expedition to collect specimens across the universe. Its up to Carl and his friends to save their town and the world. They try to tell their parents, but all of them scoff at the idea that their children would make up lies about their teacher. 

== Cast ==
*Paprika Steen - Ulla Harms
*Ulrich Thomsen - Jesper Osböll
*Jonas Wandschneider - Carl
*Nikolaj Falkenberg-Klok - Phillip
*Emma Juel Justesen - Rikke
*Mollie Maria Gilmartin - Lotte
*Sofie Gråbøl - Carls mother

==External links==
*  
*  

 

 
 
 
 

 