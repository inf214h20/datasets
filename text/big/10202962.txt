The Royal Mounted Rides Again
{{Infobox Film

 | name = The Royal Mounted Rides Again
 | image_size = 
 | caption =  Ray Taylor
 | producer = Morgan Cox Joseph ODonnell Tom Gibson Harold Channing Wire
 | narrator =  Bill Kennedy Robert Armstrong
 | music = 
 | cinematography = Jerome Ash Charles Van Enger
 | editing = Norman A. Cerf (supervisor) Irving Birnbaum Jack Dolan Alvin Todd Edgar Zane
 | distributor = Universal Pictures
 | released = 23 October 1945
 | runtime = 13 chapters (221 min)
 | country =  United States English
 | budget = 
 | preceded_by = 
 | followed_by = 
 | image = The Royal Mounted Rides Again VideoCover.jpeg
}}
 Universal Serial film serial.  Adventure serials of this type were popular in the early days of film|cinema. The serial, often called cliffhangers, would show one episode per week, with an ending that would hide the outcome of an exciting event, sometimes ending with "tune in next week for the exciting continuing saga..", or something along those lines. Actor and popular singing cowboy of the day, Addison Randall, died during the making of this serial.

==Plot== Canadian Mountie, is directed to seek out the murderer, or murderers, and bring them to justice.

The Mountie joins forces with a French-Canadian policeman, Baileys beautiful daughter, and a phony palm reader to learn the truth. The foursome soon discover that there is a secret gold mine, a double crossing casino owner, and a forger at the bottom of the crime.

==Cast==
* George Dolenz as Constable "Frenchy" Moselle Bill Kennedy as Corporal J. Wayne Decker
* Daun Kennedy as June Bailey
* Paul E. Burns as "Latitude" Bucket
* Milburn Stone as Brad Taggart Robert Armstrong as Jonathan Price
* Danny Morton as Eddie "Dancer" Clare, Prices dealer
* Addison Richards as Jackson Decker
* Tom Fadden as Lode MacKenzie
* Joe Haworth as Bunker Helen Bennett as Dillie Clark aka Madame Mysterioso
* Joseph Crehan as Sergeant Nelson
* Selmer Jackson as Superintendent MacDonald
* Daral Hudson as Sergeant Ladue George Lloyd as Kent

==Critical reception==
Cline claims that The Royal Mounted Rides Again is "pretty close to being the weakest chapter film   ever made."  An excellent cast was wasted and the story "wandered aimlessly   almost no suspense." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 46
 | chapter = 3. The Six Faces of Adventure
 }} 

==Chapter titles==
# Canaska sic|  Gold
# The Avalanche Trap
# River on Fire
# Skyline Target
# Murder Toboggan
# Ore Car Accident
# Buckboard Runaway
# Thundering Water
# Dead Men for Decoys
# Derringer Death
# Night Trail Danger
# Twenty Dollar Double Cross
# Flaming Showdown
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 241–242
 | chapter = Filmography
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Universal Serial Serial  Secret Agent X-9 (1945)
| years=The Royal Mounted Rides Again (1945)
| after=The Scarlet Horsemen (1946)}}
 

 

 
 
 
 
 
 
 
 
 
 
 
 