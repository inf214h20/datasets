The Pledge (film)
 
{{Infobox film
| name           = The Pledge
| image          = The Pledge 2001 film poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Sean Penn
| producer       = Andrew Stevens
| screenplay     = Jerzy Kromolowski Mary Olson-Kromolowski
| based on       =   Robin Wright Penn Vanessa Redgrave Sam Shepard 
| music          = Klaus Badelt Hans Zimmer
| cinematography = Chris Menges Jay Lash Cassidy
| studio         = Clyde Is Hungry Films Epsilon Motion Pictures Franchise Pictures Morgan Creek Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 123 minutes  
| country        = United States
| language       = English
| budget         = $35 million 
| gross          = $29.4 million   Retrieved October 13, 2013 
}} Robin Wright Penn, Vanessa Redgrave, Sam Shepard, Mickey Rourke, and Benicio del Toro.

It is based on  . Dürrenmatt wrote The Pledge to refine the theme he originally developed in the screenplay for the 1958 German film It Happened in Broad Daylight with Heinz Rühmann.

==Plot==
Retired police detective Jerry Black is seen mumbling to himself, apparently drunk, sitting on a bench outside a disused gas station. The scene then shifts to events in the recent past. Jerry is ice fishing, then driving to work. After work, he goes to a restaurant, where the Department has thrown him a retirement party. Police captain Eric Pollack gives Jerry a gift, a fishing trip in Mexico. The party is interrupted by the discovery of a murdered child, Ginny. Jerry decides to go with another detective, Stan Krolak, to the scene of the crime.
 Native American man with mental retardation. During the interview, the man eventually confesses, but steals one of the deputies guns and commits suicide. To the other detectives, the case is over. But Jerry does not think that the killer died that night. He is adamant about his pledge to find the killer, and does not go on the fishing trip. Jerry visits the victims grandmother, Annalise Larsen, directed there by an older woman. The Grandmother tells Jerry that the child told many stories, and a later visit to one of her friends reveals that Ginny had a friend she called "The Giant". Jerry sees a picture Ginny drew of "The Giant" and his black station wagon that does not resemble the Indian; he takes the picture.

Jerry goes to Stan, and asks him to reopen the case. Stan refuses, but gets Jerry more information about similar cases in the area. Jerrys further investigation leads him to three unsolved and similar cases that the Native American could not have committed. Jerry shows the childs picture and presents his research to Captain Pollack and Stan, who are doubtful. 

Jerry rents a cabin and spends time fishing. He notices a gas station near the lake and likes the location so much he asks the owner, Floyd Cage, if he is interested in selling the place. It is implied, though not stated, that Jerry wants to use the station to track the killer. Jerry buys the gas station and moves into the house behind it, meets local bartender Lori, and slowly becomes a father figure to her daughter Chrissy. 

Soon, Chrissy becomes friends with a local pastor, Gary Jackson. Jerry is uncomfortable about this and begins to think Jackson is the killer. Chrissy is shown meeting a man driving a black car with a toy hedgehog hanging on the rear mirror – hedgehogs being another aspect of Ginnys drawing that Jerry believes to be a clue. Chrissy explains to Jerry that she met a wizard who gave her hedgehog candies and told her not to tell her parents they met – she figured it was OK to tell Jerry, since he is not her father. Jerry realizes this is the killer, and, using Chrissy as bait, stages an operation, with Stans help, to catch him.

A car is shown driving with a porcupine hanging from the rear-view mirror. The woman who owned the chocolate shop is shown searching for "Oliver", implying that he is the killer. After hours of waiting, Stan and the other police leave. They tell Lori what happened, and, after racing to the place, Lori confronts Jerry about putting her daughter in danger. The car that was shown approaching is seen destroyed in a fiery collision with a freight truck.

In the final scene, reflecting the first, Jerry sits on a bench in front of the ruined gas station, apparently drunk, mumbling to himself that the killer is still out there.

==Cast==
* Jack Nicholson as Jerry Black
* Patricia Clarkson as Margaret Larsen
* Benicio del Toro as Toby Jay Wadenah
* Aaron Eckhart as Stan Krolak
* Helen Mirren as Doctor
* Tom Noonan as Gary Jackson Robin Wright Penn as Lori
* Vanessa Redgrave as Annalise Hansen
* Mickey Rourke as Jim Olstad
* Sam Shepard as Eric Pollack
* Harry Dean Stanton as Floyd Cage
* Dale Dickey as Strom
* Costas Mandylor as Monash Deputy
* Michael OKeefe as Duane Larsen
* Lois Smith as Helen Jackson
* Brittany Tiplady as Becky Fiske
*        ?     as Oliver
* Eileen Ryan as Jean
* Pauline Roberts as Chrissy

==Production== Merritt and Lytton, British Columbia|Lytton, all in British Columbia.

==Reception==
===Box office===
The Pledge did not perform well at the box office. The film opened in 1,275 theaters and grossed $5,765,347, with an average of $4,521 per theater and ranking #11 at the box office. The film ultimately earned $19,733,089 domestically and $9,686,202 internationally for a total of $29,419,291, below its $35 million production budget.    

===Critical response===
The Pledge received mainly positive reviews from critics. The film has a "certified fresh" score of 78% on Rotten Tomatoes based on 121 reviews with an average rating of 6.8 out of 10. The critical consensus states "Though its subject matter is grim and may make viewers queasy, The Pledge features an excellent, subtle performance by Jack Nicholson."   The film also has a score of 71 out of 100 on Metacritic based on 33 critics indicating "Generally favorable reviews." 

James Berardinelli gave The Pledge three stars, calling it "clever in the way that it gradually reveals things, but never gives us too much information at one time."  Roger Ebert gave the film four stars out of four and later added it to his Great movies list, writing: "The last third of the movie is where most police stories go on autopilot, with obligatory chases, stalkings and confrontations. Thats when The Pledge grows most compelling. Penn and Nicholson take risks with the material and elevate the movie to another, unanticipated, haunting level." 

==Accolades==
* Sean Penn - Palme dOr at the 2001 Cannes Film Festival  - NOMINATED Grand Prix of the Belgian Syndicate of Cinema Critics - NOMINATED
* 2002 for the Danish Bodil - NOMINATED
* Benicio Del Toro - 2002 ALMA Award - NOMINATED
* Brittany Tiplady - 2002 Young Artist Award - NOMINATED
* Hans Zimmer - 2001 World Soundtrack Award - NOMINATED

==References==
 

==External links==
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 