Oi Thalassies oi Hadres
{{Infobox film
| name           = Oi Thalassies oi Hadres
| image          = 
| image_size     = 
| caption        = 
| director       = Giannis Dalianidis
| producer       = Filopimin Finos
| writer         = Giannis Dalianidis
| narrator       = 
| starring       = Zoe Laskari Kostas Voutsas Martha Karagianni Faidon Georgitsis Giannis Vogiatzis Mary Chronopoulou
| music          = Mimis Plessas
| cinematography = Giorgos Arvanitis
| editing        = Petros Lykas
| distributor    = Finos Film
| released       = February 2, 1967
| runtime        = 91 minutes
| country        = Greece
| language       = Greek
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1967 Cinema Greek musical film, directed by Giannis Dalianidis and starring Zoe Laskari, Kostas Voutsas, Martha Karagianni, Faidon Georgitsis, Giannis Vogiatzis and Mary Chronopoulou. At the time of its release it was among the most expensive films ever made in Greece and one of the most commercially successful Greek films.

==Plot==
Mary (Zoe Laskari) is a rich young lady that challenges problems in a traditional folk neighbourhood in Plaka, Athens, when she moves to play modern music in a store opposite a traditional taverna, where Fotis (Faidon Georgitsis), a bouzouki player, works along with Kostas (Kostas Voutsas), Sofia (Mary Chronopoulou) and others. The gang of the taverna soon turns against Mary, for the reason that she alters the character of the neighbourhood. Eventually, Fotis falls in love with Mary, but his friends margin him as he seems willing to sacrifice everything for her in order to be accepted by her social circles. A second love story involves in the film, as Eleni (Martha Karagianni), sister of Fotis, tries to attract Kostas using various ways.

==Production== 1966 film Greek culture American musical films and make it attractive for the general public. In addition to this, the production company spent a large amount of money to make the film more glamorous, an element that was absent for the great majority of the Greek films of the era.
 Greek stars who portrayed the major roles, while composer Mimis Plessas and singer Giannis Poulopoulos surrounded the characters of the film producing a great musical result.

==Cast==
Starring:
:Zoe Laskari .... Mary Kaniatoglou
:Kostas Voutsas .... Kostas Pitouras
:Martha Karagianni .... Eleni Tsitoura
:Faidon Georgitsis .... Fotis Tsitouras
:Nana Skiada .... Mrs. Kaniatoglou
:Aris Maliagros .... Sokratis Kaniatoglou
:Giorgos Tsitsopoulos .... Jim
:Aleka Mavili .... Markisia
:Nikos Fermas .... Apostolis
and
:Giannis Vogiatzis .... Memas Gardoubas

Guest star:
:Mary Chronopoulou .... Sofia
----
 1968 hit of Finos Film and Giannis Dalianidis, Mia Kyria sta Bouzoukia.

==Major themes== pop and feministic ideology that arrives in Greece, as the main female character of the movie is a young woman from the upper class, who forms her own cohort along with her female friends and brings new mores in an old-fashioned local society.

==Release==
The film released in Greek cinemas on February 20, 1967 and made 531,278 tickets in Greece, ranked 3rd for the season 1966-67. {{cite web|url=http://theopeppasblog.pblogs.gr/2007/05/podosfairika-40-hronia-pisw-ellada.html |script-title=el:Ποδοσφαιρικά, 40 Χρόνια πίσω-Ελλάδα 
 |publisher=Theo Peppas blog |language=Greek |date=2007-06-03 |accessdate=2008-10-18}}  The film was also screened in the Cannes Film Festival and was received with enthusiasm by the audience, although it did not officially contest. Especially, Zoe Laskari was seen as an international celebrity and paparazzi followed her everywhere during her stay in Cannes.

==References==
 

==External links==
* 
*   

 
 
 
 
 