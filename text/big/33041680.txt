The Twins (1923 film)
 
{{Infobox film
| name           = The Twins
| image          = 
| image_size     =
| caption        = 
| director       = Leslie McCallum
| producer       = Leslie McCallum
| writer         = Ray Whiting
| based on       = 
| narrator       =
| starring       = Ray Whiting
| music          =
| cinematography = Leslie McCallum
| editing        = 
| studio = Blue Gum Company, for the Charity Moving Picture Company
| distributor    = 
| released       = 28 June 1923
| runtime        =
| country        = Australia English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Twins is a 1923 Australian silent film directed Melbourne cinematographer Leslie McCallum. It is a rural farce-melodrama and is considered a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 117. 

==Plot==
Characters from the country visit the city. Among the characters are a female vamp, who winds up committing suicide.

==Cast==
*Ray Whiting and Jim Paxton as the twins
*Cath McMicking as heroine
*Aubrey Gibson as villain
*Doreen Gale as vamp
*Jim Doods as Horace Hothouse
*Keith McHarg
*Billy Begg
*Norman Carlyon

==Production==
The film was made to raise funds for various charities including the Melbourne Womens Hospital. It was sponsored by Carlyons ballroom in St Kilda; Norman Carlyon appeared in the cast and a fund raising ball was held on 19 June 1923. 

==Reception==
According to contemporary reports, response to the film was positive and money was made from its release. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 


 