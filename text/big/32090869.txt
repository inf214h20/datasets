Small Apartments
{{Infobox film
| name          = Small Apartments
| image	        = Small Apartments FilmPoster.jpeg
| border        = yes
| caption       = Theatrical release poster
| director      = Jonas Åkerlund
| producer      = Ash R. Shah Timothy Wayne Peternel David Hillary Bonnie Timmermann
| screenplay    = Chris Millis
| based on      =  
| starring      = Matt Lucas James Caan Johnny Knoxville Billy Crystal Dolph Lundgren Peter Stormare Juno Temple Amanda Plummer Rebel Wilson
| cinematography= Pär M. Ekberg
| music         = Per Gessle
| editing       = Christian Larson
| studio        = 
| distributor   = Inferno Distribution (international sales) Sony Pictures
| released      =  
| runtime       = 97 minutes
| country       = United States
| language      = English
| budget        = 
}}
Small Apartments is a 2012 American comedy film directed by Jonas Åkerlund. It tells the story of Franklin Franklin, played by Matt Lucas, who by mistake kills his landlord, Mr. Olivetti, played by Peter Stormare.  The cast co-stars Dolph Lundgren, Johnny Knoxville, James Caan, Billy Crystal, Juno Temple, Rebel Wilson, Saffron Burrows and Amanda Plummer. The screenplay was written by Chris Millis and adapted from his own novella.  The film premiered at the South by Southwest Film Festival on March 10, 2012.

==Plot== eccentric Anti-social social misfit compulsive hoarder Moxie soda pickles with stoner Tommy Balls, neurotic geriatric recluse Mr. Allspice, and aspiring dancer Simone who lives with her mother - both of whom may or may not be Prostitution|prostitutes. 
 robbed (of both his brothers watch and Olivettis truck) and assaulted by two very dimwitted muggers.
 almost idolizes popular and social life. Swiss bank in a new identity. Meanwhile, fire investigator Burt Walnut, and other detectives examine Olivettis body, and quickly conclude that its staged due to Franklins amateurishness, and put a lookout for his pickup truck. Walnut, visiting the apartment complex, meets Balls and Allspice and becomes suspicious of Franklin. Walnut is estranged from his wife (who was having an affair with his cousin), and learns that Allspices wife died around the time as Olivettis wife, 13 years ago.
 torso when startled by Simone. Franklin returns to his apartment, but seeing the police, keeps on driving. Walnut discovers that Allspice has committed suicide, (likely upset over the death of Olivetti as Allspice revealed that they were close friends) and when answering a phone call by Franklin, is asked to adopt Franklins dog (also named Bernard). Walnut informs Franklin of the muggers arrest, calling them Olivettis murderers, remarking of Olivetti that "some might say that he had it coming to him."

Although innocent, Franklin takes his Switzerland flight (dressed in shorts and button up shirt), sitting next to Dr. Sage Mennox, a self-help author whom Bernard obsessed over. Franklin tells Dr. Mennox that his brother had died, and was not insane (as Mennox had previously asserted), but had actually been impaired by a fatal brain tumor "the size of a racquet ball". Mennox is taken off guard by this, and Franklin (who has been mad at him for belittling Bernard, his hero, in the past), slyly remarks that they would be stuck together, in this awkward situation (on Mennoxs part), for the duration of a very long flight. 
 Swiss women Swiss folk costume.

==Cast==
* Matt Lucas as Franklin Franklin
* James Caan as Mr. Allspice, Franklins neighbor in 244
* Johnny Knoxville as Tommy Balls, the neighbor on the other side, who works at Tags Liquor
* Billy Crystal as Burt Walnut, the fire investigator looking into Mr. Olivettis death
* Juno Temple as Simone, who lives across the way from Franklin
* James Marsden as Bernard Franklin, Franklins brother who lives in a mental hospital
* Peter Stormare as Mr. Olivetti, the landlord
* David Koechner as Detective OGrady
* DJ Qualls as Artie, night clerk at Tags Liquor
* Rosie Perez as Ms. Baker, nurse at the hospital
* Amanda Plummer as Mrs. Ballisteri, Tommy Balls Mother
* Dolph Lundgren as Dr. Sage Mennox
* Saffron Burrows as Francine
* Rebel Wilson as Rocky, Tommy Balls girlfriend
* Ned Bellamy as Daniel, the EMT
* Angela Lindvall as Lisa, the flight attendant
* David Warshofsky as Detective McGee
* Noel Gugliemi as Dog walker (driving instructor samaritan)

==Production==
The film was produced through Deep Sky, Silver Nitrate, Amuse Entertainment and Bonnie Timmerman. It was co-financed by Sense And Sensibility Ventures and Silver Nitrate.    According to director Jonas Åkerlund, it was important for him that the production had a strong element of spontaneity; he therefore did not focus solely on the film like he had with his previous features, but also made 34 commercials and five music videos the same year. Small Apartments was made largely with the same crew Åkerlund uses in his other projects. The first actor to be cast was Matt Lucas in the lead.    Photography took 20 days and ended in April 2011.    

==Release==
The film premiered at the South by Southwest Film Festival on March 10, 2012.  It was released on home media on February 19, 2013. 

==Reception==
John DeFore of The Hollywood Reporter wrote that "Small Apartments might crumble if not cemented by a compellingly weird performance by Little Britains Matt Lucas", and that "even the scene-chewingest performance here (Peter Stormare as the sleazoid landlord, seen in flashback) augments the whole instead of drawing attention from it". DeFore also complimented Billy Crystal, writing that his performance "roots the picture to its ostensible genre while reminding us how engaging the actor can be when hes appearing not to try.   " 

==References==
 

==External links==
*  
*   on Rotten Tomatoes

 

 
 
 
 