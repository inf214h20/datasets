Ghajini (2008 film)
 
 
{{Infobox film
| name           = Ghajini
| image          = Ghajini Hindi.jpg
| caption        = Theatrical release poster
| director       = A. R. Murugadoss
| producer       = Allu Aravind Madhu Mantena
| writer         = Piyush Mishra  (Dialogue) 
| screenplay     = A. R. Murugadoss
| story          = A. R. Murugadoss Memento by Christopher Nolan Pradeep Rawat Riyaz Khan
| music          = A. R. Rahman
| cinematography = Ravi K. Chandran Anthony Gonsalves
| studio         = Geetha Arts Showman Pictures
| released       =  
| runtime        = 185 minutes 
| country          = India Hindi
| budget           =   
| gross            =     
}} of the Pradeep Rawat and Riyaz Khan essay supporting roles. The film was inspired by Christopher Nolans Memento (film)|Memento.  
 Polaroid Instant 3D video 3 Idiots the following year.  Ghajinis paid preview collections were  .  With this movie, Asin made her debut in Bollywood.

==Plot==
Sunita (Jiah Khan) is a medical student, working on a project about the human brain with her class friend
. When she is denied access by her professor to the curious case of Sanjay Singhania (Aamir Khan), a man reported to have anterograde amnesia, because it is under criminal investigation, Sunita decides to investigate the matter herself.
 Pradeep Rawat), a notable social personality in the city, and the man directly responsible for Kalpanas death and Sanjays condition.

Police inspector Arjun Yadav (Riyaz Khan) tracks Sanjay down and hits him unconscious. Yadav finds two diaries in which Sanjay has chronicled the events of 2005 and 2006. The film flashes back to 2005 as Yadav reads the diary. Sanjay Singhania is the chairman of the Air Voice mobile telephone company. In the course of business, he sends his men to meet Kalpana, a struggling model of Mumbai, about putting up a billboard above her apartment. The owner of Kalpanas advertising firm misinterprets this as a romantic advance and, in view of a possible lucrative Air Voice ad campaign, encourages Kalpana to accept the overture. Kalpana thinks of it as an innocent prank that may fetch her better modelling work and decides to charade as Sanjays girlfriend. Sanjay goes to confront Kalpana about this but falls in love with her at first sight. He hides his identity and introduces himself as Sachin, and the two begin spending time together. The diary ends with Sanjay proposing to Kalpana and promising himself that he will reveal his actual identity if she accepts.

When Yadav is about to read the 2006 diary, Sanjay awakes and attacks him, tying him up. Ghajini realizes that someone is trying to kill him but cannot figure out who. Sunita visits Sanjays flat and discovers Sanjays plan to kill Ghajini. She takes both his diaries before finding Yadav, beaten and bound, and freeing him. Just then, Sanjay arrives, but he remembers neither of them and chases them out. Yadav is hit by a bus as he flees in terror, and Sunita, believing Ghajini is the good guy in danger, informs him about Sanjay. Ghajini arrives at Sanjays flat and destroys all of Sanjays photographs and notes, as well as the tattoos on Sanjays body, so that Sanjay is left with nothing to help him recover his memory.

As Sunita reads the diaries, the film flashes back to 2006, where it is revealed that Kalpana had accepted Sanjays proposal. When this diary ends abruptly, Sunita digs further and discovers that Kalpana was travelling to Goa for a modelling assignment when she came upon 25 innocent young girls being trafficked. She had saved the girls, who named Ghajini as the ringleader of the racket. Outraged, Ghajini broke into Kalpanas apartment with his goons to kill her. When Sanjay arrived, he found Kalpana stabbed. Ghajini then hit Sanjay over the head with an iron rod. Sanjays last sight was Ghajini brutally murdering Kalpana with the iron rod. Kalpanas last word to Sanjay was "Ghajini."

Sunita, now aware of the shocking truth, finds Sanjay in the hospital and tells him the truth. He flies into a heartbroken rage and tracks down Ghajini with Sunitas help. He fights off all of Ghajinis henchmen with a superior and anger-fueled strength. Ghajini, realizing Sanjay is too strong for him, flees. Sanjays memory loss strikes again, and he forgets who Ghajini is. Ghajini takes this opportunity to stab Sanjay and taunt him with the grisly tale of how he murdered Kalpana. As he is about to make Sanjay relive the experience by killing Sunita the same exact way, Sanjay recovers the memory of Kalpanas death, and overpowers Ghajini in a flash of strength. He finally kills Ghajini, in the same way Ghajini had killed Kalpana.

The film ends with a still-amnesiac Sanjay volunteering at an orphanage. Sunita gives him a gift that reminds him of his bond with Kalpana, and Sanjay imagines Kalpana by his side, finally at peace with himself.

== Cast== short term memory loss after a brutal attack
*Asin as Kalpana, a model who gains publicity by falsely proclaiming herself to be the girlfriend of Sanjay Singhania
*Jiah Khan as Sunita, a medical student, who tries to study the case of Sanjay Singhania and his amnesiac problem, even though she is forbidden to do so Pradeep Rawat as Ghajini Dharmatma, a gang honcho and the mastermind of many illegal and criminal ventures.
*Riyaz Khan as Inspector Arjun. A police inspector who is investigating the murders by Sanjay Singhania.
*Tinnu Anand as Satveer Kohli (Kalpanas Boss)
*Sunil Grover as Sampat
*Sai Tamhankar as Sunitas friend

==Production== Deadpan Desert and Mumbai. Aamir Khan had spent a year working out at the gym, training for his role.  This was the Bollywood debut for Asin.

==Release==
Ghajini was released on 25 December 2008 with an estimated 1,500 prints worldwide, {{cite web |url=http://www.hindustantimes.com/News-Feed/Cinema/Ghajini-already-a-hit-at-ticket-counters/Article1-360114.aspx|title=
Ghajini already a hit at ticket counters|work=Hindustan Times|accessdate=23 December 2011}}  including 1,200 prints(digital and analogue versions) in the domestic market,       making it the largest Bollywood release at that time. The domestic rights were sold to Geetha Arts for  , while satellite,overseas and home media rights were sold at a total of  , breaking the records of Shah Rukh Khan starrer Om Shanti Oms  . 

The overseas distributors, Reliance Entertainment released the film with 300 prints in 22 countries, including 112 prints in USA and Canada, 65 prints in the UK and 36 prints in UAE. Ghajini was also released in Norway, Germany, Denmark, Netherlands, Belgium, South Africa, Australia, New Zealand, Hong Kong and Singapore.  It had around 650 paid previews which fetched it around   70&nbsp;million. 

===Home media===
The two-disc collectors edition DVD was manufactured by Big Home Video and distributed by international distributor, Adlabs Films Ltd on 13 March 2009 with a MSRP of US$19.99. It received a 15+ age rating by the British Board of Film Classification for persistent and excessive violence. 

===Video game===
  3D Personal PC game with an MSRP of US$14.99.  Although never officially rated, the distributor recommends that 15+ year old players partake in the game.  Ghajini has also reportedly earned   from gaming rights (a first for a Bollywood film),   for satellite rights for India and overseas and something to the tune of   for home video and music. 

===Mobile content===
 
Indiagames has developed four games and one application based on the title on the mobile platform. It has games like Ultimate Workout, Memory Revival, Brain Trek, and Ghajini The Game.

===Controversies===
  Tamil original), he had not bought the rights to remake the film in Hindi. 

==Reception==

===Critical response===
Ghajini received generally positive reviews from critics. Sonia Chopra of Sify gave the movie 3.5 stars and recommended watching it "For the four As—Aamir, Asin, AR Murgadoss and AR Rahman".  Rajeev Masand of CNN IBN gave 3 stars writing, "Ghajini isnt a particularly good film, but entertainment it delivers by the bucketful."  Martin DSouza of Bollywood Trade News Network gave 3.5 stars, noting the flaws in screenplay, while praising the action.  Taran Adarsh of Bollywood Hungama remarked that the movie "is a winner all the way" and gave it 4.5 stars.  Nikhat Kazmi of The Times of India praised the performance by Aamir Khan as its high point and awarded 3.5 stars. 

Zee News described Aamirs performance as his best till date.  Sukanya Verma of Rediff gave the movie 3.5 stars, while describing the film as "a sleek album of dark memories, which are terrifying to relive and shattering to experience".  Noyon Jyoti Parasara of AOL India said, "Most comparisons often point out that a remake is not as worthy.Ghajini however succeeds when it is compared to the Tamil version directed by the same director."  Anupama Chopra of NDTV said "Ghajini isnt a great film or even a very good one but I recommend that you see it. It is, as we used to say in the old days, paisa vasool.  Kaveree Bamzai of India Today said that "This is brutality, choreographed by a poet, and therefore that much more compelling." giving it 3.5 stars. 

The film received some mixed and negative reviews. Gaurav Malani of Indiatimes gave 2 stars, criticising its length while praising the performance of the cast.  Raja Sen of Rediff rated the movie 2.5/5 and criticised the performance of Asin while concluding, "overwhelming feeling is one of regret".  Shubhra Gupta of Express India concluded that Ghajini is too long, too violent, and criticised Jiah Khans acting and dancing skills, but praised the performances of Aamir Khan and Asin.  Hindustan Times gave it 2 stars and said "Youd like to give Ghajini a long-term memory loss. Kya, kyon, kahan? Murugadoss.? Aamir? Asin? Who? Got to jog my memory... maybe after 15 minutes." 

==Box office== All Time Blockbuster".  Some other independent sources have stated that Ghajini has grossed   in two weeks—totting up a domestic gross of   and an overseas collection of  .    A success party was organised to celebrate Ghajinis   worldwide celebration in January 2009.  Before this,one more success party was organised in Mumbais Taj Lands End hotel on 30 December 2008,for celebrating that Ghajini grossed   in five days.   

== Influences == Tamil version Memento Mori. The film stars Guy Pearce as Leonard Shelby, a former insurance fraud investigator searching for the man he believes raped and killed his wife during a burglary. Leonard suffers from anterograde amnesia, which he contracted from severe head trauma during the attack on his wife. Certain concepts like writing notes behind instant Polaroid photographs and tattooing facts on his body were inspired by the film. According to Aamir Khan, "Ghajini is not a remake or even slightly inspired by Memento, but its a remake of the Tamil film, Ghajini (2005 film)|Ghajini".   
 French film Amélie. 

It was notable that Christopher Nolan was aware that an Indian film with a same kind of story similar to Memento was released though in a different story plot. As said by Anil Kapoor in an interview, when he met Nolan, he said Nolan was "upset" about the Hindi film and Nolan amusingly said that he was neither "credited" nor "compensated." 

== Soundtrack ==
{{Infobox album
| Name = Ghajini 
| Type = soundtrack 
| Artist = A.R Rahman 
| Cover = 
| Released =  
| Recorded = Panchathan Record Inn and AM Studios Feature film soundtrack 
| Length = 28:23
| Label = T-Series 
| Producer = A. R. Rahman
| Music Director = A. R. Rahman 
| Last album = Yuvvraaj (2008) 
| This album = Ghajini (2008)  Slumdog Millionaire (2009)
}}
{{Album ratings
| rev1 = Rediff
| rev1Score =    
| rev2 = Bollywood Hungama
| rev2Score =    
}}
The film has six songs composed by A. R. Rahman, with lyrics by Prasoon Joshi.

{{tracklist
| headline        = Tracklist
| music_credits   = no
| extra_column    = Artist(s)
| title1          = Guzarish
| extra1          = Javed Ali, Sonu Nigam
| length1         = 5:29
| title2          = Aye Bachchu
| extra2          = Suzanne DMello
| length2         = 3:48
| title3          = Kaise Mujhe
| extra3          = Benny Dayal, Shreya Ghoshal
| length3         = 5:46
| title4          = Behka Karthik
| length4         = 5:13
| title5          = Latoo
| extra5          = Shreya Ghoshal
| length5         = 4:30
| title6          = Kaise Mujhe 
| extra6          = Instrumental
| length6         = 4:01
}}

===Reception===
The album received positive reviews from critics and fans. Bollywood Hungama gave an extremely positive review saying, "The music of Ghajini is all set to make waves way into 2009 after the Christmas release of the film. When best of the best list would be compiled at the year end, it would be hard to ignore Ghajini."   
Rediff.com gave it the highest possible rating of five stars with the reviewer praising Rahman saying, "This could just be one of his finest albums ever. Not just are the tracks great, but each one segues into the next with perfect unpredictability."   

==Awards==
* Star Screen Award for Most Promising Newcomer - Female- Asin
* Stardust Superstar of Tomorrow - Female-Asin Stardust Hottest New Filmmaker- A.R. Murugadoss
* Stardust Hottest New Film Award
* Filmfare Best Female Debut Award-Asin
* Filmfare Best Action Award-Peter Hein IIFA Best Female Debut-Asin Pradeep Rawat

==See also==
*Short-term memory loss Ghajini
*Declarative memory

==References==
 

== External links ==
*   on    
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 