Broken in the Wars
{{Infobox film
| name           = Broken in the Wars
| image          = 
| alt            =  
| caption        = 
| director       = Cecil M. Hepworth
| producer       = 
| writer         = 
| screenplay     = 
| story          =  Henry Edwards John Hodge
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = 
| budget         = 
| gross          = 
}} silent drama Henry Edwards, John Hodge cobbler returning from the First World War is persuaded by his aristocratic former employer and the Pensions Minister to receive a grant that will enable him to open his own shop.  It was made by the Hepworth Company.

==Cast== Henry Edwards - Joe
* Chrissie White - Mrs. Joe
* Alma Taylor - Lady Dorothea John Hodge - Himself
* Gerald Ames - Customer
* John MacAndrews - Customer

==References==
 

==Bibliography==
* Bamford, Kenton. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.

==External links==
* 

 
 
 
 
 
 
 


 