The Key to Reserva
{{Infobox film
| name           = The Key to Reserva
| image          = The Key to Reserva.jpg
| caption        = 
| director       = Martin Scorsese
| producer       = Marjie Abrahams Jules Daly Emma Tillinger Koskoff
| writer         = Ted Griffin
| starring       = Martin Scorsese Simon Baker Ted Griffin Kelli OHara
| cinematography = Harris Savides
| editing        = Thelma Schoonmaker
| music = Ralph Farris  
| released       = December 14th, 2007
| runtime        = 10 minutes
| country        = Spain
| language       = English
}}
 Cava champagne starring, written and directed by Martin Scorsese. 

==Plot==

The film begins with  framing device, wherein Scorsese, playing himself, describing how he discovered three and a half pages of an unproduced Hitchcock film, "The Key to Reserva".  As part of this film preservation work he plans to film the script as Hitchcock would have filmed it.

The film, which contains no dialogue, shows Roger Thornberry (Simon Baker) arriving at a box seat during an orchestra performance.  He sees a key hidden with the boxs light bulb, and goes to retrieve it.  He is noticed by one of the performers, Leonard, who signals to his accomplice Louis Bernard, who is holding Rogers wife Grace hostage in the audience.  Leonard then goes to stop Roger.  The two fight and Leonard falls from the box seat, presumably to his death.  Roger uses the key to open a locked case which contains a bottle of Freixenet with top secret files hidden inside.  

The film abruptly stops, as Scorsese explains that a page is missing, so he simply filmed the concluding paragraph of the script which shows Louis Bernard arrested, as Roger and his wife reunite over a glass of Freixenet.

Scorsese then discusses with the interviewer possible future projects. The camera pans back to reveal Scorsese, Thelma Schoonmaker, and the interviewer in an office in a tower block, meanwhile crows flock around the building.

==Cast==

*Martin Scorsese as himself
*Simon Baker as Roger Thornberry
*Ted Griffin as interviewer
*Kelli OHara as Grace Thornberry
*Thelma Schoonmaker as herself
*Michael Stuhlbarg as Louis Bernard

== External links ==

* 

 

 
 
 
 
 
 
 

 