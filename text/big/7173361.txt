Betty Boop's Crazy Inventions
 
{{Infobox Hollywood cartoon
| cartoon_name = Betty Boops Crazy Inventions
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist = Bernard Wolf
| voice_actor = Mae Questel
| musician = Samuel Lerner Sammy Timberg
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = January 27, 1933
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}
Betty Boops Crazy Inventions is a 1933 Fleischer Studios animated short film, featuring Betty Boop.

==Plot==
At the inventors show, Betty, Bimbo (Fleischer)|Bimbo, and Koko the Clown demonstrate a variety of gadgets, including:

* The spot remover &mdash; a large steam-powered device that removes the spot by cutting a hole in the fabric.
* The chesse snuffer &mdash; a mechanical foot snuffs out the cigarette, then a mechanical hand sweeps it up.
* The soup silencer &mdash; parts from a music box are installed on a spoon to convert the slurps to music.
* The sweet corn regulator &mdash; a typewriter is adapted to position the corn for easy eating.
* The voice recorder
* The self-threading sewing machine &mdash; a mechanical hand and eyeball thread the needle.

When the automated sewing machine gets out of control and proceeds to sew various things together, Bimbo and Betty escape via an umbrella that turns into a helicopter.

==In other media==
* A short clip from this cartoon can be seen in the opening credits of the Futurama episode A Fishful of Dollars.

==A Brief Story==
Assisted by ticket-taker Bimbo the Dog and product-demonstrator Koko the Clown, Betty Boop stages a "Big Invention Show." Highlights of the program include a pig-powered pipe organ, a Rube Goldberg-style spot remover, a cigarette snuffer, a soup silencer, a sweet-corn regulator, and an egg-frying device, replete with optional hen and rooster. Betty herself demonstrates a most unusual voice recorder with a performance of "Keep a Little Song Handy." The shows number-one attraction, a very powerful self-threading sewing machine, knits up the proceedings quite nicely. ~ Hal Erickson, Rovi
A message from "CCTheMovieman" rated it 5 stars. 
" This was my first look in over 40 years at a Betty Boop cartoon.....and I was impressed. It was pretty funny and very "inventive," pun intended. This really wasnt her style, either, as I have since discovered (singing and dancing was her forte.)" 

==References==
 

==External links==
* 
* 
 

 
 
 
 
 


 