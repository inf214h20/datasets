Meet Market (film)
 
{{Infobox film
| name           = Meet Market
| image          = Meet Market film poster.jpg
| caption        = Film poster
| director       = Charlie Loventhal
| producer       = Pam Auer Scott Valentine
| writer         = Charlie Loventhal
| starring       = Krista Allen Elizabeth Berkley Susan Egan Julian McMahon Missi Pyle Alan Tudyk Aisha Tyler
| music          = David Robbins
| cinematography = Steven Fierberg
| editing        = Greg DAuria
| distributor    = Seedsman Group
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Meet Market is a 2008 film directed by Charlie Loventhal and starring Alan Tudyk, Krista Allen, Elizabeth Berkley, Laurie Holden  and Julian McMahon. The movie is a comedy about singles in Los Angeles who attempt to find love in the aisles of a supermarket. The films was released directly to DVD on February 12, 2008.

==Cast==
* Krista Allen as Lucinda
* Elizabeth Berkley as Linda
* Susan Egan as Tess
* Suzanne Krull as Lima Lips
* Julian McMahon as Hutch
* Missi Pyle as Ericka
* Jennifer Sky as Courtney
* Alan Tudyk as Danny
* Aisha Tyler as Jane
* Robert Trebor as Director Dick
* Jack Kenny as Manager Dick
* Christine Estabrook as Mom
* Laurie Holden as Billy

== External links ==
*  

 
 
 
 
 


 