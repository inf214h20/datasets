The Dragon Pearl
{{multiple issues|
 
 
}}
 

{{Infobox film
| name           = The Dragon Pearl
| image          = DragonPearl2011Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       =  Mario Andreacchio
| producer       = Liu Zhi Jiang Mario Andreacchio Mark Patterson Shan Tam  Pauline Chan William W. Wilson III
| screenplay     = Philip Dalkin
| story          = Ron Saunders John Armstrong Mario Andreacchio
| starring       = Sam Neill Wang Ji Robert Mammone Jordan Chan Li Lin Jin Louis Corbett
| music          = Frank Strangio
| editing        = Suresh Ayyar
| country        = Australia   China
| runtime        = 93 minutes
| released       =  
| budget         = 
| gross          =
}}
The Dragon Pearl is a 2011 family film that follows the story of two teenagers who meet in China to encounter a real live Chinese dragon, and also discover the mystery behind the whereabouts of his all powerful pearl.

Directed by Mario Andreacchio, the film stars Wang Ji, Sam Neill, Robert Mammone, Jordan Chan and young stars Li Lin Jin and Louis Corbett. The screenplay was written by Philip Dalkin, based on the original script by John Armstrong, and the story is by Ron Saunders, John Armstrong and Andreacchio. AMPCO Films, "Production Notes- The Dragon Pearl" 2011 

==Plot==
When teenagers Josh (Louis Corbett) and Ling (Li Lin Jin) join their respective parents, Chris ( ), who wants to seize the pearlʼs awesome power for his own sinister ambitions. The only way to stop him is for Josh and Ling to get to it ﬁrst and return it to its rightful owner. 

==Cast==
* Sam Neill as Chris Chase, a renowned archeologist who has been invited to participate on a special archeological dig site in China. 
* Wang Ji as Dr Li, the lead scientist on the excavation site. Tough but fair, Dr Li takes a no-nonsense approach to her work, but is patient and encouraging of all those around her. This earns her the love of those working at the site and from her own daughter Ling (Li Lin Jin). 
* Jordan Chan as Wu Dong, a quirky keeper of an ancient temple that has inherited his responsibilities from his family. 
* Robert Mammone as Philip Dukas. 
* Li Lin Jin as Ling the daughter of the lead scientist on the excavation Dr Li (Wang Ji). 
* Louis Corbett as Josh Chase, the son of Dr Chris Chase (Sam Neill), who is on vacation in China visiting the archeological excavation site his father is working on. 

==Chinese dragons==
Western dragons are creatures that breathe fire,  . The Chinese dragon is completely the opposite. Chinese dragons are benevolent creatures, give the emperors their power, and are representative of all the forces of nature. In most presentations of the Chinese dragon, it is shown pursuing the elusive pearl, the symbol of all power and knowledge. 
 CGI dragon by Western artists in consultation with Chinese advisors. The dragon was produced by Rising Sun Pictures, whose work spans the major blockbusters like Harry Potter movies, Lord of The Rings and The Green Lantern. 

==Production summary==

===Co-production===

The Dragon Pearl is the ﬁrst treaty co-production between Australia and China. A Treaty Co-production is where 2 producers from 2 countries, bound by international law, agree to a cultural, creative and ﬁnancial association to produce a ﬁlm together. The ﬁnal production is regarded simultaneously a full Chinese ﬁlm as well as a full Australian ﬁlm. 

===Locations===

The ﬁlm was shot entirely in China at the Hengdian World Studios, reputedly the largest studio complex in the world with over 3 million square metres of built sets. Also other locations in and around Hengdian were used. Hengdian is located around 4 hours drive south of Shanghai in the province of Zhejiang. All post production was conducted in Adelaide, South Australia. CGI and visual effects were created by two Adelaide based companies, Rising Sun Pictures and Convergen. 

==References==
 
 

==External links==
*  
*  

 
 
 
 
 