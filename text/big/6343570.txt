Return to House on Haunted Hill
{{Infobox Film name               = Return to House on Haunted Hill  image              = Return To House.jpg director           = Victor Garcia (Spanish director)|Víctor García writer             = William Massa starring  Tom Riley Andrew Lee Potts Jeffrey Combs music              = Frederik Wiedmann editing            = Robert Malina cinematography     = Lorenzo Senatore producer           = Joel Silver  Robert Zemeckis studio             = Warner Premiere Dark Castle Entertainment distributor        = Warner Home Video  released         = October 16, 2007 runtime           = 79 minutes 81 minutes (unrated version) country            = United States  language           = English 
}}
 House on idol hidden psychiatric asylum. Nearly everyone is killed during the search by supernatural forces, which seem to be connected to the idol.

==Plot==
Ariel Wolfe (Amanda Righetti) is the sister of Sara Wolfe (Ali Larter), a survivor of a publicity/birthday event hosted by Steven Price eight years ago in the Vannacutt Psychiatric Institute for the Criminally Insane. In the 1920s, the asylum was overseen by the sadistic psychiatrist Dr. Richard B. Vannacutt (Jeffrey Combs). Sara claimed that spirits of the house killed Price and the party guests, and that she barely escaped with her life.  But no one believed her.  She later commits suicide and a bereaved Ariel tries to find out why.

Ariel discovers a diary written by Dr. Vannacutt that reveals the history of the Institute. She and her friend Paul (  allegedly located inside the old Vannacutt Psychiatric Institute, a figurine of the demon Baphomet. While Desmonds henchman Samuel (Andrew Pleavin) holds Paul captive outside the asylum, Ariel, Desmond, and four of Desmonds accomplices enter the building. Once inside, they encounter Dr. Richard Hammer (Steven Pacey). Dr. Hammer has been lured into the asylum by his assistants Kyle (Andrew-Lee Potts) and Michelle (Cerina Vincent). Desmond is one of Dr. Hammers former students, and Michelle is Desmonds lover. Michelle seduced Dr. Hammer to learn what he knew about the sanatorium and the Baphomet idol, and then lured Hammer and Kyle into the asylum so they could help search for the statue.
 traumatizes her by showing Ariel the depravity he and the other inmates suffered under Dr. Vannacutt. These images reveal that Vannacutt was driven mad by the idol, and later performed experiments on the mentally ill. The dead inmate led a revolt against Vannacutt, during which the sanatorium burned down. (The audience is shown footage from the 1999 film House on Haunted Hill, which depicted these events.) The deaths of characters in the previous film were assumed to be caused by the spirits of dead inmates seeking vengeance. But now Ariel is shown that the dead are actually forced by the idol to do Vannacutts bidding and did not willingly kill. Ariel falls unconscious. She wakes to find herself in a straitjacket, and screams. Her cries lead Desmond, Dr. Hammer, and Michelle to her cell and they rescue her.

In the asylums entrance hall, Norris, who has a vision about a patient being dismembered too, is dismembered by ghosts. Ariel, Michelle, Desmond, and Dr. Hammer hear Kyle scream and rush to the entrance hall. Although the 12 hours are up, the master locking mechanism begins to lock the house down again. Ariel escapes before the house is sealed again. But she discovers that Samuel and Paul have entered into the house to look for her. The main door opens as if to invite her in, and she goes back inside. In the entrance hall, Samuel swears he heard Desmond tell them to enter the building. While Desmond and the others discuss the situation, Samuel is lured away by a shadow and killed by the ghost of Dr. Vannacutt. In the ensuing chaos, Paul, Kyle, and Dr. Hammer subdue and disarm Desmond and Michelle.

Ariel convinces the group to search for an exit through the sewer. The group first goes through the hydrotherapy room. Desmond knocks Kyle into the water and flees with Michelle. Ariel tries to save Kyle, but he is dragged to the bottom of the pool by a ghost and slain, and one of the ghosts gives Ariel a vision about the patient being thrown into the water and drowned while struggling to get to the chain. Dr. Hammer and Paul pull Ariel from the water and save her life. Elsewhere, Desmond and Michelle argue over whether to keep looking for the idol. Convinced Michelle wants the idol for herself, Desmond attempts to kill her. Michelle flees but is killed by Vannacutt. Desmond resumes the search alone. Ariel, Paul, and Dr. Hammer discover a grate leading to the sewer and a way out of the asylum, but it is blocked by iron bars too narrow for a person to fit through. The ghost of the rebellious inmate gives Ariel another vision, showing her that the idol is in a hidden chamber behind an oven in the asylums basement Crematory|crematorium.

Ariel, Paul, and Dr. Hammer descend to the crematorium, locate the correct oven, and discover a secret exit at the ovens rear. They walk down a corridor and discover the "heart of the house" (composed of living flesh).  Ariel tries to destroy the idol with her handgun but it is indestructible. She removes the idol, reasoning that if it is flushed down the sewer and leaves the building all the spirits will be freed. The team travels back up the corridor and out of the oven, but are ambushed by Desmond. With the idol threatened, the spirits begin attacking. Desmond is seized by ghosts and pushed into a furnace where he is burned alive after he has a vision about the patients being sick and also one of them is being sent to the oven to be burned alive. Paul avoids death by fleeing into the oven and back down into the heart of the house. Ariel and Dr. Hammer rush to the sewer grate to flush the idol before Paul dies. But Dr. Hammer is overcome by the idols evil and tries to strangle Ariel. They fight, and Ariel encourages Hammer to resist the idols influence. While Ariel and Hammer are fighting, the ghost of Vannacutt and inmates appeared, but Vannacutt ordered the inmates to halt and watch Ariel and Hammer fighting, hoping one of them will die. Later on Hammer recovers his senses, but the ghost of Dr. Vannacutt appears and begins killing him.  Ariel seizes the idol and throws it through the grate and into the sewer. The spirits begin vanishing (saving Pauls life at the last moment).  Several spirits attack Dr. Vannacutt, tearing him apart.  With Vannacutt no longer controlling the locking mechanism, the building comes unsealed. Ariel and Paul leave.

In a post-credits scene, a man and woman are shown about to make love on a beach. The woman feels something under the sand beneath her. They dig, and pull the Baphomet idol into the light.

==Cast==
* Amanda Righetti - Ariel Wolfe
* Cerina Vincent - Michelle
* Erik Palladino - Desmond Niles Tom Riley - Paul
* Andrew Lee Potts - Kylerick "Kyle"
* Jeffrey Combs - Dr. B. Vannacutt
* Steven Pacey - Dr. Richard Hammer
* Kalita Rainford - Harue
* Gil Kolirin - Norris Boz
* Andrew Pleavin - Samuel
* Chucky Venice - Warren Jackson
* Stilyana Mitkova - Sara Wolfe/Ghost
* Ali Larter - Sara Wolfe (flashback/deleted scene)
* Taye Diggs - Eddie Baker (flashback/deleted scene)

==Production==
Dark Castle Entertainment announced it would produce a sequel to The House on Haunted Hill in August 2006, and said it had cast Amanda Righetti in the lead at the same time.   Variety (magazine)|Variety. August 30, 2006.  In June 2007, Warner Bros. agreed to co-fund the sequel under its Warner Premiere brand, a subdivision of the studio that focuses on direct-to-DVD releases and other digital media.   Variety. June 19, 2007.  The pictured was filmed using high-definition digital media, and the script and shots were designed for use with the Navigational Cinema technology (which permits viewers to manipulate up to seven aspects of the story line to create more than 90 different versions of the film).    E!Online.com.  October 17, 2007.  Actor Jeffrey Combs said that the script did not contain the "navigational branching" scenes, and director Victor Garcia admitted that these script changes did not arrive until the start of principal photography. 

Return to House on Haunted Hill was the feature film directorial debut for Victor Garcia, who had previously helmed a single short film.  It was also the first feature film screenplay from writer William Massa.  Filming occurred in Sofia, Bulgaria. 

==Reception, releases and sequel==
In June 2007, Warners and Dark Castle announced an October 2007 release for the DVD.  It was released October 16, 2007.     In the United States, Warner Premiere released an unrated version with the Navigational Cinema technology, as well as an Motion Picture Association of America film rating system|R-rated version that did not. 

The unrated U.S. release did not contain any interviews, commentaries, or "making of" featurettes, but did include four deleted scenes; a music video for the Mushroomhead song "Simple Survival" (featured on the films soundtrack); and about 20 minutes of in-character interviews with the leads, which recapped the films plot or provided limited backstory.   Many of the Navigational Cinema features led to scene choices which included more nudity or gore, but only one choice materially changed the outcome of the story.  An additional 60 minutes of video were shot to incorporate these choices. 

The film did not receive a positive critical reception. A review in  . December 17, 2007.  However, some reviews felt the viewer ability to select alternate plots was intriguing and fun.  

A third installment in the series had been planned, but poor DVD sales for Return to House on Haunted Hill led Dark Castle to cancel these plans in October 2010. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 