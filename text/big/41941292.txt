Crash Drive
{{Infobox film
| name           = Crash Drive
| image          = "Crash_Drive"_(1959).jpg
| image_size     =
| caption        = UK theatrical poster
| director       = Max Varnel 
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         = Brian Clemens Eldon Howard
| narrator       = Wendy Williams
| music          =  James Wilson
| editing        = Lee Doig
| studio         = Danziger Productions
| distributor    = United Artists
| released       = 1959
| runtime        = 65 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Crash Drive is a 1959 British racing car film starring Dermot Walsh, made by prolific independent producers, the Danzigers.  

==Plot==
Paul Dixon is an international racing driver severely depressed after being paralyzed from the waist down in a crash. He seems to have lost everything, including his will to live. His estranged wife Ann returns to him in the wake of the accident and attempts to cure him of his despair. 

==Cast==
*Paul Dixon -	Dermot Walsh Wendy Williams Ian Fleming
*Tomson -	Anton Rodgers
*Mrs. Dixon -	Grace Arnold
*Nurse Phillips - Ann Sears
*Manotti -	George Roderick
*Forbes -	Garard Green
*Henry -	Geoffrey Hibbert

==Critical reception==
Sky Movies wrote, "this very minor, modest and mostly mediocre British melodrama - partly written by The Avengers (TV series)|The Avengers producer Brian Clemens - has a hard job getting into gear."  

==References==
 

==External links==
*  at IMDB

 
 


 
 