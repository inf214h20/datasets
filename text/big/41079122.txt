7 Notes to Infinity
; 
{{Infobox film
| name     = 7 Notes to Infinity
| image    = 7_notes_to_Infinity.jpg
| caption  = 7 Notes to Infinity Cover
| director = Shrenik Rao
| released =  
| Series   = 106 minutes
| country  = India
| language = English
| budget   =
}}
7 Notes to Infinity is a 2012 documentary film directed by Shrenik Rao. It is a musical documentary which pays tribute to Indian classical music and explores the universality of music through infinite musical compositions created from seven pitches.  

==Synopsis==
The film explains the context in which Indian classical music evolved, and illustrates the role of the great king Maharaja Swathi Thirunal Rama Varma. Legendary musicians, the likes of Dr M. Balamuralikrishna, Padma Bhushan, T. V. Gopalakrishnan, Prince of the Travancore Royal Family- Aswathi Thirunal Rama Varma, the direct descendant of the great king Maharaja Swathi Thirunal, Sanjay Subrahmanyan and a group of young and enthusiastic music students feature prominently in the film. 

With unprecedented access, 7 Notes to Infinity was shot extensively in Padmanabhapuram Palace, in Kanyakumari district, the famous Padmanabhaswamy Temple, Kuthira Malika and Kowdiar Palace in Thiruvananthapuram.The documentary also presents Swati Tirunals compositions in the Hindustani and Carnatic streams of music. 

==Release==
The film was launched on the World Music Day in Kerala with the support of Saptaparani and State Bank of Travancore in June 2012.  

==References==
 

==External links==
* http://www.7notestoinfinity.com/ Official Website
* https://www.facebook.com/7notestoinfinity
*  
* http://www.7notestoinfinity.com/press_kit.html Press release
* https://vimeo.com/ondemand/7notestoinfinity/66902976
* http://www.youtube.com/watch?v=uvFw9ufNmE4

 
 
 
 
 
 
 