The Disappointments Room
{{Infobox film
| name           = The Disappointments Room
| image          =
| caption        =
| director       = D.J. Caruso
| producer       = Geyer Kosinski Robbie Brenner
| writer         = Wentworth Miller D.J. Caruso
| starring       = Kate Beckinsale Lucas Till
| cinematography =  
| editing        = 
| music          = 
| studio         = Relativity Media Demarest Films Media Talent Group
| distributor    = Relativity Media
| released       =  
| country        = United States
| language       = English
| budget         = 
}}
The Disappointments Room is an upcoming American Horror film|horror-psychological thriller film. D.J. Caruso will direct a script penned by both him and Wentworth Miller. Kate Beckinsale and Lucas Till are the leading stars of the film. The film will be released on September 25, 2015. 

==Cast==
*Kate Beckinsale as Dana
*Lucas Till as Ben
*Gerald McRaney 
*Mel Raido as David

==Production==

===Filming=== English Tudor Luther Lashmit, at Sedgefield Country Club outside Greensboro. 

==Release==
Relativity has established that the film will be released on September 25, 2015. 

==References==
 

 
 
 
 
 
 