Pirate Treasure
 

{{Infobox Film
| name           = Pirate Treasure
| image_size     = 
| image	=	Pirate Treasure FilmPoster.jpeg
| caption        =  Ray Taylor
| producer       = Henry MacRae Ella ONeill Jack Nelson Basil Dickey
| narrator       =  Walter Miller William Desmond
| music          = Heinz Eric Roemheld Richard Fryer
| editing        = Saul A. Goodkind
| distributor    = Universal Pictures 1934
| runtime        = 12 chapters (240 min)
| country        =   English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial film serial. swashbuckling genre in the film serial medium.  
 Ray Taylor Walter Miller (as the hero, love interest and villain respectively).  The serial is especially praised for the stunt work of Talmadge.

==Plot==
Aviator Dick Moreland uses his winnings from a recent flight to fund an expedition to recover treasure buried his pirate ancestor.  However, Stanley Brasset, another member of Morelands club, steals his map and sets out to find the treasure for himself.  Dorothy Craig becomes involved when Dick needs her car to chase Brassets henchmen and recover the map, which results in Dorothy being kidnapped and requiring rescue by Dick.  When told of the treasure, Dorothy offers her fathers yacht to take them to the island.  Unable to retain the map, Brasset joins the expedition (his identity as the villain unknown to the protagonists) with henchmen hidden aboard.  The henchmen are discovered and attempt to take over the ship en route to the Caribbean but this fails.  Brasset releases them again after arrival to stop Dick from recovering the treasure.  The treasure chest itself is empty and the search by the two parties continues on the island. Island natives eventually capture Brasset and his henchmen and plan to sacrifice them.  Dick intervenes and they are brought back to America as prisoners.

==Cast==
*Richard Talmadge as Dick Moreland, aviator and adventurer
*Lucille Lund as Dorothy Craig Walter Miller as Stanley Brasset, villain
*Patrick H. OMalley, Jr. as John Craig, Dorothys father
*Ethan Laidlaw as Curt, One of Brassets henchmen William Desmond as Captain Jim Carson
*William L. Thorne as Drake
*Del Lawrence as Robert Moreland

==Production==
Pirate Treasure is a rare swashbuckling serial and the best example of the type. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 28–29
 | chapter = 3. The Six Faces of Adventure
 }} 

==Critical reception==
Due to its eerie background and the stunt work of Richard Talmadge, Cline considers Pirate Treasure the most memorable of the costume serials.   Hans J. Wollstein at Allmovie does not consider the acting or writing to be of a high standard but praises the stunts: "Talmadges acrobatics are as exciting today as they must have been in 1934."  Wollstein especially highlights the stunt in chapter three (Wheels of Fate) in which Talmadge falls between awnings from the top of a building.

==Chapter titles==
# Stolen Treasure
# The Death Plunge
# The Wheels of Fate
# The Sea Chase
# Into the Depths
# The Death Crash
# Crashing Doom
# Mutiny
# Hidden Gold
# The Fight for the Treasure
# The Fatal Plunge
# Captured
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 209
 | chapter = Filmography
 }} 

==Cliffhangers==
===Cliffhangers===
#Stolen Treasure: The speeding car carrying Dick goes over a cliff into the sea.
#Death Plunge: Dick is thrown from the roof of a tall building by Brassets henchmen.
#Wheels of Fate: Dick, on a motorbike, heads towards a narrow bridge blocked by the henchmens truck.
#Sea Chase: The motorboat carrying Dick & Dorothy collides with one piltoed by Brassets henchment.
#Into the Depths: Dick falls from the ships rigging into the sea.
#The Death Crash: Dick and henchmen go over a cliff in a speeding car.
#Crashing Doom: Dick falls to the deck of the ship during a loading accident.
#Mutiny: Dick enters the burning cargo hold.
#Hidden Gold: Dick is attacked by the henchmen and falls over a cliff.
#Fight for the Treasure: Dick and Dorothy are caught in an explosion rigged by Brasset.
#Fatal Plunge: Dick is attacked by crocodiles while fighting in a swamp.

===Resolutions===
#Death Plunge: Dick swims back to land.
#Wheels of Fate: Dicks fall is broken by a series of awnings.
#Sea Chase: Dick drives over the side of the bridge, landing on a train below.
#Into the Depths: Dick & Dorothy rescued by Dorothys father.
#The Death Crash: Dick survives the fall.
#Crashing Doom: Dick survives the fall.
#Mutiny: Dicks fall is broken by sacks of flour.
#Hidden Gold: The crew extinguish the fire.
#Fight for the Treasure: Dick survives the fall.
#Fatal Plunge: Dick and Dorothy survive but are trapped in the cave.
#Captured: Dick survives the crocodiles.

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 
*  (by individual chapter)

 
{{succession box  Universal Serial Serial  The Perils of Pauline (1933 in film|1933)
| years=Pirate Treasure (1934 in film|1934)
| after=The Vanishing Shadow (1934 in film|1934)}}
 

 

 
 
 
 
 
 
 
 