Background (1953 film)
{{Infobox film
| name           = Background
| image          = Background1953.jpg
| caption        = 
| director       = Daniel Birt
| producer       = Herbert Mason
| writer         = Warren Chetham-Strode Don Sharp
| starring       = Valerie Hobson Philip Friend Norman Wooland Arthur Grant
| music          =  ABPC
| released       = 3 November 1953 (UK) 1 July 1954 (U.S.)
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
}}

Background (U.S. Edge of Divorce) is a 1953 British domestic drama film dealing with the effects of divorce, directed by Daniel Birt and starring Valerie Hobson, Philip Friend and Norman Wooland.  It was based on a stage play by Warren Chetham-Strode, who also wrote the screenplay for the film.

A contemporary review in the Glasgow Herald gave the film a muted response, describing Hobson as "shrill" and Wooland as "too sympathetic", adding : " A heroic effort is made to apportion the blame fairly...yet intrinsically, one has to admit, the film has no great success." 

==Plot==
John and Barbie Lomax (Friend and Hobson) have been married for almost 20 years, but the marriage has seemingly reached breaking point.  After leaving the army, John has been working hard on making a career for himself as a barrister, which takes up all of his time and attention, leaving him exhausted and irritable.  He acts intolerantly and dismissively towards Barbie and their three children, and the marital relationship comes under intolerable strain as the couple argue, bicker and snipe constantly at each other.  Realising that the poisoned atmosphere is not good for the children to experience, they agree that in the circumstances divorce is the lesser evil.

They are unprepared for how badly the children react when they break the news.  The children jump to the conclusion that family friend Uncle Bill Ogden (Woolard) is to blame, assuming from what they have seen that he has designs on Barbie.  While this is true, it does not explain the depth of unhappiness felt by both their parents at their increasingly acrimonious relationship.

As the wheels of the divorce are set in motion, John and Barbie are faced with coming to agreement about what should happen to the children, whether all should be given to the custody of one parent, or whether they should be split up.  Caught in the middle, the children take matters into their own hands, forcing their parents to reassess the wisdom of the path they are about to take.  Finally they are forced into an about-face after realising the destructive effect of divorce on the children.  They look again at their relationship and see that they are still in love with each other and both have been partly to blame for the breakdown in communication between them.  They decide not to go any further with the divorce, and resolve in future to work with each other rather than carping and criticising.

==Cast==
 
 
* Valerie Hobson as Barbie Lomax
* Philip Friend as John Lomax
* Norman Wooland as Bill Ogden
* Janette Scott as Jess Lomax
* Mandy Miller as Linda Lomax
* Jeremy Spenser as Adrian Lomax
* Lily Kann as Brownie
* Helen Shingler as Mary Wallace
 
* Thora Hird as Mrs. Humphries
* Louise Hampton as Miss Russell
* Jack Melford as Mackay
* Richard Wattis as David Wallace
* Joss Ambler as Judge
* Lloyd Lamble Defence Counsel
* Barbara Hicks as Mrs. Young
* Ernest Butcher as Clerk 
 

==References==
 

== External links ==
*  
*   at BFI Film & TV Database
*  

 
 
 
 
 
 
 
 
 