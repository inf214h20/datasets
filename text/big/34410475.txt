Obliging Young Lady
{{Infobox film
| name           = Obliging Young Lady
| image          = 
| image size     = 
| caption        =  Richard Wallace
| producer       = Howard Benedict
| writer         = 
| starring       = 
| music          = 
| cinematography = Nicholas Musuraca
| editing        = Henry Berman
| distributor    = RKO Radio Pictures
| released       = 30 January 1942
| runtime        = 80 min.
| country        = United States
| language       = English
}}
 Richard Wallace.  

==Plot==
On the instructions of their lawyer, the wealthy young daughter of divorcing parents (Joan Carroll) is removed to a mountain resort, complete with a decoy mother, to protect her from the publicity.  The situation is immediately complicated by persistent reporters, a romantic interest for the fake mother, and a convention of birdwatchers.

== Cast ==
 
* Joan Carroll as Bridget Potter
* Edmond OBrien as "Red" Reddy
* Ruth Warrick as Linda Norton
* Eve Arden as "Space" OShea Robert Smith as Charles Baker
* Franklin Pangborn as Professor Gibney
* Marjorie Gateson as Mira Potter
* John Miljan as George Potter
* George Cleveland as Lodge Manager
* Luis Alberni as Riccardi Charles Lane as Detective
* Fortunio Bonanova as Chef
* Andrew Tombes as Conductor
* Almira Sessions as Maid
* Pierre Watkin as Markham
* Florence Gill as Miss Hollyrod
* Sidney Blackmer as Attorney
* Virginia Engels as Bonnie
* George Watts as Judge Knox
* Jed Prouty as Judge Rufus
* George Chandler as Skip
 

==Reception==
The film lost $118,000 at the box office. 

Nonetheless, the film is memorable for its funny tribute to a well-known baseball player of the time when Edmond OBrien keeps repeating "Heinie Manush" in cadence with the sound of the train hes riding. In the dining car he even orders, "Heinie Manush, Heinie Manush; Filet mignon, medium rare; Heinie Manush, Heinie Manush." The repetitive nature of the phrase fits the sound of the train so well, its picked up by the other riders, driving the conductor to distraction.

==References==
 

== External links ==

*  
*  

 

 
 
 
 
 
 
 

 