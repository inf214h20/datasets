Jayantabhai Ki Luv Story
{{Infobox film
| name           = Jayanta Bhai Ki Luv Story
| image          = Jayanta Bhai.jpg
| caption        = Theatrical release poster
| director       = Vinnil Markan
| producer       = Kumar S. Taurani
| screenplay     = Kiran Kotrial
| starring       = Vivek Oberoi Neha Sharma
| music          = Sachin - Jigar Background Score: Raju Singh
| cinematography = Santosh Thundiyil
| casting Director = Bhavna & Sumit Attri
| editing        = Manish More
| distributor    = Tips Music Films
| released       =  
| country        = India.
| language       = Hindi. Eng Subtitles – Shivkumar Parthasarathy
| English Subtitles       = Shivkumar Parthasarathy
| budget         = 
| gross          = 
}} romance comedy Kumar Taurani My Gangster Lover (2010).

==Cast==
* Vivek Oberoi as Abhimanyu (Jayanta Bhai)
* Neha Sharma as Simran (Bhadotri)
* Nassar as Don Zakir Hussain as Altafbhai
* Raj Singh Arora as Kunal (Kunu) Rahul Singh as Datta

==Release==
The film was expected to release sometime during Summer 2012. In July 2012, the films posters were released as a promotion for the film, in which it was also announced that the film has been scheduled to release in October 2012. The films distributors Tips Music Films released an song promo of the film, "Aa Bhi Ja Mere Mehermann" by Atif Aslam, in November 2012.

==Soundtrack==

{{Infobox album  
| Name        = Jayantabhai Ki Luv Story
| Type        = Soundtrack
| Artist      = Sachin - Jigar
| Cover       = 
| Released    = 
| Recorded    = Feature film soundtrack
| Length      =  Tips
| Producer    = 
| Reviews     =
| Last album  =   (2013)|
| This album  = Jayantabhai Ki Luv Story (2013)|
| Next album  = I, Me Aur Main (2013)|
}}

The music of the film will be directed by Sachin - Jigar while the lyrics are penned by Priya Panchal & Mayur Puri. Atif Aslam has sung 3 songs in the film.

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| Track # || Song || Lyrics || Singer(s) || Length
|-
| 1
| "Aa Bhi Ja Mere Mehermaan"
| Priya Panchaal
| Atif Aslam
| 4:45
|-
| 2
| "Thoda Thoda"
| Mayur Puri, Priya Panchaal
| Shreya Ghoshal, Sachin Sanghvi
| 4:34
|-
| 3
| "Dil Na Jaane Kyun"
| Priya Panchaal
| Atif Aslam, Anushka Manchanda
| 3:50
|-
| 4
| "Hai Na"
| Mayur Puri
| Atif Aslam, Priya Panchal
| 3:42
|}

==References==
 

==External links==
* 
* . timesofap.com

 
 
 
 
 