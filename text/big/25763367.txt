Suryakaanti
{{Infobox film
| name           = Suryakanthi
| image          =
| caption        =
| director       = K.M. Chaitanya
| producer       = M Vasu and Sujatha
| writer         = K Y Narayanaswamy|K.Y. Narayanaswamy
| starring       = Chetan Kumar, Nassar, Regina Cassandra
| music          = Illayaraja
| cinematography = K.C Venu
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Kannada
| budget         =
| gross          =
}}
Suryakanthi is a 2010 Indian Kannada language movie directed by K.M. Chaitanya of Aa Dinagalu fame. It stars Chetan Kumar, Regina Cassandra, Nassar. The music is scored by  maestro Illayaraja. 

==Plot==
The film is a story of an international assassin played by Chetan.

==Cast==
* Chetan Kumar
* Regina Cassandra
* Nasser
* Kishori Ballal
* Ganesh Yadav
* Sangeetha Gopal (Cameo)

==Soundtrack==
{{Track listing
| collapsed =
| headline =
| extra_column = Singers
| total_length =
| all_writing =
| all_lyrics =
| all_music =	Ilaiyaraaja
| writing_credits =
| lyrics_credits =
| music_credits =
| title1 =	Swalpa Soundu
| note1 =
| writer1 =
| lyrics1 =
| music1 =
| extra1 =	Ilaiyaraja, Anitha, Roshini, Suvi Suresh|Suvvi, Reshma, Neha
| length1 =
| title2 =	Chan Channare
| note2 =
| writer2 =
| lyrics2 =
| music2 =
| extra2 =	Shreya Goshal
| length2 =
| title3 =	Edeya Baagilu
| note3 =
| writer3 =
| lyrics3 =
| music3 =
| extra3 =	Kunal Ganjawala, Shreya Goshal
| length3 =
| title4 =	Mouni Naanu
| note4 =
| writer4 =
| lyrics4 =
| music4 = Karthik
| length4 =
| title5 =	Jaikaara Haakona
| note5 =
| writer5 =
| lyrics5 =
| music5 =
| extra5 =	Tippu (singer)|Tippu, Roshini
| length5 =
| title6 =	Mouni Naanu (Pathos)
| note6 =
| writer6 =
| lyrics6 =
| music6 = Karthik
| length6 =
}}

==References==
 

 
 
 
 


 