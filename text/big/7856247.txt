Cyborg 3: The Recycler
{{Infobox film
| name           = Cyborg 3: The Recycler
| image          = Cyborg3dvd.jpg
| caption        = Region 2 DVD cover
| director       = Michael Schroeder 
| producer       = Diane Mehrez Gary Jude Burkart Steve Rockmael Alexander Tabrizi
| writer         = Barry Victor Troy Bolotnick Straw Weisman
| starring       = Khrystyne Haje Zach Galligan Malcolm McDowell Michael Bailey Smith Rebecca Ferratti
| music          = Kim Bullard Julian Raymond
| music department = Lionel Felix
| cinematography = Phil Parmet
| editing        = Barry Zetlin
| distributor    = Prism Leisure Corporation Warner Vision Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}}
Cyborg 3: The Recycler is the 1995 direct-to-video sequel to Cyborg 2 starring Malcolm McDowell and Khrystyne Haje. Released on home video in 1995, the film is directed by Michael Schroeder.

==Plot==
Set in a desolate post apocalyptic world where a once thriving golden age of man and cyborgs has ended. Cyborgs are now hunted for their parts. Cash (Haje), a female cyborg learns from Doc Edford (Margaret Avery) that she is somehow pregnant.
 Richard Lynch) and his assistant Jocko (Andrew Bryniarski). Lewellyn makes a living hunting cyborgs for their parts. Though he has long wanted to find Cytown (the last haven for cyborgs), he becomes obsessed in getting Cash and her child.

==Cast==
*Malcolm McDowell as Lord Talon
*Khrystyne Haje as Casella Cash Reese
*Zach Galligan as Evans Richard Lynch as Anton Lewellyn
*Andrew Bryniarski as Jocko
*Michael Bailey Smith as Donovan
*William Katt as Decaf
*Rebecca Ferratti as Elexia
*Margaret Avery as Doc Edford
*Raye Hollitt as Finola
*Kato Kaelin as Beggar
*Evan Lurie as El Sid
*Bill Quinn as Hale
*David McSwain as Ahab

==External links==
*  
*  
*  
*    The New York Times

 
 
 
 
 
 
 
 
 


 
 