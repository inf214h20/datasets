Kabaddi (2009 film)
{{Infobox film
| name           = Kabaddi

| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Narendra Babu

| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Praveen Priyanka Kishore Dharma Avinash Sriraksha

| narrator       = 
| music          = Hamsalekha

| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2009 
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          =  
}} Kannada sports Kishore played the role of a passionate sports coach. The directorial debut film, is a love story based around the sport of Kabaddi. Though it didnt perform well at the box office, critics named it one of the top Kannada films of 2009.  {{cite web | title = Movie Review : Kabaddi | url = http://www.sify.com/movies/kabaddi-review-kannada-14897973.html 
|date=2009| accessdate = 2014-08-30  }} 
 Best First Film.

==Cast==
* Praveen
* Priyanka Kishore as Beeresh coach

==Reviews==
This movie got mostly positive reviews from the media. 

Kannada film Kabaddi has been adjudged the best film for 2008-09 by the Karnataka government appointed committee.

Kabaddi also won an award for best Dialogues for 2008-09 by the Karnataka government appointed committee.

==Soundtrack==
{{Infobox album  
| Name        = Kabaddi
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       =
| Released    = 2009
| Recorded    =
| Genre       = Film Soundtrack
| Length      =
| Label       = Akshaya Audio
| Producer    = G K Ravi, K Raju, G Kishor Kumar, Anup Gowda, N Asha, D V Rajendra Prasad 
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}

{| class="wikitable"
|-
! Song title !! Singers !!
|- Jhallanta Kaadutaalivalu || Hariharan ||
|- Toltede || Shankar Mahadevan, Lakshmi Narayan ||
|- Mutte Mutte || Chitra KS, Karthik ||
|- Bombe Maanava || Kavita Seth, Harsha ||
|- Preethi Maadiro || Shreya Ghoshal, Kunal Ganjawala ||
|}

==Box office==
It was a box office dud when released in spite of getting positive reviews from media and film critics.

==References==
 

 
 
 
 
 
 
 
 


 