A Taste of Evil
{{Infobox film
| name           = A Taste of Evil
| image          =
| image_size     =
| caption        =
| director       = John Llewellyn Moxey
| producer       = Aaron Spelling
| writer         = Jimmy Sangster
| starring       = Barbara Stanwyck Barbara Parkins Roddy McDowall
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 1971 American television film. Directed by John Llewellyn Moxey, it stars Barbara Stanwyck, Barbara Parkins and Roddy McDowall.

==Plot==  William Windom), an Alcoholism|alcoholic. Susan returns to the woods where she was raped, and is followed there by someone. She suspects that this person is Harold, but Miriam tells her that Harold left her the previous night.

One night Susan sees a shadow by her window, then finds a dead man in her bathtub.  She faints, regaining consciousness later in the presence of Dr. Michael Lomas (Roddy McDowall).  There is no trace of a body, and it seems clear to Susan that someone is trying to drive her insane.  Miriam believes that Susan is hallucinating, and begins to doubt the wisdom of Susans release from the hospital.  On a different night, Susan is chased out of the mansion by someone she thinks is Harold, and outside finds a dead body in the car.  She turns to John (Arthur OConnell) - an old family friend and the gardener - for help, only to find the car is missing when she returns.

After telling Dr. Lomas about her experiences, he concludes that her subconscious is rejecting Harold. Regardless, Susan begins to believe that Harold was the one who raped her. The same day that Harold returns to the mansion, someone from the woods calls out to Susan. She goes into the woods and finds the same dead man, who grabs her leg. She flees and hides in the cabin where she was raped.  Harold arrives shortly afterward; he is later found dead, apparently shot by Susan.  Miriam is told by the authorities that Susan will be returned to the sanatorium in Switzerland.  It is then revealed that Miriam has been conspiring with John to subvert Susans recovery, in order to disinherit her from her fathers will.  Miriam admits her lifelong jealousy of Susan, because Susans father gave her more attention than he gave Miriam.  It was John who raped Susan when she was 13.

A stalker targets Miriam, appearing to be Harold.  She assumes that John is terrorizing her, and she fires him.  The stalking continues unabated.  One rainy night, Miriam catches sight of the stalker, who does appear to be Harold.  John arrives shortly afterward, and Miriam shoots him dead.  She tries to leave the mansion, only to find Harold in the doorway.  Driven to hysteria, she screams that she "made Susan kill" him.  In the wake of this confession, the "stalking" is revealed as a counter-conspiracy masterminded by Dr. Lomas.  Harold is alive, his "shooting" a ruse to lure Miriam into a confession.  Miriam coldly allows herself to be arrested.

==Cast==
*Barbara Stanwyck as Miriam Jennings
*Barbara Parkins as Susan Wilcox
*Roddy McDowall as Dr. Michael Lomas William Windom as Harold Jennings
*Arthur OConnell as John
*Bing Russell as Sheriff
*Dawn Frame as Young Susan

==External links==

* 
* 
* 

 

 

 
 
 
 
 