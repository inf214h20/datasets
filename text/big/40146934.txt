Kung Fu Divas
{{Infobox film
| name             = Kung Fu Divas
| image            = Kungfudivasposter.jpg
| director         = Onat Diaz
| producer         = Leonardo Po Erik Matti Louie Araneta Joseph Nebriaga Dondon Monteverde Onat Diaz Martina Aileen de las Alas Marian Gracia Rivera
| caption          =
| music            = IJ Garcia
| sound editing    = Mikko Quizon
| cinematography   = Anne Monzon Monchie Redoble 
| story            = Onat Diaz
| screenplay       = Onat Diaz
| writer           = Onat Diaz
| editing          = Christopher Cantos Billy de Leon Jay Halili Melvyn M. Quimosing Bong Villasenor 
| starring         = Ai Ai delas Alas Marian Rivera
| studio           = Reality Entertainment The O&Co. Picture Factory
| distributor      = Star Cinema
| released         =  
| country          = Philippines
| language         =  
| runtime          = 125 mins
| gross            = PHP 56,772,204 
}}
 Filipino action comedy film produced under Reality Entertainment, and The O&Co. Picture Factory starring Ai Ai delas Alas and Marian Rivera. The lead stars also co-produced the film which opened in theaters on October 2, 2013 as part of Star Cinemas 20th Anniversary presentation.    

== Plot ==
Charlotte (Ai Ai de las Alas) is from a family of beauty queens, but she has yet to win a title of her own. Her final chance is the Hiyas ng Dalampasigan Pageant, and her mother has taken steps to make sure that she wins. But her chances are dashed when the mysterious Samantha (Marian Rivera) suddenly joins the contest. The two become bitter enemies following the contest, but they are soon forced by destiny to team up. It turns out the two have a hidden connection to a mystical past, and must work together to discover the truth about their heritage.  

== Cast ==
*Ai Ai delas Alas as Charlotte/Lyna (xiao yuer?)
*Marian Rivera as Samantha/Mena (hua wu que?)
*Roderick Paulate
*Gloria Diaz as Charlottes Adoptive Mother
*Edward Mendez as Kojic/Kojics Father
*Nova Villa
*Ruffa Gutierrez
*Martin Escudero
*Roy Alvarez as Charlottes Adoptive Father
*Precious Lara Quigaman
*Bianca Manalo
*German Moreno as Hiyas ng Dalampasigan Host (Cameo)
*Vicki Belo as a fictional version of herself (Cameo)
*Cacai Bautista as Samanthas original face (Cameo)

== Reception ==
The film received positive reviews from critics. Zig Marasigan of kristn.com showered the movie with positive words. He said, "Kung Fu Divas couldnt have been made by anyone other than a mad genius or a fortunate lunatic. But it seems that writer-director Onat Diaz is a bit of both. However, its Diazs experience as a commercial director that ultimately gives Kung Fu Divas its unique sense of style. Slickly shot, humorously mounted, and aptly scored, the film is weaved together in a far more polished (and creative) fashion than other mainstream releases. For a film that threatens to run away with its own ridiculous premise, Diazs real triumph is making Kung Fu Divas more cohesive and entertaining than it should be. The film suffers from a fistful of lapses in logic, but none of it is too distracting to keep us from believing that beauty pageants and wu xia make perfect sense. Kung Fu Divas embraces its own ridiculous premise with a unique visual style that is both funny and incredibly satisfying." 

Philbert Ortiz Dy from ClickTheCity.com rates the film 4 out of 5 stars, saying that "Kung Fu Divas is definitely worth a look. Kung Fu Divas is a tricky little film. Behind the broad humor, the kung fu antics and the heavy use of VFX lies this fairly subversive take on the pressures put on women to be a certain kind of beautiful. It ends up mocking beauty pageant culture, plays around with cinematic gender roles, and just touches on the current popularity of body enhancement. It’s all pretty clever. The film builds this completely absurd reality that applies the heightened aesthetics of kung fu cinema to the already strange world of beauty queens. These two sides may seem incompatible, but the film manages to blend them quite well. It just commits to the absurdity of both sides of the equation. Everything is approached in the same way, with the same outsized emotions and flair for the ridiculous. The reality of the beauty pageant is taken to such an absurd state that it only makes sense that the two contestants would just leap into the air and fight with electricity coming off their skin. That sense of unreality brings into focus the insanity of our culture’s obsession with beauty, and the film cleverly explores this idea through the story of the two main characters. The VFX are a tad clunky at points, and there are a few odd choices (among them, a cameo by a personality associated with many of the terrible things the film is satirizing), but the overall effect is quite winning. It’s just a lot of fun, with a sense of humor that skews just a little stranger than the average local comedy film. And in its heart, it actually finds something interesting to say about our culture. This film is really worth considering." Mr. Dy praised Marian Riveras comedic performance in the film, he wrote that, "Rivera is particularly great in this film. She really is much better in a comedic context, especially when playing a character that gets to abuse people. She just pulls it off with such flair." 

Duane Lucas Pascua of Spot.ph threw in glowing praises on the film and its director. He wrote, "One of the key strengths of the film is how it pokes fun into the varied facets of modern Filipino culture while managing to stay away from being preachy. Altogether, Divas is a film that will be worthy of your money. The clunky CGI and stunt-work may force you to make Hollywood comparisons, but thats well compensated by the ardent humor the film brings to the table. The convoluted narrative, however, once inspected closely, will shine a light on most of our countrys societal holes. Theres the pressure on women to live up to standard notions of beauty; also the role of women in the intrinsically paternalistic nature of provincial life; and the true definition of what beautiful is to the common masses—and how far will we go to achieve it. The director, Onat Diaz, knows how to handle his material, no matter how absurd the elements are. A true definition of a great filmmaker is how he can get everything to make sense—even if the premise itself might not make any sense.And Diaz, perhaps in a wild creative frenzy, embraces the absurd nature of this premise and majestically used an “unconventional” sense of reality to present a story that, quite honestly, reveals much about our reality today. It is quite genius, really. And what makes it more effective as a pseudo-social commentary is the fact that it uses humor, not drama, to send the message across. Like a Trojan Horse of sorts, it enters the mainstream looking like your average 2-hour laugh marathon; but as it enters your consciousness, it becomes something else—something better." 

According to PEx Movie Reviews - "One surprising element about this movie is its very earnest take on the standards of beauty and why women are drawn to be attractive. While the story makes fun of beauty pageants, it actually touches on a very realistic take on why being beautiful is not simply because of vanity. Through a couple of well-written breakdown monologues, Kung Fu Divas was able to address that theres a different pressure for women to be beautiful. Its a very insightful message and for that alone I would recommend the movie. The visual aspect of the movie is commendable. There are a couple of misses like close up shots of the stars during fight scenes but in general, Kung Fu Divas was able to deliver a very convincing world of heightened reality. The hard work from the production crew to make the visuals look good is clearly seen on screen. Sure, we still need to improve on some areas but this movie shows that we are going to the right direction. The movie also works thanks to the strong chemistry between its two stars. While De las Alas tends to go over the top in some scenes, she was still able to pull through in the end. Meanwhile, Rivera shines in the movie as she is fully committed to the character and I must say she was impressive during a revelation/breakdown scene. Its a twist thats both clever and emotional that requires for the actress to be funny and endearing at the same time and she pulled it off effortlessly." 

According to Nestor U. Torre of Philippine Daily Inquirer, "Onat Diaz’s “Kung Fu Divas” may be “just” a fantasy-comedy caper, but it’s visually vivid, doesn’t scrimp on production values, and elicit perky performances from its actors."  But the reviewer also pointed out that, "despite its obvious plus points, the film fails to come off as a complete treat, due to deficiencies in its storytelling, and what eventually turns out to be its too varied mix of disparate elements, which fail to harmoniously fuse together by the film’s final fade." However, he praised Marian Riveras performance in the film, "It’s a good thing that the flick compensates in part with the unusually perky performance it elicits from Marian, who emerges as the production’s biggest stellar surprise. Ai Ai does her best to keep up with her, but her lack of authentic physical allure and her signature “shouted” performance style conspire to make the senior comic’s actual achievement less admirable than obviously intended."  The critic likewise praised Edward Mendezs pleasantly surprising take as the two lead stars kung fu mentor and protector, "the other thespic surprise in the production is turned in by new screen hunk, Edward Mendez, in the plum “introducing” role of the twins’ kung fu mentor and defender from the other, martial-arts appendage of this perplexingly “ambidextrous” film.  He and Marian keep things viewable, even as over-the-top developments water down the production’s obvious strengths." 

The film also received rave reviews from various moviegoers who have watched the film. It was invited as an Official Selection at Fantasporto-Oporto International Film Festival 2014 and at the 14th Neuchatel International Fantastic Film Festival.

==International Film Festivals==
*FANTASPORTO  2014-34th Oporto International Film Festival
**Official Selection - Orient Express Section 

*14th Neuchatel International Fantastic Film Festival
**Official Selection - New Cinema From Asia 

==Awards==
*62nd FAMAS Awards 2014
**Best Special Effects - Winner
**Best Actress: Marian Rivera - Nominated
**Best Cinematography - Nominated
**Best Production Design - Nominated
**Best Visual Effects - Nominated



==References==
 

==External links==
* 
* 

 
 
 
 