Tuesdays with Morrie (film)
{{Infobox television film
| name         = Tuesdays with Morrie
| image        = Tuesdays with Morrie.jpg  
| image_size   = 220px
| caption      = DVD cover Mick Jackson Thomas Rickman
| based on     =  
| producer     = Executive Producers: Kate Forte Oprah Winfrey Associate Producer: Susan Heyer Supervising Producer: Jennifer Ogden	
| starring     = Jack Lemmon Hank Azaria
| music        = Marco Beltrami
| cinematography = Theo van de Sande	 	
| editing      = Carol Littleton
| studio       = Carlton America Harpo Productions ABC
| released     = December 5, 1999 
| runtime      = 89 minutes
| budget       = 
| country      = United States
| language     = English 
}}
 book of the same title.    It features Jack Lemmon in a role for which he won an Emmy award.   

==Plot==
Mitch became caught up with his career as a sport commentator and journalist. He ignored his girlfriend and did not make time to do things in life that are of the most value to a human being. Now he has learned that Morrie, one of Mitchs professors, is dying of Amyotrophic lateral sclerosis, often referred to as "Lou Gehrigs disease" or ALS. Reconnecting with Morrie is going to teach to Mitch a lot of his old friend and about himself.

==Cast and characters==
* Jack Lemmon – Morrie Schwartz
* Hank Azaria –  Mitch Albom
* Wendy Moniz – Janine
* Caroline Aaron – Connie
* Bonnie Bartlett – Charlotte
* Aaron Lustig – Rabbi Al Axelrod
* Bruce Nozick – Mr. Schwartz
* Ivo Cutzarida – Armand
* John Carroll Lynch – Walter Moran
* Kyle Sullivan – Young Morrie
* Dan Thiel – Shawn Daley
* Christian Meoli – Aldo
* John Billingsley – Sports Fan #1

==Awards== 2000 – Outstanding Single Camera Picture Editing for a Miniseries, Movie or a Special - Carol Littleton (Won)  2000 – Outstanding Lead Actor in a Miniseries or Movie - Jack Lemmon (Won) 2000 – Outstanding Supporting Actor in a Miniseries or a Movie - Hank Azaria (Won) 2000 – Outstanding Made for Television Movie (Won)

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 


 