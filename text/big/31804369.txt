Gaku: Minna no Yama (film)
{{Infobox film
| name = Gaku: Minna no Yama
| image = Gaku film poster.jpg
| caption = Film poster
| director = Osamu Katayama
| producer = Hisashi Usui
| writer = Shinichi Ishizuka (manga)
| screenplay =
| story =
| based on =  
| starring =
| music =
| cinematography =
| editing =
| studio =
| distributor = Toho
| released =  
| runtime = 126 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}}

  is a 2011 Japanese film. It is based on a  , and directed by Osamu Katayama. It debuted in Japanese cinemas on 7 May 2011.   

==Cast==
* Shun Oguri as Sanpo Shimazaki
* Masami Nagasawa as Kumi Shiina
* Kuranosuke Sasaki as Masato Noda
* Takuya Ishida as Toshio Akutsu
* Yoshie Ichige as Ayako Tanimura
* Atsuro Watabe as Hidenori Maki
* Toshihiro Yashiba as Yohei Zama
* Kyosuke Yabe as Shunichi Ando
* Manabu Hamada as Seki
* Suzunosuke as Moriya
* Hiroyuki Onoue as a man in distress
* Kazuki Namioka as Sanpos friend
* Ren Mori as Makoto Aoki
* Bengal as the victims father
* Takashi Ukaji as Shuji Yokoi
* Kaito Kobayashi as Naota Yokoi
* Ken Mitsuishi as Ichiro Kaji
* Noriko Nakagoshi as Yoko Kaji
* Ken Ishiguro as Kyozo Shiina

==Reception==

===Box Office===
This film was the highest grossing film on its debut weekend of May 7–8, grossing a total of US$3,258,511.   

==References==
 

==External links==
*    
*  

 
 
 
 


 

 