Cool Runnings
 
{{Infobox film
| name           = Cool Runnings
| image          = Coolrunnings.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Jon Turteltaub
| producer       = {{Plain list | 
* Dawn Steel
* Susan B. Landau
* Chris Meledandri
* Jeffrey Bydalek
}}
| writer         = {{Plain list | 
* Lynn Siefert
* Tommy Swerdlow Michael Goldberg
}} Michael Ritchie
| starring       = {{Plain list |  Leon
* Doug E. Doug
* Rawle D. Lewis
* Malik Yoba
* John Candy
}}
| music          = {{Plain list | 
* Jimmy Cliff
* Nick Glennie-Smith
* Hans Zimmer
}}
| cinematography = Phedon Papamichael
| editing        = Bruce Green
| studio         = Walt Disney Pictures Buena Vista Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $14 million
| gross          = $154,856,263
}} sports film directed by Jon Turteltaub, and starring Leon Robinson|Leon, Doug E. Doug, Rawle D. Lewis, Malik Yoba and John Candy. The film was released in the United States on October 1, 1993.  This was the last film featuring Candy to be released in his lifetime.

It is loosely based on the true story of the Jamaica national bobsleigh teams debut in the bobsled competition of the 1988 Winter Olympics in Calgary, Alberta, Canada.  
 I Can See Clearly Now" by Jimmy Cliff reaching the top 40 in nations such as Canada, France, and the UK.

==Plot== 100m runner, fails to qualify at the Olympic Trial for the 1988 Summer Olympics when fellow runner Junior Bevil trips and falls, taking Derice and another runner, Yul Brenner, with him.
 Irv Blitzer, an old friend of Derices father Ben who tried to recruit sprinters to the bobsled team years ago. Irving is an American bobsled two time Gold Medalist at the 1968 Winter Olympics who finished first in two events again during the 1972 Winter Olympics but was disqualified from the latter for cheating and retired in disgrace to Jamaica, where he leads an impoverished life as a bookie. Derices persistence eventually convinces Irving to be their coach and return to the life he left behind. They eventually recruit Junior and Yul, though Yul is still upset over Juniors mistake at the Olympic Trial.
 how he became rich with hard work. He encourages Yul not to give up on achieving all of his goals and the two begin to show a mutual respect for one another.

In Calgary, Irving manages to acquire an old practice sled, as the Jamaicans have never been in an actual bobsled. The Jamaicans are looked down upon by other countries, in particular the East German team whose arrogant leader, Josef, tells them to go home, resulting in a bar fight. The team resolves to view the contest more seriously, continuing to train and improve their technique. They qualify for the finals, but are subsequently disqualified due to a technicality which the Olympic committee trotted out as retribution for Irvings prior cheating scandal. A frustrated Irving storms the committee meeting and confronts his former coach from the 72 Olympic Winter Games Kurt Hemphill, now a primary judge of the 88 Olympic Winter Games. He takes responsibility for embarrassing his country with the scandal but implores the committee not to punish the team for his mistake and reminds them that the Jamaicans deserve to represent their country by competing in the Winter Games as contenders. That night at their hotel, the team gets a phone call informing them that the committee has reversed its decision and allows the Jamaicans to once again compete.

The Jamaicans first day on the track results in more embarrassment and a last place finish. Sanka identifies the problem as Derice trying to copy the Swiss team which he idolizes. Once the team develops their own style and tradition, the second day improves; the Jamaican team finishes with a fast time which puts them in eighth position. Derice asks Irving about why he decided to cheat despite his gold medals and prestige; Irving tells Derice, "A gold medal is a wonderful thing, but if youre not enough without it, youll never be enough with it," and convinces him to think of himself as a champion even if he doesnt win the gold.
 Winter Olympics four years later, they were treated as equals.

==Cast== Leon as Derice Bannock
* Doug E. Doug as Sanka Coffie
* Rawle D. Lewis as Junior Bevil
* Malik Yoba as Yul Brenner
* John Candy as Irving "Irv" Blitzer
* Raymond J. Barry as Kurt Hemphill
* Peter Outerbridge as Josef Grull
* Paul Coeur as Roger
* Larry Gilman as Larry
* Charles Hyatt as Whitby Bevil
* Winston Stona as Coolidge
* Bertina Macauley as Joy Bannock
* Kristoffer Cooper as Winston

==Music==
  
A soundtrack album with 11 tracks was released by Sony in 1993 on compact disc (Columbia Chaos OK 57553).

In some European countries the soundtrack album was released by Sony with a 12th (bonus) track being Rise Above It performed by Lock Stock and Barrel (Columbia 474840 2). Songs from the sound track also featured in a little know musical "Rasta in the Snow", which was based on events of the real Jamaican sled team.

{{Track listing
| headline        = 
| extra_column    = 
| total_length    =

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   = yes

| title1          = Wild Wild Life 
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = Wailing Souls
| extra1          = 
| length1         =

| title2          = I Can See Clearly Now 
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = Jimmy Cliff
| extra2          = 
| length2         =

| title3          = Stir It Up 
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = Diana King 
| extra3          = 
| length3         =

| title4          = Cool Me Down 
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = Tiger 
| extra4          = 
| length4         =

| title5          = Picky Picky Head 
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = Wailing Souls
| extra5          = 
| length5         =

| title6          = Jamaican Bobsledding Chant 
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = Worl-A-Girl
| extra6          = 
| length6         =

| title7          = Sweet Jamaica 
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = Tony Rebel
| extra7          = 
| length7         =

| title8          = Dolly My Baby 
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = Super Cat
| extra8          = 
| length8         =

| title9          =  The Love You Want 
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = Wailing Souls
| extra9          = 
| length9         =

| title10         = Countrylypso
| note10          = 
| writer10        = 
| lyrics10        = 
| music10         = Hans Zimmer
| extra10         = 
| length10        =

| title11         = The Walk Home 
| note11          = 
| writer11        = 
| lyrics11        = 
| music11         = Hans Zimmer
| extra11         = 
| length11        =

| title12         = Rise Above It 
| note12          = bonus track included only on European release reference number 474840 2
| writer12        = 
| lyrics12        = 
| music12         = Lock Stock and Barrel 
| extra12         = 
| length12        =

}}

==Reception==
The film received positive reviews.  Cool Runnings has received a rating of 74% on Rotten Tomatoes based on 31 reviews, with 23 positive and 8 negative. The sites consensus states "Cool Runnings rises above its formulaic sports-movie themes with charming performances, light humor, and uplifting tone." 

===Box office===
The film debuted at #3.  The film had total domestic earnings of $68,856,263 in the United States and Canada, and $86,000,000 internationally (with $416,771 earned in Jamaica), for a total of $154,856,263 worldwide.

==Differences between real life and film== Swiss team which they did), and the crash that eliminated the Jamaicans from further competition. However, there were several creative liberties taken by the filmmakers in order to complete the story.

===Characters===
The bobsledders portrayed in the film are fictional, although the people who conceived the idea of a Jamaican bobsled team were inspired by pushcart racers and tried to recruit top track sprinters. However, they did not find any elite sprinters interested in competing and instead recruited four sprinters from the Air Force for the team.
 United States had not won a gold medal in bobsleigh at the Winter Olympics in the four man event since 1948 Winter Olympics|1948. They would not win the gold again until 2010 Winter Olympics|2010.

In the film, the team is formed by Jamaican sprinters after failing to qualify for the 1988 Summer Olympics. The Jamaican Summer Olympic Trials would have occurred following the Winter Olympics in Calgary.

===Organization===
A fictional all-encompassing winter sports governing body, the "International Alliance of Winter Sports", appears in the film. In reality, each Winter Olympic Sport has its own governing body, and bobsledding falls under the jurisdiction of the International Bobsleigh & Skeleton Federation (or FIBT, the initials of its French name).

===Competition===
One of the most fictionalized parts of Cool Runnings was the competition itself. The bobsled competition in the film consists of three individual runs held on three consecutive days, whereas in reality the Olympic bobsled competition consists of four runs - two runs a day held over two consecutive days.
In the film, the Jamaicans are regarded as unwelcome outsiders to the Games by other countries (particularly East Germany) and ridiculed. In reality, the Jamaicans were treated as equals and there was no real animosity between the team and their competitors; in fact, the Jamaicans were aided by another team who lent them one of their backup sleds so they could qualify, so they did not have to buy another teams spare sled. 

While the Jamaicans did crash their bobsled on their fourth and final run, the film implied the team was a medal contenders having run a world record pace prior to the crash. They were in 24th place (out of 26) after their first run was completed in 58.04. Their second run was completed in 59.37, which was the next-to-worst time (25th). On the third run, they had the worst time (1:03.19, good for 26th place), which was almost five seconds behind the 25th fastest run. Of the 103 runs that were completed in the four-man competition, nobody else posted a time over one minute. So going into the final run, the Jamaicans were in 26th (last) place with a cumulative time of 3:00.60 after three runs. This placed them 3.23 seconds behind Portugal for 25th place, and 10.19 seconds behind the USSR team that was in third-place heading into the final run. They would have had to complete a world-record shattering time under 48.00 seconds to bring home a medal.  

===Crash===
The crash happened in the fourth and final run. In the film the crash happens on the third run and is depicted to have been caused by a mechanical failure in the front left blade of the sled. As the driver steers, a nut and bolt on the control column work loose eventually causing a loss of control as the bobsleigh comes out of a turn and subsequently crashing.

In reality, it was deemed that driver inexperience, excess speed and regressing the turn too high caused the sled to become unstable and top heavy seconds prior to it toppling onto its left side. Real TV footage of the actual crash was used in the film but heavily edited to fit in with the films version of the crash. Both the run and the high speed crash were disorienting: team member Nelson Chris Stokes "felt a bump" when they tipped, but didnt realize they had turned over until he started to smell his helmet, which was fiberglass, friction-burning on the ice, "which is something that stays with you for many years afterwards."   
 crescendo response in the movie,  but the real bobsled driver Dudley Stokes cites the spectator applause as the reason the run turned from tragedy to triumph for him. 

===Four-man sled vs two-man sled===
The film also gives the impression that the Jamaicans were the only team from the Caribbean. This was the case in the four-man sled competition, which the movie focuses on. However, in the two-man competition there was also a bobsled team from the Netherlands Antilles which finished 29th, one place ahead of Jamaicas two-man sled team, and two teams from the United States Virgin Islands finished 35th and 38th. 

The film focuses entirely on the four-man bobsled team that did crashed their sled and finished last out of the 26 teams, as all other 25 teams were able to complete all four runs. However, it ignores the fact that two members of the team (Dudley Stokes and Michael White) also competed in the two-man sled competition and successfully completed all four runs and finished in 30th place out of 38 teams that finished all runs, with three other teams not who did not finish. The remaining members of the four man sled team were Devon Harris and Chris Stokes (Dudleys younger brother).  

==Home media== Region 2.

==Legacy==
On January 18, 2014 the Jamaican bobsled team qualified for the 2 man bobsled at the 2014 Winter Olympics in Sochi, Russia. They have described themselves as "Cool Runnings, The Second Generation."    In light of the teams qualification for the 2014 Olympics, Dudley Stokes, one of the original 1988 team and now general secretary of the Jamaica Bobsleigh Federation, said "I dont think the support for the team, like weve seen over the last three days, would have been sustainable without the ongoing appeal of the movie".  The team received funding from many sources, including one donation campaign held by the online community for the cryptocurrency Dogecoin. 

==See also==
 
* Tropical nations at the Winter Olympics
* Jamaica national bobsleigh team
* White savior narrative in film

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 