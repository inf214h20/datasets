The Ladies Man (2000 film)
{{Infobox film
| name           = The Ladies Man
| image          = ladies-man-movie.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Reginald Hudlin
| producer       = Lorne Michaels
| writer         = Tim Meadows Dennis McNicholas Andrew Steele
| narrator       =  John Witherspoon Lee Evans Will Ferrell Sofia Milos Eugene Levy David Huband
| music          = Marcus Miller
| cinematography = Johnny E. Jensen
| editing        = Earl Watson
| studio         = SNL Studios
| distributor    = Paramount Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $24 million   
| gross          = $13,743,212 
| preceded_by    = 
| followed_by    = 
}}
The Ladies Man is a 2000 American comedy film that stars actor, comedian and former Saturday Night Live cast member Tim Meadows. It was directed by Reginald Hudlin. The movie focuses on the exploits of radio host and sex therapy expert Leon Phelps.

==Plot==
Leon Phelps (the "Ladies Man") was a  s, providing the woman weighs no more than 250 pounds.  A night of romance would generally center around a bottle of Courvoisier.

After finally going too far during a broadcast, Leon is fired, but he receives a note from one of his former flames who wants him to come back to her—and is willing to support him in high style. This sounds just fine with Leon, except that the woman didnt sign her name, and now Leon has to backtrack through his numerous conquests of the past and figure out who wants him to work his love magic. Meanwhile, a secret group called the Victims of the Smiling Ass (V.S.A. for short), consisting of the angry husbands and boyfriends whove been cuckolded by Leon, have discovered Leon as their target and are now hot on his trail, eager to get revenge.

==Cast==
*Tim Meadows as Leon Phelps
*Karyn Parsons as Julie Simmons
*Billy Dee Williams as Lester John Witherspoon as Scrap Iron
*Jill Talley as Candy Lee Evans as Barney
*Will Ferrell as Lance DeLune
*Tiffani Thiessen as Honey DeLune
*Sofia Milos as Cheryl
*Eugene Levy as Bucky Kent
*David Huband as Frank
*Julianne Moore as Audrey/"Bloopy"

==Box office==
The film opened at #4 at the North American box office making US$5.4 million in its opening weekend. 

==Reception==
The film was heavily panned by critics. Rotten Tomatoes gives the film a score of 11% based on reviews from 72 critics, with the average score of 3.2 out of 10. The critical consensus reads, "The Ladies Man joins the growing list of mediocre movies based on SNL skits. It just doesnt have enough material to last the length of the movie." 

==See also==
* Recurring Saturday Night Live characters and sketches

==References==
 

==External links==
*   (2000)

 
 
 

 
 
 
 
 
 
 