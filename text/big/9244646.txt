Cape of Good Hope (film)
 
 
{{Infobox film
| name           = The Cape of Good Hope
| image          = Cape of good hopefilm.jpg
| image_size     = 
| caption        = 
| director       = Mark Bamford
| producer       = 
| writer         = Mark Bamford Suzanne Kay Bamford
| narrator       = 
| starring       = Debbie Brown Eriq Ebouaney Nthati Moshesh
| music          = 
| cinematography = Larry Fong
| editing        = 
| distributor    = 
| released       = 2004
| runtime        = 107 mins
| country        = South Africa USA Afrikaans English English Xhosa Xhosa 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Cape of Good Hope is a 2004 South African comedy drama film written and produced by Suzanne Kay Bamford and her husband Mark Bamford under the direction of Mark Bamford. It was Mark Bamfords first feature film after his critically praised short, Hero (2000 film)|Hero (2001).

The film premiered at the Tribeca Film Festival and the Cannes Film Festival in 2004.

==Plot==
The story revolves around an animal rescue shelter and the characters who work there. Kate runs the shelter and is involved with a married man, oblivious to the romantic interests of Morne, the local veterinarian. Sharifa, the receptionist, is desperately struggling to have a child and satisfy her husband. And handyman Jean Claude finds himself torn between his love for a single mother and the desire to emigrate.

==Cast==
*Debbie Brown as Kate
*Eriq Ebouaney as Jean Claude
*Nthati Moshesh as Lindiwe
*Morne Visser as Morne
*Quanita Adams as Sharifa
*David Isaacs as Habib
*Kamo Masilo as Thabo
*Nick Boraine as Stephen van Heern

==Awards==
*Austin Film Festival, 2004 - Won, Audience award: Best Narrative Feature-Undistributed; Mark Bamford
*Austin Film Festival, 2004 - Won, Feature Film Award: Best Narrative Feature; Mark Bamford
*National Board of Review, USA, 2005 - Won, Special Recognition: For Excellence on Filmmaking
*Image Awards, 2006 - Nominated, Image Award: Outstanding Independent or Foreign Film

==External links==
*  
*  

 
 
 
 
 
 
 
 

 
 