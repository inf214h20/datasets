Lost Flight
{{Infobox film
| name           = Lost Flight
| image          = Lost-flight-movie-poster-1970.jpg
| image_size     = 
| caption        = Original film poster
| director       = Leonard J. Horn
| producer       = Frank Price (Executive Producer) Paul Donnelly
| writer         = Dean Riesner
| narrator       = 
| starring       = Lloyd Bridges
| music          = Dominic Frontiere
| cinematography = James A. Crabe
| editing        = Douglas Stewart, Jack W, Schoengarth and Larry Lester
| studio         = Universal City Studios    
| distributor    = National Broadcasting Company (NBC) (1969) (USA) (TV) (original broadcast)  Universal Pictures (1971) (USA) (theatrical)
| released       = 1969 (TV) 1970 (Australia theatrical release)  1971 (US theatrical release)
| runtime        = 104 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 dramatic film disaster genre films and approximates an adult version of Lord of the Flies.

==Plot==
Captain Steve Bannerman (Lloyd Bridges) has been asked to fly one last passenger flight from Hawaii to Australia for Trans-Pacific Airline. During a violent electrical storm, he crashes the jet airliner on an uninhabited South Pacific island. Bannerman takes charge of the survivors and teams with Merle Barnaby (Billy Dee Williams), a black marine returning from combat duty in Vietnam, to try to find a way to survive on the island. Among the surviving passengers and crew, they have the support of Gina Talbot (Anne Francis) and Beejay Caldwell (Jennifer Leak) but oil magnate Glenn Walkup (Ralph Meeker), nightclub entertainer Eddie Randolph (Bobby Van) and Jonesy (Andrew Prine) begin to cause trouble.

In the midst of a power struggle, the captain has to contend with not only helping his crew and passengers survive but also dealing with a number of desperate and irrational passengers. Complicating matters is a 10-year-old boy suffering from acute appendicitis and a pregnant woman. When Bannerman rejects Walkups idea of setting out in a raft as unsafe, he is brutally beaten. The raft sets out manned by Randolph and two associates, but to no avail. A radio bulletin announces the cancellation of all rescue attempts as Beejay falls from a cliff, attempting to escape Jonesy. Her panic-stricken assailant shoots Barnaby, accusing him of Beejays murder. Jonesy is exposed when Beejay revives and tries to escapes into the jungle, but is accidentally impaled by Barnabys animal trap. When a child is born, the survivors unite to create a new society.

==Cast==
* Lloyd Bridges as Captain Steve Bannerman  Bobby Van as Eddie Randolph
* Anne Francis as Gina Talbot
* Ralph Meeker as Glenn Walkup
* Andrew Prine as Jonesy 
* Linden Chiles as Allen Bedecker
* Michael Larrain as Francis Delaney
* Billy Dee Williams as Merle Barnaby
* Michael-James Wixted as Charlie Burnett
* Nobu McCarthy as Zora Lewin
* Jennifer Leak as Bee Jay Caldwell
* Maggie Thrett as 2nd Girl

==Production==
Producer Frank Price originally came up with the idea of stranded passengers on a deserted island in 1966, naming his proposed TV series Stranded. When that project failed to attract much interest, it was put on hold with a new two-hour pilot movie later hatched in 1969 that resulted in Lost Flight. Although aired in 1969, the pilot did not lead to a series. 
 Honolulu Airport in Hawaii showed the movie stand-in being fuelled and loaded while surrounded by JAL and United DC-8s and Pan American 707s. Most of the principal photography dealing with the stranded passengers and crew was filmed on Kauai.

==Reception==
Destined for mainly television broadcast in 1969 on National Broadcasting Company|NBC, Lost Flight was re-broadcast in dubbed versions in Brazil, France and West Germany.   Despite the limited theatrical release of Lost Flight in Australia in 1970, the 1971 New York release, bundled with then current One More Train to Rob (1971) triggered a review in The New York Times by Roger Greenspun, who noted that the similarities to other "lost" genre films in that the "concept about a group of people lost on an uncharted island who are forced to carve out their own civilization..." approximates other efforts.  His further critical review effused over a "lost" gem, "...it is a reasonably entertaining, well-paced, technically ambitious movie that receives great assistance from its performers—notably Lloyd Bridges (the pilot) for good, Ralph Meeker (the businessman) for evil, and Anne Francis (the mistress) for marriage and the family."  

==References==
;Notes
 
;Bibliography
 
* Kaye, Sharon, ed. Lost and Philosophy: The Island Has Its Reasons (The Blackwell Philosophy and Pop Culture Series). Hoboken, New Jersey: Wiley-Blackwell, 2007. ISBN 978-1-4051-6315-6.
* Munden, Kenneth White, ed. The American Film Institute Catalog of Motion Pictures Produced in the United States: Feature Films, 1921-1930. Berkeley, California: American Film Institute (University of California Press), 1997. ISBN 978-0-520-20969-5.
 

==External links==
*  
*  
*  

 
 
 
 
 
 