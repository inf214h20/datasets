Nilakkatha Chalanangal
{{Infobox film 
| name           = Nilakkatha Chalanangal
| image          =
| caption        =
| director       = K Sukumaran Nair
| producer       =
| writer         = Sunny Mamuttil Kanam EJ (dialogues)
| screenplay     = Kanam EJ Sathyan Madhu Madhu Jayabharathi Jose Prakash
| music          = G. Devarajan
| cinematography = CJ Mohan
| editing        = G Venkittaraman
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by K Sukumaran Nair. The film stars Sathyan (actor)|Sathyan, Madhu (actor)|Madhu, Jayabharathi and Jose Prakash in lead roles. The film had musical score by G. Devarajan.   

==Cast== Sathyan
*Madhu Madhu
*Jayabharathi
*Jose Prakash
*Alummoodan
*Aranmula Ponnamma
*Kottayam Chellappan
*S. P. Pillai
*Renuka

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dukha Velliyaazhchakale || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Madhyavenalavadhiyaayi || P Susheela || Vayalar Ramavarma || 
|-
| 3 || Priyamvadayallayo || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Sharathkaalayaamini Sumangaliyaay || P. Madhuri || Vayalar Ramavarma || 
|-
| 5 || Sreenagarathile || P Jayachandran || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 