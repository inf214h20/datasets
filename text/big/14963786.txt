Suzanna (film)
{{Infobox film
| name = Suzanna
| image = Suzanna (1923).jpg
| image_size =
| caption = Original lobby card
| director = F. Richard Jones
| producer = Mack Sennett
| writer = Mack Sennett (adaptation)
| story = Linton Wells
| starring = Mabel Normand
| music = Fred W. Jackman Homer Scott Robert Walters
| editing = Allen McNeil
| distributor = Allied Producers & Distributors Corporation
| released =  
| runtime = 80 mins.
| country = United States
| language = Silent (English intertitles)
}}
  and Mabel Normand in 1917 ]] silent comedy-drama film starring Mabel Normand and directed F. Richard Jones. The picture was produced by Mack Sennett, who also adapted the screenplay from a story by Linton Wells. A partial copy of the film, which is missing two reels, is in a European archive.   at silentera.com 
 Fred W. George Nichols, Walter McGrail, Léon Bary, Winifred Bryson, and Minnie Devereaux. 

==Cast==
* Mabel Normand as Suzanna George Nichols as Don Fernando
* Walter McGrail as Ramón
* Evelyn Sherman as Doña Isabella
* Léon Bary as Pancho (as Leon Bary)
* Eric Mayne as Don Diego
* Winifred Bryson as Dolores
* Carl Stockdale as Ruiz
* Lon Poff as Álvarez
* George Cooper as Miguel
* Minnie Devereaux as Herself (billed as "Indian Minnie")
* Black Hawk as Himself

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 
 