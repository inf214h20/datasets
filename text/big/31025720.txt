¿Por qué ya no me quieres?
{{Infobox film
| name           =¿Por qué ya no me quieres?
| image          = 
| image size     =
| caption        =
| director       =Chano Urueta
| producer       = Jesús Galindo, Pedro Galindo	
| writer         =  Eduardo Galindo (story), Ramón Pérez Peláez
| narrator       =
| starring       =  Sara Montiel, Raúl Martínez (actor)|Raúl Martínez, Agustín Lara 
| music          = José de la Vega Ignacio Torres
| editing        = José W. Bustos	
| distributor    = 
| released       = 3 June 1954 (Mexico) 
| runtime        = 93 minutes
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1954 Mexico|Mexican film. It was directed by 
Chano Urueta.

==Plot==

Tired of struggling, Lilia, played by Sara Montiel, moves to the city to live with her aunt, a former actress.
There she strives to become a singer and meets Raul a singer, played by Agustin Lara, who helps her succeed.

==Cast==

* Sara Montiel
* Agustin Lara 	
* 	 Raúl Martínez (actor)|Raúl Martínez			
* 	 Wolf Ruvinskis	 ...	as Wolf Rubinskis)
* 	 Maruja Grifell		
* 	 Carlos Riquelme		
* 	 Ángel Merino		
* 	 Pascual García Peña		 Jorge Mondragón		
* 	 José Muñoz (actor)|José Muñoz		 Guillermo Hernández	 ...	(as Lobo Negro)
* 	 Joaquín Roche		
*        José Chávez (actor)|José Chávez	 ...	(as Jose T. Chavez)
* 	 Alicia de la Gala		
* 	 Ballet de José Silva

==References==
 

==External links==
*  

 
 
 
 
 


 