White Frog
{{Infobox film
| name = White Frog
| image = White_Frog_film.jpg
| caption = Cover Art
| director = Quentin Lee
| producer = David Henry Hwang Kevin Iwashina Ellie Wen Christopher Lee Joel Soisson
| writer = Fabienne Wen and Ellie Wen
| starring = Booboo Stewart Harry Shum, Jr. BD Wong Joan Chen Gregg Sulkin
| music = David Choi Booboo Stewart MC Jin Steven Pranoto
| cinematography = Yasu Tanida
| editing = Matthew Rundell
| studio = Wentertainment Productions, Chris Lee Productions
| distributor = 
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| budget = $1 million  
| gross = 
}}
White Frog is an American film written by Fabienne Wen and Ellie Wen and directed by Quentin Lee. It is a drama-comedy, aimed at young adults, about 15-year-old Nick Young (Booboo Stewart), a neglected teen with Asperger syndrome whose life is changed forever when tragedy hits his family. It is a story of the power of family, friendship, and love.  Along with Stewart, the film also stars Harry Shum, Jr., BD Wong, Joan Chen, and Gregg Sulkin.

Its début was at the San Francisco International Asian American Film Festival on March 8, 2012. 

==Plot==
Nick Young (Booboo Stewart) is a high school freshman with Asperger Syndrome who idolizes his perfect older brother Chaz Young (Harry Shum, Jr.). His brother dies when riding his bike on the highway, a group of guys were driving recklessly. Chazs friends attend the funeral and help Nick get through his loss. Randy tells Nick how his brother spent "his whole life trying to keep up this lie that you were a perfect family". Chaz had a secret (he was gay) and a secret dream; he wanted to be a dancer.

==Cast==
* Booboo Stewart as Nick Young
* Harry Shum, Jr. as Chaz Young
* BD Wong as Oliver Young
* Joan Chen as Irene Young
* Gregg Sulkin as Randy Goldman
* Tyler Posey as Doug
* Manish Dayal as Ajit
* Talulah Riley as Ms. Lee
* Kelly Hu as Aunt May Justin Martin as Cameron
* Amy Hill as Dr. King
* Phil Abrams as Ira Goldman
* David Henry Hwang as the Pastor
* Lynn Ann Leveridge as Maria
* Kathryn Layng as Edie
* Ron McCoy as the Bearded Man
* Major Curda as Samuel
* Jasmine Di Angelo as Briana
* Carla Jimenez as Mrs. Rodriguez

==Pre-production==
White Frog was written by the mother/daughter duo Fabienne Wen and Ellie Wen.  Ellie Wens mentor, David Henry Hwang, was an executive producer.  Principal photography was completed in August 2011. 

==Score and soundtrack== score to White Frog was composed by Steven Pranoto.  The soundtrack features David Choi, CriBabi, Gowe (Musician)|Gowe, PaperDoll, Shin-B, IAMMEDIC, and Booboo and Fivel Stewart.

==References==

{{reflist|2|refs=
   
   
   
   
}}

==External links==
*  
*   
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 