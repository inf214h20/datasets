The Journals of Musan
{{Infobox film
| name           = The Journals of Musan 
| image          = TheJournalsOfMusan2011Poster.jpg
| caption        = Film poster
| director       = Park Jung-Bum 
| producer       = Park Jung-Bum 
| writer         = Park Jung-Bum
| starring       = Park Jung-Bum Jin Yong-Ok Kang Eun-Jin
| music          =  
| cinematography = Kim Jong-Sun
| editing        = Hyunjoo Jo
| distributor    = Fine Cut
| released       =  
| runtime        = 127 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name = {{Film name
 | hangul         = 무산일기
 | hanja          =  
 | rr             = Musanilgi
 | mr             = Musanilgi}}
}}
The Journals of Musan ( ) is a 2011 South Korean film by Park Jung-Bum, based on the experiences of Parks friend Jeon Seung-chul, a North Korean refugee he met while attending Yonsei University; Jeon was diagnosed with stomach cancer and died in 2008, less than 6 years after he defected to South Korea. 
The film received several major awards, including the Tiger Award at the 40th International Film Festival Rotterdam, he Jury Prize at the 13th Deauville Asian Film Festival, the New Currents award at 15th Busan International Film Festival, the grand prize at Off Plus Camera Festival in Krakow, Poland.  along with the Golden Star at the Festival International du Film de Marrakech. 

==Plot Summary==

The films protagonist is a North Korean refugee named Seung-Chul struggling to adjust to life in Seoul. He is sharing a small apartment with a fellow defector named Kyung-chul, a "broker" who helps refugees send remittances to their families in North Korea (later in the film he angers several of his friends by allegedly cheating them and stealing their money). While both face difficult circumstances in recovering from trauma and adjusting to a new life, Seong-chul and Kyung-chol react to their situations quite differently. Seong-chul is extremely shy and submissive to authority. Throughout the film hes portrayed as a diligent worker, doing thankless tasks and accepting criticism stoically. Kyung-chul, by contest, has few moral scruples, attempting to steal a pair of pants and taunting Seong-chul.

At the start of the film Seung-chul has a job pasting advertisements for sex shops throughout his neighborhood; he is repeatedly beaten up by thugs. Seeking more stable employment he applies for other jobs but is rejected when employers see his citizen registration number, which marks him as a North Korean defector. Finally he gets a night job at a karaoke bar; the bar owners daughter, it turns out, is a woman named Young-sook who he recognizes from church. However, she asks him to pretend not to know her at church, because shes ashamed of working at a karaoke bar and doesnt want members of her congregation to look down on her; Seung-chul agrees not to say anything. Later in the film, when there are no customers at the bar, some of the female employees hear him singing church hymns as he works; after laughing at him for being out of tune, they offer to teach him to sing. However, when Young-sook walks in, she is angry and demands an explanation, asking why he would sing church hymns with karaoke girls. He tells her he doesnt know any songs other than hymns- the implication is that he doesnt know South Korean pop songs and it would be frowned upon to sing North Korean songs. Not realizing hes a defector, she assumes he is lying, and fires him.

The climax occurs at a prayer meeting which Seung-Chul attends with Detective Park, the police officer assigned to help him adjust to life in South Korea. Up till now, viewers have known he is a defector only due to the numbers on his ID card; now they learn the details of his story. Born in Musan, in impoverished North Hamgyong province- he became severely malnourished and got in a fight with a friend over food. The next day he saw his friend lying on the ground, exactly where they had been fighting the day before; Seung-chul realized he was dead, and assumed the fight had killed him.  He became wracked with guilt; the pastor assures him that God will forgive him.

After the prayer meeting, however, Detective Park berates Seung-Chul, asking why he told his story and saying "Who would want to be friends with a killer?" Seung-chul tells him he doesnt have any friends, and walks out of the church; as hes leaving, Young-sook, who was at the prayer meeting and heard his story, comes up to him and apologizes, explaining she "had no idea" he was a defector. She also offers him his job back at the karaoke bar and says she wants to be his friend; he walks away without answering.  Returning to his apartment, he finds his dog, Baek-gu, missing; while he was gone, Kyung-chul had attempted to sell it, but was told no one would buy it because it was a half-breed; he then abandoned it in the middle of a busy street. Seung-chul goes to look for the dog and finds it eating out of a garbage bag.

The next night, Kyung-chul apologizes- and asks Seong-chul to retrieve the money hes hidden in Baek-gus doghouse, reassuring him that its "honest money"; he then reveals his plan to go to America. Seung-chul agrees to help him, but says their friendship is over and "this will be my last favor." Entering his apartment that night, Seung-chul is assaulted and beaten by Kyung-chuls clients, who demand to know where he is so they can force him to pay them back. Seung-chul refuses to tell them. The next morning, he goes to church, where Young-sook invites him to join her in the choir; he agrees. That night, while working at the karaoke bar, he leaves his dog Baeuk-gu outside to wait, because his apartment is no longer safe; somehow Baek-gu gets off his leash, and during a break, Seong-chul finds his body in the street, run over by a car. 

==Reception==

The film was commended for bringing attention to the plight of North Korean defectors and the discrimination they often face in South Korean society; a review the Joongang Ilbo stated "The movie captures the isolation, loneliness and disillusionment refugees feel in what was once the country of their dreams."  Korea Times columnist Kwon Jong-in said it "awaked South Koreans to an important issue that too many have often been ignorant of."

==Cast==
* Park Jung-bum ... Jeon Seung-chul
* Jin Yong-ok ... Kyung-Chul
* Kang Eun-jin ... Young-sook
* Park Young-duk ... Detective Park

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 