Who Takes Love Seriously?
{{Infobox film
| name =  Who Takes Love Seriously?
| image =
| image_size =
| caption =
| director = Erich Engel
| producer =  Curt Melnitz 
| writer =  Curt Alexander   Henry Koster 
| narrator = Max Hansen   Jenny Jugo   Otto Wallburg   Willi Schur
| music = Will Grosz   Kurt Schröder  
| cinematography = Curt Courant   
| editing =      
| studio = Terra Film
| distributor = Terra Film
| released =  28 September 1931
| runtime = 91 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Max Hansen, Jenny Jugo and Otto Wallburg. 

==Cast==
*    Max Hansen (tenor)|   Max Hansen as Max  
* Jenny Jugo as Ilse  
* Otto Wallburg as Speculator Bruno  
* Willi Schur as Jacob  
* Hedwig Wangel as Zimmervermieterin  
* Hugo Fischer-Köppe 
* Ernst Morgan 
* Werner Pledath 
* Ernst Behmer 
* Frigga Braut 
* Jakob Sinn 
* Otti Dietze 
* Arthur Mainzer
* Harry Grunwald
* Hans Ritter

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009. 

== External links ==
*  

 

 
 
 
 
 
 
 