List of horror films of 1979
 
 
A list of horror films released in 1979 in film|1979.

{| class="wikitable sortable" 1979
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | Alien (film)|Alien
| Ridley Scott || Sigourney Weaver, Tom Skerritt, Veronica Cartwright ||    ||  
|-
!   | Alisons Birthday
| Ian Coughlan || Joanne Samuel, Margie McCrae, Martin Vaughan ||   ||  
|- The Amityville Horror
| Stuart Rosenberg || James Brolin, Margot Kidder, Rod Steiger ||   ||  
|-
!   |  
| Chūsei Sone || ||   ||  
|-
!   | Black Magic Terror
| L. Sudjio || Alan Nuary, W.D. Mochtar, Teddy Purba ||   ||  
|- Beyond the Darkness Joe DAmato || Cinzia Monreale ||   ||
|-
!   | The Brood
| David Cronenberg || Oliver Reed, Samantha Eggar ||   ||  
|-
!   | The Butterfly Murders
| Tsui Hark || Lau Siu-Ming, Eddy Ko ||   ||  
|- Buried Alive
| Joe DAmato || Kieran Canter, Franca Stoppi, Cinzia Monreale ||   ||  
|-
!   | Dracula (1979 film)|Dracula
| John Badham || Frank Langella, Laurence Olivier, Donald Pleasence ||    ||  
|-
!   | The Driller Killer
| Abel Ferrara || Jimmy Laine, Carolyn Marz, Baybi Day ||   ||  
|-
!   | Fascination (1979 film)|Fascination
| Jean Rollin || Muriel Montossé, Franca Mai, Jean-Marie Lemaire ||   ||  
|-
!   | Human Experiments
| Gregory Goodell || Ellen Travolta, Linda Haynes ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Beyond The Gate and Women In Prison}}
|-
!   | Killer Fish
| Antonio Margheriti || Lee Majors, Marisa Berenson, Karen Black ||      || Natural horror 
|-
!   | Murder by Decree
| Bob Clark || Christopher Plummer, Donald Sutherland, John Gielgud ||   ||  
|-
!   | Nightwing (film)|Nightwing David Warner, Kathryn Harrold ||   ||  
|-
!   | Nosferatu the Vampyre
| Werner Herzog || Klaus Kinski, Isabelle Adjani ||    ||  
|-
!   | Phantasm (film)|Phantasm
| Don Coscarelli || A. Michael Baldwin, Bill Thornbury ||   ||  
|-
!   | Prophecy (film)|Prophecy
| John Frankenheimer || Talia Shire, Robert Foxworth, Armand Assante ||   ||  
|-
!   | The Visitor (1979 film)|Stridulum
| Giulio Paradisi || Paige Conner, Kareem Abdul-Jabbar, Franco Nero ||    ||  
|-
!   | Thirst (1979 film)|Thirst
| Rod Hardy || Chantal Contouri, David Hemmings, Henry Silva ||   ||  
|- Tourist Trap
| David Schmoeller || Chuck Connors, Jon Van Ness, Jocelyn Jones ||   ||  
|-
!   | Up From the Depths
| Charles B. Griffith || Sam Bottoms, Susanne Reed, Virgil Frye ||    ||  
|- When a Stranger Calls || Fred Walton || Carol Kane, Colleen Dewhurst, Charles Durning ||   ||  
|-
!   | Zombie Holocaust
| Francesco Martino || Ian McCullough, Alexandra Cole, Sherry Buchanan ||   ||  
|-
!   | Zombi 2 Richard Johnson ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Zombie, Island of the Living Dead, Zombie 2: The Dead Are Among Us and Zombie Flesh Eaters}}
|}

==References==
 

==Citations==
 
*  <!--
-->
*  <!--
-->
*  <!--
-->
 

 
 
 

 
 
 
 