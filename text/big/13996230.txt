Jada (film)
{{Infobox film
| name           = Jada
| image          = 
| caption        = 
| director       = Clifton Powell Stevie "Black" Lockett
| writer         = Daniel Chavez
| starring       = Juan Mabson Marlo Williams Sonalii Castillo Rockmond Dunbar Jennifer Freeman Jason Weaver Clifton Powell Siena Goines
| music          = 
| cinematography = Christopher Gosch
| editing        = Thom Obarski
| distributor    = Black & Blue Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Jada is a dramatic film released in 2008. It was directed by Clifton Powell. The film was written by Daniel Chavez.

==Plot==
A struggling widow named Jada (Siena Goines) goes through life in a tough urban neighborhood while taking care of her troubled son, Jamal (Jason Weaver) and daughter, Jasmine (Jennifer Freeman).

==Plot summary==
Jada is the story of a woman whose life becomes chaotic when her husband is killed in a questionable car accident. Her once-comfortable middle-class lifestyle comes crashing down when an insurance company labels the accident a suicide and refuses to pay her benefits as the survivor.

As a result, Jada and her two teenage children, Jasmine and Jamal, become homeless and have to live in their car until the pastor of a local church, the Rev. Terrence Mayweather, comes to their aid. The family finds shelter in a low-income housing project, but Jamal becomes entangled in the neighborhood gang. Throughout the ordeal, Jadas faith in God keeps the family together. An unlikely savior arrives in the person of Simon Williams, who has been released from prison on the Mayweathers strong recommendation. Simon becomes a mentor to Jamal in an attempt to steer him away from the gang, but tensions are high between Simon and the head of the gang, Jason Smith, whose nickname is "Thunder."

==External links==
* 

 
 
 
 
 
 

 