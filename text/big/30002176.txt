The Pagan
:For the American novelist who wrote under this name see George Moore (novelist)
{{infobox film
| name            = The Pagan
| image           = The Pagan.jpg
| caption         = 1929 lobby poster
| director        = W. S. Van Dyke
| producer        = Louis B. Mayer Irving Thalberg John Russell (story) Dorothy Farnum (scenario) John Howard Lawson (intertitles)
| starring        = Ramón Novarro Renée Adorée Donald Crisp Dorothy Janis
| music           = William Axt (music score) Nacio Herb Brown ("Pagan Love Song"  ) Arthur Freed ("Pagan Love Song"  )
| cinematography = Clyde De Vinna
| editing         = Ben Lewis
| distributor     = MGM
| released        = April 27, 1929
| runtime         = 9 reels; 7,459 feet
| country         = United States Silent English intertitles
}} part talking romantic drama filmed in Tahiti and produced and distributed by Metro Goldwyn Mayer. Both director W. S. Van Dyke and cinematographer Clyde De Vinna had previously visited Tahiti in 1928 to film White Shadows in the South Seas. The Pagan stars Ramón Novarro.  
 Where the Rex Ingram and now lost film|lost.

==Plot==
Trader Henry Slater (Donald Crisp) stops at a South Pacific island looking to obtain a cargo of copra. He is informed that half-caste Henry Shoesmith, Jr. (Ramon Novarro) owns the largest plantation, but is rather indolent.

Meanwhile, Shoesmith is lolling around, while admirer Madge (Renée Adorée), wishes she had met him before she became a fallen woman. Then the young man hears a woman singing aboard a ship. He swims out and is strongly attracted to Tito (Dorothy Janis). She, however, rebuffs him.

When the narrow-minded Slater first meets Shoesmith, he is quite rude to the native, but soon changes his manner when he learns who the young man is. The easygoing Shoesmith does not take offense, and is delighted to be formally introduced to Tito, Slaters half-caste ward. Slater starts to bargain for copra and is pleasantly surprised when Shoesmith offers him as much as he wants for free. He takes the precaution of having Shoesmith sign a contract to that effect.

Tito eventually falls in love with Shoesmith, but Slater has other plans for her. He tells Shoesmith to stay away from his ward, using the excuse that Shoesmith has no ambition. He suggests to the naive younger man that he take out a bank loan and build up his business. Then he sails away with Tito and his copra.

Shoesmith follows Slaters advice and runs a store, but Madge warns him he does not know what he is doing (he allows every customer to buy on credit). When Slater returns, Shoesmith asks Tito to marry him. She agrees. However, Slater informs the puzzled Shoesmith that the loan payments are overdue and that he is foreclosing on all of Shoesmiths property. In addition, Slater informs his ward that he will "sacrifice" himself to protect her by marrying her himself. Shoesmith is too late to stop the wedding, but while Madge distracts the guests, he carries Tito off to his native home.

Slater finds Tito while Shoesmith is away, takes her back to his ship and starts to beat her. Shoesmith follows, and a fight ensues. The younger man wins, and he and Tito swim back toward the island. However, when they spot approaching sharks, they have no choice but to head back to Slater, pursuing in his dinghy. Slater takes Tito aboard, but keeps his rival at bay with a sword. Shoesmith swims under the boat to the other side and topples Slater into the water, where the sharks get him. The young couple return to their idyllic home.

==References==
 

==External links==
* 
* 
* 
*  at Virtual History
* 

 

 
 
 
 
 
 
 
 
 
 