The Survivor (1981 film)
{{Infobox film|
  name     = The Survivor|
  image          = Survivor Film Movie Poster.jpg|
    writer         = David Ambrose  James Herbert|
  starring       = Robert Powell   Jenny Agutter|
  director       = David Hemmings|
  producer       = William Fayman|
  distributor    = Greater Union Organization Umbrella Entertainment (AUS) Warner Bros. Pictures (US/International)|
  released   =   |
  runtime        = 100 minutes |
  language = English |
  country = Australia | Brian May|
  budget         = A$1,200,000 David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p299-300 |
 gross= US$700,000 (international) (est.) |
}}
The Survivor is a 1981 Australian Horror Thriller film starring Robert Powell and Jenny Agutter, based on a novel of the same name by James Herbert. It saw the final film appearance of actor Joseph Cotten.

==Production==
$350,000 of the budget was invested by the South Australian Film Corporation, with a similar amount coming from English investors. The rest came from Greater Union, a TV sale and private investment. 
 The Innocents (1961). They chose the latter, a decision Ginnane later said was a mistake. 

Ginnane asked Brian Trenchard-Smith to cut a ten-minute trailer to promote the film to potential buyers before it had been finished. This meant he had to shoot some shots of Jenny Agutter. He later cut the trailer for the actual film when it was released. 

==See also==
* South Australian Film Corporation
* Cinema of Australia

==References==
 

==External links==
*  
*  at Oz Movies
*  
*  
*  
*  

 

 
 
 
 


 