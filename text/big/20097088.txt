Eternal (film)
{{infobox film
| name= Eternal
| director = Wilhelm Liebenberg  Federico Sanchez
| writer = Wilhelm Liebenberg  Federico Sanchez
| producer = Wilhelm Liebenberg  Tommaso Calevi Victoria Sanchez   Conrad Pla
| cinematography = Jamie Thompson
| editing = Isabelle Lévesque
| released =  
| runtime = 108 minutes
| language = English 
| country = Canada
| studio = TVA Films WildKoast Entertainment
| distributor = Sony Pictures Home Entertainment 
}}

Eternal is a 2004 film about sixteenth-century Countess  . Retrieved November 6, 2008. 

==Characters==
Mark R. Leeper writes that "Nérons exotic lesbian vampire" in Eternal (2004) "is reminiscent of Draculas Daughter, though her assistant and victim-procurer Irina (Victoria Sanchez) is much less powerful than Sandor was in the earlier film". 

==Production run==
The film premiered in Canada in September 2004. 

==Reception==
Mark R. Leeper gives the film a 6/10, concluding that the film is "polished, sexy, and entertaining, and the art direction is its best aspect. But it is not a film that will stick with the viewer. It is too similar to films like Jacks Back (about Jack the Ripper returning) and several others". 

==See also==
* Stay Alive, another recent film inspired by the "Blood Countess". 

==References==
 

==External links==
*  

 
 
 