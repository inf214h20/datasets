Aventura en Río
{{Infobox film
| name           = Aventura en Río
| image          = 
| caption        =
| director       = Alberto Gout
| producer       = Guillermo Calderón Pedro Arturo Calderón
| writer         = José Carbo Álvaro Custodio
| starring       = Ninón Sevilla Víctor Junco Luis Aldás
| music          = José M. peñaranda  Antonio Rosado
| cinematography = Alex Phillips
| editing        = 
| distributor    = Producciones Calderón S.A.
| released       = May 2, 1953 (México) 
| runtime        = 95 min 
| country        = Mexico
| language       = Spanish
| budget         =
| gross          =
}}
Aventura en Río (Adventure in Rio) is a Mexican drama film directed by Alberto Gout. It was released in 1953 and starring Ninón Sevilla and Víctor Junco.

==Plot==
Victim of amnesia, Alicia (Ninón Sevilla) a Mexican woman is forced by an operator to work as a cabaret dancer in Rio de Janeiro. Far from her home in Mexico, her husband and her daughter, the woman lives several adventures and offers a distinct variant of her personality: violent, seductive,  aggressive, able to face with courage the most fearsome villains.

==Cast==
* Ninón Sevilla ... Alicia / Nelly
* Víctor Junco ... Ignacio Pendas
* Luis Aldás ... Piraña
* Anita Blanch ... Julia Galván

==Reviews==
Last film of the Cuban-Mexican actress and rumbera Ninón Sevilla directed by Alberto Gout, in which the melodrama of cabaret of the Mexican film is set against the background the Brazilian exotic locations. Notable musical numbers, especially the dream in which the three suitors are disputed the love of the star. Ninon Sevilla travels to Brazil, where she was idolized. Her arrival in Rio de Janeiro was front page of every newspaper and her admirers were fed into care and riots in wherever she appeared. For this occasion, the actress learned Portuguese.

According counted Sevilla, when the team was shooting the film in Brazil, had problems with the reflectors. The actress personally appeared before the then President  .   

==References==
 

==External links==
*  

 
 
 
 
 
 