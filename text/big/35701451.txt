The Face in the Fog
{{infobox film
| name           = The Face in the Fog
| image          = The Face in the Fog (1922) film poster.jpg
| imagesize      =
| caption        =
| director       = Alan Crosland
| producer       = William Randolph Hearst (for Cosmopolitan Productions)
| writer         = John Lynch (scenario) Jack Boyle (scenario)
| based on       =  
| starring       = Lionel Barrymore Seena Owen
| music          =
| cinematography = Ira H. Morgan Harold Wenstrom
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (6,095ft)
| country        = United States Silent (English intertitles) 
}}
The Face in the Fog is a 1922 American silent film produced by Cosmopolitan Productions and distributed by Paramount Pictures. It was directed by Alan Crosland and starred Lionel Barrymore. An incomplete print is preserved at the Library of Congress.    

==Cast==
*Lionel Barrymore - Boston Blackie Dawson
*Seena Owen - Grand Duchess Tatiana
*Lowell Sherman - Count Alexis Orloff George Nash - Huck Kant
*Louis Wolheim - Petrus
*Mary MacLaren - Mary Dawson
*Macey Harlam - Count Ivan
*Gustav von Seyffertitz - Michael Joe King - Detective Wren
*Tom Blake - Surtep
*Marie Burke - Olga
*Joseph W. Smiley - Police Captain Martin Faust - Ivans valet
*Mario Majeroni - Grand Duke Alexis

==References==
 

==External links==
 
* 
* 
* 
 

 

 
 
 
 
 
 


 
 