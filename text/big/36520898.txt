Celluloid Man
 
{{Infobox film
| name           = Celluloid Man
| image          = Celluloid Man Poster.jpg
| director       = Shivendra Singh Dungarpur
| producer =
| starring       = 
| music          = Ram Sampath
| cinematography = Santosh Thundiyil K. U. Mohanan Abhik Mukhopadhyay P. S. Vinod H. M. Ramachandra R. V. Ramani Vikas Sivaraman Mahesh Aney Kiran Deohans Ranjan Palit V. Gopinath   
| editing        = Irene Dhar Malik
| studio         = Dungarpur Films
| released       =  
| runtime        = 150 min
| country        = India
| language       = English Hindi Kannada Bengali
| aspect ratio   = 1.85: 1 
}}

Celluloid Man is a 2012 documentary film directed by Shivendra Singh Dungarpur that explores the life and work of legendary Indian archivist P. K. Nair, founder of the National Film Archive of India and guardian of Indian cinema.
 Best Biographical Best Editing for Irene Dhar Malik.    The film was released in India on 3 May 2013 to coincide with the centenary of Indian cinema.  

==Synopsis==
P. K. Nair’s fascination with cinema began as a child and he watched his first few films lying on the white sand floor of a cinema in Trivandrum. He was a collector even then - collecting ticket stubs, lobby cards, even weighing machine tickets sporting pictures of the stars of the day. He grew up to be a great collector of films.

1,700 films were made in India during the silent film era, of which only nine survive thanks to the efforts of Nair. He travelled to remote parts of India to collect and save cans of rare films. The fact that Dadasaheb Phalke is recognized today as the father of Indian cinema is Nair’s doing. He tried to save any film that he could get his hands on, be it world cinema, Hindi popular films or regional Indian cinema. He even took world cinema to the villages of India.

He has influenced generations of Indian film students especially the Indian New Wave filmmakers such as Mani Kaul, Kumar Shahani, Adoor Gopalakrishnan, Ketan Mehta, Saeed Akhtar Mirza, Jahnu Barua, Girish Kasaravalli, John Abraham (director), Vidhu Vinod Chopra and Kundan Shah.

The film is woven with interviews of several filmmakers and film personalities who have been influenced directly and indirectly by P. K. Nair, among them Krzysztof Zanussi, Lester James Peries, Vidhu Vinod Chopra, Saeed Akhtar Mirza, Gulzar, Basu Chatterjee, Naseeruddin Shah, Kamal Haasan, Girish Kasaravalli, Jahnu Barua, Jaya Bachchan, Dilip Kumar, Saira Banu, Sitara Devi, Santosh Sivan, Rajkumar Hirani, Shyam Benegal, Mahesh Bhatt, Yash Chopra, Ramesh Sippy, Mrinal Sen and Surama Ghatak.

Besides the interviews, the film has rare footage from early Indian masterpieces like Raja Harishchandra, Kaliya Mardan, Gallant Hearts, Marthanda Verma, Jamai Babu, Fearless Nadia, Sant Tukaram, Achut Kanya, Kismat (1968 film)|Kismat, Chandralekha (1948 film)|Chandralekha and Kalpana (1948 film)|Kalpana - all films that were archived and thus saved by Nair.

==Production==
The production started at the end of 2010 and the film was completed in June 2012.

The film has been shot in Warsaw, Colombo, Pune, Nasik, Mumbai, Chennai, Thiruvananthapuram, Heggodu, Bengaluru and Kolkata, and eleven cinematographers worked on it: Santosh Thundiyil, K. U. Mohanan, Abhik Mukhopadhyay, P. S. Vinod, H. M. Ramachandra, R. V. Ramani, Vikas Sivaraman, Mahesh Aney and Kiran Deohans. 

The film was shot on 16mm film and then converted to 35mm, with an aspect ratio of 1.85: 1.

==Awards==
Celluloid Man has won two National Awards in India at the 60th National Film Awards for the Best Historical / Biographical Reconstruction and for Best Editing in 2013. It has also won the "Nestor The Chronicler" award for the best archival film for Celluloid Man at the XII Kyiv International Documentary Film Festival 2013, Ukraine.

Shivendra Singh was given the Bimal Roy Memorial Emerging Talent Award for his film Celluloid Man at a ceremony on 18 October 2013

Shivendra Singh Dungarpur was awarded the Special Jury Award for Celluloid Man at the 13th edition of the Mumbai International Film Festival (MIFF) 2014 on February 9, 2014.

==Festivals==

{| class="wikitable"
|+ FILM FESTIVAL LIST
! No
! Film Festival Name
! Place
! Screening Date
|-
! 1
| IL Cinema Ritrovato 2012 || Bologna || 25–26 June 2012
|-
! 2
| 39th Telluride Film Festival  || Telluride || 1 September 2012
|-
! 3
| 50th New York Film Festival  || New York || 4 October 2012
|-
! 4
| 14th Mumbai Film Festival   || Mumbai || 22 October 2012
|-
! 5
| 43rd International Film Festival of India || Goa || 21 & 27 November 2012
|-
! 6
| 17th International Film Festival of Kerala  || Trivandrum || 12 & 14 December 2012
|-
! 7
| 42nd International Film Festival Rotterdam  || Rotterdam || 28 & 31 January 2013
|-
! 8
| 36th Goteborg International Film Festival || Goteborg || 26–29 January 2013
|-
! 9
| 37th The Hong Kong International Film Festival  || Kowloon || 24 March & 2 April 2013
|-
! 10
| 2nd Sierra Leone International Film Festival  || Sierra Leone || 13 April 2013
|-
! 11
| 1st Scarborough Film Festival || Toronto || 8 June 2013 
|-
! 12
| XII Kyiv International Documentary Film Festival || Kyiv || 21 May 2013
|-
! 13
| Seattle International Film Festival || Seattle || 30 May & 1 April 2013
|-
! 14
| 16th Shanghai International Film Festival || Shanghai || 15 June 2013
|-
! 15
| 2nd International Film Festival of Fiji || Suva Fiji || 12–26 July 2013
|-
! 16
|  Vladivostok  International Film Festival ||  Vladivostok || 10 September 2013
|-
! 17
|  Film South Asia 2013 ||  Kathmandu || 3–6 October 2013
|| 
|}

== References ==
 

==External links==
* 

 
 
 
 
 
 
 