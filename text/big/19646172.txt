Finding Rin Tin Tin
 
{{Infobox film
| name = Finding Rin Tin Tin
| image = 
| image_size = 
| alt = 
| caption = 
| director = Danny Lerner
| producer = Les Weldon George Furla Kirk M. Hallam
| writer = David Rolland Jim Tierney Steve ODonnell William Hope Todd Jensen
| music = Stephen Edwards
| cinematography = Emil Topuzov
| editing = Michele Gisser
| studio = First Look Studios Metropolitan Filmexport Millennium Films Emmett/Furla/Oasis Films|Emmett/Furla Films Nu Image Films
| released =  
| runtime = 87 minutes  
| country = Bulgaria United States
| language = English
| budget = $9 million 
}}
Finding Rin Tin Tin is a 2007 Bulgarian–American drama film directed by Israeli filmmaker Danny Lerner. Based on historical events, the film is the most recent in a long line that includes the character Rin Tin Tin.

==Plot== American serviceman Lee Duncan as a shell-shocked puppy in a bombed-out dog kennel in Lorraine (province)|Lorraine, France. The dog was taken to America and became the hero of several films made in the 1920s and 1930s. 

==Cast==
* Tyler Jensen as Lee Duncan
* Ben Cross as Nikolaus
* Gregory Gudgeon as Gaston Steve ODonnell as Johnson William Hope as Major Snickens
* Todd Jensen as Captain Sandman
* Ivan Renkov as Jacques Ian Porter as Lt. Bryant
* Garrick Hagon as The General Michal Yanai as Monique
* Wesley Stiller as Steve

;As Rin Tin Tin
* Oskar
* Sunny
* Mira
* Zuza
* Lana (teenage)
* Andy (teenage)

==Reception== film critics and was unanimously ranked 1 star. 

==Lawsuit==
In October 2008, Daphne Hereford, an American woman breeding progeny of the original Rin Tin Tin, asked a federal court in Houston, Texas to protect her rights to the Rin Tin Tin name.  The judge ruled in favor of the filmmakers, declaring the use of the name in the film to be fair use. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 