Jumping the Broom
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Jumping the Broom
| image          = Jumping the broom poster.jpg
| caption        = Theatrical Release Poster
| director       = Salim Akil Tracey E. Edmonds Elizabeth Hunter T.D. Jakes Glendon Palmer Curtis Wallace
| screenplay     = Arlene Gibbs Elizabeth Hunter
| starring       = Angela Bassett Paula Patton Laz Alonso Loretta Devine Mike Epps  
| music          = Edward Shearmur
| cinematography = Anastas N. Michos
| editing        = Terilyn A. Shropshire
| studio         = Our Stories Films Stage 6 Films
| runtime        = 112 minutes
| distributor    = TriStar Pictures
| released       =   
| country        = United States
| language       = English
| budget         = $6.6 million   
| gross          =   $37,710,610 	  
}} Tracey E. Edmonds, Elizabeth Hunter, T.D. Jakes, Glendon Palmer, and Curtis Wallace. 
 jumping over a ceremonial broom after being married.
 Blue Rocks, Nova Scotia, standing in for Marthas Vineyard, the setting for the film.   TriStar Pictures distributed the film in the United States on May 6, 2011.

It was released on DVD and Blu-ray on August 9, 2011.

==Plot==
Sabrina Watson (Paula Patton) is the only child of the wealthy Watson family of her mother Claudine 
(Angela Bassett) and father Greg Watson (Brian Stokes Mitchell). Sabrina starts the film having another affair with Bobby, a cheating stud. She asks God to help her again get out of this situation and she promises (again) not to have another one-night stand with anyone and only have sex with her future husband. One day, she accidentally hits Jason Taylor (Laz Alonso) when driving up quickly after putting on make-up and not seeing him run through. She gets out to offer her services and overreacts. Jason forgives her and takes up a night of dinner with her. Five months later, after going out, Sabrina tells Jason about her job offer in China and asks him to still be with her in a long-distance relationship but Jason declines. She walks off sad and soon hears a music group singing, and Jason comes back and asks her to marry him, which she accepts.
 French that she thinks Greg is having an affair with his associate Amanda. While outside, Pam listens in on Geneva and Claudine fighting and finds out that Geneva is actually Sabrinas mother and gave Sabrina to Claudine and Greg after she was born.

During the bachelor party, Sabrina and Jason have a fight about his mother wanting them to be jumping the broom. Malcolm talks to Jason and complains and asks why he isnt the best man. Jason tells him that they havent been best friends in years and Malcolm has only been there to ask for money. When Jason leaves and tries to apologize to Sabrina, Chef McKenna is busy kissing Blythe and not noticing the food which begins to burn which sets off the alarm. Sabrina closes the door on him but they make up through text; however, they have doubts about their wedding.

In the morning, everything begins normally. The boys have a friendly game of football, though Pam tries to tell Jason about Claudine and Genevas secret. Blythe also talks to McKenna about the relationship. McKenna tells her that he thinks she is beautiful and a relationship is still an option. While Pam is getting fitted in her dress, she tries to confront Sabrina about the secret but is interrupted when Jason gets hurt when pushed by Malcolm. Pam tells Sabrina to ask her parents who are her real parents. Claudine and Geneva tell the truth which hurts Sabrina and causes her to drive off and cancel the wedding. Jason confronts his mother and tells her he is a grown man and to stop treating him like a little boy. Jason tells everyone to look for Sabrina and also punches Malcolm. Jason prays to God to help him.

Geneva is called by Sabrina who is at the docks in a boat. Geneva gives the story of Sabrinas father. He was a man in Paris whom she loved and planned to travel the world with but she soon found out he had a wife and child and she returned home alone and pregnant. Jason meets back with Sabrina and the two reconcile. Sabrina goes back home to dress. She gets a broom and a note from Pam saying she is returning home and is sorry. She chases down Pam and asks her to stay. They forgive each other and Pam agrees to stay. Jason and Sabrina have the wedding and also jump the broom. After the wedding, Sebastian kisses Shonda, finally winning her affections, and presumably begins a relationship. Greg and Claudine reconcile and she reveals she has a secret fund. Malcolm and Amy (Julie Bowen), the wedding planner, start sharing a moment together in which she asks if he wants to dance with her and he accepts. At the end, the whole family does the cupid shuffle with everyone happy.

==Cast==
* Paula Patton as Sabrina Watson
* Laz Alonso as Jason Taylor
* Angela Bassett as Claudine Watson
* Loretta Devine as Pam Taylor
* Valarie Pettiford as Aunt Geneva
* Mike Epps as Willie Earl Taylor
* Brian Stokes Mitchell as Greg Watson
* Meagan Good as Blythe
* Tasha Smith as Shonda Peterkin
* DeRay Davis as Malcolm
* Romeo Miller as Sebastian
* Pooch Hall as Ricky
* Gary Dourdan as Chef McKenna
* Julie Bowen as Amy
* Vera Cudjoe as Mabel
* Tenika Davis as Lauren
* T.D. Jakes as Reverend James
* Laura Kohoot as Amanda

==Critical reception==
The film received mixed or average reviews, earning a score of 56% positive on Metacritic. 
 
  Positive reviews include Kevin Thomas of The Los Angeles Times who said that the film "...is proof that it is still possible for a major studio release to be fun, smart and heart-tugging and devoid of numbskull violence and equally numbing special effects." 
{{cite web
   | url = http://articles.latimes.com/2011/may/06/entertainment/la-et-jumping-the-broom-20110506
   | title = Movie review: Jumping the Broom
   | first = Kevin
   | last = Thomas The Los Angeles Times
   | date = 2010-05-06
   | accessdate = 2011-11-22 The Chicago Sun-Times said that, "...the cast is large, well chosen and diverting." 
{{cite web
   | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20110504/REVIEWS/110509996
   | title = Jumping the Broom (PG-13)
   | first = Roger
   | last = Ebert
   | authorlink = Roger Ebert
   | work = rogerebert.com The Chicago Sun-Times
   | date = 2010-05-04
   | accessdate = 2011-11-13
}} 

Negative reviews include Stephanie Merry of The Washington Post who criticized the characters of the mothers saying, "Any light moments are quickly nullified by the oppressive women vying for the title of world’s meanest mom." 
{{cite web
   | url = http://www.washingtonpost.com/gog/movies/jumping-the-broom,1165823/critic-review.html#reviewNum1
   | title = Jumping the Broom
   | first = Stephanie
   | last = Merry
   | work = The Washington Post
   | date = 2010-05-06
   | accessdate = 2011-11-13
}}  John Anderson of Variety (magazine)|Variety also commented on the films "nasty tone". 
{{cite web
   | url = http://www.variety.com/review/VE1117945112?refcatid=31
   | title = Jumping the Broom
   | first = John
   | last = Anderson Variety
   | date = 2010-04-28
   | accessdate = 2011-11-13
}} 

==Awards/Nominations==

*Black Reel Awards  
** Best Picture, nominated
** Best Actor (Laz Alonso), nominated
** Best Supporting Actress (Angela Bassett), nominated
** Best Ensemble, nominated
** Best Director (Salim Akil), nominated
** Best Screenaplay (Elizabeth Hunter & Arlene Gibbs), nominated

*NAACP Image Awards 
**Outstanding Motion Picture, nominated
**Outstanding Actor in a Motion Picture (Laz Alonso), Won
**Outstanding Actress in a Motion Picture (Paula Patton), nominated
**Outstanding Supporting Actor in a Motion Picture (Mike Epps), Won

*BET Awards
**Best Movie, nominated

==References==
 

==External links==
*  
*  
*  
*  
*   at Metacritic
*  

 
 
 
 
 
 
 
 
 
 