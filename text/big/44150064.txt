Kooduthedunna Parava
{{Infobox film
| name           = Kooduthedunna Parava
| image          =
| caption        =
| director       = PK Joseph
| producer       = Thiruppathi Chettiyar
| writer         = PK Joseph
| screenplay     =
| starring       = Ratheesh Captain Raju Balan K Nair Jalaja
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Evershine Productions
| distributor    = Evershine Productions
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed by PK Joseph and produced by Thiruppathi Chettiyar. The film stars Ratheesh, Captain Raju, Balan K Nair and Jalaja in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Ratheesh
*Captain Raju
*Balan K Nair
*Jalaja

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Pavizhamunthirithoppil || K. J. Yesudas, Ambili, Chorus || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 