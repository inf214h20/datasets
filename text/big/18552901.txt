Ramji Rao Speaking
{{Infobox film
| name           = Ramji Rao Speaking (1989)
| image          = Ramji Rao Speaking.jpg
| image_size     =
| caption        = DVD poster
| director       = Siddique-Lal Fazil Joseph Valakuzhy (Ouseppachan) Adheeya Films
| writer         = Siddique-Lal
| narrator       = Saikumar Mukesh Mukesh Innocent Innocent Rekha Rekha Devan Devan
| music          = Bichu Thirumala S. Balakrishnan (composer)|S.Balakrishnan Venu
| editing        = T.R. Shekar
| distributor    = Century
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Malayalam
| budget         =
}}
 Innocent and Rekha in the lead roles. The film deals with social factors affecting Kerala including unemployment during the 1980s, and went on to become a cult classic.

Ramji Rao Speaking marked the debut of director duo Siddique (director)|Siddique-Lal (actor)|Lal, actors Saikumar (Malayalam actor)|Saikumar, Rekha (Tamil actress)|Rekha, N. F. Varghese, Harishree Ashokan, and music director S. Balakrishnan (composer)|S. Balakrishnan. Mannar Mathai Speaking (1995) and Mannar Mathai Speaking 2 (2014) are the sequels to this movie.
 Fazil remade Tamil as Hera Pheri, a film which became one of the biggest hits in Bollywood. It was also remade in Telugu as Dhanalakshmi I Love You.

==Plot==
The story revolves around three unemployed people (the third is a middle age unsuccessful theater owner). The story opens with the arrival of Balakrishnan (Saikumar (Malayalam actor)|Saikumar) in Cochin to dispute the denial of his company job which he was supposed to receive several years ago. Several candidates overtook his chance and the last one was Rani (Rekha (Tamil actress)|Rekha) who pretends to be an influential figure in the town. Rani threatens Balakrishnan to continue to work despite his efforts to overthrow her. The company manager (Sankaradi) who knows her family situation (poor and pathetic) helps her to remain in the job. Balakrishnan determined to stay back in the town until he succeeds to get back his job back from Rani.

During his stay Balakrishnan finds a temporary lodging in Urvasai Theater owned by Mannar Mathayi (Innocent (actor)|Innocent), with another tenant Gopalakrishnan (Mukesh (actor)|Mukesh), both were unemployed and with insignificant earning. Initially Gopalakrishnan does not like the new tenant and tries to expel him from the house but all his efforts are in vain. Gopalakrishnan is tricky and cunning. He lies to his mother that he works in a large company based in Calcutta and is building a new house in Cochin. Balakrishnan finds out and misunderstands Gopalakrishnan as a fraud. But what shocks Balakrishnan most was the realization of Ranis family situation, which pours sympathy in his mind toward her and decided to sacrifice his job. That day night Balakrishnan gets drunk and reveals the fraud play of Gopalakrishnan to Mannar Mathayi. Goplakrishnan confesses his play and justifies that projecting himself as rich and employed was the only way in front of him to comfort his mother. The truth melts the mind of both Balakrishnan and Mannar Mathayi and they all became friends and decided to enjoy the night despite their unending problems.

The turning point of the movie is here. Balakrishnan wakes in the night hearing the phone ring. A gang leader called Ramji Rao (Vijayaraghavan (actor)|Vijayaraghavan) has kidnapped the daughter of a rich businessman Urumees Thampan (Devan (actor)|Devan) and is asking a ransom of a lakh rupees. The three unemployed have no relation with Urummes Thampan; the phone was misdialed. Panicking, Balakrishan tries to find the number of Urumees Thampan from a phone directory, only to find out that the numbers of Urvasi Theaters and Urumees Thampan are interchanged in the directory. Gopalakrishnan develops a quick game play and asks Balakrishnan to act as a dealer between Ramji Rao and Urumees Thampan, without letting them know each other, and demand a ransom of three lakh to Urumees Thampan, instead of a lakh, and get the girl from Ramji Rao and earn two lakh. The plan was set properly, but the job wasnt easy to carry out by hiding from the police and keeping Ramji Rao and Urumees anonymous to each other. Finally after a struggle the three rescue the girl from the gang leader and hand her over to Urumees Thamapan. They confesses the game to him upon a police encounter. Urumees forgives them and is thankful for returning his daughter, and offers three lakh rupees as a prize. The story happily ends here.

==Cast== Saikumar as Balakrishnan Mukesh as Gopalakrishnan Innocent as Mannar Mathai Rekha as Rani Devan as Urumees Thampan Vijayaraghavan as Ramji Rao
* Mamukkoya as Hamzakkoya
* Sukumari as Goplakrishnans mother
* Sankaradi as Manager
* N. F. Varghese as a staff of Balakrishnans office Kunchan as Mathai as the servant
* Amritam Gopinath as Matron

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|-
| 1990
| Arangetra Velai Tamil cinema|Tamil
|Prabhu, Revathi, V. K. Ramaswamy (actor)|V. K. Ramaswamy Fazil (director)|Fazil
|-
| 2000 Hera Pheri Hindi cinema|Hindi Sunil Shetty, Akshay Kumar, Paresh Rawal Priyadarshan
|-
| 2002
| Dhanalakshmi I Love You Telugu cinema|Telugu Allari Naresh, Naresh
|Shiva Nageswara Rao
|-
|}

== Soundtrack ==
The films soundtrack contains 4 songs, all composed by S. Balakrishnan and Lyrics by Bichu Thirumala.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Avanavan Kurukkunna"
| M. G. Sreekumar,  C. O. Anto, Chorus
|-
| 2
| "Kalikkalam Ithu Kalikkalam"
| S. P. Balasubrahmanyam
|-
| 3
| "Kanneerkkaayaliletho"
| M. G. Sreekumar, K. S. Chitra
|-
| 4
| "Oraayiram Kinaakkalal"
| M. G. Sreekumar, Unni Menon, K. S. Chitra, C. O. Anto, Chorus
|}

==External links==
*  

 
 

 
 
 
 
 
 