Mississippi Burning
{{Infobox film
| name           = Mississippi Burning
| image          = Mississippi Burning.jpg
| caption        = Theatrical release poster 
| director       = Alan Parker
| producer       = Frederick Zollo Robert F. Colesberry
| writer         = Chris Gerolmo
| starring       = Gene Hackman Willem Dafoe Trevor Jones
| cinematography = Peter Biziou
| editing        = Gerry Hambling
| distributor    = Orion Pictures
| released       =  
| runtime        = 128 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $34,603,943   
}}
 thriller film murders of John Proctor Joseph Sullivan.
 Best Picture, Best Director Best Actor. Filming locations included a number of locales in  central Mississippi and LaFayette, Alabama.

==Plot==
In 1964, three civil rights workers who organize a voter registry for minorities in Jessup County Mississippi, go missing. The FBI sends two agents, Rupert Anderson (Hackman) and Alan Ward (Dafoe) to investigate. The pair find it difficult to conduct interviews with the local townspeople, as Sheriff Stuckey (Sartain) and his deputies exert influence over the public and are linked to a branch of the Ku Klux Klan.   

 
The wife (McDormand) of Deputy Sheriff Clinton Pell (Dourif), reveals to Anderson in a discreet conversation that the missing civil rights trio have been murdered, with their bodies buried in an earthen dam. Sheriff Stuckey deduces her confession to the FBI and informs Sheriff Pell, who beats her in retribution. Anderson and Ward devise a plan to indict members of the Klan for the murders.

They arrange for a kidnapping of mayor Tilman (Ermey), taking him to a remote shack. There, he is left with a black man (Djola) who threatens to castrate him unless he talks. The abductor is an FBI operative utilized to intimidate the mayor. The mayor gives the operative a full description of the killings, including the names of those involved. Although his statement isnt admissible in court due to coercion, his information proves valuable to the investigators.

Anderson and Ward exploit the new information to concoct a plan, luring identified KKK collaborators to a bogus meeting. The Klan members soon realize its a set up, and leave without discussing the murders. The FBI who are eavesdropping, concentrate on Lester Cowens (Vince), a Klansman of interest who exhibits a nervous demeanor which the agents believe might yield a confession. The FBI pick him up and interrogate him. Later, Cowens is at home when his window is blown out. He looks out to see a burning cross on the lawn. Cowens tries to flee in his truck but is caught by a number of hooded men, who begin to hang him. The FBI developed the ruse, arriving to rescue Cowens, while pretending to chase away the conspirators who are in fact other FBI agents.   

Cowens, believing that his KKK henchmen have threatened his life due to his admissions with the FBI, speaks to the agents and incriminates his accomplices. They charge the Klansmen with civil rights violations to gain prosecution at the federal level. Most of the perpetrators are found guilty and receive sentences ranging from three to ten years in prison. Mayor Tilman is later found dead by the FBI in an apparent suicide.

==Cast==
 
* Gene Hackman as Agent Rupert Anderson
* Willem Dafoe as Agent Alan Ward
* Frances McDormand as Mrs. Pell
* Brad Dourif as Deputy Sheriff Clinton Pell
* Gailard Sartain as Sheriff Ray Stuckey
* R. Lee Ermey as Mayor Tilman
* Stephen Tobolowsky as Clayton Townley
* Michael Rooker as Frank Bailey
* Pruitt Taylor Vince as Lester Cowens
* Badja Djola as Agent Monk
* Kevin Dunn as Agent Bird
* Tobin Bell as Agent Stokes

==Production==
===Background===
  in 1964, showing the photographs of civil rights workers Andrew Goodman, James Chaney, and Michael Schwerner, whom the film story is based on.]] murders of three Mississippi civil rights workers, the investigation into their disappearance, and the prosecution of suspects. The production does not give the real names of the murderers, due to legal considerations. Mississippi Burning does not name the victims who are referred to as "the Boys" in the film.    In the film credits, they are identified as "Goatee", based on Michael Schwerner played by Geoffrey Nauffts; "Passenger", based on Andrew Goodman portrayed by Rick Zieff; and "Black Passenger", based on James Chaney depicted by Christopher White.
 Jerry Mitchell and teacher Barry Bradford discovered his real name.  They claim the informant who revealed the location of the civil rights workers bodies was highway patrolman Maynard King, who willingly told FBI agent Joseph Sullivan.    According to Cartha DeLoach, the FBI paid for its first big break in the case, which was the location of the bodies. In his memoirs, he describes the men only as "a minister and a member of the highway patrol". DeLoach does not say how the two men knew the three civil rights workers had been buried under twelve feet of dirt in an earthen dam on a large farm a few miles outside Philadelphia, Mississippi, but he did say the FBI paid $30,000 for the piece of crucial information. 

==Release==
===Box office===
Mississippi Burning opened modestly at the U.S. box office in fifth place, grossing $3,545,305 in its first wide weekend in release.  It eventually went on to gross an estimated $34,603,943 in revenue domestically.  For 1988 as a whole, the film would cumulatively rank at a box office performance position of 33. 

===Home media=== Region 1 Code widescreen edition of the film was released on DVD in the U.S. on May 8, 2001. Special features for the DVD include; the directors audio commentary; an original theatrical trailer; English and French stereo surround options; along with French and Spanish subtitles.  Currently, there is no set date for either a Blu-ray Disc or Video on demand release for the film.

==Reception==
===Critical response=== weighted average out of 100 to critics reviews, Mississippi Burning received a score of 65 based on 11 reviews.  The film has been criticized by some for its fictionalization of history.      In Time (magazine)|Time magazine, author Jack E. White referred to the film as a "cinematic lynching of the truth".  Parker defended his film by reminding critics that it was a dramatization, not a documentary. It was also criticized for its portrayal of southern African Americans as passive victims, which also shapes the films reenactment of the assassinations.  

{{quote box|quote=
Parker, the director, doesn’t use melodrama to show how terrified the local blacks are of reprisals; he uses realism. We see what can happen to people who are not “good nigras.” The Dafoe character approaches a black man in a segregated luncheonette and asks him questions. The black refuses to talk to him – and still gets beaten by the klan.|source=—Roger Ebert, writing for the Chicago Sun-Times   |align=left|width=40%|fontsize=85%|bgcolor=#FFFFF0|quoted=2}}

On his take of the film, noted film critic Roger Ebert in the Chicago Sun-Times surmised, "We knew the outcome of this case when we walked into the theater. What we may have forgotten, or never known, is exactly what kinds of currents were in the air in 1964." In positive acclaim, he declared the motion picture to be "the best American film of 1988".  Columnist Desson Howe of The Washington Post believed the film "speeds down the complicated, painful path of civil rights in search of a good thriller. Surprisingly, it finds it". He felt the film was a "Hollywood-movie triumph that blacks could have used more of during – and since – that era."  However, critic Jonathan Rosenbaum in the Chicago Reader, lightly criticized director Parker, commenting that the film was "sordid fantasy" being "trained on the murder of three civil rights workers in Mississippi in 1964, and the feast for the self-righteous that emerges has little to do with history, sociology, or even common sense."  

===Accolades===
The film was nominated and won several awards in 1989–90. 

{|class="wikitable" border="1"
|- 
! Award
! Category
! Nominee
! Result
|- 1989 61st Academy Awards    Academy Award Best Picture Frederick Zollo, Robert F. Colesberry
| 
|- Academy Award Best Actor Gene Hackman
| 
|- Academy Award Best Supporting Actress Frances McDormand
| 
|- Academy Award Best Director Alan Parker
| 
|- Academy Award Best Sound Mixing Robert J. Litt, Elliot Tyson, Rick Kline, Danny Michael
| 
|- Academy Award Best Film Editing Gerry Hambling
| 
|- Academy Award Best Cinematography Peter Biziou
| 
|- 1989 American Annual ACE Eddie Awards Best Edited Feature Film (Dramatic) Gerry Hambling
| 
|- 1989 American Annual ASC Awards Best Edited Feature Film Gerry Hambling
| 
|- 1989 39th Berlin International Film Festival    Best Actor Gene Hackman
| 
|- Best Director Alan Parker
| 
|- 1990 43rd British Academy Film Awards Sound
|align="center" Bill Phillips, Danny Michael, Robert J. Litt, Elliot Tyson, Rick Kline
| 
|- Cinematography
|align="center" Peter Biziou
| 
|- Editing
|align="center" Gerry Hambling
| 
|- Direction
|align="center" Alan Parker
| 
|- Original Film Score Trevor Jones
| 
|- British Society of Cinematographers Best Cinematography Peter Biziou
| 
|- Casting Society of America Best Casting for a Drama Film Howard Feuer, Juliet Taylor
| 
|- 1989 Chicago Film Critics Association Awards Best Picture
|align="center" |————
| 
|- Best Supporting Actress  Frances McDormand
| 
|- Best Actor  Gene Hackman
| 
|- Best Supporting Actor  Brad Dourif
| 
|- David di Donatello Awards Best Foreign Actor Gene Hackman
| 
|- Best Foreign Film Alan Parker
| 
|- 1989 Directors Guild of America Awards Directorial Achievement  Alan Parker 
| 
|- Kansas City Film Critics Circle Awards 1988 Best Supporting Actress Frances McDormand
| 
|- 1988 Los Angeles Film Critics Association Awards Best Actor Gene Hackman
| 
|- 1989 46th Golden Globe Awards Best Motion Picture Drama
|align="center" |————
| 
|- Best Director Alan Parker
| 
|- Best Actor in a Drama Gene Hackman
| 
|- Best Screenplay Chris Gerolmo
| 
|- National Board of Review Awards 1988 Best Film
|align="center" |————
| 
|- Best Director Alan Parker
| 
|- Best Actor Gene Hackman
| 
|- Best Supporting Actress Frances McDormand
| 
|- Top Ten Film
|align="center" |————
| 
|- 1988 National Society of Film Critics Awards Best Actor Gene Hackman
| 
|- 1988 New York Film Critics Circle Awards Best Film
|align="center" |————
| 
|- Best Actor Gene Hackman
| 
|- 1990 Political Film Society Awards Human Rights
|align="center" |————
| 
|-
|}

==See also==
 
* 1988 in film
* African-American Civil Rights Movement (1954–68)
* African-American Civil Rights Movement (1954–68) in popular culture|African-American Civil Rights Movement in popular culture
* White savior narrative in film

==References==
;Footnotes
 

==Further reading==
*  
*  
*  

==External links==
 
*  
*  
*  
*  
*  
*  
*   at AlanParker.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 