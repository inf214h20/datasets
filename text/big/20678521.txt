Righteous Ties
{{Infobox film
| name           = Righteous Ties
| image          = Righteous Ties film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         = 거룩한 계보
| rr             = Georukhan gyebo
| mr             = Kŏrukhan kyebo}}
| director       = Jang Jin
| producer       = Lee Byeong-hyeok
| writer         = Jang Jin
| starring       = Jung Jae-young Jung Joon-ho
| music          = Park Geun-tae
| cinematography = Choi Sang-ho
| editing        = Steve M. Choe 
| distributor    = CJ Entertainment
| released       =  
| runtime        = 126 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =  
}} 2006 South Korean film.

== Plot ==
Chi-sung, a gangster, is sent to prison for seven years after stabbing a man on orders from his boss. But his boss betrays him and tries to have him killed, though the attempt is unsuccessful and Chi-sung manages to escape. His childhood friend Joo-joong is given the task of tracking him down, forcing him to choose between his friendship and loyalty to his gang.

== Cast ==
* Jung Jae-young ... Dong Chi-sung
* Jung Joon-ho ... Kim Ju-jung
* Ryu Seung-ryong ... Jung Sun-tan
* Min Ji-hwan ... Kim Young-hee
* Shin Goo ... Chi-sungs father 
* Lee Yong-yi ... Chi-sungs mother
* Kim Dong-ju
* Yoon Yoo-sun ... Hwa-yi
* Lee Moon-soo ... death row inmate
* Ju Jin-mo
* Jang Young-nam
* Jung Gyu-soo
* Kim Kyu-chul ... Han-wook
* Kim Il-woong
* Lee Han-wi
* Lee Jeong
* Park Jung-gi
* Yoon Yoo-seon
* Im Seung-dae
* Park Jun-se
* Lee Cheol-min ... Park Moon-soo
* Kim Sung-hoon
* Kong Ho-suk 
* Lee Sang-hoon ... Yoo Myung-sik
* Choi Won-tae ... young Ju-jung

== Release ==
Righteous Ties was released in   on its opening weekend with 450,134 admissions.  The film went on to receive a total of 1,744,677 admissions nationwide,  with a gross (as of November 12, 2006) of  . 

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 


 
 