Of Time and the City
 
 
{{Infobox film
| name           = Of Time and the City
| image          =
| caption        =
| director       = Terence Davies
| producer       = Solon Papadopoulos, Roy Boulter
| writer         = Terence Davies
| starring       =
| music          = Ian Neil
| cinematography = Tim Pollard (cinematographer)
| editing        = Liza Ryan-Carter
| distributor    = British Film Institute
| released       =  
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
Of Time and the City is a 2008 documentary collage film directed by Terence Davies.

The film has Davies recalling his life growing up in Liverpool in the 1950s and 1960s, using newsreel and documentary footage supplemented by his own commentary voiceover and contemporaneous and classical music soundtracks.
 Time Out said "The one truly great movie to emerge so far (from Cannes)..... this film is as personal, as universal in its relevance, and as gloriously cinematic as anything he has done"  and The Guardian called it "a British masterpiece, a brilliant assemblage of images that illuminate our past. Not only does it tug the heart-strings but its also savagely funny."  BBC TV film critic Mark Kermode nominated it as the best overall film of 2008 on his "Kermode Awards" section of The Culture Show, and Duane Byrge from The Hollywood Reporter lauded the film as "poetically composed" and a "masterwork". {{cite journal
  | last = Byrge
  | first = Duane
  | authorlink =
  | coauthors =
  | title = Of Time and the City
  | journal = The Hollywood Reporter, The Daily from Cannes
  | volume =
  | issue = 8
  | pages = 10
  | publisher =
  | location = Cannes
  | date = 21 May 2008
  | url =
  | format =
  | issn =
  | accessdate = }} 

Of Time and the City won Best Documentary in the Australian Film Critics Association awards for 2009.

==References within the film==
===Poetry and literature===
* A Shropshire Lad by A. E. Housman (opening narration, with the line "the land of lost content")
* "Ozymandias" by Percy Bysshe Shelley
* "The Passionate Shepherd to His Love" by Christopher Marlowe
* "The Nymphs Reply to the Shepherd" by Sir Walter Raleigh
* Four Quartets by TS Eliot|T.S. Eliot
* "Poem 301" by Emily Dickinson.
* Anton Chekhov
* James Joyce

===Music===
* Gustav Mahler
* Dmitri Shostakovich
* Jean Sibelius
* Anton Bruckner
* The Protecting Veil by John Tavener
* "He Aint Heavy, Hes My Brother" recorded by The Hollies used over images of the Korean War
* "The Folks Who Live on the Hill" performed by Peggy Lee whilst showing images of the newly erected tower blocks
* The Beatles
* Elvis Presley
* Victor Sylvester

===Fashion===
* Coco Chanel
* Elsa Schiaparelli

===Landmarks=== Liverpool Philharmonic Hall
* Aintree Racecourse
* Liverpool Metropolitan Cathedral of Christ the King
* St. Georges Hall, Liverpool
* Sefton Park
* Liverpool Stadium
* River Mersey
* Liverpool Exchange railway station
* New Brighton Tower
* Royal Liver Building
* Cunard Building
* Port of Liverpool Building
* Burbo Bank Offshore Wind Farm

===Nearby Locales===
* Salford, Greater Manchester
* New Brighton, Merseyside

===Regular Events===
* Guy Fawkes Night
* The Twelfth
* May Day

===Sports===
* Accrington Stanley F.C.
* Sheffield F.C.
* Hamilton Academical F.C.
* Queen of the South F.C.
* Preston North End F.C.
* Blackpool F.C.
* Everton F.C.
* West Ham United F.C.
* Leicester City F.C.
* Leeds United A.F.C.
* Manchester United F.C.
* Grand National

===Celebrities===
* Kenneth Horne
* Gene Kelly
* Dirk Bogarde
* Bob Danvers-Walker
* Michael OHehir
* Peter OSullevan

===Scholars===
* Carl Jung
* Friedrich Engels

===Radio programmes===
* Julian and Sandy

===Laws===
* Sexual Offences Act 1956

===Religious leaders===
* John Carmel Heenan
* Pope Pius XII
* Pope John XXIII

===Historical figures===
* William III of England

===Contemporary===
* Hovis bread
* Bakelite radio

==See also==
*The Memories of Angels, a similarly constructed film about Montreal
* David Edelsteins Top films of  

==External links==
*   – official website.
*  .
*   – Full transcript (pdf format).

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 