The Fortune Cookie
{{Infobox film
| name           = The Fortune Cookie
| image          = The Fortune Cookie (1966) poster.jpg
| image_size     = 225px
| caption        = theatrical film poster
| director       = Billy Wilder
| producer       = Billy Wilder
| writer         = I.A.L. Diamond Billy Wilder
| starring       = Jack Lemmon Walter Matthau
| music          = André Previn
| cinematography = Joseph LaShelle
| editing        = Daniel Mandell
| studio         = The Mirisch Corporation
| distributor    = United Artists
| released       =  
| runtime        = 125 minutes English
| budget         = $3,705,000
| gross          = $6,800,000   IMDb. Retrieved June 6, 2013. 
}}

The Fortune Cookie (alternative   and Jack Lemmon in their first on-screen collaboration, and directed by Billy Wilder from a script by Wilder and I.A.L. Diamond.

==Plot== football player Browns game at Cleveland Stadium.  Harrys injuries are minor, but his conniving lawyer brother-in-law William H. "Whiplash Willie" Gingrich (Walter Matthau) convinces him to pretend that his leg and hand have been partially paralyzed.  This way, they can receive a huge indemnity from the insurance company.   Harry reluctantly goes along with the scheme because he is still in love with his ex-wife, Sandy (Judi West), and it might win her back.  The insurance company suspects that the paralysis is a fake one, so a cat-and-mouse game starts between its investigator, Chester Purkey (Cliff Osmond), and the shyster Willie.  Boom Boom takes very good care of Harry, who starts having second thoughts as he witnesses guilt taking its toll on Boom Boom.  As he also sees that Sandy is back by his side strictly out of greed, Harry decides to reveal the truth, thereby ruining Willies get-rich plans.

==Cast==
  
* Jack Lemmon as Harry Hinkle
* Walter Matthau as William H. "Whiplash Willie" Gingrich
* Ron Rich as Luther "Boom Boom" Jackson
* Judi West as Sandy Hinkle
* Cliff Osmond as Chester Purkey
* Lurene Tuttle as Hinkles mother
* Harry Holcombe as OBrien
* Les Tremayne as Thompson
* Lauren Gilbert as Kincaid
 
* Marge Redmond as Charlotte Hinkle-Gingrich
* Noam Pitlik as Max
* Keith Jackson TV Sportscaster
* Harry Davis as Dr. Krugman
* Ann Shoemaker as Sister Veronica
* Maryesther Denver as Nurse
* Ned Glass as Doc Schindler
* Sig Ruman as Professor Winterhalter
* Archie Moore as Mr. Jackson
* Howard McNear as Mr. Cimoli
 

==Production==
*This was the first film to feature the movie partnership of Jack Lemmon and Walter Matthau, who were to appear together in ten films:
  
* The Fortune Cookie (1966) The Odd Couple (1968)
* Kotch (1971) The Front Page (1974)
* Buddy Buddy (1981)
  Grumpy Old Men (1993) The Grass Harp (1995)
* Grumpier Old Men (1995)
* Out to Sea (1997)
* The Odd Couple II (1998)
 

Jack Lemmon originally had two other actors proposed to star with him &ndash; Frank Sinatra and Jackie Gleason &ndash; but Lemmon insisted that he do the picture with Walter Matthau. Production on the film was halted for weeks after Walter Matthau had a heart attack.  He had slimmed from 190 to 160 pounds by the time filming was completed and had to wear a heavy black coat to conceal the weight loss.

Scenes were filmed at the Minnesota Vikings vs. Cleveland Browns game, held at Cleveland Municipal Stadium on the afternoon of Halloween 1965, in which the Vikings beat the Browns 27-17. "Saint Marks Hospital" in the film is the newly completed St. Vincent Charity Hospital, a curved building considered ultramodern at that time. The scene  was filmed on East 24th Street in an older section. Terminal Tower served as the exterior of the law firm. In one scene, one can see Erieview Tower and the steel skeleton of the Anthony J. Celebrezze Federal Building under construction.

==Box office== 24th highest grossing film of 1966. The film earned $6.8 million worldwide. 

==Awards and honors== Oscar for Best Supporting Art Direction-Set Best Cinematography Best Writing, Best Motion Picture Actor – Musical/Comedy.   

==References==
Notes
 

==External links==
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 