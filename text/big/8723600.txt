The Informant (1997 film)
 
{{Infobox film name           =The Informant image          = caption        = screenplay         =Nicholas Meyer
| based on =     starring       =Anthony Brophy Cary Elwes Timothy Dalton director       =Jim McBride producer       =Leon Falk Morgan OSullivan Ted Swanson Steven-Charles Jaffe (executive)  Nicholas Meyer (executive)  music          =Shane MacGowan cinematography =Affonso Beato studio  Showtime
|distributor    = released       =  runtime        =105 minutes country        = United States Ireland language       =English budget         = 
|gross=  awards         =
}}
The Informant is a 1997 cable TV movie produced by Showtime (TV network)|Showtime, starring Cary Elwes and Timothy Dalton.  It was directed by Jim McBride and written by Nicholas Meyer based upon the book Field of Blood by Gerald Seymour.

==Plot== IRA men who bring him back to Belfast to perform one last job due to his skill with an Rocket-propelled grenade|RPG. On their way back they are stopped by a British Army patrol led by Lt David Ferris who introduces himself to Gingy.  Gingy initially refuses the job but realises he has no choice after the Chief of the Belfast Brigade briefs him and threatens him. The job entails the killing of a judge using an RPG, during the getaway the gang smash through a roadblock and one of the soldiers from the previous patrol recognises Gingy from the previous checkpoint.

==External links==
* 
*  at Rotten Tomatoes

 

 
 
 
 
 
 
 
 
 


 