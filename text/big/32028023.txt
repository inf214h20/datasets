The Viral Factor
{{Infobox film
| name           = The Viral Factor
| image          = ViralFactor.jpg
| alt            = 
| caption        = Official poster for The Viral Factor
| film name = {{Film name| traditional    = 逆戰
| simplified     = 逆战
| pinyin         = Nì Zhàn
| jyutping       = Jik6 Zin3}}
| director       = Dante Lam
| producer       = Candy Leung
| screenplay     = Dante Lam Jack Ng
| story          = Dante Lam Candy Leung
| starring       = Jay Chou Nicholas Tse
| music          = Peter Kam
| cinematography = Kenny Tse (HKSC)
| editing        = Azrael Chung (HKSE)
| studio         = Emperor Motion Pictures
| distributor    = Emperor Motion Pictures
| released       =  
| runtime        = 120 minutes
| country        = Hong Kong China  Mandarin English English Malay Malay
| budget         = Hong Kong dollar|HK$200 million
| gross          = HK$22,212,450  ( )  ¥127.01 million ( )  US$25,414,221 ( ) 
}} Hong Kong-Chinese Chinese action action director is Chin Kar-lok. Production started in March 2011, and it was released in January 17, 2012.

==Plot==
IDC (International Defense Commission) agent Sean Wong (Andy On) and his team members are in Jordan on a dangerous mission to escort a scientist named Kenner Osama Muhammad, who has stolen a copy of the smallpox virus, to Norway. On the way out of the country, the convoy were ambushed by a group of assailants. It was later revealed that Sean has betrayed the team for his own agenda and shot his colleagues Jon Man (Jay Chou) and his girlfriend Ice (Michelle Bai). Sean then escapes with Kenner and the virus. The failed mission left Ice dead while Jon has a bullet lodged in his brain with no safe way to remove it and only two weeks to live. Three months later, the crew of a freight ship on Malaysian water was found to have been infected and killed by the smallpox virus. Elsewhere, Sean contacts an arms dealer called Tyler (Jared Robinson), who suggests to mutate the virus into a biological weapon, develop a vaccine and sell it to a corrupt pharmaceutical company. In Beijing, Jon returns home to spend his remaining days with his mother (Elaine Jin), who tells him that he has a long lost brother, Man Yeung (Nicholas Tse) whom she left behind with his father, Man Tin (Liu Kai Chi).

In Malaysian court, Man Yeung is being charged for numerous crimes that he had committed. While being escorted to the prison, he attacks the police personnel and makes a successful escape. He later meets up with corrupt police officer Russell, who aided in his escape from custody and was given a new job. On his flight to Malaysia, Jon meets Asian Centre for Disease Control and Prevention (ACDC) doctor Rachel Kan (Lin Peng), who was later abducted by Yeung after the plane landed. A suspicious Jon, who noticed that the car that Rachel was travelling in has a different driver from earlier, tails the vehicle and ended up being forced into the vehicle. Jon, unaware of Yeungs identity, puts up a fight against him and the other henchmen, causing the car to crash. Yeung escapes after a brief struggle with Jon. Later, Jon meets his father and Mans daughter, Cheung Sing who shows him a newspaper article that the man he encountered earlier was his brother.

Meanwhile, Yeung and his goons breaks into Rachels house, who holds her mother hostage and forces her to get a Carnot virus sample from the ACDC headquarters. The next day, while Yeung and Rachel are leaving the building after obtaining the virus sample, a security personnel triggers the alarm, causing a chase through the city. Yeung crashes the getaway vehicle and flees on foot. He calls his father and tells him to bring Cheung Sing to meet him at the train station. Jon, who was nearby to meet up with Rachel, witnesses the event and join in the pursuit of Yeung. Working together, Jon and Yeung managed to hold off the corrupted police officers long enough for them to board a train to escape. Jon persuades Yeung to surrender himself but the two brothers got embroiled in a fist fight. They were later intercepted by the  at a train depot, where Tin sacrifices himself in order to let Yeung and Cheung Sing escape, but Cheung Sing was caught by a henchman and Jon arrested by the police. Russell then discovers that Jon and Yeung are brothers, and orders his men to kill Jon, who is at a hospital for check-up. Yeung later follows Russell to a nightclub, who were to meet Sean. After being assaulted by Yeung to release his daughter, Russell escapes and runs to the road only to be knocked down by bus, killing him instantly. At the hospital, Russells men failed to killed Jon, who escapes after putting up a fight. 

Next morning, Yeung goes to an abandoned building to bring the Carnot virus sample to Sean in exchange for his daughter. Rachel was then forced to combine the Carnot virus sample with the smallpox virus, which Sean later tries to test on Rachels mother. Before it could be done however, Jon shows himself and holds Sean at gunpoint. Sean tries to dissuade Yeung from helping Jon by threatening his daughters life. A police tactical team then storms the building, and in the confusion, Sean escapes with the virus. Jon then gives chase and when it appears that he has nowhere to run, Sean injects the virus into Yeungs daughter, who was placed inside a car. Sean, claiming to be the only person who has the antidote to the virus, tells Jon to bring Rachel to him in exchange for Cheung Sing. With Cheung Sings life at stake, he was left with no choice but to let Sean go. Jon then gets into another car and picks up Yeung, who has been shot, before escaping together. That night, after removing the bullet, Jon and Yeung share their life stories together. However, the police moved in to their hiding place. Yeung escapes while Jon stayed behind to surrender himself.

Later while being transferred to maximum security prison, Jon hijacks the helicopter he is in. Together with Yeung and Rachel, they escape via air and arrives at Seans designated location. However, it turns out to be a trap when Mark, Seans henchman appears and shoot Yeung before abducting Rachel. Jon spots Sean and gives chase while Yeung goes after Rachel, who is brought onto a ship to create the antidote to the virus. Yeung encounters Mark dumping Cheung Sings body into the sea and kills him in a fit of anger before jumping off the ship to retrieve his daughter. Jon arrives in time to help both of them get back on, and then tells Yeung to guard the engine control room to stop the ship from starting while he goes to look for Rachel. After finding her and breaking her out, they are chased by Tyler and Sean, while Yeung easily disposes off Tylers henchman Mike. Sean then shoots and injures Rachel. Jon was nearly killed by Tyler after dropping his gun, but managed to retrieve it at the last moment and shoots the arms dealer. In the final showdown, Sean pins down both Jon and Yeung. Jon, who is becoming weaker by the minute, knows his time is almost up and because only one of them can take down Sean, walks out to become a lure allowing Yeung to shoot Sean.

In the aftermath, Yeung has surrendered to the IDC and goes to Beijing with Cheung Sing to visit his mother in a sign that shows he has forgiven her. The film ends with a narration by Jon that his family is his "everything".

==Cast==
*Jay Chou as Jon Man (萬飛)
*Nicholas Tse as Man Yeung (萬陽)
*Lin Peng as Rachel Kan (簡麗珊)
*Michelle Bai as Ice
*Andy On as Sean Wong (王尚恩) Rogue IDC Agent with intent to unleash a smallpox virus and profit from it by selling a vaccine
*Carl Ng as Ross, an Interpol Agent and Jons good friend
*Liu Kai-chi as Man Tin (萬天), Jon and Man Yeungs father
*Elaine Jin as Fung Ling (風玲), Jon and Man Yeungs mother
*Sammy Hung as Mark, Seans right hand henchman
*Deep Ng (cameo) as Man Yeungs former partner
*Philip Keung as Russell, a corrupt cop and IDC asset working for Sean.
*Ron Smoorenburg as gang member

==Production==
With a HK$200 million budget, lead actors Jay Chou and Nicholas Tse had a combined insurance coverage of $150 million and 20 bodyguards to protect them. The budget also included Malaysian ringgit|RM70,000 for apartment rentals in Kuala Lumpur.   
The producers borrowed airplanes from the air force, tanks and other things to use during filming.Filming locations included Hong Kong, Xian, Middle East and Malaysia. Around 80% of the movie was filmed in Kuala Lumpur, Malaysia.    In the movie, several scenes were shot in the compounds of International Medical University.     One of the locations used in Malaysia was an abandoned construction site in Kuala Lumpur.    Among them, the scene in a traffic jam was filmed at Jalan Raja Chulan, during long weekends and public holidays when fewer people would be in Kuala Lumpur.

==Scenes Filmed in Kuala Lumpur==
*Kuala Lumpur Sentral 吉隆坡中环车站
*Pavilion KL 柏威年廣場
*Kuala Lumpur International Airport
*Mid Valley KTM Komuter Station
*Dang Wangi Police Station, K.L.P.D (Kuala Lumpur PD) HQ

== References ==
 

==External links==
* 
* 
*  at Hong Kong Cinemagic
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 