Happy Campers (2001 film)
 
{{Infobox film
| name           = Happy Campers
| image          = 
| caption        =  Daniel Waters
| producer       = Denise Di Novi Daniel Waters
| starring       = Brad Renfro Dominique Swain Keram Malicki-Sanchez Emily Bergl Jaime King Justin Long Peter Stormare
| music          = Rolfe Kent
| cinematography = Antonio Calvache Eliot Davis
| editing        = Dan Lebental
| distributor    = New Line Cinema
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Daniel Waters.  

==Plot==
When the rule-enforcing camp director at Camp Bleeding Dove gets struck by lightning, the counselors find themselves in sole charge of their campers, and themselves.  The diverse group of college freshmen counselors create a camp experience that their campers will never forget.  They stray from the rules and create a fun, yet crazy and often irresponsible environment.  Each counselor, and some of the campers, find themselves maturing in their own way, yet are still able to create an interesting bond with each other that can only happen at summer camp.

==Cast==
* Brad Renfro as Wichita
* Dominique Swain as Wendy
* Keram Malicki-Sanchez as Jasper
* Emily Bergl as Talia
* Jaime King as Pixel
* Justin Long as Donald
* Jordan Bridges as Adam
* Peter Stormare as Oberon Ryan Adams as "Bad Boy Billy"
* Trevor Christensen as Wes

==Pop culture references==
* Heathers (1989) - Same writer on both films, in this one theres a joke about chugging Drano, which was a plot point of Heathers.
* The Breakfast Club (1985) - Referenced in dialogue. Friday the 13th (1980) - Dialogue about how most camps have a serial killer.
* The movie was filmed in Hendersonville, North Carolina, which was also the setting for Heavyweights.

==Reception==
The film received a 57% positive rating based on 7 critics reviews on the film review aggregate site Rotten Tomatoes. 

==References==
  

==External links==
*  
*  

 
 
 
 
 
 
 
 