Jarhead 2: Field of Fire
 
{{Infobox film
| name           = Jarhead 2: Field of Fire
| image          =
| caption        =
| director       = Don Michael Paul
| producer       = Jeffery Beach John Cappilla Cherise Honey Phillip J. Roth
| writer         = Berkeley Anderson Ellis Black
| based on       =  Josh Kelly Danielle Savre Bokeem Woodbine Ronny Jhutti Cassie Layton Jesse Garcia Jason Wong Esai Morales Stephen Lang
| music          = Frederik Wiedmann
| cinematography = Alexander Krumov
| editing        = Cameron Hallenbeck Universal Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 drama war film and a sequel to Jarhead (film)|Jarhead. The story is about the team of Marines who extract an education activist from the strong-hold of the Taliban insurgents.   

==Plot==
When his sergeant is killed in action against the Taliban, Corporal Chris Merrimette is given command of a squad and assigned a mission to resupply a forward operating base (FOB). The unit consists of Corporal Danny Kettner (played by Bokeem Woodbine, Lance Corporal Danielle Danni Allen,  Private Rafael Soto, Private Justin Li and Private Khalid Hassan abu Faisal, an Afghan National Army soldier helping the unit navigate.

Kettner and Faisal clash verbally, and later physically when Faisal gets tired of waiting for a team of specialists to arrive and evaluate a suspected IED. Faisal manages to disable the IED, allowing the Marines four-vehicle convoy to continue.  Not long after, the squad is flagged down by SEAL Team 10 Senior Chief John Fox. Because the Senior Chief is ranking officer, he orders Merrimette and his men to assist in his operation, which is to extract the package - a woman named Anoosh - from the Taliban-controlled Helmand River Valley. Anoosh is sought by the Taliban for defying Sharia law by attaining an education.

As the team advances, they are ambushed. Pinned behind a rock, Soto freezes. Foxs SEAL Team 10 companion - and only other survivor of a previous ambush - and Pvt Li are killed trying to get to Soto. Chief Fox motivates the squad to continue with the new mission. The team is again confronted, this time by a Taliban sniper. Not wanting gunfire to call attention to more Taliban, Chief Fox and Merrimette move through the bush and ambush the Taliban sniper and his spotter. Fox stabs the sniper to death after a brief struggle; Merrimette overcomes the spotter and beats him to death with a rock.

More Taliban soldiers, this time in small pickup trucks, surprise the squad.  In the ensuing firefight, Chief Fox and all the Taliban are killed. As he lay dying, Fox orders Merrimette to complete the mission by extracting Anoosh; it turns out - unknown to Fox - that Anoosh is scheduled to make a speech at UN HQ in New York City the next day. Fox tells Merrimette to get Anoosh to his friend, a police captain in the nearby village of Minar.

The team, Khalid, and Anoosh reach Minar and meet up the Police Captain. A suspicious person using a video camera apparently alerts the Taliban to the presence of the soldiers and their package. Soon well-armed insurgents arrive in trucks with mounted machine guns.  The Taliban attack the police building with rifles and an Rocket-propelled grenade|RPG; in the fighting Private Soto and the local policemen die. The remaining team is severely stunned and incapacitated by the RPG blast, insurgents enter, find Anoosh, and take her away in a truck, inexplicably leaving the rest of the Marines alive.

A recovering Merrimette vows to pursue Anooshs captors and deliver her to the Marine base as Chief Fox ordered. Kettner questions the validity of sacrificing Marines for "this package," and becomes skeptical about the mission. Khalid steps in and persuades the team to continue on, by telling Anooshs story - which she related to him earlier in the film, including that the Taliban executed her 13-year-old sister as punishment for Anooshs out-of-country university education.

The team sneaks into the insurgent stronghold and silently kill many men. Eventually gunfire alerts the insurgents and their Taliban leader, who vows the Americans will not find Anoosh alive. Just as he is about to execute Anoosh with a blade, she screams out to alert the Marines to her location. Merrimette hears her screams, kills Anooshs captors and rescues her.

As they are trying to make it to a truck to escape the camp, more insurgents arrive and attempt to corner the team. Kettner is wounded, but tells Merrimette to flee while he provides cover. Khalid stays with Kettner and the two of them keep the Taliban troops pinned down. Merrimette, Anoosh and Danni get to a small pickup truck, take out the insurgents and flee. Kettner and Khalid remain behind, keeping the insurgents at bay, but are both killed in a hail of gunfire.

Merrimette, Anoosh and Danni safely arrive at the Marine base, and Anoosh thanks Merrimette for his help. The commanding officer and XO arrive on scene, tell Merrimetter and Danni well done, and drive off with Anoosh, who leaves for the UNHCR conference where she will deliver her speech on education rights in Afghanistan. Merrimette and Danni then walk together further into the base.

==Cast==
*Cole Hauser as SEAL Team 10 Senior Chief Petty Officer John Fox Josh Kelly as Cpl Chris Merrimette
*Danielle Savre as Pvt Danielle Danni Allen
*Bokeem Woodbine as Cpl Danny Kettner
*Ronny Jhutti as Pvt Khalid abu Faisal
*Cassie Layton as Anoosh
*Jesse Garcia as Pvt Rafael Soto
*Jason Wong as Pvt Justin Li
*Esai Morales as Cpt Jones
*Stephen Lang as Major James Gavins

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 