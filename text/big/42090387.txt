It's a Grand Old World
{{Infobox film
| name =  Its a Grand Old World
| image =
| image_size =
| caption = Herbert Smith
| producer = Thomas Charles Arnold 
| writer =  Ingram DAbbes   Fenn Sherie   Thomas Charles Arnold   Sandy Powell
| narrator = Sandy Powell   Gina Malo   Cyril Ritchard   Garry Marsh
| music = 
| cinematography = George Stretton
| editing = Brereton Porter  British Lion 
| distributor = British Lion
| released = January 1937 
| runtime = 71 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website = Herbert Smith Sandy Powell, Gina Malo and Cyril Ritchard. It was made at Beaconsfield Studios.  The films sets were designed by Norman Arnold.

==Synopsis==
An unemployed man wins the football pools, and decides to buy a country house for his actress girlfriend.

==Cast== Sandy Powell as Sandy 
* Gina Malo as Joan 
* Cyril Ritchard as Brain 
* Frank Pettingell as Bull 
* Garry Marsh as Stage Manager 
* Ralph Truman as Banks 
* Fewlass Llewellyn as Father  John Turnbull as Auctioneer 
* Iris Charles as Girl in the Car

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 