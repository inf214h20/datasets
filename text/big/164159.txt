Le Prix du Danger
{{Infobox film
| name           = Le Prix du Danger
| image          = Le_Prix_du_Danger.jpg
| caption        = Le Prix du Danger movie poster
| writer         = Yves Boisset Jean Curtelin Robert Sheckley (based on a short story)
| starring       = Gérard Lanvin Michel Piccoli Marie-France Pisier Bruno Cremer Andréa Ferreol
| director       = Yves Boisset
| producer       =
| distributor    =
| cinematography =
| released       = January 1983
| runtime        = 100 min
| country        = France Yugoslavia
| awards         = French
| budget         =
}}
 Yugoslav science fiction movie, directed by Yves Boisset.  It is based on Robert Sheckleys short story "The Prize of Peril", published in 1958.

==Story==
The most popular television game show in the near future is one where everyday men or women volunteer to be hunted. They are hunted down and killed by a team of five pursuers, also volunteers from different walks of life. If they survive they will get an enormous cash prize. But no one has managed to survive... so far.

Our hero, a family man, needs the money, so he takes part in the game. He proves out to be very good, but things are not what they seem.

== See also ==
* Das Millionenspiel &ndash; German TV movie from 1970 The Running Man &ndash; 1987 film starring Arnold Schwarzenegger

==Bibliography==
*Willis, Donald C. (1985), Varietys Complete Science Fiction Reviews, Garland Publishing Inc, pp.&nbsp;415–416, ISBN 978-0824087128

== External links ==
*  

 
 
 
 
 
 
 
 
 


 