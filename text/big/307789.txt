House of 1000 Corpses
 
 
{{Infobox film
| name           = House of 1000 Corpses
| image          = House of 1000 Corpses poster.JPG
| image_size     = 220px
| alt            =
| caption        = Theatrical release poster
| director       = Rob Zombie
| producer       = Andy Gould
| writer         = Rob Zombie
| starring       = Sid Haig Bill Moseley Matthew McGrory Sheri Moon Zombie Karen Black Erin Daniels Chris Hardwick Jennifer Jostyn Rainn Wilson
| music          = Rob Zombie Scott Humphrey
| cinematography = Alex Poppas Tom Richmond 
| editing        = Kathryn Himoff Robert K. Lambert Sean K. Lambert Uncredited: Robert W. Hedland
| studio         = Spectacle Entertainment Group Universal Studios Lions Gate Films
| released       =  
| runtime        = 88 minutes  
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $16.8 million 
}} exploitation horror The Hills Have Eyes. 
 Universal Pictures, Universal Studios backlots, but it was ultimately shelved by the company in fear that it would receive an NC-17 rating.    The rights to the film were eventually re-purchased by Zombie, who then sold the film to Lions Gate Entertainment. It was released theatrically on April 11, 2003.

==Plot==
On October 30, 1977, Jerry Goldsmith, Bill Hudley, Mary Knowles and Denise Willis are on the road in hopes of writing a book on offbeat roadside attractions. When the four meet Captain Spaulding, the owner of a gas station and "The Museum of Monsters & Madmen", they learn the local legend of Dr. Satan. As they take off in search of the tree from which Dr. Satan was hanged, they pick up a young hitchhiker named Baby, who claims to live only a few miles away. Shortly after, the vehicles tire bursts in what is later seen to be a trap and Baby takes Bill to her familys house. Moments later, Babys half-brother, Rufus, picks up the stranded passengers and takes them to the family home.
 scalped for failing to guess Babys favorite movie star.

When Denise doesnt come home, her father Don calls the police to report her missing. Two deputies, George Wydell and Steve Naish, find the couples abandoned car in a field with a tortured victim in the trunk. Don, who was once a cop, is called to the scene to help the deputies search. They arrive at the Firefly house and upon finding other bodies, are quickly killed by the family. Later that night, the three remaining teenagers are dressed as rabbits, and taken out to an abandoned well. Mary attempts to run away, but is stabbed to death by Baby moments later.
 mental patients; Jerry is on Dr. Satans operating table being Vivisection|vivisected. Dr. Satan tells his mutated assistant, who turns out to be Earl, Mother Fireflys ex-husband, to capture Denise, but Denise outwits him and escapes the chambers by crawling to the surface. She makes her way to the main road, where she encounters Captain Spaulding, who gives her a lift in his car. She passes out from exhaustion in the front seat, and Otis suddenly appears in the backseat with a knife. Denise later wakes up to find herself strapped to Dr. Satans operating table.

==Cast==
 
* Sid Haig as Captain Spaulding
* Bill Moseley as Otis B. Driftwood
* Sheri Moon Zombie as Baby Firefly
* Karen Black as Mother Firefly
* Erin Daniels as Denise Willis
* Chris Hardwick as Jerry Goldsmith
* Rainn Wilson as Bill Hudley
* Jennifer Jostyn as Mary Knowles
* Tom Towles as Lieutenant George Wydell
* Walton Goggins as Deputy Steve Naish
* Matthew McGrory as Tiny Firefly Robert Mukes as Rufus "RJ" Firefly, Jr.
* Dennis Fimple as Grampa Hugo Firefly
* Jake McKinnon as Rufus "Earl" Firefly, Sr.
* Harrison Young as Don Willis
* Irwin Keyes as Ravelli
* Michael J. Pollard as Stucky
* Chad Bannon as Killer Karl
* Walter Phelan as S. Quentin Quale / Dr. Satan William H. Basset as Sheriff Frank Huston
* David Reynolds as Richard "Little Dick" Wick
* Joe Dobbs III as Gerry "Goober" Ober
* Gregg Gibbs as Dr. Wolfenstein
* Rob Zombie (uncredited) as Dr. Wolfensteins assistant
 Animal Crackers A Night Duck Soup A Day at the Races  "Hugo Z. Hackenbush", among others). While this was left as a subtle allusion in the first movie, the sequel The Devils Rejects brought it out into the open, with the names becoming integral to the plot. Dr. Satan was inspired by a 1950s billboard-sized poster advertising a "live spook show starring a magician called Dr. Satan" that Zombie has in his house. 

==Development and production==
  and directed some of his own music videos, but little else. Zombie had designed a haunted maze attraction for Universal Studios; Bill Moseley, who later starred in the film, presented Zombie an award for his design in 1999.  Back in the late 90s and in 2000, Rob Zombie was instrumental in reviving Universal Studios annual "Halloween Horror Night", which led to a friendship between him and the company.
 The Best Little Whorehouse in Texas (1982), and can be seen on Universal Studios tram tours.   The remaining 11 days of the shoot were spent on a ranch in Valencia, California.  The starting budget was $3–4 million, but finished at $7 million. 

==Release==
 
The film was completed in 2000; Stacey Snider, then-head of Universal, called Zombie up for a meeting. Zombie feared Snider would give him money and say "go re-shoot everything". Snider feared the film would receive an NC-17 rating, which led to the company refusing to release the film. After several months of the film being shelved, Zombie was able to purchase the film rights back from Universal, and sell them to Lions Gate Entertainment.

===Box office=== limited opening weekend and $2,522,026 on its official opening weekend. The film grossed $12,634,962 domestically and $4,194,583 in foreign totals. Altogether the film made a worldwide gross of $16,829,545.

===Critical reception===
 
The film opened on April 11, 2003 without being pre-screened for critics. Those who viewed it gave it generally negative reviews. Frank Scheck of The Hollywood Reporter wrote that the film "lives up to the spirit but not the quality of its inspirations" and is ultimately a "cheesy and ultragory exploitation horror flick" and "strangely devoid of thrills, shocks or horror."  JoBlo.com said "  slaps together just the right amount of creepy atmosphere, nervous laughter, cheap scares, fun rides and blood and guts to satisfy any major fan of the macabre." 
 psychotic murders," and was "too highbrow to be a good cheap horror movie, too lowbrow to be satire, and too boring to bear the value of the ticket." 

It currently has a 19% "Rotten" on the film review website Rotten Tomatoes. 

==Sequel==
 
  Michael Myers Halloween II) took over the role of RJ. The character of Grampa Hugo was removed entirely as Dennis Fimple died before Corpses release. The sequel received mixed reviews, but the critical reception was generally better than its predecessor.
 cameos as their characters from both films, Captain Spaulding and Otis B. Driftwood, respectively, while Sheri voiced one of the lead characters, Suzie X.

==Soundtrack==
 
 
 
Zombie composed the film score with Scott Humphrey. It is isolated on the DVD as a separate audio track.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 