La Femme de nulle part
{{Infobox film
| name =  La Femme de nulle part
| image = La Femme de nulle part.jpg
| caption =
| director = Louis Delluc
| producer =
| writer = 
| starring = Ève Francis Roger Karl
| music = Jean Wiener {{Cite web|url= http://brightlightsfilm.com/69/69etaix_bren.php#.UzAQBSeN29U
|title= Pierre Etaix - Revisiting a Forgotten Master |quote= Wiener had first composed film music for the feature La Femme de nulle part (The Woman from Nowhere, 1922), written and directed by Louis Delluc. |publisher=http://brightlightsfilm.com|accessdate=2014-03-24}} 
| cinematography =
| editing = 
| distributor =
| released =  
| runtime = 61 minutes
| country = France
| language = French
| budget = 
}}

La Femme de nulle part (The Woman from Nowhere) is a 1922 French film directed by Louis Delluc. {{Cite web|url= http://movies.msn.com/movies/movie/la-femme-de-nulle-part/
|title= La Femme De Nulle Part|publisher=movies.msn.com|accessdate=2014-03-24}} 

==Plot==
A matured woman embarks on a pilgrimage and in the course of it she gets entangled in the issues of people she would not have met otherwise.
The screenplay was one of three screenplays published under the title Drames du Cinema in 1923. 

==Cast==
* Ève Francis: the pilgrim
* Gine Avril: a young woman
* Roger Karl: the husband
* André Daven: a young man
* Michel Duran: a young man
* Noémie Scize: a nurse

==Reception==
With hindsight the film has been considered an "early masterpiece of impressionist cinema".  

==References==
 

==External links==
*  

 
 
 
 


 