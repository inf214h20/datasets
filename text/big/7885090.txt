Jonestown: The Life and Death of Peoples Temple
{{Infobox Film name           = Jonestown: The Life and Death of Peoples Temple image          = Jonestownposter.jpg caption        = Jonestown movie poster director       = Stanley Nelson producer       = Stanley Nelson writer         = Marcia Smith music          = Tom Phillips  cinematography = Michael Chin distributor    = Firelight Media American Experience released       = October 20, 2006 runtime        = 86 minutes country        = United States language       = English
}}

Jonestown: The Life and Death of Peoples Temple, is a 2006 documentary film made by Firelight Media, produced and directed by Stanley Nelson. The documentary reveals new footage of the incidents surrounding the Peoples Temple and its leader Jim Jones who led over 900 members of his religious group to a settlement in Guyana called Jonestown, where he orchestrated a mass suicide with poisoned Flavor Aid, in 1978.  It is in the form of a narrative with interviews with former Temple members, Jonestown survivors, and persons who knew Jones at various stages.

==Release==
The film premiered at the 2006 Tribeca Film Festival where it received the Outstanding Achievement in Documentary award,  and was broadcast nationally on Monday, April 9, 2007, on Public Broadcasting Service|PBSs documentary program "American Experience".  The DVD release contains a number of scenes and interviews not in the on-air program.

==Awards==
* Golden Gate Award for Best Bay Area Feature Documentary, San Francisco International Film Festival
* Outstanding Achievement in Documentary, 2006 Tribeca Film Festival
* Nominee, 2006 International Documentary Association Awards

==References==
 

==External links==
 
*  
*  
*  
*  
*  , uploaded to YouTube
 

===Film festivals===
 
* , Tribeca Film Festival, 2006
* , San Francisco Film Festival, 2006
* , Seattle International Film Festival, June 2006
* , American Film Institute, Discovery Channel Documentary Film Festival, 2006
* , Melbourne, Australia, 2006
* , British Film Institute, London Film Festival, 2006
* , Los Angeles Film Festival, 2006
* , International Film Festival, Marthas Vineyard, September 2006
* , October 2006
* , October 2006
 

===Reviews===
 
*  , San Francisco Bay Guardian, Cheryl Eddy, 2006
*  , Rotten Tomatoes, 2006
*  , New York Times, Stephen Holden, 2006
* , New York Post, Kyle Smith, October 20, 2006
*  
*  
*  
 

 
 

 
 
 
 
 
 
 
 
 
 
 