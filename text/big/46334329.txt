Surer Bhubane
 
{{Infobox film
| name           = Surer Bhubane
| image          = 
| image_size     =
| caption        = 
| director       = Prabir Mitra
| producer       = 
| writer         = Prabir Mukhopadhyay
| story          =  Madhu Sarkar
| screenplay     = Prabir Mukhopadhyay
| starring       = Prosenjit Chatterjee Tapas Pal Samit Bhanja Rupa Ganguly Subhendu Chatterjee Indrani Dutta Haradhan Bandopadhyay Koushik Bandyopadhyay
| music          = Bappi Lahiri  
| distributor    =
| released       =  
| runtime        = 
| studio         = Thakur Loknath Production
| country        = India Bengali
| budget         =
| gross          =
}}
 Bengali film directed by Prabir Mitra & music composed by Bappi Lahiri.The Film starring Prosenjit Chatterjee,Tapas Pal,Samit Bhanja,Rupa Ganguly,Subhendu Chatterjee,Indrani Dutta,Haradhan Bandopadhyay,Koushik Bandyopadhyay & others.

==Cast==
* Prosenjit Chatterjee
* Tapas Pal
* Samit Bhanja
* Rupa Ganguly
* Subhendu Chatterjee
* Indrani Dutta
* Haradhan Bandopadhyay
* Koushik Bandyopadhyay  

==Soundtrack==
=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Music
| total_length    = 
| title1          = Gaaner Khatar Pata Sarate Sarate
| extra1          = Bappi Lahiri
| length1         = 6:24
| title2          = Kothay Rakho Prem Balo
| extra2          = Bappi Lahiri
| length2         = 5:33
| title3          = Gaane Gaane Gaibo Na Keno
| extra3          = Bappi Lahiri
| length3         = 5:25
| title4          = Na Esho Na Chole Jao
| extra4          = Bappi Lahiri
| length4         = 4:08
| title5          = Maa Tomake Aaj Dekhte Cheyeche
| extra5          = Bappi Lahiri
| length5         = 5:04
| title6          = Aaj Hum Karenge Hungama
| extra6          = Bappi Lahiri
| length6         = 5:05 
}}

== References ==
 

==External links==
 
*  
*  
*  

 
 
 


 