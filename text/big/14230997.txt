Return to the Border
{{Infobox film
 | name = Return To The Border
 | image =
 | caption = Zhao Liang
 | writer =
 | starring =
 | producer = Sylvie Blum
 | distributor =
 | budget =
 | released =  2005
 | runtime = 80 min.
 | language = English
  | }} 2005 documentary directed by Zhao Liang about his return to his hometown in China that borders the Yalu river and North Korea.  The short film presents deep insight into both the Chinese and North Korean societies and changes that have taken place over the last several decades.  First person interviews add depth to the local perceptions of life on both sides of the border.  Footage of life along the North Korean border and the mistrust of foreigners by the North Korean people is vividly displayed.

==See also==
* List of documentary films about North Korea

==External links==
* 
* 
 
 
 


 
 