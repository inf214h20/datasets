Trifling Women
{{Infobox film
| name           = Trifling Women Elijah Dru Dowdy
| image          = Triflingwoman-newspaperad1922.png
| image size     = 225px
| caption        = A newspaper advertisement from the day before the opening night in New York. Rex Ingram
| producer       =
| writer         = Rex Ingram
| narrator       =
| starring       = Barbara La Marr Ramón Novarro
| cinematography = John F. Seitz
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        =
| country        = United States Silent English English intertitles
| budget         = $273,000
}}
Trifling Women was a  . 

==Plot== War shortly after. A short period later, Zareda finds out the Baron is about to poison Marquis Ferroni. Trying to save the marquis, she switches the wine glasses and the Baron dies instead.

The marquis, a powerful millionaire, is very grateful to Zareda and they soon marry. For a short period of time, Zareda is a happy woman, until the return of Ivan. Jealous, Ivan makes sure he is not giving the marquis any rest. It eventually leads to a duel, where the marquis is mortally wounded. As he is about to die, he notices his wife embracing Ivan. Realizing she is using her body to get what she wants, he uses his last seconds alive to kill them both. Thereafter, he dies. 
 Michael Powell remembered the movie thus: "Moonlight on tiger skins and blood dripping onto white faces, while sinister apes, poison and lust kept the plot rolling." 

==Cast==
*Barbara La Marr as Jacqueline de Séverac/Zareda
*Ramón Novarro as Henri/Ivan de Maupin
*Pomeroy Cannon as Léon de Séverac
*Edward Connelly as Baron François de Maupin
*Lewis Stone as The Marquis Ferroni
*Hughie Mack as Père Alphonse Bidondeau
*Eugene Pouyet as Col. Roybet John George as Achmet
*Jess Weldon as Caesar
*Bynunsky Hyman as Hassan

==Production== 1917 film Black Orchids, Rex Ingram.  Back then, the studio thought the film was too erotic and did not encourage its release. Therefore, Ingram remade the film in 1922, making it "twice as erotic." 
 The Prisoner of Zenda (1922), a highly successful film which was recently released. The reteaming made the audience suspecting they were a couple, but it was later revealed that they were no more than close friends.   

The role La Marr played was specially written for the actress.  Shortly before the films release, Novarros name, which was Ramon Samaniegos, was changed to Novarro. Ellenberger, A., Ramon Novarro: a biography of the silent film idol, 1899-1968. p.24 

==Release==
Trifling Women premiered at the Astor Theater in New York City on October 2, 1922, and had its general release on November 6.  It received mixed reviews. Harrisons Report called it "an unquestionable masterpiece."  On the other hand, the reviewer for Variety felt it was a let-down compared to The Prisoner of Zenda, and the New York Times called it "an impossible rigmarole." 

Samuel Goldwyn saw an advance screening of the film and was especially impressed by Novarro. He offered him a contract of $2,000 a week.  This would certainly have been a major career step, since his salary for Trifling Women was $125.  Novarro refused, however, deciding to remain loyal to Ingram, who gave him his big break, according to the actor. 

When Trifling Women became a worldwide financial success, La Marrs salary was raised to $6,500 a week.  She became a worldwide superstar, only outranged in popularity by Gloria Swanson.  Ingram later said that Trifling Women was the best movie he made.  Most critic agreed. Although the film was criticized for not living up to its enormous budget, the director, story and actors performances were praised. 

==Preservation==
Though some production stills survive, Trifling Women is believed to be lost.  

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 
 