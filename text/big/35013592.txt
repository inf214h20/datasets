One Goal (film)
{{Infobox film
| name           = One Goal
| image          = One Goal Agusti.jpg
| caption        = 
| director       = Sergi Agustí
| producer       = Sergi Agustí Films
| writer         = 
| starring       = 
| distributor    = 
| released       = 2008
| runtime        = 26 minutes
| country        = Sierra Leone Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Sergi Agustí
| cinematography = Pep Bonet
| editing        = Aurora Reinlein
| music          = Fermín Dorado
}}

One Goal is a 2008 documentary film directed by the Spanish documentary maker Sergi Agustí. 

== Synopsis ==
One Goal is an objective, but also a passion. This documentary follows the path towards peace that a group of young amputated men began in Sierra Leone years ago. Through the power of their game they have become an example for their society. From icons of war to icons of peace and hope, they transformed their lives through a shared passion: football. {{cite web
 |url=http://www.telegraph21.com/video/one-goal
 |work=Telegraph21
 |title=One Goal
 |accessdate=2012-03-10}} 

==Awards==

The film was a finalist in the LinkTV online film contest in the category of "Overcoming Conflict". {{cite web
 |url=http://www.linktv.org/viewchangefilmcontest/films/category/20
 |title=Online Film Contest
 |publisher=LinkTV
 |accessdate=2012-03-10}} 
It won the main award at the 14th International Sport Film Festival of 2011 in Liberec. {{cite web
 |url=http://www.lb-sto.cz/sportfilm/Soubory/2011/11_Protokol_eng.pdf
 |title=Results of 14th year of International Sport Film Festival  
 |date=30 August 2011
 |publisher=Liberecká sportovní  a tělovýchovná organizace
 |accessdate=2012-03-10}} 
The film won the Paladino di Oro prize at the Sport film Festival in Palermo, Italy in November 2010, and the Venus award to human values at the FILMETS  Badalona film Festival in Badalona, Spain in November 2010.
It won the Audience Award at the MIRADAS.DOC festival in Tenerife and the Benicassim Festival, Special Jury Mention at the Festival de Alcala de Henares ALCINE38, and the Grand Prize of Spanish Cinema in the ZINEBI Documentary Film Festival of Bilbao among others. {{cite web
 |url=http://www.sportsfilm.tv/eng/vod/film/one_goal
 |title=One Goal
 |work=SportsFilmTV
 |accessdate=2012-03-10}} 

== References ==
 
 

 
 
 
 
 
 
 
 
 
 


 
 