Roger la Honte (1922 film)
{{Infobox film
| name = Roger la Honte 
| image =
| caption =
| director = Jacques de Baroncelli
| producer =
| writer = Jules Mary  (novel)   Jacques de Baroncelli
| starring = Rita Jolivet   Gabriel Signoret   Maggy Théry
| music = 
| cinematography = Alphonse Gibory 
| editing =
| studio = Le Film dArt 
| distributor = Etablissements Louis Aubert
| released =  1922 
| runtime = 
| country = France French intertitles
| budget =
| gross =
}} of the same title by Jules Mary. 

==Cast==
*  Rita Jolivet as Julia de Noirville  
* Gabriel Signoret as Roger Laroque  
* Maggy Théry as Suzanne Laroque 
* Eric Barclay as Roger de Noirville  
* Sylvie as Henriette Laroque  
* Régine Dumien as Suzanne, enfant  
* Roger Monteaux as L. de Noirville 
* Roger Pineau as Roger de Noirville, enfant  
* André Marnay as Luversan  
* Paul Jorge as Le caissier 
* Thomy Bourdelle 
* Henri Chomette   
* Mangin   
* Noémi Seize

== References ==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999. 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 

 