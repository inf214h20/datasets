Boat People (film)
 
 
{{Infobox film| name           = Boat People
| image          = Boatpeopleposter.jpg
| caption        =
| director       = Ann Hui
| producer       = Xia Meng
| writer         = Dai An-Ping
| starring       = George Lam Andy Lau Cora Miao Season Ma
| music          = Law Wing-fai
| cinematography = Wong Chung-kei
| editing        = Kin Kin
| distributor    = Bluebird Film Company
| released       = 22 October 1982 {{cite web
|url=http://ipac.hkfa.lcsd.gov.hk/ipac20/ipac.jsp?session=U2080588093NJ.1255&profile=hkfa&uri=link=3100036@!32179@!3100024@!3100036&menu=search&submenu=basic_search&source=192.168.110.61@!horizon
|publisher=Hong Kong Film Archive
|title=Copy/Holding information for Boat People}} 
| country        = Hong Kong China
| runtime        = 106 minutes  Vietnamese
| budget         =
| gross          = Hong Kong dollar|HK$15,475,087 
}} second Hong Kong Film Awards, Boat People won awards for Best Picture, Best Director, Best New Performer, Best Screenplay, and Best Art Direction. It was also screened out of competition at the 1983 Cannes Film Festival.     In 2005, at the 24th Hong Kong Film Awards, Boat People was ranked 8th in the list of 103 best Chinese-language films in the past 100 years. {{cite web
|url=http://www.hkfaa.com/news/100films.html
|title=The Best 100 Chinese Motion Pictures
|year=2005
|accessdate=16 April 2008
|author=Hong Kong Film Awards Association|language=zh}} 

Boat People was the last film in Huis "Vietnam trilogy".  It recounts the plight of the Vietnamese people after the communist takeover following the Fall of Saigon ending the Vietnam War.

==Production== Vietnamese refugees flooded Hong Kong.  In 1979, Hui was making the documentary A Boy from Vietnam for the RTHK network.  In the process of making the film, she collected many interviews conducted with Vietnamese refugees about life in Vietnam following the Fall of Saigon.  From these interviews, she directed The Story of Woo Viet (1981) starring Chow Yun-fat as Woo Viet, a Vietnamese boat person in Hong Kong, and Boat People.
 a war Hainan Island. Berry p. 429   Boat People was the first Hong Kong movie filmed in Communist China. {{cite news
|url=http://www.time.com/time/magazine/article/0,9171,952282,00.html?promoid=googlep
|title=Faraway Place Time
|author=Richard Corliss
|date=14 November 1983
|accessdate=15 April 2008}}  Hui saved a role for Chow Yun-Fat, but because at that time Hong Kong actors working in mainland China were banned in Taiwan, Chow Yun-Fat declined the role out of fear for being blacklisted.  Six months before filming was set to start, and after the film crew were already on location in Hainan, a cameraman suggested that Hui give the role to Andy Lau.  At that time, Andy Lau was still a newcomer in the Hong Kong film industry.  Hui gave Lau the role and flew him to Hainan before a proper audition or even seeing what he looked like. 

==Characters==
*Shiomi Akutagawa (George Lam): a Japanese photojournalist who returns to Vietnam to report about life after the war
*To Minh ( 
*Cam Nuong ( 
*Officer Nguyen (Shi Mengqi): a French-educated Vietnamese official who gave half his life to the revolution; disillusioned with life after the war
*Nguyens mistress (Cora Miao): a Chinese woman who is involved with black-market trade, and a contact for people seeking to leave the country, secretly having an affair with To Minh

==Plot==
The film is shown through the point of view of a Japanese photojournalist named Shiomi Akutagawa (Lam).  Three years after covering Danang during the communist takeover, Akutagawa is invited back to Vietnam to report on life after the war.  He is guided by a government minder to a New Economic Zone near Danang and is shown a group of schoolchildren happily playing, singing songs praising Ho Chi Minh.

The scene that he sees is actually staged to deceive the foreign press.  In Danang, he witnesses a fire and is beaten by the police for taking photos without permission.  He also sees the police beating up a "reactionary".  Later he sees a family being forced to leave the city to a New Economic Zone and wonders why they would not want to go there, recalling the happy children that he saw.

In the city, he meets Cam Nuong (Ma) and her family.  Her mother secretly works as a prostitute to raise her children.  She has two younger brothers, the older one, Nhac, is a street-smart boy who is conversant in American slang, while the younger boy, Lang, was fathered by a Korean that her mother serviced.  From Cam Nuong, Akutagawa learns the grisly details of life under communism in Danang, including children searching for valuables in freshly executed corpses in the "chicken farm".  One day, Nhac finds an unexploded ordnance while scavenging in the garbage and is killed.

At the "chicken farm", Akutagawa meets To Minh (Lau), a young man who was just released from the New Economic Zone.  After To Minh attempts to rob Akutagawas camera, he is tried and re-sent to the New Economic Zone.  Akutagawa uses his connections with an official to follow him there.  At the New Economic Zone, he witnesses the inmates being mistreated.  He returns to the location where the smiling children were singing for him earlier, and finds to his horror them sleeping unclothed in overcrowded barracks.

Meanwhile, To Minh has a plan to escape the country with a friend named Thanh.  However, while on duty dismantling landmines one day, Thanh is blown up.  To Minh gets on the boat to flee the country alone, but he is set up.  The Coast Guard is waiting for them and shoots indiscriminately into the boat, killing all on board then taking all the valuables.

Cam Nuongs mother is arrested for prostitution and forced to confess publicly.  She committs suicide by impaling herself with a hook.  Akutagawa decides to sell his camera to help Cam Nuong and her brother leave the country.  On the night of the ships departure, Akutagawa helps them by carrying a container of diesel.  However, they are discovered and he is shot at.  The diesel container blows up, burning Akutagawa to death.  The film ends with Cam Nuong and her brother safely on the boat, looking forward to a new life at a freer place.

==Awards and nominations==
Boat People was nominated for 12 awards at the second Hong Kong Film Awards and won 5, including Best Film and Best Director. {{cite web
|url=http://www.hkfaa.com/history/list_02.html
|author=Hong Kong Film Awards Association
|script-title=zh:第二屆香港電影金像獎得獎名單
|accessdate=17 April 2008|language=zh}}  In 2005, it was ranked 8th of 103 best Chinese-language films in the past 100 years in a ceremony commemorating 100 years since the birth of Chinese-language cinema.  The list was selected by 101 filmmakers, critics, and scholars.

{| class="wikitable"
! Category
! Winner/Person nominated
! Win
|- Best Film Win
|- Best Director Ann Hui
|- Best Screenplay Dai An-Ping
|- Best New Performer Season Ma
|- Best Art Direction Tony Au
|- Best Actor George Lam Nod
|- Best Actress Cora Miao
|- Best Actress Season Ma
|- Best New Performer Andy Lau
|- Best Cinematography Wong Chung-kei
|- Best Editing
|Kin-kin
|- Best Original Score Law Wing-fai
|}

==Reception== returned to China (which was being negotiated at the time), with the communist Vietnamese government standing in for the communist Chinese government and warning that life in Hong Kong after the handover will be similar to life in Vietnam after the communist takeover. Fu p. 185  In Hong Kong, the film was nominated for 12 categories at the Hong Kong Film Award in 1983 and won 5, including Best Film.
 David Denby in New York Magazine.  At the New York Film Festival, it elicited unusual attention because of its perceived political content (unlike the usual kung-fu Hong Kong films that Western audiences were accustomed to) and high production value. Fu p. 158  Richard Corliss of Time Magazine wrote " ike any movie...with a strong point of view,  Boat People is propaganda", and that " he passions Boat People elicits testify...to Huis skills as a popular film maker."  Janet Maslin in The New York Times observed that Hui "manipulates her material astutely, and rarely lets it become heavy-handed" and that scenes in the film "feel like shrewdly calculating fiction rather than reportage." {{cite news
|url=http://movies.nytimes.com/movie/review?_r=2&res=9F03E4DE1F38F934A1575AC0A965948260&oref=slogin&oref=login
|title=Film Festival; Vietnams Boat People
|author=Janet Maslin
|work=The New York Times
|date=27 September 1983
|accessdate=20 April 2008}} 

However, many critics at the New York Film Festival criticised the films political content, such as J. Hoberman, Renee Shafransky, and Andrew Sarris, all writing in The Village Voice. They objected to what they saw as the one-sided portrayal of the Vietnamese government and the lack of historical perspective. Some others found it politically simplistic and sentimental. 

==Controversies==
Because the film was produced with the full co-operation of the government of the Peoples Republic of China, a government that had recently fought a war with Vietnam, many see it as anti-Vietnam propaganda despite Huis protestations.  The New York Times wrote that the films harsh view of life in communist Vietnam was not unexpected, given the PRC governments enmity to the Vietnamese.  Hui emphasised her decision to depict the suffering of Vietnamese refugees based on extensive interviews she conducted in Hong Kong.  She insisted that the PRC government never requested that she change the films content to propagandise against Vietnam, and that they only told her that "the script had to be as factually accurate as possible." {{cite web
|url=http://americancinemapapers.homestead.com/files/BOAT_PEOPLE.htm
|author=Harlan Kennedy
|title=Ann Huis Boat People – Cannes 1983: Attack in Hong Kong
|date=October 1983
|publisher=Film Comment
|accessdate=20 April 2008}}  She denied that the situation in Vietnam was grossly exaggerated in the film, such as the scene of the boat being attacked by the Coast Guard – she was inspired by news reports of a guard ship creating whirlpools to sink a boat carrying boat people. 

At the Cannes Film Festival, some left-wing sympathizers protested against the films inclusion, and it was dropped from the main competition.  This was reportedly done at the behest of the French government, seeking to solidify its relations with the Socialist Republic of Vietnam. {{cite news
|url=http://www.time.com/time/magazine/article/0,9171,926019-2,00.html
|title=In a Bunker on the C
|author=Richard Corliss
|work=Time
|date=30 May 1983
|accessdate=20 April 2008}} 

In the Republic of China (Taiwan), the film, along with all of Huis other work, was banned because it was filmed on Hainan, an island in the Peoples Republic of China. 

==Title==
*The films English title is misleading: Boat People does not tell the story of boat people, it instead tells the plight of Vietnamese people under communist rule, the reason causing them to become boat people.  The Chinese title, literally meaning "Run Towards the Angry Sea", more accurately describes the films content. 

==See also==
 
*Journey from the Fall
*Andy Lau filmography
 

==Notes==
 

==References==
*{{cite book
|first=Michael
|last=Berry
|year=2005
|title= Speaking in Images: Interviews with Contemporary Chinese Filmmakers
|publisher=Columbia University Press
|isbn=0-231-13330-8
|url=http://books.google.com/books?id=0PF21mv1vF0C&pg=PA428&lpg=PA428}}
*{{cite book|first=Poshek
|last=Fu
|author2=David Desser
|title=The Cinema of Hong Kong: History, Arts, Identity
|publisher=Cambridge University Press
|year=2000
|url=http://books.google.com/books?id=sELZJ5vihJUC
|isbn=0-521-77602-3}}

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 