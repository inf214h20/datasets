The Jade and the Pearl
 
 
 
{{Infobox film
| name        = The Jade and the Pearl
| image       = The Jade and the Pearl poster.jpg
| director    = Janet Chun
| producer    =  
| starring    =  
| studio      =  
| released    =  
| country     = Hong Kong
| language    = Cantonese
}} 2010 Hong Emperor Motion Pictures.

==Cast==

===Main cast===
* Charlene Choi as Princess Yin (嫣公主)
* Raymond Lam as General Ching Hin (程騫)
* Joey Yung as Chuk Sam Leung (祝三娘), a thief who lives on the top of the mountain
* Wong Cho-lam as Ling Kam Hoi (凌感開), a storyteller
* Ti Lung as King (皇上)
* Sire Ma as Princess Sau (秀公主)
* Macy Chan as Princess Wah (樺公主)
* Christine Kuo as Princess Kuo (苟公主)
* Kathy Yuen as Princess Yuen (婉公主)
* Katy Kung as Princess Kung (欣公主)
* Jess Shum as Princess Ying (盈公主)
* Mavis Pan as Princess Ping (平公主)
* Tsui Ming as Princess Ko (高公主)
* Yung Kai Nei as Princess Kwai (貴公主)
* Lam Yuen Ha as Princess Heung (香公主)
* Sherry Chen as Chui Luk (翠綠), an odalisque
* Cilla Kung as To Hung (桃紅), an odalisque
* Chapman To as Eunuch Yeung (楊公公)
* Carlo Ng as Eunuch Cheung (張公公)
* Tanny Tien as Mrs. Ching (程夫人), Ching Hins mother
* Alex Man as Poon Wong (番王) Patrick Dunn as the "To Fa Wui" host (桃花會主持)
* Lam Chiu Wing as Lee Ngok Ba (李惡霸)
* 6 Wing as Sai Moon (西門)
* Tats Lau as Thief Tat (山賊達, a follower of Chuk Sam Leung Steven Cheung as Thief Fan (山賊凡), a follower of Chuk Sam Leung
* Lam Suet as Sing Fu (成虎, a follower of General Ching
* Wong You Nam as Sing Pau (成豹), a follower of General Ching
* JJ Jia as Yeing Fa (楊花), a follower of General Ching
* Kenny Kwan as Scholar Cheung (張公子)
* Ken Hung as Scholar Chu (朱公子)
* Matthew Ko as Scholar Wong (王公子)
* Benjamin Yuen as Scholar Ho (何公子) Hayama Go as an Envy State agent (番國特使)
* Anderson Junior as the Mayor (鎮長)
* Chow Chi Ho as Bandit Luk Kam Sing (土匪陸金勝)
* Wong Chi Bun as the artist (畫師)
* Lam Kwok Ping as an art appreciator (藝術欣賞者)
* Kung Siu Ping as a story listener (說書聽眾)

==External links==
*  
*  

 
 
 
 
 
 
 


 
 