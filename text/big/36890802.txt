You Can't Buy Luck
{{Infobox film
| name           = You Cant Buy Luck
| image          =
| caption        =
| director       = Lew Landers
| producer       =
| writer         =
| starring       = Onslow Stevens Helen Mack
| music          =
| cinematography =
| editing        = RKO Radio Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $86,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  
| gross          = $175,000 
}}
You Cant Buy Luck is a 1937 murder mystery film. It made a profit of $24,000. 

==Plot== New York cab business Louisville with his sister, where he meets Betty McKay (Helen Mack), a pretty teacher who scoffs at his philosophy.

She scolds him for wishing for rain on the day of the Derby to aid his horse, who runs best on a muddy track, because the orphans plan an outdoor party. Although it rains as wished, Sarcasm loses the Derby, and Joe is convinced that it was because the orphans were pulling against him. In an attempt to repair the damage before the Preakness Stakes|Preakness, Joe throws the orphans a lavish party, hiring clowns and other entertainment. To Bettys surprise, Joe is as excited as the children, and they fall in love.

After Sarcasm wins the Preakness, Joe returns to New York, where Jean is back from Europe. Joe tells her that he will not be seeing her any more because he is going to marry Betty, and she cajoles $50,000 from him as a final "luck insurance" payment. Before Joe shows up with the check, however, Paul arrives at Jeans apartment. They argue when he sees that she plans to run out on him with the money.  Jean threatens him with a gun, and during a scuffle, kills her.

Joe arrives at Jeans building, where Paul is waiting outside.  He lights the unsuspecting Joes cigarette and gives him the matchbook, then telephones the police and, posing as Joe, "confesses" that he just murdered Jean. Joe is tried for Jeans murder and convicted on circumstantial evidence, but escapes before his final lockup. Using Franks cab to get around, and with the help of Betty to question the many possible suspects, Joe tracks down Paul using the passenger lists of Jeans voyage.

Paul discovers the ploy and has Joe apprehended. Joe convinces the police to question Paul. By matching partial fingerprints from the crime scene to those left by Paul at the police station, Paul is implicated in the murder and confesses. Sure now that luck cannot be bought, Joe embraces Betty.

==References==
 

==External links==
*  
*  

 
 


 