The Wolf of Wall Street (1929 film)
{{Infobox film
| name           = The Wolf of Wall Street
| image          = Wolf of wall street 1929 film herald.jpeg
| image_size     = 300px
| alt            = 
| caption        = One of theatrical film heralds
| director       = Rowland V. Lee
| producer       = 
| writer     = Doris Anderson George Bancroft Olga Baclanova Nancy Carroll
| music          = Karl Hajos
| cinematography = Victor Milner
| editing        = Robert Bassler
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 George Bancroft, Olga Baclanova, and Nancy Carroll. The story and screenplay were written by Doris Anderson.  Originally made as a silent film, The Wolf of Wall Street was completely re-filmed with sound,  becoming Bancrofts first talkie. 

The plot concerns a ruthless trader (Bancroft) who corners the market in copper and then sells short, making a fortune and ruining the finances of himself and his friends.     

==Cast==
  George Bancroft as The Wolf
* Olga Baclanova as Olga
* Nancy Carroll as Gert
* Paul Lukas as David Tyler
* Arthur Rankin as Frank
* Brandon Hurst as Sturgess
* Paul Guertzman as Office Boy
* Crauford Kent as Jessup
 

 

Cast notes:
* Thomas A. Curran the early American silent film star plays an uncredited bit part.

==Reception==
Reception for the film was mixed. Life (magazine)|Life criticised the film for depending too much on its novelty value; the advertising ran "George Bancroft talks . . . Baclanova sings", and Life noted "there is the good news that George Bancroft has a fine screen voice", but felt the film lacked substance in the plot.  Film Daily wrote that "George Bancroft as the roughneck engineering a pool in Wall Street to get the sucker is immense, as usual," but complained of a lack of action and weak story.

==See also== The Wolf of Wall Street, a 2013 film of the same name but a different story/screenplay

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 