Lady Snowblood (film)
{{Infobox film
| name           = Lady Snowblood
| image          = Lady Snowblood (film).jpg
| image_size     = 
| caption        = Theatrical film poster Toshiya Fujita
| producer       = Kikumaru Okuda
| writer         = Norio Nagata   
| based on =  
| narrator       = 
| starring       = Meiko Kaji Ko Nishimura Toshio Kurosawa Masaaki Daimon
| music          = Masaaki Hirao
| cinematography = Masaki Tamura
| editing        = 
| distributor    = Toho
| released       =  
| runtime        = 97 min
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
| studio = Tokyo Eiga 
}} Toshiya Fujita and starring Meiko Kaji.  It is based on a manga called Lady Snowblood (manga)|Shurayukihime. It is the story of Yuki, a woman who seeks vengeance upon three people who raped her mother and killed her mothers husband and son.

==Plot==

The scenes of this film do not appear in chronological order. Each paragraph below represents a passage of the film in the order it appears.

In 1874, a baby is born in a womens prison. Her deathly-ill mother names her Yuki (snow).

A woman blocks the path of several men and a rickshaw, and kills them using a sword concealed in the handle of an umbrella. She tells the last man to die, Shirayama, that her name is "Shura Yuki Hime". She appears in a poor village looking for a man called Matsuemon, the leader of an underground organization, and asks him to find four people for her, having killed Shirayama for him.

In 1873, a teacher (Masaaki Daimon), his wife Sayo (Miyoko Akaza), and their son are attacked by four criminals. A woman named Kitahama Okono (Sanae Nakahara) holds Sayo while the three men, Takemura Banzō (Noboru Nakaya), Shokei Tokuichi (Takeo Chii), and Tsukamoto Gishirō (Eiji Okada) murder the teacher and his son and rape Sayo. Tokuichi secretly takes Sayo far away to work for him. Sayo kills him with a knife and goes to prison.

In the womens prison, after the baby is born, Sayo tells her story. She has seduced any prison guard she can to conceive the child. She tells the other women to raise the child for vengeance, then dies.

In Meiji 15 (1882), the child Yuki (Mayumi Maemura) trains in sword fighting with a priest called Dōkai (Kō Nishimura).

Yuki, now twenty, finds Banzōs daughter Kobue (Yoshiko Nakada). Kobue is a prostitute, and her father is now an alcoholic wreck with gambling debts. At a gambling house, Yuki plays cards with Banzō, who is caught cheating. He is about to be killed. Yuki persuades the owners to pardon him, then leads him to a beach and kills him.

Learning that Tsukamoto Gishirō has died, she visits his grave. She cuts the flowers and then the gravestone. Matsuemon and his friends notice that Gishiro died just when Yuki started trying to find him, three years before.

A man who saw her attack the grave, a reporter named Ryūrei Ashio (Toshio Kurasawa), follows Yuki. He questions her past. Dōkai tells him her story. Ashio writes and sells the story (which, on the screen, is told using frames from the original comic). Dōkai did this to get Kitahama Okono to reveal herself. Okono sends men to kidnap Ashio. They torture him for Yukis location, but he refuses to tell them. Yuki enters the estate and kills several of Okonos men. She enters the building and is fired upon by Okono.  While Yuki fights and kills Okonos men, Okono hangs herself. Yuki slices the corpse in half.
 masquerade ball and kills him, but the dead man is not really Gishirō. Ashio and Yuki find and follow the real Gishiro. Gishirō shoots Ashio. Wounded, Ashio stops him from shooting Yuki as she swings on a lamp between balconies. Yuki stabs through Ashio into Gishirōs chest. She then cuts Gishirōs throat, but as he dies he shoots her. He falls over a railing and onto the ground floor full of guests.

Yuki, wounded, stumbles outside. Kobue suddenly appears. She stabs Yuki then runs away. Yuki falls on her face. The film ends with Yuki opening her eyes the next morning.

==Cast==

* Meiko Kaji as Yuki Kashima, aka Lady Snowblood
* Ko Nishimura as Priest Dōkai
* Toshio Kurosawa as Ryūrei Ashio
* Masaaki Daimon as Gō Kashima
* Miyoko Akaza as Sayo Kashima
* Eiji Okada as Gishirō Tsukamoto
* Sanae Nakahara as Kitahama Okono
* Noboru Nakaya as Takemura Banzō
* Takeo Chii as Shokei Tokuichi
* Hitoshi Takagi as Matsuemon
* Mayumi Maemura as Young Yuki
* Akemi Negishi as Tajire no Okiku

==Production==

The film was produced on a low budget and filmed with a minimal length of film (20,000 feet). At one point a special effect blood spatter went wrong, covering Meiko Kaji in fake blood.   

==Sequel, remakes, and homages==
The film spawned one sequel,  . A 2001 science fiction remake, The Princess Blade, stars Yumiko Shaku.

It was also a major inspiration for Quentin Tarantinos Kill Bill, which borrows plot, characters, visual motifs and settings. In particular the character O-Ren Ishii is based on Lady Snowblood. The scene in which The Bride fights O-Ren Ishii uses a snowy landscape that echoes scenes in Lady Snowblood. The theme song, "Shura No Hana", sung by Meiko Kaji (translated by Tarantino as "The Flower of Carnage") is also used in Kill Bill, Vol. 1. According to Meiko Kaji, Quentin Tarantino made the staff of Kill Bill watch DVDs of Lady Snowblood during break times. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 