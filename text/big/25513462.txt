Jerry and Tom
 

{{Infobox Film
| name = Jerry and Tom
| director = Saul Rubinek
| producer = {{plainlist|
* Elinor Reid
* Vivienne Leebosh
* Michael Paseornek
* Saul Rubinek
}}
| writer = Rick Cleveland
| starring = {{plainlist|
* Joe Mantegna
* Sam Rockwell
* Maury Chaykin
* Ted Danson
* Charles Durning
* William H. Macy
* Peter Riegert
}}
| music = David Buchbinder
| cinematography = Paul Sarossy
| editing = Sloane Klevin
| studio = Lions Gate Films
| distributor = Lions Gate Films Miramax Films
| released =  
| runtime = 107 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Jerry and Tom is a 1998 American black comedy film directed by Saul Rubinek in his directorial debut.  The screenplay was adapted by Rick Cleveland from his own 1994 one-act play.  The film stars Joe Mantegna and Sam Rockwell as contract killers who work at a used car dealership.  The film premiered at Sundance in January 1998, and, after Miramax declined to release it theatrically, aired on Showtime.

==Plot==
Hit men Tom and Jerry wait in a Chicago bar for the go-ahead to kill Stanley.  Anxious to get the hit done, Jerry suggests they perform the hit immediately; Tom refuses, and Stanley entertains them with jokes.  As the phone rings, the film shifts to a flashback set ten years earlier, in 1984.  Jerry, a youth employed at Billys used car dealership, joins Tom as he walks with Karl.  Karl describes how a local gangster bit off the nose of Karls nephew.  Disturbed by the audaciousness and brutality of the attack, Tom promises to look into the issue.  However, Karl becomes spooked when they enter Toms car, and Karl begins bargaining with Tom, to Jerrys confusion.  Tom agrees that the two are friends with a long history but strangles Karl after an apology.  Jerry throws up but insists that he is otherwise fine.

Toms next hit involves a trip out of state.  Jerry tags along, and when he becomes surly, Tom allows him to accompany him to the site of the hit itself, a cinema.  There, they meet an unnamed man who becomes annoyed with their loud conversation in which Jerry describes a domestic dispute with his girlfriend, Deb.  The man in the cinema tells them the story about how he and his fiancée, Vicki, the star of the low budget action film he is watching, angered the mob.  After hit men assassinate Vicki via a weapons mishap on the set, he goes into hiding and loses his will to live.  Moved by the story, Jerry decides to marry his girlfriend, and the two hit men rise to leave.  Jerry is surprised when Tom slits the throat of the man, and Tom admonishes him to not get so emotionally involved in the stories of their victims.
 The Killers.  Enthused by the thought of following in Reagans footsteps, Jerry is able to relax and make his first kill.  Later, as the two wait for their next hit, Tom tells Jerry about their co-worker, Vic, who is rumored to have had an affair with Marilyn Monroe and assassinated John F. Kennedy.  When the target arrives, Jerry asks to make the hit himself, and Tom impatiently watches as Jerry awkwardly attempts to make a clean kill.  After Jerry simply pulls out the pistol and shoots the man head-on, they leave.

Tom and Jerry join Vic for a meal, which Vic has prepared himself.  During the conversation, Vic reveals that he intends to publish his memoirs.  After Vic describes a feature film adaptation and talk show appearances that he has planned, the trio argue over the appropriateness of such actions and who should play each of them in a film.  Tom suggests Don Knotts for Jerry, who becomes offended that the others do not take him seriously.  Jerry pulls out a syringe and explains that he is diabetic, only to suddenly stab Vic with it.  Surprised and angered, Tom castigates Jerry for killing Vic in such an undignified manner, as they had previously planned to take Vic out back and shoot him after he finished with his dinner, a death that Tom considered more proper for an assassin of Vics stature. 

In their final hit, Jerry panics and accidentally murders an innocent bystander.  He then tells a disturbed Tom of how he occasionally fantasizes about killing his family, eventually admitting that he has pointed a loaded weapon at his infant sons head.  In the present, the phone rings, and Tom reluctantly answers it; he is given the go-ahead and kills Jerry.  After Tom unties Stanley, Stanley reveals that he has been tasked with killing Tom.  The film ends as Tom offers to tell a joke to Stanley.

==Cast==
* Joe Mantegna as Tom
* Sam Rockwell as Jerry
* Maury Chaykin as Billy
* Ted Danson as The Guy Who Loved Vicki
* Charles Durning as Vic
* William H. Macy as Karl
* Peter Riegert as Stanley
* Sarah Polley as Deb
* Shelley Cook as Vicki Torrance

==Production==
Director Rubinek described the film as influenced by Harold Pinter.  He stated that he wanted to avoid showing gratuitous violence onscreen in order to comment on its use in other films.   

==Release== Showtime picked it up and aired it in November 1999.   It was released in the US on home video in August 2000. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 75% of eight surveyed critics gave the film a positive review; the average rating was 8.5/10.   Todd McCarthy of Variety (magazine)|Variety called it "very well acted and beautifully directed".   Nathan Rabin of The A.V. Club called it a superior Quentin Tarantino knockoff that "is still just another knockoff". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 