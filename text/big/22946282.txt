A Scream from Silence
{{Infobox film
| name           = A Scream from Silence
| image          = AScreamFromSilence1979Poster.jpg
| caption        = Film poster
| director       = Anne Claire Poirier
| producer       = Jacques Gagné Anne Claire Poirier
| writer         = Anne Claire Poirier Marthe Blackburn
| starring       = Julie Vincent
| music          =
| cinematography = Michel Brault
| editing        = André Corriveau NFB
| released       =  
| runtime        = 96 minutes
| country        = Canada
| language       = French
| budget         =
}}
 Best Foreign Language Film at the 52nd Academy Awards, but was not accepted as a nominee. 

==Plot==
A young nurse named Suzanne is kidnapped, assaulted and brutally raped in the back of a truck by a woman-hating stranger. The nurses rape and the aftermath are part of a film which a director and her editor are working on. From time to time they pause the film to discuss their intentions and reactions to the film they are making. Mixed in with the story is documentary footage of the stories of other women who have been raped around the world.

==Cast==
* Julie Vincent as Suzanne
* Germain Houde as Le violeur
* Paul Savoie as Philippe
* Monique Miller as La réalisatrice
* Micheline Lanctôt as La monteuse
* Luce Guilbeault as Une cliente
* Christiane Raymond as La disciple
* Louise Portal as La comédienne
* Muriel Dutil as Lepouse (as Murielle Dutil)
* Julie Morand as La secrétaire
* Pierre Gobeil as Le policier
* Jean-Pierre Masson as Voice
* Michèle Mercure as La soeur de Philippe
* Léo Munger as Une violée
* André Pagé as Le gynécologue

==See also==
* List of submissions to the 52nd Academy Awards for Best Foreign Language Film
* List of Canadian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 