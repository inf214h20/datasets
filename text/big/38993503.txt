Aal (film)
{{Infobox film
| name         = Aal
| image        = Aal Poster.jpg
| caption      = Film poster
| director     = Aanand Krishna
| producer     = VidiyalRaju
| cinematography = N.S.Uthaya Kumar
| editing      = M.Ramesh Bharathi
| starring     = Vidharth
| music        = Johan
| studio       = Shoundaryan Pictures
| released     =  
| country      = India
| language     = Tamil
}}
 Tamil thriller film written and directed by Aanand Krishna and produced by Vidiyal Raju. It stars Vidharth (actor)| Vidharth and Harthika Shetty in the lead roles. The background score and soundtrack were composed by Johan. The film was released on 19 September, 2014. The film is a remake of 2008 Hindi film Aamir_(film)|Aamir.

==Plot==
It is about the life of an Ordinary man Facing an extraordinary Situation in a Single day.

==Cast==
* Vidharth as Amir
* Hardika Shetty as Meenakshi.

==Production==
The makers of the film noted that they managed to shoot the film in parts in Parrys Corner, making it the first film to do so in forty years. Similarly Anna Salai was used as a filming location for the first time in nearly twenty years.   Scenes were also shot in Gangtok in Sikkim, a location never ever shown before in Tamil films.  
The tagline of the film is ‘Who says a man writes his own destiny?’

==Soundtrack==
*Androru Naal
*Dammal Dammal
*My Name is Aamir (Theme music)
*Hot Freeze (Theme music)
*One Mans blood (Theme music)
*Oor Aal
*Poda Po
*Red Suitcase (Theme music)
*Leader (Theme music)

==References==
 


==External links==
*  

 
 
 
 
 

 