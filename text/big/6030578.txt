Obsession: Radical Islam's War Against the West
{{Infobox film
| name = Obsession: Radical Islams War Against the West
| image = ObsessionRadicalIslam.jpg
| caption = DVD cover art
| writer = Wayne Kopping Raphael Shore
| editing = Wayne Kopping
| starring = 
| director = Wayne Kopping
| producer = Peter Mier Raphael Shore
| distributor = Clarion Fund
| released =  
| runtime =  Persian
| budget =  
}} radical Islam Western civilization. Arab television footage depicting Islamic radicals preaching hate speech and seeking to incite global jihad.  It also draws parallels between World War IIs Nazi movement and Islamism and the Wests response to those threats. 
  
 The Clarion Fund, the films distributor.  The movie has received praise from some personalities in the media such as Glenn Beck and Sean Hannity,  Islamophobic with a political agenda, and for its association with pro-Israeli groups.

Segments of the movie were broadcast on CNN Headline News and in several specials on Fox News. The movie was also screened on 30 college campuses and Capitol Hill.  The unusual distribution of 28 million free Obsession DVDs as an insert in over 70 newspapers predominantly in swing states before the United States 2008 presidential election, garnered much attention, with 5 newspapers refusing to distribute the DVD.  National Public Radio reported that it was unclear as to who funded Clarions distribution of the DVD. 

==Plot==

The movie begins with the following statement: 
{{cite news
 | url = http://www.jacksonville.com/tu-online/stories/091708/opi_333084896.shtml
 | title = Terrorism: Looking for context
 | work = The Florida Times-Union
 | date = 2008-09-17
 | location = Jacksonville, Florida
 | accessdate = 2011-05-15
}} 
 

The film uses many images from Arab TV, provided by the Middle East Media Research Institute and Palestinian Media Watch.

==Contributors==
The following people were interviewed in the movie: {{cite web
 | url = http://www.obsessionthemovie.com/about_interviews.php
 | title = Interviewees
 | work = Obsession the movie
 | year = 2008
 | accessdate = October 8, 2008
}}  
{{cite web
 | url=http://www.obsessionforhate.com/thepundits.php
 | title = The Pundits
 | year = 2008
 | work = Obsession For Hate
 | publisher = Hate Hurts America
 | accessdate = October 8, 2008
}} 

*Nonie Darwish – Egyptian-American human rights activist
*Alan Dershowitz – American lawyer, jurist, and political commentator
*Steven Emerson – American journalist and author
*Brigitte Gabriel – Lebanese American journalist, author, and activist
*Martin Gilbert – British historian and author
*Caroline Glick – American-Israeli journalist
*Alfons Heck – was interviewed as a former member of Hitler Youth
*Glen Jenvey – British journalist John Loftus – American author, former US government prosecutor and former Army intelligence officer
*Salim Mansur – Indian-Canadian columnist
*Itamar Marcus – Israeli political activist, founder and director of Palestinian Media Watch
*Daniel Pipes – American writer, and political blogger
*Tashbih Sayyed – Pakistani-American scholar, journalist, and author
*Walid Shoebat – Palestinian immigrant to the United States, interviewed as a former PLO militant
*Khaled Abu Toameh – Israeli Arab journalist and documentary filmmaker
*Robert Wistrich – professor of European and Jewish history at the Hebrew University of Jerusalem
*Khaleel Mohammed
Khaleel Mohammed, an associate professor of religious studies 
{{cite web
 | url = http://www-rohan.sdsu.edu/~khaleel/
 | title = Welcome to Prof. Khaleel Mohammeds Web site
 | last = Mohammed
 | first = Khaleel
 | authorlink = Khaleel Mohammed
 | publisher = San Diego State University
 | accessdate = October 13, 2008
}}  at San Diego State University and the only Islamic Studies Professor interviewed 
{{cite web
 | url = http://www.obsessionthemovie.com/about_interviews_Mohammed.php
 | title = Khaleel Mohammed, PhD
 | year = 2008
 | publisher = obsessionthemovie.com
 | accessdate = October 13, 2008
}} 
discussed the meaning of jihad and its misuse by extremists. {{cite news
 | url = http://observers.france24.com/en/content/20081013-swing-states-anti-muslim-propaganda-28-m-obsession-raphael-shore
 | title = Swing states targeted with "anti-Muslim propaganda"
 | date = October 13, 2008
 | work = Observers
 | publisher = France 24
 | accessdate = October 13, 2008
}}  Mohammed later distanced himself and apologised for his participation claiming he had believed the film would be "used objectively, focusing on (the) fanatics who seek to spread violence" rather than Islam itself.

==Production==
Wayne Kopping co-wrote, directed, and edited the film. 
{{cite web
 | url = http://www.obsessionthemovie.com/about_filmakers.php
 | title = The Filmmakers
 | publisher = Obsessionthemovie.com
 | year = 2008
 | accessdate = October 12, 2008
}}  The Clarion Fund. 
{{cite web
 | url = http://www.radicalislam.org/content/about-clarion-fund
 | title = Clarion Fund: National Security Through Education
 | year = 2008
 | publisher = Clarion Fund
 | location = New York City
 | accessdate = 2008-10-29
}}   Kopping and Shore previously collaborated on  . Brett Halperin, named as the production manager, is an alias according to Shore. 
{{cite web
 | url = http://www.lrb.co.uk/v30/n19/shtz01_.html
 | title = Short Cuts
 | last = Shatz
 | first = Adam
 | work = London Review of Books
 | date = October 9, 2008
 | accessdate = October 3, 2008
}}  
{{cite news
 | url = http://www.haaretz.com/hasen/spages/873843.html
 | title = Obsession stokes passions, fears and controversy
 | last = Berman
 | first = Daphna
 | work = Haaretz
 | date = June 28, 2007
 | quote = But Mier and Halperin are just aliases, Shore says.
 | accessdate = October 7, 2008
}} 

Executive producer Peter Mier, an alias for an unnamed Canadian Jewish businessman, provided about 80 percent of the films $400,000 budget, according to Raphael Shore.  
According to IRS documents obtained by the Center for Investigative Reporting, financial support for the film came from an organization named Castello Limited. 
{{cite web
 | url = http://centerforinvestigativereporting.org/files/Clarion2.pdf
 | archiveurl= http://www.webcitation.org/5bTbWUUoJ
 | archivedate= 2008-10-10
 | title = Clarion Fund correspondence with Internal Revenue Service
 | last = Kabat
 | first = Rebecca
 | date = July 23, 2007
 | format = pdf
 | publisher = Center for Investigative Reporting
 | quote = The film Obsession was produced by Castello Limited.
 | accessdate = October 10, 2008
}}  conservative donor advised fund, granted $17.7 million to The Clarion Fund. ”One of our clients made a recommendation for Clarion and so we did it,” Whitney Ball, president of Donors Capital Fund, told Salon (website)|Salon, the news website.  

==Promotion and screenings==
The film was initially promoted via the internet by  . October 29, 2008.   Later when the film was released, HonestReporting promoted the film on its website describing it as an "affiliate" project.  The media watch group, in an interview with The Jewish Week, says it was not involved in the films production. 

CNN Headline News has shown segments of the film. 
Fox News has shown and hosted on its website segments of the film. 
{{cite news
 | url = http://www.foxnews.com/story/0,2933,226482,00.html
 | title = Documentary Portrays Islamic Extremists Call to Arms Against the Free World
 | accessdate = September 14, 2008
 | date = November 3, 2006
 | work = Fox News Channel
}} 
Seven times in November 2006, Fox News showed a one-hour special incorporating segments from the film. 
{{cite news
 | url = http://www.ipsnews.net/news.asp?idnews=37090
 | title = Film on "Radical Islam" Tied to Pro-Israel Groups
 | date = March 26, 2007
 | last = Akhavi
 | first = Khody
 | agency = Inter Press Service
 | accessdate = October 15, 2008
}} 

The documentary has been screened on 30 major campuses including Hofstra University|Hofstra, Pace University|Pace, University of Southern California|USC, UCLA, New York University|NYU, 
{{cite news
 | url = http://www.nytimes.com/2007/02/26/movies/26docu.html?_r=1&ei=5087&em=&en=a4062496f8729e35&ex=1172638800&pagewanted=print
 | title = Films View of Islam Stirs Anger on Campuses
 | last = Arenson
 | first = Karen W
 | authorlink = Karen Arenson
 | work = The New York Times
 | date = February 26, 2007
 | accessdate = October 14, 2008
}} 
and McGill University|McGill. 
{{cite web
 | url = http://www.takingliberties.ca
 | title = Taking Liberties
 | date = February 9, 2006
 | location = Montreal, Canada
 | accessdate = October 17, 2008
}}  
{{cite news
 | url = http://www.jewishtribune.ca/tribune/PDF/jt061130.pdf
 | format = pdf
 | title = Obsession screened to full house at McGill
 | last = Smajovits
 | first = Daniel
 | date = November 30, 2006 The Jewish Tribune
 | publisher = Bnai Brith Canada
 | accessdate = October 17, 2008
}} 
 Chief Deputy Majority Whip, and Debbie Wasserman Schultz (D-FL) co-sponsored a screening of the film on Capitol Hill.  Daniel Cantor Islamic Jihad suicide bomber. 
{{cite web
 | url = http://www.house.gov/list/press/fl20_schultz/ObsessionMovieScreening.html
 | title = Reps. Cantor, Wasserman Schultz Statement on Capitol Hill Screening of Obsession: Radical Islams War Against the West
 | date = December 5, 2006
 | last = Cantor
 | first = Eric
 | authorlink = Eric Cantor
 |author2=Wasserman Schultz, Debbie |authorlink2=Debbie Wasserman Schultz 
 | publisher = United States House of Representatives
 | accessdate = October 18, 2008
}} 

==Distribution==
Following the failure of traditional film distributors to pick up Shores films, The Clarion Fund retains a non-exclusive agreement to distribute Obsession, Relentless and The Third Jihad.  

The film was initially distributed to college campuses in 2007 and in September 2008, the Clarion Fund, in cooperation with the Endowment for Middle East Truth, 
{{cite news
  
 | url = http://www.ipsnews.net/print.asp?idnews=43983   
 | title = Neo-cons, Ex-Israeli Diplomats Push Islamophobic Video
 | agency = Inter Press Service
 | last = Gharib
 | first = Ali
 |author2=Clifton, Eli
 | date = September 24, 2008
 | accessdate = October 7, 2008
}}  distributed 28 million DVDs of the film by mail, 
{{cite news
 | url =http://www.pennlive.com/patriotnews/stories/index.ssf?/base/news/122110351948500.xml&coll=1
 | title = DVD raises awareness, its source says
 | last = Cassidy
 | first = Carrie
 | date = September 11, 2008
 | publisher = PennLive.com
 | work = The Patriot-News
 | location = Harrisburg, Pennsylvania
 | accessdate = October 18, 2008
}} 
and in newspaper advertising supplements, predominantly in swing states.  
{{cite news
 | url = http://www.editorandpublisher.com/eandp/columns/shoptalk_display.jsp?vnu_content_id=1003849752 
 | title = Delivering Propaganda, As If It is Toothpaste
 | last = Jackson
 | first = William E., Jr.
 | date = September 13, 2008
 | work = Editor & Publisher
 | publisher = Nielsen Business Media
 | accessdate = October 18, 2008
}} 
 churches and synagogues in the United States.   

The DVD was also distributed to all 30,000 members of the Republican Jewish Coalition. 
 News & Observer, 
{{cite journal
 | url = http://www.editorandpublisher.com/eandp/news/article_display.jsp?vnu_content_id=1003849746
 | title = Newspapers Deliver Millions of Terror DVDs to Subscribers – In Swing States
 | last = Mitchell
 | first = Greg
 | authorlink = Greg Mitchell
 |author2=Strupp, Joe
 | work = Editor & Publisher
 | publisher = Nielsen Business Media
 | date = September 13, 2008
 | accessdate = October 29, 2008
}} 
The Chronicle of Higher Education, 
and The Oregonian. 
{{cite web
 | url = http://www.editorandpublisher.com/eandp/news/article_display.jsp?vnu_content_id=1003855892
 | title = Oregonian Distributes Muslim Terror DVD – After Mayor Asks It to Refrain
 | date = September 28, 2008
 | work = Editor & Publisher
 | publisher = Nielsen Business Media
 | accessdate = October 3, 2008
}}  
{{cite news
 | url = http://www.oregonlive.com/politics/oregonian/index.ssf?/base/news/1222494903175240.xml&coll=7
 | archiveurl= http://www.webcitation.org/5bPTpzR8o  |archivedate=October 8, 2008
 | title = Critics call DVD insert unfair hit on Islam
 | last = Graves
 | first = Bill
 | date = September 28, 2008
 | work = The Oregonian
 | publisher = Oregon Live
 | quote = I could find no reason to reject this.
 | accessdate = October 6, 2008
}}  The New York Times distributed approximately 145,000 DVDs in their national edition to 
Denver, Colorado|Denver,
Miami, Florida|Miami, Tampa, Florida|Tampa, Orlando, Florida|Orlando, 
Detroit, Michigan|Detroit, Kansas City, St. Louis, Missouri|St. Louis,
Philadelphia, Pennsylvania|Philadelphia, and Milwaukee, Wisconsin|Milwaukee. 
{{cite news
 | url = http://www.ipsnews.net/news.asp?idnews=43940
 | title = Anti-Islam Film Targets "Swing State" Voters
 | last = Gharib
 | first = Ali
 | date = September 19, 2008
 | agency = Inter Press Service
 | quote = The documentary, despite an initial disclaimer that the material covered applies only to radical Islamists and not all Muslims....
 | accessdate = October 12, 2008
}}  According to a News & Observer blog post, whether the advertisement should be accepted was discussed, but publisher Orage Quarles made the "ultimate decision". The newspapers vice president of display advertising noted, "Obviously, we have distributed other product samples, whether its cereal or toothpaste." 

Many of the newspapers distributing the DVD also published articles about the film, including
The Morning Call 
of Allentown, Pennsylvania,
The Charlotte Observer, 
The News-Press 
{{cite news
 | url = http://www.news-press.com/apps/pbcs.dll/article?AID=/20080914/NEWS01/80913033/1075
 | title = DVD inside The News-Press controversial| accessdate = September 14, 2008
 | last = Myers
 | first = Rachel
 | date = September 14, 2008
 | work = The News-Press
 | location = Fort Myers, Florida
}}   
of Fort Myers, Florida,
and The News & Observer 
{{cite news
 | url = http://www.newsobserver.com/news/story/1217021.html
 | title = Controversial film on Islam delivered nationwide
 | last = Shimron
 | first = Yonat
 | date = September 13, 2008
 | work = The News & Observer
 | location = Raleigh, North Carolina
 | publisher = The McClatchy Company
 | accessdate = October 14, 2008
}}   
of Raleigh, North Carolina. Newspapers that refused to distribute the DVD included the St. Louis Post-Dispatch, 
    
the Detroit Free Press, the News & Record of Greensboro, North Carolina, and The Plain Dealer of Cleveland, Ohio. 
{{cite web
 | url=http://www.obsessionforhate.com/thepapers.php
 | title = The Papers
 | work = Obsession For Hate
 | year = 2008
 | publisher = Hate Hurts America
 | accessdate = October 8, 2008
}}  
{{cite news
 | url = http://ap.google.com/article/ALeqM5h1Rd6z2Qk3Fe45zgkt9gKSEiE46gD93JDKKO0
 | title = Newspapers get complaints for DVD ad on Muslims
 | last = Jesdanun
 | first = Anick
 | agency = Associated Press
 | date = October 4, 2008
 | quote = The Detroit Free Press, The Plain Dealer of Cleveland and the St. Louis Post-Dispatch also declined to carry the ad.
 | accessdate = October 7, 2008
}}  News & Record president and publisher Robin Saul said:
"It didnt meet our advertising standards. We were told its purpose was educational. We didnt see it as educational at all. It was fear-mongering and divisive." 
{{cite web
 | url = http://blog.news-record.com/staff/jrblog/2008/09/why_we_didnt_di.shtml
 | title = Why we didnt distribute "Obsession"
 | last = Robinson
 | first = John
 | work = The Editors Log
 | publisher = Greensboro News & Record
 | location = Greensboro, North Carolina
 | date = September 21, 2009
 | accessdate = October 12, 2008
}}  
{{cite news
 | url = http://www.editorandpublisher.com/eandp/news/article_display.jsp?vnu_content_id=1003850083
 | title = One Newspaper Refuses to Distribute Islam Terror DVD
 | last = Mitchell
 | first = Greg
 | authorlink = Greg Mitchell
 | date = September 15, 2008
 | work = Editor & Publisher
 | publisher = Nielsen Business Media
 | quote = The longtime editor, John Robinson, explained his reasoning in a column yesterday.
 | accessdate = October 13, 2008
}}  The editor John Robinson wrote: "As a journalist, my default position is to provide people with more knowledge, however troubling, rather than less. Were this truly an issue of the freedom of information, I would have argued to publish. But this was a paid advertisement presenting one side of an inflammatory issue." 

New Films International acquired the film for international distribution. 
{{cite web
 | url = http://www.newfilmsint.com/movieDetails.aspx?id=99
 | title = Obsession (Radical Islams War Against The West)
 | year = 2007
 | publisher = New Films International
 | location = Sherman Oaks, California
 | accessdate = October 23, 2008
}}   
<!-- New Films International seems also know as G-Machine (see http://www.imdb.com/company/co0230074/) http://www.newfilmsint.com/movies.aspx?t=2
-->

==Reception==
According to the documentarys website, the film was received enthusiastically. {{cite web
 | url = http://www.obsessionthemovie.com/
 | title = Official website
 | publisher = Obsessionthemovie.com
 | year = 2008
 | accessdate = February 10, 2011
}}  Glenn Beck (CNN) described "Obsession" as "one of the most important movies of our lifetime". {{cite web
 | url = http://obsessionthemovie.com/media_CNNglennbeck2.html
 | title = Glenn Beck – CNN headline News
 | year = 2008
 | accessdate = February 10, 2011
}}  Emmett Tyrrell (CNN) wrote that "Obsession," is "one of the most riveting films about the struggle the civilized world faces", 
{{cite news 	
| url = http://www.cnn.com/2006/POLITICS/03/09/tyrrell.oscars/index.html
| title = R. Emmett Tyrrell, CNN.com: Hollywoods blind spot
| year = 2006
| accessdate = May 15, 2011
}}  while Kyra Phillips encouraged CNN Newsroom viewers to see the movie which according to her "provides an incredible education".  Additional endorsements were published on Fox News, {{cite news
 | url = http://www.foxnews.com/story/0,2933,226482,00.html#
 | title = Documentary Portrays Islamic Extremists Call to Arms Against the Free World
 | accessdate = February 10, 2011
 | work=Fox News
 | date=November 3, 2006
}}  on Townhall, {{cite web
 | url = http://townhall.com/columnists/EmmettTyrrell/2006/03/09/obsession
 | title = Obsession
 | year = 2006
 | work = Townhall.com
 | accessdate = February 10, 2011
}}  in National Review, {{cite web
 | url = http://www.nationalreview.com/articles/225773/new-censors-i-obsession-i/seth-leibsohn
 | title = New Censors’ Obsession
 | year = 2008
 | accessdate = February 10, 2011
}}  in FrontPage Magazine, {{cite web
 | url = http://archive.frontpagemag.com/readArticle.aspx?ARTID=3351
 | title = Obsession: Radical Islams War Against the West
 | year = 2006
 | accessdate = February 10, 2011
}}  in The Times Gazette (OH)  {{cite web
 | url = http://www.timesgazette.com/main.asp?SectionID=1&SubSectionID=1&ArticleID=156942
 | title = Obsession is required viewing
 | year = 2008
 | accessdate = February 10, 2011
}}  and on conservative radio programs such as The Rush Limbaugh Show, the highest-rated talk radio show in the United States. {{cite web
 | url = http://obsessionthemovie.com/media_press_radio.html
 | title = Obsession – on radio programs
 | year = 2008
 | accessdate = February 10, 2011
}} 
 Islamophobic with a political agenda. Initially supplied to college campses for free screenings, the DVD ignited controversy with 30 airing the film while several declined including the State University of New York at the request of Jewish groups. Students attending the screenings at New York University were required to register with IsraelActivism.com with photographs of the event forwarded to Hasbara Fellowships, an organization that brings students to Israel and trains them to be effective pro-Israel activists on college campuses. The forwarding of names was criticised for stifling dissent and intimidating people. 
 2008 United States presidential election which was seen by many as an attempt to influence the election. The timing of the release and the unrevealed funding for the distribution, estimated to have cost around $50 million, has stimulated controversy and speculation. 
{{cite news
 | url = http://www.cnn.com/2008/US/10/14/muslim.dvd/index.html?iref=mpstoryview
 | title = Muslim DVD rattles voters in key battleground states
 | last = Feyerick
 | first = Deborah
 | authorlink = Deborah Feyerick
 |author2=Steffen, Sheila
 | date = October 15, 2008
 | work = American Morning
 | publisher = CNN
 | accessdate = October 16, 2008
}}  

The film has been criticized for "portraying Islam as a threatening religion bent on the destruction of Western civilization, interspersing incendiary commentary with images of Nazis and suicide bombing indoctrination". 
{{cite news
 | url = http://cbs4.com/local/obsession.muslim.religion.2.818889.html
 | title = "Obsession" DVD Raises Concerns Over Propaganda
 | last = Varela
 | first = Ileana
 | date = September 16, 2008
 | work = WFOR-TV
 | location = Miami, Florida
 | publisher = CBS Television Stations
 | accessdate = October 12, 2008
}}   
The Jewish Telegraphic Agency wrote: "Producers of the documentary insist that it only targets a radical minority among Muslims; however, a number of the interviewees in the documentary are on the record as describing Islam as inherently prone to hegemony." 
{{cite news
 | url = http://www.jta.org/cgi-bin/iowa/breaking/110533.html
 | title = Islamic council wants probe of Obsession
 | date = September 25, 2008
 | agency = Jewish Telegraphic Agency
 | accessdate = October 8, 2008
}}  
 Agudas Achim Congregation, a Conservative synagogue in Alexandria, Virginia, described Obsession as "the protocols of the learned elders of Saudi Arabia."  Aish HaTorah has been criticized by Rabbi Moline over its close links with The Clarion Fund.   On the matter of the shared staff between Aish HaTorah and the Clarion Fund, Rabbi Moline was quoted as saying "It is distressing to me that they   would continue to have someone who has promulgated such awful, awful stuff sitting on their board or staff.” 

The left-wing group Hate Hurts America, 
{{cite web
 | url=http://www.hatehurtsamerica.org
 | title = Hate Hurts America
 | year = 2008
 | accessdate = October 8, 2008
}}  launched a campaign against the film titled "Obsession for Hate," calling it a "classic work of hate propaganda, thinly disguised as a critique of radicalism, that attempts to subliminally demonize Muslims and their faith wholesale."  
{{cite web
 | url=http://www.obsessionforhate.com/about.php
 | title = About
 | work = Obsession For Hate
 | publisher = Hate Hurts America
 | year = 2008
 | accessdate= October 8, 2008
}} 

In Dearborn, Michigan, local religious leaders called a free screening of the documentary on September 11, 2008 a divisive publicity stunt. 
{{cite news
 | url = http://www.freep.com/apps/pbcs.dll/article?AID=/20080911/NEWS05/809110352
 | title = Critics slam screening of Muslim documentary today
 | last = Warikoo
 | first = Niraj
 | work = Detroit Free Press
 | publisher = Freep.com
 | date = September 11, 2008
 | accessdate = October 7, 2008
}}   
Joe Wierzbicki 
{{cite news
 | url = http://www.npr.org/templates/story/story.php?storyId=95076174
 | title = Charity Floods Swing States With Anti-Islam DVD
 | last = Overby
 | first = Peter
 | work = Morning Edition
 | publisher = National Public Radio
 | date = September 26, 2008
 | accessdate = October 7, 2008
 | quote= Wierzbicki, the movie promoter, also works for two political organizations.
}}  of the King Media Group, 
{{cite web
 | url = http://www.kingmediagroup.com/Bios/JoeWierzbicki.asp
 | title = Joe Wierzbicki – Director of Public Relations
 | publisher = King Media Group
 | accessdate = October 7, 2008
}} 
Russo Marsh & Rogers, 
{{cite web
 | url= http://www.rmrwest.com/index.php/RMRWest/Principals/principlas.asp#JoeWierzbicki
 | title = Principals
 | publisher = Russo Marsh & Rogers
 | accessdate = October 7, 2008
}}  and the Our Country Deserves Better PAC, 
{{cite web
 | url = http://www.ourcountrydeservesbetter.com/whoweare/index.html
 | title = Board Members and Staff
 | publisher = Our Country Deserves Better PAC
 | accessdate = October 7, 2008
}}    said: "There is a problem with an acceptance of radical Islam in Dearborn more so than anywhere else than I know of," according to the Detroit Free Press, quoting Wierzbicki as a spokesman for a California-based public relations company hired to promote the film. 
Wierzbicki later said Right Reel,  a distributor of conservative films, hired him. 
{{cite web
 | url = http://www.npr.org/blogs/secretmoney/2008/10/clarion_answers_some_questions.html
 | title = Some Answers On Clarion, And Still Some Questions
 | last = Overby
 | first = Peter
 |author2=Evans, Will
 | work = The Secret Money Project
 | publisher = National Public Radio
 | date = October 7, 2008
 | quote = He said he was hired by Right Reel, a distributor of conservative films, but he expressed doubt that Right Reel was the ultimate funder of the screening.
 | accessdate = October 10, 2008
}} 

The Associated Press reports that the Council on American-Islamic Relations has asked for the Federal Election Commission to investigate the Clarion Funds DVD distribution claiming that it was an attempt to influence the 2008 US Presidential Election. As evidence of inappropriate political bias on the part of The Clarion Fund, AP cited Patriot News of Harrisburg, Pa. reporting "that a Clarion Fund Web site ran a pro-McCain article before it attracted notice and was taken down." Ari Morgenstern, a spokesman for Middle East Truth, said targeting swing states was designed to attract media attention, but is not meant to influence the election result. Gregory Ross, spokesman for the New York-based Clarion Fund stated: "We are not telling people who to vote for, were just saying no matter who gets in office, the American people should know radical Islam is a real threat to America. We dont feel radical Islam is getting its fair share of press." 
{{cite news
 | url = http://ap.google.com/article/ALeqM5iEsmhiNuC1Aio2bPql9Iw1z6I3ywD93COGEG1
 | title = Muslim group seeks probe of radical Islam DVD
 | last = Gorski
 | first = Eric
 | date = September 23, 2008
 | agency = Associated Press
 | accessdate = October 7, 2008
}} 

The Endowment for Middle East Truth withdrew support for promoting the film. 
{{cite news
 | url = http://www.jta.org/cgi-bin/iowa/breaking/110615.html
 | title = Think tank quits Obsession Project
 | date = October 2, 2008
 | agency = Jewish Telegraphic Agency
 | accessdate = October 14, 2008
}}   

After a showing on November 13, 2007 at the University of Florida, the Gainesville campus was rocked by controversy over the film.  A forum entitled "Radical Islam Wants You Dead" was sponsored by Law School Republicans, prompting Patricia Telles-Irvin, the Universitys vice president of student affairs, to call for apology for "promoting a negative stereotype". 
{{cite news
 | url = http://www2.tbo.com/content/2007/dec/13/na-suppressing-speech-on-campus-hurts-universitys-/
 | title = Suppressing Speech On Campus Hurts Universitys Prestige
 | date = December 13, 2007
 | work = The Tampa Tribune
 | location = Tampa, Florida
 | accessdate = October 17, 2008 constitutional grounds and called for Patricia Telles-Irvins replacement. 

Think tank researcher Jennifer Bryson gave the film a mixed review, saying that while it does show how, "Islamist radicalism poses a grave threat to the freedoms of constitutional democracies, the film largely ignores potential solutions and a host of moderate Islamic voices that have gone unheard." 

==Awards==

The movie won awards at the Newport Beach Film Festival, the Liberty Film Festival and the WorldFest-Houston International Film Festival, a platform for Indie films.

==See also==
* Criticism of Islam
* Criticism of Islamism
*  
*  
* Fitna (film)|Fitna
* Iranium

==References==
 

==External links==
*  
*   Rebuttal of the film.
* Rabbi Haim Dov Beliak, Eli Clifton, Jane Hunter and Robin Podolsky   Jews On First November 2, 2008
*  

 
 
 
 
 
 
 
 
 