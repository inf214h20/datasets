Feather Duster
 

{{Infobox Hollywood cartoon|
| cartoon_name = Feather Duster
| series = Krazy Kat
| image = 
| caption =  Bill Nolan
| story_artist = 
| animator = 
| voice_actor = 
| musician = 
| producer = 
| studio = M.J. Winkler
| distributor = 
| release_date = c. 1925
| color_process = Black and white
| runtime = 5:50 English
| preceded_by = Dentist Love
| followed_by = A Picked Romance
}}

Feather Duster is a short animated film featuring Krazy Kat. It is one of the few Krazy films directed by Bill Nolan who started the third season after the character had two runs previously.

==Plot==
Inside a house, a maid is dusting some ornaments. She then puts down her duster, and goes somewhere. A woman enters the room. This woman finds the duster a good hat accessory before putting it on, and going away. The maid returns to the scene, and is shocked to see her duster missing. Krazy, who is wondering outside, hears her weeping, and comes inside. He feels sorry for her, and decides to help out.

Wondering the outside again, Krazy spots a turkey. But before he could take a feather, the turkey is able to lose him. Krazy then spots what appears to be another turkey but is actually a parasol.

After some trouble with an escaped prisoner, Krazy sees a feathery figure behind a lightpost. But it appears its actually a bomb-wielding spy. The spy hurls a bomb at Krazy which blasts the cat sky high. Krazy then lands on a buzzard, and tries to takes some tail feathers. The buzzard, however, tosses Krazy away before he could take any. Back on the ground, Krazy comes across a tent where a gremlin with a feather hat emerges. Krazy takes the feathers from the latters hat, and flees. The gremlin calls out a bigger companion who starts hurling darts at Krazy.

Krazy safely returns to the maids house. As he presents the new duster with the feathers he collected, the maid is overjoyed. The maid rewards Krazy with kisses and food.

 

 
 
 
 
 
 
 

 
 