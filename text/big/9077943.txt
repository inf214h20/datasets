A Chinese Ghost Story: The Tsui Hark Animation
 
 
{{Infobox film
| name = A Chinese Ghost Story: The Tsui Hark Animation
| image = AChineseghoststoryanim.jpg
| caption        = Andrew Chan
| producer = Tsui Hark Charles Heung Nansun Shi
| writer = Tsui Hark
| starring =
| music = Ricky Ho
| editing = Tsui Hark
| studio = Film Workshop Triangle Staff Golden Harvest  Wins Entertainment, Ltd.
| released = 1997
| runtime = 84 minutes
| country = Hong Kong Cantonese  Mandarin
| budget =
| gross =
| followed by =
}}
 Hong Kong animated film. It was written and produced by Tsui Hark and his production company, Film Workshop. The animation was produced by Japanese animation studio, Triangle Staff. It is also referred to as "Xiao Qian", "Little Pretty", "Chinese Ghost Story Xiao Qian".

==Background== CGI being used as graphical enhancements.  It was debuted at the Toronto Film Festival. The story is loosely based on a short story, titled Nie Xiaoqian, from the ancient Chinese literary work Strange Stories from a Chinese Studio by Pu Songling. It was dubbed into English by Ocean Productions and licensed by Viz Media and Geneon Entertainment.

==Story==
Tax collector Ning wanders the land with his pet dog Solid Gold grieving over his lost love Siu Lan, who dumps him over another man. He goes on, and along the way, he runs into two monks, White Cloud and Ten Miles. The two Buddhist monks, who appear to be trying to purify unholy spirits and send them to the underworld, are on rivalry with another ghostbuster, Red Beard. After a meeting and a hasty farewell to the monks who leave, Ning continues his journey.

Somehow, at night, Ning enters a ghost town, inhabited by many different monsters, ghouls and spirits. He tries to adjust himself in the street life there, but he realises that it is hard to fit in the life of the dead. Just then he gawks at a beautiful lady called Shine in a carriage scene like royalty and he falls in love with her. Unknown to him, Shine is an agent of Madame Trunk, an evil tree spirit who devours life forces from anything. Ning follows Shine and tries to acquaint with her, but Shine is at first cunning and was thinking of selling him to Madame Trunk, with help of her friend, Butterfly. But she realises that Ning is a man who seems very different from what she thought.

When mayhem breaks out in the ghost town, it turns chaotic. Ning and Shine are caught between it, and in the midst Ning saves Shine as they jump upon the Immortal Golden Dragon Train, that ferries souls to the underworld to be reborn. Shine suggests they hitch a ride to safety, and so they do, getting off sometime later.

When morning arrives, Ning notices Shine is missing. He tries looking for her, only to realise she was hiding in his shadow under sunshine. She is a ghost, and any exposure to the sun will make her disappear, as she explains to Ning, and so he protects her. Ning sets her in his umbrella, then casts the umbrella in the water of a pond where she can survive, while he stays on ground and camps. As they stay together, the two of them form a semi-romantic relationship. In the midsts, Ning and Shine encounter Red Beard. Red Beard wants to slay Shine as it his profession, but when the group is attacked by the two monks, they flee to safety, but Red Beards "robot" is destroyed to save them. Ning then pleads with Red Beard to not slay Shine, but after some time Red Beard relents and promises he wont do any harm to Shine.

While Shine is away, Butterfly is heavily questioned by Madame Trunk about where Shine is. Butterfly tries to deny the fact she knew anything, but eventually under torture, she reveals some information on Shine, with a human man who is Ning. Madame Trunk, who is running low on life force, is desperate to get Ning so she can live, and enlists Butterfly to get them.

At night, Ning and Shine return to the city, where a celebration is taking place. Mountain Evil, a mountain spirit, seen as a superstar, performs that night and charms Shine into staying on him. But when Shine pulls by accident one of his long, white, elegant hair that he claims nobody except him should touch, he is enraged. He tries to punish her, but the two monks appear and attack the ghost town. Ten Miles handles the ghost citizens, while White Cloud takes care of Mountain Evil. In the onslaught, Mountain Evils hair is completely shaven by White Cloud, who thinks that his hair should be shaven like his to repent. But Mountain Evil tries to flee back into his mountain form, dragging Shine with him. Shine manages to escape, where Ning and her settle in a forest.

Shine reveals her background about working for Madame Trunk, in which Ning says to free her he would do anything, even losing his life force to free Shine from Madame Trunks clutches. Shine takes him to Madame Trunk, but when she changes her mind at the last moment being unable to part with Ning and that by letting him be sacrificed for her freedom makes her selfish and Ning not in peace, she tries to save Ning. Butterfly understands the situation and when she realises humans can coexist with spirits, helps Shine by ensuring Ning and Shines escape. Because Madame Trunk has no more life force with Ning taken away from her, she rots and disappears, thus breaking the bond Shine is bound to her.

Shine admits she loves Ning since he helped her so much, and that Ning returns her love, but worries that humans and spirits cant be together. While they share a tranquil moment, Mountain Evil appears and tries to take Shine away for the punishment she was to have from him. They hide, and when the Immortal Golden Dragon Train arrives, they sought refuge in the train. Mountain Evil tries to attack the train, but is seen by Red Beard who destroys him. Ning asks Red Beard why he would do such a thing, Red Beard claims that he understood about spirits and ghostbusting the unholy ones are what he should do, and not just to bust all spirits.

Shine then suggests that she be reborn to stay with Ning together as they go through the Heavenly gates. Ning agrees that hell reborn to be with her, but in the midst, White Cloud and Ten Miles appear to purify the spirits themselves. Red Beard goes on a duel with them, while Ning and Shine alter the trains course to take them safely to the gates. In the scuffle that follows, Ning is thrown into the gates that are sealing. The path they took in fact made them too late. Ning struggles to keep the gates open as the train passes through him. Shine takes him and fly into the portal of rebirth, but they have to encounter the hammers of mismemory that will wipe out all spirits memories before they are reborn.

White Cloud, Ten Miles and Red Beard fall into the portal, where they are reborn into babies. Ning and Shine are separated, where Shine tells Ning the location to look for her when she is reborn. Ning awakens in a village where he sees the two baby monks and a baby Red Beard, and meets his lover Siu Lan who carries a newborn child which he thought was Shine. He takes the child to the meeting place, but realises the baby is a boy and Siu Lan Fu arrives, teaching Ning a lesson. Ning thought he had lost Shine forever, but when he sees his umbrella acting weird, he casts it in a rivers water and he dives in, just like before. Shine appears and the two of them reunite. An ecstatic Ning swims madly down the river as his dog Solid Gold smiles before the camera as the film ends, with the scene turned to a Chinese painting as the credits roll.

==Characters==
{|class="wikitable" Name
! Cantonese Voiced by Mandarin Voiced by English Voiced by
|-
| Ning || Jan Lamb || Nicky Wu || Michael Donovan
|-
| Siu Sin (Seen) / Shine || Anita Yuen || Sylvia Chang || Nicole Oliver
|-
| Siu Lan / Lan || Charlie Yeung || Linda Wong Hing || Janyse Jaud
|-
| Solid Gold || Tsui Hark || ? || Scott McNeil
|- Matt Smith
|- Raymond Wong Richard Newman
|- Don Brown
|-
| Siu Deep / Butterfly || Charlie Yeung || Liu Jo-Ying || Venus Terzo
|-
| Madame Trunk || Kelly Chen || Yon Fan || Shirley Millner
|-
| Mountain Evil || Jordan Chan || Lo Ta-Yu || Scott McNeil
|-
| Siu Lan Fu || Ronald Cheng || Ronald Cheng || ?
|-
|}

==Awards==
* Won the best animated film award in the 1998 Asia Pacific Film Festival.
* Won the Best Animated Feature award in the 1997 Golden Horse Film Festival
* Won the Film of Merit award in the 1998 Hong Kong Film Critics Society Awards.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 