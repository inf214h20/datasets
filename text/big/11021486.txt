Christmas at Maxwell's
{{Infobox film
| name           = Christmas at Maxwells
| image          = Christmas at Maxwells.jpg
| image size     = 
| alt            = 
| caption        = Promotional poster
| director       = William C. Laufer
| producer       = William C. Laufer Tiffany Laufer
| writer         = William C. Laufer
| narrator       =  Andrew May Jack Hourigan Helen Welch
| music          = Richard John Baker Mike Petrone
| cinematography = Tiffany Laufer
| editing        = Ronen Pestes
| studio         = Laufer Film Company
| distributor    = Aloha Releasing Inc. Laufer Film Company Virtual Film Distribution
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         = United States|$ 3,000,000
| gross          = 
| preceded by    = 
| followed by    = 
}}
 American independent independent drama Andrew May Jacqueline Jack Hourigan. {{cite news
|url=http://www.wkyc.com/news/regional/akron_article.aspx?storyid=103735&catid=6
|title=Another Cleveland Christmas movie: Christmas at Maxwells
|date=December 24, 2008
|publisher=WKYC
|accessdate=7 December 2009}}  {{cite news
|url=http://www.nytimes.com/2006/11/05/movies/moviesspecial/05nove.html
|title=December releases
|date=November 5, 2006
|publisher=New York Times
|accessdate=7 December 2009
| first=Dave
| last=Kehr}} 

==Plot==
Suzie Austin (Jack Hourigan) has cancer and her most recent prognosis is unfavorable.  Fearing that this may be their last Christmas together, husband Andrew Austin (Andrew May) takes Suzie and their two children, Chris (Charlie May) and Mary (Julia May) to the familys summer home on Lake Erie to celebrate the holiday.  There they meet Gus (Angus May). Andrew comes to terms with his past as the family deals with his wifes failing health.

==Background==
The film is based upon Laufers real-life experiences and was shot on locations in Ohio. {{cite web
|url=http://www.evliving.com/events.php?action=fullnews&id=6140
|title=Christmas at Maxwells to Debut 
|date=December 7, 2006
|publisher=East Valley Living
|accessdate=7 December 2009}}  {{cite news
|url=http://www.cbn.com/entertainment/screen/ElliottB_ChristmasatMaxwells.aspx
|title=review: Christmas at Maxwells
|last=Elliott 
|first=Belinda
|publisher=Christian Broadcasting Network
|accessdate=7 December 2009}}  Laufers daughter, Tiffany Laufer, an American Film Institute Alumna, served as cinematographer and co-producer. {{cite web
|url=http://www.dove.org/news.asp?ArticleID=89
|title=Father And Daughter Work On Family Film Together
|last=Carpenter
|first=Edwin L. 
|publisher=Dove Foundation
|accessdate=7 December 2009}}   An advance screening was held on November 28, 2006 with all ticket proceeds going to the American Cancer Society. {{cite web
|url=http://www.philanthropyjournal.org/archive/123226|title=Durham company invests in IT recycling company
|date=November 27, 2006|publisher=Philanthropy Journal
|accessdate=7 December 2009}}   The film had its official theatrical release on December 1, 2006, and its television debut on Christmas Day.

==Partial cast==
* Andrew May as Andrew Austin 
* Jack Hourigan as Suzie Austin 
* Helen Welch as Rachel Henderson 
* Rick Montgomery Jr. as Dr. Callahan 
* Tracie Field as Tootsie 
* Robert Hawkes as Col. Pickerling 
* Angus May as Uncle Gus 
* Charlie May as Chris Austin 
* Julia May as Mary Austin
* William C. Laufer as Fr. Johnston

==Reception== CBN wrote that even with its low budget, the film was "beautifully photographed with rich warming images of Christmas", but cautioned that themes of illness and death might be too heavy for young children.  She summarized that the film was a "heartwarming holiday tale that lovingly illustrates the power of faith and the fact that miracles can and do still happen."   East Valley Living reports that the film had received an award from the Dove Foundation. 
In 2010 TBN Trinity Broadcasting Network picked up the movie for a worldwide TV release.

==References==
 

==External links==
*  
*   at Rotten Tomatoes
*  
*  

 
 
 
 
 
 