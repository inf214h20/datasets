Ustanička ulica
Ustanička ulica is a 2012 Serbian political thriller directed by Miroslav Terzić. The films screenplay was co-written by Ðorde Milosavljević and Nikola Pejaković with input from journalist Filip Švarm.

It premiered on 13 March 2012.

==Plot==
Dušan Ilić (Gordan Kičić), employed at the Serbian state prosecutors office, gets a top secret case to investigate a war crime committed during Yugoslav Wars by a disbanded paramilitary unit. He manages to find Mićun (Uliks Fehmiu) whos the only surviving witness. 

==Cast==
*Gordan Kičić ... Dušan Ilić
*Uliks Fehmiu ... Sredoje Govoruša a.k.a. Mićun Duvnjak
*Rade Šerbedžija ... Vraneš
*Petar Božović ... Grbavi

==Production==
The movies producer is Gordan Kičić through his production company Kombajn Film. He reportedly turned to Ustanička ulica after trying, and ultimately failing, to secure financing for two previous projects — film adaptations of Marko Vidojkovićs Kandže and Biljana Srbljanovićs Beogradska trilogija. 

The filming of Ustanička ulica started on 13 April 2011 and wrapped up on 4 June with 42 shooting days in total on locations in Belgrade, Vojvodina, and Golubac.

==Reaction==
Ustanička ulicas release inspired former film critic and current screenwriter Dimitrije Vojnov into writing a scathing blog entry on the recent state of contemporary Serbian cinema and Serbian movie audiences viewing habits, while also reviewing Ustanička ulica itself. Hes of the opinion that Serbian film theatre audiences have been conditioned into rejecting ambitious, well-crafted cinematic projects, blaming "years of being submerged in the residue of Serbian anti-films that mock any film form or standards" for this "crisis of reception on the part of Serbian audiences". He thus expresses concern that Ustanička ulica, "a movie that doesnt pander or resort to sensationalism, will be ignored by the audiences much like similarly ambitious and well-executed Dejan Zečevićs Neprijatelj was last year". 

==Critical reception==
The film received mixed reviews.

Writing for web magazine Popboks, Đorđe Bajić opines that "despite being promoted as a provocative political film, its hard to imagine Ustanička ulica generating any kind of political discussion", adding: "Yes, there are politics in this movie, but theyve largely been marginalized and pushed to the side. Additionally, the films politics have downright been rendered inconsequential by the screenplay gaffes". He further praises Miroslav Terzićs direction and Fehmius portrayal of Mićun/Sredoje, but has big problems with the "films second-rate screenplay that seriously erodes its overall credibility", saying: "Its hard to believe such an ambitious film even went into production with a script full of glaring dramaturgical oversights and uneven parts, including one unbelievable and unnecessary coincidence, forced parallelism, numerous naivetés, mishandled characters, two-bit dialogues..." 

Along similar lines, Dubravka Lakić of Politika praises the films polished look ("brings American movies and television crime dramas to mind") and atmosphere in addition to Terzićs directorial skills as well as director of photography Miladin Čolakovićs camera work, while having issues with the films wider context and its screenplay. She disapproves of the "way the screenwriters divide Serbs in two groups by pinning the war crimes guilt squarely on Serbs from outside of Serbia, while absolving Serbs from Serbia of everything". Furthermore, she feels the screenplay is the movies weakest link as it "contains script gaffes, dramaturgical oversights, as well as far too many coincidences, parallels, and arbitrary decisions, even occasional naive characterization with several characters completely mishandled". 

Blics Milan Vlajčić concludes his review by calling Ustanička ulica a "refreshing event thats worthy of careful viewing in the pretty precarious contemporary Serbian cinema". Before that, much like other reviewers, he singles out the director Terzić, cinematographer Čolaković, and actor Fehmiu for praise while criticizing the screenplay "whose arbitrary coincidences undermine the storys believability". 

==References==
 

==External links==
* 
* 






 
 
 