Not Fade Away (film)
{{Infobox film
| name           = Not Fade Away
| image          = Not Fade Away poster.jpg
| caption        = Theatrical release poster
| director       = David Chase Mark Johnson
| writer         = David Chase
| starring       = John Magaro Jack Huston Bella Heathcote
| music          = 
| cinematography = Eigil Bryld
| editing        = Sidney Wolinsky
| studio         = 
| distributor    = Paramount Vantage
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $20 million   
| gross          = $610,792 
}}

Not Fade Away is a 2012 drama film and the directorial debut of The Sopranos creator David Chase. It was released on December 21, 2012. 

==Plot==
Set in suburban New Jersey in the 1960s, a group of friends form a rock band and try to make it big.

In his late teens, Douglas Damiano (John Magaro) lives with his father Pat (James Gandolfini), who suffers from mycosis fungoides and is physically rough with Douglas; with his mother Antoinette (Molly Price), who frequently grows hysterical and threatens suicide, and with his sister Evelyn (Meg Guzulescu), the films narrator.  Douglas sees his friend Eugene Gaunt (Jack Huston) singing and playing lead guitar for a band in high school and resolves to join the band to earn the affections of a girl named Grace Dietz (Bella Heathcote).  Douglas gets his chance when the bands current drummer is drafted into Vietnam.  In the wake of the "British Invasion", Gene is trying to remodel his band after The Beatles and The Rolling Stones, and he believes Douglas is suited to this style.

The bands members fluctuate, with an awkward bass player losing his position.  The core of the band is Douglas, Gene, and their friend Wells (Brill).  Gene is initially the lead singer and guitarist, with Wells on rhythm guitar and Douglas on drums and backup vocals.  However, one night at a party Gene accidentally swallows a marijuana|joint, while trying to smoke it through a toilet paper roll and is unable to sing.  Douglas takes over on lead vocals—the bands members later agree that he is superior to Gene.  Gene feels threatened by Douglas and tries to keep the spotlight on himself.  During one concert he juggles firecrackers in protest of the Vietnam War, but fumbles and burns his scalp, humiliating himself and the band.  Douglas goes to Genes house to formally kick him out of the band, and they end up fighting until Genes father restrains him.
 fellate him regularly.  Douglas and Grace have an argument in which she wrongly accuses him of sleeping with her sister Joy (Dominique McElligott).  Joy, an eccentric deeply invested in the counter-culture movement, develops increasing friction with her father (Christopher McDonald), who ultimately has her committed to a mental hospital.  After this, Douglas and a grief-stricken Grace get back together.

Douglas initially tells his father that he intends to join the army and get an ROTC scholarship, but abandons these plans after the Vietnam War begins.  He drops out of college to pursue his musical career full-time, which drives a rift between himself and Pat.  However, when Pat learns that his psoriasis is actually cancer, he starts to mellow somewhat, sharing a dinner with his son and revealing a personal secret that involves Douglas mother.

Meanwhile, two years from the bands inception, Douglas and Wells get the opportunity to audition for Jerry Ragovoy (Brad Garrett).  They recruit Gene back into the band for the audition, though Gene tells them he wants a "featured" credit on their performance of an original song, "The St. Valentines Day Massacre."  Ragovoy sees potential in the band, but outlines a rigorous work schedule, described as "rock and roll boot camp," before hes ready to sign them to a contract. He states that great music is "ten percent inspiration and ninety percent perspiration," a sentiment earlier expressed to Douglas by his father.  The band members are disillusioned with this prospect, Douglas in particular.

While test-driving Genes motorcycle, Wells swerves into a tree, breaking multiple limbs.  This postpones their potential record deal for another year.  Douglas, who is losing interest in the rock and roll lifestyle, decides to go to film school.  He and Grace move to California.  While at a party in Los Angeles, Douglas sees Charlie Watts leaving the house and hears a rumor that Mick Jagger is in the bathroom with several women, though nobody at the party actually sees Jagger.

Douglas tries to hitchhike home and is invited into the car of a strange woman wearing frightening face paint, who comments that he looks lonesome.  Unsettled, Douglas refuses the ride and walks home after briefly windowshopping at a music store. Looking around the downtown scene, he looks to the sky and witnesses again the illuminated clouds that so inspired him before. Smiling, Douglas walks down the street and off camera.  His sister Evelyn enters the scene and addresses the camera directly, commenting that she is writing an essay about Americas two biggest innovations: nuclear weapons and rock n roll.  She speculates as to which one will win in the end, then dances to a song by The Modern Lovers, "Roadrunner" (performed by The Sex Pistols), in the middle of the street.

==Cast==
*John Magaro as Douglas
*Will Brill as Wells
*Jack Huston as Eugene
*Dominique McElligott as Joy Dietz
*Bella Heathcote as Grace Dietz
*Molly Price as Antoinette
*Meg Guzulescu as Evelyn
*Brad Garrett as Jerry Ragovoy
*James Gandolfini as Pat
*Christopher McDonald as Jack Dietz
*Isiah Whitlock, Jr. as Landers
*Louis Mustillo as Uncle Johnny Vitelloni
*Rebecca Luker as Marti Dietz

==Soundtrack listing==

{{Track listing
| headline        = 
| writing_credits = yes
| title1          = There Was a Time
| writer1         = James Brown
| length1         = 3:36 Tell Me
| writer2         = The Rolling Stones
| length2         = 3:49
| title3          = Ride On Baby
| writer3         = The Twylight Zones
| length3         = 2:52 Bo Diddley
| writer4         = Bo Diddley
| length4         = 2:46
| title5          = Bo Diddley
| writer5         = The Twylight Zones
| length5         = 2:48
| title6          = Subterranean Homesick Blues
| writer6         = The Twylight Zones
| length6         = 3:56
| title7          = Parachute Woman
| writer7         = The Rolling Stones
| length7         = 2:20
| title8          = Go Now
| writer8         = The Moody Blues
| length8         = 3:13
| title9          = Time Is On My Side
| writer9         = The Twylight Zones
| length9         = 3:24
| title10          = Dust My Broom
| writer10         = Elmore James
| length10         = 2:57 I Aint Gonna Eat Out My Heart
| writer11         = The Rascals
| length11         = 2:43
| title12          = Good Morning Blues
| writer12         = Lead Belly
| length12         = 2:56 Train Kept A Rollin
| writer13         = Johnny Burnette & The Rock N Roll Trio
| length13         = 2:16
| title14          = Train Kept A Rollin
| writer14         = The Twylight Zones
| length14         = 2:15
| title15          = Pretty Ballerina
| writer15         = The Left Banke
| length15         = 2:38
| title16          = Down So Low Mother Earth
| length16         = 3:52
| title17          = Itchycoo Park
| writer17         = Small Faces
| length17         = 2:49
| title18          = Me and the Devil Blues
| writer18         = Robert Johnson
| length18         = 2:33
| title19          = The St. Valentines Day Massacre
| writer19         = The Twylight Zones
| length19         = 3:49
| title20          = T.B. Sheets
| writer20         = Van Morrison
| length20         = 9:47
| title21          = Some Velvet Morning
| writer21         = Nancy Sinatra and Lee Hazlewood
| length21         = 3:43
| title22          = Bali Hai South Pacific
| length22         = 3:41
| title23          = Road Runner
| writer23         = The Modern Lovers
| length23         = 3:43
| title24          = Pipeline
| writer24         = The Twylight Zones
| length24         = 2:37
| title25          = She Belongs to Me
| writer25         = Bob Dylan
| length25         = 2:49
| title26          = Surgical Supply Jingle
| writer26         = Margaret Dorn
| length26         = 0:40

}}
 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 