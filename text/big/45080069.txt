Nanna Thamma
{{Infobox film 
| name           = Nanna Thamma
| image          =  
| caption        = 
| director       = K. Babu Rao
| producer       = K V Guptha
| writer         = K. Babu Rao (Remake of Jarigina Katha (Telugu) (1969))
| screenplay     = K. Babu Rao Rajkumar Jayanthi Jayanthi Gangadhar Gangadhar R. Nagendra Rao Ghantasala
| cinematography = Madhava Bul Bule
| editing        = R Hanumantha Rao
| studio         = Vijaya Pictures Circuit
| distributor    = Vijaya Pictures Circuit
| released       =  
| runtime        = 167 min
| country        = India Kannada
}}
 1970 Cinema Indian Kannada Kannada film, Gangadhar and R. Nagendra Rao in lead roles. The film had musical score by Ghantasala (singer)|Ghantasala. 

==Cast==
  Rajkumar
*Jayanthi Jayanthi
*Gangadhar Gangadhar
*R. Nagendra Rao Balakrishna
*Dinesh
*Nagappa
*H. R. Shastry
*Hanumanthachar
*Shyam
*Thimmayya
*Narayan
*Master Prakash
*C. Nageshwara Rao
*Vijayabhanu
*Ramadevi
*Papamma
*Baby Brahmaji
*B. Jayamma in Guest Appearance
 

==Soundtrack==
The music was composed by Ghantasala (singer)|Ghantasala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ide Hosa Haadu || PB. Srinivas || Kanagal Prabhkara Sastry || 03.28
|}

==References==
 

==External links==
*  
*  

 
 
 


 