Deface (film)
{{Infobox Film
| name           = DEFACE
| image          = DEFACE poster.jpg
| caption        =  John Arlotto
| producer       = John Arlotto, Roman Wyden
| writer         = John Arlotto
| starring       = Joseph Steven Yang Greg Joung Paik James Kyson Lee Aira H. Kim Alexis Rhee James Mann
| music          = Michael Mouracade
| editing        = John Arlotto
| runtime        = 20 min.
| country        = USA
| language       = Korean
| budget         = 
}}

Deface is unique in that it is the first American-made film in which the entire narrative takes place in North Korea, with a fully Asian American cast. The film is subtitled in English with the actors speaking Korean with a North Korean dialect.

Deface has played in numerous film festivals and has garnered several awards. In 2007 it won Best Narrative Short at the Austin Film Festival, qualifying the film for Oscar consideration in the category of Best Live Action Short Film of 2008.

==Story==

When a North Korean man is pushed to the edge by his daughters senseless death, he risks his life to challenge the oppressive government, making his voice heard through an extreme and unusual action.

==Synopsis==
Deface is the story of Sooyoung, a faceless factory worker in a small town in North Korea who stays quiet and follows the rules.  But when his daughter dies of starvation, Sooyoung is driven by his outrage to vandalize the large propaganda posters that decorate the town with happy, assuring slogans.  Sooyoung risks his life to deface the billboards, repainting the messages to reflect the harsh reality of life in North Korea, and desecrating the images of Kim Il-Sung and Kim Jong-Il.  He manages to avoid capture by the newly formed “Anti-Graffiti” patrol, but when he sees that his act of protest leads to the arrest of local innocents, he must question to what end do his rebellious actions really serve.  Will Sooyoung continue his campaign of revolution or will the government silence another individual voice?  Original, controversial and painstakingly researched, DEFACE sheds light on the Hermit Nation through dramatic storytelling, amazing performances and compelling imagery.

==Cast==
*Joseph Steven Yang as Sooyoung
*Alexis Rhee as Jeung-Un
*Greg Joung Paik as Commander
*James Kyson Lee as Agent Jae Sun
*Aira H. Kim as Kyung-Ha
*Jun Hee Lee as Beggar Boy #1
*Willis Chung as Beggar Boy #2
*Hyun Joo Shin as Foreman
*Arnold Chun as Soldier #1
*Benjamin Kim as Soldier #2
*Joon Park as Soldier #3
*Cy Shim as Soldier #4
*Jeanie J. Lee as Female Soldier
*Roland Young as Mr. Rhee
*Sung Hoon Kim as Agent #2

==Key personnel / crew==

*Writer/Director - John Arlotto Producer - Roman Wyden
*Executive Producer - John Arlotto
*Director of Photography - James Mann Editor - John Arlotto
*Music - Michael Mouracade
*Production Designer - Christian Chi Lee
*Costume Designer - Francine LeCoultre
*Sound Designer - Kaspar Hugentobler
*Makeup/Hair - Yoko Nobushi
*Art Director - Brandon Nicholas
*Stunts - Linn Oeding
*Script Supervisor - Jeff Gonzalez Gaffer - Kevin Duggin

==Critical reception==

Deborah Nicol of Critical Mass Film House says, “DEFACE is John Arlotto’s very powerful short about the effect of a dictatorship regime on it’s working people.  Though Sooyoung (portrayed wonderfully by Joseph Steven Yang) follows in step with everyone else to ensure a meager life for his family, all that changes when they become ill from malnutrition and horrendous conditions.  Surrounded by propaganda posters, he fights the system by defacing the property (e.g. “We have nothing to envy” to “We have everything to envy”).”  Deborah Nicol’s review is available at: http://www.mydesert.com/apps/pbcs.dll/section?category=PluckPersona&U=654fa9a082c54ac7a722a8679cae98bd&plckController=PersonaBlog&plckScript=personaScript&plckElementId=personaDest&plckPersonaPage=BlogViewPost&plckPostId=Blog%3a654fa9a082c54ac7a722a8679cae98bdPost%3a1a48e2c9-c7e0-434c-8b77-90109c764b49&sid=sitelife.mydesert.com

“DEFACE is possibly our favorite film out of the entire bunch and should be a strong contender for an audience award. Sooyoung (played soulfully by Joseph Steven Yang) loses his wife and his daughter to the harsh conditions in North Korea. Every day he is forced to degrade himself when he cannot meet the harsh manual labor quotas set for him by the government, and yet must walk past propaganda billboards that proclaim "We are happy!" His public acts of civil disobedience are punishable by death, and in the end he wonders whether he has really made a difference. At 20 minutes, this is one of the longer shorts, but we didnt want it to end. Writer/Director John Arlotto dares to ask the audience ‘What if we, as artists, risked our lives each time we expressed ourselves creatively?’” -Austinist.com

DEFACE is likely to stand out from the crowd because   takes on the North Korean regime. The story is quick and simple-a bereaved father decides to deface a North Korean propaganda poster-and director John Arlotto locates it effectively in the desolate landscape of totalitarianism. -The Baltimore Weekly

==External links==
* 
* 

 
 
 
 