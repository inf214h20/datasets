A Cage of Nightingales
{{Infobox film
| name           = La Cage aux rossignols
| image          = A Cage of Nightingales.jpg
| image size     =
| caption        =
| director       = Jean Dréville
| producer       =
| writer         = Georges Chaperot  (story)  René Wheeler  (story & screenplay)  Noël-Noël  (adaptation, dialogue & screenplay) 
| narrator       =
| starring       = Noël-Noël Micheline Francey Georges Biscot
| music          = René Cloërec
| cinematography = Paul Cotteret Marcel Weiss
| editing        = Jacques Grassi
| distributor    = Compagnie Parisienne de Location de Films (CPLF)
| released       =  
| runtime        = 89 min
| country        = France
| language       = French
| budget         =
| gross          =5,085,489 admissions (France)   at Box Office Story 
}}
 The Chorus (2004).

==Synopsis==
Clement Mathieu seeks to publish his novel without success. With the help of a friend who is a journalist, his story about the Cage of Nightingales is slipped surreptitiously into a newspaper...

In France, in the 1930s, a supervisor at a rehabilitation house awakens difficult teens inner musical tendencies by forming a choir, despite the directors skepticism. Later, this experience is reported in a novel in a major newspaper.

The history of the Cage of Nightingales is directly inspired by that of an actual educational centre, called Ker Goat, where Jacques Dietz, Roger Riffier and their teams worked to help children in difficulty through choral singing and innovative teaching methods.

==Cast==

*Noël-Noël as Clément Mathieu
*Micheline Francey as Micheline
*Georges Biscot as Raymond
*René Génin as Le père Maxence
*René Blancard as Monsieur Rachin
*Marguerite Ducouret as La mère de Micheline
*Marcelle Praince as La présidente
*Marthe Mellot as Marie
*Georges Paulais as Monsieur Langlois
*André Nicolle as Monsieur de la Frade
*Richard Francoeur as Monsieur de Mézères Jean Morel as Le directeur
*Roger Vincent as Lacadémicien
*Michel François as Lequerec
*Roger Krebs as Laugier
==Reception==
The film was the second most popular movie at the French box office in 1945. 
==References==
 
==External links==
* 

 
 
 
 
 
 

 