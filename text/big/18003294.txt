Rakthasakshikal Sindabad
{{Infobox film
| name           = Rakthasaakshikal Zindabad (1998)
| image          = Rakthasakshikal Zindabad.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Rakthasaakshikal Zindabad (DVD cover)
| director       = Venu Nagavalli
| producer       = Mukesh, R.Venu for Bhavadhara
| writer         = 
| screenplay     = Venu Nagavalli Cheriyan Kalpakavadi
| story          = Cheriyan Kalpakavadi
| based on       =  
| narrator       =  Murali Nedumudi Sukanya 
| music          = MG Radhakrishnan
| cinematography = P Sukumar
| editing        = N Gopalakrishnan
| studio         = Surya Cine Arts
| distributor    = Surya Cine Arts Seven Arts
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 thriller movie released in 1998 directed by Venu Nagavalli and featuring Mohanlal, Suresh Gopi and Nassar. Revolt scenes in the film were directed by Priyadarshan.

== Plot == Communism came into picture.

== Cast ==
* Mohanlal - Siva Subrahmanya Iyer
* Suresh Gopi - Urmees Tharakan Mappilassery
* Nassar - Divan Sir C. P. Ramaswami Iyer Murali - E. Sreedharan
* Nedumudi Venu - Annan
* Karamana Janardanan Nair - Raman Thirumulpadu
* Rajan P Dev - Mappilassery Tharakan
* Mala Aravindan Zainuddin
* Jagannatha Varma Sukanya - Sivakami Ammal
* Ranjitha Maathu
* Sukumari - Subbulaxmi Amma
* Kalamandalam Sreejaya - Ammini

== Soundtrack ==
The films soundtrack contains 9 songs, all composed by M. G. Radhakrishnan. Lyrics by P. Bhaskaran, O. N. V. Kurup, Gireesh Puthenchery, Ezhacheri Ramachandran.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Balikudeerangal"
| K. J. Yesudas
|-
| 2
| "Cheruvallikkaavilinnu  "
| Sudeep Kumar
|-
| 3
| "Kizhakku Pulari"
| K. J. Yesudas, M. G. Sreekumar, Chorus
|-
| 4
| "Nammalu Koyyum"
| M. G. Sreekumar, K. S. Chitra, Chorus
|-
| 5
| "Panineer Maariyil  "
| Radhika Thilak, Sudeep Kumar
|-
| 6
| "Ponnaaryan Paadam"
| K. S. Chitra
|-
| 7
| "Vaikaasihennalo"
| M. G. Sreekumar, K. S. Chitra
|-
| 8
| "Vaikaasithennalo (F)"
| K. S. Chitra
|-
| 9
| "Vaikaasithennalo (M)"
| M. G. Sreekumar
|}

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 