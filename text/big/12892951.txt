What He Forgot
{{Infobox film
| name           = What He Forgot
| image          =
| image size     =
| caption        =
| director       = Jerold T. Hevener
| producer       = Arthur Hotaling Siegmund Lubin
| writer         = E.W. Sargent	
| narrator       =
| starring       = Jerold T. Hevener
| music          =
| cinematography =
| editing        =
| distributor    = General Film Company
| released       = January 2, 1915
| runtime        = split reel United States English intertitles
| budget         =
| preceded by    =
| followed by    =
}}

What He Forgot is a 1915 American comedy film featuring Oliver Hardy.

==Cast==
* Jerold T. Hevener - Bill Bailey
* Mae Hotely - Bella Bailey
* Eloise Willard - Her Mother
* George T. Welsh - Her Father
* Royal Byron - Jack
* Nellie Farron - An Actress
* Oliver Hardy - (as Babe Hardy)

==See also==
* List of American films of 1915
* Filmography of Oliver Hardy

==External links==
* 

 
 
 
 
 
 
 

 