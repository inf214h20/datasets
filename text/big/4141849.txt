The Horror of Frankenstein
 
 
{{Infobox film
| name = The Horror of Frankenstein
| image	= The Horror of Frankenstein FilmPoster.jpeg
| caption =
| director = Jimmy Sangster
| producer = Jimmy Sangster
| writer = Jimmy Sangster Jeremy Burnham (screenplay) Mary Shelley (characters)
| starring = Ralph Bates Kate OMara Veronica Carlson David Prowse
| music = Malcolm Williamson
| cinematography = Moray Grant
| editing = Chris Barnes EMI Elstree Hammer Film Productions Metro-Goldwyn-Mayer
| distributor = EMI Films#MGM-EMI|MGM-EMI
| released =  
| runtime = 95 min.
| country = United Kingdom
| language = English
| budget = ₤200,000 Marcus Hearn & Alan Barnes, The Hammer Story: The Authorised History of Hammer Films, Titan Books, 2007 p 138 
}}

The Horror of Frankenstein is a 1970 British horror film by Hammer Film Productions that is both a semi-parody and remake of the 1957 film The Curse of Frankenstein. It was produced and directed by Jimmy Sangster, starring Ralph Bates, Kate OMara, Veronica Carlson and David Prowse as the monster. The original music score was composed by Malcolm Williamson.

==Plot==

Victor Frankenstein, a cold, arrogant and womanizing genius, is angry when his father forbids him to continue his anatomy experiments. He ruthlessly murders his father by sabotaging the old mans shotgun, consequently inheriting the title of Baron von Frankenstein and the family fortune. He uses the money to enter medical school in Vienna, but is forced to return home when he impregnates the daughter of the Dean (education)|Dean.

Returning to his own castle, he sets up a laboratory and starts a series of experiments involving the revival of the dead. He eventually builds a composite body from human parts, which he then brings to life.  The creature goes on a homicidal rampage until it is accidentally destroyed when a vat where it has been hidden is flooded with acid.

==Cast== Baron Victor Frankenstein
* Kate OMara as Alys
* Veronica Carlson as Elizabeth Heiss
* Dennis Price as The Graverobber
* Jon Finch as Lieutenant Henry Becker
* Bernard Archard as Professor Heiss
* Graham James as Wilhelm Kassner  James Hayter as Bailiff
* Joan Rice as Graverobbers wife
* Stephen Turner as Stephan 
* Neil Wilson as Schoolmaster
* James Cossins as Dean
* Glenys OBrien as Maggie
* Geoffrey Lumsden as Instructor
* Chris Lethbridge-Baker as Priest 
* Terry Duggan as First Bandit
* George Belbin as Baron Frankenstein 
* Hal Jeayes as Woodsman 
* Carol Jeayes as Woodsmans Daughter 
* Michael Goldie as Workman  The Monster

==Production==
The film was entirely financed by EMI. 

==Credits==
* Produced and directed by Jimmy Sangster
* Screenplay by Jeremy Burnham and Jimmy Sangster, based on the characters created by Mary Shelley
* Production manager: Tom Sachs
* Music by Malcolm Williamson
* Photography by Moray Grant
* Art direction: Scott MacGregor
* Edited by Chris Barnes Tom Smith

===Cast notes===

Ralph Bates was cast as Victor Frankenstein, the role having, five times previously, been played by Peter Cushing. Soon afterwards, he did a take on Dr. Jekyll in the Hammer film Dr. Jekyll and Sister Hyde (1971), which co-starred Martine Beswick.

In the mid-1960s, David Prowse, later famous for his portrayal of   (1974), where his overall appearance was much more horrifically elaborate.

==See also==
* Frankenstein in popular culture|Frankenstein in popular culture
* List of films featuring Frankensteins monster

==References==
 

==External links==
*  
*  
*  at Trailers from Hell

 
 
 

 
 
 
 
 
 
 
 
 
 