Atlantic Convoy
{{Infobox film
| name           = Atlantic Convoy
| image          =
| caption        =
| director       = Lew Landers
| producer       = Colbert Clark
| writer         = Robert Lee Johnson  
| starring       =
| music          = Morris Stoloff   
| cinematography = Henry Freulich James Sweeney
| distributor    = Columbia Pictures
| released       =  
| runtime        = 66 min.
| country        = United States
| language       = English
}}

Atlantic Convoy is a 1942 American film.  The story follows naval patrols based on the Icelandic coast battling the German U-boats of World War II, and the German efforts to infiltrate their operations with spies and saboteurs.

== Cast ==
* Bruce Bennett as Captain Morgan
* Virginia Field as Lida Adams John Beal as Carl Hansen
* Clifford Severn as Sandy Brown
* Larry Parks as Gregory
* Lloyd Bridges as Bert
* Victor Kilian as Otto
* Hans Schumm as Commander von Smith
* Erik Rolf as Gunther
* Eddie Laughton as Radio Operator
* Wilhelm von Brincken as U-Boat Officer

== External links ==
*  

 

 
 
 
 
 


 