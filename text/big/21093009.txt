Exorcism: The Possession of Gail Bowers
{{Infobox film name           =Exorcism: The Possession of Gail Bowers image          =Exorcism gail bowers dvd cover.jpg image_size     = caption        =DVD cover director       =Leigh Scott producer       =David Michael Latt writer         =Leigh Scott narrator       = starring       =Erica Roby Griff Furst Thomas Downey Noel Thurman music          = Eliza Swenson cinematography = editing        =Leigh Scott Kristen Quintrall distributor    =The Asylum released       =January 31, 2006 runtime        =91 minutes country        =United States language  English
|budget         = $20,000 gross          = preceded_by    = followed_by    =
}}
Exorcism: The Possession of Gail Bowers is a 2006 direct-to-DVD horror film by The Asylum, written and directed by Leigh Scott.

==Plot== exorcise Gail possessed by Blackthorn Industries, tells of the problems that the neighborhood faces as the result of Gails possessions, and that medical science has failed to make amends.

Using what powers are available, Father Bates visits Gail in her home and begins to perform an exorcism. It is during this service that the malevolent forces possessing Gail begin to fight back against the priest, and force themselves to be revealed for the first time.

==See also==
* List of American films of 2006
*The Exorcism of Emily Rose - A similar film released in 2005 Dead Men Walking - Another horror film by The Asylum film studio, which was referenced in this film
*Repossessed - A 1990 horror-comedy with a similar storyline

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 