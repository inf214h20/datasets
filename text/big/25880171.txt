King Kennedy
{{multiple issues|
 
 
}}

{{Infobox film
| name     = King Kennedy
| director = Ronan ORahilly
| writer   =  Ronan ORahilly 
| producer =  
| starring =  
| released =
| country  = United Kingdom
| language = English
}} conspiracy and murder, and features some of the most memorable moments in 1960s America, including  Marilyn Monroes world famous "Happy Birthday, Mr. President" at Madison Square Garden and Martin Luther Kings "I Have A Dream" speech on the steps of the Lincoln Memorial in which he called for racial equality and an end to discrimination. The film is designed primarily to remind, focusing on the characters and events that build up to the assassinations of John F. Kennedy, Robert F. Kennedy and Martin Luther King as their apparent determination to shy away from war, discrimination and hatred became ever more publicized.

The film is classified as being "In Production" on Amazons Internet Movie Database. Further information including details on the release of the film can be found on the official site of the filmmakers.

== Cast ==
* John F. Kennedy as himself
* Robert F. Kennedy as himself
* Jacqueline Bouvier Kennedy as herself
* J. Edgar Hoover as himself
* Martin Luther King as himself
* Lee Harvey Oswald as himself
* Sirhan Sirhan as himself
* Marilyn Monroe as herself
* Frank Sinatra as himself
* Fidel Castro as himself
* Jimmy Hoffa as himself
* Howard Hunt as himself
* Sam Giancana as himself
* Bull Conner as himself
* Richard Helms as himself
* Nikita Khrushchev as himself
* James Earl Ray as himself
* Malcolm X as himself

== External links ==
*  
*  
*   by Screen Daily

 
 
 
 
 
 
 