Nightflyers
{{Infobox Film
| name           = Nightflyers
| image          = Nightflyersposter.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Robert Collector
| producer       = Robert Jaffe	
| writer         = George R. R. Martin Robert Jaffe
| narrator       =  James Avery
| music          = Doug Timm Shelly Johnson
| editing        = Tom Siiter
| distributor    = The Vista Organization 1987
| runtime        = 89 min
| country        = United States English
| budget         = 
| gross          =  $1,149,470 
| preceded_by    = 
| followed_by    = 
}}
 1987 science science fiction-horror horror film based on that novella.

==Novella==
The 1980 novella was the recipient of the 1983 Seiun Award for Best Foreign Language Short Story of the Year.

==Collection== collection is the fifth by Martin and was first published in December 1985. It contains the following stories:
*"Nightflyers"
*"Override"
*"Weekend in a War Zone"
*"And Seven Times Never Kill Man"
*"Nor the Many-Colored Fires of a Star Ring" A Song for Lya"

==Film== James Avery, and John Standing.

The film was released theatrically on October 23, 1987 and mainly received negative reviews upon release , although the movie has started to gain a small cult following  upon its 1988 home video release by International Video Entertainment.  In a classic blunder of advertising , it was marketed as a Halloween horror film, which it was not .

== References ==
 

==External links==
*  

 

 
 
 
 
 
 


 
 

 