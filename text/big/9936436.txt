Gordon of Ghost City
{{Infobox film
| name           = Gordon of Ghost City
| image_size     = 
| image	=	Gordon of Ghost City FilmPoster.jpeg
| caption        =  Ray Taylor
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       = 1933
| runtime        = 12 chapters (220 min)
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial movie serial based on the novel Oh, Promise Me! by Peter B. Kyne.

==Cast==
* Buck Jones - Buck Gordon
* Madge Bellamy - Mary Gray Walter Miller - Rance Radigan
* Tom Ricketts - Amos Gray William Desmond - John Mulford Francis Ford - The Mystery Man
* Edmund Cobb - Cowhand Scotty Craig Reynolds - Henchman Ed (as Hugh Enfield)
* Bud Osborne - Henchman Hank (as Bud Osbourne)
* Ethan Laidlaw - Henchman Pete

==Chapter titles==
# A Lone Hand
# The Stampede
# Trapped
# The Man of Mystery
# Riding for Life
# Blazing Prairies
# Entombed in the Tunnel
# The Thundering Herd
# Flames of Fury
# Swimming in the Torrent
# A Wild Ride
# Mystery of Ghost City
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 208
 | chapter = Filmography
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 

 
{{succession box  Universal Serial Serial 
| before=The Phantom of the Air (1933)
| years=Gordon of Ghost City (1933) The Perils of Pauline (1933)}}
 

 
 

 
 
 
 
 
 
 
 
 


 