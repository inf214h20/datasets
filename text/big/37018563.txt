Kya Dilli Kya Lahore
 
 
{{Infobox film
| image         = Kya_dilli_kya_lahore_the_movie.jpg
| director       = Vijay Raaz
| producer       = Karan Arora
| story          = Aseem Arora
| screenplay     = Aseem Arora Pratham S. Jolly Manu Rishi
| dialogue       = Manu Rishi 
| starring       = Vijay Raaz Manu Rishi Raj Zutshi
| music          = Sandesh Shandilya
| cinematography = Raaj Chakravarthy
| editing        = Archit D. Rastogi
| studio         = Picture Thoughts Production 
| distributor    = Wave Cinemas
| released       =  
| runtime        = 100 minutes
| country        = India
| language       = Hindi/Urdu
| budget         = 
| gross          = 
}}
 war drama film set in 1948, post-Independence period and deals with the subject of partition of India|Indo-Pak partition. The movie stars Vijay Raaz, Manu Rishi, Raj Zutshi and Vishwajeet Pradhan in the lead roles, and credits Gulzar as the presenter of the film. 

The film is produced by Karan Arora and also marks the directorial debut of actor Vijay Raaz. The first look of the movie was released at Wagah Border. The film was released worldwide on 2 May 2014 to extremely positive reviews.  

==Plot==
In 1948, a cross-fire erupts at an isolated stretch of Indo-Pak border, leaving only two soldiers alive. One is an Indian soldier of Pakistani origin while the other happens to be a Pakistani soldier of Indian origin. An ironic story of pride and survival begins when - in an attempt to evade danger, they bump into each other. And amidst continuous exchange of bullets, altercations and murkier situations, it evolves into a journey of human connection with an unforeseeable end..

==Cast==
* Vijay Raaz - Rehmat Ali
* Manu Rishi- Samarth Pratap Shastri
* Raj Zutshi - Barfi Singh
* Vishwajeet Pradhan - Pakistani Captain 

==First Look==
 
First Look of the movie was unveiled at Wagah Border (Punjab) with a symbolic colourful flag lowering ceremony. The star cast held a candle light vigil on 14 August 2012.   And, giant posters of the movie were set up where hundreds flocked to watch the ceremony. The first look has been very impressive and Gulzars shayari from the movie has already touched peoples hearts. 

About the movie, Raaz says that "It is a very emotional film" for him and that "It is a very innocent story"  
"The film comes with the message that humanity is bigger than any religion or any country and thats what we have tried to convey through the film."

About involvement in the movie, Gulzar sahib says that "This subject (partition) is very close" to his heart and he has been "writing about it a lot." He further adds, "After seeing the film, I was surprised to know that many youngsters have been associated with the film and yet they have brought the essence of the whole film so well. Karan had brought the script to me and after they completed the film, he brought it to show it to me. The film was bang on. The film talks about peace yet is not preachy and there is no blood shed, it is a human drama and very relevant."  

Producer Karan Arora added that this "is not like a typical Bollywood extravaganza, it is a simple film." He is " not worried about the commercial viability" of the film and is content to know that he made "an honest film"  

==Production==
Vijay Raaz says that he "was thinking about direction when the story came to me. We have grown up on Partition stories so I was naturally attracted to the film. It is a simple story but at the same time it is very difficult," 

The films writers Aseem Arora, Pratham Jolly and Manu Rishi won Best Screenplay award at the prestigious 5th Dada Sahab Phalke Film Festival.

It got rave reviews and appreciation from critics. Mumbai Mirror, NDTV and Times of India gave the film three stars while Divya Sologoma gave it four stars praising the contents purity and mentor Gulzars poetry. 

==See Also==
* No Mans Land (2001 film)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 