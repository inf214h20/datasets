Shipyard Sally
Shipyard British musical musical comedy pub near the John Brown & Company shipyard at Clydebank. When the closure of the yard threatens to put many out of work she leads a campaign to persuade the government to reconsider the decision. Made shortly before the outbreak of the Second World War, it was Fields last British film.  The film is notable for the song Wish Me Luck as You Wave Me Goodbye which became a major hit.

==Cast==
* Gracie Fields - Sally Fitzgerald
* Sydney Howard - Major Fitzgerald
* Morton Selten - Lord Alfred Randall
* Norma Varden - Lady Patricia Randall
* Oliver Wakefield - Forsyth
* Tucker McGuire - Linda Marsh
* MacDonald Parke - Diggs Richard Cooper - Sir John Treacher
* Joan Cowick - Uncredited

==Bibliography==
* Shafer, Stephen C. British Popular Films 1929-1939:The Cinema of Reassurance. Rutledge, 1997.

==References==
 

 

 
 
 
 
 
 


 
 