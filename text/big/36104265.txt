Baron Blood (film)
 
{{Infobox film
| name           = Baron Blood
| image          = Baron Blood (film) poster.jpg
| alt            = 
| director       = Mario Bava
| producer       = Samuel Z. Arkoff Alfredo Leone
| writer         = Vincent Fotre
| starring       = Joseph Cotten Elke Sommer Massimo Girotti 
| music          = Stelvio Cipriani Les Baxter  (US version) 
| editing        = Carlo Reali
| studio         = Euro America Produzioni Cinematografiche Dieter Geissler Filmproduktion
| released       = 25 February 1972
| runtime        = 98 min.  (Italy)  90 min.  (USA) 
| country        = West Germany Italy
| language       = Italian ITL €262,741,000
}}
 horror film directed by Mario Bava. It is one of Bavas least critically popular films.

==Plot==
 
American Peter Kleist travels to visit the castle of his Austrian ancestor Baron Otto Von Kleist who had a reputation that earned him the nickname "Baron Blood" and who was cursed by a witch, Elisabeth Holle, for his evil deeds against the villagers before he burned her at the stake. 

Peter is shown a parchment with a spell reputed to have the power to bring Baron Blood back to life. As a lark with Eva, a female architect renovating the castle for a hotel project, he reads the invocation out loud in the castle. Frightened by an unseen presence, they read the spell to send him back. They later read the invocation again, only this time the parchment is burned before they can read the dismissal. The revived corpse-like Baron goes into town and murders a doctor, starting a reign of terror against the villagers. With each murder victim he becomes more human yet can revert to his hideous appearance. 

Gaining access to his hidden treasure, the Baron appears under the disguise of wheelchair-bound Alfred Becker and buys the castle. The murder spree has hurt the hotel project. Eva continues to work for Becker on renovating the castle as his home. After Eva is chased through the fog bound streets of the village by the Baron, Eva and Peter turn to a clairvoyant who puts them in touch with the spirit of Elisabeth Holle for clues on how to combat him. The little girl Gretchen who has seen both the "ghost" (the amubulatory Baron in corpse mode) and Becker (the Baron in human disguise) tell Eva and Peter that the eyes of the ghost and Becker are the same.

== Cast ==
* Joseph Cotten: Baron Otto von Kleist/Alfred Becker
* Elke Sommer: Eva Arnold
* Massimo Girotti: Dr. Karl Hummel
* Rada Rassimov: Christina Hoffmann
* Antonio Cantafora: Peter Kleist
* Umberto Raho: Inspector (as Humi Raho)
* Luciano Pigozzi: Fritz
* Nicoletta Elmi: Gretchen Hummel

==Production==
  Austrian castle named Burg Kreuzenstein. 

==Release==
 
The film was bought of release in the US by AIP who cut the film by ten minutes and replaced Stelvio Ciprianis score with one by Les Baxter. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 19 

== Critical reception ==
Baron Blood has been poorly received by critics. 

Roger Ebert gave the film two stars out of four, writing, "sometimes you can enjoy horror movies because theyre so bad, but Baron Blood isnt bad enough."  On the other hand, film critic Leonard Maltin gave the movie 2.5 stars, briefly noting "standard plot is livened by unusual settings and lighting".    Daryl Loomis from DVD Verdict gave the film a mostly positive review stating, "Baron Blood is not Mario Bavas best film, but its far from his worst. Its bloody and full of torture, if not so full of suspense, but its still a lot of fun." 
Dread Central awarded the film a score of 3 / 5 commenting, "Baron Blood   a particularly uneven piece of work; yet, Bava’s eye is consistently impressive, creating swathes of Gothic imagery such as a chase sequence through fog-laden streets, sterling use of shadow in framing his antagonist, and a great location in the form of the Baron’s castle. To be expected is also the director’s excellent use of lighting and primary colours, making this another rich visual experience with that distinctly European feel. While it certainly isn’t anywhere near the upper echelons of Bava’s filmography, it offers enough in the way of style and the gleefully macabre to keep it afloat". 
A.H. Weiler from New York Times gave the film a negative review calling the title villain "bland", and stated, "Under Mario Bavas pedestrian direction, the concocted creaking, screaming, gory murders and Miss Sommers frightened racing through dark passageways largely add up to spectral schlock". 
The film currently has a 0% approval rating on movie review aggregator website Rotten Tomatoes based on six reviews. 
== In popular culture == tabloids in the late 1990s. The hoax was an audio recording of the sounds of Hell, recorded by seismologists in Siberia. The sounds in the nine-mile deep pit included yells and haunting screams for help from sinners supposedly sent to Hell. 

The recording, however, was later revealed to have been a cleverly remixed portion of the soundtrack of the movie Baron Blood, with various effects added. 

==Biography== 
* 

== References ==

 

== External links ==

*  

 

 
 
 
 