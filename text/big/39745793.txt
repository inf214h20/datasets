Miss Leelavathi
{{Infobox film name           = Miss Leelavathi image          =  image_size     =  caption        =  director       = M. R. Vittal producer       = K. S. Jagannath writer         = Korati Srinivasa Rao narrator       =  starring  Jayanthi K. S. Ashwath Udaykumar music          = R. Sudarsanam cinematography = S. V. Srikanth   Kumar editing        = S. P. N. Krishna   T. P. Velayudham distributor    = Sri Rajarajeshwari released       = 1965 runtime        = 162 mins country        = India language       = Kannada budget         =  preceded_by    =  followed_by    = 
}}
 Jayanthi as National Award for second best film in Kannada. 

The film was talked about for its bold portrayal of women and was accredited of being the first Kannada feature film showing the leading lady wearing a swim suit.  The film was a musical hit with R. Sudarsanam as the composer.

== Plot ==
This socially relevant movie explores the darkness of female sexuality on the screen. The lead protagonist Leelavathi (Jayanthi (actress)|Jayanthi) is shown as a bold girl to a doting father (K. S. Ashwath) who is completely outright in wearing revealing attires and expressing her views about the pre-marital sexual acts. She also comes under many experimental circumstances in her life which makes the rest of the world highly critical about her. She is shown to finally succumb to the moral trappings of the time and her father is also ridiculed for being bringing her up more liberally.

==Cast== Jayanthi as Leelavathi
* Udaykumar
* K. S. Ashwath  
* Vanisree
* Jayasree
* Papamma
* Ramesh Narasimharaju

==Soundtrack ==
{| class="wikitable sortable"
|-
! Title !! Singers !! Lyrics 
|- Nodu Baa Nodu Baa || S. Janaki ||  Vijaya Narasimha
|- Doni Saagali || S. Janaki, Ramachandra || Kuvempu
|- Hiriya Naagara || P. Jayadev ||  Vijaya Narasimha
|- Bayake Balli || P. B. Srinivas, S. Janaki ||  Vijaya Narasimha
|- Neerinalli Neenu || P. B. Sreenivas, S. Janaki || Vijaya Narasimha
|}

==Awards== Second Best Film in Kannada
; This film screened at IFFI 1992 Kannada cinema Retrospect.

==References==
 

== External links ==
*  

 

 
 
 