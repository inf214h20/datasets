The Officers' Ward (film)
 The Officers Ward}}
 
{{Infobox film
| name = The Officers Ward
| image =
| image_size =
| caption =
| director = François Dupeyron
| producer = Laurent Pétin Michèle Pétin
| writer = François Dupeyron Marc Dugain
| narrator =
| starring = Eric Caravaca
| music =
| cinematography = Tetsuo Nagata
| editing = Dominique Faysse
| distributor =
| released = 18 May 2001
| runtime = 135 minutes
| country = France
| language = French
| budget =
| preceded_by =
| followed_by =
}} 2001 cinema French film, novel by Marc Dugain. It is supposedly based on the experiences of one of the authors own ancestors during World War I.

==Synopsis==
The film concentrates more on the period spent in hospital than the original novel, and emphasizes the horror of the friends injuries. On Adriens arrival at the ward, all the mirrors are removed and staff are instructed not to give any to him, but we see from the expressions on the faces of others just how bad the damage is. Adrien becomes increasingly desperate to see the damage done to his face, even asking a visitor to draw a picture of him. Dupeyron ensures that we do not see the horrifying extent of Adriens injuries until the moment that he himself does - by looking at his reflection in a window pane.

There is a strong focus on the fleeting romance between Adrien and Clémence, a woman he meets by chance shortly before departing for the war, and his later attempts to track her down. When he finally does meet her again, she fails to recognise him.

Whereas the novel follows the experiences of the group right up to World War II and beyond, the film ends just after the First World War, the final scene being Adriens chance meeting with his future wife.

==Cast==
* Eric Caravaca - Adrien
* Denis Podalydès - Henri
* Grégori Derangère - Pierre
* Sabine Azéma - Anaïs
* André Dussollier - The surgeon
* Isabelle Renauld - Marguerite
* Géraldine Pailhas - Clémence
* Jean-Michel Portal - Alain
* Guy Tréjan - The minister
* Xavier De Guillebon - Louis
* Catherine Arditi - Adriens mother
* Paul Le Person - Adriens grandfather
* Circé Lethem - Adriens sister
* Elise Tielrooy - Nurse Cécile
* Agathe Dronne - Adriens future wife

==Awards and nominations== Cannes Film Festival (France)
**Nominated: Golden Palm (François Dupeyron)   
*César Awards (France)
**Won: Best Cinematography (Tetsuo Nagata)
**Won: Best Actor &ndash; Supporting Role (André Dussollier)
**Nominated: Best Actor &ndash; Leading Role (Eric Caravaca)
**Nominated: Best Costume Design (Catherine Bouchard)
**Nominated: Best Director (François Dupeyron)
**Nominated: Best Film
**Nominated: Best Writing (François Dupeyron)
**Nominated: Most Promising Actor (Grégori Derangère and Jean-Michel Portal)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 