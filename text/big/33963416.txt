Behind the Headlines (1956 film)
{{Infobox film
| name           = Behind the Headlines
| image          = 
| image_size     = 
| caption        =  Charles Saunders
| producer       = Guido Coen
| writer         = Robert Chapman (novel)   Allan MacKinnon 
| narrator       =  Paul Carpenter Adrienne Corri Hazel Court   Alfie Bass
| music          = Stanley Black
| cinematography = Geoffrey Faithfull  
| editing        = Margery Saunders
| studio         = Kenilworth Film Productions Rank Organisation
| released       = July 1956
| runtime        = 65 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British crime Charles Saunders Paul Carpenter, Adrienne Corri, Hazel Court and Alfie Bass.  A male and female journalist join forces to hunt down a murderer. It was based on the novel Behind the Headlines by Robert Chapman.

==Plot==
Paul Banner used to be an US reporter working in London. Recently he has gone freelance, leaving his paper so that he can focus more on chasing down facts and selling his stories once he gets them, no more deadlines or misguided editors to divert his attention. When showgirl Nina Duke is murdered the press are all harrying the police for statements and facts but Banner hangs back and does a little work of his own to uncover the story. Nina, it transpires, was in jail for blackmail previously so it is possible that this was why she was killed, however can Banner get the story that the police cannot? 

==Cast== Paul Carpenter - Paul Banner 
* Adrienne Corri - Pam Barnes 
* Hazel Court - Maxine 
* Alfie Bass - Sammy 
* Ewen Solon - Superintendent Faro 
* Trevor Reid - Bunting 
* Melissa Stribling - Mary Carrick 
* Olive Gregg - Mrs. Bunting 
* Harry Fowler - Alfie 
* Magda Miller - Nina Duke  Arthur Rigby - Hollings  Leonard Williams - Jock Macrae 
* Gaylord Cavallaro - Jeff Holly  Tom Gill - Creloch 
* Colin Rix - Bernard 
* Anita Wuest - Model 
* Sandra Colville - Waitress 
* Marian Collins - Nurse 
* Constance Wake - Receptionist

==References==
 

==Bibliography==
* Chibnall, Steve & McFarlane, Brian. The British B Film. Palgrave MacMillan, 2009.

==External links==
* 

 

 
 
 
 
 
 
 