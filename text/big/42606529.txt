Moomins on the Riviera
{{Infobox film name            = Moomins on the Riviera image           = Moomins on the Riviera poster.jpeg caption         = The international poster director        = Xavier Picard producer        = Hanna Hemilä writer  Leslie Stewart Annina Enckell Hanna Hemilä Xavier Picard Beata Harju based on        =   starring        = {{Plainlist|
*Russell Tovey
*Nathaniel Parker
*Tracy Ann Oberman
*Stephanie Winiecki
*Ruth Gibson
*Philippe Smolikowski
*Dave Browne
*Shelley Blond}}  music           =  cinematography  =  editing         = studio          = Handle Productions Pictak Cie distributor     = Nordisk Film   Gebeka Films   Vertigo Films   released        =   runtime         = 80 minutes country         = France Finland language        = English budget          = €3.6 million    (US$4 million) gross           = $1,4 million   
}} French  traditional animated Lars Jansson. Leslie Stewart, Annina Enckell, Hanna Hemilä, Xavier Picard and Beata Harju. The British English version of the film stars Russell Tovey as Moomintroll|Moomin, Nathaniel Parker as Moominpappa and Tracy Ann Oberman as Moominmamma.

The film is based on the Moomin Tove Janssons original comic strip: Moomin on the Riviera and it is the first animated feature based on the comic strips. In the film, the Moomins along with Snorkmaiden and Little My sail for the French Riviera|Riviera, where their unity is threatened. Snorkmaiden is dazzled by the attentions of the playboy Clark Tresco and Moominpappa befriends an aristocrat Marquis Mongaga, while Moomin and Moominmamma decide to move to the beach in order to escape all the glamorous extravagance.   

Moomins on the Riviera has been first released theatrically on 10 October 2014 in Finland to celebrate the 100th anniversary of Tove Janssons birth.  In United Kingdom, the film made its premiere on 11 October 2014 at BFI London Film Festival    and it is set to have wider theatrical release in 22 May 2015.   

==Plot==
 

==English Cast== Moomin
* Nathaniel Parker as Moominpappa
* Tracy Ann Oberman as Moominmamma
* Stephanie Winiecki as Snorkmaiden
* Ruth Gibson as Little My
* Philippe Smolikowski as Marquis Mongaga
* Dave Browne as Clark Tresco
* Shelley Blond as Audrey Glamour

Additional roles from the English version of the film are played by Dominic Frisby, Ian Conningham, Alison O’Donnell, Bernard Alane, Bruno Magnes, Andy Turvey, Kris Gummerus, Glyn Welden Banks, Lee Willis, Sanna-June Hyde, Christopher Sloan and Leslie Hyde. Maria Sid and Beata Harju, who provide voices for Moominmamma and the Mymble in the Finnish and Swedish versions, also voice minor roles in the English dub. 

==Production== Telescreen had Moomin comic strip story.  The producer Hanna Hemilä who is Sophias close friend, tells Sophia about French animation director Xavier Picard, who could be interested to turn comic strips to the big screen. They contact Picard     and the film started production at France in 2010.  
 hand drawn animation with a reduced color scale for the backgrounds in order to maintain continuity with the black-and-white comic strips.  Pictak Cie has been on developing the visuals of the film, Chinese Sandman Animation Studio has done 120,000 drawings for the hand-made animation and Handle Productions is producing the film.    The film marks the feature film directorial debut of Xavier Picard.  Picard wasnt aware of the Moomins before he discovered them 20 years ago in Japan,    but he has been particularly fascinated by comic strips and has stated to want translate the art of Tove Jansson into animation. 
 Finnish voice English voice cast has been confirmed on September 2014 press announcement.   The films score is confirmed to being made by many Finnish and French composers, including Jean de Aguiar, Panu Aaltio, Timo Lassy, Milla Viljamaa and Anna-Karin Korhonen. 
 Riviera is fun and dramatic setting, they have also add the side story at the beginning of the film, that takes place to the Moominvalley and where Moomins got their idea for the sea voyage. The reason for this has been to introduce the dwelling place of the Moomins for the international audience. Sophia Jansson herself has approved the change. 

==Release==
Moomins on the Riviera has been first released on 10 October 2014 in Finland to celebrate the 100th anniversary of Tove Jansson’s birth,      and on 31 October 2014 in Sweden.  In France, the film has been released on 4 February 2015.  Indie Sales acquired the release rights for the Toronto Film Festival    and in addition, the film has been sold to other countries, including Switzerland and Japan.  For the English theatrical release, the film has made its premiere on 11 October 2014 at BFI London Film Festival in United Kingdom   The film is set to have wider theatrical release in UK for 22 May 2015 that is distributed by Vertigo Films.   In February 2015, the film is confirmed to being featured on New York International Childrens Film Festival. 

Region 2 home media edition of Moomins on the Riviera is released on February 11, 2015 for the DVD and Blu-Ray Disc|Blu-Ray in Finland. 

==Reception==
===Critical response===
From BFI London Film Festival screening, Matt Micucci of CineCola praises the films screenplay and soundtrack, while calling "a smart and surprisingly quick witted film as well as the perfect tribute to a celebrated and much loved comic strip that has the potential of winning it an even greater and more international audience".  W!ZARD Radio has also given positive review for the film and calls the film "clear cautionary tale against the corporate modern world and its obsession with image, reminding us to take a breather and be content with a simpler way of life".  Sara Steensig of gbtimes was enthusiastic towards the film despite pointing out of Moominpappas painful hangover and Moomintrolls bitter jealousy, but she notes after that "These are phenomena that adults will recognize but most children will not, and they are shown in a way that will not make young kids wonder about things they are not ready for." 

===Box office===
As of October 26, 2014, Moomins on the Riviera grossed $1,381,862 in Finland.  The film first earned $337,391 (28,500 viewers) on its first weekend, surpassing The Grump, and became the highest grossing film during its first two weekends in Finland.  

==Possible TV-series==
In an October 2014 blog article of Screendaily, Sophia Jansson has given a statement that the films “artistic team has made an effort to be true to the original drawings and the original text” and it is hinted that Jansson is now in discussions with various parties for a new, similarly animated television series. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 