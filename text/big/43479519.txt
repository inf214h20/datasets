Konthayum Poonoolum
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Konthayum Poonulum
| image          =
| caption        = 
| director       = Jijo Antony
| producer         royson
| writer         = Vinod Sreekumar
| starring       = Kunchacko Boban Bhama Manoj K Jayan
| music          = Mejo Joseph
| cinematography = Pappinu
| editing        = vijay sankar
| studio         = Relax Events
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} 2014 Indian Malayalam Family drama  directed by Jijo Antony. It stars Kunchako Boban,Bhama and Manoj K Jayan in the lead.

== Plot ==

 
A baker (Kunchacko Boban)  rushes home to be with his heavily pregnant wife (Bhama), who has been left alone. There is his dear jobless friend Shine Tom Chacko, whom he meets up with, once he is back home.

A police officer (Kalabhavan Mani), picks up a lady (Kavitha Nair) from the streets, but she disappears into thin air, making him believe that she isnt of the human kind. That he has made fun of his wifes spiritual beliefs earlier adds to his fears.

An old man (Janardhanan) doesnt know whom to turn to, when an accident leaves his daughter (Anju Aravind) and granddaughter in a critical state. A bunch of girls in a hostel decide to give the Ojho board a try, for one more time, which sends one of them wandering into the cemetery in the dead of the night.

A money lender, (Manoj K Jayan) who has developed an imaginary, invisible friend called Johny, with whom he shares drinks, is shocked beyond his wits to find Johny (Saiju Kurup) following him, one fine day. And true to his character, Johny is invisible to everyone else, which sends the stunned man into a delirium. An old time photographer (Joy Mathew) confounds us to bits with his exploration of the super natural that lands him, though not literally, in the mortuary.

==Cast==
* Kunchako Boban as Krishnan
* Bhama as Amrita
* Shine Tom Chacko as Martin
* Kalabhavan Mani as Police officer Mathachan
* Kavitha Nair as Annie Janardhanan as Freddy
* Anju Aravind as Alice
* Manoj K Jayan as Sethu
* Saiju Kurup as Johny
* Joy Mathew as Aloshi
*Shobha Mohan as Mary Sarayu
*Poojitha Menon as Ann
*Saiju Kurup as Johny
*Poojappura Ravi as Ramettan
*Indrans as Hamsakka
*Sona Nair as Sethus wife
*Kundara Johny as Sandhya Mohan
*Narayanankutty as Photographer
*Kanakalatha as Nun
*T Parvathy as Lekha

==References==
 

==External links==
*  

 
 
 
 
 


 