Emmy (film)
{{Infobox film
| name           = Emmy
| image          = 
| image_size     = 
| caption        = 
| director       = Steve Sekely
| producer       = 
| writer         = Viktor Rákosi (novel)   József Lengyel   István Mihály
| narrator       = 
| starring       = Gábor Rajnay  Irén Ágay Ella Gombaszögi    Pál Jávor (actor)|Pál Jávor
| music          = Szabolcs Fényes
| editing        = József Szilas
| cinematography = István Eiben  
| studio         = Mûvész Film 
| distributor    = 
| released       = 21 December 1934
| runtime        = 79 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian comedy film directed by Steve Sekely and starring Gábor Rajnay, Irén Ágay and Ella Gombaszögi. It is based on a novel by Viktor Rákosi.

==Cast==
* Gábor Rajnay - Maleczky ezredes 
* Irén Ágay - Emmy 
* Ella Gombaszögi - Melitta, társalkodónö 
* Pál Jávor (actor)|Pál Jávor - Korponay László  Antal Páger - Pálóczy 
* Mici Erdélyi - Tapsika, szubrett 
* Gyula Kabos - Sztringai Jakab 
* Karola Zala - Váthyné 
* Blanka Szombathelyi - Böske 
* László Keleti - Benkovics önkéntes 
* György Kerekes - Petri önkéntes 
* Imre Apáthi - Pozdorjai önkéntes 
* László Dezsõffy - Õrmester 
* Zoltán Makláry - Csárdás 
* Gyula Gózon - Markos

==External links==
* 

 

 
 
 
 
 
 