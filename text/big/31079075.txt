The Sporting Lover
{{Infobox film
| name           = The Sporting Lover
| image          =
| caption        =
| director       = Alan Hale
| producer       = Edward Small E.M. Asher
| writer         = 
| based on = play Good Luck by Ian Hay
| starring       = 
| music          =
| cinematography = 
| editing        = 
| studio = First National
| distributor    =
| released       = 1926
| runtime        = 
| country        = United States
| language       = Silent
| budget         =
| gross          =
}} American sports sports romance Alan Hale Barbara Bedford and Ward Crane.  THRILLS AT AMBASSADOR Good Luck by Ian Hay.

==Plot==
During the First World War an American officer and a British aristocrat fall in love but are separated. After the war he returns to find her engaged to another man. The issue is settled by a bet on The Derby horse race. 

==Cast==
* Conway Tearle - Captain Terrance Connaughton Barbara Bedford - Lady Gwendolyn
* Ward Crane - Captain Sir Phillip Barton Arthur Rankin - Algernon Cravens
* Charles McHugh - Paddy OBrien - Connaughtons Servant
* Johnny Fox - Aloysius Patrick OBrien - Paddys Son
* Bodil Rosing - Nora OBrien
* George Ovey - Jockey

==References==
 

==External links==
* 
*  at TCMDB

 

 
 
 
 
 
 
 
 


 
 