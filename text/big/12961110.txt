The Try Out
 
{{Infobox film
| name           = The Try Out
| image          = 
| image_size     = 
| caption        = 
| director       = Bobby Burns Walter Stull
| producer       = Louis Burstein
| writer         = 
| narrator       = 
| starring       = Walter Stull
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Bobby Burns - Pokes
* Walter Stull - Jabbs
* Ethel Marie Burton - Ingenue (as Ethel Burton)
* Robin Williamson - A Tramp
* Harry Naughton - Cameraman
* Frank Hanson
* Oliver Hardy - (as Babe Hardy)

==See also==
* List of American films of 1916
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 

 