The Mad Bomberg (1957 film)
{{Infobox film
| name = The Mad Bomberg
| image =
| caption =
| director = Rolf Thiele
| producer = Carola Bornée   Gero Wecker
| based on =  
| writer = Hans Jacoby   Per Schwenzen
| starring = Hans Albers   Marion Michael   Harald Juhnke   Paul Henckels
| music = Hans-Martin Majewski
| cinematography = Václav Vích
| editing =  Caspar van den Berg
| studio = Arca-Filmproduktion 
| distributor = Neue Filmverleih
| released =  
| runtime = 95 minutes
| country = West Germany 
| language = German
| budget =
| gross =
}} novel of the same title by Josef Winckler which was based on a real historical Westphalian aristocrat of the nineteenth century. The film was conceived partly as an attempt to replicate the success of Albers hit film Münchhausen (film)|Münchhausen (1943). 

==Cast==
* Hans Albers as Baron Gisbert von Bomberg  
* Marion Michael as Paula Mühlberg 
* Harald Juhnke as Dr. Roland  
* Paul Henckels as Dr. Emil Landois 
* Ingeborg Christiansen as Emma, the buxom maid  
* Gert Fröbe as Gustav-Eberhard Mühlberg  
* Camilla Spira as Frau Kommerzienrat Mühlberg 
* Ilse Künkele as Baroneß Adelheid von Twackel 
* Erich Fiedler as Baron von Twackel 
* Hubert von Meyerinck as Pastor 
* Wanda Rotha as Editha 
* Walter M. Wülf as Fuchs, the majordomo  
* Herbert Hübner as Regiment Commander von Strullbach 
* Otto Stoeckel as Kuno von Schnappwitz  
* Thea Grodtzinsky as Mathilde von Schnappwitz  
* Herbert Weissbach as Count Murveldt 
* Hans Leibelt as Professor von Wetzelstien  
* Margit Symo as Galina Krakowskaja 
* Helga Warnecke as Aunt Laura

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 