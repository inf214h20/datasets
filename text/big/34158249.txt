Ship Cafe
{{Infobox film
| name           = Ship Cafe
| image          =
| caption        =
| director       = Robert Florey
| producer       =
| writer         =
| starring       =
| music          =
| cinematography = Theodor Sparkuhl
| editing        =
| distributor    = Paramount Pictures
| released       = 9 November 1935
| runtime        = 65 min.
| country        = United States
| language       = English
| budget         =
}}

Ship Cafe is a 1935 American musical film directed by Robert Florey.

== Cast ==
* Carl Brisson as Chris Anderson
* Arline Judge as Ruby
* Mady Christians as Countess Boranoff
* William Frawley as Briney OBrien
* Eddie Davis as Eddie Davis
* Inez Courtney as Molly
* Grant Withers as Rocky Stone
* Hedda Hopper as Tutor
* Irving Bacon as Slim
* uncredited players include Jack Norton and Tiny Sandford

== External links ==
*  

 

 
 
 
 
 


 