Green Light (1937 film)
{{Infobox film
| name           = Green Light
| image          = Green-light-1937.jpg
| caption        = movie poster
| director       = Frank Borzage
| producer       = Frank Borzage
| writer         = Milton Krims Lloyd C. Douglas (novel)
| narrator       =
| starring       =
| music          = Max Steiner
| cinematography = Byron Haskin
| editing        = James Gibbon
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 85&nbsp;min.
| country        = United States|U.S. English
| budget         = $500,000 
}} American film directed by Frank Borzage.  

The film is adapted from a novel written by Lloyd C. Douglas. The novel is closely related to Douglas previous book, Magnificent Obsession, which was also adapted as a movie. 

==Plot==
Errol Flynn stars as Dr. Newell Paige, a surgeon whose refusal to name the real culprit in an operation gone fatally awry results in the ruin of his career.  Dismissed from the hospital staff, Paige leaves Massachusetts and travels to Montana to assist a researcher in Rocky Mountain spotted fever, almost dying when he subjects himself to an experimental serum.  Anita Louise stars as Phyllis Dexter, his eventual love interest, and Cedric Hardwicke as Dean Harcourt, an Anglican clergyman and radio preacher whose advice Dr. Paige at first dismisses, then later realizes is the truth.  The film ends with Paige, returned to his former post and cleared of all charges, and Phyllis seated in the cathedral, listening to Dean Harcourt quoting a Psalm, followed by the St. Luke choristers amen.

==Cast==
* Errol Flynn as Dr. Newel Paige
* Anita Louise as Phyllis Dexter
* Margaret Lindsay as Frances Ogilvie Sir Cedric Hardwicke as Dean Harcourt
* Walter Abel as John Stafford
* Spring Byington as Mrs. Dexter
* Erin OBrien-Moore as Pat Arlen
* Henry Kolker as Dr. Lane
* Pierre Watkin as Dr. Booth
* Granville Bates as Sheriff
* Russel Simpson as Sheep Man
* Myrle Sedman as A Nurse
* St. Lukes Choristers

==Original Novel==
{{infobox book |  
| name          = Green Light
| title_orig    = 
| translator    = 
| image         = 
| image_caption = 
| author        = Lloyd C. Douglas
| illustrator   = 
| cover_artist  = 
| country       =  English
| series        = 
| genre         = 
| publisher     = 
| release_date  = 1935
| english_release_date =
| media_type    = 
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}
The book was based on a best selling novel.
==Production== Captain Blood The Charge of the Light Brigade Flynn had asked Warner Brothers for a regular non swashbuckling role and this film was the result. However, after this Flynns next film was The Prince and the Pauper.
 Leslie Howard would be the star,  then they said the leads would be  Flynn and Olivia de Havilland.  De Havilland dropped out and the female leads were then to be played by Anita Louise and Ann Dvorak.  Dvorak was then replaced by Margaret Lindsay. 
==Release==
The film was popular at the box office. Tony Thomas, Rudy Behlmer * Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 52 

After completion of the film, Flynn was meant to start in The White Rajah, a biopic of Sir James Brooke based on a script by the actor himself.  However this did not eventuate.

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 