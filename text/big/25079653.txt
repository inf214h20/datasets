The Saga of the Viking Women and Their Voyage to the Waters of the Great Sea Serpent
{{Infobox film
| name           = The Saga of the Viking Women and Their Voyage to the Waters of the Great Sea Serpent
| image          = VikingWomenSeaSerpentPoster.jpg
| image_size     =
| caption        =
| director       = Roger Corman
| producer  = Roger Corman
| writer         =
| narrator       =
| starring       = Abby Dalton Susan Cabot
| music          =
| cinematography =
| editing        =
| distributor    = American International Pictures
| released       =  
| runtime        = 71 min.
| country        = United States
| language       = English
| budget         = $65,000 (estimated)
| gross          =
| website        =
| amg_id         =
}}
The Saga of the Viking Women and Their Voyage to the Waters of the Great Sea Serpent (also known as The Viking Women and the Sea Serpent) is a 1957 film directed by Roger Corman.  It starred Abby Dalton, Susan Cabot, and June Kenney. It was released by American International Pictures as a double feature with The Astounding She-Monster. The film was featured in an episode of Mystery Science Theater 3000.

==Production==
Roger Corman was inspired to make the film after being impressed by a presentation from special effects experts Jack Rabin and Irving Block. He later felt the budget was inadequate to execute what they wanted and said the film taught him an important lesson not to make a big scale movie on a low budget. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p127-129 

On the first day of filming, the actress Corman had cast in the lead, Kipp Hamilton, held out for more money, so he fired her and promoted second lead Abby Dalton instead. 

==Criticism==
The film currently holds a 2.5/10 user rating on IMDb.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 