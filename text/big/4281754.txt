Tommy Tricker and the Stamp Traveller
{{Infobox film
| name           = Tommy Tricker and the Stamp Traveller
| image          =
| caption        =
| director       = Michael Rubbo
| producer       = Rock Demers
| writer         = Michael Rubbo
| narrator       =
| starring       = Lucas Evans Anthony Rogers Jill Stanley Andrew Whitehead Paul Popowich Rufus Wainwright
| music          = Anna McGarrigle Jane McGarrigle Kate McGarrigle
| cinematography = Andreas Poulsson
| editing        = André Corriveau (filmmaker)|André Corriveau
| studio         = Les Productions La Fête Inc.
| distributor    =
| released       =  
| runtime        = 105 minutes
| country        = Canada English
| budget         =
| gross          =
}}
Tommy Tricker and the Stamp Traveller is a 1988 Canadian film, which was written and directed by Michael Rubbo. It is the seventh in the Tales for All series of childrens movies created by Les Productions la Fête.

==Premise==
It is a childrens film in which a young boy, Ralph, and his sister discover a magical ability to travel the world riding within postage stamps. Complicated by a series of rules, they are soon lost in such far-flung places as Australia and China. Ralph is plagued by a debilitating stutter, and the film is essentially a coming-of-age story wherein Ralph can speak unfettered by the movies end.

==Soundtrack==
The movie features one of the first appearances of Rufus Wainwright. Wainwright also provides the song Im a Runnin and his sister, Martha Wainwright, provides the song "Tommy, Come Back" for the soundtrack.

==Cast==
*Anthony Rogers .... Tommy
*Lucas Evans .... Ralph
*Jill Stanley .... Nancy
*Andrew Whitehead .... Albert
*Paul Popowich .... Cass
*Han Yun .... Mai Ling
*Chen Yuen Tao .... Chen Tow
*Catherine Wright .... Cheryl
*Rufus Wainwright .... Singer

==See also==
*The Return of Tommy Tricker

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 