Manchi Manushulu
{{Infobox film
| name           = Manchi Manushulu
| image          = Manchi Manushulu.JPG
| image_size     =
| caption        = DVD cover of Manchi Manushulu film
| director       = V. B. Rajendra Prasad
| producer       = V. B. Rajendra Prasad
| writer         = 
| narrator       =
| starring       = Shobhan Babu Manjula Anjali Devi 
| music          = K. V. Mahadevan
| cinematography = 
| editing        = 
| studio         =
| distributor    =
| released       = 1974
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Manchi Manushulu (English: Good People) is a 1974 Telugu drama film directed by V. B. Rajendra Prasad and starring Shobhan Babu and Manjula. It is produced from the banner Jagapathi Pictures.
 Aa Gale Lag Jaa (1973), which in turn is remade into Tamil language entitled Uthaman (1976 film)|Uthaman in 1976 starring Shivaji Ganesan. 

==The cast==
* Shobhan Babu
* Manjula
* Anjali Devi
* Master Tito

== Soundtrack ==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =
| all_writing     =
| all_lyrics      = 
| all_music       = K. V. Mahadevan
| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = Neevu Leka Nenu Lenu (Happy)
| note1           =
| writer1         = Athreya
| music1          =
| extra1          = S. P. Balasubramaniam, P. Susheela
| length1         = 4:33
| title6          = Neevu Leka Nenu Lenu (Sad)
| note6           =
| writer6         =
| lyrics6         = Athreya
| music6          =
| extra6          = S. P. Balasubramaniam, P. Susheela
| length6         = 3:42
| title2          = Ninnu Marachipovalani
| note2           =
| writer2         =
| lyrics2         = Athreya
| music2          =
| extra2          = S. P. Balasubramaniam
| length2         = 4:33
| title3          = Padaku Padaku Ventapadaku
| note3           =
| writer3         =
| lyrics3         = Athreya
| music3          =
| extra3          = S. P. Balasubramaniam, P. Susheela
| length3         = 6:34
| title4          = Pellayyindi Prema Vinduku
| note4           =
| writer4         =
| lyrics4         = Athreya
| music4          =
| extra4          = S. P. Balasubramaniam, P. Susheela
| length4         = 4:37
| title5          = Vinu Naa Maata Vinnavante
| note5           =
| writer5         =
| lyrics5         = Arudra
| music5          =
| extra5          = S. P. Balasubramaniam, P. Susheela
| length5         = 4:03
| title7          = Harilo Ranga Hari
| note7           =
| writer7         =
| lyrics7         = Athreya
| music7          =
| extra7          = S. P. Balasubramaniam, P. Susheela
| length7         = 5:02
| title8          = 
| note8           =
| writer8         =
| lyrics8         = 
| music8          =
| extra8          = 
| length8         =
}}

==Boxoffice==
* The film ran for more than 100 days in ten centers  and celebrated Silver Jubilee in Hyderabad, India|Hyderabad. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 