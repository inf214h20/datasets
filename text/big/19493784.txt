Elly Petersen
{{Infobox Film
| name           = Elly Petersen
| image          = Elly Petersen.jpg
| image_size     = 
| caption        = DVD cover
| director       = Jon Iversen Alice OFredericks
| producer       = Henning Karmark
| writer         = Leck Fischer Mogens Klitgaard
| narrator       = 
| starring       = Bodil Kjer
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = 
| released       = 7 August 1944
| runtime        = 100 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Elly Petersen is a 1944 Danish drama film directed by Jon Iversen and Alice OFredericks. 

==Cast==
* Bodil Kjer - Elly Petersen
* Poul Reichhardt - Hjalmer
* Lilian Ellis - Nina
* Karl Gustav Ahlefeldt - Leif Faber
* Grethe Holmer - Lise Faber
* Betty Helsengreen - Agnes
* Irwin Hasselmann - Lauritsen
* Ib Schønberg - Hjalmars far
* Valdemar Skjerning

==External links==
* 

 

 
 
 
 
 
 
 
 