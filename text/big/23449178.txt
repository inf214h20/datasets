Mountain Strawberries 6
{{Infobox film
| name           = Mountain Strawberries 6
| image          = Mountain Strawberries 6.jpg
| caption        = Theatrical poster for Mountain Strawberries 6 (1994)
| film name = {{Film name
 | hangul         =   6
 | hanja          =  딸기 6
 | rr             = Sanddalgi 6
 | mr             = Santtalgi 6}}
| director       = Kim Su-hyeong 
| producer       = Kim Su-hyeong
| writer         = Ryu Ji-hyeong
| starring       = Gang Hye-ji
| music          = Lee Jong-sik
| cinematography = Chun Jo-myuong
| editing        = Ree Kyoung-ja
| distributor    = Seo Kang Planning
| released       =  
| runtime        = 92 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Mountain Strawberries 6, (hangul|산딸기 6 - Sanddalgi 6) also known as Wild Strawberries 6, is a 1994 South Korean film directed by Kim Su-hyeong. It was the sixth and final entry in the Mountain Strawberries series.

==Synopsis== Gangwon Province. They pursue an unsuccessful life of crime together until deciding to settle down and live honestly.   

==Cast==
* Gang Hye-ji 
* Lee Young-wuk
* Heo Na-hui
* Kim Kuk-hyeon
* Kim Yeong-sik
* An Jin-su
* Ra Kap-sung
* You Il-moon
* Park Bu-yang
* Oh Do-kyu

==Bibliography==

===English===
*  

===Korean===
*  
*  

==Notes==
 

 
 
 
 


 
 