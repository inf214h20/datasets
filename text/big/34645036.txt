Hope Springs (2012 film)
 Hope Springs}}
{{Infobox film
| name           = Hope Springs
| image          = Hope Springs 2012.jpg
| alt            =
| caption        = Theatrical release poster
| director       = David Frankel
| producer       = Todd Black Guymon Casady Brian Bell
| writer         = Vanessa Taylor
| starring       = Meryl Streep Tommy Lee Jones Steve Carell Theodore Shapiro
| cinematography = Florian Ballhaus
| editing        = Steven Weisberg
| studio         = Mandate Pictures Film 360 Escape Artists
| distributor    = Columbia Pictures Metro-Goldwyn-Mayer 
| released       =  
| country        = United States
| language       = English
| runtime        = 100 minutes 
| budget         = $30 million 
| gross          = $114,281,051 
}}
 romantic comedy-drama film directed by David Frankel, written by Vanessa Taylor, and starring Meryl Streep, Tommy Lee Jones and Steve Carell. The film was released on August 8, 2012. It received generally positive reviews and the cast was praised for their performances. It was nominated for a Golden Globe, and won a Peoples Choice Award.

==Plot==
Although a devoted couple, empty nesters Kay and Arnold Soames (Meryl Streep and Tommy Lee Jones) are in need of (in Kays opinion) help to reignite the spark in their marriage. They have slept in separate rooms for years since their youngest child went off to college, and forgo any physical affection. One day Kay (who works as a Coldwater Creek employee) tells Arnold (a partner in an Omaha accounting firm) she has paid for them to undergo a week of intense marriage counseling with Dr. Bernie Feld (Steve Carell) in a coastal resort town in Maine. Arnold, a creature of plodding, unimaginative routine, denies their marriage is in trouble.

In sessions with Dr. Feld they (mainly Kay) try to articulate their feelings, revitalize their relationship, and find the spark that caused them to fall in love in the first place. Dr. Feld counsels them, asking increasingly frank questions about their sex life and feelings. Arnold is angry and defensive, unwilling to see the depth of his wifes disappointment. Angry and crying, Kay goes alone to a bar where she has several glasses of wine, confides in the bartendress and learns that few others are having any sex, either. Arnold visits a nautical museum.

Back together, they spend the night in the same bed for the first time in years, and Kay awakes to find Arnolds arm around her. At this sign of progress, Dr. Feld urges new measures. They make halting attempts at intimacy on the bed of their budget motel and again in a movie theater, but this time with disastrous results.

In a one-on-one session, Dr. Feld explains to Arnold that couples seeking marriage counseling are doing so for a reason, and asks Arnold frankly, "Is this the best you can do?"  Arnold finally takes the initiative to arrange a romantic dinner and a night at a luxury inn, where they attempt to make love in front of a fireplace, but the grand design fails. At their final session, Dr. Feld tells them theyve made much progress and should take up couples therapy back home.
 renew their wedding vows on a beach with Dr. Feld and their grown children present, making promises to be more understanding and considerate of each other.

==Cast==
* Meryl Streep as Kay Soames
* Tommy Lee Jones as Arnold Soames
* Steve Carell as Dr. Bernard Bernie Feld
* Elisabeth Shue as Karen
* Jean Smart as Eileen
* Ben Rappaport as Brad
* Marin Ireland as Molly
* Mimi Rogers as Carol
* Becky Ann Baker as Cora
* Lee Cunningham as Lee, the unhappy wife

==Production==
The project was first announced in 2010 with Streep and Jeff Bridges in talks for the leads  and Mike Nichols attached as director.  Bridges soon dropped out,  and James Gandolfini and Philip Seymour Hoffman were soon attached to the project. 

The project then replaced Nichols with David Frankel,  without the involvement of Ganfoldini and Hoffman. Steve Carell joined the cast in February 2011,  with Tommy Lee Jones replacing Bridges in the opposite lead. 

Filming took place in September and October 2011 in Connecticut.  

==Reception==
Reviews were mostly positive, with critics praising the cast, particularly Streep, Jones, and Carell.  On Metacritic, which assigns a weighted mean rating out of 100 to reviews from mainstream critics, the film scored a 65 out of 100, indicating "generally positive reviews".  The film holds a 74% approval rating on aggregate review site Rotten Tomatoes with an average score of 6.6/10, based on 159 reviews. The sites consensus states "Led by a pair of mesmerizing performances from Meryl Streep and Tommy Lee Jones, Hope Springs offers filmgoers some grown-up laughs -- and a thoughtful look at mature relationships."  Rex Reed of The New York Observer praised the film, saying "I think everything about the movie is too subtle and real to appeal to the Batman demographic, but for mature audiences who have forgotten how to smile, it takes up where The Best Exotic Marigold Hotel left off."  Angie Errigo of Empire (film magazine)|Empire magazine felt that while the film was "Very funny, its also penetrating on the ravages of time on love and marriage and sweetly touching, but with abundantly incongruous randy content to heartily amuse."  Roger Ebert of the Chicago Sun-Times praised the performance of Jones, writing, "The reason to see it is for Jones. This man who can stride fearlessly through roles requiring strong, determined men, this actor who can seem in complete control, finds a character here who seems unlike any other he has played and plays it bravely." 

===Accolades===
{| class="wikitable"
|-
! Year
! Award
! Category
! Result
! Recipient
|- 2013
|- 70th Golden Globe Awards Best Actress – Motion Picture Musical or Comedy
| rowspan=2  
| rowspan=3 | Meryl Streep
|- 39th Peoples Choice Awards Favorite Dramatic Movie Actress
|- Favorite Movie Icon
| rowspan=1  
|-
|}

==Home media==
Hope Springs was released on DVD and Blu-ray Disc on December 5, 2012.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 