The Master and His Servants
 
{{Infobox film
| name           = The Master and His Servants
| image          = 
| image size     = 
| caption        = 
| director       = Arne Skouen
| producer       = Arild Brinchmann
| writer         = Arne Skouen Axel Kielland
| starring       = Claes Gill
| music          = 
| cinematography = Finn Bergan
| editing        = 
| distributor    = 
| released       = 19 February 1959
| runtime        = 83 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

The Master and His Servants ( ) is a 1959 Norwegian drama film directed by Arne Skouen. The film is based on a 1955 play by Axel Kielland, who also plays a minor character in the film. The play and the film is based on a true story from Sweden. The Master and his Servants was entered into the 9th Berlin International Film Festival.   

==Plot==
Just after Sigurd Helmer (Claes Hill) is ordained a bishop, his archrival Tornkvist (Georg Løkkeberg) hands over a letter to the police claiming Helmer obtained his position through foul play. A power struggle between the two ensues which has serious implications for Helmer. The letter proves that Helmer has slandered his competitor Tornkvist in anonymous letters. 

Tornkvist reveals his accusations at the inaugural dinner at the bishops home. At the same time he announces his engagement to the bishops daughter, Agnes (Anne-Lise Tangstad), who then sides with him against her own father. Tornkvist and Agnes leave and the party breaks up in bewilderment. Helmer is left with his wife who is torn between her love for her husband and her Christian faith. She wants him to accept responsibility like a true Christian. Eventually it emerges that it was Helmers secretary who had written the letters. The secretary writes a confession and tries to commit suicide.

==Cast==
* Claes Gill - Sugurd Helmer, biskop
* Wenche Foss - Fru Helmer
* Georg Løkkeberg - Arvid Tornkvist, dr. theol
* Urda Arneberg - Frk. Monsen
* Lars Andreas Larssen - Leif Helmer
* Anne-Lise Tangstad - Agnes Helmer Sverre Hansen - Statsadvokaten
* Harald Heide Steen - Forsvareren
* Axel Kielland - Dommeren
* Einar Sissener - Politiebetsmann
* Egil Hjorth-Jenssen - Skrivemaskinreperatøren
* Helge Essmar - Legpredikanten
* Hans Coucheron-Aamot - Steen, biskop
* Carl Frederik Prytz - Prest

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 