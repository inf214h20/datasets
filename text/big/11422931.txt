Ticket to New Year's
{{Infobox film
| name           = Ticket to New Years
| image          = GDTicketToNewYearsDVD.jpg
| caption        = Ticket to New Years DVD cover
| director       = Len DellAmico
| producer       = Len DellAmico
| starring       = The Grateful Dead
| distributor    = Monterey Home Video
| released       =  
| runtime        = 145 minutes
| country        = United States
| language       = English
}} Oakland Coliseum Arena in Oakland, California on December 31, 1987.  It was released on VHS video tape and on Laserdisc in 1996, and on DVD in 1998.

==Cooking with Jerry==
The December 31, 1987 concert was broadcast on pay-per-view TV.  The broadcast included several sketches that had been recorded in advance with members of the Grateful Dead.  These are included in Ticket to New Years, between the first and second sets.
 Tom Davis, hors doeuvres Vulcan mind meld to read the thoughts of Jerry Garcia as Santa Claus.  The sketches conclude with the band answering more questions from Deadheads.
 Bill Graham can briefly be seen, riding in over the audience on a replica of the Golden Gate Bridge.

==Track listing==
===First set===
* "Bertha (song)|Bertha" (Jerry Garcia|Garcia, Robert Hunter (lyricist)|Hunter)
* "Cold Rain And Snow" (traditional, arranged by Grateful Dead)
* "Little Red Rooster" (Willie Dixon|Dixon)
* "When Push Comes To Shove" (Garcia, Hunter)
* "When I Paint My Masterpiece" (Bob Dylan|Dylan)
* "Bird Song" (Garcia, Hunter)
* "The Music Never Stopped" (Bob Weir|Weir, John Perry Barlow|Barlow)

===Second Set===
* "Hell In A Bucket" (Weir Barlow)
* "Uncle Johns Band" (Garcia, Hunter)
* "Terrapin Station" (Garcia, Hunter)
* "Drums" (Mickey Hart|Hart, Bill Kreutzmann|Kreutzmann)
* "Space" (Garcia, Phil Lesh|Lesh, Brent Mydland|Mydland, Weir)
* "The Other One" (Kreutzmann, Weir)
* "Wharf Rat" (Garcia, Hunter)
* "Throwing Stones" (Weir, Barlow) Not Fade Away" (Buddy Holly|Hardin, Norman Petty|Petty)

===Encore=== David Nelson, and Ramblin Jack Elliott

==Concert set list== Promised Land", which does not appear on Ticket to New Years.

The concert also had a third set, the first four songs of which are not included in the video — "Man Smart, Woman Smarter", "Iko Iko", "Day-O (The Banana Boat Song)|Day-O" and "Do You Wanna Dance?". The Grateful Dead performed these songs with members of The Neville Brothers.

"Knockin on Heavens Door" concluded the show. On the video, it is presented as the encore. At the concert, it was the fifth and final song of the third set.

==Credits==
===Grateful Dead=== guitar
* percussion
* Bill Kreutzmann – drums, percussion bass
* keyboards
* Bob Weir – guitar

===Production===
* Len DellAmico – director, producer
* John Cutler and Phil Lesh – post production, audio mix
* Candace Brightman – lighting director
* Dan Healy – concert sound
* Allen Newman – line producer
* Jeffrey Norman – audio post editing
* Bill Weber, Western Images – video post editing

==References==
 
* 
* 
* 
* Scott, John W; Dolgushkin, Mike; Nixon, Stu. DeadBase XI: The Complete Guide to Grateful Dead Song Lists, 1999, DeadBase, ISBN 1-877657-22-0
 

 

 
 
 