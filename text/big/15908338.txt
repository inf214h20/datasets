Ab Tak Chhappan
 
 
{{Infobox film
| name           = Ab Tak Chhappan
| image          = Ab_Tak_Chhappan.jpg
| caption        = 
| director       = Shimit Amin
| producer       = Ram Gopal Varma
| writer         = Sandeep Srivastava
| screenplay     = 
| story          = 
| based on       =  
| starring       = Nana Patekar   Mohan Agashe  Revathi  Hrishitaa Bhatt
| music          = Salim-Sulaiman
| cinematography = Vishal Sinha
| editing        = Murad Siddiqui
| studio         = 
| distributor    = 
| released       =  
| run time       = 
| country        = India Hindi Marathi Marathi
| budget         =   (estimated)
| gross          =   (14 May 2004)
}}
 Yashpal Sharma, Mohan Agashe, Nakul Vaid, and Hrishitaa Bhatt in supporting roles.
The story revolves around Inspector Sadhu Agashe (Nana Patekar) from the Mumbai Encounter Squad famous for having killed 56 people in police encounters.  It is inspired by the life of Police sub-Inspector with Mumbai Police force Daya Nayak. The blockbuster film was premièred at the New York Asian Film Festival.    The sequel
Ab Tak Chhappan 2 was directed by Aejaz Gulab. 

==Plot==
  Yashpal Sharma) despises Sadhu to no end; he feels Sadhu intentionally belittles him. Also Imtiyaz is more concerned about adding to his encounter score and therefore ends up killing more than the primary target which is the main reason Sadhu dislikes him. To add to his woes, Imtiyaz is unable to surpass Sadhu’s encounter "score". Enter Jatin (Nakul Vaid), a rookie to this line of policing who manages to impress Sadhu. The inspector takes the newcomer under his wing, further antagonising Imtiyaz.  All of them report to the Commissioner Pradhan (Mohan Agashe) who is a fair and honest police officer.

During these events, Sadhu establishes a love-hate friendship on the phone with Zameer (Prasad Purandare), a notorious underworld leader based abroad, who grudgingly admires Sadhu for his no-nonsense attitude. Zameer and rival don, Rajashekhar run the Mumbai underworld.

Sadhu Agashe’s world begins to turn upside down as Pradhan retires and with the entrance of the new commissioner, Suchek (Jeeva (Telugu actor)|Jeeva) who has a strong link to the Don, Rajashekhar. Suchek takes a liking towards Imtiyaz who is willing to do encounters primarily of Zameers men mainly on Rajashekhars orders. Suchek starts undermining and belittling Sadhu. Sadhu continues on his honest path. Eventually, the pressures of his career take a toll on his personal life as some men kill his wife. During his personal investigation into this matter, Sadhu kills Feroz, right-hand man of Rajashekhar. Sadhu is compelled to resign from the force and Suchek (on Rajashekhars orders) sends Imtiyaz to kill Sadhu. Sadhu manages to kill him and in a peculiar chain of events, Sadhu Agashe, a once famed inspector, becomes a fugitive of the law. Suchek announces shoot at sight orders against Sadhu despite Pradhans advice to the contrary and Sadhu is forced to ask Zameer for help in escaping from India.

Meanwhile Jatin, who has been growing increasingly disenchanted by Sucheks behaviour, resigns and calls for a press conference and exposes Sucheks connection to Rajashekhar. Suchek disputes this but is suspended pending investigation.

Sadhu goes to Zameers HQ and thanks him for releasing him and tells him that he is now Zameers man. As Zameer and Sadhu are drinking alone celebrating this, Sadhu breaks a glass and uses the sharp edge to kill Zameer and escape. Then the scene rolls forward to a location abroad where Sadhu and Pradhan are having coffee. When Pradhan asks Sadhu about why he has run away thereby proving the allegations against him, Sadhu tells him that it is part of his plan. He was able to kill Zameer as a fugitive which he never could have as a cop. He says that he will now go to Rajashekhar since Rajashekhar is thrilled at Zameers death and kill him too. Sadhu says that he doesnt care what the world thinks of him and he will always be a cop and will continue his work of eliminating crime until he dies. He requests Pradhan to look after his son who is with his aunt in Pune and Pradhan contemplatively agrees. The end credits roll as Sadhu gets up and walks off after saying good bye to Pradhan.

==Cast==

*Nana Patekar as Inspector Sadhu Agashe Yashpal Sharma as Sub-Inspector Imtiaz Siiddiqui
*Prasad Purandhare as Don Zameer
*Nakul Vaid as Sub-Inspector Jatin Shukla
*Kunal Vijaykar as Sub-Inspector Francis Alvarez Jeeva as Joint Commissioner Suchak
*Revathi as Mrs. Sadhu Agashe
*Tanmay Jahagirdar as Sadhu Agashes son
*Hrishitaa Bhatt as Vaishali / Mrs. Jatin Shukla
*Parvez Khan as Firoz
*Mohan Agashe as Ex-Commissioner Pradhan
*Narayan Patil as Sub-Inspector Narayan
*Dibyendu Bhattacharya as Zameers Gang

==External links==
*  
*  

==References==
 

 

 
 
 
 
 
 