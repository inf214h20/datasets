Bengal Brigade
{{Infobox film
| name = Bengal Brigade
| image_size =
| image = Bengal Brigade FilmPoster.jpeg
| caption =
| director = Laslo Benedek
| producer = Ted Richmond
| writer = Hall Hunter (novel) Seton I. Miller
| narrator =
| starring = Rock Hudson Arlene Dahl
| music = Hans J. Salter
| cinematography = Maury Gertsman Frank Gross
| studio = Universal-International (UI)
| distributor =
| released =  
| runtime = 87 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Bengal Brigade (also known as Bengal Rifles) is a 1954 American action film directed by Laslo Benedek and starring Rock Hudson, Arlene Dahl and Ursula Thiess.

Set in British India in 1857, at the outbreak of the Indian Mutiny. A British officer, Captain Claybourne (Hudson), is cashiered from his regiment over a charge of disobeying orders, but finds that his duty to his men is far from over.

==Cast==
* Rock Hudson as Capt. Jeffrey Claybourne
* Arlene Dahl as Vivian Morrow
* Ursula Thiess as Latah
* Torin Thatcher as Col. Morrow
* Arnold Moss as Rajah Karam
* Dan OHerlihy as Capt. Ronald Blaine (as Daniel OHerlihy)
* Michael Ansara as Sgt. Maj. Furan Singh Harold Gordon as Hari Lal
* Shepard Menken as Bulbie (as Shep Menken) Leonard Strong as Mahindra
* Leslie Denison as Captain Ian McLeod (as Leslie Dennison) John Dodsworth as Captain Guy Fitz-Morell
* Ramsay Hill as Major Jennings Sujata as Indian Dancer Asoka as Indian Dancer

== External links ==
* 
*  

 

 
 
 
 
 
 
 

 