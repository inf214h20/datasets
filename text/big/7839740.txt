Pizza (2005 film)
 
{{Infobox film
| name           = Pizza
| image          = 
| caption        =  Mark Christopher
| producer       = 
| writer         = Mark Christopher
| starring       = Ethan Embry  Kylie Sparks
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  $5,716 
}} independent film Mark Christopher, region 1 DVD was released on October 24, 2006.

==Plot==
Cara-Ethyl (Kylie Sparks) is an eccentric and sheltered girl on the eve of her eighteenth birthday who desperately dreams of an exciting life.  But shes left with her blind, clueless (but well-meaning) mother (Julie Hagerty), a pest of a brother and made-up friends (Cara pretends she has a friend for her mother).

All that is changed when the pizza man, Matt Firenze (Ethan Embry), comes to the door.  Soon, Cara persuades Matt to allow her to go with him on his deliveries.  As the night progresses, Cara-Ethyl and Matt impart their wisdom and learn from each other, and both are forced to evaluate their lives.

==Cast==
*Ethan Embry as Matt Firenze
*Kylie Sparks as Cara-Ethyl
*Alexis Dziena as Emily 
*Julie Hagerty as Darlene
*Judah Friedlander as Jimmy
*Marylouise Burke as Aunt Grandma
*Richard Easton as Mr. Mitchell
*Jessica Dunphy as Desire
*Julia Kay as Gina Morrissey
*Jesse McCartney as Justin Bridges

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 