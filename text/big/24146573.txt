The Great Dan Patch
{{Infobox film
| name           = The Great Dan Patch
| image          = The Great Dan Patch (1949) 1.jpg
| image_size     = 200px
| caption        = Film still with Gail Russell and John Hoyt (center)
| director       = Joseph M. Newman
| producer       = Edward Dein (associate producer) John Taintor Foote (producer) W. R. Frank (executive producer)
| writer         = John Taintor Foote
| narrator       =
| starring       = See below
| music          = Rudy Schrager
| cinematography = Gilbert Warrenton
| editing        =
| studio         = W. R. Frank Productions
| distributor    = United Artists
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Great Dan Patch is a 1949 American film directed by Joseph M. Newman about the trotting horse Dan Patch. The film is also known as Ride a Reckless Mile (American reissue title), and Dan Patch.

==Plot Summary==

In the last decade of the 19th century, chemist David Palmer returns to Oxford, Indiana, after living in Chicago for a few years. The return is brought on by his upcoming marriage to school teacher Ruth Treadwell.

Upon his arrival, David receives a very warm welcome from his father, Dan, who makes a living breeding race horses. Dan is disappointed, however, when David tells him they are not to live as a married couple on the farm, but plans to move to Indianapolis. Ruth, who has looked forward to join the sophisticated social life in Chicago is also disappointed by the news.

Ruth is quite satisfied when they move to Indianapolis and buy a vast strip of land where David starts his new job. Soon, David visits Oxford to watch his fathers new horse debut on the track. A mistake by the groom leads to the horse being severely injured, and can only be used for breeding in the future.

Dan makes an attempt to breed the horse with Joe Patchen, a reputable stud from Illinois, and the resulting colt is named Dan Patch. The colt soon grows into a strong horse, ready to train for the race track with Dans horse trainer, Ben Lathrop.

One day David gets a visit from Ben and his tomboy teenage daughter Cissy at the estate. The visitors are treated poorly by Ruth, who as turned into an up-and-coming socialité and shows no interest in horse breeding. Ben tells David that Dan Patch has made a record training run before he and his daughter leave.

It is not until his aunt Netty sends him a telegram that David learns about his fathers illness. He rushes to Oxford and is by his fathers side as he passes. David makes a promise to his dying father that he will continue training Dan Patch. For this purpose he brings the horse home with him and builds a race track on the estate. Ruth is not at all happy with the development. As they quarrel, David realizes that he doesnt love her anymore.

However, the training is successful, and Dan Patch wins his first race at a county fair. Soon the horse is ready for the big races, and he wins in Detroit, Cleveland and Columbus. In Kentucky, Ben becomes ill and David gets to take his place as a driver. Since the other drivers refuse to race, realizing their horse wont beat Dan Patch, the horse gets to race against time, and breaks the record, running a mile in under two minutes.

Because of some trouble at Davids regular work at the chemistry company, Ruth tells him to come home and solve it, not willing to risk her high maintenance lifestyle. David sees no other alternative than to return to Indianapolis. When he goes to tell Ben and Cissy, he sees one of the younger drivers, Bud Ransome, talk to Cissy. Later, a fire breaks out in the stables, and David and Cissy manage to put it out, saving the horses.

When David comes back to Ruth, he tells her he has decided to sell the estate,quit his job and continue breeding horses instead. She demands a divorce, refusing to live on the farm with him.  Rith gets most of their assets in the divorce, and David keeps the farm and Dan Patch. Cissy is overjoyed that he has returned, having had a crush on him for years.

David understands he is in love with young Cissy, and together they decide to sell Dan Patch for $60,000 to be able to live on the farm and continue breeding horses. David and Cissy eventually marry and are blessed with a daughter.

The horse goes on to break its own record, running a mile in 1 minute fifty-six seconds in 1906. It makes also makes a lot of money from different commercial deals, involving using its name on promotional goods. In its last race, watched by David and his family, Dan Patch beats his own record by a full second. 

==Cast==
*Dennis OKeefe as David Palmer
*Gail Russell as Cissy Lathrop
*Ruth Warrick as Ruth Treadwell
*Charlotte Greenwood as Aunt Netty
*Henry Hull as Dan Palmer
*John Hoyt as Ben Lathrop
*Arthur Hunnicutt as Chet Williams
*Clarence Muse as Voodoo
*Harry Lauter as Bud Ransome
*Visalia Abbe as Dan Patch, a Horse

==Soundtrack==
* Clarence Muse - "Cant Get You Then - Cant Get You Now" (Written by Martin Broones) Alexander Laszlo)
* "My Old Kentucky Home, Good-Night" (Written by Stephen Foster)

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 