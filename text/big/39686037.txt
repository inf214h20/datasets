Peter the Mariner
{{Infobox film
| name           = Peter the Mariner
| image          = 
| image_size     = 
| caption        = 
| director       = Reinhold Schünzel 
| producer       = Reinhold Schünzel
| writer         = Heinz Gordon    Georg C. Klaren   G.A. Mindzenthy 
| narrator       = 
| starring       = Reinhold Schünzel   Renate Müller   Hans Heinrich von Twardowski   Rudolf Biebrach
| music          = 
| editing        =
| cinematography = Frederik Fuglsang
| studio         = Reinhold Schünzel Film
| distributor    = Süd-Film
| released       = 14 May 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy comedy drama film directed by Reinhold Schünzel and starring Schünzel, Renate Müller and Hans Heinrich von Twardowski. A man goes on a series of travels around the world after discovering that his wife has been unfaithful to him. 
 
==Cast==
* Reinhold Schünzel as Peter Sturz 
* Renate Müller as Victoria 
* Hans Heinrich von Twardowski as Adolf Angel 
* Rudolf Biebrach as Martin 
* Allan Durant as Herbert Röder

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 