Mondo Topless
 
 
{{Infobox film
| name           = Mondo Topless
| image          = Mondo Topless.jpg
| image_size     =
| caption        = Poster to Mondo Topless
| director       = Russ Meyer
| producer       = Eve Meyer Russ Meyer
| writer         =
| narrator       = John Furlong
| starring       = Babette Bardot Pat Barrington Darlene Gray
| music          = The Aladdins
| cinematography = Russ Meyer
| editing        = Russ Meyer
| distributor    = Eve Productions
| released       =  
| runtime        = 60 minutes
| country        = United States English
| budget         =
| gross          =
}}
Mondo Topless is a 1966 pseudo documentary directed by Russ Meyer, featuring Babette Bardot and Lorna Maitland among others. It was Meyers first color film following a string of black & white "roughie nudies", including Faster, Pussycat! Kill! Kill!. While a straightforward sexploitation film, the film owes some debt to the French new wave and cinéma vérité traditions, and is known to some under the titles Mondo Girls and Mondo Top. 

Its tagline: "Two Much For One Man...Russ Meyers Busty Buxotic Beauties ... Titilating ... Torrid ... Untopable ... Too Much For One Man!"

The film was banned in Finland.

== Plot == counter culture movement, somewhat similar to the beatnik or hippie movements that were highly prevalent during the same era. The "Topless" movement as it is called by the narrator could also be perceived as an allegorical subset of the Sexual Revolution of the 1960s.

== Cast ==
* Babette Bardot as Bouncy
* Pat Barrington as Herself (as Pat Barringer)
* Sin Lenee as Lucious
* Darlene Gray as Buxotic
* Diane Young as Yummy
* Darla Paris as Delicious
* Donna X as Xciting
* Veronique Gabriel as Herself (Europe in the Raw footage)
* Greta Thorwald as Herself (Europe in the Raw footage)
* Denice Duval as Herself (Europe in the Raw footage)
* Abundavita as Herself (Europe in the Raw footage)
* Heide Richter as Herself (Europe in the Raw footage)
* Gigi La Touche as Herself (Europe in the Raw footage)
* Yvette Le Grand as Herself (Europe in the Raw footage)
* Lorna Maitland as Herself (Lorna screentest footage)

== Documentary traditions ==
The title Mondo Topless derives from the series of Mondo film|"mondo" films of the early 1960s. The first and most successful of these was Mondo Cane (A Dogs World). The purpose of these films was to bypass censorship laws by presenting both sexual and graphically violent material in a documentary format.

Mondo Topless shares some stylistic similarities with Jean-Luc Godards collaborative effort, Le plus vieux métier du monde (The Oldest Trade in the World). Mondo Topless, like most other Meyer films, drew much of its inspiration from the more relaxed European attitudes toward sex, and was followed by a host of imitators.

== External links ==
*  
*  

 

 
 
 
 
 