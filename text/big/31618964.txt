Marumakal
{{Infobox Film
| name           = Marumakal
| image          = Marumakal.jpg
| caption        = 
| director       = M. K. Chari
| producer       = Paul Kallungal
| writer         = Kedamangalam Sadanandan
| starring       = Prem Nazir Kedamangalam Sadanandan  T. S. Muthaiah S. J. Dev Neyyattinkara Komalam Revathi Ammini S. V. Susheela L. Vijayalakshmi Durga Varma
| music          = 
| cinematography =  
| editing        = 
| studio         = M. P. Productions
| distributor    = Pearl Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}}
Marumakal is a 1952 Malayalam film that marks the acting debut of Prem Nazir. Produced by Paul Kallungal under the banner of M. P. Productions and directed by M. K. Chari, the film stars Prem Nazir in the lead role of the protagonist, and Kedamangalam Sadanandan,  T. S. Muthaiah, S. J. Dev, Neyyattinkara Komalam, Revathi, Ammini, S. V. Susheela, Vijayalakshmi and Durga Varma in other roles. Marumakal, which had a delayed release, was a fiasco at the box office. Prem Nazir is credited as Abdul Khader itself in the film. He was renamed Prem Nazir only after his next release Visappinte Vili. Nazir watched the film from Coastal Talkies, Alappuzha. 

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 
 