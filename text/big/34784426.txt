Love You to Death (film)
{{Infobox film
| name           = Love You To Death
| image          = Love You To Death.jpg
| alt            =  
| caption        = 
| director       = Rafeeq Ellias
| producer       = Chiman Salva
| writer  = Rafeeq Ellias Abhro Banerjee Yuki Ellias Pallu Newatia
| starring       = Yuki Ellias Chandan Roy Sanyal Sheeba Chaddha
| music          = Ronit Chaterji
| cinematography = Rafeeq Ellias
| editing        = Abhro Banerjee
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Love You To Death is a 2012 Hindi-language comedy film directed by Rafeeq Ellias, featuring Yuki Ellias,  Chandan Roy Sanyal, Sheeba Chaddha in the lead roles. The films music was composed by Ronit Chaterji.
The film was released on February 3, 2012.

==Cast==
* Yuki Ellias
* Chandan Roy Sanyal
* Sheeba Chaddha
* Suhasini Mulay
* Kallol Banerjee

==Critical Reception==
The film received mixed reviews from critics. Avijit Ghosh from Times of India gave it 2.5/5 stating that "LYTD is loaded with sly, witty dialogues that might have sounded great in conversations and on paper but doesnt translate into laughter on celluloid.The movies last 20 minutes are its best. Using a live art installation as the setting for the climax and where dozens of revolvers - but only one has bullets -- circulate among the guests, makes for engrossing suspense."
 http://timesofindia.indiatimes.com/entertainment/movie-reviews/hindi/Love-You-To-Death/movie-review/11727848.cms
 

==References==
 

 
 
 
 
 
 


 
 