One Man's Journey
 
{{Infobox film name = One Mans Journey image      = One-Mans-Journey-1933.jpg caption    = lobby card director  Charles Kerr (assistant) writer     = Lester Cohen Katharine Havilland-Taylor starring  Dorothy Jordan David Landau producer   = Pandro S. Berman Merian C. Cooper editing  Arthur Roberts
|distributor= RKO Radio Pictures budget     =
|released= September 8, 1933 runtime    = 72 minutes language = English
|}} American drama film, starring Lionel Barrymore as Dr. Eli Watt. The picture was based on the novel The Failure, written by Katharine Haviland-Taylor. It was remade by RKO as A Man to Remember (1938). The story tells of a small town doctor working under difficult circumstances in a rural area somewhere in the United States.

==Reception==
The film was popular at the box office. 

==Production and preservation status==
On 4 April and 11 April 2007, Turner Classic Movies premiered six films produced by Merian C. Cooper at RKO but out of distribution for more than 50 years. According to TCM host Robert Osborne, Cooper agreed to a legal settlement with RKO in 1946, after accusing RKO of not giving him all the money due him from his RKO producers contract in the 1930s. The settlement gave Cooper complete ownership of six RKO titles:

*Rafter Romance (1933) with Ginger Rogers
*Double Harness (1933) with Ann Harding and William Powell Robert Young
*One Mans Journey
*Living on Love (1937)
*A Man to Remember (1938)

According to an interview with a retired RKO executive, used as a promo on TCM for the premiere, Cooper allowed the films to be shown in 1955-1956 in a limited re-release and only in New York City.

Interestingly, Joel McCrea (son, Dr. Jimmy Watt) and Frances Dee (Jimmys fiancée, Joan Stockton) became real-life husband and wife the year that the film was released.

==References==
 

==External links==
*  
*  
*   (Includes film clip)

 

 
 
 
 
 
 
 
 
 


 