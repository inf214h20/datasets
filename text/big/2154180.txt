Twilight's Last Gleaming
{{Infobox film
| name           = Twilights Last Gleaming
| image          = twilights last gleaming movie poster.jpg
| producer       = Merv Adelson
| director       = Robert Aldrich
| writer         = Ronald M. Cohen and Edward Huebsch (screenplay) Walter Wager (novel) William Smith
| music          = Jerry Goldsmith
| cinematography = Robert B. Hauser William Martin Maury Winetrobe
| studio         = Lorimar Productions Allied Artists
| released       =  
| runtime        = 146 min. English
| budget         = $6.2 million Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 300 
| gross          = $4.5 million Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 42 
}} Bavaria studios.

Loosely based on a 1971 novel, Viper Three by Walter Wager, it tells the story of Lawrence Dell, a renegade USAF general, who escapes from a military prison and takes over an ICBM silo near Montana, threatening to launch the missiles and start World War III unless the President reveals a top secret document to the American people about the Vietnam War.
 split screen technique is used at several points in the movie to give the audience insight into the simultaneously occurring strands of the storyline. The films title, which functions on several levels, is taken from The Star-Spangled Banner, the national anthem of the United States of America:
:O say, can you see,  by the dawns early light / what so proudly we hailed at the twilights last gleaming.

==Plot==
After escaping from a military prison, rogue Air Force General Lawrence Dell (Burt Lancaster) and accomplices Powell and Garvas infiltrate an ICBM complex and gain launch control over its nine nuclear missiles. They then make direct contact with the US government (avoiding any media attention) and demand both ten million dollars ransom and, more importantly, that the President (Charles Durning) go on national television and make public the contents of a top-secret document.

The document, which is unknown to the current president but not to certain members of his United States Cabinet|cabinet, contains conclusive proof that the US government knew there was no realistic hope of winning the Vietnam War but continued fighting it for the sole purpose of demonstrating to the Soviet Union their unwavering commitment to defeating communism.

While the President and his Cabinet debate the practical, personal, and ethical aspects of agreeing to these demands, they also authorize the military to send an elite team led by General MacKenzie (Richard Widmark) to penetrate the ICBM complex and incinerate its command center with a low-yield tactical nuclear device. When this attempt is discovered, the hostage-takers respond by initiating a missile launch. As the military and President Stevens watch the underground missile silo launch covers begin to open, they agree to call off the attempt and the launch is aborted.

Eventually, the President agrees to meet the demands, which include allowing himself to be taken hostage and used as a human shield while Dell and his team make their escape from the complex. As the president leaves the White House, he asks the Secretary of Defense to release the document should he be killed in the process. US Air Force snipers kill the general but also shoot the President, who with his dying breath asks the Secretary of Defense if he will release the document. The Secretary cannot bring himself to answer.

==Cast==
 
* Burt Lancaster as Gen. Lawrence Dell
* Paul Winfield as Willis Powell
* Burt Young as Augie Garvas
* Richard Widmark as Gen. Martin MacKenzie
* Charles Durning as President Stevens
* Joseph Cotten as Secretary of State Renfrew
* Melvyn Douglas as Secretary of Defense Guthrie
* Richard Jaeckel as Capt. Towne William Marshall as Attorney General Klinger
* Roscoe Lee Browne as James Forrest Leif Erickson as Ralph Whittaker
* Gerald S. OLoughlin as Brig. Gen. ORourke William Smith as Hoxey
* John Ratzenberger as Sgt. Kopecki
 

== Distribution ==
The film did poorly at the box office. 

In France it recorded admissions of 88,945.   at Box Office Story 

It was unsuited for videocassettes, because the split-screen effects do not work well in low resolution of that format.  After the rights reverted to the film’s German co-producers, a major remastering effort was done by Bavaria Media, who released a Blu-ray edition, distributed in the United States by Olive Films in 2012. 

== See also ==
*Seven Days in May, also starring Burt Lancaster as a renegade member of the U.S. military attempting to stage a coup

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 