One for the Road (2003 film)
{{Infobox film
| name           = One for the Road
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Chris Cooke
| producer       =  
| writer         = Chris Cooke
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Steve Blackman
| cinematography = Nick Gordon Smith
| editing        = Nick Fenton
| studio         =  
| distributor    = Palisades Tartan
| released       =  
| runtime        = 94 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
One for the Road is a 2003 British comedy-drama film written and directed by Chris Cooke. Filmed on location in and around Nottinghamshire, the film stars Hywel Bennett, Gregory Chisholm, Mark Devenport, and Rupert Procter as four men who meet at a compulsory rehabilitation class after being sentenced for drink driving. {{cite web |url= http://www.bbc.co.uk/nottingham/features/2002/11/one_for_the_road.shtml|title= One for the Road|author=  |date= November 2002
|work= BBC Local: Nottinghamshire|publisher= BBC|accessdate=17 March 2013}} 
 Michael Powell Award for Best New British Feature Film at the 2003 Edinburgh International Film Festival  and for the Golden Hitchcock Award at the 2003 Dinard Festival of British Film.

==References==
 

==External links==
* 

 
 
 
 
 


 
 