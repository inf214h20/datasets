Harry Potter and the Philosopher's Stone (film)
 
 
{{Infobox film
| name           = Harry Potter and the Philosophers Stone
| image          = Harry Potter and the Philosophers Stone posters.JPG
| alt            = Two posters, one with photographs and the other hand-drawn, both depicting a young boy with glasses, an old man with glasses, a young girl holding books, a redheaded boy, and a large bearded man in front of a castle, with an owl flying. The left poster also features an adult man, an old woman, and a train, with the titles being "Harry Potter and the Philosophers Stone". The right poster has a long-nosed goblin and blowtorches, with the title "Harry Potter and the Sorcerers Stone".
| caption        = International poster displaying the Philosophers Stone title (left) and the American poster, designed by Drew Struzan, displaying the Sorcerers Stone title (right). Chris Columbus
| producer       = David Heyman
| screenplay     = Steve Kloves
| based on       =  
| starring       = Daniel Radcliffe Rupert Grint Emma Watson
| music          = John Williams
| cinematography = John Seale
| editing        = Richard Francis-Bruce
| studio         = Heyday Films 1492 Pictures Warner Bros. Pictures
| released       =  
| runtime        = 152 minutes  
| country        = United Kingdom United States 
| language       = English
| budget         = United States dollar|$125 million 
| gross          = $974.8 million 
}} Chris Columbus Warner Bros. novel of Harry Potters first year at Hogwarts as he discovers that he is a famous wizard and begins his magical education. 
 Harry Potter and the Chamber of Secrets.
 Leavesden Film Studios and historic buildings around the UK.
 Best Original Best Art Best Costume 21st highest-grossing film of all time and the second highest-grossing film in the series behind the final film, Harry Potter and the Deathly Hallows – Part 2.

==Plot==
  Harry Potter Dursleys in Hogwarts School of Witchcraft and Wizardry. After buying his school supplies from the hidden London street, Diagon Alley, Harry boards the train to Hogwarts via the concealed Platform 9¾ in Kings Cross Station.

On the train, Harry meets  , Hufflepuff, Ravenclaw, and Slytherin. As Slytherin is noted for being the house of darker wizards and witches, Harry successfully begs the magical Sorting Hat not to put him in Slytherin. He winds up in Gryffindor, along with Ron and Hermione. Rons older brothers have all gone to Gryffindor too: mischievous twins Fred and George, Percy, Charlie (who researches dragons in Romania) and Bill (who works for Gringotts Bank).

At Hogwarts, Harry begins learning wizardry and also discovers more about his past and his parents. He gets recruited for Gryffindors Quidditch (a sport in the wizarding world where people fly on broomsticks) team as a Seeker (Quidditch)|Seeker, as his father was before him. One night, he, Ron and Hermione find a giant three-headed dog named Fluffy, who belongs to Hagrid, on a restricted floor at the school. They later find out the dog is guarding the Philosophers Stone, an item that can be used to grant its owner immortality. After Ron tells Harry what he thinks about Hermione, unintentionally hurting her feelings, the 3 of them battle a fully-grown mountain troll on Halloween in the girls bathroom. Harry concludes that his potions teacher, the unfriendly Severus Snape (Alan Rickman), is trying to obtain the stone in order to return Voldemort to a human form. Harry encounters Voldemort in the Forbidden Forest where he, Ron, Hermione and Draco Malfoy (Tom Felton) are serving detention by helping Hagrid look for an injured unicorn after being caught wandering around at night.

After hearing from Hagrid that Fluffy will fall asleep if played music, Harry, Ron and Hermione decide to find the stone before Snape does. They face a series of tasks that are helping protect the stone and test each of their respective skills, which include surviving a deadly plant, flying past hundreds of flying keys and winning a violent, life-sized chess game.

 
After getting past the tasks, Harry finds out that it was not Snape who wanted the stone, but rather Defence Against the Dark Arts teacher Professor Quirrell. Quirrell removes his turban and reveals Voldemort to be living on the back of his head. Voldemort tries to convince Harry to give him the stone (which Harry suddenly finds in his pocket as the result of an enchantment by the headmaster, Albus Dumbledore), by promising to bring his parents back from the dead, but Harry refuses. Quirrell tries to kill him but Harrys touch prevents Quirrell from hurting Harry and causes his hand to turn to dust. Quirrell then tries to take the stone but Harry grabs his face, causing Quirrell to turn into dust and die. When Harry gets up, Voldemorts spirit forms and passes through Harry, knocking him unconscious, before fleeing.

Harry wakes up in the schools hospital wing, with Professor Dumbledore at his side. Dumbledore explains that the stone has been destroyed, and that, despite Ron nearly being killed in the chess match, both he and Hermione are fine. The reason Quirrell burned at Harrys touch was because when Harrys mother died to save him, her death gave Harry a magical, love-based protection against Voldemort. Harry, Ron and Hermione are rewarded house points for their heroic performances, and Neville Longbottom (Matthew Lewis) is rewarded for bravely standing up to them, winning Gryffindor the House Cup. Before Harry and the rest of the students leave for the summer, Harry realizes that while every other student is going home, Hogwarts is truly his home.

==Cast==
 
Rowling personally insisted that the cast be kept British.    Susie Figgis was appointed as casting director, working with both Columbus and Rowling in auditioning the lead roles of Harry, Ron and Hermione.    Open casting calls were held for the main three roles,  with only British children being considered.    The principal auditions took place in three parts, with those auditioning having to read a page from Harry Potter and the Philosophers Stone, then if called back, they had to improvise a scene of the students arrival at Hogwarts, they were then given several pages from the script to read in front of Columbus.  Scenes from Columbus script for the 1985 film Young Sherlock Holmes were also used in auditions.    On 11 July 2000, Figgis left the production, complaining that Columbus did not consider any of the thousands of children they had auditioned "worthy".  On 8 August 2000, the virtually unknown Daniel Radcliffe and newcomers Emma Watson and Rupert Grint were selected to play Harry Potter, Hermione Granger and Ron Weasley, respectively.  Harry Potter. David Copperfield, William Moseley, The Chronicles of Narnia series, also auditioned for the role. 
* Rupert Grint as Ron Weasley, Harrys best friend at Hogwarts. He decided he would be perfect for the part "because   ginger hair," and was a fan of the series.  Having seen a Newsround report about the open casting he sent in a video of himself rapping about how he wished to receive the part. His attempt was successful as the casting team asked for a meeting with him. 
* Emma Watson as Hermione Granger, Harrys other best friend and the trios brains. Watsons Oxford theatre teacher passed her name on to the casting agents and she had to do over five interviews before she got the part.  Watson took her audition seriously, but "never really thought   had any chance of getting the role."  The producers were impressed by Watsons self-confidence and she outperformed the thousands of other girls who had applied.       -->
 ghost of Hogwarts Gryffindor House.
* Robbie Coltrane as Rubeus Hagrid, a half-giant and Hogwarts Groundskeeper. Coltrane was Rowlings first choice for the part.    Coltrane, who was already a fan of the books, prepared for the role by talking with Rowling about Hagrids past and future. 
* Warwick Davis as Filius Flitwick, the Charms Master and head of Hogwarts Ravenclaw House.
* Richard Griffiths as Vernon Dursley, Harrys Muggle (non-magical) uncle.
* Richard Harris as Albus Dumbledore, Hogwarts Headmaster and one of the most famous and powerful wizards of all time. Harris initially rejected the role of Dumbledore, only to reverse his decision after his granddaughter stated she would never speak to him again if he did not take it.  Harry Potter and the Prisoner of Azkaban. 
* John Hurt as Mr. Ollivander, the owner of Ollivanders, the finest wand producers in the wizarding world since 382 B.C. Planet of the Apes. 
* Fiona Shaw as Petunia Dursley, Harrys Muggle aunt.
* Maggie Smith as Minerva McGonagall, the Deputy Headmistress, head of Gryffindor and transfiguration teacher at Hogwarts. Smith was Rowlings personal choice for the part. 
* Julie Walters as Molly Weasley, Rons caring mother. She shows Harry how to get to Platform  . Before Walters was cast, American actress Rosie ODonnell held talks with Columbus about playing Mrs. Weasley. 
 

Rik Mayall was cast in the role of Peeves, a poltergeist who likes to prank students in the novel. Mayall had to shout his lines off camera during takes,  but the scene ended up being cut from the film.   

==Production==
===Development=== Richard Harris Harry Potter and the Goblet of Fire where characters from the book are specified as such.  Rowling was hesitant to sell the rights because she "didnt want to give them control over the rest of the story" by selling the rights to the characters, which would have enabled Warner Bros. to make non-author-written sequels. 
 Minority Report, Memoirs of a Geisha or Harry Potter, "came together first," with him opting to direct A.I. 
 Chris Columbus, Terry Gilliam, Mike Newell, Great Expectations Oliver Twist (1948), wishing to use "that sort of darkness, that sort of edge, that quality to the cinematography," taking the colour designs from Oliver! (film)|Oliver! and The Godfather. 

{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "Harry Potter is the kind of timeless literary achievement that comes around once in a lifetime. Since the books have generated such a passionate following across the world, it was important to us to find a director that  has an affinity for both children and magic. I cant think of anyone more ideally suited for this job than Chris."
|-
| style="text-align: left;" | — Lorenzo di Bonaventura 
|}

Steve Kloves was selected to write the films screenplay. He described adapting the book as "tough", as it did not "lend itself to adaptation as well as the next two books."    Kloves often received synopses of books proposed as film adaptations from Warner Bros., which he "almost never read",  but Harry Potter jumped out at him.  He went out and bought the book, and became an instant fan of the series.  When speaking to Warner Bros., he stated that the film had to be British, and had to be true to the characters.  Kloves was nervous when he first met Rowling as he did not want her to think he was going to "  her baby."  Rowling admitted that she "was really ready to hate this Steve Kloves," but recalled her initial meeting with him: "The first time I met him, he said to me, You know who my favourite character is? And I thought, Youre gonna say Ron. I know youre gonna say Ron. But he said Hermione. And I just kind of melted."  Rowling received a large amount of creative control, an arrangement that Columbus did not mind.

Warner Bros. had initially planned to release the film over the 4 July 2001 weekend, making for such a short production window that several proposed directors pulled themselves out of the running. However, due to time constraints the date was put back to 16 November 2001. 

===Filming===
  was used as a principal filming location for Hogwarts.|alt=A large castle, with a ditch and trees in front of it.]] Divinity School Australia House was selected as the location for Gringotts Wizarding Bank,  while Christ Church, Oxford was the location for the Hogwarts trophy room.    London Zoo was used as the location for the scene in which Harry accidentally sets a snake on Dudley,  with London Kings Cross railway station|Kings Cross Station also being used as the book specifies.   

 
Because the films American title was different, all scenes that mention the philosophers stone by name had to be re-shot, once with the actors saying "philosophers" and once with "sorcerers".  The children filmed for four hours and then did three hours of schoolwork. They also developed a liking for fake facial injuries from the makeup staff. Radcliffe was initially meant to wear green contact lenses as his eyes are blue, and not green like Harrys, but the lenses gave Radcliffe extreme irritation, and, upon consultation with Rowling, it was agreed that Harry could have blue eyes. 

===Design and special effects=== Georgian and Queen Anne architecture. 

Columbus originally planned to use both animatronics and CGI animation to create the films magical creatures, including Fluffy.  Nick Dudman, who worked on  , was given the task of creating the needed prosthetics for the film, with Jim Hensons Creature Shop providing creature effects.    John Coppinger stated that the magical creatures that needed to be created for the film had to be designed multiple times.  The film features nearly 600 special effects shots, involving numerous companies. Industrial Light & Magic created Lord Voldemorts face on the back of Quirrell, Rhythm & Hues animated Norbert; and Sony Pictures Imageworks produced the films Quidditch scenes. 

===Music===
 
 
John Williams was selected to compose the films score.  Williams composed the score at his homes in Los Angeles and Tanglewood before recording it in London in August 2001. One of the main themes is entitled "Hedwigs Theme"; Williams retained it for his finished score as "everyone seemed to like it".   

==Differences from the book==
 
 Columbus repeatedly checked with Rowling to make sure he was getting minor details in the film correct.  Kloves described the film as being "really faithful" to the book. He added some dialogue, of which Rowling approved. One of the lines originally included had to be removed after Rowling told him that it would directly contradict an event in the then-unreleased Harry Potter and the Order of the Phoenix novel. 

Several minor characters have been removed from the film version, most prominent among them the spectral History of Magic teacher,   the centaur, who is described in the book as being palomino with light blonde hair, is shown to be dark in the film.  Additionally, the Quidditch pitch is altered from a traditional stadium to an open field circled by spectator towers. 

==Distribution==
===Marketing===
The first teaser poster was released in December 2000.  The first teaser trailer was released via satellite on 2 March 2001 and debuted in cinemas with the release of  . 

===Home media===
Warner Bros. first released the film on VHS and DVD on 11 February 2002 in the E3 UK    11 May 2002 in the UK  and 28 May 2002 in the US  An Ultimate Edition was later released exclusively in the US that included a Blu-ray Disc|Blu-ray and DVD. The release contains an extended version of the film, with many of the deleted scenes edited back in; additionally, the set includes the existing special features disc, Radcliffes, Grints, and Watsons first screen tests, a feature-length special Creating the World of Harry Potter Part 1: The Magic Begins, and a 48-page hardcover booklet.  The films extended version has a running time of about 159 minutes, which has previously been shown during certain television airings. 

==Reception==
===Critical reception=== The Telegraph Gone with the Wind and put "The script is faithful, the actors are just right, the sets, costumes, makeup and effects match and sometimes exceed anything one could imagine."  Jonathan Foreman  of the New York Post recalled that the film was "remarkably faithful," to its literary counterpart as well as a "consistently entertaining if overlong adaptation." 

  wished that the film had been directed by Tim Burton, finding the cinematography "bland and muggy," and the majority of the film a "solidly dull celebration of dribbling goo."  Elvis Mitchell of The New York Times was highly negative about the film, saying "  is like a theme park thats a few years past its prime; the rides clatter and groan with metal fatigue every time they take a curve." He also said it suffered from "a lack of imagination" and wooden characters, adding, "The Sorting Hat has more personality than anything else in the movie." 

===Box office=== Mamma Mia!. 
 Deathly Hallows - Part 2, which grossed more than $1 billion worldwide. 

===Accolades=== AFI Film Art Directors Broadcast Film Critics Award for Best Live Action Family Film and was nominated for Best Child Performance (for Daniel Radcliffe) and Best Composer (John Williams).   

{| class="collapsible collapsed" style="width:100%; border:1px solid #cedff2; background:#F5FAFF"
|-

! style="text-align:left;"| List of awards and nominations
|-
|  

{| class="wikitable" style="font-size:90%;"
|-
! Award
! Category
! Name
! Outcome
|- 74th Academy Awards    Best Costume Design
| Judianna Mokovsky
|  
|- Achievement in Art Direction
| Stuart Craig
|  
|- Best Original Score
| John Williams
|  
|- Amanda Awards   
| Best Foreign Feature Film
|
|  
|-
| American Film Institute Awards 2001 
| Best Digital Effects Artist:
| Robert Legato, Nick Davis, Roger Guyett
|  
|- Art Directors Guild Award 
| Excellence in Production Design for a Period or Fantasy Film
| Stuart Craig, John King, Neil Lamont, Andrew Ackland-Snow, Peter Francis, Michael Lamont, Simon Lamont, Steve Lawrence, Lucinda Thomson, Stephen Morahan, Dominic Masters, Gary Tomkins
|  
|-
| Awards of the Japanese Academy    Outstanding Foreign Language Film
|
|  
|- Artios Award 
| Feature Film – Comedy
| Jane Jenkins, Janet Hirshenson
|  
|-
| Bogey Awards   
| Bogey Award in Titanium
|
|  
|-
| rowspan="7" | 55th British Academy Film Awards  Best British Film
|
|  
|- Best Supporting Actor
| Robbie Coltrane
|  
|- Best Costume Design
| Judianna Makovsky
|  
|- Best Production Design
| Stuart Craig
|  
|- Best Makeup & Hair
| Nick Dudman, Eithne Fennel, Amanda Knight
|  
|- Best Sound
|
|  
|- Best Visual Effects
|
|  
|-
| rowspan="3" | Broadcast Film Critics Association 
| Best Family Film (Live Action)
|
|  
|-
| Best Child Performance
| Daniel Radcliffe
|  
|-
| Best Composer
| John Williams
|  
|- Broadcast Music Incorporated Film & TV Awards   
| BMI Film Music Award
| John Williams
|  
|- Costume Designers Guild Award 
| Excellence in Fantasy Costume Design
| Judianna Makovsky
|  
|- Eddie Awards   
| Best Edited Feature Film – Dramatic
| Richard Francis-Bruce
|  
|- Empire Awards   
| Best Film
|
|  
|-
| Best Debut
| Daniel Radcliffe, Rupert Grint, Emma Watson
|  
|-
| Evening Standard British Film Awards   
| Technical Achievement Award
| Stuart Craig
|  
|- Golden Reel Awards   
| Best Sound Editing – Foreign Film
| Eddy Joseph, Martin Cantwell, Nick Lowe, Colin Ritchie, Peter Holt
|  
|-
| 45th Grammy Awards    Best Score Soundtrack Album for a Motion Picture, Television or Other Visual Media
| John Williams
|  
|-
| Hugo Awards    Best Dramatic Presentation
|
|  
|-
| 2002 Kids Choice Awards    Favorite Movie
|
|  
|-
| 2002 MTV Movie Awards   
| Breakthrough Male Performance
| Daniel Radcliffe
|  
|- Phoenix Film Critics Society Awards   
| Best Family Film
|
|  
|-
| Best Newcomer
| Daniel Radcliffe
|  
|-
| Best Youth Performance
| Emma Watson
|  
|-
| Best Costume Design
| Judianna Makovsky
|  
|-
| Best Original Score
| John Williams
|  
|-
| Best Production Design
| Stuart Craig
|  
|-
| Best Visual Effects
| Robert Legato, Nick Davis, John Richardson, Roger Guyett
| 
|- 13th Producers Guild of America Awards 
| Producer of the Year Award in Theatrical Motion Pictures
| David Heyman
|  
|- Satellite Awards 
| Best Motion Picture, Animated or Mixed Media
|
|  
|-
| Best Film Editing
| Richard Francis-Bruce
|  
|-
| Best Art Direction
| Stuard Craig
|  
|-
| Best Visual Effects
| Robert Legato, Nick Davis, Roger Guyett, John Richardson
|  
|-
| Outstanding New Talent Special Achievement Award
| Rupert Grint
|  
|- Saturn Award|28th Saturn Awards   
| Best Fantasy Film
|
|  
|-
| Best Director
| Chris Columbus
|  
|-
| Best Supporting Actor
| Robbie Coltrane
|  
|-
| Best Supporting Actress
| Maggie Smith
|  
|-
| Best Performance by a Younger Actor
| Daniel Radcliffe
|  
|-
| Best Performance by a Younger Actress
| Emma Watson
|  
|-
| Best Costumes
| Judianna Makovsky
|  
|-
| Best Make-Up
| Nick Dudman, Mark Coulier, John Lambert
|  
|-
| Best Special Effects
| Robert Legato, Nick Davis, Roger Guyett, John Richardson
|  
|-
| Sierra Awards   
| Best Family Film
|
|  
|-
| Teen Choice Awards    Choice Movie, Drama/Action Adventure
|
|  
|- Young Artist Awards 
| Best Family Feature Film – Drama
|
|  
|-
| Best Performance in a Feature Film – Leading Young Actress
| Emma Watson (Tied with Scarlett Johansson)
|  
|-
| Best Performance in a Feature Film – Supporting Young Actor
| Tom Felton
|  
|-
| Best ensemble in a Feature Film
|
|  
|-
| Most Promising Young Newcomer
| Rupert Grint
|  
|}
 
|}

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 