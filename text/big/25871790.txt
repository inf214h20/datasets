Kaboom (film)
 
{{Infobox film
| name = Kaboom
| image = Gregg Araki Kaboom.jpg
| alt = 
| caption = Theatrical release poster
| director = Gregg Araki
| producer = {{Plainlist|
* Gregg Araki
* Pascal Caucheteux
* Andrea Sperling}}
| writer = Gregg Araki
| starring = {{Plainlist| Thomas Dekker
* Juno Temple
* Haley Bennett
* Roxane Mesquida
* Brennan Mejia
* James Duval}}
| music = {{Plainlist|
* Robin Guthrie
* Vivek Maddala
* Mark Peters
* Ulrich Schnauss}}
| cinematography = Sandra Valde-Hansen
| editing = Gregg Araki
| studio = {{Plainlist|
* Desperate Pictures Wild Bunch}} Sundance Selects
| released =  
| runtime = 86 minutes  
| country = {{Plainlist|
* United States
* France}}
| language = English
| budget = 
| gross = $539,957 
}} science fiction mystery Fantasy fantasy comedy Thomas Dekker, Juno Temple, Haley Bennett, and James Duval.   

A science fiction story centered on the sexual adventures of a group of college students and their investigation of a bizarre cult, the film premiered at the 2010 Cannes Film Festival,  where it was awarded the first ever Queer Palm for its contribution to lesbian, gay, bisexual or transgender issues. 

==Plot== Thomas Dekker) is an 18-year-old film student who identifies sexually as "undeclared" with a strong sexual appetite. He has been having strange dreams. He is going to college with his best friend, Stella (Haley Bennett), whom he has known since junior high. Smith finds a note saying that he is the "chosen son". He has a roommate, Thor (Chris Zylka), whom he lusts after, regretting that Thor is straight. Stella goes to a party with Smith, but hooks up with another girl, Lorelei (Roxane Mesquida). He recognizes Lorelei as one of the people in his dream. Smith notices a guy (Brendan Mejia), but he is distracted when a red-haired girl from his dream vomits on his shoe. The guy vanishes, but Smith gets picked up by London (Juno Temple), a British student. They have sex, but to Smiths regret she does not want to be with him except during the sex.

Smith visits a nude beach, and meets a man named Hunter (Jason Olive). They start to have sex, but Smith is disappointed to hear that Hunter is married. Stella discovers that Lorelei is not only unstable, but also a witch with rejection issues. Stella keeps trying to dump her, but has difficulty as the witch takes over Smiths body, and later tries to strangle her in the washroom. Stella saves herself by spraying water on the witch, causing her to burn up.

During this time, Smith continues to dream about the red-haired girl. In his dreams, they are both pursued by people wearing animal masks. Smith finds out that the girl was killed, and her head was cut off. He later meets her twin sister, who says that she was kidnapped many years ago by men wearing animal masks. He also meets the guy from the earlier party, and learns his name is Oliver. He is gay and wants to go on a date with Smith.

Smith walks in on Thor and his best friend Rex (Andy Fischer-Price) wrestling in their underwear. Though they are calling each other gay, Stella explains that only straight guys can get away with that. London seduces Rex and convinces him to have a three-way with Smith for his nineteenth birthday. The animal-masked people finally capture Smith, London, and Smiths mom. They are bundled into a van to be driven to meet the head of a secret cult. Smith finds out that it is his father (although he was always told that his father died when he was young).

Meanwhile, Stella, Oliver, and the perpetually stoned "Messiah" (James Duval) hook up to pursue the van. Oliver has powers like Loreleis, but uses them for good. It turns out that meeting Oliver wasnt chance.  He was looking for Smith to protect him. The Messiah was only acting stoned. Hes also there to protect Smith. The animal-masked people turn out to be Thor, Rex, and Hunter, whose mission is to get London and Smith to a secret underground shelter to survive the explosion of dozens of nuclear bombs. Non-cult members will be annihilated, and the cult will take over the world with Smith as its leader.

The Messiah tries to run the van off the road, and both vehicles scream towards a bridge that is out. Smiths father presses a button and the whole Earth explodes.

==Cast==
 , Gregg Araki and Roxane Mesquida promoting the film at the 2010 Deauville American Film Festival.]] Thomas Dekker as Smith
* Juno Temple as London
* Haley Bennett as Stella
* Roxane Mesquida as Lorelei/Laura
* Brennan Mejia as Oliver
* James Duval as The Messiah
* Kelly Lynch as Smiths mother
* Chris Zylka as Thor
* Nicole LaLiberte as Madeleine OHara/Rebecca Novak
* Andy Fischer-Price as Rex
* Jason Olive as Hunter
* Carlo Mendez as Milo
* Brandy Futch as Drug Fairy Nymph

==Reception==
The film received mixed reviews from critics. Review aggregator Rotten Tomatoes reports that 58% out of 88 critics gave the film a positive review.  On Metacritic, the film has a 64/100 rating, indicating "generally favorable reviews". 

Bruce DeMara from the Toronto Star praised the films cast and called it "Araki’s most ambitious   to date, with a quick pace, music that’s hip and cool and a mood that alternates between playful and eccentric."  Sam Adams from the Los Angeles Times was much more critical about it, and said it was "less a movie than a masturbatory doodle, a sloppy, shoddy regurgitation of Araki’s pet trope that tries to pass off its slipshod structure as a free-wheeling lark." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 