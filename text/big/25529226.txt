The Sister-in-Law
{{Infobox film
| name           = The Sister-in-Law
| image          = The Sister-in-Law.jpg
| image_size     =
| alt            =
| caption        = Theatrical poster to The Sister-in-Law
| director       = Joseph Ruben
| producer       = Joseph Ruben Jonathan Noah Krivine
| writer         = Joseph Ruben
| narrator       = John Savage Will MacMillan Anne Saxon
| music          = John Savage
| cinematography = Bruce G. Sparks George Bowers
| studio         =
| distributor    = Crown International Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States English
| budget         =
| gross          =
}}
 1974 film written and directed by Joseph Ruben. The film was his directorial debut and was shot in New York City.  The film received an R rating from the MPAA. 

==Taglines==
"She Helped Her Husband Destroy His Brother By The Most Immoral Act Imaginable"

"She Kept It All In The Family!"

==Plot== John Savage), a singer-songwriter who has been traveling the country, makes a mistake when he goes to visit his brother, Edward  (Will MacMillan), and gets involved with his bored sister-in-law, Joanna (Anne Saxon).
 drug mule.

It may or may not be a mistake, which they will not live to regret. 

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- John Savage || Robert Strong
|-
| Will MacMillan || Edward Strong
|-
| Anne Saxon || Joanna Strong
|-
| Meridith Baer || Deborah Holt
|-
| Frank Scioscia || Benjo
|}

==Home Media== VOD and Blue Money, through Mill Creek Entertainment and other companies. 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 