List of Kannada films of 2007
  Sandalwood (Kannada language film industry) based in Bangalore in the year 2007.

==Commercially Successful Films of Year==
 Duniya - Blockbuster
* Hudugaata - Hit
* Snehana Preethina - Super Hit
* Cheluvina Chittara - Super Hit
* Maathaad Maathaadu Mallige - Hit
* Milana - Block Buster (500 days)
* Aa Dinagalu - Super Hit
* Ee Bandhana - Super Hit
* Anatharu - Super Hit
* Santha - Super Hit Bhoopathi - Super Hit
* Pallakki - Super Hit
* Ganesha - Hit
* Chanda - Super Hit

==List of Released Films==

{| class="wikitable"
|-
! Month !! Title !! Director !! Cast !! Music !! Other notes
|- Shashank || Prajwal Devaraj, Devaki || Hamsalekha || 
|-
|Hosavarsha || Vijay Roshan || Naveen Mayur, Arathi Puri, Shobhraj, Shikka, Devan || Sangeetha Raja ||
|-
|Ee Rajeev Gandhi Alla || Ravi Srivatsa || Vijay Raghavendra, Rakshitha || Sadhu Kokila ||
|-
|Thayiya Madilu || S. Narayan || Shivarajkumar, Rakshita || S. A. Rajkumar ||
|-
|Naayi Neralu || Girish Kasaravalli || Pavithra Lokesh, Ananya Kasaravalli || Isaac Thomas Kottakapalli ||
|-
|Sneha Parva || G K Mudduraj || Paartha, Sowmya, Chetan, Doddanna, Master Anand || Lio ||
|-
|Arasu (kannada film)|Arasu || Mahesh Babu || Puneet Rajkumar, Ramya, Meera Jasmine || Joshua Sridhar || 
|- Neethu || Abhiman Roy ||
|- Tara ||  R P Patnaik ||
|-
|Thangiya Mane || M S Srinivasa || Vijay, Rashmi, Dharma || K Raju Upendrakumar ||
|-
|Police Story 2 || Thriller Manju || Sai Kumar (Telugu actor)|Saikumar, Rockline Venkatesh, Shobharaj ||  R P Patnaik ||
|-
|Bhoopathi (2007 film)|Bhoopathi || S Govindu || Darshan Tugudeep|Darshan, Sherin || V. Harikrishna ||
|-
|SMS 6260 ||  Sundeep Malani || Diganth, Jaanu || Michael Dayao, Lokesh – Prakash, Dr. Ravi E. Dattatreya ||
|- Soori || Duniya Vijay, Rashmi, Rangayana Raghu || V. Manohar ||
|-
|Sajni || Murugesh || Sameer Dattani|Dhyan, Sharmila Mandre, Ananth Nag || A. R. Rahman ||
|-
|rowspan="6"| March  ||Parodi || Sai Prakash || Upendra, Neha, Umashree || Rajesh Ramnath ||
|-
|VIP 5 || B S Rajashekar, B S Yashodhara || Vinod Prabhakar, Mohan || Rajesh Ramnath ||
|-
|Sri Dhanamma Devi || Chindodi Bangaresh || Shivadwaj, Anu Prabhakar || Hamsalekha ||
|- Sachin || Vishnuvardhan (actor)|Vishnuvardhan, Ramesh Aravind, Gurukiran, Prema (actress)|Prema, Umashree || Gurukiran ||
|-
|Ondhu Preethiya Kathe || Rajashekar Rao K || Yagna Shetty, Shankar Aryan, Narayana Swamy || Gandarva  ||
|- Saikumar || K V Ravichandra ||
|- Tara || Hamsalekha ||
|-
|Thimma || Sai Sagar || Arjun, Saroja Devi, Prashanti Naik || Venkat - Narayan  ||
|-
|E Preethi Onthara || Tenemane Subramanyam  || Sachin Suverna, Manya (actress)|Maanya, Mithun Tejasvi || Shameer ||
|-
|Lancha Samrajya || Boodal Krishnamurthy ||  Master Hirannaiah, C. R. Simha, Umashree, Ramesh Bhat || Raju Upendra Kumar  ||
|-
|Banamathi || Jayasimha Musuri || Devaraj || NA ||
|-
|Soundarya || E Chennagangappa || Ramesh Aravind, Rahul Dev, Sakshi Shivanand, Baby Shreya  || Hamsalekha ||
|-
|Bhaktha || Sri Ram || Agnni, Sindhu || Abhiman Roy ||
|-
|Dadagiri || Joe Simon || Sri Krishna, Avinash, Shobharaj, B. C. Patil  || Abhiman Roy ||
|-
|Janapada || Baraguru Ramachandrappa || Raghava, Radhika (Kannada actress)|Radhika, Nandini || Hamsalekha ||
|-
|Bhugatha || B R Keshav || Vivek Bidu, Nagendra Urs, Daksha, Kangana, Harish Rai || M S Maruthi ||
|-
|rowspan="7"| May  || Masti (2007 film)|Masti || Shivamani || Upendra, Jennifer Kotwal, Shashikumar, Umashree || Gurukiran ||
|-
|Pallakki || Narendra Babu K || Prem Kumar (Kannada actor)|Prem, Gurukiran, Ramanithu Chaudhary || Gurukiran ||
|-
|Masanada Makkalu || C Lakshmana || Dattanna, Ohilesh || R Damodhar ||
|- Neethu || Rajbharath ||
|-
|Dushman || B.S. Bagalkote || Shobharaj || NA ||
|- Radhika  || M P Naidu ||
|-
|Santha || S. Murali Mohan || Shivarajkumar, Aarti Chhabria || Gurukiran ||
|-
|rowspan="8"| June  ||Kshana Kshana || Sunil Kumar Desai || Vishnuvardhan (actor)|Vishnuvardhan, Auditya, Prema (actress)|Prema, Kiran Rathod || R. P. Patnaik ||
|- Rekha || Jassie Gift ||
|-
|No 73, Shanthi Nivasa || Sudeep || Sudeep, Deepu, Anu Prabhakar || Ramani Bharadwaj ||
|-
|Jambada Hudugi || Priya Hassan || Jai Akash, Priya Hassan || Rajesh Ramanath ||
|-
|Cheluvina Chittara || S. Narayan || Ganesh (actor)|Ganesh, Amoolya, Komal Kumar || Mano Murthy ||
|-
|Sathyavan Savithri || Ramesh Aravind || Ramesh Aravind, Jennifer Kotwal, Daisy Bopanna, Aniruddh || Guru Kiran ||
|-
|Kaada Beladingalu || B. S. Lingadevaru || Loknath|C.H. Lokanath, H. G. Dattatreya, Ananya Kasaravalli || Rajesh Ramanath ||
|-
|Bombugalu Saar Bombugalu || B Shankar || Adarsh, Sunayana, B. C. Patil, Shobharaj || M S Maruthi ||
|- Srikanth || R. P. Patnaik ||
|-
|Thamashegagi || Kodlu Ramakrishna || Aniruddh, Rekha Vedavyas|Rekha, Komal Kumar || R P Patnaik ||
|-
|Snehana Preethina || Sahuji Shinde || Darshan Tugudeep|Darshan, Auditya, Lakshmi Rai, Sindhu Tolani || V. Harikrishna ||
|-
|Meera Madhava Raghava || T. N. Seetharam || Diganth, Ramya, Thilak, Harini || Hamsalekha ||
|-
|rowspan="7"| August  ||Manmatha (film)|Manmatha || N Rajesh Fernandis || Jaggesh, Komal Kumar, Gurleen Chopra || Vijaya Bharathi ||
|- Prem Kumar, Mallika Kapoor, Tejaswini Prakash || R. P. Patnaik||
|- Tara || Mano Murthy ||
|-
|Gandana Mane || S Mahender || Shivarajkumar, Gowri Munjal || V. Manohar ||
|-
|Ninade Nenapu || Madhan Patel || Mayur Patel, Maya || Mahesh Patel ||
|-
|Nali Naliyutha || Jayanth || Aniruddh, Vidisha (actress)|Vidisha, Ananth Nag || Rajesh Ramanath||
|-
|Jeevana Dhaare || Muraju Boodhigere || Siddarth || Raju Upendra Kumar||
|- Prakash || Puneeth Rajkumar, Parvathi Menon, Pooja Gandhi || Mano Murthy ||
|-
|Prarambha || Santosh Sivan || Prabhu Deva, Skandha, B. Saroja Devi || || Short film
|-
|Anatharu || Sadhu Kokila || Upendra, Darshan (actor)|Darshan, Radhika (Kannada actress)|Radhika, Sanghavi || Sadhu Kokila ||
|- Avinash || K M Indra ||
|- Kishore || Rajesh Ramanath ||
|- 
|rowspan="6"| October  ||Krishna (2007 film)|Krishna || M. D. Sridhar || Ganesh (actor)|Ganesh, Pooja Gandhi, Sharmila Mandre || V. Harikrishna ||
|-
|Dheemantha Manushya || Tarani || Gopal Shetty Koragal || A Karthikraj ||
|-
|Circle Rowdy || B. Ramamurthy || Vinod Prabhakar, Varini, Umashree || NA ||
|-
|Shukra || G K Mudduraj || Vinod Raj || Mallikarjuna ||
|- Harsha || Prajwal Devaraj, Tarun Chandra, Kirat Bhattal || Mano Murthy ||
|-
|Aa Dinagalu || K. M. Chaitanya || Chetan Kumar, Veda Sastry|Archana, Sharath Lohitashwa || Illayaraja ||
|-
|rowspan="8"| November  ||Yuga || Chandra || Duniya Vijay, Kavya || Arjun Janya ||
|-
|Snehanjali || Girish Kamplapur, R G Siddaramu || Dhruva || Rajesh Ramanath ||
|-
|Ganesha || Dinesh Baboo || Jaggesh, Ananth Nag, Vanitha Vasu, Vanishree || Manikanth Kadri ||
|-
|Chanda || S. Narayan || Duniya Vijay, Shubha Poonja || S. Narayan ||
|-
|Orata I Love You || Shree || Prashanth, Sowmya || G.R. Shankar ||
|- Rekha || Mano Murthy ||
|- Rekha || Hamsalekha ||
|-
|Right Adare || Sharavana || Mohan, Sunil, Satya, Bhavya, Sangeetha Shetty, Preethi, Priyanka || A. M. Neel ||
|-
|rowspan="6"| December  ||Operation Ankusha || H. Vasudev Rao || Jayanthi (actress)|Jayanthi, Ashok Rao, Chethan || V. Manohar ||
|- Lava Kusha || Sai Prakash || Shivarajkumar, Upendra, Jennifer Kotwal, Charmy Kaur || Gurukiran ||
|- Kashinath || Kashinath || Anamika ||
|-
|Nanu Neenu Jodi || N.R. Nanjunde Gowda || Vijay Raghavendra, Madhumitha || Hamsalekha ||
|- Tara || Mano Murthy ||
|- Prem || Prem (Kiran)|Prem, Namratha, Rohini, Mallika Sherawat || R P Patnaik ||
|-
|}

==See also==

*Kannada films of 2008
*Cinema of Karnataka

==References==
 

==External links==
*   at the Internet Movie Database

 
 

 
 
 
 
 