Run Leia Run
 

{{Infobox film
| name = Run Leia Run
| image =
| director = Adam Bertocci
| producer = Adam Bertocci
| writer = Adam Bertocci Will Abe Butler
| music = Brian Sadler
| distributor = TheForce.Net
| released =  
| runtime = 16 min
| language = English
| budget = $400
}}
Run Leia Run is a fan film that made its debut on the Internet in January 2003 following a premiere at Northwestern University. Created in Macromedia Flash by filmmaker Adam Bertocci, it crosses the story and style of Run Lola Run with the Star Wars universe in a retelling of The Empire Strikes Back, specifically the story of Han Solos pursuit by bounty hunters and the Empire.
 Will Butler as Han and for its success at mainstream film festivals in addition to fan events. Other notable cast members include Ben Fletcher as Darth Vader, who previously voiced the Sith Lord for popular fan films such as Knightquest and Broken Allegiance.

Run Leia Run garnered the Audience Award for Digital Animation at the 2003 Downstream International Film Festival in Decatur, GA and Best Animation at the Star Walking convention in Melbourne, Australia. Other notable screenings include DragonCon and the official Gen Con fan films track.

The UK magazine DVD Review mentioned the film in its September 2004 issue, praising it thus: "Overlaid with a throbbing hardcore soundtrack, this is wilder than a night out in the bars of Mos Eisley."

==External links==
*  
*  
*  

 

 
 
 
 

 