Finding His Voice
 
{{Infobox Hollywood cartoon
| cartoon_name = Finding His Voice
| image =
| caption =
| director = F. Lyle Goldman Max Fleischer
| script = W. E. Erpi
| animator = Al Eugster Billy Murray Walter Scanlan
| musician =
| producer = Western Electric
| distributor = Western Electric
| release_date =  
| color_process = B&W
| runtime = 10:38 minutes English
}} instructional film Billy Murray and Walter Scanlan, uncredited, provide the speaking and singing voices. Murray also provided the voice for the Fleischer Studios character Bimbo (Fleischer Studios)|Bimbo.

==Plot==
A live-action hand draws a strip of (sound) film, which takes the form of a human head, who uses musical notes to form a body. Then he sings notes that form a xylophone. Then, he performs a short solo, until another piece of film (silent) jumps on him. The talking strip yells, "Hey, Mute! Whats the big idea, ruining my act?" The silent strip uses sign language, with subtitles above, asking him about his voices origin. He talks about a man named "Dr. Western" that gave him "a set of vocal cords", saying he needs to see him, too.
 Merrily We Roll Along" as they sail on a boat, with an awkward ending of a whale eating the boat and an advertisement for Western Electric.

==Production background==
Late in 1926, AT&T and Western Electric created a licensing division, Electrical Research Products Inc. (ERPI), to handle rights to the companys film-related audio technology. (In Finding His Voice, the credits give W. E. Erpi as the author of the story.)
 Don Juan—Warners Case a sublicense for the use of the Western Electric system. In exchange for the sublicense, both Warners and ERPI received a share of Foxs related revenues. The patents of all three concerns were cross-licensed.  Superior recording and amplification technology was now available to two Hollywood studios, pursuing two very different methods of sound reproduction.

Although the film explained the Western Electric sound-on-film system, when the film was originally released, the sound was provided by the Vitaphone sound-on-disc system.

Co-director F. Lyle Goldman had done the animation for Wireless Telephony (1921) and The Mystery Box (1922) for the Bray Studios and released by Goldwyn Pictures, and The Ear (1920) for International Film Service and released by Paramount Pictures. 

==See also==
*RCA Photophone Fox Movietone
*Phonofilm
*Sound film
*Vitaphone

==References==
 

==Bibliography==
* Douglas Gomery, The Coming of Sound: A History (New York and Oxon, UK: Routledge, 2005) ISBN 0-415-96900-X

==External links==
*  
*  
*  
*  

 
 
 
 
 
 