The Unreturned
{{Infobox film
| name           = The Unreturned
| image          = Unreturnedposter newest 1000.jpg
| caption        = 
| director       = Nathan Fisher
| producer       = Nathan Fisher Matthew Bowlby Kays Mejri
| writer         = 
| narrator       = 
| starring       = 
| music          = Zaydoon Triko Evil Science Bejesus
| cinematography = Nathan Fisher
| editing        = Nathan Fisher
| distributor    = Sideways Film
| released       =  
| runtime        = 75 minutes
| country        = Canada United States
| language       = Arabic and English with English subtitles
| budget         = 
}} Iraqi refugees caught in an absurdist purgatory of endless bureaucracy, dwindling life savings, and forced idleness. The Unreturned was shot in verité style in Syria and Jordan, with unscripted narration by the refugees in the film. These Iraqis come from diverse ethnic and religious backgrounds. 
{{cite news
| url=http://www.citypages.com/2010-04-14/news/mspiff-your-passport-to-58-countries-via-145-films/
| title=EXILED: A Twin Cities filmmaker examines the biggest untold story of wartime Iraq
| publisher=City Pages
| date=2010-04-14
| author=David Hansen
}}  
{{cite news
| url=http://mspfilmfest.org/MMX/content/unreturned
| title=The Unreturned
| publisher=Minneapolis-St. Paul International Film Festival
| date=2010-03-16
| quote=
}} 

The films world premiere was April 25, 2010 at the Minneapolis-St. Paul International Film Festival, where it was awarded "Best of Festival" honors.   
{{cite news
| url=http://www.startribune.com/blogs/92235214.html?elr=KArks47cQiU17cQiU47cQUU
| title=MSPIFFs "Best of Fest" lineup May 1-6
| publisher=Minneapolis Star Tribune
| date=2010-04-27
| author=Colin Covert
}}  The film is also an official selection at the 2010 Marfa Film Festival and the 2010 Human Rights Watch International Film Festival in New York. 
{{cite news
| url=http://www.filmthreat.com/festivals/21660/
| title=2010 Marfa Film Festival Announces Program
| publisher=Film Threat
| date=2010-04-21
| author=Mark Bell
}}  
{{cite news
| url=http://marfafilmfestival.org/2010/films/
| title=Marfa Film Festival Films
| publisher=Marfa Film Festival
| date=2010-04-27
}}  
{{cite news
| url=http://www.hrw.org/en/iff/unreturned
| title=The Unreturned (NY Premiere) 
| publisher=Human Rights Watch
| date=2010-05-25
}} 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 