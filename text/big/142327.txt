Groundhog Day (film)
{{Infobox film
| name           = Groundhog Day
| image          = Groundhog Day (movie poster).jpg
| caption        = Theatrical release poster
| director       = Harold Ramis
| producer       = {{Plain list|
* Trevor Albert
* Harold Ramis
}}
| screenplay     = {{Plain list|
* Danny Rubin
* Harold Ramis
}}
| story          = Danny Rubin
| starring       = {{Plain list|
* Bill Murray
* Andie MacDowell
* Chris Elliott
}}
| music          = George Fenton John Bailey
| editing        = Pembroke J. Herring
| distributor    = Columbia Pictures
| released       = February 12, 1993
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $14.6 million   
| gross          = $70.9 million  }}   
}}
 fantasy comedy film directed by Harold Ramis, starring Bill Murray, Andie MacDowell, and Chris Elliott. It was written by Ramis and Danny Rubin, based on a story by Rubin.

Murray plays Phil Connors, an arrogant Pittsburgh TV weatherman who, during an assignment covering the annual Groundhog Day event in Punxsutawney, Pennsylvania, finds himself in a time loop, repeating the same day again and again. After indulging in hedonism and committing suicide numerous times, he begins to re-examine his life and priorities.

In 2006, the film was added to the United States National Film Registry as being deemed "culturally, historically, or aesthetically significant". 

==Plot== misanthropic TV meteorologist Phil cameraman Larry Pittsburgh television station WPBH-TV 9 travel to Punxsutawney, Pennsylvania, to cover the annual Groundhog Day festivities. The following morning, Phil, who does not like the assignment or Punxsutawney, grudgingly gives his report on the festivities. He then gets his team on the road back to Pittsburgh, but a blizzard shuts down all travel. The team is forced to return to Punxsutawney and stay another night.

Phil wakes up to find that he is reliving February 2. The day plays out exactly as it did before, with no one but Phil aware of the time loop. At first he is confused, but, when the phenomenon continues on subsequent days, he decides to take advantage of the situation with no fear of long-term consequences: he learns secrets from the towns residents, seduces women, steals money, gets drunk, drives recklessly, and gets thrown in jail. However, his attempts to get closer to Rita, to whom he has become attracted, repeatedly fail.
 depressed and tries more and more drastically to end the time loop; he gives ridiculous and offensive reports on the festival and eventually kidnaps Punxsutawney Phil and, after a police chase, drives off a high overlook into a quarry, killing both himself and the groundhog. However, Phil wakes up and finds that nothing has changed; further attempts at suicide also fail to break the time loop, as he continues to find himself waking at six oclock on the morning of February 2 with the clock radio on his nightstand playing "I Got You Babe" by Sonny & Cher.

When Phil explains the situation to Rita, she spends the day with him and into the early morning hours, but they fall asleep together and he awakens again, alone, still in the time loop. Eventually, Phil endeavors to improve himself. He begins to use his by-now vast knowledge of the days events to help as many people around town as possible, and uses the time to learn, among other things, how to play the piano, to sculpt ice, and speak French language|French.

Eventually, Phil is able to befriend almost everyone he meets during the day, using his experiences to save lives, to help townspeople, and ultimately to impress Rita, without having to resort to manipulation as on previous days.  He crafts a report on the Groundhog Day celebration so eloquent that all the other stations turn their microphones to him. After the towns evening dance, Rita "buys" Phil at the events bachelor auction. Phil makes a snow sculpture of Ritas face and they kiss, then retire to his room. He wakes the next morning and finds the time loop is broken; it is now February 3 and Rita is still with him. They walk outside and Phil proposes that they move to Punxsutawney together.
==Number of days Phil spends in Punxsutawney==
Estimates regarding how long Phil is trapped in the time loop vary widely. Director Ramis stated in the DVD commentary that he believes 10 years pass. However, in an e-mail response sent to Heeb magazine, Ramis  wrote, "I think the 10-year estimate is too short. It takes at least 10 years to get good at anything, and allotting for the down time and misguided years he spent, it had to be more like 30 or 40 years." 

According to actor Stephen Tobolowsky, Ramis told him that the entire progress of Groundhog Day covered 10,000 years. "I always thought that there were nine days represented  , and Danny Rubin, the writer, said that he felt something like 23 days were represented in the movie,   over 10,000 years." 

In 2014, a popular culture website, going through every stage of the film, calculated that Phil spent "12,395 days" in the time loop, or, without elaborating on leap years, "33 years and 350 days."   by Simon Gallagher, WhatCulture.com 

==Cast==
 
* Bill Murray as Phil Connors
* Andie MacDowell as Rita Hanson
* Chris Elliott as Larry the camera man
* Stephen Tobolowsky as Ned Ryerson
* Brian Doyle-Murray as Buster Green
* Angela Paton as Mrs. Lancaster
* Rick Ducommun as Gus
* Rick Overton as Ralph
* Robin Duke as Doris the waitress
* Marita Geraghty as Nancy Taylor
* Harold Ramis as Neurologist
* Willie Garson as Phils Asst. Kenny
* Ken Hudson Campbell as man in hallway
* Richard Henzel as D.J. #1
* Rob Riley as D.J. #2
* David Pasquesi as Psychiatrist
* Hynden Walch as Debbie the bride
* Michael Shannon as Fred the groom
* Eric Saiet as Busters son
* Peggy Roeder as the piano teacher

==Production==
  mid narrative, without explaining how or why Phil was repeating Groundhog Day. The filmmakers believed the audience would feel cheated without seeing Phils growing realization of the nature of the time loop. In addition, the original ended with Phil and Rita waking on February the 3rd and finding that Rita was now trapped in her own time loop.

 
During the filming, Ramis and Murray, despite their longtime collaboration, had a personal and professional falling out which remained unresolved for more than 10 years.  

===Location===
 
 

The shooting location     for most of the film was Woodstock, Illinois, over 50 miles northwest of Chicago about   from the Wisconsin border. Residents of the city helped in the production by bringing out heaters to warm the cast and crew in cold weather. The real Gobblers Knob is located in a rural area about   east of Punxsutawney, but the film location gives the impression that it is in the center of the town. The Tip Top Cafe, where much of the film takes place, was originally a set created for the film, but local demand led to its remaining open as a real cafe. After it closed, the Tip Top Bistro took its place, eventually to be replaced by Bellas Gelateria, and later a chicken restaurant. 

===Final scene===
Stephen Tobolowsky at Groundhog Day 2010 in Punxsutawney recalled the making of the final scene:

 

  }}

==Reception==
The film was released to generally favorable reviews, holding a score of 72 out of 100 at Metacritic.  Owen Gleiberman of Entertainment Weekly gave it a B–  and Desson Howe of The Washington Post noted that even though the film is a good Bill Murray vehicle, "Groundhog will never be designated a national film treasure by the Library of Congress".  Despite saying this, the film was selected by the National Film Preservation Board for preservation in the Library of Congress in 2006. 
 13th among Jurassic Park. 

The popularity and critical consensus of Groundhog Day has increased significantly since its initial release, with the film currently holding a 96% "Certified Fresh" rating on Rotten Tomatoes and being aired numerous times on television.  The film is regarded as a contemporary classic. Roger Ebert revisited it in his "Great Movies" series.  After giving it a three-star rating in his original review,  Ebert acknowledged in his "Great Movies" essay that, like many viewers, he had initially underestimated the films many virtues and only came to truly appreciate it through repeated viewings.
 The Fugitive). In 2000, readers of Total Film voted it the seventh greatest comedy film of all time. The Writers Guild of America ranked the screenplay #27 on their list of 101 Greatest Screenplays ever written.   In 2009, American literary theorist Stanley Fish named the film as among the ten best American films ever. 

===Awards===
* British Comedy Awards 1993 (Comedy Film)
* Saturn Award for Best Actress (Film) (Andie MacDowell, for playing Rita)

In June 2008, AFI revealed its "Ten top Ten"—the best ten films in ten "classic" American film genres—after polling over 1,500 people from the creative community. Groundhog Day was acknowledged as the eighth best film in the fantasy genre.  

American Film Institute recognition
* AFIs 100 Years...100 Laughs - #34
* AFIs 100 Years...100 Passions - Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 
* AFIs 10 Top 10 - #8 Fantasy Film

==Legacy==
The phrase "Groundhog Day" has entered common use as a reference to an unpleasant situation that continually repeats, or seems to. 
 Bosnia operations, President Clinton in January 1996 specifically referred to the movie and the use of the phrase by military personnel in Bosnia.  Fourteen years after the movie was released, "Groundhog Day" was noted as American military slang for any day of a tour of duty in Iraq.  In fact, an episode of the PBS mini-series Carrier (documentary)|Carrier that focuses on the repetition involved in a seafaring deployment is titled "Groundhog Day."
 British Prime Minister Tony Blairs treatment following the 2004 Hutton Inquiry to Groundhog Day. "  was, he said, like Groundhog Day, with the prime ministers critics demanding one inquiry, then another inquiry, then another inquiry." Blair responded approvingly, "I could not have put it better myself. Indeed I did not put it better myself." 
 spiritual transcendence. Buddhists   because they see its themes of selflessness and rebirth as a reflection of their own spiritual messages. It has also, in the Catholic tradition, been seen as a representation of Purgatory. It has even been dubbed by some religious leaders as the "most spiritual film of our time". 

Theologian Michael P. Pholey, writing for Touchstone Magazine, suggests that since "deciphering which" of the proposed philosophical and religious "interpretations is correct is no easy task, especially since" Harold Ramis "has ambiguous religious beliefs (he is an agnostic raised Jewish and married to a Buddhist)" and the films commentators "seem wedded to a single hermeneutical lens, forcing them to ignore contradictory data...A more fruitful approach, I suggest, would involve following all of the clues, clues that lead not only to religion but also to the great conversation of philosophy. Once we do so, Groundhog Day may be seen for what it is: a stunning allegory of moral, intellectual, and even religious excellence in the face of postmodern decay, a sort of Christian-Aristotelian Pilgrim’s Progress for those lost in the contemporary cosmos." 
 libertarian Ludwig von Mises Institute used the movie to illustrate a critique of mainstream economics, arguing that "In economic terms the final reliving of the day constitutes what economists refer to as a perfectly competitive equilibrium based on perfect information. ... In the hypothetical world of Phil Connors in Groundhog Day all of the parameters of the game he is playing are reset back to their original position every night while he sleeps. In the real world there are no constants." 

In August 2003,  . It cannot be improved."  In January 2014, it was revealed that a musical project was being worked on by Tim Minchin and Matthew Warchus. 

In 2004, Italian film director Giulio Manfredonia shot a remake of Groundhog Day under the title of È già ieri (Its Yesterday Already). The movie features a mixed cast of Italian and Spanish actors and actresses and is about an egocentric TV documentarian (Antonio Albanese) who finds himself trapped in a time loop during a reportage he is taking in Tenerife.

In the 2015 memoir, Guantánamo Diary, Mohamedou Ould Slahi refers to the film twice (pp.  237, 311) to describe his twelve year long (and still ongoing)  confinement in Guantanamo, Cuba. 

==See also==
* List of films featuring time loops

==References==
 

==Further reading==
* 
* 

==External links==
 
 
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*   (Feb 2 2012)
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 