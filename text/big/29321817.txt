Bleach: Hell Verse
{{Infobox film
| name           = Bleach: Hell Verse
| image          = Bleach Hell Verse poster.png
| caption        = Japanese theatrical poster
| film name = {{Film name| kanji          = BLEACH 地獄篇
| romaji         = Burīchi Jigoku-Hen}}
| director       = Noriyuki Abe
| producer       = 
| writer         = Natsuko Takahashi Ookubo Masahiro Tite Kubo (original manga)
| starring       = Masakazu Morita Fumiko Orikasa Kentarō Itō Ryōtarō Okiayu Romi Park Tōru Furuya Kazuya Nakai Masaki Aizawa Keikō Sakai Kōzō Shioya Binbin Takaoka
| music          = Shiro Sagisu
| cinematography = Toshiyuki Fukushima
| editing        = Junichi Uematsu
| studio         = Aniplex   Bandai Co., Ltd.   Dentsu   Studio Pierrot    TV Tokyo
| distributor    = Toho
| released       =  
| runtime        = 95 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = USD|$6,488,991  
}} anime and manga series Bleach (manga)|Bleach. The films theme song is "Save the One, Save the All", performed by T.M.Revolution and its screenplay was written by Natsuko Takahashi and Ookubo Masahiro, with Tite Kubo, author of the manga, overseeing the production. 
 season 14 of the animation series and included in the regular TV Tokyo schedule, which aired on November 30, 2010. A promotional manga chapter, titled Imaginary Number 01: The Unforgivens, was published showing Shuren confronting Szayel Aporro Granz and Aaroniero Arruruerie in Hell. Additionally, a promotional information book called Bleach: Official Invitation Book The Hell Verse was also released to commemorate the release of Bleach: Hell Verse. The DVD was released on August 25, 2011 in Japan. The English dub was released on DVD and Blu-ray on December 4, 2012 in the United States and on February 24, 2013 in the United Kingdom.

==Plot== Ulquiorra in Karin and Yuzu—are attacked by Shuren, the leader of the Sinners. Ichigo manages to return in time to attack Shuren, but is unable to defeat him. Kokutō, a Sinner not allied with Shuren, rescues Karin, but Shuren manages to depart with Yuzu. Kokutō offers to assist Ichigo by showing him the route into Hell, and Rukia, Renji, and Uryū Ishida decide to join the quest. 

During a fight against Kushanāda, Ichigos Hollow mask sponstaneously manifests; Kokutō explains Hell brings out ones hidden powers, and anyone killed in Hell become trapped as well. Kushanāda torture Sinners by consuming them; consumed spirits are eventually reborn at a lower level, after which they are again caught and consumed in a cycle which continues until their will is completely crushed and their remains turn to dust. Confronted by Shurens minions, Rukia, Renji and Uryū fight while Ichigo and Kokutō move on to Shurens lair, where Yuzu is held captive. Shuren orders Ichigo to destroy the Gates of Hell, believing it will free the Sinner. Once Shurens group is defeated, Kokutō reveals he tricked Shuren into luring Ichigo to Hell; Kokutō wants to use Ichigos Hollow powers to break the invisible chains which tie him to Hell. Kokutō then taunts Ichigo by revealing that Yuzu has become an Sinner, and cutting down Rukia, Uryū, and Renji. Enraged, Ichigo transforms into his Hollow form and blasts Kokutō with his Cero, destroying most of the chains binding him to Hell and burning through the gates. Renji breaks Ichigos Hollow mask and activates a kido that teleports Ichigo and Yuzu out of Hell, leaving himself, Rukia, and Uryū trapped.

The Soul Reapers arrive at the now-broken gates and begin repairs. Orihime is brought to heal Yuzu, but even her powers are not enough to undo Hells bindings; however, Yuzu later spotaneously recovers on her own. Using a Kushinada attempting to exit the gates as a distraction, Ichigo flies down to the deepest level of Hell to rescue his friends. Unfortunately, Rukia has become an Sinner, while Renji and Uryū are rotting away. Ichigo fights Kokutō, while fighting off his Hollow side. Suprisingly, the Kushanāda become a Skull-Clad armor for Ichigo, who explains that Hell itself is asking him for help; he breaks everyones chains, saving his friends, while binding Kokutō in more that drag him away into the depths of hell. Despite Rukias warning, Ichigo then discards the armor, causing the Kushanada to attack again; all four flee back the gates, where they manage to jump out into the World of the Living, just as the gates finish repairing themselves. Falling, the group is caught by Orihimes shield and safely lowered the ground as the Gates of Hell vanish.

==New characters==
*  
:  
:: The leader of the Sinner (minus Kokutō), he set up Yuzus kidnapping in order to get Ichigo into Hell so he can use his Hollow powers to tear the Gates of Hell open so he can take over the World of the Living. He is defeated by Ichigo. Shuren appears in the   as a downloadable content playable character.

*  
:  
:: A Sinner who claims to have been damned for avenging the death of his sister and the main antagonist of the film, and was revived by Hollow Ichigos fight with Ulquiorra Schiffer. He initially poses as an ally, saving Karin Kurosaki, but betrays Ichigo by killing Yuzu. He intends to use Ichigos powerful Cero to cut the chains binding him to Hell to escape, but is sealed by Ichigos Skullclad form. Kokutō appears in the   as a normal playable character.

*  
:  
:: A Sinner with regenerating tentacles for arms; a few lay hidden beneath the bandages covering the upper part of his face. He is killed by Rukia and then by Kokutō.

*  
:  
:: A muscular, dim-witted Sinner who serves Shuren. He is able to detach his arm in a similar fashion to a grappling hook, using it as his primary weapon. He is killed by Renji and then by Kokutō.

*  
:  
:: An obese Sinner that can create mouths on his body and absorb enemies attacks. Taikon talks in a rather effeminate way, even sending attacks out in the forms of kisses. He is killed by Uryū and then by Kokutō.

*  
:  
:: A Sinner that uses a triple-pronged glaive whilst attacking. His mask was shattered by Uryū and was killed by Kushanādas blade.

==Cast==
{| class="wikitable"
|-
! Character
! Japanese voice
! English voice 
|-
| Ichigo Kurosaki
| Masakazu Morita
| Johnny Yong Bosch
|-
| Rukia Kuchiki
| Fumiko Orikasa
| Michelle Ruff
|-
| Renji Abarai
| Kentarō Itō
| Wally Wingert
|-
| Uryū Ishida
| Noriaki Sugiyama
| Derek Stephen Prince
|-
| Orihime Inoue
| Yuki Matsuoka
| Stephanie Sheh
|-
| Yasutora Sado
| Hiroki Yasumoto
| Jamieson Price
|- Keigo Asano
| Katsuyuki Konishi
| Yuri Lowenthal
|- Mizuiro Kojima
| Jun Fukuyama
| Tom Fahn
|- Tatsuki Arisawa
| Junko Noda
| Wendee Lee
|- Isshin Kurosaki
| Toshiyuki Morikawa
| Patrick Seitz
|- Yuzu Kurosaki
| Ayumi Sena
| Janice Kawaye
|- Karin Kurosaki
| Rie Kugimiya
| Kate Higgins
|-
| List of Soul Reapers in Bleach#Genryūsai Shigekuni Yamamoto|Genryūsai Shigekuni Yamamoto
| Masaaki Tsukada Bob Johnson
|-
| Byakuya Kuchiki
| Ryōtarō Okiayu
| Dan Woren
|-
| Tōshirō Hitsugaya
| Romi Park
| Steve Staley
|-
| List of Soul Reapers in Bleach#Jūshirō Ukitake|Jūshirō Ukitake
| Hideo Ishikawa
| Liam OBrien
|- Rangiku Matsumoto
| Kaya Matsutani
| Megan Hollingshead
|-
| Kokutō
| Kazuya Nakai
| Travis Willingham
|-
| Shuren
| Tōru Furuya
| Benjamin Diskin
|-
| Gunjō
| Masaki Aizawa
| Jamieson Price (credited as Taylor Henry)
|-
| Garogai
| Keikō Sakai
| Jamie Simone
|-
| Taikon
| Kōzō Shioya
| Joe Ochman
|-
| Murakumo
| Binbin Takaoka Steve Kramer
|}

==References==
 

==External links==
*    
*  
*  

 
 
 

 
 
 
 
 