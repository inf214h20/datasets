Anatomy 2
{{Infobox film
| name           = Anatomy 2
| image          = Anatomie-2-poster.jpg
| caption        = German film poster
| director       = Stefan Ruzowitzky
| producer       = Andrea Willson
| writer         = Stefan Ruzowitzky
| starring       = Barnaby Metschurat Franka Potente
| music          = Marius Ruhland
| cinematography = Andreas Berger
| editing        = Hans Funck
| distributor    = Strand Releasing
| released       =  
| runtime        = 101 minutes
| country        = Germany
| language       = German English
| budget         = 
}}
Anatomy 2 ( ) is a 2003 German thriller film written and directed by Stefan Ruzowitzky that stars Franka Potente. Its the sequel to the 2000 film Anatomy (film)|Anatomy. The story moves to Berlin for this film.

== Plot ==
 Hippocratic Society for unrestricted medical research, slashes himself in front of Professor Mueller LaRousse and an audience.

The main character is the young neurosurgeon Jo from Duisburg, who wants to complete his practical course at a large hospital in Berlin. He hopes to join the research group of Mueller LaRousse because his younger brother has Muscular Dystrophy, which was the cause  of their fathers death.

Paula Henning (Franka Potente) is a police investigator investigating the Anti-Hippocratic society, but does not appear until the very end of the film. The main cast, Barnaby Metschurat, Ariane Schnug, August Diehl, Herbert Knaup, Birgit von Rönn, Klaus Schindler, are the principals.

Potentes appearance was clearly a trick to bring in theatergoers. She is an unessential character.

==Cast==
* Barnaby Metschurat as Joachim Hauser
* Ariane Schnug as Junge Kellerin
* Herbert Knaup as Prof. Charles Müller-LaRousse
* Wotan Wilke Möhring as Gregor
* Heike Makatsch as Viktoria
* Franka Potente as Paula Henning (in a cameo appearance at the end of the film)

== External links ==
*   (in German language|German)
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 