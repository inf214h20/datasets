Not with My Wife, You Don't!
{{Infobox film
| name           = Not with My Wife, You Dont!
| image          = Not with My Wife, You Dont! poster.jpg
| image_size     =
| caption        = Theatrical release poster
| producer       = Norman Panama
| director       = Norman Panama Peter Barnes (screenplay) Larry Gelbart (Screenplay)
| starring       = Tony Curtis Virna Lisi George C. Scott Richard Eastham
| music          = John Williams Johnny Mercer (Composer)
| cinematography =Charles Lang
| editing        = Aaron Stell
| studio         = Fernwood Productions Reynard Productions 
| distributor    = Warner Bros.
| released       = 1966
| runtime        = 119 minutes, Technicolor
| country        = United States
| language       = English
}}
Not with My Wife, You Dont! is a 1966 comedy film starred by Tony Curtis, Virna Lisi and George C. Scott. The film was nominated for a Golden Globe for Best Motion Picture - Musical/Comedy. The plot basically follows the standard  storyline of the long-running "road movies" popularized by Bob Hope, Bing Crosby and Dorothy Lamour, also products of the Norman Panama-Melvin Frank writing team. Curtiss and Golenbock 2009, p. 259.   

The opening title sequence and interior sequences with an animated green monster were created by Saul Bass.

==Plot== aircraft crash. She soon discovers that Martin is alive, but remains happily married to Ferris until, Martin, her former love, re-enters their lives 14 years later.

London-based Ferris, now a military attache assigned to looking after military "brass", especially General Parker (Carroll OConnor) has been neglectful of his wife. When Martin uses his influence to have Ferris shipped to Labrador for an Arctic survival course, she is prepared to seek a divorce. In the guise of an Arab potentate, Ferris, steals a V.I.P jet and wings it to Rome to reconcile with his wife. Martin really wants to keep his single lifestyle, and cant see himself as the "marrying kind." Two years later, with their marriage on firmer grounds, the Ferris family has twin boys while Ferris continues making life easy for military V.I.P.s, including the newly appointed Brig. Gen. Tank Martin, who is now flying with the United States Air Force Thunderbirds air demonstration team.

==Cast==
As appearing in screen credits (main roles identified):   IMDb. Retrieved: December 4, 2011. 
{| class="wikitable" width="60%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Tony Curtis||Col. Tom Ferris
|- Virna Lisi || Julie Ferris/Lieutenant Julietta Perodi
|- George C. Scott || Col. "Tank" Martin
|- Carroll OConnor|| General Parker 
|- Richard Eastham || General Walters
|- Eddie Ryder  || Sergeant Gilroy
|- George Tyne Sergeant Dogerty 
|- Ann Doran || Doris Parker
|-  Donna Danton || Nurse Sally Ann
|- Natalie Core || Lillian Walters
|- Buck Young || Air Police Colonel
|- Maurice Dallimore || BBC commentator
|}
  air demonstration team.]]

==Production==
Filmed with the full cooperation of the United States Air Force, scenes of contemporary North American F-86 Sabre and North American F-100 Super Sabre fighters are shot in Technicolor. Principal photography took place in Labrador, Canada, London, Rome and Lazio, Italy, as well as in Washington, District of Columbia.
 Mighty Joe Young with Bob Hope making a cameo appearance, which further accentuated the slapstick nature of the farce.  Curtis later commented that he felt that casting had always remained an issue, as he was better suited to playing the "wolf" rather than the more passive character of the besieged husband. 

==Reception==
Considered an amicable comedy typical of the period, critics like Bosley Crowther of The New York Times gave Not with My Wife, You Dont! a sympathetic review. "It is, nevertheless, the kind of farce that will someday look like a couple of million dollars in the context of the small screens regular programing. It has been beautifully photographed in Technicolor and it has a competent cast headed by Tony Curtis, Virna Lisi and George C. Scott. And, on the small screen, its gags and situations may seem almost Shavian." 

Variety (magazine)|Variety saw a great deal in the films lightweight premise, "Zesty scripting, fine performances, solid direction and strong production values sustain hilarity throughout." 

==Awards==
Not with My Wife, You Dont! was nominated for the 1967 Golden Globe in the category of Best Motion Picture - Musical/Comedy.

==References==
;Notes
 
;Citations
 
;Bibliography
 
* Curtis, Tony and Peter Golenbock. American Prince: A Memoir. New York: Three Rivers Press, 2009. ISBN 978-0-307-40856-3.
* Sheward, David. Rage and Glory: The Volatile Life and Career of George C. Scott. New York: Applause Books, 2008. ISBN 978-1-55783-670-0.
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 