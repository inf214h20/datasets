When You're Strange
{{Infobox film
| name           = When Youre Strange
| image          = When Youre Strange.jpg
| alt            = 
| caption        = The Theatrical Poster
| director       = Tom DiCillo
| producer       = John Beug Jeff Jampol Peter Jankowski
| writer         = Tom DiCillo
| narrator       = Johnny Depp
| music          = The Doors
| cinematography = Paul Ferrara
| editing        = Micky Blythe Kevin Krasny
| studio         =
| distributor    = Rhino Entertainment
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
When Youre Strange is a 2009 documentary about   publicly available. Cohen, Jonathan.  . billboard.com. December 18, 2008. 
 1991 film about the group that Stone directed, and which drew criticism from many Doors fans and several people who knew Morrison.

==Production==
The documentary was first screened at the Sundance Film Festival on January 17, 2009.  It received somewhat favorable reviews from that showing, but the narration (by director DiCillo) was singled out by most viewers as very seriously flawed for its monotonic delivery. Due to the rash of complaints about the narration, Johnny Depp was hired to redub it. A few months later, DiCillo pronounced the film "just about locked" and announced that there would be a showing of the new "redux" version. It debuted at the Los Angeles Film Festival on Sunday, June 21, 2009.
 PBS showed this film as part of its series American Masters on May 12, 2010.  The film was released on DVD on June 29, 2010.   In France the film, distributed by MK2, was released under its original title and received an excellent reception. 

==Reception==
Doors guitarist  . June 30, 2010. 

Overall Krieger felt ‘really happy’ about how the film has turned out, crediting in particular the editing work.
The surviving members of the band decided not to get too involved in the project to try to get the right neutral balance that an outsider would try to achieve." 

The film was nominated for an Emmy Award for Outstanding Nonfiction Series following its airing on American Masters on PBS.  In December 2010 the film was nominated for a Grammy Award for Best Long Form Video and subsequently won the award in February 2011. 

== Cast ==
*Jim Morrison as himself (archive footage) Vocalist for The Doors
*Ray Manzarek as himself (archive footage) Keyboardist for The Doors
*John Densmore as himself (archive footage) Percussionist for The Doors
*Robby Krieger as himself (archive footage) Guitarist for The Doors
*Johnny Depp (narration)
*Paul A. Rothchild as himself (archive footage) Producer (First Five Albums)
*Bruce Botnick as himself (archive footage) Sound Engineer and Producer for (L.A. Woman)

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 