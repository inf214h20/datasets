Four Minutes (film)
{{Infobox film
| name        = Four Minutes
| image       = Film vier minuten.jpg
| caption     = The German theatrical poster Chris Kraus
| producer    = Alexandra Kordes, Meike Kordes
| writer      = Chris Kraus
| starring    = Monica Bleibtreu Hannah Herzsprung Sven Pippig Richy Müller
| cinematography = Judith Kaufmann
| distributor = EuropaCorp Distribution Senator International
| released    =  
| runtime     = 112 minutes
| country     = Germany
| language    = German
| budget      = 
| gross       = 
| music       = 
}} Chris Kraus starring Monica Bleibtreu, Hannah Herzsprung, Sven Pippig, and Richy Müller.

==Plot==

Traude Krueger (Bleibtreu) is working as a piano teacher in a womens prison. While selecting new students, she meets Jenny Von Loeben (Herzsprung). When she tells her she cant take any lessons because her hands are too rough and she is uncooperative, Jenny becomes enraged and almost kills the prison guard, Mütze (Pippig), also one of Kruegers students. Then she starts playing the piano. Krueger listens from the hallway and, impressed by her talent, later offers Jenny lessons, but requires absolute obedience, including eating a sheet of paper. She tells Jenny never to play that kind of negro-music again.

Jennys adoptive father wanted to turn her into a Mozart-like child prodigy when she was young, but when she resisted going to further contests, he molested her. Krueger plans to have her compete again. While practicing, some inmates become jealous of Jenny, who doesnt seem to get punished for beating up the guard. Some of the prison personnel oppose giving her the freedom to play the piano. However, the prison director wants positive media attention for his prison.

Jenny reaches the finals of a piano competition for players of 21 and under. Mütze transfers her to the cell of her rival inmates. They strap her hands to the bed with some cloth and set them on fire. Jenny severely wounds one of the culprits, and she is forbidden to enter the competition. Krueger learns that Mütze deliberately set up the conflict and she confronts him. Krueger resigns, and takes her piano. Mütze aids Jenny escaping from prison with the piano so she can play at the competition.

Jenny learns that Kruger has had contact with her adoptive father. Thinking he arranged all of it, and that Krueger was just being bribed into teaching her, she rages violently. Krueger tells her about her own past, how she lost her great love, another woman, during the second world war, because she was a communist, and how she also taught her to play the piano.

Krueger convinces Jenny to play at the competition where, because the police have come to take her back to jail, she has only four minutes to convince the crowd. She diverts from the original plan of playing a piece by Schumann, and plays a unique piece of her beloved "negro-music", including percussion, foot-stomping and reaching under the lid to pound the strings. When she is finished, the crowd erupts in a standing ovation.

==Awards==

2006 
 Bavarian Film Awards
** "Best Actress" (Monica Bleibtreu) 
** "Best New Actress" (Hannah Herzsprung) 
** "Best Screenplay" 
** "Best New Director"  Shanghai International Film Festival) 
** "Best Feature Film" 
* "Best Feature Film" at the Reykjavik International Film Festival 
* Golden Biber (28th Biberach Film Festival) 
** "Best Film" 
** "Audience Award" 
* Golden Heinrich (20th International Film Festival Brunswick) 
** "Audience Award" 
* "Best Set Design" (Silke Buhr) at the 40th Hofer International Film Festival 
* State of Baden-Wuerttemberg 
** "Best Screenplay" 

2007

* German Film Awards
** "Best Feature Film in Gold" 
** "Best outstanding performances - female leading role" (Monica Bleibtreu)

== External links ==
*  
*   with English subtitles

 
 
 
 
 