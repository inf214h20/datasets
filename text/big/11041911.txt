Shaadi Ka Laddoo
 
{{Infobox film
| name           = Shaadi Ka Laddoo
| image          = ShaadiKaLaddoo.jpg
| caption        = DVD cover
| director       = Raj Kaushal
| producer       = Raj Kaushal Vicky Tejwani
| writer         = Shashank Dabral
| starring       = Sanjay Suri Mandira Bedi Aashish Chaudhary Divya Dutta Samita Bangargi Sameer Malhotra Negar Khan
| music          = Vishal Dadlani Shekhar Ravjiani
| cinematography = Amit Roy
| editing        = Sanjib Datta
| distributor    = RaMa Productions
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Shaadi Ka Laddoo is a 2004 Bollywood comedy film directed by Raj Kaushal. The film premiered on 23 April 2004.

==Plot==
India-based artist Shomu (Sanjay Suri) and his wife Geetu (Divya Dutta) are happily married with two children. Shomu decides to travel to Britain for business purposes, as well as to meet his childhood friend Ravi Kapoor (Aashish Chaudhary). Once in Britain, Shomu finds himself getting close to single women and realizes that he is ready for an extramarital affair. Ravi, to the contrary, believes that Shomu is the luckiest man on earth, as he is in love with his wife and their marriage is rock steady. Distrusting her husband, Geetu asks a British-based friend to check on him. The friend reports back that Shomu lasts long in bed. Geetu decides to go to Britain as well and catch Shomu red-handed. In the meantime, Ravi meets a waitress named Menaka Choudhary (Samita Bangargi) and decides to propose marriage to her, apprehensive that she, too, will turn him down. The stage is all set for sparks to fly.

==Cast==
*Sanjay Suri as Som Dutta 
*Mandira Bedi as Tara 
*Aashish Chaudhary as Ravi Kapur 
*Divya Dutta as Geetu 
*Samita Bangargi as Meneka Choudhary 
*Sameer Malhotra as Geetus Uncle 
*Negar Khan
*John Clubb

==Soundtrack==
{{Infobox album
| Name        = Shaadi Ka Laddoo
| Type        = soundtrack
| Artist      = Vishal-Shekhar
| Cover       = 
| Released    = 2004
| Genre       = Film soundtrack
| Length      = 24:46 Universal
| Producer    = Raj Kaushal Vicky Tejwani
| Last album  = Plan (film)|Plan (2004)
| This album  = Shaadi Ka Laddoo (2004)
| Next album  = Popcorn Khao! Mast Ho Jao (2004)
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)!! Duration
|- 1
|"Chal Hatt" Sunidhi Chauhan
|04:42
|- 2
|"Bach Ke Rehna" Sunidhi Chauhan
|03:45
|- 3
|"Kuchh To Ho Raha Hai" Shaan (singer)|Shaan & Mahalakshmi Iyer
|04:11
|- 4
|"Woh Kaun Hai" Sunidhi Chauhan
|04:35
|- 5
|"Tum Kaho To" Udit Narayan & Mahalakshmi Iyer
|04:20
|- 6
|"Biwi Ka Belan" Shaan (singer)|Shaan KK
|03:13
|}

==External links==
*  

 
 
 
 


 