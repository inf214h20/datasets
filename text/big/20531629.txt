The Real Thing at Last
{{Infobox film
| name = The Real Thing at Last
| image =
| caption =
| director = L. C. MacBean J. M. Barrie
| producer = A. E. Matthews
| writer = J. M. Barrie William Shakespeare (original play)
| starring = Edmund Gwenn Nelson Keys Godfrey Tearle Owen Nares Norman Forbes
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 30 minutes English intertitles)
| country = United Kingdom
| budget =
}}
 satirical silent movie based on the play Macbeth.  It was written in 1916 by Peter Pan creator and playwright J. M. Barrie as a parody of the American film industry.  The film was made in response to news that American filmmakers intended to honor the 300th anniversary of William Shakespeares death, and presents itself as a crass example of how they would adapt the classic play.
 Queen Mary, Princess Mary, Prince Albert (later King George VI).   

==Synopsis==

American film producer Rupert K. Thunder (Edmund Gwenn) hosted the film live.  The movie loosely follows the plot of the play, with comically melodramatic embellishments. 

==Cast==

*Edmund Gwenn - Rupert K. Thunder (live host)
*Nelson Keys - Lady Macbeth Macduff
*Owen Nares - General Banquo Duncan
*Caleb Caleb Porter - Witch George Kelly - Witch
*Ernest Thesiger - Witch
*Gladys Cooper - American Witch
*Teddie Gerard - American Witch
*Pauline Chase - American Witch
*Frederick Volpe - Murderer
*Moya Mannering - Messenger
*A.E. Matthews - Murdered
*Marie Lohr - Murdered
*Frederick Kerr - Murdered
*Irene Vanbrugh - Lady
*Eva Rowland - Lady
*Arthur Shirley - Courtier
*Leslie Henson - Charlie Chaplin

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 