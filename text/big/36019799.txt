William Tell (1923 film)
{{Infobox film
| name           = William Tell
| image          = 
| image_size     = 
| caption        = 
| director       = Rudolf Dworsky   Rudolf Walther-Fein
| producer       = 
| writer         = Willy Rath
| narrator       = 
| starring       = Hans Marr   Conrad Veidt   Erich Kaiser-Titz
| music          = 
| editing        = 
| cinematography = Guido Seeber
| studio         = Althoff-Ambos-Film
| distributor    = Aafa-Film
| released       = 1923
| runtime        = 85 minutes
| country        = Weimar Republic Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent adventure film directed by Rudolf Dworsky and Rudolf Walther-Fein and starring Hans Marr, Conrad Veidt and Erich Kaiser-Titz. The film portrays the story of the legendary Swiss national hero William Tell. The sets were designed by Rudi Feld.

==Cast==
* Hans Marr – Wilhelm Tell
* Conrad Veidt – Hermann Gessler
* Erich Kaiser-Titz – Kaiser Albrecht I
* Emil Rameau – Der Kanzler
* Fritz Kampers – Rudolf der Harra
* Hermann Vallentin – Wolffenschießen
* Josef Peterhans – Landenberger
* Erna Morena – Berta von Bruneck
* Käthe Haack – Agi, ihre Gesellschafterin
* Max Gülstorff – Attinghausen
* Johannes Riemann – Ulrich von Rudenz
* Xenia Desni – Hedwig Tell
* Otto Gebühr – Heinrich von Melchthal Theodor Becker – Konrad Baumgarten
* Carl Ebert – Arnold von Melchthal (as Karl Ebert)
* Grete Reinwald – Armgard Baumgarten, seine Frau
* Eduard von Winterstein – Stauffacher
* Robert Leffler – Rösselmann, der Pfarrer
* Wilhelm Diegelmann – Der Stier von Uri
* Agnes Straub – Stauffacherin Willi Müller – Walter, Tells Sohn
* Hans Peter Peterhans – Wilhelm, Tells Knabe

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 