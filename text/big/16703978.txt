Breath (2007 film)
{{Infobox film  name = Breath image = Breath film poster.jpg
| film name      = {{Film name hangul =   hanja =  rr = Soom mr = Sum}} director = Kim Ki-duk writer = Kim Ki-duk starring = Chang Chen Park Ji-a music = Kim Myeong-jong cinematography = Seong Jong-mu editing = Wang Su-an distributor = Sponge released =   runtime = 84 minutes language = Korean country = South Korea gross = $624,947
}}
Breath (Hangul|숨, Soom) is the fourteenth feature film by South Korean director Kim Ki-duk.

== Plot ==
A loner housewife, Yeon, deals with her depression and anger by beginning a passionate affair with a convicted man on death row. After discovering her husband’s infidelity, Yeon visits the prison where a notorious condemned criminal, Jin, is confined. She has been following the news reports of his numerous suicide attempts. Despite knowing Jins crimes, Yeon treats him like an old lover and puts all her efforts into his happiness, even though she doesnt know him.

== Cast ==
* Chang Chen ... Jang Jin
* Park Ji-a ... Yeon
* Ha Jung-woo ... Yeons husband
* Kang In-hyeong ... Young prisoner
* Kim Ki-duk ... Prison warden

== Reception ==
Breath was nominated for the Palme dOr award at the 2007 Cannes Film Festival,    although the prize was eventually awarded to the film 4 Months, 3 Weeks and 2 Days.

The film grossed a total of $624,947 internationally.  . Boxofficemojo. Retrieved March 04, 2012. 

== References ==
 

== External links ==
*  
*  
*  
*   at Festival de Cannes

 

 
 
 
 
 
 
 
 
 
 
 
 


 