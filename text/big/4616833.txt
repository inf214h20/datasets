Ernest in the Army
{{Infobox film
| name=Ernest in the Army
| image=Ernest Army.jpg
| image size = 190px
| director=John R. Cherry III
| writer= John R. Cherry III (story) Joseph Dattorre (screenplay) Jeff Pillars (screenplay)
| starring= Jim Varney Hayley Tyson
| producer= Kenneth M. Badish John R. Cherry III Emshell Producers
| released=  
| runtime = 85 min.
| country = United States
| language= English
| budget=
}}
 Army because he wants to drive large vehicles, but ends up being sent into combat. It was shot in Cape Town, South Africas Koeberg Nature Reserve. John Cherrys son, Josh portrayed Corporal Davis.

==Plot synopsis==
Ernest (Jim Varney) is working as a golf ball collector at a golf range in Valdosta, Georgia, but fantasizes about being a war hero. A friend tells him that if he joins the United States Army|Army, he will get to drive large vehicles and never have to go into actual combat. He enlists, but is deployed to the fictional Middle Eastern country of Karifistan, where his unit has to save the country from being invaded by an evil Islamic dictator named Tufuti of Aziria. Once he began, Ernest and his team investigates a dictator who was responsible for the wars in the nearby village. Suddenly, he finds a lost boy and has to keep him safe until his father is found.

==Cast==
*Jim Varney  as Private Ernest P. Worrell / Army Captain / Arab on Quicksand
*Hayley Tyson as Cindy Swanson
*David Müller  as Col. Bradley Pierre Gullet 
*Christo Davids as Ben-Ali (lost boy in desert)
*Jeff Pillars as Gen. Rodney Lincoln (as Jeff Pillars)
*Duke Ernsberger  as Barnes (Generals aide)
*Ivan Lucas as President Almar Habib Tufuti
*John R. Cherry III  as Sgt. Ben Kovsky, aka Sarge
*Josh Cherry as Corp. Davis

==DVD availability==
This film had its first DVD release from   and Ernest Goes to Africa.

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 