Pardon My Past
:For the television episode, see List of Charmed episodes#Season 2.
{{Infobox Film
| name           = Pardon My Past
| image          = Pardon My Past - 1945 Poster-color.jpg
| image_size     = 
| caption        = 1945 Theatrical Poster
| director       = Leslie Fenton
| producer       = Leslie Fenton Fred MacMurray
| writer         = Patterson McNutt (story) Harlan Ware (story) Earl Felton Karl Kamb
| starring       = Fred MacMurray Marguerite Chapman Akim Tamiroff
| music          = Dimitri Tiomkin
| cinematography = Russell Metty
| editing        = Richard V. Heermance Otho Lovering (sup)
| distributor    = Columbia Pictures
| released       = December 25, 1945 (premiere)
| runtime        = 85−89 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
  1945 comedy film about a penniless ex-veteran who gets mistaken for a millionaire by the latters family and an unpaid Bookmaker|bookie.

==Plot==
Eddie York and Chuck Gibson are two ex-soldiers leaving the service to become mink farmers in Beaver Dam, Wisconsin. As they leave New York City to travel to their new home, they stop at a tailor to get new suits. While at the tailor, Eddie is mistaken for a wealthy playboy, Francis Pemberton, by a local thug. Since Francis owes $12,000 to a bookie named Jim Arnold, the thug decides to bring Francis back with him to his boss.

The thug holds Eddie and Chuck at gunpoint and makes them follow him to Arnold, where the bookie claims his money back. Not for a second buying Eddies explanation that he is not Francis, Arnold takes Eddies wallet containing $3,000 in cash, and tells Eddie to come back the next day with the rest, or else.

Eddie sees no other alternative than to find Francis and straighten him out to get his money back. He goes to the Pemberton mansion to find Francis, but it turns out Francis hasnt been home in two years, but spent his time boozing in Mexico. Eddie is again mistaken for Francis

Chuck tries to get their money back by telling the people at the house that Francis has promised to invest $3,000 in a mink farm. This meets no objection in the household, and hey tell "Francis" to go and get cash in the family safe. Of course Eddie doesnt have the combination to the safe.

Awaiting someone who can help them open the safe, the two men are invited by Joan, a frail young woman with loose connection to the family, to stay the night at the mansion. In the evening, more Pembertons arrive back at the house, including Grandpa Pemberton, Francis wife Mary and his daughter Stephanie. Eddie is ordered by Grandpa to talk to his wife and daughter, and when he meets them they notice that there is a notable change in Francis behavior. Joan and Mary are very impressed by Francis sudden generosity and capacity. Even Grandpa notices something different about his grandson.

Uncle Wills, the man with the safe combination, arrives in the morning, and is stunned by the audacity of Francis, who usually is much more evasive and timid. Mary has left a note to Francis, where "her side of the story" is explained. It turns out that there is a feud going on between husband and wife regarding custody of their daughter Stephanie. Wills is helping Francis to get sole custody of his daughter. Unaware of this, Eddie tells uncle Wills to let Mary see Stephanie as much as she wants, making Wills surprised.

Soon Arnold arrives to the mansion to collect his money, and Eddie orders Wills to get the money from the safe. Wills tells "Francis" that he will lose half his fortune if he loses custody of the daughter, and reminds him of the lawsuit where they try to declare Mary unfit as a mother. Wills then writes a check for the sum of $12,000 to pay off Arnold.

Upon giving the check to Arnold, Eddie gets his wallet back. Before leaving the family to its own, Eddie decides to try and help fix the relations between Francis and Mary, leaving the letter from Mary to Joan, asking her to mail it to Mexico.

Wills suspects that Francis has lost his mind and tries to commit him to a psychic ward, but fails as Eddie escapes his clutches. At the same time, the real Francis arrives in a taxi to the mansion. Eddies true identity is then revealed, and Grandpa confesses that Eddie in fact is Francis twin brother, and that they were separated by birth.

Wills attacks both Eddie and Francis with accusations of concocting different plans to hurt the family fortune, but Joan is on their side and hands the letter to Francis. When Eddie goes to the tailor again to fetch his new suit, Arnold meets him there, furious over the fact that Wills check bounced. Instead, Arnold demands that his payment be in rare books from the Pemberton library, and they go there to steal books at his discretion.

Joan finds herself appalled by the way things are handled at the Pemberton house and leaves. Eddie, who has taken a liking to her, follows her, but Chuck is left behind in the clutches of Arnold who still demands his payment.

Grandpa meets Arnold and reveals to both him and Francis that Eddie is the twin brother. Francis writes another check to Arnokd, who releases Chuck. On his way out of the house, Arnold helps Eddie explain the twin-brother confusion to Jan, and she also reveals she is in love with Eddie, who proposes to her on the spot.

It turns out Wills have managed to get Mary arrested for trying to kidnap Stephanie, and the police bring her to the mansion. Eddie impersonates Francis again and make the police release Mary, and she comes into the house to see her daughter. Meanwhile, Francis has been bound and gagged by Wills, who believes he is Eddie.

Eddie goes on to trick Wills that he is Francis and gets him to confess all the letters from Mary that he has destroyed and that never reached Francis. Francis hears what Wills has done and throws him out of the house. Francis gets another chance of reconciling with Mary and takes it. Eddie and Joan elope together with Chuck in a taxi, and stop by the tailor on the way to the train station. Arnold is there again and, as a peace offering for all the misunderstandings about Eddies identity, Arnold gives them a pair of minks to start off their business. 

==Cast==
* Fred MacMurray as Eddie York / Francis Pemberton
* Marguerite Chapman as Joan
* Akim Tamiroff as Jim Arnold, the bookie
* William Demarest as Chuck Gibson, Eddies friend
* Rita Johnson as Mary Pemberton Harry Davenport as Grandpa Pemberton
* Douglass Dumbrille as Uncle Wills
* Karolyn Grimes as Stephanie Pemberton
* Dewey Robinson as Plainclothesman
* Hugh Prosser as Mr. Long

==Notes==
Fred MacMurray grew up in Beaver Dam, Wisconsin, and this film, the only one ever produced by him, is made reference to no fewer than eight times, or once every eleven minutes in the movie.

This film is not available on home video.

== References ==
 

== External links ==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 