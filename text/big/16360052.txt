Pagliacci (1948 film)
{{Infobox film
| name           = Pagliacci
| image          = Pagliacci 1948 Film Poster.jpg
| image_size     =
| caption        = Original poster for Pagliacci, 1948 Mario Costa
| producer       = Alberto Giacalone, Leopoldo Imperiali  Mario Costa 
| narrator       = 
| starring       = Tito Gobbi and Gina Lollobrigida
| music          =
| cinematography = Mario Bava
| editing        = Otello Colangeli
| distributor    =Itala Film
| released       = 1948 
| runtime        = 81 minutes
| country        = Italy Italian
}}
 1948 cinema Italian film Mario Costa. The film stars Tito Gobbi and Gina Lollobrigida. It recounts the tragedy of Canio, the lead clown (or pagliaccio in Italian) in a commedia dellarte troupe, his wife Nedda, and her lover, Silvio. When Nedda spurns the advances of Tonio, another player in the troupe, he tells Canio about Neddas betrayal. In a jealous rage Canio murders both Nedda and Silvio. The only actor in the cast who also sang his role was the celebrated Italian baritone, Tito Gobbi, but the film is largely very faithful to its source material, presenting the opera nearly complete.

The film premiered on 16 April 1950 in the USA. It is notable for having been filmed largely in outdoor settings, much like a big-budgeted film version of a Broadway musical.

==Cast==
*Tito Gobbi as  Tonio, the hunchback / Silvio
*Gina Lollobrigida as Nedda, wife of Canio
*Onelia Fineschi as  Nedda (singing voice)
*Afro Poli as Canio, master of the troupe
*Galliano Masini as  Canio (singing voice)
*Filippo Morucci as Beppe, troupe harlequin
*Gino Sinimberghi as  Beppe (singing voice)

== Sources==
*  

 

 
 
 
 
 
 
 

 