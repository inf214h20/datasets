Thirumanam Enum Nikkah
{{Infobox film
| name     = Thirumanam Ennum Nikkah
| image    = Thirumanam ennum nikkah.jpg
| director = Anis
| producer = Viswanathan Ravichandran
| writer   = Srivatsa.R Jai Nazriya Nazim Heebah Patel Jamal
| music    = M. Ghibran
| cinematography = Loganathan
| editing  = Kasi Vishwanath Aascar Film Pvt. Ltd
| distributor = 
| released =  
| runtime  =133 minutes
| country  = India Tamil
| budget   =
| gross    =
}} Tamil romance Jai and Nazriya Nazim in the lead roles, while Heebah Patel, Deekshitha Manikkam and Jamal play supporting roles. The music of the film made a lasting impact representing both the Hindu and Muslim cultures.

==Plot==
Ragava (Jai) and Vishnu Priya (Nazriya Nazim) are travelling in the train from Chennai to Coimbatore under the identities Abu Backer and Aayisha. Ragava acquires the identity of Abu as he gets a ticket reserved in that name and Priya acquires the identity of Aayisha as she is impersonating her Muslim friend for a project. Abu/Ragava helps her from a stalker and in course falls in love with her. She too develops feelings for him. Back in Chennai, they start to get to know each other. They assume that the other person is a Muslim. But soon Priya feels guilty and asks Ragava to keep away from her. Saddened by this, Ragava meets a Unani doctor Showkhat Ali (Jamal) and learns about Islam just for the sake of Aayisha. Showkhats daughter Naseema (Heebah Patel) falls in love with Ragava, assuming him to be Abu Backer. On the day of Ramzan, Priya confesses her love to Ragava and they start a relationship. Under certain circumstances, they both come to know about their true identities. Though their family arrange their marriage, they feel that they had true love only for their Muslim counterparts. On the day of their marriage, they break up and move away. Meanwhile, Naseema comes to know about Ragavas marriage and her cousin  Ashraff (Dinesh) plans for revenge. Ragava and Priya attend Priyas friend Aayishas marriage and they feel that they still have love for each other. As Ragava leaves the venue, Ashraff and his friends beat him up. Naseema stops them and confesses that only she had mistaken Ragava. Priya notices the brawl, realizes her love and hugs Ragava. Finally, they both get married.

==Cast== Jai as Ragava / Vijaya Ragava Chari (Abu Bakr) Srivatsa
* Nazriya Nazim as Vishnu Priya (Aayisha)
* Heebah Patel as Naseema
* Jamal as Showkhat Ali
* Dinesh as Ashraf
* Pandiarajan as Roshan Kumar
* Mayilsamy
* Deekshitha Manikkam as Aayisha
* Badri Narayanan as Sarangan
* Nassar in a cameo appearance

==Production== Jai and Samantha announced Raja Rani,  Naiyaandi and Vaayai Moodi Pesavum ended up releasing earlier.  Heebah Patel, who had worked only as a model in commercials, was roped in to play the other female lead.  Deekshitha Manikkam, runner up of the Miss South India 2013, was signed up to play the pivotal character of Nazriyas friend in the film. 

The film was revealed to be along the lines of the Hindi film Hum Aapke Hain Koun..! (1994) and Monsoon Wedding (2001), showcasing the rich traditions and culture of Indian weddings. 

 
 Malabar and Muharram in Hyderabad were captured for the film. The director explained that waiting till the actual ceremonies took place during the year added to the delay in shooting. 

==Controversy==
The communitys rituals were portrayed  in an objectionable manner in the Tamil movie Thirumanam Ennum Nikkah. Claiming that the muharram chest-beating procession had been portrayed in a derogatory manner in the film, a Chennai-based organization, Tamil Nadu Shia Muslim Jamath, has filed a writ petition in the Madras high court to stall the release of the film. In the petition, the Jamath vice-president Tablez Ali Khan of Thousand Lights said the chest-beating procession is mourning the martyrdom of the grandson of Muhammad, and the occasion is sentimental having religious sanctity. He added that it was a direct attack on the principles of secularism enshrined in the Constitution. 
The petitioner wanted the court to direct the complaint and forbear the producers from releasing the film. 

==Soundtrack==
{{Infobox album|  
| Name = Thirumanam Ennum Nikkah Srivatsa
| Type = Soundtrack
| Artist = M. Ghibran
| Cover  = 
| Released = 24 December 2013 Feature film soundtrack
| Length =  Tamil
| Label = Think Music
| Producer = M. Ghibran
| Reviews =
| Last album =Naiyaandi (2013)
| This album =Thirumanam Ennum Nikkah (2013)
| Next album =Vishwaroopam 2 (2014)
}}
The films score and soundtrack are composed by M. Ghibran. The soundtrack was released on December 24, 2013, to positive reviews.  

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length =  
| lyrics_credits = yes
| title1 = Chillendra Chillendra
| lyrics1 = Kadhal Mathi & Munna Shaoukat Ali
| extra1 =  Sundar Narayana Rao, Kaushiki Chakrabarty, Munna Shaoukat Ali & M. Ghibran
| length1 = 05:37
| title2 = Enthaaraa Enthaaraa
| lyrics2 = Karthik Netha Shadab Faridi & Chinmayi
| length2 = 04:41
| title3 = Kannukkul Pothivaippen
| lyrics3 = Parvathy
| extra3 = Charulatha Mani, Sadhana Sargam, Vijay Prakash & Dr. R. Ganesh
| length3 = 04:44
| title4 = Rayile Raa
| lyrics4 = Thenmozhi Das
| extra4 =  Bonnie Chakraborty, "Isai Mazhai" Haresh, Ashwitha & Nivas
| length4 =  05:32
| title5 = Yaaro Ival
| lyrics5 = Parvathy
| extra5 =  Yazin Nizar
| length5 = 03:55
| title6 = Khwaja Ji
| lyrics6 = Nizami
| extra6 =  Arifullah Shah Khalif -e- Rifayee & group
| length6 = 02:49
}}

==Release==
Producer Ravichandran announced that the film would be released in the middle of 2014, and the theatrical trailer was released in April 2014.  

The film was scheduled to release on June 24, 2014. 

==References==
 

== External links ==
*  

 
 
 
 
 