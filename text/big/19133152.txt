Blind Date (1959 film)
{{Infobox film
| name           = Blind Date
| image_size     =
| image	         = Blind Date FilmPoster.jpeg
| caption        = A film poster bearing the U.S. title: Chance Meeting
| director       = Joseph Losey
| producer       = David Deutsch Luggi Waldleitner
| writer         = Leigh Howard (novel) Ben Barzman Millard Lampell
| starring       = Hardy Krüger Stanley Baker Micheline Presle
| music          = Richard Rodney Bennett
| cinematography = Christopher Challis
| editing        = Reginald Mills
| studio         = Independent Artists Rank (UK)
| released       = August 1959 (UK) April 29, 1960 (US)
| runtime        = 90-96 minutes
| country        = United Kingdom English
| budget         =
| gross          =
}} 1959 murder mystery film. A police inspector investigates a womans death, with her lover being the prime suspect. Ben Barzman and Millard Lampell were nominated for the BAFTA Award for Best British Screenplay.

The film was one of Stanley Bakers favourites. Howard Thompson, STANLEY BAKER: PERIPATETIC ACTOR-PRODUCER: GENESIS PROVINCIAL DEBUT, New York Times (1923-Current file)   01 Sep 1963: X5. 

==Plot==
Jan Van Rooyer, a painter, has been art teacher to Jacqueline Cousteau, an older woman who becomes his lover. But when she turns up dead, Van Rooyer must explain their relationship to Morgan, a police detective.

==Cast==
*Hardy Krüger as Jan Van Rooyer
*Stanley Baker as Inspector Morgan
*Micheline Presle as Jacqueline Cousteau
*John Van Eyssen as Inspector Westover Gordon Jackson as Sergeant
*Robert Flemyng as Sir Brian Lewis
*Jack MacGowran as Postman
*Redmond Phillips as Police Doctor
*George Roubicek as Police Constable
*Lee Montague as Sergeant Farrow

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 

 