Hit List (2012 American film)
{{Infobox film
| name           = Hit List(2012 film)
| image          =
| alt            =  
| image_size     = 180
| caption        = 
| director       = Minh Collins
| producer       =  
| writer         =  
| starring       =  
| music          = 
| cinematography = Emile Haris
| editing        = 
| studio         = 
| distributor    = United Films International (International) United King Films (Israel)
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Hit List is a 2012 dark romantic-comedy film starring Joey Lawrence and Shirly Brener.

==Plot== John Savage), she ventures into online dating. Just as she reaches her limit of unsuccessful dates, Charlotte meets her soul mate Lyle. For the first time in her life, everything is perfect...until Lyle discovers Charlottes journal. Unknown to her, Lyle is a hit man who sets out to prove just how deeply he loves her by killing everyone on her list. As people on Charlottes "hit list" start disappearing, Charlotte begins to figure out Lyles true profession and what to do about it.

==Cast==
* Joey Lawrence as Lyle Wilkes
* Shirly Brener as Charlotte Murphy
* Bryce Johnson as Chad
* Andrea Evans as Diane Murphy
* Curtis Armstrong as Mr. Button John Savage as Walter Murphy
* Michael Kostroff as Mr. Weller Chris Owen as Wick
* Loren Lester as Billy Joey Philbin
* Jenny O’Hara as Phils’s Mother

==Production==
Actor Joey Lawrence, who currently stars in Melissa & Joey sought out the role as hit man Lyle Wilkes to broaden his range of roles as an adult actor.


Minh Collins directed the film and followed his usual comedic style as depicted in previous work: pilots such as “Bottom Feeders” and “Venice Beach Sushi” and the webiseries “Couch Surfing”.

 Huntington Park, Marina del Rey, and North Hollywood in California (U.S. state)|California. In Cabo San Lucas, the director met perfect body doubles for Joey Lawrence and Shirly Brener in the hotel room next to his.

==Music==
* "For All I Care"
Performed by Laura Wiggins (as Laura Slade Wiggins) and Carrick Moore Gerety 
Written by Laura Wiggins (as Laura Slade Wiggins) and Carrick Moore Gerety 
Mixed by David Sampen and Seann Flynn

* "Closer"
Performed by T&L 
Written by Todor Kobakov and Lindy Vopnfjord 
Published by Runaway Music Canada 
Licensed courtesy of Runaway Music Canada

*"Dance Party"
Performed by T&L 
Written by Todor Kobakov and Lindy Vopnfjord 
Published by Runaway Music Canada 
Licensed courtesy of Runaway Music Canada

*"Live"
Performed by Damian Joyce 
Written by Damian Joyce 
Licensed courtesy of Damian Joyce

*"Walking On Your Side" Mofus  
Written by Mofus  
Licensed courtesy of Runaway Music Canada

*"Its All Good"
Performed by Mofus  
Written by Mofus  
Licensed courtesy of Runaway Music Canada

*"Chivalrys Dead"
Performed by The Paper Cranes  
Written by The Paper Cranes  
Published by Unfamiliar Records 
Licensed courtesy of Unfamiliar Records

*"You Will Lie"
Performed by Thomas DArcy  
Written by Thomas DArcy  
Published by Thomas DArcy Music Inc.  
Licensed courtesy of Thomas DArcy Music Inc.

*"LaBamba"
Performed by Lindy Vopnfjord 
Published by Lindy Vopnfjord 
Licensed courtesy of Lindy Vopnfjord

*"National Anthem"
Performed by National Anthem 
Written by National Anthem 
Published by National Anthem 
Licensed courtesy of National Anthem

*"Harmony to My Heartbeat"
Performed by Sally Seltmann  
Written by Sally Seltmann  
Published by Runaway Music Canada 
Licensed courtesy of Arts & Crafts Productions

==References==
* 
*  

 
 
 
 