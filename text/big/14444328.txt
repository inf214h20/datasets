Sodom and Gomorrah (1922 film)
{{Infobox film
| name           = Sodom und Gomorrha
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Curtiz (Mihaly Kertész) Sascha Kolowrat-Krakowsky
| writer         = Michael Curtiz (Mihaly Kertész) Ladislaus Vajda
| narrator       = 
| starring       = Lucy Doraine Walter Slezak Richard Berczeller
| music          = Giuseppe Becce
| cinematography = Franz Planer Gustav Ucicky
| editing        = 
| distributor    = Sascha-Film
| released       = 13 October 1922
| runtime        = 180 minutes; (restored: 98 minutes)
| country        = Austria
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Austrian film history. In the creation of the film between 3,000 and 14,000 performers, extras and crew were employed. 

==Cast==
* Richard Berczeller - Lot 
* Lucy Doraine - Mary Conway / Lea, Lots wife / Queen of Syria
* Walter Slezak - Edward Harber / Galilean goldsmith
* Victor Varconi - Priest / Angel of the Lord
* Kurt Ehrle - Harry Lighton / sculptor
* Georg Reimers - Jackson Harber
* Erika Wagner - Agatha Conway

The cast of thousands also included among the extras: Paul Askonas, Willi Forst, Béla Balázs, Hans Thimig, Franz Herterich and Julius von Szöreghy.

==Story==
Mary, a young girl exposed from her infancy to evil influences, is in love with Harry, a sculptor, but for the sake of financial gain becomes engaged to be married to the rich banker Jackson Harber, a much older man, and the former lover of her mother. Harry attempts suicide. By her abandoned behaviour, including her attempted seduction not only of Harbers adolescent son, Eduard, but also of Eduards tutor, a priest, Mary drives Harber to the verge of suicide as well. The first historical sequence shows Mary as the Queen of Syria who cruelly executes a young goldsmith and jeweller (played by the same actor as Eduard). Back in the present, Mary has arranged an assignation with both Harber and Eduard, neither knowing of the intentions of the other, at night in a summerhouse. While waiting for them she falls asleep: an Expressionist dream shows Harber and Eduard fighting over her, and Eduard killing his father. This is succeeded by the main historical sequence, the wickedness and destruction of Sodom, in which Mary now appears as Lea (Lia), Lot (Bible)|Lots wife. The dreams shock Mary into a realisation of the true nature and consequences of her behaviour, and she returns in penitence to Harry. 

==Production== producer was Sascha Kolowrat-Krakowsky, who according to contemporary film magazines  came up with the idea, while on a trip to United States to discover more about the American film industry, of making an epic film with many extras in Austria, as such films - "Intolerance (film)|Intolerance" seems to have been a particular model - were very popular at that time in the US and Kolowrat-Krakowsky had America in view as an additional potential market. For this purpose he founded the Herz Film Corporation in New York as a branch of his Austria company Sascha-Film.
 Hungarian wife Lucy Doraine played the leading role of Mary Conway. Walter Slezak played Edward, the young son of her fiancé. Among the extras, according to their own accounts, were Willi Forst, Hans Thimig, Paula Wessely and Béla Balázs.
 Italian films German historical dramas. Thousands of craftsmen, architects, decorators, sculptors, stuccoists, stage and set builders, pyrotechnicians, cameramen, hairdressers, mask makers and tailors, with assistants, labourers and extras, mostly the unemployed and juveniles, found employment for three years during the making of the film, in an Austria crippled by inflation and unemployment. Thousands of costumes, wigs, beards, sandals, standards, horse harnesses and other such things were made specially for the production, generally on site. Béla Balász referred to it as "prop madness".  Sodom und Gomorrha cost more than five times the planned budget and in later films, on the basis of such expensive experiences, expenditure on props was drastically reduced.

The outdoor shoots were made at the Laaerberg near Vienna, in the Lainzer Tiergarten, in Laxenburg, in Schönbrunn Palace|Schönbrunn and on the Steirischer Erzberg. The Laaerberg was particularly suitable for filming, as at this time it was a waste area, with a few clay pits filled with water. Just for the preliminary construction and erection of the backdrops several thousand workers were required. During filming between 300 to 500 actors were always needed, for crowd scenes as many as 3,000. In addition similar quantities of horses were required for some scenes.

At the end of the film the temple was supposed to collapse, for which pyrotechnicians were appointed to blow it up. However, there were accidents, causing injuries and deaths, which were to have legal consequences. The director was acquitted, but the chief pyrotechnician was arrested for 10 days and fined 500,000 Austro-Hungarian krone|Kronen.

==Background==
Many of those of worked on this film later became leading names in their fields. The cameraman Franz Planer made a career in Hollywood, as did the director Michael Curtiz and the actor Walter Slezak, who also emigrated a few years later. Gustav Ucicky, employed as a cameraman, later became a successful director in Germany and Austria. The set designer and builder Julius von Borsody worked for decades longer in this capacity in Austrian films. After the film was finished, Michael Curtiz and Lucy Doraine were divorced.

==Architecture==
The films architectural masterpiece, designed by three architects, was the "Temple of Sodom", which was counted as one of the worlds great film structures of the time. Under the direction of the architect Julius von Borsody his assistants Hans Rouc and Stefan Wessely worked with specialist companies such as Mautner und Rothmüller and the Österreichische Filmdienst on the monumental buildings of Sodom, Gomorrha and Syria. A noticeable feature of the architecture of the buildings was the ornament, strongly reminiscent of Jugendstil. The dream scenes featured Expressionist architecture.

==Further staff==
The production design was by Julius Borsidine and Edgar G. Ulmer. Remigius Geyling, costume designer at the  Burgtheater, was responsible for the costumes, including the design of the headgear for Lucy Doraine, who, even in the edition available today, has 11 different costumes. Arthur Gottlein was the production assistant.

==Performance== Sibelius and Verdi he used works by less well-known composers, such as the Hans Heiling overture by Heinrich Marschner, the overture from the opera  Yelva by Carl Gottlieb Reissiger and others.

==Versions== DDR and Czechoslovakia, as well as from Bologna and Hungary, so that although the whole film is not recovered, all four sequences have now been restored. The restored version has a running time of 98 minutes.

==See also==
* Michael Curtiz filmography

==Notes and references==
:parts of this article are translated from its equivalent on the German Wikipedia, retrieved on 25 November 2007
 

==Sources==
* Fritz, Walter, and Lachmann, Götz, editors, 1988: Sodom und Gomorrha — Die Legende von Sünde und Strafe. Vienna
*   (includes contemporary reviews in German)  
*    
*  

==External links==
*  
*  

==References==
* Büttner, Elisabeth, and Dewald, Christian, 1999: Michael Kertész. Filmarbeit in Österreich bzw. bei der Sascha-Filmindustrie A.-G., Wien, 1919–1926, in Elektrische Schatten. Beiträge zur österreichischen Stummfilmgeschichte (ed. Francesco Bono, Paolo Caneppele and Günter Krenn, Vienna, 1999)
* Fritz, Walter, 1997: Im Kino erlebe ich die Welt. 100 Jahre Kino und Film in Österreich. Vienna
* Gottlein, Arthur, 1976: Der österreichische Film. Ein Bilderbuch. Vienna
* Krenn, Günter: Sodom und Gomorrha 96—Die unendliche Geschichte einer Rekonstruktion, in: Österreichisches Filmarchiv Jahrbuch 1996, Vienna
* Pluch, Barbara, 1989: Der österreichische Monumentalstummfilm—Ein Beitrag zur Filmgeschichte der zwanziger Jahre. Masters thesis, University of Vienna

 

 
 
 
 
 
 
 
 
 
 
 