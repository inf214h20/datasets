Kadavu (film)
{{Infobox Film
| name           = Kadavu (The Ferry)
| image          = 
| caption        = 
| director       = M. T. Vasudevan Nair
| producer       = M. T. Vasudevan Nair
| screenplay     = M. T. Vasudevan Nair
| based on       =   Monisha Bhagya Roopa Biyon
| music          = Rajeev Taranath Venu
| editing        = B. Lenin
| studio         = Novel Films
| distributor    =  
| released       =   
| runtime        = 104 minutes
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}}
Kadavu ( ) is a 1991  . Retrieved May 30, 2014. 

==Plot==
Deserted by his mother, teenaged Raju is adopted by Beeranikka, a Muslim ferryman. Raju soon starts accompanying Beeranikka in ferrying people. One day, he meets a young girl who is returning to her native town Kozhikode after the death of her mother. The girl invites Raju to Kozhikode where she would be staying with her father and uncle. Not interested in leaving the ferrying job, Raju rejects the invitation. A few days later, he finds himself in possession of an ornament which he thinks belongs to the girl. He travels to Kozhikode to return the ornament and finds the girl after many days of search. The girl does not recognise Raju and says the ornament does not belong to her. Disheartened, Raju returns to the ferry.

==Cast== Monisha played the role of the young girl. Kunjandi, Thilakan and Bhagya Roopa, Biyon played other pivotal roles.

==Awards==
; International awards
* Special Jury Award at Singapore International Film Festival 
* Award at Tokyo International Film Festival  National Film Awards Best Screenplay Best Feature Film in Malayalam
; Kerala State Film Awards Best Film Best Screenplay Best Child Artist - Santhosh Antony

== References ==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 

 
 
 


 