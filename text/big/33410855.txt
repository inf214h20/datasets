Love Child (2011 film)
 
{{Infobox film
| name           = Love Child
| image          = 
| caption        = 
| director       = Leticia Tonos
| producer       = Leticia Tonos
| writer         = Leticia Tonos
| starring       = Julietta Rodriguez
| music          = 
| cinematography = Sonnel Velazquez
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Dominican Republic
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Cast==
* Julietta Rodriguez as Maria
* Victor Checo as Joaquin
* Andres Ramos as Justiniano
* Gastner Legerme as Polo Montifa
* Dionis Rufino as Melido
* Kalent Zaiz as Juana
* Frank Perozo as Rubi
* Héctor Sierra as Papito

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Dominican submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 

 
 