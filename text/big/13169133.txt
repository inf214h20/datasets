The Great Ecstasy of Robert Carmichael
{{Infobox film |
name           = The Great Ecstasy of Robert Carmichael |
image          = The Great Robert Carmichael film.jpg |
caption        = The Great Ecstasy of Robert Carmichael 2006 | Thomas Clay |
producer       = Joseph Lang |
writer         = Thomas Clay, Joseph Lang | Daniel Spencer, Lesley Manville, Danny Dyer  |
music          =  Elgar, Harvey, Purcell |
cinematography = Yorgos Arvanitis AFC |
editing        = |
distributor    = Boudu Film LLP |
released       =   |
runtime        = 96 minutes |
country        = United Kingdom |
language       = English |
budget         = |
gross          = |
}} Thomas Clay, Daniel Spencer in the title role, with Lesley Manville and Danny Dyer in support.

==Plot==
An introverted, socially awkward, middle-class youth, Robert Carmichael, is a talented cello player but is bored by his existence in the coastal town of Newhaven, East Sussex|Newhaven. He becomes associated with several other unsavory teenagers, and is soon tempted into the use of hard drugs like cocaine and Methylenedioxymethamphetamine|ecstasy. Robert initially does not take part in the rape of a teenage girl in a squalid flat with the gang, but later joins in another violent attack on a middle-aged couple, with the woman involved being viciously raped.

==Main cast== Daniel Spencer – Robert Carmichael
* Lesley Manville – Sarah Carmichael
* Danny Dyer – Larry Haydn
* Ryan Winsley – Joe
* Charles Mnene – Ben Michael Howe – Jonathan Abbott
* Miranda Wilson – Monica Abbott
* Grace Kemp – concert goer

==Recognition==
The film was shown at the Edinburgh Film Festival and also the Cannes Film Festival as part of the International Critics Week|Critics Week sidebar, where it was nominated for the Camera dOr award.  

==References==
 

==External links==
*   "Ultra violent and nauseating, but technically dazzling."
*  
* 

 
 
 
 
 
 
 
 