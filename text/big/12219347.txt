Aakhri Daku
{{Infobox film
| name           = Aakhri Daku
| image          =
| image_size     =
| caption        = 
| director       = Prakash Mehra
| producer       = Surendra Pal Bhalla
| writer         = 
| narrator       = 
| starring       = Vinod Khanna  Randhir Kapoor Rekha Reena Roy
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1978
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Aakhri Daku is a 1978 Hindi movie produced by Shanti Sagar and directed by Prakash Mehra.    The film stars Vinod Khanna, Randhir Kapoor, Rekha, Reena Roy, Sujit Kumar, Ranjeet, Keshto Mukherjee and Paintal (comedian)|Paintal. The music to the film is by Kalyanji Anandji.   

==Cast==
*Randhir Kapoor		
*Vinod Khanna		
*Rekha		
*Reena Roy		
*Sujit Kumar		
*Ranjeet		
*Paintal		 Agha		
*Keshto Mukherjee		
*P. Jairaj		 Mumtaz Begum 		 Dulari

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kahin Na Jiya Lage"
| Asha Bhosle
|-
| 2
| "Koi Na Koi To Sabhi Men Kami Hai"
| Kishore Kumar
|-
| 3
| "Maiya Mere Bhaiya Na Aaye Ri"
| Lata Mangeshkar
|-
| 4
| "Yaar Mere Paise Ka Diwana"
| Lata Mangeshkar
|}

==References==
 

== External links ==
*  

 
 
 
 
 

 