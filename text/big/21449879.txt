The Other Side of the Underneath
  Jane Arden Sheila Allen, Liz Danciger, Penny Slinger, Ann Lynn, and Suzanka Fraey. Other members of the Holocaust Theatre Company appear in the film. It is the only British feature film in the 1970s to be solely directed by a woman. Jane Arden herself also appears in the film. The title of the film is taken from a line in Ardens play Vagina Rex and the Gas Oven which was a huge success at the London Arts Lab in 1969.
 Separation (1967) Jack Bond.
 Jack Bond, Rude Boy, 1980), and Robert Hargreaves.

The locations for the films were primarily in and around the Welsh mining communities of Abertillery and Cwmtillery in Ebbw Fach, the Eastern and smaller of the two valleys in Ebbw Vale.  One early episode was filmed at the Newport Transporter Bridge. The extraordinary soundtrack to the film was primarily the work of the cellist Sally Minford who appears, usually actually playing the cello, in many interior and exterior scenes, and the sound editor Robert Hargreaves.

==References==
 BFI DVD BFI 2009

==External links==
*  

 
 
 
 