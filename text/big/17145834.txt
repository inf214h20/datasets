The Invisible Menace
 
{{Infobox film
| name           = The Invisible Menace
| image          = Poster of the movie The Invisible Menace.jpg
| caption        = Film poster
| director       = John Farrow Jack L. Warner
| writer         = Ralph Spencer Zink Crane Wilbur Marie Wilson
| music          = 
| cinematography = L. William OConnell
| editing        = Harold McLernon Warner Bros. Pictures
| released       =  
| runtime        = 55 minutes
| country        = United States 
| language       = English
| budget         = 
}}

The Invisible Menace is a 1938 American mystery film directed by John Farrow and starring Boris Karloff. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 2216-217 

==Cast==
* Boris Karloff - Mr. Jevries, aka Dolman Marie Wilson - Sally Wilson Pratt
* Eddie Craven - Pvt. Eddie Pratt
* Regis Toomey - Lt. Matthews
* Henry Kolker - Col. George Hackett
* Cy Kendall - Col. Bob Rogers
* Charles Trowbridge - Dr. Brooks
* Eddie Acuff - Cpl. Sanger
* Frank Faylen - Al (private of the guard)
* Phyllis Barry - Mrs. Aline Dolman
* Harland Tucker - Ted Reilly
* William Haade - Pvt. Ferris
* John Ridgely - Pvt. Innes (scenes deleted)
* Jack Mower - Sgt. Peterson
* Anderson Lawler - Pvt. Abbott (as Anderson Lawlor)
* John Harron - Pvt. Murphy

==See also==
* Boris Karloff filmography
==References==
 
==External links==
* 

 

 
 
 
 
 
 
 
 
 
 