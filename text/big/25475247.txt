A Map of the World (film)
{{Infobox film
| name           = A Map of the World
| director       = Scott Eliott Frank Marshall
| screenplay     = Peter Hedges Polly Platt
| based on       =  
| starring       = Sigourney Weaver Julianne Moore David Strathairn
| music          = Pat Metheny
| cinematography = Seamus McGarvey Craig McKay Overseas Filmgroup
| distributor    = USA Films
| released       =  September 13, 1999  (Toronto International Film Festival|TIFF)  January 21, 2000 (USA)
| runtime        = 125 minutes US
| English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} novel of the same name by Jane Hamilton. It was directed by Scott Eliott. The movie stars Sigourney Weaver, Julianne Moore, and David Strathairn. Sigourney Weaver was nominated for a Golden Globe Award for Best Actress in a Motion Picture Drama.

== Synopsis ==

Alice Goodwin (Sigourney Weaver) is a school nurse who lives with her husband Howard (David Strathairn) and two girls on a small dairy farm in Wisconsin. After the death of the daughter of her friend Theresa Collins (Julianne Moore) on Alices property, the couple watch helplessly as the community turns against them. To make matters worse, Alice finds herself fighting charges of child abuse.

==Reception== Three Kings in "being free--in being capable of taking any turn at any moment, without the need to follow tired conventions".   The film premiered at the Toronto International Film Festival.

==References==
 

==External links==
* 

 
 
 