Future Fear
{{Infobox film
| name           = Future Fear
| director       = Lewis Baumanderh
| producer       =  
| screenplay     =  
| starring       =  
| music          = Donald Quan
| cinematography = Graeme Mears
| editing        =  
| studio         = Aliceco Inc.
| distributor    = New Horizons Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
}}
Feature Fear is a 1997 American science fiction film directed by Lewis Baumander.  The film stars Jeff Wincott as Dr. John Denniel, a geneticist who has found a cure to an extraterrestrial virus that threatens to kill everyone on earth.  General Wallace, played by Stacy Keach, wants to repopulate earth with a purely Aryan race of humans.  She sends an assassin, Maria Ford, to try to stop Dr. Denniel before he is able to save mankind from the virus.

==Cast==
* Jeff Wincott as John Denniel
* Maria Ford as Anna Pontaine
* Stacy Keach as General Wallace
* Shawn Thompson as Robert
* Kristie Ropiejko as Yvette
* Michael Seater as Young Denniel
* Danielle Dasilva as Young Anna
* Robert Tinkler as Young Wallace
* Stephanie Jones as Deniels Mother
* Michael Berger as Denniels Father

==References==
 

==External links==
 
*  
*  

 
 
 
 
 

 