Dick Tracy (serial)
{{Infobox Film
| name           = Dick Tracy
| image          = DickTracy (movie poster).jpg
| image_size     = 
| caption        =  Ray Taylor
| producer       = Nat Levine J. Laurence Wickland (Associate) George Morgan Barry Shipman Winston Miller Chester Gould (comic strip)
| narrator       = 
| starring       = Ralph Byrd Kay Hughes Smiley Burnette Lee Van Atta John Picorri Robert E. Marcato Carleton Young Fred Hamilton
| music          = Harry Grey William Nobles Edgar Lyons
| editing        = Helene Turner Edward Todd William Witney
| distributor    = Republic Pictures
| released       = USA 20 February 1937 (serial) {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 20–21
 | chapter = 
 }}  USA 27 December 1937 (feature) 
 | runtime         = 15 chapters / 290 minutes (serial)  73 minutes (feature) 
| country        = United States
| language       = English
| budget          = $112,334 (negative cost: $127,640) 
}}
 Republic Serial movie serial Ray Taylor.

==Synopsis==
 sound weapon Bay Bridge in San Francisco and stealing an experimental "Speed Plane", the Spider captures Dick Tracys brother, Gordon. The Spiders minion, Dr. Moloch, performs a brain operation on Gordon Tracy to turn him evil, making him secretly part of the Spider Ring and so turning brother against brother.

==Cast==

===Starring Cast===
*Ralph Byrd as Dick Tracy
*Kay Hughes as Gwen Andrews
*Smiley Burnette as Mike McGurk
*Lee Van Atta as Junior
*Robert E. Marcato as Chief Patton
*John Picorri as Dr Moloch
*Richard Beach as Gordon Tracy (pre-operation in Chapter 1)
*Carleton Young as Gordon Tracy (post-operation in Chapter 1)
*Fred Hamilton as Steve Lockwood
*Francis X. Bushman as Clive Anderson

The above cast members appear in the opening credits in "cameo" display - sequential pictures of each actor with his/her name (and sometimes character name) superimposed at the bottom of the screen - for the first episode, followed by a listing of supporting players. Subsequent chapters simply listed the stars on one screen and the same supporting cast a second. This approach to cast display was used by Republic from its first serial through Haunted Harbor in 1944. Universal serials presented a similar approach to cast display until 1940, only in their case, the star-cameos appeared with the first 3-4 episodes, and subsequent episodes listed these names usually followed, on a scrolling cast list, by part, but not often all, of the supporting players who had been named on the episodes with the cameos. Occasionally, a new player or two might be added. Columbia only a few times adopted this approach to displaying the cast of its serials. Republic, Universal, Warner Bros. Pictures, and some independents also used star "cameos" in numbers of their b-pictures during the 1930s.

===Supporting Cast===
*John Dilson as Ellery Brewster
*Wedgwood Nowell as H. T. Clayton
*Theodore Lorch as Paterno
*Edwin Stanley as Walter Odette (The Spider/ The Lame One)
*Harrison Greene as Cloggerstein
*Herbert Weber as Tony Martino
*Buddy Roosevelt as Burke
*George DeNormand as Flynn Byron K. Foulger as Kovitch

The above cast members appear in the opening credits as simply a list of the actors names.

==Production==
Dick Tracy was budgeted at $112,334 although the final negative cost was $127,640 (a $15,306, or 13.6%, overspend).  It was the most expensive Republic serial until S O S Coast Guard was released later in the year. 

It was filmed between 30 November and 24 December 1936 under the working titles Adventures of Dick Tracy and The Spider Ring.   The serials production number was 420. 

In this serial, Dick Tracy is a G-Man (slang)|G-Man (Federal Bureau of Investigation|FBI) in San Francisco rather than a Midwestern city police detective as in the comic strip. Most of the Dick Tracy supporting cast and rogues gallery were also dropped and new, original characters used instead. Dick Tracy creator Chester Gould approved the script despite these changes.

There were three sequels to this serial. They were all permitted by an interpretation of the original contract, which allowed a "series or serial". That meant that, Dick Tracy creator, Chester Gould was only paid for the rights to produce this serial but not for any of the sequels. 

===Stunts===
*George DeNormand as Dick Tracy (doubling Ralph Byrd)
*Loren Riebe (doubling Jack Gardner)

===Special Effects===
*John T. Coyle
*The Lydecker brothers

==Release==

===Theatrical===
Dick Tracys official release date is 20 February 1937, although this is actually the date the seventh chapter was made available to film exchanges. 

A 73-minute feature film version, created by editing the serial footage together, was released on 27 December 1937. 

==Critical reception==
Cline states that the Dick Tracy serials were "unexcelled in the action field," adding that "in any listing of serials released after 1930, the four Dick Tracy adventures from Republic must stand out as classics of the suspense detective thrillers, and the models for many others to follow." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 2. In Search of Ammunition
 | page = 20
 }}   He goes on to write that Ralph Byrd "played the part   to the hilt, giving his portrayal such unbridled, exuberant enthusiasm that the resulting excitement was contagious."  Byrd become identified with the character following the release of this serial. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 5. A Cheer for the Champions (The Heroes and Heroines)
 | page = 80
 }}   The final chapter reunion between Dick and Gordon Tracy, as Gordon lies dying and his memory returns, is "one of the few moments of real emotional drama ever attempted in serials". This added to the human quality of Dick Tracy, which was present in both this serial and Chester Goulds original strip. 

==Chapter titles==
#The Spider Strikes (29 min 31s)
#The Bridge of Terror (19 min 11s)
#The Fur Pirates (20 min 25s)
#Death Rides the Sky (20 min 49s)
#Brother Against Brother (19 min 14s)
#Dangerous Waters (16 min 52s)
#The Ghost Town Mystery (20 min 11s)
#Battle in the Clouds (18 min 40s)
#The Stratosphere Adventure (18 min 00s)
#The Gold Ship (18 min 28s)
#Harbor Pursuit (16 min 35s)
#The Trail of the Spider (17 min 39s) -- Clipshow|Re-Cap Chapter
#The Fire Trap (16 min 45s)
#The Devil in White (20 min 35s)
#Brothers United (16 min 59s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 218
 }} 

Dick Tracy was the only 15-chapter serial released by Republic in 1937. 

==Cliffhangers== Bay Bridge, sonic weapon, begins to collapse on top of Dick Tracy. plane is damaged by gunfire and crashes into a rail bridge.
#The Fur Pirates: Chasing his brother and the Spider Ring by motorboat, Dick Tracy is crushed between two moored ships moving closer together.
#Death Rides the Sky: Dick Tracy transfers to an Unmanned aerial vehicle|unmanned, remote plane to save Junior but it is shot down by the Spider Ring.
#Brother Against Brother: In a roof-top chase, Gordon shoots his brother Dick, sending him into a multi-storey fall. mooring line, pulling him under the waves.
#The Ghost Town Mystery: Dick Tracy falls into a pit in a mine tunnel. Spider Ring thugs take aim.
#Battle in the Clouds: Gordon shoots down his brothers Aeroplane|plane.
#The Stratosphere Adventure: Dick Tracy is knocked unconscious aboard a burning, crashing Airship|Zeppelin.
#The Gold Ship: A steel plate from a ships hull falls on top of Dick Tracy.
#Harbour Pursuit: Dick Tracys motorboat has its control s shot out by thugs, crashing into an oncoming ship.
#The Trail of the Spider: The lights go out, the Spider mark shines on Dick Tracys forehead, several shots are fired.
#The Fire Trap:  Dick Tracy falls while escaping a burning ship. brain operation as his brother Gordon.

==References in other films==
* The cliffhanger for chapter three, a motorboat chase, is copied in the movie Indiana Jones and the Last Crusade (1989 in film|1989). Republic serial The Fighting Devil Dogs (1938 in film|1938). {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 3. The Six Faces of Adventure
 | page = 52
}} 

==References==
 

==External links==
*  
*  
*  

===Download or view online===
*  
  
*  
*  
*  
*  
*  
*  
*  
*  
 
*  
*  
*  
*  
*  
*  
*  
 
*  

 
 

 
 
 
 
 
 
 
 
 
 