Mickey's Big Broadcast
{{Infobox film
| name           = Mickeys Big Broadcast
| image          =
| caption        =
| director       = Jesse Duffy
| producer       = Larry Darmour
| writer         = Fontaine Fox
| narrator       =
| starring       = Mickey Rooney Billy Barty Jimmy Robinson Delia Bogard Marvin Stephens Douglas Fox
| music          =
| cinematography =
| editing        =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 20 minutes
| country        = United States 
| language       = English
| budget         =
}}
 Mickey McGuire series starring a young Mickey Rooney. Directed by Jesse Duffy, the two-reel short was released to theaters on June 6, 1933 by RKO Radio Pictures.

==Plot==
Mickey and the gang try to participate in a local radio contest. But with Stinky Davis and his dad up to their old tricks, the gang are left out of the contest. Instead, they decide to start their own radio show in the clubhouse.

==Notes==
An edited version of this short appeared in the feature film compilation "Mickey the Great".

==Cast==
*Mickey Rooney – Mickey McGuire
*Billy Barty – Billy McGuire Jimmy Robinson – Hambone Johnson
*Marvin Stephens – Katrink
*Delia Bogard – Tomboy Taylor Douglas Fox – Stinkie Davis

== External links ==
*  

 
 
 
 
 
 
 


 