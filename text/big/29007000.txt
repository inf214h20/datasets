Kawasaki's Rose
 
{{Infobox film
| name           = Kawasakis Rose
| image          = Kawasakis Rose.jpg
| caption        = Film poster
| director       = Jan Hřebejk
| producer       = Rudolf Biermann Tomás Hoffman
| writer         = Petr Jarchovský
| starring       = Lenka Vlasáková
| music          = 
| cinematography = Martin Sácha
| editing        = Vladimír Barák
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
}} Best Foreign Language Film at the 83rd Academy Awards, but it did not make the final shortlist.    It had already won two prizes from independent juries at the Berlinale, as well as the Golden Kingfisher and viewers prizes at the Czech festival Finale Plzen.   

The film is a study of memory, the repressive Communist era, and reconciliation. Along with Honeymoon (2013 film)|Honeymoon (Líbánky), and Innocence (2011 film)|Innocence, with this film Hrebejk presents a loose trilogy of films in which shadows from the past already came to haunt the present of its characters.

==Cast==
* Lenka Vlasáková as Lucie
* Milan Mikulcík as Ludek
* Martin Huba as Pavel
* Daniela Kolárová as Jana
* Antonin Kratochvil as Borek (as Antonín Kratochvíl)
* Anna Simonová as Bára
* Petra Hrebícková as Radka
* Ladislav Chudík as Kafka
* Ladislav Smocek as Dr. Pesek
* Vladimir Kulhavy as Chief Physician

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Czech submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 