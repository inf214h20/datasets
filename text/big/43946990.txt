Cross Belt
{{Infobox film 
| name           = Cross Belt
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       = A Ponnappan
| writer         = NN Pillai
| screenplay     = Sathyan Sharada Sharada Kaviyoor Ponnamma Adoor Bhasi
| music          = MS Baburaj
| cinematography = EN Balakrishnan
| editing        = TR Sreenivasalu
| studio         = Deepthi Films
| distributor    = Deepthi Films
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by Crossbelt Mani and produced by A Ponnappan. The film stars Sathyan (actor)|Sathyan, Sharada (actress)|Sharada, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by MS Baburaj.   

==Cast==
  Sathyan
*Sharada Sharada
*Kaviyoor Ponnamma
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*P. J. Antony
*Aranmula Ponnamma
*Bahadoor
*Kottarakkara Sreedharan Nair
*Kottayam Chellappan
*N. Govindankutty
*Paravoor Bharathan
*Ushakumari
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaalam Maari Varum || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Kaalam Maari Varum   || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Zindabaad || K. J. Yesudas, Raveendran, CO Anto || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 