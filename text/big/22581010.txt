Stricken (2010 film)
{{Infobox film
| name           = Stricken
| image          =
| alt            =  
| caption        = Theatrical release poster
| director       = Matthew Sconce
| producer       = Matthew Sconce Gary Sconce
| writer         = Matthew Sconce David Fine
| music          = James Mierkey
| cinematography = Matthew Sconce
| editing        = Matthew Sconce
| studio         = 
| distributor    = Seeking Distribution
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 1 million
| gross          = 
}}
Stricken is an American film that premiered on June 25, 2010,  It was directed and written by Matthew Sconce, the movie starred Stephanie French and David Fine of Rent, Sweet November, The Pursuit of Happiness, and many other films. It is based on the short film of the same name that won Best Sound Design at the Action On Film Festival in 2007, and its screenplay won Best Adapted Screenplay in 2008. 

== Plot ==
After her mothers death and her fathers brutal suicide, 25 year old Sarah Black fears she is losing her grip on reality. She is haunted by nightmares and terrifying visions, and she cannot shake the feeling that something evil is about to find her. When people she cares about start to die, Sarah believes she may be next. Detective Scott Aro has been investigating a string of murders for 10 years, but nothing he has seen can prepare him for what lies ahead. As hope seems lost, the two must face the evil that has been unleashed and battle to stay alive as they discover some things will not stay dead.... 

== Cast ==
* Stephanie French as Sarah Black David Fine as Detective Scott Aro
* Christina L. Tellifson - Jenny Hansen
* Fragino M. Arola - James
* Brett Gipson - Mike
* Heidi Harian - Waitress Joyce
* Ryan Kos - Martin Black
* Taigtus Woods - Funeral Goers
* Master Dave Johnson - Detective Rich
* Heather Sconce - Trinket

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 