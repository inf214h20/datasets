The Oregon Trail (1939 serial)
 
{{Infobox film
| name           = The Oregon Trail
| image          =The Oregon Trail (1939 serial).jpg
| image_size     =
| caption        =DVD cover
| director       = Ford Beebe Saul A. Goodkind
| producer       = Henry MacRae
| writer         = George H. Plympton Basil Dickey Edmond Kelso W.W. Watson
| narrator       = James Blaine Charles Stevens Jack C. Smith
| music          =
| cinematography = Jerome Ash William A. Sickner
| editing        = Joseph Gluck Louis Sackin Alvin Todd
| distributor    = Universal Pictures
| released       =   1 May 1939
| runtime        = 15 chapters (320 min)
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
}} Universal Serial movie serial starring Johnny Mack Brown.

==Plot==
Jeff Scott is sent to investigate problems with wagon trains attempting to make the journey to Oregon. Sam Morgan has sent his henchmen, under lead-henchman Bull Bragg, to stop the wagon trains in order to maintain control of the fur trade in the area.
 

==Cast==
*Johnny Mack Brown as Jeff Scott, in the last of four serials Brown made for Universal.
*Louise Stanley as Margaret Mason
*Fuzzy Knight as Deadwood Hawkins, Scotts sidekick
*Bill Cody, Jr. as Jimmie Clark
*Edward LeSaint as John Mason James Blaine as Sam Morgan, villain Charles Stevens as "Breed"
*Jack C. Smith as Bull Bragg, Sam Morgans main henchman Colonel Custer Charles Murphy as Tompkins Colin Kenny as Slade, a henchman
*Forrest Taylor as Daggett, a henchman
*Jim Toney as Idaho Ike

==Production==

===Stunts===
*Yakima Canutt (archive footage) Cliff Lyons Tom Steele

==Chapter titles==
# The Renegades Revenge
# The Flaming Forest
# The Brink of Disaster
# Thundering Doom
# The Stampede
# Indian Vengeance
# Trail of Treachery
# Redskin Revenge
# The Avalanche of Doom
# The Plunge of Peril
# Trapped in the Flames
# The Baited Trap
# Crashing Timbers
# Death in the Night
# Trails End
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 224
 | chapter = Filmography
 }} 

==References==
 

==External links==
* 
* 

 
{{Succession box Universal Serial Serial
| Buck Rogers (1939 in film|1939)
| years=The Oregon Trail (1939 in film|1939)
| after=The Phantom Creeps (1939 in film|1939)}}
 

 

 

 
 
 
 
 
 
 
 
 