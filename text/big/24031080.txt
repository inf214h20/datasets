The Peterville Diamond
{{Infobox film
| name = The Peterville Diamond
| image =
| image_size =
| caption =
| director = Walter Forde Max Milner Brock Williams
| narrator = Donald Stewart   Renee Houston   Oliver Wakefield 
| music = Jack Beaver
| cinematography = Basil Emmott 
| editing = Terence Fisher 
| studio = Warner Brothers
| distributor = Warner Brothers
| released = 1942
| runtime = 85 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} British comedy Donald Stewart and Renee Houston. It is also known by the alternative title Jewel Robbery.

==Synopsis==
A thief tries to steal a diamond from the wife of a wealthy businessman, at her suggestion in an effort to get her husband to pay her more attention.  

==Production==
The film was based on a play by Ladislas Fodor and adapted by Gordon Wellesley. It was made at Teddington Studios by the British subsidiary of Warner Brothers. The films sets were by the resident art director Norman Arnold.

==Cast==
* Anne Crawford as Teri Mortimer  Donald Stewart as Charles Mortimer 
* Renee Houston as Lady Margaret  
* Oliver Wakefield as Baron Redburn  
* Charles Heslop as Dilfallow  
* William Hartnell as Joseph
* Felix Aylmer as President 
* Charles Victor as Dan  
* Joss Ambler as Police Chief  
* Paul Sheridan as Luis 
* Jeremy Hawk as Pierre 
* Julian Somers as Andre 
* Rosamund Greenwood as Miss Geach  Billy Holland as First Detective Inspector  
* Noel Dainton as Second Detective Inspector  
* Leo de Pokorny as Receptionist

==References==
 

==Bibliography==
* Hutchings, Peter. Terence Fisher. Manchester University Press, 2001.

==External links==
 

 

 
 
 
 
 
 
 
 
 
 
 
 

 
 