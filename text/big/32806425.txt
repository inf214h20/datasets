Discarded Lovers
{{Infobox film
| name           = Discarded Lovers
| image_size     =
| image	=	Discarded Lovers FilmPoster.jpeg
| caption        =
| director       = Fred C. Newmeyer
| producer       =
| writer         = Arthur Hoerl Edward T. Lowe Jr.
| narrator       =
| starring       = See below
| music          =
| cinematography = William Hyer
| editing        = Charles J. Hunt
| distributor    =
| released       = 1932
| runtime        = 60 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Discarded Lovers is a 1932 American film directed by Fred C. Newmeyer.

==Plot summary==
 

==Cast==
*Natalie Moorhead as Irma Gladden
*Russell Hopton as Bob Adair
*J. Farrell MacDonald as Chief Sommers
*Barbara Weeks as Valerie Christine
*Jason Robards Sr. as Rex Forsythe
*Roy DArcy as Andre Leighton
*Sharon Lynn as Mrs. Sibley
*Fred Kelsey as Sgt. Delaney
*Robert Frazer as Warren Sibley
*Jack Trent as Ralph Norman, Chauffeur
*Allen Dailey as Robert Worth

==Soundtrack==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 