Autoluminescent
{{Infobox film
| name           = Autoluminescent
| image          = 
| alt            = 
| caption        = 
| director       = Lynn-Maree Milburn Richard Lowenstein
| producer       = Richard Lowenstein Andrew de Groot, Lynn-Maree Milburn, Genevieve McGuckin, Rowland S. Howard
| writer         = Lynn-Maree Milburn
| starring       = Rowland S. Howard Nick Cave Mick Harvey Wim Wenders Lydia Lunch Adalita The Birthday Party These Immortal Souls Crime and the City Solution
| cinematography = Andrew de Groot
| editing        = Richard Lowenstein Lynn-Maree Milburn
| studio         = Ghost Pictures
| distributor    = Umbrella Entertainment (ANZ), Odins Eye Entertainment (ROW Sales Agent)
| released       =  
| runtime        = 110 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}
Autoluminescent is a 2011 documentary film about musician Rowland S. Howard directed by Lynn-Maree Milburn and Richard Lowenstein.   

== Festivals and awards ==
Autoluminescent won an ATOM Award in 2012 in the Best Documentary Biography section.    The world premiere of Autoluminescent was at the Melbourne International Film Festival in August 2011.    Autoluminescent premiered internationally at the Santa Barbara International Film Festival in 2012.   

==References==
 

== External links ==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 

 