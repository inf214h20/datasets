Dead Air (2009 film)
{{Infobox Film
| name           = Dead Air
| image          = Dead air poster.jpg
| caption        = Theatrical release poster
| director       = Corbin Bernsen
| producer       = {{plainlist|
* Chris Aronoff
* Corbin Bernsen
* Jesse Lawler
}} 
| writer         = Kenny Yakkel
| starring       = {{plainlist|
* Bill Moseley
* Patricia Tallman
* David Moscow
* Navid Negahban
* Anthony Ray Parker
}}
| music          =  {{plainlist|
* Brandon McCormick
*  Mike Post
}}
| cinematography = Eric G. Petersen
| editing        = David Yeaman
| distributor    = {{plainlist|
* Antibody Films
* Public Media Works
}}
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}}
Dead Air is a 2009 science fiction horror film directed by Corbin Bernsen.  Dead Air focuses on a radio station that warns its listeners after an explosion unleashes "zombies" into Los Angeles.  Writer Kenny Yakkel explains that the zombies are not actual zombies: "Its like a PCP zombie movie, thats my take on it cause theyre not really dead." 

==Plot==
A plague in the form of a toxic viral gas is unleashed at major sporting events across the United States. The gas turns its victims instantly "into an immediate rage of insanity and violence".  Controversial Los Angeles talk show radio host Logan Burnhardt and his production team are caught up in the middle of the chaos. Only blocks away from the explosion site, they begin to receive reports of rioters in the streets and listeners continue to call in reports of their first hand experiences. In addition to those infected by the virus, the terrorists, led by Abir, responsible for the attacks attempt to make their way to Logans studio and kill anyone in the way.

==Cast==
* Bill Moseley as Logan Burnhardt
* Patricia Tallman as Lucy
* David Moscow as Gil
* Navid Negahban as Abir
* Corbin Bernsen as Dr. F
* Josh Feinman as Burt
* Anthony Ray Parker as Tanner
* Haley Pullos as Dee Dee
* Nicholas Guilak as Kalil
* Herzl Tobey as Majeed
* Lakshmi Manchu as Gabbi

==Production== Talk Radio, "but this is less about the flesheating-zombie thing and more about the paranoia following 9/11."  It  brings together Bill Moseley and Patricia Tallman as Lucy, Moseleys characters ex-wife; the two had previously worked together on Tom Savinis remake of Night of the Living Dead. The film also marks Tallmans return to the genre, which the actress says is her first work of substance since the end of her run on TVs Babylon 5.   The budget was under $500,000.   

==Release==
Though originally slated for a Winter 2007 theatrical release, Dead Air, was released on October 27, 2009, on DVD. 

==Reception==
Paul Mount of Starburst (magazine)|Starburst rated it 6/10 stars and called it "an enjoyably hokey experience" that overcomes its derivativeness.   Gareth Jones of Dread Central rated it 2/5 stars and wrote that the scenes "lack any kind of suspense or sense of threat".   Dustin Hall of Brutal as Hell wrote the film "isn’t particularly scary" but "gives much to ponder".   Writing in The Zombie Movie Encyclopedia, Volume 2, academic Peter Dendle said that although the film is often compared to Pontypool (film)|Pontypool, it stands on its own and has a different message: political and social versus the more philosophical and linguistic themes of Pontypool. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 