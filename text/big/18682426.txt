A Place to Grow
 
{{Infobox Film

 | name = A Place to Grow
 | caption = 
 | director = Merlin Miller
 | producer = Merlin Miller
 | writer = Merlin Miller Sandy Dillbeck Woody P. Snow
 | narrator =  John Beck Nikki Dunaway Wilford Brimley Boxcar Willie
 | music = Gary Morris Nick Sibley
 | cinematography = Peter Wolf
 | editing = Roger Jared
 | distributor = MTI Home Video
 | released = United States: June 13, 1998 (DVD)
 | runtime = 96 minutes (TV) 118 minutes (DVD)
 | country = United States English
 | budget = 
 | gross = 
 | preceded_by = 
 | followed_by = 
 | 
}} French TV television drama Merlin (Merle) Miller.  

==Plot==
Upon his brothers death, a recording artist returns to his hometown to sell the family farm.

==Synopsis==
After moving back home to the farm after his brothers death, a musician and his family begin to suspect that the brothers death may not have been accidental, and that a local businessman may be involved. 

==Cast==
* Gary Morris as Matt Walker
* Tracy Kristofferson as Cheryl Shuler John Beck as Paul Shuler
* Nikki Dunaway as Laura Shuler
* Wilford Brimley as Jake
* Boxcar Willie as Carl Betz
* Woody P. Snow as Bill Carlson
* Ed Mosher as Pastor at the cemetery
* David C. Henry as behind the scenes camraman
* David C. Henry Sr. as a walkby in the livestock show scene
* Max Lawmaster as a person in the restaurant scene
* Ed Marshall as Dan
* Sandy Lowe as Linda
* Marilyn Harper as Peg
* Juice Newton as Centennial Singer
* Steve Wariner as Centennial Singer
* John Hornsby as Centennial Singer
* R.J. Burns as Scott Walker
* Michelle Tennis as Michelle

==Soundtracks==
  Steve Wariner Music (Broadcast Music Incorporated|BMI)
* "Where Were You?", written by Gary Morris and Jeff Rea, performed by Gary Morris and Juice Newton, courtesy of Logrhythm Music (BMI)
* "Big Ole Black Guitar", written by Chuck Glass, Jim Glass, and Mike Lamb, performed by John Hornsby, courtesy of Logrhythm Music (BMI)
* "Empty", written by Gary Morris and Jeff Rea, performed by Gary Morris, courtesy of Logrhythm Music (BMI)
* "Symptoms of Love", written by Jon McElroy and Craig Karp, performed by Juice Newton, courtesy of Logrhythm Music (BMI)
* "A Month of Blue Mondays", written by Craig Karp and Dave Gibson, performed by Steve Wariner, courtesy of Logrhythm Music (BMI)
* "The Land", written by Jeff Rea and Jon McElroy, performed by Marty Raybon, courtesy of Logrhythm Music (BMI)
* "Lauras Song", written by Dottie Moore and Jeff Rea, performed by Gary Morris, courtesy of Logrhythm Music (BMI)
* "Amazing Grace", arranged and performed by Gary Morris Stan Munsey, Jr., performed by Gary Morris, courtesy of Logrhythm Music (BMI) and Royalhaven Music, Inc. (BMI) Matt King, courtesy of Logrhythm Music (BMI) and G.I.D. Music, Inc. (ASCAP) Jeff Black, performed by Lisa Brokop, courtesy of Warner-Tamberlane Publishing Corp. Jamie OHara, Sony Songs, Inc./Eiffel Tower Music (BMI)
* "For Your Love", written by Joe Ely, performed by Chris LeDoux, courtesy of Sony Songs, Inc./Eiffel Tower Music (BMI)

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 