Ravimama
{{Infobox film
| name = Ravimama
| image =
| caption = 
| director = S. Narayan
| writer = P. Vasu
| based on = En Thangachi Padichava (1988)   Aaj Ka Arjun (1990)
| producer = S. K. Raju
| starring = V. Ravichandran   Nagma   Doddanna   Hema Panchamukhi
| music = Chaitanya
| cinematography = G. V. S. Seetharam
| editing = P. R. Soundarraj
| studio  = Sri Rajarajeshwari Creations
| released =  
| runtime = 141 minutes Kannada
| country = India
}}
 Hindi blockbuster Tamil film En Thangachi Padichava.

The film released on 19 March 1999 across Karnataka cinema halls and was received well while soundtrack and score by Chaitanya (L. N. Shastry) was received well.

== Cast ==
* V. Ravichandran
* Nagma 
* Hema Panchamukhi
* Lokesh
* Doddanna
* Vijay Kashi
* Thiagarajan
* Jyothi
* Venu

== Soundtrack ==
All the songs are composed by Chaitanya and written by S. Narayan. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|-
| 1 || "Aa Kiranagalige" || L. N. Shastry, K. S. Chithra || S. Narayan
|-
| 2 || "Muddu Muddu" || L. N. Shastry, Sujatha Dutt || S. Narayan
|-
| 3 || "Priya Priya" || Rajesh Krishnan, K. S. Chithra || S. Narayan
|-
| 4 || "Aa Aaa Kaliyabeku" || S. P. Balasubrahmanyam, Suma Shastry || S. Narayan
|-
| 5 || "Nannase Mallige"  || S. P. Balasubrahmanyam ||S. Narayan
|-
| 6 || "Aleyo Ale" || S. P. Balasubrahmanyam, K. S. Chithra || S. Narayan
|-
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 


 

 