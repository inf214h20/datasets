Black Seed (film)
 
{{Infobox film
| name           = Black Seed
| image          = 
| caption        = 
| director       = Kiril Cenevski
| producer       = 
| writer         = Tasko Georgievski Kiril Cenevski
| starring       = Darko Damevski
| music          = 
| cinematography = Ljube Petkovski
| editing        = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Macedonia
| language       = Macedonian
| budget         = 
}}
 Best Foreign Language Film at the 44th Academy Awards, but was not accepted as a nominee.  It was also entered into the 7th Moscow International Film Festival.   

The film recounts the story of Macedonian soldiers in the Greek army being transported to prison camps on Greek islands. Suspected of being communists, they are maltreated by the Greek officers running the camp.

==Cast==
* Darko Damevski as Andon Sovicanov
* Aco Jovanovski as Hristos
* Risto Siskov as Paris
* Pavle Vujisic as Maki
* Voja Miric as Major
* Mite Grozdanov as Marko
* Nenad Milosavljevic as Niko

==See also==
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Yugoslav submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 