Le Boucher
{{Infobox film
| name           = Le Boucher
| image          = Leboucher.jpg
| image_size     =
| caption        = French film poster
| director       = Claude Chabrol
| producer       = André Génovès
| writer         = Claude Chabrol
| narrator       =
| starring       = Stéphane Audran Jean Yanne
| music          = Pierre Jansen
| cinematography = Jean Rabier
| editing        = Jacques Gaillard
| distributor    = Cinerama Releasing Corporation (United States|U.S.)
| released       = February 27, 1970
| runtime        = 93 mins.
| country        = France Italy French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1970 France|French thriller film written and directed by Claude Chabrol. The film had a total of 1,148,554 admissions in France.  http://www.jpbox-office.com/fichfilm.php?id=8932 

==Plot synopsis== platonic relationship. The film examines how Hélène handles her suspicion of Popaul as a series of women in the small town fall victim to an unknown murderer.

==Themes in the film== Algeria and Indochina repeatedly. Hélène is admired in the community for her selfless dedication to children &mdash; she forgoes a personal life for servitude. Chabrol hints that these characters are not as they seem. Repression and representation seem to be themes Chabrol works with in the film. In confronting repression and representation as Chabrol does, a character, who seems as altruistic as Hélène, takes on a new connotation. Little does Hélène realise that she is driving Popaul to these acts, leaving him stranded with his demons and self-disgust. 

Even before he met Hélène, Popaul was tortured.  Towards the end of the film, he delivers a soliloquy that allows the viewer to sympathize with him:
 

This is a man who has seen nothing but blood, who cannot get beyond it and who is destroyed by the possibility that humankind is little more than blood.   Popaul’s rejection by Hélène is what drives him to murder and we see by the end of the film that Hélène is a flawed character. The seemingly ideal schoolteacher indirectly affects the perfect, small town. The beginning of the film frames the town enjoying the wedding. The last shots of Hélène in the village are dramatically different.  This progression from light-heart to devastation is very subtly made but then, as in many of Chabrol’s thrillers, the darker side of human nature begins to intrude gradually before making a spectacular and gripping entrance in the last twenty or so minutes of the film. 

The repression of emotion in Hélène and impulsive anger caused by rejection in Popaul is important in the film, unlike other crime films. The murders or the hope that Popaul will be caught are not central to it. This reference to mans antecedents establishes an important theme in the film: the residual atavistic impulses in 20th-century man.  With Popaul, Chabrol shows us a character who is natural in the purest meaning of the word.  Popaul wants to love and to be loved but experience has left him scarred; Helene’s rejection seems to be the thing that pushes him over the edge.  Popaul’s anger is animalistic but it’s clear it doesn’t define him in the same way it would a traditional villain in a film. By the end of the film plot themes have not been resolved. Nor is the plot resolved with an action we have been particularly guided to consider. The murderous but sympathetic butcher quite suddenly commits suicide and as a result the schoolteacher feels guilt at not having given herself to him; we share this guilt and are implicated in his suicide, because Chabrol had ultimately tipped the balance against our wanting the schoolteacher to let the butcher into her house at the crucial moment when he most needed human sustenance. 

==Cast==
*Stéphane Audran as Hélène Daville
*Jean Yanne as Paul Thomas ("Popaul")
*Antonio Passalia as Angelo
*Pascal Ferone as Père Cahrpy
*Mario Beccara as Leon Hamel
*William Guérault as Charles
*Roger Rudel as Police Inspector Grumbach

==References==
 

==External links==
* 
*  

 

 

 
 
 
 
 
 
 
 
 