Maanikya
 
 
{{Infobox film
| name       = Maanikya
| image      = Maanikya poster1.jpg
| director   = Sudeep
| producer   = N. M. Kumar Priya Sudeep Kolla Praveen
| writer     = Koratala Siva
| starring   =  
| music      = Arjun Janya
| cinematography = Shekhar Chandra
| editing    = N. M. Vishwa
| dialogue   = Ravi Srivatsa
| studio     = 
| runtime    = 165 minutess
| released   =   }}
| country    = India
| language   = Kannada
| budget     =   to   
| gross      =      +    (Satellite + dubbing rights)
}}
 Kannada Action action film Varalaxmi Sharathkumar Telugu film Kraanthiveera Sangolli Raayanna.  The satellite rights of the movie were sold for a record-breaking price as well.   

==Cast==
  Ravichandran as Adishesha
* Sudeep as Vijay
* Ramya Krishna as Lavanya Varalaxmi Sharathkumar as Sindhu
* Ranya as Manasa
* P. Ravi Shankar as Beera Ashok as Ugrappa
* Avinash Vijayakumar
* Sadhu Kokila as Veera Pratapa Simha Sharan
* Tennis Krishna as Nagappa
* Kiran Rathod in item number
* Shweta Pandit in item number
* Rishika Singh in item number
* Komal Gandhi Friend of Manasa
and others
 

==Plot==

The movie begins with a girl collecting funds for an organisation in Singapore. A goon gives her money and then chases her at night when she goes home.  Then vijay (Sudeep), enters and resolves the conflict without fighting. The girl introduces herself as Manasa (Ranya). They slowly  become friends. But one day she asks vijay to leave her and go as she cannot stand the separation from him if their relationship develops any further. He then goes back to India and changes the mind of Manasas brother and when they give a vacation he goes with him to his village. There, where everyone are conservative in nature, he changes their nature and makes them more lovable.

When Manasa expresses her love for him, he tells his flashback. It is then revealed that he was born to their rivals family. His father (V. Ravichandran) wanted to convert the people of his village. But his mother Lavanya (Ramya Krishnan)  didnt want to stay there, so she left him. Later when vijay goes there, he falls in love with Sindhu (Varalaxmi Sarathkumar).

He starts taking revenge on the Manasas family without revealing his identity. Then they decide to marry vijay with Sindhu because they both are in love with each other. During the marriage the inspector tells his father that the enmity is rekindling because his son is taking revenge on the rivals. Soon afterwards, the rivals come and start killing everyone. After the fight it is known that vijays mother dies. His father banishes him and blames her death on him. The story is now back to the present. Manasas uncle challenges vijay that if he can defeat his men then he too would follow non violence. After successfully defeating them, her uncle asks vijay that if he could defeat the rivals son (not knowing that it was vijay himself) he would marry Manasa to vijay. 

Vijay gets angry at his stubbornness and reveals his true identity. He starts fighting with Manasa"s brofher. Then all her family members come and convert him too. This was watched by his father who had just arrived there. His father welcomes him back to the family. The film ends with him reuniting with Sindhu.

==Production==

===Development===
After the success of his previous venture, Bachchan (2013 film)|Bachchan, Sudeep met with lot of offers from various southern film industries. However, he announced through his social network site that he would be teaming up with ace actor – director V. Ravichandran for his next venture.  It was reported that Sudeep would be directing Ravichandran for his next film. Later in July 2013, it was speculated that Ravichandran would be playing father to Sudeep and the film may be a remake of 2013 Telugu film Mirchi (film)|Mirchi which starred Prabhas and Satyaraj in the son – father roles respectively.  It was later confirmed that the story is tweaked and Ravichandran would be playing the father role to Sudeep and Ramya Krishnan would be his pair. 

===Title===
Sudeep profusely thanked veteran Dwarakish for suggesting the title Maanikya 

===Filming===
The filming of the film commenced from August 2013 at a Bangalore city college.  Major portions of the film is shot at Bidar(crown city of karnataka) and Hyderabad. The film unit also flew to Bangkok to film a song sequence and few action sequences. 

===Pre-release revenues===
The film did   of business before its release by the (satellite-dubbing-audio-DVD-other) rights.   

The satellite rights of the film were sold for   to Asianet Suvarna and Hindi dubbing rights for  . Meanwhile Tamil, Telugu and Malayalam dubbing rights for   each.  The audio rights have been acquired by Anand Audio for   and the DVD rights have gone for a similar amount. The remaining rights too have been secured for  . 

==Release and response==

===Release===
Maanikya has been released in nearly 250+ screens on 1 May 2014.  And also released on 15 May 2014 in Sharjah (emirate)|Sharjah, on 16 May 2014 in Dubai and Abu Dhabi.       Maanikya is the first Kannada film released in Oman and Bahrain and released on 30 May 2014.    Maanikya also released in USA on 30 May 2014   and in Perth, Australia on 17 August 2014  Maanikya is the first Kannada film screened in Indian Film Festival of Ireland and screened on 20 September 2014 

===Response===
 
Maanikya is considered as blockbuster of this year in the Kannada film Industry and also got a good response from Dubai, Sharjah (emirate)|Sharjah, Abu Dhabi people. One of sudeep fan from Japan named as Junko came to Bangalore to see this film.

==Box office==
Maanikya is the costliest film ever made in Kannada.  The budget of the film is not officially announced but said to be between   and  .  By the 21st day the film collected  .   Day Box Office Collection|publisher=boxofficecapsule.net|date=22 May 2014}}  Maanikya completed 100 days on 8 August 2014. Total collections of the film are  . 

==Soundtrack==
{{Infobox album
| Name = Maanikya
| Type = soundtrack
| Artist = Arjun Janya Kannada
| Label = Anand Audio
| Producer = 
| Length = 29:06
| Cover = Maanikya_audio_cover.jpg
| Released    =   Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}}

The soundtrack of the album was released on 29 March 2014.    

{{Track listing
| total_length   = 29:06
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Ninna Hinde
| lyrics1 	= Jayanth Kaikini Karthik
| length1       = 04:31
| title2        = Huchcha Na
| lyrics2 	= Yograj Bhat
| extra2        = Vijay Prakash
| length2       = 04:09
| title3        = Maamu Maamu Kaviraj
| extra3        = Vijay Prakash, Priya Himesh
| length3       = 03:58
| title4        = Pantara Panta
| lyrics4       = Dr. V. Nagendra Prasad
| extra4        = Maalati
| length4       = 02:02
| title5        = Jeeva Jeeva
| lyrics5       = K. Kalyan
| extra5        = Shankar Mahadevan
| length5       = 04:55
| title6        = Jeena Jeenaya
| lyrics6       = Dr. V. Nagendra Prasad Shaan
| length6       = 03:50
| title7        = Nalla Malla
| lyrics7       = Dr. V. Nagendra Prasad
| extra7        = Arjun Janya
| length7       = 02:43
| title8        = Belake Belake
| lyrics8       = Dr. V. Nagendra Prasad
| extra8        = Shankar Mahadevan
| length8       = 01:11
| title9        = Mani Mani Maanikya
| lyrics9       = Kaviraj
| extra9        = G. Vijay Babu, Shivarudra Naik (kanakapura)
| length9       = 01:43
}}

==Critical reception==
{{Album ratings
| rev1 =Times of India
| rev1Score =  
| rev2 =Bangalore Mirror
| rev2Score =  
| rev3 =One India
| rev3Score =  
| rev4 =Deccan Herald
| rev4Score =  
| rev5 =Chitraloka
| rev5Score =  
| rev6 =Indian Express
| rev6Score =  
| rev7 =Deccan Chronicle
| rev7Score =  
| rev8 = Vijaya Karnataka
| rev8Score =  
||}}

The Film opened to mostly positive reviews from the Critics.

* Bangalore mirror gave the film 3.5/5 stars saying "Its a near-perfect mix of action, drama, romance and comedy. In a line, its the story of a scion of a family to bring together two warring families. However, it is not as simple as that. The film meddles with multiple genres —from romance to action to comedy to family drama to something more. The plot of the film matches its length, the narration and the twists keep the audience glued. Credit should go to director Sudeep for packing every available slot with good actors. The film is a real ensemble. There are top-notch actors everywhere."  

* Chitraloka gave the film 3.75/5 stars and called it a complete family entertainer and further said "Maanikya comes as a cool breeze for the summer. It is the perfect holiday gift from Sudeep and team. It has all the masala elements that a commercial film demands. There are so many things in Maanikya but all of them are perfectly placed. It is a film you will love to watch in a cool big screen theatre. This is one film you should reserve a day for this week."  

* One India gave the film 3.5/5 stars and said "Maanikya, which is the first combination of Abhinaya Chakravarthy Kichcha Sudeep and Sandalwoods ace actor Crazy Star Ravichandran, has got a grand opening across the state, and will be a biggest treat for the long weekend. Sudeep looks superb in action, comedy, dance and sentimental sequence. It is Kiccha, who steals the show."  

* Times of India gave 4/5 stars and said "Armed with a good script, the director has taken care to see that the narration does not lag at any point. The story moves fast and almost all the major characters have equal share of work to do. With a long list of senior artistes, Sudeep has succeeded in giving them the right roles." 

* Deccan Herald gave the film 3/5 stars and said "Fans will surely have no complaints with Maanikya."  

* Sharadhaa from the India express called Maanikya a Refreshing Treat for Sudeeps Fans, saying "Sudeep has pulled off the film credibly as a director and actor but looks a bit tired at times probably because of the huge responsibility he undertook.  He is excellent in scenes when he shows the human frailty of his character. A role only matched by Ravichandran  as the father. With writing by Koratala Siva, a good supporting cast combined with Arjun Janya’s music, cinematography by Shekhar Chandra and Sudeep’s over all effort, the film has turned out to be a good family entertainer."  

* Shashiprasad from the Deccan Chronicle said "What happens when Crazy Star V. Ravichandran and Kichcha Sudeep come together on big screen for the first time? Entertainment at its best would be the simplest answer to it. When nobody could imagine the evergreen lover of sandalwood shifting his gear down for a fathrer role, Maanikya is a well crafted remake of the hit Telugu movie Mirchi. Crazy, Kichcha shine in yet another remake."  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 