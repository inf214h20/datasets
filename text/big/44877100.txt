Chandrodaya
{{Infobox film
| name = Chandrodaya
| image =
| caption =
| director = S. Mahendar
| writer = Mani Ratnam
| producer = Prema  Bhavana
| music = Hamsalekha
| cinematography = Vijayakumar
| editing = Narasaiah
| studio  = 
| released =  
| runtime = 140&nbsp;min
| language = Kannada
| country = India
}}
 Prema in the lead roles.    The film had a musical score by Hamsalekha.
 Mohan in the lead roles.

==Cast==
* Shivarajkumar
* Ramesh Aravind Prema
* Bhavana
* Avinash
* Sihi Kahi Chandru
* Ramesh Bhat

==Soundtrack==
All the songs are composed and written by Hamsalekha.   

{|class="wikitable"
! Sl No !! Song Title !! Singer(s)
|-
| 1 || "Hoovige Thangali" || S. P. Balasubramanyam, K. S. Chithra
|-
| 2 || "Oho Chandrama" || S. P. Balasubramanyam
|-
| 3 || "Inchara Inchara" || K. S. Chithra
|-
| 4 || "Baaro Geleya" || Rajesh Krishnan
|-
| 5 || "Bull Bulla" || Rajesh Krishnan
|-
| 6 || "Raja Naane Shivaraja"|| Shivarajkumar
|-
|}

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 


 

 