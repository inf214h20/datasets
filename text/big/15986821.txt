Her Dangerous Path
{{Infobox film
| name           = Her Dangerous Path
| image          = 
| caption        =  Roy Clements
| producer       = Hal Roach
| writer         = Frank Howard Clark
| starring       = Edna Murphy Charley Chase
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 10 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 adventure film Roy Clements.   

==Cast==
* Edna Murphy - Corinne Grant
* Charley Chase - Glen Harper (as Charles Parrott)
* Hayford Hobbs - Donald Bartlett
* William F. Moran - Dr. Philip Markham
* Scott Pembroke - Dr. Harrison (as Percy Pembroke) William Gillespie - John Dryden
* Glenn Tryon - Reporter
* Ray Myers - Clinton Hodge Colin Kenny - Stanley Fleming Eddie Baker - Jack Reynolds (as Ed Baker)
* Fred McPherson - Professor Comstock
* Frank Lackteen - Malay George
* Sam Lufkin - Sam Comstock
* Fong Wong - Oracle of the Sands

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
 
 
 
 


 