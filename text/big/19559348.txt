Det støver stadig
 
{{Infobox film
| name           =  Det støver stadig
| image          = 
| caption        = 
| director       = Poul Bang
| producer       = 
| writer         = Arvid Müller Aage Stentoft Øyvind Vennerød
| starring       = Helle Virkner
| music          = Sven Gyldmark
| cinematography = Aage Wiltrup
| editing        = Lizzi Weischenfeldt
| studio         = Saga Studios
| released       =  
| runtime        = 82 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Det støver stadig is a 1962 Danish comedy film directed by Poul Bang and starring Helle Virkner.

==Cast==
* Helle Virkner – Fru Henriksen
* Søren Elung Jensen – Hr. Henriksen
* Dirch Passer – Alf Thomsen
* Hanne Borchsenius – Frk. Monalisa Jacobsen
* Henrik Wiehe – Hr. Johansen
* Bodil Udsen – Fru Hansen
* Ove Sprogøe – Thorbjørn Hansen
* Karl Stegger – Hr. Feddersen
* Solveig Sundborg – Fru Feddersen
* Beatrice Palner – Fru Svendsen
* Henning Palner – Hr. Svendsen
* Olaf Ussing – Borgmesteren
* Gunnar Lemvigh – T. Eliassen
* Paul Hagen – Fotograf
* Judy Gringer – Model
* Gunnar Bigum – Kunde hos Feddersen
* Asbjørn Andersen – Redaktøren
* Ellen Malberg
* Bettina Heltberg – Klinikassistent
* Edith Hermansen
* Einar Reim – Doctor Krogh
* Pierre Miehe-Renard
* Bente Weischenfeldt
* Ulla Darni
* Lene Bro – Fotomodel
* Ebba Amfeldt – Dame på posthus

==External links==
* 

 
 
 
 
 
 
 


 
 