Robot Taekwon V
{{Infobox film
| name            = Robot Taekwon V
| image           = Robot Taekwon V 1976.jpg
| caption         = Poster for Robot Taekwon V (1976)
| director        = Kim Cheong-gi
| producer        = Yu Hyun-mok
| writer          = Ji Sang-Hak
| starring        =
| music           = Choi Chang-Kwon
| cinematography  = Cho Bok-Dong
| editing         = Yoon Ji-Young
| distributor     = Yoo Productions   Seoul Donghwa
| released        =  
| runtime         = 85 minutes
| country         = South Korea
| language        = Korean
| budget          =
| film name      = {{Film name
 | hangul          = 로보트 태권브이
 | hanja           =
 | rr              = Roboteu taegwon beui
 | mr              = Robotŭ taegwŏn pŭi}}
}}
Robot Taekwon V (로보트 태권 V) is a South Korean animated film directed by Kim Cheong-gi and produced by Yu Hyun-mok, the prominent director of such films as Obaltan (오발탄) (Aimless Bullet) (1960). It was released on July 24, 1976, immediately becoming a mega-smash hit in the late 1970s, and consequently inspired a string of sequels in following years. Robot Taekwon V was released in the United States in a dubbed format under the name Voltar the Invincible. Robot Taekwon V became the first Korean film to receive full digital restoration treatment in 2005.

Robot Taekwon V was heavily based on Mazinger Z, and has been accused by some as plagiarism. 

==Plot== world domination, creates an army of giant robots to kidnap world-class athletes and conquer the world. To fight off this attack, Dr. Kim creates Robot Taekwon V. Kim Hoon, the taekwon-do champion eldest son of Dr Kim, pilots Robot Taekwon V either mechanically or through his physical power by merging his taekwon-do movements with the robot.

Comic relief is provided by Kim Hoons younger buddy, elementary school student Kim Cheol. He has fashioned himself as "Tin-Can Robot Cheol" by cutting eyeholes in a tea kettle and wearing it on his head. Kim Hoons girlfriend, Yoon Yeong-hee, is a pilot and taekwon-do practitioner. She can also operate Robot Taekwon V with buttons and levers, and pilots Kim Hoon in and out of the robot. 

==Background==
The Japanese giant-robot manga and anime Mazinger Z was popular in South Korea at the time of Robot Taekwon Vs creation, and Kim Cheong-gi freely discusses the influence of Mazinger Z on his cartoon, saying he wanted to create a Korean hero for Korean children. In order to emphasize the Korean ties of the film, he had leading characters perform the traditional martial art, taekwondo, and gave the robot the ability to do taekwondo kicks. {{Cite web|url=http://www.kocca.or.kr/ctnews/eng/SITE/data/html_dir/2003/09/13/200309130001.html|title=Classic Taekwon V reanimated
|accessdate=2007-05-04|last=O|first=Youn-hee|date=September 13, 2003|work= |archiveurl=http://web.archive.org/web/20040304152938/http://www.kocca.or.kr/ctnews/eng/SITE/data/html_dir/2003/09/13/200309130001.html|archivedate=2004-03-04}} 
The sequel, "Super Taekwon V", had taken designs from Mobile Suit Gundam|Gundam and Combat Mecha Xabungle|Xabungle 

==Restoration== Hongdae and Insadong, one of the statues landed outside of the National Assembly (parliament) building. The restoration was widely released in early 2007 and set a new record for domestic animated films, attracting over 600,000 viewers in 18 days.  

==Sequels==
Robot Taekwon V has inspired a number of film and comic book sequels. The film sequels include:
* 로보트 태권V 우주작전 (Robot Taekwon V: Space Mission)(December 13, 1976)
* 로보트 태권V 수중특공대 (Robot Taekwon V: Underwater Rangers)(July 20, 1977)
* 로보트 태권V 대 황금날개의 대결 (Robot Taekwon V vs. Golden Wings Showdown)(July 26, 1978)
* 날아라! 우주전함 거북선 (July 26, 1979)
* 슈퍼 태권V (Super Taekwon V)(July 30, 1982)
* 84 태권V (84 Taekwon V)(August 3, 1984)
* 로보트 태권V 90(Robot Taekwon V 90)(July 28, 1990)

==Notes==
 

==References==
*  
*  
* {{Cite web|url=http://www.kocca.or.kr/ctnews/eng/SITE/data/html_dir/2003/09/13/200309130001.html|title=Classic Taekwon V reanimated
|accessdate=2007-05-04|last=O|first=Youn-hee|date=September 13, 2003|work= |archiveurl=http://web.archive.org/web/20040304152938/http://www.kocca.or.kr/ctnews/eng/SITE/data/html_dir/2003/09/13/200309130001.html|archivedate=2004-03-04}}
* 
*  at  
*  (in Korean)
*  at  
*  at  　(in Japanese, with images & video clip)
* 
* 

*Powell, Alex J. Korean Animation. Swindle Magazine. Issue 10 2007.  

==External links==
* 

==See also==
*List of animated feature-length films

 
 
 
 