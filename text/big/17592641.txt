The Viking Watch of the Danish Seaman
{{Infobox film
| name           = The Viking Watch of the Danish Seaman
| image          = TheVikingWatchPoster.jpg
| image_size     = 200px
| caption        = 1948 Movie Poster by Kai Rasch
| director       = Bodil Ipsen Lau Lauritzen Jr.
| producer       = Lau Lauritzen Jr.
| writer         = Grete Frische
| narrator       = 
| starring       = Poul Reichhardt Lisbeth Movin
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = ASA Film 1948
| runtime        = 105 min.
| country        = Denmark Danish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1948 Denmark|Danish Best Danish Film in 1949.

==Cast==
{| class="wikitable" |- bgcolor="#CCCCCC"
!   Actor !! Role
|- Poul Reichhardt||Seaman Richardt Hansen
|- Lisbeth Movin || Mille Andersen
|- Lau Lauritzen, Jr. || Seaman Kaj (Frische)
|- Johannes Meyer ||  Jonas Olsen
|- Pauline Murray Pauline Murray Marion
|- Seaman Kaare
|- Per Buckhøj || Captain of the Marie Grubbe
|- Preben Lerdorff Rye || First Mate of the Marie Grubbe
|- Axel Frische Captain of the Jette
|- Kjeld Jacobsen || First Mate of the Jette
|- Telegraph Operator
|- Carl Heger || Seaman Frederik	
|- Preben Neergaard Seaman Lauritz
|- Karl Jørgensen Captain of the Anne Margrete
|- Ove Sprogøe Seaman Henry
|- Preben Uglebjerg|| Cabin Boy
|- Jon Iversen Danish Ambassador
|- Aksel Stevnsborg|| German Abassador
|- Henning Windfeldt || Seaman Per
|- Poul Secher ||
|- Poul Bundgaard || Mate
|-
|}

==External links==
* 
* 
*  (In Danish)

 

 
 
 
 
 
 
 

 
 