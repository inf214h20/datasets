La Reine Margot (1954 film)
{{Infobox film
| name           = La Reine Margot
| image          = 
| caption        = 
| director       = Jean Dréville
| producer       = Lux Compagnie Cinématographique de France (1954) (France) Alexandre Dumas Abel Gance
| starring       = Jeanne Moreau Louis de Funès
| music          = Paul Misraki
| cinematography = 
| editing        = 
| distributor    = Lux
| released       = 25 November 1955
| runtime        = 93 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
}}
 the novel Alexandre Dumas. It stars Jeanne Moreau and Louis de Funès. The film is also known under the titles Queen Margot and A Woman of Evil. 

== Cast ==
* Jeanne Moreau as Margaret of Valois 
* Armando Francioli as Joseph Boniface de La Môle
* Henri Génès as Hannibal de Coconas Charles IX Henri de Navarre
* Françoise Rosay as Catherine de Medici
* Vittorio Sanipoli as Maurevel Henriette de Nevers
* Patrizia Lari as Charlotte de Sauve Henri dAnjou
* Louis de Funès (uncredited) as René Bianchi
* Jacques Eyser as Caboche duc Henri de Guise Admiral Gaspard de Coligny
* Nicole Riche as Gilonne
* Jean Temerson as aubergiste de La Belle Étoile
* Robert Moor as procureur
* Olivier Mathot as Pierre

== See also ==
* La Reine Margot (1994 film)|La Reine Margot (1994 film)

== References ==
 

== External links ==
*  
*   at the Films de France

 

 
 
 
 
 
 
 
 
 
 

 