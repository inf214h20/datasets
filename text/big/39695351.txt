Man Against Man
{{Infobox film
| name           = Man Against Man
| image          = 
| image_size     = 
| caption        = 
| director       = Harry Piel 
| producer       = 
| writer         = Robert Liebmann   Herbert Nossen 
| narrator       = 
| starring       = Harry Piel   Dary Holm   Fritz Beckmann   Hertha von Walther
| music          = 
| editing        =
| cinematography = Ewald Daub   Gotthardt Wolf
| studio         = Ring Film UFA
| released       = 14 May 1928
| runtime        = 
| country        = Germany 
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent thriller film  directed by Harry Piel and starring Piel, Dary Holm and Fritz Beckmann.

==Cast==
* Harry Piel as Harry Paulsen 
* Dary Holm as Miß Gladys Norton 
* Fritz Beckmann as Bartholomeo, Wirt der Schmugglerschenke 
* Hertha von Walther as Emita 
* Philipp Manning as Fuessli 
* Eugen Burg as Berner 
* Georg John as Zamok 
* Charly Berger as Porter  Charles Francois as Kanzow

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 
 
 
 
 
 
 


 
 