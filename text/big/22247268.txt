Lekhayude Maranam Oru Flashback
{{Infobox film
| name           = Lekhayude Maranam:Oru Flashback
| image          = Lekhayude Maranam Oru Flashback.jpg
| image_size     =
| alt            = 
| caption        = 
| director       = K. G. George
| producer       = 
| story          = K. G. George
| screenplay     = S. L. Puram Sadanandan
| narrator       = 
| starring       = Bharath Gopi,  Nalini,  Mammootty
| music          = M. B. Sreenivasan
| cinematography = 
| editing        = G.O. Sundaram	 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Lekhayude Maranam: Oru Flashback ( , meaning The death of Lekha, a flashback) is a 1983 Malayalam film by K. G. George.    The movie is loosely based on the real life incident of the death of actress Shobha who committed suicide, following a relationship with director Balu Mahendra.

==Plot==
K G George uses the cinema within cinema technique for this film. With hints to the real life incident of suicide of popular actress Shobha, Lekhayude Maranam:Oru Flashback became controversial even before its release.

A poor Malayalee family reaches Madras to try luck for their daughter Shantamma in the film industry. She slowly starts climbing the ladder of success and become a popular actress Lekha. At the peak of her career she commits suicide by hanging herself. The film recounts events that lead her to suicide, including a stint as a prostitute succumbing to the pressure from her parents for the lust of quick money and her unsuccessful love affair with a director. 

==Cast==
*Bharath Gopi -  Suresh 
*Mammootty- Prem Sagar (Film Star)  Nalini -  Lekha 
*Sharada-Geetha
*Meenakumari Shubha -  Lekhas Mother 
*John Varghese -  Lekhas Father
*Mohan Jose - Lekhas uncle
*Thodupuzha Vasanthi Ramu

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Enneyunarthiya Pularkalathil   || Selma George || ONV Kurup ||
|-
| 2 || Mookathayude souvarnam || Selma George || ONV Kurup ||
|-
| 3 || Prabhaamayi || P Jayachandran, Selma George || ONV Kurup ||
|}

== References ==
 

==External links==
*  

 
 
 
 

 