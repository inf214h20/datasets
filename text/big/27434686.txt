Safari (1956 film)
 
{{Infobox film
| name           = Safari
| image          = Safari_1956_UK_trade_ad.jpg
| caption        = British original 1956 promotional poster Terence Young 
| producer       = Irving Allen Albert R. Broccoli 
| screenplay     = Robert Buckner Anthony Veiller (story)  Earl Cameron
| music          = William Alwyn
| cinematography = John Wilcox
| editing        = Michael Gordon
| studio         = Warwick Films
| distributor    = Columbia Pictures  
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| gross = $1.4 million (US rentals)  1,764,445 admissions (France)  
}} 1956 British Terence Young Earl Cameron, it was intentionally cast to attract an American audience, by making both the hero and the lead female character Americans played by American actors.

==Plot==
When American white hunter Ken Duffield (Victor Mature) is off leading a safari, Mau Mau terrorists attack his farm; slaughtering the labourers and livestock. Duffields son Charlie and Aunt May (Estelle Brody) defend their home against the mass attack but they do not know that their houseboy Jeroge (a poor transliteration of Njoroge) (Earl Cameron) is actually a Mau Mau general.  From inside the farmhouse, Jeroge murders Aunt May with a machete and Charlie by repeatedly firing Mays rifle into the child.

When Duffield returns to his destroyed homestead, the police have obtained information about Jeroges role in the affair. Surmising that Duffield will use his hunting expertise to track down and revenge himself on the terrorists in general and Jeroge in particular, they escort him back to Nairobi and revoke his hunting licence until the situation and Duffield cools down.
 trophy fiancée Linda (Janet Leigh), arrive in Nairobi determined to hire Duffield to lead a safari so Sir Vincent can bag a legendary man-eating lion named "Hatari". Duffield knows that Hatari resides in an area that Jeroge is known to frequent and that Sir Vincent can use his influence to get his hunting licence back.

Setting off on safari with his boss boy Jerusulem (Orlando Martins) and Odongo (Juma), Sir Vincent suspects Duffield is not interested in hunting lions when he carries a Sten gun; Duffield explaining "you never know what kind of animals you may find". Sir Vincent has his suspicion confirmed when Duffield jumps out of his Land Rover to join the police in a firefight against the Mau Mau and is keen to extract information from the prisoners.
 Maasai where the audience witnesses a traditional Maasai lion hunt. But his plans face peril when a police radio report reveals that an unknown member of the safari is a Mau Mau plant, the obsessive Sir Vincent is determined to get sole credit for killing Hatari and therefore unloads Duffields rifle, Linda decides to take an excursion down a crocodile infested river in a rubber dinghy, and when another police radio report warns that 200 Mau Mau prisoners have escaped and are headed towards Duffields safari to link up with Jeroge.

==Main cast==
 
* Victor Mature as Ken Duffield 
* Janet Leigh as Linda Latham 
* John Justin as Brian Sinden 
* Roland Culver as Sir Vincent Brampton 
* Liam Redmond as Roy Shaw  Earl Cameron as Jeroge( Njoroge)
* Orlando Martins as Jerusalem  Juma as Odongo 
* Lionel Ngakane as Kakora
* Harry Quashie as OKeefe  
* Slim Harris as Renegade  
* Cy Grant as Chief Massai  
* John Wynn as Charley  
* Arthur Lovegrove as Blake  
* Estelle Brody as Aunty May
  

==Production==
Rhonda Fleming was originally announced for the female lead and the producers were hoping to get Humphrey Bogart to star. Mary Murphy, a Cinderella, to Star with Milland
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   01 Mar 1955: a4.  
 Juma repeating his role as Odongo.  In an interview in 1997, Janet Leigh recalled that the films second unit was actually attacked by the Mau Mau. 

Studio work was done at Elstree. "These Are the Facts", Kinematograph Weekly, 31 May 1956 p 14 

==Reception== Empire Cinema in London, and the reviewer for The Times was not very favourable, in a review titled "Safari": An American film about the Mau Mau:

"Surely it is neither priggish nor pompous to find something disagreeable in the idea of so horrifying an episode as the Mau Mau terrorism in Kenya serving as material for a film whose purpose is solely to entertain – another film, made with a British cast on the same subject, has a purpose above and beyond that. Again, it is not intolerably insular to take the line that it is all wrong that an American, played by an American actor, should be the hero, and shown as the only one capable of dealing with the situation. Finally, there are those squeamish enough not to feel altogether comfortable at the sight of an elephant, a lion, and a rhinoceros being shot – it is to be hoped the scenes were faked – to give the audience a vicarious thrill. -- A film that leaves an unpleasant taste in the mouth." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 