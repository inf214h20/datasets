1969 (film)
{{Infobox film
| name = 1969
| image = Nineteensixtyninefilm.jpg
| caption = Theatrical release poster
| director = Ernest Thompson
| writer = Ernest Thompson
| starring = Robert Downey, Jr. Kiefer Sutherland Bruce Dern Mariette Hartley Winona Ryder Joanna Cassidy
| producer = Bill Badalato Daniel Grodnik
| music = Michael Small
| cinematography = Jules Brenner
| editing = William M. Anderson
| distributor = Atlantic Releasing
| released =  
| runtime = 95 minutes
| country = United States English
| budget = $37 million
| gross = $5,979,011 
}}
1969 is a 1988 drama film starring Robert Downey, Jr., Kiefer Sutherland, and Winona Ryder. It was written and directed by Ernest Thompson. The original music score is composed by Michael Small. The film deals with the Vietnam War and the resulting social tensions between those who support and oppose the war in small-town America.

==Plot==
The boys arrive on Easter morning and shout their greetings across the glen to their family during a lakeside Easter Sunrise service, much to the amusement of Ralphs younger sister Beth (Winona Ryder) and mother Ev (Joanna Cassidy) and embarrassment of Scotts mother Jessie (Mariette Hartley) and father Cliff (Bruce Dern). 
 draft board office. 

Scott is now determined to avoid Ralphs fate and plans to leave town and head to Canada to avoid the draft. Beth finds Scott in his van and convinces him to allow her to join him on his trip. They admit their attraction to each other. Later, the two decide to visit Ralph in jail to tell him that they are leaving.

Scott and Beth get to the Canadian border and are about to cross but have a change of heart and head back to Maryland. When they get home, they learn of Aldens death. Scott leads a huge march downtown in the midst of the Aldens funeral, and where Ralph is released from jail and they are reunited.

==Main cast==
*Robert Downey, Jr. as Ralph Karr
*Kiefer Sutherland as Scott Denny
*Bruce Dern as Cliff Denny
*Mariette Hartley as Jessie Denny
*Winona Ryder as Beth Karr
*Joanna Cassidy as Ev Karr

==Critical reception== Bruce ] Dern, unusually laconic here, is unexpectedly moving as the character who seems most confused by changing times.  And Variety (magazine)|Variety said of it, "Affecting memories and good intentions dont always add up to good screen stories, and such is the case in 1969, one of the murkiest reflections on the Vietnam War era yet, notwithstanding good performances all around and bright packaging of Kiefer Sutherland and Robert Downey, Jr. in the leads." 

==Box Office ==
The film was a box office bomb, grossing $5,979,011 against a $37 millon budget.

==Soundtrack== Get Together", performed as a solo by Youngbloods lead singer, Jesse Colin Young.

==References==
 

==External links==
*  
*  
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 