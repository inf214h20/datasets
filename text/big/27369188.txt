Wilderness (miniseries)
{{Infobox film
| name           = Wilderness
| image          =
| image_size     =
| caption        =
| director       = Ben Bolt
| producer       = Tim Vaughan (producer) Andrew Davies (writer) Bernadette Davis (writer)
| narrator       =
| starring       = See below
| music          = Robert Lockhart
| cinematography =
| editing        = Michael Parkinson
| distributor    =
| released       =
| runtime        = 174 minutes 100 minutes (American DVD version)
| country        = UK
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Wilderness is a 1996 British mini-series directed by Ben Bolt.

== Cast ==
*Amanda Ooms as Alice White
*Owen Teale as Dan Somers
*Michael Kitchen as Luther Adams
*Gemma Jones as Jane Garth
*Johanna Benyon as Serena
*Molly Bolt as Dans daughter
*Mark Caven as Chuck
*Jim Dunk as Butcher
*David Gillespie as Maurice
*Mary Healey as Nurse
*Terence Hillyer as Carl
*Catherine Holman as Young Alice
*Brigitte Kahn as Alices mother
*Val Lehman as Vet
*Nicholas Lumley as Alices father
*Rosalind March as Eleanor
*Philip McGough as Marcus
*Liz Moscrop as Luthers receptionist
*Gerard OHare as Kevin
*Catherine Russell as Deborah
*Nina Thomas as Sarah
*Rupert Vansittart as Jeremy
*Daniel Wilson as Darren (as James Wilson)

== Soundtrack ==
 

== External links ==
* 

 
 
 


 