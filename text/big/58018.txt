Tora! Tora! Tora!
 
{{Infobox film
| name           = Tora! Tora! Tora!
| image          = ToraToraTora1970.png
| caption        = Original movie poster Toshio Masuda Kinji Fukasaku
| producer       = Elmo Williams Richard Fleischer
| screenplay     = Larry Forrester Hideo Oguni Ryuzo Kikushima   Akira Kurosawa
| based on       = Tora! Tora! Tora! by Gordon W. Prange and The Broken Seal by Ladislas Farago
| starring       = Martin Balsam Joseph Cotten Sō Yamamura Tatsuya Mihashi E. G. Marshall James Whitmore Jason Robards
| music          = Jerry Goldsmith
| cinematography = Charles F. Wheeler Shinsaku Himeda Masamichi Satoh Osamu Furuya
| editing        = James E. Newcom Pembroke J. Herring Inoue Chikaya
| distributor    = 20th Century Fox
| released       =  
| runtime        = 144 minutes
| country        = United States Japan
| language       = English/Japanese
| budget         = $25,485,000  
| gross          = $29,548,291   
}} Japanese attack on Pearl Harbor. The film was directed by Richard Fleischer and stars an ensemble cast, including Martin Balsam, Joseph Cotten, Sō Yamamura, E. G. Marshall, James Whitmore and Jason Robards.
 codeword used to indicate that complete surprise had been achieved. Tora (虎,  ) literally means "tiger", but in this case was an acronym for totsugeki raigeki　(突撃雷撃, "lightning attack"). 

==Plot== Combined Fleet Admiral Isoroku army hotheads preventive strike, believing Japans only hope is to annihilate the American Pacific fleet at the outset of hostilities.
 Purple Code, allowing the United States to intercept secret Japanese radio transmissions. Monitoring the transmissions are U.S. Army Col. Bratton (E. G. Marshall) and
U.S. Navy Lt. Commander Kramer (Wesley Addy).

Japanese commanders call on the famous Air Staff Officer Minoru Genda (Tatsuya Mihashi) to mastermind the attack. Gendas Japanese Naval Academy classmate, Mitsuo Fuchida (Takahiro Tamura), is chosen to be the leader of the attack.  At Pearl Harbor, although hampered by a late-arriving critical intelligence report about the attack fleet, Admiral Kimmel (Martin Balsam) and General Short (Jason Robards) do their best to enhance defenses. Short orders his aircraft to be concentrated in the middle of their airfields to prevent sabotage, though leaving them vulnerable to an air raid.

Diplomatic tensions increase between the U.S. and Japan as the Japanese ambassador continues negotiations to avoid war. Army General   (Edward Andrews) is informed of the increased threat, but decides not to inform Hawaii until after calling the President, although it is not clear if he takes any action at all.

Finally, at 11:30 am, Colonel Bratton convinces the Army Chief of Staff, General George Marshall (Keith Andes), that a greater threat exists, and Marshall orders that Pearl Harbor be notified of an impending attack. An American destroyer,  , spots a Japanese midget submarine trying to slip through the defensive net and enter Pearl Harbor, sinks it, and notifies the base. Although the receiving officer, Lieutenant Kaminsky (Neville Brand), takes the report of an attempted foreign incursion seriously, Captain John Earle (Richard Anderson) at Pearl Harbor demands confirmation before calling an alert. Admiral Kimmel later learns of this negligence and is furious he was not told of this foreign action immediately. Meanwhile, the two privates posted at the remote radar spot the incoming Japanese aircraft and inform the Hickham Field Information Center, but the Army Air Forces Lieutenant in charge, Kermit Tyler (Jerry Cox), dismisses the report, thinking it is a group of American B-17 bombers coming from the mainland.

The Japanese intend to break off negotiations at 1:00 pm, 30 minutes before the attack. However, the typist for the Japanese ambassador is slow, and cannot decode the 14th message fast enough. A final attempt to warn Pearl Harbor is stymied by poor atmospherics and bungling when the telegram is not marked urgent; it will be received by Pearl Harbor after the attack. The incoming Japanese fighter pilots dont even receive any anti-aircraft fire as they approach the base. As a result, the squadron leader radios in the code phrase marking that complete surprise for the attack has been achieved: "Tora! Tora! Tora!"
 Ken Taylor George Welch) race to remote Haleiwa and manage to take off to engage the attacking planes, as the Japanese have not hit the smaller airfields.
 Secretary of Japanese ambassador (Shōgo Shimada (actor)|Shōgo Shimada), helpless to explain the late ultimatum and the unprovoked sneak attack, is bluntly rebuffed by Hull.

The Japanese fleet commander, Admiral  ." 

==Cast==
The film was deliberately cast with actors who were not true box-office stars, in order to place the emphasis on the story rather than the actors who were in it. The original cast list had included many Japanese amateurs. 
Cast in credits order:   imdb. Retrieved: May 5, 2009. 
{{columns-list|2| Admiral Husband E. Kimmel, Commander-in-Chief, U.S. Pacific Fleet Admiral Isoroku Yamamoto, Commander-in-Chief, Combined Fleet Secretary of War Henry L. Stimson Commander Minoru 1st Air Fleet Colonel Rufus War Department Vice Admiral William F. Halsey, Commander, Aircraft Battle Force, U.S. Pacific Fleet
* Takahiro Tamura as Commander Mitsuo Fuchida, Commander, Air Group, Japanese aircraft carrier Akagi|Akagi Vice Admiral Chuichi Nagumo, Commander-in-Chief, 1st Air Fleet Lieutenant General Walter C. Short, Commander-in-Chief, United States Army Pacific Command|U.S. Army Forces Hawaii Lieutenant Commander Alwin D. Kramer, Cryptographer, OP-20-G Shogo Shimada Kichisaburo Nomura, Japanese Ambassador to the United States USS Nevada Prime Minister Prince Fumimaro Konoe Leon Ames Secretary of the Navy Frank Knox Minister of the Navy Captain John B. Earle, Chief of Staff, 14th Naval District Foreign Minister Yosuke Matsuoka General George George C. Marshall, Chief of Staff of the U.S. Army Rear Admiral Tamon Yamaguchi, Commander, Second Carrier Division Harold R. Stark, Chief of Naval Operations
* Bontaro Miyake as Admiral Koshiro Oikawa, Minister of the Navy
* Neville Brand as Lieutenant Harold Kaminski, Duty Officer, 14th Naval District
* Ichiro Ryuzaki as Rear Admiral Ryunosuke Kusaka, Chief of Staff, 1st Air Fleet
* Leora Dana as Mrs. Kramer Rikugun Taishō Minister of War Secretary of State Cordell Hull Major Truman H. Landon, Commanding Officer, 38th Reconnaissance Squadron
* Kazuko Ichikawa as Geisha in Kagoshima Theodore S. Director of Naval Intelligence
* Hank Jones as Davey, civilian student pilot Second Lieutenant George Welch, pilot, 47th Pursuit Squadron
* Karl Lukas as Captain Harold C. Train, Chief of Staff, Battle Force, U.S. Pacific Fleet
* June Dayton as Ray Cave, secretary, OP-20-G Lieutenant Lawrence E. Ruff, Communications Officer, USS Nevada
* Jeff Donnell as Cornelia Clark Fort, civilian flying instructor Captain Kameto "Gandhi" Kuroshima, Senior Staff Officer, Combined Fleet
* Richard Erdman as Colonel Edward F. French, Chief, War Department Signal Center
* Hiroshi Nihonyanagi as Rear Admiral Chuichi Hara, Commander, 5th Carrier Division USS Ward
* Carl Reindel as Second Lieutenant Kenneth M. Taylor, pilot, 47th Pursuit Squadron Mess Attendant USS West Virginia Rear Admiral Patrick N. L. Bellinger, Commander, Patrol Wing Two Lieutenant Commander Shigeharu Murata, Commander, 1st Torpedo Attack Unit, Akagi
* Hisao Toake as Saburo Kurusu, Japanese Special Envoy to the United States Takijiro Onishi, 11th Air Fleet (uncredited) Lord Keeper Marquis Koichi Kido (uncredited)
* Kiyoshi Atsumi as Japanese Cook #1 (uncredited)
* Harold Conway as Counselor Eugene Dooman, U.S. Embassy in Tokyo (uncredited)
* Dick Cook as Lieutenant Commander Logan C. Ramsey, Chief of Staff, Patrol Wing Two (uncredited) First Lieutenant Kermit A. Tyler, Executive Officer, 78th Pursuit Squadron and Officer in Charge, Pearl Harbor Intercept Center (uncredited)
* Mike Daneen as First Secretary Edward S. Crocker, U.S. Embassy in Tokyo (uncredited) Francis De Sales as Lieutenant Commander Arthur H. McCollum, Head, Far East Asia Section, Office of Naval Intelligence (uncredited) Gordon A. Blake, Operations Officer, Hickam Field (uncredited) Bill Edwards as Colonel Kendall J. Fielder, G-2 Intelligence Officer, U.S. Army Forces Hawaii (uncredited) Lieutenant Colonel Carroll A. Powell, Chief Signal Officer, U.S. Army Forces Hawaii (uncredited)
* Charles Gilbert as Lieutenant Colonel William H. Murphy, Air Warning Development Officer, U.S. Army Forces Hawaii (uncredited) Lieutenant Mitsuo Matsuzaki, Fuchida’s pilot, 1st Torpedo Attack Unit, Akagi (uncredited)
* Robert Karnes as Major John H. Dillon, Knoxs aide (uncredited)
* Randall Duk Kim as Tadao, Japanese messenger boy (uncredited)
* Berry Kroeger as General (uncredited)
* Akira Kume as First Secretary Katsuzo Okumura, Japanese Embassy in Washington, D.C. (uncredited)
* Dan Leegant as George Street, RCA Honolulu District Manager (uncredited)
* Ken Lynch as Rear Admiral John H. Newton, Commander, Cruisers, Scouting Force, U.S. Pacific Fleet and Commander, Task Force 12 (uncredited)
* Mitch Mitchell as Colonel Walter C. Phillips, Chief of Staff, U.S. Army Forces Hawaii (uncredited) Walter Reed as Vice Admiral William S. Pye, Commander, Battle Force, U.S. Pacific Fleet (uncredited) Commander William H. Buracker, Operations Officer, Aircraft Battle Force, U.S. Pacific Fleet (uncredited) Brigadier General Howard C. Davidson, Commander, 14th Pursuit Wing (uncredited)
* Tommy Splittgerber as Ed Klein, RCA telegraph operator (uncredited)
* G. D. Spradlin as Commander Maurice E. Curts, Communications Officer, U.S. Pacific Fleet (uncredited) Major General Frederick L. Martin, Commander, Hawaiian Air Force (uncredited)
* George Tobias as Captain on Flight Line at Hickam Field (uncredited)
* Harlan Warde as Brigadier General Leonard T. Gerow, Chief, War Plans Division, War Department (uncredited) Joseph C. Grew, U.S. Ambassador to Japan (uncredited) Ensign Edgar USS California (uncredited) Private Joseph L. Lockard, radar operator, Opana Point (uncredited)
* Bill Zuckert as Admiral James O. Richardson, Commander-in-Chief, U.S. Pacific Fleet (uncredited)
}}

==Production==
  stood in for the A6M Zero as there were no airworthy types at that time. Only Zeros from the carrier Akagi were depicted, identifiable by the single red band on the rear fuselage.]] George Welch.   ]] The Longest General Short Admiral Kimmel, though scapegoated for decades, provided adequate defensive measures for the apparent threats, including relocation of the fighter aircraft at Pearl Harbor to the middle of the base, in response to fears of sabotage from local Japanese. Despite a breakthrough in intelligence, they had received limited warning of the increasing risk of aerial attack.  Recognizing that a balanced and objective recounting was necessary, Zanuck developed an American-Japanese co-production, allowing for "a point of view from both nations."  He was helped out by his son, Richard D. Zanuck, who was chief executive at Fox during this time.
 Orriss 1984, Toshio Masuda Galbraith 2002, p. 156.  

Richard Fleischer said of Akira Kurosawas role in the project:
 
 Parish 1990, p. 411. 

Four cinematographers were involved in the main photography:  . A number of well-known cameramen also worked on the second units without credit, including Thomas Del Ruth and Rexford Metz.  The second unit doing miniature photography was directed by Ray Kellogg, while the second unit doing plane sequences was directed by Robert Enrietto.
 Robert McCall painted several scenes for various posters of the film. 
 Battle of Pearl Harbor would contain cut scenes from both films.

The carrier entering Pearl Harbor towards the end of the film was in fact the Iwo Jima-class amphibious assault ship  , returning to port. The "Japanese" aircraft carrier was the anti-submarine carrier  . The Japanese A6M Zero fighters, and somewhat longer "Kate" torpedo bombers or "Val" dive bombers were heavily modified RCAF Harvard (T-6 Texan) and BT-13 Valiant pilot training aircraft. The large fleet of Japanese aircraft was created by Lynn Garrison, a well-known aerial action coordinator, who produced a number of conversions. Garrison and Jack Canary coordinated the actual engineering work at facilities in the Los Angeles area. These aircraft still make appearances at air shows. 

In preparation for filming,   was berthed at North Island in San Diego to load all the aircraft, maintenance, and film crew prior to sailing to Hawaii. The night before filming the "Japanese" take-off scenes she sailed to a spot a few miles west of San Diego and at dawn the film crew filmed the launches of all the aircraft. Since these "Japanese" aircraft were not actual carrier based aircraft they did not have arresting gear with which to land back on the carrier, and continued on to land at North Island Naval Air Station.   sailed back to North Island and re-loaded the aircraft. She then sailed to Hawaii and the aircraft were off-loaded and used to film the attack scenes in and around Pearl Harbor. Aircraft Specialties of Mesa, AZ performed maintenance on the aircraft while in Hawaii.  

A Boeing B-17 Flying Fortress’s actual crash landing during filming, a result of a jammed landing gear, was filmed and used in the final cut. The film crew received word that one of the B-17s could not lower their starboard landing gear so they quickly set up to film the "single gear" landing. The aircraft stayed aloft to use up as much fuel as possible, which gave the film crew some time to prepare, prior to landing. After viewing the "single gear" landing footage they decided to include it in the movie. In the sequence depicting the crash, only the final crash was actual footage. For the scenes leading up to the crash they manually retracted the starboard landing gear on a functioning B-17 and filmed the scenes of its final approach. After touching down on one wheel the pilot simply applied power and took off again. In the movie, all the approach footage was of this aircraft, and then, right at the moment of touchdown, they switch to the actual crash footage. The difference in production values between the actual footage and the final approach footage is quite clear. The B-17 that actually landed with one gear up sustained only minor damage to the starboard wing and propellers and was repaired and returned to service. A total of five Boeing B-17s were obtained for filming. Other U.S. aircraft used are the Consolidated PBY Catalina and, especially, the Curtiss P-40 Warhawk (two flyable examples were used). Predominately, P-40 fighters are used to depict the U.S. defenders with a full-scale P-40 used as a template for fiberglass replicas (some with working engines and props) that were strafed and blown up during filming.  Fleischer also said a scene involving a P-40 model crashing into the middle of a line of P-40s was unintended, as it was supposed to crash at the end of the line. The stuntmen involved in the scene were actually running for their lives. #refOHara1969|OHara 1969, p. 23. 

==Historical accuracy==
 
 bridge island and its angled landing deck. The Japanese carriers had small bridge islands, and angled flight decks were not invented until after the war.  In addition, during the scene in which Admiral Halsey is watching bombing practice an aircraft carrier with the hull number 14 is shown. Admiral Halsey was on the  , not the Essex-class carrier  , which would not be commissioned until 1944. This is understandable, however, as both the Enterprise and all six of the Japanese carriers from the attack had been scrapped and sunk, respectively. Enterprise was scrapped in 1959, and four of the six, including Akagi, were sunk not six months after the attack at the Battle of Midway.
 Japanese carrier starboard side port side Orriss 1984, Robertson 1961, pp. 160–161. 

The   was an old "4-piper" destroyer commissioned in 1918; the ship used in the movie,  , which portrays the Ward looked far different from the original destroyer.  In addition, in the movie she fired two shots from her #1 turret. In reality, the Ward fired the first shot from the #1 4" un-turreted gunmount and the second shot from the #3 wing mount. 
 mast tower. The large scale model of the stern shows the two aft gun turrets with three gun barrels in each; in reality, Nevada had two heightened fore and aft turrets with two barrels each while the lower two turrets fore and aft had three barrels each. Another model of Nevada, used in the film to portray the whole ship, displays the turrets accurately. It should be noted that the reason for this anomaly is because the aft section model was used in the film to portray both USS Nevada and USS Arizona. The ships looked remarkably similar except that Arizona had four triple turrets and a slightly different stern section. Footage and photographs not used in the film show the cage mast as being built on the ground. The USS Nevada/USS Arizona stern section was shown exploding to represent the explosion that destroyed the Arizona.

The film has a Japanese  s ordnance building; in the second wave, a Japanese Zero did deliberately crash into a hillside after U.S. Navy CPO John William Finn at Naval Air Station at Kāneʻohe Bay had shot and damaged the aircraft; also during the second wave, a Japanese aircraft that was damaged crashed into the seaplane tender USS Curtiss. 

During a number of shots of the attack squadrons traversing across Oahu, a small cross can be seen on one of the mountainsides. The cross was actually erected after the attack as a memorial to the victims of the attack. 

==Reception== ninth highest-grossing Parish 1990, p. 412.  

 s Gulf Coast Wings Tora! Tora! Tora! team still fly the movies aircraft simulating the attack at airshows.]] Orriss 1984, p. 200. 
 Hardwick and review aggregate website Rotten Tomatoes,  based on 27 critical reviews. In 1994, a survey at the USS Arizona Memorial in Honolulu determined that for Americans the film was the most common source of popular knowledge about the Pearl Harbor attack. 
 The Final Countdown (1980), and Australia (2008 film)|Australia (2008) as well as the Magnum, P. I. television series episode titled "Lest We Forget" (first airdate February 12, 1981). 

==Honors==
Tora! Tora! Tora! was nominated for five Academy Awards, winning one for Visual Effects.
*Winner Best Special Effects (L.B. Abbott, A.D. Flowers) 
 Best Art Richard Day, Taizô Kawashima, Walter M. Scott, Norman Rockett, Carl Biddiscombe) Best Cinematography (Charles F. Wheeler, Osamu Furuya, Shinsaku Himeda, Masamichi Satoh) Best Film Editing (James E. Newcom, Pembroke J. Herring, Shinya Inoue as Inoue Chikaya) Best Sound (Murray Spivack, Herman Lewis). Canby, Vincent.   The New York Times, September 24, 1970. Retrieved: March 11, 2009. 

==In popular culture==
The name of the film has been borrowed – and parodied – for various media productions, including the "Torah Torah Torah" episodes of the television shows Magnum, P.I. and NYPD Blue, the band Tora! Tora! Torrance!, the Toyah Willcox live album Toyah! Toyah! Toyah!, the Depeche Mode song "Tora! Tora! Tora!" from their first album Speak & Spell, and the Tory! Tory! Tory! documentary on Thatcherism. 

==See also==
*Attack on Pearl Harbor
*Isoroku Yamamotos sleeping giant quote
*Pearl Harbor (film)
*List of historical drama films
*List of historical drama films of Asia

==References==
Notes
 

Citations
 

Bibliography
 
*  Agawa, Hiroyuki. The Reluctant Admiral: Yamamoto and the Imperial Navy. Tokyo: Kodansha International, 2000. ISBN 4-7700-2539-4. 
*  Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7. 
*  Galbraith, Stuart, IV. The Emperor and the Wolf: The Lives and Films of Akira Kurosawa and Toshiro Mifune. New York: Faber & Faber, Inc., 2002. ISBN 0-571-19982-8. 
*  Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies." The Making of the Great Aviation Films. General Aviation Series, Volume 2, 1989. 
*  Hathaway, John. "Tora! Tora! Tora!" Flying Review, Vol. 25, No. 3, July 1969. 
*  OHara, Bob. "Tora Tora Tora: A great historical flying film." Air Classics, Volume 6, No. 1, October 1969. 
*  Carnes, Mark C. "Tora! Tora! Tora!" Past Imperfect: History According to the Movies. New York: Holt, 1996. ISBN 978-0-8050-3760-9. 
*  Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorn, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X. 
*  Parish, James Robert. The Great Combat Pictures: Twentieth-Century Warfare on the Screen. Metuchen, New Jersey: The Scarecrow Press, 1990. ISBN 978-0-8108-2315-0. 
*  Gordon Prange|Prange, Gordon. "Tora! Tora! Tora!" Readers Digest, October, November, 1963. 
*  Robertson, Bruce. Aircraft Camouflage and Markings, 1907-1954. London: Harleyford Publications, 1961. ISBN 978-0-8168-6355-6. 
*  Shinsato, Douglas and Tadanori Urabe. For That One Day: The Memoirs of Mitsuo Fuchida, Commander of the Attack on Pearl Harbor. Kamuela, Hawaii: eXperience, inc., 2011. ISBN 978-0-9846745-0-3. 
* Thorsten, Marie and Geoffrey White.   The Asia-Pacific Journal: Japan Focus, December 27, 2010. 
 

==External links==
 
 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 