White Horse (film)
{{Infobox film
| name           =  White Horse
| image          = Whitehorsescreenshot.jpg
| image_size     = 200px
| caption        = Screenshot from the film
| director       = Maryann DeLeo Christophe Bisson
| producer       =
| writer         = Maryann DeLeo Christophe Bisson
| narrator       =
| starring       = Maxym Surkov
| music          =
| cinematography =
| editing        = Flavis Fontes Jeremy Stulberg
| distributor    = Downtown TV Documentaries HBO
| released       = 2008
| runtime        = 18 min
| country        = United States Russian English English
| budget         =
| gross          =
}}

White Horse is a short documentary by filmmakers Maryann DeLeo and Christophe Bisson that features a man (Maxym Surkov) returning to his Ukraine home for the first time in twenty years. Evacuated from the city of Pripyat, Ukraine in 1986 due to the Chernobyl disaster, he has not returned since then. DeLeo is the same filmmaker of the 2004 Academy Award-winning short film Chernobyl Heart. 

==Plot== exclusion zone, April 26th". Outside the door of the apartment, he remarks how he wishes he could stay forever. He throws his old ball through the door and walks out of the apartment complex. The film ends with Surkov snapping some twigs in an old courtyard and then an image of the car they traveled in leaving the exclusion zone. 

Film-makers attempted to contact Maxym Surkov, the featured interviewee, when the film debuted. They were informed that he had died from a heart attack in February 2008, shortly after the completion of the film. He is survived by his wife and one daughter. 

==Reception==
In 2008, the film was nominated for a Golden Bear at the Berlin International Film Festival.   It also went to the Viennale Film Festival in Vienna, the Lisbon International Film Festival, and showed in Paris at Cinema Du Reel.  It was shown on HBO in the USA in April 2009.  There is an interview with the directors on the HBO website.  http://www.hbo.com/docs/programs/whitehorse/index.html

==See also==
* Chernobyl Heart
*List of books about nuclear issues
*List of Chernobyl-related articles
*The Truth About Chernobyl

==References==
 

==External links==
*   at the Internet Movie Database.
*   at Allmovie.
*   at HBO.

 
 
 
 
 
 