Los Enchiladas!
{{Infobox film
| name           = Los Enchiladas!
| director       = Mitch Hedberg
| writer         = Mitch Hedberg
| producer       = Mitch Hedberg  Jana Johnson  Brian Malow
| narrator       =
| starring       = Mitch Hedberg Dave Attell Jana Johnson Brian Malow Marc Maron Todd Barry Chard Hogan Felicia Michaels Dave Mordal
| released       =  
| runtime        = 75 min
| country        = United States
}}
Los Enchiladas! is a 1999 film written, directed by and starring stand-up comedian Mitch Hedberg. The film is loosely based on his life growing up in Minnesota and his experience working in restaurants.  It parodies a chain Mexican restaurant in a suburb of St. Paul, Minnesota, on the day before Cinco de Mayo (the busiest day of the year for Mexican restaurants).  The storyline follows an ensemble cast of absurd characters that work in and visit the restaurant throughout the course of one day.

The cast is composed of many of Mitchs comedian friends and rounded out by local Minnesota actors.

Three different local restaurants were used to portray the films restaurant; one for the dining room scenes, a second for the kitchen scenes, and a third for the outside of the restaurant, which was a Chi-Chis that Mitch actually worked at many years before and was the main inspiration for the ridiculously inauthentic Los Enchiladas.
 Paramount Theatre in Anderson, Indiana on August 12, 2010. It has never been publicly released for purchase or download, although Mitchs fanbase has expressed a continuing desire for the film to become available.    

In April 2011, a unofficial workprint copy was released on various torrent & p2p sites.  This release appears to have been specifically opposed by Hedbergs widow, Lynn Shawcroft, who is working towards a wider release of the film.  

== References ==
 

==External links==
* 
 
 
 
 
 


 