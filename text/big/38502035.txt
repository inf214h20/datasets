Linsanity (film)
 
{{Infobox film
| name           = Linsanity
| image          = Linsanity-poster.jpg
| caption        = Theatrical release poster
| director       = Evan Jackson Leong
| producer       = Christopher Chen Allen Lu Patricia Sun James D. Stern Sam Kwok Brian Yang
| writer         =
| narrator       = Daniel Dae Kim 
| starring       = Jeremy Lin
| music          = The Newton Brothers
| cinematography = Evan Jackson Leong
| editing        = 
| distributor    = Ketchup Entertainment
| studio         = Arowana Films 408 Films
| released       = January 20, 2013 (2013 Sundance Film Festival|Sundance) March 14, 2013 (CAAMFest) October 4, 2013 (US release)
| runtime        = 88 minutes
| country        = United States
| language       = English Mandarin
| budget         = 
| gross          =  $298,178 
}}
Linsanity (2013) is a documentary film about the rise of Asian-American basketball player Jeremy Lin. The film was directed by Evan Jackson Leong.
 his rise to prominence in 2012 with the New York Knicks in the National Basketball Association (NBA).  It shows him overcoming discouragements and racism and achieving success through his faith and desire.    The New York Times wrote that it also offered a rare view of Christianity among Asian Americans.    Leong had filmed Lin since he was a star college basketball player at Harvard University and during his early struggles in the NBA.  The film is narrated by actor Daniel Dae Kim. 

  in 2013 in San Francisco.]]
Linsanity premiered to a sold-out screening at the 2013 Sundance Film Festival on January 20, 2013.       The Los Angeles Times wrote that it received a "rousing response, easily making it one of the most crowd-pleasing documentaries to play the festival this year."  Linsanity was the opening night film for the CAAMFest film festival in San Francisico, where it opened to a sellout on March 14.    It made its Asian premiere on March 30 in a sold-out screening at the Hong Kong International Film Festival.   The film also opened the Los Angeles Asian Pacific Film Festival on May 2. 

The films distribution in the United States was being handled by Creative Artists Agency, while Fortissimo Films obtained the international distribution rights. 
Ketchup Entertainment, LA based distribution company, picked up US distribution rights for Linsanity on July 24, 2013. Stephen Stanley of Ketchup negotiated the deal with Nick Ogiony and Dan Steinman of CAA, Gregory Schenz at Endgame Entertainment and Helen Dooley at Williams & Connolly. {{cite web|last=McNary|first=
Dave|title=Jeremy Lin’s ‘Linsanity’ Documentary Gets Distribution|date=July 24, 2013|work=Variety Magazine|url=http://variety.com/2013/film/news/jeremy-lin-linsanity-documentary-1200567327|archiveurl=http://www.webcitation.org/6J03OANHr|archivedate=August 19, 2013|deadurl=no}}  The film was shown at art houses, and was subsequently made available for download and DVD. 

==References==
 

==External links==
* 
* 

 
 
 
 