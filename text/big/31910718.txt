A Beautiful Life (2011 film)
{{Infobox film
| name           = A Beautiful Life
| image          = A Beautiful Life 2011 poster.jpg
| alt            = 
| caption        = Hong Kong poster
| film name =  
| simplified     =  
| pinyin         = Bù Zaì Ràng Ní Gū Dān
| jyutping       = Bat1 Zoi3 Jeong6 Nei2 Gu1 Daan1}}
| director       = Andrew Lau
| producer       = Andrew Lau
| writer         = Theresa Tang Liu Ye Tian Liang Anthony Wong
| music          = Chan Kwong-wing
| cinematography = Andrew Lau Lai Yiu-fai
| editing        = Azrael Chung Media Asia Films Beijing Bona Film and Cultural Communication China Film Media Asia Audio Video Distribution Basic Pictures
| distributor    = Media Asia Distributions
| released       =  
| runtime        = 124 minutes
| country        = China Hong Kong
| language       = Mandarin
| budget         = 
| gross          = $3,707,927  
}} Liu Ye, Anthony Wong.

==Plot==

A real-estate agent Li Peiru (Shu Qi) gets drunk at a karaoke bar and throws up on a lonely cop Fang Zhendong (Liu Ye). Zhendong quickly feels a connection to the flirtatious Peiru despite the fact that shes having an affair with her married boss. The affair ends with Peiru finding out that her boss has been cheating on her.

Peiru works hard to get funding for her business idea, and when she repeatedly fails, Fang sells his house and puts up the money to fund her business. The business fails before it can even get started, and she finds herself destitute.

Meanwhile, Fang has found out that an injury is causing him to slowly lose his mental faculties. To make matters worse, Fang loses his job as a police officer after using an official vehicle for personal purposes.

As she begins to recover from the loss of her business, Peiru begins to appreciate the selfless support she has received from Fang. She realizes that she has fallen in love with Zhendong, but she is unable to find him since he had moved.

One day, when she has established herself in a regular job, Fangs friend comes to see her. She was told of Fengs progressively worsening condition and heads  off to look for him.

They meet and reconnect. He insists that she move on since he can no longer support her, but will need her support. She stays and they eventually get married.

Fang is aware that his condition is worsening over time. Eventually, he is unable to do his job and gets lost on his way home from work. Upon arriving back at home, he finds out from Peiru that she is pregnant.

As Fangs condition worsens, he finds himself forgetting more every day, which makes taking care of his new child difficult and frustrating.

Fang sees a burglar coming out of a home near his, and gives chase. After a run through the back-alleys of his neighborhood, he is hit on the head with a brick and ends up in the hospital with serious head-trauma. After days of keeping constant vigil by his bedside, Peiru becomes sick and passes out just as Fangs heart stops beating. They both flash-back to earlier, better times.

She awakens to Fang at her bedside apologizing for making her worry.

They live happily ever after.

==Cast==
{| class="wikitable"
|-
! Cast
! Role
|-
| Shu Qi || Li Peiru
|- Liu Ye || Fang Zhengdong
|- Tian Liang || Fang Zhencong
|-
| Feng Danying || Xiaowan
|-
| Sa Rina || Xiaowans mother
|-
| Zhang Songwen ||
|-
| Gao Tian ||
|- Anthony Wong ||
|-
|  || Peirus mother
|}

==References==
 

==External links==
* 
* 
* 
*  at Hong Kong Cinemagic

 

 
 
 
 
 
 
 
 
 
 
 
 

 