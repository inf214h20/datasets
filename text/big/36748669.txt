Bloodmoon (1990 film)
 
 
{{Infobox film
| name           = Bloodmoon
| image          = Bloodmoon poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Alec Mills
| producer       = Graham Burke Gregory Coote David Munro Stanley OToole
| writer         = Robert Brennan
| starring       = Leon Lissek Christine Amor Ian Williams Helen Thomson Craig Cronin
| music          = Brian May
| cinematography = John Stokes
| editing        = David Halliday
| studio         = Michael Fisher Productions Village Roadshow Pictures
| distributor    = Roadshow Entertainment
| released       = 22 March 1990 (Australia)
| runtime        = 100 min.
| country        = Australia
| language       = English
| gross          = A$419,769 (Australia) 
}}
Bloodmoon is a 1990  .

==Plot==
 
A serial killer is at large in a catholic school, stalking the students and killing them.

==Production==
 
It was shot on the Gold Coast. 

==Release==
 
The film was released with a "fright break" towards the end where the audience had a chance to walk out and claim a refund. 

== Critical reception ==
  AllMovie called it a "silly Australian slasher film".  Jim Schembri wrote in Cinema Papers that:
 It is an unspeakably funny film, but in the saddest possible way. It is a film promoted as a horror film, and it is for anyone with any faith left in Australian mainstream film... For a film as awful, as cliche ridden, as derivative as unimaginative and as poorly made as Bloodmoon to be made in Australia in 1990 is a cause of national mourning. Jim Schembri, "Bloodmoon", Cinema Papers May 1999 p66-67  

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 
 