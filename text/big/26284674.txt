A Song for Tomorrow
 
 
{{Infobox film
| name           = A Song for Tomorrow
| image          = 
| image_size     = 
| caption        = 
| director       = Terence Fisher
| writer         = 
| narrator       = 
| starring       = Evelyn Maccabe
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = 7 June 1948
| runtime        = 80 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
A Song for Tomorrow is a 1948 drama film directed by Terence Fisher, in his first directed film. It stars Evelyn Maccabe and Ralph Michael. 

==Cast==
*Evelyn Maccabe as Helen Maxwell
* Christopher Lee as Auguste
*Conrad Phillips as Lieutenant Fenton
*Ralph Michael as Roger Stanton
*Shaun Noble as Derek Wardell James Hayter as Nicholas Klaussman

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 