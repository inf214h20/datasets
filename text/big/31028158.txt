Los tres García
{{Infobox film
| name           =Los tres García
| image          = 
| image size     =
| caption        =
| director       =
| producer       =
| writer         = 
| narrator       =
| starring       = Pedro Infante   Sara García   Marga López
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 1947
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1947 Mexico|Mexican film. It stars Pedro Infante, Abel Salazar and Carlos Orellana.  For the films exterior shots, the Delegation or City of Cuajimalpa, Mexico, was used, specially Parroquia San Pedro Apostol, which still provides religious services.  Pedro Infante fell in love with the town, the locals and countryside, to the point of building a large mansion just outside of Cuajimalpa. He lived there until his death in 1957. The house stood there until the 1990s; it was later demolished and Husky Injection Molding Systems Mexico was constructed on the same site. The northwest exterior wall that surrounded Pedros property still stands to this day.

==Cast==
*Pedro Infante as Luis Antonio García
*Sara García as Grandma Luisa García
*Marga López as Lupita Smith García
*Abel Salazar as José Luis García
*Víctor Manuel Mendoza as Luis Manuel García
*Carlos Orellana as Priest
*Fernando Soto as Tranquilino
*Antonio R. Frausto as Mayor Don Cosme
*Clifford Carr as Mr. John Smith

==External links==
*  

 
 
 
 


 