Ek Duuje Ke Liye
 
 
{{Infobox film
| name = Ek Duuje Ke Liye
| image = Ek Duuje ke liye.jpg
| caption = Poster
| director = K. Balachander
| writer = K. Balachander
| starring =  
| producer = L. V. Prasad
| music = Laxmikant-Pyarelal
| cinematography = B. S. Lokanath
| editing = N. R. Kittu
| distributor = 
| released = 5 June 1981
| runtime = 163 min.
| language = Hindi
| budget = 
| gross =   
}}
 Telugu movie Maro Charitra, National Film Filmfare nominations, eventually winning three.

==Plot==
The movie is about the love between a Tamil man, Vasu (Kamal Haasan), and a North Indian woman, Sapna (Rati Agnihotri), who are neighbours in Goa. They come from totally different backgrounds and can hardly speak the others language. Their parents despise each other and they have regular skirmishes. When Vasu and Sapna admit their love, there is chaos in their homes, and their parents reject the idea .

As a ploy to separate the lovers, their parents impose a condition that Vasu and Sapna should stay away from each other for a year. After such a period, if they still want to get together, they can get married. During the year there should be no contact between them whatsoever. Vasu and Sapna reluctantly agree to the condition and decide to separate.

Vasu moves to Hyderabad, India|Hyderabad, and they both initially suffer a lot tolerating the separation. Vasu then meets Sandhya (Madhavi (actress)|Madhavi), a widow who teaches him Hindi. Meanwhile, Sapnas mother brings a family friends son, Chakram (Rakesh Bedi), to Goa to distract Sapna from her devotion to Vasu, but she is not impressed. At a chance meeting in Mangalore, Chakram lies to Vasu that Sapna has agreed to marry him. Vasu gets upset and decides to marry Sandhya on the rebound. However, Sandhya comes to know of Vasus real love and goes to Goa to know the exact situation and to clear the misunderstanding between the lovers.

Vasu then returns to Goa and impresses Sapnas parents with his Hindi skills. When Vasu goes to meet Sapna he is attacked by a group of goons hired by Sandhyas brother (Raza Murad). Meanwhile, Sapna is raped by a familiar person (Sunil Thapa, who plays a librarian) at a temple and is left to die. The movie ends tragically when Vasu and Sapna commit suicide by jumping off a cliff.

==Cast==
{| class="wikitable"
|-
! Characters name
! Played by
|-
| Vasudev
| Kamal Haasan
|-
| Sapna
| Rati Agnihotri
|-
| Sandhya Madhavi
|-
| Chakram
| Rakesh Bedi
|-
| Sapnas mother
| Shubha Khote
|}

Ek Duuje Ke Liye marked the debut of two stars from South India in Hindi films: leading lady Rati Agnihotri and playback singer S. P. Balasubrahmanyam. Both received Filmfare nominations becoming top stars in Hindi films later.
 Maro Charitra (1979) was leading lady Saritha|Sarita, as her role was now played by Rati Agnihotri. Director K. Balachander, Kamal Haasan, Madhavi, and S.P. Balasubrahmanyam all repeated their artistry in the Hindi hit.

==Soundtrack==
{{Infobox album  		
| Name        = Ek Duuje Ke Liye
| Type        = soundtrack
| Artist      = Laxmikant-Pyarelal
| Cover       = EkDuujeAudioLabel.jpg
| Caption     = 
| Released    =  
| Recorded    =
| Genre       = 
| Length      =  Hindi
| Label       = 
| Producer    = 
| Reviews     = 
| Compiler    = 
| Misc        = 
}}		
 South Indian singer S. P. Balasubrahmanyam; the music directors were initially against including him, feeling that the "Madrasi" would not do justice to a Hindi composition, but Balachander cited that if the lead character played by Haasan could not speak Hindi well, then even if Balasubrahmanyam blemished the song, it would "capture the character." 

* "Tere Mere Beech Mein" – Lata Mangeshkar, S. P. Balasubrahmanyam
* "Hum Tum Dono Jab Mil Jayen" – Lata Mangeshkar, S. P. Balasubrahmanyam
* "Mere Jeevan Saathi" – S. P. Balasubrahmanyam, Anuradha Paudwal
* "Hum Bane Tum Bane" – Lata Mangeshkar, S. P. Balasubrahmanyam
* "Tere Mere Beech Mein (Sad)" – S. P. Balasubrahmanyam
* "Solah Baras Ki Bali Umar" – by Lata Mangeshkar, Anup Jalota
 sampled in the hit 2004 Britney Spears song "Toxic (song)|Toxic" as part of its Hook (music)|hook.   

==Awards and nominations== National Film Awards Best Male Playback Singer – S. P. Balasubrahmanyam
;Filmfare Awards
;;Wins Best Editing – K.R. Kitoo Best Lyricist – Anand Bakshi for the song "Tere Mere Beech" Best Screenplay – K. Balachander
;;Nominations Best Film Best Director – K. Balachander Best Actor – Kamal Haasan Best Actress – Rati Agnihotri Best Supporting Madhavi
* Best Performance Asrani
* Best Story – K. Balachander Best Music Director – Laxmikant-Pyarelal Best Lyricist – Anand Bakshi for the song "Solah Baras Ki Bali Umar" Best Male Playback Singer – S. P. Balasubrahmanyam for the song "Tere Mere Beech"

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 