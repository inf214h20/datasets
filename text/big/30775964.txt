Gokaiger Goseiger Super Sentai 199 Hero Great Battle
{{Infobox film
| name           = Gokaiger Goseiger Super Sentai 199 Hero Great Battle
| film name = {{Film name| kanji = ゴーカイジャー ゴセイジャー スーパー戦隊199ヒーロー 大決戦
| romaji = Gōkaijā Goseijā Sūpā Sentai Hyakukyūjūkyū Hīrō Daikessen}}
| image          = 199 Hero Great Battle.jpg
| alt            =  
| caption        = Official movie poster
| director       = Noboru Takemoto
| producer       = Motoi Sasaki Takaaki Utsunomiya Takahiro Omori Koichi Yada Akihiro Fukada Tsuyoshi Nakano
| screenplay     = Naruhisa Arakawa
| story          = Shotaro Ishinomori Toei Company
| narrator       = Tomokazu Seki
| starring       =  
| music          = Kosuke Yamashita Kazunori Miyake Project.R
| cinematography = Shingo Osawa Ren Sato
| studio         = TV Asahi Toei Video Toei Company Toei Agency Bandai Kinoshita Komuten
| distributor    = Toei Company
| released       =  
| runtime        = 81 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| italic title   = force
}} Super Sentai Series.  The film was directed by Noboru Takemoto and written by Naruhisa Arakawa, and primarily features the casts of Kaizoku Sentai Gokaiger and Tensou Sentai Goseiger, among the 199 total heroes from the Super Sentai series to appear.  On March 16, 2011, Toei released a statement concerning the effects the 2011 Tōhoku earthquake and tsunami have had on the filming schedule, and the films release was postponed  to June 11, 2011.  The film entered the number 2 in the Japanese box office and earned a total of  288,610,400 (approximately  3.6 million) on its first week.  A combination of all the main 35 Giant Robots, entirely done in Computer-generated imagery|CG, was planned to be included in the climactic scene of the movie,  but it was ultimately not included in the theatrical cut. The catchphrase for the movie is  . The movie takes place between episodes 16 and 17 of Gokaiger.

==Plot== Gosei Knight Akarenger and Big One and are told that all the Sentai teams are gathering. With the Sentai supporting warriors giving them safe passage, the Goseigers meet up with the other 33 Super Sentai and the teams proceed to battle the numerous Gormin and Zugormin. The battle reaches its end when Akarenger tells everyone to join their powers together so their combined powers can wipe out the armada. Having survived, the Gosei Angels learn that they and other Super Sentai have lost their powers, which have dispersed all over the universe. Alata believes they can still protect the planet without their powers as the teams go their separate ways.
 they have it, the Gokaigers demand the Goseiger Keys back. Despite attempting to settle things peacefully, Gosei Red sees no choice but to fight. The battle between the two Sentai teams ends in a stalemate with the pirates withdrawing.
 Hades God Yogoshimacritein as the three villains teleport the two groups away. Afterwards, Navi investigates the scene and finds the Gosei Knight key which Alata dropped.
 DaiDenzin toy, Daigoro Oume Ryo of Variblune made him consider suicide on the street. The heroes, however, encourage him not to lose hope as Ryo offers him a serving of his Jiaozi#Japanese version|gyōza. The three Sentai warriors, along with their various allies, witness the Black Cross King appearing in the sky as he announces that he has their powers and will soon acquire the powers of the Gokaigers and Goseigers before enacting his revenge.

Finding themselves in an office building, Marvelous and Alata find themselves needing to defeat Brajira while dealing with his Bibi Soldiers in order to return to their space. Doc, Ahim, Eri and Hyde have their trouble with Dagon and his Zobils while Joe, Luka, Agri and Moune contend with Yogoshimacritein and his Ugatz in a Feudal Japan movie set within a thirty minute timeline. Despite their differences, the Gokaigers and Goseigers find common ground with each other and manage to defeat the three resurrected villains and return to their space where are welcomed back by Navi and a restored Gosei Knight. Deciding to accept a team up with the Gosei Angels, Captain Marvelous and his crew face the Black Cross King as he has them fight the animated Ranger Keys. After a grueling battle, the Gokaigers and Super Goseigers manage to revert their numerous opponents back into Ranger Keys. Upset by the turn of events, yet taking advantage of their weakened state, the Black Cross King assumes his full size to finish the two Sentai teams off himself. However, the Ranger Keys suddenly glow and enveloped the Super Sentai groups in a bright light.
 Kanpei Kuroda, Rei Tachibana, Riki Honoo, Satoru Akashi, Chiaki Tani, Genta Umemori Saki Rōyama Nozomu Amachi Daidenzin toy.
 Gokai Silver.

==Cast==
* Captain Marvelous:  
* Joe Gibken:  
* Luka Millfy:  
* Don Dogoier:  
* Ahim de Famille:  
* Navi:  
* Alata:  
* Eri:  
* Agri:  
* Moune:  
* Hyde:  
* Gosei Knight:  
* Nozomu Amachi:   Chiaki Tani:   Genta Umemori:   Saki Rōyama:   Satoru Akashi:   Koume "Umeko" Kodou:   Ryo of the Heaven Fire Star:   Riki Honoo:   Hyper Hobby June 2011 
* Choudenshi Bioman#Biomen|Shirō Gō:    Rei Tachibana:    Kanpei Kuroda:    Daigoro Oume:   Big One/Sokichi Aorenger Voice:     Theatrical pamphlet  Tsuyoshi Kaijo:     Ninja Red, Black Knight (Hyuuga):   Toei Hero Max vol. 37  
* Gekisou Sentai Carranger#Others|Signalman:    Anubian Chief Doggie "Boss" Kruger / Deka Master:    Wolzard Fire:   
* Salaryman:   of   
* Bandsman:   of Wagaya 
* Bank Clerk:   of Wagaya 
* Warz Gill:  
* Damarasu:  
* Insarn:  
* Barizorg:  
* Brajira:  
* Yogoshimacritein:  
* Dagon:  
* Black Cross King:   
* Tensouder Voice:   Kirenger Voice, Red Falcon Zubaan Voice:  

==Theme song==
* 
**Lyrics: Shoko Fujibayashi & Naruhisa Arakawa
**Composition: Kenichiro Ōishi
**Arrangement: Project.R (Kenichiro Ōishi)
**Artist: Project.R

==References==
 

==External links==
*     Toei  

 
 

 
 
 
 