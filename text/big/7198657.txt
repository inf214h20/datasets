A Mind of Her Own
{{Infobox Film
| name           =A Mind of Her Own
| image          = 
| image_size     = 
| caption        = 
| director       = Owen Carey Jones	 	 	 
| producer       = Owen Carey Jones	 	 
| writer         = Owen Carey Jones	 	 
| narrator       = 
| starring       = Nicky Talacko
| music          = Alan Moore 
| cinematography = Simon Dennis
| editing        = Owen Carey Jones	  	
| distributor    = Carey Films
| released       = 24 March 2006  	 	
| runtime        = 93 min 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 British film released in 2006 in film|2006. Based on a true story, the film is directed, produced, and written by Owen Carey Jones and stars Nicky Talacko as Sophie and Amanda Rawnsley as Becky.

==Plot==
Sophie dreams of going to medical school but is discouraged by virtually everyone as she struggles with dyslexia. Encouraged by her friend Becky, Sophie eventually puts herself through college and graduate school and helps develop a cure for paralysis.

==Awards==
*2005 Heartland Film Festival: Crystal Heart Award, Best Feature film; Best actress (Nicky Talacko)
*2005 Monaco International Film Festival: Angel Award, Best Newcomer (Nicky Talacko); Best Supporting Actress (Amanda Rawnsley)

==See also==
*List of artistic depictions of dyslexia

==External links==
*  
*  
*  
* 

 
 
 
 
 
 
 


 