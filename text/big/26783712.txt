Oka Oori Katha
{{Infobox film
| name           = Oka Oori Katha
| image          =
| image_size     =
| caption        =
| director       = Mrinal Sen
| producer       = A. Parandhama Reddy
| writer         = Munshi Premchand
| narrator       =
| starring       = A. R. Krishna Pradeep Kumar G. Narayana Rao M. V. Vasudeva Rao Mamata Shankar
| music          = Vijay Raghava Rao
| cinematography = K. K. Mahajan
| editing        = Nangadhar Naskar
| studio         =
| distributor    =Chandrodaya Art Films
| released       = 1977
| runtime        =
| country        = India Telugu
| budget         =
}}
Oka Oori Katha (English title: The Marginal Ones;   directed by Mrinal Sen.  It is based on the story Kafan by Munshi Premchand.  The film was one of the Indian entries at the 4th Hong Kong International Film Festival, won Special Awards at the Karlovy Vary International Film Festival,
and Carthage Film Festival.  The film has received the National Film Award for Best Feature Film in Telugu for that year, and was premired at the International Film Festival of India. 

==The plot==
Venkaiah (Vasudeva Rao) and his son Kistaiah (Narayana Rao) lives in a village. Venkaiah lives in a queer world of his own. They have learnt to conquer hunger and are mentally strong. They consider the poor farmers are fools to work for the rich and suffer. Kistaiah wants to marry Nilamma (Mamata Shankar). The father does not like the marriage. Kistaiah refused and married Nilamma.

Nilamma tries to control the family. Venkaiah does not change. Kistaiah stands between them. There is bitterness in the family. In course of time Nilamma conceives. One day, they find Nilamma in acute pain. The father refuses to call a midwife and Nilamma dies. They decide to conduct funeral rites to Nilamma. They go begging around the village and gather some money and decide to spend it on drinks.

==Credits==
===Cast===
* A. R. Krishna
* Pradeep Kumar
* G. Narayana Rao	as 	Kistaiah
* M. V. Vasudeva Rao	as 	Venkaiah
* Mamata Shankar	as 	Nilamma
*Bhadra Reddy as Zamindar

===Crew===
* Director: Mrinal Sen
* Story: Munshi Premchand
* Screenplay: Mohit Chattopadhya
* Writer: Yandamoori Veerendranath
* Producer: A. Parandhama Reddy
* Original Music: Vijay Raghava Rao
* Cinematography: K. K. Mahajan
* Assistant Cameraman: Nadeem Khan
* Film Editing: Gangadhar Naskar
* Art Direction: B. Kalyan

==Awards==
* Nandi Award for Best Feature Film in 1977 from Government of Andhra Pradesh.
* National Film Award for Best Feature Film in Telugu in 1977.
* Karlovy Vary International Film Festival - Special Jury Award
* Carthage Film Festival - Special Award

==References==
 

==External links==
*  

 
 

 
 
 
 
 