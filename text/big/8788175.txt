Dracula (miniseries)
:For the 2006 film, see Bram Stokers Draculas Curse

{{Infobox Film 
| name            = Dracula
| image           = Bram_Stokers_Draculas_Curse_FilmPoster.jpeg
| caption         = DVD cover Roger Young Eric Lerner
| producer        = Paolo De Crescenzo Roger Young
| starring        = Patrick Bergin Hardy Krüger Jr. Kai Wiesinger
| music           = Harald Kloser Thomas Wanker
| cinematography  = Elemér Ragályi
| editing         = Alessandro Lucidi RaiTrade Beta Rai Fiction KirchMedia
| distributor     = Sirio Vide
| released        = 29 May 2002
| runtime         = 173 minutes
| country         = Italy / Germany English
| budget          = 
}}
 
 Roger Young. novel of the same name by Bram Stoker, though it updates the events of the novel to the present day. 

==Plot summary== American lawyer Jonathan Harker (Hardy Krüger Jr.) suddenly proposes to his girlfriend Mina (Stefania Rocca).  He wants to marry her within the week. Their friends Lucy (Muriel Baumeister), Quincy (Alessio Boni) and Arthur (Conrad Hornby) have been invited by Jonathan and have just arrived for the wedding, all without Minas awareness. Meanwhile, they meet the promoter of the party, the psychiatrist Dr. Seward (Kai Wiesinger). Later in the same night, Jonathan is called by a rich client, Tepes (Patrick Bergin), who hires him to prepare the inventory of the wealth of his uncle, the count Vladislav Tepes (Patrick Bergin), in Romania. Jonathan travels to the Carpathian Mountains in his Porsche, has an accident and finally arrives in the counts old castle.

Vlad Tepes, here calling himself Count Vladislav Tepes, decides to leave his castle and move to the west. He says he feels tired from Romanias decline and the seclusion of his life.

In Budapest he discusses some illegal business with Harker.  He also wants Jonathans help in turning his collection of paintings, jewels and his gold deposits to cash.  Jonathans friends businessman Quincey Morris, specialising in money swindles, and Arthur Holmwood, a British diplomat who is in a debt, offer to help.  Though Jonathan and Arthur have their doubts about the deal Quincey convinces them that money is all that matters and its one true power that makes the world go around.

Dracula gets very interested in those young people—the men, hungry for money and power; Lucy, who wants to sleep in many beds, in many cities, have new experiences and live for ever; and Mina, who wants to change the world and end human suffering.  Throughout the film Dracula tries to seduce all five of them into his own world, make them wish to become vampires.  Focusing again and again on how hypocritical morality is and promising them the loss of their consciences, he says survival of the fittest is the proper way and even the strong cannot save the weak.  He also references Gods slaughters in the Bible to prove that humanity was created in his image, the image of a killer.

There to stop him is the researcher of the occult and Sewards teacher Dr. Enrico Valenzi the one who believes that Dracula can be defeated when he faces a strong will empowered by faith.  But throughout this film he raises more and more self-doubts and his will is almost broken by the end.

It is Mina, halfway through her transformation to a vampire, that manages to make Dracula trust her and kills him as he holds her in an embrace. The films end with Mina still having the vampires mark and how that affects her remains a question.

==Cast==
* Patrick Bergin as Vladislav Tepes / Dracula
* Giancarlo Giannini as Dr. Enrico Valenzi
* Hardy Krüger Jr. as Jonathan Harker
* Stefania Rocca as Mina
* Muriel Baumeister	as Lucy
* Kai Wiesinger as Dr. Seward
* Alessio Boni as Quincy
* Conrad Hornby	as Arthur Holmwood
* Brett Forest as Roenfield
* Alessia Merz as Fair Woman
* Piroska Kiss as Dark Woman
* István Göz as Male Nurse
* Barna Illyés as Border Guard
* Csaba Pethes as Captain of the Tug
* Balázs Tardy as Tug Crew Member 1
* Levente Törköly as Tug Crew Member 2
* Ilona Kassai as Woman at the Hotel
* Imola Gáspár as Woman at the Manor
* Csilla Bakonyi		
* Petra Hauman		
* Tibor Kenderesi		
* Andrew Divoff as Doctor

==Reception==
Critical reaction to the film has been mixed to negative. David Johnson of DVD Verdict offered a positive review, saying: "Everyone involved commits to doing an okay job, and the production values and general atmosphere help shed the burden of the film stock and sad-sack effects. Bergins Dracula is effectively crusty and malicious, and Muriel Baumeister has a good time hamming it up as the infected Lucy."    Others were less positive: The SF, Horror, and Fantasy Film Review wrote, "While the film does an excellent job in updating Dracula to the midst of New Europes nouveau riche, director Roger Young lets the show down considerably in the second half. ... The script does get caught up in some pretentious natterings   the performances are particularly uneven." 

Noel Megahey of DVD Times said, "Its   awkwardness in the script and the dialogue that weighs heavily on the film, although the film actually does operate half-way successfully when it moves into the non-verbal action sequences. What really sinks the film in the end, though, is not the weakness of the special effects, but the performances and the delivery of the pan-European cast that struggles through their semi-dubbed English-language lines."  David Hall of EatMyBrains.com said, "There have been far worse cinematic incarnations of Stokers tale than this — but it must rank as one of the dreariest adaptations ever — a toothless bore shorn of any frisson of eroticism, with nary a drop of blood in sight." 

==See also==
*Vampire film

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 