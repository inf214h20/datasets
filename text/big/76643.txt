A Woman Under the Influence
{{Infobox film
| name           = A Woman Under the Influence
| image          = Awomanunderinfluence.jpg
| image_size     = 225px
| caption        = Theatrical release poster
| director       = John Cassavetes
| producer       = Sam Shaw
| writer         = John Cassavetes
| starring       = Gena Rowlands Peter Falk
| music          = Bo Harwood
| cinematography = Mitch Breit Al Ruban
| editing        = David Armstrong Sheila Viseltear
| studio         = Faces International Films
| distributor    = Cine-Source
| released       = November 18, 1974
| runtime        = 155 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 American drama film written and directed by John Cassavetes. It focuses on a woman whose unusual behavior leads her husband to commit her for psychiatric treatment and the effect this has on their family. It received two Academy Award nominations for Best Actress and Best Director. In 1990, A Woman Under the Influence was selected for preservation in the United States National Film Registry as being "culturally, historically, or aesthetically significant", one of the first fifty films to be so honored. 

== Plot ==
Los Angeles housewife and mother Mabel loves her construction worker husband Nick and desperately wants to please him, but the strange mannerisms and increasingly odd behavior she displays while in the company of others has him concerned. Convinced she has become a threat to herself and others, he reluctantly commits her to an institution, where she undergoes treatment for six months. Left alone with his three children, Nick proves to be neither wiser nor better than his wife in the way he relates to and interacts with them or accepts the role society expects him to play. After six months Mabel returns home but she is not prepared to do so emotionally or mentally, and neither is her husband prepared correctly for her return. At first Nick invites a large group of people to the house for a party to celebrate his wifes return, but realizing at the last minute that this is foolish, he sends most of them home. Mabel then returns with mostly only close family, including her parents, Nicks parents, and their three children to greet her but even this is overwhelming and the evening disintegrates into yet another emotionally and psychologically devastating event. After much drama, including a moment when Mabel cuts herself during a psychotic episode, the rest of the family leaves and the husband and wife are left alone to put their children to sleep. The youngsters profess their love for their mother as she tucks them in and then Nick and Mabel themselves ready their bed for a night to be alone as the film ends without real resolution.

== Cast ==
* Gena Rowlands as Mabel Longhetti 
* Peter Falk as Nick Longhetti
* Fred Draper as George Mortensen
* Lady Rowlands as Martha Mortensen
* Katherine Cassavetes as Margaret Longhetti
* Matthew Laborteaux as Angelo Longhetti
* Matthew Cassel as Tony Longhetti
* Christina Grisanti as Maria Longhetti

== Production ==
John Cassavetes was inspired to write A Woman Under the Influence when his wife Gena Rowlands expressed a desire to appear in a play about the difficulties faced by contemporary women. His completed script was so intense and emotional she knew she would be unable to perform it eight times a week, so he decided to adapt it for the screen. When he tried to raise funding for the project, he was told, "No one wants to see a crazy, middle-aged dame."   

Lacking studio financing, Cassavetes mortgaged his house and borrowed from family and friends, one of whom was Peter Falk, who liked the screenplay so much he invested $500,000 in the project.  The crew consisted of professionals and students from the American Film Institute, where Cassavetes was serving as the first "filmmaker in residence" at their Center for Advanced Film Studies. Working with a limited budget forced him to shoot scenes in a real house near Hollywood Boulevard, and Rowlands was responsible for her own hairstyling and makeup.  

Upon completion of the film, Cassavetes was unable to find a distributor, so he personally called theater owners and asked them to run the film. According to college student Jeff Lipsky, who was hired to help distribute the film, "It was the first time in the history of motion pictures that an independent film was distributed without the use of a nationwide system of sub-distributors." It was booked into art houses and shown on college campuses, where Cassavetes and Falk discussed it with the audience.  It was shown at the San Sebastián Film Festival, where Rowlands was named Best Actress and Cassavetes won the Silver Shell Award for Best Director, and the New York Film Festival, where it captured the attention of film critics like Rex Reed. When Richard Dreyfuss appeared on The Mike Douglas Show with Peter Falk, he described the film as "the most incredible, disturbing, scary, brilliant, dark, sad, depressing movie" and added, "I went crazy. I went home and vomited," which prompted curious audiences to seek out the film capable of making Dreyfuss (who is himself bipolar) ill. 

== Critical reception ==
A Woman Under the Influence has an overall approval rating of 95% on Rotten Tomatoes. 

Nora Sayre of the New York Times observed, "Miss Rowlands unleashes an extraordinary characterization   The actresses style of performing sometimes shows a kinship with that of the early Kim Stanley or the recent Joanne Woodward, but the notes of desperation are emphatically her own   Peter Falk gives a rousing performance   and the children are very well directed. But the movie didnt need to be 2 hours and 35 minutes long: theres too much small talk, which doesnt really reveal character. Still, the most frightening scenes are extremely compelling, and this is a thoughtful film that does prompt serious discussion." 

Roger Ebert of the Chicago Sun-Times rated the film four out of four stars and called it "terribly complicated, involved and fascinating – a revelation." He added, "The characters are larger than life (although not less convincing because of that), and their loves and rages, their fights and moments of tenderness, exist at exhausting levels of emotion   Cassavetes is strongest as a writer and filmmaker at creating specific characters and then sticking with them through long, painful, uncompromising scenes until we know them well enough to read them, to predict what theyll do next and even to begin to understand why."  Almost a quarter-century later, Ebert wrote a second review, in which he called Woman Under the Influence "perhaps the greatest of Cassavetes films." 

Time Out London said, "The brilliance of the film lies in its sympathetic and humorous exposure of social structure. Rowlands unfortunately overdoes the manic psychosis at times, and lapses into a melodramatic style which is unconvincing and unsympathetic; but Falk is persuasively insane as the husband; and the result is an astonishing, compulsive film, directed with a crackling energy."  

TV Guide rated the film four out of four stars, calling it "tough-minded" and "moving" and "an insightful essay on sexual politics." 

== Awards and honors ==
* Academy Award for Best Actress (Gena Rowlands, nominee)
* Academy Award for Best Director (John Cassavetes, nominee)
* Golden Globe Award for Best Motion Picture – Drama (nominee)
* Golden Globe Award for Best Actress – Motion Picture Drama (Gena Rowlands, winner)
* Golden Globe Award for Best Director (John Cassavetes, nominee)
* Golden Globe Award for Best Screenplay (John Cassavetes, nominee)
* Kansas City Film Critics Circle Award for Best Actress (Gena Rowlands, winner) National Board of Review Award for Best Actress (Gena Rowlands, winner)
* Writers Guild of America Award for Best Original Screenplay (John Cassavetes, nominee) Grand Prix of the Belgian Film Critics Association (winner)

== Restoration and preservation ==
The world premier screening of a restored print was held at the Castro Theatre in San Francisco on April 26, 2009 as part of the San Francisco International Film Festival. Gena Rowlands was in attendance and spoke briefly. The restoration was done by the UCLA Film & Television Archive with funding provided by Gucci and the Film Foundation.

== Home media ==
In 1992 Touchstone Home Video released the movie on VHS. 
 fullscreen format with audio tracks in English and French and subtitles in French.
 Opening Night – as part of the eight-disc box set John Cassavetes – Five Films by The Criterion Collection. The film is in anamorphic widescreen format with an English audiotrack. Bonus features include commentary by sound recordist and composer Bo Harwood and camera operator Mike Ferris and interviews with Gena Rowlands and Peter Falk. On October 22, 2013, the box set was re-released on Blu-ray. 

== In popular culture == Juliana Hatfield Three song Mabel was written as a tribute to the character Mabel Longhetti. 

== References ==
 

==Further reading==

* , 1994.
* , 2001.

== External links ==
* 
* 
* 
* , interview from May 2, 1975 by Nicholas Pasquariello
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 