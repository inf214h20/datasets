Hatavadi
{{Infobox film
| name           = Hatavadi
| image          = 
| caption        =  Ravichandran
| producer       = Sandesh Nagaraj
| writer         = 
| screenplay     = Ravichandran
| story          = Ravichandran Radhika
| music          = Ravichandran
| cinematography = G. S. V. Seetharam
| editing        = Ravichandran
| studio         = Sandesh Combines
| distributor    = 
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

Hatavadi ( ) is a 2006 Indian Kannada language film directed by V. Ravichandran. He stars in the lead role opposite Radhika Kumaraswamy|Radhika. In addition to directing the film and acting, Ravichandran also took roles of the films screenwriter, dialogue-writer, composer, lyricist and editor.  The supporting cast features Sharan (actor)|Sharan, Doddanna, Lakshman, Vinaya Prasad, Mukhyamantri Chandru and Chitra Shenoy.

==Cast==
  Ravichandran as Balu Radhika as Anisha Sharan
* Doddanna
* Lakshman
* K. S. L. Swamy
* Vinaya Prasad
* Padmaja Rao
* Ramesh Bhat
* Sadashiva Bramhavar
* Mukhyamantri Chandru
* Avinash
* Chitra Shenoy
* Bank Janardhan
* Rekha Das
* M. N. Lakshmidevi
* Om Prakash Rao
* Damini
* B. V. Radha
* Dayanand
 

==Production== Ravichandran announced that he would be directing Hatavadi, though the formal announcement of the film came seven months after its launch. The film was his second directorial under the banner Sandesh Combines, after Mommaga (1997).  

==Soundtrack==
{{Infobox album
| Name        = Hatavadi
| Type        = Soundtrack Ravichandran
| Cover       = 2006 Kannada film Hatavadi album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 26 June 2006
| Recorded    =  Feature film soundtrack
| Length      = 42:18
| Label       = Sri Eswari Entertainment
| Producer    = Ravichandran
| Last album  = Pandu Ranga Vittala (2005)
| This album  = Hatavadi (2006)
| Next album  = Neelakanta (2006)
}}
 Ravichandran composed the music for the soundtracks also writing its lyrics. The album consists of eight soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 42:18
| lyrics_credits = yes
| title1 = Aata Hudugatavo Ravichandran
| extra1 = Shankar Mahadevan
| length1 = 5:50
| title2 = Chali Chali
| lyrics2 = Ravichandran Chithra
| length2 = 4:27
| title3 = Ee Preethigagi
| lyrics3 = Ravichandran
| extra3 = S. P. Balasubrahmanyam
| length3 = 5:41
| title4 = Mukhadalli Yenide
| lyrics4 = Ravichandran
| extra4 = S. P. Balasubrahmanyam
| length4 = 4:36
| title5 = Oorella Suthi
| lyrics5 = Ravichandran
| extra5 = S. P. Balasubrahmanyam
| length5 = 5:51
| title6 = Thai Thai
| lyrics6 = Ravichandran
| extra6 = Udit Narayan, Malathi
| length6 = 4:29
| title7 = Yaaru Yaaru
| lyrics7 = Shree Chandru
| extra7 = Shankar Mahadevan, C. Aswath, B. Jayashree
| length7 = 6:37
| title8 = Yaramma Ivalu
| lyrics8 = Ravichandran
| extra8 = S. P. Balasubrahmanyam
| length8 = 4:47
}}

==Critical reception== Radhika and writes, "... it is to Radhikas credit that she stands up to a superb performance. Her emotions are perfect and she is presented very well on screen." 

==References==
 

==External links==
 

 
 
 