The Fox and the Child
{{Infobox film
| name           = The Fox and the Child
| image          = The Fox & the Child.jpg
| caption        = UK release poster
| director       = Luc Jacquet
| producer       = Yves Darondeau Christophe Lioud Emmanuel Priou
| writer         = Luc Jacquet Eric Rognard 
| narrator       = Isabelle Carré (French language|French)  Kate Winslet (English language|English) 
| starring       = Bertille Noël-Bruneau Isabelle Carré
| music          = Evgueni Galperine Alice Lewis   David Reyes
| cinematography = Eric Dumage Gérard Simon
| editing        = Sabine Emiliani Wild Bunch Future Films PictureHouse New Line Cinema (US) StudioCanal (Germany) Buena Vista International (France) 
| released       =  
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 29,610,210 $  
}}
The Fox and the Child (  by Luc Jacquet. Starring Bertille Noël-Bruneau, Isabelle Carré and Thomas Laliberté. The English version of the film is narrated by Kate Winslet.

==Plot==
A young girl of about 10 years lives in a farm house in the Jura mountains in eastern France. One day in autumn, when she rides her bicycle to school through the forest, she observes a hunting fox. Of course, the fox escapes, but the girl yearns to meet the animal again.

She spends most of her free time in the forests trying to find the fox during the following months, but she doesnt meet the fox again until winter comes. During the winter, she follows the foxs tracks far across the fields. Suddenly she compares her hand to the size of the tracks near to those she is following and discovers they are relatively fresh wolf tracks; she is alarmed as a wolf pack begins howling near her. She runs away panicked, falls and hurts her ankle.

The ankle heals very slowly, so that she has to stay at home during the winter reading a book about foxes and other animals of the forest.

When spring arrives, the girl is looking for fox holes and waits for the fox. The fox has young babies and moves holes because of her observations; therefore the girl decides to observe the fox from a greater distance.

She finds the fox again and tries to get the animal accustomed to her. She feeds it with meat. Later she can even touch the fox and is led to the new den. Finally, the fox arrives at her house and she lets it inside, wanting to show it her bedroom. But the fox becomes distressed at not being able to find a way out and escapes by smashing through her brittle plate glass window, causing the glass to shatter. The fox is hurt badly, but survives and the girl learns that she cannot keep wild animals as pets at home as they are unpredictable and may break more things such as fragile glass windows.

Years later she tells the whole story to her son, as seen at the end of the film. Some versions (DVD English) leave out this scene and put the gist of its text over the scene before it.

==About the film==
The film was shot on the Plateau de Retord in (Ain), which the  film director knows well because he spent his youth there, in the summer,  as well as in the  Abruzzo in Italy.
The foxes in the film were played by six animals: Titus, Sally, Ziza, Scott, Tango and Pitchou. Titus was the fox who had been tamed by Marie-Noëlle Baroni. It died on March 17, 2008 at the advanced age of 12 years. 

==Nominations==
* Young Artist Award
** 1. Nomination for "Best Performance in an International Feature Film - Leading Young Performer": Bertille Noël-Bruneau
** 2. Nomination for "Best International Feature Film"   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 