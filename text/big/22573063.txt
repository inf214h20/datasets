Himiko (film)
 
{{Infobox film
| name           = Himiko
| image          = 
| caption        = 
| director       = Masahiro Shinoda
| producer       = Kiyoshi Iwashita Kinshirô Kuzui Masahiro Shinoda
| writer         = Masahiro Shinoda Taeko Tomioka
| starring       = Shima Iwashita
| music          = Tōru Takemitsu Tatsuo Suzuki
| editing        = Sachiko Yamaji
| distributor    = Art Theatre Guild
| released       =  
| runtime        = 100 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

Himiko ( ) is a 1974 Japanese drama film directed by Masahiro Shinoda. It was entered into the 1974 Cannes Film Festival Feature Film Competition.   

==Cast==
* Shima Iwashita as Himiko
* Masao Kusakari as Takehiko
* Rentarō Mikuni as Nashime
* Rie Yokoyama as Adahime
* Jun Hamamura as Narrator
* Tatsumi Hijikata as Dancer
* Yoshi Kato as Ohkimi
* Choichiro Kawarazaki as Mimaki
* Kenzo Kawarazaki as Ikume

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 