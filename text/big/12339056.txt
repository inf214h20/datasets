Puteri Impian
 
 
{{Infobox film name           = Puteri Impian image          = Puteri Impian.jpg image_size     = 190px caption        = DVD director       = Aziz M. Osman producer       = Khalil Mohd. Zain writer         = Aziz M. Osman narrator       = starring       = Amy Mastura Chico Harahap Rashid Hairie Othman music          = Azman Abu Hassan cinematography = editing        = distributor    = released       =   runtime        = country        = Malaysia language       = Malay budget         = gross          =
}}
 Princess Diana, who died not long before the film was released.

A sequel titled Puteri Impian 2 was released the following year in 1998. The film is also credited for inspiring the creation of a Malaysian sitcom called Puteri, which has a similar premise.  Of note, Amy Mastura sang the theme song of the Puteri series.

==Plot==
A common factory worker named Nora Mat Jidin has always dreamed of becoming a princess. When she learns about an upcoming "Memburu Impian" competition, she submits her name as an applicant. She is overjoyed when she is picked as the winner, in which she will be given an all-expense-paid experience as Princess for one week, during which her exploits will be filmed for the "Puteri Impian" television show.

Nora revels in the experience, where she is given a thorough makeover and lives luxuriantly in the Istana Hotel (lit: Palace Hotel). The only shortcoming of the experience is her cameraman, Zulkifli, who immediately antagonises her and mocks her for her wide-eyed naivety.  However, Zulkiflis warning comes true when, during an event that Nora officiates as a princess, she is jeered at by onlookers who point out that she is nothing more than a dressed up nobody.

Matters become complicated when Nora crosses paths with Tengku Faizal, who is a real prince. Tengku Faizal is enchanted by Noras simple ways and sweet demeanor, and the two start dating. Zulkifli reacts to this jealously, to which Nora responds by using Tengku Faizal to keep Zulkifli at bay. When Tengku Faizals parents learn that the prince is considering proposing to Nora, the Queen confronts Nora directly to let her know their disapproval.

The film ends with Nora, wiser from the experience, rejecting Tengku Faizals proposal and returning to her simple life as a factory worker after the show ends. Zulkifli approves of her decision, but does not approach her.

==Cast==
* Amy Mastura as Nora Mat Jidin
* Cico Harahap as Zulkifli
* Nor Aliah Lee as Nancy
* Hairie Othman as Tengku Faizal
* Kuman as the Cameraman
* Azwan Ali as TV Presenter
* Sophia Ibrahim as the Queen

==Soundtrack==
Although the film does not have an official soundtrack, actress Amy Mastura released a studio album titled "Puteri", which contains a number of songs that were used in the film. The track list of this album is as follows:
# Hanya Dalam Lagu
# Puteri
# Gema Rembulan
# Cinta 2 Jiwa
# Kasih Ku
# Halaman Sejahtera
# Izinkan
# Di Antara Cinta
# Suatu Masa Suatu Ketika
# Terimalah Aku

==Awards==
14th Malaysia Film Festival (1999)
* Best Actress (Amy Mastura)
* Best Supporting Actress (Nor Aliah Lee)
* Best Sound (Azman Abu Hassan)
* Special Jury Award for being nominated for all categories

3rd Annual TV3 Skrin Award 97/98 (1998)
* Best Supporting Actress (Nor Aliah Lee)

==References==
 

==External links==
*  
*  
*  

 
 
  
 
 