Bless the Child
 
{{Infobox film
| name           = Bless the Child
| image          = Bless the Child film.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Chuck Russell
| producer       = Mace Neufeld Tom Rickman Clifford Green Ellen Green
| based on       =  
| starring       = Kim Basinger Jimmy Smits Angela Bettis Rufus Sewell Christina Ricci Holliston Coleman
| music          = Christopher Young
| cinematography = Peter Menzies Jr.
| editing        = Alan Heim
| studio         = Icon Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States Germany 
| language       = English
| budget         = $65 million
| gross          = $40,443,010   
}} thriller  film  directed by Chuck Russell, starring Kim Basinger, Jimmy Smits, Angela Bettis, Rufus Sewell, Christina Ricci, and Holliston Coleman. It is based on the novel of the same name by Cathy Cash Spellman.

==Plot== autistic daughter, Cody, at her home. Maggie takes Cody in, and she becomes the daughter she never had.

Six years later, Jenna suddenly re-appears with a mysterious new husband, Eric (Rufus Sewell), and abducts Cody (Holliston Coleman). Despite the fact that Maggie has no legal rights to Cody, FBI agent John Travis (Jimmy Smits), an expert in ritual murder and occult-related crime, takes up her case when he realizes that Cody shares the same birth date as several other recently missing children.

Cody, it soon becomes clear, is more than simply "special." She manifests extraordinary powers that the forces of evil have waited centuries to control, and her abduction sparks a clash between the soldiers of good and evil that can only be resolved, in the end, by the strength of one small child and the love she inspires in those she touches.

==Cast==
* Kim Basinger as Maggie OConnor
* Angela Bettis as Jenna OConnor
* Rufus Sewell as Eric Stark
* Christina Ricci as Cheri Post
* Holliston Coleman as Cody OConnor
* Jimmy Smits as Agent John Travis
* Michael Gaston as Detective Frank Bugatti
* Lumi Cavazos as Sister Rosa
* Ian Holm as Reverend Grissom

==Reception==
Bless the Child received almost universally negative reviews from critics, with Rotten Tomatoes giving this film a 3% rating and is ranked #29 on their worst-reviewed films of the last decade. 

Film critic Bruce Kirkland felt that Bless the Child was mocking Scientology in the guise of the fictional cult "The New Dawn". 

==Box office==
The film opened at #7 at the North American box office making United States dollar|USD$9.4 million in its opening weekend. It went on to gross only $40.4 million worldwide, below its $65 million budget. 
==Awards== Madonna for The Next Best Thing. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 

 
 
 
   
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 