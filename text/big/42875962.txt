The Two Orphans (1942 film)
{{Infobox film
| name = The Two Orphans 
| image =
| image_size =
| caption =
| director = Carmine Gallone
| producer = Federico Curioni  Carmine Gallone 
| writer =   Adolphe dEnnery(novel)   Eugène Cormon (novel)   Guido Cantini 
| narrator = Roberto Villa  Renzo Rossellini 
| cinematography = Anchise Brizzi
| editing = Niccolò Lazzari
| studio =   Grandi Film  ICI 
| released = 16 August 1942   
| runtime = 85 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama The Two Orphans by Adolphe dEnnery and Eugène Cormon, one of a large number of film adaptations. It was made at Cinecittà in Rome.

==Cast==
* Alida Valli as Enrichetta  
* María Denis as Luisa 
* Osvaldo Valenti as Pietro   Roberto Villa as Ruggero de Vaudray  
* Otello Toso as Giacomo  
* Gilda Marchiò as Madame Frochard  
* Germana Paolieri as Marianna  
* Memo Benassi as Il conte di Linières  
* Tina Lattanzi as La contessa Diana di Linières
* Enrico Glori as Il marchese di Presle 
* Guido Celano as Monsieur Gérard  
* Pio Campa as Marais  
* Adele Garavaglia as Suor Genoveffa 
* Umberto Spadaro as Il custode della stazione  
* Giuseppe Varni as Picard, il cameriere
* Ruggero Capodaglio
* Marisa Dianora 
* Edvige Elisabeth 
* Giulio Panicali 
* Luigi Pavese
* Aldo Silvani 
* Edda Soligo
* Aris Valeri

== References ==
 

== Bibliography ==
* Nowell-Smith, Geoffrey & Hay, James & Volpi, Gianni. The Companion to Italian Cinema. Cassell, 1996.
 
== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 