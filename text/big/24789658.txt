The Sweet House of Horrors
{{Infobox film
| name = The Sweet House of Horrors
| image = Sweethouseofhorrors.jpg
| caption = Shriek Show DVD Cover
| director = Lucio Fulci
| producer = 
| writer = Lucio Fulci
| starring = Jean-Christophe Brétigniere Cinzia Monreale Lubka Lenzi
| cinematography = Sebastiano Celeste
| music = Vince Tempera	
| editing = Alberto Moriani
| distributor = 
| released = 1989
| runtime = 
| country = Italy
| awards = Italian
| budget = 
| preceded_by = 
| followed_by = 
}}
The Sweet House of Horrors (in original Italian, La dolce casa degli orrori) is a 1989 Italian horror film directed by Lucio Fulci.

==Plot==
After being murdered by a burglar, a couple return as spirits to care for their two young children. They also seek revenge against their murderer and try to prevent their house from being demolished.

Mary and Roberto Valdi (Lubka Cibolo and Pascal Persiano) return home from a party to find a masked intruder ransacking their elegant country house. The masked man attacks and brutally kills both of them, and then disguises their deaths as an auto accident by driving their dead bodies to an isolated hilltop in their own car and pushing it over the edge.

At their parents funeral, bereaved children Sarah (Ilary Blasi) and Marco (Giuliano Gensini) exhibit a strange mixture of grief and stifled hilarity, chewing gum whilst weeping and giggling at the elderly priest (Dante Fioretti) conducing the graveside ceremony. The childrens Aunt Marcia, Mary Valdis younger sister (Cinzia Monreale) and Uncle Carlo, her husband (Jean Christophe Bretigniere), decide to stay with the children at the Valdi house while arrangements are made to sell the property. When Carlo is called away on business, Marcia spends an uneasy night at the Valdi house without him. Disturbed during the night, she explores the attic and is frightened by a giant toy fly which seems to attack her.

Over the next few days, Sarah and Marco are adamant that they wish to continue living at the house and are openly hostile to Mr. Colby (Franco Diogene), the real estate agent whom Carlo brings to the house. When the overweight Mr. Colby suffers an accident at the house after falling down a flight of stairs, the children laugh maliciously. Meanwhile, the Valdis gardener Guido (Lino Salemme) watches over the events with suspicion. A flashback recalled by Guido reveals that he was the masked intruder who broke into the house to rob it, only to be surprised when his employers returned sooner than anticipated, and he was forced to kill them. Elsewhere, Marcia becomes more and more scared of the house, and supernatural presences make themselves felt.

That night, Sarah and Marco are visited in their beds by floating flames which they suspect represent their dead parents. The following morning, Guido is about accept an generous check from Carlo for renovation of the house when a violent flashback, brought on by the supernatural forces within the house, shocks him into screaming his guilt for all to hear. He runs off and accidentally gets run over and killed by an oncoming truck on the road.

The following day, the injured Mr. Colby returns, but is attacked by a violent wind-storm in the hallway. Supernatural flames appear and heat up his crutches, burning his hands, making him fall down. Terrified, but defiant, he leaves the house again. That evening, Sarah and Marco conduct a masked ritual to contact the spirit world. They asks their parents to manifest themselves and to look after them. Their wish is somehow granted and they are greeted by their parents ghosts. Sarah and Marco are finally happy and able to spend more time with their loving, but non-corporeal parents, at any time they wish. But Marcia and Carlo cannot see the ghosts and they try to bundle the children into their car, intending to leave the house and return to their own home. However, a ghostly fog prevents them from getting very far and they are forced to return. The frightened couple bring in a medium, an arrogant, caped spirit challenger (Alexander Vernon Dobtcheff) to exercise the house of the childrens parents.

After a brief skirmish with the ghostly forces in the house, the spiritualist returns with a wrecking crew and a giant bulldozer and again commands the spirits of Mary and Roberto to leave the house before its smashed to rubble. For a while, the children try to inhabit an old lean-to playhouse theyd constructed, while the spiritualist, Mr. Colby, Marcia and Carlo are outside trying to persuade them to leave the building. The spirits of Sarah and Marcos parents enter two small stones as a way of staying with them forever wherever they go. The children finally walk out of the house where the spiritualist sees them hiding something in their pockets and tries to confiscate the stones. But when he picks up one, his right hand is melted away as he screams in agony. The family of living children and ghostly parents will apparently remain together.

==Production== DTV release. Shreik Show DVD Liner Notes, 2002. Last accessed: September 2009. 

==Releases==
Media-Blasters subsidiary Shriek Show would release the film for the first time ever in America in 2002. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 