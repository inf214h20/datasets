Unnai Ninaithu
{{Infobox film
| name = Unnai Ninaithu
| image = 
| director = Vikraman
| writer = Vikraman
| producer =  K. Muralitharan V. Swaminathan G. Venugopal
| cinematography = Balasubramaniem
| editing = V. Jaishankar
| studio = Lakshmi Movie Makers Surya Laila Laila Sneha Sneha Ramesh Khanna Delhi Ganesh Charle music = Sirpy released = 10 May 2002 runtime = 163 minutes
| language = Tamil
}} Laila and Telugu as Cheppave Chirugali in 2004.

==Plot== Laila and Suriya falling Sneha gets weaved into the plot and ends up falling for Suriyas charm, and fastidious behaviour and they end up having great times together, that is until Laila comes back all heart-broken after finding out her boyfriend wants nothing further to do with her as he was going to marry someone else. Upon seeing this Suriya becomes greatly disturbed and helps Laila out in all the ways he can. In the end Laila is moving back to her hometown and at the train station asks for Suriyas hand in marriage, only to find that he is not interested. He states that love isnt like a tape-recorder to play over and over again, it only comes once. He tells her he is now only interested in Sneha who will wait for him all her life. He sees her off and is leaving the train station when Sneha runs up to him. They both smile at each other and walk off into the sun set.

==Cast== Suriya  as Surya Laila as Nirmala Sneha as Radha
* Ramesh Khanna as Krishnamoorthy
* Chitra Lakshmanan as Vaidyalingam
* Charle as Mei Meiyappan
* R. Sundarrajan Sheela as Radhas Sister
* Thalaivasal Vijay as Nirmalas Father
* Pallavi as Nirmalas Mother
* Ramji as Selvam
* Hari Prashanth as Nirmalas brother
* Mayilsamy
* Sathyapriya as Radhas Mother
* Uma Maheshwari as Radhas Sister
* Kumaresan as Room Boy
* Manager Cheena as Deluxe room customer Pandu
* Singamuthu
* Vanaja

==Production== Vijay was Suriya replaced Vijay. 

Most of the story takes place with the backdrop of a lodge with a mess attached. To get the right location for this, the unit had travelled to many places, but no suitable location was found. It was then that a set was erected at the Campa Cola grounds, Chennai. The unit shifted to Sri Lanka to picturise a song, picturised on Surya, Laila and Sneha, it captured the exotic locations at Kandy, Ramboda falls, etc. Three other songs were shot in Malaysia, Thailand, and Sri Lanka. It is the first time that director Vikraman has chosen foreign locations to shoot his songs. 

==Critical reception==
Hindu wrote:"The story must have had a strong theme to begin with and the screenplay surely aims at conveying something different. But things do not progress in that direction".  Sify wrote:"The film is very slow and one feels that the story is as old as the hills. The comedy track by the inmates of the lodge like an astrologer and a quack doctor is boring. The songs tuned by Sirpy reminds you of songs in Vikraman’s earlier films and even the picturisations are repetitive. Of the cast Surya is the only saving grace while the girls Laila and Sneha has nothing much to do. Ramesh Kanna, Charlie & Co are a pain". 

the film made commercial success in box office which remaked in many languages.

==Awards==

{| class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|- 2002 Filmfare Awards South Filmfare Best Filmfare Award for Best Supporting Actress surya (actress)|Sneha
| 
|- 2002 Tamil Nadu State Awards  Tamil Nadu Best Film Unnai Ninaithu
| 
|- Tamil Nadu Best Actress Sneha
| 
|- Tamil Nadu Best Music Director Sirpy
| 
|- Tamil Nadu Best Lyricist Ravishankar
| 
|- Tamil Nadu State Film Award for Best Male Playback Unnimenon
| 
|}

==Remakes==
{| class="wikitable"
|-
! Year
! Film
! Language
! Cast
! Director
|- 2004
|Cheppave Chirugali Telugu
|Venu Tottempudi|Venu, Ashima Vikraman
|- 2007
|Krishna (2007 film)|Krishna Kannada
|Ganesh, Pooja Gandhi
|M. D. Sridhar
|}

==References==
 

 
 

 
 
 
 
 