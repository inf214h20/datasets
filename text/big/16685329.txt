Come Spy with Me (film)
{{Infobox Film
| name           = Come Spy with Me
| image          = Comespywithme-1967film.JPG
| image_size     = 
| caption        = Come Spy with Me theatrical poster Marshall Stone
| producer       = Arnold Kaiser
| writer         = Stuart James (story)   Cherney Berg   Erven Jourdan
| starring       = Troy Donahue   Andrea Dromm   Albert Dekker   Mart Hulswit   Dan Ferrone   Louis Edmonds
| music          = Bob Bowers   Smokey Robinson (theme)
| cinematography = Zoli Vidor
| editing        = Hy Goldman
| studio         = ABC Circle Films Futuramic MPO
| distributor    = 20th Century Fox
| released       = January 18, 1967  (U.S.A.) 
| runtime        = 85 minutes
| country        = United States English
| budget         = 
}}
 American spy Marshall Stone, and released by 20th Century Fox. 
==Plot==
Starring   performed the films titular theme song, written by lead singer Smokey Robinson|Robinson.

==Production==
The film was the first produced under a 13-film co-production treaty between Allied Artists and five television stations owned by ABC. MISS PAGE TO PLAY WIFE OF SOCRATES: Anthony Quayle Also Joins Cast of Hallmark Opener
New York Times (1923-Current file)   20 July 1966: 61.  

It was the first movie made by Troy Donahue following the end of his contract with Warner Bros. It was originally called Red on Red. Logan to Direct Camelot
Martin, Betty. Los Angeles Times (1923-Current File)   10 Mar 1966: d16.  

Hot off her success for her performance in  .  Dromm never made another film.
 1967 female spy films; Fathom (film)|Fathom with Raquel Welch and Caprice (1967 film)|Caprice with Doris Day.

==Critical reaction==

The film was released to mostly negative reviews and was not a success.  The New York Herald Tribune film review stated "The film belongs on top of sky high pile of other I Spy losers" ending the review with "come die with me".  The review in Variety paraphrased Dromms National Airline commercial catchphrase "Is this any way to run an airline? You bet it is!" to "Is this any way to make a motion picture? You bet it isnt!"

Consequently, Come Spy with Me has not been reissued by 20th Century Fox or released on home video; save for some television broadcasts, it has not been seen in several decades.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 