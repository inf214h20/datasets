Paragraph 175 (film)
 
 
  Paragraph 175, a joint German&ndash;British&ndash;American production, publicized the effects of the law on concentration camp internees.]]
 Jeffrey Friedman, Jeffrey Friedman, Janet Cole, Michael Ehrenzweig, Sheila Nevins and Howard Rosenman. The film chronicles the lives of several gay men and one lesbian who were persecuted by the Nazis. The gay men were arrested by the Nazis for the crime of homosexuality under Paragraph 175, the sodomy provision of the German penal code, dating back to 1871.

Between 1933 and 1945, 100,000 men were arrested under Paragraph 175. Some were imprisoned, others were sent to concentration camps. Only about 4,000 survived; see Paragraph 175 for full details.

In 2000, fewer than ten of these men were known to be living. Five come forward in the documentary to tell their stories for the first time, considered to be among the last untold stories of the Third Reich. 
 resistance fighter French Alsatian teenager, who watched as his lover was eaten alive by dogs in the camps.

== Award ==
* Teddy Award for best documentary film, 2000

== See also ==
* Persecution of homosexuals in Nazi Germany and the Holocaust

== External links ==
*  
*   - Rob Epstein remembers Pierre Seel - 2 December 2005 (audio file)

 

 
 
 
 
 
 
 
 
 
 

 
 