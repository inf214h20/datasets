A Midnight Clear
{{Infobox film
 | name = A Midnight Clear
 | image = A Midnight Clear DVD Cover.jpg
 | caption = VHS cover
 | director = Keith Gordon
 | producer = Bill Borden Dale Pollock Armyan Bernstein Tom Rosenberg Marc Abraham William Wharton   Keith Gordon  
 | starring = Peter Berg Kevin Dillon Arye Gross Ethan Hawke Gary Sinise Frank Whaley John C. McGinley Larry Joshua David Jensen Curt Lowens
 | music = Mark Isham Tom Richmond
 | editing = Don Brochu InterStar Releasing Beacon Communications A&M Films Sovereign Pictures Columbia Tristar Home Video   MGM Home Entertainment  
 | released = April 24, 1992
 | runtime = 108 minutes
 | country = United States
 | language = English
 | budget = $5 million
 | gross = $1,526,697
 }} intelligence unit German platoon that wishes to surrender.

==Plot==
In the early phase of the Battle of the Bulge in December 1944, a small US Army intelligence and reconnaissance squad (selected for their high IQs), are sent to occupy a deserted chateau near the German lines to gather information on the enemy’s movements. Losses from an earlier patrol has reduced the squad to just six men- Sgt Knott, Miller, Avakian, Shutzer, Wilkins and Mundy. On their way to the chateau, they discover the frozen corpses of a German and an American in a standing embrace, seemingly arranged by the Germans as a grim joke.

Settling in to their temporary home, they soon discover they are not alone. A group of German soldiers has occupied a position nearby. Knott, Mundy and Shutzer, whilst out on a patrol, suddenly see a trio of German soldiers aiming their weapons at them but the enemy then vanish without shooting. The Germans, clearly more skilled and experienced than the young GIs, soon leave calling cards, start a snowball fight one evening and offer a Christmas truce. At first, the Americans think the Germans are taunting them but it eventually becomes clear that the enemy want to parlay. Shutzer speaks enough German to communicate with the enemy who turn out to be a small group of youngsters still in their teens, commanded by an aging NCO. Having survived the Russian front, the Germans say they wish to surrender but on the condition that both groups agree to fake a skirmish in order to leave signs that the Germans were captured only after a struggle, so their superiors will not think they deserted, thus sparing their families back home any repercussions. All of the GI squad agree to participate except Wilkins who has been mentally unstable since receiving news from home of the death of his newborn child. He is left behind at the chateau, the others not telling him about the plan.

The two groups meet and proceed to fire their weapons into the air as planned. However Wilkins hears the shooting and thinks the engagement is real. Arriving at the scene, Wilkins opens fire at the Germans whereupon the latter, thinking they have been tricked, immediately shoot back. The situation immediately goes out of control and Knotts squad are forced to kill all of the enemy soldiers but not before Mundy is fatally hit and Shutzer is also badly wounded. Mundy’s final words are to beg the others not to tell Wilkins that the skirmish was intended to be fake. The squad’s superior officer arrives, reprimanding them for their conduct, before taking Shutzer back for treatment (they later receive word that he died in hospital). Left alone again, the four remaining soldiers quietly reflect as they try and celebrate Christmas, cleaning Mundy’s body in a bathtub. Shortly afterwards, the squad is forced to flee as the Germans attack the area in strength. Carrying Mundy’s corpse, they disguise themselves as medics and escape back to American lines.   There Knott is informed that Wilkins has been recommended for the Bronze Star and transferred to the motor pool, while the rest of the squad will be sent into the front line to fight as regular infantry.

==Cast==
*Peter Berg as Bud Miller
*Kevin Dillon as Mel Avakian
*Arye Gross as Stan Shutzer
*Ethan Hawke as Will Knott
*Gary Sinise as Vance Mother Wilkins
*Frank Whaley as Paul Father Mundy
*John C. McGinley as Maj. Griffin
*David Jensen as Sgt. Hunt
*Larry Joshua as Lt. Ware
*Curt Lowens as older German NCO
*Rachel Griffin as Janice
*Timothy Shoemaker as Eddie

==Critical reception==
The film received mostly positive reviews, with an 86% favorable rating on Rotten Tomatoes based on 35 reviews.   The Washington Post reviewer lauded it as "a war film completely unlike any other, a compelling accomplishment thats more soul than blood and bullets."  Vincent Canby of the New York Times praised the films solid construction, concluding that "In A Midnight Clear, just about everything works." 

Philip French reviewing the films 2012 DVD release in The Observer (newspaper)|The Observer described the film as an ironic, at times surreal fable.....and the plots twists are matched by the sharpness of its moral insights. 

==Awards and nominations==
;Nominations
*1993 Independent Spirit Award – Best Screenplay for Keith Gordon

==Trivia==
East Prussian-born actor Curt Lowens who played the veteran German NCO was a Jewish survivor of the Holocaust during the Second World War and he became a member of the underground resistance in the Netherlands. 

Director Steven Spielberg, when auditioning actors for his 1998 film Saving Private Ryan, had them read lines from the script of A Midnight Clear. 

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 