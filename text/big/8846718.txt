Candy Land: The Great Lollipop Adventure
{{Infobox Film
| name = Candy Land: The Great Lollipop Adventure
| image = CandyLand2005.jpg
| image_size =
| caption =
| director = Davis Doi Robert Winthrop
| writer = Janna King Kalichman Jymn Magon
| narrator =
| starring =
| music =
| cinematography =
| editing =
| distributor = SD Entertainment Hasbro Studios Paramount Pictures
| released = 2005
| runtime = 57 minutes
| country = U.S.A. English
| budget =
| preceded_by =
| followed_by =
}}
Candy Land: The Great Lollipop Adventure is a 2005 animated direct-to-DVD film  in which Jib the gingerbread boy, Princess Lolly and Mr. Mint try to save Candy Land from the evil Lord Licorice. The movie is based on the boardgame Candyland and was released on March 8, 2005 in the US and October 28, 2005 in Canada.

==Plot==
The evil Lord Licorice starts to take over Candy Land, assisted by his Bites, changing it from a brightly colored happy kingdom to a somber, drab-hued place. Gingerbread boy Jib is traveling the Rainbow Road to the annual Sweet Celebration. He enlists his friends Mister Mint and Princess Lolly to save Candy Land from a drab fate. Together with other Candy Land characters they outwit Lord Licorice and restore Candy Land to its colorful self.

==Cast (voice)==
* Alberto Ghisi as Jib, the main protagonist
* Ian James Corlett as Mr. Mint, a clown lumberjack of the Peppermint Forest/a Snow Beaver
* Scott McNeil as King Kandy/a Licorice Bite Doug Parker as Jolly/Gloppy
* Britt McKillip as Princess Lolly
* Mark Oliver as Lord Licorice, the main antagonist that makes Candy Land turn into Licorice Land.
* Kathleen Barr as Princess Frostine Ellen Kennedy as Gramma Nut
* Jane Mortifee as Mamma Gingertree, a tree that replaces Plumpy.

==The Crew==
* Davis Doi - Director
* Paul Sabella - Executive Producer
* Jonathan Dern - Executive Producer
* Carolyn Monroe - Executive Producer
* Mallory Lewis - Executive Producer
* Cheryl McCarthy - Producer
* Robert Winthrop - Producer

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 