Scattergood Baines
{{Infobox film
| name            = Scattergood Baines
| image           =
| caption         =
| director        = Christy Cabanne John E. Burch (assistant)
| producer        = Jerrold T. Brandt
| writer          =
| narrator        =
| starring        =
| music           =
| cinematography  = Jack MacKenzie
| editing         = Henry Berman
| distributor     = RKO Radio Pictures
| released        =  
| runtime         = 69 minutes
| country         = United States
| language        = English
}}

Scattergood Baines is a 1941 American comedy-drama film.  It is based on a novel by Clarence Budington Kelland. The character of Scattergood was also popular during the days of live radio.

In the film, Guy Kibbee plays the title character.  The plot has him choosing the small New England town of Cold River to settle down in and, twenty years later, his outmaneuvering of the townspeople both when it comes to large matters (ownership of the local railroad) and small (the pretty new schoolteachers hair).  

Five other Scattergood Baines films, all starring Guy Kibbee, were subsequently made.

== Cast == 

* Guy Kibbee as Scattergood Baines Carol Hughes as Helen Parker John Archer as Johnny Bones
* Dink Trout as Pliny Pickett
* Emma Dunn as Mirandy Baines
* Willie Best as Hipp
* Fern Emmett as Clara Potts
* Lee Lasses White as Ed Potts
* Kate Harrington as Gertrude Brown
* Joseph Crehan as Keith
* Edward Earle as Crane
* Bradley Page as McKettrick
* Paul White as Young Hipp
* Earle Hodgins as Jim Barton

==Background==
The homespun but canny Baines was originally created by popular writer Clarence Budington Kelland in stories for The Saturday Evening Post, and a radio version ran from 1938 through 1950.  This film did well enough at the box office to allow five sequels, all starring Kibbee:  

* Scattergood Pulls the Strings (1941)
* Scattergood Meets Broadway (1941)
* Scattergood Rides High (1942)
* Scattergood Survives a Murder (1942)
* Cinderella Swings It (1943)

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 