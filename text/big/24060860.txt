Marked Men (1940 film)
{{Infobox film
| name           = Marked Men
| image_size     =
| image	         = Marked Men FilmPoster.jpeg
| caption        =
| director       = Sam Newfield
| producer       = Sigmund Neufeld
| writer         = Harold Greene (story) George Bricker (screenplay)
| narrator       =
| starring       =
| music          =
| cinematography = Jack Greenhalgh
| editing        = Holbrook N. Todd
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
 American film directed by Sam Newfield (using the pseudonym "Sherman Scott") for Producers Releasing Corporation.

The film is also known as Desert Escape in the USA (TV title).

== Plot summary ==
Bill Carver (Warren Hull) is a man who was wrongfully sentenced to prison after being framed by mob gangster Joe Mallon (Paul Bryar). In prison, he is involuntarily involved in a jailbreak, also arranged by Mallon, who is also serving in the same facility.

The attempt to break is a failure, and Mallon and his men are soon caught by the police and put back behind bars again. All except for Bill, who gets away together with his dog Wolf, and vanishes into the desert. Out in the wild he meets Dr. James Prentiss Harkness (John Dilson) who lives there with his pretty daughter Linda (Isabel Jewell). Bill stays with the doctor, but then there is another jailbreak at the prison, more successful this time.

The refugees hold up a bank and make it look like Bill was involved in the robbing. Bill has to act to get rid of the blame for the robbing, and the only way he can do this is by catching the real perpetrators, Mallon and his men. He leaves his safe haven at the doctors place, and starts tracking the gang through the desert. Soon he finds Mallon and his men, who are lost in the desert, and in need of Bill helping them find the border to cross over to Mexico.

Bill agrees, seeing a chance of clearing his name, but the journey becomes a living hell for them all. Because of the lack of water and the perils in the desert at night, only Bill, his dog and Mallon remain when they come close to the border. Bill forces the weak and famined gangster to sign a written confession.

Just as Mallon finishes signing the document, Dr. Harkness and his daughter arrive to the rescue. Mallon desperately tries to kill Bill, but he is rescued by his dog. When his guilt is erased Bill can return to live inpeace and quiet with the doctor and his daughter. 

== Cast ==
*Warren Hull as Bill Carver
*Isabel Jewell as Linda Harkness
*John Dilson as Dr. James Prentiss Harkness
*Paul Bryar as Joe Mellon Charles Williams as Charlie Sloane
*Lyle Clement as Marshal Dan Tait
*Budd Buster as Mr. Marvin, druggist
*Al St. John as Gimpy, a thug
*Eddie Fetherston as Marty, a thug
*Ted Erwin as Mike, a thug
*Art Miles as Blimp - a Thug
*Grey Shadow as Wolf - the Dog

== Soundtrack ==
 

== External links ==
* 
* 

==References==
 

 

 
 
 
 
 
 
 
 


 