Maniyara
{{Infobox film
| name           = Maniyara
| image          =
| image_size     =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = T. E. Vasudevan
| writer         =
| screenplay     = Seema Adoor Bhasi Shanthi Krishna
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Jaijaya Combines
| distributor    = Jaijaya Combines
| released       =  
| country        = India Malayalam
}} 1983 Cinema Indian Malayalam Malayalam film, directed by  M. Krishnan Nair (director)|M. Krishnan Nair and produced by T. E. Vasudevan. The film stars Mammootty, Seema (actress)|Seema, Adoor Bhasi and Shanthi Krishna in lead roles. The film had musical score by A. T. Ummer.    

==Cast==
*Mammootty as Shameer Seema
*Adoor Bhasi
*Shanthi Krishna
*Shanavas
*Sankaradi
*Mala Aravindan
*Sathyakala

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kwaja Sheikhin Maqbaraa || K. J. Yesudas, Jolly Abraham || P. Bhaskaran ||
|-
| 2 || Mizhiyina njaanadaykkumpol || K. J. Yesudas, Ambili || P. Bhaskaran ||
|-
| 3 || Ninavinte kaayalil || K. J. Yesudas, Ambili || P. Bhaskaran ||
|-
| 4 || Penne Manavaatti || K. J. Yesudas, Vani Jairam || P. Bhaskaran ||
|-
| 5 || Viphalam || S Janaki, Chorus || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 


 