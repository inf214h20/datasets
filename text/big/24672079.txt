Smother (film)
{{Infobox film
| name           = Smother 
| image          = 
| caption        = 
| director       = Vince Di Meglio
| writer         = Tim Rasmussen Vince Di Meglio 
| starring       = Diane Keaton Dax Shepard Liv Tyler Mike White Ken Howard	
| music          = Angela Desveaux 
| cinematography =
| editing        =
| studio         = The Carsey-Werner Company Variance Films
| distributor    = Screen Media
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Smother is a 2008 comedy film starring Diane Keaton as a mother who is over-attached to her adult son. The movie is directed by Vince Di Meglio.

== Plot == Mike White) has moved in. Later that evening his mother, Marilyn (Diane Keaton) also arrives with her dogs and asks whether she can stay. Even though Noah is displeased, he allows Marilyn to stay. He discovers his mother has left his father, suspecting that he had an affair. He and Marilyn get hired at a carpet store, but because of Marilyns stupid tasks both of them get fired. Meanwhile his relationship with his wife, Clare, (Liv Tyler) deteriorates and she subsequently leaves. Marilyn spies her husband and they have an encounter. Her husband, Gene (Ken Howard) confesses that he has tried to cheat on her twice. Noahs grandmother, Helen Cooper (Selma Stern) dies, and at the funeral Noah and Maryiln debate. Noah gets moved by his mothers words and realises that his decision not to have a baby was wrong and rushes to Clare to apologize. The film ends with Marylin and Myron moving in together elsewhere.

== Cast ==

*Diane Keaton as Marilyn Cooper
*Dax Shepard as Noah Cooper
*Liv Tyler as Clare Cooper Mike White as Myron Stubbs
*Ken Howard as Gene Cooper
* Selma Stern as Helen Cooper Jerry Lambert as Donnie Booker
*Don Lake as Minister
*Sarah Lancaster as Holly

==External links==
*  

 
 
 
 
 


 