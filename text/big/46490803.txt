Sadhaaram
{{Infobox film
  | name = Sadhaaram சதாரம்
  | image = 
  | caption =
  | director = V. C. Subbaraman
  | producer = V. C. Subbaraman
  | writer = Follore Story A. K. Velan & A. T. Krishnaswami (dialogue)
  | starring =P. Bhanumathi  K. R. Ramaswamy  Gemini Ganesan
  | music = G. Ramanathan
  | cinematography = P. Sridhar
  | editing = 
  | Production company = Kasthuri Films
  | distributor = Kasthuri Films
  | released = 13 April 1956
  | runtime = . Tamil
  | budget =
  | preceded_by =
  | followed_by =
  }}
 Tamil film starring P. Bhanumathi, K. R. Ramaswamy and Gemini Ganesan.  The film was released in the year 1956. 

==Production==
Sankaradas Swamigal picked themes from mythology, literature and folklore and adapted them for the stage with deft, innovative touches. Sadhaaram is an illustrations of his brilliant stagecraft and was one such popular play that highlighted the noble attributes of Indian womanhood. As a child, P. U. Chinnappa had won praise acting as a thieving boy in this play.

A silent movie version of the story had been produced in 1930. The next celluloid adaptation was Naveena Sadhaaram in 1935 by Madras United Artistes Corporation. Directed by K. Subramanyam, the movie starred G. Pattu Iyer, S. S. Mani Bagavathar, S. D. Subbulakshmi and Parvathi Bai. 

This is the third attempt by V. C. Subbaraman which is in 1956.

==Plot==

 

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| P. Bhanumathi || Sadhaaram
|-
| K. R. Ramaswamy || 
|-
| Gemini Ganesan || 
|-
| M. N. Rajam ||  
|-
| T. S. Balaiah ||
|-
| V. K. Ramasamy (actor)|V. K. Ramasamy  ||
|-
| K. Sarangkapani || 
|-
| M. S. S. Bhagyam  ||
|-
| P. S. Vengadasalam || 
|-
| C. K. Saraswathi || 
|-
| K. S. Angamuthu || 
|}

==Crew==
* Producer: V. C. Subbaraman
* Production Company: Kasthuri Films
* Director: V. C. Subbaraman
* Music: G. Ramanathan
* Lyrics: Thanjai N. Ramaiah Dass
* Story: Folklore
* Screenplay: A. K. Velan & A. T. Krishnaswami
* Dialogues: A. K. Velan & A. T. Krishnaswami
* Art Direction: 
* Editing: 
* Choreography: 
* Cinematography: P. Sridhar
* Stunt: 
* Dance: Kumari Kala Devi

==Soundtrack==
The music was composed by G. Ramanathan. All lyrics were by Thanjai N. Ramaiah Dass.  Singers are K. R. Ramaswamy, P. Bhanumathi, K. Sarangkapani & V. K. Ramasamy (actor)|V. K. Ramasamy. Playback singers are T. M. Soundararajan, Thiruchi Loganathan, Seerkazhi Govindarajan, S. C. Krishnan, V. T. Rajagopalan, Jikki,  K. Jamuna Rani, A. P. Komala & A. G. Rathnamala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 100%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ponggi Varum Pudhu Nilavee || T. M. Soundararajan &  P. Bhanumathi  || || 03:11
|- 
| 2 || Anggum Inggum Paartthidaamal || Jikki || || 05:19
|- 
| 3 || Raajaatthi Kanne Raajaatthi  || K. R. Ramaswamy   ||  || 02:42
|- 
| 4 || Annaiye Kaaliyamma Eeswari || T. M. Soundararajan, V. T. Rajagopalan, A. P. Komala & A. G. Rathnamala  ||  || 05:46
|- 
| 5 || Azhagu Brammachchaari Vaarum Aiyaa  || K. Jamuna Rani  ||  || 02:29
|- 
| 6 || Kaayam Karuvappillai Soodandi ||  S. C. Krishnan & K. Sarangkapani ||  || 02:25
|- 
| 7 || Enggum Oli Veesudhe Ennai Thedi || P. Bhanumathi, A. P. Komala & A. G. Rathnamala  ||  || 03:10
|- 
| 8 ||  Ninaindhu Ninaindhu Nenjam Urugudhe  || T. M. Soundararajan  ||  || 03:11
|- 
| 9 || Laaba Nashtamadaa Naina || Seerkazhi Govindarajan & V. K. Ramasamy (actor)|V. K. Ramasamy  ||  || 02:11
|- 
| 10 || Nal Vaakuu Nee Kodadi  || K. R. Ramaswamy  ||  || 01:32
|- 
| 11 || Madhiyaadhaar Vaasal.... Mann Meedhu Maanam  || Thiruchi Loganathan  ||  || 02:54
|- 
| 12 || Pudhumai Enna Solven  || T. M. Soundararajan  ||  || 03:24
|- 
| 13 || Thaaye Ezhai Mugam || P. Bhanumathi  || || 
|- 
| 14||  Villaadhi Villanadaa.... Thullaattam Pottadhellaam   ||  K. R. Ramaswamy ||  || 02:39
|}

==References==
 

==External links==
*  
*  

 
 
 