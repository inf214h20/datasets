Battle for Terra
{{Infobox film
| name           = Battle for Terra
| image          = Battle-for-terra-poster.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Aristomenis Tsirbas
| producer       = Ryan Colucci Keith Calder Dane Allan Smith Jessica Wu
| screenplay     = Evan Spiliotopoulos
| story          = Aristomenis Tsirbas Brian Cox Chris Evans Danny Glover Amanda Peet David Cross Justin Long with Dennis Quaid and Luke Wilson 
| music          = Abel Korzeniowski
| cinematography = Aristomenis Tsirbas
| editing        = J. Kathleen Gibson SnootToons MeniThings Productions
| distributor    = Roadside Attractions  (released through Lionsgate Films) 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =  
| gross          = $6,129,529   
}} computer animated Brian Cox, Luke Wilson, Amanda Peet, Dennis Quaid and Justin Long among others.
 camera could be added to the film.    After the film was shown at festivals and distributors showed an interest in it a small team was hired to render the entire film again from the perspective of the second camera for a true 3D effect. 

It won the Grand Prize for Best Animated Feature at the 2008 Ottawa International Animation Festival. 

==Plot==

Mala (Evan Rachel Wood) and Senn (Justin Long) are young alien creatures who live on Terra, a planet from a star system in the Milky Way. Terra is a peaceful planet of small alien creatures who have a rich semi-advanced culture.

One day a large, mysterious object blocks the Terrian sun, piquing the Terrians interest. However, since the Terrian culture bans the development of new technologies, such as telescopes, without the approval of the ruling council, none of the inhabitants are able to get a closer look at the huge object in their sky.

Mala, who is inventive and headstrong, goes against the rules of her community and creates a telescope, which she takes out into the dark empty area outside the Terrian city and uses to view the object and witnesses smaller objects coming from the large object that turn out to be incoming scout spaceships. She returns to the city to find that the scout spaceships have already started abducting Terrians (who willingly offer themselves to the "scout spaceships" mistaking them as their new "gods").

After Malas own father, Roven (Dennis Quaid) is abducted she goads a ship into tailing her and lures it into a trap, which causes it to crash. Afterward, she saves the life of the pilot, revealed to be a human, an officer named Lieutenant Jim Stanton (Luke Wilson). After his personal robot assistant named Giddy (David Cross) warns Mala that Stanton will die without a supply of oxygen, (The Terran atmosphere contains none), she makes a deal with Giddy that shed help him only if Giddy teaches Mala their language. So Giddy teaches her humans language. Then she creates an oxygen generator and fills a tent with air containing oxygen so that Jim can breathe. The Robot informs Mala that the mysterious object is a generation ship called The Ark, containing humans from Earth.

Centuries beforehand, both Mars and Venus were terraformed by humans and colonized. But 200 years later, the two planets demanded independence from Earth, which the Earth government refused to grant them due to the high demand of resources from the two colonies. The dispute between the three planets escalated into a violent interplanetary war that left all three planets uninhabitable. The Ark, containing the remnants of the human race, traveled for several generations looking for a new home. When Stanton awakes the robot informs Stanton and Mala that a crucial part of the ship was damaged in the crash, Mala offers to make a replacement part herself. When Stanton, Mala, and the robot return to the crash site, they discover that the ship has been moved.

The trio track the ship to a huge underground military facility which was built by a previous, warlike generation of the Terrians. The trio realizes that despite the current peaceful nature of the Terrian city, the elders have secretly retained the military technology from the dark days of war. After infiltrating the facility, fixing the ship and flying back to the Ark, Jim orders Mala to stay and goes to be debriefed.
 Brian Cox), takes power over the civilian leaders in a coup, and declares war on Terra, citing the deteriorating condition of the Ark. His goal is to annihilate the Terrians so that the humans can turn Terra into the new Earth. He plans to drop a huge machine onto the planets surface—called the Terraformer—which will create an Earthlike atmosphere. Stanton is sent to be in the first group of space-fighters designated to defend the Terraformer, while General Hemmer will go down to the planets surface in the Terraformer to personally supervise the terraforming process.

After the humans drop the Terraformer machine onto the surface, it begins to replace the native gases with oxygen and nitrogen, which will asphyxiate the aliens. The terrian elders bring out all of the secretly-hidden military technologies from their secret base, and huge waves of terrian glider-fighters attack the Terraformer machine. The human spacefighter ships begin a huge and bloody battle against the high-tech alien glider-fighters, and the sky is filled with laser cannon fire and explosions.

Finally, as the Terraformer is close to the completion of its goal of turning the Terrian atmosphere into an Earth like atmosphere, Lieutenant Stanton realizes that annihilating all of the inhabitants is morally wrong. He turns his ship towards the Terraformer machine, and, with his laser cannons blazing, attacks it. As his ship is raked with anti-aircraft defensive fire, Stanton fires his air-to-air missiles at the Terraformers command module, destroying it, General Hemmer and Stanton in a ball of flame.

An epilogue shows what happens in Terra some time later. The Terrians and the humans have decided to live in peace. With the Terraformer machine destroyed, the Terrian atmosphere becomes safe once again for the aliens. The Terrians create a sanctuary for the human colonists to live in, with an Earth-like atmosphere. In the human sanctuary, a large statue of Lieutenant Stanton is erected, in honor of his memory and sacrifice.

==Cast==
* Evan Rachel Wood as Mala Brian Cox as General Hemmer
* Luke Wilson as Jim Stanton
* David Cross as Giddy
* Justin Long as Senn
* Amanda Peet as Maria Montez
* Dennis Quaid as Roven Chris Evans as Stewart Stanton
* James Garner as Doron
* Danny Glover as President Chen
* Mark Hamill as Elder Orin
* Tiffany Brevard as Singer Soloist
* Danny Trejo as Elder Berum
* Phil LaMarr as Fabric Merchant
* Laraine Newman as Toy Merchant
* Ron Perlman as Elder Vorin

==Release==
 
Roadside Attractions handled theatrical marketing in   (4,099 screens) and Ghosts of Girlfriends Past (3,175).

==Reception==

===Critical reaction===
The film has received average reviews from critics. Based on 94 reviews collected by Rotten Tomatoes, the film has an average rating of 5.5/10, and a score of 48% from critics.  Another review aggretator, Metacritic, which assigns a normalized rating out of 100 top reviews from mainstream critics, the film has received an average score of 54%, based on 19 reviews. 

===Box office===
The film opened at #12 in the United States grossing $1,082,064 in 1,159 theaters with an average of $934 per theater. The films international box office began May 14, 2009 in Russia with a 5th place opening of $332,634 at 83 screens. "Battle for Terras" current worldwide total is $6,101,046. 

==Home media release==
Battle for Terra was released on DVD and Blu-ray Disc September 22, 2009 by Lionsgate Home Entertainment. 

Battle for Terra was released in France in French and English version by Rézo Films on DVD and Blu-ray Disc October 20, 2010 and include a 3D version of the movie with 4 3D glasses. {{cite web|url= Inficolor 3D technology, a patent pending stereoscopic system, first demonstrated at the International Broadcasting Convention in 2007 and deployed in 2010. It works with traditional 2D flat screens and HDTV sets and uses glasses with complex color filters and dedicated image processing that allow natural color perception with a pleasant 3D experience. When observed without glasses, some slight doubling can be noticed in the background of the action which allows watching the movie in 2D without the glasses. This is not possible with traditional brute force anaglyphic systems. 

A Region B Blu-ray 3D was released in Germany. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 