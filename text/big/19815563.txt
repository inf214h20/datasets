80,000 Suspects
{{Infobox film
| name           = 80,000 Suspects
| image          = 80,000Suspects1963Poster.jpg
| alt            =  
| caption        = US Poster
| director       = Val Guest
| producer       = Val Guest
| writer         = Val Guest
| screenplay     = 
| story          = 
| based on       = Pillars of Midnight by Elleston Trevor Richard Johnson Yolande Donlan Cyril Cusack
| music          = Stanley Black Arthur Grant
| editing        = Bill Lenny
| studio         = 
| distributor    = Rank Organisation (UK) Twentieth Century-Fox(US)
| released       =    
| runtime        = 113 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
80,000 Suspects is a 1963 British film, directed by Val Guest which concerns an outbreak of smallpox in Bath, Somerset|Bath, England.

==Plot==
  
A romantic melodrama set against the backdrop of a smallpox epidemic which features Richard Johnson as the diligent doctor in control and Claire Bloom as his outwardly serene and devoted wife. Commencing on New Year’s Eve in the city of Bath, Dr. Steven Monks (Richard Johnson) diagnoses a mystery patient as being infected with smallpox and sets in motion a city wide quarantine to contain the outbreak. His commitment to the task is affected by the deterioration of his marriage to ex-nurse Julie (Claire Bloom) following his clandestine affair with a family friend. Monk receives an unexpected blow when the disease strikes closer to home than anticipated and Julie is diagnosed as having contracted the virus. The medical team gradually contain the outbreak until only one unidentified case remains. The search narrows the identity of final carrier down to Ruth Preston (Yolande Donlan), the woman with whom Monks had been having an affair and the wife of his close colleague Clifford (Michael Goodliffe). She’s eventually traced to a deserted house where she’s sheltering, lonely and desperately ill.

==Cast==
*Claire Bloom as Julie Monks Richard Johnson as Steven Monks 
*Yolande Donlan as Ruth Preston 
*Cyril Cusack as Father Maguire 
*Michael Goodliffe as Clifford Preston 
*Mervyn Johns as Buckridge 
*Kay Walsh as Matron 
*Norman Bird as Mr. Davis 
*Basil Dignam as Medical Officer 
*Arthur Christiansen as Editor 
*Ray Barrett as Bennett 
*Andrew Crawford as Dr. Ruddling 
*Jill Curzon as Nurse Jill 
*Vanda Godsell as Mrs. Davis 
*Ursula Howells as Joanna Duten David Weston as Brian Davis

== External links ==
*  
* British Movie Directors   Retrieved 2010-03-11.
* Original Clip from film   Retrieved 2010-03-11.
* Opening scenes Clip from film   Retrieved 2010-03-11.
* Visit Bath - Movie Locations in the City   Retrieved 2010-03-11.
* Val Guest - Obituary, 15 May 2006 - The Independent   Retrieved 2010-03-12.

 

 
 
 
 