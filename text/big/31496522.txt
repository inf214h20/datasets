Chalo Dilli
 
 
{{Infobox film
| name            = Chalo Dilli
| image           = Chalo-dilli.jpg
| caption         = Theatrical release poster
| director        = Shashant Shah
| producer        = Krishika Lulla Kavita Bhupathi Chadda 
| screenplay      = Arshad Sayed
| story           = Arshad Sayed
| starring        = Lara Dutta Vinay Pathak Akshay Kumar  Sachin Gupta (Hi5)  Rohit Kulkarni Roshan Balu 
| cinematography  = Nikos Andritsakis
| editing         = Aseem Sinha
| studio          = 	 	 Eros International Media Ltd. Bheegi Basanti Entertainment
| released        =  
| runtime         =
| country         = India
| language        = Hindi
| budget          =  
| gross           =  
}} Eros International Media Ltd. was shot at locations in Mumbai, Delhi and Jaipur.  It was released on 29 April 2011. It was inspired by the film Planes, Trains and Automobiles starring Steve Martin.  A sequel to the movie named Chalo China was set to be made in 2014 but postponed due to lack of financers. 

==Plot==

Mihika Banerjee (Lara Dutta), a successful investment banker living in Mumbai, misses her flight to Delhi and needs to get there any way possible to meet her husband, Lt. Col. Vikram Rathore (Akshay Kumar). She meets Manu Gupta (Vinay Pathak) and they discover the colours of India in their journey to Delhi. They go by road and train and the audience gets a chance to see both the large urban conglomerations and the small rural areas that make up India. During the journey, Mihika experiences various difficulties which she has never encountered in her high class life. When Mihikas money is stolen while she is buying a train ticket, she is forced to travel with Manu in the ordinary class in Jhunjhunu District. Thus the film revolves around Mihikas and Manus journey amidst difficulties.

==Cast==
* Lara Dutta as Mihika Banerjee
* Vinay Pathak as Manu Gupta
* Akshay Kumar as lieutenant Colonel Vikram  Singh Rana (Husband of Mihika)
* Pankaj Jha as Inspector Surendra Mishra
* Brijendra Kala as K.C Pant ( Train Ticket Examiner)
* Rahul Singh (actor) as Bhairon Singh Gurjar (also Gujjar Singh)
* Raghavendra (Raghav) Tiwari as Naresh
* Lokesh Verma as Billu
* Ajit Mathur as Pappu 
* Gaurav Gera as Gopi
* Yana Gupta as Laila (Guest Appearance in the song "Laila O Laila")
* Mukesh Bhatt (actor) as Pandeyji
* Teddy Maurya as Bhaiyaji
* Manoj Bakshi as Jhujjhar Singh
* Narottam Bain as Bablu
*Dadhi R Pandey as Dharampalji (Truck Driver)
*Narottam Bain as Shivratan Bansod (Taxi driver)

==Critical reception==
The film received a mixed to negative reception, and it was given 3.5/5 stars by The Times of India,  2.5/5 stars by IndiaWeekly  and 1.5/5 by rediff.com. 

{{Infobox album
| Name        = Chalo Dilli
| Type        = Soundtrack
| Artist      = Vishal-Shekhar
| Cover       = Chalo-dilli.jpg
| Released    = 29 April 2011
| Recorded    =
| Genre       = Film soundtrack
| Length      =
| Label       = T-Series
| Producer    =
}}

==Soundtrack== Sachin Gupta, Rohit Kulkarni, and Roshan Balu. Lyrics are penned by Manthan, Anand Raj Anand, Krishika Lulla, Shabbir Ahmed, and Nisha Mascarenhas.

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = yes
| title1 = Chalo Dilli 
| extra1 = Raja Hasan
| music1 = Gourov Dasgupta
| length1 = 4:34
| title2 = Hi 5
| extra2 = Neeraj Shridhar Sachin Gupta
| length2 = 5:49
| title3 = Kaun Se Badi Baat
| extra3 = Kamal Heer
| music3 = Rohit Kulkarni
| length3 = 5:12
| title4 = Laila O Laila
| extra4 = June Banerjee
| music4 = Gourov Dasgupta
| length4 = 3:11
| title5 = Matargashtiya
| extra5 = Sukhwinder Singh
| music5 = Anand Raj Anand
| length5 = 4:47
| title6 = Moments in Life
| extra6 = Natalie Di Luccio
| music6 = Rohit Kulkarni
| length6 = 3:24
| title7 = Hi 5
| note7 = Club Mix
| extra7 = Neeraj Shridhar Sachin Gupta
| length7 = 2:33
| title8 = Laila
| note8 = Club Mix
| extra8 = June Banerjee
| music8 = Roshan Balu
| length8 = 4:35
}}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 