Fathers and Sons (1957 film)
 
{{Infobox film
| name           = Fathers and Sons
| image          = Fathers and Sons (1957 film).JPG
| caption        = Film poster
| director       = Mario Monicelli
| producer       = Guido Giambartolomei
| writer         = Leonardo Benvenuti Luigi Emmanuele Agenore Incrocci Mario Monicelli Furio Scarpelli
| starring       = Vittorio De Sica
| music          = Carlo Rustichelli
| cinematography = Leonida Barboni
| editing        = Otello Colangeli Mario Serandrei
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Fathers and Sons (  and also known as A Tailors Maid) is a 1957 Italian comedy film directed by Mario Monicelli.    At the 7th Berlin International Film Festival Monicelli won the Silver Bear for Best Director award.   

==Cast==
* Vittorio De Sica - Vincenzo Corallo
* Lorella De Luca - Marcella Corallo Riccardo Garrone - Carlo Corallo
* Marcello Mastroianni - Cesare
* Fiorella Mari - Rita
* Franco Interlenghi - Guido Blasi
* Antonella Lualdi - Giulia Blasi
* Memmo Carotenuto - Amerigo Santarelli
* Marisa Merlini - Ines Santarelli
* Ruggero Marchi - Vittorio Bacci
* Emma Baron - Missis Bacci
* Gabriele Antonini - Sandro Bacci
* Franco Di Trocchio - Alvaruccio
* Raffaele Pisu - Vezio Bacci
* Franca Gandolfi
* Mario Merlini
* Amerigo Santarelli

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 