That Night
{{Infobox film
| name           = That Night
| image          = That Night 1992 poster.jpg
| caption        = Theatrical release poster
| director       = Craig Bolotin
| producer       = Arnon Milchan Stephen Reuther
| screenplay     = Craig Bolotin
| based on       =  
| starring       = C. Thomas Howell Juliette Lewis Eliza Dushku Helen Shaver David Newman
| cinematography = Bruce Surtees
| editing        = Priscilla Nedd-Friendly studio  Le Studio Canal+ Regency Enterprises Alcor Films
| distributor    = Warner Bros. 1993
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $7,000,000
| gross          = $20,473
}}
 romantic drama film written and directed by Craig Bolotin, and starring C. Thomas Howell and Juliette Lewis. It is based on the novel of the same name by Alice McDermott. 
 Buffy the Vampire Slayer fame) and Katherine Heigl (of Greys Anatomy fame) made their first film appearances in it, sharing a few scenes.  Dushku was eleven years old at that time, and Heigl was thirteen.

== Plot synopsis ==
In 1961 Long Island, Alice Bloom (Eliza Dushku) is a ten-year-old girl who is trying to understand how love works. She is infatuated with the girl across the street, 17-year old Sheryl O Connor (Juliette Lewis). She often looks at her from across the street, as their bedroom windows are level with each other. Alice starts to copy every detail about Sheryl, including her perfume and the record she listens to. As Alice and her mother pick up her father from work, she notices Sheryl speeding up to the train station to pick up her own father. She admires the affection that Sheryls father gives her, as she doesnt receive the same from her own father.  She then tells her mother about how amazing Sheryl is: about how she could travel long distances in her car in no time at all, how she was slapped in the face by one of her Catholic School teachers and never cried, and how she ran the mile in gym and never broke a sweat. Alices mother does not believe what she is saying.

One day she decides to go bowling with some of her friends and is ridiculed by them when she rolls a ball into the lane next to hers, and her friends award her with a score of "minus zero" and calling her a dufus. Reeling from comments made to her, she immediately becomes excited when Sheryl walks into the bowling alley along with a group of guys trying to win her affection. Sheryl, seemingly innocent and moral, rejects their advances. She rings the bell at the front desk, and from under the counter a boy named Rick (C. Thomas Howell) appears. They are instantly attracted to each other. As Alice continues to bowl with her friends, she constantly watches Sheryls every move. Her friends then mention that they think Sheryls breasts are fake, because they do not move. Alice insists they are real, and says that they are friends. They make her go over to talk to Sheryl and get Sheryl to take a drink of her soda, thereby proving that they are friends. But before she can get there, Rick pages her to come back to the desk, and a police officer tells her that her father just died.

During the funeral, Sheryl is obviously upset. As she is sitting in the bathroom, she notices her bowling shoes on the floor and goes to the bowling alley to return them. There she finds Rick repairing one of the pin returns. He tells her they are closed, and she starts crying over her father. After some conversation, Rick walks Sheryl home, and leads to their first kiss. This is observed by Alice, who earlier had spotted Sheryl running to go to the bowling alley. The next day Rick comes back with his gang, and they take Sheryl to the beach, where they have oysters and tequila and Sheryl pours her heart out over her fathers death. They spend the whole day and night together.

All is not well, however.  Sheryls mother disapproves of the relationship between her and Rick.  Eventually, she bars her from seeing him, and the neighborhood begins to revile Rick.  Sheryl refused to stop seeing him however, even as her mother attempts to put her on lockdown.  She gets her chance again as she addresses a coughing Alice laying in her grass.  Alice was victim of a prank at a boys birthday party during a game of spin the bottle.  Sheryl babysits Alice (with Alices parents being out, as they had dropped her off at the party), and the two begin to take root in their friendship.  Sheryl imparting wisdom about the boys bullying of her being typical traits of young boys who crush on young girls.  Alice offers also to help Sheryl sneak out and see Rick.  The three of them spend much of the night together.  Which includes Alice going to seedy places like dive bars and under the boardwalk (a hangout for many youths like Sheryl and Rick).  She also makes a record in a booth detailing everything that happened that night.  Sheryl suggests that she bury it in a time capsule for her to dig up many years later.  

Alice continues to help Sheryl and Rick hide their relationship.  She goes to the bowling alley to explain to Rick why Sheryl couldnt make it to see him one day, even yelling at her father in his anger as he advises her to stay away from him.  Feeling that her father didnt understand love.  Its revealed that Sheryl is pregnant.  Her mother decides to send her to an unwed mothers home 300 miles or so from Long Island.  Despite Sheryls protests (to include Sheryl suggesting abortion, and still wanting to see Rick), she eventually does what her mother says.  Rick repeatedly calls Sheryls house, only to have her mother tell him not to call, and hang up on him.  Finally, he and his gang drive to her house, where Rick pleads to speak to Sheryl.  This captures the attention of most of the immediate block.  Her mother informs him that she has gone, and that he is to leave as well.  He refuses to believe her (looking at Sheryls still open window), and pushes her inside to go in to the house.  The neighborhood fathers then rush to help her, and a brawl ensues between Ricks gang and them, with Rick getting hit in the head with a snow shovel.  He would spend a week in jail, however its assumed that no serious charges were filed.

Alice becomes withdrawn from the incident.  Once she walks in on her parents listening to the record she made that night, she decides to run away.  She ends up under the boardwalk, where Rick finds her, and the two talk.  Reluctantly, Rick agrees to drive upstate with Alice to meet Sheryl at her unwed mothers home.  After some bartering with a few other occupants of the home, she is snuck inside to meet Sheryl, asking if she can make it out to a restaurant at midnight to meet Rick.  Sheryl agrees.

The two meet, but Sheryl seems to have come to an understanding regarding her situation.  She decides that she wants to put the baby up for adoption.  She also suggests that her situation is too complicated for a young woman such as herself to realistically see through a life with Rick, a baby, and no money or viable career opportunities for either of them.  Rick is upset, and hands her an engagement ring that he planned to propose with, suggesting that she could pawn it.  Alice then talks to Sheryl, asking her what happened to her earlier views regarding how nothing could stop true love.  Sheryl tells her that she is simply too young to understand.  As Alice suggests the three of them running away together, Sheryl tells Alice that she mustnt leave her loved ones.  As Alice is put on a bus back to Long Island and stares out the window at Rick, Sheryl comes out, and the two embrace.  

Alice makes it home, and her parents are relieved to have her back.  She states that despite the gossip about Sheryl that went on afterward, she received a postcard telling her the truth: That Sheryl and Rick were well on their way to the west coast, and they were doing well.  She follows up on her promise to bury her record of that night, and to dig it up in 1999.  She then meets up with one of her male friends from earlier.  They discuss her running away, and its made clear that Sheryls earlier suggestion to Alice (that the boy was mean due to his affection towards her) was true.  Alice then reveals that she learned some things that summer that she would never forget.

== Cast ==
*Juliette Lewis as Sheryl OConnor
*Eliza Dushku as Alice Bloom
*C. Thomas Howell as Rick
*Helen Shaver as Ann OConnor
*J. Smith-Cameron as Carol Bloom
*John Dossett as Larry Bloom
*Katherine Heigl as Kathryn

== Reception ==
The film itself received mildly positive reviews, but the performances by Dushku and Lewis were praised. 
David Stratton, in Daily Variety wrote "This isnt exactly riveting material, and the films modest production values seem more suited to the small screen.  Nevertheless,  ushku makes the hero-worshiping moppet an engaging character, and Howell is just right as every suburban moms idea of a daughters undesirable boyfriend. Lewis, her hair dyed blond, is more than adequate as the vivacious Sheryl."    
Janet Maslin in the New York Times expressed detailed disappointment in the number and depth of changes made in the film, and found Lewis "slinky, demonstrative performance is way out of proportion to the tepid film built around it." 
Entertainment Weekly graded the film "B-", remarking that director Bolotin "leans too heavily on period detail, but That Night clicks whenever it taps into the crazy, stupid madness of teen lust."   
In Rolling Stone magazine, Peter Travers wrote that while book author McDermott made clear how the intensity of the teen romance changes Alices life, first-time director Bolotin offers a "pale facsimile that traffics in too many coming-of-age clichés", but concluded, "what makes That Night worth seeing is a knockout performance from Lewis, who evokes the joy and confusion of sexuality. You cant take your eyes off her." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 