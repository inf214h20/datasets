Premada Kanike
{{Infobox film name           = Premada Kanike image          =  director       = V. Somashekhar producer       = Jayadevi  story          = Salim-Javed screenplay     = Chi. Udaya Shankar based on       =   starring  Rajkumar   Aarathi   Jayamala   Vajramuni music          = Upendra Kumar cinematography = D. V. Rajaram   editing        = P. Bhakthavathsalam studio         = Jayadevi Films distributor    =  released       =   runtime        = 151 minutes country        = India language       = Kannada
}}
 1976 Indian Kannada language thriller film directed by V. Somashekhar and produced by Jayadevi. The film starred Rajkumar (actor)|Rajkumar, Aarathi and Jayamala in the lead roles. The songs composed by Upendra Kumar were received extremely well and considered as evergreen hits. The story for the movie was written by Salim-Javed.   the film is loosy based of Justice Viswanathan (1971). 
 Tamil as Polladhavan (1980 film)|Polladhavan (1980) starring Rajinikanth.

The story is about a teacher and her nephew hired by rich family to teach their daughter. One day she witness a murder in a train and discovers that the murderer is the father of her student. The girl who plays student, Poornima, is the real-life daughter of Rajkumar.  It also features his youngest son Puneeth Rajkumar in a minor role of an infant. 

==Cast==
  Rajkumar as Manohar
* Aarathi as Sita
* Jayamala as Kumada 
* Vajramuni
* Rajashankar Balakrishna
* Sampath
* Thoogudeepa Srinivas as Chandu
* Bhatti Mahadevappa
* Joker Shyam
* Mallesh
* Shani Mahadevappa
* Shivaji Rao
* Venkataraju
* G. Shivanand 
* Rajanand 
* Baby Poornima Rajkumar as Shoba Master Lohith as Shobha
* Baby Rajeshwari
* Baby Jaishanthi
* Ramadevi
* B. Jaya (actress)|B. Jaya
* Rathnamala
* Jayamma
* Pushpa
* Rajani
 

==Soundtrack==
{{Infobox album
| Name        = Premada Kanike
| Type        = Soundtrack
| Artist      = Upendra Kumar
| Cover       = Premada Kanike album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1976
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Upendra Kumar composed the music for the soundtracks and lyrics were penned by Chi. Udaya Shankar and Vijaya Narasimha.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Putta Putta
| lyrics1 = Chi. Udaya Shankar
| extra1 = S. Janaki
| length1 = 
| title2 = Baanigondu Elle Ellide
| lyrics2 = Chi. Udaya Shankar Rajkumar
| length2 = 
| title3 = Chinna Endu Naguthiru
| lyrics3 = Chi. Udaya Shankar
| extra3 = P. B. Sreenivas
| length3 = 
| title4 = Idu Yaaru Bareda Katheyo
| lyrics4 = Chi. Udaya Shankar
| extra4 = Rajkumar
| length4 = 
| title5 = Naguveya Henne Naanu
| lyrics5 = Vijaya Narasimha
| extra5 = Rajkumar, H. P. Geetha
| length5 = 
| title6 = Na Bidalare Endu Ninna
| lyrics6 = Vijaya Narasimha
| extra6 = Rajkumar, Vani Jairam
| length6 = 
}}

==Awards==

*Karnataka State Film Awards -
 Best Film
# Best Dialogue writer - Chi. Udaya Shankar Best Editing - P. Bhaktavatsalam Best Child Actress - Baby Poornima Rajkumar

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 