The Mummy (1911 film)
 
 
{{Infobox film
| name           = The Mummy
| image          = File:The_Mummy_Thanhouser.jpg
| caption        = Surviving film still
| director       = 
| producer       = Thanhouser Company
| writer         = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        = 1 reel
| country        = United States 
| language       = Silent English intertitles
| budget         = 
}}
 silent Short short film produced by the Thanhouser Company. The film details the story of Jack Thornton, a businessman, who is in love with Professor Dixs daughter. Jack purchases a mummy and plans to win his respect as an Egyptologist, but the mummy is reanimated in Jacks room by a live electrical wire. The mummy takes immediate interest in Jack, but is rejected and mummifies him. Before Professor Dix can cut up the now-mummified Jack, she returns and saves him. Jack explains everything and the film concludes with Professor Dix marrying the mummy. The production was one of several films of the same name produced in 1911 and was met with favorable reviews. The film is presumed to be Lost film|lost.

== Plot ==
The original synopsis of the film was published in the   himself. To start his collection, he purchases a mummy at an auction sale, and takes it home expecting that later he can make a great hit with his sweethearts father, by presenting it to him as a gift. While the mummy is in Jacks room, a live electric wire is by accident brought in contact with it. The body has been so perfectly mummified, that the electric current is all that is necessary to ignite the vital spark, and Jack is amazed to see dancing forth from the case which he thought contained only unattractive rags and bones, a beautiful Egyptian princess. As soon as she is released, the mummy makes violent love to Jack, and causes his sweetheart to quarrel with him (for how can a plain businessman explain the presence in his room of a beautiful barbarian?). When her love is spurned, the visitor from the distant past avenges herself by having Jack made into a mummy and placed in the case in her stead. Her heart relents, however, in time to save him from being cut up by the professor, who with the sharp knife, starts to investigate the contents of the mummy case. But all ends happily when Jacks plain statements of the seemingly impossible facts are proved true by the professor. Jack is reunited to his sweetheart, and the professor, being a widower, also an ardent admirer of everything antique, leads the recreated Egyptian lady to the altar, in spite of the fact that there is a difference of several thousand years in their ages."   

== Cast ==
*William Garwood as Jack   
*Harry Benham  likely as Professor Dix

== Production ==
 William Russell George Middleton, Grace Moore, John W. Noble, Anna Rosemond, Mrs. George Walters. 

The film was given the production code number 191 and had a code word of "Mum".   A catalog listed the films length at 995 feet.   The films expected run-time was fifteen minutes and was billed by the Thanhouser Company as having many novelties and being of a comedic nature.   It was released on March 7, 1911 and listed as a drama by the Moving Picture World.   It was distributed by the Motion Picture Distributing and Sales Company.    The production came during a time of renewed interest in Egyptology in which Pathé and Urban Films would release their own films titled The Mummy. This would be followed by Essanay Studios in 1912 with the release of When Soul Meets Soul and Gaumont Film Company  The Vengeance of Egypt.  The film had a unique special effect in which the bodies of the cast dissolve and take an aerial flight to Egypt at the end of the film.   

== Reception ==
The film has had known viewings across several states, including Wisconsin,  Pennsylvania,   and Kansas.  An advertisement for the Lyric Theater in Indiana noted the films debut, but unambiguously included notes for "Miss Hawthorne" and "Dot Washburn" on the bill. Both Miss Hawthorne and Dot Washburn were not credited in the film in any source and were likely other acts in part of the theaters bill for March 7, 1911. 

A review in the Moving Picture World confirmed that the novelty of a "mummy walking out of a case as an Egyptian Princess is sufficiently unusual to create interest, and this interest is increased when, after the young man has spurned her love, she forces him into the case and he becomes a mummy."   Another review more generically reflected on the production as being "exceptionally good as to photography, acting, staging" alongside other productions.     Three other reviews in the The Billboard, The New York Dramatic Mirror and The Morning Telegraph were all positive with emphasis on the special effects. 

Pantelis Michelakis and Maria Wykes book The Ancient World in Silent Cinema provides additional nuance in noting the films erotic underpinnings in which the past is bridged to the present through marriage to the re-animated Egyptian princess.  The film has erroneously been claimed to be the earliest "Mummy"-themed film, but The Monster Book notes films including Robbing Cleopatras Tomb from 1899 and La Momie du roi in 1909 as earlier examples.   The film is presumed to be Lost film|lost.

==References==
 

 
 
 
 
 
 