Rosie Dixon – Night Nurse
{{Infobox film
| name           = Rose Dixon – Night Nurse
| image          = File:Rosie Dixon-Night Nurse.jpg|thumb|Rosie Dixon – Night Nurse poster
| caption        = 
| director       = Justin Cartwright
| producer       =  Christopher Wood Justin Cartwright
| based on = novel by Christopher Wood
| starring       = Beryl Reid John Le Mesurier Arthur Askey Debbie Ash
| music          = Ed Welch
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1978
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = £300,000 
}} British comedy Christopher Wood.

A new student nurse arrives at a hospital, attracting interest from the staff with comedic consequences. The film was one of several soft sex comedies released in the 1970s to cash in on the success of the Confessions series (also written by Wood under the pseudonym Timothy Lea). Like the Confessions films it was adapted from a book, the authors credit going to the fictional Rosie herself. Nine Rosie Dixon novels were published, but only the first was adapted into a movie. The character of Penny Sutton&nbsp;– Rosies best friend in the movie and in the books&nbsp;–  was the star of an earlier series of similar novels that depicted Penny as an airline stewardess.

The film stars Debbie Ash, better known as one of the dance troupe Hot Gossip, along with her sister Leslie Ash (later a TV star in her own right) as Rosies sister Natalie.

==Cast==
{{columns-list|2|
* Beryl Reid  – Matron
* John Le Mesurier – Sir Archibald MacGregor
* Arthur Askey  – Mr. Arkwright
* Debbie Ash  – Rosie Dixon
* Liz Fraser  – Mrs. Dixon
* Lance Percival  – Jake Fletcher
* John Junkin  – Mr. Dixon
* Bob Todd  – Mr. Buchanan
* Carolyne Argyle – Penny Green
* Jeremy Sinden – Dr. Robert Fishlock
* Christopher Ellison – Dr. Adam Quint
* Peter Mantle – Dr. Tom Richmond
* Ian Sharp – Dr.Seamus MacSweeney
* Leslie Ash – Natalie Dixon
* David Timson – Geoffrey Ramsbottom
* John Clive – Grieves
* Patricia Hodge – Sister Belter
* Peter Bull – August Visitor
* Glenna Forster-Jones – Staff Nurse Smythe
* Harry Towb – Mr. Phillips
* Joan Benham – Sister Tutor
* Sara Pugsley – Night Sister
* Jon Lingard-Lane – Traction Patient / Barnabus Medic
* Claire Davenport – Mrs. Buchanan
}}

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 