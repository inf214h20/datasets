Big Time (1929 film)
{{Infobox film
| name           = Big Time
| image          = 
| image_size     = 
| caption        = 
| director       = Kenneth Hawks
| producer       =  Wallace Smith (story) Sidney Lanfield William K. Wells
| narrator       = 
| starring       = Lee Tracy Mae Clarke
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| studio         = Fox Film Corporation
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Big Time is a 1929 film starring Lee Tracy and Mae Clarke as a show business couple who break up over his infidelity. This was Clarkes film debut. Director Kenneth Hawks was Howard Hawks brother.

==Cast==
*Lee Tracy as Eddie Burns
*Mae Clarke as Lily Clark
*Daphne Pollard as Sybil
*Josephine Dunn as Gloria
*Stepin Fetchit as Eli

Director John Ford had a cameo as himself.

==External links==
*  

 
 
 
 
 


 