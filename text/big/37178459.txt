The Exciters (film)
{{infobox film
| name           = The Exciters
| image          =
| imagesize      =
| caption        =
| director       = Maurice Campbell
| producer       = Adolph Zukor Jesse Lasky John Colton (scenario)
| based on       =  
| starring       = Bebe Daniels George Webber
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States
| language       = Silent (English intertitles)

}} lost 1923 American silent film comedy produced by Famous Players-Lasky and distributed through Paramount Pictures. It is based on a 1922 Broadway play, The Exciters, by Martin Brown. This film was directed by Maurice Campbell, husband of stage star Henrietta Crosman, and stars Bebe Daniels, then a popular Paramount contract star. 

On the Broadway stage, Bebe Danielss role was played by Tallulah Bankhead. 

==Cast==
*Bebe Daniels - Ronnie Rand
*Antonio Moreno - Pierre Martel
*Burr McIntosh - Rackam, The Lawyer
*Diana Allen - Ermintrude
*Cyril Ring - Roger Patton
*Bigelow Cooper - Hilary Rand
*Ida Darling - Mrs. Rand
*Jane Thomas - Della Vaughn
*Allan Simpson - The Mechanician
*George Backus - The Minister
*Henry Sedley - Gentleman Eddie
*Erville Alderson - Chloroform Charlie
*Tom Blake - Flash

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 