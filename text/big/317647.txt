Tomorrow Never Dies
 
 
 
{{Infobox film
| name = Tomorrow Never Dies
| image = Tomorrow Never Dies (UK cinema poster).jpg
| caption = British cinema poster for Tomorrow Never Dies, by Keith Hamshere and George Whitear
|alt=A man wearing evening dress holds a gun. On his sides are a white woman in a white dress and an Asian woman in a red, sparkling dress holding a gun. On the background are monitors with scenes of the film, with two at the top showing a man wearing glasses holding a baton. On the bottom of the screen are two images of the 007 logo under the title "Tomorrow Never Dies" and the film credits.
| director = Roger Spottiswoode
| producer = Michael G. Wilson Barbara Broccoli
| writer = Bruce Feirstein
| based on =  
| starring = Pierce Brosnan Jonathan Pryce Michelle Yeoh Teri Hatcher Joe Don Baker Judi Dench
| music = David Arnold
| cinematography = Robert Elswit
| editing = Michel Arcand Dominique Fortin
| composer = Sheryl Crow Mitchell Froom
| performer = Sheryl Crow
| studio = Eon Productions
| distributor = United Artists United International Pictures (UK)
| released =  
| runtime = 119 minutes
| country = United Kingdom
| language = English
| budget = $110 million
| gross = $333,011,068
}} MI6 agent James Bond. Directed by Roger Spottiswoode, with the screenplay written by Bruce Feirstein, the film follows Bond as he attempts to stop a power-mad media mogul from engineering world events to initiate World War III.

The film was produced by Michael G. Wilson and Barbara Broccoli, and was the first James Bond film made after the death of producer Albert R. Broccoli, to whom the movie pays tribute in the end credits. Filming locations included France, Thailand, Germany, Mexico and the United Kingdom. Tomorrow Never Dies performed well at the box office and earned a Golden Globe nomination despite mixed reviews. While its performance at the domestic box office surpassed that of its predecessor, GoldenEye,  it was the only Pierce Brosnan Bond film not to open at number one at the box office, as it opened the same day as Titanic (1997 film)|Titanic, but instead at number two. 

==Plot== MI6 sends James Bond, GPS encoder made by the U.S. military. Despite M (James Bond)|Ms insistence to let 007 finish his reconnaissance, British Admiral Roebuck launches a missile attack on the arms bazaar. Bond then discovers two Soviet nuclear torpedoes mounted on an L-39 Albatros, but because the missile is too far along to be aborted, 007 hijacks the L-39 and flies away seconds before the bazaar is struck. Amidst the confusion, Gupta escapes with the encoder.
 Media baron Elliot Carver, head of the Carver Media Group Network (CMGN), soon begins his plans to use the encoder to provoke war between China and the United Kingdom, hoping to replace the current Chinese government with one more supportive to Carvers plans of exclusive broadcast rights in their country. Meaconing the GPS signal using the encoder, Gupta sends a British frigate, the HMS Devonshire, off-course into Chinese-held waters in the South China Sea, where Carvers stealth ship, commanded by List of James Bond henchmen in Tomorrow Never Dies#Mr. Stamper|Mr. Stamper, sinks the frigate with a sea drill and steals one of its missiles, while shooting down a Chinese Chengdu J-7|J-7 fighter jet sent to investigate the British presence, and killing off the Devonshires survivors with Chinese weaponry. After reading a CMGN report of the incident as a Chinese attack, a government minister orders Roebuck to deploy the British Fleet to recover the frigate, and possibly retaliate, while leaving M only forty-eight hours to investigate its sinking.

M sends Bond to investigate Carver, due to Carver Media releasing their news articles with critical details hours before the events had become known, along with MI6 noticing a spurious signal from one of his CMGN communications satellites when the frigate was sunk. Bond travels to Hamburg and seduces Carvers wife, Paris, an ex-girlfriend, to get information that would help him enter Carvers newspaper headquarters. After Bond steals back the GPS encoder, Carver orders Paris and Bond killed. Paris is killed by Dr. Kaufman, Mr. Stampers teacher on Chakra Torture, but Bond shoots Kaufman and escapes, protecting the encoder. Bond, after visiting the Americans and learning that the encoder had been tampered with, goes to the South China Sea to investigate the wreck (which was actually in Vietnamese waters), discovering one of its cruise missiles missing. He and Wai Lin, a Chinese spy on the same case, after avoiding being trapped in the sunk ship, are captured by Stamper and taken to the CMGN tower in Ho Chi Minh City, but soon escape and decide to both collaborate on the investigation.
 HMS Bedford searches for them.

==Cast== James Bond, MI6 agent 007. psychopathic media mogul who plans to provoke global war in order to boost sales and ratings of his news divisions.
* Michelle Yeoh as Colonel Wai Lin, a skilled Chinese spy and Bonds ally.
* Teri Hatcher as Paris Carver, a former girlfriend of Bond who is now Carvers trophy wife.
* Götz Otto as Richard Stamper, Carvers henchman, who is skilled in the art of Chakra torture.
* Ricky Jay as Henry Gupta, an American "Techno-terrorist" in the employ of Carver. Bruce Feirstein said he named this character after a Gupta Bakery, which he passed on the way to the studios.  Jack Wade, CIA liaison, reprising his role from GoldenEye.
* Vincent Schiavelli as Dr. Kaufman, a professional assassin used by Elliot Carver.
* Judi Dench as M (James Bond)|M, reprising her role from GoldenEye.
* Desmond Llewelyn in his penultimate appearance as Q (James Bond)|Q.
* Samantha Bond as Miss Moneypenny.
* Daphne Deckers as PR person of Carver Media Group Network. Geoffrey Palmer as Admiral Roebuck, Ms contentious military contact. Charles Robinson, Ms Chief of Staff.
* Julian Fellowes as the British Minister of Defence, who orders Admiral Roebuck to send the fleet to the China Sea. He is the successor to Sir Frederick Gray (Geoffrey Keen).
* Cecilie Thomsen as Professor Inga Bergstrom.
* Gerard Butler and Julian Rhind-Tutt as crewmen of HMS Devonshire. Michael Byrne as Admiral Kelly, commander of the Royal Navy task force sent to the South China Sea.

==Production==
Following the success of  , who had previously been involved with the series production since its beginning. The rush to complete the film drove the budget to $110 million.   The producers were unable to convince Martin Campbell, the director of GoldenEye, to return; his agent said that "Martin just didnt want to do two Bond films in a row." Instead, Roger Spottiswoode was chosen in September 1996.  Spottiswoode said he had previously offered to direct a Bond film while Timothy Dalton was still in the leading role. 

===Writing=== transfer of sovereignty to China; however, this plot could not be used for a film opening at the end of the year, so they had to start "almost from scratch at T-minus zero!"   
 treatment written by Donald E. Westlake, although how much of Westlakes material remains is unknown.  Bruce Feirstein, who had worked on GoldenEye, penned the initial script. Feirstein claimed that his inspiration was his own experience working with journalism, stating that he aimed to "write something that was grounded in a nightmare of reality."  Feirsteins script was then passed to Spottiswoode who reworked it. He gathered seven Hollywood screenwriters in London to brainstorm, eventually choosing Nicholas Meyer to perform rewrites.  The script was also worked on by Dan Petrie, Jr. and David Campbell Wilson before Feirstein, who retained the sole writing credit, was brought in for a final polish.   While many reviewers compared Elliot Carver to Rupert Murdoch, Feirstein based the character on Robert Maxwell. There is a reference to the moguls death when M instructs Moneypenny to issue a press release stating that Carver died “falling overboard on his yacht."   

Wilson stated, "We didnt have a script that was ready to shoot on the first day of filming", while Pierce Brosnan said, "We had a script that was not functioning in certain areas."  The Daily Mail reported on arguments between Spottiswoode and the producers with the former favouring the Petrie version, but the latter reinstating Feirstein to rewrite it two weeks before filming was due to begin. They also said that Jonathan Pryce and Teri Hatcher were unhappy with their new roles, causing further re-scripting.   

The title was inspired by  ed to MGM. But through an error this became Tomorrow Never Dies, a title which MGM found so attractive that they insisted on using.  The title was the first not to have any relation to Flemings life or work. 

===Casting===
Teri Hatcher was three months pregnant when shooting started, although her publicist stated the pregnancy did not affect the production schedule.  Hatcher later regretted playing Paris Carver, saying "Its such an artificial kind of character to be playing that you dont get any special satisfaction from it."  Actress Sela Ward auditioned for the role, but lost out, reportedly being told the producers wanted her, but ten years younger.  Hatcher was seven years Wards junior. According to Brosnan, Monica Bellucci also screen tested for the role but as Brosnan remarked, "the fools said no."   

The role of Elliot Carver was initially offered to Anthony Hopkins (who also had been offered a role in GoldenEye), but he rejected it.      

Natasha Henstridge was rumoured as cast in the lead Bond Girl role,  but eventually, Yeoh was confirmed in that role. Brosnan was impressed, describing her as a "wonderful actress" who was "serious and committed about her work".  She reputedly wanted to perform her own stunts, but was prevented because director Spottiswoode ruled it too dangerous and prohibited by insurance restrictions.  

When Götz Otto was called in for casting, he was given twenty seconds to introduce himself; his hair had recently been cropped short for a TV role. Saying, "Im big, Im bad, and Im German", he did it in five. 

===Filming===
  car with a steering wheel on the back seat, seen at an exhibition at Museum Industriekultur, Nuremberg.|alt=Side view of a vehicle with its doors open. Behind the left front seat can be seen a steering wheel and monitor.]]
 The Man with the Golden Gun.  

Spottiswoode tried to innovate in the action scenes. Since the director felt that after the tank chase in GoldenEye he could not use a bigger vehicle, a scene with Bond and Wai Lin in a BMW motorcycle was created. Another innovation was the remote-controlled car, which had no visible driver – an effect achieved by adapting a BMW 750i to put the steering wheel on the back seat.  The car chase sequence with the 750i took three weeks to film, with Brent Cross car park being used to simulate Hamburg – although the final leap was filmed on location.    A stunt involving setting fire to three vehicles produced more smoke than anticipated, causing a member of the public to call the fire brigade.  The upwards camera angle filming the HALO jump created the illusion of having the stuntman opening its parachute close to the water. 

During filming, there were reports of disputes on set. The Daily Mail reported that Spottiswoode and Feirstein were no longer on speaking terms and that crew members had threatened to resign, with one saying "All the happiness and teamwork which is the hallmark of Bond has disappeared completely."  This was denied by Brosnan who claimed "It was nothing more than good old creative argy-bargy",  with Spottiswoode saying "It has all been made up...Nothing important really went wrong."  Spottiswoode did not return to direct the next film; he said the producers asked him, but he was too tired.  Apparently, Brosnan and Hatcher feuded briefly during filming due to her arriving late onto the set one day. The matter was quickly resolved though and Brosnan apologised to Hatcher after realising she was pregnant and was late for that reason. 
 P5 was Quantum of Solace in 2008.

===Music===
 
 From Russia Filmtracks describing it as "an excellent tribute to the entire series of Bond score".   
 Don Black Saint Etienne, Help The Aged".

==Release and reception==
The film had a World Charity Premiere at  , which grossed almost $20 million more. 

The critical reception of the film was mixed, with the film review collection website Rotten Tomatoes giving it a 57% rating,    and similar site Metacritic rating it at 56%.    In the Chicago Sun-Times, Roger Ebert gave the film three out of four-stars, saying "Tomorrow Never Dies gets the job done, sometimes excitingly, often with style" with the villain "slightly more contemporary and plausible than usual", bringing "some subtler-than-usual satire into the film".  James Berardinelli described it as "the best Bond film in many years" and said Brosnan "inhabits his character with a suave confidence that is very like Sean Connery|Connerys."  However, in the Los Angeles Times, Kenneth Turan thought a lot of Tomorrow Never Dies had a "stodgy, been-there feeling", with little change from previous films,  and Charles Taylor wrote for Salon.com that the film was "a flat, impersonal affair". 
 MPSE Golden Golden Reel BMI Film Music Award. 
 

The original UK release received various cuts to scenes of violence and martial arts weaponry, and to reduce the impact of sound effects, in order to receive a more box-office-friendly 12 certificate. Further cuts were made to the video/DVD release to retain this rating. These edits were restored for the Ultimate Edition DVD release in the UK, which was consequently upgraded to a 15 certificate. 

==Appearances in other media==
  John Gardners You Only Pinkertons Detective Agency, which is thus exclusive to the literary series. Subsequent Bond novels by Benson were affected by Tomorrow Never Dies, specifically Bonds weapon of choice being changed from the Walther PPK to the Walther P99. Benson said in an interview that he felt Tomorrow Never Dies was the best of the three novelisations he wrote. 
 Tomorrow Never Black Ops and published by Electronic Arts on 16 November 1999. Game Revolution described it as "really just an empty and shallow game",  and IGN said it was "mediocre". 

==See also==
* Outline of James Bond
* Sea Shadow (IX-529), the boat that inspired the design of Carvers stealth boat      
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 