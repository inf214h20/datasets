Manèges
{{Infobox film
| name           = Manèges
| image          = Manegesposter.jpg
| image_size     =
| caption        = Manèges French release poster
| director       = Yves Allégret
| producer       = Émile Natan André Paulvé
| writer         = Jacques Sigurd
| starring       = Simone Signoret Bernard Blier Jane Marken
| music          = &mdash;
| cinematography = Jean Bourgouin
| editing        = Suzanne Girardin Maurice Serein 
| distributor    = DisCina International Films
| released       = 25 January 1950
| runtime        = 91 min.
| country        =   French
| budget         =
}}
 French film flashback and misogynistic in tone.

The film was released in English-speaking markets under various titles, including The Wanton, The Cheat and The Riding School.

==Plot==
Robert (Bernard Blier), a riding school owner, and his wife Dora (Simone Signoret) have a seemingly happy marriage until Dora is critically injured in a road accident.  Robert and Doras mother (Jane Marken) rush to the hospital to which she has been taken.  Believing she is about to die, Dora spitefully asks her mother &ndash; a vulgar, tawdry and heavy-drinking woman &ndash; to put Robert in the picture regarding the true nature of the marriage, so that she can die gloating over his distress.

As Dora is taken to the operating theatre, her mother takes vicious pleasure in informing Robert &ndash; who has been reflecting with sadness about happy times and the prospect of losing his wife &ndash; that the marriage has been a sham from the start.  Via a series of flashbacks, it is revealed that Dora is a Psychological manipulation|manipulative, conniving and amoral gold-digger.  Encouraged by her equally unprincipled mother, she set out to snare Robert purely for access to his finances in order that she (and her mother, to whom she has siphoned off significant amounts of money) could live a life of ease and outward respectability.  In fact she has always despised Robert, mocking his unsuspicious nature and gullibility while amusing herself with a string of lovers.  With the riding school business recently failing, Dora had decided that she had taken Robert for as much as she could, and had been planning to leave him for François, a richer lover who could further her social-climbing ambitions.

News comes through from the operating theatre that Dora will live, but is permanently paralysed.  Robert states his intention to abandon her to her fate, leaving the mother alone with the prospect of having to care for her disabled daughter in straitened financial circumstances. 

==Cast==
*  Simone Signoret as Dora
*  Bernard Blier as Robert
*  Jane Marken as Doras mother (unnamed in the film)
*  Franck Villard as François
*  Jacques Baumer as Louis
*  Mona Dol as Head Nurse
*  Jean Ozenne as Éric
*  Gabriel Gobin as Émile

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 