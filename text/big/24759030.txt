BirdWatchers
{{Infobox Film
| name           = BirdWatchers
| image          = Birdwatchers-film.jpg
| caption        = 
| director       = Marco Bechis
| producer       = Amedeo Pagani Marco Bechis Fabiano Gullane Caio Gullane
| writer         = Marco Bechis Luiz Bolognesi
| starring       = Claudio Santamaria Alicélia Batista Cabreira Chiara Caselli Andrea Guerra
| cinematography = Hélico Alemão Nagamine
| editing        = Jacopo Quadri
| studio         = Classic Rai Cinema Karta Film Gullane Filmes
| distributor    = 01 Distribution (Italy) Paris Filmes (Brazil)
| released       =  
| runtime        = 104 minutes
| country        = Italy Brazil Guarani English
| budget         = €3 million   
| gross          = $7,263 
}}

BirdWatchers ( ) is a 2008 Brazilian-Italian drama film directed by Marco Bechis. It depicts the breakdown of a community of Guarani-Kaiowa native Indians whilst attempting to reclaim their ancestral land from a local farmer.

==Cast==
*Claudio Santamaria as Roberto
*Alicélia Batista Cabreira as Lia
*Chiara Caselli as Beatrice
*Abrísio da Silva Pedro as Osvaldo
*Ademilson Concianza Verga as Irineu
*Ambrósio Vilhalva as Nádio
*Matheus Nachtergaele as Dimas
*Fabiane Pereira da Silva as Maria
*Eliane Juca da Silva as Mami
*Nelson Concianza as Nhanderu
*Leonardo Medeiros as Moreira
*Inéia Arce Gonçalves as maid
*Poli Fernandez Souza as Tito
*Urbano Palácio as Josimar

==Production==
The film was shot in Dourados, Mato Grosso do Sul  over ten weeks in 2007 with a cast whose most members had never acted before and without use of a script. 

The songs Sacris solemnis and O gloriosa virginum were composed by the Italian Jesuit Domenico Zipoli who went to South America to live at the Jesuit Reductions.

==Awards== One World Media Award in the Drama category, by unanimous vote of the jury. 

==See also==
* Survival International

== References ==
 

==External links==
*  
*   - Guarani fund launched with an appeal at the end of the film
*  
*  , retrieved 11/09/09

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 