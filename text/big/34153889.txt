Gandu Bherunda
{{Infobox film
| name           = Gandu Bherunda
| image          = 
| caption        = 
| director       = Rajendra Singh Babu
| producer       = H. N. Muddu Krishna
| screenplay     = Rajendra Singh Babu
| story          = H. V. Subba Rao
| based on       =  
| starring       = Vajramuni Amrish Puri Srinath Ambarish Shankar Nag
| narrator       = 
| music          = Chellapilla Satyam
| cinematography = P. S. Prakash
| editing        = K. Balu
| studio         = Anjanadri Pictures
| distributor    = Parijatha Venkateshwara Pictures
| released       =  
| runtime        = 
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Lakshmi and Jaimala appearing in supporting roles.
 American film epigraphist and a geologist on a hunt for an ancient treasure in an island "Hasta Dweepa" (Palm Island), from the then 700-year old Bherunda kingdom. The film is said to be ahead of its time of release and is considered "technically brilliant". However, it failed to perform commercially at the time. 

==Plot==
The films opens to a visual narration with a series of ancient paintings, of a wealthy kingdom named Bherunda that existed around 700 years ago at the erstwhile Mysore State (present-day Karnataka), ruled by king Aditya Devanarayana. As a battle with the kingdoms enemies approached, he ordered that all the kingdoms wealth be transported and stored in a faraway island, in a cave. Having won the battle, the enemies returned empty-handed. But, the location of the wealth remained a mystery for centuries.

Mark Abraham (Vajramuni), an epigraphist and linguistic expert, stumbles upon the writings of an explorer about the treasure of the Bherunda kingdom and determines to retrieve it, and is assisted by his student Myna (Jaimala (actress)|Jaimala). He is joined by Jai, a geologist, who breaks the news to Abraham of having the located the spot of the wrecks of the ship the carried the treasure to the island "Hasta Dweepa". They embark on a trip to retrieve the map of the cave, and Abraham is seen killing Jai assisted by Myna, after they retrieve a medal from the wreck.

Raju (Srinath) and Bijju (Ambarish) are brothers working for Abraham, living with their little brother Chanda. Promising them of their share from the treasure, they embark to the island that holds the treasure. But, keen to not share the wealth with anybody, he kills Chanda and other crew on the boat and wounds the brothers, before leaving the island. Raju and Bijju are tended to by Nayar (Shankar Nag), an islander living with his dog, Manja. The trio return to the mainland and start living together leading a good life, but still holding a grudge toward Abraham. Two years later, on a day at work, they unearth a trunk containing an ancient vase, what proves to be treasure of Bherunda. Keenly following the developments is Jai, who had escaped death at the loss of his left arm. He insists that they join him in retrieving the treasure before Abraham does, to which they agree. Jai plants his sister Champa (Lakshmi (actress)|Lakshmi) in Abrahams team to retrieve the map. Abrahams team embark to the island, tailed by Jais team. Champa, in hurry to escape with the map, throws it into the sea and diving, and loses the map. She is helped to the Jais boat by Raju and Bijju and decide to stay ashore the night, when their boat is destroyed by Abrahams men.

Once on the island, Nayar breaks to the team that the island is the same to which the brothers had arrived with Abraham two years ago. Abraham arrives with his team, having killed his crew and assistants but Myna, who he stabs in the stomach on Jais insistence, so that they share the wealth, prior to which latter ditched the brothers and his sister, and killing Nayar. Jai and Abraham reach the spot and enter the cave to find large quantity of wealth in the form of gold. The brothers enter the cave immediately afterwards with Champa, and a fight ensues between the brothers on one side and Jai and Abraham on the other. Bombed with explosives by Jai in the ensuing fight, all get killed but for Bijju and Champa, who leave the cave with the wealth untouched.

== Cast ==
 
* Srinath as Raju
* Ambarish as Bijju
* Shankar Nag as Nayar
* Vajramuni as Mark Abraham
* Amrish Puri as Professor Jai Lakshmi as Champa Jaimala as Myna
* Jayamalini
* Lakshmi Janardhana
* Baby Rekha Balakrishna as James Robert
* Udaykumar
* Dinesh as Taggappa Sharanappa Menshinkaayi, assistant to Abraham
* Sunder Krishna Urs as Babu, Menshinkaayis son
* M. S. Umesh, Abrahams assistant
* Nandagopal
* Tiptur Siddaramaiah
* Borkar
* Surendra
* Peter
 

== Production ==
The film was based on the 1969 film Mackennas Gold. It is the first Kannada film to be shot underwater. The film ran into trouble during its production and filming stages, and took years to be completed before releasing it in 1984. 

==Soundtrack==
{{Infobox album
| Name        = Gandu Bherunda
| Type        = Soundtrack
| Artist      = Chellapilla Satyam
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The music for the soundtracks was composed by Chellapilla Satyam, with lyrics written by Chi. Udaya Shankar, R. N. Jayagopal and Doddarange Gowda. The album consists five soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Gaganake Soorya 
| lyrics1 = Chi. Udaya Shankar
| extra1 = P. B. Sreenivas, S. P. Balasubrahmanyam
| length1 = 
| title2 = Hey Enchini Maaraayre
| lyrics2 = R. N. Jayagopal
| extra2 = S. Janaki
| length2 = 
| title3 = Novu Thumba Novu	
| lyrics3 = Doddarange Gowda
| extra3 = S. Janaki
| length3 = 
| title4 = Baare Baare Nannavale
| lyrics4 = R. N. Jayagopal
| extra4 = S. P. Balasubrahmanyam, S. Janaki
| length4 = 
| title5 = Bairunda Bairunda	
| lyrics5 = 
| extra5 = S. P. Balasubrahmanyam, S. Janaki
| length5 = 
}}

==References==
 

 

 
 
 
 
 