Heiter bis Wolkig
{{Infobox film
 | name = Heiter bis Wolkig
 | image = 
 | director = Marco Petry
 | writer = Axel Staeck	 	
 | starring = Max Riemelt, Elyas MBarek Jessica Schwarz Anna Fischer
 | distributor = Constantin Film
 | budget =
 | released =  
 | runtime = 
 | language = German
}}
Heiter bis Wolkig is a 2012 German comedy-drama film directed by Marco Petry and starring Max Riemelt, Elyas MBarek, Jessica Schwarz and Anna Fischer. 

==Plot==
Tim and his friend Can (Elyas M’Barek) go to bars and pretend that one is looking for a date for his friend, who is supposedly deathly ill, and needs one last roll in the hay. Thus Tim (Max Riemelt) meets Marie (Anna Fischer) and they fall in love. Marie must care for her older, ill sister Edda (Jessica Schwarz) who spends a lot of time in bed awaiting death. Marie still believes that Tim has fatal cancer, but Edda (who can judge from her own experience) knows that Tim is putting on an act. 

==Cast==
 
*Max Riemelt as Tim
*Elyas MBarek as Can
*Jessica Schwarz as Edda
*Anna Fischer as Marie
*Dieter Tappert as Paul
*Johann von Bülow as Dr. Seibold
*Stephan Luca as Thomas
*Johannes Kienast as Holger

== References ==
 

==External links==
*  

 
 
 
 
 


 
 