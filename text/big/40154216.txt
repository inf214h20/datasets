The Turning (2013 film)
{{Infobox film
| name           = The Turning
| image          = The Turning poster.jpg
| caption        = 
| director       = The Turning Ensemble
| producer       = Robert Connolly Maggie Miles The Turning Ensemble
| writer         = Tim Winton
| starring       = Cate Blanchett   Rose Byrne   Hugo Weaving   Miranda Otto 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 180 minutes
| country        = Australia
| language       = English
| budget         = 
}}
 short stories by Tim Winton. It premiered at the Melbourne International Film Festival on 3 August 2013.  It was nominated for the 2013 Asia Pacific Screen Award for Best Feature Film,  and was screened in the Berlinale Special Galas section of the 64th Berlin International Film Festival.   

==Production==
18 directors worked on the film, including Mia Wasikowska, David Wenham, and Stephen Page who made their directorial debuts.  Initially, Cate Blanchett also intended to direct before switching to an acting role, with Simon Stone taking her place behind the camera. 

==Directors==
* Jonathan auf der Heide ("Fog")
* Tony Ayres 	 ("Cockleshell")
* Simon Stone    	 ("Reunion")
* Jub Clerc	 	 ("Abbreviation")
* Robert Connolly	 ("Aquifer")
* Shaun Gladwell	 ("Family")
* Rhys Graham	 ("Small Mercies")
* Justin Kurzel	 ("Boner McPharlins Moll")
* Yaron Lifschitz	 ("Immunity")
* Anthony Lucas	 ("Damaged Goods")
* Claire McCarthy	 ("The Turning")
* Ian Meadows	 ("Defender")
* Ashlee Page	 ("On Her Knees")
* Stephen Page	 ("Sand")
* Warwick Thornton	 ("Big World")
* Marieka Walsh	 ("Ash Wednesday")
* Mia Wasikowska	 ("Long, Clear View")
* David Wenham	 ("Commission")

==90-minute broadcast version==
The Turning was heavily cut down and rearranged for broadcast on   content.

==Reception==
The Turning received positive reviews from critics and audiences, earning an approval rating of 88% on Rotten Tomatoes, based on 24 reviews. 

===Awards and nominations===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (3rd AACTA Awards|3rd)  AACTA Award Best Film Robert Connolly
| 
|- Maggie Miles
| 
|- The Turning Ensemble
| 
|- AACTA Award Best Direction
| 
|- AACTA Award Best Adapted Screenplay
| 
|- AACTA Award Best Actor Hugo Weaving
| 
|- AACTA Award Best Actress Rose Byrne
| 
|- AACTA Award Best Supporting Actress Mirrah Foulkes
| 
|- AACTA Award Best Editing The Turning Ensemble
| 
|-
|}

==References==
{{reflist|refs=

   

   

}}

==External links==
*  
*  
*  

 

 
 
 
 
 
 