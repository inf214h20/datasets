Sand Wars
 
{{Infobox film
| name = Sand Wars|
| image = Poster_sand_wars.jpg
| director = Denis Delestrac
| writer = Denis Delestrac
| narrator = Trevor Hayes
| producer = Guillaume Rappeneau Laurent Mini Nathalie Barton
| studio = ARTE France
| editing = Michelle Hollander Seamus Haley Ibon Olaskoaga
| music = Jean-Sébastien Robitaille
| cinematography = Marc Martinez Alam Raja
| distributor = PBS International Green Planet Films
| released =  
| runtime = 75 minutes
| country = France Canada
| language = English French
}}
Sand Wars is an epic eco-thriller by director Denis Delestrac and produced by Rappi Productions, La Compagnie des Taxi-Brousse, InfomAction, Arte France, with the support of The Santa Aguila Foundation.

The film premiered at the Cinéma Publicis on the Champs-Elysées in 2013 and was first broadcast in May in France and Germany (Arte), where it became the highest rated documentary for 2013.  A Gold Panda Award winner, Sand Wars is distributed worldwide by PBS International.

Among many other outreach victories, the film inspired the United Nations Environment Programme (UNEP) to publish a Global Environmental Alert in March 2014 titled "Sand, rarer than one thinks". 

==Synopsis==
By the end of the 21st century, beaches will be a thing of the past. That is the alarming forecast of a growing number of scientists and environmental NGOs. Sand has become a vital commodity for our modern economies: we use it in our toothpaste, detergents, and cosmetics, and computers and mobile phones couldn’t exist without it. Our houses, skyscrapers, bridges and airports are all basically made with sand: it has become the most widely consumed natural resource on the planet after fresh water. The worldwide construction boom fuelled by emerging economies and increasing urbanization has led to intensive sand extraction on land and in the oceans, with damaging environmental impacts. Sand Wars takes us around the world as it tracks the contractors, sand smugglers and unscrupulous property developers involved in the new gold rush, and meets the environmentalists and local populations struggling to reverse the threat to the future of this resource that we all take for granted.

==Festivals and awards==
Official selections :
* Hawaii International Film Festival 2013 (USA)
* Orlando Film Festival 2013 (USA Florida)
* San Sebastian Surfilm Festival 2013 (Spain)
* Eko Film Festival 2013, Octova (Czech Republic)
* Kuala Lumpur Environmental Film Festival 2013 (Malaysia)
* Melbourne Environmental Film Festival 2013 (Australia)
* Green Film Festival 2013, Buenos Aires (Argentina)
* Filmambiente Rio de Janeiro 2013 (Brazil)
* Festival de Cine Verde de Barichara 2013 (Colombia)
* Barcelona International Environmental Film Festival 2013 (Spain)
* Escales La Rochelle 2013 (France)
* Sichuan TV Festival 2013 (China)
* Japan Prize NHK 2013, Tokyo (Japan)
* Planet in Focus 2013, Toronto (Canada)
* Festival du Film d’Environnement de Paris 2014 (France)
* Festival du Film Vert 2014, Lausanne (Switzerland)
* Washington DC Environmental Film Festival 2014 (USA)
* Sao Paulo Ecofalante Environmental Film Festival 2014 (Brazil)
* San Francisco International Ocean Film Festival 2014 (USA California )
* Cinema Planeta 2014, Cuernavaca (Mexico)
* Festival du Grand Reportage d’Actualité 2014, Le Touquet (France)
* Festival Internacional de Cine de la Naturaleza y el Hombre 2014, Tenerife (Spain)
* Maremostra International Film Festival 2014, Palma de Majorque (Spain)
* Banff World Media Festival 2014, Banff (Canada)
* Monte Carlo 33rd International URTI (Monaco)
* 40 Fathoms Film Festival 2014, Cap Town (South Africa)
* International Film Festival Of Patmos 2014 (Greece)
* Mokpo Ocean Film Festival 2014 (South Korea)
* Dominican Environmental Film Festival 2014, Santo Domingo (Dominican Republic)
* Wildscreen Festival 2014, Bristol (United Kingdom)
* Ocean Film Festival 2014, Saint Petersburg (USA Florida)
* Bahamas International Film Festival 2014, Nassau (Bahamas)
* Brattleboro Film Festival 2014, Brattleboro, VT (USA)
* Wild and Scenic Film Festival 2015, Nevada City, CA (USA)
* San Francisco Intl Ocean Film Festival Special Screening, 2015 San Francisco, CA
* Corvallis Environmental Center Eco-Film Fest 2015, Corvallis, OR (USA)
* Green Me Film Festival 2015, Berlin Germany
* California Film Institute Youth Environmental Forum, 2015, San Rafael CA (USA)


Awards :
* EKO Film 2013
Best movie - Inspiration Category 
* Sishuan TV Festival 2013
Gold Panda for Best Nature & Environment Protection Award
* Barcelona International Environmental Film Festival 2013
Golden Sun for Best documentary
* Japan Prize NHK 2013
2nd Prize - Welfare Education Category 
* Festival du Film Vert 2014
Switzerland Greenpeace Prize
* San Francisco International Ocean Film Festival 2014
Environment Award
* Monte Carlo 33rd International URTI Grand Prix for Author’s Documentary
Official selection of the 10 finalist documentaries
* Banff World Media Festival 2014
Rockie Award for Best Environmental and Wildlife Program
* Prix Gémeaux 2014
Award for Best Documentary – Nature & Science Category
* Blue Ocean Film Festival 2014
Nominated for Marine Science Category
* Bristol Wildscreen Festival 2014
Nominated for Panda Awards - Environment & Conservation Category
* Wild and Scenic Film Festival 2015
Winner: Environmental Award and Festival Honorable Mention
* Green Me Film Festival 2015
Winner: Soil Film Award

==References==
 

==External links==
* Sand Wars on  
*  
*  
*  
*   TEDx Talk by Denis Delestrac
*  
*  
*  

 