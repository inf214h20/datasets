Where's Sally?
{{Infobox film
| name           = Wheres Sally?
| image          =
| image_size     =
| caption        =
| director       = Arthur B. Woods
| producer       = Irving Asher Brock Williams
| starring       = Chili Bouchier Gene Gerrard Claude Hulbert
| music          =
| cinematography = Basil Emmott
| editing        = Warner Brothers-First First National Productions
| released       = May 1936
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
}} 1936 British comedy film, directed by Arthur B. Woods and starring Chili Bouchier, Gene Gerrard and Claude Hulbert.  The film was a quota quickie production and is now believed to be Lost film|lost. 

==Plot==
A womanising playboy becomes tired of his philandering lifestyle and asks his current girlfriend to marry him.  At the wedding reception, his best man makes a speech treating the entire gathering to the finer details of the bridegrooms chequered romantic history.  The bride becomes upset, and her new husband is furious with his best friend for being so indiscreet.  He whisks her straight out of the wedding hall and they set off on honeymoon.

Matters are set for a series of farcical complications and misunderstandings as they start to meet a motley selection of odd characters who do nothing to improve relations between the newly-weds.  Then the best friends wife turns up at the honeymoon location, announcing that she has left her husband in disgust.  He is quickly on the scene trying to change her mind, and soon there are two sets of bickering couples going full steam, while the bridegroom and his best friend also clash with each other.  The bewildered bride has to try to make up her mind whether or not to stay with her new husband.

==Cast==
* Chili Bouchier as Sonia
* Gene Gerrard as Jimmy Findlay
* Claude Hulbert as Tony Chivers
* Reginald Purdell as Dick Burgess
* Renee Gadd as Sally
* Violet Farebrother as Mrs. Hickory
* Athole Stewart as Lord Mullion
* Morland Graham as Polkinholme

==References==
 

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 


 