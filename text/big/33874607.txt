Sandhitha Velai
{{Infobox film
| name           = Sandhitha Velai
| image          = 
| image_size     =
| caption        =  Ravichandran
| producer       = Kaja Mohideen
| story          = 
| screenplay     =  Karthik Roja Roja Kausalya Kausalya
| Deva
| cinematography = Jayanan Vincent
| editing        = 
| distributor    =
| studio         = 
| released       = April 14, 2000
| runtime        =
| country        = India
| language       = Tamil
| preceded_by    =
| followed_by    =
| website        =
}}
 2000 Tamil Ravichandran being Karthik in Kausalya and Moulee play supporting roles. The film was released in April 14, 2000 during Tamil new year. The film received negative reviews and was declared as a moderate success at the box office.  

==Cast== Karthik as Aadalarasu and Thirunaavukkarasu Roja as Thilaka Kausalya as Akalya Vijayakumar
*Moulee  Sujatha 
*Nassar
*Manivannan Vivek
*Chinni Jayanth
*Thalaivasal Vijay

==Production==
The filming was began in 1998 and became the second directorial of Ravichandran after his debut Kannethire Thondrinaal. The film was briefly delayed due to selection of heroines. Suvalakshmi was initially selected as heroine later she was replaced by Roja. For second heroine, Simran was selected but due to date clashes, she was replaced by Isha but she too was replaced by Kousalya.  There were rumours that film was delayed due to Karthik experiencing trouble due to his health. 

==Soundtrack==
Music is composed by Deva. 
*Vaa Vaa En Thalaiva - Unnikrishnan, Harini
*Pennkiliye - Unnikrishnan, Sujatha
*Ceylon Singala Penne - Sukhwinder Singh
*Muniyamma Kobapadathe - Sabesh

==References==
 

 
 
 
 
 

 