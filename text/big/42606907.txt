Rayaru Bandaru Mavana Manege
{{Infobox film name           = Rayaru Bandaru Mavana Manege image          = image_size     = caption        = director       = Dwarakish producer       = P. Balaram writer         = Priyadarshan narrator       = starring  Vishnuvardhan Farheen|Bindiya Dolly Minhas music          = Raj-Koti cinematography = Mahendar editing        = Murali Ramaiah studio         = Sri Anupama Productions released       =   runtime        = 145 minutes country        = India language  Kannada
|budget         =
}}
 Kannada romantic Vishnuvardhan  Bindiya and Dolly Minhas in the prominent roles.  The music for the film was scored by Raj-Koti and the audio was bought by Lahari Music.
 Malayalam film Chithram (1988) starring Mohanlal and Ranjini (actress)|Ranjini. 

==Cast== Vishnuvardhan
* Bindiya  Dolly
* Dwarakish
* C. R. Simha
* Vajramuni
* Sihi Kahi Chandru
* Shivakumar
* Vasanth Kunigal

==Soundtrack==
The music of the film was composed by Raj-Koti. The soundtrack consisted one Thyagaraja kriti "Nagumomu" performed by K. J. Yesudas 

{{Infobox album  
| Name        = Rayaru Bandaru Mavana Manege
| Type        = Soundtrack
| Artist      = Raj-Koti
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Lahari Music
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Baare Baare Deviye
| lyrics1 	= M. N. Vyasa Rao
| extra1        = S. P. Balasubrahmanyam, K. S. Chithra
| length1       = 
| title2        = Adavi Deviya 
| lyrics2 	= M. N. Vyasa Rao
| extra2        = S. P. Balasubrahmanyam, K. S. Chithra
| length2       = 
| title3        = Muddina Hudugi Chanda
| lyrics3       = R. N. Jayagopal
| extra3 	= S. P. Balasubrahmanyam, K. S. Chithra
| length3       = 
| title4        = Aparadhi Naanalla
| extra4        = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics4 	= R. N. Jayagopal
| length4       = 
| title5        = Nagumomu 
| extra5        = K. J. Yesudas
| lyrics5       = Thyagaraja
| length5       = 
}}

==References==
 

==External source==
*  

 

 
 
 
 
 
 


 