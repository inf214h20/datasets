Heartlands (film)
 
{{Infobox film
| name           = Heartlands
| image          = Heartlands FilmPoster.jpeg
| caption        = 
| director       = Damien ODonnell Richard Jobson, Gina Carter
| writer         = Paul Fraser
| starring       = Michael Sheen Mark Addy Mark Strong Celia Imrie
| music          = 
| cinematography = 
| editing        = Fran Parker
| studio         = DNA Films
| distributor    = Miramax Films
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Heartlands is a 2002 film directed by Damien ODonnell and written by Paul Fraser.  It is a comedy-drama-road movie, running at 90 minutes, produced in the United Kingdom.  It was screened at the Edinburgh Film Festival.

==Plot==
 
Colin (Michael Sheen|Sheen), is a mild mannered newsagent who works all day and then plays on his local darts team in the evening. One night, he discovers that his wife, Sandra, has been unfaithful with the dart teams captain, Geoff. When Colin confronts his wife about the affair, they have an argument and she leaves him. The darts team is going to a Regional finals in Blackpool, but Geoff drops Colin from the team and takes Sandra with him for a weekend away.

Colins best friend, Zippy, advises him that if he does nothing, he will one day look back with regret, so he resolves to travel to Blackpool and tell his wife that he loves her. Leaving the newsagent in the hands of his regular customers, he jumps on his Honda 50 moped and starts travelling.

His first stop-off is at a motorway cafe, where he tries to strike up a conversation with a waitress. Unused to social situations, his clumsy attempts at small talk are ignored by the busy waitress.

In the evening, he heads into a biker pub. Ignoring the laughs and jeers at his mode of transportation, he makes his way to the bar, and starts chatting with landlord, Ron, and his barmaid, Mandy. He also talks to one of the bikers, Ian, who challenges him to a game of darts. Colin then joins Ian and his girlfriend for a few drinks in the makeshift campsite at the back of the pub, and Ian persuades Colin to allow him to cut his hair. With a new look, Colin attends a performance at the pub by English folk singer Kate Rusby (whose music forms the soundtrack to the film).  Colin spends the rest of the evening entertaining his new friends with darts stories of his hero, Eric Bristow, and toothbrush juggling around the campfire.    When he wakes the next day, all his new friends are gone, so he continues on his journey.

Taking a break to stretch his legs he meets an eco-warrior chained to a tree, and shares a cheese sandwich. Later he happens on a group of girl guides and spends a few hours in their company. He has a heart to heart with the guide leader, Sonja, explaining how he gave his wife everything she ever wanted, yet it was still not enough. When he decides to get going, he discovers the girls have decorated his beloved moped with leaves. He heads up a highway, pulling in at a fork in the road. He climbs off his bike to check the map, and his moped is crushed under the wheels of a speeding lorry.

While walking along the road, he is passed by the landlord Ron, who is taking his barmaid to Blackpool for a dirty weekend. She however, has brought her daughter along with her, seeing this weekend as more of an opportunity to get away with her kid. On arrival in Blackpool, Ron asks Colin to return the favour of the lift by taking the kid out of the way for a few hours. However, Ron proceeds to chat up the hotel receptionist so Colin asks Mandy and her daughter if the two of them fancy joining him for a stroll.

The three of them have fun at a fair. Come evening, Colin says he needs to go find his friends. As he is walking around the town, he sees his wife. Colin runs in the opposite direction and spends the night on a sea front bench. The next day he goes to the darts finals.

When he enters the venue, he finds Geoff having an argument with the opposing team, which quickly deteriorates into a brawl. He asks Sandra if she would mind stepping out for a chat, and despite Geoff trying to stop her, she agrees. She tearfully confesses her regret and that she no longer wants to be involved with Geoff. She realizes that she has been very stupid, and is looking for forgiveness. Colin tells her that he loves her more than anyone else in the whole world, but that he does not want to go back to his old life. He has experienced some of the wider world, and wants to continue on the road to see what is around the next corner. He offers her the newsagent and wishes her goodbye.

Colin walks past a cafe and nods a hello his hero, Eric Bristow, who nods in return. Colin gets a new Honda C50 moped, and gets back on the road. Mandy and her daughter wave at Colin from a bus.

==Cast==
*Michael Sheen – Colin
*Mark Addy – Ron
*Mark Strong - Ian Jim Carter – Geoff
*Celia Imrie – Sonja
*Ruth Jones – Mandy
*Phillipa Peak – Sarah Jane Robbins – Sandra
*Paul Shane – Zippy
*Joseph Dempsie – Craig

==References==
 

==External links==
* 

 

 
 
 
 
 