Laadam
 
{{Infobox film
| name           = Laadam
| image          = 
| alt            =  
| caption        = 
| director       = Prabu Solomon
| producer       = Ponnurangam
| writer         = Prabu Solomon
| screenplay     = Prabu Solomon
| starring       = Charmy Kaur Aravindhan Kota Srinivasa Rao Dheeraj Kher Babu Antony  Dharan
| cinematography = Sukumar,
| editing        = 
| studio         = 
| distributor    = Cosmos Entertainment
| released       =  
| runtime        = 
| country        = India Tamil Tamil language|தமிழ்
| budget         = 
| gross          = 
}}
Laadam is a   and debutant Aravindhan,  directed by Prabu Solomon.  Kota Srinivasa Rao, Dheeraj Kher and Babu Antony also play prominent roles in the gangster-orientated thriller. Upon release the film was dubbed into the Telugu language as 16 Days. This movie is a cheap rip-off of 2006 crime thriller film "Lucky Number Slevin".

==Plot==

The plot is a lift from the highly rated comedy crime/thriller English Movie "Lucky Number Slevin". The plot has been indianized to make it relevant to Indian audiences. Everything else in the movie is similar except for the motive of the killings. There is no role for Bruce Willis Character in Laadam. Charmis character is based on Korean flick 3-Iron.

The story begins with Kunjidhapadam (Aravindan) who comes from a small place to the city for an interview and he manages to find place to stay with his distant relative Subramanyam who is actually a defaulter on loans taken on interest. He is absconding but then leaves his key at a secret place. Kunjidhapadam takes the key and stays and he also manages to get a job. On the other hand, there is Angel (Charmy Kaur) who grows as an orphan and works with a mineral water company. She doesnt have a place to stay so she locates those houses without occupants and stays there for a night and gets going in the morning. One such instance gets her to Kunjidhapadams house. All this apart, there are two dons Pavadai (Kota Srinivasa Rao) and Vembuli (Jayaprakash) who are out to kill each other and Vembuli is successful in killing Pavadais son. So he vows to kill Vembulis son by the 12th day ceremony of his son. A twist of fate occurs and Pavadais goons mistake Kunjidhapadam to be Subrahmanyam and get him to take money. Kunjidhapadam gives an idea to kill Vembulis son and he is given a 16 day deadline to kill else Pavadai threatens to kill him

Vembuli comes to know of this and tries to kill Kunjidhapadam. But he comes alive. After that Kunjidhapadam again meets Angel and tries to get money from her to give it back to Pavadai which his distant relative Subramanyam has to give. At that time Pavadai realizes and confirms that Kunjidhapadam has to kill Vembulies son.

He on the way back he mets the opposite gangsters and tries to escape from them. The gangsters get his files which contain his degree certificate. After that he meets Angel and returnes her money, which Pavadai returned with double of the amount and a gun. He meets Angel and both plan to stay in comedy actor Vadivelus house. After that both follows a men working under Vembulie and went  inside a restaurant where he managed to get information about Vembulies son who is hidden in a moving air bus with lots of securities. And he is in search of the bus and founds. After that he tries to stop the bus and wants to talk with Vembulies son. He tells all  things happened with Pavadai and ask him to give him a job with security from Pavadai. But they plays a life game and ordered not to face again or else he will be killed. After that both went to a ministers house to stay at night and in morning they got caught and they managed to escape from the ministers gang. A police tries to escape them and he brings both to station and mistakenly he makes them to get married. And brought them to his house, because it is 100th marriage done in police station. In house first night has been arranged and at that time both Angel and Kunjidhapadam becomes closer and get good affectionate and both caught in love. Next day he tries to see Vembulies son who is in a small boat and some of his gangsters were playing in beach and they throws him into the sea. He manages to swim and reaches the boat and asks for his degree certificates. They all played and tornes all his cerfificate and one of the guy from that gang asks him to send his Angel to him for prostitution. 
Now Kunjidhapadam got angry and kills all in boat and kills Vembulies son too and brings the body in a small boat to beach where all gangsters were playing and kills all the mens in the gang. After that he brings the body to Vembulies house and kidnapps Vembuli as well as Pavadai. And keeps both tied in a rock area and murders all. And again joins with Angel and starts back to Hydrabad and the film ends with got good job, they became parent for 2 childrens and happy lived.

==Cast==

* Charmy Kaur as Angel
* Vadivelu (guest appearance)
* Kunal Shah (guest appearance)

==Production==
 Tamil lessons, to become fluent in the language prior to the film. Soon after the launch of the film, the original producers, Chozha Creations, sold their stake in the film to Cosmos Entertainment. To capitulate on Charmys popularity in Andhra Pradesh, a Telugu version has been readied to be released shortly after the release of the original.

The film in October 2008, faced a high court order staying its release. The petition against the movie was filed by Ponnurangan on behalf of Chozha Creations, giving an issue is one of financial settlement between Chozha Creations and Cosmos Enterprises, the producers of Laadam.In the petition, it has been said that Laadam was being initially produced by Chozha Creations. But, midway through the making, the movie was sold outright to Cosmos Enterprises for a sum of Rs. 90 lakhs as per an agreement reached in June. But, Chozha Creations claims to have received only Rs. 40 lakhs to date and have thus filed the petition. In the interim, Laadam has been completed and is ready for release. But this stay order might now delay or even worse, prevent its release. The high court has given a period of 2 weeks for Cosmos Enterprises to file an explanation. 

==Soundtrack==
1. Siru Thodudhalile 
From the music composed by Dharan, two songs have been recorded, one each by Dr. Burn and Emce Jezz, both rappers of Malaysian based band, Natchatra. 

==References==
 

 

 

 
 
 
 