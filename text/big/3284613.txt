Comic Book Confidential
{{Infobox Film
 | name = Comic Book Confidential
 | image = Comic_Book_Confidential.jpg
 | caption = Movie poster for Comic Book Confidential
 | director = Ron Mann
 | producer = Don Haig Martin Harbury Charles Lippincott
 | writer = Ron Mann Charles Lippincott Frank Miller
 | music = Dr. John Keith Elliott Gerard Leckey
 | cinematography = Joan Churchill Robert Fresco
 | editing = Robert Kennedy
 | distributor = Cinecom Pictures  
 | released = 1988 1991 (Laserdisc) 1993 (VHS) 1994 (CD-ROM) 2002 (DVD) 2012 (Blu-Ray)
 | runtime = 90 min.
 | language = English
 | budget =
}} Canadian documentary film, released in 1988. Directed by Ron Mann and written by Mann and Charles Lippincott, the film is a survey of the history of the comic book medium in the United States from the 1930s to the 1980s, as an art form and in social context.

==Synopsis== Charles Burns, Frank Miller, Stan Lee, Will Eisner, Robert Crumb, Harvey Pekar and William M. Gaines. In interviews, the creators discuss their contributions and history, and read passages from their works over filmograph animations.  Montages of comics through the decades, archival scenes of politically important moments, and a live-action Zippy the Pinhead are featured.

==Production== Julius "Julie" Schwartz  and creator of the first all-woman comic book It Aint Me Babe, Trina Robbins. Johnson, Paddy (October 25, 2006).  . The Reeler. 

==Release==
Confidential was first released theatrically in Canada in 1988, and in the United States in 1989. It was released unrated. 
The 1991 laserdisc had extra features consisting of a complete comic by each artist, shot for TV viewing.  . lddb.com; Laserdisc Database.  

Confidential was one of the first films to be released in  , p. 240.  

==Reception==
The film received the 1989  , 1989.   Caryn James of The New York Times found the film deft and intelligent&mdash;it "takes off when it abandons the archives and focuses on the creators," but "it plays to the converted," and its attempt to relate comics to social context is "fleeting." James, Caryn (June 14, 1989).  . The New York Times.  Desson Howe, in the Washington Post, wrote that the film was "a pleasure," and engaging throughout. Howe, Desson (August 18, 1989). .  .  Peter Rist described the film in 2001 as "Manns greatest success, both critically and popularly." 

==References==
 

==External links==
*   Official site.
*  
*  . Canadian Film Encyclopedia; Film Reference Library (CA).

 
 
 
 
 
 
 
 