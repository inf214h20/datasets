Maanasa Sarovara
{{Infobox Film name           = Manasa Sarovara   ಮಾನಸ ಸರೋವರ image          = Maanasa Sarovara.jpg image_size     = Theatrical poster caption        =  director       = Puttanna Kanagal producer       = Varghese Kamalakar Geetha Srinath Puttanna Kanagal writer         = Puttanna Kanagal screenplay     = Puttanna Kanagal starring  Ramakrishna   G.Pandithachar music          = Vijaya Bhaskar cinematography = B. S. Basavaraj editing        = V. P. Krishna distributor    = Mithra Vrunda Movies released       = 1982 runtime        = 140 minutes country        = India language       = Kannada budget         =  preceded_by    =  followed_by    = 
}}
 Karnataka State Award for Best Actress for her role as an insane woman in the film. 

== Plot ==
A middle aged psychiatrist finds a young insane woman wandering aimlessly along the streets. He takes her to his abode for treatment. Slowly the woman recovers from her trauma. The doctor promptly falls in love with her and decides to marry her. In the meantime, the doctors nephew arrives and falls head over heels for her. The doctor cannot tolerate this intimacy and goes insane.

==Cast==
* Srinath as Psychiatrist 
* Padmavasanthi as young insane woman  Ramakrishna as Doctors nephew
* G.Pandithachar as trusted butler and confidante

== Reception ==
Songs such as "Vedanti helidanu honella mannu mannu" and "Nine saakida gini" are excellent. A visual treat and human thoughts are spread like silk thread which shows the characteristics and need for a love by a person who loses love from his wife and lover. This movie is supposed to be inspired by the tragedy in the life of Legendary Director Puttanna Kanagal himself. Specially the song "Neene Saaakida Gini" penned by great Vijaya Narasimha is an evergreen song. Haadu Haadu is an epic of a song. The song depicts three young characters and their surging hormonal levels with brilliant ease. Vijay Bhaskar proves yet again that he is a creator extraordinaire.

==Soundtrack==
{| class="wikitable sortable"
|-
! Title !! Singers !! Lyrics 
|- Manasa Sarovara     || S. P. Balasubrahmanyam, Vani Jayaram || Vijaya Narasimha 
|- Vedanthi Helidanu   || P. B. Sreenivas || G. S. Shivarudrappa 
|- Keliranna Keli Karim Khan 
|- Neene Sakida Gini   || S. P. Balasubrahmanyam || Vijaya Narasimha
|- Chanda Chanda       || P. Jayachandran || M. N. Vyasarao 
|- Haadu Haadu         || Vani Jayaram || G. S. Shivarudrappa
|}

== Awards ==
* Karnataka State Film Awards 1982-83 Best Actress - Padmavasanthi

== References ==
 

== External links ==
*  
* http://www.raaga.com/channels/kannada/moviedetail.asp?mid=k0000483

 
 
 
 
 


 