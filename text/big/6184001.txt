Partners (1982 film)
{{Infobox Film
| name = Partners
| image = Partners 1982.jpg
| caption = 
| director = James Burrows
| producer = Aaron Russo Francis Veber
| writer = Francis Veber
| starring       = {{plainlist|
* Ryan ONeal
* John Hurt Kenneth McMillan
* Robyn Douglass
}}
| music = Georges Delerue
| cinematography = Victor J. Kemper
| editing = Danford B. Greene   Stephen Lovejoy
| distributor = Paramount Pictures
| released = 30 April 1982
| runtime = 93 min 
| country =   English
| budget = 
| gross = $4,109,724 (USA)
| preceded_by = 
| followed_by = 
}}
 buddy comedy, starring Ryan ONeal and John Hurt as a mismatched pair of cops.

==Plot== gay couple closeted although the entire Department knows about his sexual identity. The pair discover an earlier murder and learn that both victims appeared in the same gay magazine. Each had received a call from a hoarse-voiced man asking them to model for him, only to turn up dead soon after. Benson models for the magazine and is approached by the same hoarse-voiced man; but, when another model turns up dead, the man is cleared as a suspect.

Benson grows close to Jill, the photographer of his shoot, and plans a weekend getaway with her. Kerwin suspects her of the murders, but his superiors put it down to jealousy. Kerwin uncovers evidence implicating Jill; but, when the police move to apprehend her, they discover her corpse. Her death unknown to Benson, he arrives for his rendezvous with Jill; and Kerwin races to his aid. Jills killer, a closeted man whom Jill and one of the victims were blackmailing, admits to Benson that he killed Jill and two of the men but insists that Jill killed her partner in crime. Realizing that Kerwin is outside, the killer shoots at Kerwin who returns fire. Kerwin is wounded, but the other man is killed.

==Analysis==
The film derives much of its alleged humor from Bensons discomfort with homosexuality and his stereotyping "gay behaviour." The film shows the evolution of Bensons attitudes toward Kerwin, in particular, and gay people, in general.

==Reception== Worst Actor of the Decade.  
When asked if the movie drew any complaints from gay men during filming, John Hurt said, "They didnt like it that I was wearing a lilac colored, track suit in it.  They say homosexuals do not necessarily do that.  And the person who saying this is sitting there in a pink track suit, Its a crazy world we live in."  

==See also==
* List of American films of 1982

==References==
 

==External links==
* 

 
 
 
 
 