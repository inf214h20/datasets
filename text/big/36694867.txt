Ceddo
{{Infobox film
| name           = Ceddo
| image          = 
| caption        = 
| director       = Ousmane Sembène
| writer         = Ousmane Sembène
| music          = Manu Dibango
| starring       = Tabata Ndiaye Moustapha Yade
| producer       = 
| distributor    = 
| released       =  
| runtime        = 120 minutes French Wolof Wolof
| country        = Senegal
| screenplay     = 
| cinematography = Georges Caristan
| editing        = Florence Eymon
}}

Ceddo ( ) is a 1977 Senegalese film directed by Ousmane Sembène.  It was entered into the 10th Moscow International Film Festival.   

== Plot ==
In a late 19th century Senegal, the Ceddo ("commoners") try to preserve their traditional culture against the onslaught of Islam, Christianity, and the slave trade. When local king Demba War sides with the Muslims, the Ceddo abduct his daughter, Dior Yacine, to protest their forced conversion to Islam. After trying to rescue the princess, various heirs to the throne are killed, and Demba War is killed during the night. Eventually the kidnappers are killed and Dior Yacine is brought back to the village to confront the imam, just as all the villagers are being given Muslim names.

== Banning ==
Along with a number of his other films, Ceddo was banned in Senegal for its presentation of the conflicts between the Islamic and Christian religions and ethnic and traditional beliefs.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 