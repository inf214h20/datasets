Brenda Starr (film)
{{Infobox film
| name           = Brenda Starr
| image          = Brendastarrposter.jpg
| image size     =
| caption        = U.S. theatrical release poster
| director       = Robert Ellis Miller
| producer       = Myron A. Hyman
| writer         = Noreen Stone James D. Buchanan Delia Ephron|"Jenny Wolkind" (pseud. Delia Ephron) Dale Messick
| narrator       =
| starring       = Brooke Shields Tony Peck Timothy Dalton Diana Scarwid
| music          = Johnny Mandel
| cinematography = Freddie Francis Peter Stein
| editing        = Mark Melnick Triumph Releasing Corporation
| released       = May 15, 1989 (France) April 15, 1992 (USA)
| runtime        = 93 min
| country        = United States
| language       = English
| budget         = $16,000,000 
| gross          = $67,878
| preceded by    =
| followed by    =
}}
 adventure film, Dale Messicks Brenda Starr comic strip. The film was directed by Robert Ellis Miller, and stars Brooke Shields, Timothy Dalton, and Jeffrey Tambor. 

==Plot==
Mike is a struggling artist who draws the Brenda Starr comic strip for a newspaper. When Brenda comes to life and sees how unappreciated she is by Mike, she leaves the comic. To return her to her rightful place and keep his job, Mike draws himself into the strip.

Within her fictional world, Brenda Starr is an ace reporter for the New York Flash. She is talented, fearless, smart and a very snappy dresser. The only competition she has is from the rival papers top reporter, Libby Lipscomb.

Brenda heads to the Amazon jungle, in order to find a scientist with a secret formula, which will create cheap and powerful fuel from ordinary water. There, she must steal the formula from her competition and foreign spies.

==Cast==
*Brooke Shields as Brenda Starr
*Tony Peck as Mike Randall
*Timothy Dalton as Basil St. John
*Diana Scarwid as Libby Lipscomb
*Nestor Serrano as Jose
*Jeffrey Tambor as Vladimir
*June Gable as Luba
*Charles Durning as Francis I. Livright
*Kathleen Wilhoite as Hank OHare
*John Short as Pesky Miller
*Eddie Albert as Police Chief Maloney
*Mark von Holstein as Donovan OShea
*Henry Gibson as Professor Gerhardt Von Kreutzer
*Matthew Cowles as Capt. Borg
*Tom Aldredge as Capt. Borg Impostor President Harry S. Truman
*Sergio Kato as Cab Driver Jose
*Steve Millar as Flight Passenger and Hungry Restaurant Man
The project originally envisioned Jessica Lange as Brenda Starr. Other actors considered were Anjelica Huston, Melanie Griffith, and finally Brooke Shields. 

==Post production and release== distribution  rights.  
 bombed at the box office, making US$30,000 in its first week.  Negative reviews were blamed and the film was pulled from theatres shortly after its theatrical distribution. 

==Reception==
The film received scathing reviews.

Owen Gleiberman, of Entertainment Weekly, graded the film F (grade)|F, stating that Brenda "... comes off as a giggly (if spectacularly elongated) high school princess" and that Brenda Starr "is so flaccid and cheap-looking, so ineptly pieced together, that it verges on the avant-garde. I suspect they wont even like it in France."  

Peter Travers of Rolling Stone magazine gave the film an equally negative review, writing, "Theres been so much negative insider buzz about Brookes Brenda that you might be harboring a hope that the damned thing turned out all right. Get over it. Brenda is not as bad as the also-rans that Hollywood traditionally dumps on us before Labor Day ... its a heap worse." 
 The New York Times Janet Maslin commented, "This would-be comic romp is badly dated in several conspicuous ways. Its cold war villains are embarrassingly outre (even allowing for the films 1940s look, in keeping with the peak popularity of Brenda Starr as a comic strip heroine) ... most dated of all is Brenda herself, the "girl reporter" who worries chiefly about not running her stockings or breaking her high heels, and who in one scene actually uses a black patent leather handbag as a secret weapon."  

Pamela Bruce, of The Austin Chronicle, was highly critical of the film: "After gathering dust for five years, some studio executive decided that there just isnt enough dreck in the world and decided to unleash Brenda Starr upon us poor, unsuspecting mortals." 

==Home video== rated PG, was released on both VHS  and DVD  formats.

The DVD version is available for purchase in two variations; one for all   Stereo sound. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 