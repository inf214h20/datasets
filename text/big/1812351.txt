Mindwalk
{{Infobox film
| name           = Mindwalk
| image          = mindwalk.gif
| image_size     =
| caption        = VHS box art
| director       = Bernt Capra
| producer       = Klaus Lintschinger (executive)
| writer         = Bernt Capra (story) Floyd Byars & Fritjof Capra (screenplay)
| narrator       = John Heard
| music          = Philip Glass
| cinematography = Karl Kases
| editing        = Jean-Claude Piroué
| distributor    = Triton Pictures
| released       =  
| runtime        = 112 min.
| country        = United States
| language       = English
| budget         =
}}
Mindwalk is a 1990 feature film directed by Bernt Amadeus Capra, based on his own short story, based in turn on the book The Turning Point (Book)|The Turning Point by his brother Fritjof Capra, the author of the book The Tao of Physics.
 John Heard), a former political speechwriter, as they wander around Mont Saint-Michel, France. The movie serves as an introduction to systems theory and systems thinking, while insights into modern physical theories such as quantum mechanics and particle physics are also given. 

Political and social problems, and alternative solutions for them, are another major focus of the film. However, specific problems and solutions are not the main focus; rather, different perspectives are presented through which these problems can be viewed and considered. Sonia Hoffmans perspective is referred to as the holism|holistic, or systems theory, perspective. Thomas Harriman, the poet, recites the poem "Enigmas" by Pablo Neruda (based on the translation by Robert Bly) at the end of the movie, concluding the core of the discussion.

The film was filmed on the mount and has views of many structures there, including the approach over the tidal flats, the cathedral, the walkways and the giant, ancient clock mechanism.

==Cast==
* Liv Ullmann as Sonia Hoffman
* Sam Waterston as Jack Edwards John Heard as Thomas Harriman
* Ione Skye as Kit Hoffman
* Emmanuel Montes as Romain
* Gabrielle Danchik as Tour guide
* Jeanne van Phue as Tourist 1
* Penny White as Tourist 2
* Jean Boursin as Sacristan

==External links==
*  
*  
*  

 

 
 
 