Whispering Whoopee
{{Infobox film
| name           = Whispering Whoopee
| image          = 
| image_size     = 
| caption        = 
| director       = James W. Horne
| producer       = Hal Roach (producer)
| writer         = H.M. Walker (dialogue)
| narrator       = 
| starring       = See below
| music          = Leroy Shield  (uncredited)
| cinematography = Len Powers
| editing        = Richard C. Currier
| studio         = 
| distributor    = 
| released       = 1930
| runtime        = 20&nbsp;minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Whispering Whoopee is a 1930 American short film directed by James W. Horne. It is in the Public Domain. 

== Plot summary ==
 

== Cast ==
*Charley Chase as Charley
*Thelma Todd as Miss Todd
*Anita Garvin as Miss Garvin
*Dolores Brinkman as Miss Brinkman
*Kay Deslys as Miss Deslys
*Eddie Dunn as Ricketts, the Butler
*Dell Henderson as Mr. Henderson
*Carl Stockdale as Mr. Stockdale
*Tenen Holtz as Mr. Holtz

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 


 