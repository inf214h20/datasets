Ganga Maiyya Tohe Piyari Chadhaibo
{{Infobox film 
| name           = 
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kundan Kumar 
| producer       = Bishwanath Prasad Shahabadi
| writer         =
| screenplay     =Nazir Hussain
| story          =Nazir Hussain
| based on       =  
| narrator       = Kumkum  Ashim Kumar Nazir Hussain  Tiwari Chitragupta Shailendra Shailendra
| cinematography =R. K. Pandit
| editing        =Kamalakar
| studio         =
| distributor    =
| released       =   
| runtime        =120 min.
| country        = India
| language       = Bhojpuri
| budget         =
| gross          =
}} directed by Kundan Kumar. It was the first-ever Bhojpuri film, and starred Kumkum (actress)|Kumkum, Ashim Kumar and Nazir Hussain. It had music by Chitragupta (composer)|Chitragupta, lyrics by Shailendra and songs sung by Lata Mangeshkar and Mohammad Rafi.

Ganga Maiyya Tohe Piyari Chadhaibo was released on February 22, 1963 at Veena Cinema, Patna. The film was directed by Kundan Kumar and produced by Bishwanath Prasad Shahabadi on behest of the first president of India, Dr. Rajendra Prasad, with initial budget of Rs.150,000 eventually ending up at approximately 500,000. It was shown to Desh Ratna Dr. Rajendra Prasad at a special screening organized at Sadaqat Ashram, Patna before its release.  

The theme is based on widow remarriage.

==Cast== Kumkum
* Jagannath Shukla(Jaggi Baba)
* Ashim Kumar as Shyam
* Nazir Hussain
* Tiwari (Ramayan Tiwari) Helen
* Padma Khanna
* Sujit Kumar
* Leela Mishra
* Tun Tun
* Bhagwan Sinha

==Soundtrack==
Ganga Maiyya Tohe Piyari Chadhaibo  has music by Chitragupta (composer)|Chitragupta, with lyrics by Shailendra (lyricist)|Shailendra. 

* "Ganga Maiyya Tohe Piyari Chadhaibo" - Lata Mangeshkar, Usha Mangeshkar
* "Sonwa Ke Pinjra Mein" - Mohammad Rafi
* "More Karejwa Men Pir " - Lata Mangeshkar, Usha Mangeshkar
* "Kahe Bansuria Bajaile" (Happy) - Lata Mangeshkar
* "Ab To Lagat Mora Solvwa Saal" - Suman Kalyanpur
* " Luk Chuk Badra" - Lata Mangeshkar
* "Kahe Bansuria Bajaile" (Sad) - Lata Mangeshkar

==Production==

===Development===
At an award function in Mumbai in the late 1950s, character actor Nazir Hussain met then president Dr. Rajendra Prasad, who also belonged to Bihar. During their conversation Prasad asked Hussain, "Why dont you make films in Bhojpuri?" The conversation inspired Hussain. He had already written the screenplay of the Ganga Maiyya Tohe Piyari Chadhaibo, meaning "Ganges Mother I will offer you the auspicious yellow sari" (if my wishes come true) and had first given it to Bimal Roy, with whom he had worked in Devdas (1955).    

In a chance enounter, Hussain met Bishwanath Prasad Shahabad, a businessman from Arrah in Bihar, at a film studio in Bombay. Shahbad, owned cinema halls in Dhanbad and Giridih. When Hussain narrated the story to Shahbad, he immediately agreed to finance the film at  , though eventually it cost  . Kundan Kumar, who hailed from Varanasi, and had made the film Bade Ghar Ki Bahu (1960) with Geeta Bali and Abhi Bhattacharya was chosen. 

===Filming===
The muhurat shot was shot at Shaheed Smarak (Martyrs Memorial) in Patna on February 16, 1961. The shooting formally started the next day. The film was mostly shot in Bihta, a small town 35&nbsp;km west of Patna and pilgrimage town of Varanasi. It has sequences shot at Gol Ghar in Patna and Arrah railway station. 

==Release and reception==
At the First Bhojpuri Film Awards for Bhojpuri and Magadhi films, organized by the Bhojpuri Film Samaroh Samiti and held on April 27, 1965, at Ananda Bazar Patrika Bhawan, Calcutta, Ganga Maiyya Tohe Piyari Chadhaibo won numerous awards, including Best Film, Best Actress (Kum Kum), Best Supporting Actor (Nazir Hussain), Best Lyrics (Shailendra), Best Story (Nazir Hussain) and Best Playback Singer - Male (Mohammed Rafi - "Sonwa Ke Pinjre mein").   

==Legacy==
In 2011, it was shown during 99th Bihar Divas (Bihar Day) celebrations.      

==References==
 
*  
*  

==External links==
*  

 
 
 
 
 