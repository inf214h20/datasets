A Cock and Bull Story
 
 
 
{{Infobox film
| name           = A Cock and Bull Story
| image          = Tristram_shandy_a_cock_and_bull_story_ver2.jpg
| caption        = Film poster
| screenplay     = Frank Cottrell Boyce
| based on       =  
| music          = Michael Nyman Nino Rota
| cinematography = Marcel Zyskind
| editing        = Peter Christelis
| starring       = Steve Coogan Rob Brydon Keeley Hawes Shirley Henderson Stephen Fry and Gillian Anderson
| director       = Michael Winterbottom
| producer       = Andrew Eaton
| studio         = BBC Films Baby Cow Productions EM Media East Midlands Media Initiative Revolution Films Scion Films
| distributor    = Redbus Film Distribution
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} Tristram Shandy. Gillian Anderson and Keeley Hawes also play themselves in addition to their Tristram Shandy roles. Since the book is about a man attempting but failing to write his autobiography, the film takes the form of being about failing to make the film.

== Plot ==
A Cock and Bull Story depicts Steve Coogan playing himself as an arrogant actor with low self-esteem and a complicated love life. Coogan is playing the eponymous role in an adaptation of The Life and Opinions of Tristram Shandy, Gentleman being filmed at a stately home. He constantly spars with actor Rob Brydon, who is playing Uncle Toby and believes his role to be of equal importance to Coogans, calling himself the "co-lead".
 Battle of Namur and Tristrams sudden and accidental circumcision at the age of three. Uncle Tobys wooing of Widow Wadnam (Gillian Anderson) takes place in a sequence dreamed by Steve Coogan and after the cast and crew have viewed the "completed" film ending, with Walter Shandy fainting at the sight of his wife giving birth, the question "How does the book end?" is followed by the concluding scene of the novel, in which Yorick says "It is a story about a Cock and a Bull – and the best of its kind that ever I heard!" (Yorick is not in the film-within-the-film; in this scene he is played by Stephen Fry, who appears elsewhere in the film as Patrick, a scholarly talking-head.)

==Cast==
*Steve Coogan as Tristram Shandy / Walter Shandy / Steve Coogan
*Rob Brydon as Captain Toby Shandy / Rob Brydon
*Raymond Waring as Corporal Trim / Raymond Waring
*Keeley Hawes as Elizabeth Shandy / Keeley Hawes
*Shirley Henderson as Susannah / Shirley Henderson
*Gillian Anderson as Widow Wadman / Gillian Anderson
*Dylan Moran as Dr. Slop / Dylan Moran
*David Walliams as Curate
*Stephen Fry as Parson Yorick / Patrick Curator / Stephen Fry
*Jeremy Northam as Mark (director)
*Ian Hart as Joe (writer)
*James Fleet as Simon (producer)
*Naomie Harris as Jennie
*Kelly Macdonald as Jenny Mark Williams as Ingoldsby
*Greg Wise as Greg
*Roger Allam as Adrian
*Ashley Jensen as Lindsey
*Ronni Ancona as Anita
*Kieran OBrien as Gary Anthony H. Wilson as TV interviewer

==Soundtrack==
The films soundtrack is notable for featuring numerous excerpts from Nino Rotas score for the Federico Fellini film 8½, itself a self-reflexive work about the making of a film. Other non-diegetic musical references are made to Amarcord, The Draughtsmans Contract, Smiles of a Summer Night, Fanny and Alexander and Barry Lyndon. Michael Nyman, composer of The Draughtsmans Contract provides a new arrangement of the Handel Sarabande featured in the latter film, while the tracks of The Draughtsmans Contract (the original soundtrack recordings, the score has been re-recorded numerous times) serve as a temp track to film of the Sterne material.

==Locations==
The film was recorded at a number of locations in England: 
*Blickling Hall, Norfolk
*Felbrigg Hall, Norfolk Gunthorpe Hall, Norfolk
*Heydon Hall, Norfolk
*Deene Park, Northamptonshire
*Kirby Hall, Northamptonshire
*Lamport Hall, Northamptonshire
*Shandy Hall, North Yorkshire - Which was Laurence Sternes home where part of Tristram Shandy was written.
*Quenby Hall, Leicestershire

==Reception==
The film has received very positive reviews. Review aggregate Rotten Tomatoes reports that 90% of critics have given the film a positive review based on 121 reviews. 

==Home release==
A Cock and Bull Story was released on both Region 1 and Region 2 DVD in July 2006.

==The Trip== The Trip.   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 