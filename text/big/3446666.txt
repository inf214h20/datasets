Tarzoon: Shame of the Jungle
{{Infobox film
| name           = Tarzoon: Shame of the Jungle
| image          = Tarzoon, Shame of the Jungle.jpg
| caption        = American theatrical release poster
| director       = Picha Boris Szulzinger
| producer       = Boris Szulzinger
| writer         = Original version: Pierre Bartier   Michael ODonoghue
| starring       = Johnny Weissmuller, Jr. John Belushi Bill Murray Christopher Guest Brian Doyle-Murray
| music          = Marc Moulin
| cinematography = Raymond Burlet
| editing        = Claude Cohen
| distributor    = 20th Century Fox (France) International Harmony (US)
| released       =  
| runtime        = 85 minutes (France) 79 minutes (US)
| country        = France Belgium
| language       = French
| budget         =
| gross          =
}}
 Belgian animated film directed by cartoonist Picha and Boris Szulzinger. The film was the first foreign-animated film to receive both an X rating and wide distribution in the United States.   
This film is also known as Jungle Burger in the United Kingdom.

==Plot==
The film takes place in the deepest part of Africa - "Bush Country". The evil, bald, and multi-breasted Queen Bazonga, who resides in a blimp, inside a cave shaped like a womens legs spread open revealing her vagina, plans to conquer the earth. Before she can do that, however, she wishes to have a full set of hair so people can take her seriously. Her two-headed assistant, The Charles Of The Pits, suggest a "scalp transplant", an experiment where someone elses hair is transplanted to another persons head. Bazonga demands that she wants the hair of June, the maid of Shame, ruler of the jungle. Bazonga sends out her penis soldiers to kidnap June. Meanwhile, that night, June kicks out Shame from their home after another night of unsuccessful sex. She ends up sleeping with Flicka, Shames monkey pal. The next morning, Bazongas soldiers barge in and kidnap June, but only after they have an orgy with her. Shame hears Junes screams and comes to her rescue, but he is too late. Shame eventually decides to save his mate and immediately sets out on his quest with Flicka.

As he swings through the jungle, a airplane crashes in a giant mud pit, containing a crew of four explorers set out to find Shame. The crew include the eccentric Professor Cedric Addlepate, the ditzy Stella Starlet, the grumbling Brutish (who only wants to find Shame for fame), and his assistant Short, a nervous black man. As the crew wander through the jungle, they eventually find Shame, but before they can get acquainted, Brutish and Short step in to take Shame back to the plane, leaving the Professor to be eaten alive by savage monkeys known as "Molar Men" while Stella is tied up to a tree. The Molar Men catch up with Brutish and Short and eat them both. They free Shame from the cage, but only to try and eat him. Shame gets saved by a beer-guzzling frat boy named Craig Baker, who flies on a carpet run by a flock of birds. After a lengthy conversation with Shame, Craig gets drunk and falls off the carpet. Shame also falls off, but is saved by Flicka.
 generator exploding, which sends the place on fire. Bazonga cant escape from the fire in her office. Shame sees this and saves her by igniting the Emergency Fire Alarm, which sends out more of Bazongas soldiers to cover themselves in condoms and dive inside Bazongas vagina until she explodes. One of the heads on the Charles Of The Pits kills the other while in a heated conversation. Shocked by this, the other head sets June free. They try to escape, but the Charles Of The Pits is killed by acid semen shooting all over the place.

Shame eventually finds June, who keeps bickering to Shame while they find a way to escape the blimp, as it drills its way out of the cave and flies all around the jungle. The two find an emergency two-seated parachute, and spring out of the blimp, which finally crashes onto Bazongas cave, destroying it forever. As June kisses Shame for his bravery, they both spot Stella Starlet, who becomes the leader of the Molar Men, and plans to conquer Hollywood.

== Production ==
A 15-minute pilot was shown at the 1974 Cannes Film Festival, and the film was finished by September 1975. The following year, the estate of Edgar Rice Burroughs sued the producer of Tarzoon and 20th Century Fox, the films distributor in France, for alleged plagiarism. The estate lost the case after the French court determined the film was a legitimate parody. 

In 1978, the film was imported into the United States by International Harmony and Stuart S. Shapiro. Shapiro recalls telling customs that the film was a work in progress and it would be edited to be suitable for theatrical release in the U.S. He did not remember any problems bringing the film into the country.  The distributor encountered problems finding theaters willing to show the X rated version of the film. The film ended up making a profit in San Francisco, but was largely unsuccessful in other towns. Much of its success was credited to International Harmonys ad campaign created by writer Edwin Heaven who used the films disadvantage (rated X) to its marketing advantage; radio ads and giant posters plastered all over San Francisco proclaimed: "YOURE GOING TO LAUGH YOUR X OFF!"  

Eventually, the film was reedited and dubbed. After several changes, the distributor persuaded the MPAA to change the films rating to an R.  The R rated version of the film featured new dialogue performed by American actors and comedians such as John Belushi, Adolph Caesar, Brian Doyle-Murray, Judy Graubart, Bill Murray and Johnny Weissmuller Jr.

The Burroughs estate filed another lawsuit demanding that the name of the film be changed when their lawyer found a New York State statute covering disillusion of trademark. They argued that Tarzan was a wholesome trademark and that the current product degraded the characters name. A judge agreed. The suit was filed three weeks into the films New York run.  The title was shortened to Shame of the Jungle, and the "Tarzoon" character name was altered by cutting the name out of the soundtrack negative and splicing it back into the soundtrack upside down.  According to Shapiro, the film did not do as well at the box office, because audiences were attracted to the "Tarzoon" name. 

== Response ==
 Fritz the Dirty Duck. Playboy praised the films artwork, but felt that the film became "monotonous after a good start — still, in the off-the-wall category, the most literate prurient and amusing challenge to community standards since Fritz the Cat."  

The film was banned by the New Zealand Board of Censors in 1980. 

==Release==
The film was released on DVD in the UK by Lace DVD on 31 January 2011. There are no plans to release the film officially on DVD in North America, although there are unofficial DVDs being sold online.

== Cast ==

===Original French version===
* Georges Aminel - Shame
* Arlette Thomas - June
* Paule Emanuele - Queen Bazonga Claude Bertrand - Chief MBulu
* Pierre Trabaud - Charles Of The Pits #1
* Roger Carel - Charles Of The Pits #2
* Guy Piérauld - Professor Cedric Addlepate

===English version===
* Johnny Weissmuller, Jr. - Shame Pat Bright - Queen Bazonga
* Emily Prager - June
* John Belushi - Craig Baker
* Brian Doyle-Murray - Charles Of The Pits #1 Andrew Duncan - Charles Of The Pits #2
* Christopher Guest - Chief MBulu / Short / Nurse
* Judy Graubart - Stella Starlet
* Adolph Caesar - Brutish
* Guy Sorel - Cedric Addlepate
* Bill Murray - Reporter
* Bob Perry - Narrator
* Deya Ment - Additional voices John Baddeley - Additional voices

== References ==
 

== External links ==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 