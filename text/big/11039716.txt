Whiteout (2009 film)
{{Infobox film
| name           = Whiteout
| image          = Whiteout poster.jpg
| caption        = Theatrical release poster
| director       = Dominic Sena Stuart Baird  (uncredited reshoots)  Len Wiseman  (uncredited reshoots) 
| producer       = Joel Silver Susan Downey David Gambino Chad Hayes Carey Hayes
| based on       = Whiteout (Oni Press)|Whiteout by Greg Rucka Steve Lieber
| starring       = Kate Beckinsale Tom Skerritt Columbus Short Gabriel Macht
| music          = John Frizzell
| editing        = Martin Hunter Stuart Baird
| cinematography = Christopher Soos
| studio         = Dark Castle Entertainment
| distributor    = Warner Bros. Pictures
| released       =   
| runtime        = 101 minutes  
| country        = United States France Canada 
| language       = English   Russian
| budget         = $35 million 
| gross          = $17,840,867 
}}
 1998 comic book of the same name by Greg Rucka and Steve Lieber. Directed by Dominic Sena, with uncredited reshoots by Stuart Baird and Len Wiseman, it stars Kate Beckinsale, Tom Skerritt, Gabriel Macht and Alex OLoughlin in the lead roles. The film was distributed by Warner Bros. and released on September&nbsp;11, 2009.  It was produced under the banner of Dark Castle Entertainment by Joel Silver, Susan Downey and David Gambino.
 Deputy U.S. Marshal Carrie Stetko (Kate Beckinsale) is planning to leave in a few days. After finding a dead body, Stetko is attacked by a masked killer who is trying to get hold of the cargo in an old Soviet plane that crash landed in the ice during the Cold War.

== Plot == Deputy U.S. Marshal Carrie Stetko (Kate Beckinsale) has been working in Antarctica for two years, since a betrayal by her partner in Miami that killed him and nearly killed her. She plans to retire after returning to the United States in two days.

Stetko, her friend Doc (Tom Skerritt), and pilot Delfy (Columbus Short) fly to the remote Haworth Mesa to retrieve a discovered body. The dead man is Anton Weiss, one of a group of three scientists looking for meteorites. An autopsy finds evidence of murder by axe. A murder requires a federal investigation; Stetko considers sending the body to McMurdo Station to avoid spending another winter in Antarctica, but decides to continue the investigation. When Stetko goes to speak to one of the others at Vostok Station, she finds him dying from a neck wound and is herself attacked by a black-clad man with an axe. Stetko injures her hands in escaping, losing the wet skin of her fingers on the metal handle of a door. Later, she finds Robert Pryce (Gabriel Macht), a United Nations security agent, examining the body of the second scientist. They conclude that the third, who is missing, must be the killer and set out to explore the group’s most recent research site. There, Stetko falls through the ice to find the old Russian cargo plane. Pryce and Delfy join her to investigate, and they realize that the locked box had been opened and six cylinders removed. Pryce reveals that it is possible that nuclear fuel of interest to arms traffickers may be in the cylinders.
 frostbitten fingers amputated by Doc. She then finds the missing scientist hiding in her office. He tells her that he and his two companions found the plane and took the canisters, but the killer has them now. Before Stetko can protect him he is killed, but Carrie captures his killer who is revealed to be Australian biologist Russell Haden (Alex OLoughlin). The base commander orders everyone to evacuate because of the murders. With Haden locked in the brig and the winter storm near, Stetko and Pryce search for the canisters. 

Stetko checks the last departing planes cargo manifest and learns that the bodies of the dead scientists were not aboard. She searches their body bags and notices that the stitching on Weisss old wound matches the distinctive pattern on her amputated fingers. Stetko explores the body and finds several bags of large, uncut diamonds. Doc confesses that he was part of a  diamond smuggling ring with the others before Haden killed the rest. He had hoped that the diamonds would make him wealthy outside Antarctica. When Doc tells Stetko he wants to see the aurora australis one last time, she allows him to walk outside to his death.

Six months later, Stetko, Pryce, and Delfy have wintered at the facility. She transmits an email to her superior, rescinding her previous resignation and asking for a warmer location for her assignment.

== Cast ==
*Kate Beckinsale as Carrie Stetko, the Deputy U.S. Marshal investigating the killings at the base.    
*Tom Skerritt as Dr. John Fury, the base doctor.
*Columbus Short as Delfy, a pilot who helps Stetko in the investigation. 
*Gabriel Macht as Robert Pryce, a UN security agent who aids Stetko in the investigation.   
*Alex OLoughlin as Russell Haden, a biologist.   
*Shawn Doyle as Sam Murphy, the stations manager.
*Arthur Holden as McGuire
*Bashar Rahal as Russian Pilot
*Roman Varshavky as Russian guard

== Production == turnaround after a lack of production, and the rights were acquired by Universal Studios. The studio cast Reese Witherspoon to star in Whiteout, which would be based on the screenplay written by the Hoebers.   By May 2004, a second draft of the script had been written, and a director was still being sought.  Ultimately, rights over the film changed ownership, detaching Witherspoon from the project. 

In October 2006, Whiteout entered development at Dark Castle Entertainment with production slated to begin in the coming winter for a release date in the first quarter of 2008.  Dominic Sena, a fan of the graphic novel since its 98 debut, had sought to acquire the rights to direct a film adaptation, and when rights were acquired by Dark Castle, Sena petitioned to producer Joel Silver, president of the company, for the opportunity to direct Whiteout.  In February 2007, with Warner Bros. signed on to distribute Whiteout, Sena was hired to direct the film, based on the adapted screenplay by the Hoebers. In the same month, Beckinsale was cast in the lead role.    Production began on March&nbsp;5, 2007 in Manitoba, with later footage being shot in Montreal.  A set was also constructed on the shore of Lake Winnipeg.  The film was primarily set in a bright world of ice and sunlight, an unconventional approach to the murder mystery genre. Both real and fake snow were used in production. The author of the graphic novel, Greg Rucka, applauded the film adaptation of his source material, but upon seeing the finished film, felt differently, saying that "Comic Carrie and One Act Play Carrie would shake Movie Carrie down behind the bleachers, laugh her out of the You Share Our Name Club, and send her limping and mewling home to mother. And they wouldnt feel a moments regret about doing it, either.".   Filming concluded a few weeks before San Diego Comic-Con International|Comic-Con in July 2007. 

==Reception==

===Critical response=== average score normalized rating out of 100 to reviews from mainstream critics, the film received an average score of 28, based on 19 reviews.    Richard Roeper gave the movie 2 stars in the Chicago Sun Times calling it a "formulaic thriller that is ultimately no less predictable or interesting simply because it is set in the coldest and most isolated place on Earth."      Online critics at Zap2it claim, "The film moves like frozen molasses, letting the audience get out ahead of the narrative developments at every turn."   

===Box office===
The film was released to U.S. theaters on September 11, 2009. It was a box office bomb. The film continued to have major decreases in ticket sales, and has a gross of $10,275,638 to date.  It has grossed only $7,565,229 internationally to date bringing the total return to just $17,840,867 from a budget of $35 million.

==Release==
The theatrical release was on September 11, 2009. The film was released on DVD  and Blu-ray Disc on January 19, 2010. 

== References ==
 

== External links ==
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 