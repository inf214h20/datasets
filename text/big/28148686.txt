Out of the City
 
{{Infobox film
| name           = Out of the City
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Tomás Vorel
| producer       = Ondrej Trojan
| writer         = Tomás Vorel
| narrator       = 
| starring       = Tomás Hanák
| music          = 
| cinematography = Marek Jicha
| editing        = Vladimír Barák
| studio         = 
| distributor    = 
| released       = 5 October 2000
| runtime        = 104 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech comedy film directed by Tomás Vorel. It was released in 2000. Out of the City is Vorels fourth feature film.

==Plot==
A programmer named Honza decides to clear his head over the weekend from his hectic job, picks up his ex-wifes son, and together they set off on a trip into the countryside. Their civilization is highlighted in the film using mobile phone, laptop and digital camera. They become familiar with a charming country girl named Markéta and her grandmother, who live in a house with scented herbs and homemade liqueurs. A weekend trip stretches to several weeks stay in the small village. Honza and his son spend the most beautiful and gayest moments of their life in the country. The film is a parade of quirky rural characters. Honzas absence from work and his son from school is not without consequences, and father and son are forced to return to civilization. But Honza cannot forget his weeks in the country, or the girl Margaret.

==Cast==
* Tomás Hanák - Honza
* Barbora Nimcová - Markéta Michal Vorel - Honzík
* Anezka Kusiaková - Anicka
* Ljuba Skorepová - Granny
* Bolek Polívka - Ludva
* Eva Holubová - Vlasticka
* Leos Sucharípa - Bee keeper
* Alena Stréblová - Helena
* David Vávra - Jarda
* Jirí Burda - Kocí (as Jirí Hruska)
* Milan Steindler - Hunter
* Radomil Uhlir
* Petr Ctvrtnícek
* Tomás Vorel Jr. - Ludvas Son
* Daniela Kolárová - Markétas mother

==Music== 
In the film we see several original songs, Cesta z města (The Way of the City), Dva skřítci (Two Elves), and also a choral passage of the Easter responsory O Filii et Filiae by Jean Tisserand (only a Czech text is used here, the music differs from the original). 

==References==
 

==External links==
*  

 
 
 
 