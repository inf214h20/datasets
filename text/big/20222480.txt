The Show Must Go On (film)
{{Infobox film
| name           = The Show Must Go On
| image          = The Show Must Go On film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =    
 | hanja          = 우아한  
 | rr             = Uahan segye
 | mr             = Uahan segye}}
| director       = Han Jae-rim
| producer       = 
| writer         = Han Jae-rim
| starring       = Song Kang-ho Oh Dal-su Yoon Je-moon Choi Il-hwa Park Ji-young
| music          = Yoko Kanno
| cinematography = Park Yong-soo
| editing        = Kim Sun-min
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 112 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} 2007 South Korean film.

== Plot ==
In-gu is a gangster but also a husband and father who dreams of moving his family out of their drab apartment and into a bigger home. Shunned by his daughter and nagged by his wife to get a respectable job, In-gu nonetheless perseveres down his chosen path to provide for his family. But his family life starts to get in the way of his business, and to make matters worse he is plotted against by the younger brother of his boss which favors In-gu for his abilities to enforce territorial rights.

== Cast ==
* Song Kang-ho ... In-gu
* Park Ji-young ... Mi-ryung
* Kim So-eun ... Hee-soo
* Oh Dal-su ... Hyun-soo
* Yoon Je-moon ... Director Noh
* Choi Il-hwa ... Chairman Noh 
* Choi Jong-ryul ... owner of dumpling restaurant
* Jung In-gi ... police chief
* Kim Kyeong-ik ... Hee-soos teacher
* Kwon Tae-won ... police chief of police substation
* Lee Dae-yeon ... company president Baek
* Oh Jung-se ... manager
* Lydia Park ... female employee
* Lee Yong-yi ... granny in broken-down home
* Min Seong-wook ... Director Nohs subordinate
* Son Se-bin ... daughter of woman in mourning
* Park Jin-woo ... reporter
* Lee Jang-hoon ... Yong-seok

== Release ==
The film was released in South Korea on April 5, 2007, and received a total of 1,025,781 admissions nationwide. 

== Awards ==
The Show Must Go On won Best Film and Best Actor for Song Kang-ho at the 28th Blue Dragon Film Awards in 2007. 

== References ==
 

== External links ==
*  
*  
*  
*  
*   at Koreanfilm.org

 
 
 
 
 

 
 
 
 
 
 
 
 
 


 
 