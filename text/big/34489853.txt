Buffalo Girls (film)
{{Infobox film
| name           = Buffalo Girls
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Todd Kellstein
| producer       = 
Lanette Phillips  
Jonathon Ker 
Co-producer:
Michael J. Pierce 
Executive producers:
Paul Rachman  
Michael Raimondi  
Noah Haeussner 
Associate producers:
Nopporn Jumpon  
Takako Yagi  
Charlie Lyons
| music          = Scott Hackwith
| editing        = Zimo Huang
| studio         = Buffalo Girls Movie, in Association with Union Entertainment Group
| distributor    = 
| released       = January 22, 2012
| runtime        = 64 minutes
| country        = United States
| language       = Thai 
With English subtitles
| budget         = 
| gross          = 
}}
Buffalo Girls is a documentary film about two eight-year-old Thai girls who engage in professional Muay Thai boxing in rural Thailand.  The film was directed by Todd Kellstein and produced by Lanette Phillips and Jonathon Ker. It had its World Premiere at the Slamdance Film Festival on January 22, 2012.  The film was singled out in several previews of the festival, including those by the Los Angeles Times,  the Salt Lake City Weekly  and the Park Record.  
Film critic Kenneth Turan of the Los Angeles Times called the film "unexpected and fascinating."     The film received a favorable review in the Hollywood Reporter,  and was reviewed locally by Salt Lake Magazine,  and SLUG Magazine 

==Plot==
The documentary is an intimate look into the lives of two very young, professional Muay Thai fighters: Stam Sor Con Lek and Pet Chor Chanachai.  At only eight years old, the two girls fight throughout rural Thailand to earn money to support their families, as well as trying to secure the 22-kilogram Muay Thai Championship belt of Thailand. The film also addresses the culture of childrens fighting through interviews with the childrens parents, the referees who officiate the fights, and the professional gamblers who bet on them.

The impoverished farming communities of rural Thailand offer few opportunities for economic gain and boxing is one of the few alternatives to the country’s commercial sex trade as a means of escaping the extreme poverty. Child boxers in Thailand can often earn a family’s monthly rent from a single bout, sometimes taking home more than what a farmer or factory worker earns in a month.  For the film’s young protagonists, boxing is an opportunity to help their parents supplement the family income and improve their standard of living. 

==Production==
Filmmaker Todd Kellstein was working on a documentary about Thai prisons when he came across children boxing in a rural province.  Kellstein became enamored with what he saw and enlisted the help of producer Lanette Phillips and co-producer Michael J. Pierce to help finance the production of the film.  What he thought would be a six-week project required a three-year stay in Thailand, in order to understand the culture, learn Thai, and earn the trust of his young subjects.   In researching the film, the filmmaker attend close to 300 fights in different provinces around the country.   Prior to Buffalo Girls, Kellstein’s film background was in directing music videos.  It was during his time as a music video director where Kellstein met Phillips and Pierce, as well as Jonathon Ker, the filmss other producer.  Kellstein had worked with prominent artists such as Bon Jovi, but he went to Thailand looking for something else.  He told the Los Angeles Times that, initially, he thought he was witnessing child abuse.  During the process of making the film, however, he changed his mind and sought to create a balance in presenting the potentially controversial subject matter.
  

== References ==
 

== External links ==
*  
*   at IMDB
*  
*  
* Film Threat interview  
*MSNBC News Story   ]

 
 
 
 
 
 