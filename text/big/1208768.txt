Someone Like You (film)
 
 
{{Infobox film
| name = Someone Like You
| image = Someone Like You film.jpg
| caption = Theatrical release poster
| director = Tony Goldwyn 
| writer = Elizabeth Chandler Laura Zigman  
| starring = Ashley Judd Greg Kinnear Hugh Jackman Marisa Tomei Ellen Barkin 
| producer = Lynda Obst 
| cinematography = Anthony B. Richmond
| distributor = 20th Century Fox 
| budget = $23 million
| gross = $38,689,940 
| released =  
| country = United States
| runtime = 97 minutes
| language = English
}}

Someone Like You is a 2001 romantic comedy film, based on Laura Zigmans novel Animal Husbandry which tells of a heartbroken woman who is looking for the reason she was dumped. The film stars Ashley Judd, Greg Kinnear, Hugh Jackman, Marisa Tomei and Ellen Barkin and was directed by Tony Goldwyn.

==Plot==
The movie starts with a voice-over of Jane Goodale (Ashley Judd) over the images of a scientific experiment with a bull and a herd of cows, apparently the bull never mounts a cow twice, not even if her scent is changed. He prefers a new cow. Jane says that until recently, she believed that men are all like the bull, but to understand what happened we have to go back in time. Jane is a production assistant with a talk show that has recently been syndicated, which means that the host Diane Roberts (Ellen Barkin) who wants to be the best, is always looking for the ungettable guests, like Fidel Castro. Eddie Alden (Hugh Jackman) is the producer of the talk show and is a womanizer, much to the dismay of Jane who comments on his actions with amicable critique.

Eddie is looking for a roommate, but his messages on the bulletin board are sabotaged by disgruntled exes. He wonders whether Jane would like to move in, but she turns him down with a vengeance. Then they meet the new producer of the show, Ray Brown (Greg Kinnear), and Jane is immediately smitten. She tells her friend Liz (Marisa Tomei) and discusses her bad luck with men. Meanwhile her sister is trying to get pregnant with a fertility program. Ray calls Jane and they spend an evening together and end up kissing. The next morning she calls Liz and is ecstatic. Liz gives her some advice as to how to deal with Ray and his girlfriend Dee (with whom he has trouble). Ray and Jane seem to be very much in love. 

The relationship evolves, and they decide to move in together. Jane puts in her notice; Ray goes to tell his girlfriend that it is over, but doesnt tell her about the new woman in his life. Ray starts to get distant while Jane is packing to move over, and over dinner he breaks it off, leaving Jane in tears. The next morning in the office Jane takes her revenge by announcing to move in with Eddie. She learns to deal with the many women in Eddies life. The two bond over scotch and leftover Asian food. She reads an article about the old cow syndrome and starts researching for her theory of men. Liz works at a magazine for men and needs a columnist and persuades Jane to write a column about her theory, under the pen name Dr. Marie Charles.

The column that deals with the insecurity and dishonesty of men is a big hit. Everybody wants to meet Dr. Charles, so does Diane. At a Christmas party, Ray tells her he misses her and asks her out for New Years Eve, but he stands her up. When Jane shows up at a party looking for Eddie at midnight, she cant find him and leaves in tears. Eddie tries to go after her but cant find her. Back at the office, Ray tries to apologize, but Diane interrupts wearing a shirt Jane bought for Ray. Jane realizes she is "Dee," and Ray is back with her. A board meeting is going to start and Eddie makes sure Jane isnt crying going in, but Jane is distracted so he covers for her. When Ray shows emotion over a Gérard Depardieu movie, Jane spills her guts over his inability to show empathy for her broken heart. Diane, unaware of Janes relationship with Ray, gives her advice on how to win her boyfriend back, telling her how she got hers back.

At a bar, Liz follows Dr. Charless advice and is not going to fall for the same kind of guy anymore. Jane and Eddie get into an argument over the advice of Dr. Charles, back home she tells him she has to believe the theory because otherwise she is afraid that men dont leave women - they leave her. Eddie comforts her by saying Ray is not the last man shell ever love, and they fall asleep together. The next morning Eddie wakes up happy and comfortable while Jane freaks out. Eddie tells her not to analyze this too. He is happy that he slept the whole night with her without it leading to sex. She tells Eddie he will show his true colours and will hurt her some time soon. Eddie tells her it is not about him, but it is her attitude that is the problem. Jane gets a call and it is her brother-in-law telling her that her sister suffered a miscarriage. At the hospital she sees the true love between them and decides to tell Diane that Dr. Charles is going to be on her show.

The interview is meant to be over the telephone, but Jane changes her mind and goes on the stage. She tells the audience that there is no Dr. Charles (we see Eddie leave at this point) and that the theory is ridiculous because she was hurt and needed to blame men for her pain. By this time Eddie is long gone. Jane goes after Eddie and tells him she found new love. He doesnt answer but kisses her with passion, with the dulcet tones of Van Morrisons "Someone Like You" playing in the background.


==Cast==

*Ashley Judd as Jane Goodale, a television show producer who is looking for love and the problems men have with giving it.
*Greg Kinnear as Ray Brown, the shows hunky executive producer. He dumps Jane right before they were supposed to move in together.
*Hugh Jackman as Eddie Alden, a womanizing coworker of Janes.
*Marisa Tomei as Liz, Janes best friend.
*Ellen Barkin as Diane Roberts, the star talent of Janes show.
*Catherine Dent as Alice, Janes sister.
*Peter Friedman as Stephen, Alices husband.
*Laura Regan as Evelyn.

==Reception==
The film opened at #2 at the North American box office making $10,010,600 in its opening weekend, behind Spy Kids. Overall the movie grossed over $38,689,940 worldwide.    consensus was "A light and predictable, if somewhat shallow, romantic comedy thats easy to sit through because of the charming leads." The score among critics was tallied at 41%.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 