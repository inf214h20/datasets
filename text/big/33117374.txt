Once Upon a Time in the East (2011 film)
 
{{Infobox film
| name           = Once Upon a Time in the East
| image          = Oute21.jpg
| alt            =  
| caption        = Theatrical film poster
| director       = Demir Yanev
| producer       = Demir Yanev
| story          = Georgi Stoev
| starring       = 
| music          = Vasil Genev
| cinematography = Martin Balkansky
| editing        = Lubomir Todorov
| studio         = 
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
| gross          = 
}}

Once Upon a Time in the East is a Bulgarian documentary film based on the books by the late Georgi Stoev. The feature length documentary tells the story of the Bulgarian transition period and includes reconstructions, TV archive from Bulgarian National Television (BNT), NOVA TV, bTV and features interviews with contemporary Bulgarian figures like Palmi Ranchev, Atanas Orachev, Rumen Leonidov, Haralan Alexandrov, Andrey Pantev and Georg Kraev. Other witnesses and participants in the power structures tell their stories of the establishment of VIS & SIC and the fierce brutality that is required to survive the days of the transition.

Produced and directed by Demir Yanev the film was chosen to participate in the Al Jazeera Documentary Film Festival in April, 2011 and also in the Golden Rhyton in Plovdiv.

==Plot==
Bulgaria being part of the Eastern bloc joined the race for freedom and democracy during the 1990s. Thousands flooded the streets speaking their minds with blistering hopes of developing new democratic forms of society. Within a few years the new systems of government transformed released criminals, prisoners and other sportsmen, mainly wrestlers into groups of street gangs, racketeers and security companies. With these gangs of thugs the newly appointed democrats privatized the country... 20 years later one man stood out to tell his and their story...

A former wrestler himself – Georgi Stoev.

==Archive==
Selected parts of Georgi Stoev’s last TV interviews with Bulgaria’s E- host lead the narrative of this film. Other TV archive material with the most spectacular contract killings in Bulgaria illustrate the heavy criminal transition in Bulgaria from the late 1990s up to present day.

==Reconstructions==
Reconstructions portray meetings and events described in Georgi Stoev’s books. With a precise cast of characters almost every hero in Georgi Stoevs books takes part in this film. Secret meetings between General Gotzev and Mladen Mihalev  - Madjoe mark a deep blueprint in the film’s plot. Poly Pantev & The Jenya’s rise to power in the SIC also take a leap into the storyline as do Georgi Stoevs own stories and ambitions.   
A dynamic editing style and Vasil Genev’s own music additionally highlight a structure of portraying Georgi Stoevs agonic clash with the police and the prosecution and furthermore deconstructs several secret, allegorically coded characters from Christo Kalchev’s books.

 

==Crew==
After several unsuccessful attempts with different crews the production came to a halt for an uncertain period of time during the early autumn of 2009. Not after too long, however, producer, director Demir Yanev finds Martin Balkansky (DoP) and Vlado Kaloyanov (Sound) and the filming resumes once again. Heading through time and all kinds of troubles including weather and financials the film completed its production in early winter 2009. With two additional filming days one during March & one during May the film’s editing and post production phase ended in August, 2010.

==External links==
*  
*   new level productions - Sofia, Bulgaria

 
 
 
 
 
 
 