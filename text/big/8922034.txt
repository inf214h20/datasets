Adieu Blaireau
{{Infobox film
| name           = Adieu Blaireau
| image          = adieublaireau.jpg
| caption        = Theatrical Poster
| director       = Bob Decout
| producer       =
| writer         = Bob Decout
| narrator       =
| starring       = Philippe Léotard Annie Girardot Juliette Binoche
| music          = Adrien Nataf
| cinematography = Serge Halsdorf
| editing        = Sophie Bhaud
| distributor    = A&M Films
| released       = 30 April 1985
| runtime        = 120 minutes
| country        = France French
| budget         =
| preceded by    =
| followed by    =
}} 1985 film directed by Bob Decout.

The picture stars Philippe Léotard, Annie Girardot and Juliette Binoche and premiered at the 1985 Cognac Festival du Film Policier.

== Cast ==
* Philippe Léotard : Fred
* Annie Girardot : Colette
* Jacques Penot : Gégé
* Christian Marquand : Victor
* Juliette Binoche : Brigitte B., aka "B.B"
* Amidou : Poupée
* Albert Dray : Boris
* Yves Rénier : "Professeur"
* John Dobrynine : Killer
* Agathe Gil : Gigi
* Pierre Arditi : La Grenouille
* Hubert Deschamps : Drunk
* Serge Marquand : Cafe owner
==Soundtrack==
The song "Mama", sung by the French singer Janet (French singer)|Janet, known for the 1972 song "Bénie Soit La Pluie" / "Le Chocolat", was released as a single in 1985.

==References==
 
==External links==
*  .

 
 
 

 