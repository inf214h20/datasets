Richard III (1912 film)
{{Infobox film
| name = Richard III
| image = Richard III 1912 Poster.jpg
| image_size =
| caption = James Keane
| producer = J. Stuart Blackton M.B. Dudley
| writer = James Keane William Shakespeare
| narrator =
| starring = Frederick Warde Robert Gemp
| music =
| cinematography =
| editing =
| distributor = States Rights Independent Exchanges
| released = October 15, 1912
| runtime = 55 min.
| country = France United States English
| budget = $30,000 (estimated)
| gross =
| preceded_by =
| followed_by =
}} the title 1699 adaptation James Keane, who also served as co-director and actor (playing Richmond).
 nitrate film hand tinting effect used in the original 1912 release.

When the film was released in the US, actor Frederick Warde would often appear at screenings, giving a short lecture, and then reading extracts from the play during the changing of the reels. The film itself begins with Warde, in modern dress, emerging from behind a theatrical curtain and bowing, and concludes with him bowing again, and returning behind the curtain. The film also features two scenes from 3 Henry VI (the murder of Prince Edward and Richards murder of Henry VI).   
 Kino International released the film on DVD, with a newly composed score by Ennio Morricone, and a 17-minute documentary film "Rediscovering Richard: Looking Back on a Forgotten Classic".

==Cast==
*Robert Gemp ... King Edward IV
*Frederick Warde ... Richard, Duke of Gloucester Albert Gardner Prince Edward of Lancaster James Keane ... Earl of Richmond
*George Moss ... Tressel
*Howard Stuart ... Edward
*Virginia Rankin ... York
*Violet Stuart ... Lady Anne Plantagenet
*Carey Lee ... Queen Elizabeth
*Carlotta De Felice ... Princess Elizabeth

==See also==
*List of rediscovered films

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 