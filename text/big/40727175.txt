Silvery Moon
{{Infobox Hollywood cartoon|
| cartoon_name = Silvery Moon
| series = Aesops Fables (film series)|Aesops Fables
| image = 
| caption = 
| director = John Foster Mannie Davis
| story_artist = 
| animator = 
| voice_actor = 
| musician = 
| producer = Amadee J. van Beuren
| studio = Van Beuren Studios
| distributor = RKO Radio Pictures
| release_date = January 13, 1933
| color_process = Black and white
| runtime = 6:01 English
| preceded_by = Golden Goose
| followed_by = A.M. To P.M. 
}}

Silvery Moon, also known by its alternate title Candy Town, is a short animated film by the Van Beuren Studio and as part of the Aesops Fables cartoon series. The story appears to be inspired by the story of Hansel and Gretel, published by the Brothers Grimm, albeit having a less dark scenario.

==Summary== Moonlight Bay. The cartoon then opens with a feline couple boating in a bay. The boy cat paddles the boat while the girl cat sings the chorus of the aforementioned song. The boy cat tells his partner that the moon is made of cheese. But the girl cat objects and says it is made of desserts and other sweet stuff. Appreciating their views, the moon becomes animated, and therefore conjures a stairway to it. The cats then walk up the steps.

The environment of the moon appears a lot like how the girl cat described it. Things like cakes, candy canes and ice cream are a common sight. After eating some of the scenery, the cats play some of the musical instruments which are also present. They then come to a table with fruits on top where they resume eating. Meanwhile, a hostile animated bottle of castor oil and spoon approach. Though they are stuffed with sweets, the cats are able to run. The bottle and spoon chase them across the lunar terrain until they decide to jump off an edge.

After leaping from the moon, the cats drop back into the bay but are unharmed. Near to them is their boat which they climb back on. Despite the frightening chase, the cats enjoyed the experience as they give thanks to the animated moon.

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 
 
 
 
 
 


 