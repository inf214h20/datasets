Swing (2003 film)
{{Infobox Film
| name           = Swing
| image          = Swing 2003 film.jpg
| caption        = 
| director       = Martin Guigui
| producer       = Debbie Brubaker Elizabeth Estrada John Harvey Mary Keil Ned Kopp Ken Patton Jan St. John Dahlia Waingort Jenni Gold
| writer         = Mary Keil 
| starring       = Constance Brenneman Innis Casey Tom Skerritt Jacqueline Bisset Jonathan Winters Nell Carter Dahlia Waingort Adam Tomei Barry Bostwick Mindy Cohn 
| music          = Gennaro Cannelora, Israel Tanenbaum   
| cinematography = Massimo Zeri  
| editing        = Charles B. Weber
| distributor  =    
| released       = 2003
| runtime        = 
| country        =  United States English 
}}
Swing is an American romantic comedy film starring Constance Brenneman, Innis Casey, Tom Skerritt, Jacqueline Bisset, Jonathan Winters, Nell Carter, Dahlia Waingort, Adam Tomei, Barry Bostwick, Mindy Cohn and directed by Martin Guigui.

==Plot==
Anthony is caught between dreams of being a musician and pleasing his father and fiance. Encouraged by his great uncle, Anthony finds inspiration from a mysterious older woman in an other worldly night club, who teaches him to find happiness through swing dancing.

==Reception==
The film received a 42% rating from Rotten Tomatoes. 

== Sources ==
*http://www.imdb.com/title/tt0358722/
*http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2005/02/11/DDG8QB8I041.DTL
*http://www.nytimes.com/2004/07/09/movies/film-review-a-sexy-ghost-can-be-a-great-dance-partner.html
*

 
 
 
 


 