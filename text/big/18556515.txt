From the Clouds to the Resistance
{{Infobox Film
| name           = From the Clouds to the Resistance
| image          = 
| image_size     = 
| caption        = 
| director       = Danièle Huillet Jean-Marie Straub
| producer       = Danièle Huillet Jean-Marie Straub
| writer         = Cesare Pavese Danièle Huillet Jean-Marie Straub
| narrator       = 
| starring       = Olimpia Carlisi
| music          = 
| cinematography = Giovanni Canfarelli Modica Saverio Diamante
| editing        = 
| distributor    = 
| released       = 7 November 1979
| runtime        = 104 minutes
| country        = Italy 
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Italian Dalla nube alla resistenza) is a 1979 Italian drama film directed by Danièle Huillet and Jean-Marie Straub.    It competed in the Un Certain Regard section at the 1979 Cannes Film Festival.   

==Cast==
* Olimpia Carlisi - The Cloud Guido Lombardi - Issione
* Gino Felici - Ippoloco
* Lori Pelosini - Sarpedonte
* Walter Pardini - Edipo
* Ennio Lauricella - Tiresia Andrea Bacci - 1st hunter
* Loris Cavallini - 2nd hunter
* Francesco Ragusa - Litierse
* Fiorangelo Pucci - Eracle
* Dolando Bernardini - Father
* Andrea Filippi - Son
* Mauro Monni - The Bastard
* Carmelo Lacorte - Nuto
* Mario di Mattia - Cinto

==References==
 

==External links==
* 

 

 
 
 
 
 
 