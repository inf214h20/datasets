Rites of Passage (1999 film)
 
{{Infobox Film
| name           = Rites of Passage
| image          = Rites of Passage.jpg
| image_size     = 
| caption        = 
| director       = Victor Salva
| producer       = 
| writer         = Victor Salva
| narrator       = 
| starring       = Dean Stockwell James Remar Jason Behr
| music          = 
| cinematography = 
| editing        = 
| distributor    =  1999
| runtime        = 95 min.
| country        =   English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} thriller film written and directed by Victor Salva. It stars Dean Stockwell, James Remar, and Jason Behr.

==Plot==
The film begins with two recently escaped convicts &mdash; Frank (James Remar) and Red (Jaimz Woolvett) &mdash; approach a group of campers.  The elder of the pair shoots and kills the campers with single rapidfire shots.

D.J. Farraday (Robert Glen Keith) discovers that his father, Del (Stockwell), has been having an affair. D.J. asks Del to meet him at the familys cabin by the lake, where D.J. intends to confront him about his adultery. When the two arrive at the summer house, they find the younger son, Campbell "Cam" Farraday (Behr), already there. Eventually it is revealed that Del had found Cam and his boyfriend, Billy, embracing there at the cabin. Del brutally beat Billy, and father and son have not spoken since.  Cam seems determined to leave the confrontational situation.  While D.J. is trying to convince Cam to stay and attempt a reconciliation, Cam reveals to him that Billy is dead, and the clear implication is that Cam blames his father for the loss.

A short while later, the two escaped convicts show up at the cabin and ask to use the phone, claiming that their car has broken down.  Red, who is introduced as Franks adult son, makes a point of taking up the hospitality offered, using that excuse to remain in the house (over Franks objection.)  As the evening progresses Frank seems to be forcing Del into a challenge.  Tensions are high, as Frank makes his play for Alpha of the house.  At one point, D.J. (Del Junior) goes so far as to suggest that "we all just whip em out and get this over with."

The police show up looking for the two escaped convicts, and things get tense very quickly.  It soon becomes clear that Cam knew the convicts and has had some sort of entanglement with them.  From there, the twists and turns begin to thicken, and the onionskin layers of this tale are revealed.  The family must reconcile and put aside their issues with each other to deal with the menacing force of Frank.

==Production and releases==
This was the first film from Salva since the controversy surrounding his film, Powder (film)|Powder. Salva based much of the dialogue between Del, D.J. and Cam on tense conversations he and his own father had while Salva was growing up. The production took 18 days to film. Co-star Jason Behr received the script only two weeks before shooting began.

Two versions of the film have been released. A directors cut of the film with commentary by Salva and Behr was released by Bell Canyon Entertainment on May 2, 2000. This version of the film features several deleted scenes which further explain the relationships between the major characters. A theatrical release version was released by Wolfe Video on August 30, 2000. This edition contains no commentaries.

==References==
*Harvey, Dennis. "Rites of Passage (review)." Variety. June 28, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 