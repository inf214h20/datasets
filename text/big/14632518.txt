The Little American
 
{{Infobox film
| name           = The Little American
| image          = The Little American 1917.jpg
| caption        = Film poster
| director       = Cecil B. DeMille Joseph Levering
| producer       = Cecil B. DeMille Mary Pickford
| writer         = Jeanie MacPherson Cecil B. DeMille (uncredited) Clarence J. Harris (uncredited)
| screenplay     = Jeanie MacPherson (uncredited)
| starring       = Mary Pickford
| cinematography = Alvin Wyckoff
| editing        = Cecil B. DeMille
| distributor    = Artcraft Pictures Corporation
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = United States dollar|$166,949.16   
| gross          = $446,236.88 
}}
 
  silent romance romantic war war drama film directed by Cecil B. DeMille. The film stars Mary Pickford (who also served as producer) as an American woman who is in love with both a German and a French soldier during World War I. A print of the film is housed at the UCLA Film and Television Archive and has been released on DVD. 

==Plot== Jack Holt) lives in America with his German father and American mother. He notices a young lady, Angela More (Mary Pickford). As she is celebrating her birthday on the Fourth of July of 1914, she receives flowers from the French Count Jules De Destin (Raymond Hatton). They are interrupted by Karl, who also gives her a present. They soon battle for Angelas attention. To lose his competition, Count Jules arranges for Karl to be sent to Hamburg, where he will have to join his regiment. Angela is crushed when he announces he has to leave. The next day, Angela reads in the paper the Germans and French are at war and 10,000 Germans have been killed already.

Three months pass by without a word from Karl. Karl is wounded in the fighting. Word spreads that Germany will sink any ship which is thought to be carrying munitions to the Allies. Angela is aboard one of those ships when it is hit. Angela saves herself by climbing on a floating table and begging the attackers not to fire on the passengers. Angela is eventually rescued.

After weeks of ceaseless hammering from the German guns, the French fall back on Vangy. Angela arrives in Vangy as well to visit her aunt, only to discover she has died. The Old Prussians are bombing the city and Angela is requested to flee. However, she is determined to stay to nurse the wounded soldiers. Meanwhile, the Germans enter the chateau with the intention of getting drunk and enjoying themselves with the young women. A French soldier tries to help Angela escape, but she is unwilling to. He next asks her to let a French soldier spy on the Germans and inform the French via a secret hidden telephone. Angela is afraid, but gives them permission.

The Germans are intent on raping Angela, who is the only person in the mansion not to be hidden. She reveals herself to be an American to save herself, but they do not believe her. Angela attempts to run away and hide, but is discovered by a German soldier who turns out to be Karl. Angela orders him to save the other women in the house, but Karl responds he cannot give orders to his fellow Germans. She realizes there is nothing she can do. With permission to leave the mansion, she witnesses the execution of the French soldiers. She is heartbroken and decides to go back in for revenge.

Angela secretly calls the French with the hidden telephone and informs them that there are three gun holders near the chateau. The French prepare themselves and attack the Germans. The Germans realize someone is giving the French information and Karl catches Angela. He tries to help her escape, but they are caught. The commander orders that Angela be shot. When Karl tries to save her, he is to sentenced to be executed as well for treason. As the couple face death, the French bomb the mansion, enabling Angela and Karl to escape. They are too weak to run and collapse near a statue of Jesus. The next day, they are found by French soldiers. They initially want to shoot Karl, but Angela begs them to set him free. They eventually allow her to fly back to America with Karl by her side as a German prisoner.

==Cast==
* Mary Pickford as Angela More Jack Holt as Karl Von Austreim
* Raymond Hatton as Count Jules De Destin
* Hobart Bosworth as German Colonel Walter Long as German Captain
* James Neill as Senator John Moore Ben Alexander as Bobby Moore
* Guy Oliver as Frederick Von Austreim
* Edythe Chapman as Mrs. Von Austreim
* Lillian Leighton as Angelas Great Aunt
* DeWitt Jennings as English Barrister 
* Wallace Beery as German soldier (uncredited)
* Gordon Griffith as Child (uncredited)
* Ramon Novarro as Wounded Soldier (uncredited)
* Colleen Moore as Nurse (uncredited)
* Norman Kerry as Wounded Soldier (uncredited)
* Sam Wood as Wounded Soldier (uncredited)

==Reception== declared war on Germany earlier in 1917, the Chicago Board of Censors initially blocked exhibition of the film in that city, calling it anti-German and suggesting that showing it could start a riot.  Artcraft challenged the Board in state court and, after a jury trial, the refusal of the board to issue a permit despite a court order, and the denial of a second appeal by the board, won the right to show the film in Chicago. 

==See also==
* Mary Pickford filmography
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 