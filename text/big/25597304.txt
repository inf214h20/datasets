Salad by the Roots
{{Infobox Film
| name           = Salad by the Roots
| image          = Salad by the Roots.jpg
| caption        = 
| director       = 
| producer       = Georges Lautner
| writer         = 
| starring       = Louis de Funès (Jockey Jack)  Michel Serrault (Jérôme Martinet)  Mireille Darc (Rocky-la-Braise)  Maurice Biraud (Jo Arengeot)  Francis Blanche (Loncle Absalon).  
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1964 (France)
| runtime        = 
| country        = France French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} French comedy in black and white.

==Plot==
When he got out of prison, Jo (Maurice Biraud) finds out that his love "Rocky" (Mireille Darc) was cheating on him with Jockey Jack (Louis de Funès). To get his revenge he sends his man "Pommes-Chips" to kill Jockey. But accidentally Jockey kills "Pommes-Chips". He now has to hide the dead man, so he hides him in a big violin case belonging to his cousin Jerome (Michel Serrault). But inside the vest of the dead man are 3 future winning horse race numbers.

The dead man is taken everywhere inside the violin case and in a comedic way every time someone was about to open it a miracle comes by and it stays closed, until an old drunk lady discovers and spreads the news. The body inside the case is taken by Jockey and his cousin Jerome to Jeromes house where he lives with his aunt and uncle (Francis Blanche) who happens to work in the coffin business and uses the same type of violin cases to transport dead people. The body of "Pommes-Chips" is presumably buried by the uncle who takes his jacket.

Later on, Jo, who fell back in love with his mistress "Rocky", watches the race on television and the numbers whom the dead "Pommes-Chips" had in his vest finished in the first 3 positions with a reward of about 1800000 francs. Then starts the race after "Pommes-Chips" jacket, Jo sends "Rocky" to know what might have happened to it from Jerome. She kisses him many times and tries to seduce him.

Later on, Jo knew that the uncle holds the jacket, takes the supposed winning numbers from the jacket and hurries to submit. The worker looks at the numbers and laughs, theyre the wrong ones. Jo leaves disappointed. Seconds later Jerome enters and submits the card he holds with the 3 winning numbers, and goes with the prize. Jo tries his luck many more times in the horse race and is caught by the police for bankruptcy. He sadly says goodbye to "Rocky", and when the police takes him away, she starts running and laughing and goes to hug Jerome and they go together in a "new" convertible car while still laughing.

==References==
 

==External links==
*  

 
 
 
 
 
 

 