Suspense (1913 film)
 
{{Infobox film
| name           = Suspense
| image          = Suspense (1913 film).jpg
| caption        = Film still
| director       = Phillips Smalley Lois Weber
| producer       =
| writer         = Lois Weber
| starring       = Lois Weber Val Paul
| cinematography =
| editing        = Universal Film Manufacturing Company
| released       =  
| runtime        = 10 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 silent short thriller directed Valentine Paul. Lon Chaney split screen shot. 

A print of the film is preserved at the film archive of the British Film Institute.   
 

==Cast==
* Lois Weber as The Wife Valentine Paul as The Husband
* Douglas Gerrard as The Pursuer
* Sam Kaufman as The Tramp Lon Chaney as A Hobo (uncredited/unconfirmed)

==Plot==
A servant leaves a new mother with only a written letter of notice, placing her key under the doormat as she leaves.  Her exit attracts the attention of a tramp to the house.  The husband has previously phoned that he is working late, the wife decides not to ring back when she finds the note, but does ring back when she sees the tramp.  Her husband listens horrified as she documents the break in, then the tramp cuts the line.  The husband steals a car and is immediately pursued by the cars owner & the police, who nearly but not quite manage to jump into the stolen car during a high-speed chase.  The husband manages to gain a lead over the police but then accidentally strikes a man smoking in the road, and checks that he is OK.  Meanwhile the tramp is breaking into the room where the wife has locked herself and her baby, violently thrusting himself through the wood door, carrying a large knife. 

==Lon Chaney connection==
The film has been asserted as Lon Chaneys earliest extant film based on a brief scene in which a similar individual appears on camera. Jon C. Mirsalis writes "Some Chaney fans have doubted that it is actually Chaney in the film, but close examination of a high quality 16mm print clearly showed that the Hobo is played by Chaney."    The documentary Lon Chaney: A Thousand Faces states that his film debut occurred after his wifes suicide attempt in April 1913 and "his earliest films were made at the first studio to open in Hollywood,   and Max Dills company in San Francisco, California in September 1912.    Blake specifically dismisses Chaneys appearance in Suspense in his book, A Thousand Faces: Lon Chaneys Unique Artistry in Motion Pictures. 

==Release==
Suspense was released on July 6, 1913 by the Rex Motion Picture Company.    

A print of the film is preserved at the film archive of the British Film Institute.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 