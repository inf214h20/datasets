The Game (2010 film)
{{Infobox film
| name           = The Game
| image          = The Game 2009 film.jpg
| alt            = 
| caption        = Release Poster
| director       = Frank Rajah Arase
| producer       = Abdul Salam Mumuni
| writer         = Frank Rajah Arase
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Heroes Productions
| released       = 
| runtime        = 
| country        = Ghana   Nigeria
| language       = English
| budget         =
| gross          =
}}
The Game is a 2010 Ghanaian Nigerian thriller film directed by Frank Rajah Arase, starring Majid Michel, Yvonne Okoro and Yvonne Nelson.  

==Cast==
*Majid Michel as Teddy Elbert
*John Dumelo as Ronnie Lawson
*Yvonne Okoro as Brandy
*Yvonne Nelson as Shennel Johnson
*Beverly Afaglo as Detective
*Nadia Archer Kang as Jackie Oppong
*Johannes Maier as Bill
*Lion De Angelo as Fred
*Fred Nuamah as Jake Freeman
*Ebi Bright as Letoya Benson

==Reception==
It got a 3 out of 5 star rating from Nollywood Reinvented who questioned the originality of the storyline, as the film seems copied from two 2008 Bollywood films; Race (2008 film)|Race and Ghajini (2008 film)|Ghajini.  Nollywood Forever gave it a 58% rating. While the reviewer found the cinematography to be of high quality, he found the plot too convoluted and confusing. 

==References==
 

 
 
 
 
 
 
 


 
 