The Duchess and the Dirtwater Fox
{{Infobox film
| name           = The Duchess and the Dirtwater Fox
| image          = duchess_and_the_dirtwater_fox_movie_poster.jpg
| caption        = Theatrical poster
| director       = Melvin Frank
| producer       = Melvin Frank Jack Rose
| narrator       = 
| starring       = Goldie Hawn George Segal Charles Fox
| cinematography = Joseph F. Biroc
| editing        = Frank Bracht 
Bill Butler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $4,590,000 
| gross = $4,120,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
}}
The Duchess and the Dirtwater Fox is a 1976 Western romantic comedy film starring Goldie Hawn and George Segal, produced, directed and co-written by Melvin Frank.

==Plot==
A popular dance hall girl, Duchess, joins with a gambler nicknamed the "Dirtwater Fox" on the way to Salt Lake City, Utah. Seeking refuge from a pursuing gang of outlaws, the Duchess and the Dirtwater Fox join a wagon train of Mormons. On their trip, they encounter snakes, rapids, horseback pursuits through towns, and even getting tied up by the outlaws (eventually escaping).

==Production==
According to the closing credits, much of the film was shot in and around the historic community of Central City, Colorado.  Matte paintings were used to re-create the historic look of San Francisco and Salt Lake City.

==Cast==
* George Segal as Charlie "Dirtwater Fox" Malloy
* Goldie Hawn as Amanda "Duchess" Quaid
* Conrad Janis as Gladstone
* Thayer David as Josiah Widdicombe Jennifer Lee as Trollop
* Sid Gould as Rabbi
* Pat Ast as Music hall singer
* E. J. André as Prospector
* Richard Farnsworth as Stagecoach driver
* Clifford Turknett as Mr. Weatherly

==Reviews==
The film was given mixed to positive reviews. Some said that "it was an outstanding cast and a wonderful comedy" while other claimed "too comedic and less western". As of 21 July 2012, Rotten Tomatoes gave it a 20% fresh rating among critics, while it fared better with audiences with a rating of 45%.

==References==
 

==External links==
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 