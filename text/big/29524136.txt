We're Going to Eat You
 
 
{{Infobox film
| name           = Were Going to Eat You
| image size     = 
| image	=	Were Going to Eat You FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Tsui Hark
| producer       = Ng See Yuen
| writer         = 
| screenplay     = 
| story          =
| based on       =  
| narrator       = 
| starring       = Norman Chu Eddy Ko Melvin Wong Kwok Choi Hon Mo-lin Cheung
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$1,054,985.50
}}

Were Going to Eat You is a 1980 Hong Kong horror comedy film directed by Tsui Hark. The film is about a secret agent, Agent 999, who is attempting to capture a thief named Rolex. Agent 999s hunt leads him to a village that is inhabited by cannibals. The film was not as big a success in Hong Kong as Tsuis other 1980 film Dangerous Encounter - 1st Kind.

==Plot==
A secret agent named Agent 999 is in trying to apprehend a thief named Rolex. His hunt leads him to a village where residents routinely capture visitors and eat them. Although the cannibalistic ritual has been initiated by the town chief, the villagers feel close to rebelling against him as more of their food has been given to his soldiers than the townspeople. Agent 999 is rescued from the villagers by Rolex, who has been posing at the village chiefs assistant. He rescues Agent 999 as an act to help redeem his career. Soon after, Rolex is caught by the village chief and eaten. This leads to Agent 999 escaping from several different villagers along with a newfound love in his life named Eileen.

==Themes==
Were Going to Eat You contains anti-communist themes. This is shown through the village chiefs attitudes toward the distribution of meat amongst the villagers and soldiers and his uniform.  Tsui suggested that this theme possibly came from his own student film work. Morton, 2009. p.40 

==Production==
Large portions of the music from Were Going to Eat You is lifted from the Italian horror film Suspiria.  Tsui said the use of this existing music at the time was that there was no budget for an original soundtrack. 

==Release==
Were Going to Eat You debuted on 2 April 1980. Morton, 2009. p.38 
It took in HK$1,054,985.50 in the Hong Kong box office. At the end of the year, it placed at number 113 of the top-grossing Hong Kong films for 1980. This was very low in comparison to Dangerous Encounter - 1st Kind, which was the 33rd top-grossing film at the Hong Kong box office in 1980. Morton, 2009. p.39  Were Going to Eat You has been released under several titles, including Hell Has No Gates, Kung Fu Cannibals, No Door to Hell, We Are Going to Eat You and Were Going to Eat You!.  Tsui has panned the film itself, saying that "it didnt turn out good". 

==Notes==
 

===References===
* {{Cite book
 | last= Morton
 | first= Lisa
 | title= The Cinema of Tsui Hark McFarland
 |year= 2009
 |isbn= 0-7864-4460-6
 |url=http://books.google.ca/books?id=MiulRecfarcC&printsec=frontcover
 |accessdate=8 November 2010
}}
* {{Cite book
 | last= OBrien
 | first= Daniel
 | title= Spooky Encounters: A Gwailos Guide to Hong Kong Horror
 |publisher= Headpress
 |year= 2003
 |isbn= 1-900486-31-8
 |url=http://books.google.ca/books?id=8XehfPFuFY8C
 |accessdate=8 November 2010
}}

==External links==
* 
* 

 

 
 
 
 
 
 