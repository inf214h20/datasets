Hero Hindustani
 
 
{{Infobox film
| name = Hero Hindustani
| image = Herohindustani.JPG
| director = Aziz Sejawal
| producer = 
| writer = Kader Khan (Dialogues)
| story = Yunus Sajawal
| screenplay = Yunus Sajawal
| cinematography = Nazeeb Khan
| editing = Waman Bhosle
| starring = Arshad Warsi Namrata Shirodkar Kader Khan
| music = Anu Malik
| released =  
| language = Hindi, Urdu
}}

Hero Hindustani is a 1998 Indian film directed by Aziz Sejawal. It stars Arshad Warsi and Namrata Shirodkar.

== Plot ==
Purushottam Agarwal shifts to London for better standard of living. He wants his grand daughter Nikki to respect Indian culture and marry an Indian man. Initially, Nikki rejects because she is already in love with Rohit. But, pressurized by her grandfathers stubborn attitude, she plans to move to India. In India, she meets Rommie a tourist guide. Here, she makes a plan to fool her grandfather by faking her marriage to Rommie. Then she plans to allow Rommie to act as a villainous person in front of Purushottam so that he changes his views about Indian grooms. She presents Rommie as a wealthy man in front of Purushottam. Nikki and Rommie, both initially hate each other. As per the plan, Rommie makes every attempt to act negative but destiny does not seem to support him. Every attempt takes him closer to Purushottams heart. Will Purushottam ever come to know about the plan?

== Cast ==
* Arshad Warsi  as  Rommie
* Namrata Shirodkar  as  Namrata Nikki Agarwal
* Kader Khan  as  Topi
* Paresh Rawal  as  Dadaji / Purshotam Harnam Agarwal
* Parmeet Sethi  as  Rohit
* Shakti Kapoor  as  Cadbury
* Asrani  as  Cameroon 
* Bharat Kapoor  as  Ranveer Singh 
* Pramod Muthu  as  Rashid
* Shehzad Khan  as  The Police Officer

== Soundtrack ==
{{Track listing
| headline        = Songs
| extra_column    = Playback
| all_music       = Anu Malik
| lyrics_credits = yes

| title1  = Aadha Ticket Mera Full Ho Gaya
| lyrics1 = Prayag Raj
| extra1  = Abhijeet

| title2  = Aisi Waisi Baat Nahin
| lyrics2 = Rahat Indori
| extra2  = Roop Kumar Rathod, Hema Sardesai, Sapna Awasthi

| title3  = Chaand Nazar Aa Gaya
| lyrics3 = Gauhar Kanpuri
| extra3  = Sonu Nigam, Alka Yagnik, Iqbal, Afzal

| title4  = Deewana Deewana Main Tera
| lyrics4 = Zameer Qazmi
| extra4  = Kumar Sanu, Sadhana Sargam

| title5  = Hero Hindustani
| lyrics5 = Rahat Indori
| extra5  = Kumar Sanu, Alka Yagnik

| title6  = Hero Hindustani
| note6   = Sad
| lyrics6 = Rahat Indori
| extra6  = Alka Yagnik

| title7  = Maahe Ramzan
| lyrics7 = Gauhar Kanpuri
| extra7  = Sonu Nigam, Alka Yagnik, Iqbal, Afzal

| title8  = Saawal Saawal
| lyrics8 = Dev Kohli
| extra8  = Ataullah Khan
}}

== External links ==
*  

 
 
 


 