The Belly of an Architect
{{Infobox film
| name           = The Belly of an Architect
| image          = BellyOfArchitect.jpg
| caption        = DVD cover
| director       = Peter Greenaway
| producer       =
| writer         = Peter Greenaway
| narrator       =
| starring       = Brian Dennehy Chloe Webb Lambert Wilson
| music          = Wim Mertens
| cinematography = Sacha Vierny John Wilson
| distributor    = Hemdale Film Corporation
| released       = 1987
| runtime        = 120 minutes
| country        = United Kingdom / Italy
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
The Belly of an Architect is a 1987 film drama written and directed by Peter Greenaway, featuring original music by Glenn Branca and Wim Mertens. Starring Brian Dennehy and Chloe Webb it was nominated for the Palme dOr (Golden Palm) award at the 1987 Cannes Film Festival.   

==Plot==

The American architect Stourley Kracklite has been commissioned to construct an exhibition in Rome dedicated to the architecture of Etienne-Louis Boullée. Doubts arise among his Italian colleagues about the legitimacy of Boullée among the pantheon of famed architects, perhaps because Boullée was an inspiration for Adolf Hitlers architect Albert Speer.

Tirelessly dedicated to the project, Kracklites marriage deteriorates, along with his health. His social and physical decline corresponds to the decline of his idol Boullée, who until the C20th was hardly known. 

Kracklite becomes obsessed with the historical Caesar Augustus after hearing that Livia, his wife, supposedly poisoned him. Kracklite assumes that his own wife, Louisa, is trying to do the same because he is suffering increasing stomach pains.

She informs him that she is pregnant, and is sexually involved with the younger co-organiser of the exhibition. 

He discovers that he has a stomach cancer and has not long to live. The film ends at opening ceremony, which Kracklite watches from a higher vantage point. Louisa gives birth to their child, and Kracklite jumps to his death.

==Style==
Director Greenaways visual technique heightens Kracklites alienation. There are few close-up shots of the other actors beside Dennehy, who himself is dwarfed by the dominance of the Roman architecture surrounding him.

Greenaways trademark historical reenactments also compose a major theme: many visual images of the film appear to replicate major 18th Century works of art and architecture. In addition there are subtle references to Isaac Newton and the law of gravity, perhaps alluding to Kracklites own inability to escape the physical laws of mortality.

==Cast==
* Brian Dennehy - Stourley Kracklite
* Chloe Webb - Louisa Kracklite
* Lambert Wilson - Caspasian Speckler
* Sergio Fantoni - Io Speckler
* Stefania Casini - Flavia Speckler
* Vanni Corbellini - Frederico
* Alfredo Varelli - Julio
* Geoffrey Copleston - Caspetti
* Francesco Carnelutti - Pastarri
* Marino Masé - Trettorio
* Marne Maitland - Battistino
* Claudio Spadaro - Mori
* Rate Furlan - Violinist Julian Jenkins - Old Doctor
* Enrica Maria Scrivano - Mother

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 