Palominas
{{Infobox film
| name = Palominas
| image = 
| alt = 
| caption = 
| director = Thadd Turner
| producer = Thadd Turner Janet DuBois Lou Tedesco
| writer = Thadd Turner
| starring = Buck Taylor Daryl Hannah Wes Studi Ryan Merriman Sean Murray
| cinematography = Virgil L. Harper
| editing = Aram Nigoghossian
| studio = Talmarc Productions Turnstone Entertainment Allied Artists Pictures
| released =  
| runtime = 
| country = United States
| language = English
| budget = $3.5 million
| gross = 
}} Western drama film written and directed by Thadd Turner. The film stars Buck Taylor, Daryl Hannah, Wes Studi, and Ryan Merriman.

==Cast==
* Buck Taylor as Frank Taylor
* Daryl Hannah as Louisa Green
* Wes Studi as Sam Jennings
* Ryan Merriman as Tramp Weathers
* Michael Parks as A.J. Whitney
* Christian Fortune as Temple James
* Chris Browning as Fargo
* Trace Adkins as Judge Guilt Roads
* Katie Cleary as Blue Velvet

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 