Beat the Drum
{{Infobox film
| name           = Beat the Drum
| image          = Beat the drum dvd cover.jpg
| image_size     = 200
| alt            = 
| caption        = DVD cover David Hickson
| producer       = W. David McBrayer Karen S. Shapiro Richard Shaw
| writer         = W. David McBrayer
| narrator       = 
| starring       = 
| music          = Klaus Badelt Ramin Djawadi
| cinematography = Lance Gewer
| editing        = Mark Winitsky 
| studio         = 
| distributor    = 
| released       = United States: October 10, 2003 France: May 18, 2004 Hong Kong: November 2, 2006
| runtime        = 114 minutes 
| country        = South Africa, United States Zulu
| budget         = United States dollar|US$1.5 million 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} David Hickson, is an award-winning South African film starring Clive Scott and Owen Sejake. 

Premiering at the 2003 Mill Valley Film Festival, Beat The Drum won 30 international film festival awards, including the prestigious Montreal Zenith DOr and the Monaco International Film Festival Angel Award, the festivals top honor. The film also won Best Picture, Best Director (David Hickson), Best Supporting Actor (Owen Sejake), Best Actor (Junior Singo), Best Music (Klaus Badelt and Ramin Djawadi) and several Audience Awards.

McBrayer has said that he wrote Beat The Drum to "help give a voice to the voiceless. I simply wanted to be an honest witness to the plight of these kids.  When there is a tear in the human fabric we should all feel it."  

==Synopsis==
A young South African orphan named Musa (Junior Singo) leaves his AIDS-ravaged village in KwaZulu-Natal, taking along only a drum given to him by his father, for the gritty streets of Johannesburg in search of work and his uncle. The trip proves to be enlightening for young Musa, who is faced with the culture shock of urban society. Meanwhile, a wealthy lawyer from a privileged family learns he has AIDS, and a truck drivers dangerous sexual proclivities endanger his wife.   

==Reception==
Variety (magazine)|Variety heralded the film as “Spectacular," calling it a "handsome well-crafted family drama...naturalistic performances...affecting human drama...first-rate!"      Leonard Maltin of Entertainment Tonight called it, "a film with a big heart and a vital message.”  The Hollywood Reporter said, "Audiences were enthralled by this movie."  In its on-air interview with director David Hickson and young star Junior Singo (Musa) CNN called the movie, "Profoundly moving and spiritually uplifting."

==Beat the Drum Village==
Due to the efforts of the film’s producers, Kimmel International, and President of Entertainment in Motion, Bill Grant, a portion of the proceeds from the films domestic and international airlines sales provided the resources to buy the land and initiate the project that would become Beat the Drum Village.  Beat the Drum Village provides family-style housing, food, clothing, education and medical care for children orphaned by and living with HIV/AIDS in Kenya.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 