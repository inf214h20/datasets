Casbah (film)
{{Infobox film
| name           =  Casbah
| image_size     =
| image	         = Casbah FilmPoster.jpeg
| caption        = John Berry
| producer       = Nat C. Goldstone
| writer         = Leslie Bush-Fekete
| story          = Henri La Barthe (as Detective Ashelbe)
| narrator       = Tony Martin
| music          = Walter Scharf
| cinematography = Irving Glassberg
| editing        = Edward Curtiss
| studio         = Marston Productions DRAMA AND FILM: Noyes Poem Purchased; De Carlo, Martin Lead
Schallert, Edwin. Los Angeles Times (1923-Current File)   09 June 1947: A7.  
| distributor    = Universal Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         = $1.3 million COLBERT SIGNED FOR LEAD IN EVE: Mankiewicz to Direct Film for Fox--Studio Revives American Guerrilla Bids $5,000 for "Casbah"
By THOMAS F. BRADY Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   04 Feb 1950: 19.  
| gross          = $1,092,283 (rentals) 
}}	 John Berry, Tony Martin, and released by Universal Studios.

==Plot== Tony Martin), who leads a gang of jewel thieves in the Casbah of Algiers, where he has exiled himself to escape imprisonment in his native France. Inez (Yvonne De Carlo), his girl friend, is infuriated when Pépé flirts with Gaby (Marta Toren), a French visitor, but Pépé tells her to mind her own business. Detective Slimane (Peter Lorre) is trying to lure Pépé out of the Casbah so he can be jailed. Against Slimanes advice, Police Chief Louvain (Thomas Gomez) captures Pépé in a dragnet, but his followers free him. Inez realizes that Pépé has fallen in love with Gaby and intends to follow her to Europe. Slimane knows the same and uses her as the bait to lure Pépé out of the Casbah.

==Cast and roles==
* Yvonne De Carlo - Inez Tony Martin - Pépé Le Moko
* Peter Lorre - Slimane
* Märta Torén - Gaby
* Hugo Haas - Omar
* Thomas Gomez - Louvain
* Douglas Dick - Carlo
* Herbert Rudley - Claude
* Gene Walker - Roland
* Curt Conway - Maurice

 

Cast notes:
* Eartha Kitt plays an uncredited bit part.  This was her film debut.

==Production==
The film was made by Marston Productions, Tony Martins production company. The Bank of America lent $800,000 to finance the film; Universal provided some of the balance. 
==Reception==
The film only recouped $600,000 of its negative cost. Universal succeeded in getting a court judgment against Marston of $350,000. They bought all rights to the film at public auction for $5,000. This purchase was subject to an unsatisfied lien against the property of $195,000 to the Bank of America. 
===Awards===
In 1949, the film was nominated for an Academy Award for Best Original Song for the song "For Every Man Theres a Woman" by Harold Arlen (music) and Leo Robin (lyrics).

==See also==
* Pépé le Moko, a 1937 film
* Algiers (film)|Algiers, a 1938 film
* Casbah, the citadel of Algiers

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 

 