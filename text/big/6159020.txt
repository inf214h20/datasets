Armageddon (1997 film)
 
 
 
 
{{Infobox film
| name           = Armageddon
| image          = Armageddon1997.jpg
| image size     =
| caption        = Film poster
| film name      = {{Film name
| traditional    = 天地雄心
| simplified     = 天地雄心
| pinyin         = Tiān Dì Xióng Xīn
| jyutping       = Tin1 Dei6 Hung4 Sam1 }}
| director       = Gordon Chan
| producer       = Gordon Chan
| writer         = Gordon Chan   Vincent Kok
| narrator       = Anthony Wong
| music          = Chan Kwong-Wing
| cinematography = Wong Wing-Hang
| editing        = Chan Ki-Hop
| studio         = Wins Entertainment
| distributor    = China Star Entertainment Group
| released       =  
| runtime        = 112 minutes Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK$23,698,750
| preceded by    =
| followed by    =
}}
 1997 Cinema Hong Kong action Science Anthony Wong.

==Summary==
In the near future, 10 men are voted as the 10 who could lead the world; on the list is leading Hong Kong scientist Dr. Ken (Andy Lau). When some of the other important figures on the list start dying in suspicious cases of spontaneous human combustion, the Hong Kong CID and the British MI6 get involved to try to protect Dr. Ken. Kens close friend in the Hong Kong police, Chiu (Anthony Wong) helps Ken stay out of trouble.

==Cast==
* Andy Lau as Dr. Tak Ken
* Michelle Reis as Adele Anthony Wong as Chiu Tai Pang
* Vincent Kok as TC
* Wayne Lai as Chuck
* Claudia Lau as Superintendent Ivy Yip
* Jessica Chau as Teresa
* Michael Lui as Clarence Chung
* Angel Wong as Janis
* Kam Kong as Mr. Yao
* Jacky Wong
* Steven Lau

 {{Cite web |url=http://www.imdb.com/title/tt0183892/ |title=Armageddon 
 |accessdate=2 July 2010 |publisher=imdb.com}} 
   

==See also==
*Andy Lau filmography
* List of Hong Kong films

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 