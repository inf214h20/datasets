Something in the Air (2012 film)
 
{{Infobox film
| name           = Something in the Air
| image          = Something in the Air poster.jpg
| caption        = Film poster
| director       = Olivier Assayas
| producer       = Charles Gillibert Nathanaël Karmitz
| writer         = Olivier Assayas
| starring       = Lola Créton Clément Métayer Felix Armand
| music          = 
| cinematography = Eric Gautier
| editing        = Luc Barnier 
| distributor    = MK2 Diffusion
| released       =  
| runtime        = 122 minutes
| country        = France
| language       = French
| budget         = 
}} Osella for Best Screenplay.   

==Plot==
During the 1970s a student named Gilles gets entangled in contemporary political turmoils although he would rather just be a creative artist. While torn between his solidarity to his friends and his personal ambitions he falls in love with Christine.

==Cast==
* Clement Metayer as Gilles
* Lola Créton as Christine
* Felix Armand as Alain
* Carole Combes as Laure
* India Menuez as Leslie
* Hugo Conzelmann as Jean-Pierre
* Mathias Renou as Vincent
* Dolores Chaplin as Actrice Londres
* Victoria Ley as Show-going hippy
* Nathanjohn Carter as German Soldier Extra
* Nick Donald as German Soldier
* Félix de Givry as Christophe
* Paul Spera as Carl

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 