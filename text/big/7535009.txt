Betty Boop's Life Guard
{{Infobox Hollywood cartoon|
| cartoon_name = Betty Boops Life Guard
| series = Betty Boop
| image = 
| caption =
| director = Dave Fleischer
| story_artist = 
| animator = Willard Bowsky David Tendlar
| voice_actor = Bonnie Poe 
| musician = 
| producer = Max Fleischer 
| distributor = Paramount Pictures
| release_date = July 13, 1934
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}
Betty Boops Life Guard is a 1934 Fleischer Studios animated short film, starring Betty Boop.

==Plot==

Betty is spending the day at the beach, where her boyfriend Fearless Freddy works as a life guard.  Betty is enjoying the ocean while floating in her inflatable rubber horsey when it springs a leak.  Freddy dives in to save Betty, but she goes under, where she begins to imagine shes a mermaid.  At first Betty enjoys her new underwater life, swimming and singing with the other undersea inhabitants.  The fun ends when a sea monster chases her.  Just before the monster catches her, she wakes up, safe in Freddys arms.

==Notes and comments==

*This is the first Motion Picture Production Code|Post-Code Betty Boop cartoon. The Hays Office ordered the removal of the suggestive curtain introduction which had started the cartoons up until now because Betty Boops winks and shaking of her hips was deemed "suggestive of immorality."
*This cartoon featured the second appearance of Bettys boyfriend Fearless Freddy.
*This was released just 13 days after the Hays-Code affected in July 1, 1934.

==External links==
* 
 

 
 
 
 
 


 