Zombie Chronicles
{{Infobox film
| name           = Zombie Chronicles
| image          = Zombie Chronicles.jpg
| image size     =
| caption        =
| director       = Brad Sykes Jeff Ferguson David S. Sterling
| writer         = Garrett Clancy
| narrator       =
| starring       = Emmy Smith Joe Haggerty Garrett Clancy Janet Tracy Keijser Beverly Lynne
| music          =
| cinematography = Jeff Leroy
| editing        = John Polonia Mark Polonia
| distributor    = Brain Damage Films , Razor Digital Entertainment  2001
| runtime        = 71 minutes
| country        =   English
| budget         =
| preceded by    =
| followed by    =
}} 2001 horror film directed by Brad Sykes. It follows Tara Woodley, a reporter who visits an old desert town to research her article on the ghost town legends from around there. On the way she picks up the hitchhiker, Ebenezer Jackson, who takes her to an abandoned building where she interviews him via tape recorder.

==Structure==
The film begins with footage of zombies rising and attacking the living, and then follows Tara to the abandoned building where she proceeds to interview the hitchhiker. He tells her a story which becomes one of the films segments. We are then taken back to Tara and Ebenezer who then proceeds to tell her another story which then becomes the second and final short film. After this short film we go back to the abandoned building for the films climax.

==Cast==
*Tara Woodley - Emmy Smith
*Ebenezer Jackson - Joe Haggerty
*Sgt. Ben Draper - Garret Clancy Greg Brown
*Jason - Mike Coen
*Buzz - John Kyle Grady
*Melinda - Janet Tracy Keijser
*Marsh - Beverly Lynne
*Geeter - Jarrod Robbins

==External links==
*  

== References ==
 

 
 
 
 

 