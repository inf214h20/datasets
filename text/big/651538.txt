Dracula (1958 film)
 
 
{{Infobox film name = Dracula image = Dracula1958poster.jpg caption = Original film poster director = Terence Fisher writer = Jimmy Sangster Bram Stoker (Novel) starring = Christopher Lee Peter Cushing Michael Gough Melissa Stribling music = James Bernard producer = Anthony Hinds cinematography = Jack Asher editing = Bill Lenny studio = Hammer Film Productions distributor = Rank Organisation (UK) Universal-International(USA) released =   runtime = 82 minutes country = United Kingdom language = English
|budget = £81,000  gross = 1,008,834 admissions (France) 
}} Hammer Horror films inspired by the Bram Stoker novel Dracula. It was directed by Terence Fisher, and stars Christopher Lee, Peter Cushing, Michael Gough, Melissa Stribling and Carol Marsh. In the United States, the film was retitled Horror of Dracula to avoid confusion with the earlier Dracula (1931 English-language film)|Dracula  (1931) starring Bela Lugosi.
 Bray Studios on 17 November 1957 with an investment of £81,000. *  |pages=256}} 

==Plot summary== Klausenburg (Cluj), to take up his post as librarian. Inside, he is startled by a young woman who claims she is a prisoner and begs for his help. Dracula then appears to greet Harker and guide him to his room, where he locks him in. Jonathan starts to write in his diary, and his true intentions are revealed: he is a vampire hunter and has come to kill Dracula.

  as Jonathan Harker.]]
Freed sometime later, Harker again is confronted by the desperate woman. She begs him for help but then bites his neck. Just as she does, Dracula – fangs bared and lips bloody – arrives and yanks her away. When he awakens in daylight, Harker finds the bite mark. He hides his journal in a shrine to the Virgin Mary outside the castle and descends into the crypt, where he finds Dracula and the vampire woman resting in their coffins. Armed with a stake, he impales the woman, who, as he looks on, immediately ages from young to old. Whilst he does this, the sun sets, and when he turns to Draculas coffin with the intention of killing the vampire, he finds it empty. Looking up, Harker is in time to see the Count shut the door and they are both plunged into darkness...
 Lucy Holmwood. Lucy is ill, so the news is kept from her and Lucys little niece, Tania. But, when night falls, Lucy removes the crucifix from round her neck, opens the doors to her terrace and lays bare her neck – already, it bears the mark of a vampire bite. And soon Dracula arrives and bites her again.

Mina seeks out Van Helsings aid in treating Lucys declining health, but Lucy begs Gerda the maid to remove his prescribed garlic bouquets, and she dies. However shortly after shes buried, Tania is spirited away into the night and is returned the next day, claiming Lucy had beckoned her. Realizing that Lucy has arisen as a vampire, Van Helsing turns over Harkers journal and reveals the truth to prove his claim. The next night, Lucy,  now undead and evil, lures away Tania once more to a graveyard with the intent to feed on and turn her into a vampire. But the child is saved when Arthur, curious of Van Helsings claim, spots them and calls out to Lucy. Lucy turns her attention to him but Helsing manages to ward her off with a cross and forces her to flee back to her crypt. Van Helsing takes Tania home and returns to find Arthur still at Lucys crypt. He explains that Lucy is both Draculas revenge against Harker and a replacement for the bride killed by him. Van Helsing suggests using Lucy as a means to find Dracula. But Arthur refuses as it runs the risk of her biting someone else, plus not wanting see Lucy corrupted any further. So Van Helsing stakes Lucy in her coffin.

  as Count Dracula.]]
Van Helsing and Arthur travel to the customs house in Ingstadt to track down the destination of Draculas coffin (which Van Helsing saw carried away when he arrived at Draculas castle). Meanwhile, Mina is called away from home by a message telling her to meet Arthur at an address in Karlstadt – the same address Arthur and Van Helsing are told the coffin was bound for – and Dracula is indeed waiting for her...

The next morning, Arthur and Van Helsing find Mina in a strange state. They leave for the address they were given, an undertakers, but find the coffin missing. When they decide to set off again, Arthur tries to give Mina a cross to wear, but it burns her, revealing that shes infected by vampirism and is slowly turning into a vampire herself.

During the night, Van Helsing and Arthur guard Minas windows outside against a return of Dracula, but Dracula nonetheless appears inside the house and bites her. A remark by Gerda leads Van Helsing to the coffins location: the cellar of the Holmwoods own house. But Dracula is not in the coffin and instead escapes into the night with Mina, intent on making her a new bride.

A chase then begins as Dracula rushes to return to his castle near Klausenberg before sunrise. He attempts to bury Mina alive outside the crypts but is caught by Van Helsing and Arthur. Inside the castle, Van Helsing and Dracula struggle. Van Helsing tears open the curtain to let in the sunlight and, forming a cross of candlesticks, he forces Dracula into it. Dracula crumbles into dust as Van Helsing looks on. Mina recovers, the cross-shaped scar fading from her hand indicating shes been saved, as Draculas ashes blow away, leaving only a ring behind.

==Cast==
*Christopher Lee as Count Dracula
*Peter Cushing as Abraham Van Helsing
*Michael Gough as Arthur Holmwood
*Melissa Stribling as Mina Harker
*Carol Marsh as Lucy Westenra
*John Van Eyssen as Jonathan Harker
*Janina Faye as Tania
*Charles Lloyd-Pack as John Seward George Merritt as Policeman George Woodbridge as Landlord George Benson as Frontier Official
*Miles Malleson as Undertaker
*Geoffrey Bayldon as Porter
*Olga Dickie as Gerda
*Barbara Archer as Inga
*Valerie Gaunt as Brides of Dracula

==Production==
===Special effects===
The filming of Draculas destruction included a shot in which Dracula appears to peel away his decaying skin. This was accomplished by putting a layer of red makeup on Christopher Lees face, and then covering his entire face with a thin coating of morticians wax, which was then made up to conform to his normal skin tone. When he raked his fingers across the wax, it revealed the "raw" marks underneath. This startling sequence was cut out, but was restored for the 2012 Blu-ray release, using footage from a badly damaged Japanese print.

===Zodiac wheel in final scene===
At the end of the movie, Dracula is destroyed on an inlaid Zodiac wheel on the floor, which has several quotes in Latin and Greek. The inner circle in Greek has a quote from Homers Odyssey Book 18.136–7: "τοῖος γὰρ νόος ἐστὶν ἐπιχθονίων ἀνθρώπων οἷον ἐπ᾽ ἦμαρ ἄγησι πατὴρ ἀνδρῶν τε θεῶν τε" ("The mind of men who live on the earth is such as the day the father of gods and men   brings upon them.") The outer wheel is written in Latin, and is a quote from Hesiod via Bartolomeo Anglico (De proprietatibus rerum, Book 8, Chapter 2): "Tellus vero primum siquidem genuit parem sibi coelum stellis ornatum, ut ipsam totam obtegat, utque esset beatis Diis sedes tuta semper." ("And Earth first bare starry Heaven, equal to herself, to cover her on every side, and to be an ever-sure abiding-place for the blessed gods.") Draculas ring is left on the glyph of the sign of Aquarius on the Zodiac wheel.

==Reception==
Dracula was a critical and commercial success upon its release and was well received by critics and fans alike of Bram Stokers works. The film currently scores 93% on Rotten Tomatoes with the consensus "Trading gore for grandeur, Horror of Dracula marks an impressive turn for inveterate Christopher Lee as the titular vampire, and a typical Hammer mood that makes aristocracy quite sexy."

==Release==
===Original===
 

===DVDs===
DVD release  The Mummy in a box-set entitled Hammer Horror Originals.

2007 UK re-release  BFI in 2007. When the film was originally released in the UK, the BBFC gave it an X rating, being cut, while the 2007 uncut re-release was given a British Board of Film Classification|12A.

Blu-ray Disc Restored Version 
For many years historians have pointed to the fact that an even longer, more explicit, version of the film played in Japanese and European cinemas in 1958. Efforts to locate the legendary Japanese version of Dracula had been fruitless.

In September 2011, Hammer announced that part of the Japanese release had been found in the National Film Center at the National Museum of Modern Art, Tokyo. The first five reels of the film held by the center were destroyed in a fire in 1984, but the last four reels were recovered. The recovered reels include the last 36 minutes of the film and includes two extended scenes, one of which is the discovery of a complete version of the films iconic disintegration scene. The announcement mentioned a HD telecine transfer of all four reels with a view for a future UK release. 
 double play Blu-ray Disc set in the UK on 18 March 2013. This release contains the 2007 BFI restoration along with the 2012 high-definition Hammer restoration which includes footage which was previously believed to be lost.  The set contains both Blu-ray Disc and DVD copies of the film as well as several bonus documentaries covering the films production, censorship and restoration processes.

==See also==
*Vampire film

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 