Kodungallooramma
{{Infobox film 
| name           = Kodungallooramma
| image          =
| caption        =
| director       = Kunchacko
| producer       = M Kunchacko
| writer         = Jagathy NK Achari
| screenplay     =
| starring       = Prem Nazir K. R. Vijaya Adoor Bhasi Thikkurissi Sukumaran Nair
| music          = K. Raghavan
| cinematography =
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed and produced by Kunchacko . The film stars Prem Nazir, K. R. Vijaya, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by K. Raghavan.   

==Cast==
 
*Prem Nazir as Kovalan
*K. R. Vijaya as Kannaki
*Adoor Bhasi as Chinees Marchant
*Thikkurissi Sukumaran Nair as Kovalans Father
*Jose Prakash as Cholarajavu
*Manavalan Joseph as Swami
*Adoor Pankajam as Konkimaami
*Aranmula Ponnamma as Kavunthi
*Jyothilakshmi as Madhavi
*Kaduvakulam Antony as Cook
*Kalaikkal Kumaran as Dalapathi
*Kanchana as Thattathi
*Kottarakkara Sreedharan Nair as Pandya rajavu
*N. Govindankutty as Thattaan
*Nellikode Bhaskaran as Ganapathy
*S. P. Pillai as Konkammavan  Khadeeja as Maharani
 

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhadradeepam || S Janaki || Vayalar Ramavarma || 
|-
| 2 || Kaaverippoompattanathil || P Susheela, M Balamuralikrishna || Vayalar Ramavarma || 
|-
| 3 || Kodungallooramme || M Balamuralikrishna, Chorus || Vayalar Ramavarma || 
|-
| 4 || Manjubhaashini || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Narthaki || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|-
| 6 || Rithukanyakayude || P Susheela || Vayalar Ramavarma || 
|-
| 7 || Sthree Hridayam || PB Sreenivas || Vayalar Ramavarma || 
|-
| 8 || Udayaasthamanangale || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 