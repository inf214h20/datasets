The Clearstream Affair
{{Infobox film
| name           = The Clearstream Affair
| image          = LEnquête.jpg
| caption        = Film poster
| director       = Vincent Garenq
| producer       = Philip Boëffard Christophe Rossignon 
| screenplay     = Stéphane Cabel Vincent Garenq  	
| based on       =  
| starring       = Gilles Lellouche Charles Berling Laurent Capelluto Florence Loiret-Caille
| music          = Erwann Kermorvant 	
| cinematography = Renaud Chassaing 		 
| editing        = Vincent Garenq Elodie Codaccioni Raphaël de Monpezat
| studio         = Nord-Ouest Productions   Samsa Film Artémis Productions France 3 Cinéma Mars Films Cool Industrie Belgacom
| distributor    = Mars Distribution  
| released       =  
| runtime        = 106 minutes
| country        = France Luxembourg Belgium
| language       = French
| budget         = €8 million 
| gross          = 
}}

The Clearstream Affair (French title: LEnquête) is a 2014 thriller film directed by Vincent Garenq, based on the Clearstream scandal in 2001.

== Cast ==
* Gilles Lellouche as Denis Robert 
* Charles Berling as Judge Renaud Van Ruymbeke
* Laurent Capelluto as Imah Lahoud
* Florence Loiret Caille  as Géraldine Robert  
* Christian Kmiotek as Régis Hempel 
* Grégoire Bonnet as Laurent Beccaria 
* Antoine Gouy as Florian Bourges 
* Eric Naggar as Jean-Louis Gergorin
* Laurent DOlce as Vincent Peillon
* Gilles Arbona as General Rondot
* Hervé Falloux as Dominique de Villepin 
* Thomas Séraphine as Arnaud Montebourg

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 