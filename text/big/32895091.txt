The Tide of Death
 
{{Infobox film
| name           = The Tide of Death
| image          = 
| image_size     =
| caption        = 
| director       = Raymond Longford
| producer       = Charles Cozens Spencer
| writer         = Raymond Longford
| based on = an original story by Raymond Longford
| narrator       =
| starring       = Lottie Lyell
| music          =
| cinematography = Tasman Higgins Arthur Higgins
| editing        = Tasman Higgins "Raymond Longford", Cinema Papers, January 1974 p51 
| distributor    = 
| studio        = Spencers Pictures
| released       = 13 April 1912 (Sydney) 
| runtime        = over 3,000 feet 
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}

The Tide of Death is a 1912 Australian silent film directed by Raymond Longford based on an original story by Longford.  This was rare at the time because most Australian silent films were based on plays or novels.

It is considered a lost film.

== Synopsis ==
The film is set in Australias past.  It opens with a young mining contractor, Philip Maxwell, blowing up a rock at a mining site. He then rides on horseback to the bank to get wages for his crew. "The Lizard", a camp loafer, learns about this and informs Black Dan Bryce, who decides to rob Philip with his gang. This is overheard by Dans step daughter Sylvia who rides for help.

The saddle bag containing the money is secured by one of Dans gang, who is overpowered by Phillip. He manages to conceal the money just before he is captured by Dan and refuses to say where it is. The gang suspect Sylvia of being an informant and knock her down; she recovers in time to see Phillip being taken away, then finds Phillips horse and rides for help.

Dan tries to get Phillip to talk by tying him to a stake in the middle of a rising tidal creek, while Sylvia manages to alert Phillips mining crew. The water is rising to Phillips chin when Slyvia arrives on horseback, dashes into the creek, stems the tide and cuts Phillip free.

Two years later Philip and Sylvia are married and have a baby, Edna, when Phillip gets an urgent telegram from home. While he is away their house is burgled by Dan and his old gang, who recognise Sylvia and kidnap her to get revenge. They take her away to a hut and make her their servant.

Unable to find any trace of his wife, Philip thinks she has left him, so he sells his property and goes abroad with Edna. Black Dans gang argue amongst themselves, causing a fire at the hut, enabling Sylvia to escape. When she returns home she finds her house abandoned and Philip and her child gone. She goes to a convent where the sisters nurse her through an illness. She later becomes an assistant teacher at Spencerville Private School.

Three years later Philip and Edna return to Australia. Edna loses a trinket which one of the school children find and return to Sylvia. She advertises it and Ednas nurse draws Phillips attention to it. He and Edna visit the school and are reunited with Slyvia   

== Cast ==
*Lottie Lyell as Sylvia Grey 
*Augustus Neville as Philip Maxwell
*Frank Harcourt as Black Dan Bryce
*Bert Harvey as Dan gang member
*DL Dalziel as Dan gang member
*D Sweeney as Dan gang member
*G Flinn as bushman
*Fred Twitcham as bushman
*Arthur Steel as bushman
*Joe Hamilton as Mat Davis
*Lois Cumming as Jenny
*Little Annie Gentile as Little Edna
*Arno the horse

==Production==
The film was shot in and around Sydney  using many of the cast who had appeared in Longfords earlier movies.

== Reception ==
Advertising said the film was "an original picture suggested and worked on by one of Spencers staff, played by artists of their own selection, and photographed by their own operators, who have acquired a reputation for turning out the best cinematograph work in the commonwealth." 

The film was well received by critics and the public, screening throughout Australia and reportedly attracting good crowds.  

The Sydney Morning Herald said the film was "not wanting in either dramatic strength or love interest. The principal sensations are the blasting explosion, the hero being gradually drowned by the rising tide, and a big fire scene. Many beautiful Australian scenic settings have been introduced."  The Perth Daily News said it was "worthy to rank with the best productions of the leading American or European firms." 

However the critic from the Perth Sunday Times thought the movie should have ended where the hero was rescued from the flood:
 Instead of which, probably having a lot more inoffensive celluloid left, Spencer extended the drama to maudlin limits and completely spoiled the story. Likewise the photography in the latter part was of decidedly an amateurish calibre, and completely out of harmony with the first portion. It seems to this print as if the original photographer had grown weary of his work and had gone home and left his understudy to do the fag end of the fake.  

== References ==
 

== External links ==
*  
*   at National Film and Sound Archive
*  at AustLit
 

 
 
 
 
 
 
 