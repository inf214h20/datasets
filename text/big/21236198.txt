Thief of Damascus
 
{{Infobox film
| name           =Thief of Damascus
| image          = Thidampos.jpg
| caption        = Original film poster
| director       = Will Jason
| producer       = Sam Katzman
| writer         = Robert E. Kent John Sutton Jeff Donnell Lon Chaney Jr.
| music          = John Leipold
| cinematography = Ellis W. Carter
| editing        = William A. Lyon
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 78 min.
| language       = English
| budget         =
}}
 adventure Technicolor John Sutton, Joan of The Magic Carpet and followed by Siren of Bagdad.

==Synopsis== Siege of Damascus for his ruler Khalid ibn al-Walid, he is made an enemy of the State. Amdar escapes and steals a scimitar made of Damascus steel.  He leads an alliance of Sinbad without his ship, Aladdin without his lamp, Sheherazade, and Ali Baba and his 40 thieves to depose Khalid and win the heart of Princess Zafir.

==Cast==
As appearing in screen credits (main roles identified):   
{| class="wikitable" width="60%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Paul Henreid || Abu Amdar
|- John Sutton || Khalid
|-
| Jeff Donnell || Sheherazade
|-
| Lon Chaney Jr. || Sinbad
|-
| Elena Verdugo || Neela
|-
| Helen Gilbert || Princess Zafir
|-
| Robert Clary || Aladdin
|-
| Edward Colmans || Sultan Raudah
|-
| Nelson Leigh || Ben Jammal
|-
| Philip Van Zandt || Ali Baba
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 