Dangerous Medicine
 
 
{{Infobox film
| name           = Dangerous Medicine
| image          =
| image_size     =
| caption        =
| director       = Arthur B. Woods Jerome Jackson
| writer         = Paul England Paul Gangelin
| starring       = Elizabeth Allan Cyril Ritchard
| music          =
| cinematography = Basil Emmott
| editing        = Warner Brothers-First First National Productions
| released       = August 1938
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
}}
Dangerous Medicine is a 1938 British crime film, directed by Arthur B. Woods and starring Elizabeth Allan and Cyril Ritchard.  It is now classed as a lost film.  

==Plot==
Secretary Victoria Ainswell (Allan) marries her wealthy elderly boss.  Soon after the wedding he dies suddenly in suspicious circumstances, and the autopsy reveals that the police have a murderer on their hands.  Everything points to Victoria as the only person with means, opportunity and motive, and as she can provide no sensible explanation as to who else could have killed her husband, she is arrested and put on trial for murder.

Victoria is found guilty and sentenced to hang.  As she is being driven back to prison, the car is involved in a serious road accident.  Victoria is critically injured and is rushed to hospital, where brilliant doctor Noel Penwood (Ritchard) fights desperately against the odds to save her life.  He finds a shard of glass has pierced her heart, and has to perform extremely risky surgery to remove it.

Once the operation is over and Victoria is off the danger list, Penwood learns that she faces execution.  He is appalled by the horrendous irony that he has saved her life so heroically, only for it to soon to be taken anyway through process of law.  As Victoria recovers, he listens to her story, believes in her innocence and starts to fall in love with her.  Against all ethics, he smuggles her out of the hospital and puts her in hiding.  The now romantically-involved couple do some sleuthing of their own, and finally stumble on the identity of the real killer.  The police are extremely grateful and apologetic, and Victoria is exonerated, leaving her free to pursue the romance with Penwood.

==Cast==
* Elizabeth Allan as Victoria Ainswell
* Cyril Ritchard as Dr. Noel Penwood
* Edmund Breon as Totsie Mainwaring Anthony Holles as Alistair Hoard
* Aubrey Mallalieu as Judge
* Basil Gill as Sir Francis
* Leo Genn as Murdoch Gordon McLeod as Mr. Buller
* Frederick Burtwell as Mr. George
* Allan Jeayes as Superintendent Fox

==References==
 

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 