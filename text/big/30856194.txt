Bol Bachchan
 
 

{{Infobox film
|name=Bol Bachchan
|image=Bol_Bachchan.jpg
|alt= 
|caption=Theatrical release poster
|director=Rohit Shetty
|producer=Ajay Devgn Dhillin Mehta
|writer=Sajid-Farhad  (dialogues) 
|screenplay=Yunus Sajawal
|story=Rohit Shetty
|starring=Ajay Devgn Asin Abhishek Bachchan Prachi Desai
|music=Himesh Reshammiya Ajay-Atul
|cinematography=Dudley
|editing=Steven H. Bernard
|studio=Fox Star Studios
|distributor=Ajay Devgn Films Shree Ashtavinayak Cine Vision Ltd
|released=6 July 2012
|runtime=149 minutes   
|country=India
|language=Hindi
| budget=     
| gross= 
}}
 2012 Bollywood romantic action comedy film. The film, made on a budget of  ,    is inspired by the 1979 film, Gol Maal. Bol Bachchans official trailer was released on 24 May 2012  and the film was released on 6 July 2012 in around 2575 screens worldwide with 2700 prints. It received mixed critical acclaim but had a good opening at the box office.  The film reportedly made a record for its advance bookings.     Venkatesh and Ram Pothineni reprised the roles of Ajay Devgan and Abhishek Bachchan respectively.

==Plot==
Abbas Ali (Abhishek Bachchan) lives in Chandni Chowk with his sister Sania (Asin Thottumkal). When their ancestral property is taken from them by deception, Abbas loses his job. Their family friend, Shastri (Asrani), convinces them to move to his village, Ranakpur, where he assures Abbas that he will get him a job at his boss, Prithviraj Raghuvanshis (Ajay Devgn), place. Pritvhiraj is a powerful but kind-hearted man who hates lies and liars, and punishes them harshly. Abbas and Sania arrive at Ranakpur and meet Abbass friend Ravi (Krishna Abhishek), Shastris son, who runs a drama company with his friends. Abbas learns that there is a temple at the border of Ranakpur and Kherwada, the village where Prithvirajs cousin Vikrant Raghuvanshi is powerful. The two cousins are archenemies. The temple, being at the border of the two villages, was disputed and has been locked for years.
 Eid as effeminate man and classical Kathak dancer. Things become worse for Abbas and Ravi when Prithviraj kindly hires Abbas for teaching Kathak to his sister Radhika (Prachi Desai). The more Abbas tries to manage his dual life without letting Prithviraj know, the more he has to lie to cover up other lies; this forms the crux of the story.

==Cast==
* Abhishek Bachchan as Abbas Ali / Abhishek Bachchan
* Ajay Devgn as Prithviraj Raghuvanshi
* Asin as Sania Ali /Sania Bachchan /Apeksha
* Prachi Desai as Radhika Raghuvanshi
* Archana Puran Singh as Zohra / Madhumati
* Krishna Abhishek as Ravi Shastri Asrani as Shastri
* Amitabh Bachchan as himself in the title song "Bol Bachchan"
* Paresh Ganatra as Builder (cameo)
* Neeraj Vora as Maakhan Vijay Ishwarlal Pawar as Kailash (actor in Ravis plays )
* Prasad Barve as Ravis friend
* Jeetu Verma as Vikrant Raghuvanshi
* Robin Bhatt as Judge

==Production==
The film was originally expected for a Diwali 2011 release, though was postponed to 2012.  On Dussehra 2011, director Rohit Shetty who previously directed the comedy flick, Golmaal 3 (2010), did first give actor Ajay Devgn an offer for Golmaal 4, but Devgn claimed "Golmaal 4 will happen next to next year. Its necessary to have two years gap between two Golmaal films. Its not good if it comes every year." After the announcement, it was added that Bol Bachchan will be made before Golmaal 4.    In January 2011, it was revealed that actress Asin was roped in for the first female lead after Bipasha Basu rejected the role. Earlier, actress Genelia DSouza was signed to play the second female lead, but due to her differences with the makers, she opted out of the project and hence the role was offered to Prachi Desai. 

In Jubilee Comedy Circus, Krushna Abhishek did a performance of Rohit Shetty never giving him a film role to do but having promised him in earlier seasons of the show, Rohit was so impressed by the performance, he decided to give Krushna a role in Bol Bachchan. Shetty also added co-host Archana Puran Singh from the same show to the film cast. It was revealed in the 300th episode special of Comedy Circus, where Rohit Shetty was a guest judge, that Paresh Ganatra was among the cast.  The film has been shot at various locations,  including Rohit Shettys seemingly favourite Panchgani and Chomu Palace,  among other locations.

==Soundtrack==
{{Infobox album |  
|Name=Bol Bachchan
|Type=Soundtrack
|Artist=Himesh Reshammiya
|Cover=Bol Bachchan Album Cover.jpg
|Released=6 July 2012
|Genre=Film soundtrack
|Length=170 minutes
|Label= T-Series
|Producer=Ajay Devgn Last album=Dangerous Ishhq (2012) This album=Bol Bachchan (2012) Next album=Oh Oh My God (2012)
}}

The music of the film was composed by Himesh Reshammiya while the lyrics were penned by Farhad-Sajid, Shabbir Ahmed, Swanand Kirkire and Sameer (lyricist)|Sameer. The album was released on 7 June 2012 and included four original soundtracks along with four remix versions of the tracks. Three songs were composed by Reshammiya of which two were featured in the film and one was listed only in the album. One song, "Nach Le Nach Le", composed by Ajay-Atul was also included in the album.  Among the songs, "Jab Se Dekhi Hai", "Nach Le Nach Le" became moderately popular. 

{{Track listing
|extra_column=Singers
|lyrics_credits=yes
|music_credits=no
|all_music=Himesh Reshammiya, except where noted
|title1=Bol Bachchan
|extra1=Amitabh Bachchan, Abhishek Bachchan, Ajay Devgn Himesh Reshammiya, Mamta Sharma, Vineet Singh
|music1=Himesh Reshammiya
|lyrics1=Farhad-Sajid
|length1=5:59
|title2=Chalao Na Naino Se
|extra2=Himesh Reshammiya ,Shreya Ghoshal & Shabab Sabri
|music2=Himesh Reshammiya
|lyrics2=Shabbir Ahmed
|length2=5:41
|title3=Nach Le
|extra3=Sukhwinder Singh, Shreya Ghoshal
|music3=Ajay-Atul
|lyrics3=Swanand Kirkire
|length3=4:43
|note3=Music by Ajay-Atul
|title4=Jab Se Dheki Hai
|extra4=Mohit Chauhan
|music4=Himesh Reshammiya
|lyrics4=Sameer
|length4=4:13
|title5=Bol Bachchan Remix
|extra5=Amitabh Bachchan, Abhishek Bachchan, Ajay Devgn Himesh Reshammiya, Mamta Sharma, Vineet Singh
|music5=Himesh Reshammiya
|lyrics5=Farhad-Sajid
|length5=4:52
|note5=Remix by:DJ A.Sen and DJ Amann Nagpal
|title6=Chalao Na Naino Se Remix
|extra6=Himesh Reshammiya, Shreya Ghoshal & Shabab Sabri
|music6=Himesh Reshammiya
|lyrics6=Shabbir Ahmed
|length6=5:27
|note6=Remix by:DJ Sheizwood
|title7=Nach Le Remix
|extra7=Sukhwinder Singh, Shreya Ghoshal
|music7=Ajay-Atul
|lyrics7=Swanand Kirkire
|length7=5:09
|note7=Remix by: DJ Shiva
|title8=Jab Se Dheki Hai Remix
|extra8=Mohit Chauhan
|music8=Himesh Reshammiya
|lyrics8=Shabbir Ahmed
|length8=4:06
|note8=Remix by:Teenu Arora
}}

==Release==
Bol Bachchan was released on 6 July 2012 in 2575 screens in India.  Upon release, the film received mixed critical response, but strong box office collections by the end its first weekend. On the review aggregator website Top10Bollywood, the film scored 2.31, based on 34 reviews. 

===Critical reception===
 
On positive side, Taran Adarsh of Bollywood Hungama gave the film 4 out of 5 stars and said, "On the whole, Bol Bachchan is a dhamaal entertainer that has the Rohit Shetty stamp all over it. A film that pays homage to the cinema of the 1970s and 1980s, especially the ones made by Manmohan Desai. Big stars, big visuals, big entertainment, Bol Bachchan has it all."  Devesh Sharma of Filmfare also awarded the film 4 out of 5 stars and commented, "How we wish director Rohit Shetty had kept aside his passion of blowing up vehicles wholescale just this once. The exaggerated action scenes eat into the narrative and jar the pace. Would this film be a crowd pleaser– well yes. Its a good leave-your-brains-behind product that Shetty is famous for. There are flashes that suggest he could have gone beyond and made a more polished film but the lure of making a blockbuster proved too much, I guess. The masses would be happy but the same cant be said for the soul of Hrishikesh Mukherjee."  Srijana Mitra Das of Times of India gave the film 3.5 out of 5 stars and stated, "Rohit Shettys latest movie has a constant up-and-down aspect to it, one sequence making you shriek in your seat with laughter, another sending your mind wandering off to the mundane. But at the very heart of things Shettys madly in love with the movies and BB is his homage to that all-time classic, Golmaal."  Mrigank Dhaniwala of Koimoi also gave the film 3.5 out of 5 stars and concluded, "On the whole, Bol Bachchan delivers entertainment in huge dollops. For that, it will earn the love of the paying public and will have a successful run at the box-office."  Gaurav Malani of Times of India gave the film a positive feedback and said, "For a (pleasant) change, Rohit Shetty doesnt do Golmaal his style. Rather he does Golmaal in its original form and thats what creates a decent difference, making Bol Bachchan fairly entertaining!". 

Sukanya Verma of Rediff gave the film 2.5 out of 5 stars and stated, "Bol Bachchan is dispensable cinema, forgotten almost immediately after its over. What I kept wondering is how does Asrani who acted in Mukerjees acclaimed films like Chupke Chupke, Abhimaan, Bawarchi feel about working in the remake of a film where the hero wore his kurta. Dont know what Im talking about? You deserve Bol Bachchan. But if you do, you must have already begun scouting for your copy of Gol Maal somewhere."  Shubhra Gupta of The Indian Express gave the film 2.5 out of 5 stars and commented, "If your nosy is not turned up too high, Bol Bachchan, less blaring than your standard Rohit Shetty comedy, can give you sporadic chuckles, and a few helpess laughs. Cant expect more."  Vinayak Chakravorthy of India Today gave the film 2.5 out of 5 stars and concluded, "Far from being LOL stuff, Bol Bachchan ends up being Bore Bachchan, with a lame climax." 

The film received some negative reviews. Rajeev Masand of CNN-IBN gave the film 2 out of 5 stars and said, "Even if youre a fan of Rohit Shettys cinema, its unlikely your chest will become a blouse over this one!"  Blessy Chettiar of DNA India gave the film 2 out of 5 stars and commented, "If you liked the Shetty Golmaals and Singham, this review will only be bol bachchan for you. Have fun while it lasts."  Ananya Bhattacharya of Zee News gave the film and said, "They say what cant be cured, must be endured. Once you give in to that adage and ask your white matter to exit the theatre, you will enjoy the film. Watch Bol Bachchan just for laughing."  Aniruddha Guha of DNA India gave the film 2 out of 5 stars and stated, "Bol Bachchan, overall, falls short of being a laugh riot in spite of having the ammunition for it. In its current form, its best enjoyed inebriated."  Saibal Chatterjee of NDTV gave the film 1.5 out of 5 stars and concluded that "Bol Bachchan is a comedy so absurd that it could reduce you to tears of despair. Conversely, if you have the stomach for such rampant silliness, it might propel you into paroxysms of delight. The call is entirely yours."  Kunal Guha of Yahoo!|Yahoo! India rated the movie 1 out of 5 stars and said, "Bol Bachchan (BB) jams chopsticks up the nose of Hrishikesh Mukherjees comic classic Gol Maal and digs itself six feet under with it. While the story is same in theory, being a Rohit Shetty film only adds some cars nailing somersaults, trucks attempting a ballet, baddies playing mid-air Garba after being biffed and Ajay Devgn drawing his eyebrows close enough to show that he means business." 

==Awards==
* Winner, Screen Award for Best Comedian: Abhishek Bachchan
* Winner, Stardust Award for Best Actor (Comedy/Romance): Abhishek Bachchan
* Winner, Zee Cine Award for Best Actor in a Comic Role: Abhishek Bachchan
* Winner, Big Star Entertainment Award for Best Actor in Comedy: Abhishek Bachchan
* Nominated, Stardust Award for Best Breakthrough Supporting Performance (Female): Prachi Desai
* Nominated, Stardust Award for Best Comedy Actress (Female): Asin Thottumkal
* Nominated, Screen Awards Best Actress (Popular Choice): Asin Thottumkal

==Box office==

===Domestic===
Bol Bachchan opened better at single screens as compared to multiplexes. Single screens on average opened to around 70% mark while multiplexes were more closer to the 60% mark.  Bol Bachchan collected around   nett on its first day.  Bol Bachchan had a good jump on Sunday as it grossed around   nett for a   nett weekend.  The film collected   on Monday.  Bol Bachchan has come into its own on Tuesday with strong collections of around   nett and a total of   nett in five days.  Bol Bachchan has had a good first week and collected   nett.The first week collections of Bol Bachchan are better than Singham but less than Golmaal 3.  Bol Bachchan collected around   nett on its second Friday. It is a 70% drop from its first day as multiplex business was hit by the new release Cocktail (2012 film)|Cocktail but single screen business remained good. The film has collected around   nett in eight days.  Bol Bachchan crossed the   nett mark in ten days as it grossed   nett approx over the weekend.  Bol Bachchan grossed around   nett in its second week taking its two-week total to   nett.  Bol Bachchan grossed   nett approx in week three,thus taken its total to  .  Bol Bachchan had grossed   at the end of its run in domestic market.    Its final distributor share was around  . 

===Overseas===
Bol Bachchan had a decent opening Overseas of around $2 million. UAE was good as comedy films do better in that market.  Bol Bachchan has grossed   in 17 days.  Finally,The film netted US$4 Million overseas.  

==See also==
* List of highest-grossing Bollywood films

==References==
 

==External links==
*  

 

 
 
 