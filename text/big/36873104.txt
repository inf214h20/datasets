Kalidasu
{{Infobox film
| name = Kalidasu
| image = Film_poster_of_Kalidasu.jpg
| alt =  
| caption =
| director =G. Ravicharan Reddy
| producer = Chinthalapudi Srinivasa Rao    A.Naga Suseela
| writer = G. Ravicharan Reddy     (Story)   G. Ravicharan Reddy    (dialogues ) 
| screenplay = G. Ravicharan Reddy
| starring = Sushanth Tamannaah Chakri
| cinematography = Akhilan
| editing = Goutham Raju	 		
| studio = Sri Nag Corporation   Annapurna Studios   (Presents)  
| distributor = 
| released = 11 April 2008
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
}}

 Kalidasu   ( ) is a 2008 Telugu film, directed by G. Ravicharan Reddy and produced by Chinthalapudi Srinivasa Rao and A. Naga Suseela, under Sri Nag Corporation. The film stars Sushanth and Tamannaah in the lead roles.   The film was a success at box office playing more than 50 days at a number of centers across Andhra Pradesh. 

==Plot==
Kalidasu (Sushanth) is the son of a sincere police officer (Nagendra Babu|Nagababu), who is killed in the hands of a factionist (GV). However, Kalidasu kills that factionist and escapes from Rayalaseema and reaches Guntur. Kanakadurga (Jayasudha) brings him up along with her son Jai Dev (Vamsikrishna). Kalidasu helps Dev to continue his studies in Hyderabad, by turning himself a car-lifter. Baba (Chalapati Rao) buys such lifted cars. His son Basha (Sunil (actor)|Sunil) also becomes his good friend. Once, Kalidasu lifts a car and finds a girl hiding in it. The girl says that her name is Mirchi (Tamannaah) and pretends that she is a lunatic. But Kalidasu does not believe that she is lunatic and suspects her that she is pretending. Once some goons led by Pratap and Ajay (Vinodkumar and Ajay (actor)|Ajay) reach Guntur and take away Mirchi after killing Basha.

Kalidasu goes to Hyderabad after noticing Devs photo with Mirchi in her mobile phone. However, after going to the college, the principal (Tanikella Bharani) reveals that Dev is killed by the goons led by a political leader (Rajan P. Dev) and his henchmen Pratap and Ajay to save the college maintained by Archana (Real name of Mirchi). Kalidasu decides to take revenge against the killers of Basha and Dev. Then Kalidasu starts his mind game and plays a trick, which makes Pratap to kill his own brothers. Later, Ajay kills his brother-in-law Pratap. In the climax, Kalidasus trick again makes Ajay meet his own death. The film ends on a happy note as Kalidasu and Archana are in Love and lead a happy life.

==Cast==
*Sushanth as Kalidasu
*Tamannaah as Archana Ajay as Ajay Sunil as Basha
*Vamsikrishna as Jayadev
*Rajan P. Dev as Home Minister
*Vinodkumar as Pratap
*Jayasudha as Kanakadurga Nagababu as Kalidasus Father
*Ravi Babu as Police Officer
*Tanikella Bharani as Principal of College
*Chalapathi Rao

==Reception==
Telugucinema.com gave a review of rating 3/5 stating "Sushanth makes decent debut. He can become good actor by honing skills further. A very formulaic story but decent film for a debutant."  Oneindia Entertainment gave a review stating "Despite less glamour, the debutant hero, Sushant, is able to gain mass hero image. There are several elements in the film like sentiment, glamour, action and love scenes in the film to cater to the needs of all classes of audiences. The film does not appear like a debutants film and the production values of Sri Nag Corporation are adequate."  IndiaGlitz gave a review stating "Sushant showed excellent ease in dances and action scenes, despite it is his debut film. Tamanna too looked gorgeous in the film in ultra modern outfits. She was very romantic all through the film. Though there is not much scope to prove her acting talents, her role was moulded in a near sexy way. The director was able to maintain good tempo all through the film. He did never lose grip on the story. He moulded the heros character with a sole aim of giving him a mass image."  Cinegoer.com gave a review stating "The movie has nothing new or good to offer, but it isnt an unbearable affair either. Sushanths debut is not bad, but a makeover and voice modulation, some more of the grooming and training (dance-fights-etc) hes obviously received will go a long way. For now, he has miles to travel before he gets a claim to fame. The movie has taken a middling formulaic route."  Sify.com gave a review stating the film to be a watchable one. 

==Music==
{{Infobox album
| Name = Kalidasu
| Longtype = to Kalidasu
| Type = Soundtrack Chakri
| Cover = Kalidasu ACD.jpg
| Released = March 31, 2008
| Recorded = 2008 Feature film soundtrack
| Length =  Telugu
| Label = Aditya Music Chakri
| Reviews =
| Last album = 
| This album = 
| Next album = 
}} Chakri and Chandrabose 

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! No. !! Song !! Singers !! Length (m:ss)
|- Chakri (music director)|Chakri, Kousalya || 05:13
|- 2 || "Thadisi Mopedu "||Ravivarma, Anuradha Sriram ||05:10
|-
| 3||  "Ellake Ellake "|| Vasu, Geeta Madhuri ||04:03
|-
| 4|| "Prema (Okasari Karam) "|| Chakri,Kousalya ||03:58
|- Devan ||04:37
|- Baba Sehgal  ||04:27
|}

==References==
 

==External links==
* 

 
 
 