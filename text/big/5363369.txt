Yanks
 Yank}}
{{Infobox film
| name           = Yanks
| image          = Yanks poster.jpg
| caption        = Theatrical release poster
| director       = John Schlesinger
| producer       = Joseph Janni Lester Persky
| story          =Colin Welland
| screenplay         = Colin Welland Walter Bernstein
| narrator       = Rachel Roberts Lisa Eichhorn
| music          = Richard Rodney Bennett
| cinematography = Dick Bush Jim Clark
| studio  = CIP Filmproduktion GmbH
| distributor    = Universal Pictures (US) United Artists (UK)
| released       =  
| runtime        = 141 minutes
| country        = United Kingdom West Germany United States English
| budget         =
}} 1979 Period period drama Sunday Bloody Sunday which he directed in 1971. Despite being set during the Second World War, the film is a character study which features no combat or fighting scenes.
 American soldiers GIs or "Yanks" and the more reserved British population.

==Plot== invasion of Europe over a million Americans landed in Britain. They came to serve on other battle fronts or to man the vast U.S. bases in England. Hardly a city, town or village remained untouched."
 American troops British soldier Englishmen she platonic at first.

For her part, Helen (Vanessa Redgrave) is a bit more worldly in her affairs. Captain John (William Devane) comes to her estate often, and a relationship develops. They are both married, but her husband is away at sea, and his wife is thousands of miles distant.
 Rachel Roberts) condemns their relationship as a kind of betrayal.
 Welsh seaside resort, where they make love but without completion. Jean is crushed, although Matt says "not like this."  She feels spurned, and that her willingness to risk everything has not been matched by him, concluding that he is "not ready" for her.

Shortly afterwards, the Americans ship out by troop train to Southern England to prepare for D-Day. A characteristic last-minute gift and message from Matt prompt Jean into racing to the railway station. With the town and station a hive of activity, hundreds of the townswomen, some of them pregnant from liaisons with men they may never see again, scramble to catch one last glimpse of their American boyfriends before the train leaves. Matt shouts from the departing train that he will return.

==Cast== 1st Sgt Matt Dyson
* Lisa Eichhorn as Jean Moreton
* Vanessa Redgrave as Helen Captain John Sgt Danny Ruffelo Wendy Morgan as Mollie Rachel Roberts as Mrs. Clarrie Moreton
* Tony Melody as Mr. Jim Moreton

==Production==
Schlesinger was able to obtain the finance to make Yanks - which was a personal project - because of the financial success of his   on January 30, 2012. ISBN 0-934223-59-9, ISBN 978-0-934223-59-1. 
 Hyde  pillbox attached to a former Royal Ordnance Factory in Steeton, West Yorkshire.  However the cinema sequences were shot at the Rainbow Theatre, Finsbury Park, London and exterior shots of the unnamed Welsh resort were filmed along Happy Valley Road, Llandudno, North Wales.

The ending, where the troops board their train to head to the front, were filmed at Keighley railway station on the line belonging to the Keighley and Worth Valley Railway. An authentic World War II locomotive, which is preserved by the heritage railway, was used for the scene.

==References==
 

==External links==
* 
* 
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 