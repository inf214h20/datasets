New Delhi (1987 film)
 
{{Infobox film
| name = New Delhi
| image = Newdelhi1987film.jpg
| caption = Poster designed by Gayathri Ashokan Joshiy
| producer       = Joy Thomas
| writer         = Dennis Joseph
| narrator       =  Urvashi Devan Devan Vijayaraghavan Vijayaraghavan Mohan Jose Shyam
| cinematography = Jayanan Vincent
| editing        = K. Sankunny
| stunts         = A. R. Basha
| studio         = Jubilee Productions
| distributor    = Jubily Pictures 
| released       =  
| runtime        = 143 minutes 
| country        = India crime  drama  thriller
| language       = Malayalam 
| budget         = 
| gross          = 
}}
 Malayalam Thriller thriller film written by Dennis Joseph and directed by Joshi (director)|Joshiy.  It stars Mammootty, Suresh Gopi, Sumalatha, Urvashi (actress)|Urvashi, Thiagarajan|B. Thiagarajan, Siddique (actor)|Siddique, Vijayaraghavan (actor)|Vijayaraghavan, Mohan Jose, Devan (actor)|Devan, and Jagannatha Varma. 
 1987 after having a string of box-office flops in the prior year.  Many later films adapted the storyline of New Delhi.
 Telugu as Anthima Theerpu, Hindi version and Ambareesh in Kannada version, respectively.

Thiagarajan, who played the role of Salem Vishnu in New Delhi, later produced and directed a Tamil film titled Salem Vishnu which showcased the prequel story of his character and why he became a murderer.

==Plot==

New Delhi is about Delhi-based Malayali journalist, G. Krishnamoorthy (Mammootty), who is imprisoned in a mental asylum after exposing the wrongdoings of two corrupt politicians, and the film follows his subsequent attempts at revenge. The story is loosely based on the novel  The Almighty by Irving Wallace. It ran clean for 200 days.

==Summary==
 Urvashi plays Republic Day and brutally rape her in a hotel room. G.K. attempts to report the incident in the newspaper, but is foiled by his chief editor, who notifies the villains. After being arrested on fake charges, G.K. is brought to trial to face many false witnesses including Fernandes and her father, who claims nobody harmed his daughter. G.K. is deemed mentally unfit and sentenced to one and a half years in a mental asylum, as well as five years in prison where he suffers abuse. G. K. meets Ananthan (Vijayaraghavan (actor)|Vijayaraghavan), Siddiq (Siddique (actor)|Siddique) and Appus acquaintance from the jail. He was once selected to be set free from prison on Republic day, but he was denied the chance by Shankar.

Nataraj Vishnu (Thiagarajan) is a murderer who was sentenced to death by the court. G.K., who already has a plan of revenge, meets Vishnu and offers to help him break out of jail. After completing the five-year period, G.K. is free. Maria is all set, ready with a new newspaper which she dedicates to G.K. He arranged help for Vishnu, Ananthan, Siddiq, and Appu to break out of jail. G.K. delays the issuing of his newspaper even though the newspaper had all the facilities and trained journalists, including Uma and Suresh (Suresh Gopi), Umas fiancée. He wants his newspaper to release only after getting sensational news.

. The newspaper was printed the night the judge who had sentenced G.K. to imprisonment was killed. The newspaper has a grand reception by the people since this sensational news. G.K. murdered his torturers brutally and reported this in his newspaper before the actual occurrence of the incident. G.K.s newspaper soon becomes the leading newspaper in India.

Uma and Suresh have doubts about how the news being published in their newspaper so early by the "unknown" reporter. G.K. instructs the team of four to kill C. R. Panikkar. He is electrocuted and killed, the torturing method G.K. faced in the mental asylum. This is witnessed by Suresh and the team realises it and reports to G.K. who instructs the team to kill Suresh who has some evidences and photos about this. G.K., who later came to know about the affair between Uma and Suresh, tries to rescue Suresh but could not.

The last prey of G.K.s series of killings was Shankar, who is a central minister. G.K., as usual, instructs the team to kill Shankar, and he publishes a news about Minister Shankars murder. But the team could not cross the tight security to kill him. They all were killed in an encounter with police. G.K., who was all set to release his next days newspaper, is arrested by the police for conspiring to attack Shankar. Shankar comes to the office of G.K. to harass him.  Soon, Maria Fernandes shoots Shankar with the pistol. The police try to lock her, but she succeeds in killing him.

==Cast==
*Mammootty as G. Krishnamoorthy aka G.K.
*Sumalatha as Maria Fernandes
*Thiagarajan as Nataraj Vishnu aka Salem Vishnu Urvashi as Uma
*Suresh Gopi as Suresh Devan as Shankar
*Jagannatha Varma as C.R. Panikkar
*Prathapachandran as Jailor Vijayaraghavan as Ananthan Siddique as Siddique
*Mohan Jose as Appu

==Production==

===Casting===
The original cast included Mammootty, Sumalatha, Suresh Gopi, Devan, and Urvashi (actress)|Urvashi, Thiagarajan, Siddique (actor)|Siddique, Vijayaraghavan (actor)|Vijayaraghavan, Mohan Jose, and Jagannatha Varma. Most of these actors were in the early days of their careers.

===Filming===
The film was scheduled to begin on January 17, 1987, in New Delhi, but due to tight security following communal violence in Delhi, it was postponed to January 29, which created a huge loss to the producer. The film was shot on location in New Delhi. Major portions were shot in the Kerala House and in the Delhi Jail. Principal photography was completed on February 14, after a tight 16- to 17-day schedule, a relatively short time period.
*Newdelhi Ran 115 days in Ernakulam shenayees complex it placed Top 2 in 1987.

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|-
| 1988 Antima Teerpu Cinema of Telugu
|Krishnamraju, Ranganath
|Joshiy
|-
| 1988 New Delhi Hindi
|Jeetendra, Sumalatha, Raza Murad, Thiagarajan Joshiy
|-
| 1988
|New Delhi Cinema of Kannada
|Ambareesh, Sumalatha, Thiagarajan Joshiy
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 