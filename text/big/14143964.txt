Hop Harrigan (serial)
{{Infobox film
| name           = Hop Harrigan
| image          = 
| image_size     = 
| caption        = 
| director       = Derwin Abrahams
| producer       = Sam Katzman
| writer         = 
| narrator       = 
| starring       = William Bakewell Jennifer Holt Robert Buzz Henry Sumner Getchell
| music          = 
| cinematography = 
| editing        = 
| distributor    = Columbia Pictures
| released       = 1946
| runtime        = 
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Columbia Serial film serial, based on the Hop Harrigan comic books.

==Plot==
Hop Harrigan (Bakewell) and his pal "Tank" Tinker (Getchell) operate a small airport and flying service where they are hired by J. Westly Arnold (Vogan) to fly scientist, Dr. Tabor (Merton), to his secret laboratory where he has a revolutionary new power unit. But an unknown character known as The Chief Pilot (Oakman) is interested in the invention and uses a destructive ray to cripple Hops airplane and kidnap Tabor. Hop and "Tank", aided by Gail Nolan (Jennifer Holt) and her younger brother, Jackie (Robert Buzz Henry), finally overcome the criminals only find a bigger threat to them all within their group...

==Cast==
* William Bakewell as Hop Harrigan. Bakewell is older than the character he is playing but a "convincing enough hero." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 31
 | chapter = 3. The Six Faces of Adventure
 }} 
* Jennifer Holt as Gail Nolan
* Robert Buzz Henry as Jackie Nolan
* Sumner Getchell as "Tank" Tinker
* Emmett Vogan as J. Westly Arnold
* Claire James as Gwen Arnold
* John Merton as Dr. Tobor
* Wheeler Oakman as Alex Ballard/The Chief Pilot Ernie Adams as Retner
* Peter Michael as Mark Craven Terry Frost as Barry
* Anthony Warde as Edwards
* Jackie Moran as Fraser
* Bobby Stone as Gray
* Jack Buchanon as Deputy Sheriff

==Production==
 
Hop Harrigan was based on Jon Blummers All-American Comics and associated radio series. 

==Critical reception==
Cline wrote that Hop Harrigan is "a fairly action-filled cliffhanger...  action was well paced, making this chapterplay as convincing and successful as it was meant to be." 

==Chapter titles==
# A Mad Mission
# The Secret Ray
# The Mystery Plane
# Plunging Peril
# Betrayed by a Madman
# A Flaming Trap
# One Chance for Life
# White Fumes of Fate
# Dr. Tobors Revenge
# Juggernaut of Fate
# Flying to Oblivion
# Lost in the Skies
# No Escape
# The Chute that Failed
# The Fate of the World
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 243
 | chapter = Filmography
 }} 

==See also==
*List of film serials by year
*List of film serials by studio

==References==
 

==External links==
*  
*  

 
{{succession box  Columbia Serial Serial 
| before=Whos Guilty? (1945 in film|1945)
| years=Hop Harrigan (1946 in film|1946)
| after=Chick Carter, Detective (1946 in film|1946)}}
 

 
 

 
 
 
 
 
 
 
 
 