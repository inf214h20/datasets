Hello-Goodbye
{{Infobox film
| name           = Hello-Goodbye
| image          = Hellogoodbye1970.jpg
| caption        = Hello-Goodbye
| director       = Jean Negulesco
| producer       = André Hakim
| writer         = Roger Marshall
| starring       = Michael Crawford Genevieve Gilles Curd Jürgens
| music          = Francis Lai
| cinematography = Henri Decaë
| editing        = Richard Bryan
| studio         = Darryl F. Zanuck Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 107 min.
| country        = United Kingdom
| language       = English
| budget         = $4.4 million 
}}

Hello-Goodbye is a 1970 British light comedy film, starring Michael Crawford, and directed by Jean Negulesco, whose final film this was. 

==Plot==
Harry England (Crawford), a British car salesman on a trip to France, meets a Baroness "Danny" (Genevieve Gilles) when her Rolls-Royce breaks down. They spend a few days together and become lovers before she disappears one night, but Harry does not know her surname.

The Baron (Curd Jürgens) then hires Harry to teach his teenage son about cars on their country estate. Harry encounters the Baroness again and their affair continues. Harry falls in love and asks the Baroness to leave the Baron, who has taken up with a lady of his own.

==Cast==
* Michael Crawford as Harry England
* Genevieve Gilles as Dany
* Curd Jürgens as Baron De Choisis
* Ira von Fürstenberg  as Evelyne Rossan 
* Lon Satton as Cole Strutter Peter Myers as Bentley Mike Marshall as Paul
* Didier Haudepin as Raymond
* Vivian Pickles as Joycie
*   as Monique
*   as the hotel porter
* Denise Grey as the concierge
* Jeffry Wickham as Dickie

==References==
 

==External links==
*  
*  
*  

 

 
 