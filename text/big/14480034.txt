My Brother-in-Law Killed My Sister
{{Infobox film
| name           = My Brother-in-Law Killed My Sister
| image          = Mon beau-frère a tué ma soeur.jpg
| caption        = original poster
| director       = Jacques Rouffio
| producer       = Giorgio Silvagni
| writer         = Georges Conchon Jacques Rouffio 
| starring       = 
| music          = Philippe Sarde 
| cinematography = Jacques Loiseleux 
| editing        = Anna Ruiz 
| distributor    = Société des Etablissements L. Gaumont 
| released       =  
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = 
}}
My Brother-in-Law Killed My Sister ( , ( ) is a 1986 French film directed by Jacques Rouffio. It played in competition at the 36th Berlin International Film Festival.   

==Synopsis==
Two members of the French Academy agree to help the attractive young veterinarian Esther investigate the suspicious death of her sister. Esther is convinced her brother-in-law is responsible, but soon it becomes apparent that those responsible are linked to the very highest echelons of power in the Vatican.

==Cast==
* Michel Serrault: Octave Clapoteau
* Michel Piccoli: Stephen Sembadel
* Juliette Binoche: Esther
* Jean Carmet: Jocelyn Bouloire
* Milva: Renata Palozzi
* Pierre Bisson: H
* Tom November: Leo
* Isabelle Petit-Jacques: Micheline
* Agnes Garreau: Muriel
* Francis Girod: The TV interviewer
* Gérard Klein (actor)|Gérard Klein: The radio journalist

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 