Armaan (2013 film)
{{Infobox film
| name           = Armaan
| image          = Armaan film.jpg
| alt            = 
| caption        = Making of Ko Ko Korina.
| director       = Anjum Shahzad
| associate director       = Wajahatullah Khan
| producer       = Abdullah Kadwani 7th Sky Entertainment Tarang Housefull
| writer         = Vasay Chaudhry
| based on       =  
| music          = Farrukh Abid & Shoaib Farrukh
| cinematography = 
| editing        = Ehtesham Hameed Khan (ET) & Waqas Ali Khan (VIKY)
| studio         = 7th Sky Entertainment
| distributor    = Geo Films
| released       =  
| runtime        = 140 minutes
| country        = Pakistan
| language       = Urdu
| budget         = 18 millions
| gross          = 
}}
 same name released in 1966, which starred Waheed Murad and Zeba.  The film is produced by Tarang Housefull, Abdullah Kadwani and 7th Sky Entertainment. Film stars Fawad Afzal Khan, Aamina Sheikh, Vasay Chaudhry and Mahnoor Khan. Fawad Afzal Khan and Aamina Sheikh won Tarang Housefull Awards for Best On Screen Couple.

== Plot ==
The film tells of a beautiful but underprivileged girl, Zarnaab, living in Murree with her widowed stepmother and half-sister, Zartaab, and a flirty man, Armaan. Armaan meets Zarnaab at a friends Mayoon and is mesmerized by her. Trying to flirt with her, Zarnaab pranks him by switching places with a hermaphrodite. The encounter of Armaan flirting with the hermophrodite is recorded on the video of the ceremony. This leads to an infuriated Armaan. Armaans manager, Danny, picks him up from the ceremony and drives him home. In the car, Armaan orders Danny to find out the name and address of the girl, Zarnaab, who pranked him. 

The following morning, Armaans grandfather (and also his only guardian) informs Armaan about his dead fathers letter in which he stated that he would prefer that Armaan marries his relativss daughter in Murree. Meanwhile, Danny manages to find out about Zarnaab and also that she lives in Murree. Armaan is reluctant at first to go to Murree to meet the girl his grandfather insisted him to meet but when Danny informs him about Zarnaabs location, he agrees to go.
  
On their way to Murree, their car stops working. Danny insists that they walk to the relatives house but Armaan, then, informs Danny that they are switching roles as Armaan does not want to meet the relatives daughter. Armaan strolls of to explore Murree and search for Zarnaab. Upon hearing the piano playing from a nearby school, Armaan, follows the sound to the classroom where it is being played. He finds out the Zarnaab is the one who is playing the piano.  Panic struken, Zarnaab calls the security guard and the principal, Zarnaabs stepmother, also arrives. They kick Armaan out of the school boundaries.

Danny, disguised as Armaan, arrives at the relatives house. Armaan also arrives their to find out that the schools principal is also the relative. Danny (fake Armaan) informs them all that Armaan (fake Danny) is his manager. Danny meets Zartaab but Zartaab doesnt like him. Zartaab is in love with a guy named Salman and wants to marry him only. Her mother doesnt know about him but when she informs her mother, she instantly disagrees. Meanwhile, Armaan (fake Danny) and Zarnaab fall in love. At one incident, Zarnaab and Armaan are sitting near a tree covered with shredded cloth. When he questions the state of the tree being like that, Zarnaab narrated to him the story behind it. Many 100 years ago, there were two lovers who wanted to get married but their families disagreed. So they decided that if they cant live together, at least they can die together. Before jumping off the cliff, they leave a sign of their love, the cloth, on the tree. When they jump, the cloth rips off and pulls them back to the cliff. Armaan is skeptic and Zarnaab tries to prove him the truthfulness of the story. She takes a red cloth and ties it to the tree and is about to jump off the cliff but Armaan stops her and leaves angry. She, then, mumbles that he didnt ask whose name did I tie that cloth for, implicating that she tied the cloth for Armaan.

At a friends wedding, Zartaab meets Salman to inform him that the situation is becoming really serious and he should inform his family and they should get married, otherwise, she would have to marry Danny (fake Armaan). Salman dumps her and walks away. Meanwhile, Armaan in on the verge of confessing to Zarnaab about him being Armaan and not Danny but she finds out before he could confess and is infuriated. They arrive home where Zartaab storms off into her room when her mother asks her if she spent quality time with Danny (fake Armaan).  

The following day, Armaan apologizes to Zarnaab and they patch up. When Armaan arrives home, he is shocked to find his grandfather there and the household finds out about their act. Armaan and Danny are punished by Armaans grandfather for disguising themselves by doing some sit-ups. Armaan, then, insists his grandfather to ask for Zarnaabs hand in marriage. Meanwhile, Zartaabs mother asks her why she doesnt want to marry Armaan and she confesses that she is pregnant with Salmans child. Furthermore, she tries to commint suicide but is saved in time. Her mother implicates for the childs abortion but the doctor states that it is too late to abort the child. Also, Zartaab doesnt want to abort it. Zartaabs mother, then, requests Zarnaab to insist Zartaab to marry Armaan. Zarnaab sacrifices her love for Armaan for the sake of her sister and stepmother. Zarnaab shows Armaan Zartaabs untrasound photos and states she is pregnant with her lovers child and that he should marry Zartaab. So he asks for Zartaabs hand in marriage.

On the day of their wedding, Salman goes to Zartaabs bedroom to ask for forgiveness to which she shouts that he tried to ruin her life and foremost her sisters. She states that Zarnaab sacrificed by lying to Armaan about the baby being hers so that the family isnt lowered in respect from the societys prospective. Armaan overhears that and leaves to find Zarnaab. Armaan rushes to find her trying to commit suicide on the cliffs. He holds her hand to stop her and then, takes her back. Before leaving, he takes of the cloth from the tree and throws it away.

== Cast ==
* Fawad Afzal Khan as Armaan
* Aamina Sheikh as Zarnaab
* Lubna Aslam as Zartaabs mother.
* Manzoor Qureshi as Armaans grandfather
* Mahnoor Khan as Zartaab
* Jahanzaib Khan as Salman 
* Vasay Chaudhry as Danny

== Soundtrack ==
{{Infobox album  
| Name = Armaan
| Type = Soundtrack Shaan and Sunidhi Chauhan
| Cover =
| Genre = 
| Producer =Abdullah Kadwani Urdu
| Length =  
}}
 Shaan and Neeraj Shridhar.

{{Track listing
| headline = Tracklist
| extra_column    = Singer(s)
| total_length    = 
| title1          = Ko Ko Korina
| extra1          = Neeraj Shridhar and Sunidhi Chauhan
| length1        = 5:32
| title2          = Akelay Na Jaana 
| extra2         = Rahat Fateh Ali Khan and Shreya Ghoshal
| length2         = 5:23
| title3          = Jab Piyar Main Do Dil Shaan
| length3        = 5:38
}}

== References ==
 

 
 
 
 
 
 
 