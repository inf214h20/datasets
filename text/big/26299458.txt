Plastic Bag (film)
{{Infobox film
| name           = Plastic Bag
| image          = PlasticBagPoster(wlaurels).jpg
| caption        = 
| director       = Ramin Bahrani
| producer       = Adam Spielberg   Ramin Bahrani
| writer         = Ramin Bahrani   Jenni Jenkins
| starring       = Werner Herzog
| music          = Kjartan Sveinsson
| released       =  
| runtime        = 18 minutes
| country        = United States
| language       = English
}} Telluride and the New York Film Festival. The film is part of the Independent Television Service (ITVS) online series Futurestates and was produced by Noruz Films   and Gigantic Pictures. 

==Plot==
In a not too distant future, a Plastic Bag (voice of Werner Herzog) goes on an epic journey in search of its lost Maker, wondering if there is any point to life without her. The Bag encounters strange creatures, brief love in the sky, a colony of prophetic torn bags on a fence and the unknown. To be with its own kind, the Bag goes deep under the oceans into   of spinning garbage known as the North Pacific Trash Vortex.

==Screenings==
*Corto Cortissimo, Venice Film Festival, 2009
*Official Selection, New York Film Festival, 2009
*Official Selection, Telluride Film Festival, 2009
*Official Selection, South by Southwest Film Festival, 2010

== References ==
 
 

== External links ==
* site about it| 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 