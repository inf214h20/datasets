Badhti Ka Naam Dadhi
{{Infobox film
| image          = Badhti Ka Naam Dadhi.jpg
| name           = Badhti Ka Naam Dadhi
| director       = Kishore Kumar
| music          = Kishore Kumar
| released       = 1974
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
 1974 Bollywood drama film directed by Kishore Kumar. The name is similar to a previous Kishore Kumar film Chalti Ka Naam Gaadi.

==Plot==
The premise of the film is that a multimillionaire, who has no heir, decides to leave his wealth to the person who has the longest beard. What follows is complete mayhem as Kishore Kumar and K.N. Singh plot to outwit one another.

==Cast==
* I. S. Johar as Seth Jhunjhunwala
* Amit Kumar as Theatre Owners Nephew
* Ashok Kumar as Gulfam
* Kishore Kumar as Theatre Owner / Police Commissioner / Director / Constable
* Bappi Lahiri as Bappu
* Sheetal as Jhunjhunwalas Secretary
* K.N. Singh as Khadak Singh

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Badhti Ka Naam Dadhi"
| Kishore Kumar, Manna Dey, Mohammed Rafi
|-
| 2
| "Bhole Re Sajan"
| Kishore Kumar, Asha Bhosle
|-
| 3
| "He Gori Banki Chhori"
| Kishore Kumar
|-
| 4
| "Hun Kaun Chhun Mane Khabar"
| I. S. Johar, Bhagwan, Maruti, Sundar
|-
| 5
| "Phir Suhani Sham Dhali"
| Kishore Kumar
|-
| 6
| "Sun Chache Bol Bhatije"
| Kishore Kumar, Amit Kumar
|-
| 7
| "Yeh Jawani Din Char"
| Bappi Lahiri
|-
| 8
| "Zindagi Hasin Hai" Bhupinder Singh
|}

==External links==
*  

 
 
 
 

 
 