The Collingswood Story
 
{{Infobox film
| name               = The Collingswood Story
| writer             = Mike Costanza
| starring           = Stephanie Dees Johnny Burton Grant Edmonds Vera Madeline
| director           = Mike Costanza
| producer           = Beverly Burton
| music              = John DeBorde
| distributor        = Anchor Bay Entertainment
| released           = June 21, 2006 (UK)
| runtime            = 86 min. English
}}
The Collingswood Story is an award winning indie horror film, hailed by Shivers Magazine as "The Best Low Budget Movie Since The Blair Witch Project".  The Collingswood Story was released in 2006 by Anchor Bay UK/Starz Home Entertainment.

== Plot ==
The Collingswood Story focuses on the relationship between college students Rebecca Miles and Johnny Dalmass who attempt a long distance relationship via webcams.

Rebecca Miles has left her hometown—and her boyfriend, John—behind to go to college in Collingswood, New Jersey. For her 21st birthday, John gives her a webcam so the two can stay in touch.

The couple is soon plunged into a nightmarish world when they stumble upon an online psychic, Vera Madeline, who initiates a deadly reign of terror over their lives.

== Background ==
 
The Collingswood Story, which began production in the year 2000, employed the concept of a feature film viewed entirely through each characters webcam.  At the time Skype, YouTube, and built-in computer cams was years before the concept was mainstream and distributors were reluctant to release the "webcam" film.

The filmmaker, Mike Costanza, sent screeners to every major horror website in the US and the UK.  The online horror community, hesitant at first to watch an indie film with no distribution, found The Collingswood Story scary and innovative and began to generate a buzz for the new "indie thrill sensation".  The online horror communitys support helped put this indie horror film on the map.

In 2005, The Collingswood Story screened at many international festivals including  , Dreamwatch, SFX (magazine)|SFX, and journalist Kim Newman also praised the film.

 
The Collingswood Story was then pitched as a remake to various studios including the producers of Paranormal Activity.

In April 2011,   screened the film during their celebration of Indie Horror Month.

== Awards ==
* Best Cast, Fearless Tales Film Festival San Francisco, California 2005
* Best Indie Film, Horror Review Awards

== Festivals ==
Frightfest, London 
Festival of Fantastic Films, Manchester 
Fearless Tales, San Francisco 
Boston Fantastic Film Festival 
Horrorthon, Dublin

== References ==

 

"The Best Low Budget Movie Since The Blair Witch Project"—Shivers Magazine, October 2006, Iss. 108, pg. 66, by: Calum Waddell
"Kim Newmans Dungeon Breakout Name to Watch: Michael Costanza"—Empire Magazine October 2006, pg. 66, by: Kim Newman
"Pleasantly Spooky Post Blair Witch Ghost Story"—SFX Magazine" August 2006, Iss. 146, pg. 130, by: Andrew Osmond
"The Making of The Collingswood Story"—The Dark Side Magazine (UK) September 2006, Iss. 122, pg. 60, by: Cameron Scott
DVD Monthly August 2006, Iss. 95, pg. 132, by: Jordon Brown

== External links ==
*  
*  
*  

 
 
 
 
 