Asphalt (1929 film)
{{Infobox film
| name           = Asphalt
| image          = ASPHALT orig colour.jpg
| image_size     =
| caption        = 
| director       = Joe May
| producer       = Erich Pommer
| writer         = Hans Szekely
| narrator       = 
| starring       = Gustav Fröhlich Else Heller Albert Steinruck Betty Amann
| music          = 
| cinematography = Günther Rittau
| editing        = 
| distributor    = Universum Film AG
| released       =  
| runtime        = 93 min
| country        = Weimar Republic German intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 German silent film. The film was one of the last silent films released in Germany as the world was entering the era of sound film.

==Production==

===Crew===
Asphalt was made by Universum Film AG|UFA; a German studio, and produced by the Erich Pommer who was responsible for producing several films by directors including Fritz Langs Der müde Tod (1921), Dr. Mabuse, der Spieler (1922), Die Nibelungen (1924), Metropolis (1927 film)|Metropolis (1927), and Spione (1928), F. W. Murnaus The Last Laugh (1924), Faust (1926 film)|Faust (1926), Tartüff (1927), and other popular films of the era including The Cabinet of Dr. Caligari (1920) and Varieté (1925).

Director Joe May co-wrote the script. The sets were designed by  , with the uncredited assistance of Robert Herlth (Der müde Tod, Der letzte Mann, Tartüff, Faust) and Walter Röhrig (Das Cabinet des Dr. Caligari, Tartüff, Faust). The cinematographer was Günther Rittau (Die Nibelungen, Metropolis, Der blaue Engel).

==Synopsis==
Else, played by Betty Amann, becomes the tragic hero of Asphalt as her character transcends her unhappy criminal life to redeem herself in an unfolding confession of true love. 

As fitting with the harsh urban landscape of Berlin, Else begins as a gorgeous trickster when the story begins. Her high fashion clothes and perfectly ornamented makeup make her deserving to be peering over diamond cases while batting her eyes in want at the jeweler. We immediately experience the power of her female persuasion between the cuts between the almost possessed jeweler and her. She is good at what she does- seducing men- not stealing diamonds parse. She lies as she gets caught professing it was the first time, that she needed the money. Even when she meets Albert, played by Gustav Frohlich, she insists her luxurious apartment and belongings are not hers. She maintains her story and pristine female fatal composure until she flings herself into his arms and confesses to him, I like you. A close-up captures the couples passionate embrace. 

Yet, it is the rare moments when Else thinks about Albert that we begin to see her character evolve. With most of her expression transcending from her sad eyes throughout the film. Her first smile in the film comes after she finds the passport photo of Albert in her apartment. Gazing at the photo she smiles comparing him to her criminal older and uglier boyfriend in a photo beside her. This is the first soft moment with this character and the smile is very contrast as well. She will stare and smile at his picture again in the nightclub, when she becomes compelled to return his passport and give him a gift of cigars, a scene that results to a confession of love from both Else and Albert. 

It is when Albert is at Elses feet, begging for her to be his wife when she can no longer stand the differences between them. As he looks up to her in her white elegant dress and she runs away. She breaks away and exposes all her stolen goods from her criminal past. Her confession becomes a conduit metaphor for the criminal underworld of the city as she pleads with him, Dont go, dont let me go down. She is looking for mercy and seeking to be redeemed. She believes he can save her and this is motivation enough for her to confess. 

In the closing scene, the director portrays Else in a dark suit voluntarily turning herself into the police. Her once flashy, teary eyes are humbled and filled with tears of compassion and love. This confession becomes an expression of her love and she rescues her lover from being accused of murder. 

She is able to smile once again as Albert follows her and professes he will wait for her. The gorgeous closing shot shows Albert watching Else through a barred doorway as she goes off to jail. 

===Premiere===
Asphalt was premiered on March 11, 1929 at Berlins prestigious Ufa-Palast am Zoo. Critics noted the cheap, pulp-fiction nature of the plot but also praised May’s skill and cinematography and editing. 

==Cast==
* Albert Steinrück as Hauptwachtmeister Holk
* Else Heller as Frau Holk 		
* Gustav Fröhlich as Wachtmeister Albert Holk
* Betty Amann as Else Kramer
* Hans Adalbert Schlettow as Konsul Langen
* Rosa Valetti as Frau an der Theke
* Kurt Vespermann as Curt Vesperman

==References==
 

==External links==
 
*   at the Masters of Cinema website
*   at Rotten Tomatoes
*  
*  
*  

 
 
 
 
 
 
 
 


 