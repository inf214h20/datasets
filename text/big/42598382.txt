Scream Park
{{Infobox film
| name           = Scream Park
| image          = File:Scream Park dvd cover.jpg
| alt            = 
| caption        = DVD cover
| film name      = 
| director       = Cary Hill
| producer       = 
| writer         = Cary Hill
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Wendy Wygant, Steve Rudzinski, Nivek Ogre
| music          = 
| cinematography = Nathan W. Fullerton
| editing        = Scott Lewis
| studio         = ProtoMedia
| distributor    = WildEye Releasing
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Scream Park is a 2013 horror film by American director Cary Hill and his directorial debut.  The film had its world release on January 6, 2013 in Pittsburgh, Pennsylvania and was partially funded through a successful Kickstarter campaign.  

Of the film, Hill stated that he was inspired to create Scream Park after visiting a theme park and thinking that it would be a good location for an 80s themed horror film. Filming took place at Conneaut Lake Park in Conneaut Lake, Pennsylvania and at the University of Pittsburgh.    

==Synopsis==
Jennifer (Wendy Wygant) works at Fright Land, a theme park thats closing after years of declining sales and lack of interest from the general public. While initially reluctant, Jennifer agrees to attend a party held by the other park employees to memorialize the parks closing. She cant help but feel that something feels off about the nights events and her suspicions are soon proven to be correct, as park owner Mr. Hyde (Doug Bradley) has decided that the only way to save the park is to hire two men to murder all of the parks employees. He theorizes that after the killers complete their task, morbid murder fans will pay large amounts of money to visit an amusement park themed around the murders.

==Cast==
*Wendy Wygant as Jennifer
*Steve Rudzinski as Marty
*Nivek Ogre as Iggy
*Alicia Marie Marcucci as Allison
*Nicole Beattie as Missi
*Doug Bradley as Mr. Hyde
*Kailey Marie Harris as Carlee
*Dean Jacobs as Tony
*Tyler Kale as Rhodie
*Ian Lemmon as Ogre
*Carrie Lee Martz as Attendant
*Kyle Riordan as Roy

==Reception==
Critical reception for Scream Park has been positive, and Bloody Disgusting commented that the movie had "a lot to enjoy".  DVD Talk noted that the movie was low budget and had some shortcomings, but that the acting was good and that fans of similarly low budget films would greatly enjoy the movie.  Aint It Cool News stated that Scream Park felt familiar to similar films in the genre, but that it "does the familiar really, really well." 

==References==
 

==External links==
*  
*  
*  

 
 
 