Catherine the Great (1920 film)
{{Infobox film
| name           = Catherine the Great 
| image          = 
| image_size     = 
| caption        = 
| director       = Reinhold Schünzel 
| producer       = Arzén von Cserépy
| writer         = Hans Behrendt Bobby E. Lüthge Reinhold Schünzel
| narrator       =  Fritz Delius
| music          = 
| editing        = 
| cinematography = Karl Freund
| studio         = Cserépy-Film
| distributor    = 
| released       = 22 October 1920
| runtime        = 
| country        = Germany Silent German German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 German silent silent historical Fritz Delius. epic portrayal extras and 500 horses were used. 

==Cast==
*Lucie Höflich - Katharina die Große 
*Fritz Kortner - Potemkin  Fritz Delius - Gregori Orlow 
*Reinhold Schünzel - Tsar Peter  Paul Hartmann - Alexander Manonow 
*Gustav Botz - Alexei Bestuschew 
*Gertrud de Lalsky - Elisabeth 
*Mechthildis Thein - Fürstin Woronzow 
*Leonhard Haskel - Bressan 
*Karl Platen - Feldmarschall Schwerin 
*Hugo Flink - Saltykow 
*Ilka Grüning   
*Albert Steinrück   
*Fritz Junkermann   
*Hans Behrendt   
*Agna Bergen   
*Lo Bergner   
*Else Brandus   
*Hanne Brinkmann   
*Alexander Ekert   
*Käthe Ellerbock   
*Reinhold François   
*Lotte Gühne   
*Ellinor Hansen   
*Loo Hardy   
*Nella Retslag   
*Suse Stollberg   
*Hedwig Strasser   
*Ferdinand von Alten   
*Cara von Joe   
*Ilse Wilke

==References==
 

==Bibliography==
*Kreimeier, Klaus. The UFA Story: A Story of Germanys Greatest Film Company 1918-1945. University of California Press, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 