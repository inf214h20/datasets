La sangre y la semilla
{{Infobox film
| name           =La sangre y la semilla
| image          = La sangre y la semilla.jpg
| caption        = 
| director       =  Alberto Du Bois
| producer       = Benjamìn Bogado
| writer         = Mario Halley Mora Augusto Roa Bastos
| starring       = 
| music          = Herminio Giménez
| cinematography = Luis Galán de Tierra	 	
| editing        = Rosalino Caterbeti Jorge Levillotti	 
| studio         =
| released       =12 November 1959
| runtime        = 82 minutes
| country        = Argentina Paraguay
| language       = Spanish Guaraní
| budget         = 
| gross          = 
}} Cerro Corá (1870).  The film premiered on 12 November 1959. 

==Plot==
Paquita, the widow of a Paraguayan officer, rescues a wounded sergeant who has been a companion of her husband. She takes care of him and nurtures him back to health. But they only have one hope, to keep fighting for Paraguay against the Argentine Army.

==Cast==
*César Alvarez Blanco Ernesto Báez
*Raul Valentino Benítez
*Leandro Cacavellos
*Roque Centurión Miranda
*Celia Elís
*José Guisone Carlos Gómez
*Mercedes Jané
*Mario Prono
*Romualdo Quiroga
*Rafael Rojas Doria
*Miguel Angel Yegros
*Olga Zubarry

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 