The Outsider (1980 film)
{{Infobox film
| name           = The Outsider
| image          = File:Outsider_1980_film.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Tony Luraschi
| producer       =  Cinematic Arts B.V.,    Philippe Modave (executive)   
| writer         = 
| screenplay     = 
| story          = 
| based on       =     
| narrator       =  Niall OBrien
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Paramount Pictures    Cinema International Corporation (Great Britain) http://www.tcd.ie/irishfilm/print.php?search=mainname&q=songs&exactMatch=1&extraSearch= 
| released       = 1979   -  or The Outsider, The (1979, Neth, 128 min) d/sc    - Tony Luraschis Film (GB 1979, nach dem Roman von Colin Leinster)]]  or   
| runtime        = 128 minutes   
| country        = 
| language       = English
| budget         = 
| gross          = 
}}

The Outsider is a 1980 film thriller set largely in Belfast during The Troubles; it was the first film directed by Italian-American Tony Luraschi. The film is based on the book The Heritage of Michael Flaherty by Colin Leinster, and details the fictional experience of an idealistic Irish-American who travels to Ireland and joins the Irish Republican Army in the 1970s.

Luraschi, who had worked as an assistant director with Stanley Kramer and Roger Vadim, had never been to Ireland until 1976.    
The company was unable to film in Northern Ireland, so instead made arrangement with a local residents association to film the exterior scenes in the Dublin suburb of Ringsend.       

==Response==

===Reviews===
*New Yorker: ""the film caused a minor scandal in London recently, where government officials were outraged at a scene that showed a British officer participating in the torture of an Irish prisoner."" 

*"his skill at realistically conveying the terrible waste of the civil strife in Northern Ireland and the chilling day-to-day acceptance of violence as a way of life there. Unfortunately, the red-herring contrivances of his plot trivialize his powerful material." 

*Stepan OFetchit... At the other extreme, modern-dress movies like Tony Luraschis The Outsider.. purport to present a real, contemporary Ireland while effectively reducing it to a traffic snarl-up of faceless ideologues wielding guns, balaclavas, and gritty one-liners.   

* The Outsider (U.S.-COLOR) Thoughtful terrorism drama, starring the IRA. ... Lack of concession on the part of director-scripter Tony Luraschi to conventional thriller pacing makes the Paramount-financed production no easy moneyspinner.   


===Political reaction===
The film was dropped from 1979 London festival. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 