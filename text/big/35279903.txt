Leah Kleschna
  Lyric Theatre Helen Gahagan as Leah 
 New Theatre with Lena Ashwell in the tile role. The production by Charles Frohman| Charles Frohman’s company resulted in the threat of legal action by Mrs. Fiske who claimed she had purchased the English rights to the play.  

On December 10, 1913 the silent film Leah Kleschna premiered with Carlotta Nillson playing Leah, House Peters as Sylvaine and Hale Clarendon as Kleschna. The picture was directed by J. Searle Dawley and produced by the Famous Players Film Company.  A year or so later Nillson played Leah once again in a Daniel Frohman road production of the play. 

==Synopsis==
*Act I: Kleschnas Lodgings in Paris.
*Act II: Study in Paul Sylvaines House at St. Cloud.
*Act III: Same as Act II
*Act IIII: Same as Act I
*Act V: Lettuce field near Wiener Neustadt, Austria

Leah Kleschna is the daughter of a manipulative master jewel thief  who has raised her to follow in his footsteps. When Leah is confronted by Paul Sylvaine, the owner of the house her father had sent her to rob, she is persuaded to contemplate her life as a thief.  Eventually Leah returns the jewels she stole, abandons her father and leaves Paris to work on the country farm she was raised on. The story comes to a happy conclusion when a few years later Sylvaine reunites with Leah and a romance ensues. 

==Reception==
The New York Times,’’ December 13, 1904, wrote: The Belle of New York which endured a large measure of success, both here and in London. On this occasion Mr. McLellan has made an excursion into a more serious type of drama. But it is only fair to state that such appeal as the play made last night was in large part due to the exceptional brilliant acting of at least five of its chief protagonists. One or two of the minor roles might have been better played, but making due allowance for the usual conditions of nervous exaggeration incident to a first night, it may be said with conviction that the acting of George Arliss, John Mason, Charles Cartwright, William B. Mack and the star herself, provided an ensemble such as is rarely excelled.”    The Standard: (Source: The New York Times; May 3, 1905)  
 “It is a piece far out of the common, and holds out the promise that in Mr. McLellan a new power has arisen.” 
Daily Chronicle: (Source:The New York Times,’’ May 3, 1905)  
 ”There hitherto were seven wonders of the world. The eighth is added in the mere that the author of The Belle of New York should be also the author of Leah Kleschna, the strongest piece of sheer stagecraft we have known from any source for years.” 

==Original Cast ==
New York - London
*Leah Kleschna: Minnie Maddern Fiske *Lena Ashwell
*Kleschna (a.k.a. Monsieur Garnier): Charles Cartwright *Charles Warner
*Paul Sylvaine: John B. Mason *Leonard Boyne
*Sophie Chaponniere: Frances Welstead *Betty Callish
*Schram: William B. Mack * William Devereux
*General Berton: Edward Donnelley *J. G. Grahame
*Raoul Berton: George Arliss *Herbert Waring
*Madame Berton: Cecilia Radclyffe *Mary Barton Emily Stevens *Daisy Markham
*Valentin Favre: Etienne Girardot * Bertram Steer
*Herr Linden: Robert V. Ferguson *A. E. Drinkwater
*Anton Pfaff: Charles Terry *W. Hubert
*Johann: H. Chapman Ford *A. W. Bascomb
*Reichmann: Monroe Salisbury *Henry Williams
*Baptiste: James Morley * Reginald Walter
*Frieda: Marie Fedor *Dora Gray
*Charlotte: Mary Maddern *Mrs. Stanislaus Calhaem
  

==Source==
 

 
 
 
 
 
 