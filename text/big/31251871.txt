Sunshine Susie
  British musical Die Privatsekretärin was made, also starring Renate Müller.

It is also known under the alternative title The Office Girl. The film established Müller as a star in Britain. 

==Cast==
* Renate Müller – Susie Surster 
* Jack Hulbert – Herr Hasel 
* Owen Nares – Herr Arvray 
* Morris Harvey – Klapper 
* Sybil Grove – Secretary 
* Gladys Hamer – Maid 
* Daphne Scorer – Elsa 
* Barbara Gott – Minor role

==Reception==
The film was a big hit and was voted the best British film of 1932. 

==References==
 

==Bibliography==
* Bergfelder, Tim & Cargnelli, Christian. Destination London: German-speaking emigrés and British cinema, 1925–1950. Berghahn Books, 2008.

==External links==
* 

 
 
 
 
 
 

 