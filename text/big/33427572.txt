Ainthu Ainthu Ainthu
{{Infobox film
| name           = Ainthu Ainthu Ainthu
| image          = Ainthu Ainthu Ainthu 555.jpg
| caption        = Theatrical poster Sasi
| producer       = Chennai Cinema  (India)  Pvt.Ltd
| writer         = Sasi  (dialogue) 
| story          = Aravind-Sureshkumar
| screenplay     = Sasi
| starring       = Bharath Mrithika Erica Fernandes Simon
| cinematography = Saravanan Abimonyu
| editing        = Subarak
| studio         = Chennai Cinema  (India)  Pvt.Ltd  
| distributor    = Chennai Cinema  (India)  Pvt.Ltd
| released       = 10 August 2013 
| runtime        = 147 Minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
}} Tamil romantic Santhanam in other pivotal roles. The film, an action drama revolving around psychology and mind games, had been in production since 2008.    The film released on 10 August 2013 to positive reviews and commercial success.

==Plot==
Aravind (Bharath) had met with a terrible accident seven months ago, that left him physically and emotionally scarred.  He is currently living with his elder brother Gopal (Santhanam (actor)|Santhanam) and being treated for some psychotic disorder.  Aravind claims that during the accident, his girlfriend Liyana (Mrithika) was with him and she died in his arms. The strange thing is that such a girl does not seem to exist at all.  No one has seen or even heard of her; she does not appear in the student list in her college or even the census reports. She is not mentioned in the newspaper reports of the accident either and some strangers are occupying her home. The psychiatrist is of the opinion that Liyana is a figment of Aravind’s imagination and exists only in his mind. Unable to convince even Gopal of her existence, Aravind tries to get on with his life by going back to work. Here he meets Manjari (Erica Fernandes), a colleague (in a software company Paayal Infotech which is owned by CEO Sudesh Berry), who sympathises with him and also tries to help find some clue to Liyana’s identity. Despite all evidence against Liyana’s existence, memories of her and their love continue to haunt Aravind. Fortunately for him, a chance meeting with Liyana’s aunt sets the ball rolling (Aravind obtains all the information from Liyanas aunty who is not really her aunty but instead pretended to be, she gets killed by a goon who is killed by in a few seconds). After that things begin to unravel, Aravind finds out that he is being tortured by his own company boss (Sudesh). Reason for doing this is that Liyana happens to be splitting image of Sudeshs own dead lover called Payal so inorder to marry her, Sudesh made a plan to torture Aravind by involving Manjari in this game, Gopal gets killed in this mayhem. Aravind shoots Manjari and henchmen. Sudesh narrates that doctor was a fake person set up by him to fool Aravind and also threatened his brother Gopal to pretend ignorance about Liyana. Doctor tortures Aravind with a planned shock treatment to make him go insane but he is completely healthy and kills the doctor. He kills Sudesh and henchmen and unites with Liyana who is alive. 

==Cast== Bharath as Aravind
* Mrithika as Liyana and Paayal
* Erica Fernandes as Manjari Santhanam as Gopal
* Sudesh Berry as Chitranjan
* John Vijay
* Manobala as Yoga Instructor
* Rajbharath as Nikhil
* T. V. Rathnavelu as Doctor
* Lakshmi as Liyanas aunty
* Swaminathan
* Sathish Krishnan

==Production==
After the moderate success of his romantic film, Poo (film)|Poo, Sasi began work on his next, an action entertainer, in late 2008 and worked on the script of the film for almost two years.  Bharath was signed on to play the lead role in mid-2010 and in interviews since signing the project, he has expressed how important the film will be to his career.  The shoot of the film began in June 2011.  Bharath is sporting six-pack for the film. 

==Soundtrack==
{{Infobox album
| Italic title  = 
| Name          = Ainthu Ainthu Ainthu
| Type          = soundtrack
| Longtype      = to Ainthu Ainthu Ainthu Simon
| Cover         = Ainthu Ainthu Ainthu Audio Cover.jpg
| Border        = 
| Alt           = 
| Caption       = Front Cover
| Released      = 15 April 2013
| Recorded      = 2012 Feature film soundtrack
| Length        =  Tamil
| Label         = Sony Music Simon
| Chronology    = 
| Last album    =
| This album    =
| Next album    = 
}}
 Shankar and was received by actor  Dhanush. The launch of the films album was held on April 15, 2013.

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Vizhiyile Vizhiyile
| lyrics1         = Na. Muthukumar
| extra1          = Haricharan, Chinmayi, Blaaze
| length1         = 
| title2          = Mudhal Mazhai Kaalam
| lyrics2         = Na. Muthukumar
| extra2          = Deepak Doddera, Preethika
| length2         = 
| title3          = Rowdy Girls
| lyrics3         = Na. Muthukumar
| extra3          = Preethika, Sheeba Truman
| length3         = 
| title4          = Kadhal Indha kadhal
| lyrics4         = Na. Muthukumar
| extra4          = Sathya Prakash, Kalyani
| length4         = 
| title5          = Elavu Simon
| Simon
| length5         = 
| title6          = Saregama
| lyrics6         = BSK
| extra6          = Sathya Prakash
| length6         = 
| title7          = Ghanni Khamma
| lyrics7         = Noel Rodrigues, Heena D.
| extra7          = Meghana Dandekar
| length7         = 
}}

==Release==
The satellite rights of the film were secured by Zee Tamil. The film was given a "U/A" certificate by the Indian Censor Board. 555 was originally scheduled to release on 15 August 2013. coinciding with Independence day, but was brought forward to 10 August. 

==Reception==
555 opened to positive reviews from critics. Behindwoods said, "Sasi is a master teller when it comes to narrate a story about love and its finer aspects and there would always be a kind of poignancy in them. 555 is no exception. In Arvind Sureshkumars story, Sasi has also used a lot of action and suspense to drive home his point. There are adequate twists and turns in the narration to make the audience engrossed in the enterprise." 

A review from Sify gave 4 stars out of 5 and said "Director Sasi, who has made few realistic films in the past, has made a hardcore action entertainer with romance and suspense, a new genre for him. Though his intentions are noble and Bharath has put in a lot of effort, the film is far from being watchable and fails to impress." 

Prashanth Reddy of Desimartini said, "A few minutes into the film, I smugly told myself how I expected it to end; my predictions couldnt have been more wrong. Thats the one thing about Ainthu Ainthu Ainthu that I love: it always kept me puzzled about the happenings and continuously piled mounds of absurd as it moved forward." and added, "The kind of epic back story and sentimentality it lends its antagonist is sheer rip-roaring awesomeness. The non-linear screenplay is a huge plus in making the film interesting." 

Indiaglitz.com said, "Bharath has put his heart and soul in this movie, making it worth the wait. His transformation from an urban cool dude to a steel gritted toned angry hunk is amazing, even though the justification for his clenched body is never shown in a convincing manner. All said, this might be the movie that could bring back him to the limelight again. The rest of the cast are adequate enough to fill their roles neatly." 

M Suganth of The Times of India gave 3/5 and said, "Sasi, who has so far made soft films like Sollamale and Poo, tries his hand at a commercial thriller with action, suspense, and of course, the inevitable romance. While the directors intent in giving us a fresh and different commercial film is evident, sadly, he doesnt manage to sidestep the genres pitfalls." 

S Saraswathi of Rediff said Ainthu Ainthu Ainthu is a romantic thriller that has all the necessary twists and turns which will definitely entertain the audience. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 