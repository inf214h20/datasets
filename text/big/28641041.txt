Children of the Stars
{{Infobox film
| name           = Children of the Stars
| image          =  Childrenofthestars2.JPG
| image_size     =  
| alt            =
| caption        =  Feng Jia Wei (far left) and a friend  at the Beijing Stars and Rain School. 
| director       =  Rob Aspey
| producer       =   Alexander Haase
| writer         =
| narrator       =
| starring       =
| music          =  
| cinematography =  
| editing        =  
| studio         =  
| distributor    =
| runtime        = 49 minutes
| country        = Peoples Republic of China English subtitles
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Children of the Stars is a 2007 documentary about children with autism ( ) in the Peoples Republic of China. It was produced by Alexander Haase and  directed by Rob Aspey. The title is an English translation of the expression   ( ), a phrase used in Taiwan to describe autistic children. Beijing Stars and Rain (2007).  . Accessed 9 September 2010. 

The documentary focuses on the Beijing Stars and Rain Teaching Institute (北京星星雨教育研究所), founded in 1993 by Tian Huiping. In 1992 Tian traveled  from   in China dedicated to serving children with autism. Field, Brian (2010).  . Autism Support Network. Accessed 9 September 2010.  It offers an 11-week educational program for families, based on applied behavior analysis.

The documentary follows the progress of five-year-old Feng Jia Wei and his father and mother, Feng Lei and Hao Yue Chun, who traveled across China to enroll their son in the school. The parents faced hardship in their community—a complete lack of schools or facilities to deal with autistic children, no network of governmental aid or funds, ostracism from neighbors, and loss of their jobs.  . Official website of "Children of the Stars". Accessed 9 September 2010.  Before coming to this school they had considered committing suicide as a family —much as Tian Huiping, the schools founder, had years before. Vause, John. (March 31, 2008).  . CNN. Accessed 9 September 2010.  Public health services for autistic children in China are almost nonexistent, as Tian Huiping explains:
 Parents of autistic children cant expect any help from society. Its not just a question of financial help; they cannot get specialized help either... More   98 percent of autistic children have to stay at home because they are rejected by kindergartens even if they are very high-functional &ndash; autistic children have very poor social skills. Attending school is a dream.  

At the time of their arrival, Jia Wei is violent and uncooperative. He cannot speak, and shows little recognition of his parents, whom he frequently bites and hits: 
 Families had been waiting for 2 years to attend and often arrived in a state of despair. They could not communicate with their children or understand the symptoms of autism at all. They were looking for a cure and thought that this school was where miracles happened. Asprey, Rob. (2009).  . Website of the Sprout Film Festival. Accessed 9 September 2010. 
 

The program requires weeks of tireless work. As producer  Alender Haase describes the early days watching Jia Weis family struggle: "Sometimes I said to myself: Well, thats enough; I cant stand it anymore. We wanted to drop the camera and leave. The parents seemed to be on the edge of mental collapse. But the strong power of love and patience pulled them back, and so it did to us."  Qi, Lin. (2007).   . Accessed 9 September 2010.  In time, Jia Weis family begins to see a number of break-through experiences, and their first opportunity to live in a nurturing setting offers them hope. However, they are apprehensive about returning to their unfriendly home town, and fear that Jia Weis obvious progress will not be enough to let him enter a public school. 

Children of the Stars is described as a "riveting documentary" on Library Journals website: 
   parents candidly share their difficulty in a society that doesnt understand autism. Their desperation is evident as they coax and cajole Jia Wei through school activities intended to help him to communicate. Then we witness the moment when Jia Wei calls out "daddy" for the first time. These connections with the parents make this heartwarming movie wonderful to watch." Library Journal. (2009, April 15).   (Reviews). Accessed 9 September 2010.  

==Awards==
;2008
:* "Chris Award" at the Columbus International Film Festival in Columbus, Ohio, USA
:* "Documentaries up to 60 minutes"  award in the New Delhi We Care International Documentary Festival, New Delhi, India.
;2010
:*"Excellence Award" at the 2010 Superfest International Disability Film Festival in Berkeley, CA.

==Notes==
 

==External links==
*  , official website of film
**  
* 
*   at autismchina.org
*  . (Video) AFP Global News
* Johnson, Ian (2008).  . Wall Street Journal.
*  
*  
 
 

 
 
 
 
 
 
 