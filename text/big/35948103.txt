Gaz Bar Blues
{{Infobox film
| name           = Gaz Bar Blues
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Louis Bélanger
| producer       = 
| writer         =
| screenplay     = Louis Bélanger
| narrator       = 
| starring       = Serge Thériault
| music          = Guy Bélanger Claude Fradette
| cinematography = 
| scenography    = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2003
| runtime        = 115 minutes
| country        = Quebec (Canada)
| language       = French
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}{{external media
| image1 =  }}
 2003 Quebec|Québécois drama and comedy film directed by Louis Bélanger. The film is set in 1989 in the outskirts of an unspecified Québécois city.

==Plot== gas station. He manages his relationships with his sons Réjean, Guy, the 13-year-old Alain, with which manages the station; and his daughter Nathalie (Fanny Mallette). The Gas Bar is also regularly attended by his friends as Gaston Savard (Gilles Renaud), Jos, Normand Party, Yves Michaud, Claude, Nelson and Ti-Pit.
 the wall. Daniel Brière), inspector of the company Champlain that owns the station.
 East Germans, as well as the new self-services will replace filling stations.

Arrested in East Berlin while attempting to rebuild the Berlin Wall (a symbolic gesture), and repatriated to Canada, Réjean starts again working. One day he faces the inspector Gobeil, telling him hes a madmen and threatening him if he continues to torment his father. During the nth robbery, Alain is taken hostage and then exchanged with his father. Mr. Brochu, saved by police, begins to reflect on the fact that his desire to keep the family together with the work in the gas station is dismembering it. So he decides to start therapy against Parkinson in the hospital and to attend a concert of his son Guy in a pub. One day he decides to close the activity of the gas station and retires to be closer to his 4 kids. The day after Jos and Ti-Pit read the "End of Business" message of "The Boss": first perplexed, immediately go in search of the other friends of the group, sure to find them at the convenience store.

==Cast==
 
* Serge Thériault as François Brochu
* Gilles Renaud as Gaston Savard
* Sébastien Delorme as Réjean Brochu
* Danny Gilmore as Guy Brochu
* Maxime Dumontier as Alain Brochu
* Fanny Mallette as Nathalie Brochu
* Gaston Caron as Jos
* Gaston Lepage as Normand Patry
* Daniel Gadouas as Yves Michaud
 
* Roger Léger as Claude
* Claude Legault as Ti-Pit
* Réal Bossé as Nelson Yves Bélanger as Coyote
* Vincent Bilodeau as Mononc Boivin
* Daniel Rousse as Dan
* Marc Beaupré as Yoyo Daniel Brière as Inspector Gobeil
* Emmanuel Bilodeau as Jocelyn
 

==Awards==
*2003 Special Grand Prix of the Jury of the Montreal World Film Festival 
*2004    Best Music to Guy Bélanger and Claude Fradette 

==See also==
 
*List of Canadian films of 2003

==References==
 

==External links==
*  
*  

 
 
 
 
 
 