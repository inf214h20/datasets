Inquest (1931 German film)
{{Infobox film
| name = Inquest 
| image =
| caption =
| director = Robert Siodmak
| producer = Erich Pommer  
| writer =  Max Alsberg (play)   Ernst Hesse (play)   Robert Liebmann   Hans Müller   Robert Siodmak
| narrator =
| starring = Albert Bassermann   Gustav Fröhlich   Hans Brausewetter
| music =  
| cinematography = Otto Baecker   Konstantin Irmen-Tschet  
| editing =  Viktor Gertler       UFA 
| distributor = UFA
| released =  
| runtime = 95 minutes
| country = Germany  German 
| budget =
| gross =
}} Paul Martin, who soon after emerged as a leading director, was assistant director to Siodmak on the film. It was based on a 1927 play of the same title by Max Alsberg and Ernst Hesse.

==Synopsis==
When a prostitute is murdered in a cheap Berlin boarding house, an investigating judge suspects that the killer is her boyfriend, unaware that his own son and daughter are also mixed up in the case.

==Cast==
* Albert Bassermann as Dr. Konrad Bienert, Landgerichtsrat  
* Gustav Fröhlich as Fritz Bernt, Student  
* Hans Brausewetter as Walter Bienert, Beinerts Sohn, Student  
* Charlotte Ander as Gerda Bienert - Bienerts Tochter  
* Anni Markart as Erna Kabisch  
* Edith Meinhard as Mella Ziehr  
* Oskar Sima as Karl Zülke, Portier  
* Julius Falkenstein as Anatol Scherr, ein Hausbewohner 
* Heinrich Gretler as Kurt Brann, sein Untermieter  
* Hermann Speelmans as Bruno Klatte, Artist  
* Jakob Tiedtke as Ein genierter Herr  
* Gerhard Bienert as Baumann, Kriminalkommissar  
* Heinz Berghaus as Schneider, Kriminalbeamter  
* Carl Lambertin as Kriebel, Kiminalbeamter 
* Emilia Unda
* Erwin Splettstößer

== References ==
 

== Bibliography ==
* Spicer, Andrew. Historical Dictionary of Film Noir. Scarecrow Press, 2010.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 