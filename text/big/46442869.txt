Ideal Woman Sought
{{Infobox film
| name = Ideal Woman Sought
| image = 
| image_size =
| caption =
| director = Franz Antel
| producer = 
| writer = Franz Beron (play)   Jutta Bornemann   Gunther Philipp   Franz Antel 
| narrator =
| starring = Inge Egger   Jeanette Schultze   Waltraut Haas Werner Müller 
| cinematography = Hans Heinz Theyer    
| editing = 
| studio =  Cziffra-Film   Schönbrunn-Film 
| distributor = Constantin Film
| released =  30 September 1952 
| runtime = 96 minutes
| country = Austria   German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Ideal Woman Sought (German:Ideale Frau gesucht) is a 1952 Austrian musical film directed by Franz Antel and starring Inge Egger, Jeanette Schultze and Waltraut Haas. 

==Cast==
*  Inge Egger as Irene Mertens  
* Jeanette Schultze as Ruth  
* Waltraut Haas as Luise  
* Susi Nicoletti as Chérie  
* Wolf Albach-Retty as Robby Holm  
* Gunther Philipp as Stefan Blitz  
* Oskar Sima as Bierhaus  
* Rudolf Carl as Krappl  
* Cornelia Froboess as Cornelia, Sängerin  
* Fritz von Friedl as Peter  
* Ilse Peternell as Isolde  
* Jutta Bornemann as Frl. Wurm  
* Hilde Jaeger as Frl. Aufrecht 
* Peter Preses as Dir. Maier  
* Raoul Retzer as Gigantino  
* Hellmuth Schönemaker  Otto Stuppacher
* Theodor Grieg 
* Rita Paul as Singer

== References ==
 

== Bibliography ==
* Robert Dassanowsky. Austrian Cinema: A History. McFarland, 2005.

== External links ==
*  

 
 
 
 
 
 

 