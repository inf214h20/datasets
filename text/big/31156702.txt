Thandavam
 
{{Infobox film
| name           =Thandavam
| image          = 
| caption        =
| director       =Shaji Kailas
| producer       =Johnny Sagarika
| writer         =Suresh Babu Saikumar  Nedumudi Venu  Manoj K. Jayan Jagadish
| music          =Perumbavoor G. Raveendranath  M G Sreekumar
| cinematography = 
| editing        =L.Bhoominathan
| distributor    =Johnny Sagarika
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Thandavam is a Malayalam action  film released in 2002. Directed by Shaji Kailas, this film had a huge star cast including Mohanlal, Kiran Rathod, Nedumudi Venu, Captain Raju, Saikumar (Malayalam actor)|Saikumar, Manoj K. Jayan and Jagadish. Though, Thandavam had one of the best initial pull in the history of Malayalam cinema, it failed to satisfy even the hardcore Mohanlal fans.

==Plot==
The story centers on Kasinathan (Mohanlal), a pragmatic but playful businessman keen on general welfare of his men also running a resort and massage parlour as a side business. Kasinathan is the younger brother of Swaminathan (Nedumudi Venu), the heir to Valiyamangalam Malika and the head of erstwhile feudal Midhilapuri. Midhilapuri is an idyllic village prospered through judicious use of agriculture as the path to good life. Kasinathan is powerful in his own right and is tipped to succeed influential politician Menon (Janardhanan (actor)|Janardhanan) in Kerala politics. Kasinathan has a secret admirer in the form of a mysterious girl (Kiran Rathod) who drops messages and love hints everyday. But Kasinathan has still better issues to worry about! The burning issue of providing drinking water supply to Udayankara colony is time and again sabotaged by Cherpunkal Shankar Das (Saikumar (Malayalam actor)|Saikumar), a wily and unscrupulous politician of crassiest morals who is intent on teaching a lesson to the alleged loyalty of the people who continue to support righteous politicians. The reciprocal justice is carried out by Kasinathan who in a strategic move corners Shankar Das in a compromising position with a serial actress (Maya Viswanath)and blackmails him to release permission for water supply in the village. Shankar Das is forced to resign from the cabinet. Ensuing celebration is amply enjoyed in a rain dance song by Kasinathan with all and sundry of his devoted satellites including characters Murugan (Jagadish),Vellapulli Mathachan(Jagathy Sreekumar), Tomi (Vijayakumar), Basheer (Salim Kumar) Pushpakumaran (Maniyanpilla Raju) etc.  Kasinathans secret lover turns out to be Meenakshi the only sister of his good friend DYSP Rajeevan (Captain Raju)

The upcoming business man Dasappan Gounder (Manoj K. Jayan) and his henchmen smells an opportunity to market Cola drinks in Midhilapuri and approaches Swaminathan to start a mutually profitable business deal. Swaminathan in his idealist moral sense is staunchly against the endeavour. Disappointed Gounder joins force with Shankar Das and craftily plots death of Swaminathan in a planned operation. The disappearance of Swaminthan moderates Kasinathan to an extent. He decides to don the garb of saviour of Midhilapauri and returns to his Valiyamangalam Malika. In a chance encounter, Kasinathan learns of the cause of death of his elder brother as planned murder perpetrated by Gounder, DYSP and Shankar Das. This unleashes the beast within him and true to his name, Kasinathan trounces on the hapless Gounder and Shankar Das to a vegetative life death end in the climax. Dasappan Gounder is lured to a barn and is thrashed to senselessness by Kasinathan. Meanwhile, Shankar Das is on a self-imposed pilgrimage to gather strength for a showdown with Kasinathan. He is eventually caught in the middle of the Kayakalpa treatment at an ashram and goes mad in the end, a just retribution to a life full of sins and debauched deeds.

After sorting out the evil issues, Kasinathan is still troubled by the “gap” and decides to solve this issue once and for all by marrying his sweet heart.

==Cast==
* Mohanlal as Kasinathan
* Nedumudi Venu as Swaminathan (Elder brother of Kasinathan)
* Kiran Rathod as Meenakshi Saikumar as Cherpunkal Shankar Das Janardhanan as Menon
* Vineetha
* Manoj K. Jayan as Dasappan Gounder
* Jagadish as Murugan
* Jagathy Sreekumar as Vellapulli Mathachan
* Vijayakumar as Tomy Bhavani
* Salim Kumar as Basheer
* T. P. Madhavan as Warrier
* Sadiq
* Maniyanpilla Raju as Pushpakumaran
* Captain Raju as DySP/Elder brother of Meenakshi and friend of Kasinathan
* Krishna and Arun (children of Nedumudi venu)

==References==
 
 

 
 
 