Parudeesa
{{Infobox film
| title          = Parudeesa
| image          = Parudeesa poster.jpg
| caption        = Theatrical poster
| director       = R. Sarath
| producer       = Thampi Antony
| writer         = Vinu Abraham Sreenivasan Thampy Antony Swetha Menon
| music          = Ouseppachan  (Songs)  Issac Thomas Kottukapally  (Background score) 
| cinematography = Sajan Kalathil
| editing        = Ajith
| studio         = Kayal Films
| distributor    = Remya Movies
| runtime        = 
| released       =  
| language       = Malayalam
| budget         = 
| gross          = 
| country        = India
}} national award-winning filmmaker R. Sarath. The film stars Sreenivasan (actor)|Sreenivasan, Thampy Antony and Swetha Menon in the lead roles. Like in his previous films, Sarath interweaves a complex theme in Parudeesa—that of conflict of belief, the perpetual disagreement between orthodox and unorthodox paths of religion.    The film, set against the backdrop of a remote hillside hamlet, unravels the story of a priest and a verger. While the priest is still lost in the labyrinth of orthodoxy, verger has a very liberated outlook about religion, or rather life in general. The film captures the quintessential conflict flickered by the situation. Sreenivasan plays the priest while Thampy Antony appears as the verger and Swetha Menon plays the role of a cook at the nearby convent. Noted comedian and actor Jagathy Sreekumar plays another full-length character, Parudeesa being the last film he completed before the accident. The film released on 26 October 2012 and met with a mixed reaction. Reviews among major critics and parallel movie buffs have been positive but most online critics of mainstream cinema have given the film negative reviews.

The film became controversial for inciting the clergymen in the film. In reply to the controversies created, Sarath said: "It’s a baseless allegation that the film instigates an anti-religious propaganda. In fact Parudeesa is a film that glorifies Christ. It makes a jibe at the situation where religion supersedes everything else, even god. From the progressive ideologies of 60s and 70s we are going back to the perils of theocracy. The film attacks superstitions and regression from a pointblank position." Navamy Sudhish (28 October 2012).  . The New Indian Express. Retrieved 12 November 2012.  Vinu Abraham Vinu Abraham says in a sense Parudeesa attempts to define faith. "We are trying to tell that despite all its external rigidity, religion, at its core, should be a progressive forum," says Vinu. 
 national award-winning musicians composed music for the film. The films background score is by Issac Thomas Kottukapally while the songs are composed by Ouseppachan. Thampi Antony who is acting in as well as producing the film pens a song in the film. The song starting with the lines, "Yathra Chodikkunnu", is sung by Vijay Yesudas. 

==Plot==
Parudeesa tells the story of a Catholic parish in a hillside village in Kerala. Set in two milieus – Kerala of the sixties and seventies and that of the eighties and nineties – the film unfolds through the conflict of ideals between Father Aanjalithanam (Sreenivasan (actor)|Sreenivasan), an orthodox priest, who is the last word in the parish, and a firebrand verger named Deacon Jose (Thampy Antony), who believes that Christ was the original revolutionary. Both of them believe that their way to God is the true path, the only path.

As he sets out to bring a change in society, increasingly Jose finds himself facing opposition at every turn, and thats not only from Father Aanjalithanam but from within the parish and the extended village community too. The catalyst for the conflict is feisty Theresia (Swetha Menon), an umarried maid who works in the kitchen of a nearby convent. Jagathy Sreekumar also has an important role in the film, as the powerful secretary of the church committee. 

==Cast== Sreenivasan as Father Aanjalithanam
* Thampy Antony as Deacon Jose 
* Swetha Menon as Theresia
* Jagathy Sreekumar as Authachan
*Nandhu as the rubber tapper
* Indrans
* Ambika Mohan as Authachans wife

==Reception==
The film received mixed reviews upon release. Reviews among major critics and parallel movie buffs have been positive but most online critics of mainstream cinema have given the film negative reviews.All said and done, the paradise still remains an enigma, and salvation appears as unfathomable as it has always been."  The critic of Sify.com rated the film "Below Average" and wrote, "There is a decent storyline here but the problem is the way it has been narrated. . The usual cliches of the offbeat films are there and the efforts to commercialise the presentation have ended up as a rather half-baked one." The reviewer also criticised Thampi Antonys performance stating that the actor is "evidently struggling to fit in to the role". 

==References==
 

 
 
 
 
 
 