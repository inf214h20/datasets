The Eternal Sea
{{Infobox film
| name = The Eternal Sea
| image =
| image_size =
| caption =
| director = John H. Auer
| producer = John H. Auer
 | writer = Allen Rivkin   William Wister Haines   Luther Davis
| narrator =
| starring = Sterling Hayden   Alexis Smith   Ben Cooper   Dean Jagger
| music = Elmer Bernstein John L. Russell Fred Allen    
| studio = Republic Pictures
| distributor = Republic Pictures
| released =  May 5, 1955 
| runtime = 103 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Eternal Sea is a 1955 American war film directed by John H. Auer and starring Sterling Hayden, Alexis Smith and Ben Cooper. After an American naval officer loses his leg at the Battle of Leyte Gulf, he resists attempts to retire him and continues in the service after learning to cope with his disability. He goes on be promoted to admiral and commands an aircraft carrier during the Korean War. 

The film was one of a number of more ambitious productions by Republic Pictures, which had traditionally made low-budget second features. Its release was undermined by the studios growing financial problems which led to its eventual closure in 1959. 

==Cast==
* Sterling Hayden as Rear-Adm. John Madison Hoskins  
* Alexis Smith as Sue Hoskins  
* Ben Cooper as Seaman P.J. Zuggy Zugbaum  
* Dean Jagger as Vice-Adm. Thomas L. Semple 
* Virginia Grey as Dorothy Buracker  
* Hayden Rorke as Capt. William Buracker   Douglas Kennedy as Capt. Walter Riley 
* Louis Jean Heydt as Capt. Walter F. Rodee  Richard Crane as Lt. Johnson  
* Morris Ankrum as Vice-Adm. Arthur Dewey Struble  
* Frank Ferguson as Admiral L.D.   John Maxwell as Adm. William F. Bull Halsey 
* William Kerwin as Cole

==References==
 

==Bibliography==
* Norden, Martin F. The Cinema of Isolation: A History of Physical Disability in the Movies. Rutgers University Press, 1994.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 