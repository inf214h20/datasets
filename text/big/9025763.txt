Supervising Women Workers
  US Office of Education and aimed at male foremen who now had to supervise women for war work.

The film depicts the social and gender relations and attitudes of its time. It notes that most of the women had never been in industry before, and were unfamiliar with the terminology and mores common to the plant. Each thing had to be broken down and explained in detail. The film also reminds men that women of the day have been at work in things like knitting and sewing, and that these skills could be appropriated for war work. In a short vignette the foreman returns home to his wife, complaining about all the women asking for time off. His wife then tells him she had to cook, clean, and take care of the children all day, at which point the foreman realizes that women really work two jobs, one in the factory and the other at home.

In another vignette a foremen gets into an argument about a woman not wearing a protective hat, and it is shown that he has to explain to her why the protective cap was necessary. Finally, there are four basic rules to supervising women workers:
*dont mix business with pleasure
*remember that women are awfully jealous of each other
*avoid undue familiarity
*women are more sensitive than men.

== See also ==
List of Allied propaganda films of World War II

== External links ==
* 

 
 
 


 