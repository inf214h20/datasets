Xtro 3: Watch the Skies
{{Infobox film
| name           = Xtro 3: Watch the Skies
| image          = Xtro3DVD.jpg
| image size     =
| caption        = Image Entertainment DVD cover
| director       = Harry Bromley Davenport
| producer       = Harry Bromley Davenport Jamie Beardsley
| writer         = Daryl Haney
| narrator       =
| starring       = Robert Culp Andrew Divoff Daryl Haney Jim Hanks
| music          = Harry Bromley Davenport
| cinematography =
| editing        =
| distributor    = Image Entertainment (DVD)  (USA) 
| released       =   November 14, 1995
| runtime        = 90 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} horror Xtro series.

==Plot==
For years, the government has successfully covered up very real proof of U.F.O.s. but when a group of Marines is dispatched to a deserted island, they uncover unsettling evidence: old films documenting brutal medical experiments on aliens. They also uncover a lone surviving alien out for revenge, and a military intelligence plot to sacrifice them and conceal existence of the encounter. The shroud of mystery is about to be lifted.

==DVD release==

The film has been released on DVD twice. The first DVD was released in 1999 by Image Entertainment, who coincidentally, would release the first two entries in the Xtro series on DVD years later. In 2005, the film was released yet again on DVD by Showcase Entertainment. Both DVDs are now discontinued.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 