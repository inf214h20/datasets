Fading Wave
{{Infobox film
| image    = 
| caption  = 
| name     = Fading Wave traditional = 老炮兒
|                          simplified = 老炮儿
|                              pinyin = Lǎopàoér}}
| director = Guan Hu
| producer = Wang Zhonglei
| writer   = Dong Runnian
| starring = Feng Xiaogang Kris Wu Zhang Hanyu Li Yifeng Xu Qing
| music    = 
| cinematography = 
| editing  = 
| studio   = Huayi Brothers
| distributor = 
| released =  
| runtime = ? minutes
| country = China
| language = Mandarin
| budget = 
| gross = 
}} action comedy film film directed by Guan Hu and written by Dong Runnian. It star Feng Xiaogang, Kris Wu, Zhang Hanyu, Li Yifeng, and Xu Qing.  Fading Wave is scheduled for release on December 24, 2015 in China. 

==Plot==
Fading Wave tells of an old 50-something-year-old street punk called “Lao Pao Er” who has reigned over the Beijing streets as the neighbourhood kingpin for many years. One day, when his mischievous son causes a dispute against a much younger drag-racing street punk, he steps up to help defend him. “Lao Pao Er” then discovers that the younger generation of gang members have already named this drag racer as his successor, and he no longer holds any ground in the street world.   

==Cast==
*Feng Xiaogang as Liu Ye (Lao Pao Er)
*Li Yifeng as Xiao Bo (Lao Pao Ers son)
*Kris Wu as Xiao Fei (Er Huan Shi San Lang)
*Zhang Hanyu as Men San Er
*Xu Qing as Hua Xia Zi
*Liu Hua as Deng Zhao Er
*Liang Jing as Deng Zhao Ers wife

==Production==
Filming for Lao Pao Er kicked off late November 2014  in Beijing    and ended on February 2015. On December 12, 2014, Feng Xiaogang, Liu Hua, Zhang Hanyu, Wang Zhonglei, Guan Hu, Xu Qing, Liang Jing, Li Yifeng, and Kris Wu attended the opening ceremony.   

==References==
 

 

 
 
 
 
 
 
 


 