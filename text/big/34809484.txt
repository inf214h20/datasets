Adimaippenn
 
 
{{Infobox film
| name           = ADIMAI PENN
| image          = Adimai Penn.jpg
| image_size     =
| caption        =
| director       = K. Shankar
| producer       = M. G. Ramachandran
| writer         = Swornam (dialogues)
| story          = R. M. Veerapan S. Lakshmanan S. K. T. Samy
| narrator       =
| starring       = M. G. Ramachandran J. Jayalalitha S. A. Ashokan Pandari Bai Rajasree R. S. Manohar J. P. Chandrababu Cho Ramaswamy
| music          = K. V. Mahadevan
| cinematography = V. Ramamoorthy
| editing        = K. Narayanan
| studio         = Em. Gee. Yar Pictures (P) Ltd.
| released       = 1 May 1969
| runtime        =
| country        = India Tamil
| gross          =   2.3 Crores
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Tamil Tamil sword-and-sandal film directed by K. Shankar.

The film features M. G. Ramachandran as the main character, with J. Jayalalitha, S. A. Ashokan, Pandari Bai, Rajasree, R. S. Manohar, J. P. Chandrababu and Cho Ramaswamy in supporting roles.

==Plot==

Abhirami Mangamma (Pandari Bai), a queen is desired by another ruler, Sengodan (S. A. Ashokan). Several years later, Sengodan sees the girl (now a queen) out hunting. He declares his love, but the queen says that she is a mother. Sengodan tries to kill her son (MGR), the Prince Vengaiya and the queen cuts off Sengodans leg with an axe.

The king Vengaiyan (MGR) from the Vengaiya Mount Kingdom goes to Sengodans country (Soorukathu Kingdom) seeking justice, and Sengodan agrees to a duel. The duel takes place over a net with spears below it; whoever falls on the net will die. A dueler will lose if he loses his weapon or falls from the net, and his country will be enslaved by the winner.

Since Sengodan has only one leg, the king Vengaiyan binds his own leg and they begin the duel. Although the king Vengaiyan wins, Sengodan kills him with a spear. Sengodan orders his men to seize the country and summon the queen and her son. One of the kings aides escapes and saves the queen, but the prince Vengaiya is taken prisoner. All women in the country are enslaved.

The queen stays in hiding for many years. The kings aide is imprisoned and sees the prince Vengaiya, who has been forced to live in a two-foot-high cell. The prince Vengaiya has forgotten how to talk or eat with his hands, and the aide is horrified by his condition. They escape from the prison by the river. The aide dies in his granddaughter’s arms after she promises to heal the prince and help abolish slavery in their country. The granddaughter, Jeeva (Jayalalithaa), takes the prince to her hut. Although he behaves badly, Jeeva teaches him to speak, write and fight. Vengaiya begins to understand that he is a prince, but is a hunchback because of his confinement.

The prince Vengaiya saves a girl (Jothilakshmi) from two warriors. When he is helping the girl (who has been bound between two heavy wooden planks, like a pillory), his spine straightens and he can stand normally. Jeeva tells the prince what he must do, and shows him his mother. When he sees his mother’s condition, the prince Vengaiya vows that he will release the country from Sengodans enslavement. The prince, with help from Jeeva and others, attacks a group of soldiers and begins freeing people from slavery. During one assault, he meets Magudhapadhy, the leader (R. S. Manohar) of a neighboring nation (Pavala Kingdom) who is related to Sengodan. The commander Magudhapadhy  is astonished to see Jeeva because of her resemblance to his queen, Pavala Vaalee (Jayalalithaa). He conspires to replace the queen with Jeeva and take over the country, which separates the prince Vengaiya  and Jeeva from their followers. The commander, claiming the prince is a spy, hides Jeeva.

The queen Pavala Vaalee (Jeevas sister) presides over the princes trial. She is attracted to him, and orders that he be released as her bodyguard. The commander plans to kill both the queen and the prince at a party with a poisoned drink, but it is moved by one of the princes aides (a magician) (Cho Ramaswamy).

The commander orders the arrest of the prince and the queen. Jeeva impersonates the queen so she and the prince can be freed and allowed to return to their country. The queen Pavala Vaalee is dressed in Jeevas clothes and kept in captivity, to be killed later. The commander goes to the prison and admits his plan; the prince kills him and escapes with the queen, thinking she is Jeeva.

The prince Vengaiya finally reaches his country, which has changed during his long absence. His house has been gutted by fire, his farms plundered and his men oppose him. The prince tells them his story, coercing them to rejoin the army. The queen Pavala Vaalee joins the Sengodan side, awaiting revenge.

Abhirami Mangamma, the princes mother is captured by Sengodans men, who threaten to execute her. The prince and his men sneak into the palace and fight Sengodan; he kills Sengodan, releases his mother, frees his kingdom and the film ends happily.

==Cast==
{| class="wikitable" width="100%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as The king Vengaiyan from the Vengaiya Mount Kingdom (Vengaiya Malaiyan) and his son, the Prince Vengaiya  
|-
| J. Jayalalitha || as Jeeva, the right-hand of the Prince Vengaiya and the Queen Pavala Vaalee from Pavala Kingdom
|-
| S. A. Ashokan || as The king Sengodan from Soorukathu Kingdom
|-
| R. S. Manohar|| as The royal bodyguard Magudhapadhy (from Pavala Kingdom)
|-
| J. P. Chandrababu || as The doctor of campaign, the Prince Vengaiya s friend
|-
| Cho Ramaswamy || as The royal bodyguard Magudhapadhy s magician  
|-
| Jothilakshmi || as Azhagu s elder sister, the woman in the pillory (stocks) 
|-
| Rajasree || as The princess Muthazhagi, (the king Sengodanqueen s foster daughter) 
|-
| Pandari Bai || as The Queen Abhirami Mangamma, the Prince Vengaiya s mother
|-
| Pushpamala || as 
|-
| Baby Rani || as Azhagu, the little girl
|-
| O.A.K.Devar || as
|-
| Thirupadiswamy || as
|- Justin || as Marappa, the chief of Sengodan s soldiers
|-
| Govindharaj || as The chief of Sengodan s soldiers
|-
| N.S.Nadharajan || as
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Awards==

The Best Movie (Sirede Padham)

By The Tamil Nadu Government Award (Tamizh Aracin Parisu)

The Best Tamil Movie (Sirede Tamizh Padham)

By The Film Fare Award (Film Fare Parisu Virundhu)

==Around the movie==
J. P. Chandrababus final movie with MGR.

One of the first Tamil peplum (or sword-and-sandal movie) where MGR finances (under his Emgiyaar Pictures, the second production) and    plays a character in Hercules or close to Maciste, it is as we want.

MGR re-hangs just a little the weft of Maciste of 1961 and some scenes, Atlas Against the Cyclops, the original title was Maciste nella terra dei ciclopi.

Filming for ADIMAI PENN began in 1968, with B. Saroja Devi initially cast as the lead actress and many of her scenes shot.

Halfway during the filming she married, and MGRamachandran decided to replace her with Jayalalithaa in a dual role. 

Small correction:
-----------------
Originally filming of this movie was planned to take place in January 1967 but because of the shooting incident it was just postponed. Initially Saroja Devi was cast as the heroine but her announcement that she wasnt going to act with MGR anymore especially after the shooting incident made MGR remove her from the project and cast Jayalalithaa for her part. Only Saroja Devis screen still shots were taken in end of 1966. Saroja Devi was never cast in MGRs movies after 1966 Arasakatalai which also was completed by removing her character at the end of the movie because of her announcement. Saroja Devi married in March 1967 not in 1968.
-----------------
This was the movie that brought friction between MGR and T. M. Soundararajan which made T. M. Soundararajan increase his rate per song (from Rs. 500 to Rs.1,500 in 1969). This problem gave an opportunity to S.P.Balasubrahmanyam in the later years. Practically this particular matter made S.P.Balasubrahmanyam what he is today.
-----------------
Originally S.P.Balasubramaniam did the song "Thaayillaamal Naanillai" but during the screen test MGR suggested the voice didnt match with the scene. So back in Madras MGR made T.M.Soundararajan to do the song again. The version "Thaayillaamal Naanillai" in S.P.Balasubramaniams voice still lies in the archives but never released to the public.
-----------------

A Silver Jubilee hit (Madurai-Meenakshi ) and it ran 100 days in major cities 
This industrial record was beaten by Ulagam Sutrum Valiban four years later.

"ADIMAI PENN" is also known for Sa ashokans performance . This along with Ulagam Sutrum Valiban is considered as his career best performances.

The peplos ADIMAI PENN is the first union, MGR - K. Shankar - K. V. Mahadevan. In 1975, the trio re-forms for PALLANDU VAAZHGA !

Under Emgiyaar Pictures : 
1) NADODI MANNAN in August 22, 1958
2) ADIMAI PENN in May 1, 1969
3) ULAGAM SUTRUM VALIBAN in May 11, 1973

==Soundtrack==
{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 28.16
| title1       = Aayiram Nilave Vaa
| extra1       = S. P. Balasubrahmanyam, P. Susheela
| lyrics1      = Pulamaipithan
| length1      = 4.56
| title2       = Amma Endral
| extra2       = J. Jayalalitha
| lyrics2      = Vaali
| length2      = 4.59
| title3       = Kaalathai Vendravan
| extra3       = P. Susheela , S. Janaki
| lyrics3      = Avinashi Mani
| length3      = 6.14
| title4       = Thaai Illamal
| extra4       = T. M. Soundararajan
| lyrics4      = Alangudi Somu
| length4      = 3.36
| title5       = Unnai Paarthu
| extra5       = T. M. Soundararajan
| lyrics5      = Vaali
| length5      = 5.14
| title6       = Yemmattraathe
| extra6       = T. M. Soundararajan
| lyrics6      = Vaali
| length6      = 4.37
| title7       = Amma Endral (Not in the movie, not held)
| extra7       = T. M. Soundararajan
| lyrics7      = Vaali
| length7      = 2.35
| title8       = Thaai Illamal (Not in the movie, not held)
| extra8       = S. P. Balasubrahmanyam
| lyrics8      = Alangudi Somu
| length8      = 3.36
}}

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 