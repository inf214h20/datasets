Der Herr der Liebe
{{Infobox film
| name           = Der Herr der Liebe
| image          = Master of Love.jpg
| caption        = Film poster
| director       = Fritz Lang
| producer       = Erich Pommer
| writer         = Leo Koffler
| starring       = Carl de Vogt Gilda Langer
| cinematography = Carl Hoffmann
| distributor    = Helios Film
| released       =  
| runtime        = 60 minutes
| country        = Weimar Republic
| language       = Silent film German intertitles
}}
Der Herr der Liebe ( ) is a 1919 silent film directed in Germany by Fritz Lang. It was his second film.  Carl de Vogt and Gilda Langer starred, as they had in Langs debut feature, Halbblut. Lang himself is said to have acted in a supporting role.  

The film is now considered to be lost film|lost.   

== Plot ==
Residing in a castle in the Carpathian Mountains, Hungarian nobleman Vasile Disecu becomes infatuated with Suzette, the daughter of his neighbor. He mistakes Stefana, a maid who is secretly in love with him, for Suzette and makes love to her. When Yvette, his wife  or mistress,  finds out, she avenges herself with a liaison with Lazar, a Jewish peddler. Vasile imprisons Lazar. He kills Yvette and then himself.

== Cast ==
*Carl de Vogt as Vasile Disecu
*Gilda Langer as Yvette
*Erika Unruh as Stefana
*Max Narlinski as Lazar
*Sadjah Gezza as Suzette

== See also ==
*List of films made in Weimar Germany
*List of lost films

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 