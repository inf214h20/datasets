'night, Mother (film)
{{Infobox film
| name           =night, Mother
| image          =nightMotherFilmPoster.jpg 
| caption        =Film poster Tom Moore
| producer       =Aaron Spelling Alan Greisman
| writer         =Marsha Norman
| starring       =Sissy Spacek Anne Bancroft 
| music          =David Shire 
| cinematography =Stephen M. Katz
| editing        =Suzanne Pettit
| distributor    =Universal Pictures 
| released       = 
| runtime        =96 minutes
| country        =United States
| language       =English
| budget         = 
}}
 same name. It was entered into the 37th Berlin International Film Festival.   

==Plot==
Jessie is a middle-aged woman living with her widowed mother, Thelma. One night, she calmly tells Thelma that she plans to commit suicide that very evening. She makes this revelation all while nonchalantly organizing household items and preparing to do her mothers nails.

The resulting intense conversation between Jessie and Thelma reveals Jessies reasons for her decision and how thoroughly she has planned her own death, culminating in a disturbing yet unavoidable climax.

==Cast==
*Sissy Spacek ...  Jessie Cates
*Anne Bancroft ...  Thelma Cates
*Ed Berke ...  Dawson Cates
*Carol Robbins ...  Loretta Cates
*Jennifer Roosendahl ...  Melodie Cates
*Michael Kenworthy...  Kenny Cates
*Sari Walker ...  Agnes Fletcher
*Claire Malis ...  Operator (voice)

==Release==
The film was given its first-ever U.S. DVD release by Universal Studios on August 3, 2010.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 

 