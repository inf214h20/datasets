Wer (film)
{{Infobox film
| name           = Wer
| image          = Wer2013BrentBellMoviePoster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Brent Bell
| producer       = Matthew Peterman Morris Paulson Steven Schneider
| writer         = William Brent Bell Matthew Peterman
| starring       = A.J. Cook  Brian Scott OConnor  Simon Quarterman  Sebastian Roché Vik Sahay
| music          = Brett Detar
| cinematography = Alejandro Martínez
| editing        = William Brent Bell  Tim Mirkovich
| studio         = FilmDistrict Sierra Pictures Incentive Filmed Entertainment Prototype Room 101, Inc.
| distributor    = FilmDistrict
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} VOD in the United States in August 2014.  


==Synopsis==
Defense attorney Kate (A.J. Cook) is called to defend Talan (Brian Scott OConnor), after he is charged with the murders of a vacationing family. She learns that he is a werewolf who is capable of the slayings. Talan then escapes from his imprisonment into the city of Lyon, France.

==Cast==
*A.J. Cook as Kate Moore
*Vik Sahay as Eric Sarin
*Sebastian Roché as Klaus Pistor
*Stephanie Lemelin as Claire Porter
*Oaklee Pendergast as Peter Porter
*Angelina Armani as Claires Sister
*Simon Quarterman as Gavin Flemyng
*Brian Johnson as Henry Porter
*Collin Blair as British Reporter (as Collin Jay Blair)
*Brian Scott OConnor as Talan Gwynek
*Corneliu Ulici as Police Officer

==Production==
Plans for Wer were initially announced in January 2012 during an interview with Dread Central, when Bell and producer Matthew Peterman said they had been working on a werewolf-themed project for the past 10 months.    The two initially planned to begin production on the film, a "faux-documentary style project," in Romania during April 2012.   

In March 2012, Cook was confirmed as the lead heroine of Wer, and Rob Halls Almost Human, Inc company was recruited to create the movies special effects.   Later that same year, Vik Sahay and Sebastian Roché were also attached to the project.   Sahay initially debated whether or not to take the role of Eric Sarin due to a busy schedule, but ultimately decided to join because the character and the movies world "were just too interesting to walk away from".    

Filming for Wer began in May 2012 in Bucharest, Romania,  and Bell shot the movie concurrently with another project, The Vatican.  A trailer for Wer was released in October 2013. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 