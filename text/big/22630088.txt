Pippi Goes on Board (film)
{{Infobox film
| name           = Pippi Goes on Board
| image          = Pippi2 gip.jpg
| caption        = The DVD cover
| director       = Olle Hellbom
| producer       = Ernst Liesenhoff Olle Nordemar
| writer         = Astrid Lindgren (novel)
| starring       = Inger Nilsson Maria Persson Pär Sundberg
| music          = John Powell
| editing        = Amrik Minku John Cryer
| released       = October 31, 1969 (West Germany)

February 10, 1973 (Sweden)
August, 1975 (USA)
| distributor    = Beta Film
| runtime        = 83 min.
| country        = Sweden West Germany Swedish
}}
 1969 Swedish eponymous childrens Pippi Longstocking. The movie consisted of re-edited footage from the TV series. It was released in the USA in 1975.

==Plot==
The further adventures of super-strong girl Pippi Longstocking and her friends Tommy and Annika in this sequel compilation film of the classic Swedish TV series.

==Cast==
*Inger Nilsson - Pippi Longstocking
*Pär Sundberg - Tommy
*Maria Persson - Annika
*Beppe Wolgers - Captain Efraim Longstocking
*Margot Trooger - Mrs. Prysselius
*Hans Clarin - Dunder-Karlsson
*Paul Esser - Blom
*Ulf G. Johnsson - Kling
*Göthe Grefbo - Klang
*Fredrik Ohlsson - Mr. Settergren
*Öllegård Wellton - Mrs. Settergren
*Staffan Hallerstam - Benke

==See also==
*Pippi Longstocking - The character Pippi Longstocking - The TV series

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 
 