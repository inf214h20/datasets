Resin (film)
 
{{Infobox film
| name = Resin
| image = 
| alt = 
| caption =  Vladamir Gyorski
| producer = Steven Sobel
| writer = Steven Sobel
| starring = David Alvarado Dogme
| cinematography = Brent Meeske
| editing = Jessica Kehrhahn
| distributor = Organic Film
| released =  
| runtime = 87 minutes
| country = United States
| language = English
}}
Resin is a 2001 American drama film directed by Steven Sobel (under the pseudonym Vladamir Gyorski). It is the twenty-third film in the Dogme 95 film movement.

==Plot==
The film is a scathing indictment of Californias controversial Three-strikes law. It follows a vagabond named Zeke, who is a low-level marijuana dealer in Isla Vista, the college town bordering the University of California at Santa Barbara. Zeke takes a bad felony plea deal at the opening of the film, in part due to a poor public defenders advice. This comes back to haunt him later in the film when he is wrongfully accused of assaulting some frat kids, when they jump him and he is forced to defend himself. When he is arrested soon after as part of an undercover ongoing narcotics sting, he comes face-to-face with a mandatory 25 to life sentence.

==Cast==
* David Alvarado as Zeke
* Mia Kelly as Sira
* Michael Glazer as Glazer

==Accolades==
Resin won the Audience Award at its World Premiere at the Chicago Underground Film Festival on August 21, 2001. The film went on to win several best in festival awards, as well as the Digital Vanguard Award in Park City and the Special Prize at the Seoul Net festival in Korea.

==Availability==
The film has been difficult to find due to a number of rights issues related to the soundtrack, which often featured well-known artists in the background of scenes due to Dogme rule #2 stating that all sound must be recorded at the location where the action takes place.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 