Reconstruction (2001 film)
 
{{Infobox Film
| name           = Reconstruction
| image          = Reconstruction.jpg
| image_size     = 
| caption        = 
| director       = Irene Lusztig
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       = 2001 
| runtime        = 90 minutes
| country        =  Hebrew and French and Romanian with subtitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 documentary made by Irene Lusztig that investigates the Ioanid Gang bank heist committed in 1959 in Communist Romania. The film focuses on Monica Sevianu, Lusztigs grandmother, and the only female involved in the heist.

The documentary gets its name from a propaganda film that was made by the Romanian government three years after the robbery was committed. The original Reconstruction was a strange blend of documentary and fiction, where the criminals played themselves in a crime-film whose plot was driven by clues that self-congratulatory detectives pieced together. The propaganda film was only screened to journalists and high ranking communist officials before being buried beneath government files. Lusztig managed to find a copy and incorporates clips into her documentary.

==Summary==
Exploring her dark family secret, Lusztig blends Romanias history with her personal history. Modern footage of Bucharest, news clips from the postwar era, and personal family interviews are complied in order to make sense of her grandmothers criminal behavior. But, unlike a typical crime-thriller, the real-life criminal is complicated and cant be fully understood from a few pieces of evidence and an extorted confession.

By 1959 it was clear that the utopia communism had promised Romania was not going to be a reality. Antisemitism, which had been boiling since before the war, continued to pose as a threat, and the suspicion and distrust of the communist age effected the entire nation. Monica and her husband Gugu, who also participated in the robbery, had been communist supporters who were abandoned and abused by the government as soon as it came to power.

Lusztigs documentary speculates that governments oppression and antisemitism encouraged their criminal behavior. Having given up on their hopes for a government they believed in, Monica and her husband turned to adventure and pleasure seeking. With the money she stole, before her arrest, Monica bought a pleated gray skirt and an orange angora sweater.  Material pleasures offered happiness and certainty that the government had taken from her.

With a tone of uncertainty Lusztig tries to sort through the rumors that surround the crime in hopes of discovering what really happened. Were the criminals going to give the money to poor Jews in their community? Were they going to fly away and escape to Israel? Or, perhaps, was the crime staged by the communist government to encourage antisemitism? Surprises and twists unfold in the process to unveil the truth.

==Production== German writer Heinrich Bölls novel Group Portrait with Lady (novel)|Group Portrait with Lady. 
{{cite news
  | last =Gambale
  | first =Maria Luisa
  | title = Reconstructing the Romanian Past
  | publisher = New England Film
  | date =
  | url =http://www.newenglandfilm.com/news/archives/01october/romanian.htm
  | accessdate = 13 August }} 

*Lusztigs editing and filming draws from Dusan Makavejev and the belief that a filmmaker should trust his or her intuition and can make connections between seemingly unconnected images. 

==Film festivals and awards==
Reconstruction was screened at a number of film festivals including:
* Vancouver International Film Festival Docaviv Documentary Film Festival
* Its All True São Paulo Documentary Film Festival
* Singapore International Film Festival
* Cleveland International Film Festival
* Documentary Fortnight, Museum of Modern Art, NY
* Boston International Womens Film Festival
* Atlanta Film Festival
* Palic International Film Festival Serbia
* Alpe Adria Cinema Film Festival Italy
* New York Jewish Women Film Festival

Reconstruction received the following awards:
* FIPRESCI Jury Nomination- from IDFA Amsterdam International Documentary Festival
* Certificate of Merit- from San Francisco International Film Festival
* "Rediscoveries/Discoveries" Award- from the Boston Society of Film Critics
* Best Documentary- at the New England Film Festival

==Reception==
Reconstruction received praise from the Boston Phoenix, Variety (magazine)|Variety, and film scholars at Harvard and University of California, Berkeley|Berkeley. It has been hailed as "an example of personal documentary at its best." {{cite web
  | last = Lusztig
  | first =Irene
  | title =Reconstruction
  | publisher = Women Make Movies
  | date =
  | url =http://www.wmm.com/filmcatalog/pages/c585.shtml
  | accessdate =13 August}} 

==Notes==
 

==See also==
*Ioanid Gang
*Romania
*Criticisms of Communist party rule
*bank robberies
*propaganda films

===Documentaries about other singular Jewish women===
*Queen of the Mountain
*Marions Triumph
*Polas March

==References==
*{{cite news
  | last = Peary
  | first =Gerald
  | title =Reconstruction
  | work =
  | pages =
  | publisher =Gerald Peary
  | date =October 2001
  | url =http://geraldpeary.com/reviews/pqr/reconstruction.html
  | accessdate = 13 August  }}

*{{cite news
  | last =Gambale
  | first =Maria Luisa
  | title = Reconstructing the Romanian Past
  | publisher = New England Film
  | date =
  | url =http://www.newenglandfilm.com/news/archives/01october/romanian.htm
  | accessdate = 13 August }}

*{{cite web
  | last = Lusztig
  | first =Irene
  | title =Reconstruction
  | publisher = Women Make Movies
  | date =
  | url =http://www.wmm.com/filmcatalog/pages/c585.shtml}}
*{{cite web
  | title = Reconstruction
  | publisher =Komsomol Films
  | date =
  | url =http://www.komsomolfilms.com/filmsframeset.html
  | accessdate = 13 August}}

==External links==
* 
* 
* 
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 