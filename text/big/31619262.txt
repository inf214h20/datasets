The Big Swallow
{{Infobox film
| name           = The Big Swallow
| image          = The Big Swallow (1901).ogv
| image_size     = 
| caption        =  James Williamson
| producer       =
| writer         =
| narrator       =
| starring       = Sam Dalton
| music          =
| cinematography = James Williamson
| editing        =
| studio         = Williamson Kinematograph Company
| distributor    =
| released       =  
| runtime        = 1 min 8 secs
| country        = United Kingdom Silent
| budget         =
}}
 1901 UK|British short  silent comedy James Williamson, featuring a man, irritated by the presence of a photographer, who solves his dilemma by swallowing him and his camera whole. The three-shot trick film is, according to Michael Brooke of BFI Screenonline, "one of the most important early British films in that it was one of the first to deliberately exploit the contrast between the eye of the camera and of the audience watching the final film."   

==Reviews== George Albert Smith in Grandmas Reading Glass and Spiders on a Web (both 1900), "a stage further by featuring a man advancing towards the camera, remaining in more or less perfect focus until his mouth appears to swallow the lens."

Although the directors, "purpose was primarily comic (and doubtless inspired by unwanted attention from increasingly savvy passers-by while filming his actuality shorts)," he creates, "one of the most striking genre entries," and, "makes imaginative use of an extreme close-up to create one of the seminal images of early British (and world) cinema, as effective in its way as the slashed eyeball of Un Chien Andalou (1929), and of just as much appeal to the Surrealist movement."

The film, however, "might have been still more effective if Williamson had omitted the second and third shots," in which he, "cuts to the photographer apparently disappearing into a black void, and then back to the man who retires munching him up and expressing great satisfaction, "since they detract from the logical purity of the first, ending on a completely blank screen as the swallowed camera is no longer able to function as a surrogate for the audiences point of view." 

==References==
 

==External links==
 
* 
*  

 
 
 
 
 
 
 
 


 
 