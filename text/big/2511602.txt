Dr. Dolittle (film)
 
 
 

{{Infobox film
| name = Dr. Dolittle
| image = Dr dolittle movie 1998.jpg
| caption = Theatrical release poster
| alt = Man in a white medical coat, with a white stethascope hanging from his neck, and a group of small animals
| director = Betty Thomas John Davis David T. Friendly Joseph Singer
| writer = Hugh Lofting Nat Mauldin Larry Levin Kristen Wilson Norm Macdonald Kyla Pratt Raven-Symoné
| music = Richard Gibbs
| editing = Peter Teschner Friendly Films
| distributor = 20th Century Fox
| released =  
| runtime = 85 minutes
| country = United States
| language = English
| budget = $70.5 million
| gross = $294.4 million 
}} musical under same title, box office bomb, but still remains a cult classic and a two-time Academy Award-winner. Although the 1998 film was rated Motion Picture Association of America film rating system|PG-13 by the Motion Picture Association of America|MPAA, it was marketed as a family film.

The 1998 film was a box office success, despite receiving mixed reviews from critics. The films success generated 4 sequels;  , and  . 

==Plot== minister to perform an exorcism. When John is scared his dog attacks the minister, and his father gives the dog up for adoption. The incident greatly upsets John and he stops talking to animals.

Thirty years later, John (Eddie Murphy) is now living in San Francisco, California. He is a doctor, married with two children, and an animal hater. His 13-year-old daughter Charisse (Raven-Symoné) demands to be called Paprika, and his youngest daughter Maya (Kyla Pratt) is a nerdy girl who dislikes socializing, and is raising what she believes to be a swan egg so it will bond with her. She also has a Guinea Pig named Rodney (voiced by Chris Rock). Johns wife Lisa (Kristen Wilson) wants to spend time with him, while at Johns practice, a large medical company owned by Calloway (Peter Boyle) wants to buy his practice, making them a great deal of money.

John takes his family out to the country for a vacation. After taking the family to the country, he has to return to work to see a patient and to bring Maya Rodney. On the way home John accidentally hits a dog (voiced by Norm MacDonald) and bumps his head on the windshield. The dog runs off, shouting "Watch where youre going next time, you bone-head!"; the first time that John has understood an animal since his childhood. The next day, John is driving Rodney to the country and Rodney starts talking to him as well, as John is terrified of what is happening to him. During the night, an owl (voiced by Jenna Elfman) asks him to remove a twig in her wing. He obliges and she tells all the animals about his kind act. Soon many animals start asking favors of John. Scared, he goes to see Dr. Sam Litvak (Steven Gilborn) for a CAT scan, but nothing is wrong with him. The next day he finds the dog he nearly hit being taken to a kennel. John rescues the dog and names him Lucky.

That night, John finds that his reputation for understanding animals has spread, and a mass of animals come to him seeking treatment. In the process, John relearns to appreciate his gift and confides in Lucky that he feels more energized about his work than he has in years. A drunken circus monkey tells the doctor about a sick tiger (voiced by Albert Brooks) who is planning to commit suicide. They find the tiger, named Jake, on top of Coit Tower planning to jump, as something in his brain is causing him immense pain. John and Lucky convince Jake they can help him. Lisa and Johns partner Mark (Oliver Platt) catch him giving CPR to a rat and have him committed to an insane asylum. Lucky comes to see John and tells him Jake is getting worse, but John is angry at what has happened to him. Lucky leaves, telling John hes hiding from his true self, and John is released when he vows to never talk to animals again. At home, Maya hears the news and tells her grandfather what has happened with him. John overhears this and apologizes to Lucky. During a party where Calloway will buy the company, John and Lucky bring Jake to the clinic to operate on him. The police arrive seeking the tiger, but Lucky gathers all the animals of San Francisco to help guard the building in which John is operating on the tiger.

At the party the guests discover Jake, but John explains what he is doing and gets Jake onto the operating table as the guests watch. Johns father, realizing Johns ability as the gift it is, confides in Lisa he knows John truly can talk to animals, and she comes to comfort Jake as John works. John finds out that Jake is suffering from a blood clot stuck in his head, and saves his life. Calloway is impressed, but John declines his offer to buy the company. In an epilogue, John is now both a human doctor and a veterinarian, and Mayas egg hatches, revealing it to be an alligator. John and Lucky head to the circus to visit Jake and talk about their future as friends while the song "Talk with the Animals" plays in the background.

==Cast==
 
* Eddie Murphy as Doctor Dolittle|Dr. John Dolittle
* Ossie Davis as Grandpa Archer Dolittle
* Oliver Platt as Dr. Mark Weller
* Peter Boyle as Calloway
* Richard Schiff as Dr. Gene "Geno" Reiss
* Kristen Wilson as Lisa Dolittle
* Jeffrey Tambor as Dr. Fish
* Kyla Pratt as Maya Dolittle
* Raven-Symoné as Charisse Dolittle
* Paul Giamatti as Blaine Hammersmith (uncredited)
* Pruitt Taylor Vince as Patient at Hammersmith (uncredited)

===Voice cast===
 
  Royce Applegate
*Albert Brooks: Jake the tiger 
*Hamilton Camp
*Erin Cardillo Jim Dean
*Ellen DeGeneres
*Jeff Doucette
*Brian Doyle-Murray
*Chad Einbidnder
*Jenna Elfman
*Eddie Frierson
*Gilbert Gottfried Archie Hahn
 
*Phyllis Katz
*Julie Kavner
*John Leguizamo
*Jonathan Lipnicki: Tiger cub 
*Norm Macdonald: Lucky 
*Kerrigan Mahan
*Phil Proctor
*Paul Reubens
*Chris Rock: Rodney 
*Garry Shandling
*Tom Towles
 

==Soundtrack==
{{Infobox album  
| Name        = Dr. Dolittle
| Type        = soundtrack
| Artist      = Various artists
| Cover       = Dr. Dolittle OST.jpg
| Released    = June 16, 1998
| Recorded    = 1997&ndash;98 Hip hop, Contemporary R&B|R&B
| Length      =  Atlantic
| Producer    = Timbaland, Rodney Jerkins, The Legendary Traxster, Various
| Chronology  = Dr. Dolittle soundtracks
| Last album  = 
| This album  = Dr. Dolittle (1998)
| Next album  = Dr. Dolittle 2#Soundtrack|Dr. Dolittle 2 (2001)
| Misc                = {{Singles
  | Name            = Dr. Dolittle
  | Type              = soundtrack
  | single 1         = Are You That Somebody?
  | single 1 date = June 16, 1998
  | single 2         = Same Ol G
  | single 2 date = July 28, 1998
  | single 3         = Thats Why I Lie
  | single 3 date = September 22, 1998
  }}
}}
The soundtrack was released on June 16, 1998 through Atlantic Records and consisted of a blend of hip hop and contemporary R&B. The soundtrack was a huge success, peaking at 4 on both the Billboard 200|Billboard 200 and the Top R&B/Hip-Hop Albums and was certified 2× Multi-Platinum on October 20, 1998. Allmusic rated the soundtrack four stars out of five. 

The soundtracks lone charting single, "Are You That Somebody?" by Aaliyah, also found success, making it to 21 on the Billboard Hot 100|Billboard Hot 100 and received a nomination for Best Female R&B Vocal Performance at the Grammy Awards. 

# " )
# "  and Shaunta)
# " )
# " )
# " )
# "Da Funk" – 4:29 (Timbaland)
# "  and Ivan Matias)
# "Your Dress" – 3:59 (Playa (band)|Playa)
# "Woof Woof" – 4:11 (69 Boyz)
# "Rock Steady" – 3:05 (Dawn Robinson)
# "In Your World" – 4:50 (Twista and Speedknot Mobstaz)
# "Lovin You So" – 3:35 (Jody Watley)
# "Dance" – 3:38 (Robin S. and Mary Mary)
# "Push Em Up" – 3:46 (DJ Toomp, Eddie Kane and Deville)
# "Aint Nothin but a Party" – 3:57 (The Sugarhill Gang)

==Reception==
The film received mixed reviews, with a 44% rating on Rotten Tomatoes. {{cite web
| url = http://www.rottentomatoes.com/m/dr_dolittle/
| title = Dr. Dolittle (1998)
| publisher = Flixster
| work = Rotten Tomatoes
}} 

===Box office===
On its opening weekend, the film grossed $29,014,324 across 2,777 theaters in the United States and Canada, ranking #1 at the box office, the best debut for an Fox film that week. By the end of its run, Dr. Dolittle had grossed $144,156,605 in the United States and $150,300,000 internationally, totaling $294,456,605 worldwide. {{cite web
| url=http://boxofficemojo.com/movies/?id=drdolittle.htm
| title=Dr. Dolittle (1998) IMDb
| work = Box Office Mojo
| accessdate=2011-07-27
}} 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 