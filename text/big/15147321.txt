Reprise (film)
{{Infobox film name           = Reprise image          = Reprise film.jpg caption        = International Theatrical poster director       = Joachim Trier producer       = Karin Julsrud writer         = Joachim Trier, Eskil Vogt starring       = Anders Danielsen Lie Espen Klouman-Høiner Viktoria Winge music          = Ola Fløttum, Knut Schreiner cinematography = Jakob Ihre editing        = Olivier Bugge Coutté distributor    = Spelling Films Maverick Films released       = 8 September 2006 (Norway) 2 August 2007 (Germany) 28 June 2007 (Netherlands) 7 September 2007 (UK) 
4 January 2008 (Poland) 16 May 2008 (U.S.) runtime        = 105 minutes country        = Norway language  Norwegian	
}}
 Norwegian film directed by Joachim Trier. Co-written over the course of five years with Eskil Vogt, it is Triers first feature-length film. In 2006 it was the Norwegian candidate for the Academy Award for best foreign-language film.

== Plot ==
Two 23-year-olds, Erik and Phillip, dream of becoming successful writers. They idolize the reclusive writer Sten Egil Dahl (a character modeled on Tor Ulven).  When they both try to get a manuscript published, Eriks is rejected. Phillips, on the other hand, is accepted and he becomes a star of the Norwegian literary scene overnight. Phillip meets Kari, with whom he falls in love. Six months later, Erik and his friends pick up Phillip at a psychiatric hospital to bring him home after a long treatment. Erik still hasnt given up his dream, but Phillip isnt able to write anymore, although his friend encourages him to make a new effort. Instead Phillip tries to get his ruined relationship with Kari to start from the beginning again. Dahl provides Erik with constructive feedback on his writing, including insights on how to write more authentically.

==Critical reception==
Reprise received generally positive reviews from Western critics. As of August 23, 2008, the review aggregator Rotten Tomatoes reported that 87% of critics gave the film positive reviews, based on 69 reviews.  Metacritic reported the film had an average score of 78 out of 100, based on 10 reviews. 

===Top ten lists===
The film appeared on several critics top ten lists of the best films of 2008.     
 New York Daily News 
*6th - Nathan Rabin, The A.V. Club 
*8th - Keith Phipps, The A.V. Club 
*10th - Andrew OHehir, Salon.com|Salon 

In November 2009, film critics at the Norwegian newspaper Verdens Gang named Reprise the best Norwegian film of the decade.   

==Box office performance==
As of July 9, 2008, the film has grossed approximately $1,161,592 worldwide– $514,013 in the United States and Canada and $647,579 in other territories  (including $491,785 in Norway ).

== Awards ==

*Istanbul International Film Festival
** The golden Tulip: Best Film

*European Film Festival in Lecce
**The golden Olive: Best Film
**Best Script

*Karlovy Vary International Film Festival
**Best Direction
**Don Quijote Prize

*Nordic Film Festival in Rouen
**Best Film
**Audience Award

*Toronto International Film Festival
**Discovery Award
 Amanda Award
**Best Film
**Best Direction
**Best Script

Reprise was shown outside of competition at the 2007 Sundance Film Festival.

==References==
 

==External links==
* 
* 
* 
* 
* 
*  at YouTube
*   at  

 

 
 
 
 
 
 