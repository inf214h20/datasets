The Heist (2008 film)
 
 
 
{{Infobox film
| name = The Heist
| image =
| caption =
| runtime = 6 minutes
| director = Campbell Cooley
| producer = Campbell Cooley
| starring = Marissa Stott, Helen Haskell
| country = New Zealand
| released =  
}}
The Heist is a quirky crime-drama about two first-time jewel thieves who stage a bizarre diamond heist with seemingly disastrous results, written and directed by short-film director Campbell Cooley and presented in 2008.

With The Heist Campbell Cooley wanted to tell a character-driven story without dialogue and, to add to the complexity of the challenge, he also underscored the entire story to a minuet by Wolfgang Amadeus Mozart, writing the story to the music. Consistent with his style, the film is full of twists and surprises. The action flows in harmony with Mozart’s Minuet movement from Divertimento in D Major.

The Leith Short Film Festival in Scotland called it a "Witty & engaging crime-drama".    The Panorama International Film Festival of Patras, Greece are quoted as saying: "...(the) film is very beautiful!". In April 2009, the Magma Short Film Festival in Rotorua, New Zealand bestowed it a Highly Commended Award in the festival category of Self Funded Films.

==Screening history==
Completed in March 2008, the film has screened at the following festivals:
*11th International Panorama of Independent Film, Greece, October 2009   
*Magma Short Film Festival, New Zealand, May 2009
*Auckland Fringe Short Film Festival, New Zealand, March 2009
*Hamilton Underground Film Festival, New Zealand, December 2008
*Allshorts Film Festival, New Zealand, October–November 2008
*Big Mountain Short Film Festival, New Zealand, October 2008
*Naperville Independent Film Festival, USA, September 2008
*Anonimul International Film Festival, Romania, August 2008   
*Shaky Isles Festival of Aotearoa, England, July 2008
*CinemadaMare Film Festival, Italy, July 2008   
*Leith Short Film Festival, Scotland, June 2008 
*Dunedin Fringe Festival, New Zealand, April 2008

The Heist has also been purchased by The Rialto Channel and premièred on Australian TV 4 August 2008.

===Current characters===
{| width="40%" class="wikitable" Character
!width="50%" Actor
|-
| Mira || Marissa Stott
|-
| Margaret || Helen Haskell
|-
| Thief#1 || Phil Jones
|-
| Thief#2 || Andrew McCartney
|-
| Mother || Lise Baldwin
|-
| Boy || Jaden Movold
|-
|}

==References==
 

==External links==
*  
* http://www.independent.gr
* http://www.dunedinfringe.org.nz/
* http://www.leithshortfilmfestival.co.uk/
* http://www.cinemadamare.com/home.php
* http://www.festival-anonimul.ro/festival_progr_en
* http://www.bigmountain.co.nz
* http://www.allshorts.org.nz
* http://www.rialtochannel.co.nz/
* http://circuit47.com/huff/

 
 
 
 


 