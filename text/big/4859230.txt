Sheerluck Holmes and the Golden Ruler
{{Infobox film
| name           = Sheerluck Holmes and the Golden Ruler
| image          = Sheerluck holmes and the golden ruler.jpg
| alt            = 
| caption        = DVD cover
| director       = Mike Nawrocki
| producer       = David Pitts
| writer         = Robert G. Lee
| starring       = Phil Vischer Mike Nawrocki Jim Poole Tim Hodge
| music          = Kurt Heinecke
| editing        = J. Chris Wall
| studio         = Big Idea Productions
| distributor    = Word Entertainment
| released       =  
| runtime        = 52 minutes
| country        = United States
| language       = English
| budget         = 
}}
Sheerluck Holmes and the Golden Ruler is the List of VeggieTales episodes|twenty-second episode in the VeggieTales series. It was released in early 2006 in both DVD and VHS format. Subtitled "A Lesson in Friendship", it features two stories that illustrate what it means to be a good friend. The first is a parody of Don Quixote de la Mancha by Miguel de Cervantes. The second is a parody of the Sherlock Holmes novels and stories by Sir Arthur Conan Doyle. This is also the last episode in the series to be released on VHS and the second video to have the 2002–present Big Idea logo.

== Plot ==
 
At the countertop, hosted by Bob and Larry who receive a message from a child from Minot, North Dakota asking how to get friends. But Larry just tells mean jokes about South Dakota losing to North Dakota. When Larry thinks that hes "On a Roll" on his jokes, Bob angrily starts the stories.

An adaption of Spanish novel Don Quixote, "Asparagus of La Mancha" starts with Knight Don Quixote facing three Peas in a surrealist world made of cooking utensils and foods. Then he wakes up. It was a dream. He and his best friend Poncho work in a Spanish restaurant of Cafe LaMancha. However, Jean Claude comes in to inform the duo of a new restaurant opening across the street called The Food Factory, the most successful, and biggest, restaurant in the world. With their customers abandoning them, Don is sure they will go out of businesses.

After losing another game of Spanish checkers to Poncho, Don goes back to sleep. He assumes his latest dream has informed him to try different themes to win back their customers. This fails, and shortly after, the Food Factory manager offers Poncho a job at the Food Factory. Poncho refuses.

Dons next dream convinces him to attack the Food Factory. Poncho tries to tell him out of it by giving him his idea; A Touchdown Turkey but Don refuses Ponchos idea and attacks the Food Factory only to get incarcerated. While visiting Don in prison, Poncho learns that Dons addiction to his super-spicy salsa is causing the bad dreams. He weans Don off the salsa, much to Dons disappointment. The next morning, Don claims he slept well. Sheriff Bob claims hell let Don go if he quits the salsa. Poncho has an idea to open their restaurant for "la breakfast", as the Food Factory doesnt open until lunchtime.
 Silly Song, Larrys ball bounces into a gated community; instead of helping him, the residents just sing about how lovely life is there. Larry, at the end, annoyingly leaves but the singers finally throw him his ball and Larry thanks them.

"Sheerluck Holmes" opens up to Sheerluck and Watson entering Doylies ice cream parlor, claiming they have "The Howling Dogs of Baker Street" case wrapped up. Sheerluck takes all the credit and praise, upsetting Watson. After leaving, Scooter informs the duo of a plot to steal the Golden Ruler, the kingdoms most valuable treasure.

At the palace, they meet up with the Prime Minister, and find a few clues including footprints and a secret passageway, but police group, Fish and Chips, want to solve the case themselves.

Back at Doylies, when Sheerluck takes the credit again for the clues Watson finally cracks and leaves, saying to Sheerluck, "When you want to start treating me like a friend, come talk to me."

The very next morning, at Sheerlucks apartment, Scooter informs him that the Golden Ruler has been stolen. He arrives at Watsons apartment, only to be battered by Watsons maids.
 Golden Rule, after which Watson appears; he has been disguised as one of the palace guards. In a CSI-type method, he explains the clues and finds that the thief is a palace guard who is a French spy imposter. The Prime Minister interrogates them politely, but they cannot match any of them to the crime. However Sheerluck trips and falls onto the guards causing them to fall onto each other like dominoes and revealing the culprit to be a guard. The guard is arrested, and Sheerluck and Watson celebrate at Doylies.

Back at the countertop, Larry apologizes for insulting South Dakota and receives a musical message from South Dakota which shows their/its acceptance for his apology. Following this, Bob and Larry finish (Larry returns in "BAD APPLE" as worlds hero "Larryboy") and the show ends with the credits rolled.

==The Asparagus of LaMancha==
* Archibald Asparagus as Don  Quixote (pronounced KEE-HO-TAY)
* Mr. Lunt as Poncho
* Mr. Nezzer as the Food Factory Manger
* Bob the Tomato as the Sheriff
* Jean Claude Pea as himself and coward
* Phillipe Pea as coward
* Christophe Pea as coward
* Other Spanish citizens of Cafe LaManacha and The Food Factory
* A Pea With Gray Hair as himself
* Unnamed Villager Woman as herself
* Villager Women with Tan Dress as herself
* Unnamed Village Man as himself
* Village Man with Pink Shirt or Pink Hat as himself
* Unnamed Fly Villager as himself
* Carrot Men with Green Stripes Brown Rope Brown Hair as himself
* Unnamed Village Girl as herself
* Bob Pea as cockney eagle
* Girl Pea as herself
* Carrot Men with Tan Pants Brown Rope Brown Hair as himself
* Unnamed Villager Old Man as himself
* Villager Old Man with Black Pants as himself
* James Pea as himself

==Sheerluck Holmes and the Golden Ruler==
* Larry the Cucumber Sheerluck Holmes
* Bob the Tomato as Dr. Watson
* Archibald Asparagus as the Prime Minster
* Pa Grape as the King of England
* Madame Blueberry as the Queen of England
*Jean Claude Pea as the criminal
*Phillipe Pea as the criminal
*The French Peas as the criminals
* Jimmy and Jerry Gourd as the ice cream patrons
* Soccer carrot as the English citizen
* Mr. Nezzer as Detective Bill Trout
* Mr. Lunt as Sergeant John Spud
* Other London citizens
* Other ice cream patrons

== Songs ==

Besides the "VeggieTales Theme Song", it contains the following songs:

* "The Gated Community" (Silly Song) by Nawrocki, Ward, and West
* "Call on Me" by Nawrocki
* "Call on Us" by Nawrocki and Vischer

== External links ==
*  
*   from families.com

 
 

 
 
 
 
 