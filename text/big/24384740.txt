Hereafter (film)
{{Infobox film
| name           = Hereafter
| image          = Hereafter.jpg
| caption        = Theatrical release poster
| director       = Clint Eastwood Kathleen Kennedy Robert Lorenz Steven Spielberg (executive)
| writer         = Peter Morgan
| starring       = Matt Damon Cécile de France
| music          = Clint Eastwood Tom Stern
| editing        = Joel Cox Gary D. Roach
| studio         = The Kennedy/Marshall Company|Kennedy/Marshall Malpaso Productions Amblin Entertainment (uncredited) Warner Bros. Pictures
| released       =  
| runtime        = 129 minutes
| country        = United States
| language       = English French
| budget         = $50 million   
| gross          = $105,197,635 
}} American factory French television British twins Marcus and Jason (played by Frankie and George McLaren). Bryce Dallas Howard, Lyndsey Marshal, Jay Mohr, and Thierry Neuvic have supporting roles.
 on spec to DreamWorks in 2008, but it transferred to Warner Bros. by the time Eastwood (who has a long-standing relationship with Warner Bros.) had signed on to direct in 2009. Principal photography ran from October 2009 to February 2010 on locations in London, San Francisco, Paris, and Hawaii.
 premiered as a "Special Presentation" at the 2010 Toronto International Film Festival in September 2010. The film was given a limited release on October 15, 2010 and was released across North America on October 22, 2010. Although a box office success, the film received mixed reviews, with critics praising the plot and acting performances, while noting that the movie suffered from a lack of focus on the story.

==Plot==
On assignment in Thailand, French television journalist Marie Lelay (Cécile de France) is shopping for souvenirs for her lovers children. She finds a stand where a mother and her daughter work; they sell gifts to Marie for a dollar. Her lover Didier (Thierry Neuvic) looks over the balcony and witnesses the 2004 Indian Ocean tsunami coming into shore. It hits as Marie watches from a distance. She grabs hold of the girl and runs away from the shore but is quickly swallowed by the wave. Pulled lifeless from the water, she is resuscitated by rescuers but is left for dead. She gasps back to life after having a Near death experience|near-death experience in which she sees a vision of human figures inhabiting a realm of light, among them the silhouettes of the mother and daughter holding hands.  Marie and Didier are soon reunited as the disaster subsides and they return to Paris. Maries experience, however, interferes with her work performance to the point that Didier (who is also her producer) sends her on a leave of absence to write the book theyve discussed, which would add to her prestige.
 San Francisco medium with a gift for communicating with the dead, George abandoned his old career because he was unable to deal with the emotional impact of the reunions and the often disturbingly intimate family secrets revealed. While doing the reading, George hears the word June and asks if a date in June means anything to him. Christos at first denies that it means anything, but privately reveals to Billy that June was the name of his late wifes nurse, whom he was in love with for ten years.

From there, 12-year-old London twins Marcus and Jason (Frankie and George McLaren) try desperately to prevent their alcoholic, heroin-addicted mother, Jackie (Lyndsey Marshal), from losing them to social services. After evading the authorities yet again, the boys mother sends Jason to the chemist (pharmacist) to pick up her detox prescription. On the way home, Jason is attacked by street thugs, and while trying to escape, he is hit by a van and killed. No longer able to protect his mother, and barely able to cope with life without the brother he idolizes, Marcus is sent to a foster home.

Now writing a book and with more time to contemplate her near-death experience, Marie travels to Switzerland to meet a renowned specialist in the field. As the director of a hospice who has seen her share of dying patients, the doctor describes herself as a former skeptic who was convinced by the evidence that the afterlife exists and that people like Marie have had a genuine view of it. She persuades Marie to write a book on her experience in the hope that the scientific community will ultimately accept the reality of life beyond death.
 2005 London Bombings.

George enrolls in a cooking class taught by one of San Franciscos leading chefs. Its  students are paired-up, resulting in George being partnered with a young woman named Melanie (Bryce Dallas Howard). The two soon hit it off and after attending their second class decide to put their new culinary skills to use by preparing an Italian dinner at Georges place. All goes well until they hear an ill-timed phone message from his brother, which inclines George to reveal his past as a psychic to Melanie. Curious, she presses George to do a reading for her. George explains his reluctance, but acquiesces. They contact the spirit of Melanies father, who ends the session by asking her forgiveness for what he did to her as a child. Melanie flees Georges home in tears, and she doesnt return to the cooking class.

Having been in talks with a publisher before her trip to Thailand about a biography of  ) rejects the manuscript but soon steers her toward other publishers who might be interested, the most promising of them in London.

Marie learns from Didier that he does not intend on having her back at the job he urged her to take leave of, because her public interest in the hereafter damages her reputation as a serious journalist, and that he is having an affair with the woman who replaced her on the TV news program. 

George is laid off from his factory job, and is persuaded by Billy to revive his psychic practice. Still heartbroken over the fiasco with Melanie, he changes his mind and impulsively leaves San Francisco to make a new start elsewhere. He travels to London and listens every night to audiobook readings by Derek Jacobi of Charles Dickens works. As a Dickens devotee, he also visits the Dickens Museum and attends a live reading of Dickens at the London Book Fair. There, one of the presenters is Marie, reading her now published book, Hereafter. While handing a signed copy of her book to George, their hands touch and George has a psychic flash of Maries tsunami drowning.

Marcus and his foster parents are also at the London Book Fair. Asking leave of them, Marcus spots George, someone he has read about and seen online. Marcus attempts to speak with the medium, who brushes him off and returns to his hotel. Marcus follows him, standing outside the hotel until nightfall. Eventually George asks him in and agrees to do his reading.

Through George, Jason tells Marcus that he is happy in the afterlife. He instructs Marcus to stop wearing his cap and says it was he who knocked it off his head at the train station. It was used, he says, to keep Marcus from the doomed train but now he must stand on his own. Jason tells him not to fear this "because we are one". As Marcus leaves George, he says he is sorry about "the French woman" as he could tell that "you like her." The last time we see Marcus, he is visiting his mother in a rehab center. She is visibly better and he is not wearing Jasons cap.

Marcus lets George know where Marie is staying. George leaves an anonymous note for Marie, saying he believes her book to be true. She decides to join the anonymous fan for lunch and discovers George. While she is looking for him, George sees a vision of them kissing at the same meeting. Their shared glimpses of the hereafter having made them appreciate this life all the more, George and Marie walk away hand-in-hand.

==Cast==
 
*  , and was cast in Hereafter because Eastwood was so impressed by him.  The original Hereafter production schedule clashed with Damons filming commitments to The Adjustment Bureau, so he emailed Eastwood, suggesting that the director recast the role of George for either Christian Bale, Casey Affleck, Hayden Christensen or Josh Brolin. Instead, Eastwood altered the filming schedule to accommodate Damon, and the actor was able to complete both films. 
* Cécile de France as Marie Lelay, a French television journalist who survives a tsunami.  Dawtrey, Adam (October 20, 2009). " ". guardian.co.uk (Guardian News & Media). Retrieved on November 9, 2009. 
* Frankie and George McLaren as Marcus and Jason, twin brothers. Jason is killed in a car accident early in the film, and Marcus later attempts to contact him in the afterlife.  Eastwood selected the two actors to play the brothers despite them having never acted before because he did not want "child actors whod been over-instructed in Child Acting 101." Svetkey, Benjamin (August 20, 2010). "Fall Movie Preview: Hereafter". Entertainment Weekly (Time Inc): p.&nbsp;65. 
* Lyndsey Marshal as Jackie, Marcus and Jasons mother, a heroin addict. 
* Thierry Neuvic as Didier, Maries lover. Neuvic was on holiday in Corsica in September 2009 when he was called to audition for a role in the film. His audition, which took place at a Paris hotel, lasted 15 minutes, and he read two scenes for Eastwood. Most of Neuvics scenes were filmed in Paris. 
* Jay Mohr as Billy Lonegan, Georges older brother. 
* Bryce Dallas Howard as Melanie, a woman with whom George tries to start a relationship.  
* Mylène Jampanoï  as Reporter Jasmine
* Marthe Keller as a doctor and the director of a hospice in Switzerland who speaks with Marie.
* Derek Jacobi appears as himself.  He reads Charles Dickens Little Dorrit at the London Book Fair.
* Niamh Cusack  as Marcus foster mother
* George Costigan as Marcus foster father
* Richard Kind as Christos Andreou, a wealthy client of Billys who asks Billy for Georges psychic assistance to communicate with his late wife.
* Jean-Yves Berteloot as Michel, Maries publisher.
* Steven R. Schirripa as Carlos, the cooking instructor
* Jenifer Lewis  as Candace, Christos neighbor of whom he tells about Georges psychic reading; she comes to ask for his assistance in contacting her dead child.
*Mathew Baynton as a College Receptionist
*Céline Sallette as Secretary

==Production==
 
  was initially concerned that the low-key ending to the script would put audiences off the film, so Morgan rewrote it to be grander. However, subsequent drafts restored the original ending. Boucher Geoff (September 9, 2010). " ". The Los Angeles Times (Tribune Company). Retrieved on September 11, 2010.  Following its split from   for Warner Bros.. Eastwood was attracted to the script because he was keen to direct a supernatural thriller, and liked how Morgan incorporated real-world events into fiction. Eastwood told LA Weekly, "Theres a certain charlatan aspect to the hereafter, to those who prey on peoples beliefs that theres some afterlife, and mankind doesnt seem to be willing to accept that this is your life and you should do the best you can with it and enjoy it while you’re here, and thatll be enough. There has to be immortality or eternal life and embracing some religious thing. I dont have the answer. Maybe there is a hereafter, but I dont know, so I approach it by not knowing. I just tell the story." Foundas, Scott (December 10, 2009). " ". LA Weekly (Village Voice Media). Retrieved on January 11, 2010. 

Production was based in the   was filmed in a stairwell inside the France Télévisions building. 
 Camberwell New Cemetery.  The room was redressed to represent a fictional Center For Psychic Advancement, which the Marcus character visits.  De France filmed the underwater scene in a studio tank at Pinewood Studios.  After these scenes were shot, production was halted, as Matt Damon was working on another film. Belloni, Matthew; Stephen Galloway (November 30, 2009). " ". The Hollywood Reporter (Nielsen Business Media). Retrieved on December 5, 2009.  

Filming resumed on January 12, 2010; Eastwood filmed scenes with de France for three days on the Hawaiian island of  .  Chatenever, Rick (January 12, 2010). " ". The Maui News. Retrieved on January 12, 2010. 

Production next moved to the San Francisco Bay Area. On January 19, scenes featuring Damon were shot at the California and Hawaiian Sugar Company refinery in Crockett, California, and the exterior of C&H Sugar is seen on screen. The location was not announced until filming had concluded, for fear that large crowds would gather to watch.  Filming also took place in Nob Hill, San Francisco and Emeryville, California|Emeryville. While production was in the Bay Area, it employed 300 local extras and crew members. Production returned to London on January 29 to shoot Damons final scenes.   On February 3, scenes were filmed at the Charles Dickens Museum.  Later in the month, the London Book Fair was recreated inside the Alexandra Palace for a scene featuring George and Marie. Publishers including Random House had their stands built for the three-day shoot, two months before the real London Book Fair took place.  Filming wrapped afterwards. 

Visual effects work was carried out by Los Angeles-based Scanline VFX. 169 effects were created, the key sequence of which was the tsunami, which features "full CG water shots and CG water extensions to water plates, digital doubles, CG set extensions, matte paintings, digital make-up fx and full CG environments with extensive destruction, from toppling digital palm trees to colliding digital cars". Desowitz, Bill (August 27, 2010). " ". AWN.com (Animation World Network). Retrieved on September 4, 2010.  An effect described as the "hereafter effect" also appears, "  the viewer glimpses into the afterlife". 

==Release==
After initial speculation by Variety that the film would be released in December 2010, Warner Bros. announced that Hereafter would go on general release in the United States and Canada on October 22, 2010. McNary, Dave (November 9, 2009). " ". Variety (Reed Business Information). Retrieved on November 11, 2009.  
 The Town Life as We Know It. Hereafter was also screened on October 10, 2010 as the Closing Night Film of the 48th New York Film Festival.  The film was given a limited release on October 15, 2010. 

The film premiered in Japan on February 19, 2011.    11 March 2011 earthquake and tsunami in Japan, the film was withdrawn from all cinemas in that country, two weeks earlier than originally planned.   "Warner Bros. spokesperson Satoru Otani said the films terrifying tsunami scenes were not appropriate at this time."   

===Critical reception===
Hereafter has received mixed reviews from critics. Review aggregate  , another review aggregator, assigned the film a weighted average score of 56/100 based on 41 reviews.  Roger Ebert, however, gave the film four stars (out of four), calling it a film that "considers the idea of an afterlife with tenderness, beauty and a gentle tact. I was surprised to find it enthralling." 

The film received a nomination for the Academy Award for Best Visual Effects, but lost to another Warner Bros. film, Inception.

==References==
 

==External links==
 
*  
*  
*  
*   at Metacritic
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 