Heritage (1984 film)
 
{{Infobox film
| name           = Heritage ( )
| image          = 
| image_size     = 
| caption        = 
| director       = Matjaž Klopčič
| producer       = 
| writer         = Matjaž Klopčič
| starring       = Ivo Ban
| music          = 
| cinematography = Tomislav Pinter
| editing        = Darinka Peršin
| distributor    = 
| released       =     
| runtime        = 109 minutes
| country        = Yugoslavia
| language       = Slovene
| budget         = 
}} Yugoslavian drama film directed by Matjaž Klopčič. It was screened in the Un Certain Regard section at the 1985 Cannes Film Festival.   

==Cast==
* Ivo Ban - Viktor
* Polde Bibič
* Bine Matoh
* Bernarda Oman
* Boris Ostan
* Radko Polič
* Majda Potokar - Mila
* Milena Zupančič

==References==
 

==External links==
* 

 
 
 
 
 
 
 