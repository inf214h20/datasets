Oil and Water (film)
{{Infobox film
| name           = Oil and Water
| director       = D. W. Griffith
| producer       = 
| writer         = Edward J. Montagne
| narrator       =  Harry Carey
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 25 minutes
| country        = United States English intertitles
}} Harry Carey.  A stage dancer (Sweet) and a serious-type homebody (Walthall) discover, after marriage, that their individual styles dont mesh.  The movie includes elaborate dance sequences.

The film was exhibited at the Museum of Modern Art in New York City in July 2007 as part of a Biograph studio retrospective.

==Cast==
* Blanche Sweet - Mlle. Genova
* Henry B. Walthall - The Idealist
* Lionel Barrymore - In First Audience/In Second Audience/Visitor Walter Miller - The Idealists Brother, a Minister
* Clara T. Bracy - The Nurse Harry Carey - Stage Manager/At Dinner
* Gertrude Bambrick - Among Dancers
* Kathleen Butler - In First Audience/Among Dancers
* William J. Butler - Among Dancers John T. Dillon - In Second Audience/At Dinner
* Frank Evans - In First Audience/In Second Audience
* Dorothy Gish - In First Audience
* Lillian Gish - In First Audience
* Robert Harron
* J. Jiquel Lanoe - In First Audience/Among Dancers
* Adolph Lestina - In Second Audience
* Charles Hill Mailes Joseph McDermott - Actor in Play/At Dinner
* W. Chrystie Miller - In First Audience
* Antonio Moreno - Actor in Play
* Alfred Paget - Among Dancers
* Matt Snyder - In First Audience Charles West - In First Audience/In Second Audience

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography
* Blanche Sweet filmography
* Lionel Barrymore filmography

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 