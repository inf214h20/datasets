Two in a Taxi
{{Infobox film
| name           = Two in a Taxi
| image          =
| caption        =
| director       = Robert Florey
| producer       = Irving Briskin
| writer         = 
| starring       = 
| music          =
| cinematography = 
| editing        =
| distributor    = Columbia Pictures
| released       = 10 July 1941
| runtime        = 63 min.
| country        = United States
| language       = English 
| budget         =
}}

Two in a Taxi is a 1941 American film directed by Robert Florey.  Writer Marvin Wald was inspired by seeing a production of Clifford Odets Waiting for Lefty to write this drama of cab drivers and their economic struggles. 

==Cast==
* Anita Louise as Bonnie
* Russell Hayden as Jimmy Owens
* Noah Beery Jr. as Sandy Connors
* Dick Purcell as Bill Gratton
* Chick Chandler as Sid
* Fay Helm as Ethel
* George Cleveland as Gas Station Proprietor
* Ben Taggart as Sweeny
* Paul Porcasi as Herman Henry Brandon as the professor John Harmon as Benny
* James Seay as Cristy Reardon

==External links==
*  

==References==
 

 

 
 
 
 
 
 
 


 