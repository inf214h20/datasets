Hunted (film)
{{Infobox film
| name           = Hunted (a.k.a. The Stranger In Between)
| image          = Hunted1952.jpg
| caption        = Hunted VHS videotape cover
| director       = Charles Crichton
| producer       = Julian Wintle
| writer         = Jack Whittingham and Michael McCarthy
| starring       = Dirk Bogarde Jon Whiteley Elizabeth Sellars Kay Walsh
| music          = Hubert Clifford Eric Cross
| editing        = Gordon Hales and Geoffrey Muller Independent Artists British Film Makers
| distributor    = General Film Distributors
| released       =  
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
 Eric Cross and music by Hubert Clifford.  Hunted can also be seen as an unusual example of the buddy film genre.

The film won the Golden Leopard award at the 1952 Locarno International Film Festival.   

==Plot==
Robbie (Jon Whiteley), an orphaned 6-year-old boy, has been placed with uncaring and harsh adoptive parents in London.  Having accidentally set a small fire in the house, and fearing he will receive severe punishment as he has in the past for misdemeanours, he flees into the London streets.  He finally takes shelter in a derelict bombed-out building, where he stumbles across Chris Lloyd (Dirk Bogarde) and the body of the man Lloyd has just killed &ndash; his employer, who Lloyd had discovered was having an affair with his wife.  Now on the run, and aware that Robbie is the only witness to his crime, Lloyd realises that he will have to get out of London and that he has no option but to take the boy with him.  The film follows the pair as they travel northwards towards Scotland with the police in somewhat baffled pursuit, and charts the developing relationship between the two.  Initially Lloyd regards Robbie dismissively, as an unwanted inconvenience, while Robbie is wary and suspicious of Lloyd.  As their journey progresses however, the pair gradually develop a strong bond of friendship, trust and common cause, with both feeling they have burned their bridges and now have nothing to lose.  They finally reach a small Scottish fishing port, where Lloyd steals a boat and sets sail for Ireland.  During the voyage Robbie falls seriously ill, and Lloyd turns the boat back towards Scotland, where he knows the police are waiting for him.

==Cast==
* Dirk Bogarde as Chris Lloyd
* Jon Whiteley as Robbie
* Elizabeth Sellars as Magda Lloyd
* Kay Walsh as Mrs. Sykes
* Frederick Piper as Mr. Sykes
* Julian Somers as Jack Lloyd
* Jane Aird as Mrs. Campbell Jack Stewart as Mr. Campbell
* Geoffrey Keen as Detective Inspector Drakin
* Douglas Blackwell as Detective Sergeant Grayson   Leonard White as Police Station Sergeant  
* Gerald Anderson as Assistant Commissioner  
* Denis Webb as Chief Superintendent  
* Gerald Case as Deputy Assistant Commissioner  
* John Bushelle as Chief Inspector

==Production==
Jon Whiteley was cast after a friend of Charles Crichton heard him reciting "The Owl and the Pussycat" on radio on The Childrens Hour. He was called in for a screen test and was cast. 

Much of the film was shot on location, with three main areas being used.   Victoria area, which at the time still had derelict corners showing evidence of wartime damage. pottery kilns.  The railway sequence in this section was shot on the now-defunct Potteries Loop Line, and this scene has come to be regarded as historically significant by British railway enthusiasts as it provides a very rare filmic depiction of the long-gone line in operation. 
*Scottish filming took place in the vicinity of the remote coastal village of Portpatrick in the far south-west of the country.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 