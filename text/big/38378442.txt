Kakabakaba Ka Ba?
{{Infobox film
| name           = Kakabakaba Ka Ba?
| image          = 
| image_size     = 
| caption        = Film poster
| director       = Mike De Leon
| producer       = Manuel De Leon Encarnacion De Leon
| writer         = Clodualdo Del Mundo Raquel Villavicencio Mike De Leon
| narrator       = 
| starring       = Christopher De Leon Charo Santos Jay Ilagan Sandy Andolong
| music          = Lorrie Ilustre
| cinematography = Rody Lacap Ike Jarlego, Jr.
| distributor    = LVN Pictures
| released       = 1980
| runtime        = 104 minutes
| country        = Philippines Filipino
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Charo Santos starred as main heroes in the story, while Johnny Delgado and APO Hiking Societys Buboy Garovillo portrayed as main villains.

==Cast==
{| class="wikitable" style="font-size:95%;"
! width="140px" | Actor !! width="150px" | Character 
|-
| Christopher De Leon || Johnny 
| Charo Santos || Melanie 
| Jay Ilagan || Nonong 
| Sandy Andolong || Nancy 
| Boboy Garovillo || Onota 
| Johnny Delgado || Gunmaster sa Japan 
| Armida Siguion-Reyna || Madame Lily 
| Leo Martinez || Padre Blanco 
| Nanette Inventor || Mother Superior 
| Joe Jardi || Goon 
| Moody Diaz || Melody / Virgie 
| Danny Javier || Sampagita 
| George Javier || Grand Master 
| UP Concert Chorus || fake priests and nuns 
|-
|}

The APO Hiking Society made a contribution in this film. While Buboy Garovillo and Danny Javier portrayed the characters of Onota (main villain) and Sampagita (supporting role) respectively, Jim Paredes made a cameo appearance as the conductor for the New Minstrels Band in the finale.

==Awards and Citations==

=== FAMAS Awards===
{| class="wikitable" width=50%
|-
!Year
!Result
!Category/Recipient(s)
|-
|rowspan=1| 1981
| Nominated
|Best Director Mike De Leon  Best Supporting Actor Johnny Delgado  Best Picture
|-
|}

===Urian Awards===
{| class="wikitable" width=50%
|-
!Year
!Result
!Category/Recipient(s)
|-
|rowspan=2| 1981
| Won Ike Jarlego, Jr.  Best Music Lorrie Ilustre  Best Sound Ramon Reyes  Best Supporting Actor Johnny Delgado
|-
| Nominated Clodualdo Del Mundo Jr. Mike De Leon Raquel Villavicencio  Best Production Design Raquel Villavicencio  Best Cinematography Rody Lacap  Best Picture
|-
|}

==See also==
*Mike De Leon
*Christopher De Leon
*Charo Santos
*Jay Ilagan
*Sandy Andolong
*Johnny Delgado
*APO Hiking Society

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 
 