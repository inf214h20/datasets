Dead Wood
 
{{Infobox film
| name           = Dead Wood
| image          = Dead Wood theatrical poster.jpg
| caption        = Theatrical Poster
| director       = Sebastian Smith Richard Stiles David Bryant
| producer       = Sebastian Smith Richard Stiles David Bryant
| writer         = Sebastian Smith Richard Stiles David Bryant
| editor         =
| sound          = 
| starring       = Emily Juniper Fergus March Rebecca Craven Nina Kwok John Samuel Worsey
| music          = Chris Bouchard Adam Langston
| cinematography = Sebastian Smith Richard Stiles David Bryant
| editing        = Sebastian Smith Richard Stiles David Bryant
| studio         = Menan Films
| distributor    = DNC Entertainment 
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Dead Wood is a 2007 British horror film, written, produced and directed by Sebastian Smith, Richard Stiles and David Bryant and starring Emily Juniper, Fergus March, Rebecca Craven, Nina Kwok and John Samuel Worsey with Bryant appearing in a small role.  Dead Wood was shown at film festivals across Italy, the UK and the United States, before being released on DVD throughout Europe and North America in 2009.

== Synopsis ==
Four friends leave the city for a relaxing camping weekend in the woods. Once they get settled, a strange young woman enters their campsite looking for her lost boyfriend. One of the four himself goes missing and the rest are pitted against mysterious forces in a fight for their very survival.

== Cast ==
* Fergus March as Webb
* Emily Juniper as Larri
* John Samuel Worsey as Milk
* Rebecca Craven as Jess
* Nina Kwok as Ketsy
* David Bryant as Rob
* Jay Worthy as Driver
* Leighton Wise as Forestry Foreman

==Recognition==
===Awards and nominations===
The film won the B-Movie Award for Best Digital Effects at the 2007 B-Movie Film Festival in Syracuse, New York.

===Reception===
Reception for the film has been mixed, with Total Sci-Fi Online saying Dead Wood was "Plodding in places and overly reminiscent of Blair Witch in others, Bryant and co. are nevertheless a team to watch."  Dread Central praised the films "strong performances" but criticized Dead Woods "slow beginning".  DVD Talk also reviewed the film, saying Dead Wood "has a near equal number of strengths and weaknesses" but that due to the extras the movie was "a mild recommendation for the horror crowd."  Fatally Yours wrote "despite not liking the first half of the film I admit I am glad to have seen Dead Wood." 

== References ==
 

== External links==
*  
*  
*  

 
 
 
 
 
 
 
 