Keep 'Em Slugging
{{Infobox film
| name           =Keep &#39;Em Slugging
| image          =
| caption        =
| director       = Christy Cabanne
| producer       = Ben Piver
| writer         = Brenda Weisberg (screenplay) Edward Handler (story) Robert Gordon (story)
| narrator       =
| starring       = The Dead End Kids Shemp Howard
| music          = Hans J. Salter Frank Skinner
| cinematography = William Sickner
| editor         = Ray Snyder
| distributor    = Universal Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
}}

Keep &#39;Em Slugging is a 1943 American film starring the Little Tough Guys and directed by Christy Cabanne for Universal Pictures.

==Plot==
Teenage gang leader Tommy Banning is preparing for the Summer vacation by telling his members about the importance of doing their share to help out during the war. The best way to do this, according to Tommys advice, is to end the gang activities and instead take legitimate useful jobs. But this seems to be a greater task than they could imagine, since most gang members have criminal records for juvenile delinquency, and they fail getting regular jobs. When Tommys sister Sheila asks her boss, Frank Moulton, at the Carruthers department store where she works, he agrees to hire Tommy only if she goes on a date with him. Sheila has a boyfriend and wont do that, but her boyfriend Jerry Brady instead gets Tommy the job at the department store. Upon starting his new job, Tommy is smitten by a slaes girl, Suzanne Booker, and they go on a movie date together. At the cinema, some of Tommys gang, Albert "Pig" Gum, String and Ape, turn up and ruin the date. Soon enough Pig, String and Ape all have jobs, the latter two in the same store as Tommy. 
What Tommy and the gang are unaware of is that Moulton is in cahoots with a gangster, Duke Redman, and meet with him to discuss their dealing. It turns out Redman is disappointed in Moulton for not giving him enough business, and to remedy this Moulton give him the names of Tommy and his gang. After using the sexy singer Lola Laverne as bait, Tommy meets with Redman, but refuses to come work for him stealing goods from the department store. Because of this, Tommy is framed for stealing a pice of jewelry and sent to jail. In protest, Sheila quits her job, and it turns out her boyfriend Jerry is the son of the owner. Jerry gets Tommy out of prison, but his family still think he is guilty of the theft. Tommy decides to act against Moulton and Redman, and meets with his gang. After following Moulton to Redmans headquarters, the gang learsn that Redman plans to rob a silk shipment to the department store. Tommy and the gang manage to hold the Redman gangsters enclosed in a room using a fire hose, until the police arrives. As a reward for catching the gang and stopping the robbery, Tommy gets Moultons job at the store, the rest of the gang start working in the shipping department and Jerry and Sheila reconcile. Thus the gang is disbanded and the members all go legitimate. 

==Cast==
===The Dead End Kids===
* Huntz Hall as Pig
* Bobby Jordan as Tommy Banning
* Gabriel Dell as String
* Norman Abbott as Ape

===Additional cast===
* Evelyn Ankers as Sheila Banning
* Elyse Knox as Suzanne
* Frank Albertson as Frank Moulto, store executive
* Don Porter as Jerry
* Shemp Howard as Binky, soda jerk
* Samuel S. Hinds as Carruthers
* Joan Marsh as Lela
* Milburn Stone as Duke Redman

==Notes==
This was the final film in Universals Little Tough Guys series, and although Universal still billed the group as "The Dead End Kids and The Little Tough Guys", none of the Little Tough Guys appeared in this film.

Billy Halop and Bernard Punsley, stars of prior films in the series, were both drafted prior to this film. As a result, their usual roles were respectively taken over by Bobby Jordan and Norman Abbott. With Halop gone, Huntz Hall graduated to top billing for this one film. Similarly, co-star Don Porter was also drafted, but was allowed to complete the film.
 Dave Durand, a member of the concurrent running East Side Kids series, appears as an extra in this film, and Jane Frazee and Robert Paige make uncredited appearances as stars in the movie house film.

An idea of how quickly the picture was shot can be seen in a blooper that found its way into the finished film. When Jordan arrives home from jail, his sister (Ankers) rushes to hug him and trips on the carpet. The actors reactions make it clear that this was not scripted, and Jordan, thinking quickly, ad-libs, "I been meaning to fix that rug."

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 