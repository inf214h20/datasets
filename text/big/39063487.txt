Pierre of the Plains (1914 film)
Pierre of the Plains is a 1914 film co-written by and starring Edgar Selwyn, based upon a novel by Gilbert Parker entitled Pierre and his People. Selwyn adapted the novel into the play Pierre of the Plains in 1908 and it made Elsie Ferguson a Broadway star. Benjamin S. Kutler wrote the scenario. The supporting cast features Dorothy Dalton and Sydney Seaward. Elsie Ferguson appeared in Paramounts version in 1918 under the title Heart of the Wilds, directed by Marshall Neilan.  The play was filmed in 1942, again entitled Pierre of the Plains.

==External links==
 

 
 


 