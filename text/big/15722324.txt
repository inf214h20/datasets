The Jewel of Death
 
{{Infobox film
| name           = The Jewel of Death
| image          =
| director       = Laurence Lamers
| producer       = Laurence Lamers
| writer         = Laurence Lamers Kimbel Bouwman
| starring       = Delia Maier Frank Vincent Ogilvie Albert Blitz Mathias de Koning Arnold Korporaal
| music          = Bernard Herrmann
| cinematography = Jonathan Weyland Joost Meeuwig
| editing        = Wim Louwrier
| sales agent    = Waterwood Films
| premiere       = April, 1992
| runtime        = 16 min.
| country        = The Netherlands
| language       = English
| budget         = $15,000
}} Netherlands English language short fiction film filmed by Laurence Lamers, in 1992.

==Synopsis==
Filmed in a typical forties style, it tells a story of a cursed jewel, which has been stolen; and eventually everyone who tries to possess it is killed

==Cast==
* Delia Maier
* Frank Vincent Ogilvie
* Albert Blitz
* Matthias de Koning
* Arnold Korporaal

==Production==
The film was a student production of the director Laurence Lamers

The music used in the film came from Bernard Herrmann; it was the music used in North by Northwest and some other known films made in the forties/fifties.

The production is estimated on 30.000 guilders, which was at the time the most expensive filmproduction for students in the Netherlands

==Premiere==
The film had its world premiere on April 1992 in the former Odeon Theatre in Amsterdam, The Netherlands.

==Crew==
*Director: Laurence Lamers
*Screenwriters: Laurence Lamers and Kimbel Bouwman
*D.O.P: Jonathan Weyland and Joost Meeuwig
*Editor: Wim Louwrier
*Composer: Bernard Herrmann

==Festivals==
The film was shown at:
*The Dutch Festival (The Netherlands)

==External links==
* 

 
 
 
 
 
 


 
 