Lakshmi Kalyanam (2007 film)
 
{{Infobox film
| name           = Lakshmi Kalyanam
| image          = Lakshmi Kalyanam.jpg
| caption        = Movie Poster Teja
| producer       = K.Mahendra, Naveen Varadarajan (Line Producer)
| writer         =
| starring       = Nandamuri Kalyan Ram Kajal Aggarwal Sayaji Shinde
| music          = R. P. Patnaik Mani Sharma
| cinematography =
| editing        =
| distributor    =
| released       = 15 February 2007
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}} Tollywood action action drama film directed by Teja (film director)|Teja. Nandamuri Kalyan Ram and Kajal Aggarwal played the lead roles. The film released on 15 February 2007 and was declared a Hit at the box office, and was one of the years highest grossers.  This film was dubbed in Hindi as Meri Saugandh and in Tamil as Machakaalai. This film was Kajals Tollywood debut.

==Plot==
Two feuding villages form the backdrop of this story, with Ramu (Kalyan Ram) and Lakshmi (Kajal Agarwal) belonging to one village and villain Giridhar (Ajay (actor)|Ajay) to another. Lakshmi and Ramu are cousins, who grow up to love each other, which is not appreciated by Lakshmis father, who is also the village head.

Meanwhile, Giridhars evil eye falls upon Lakshmi when he chances upon her in college. He writes on her back in blood that he would marry her. This enrages Ramu who thrashes Giridhar only to be taunted to prove his love towards Lakshmi by opening the doors of the temple, which is the bone of contention between the two villages.

==Cast==
* Kalyan Ram as Ramu
* Kajal Agarwal as Lakshmi Ajay as Giridhar
* Sayaji Shinde as Chenchuramaiah
* Srinivasa Reddy as Gopalam Suhasini as Parijatham
* Nagineedu as District Collector
* Prabha
* Subhashini
* Harshavardhan Rallapalli
* Sudhakar
* Raghu Babu 
* Lakshmipati
* Duvvasi Mohan
* Kondavalasa Lakshmana Rao
* J. V. Ramana Murthi
* Narra Venkateswara Rao
* Telangana Shakuntala
* Pavala Shyamala

==Music==
{{Infobox album
| Name = Lakshmi Kalyanam
| Longtype = to Lakshmi Kalyanam
| Type = Soundtrack
| Artist = R. P. Patnaik
| Cover =
| Released = 26 January 2007
| Recorded = 2007 Feature film soundtrack
| Length = 30:07 Telugu
| Label = Madhura Entertainment
| Producer = R. P. Patnaik
| Reviews =
| Last album =
| This album =
| Next album =
}}
Audio of Lakshmi Kalyanam was released on 26 January 2007 at Big 92.7 FM radio station in Hyderabad. Big FM station director Ashwin launched the audio and gave the first unit to music director RP Patnaik. This function was also graced by Swapna (programming director of BIG FM), Jeethi (producer), Nihal, Pranathi and Malavika. Madhura Entertainment bought the audio rights. 
{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 30:07
| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = Avva Avva
| note1           =
| lyrics1         = Chinni Charan Malavika
| length1         = 04:39
| title2          = Bava Bava
| note2           = 
| lyrics2         = Ramajogayya Sastry
| extra2          = Shankar Mahadevan, Sujatha Mohan
| length2         = 04:38
| title3          = Aligava
| note3           =
| lyrics3         = Ramajogayya Sastry
| extra3          = Nihal, Pranavi
| length3         = 04:06
| title4          = Chinni Chintakaya
| note4           =
| lyrics4         = Ramajogayya Sastry
| extra4          = Master Rohith
| length4         = 04:24
| title5          = Gunde Gontulo
| note5           =
| lyrics5         = Ramajogayya Sastry
| extra5          = KK (singer)|K.K.
| length5         = 04:38
| title6          = Labjanaka
| note6           =
| lyrics6         = Ramajogayya Sastry
| extra6          = R. P. Patnaik, Ravi Varma, Raghu Babu, Harsha, Kondavalasa Lakshmana Rao|Kondavalasa, Duvvasi, Geetha Madhuri
| length6         = 04:58
| title7          = Rathi Gunde
| note7           =
| lyrics7         = Ramajogayya Sastry
| extra7          = Vandemataram Srinivas
| length7         = 02:44
}}

==References==
 

==External links==
*  

 

 
 
 
 