Mother Carey's Chickens (film)
{{Infobox film
| name           = Mother Careys Chickens
| image          =
| caption        = James Anderson (assistant)
| producer       = Pandro S. Berman
| writer         = S.K. Lauren Gertrude Purcell Anne Shirley Ruby Keeler
| music          =
| cinematography = J. Roy Hunt
| editing        = George Hively
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States English
| budget         = $358,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56 
| gross          = $703,000 
}} 1938 drama Anne Shirley and Ruby Keeler. The film was directed by Rowland V. Lee and based upon a 1917 play by Kate Douglas Wiggin and Rachel Crothers, which in turn was adapted from Wiggins novel of the same title.

Originally Katharine Hepburn was assigned to the lead role. She refused, however, and left RKO in order to avoid having to appear in the film.

In 1963, Walt Disney released Summer Magic, a loose remake of this film, with Hayley Mills as Nancy Carey.

==Plot==
Mr. Carey (Ralph Morgan), a captain in the United States Navy, dies during the Spanish-American War. His wife Margaret, daughters Nancy and Kitty and sons Gilbert and Peter are left behind. They are now on their own with only Capt. Careys pension for income. The family moves into a series of ever-smaller rented houses while Mrs. Carey works in a textile mill. When she is injured, they lease a broken down mansion for a year at a nominal fee, and invest the captains small life insurance payment to fix it up into a boarding house. Both daughters fall in love, Kitty with a local teacher and Nancy with Tom Hamilton (Frank Albertson), the son of the absentee owner.

When the Hamiltons put the house up for sale, the family is given an eviction order by Tom Hamilton, a doctor who wants the money from the sale to study in Europe. However fate intervenes and Tom saves Peter from a serious illness, then falls in love with Nancy. The new owners, the Fullers, move in to force the family to vacate. The Careys and their beaus try to avoid being put out of their house by scaring off the Fullers. 

==Cast== Anne Shirley - Nancy Carey
*Ruby Keeler - Katherine Kitty Carey James Ellison - Ralph Thurston
*Fay Bainter - Mrs. Margaret Carey
*Walter Brennan - Mr. Ossian Popham
*Donnie Dunagan - Peter Carey
*Frank Albertson - Tom Hamilton Jr.
*Alma Kruger - Aunt Bertha
*Jackie Moran - Gilbert Carey Margaret Hamilton - Mrs. Pauline Fuller
*Virginia Weidler - Lally Joy Popham
*Ralph Morgan - Captain John Carey

==Reception==
The film earned a profit of $110,000. 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 


 