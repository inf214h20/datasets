Three Men on a Horse (film)
{{Infobox film
| name           = Three Men on a Horse
| image_size     = 
| image	=	Three Men on a Horse FilmPoster.jpeg
| caption        = 
| director       = Mervyn LeRoy (uncredited)
| producer       = Mervyn LeRoy
| writer         = George Abbott (play) John Cecil Holm (play) Laird Doyle
| starring       = Frank McHugh Howard Jackson
| cinematography = Sol Polito
| editing        = Ralph Dawson
| distributor    = Warner Brothers
| released       = November 21, 1936
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1936 comedy Broadway Three play of the same name written by George Abbott and John Cecil Holm. A mild-mannered greeting card poet has the uncanny ability to pick winners in horse races.

==Plot== Paul Harvey), unappreciative boss, greeting card publisher J.G. Carver (Guy Kibbee), and the lack of support of his wife Audrey (Carol Hughes). Erwin goes on a drinking binge and ends up in a hotel bar.

When he overhears Charlie (Allen Jenkins), and Frankie (Teddy Hart) bemoaning their bad luck betting on horses, he gives them his pick for the next race. They ignore the drunk, but when Erwin proves right, their friend Patsy (Sam Levene) decides to place the little money they have left on Erwins next choice. It wins, as do all his other selections that day, making the jubilant trio rich. They decide to hang onto their newfound goldmine.

Patsy tries to do Erwin a good turn by calling his boss and demanding a raise for him as his "manager", but the plan backfires; Carver fires Erwin. Erwin is upset, but Patsy gives him 10% of the winnings and convinces him that he can make more money betting than writing greeting card poems.

The next day, Erwin makes his picks, but discards his first choice for the sixth race. This arouses Charlies suspicions. His fear about being double crossed infects the other two. Patsy forces Erwin to bet all his winnings on his second selection for that race. However, Erwin had told Patsys girlfriend Mabel (Joan Blondell) that he never bet on the horses because he was worried that doing so would take away his ability.

The whole gang go to the racetrack to watch the race. Erwins discarded choice wins, barely edging his second pick. When they get back to the hotel, Patsy starts beating Erwin. Then they hear on the radio that the winner has been disqualified. This inspires Erwin to punch Patsy back. When Clarence shows up, Erwin punches him too. Following close behind, Carver (desperate to fulfill a contract for Mothers Day cards) offers Erwin a raise and a new office, which Audrey persuades him to take. Erwin bids the gamblers goodbye, telling them that having finally bet for real, he has lost his knack.

==Cast==
*Frank McHugh as Erwin Trowbridge
*Joan Blondell as Mabel, Patsys ditsy girlfriend
*Guy Kibbee as J.G. Carver Carol Hughes as Audrey Trowbridge
*Allen Jenkins as Charlie
*Sam Levene as Patsy. Levene played the same role in the stage play.
*Teddy Hart as Frankie
*Edgar Kennedy as Harry the bartender Paul Harvey as Clarence Dobbins Eddie Anderson as Moses the elevator operator
*Virginia Sale as Chambermaid Harry Davenport as Williams
*Ottola Nesmith as Miss Hillary (as Tola Nesmith)
*Eily Malyon as Miss Burns

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 