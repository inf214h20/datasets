Three Modern Women
{{Infobox film
| name           = Three Modern Women
| image          = 
| caption        = 
| director       = Bu Wancang
| producer       = 
| writer         = Tian Han Chen Yanyan Li Zhuozhuo Jin Yan
| music          = 
| cinematography = 
| editing        = 
| studio         = Lianhua Film Company
| distributor    = 
| released       =  
| runtime        =  China
| language       =
| budget         = 
| gross          = 
}}

Three Modern Women ( ) is a 1932 Chinese film directed by Bu Wancang and written by Tian Han.    {{cite book
 | last    = Kuoshu
 | first      = Harry H.
 | url = http://books.google.com/books?id=bGHBX6V7IKwC&pg=PA110
 | year        = 2002
 | page        = 110
 | title       = Celluloid China: Cinematic Encounters with Culture and Society
 | publisher   = Southern Illinois University Press
 | ISBN          = 978-0-8093-2456-9 }}  The film tells a story about the romantic relationships between a movie star and three women representing three archetypes of contemporary women. Released by the Lianhua Film Company, it was highly popular and won praise from left-wing critics. 

== Cast ==
*Ruan Lingyu Chen Yanyan
*Li Zhuozhuo
*Jin Yan

==Production==
Ruan Lingyu by the time was known for her classical romantic roles as downtrodden helpless females, and was initially not cast because the lead role did not fit her image. She was determined to be in the film and approached and convinced the director to assign her. 

==See also==
*New Women – 1935 film directed by Cai Chusheng
*Women Side by Side – 1949 film directed by Chen Liting
*List of Chinese films of the 1930s

== References ==
 

 
 
 
 
 
 
 
 
 
 
 