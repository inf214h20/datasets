Final Justice (1988 film)
 
 
{{Infobox film
| name           = Final Justice
| image          = FinalJustice1988.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 霹靂先鋒
| simplified     = 霹雳先锋
| pinyin         = Pī Lì Xiān Fēng
| jyutping       = Pik1 Lik1 Sin1 Fung1 }}
| director       = Parkman Wong
| producer       = Danny Lee
| writer         = 
| screenplay     = James Fung
| story          = Chung Hon Chiu
| based on       =  Danny Lee Stephen Chow
| music          = The Melody Bank
| cinematography = Choi Wai Kei
| editing        = Robert Choi
| studio         = Magnum Films
| distributor    = 
| released       =  
| runtime        = 95 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$8,916,612
}}
 1988 Cinema Hong Kong Danny Lee (also producer) and Stephen Chow. Final Justice is Chows debut film debut, playing in a dramatic role, which netted him the Golden Horse Awards for Best Supporting Actor. Chow would later go on to be the top comedy superstar in Hong Kong.

==Plot== Danny Lee) of the Wan Chai District has been a pioneer in the area, often establishing outstanding services. However, Cheungs staunch handling style, has attracted many complaints towards him. Cheungs new superior, Chief Inspector Lo Tai Wai (Ricky Wong) is dissatisfied with him, accusing him of disobeying superior orders, while also frequently clamped by Cheung.

One day, criminal Judge (Shing Fui-On) is released from prison, and leads his former associates Bull (Tommy Wong), Chicken (William Ho) and Smut (Victor Hon) to prepare a major crime spree plan. Meanwhile, car thief Boy (Stephen Chow) regards Judge as his idol and willingly serves him,  but in the course of a car theft incident, he is arrested by Cheung.

Unexpectedly, Judge and his associates use a stolen car to rob an illegal underground casino, killing some customers in the process. Boy is innocently involved in the robbery case. Moreover, Lo regards Boy as an accomplice to the robbery and charges him for robbery and murder. However, Cheung believes that Boy is innocent and is determined to find new evidence to overturn the false allegations.

Finally, Cheung discovers that Judge and his crew are planning to kidnap a wealthy tycoon.

==Cast== Danny Lee as District Sergeant Cheung Tit Chu
*Stephen Chow as Boy
*Shing Fui-On as Judge
*William Ho as Chicken
*Tommy Wong as Bull
*Victor Hon as Smut
*Ricky Wong as Chief Inspector Lo Tai Wai
*Ken Lo as Kwong
*Chiu Jun Chiu as Sergeant Chiu
*Wong Ang as Pickpocket
*Debbie Chui as Boys girlfriend
*Stephen Chang as Mr. Cheng
*Hung Tung Kim as Kim
*Shing Fuk On as Dummy
*Ng Yuen Sam as Lift passenger
*Chan Ging as Truck driver
*Law Shu Kei as Schoolmaster
*James Ha as Boli
*Parkman Wong as Ambulance attendant 
*Strawberry Yeung as Nancy
*Chan Chi Hung as Hostage
*Ernest Mauser as Hostage
*Ho Chi Moon as Gambling joint customer
*Yee Tin Hung as Dummys thug
*Cheung Kwok Leung as Dummys thug
*Lam Chi Tai as Dummys thug
*Fei Pak as Policeman
*Lee Wah Kon as Gambling joint customer
*Lam Foo Wai as Dummys thug
*Tong Pau Chung

==Box office==
The film grossed HK$8,916,612 at the Hong Kong box office during its theatrical run from 23 June to 13 July 1988 in Hong Kong.

==Awards and nominations==
*8th Hong Kong Film Awards Best Supporting Actor (Stephen Chow) Best New Performer (Stephen Chow)

*25th Golden Horse Awards
**Won: Best Supporting Actor (Stephen Chow)

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 
 
 
 
 
 
 
 
 
 
 
 
 