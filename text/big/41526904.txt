Flatfoot in Africa
{{Infobox film
 | name = Piedone lafricano (Flatfoot in Africa)
 | image = Flatfoot in Africa.jpg
 | caption = Steno
 | writer = Steno (director)|Steno, Giovanni Simonelli, Franco Verucci
 | starring =  Bud Spencer, Enzo Cannavale, Dagmar Lassander
 | music =  Guido & Maurizio De Angelis
 | cinematography = Alberto Spagnoli 
 | editing =  Mario Morra
 | producer = Laser Rialto Film
 | distributor = Titanus, Medusa Film country = Italy - South Africa
| runtime = 108 min
 | language =  Italian
 | budget =
 }} 1978 Cinema Italian "Poliziotteschi|poliziottesco"-comedy film directed by Steno (director)|Steno. It is the third and penultimate chapter in the "Flatfoot" film series.       

== Plot summary ==
A trail of illicit diamonds takes Flatfoot and his acquaintance, Naples police commissioner Caputo, from Johannesburg to Swakopmund in the hopes of breaking up a South African smuggling ring. They are joined by Bodo, an African child, and confounded in their search by corrupt mining officials and an antagonistic inspector in the South-West African police.

== Cast ==

* Bud Spencer: Insp. Flatfoot Rizzo 
* Enzo Cannavale: Caputo
* Werner Pochath: Spiros
* Joe Stewardson: Smollet
* Carel Trichardt: captain Muller
* Dagmar Lassander: Margy Connors
* Desmond Thompson: Inspector Desmond
* Baldwin Dakile: Bodo
* Antonio Allocca
* Giovanni Cianfriglia
==References==
 

==External links==
* 

 
 
 
 
  
 
 
 
 
 
 
 
 
 
 

 
 