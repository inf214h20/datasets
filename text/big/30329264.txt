Free Man (film)
{{Infobox film
| name           = Free Man
| image          = FreeManTheatricalPoster.jpg
| alt            =
| caption        = Theatrical Poster
| director       = Mehmet Tanrısever
| producer       = Mehmet Tanrısever
| writer         =   
| starring       =   
| music          =  
| cinematography = Ali Özel
| editing        = Mevlüt Koçak
| studio         = Feza Film
| distributor    = Özen Film
| released       =  
| runtime        = 163 minutes
| country        = Turkey
| language       = Turkish
| budget         =
| gross          = US$3,793,421
}}
Free Man ( }}) is a 2011 Turkish biographical film, co-written, produced and directed by Mehmet Tanrısever, starring Mürşit Ağa Bağ as controversial Muslim scholar Said Nursî. The film, which opened on   at number 2 in the Turkish box office, had a gala premiere on   at the Türker İnanoğlu Maslak (TİM) Show Center in Istanbul.      

==Production==
Director Mehmet Tanrısever returned to filmmaking after a 20-year break to make the film, stating that, "I first started filmmaking because I wanted to express an idea. Then many years later I took up filmmaking again for the same reasons. I think it’s important to keep up efforts like this." "So, I liquidated my business and spent time making movies. I ended up making this movie." 

"I actually wanted to make this movie 20 years ago, but I came across some spiritual obstacles." Tanrısever stated in an interview, "One of the screenwriters, Mehmet Uyar, saw Said Nursi in his dream. The esteemed scholar dismissed our friend, saying: Now is not the time. Don’t you know me?" "I believed in the power of these obstacles and decided not to make the movie." However, he continued, "After 20 years, we decided to try again." 

The film was shot in eight weeks out of a total production time of fourteen weeks and the director, pleased with the result, stated that it, "is one of the top five movies among period dramas shot in Turkey. In fact, Hür Adam is perhaps the best one. I look at other period pieces that have been made, and when I compare my movie with them, I would say it is the best. I know what I did, what I produced. I cannot be modest about this." 

Ankara Chief Public Prosecutor’s Office launched an investigation into the production following a complaint by lawyer Ömer Ediz Yoruz, who alleged that the film "insults the spiritual personality of Atatürk", which is a criminal offence in Turkey. Filmmaker Gani Rüzgar Şavata also filed a complaint against the director and screenwriters for "using the screenplay, for which he holds part of the copyright, without his consent". 

==Plot==
Muslim Kurdish scholar Said Nursî passes through three phases, known to his followers as the "Old Said", the "New Said" and the "Third Said", during which time his writings, consisting of letters to his students about faith and religious philosophy, result in Risale-i Nur Collection, a body of Quranic commentary exceeding 6,000 pages.

==Release==
The film opened on nationwide general release in 238 screens across Turkey on   at number 2 in the national box office with a first weekend gross of US$1,228,126.   

==Reception==
===Box Office===
The film was number 2 in the Turkish national box office for two weeks running and has made a total gross of US$3,793,421. 

===Reviews===
Director Mehmet Tanrısever responded to Turkish film critics, who slammed the movie for its rather lengthy run at 160 minutes, its being a low-quality propaganda film and a storyline that lacks energy and frequently falls into repetition, by stating that, "The debates show that we did a good thing. It means that some truths are being exposed, so its being talked about. Good results come out of debates; it is rather worrying when something is not being talked about, as that would mean theres no democracy there". 

==Awards==

Free Man has been nominated at Oaxaca Film Fest.

==See also==
* 2011 in film
* Turkish films of 2011

==References==
 

==External links==
*    
*  

 
 
 
 
 
 
 