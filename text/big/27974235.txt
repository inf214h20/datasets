The Dry Land
 
{{Infobox film
| name            = The Dry Land
| image           = The Dry Land.jpg
| producer        = Heather Rae
| director        = Ryan Piers Williams
| writer          = Ryan Piers Williams
| released        =  
| country         = United States
| language        = English
| runtime         = 92 minutes
| starring        = Ryan O’Nan America Ferrera Wilmer Valderrama Jason Ritter Melissa Leo
| distributor     = Maya Entertainment Freestyle Releasing 
| gross           = $11,777 
}}
The Dry Land, or American Tragic, is a drama film, directed and written by Ryan Piers Williams. It opened worldwide on July 30, 2010.

==Plot==
 
James (Ryan ONan) returns from Iraq to face a new battle returning to his small-town life in Texas. His wife (America Ferrera), his mother (Melissa Leo), and his friend (Jason Ritter) provide support, but they cant fully understand the pain and suffering he feels since his tour of duty ended. Lonely, James reconnects with an army buddy, Raymond (Wilmer Valderrama), who provides him with compassion and friendship during his battle to process his experiences in Iraq. But their reunion also exposes the different ways that war affects people, at least on the surface. This moving story of redemption and reconstruction extends beyond a post-traumatic-stress-disorder narrative. ONans performance is moving as he explores the depths of his internal struggle; Ferrera fearlessly tackles her role of a young wife in turmoil. The Dry Land is about one mans fight within his own terrain, his country, his home, and his mental health. He fights to rebuild what he lost.

==Cast==
*America Ferrera as Sarah
*Jason Ritter as Michael
*Ethan Suplee as Jack
*Wilmer Valderrama as Raymond Gonzales
*Melissa Leo as Martha
*June Diane Raphael as Susie Evan Jones as Joe Davis
*Ana Claudia Talancón as Adriana
*Sasha Spielberg as Sally
*Ryan ONan as James
*Barry Shabaka Henley as Colonel Stephen Evans Benito Martinez as David Valdez
*Diego Klattenhoff as Henry
*Misty Upham as Gloria
*Jenny Gabrielle as Tina
*Jeremiah Bitsui as Luis

==Reception==
The film received generally mixed to positive reviews. Review aggregator Rotten Tomatoes reports that 61% of 23 critics gave the film a positive review, with a rating average of 5.4/10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 47 based on 10 reviews signifying "mixed or average reviews". 

Entertainment Weekly gave the film a "B" grade and praised Ryan ONans "quietly riveting performance as an Iraq-war veteran who comes undone after he returns home to dusty Texas (the filmmakers home turf)". 

The film received Imagen Awards nominations for best feature film and for America Ferrera as best actress. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 