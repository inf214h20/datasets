Daylight Fades
 
{{Infobox film
| name = Daylight Fades
| image = Daylight Fades.jpg
| director = Brad Ellis
| writer = Allen Gardner
| starring = Matthew Stiller Rachel Miles Allen Gardner
| released =  
| country = United States
| language = English
| studio = School Pictures
}}
Daylight Fades  is a 2010 feature film produced by Memphis, Tennessee|Memphis-based Old School Pictures in association with Southern Pics, LLC, written by Allen C. Gardner and directed by Brad Ellis. It stars Matthew Stiller as Johnny and Rachel Miles as Elizabeth. Principal photography was completed after a 38-day shoot in January and February 2009. The film premiered in Memphis on June 15, 2010. 

==Plot==
Elizabeth, a tough, jaded young woman, and Johnny, a shy, kind-hearted young man, meet at a  .

Struggling to come to terms with their past, present, and each other, Elizabeth, Johnny, and Seth must do nothing less than discover who they truly are and what theyre really capable of.  Daylight Fades is a film that explores  , and the bonds that can keep us together or pull us apart.  Its a story about people who are desperate for a second chance, realizing it may be their last.

==Cast==
*Matthew Stiller- Johnny
*Rachel Miles- Elizabeth
*Allen C. Gardner- Seth
*Rachel Kimsey- Raven
*Clare Grant- Shauna
*Billy Worley- Rick

==References==
 
*The Memphis Flyer: 
*MemphisConnect.com: 
*The Daily Helmsman: 
*Live From Memphis: 
*"Chasing Daylight:The Making of Daylight Fades" on Live From Memphis: 
*"Chasing Daylight:  The Making of Daylight Fades" on Indie Memphis: 
*Live From Memphis on "Chasing Daylight:  The Making of Daylight Fades": 

 

==External links==
*  
*  

 
 
 