Popular Science (film)
 
 
Popular Science (1935–1949) was a series of short films, produced by Jerry Fairbanks and released by Paramount Pictures. 

The Popular Science film series is a Hollywood entertainment production - the only attempt by the movie industry to chronicle the progress of science, industry and popular culture during the first half of the 20th Century. 
 Telephone Answering Northrop "Flying Wing" (1948). The series also promoted Paramount with a tour (1938) of the then-new Fleischer Studios facility in Miami, Florida, which produced animated cartoons for Paramount.
 Popular Science magazine, the series introduced its audience to advances in medicine, aviation, science and technology, television, home improvement, airplane|planes, trains and automobiles, as well as an assortment of strange and whimsical inventions.

During its 14-year theatrical run, the Popular Science film series was honored with numerous awards and acclaim, including 5 Academy Award nominations. The Popular Science series also received a Special Commendation from the US Department of War in 1943 for its unparalleled coverage of American military technology involved World War II. 

This film series has been a staple on television for decades, most recently shown on the American Movie Classics cable network, hosted by Nick Clooney and Bob Dorian. The series, as well as the rest of the Jerry Fairbanks film library, is owned by Shields Pictures.

==External links==
* 
* 
* 

 
 
 
 
 




 