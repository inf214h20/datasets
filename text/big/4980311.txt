Masoom (1983 film)
 
{{Infobox film
| name           = Masoom
| image          = Masoom 1983.jpg
| image_size     =
| caption        = DVD cover
| director       = Shekhar Kapur
| producer       = Chanda Dutt Devi Dutt 
| screenplay     = Gulzar
| based on       =  
| narrator       =
| starring       = Naseeruddin Shah Shabana Azmi
| music          = R D Burman
| cinematography = Pravin Bhatt
| editing        = Aruna Raje  Vikas Desai
| distributor    = Bombino Video Pvt. Ltd.
| released       =  
| runtime        = 165 minutes
| country        = India
| language       = Hindi
| budget         =
}} 1983 Cinema Indian drama film and directorial debut of critically acclaimed filmmaker Shekhar Kapur.  An adaptation of Man, Woman and Child, 1980 novel by Erich Segal.   the film is a coming-of-age story starring Naseeruddin Shah and Shabana Azmi in lead roles along with Tanuja, Supriya Pathak and Saeed Jaffrey. It features Jugal Hansraj, Aradhana and Urmila Matondkar as child actors. The screenplay, dialogues and lyrics are written by Gulzar and the music is contributed by R.D. Burman. 

Shekhar Kapur, the director of the movie, went on to make several other internationally acclaimed movies like Bandit Queen, Mr. India and Elizabeth (film)|Elizabeth.

==Plot==
Indu (Shabana Azmi) and DK (Naseeruddin Shah) have a happy marriage and two daughters &mdash; Rinky and Minni, and live in Delhi. The tranquility of their life is shattered when DK receives word that he has a son from a short-term affair with Bhavana (Supriya Pathak) during his visit to Nainital when his wife Indu was about to give birth to their first child Rinky. Bhavana did not tell DK about this since she did not want to disturb his life. Now that she is dead, her guardian Masterji sends word to DK in Delhi saying that the boy, Rahul (Jugal Hansraj), who is eight years old, needs a home. Despite the objections of Indu, who is devastated to learn of her husbands infidelity, the boy comes to stay with them in Delhi for some time. Rahul is never told that DK is his father. Rahul bonds with DK and his daughters. But Indu cant bear to look at him, who is a tangible reminder of DKs betrayal.

DK, worried by the effect that Rahul is having on his family, decides to put him in a boarding school in St. Josephs College, Nainital; Rahul accepts with reluctance. After gaining admission at the school and returning to Delhi before his permanent move to Nainital, Rahul figures out that DK is his father and runs away from home. After he is escorted home by a police officer, Rahul confesses his awareness of the identity of his father to Indu. Indu is unable to bear the heartbreak of the young child and intercepts Rahul before he is put onto the train to Nainital, thereby accepting him into the family and wholeheartedly forgiving DK.

==Cast==
* Naseeruddin Shah as D.K. Malhotra (DK) 
* Shabana Azmi as Indu Malhotra 
* Supriya Pathak as Bhavana 
* Jugal Hansraj as Rahul 
* Urmila Matondkar as Rinky 
* Aradhana Srivastav  as Minni  {{cite web | title = Mini from Shekhar Kapurs Masoom  traced in Delhi | url = http://timesofindia.indiatimes.com/entertainment/hindi/bollywood/news/Mini-from-Shekhar-Kapurs-Masoom-traced-in-Delhi/articleshow/29976112.cms 
|author=Vickey Lalwani|date= Feb 7, 2014| accessdate = 2014-09-07 | publisher = The Times of India}}  
* Tanuja as Chanda 
* Saeed Jaffrey as Suri 
* Paidi Jairaj as Master Ji
* Satish Kaushik as Tiwary Ji

==Soundtrack==
Lyrics for all songs of Masoom were penned by Gulzar and music was composed by R.D. Burman.
{{Track listing
| extra_column = Singer(s)
| title1 = Do Naina Aur Ek Kahani | extra1 = Aarti Mukherjee | length1 =  Bhupinder Singh, Suresh Wadkar | length2 = 
| title3 = Tujhse Naraz (male) | extra3 = Anup Ghoshal | length3 = 
| title4 = Tujhse Naraz (female) | extra4 = Lata Mangeshkar | length4 = 
| title5 = Lakdi ki Kathi | extra5 = Vanita Mishra, Gauri Bapat, Gurpreet Kaur | length5 = 
}}

== Awards ==
National Film Award for best female playback singer - Aarti Mukherji
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient !! Result
|- Best Film Critics || ||  
|- Best Actor || Naseeruddin Shah ||  
|- Best Music Director || R.D. Burman ||  
|- Best Lyricist || Gulzar for "Tujhse Naraaz Nahin" ||  
|- Best Female Playback Singer || Aarti Mukherji for "Do Naina Ek Kahani" ||  
|- Best Film || ||  
|- Best Director || Shekhar Kapur||  
|- Best Actress || Shabana Azmi||  
|-
|}

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 