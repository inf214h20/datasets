Nōnai Poison Berry
{{Infobox animanga/Header
| name            = Nōnai Poison Berry
| image           = 
| caption         = 
| ja_kanji        = 脳内ポイズンベリー
| ja_romaji       = Nōnai Poizun Berii
| genre           = romance manga|Romance, slice of life  
}}
{{Infobox animanga/Print
| type            = manga
| title           = 
| author          = Setona Mizushiro
| illustrator     = 
| publisher       = Shueisha
| publisher_en    = 
| demographic     = josei manga|Josei
| imprint         = 
| magazine        = Cocohana|Chorus/Cocohana
| magazine_en     = 
| published       = 
| first           = December 28, 2009
| last            = 
| volumes         = 4
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = 
| director        = Yūichi Satō
| producer        = 
| writer          = 
| music           = 
| studio          = 
| licensee        = 
| released        =  
| runtime         = 
}}
  romance josei manga|josei manga series written and illustrated by Setona Mizushiro.  It started serialization in 2009 in Cocohana|Chorus manga magazine  and 4 tankōbon volumes have been published so far. It will be adapted into a live action film that is scheduled for release in 2015.   

==Characters==
*Ichiko Sakurai
:Played by Yōko Maki (actress)|Yōko Maki   
*Saotome
:Played by Yūki Furukawa 
*Ochi
:Played by Sonha 
*Chairman Yoshida  Hidetoshi Nishijima 
*Ishibashi 
:Played by Ryūnosuke Kamiki 
*Ikeda 
:Played by Yō Yoshida 
*Hatoko
:Played by Hiyori Sakurada 
*Secretary Kishi
:Played by Kazuyuki Asano 

==Volumes==
*1 (May 19, 2011) 
*2 (July 25, 2012) 
*3 (August 23, 2013) 
*4 (September 25, 2014) 
*5 (TBA) 

==Reception==
It was number twenty on the 2013 Kono Manga ga Sugoi! Top 20 Manga for Female Readers survey.  It was also a Jury Selection in the Manga Division at the 17th Japan Media Arts Festival Awards. 

==References==
 

==External links==
*   

 
 
 

 
 
 
 
 
 
 
 
 
 


 
 