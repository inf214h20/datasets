Jeena Marna Tere Sang
{{Infobox film
| name           = Jeena Marna Tere Sang
| image          = JeenaMarnaTereSang.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional Poster
| director       = Vijay Reddy	 	
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sanjay Dutt Raveena Tandon
| music          = Dilip Sen - Sameer Sen
| cinematography = Harmeet Singh
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Vijay Reddy. It stars Sanjay Dutt and Raveena Tandon in pivotal roles.

==Cast==
* Sanjay Dutt
* Raveena Tandon
* Javed Jaffrey
* Sadashiv Amrapurkar
* Paresh Rawal
* Aruna Irani
* Annu Kapoor
* Avtar Gill
* Asrani
* Reema Lagoo

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chaha Hai Tumhein Chahenge Saanson Mein Bas Jayeng"
| Kumar Sanu, Anuradha Paudwal
|-
| 2
| "Kal Maine Khuli Aankh Se Ek Sapna Dekha"
|  Abhijeet Bhattacharya|Abhijeet, Anuradha Paudwal
|-
| 3
| "Tumse Hai Kita Pyar Ye Hum Keh Nahin Sakte"
| Vipin Sachdeva, Anuradha Paudwal
|-
| 4
| "Aaj Dil Ki Baatein Keh Denge Hum Sabhi"
| Babla Mehta, Anuradha Paudwal
|-
| 5
| "Tune Zamane Ye Kya Kar Diya"
| Vipin Sachdeva, Anuradha Paudwal
|-
| 6
| "Kala Doriya"
| Anuradha Paudwal
|-
| 7
| "Dil Ek Mandir Pyar Hai Pooja"
| Anuradha Paudwal
|-
| 8
| "Jo Sache Premi Hain"
| Vipin Sachdeva, Anuradha Paudwal
|-
| 9
| "Tere Liye Laya Hoom Main Lal Lal Chunariya"
| Mohammed Aziz, Anuradha Paudwal
|}

==External links==
* 

 
 
 

 