God Grew Tired of Us
{{Infobox Film | name = God Grew Tired of Us
  | image = GodGrewTiredOfUs.jpg
  | caption = Promotional poster for God Grew Tired of Us
  | director = Christopher Dillon Quinn

  | executive producer = Peter Gilbert Brad Pitt Steven Rosenbaum Adam Schlesinger Jack Schneider
  | co-producer = Eric Gilliland Catherine Keener Dermot Mulroney
  | producer = Molly Bradford Pace Christopher Dillon Quinn Tom Walker
  | writer = 
  | starring = John Dau Nicole Kidman Daniel Abul Pach Panther Bior
  | music = Gary Calamar Jamie Saft
  | cinematography = Bunt Young
  | editing =  Johanna Giebelhaus Geoffrey Richman
  | distributor = 
  | released =  
  | runtime =  86 minutes English
  | budget = 
  }}
God Grew Tired of Us is a 2006 documentary film about three of the "Lost Boys of Sudan", a group of some 25,000 young men who have fled the wars in Sudan since the 1980s, and their experiences as they move to the United States. The film was written and directed by Christopher Dillon Quinn.

==Synopsis==
God Grew Tired Of Us chronicles the arduous journey of three young Southern Sudanese men, John Bul Dau, Daniel Pach and Panther Bior, to the United States where they strive for a brighter future. As young boys in the 1980s, they had walked a thousand miles to escape their war-ridden homeland, and then had to make another arduous journey to escape Ethiopia.
 shock of being thrust into the economically intense culture of the United States, learning new customs, adapting to new and strange foods, coping with the  ordeal of getting, and keeping a job, or multiple jobs, while never forgetting the loved ones they left behind in Africa. They dedicate themselves to doing whatever they can to help those they left behind in Kakuma, and to discovering the fate of their parents and family.

The title comes from a statement by John, in expressing that he thought the suffering and killings he saw during his countrys civil war may have been the final judgment on the earth spoken of in the Bible, because "God was tired of us," "tired of the of bad things the people were doing."

God Grew Tired Of Us was produced, written and directed by Christopher Dillon Quinn and narrated by Nicole Kidman; the executive producer was Brad Pitt.  The title of the documentary is a quote from John Dau discussing the despair he and other Sudanese felt during the civil war.   

==Awards==
At the 2006 Sundance Film Festival, the film won both the "Grand Jury Prize: Documentary" and the "Audience Award" in the "Independent Film Competition: Documentary" category.

The film also won best documentary at the Deauville Film Festival in France and the Galway Film Festival in Ireland.

Christopher Dillon Quinn was awarded The Emerging Documentary Filmmaker Award by the International Documentary Association in 2007 for directing God Grew Tired of Us.

==Music== Gigi
* Gua – Emmanuel Jal
* Mai Wah Ihtagab – Abd El Gadir Salim

==See also==
*International Rescue Committee

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
{{succession box
| title= 
| years=2006 Why We Fight
| after=Manda Bala (Send a Bullet)}}
 

 
 
 
 
 
 
 
 