Moving Target (2011 film)
{{Infobox film
| name           = Moving Target
| image          =
| caption        =
| director       = Mark Tierney
| producer       = Jerome Scott
| writer         = Jeremy Henman Michael Greco Jake Maskal Steven Berkoff Meredith Ostrom Nick Townsend Paul McGann Mark Rathbone Duncan Bannatyne Lou Doillon Colin Salmon
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2011
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
Moving Target is a 2011 British thriller (genre)| thriller, from director Mark Tierney. The film was formerly known as Naked in London.

Set on the hottest day of the year in London it follows the character Steve Lynch in an afternoon full of explosive situations.

== Plot ==
On the hottest day of a sweltering London summer, Steve Lynch must repay a £1,000,000 loan by 5pm, or lose everything he has ever worked for. An outrageous wager offers him a solution: run from North to South West London in less than two hours. But the events and people he encounters along the way will change his life forever.

== Background ==
NIL is set in London on a bank holiday that also happens to be the hottest day of the year (in real life usually in late July, early August). The film was partly inspired by the summer of 2003, which had record breaking sunshine.

== Cast == Michael Greco as Steve Lynch
* Steven Berkoff as Lawrence Masters
* Jake Maskall as Jonathan Porchester
* Meredith Ostrom as Callas
* Francesca Annis as Vanassa Swift
* Mark Rathbone as Roy Wegerley
* Nick Townsend as Peter Denby
* Duncan Bannatyne as Donald McKay
* Colin Salmon as Ralph
* David Cleveland-Dunn as Bobby Adams
* Nik Philpot as Driver

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 