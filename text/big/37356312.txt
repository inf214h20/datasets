The Pursuit of Happiness (1988 film)
 
{{Infobox film
| name           = The Pursuit of Happiness
| image          = 
| image size     =
| caption        = 
| director       = Martha Ansara
| producer       = 
| writer         = 
| based on = 
| narrator       = Peter Hardy Laura Black
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1988
| runtime        = 
| country        = Australia English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}}
The Pursuit of Happiness is a 1988 Australian film directed by Martha Ansara. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p223  

In the mid 1980s Ansara was involved in the Australian anti-nuclear movement and wanted to make a film about Australias relationship with the US. She originally intended to make a documentary but then it evolved into a dramatic feature about a married relationship that acted as a paradigm for the US-Australia relationship. Ansara:
 It was a bit of a crude analysis, but it took so long to make (under the 10BA system) that by the time we finished the film - it was 1987 - for various reasons, the anti-nuclear movement was on the way out, so the film missed its time. And with films, timing is everything. If we had done it in a year, which we couldnt, it probably would have done very, very well. As it was, it did return 40 percent to the investors, but that was because it was very low-budget. The interesting thing about the film is that, if the audience was not very sophisticated, they liked it enormously. If they were sophisticated, they thought it was really daggy.  

==References==
 

==External links==
*  at IMDB
*  at Oz Movies
 
 
 

 