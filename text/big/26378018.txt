Adieu Philippine
{{Infobox film
| name           = Adieu Philippine
| image          = Adieuphilippine.jpg
| caption        = 
| director       = Jacques Rozier
| producer       = 
| writer         = Jacques Rozier Michèle OGlor
| starring       = Jean-Claude Aimini Stefania Sabatini Yveline Céry
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = France
| language       = French 
| budget         = 
}}
Adieu Philippine ( , "Farewell, Philippine") is a 1962 French film directed by Jacques Rozier.  Although obscure and difficult to screen, it has been praised as one of the key films of the French New Wave.  It premiered at the 1962 Cannes Film Festival. 

==Synopsis==
Michel is a bored young man in Paris about to be sent to Algeria in the army. He works as a technician at a TV station. One day he meets two teenage girls, Juliette and Liliane, and begins dating them both separately. Eventually the girls find out and Michel goes on vacation to Corsica to escape them. The two girls follow him there and the three search for a film director who owes Michel money. Juliette and Liliane watch Michel sail away on a boat headed for Algeria.

==Cast==
*Jean-Claude Aimini as Michel
*Daniel Descamps as Daniel
*Stefania Sabatini as Juliette
*Yveline Céry as Liliane
*Vittorio Caprioli as Pachala
*David Tonelli as Horatio
*Annie Markhan as Juliette (voice)
*André Tarroux as Régnier de lÎsle
*Christian Longuet as Christian
*Michel Soyet as André

==Reception==
François Truffaut praised the film and called it "the clearest success of the new cinema where spontaneity is all the more powerful when it is the result of long and careful work." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 