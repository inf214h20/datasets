Lula, Son of Brazil
{{Infobox film
| name           = Lula, Son of Brazil
| image          = Lula o filho do brasil poster.jpg
| caption        = Brazilian theatrical release poster
| director       = Fábio Barreto 
| producer       = Luiz Carlos Barreto   Paula Barreto
| writer         = Denise Paraná   Fábio Barreto   Daniel Tendler   Fernando Bonassi
| starring       = Rui Ricardo Dias   Glória Pires   Cléo Pires   Juliana Baroni   Milhem Cortaz Antonio Pinto
| cinematography = Gustavo Hadba
| editing        = Leticia Giffoni
| studio         = Luiz Carlos Barreto Produções Cinematográficas
| distributor    = Downtown Filmes
| released       =  
| runtime        = 128 minutes
| country        = Brazil
| language       = Portuguese
| budget         = Brazilian Real|R$17 million
| gross          = $3,785,593 
}} not having been chosen to compete.

The film was a commercial and critical failure,   accused as election propaganda,  
and producers even aired for free.  Some observers in Brazil noted the film was a cult of personality. 

==Plot==
The film begins in October 1945 in Garanhuns, a municipality in the countryside of Pernambuco, when Luiz Inácio da Silva, nicknamed Lula, is born as the seventh child of Dona Lindu and Aristides. Two weeks after his birth, Aristides moves to Santos, São Paulo|Santos, a coastal city in São Paulo (state)|São Paulo, with Dona Mocinha, a cousin of Dona Lindu. Lindu raises Lulas siblings alone until December 1952, when the family moves to Santos to meet the patriarch. Upon their arrival, Dona Lindu discovers that Aristides had formed a second family with Dona Mocinha.

Aristides two families live in the same house and, as time goes on, they struggle to survive. Lula and his siblings attend elementary school and work as street vendors. Later, Lindu leaves the alcoholic and abusive Aristides and moves with her children to São Paulo. Lula receives a degree as lathe operator and gets a formal job in the car industry, where he loses a finger in a press. This and his brother Zizas arrest leads him into trade union activism, which made him nationally known in a period when such activities were forbidden. Lula is incarcerated for his activities, just as his mother dies.

==Production==
Based on the book of the same name by journalist Denise Paraná,  the film narrates the story of Lula from his birth until his mothers death, when he was a 35-year-old union leader detained by the  . October 5, 2008. Accessed on November 28, 2008. 

João Miguel, better known for his role in  , known for playing a corrupt police officer in the Golden Bear-winning film Tropa de Elite, was cast as Lulas father Aristides. 
 2002 and 2006 presidential elections,  claimed to have obtained the budget without government funds in order to repel criticism from their work.  According to O Estado de S. Paulo|Agência Estado, however, the film was sponsored by several contractors, something extremely unusual for the Brazilian film market. Three of these companies maintain direct business with the federal government. 

The shooting of the film began in late January 2009 in the   and Guarujá, cities on the coast of São Paulo (state)|São Paulo where Lula spent most of his childhood.  The shooting was predicted to end on March 21, 2009. The film was then edited in time to premiere in some film festivals prior to its wide release in Brazilian cinemas on January 1, 2010.

==Cast==
 . From right to left: Rui Ricardo Dias, Glória Pires, Cléo Pires, Lucélia Santos, Juliana Baroni and Milhem Cortaz.]]

 , with actors Rui Ricardo Dias, Felipe Falanga and Guilherme Tortólio during the films premiere in the city of São Bernardo do Campo.]]

Main cast:
* Rui Ricardo Dias as Luiz Inácio Lula da Silva
* Glória Pires as Dona Lindu Marisa Letícia
* Cléo Pires as Lourdes da Silva
* Milhem Cortaz as Aristides
* Sóstenes Vidal as Ziza

Supporting cast:
* Guilherme Tortólio as adolescent Lula
* Felipe Falanga as young Lula
* Rayana Cavalho as Dona Mocinha
* Lucélia Santos as Lulas teacher
* Antonio Pitanga as Mr. Cristóvão
* Celso Frateschi as Mr. Álvaro
* Marcos Cesana as Cláudio Feitosa
* Clayton Mariano as Lambari
* Mariah Teixeira as Marinete
* Suzana Costa as midwife
* Jones Melo as salesman Pau de Arara driver
* Antonio Saboia as Vavá
* Eduardo Acaiabe as Geraldão
* Marat Descartes as Arnaldo
* Nei Piacentini as Dr. Miguel
* Luccas Papp as Lambari (age 15)
* Vanessa Bizarro as Lourdes (age 13)
* Maicon Gouveia as Jaime
* Jonas Mello as Tosinho
* Fernando Alves Pinto as journalist
* Fernanda Laranjeira as Tiana

==Historical background== Brazilian history, and this is a reason why Paraná decided to write her book, which was also her Ph.D. dissertation at the University of São Paulo History School. During her research for the book, Paraná interviewed Lula himself and several people connected to him. According to her, while listening to Lulas statements, she thought that it was "a poorly written movie script, because everything fits". 
 labour at death in migration of Northeast region of Brazil. 

==Soundtrack==
 
  recorded "Meu Primeiro Amor" at the request of Lula.]]
 Pra Frente Brasil", an anthem for the Brazilian national football team during the 1970 FIFA World Cup.  The films soundtrack album was released a month before the film, marking the first time this has ever happened in Brazil. 

==Release==
  during the premiere of Lula, Son of Brasil at the 2009 Brasília Film Festival.]]

The film premiered at the opening of the 42nd Brasília Film Festival, in the  , the films international premiere was held in Washington, D.C. at the request of American president Barack Obama.  On January 1, 2010 the film received its wide release, debuting at over 500 cinemas all over Brazil.    . Rolling Stone Brasil. December 16, 2009. 

Unionized workers were able to purchase the films tickets for 5 reais (about 2,5 U.S. dollars) between November 20 and December 31, 2009 after a deal made between labour unions Central Única dos Trabalhadores and Força Sindical and the producers. Since its wide release on January 1, 2010, unionized workers were able to buy tickets with a 50% discount. 

The films DVD release happened on May 2009. It has affordable prices, from 10 to 12 reais (around 5 to 7 U.S. dollars), in order to counteract  . May 24, 2010.  He spread his plea throughout video rental associations and trade unions in the country.  The reason he gave for starting the campaign was the fact that Lula had watched 2 Filhos de Francisco before being released, encouraging piracy.  But, there is no evidence that proves that President Lula had watched by illegal means.
 presidential election Jewish community. Queen of England. We had this available, but avoided putting it in the original version in order not to get very jingoism|jingoistic", he said. 

The film has two scheduled dates for television premiere. Boechat, Ricardo.  . Istoé  It shall be featured on Canal Brasils programming in February if the Academy of Motion Picture Arts and Sciences announces that the film will compete for the Academy Award for Best Foreign Language Film.  Otherwise, it will be aired in March, in accordance with the rules of the Academy. 

==Reception==

===Public===
Lula, Son of Brazil was a flop. It grossed only two million reais (around one million U.S. dollars) in its first week of release,  being watched by no more than 200,000 people.  In spite of the controversy surrounding the film, it was still the second in the box office that week, behind Avatar (2009 film)|Avatar. As of January 8, the film had been watched by almost 320,000 people.  At the end of its theatrical run, the film had sold 852,212 tickets, making it only the 7th highest-grossing national production of 2010 in Brazil. 
 eponymous film. 

===Critical===
Alexei Barrionuevo, correspondent for The New York Times in Brazil, criticized the film for "failing to mention that Lula abandoned his girlfriend, Miriam Cordeiro, when she was six months pregnant". Lurian Cordeiro, Lulas daughter with Miriam, sent a letter to the editor saying that her mother was not "abandoned". Cordeiro said that Lula not only paid for all of her mothers medical costs, but also legally recognized her as his daughter on the very day after her birth. She also noted the fact that none of Lulas children are mentioned in the film, questioning that "If the movie is about my fathers path from impoverished immigrant to trade union leader, where do my brothers and I fit in?". She finished the letter by saying that she "loved the film" and that she agrees "with President Obama: Lula is the man!".   

On March 15, 2010, Brazilian magazine Veja (magazine)|Veja made a list of the top 10 worst Brazilian films of all time, and Lula, Son of Brazil was featured in ninth place. The lists author, Pollyane Lima e Silva, stated: "The director, Fábio Barreto, went on to say that his intention was not to be faithful to reality, but was, instead, to do a melodrama. And he was successful, since many scenes shown in the film never happened, and others were exaggerated so that Lula could seem like a hero". 

It currently holds a 33% "Rotten" rating at Rotten Tomatoes.

===Political criticism===
The film has been criticized by the Brazilian press for being released in an electoral year, while Lula was trying to elect his successor Dilma Rousseff. The release date would represent a deliberate attempt to influence the outcome of the election, in order to help Lulas candidate.  Rousseff admitted in an interview that the film could influence the outcome of the election.  But she added that "there is no possible way to measure the effect of this". The DVD release date, scheduled for May 2010, has also been criticized, as the electoral debate would be warming up.  On November 2009, it was revealed that Rede Globo bought the exclusive television rights for the film, but decided to broadcast it only after the elections.   

The biggest criticism, however, is that the film ignores less flattering aspects of Lulas career. Duffy, Gary.  . BBC. January 11, 2010.   According to O Globos Ricardo Noblat, the film represents an attempt to eliminate all of Lula failures, making him look like the almost perfect protagonist, showing him in an overly romanticized and heroic role. This would also have electoral purposes, according to him.  The film was also criticized by opposition politicians, the press and by many Brazilians because many of the companies that invested in the film (such as the heavy construction firms Odebrecht and Camargo Correa, as well as electric utilities) rely on government concessions and have major contracts with the federal government. 
 chief of staff and chosen successor, Dilma Rousseff". "Political analysts sees the movie as part of the renovation of the myth of Lula, which could help him return to power in 2014", he added. 

Glória Pires, which played Lulas mother in the film, said that Lula, Son of Brazil was a victim of political prejudice.   Moratelli, Valmir.  . Último Segundo. August 20, 2010.  According to her, "people saw another movie (...); the film I was in was based on a beautiful story of overcoming adversities, which was about a man that would become president, and not only a movie about the president itself".  Main actor Rui Ricardo Dias said that "those in power today does not depend on this film for absolutely nothing. Nor is this production going to change anything in the course of the elections". 

===Academy Award submission=== submission to Brazilian film Best Actress award". 
 Chico Xavier was also in the competition to represent Brazil at the Oscars, criticized the Ministrys choice, labelling it as political, and saying that the films title should have been Lula, the Owner of Brazil.    Arnaldo Jabor, whose film A Suprema Felicidade was also in the competition for the submission, chose not to comment on the Ministrys choice, saying that the commission "must have had reasons" to choose the film. 

According to producer Paula Barreto, the film may not be the best among the 23 competitors for the submission, but is the most adequate for the Academy Awards.    She believes that the film was "analyzed by reputable people, who know the film industry and also how the Oscar dispute works".  Farias responded the criticism for his commissions choice by saying that "our position has nothing to do with politics".  The film didnt make the shortlist published in January 2011.   

==References==
 
 

==External links==
 
*    
*    
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 