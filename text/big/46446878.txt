Dusty and Sweets McGee
{{Infobox film
| name           = Dusty and Sweets McGee
| image          = Dusty and Sweets McGee poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Floyd Mutrux
| producer       = Michael Laughlin Floyd Mutrux 
| writer         = Floyd Mutrux 
| starring       = Clifton Tip Fredell Kit Ryder Billy Gray Bob Graham Nancy Wheeler Russ Knight
| music          = Jake Holmes Van Morrison Ricky Nelson
| cinematography = William A. Fraker
| editing        = Richard A. Harris
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Dusty and Sweets McGee is a 1971 American drama film written and directed by Floyd Mutrux. The film stars Clifton Tip Fredell, Kit Ryder, Billy Gray, Bob Graham, Nancy Wheeler and Russ Knight. The film was released by Warner Bros. on July 14, 1971.  

==Plot==
 

== Cast ==
*Clifton Tip Fredell as Tip
*Kit Ryder as Male Hustler
*Billy Gray as City Life
*Bob Graham as Little Boy
*Nancy Wheeler as Nancy
*Russ Knight as Weird Beard
*William A. Fraker as The Cellist 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 