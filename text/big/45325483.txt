The Writing on the Wall (1910 film)
 
 
{{Infobox film
| name           = The Writing on the Wall
| image          = Thanhouser Billboard June 1910.jpg
| caption        = Advertisement in Billboard (magazine)|Billboard magazine
| director       = Barry ONeil
| producer       = Thanhouser Company
| writer         = Lloyd Lonergan
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama produced by the Thanhouser Company. Directed by Barry ONeil from a script by Lloyd Lonergan, this presumed lost film focuses on a young girl named Grace who becomes attracted to a wealthy man named Jack. Two men, named Turner and Hank plot to rob Jack after he withdraws a large sum of money from a bank, but Grace warns him of a plot to drug him. Jack escapes and marries Grace. The film has no known trade publication reviews, but reviews may exist for this film. Theaters were advertising this film as late as 1913.

== Plot ==
The film is presumed lost film|lost, but a surviving synopsis of the film was published in the The Moving Picture World on June 11, 1910. It states: "Turner, a man of bad character, conducts an inn in the country. His stepdaughter Grace is his trudge. When on an errand to the village she meets Jack, wealthy young man. He is much attracted by her. Jack goes to the bank and draws a large sum of money. Hank, a pal of Turner, sees him and tries to ambush him on the road, but fails. Then he goes ahead to tell Turner. Jack, on his trip, meets Grace. Nearly exhausted, he takes her home, and thus seems to play directly into the hands of the villains. They steal his revolver, and would drug him, were it not for the fact that Grace writes a warning on the wall with wine. Jack, aided by Grace, escapes but the girl is wounded. Jack marries Grace."    

== Production == Romeo and Juliet. Film historian Q. David Bowers does not attribute a cameraman for this production, but two possible candidates exist. Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.    The cast for the production is also unknown, but the cast may have included the leading players of the Thanhouser productions, Anna Rosemond and Frank H. Crane. Rosemond was one of two leading ladies for the first year of the company.  Crane was also involved in the very beginnings of the Thanhouser Company and acted in numerous productions before becoming a director at Thanhouser. 

An article in The Moving Picture World over a botched scene identifies Barry ONeil as the director who stunned the studio workers. In a critical scene in the film, the two villains bungled the attempt to drug the hero and wound up drinking the drugged drink. ONeil reportedly called out, "Stop! Youve drank the poison!" The article was published on May 14, 1910, and gives a loose timetable for when the film was being produced. 

== Release ==
The single reel drama, approximately 1000 feet in length, was released on June 10, 1910.  The film was advertised in the June 11, 1910 edition of Billboard (magazine)|Billboard magazine.  The film was advertised as late as 1911 in one Indiana theater and as far late as 1913 in a Texas theater.   It is unknown if published reviews for this work exist, but there is an absence of a citation Bowers or the American Film Institute catalogs.   Given this absence, it is possible that additional details or commentary can be obtained from advertisements or local newspapers outside of typical trade publications.

== References ==
 
 
 
 
 
 
 
 
 