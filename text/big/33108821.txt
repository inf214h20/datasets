A Coo-ee from Home
 
 
{{Infobox film
  | name     = A Coo-ee from Home
  | image    = 
  | caption  =  Charles Woods
  | producer = 
  | writer   = 
  | based on =  Charles Villiers
  | music    = 
  | cinematography = Tasman Higgins
  | editing  = 
| studio = Woods Australian Films
  | distributor = Woods Australian Films
  | released = 14 January 1918   
  | runtime  = 8,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Charles Woods about a miner who falls in love with a wealthy heiress. During filming a climactic shark attack, leading actor Bryce Rowe was attacked by a real shark and almost died. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 74 

==Plot==
Miner Will Morrison marries heiress Grace Norwood. Jealous Richard Myers tries to convince Will that Grace is unfaithful and when that fails he drugs Will and frames him for murder. Will is sentenced to death but a prison chaplain helps him escape. He runs away to sea, is exposed on board, jumps into the water, is attacked by a shark, but he manages to fight it off and escape. He reads that Richard has been arrested for murder back home. Will returns home and marries Grave. 

==Cast==
*Gertrude Darley as Grace Norwood
*Bryce Rowe as Will Morrison Charles Villiers
*Charles Woods

==Production==
During filming the shark attack sequence, actor Bryce Rowe was attacked by a real shark and almost died. He had to spend two weeks in hospital. 

==Release==
The fight between Will and the shark was heavily publicised on release. 

==References==
 

==External links==
*  
*   at National Film and Sound Archive

 
 
 
 
 
 
 
 


 