Zero Focus
 
{{Infobox film name           = Zero Focus image          = Zero Focus - DVD Cover.jpg caption        = DVD Cover director       = Yoshitaro Nomura producer       = Shiro Kido writer         = Shinobu Hashimoto Yoji Yamada Seicho Matsumoto (novel) starring       = Yoshiko Kuga music          = Yasushi Akutagawa cinematography = Takashi Kawamata editing        = distributor    = released       =   runtime        = 95 minutes country        = Japan language       = Japanese budget         =
}} 1961 Cinema Japanese mystery film directed by Yoshitaro Nomura and is based on a novel by Seicho Matsumoto.

==Plot==
One week into newlywed Teiko Uharas marriage, her husband, Kenichi, leaves on a short business trip and never returns. Teiko travels across Japan to search for him, and along the way discovers some surprising facts about her husbands past. With only a pair of old photographs among his belongings to go off of, Teiko tries to figure out what has happened to him.

==Cast==
* Yoshiko Kuga as Teiko Uhara
* Hizuru Takachiho as Sachiko Murota / Emmy
* Ineko Arima as Hisako Tanuma
* Koji Nambara as Kenichi Uhara
* Kō Nishimura as Sotaro Uhara
* Yoshi Kato as Mr. Murota
* Sadako Sawamura as Sotaros wife
* Takanobu Hozumi as Mr. Honda

==Awards==
*Won the 1962 Blue Ribbon Awards for Best Supporting Actress (Hizuru Takachiho)  , imdb.com 

==Remake==
A remake directed by Inudō Isshin with Ryoko Hirosue|Ryōko Hirosue as Teiko Uhara was released in 2009.

==References==
 

==External links==
*  
*  
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 


 
 