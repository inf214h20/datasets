Lenny (film)
{{Infobox film
| name           = Lenny
| image          = LennyOScheck.jpg
| image_size     =
| caption        = Original movie poster
| director       = Bob Fosse
| writer         = Julian Barry
| starring       = Dustin Hoffman Valerie Perrine
| producer       = Marvin Worth
| music          = Ralph Burns
| cinematography = Bruce Surtees
| editing        = Alan Heim MGM (2003, Twilight Time (under license from MGM) (2015, Blu-Ray DVD)
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $11,622,000  (rentals)  
}}
Lenny is a 1974 American biographical film about the comedian Lenny Bruce, starring Dustin Hoffman and directed by Bob Fosse. The screenplay by Julian Barry is based on his play of the same name.

==Plot==
The film jumps between various sections of Bruces life, including scenes of when he was in his prime and the burned-out, strung-out performer who, in the twilight of his life, used his nightclub act to pour out his personal frustrations. We watch as up-and-coming Bruce courts his "Shiksa goddess", a stripper named Honey. With family responsibilities, Lenny is encouraged to do a "safe" act, but he cannot do it. Constantly in trouble for flouting obscenity laws, Lenny develops a near-messianic complex which fuels both his comedy genius and his talent for self-destruction. Worn out by a lifetime of tilting at Establishment windmills, Lenny Bruce dies of a morphine overdose in 1966.

==Cast==
* Dustin Hoffman as Lenny Bruce Honey Bruce
* Jan Miner as Sally Marr
* Stanley Beck as Artie Silver
* Rashel Novikoff as Aunt Mema
* Gary Morton as Sherman Hart
* Guy Rennie as Jack Goldman

==Reception==
Lenny received favorable praise from critics and audiences alike, earning a score of 100% "Fresh" on the review aggregate website Rotten Tomatoes based on 16 reviews. 

==Awards and honors==
 Best Picture, Best Director, Best Actor, Best Actress, Best Adapted Best Cinematography.
 Best Actress at the 1975 Cannes Film Festival.   

==Casting==
One of the more interesting casting decisions was made while filming in the Broward County Courthouse; used as the set for the Miami Courthouse. Director Fosse decided to cast a real life Broward County Bailiff in the role of the Dade County Bailiff that would drag Dustin Hoffman (Lenny) out of the Courtroom. Aldo DeMeo, the President of the Bailiffs Association at the time, was offered the role. Though Aldo was uncredited, the scene when Lenny is removed from the courtroom was chosen as the clip screened at the Academy Awards to represent the film as a candidate for Best Picture.  Casting was completed by Florida-based casting director, Beverly McDermott. 
 
  arrested in 1961]]

==DVD==
Lenny was released to DVD by MGM Home Video on April 1st, 2003 as a Region 1 widescreen DVD and by Twilight Time (under license from MGM) as a Region 1 widescreen Blu-ray Disk on February 10th, 2015.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 