Chronicle (film)
 
{{Infobox film
| name = Chronicle
| image = Chronicle Film Poster.jpg
| caption = Teaser poster
| alt = 
| director = Josh Trank
| producer = {{Plain list | John Davis
*Adam Schroeder
}}
| screenplay = Max Landis
| story = Max Landis Josh Trank
| based on =
| starring = {{Plain list | 
* Dane DeHaan  Alex Russell
* Michael B. Jordan Michael Kelly
* Ashley Hinshaw
}}
| cinematography = Matthew Jensen
| editing = Elliot Greenberg
| studio = Davis Entertainment
| distributor = 20th Century Fox
| released =  
| runtime = 83 minutes    
| country = United States
| language = English
| budget = $12 million   
| gross =  $126.6 million   
}} Alex Russell) and more popular Steve (Michael B. Jordan), who form a bond after gaining telekinetic abilities from an unknown object. They first use their abilities for mischief and personal gain until Andrew turns to darker purposes.
 found footage filmed from the perspective of various video recording devices. It primarily uses Andrews hand-held camcorder to document the events of his life. Released in the United Kingdom and Ireland on February 1, 2012, and in the United States on February 3, 2012, it received a positive critical response and grossed $126 million worldwide.

==Plot==
Seattle teenager Andrew Detmer starts videotaping his life; his mother Karen is dying of cancer and his alcoholic father Richard is verbally and physically abusive. At school, Andrew is frequently bullied.
 telekinetic abilities, but bleeding from their noses when they overexert themselves. They develop a close friendship and begin using their abilities to play pranks, but when Andrew accidentally pushes a rude motorist off the road and into a river, Matt insists that they restrict the use of their powers, particularly against living things.

When they discover flight abilities, they agree to fly around the world together after graduation. Andrew wants to visit Tibet because of its peaceful nature. Steve encourages him to enter the school talent show to gain popularity. Andrew amazes his fellow students by disguising his powers as an impressive magic act. After the show, Andrew, Matt and Steve celebrate at a house party. After drinking with his classmate Monica, she and Andrew go upstairs to have sex, but he vomits on her, humiliating himself.

Andrew becomes increasingly withdrawn and aggressive, culminating when his father Richard attacks him and Andrew uses his powers to overwhelm him. His outburst is so extreme that it inflicts psychically connected nosebleeds on Steve and Matt. While Matt ignores the nosebleed, Steve goes up to the middle of a storm and tries to console Andrew who grows increasingly frustrated, but Steve is suddenly struck by a lightning bolt, and killed.  At Steves funeral, Matt confronts Andrew about the suspicious circumstances of Steves death. Andrew denies responsibility to Matt, but he privately begs for forgiveness at Steves grave.

Andrew grows distant from Matt and again finds himself ostracized at school. After being bullied, he uses his powers to tear teeth out of a bullys mouth. Andrew begins to identify himself as an apex predator, rationalizing that he should not feel guilt for using his powers to hurt those weaker than himself. When his mothers condition deteriorates, Andrew uses his powers to steal money for her medicine. After mugging some local thugs, he robs a gas station where he inadvertently causes an explosion that puts him in the hospital, and under police investigation. At his bedside, his father informs the unconscious Andrew that his mother has died, and he angrily blames Andrew for her death. As his father is about to strike him, Andrew awakens and the wall of his hospital room explodes.

Elsewhere, Matt experiences a nose bleed and senses that Andrew is in trouble. He goes to the hospital, where Andrew is floating outside. After saving Richard when Andrew attempts to kill him, Matt confronts his cousin at the Space Needle and tries to reason with him, but Andrew grows hostile and irrational at any perceived attempt to control him. Andrew attacks Matt and the pair fight across the city, crashing through buildings and hurling vehicles. Injured and enraged, Andrew uses his powers to destroy the buildings around him, threatening hundreds of lives. Unable to get through to Andrew and left with no other choice, Matt telekinetically tears a spear from a nearby statue, and impales Andrew with it, killing him. The police surround Matt, but he flies away.

Later, Matt lands in Tibet with Andrew’s camera. Speaking to the camera while addressing Andrew, Matt vows to use his powers for good and to find out what happened to them in the hole. Matt positions the camera to view a Tibetan monastery in the distance before flying away, leaving the camera behind.

==Cast==
* Dane DeHaan as Andrew Detmer Alex Russell as Matt Garetty
* Michael B. Jordan as Steve Montgomery Michael Kelly as Richard Detmer
* Ashley Hinshaw as Casey Letter Anna Wood as Monica
* Bo Petersen as Karen Detmer

==Production== Fear Itself Cape Town, The Fury as influences on Chronicle.  Filming started in May 2011 and continued for eighteen weeks, ending in August 2011.  Cinematographer Matthew Jensen used the Arri Alexa video camera to shoot the movie and Angenieux Optimo and Cook s4 lenses.  Postproduction techniques were used to give it a "found footage" look.  A cable cam rig was used for a shot in which the character Andrew levitated his camera 120 feet into the air.  The Arri Alexa camera was mounted on a skateboard to simulate Andrews camera sliding across a floor.  Stuntmen were suspended from crane wire rigs for flying scenes, with green screen special effects used for closeups of the actors.  Andrews video camera in the movie was a Canon XL1 MiniDV and later he switched to a HD camera that resembles a Canon Vixia HF M30.  His "Seattle" bedroom was actually a set constructed on a film studio stage in Cape Town.  Because in South Africa, vehicles drive on the left side and have steering wheels on the right side, American style vehicles had to be shipped in for the production.  DVD dailies were provided to the director and cinematographer by the Cape Town firm HD Hub. 

==Release==

===Box office=== The Woman The Grey ($9.5 million).    Next to an estimated production budget of $12 million, it was an unexpected financial success as the film became the fourth highest Super Bowl debut.  Chronicle also opened as a number one hit internationally, opening in 33 foreign markets such as  Australia, China, and the United Kingdom where it earned the most with $3.5 million. 

Overall, the film grossed $64.3 million in the United States and Canada, and $58.8 million in other territories, for a worldwide total of $123.1 million.   

 

===Critical response===
Chronicle has received positive reviews. Review aggregation website  , which assigns a weighted mean rating out of 100 to reviews from mainstream critics, the film received a score of 69/100, based on 31 reviews, which indicates "Generally favorable reviews".   

Film critic Roger Ebert gave the film a positive review, saying "From   deceptively ordinary beginning, Josh Tranks Chronicle grows into an uncommonly entertaining movie that involves elements of a superhero origin story, a science-fiction fantasy and a drama about a disturbed teenager," giving the film 3.5 stars out of 4. 
Empire (film magazine)|Empire critic Mark Dinning gave the film 4 stars out of 5, saying the film was "A stunning superhero/sci-fi that has appeared out of nowhere to demand your immediate attention." 
Total Film gave the film a five-star review (denoting outstanding): "Believable then bad-ass, it isnt wholly original but it does brim with emotion, imagination and modern implication." 

On the negative side, Andrew Schenker of Slant Magazine gave the film 2 out of 4 stars, saying the film, "offers up little more than a tired morality play about the dangers of power, rehashing stale insights about the narcissism of the documentary impulse." 

===Home media===
Chronicle was released on DVD and Blu-ray Disc on May 15, 2012. The film was released on DVD and a special "Lost Footage" edition for Blu-ray, which contains additional footage that was not shown in theaters. 

===Awards===
The film was nominated for Best Science Fiction Film at 39th Saturn Awards, but lost to The Avengers (2012 film)|Marvels The Avengers.
 
|- 2012
| rowspan="3"|Chronicle Golden Trailer Award for Best Most Original Trailer
|  
|- Golden Trailer Award for Best in Show
|  
|- 2013
| Saturn Award for Best Science Fiction Film
|  
 

==Sequel==
Fox hired Max Landis to write a sequel.  Whether director Josh Trank would return was unclear.  The Hollywood Reporter gave a brief one-line mention in its March 23, 2012 issue that a sequel was in development.  However, it was later reported that Fox was not happy with the script.  On April 10, 2013, Landis told IGN that Fox did like the script and theyre moving along with it; Landis also said that the sequel would be darker in tone.  On July 17, 2013, Landis revealed on his Twitter account that he and Trank are no longer working on the sequel and new writers have taken over to write the film.  Fox set Jack Stanley to write the film. 

==References==
 

==External links==
 
 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 