Bronson (film)
 
 
 
{{Infobox film
| name = Bronson
| image= Bronson poster.jpg
| alt =
| caption = Theatrical release poster
| director = Nicolas Winding Refn
| producer = Rupert Preston Danny Hansford
| writer = Brock Norman Brock Nicolas Winding Refn
| starring = Tom Hardy
| music = Johnny Jewel
| cinematography = Larry Smith
| editing = Matthew Newman
| studio = Aramid Entertainment Str8jacket Creations EM Media 4DH Films Perfume Films
| distributor = Vertigo Films
| released =  
| runtime = 92 minutes  
| country = United Kingdom
| language = English
| budget = $230,000
| gross = $2.2 million   
}} biographical Psychological psychological drama Michael Gordon Peterson, who was renamed Charles Bronson by his fight promoter. Born into a respectable middle-class family, Peterson would nevertheless become one of the United Kingdoms most dangerous criminals, and is known for having spent almost his entire adult life in solitary confinement. Bronson is narrated with humour, blurring the line between comedy and horror.

==Plot synopsis==
In this drama based on a true story, theres no one tougher or more brutal in the English penal system than prisoner Michael Peterson, aka Charles Bronson (Tom Hardy). First incarcerated after robbing a jewelry store, the married Bronson is sentenced to seven years. But his incorrigible, savage behavior quickly gets him in trouble with guards, fellow inmates and even a dog. The only place where Bronson cant do any harm is in solitary confinement, where he spends most of his time.

==Cast== Michael Gordon Peterson/Charles Bronson Matt King as Paul Daniels, nightclub owner and former fellow prisoner
* James Lance as Phil Danielson, prison art teacher
* Amanda Burton as Charlies mother
* Kelly Adams as Irene Peterson, Charlies wife
* Juliet Oldfield as Alison, Charlies lover Jonathan Phillips as the Prison Governor
* Mark Powley as Prison Officer Andy Love
* Hugh Ross as Uncle Jack
* Joe Tucker as John White, fellow patient in Rampton
* Gordon Brown as Screw, the first guard Bronson fights
* Charlie Whyman as fellow patient in Rampton

==Production==
For the role, Hardy had telephone conversations with the real Charles Bronson, before meeting him in person. During their meetings, Bronson was so impressed by how Hardy had managed to build up his physique for the role and how good he was at imitating him that he shaved off his trademark moustache so that it could be made into a loose-moustache for Hardy to use in the film.  The movie was filmed in and around the St. Anns, Sherwood, Worksop and Welbeck Abbey areas of Nottingham and Nottinghamshire.  The post office shown at the beginning of the film is located in Lostwithiel, Cornwall.

Director Refn was not permitted to visit Bronson in prison because he is not from Britain. He was only allowed two phone calls with him.

==Reception==
===Box office===
Bronson opened in a single theater in North America and made $10,940. The film ended up earning $104,979 in the U.S with the widest release being in 10 theaters. Internationally it made $2,155,733 for a total of $2,260,712. 

===Critical response===
Rotten Tomatoes gives the film a "Certified Fresh" score of 76% based on reviews from 79 critics, with an average rating of 6.6 out of 10 with the consensus "Undeniably gripping, Bronson forces the viewer to make some hard decisions about where the line between art and exploitation lies."  Metacritic gives the film a "generally favourable" average score of 71 out of 100 based on 22 reviews. 

Roger Ebert gave the film three stars out of four and praised the decision not to attempt to rationalise and explain Bronsons behaviour stating in his review, "I suppose, after all, Nicolas Winding Refn, the director and co-writer of "Bronson," was wise to leave out any sort of an explanation. Can you imagine how youd cringe if the film ended in a flashback of little Mickey undergoing childhood trauma? There is some human behavior beyond our ability to comprehend. I was reading a theory the other day that a few people just happen to be pure evil. Im afraid I believe it. They lack any conscience, any sense of pity or empathy for their victims. But Bronson is his own victim. How do you figure that?" 

Bronson was not initially allowed to view the film, but had said that if his mum liked it he was sure he would as well. According to Refns DVD audio commentary, his mother said she loved it. On 15 November 2011, he was granted permission to view it. Describing it as "theatrical, creative, and brilliant", Bronson heaped praise upon his portrayer, Hardy, but disagreed on the implied distance between himself and his father and the portrayal of Paul Edmunds as "a bit of a ponce." Nevertheless, he challenged his own familys reaction to the portrayal of his Uncle Jack, stating that he "loved" it, as would Jack himself.    Bronson had been originally displeased with the choice of Hardy, but after their first physical meeting, Hardy assured him that he would "fix it."  As proven, Bronsons development in trust in Hardys acting grew, even describing him as "Britains No. 1 actor." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 