Zoo in Budapest
{{Infobox film
| name           = Zoo in Budapest
| image          = Zoo in Budapest.jpg
| image size     =
| caption        =
| director       = Rowland V. Lee
| producer       = Jesse L. Lasky
| writer         = Melville Baker Jack Kirkland (story) Dan Totheroh Louise Long Rowland V. Lee (screenplay)
| narrator       =
| starring       = Loretta Young Gene Raymond
| music          = R.H. Bassett Peter Brunelli Louis De Francesco Hugo Friedhofer Werner Janssen J.S. Zamecnik (all uncredited)
| cinematography = Lee Garmes
| editing        = Harold D. Schuster
| distributor    = Fox Film Corporation
| released       = April 28, 1933
| runtime        = 94 min
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Zoo in Budapest (1933 in film|1933) is a film directed by Rowland V. Lee and starring Loretta Young, Gene Raymond, O.P. Heggie, and Paul Fix.

== Plot ==
Flamboyant Zani (Raymond) is a kindly young man who grew up entirely and works in the zoo in Budapest. His only true friends are the zoos animals, and indeed Zani has been chastised by his boss for being too nice to them. From the fashionable women visitors who wear them, Zani steals their animal furs.

While hiding out, Zani meets Eve (Young), a young and beautiful orphan girl. Eve must somehow escape from her strict orphan school, since she is faced with the prospect of being forced to work as an indentured servant (more like a slave) until she grows up. Zani and Eve together hide overnight in the zoo.

Dr. Grunbaum, the zoo director, is forced to organize a search party. Zani proves too elusive and harbors Eve in a bear cave. When evil zookeeper Heinie discovers them, Zani saves Eve from vicious attack. More scuffles result in crisis when dangerous animals become freed from their cages. The resourceful Zani brings pacification and redeems himself by saving a young child from a hungry tiger.

Grunbaum is played by O.P. Heggie, the South Australian actor known on London and Broadway stage as The Man with the Golden Voice. His kindly personality, which was also shown in Anne of Green Gables and The Bride of Frankenstein, is perfect here for portraying an enlightened approach in zoo management.

The film is a fantasy about a better world, a typical Depression-time theme. In those early days of the sound film, its audaciously creative approach to what is now called "sound design" was far from the conventional style where actors shouted into static microphones.

== External links ==
* 
*  


 

 
 
 
 
 
 
 
 
 
 


 