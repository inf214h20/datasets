Anna Favetti
{{Infobox film
| name           = Anna Favetti
| image          = 
| image_size     = 
| caption        = 
| director       = Erich Waschneck
| producer       = Erich Waschneck
| writer         = Walter von Hollander (novel & screenplay)
| narrator       = 
| starring       = Brigitte Horney   Mathias Wieman   Irene Falckenberg  Maria Koppenhöfer
| music          = Werner Eisbrenner   
| editing        = Walter Fredersdorf
| cinematography = Werner Bohne UFA
| UFA
| released       = 28 April 1938
| runtime        = 99 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}} German romance romantic drama film directed by Erich Waschneck and starring Brigitte Horney, Mathias Wieman and Irene Falckenberg. The screenplay was written by Walter von Hollander, adapted from his own novel Licht im dunklen Haus.

==Cast==
*  Brigitte Horney as Anna Favetti 
* Mathias Wieman as Hemmstreet 
* Irene Falckenberg as Irene Hemmstreet 
* Maria Koppenhöfer as Frau Favetti 
* Friedrich Kayßler as Herr Favetti 
* Karl Schönböck as Kingston 
* Elsa Wagner as Frau Stetius 
* Franz Schafheitlin as Dr. Thom 
* Jeanette Bethge as Bertha 
* Paul Bildt as Dr. Fister 
* Beppo Brem as Billy Blake 
* Rolf Wernicke as Reporter 
* Edwin Jürgensen as Empfangschef 
* Erwin Biegel as Kellner im Café 
* Hubert von Meyerinck as Hotelgast 
* Annemarie Korff as Dr. Thoms Sekretärin 
* Eva Sommer as Backfisch im Hotel 
* F.W. Schröder-Schrom as Professor der Jury  Charlotte Schultz as Portiersfrau

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Rentschler, Eric. The Ministry of Illusion: Nazi Cinema and Its Afterlife. Harvard University Press, 1996.

==External links==
* 

 
 
 
 
 
 
 
 

 