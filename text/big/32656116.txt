Thoroughbred (film)
 
 
{{Infobox film name           = Thoroughbred image          = Helen Twelvetrees during filming of "Thoroughbred", Sydney, 1936 Sam Hood.jpg caption        = Helen Twelvetrees during filming of Thoroughbred producer       = Ken G. Hall director       = Ken G. Hall  writer         = Edmond Seward starring       = Helen Twelvetrees Frank Leighton John Longden music          = Hamilton Webber cinematography = George Heath editing        = William Shepherd studio         = Cinesound Productions distributor    = British Empire Films released       = May 1936 (Australia) July 1936 (UK) runtime        = 89 minutes country        = Australia language       = English  budget         = ₤25,000 

}}
Thoroughbred is a 1936 Australian race-horse drama movie directed by Ken G. Hall, partly based on the life and career of Phar Lap. Hollywood star Helen Twelvetrees was imported to Australian to appear in the film.

==Synopsis==
A Canadian horse trainer, Joan, is the adopted daughter of horse trainer and breeder Ma Dawson. She buys an unwanted thoroughbred colt named Stormalong. Joan nurses the horse back to health with the help of Mas son Tommy, and Stormalong starts to win races. He becomes the favourite to win the Melbourne Cup which attracts the interest of a gambling syndicate who try to dope the horse and kill it in a stable fire. They then kidnap Tommy prior to the race.

Stormalong manages to participate in the Cup, and although is mortally wounded by a sniper, lives long enough to come first place. Tommy escapes and helps the police capture the gangsters.

==Cast==
*Helen Twelvetrees as Joan
*Frank Leighton as Tommy Dawson
*John Longden as Bill Peel
*Nellie Barnes as Judy Cross 
*Elaine Hamill
*Ronald Whelan
*Les Warton
*Harold Meade
*Edmond Seward as Mr Terry

==Production==
The film was the first made by Cinesound after the studio ceased production in 1935 enabling Hall to visit Hollywood for a number of months. While in Hollywood there he signed contracts with American star Helen Twelvetrees and writer Edmond Seward to work on the film. He also purchased a rear-projection unit which was used extensively in the film.  The budget was originally announced as £25,000. 

Twelvetrees was paid ₤1,000 a week, reportedly the highest salary ever paid by the Australian film industry to an actor.  Her co-stars would be Australian leading man Frank Leighton and English actor John Longden who was having an extended stay in Australia. According to Ken G. Hall, Twelvetrees and Leighton had an affair during filming, despite the actress having been accompanied to Australia by her husband and baby. Her husband found out and threatened to kill Leighton. Hall told Stuart F. Doyle who arranged for some detective friends to force Twelvetrees husband to leave Australia. 

This was the first movie with Cinesound for actor Ron Whelan, who joined the company as assistant director and also worked as an actor in several films. 

Australias Prime Minister Joseph Lyons visited the set during filming. 

The horses races were shot in part by a camera man being towed on a sled. 

The climax is similar to the 1934 Frank Capra film, Broadway Bill. Hall claimed he was unaware of this and blamed it on Seward. 

==Release==
The film was popular Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 172.  although reviews were mixed, with some criticism of the script. 

The film received a release in the UK, but was subject to cuts from the censor on the grounds of scenes depicting cruelty to animals, in particular the stable fire.  The movie was not a success at the English box office. 

A novelised version of the screenplay sold out within three days, at a rate of 1,000 copies a day. 

==References==
 
Hall, Ken G. Directed by Ken G. Hall: Autobiography of an Australian Filmmaker, Lansdowne Press, 1977

==External links==
 
*  in the Internet Movie Database
*  at Australian Screen Online
*  at Oz Movies
 

 
 
 
 