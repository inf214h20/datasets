Mani Magudam
{{Infobox film
| name           = Mani Magudam
| image          =
| caption        =
| director       = S. S. Rajendran
| producer       = D. V. Narayanasami
| writer         = 
| screenplay     = 
| story          = Karunanidhi
| starring       = S. S. Rajendran C. R. Vijayakumari Jayalalithaa M. N. Nambiar
| music          = R. Sudarsanam
| cinematography = T. M. Sundarababu
| editing        = R. Devarajan
| studio         = S.S.R. Pictures
| distributor    = S.S.R. Pictures
| released       =  
| runtime        = 128 minutes
| country        = India
| language       = Tamil language|Tamil}}
 1966 Cinema Indian Tamil Tamil film, directed by S. S. Rajendran and produced by D. V. Narayanasami. The film stars S. S. Rajendran, C. R. Vijayakumari, Jayalalithaa,andM. N. Nambiar in lead roles. The film had musical score by R. Sudarsanam.  

==Cast==
 
*S. S. Rajendran
*C. R. Vijayakumari
*Jayalalithaa
*M. N. Nambiar
*S. A. Natarajan Manorama
*A. Karunanidhi
*O. A. K. Devar
*En Thangai Natarajan
*R. V. Udayappa
*N. S. K. Kolappan
*S. V. Sahasranamam in Guest appearance
*Nagesh in Guest appearance
 

==Soundtrack==
The music was composed by R. Sudarsanam.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aathavan Uthiththaan || T. M. Soundararajan || Kannadasan || 04.09 
|-  Vaali || 03.19 
|-  Susheela || Kannadasan || 03.11 
|- 
| 4 || Valiyor Silar || T. M. Soundararajan || Bharathidasan || 03.10 
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 