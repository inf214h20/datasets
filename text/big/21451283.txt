Kurulu Bedda
{{Infobox film| name = Kurulu Bedda
 | image =
 | imagesize =
 | caption =
 | director = L. S. Ramachandran
 | producer = Arthur Amaratunga
 | Screenplay = P. K. D. Seneviratne
 | starring = Punya Heendeniya, D. R. Nanayakkara
 | music = Karunaratne Abeysekera (lyrics) P. K. D. Seneviratne (poems) R. Muttusamy (music)
 | cinematography =
 | editing =
 | distributor =
 | country    = Sri Lanka
 | released =   March 29, 1961
| runtime = Sinhala
 | budget =
   | followed_by =
}}

Kurulu Bedda is a 1961 Sri Lankan drama written by P. K. D Seneviratne. It attempted to follow in the precedent set by Rekava and create a truly Sinhala film.   

==Plot==
The story takes place in a small Sri Lankan village Kurulu Bedda. The Bandara family are members of the wealthy elite. Bandara Mahatthaya is the young heir of the family name and Bandara Menike is his mother. Siyadoris is a village man who works for the Bandara family. He has a pretty daughter named Ranmenike. Another member of the village is Kaithan Baas, a crafty marriage broker who advises Bandara Menike.

Bandara Mahatthaya has given up his medical education to take up his family properties when his father dies. He becomes attracted to Ranmenike after spotting her taking a bath while walking with a friend from medical school. He takes a photo of her and is seen by Kaithan Baas, who scolds this behavior to his mother.

Kaithan is brought before the rural court soon after on a fowl theft charge. Siyadoris testifies against him but Kaithan uses his cunning ways to escape conviction. From that day on he swears to get his revenge against Siyadoris family.

The medical student friend of Bandara Mahatthaya finishes his education and comes to the village as a doctor. On his arrival, he oversees Health Week celebration which includes a beauty contest. Ranmenike enters and wins much to the ire of Kaithan.

During a thovil ceremony, Kaithan sets fire to Siyadoris house and is finally imprisoned. The doctor and Bandara Mahatthaya help out Siyadoris with money to build a new house. The doctor is transferred to a different district soon after. At that time, Bandara Menike forces Siyadoris into conducting his daughters marriage to a man from a distant village. Bandara Mahatthaya is saddened but doesnt do anything to interfere. He presents Ranmenike with a gift on the wedding day which brings tears to her eyes. She then leaves for her husbands village.

Time passes and Bandara Mahatthaya remains a bachelor extending all his strength into helping the city. Ranmenike becomes ill after her husband is killed in an accident and is admitted to the doctor friends hospital which as fate has it is in her husbands district. Bandara Mahatthaya comes to the hospital and shares his feelings. They exchange words, Ranmenike entrusts him with his wedding gift to be given to her daughter and she dies during the birth. Bandara Mahatthaya leaves with the child. The village then weeps for Ranmenikes goodness.

===Cast===
*Punya Heendeniya &ndash; Ranmenike
*D. R. Nanayakkara &ndash; Kaithan Baas

==Songs==
*"Aruna Udaya" &ndash; J. A. Milton Perera
*"Oya Balma Oya Kalma" &ndash; Latha and Dharmadasa Walpola
*"Wi Kirili Yay Igili" &ndash; Latha Walpola (G. S. B. Rani Perea in the movie)
*"Kurulu Kobey" &ndash; Latha Walpola and chorus
*"Wathey Watunu Pol Athu (folk poem)" &ndash; Dharmadasa Walpola

==Production==
=== Development ===
Arthur Amaratunga who hailed from a small village was inspired to make a truly Sinhala film, after watching Rekava, that avoided the crudities common in popular film of the time. He subsequently came across a collection of stories by P. K. D. Seneviratne broadcast over the radio as Kurulu Bedda which were fitting to his goal. He contacted Seneviratne and got him to write a film script based on the work. 

Amaratunga operated under limited resources. He could only afford call sheets from the rundown Sundara Sound Studios and a South Indian crew that had no experience with location shooting. Poor photography, poor editing and poor processing resulted. 

==Reception==
The film received rave reviews upon release and was a box office hit. A lot of praise was directed at the authenticity of the film in dealing with village characters. A critic for a Sinhala daily wrote: "The makers of Kurulubedde have not learned the Background of the Ceylon villager from books or hearsay. They have actually lived it." 

==References==
 

 
 