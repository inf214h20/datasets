Fort Defiance (film)
{{Infobox film
| name           = Fort Defiance 
| image          = Fort Defiance poster.jpg
| alt            = 
| caption        = Theatrical release poster John Rawlins Charles Kerr (assistant)
| producer       = Frank Melford 
| screenplay     = Louis Lantz  Ben Johnson Peter Graves Tracey Roberts George Cleveland Ralph Sanford
| music          = Paul Sawtell
| cinematography = Stanley Cortez
| editing        = 
| studio         = Ventura Pictures Corporation
| distributor    = United Artists
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film John Rawlins Ben Johnson, Peter Graves, Tracey Roberts, George Cleveland and Ralph Sanford. The film was released on October 9, 1951, by United Artists.  

==Plot==
 

== Cast ==
*Dane Clark as Johnny Tallon Ben Johnson as Ben Shelby
*Peter Graves as Ned Tallon
*Tracey Roberts as Julie Morse
*George Cleveland as Uncle Charlie Tallon
*Ralph Sanford as Jed Brown, Stagecoach Driver
*Iron Eyes Cody as Brave Bear Dennis Moore as Lt. Lucas
*Craig Woods as Dave Parker
*Dick Elliott as Kincaid
*Bryan Slim Hightower as Hankey 
*Phil Rawlins as Les 
*Jerry Ambler as Cheyenne
*Kit Guard as Tracy
*Wes Hudman as Stranger 
*Hugh Hooker as Ed

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 