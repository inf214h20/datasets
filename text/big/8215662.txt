Comin' Round the Mountain
{{Infobox film
| name           = Comin Round the Mountain
| image          = cominroundthemountain.jpg
| image size     = 190px
| caption        = Theatrical release poster
| director       = Charles Lamont
| producer       = Howard Christie
| writer         = Robert Lees Frederic Riedel Margaret Hamilton
| music          = Joseph Gershenson
| cinematography =
| editing        = Edward Curtiss
| distributor    = Universal-International
| released       =  
| runtime        = 77 min.
| country        = United States
| language       = English
| budget         = $638,120 
| gross          = $1,550,000 (US rentals) 
}}
Comin Round The Mountain is a 1951 film starring the comedy team of Abbott and Costello.

==Plot==
Theatrical agent Al Stewart (Bud Abbott) has successfully booked his client, Dorothy McCoy (Dorothy Shay), "The Manhattan Hillbilly", at a New York nightclub. Unfortunately, he has also booked an inept escape artist, The Great Wilbert (Lou Costello), at the same location. During his performance, Wilbert cannot escape from his shackles and screams for help. Dorothy recognizes Wilberts shrill scream as the McCoy clan yell. More evidence of Wilberts heritage, namely a photograph and concertina, are found in his dressing room, and prove that he is the long-lost grandson of Squeeze Box McCoy, leader of the McCoy clan. Granny McCoy (Ida Moore) has been looking for Wilbert, as she will reveal where Squeeze Box hid his gold to kin folk only. Al, Dorothy and Wilbert head to Kentucky, and Granny recounts the story of the McCoy-Winfield feud that began over sixty years ago. The McCoys choose Wilbert to represent them against Devil Dan Winfield (Glenn Strange) in a turkey shoot. Wilbert has never even seen a gun before, and his carelessness leads to a revival of the feud.
 Margaret Hamilton) to obtain a love potion to use on Dorothy. While obtaining the potion, Huddy and Wilbert make voodoo dolls of each other and proceed to stick pins in them, which inflicts pain in the other person. After finally obtaining the potion, Wilbert gets on Huddys broom (complete with windshield and wipers), flies through the door, and crashes into a tree.

The potion initially works well, as Dorothy does fall for Wilbert, but unfortunately, everyone gets a sip of the concoction and falls in love. The potions effects eventually fade, and Clark and Dorothy prepare to marry. The Winfield clan soon arrive ready for a fight, during which a stray bullet breaks the love potion jar, leading Devil Dan to taste it and fall for Wilbert. Soon afterwards, a map leading to the treasure is found in Wilberts concertina. Devil Dan helps them enter the mine, where they eventually break through the rock, finding themselves in a vault filled with gold. Armed guards arrive to arrest the hapless treasure seekers, who have just broken into Fort Knox.

==Production==
Comin Round the Mountain was filmed from January 15 through February 12, 1951 and shot almost entirely in sequence. 

==Routines==
*Youre forty, shes Ten, first seen in Buck Privates, was included in this film.  This is how it goes:

"Age has got everything to do with it.  Say youre forty, and youre in love with a little girl thats ten."

"Thats ridiculous!"

"You are four times as old as that little girl.  Say you wait five more years.  Youre forty-five, shes fifteen.  Now youre only three times as old as that little girl.  You wait another fifteen years.  Shes thirty, youre sixty.  Now  youre only half as old as that little girl.  Now, how long will it take for you and the little girl to be the same age?"

"Thats ridiculous!  Shell pass me by.  Then, shell have to wait for me!"

"Why should she wait for you?"

"I was nice enough to wait for her!"

==DVD release==
This film has been released twice on DVD.  The first time, on The Best of Abbott and Costello Volume Three, on August 3, 2004, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 