The Haunted World of El Superbeasto
{{Infobox film
| name = The Haunted World of El Superbeasto
| image = El-superbeasto-trailer-icecream.jpg
| caption =Theatrical poster
| director = Rob Zombie
| producer = Andy Gould Tom Klein Rob Zombie
| writer =  Tom Papa Rob Zombie
| starring = Tom Papa Sheri Moon Zombie Paul Giamatti Tom Kenny Rosario Dawson Brian Posehn
| music = Tyler Bates Chris Hardwick & Mike Phirman (Hard n Phirm)
| cinematography = 
| based on = The Haunted World of El Superbeasto by Rob Zombie
| editing = Bret Marnell
| studio = Starz Media Film Roman
| distributor = Anchor Bay Films 
| released =  
| runtime = 77 minutes
| country = United States
| language = English
| budget = $10 million
}} adult animated exploitation musical musical horror action comedy film based on the comic book series created by Rob Zombie. It follows the character of El Superbeasto and his sidekick sister, Suzi-X, voiced by Sheri Moon Zombie. The film was released on September 22, 2009.

==Plot==
The film follows the adventures of El Superbeasto (Tom Papa), a suave, yet violent exploitation film actor/director and former masked wrestler, and his sultry "sidekick" and sister, the super-agent Suzi-X (Sheri Moon Zombie), as they prevent the evil Dr. Satan (Paul Giamatti) from taking over the world by marrying the foul-mouthed stripper Velvet Von Black (Rosario Dawson) with the mark of the devil on her backside. The adventure, set in the mythic world of Monsterland, also features Murray the Robot (Brian Posehn), Suzi-Xs sidekick and vehicle, based on the robot featured in the 1939 serial The Phantom Creeps starring Bela Lugosi. 

Director Rob Zombie also references several other films. Tom Papa, writer and the voice of the titular character, incorporated his style of humor to his character. Throughout the film, El Superbeasto often makes observations in unusual moments, like Papa does in his stand-ups.

==Cast==
* Tom Papa as El Superbeasto
* Sheri Moon Zombie as Suzi-X
* Paul Giamatti as Dr. Satan/Steve Wachowski
* Rosario Dawson as Velvet Von Black
* Tom Kenny as Otto
* Brian Posehn as Murray the Robot
* Dee Wallace as Trixie
* Ken Foree as Luke St. Luke  Geoffrey Lewis as Lenny  
* Rob Paulsen as El Gato/Commandant Hess 
* Daniel Roebuck as Morris Green 
* Danny Trejo as Rico
* Debra Wilson as Delores
* Harland Williams as Gerard the Exterminator 
* Clint Howard as Ctulu
* Charlie Adler as Krongarr
* Joe Alaskey as Erik the Newscaster
* April Winchell as Dame Grace Appleton
* John DiMaggio as Burt the Spurt
* Jess Harnell as Uncle Carl
* Sid Haig as Captain Spaulding
* Bill Moseley as Otis   
* Tura Satana as Varla
* Dee Bradley Baker as Nazi Zombie

==Production==
Work began on The Haunted World of El Superbeasto in 2006 and a release date was later scheduled for May 2007, but the film was completed in 2009. In an interview conducted on July 20, 2007 by shocktillyoudrop.com, Zombie explained that, "Nothing really much  ." During that time, the film was still being animated, however, Zombie then began work on Halloween (2007 film)|Halloween. He informed the animators that he had "to walk away because I cant split my time between two things". Zombie noted that work on The Haunted World of El Superbeasto "started when I was on The Devils Rejects|Rejects and its now just sitting on a shelf waiting for me to finish Halloween".   

In a November 2007 interview with Bloody Disgusting, Zombie announced that the film was "almost finished". He went on to say that, although he was then on tour until February, "we will finally finish the music on Superbeasto and itll be done" afterwards.   

Rob Zombie had the following to say in a September 28, 2008 posting on the official El Superbeasto Myspace page: "Were down to the end! 3 long years in the making and worth every second. By Halloween this thing will be in the can completely DONE!"
 Oscar gold, we now have another animated contender." 

In an October 29, 2008, Blender Magazine interview, Rob Zombie stated that "Ive been working on   for three years, and Im actually in the last weeks of it — Im mixing the sound this week. Im not sure of the release date yet, but thatll be out probably early 2009. Its a full-length adult animated comedy."  The film has been screen-tested for a 2009 release by Anchor Bay Entertainment. 
 SpongeBob and Scooby-Doo were filthy."  The film, running feature length at 75 minutes, was released on DVD on September 22 of that year.  A majority of the songs in the film are written and performed by Chris Hardwick and Mike Phirmans comedy duo Hard n Phirm.

==Reception==
El Superbeasto received mixed reviews. While theres no approval rating on Rotten Tomatoes by critics, the audience rating is currently at 48%.

==References==
 

==External links==
* 
*  
*   at the  

 

 
 
 
 
 
 
 
 
 
 
 