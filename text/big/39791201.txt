Calapor (film)
{{Infobox film
| name           = Calapor
| image          = Calapor 2013 Film Poster.jpg
| caption        = 
| director       = Dinesh P. Bhonsle
| producer       = A. Durga Prasad
| writer         = R. Mihir Vardhan
| starring       = Priyanshu Chatterjee Hemant Gopal Rituparna Sengupta Raghubir Yadav Harsh Chhaya P. Subbaraju Aziz Naseer Binny Sharma Aakash Sharma Anjan Srivastav Minaxi Martins Vinod Anand
| music          = Arjuna Harjai
| cinematography = Mahesh Aney 
| editing        = Virendra Hari Gharse
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
}} thriller film produced by A. Durga Prasad and by directed by Dinesh P. Bhonsle. The film is a taut thriller, set in the backdrop of a jail. It is a modern story of reformation. The film provokes thoughts about the nature of human being while infusing hope that there is good in every human being.  The film is slated for August 2, 2013 release. 

==Plot==
When a reluctant Jyotsna, an artiste with her assistant Ragini arrive in Calapor Central Jail to conduct an art program for prison inmates, initiated by a very sincere and committed reformist Jail Superintendent, Karunakar, the agonizing past of a criminal husband who cheated her and a delinquent son who ran away, returns to torment her. With adversity comes an opportunity; a second chance that Jyotsna as a mother cannot afford to lose or fail to succeed.

==Cast==
* Priyanshu Chatterjee
* Rituparna Sengupta
* Raghubir Yadav
* Harsh Chhaya
* Subbaraju
* Aziz Naseer
* Binny Sharma
* Aakash Sharma
* Hemant Gopal
* Anjan Srivastav
* Vinod Anand
* Minaxi Martins
* Kumar shivam
*Ghanshyam srivastva

==References==
 

==External links==
*   at Bollywood Hungama
*   at Weird Angles 

 
 
 


 