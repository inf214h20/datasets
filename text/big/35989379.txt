Thai Ullam
Thai Ullam ( ) ( ) is a 1952 Indian Tamil film directed by K. Ramnoth. Starring R. S. Manohar and Gemini Ganesan in the lead roles, the film has music composed by V. Nagaiah and Rama Rao.  It is an adaptation of the English tearjerker novel East Lynne, by Henry Wood. 

==Cast==
* R. S. Manohar
* Gemini Ganesan
* Chittoor V. Nagaiah
* C. V. V. Panthulu
* Javert Seetharaman
* Chandrababu
* M. V. Rajamma
* Madhuri Devi
* K. R. Chellam
* ‘Friend’ Ramasami
* T. P. Muthulakshmi
* G. Sakunthala

==Production==
K. Ramnoth, who had left Gemini Studios on August 15, 1947 worked for a well-known film unit of that time, Narayanan & Company. For it, he made the film Thai Ullam, an adaptation of the novel East Lynne by the noted writer Henry Wood.  While R. S. Manohar was cast as the male lead, T. S. Balaiah was considered for playing the antagonist.
He however opted out of the project after demanding a salary of  75,000, which he was refused.    Subsequently, he was replaced by a then struggling actor named R. Ganesh, who later became known as Gemini Ganesan. The film became a major breakthrough for Ganesan, who would later become a part of the "Big Three" of Tamil cinema, the other two being Sivaji Ganesan and M. G. Ramachandran.   

==Soundtrack==
{| class="wikitable"
|-
! Track !! Song !! Singer !! Lyrics
|-
| 1 || Kathayai Kelada || M. L. Vasanthakumari || Surabhi
|-
| 2 || Konjum Puraave || M. L. Vasanthakumari || Udumalai Narayana Kavi
|-
| 3 || Kovil Muluthum Kanden || M. L. Vasanthakumari || S. D. Sundaram
|-
| 4 || Vellai Thamarai || M. L. Vasanthakumari || S. D. Sundaram
|}

==Reception==
Film historian Randor Guy called it a "cinematic masterpiece", and concluded that the film would be "Remembered for: melodious music, especially hit tunes by the inimitable MLV." 

==References==
 

==External links==
*  

 
 
 
 


 