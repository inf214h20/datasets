Into the West (film)
 
 
 
{{Infobox film
| name           = Into the West
| image          = Into The West (1993 movie poster).jpg
| caption        = Theatrical release poster Mike Newell Jonathan Cavendish Tim Palmer Gabriel Byrne (associate)
| writer         = Jim Sheridan David Keating David Kelly
| music          = Patrick Doyle
| cinematography = Newton Thomas Sigel Peter Boyle
| distributor    = Miramax Family Films
| released       = 11 December 1992
| runtime        = 97 minutes
| country        = Ireland
| language       = English
| budget         =
}} magical realist Mike Newell.
 My Left The Miracle, The Commitments, The Boxer, In the Name of the Father and The Crying Game. The film also received several awards for Best Film, Best European Film, and Outstanding Family Foreign Film. 

==Synopsis==
Into the West is a film about two young boys, Tito (Conroy) and Ossie (Fitzgerald), whose father "Papa" Reilly (Byrne) was "King of Irish Travellers" until his wife, Mary, died during the birth of their second son, Ossie.  The boys grandfather (David Kelly) is an old story-telling Traveller, who regales the children with Irish folk-tales and legends. When he is followed by a beautiful white horse called Tír na nÓg (meaning "Land of Eternal Youth" in Irish), from the sea to Dublin, where the boys and their father have now settled down in a grim tower block in Ballymun, the boys are overwhelmed with joy and dreams of becoming cowboys. The horse is stolen from them and they begin their adventure to get their mystical horse back. They escape the poverty of a north Dublin council estate, and ride "Into the West" where they find that Tír na nÓg is not just a horse.

==Cast==
*Gabriel Byrne as Papa Reilly
*Ellen Barkin as Kathleen
*Ciarán Fitzgerald as Ozzie
*Ruaidhri Conroy|Rúaidhrí Conroy as Tito (credited as Ruaidhrí Conroy)  David Kelly as Grandfather Johnny Murphy as Tracker
*Colm Meaney as Barreller John Kavanagh as Hartnett
*Brendan Gleeson as Inspector Bolger Jim Norton as Superintendent OMara Anita Reeves as Mrs. Murphy
*Ray McBride as Mr. Murphy
*Dave Duffy as Morrissey
*Stuart Dannell-Foran as Conor Murphy (credited as Stuart Dannell)
*Becca Hollinshead as Birdy Murphy

==Production==
The script was written by Jim Sheridan, who did not intend to write simply for children, although the film mainly follows two young children on the run with their beautiful, magical white horse. Other themes, targeted to adults, are present: dealing with grief, the clash of cultures with differing values, and the use of the police by the rich and powerful to enforce property rights in their favour.  Sheridan wrote the script five years before he directed My Left Foot.

Gabriel Byrne has said it was one of the best scripts he ever read, and described it at the time as Jim Sheridans best work to date. Byrne was committed to the work, he said: Apart from it being a story about Travellers, and the relationship between a father and his two sons, it really was in a way about Ireland. 

Ellen Barkin has said that from the first reading she thought it an extraordinary piece of film writing. 
 Portarlington in County Laois.

==Reception==
The film has received a mostly positive critical reception. On Rotten Tomatoes, the film holds a score of 70% and holds an audience share of 73,  and at the Internet Movie Database, it holds a rating of 6.7/10. 

Roger Ebert of the Chicago Sun-Times has said the "kids will probably love this movie, but adults will get a lot more out of it".  Variety Staff has said that "Into the West is a likable but modest pic", and that "a major asset throughout is Patrick Doyles rich, Gaelic-flavoured scoring that carries the movies emotional line and fairy tale atmosphere.  Desson Howe of The Washington Post has said that the film is "a charming childrens crusade – a rewarding journey for all ages".  Rita Kempley of the Washington Post has said that "the movie is alternately grim and lyrical", and "though long on ambiance and short on story, it may appeal to the spiritually inclined – and to oater lovers. 

===Awards=== Mike Newell.  Oulu International Childrens Film Festival – Mike Newell. Golden Calf for Best European Film at the Netherlands Film Festival – Mike Newell.
* 1994: Young Artist Award for Outstanding Family Foreign Film at the Young Artist Awards – Mike Newell. 
* 1994: Young Artist Award for Outstanding Youth Actor in a Family Film at the Young Artist Awards – Rúaidhrí Conroy & Ciarán Fitzgerald. 

==Home Video Releases== aspect ratio of 1.85:1 anamorphic widescreen. 

The VHS was released in Ireland and the UK on 21 September 1993 by Entertainment in Video.  It was released on DVD in Ireland and the UK on 17 December 2001 by Entertainment in Video and again on 15 September 2003 by Cinema Club

==References==
 

==External links==
*  
*  
*   at Rotten Tomatoes
*  

== Bibliography ==
*Lance Pettitt, Screening Ireland: film and television representation, Manchester University Press, 2000, 320 p. (ISBN 9780719052705) 
*Joe Cleary, Outrageous Fortune: Capital and Culture in Modern Ireland, vol. 1, Field Day Publications, coll. « Field day files », 2007, 320 p. (ISBN 9780946755356)

 
 

 
 
 
 
 
 
 
 