The Devil's Hand (2014 film)
{{Infobox film
| name           = The Devils Hand
| image          = The Devils Hand 2014 film poster.jpg
| alt            = 
| caption        = 
| director       = Christian E. Christiansen
| producer       = 
| writer         = Karl Mueller
| starring       = Rufus Sewell Alycia Debnam-Carey Adelaide Kane
| music          = Anton Sanko
| cinematography = Frank Godwin
| editing        = Timothy Alverson Ryan Folsey Steve Mirkovich
| production companies = LD Entertainment
| distributor    = Roadside Attractions
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
The Devils Hand, also known under its working titles of Where the Devil Hides, The Devils Rapture, and The Occult, is a 2014 horror movie that was directed by Christian E. Christiansen.   The film was released direct to video on 14 October 2014 and centers upon five girls born into an Amish community that believes them to be part of a Satanic prophecy. 

==Synopsis==
On June 6, six women in a close-knit Amish community go into labor and deliver six girls, sparking fears that they could fulfill an ancient prophecy. The prophecy states that six girls will be born on the sixth day of the six month (6-6-6) and that one of them will become the "Devil’s Hand". Out of fear, one mother kills both herself and her daughter shortly after her birth, and the remaining five girls grow up with relative ignorance of the prophecy. As they come closer to their eighteenth birthday, their actions are constantly monitored by the community- especially Elder Beacon (Colm Meaney), who views any ungodly actions as proof that one of them is Satans minion. This is all made more troubling by the fact that one of the girls, Mary (Alycia Debnam-Carey), has started having terrifying visions that could suggest that she is the Devils Hand. As tensions rise, a mysterious figure begins to murder the girls one by one.

==Cast==
*Rufus Sewell as Jacob Orange
*Alycia Debnam-Carey as Mary
*Thomas McDonell as Trevor
*Adelaide Kane as Ruth
*Leah Pipes as Sarah
*Ric Reitz as Sheriff Stevens
*Jennifer Carpenter as Rebekah
*Colm Meaney as Elder Beacon
*Jim McKeny as Elder Stone
*Katie Garfield as Abby
*Nicole Elliott as Hannah

==Reception==
Fangoria and The Dissolve both panned The Devils Hand,  and Fangoria wrote that although the cinematography was nice and the film had some talented actors, the film "plays more like a CW-style teen melodrama than a serious theological terror film, especially when Mary begins hanging out with Trevor (Thomas McDonell), a boy from the next town over who just happens to be the son of the local sheriff. The movie seems more devoted to their lovey-dovey subplot than to exploring its own darker sides—including developing insinuations that Elder Beacon is a perv in addition to being a zealot. Then, at the very end, it finally remembers it’s a horror film and delivers a suitably spooky conclusion; but all the blood and thunder of the last few minutes serve mostly to point up how half-hearted the previous 80 are."  Dread Central was more positive in their review, stating "While The Devil’s Hand is not the most intelligent nor tightly plotted or creative thriller to come along lately, it is well-acted, very gory and has a great-guns ending zinger that’s a fitting nod to the old school." 

==References==
 

==External links==
*  

 
 
 
 
 