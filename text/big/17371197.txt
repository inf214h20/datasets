Treed Murray
{{Infobox film
| name           = Treed Murray
| image          = Treed_Murray_Poster.JPG
| caption        = Poster William Phillips
| producer       = Paula Devonshire Helen Du Toit Marguerite Pigott
| writer         = William Phillips
| starring       = David Hewlett
| music          = Jim McGrath Joel Feeney Marc Jordan
| cinematography = John Holosko
| released       = November 30, 2001
| runtime        = 90 minutes
| country        = Canada
| language       = English
}}
 William Phillips and features Stargate Atlantis star, British-born Canadian actor  David Hewlett. It won two Genie Awards (Overall Sound and Sound Editing), and was nominated for three more (Motion Picture, Direction, and Music - Original Song).

==Plot==
During a routine walk home, advertising executive Murray (David Hewlett) runs into Carter (Kevin Duhaney), a would-be mugger. After a struggle, Murray strikes Carter in the face with his briefcase and proceeds to walk away. Murray notices that the mugger is part of a small gang, which begins to chase him. Fearing for his life, he climbs a tree to evade capture, but finds himself unable to leave when the gang decides to wait for him to come down.

Attempts to call for help are useless, as Murray had been forced to give up his possessions to his attackers. As the night rolls in, the gang slowly becomes desperate and tries to force Murray out of the tree but find themselves accidentally providing him with weapons and hostages. The night continues and through several events, the two sides learn the importance of human life and begin to change.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| David Hewlett
| Murray
|-
| Aaron Ashmore
| Dwayne
|-
| Clé Bennett
| Shark
|-
| Kevin Duhaney
| Carter
|-
| Jessica Greco
| Kelly
|-
| Carter Hayden
| KC
|-
| Julian Richings
| Homeless Man
|}

==External links==
*  
*  

 
 
 
 


 