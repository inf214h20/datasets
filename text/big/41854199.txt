Dereza (film)
{{Infobox film
| name           = Dereza Дереза
| image          =
| image_size     = 
| caption        = 
| director       = Alexander Davydov 
| producer       =  Roman Kachanov 
| narrator       = 
| starring       =  Nina Zorskaya,  Anatoly Barantsev, Irina Muravyova
| music          = Nina Savicheva
| cinematography = Alexander Chekhovsky
| editing        = N. Bordzilovskaya
| distributor    = 
| released       = 1985
| runtime        = 10 minutes
| country        = USSR
| language       = Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =
}} Soviet animated Soyuzmultfilm studio. The film received national and international recognition.

== Plot summary ==
The plot of the film is based on a Russian Folk Tale. The Cartoon follows a goat, Dereza, who is talented but lazzy. Dereza prefers idleness, pretending to be unhappy and hungry. Using the protection of the Grandfather who believes her, she loses sense of proportion and is thrown out of the house. The goat later repents.

== Creators ==
{| class="wikitable"
|-
! !! English !! Russian
|-
| Director 
| Alexander Davydov 
| Александр Давыдов
|-
| Writer  Roman Kachanov
| Роман Качанов
|-
| Art Director 
| Anatoly Savchenko
| Анатолий Савченко
|-
| Animators  
| Yuriy Meshcheryakov,  Vitaly Bobrov,  Alexander Mazaev,  Alexander Dorogov,  Galina Zolotovskaya
| Юрий Мещеряков,  Виталий Бобров,  Александр Мазаев,  Александр Дорогов,  Галина Золотовская
|-
| Artists 
| Irina Svetlitsa,  E. Antusheva,  V. Kharitonova,  Alexander Markelov
| Ирина Светлица,  Э. Антушева,  В. Харитонова,  Александр Маркелов
|-
| Camera 
| Alexander Chekhovsky
| Александр Чеховский
|-
| Music 
| Nina Savicheva
| Нина Савичева
|-
| Sound
| Vladimir Kutuzov
| Владимир Кутузов
|-
| Executive Producer 
| V. Yegorshin 
| В. Егоршин
|-
| Voice Actors 
| Nina Zorskaya,  Anatoly Barantsev,  Irina Muravyova
| Нина Зорская,  Анатолий Баранцев,  Ирина Муравьева
|-
| Editor
| N. Bordzilovskaya
| Н. Бордзиловская
|-
| Texts of Songs
| Alexander Timofeyevsky
| Александр Тимофеевский
|-
| Script Editor
| Tatiana Paporova
| Татьяна Папорова
|}

==References==
 

==External links==
*  at Animator.ru
 

 
 
 
 
 
 
 
 


 
 