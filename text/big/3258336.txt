Dhadkan
{{Infobox film
| name           = Dhadkan Akshay Kumar
| image          = DhadkanDVD.jpg
| director       = Dharmesh Darshan
| producer       = Ratan Jain
| writer         = Naseem Mukri Raj Sinha
| narrator       =
| starring       = Akshay Kumar Sunil Shetty Shilpa Shetty Mahima Chaudhry
| music          = Nadeem-Shravan
| cinematography = W.B. Rao
| editing        = Bharat Singh
| distributor    =
| released       =  
| runtime        = 160 mins
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
 romantic drama film which deals with the  love triangle of individuals portrayed by Sunil Shetty, Shilpa Shetty and Akshay Kumar. The film was a critical as well as commercial success and was the fourth highest grossing movie of the year. Sunil Shetty won a Filmfare Best Villain Award for his performance.

== Synopsis ==
Dhadkan is about Anjali (Shilpa Shetty), a young woman who hails from an extremely rich and influential family. Her father Mr. Chauhan (Kiran Kumar) is a renowned businessman and has many dreams for his daughter. Anjali is in love with Dev (Sunil Shetty) who is very poor and often cannot even afford to clothe himself properly. Dev loves Anjali and wants to marry her &mdash; so he has to meet her father.

When Anjali puts forward to her parents the proposal of marrying Dev, she is rebuked and gets an outright refusal. Moreover, her parents have chosen for her a wealthy suitor from Delhi. Not wanting to hurt her parents Anjali finally gives in and marries Ram (Akshay Kumar) who her parents believe will be a perfect match for her.

Ram is a man of great ideals, who believes in giving a rightful place to his wife and respects her sensibilities. Despite this, he is unable to win Anjali’s love at first and their marriage remains on the edge. However, after seeing the magnanimity of her husband’s heart in forgiving and accepting her, she realises she has fallen in love with him.

Anjali leads a life of bliss and becomes an ideal wife. But suddenly, at their third wedding anniversary party, Dev returns and reveals his intent of winning Anjali back. Dev is now a wealthy businessman, and Anjali finds herself at a crossroads where she has to fight for her husband with her former love. Anjali is in love with her husband and has no wish to return to her former lover. When she tells him this, he sets out to ruin Rams business. But the truth wins in the end when Anjali tells Dev she is pregnant with Rams child. Dev realises his folly and decides to marry his friend and business partner Sheetal Varma (Mahima Chaudhary) who has secretly loved him for a long time.

== Cast ==
* Akshay Kumar ... Ram
* Sunil Shetty ... Dev	
* Shilpa Shetty ... Anjali
* Mahima Chaudhry ... Sheetal Varma 	
* Sharmila Tagore ... Devs mother (Special Appearance)	
* Sushma Seth ... Rams stepmother	
* Parmeet Sethi ... Bob (Rams stepbrother)
* Manjeet Kullar ... Nikki (Rams stepsister)
* Kiran Kumar ... Anjalis father
* Anjana Mumtaz... Anjalis mother
* Anupam Kher ... Sheetals father (Special Appearance)
* Kader Khan ... Singer (Special Appearance in song "Dulhe Ka Sehra")

== Crew ==
* Director:: Dharmesh Darshan
* Story: Raj Sinha and Naseem Mukri
* Dialogue: Dharmesh Darshan and Naseem Mukri
* Producer: Ratan Jain
* Music: Nadeem-Shravan
* Art Direction: Bijon Das Gupta
* Editor: Bharat Sameer
* Costume Designer: Vikram Phadnis and Navin Shetty

== Music ==
{{Infobox album
| Name = Dhadkan
| Type = Soundtrack
| Artist = Nadeem-Shravan
| Released = 2000
| Recorded =  Feature film soundtrack
| Length = 
| Label = Venus
| Producer = Nadeem-Shravan
| Last album = Sirf Tum (1999)
| This album = Dhadkan (2000)
| Next album = Kasoor (2001)
}}

The music of this film gave the message loud and clear that composer Nadeem-Shravan are back in a big way as they recreated their magic of the 90s once again. Dil Ne Yeh Kaha became a legendary love track.

{| class="wikitable "
|-
! Track # !! Title !! Singer(s)
|-
| 1 || "Dil Ne Yeh Kaha Hai Dil Se" || Udit Narayan, Alka Yagnik, Kumar Sanu
|-
| 2 || "Tum Dil Ki Dhadkan Mein" || Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
|-
| 3 || "Dulhe Ka Sehra" || Nusrat Fateh Ali Khan
|-
| 4 || "Dil Ne Yeh Kaha Hai Dil Se (II)" || Sonu Nigam, Alka Yagnik
|-
| 5 || "Na Na Karte Pyar" || Udit Narayan, Alka Yagnik
|-
| 6 || "Tum Dil Ki Dhadkan Mein (Sad)" || Kumar Sanu
|-
| 7 || "Aksar Is Duniya Mein"|| Alka Yagnik
|}

== Review ==

Taran Adarsh wrote of the film:

 

Adarsh described Shilpa Shetty as the "life of the enterprise", adding that she "looks good, delivers her lines effectively and emotes with utmost conviction." He also said "Akshay Kumar shows vast improvement as an actor. He is very controlled and handles this difficult role with sincerity." 
 

== Awards ==
;Filmfare Awards
 Best Female Playback -  Alka Yagnik - "Dil Ne Yeh Kaha Hai". Best Villain - Sunil Shetty

Nominated Best Choreography  Best Director - Dharmesh Darshan Best Movie - Dharmesh Darshan Best Lyricist Sameer - "Tum Dil Ki Dhadkan Mein". Best Music Director - Nadeem Shravan Best Male Playback  - Udit Narayan - "Dil Ne Yeh Kaha Hai". Best Supporting Actress - Mahima Chaudhry

;International Indian Film Academy Awards
Nominated Best Actor - Akshay Kumar Best Actress  - Shilpa Shetty  Best Supporting Actress - Mahima Chaudhry Best Movie - Dharmesh Darshan Best Story - Dharmesh Darshan Best Director - Dharmesh Darshan Best Female - Alka Yagnik - "Dil Ne Yeh Kaha Hai". Best Lyricist Sameer - "Dil Ne Yeh Kaha Hai". Best Lyricist - Sameer - "Tum Dil Ki Dhadkan Mein". Best Male Playback - Abhijeet Bhattacharya|Abhijeet- "Tum Dil Ki Dhadkan Mein". Best Music Director - Nadeem-Shravan  Best Villain - Sunil Shetty

;Screen Awards

Won
* Best Publicity Design - Himanshu Nanda, Rahul Nanda

Nominated
 Best Lyricist Sameer - "Dil Ne Ye Kaha Hai". Best Music Director - Nadeem Shravan Best Female Playback - Alka Yagnik - "Dil Ne Ye Kaha Hai". Best Male Playback - Abhijeet Bhattacharya|Abhijeet- "Tum Dil Ki Dhadkan". Best Male Playback - Udit Narayan - "Dil Ne Ye Kaha Hai". Best Supporting Actress - Mahima Chaudhry

==Sequel==
Dharmesh Darshan the director of the film announced Dhadkan 2 - The Heart Beats Again which will be a sequel to Dhadkan. It will have fresh cast and Music Directors. It will release in late 2015

== References ==
 

== External links ==
* 

 
 
 
 