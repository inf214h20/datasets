Suddenly It's Magic
{{Infobox film
| name           = Suddenly Its Magic
| image          = Suddenly Its Magic Poster.jpg
| caption        = Theatrical movie poster
| director       = Rory Quintos
| producer       =  
| writer         =  
| screenplay     = Vanessa Valdez
| music          = Teressa Barrozo
| cinematography = Marcial Tarnate III
| editing        = Marya Ignacio
| starring       =  
| studio         = Star Cinema
| distributor    = Star Cinema (Philippines) Jalan (Singapore)
| released       =  
| country        =  
| language       =  
| runtime        = 110 minutes 
| budget         = 
| gross          = PHP 97,196,663      
 
}}
 Thai romantic Baifern Pimchanok confirmed on her Twitter account that the said movie will screen in Thailand on March 14, 2013 at the Major Cineplex theater.  Following Thailands premiere of the movie, on March 25, 2013, Cambodias Legend Cinemas confirmed on their Facebook fanpage of the official screening of the movie in the country on March 28, 2013. 

The story depicts two star-crossed lovers: Marcus Hanson (Mario Maurer), a Thai superstar, and Joey Hermosa (Erich Gonzales), a Filipina baker. When Marcus decides to fly to Philippines to escape from his career, he meets Joey. Even though their worlds collide, the two later fall in love. But their relationship is tested by conflicts from their worlds.

==Plot==
Joey Hermosa (Erich Gonzales) and Marcus Hanson (Mario Maurer) only have two things in common. One: they live to make fairy-tale romances happen — Joey through her exquisite wedding cakes; Marcus through the numerous romantic comedies he stars in. Two: their own love stories do not have the fairy-tale happy endings — she was just recently dumped at the altar; he just discovered that his on-screen partner and real-life girlfriend had fallen in love with another man.

These two broken-hearted people will find love again when they least expect it. Sounds like the perfect recipe for a happy ever after!

Desperate to escape the media frenzy and the intrusive questions of the public about his love life and career, Marcus impulsively decides to go on vacation in the Philippines where he meets Joey who is determined to move forward with her life. Marcus finds himself drawn to Joey’s passion for baking, and rediscovers his love for acting. In love once again, he invites her to join him in Thailand. Joey refuses at first, but she eventually follows him to Bangkok and allows herself to fall in love again. But their love encounters opposition from Marcus’ fans who are desperate to see him reunite with his ex-girlfriend, and from his overprotective mother. Moreover, Joey’s responsibilities back home cause a strain in their relationship. Despite their love for each other, Joey and Marcus begin to question if their dreams are worth sacrificing for holiday romance suddenly turned serious.Then when Marcus got accepted in Hollywood he remembered Joey and went back to the Philippines and surprised her and took a balloon and attached it to a paper that says " Marcus and Joey Forever"

Can Marcus and Joey’s fairytale romance survive life’s realities?.  

==Cast==
*Mario Maurer   as Marcus Hanson
*Erich Gonzales as Josephine "Joey" Hermosa Baifern Pimchanok  as Sririta Taylor
*Joross Gamboa as Sam
*Cacai Bautista as Marj
*Ces Quesada as Tita Tetz
*Guji Lorenzana as Marvin
*Jestoni Alarcon as Governor Martinez
*Joy Viado as Jinggay
*Dinkee Doo as Dan
*Dionne Monsanto as Ditas
*Michael Shaowanasai as Peter
*Apasiri Chantrasmi as Mai
*Nana Dakin as Lalita
*Karinyut Jindawat as Mitr

==Production==
The idea of doing the movie is due to the impact of Thai movie   language.  In April 2012, Maurer went back to the Philippines to shoot scenes in  , Thailand to shoot for a month with other Thai actors that were cast in the film.   The shooting ended on June 19, 2012.  There was an attempt to change the title to Im in love with a Thai actor; however it did not push through. A cameo role for Pimchanok Luevisadpaibul was also confirmed by PR (official promoter of Maurer in the Philippines).

== International screenings ==
Here are some schedule dates and venues for the international screening of Suddenly Its Magic:  

{| class="wikitable"
|-
! Date !! Country 
|-
| November 1 || Singapore
|- North Hollywood, San Bruno, Union City, Hawaii 
|- Sacramento
|-
| March 14, 2013 || Bangkok, Thailand  
|-
|}
(notes: please refer to the link given for the complete information about the screening)

==Controversies==
The film had a controversy news that Suddenly Its Magic will be bought by United States, Cambodia, and Thailand. 

==Trivias==
Suddenly Its Magic titled from original song "Suddenly Its Magic" by the Music of Willie Wilcox and Lyrics of Willie Wilcox Amy La Television

==Awards and nominations==
{| class="wikitable"
|- Year
! Award
! Category
! Nominated work
! Result
|-
| rowspan="4" style="text-align:center"|2013 ASAP Pop Viewers Choice Awards 
| Pop Movie
| rowspan="2"|Suddenly Its Magic
|  
|-
| Pop Movie Theme Song
|  
|-
| Pop Screen Kiss Erich Gonzales and Mario Maurer
|  
|-
| Pop Love Team
|  
|}

==References==
 

==External links==
* 
*  at Box Office Mojo

 
 
 
 
 
 
 