Catch Me Daddy
 
{{Infobox film
| name           = Catch Me Daddy
| image          = Catch Me Daddy POSTER.jpg
| caption        = Film poster
| director       = Daniel Wolfe
| producer       = 
| writer         = Daniel Wolfe Matthew Wolfe
| starring       = Sameena Jabeen Ahmed Gary Lewis Conor McCarron
| music          = 
| cinematography = 
| editing        =  Film 4 British Film Institute Screen Yorkshire
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}

Catch Me Daddy is a 2014 British thriller film directed by Daniel Wolfe. It was screened as part of the Directors Fortnight section of the 2014 Cannes Film Festival.   

The film received its British debut screening at the 2014 London Film Festival. Sameena Ahmed won the award at the festival for the Best British Newcomer. It was shown at the Leeds Film Festival in November 2014.

==Plot==
A young woman has run away from her Pakistani family and is living with her non-Pakistani boyfriend. Her brother and other men try to track her down. 
==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 