Time (2006 film)
{{Infobox film  name        = Time image       = Time film poster.jpg caption     = Time film poster director    = Kim Ki-duk  producer    = writer      = Kim Ki-duk starring    = Ha Jung-woo Sung Hyun-ah  distributor = Happinet Pictures Korea
| film name      = {{Film name hangul      =   hanja       =   rr          = Sigan mr          = Sigan}}
| budget = $1,000,000 gross       =    . Box Office Mojo. Retrieved 4 March 2012.  released    =   runtime     = 97 minutes
| country    = South Korea language    = Korean
}} South Korean director Kim Ki-duk. It premiered at the Karlovy Vary International Film Festival on June 30, 2006.

== Plot ==
Seh-hee and Ji-woo (Ha Jung-woo) are a young couple two years into their relationship. Though he never acts on his impulses, Ji-woo has something of a roving eye and Seh-hee is intensely jealous and fearful that Ji-woo will soon lose interest and leave her. Believing that Ji-woo is bored with seeing the same, boring her all the time, Seh-hee takes drastic action, leaving him without warning and having drastic cosmetic surgery, taking on a new face, which she hopes to use to snare him again, under an assumed identity, once she has healed. But when Ji-woo shows interest in this new and "improved" Seh-hee (Sung Hyun-ah), it triggers only more self-doubt and loathing.  After all, he may love the new girl, but does this mean that he has rejected the old? Seh-hee is utterly trapped in her own insecurities, a situation that prompts Ji-woo to take drastic action of his own.

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 