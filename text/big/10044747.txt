Mungaru Male
 
{{Infobox film
| name           = Mungaru Male
| image          = MM-02.jpg
| caption        = Movie poster
| director       = Yogaraj Bhat
| writer         = Yogaraj Bhat Preetam Gubbi
| producer       = E. Krishna  Ganesh   Pooja Gandhi   Anant Nag   Padmaja Rao   Sudha Belavadi
| cinematography = Krishna (Cinematographer)|S. Krishna
| editing        = Deepu S. Kumar
| music          = Mano Murthy
| released       =  
| runtime        = 138 minutes 
| language       = Kannada
| budget         =  2 crore 
| gross          =  65 crore 
| country        = India
}}

Mungaru  Male ( ) is a 2006 Indian Kannada language film directed by Yogaraj Bhat and produced by E. Krishnappa. It stars Ganesh (actor)|Ganesh, Pooja Gandhi, Anant Nag, Padmaja Rao in lead roles. The film is believed to have shaped and strengthened the careers of Gandhi, Ganesh, director Bhat, playback singers Sonu Nigam, Kunal Ganjawala, Udit Narayan and Shreya Ghoshal in South India and lyricist Jayant Kaikini, choreographers A. Harsha, Imran Sardaria, composer Mano Murthy and others. 

The film was the first in India for any language to be screened for continuously one year in a multiplex.    Overall it ran over 500 days in Karnataka. It was remade in   as Premer Kahini 
{{cite web
|url=http://www.screenindia.com/news/Premer-Kahini/303194/
|title=Premer Kahini - ScreenIndia.Com
|publisher=www.screenindia.com
|accessdate=2008-11-04
|last=
|first=
}}
  in 2008. A   as Gnabagangal Thalattum in the year 2013. A sequel to the film titled Mungaru Male 2 was announced in July 2014.   

==Plot==
The protagonist Preetam (Ganesh (actor)|Ganesh), on a visit to Eva Mall (a famous Mall in Bangalore) amidst a heavy wind, spots a pretty girl, Nandini (Pooja Gandhi). While staring at her, he inadvertently falls into a manhole. Nandini rescues him from the pit, but in the process loses her heart-shaped watch she had just bought.

While accompanying his mother to Madikeri, Preetam confronts a man named Jaanu (Neenaasam Ashwath). Jaanu, who has been following Nandini, beats up Preetam thinking that he is in love with Nandini. Preetam, unaware that Jaanu has vowed not to allow any one near Nandini, trashes Jaanu and his gang in return. 

In Madikeri, Preetam meets Nandini unexpectedly. He identifies himself and expresses his love towards her and offers to tie the watch as an indication for their marriage. Nandini, who is already engaged rejects his request. Still, Preetam vows to marry Nandini if she meets him again. In the meantime, Preetam discovers that his host in Madikeri, Col. Subbayya (Anant Nag) is Nandinis father, who is pretty much deaf, and Nandinis marriage is a just a week away. Dejected, Preetam throws Nandinis heart-shaped watch away. But Nandini calls him over the phone and taunts him to return. Delighted, Preetam goes in search of her watch and brings it back. While searching it, he spots a rabbit, which he calls Devadas, and brings it along with him. 

Since Nandinis friends are due to arrive from Mumbai for the marriage, Preetam takes Nandini to the railway station. The train from Mumbai is delayed by five hours, so Nandini and Preetam decide to visit a nearby hill-temple. While returning from the temple, Preetam and Nandini are caught in rain. An old couple offers Preetam and Nandini to take shelter inside their hut. Preetam, still in two minds about expressing his love to Nandini, grabs a couple of toddy bottles, goes out in rain and starts drinking. However, when Nandini walks towards him, offering an umbrella, he is under a state of intoxication and tells Nandini that hed better stay away from Nandini to remain a decent boy, rather than to propose to or elope away with her.

Nandini is now in love with Preetam and is in a dilemma as her wedding is due in a few days. Nandini requests him to take her to the top of a waterfall and expresses her love towards Preetam, standing at the edge of the waterfall. 

Preetam, intent on marrying Nandini, takes her father, Subbayya for a morning jog to discuss his marriage with Nandini. But Subbayya a heart patient, tells Preetham that hes expected to die anytime and his only aim in life is to get Nandini married off to Gautam (Diganth). On the night before the marriage, Preetam drives away from the house without taking Devadas. He then starts drinking the whole night in a road-side bar. He finds Gautam, asking the bar-owner for directions to Subbayyas home. When Jaanu tries to kill Gautam, Preetam saves Gautam and convinces Jaanu that only Gautam is the best person to marry Nandini.

Next day, he drops Gautam to the marriage house, just in time for the marriage. He then declines to attend Gautams marriage. Gautam asks for the heart-shaped watch as a remembrance but Preetham does not agree to give it. Preetam then leaves. Meanwhile, on the wedding day everyone is searching for Preetam, but he is nowhere to be found. His mother is the only one who know the truth about his love, and is worried of his whereabouts, but does not show her worry.

Resignedly, Preetam watches the arch proclaiming "Gautham weds Nandini". As he is leaving, he spots Devadas and takes it with him. He drives towards Bangalore, all the while expressing his disappointment to Devadas, but later he comes to know that Devadas is dead.

==Cast== Ganesh as Preetam
* Pooja Gandhi as Nandini
* Ananth Nag as Colonel Subbayya (Nandinis father)
* Sudha Belwadi as Kamala (Preetams mother)
* Padmaja Rao as Bablee (Nandinis mother)
* Neenasam Ashwath as Jolly
* Diganth as Gautam (Nandinis fiancé)
* Jai Jagadish (Preetams Father)
* A Rabbit as Devdas

==Crew==
* Director: Yograj Bhat
* Producer: E. Krishnappa
* Cinematography: Krishna
* Editing: Deepu S. Kumar
* Story:  Preetham Gubbi
* Dialogues: Yograj Bhat/Preetham Gubbi
* Lyrics: Jayanth Kaikini, Yograj Bhat, Kaviraj, Shiva
* Choreography: Harsha, Raghu
* Art: Ismail
* Music: Mano Murthy
* Singers: Sonu Nigam, Udit Narayan, Hemanth Kumar, Kunal Ganjawala, Shreya Ghosal, Priya Hemesh, Stephen & Sunidhi Chauhan.

==Background and development== Puneet Rajkumar who rejected it.  Ganesh (actor)|Ganesh, whose first film to be released, Chellata, was involved in the script making stage. He introduced Yograj Bhat to producer E. Krishnappa who was Ganeshs acquaintance. Krishnappa agreed to finance the film, since he knew Ganesh. Since Yograj Bhat was unable to cast well-known Kannada actresses, he signed on a relatively unknown actress, Pooja Gandhi, for the lead female role in the film.

===Filming===
Up to 80% of the scenes in the film were shot in the rain.  Shooting locations included Madikeri,  Sakaleshpura,  Jog Falls,  and Gadag. Krishna, the camera man was a part-time photographer prior to the film.

==Soundtrack==
{{Infobox album
|  Name        = Mungaru Male
|  Type        = soundtrack
|  Artist      = Mano Murthy
|  Cover       = Mungaru Male Soundtrack cover.jpg
|  Released    =  
|  Recorded    =  Feature film soundtrack
|  Length      = 
|  Label       = Anand Audio
|  Producer    = Mano Murthy
|  Reviews     = 
|  Last album  = Amrithadhare (2005)
|  This album  = Mungaru Male (2006)
|  Next album  = Cheluvina Chittara (2007)
}}
 Kaviraj and  Hrudaya Shiva. While the crew was filming in Byakrolli, near Sakleshpur, Kaikini toured North Karnataka.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Onde Ondu Sari Kaviraj
| extra1 = Kunal Ganjawala, Priya Himesh
| length1 = 
| title2 = Mungaru Maleye
| lyrics2 = Yogaraj Bhat
| extra2 = Sonu Nigam
| length2 = 
| title3 = Kunidu Kunidu Baare
| lyrics3 = Jayant Kaikini
| extra3 = Udit Narayan, Sunidhi Chauhan, Stephen
| length3 = 
| title4 = Anisuthide Yako Indu
| lyrics4 = Jayant Kaikini
| extra4 = Sonu Nigam
| length4 = 
| title5 = Suvvi Suvvali
| lyrics5 = Hrudaya Shiva
| extra5 = Hemanth
| length5 = 
| title6 = Ivanu Geleyanalla
| lyrics6 = Hrudaya Shiva
| extra6 = Shreya Ghoshal
| length6 = 
| title7 = Araluthiru Jeevada Geleya
| lyrics7 = Jayant Kaikini
| extra7 = Shreya Ghoshal
| length7 = 
}}

===Reception===
Upon the albums release, it topped the charts with the song "Anisuthide Yako Indu" proving to be massive success getting very high radio and TV air time. 

==Box office==
It helped in expansion of Kannada film market, which is first of its kind. Made with a budget of  2 crore, the film went on to earn around  50 crore.       Mungaru Male completed 100 days.  The film has also been released with 150 prints in Karnataka.  This was the first Indian movie which ran over a year in a multiplex, PVR cinemas Bangalore.  Mungaaru Male was being screened at full houses, one year after its release.   IBN Movies. February 12, 2008. "...Mungaaru Male, which broke all records, totally grossed Rs 75 crores." 

==Home media==
The movie was released on DVD with 5.1 channel surround sound and English subtitles. This was the first Kannada movie to be released with that clarity.

==Critical response==
The film received average to highly positive reviews. Rediff.com awarded it a three-star rating and praised its cinematography, technical values, and the acting of Ganesh and Anant Nag, but opined that the story was flawed.  Nowrunning.com felt that the film lacked a credible story line and gave too much importance to music and song picturisation.  Kannada portal Thatskannada.com praised the film for its content, narration and usage of rain as a metaphor for love.  Indicine.com    gave it four stars.

==In other languages== Tamil as Gnabagangal Thalattum in the year 2013. The Telugu-language version was released in 2008 as Vaana (film)|Vaana, meaning rain.   It was remade in Bengali language as Premer Kahini. Boney Kapoor is planning to remake the film in Hindi starring either Imran Khan or Ranbir Kapoor. 

==Awards==
The film was awarded the Best Kannada Film award for the year 2006-07 by the Government of Karnataka (seven awards in total from the Government of Karnataka). Additionally, it also won 3 Filmfare awards for best music, awarded to Mano Murthy, and best direction & best film awarded to Yograj Bhat. However, it did not win a single national award from the Government of India.   

==Book on the movie==
The director, Yogaraj Bhat, wrote about the making of Mungaru Male in the book Haage Summane, which was released on the occasion of the silver jubilee celebration for the movie. 

==Sequel==
It was announced in July 2014 that Shashank would be directing the sequel of the film, titled Mungaru Male 2. Ganesh would be reprising his role in the film, which would be produced by J. Gangadhar under the banner of E. K. Pictures. 

== See also ==
* Vaana (film)|Vaana
* Premer Kahini

==Notes==
 

==References==
*  

==External links==
*  

 

 
 
 
 
 
 