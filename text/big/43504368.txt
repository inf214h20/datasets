Unfriended
 
{{Infobox film
| name           = Unfriended
| image          = File:Unfriended 2015 teaser poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Levan Gabriadze
| producer       = {{Plainlist|
* Timur Bekmambetov
* Jason Blum
* Nelson Greaves}}
| writer         =Kiel  Kimsey 
| starring       = {{Plainlist|
* Shelley Hennig
* Renee Olstead
* Will Peltz
* Jacob Wysocki
* Courtney Halverson
* Moses Jacob Storm
* Heather Sossaman}}
| music          = 
| cinematography = Adam Sidman
| editing        = Parker Laramie
| studio         = {{Plainlist|
* Bazelevs Company
* Blumhouse Productions}} Universal Pictures
| released       =  
| runtime        = 83 minutes  
| country        = United States
| language       = English
| budget         = $1 million 
| gross          = $32,742,475   
}} found footage supernatural horror film directed by Levan Gabriadze,    written by Nelson Greaves, and produced by Timur Bekmambetov, Jason Blum, and Greaves.

The film premiered at the Fantasia Festival on July 20, 2014, and at SXSW on March 13, 2015. It received a theatrical release on April 17, 2015. The film stars Shelley Hennig as one of several friends who find themselves terrorized online by an anonymous person.  A variation on the found footage genre, the entire film is set in real-time on a characters computer screen.  The film received positive reviews from critics  and has grossed over $32 million, against a budget of $1 million, becoming a huge financial success.

==Plot==
Set in  ), Ken Smith (Jacob Wysocki), and Adam Sewell (Will Peltz), as well as an unnamed, faceless account with the screen name billie227. 
 911 to report online abuse. Ken reveals that he personally did not like Laura while Blaire types out several messages to Mitch without sending them, implying that Laura may have been abused by her uncle.

Soon after Val calls 911, Laura emails Blaire an Instagram picture revealing that Val commented on the abusive video, telling Laura to kill herself. Comments pour in from Instagram users, expressing their hatred for Val. Vals Skype video feed is interrupted and she reappears as a seemingly frozen image in a bathroom, sitting next to an open bottle of bleach. Concerned, Blaire calls her only for the group to see Vals phone, propelled by vibration, moving across the bottom of her screen while an unresponsive Val continues to blankly stare wide-eyed at the camera. All of the friends also comment on how the mirror set behind her in the room is evidently shattered. A loud bang is heard and the computer falls; the police arrive and the remaining friends decode the police code used to classify her death, which is being labeled a suicide. Though not explicitly stated or seen, it is heavily implied that Val was forced to drink the bleach. 

Laura begins sending photos to the group. Mitch and the others dont open theirs out of fright, but Blaire does and sees they reveal her sleeping with Adam. Having had enough, Ken manages to email Trojan horse removal software to rid everyones computers from the malicious account. Despite threats from Laura, they apparently get her off of their Skype call. This reprieve is short lived, as Laura answers the phone when Adam calls the police, and resurfaces on Skype with a camera view that is behind a lattice in Kens room, set looking at him from the back. When Ken finds the source of the video, he looks at it in horror and doesnt respond to his friends asking him what he is seeing before his video feed is stilled and then disconnected. Fragments of the video do come in shortly thereafter, showing him being attacked by an unseen force, then mangling his arm in a blender, before dying by breaking the blender and using the blades to slice his throat.

Laura then forces the four remaining friends to play  , Blaire got drunk and crashed Jess mothers car, Adam bargained with Laura to trade Jess life for his, and Mitch ratted Adam out to the cops for selling cannabis (drug)|cannabis. Tension between the four ensues before a drunk Adam becomes enraged and uses the game to reveal that Blaire is no longer a virgin and in fact had slept with him behind Mitchs back; Laura uploads a video of them doing it to YouTube to prove it. Both Blaire and Adam receive messages from their printer, and refuse to reveal them. Mitch becomes furious that Blaire and Adam are apparently "sending notes" and threatens to leave his bedroom. Laura insists that if he leaves his computer he will die, and in a moment of high stress, Blaire reveals her note which states "If you reveal this note, Adam will die." Adam promptly shoots himself in the head as his camera reveals his note: "If you reveal this note, Blaire will die."

Laura insists that the game is still going on and asks whether anyone has ever defaced Lauras grave. After Jess refuses to admit to it, Laura turns off the lights in her house, and Jess locks herself in her bathroom while Blaire goes on to Chatroulette to call for help. Blaire successfully gets someone to call the police, but Jess video feed comes back to show Jess being thrown across the room. Seconds after, she is choking on a hot hair curling iron shoved in her throat, soon killing her. Laura then uploads an image to Facebook of Jess with the curling iron in her throat with the caption "Looks like she finally STFU." Laura pressures Blaire and Mitch into admitting who posted the video in the first place, with Blaire admitting that it was Mitch at the last moment. Mitch stabs himself through the head with a large knife, leaving Blaire all alone.

Laura insists that Blaire confess one further thing while Blaire professes her innocence while trying to reason with her past friend by showing her the good times they used to have. Laura then uploads the full video that caused her to commit suicide to Facebook; however, in this one, Blaire turns the camera back to reveal that she was the cameraman, laughing and saying, "I got her." Blaires Facebook account becomes flooded with hateful comments as she silently watches. Laura tells her that she wishes she could forgive her as she signs off of Skype. Suddenly, the door opens in Blaires room, while silhouetted hands begin to close Blaires laptop. Seconds after, Lauras ghost jumps at Blaire and presumably kills her.

==Cast==
* Shelley Hennig as Blaire Lily
* Moses Jacob Storm as Mitch Roussel 
* Renee Olstead as Jess Felton
* Will Peltz as Adam Sewell
* Jacob Wysocki as Ken Smith 
* Courtney Halverson as Val Rommel 
* Heather Sossaman as Laura Barns 
* Mickey River as Dank Jimmy 
* Cal Barnes as Rando Pauls

==Production==
Gabriadze was attracted to the project (then titled Offline) as it focused on the theme of bullying. He noted that the nature of bullying had changed since he was in school, as the Internet allowed for bullies to continue their actions even after school hours.   

The films title changed during shooting (and would also change prior to its theatrical release), as the films crew felt that the title of Offline was "too general and not obvious" and that the then title of Cybernatural was "more to the point of what it is".  For wide release, the film was re-titled Unfriended. Production was 16 days total, including six 12-hour days of principal photography, three days of pick-ups and then a few more reshoots. 

==Release== Universal Pictures SXSW on March 13, 2015. 

===Marketing=== teaser trailer was released with scenes from the film. The teaser shows the original title of the film which at the time was Cybernatural.  On January 12, 2015, the films first official trailer with the title Unfriended was released.  Shortly after, on February 6, 2015, the film was screened at Playlist Live, a popular convention for internet celebrities from Vine and YouTube.  On March 13, 2015, the day of the films official premiere at SXSW, scenes from the film were uploaded and a chat box appeared, where viewers could talk to Laura. Once she was finished talking, scenes appeared on the screen.  Images were also released. 

On February 13, 2015, a campaign was launched with Kik Messenger, in which Kik users could have a chat conversation with Laura.  This made use of automated responses and pre-scripted responses, while also driving users to a dedicated microsite. 

On March 13, 2015, after the films premiere at SXSW, an after-party was hosted by Blumhouse. Exclusive Never Have I Ever cards were released at SXSW later, and a "NEVER HAVE I EVER" section was set up on the films official website. Unfriended-themed photo booths were set up as well  During production, official Facebook accounts were set up for the characters in the film, and, after the premiere at SXSW, people who attended were "friended" by the official Laura Barns Facebook account. There was also a Twitter account, which tweeted attendees of the after-party. 

==Reception==

===Box office===
 , Unfriended has grossed $32.3 million worldwide, against a budget of $1 million.   

In North America, the film opened simultaneously with   and  , which opened with $41.9 million in July 2013. 

===Critical reception=== average rating of 6/10. The sites critical consensus reads, "Unfriended subverts found-footage horror clichés to deliver a surprisingly scary entry in the teen slasher genre with a technological twist."  On Metacritic, the film has a score of 59 out of 100, based on 30 critics, indicating "mixed or average reviews". 

Reception at the Fantasia Film Festival was mostly positive.     Common praise for the film centered upon its acting and visuals,  and Twitch Film commented that the film was an "interesting look at modern methods of communication and the ramifications of the new normal of always-on social interaction."  Variety (magazine)|Variety commented that while the film was "exasperating" at points, they also felt that it was clever and innovative. 

Dread Central also praised the film overall, but stated that they felt that the movies one major flaw was "the fashion in which we are trafficked to each scare- through multi-screen clicking, copying, pasting and re-sizing, basically all-around multi-tasking. It can be trying to sit through and I liken it to sitting over someones shoulder watching them web-surf... endlessly."  It was named Most Innovative Film at the Fantasia Film Festival and received a Special Mention for Feature Film.    

British film critic Mark Kermode gave the film a positive review, calling it a film which understands Skyping culture and cyber-bullying. He said, "Many people whove seen the trailer say, Youre being stalked through the internet. Just log off. The point is they cant because theyre addicted." While on one hand admitting it was a "shrieky, teen-terrorized, slasher movie," on the other hand he said it was a film about how cyber-bullying only works if you cooperate with it. 

Horror Homeroom gave Unfriended mixed reviews, arguing that while the concept of an entirely online diegesis is intriguing, its central flaw is that is "simply isnt scary", and that it is perhaps most frightening (perhaps unintentionally) in its portrait of teen amorality. 

In CinemaScore polls conducted during the opening weekend, cinema audiences gave Unfriended an average grade of "C", on an A+ to F scale. 

==See also== The Den
* Bullying and suicide
* Cyberbullying

==References==
 

==External links==
*  
*  
*  
*  
*  
*   A short story (originally entitled Nieve Blanca and the Seven Suitemates)

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 