Snow White (1933 film)
 
{{Infobox Hollywood cartoon|
| cartoon_name = Snow White
| series = Betty Boop
| image = Betty Boop in Snow White.png
| caption =
| director = Dave Fleischer
| story_artist =
| animator = Roland Crandall (as Roland C. Crandall)
| voice_actor = Mae Questel Cab Calloway (vocal chorus)
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = March 31, 1933
| color_process = Black-and-white
| runtime = 7 mins English
}}
 
Snow-White is a film in the Betty Boop series from Max Fleischers Fleischer Studios directed in 1933. Dave Fleischer was credited as director, although virtually all the animation was done by Roland Crandall. Crandall received the opportunity to make Snow-White on his own as a reward for his several years of devotion to the Fleischer studio, and the resulting film is considered both his masterwork and an important milestone of The Golden Age of American animation. Snow-White took Crandall six months to complete.

==Synopsis== the Queen Bimbo and Koko to behead Betty. With tears in their eyes, they take Betty into the forest and prepare to execute her. Betty escapes into a frozen river, which encloses her in a coffin of ice. This block slips downhill to the home of the seven dwarfs, who carry the frozen Betty into an enchanted cave. Meanwhile, Koko falls down a hole and arrives at the same cave, where the evil Queen turns him into a grotesque creature as he sings the St. James Infirmary Blues. With her rivals disposed of, the Queen again asks the magic mirror who the fairest in the land is, but the mirror explodes in a puff of magic smoke that returns Betty and Koko to their normal states and changes the Queen into a hideous monster. The queen monster chases the protagonists until Bimbo grabs its tongue and, with one mighty yank turns it inside out. Betty, Koko, and Bimbo dance around in a circle of victory as the film ends.

==Note==
This plot, such as it is, is really more a framework to display a series of gags, musical selections, and animation. Critics have cited the film as having some of the most imaginative animation and background drawings from the Fleischer Studios artists. Mae Questel performs the voices of Betty Boop and the Olive Oyl-ish Queen, and Cab Calloway is the voice of Koko the Clown, singing "St. James Infirmary Blues". Kokos dancing (including some moves that look like a "moonwalk (dance)|moonwalk") during the "St. James" number is rotoscoped from footage of Cab Calloway. 

The film has been deemed "culturally significant" by the United States Library of Congress and selected for preservation in the National Film Registry. In 1994 it was voted #19 of the 50 Greatest Cartoons of all time by members of the animation field. The film is now public domain.

==See also==
 
* Snow White Snow White and the Seven Dwarfs
* List of films in the public domain

== External links ==
*  
*   on YouTube
*  

 
 

 
 
 
 
 
 
 
 
 
 
 