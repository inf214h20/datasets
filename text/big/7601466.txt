Sukob
{{Infobox film
| name = Sukob
| image = Sukob Poster.jpg
| caption = Philippine theatrical poster
| director = Chito S. Roño
| producer = Lea Calmerin Tess V. Fuentes Charo Santos-Concio Malou N. Santos
| writer = Chito Rono
| starring = Kris Aquino Claudine Barretto
| studio = Star Cinema
| distributor = Star Cinema
| released =   
| runtime = 100 min P203 Million (Worldwide) $300,454  (United States)
| country = Philippines
| language =  
}} 2006 Cinema Filipino horror Feng Shui. Sukob is considered as the highest-grossing Filipino film of all-time earning PHP 186 million until in 2009 when it was surpassed by the romance film You Changed My Life in box office. The films premise is based on a Filipino superstition in which one should not get married in the same year an immediate relative dies or is married or no siblings should be married within a year. 

==Plot== Overseas Filipino Worker Sandy (Kris Aquino) return to the Philippines with Dale (Wendell Ramos) from Dubai, United Arab Emirates in preparation for their wedding. After returning to her home, Sandy learns from a caretaker of her neighbors house that Helen, Sandys childhood friend, had died along with her family years ago. She tried to convince her mother, Tessie (Boots Anson-Roa) about what happened to Helen. A short time after the death of Helens father Dr. Quisumbing, Helen plans her wedding but is advised that she must postpone the wedding because her marriage will be cursed which one should not marry within the same year when an immediate family member passed away. Helen didnt heed the warning and resumes the wedding. A few weeks after the wedding, Helens husband dies in a plane crash. As Helen goes to the crash site, she dies in a bus accident. A few weeks later, Helens mother suddenly disappears inside her house. After several years, their house has been abandoned after their deaths and no other tenants had sold it.

Meanwhile in Aliaga, Nueva Ecija|Bibiclat, Aliaga, Diana (Claudine Barretto) and Brian (Bernard Palanca) celebrates their marriage when they were interrupted along with the other sponsors by a sound of a funeral toll from the belfry of the church. After they resume the wedding, Diana saw the glimpse of a mysterious flower girl watching her during the reception.

The next day, Sandy and Dale proceeds with their wedding. During the ceremony, Sandy became bewildered and her nose began to bleed. While Sandy resume her rites with Dale, she saw the ghostly flower girl in front of her. Meanwhile, Diana witness Brian accidentally fell off the roof of their house. She, Erning (Jhong Hilario) and the other builders take him to the local hospital but dies from his defenestration. At the morgue while Diana was mourning of Brians loss, Brians mother Belen (Raquel Villavicencio), Dianas aunt Lagring, Erning and Lagrings daughter Grace (Glaiza de Castro) arrive but Brians carcass suddenly disappears and left Dianas wedding veil, which she tried to find it earlier.
 Maurene Mauricio), was present at the accident and also saw the flower girl at the reception earlier. She recalls to Sandy in her vision about what happened to Helens wedding. After Joya left, the police give the veil to Sandy, who was anxious of how it got here after it was packed at the box along with the other wedding customs from the accident.
 bridal cord from the accident.

Later during their sleep at her house, Sandy frantically saw the flower girl in her bedroom when Dale awakes and comforts her. The couple decide to seek help from Joya and ;after they learn from Dales mother Gilda (Liza Lorena) that shell be leaving with Paola to Nueva Ecija, arrive at the bus station before Paola could leave with her daughter.

Things become more mysterious when Sandy and Diana receives their respective wedding photos. The people who died are all headless in the photos, and they realize that these people are bound to die. Joya tells Sandy that she is cursed by sukob. Sandy is shocked, as no one in her family has died recently nor gotten married.

In searching for the truth, Sandy unearths a dark family secret. Sandy goes to her hometown with Dale and while staying at a hotel, Dale was taken away by the flower girl with the candle. She goes to the police station but the station runs out of electricity, while tables and chairs begin to move by themselves. Sandy runs outside and as she drives her car, she accidentally hits Diana. She helps Diana and, after asking questions, finds out they are really half-sisters.

Both realize they were married in the same year and on the same day, and one must sacrifice a life in order to break the curse. Diana and Sandy go to the church where Diana was wed and are encountered by the ghostly flower girl. The two are entrapped at the top of the belfry and the flower girl, whose mission is to take the life of the sacrificed, attempts to touch Dianas belly. This meant that her baby is the sacrifice. Sandy stops the ghost and falls from the belfry, dying and thus breaking the curse. Diana finally meets their father, and the movie ends when he sees the ghosts of Tessie and Sandy, causing him to scream.

==Cast==
*Kris Aquino as Sandy
*Claudine Barretto as Diana
*Wendell Ramos as Dale
*Boots Anson-Roa as Tessie
*Ronaldo Valdez as Fred
*Bernard Palanca as Brian
*Liza Lorena as Gilda
*Maja Salvador as Joya
*Raquel Villavicencio as Belen
*Jhong Hilario as Erning
*Glaiza de Castro as Grace Maurene Mauricio as Paola

==International Release==
Sukob (The Wedding Curse) was released in the United States in eight theaters. It earned $300,454.

==Reception==
The film was a critical and commercial success. The film earned P186.41 Million domestically and PHP203 million worldwide.
The film was considered by some as one of the scariest Filipino horror films ever made, alongside the 2004 hit Feng Shui, also directed by Roño and also starring Kris Aquino.

==Awards and recognitions==
* 1st Gawad Genio Awards (The Annual Critics Academy Film Desk)(Zamboanga City)
*: Best Film sound engineer: Albert Michael Idioma
*: Blockbuster Film: Sukob
*: Blockbuster Film Director: Chito S. Rono
*: Blockbuster Film Actress: Kris Aquino
*: Blockbuster Film Actor: Wendell Ramos
*: Blockbuster Film Producer: Star Cinema - ABS-CBN Film Productions, Inc.

==See also==
* Star Cinema
* ABS-CBN

==External links==
*  
*  
* 

==References==
 

 

 
 
 
 
 
 