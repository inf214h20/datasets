Pop Class
{{Infobox film
| name           = Pop Class
| image          = Sam Conpcion Pop Class.jpg
| caption        = DVD Cover
| director       = Paul Soriano
| producer       = Paul Soriano Kathleen Dy-Go (Executive)
| writer         = Njel De Mesa
| based on       =
| starring       = Sam Concepcion Cheska ortega Mikki Villa Grace Tanchanco Emily Bolanos Hanna Flores Janeena Chan Roden Araneta
| music          = Ito Rapadas
| cinematography = Anne Monzon
| editing        = Mark Victor
| location       = San Beda College Alabang Universal Records
| released       =  
| runtime        =
| country        = Philippines
| language       = English
}}
 Universal Records. The concept of the album is a mini-movie-musical following the tradition of Glee (TV series)|Glee, Camp Rock and High School Musical.

==Synopsis==
Sam (Concepcion) and Cheska (Ortega) are on their way to fulfilling their pop dreams as the most promising students in their Pop Class (a Pop Performance Workshop Class they have religiously attended every summer since they were kids). But when Cheska inexplicably drops out, Sam is devastated and falls into an uninspired artistic rut. Will his best friend Cheskas coming back—years after—take him out of his misery or make matters worse (since the school is about to close)? With a spirited production of cool dance sequences and new tween music, "Pop Class" will surely make you fall in love and prove that "you can never just walk away from your dreams". 

==Cast==
 
* Sam Concepcion as Sam
* Cheska Ortega as Cheska
* Mikki Villa as Ojo
* Grace Tanchanco as Yvonne
* Maureen Santiago as Cheskas mom

Extended cast
* Roden Araneta as Dance Instructor
* Emily Bolaños as Performance Instructor

Pop Class Students
* Janeena Chan as Girl 1
* Hanna Flores as Girl 2 (Hannah Flores)
* Jacky Dos Santos as Girl 3
* Mara Tanchanco as Girl 4
* Ericka Reyes as Girl 5
* Ariel Mendoza as Classmate / Dancer
* Lucky Jeff Nasayao as Classmate / Dancer
* Chester Carmona as Classmate / Dancer
* Bernard Rayoso as Classmate / Dancer
* Mark Reyes as Classmate / Dancer
* Charlie Velasco as Classmate / Dancer
* Alfred Lauzon as Classmate / Dancer
 
 
Special Participation of:
* SAMSTERS as theater audience
 
 

==Songs==
 

On September 2011, an EP entitled "Forever Young" was released which contains the tracks from the Pop Class Album.

{| class="wikitable"
|-
! Song Title !! Sung by !! Scene
|-
| Hala Heyla Hey || Sam Concepcion || Hallway, Gymnasium
|-
| Missed You || Sam Concepcion || Classroom
|-
| Be My Girlfriend || Sam Concepcion || Practice Room
|-
| Someone || Sam Concepcion || 
|-
| Dream On || Sam Concepcion feat. Cheska Ortega || Auditorium
|}

==References==
 

==External links==
*  

 