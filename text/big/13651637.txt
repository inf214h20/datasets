So Close to Paradise
{{Infobox film
| name           = So Close to Paradise
| image          = SoCloseToParadise.jpg
| image_size     = 
| caption        =
| director       = Wang Xiaoshuai
| producer       = Han Sanping Tian Zhuangzhuang
| writer         = Wang Xiaoshuai Pang Ming Guo Tao Wu Tao
| music          = Liu Lin Yang Tao Liu Fang Yang Hong Yu
| distributor    = United States:   Worldwide: Fortissimo Films
| released       = Hong Kong: December 10, 1998 United States: March 9, 2001
| runtime        = 93 minutes
| country        = China
| language       = Mandarin
| budget         = 
}}	
So Close to Paradise ( ) is a   and Wang Tong.

The film follows two migrants, Dong Zi, and Gao Ping as they travel through Wuhans underground and in the process kidnap a nightclub singer. 

== Plot ==
 Guo Tao) Wu Tao). Enraged, Gao drags his friend into Wuhans underworld. Their first stop is a local bar, the Li Li Club where they go looking for Ruan Hong (Wang Tong), a Vietnamese singer who is said to know Su Wu. After speaking with her, Gao and Dong kidnap the young woman and drag her to their apartment. Gao asks Dong to leave the room after which he proceeds to rape her, while Dong peers through a crack in the door. Soon afterwards, however, Gao and Ruan suddenly become lovers, much to Dong Zis surprise and consternation.

As Gao and Ruan continue their relationship, Dong Zi becomes sullen and seemingly jealous, though it is unclear of whom. Later Ruan and Gao appear to get in a fight as Gao Ping continues to obsess over the small amount of money he has lost. Enraged, Ruan leaves the apartment with a curious Dong Zi in tow. The two talk throughout the night and Ruan reveals that she only wants to become a professional singer, though such dreams seem out of reach. She eventually returns to the apartment, but only to leave the phone number of Su Wu.

Upon finding Su Wu, Gao beats and imprisons Su in a cellar before releasing him, after Su agrees to take him to his boss. What happens next is not shown on screen, though Dong Zi narrating explains that Gao Ping somehow became caught up in a scheme with Su Wu and his boss, leading to Gao murdering an unknown person. Tensions rise, however, when it is discovered that Ruan Hong is the kept woman of Su Wus boss. Realizing the danger, Gao Ping disappears just as the boss and the police begin to close in on him. Secretly communicating with Ruan, Gao promises to take her away but fails to show up on the appointed day. Ruan is subsequently arrested when the Li Li Club is seized as a den of Prostitution in China|prostitution. When Gao finally comes back to his apartment, he is caught by the Boss whose men beat him to death.

The film then ends as Ruan, now released from prison, returns to the apartment of Dong Zi and Gao Ping. For Dong Zi, she is the only person he knows in the city. They reminisce of the dead Gao, and Ruan thanks Dong Zi for "kidnapping" her that night. Before she leaves, Dong Zi gives her a walkman with a tape of one of her songs.

== Cast ==
*Shi Yu as Dong Zi, a naïve migrant dockworker. Guo Tao as Gao Ping, Dong Zis friend, a small-time criminal.
*Wang Tong as Ruan Hong, a Vietnamese cabaret singer. Wu Tao as Su Wu, a local gangster.

== Production ==
Production for So Close to Paradise began while Wang Xiaoshuai was officially blacklisted. While Wang participated in self-criticisms and networking opportunities in an effort to be allowed to make films again, he was invited to join Tian Zhuangzhuangs production company to begin work on the screenplay of So Close to Paradise, then called The Girl from Vietnam. Berry, p. 171.  During this period, the screenplay was vetted by both Beijing Film Studio executives and individual investors, who, while supportive of the films portrayal of modern social reality, had some misgivings. This led to a shift in focus from the characters sexual impulses to the general states of being.  Wang had originally planned to begin shooting So Close to Paradise in June or July 1994 in Wuhan but was delayed until October due to continued bureaucracy problems between Wang and the state apparatus.  As a result of the shift in season, filming of Paradise proved to be far more complicated in terms of logistics than Wang had initially anticipated. Combined with extensive edits to meet government censor approval and the production of the film stretched on for years. 

While the film was financed with official backing (in contrast to Wangs earlier films), its release was nevertheless delayed and its content subject to substantial censorship by the state apparatus.  Indeed it took nearly four years before the film was finally screened.    Reasons for the problems have been speculated to revolve around both the films gritty depiction of urban life, as well as the fact that a central character is Vietnamese.       At one point, the Chinese Film Bureau (State Administration of Radio, Film, and Television|SARFT) argued that the film literally had a "funny smell," which Wang attributed to the censors general distaste with the films depressing tone and subject matter. Berry, p. 172.  When extensive edits to the films "mood" failed to appease the censors, they eventually acquiesced and approved the film, in part because the process had dragged on so long. 

== Release and reception ==

Paradise was originally shown in mainland theaters in the fall of 1998 (nearly four years after production began), and was eventually shown in Hong Kong in December of that year (under the title Take Me Off) at the 1998 Mainland-Hong Kong-Taiwan Film Festival. Following Hong Kong, it reached foreign shores under the title of So Close to Paradise.  The film received a Western premiere at the 1999 Cannes Film Festival as part of the Un Certain Regard competition.     It would go on to win a Tiger Award for Best Film at the 2000 International Film Festival Rotterdam.  Domestically, So Close to Paradise had a much more tortuous journey to cinemas. In an interview, Wang noted that as a result of the films unusually long gestation period, promotion of the film proved near impossible.  While a few cinemas may have received the film in 1998, no commercial release of So Close to Paradise took place.  Rather, So Close to Paradise had to wait another six years to be commercially released in mainland China. 
 noir qualities Suzhou River, though he ultimately states that Paradise generally fails to engage the audience. At the same time, he showers praise on the films technical aspects, stating that "Mr. Wangs extraordinary sense of color and composition reanimates some of its secondhand attitudes."  Derek Elley of Variety (magazine)|Variety, generally praising both the cast performances (as "flavorful") and the more technical aspects of the film.  The Hollywood Reporter, meanwhile, is generally negative in its review, arguing that the films simplistic noir story lacks proper "execution" leaving it "heavy-handed and lethargic." 

== References ==
 

== External links ==
*  
*  
*  
*  
*   at the Chinese Movie Database

 

 
 
 
 
 
 