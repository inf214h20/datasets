A Letter to Elia
 
{{Infobox film
| name           = A Letter to Elia
| image          = ALetterToElia2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Kent Jones Martin Scorsese
| producer       = Emma Tillinger Koskoff Martin Scorsese
| writer         = Kent Jones Martin Scorsese
| screenplay     = 
| story          = 
| based on       =  
| starring       = Elia Kazan Martin Scorsese
| music          = 
| cinematography = Mark Raker
| editing        = Rachel Reichman
| studio         = 
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
A Letter to Elia is a 2010 documentary film directed by Kent Jones and Martin Scorsese that follows the life and career of film director Elia Kazan and how he influenced Scorsese. Made from clips from films, stills, readings from Kazans autobiography, a speech he wrote on directing read by Elias Koteas, a videotaped interview done late in Kazans life, and Scorseses commentary on and off screen.

== Accolades ==
2010 Peabody Award Winner 

==References==
 

==External links==
*  
*   @PBS.org

 

 
 
 
 
 
 
 
 

 