Padosi (film)
{{Infobox film
| name           = Padosi
| image          = Padosi_(1941).jpg
| image_size     = 
| caption        = 
| director       = V. Shantaram
| producer       = Prabhat Film Company
| writer         = Vishram Bedekar
| narrator       =  Radhakrishan Anees Khatoon
| music          = Master Krishnarao
| cinematography = V. Avdhoot
| editing        = 
| distributor    =
| studio         = Prabhat Film Company
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Marathi is 1941 Hindi Aadmi (1939).    The story and dialogue were by Vishram Bedekar in the Marathi version, while the dialogues in the Hindi version by Pandit Sudarshan who also wrote the lyrics. The music director was Master Krishnarao. The famous character artist Radhakrishan made his acting debut in the film as a villain     The film starred Mazhar Khan, Gajanan Jagirdar, Anees Khatoon, Radha Kishan, Lajwanti, Sumitra, Gopal, Balak Ram. 

The story concentrated on the topical issue of Hindu-Muslim unity making a "strong plea" for it.       Muslim League. Shantaram attempted a theme showing amicable relations between the Hindus and the Muslims. To achieve a better geniality he had Mazhar Khan, a Muslim playing the role of a Hindu and Gajanan Jagirdar, a Hindu play the Muslim character.   

With his trio of social films and especially Padosi, Shantaram has been cited by author Bajaj as the third best director pre-1960 in the top ten rating following P. C. Barua for Devdas (1935 film)|Devdas (1935) and Homi Wadia for Frontier Mail (1936), followed by Mehboob Khan for Mother India (1957).   

==Plot==
The story is set in a small village in India where the villagers of different communities live in harmony. Pandit (Mazhar Khan) a Hindu, and Mirza (Gajanan Jagirdar) a Muslim, are two old friends who function as the village elders and look out for each others families. An industrialist, Onkar, arrives to construct a dam in the village. He is opposed by the two friends and the other villagers. Onkar decides to create distrust and disunity between the two communities and friends. When a house is set on fire, Mirza is made to believe it is the work of Pandit and his son. Encouraged by the villagers, he is forced to ex-communicate the two. This causes strife and the dam is constructed. Finally the dam breaks and the two old friends come together again and die in their attempt to save lives.

==Cast==
* Mazhar Khan as Thakur
* Gajanan Jagirdar as Mirza
* Anees Khatoon as Girija
* Radhakrishan as Jayaram
* Balwant Singh as Gokul
* D.D. Kashyap as Onkar
* Vasant Thengadi as Naim
* Lajwanti as Ameena
* Sumitra Devi as Munni
* Gopal as Sarju
* Balakram as Akbar
* Sarla Devi as Jodhi

==Crew==
* Director: V. Shantaram
* Producer: Prabhat Film Company
* Music: Master Krishnarao Phulambrikar
* Lyricist: Pandit Sudarshan
* Cinematographer: V. Avadhoot
* Art Direction: S. Fattelal
* Sound Recordist: Vishnupant Govind Damle|V. G. Damle
* Special effects: Prahlad Dutt
* Choreographer: Kali Bose
* Editor: A. R. Sheikh, Raja Nene

==Marathi Version==
The Marathi version Shejari had essentially the same cast and crew. However, in the Marathi version, the role of Pandit was played by Keshavrao Date, while Jagirdar played his original role of the Muslim friend, Mirza. The Marathi cast included Gajanan Jagirdar, Keshavrao Date, Chandrakant, Jayashree and Master Chhotu. 

==Review And Box-office==
The social and patriotic aspect of the film was stated to be a reason for its critical as well as commercial success as were Shantarams other socially relevant films like Duniya Na Mane, Aadmi and Shakuntala.    The film was known for its harsh criticism of the British Divide and Rule policy.. 

==Soundtrack==
The music director was Master Krishnarao who scored a ten-minute long song Lakh Lakh Chanderi” featuring a dance scene which was highly commended.    Vasant Desai was the assistant music director. The lyrics were written by Pandit Sudarshan and the singers were Krishnarao, Anees Khatoon, Balwant Singh, Balakram, Gopal and Mulia. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| Avadhapuri Sab Prem Hi Chhaava 
| Mazhar Khan/Krishnarao
|-
| 2
| Sathi Janam Maran Ka Tu 
| Balwant Singh
|-
| 3
| Kaka Abba Bade Khiladi 
| Gopal, Balakram, Balwant Singh
|-
| 4
| Ban Mein Bahar Aa Gayi 
| Anees Khatoon, Balwant Singh
|-
| 5
| Kaisa Chaya Hai Ujala Rasiya 
| Anees Khatoon, Balwant Singh
|-
| 6
| Mai Geet Sunati Rehti Hu 
| Anees Khatoon
|-
| 7
| Sari Duniya Musafir 
| Balwant Singh, Anees Khatoon
|-
| 8
| Uth Meri Pyari Beti 
| Anees Khatoon
|-
| 9
| Bharath Avadh Me Shok Manaye 
|
|-
| 10
| Mami Chail Chabili Naar 
|
|-
| 11
| Woh Din Bisar Hi Nahi More Raja
| Mulia
|-
| 12
| Rama Bin Kachhu Nahin Suhave
| Mulia
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 