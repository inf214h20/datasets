The Invisible Monster
{{Infobox film
| name = The Invisible Monster
| director =Fred C. Brannon
| image	=	The Invisible Monster FilmPoster.jpeg
| producer =Franklin Adreon
| writer =Ronald Davison Richard Webb John Crawford George Meeker Stanley Wilson
| cinematography =Ellis W. Carter
| distributor =Republic Pictures
| released       =   10 May 1950 (serial) {{Cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 118–119
 | chapter =
 }}    1966 (TV) 
| runtime         = 12 chapters / 167 minutes (serial)  100 minutes (TV)  English
| budget          = $153,070 (negative cost: $119,343) 
| awards =
}}
 Republic Serial film serial.

==Plot==
A would-be dictator and scientist, known only as The Phantom Ruler, has developed a formula which, when sprayed on some solid object, renders that object and everything it contains invisible when exposed to rays emitted by a special lamp, also his own invention.  Covered from head to toe in formula-treated cloth, he thus moves about unseen, presently with the objective of stealing enough money and formula components to render an entire army of willing followers invisible.  Two henchmen assist him, along with several illegal aliens smuggled into the US by him and used to infiltrate, as employees, possible sites for him to later rob while invisible.  When he successfully robs a bank vault, an investigator from the banks insuror teams up with a woman police detective to solve the mystery of the money which to all outside appearances has just vanished.  Tracking clues and interrupting other attempts by the Phantom Ruler to commit crimes, the protagonists round up enough evidence that they are not merely dealing with an ordinary crime ring.  Eventually they discover the invisibility fluid and lamp, and the Phantom Ruler is killed when he trips over an open high-power electric cable he had laid on the floor of his den to do in the forces of law and order closing in upon him.

==Cast== Richard Webb as Lane Carson
* Aline Towne as Carol Richards
* Lane Bradford as Burton
* Stanley Price as The Phantom Ruler. The villain of the serial uses the trappings of the Mystery Villain but his identity is revealed to the audience in the first chapter. {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 51
 | chapter = 3. The Six Faces of Adventure
 }}  John Crawford as Harris
* George Meeker as Harry Long

==Production==
The Invisible Monster was budgeted at $153,070 although the final negative cost was $152,115 (a $955, or 0.6%, under spend). 

It was filmed between 7 March and 30 March 1950 under the working title The Phantom Ruler.   The serials production number was 1707. 

===Stunts=== Tom Steele as Lane Carson (doubling Richard Webb)
* Dale Van Sickel as Harry Long (doubling George Meeker)

===Special effects===
Special effects created by the Lydecker brothers.

==Release==
===Theatrical===
The Invisible Monsters official release date is 10 May 1950, although this is actually the date the sixth chapter was made available to film exchanges. 

===Television===
The Invisible Monster was one of twenty-six Republic serials re-released as a film on television in 1966.  The title of the film was changed to Slaves of the Invisible Monster.  This version was cut down to 100-minutes in length. 

==Critical reception==
Cline describes this serial as just a "quickie." {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 91
 | chapter = 5. A Cheer for the Champions (The Heroes and Heroines)
 }} 

==Chapter titles==
# Slaves of the Phantom (20min)
# The Acid Clue (13min 20s)
# The Death Car (13min 20s)
# Highway Holocaust (13min 20s)
# Bridge to Eternity (13min 20s)
# Ordeal by Fire (13min 20s)
# Murder Train (13min 20s)
# Window of Peril (13min 20s)
# Trail to Destruction (13min 20s)
# High Voltage Danger (13min 20s) - a clipshow|re-cap chapter
# Deaths Highway (13min 20s)
# The Phantom Meets Justice (13min 20s)
 Source:   {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 250
 | chapter = Filmography
 }} 

==References==
  

==External links==
* 
* 

 
{{Succession box Republic Serial Serial
| before=Radar Patrol vs Spy King (1949 in film|1949)
| years=The Invisible Monster (1950 in film|1950)
| after=Desperadoes of the West (1950 in film|1950)}}
 

 
 

 
 
 
 
 
 
 
 