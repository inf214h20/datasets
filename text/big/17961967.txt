Hallucinations of a Deranged Mind
{{Infobox film
| name           = Hallucinations of a Deranged Mind 
| image          = Hallucinations oadm.gif
| caption        = Theatrical release poster
| director       = Jose Mojica Marins 
| producer       = Jose Mojica Marins
| writer         = Jose Mojica Marins Rubens Francisco Luchetti 
| starring       = Jose Mojica Marins Jorge Peres Magna Miller
| music          = Beto Strada	
| cinematography = Giorgio Attili
| editing        = Nilcemar Leyart
| studio         = Produções Cinematográficas Zé do Caixão
| distributor    = Central de Distribuição de Filmes Cinematográficos
| released       = August 2, 1978
| runtime        = 86 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}
 1978 Cinema Brazilian horror film directed by José Mojica Marins. Marins is also known by his alter ego Zé do Caixão (in English, Coffin Joe). The film features Coffin Joe as the central character, although it is not part of the "Coffin Joe trilogy".         

==Plot==
The story is built around a montage of scenes that were omitted or censored from four of Marins earlier films: Awakening of the Beast, This Night Ill Possess Your Corpse, The Bloody Exorcism of Coffin Joe and The Strange World of Coffin Joe. Marins filmed approximately 35 minutes of new scenes, also adding the characters to the plot. Marins portrays himself as well as the character of Coffin Joe in the film.

The story is built around Dr.Hamílton, a psychiatrist who is terrorized by nightmares in which Coffin Joe tries to steal his wife. His colleagues decide to seek medical help with the assistance of filmmaker Jose Mojica Marins (appearing as himself), who tries to reassure Dr. Hamílton that Coffin Joe is merely a creation of his mind.   

==Cast==
*José Mojica Marins as Himself/Coffin Joe
*Jorge Peres as Dr. Hamilton
*Magna Miller as Tânia, wife of Dr. Hamilton
*Jayme Cortez
*Lírio Bertelli
*Anadir Goi
*João da Cruz
*Alexa Brandwira
*Walter Setembro
*Natalina Barbosa

==References==
 

== External links ==
*   
* 
*  on    
* 

 

 
 
 
 
 
 
 