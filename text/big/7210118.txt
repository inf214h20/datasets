Land of Look Behind
{{Infobox Film
| name           = Land of Look Behind
| image          = LandOfLookBehind.jpg
| image_size     = 
| caption        = DVD cover Alan Greenberg
| producer       = Alan Greenberg
| writer         = 
| narrator       = 
| starring       = Bob Marley Louie Lepkie Mutabaruka Gregory Isaacs
| music          = 
| cinematography = Jörg Schmidt-Reitwein
| editing        = 
| distributor    = Solo Man Subversive Cinema
| released       = December 26, 1982
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}} Alan Greenberg, and was the directors first film.  The films cinematographer was Jörg Schmidt-Reitwein, an associate of Werner Herzog.  The musical score is by K. (Kerry) Leimer. 
 Rastafari are interviewed, and performers Gregory Isaacs and Mutabaruka are also featured. In addition, Father Amde Hamilton of The Watts Prophets performs a spoken word piece during Marleys funeral service.

Land of Look Behind won the  .

Werner Herzog has said in the films DVD commentary that "This film achieves things never seen before in the history of cinema."  The American director Jim Jarmusch writes in the DVD liner notes that Land of Look Behind is "striking...beautiful...near-perfect." 

In 2007 the film was released on DVD, with interviews of and commentary by Greenberg and Herzog.

==External links==
* 
*  
* 
* 


 
 
 
 
 
 
 
 
 
 


 