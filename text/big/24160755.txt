Now & Forever (2002 film)
{{Infobox film
| name           = Now & Forever
| image          = Now_and_Forever_(2002_film).jpg
| caption        =
| director       = Bob Clark
| writer         =
| narrator       =
| starring       = Adam Beach Mia Kirshner Gordon Tootoosis Simon R. Baker
| music          = Paul Zaza
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 101 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Now & Forever is a 2002 romance film directed by Bob Clark.

==Synopsis==
Against a backdrop of clashing cultures, John Myron (Adam Beach) and Angela Wilson (Mia Kirshner) find each other and over the years form a powerful bond. One tragic night, John rescues Angela from a wicked act of betrayal. Faced with its aftermath, Angela flees town, unaware that she has put into motion a dramatic and intense string of events that will forever change the course of their lives. Harboring a secret, John guides Angela to a shocking realization that will uncover the past. It is a dramatic contemporary love story combining elements of spirituality, heart and integrity.

==External links==
* 

 

 
 
 
 
 


 