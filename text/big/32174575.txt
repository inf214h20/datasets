Homeless Joe
 
 
 
}}
{{Infobox film
| name = Homeless Joe

| image = Homeless_Joe_Poster.jpg

| producer = Mike Merickel Patty Sharkey

| director = Bruce Fordyce

| editor = Bruce Fordyce
| starring = Eric Stayberg Nicola Lambo Antonio Reynolds Tawny Young Mike Hollinger Kristina Citi Jonathan Coco E. James Fordyce Matt Hamilton Kevin Kennedy

| distributor = Troma Entertainment

}}
Homeless Joe is a 2010 horror film directed by Bruce Fordyce and produced by Mike Merickel and Patty Sharkey. Homeless Joe is being distributed globally by Troma Entertainment, Inc.

== Synopsis ==
Sean Goodman suffers from perpetual anxiety and is constantly having to take pills to calm himself. He joins a group of friends as they head to a party. On the way, his friend Dale beats up a homeless man who gets in his way. As the party ends, they find the tires on their vehicle have been slashed and so head to another girl’s house a few blocks away to get a ride back. But on the way there, someone starts attacking and abducting the members of the group. With Sean the only one left, he must follow their mystery attacker into the sewer tunnels. There he finds that a laboratory where a series of secret experiments has produced Homeless Joe, an artificially created serial killer designed to create urban mayhem. Sean rescues his friends but as they escape back to the surface, Homeless Joe comes hunting and slaughtering them.

== Cast ==
* Eric Stayberg as Sean Goodman
* Kevin Kennedy as Homeless Joe
* Nicola Lambo as Lisa
* Antonio Reynolds as Dale
* Tawny Young as Kat
* Mike Hollinger as Burrows
* Kristina Citi as Denise
* Jonathan Coco as Monte
* E. James Fordyce as Dr. Klauss
* Matt Hamilton as The Stoner
* Sean Coffelt as The Captive

The film was shot in June 2008 in Lake Forest, California, Huntington Beach, California and Costa Mesa, California. Its budget is estimated at $40,000.

== External links ==
*  
*  
*  
*  

 
 
 
 


 