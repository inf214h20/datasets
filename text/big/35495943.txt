Cowboy Up
{{Infobox film
| name           = Cowboy Up 
 aka. Ring of Fire (UK TV 2015)
| image          = Cowboy Up.jpg
| image_size     = 
| caption        = 
| director       = Xavier Koller
| producer       = Al Corley (producer) Eugene Musso (producer) Bart Rosenblatt (producer) James Redford
| narrator       = 
| starring       = Kiefer Sutherland
| music          = Daniel Licht
| cinematography = Andrew Dintenfass	 	 	
| editing        = Anthony Sherin
| studio         = 
| distributor    = Buena Vista Home Entertainment Columbia TriStar Home Entertainment
| released       = July 11, 2001
| runtime        = 105 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Marcus Thomas.  It won the Crystal Heart Award at the 2001 Heartland Film Festival. 


==Plot== Marcus Thomas) is recovering from injuries that almost took his life. Against the wishes of his mother Rose Braxton (Melinda Dillon) and girlfriend Connie (Molly Ringwald), Ely starts riding bulls again to achieve his dream of becoming a champion bull rider like his father, Reid Braxton (Pete Postlethwaite). Elys older brother, Hank (Kiefer Sutherland), is a champion bullfighter and stock contractor, and the two use each other to better their rodeo skills and work together at their family ranch in Santa Maria, California. As Elys rodeo career becomes highly successful, he starts a relationship with Celia Jones (Daryl Hannah), who is a barrel racer and Hanks love interest. Hank is consumed with anger and jealousy at Elys betrayal and the brothers become estranged.

At the championship rodeo in Las Vegas, Ely, having ended his relationship with Celia, draws Hanks unridden and greatly feared bull Zapata and asks for Hanks help. Hank tells Ely to let go of the past memories  he has being carrying around his entire life, and gives Ely the address of their fathers house in Las Vegas. Ely meets his father, but his father does not recognize Ely and thinks he is a rodeo reporter, but Ely says he is just a bull rider. Ely realizes his father is not the hero he has idolized.

Ely successfully rides Zapata, but is injured; in the process, Hank rushes to save Ely from Zapata, but is killed when Zapata crushes his chest. When the Braxton ranch hand and family friend Joe (Russell Means) brings Zapata back to the Braxton ranch, Rose grabs a shotgun and almost shoots Zapata in her grief over Hanks death, but Joe and Ely talk her down. They say they will shoot Zapata, but Ely remembers Hanks pride in the bull and shoots into the air as Zapata calmly walks into the pasture as Joe tells Ely that Hank would not have shot Zapata.

==Cast==
*Kiefer Sutherland as Hank Braxton Marcus Thomas as Ely Braxton
*Daryl Hannah as Celia Jones
*Melinda Dillon as Rose Braxton
*Molly Ringwald as Connie
*Russell Means as Joe
*Anthony Lucero as Jed
*Bo Hopkins as Ray Drupp
*Pete Postlethwaite as Reid Braxton
*Donnie Gay as himself
*Pam Minick as herself

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 