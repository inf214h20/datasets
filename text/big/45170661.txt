Beluvalada Madilalli
{{Infobox film 
| name           = Beluvalada Madilalli
| image          =  
| caption        = 
| director       = Geethapriya
| producer       = Smt Vanajakshi Devaraj
| story          = H. Devirappa (Based on Novel Beluvalada Madilalli)
| writer         = Geethapriya
| screenplay     = Geethapriya Rajesh Kalpana Kalpana Balakrishna Balakrishna Venkatarao Thalageri
| music          = Rajan-Nagendra
| cinematography = T. V. Balu
| editing        = Bal G. Yadav
| studio         = Nirupama Movies
| distributor    = Nirupama Movies
| released       =  
| runtime        = 137 min
| country        = India Kannada
}}
 1975 Cinema Indian Kannada Kannada film, Balakrishna and Venkatarao Thalageri in lead roles. The film had musical score by Rajan-Nagendra. 

==Cast==
  Rajesh
*Kalpana Kalpana
*Balakrishna Balakrishna
*Venkatarao Thalageri
*Shakthi Prasad
*Master Nataraj
*B. S. Jayakumar
*Chandrashekar
*Devaraj
*V. Gopinath
*Pandu
*Sharath
*Rajashekar
*B. Jayashree
*Girija
*Indrani
*Baby Vani
*Jyothilatha
*Madhuri
*Hemavathi
*Lakshmi
*Kamal D. Kallimani
*Vijaykumar
*Hanumesh
*Master Yogish
*Master Srinivas
*Shanthala
*Baby Savitha
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Geetha Priya || 03.28
|-
| 2 || Muthu Malegagi Oothu || Vani Jayaram || Geetha Priya || 03.39
|- Janaki || Geetha Priya || 03.42
|-
| 4 || Yellarana Kayo Dyavre || S. P. Balasubrahmanyam || Geetha Priya || 03.38
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 


 