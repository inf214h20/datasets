Gasp (2012 film)
{{Infobox film
| name = Gasp
| image          = Gasp_official_poster.jpg
| caption        = Official Poster
| border         = yes
| director = Eicke Bettinga
| producer = Samuel Huang
| writer = Eicke Bettinga Samuel Huang
| starring = Jan Amazigh Sid Gianni Scülfort Julie Trappett Philippe Jacq
| cinematography = Eicke Bettinga
| editing = Oliver Szyza
| runtime = 15 minutes
| released =  
| country = Germany
| language = German, French
}}

Gasp is a German short film drama written and directed by Eicke Bettinga. The film premiered in the short film competition at the 2012 Cannes Film Festival where it was nominated for the Short Film Palme dOr. 

==Plot==
Its the story about a teenager who only has one wish: to feel something - anything. One day he pushes his longing to the limit.

== Critical reception ==
"Eicke Bettinga touches on the big, abstract themes of solidarity, of human connection, of isolation, numbness and sacrifice."—Cannes Nisimazine   

==References==
 

== External links ==
* GASP at the 65th  
*  

 
 
 
 
 
 
 
 


 
 