Winner Takes All (2000 film)
 
 
{{Infobox film
 | name          = Winner Takes All
 | image         = Winnertakrsall.jpg
 | caption       = Poster of the movie
 | director      = Clifton Ko Raymond Wong Raymond Wong Annie Wu Raymond Wong
 | music          =
 | cinematography =
 | editing        =
 | distributor    = China Star Entertainment Group
 | released       = 2000
 | runtime        = 91 minutes
 | country        = Hong Kong
 | awards         = Cantonese
 | budget         =
 | gross          =
 | preceded by    =
 | followed by    =
  }}
 Hong Kong film directed by Clifton Ko. This is comedy film for Lunar New Year.

==Plot Outline==
Ferrari (Nicholas Tse), is a young swindler. He has been swindling people for money in many situations. One day, a cop nicknamed Stupid (Karl Maka) is about to catch him swindling. He makes a deal with Ferrari to help him catch Master Swindler Wong (Sam Hui), the worlds most notorious swindler who is penning his autobiography of his swindling ways. Ferrari runs into Bastardly Sze (Alec Su) who is stalking a rich woman and taking her pictures.

Paulina Wu (Joey Yung)s father is one of the victims of the Master Castrator Swindler. So she decides to get her fathers money back. On her trip, she runs into Bastardly Sze and Ferrari. The three wants swindle the girl, whom they think is Master Swindler Wongs daughter, Wu Sen Kwan (Ruby Lin).

With Wu Sen Kwan is her so-call assistant, Ching (Annie Wu). Ching and Ferrari once met before. Ferrari and Ching were going out on a date. Ching asked Ferrari wait for her to come, she never did. Later, he learned Ching had swindled him.

The group (Ferrari, Bastardly Sze, and Paulina) are following fake Swindler Wongs daughter to the boat. They have a plan to swindle her money. Ferrari pretends to be a priest. He asks Swindler Wongs daughter to donate $1 million for charity. During that time, Bastardly Sze is falling for Wu Sen Kwan. Paulina is jealous of Wu Sen Kwan because she likes Bastardly Sze. Sze feels bad in swindling Sen Kwan.

==Cast and roles==
* Nicholas Tse - Ferrari
* Ruby Lin - Wu Sen Kwan Annie Wu - Ching
* Joey Yung - Paulina Wu
* Alec Su - Bastardy Sze Raymond Wong - Father Wong
* Karl Maka - Inspector Stupid
* Sam Hui - Master Swindler Wong
* Sandra Ng
* Ricky Hui
* Perry Chiu

==See also==
* List of Hong Kong films

==External links==
* 
*  at LoveHKFilm.com
* 

 
 
 
 
 
 


 