Sand (1949 film)
{{Infobox film 
 | name = Sand
 | director = Louis King 
 | producer = Robert Bassler Darryl F. Zanuck (uncredited) Will James (novel) Mark Stevens Coleen Gray Rory Calhoun
 | music = Daniele Amfitheatrof
 | cinematography = Charles G. Clarke
 | editing =Nick DeMaggio
 | distributor = 20th Century Fox 
 | released =  
 | runtime = 78 minutes
 | language = English
 | country = United States
 | budget = 
}}
 Western that Best Cinematography (color)-which Charles G. Clarke was nominated for.    

==Plot==

Based on the 1932 novel of the same name, Jeff Keanes expensive show horse escapes and runs loose in the Colorado wilderness.

==Cast==

===Credited Cast Members=== Mark Stevens as Jeff Keane
*Coleen Gray as Joan Hartley
*Rory Calhoun as Chick Palmer
*Charley Grapewin as Doug Robert Patten as Boyd (as Bob Patten)

===Uncredited cast Members===

*Robert Adler
*Paul E. Burns
*Harry Cheshire
*Davison Clark
*Iron Eyes Cody
*Joseph Cody
*Mikel Conrad
*Charles Vernon David Cypert
*William Monroe Cypert
*Ben Erway
*Jack Gallagher
*Paul Hogan
*Tom London
*J. Farrell MacDonald
*George Matthews
*George Melford
*Suns Red Shadow
*Jay Silverheels Bill Walker
*Colin Ward

==References==
 

== External links ==
*  
* 
*  

 
 
 
 
 
 
 


 