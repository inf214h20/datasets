Come Undone (film)
 
{{Infobox film
| name = Come Undone
| image = Come Undone.jpg
| caption =
| director = Silvio Soldini
| producer = Lionello Cerri Ruth Waldburger
| writer = Silvio Soldini Doriana Leondeff Angelo Carbone
| starring = Alba Rohrwacher Pierfrancesco Favino
| music = Giovanni Venosta
| cinematography =
| editing = Carlotta Cristiani
| distributor =
| released =  
| runtime =
| country = Italy
| language = Italian
| budget =
| gross =
}}
Come Undone ( ) is a 2010 film directed by Italian director Silvio Soldini.

==Plot==
Anna (Alba Rohrwacher) works at an insurance company and is married to Alessio (Giuseppe Battiston) who wants to have a baby. She then happens to meet Domenico (Pierfrancesco Favino), headwaiter at a local restaurant. The two start a passionate relationship, but their personal life gets in the way.

==Cast==
* Alba Rohrwacher as Anna
* Pierfrancesco Favino as Domenico
* Giuseppe Battiston as Alessio, Annas husband
* Teresa Saponangelo as Miriam, Domenicos wife
* Fabio Troiano as Bruno
* Adriana De Guilmi as Annas mother
* Gigio Alberti as Annas boss
* Ninni Bruschetta as Domenicos brother

==Awards==
* Festival du Film de Cabourg 2010: Grand Prix

==External links==
*  

 
 
 
 
 
 


 