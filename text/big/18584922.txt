Trade Winds (film)
{{Infobox film
| name           = Trade Winds
| image          = TRADE WINDS 1938.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Tay Garnett
| producer       = Tay Garrett Walter Wanger  (executive) Alan Campbell Frank R. Adams
| story          = original story Tay Garnett
| starring       = Fredric March Joan Bennett Alfred Newman
| cinematography = Rudolph Mate, A.S.C. Foreign exterior photography James B. Shackelford, A.S.C.
| editing        = Otho Lovering Dorothy Spencer 
| studio         = Walter Wanger Productions, Incorporated A Tay Garnett Production
| distributor    = United Artists
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $738,733 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p439 
| gross = $964,404 
}} Alan Campbell and Frank R. Adams, based on story by Tay Garnett.

==Plot==
Former police detective Sam Wye is on the trail of socialite Kay Kerrigan who is accused of fatally shooting millionaire cad Thomas Bruhme. Kay has taken a ship to the South Seas followed by Sam, Sams secretary Jean Livingstone as well as police detectives Ben "Homer" Blodgett and George Faulkner. Along the way, through various adventures, Sam and Kay fall in love, as do Homer and Jean. Sam eventually determines that the actual killer was a jealous husband whose wife was having an affair with Bruhme. Kay is thus cleared and free to marry Sam.

==Cast==
  
*Fredric March as  Sam Wye 
*Joan Bennett as  Kay Kerrigan
  
*with Ralph Bellamy as  Ben Blodgett 
*Ann Sothern as  Jean Livingstone 
*Sidney Blackmer as  Thomas Bruhme II  Thomas Mitchell as  Commissioner Blackton  Robert Elliott as  Detective George Faulkner
 

==Release==
The film earned a profit of $71,129. 

==References==
 

==External links==
* 
*  
*  at Allmovie
*  by Ned Scott
Streaming audio
*  on Lux Radio Theater: March 4, 1940 
*  on the Screen Directors Playhouse: May 29, 1949

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 