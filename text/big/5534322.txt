Goodbye Bafana
 
 
{{Infobox film name = Goodbye Bafana image = Goodbye bafana.jpg caption = Cinema poster James Gregory (book) Greg Latter (screenplay) starring = Dennis Haysbert Joseph Fiennes Diane Kruger producer = director = Bille August cinematography = Robert Fraisse editor = Hervé Schneid music = Dario Marianelli released =   runtime = 140 minutes country = Germany France Belgium Italy South Africa language = Xhosa
|budget = $30,000,000
}} James Gregory censor officer and prison guard, based on Gregorys book Goodbye Bafana: Nelson Mandela, My Prisoner, My Friend. The movie also explores the relationship of James Gregory and his wife as their life changes while Mandela is under Gregorys watch.

Bafana means boys. Gregory lived on a farm and had a black friend when he was a child, and that is the reason he is able to speak Xhosa language|Xhosa.

==Plot==
The young revolutionary Nelson Mandela is arrested, and it is the task of censor James Gregory to watch him. He has long since moved to South Africa with the family for his work in the prison of Robben Island, and slowly he clashes with the politics and racist culture of his countrymen and the people of his own race. Gregory begins to express hatred for South African apartheid. In time, Gregory challenges his superiors, and seeks to improve Mandelas life until he is released from prison after twenty-seven years of imprisonment, and is elected president of South Africa.

==Cast== James Gregory
*Dennis Haysbert as Nelson Mandela
*Diane Kruger as Gloria Gregory
*Patrick Lyster as Maj. Pieter Jordaan
*Norman Anstey as Jimmy Kruger
*Shiloh Henderson as Brent Gregory
*Tyrone Keogh as Brent Gregory
*Megan Smith as Natasha Gregory
*Jessica Manuel as Natasha Gregory
*Faith Ndukwana as Winnie Mandela
*Terry Pheto as Zindzi Mandela
*Leslie Mongezi as Walter Sisulu
*Zingizile Mtuzula as Raymond Mhlaba (as Zingi Mtuzula)
*Mehboob Bawa as Ahmed Kathrada
*Shakes Myeko as Andrew Mlangeni
*Sizwe Msutu as Cyril Ramaphosa

==Factual basis==
The autobiography the film was based on, Goodbye Bafana: Nelson Mandela, My Prisoner, My Friend, was derided by Mandelas longtime friend, the late   he accused James Gregory, who died of cancer in 2003, of lying and violating Mandelas privacy in his work Goodbye Bafana. Sampson said that Gregory had rarely spoken to Mandela, but censored the letters sent to the prisoner and used this information to fabricate a close relationship with him. Sampson also claimed that other warders suspected Gregory of spying for the government, and that Mandela considered suing Gregory. 

Writing in The Guardian, critic, Alex von Tunzelmann, stated that the movie was a "dubious tale" of Nelson Mandelas imprisonment, based on his prison guards memoirs and that it was a story that contradicted all other known accounts of his time in imprisonment. She went on to say that there was no excuse for the "historical negligence in this movie" – stating that its implicit dismissal of the contradictory accounts of Nelson Mandela and others could be seen as insulting. 
 Long Walk to Freedom, Nelson Mandela mentions James Gregory in two occasions. The first was during his imprisonment in Pollsmoor Prison|Pollsmoor:
  
The second occasion that Mandela mentions Gregory in his autobiography is on the day of his release in 1990 from prison:
 

The The Making of Goodbye Bafana, on the Goodbye Bafana DVD, contains an interview with Nelson Mandela where he speaks of James Gregory:  

==References==
 

==External links==
*  
*   Retrieved 2012-07-10

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 