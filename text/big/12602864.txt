My Dearest Senorita
{{Infobox film
| name           = My Dearest Senorita
| image          = Mi querida señorita.jpg
| image_size     = 
| caption        = Spanish film poster
| director       = Jaime de Armiñán
| producer       = Luis Megino	
| writer         = José Luis Borau Jaime de Armiñán
| narrator       = 
| starring       = José Luis López Vázquez Julieta Serrano Antonio Ferrandis
| music          = Rafael Ferro
| cinematography = Luis Cuadrado          
| editing        = Ana Romero Marchent  
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

My Dearest Senorita ( ) is a 1972 Spanish film directed  by Jaime de Armiñán. A black comedy on the subject of sex change, it was the first Spanish film that talked about sexual orientation, which was a taboo subject in Spain during Francisco Franco|Francos regime.

==Plot==
Adela is a 43-year-old spinster who lives quietly alone in an isolated northern provincial Spanish village. With no other accomplishments of a "lady of rank" and a small annuity, she spends her days sewing and doing charity work. Never feeling particularly attracted to men, she is waited upon in her home by a faithful ladys maid named Isabelita, who adores her.

One day, the local bank manager starts to court Adela and sets his sights on marriage. Adela, repelled by the physical contact of her suitor, resolves, after an argument with her Isabelita over the situation, to consult a doctor. Adela fires Isabelita, but sees the doctor anyway.  Adela discovers after the consultation that she is not a woman after all, but a man. The former Adela takes then a new masculine identity as "Juan", and moves from the village to Madrid.

Juan arrives in Madrid and accidentally meets Isabelita, the servant girl he fired when he was Adela. Juan searches for a job and a new purpose in his life with his new identity. Life is very hard in Madrid and Juan uses his sewing skills to bring him a small income and enable him to obtain a work permit. However, he has problems securing an identity card.

As Juan prospers, he falls in love with Isabelita, but denies himself consummation of their affair for fear of a poor sexual response. Eventually, Juan is able to fulfill his duties as a man and is successful with Isabelita. While making love to her, he warns that one day he will tell her a secret. Isabelita says then: "There is no need, señorita", showing that she has known for sometime his secret.

==Cast==
*José Luis López Vázquez	... 	Adela Castro Molina/Juan
*Julieta Serrano	... 	Isabelita
* Antonio Ferrandis	... 	Santiago
*Enrique Ávila	... 	Father José María 
*Lola Gaos	... 	Aunt Chus
*Chus Lampreave	... 	Chus niece
* Mónica Randall	... 	Feli
*José Luis Borau    ...  Doctor

==Reception==
The film had a great controversy because it was made in the late time of Franquism. However, it attracted international attention and was nominated for an Academy Award for Best Foreign Film in 1973.    José Luis López Vázquez gives a bravura performance with delicacy. The film never degenerates into a cheap, sensational comedy but is full of humor, pathos and humility. Jaime de Armiñán shows his own strong literary and theatrical influences with an originality not seen before in Spanish cinema.

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

== References ==
 

==Notes==
*Director Jaime de Armiñán and actress Julieta Serrano speak about film at  
* Schwartz, Ronald, The Great Spanish Films: 1950- 1990,Scarecrow Press, London, 1991, ISBN 0-8108-2488-4

== External links ==
*  

 
 
 
 
 
 
 
 