Vampire Circus
 
 
{{Infobox film
| name           = Vampire Circus
| image          = Vampirecircus.jpg
| alt            =
| caption        = Theatrical release poster Robert Young
| producer       = Wilbur Stark Michael Carreras
| screenplay     = Judson Kinberg
| story          = George Baxt Wilbur Stark Anthony Higgins John Moulder-Brown Lalla Ward Robin Sachs Lynne Frederick David Whitaker
| editing        = Peter Musgrave
| studio         = Hammer Film Productions
| distributor    = Rank Film Distributors Ltd   20th Century Fox  
| released       = 30 April 1972
| runtime        = 87 min.
| country        = United Kingdom
| language       = English
}}
 horror film, Robert Young. Anthony Higgins (billed as Anthony Corlan). The story concerns a travelling circus whose vampiric artists prey on the children of a 19th-century Austrian village. It was filmed at Pinewood Studios.

== Plot ==
 run the gauntlet, but when her husband intervenes, she runs back into the castle where the briefly revived Count tells her to find his cousin Emil at "the Circus of Night". After laying out his body in the crypt, she escapes through an underground tunnel as the villagers blow the castle up with gunpowder and set fire to it.
 plague and blockaded by the authorities of neighboring towns, with men ready to shoot any villager who tries to leave. The citizens fear that the pestilence may be due to the Counts curse, though the new physician Dr. Kersh scoffs at the notion, dismissing vampires as just a myth. Then a travelling circus calling itself the Circus of Night arrives at the village, led by a dwarf and an alluring gypsy woman who are ambivalent about how they got past the blockade. The villagers, appreciative of the distraction from their troubles, do not press the matter. While his courageous son Anton distracts the armed men at the blockade, Dr. Kersh gets past them to appeal for help from the capital. Neither he nor anyone back in the village suspect that one of the circus artists, Emil, is a vampire and Count Mitterhauss cousin. Emil and the gypsy woman go to the remains of the castle, where in the crypt they find the Counts staked body still preserved, and they reiterate his curse that all who killed him and all their children must die.

At the Circus of Night, the villagers are amazed and delighted by the entertainment, including an erotic performance by two dancers dressed as a tiger-woman and her trainer. Despite his wifes concerns over their wayward daughter Rosas physical attraction to the handsome Emil, the Burgermeister takes her to the circus and, at the gypsy womans invitation, visits the hall of mirrors where he sees a vision in one of a revived Count Mitterhaus, causing him to collapse. Frightened by this event, Schilt tries to flee with his family from the blocked village guided by the dwarf Michael, only to be abandoned by him in the forest to be mauled to death by the circus panther. Müllers daughter Dora, who has slipped past the blockade and is returning to the village despite her anxious father having sent her away, discovers the Schilts dismembered bodies, arousing suspicions about the animals of the circus. Anton, having been deputized by his father to stand in for him, insists that wolves or wild boars are responsible, unaware that several of the circus animals are vampire shapeshifters, including Emil, who is the panther, and twin acrobats Heinrich and Helga. That evening, Jon and his brother Gustav, two village boys whose father Mr. Hauser helped instigate the killing of Mitterhaus, are invited by the gypsy woman to enter the hall of mirrors. While looking in the mirror where the Burgermeister had his vision, they are magically drawn in by Heinrich and Helga who whisk them to the Counts crypt and drain them. After the boys bodies are found near the castle, their grieving father and the sick Burgermeister begin to shoot the circus animals. After an encounter with Emil, the Burgermeister dies of heart failure, while his daughter runs off with the vampire who then bites and kills the girl.

Dora and Anton, who are in love, are lured by the twins Heinrich and Helga into the hall of mirrors where they try to seize Dora, but the cross she is wearing saves her. Later, the vampires enter the school house where Dora and Anton have taken refuge. Emil, in panther form, kills the students, diverting Anton while the gypsy woman (now revealed as the twins human mother by Mitterhaus) tears the cross from Doras neck, enabling Heinrich and Helga to attack her. Dora, however, escapes into the school chapel, where the twins are overwhelmed by a giant crucifix which she topples on them, destroying them. Nevertheless, with the help of the circus strongman, who being human is impervious to crosses, Emil and the gypsy woman succeed in having Dora kidnapped and taken to the crypt at Castle Mitterhaus. There they extract drops of her blood, which they use with blood left over from the previous child victims as part of a ritual to restore the Count back to life. Meanwhile, Dr. Kersch returns from the capital with an imperial escort and medicines for the plague. He also brings news of vampire killings in other villages, all of them toured by the Circus of Night. The men attack the circus and set fire to it, killing the strongman when he tries to stop them. As Hauser starts to burn down the hall of mirrors, he sees a vision in the one mirror of Emil and the gypsy woman bleeding a helpless Dora over the Counts body. This horrifying sight distracts him long enough to be fatally burned by the fire, though he lives long enough to alert Anton and the other men to Doras plight.

Back in the castle crypt, the gypsy woman is killed when out of a sudden attack of remorse she attempts to save Dora from Emil. As she falls down dead, the gypsys face is transformed, revealing her to be Anna Müller. Anton, finding his way through the underground tunnel into the crypt despite a deadly ambush by Michael the dwarf, attempts to rescue Dora but is halted by Emil. When Anton holds the vampire back with a crucifix, an attacking bat summoned by Emil causes him to drop it, placing him at the monsters mercy. Just then Müller, Dr. Kersh, and a soldier break into the crypt and battle Emil, while Anton fends off the bat with a torch as it continues to attack him and Dora. Emil kills or disables all his attackers but Müller, having dropped the crossbow he brought, pierces him with the stake from the Counts chest as he dies. Revived at last, the Count rises from his sarcophagus and advances on Dora and Anton. Then Anton seizes Müllers crossbow which is shaped like a crucifix, repelling the Count long enough for the young man to throw the crossbow over his head and fire an arrow into the vampires neck, decapitating him. As Dr. Kersh leads Dora and Anton from the tomb, he and the villagers set the ruins alight with torches, ending the curse.

== Cast ==

* Adrienne Corri as Gypsy Woman
* Laurence Payne as Professor Albert Müller
* Thorley Walters as Peter, the Mayor of Stitl
* Lynne Frederick as Dora Müller
* John Moulder-Brown as Anton Kersh
* Elizabeth Seal as Gerta Hauser Anthony Higgins (billed as Anthony Corlan) as Emil Richard Owens as Dr. Kersh
* Domini Blythe as Anna Müller
* Robin Hunter as Mr Hauser
* Robert Tayman as Count Mitterhaus
* Robin Sachs as Heinrich (twin brother of Helga)
* Lalla Ward as Helga (twin sister of Heinrich)
* Skip Martin as Michael the dwarf
* David Prowse as the Strongman
* Mary Wimbush as Elvira
* Christina Paul as Rosa
* Roderick Shaw as Jon Hauser
* Barnaby Shaw as Gustav Hauser
* John Bown as Mr Schilt
* Sibylla Kay as Mrs. Schilt
* Jane Darby as Jenny Schilt
* Dorothy Frere as Granma Schilt
* Milovan Vesnitch as the erotic male dancer
* Serena as the erotic tiger-woman dancer
* Sean Hewitt as First Soldier
* David de Keyser as the voice of Mitterhauss curse (uncredited) fantasy series Doctor Who in the serial The Leisure Hive. The film also heralded the screen debut of Lynne Frederick, who would later marry comic Peter Sellers. David Prowse, who later played Darth Vader in the first Star Wars trilogy, appears in a silent role as the circus strongman. Robin Sachs would later appear later in his career as a recurring villainous character Ethan Rayne on "Buffy the Vampire Slayer" and the space conqueror Sarris in the science-fiction comedy "Galaxy Quest".

== Critical reception ==

Vampire Circus has been well received by modern critics, and currently holds an 80% approval rating on movie review aggregator website Rotten Tomatoes. 
 AllMovie called the film "one of the studios more stylish and intelligent projects".  PopMatters also called it "one of the companys last great classics", writing, "erotic, grotesque, chilling, bloody, suspenseful and loaded with doom and gloom atmosphere, this is the kind of experiment in terror that reinvigorates your love of the scary movie artform.". 

Critics at the time of its original release werent quite as impressed. New York Times film reviewer Howard Thompson dismissed it outright without even the courtesy of a proper review in favor of its double-billing Hammer counterpart "Countess Dracula". His curt review measured two sentences, "Wise horror fans will skip Vampire Circus and settle for Countess Dracula on the new double bill at the Forum. Both are Hammer Productions, Englands scream factory, but the first was dealt a quick, careless anvil." before continuing with semi-praise for Countess Dracula.   

== Novelization ==
 Mark Morris was published in 2012. 

==See also==
*Vampire film

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 