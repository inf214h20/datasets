Weekend of Shadows
{{Infobox film
| name           = Weekend of Shadows
| image          = 
| image size     =
| caption        = 
| director       = Tom Jeffrey
| producer       = Matt Carroll
| writer         = Peter Yeldham
| based on = novel The Reckoning by Hugh Atkinson
| narrator       = John Waters
| music          = Charles Marawood
| cinematography = 
| editing        = Rod Adamson
| studio = South Australian Film Corporation
| distributor    = 
| released       = 12 April 1978
| runtime        = 
| country        = Australia English
| budget         =AU$495,000 David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p121-122 
| gross = AU$61,000 (Australia) 
| preceded by    =
| followed by    =
}}
Weekend of Shadows is a 1978 film directed by Tom Jeffrey.

==Plot==
In the 1930s, a farmers wife in a small town is murdered. Suspicion falls on a Polish labourer and a posse is formed to catch him.

==Cast== John Waters ... Rabbit
*Melissa Jaffer ... Vi
*Wyn Roberts ... Sergeant Caxton
*Graham Rouse ... Ab Nolan
*Graeme Blundell ... Bernie Collins Bill Hunter ... Bosun
*Bryan Brown ... Bennett

==Production==
Writer Peter Yeldham later called the film a "real disaster... we had constant changes and insecurity about it, right up to the day of shooting. I think many of these changes didnt help the film." 
 Macclesfield in the Adelaide Hills. The film was a commercial disappointment. Director Tom Jeffrey:  I screened the film to Hugh Atkinson, the writer of the novel, after we had finished it and he came up to me afterwards and I remember him saying quite clearly, "Youve got it." I said, "What do you mean?" He said, "You understand the ending." And I still didnt know whether I had it right, whether I was on his wavelength, and I said, "Please explain, Hugh." And he said, "Its about the Crucifixion." And I said, "Yes, well, I didnt really realise that, but if you can see that in the ending and if thats what your original intention was and Ive unwittingly achieved that for you, Im very pleased.".. The film was actually looking at the power of the group and how it can get out of hand.  

==References==
 

==External links==
*  at IMDB
*  at Oz Movies
 
 
 
 
 