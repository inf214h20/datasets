Cyclone (1987 film)
 
{{Infobox film
| name           = Cyclone
| image          = Cyclone87film.jpg
| image_size     = Promotional film poster
| director       = Fred Olen Ray
| producer       = Paul Hertzberg
| writer         =  
| narrator       =
| starring       = Heather Thomas Jeffrey Combs Martin Landau
| music          = Haunted Garage David A. Jackson Michael Sonye
| cinematography = Paul Elliott
| editing        = Robert A. Ferretti
| distributor    = Cinetel Films
| released       =  
| runtime        = 89 min.
| country        = United States
| language       = English
| budget         =
| gross          = $41,174 (USA)
}}
 science fiction action film about a woman who must keep the ultimate motorcycle from falling into the wrong hands. The film was directed by Fred Olen Ray, and stars Heather Thomas, Jeffrey Combs, and Martin Landau.

==Plot==
Rick has developed the ultimate motorcycle, the Cyclone. It is a $5 million bike equipped with rocket launchers and laser guns. Rick meets his fate and it is up to his girlfriend Teri to keep the Cyclone from falling into the wrong hands. Teri can trust no one but herself.

==Cast==
* Heather Thomas as Teri Marshall 
* Jeffrey Combs as Rick Davenport 
* Martin Landau as Basarian

==DVD release==
Platinum Disc released a budget DVD of the film in 2002.

Shout! Factory announced they would release Cyclone as part of a four-film "Action-Packed Movie Marathon" DVD set on March 19, 2013. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 