Doraemon: Nobita's Little Star Wars
{{Infobox film
| name           = Nobitas Little Star Wars
| image          = Nobitas Little Star Wars.jpg
| caption        =
| director       = Tsutomu Shibayama
| producer       = Junichi Kimura Masami Hatano Soichi Bessho Yoshiaki Koizumi
| writer         = Fujiko F. Fujio
| narrator       =
| starring       = Nobuyo Ōyama Noriko Ohara
| music          = Shunsuke Kikuchi
| cinematography =
| editing        = Kazuo Inoue
| studio         = Asatsu
| distributor    = Toho Company
| released       =  
| runtime        = 97 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = ¥1.20 billion  ($9.6 million)
}}
  is a feature-length Doraemon film which premiered on March 16, 1985.  As the films title suggests, it is a parody of the original Star Wars trilogy. The films theme song is performed by Tetsuya Takeda.

==Cast==
* Nobuyo Ōyama - Doraemon
* Noriko Ohara - Nobita Nobi
* Michiko Nomura - Shizuka Minamoto
* Kaneta Kimotsuki - Suneo Honekawa
* Kazuya Tatekabe - Jian

==Plot==
Papi, the tiny president of a faraway planet, escapes to Earth to avoid being captured by the military forces that took over.

Nobita is making a space movie with Gian and Suneo. After Nobita accidentally destroys the set, both Suneo and Gian "fire" him. Nobita goes to Doraemon to help him create a space movie. Failed at asking Dekisugi to help (who is asked to join by Gian and Suneo), they instead ask Shizuka for help, who prefers to make a movie about dolls, disappointing Nobita. Shizuka uses her favorite rabbit doll for the filming, but it disappears after she and the others are distracted. After Nobitas mother rediscovers the doll, Doraemon, Nobita, and Shizuka find Papi inside it, who explains that he is the president of Pirika planet, which is currently suppressed by a dictator named General Gilmore. Becoming a top target for Gilmore, Pirika is transported to Earth for safety. He is offered shelter in Shizukas doll house. Later, Gilmores top officer, Dorakorule, leads an attack on Gian and Suneos set. Learning that Dorakorule is spying on anyone who hide Papi, Nobita and his friends hide themselves from the spies (taking form of fireflies) and create a hideout in Shizukas house, which doubles as a factory for weapon-making. However, the hideout is subsequently raided, Shizuka is kidnapped, and the "Size-Changing Flashlight" (used to shrink Nobita and his friends) is taken, leaving the others stuck at their form. Papi offers himself to be taken in exchange for Shizukas safety, which Dorakorule accepted. Papis pet dog, Rokoroko arrives but is too late to save Papi. After explaining about himself, he, Nobita, and his friends go to Pirika to save Papi.

They arrive at Pirikas ring system and find the Freedom Partys hideout, where Papi and Rokoroko originated. They receive information about Papis planned execution the next day and proceed to stage his rescue; Doraemon, Nobita, Gian, and Rokoroko travel to Pirika using a spaceship, while Shizuka and Suneo guard the ring system. Shizuka and Suneo manage to fend off the military army and proceed to help the others. They arrive at the planet, but are raided and are almost drowned in the sea until they mysteriously return to their normal size. Meanwhile, Doraemon, Nobita, Gian, and Rokoroko arrive with the secret Freedom Partys hideout in the planet, but are captured by General Gilmore. They are sentenced execution, but eventually grown back to their normal size and meet up with Shizuka and Suneo, realizing that they had passed the flashlights power limit. They proceed to battle the military army and when Gilmore tries to escape, he is captured by the Pirikas citizens. Saying goodbye to Papi, Nobita and his friends go back to Earth.

==Television broadcast==
The film aired in India on Hungama TV on 5 December 2010 and Disney Channel India on 26 December 2010 as Doraemon in Nobitas Little Space War.

==References==
 

== External links ==
*    
*  

 
 

 
 
 
 
 
 
 