Compliments of Mister Flow
{{Infobox film
| name =Compliments of Mister Flow 
| image =
| image_size =
| caption =
| director = Robert Siodmak
| producer = Fernand Rivers 
| writer =  Gaston Leroux  (novel)   Henri Jeanson
| narrator =
| starring = Fernand Gravey   Edwige Feuillère   Louis Jouvet
| music = Michel Michelet
| cinematography = Jean Bachelet   René Gaveau   André Thomas (cinematographer)|André Thomas  
| editing =  
| studio = Vondas Films 
| distributor = Compagnie Universelle Cinématographique 
| released = 2 December 1936 
| runtime = 100 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Mister Flow by Gaston Leroux.

==Cast==
*  Fernand Gravey as Antonin Rose 
* Edwige Feuillère as Lady Helena Scarlett 
* Louis Jouvet as Durin / Mr. Flow 
* Jean Périer as Lord Philippe Scarlett 
* Vladimir Sokoloff as Merlow 
* Jim Gérald as Le Cubain 
* Jean Wall as Pierre 
* Mila Parély as Marceline 
* Victor Vina as Garber
* Philippe Richard as Le procureur  
* Tsugundo Maki as Maki  
* Yves Gladine as Un inspecteur  
* Marguerite de Morlaye 
* Myno Burney 
* Léon Arvel   
* Georges Cahuzac

== References ==
 

== Bibliography ==
* Greco, Joseph. The File on Robert Siodmak in Hollywood, 1941-1951. Universal-Publishers, 1999. 

== External links ==
*  

 

 
 
 
 
 
 

 