Stick It
 
{{Infobox film
| name           = Stick It
| image          = Stickit.jpg
| image_size     =
| caption        = Film poster
| director       = Jessica Bendinger
| producer       = Gail Lyon
| writer         = Jessica Bendinger
| narrator       = Missy Peregrym
| starring       = Jeff Bridges Missy Peregrym Michael Simpson
| cinematography = Daryn Okada
| editing        = Troy Takaki
| studio         = Touchstone Pictures Spyglass Entertainment Buena Vista Pictures
| released       = 28 April 2006
| runtime        = 103 minutes US
| English
| budget         = $20 million 
| gross          = $31,976,848
}}
	 Bring It On; the film marks her directorial debut. It was produced by Touchstone Pictures and was released in theatres on April 28, 2006.

==Plot== World Championships, but she walked out of competition in the middle of the finals, costing the American team the gold medal and leaving many people hurt and crushed, making her one of the most hated people in gymnastics.

Haley goes to the elite Vickerman Gymnastics Academy (VGA), her ultimate nightmare, run by legendary coach Burt Vickerman (Jeff Bridges). Haley has a talk with Coach Vickerman, who convinces her to take up the sport once again&nbsp;– at least until she can enter an upcoming invitational competition. Vickerman convinces her that she can use the prize money from the competition to repay some property damage debts she still owes and leave gymnastics once and for all. Disliking the sports rigid rules and intense training schedule, Haley is reluctant to come out of retirement. Her attitude toward her fellow gymnasts&nbsp;– as well as her past&nbsp;– causes conflicts. After getting the cold shoulder the first day at the gym, Haley realizes what she is up against.

At the invitational, Haleys talent shines and her return from gymnastics retirement seems for the better. But all is not what it seems in the scoring system. She starts to remember one of the many reasons she retired&nbsp;– the flaws in judging. The panels do not look at the difficulty of the move nor do they look at the technique; they merely take deductions for unimportant minor errors. As Haley Graham says, "It doesnt matter how well you do. Its how well you follow their rules."
 stressed by her dominating mother, who has arrived to watch the meet. Her conduct at the World Championship ("Worlds") has not been forgotten by the other athletes, and they treat her with open hostility. Haley finally breaks down in the middle of her balance beam routine and, in a repeat of the World Championships, leaves the arena before completing the competition. Before she leaves, she reveals to Vickerman the reason she walked out of Worlds: she had just discovered that her mother was having an affair with her then-coach, and her parents were divorcing as a result.

Although she did not complete the invitational, Haley continues to train and, with three of her teammates Mina (Maddy Curley), Wei Wei (Nikki SooHoo) and Joanne (Vanessa Lengies), qualifies for the National Championships. The biased judging leaves her far back in the all-around standings, but this does not keep her out of the event finals. In the first event final, vault (gymnastics)|vault, Mina executes an extremely difficult maneuver perfectly but receives a low score (9.500 out of 10). When Vickerman questions the judges, he learns that Mina was penalised on the technicality of showing a bra strap. Haley is next up, however, instead of vaulting, she shows her bra strap to the judges and forfeits her turn in disgust (otherwise known as a "scratch"). The other gymnasts follow suit, earning a string of zeroes and forcing the judges to award Mina the vault gold medal anyway.

Haleys bold action sparks a movement. The gymnasts talk amongst themselves and realize that if they could choose the winner, the judging would be fair. They convince all the others in the competition to do the same, choosing one person from each event who they deem the best to be the "winner". The winner completes her routine; the others jump on and off the apparatus and scratch. It seems the movement will be ruined when Tricia Skilken, a long-time judges favorite, arrives and threatens the choice of winners by competing herself. Trisha finally comes to her senses, though, and realizes that scratching is for the good of the competition to make a point.
 NCAA gymnastics.

==Cast==
*Missy Peregrym as Haley Graham
*Jeff Bridges as Burt Vickerman
*Vanessa Lengies as Joanne Charis
*Maddy Curley as Mina Hoyt
*Nikki SooHoo as Wei Wei Yong
*Kellan Lutz as Frank
*John Patrick Amedori as Poot
*Tarah Paige as Tricia Skilken
*Jon Gries as Brice Graham
*Gia Carides as Alice Graham
*John Kapelos as Chris DeFrank
*Julie Warner as Phyllis Charis
*Andrea Bendewald as Madisons Mom

Doubles
* Isabelle Severino&nbsp;– Missy Peregryms gymnastics double (main)
* Jessica Miyagi&nbsp;– Missy Peregryms gymnastics double (beam routine&nbsp;– IG Classic)
* Annie Gagnon&nbsp;– Vanessa Lengiess gymnastics double
* Kate Stopper&nbsp;– Maddy Curleys gymnastics double
* Tacia Van Vleet&nbsp;– Nikki SooHoos gymnastics double

Cameos
* Tim Daggett&nbsp;– Himself
* Elfi Schlegel&nbsp;– Herself
* Bart Conner&nbsp;– Himself
* Carly Patterson&nbsp;– Herself
* Nastia Liukin&nbsp;– Herself
* Valeri Liukin&nbsp;– Himself (Nastia Liukins spotter in her uneven bars routine)
* Mohini Bhardwaj
* Allana Slater Yang Yun

== Soundtrack==
#We Run This&nbsp;– Missy Elliott (Stick It Edit)
#Abra Cadabra&nbsp;– Talib Kweli
#Beware of the Boys&nbsp;– Panjabi MC (Mundian To Bach Ke)
#Fire Fire&nbsp;– Fannypack/Mr. Vegas
#Dance Commander&nbsp;– Electric Six
#Game, The&nbsp;– Jurassic 5
#If I Only Knew&nbsp;– Lisa Lavie
#Breakdown&nbsp;– The Toques Nu Nu (Yeah Yeah)&nbsp;– Fannypack (Double J & Hayze Extended mix)
#Crowded&nbsp;– Jeannie Ortega/Papoose
#Anthem Part Two&nbsp;– Blink-182
#Halen&nbsp;– Mike Simpson K7
#Outta Damone
#Love Song&nbsp;– J.P. Amedori (Bonus Track)
#Brain Stew&nbsp;– Green Day
#Holiday&nbsp;– Green Day

The movie also features brief pieces of other songs, which were not included in the soundtrack, including My Morning Jackets "One Big Holiday" and Fall Out Boys "Our Lawyer Made Us Change The Name Of This Song So We Wouldnt Get Sued" and "I Slept With Someone in Fall Out Boy and All I Got Was this Stupid Song Written About Me".

== Reception ==
Stick It was released on April 28, 2006 and grossed $10,803,610 in the opening weekend. The movie grossed $26,910,736 total in the domestic market and  $5,066.112 overseas for a total of $31,976,848 after 13 weeks at the box office. 

The movie received a score of 52 on Metacritic from film critics. Additionally Stick It garnered generally favorable reviews and a score of 7.2 from users.  

Critic Nathan Lee of the New York Times gave the film a positive review stating, "A spry teenage comedy that gets everything right, "Stick It" takes the usual batch of underdogs, dirt bags, mean girls and bimbos and sends them somersaulting through happy clichés and unexpected invention."  The film had the highest per screen average on its opening weekend with 2,038 movie theaters, making an average of $5,301 per screen.  Stick It earned a 31% on the Rotten Tomatoes Tomatometer scale.

== See also ==
 
*Make It or Break It, a television series that was inspired by this film 
*Code of Points (artistic gymnastics)

==References==
 

==External links==
*  
*  
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 