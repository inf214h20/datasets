Io sto con gli ippopotami
{{Infobox film
| name           = Io sto con gli ippopotami
| image          = Io sto con gli ippopotami.jpg
| image_size     = 
| caption        = 
| director       = Italo Zingarelli
| producer       = Vincent G. Cox (line producer) Roberto Palaggi (producer)
| writer         = Barbara Alberti (writer) and Amedeo Pagani (writer) and Vincenzo Mannino (writer) and Italo Zingarelli (writer)
| narrator       = 
| starring       = Terence Hill Bud Spencer Joe Bugner
| music          = Walter Rizzati
| cinematography = Aiace Parolin
| editing        = Claudio M. Cutry
| studio         = 
| distributor    =  1979
| runtime        = 108 minutes (Italy)
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Io sto con gli ippopotami (English: "Im with the Hippos") is a 1979 Italian adventure-comedy film directed by Italo Zingarelli and starring Terence Hill and Bud Spencer.

The film is also known as Im for the Hippopotamus.

== Plot summary ==
Two men engage in a semi-friendly rivalry at the beginning of the film.  Hills character occasionally ruins the others hunting safaris.  It turns out that Spencers character is none too honest, either, since he gives his tourists rifles loaded with blanks so they cant hurt each other or the animals. The two have to team up to stop a villain, with plenty of comedy, eating, and violence.

== Cast ==
*Terence Hill as Slim
*Bud Spencer as Tom
*Joe Bugner as Ormond
*May Dlamini as Mama Leone
*Dawn Jürgens as Stella (Slims lady-love)
*Malcolm Kirk as Ormonds bald henchman
*Ben Masinga as Jason
*Les Marcowitz
*Johan Naude
*Nick Van Rensburg
*Hugh Rouse as Police Captain, arresting Slim and Tom
*Mike Schutte as Ormonds henchman (German version: Lumpi)
*Kosie Smith as Ormonds henchman with moustache (German version: Kopf, wie n Hauklotz)
*Joseph Szucs
*Sandy Nkomo as Senghor (Stellas father)

== Soundtrack ==
*Bud Spencer - "Grau Grau Grau"

== External links ==
* 
*  (Dubbed in German)

 

 
 
 
 
 
 
 
 
 
 


 
 