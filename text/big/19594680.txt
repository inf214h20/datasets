Onkel Joakims hemmelighed
{{Infobox film
| name           = Onkel Joakims hemmelighed
| image          = 
| image size     = 
| caption        = 
| director       = Carl Ottosen
| producer       = Dirch Passer Henrik Sandberg
| writer         = Eva Eklund Poul Mogensen Carl Ottosen
| narrator       = 
| starring       = Gunnar Lauring
| music          = Sven Gyldmark
| cinematography = Henning Bendtsen
| editing        = Edith Nisted Nielsen
| distributor    = 
| released       = 24 November 1967
| runtime        = 103 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Onkel Joakims hemmelighed (also known as Nyhavns glade gutter) is a 1967 Danish family film directed by Carl Ottosen and starring Gunnar Lauring. 

==Cast==
* Gunnar Lauring - Viktor Erdner
* Sigrid Horne-Rasmussen - Emilie Erdner
* Vivi Bach - Eva Erdner
* Kai Holm - Joakim Nielsen
* Povl Wøldike - Count von Flagen
* Per Pallesen - Count Horace von Flagen
* Preben Mahrt - Lawyer Frost
* Willy Rathnov - Sailor Rasmus
* Paul Hagen - Viggo
* Ole Wisborg - Svend
* Karl Stegger - Morten Hansen
* Poul Bundgaard - The chef Søren
* Dirch Passer - The Masterthief
* Bodil Udsen - Waitress Mona Lisa
* Kirsten Peüliche - Waitress Vicki
* Preben Kaas - Esben Andersen Arthur Jensen - Peter Jensen
* Ove Sprogøe - CEO Schwartz
* André Sallyman - Max
* Bent Vejlby - Max
* William Kisum - Esben
* Susanne Bruun-Koppel - Misse
* Marianne Kjærulff-Schmidt - Irene
* Ulla Johansson - Bitten
* Lisbeth Lindeborg - Rosa
* Peer Guldbrandsen - The Florist
* Marchen Passer - Maid
* Mogens Brandt - Psychiatrist Count von Flagen
* Jessie Rindom - Countess von Flagen

==External links==
* 

 
 
 
 
 