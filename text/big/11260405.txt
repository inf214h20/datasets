Electric Earthquake
{{Infobox Hollywood cartoon|
| cartoon_name = Electric Earthquake Superman
| image = Electricearthquake1.JPG
| caption = Title card from Electric Earthquake
| director = Dave Fleischer
| story_artist = Seymour Kneitel   Isadore Sparber
| animator = Steve Muffati
| voice_actor = Bud Collyer   Joan Alexander   Julian Noa   Jackson Beck
| musician = Sammy Timberg
| producer = Max Fleischer
| studio = Fleischer Studios
| distributor = Paramount Pictures
| release_date = May 15, 1942 (USA)
| color_process = Technicolor
| runtime = 8 min. (one reel)
| preceded_by = The Magnetic Telescope (1942)
| followed_by = Volcano (animated short)|Volcano (1942)
| movie_language = English
}}

Electric Earthquake is the seventh of the seventeen  .

==Plot==

The story begins with a view of the city, lowering to a view of the ground underneath.  Deep under the docks, several large wires are connected to the bedrock.  Following the wires away from the coast along the ocean floor, it is shown that they all converge in a strange underwater capsule.  An elevator-like object emerges from the top and rises to an abandoned fishing house infested with rats.  A man exits the elevator and heads toward the city in a motorboat.

  Native American man warns Lois Lane, Clark Kent, and Perry White that they must run a report that Manhattan belongs to his people and should be given back to them.  The Planet crew judges him to be crazy, and his threats to be empty... at least, everyone but Lois, who follows him to his motorboat.  Hiding in the back, Lois is taken to the deserted fishing house on the water and sees his elevator.  The man catches her watching him in the elevators reflection, and calmly invites her to follow him, promising an amazing story.  Lois follows.

The elevator lowers into the underwater capsule, and the man offers her a seat, then pushes a button which pins her arms and legs to the chair.  Stepping up to the controls, he starts up his earthquake machine, sending a powerful surge of electricity through one of the wires and into the bedrock under the city.  The large explosion causes the entire city to shake, and runs a large crack through the Daily Planet building.  Clark takes advantage of the commotion to change into his Superman costume.

In one leap, Superman dives into the ocean and notices the several wires embedded in the rock.  He pulls one of them out only to have it explode in his face, flinging him the ground and piling him with bedrock.  He pushes the rock away and pulls at a few more, only to have the wires writhe with electric current and wind around him.  At one point, Superman comes up for air only to have one of the wires wind around his neck and pull him down.

Finally, Superman follows the wires to the underwater capsule and pulls them out from its base, causing explosions which destroy the machine.  As water fills the capsule, the villain takes the elevator to the surface, leaving Lois trapped as the water in the capsule slowly rises.  Superman spots the elevator and catches the villain at the top, but is told that Lois is trapped, and darts back down to save her.  The villain, meanwhile, loads the elevator with dynamite and sends it down after him.  Superman, however, saves Lois in time, and captures the villain as he is making a getaway in his motorboat.

Later, as the Daily Planet reports Supermans latest heroic deed while the villain is implied to be arrested and sent to jail for his rampage, Clark and Lois are seen watching the now-rebuilt Manhattan from a cruise ship. Clark remarks, "You know, Lois, the old island looks just as good as ever." Lois replies, "Thats right, Clark. Thanks to Superman."

==Cast==
* Bud Collyer as Superman/Clark Kent
* Joan Alexander as Lois Lane
* Julian Noa as Perry White
* Jackson Beck as the Narrator, Indian Scientist

==References==
 

==External links==
*  
*   at the Internet Archive
*   at the Internet Movie Database
*   on YouTube

 

 
 
 
 
 