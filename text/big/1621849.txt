The Curse of the Cat People
 
{{Infobox film
|  name           = The Curse of the Cat People
|  image          = Curseofthecatpeople.jpg
|  image size     = 190px
|  caption        = 
|  producer       = Val Lewton
|  director       = Robert Wise Gunther von Fritsch
|  writer         = DeWitt Bodeen Val Lewton (uncredited)
|  starring       = Simone Simon Kent Smith Jane Randolph Ann Carter Eve March
|  music          = Roy Webb
|  cinematography = Nicholas Musuraca
|  editing        = J.R. Whittredge
|  distributor    = RKO
|  released       =  
|  runtime        = 70 minutes
|  country        = United States
|  language       = English
|  budget         = $212,000
|  gross          = 
}}
The Curse of the Cat People is a 1944 film directed by Gunther von Fritsch and Robert Wise, and produced by Val Lewton. This film, which was then-film editor Robert Wises first directing credit, is the sequel to Cat People (1942 film)|Cat People (1942) and has many of the same characters. However, the movie has a completely different story, and no visible cat people, only the ghost of a character established as a cat-person in the previous film. The screenplay was again written by DeWitt Bodeen.

==Plot==
                   Elizabeth Russell), whom she suspects to be a "spy" only pretending to be her relative. Oliver, angry at Amy for repeatedly speaking of her new imaginary friend, punishes her. When Irena announces to Amy that she must leave her, Amy runs out of the house. A snow storm comes up, and Amy seeks shelter in the Farrens house. Barbara, mad and jealous at her mothers preference for Amy over her, intends to strangle the girl. At this moment, Amy sees Irenas features in Barbara and embraces her. Barbara, perplexed by this gesture of affection, spares her life. Oliver arrives at the house and takes Amy home, promising to accept her fantasies.

==Cast==
 
* Simone Simon as Irena Reed, Olivers dead wife
* Kent Smith as Oliver Reed
* Jane Randolph as Alice Reed
* Ann Carter as Amy Reed
* Eve March as Miss Callahan, Amys teacher Julia Dean as Mrs. Julia Farren Elizabeth Russell as Barbara Farren
* Erford Gage as Police Captain Sir Lancelot as Edward, Reeds butler/cook

==Production== The Magnificent Ambersons (1942), as had already been done with the predecessor Cat People. 
 Cat People, The Headless Horseman" (Washington Irvings "The Legend of Sleepy Hollow") which is cited in The Curse of the Cat People. Stafford, Jeff   

Studio executives were disappointed when Lewton screened his final cut for them, and insisted on some additional scenes, such as the one of the boys chasing a black cat, being filmed and inserted into the picture.  At the same time, some details which were crucial to the plot were lost in the re-editing necessary to accommodate the new scenes. 
  by Goya, 1787–88]]

===Production notes===
* Amys teacher mentions a book, The Inner World of Childhood, which is an actual book written by American psychologist Frances Wickes and published in 1927.  Psychology pioneer Carl Jung admired the book, and in 1931 wrote an introduction to it.    Cat People, is an adaptation of the French lullaby Do, do, lenfant do.  The carol Irena sings in counterpoint with Shepherds Shake Off Your Drowsy Sleep is the traditional French Christmas carol Il Est Né, Le Divin Enfant. 
* The painting in the Reed house which is described as Irenas favorite piece of art is the portrait of Manuel Osorio Manrique de Zuñiga by Goya. 

==Reception== The New York Times Bosley Crowther called it "a rare departure from the ordinary run of horror films   emerges as an oddly touching study of the working of a sensitive childs mind". 

The films reputation has grown since its initial release.  Film historian William K. Everson found the same sense of beauty at work in The Curse of the Cat People and Jean Cocteaus La Belle et la Bête.  Director Joe Dante said that the films "disturbingly Disneyesque fairy tale qualities have perplexed horror fans for decades", and the film has been utilized in college psychology courses.  In 2010, The Moving Arts Film Journal ranked it the 35th greatest film of all time. 

==DVD releases==
The Curse of the Cat People is available as part of the Cat People double feature DVD which itself is part of the Val Lewton Horror Collection DVD box from Warner Home Video.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 