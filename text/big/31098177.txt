The State I Am In (film)
 
 
{{Infobox film
| name           = The State I Am In
| image          = 
| image size     = 
| alt            = 
| caption        =  Christian Petzold
| producer       = 
| writer         = Harun Farocki Christian Petzold
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Julia Hummer Barbara Auer Richy Müller Bilge Bingül Katharina Schüttler Bernd Tauber
| music          = Stefan Will
| cinematography = Hans Fromm
| editing        = Bettina Böhler
| studio         = Schramm Film Hessischer Rundfunk Arte
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
 German drama Christian Petzold.

==Further reading==

Gerhardt, Christina. “RAF as German and Family History: Von Trotta’s Marianne and Juliane and Petzold’s The State I Am in.” The Place of Politics in German Film. Ed. Martin Blumenthal-Barby. Bielefeld: Aisthesis, 2014. 166-184.

Gerhardt, Christina. "Working through the Past: Petzolds The State I Am In." Potsdamer Almanach. Göttingen: Wallstein Verlag, 2011. 31-42.  

==External links==
* 
* 

 
 
 
 
 
 
 


 
 