The Jet Benny Show
{{multiple issues|
 
 
}}

{{Infobox Film
| name = The Jet Benny Show
| image = The Jet Benny Show.jpg
| image_size = 
| caption = 
| director = Roger D. Evans
| producer = 
| writer = Mark Feltch
| narrator = 
| starring = Steve Norman (as Jet Benny) James Black Kevin Dees Steve Garfinkel James Luedemann Polly MacIntyre Richard Sabel Mark Walz
| music = Marianne Pendino
| cinematography = 
| editing = 
| distributor = United Entertainment
| released = 1986
| runtime = 77 minutes
| country = United States
| language = English
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 title character is a spoof of Jack Benny. The plot is a broad parody of the Star Wars films, and is deliberately low-budget in appearance.

==Synopsis==

===Opening===
At the start of the film, we see a  ).

===The “sketch”===
Jet Benny (Norman), a self-proclaimed “intergalactic  .

One day, Jet sees a woman (MacIntyre) being whipped and beaten by a thug and his henchmen, and he uses his raygun to scare them off. The woman turns out to be Princess Miranda; her family and people have been tyrannized by the forces of Lord Zane for the last few years, with the aid of android Rochester whose memory had been damaged by the crash.
 walled city, which is being besieged by Zane’s forces. The heroes win the battle, and the members of the royal family are reunited.

Jet is getting ready to leave the planet when Miranda catches up to him and kisses him; as a result, Jet, who had been eager to get away from the planet, is now reluctant to leave.

===Closing===
On the television show, the host thanks the guest and cast members for their work, and starts talking about next week’s show.

==Low-budget look== rotoscoped animation.

==Notes==
* The names of the prince and princess were taken from Carmen Miranda, a fact that was made clear to the audience near the end of the movie.
 Eddie Anderson’s character from The Jack Benny Program; besides the name, however, the two characters have nothing in common.

* Jet’s vessel, the Maxwell, was named after the Maxwell automobile that Jack Benny claimed to own during sketches on his show.

==Availability==
This movie has been released on VHS videocassette, but not on DVD.

==External links==
*  
*  
*  : Roger D. Evans talks about the making of The Jet Benny Show (October, 2001)

 
 
 