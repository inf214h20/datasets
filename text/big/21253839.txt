Ski Patrol (1990 film)
{{Infobox_Film name = Ski Patrol image = Poster of the movie Ski Patrol.jpg caption = 1990 movie poster. director = Richard Correll producer = Paul Maslansky writer = Steven Long Mitchell Craig W. Van Sickle Wink Roberts starring = Roger Rose Yvette Nipar T.K. Carter Leslie Jordan Paul Feig George Lopez Corby Timbrook Ray Walston Martin Mull Dante Hen music =Bruce Bruce Miller cinematography =John M. Stephens editing =Scott K. Wallace distributor = Epic Productions released = January 12, 1990 runtime = 91 minutes country = United States language = English
|budget =  gross =  followed_by =
|}}

Ski Patrol is a 1990 comedy film directed by Richard Correll, and starring Roger Rose, Yvette Nipar, T.K. Carter, George Lopez, Ray Walston, and Martin Mull.

==Cast==
*Roger Rose - Jerry Cramer
*Yvette Nipar - Ellen
*T.K. Carter - Iceman
*Leslie Jordan - Murray
*Paul Feig - Stanley
*Sean Gregory Sullivan - Suicide
* Tess  - Tiana
*George Lopez - Eddie Martinez
*Corby Timbrook - Lance
*Ray Walston - Pops
*Martin Mull - Sam Marris

== External links ==
*  

 
 
 
 
 
 

 