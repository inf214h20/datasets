The Cat and the Kit
{{Infobox Hollywood cartoon|
| cartoon_name = The Cat and the Kit
| series = Felix the Cat
| image = 
| caption = 
| director = Pat Sullivan
| story_artist = 
| animator = 
| voice_actor = 
| musician = 
| producer = 
| studio = Pat Sullivan Studios
| distributor = General Electric
| release_date = 1925
| color_process = Black and white
| runtime = 10 min. English
| preceded_by = 
| followed_by = 
}}

The Cat and the Kit is a silent short film by the Pat Sullivan Studio, featuring Felix the Cat. This particular cartoon was used to promote products, namely Mazda light bulbs from General Electric. {{cite web
|url=http://www.cartoonbrew.com/classic/felix-the-cat-in-the-cat-and-the-kit-1925.html
|title=Felix The Cat in "The Cat and The Kit" (1925)
|date=March 6, 2012
|accessdate=2012-06-28
|author=Jerry Beck
|publisher=Cartoon Brew
}} 

==Plot==
Felix is getting married. He is dressing in his house when a telephone rang. Speaking to him on the phone is his bride who says shell be waiting for him at the church. Felix boards his car and heads off.

The headlamps on Felixs car are faulty, and one of them goes out. On the way, a motorcycle is
coming from the opposite direction, and its rider thought his car is another motorcycle because of only a single light is glowing. Fortunately, the two motorists passed each other without collision.

While continuing on his journey, Felix is flagged down by a cop who confronts him about his headlight problem. The cat then comes to a store selling car accessories to replace his lights. He wanted a pair of Mazda light bulbs but the store clerk tells him no stock of the product is available, and therefore makes suggestion of other lights, claiming they would work well also.

Felix puts on the regular bulbs on his vehicle, and continues his travel. With one of the lights shining horizontally and other one diagonally upward, Felix is again flagged by a cop who tells him to have them glow on the road. He then tries to adjust their focus but to no avail, and therefore goes on driving. This resulted in disturbed residents on the way firing guns and physically assaulting him. Moments later, his newly replaced lights burn out. As a consequence, Felixs car runs into a tree, then into a boulder, before falling off a cliff.

Luckily, his vehicle isnt seriously damaged, and Felix finally reaches the church. Much to his surprise, he finds a note on a wall saying he is too late and someone else took and decided to own his bride. Felix, however, refuses to concede, and is determined to get her back. He then enters another auto parts store, and is amazed that the place has Mazda light bulbs which he buys at last.

Felix fits the Mazda bulbs onto his car, and adjusted the lights focus properly. The cat rides again in his vehicle and goes on full speed. He then catches up at the vehicle carrying his bride. As he tailgates the other car, Felix stands up, walks towards it, and pick ups his girl without the driver noticing. The cat and the bride turn to the other direction and drive off. An epilogue text mentions Felixs advice about using quality lights.

==See also==
*Mazda (light bulb)

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 