The Key (1934 film)
{{Infobox film
| name           = The Key
| image          =
| caption        =
| director       = Michael Curtiz
| producer       = Robert R. Presnell, Sr.
| writer         =   Jocelyn Lee Hardy
| narrator       =
| starring       = William Powell Edna Best
| music          = Allie Wrubel (composer) Mort Dixon (composer)
| cinematography = Ernest Haller Thomas Richards
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

The Key  is a 1934 Pre-Code American film directed by Michael Curtiz. It was re-issued as High Peril (pre-release title Sue of Fury) in 1960.   The story concerns a love triangle and is set during the Irish War of Independence. It characterises the Irish Republican Army as "little more than a gangster organization."  
==Plot==
 
Captain Bill Tennant (William Powell) is a British officer stationed in Dublin in 1920. Tennant gets involved with Norah, the wife of his best friend, British intelligence officer Captain Andy Kerr. Kerr is captured by the IRA, to be released only on condition that an Irish prisoner, Peadar Conlan, is not hanged. Tennant forges an order for Conlans release, saving the life of his lovers husband, but at the cost of a prison sentence. 

==Cast==
* William Powell as Capt. Bill Tennant  
* Edna Best as Norah Kerr  
* Colin Clive as Capt. Andrew Andy Kerr  
* Hobart Cavanaugh as Homer, Tennants Aide  
* Halliwell Hobbes as General C.O. Furlong  
* Donald Crisp as Peadar Conlan  
* J.M. Kerrigan as ODuffy  
* Henry ONeill as Dan   Phil Regan as Young Irishman Killed by Andrew  
* Arthur Treacher as Lt. Merriman, Furlongs Aide  
* Maxine Doyle as Pauline OConnor  
* Arthur Aylesworth as Kirby  
* Gertrude Short as Evie a Barmaid   Anne Shirley as Flower Girl (as Dawn ODay)

==Background== Captain J L Hoppy Hardy, a decorated veteran of World War I who had made repeated escapes from German prisoner of war camps. His nickname derived from his wooden leg, having lost a limb in combat.

After the Great War Hardy served as a military intelligence officer attached to the Auxiliary Division of the Royal Irish Constabulary during the Irish War of Independence.  He specialised in interrogating IRA prisoners and became a hated figure for them, frequently accused of brutality.   

He brings his experiences to the film and the characters of Tennant and Kerr may have been inspired by his contemporary officers Douglas Duff who referred to himself as a soldier for hire in his autobiography and W. L. King who served with Hardy as an intelligence officer and romanced and married a Dublin girl during his service in Ireland.
     

==References==
 	

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 