The Guv'nor (film)
{{Infobox film
| name           = The Guvnor
| image          = 
| image_size     = 
| caption        = 
| director       = Milton Rosmer
| producer       = Michael Balcon
| writer         = Guy Bolton Maude T. Howell Paul Lafitte
| starring       = George Arliss
| music          = 
| cinematography = 
| editing        =  Gaumont British Picture Corp.
| released       = November 22, 1935
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

The Guvnor (released in the U.S. as Mr. Hobo) is a 1935  British comedy film starring George Arliss as a tramp who rides a series of misunderstandings and becomes the president of a bank.

==Plot== Frank Cellier) is in a great deal of trouble - the Paris bank of which he is president is bankrupt, though nobody else knows yet. He tells his secret to his crony Dubois (George Hayes), since he needs his help. Dubois is to purchase an iron mine that is supposedly played out. However, Barsacs mining engineer has found rich, untapped deposits of ore. The mine is 51% owned by the widow Mrs. Granville (Henrietta Watson) and her daughter Madeleine (Viola Keats), who are deeply in debt to his bank. Barsac uses his stepson Paul (Patric Knowles) as an intermediary, since Paul is a friend of the family, particularly the beautiful Madeleine.

Meanwhile, a vagabond known as the "Guvnor" (George Arliss) decides to head south for the winter with his friend Flit (Gene Gerrard). At the Granville estate, he offers to mend some china in exchange for food and is treated very cordially by Madeleine.
 great banking dynasty who, unwilling to have his illustrious family name besmirched by an arrest, gives the Guvnor a check for 2000 francs and has the two men released.

The Guvnor is happy with his lifestyle, so he offers the money to Flit. They clean themselves up before trying to cash the check at Barzacs bank. Barzac mistakes the Guvnor for one of the Rothschilds and tries to persuade him to join the board of directors to prop up the bank. During the conversation, the Guvnor catches Barzac in a lie about Madeleine and becomes interested. He is made president of the bank.

When the Guvnor learns details about Barsacs scheme from Madame Barzac (Mary Clare), who is anxious to prevent her husband from investing in a "worthless" mine, he returns to the Granville estate. There, dressed as the tramp, he advises Madeleine to get Paul to ask for impartial advice about Barzacs strong recommendation to sell - from Monsieur Rothschild. Instead, she goes to see Rothschild herself and discovers his real identity. She believes that he has deceived her so he can purchase the mine himself and stalks out before he can explain.

The next morning, the Guvnor attends a meeting of the shareholders called to vote on whether to sell for the pittance Dubois is offering. The Guvnor denounces Barzac and Dubois, but Madeleine votes to sell.

The wily Guvnor then makes it look as if he has committed suicide. People fear he did so because there is something wrong with the bank and Dubois company; panic selling soon drives down the price of shares in both. Meanwhile, Paul buys them on the Guvnors behalf. Having saved the Granvilles and ruined Barzac and Dubois, the Guvnor gives the shares to Madeleine and Paul as a wedding present and resumes his carefree journey to warmer climes.

==Cast==
*George Arliss as François Rothschild, the "Guvnor"
*Gene Gerrard as Flit
*Viola Keats as Madelaine Granville
*Patric Knowles as Paul Frank Cellier as Barsac
*George Hayes as Dubois
*Mary Clare as Madame Barsac
*Henrietta Watson as Mrs. Granville

==External links==
*  
*  
*  

 
 
 
 
 
 

 