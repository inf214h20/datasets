Les Aventures des Schtroumpfs
{{Infobox Film
| name = Les Aventures des Schtroumpfs
| image =
| caption =
| director = Eddy Ryssack, Maurice Rosy
| producer = Charles Dupuis
| writer = Yvan Delporte, Peyo, André Franquin Jacques Courtois, Paul Roland
| music = Roland Renerte
| cinematography = Raoul Cauvin, Norbert Declercq
| animation = Eddy Ryssack, Vivian Miessen, Charles Degatte
| distributor =
| released = 1965
| runtime = 90 mins.
| country = Belgium French
}}
 animated feature film based on the Belgian comic book series "The Smurfs". It was the first animated feature film featuring the Smurf characters. The film was released in 1965 in Belgium. 

The film featured the voices of popular actors then working for radio.

The film was overshadowed eleven years later by the Belvision film The Smurfs and the Magic Flute.

==Plot== Walloon TV:

=== Smurfnapped ===
A Smurf gets himself captured by Gargamel. Now, the Smurfs must save him before he gets killed.

=== The Smurfs and the Magic Egg ===
The Smurfs discover a magic egg. But they dont know it has been created by Gargamel.

=== The Black Smurfs ===
A contagious disease terrorizes the village.

=== The Smurfs and the Dragon ===
The Smurfs befriend a domesticated dragon.

=== The Flying Smurf ===
One of the Smurfs attempts to fly like a bird.

==See also==
* List of animated feature-length films
* The Smurfs

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 

 