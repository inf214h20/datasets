Hawaii, Oslo
 

{{Infobox film
| name           = Hawaii, Oslo
| image          = Hawaii, Oslo.jpg
| image_size     = 
| caption        = 
| alt            =
| director       = Erik Poppe
| producer       = Finn Gjerdrum
| screenplay     = Harald Rosenløw Eeg
| story          =  Erik Poppe  Harald Rosenløw Eeg Petronella Barker  
| music          =  John Erik Kaada  Bugge Wesseltoft
| cinematography = 
| editing        = Arthur Coburn
| distributor    = Paradox Spillefilm
| released       = 24 September 2004 (Norway)
| runtime        = 125 minutes
| country        = Norway  Norwegian
| NOK 20,000,000 (estimated)   
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Norwegian drama Petronella Barker. 

The films music was composed by John Erik Kaada and Bugge Wesseltoft.  Produced by Finn Gjerdrum and distributed by Pardox Spillefilm, the film is in the Norwegian language and was edited by Arthur Coburn.

==Production== NOK 20,000,000. 

==Plot==
Vidar (Seim), who works at a psychiatric hospital, tries to keep himself awake as much as he can, because he has several times dreamt of horrible events that turned out to be true premonitions. At one point, he dreams that Leon (Røise), one of the patients, who is supposed to meet his ex-girlfriend, never meets her, but is hit by an ambulance instead.

== Release and reception ==
The film was released on 24 September 2004 and was generally well received by the Norwegian press. Dagbladet gave the film five out of six points, and called it an "intense cinematic experience".  Aftenposten awarded six out of six points, claiming the movie expanded the boundaries of Norwegian film.  Verdens Gang also gave the film six out of six points. 

===Awards===
The film was awarded two Amanda Awards in 2005 "Best Film (Norwegian)" and "Best Screenplay". It was also nominated in the categories "Best Director" and "Best Actor" (Stig Henrik Hoff). 

==Cast==
 
 
* Trond Espen Seim as Vidar
* Jan Gunnar Røise as Leon
* Evy Kasseth Røsten as Åsa
* Stig Henrik Hoff as Frode
* Silje Torp Færavaag as Milla
* Robert Skjærstad as Viggo Petronella Barker as Bobbie
* Bejamin Røsler as Mikkel
* Ferdinand Falsen-Hiis as Magne
* Judith Darko as Tina
* Aksel Hennie as Trygve
* Morten Faldaas as John
 

==See also==
 
 
* 2004 in film
* Cinema of Norway
* List of Amanda Award winners
* List of drama films
* Norwegian films of the 2000s
 
 

== References ==
 

==External links==
* 

 
 
 
 
 
 
 