Baandhon
{{Infobox film
| name           = Baandhon
| image          = Baandhon PVR release poster.jpg
| alt            = The PVR release poster of Baandhon
| caption        = The PVR release poster of Baandhon
| director       = Jahnu Barua
| producer       = Jahnu Barua
| writer         = Jahnu Barua
| starring       = 
| music          = Durbajyoti Phookon
| cinematography = Suman Duwarah
| editing        = Cheragh Todiwala
| studio         = 
| distributor    = 
| released       =   |df=y}}
| runtime        = 110 minutes
| country        = India
| language       = Assamese
}}
 Assamese Assamese language drama PVR theaters across rest of India. 

The film focuses on an elderly couple, Dandeswar and Hkawni, who arrive in Mumbai in search of their missing grandson after 2008 Mumbai attacks|26/11 2008 terror attack.     Best Feature Film in Assamese award in the 60th National Film Awards    and also won the Best Film award in the Indian Films Competition at the Bengaluru International Film Festival for the year 2012. 

==Plot== IIT Bombay. However, things take an unprecedented turn when Pona disconnects a call from Dandeswar in the middle of a conversation. The subsequent tries to contact him fails. It all took place at the same time with the 2008 Mumbai attacks|26/11 2008 terror attack. The couple arrives in Mumbai in search of their missing grandson.

==Cast==
* Bishnu Kharghoria as Dandeswar
* Bina Patangia as Hkawni
* Jatin Bora as Jatin
* Jerifa Wahid as Ranjana
* Abatush Bhuyan
* Angshuman Bhuyan

==Music==
There are no songs in the film except background music.  Background music director is Dhurbajyoti Phookan.

==Awards / Honours== Kerala Film Festival and the Mumbai Film Festival in the year 2012.   , access date: 20-12-2012    access date: 20-12-2012  
 Best Feature Film in Assamese. 

==See also==
*Jollywood

==References==
 

==External links==
*  
*   at YouTube
*  , at The Hindu Images
*  
 

 
 
 
 