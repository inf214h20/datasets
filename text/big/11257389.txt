Ek Phool Char Kaante
{{Infobox film
| name           =Ek Phool Char Kaante
| image          = 
| image_size     = 
| caption        = 
| director       = Bhappi Sonie
| producer       = 
| writer         = 
| narrator       = 
| starring       = Sunil Dutt Waheeda Rehman
| music          = Shankar Jaikishan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1960
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Hindi film Johnny Walker, Gopi and Tun Tun. The films music is by Shankar Jaikishan.

The film included the famous song "Matwali Naar Thumak Thumak Chali Jaaye".

==Plot==
The film is a light-hearted comic love story, wherein Sunil Dutt falls in love with a phool (flower) Waheeda Rehman but has to impress her four uncles, the kaante (thorns) of the title. Each uncle has a different idiosyncrasy and Sunil Dutt pretends to be an expert in that field in order to impress that particular uncle. The four uncles select their protégé for their niece, not knowing that each is selecting the same boy. When the boy whom they have approved disappears, they agree to marry their niece off to her choice, who is Sunil Dutt again. 

==Cast==
* Sunil Dutt
* Waheeda Rehman Johnny Walker Dhumal
* David Abraham
* Gopi
* Tun Tun
* Sulochana Latkar Mumtaz Begum
* Bela Bose

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!!Lyricist
|-
| 1.
| "Banwari Re Jeene Ka Sahara"
| Lata Mangeshkar"
| Hasrat Jaipuri
|-
| 2.
| "Ankhon Mein Rang Kyon Aaya"
| Mukesh, Lata Mangeshkar"
| Hasrat Jaipuri
|-
| 3.
| "Dil Ae Dil Baharon Se Mil"
| Talat Mahmood, Lata Mangeshkar"
| Shailendra
|-
| 4.
| "Matwali Naar Thumak Thumak"
| Mukesh (singer)|Mukesh"
| Shailendra
|-
| 5.
| "Baby Of Bombay"
| Iqbal Singh"
| Shailendra
|-
| 6.
| "O Meri Baby Doll"
| Mohammed Rafi"
| Shailendra
|-
| 7.
| "Sambhal Ke Karna Jo Kuchh Karna"
| Mukesh"
| Shailendra
|-
| 8.
| "Soch Rahi Thi Kahoon Na Kahoon"
| Lata Mangeshkar"
| Shailendra
|-
| 9
| "Tirchi Nazar Se Yun Na Dekh"
| Lata Mangeshkar"
| Hasrat Jaipuri
|}

== External links ==
*  

 
 
 
 
 

 