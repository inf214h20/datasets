Little Malcolm
 
{{Infobox film
| name           = Little Malcolm
| caption        = DVD cover
| image	         = Little Malcolm FilmPoster.jpeg
| director       = Stuart Cooper
| producer       = George Harrison Gavrik Losey
| writer         = David Halliwell (play) Derek Woodward
| starring       = John Hurt
| music          = 
| cinematography = John Alcott
| editing        = Ray Lovejoy Apple Films
| released       =  
| runtime        = 109 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}

Little Malcolm is a 1974 British comedy drama film directed by Stuart Cooper. It was entered into the 24th Berlin International Film Festival where it won the Silver Bear. 

The film is based on the stage play Little Malcolm and His Struggle Against the Eunuchs by David Halliwell. Clayson, p. 370.  The full name of the play is used as the film title on the BFI Flipside 2011 DVD release. 
 Beatle George Splinter by their manager Mal Evans, produced their song "Lonely Man" for inclusion in a pivotal scene.  Badman, p. 129. 

Like many of Apple Corps|Apples film and recording projects, production on Little Malcolm was then jeopardised by lawsuits pertaining to Harrison, John Lennon and Ringo Starrs severing of ties with manager Allen Klein.   Speaking in 2011, Cooper recalled that Harrison "fought for a very long time to extract Little Malcolm from the official receivers"; its entry in the Berlin festival was only possible because the festival was an artistic forum and not finance-related.  After what Cooper described as an "incredible" reception at Berlin for "this very British film",  Little Malcolm went on to win a gold medal at the Atlanta Film Festival in August 1974.  Once the Beatles partnership had been formally dissolved in January 1975, the film received a brief run in Londons West End. 

==Cast==
* Rosalind Ayres as Ann Gedge
* John Hurt as Malcolm Scrawdyke
* John McEnery as Wick Blagdon
* Raymond Platt as Irwin Ingham David Warner as Dennis Charles Nipple
==Soundtrack==
The soundtrack featured the band Harpoon singing "Not With You". 

==Citations==
 

==Sources==
 
* Keith Badman, The Beatles Diary Volume 2: After the Break-Up 1970–2001, Omnibus Press (London, 2001; ISBN 0-7119-8307-0).
* Alan Clayson, George Harrison, Sanctuary (London, 2003; ISBN 1-86074-489-3).
* Peter Doggett, You Never Give Me Your Money: The Beatles After the Breakup, It Books (New York, NY, 2011; ISBN 978-0-06-177418-8).
* Bob Woffinden, The Beatles Apart, Proteus (London, 1981; ISBN 0-906071-89-5).
 

==External links==
* 

 

 
 
 
 
 
 
 
 