Perfect Mismatch
{{Infobox film
| name = Perfect Mismatch
| image = Perfect Mismatch Movie Poster.jpg
| caption = Theatrical poster
| director = Ajmal Zaheer Ahmad
| producer = Xperience Films
| writer =   
| starring = Anubhav Anand Nandana Sen Anupam Kher Boman Irani
| music = 
| cinematography = Robert Mehnert
| distributor = 
| released =   
| runtime = 
| country = India
| language = English
| budget = 
| gross =
}}
Perfect Mismatch (formerly known as Its a Mismatch ) is a Bollywood film directed by Ajmal Zaheer Ahmad. 

==Synopsis==
 
Mr. Bhalla (Anupam Kher), a hyper-vibrant Punjabi lives in US along with his wife, daughter and nephew Aman (Anubhav Anand). Aman is in love with Neha (Nandana Sen), daughter of Mr. Patel (Boman Irani), a conventional Gujarati. Although the two are in love, their families have huge differences due to drastically different lifestyles. In the end, its up to Aman and Neha to not only bring their families together but to live up to their expectations.

==Cast==
* Anubhav Anand as Aman
* Nandana Sen as Neha
* Anupam Kher as Mr. Bhalla
* Boman Irani as Mr. Patel
* Sheel Gupta as  Preeti
* Geeta Sugandh as  Mrs. Shah
* Kanaiya Sugandh as  Mr. Shah
* Osman Soykut as  Mr. Brenner

==Release==
Perfect Mismatch was produced under the banner Xperience Films and had a limited release in India. The film was originally titled Its A Mismatch, but the producers had to change to title as it was not available. The film was released contemporaneously with another Bollywood film, Luck (2009 film)|Luck, which adversely affected its ticket sales.  Perfect Mismatch didnt perform well at the box office in India, but had a successful festival run in the USA. It was an official selection at one of the top 10 International Film Festivals - Cinequest Film Festival.  The original songs for the soundtrack were composed by the Montreal-based group, Josh (band)|Josh.

==References==
 

==External links==
*  

 
 
 