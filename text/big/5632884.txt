Penelope (1966 film)
{{Infobox film
| name           = Penelope
| image          = Penelope (1966 film) poster.jpg
| image_size     =
| caption        =
| director       = Arthur Hiller
| producer       = Arthur Loew, Jr. George Wells
| starring       = Natalie Wood Ian Bannen Dick Shawn Peter Falk Lila Kedrova Lou Jacobi Jonathan Winters Johnny Williams
| cinematography = Harry Stradling
| editing        = Rita Roland
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $4 million 
| gross          = $4,000,000  (rentals)  
}}

Penelope is a 1966 comedy film directed by Arthur Hiller and starring Natalie Wood, Ian Bannen, Peter Falk, Jonathan Winters, and Dick Shawn. A novelisation of the screenplay was written by Howard Melvin Fast writing as E.V. Cunningham.

==Plot==
 
Penelope Elcott (Natalie Wood) is the wife of wealthy banker James Elcott (Ian Bannen).  Penelope decides to disguise herself as an old woman and rob her husbands bank.  While the police, including Lieutenant Horatio Bixbee (Peter Falk), rush to get to the bank, Penelope escapes in a red wig and yellow suit.  She donates some of the stolen money to a Salvation Army worker and donates the suit to a second-hand thrift shop. Con artists Sabada (Lila Kedrova) and Ducky (Lou Jacobi) immediately recognize the suit as an original designer outfit from Paris, and purchase it for a mere $7.

Penelope visits her psychiatrist, Gregory (Dick Shawn), and tells him all about her criminal activities. She says it began in college, when a professor (Jonathan Winters) was trying to rape her and she escaped wearing only her underwear. During the chase, she stole a diamond set watch of the Professors. She next stole on her wedding day. When she caught her maid of honour Mildred Halliday (Norma Crane) kissing James, she swiped Mildreds earrings and necklace. Gregory suggests she is stealing to attract attention from her distant husband.

A young woman, Honeysuckle Rose, is accused of being the thief. Gregory wants to return the stolen money to the bank, but panics when he hears police cars arriving.  Penelope confesses and tries to clear the innocent Honeysuckle, but Horatio the cop and husband James dont believe her. Ducky and Sabada pay a visit, trying to blackmail her, but Penelope foils their blackmail attempt. 

Penelope hosts a dinner party, having stolen from all the invited guests. She tries to return the stolen items, but all claim that they have never seen them before. Penelope, confused and frightened, runs away. She again robs James bank, but unlike the previous time, she is crying. James begs Horatio to find her. Penelope herself goes to Horatio with the stolen money, but the cop knows James would not press charges against his own wife.

The psychiatrist, Gregory, explains the dinner guests denied recognizing the stolen items because they would lose the fraudulently inflated insurance claims they collected.  Gregory breaks down and begs Penelope to run away with him.  She refuses, telling him she is cured. James realizes that he has neglected Penelope and starts seeing her face everywhere he turns. He goes to the psychiatrists office, where James and Penelope happily reunite.

==Cast==
* Natalie Wood as Penelope Elcott
* Ian Bannen as James B. Elcott
* Dick Shawn as Dr. Gregory Mannix
* Peter Falk as Lieutenant Horatio Bixbee
* Lila Kedrova as Princess Sadaba
* Lou Jacobi as Ducky
* Jonathan Winters as Professor Klobb
* Norma Crane as Mildred Halliday
* Arthur Malet as Salvation Army Major Higgins
* Jerome Cowan as Bank Manager
* Arlene Golonka as Honeysuckle Rose
* Amzie Strickland as Miss Serena Bill Gunn as Sergeant Rothschild
* Carl Ballantine as Boom Boom Iggie Wolfington as Store Owner

==References==
 
==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 