Lakshyam (1972 film)
{{Infobox film
| name           = Lakshyam
| image          =
| caption        =
| director       = Jipson
| producer       = Jipson
| writer         = Sherly Jipson (dialogues)
| screenplay     = Jipson Sathyan Madhu Madhu Jayabharathi Prasad
| music          = M. K. Arjunan
| cinematography = N Karthikeyan
| editing        = TR Natarajan
| studio         = Jwala Films
| distributor    = Jwala Films
| released       =  
| country        = India
| language       = Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed and produced by Jipson . The film stars Sathyan (actor)|Sathyan, Madhu (actor)|Madhu, Jayabharathi and Prasad in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
  Sathyan
* Madhu
* Jayabharathi
* Prasad
* Sankaradi
* T. S. Muthaiah
* Paul Vengola
* Bahadoor Khadeeja
* Kousalya Meena
* N. Govindankutty
* PO Thomas
* Paappi
* Philomina Ragini
* Rajakumari Venu
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sherly and Jipson.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Daaham Ee Moham || LR Eeswari || Sherly ||
|-
| 2 || Innu Njan Kaanunna || K. J. Yesudas || Sherly ||
|-
| 3 || Maappuchodikkunnu Njan || CO Anto || Sherly ||
|-
| 4 || Njettattu Mannil Veezhuvaan || K. J. Yesudas || Sherly ||
|-
| 5 || Paapathin Kurishenthi || PB Sreenivas || Jipson ||
|}

==References==
 

==External links==
*  

 
 
 

 