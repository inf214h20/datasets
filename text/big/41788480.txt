Dhruvasangamam
{{Infobox film 
| name           = Dhruvasangamam
| image          =
| caption        =
| director       = J. Sasikumar
| producer       =
| writer         = Jessy Rexena S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan Shubha Sukumaran
| music          = Raveendran
| cinematography = C Ramachandra Menon
| editing        = G Murali
| studio         = Velamkanni International
| distributor    = Velamkanni International
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, Shubha and Sukumaran in lead roles. The film had musical score by Raveendran.   

==Cast==
*Mohanlal
*Manavalan Joseph Shubha
*Sukumaran
*Alummoodan
*K. P. Ummer
*Kuthiravattam Pappu Meena
*Reena Reena

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Adharam Pakarum || Lathika || Sathyan Anthikkad || 
|-
| 2 || Maanasa Devi || K. J. Yesudas || Sathyan Anthikkad || 
|-
| 3 || Sharathkaala Megham || K. J. Yesudas || Sathyan Anthikkad || 
|-
| 4 || Vanamaala Choodi || K. J. Yesudas || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 