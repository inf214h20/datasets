Paris Manhattan
{{Infobox film
| name = Paris Manhattan
| image = Paris-Manhattan-Poster1.jpg
| caption = French movie poster
| director = Sophie Lellouche
| screenplay = Sophie Lellouche
| based on = 
| starring = Alice Taglioni Patrick Bruel Yannick Soulier Woody Allen
| producer = 
| released =    
| runtime = 77 min
| country = France
| language = French English
| budget         = $5,300,000
| gross          = $2,873,185 
}} French feature film, which premiered on April 2, 2012 at the Festival of French Cinema in Australia.  This is the first feature film by writer-director Sophie Lellouche.

== Plot ==
 Jewish family, who during her early years was introduced to and fell in love with Woody Allens films. Growing up, she strongly desires a relationship, but the only man she ever loved was taken away from her by her own sister. On Alices bedroom wall hangs a huge poster of Woody Allen, with whom she has long night conversations, and he talks back to her through excerpts of dialogue from his films.

Ten years go by. Alice has taken over her fathers pharmacy after he retired, her sister is long married to the man she stole from Alice, and the poster still hangs over the bed. She is thirty, and lonely, and her family is trying its best to introduce her to unmarried men. She is having a hard time choosing from two emerging suitors, Vincent and Victor. Almost by accident, but with help of Victor, Alice eventually  meets Woody Allen on the streets of Paris. This time, the real Woody Allen, not the voice of the Poster, gives her personal advice, which happens to be exactly what Alice considered doing anyway.

== Cast ==
* Alice Taglioni as Alice
* Patrick Bruel as Victor
* Marine Delterme as Helen
* Yannick Soulier as Vincent
* Louis-Do de Lencquesaing as Pierre
* Michel Aumont as Alices Father
* Marie-Christine Adam as Nicole, Alices Mother
* Woody Allen as himself

== Release ==
The film was released in Germany on October 4, 2012. 

== Reception == Play It Again, Sam, stating that "This update-cum-ripoff might be aiming for witty and romantic, but its mostly a hollow, rambling effort leavened with some stargazing". 

== References ==
 

== External links ==
*  
*  

 
 
 

 