Faults (film)
{{Infobox film
| name           = Faults
| image          = Faults (film) POSTER.jpg
| alt            = 
| caption        = 
| director       = Riley Stearns
| producer       = Mary Elizabeth Winstead   Keith Calder   Jessica Wu
| writer         = Riley Stearns Chris Ellis   Jon Gries   Lance Reddick   Beth Grant 
| music          = Heather McIntosh
| cinematography = Michael Ragen
| editing        = Sarah Beth Shapiro
| studio         = Snoot Entertainment
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Chris Ellis and Lance Reddick. The film premiered at the 2014 SXSW on March 9, 2014, and was picked up by Screen Media Films for theatrical release on March 6, 2015. 

== Cast ==
* Leland Orser as Ansel Roth
* Mary Elizabeth Winstead as Claire
* Jon Gries as Terry
* Lance Reddick as Mick
* Beth Grant as Evelyn Chris Ellis

== Production == Cops when he was a child, in which “a girl called the police and was like,I’m locked in this hotel room and they’re not letting me out.   and the cops told her that her parents knew what was best and that she had to stay. And they left. And I was freaked out by that because even as a kid you realize there’s something weird about that – telling you that you have to stay in a place you don’t want to be.”    

Stearns cited Paul Thomas Anderson’s works, namely Punch-Drunk Love, The Coen Brothers’ Fargo (film)|Fargo, Yorgos Lanthimos’ Dogtooth (film)|Dogtooth and Alps (film)|Alps among the many works he was inspired by. 

To prepare themselves for the role, Winstead and Orser read the book “Let Our Children Go” by Ted Patrick, as well as watching YouTube videos from that era of people in cults or people who had just gotten out of cults. They had both also created their own backstories for their characters based on Stearns’ script.  Winstead has repeatedly mentioned that Claire is her most challenging role to date. “I have to play a few different characters over the course of the film   I had to sort of really keep track from scene to scene to remind myself of where she was,” Winstead says. 
 Long Beach San Pedro). 

== Casting ==
In July 2013, it was reported that Mary Elizabeth Winstead had been cast in Faults.  In December 2013, Leland Orser was also cast. 

== Release == AFI Fest from November 6 to 13. 

On August 26, it was reported that  . 

== Reception ==
Faults has received positive response since its release. Rotten Tomatoes has given Faults an approval score of 88%, based on 24 select reviews.  Many reviewers commended the films off-kilter black comedy that underlays the plot; Hollywood Reporter says, "Faults is not what it seems. Though a black-comic atmosphere persists, the debut feature is serious about manipulation and brainwashing, and a quietly commanding performance by Mary Elizabeth Winstead helps establish that seriousness."  In a 4.5/5 review, Bloody Disgusting calls it "a modern cult thriller   manages to be laugh out loud funny in a manner that doesn’t even come close to undercutting its central objective."  IndieWires writeup that gives it a grade of B+ praises the films cast, saying "Much of the odd comedic formula emerges from a pair of carefully orchestrated lead performances."  Howard Gorman of SCREAM: The Horror Magazine gave Faults a 4.5 star review, calling it a "stunning debut with a creepily cryptic, thought-provoking script loaded with caustic satire, all of which is invigorated by absolutely stunning performances."  

== References ==
 

== External links ==
*  
*  

 
 
 
 