Enna Satham Indha Neram
{{Infobox film
| name = Enna Satham Indha Neram
| image = Enna Satham Intha Neram.jpg
| caption = Promotional poster
| director = Guru Ramesh
| producer = A.V._Anoop|A. V. Anoop
| based on = 
| editing = V. T. Vijayan T. S. Jai 
| studio = A. V. A Productions
| distributor = RPP Film Factory
| cinematography = Sanjay. B. Lokanath 
| starring = Nithin Sathya M. Raja Maanu Malavika Wales
| music    = Naga 
| released =  
| country = India
| language = Tamil
| runtime = 
}}
 Tamil comedy-thriller film  directed by Guru Ramesh and produced by A.V._Anoop|A. V. Anoop. The film features quadruplets Adhiti, Aakrithi, Akshathy and Aapthi in the lead roles,    with Nithin Sathya, M. Raja, Maanu and Malavika Wales also appearing in prominent roles. The film tells the story of quadruplets who lose their way inside a zoo where they came for an excursion. The zoo keeper (Nithin) rescues them after one whole days effort.    Enna Satham Indha Neram entered the Limca Book of Records for being the "first ever film in the world arena, to cast quadruplets - four children born of the same pregnancy - in the same plot." 

==Cast==
* Adhiti, Aakrithi, Akshathy and Aapthi as the Quadruplets
:Guru Ramesh considered his decision to include quadruplets in his script as "unrealistic". After he "started writing to friends and several groups where twins and triplets socialise on Facebook", he came to know of the quadruplets through a friend. The parents of the twins were initially sceptical towards their casting, but after Guru Ramesh narrated the script to them, they agreed. http://www.thehindu.com/features/cinema/shooting-with-quadruplets/article6085415.ece 

* Nithin Sathya as Kadhir, The Zoo Keeper
 deaf and mute students.
:To prepare for her role, she had to attend a workshop for learning sign languages. Her teacher was the same person who taught Jyothika sign language for Mozhi (film)|Mozhi (2007). 

* M. Raja as Raja, the father of the Quadruplets
:On Rajas casting, Guru Ramesh said, "Despite being a celebrity director, he didn’t complain when I told him that he was not a full-fledged protagonist. He was fine with it. In fact, I would say there is no one protagonist in the film." 

* Maanu as the mother of the Quadruplets.  sick in 2011), and narrated the script to him. On Rajinikanths suggestion, Maanu finally accepted to act in the film,  although his only concern was whether she should play "a mother of four seven-year-olds", which was not an issue for Maanu. 
*Puralavan

*Imman Annachi

*Manobala

* Sivashankar

* Swaminathan as Murugaraj, a Blue Cross Member

*Vaiyapuri

==Production== Zoological Park Thalakkonam falls. comeback film Lollusabha Swaminathan. 

==Critical reception==
M. Suganth of The Times of India rated it 1.5 out of 5 stars, saying "The only sensible beings in the film are the kids themselves, and they seem more than capable of taking care of themselves, despite their age and disabilities — when one of them falls in a pit, they rescue her all by themselves, intelligently signal their location with balloons, and even use the tab that they have to find their way out of the place. And, it is only for them that you even endure the film."  Udhav Naig of The Hindu said, "Despite the emotional set-up — four hearing- and speech-impaired children have lost their way inside the zoo with a giant snake — its a bummer, from start to finish."  Behindwoods said, "The filmmaking aspects of the movie are below par, with only the shots from the snake’s point of view standing out for being a little innovative. The VFX work on the snake is amateurish but it isn’t fair to expect much on this front, from ESIN, considering the budget constraints. But, animal lovers would get to see some lovely shots of the zoo." and rated the film 1.5 out of 5 stars. http://behindwoods.com/tamil-movies/enna-satham-indha-neram/enna-satham-indha-neram-review.html 

==References==
 

==External links==
* 

 
 