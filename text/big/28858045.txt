Syzyfowe prace (film)
{{Infobox film
| name           = Syzyfowe prace
| image          =
| alt            = 
| caption        = 
| director       = Pawel Komorowski
| producer       = Jerzy Fidler
| writer         = Pawel Komorowski
| starring       = Lukasz Garlicki Bartek Kasprzykowski
| music          = Jerzy Matuszkiewicz
| cinematography = Wieslaw Zdort
| editing        = Barbara Fronc
| studio         = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        =  
| language       = Polish
| budget         = 
| gross          =
}} novel of the same name by Stefan Żeromski. It was released in 2000. 

==Plot==
The story takes place in the second half of the nineteenth century in the Polish territories that were annexed by Russia. The main character, Marcin Borowicz is sent to a village school in Owczary, in order to get some basic education that will later on allow him to pass his entry exams and continue his education at a russian high school (Gymnasium) in Kielce. At the junior school he has his first experiences of the Russification process of which is initially unaware but which is slowly beginning to have a severe impact on his political views and his beliefs about the Polish history and culture.

A few years later, in summer 1884, Marcin Borowicz travels with his mother and Joseph to Kielce in order to take the entry exams for the Kielce Gymnasium. Having passed his exams, he is accepted as a preparatory student and begins his education. However, Martin finds it difficult to get along with his new classmates, especially those that are often unpleasant or someteimes even downright mean to him and other pupils. One day after school Borowicz goes to detention for a fight with one of the students and for talking in Polish instead of Russian (which is the compulsory language and has to be used at school at all times). Later on, having arrived late for the Mass at the school chapel, he is a witness to a heated argument between the old priest and one of the school teachers, Mr. Majewski, who demands that the Mass is celebrated in Russian in line with the new school regulations. The prefect, however, is adamant; he strongly insists on conducting the Mass in Polish and the conversation is quickly becoming so unpleasant that the priest finally loses his temper, tells Majewski that the discussion is over and unceremoniously orders the teacher to get out of the chapel and shows him to the door. Shortly after that, the priest, discovers that Martin witnessed the entire incident and instructs him to keep it a secret and not to discuss what he saw with anyone.... 

==References==
 

==External links==
*  

 
 
 
 
 

 