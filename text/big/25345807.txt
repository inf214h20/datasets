Carl Gustav, gjengen og parkeringsbandittene
{{Infobox film
| name           = Carl Gustav, gjengen og parkeringsbandittene
| image          = 
| image size     =
| caption        = 
| director       = Ola Solum
| producer       = 
| writer         = Jan Lindvik   Inge Tenvik
| narrator       =
| starring       = Frank Arne Johansen   Merete Andersen   Kjetil Bøe   Marius Hansteen   Jarle Rosenlund   Marianne Øversveen
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 26 August 1982
| runtime        = 79 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
Carl Gustav, gjengen og parkeringsbandittene (Carl Gustav, the Gang and the Parking Bandits) is a Norwegian childrens mystery film from 1982. It is about 12-year-old Carl Gustav and his friends, who one day discover a playground has been converted to a parking lot. Several other playgrounds in suburban Bergen disappear. The film was directed by Ola Solum and featured Frank Arne Johansen in the lead role. The film was produced by Norsk Film and given a seven-year rating. 

The film received good reviews from Aftenposten and Dagsavisen|Arbeiderbladet. It received criticism for using too much special effects, a long title and a thin plot, but was appeased for Solums good work as a director and the good use of children in the main roles. 

==References==
 

 
 
 


 
 