Irreversi
{{Infobox Film Mandarin version: Hui lu) 
| image          = Irreversi.jpg
| image size     = 200px
| caption        = Irreversi theatrical poster
| director       = Michael Gleissner
| producer       = Irreversi: Kacy Andrews Lisa Schahet Hui lu: Elliot Tong
| writer         = Michael Gleissner Mark Jacyszyn Scott Kurttila Andrew Lacrosse Hao Qin
| music          = Erik Godal Jaye Muller Ben Patton
| cinematography = Irreversi:Jack Messitt Hui lu:Jason Kwan
| editing        = Irreversi: Stanley Tam Kristoffer Villarino Hui lu: Kwok-Wing Leung
| studio         = Bigfoot Entertainment
| distributor    = Bigfoot Entertainment
| released       = Irreversi: 2010 ( )
| runtime        = 87 minutes
| country        = 
| language       = Irreversi: English Hui lu: Mandarin
| budget         = est. $2,000,000 US
| preceded_by    = 
| followed_by    = 
}}
 2010 dramatic film directed by Michael Gleissner and shot in Hong Kong as the English language version of Gleissners Standard Chinese|Mandarin-language film Hui lu, which he filmed and directed at the same time in the same locations but with an entirely different cast.    The title refers to how a situation can reverse and turn against the original perpetrator.

==Production== Vietnamese supermodel Bebe Pham and Geissner himself using the pseudonym of Ken Arden.  Gleissner directed both films and produced the English version.  Elliot Tong produced the Chinese version,  which was itself released in Hong Kong in 2007 and screened in Beijing, Shanghai, Guangzhou, Shenzhen and Chengdu in 2009. 

==Synopsis==
Adam, a young entrepreneur, has sold his technology company.  The money allows him and his new wife Lynda to enjoy a life of luxury, but their marriage becomes unstable when Lynda begins to suspect that Adams newly acquired fortune is related to the recent death of her brother.

==Cast==

===Irreversi===
* Mei Melancon as Lynda
* Ian Bohen as Adam
* Estella Warren as Kat 
* Kenny Doughty as David 
* Michael Gleissner as Charlie (as Ken Arden) Caroline Carver as Terry 
* Bebe Pham as Show Model 
* Kersten Hui as Taxidermy Boss
* Jo Wee as Ling
* Christian Mills as Simon
* Grean Villacarlos as John

===Hui lu===
  )]]
* Margaret Wang as Lynda Wei 
* Hawick Lau as Adam Liu 
* Sasha Hou as Kat Chen  Hao Qin as David Du 
* Michael Gleissner as Charlie (as Ken Arden)
* Susanna Fung as Terry 
* Bebe Pham as Show Model 
* Kersten Hui as Taxidermy Boss
* Yao Hsiao as Ling
* Elliot Tong as Simon Chu
* Philip Ng as John Wei

==Recognition==

===Awards and nominations===
In 2008 Ireversi - Hiu lu Won the Award of Merit at the 6th Annual Accolade Competition. 

==References==
 

   

   

   

   

   

 

==External links==
* Irreversi  
* Hui lu  
*  
*  
*  
*  
*  

 
 
 