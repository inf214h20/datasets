In the Company of Actors
{{Infobox film
| name           = In The Company of Actors
| image          = File:In_The_Company_Of_Actors_Official_Movie_Poster.png
| director       = Ian Darling, 
| producer       = Ian Darling
| cinematography =
| distributor    =
| runtime        = 75 minutes
| country        = Australia
| language       = English
}}
 documentary produced by Shark Island Productions and directed by Ian Darling.

==Subject==

In The Company of Actors is a documentary featuring an ensemble of Australias finest Actors, Cate Blanchett, Hugo Weaving, Justine Clarke and Aden Young as they prepare a production of Hedda Gabler from rehearsals at Sydney Theatre Company to opening night at the Brooklyn Academy of Music (BAM) in New York. 

The documentary lets the audience take a rare glimpse into the challenging creative process within the sacred space of the rehearsal room. 

It was broadcast in Australia on ABC1 February 2008. 

===Education and Outreach===

The film and study guide  package was donated to English, Drama and Media departments in all secondary schools across Australia with support from the Caledonia Foundation.

===Accolades===

Official Selection 
Sydney Film Festival in 2007 ; Melbourne International Film Festival, Vancouver International Film Festival, St Tropez Internationales du Cinema des Antiodes, Santa Barbara International Film Festival, Mumbai International Film Festival, OzFliz Ontario, The London Australian Film Festival, River Run International Film Festival.

==References==
 

==External links==
*  
*  
* 

 
 
 
 


 
 