Habba
{{Infobox film
| name           = Habba
| image          = 
| director       = D. Rajendra Babu
| producer       = Nara Bharathi Devi
| starring       =  
| story          = J. K. Bharavi
| screenplay     = D. Rajendra Babu
| music          = Hamsalekha
| cinematography = P. K. H. Das
| editing        = Shashikumar
| studio         = Chinni Chitra
| distributor    = 
| country        = India
| language       = Kannada
| running length = 142&nbsp;minutes
| released        =  
}}
 Kannada family Kasthuri and Charulatha.  The movie was produced by Bharathi Devi for Chinni Chitra productions. The film was remade in Telugu as Chandravamsam with Ghattamaneni Krishna|Krishna. 

==Cast== Vishnuvardhan
*Jayapradha
*Devaraj
*Ambareesh Urvashi
*Kasthuri Kasthuri
*Shashikumar
*Ramkumar Vijayalakshmi
*Charulatha
*Nagendra Babu
*Kazhan Khan
*Ravi Babu
*Bank Janardhan
*Pavithra Lokesh
*Umesh
*Sriraksha
*Naveen Kumar M S

== Awards ==
*Karnataka State Film Awards
#Karnataka State Film Award for Best Screenplay - D. Rajendra Babu

==Soundtrack== Nanditha made her entry into film songs with this soundtrack  The song "Habba Habba" is a slight modified version of the song "Yele Hombisile" which Hamsalekha had composed for the film Halunda Thavaru.

{{Infobox album  
| Name        = Habba
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Anand Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Channappa Channegowda
| lyrics1 	= Hamsalekha
| extra1        = Rajesh Krishnan, K. S. Chithra, Ramesh Chandra, Vishnu
| length1       = 05:32
| title2        = Aarathi Yettire
| lyrics2 	= Hamsalekha Nanditha
| length2       = 03:00
| title3        = Dheem Takita
| lyrics3       = Hamsalekha
| extra3 	= S. P. Balasubrahmanyam
| length3       = 04:58
| title4        = Habba Habba
| extra4        = Rajesh Krishnan, Ramesh Chandra, Nanditha
| lyrics4 	= Hamsalekha
| length4       = 04:57
| title5        = Jenina Goodu
| extra5        = Rajesh Krishnan, K. S. Chithra
| lyrics5       = Hamsalekha
| length5       = 05:25
| title6        = Koti Koti Devaru
| extra6        = Nanditha
| lyrics6       = Hamsalekha
| length6       = 01:52
| title7        = Mama Mama Masti
| extra7        = S. P. Balasubrahmanyam
| lyrics7       = Hamsalekha
| length7       = 05:04
| title8        = Mai Tholiyo Shastra
| extra8        = Nanditha
| lyrics8       = Hamsalekha
| length8       = 01:26
| title9        = Onde Usirante
| extra9        = Rajesh Krishnan, K. S. Chithra
| lyrics9       = Hamsalekha
| length9       = 06:06
| title10        = Yaare Yaare
| extra10        = Rajesh Krishnan, K. S. Chithra
| lyrics10       = Hamsalekha
| length10      = 05:15
}}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 