Lullaby Land (1933 film)
{{Infobox Hollywood cartoon
| cartoon name      =Lullaby Land
| series            = Silly Symphonies
| image             = 
| image size        = 
| alt               = 
| caption           = 
| director          = Wilfred Jackson
| producer          = 
| story artist      = 
| narrator          = 
| voice actor       = 
| musician          = 
| animator          = 
| layout artist     = 
| background artist =  Walt Disney Productions
| distributor       = United Artists
| release date      = 1933
| color process     = Technicolor
| runtime           = 
| country           = United States
| language          = English
| preceded by       = 
| followed by       = 
}}
 animated Disney short film. It was released in 1933.  The quilt from Lullaby Land inspired the garden section of the Storybook Land Canal Boats ride at Disneyland California. 

==Plot==
A baby is transported to "Lullaby Land of Nowhere", where pacifiers grow on trees, diapers, bottles, and potty chairs march on parade, and the gingham dog comes to life. He wanders into the "forbidden garden", full of things like scissors, knives, and fountain pens that are not for baby and begins smashing watches with hammers and playing with giant matches. The matches chase after him; baby escapes by riding a bar of soap across a pond, but the smoke from the matches turns into boogey-men, which chase baby but vanish not long afterwards. The benevolent sandman, dressed as a wizard, spots baby hiding and works his magic, bringing baby to sleep.

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 