The Business (film)
 
{{Infobox film
| name           = The Business
| image          = Businessthe.jpg
| caption        = Film poster
| director       = Nick Love
| producer       = Allan Niblo James Richardson
| writer         = Nick Love Geoff Bell Georgina Chapman
| music          = Ivor Guest
| cinematography = Damian Bromley
| editing        = Stuart Gazzard
| studio         = Vertigo Films Monkey Productions S.L. Powder Films
| distributor    = Pathé
| released       = 2 September 2005
| runtime        = 97 minutes
| country        = United Kingdom Spain
| language       = English Spanish
| budget         = £2,000,000
| gross          = £1,535,641
}}
 The Football Geoff Bell and Georgina Chapman. The plot of The Business follows the Greek tragedy-like rise and fall of a young cockneys career within a drug importing business run by a group of British ex-pat fugitive criminals living on the coast of the Costa del Sol (aka the "Costa del Crime") in Spain.

==Plot== South East Thatcher era of the 1980s, with little hope of ever making anything of himself, yet he dreams of "being somebody" and escaping his lonely, isolated lifestyle. After seriously assaulting his mothers abusive boyfriend, he becomes a fugitive, and through family connections escapes to Spains Costa Del Sol. His job there is to deliver a bag containing drugs and cash to "Playboy Charlie" (Hassan), an ex-pat and criminal-on-the-run, a suave and dapper man who is very successful in Spain and runs his own nightclub and drugs business and lives a life of excess and luxury. Impressed by Frankies honesty in not opening the bag, Charlie takes a liking to Frankie, introduces him to his business associates, including the psychopathic Sammy (Bell), and invites him to remain in Spain and work as his driver.
 cannabis across the Strait of Gibraltar from Morocco, in which children are used although the children are sometimes shot dead by the Spanish Navy patrolmen.

The film then follows the rise-and-fall pattern common to many gangster films, showing first the criminals living the high life as their cannabis trade is booming, and then the downfall as greed and jealousy (not helped by the obvious attraction between Frankie and Sammys beautiful trophy wife Carly) introduce rivalries between them, and eventually split them up. Charlie and Frankie decide to go into business alone, importing cocaine instead of cannabis through drop-offs from Colombian aeroplanes, but this is the cause of the final catastrophe. Not only do they both become increasingly addicted to the drug itself, but also the local mayor, who had been happy to ignore the cannabis trade but had warned them not to import cocaine, discovers what they are doing and uses the weight of the law to shut them down and close their businesses. An assassination attempt on the mayors life ends in failure, and the gruesome death of one of the gang.

Six months later Frankie and Charlie are homeless thugs with nothing to lose and reduced to stealing in order to survive. While organising a disappointing reunion party at Charlies old bar (which Frankies former heroin addict friend Sonny is now running), Frankie meets the scheming Carly again and decides to make one last deal. He invites Sammy in on a pick-up, but while both intend to betray the other, Carly has given Sammy a pistol with an empty clip. Sammy tries to shoot Frankie, who in turn attacks him with a rock. The fight ends quickly as Spanish Navy patrolmens gunfire fatally shoots Sammy, and Frankie escapes through a sewage pipe. Frankie emerges from the sewer to meet Carly, who had masterminded the whole thing, finally getting his happy ending. But at the last minute, he realises he cant trust Carly when he finds another pistol in her handbag amongst their money, so he knocks her out and drives off triumphantly into the sunset on his own.
 working on the door. The theatrical ending also reveals that "Carly went back to her parents house in Penge", "Sammy went to Hell" and "Frankie went to Hollywood" - an obvious pun on the name of popular 1980s band Frankie Goes to Hollywood.

==Cast==
* Danny Dyer as Frankie
* Tamer Hassan as Charlie Geoff Bell as Sammy
* Georgina Chapman as Carly
* Linda Henry as Shirley
* Roland Manookian as Sonny
* Camille Coduri as Nora
* Andy Parfitt as Andy
* Michael Maxwell as Jimmy
* Arturo Venegas as The Mayor
* Eddie Webber as Ronnie
* Dan Mead as Danny
* Martin Marquez as the mayors aide
* Sally Watkins as Mum
* Alex Goodger as Dead Moroccan Child

==Soundtrack== Ivor Guest, The Football Factory). The songs featured include:
 Planet Earth" Welcome to the Pleasuredome"
# Mary Jane Girls – "All Night Long"
# The Cult – "Wild Flower" Loose Ends – "Hangin on a String (Contemplating)"
# Rick James – "Ghetto Life" Blondie - Heart of Glass"
# Simple Minds – "Dont You (Forget About Me)"
# Martha and the Muffins – "Echo Beach"
# The Buggles – "Video Killed the Radio Star"
# A Flock of Seagulls – "I Ran (So Far Away)"
# Belouis Some – "Imagination" Shannon – Let the Music Play" Modern Love"
# Talk Talk – "Its My Life (Talk Talk song)|Its My Life"
# The Knack – "My Sharona"
# Roxy Music – "Avalon"
# Simple Minds - "Themes For Great Cities" Maid of Orleans"
# Adam and the Ants – "Kings of the Wild Frontier" Blondie – Call Me"
# Kim Carnes – "Bette Davis Eyes"

==Critical reception==
The Business was nominated for "Best Achievement in Production" at the 2005 British Independent Film Award, {{cite web|url=http://www.imdb.com/title/tt0429715/awards|title=The Business (2005)
Awards|publisher=IMDb.com|accessdate=2014-08-05}}  although it lost to Jan Dunn|Gypo. 
 Layer Cake". The Football Factory"; Whats on TV said "A rush of 80s mood, fashion and music and nailbiting climax are the icing on a brilliant crime cake". 

Outside the world of lads mags, critical reception to the film was mixed to negative. The Business currently has a 50% rating on aggregate ratings site Rotten Tomatoes based on six reviews. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 