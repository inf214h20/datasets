In the Presence of Mine Enemies (film)
{{Infobox television film bgcolour     =  name         = In the Presence of Mine Enemies image        = File:In the Presence of Mine Enemies 1997 TV film DVD cover.jpg image_size   = 200px  image_sized  =   caption      = DVD cover format  Drama
|runtime      =  director     = Joan Micklin Silver producer     =  writer       = Rod Serling original script starring     = Armin Mueller-Stahl Charles Dance Elina Löwensohn Chad Lowe   And Introducing Jason Schwartz as Israel editing      =  music        =  country      = United States language  English
|first_aired  = 1997 network      =  released     = 
}} Showtime TV movie about the Warsaw Ghetto Uprising in World War II.

The film is a remake of an original TV drama scripted by Rod Serling for Playhouse 90 which originally starred Charles Laughton. 

The plot centres on a rabbi (played in the 1997 version by Armin Mueller-Stahl), and his children (Elina Lowensohn and Don McKellar). The movie also features Charles Dance as a German officer, and introducing Jason Schwartz as Israel leader of the orphan rebellion.  

==References==
 

==External links==
*  

 
 
 
 