Tales of a Golden Geisha
{{Infobox film name        = Ageman image       = Tales of a Golden Geisha.jpg caption     = director    = Juzo Itami producer    =  Juzo Itami  writer      = Juzo Itami  starring    = Nobuko Miyamoto Masahiko Tsugawa Mitsuko Ishii Hideji Otaki Shōgo Shimada (actor)|Shōgo Shimada distributor = released  1990
|runtime     = 124 min. language  Japanese
|budget      =
}} director Juzo Itami.

The film stars two of Itamis regular actors, Nobuko Miyamoto as a geisha who brings luck to the men with whom she sleeps, and Masahiko Tsugawa as her unfaithful, sometimes partner. As well as showing her relationships with the man she loves and the men who employ her, it satirizes corruption and the influence of money in Japanese politics.
 Japanese Academy Awards - Best Director (Juzo Itami), Best Actress (Nobuko Miyamoto), Best Screenplay (Juzo Itami) and Best Editing (Akira Suzuki).   
 

== Cast ==
*Nobuko Miyamoto as Nayoko
*Masahiko Tsugawa as Mondo Suzuki

*Ryunosuke Kaneda as Tamonin
*Atsuko Ichinomiya as Rin, Tamonins mother
*Kin Sugai as Foster mother
*Kazuyo Mita as Hiyoko
*Mitsuko Ishii as Eiko
*Yoriko Douguchi as Junko
*Maiko Minami as Sayori-chan
*Fukumi Kuroda as Kiyoka
*Isao Hashizume
*Haruna Takase as Fur shop manager
*Tokuko Sugiyama as Restaurant manager
*Yakan Yanagi as Foster father
*Michiyo Yokoyama as Tamonins court lady
*Hiroko Seki as Shinkame Chiyo restaurant manager
*Noborou Yano as Hirutaji chief
*Yan Yano as Hirutaji chief
*Harukazu Kitami as Bo-san #1
*Akira Kubo as Bo-san #2
*Yoshihiro Kato as Segawa Kikunojo
*Akari Uchida as Houte couture woman
*Mihoko Shibata as Woman carer in movie
*Mansaku Fuwa as Executioners assistant in the dream
*Yôichi Ueda as Camera man
*Shinobu Oshizaka as Doctor
*Eijirō Tōno as Prime Minister
*Kiyokata Saruwaka as Traditional dance teacher
*Kazuo Kitamura as Tsurumaru
*Akira Takarada as Inukai
*Shōgo Shimada (actor)|Shōgo Shimada as Zenbu Okura
*Hideji Ōtaki as Chijiwa (uncredited)
*Hiroshi Okouchi as Chichiiwa

==References==
 

==External links==
* 

 

 
 
 
 
 


 