The Squeaker (1963 film)
 
{{Infobox film
| name           = The Squeaker
| image          = Poster of the film Der Zinker.jpg
| caption        = Film poster for Der Zinker
| director       = Alfred Vohrer
| producer       = Horst Wendlandt Jacques Willemetz
| writer         = Harald G. Petersson based on a novel by Edgar Wallace
| starring       = Heinz Drache Peter Thomas
| cinematography =   Hermann Haller
| studio         = Rialto Film, Les Films Jaques Willemetz
| distributor    = Constantin Film
| released       =  
| runtime        = 89 minutes
| country        = West Germany France
| language       = German
| budget         = 
}}
 very successful series of German films based on the writings of Edgar Wallace and adapted from the 1927 novel of the same name.

==Plot==
Both Scotland Yard and the criminal community of London are trying to discover the identity of "the Squealer". This mysterious fence forces criminals to sell him their wares for a pittance. When some object, he "squeals" to the police. Those who oppose him are ruthlessly killed, preferably by means of the poison of the Black Mamba. Inspector Elford of Scotland Yard investigates and he has plenty of suspects. The trail leads to the strange Mr. Sutton, owner of a zoological store that also carries predators and poisonous snakes. During his investigation, Elford meets Mrs. Mulford, an older lady who tries to help ex-convicts, and Beryl, her niece, who writes crime stories and works as a court reporter. Inspector Elford discovers a similarity between the typeset of a machine also used by Sutton and letters written by the Squealer. To make Sutton confess, he is tricked by Mrs. Mulford in cooperation with the police into drinking what he believes to be poisoned tea.       

==Cast==
* Heinz Drache - Inspector Bill Elford
* Barbara Rütting - Beryl
* Günter Pfitzmann - Frankie Sutton
* Jan Hendriks - Mr. Leslie
* Inge Langen - Millie Trent
* Agnes Windeck - Mrs. Mulford
* Wolfgang Wahl - Sergeant Lomm
* Siegfried Wischnewski - Der Lord
* Siegfried Schürenberg - Sir Geoffrey Fielding
* Albert Bessler - Butler
* Heinz Spitzner - Dr. Green
* Erik von Loewis - Juwelier
* Stanislav Ledinek - Der Champ
* Winfried Groth - Jimmy
* Eddi Arent - Josua Jos Harras
* Klaus Kinski - Krishna

==Production== very successful 1931 (Germany) 1937 (again United Kingdom). In 1961, an initial treatment had been written by Egon Eis who had earlier also worked on the script for the 1931 film made in Germany. However, he did not want to copy his own work and refused to do the final script for the new film. Petersson, who so far had only reworked scripts for Rialto, was now tasked with writing an entire script for the first time. His script was much more terse than the novel and it found the approval of producer Wendlandt.  
 widescreen method.     
Alfred Vohrer directed the film. To boost production values, some scenes were shot on location in a wintry London.  Additional exteriors were shot in West Berlin. CCC studios were used for interior cinematography—making this the first of a total of 15 films from the series that used Artur Brauners studios.    Filming took place between 22 January and 28 February 1963.  This was the first film of the series to feature the signature beginning with a series of shots ringing out followed by a (coloured) bloodstain spreading across the screen as an invisible voice intones "Hello, this is Edgar Wallace!". 
 FSK gave the film a rating of 16 and up and found it not appropriate for screenings on public holidays. It was released on 26 April 1963. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 