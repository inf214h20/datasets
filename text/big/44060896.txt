Etho Oru Swapnam
{{Infobox film
| name           = Etho Oru Swapnam
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       = Sreekumaran Thampi
| writer         = K Surendran Sreekumaran Thampi (dialogues)
| screenplay     = Sreekumaran Thampi
| starring       = Jayan Sheela Jagathy Sreekumar Sreelatha Namboothiri
| music          = Salil Chowdhary
| cinematography = Hemachandran
| editing        = K Sankunni
| studio         = Bhavani Rajeswari
| distributor    = Bhavani Rajeswari
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed and produced by Sreekumaran Thampi. The film stars Jayan, Sheela, Jagathy Sreekumar and Sreelatha Namboothiri in lead roles. The film had musical score by Salil Chowdhary.   

==Cast==
 
*Jayan
*Sheela
*Jagathy Sreekumar
*Sreelatha Namboothiri
*Vaikkam Mani
*Sukumaran
*Kailasnath
*Kanakadurga
*Mallika Sukumaran
*Nanditha Bose
*Priyamvada Ravikumar
*Somasekharan Nair
 

==Soundtrack==
The music was composed by Salil Chowdhary and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Oru Mukham Maathram   || Sabitha Chowdhary || Sreekumaran Thampi ||
|-
| 2 || Oru Mukham Maathram   || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Poo Niranjaal || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 4 || Poomaanam || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 5 || Sreepadam Vidarna || K. J. Yesudas, Chorus || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 