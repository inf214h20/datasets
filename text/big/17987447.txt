Subramaniapuram
 
{{Infobox film
| name           = Subramaniapuram
| image          = Subramaniapuram.jpg
| caption        = poster
| director       = M. Sasikumar
| producer       = M. Sasikumar
| writer         = M. Sasikumar Jai Colours Swathi M. Sasikumar Ganja Karuppu Samuthirakani
| music          = James Vasanthan
| cinematography = S.R. Kathiir
| editing        = Raja Mohammed
| studio         = Company Productions
| distributor    = Company Productions
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
Subramaniapuram is a 2008 Tamil film produced, written, and directed by M. Sasikumar. The low-budget film received critical acclaim for its original script, expert direction, screenplay, editing, accurate sets and costumes to resurrect Madurai from the 1980s. Sasikumar cast then relatively new actors Jai (actor)|Jai, Colours Swathi|Swathi, Ganja Karuppu and himself in pivotal roles. Shot in a remarkable 85 days, it became a huge box office grosser. Sasikumar is now considered one of the promising young directors in Tamil film industry.The film was latter dubbed into Telugu with the name Ananthapuram 1980. It was remade in Kannada with the name Prem Adda which opened to generally positive reviews on December 7, 2012.

==Plot==
The story takes place in the Subramaniapuram area of Madurai city. A convict is released from prison in 2008 and is stabbed outside the prison gates...

The film then moves to 1980 where Azaghar (Jai (actor)|Jai), Paraman (M. Sasikumar), Kaasi (Ganja Karuppu), Dopa and Dumka, a polio-stricken physically challenged person, are part of a set of close friends, who are unemployed. They pass their time drinking liquor and fooling around on the streets opposite the house of an ex-councilor Somu. Kanugu (Samuthirakani) is his brother. Apart from them the family consists of Somus wife, their three children including Thulasi (Colours Swathi|Swathi) and Thulasis uncle.

The five friends, particularly Paraman and Azhagu, often end up in jail due to frequent fighting. Cops get a call from someone complaining about the friends each time they do something wrong. Every time they are arrested, Kanugu and Somu bail them out immediately. In the meantime, Azhagu and Thulasi develop mutual feelings for each other. Paraman is against his friend developing feelings for a girl and Azhagu, not heeding to his friends thoughts, throw up quite a few funny scenes.

There are signs of things to come when Somu is not selected in a local temples committee for a function. Things take a sudden turn just before intermission when Somu loses his partys councilor post and is ridiculed by his wife for being jobless. This leads Kanugu to lock himself up in a lodge and drink all day. He makes sure the friends hear about him and come to visit. He requests them to murder the person who was chosen for councilors post ahead of his brother. Azhagu, Paraman and Kaasi hatch a plan and execute the person almost perfectly. The first half ends here with them running away after the murder leaving a cycle behind.

The second half begins with the cops finding out that Paraman and Azhagu have committed the murder with the help of the cycle they left behind. They surrender themselves before the court hoping that Kanugu will bail them out soon. But they come in for rude shock when they learn through Kaasi that Somu has been selected for the councilors post and is avoiding their contact. They come to terms with reality and stay helpless in jail where they befriend a fellow inmate. He learns their situation and bails them out.

The same friend who aided these guys expects a favor from them &mdash; kill his brother-in-law for murdering his sister. Accomplishing this task, these guys now look out for killing Kanugu who cheated them. In the meantime, Thulasi and Azaghu continue to meet up. This leads to Azhagan almost getting killed by Kanugus men. The friends strike back killing those men later in the day. A few days later they end up hurting Thulasis uncle in their bid to kill Kanugu. To save his life from the clutches of these buddies, Kanugu sets a trap for Azhagar using Thulasi as bait and kills him using his henchmen. Paraman takes revenge for his friends death by decapitating Kanugu and laying his head at his friends murder site. Paraman then calls to Kasi and reveals how he killed Kanugu, during which he sees Somus henchmen rushing behind Kasi. Kasi betrays Paraman and leaves him at the mercy of the henchmen.

The story shifts back to the present day where its revealed the person who was stabbed outside the prison walls is Kasi. He lies in the hospital in critical condition and is being interrogated by a policeman. The doctor intervenes and asks him to leave, after which Dumka comes in and reveals that it is Dopa who stabbed him and then proceeds to remove his air supply and kills him after reminding him of his betrayal.

==Cast== Jai as Azhagar Swathi as Thulasi
* M. Sasikumar as Paraman
* Ganja Karuppu as Kaasi
* Samuthirakani as Kanugu
* Maari as Dumka
* Vichithran as Dopa
* K. G. Mohan as Shitta
* Raja
* Jayachandran
* Jayaprakash
* Murugan

==Resurrecting the 1980s: The sets==
Sasikumar started collecting old photos, banners and boards of shops to imitate the style of writing from that period and also searched the Internet for photographs of the 80s. He especially researched photographs of wedding processions along the streets for a clear picture of how the roads looked and the kind of vehicles in use. The team relied on this visual information to construct the sets for the movie.  . behindwoods.com.  

==Music==
Asked why he chose to work with James Vasanthan, a debutant music director when he had many other options, Sasikumar spoke of his apprehension to approach an established music director as he was a debut director himself. I was not sure whether they would listen to me and give me what I wanted he said in an interview.  Vasanthan had been Sasikumars music teacher at St. Peter’s boarding school in Kodaikkanal. Sasikumar approached James with the project and the music became a remarkable success.

===Soundtrack===

Subramaniapuram has five songs composed by James Vasanthan. This is the first time a Tamil movie featured a promotional song. (The song does not feature in the movie but has been released to media.)

{| border="2" cellpadding="4" cellspacing="0" style="margin:1em 1em 1em 0; background:#f9f9f9; border:1px #aaa solid; border-collapse:collapse; font-size:95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Notes
|-
| 1 || Kangal Irandal || Belly Raj, Deepa Miriam ||
|-
| 2 || Kadhal Siluvayil Araidhaal Ennai || Shankar Mahadevan ||
|-
| 3 || Madera Kulunga Kulunga || Velmurugan, Suchitra ||
|-
| 4 || Theneeril Snehitham || Benny Dayal ||
|-
| 5 || Subramaniyapuram Theme || Belly Raj ||
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 