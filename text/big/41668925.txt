NH10
 
 
{{Infobox film
| name           = NH10
| image          = NH10 Poster.jpg
| alt            = Official release poster
| caption        = Theatrical release poster Navdeep Singh
| producer       = Vikramaditya Motwane Anurag Kashyap Vikas Bahl Anushka Sharma Sunil Lulla Krishika Lulla Karnesh Sharma
| writer         = Sudip Sharma
| starring       = Amit Saha(Balurghat) Anushka Sharma Arindam Goswami(Durgapur)Neil Bhoopalam
| music          =  Anirban Chakraborty   Sanjeev-Darshan   Ayush Shrestha   Savera Mehta   Samira Koppikar
| cinematography =  
| editing        =
| studio         = Phantom Films Clean Slate Films
| distributor    = Eros International
| released       =   
| country        = India
| language       = Hindi
| budget         =    
 
| gross          =    
}} Indian crime-thriller thriller film Navdeep Singh. National Highway 10 in India.

NH10 released theatrically on 13 March 2015. The film and Sharmas performance garnered critical acclaim, and the film emerged as a sleeper hit.  {{cite web|url=http://www.hindustantimes.com/bollywood/n
h10-and-more-sleeper-hits-a-new-trend-in-bollywood/article1-1327254.aspx|title=NH10 and more: Sleeper hits, a new trend in Bollywood|author=Prashant Singh|work=Hindustan Times|accessdate=17 March 2015}}  

==Plot==
 
The film starts with couple Meera (Anushka Sharma) and Arjun (Neil Bhoopalam) going to a party, where Meera gets an urgent work call from office. On the way she is attacked by some street punks who smash her cars window. Meera manages to escape but is shaken up by the incident. Meanwhile, Arjun suggests a road trip for Meeras upcoming birthday. The couple heads out the next day. Though reluctant, they continue their journey. While stopping at a roadside Dhaba for lunch, a petrified young girl arrives and pleads for help, saying few men outside are about to kill her. Meera shakes her off. As they wait, they see a gang of men round up the same girl and a boy, beat them savagely and drag them to a SUV. Arjun intervenes but Satbir (Darshan Kumar), the gang leader pushes him, saying that the girl is his sister. As the horrified couple watches, the gang drives off. Despite Meeras pleading, Arjun drives after the gang. He stops the car near the gangs vehicle on a deserted area near the highway. There the couple witness a brutal Honor killing of a young boy and the girl Pinky (Kanchan Sharma). Terrified Arjun and Meera attempt to flee but the gang finds them . As the gang digs graves, Satbir shoots Pinky, his sister. Meanwhile a fight ensues and Arjun shoots Chotte, one of the gang member. The gang is enraged and the couple runs for their lives.

At night, the couple suddenly faces one of the gang members. Meera shoots him dead and Arjun gets injured. They make it to a railway bridge where Meera tells Arjun to wait till she gets help. She finds a police station where she asks a police man to help as they have witnessed an Honour killing – at the mention of that word, the cop throws Meera out of the police station. On the way, a senior police officer meets her in his van and starts driving back to where Arjun is . Meera realises that the police officer is with her attackers. She manages to stab him and drives off, with the gang chasing her. As they find her car looking, Meera stumbles off and finds a labourers hut on the site. The labourer and his wife protect her from the gang. But they advise her to go to the Chief of nearby village.

Later Meera reaches the village and meets two children and one agrees to take her to the village Chief – his own grandmother Ammaji (Deepti Naval), to whome Meera tells her story. Ammajis mood changes the moment she hears of the honour killing and a puzzled Meera looks around to see pillow covers saying Pinky. Ammaji locks her in the room and calls the gang. They drag her out and beat her in the courtyard. She rushes to the under-bridge to find Arjun has been murdered. 

Meera shrieks in grief and rage and returns to the village where she drives the jeep ruthlessly into two gang members, killing them. Then she kills every gang members. Ammaji arrives and gasps, seeing all the men dead. She says Pinky was her own daughter but broke rules and needed to be punished. Meera echoes her words and leaves the village as dawn breaks.
 
==Cast==
* Anushka Sharma as Meera
* Neil Bhoopalam as Arjun
* Darshan Kumaar as Satbir
* Deepti Naval as Ammaji
* Tanya Purohit Dobhal as Satbirs wife

==Production==
Shooting of the film began in mid January 2014 in Delhi.  The team shot in locations across the northern plains of India, such as Jodhpur where they experienced sandstorms.  The films shooting was wrapped up in May and was scheduled for a September release. However the film officially released on 13 March 2015.

The official trailer was launched at a suburban multiplex in Mumbai on 5 February 2015 in presence of the cast and producers. 

==Release==
 Navdeep Singh Censor board wanted to ban the movie.  It was subsequently passed with nine cuts and an "Adults only" certificate. 

==Reception==

Anubhav Parsheera of India Today gave 4 stars out of 5 and wrote "NH10 is a necessary tale of revenge".  Shubha Shetty-Saha of Mid Day|mid-day gave 4 stars out of 5 and called it "a non-stop, relentless, edge-of-the-seat experience".  Rajeev Masand of Web18|Ibnlive.com rated the film 3.5 out of 5 and stated "Its often terrifying on this highway, but youll be glad you were there for the ride".   The Guardian termed it as "a misogynistic slasher movie with a topical twist"  Bollywood Hungama News Network gave it 3 stars.  NDTV gave 3 stars to the movie.   Hindustantimes critic stated "nails it in the first half, but the second half lacks the same fluidity and penetration power. NH10 displays a great potential and then fails to capitalise on it."  

Certain journalists believed that the film was inspired from the 2008 British thriller Eden Lake,   but Singh denied the allegations. 

== Soundtrack ==
{{Infobox album
| Name = NH10
| Type = Soundtrack
| Artist = 
| Cover = 
| Released =   Feature film soundtrack
| Length =   Eros Music
}}
The soundtrack was composed by Sanjeev-Darshan, Bann Chakraborty, Abhirup Chand, Ayush Shrestha, Savera Mehta and Samira Koppikar.
{{Track listing
| lyrics_credits = yes
| music_credits = yes
| extra_column = Singer(s)
| total_length = 33:05
| title1 = Chhil Gaye Naina
| lyrics1= Kumaar 
| music1= Sanjeev-Darshan
| extra1 = Kanika Kapoor, Dipanshu Pandit
| length1 = 3:17
| title2 = Le Chal Mujhe (Male Version)
| lyrics2= Bann Chakraborty & Abhiruchi Chand
| music2=  Bann Chakraborty
| extra2 = Mohit Chauhan
| length2 = 4:13
| title3 = Main Jo
| lyrics3= Manoj Tapadia 
| music3=  Ayush Shrestha & Savera Mehta
| extra3 = Nayantara Bhatkal, Savera Mehta
| length3 = 2:44
| title4 = Khoney De
| lyrics4= Bann Chakraborty 
| music4=  Bann Chakraborty
| extra4 = Mohit Chauhan, Neeti Mohan
| length4  = 4:25
| title5 = Maati Ka Palang
| lyrics5= Neeraj Rajawat 
| music5=  Samira Koppikar
| extra5 = Samira Koppikar
| length5 = 3:11
| title6 = Le Chal Mujhe (Female Version)
| lyrics6= Bann Chakraborty & Abhiruchi Chand 
| music6=  Bann Chakraborty
| extra6 = Shilpa Rao
| length6 = 4:13
| title7 = Kya Karein Varun Grover
| music7=  Savera Mehta & Ayush Shrestha
| extra7 = Rachel Varghese
| length7 = 3:01
| title8 = Le Chal Mujhe (Reprise Version)
| lyrics8= Bann Chakraborty & Abhiruchi Chand
| music8=  Bann Chakraborty
| extra8 = Arijit Singh, Mohit Chauhan
| length8 = 3:37
| title9 = Khoney De
| lyrics9=
| music9=  Bann Chakraborty
| extra9 = 
| note9  = Instrumental Version
| length9 = 4:24
}}

==Sequel==
The producers of NH10 have confirmed that they will be making a sequel named "NH12" and explore a similar theme.  

==References==
 

==External links==
*  
*  

 
 
 


 