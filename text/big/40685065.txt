Bhama Rukmani
{{Infobox film
| name           = Bhama Rukmani
| image          = 
| caption        = 
| director       = R. Bhaskaran
| producer       = Pavithiram K. Sundaraj
| writer         = K. Bhagyaraj Raadhika Praveena Nagesh
| music          = M. S. Viswanathan
| cinematography = A. Kannan Narayanan
| editing        = R. Bhaskaran
| studio         = Sri Kamakshi Amman Movies
| distributor    = Sri Kamakshi Amman Movies
| released       =  
| runtime        = 122 minutes
| country        =   India Tamil
}}
 1980 Tamil Raadhika and Praveena in lead roles. The film, had musical score by M. S. Viswanathan and was released on 12 June 1980. 

==Cast==
* K. Bhagyaraj Raadhika
* Praveena Nagesh
* Kallapetti Singaram
* Lakshmi Narayan
* Usilaimani
* Chandran Babu
* K. A. Thangavelu
* Kanthimathi
* Sundari Bhai
* Y. G. Mahendra (Guest Appearance) Jayamala (Guest Appearance)

==Soundtrack==

The music composed by M. S. Viswanathan, while lyrics written by Muthulingam and Chidambaranathan.
{| class="wikitable"
|-
! # !! Song!! Singers
|-
| 1
| Nee Oru Kodi Malar
| S. P. Balasubrahmanyam, S Janaki
|-
|}

==References==
 

 
 
 
 


 