Ugetsu
 
{{Infobox film
| name           = Ugetsu
| image          = Ugetsu monogatari poster.jpg
| alt            =
| caption        = 1953 Japanese poster
| director       = Kenji Mizoguchi
| producer       = Masaichi Nagata

| writer         = Matsutarō Kawaguchi Yoshikata Yoda Akinari Ueda|starring Masayuki Mori Machiko Kyō Kinuyo Tanaka
| music          = Fumio Hayasaka Ichiro Saito Tamekichi Mochizuki
| cinematography = Kazuo Miyagawa
| editing        = Mitsuzō Miyata
| distributor    = Daiei Film
| released       =  
| runtime        = 94 minutes
| country        = Japan Japanese
| budget         =
| gross          =
}} book of Masayuki Mori and Machiko Kyō. It is one of Mizoguchis most celebrated films, regarded by critics as a masterwork of Japanese cinema  and a definitive piece during Japans Golden Age of Film.     Along with Akira Kurosawas 1950 film Rashomon, Ugetsu is credited with having popularized Japanese cinema in the West.    

==Plot==
Ugetsu is set in villages which line the shore of Lake Biwa in Ōmi Province in the late 16th century. It revolves around two peasant couples – Genjurō and Miyagi, Tōbei and Ohama – who are uprooted as Shibata Katsuies army sweeps through their farming village, Nakanogō. Genjurō, a potter, takes his wares to nearby Ōmizo Domain|Ōmizo. He is accompanied by Tōbei, who dreams of becoming a samurai. A respected sage tells Miyagi to warn her husband about seeking profit in time of upheaval, and to prepare for a probable attack on the village. Genjurō arrives with his profits, but she asks him to stop. Genjurō nevertheless works long hours to finish his pottery. That night Nakanogō is attacked by soldiers, and the four main characters hide out in the woods.

Genjurō decides to take the pots to a different marketplace, and the two couples travel across a lake. Out of the thick fog another boat appears. The sole passenger tells them he was attacked by pirates, warns them back to their homes, then dies. The two men decide to return their wives to the shore. Tōbeis wife refuses to go; Miyagi begs Genjurō not to leave her, but is left on the shore with their young son, Genichi, clasped to her back. At market, Genjurōs pottery sells well. After taking his promised share of the profits, Tōbei runs off to buy samurai armor, and sneaks into the ranks of a clan of samurai. Lost from her companions, Ohama has wandered beyond Nagahama in her desperate search for Tōbei. She is raped by a group of soldiers.

Genjurō is visited by a noblewoman and her female servant, who order several pieces of pottery and tell him to take them to the Kutsuki mansion. Genjurō learns that Oda Nobunaga|Nobunagas soldiers have attacked the manor and killed all who lived there, except Lady Wakasa and her servant. He also learns that Lady Wakasas father haunts the manor. Genjurō is seduced by Lady Wakasa, and she convinces him to marry her. Meanwhile, Nakanogō is under attack. Miyagi and her son hide from soldiers and are found by an elderly woman who hurries them to safety. In the woods, several soldiers desperately search her for food. She fights with the soldiers and is stabbed. She collapses with her son still clutching her back.

Tōbei steals the severed head of a general, which he presents to the commander of the victorious side. He is rewarded with armor, a mount, and a retinue. Tōbei later rides into the marketplace on his new horse, eager to return home to show his wife. However, he visits a brothel and finds her working there as a prostitute. Tōbei promises to buy back her honor. Later, the two return to Nakanogō, Tōbei throwing his armor into a river along the way.

Genjurō meets a priest, who tells him to return to his loved ones or certain death awaits him.  When Genjurō mentions the noblewoman, the priest reveals that the noblewoman is dead and must be exorcism|exorcised, and then invites Genjurō to his home where he paints Buddhist prayers on his body. Genjurō returns to the Kutsuki mansion. He admits that he is married, has a child and wishes to return home. Lady Wakasa will not let him go. They admit they are spirits, returned to this world so that Lady Wakasa, who was slain before she knew love, could experience its joys. She tells him to wash away the Buddhist symbols. Genjurō reaches for a sword, throws himself out of the manor, and passes out. The next day, he is awakened by soldiers.  They accuse him of stealing the sword, but he denies it, saying it is from the Kutsuki mansion.  The soldiers laugh at him, saying the Kutsuki mansion was burned down over a month ago. Genjurō arises and finds the mansion he has lived in is nothing more than a pile of burnt wood. The soldiers confiscate his money; but because Shibatas army burned down the prison, they leave Genjurō in the rubble. He returns home by foot, searching for his wife.

Miyagi, delighted to see him, will not let him tell of his terrible mistake. Genjurō holds his sleeping son in his arms, and eventually lies down to sleep.  The next morning, Genjurō wakes to the village chief knocking on his door. He is surprised to see Genjurō home, and expresses concern. He explains that he has been caring for Genjurōs son, and that the boy must have come to his old home in the middle of the night. Genjurō calls for Miyagi. The neighbor asks if Genjurō is dreaming, as his wife is dead. Miyagis spirit tells Genjurō: "I am always with you", while he continues on pottery, and their son offers food to her. As with others, the film closes with 終 (owari, the end), in handwritten cursive.

==Cast==
 , the setting of Ugetsu, within Japan.]]
*Machiko Kyō as Lady Wakasa
*Mitsuko Mito as Ohama
*Kinuyo Tanaka as Miyagi Masayuki Mori as Genjurō
*Eitaro Ozawa as Tōbei (as Sakae Ozawa)
*Ikio Sawamura as Genichi
*Kikue Mōri as Ukon, Lady Wakasas Nurse
*Ryōsuke Kagawa as Village Master
*Eigoro Onoe as Knight
*Saburo Date as Vassal
* Sugisaku Aoyama as Old Priest
*Reiko Kongo as an Old Woman in Brothel
*Shozo Nanbu as Shinto Priest
*Ichirō Amano as Boatsman
*Kichijirō Ueda as Shop Owner
*Teruko Omi as Prostitute
*Keiko Koyanagi as Prostitute
*Mitsusaburō Ramon as Captain of Tamba Soldiers
*Jun Fujikawa as Lost Soldier
*Ryuuji Fukui as Lost Soldier
*Masayoshi Kikuno as Soldier
*Hajime Koshikawa		
*Sugisaka Koyama as High Priest
*Ryuzaburo Mitsuoka as Soldier
*Koji Murata		
*Fumihiko Yokoyama

==Production==

 
After the success of his previous film The Life of Oharu, Mizoguchi was offered to make a film at Daiei Film studios by his old friend Masaichi Nagata. The deal promised Mizoguchi complete artistic control and a large budget. Despite this, Mizoguchi was eventually pressured to make a less pessimistic ending for the film.    The films title Ugetsu is a contraction of Ugetsu Monogatari, the Japanese title, from Akinaris Ugetsu Monogatari.  Mizoguchi based his film on two stories from the book, "The House in the Thicket" (Asaji ga Yado) and "The Lust of the White Serpent" (Jasei no In).     Other inspirations for the films script include Guy de Maupassants Déconé! (How He Got the Legion of Honor).   While writing the script, Mizoguchi told his screenwriter and long-time collaborator Yoshikata Yoda "Whether war originates in the rulers personal motives, or in some public concern, how violence, disguised as war, oppresses and torments the populace both physically and spiritually... I want to emphasize this as the main theme of the film."  During the shooting Yoda was constantly rewriting and revising scenes due to Mizoguchis perfectionism.   

 
Mizoguchi told his cinematographer Kazuo Miyagawa that he wanted the film "to unroll seamlessly like a scroll-painting."  The film has been praised for its cinematography, such as the opening shot and the scene where Genjurō and Lady Wakasa make love by a stream and the camera follows the flow of the water instead of lingering on the two lovers.  Miyagawa stated that this film was the only occasion in which Mizoguchi complimented him for his camera work.   

==Release==
Ugetsu was released in Japan on March 26, 1953. 
It was shown at the 1953 Venice Film Festival. Accompanied by Yoda and Kinuyo Tanaka,  Mizoguchi made his first trip outside of Japan to attend the festival. He spent most of his time in Italy inside his hotel room praying to a scroll with a portrait of Nichiren. While in Venice he met director William Wyler, whose film Roman Holiday was also screening in competition at the festival and was rumored be the winner of the Silver Lion for best director. 

===Home media===
In Japan, the film is in the public domain and is sold in low-price DVD box-sets. 

On November 8, 2005, Ugetsu became available for the first time on  , directed by Kaneto Shindo.  The boxset also includes a booklet with an essay by Keiko I. McDonald (author of Mizoguchi and editor of Ugetsu) and the three short stories from which the film draws inspiration.
 Region 2 DVD released by Eureka Entertainment as part of their Masters of Cinema series. The 2 x disc special edition containing new transfers is released in a double pack which twins it with Miss Oyu. This UK set was released on Blu-ray Disc|Blu-ray on April 23, 2012 in a Dual Format combo (the DVDs being the same discs as the 2008 release). However, due to the booklet size limitations, it does not include Keiko I. McDonalds essay, but only retains the translations of Ueda Akinari’s short stories.

==Reception==

===Critical reception===
Ugetsu is one of a number of films that is arguably more popular in western countries than it is in Japan. Japanese film historian Tadao Satō pointed out that while this film, along with Mizoguchis other works of the period The Crucified Lovers and Sansho the Bailiff, was probably not meant specifically to be sold to westerners as an "exotic" piece, it was perceived by studio executives as the kind of film that would not necessarily make a profit in Japanese theaters but would win awards at international film festivals.   

The film was immediately popular in western countries and praised by such film critics as Lindsay Anderson and Donald Richie. Richie called it "one of the most perfect movies in the history of Japanese cinema" and especially praised the beauty and morality of the films opening and closing shots. Richie stated that "Ugetsu opens with a long panorama around a lake, a shot which begins on the far shore and then tilts down to reveal the village at the conclusion. It closes with the child and the father offering a bowl of rice at the mothers grave...with the camera moving off into an upward tilting panorama which describes the movement of the opening." 

Bosley Crowther wrote that the film had "a strangely obscure, inferential, almost studiedly perplexing quality". * {{cite web
|url=http://movies.nytimes.com/movie/review?res=9E03E3D7163DE53ABC4053DFBF66838F649EDE
|last=Crowther|first=Bosley|authorlink=Bosley Crowther|date=September 8, 1954|
publisher=The New York Times|
title=The Screen in Review; Ugetsu, From Japan, Offered at Plaza|accessdate=1 January 2013}} 
 greatest of all films", writing that "At the end of Ugetsu, aware we have seen a fable, we also feel curiously as if we have witnessed true lives and fates."  Director Martin Scorsese has also listed it as one of his favourite films of all time.  Ugetsu has a 100% approval rating on Rotten Tomatoes. 

===Accolades===
Ugetsu won the Silver Lion Award for Best Direction at the Venice Film Festival in 1953.  The night before, Mizoguchi, believing that if the film did not win an award the shame would prevent him from returning to Japan, stayed in his hotel room and prayed.  In Japan it was named No. 3 in Kinema Junpos Best Ten for Japanese films of 1953.  and won awards for Art Direction and for Sound at the 8th Mainichi Film Awards.  It also won an award for Cinematography from the ministry of Education.  The film appeared in Sight and Sound magazines top ten critics poll of the greatest movies ever made, which is held once every decade, in 1962 and 1972.   In 2000, The Village Voice newspaper ranked Ugetsu at #29 on their list of the 100 best films of the 20th century. 

==See also==
* List of ghost films

==References==
 

===Bibliography===
*  
*  

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 