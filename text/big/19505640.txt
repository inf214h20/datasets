Flintesønnerne
{{Infobox film
| name           = Flintesønnerne
| image          = Flintesønnerne.jpg
| caption        = 
| director       = Alice OFredericks
| producer       = Henning Karmark
| writer         = Grete Frische Morten Korch
| starring       = Poul Reichhardt
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw ASA Film
| released       = 21 December 1956
| runtime        = 105 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Flintesønnerne is a 1956 Danish family film directed by Alice OFredericks.

==Cast==
* Poul Reichhardt - Jesper Poulsen
* Peter Malberg - Mikkel
* Ib Mossin - Viggo
* Ebbe Langberg - Martin
* Hanne Winther-Jørgensen - Karen
* Astrid Villaume - Anna
* Valdemar Skjerning - Kresten Flint
* Tove Maës - Else Flint
* Agnes Phister-Andresen - Faster Jane
* Helga Frier - Grete
* Ove Rud - Hans
* Keld Markuslund - Mads
* Einar Juhl - Sandbjergmanden
* Irene Hansen - Vera
* Per Lauesgaard - Karl
* Annelis Morten Hansen - Rikke
* Børge Møller Grimstrup - Gårdmand
* Mogens Juul - Jacob
* Inga Hansen

==External links==
* 

 

 
 
 
 
 
 


 