Idanilangal
{{Infobox film
| name           = Idanilangal
| image          =
| caption        =
| director       = IV Sasi
| producer       = K. Balachander
| writer         = MT Vasudevan Nair
| screenplay     = MT Vasudevan Nair Menaka Seema Seema
| music          = M. S. Viswanathan
| cinematography = NA Thara
| editing        = K Narayanan
| studio         = Kavithalayaa Productions
| distributor    = 
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Menaka and Seema in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
 
*Mammootty
*Mohanlal Menaka
*Seema Seema
*Sukumari Shubha
*Bhagyalakshmi
*Jagannatha Varma
*Janardanan
*Kundara Johny
*Kuthiravattam Pappu Meena
*Paravoor Bharathan
*Ranipadmini Vincent
*Vineeth
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by S Ramesan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Indrachaapathin njaanazhinju || K. J. Yesudas || S Ramesan Nair || 
|-
| 2 || Vayanaadan Manjalinu || P Susheela || S Ramesan Nair || 
|}

==References==
 

==External links==
*  

 

 
 
 

 