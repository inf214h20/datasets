Hush… Hush, Sweet Charlotte
 
{{Infobox film
| name = Hush... Hush, Sweet Charlotte
| image = Hush Hush Sweet Charlotte Poster.JPG
| caption = Promotional Poster for Hush... Hush, Sweet Charlotte
| director = Robert Aldrich
| producer = Robert Aldrich
| writer = Henry Farrell Lukas Heller
| starring = Bette Davis, Olivia de Havilland, Joseph Cotten, Agnes Moorehead, Cecil Kellaway, Mary Astor
| music = Frank De Vol
| cinematography = Joseph F. Biroc
| editing = Michael Luciano
| studio = The Associates and Aldrich
| distributor = 20th Century Fox
| released =  
| runtime = 133 min.
| country = United States English
| budget = $2,235,000 
| gross = $4,000,000 (US/Canada)  (rentals)   79,168 admissions (France)   at Box Office Story 
}}
 American thriller film directed and produced by Robert Aldrich, and starring Bette Davis, Olivia de Havilland, Joseph Cotten, and Agnes Moorehead, as well as Mary Astor in her final film. 

The movie was adapted for the screen by Henry Farrell and Lukas Heller, from Farrells unpublished short story, "What Ever Happened to Cousin Charlotte?" It received seven Academy Award nominations.

==Plot summary== antebellum mansion in Ascension Parish, Louisiana. However, it turns out John is married to another woman. Charlottes father detests the affair, and to end it he intimidates John by telling him his wife visited the day before and he told her about the affair with his daughter. To end the affair and return to his wife, John tells Charlotte he no longer loves her.

Sometime in the evening John is brutally murdered. In the mansions summerhouse, he is decapitated with a meat cleaver and his hand severed. Charlotte discovers Johns mutilated body and is traumatized. She walks to where the party is taking place, her white dress covered with Johns blood. People assume Charlotte is the murderer.

The story jumps to 1964. Charlotte is now an elderly, wealthy spinster, still living in the familys now decrepit plantation mansion. Also living in the mansion is Velma (Agnes Moorehead), Charlottes housekeeper. Charlottes father died the year following John Mayhews murder. He died believing his daughter murdered Mayhew. Charlotte believed her father killed Mayhew. The community assumed Charlotte murdered her lover, in a fit of rage after John called off their plans to elope and ended the relationship.

The Louisiana Highway Commission plans to demolish the mansion to build a new highway through the property.  Charlotte is vehemently against this. She ignores an eviction notice and refuses to vacate. She keeps the foreman (George Kennedy), his demolition crew, and the bulldozer away by shooting at them with a rifle. They temporarily give up and leave.

Seeking help in her fight against the Highway Commission, Charlotte calls on Miriam (Olivia de Havilland), a poor cousin, who in childhood lived at the mansion with the Hollis family. Separately, Miriam renews her relationship with Drew Bayliss (Joseph Cotten), a local doctor who, after the Mayhew murder, broke off his relationship with Miriam.

With Miriams arrival at the mansion, Charlottes sanity mysteriously starts to deteriorate. Charlottes nights are haunted by a harpsichord playing a melody Mayhew wrote for her, and by the appearance of Mayhews disembodied hand and head. Housekeeper Velma, suspecting that Miriam and Drew are after Charlottes money, seeks help from Mr. Willis (Cecil Kellaway), an insurance investigator who is still interested in the Mayhew murder, and who has visited Mayhews ailing widow, Jewel (Mary Astor).

Miriam fires Velma, who later returns and discovers that Charlotte is being drugged. Miriam catches Velma trying to help Charlotte escape the house. The two argue at the top of the stairs. Velma tries to leave but Miriam smashes a chair over her head causing Velma to tumble down the stairs to her death.

One night, a drugged Charlotte runs downstairs in the grip of a hallucination, believing John has returned to her. To drive Charlotte completely insane, Miriam and Drew trick Charlotte into shooting Drew with a gun loaded with blanks. Miriam helps Charlotte dispose of the supposedly dead Drew into a swamp. Charlotte returns to the mansion only to see the deceased Drew at the top of the stairs. This reduces Charlotte to whimpering madness.

Now believing Charlotte completely insane and having locked her in her bedroom, Miriam and Drew walk into the garden to discuss their plan: to drive Charlotte insane so they can steal her money.  Miriam also tells Drew that, back in 1927, she witnessed Mayhews wife Jewel murder her husband. Shes been using this information to blackmail Jewel for all these years, while also plotting to gain possession of Charlottes wealth.

Charlotte overhears the entire conversation between Miriam and Drew. Charlotte moves toward a huge stone urn on the ledge of the balcony, almost directly over the plotting lovers heads, and pushes it over towards Miriam and Drew.  As Miriam embraces Drew she notices the urn being tipped and coming down on her and Drew, and also discovers Charlotte glaring at them. The look in Charlottes eyes tells Miriam the entire conversation was overheard. As she stands stunned at the sight of Charlotte, the massive stone urn crushes the two dead.

The next morning the authorities drive Charlotte away, presumably to an insane asylum. Many neighbors and locals gather at the Hollis home to watch the spectacle, believing that crazy Charlotte has murdered again. In the car, insurance investigator Willis hands Charlotte an envelope from the now-dead Jewel Mayhew, who had a stroke after hearing of the previous nights incident. In the note, Jewel confesses to the murder of her husband, John Mayhew. As the authorities drive Charlotte away, she looks back at her beloved plantation, apparently for the last time.

==Principal cast==
* Bette Davis as Charlotte Hollis
* Olivia de Havilland as Miriam Deering
* Joseph Cotten as Dr. Drew Bayliss
* Agnes Moorehead as Velma Cruther
* Cecil Kellaway as Harry Willis
* Mary Astor as Jewel Mayhew
* Victor Buono as Big Sam Hollis
* Wesley Addy as the Sheriff William Campbell as Paul Merchand, reporter
* Bruce Dern as John Mayhew
* George Kennedy as the foreman

==Production== What Ever Happened to Baby Jane? (1962), director Robert Aldrich wanted to reunite stars Joan Crawford and Bette Davis.  After Crawford worked a week in Baton Rouge and only four days in Hollywood, she quit the film, claiming she was ill.   

Alain Silver and James Ursini wrote in their book Whatever Happened to Robert Aldrich?, "Reputedly, Crawford was still incensed by Daviss attitude on Baby Jane and did not want to be upstaged again, as Daviss nomination for Best Actress convinced her she had been. Because Crawford had told others that she was feigning illness to get out of the movie entirely, Aldrich was in an even worse position..."  Desperate to resolve the situation, "Aldrich hired a private detective to record her   movements." When shooting was suspended indefinitely, the production insurance company insisted that either Crawford be replaced or the production cancelled. 

Davis suggested her friend Olivia de Havilland to Aldrich as a replacement for Crawford after Katharine Hepburn, Vivien Leigh, Loretta Young, and Barbara Stanwyck declined the role. Leigh said, "I can just about stand to look at Joan Crawford at six in the morning on a southern plantation, but I couldnt possibly look at Bette Davis." Though the Davis-Crawford partnership was not repeated, Victor Buono from What Ever Happened to Baby Jane? reunited with Davis for Hush. The cast also included Mary Astor, a friend and former co-worker of Davis during her time at Warner Bros. 

Scenes outside the Hollis mansion were shot on location at Houmas House plantation in Louisiana.      The inside scenes were shot on a soundstage in Hollywood.

==Critical reception== sadistic and brutally sickening. So, instead of coming out funny, as did Whatever Happened to Baby Jane?, it comes out grisly, pretentious, disgusting and profoundly annoying." {{cite web|url=http://movies.nytimes.com/movie/review?res=9F03E3DF1730E33ABC4C53DFB566838E679EDE|title=Movie Review -
  Hush Hush Sweet Charlotte - New Movie at Capitol Echoes Baby Jane - NYTimes.com|work=nytimes.com|accessdate=25 February 2015}} 

Variety (magazine)|Variety says, "Davis portrayal is reminiscent of Jane in its emotional overtones, in her style of characterization of the near-crazed former Southern belle, aided by haggard makeup and outlandish attire. It is an outgoing performance, and she plays it to the limit. De Havilland, on the other hand, is far more restrained but none the less effective dramatically in her offbeat role." 

Time Out London says, "Over the top, of course, and not a lot to it, but its efficiently directed, beautifully shot, and contains enough scary sequences amid the brooding, tense atmosphere. Splendid performances from Davis and Moorehead, too." 
 guignol is about as grand as it gets."

Kenneth Tynan asserted that, "...(Davis) has done nothing better since The Little Foxes."

==Accolades==
 Academy Award nominations for the following: Best Supporting Actress (Agnes Moorehead) Best Art Direction (Black-and-White) (William Glasgow and Raphael Bretton for set decoration) Best Black-and-White Cinematography (Joseph Biroc) Best Costume Design Black-and-White (Norma Koch) Best Film Editing (Michael Luciano) Best Original Score (Frank De Vol) Best Song (Frank De Vol and Mack David).   
 Golden Globe Award for Best Supporting Actress. Farrell and Heller won a 1965 Edgar Award, from the Mystery Writers of America, for Best Motion Picture Screenplay. The song became a hit for Patti Page, who took it to #8 on the Billboard Hot 100|Billboard Hot 100. The films seven Oscar nominations were the most for a movie of the horror genre up to that time.

==DVD releases==
Hush... Hush, Sweet Charlotte was first released on DVD on August 9, 2005. It was re-released on April 8, 2008 as part of The Bette Davis Centenary Celebration Collection 5-DVD box-set. 

==See also==

* Psycho-biddy

==References==
 

==External links==
*   at Internet Movie Database
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 