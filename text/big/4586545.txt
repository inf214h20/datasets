Watermarks (film)
 
 
{{Infobox film
| name           = Watermarks
| image          = Watermarks (film).jpg
| caption        = 
| director       = Yaron Zilberman
| producer       = Yonatan Israel
| writer         = Ronen Dorfan Yaron Zilberman Yonatan Israel
| starring       = 
| music          = 
| cinematography = Tom Hurwitz
| editing        = Ruben Korenfeld Yuval Sher
| distributor    = Cinephil
| released       =  
| runtime        = 80 minutes German Hebrew Hebrew
| budget         = 
| gross          = $246,094 
}}
 directorial debut, Hakoah swim team during the rise of fascism in 1930s Austria.  The film describes the womens success as athletes leading up to the Anschluss of 1938 when the swimmers fled Austria to disparate locations in Israel|Palestine, England, and the United States.  The documentary ends with some of the women from the swim team returning to Vienna sixty-five years later for a reunion at their old swimming pool.

One of the women featured in the film, Judith Haspel, was a record-setting swimmer who was selected to represent Austria in the 1936 Summer Olympics in Berlin. She refused to go and was stripped of her records and banned from competition. Her records were reinstated in 1995.

==External links==
*  
*  
*  
*  
*  

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 