Mommaga
{{Infobox film
| name = Mommaga
| image = 
| caption =
| director = V. Ravichandran
| writer = R. Selvaraj Meena   Umashri   Prakash Rai
| producer = Sandesh Nagaraj
| music = Hamsalekha
| cinematography = G. S. V. Seetharam
| editing = Shyam
| studio = Sandesh Combines
| released = 1997
| runtime = 139 minutes
| language = Kannada
| country = India
| budget =
}} romantic drama drama film Meena and Umashri in the leading roles.  The film was a musical hit upon release with the songs composed and written by Hamsalekha being received well.

== Cast ==
* V. Ravichandran  Meena 
* Prakash Rai
* Umashri
* C. R. Simha
* Master Anand
* Ashalatha
* Ramesh Bhat
* Vijay Kashi
* Lakshman
* M. D. Kaushik
* Shanthamma

== Soundtrack ==
The music was composed and lyrics were written by Hamsalekha.  
A total of 10 tracks have been composed for the film.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Dolu Dolu Nanna
| extra1 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Kere Eri Myale
| extra2 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Hey Gumlakkadi
| extra3 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Maharaja Rajashri
| extra4 = S. P. Balasubrahmanyam
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Ammanigagi Karna Sotha
| extra5 = S. P. Balasubrahmanyam
| lyrics5 = Hamsalekha
| length5 = 
| title6 = Baaro Anna Baaro Thamma
| extra6 = S. P. Balasubrahmanyam
| lyrics6 = Hamsalekha
| length6 = 
| title7 = Sri Ranganu Kanmuchi
| extra7 = Rajesh Krishnan, K. S. Chithra
| lyrics7 = Hamsalekha
| length7 = 
| title8 = Oo Hoogale
| extra8 = Rajesh Krishnan, K. S. Chithra
| lyrics8 = Hamsalekha
| length8 = 
| title9 = Akka Thangi Ibbaru
| extra9 = K. S. Chithra
| lyrics9 = Hamsalekha
| length9 = 
| title10 = Huttutha Ondu Kombu
| extra10 = L. N. Shastry
| lyrics10 = Hamsalekha
| length10 = 
|}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 

 