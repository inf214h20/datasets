Bugged by a Bee
 
{{Infobox Hollywood cartoon|
| cartoon_name = Bugged by a Bee Cool Cat)
| image =
| caption =
| director = Robert McKimson
| story_artist = Cal Howard Jim Davis Ed Solomon
| voice_actor = Larry Storch
| musician = William Lava
| producer = William L. Hendricks Jaime Diaz
| background_artist = Bob Abrams
| distributor = Warner Bros.-Seven Arts
| release_date = July 26, 1969
| color_process = Technicolor
| runtime = 6:21 min English
}}
 1969 animated Cool Cat, Injun Trouble would follow Bugged by a Bee, but it was in the Merrie Melodies series and is much harder to find.

This cartoon was the last Looney Tunes cartoon until 1988s The Night of the Living Duck.

==Synopsis== Disco Tech, and sings an opening song about how hes "workin through college, to gain a lotta knowledge." During this song a bee disturbs him, and he swats the bee to the ground with his guitar. As the angry bee sharpens its stinger, Cool Cat checks out the colleges sports programs, and decides to try out pole vaulting to impress the female students. His first attempt goes horribly wrong when his pole gets stuck in a chipmunks hole, and when he goes to try again the bee stings him just as he begins his run-up. The pain of the sting gives Cool Cat enough power in his run-up to set a record-breaking vault over the pole, and the colleges baseball coach is impressed enough to let him try out for the team.
 rowing and football match which has somehow ended up 0-0 right until the final few minutes, Cool Cat gets stung just as hes handling the ball, causing him to swallow the ball and dart around the stadium, while the other players ask each other who has got the ball. Cool Cat eventually flops down on the touchline, and gets stung again, which causes him to spit the ball out into his hands and scores the winning touchdown.

Afterwards, the college holds a ceremony in honor of Cool Cats achievements, which have propelled Disco Tech to the top of all the sports leagues. To Cool Cats chagrin however, the person being awarded at the ceremony is actually the bee, not himself.

==External links==
*  

 
{{succession box |
before=3 Ring Wing Ding|
title= Cool Cat shorts |
years= 1969 | Injun Trouble}}
 

 
 
 
 


 