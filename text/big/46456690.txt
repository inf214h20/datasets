The Racokzi March
{{Infobox film
| name = The Racokzi March 
| image = 
| image_size =
| caption =
| director = Gustav Fröhlich   Steve Sekely
| producer = 
| writer = Ferenc Herczeg  (play)   Ernst Marischka   Franz Vayda   Andor Zsoldos
| narrator =
| starring = Gustav Fröhlich   Leopold Kramer   Camilla Horn
| music = Paul Abraham  
| cinematography = István Eiben   Willy Goldberger
| editing = 
| studio =  Hunnia Filmgyár   Mondial-Film   Märkische Film 
| distributor = 
| released =  15 December 1933
| runtime = 
| country = Austria   Czechoslovakia   Germany German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} The Racokzi March was made.

==Cast==
*    Gustav Fröhlich as Oberleutnant Tarjan  
* Leopold Kramer as Graf Job 
* Camilla Horn as Vilma, seine Tochter   Paul Wagner as Rittmeister Arpad Graf Job, sein Sohn   Ellen Frank as Erika, seine Nichte  
* Tibor Halmay as Leutnant Lorant 
* Margit Angerer as Eine Konzertsängerin  
* László Dezsõffy as Der Wachtmeister  
* Anton Pointner as Merlin, Jobs Gutsnachtbar  
* Charles Puffy as Der Tierarzt  
* Willi Schur as Mischka, Tarjans Offizierdiener  
* Rudolf Teubler as Der Altbauer  
* Otto Treßler as Der Regimentsarzt   Peter Wolff as Fähnrich Bilitzky

== References ==
 

== Bibliography ==
* Robert Dassanowsky. Austrian Cinema: A History. McFarland, 2005.

== External links ==
*  

 
 
 
 
 
 
 
 

 