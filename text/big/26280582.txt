On Board (film)
{{Infobox Film
| name           = On Board
| image          = 
| image size     = 
| caption        = 
| director       = Serdar Akar
| producer       = Sevil Demirci Önder Çakar
| writer         = Serdar Akar Önder Çakar
| narrator       = 
| starring       = Erkan Can Haldun Boysan Yıldıray Şahinler Naci Taşdöğen
| music          = Uğur Yücel
| cinematography = Mehmet Aksın
| editing        = 
| distributor    = TurkishFilmChannel
| released       =  
| runtime        = 112 mins.
| country        = Turkey
| language       = Turkish
| budget         = 
}}
On Board ( ) is a 1998 Turkish drama film, written and directed by Serdar Akar, about four sailors who kidnap a prostitute. The film, which went on nationwide general release across Turkey on  , won awards at film festivals in Ankara and Antalya, including the Golden Orange for Second Best Film. It was shot concurrently with A Madonna in Laleli ( ), directed by Kudret Sabancı, and many of the main characters from the two films cross paths.   

==Synopsis==
A dredging ship is anchored in Istanbul for food and supplies. But the sailor sent to get them is mugged by six people and one weeks worth of wages are stolen from him. The captain leads his men into the city to find the people who did this. Upon meeting the men, they manage to get back the money and take back the woman in the group back to the ship.

==Cast==
*Erkan Can as Captain İdris
*Haldun Boysan as Kamil
*Yıldıray Şahinler as Ali
*Naci Taşdöğen as Boksör
*Ella Manea as the woman
*Cengiz Küçükayvaz as Doktor
*İştar Gökseven as Makor
*Güven Kıraç as Aziz
*Funda Şirinkal
*Bülent Çakırer

=== Reception ===

== Reviews ==
Rekin Teksoy wrote that, The film showcased the director’s tendency to breakway from familiar cinematographic idioms, and, The lives of four seaman were colourfully brought to the screen in all their vulgarity and violence, yet failed to be believable. 

==Awards==
The film won the following awards:    

*International Critics Week (Semaine de la Critique) Official Selection Cannes Film Festival 1999
*35th Antalya Golden Orange Film Festival: Best Director (Serdar Akar) Second Best Film, Best Screenplay (Nevzat Dişiaçık), Best Actor (Erkan Can),
*11th Ankara Film Festival:  Jury Special Prize (Serdar Akar), Best Actor (Erkan Can), Best Supporting Actor (Haldun Boysan), Most Promising New Director (Serdar Akar), Most Promising New Screenwriter (Serdar Akar)
*10th Orhan Arıburnu Awards: Best Film, Best Director (Serdar Akar), Best Actor (Erkan Can)

==References==
 

==External links==
*  
*   page for the film

 
 
 
 
 
 