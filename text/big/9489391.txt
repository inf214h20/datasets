The Prowler (1981 film)
 
{{Infobox film
| name           = The Prowler
| image          = The Prowler.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Joseph Zito
| producer       = Joseph Zito   David Streit
| writer         = 
| screenplay     = Neal Barbera   Glenn Leopold
| story          = 
| based on       = 
| narrator       = 
| starring       = Vicky Dawson   Farley Granger   Lawrence Tierney   Christopher Goutman 
| music          = Richard Einhorn
| cinematography = João Fernandes
| editing        = Joel Goodman
| studio         = Graduation
| distributor    = Sandhurst
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $1,000,000   
| gross          = 
}}

The Prowler (also known as Rosemarys Killer) is a 1981 horror film directed by Joseph Zito, and written by Neal Barbera and Glenn Leopold.

== Plot ==
During the Second World War a woman named Rosemary Chatham breaks up with her boyfriend who is serving in the army, via Dear John letter|letter.

On June 28, 1945, the town of Avalon Bay holds its graduation dance. Rosemary (Joy Glaccum) and her new boyfriend Roy (Timothy Wahrer) leave the party and are murdered by a mysterious prowler in combat uniform who impales them on a pitchfork.

Thirty five years later Pam MacDonald (Vicky Dawson) revives the graduation dance. She visits Sheriff George Fraser (Farley Granger), mentioning Major Chatham (Lawrence Tierney) and his refusal to allow the annual celebrations after the murders. The Sheriff tells Pam that someone robbed a nearby store, murdered someone and stole their car. Despite the possibility of the killer heading to Avalon Bay, the sheriff leaves to go fishing, putting deputy Mark London (Christopher Goutman), Pams boyfriend, in charge.

That night Pam goes to the dance, leaving her roommate Sherry (Lisa Dunsheath) in the shower. Sherrys boyfriend Carl (David Sederholm) arrives to collect her but the killer stabs him to death with a bayonet, then kills Sherry in the shower with a pitchfork. Mark arrives at the dance but Pam is annoyed when Lisa (Cindy Weintraub) pulls him away to dance then causes punch to spill over Pams dress. Pam goes home and changes, not realizing two murders have occurred in her dorm. On leaving she is pursued by the killer but evades him, catching up with Mark. They search Major Chathams house, finding pictures and a scrapbook of Rosemarys. They return to the dance and inform chaperon Miss Allison (Donna Davis) of the prowler; she asks all party goers to remain in the hall until he is apprehended. However, Lisa has already left to go for a swim in the pool. The killer stalks Lisa and kills her, then kills Allison who had gone to retrieve Lisa.

Mark and Pam are informed that the cemetery gates are open so investigate and find Rosemarys grave open, with Lisas body in the coffin. Mark tries unsuccessfully to reach Sheriff Fraser; he and Pam return to Major Chathams house where Mark is knocked out and Pam pursued by the killer. A villager, Otto, arrives and shoots the killer but the killer is only injured and revives to shoot Otto dead. Pam stabs the killer with the pitchfork and they struggle for the shotgun. The killer is revealed to be Sheriff Fraser. Pam gets the gun and shoots him in the head, killing him.

The next morning Mark returns Pam to the dorm where she discovers the bodies of Sherry and Carl. She is horrified as Carls corpse grabs her in an imagined moment of shock.

== Cast ==
* Vicky Dawson as Pam MacDonald
* Christopher Goutman as Deputy Mark London
* Lawrence Tierney as Major Chatham
* Farley Granger as Sheriff George Fraser
* Cindy Weintraub as Lisa
* Lisa Dunsheath as Sherry
* David Sederholm as Carl
* Bill Nunnery as Hotel Clerk
* Thom Bray as Ben
* Diane Rode as Sally
* Bryan Englund as Paul
* Donna Davis as Miss Allison
* Carleton Carpenter as 1945 M.C
* Joy Glaccum as Francis Rosemary Chatham
* Timothy Wahrer as Roy
* John Seitz as Pat Kingsley
* Bill Hugh Collins as Otto
* Dan Lounsbery as Jimmy Turner
* Douglas Stevenson as Young Pat Kingsley
* Susan Monts as Young Pat Kingsleys Date
==Production==
 
The film was shot in Cape May, New Jersey. 
The cemetery scenes of the film were shot in an actual cemetery on the night of Halloween 1980, as well as the open grave that was awaiting a funeral. According to the audio commentary, AVCO Embassy pictures offered $750,000 for distribution rights of the film (the total budget was $1,000,000). Director Joseph Zito sated that the producer declined the offer and decided to self-distribute the film himself which hurt the films box-office. The films entire shooting schedule was built around the filming and effects.

== Release ==
 
The Prowler was released as Rosemarys Killer in Australia and Europe, and is missing almost a minute of Tom Savinis gore effects. 

The German version omits all of the gore scenes (including the revelation of the killers identity) and replaced the soundtrack with bird sounds for daytime scenes, cricket sounds for the night scenes, and Richard Enhorns score with synthesizer music by an uncredited musician. This version goes by the title Die Forke des Todes (The Pitchfork of Death).

The Encyclopedia of Horror reports that "Savinis particularly graphic special effects resulted in most of the murders being trimmed in the British release print." 

=== DVD release ===
Blue Underground released an uncut version of The Prowler on DVD in 2002 and on Blu-Ray in 2010. The extras include a trailer, a still and poster gallery, behind the scenes gore footage with Tom Savini, and an audio commentary with Joseph Zito and Tom Savini.

== Reception ==
 
AllRovi called it a "run-of-the-mill entry in the early 80s slasher film cycle" that "benefits from an unexpected amount of technical gloss, but has little else to offer". 
 My Bloody Valentine the film moves away from the genres usual Midwestern setting, but that it does little with the new location, nor with its potentially interesting returning G.I. motif. Like Valentine, the film is judged as being certainly polished, atmospheric, and suspenseful, though hardly original.  Milne, Tom. Willemin, Paul. Hardy, Phil. (Ed.) Encyclopedia of Horror, Octopus Books, 1986. ISBN 0-7064-2771-8 p 370 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 