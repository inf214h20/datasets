Just a Girl (film)
{{Infobox film
| name           = Just a Girl
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Butler
| producer       = 
| writer         = Charles Garvice (novel)   Harry Engholm
| narrator       = 
| starring       = Owen Nares Daisy Burrell
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    =  
| released       = 1916 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} silent motion picture of 1916 directed by Alexander Butler and starring Owen Nares, Daisy Burrell and Paul England. A romance film|romance, it was adapted by Harry Engholm from Charles Garvices novel of the same title published in 1895.

==Plot== English peer in need of money. However, she refuses him and marries the man she loves, Norman Druce, a humble miner. Robert Connelly, Jay Robert Nash, Stanley Ralph Ross, Motion Picture Guide Silent Film 1910-1936 (1988), p. 132: "JUST A GIRL** (1916, Brit.) 7 reels Samuelson/Moss bw Owen Nares (Lord Trafford), Daisy Burrell (Esmeralda), J. Hastings Batson (The Duke), Minna Grey (The Duchess), Paul England (The Miner). In another of those British social-class soap operas, an Australian heiress rejects an impoverished lord to marry the miner she really loves. d, Alexander Butler; w, Harry Engholm (based on the novel by Charles Garvice)." 

==Cast==
* Owen Nares – Lord Trafford
* Daisy Burrell – Esmeralda 
* J. Hastings Batson – Duke
* Minna Grey – Duchess 
* Paul England – Norman Druce (the miner)

==Overseas==
The film was distributed in Sweden under the title Australiens vilda ros (Australias Wild Rose) and subtitled Esmeralda, lägrets stolthet (Esmeralda, Pride of the Camp). The Swedish premiere was at the Odeon, Stockholm, on 3 October 1917. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 

 