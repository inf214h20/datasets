Prison Town, USA
{{Infobox film
 | name = Prison Town, USA
 | image = Prison Town, USA VideoCover.jpeg
 | caption = 
 | director = Katie Galloway, Po Kutchins
 | producer = Katie Galloway, Po Kutchins
 | writer = Katie Galloway, Po Kutchins
 | starring = 
 | music = JJ McGeehan
 | cinematography = Evan Eames
 | editing = Beth Segal
 | distributor = Docurama
 | released =  
 | runtime = 87 minutes
 | country = United States
 | language = English
 | budget = 
}}
Prison Town, USA is a 2007 documentary film about Susanville, California, a small, rural town that tries to resuscitate its economy by building a prison. The film follows four men, their families, and how they react to the new institution: Lonnie Tyler, a prisoner, Dawayne Brasher and Gabe Jones, guards, and Mike OKelly, a dairyman threatened by how the prison could affect the communitys buy-local policy. 

Prison Town, USA was written, produced, and directed by Katie Galloway and Po Kutchins and was aired as part of the P.O.V. series on Public Broadcasting Service|PBS.

==References==
 

== External links ==
*  
*  

 
 
 

 