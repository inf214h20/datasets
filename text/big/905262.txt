The Basque Ball: Skin Against Stone
{{Infobox film
| name           = The Basque Ball: Skin Against Stone
| image =  La pelota vasca poster.jpg
| image_size     = 
| caption        = Theatrical Poster
| director       = Julio Médem
| producer       = Julio Médem Koldo Zuazua
| writer         = Julio Médem
| narrator       = 
| starring       = 
| music          = 
| cinematography = Javier Aguirre Ricardo de Gracia Daniel Sosa Segura
| editing        = Julio Medem
| distributor    = Golem Distribución
| released       =  
| runtime        = 110 minutes
| country        = Spain Basque English English French French Spanish Spanish
| budget         = 
}} Spanish filmmaker Julio Médem.

==Overview== Basque Country. Basque problem&mdash;it is obviously a film designed to be viewed by Spanish audiences, or people familiar with the issues.

The movie also utilizes footage from the Basque portions of the 1955 travelogue Around The World With Orson Welles, and continually intercuts between interviews and jai alai players.

==Criticism== Partido Popular and ETA refused to take part in the interviews. The former went so far as to request the organisers of the Donostia-San Sebastian International Film Festival to reconsider the films suitability. This, in turn, has led some to call it an incomplete documentary. 

It has also been openly criticized by both extremes, and Medem, who is Basque, has been accused of being both pro-ETA and pro-"Spanish occupation". Indeed, two of the interviewees, Iñaki Ezquerra and Gotzone Mora (both members of the intellectual group the Ermua Forum) demanded that Medem retract their interviews, accusing him of presenting the Spanish Guardia Civil and police forces as torturers and ETA and their followers as victims.  Despite these protests, their interviews remained due to the films imminent release date. They did not, however, appear on the 7-hour DVD Edition.

There is a three-disc special edition DVD (ISBN 0-499-01513-4) released with seven hours of edited footage that goes deeper into the history of the Basque Country and a Spanish-language book (ISBN 84-03-09425-6).

==Awards and nominations==
* 2004: Nominated, Goya Awards, Best Documentary
* 2004: Nominated, European Film Awards, Best Documentary
* 2004: Nominated, Cartagena Film Festival, Best Film

==Notes==
 

==External links==
*  .
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 