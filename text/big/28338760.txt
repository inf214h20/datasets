My Last Day Without You
 
My Last Day Without You is an independent feature film starring Nicole Beharie and Ken Duken, and directed by Stefan Schaefer. It was written by Schaefer and Christoph Silber, and produced by Diane Crespo, Silber and Schaefer and their companies Cicala Filmworks and Silver Shepherd. 

Festival screenings include, among others: Brooklyn International Film Festival, Oldenburg International Film Festival, Heartland Film Festival, Hawaii International Film Festival, Lone Star Film Festival, Atlanta Film Festival, Sedona International Film Festival, and Langston Hughes African American Film Festival.

The film opened in the US on Oct. 4th, 2013 in AMC Theaters, and had its cable premiere on April 21, 2014 on Up TV. It released in theaters, on TV and via VOD in many international territories, handled by Mission Pictures International. DVD and VOD release in the US was Dec. 1, 2014. 

This film was co-produced by Klaus Popa and Matthias Muller, and was licensed by Stimme der Hoffnung e.V. It screened in German-speaking territories on HOPE Channel TV and in a number of cinemas in Germany and other countries. 

== Plot ==
On a one-day business trip to New York, a German business executive (Ken Duken) falls in love with a singer-songwriter (Nicole Beharie) who exposes him to her Brooklyn world and emotions he has never experienced before.

== Cast ==
The cast includes Ken Duken, Nicole Beharie, Marlene Forte, Laith Nakli, Robert Clohessy, and Reg E. Cathey.

== Awards and nominations ==
* 2011 Brooklyn Film Festival - "Excellence in Producing" / "Best Producers" Award 
* 2011 Brooklyn Film Festival - Nominated, Best Feature Film 
* 2012 Black Reel Awards - Best Independent Feature Film 
* 2012 Black Reel Awards - Nominated, Best Original Song

== Soundtrack ==
Lead actress Nicole Beharie performs five original songs in the film. The songs are written by Scott Jacoby (producer), Stefan Schaefer, and Christoph Silber. The soundtrack also features three Grammy nominated artists. It was released for digital download and streaming via Google Play, Amazon.com, iTunes, and Spotify on Nov. 5th 2013.

=== Track listing ===

{{Track listing
| extra_column    = Artist(s)
| writing_credits = no
| title1          = Two Hands 
| extra1          = Nicole Beharie 
| length1         = 3:23
| title2          = Sunshine
| extra2          = Autumn Rowe  
| length2         = 4:42
| title3          = Catch Me
| extra3          = Nicole Beharie 
| length3         = 3:05
| title4          = Cuidad Mambo
| extra4          = Tom Barber 
| length4         = 1:22
| title5          = Morning Light
| extra5          = Nicole Beharie
| length5         = 4:22
| title6          = High Time
| extra6          = Maiysha
| length6         = 4:13
| title7          = Scars
| extra7          = Nicole Beharie  featuring Kokayi
| length7         = 3:22
| title8          = Black Madoff
| extra8          =  
| length8         = 3:45
| title9          = Abc Blues
| extra9          = Billy White
| length9         = 4:25
| title10         = Dumela
| extra10         = Carolyn Malachi
| length10        = 4:54
| title11         = My Last Day Without You
| extra11         = Nicole Beharie 
| length11        = 4:29
}}

== External links ==
*  
*  
*  
*  

 
 
 
 
 