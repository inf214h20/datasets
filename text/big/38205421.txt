Mademoiselle Parley Voo
{{Infobox film
| name           = Mademoiselle Parley Voo
| image          =
| caption        =
| director       = Maurice Elvey 
| producer       = Gareth Gundrey Jack Harris John Stuart   Alf Goddard   John Longden
| music          =
| cinematography =  
| editing        = 
| studio         = Gaumont British Picture Corporation
| distributor    = Gaumont British Picture Corporation
| released       = June 1928
| runtime        = 7,300 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama John Stuart Mademoiselle from Armentieres (1926), and was equally successful.  Both films refer to the popular First World War song Mademoiselle from Armentières. It was made at Lime Grove Studios in Shepherds Bush.

==Cast==
* Estelle Brody as Mademoiselle  John Stuart as John 
* Alf Goddard as Fred 
* John Longden as Le Beau 
* John Ashby as Their Son
* Humberstone Wright as The Old Soldier
* Wallace Bosco as Bollinger

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 

 