Morbid: A Love Story
 
 
 
}}

{{Infobox film
| name = Morbid: A Love Story
| image = Morbid A Love Story.jpg
| caption = US theatrical poster for Morbid
| director = Edward Payson
| producer = Edward Payson Reuben Rasheed Peter Greeley
| writer = Edward Payson
| starring = Liam J. Smith Chris Vanderhorst Anna Palestis
| music = A.J. Hicks
| cinematography = Brian Baker
| editing = Dan Haff
| distributor = Anti-Hero Productions
| released =  
| runtime = 90 minutes
| language = English
| country = United States
}}
Morbid: A Love Story is a 2008 horror film, written, directed, and produced by Edward Payson, starring Liam J. Smith & Anna Palestis.

Smith plays the disturbed serial killer, Christian.  Palestis plays his love interest, Belle. Dylan Voorhees plays a young Christian.

Although claiming to be "pretty gruesome", as there is plenty of blood and graphic violence, the film is different from a run-of-the mill slasher film in that the serial killer is the protagonist.  Morbid: A Love Story is due to be released in September 2009 with its premiere at the New Beverly Cinema. (https://an-anti-hero-production.ticketleap.com)

==Plot==
Christian Thomas is a typical college student, but is also a serial killer. He sees death as the greatest art form, ranking his targets by degree of difficulty and making his victims his easel. Christian takes a liking to Belle, a girl in his class, and falls in love. He decides he wants to lead a normal life and stop his killing ways, but old habits die hard.

== Cast and characters ==
*Liam J. Smith as Christian Thomas, a serial killer college student who falls in love.
*Anna Palestis as Belle, Christians love interest.

Additional cast members include:

*Dylan Voorhees as young Christian Thomas
*Scott Naurath as Bob
*Amber Rose as Laura
*David B. McCowan as Dr. Green
*Reuben Rasheed as Tyson Burns
*Barbara Griswold as Kristen Stevenson
*Sheila Pancani as  F.B.I. Agent Jennifer Martinez
*Alpha Takahashi as Mrs. Kill
*Dontreal Bacon as Ben
*Marcelo Olivas as Derek
*Andrew Cory Aikman as Bens friend
*Cody Cowell as Mr. Wallace Latin lover
*Christina Desiere as Christians mother
*Adam Linder as Harold
*Amanda Chism as Janine Weintrob
*David Finn as Jared Thomas
*Gerard Gilgeours as Jim
*Clinton Emmanuel as Garrett
*Elisa Catherine Taylor as Susan
*Justin Zagri as Mike
*Dontreal Bacon as Ben

==References==
 

== External links ==
*  
*  
* AITH  
* MEH  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 