The Sisterhood of the Traveling Pants (film)
{{Infobox film 
| name = The Sisterhood of the Traveling Pants
| image = Sisterhood_of_the_traveling_pants.jpg
| caption = Theatrical release poster
| director = Ken Kwapis
| music = Cliff Eidelman John Bailey
| producer = Debra Martin Chase Denise Di Novi Andrew Kosove Broderick Johnson
| based on = 
| writer = Screenplay: Delia Ephron Elizabeth Chandler
| starring = Amber Tamblyn Blake Lively Alexis Bledel America Ferrera Bradley Whitford 12th Street Di Novi Pictures
| distributor = Warner Bros.
| released =  
| runtime = 119 minutes
| country = United States English
| budget = $25 million 
| gross = $42,013,878   
}}
 The Sisterhood of the Traveling Pants by Ann Brashares released by Warner Bros. Pictures. It was directed by Ken Kwapis and written by Delia Ephron.
 Ashcroft area in British Columbia, Canada. 

==Plot==
Four teenage girls — Carmen, Tibby, Bridget, and Lena — are best friends from Bethesda, Maryland, who are about to separate for the summer for the first time in their lives. Lena is spending the summer in Greece with her grandparents; Tibby is staying at home; Bridget is going to soccer camp in Mexico; and Carmen is visiting her father in South Carolina. On one of their final days they went shopping together, the girls find a seemingly ordinary pair of jeans that fit them all perfectly and flatter their figures, despite their very different measurements. The girls dub them the Traveling Pants and decide to share them equally over the course of the summer. They part the next day, and the film focuses on each girls journey separately.

===Carmen Lowell=== Puerto Rican mother. During her time there, her father and her new family neglect her emotionally, driving her to throw a stone through their dining room window, and catch a bus back to Maryland. At home she tells Tibby about her time with her dad and Tibby convinces her to confront her father with a phone call and finally tell him that shes mad at him. Carmen tells her father and he apologizes. Her summer ends with the four of them returning south where she is an attendant at her fathers wedding, where at the reception he makes a public apology for having snubbed her.
===Lena Kaligaris===
Through the unexpected intervention of the Pants, Lena meets a boy named Kostas Dounas, a Greek-American like her. Lena learns from her grandparents that her family and Kostas family are sworn enemies stemming from an old family feud. Despite this, Kostas continues to pursue Lena, and the two develop feelings for each other. Lena holds back, though, until one day when she admits to herself that she is afraid of love. Once she makes this realization, she begins a secret relationship with Kostas. On their last night together, Kostas tells Lena that he loves her. Before Lena can answer, Lenas family barges in, angrily pulling her away. Lena later confronts her grandfather and asks to go see Kostas before he leaves, to which her grandfather agrees. Kostas and Lena share a passionate kiss, and Lena confesses her love for him.

===Tibby Rollins===
While on the job at a discount department store, Tibby hears a loud crashing sound, and finds a young girl who has fainted in the deodorant aisle. She frantically calls for help, and the girl is taken away in an ambulance. When Lena mails the magical Pants to Tibby, they are delivered to the wrong house. Bailey Graffman, the girl who had fainted at the store, has them.

Fascinated by Tibbys movie, or "suckumentary" as she calls it, Bailey becomes Tibbys self-appointed assistant. Tibby is annoyed by this at first, but gradually grows to accept Bailey. She later learns from Baileys neighbor that Bailey has leukemia.

Bailey eventually goes to the hospital with a bad infection. Tibby avoids the hospital for a while, but eventually visits Bailey, bringing the Traveling Pants. She offers them to Bailey and pleads with her to take them so that they can help her. Bailey responds by saying that the pants have already worked their magic on Bailey by bringing her and Tibby together. Tibby spends a lot of time with Bailey in the hospital after that. A couple of days later, Tibby receives a phone call in the morning from Mrs. Graffman, Baileys mother, saying that Bailey died in the night.

When Carmen comes back home from South Carolina, Tibby visits her to try and help her with her feelings of being snubbed by her father. Carmen lashes out at Tibby by saying that she has no feelings, and Tibby leaves in tears. The pair reconcile, and later go to Bridgets house in order to bring her out of her depression.

Over the course of the movie, Tibby undergoes dramatic changes in her outlook due to her time with Bailey.

===Bridget Vreeland===
Shortly after arriving at soccer camp in Baja California, Mexico, Bridget develops a crush on one of the coaches, Eric Richman. She reveals to Eric and the audience that a psychiatrist who evaluated her following her mothers suicide described her as "single-minded to the point of recklessness," presumably as a way of avoiding dealing with her mothers death. This statement aptly describes Bridgets pursuit of Eric, despite the fact that flings between coaches and campers are forbidden. She flirts with Eric and shows off for him during games.
 depression and suicide. Carmen and Tibby comfort Bridget by reassuring her that she is stronger than her mother. Eric visits Bridget and apologizes for his behavior over the summer, and tells her that while she is too young for him now, he hopes she will give him a shot when she is older, giving Bridget much-needed closure.

==Cast==
   Tabitha "Tibby" Tomko-Rollins
*America Ferrera as Carmen Lowell Lena Kaligaris Bridget Vreeland
*Bradley Whitford as Carmens father, Albert "Al" Lowell  
*Jenna Boyd as Bailey Graffman
*Kyle Schmid as Paul Rodman
 
*Mike Vogel as Eric Richman
*Michael Rady as Kostas Dounas
*Kristie Marsden as Soccer Pal Olivia
*Emily Tennant as Krista Rodman
*Leonardo Nam as Brian McBrian

 

==Production==
Principal photography started on the island of Santorini, Greece. Filming then continued in Cabo San Lucas, Mexico. 

== Reception ==
===Critical response===
  
According to review aggregator Rotten Tomatoes, "This adaptation of a beloved novel charms with its heartwarming tale of friendship and young adulthood; realistic portrayals of the lives of teenage girls lend the comedy-drama sincerity, and may capture hearts outside the female-centric demographic." It has a score of 77% based on 125 reviews.   
Metacritic gave the film a score of 66%, with "generally favorable reviews", based on 34 reviews.   
Yahoo! Movies gave the film a B rating based on 13 different critics. 

=== Box office ===
On its opening weekend, the film opened #5 at the box office with $9,833,340.  As of November 14, 2008, the film has grossed $42,013,878 worldwide.   

==Home media==
The DVD was released in the US on October 11, 2005.  It has yet to be released on Blu-ray Disc|Blu-ray.

==Soundtrack and score==

The song album was released by Columbia Records on May 24, 2005.

# These Days - Chantal Kreviazuk (3:57)
# Unwritten - Natasha Bedingfield (4:19)
# Time Of Our Lives - Paul van Dyk (3:37)
# Black Roses Red - Alana Grace (4:12)
# If God Made You (Radio Remix) - Five For Fighting (4:16)
# Just For You - William Tell (3:46)
# Closer To You - Brandi Carlile (2:54)
# No Sleep Tonight - The Faders (3:00)
# I Want You To Know - Chantal Kreviazuk (3:19)
# Be Be Your Love - Rachael Yamagata (4:14)
# Suns Gonna Rise - Shannon Curfman (3:55)
# Simple - Katy Perry (3:39)
# Always There In You - Valli Girls (an early track involving members of Haim (band)|HAIM)  (3:46)

The album of Cliff Eidelmans score was released by Varèse Sarabande on July 12, 2005.

# Prologue (3:44)
# Deja Blue (1:04)
# Fate (1:01)
# Rules of the Pants (3:26)
# A Touch of Greece (1:18)
# Honey (1:10)
# The Traveling Pants (:53)
# Reflection (2:07)
# Running (1:26)
# Traveling to Baja (:39)
# The Way of the Pants (:34)
# Letter (1:48)
# Broken Heart (1:16)
# A Brave Soul (1:15)
# Last Words (:58)
# Us (2:18)
# Sisterhood Reunites (1:14)
# Together (1:29)
# The Traveling Song (3:17)
# Piano Suite (4:03)

==Awards== Imagen Foundation Awards for Best Actress (America Ferrera) ALMA Award for Outstanding Actress in a Motion Picture (America Ferrera) Young Artist Award for Best Family Feature Film - Drama
*2006 -  Nominated; Young Artist Award for Best Performance in a Feature Film - Supporting Young Actress (Jenna Boyd) Satellite Award for Outstanding Actress in a Supporting Role, Comedy or Musical (America Ferrera)
*2005 -  Nominated; Satellite Award for Outstanding Youth DVD (Widescreen Edition)
*2005 -  Nominated;   (Alexis Bledel)
*2005 -  Nominated; Teen Choice Award for Choice Movie Actress: Drama (Amber Tamblyn)
*2005 - Nominated; Teen Choice Award for Choice Movie Breakout Performance - Female (America Ferrera)
*2005 - Nominated; Teen Choice Award for Choice Movie Breakout Performance - Female (Jenna Boyd)
*2005 - Nominated; Teen Choice Award for Choice Movie Breakout Performance - Female (Blake Lively)
*2005 - Nominated; Teen Choice Award for Choice Movie Breakout Performance - Male (Michael Rady)
*2005 - Nominated; Teen Choice Award for Choice Movie Hissy Fit (America Ferrera)
*2005 - Nominated; Teen Choice Award for Choice Movie Love Scene (Alexis Bledel and Michael Rady)
*2005 - Nominated;  

==Sequel==
All four actresses signed on for the sequel  and filming started June 2007. The movie, titled The Sisterhood of the Traveling Pants 2, was released on August 6, 2008. The movie is said to be based on the second, third and fourth books in the Traveling Pants series, while the time is apparently of Girls in Pants, i.e. after high school graduation before the girls leave for college. It is rated PG-13 for mature material and sensuality, unlike the first film which was PG.

==References==
;Notes
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 