Murderers Among Us
{{Infobox Film
| name           = The Murderers Are Among Us (UK) Murderers Among Us (US)  Die Mörder sind unter uns (Germany)
| image          = Murderers Among Us poster.jpg
| image_size     = 200px
| caption        = British movie poster
| director       = Wolfgang Staudte
| producer       = Deutsche Film-Aktiengesellschaft
| writer         = Wolfgang Staudte Ernst Wilhelm Borchert Hildegard Knef Arno Paulsen Erna Sellmer
| music          = Ernst Roters
| cinematography = Friedl Behn-Grund Eugen Klagemann
| editing        = Hans Heinrich
| released       = 1946
| runtime        = 91 minutes
| country        = Germany
| language       = German
}} German films  and the first Rubble film|Trümmerfilm. It was produced in 1945 and 1946 in the Althoff-Atelier in Babelsberg and in Jofa-Ateliers in Johannisthal. It was written and directed by Wolfgang Staudte.

==Plot== Ernst Wilhelm captain Ferdinand Eastern Front. He is now a successful businessman, producing pots out of old Stahlhelme, the German military steel helmet. On Christmas Eve, Mertens plans to kill him, but Wallner stops him at the last minute. They decide to have Brückner put on trial then, and the two start a new life together.

==Filming== Soviets were afraid that viewers could interpret that as a call for vigilante justice.
 Soviet sector. Federal Republic on November 18, 1971.

==Reception==
The picture sold 6,468,921 tickets. 
 Jud Süß.

==See also==
*Cinema of Germany

==References==
 
*  in progress-film.de, the distributor of the complete DEFA film heritage
*  in filmref.com. Retrieved 2007-01-19.
*  in German-films.de. Retrieved 2007-01-19.
==External links==
*   on Ostfilm.de.
* 
*  
*  

 
 
 
 
 
 
 
 