Never a Dull Moment (1950 film)
{{Infobox film
| name           = Never a Dull Moment
| image          = NeverADullMoment1950.jpg
| caption        =
| writer         = Lou Breslow Doris Anderson Kay Swift (book)
| starring       = Irene Dunne Fred MacMurray George Marshall
| producer       = Harriet Parsons
| music          =
| cinematography =
| editing        =
| distributor    = RKO
| released       =   |ref2= }}
| runtime        =
| country        = United States
| language       = English budget = $1.2 million Charles Tranberg, Fred MacMurray: A Biography, Bear Manor Media, 2014 
| gross = $1,425,000 (US)  or $1.8 million 
|}} 1950 RKO comedy film starring Irene Dunne and Fred MacMurray.  The film is based on the 1943 book Who Could Ask For Anything More? by Kay Swift.

==Plot==
At a rodeo in New York, visiting cowboy Chris Heyward is charmed to make the acquaintance of Kay Kingsley, a songwriter. They marry and move out west to his ranch.

Wyoming welcomes her, including Chris former romantic interest Jean Morrow and his two daughters from a previous marriage, Nan and Tina. A rival rancher named Mears holds the water rights to his land and is accidentally humiliated by Kay at a dance.

She tries without complaint to adjust to her new life, but its hard. A windstorm threatens their home, the children are leery of her and Kay accidentally kills Mears prize steer.

Offered a job back in New York that could help pay for the water, Kay takes it but alienates Chris. He needs to be persuaded by the kids to return east and win her back.

==Cast==
*Irene Dunne ... Kay
*Fred MacMurray ... Chris
*William Demarest ... Mears
*Andy Devine ... Orvie
*Gigi Perreau ... Tina 
*Natalie Wood ... Nan
*Philip Ober ... Jed
*Jack Kirkwood ... Papa Dude
*Ann Doran ... Jean

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 


 