Aabroo
 
{{Infobox film
| name           =Aabroo
| image          = 
| caption        = 
| director       =  Govind Ram
| producer       =  Hind Pictures
| writer         = Yakub Masud Nazir Ahmed
| music          = Pandit Gobindram
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Nazir Ahmed, Laddan and Chandabai. The music was composed by Pandit Gobindram.   

Nazir had starred with Sitara Devi earlier in Baagbaan (1938) and when he re-launched his banner Hind Pictures in 1943, he offered a partnership to Sitara Devi. She acted in all the five films produced by Hind Pictures that year. Four of them including Aabroo co-starred Nazir while one (Salma) had Ishwarlal opposite her.     

==Cast==
*Sitara Devi Nazir Ahmed Yakub
*Masud
*Laddan
*Chandabai
*Jagdish Sethi
*Vatsala Kumthekar
*Shakir
*Janardan Sharma

==Soundtrack==
The music director was Pandit Gobindram and the lyrics were penned by several lyricists namely: Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati.    One version of the old popular song "Inhi Logon Ne Le Leena Dupatta Mera" is rendered in the film  by Yakub. The lyrics are credited to Tanvir Naqvi    though there is a song recorded earlier than this. The song later gained popularity when used in Pakeezah 1972, sung by Lata Mangeshkar and with lyrics credited to Majrooh Sultanpuri.   

===Discography===

{| class="wikitable"
|-
! Number !! Song !! Singer !! Lyricist 
|- Ke Loot Vatsala Kumthekar Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati
|- Naiya Hamari Hasrat Lakhnavi
|- Inhi Logon Yakub || Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati
|- Piya Milan Sitara Devi, G. M. Durrani|| Hasrat Lakhnavi 
|- Ye Gham Sitara Devi Rajjan
|- Gori Baanke Sitara Devi, Hasrat Lakhnavi
|- Sautan Ke Sitara Devi || Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati
|- Haaye Kisi Sitara Devi || Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati
|- Pune Se Laayi Paan Re || Sitara Devi, Nazir Ahmad || Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati
|- Hamari Zindagi Sitara Devi || Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati
|- Dukh Dard Sitara Devi || Tanveer Naqvi, Rajjan, Hasrat Lakhnavi, Swami Ramanand Saraswati
|-
|}

==References==
 

==External links==
*  
*  

 

 
 
 