The Gay Diplomat
{{Infobox film
| name           = The Gay Diplomat
| image          = Ivan Lebedeff.jpg
| image_size     =
| caption        = Ivan Lebedeff as Captain Orloff
| director       = {{plainlist|*Richard Boleslawski
*Ray Lissner (assistant)}}
| producer       = {{plainlist|*William LeBaron
*Pandro S. Berman}}
| writer         = Benn W. Levy 
| starring       = {{plainlist|*Ivan Lebedeff
*Genevieve Tobin
*Betty Compson}}
| music          = Max Steiner
| cinematography = Leo Tover Arthur Roberts RKO Radio Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = $184,000 
| gross          = $131,000 
}}
The Gay Diplomat is a 1931 American film. Directed by Richard Boleslawski for RKO Radio Pictures, it starred Ivan Lebedeff, Genevieve Tobin and Betty Compson.

==Synopsis==
Captain Orloff (Ivan Lebedeff) is a Russian military officer who is sent to Bucharest to discover and dispose of a female spy. The three suspected spies are Countess Diana Dorchy (Genevieve Tobin), Baroness Alma Corri (Betty Compson) and Madame Blinis (Ilka Chase). Before learning the identity of the spy, Orloff falls in love with Diana. In the course of events, the spy is revealed to be Alma who is ultimately tricked into confessing. Orloff returns with his prisoner to St. Petersburg and is joined on the train by Diana. 

==Cast==
*Genevieve Tobin as Countess Diana Dorchy
*Betty Compson as Baroness Alma Corri
*Ivan Lebedeff as Captain Ivan Orloff
*Ilka Chase as Madame Blinis
*Purnell Pratt as Colonel George Gorin
*Colin Campbell as Gamble
*Arthur Edmund Carew as The Suave Man
*Edward Martindel as Ambassador
*John St. Polis
*Judith Vosselli George Irving as A colonel Rita LaRoy as Natalie

==Production==
According to the trade journal Film Daily, RKO reported the original story "Strange Women" was written by Lebedeff and Benn W. Levy.  In addition to Strange Women, working titles included Woman Pursued and Kisses By Command.
 Henry Hobart as supervising producer in mid-production, thus earning Berman his first screen credit. 

The picture was Lebedeffs first starring role and he figured heavily in RKOs marketing campaign, which touted him as another Valentino and portrayed the story as based on events from his life.   Tobin was borrowed from Universal to play the female lead. 

==Reception==
The film was released September 19, 1931. According to RKO records, the film was the studios lowest grossing film of the 1930–31 season and lost $115,000 at the box office. 

The Gay Diplomat was generally poorly received by critics. New York Times critic Mordaunt Hall called it "highly predictable".  The Variety reviewer found the story incomprehensible and called the dialog "inane" and the acting "some of the poorest";  Film Daily, summed it up as "mechanical and slow moving&nbsp;... with artificial treatment and acting". 

==References==
{{reflist|refs=
   
   
   
   
   
   
   Full page advertisement for Gay Diplomat. 
}}

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 