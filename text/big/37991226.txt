Robert and Bertram (1939 film)
{{Infobox film
| name = Robert and Bertram
| image =
| image_size =
| caption =
| director = Hans Heinz Zerlett Helmut Schreiber
| writer = Gustav Raeder (play)   Hans Heinz Zerlett
| narrator =
| starring = Rudi Godden   Kurt Seifert   Carla Rust   Fritz Kampers
| music = Leo Leux
| cinematography = Friedl Behn-Grund
| editing = Ella Ensink
| studio = Tobis Filmkunst
| distributor = Tobis-Filmverleih
| released = 7 July 1939
| runtime =
| country = Germany German
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical comedy Robert and Polish film of the same title the previous year. It was set in 1839.

It was the only anti-semitic musical comedy released during the Nazi era and the first film since Kristallnacht to focus on Jews as cultural and economic outsiders.  In fact the antagonist of this film itself, the Jew Nathan Ipelmeyer is not a cultural and economic outsider, but a very wealthy "Kommerzienrat" (cf. Geheimrat).

==Cast==
* Rudi Godden as Robert
* Kurt Seifert as Bertram
* Carla Rust as Lenchen
* Fritz Kampers as Strambach, Gefängnisverwalter
* Heinz Schorlemmer as Michael, Strambachs Neffe Kommerzienrat
* Tatjana Sais as Isodora Ipelmeyer, his daughter
* Ursula Deinert as a dancer
* Robert Dorsay as Jack, Ipelmeyers servant
* Erwin Biegel as Fochheimer
* Hans Stiebner as Blank, cop
* Arthur Schröder as Herr Biedermeier
* Willi Schur as a minstrel
* Eva Tinschmann as a minstrel
* Inge van der Straaten as Frau Ipelmeyer
* Friedrich Beug as the chief of the police
* Peter Bosse as Junge beim Ballonaufstieg
* Fred Goebel as a guard
* Harry Gondi as a guard
* Aribert Grimmer as Gendarm
* Otto F. Henning as Minister
* Fritz Hoopts as Flint, cop
* Kurt Keller-Nebri as Mylord beim Empfang Ipelmeyer
* Franz Kossak as Dame von Café Kranzler
* Gustl Kreusch as Dame von Café Kranzler
* Walter Lieck as Dr. Cordvan
* Alfred Maack as innkeeper Manfred Meurer as Lakai
* Arnim Münch as a bookkeeper
* Lucie Polzin as Mutter des Jungen
* F.W. Schröder-Schrom as one of Ipelmeyers guests
* Rudolf Schündler as one of Ipelmeyers guests
* Gerhard Dammann as Peter
* Claire Glib as a belly dancer
* Kurt Mikulski as Fritz
* Gerti Ober as a girl in the Café Kranzler
* Egon Stief as Mann mit der Kraftmaschine
* Auguste Wanner-Kirsch as Hochzeitgast
* Kurt Zehe as the giant at the fair

==References==
 

==Bibliography==
* OBrien, Mary-Elizabeth. Nazi Cinema as Enchantment: The Politics of Entertainment in the Third Reich. Camden House, 2006.
* Waldman, Harry. Nazi Films In America, 1933-1942. McFarland, 2008.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 