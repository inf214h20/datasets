Move (film)
{{Infobox film
| name           = Move
| image          = Move -- movie poster.jpg
| image_size     = 220
| caption        = Film poster
| director       = Stuart Rosenberg
| producer       = Pandro S. Berman
| writer         = Joel Lieber Stanley Hart
| starring       = Elliott Gould Paula Prentiss
| music          = Marvin Hamlisch
| cinematography =
| editing        =
| studio         = Twentieth Century-Fox Film Corporation
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 min
| country        = United States
| language       =
| budget         = $2,785,000 
| gross          =
}}
Move is a 1970 comedy film starring Elliott Gould, Paula Prentiss and Geneviève Waïte, and directed by Stuart Rosenberg. The screenplay was written by Joel Lieber and Stanley Hart, adapted from a novel by Lieber.   

== Synopsis ==
The film covers three days in the life of Hiram Jaffe (Gould), a would-be playwright who supplements his living as a porn writer and by walking dogs.  He and his wife, Dolly (Paula Prentiss), are moving to a new apartment on New Yorks Upper West Side.  Jaffe is beset by problems, including his inability to persuade the moving man to move the couples furniture, and retreats into fantasy.

== Cast ==
* Elliott Gould as Hiram Jaffe
* Paula Prentiss as Dolly Jaffe
* Geneviève Waïte as Girl
* John Larch as Patrolman
* Joe Silver as Oscar
* Graham Jarvis as Dr. Picker
* Ron ONeal as Peter
* Garrie Beau as Andrea

== Notes ==
 

== References ==
*  
*  

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 