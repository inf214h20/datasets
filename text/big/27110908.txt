The Night Holds Terror
{{Infobox film
| name           = The Night Holds Terror
| image          = Night holds terror 1955 poster small.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew L. Stone
| producer       = Andrew L. Stone
| screenplay     = Andrew L. Stone
| narrator       =  Jack Kelly Vince Edwards John Cassavetes
| music          = Lucien Cailliet
| cinematography = Fred Jackman Jr.
| editing        = Virginia L. Stone
| studio         = Columbia Pictures
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
  Jack Kelly. 

It was originally shown on Z Channel, uncut and commercial-free.

==Plot==
Family man Gene Courtier makes the mistake of picking up a hitch-hiker, Victor Gosset, a wanted criminal.

Gosset and his accomplices, Robert Batsford and Luther Logan, take Courtier and his family captive in their home at gunpoint. They demand that Courtier sell the car in the morning and hand over the money.

With police closing in, the gang refuses to leave. Courtiers father is a wealthy man, so the robbers now want a large sum in ransom. Courtier gets the best of them eventually, however, with all three of his kidnapers ending up dead.

==Cast== Jack Kelly as Gene Courtier
* Vince Edwards as Gosset
* John Cassavetes as Batsford
* David Cross as Logan
* Hildy Parks as Doris Courtier
* Jack Kruschen as Pope

==References==

===Notes===
 

===References===
*  
*  

==External links==
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*  

 
 
 
 
 
 
 


 