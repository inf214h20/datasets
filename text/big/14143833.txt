Black Arrow (serial)
{{Infobox film
| name           = Black Arrow
| image          = Black_Arrow-serial.jpg
| caption        =
| director       = Lew Landers B. Reeves Eason Rudolph C. Flothow Sherman Lowe Jack Stanley Leighton Brill Royal K. Cole
| narrator       = Robert Scott Robert Williams Kenneth MacDonald
| music          = Lee Zahler Richard Fryer Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 chapters 270 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Columbia Serial film serial.  It was the twenty-fourth of the fifty-seven serials released by Columbia.

==Plot==
Buck Sherman and Jake Jackson, a couple of evil carpetbaggers, illegally enter a Navajo reservation in to prospect for gold and end up killing Aranho, the Navajos chief.  Black Arrow, presumed Aranhos son, refuses to kill the Indian agent, Tom Whitney, in revenge as demanded by Navajo law. Then, he is driven off the reservation for his reluctance to kill  Whitney and decides joins forces with Pancho, Mary Brent and the agent to goes in search  of the men who killed the chief.

==Cast==
{|
|- Mark Roberts Robert Scott || Black Arrow
|- Adele Jergens || Mary Brent
|- Robert Williams Robert Williams || Buck Sherman
|- Kenneth MacDonald Kenneth MacDonald || Jake Jackson
|- Charles B. Charles Middleton || Tom Whitney
|- Martin Garralaga || Pancho
|- George J. Lewis || Snake-That-Walks
|-
|I. Stanford Jolley || Tobis Becker
|- Bud Osborne || Fred
|- Stanley Price || Wade
|- Eddie Parker || Hank
|- Ted Mapes || Hank
|- Dan White Dan White || Paul Brent
|- Chief Thundercloud &nbsp; &nbsp; || Tribal Medicine Man
|- Bud Osborne || Fred
|-
|}

==Chapter titles==
# The City of Gold
# Signal of Fear
# The Seal of Doom
# Terror of the Badlands
# The Secret of the Vault
# Appointment with Death
# The Chamber of Horror
# The Vanishing Dagger
# Escape from Death
# The Gold Cache
# The Curse of the Killer
# Test by Torture
# The Sign of Evil
# An Indians Revenge
# Black Arrow Triumphs

==Fact==
*In this serial, the completely unknown Robert Scott starred as Black Arrow. It was his first and only film leading role. He later changed his name to Mark Roberts and starred as reporter Hildy Johnson in the 1949-1950 syndicated television series The Front Page.

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 