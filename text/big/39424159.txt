Secret of Deep Harbor
 
{{Infobox film
| name           = Secret of Deep Harbor
| image size     =
| image    	 = "Secret_of_Deep_Harbor"_(1961).jpg	 Ron Foster
| director       = Edward L. Cahn
| producer       = Edward Small (executive) Robert E. Kent
| writer         =  Owen Harris Wells Root
| based on       = novel by Max Miller Ron Foster Merry Anders
| music          = Richard LaSalle
| cinematography = Gilbert Warrenton
| editing        =  Kenneth G. Crane
| distributor    = United Artists
| studio         = Harvard Film Corporation
| released       = {{Film date|1961|10|25|ref1= CINEMA 16 PLANS PROGRAMING SHIFT: Film Society to Concentrate on Foreign Moviemakers
By EUGENE ARCHER. New York Times (1923-Current file)   October 25, 1961: 34. }}
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
}}
The Secret of Deep Harbor is a 1961 film. 

It was a remake by producer Edward Small of his earlier I Cover the Waterfront (1932). The Screen: Waterfront Melodrama at Local Theatres
Thompson, Howard. New York Times (1923-Current file)   October 26,  1961: 39. 

==Plot==
Reporter Skip Hanlon (Ron Foster) is in love with Janey Fowler (Merry Anders), whose father (Barry Kelley) is a sea captain employed by the Mafia. The mob pay the captain to transport gangsters out of the U.S., but when a murder occurs, Skip blows the whistle on the captain. Janey sides with her father and goes on the run with him to Mexico, with Skip hot on their trail.

==Cast list== Ron Foster
*Milo Fowler - 	Barry Kelley
*Janey Fowler - 	Merry Anders
*Barney Hanes - 	Norman Alden
*Travis - 	James Seay
*Rick Correll - 	Grant Richards
*Frank Miner - 	Ralph Manza
*Mama Miller - 	Billie Bird
*Rita - 	Elaine Walker
*Doctor - 	 Max Mellinger

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 

 
 
 
 
 
 
 


 