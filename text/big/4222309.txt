The Simian Line
 
{{Infobox Film
| name           = The Simian Line
| image          = The Simian Line (film).jpg
| image_size     = 
| caption        = DVD cover
| director       = Linda Yellen
| producer       = Linda Yellen Robert Renfield
| writer         = Linda Yellen Michael Leeds Gisela Bernice
| narrator       = 
| starring       = Harry Connick, Jr. Cindy Crawford Tyne Daly William Hurt Monica Keena Samantha Mathis Lynn Redgrave
| music          = Patrick Seymour
| cinematography = David Bridges
| editing        = Bob Jorissen
| distributor    = Gabriel Film Group
| released       = Theatrical: US: Nov. 16,   
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 American improvisational LA in 2001. It was filmed over an eleven-day period. The ensemble cast includes Harry Connick, Jr., Cindy Crawford, Tyne Daly, William Hurt, Monica Keena, Samantha Mathis, Lynn Redgrave, Jamey Sheridan and Eric Stoltz.

==Plot==
When Katharine (Lynn Redgrave) throws a party on Halloween, a psychic called Arnita (Tyne Daly) predicts that one of the three couples present at the party will break up by the end of the year. The guests dont take her seriously. Arnita doesnt tell them that she can see a fourth couple at the party, the long dead Mae (Samantha Mathis) and Edward (William Hurt). As days go by, Katharine grows increasingly jealous of her lover Rick (Harry Connick, Jr.), and his flirting with her neighbor Sandra (Cindy Crawford). Sandra is married to Paul (Jamey Sheridan). Marta (Monica Keena) and Billy (Dylan Bruno) are rock musicians who live in the same building as Katharine.

==Music==
*Patrick Seymour - music  The Water is Wide"
*The Mortal Sinners and Bite Me Band - "Bite Me bit*hes"

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 