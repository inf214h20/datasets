The Banquet (1991 film)
 
 
{{Infobox film
| name           = The Banquet
| image          = TheBanquet VHScover.jpg
| caption        = The Banquet VHS cover
| film name = {{Film name| traditional    = 豪門夜宴
| simplified     = 豪门夜宴
| pinyin         = Háo Mén Yè Yàn
| jyutping       = Hou4 Mun4 Je6 Jin3}}
| director       = Alfred Cheung Joe Cheung Clifton Ko Tsui Hark
| producer       = 
| writer         = 
| starring       = Eric Tsang Richard Ng Carol Cheng Sammo Hung John Shum Andy Lau Leslie Cheung Tony Leung Chiu-Wai Tony Leung Ka-Fai Jacky Cheung Aaron Kwok Leon Lai Rosamund Kwan Maggie Cheung Anita Mui Carina Lau Gong Li Joey Wong George Lam Alan Tam Stephen Chow Michael Hui
| music          = Lowell Lo David Chung Peter Pau
| editing        = Shao Feng Jin Ma David Wu Marco Mak
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}} 1991 Cinema Hong Kong comedy film. It was quickly filmed for a Hong Kong flood relief charity, after the Yangtze River flooded in July of that year, killing over 1,700 people and displacing many more in the eastern and southern regions of mainland China. {{cite web
  | title = The Banquet
  | work = Review
  | publisher = www.HKCuk.co.uk
  | url = http://www.hkcuk.co.uk/reviews/banquet.htm
  | accessdate = 2007-09-11 }} 

A large ensemble of actors and crew (including multiple directors and cinematographers) worked on the film, many in supporting roles and cameos. The principal star is Eric Tsang.

==Plot==
Developer Tsang Siu-Chi (Eric Tsang) and his agent (Jacky Cheung) have bought two of a group of four properties. Rival developer, Boss Hung (Sammo Hung) has secured the other two properties. Both aim to buy all four so they can knock them down and build hotels. 

The agent learns that billionaire Kuwaiti Prince Allabarba (George Lam) is due to arrive in Hong Kong and advises Tsang that they could dupe him in order to gain a billion dollar contract. The princes father has recently died and the prince bitterly regrets that he wasnt a good son.

The agent tells Tsang that he should make a show of the positive relationship he has with his father, to impress the prince. Unfortunately, Tsang has not seen his father (Richard Ng) for 10 years. Along with his wife (Carol Cheng) and his sycophantic assistant (Tony Leung Chiu Wai), Tsang heads off to bring his father back. When they meet up, Tsang pretends to have cancer to convince his father to come home, along with his sister (Rosamund Kwan) and her husband (Tony Leung Ka Fai).

Tsang throws a banquet to impress the prince, pretending that it is also a birthday party for his father. However, it has all been a ploy by the agent, who has secretly been working for Boss Hung.

==Cast==
Almost 100 well-known Hong Kong actors appeared in the film, many of them in cameo roles.
The core cast consists of:
* Eric Tsang - Tsang Siu-Chi
* Sammo Hung - Boss Hung Tai-Po
* Jacky Cheung - Jacky Cheung Ah Hok Yau
* John Shum - Curly, Boss Hungs assistant
* Tony Leung Chiu-Wai - Wai, Tsangs assistant
* Rosamund Kwan - Gigi, Tsangs sister
* Tony Leung Ka-Fai - Leung, Gigis husband
* Richard Ng - Father Tsang
* Carol Cheng (Cheng Yu-Ling, aka Do Do Cheng) - Mimi, Tsangs wife
* Joey Wong - Honey, Jackys wife
* George Lam - Prince Alibaba of Kuwait ("Allabarba" in subtitles)
* Kwan Hoi San - Uncle Chicken Roll
* Lau Siu-Ming - Wong
* Jamie Luk - Vassal
* Pau Hon Lam - Uncle Lotus Seed Bun
* Michelle Reis - Kar-Yan Li
* Lydia Shum - Aunt Bill (as Lydia Sham)
* Bill Tung - Uncle Bill Raymond Wong - Forty
* Gabriel Wong - Vassal
* Tony Leung Ka-Fai - Ah Fai
* Stephen Chow - Himself
* Andy Lau - Presenter
* Maggie Cheung - Personal Singing Instructor
* Chin Kar Lok
* Leslie Cheung - Himself
* Anita Mui - Herself
* Aaron Kwok - Leslies Younger Brother Anthony Chan
* Ti Lung - Cook # 2
* Kara Hui - Household Servant
* Teresa Mo - Presenter
* Simon Yam - Wais friend, body language instructor & gigolo David Wu - Jogger
* John Woo - Guest
* Yuen Miu Mars
* Yuen Tak
* Sandra Ng - Trolley Waitress
* Andrew Yu
* Candice Yu
* Eric Kot - English Instructor
* Jan Lamb
* Karl Maka - Wais uncle, make-up artist
* Sally Yeh - Herself
* Sylvia Chang - Herself
* Angie Chiu
* Gong Li - Herself
* Michael Hui - Himself
* Leon Lai - Cook Assistant
* Alan Tam - Ali Baba dream version
* Ng Man Tat - Cook #1
* Meg Lam
* Wong Wan-Si
* Kenneth Tsang - Waiter
* Teddy Robin - Football player
* Alfred Cheung
* Philip Chan - Police Officer
* Melvin Wong - Guest
* Billy Lau
* Gordon Liu
* Maria Cordero - Guest
* Gloria Yip
* Josephine Koo
* Lee Hoi San
* Mimi Zhu - Guest
* Tai Chi Squadron - Music Band
* Grasshopper
* Lowell Lo - Taxi Driver
* Anglie Leung
* Lau Kar Leung - Martial Arts Instructor for Fencing
* Fung Hak On
The character of Father Tsang has a number of staff, including a sword expert, Master Lau / Uncle Nine (Lau Kar-leung), a servant (Kara Hui), two English teachers (Eric Kot and Jan Lamb), a make-up artist Mak (Karl Maka) and a body language expert / gigolo (Simon Yam).

Tsang Siu-Chu has a daydream about the banquet, in which his imagined self is played by Leslie Cheung, with Aaron Kwok as his brother, and the imagined Prince Allabarba is played by Alan Tam. He also fantasises that a stream of attractive actresses including Anita Mui, Sally Yeh, Sylvia Chang, Angie Chiu and Gong Li attend the meal. These are followed by leading Hong Kong actors including Anthony Chan Yau, Stephen Chow and Michael Hui (accompanied by Maria Cordero). All of these actors play themselves in the dream sequence, and some return in additional roles at the actual banquet.

At the actual banquet, Tsangs staff include cooks Leon Lai and Ng Man Tat, servants Meg Lam and Wong Wan-Si, and waiting staff May Lo Mei-Mei, Sandra Ng, Fennie Yuen, Ti Lung and Kenneth Tsang.
 Tony Ching, Ku Feng, Carina Lau, Lee Hoi San, Loletta Lee, Waise Lee, Maggie Cheung, Bryan Leung, Mars (actor)|Mars, Lawrence Ng, Barry Wong, Johnnie To, Melvin Wong, John Woo, Pauline Yeung, Gloria Yip, Chor Yuen, Tai Chi Squadron, Yuen Cheung Yan, Mimi Zhu and the band members of Grasshopper (band)|Grasshopper.
 Paul Wong, Wong Ka Kui, Wong Ka Keung and Yip Sai Wing.
 James Wong David Wu as a jogger, Lowell Lo as a cab driver, and Mars (actor) as an unknown role.

==Box office==
The film grossed HK $21.92 million in Hong Kong. {{cite web
  | title = The Banquet
  | work = Database entry
  | publisher = Hong Kong Cinemagic 
  | url = http://www.hkcinemagic.com/en/movie.asp?id=17
  | accessdate = 2007-09-11 }} 

==See also==
* List of Hong Kong films

==References==
 

==External links==
*  
*  
*   at the  

 
 

 
 
 
 
 
 