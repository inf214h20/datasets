Cassadaga (film)
{{Infobox film
| name           = Cassadaga
| image          = Cassadagafilmposter.jpg
| alt            = 
| caption        = 
| director       = Anthony DiBlasi
| producer       = Scott Poiley Bruce Wood
| writer         = Bruce Wood & Scott Poiley
| starring       = Kelen Coleman Kevin Alejandro Louise Fletcher Amy LoCicero Rus Blackwell Sarah Sculco
| music          = Dani Donadi
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
Cassadaga is a 2011 American independent horror film directed by Anthony DiBlasi. The screenplay is the feature-length debut for co-writers Bruce Wood and Scott Poiley.    The film stars Kelen Coleman, Kevin Alejandro, Louise Fletcher, Rus Blackwell, Hank Stone, J Larose, Amy LoCiero and Christina Bach.    
 sociopath dubbed "Geppetto". Cassadaga had its World Premiere at the Mann Chinese Theater 6 in Hollywood, California on October 22, 2011 as part of the Screamfest LA Film Festival. 

== Plot ==
 
Devastated by the death of her younger sister, Lily Morel seeks solace at the spiritualist community of Cassadaga, Florida|Cassadaga. But instead of finding closure, she contacts something else - the vengeful ghost of a murdered young woman. With her life crumbling all-around her, Lily races to unravel the mysterious circumstances surrounding the womans death - a task that will bring her face-to-face with a sadistic serial killer known only as "Geppetto".

== Cast ==
*Kelen Coleman as Lily
*Kevin Alejandro as Mike
*Louise Fletcher as Claire
*Amy LoCicero as Jennifer
*Rus Blackwell as Christian Burton
*Sarah Sculco as Michelle
*Lucius Baston as Officer Bill Hall
*Christina Bach as Gabriella
*Hank Stone as Maxwell
*J. LaRose as Heath
*Randy Molnar as Professor Randall
*Beth Marshall as Amity & Corrie

== Production ==
Writer/Producer Bruce Wood says that he was fascinated with the town of Cassadaga when he was a child and felt compelled to use the location in a film after discovering that no films had yet to utilize the world-famous location.  While writing, Wood had the basic outline of the character “Geppetto” but it wasn’t until he was at the gym and thought up the shocking opening sequence that he discovered why “Geppetto” was the way he was; “in that moment I saw him as a child, I knew this character and what motivated him.” 

Lee Grimes was in charge of the special effects make-up. Director Anthony DiBlasi chose this script because he wanted to do a horror film where “there were characters in that I thought people could relate to and feel bad for when things happen to them”.  Oscar winner Louise Fletcher wanted to be in the movie because both her parents were deaf, just like the main character Lily.  The actor playing “Geppetto” was kept a complete secret until opening night. 

==Reception==
 
The films reception at Screamfest has been mixed to positive,  with Dread Central saying "while not a perfect film, the flick does have a freshly unique voice and is one which more than likely genre fans will want to sink their teeth into."  Gores Truly said the film was "a fantastic film, a disturbing horror and one I would happily add to my collection."  ShockYa.com reviewed it, saying "Although there are some aspects to the pacing and acting that didn’t exactly tickle my fancy, Cassadaga is still a solid horror film that is fun to watch". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 