Malibu Shark Attack
{{Infobox television film
| name = Malibu Shark Attack
| image = Malibu shark attack poster.jpg
| caption = Movie poster
| alt = Mega Shark of Malibu David Lister
| producer = Dale G. Bradley Grant Bradley Richard Stewart Brian Trenchard-Smith
| writer = Keith Shaw
| cast = Megan Clarke Kara Eide Lyn Kidd Tiffany Mak Laura Toplass Kathy Burns Matt de Casanove
| starring = Warren Christie Peta Wilson Chelan Simmons Sonya Salomaa
| music = Michael Neilson
| cinematography = Brian J. Breheny
| editing = Asim Nuraney
| genre = Drama Horror Thriller
| studio = Insight Film Studios Limelight International Media Entertainment
| distributor = IPA Asia Pacific (Thailand/Vietnam) Splendid Film (Germany)
| network = Syfy
| released =  
| runtime = 86 minutes
| country = Canada Australia
| language = English
| budget = $3,000,000 (estimated)
}}
 David Lister and produced for the Syfy channel. It is the 19th film in the Maneater Series.

==Plot== prehistoric goblin sharks who begin to devour swimmers along the beach. Chavez travels to a nearby house that is undergoing construction and meets with the workers Colin (Jeffery Gannon), who is Heathers new boyfriend, George (Mungo McKay), Yancey (Renee Bowen) and Karl (Evert McQueen). A warning of a tsunami arises and Chavez returns to the beach, saving Heather who had been knocked into the water by a shark after being sent to investigate some people causing trouble in the water. Doug and Barb evacuate the beach, and take shelter in the lifeguard hut with Heather, Chavez, Jenny and Bryan.

As the tsunami hits, Bryan is knocked unconscious and Jenny suffers a large cut on her leg. Heather manages to stitch the cut together, however the blood attracts the goblin sharks, as the group realize they are stranded in the hut, with help unlikely due to the damage the tsunami has created. The sharks soon begin to attack the hut and manage to break the floor, dragging Barb out of the hut and devouring her. As the group mourn Barbs death, the construction crew have become stranded in the house they were working at. Yancey decides to try to swim to land and get help, however she is quickly attacked and eaten by sharks, before a shark jumps up and drags Karl into the water. Soon after, Colin and George discover a boat and begin to travel to the hut. Back at the hut, the group manage to kill one of the sharks, before Chavez swims out of the hut to retrieve a flare gun. The others attempt to distract the sharks, however one attempts to eat Chavez, resulting in him shooting and killing the shark with the flare gun, instead of summoning help.

Chavez returns to the hut, before the group move up to the roof as the sharks continue to damage the hut. Colin and George arrive on the boat and everyone boards, but the fuel soon runs out, forcing them to drift to land. After hours, the boat reaches the house that was under construction. The boat becomes stuck on a gate, so George enters the site to try and find pliers to open the gate. As the rest wait, the sharks arrive. Bryan jumps into the water, sacrificing himself so the rest can escape. Doug, Jenny and Colin manage to get into the flooded house and encounter a shark but manage to kill it, while Heather and Chavez discover a half eaten George, before taking shelter in a car, where they reconcile. Chavez kills another shark, before entering the house with Heather. The group meet up, and manage to trap and kill the final shark. In the morning, a helicopter arrives to rescue them.

==Cast==
*Remi Broadway as Doug
*Warren Christie as Chavez
*Nicolas G. Cooper as Bryan
*Jeffery Gannon as Colin Smith
*Mungo McKay as George
*Evert McQueen as Karl
*Sonya Salomaa as Barb
*Peta Wilson as Heather
*Chelan Simmons as Jenny
*Renee Bowen as Yancey

==Reviews==
Reviews have been mostly negative. It has 3.3 out of 10 rating on IMDb.

==Home Media==
Malibu Shark Attack was released on DVD on August 16, 2011.

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 