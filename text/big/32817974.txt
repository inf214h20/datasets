A Son Is Born
 
{{Infobox film
| name           = A Son Is Born
| image          = A_Son_is_Born.jpg
| image_size     = 
| caption        = Lobby card Eric Porter Eric Porter
| writer         = Gloria Bourner
| narrator       = 
| starring       = Ron Randell   Peter Finch Muriel Steinbeck
| music          = Sydney John Kay
| cinematography = Arthur Higgins Damien Parer (war photography)
| editing        = James Pearson
| studio         = Eric Porter Studios
| distributor    = British Empire Films
| released       = 20 September 1946
| runtime        = 85 mins
| country        = Australia
| language       = English
| budget         = ₤10,000 
}}
A Son Is Born is a 1946 Australian melodrama.

==Synopsis==
In 1920, Laurette marries an irresponsible drifter, Paul Graham. They have a son, David, but later divorce due to Pauls drinking and infidelities. David chooses to stay with his father and Laurette  marries again, this time to John, a rich businessman with a teenaged daughter, Kay. 

Years later Paul is killed in a car accident and David comes to live with his mother, John and Kay. To get revenge on his mother for "abandoning" his father, David seduces Kay into marriage and abandons her, but realises the error of his ways serving in New Guinea during World War II. He is injured in battle but survives to be reunited with Kay, Laurette and John.

==Cast==
*Muriel Steinbeck as Laurette Graham
*Ron Randell as David Graham
*Peter Finch as Paul Graham John McCallum as John Seldon
*Jane Holland as Kay Seldon
*Kitty Bluett as Phyllis
*Peter Dunstan as David Graham as a boy

==Production==
The script was written by Gloria Bourner, who was a cartoonist.  Eric Porter storyboarded the entire film prior to filming.  He also put up half the budget himself, with the balance coming from Charles Munro and some private investors. 

Peter Finch, Ron Randell, Muriel Steinbeck and John McCallum were all well established actors when the film was made. Jane Holland was a 22 year old radio actor who later moved to England and married Leo McKern.  Kitty Bluett was a musical comedy star, the daughter of comedian Fred Bluett. 

The film was shot in the Supreme Sound System studio in early 1945. Filming was scheduled to allow the actors to take radio and stage jobs, and sometimes would start at midnight.  War footage shot by Damien Parer is used in the New Guinea sequences. 

The movie was shot prior to Smithy (1946 film)|Smithy (1946), also starring Randell and Steinbeck, but its release was held off until after that bigger budget movie to take advantage of its publicity.

==Reception==
Ron Randell was mobbed by female fans at the films premiere.  Critical response was mixed, many comparing the film unfavourably with Smithy (1946 film)|Smithy.   
 Dorrigo and Coffs Harbour called Storm Hill, but it never eventuated.  

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive
*  at National Archives of Australia
*  at Oz Movies
 
 
 