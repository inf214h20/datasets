Los Irrompibles
 
{{Infobox film
| name           =Los Irrompibles
| image          =
| image_size     =
| caption        =
| director       = Emilio Vieyra
| producer       = Fernando Molina
| writer         = Emilio Vieyra Antonio Rosso
| narrator       = Jorge Martínez Ricardo Espalter Graciela Alfano Roberto Escalada Rolando Dumas Santiago Gómez Cou Betiana Blum Enrique Almada Eduardo DAngelo Berugo Carámbula Andrés Redondo
| music          = Luis María Serra Emilio Vieyra
| cinematography = Nort Films (Argentina)
| editor       =
| distributor    = Nort Films (Argentina)
| released       = 1975
| runtime        = 85 min.
| country        = Argentina Spanish
| budget         =
}}
 1975 Argentina|Argentine western film.

==Plot==
Harry "El Caliente" and Billy "El Frío" (Jorge Martínez and Ricardo Espalter) are two successful detectives, hired directly from the United States to solve a series of gold robberies from coaches owned by the Argentine Gold Mining Company. They are "spaghetti western" type cowboys, who are helped by four mysterious riders dressed in white overcoats and white hats (played by Enrique Almada, Eduardo DAngelo, Andrés Redondo and Berugo Carámbula). The four riders always appear just to help the two detectives, in the role of "guardian angels".

==Cast== Jorge Martínez as Harry "El Caliente"
* Ricardo Espalter as Billy "El Frío"
* Graciela Alfano
* Roberto Escalada
* Rolando Dumas
* Santiago Gómez Cou
* Betiana Blum
* Enrique Almada
* Eduardo DAngelo
* Berugo Carámbula
* Andrés Redondo

==External links==
*  

 
 
 
 


 