Faith in London
 
{{Infobox film
| name           = Faith in London
| image	         = Faith in London.jpg
| image size     = 
| caption        = Promotional poster
| director       = Tariq Chow
| producer       = Tariq Chow
| writer         = Tariq Chow
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = Moby
| cinematography = Tariq Chow
| editing        = Tariq Chow
| studio         = Film Pill   
| distributor    = 
| released       =  
| runtime        = 3 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 British silent silent short short documentary film directed, written and produced by Tariq Chow. The film celebrates the common instruction towards compassion that exists in all religions and honours multi-faith coexistence London.

==Summary==
The message of Faith in London is presented by representatives of six worldwide religions, each displaying a written quote from their core beliefs,    highlighting the common thread that runs through all the main religions.   

The film was shot at a series of places of worship around London – a church (building)|church, Hindu temple, synagogue, mosque, gurdwara and Buddhist temple. Outside each of them stands a worshipper, holding a board on which is written an extract from their scriptures, urging compassion and love. 

==Appearances==
*Graeme Innes
*Harish Raval
*Philip Rosenberg
*Simone Nardella
*Pertejbakhtawar Singjohal
*Karma Namjal and Ani Samten

==Production==
  }}
Faith in London was written, produced, directed, filmed and edited by Tariq Chow (Chowdhury). It features the song "Live Forever" by Moby.    Filming took place in May 2010 at St Pauls Cathedral, Ilford Hindu Centre, United Synagogue Central, London Central Mosque, Gurdwara Sri Guru Singh Sabha and Kagyu Samye Dzong London Tibetan Buddhist Centre.   

In July 2010, Chowdhury told The CNN Wire, "The journey of making the film was far more incredible than what the film would suggest, and its sad that I am the only person who went through that." He added, "the instruction toward being kind and compassionate towards everyone, and this is found in the texts of all the six major religions that I featured in my film." 

==Release==
On 22 July 2010, Faith in London premiered at a British Academy of Film and Television Arts (BAFTA) event in London,     where it was joint runner up in the 18-25 film-maker category  of the Tony Blair Faith Foundations Faith Shorts. 

The film featured in The CNN Wire in July 2010,  The Guardian in August 2010,  and on a special edition of Songs of Praise on BBC One in September 2011.       The film was screened globally by the United Nations Alliance of Civilizations.   

==Reception==
Tony Blair wrote in The Guardian of Faith in London, "This highlights the common thread that runs through all the main religions."  The SIGNIS Jury said of the film, "Simplicity and artfulness make Faith in London special." 

==Awards and nominations==
{| class="wikitable sortable"
! Year
! Award
! Category
! Result
|-
| rowspan="1"| 2010
| Tony Blair Faith Foundation Faith Shorts
| 18-25 film-maker
| style= "background:#faeb86" | 
|-
| rowspan="1"| 2011
| PLURAL+ Youth Video Festival
| SIGNIS Prize
|   
|-
|}

==See also==
*Religion in London
*Interfaith dialogue

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 
 
 
 
 