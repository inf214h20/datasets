Artist (film)
{{Infobox film
| name           = Artist
| image          = 
| alt            =  
| caption        = 
| director       = Shyamaprasad
| producer       = M. Mani
| writer         = Shyamaprasad
| based on       =  
| starring       = {{Plainlist|
* Fahadh Faasil
* Ann Augustine
* Sreeram Ramachandran
* Sidhartha Siva
* Srinda Ashab
}}
| music          = Bijibal
| editing        = Vinod Sukumaran
| studio         = Sunitha Productions
| distributor    = Sunitha Productions
| runtime        = 
| released       =  
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Artist is a 2013 Indian Malayalam drama film written and directed by Shyamaprasad. An adaptation of Dreams In Prussian Blue, a paperback novel by Paritosh Uttam, the film is about two fine arts students, both driven by individual ambitions, who decide to live together. The film traces the course of their relationship and their progression as artists. It features Fahadh Faasil playing Michael and Ann Augustine as his lover, Gayathri. {{cite news
| author= P. Anima
| date= September 6, 2013
| title= Being an artist
| newspaper= The Hindu
| location= Kozhikode
| publisher= Thehindu.com
| url= http://www.thehindu.com/features/cinema/being-an-artist/article5100677.ece Vanitha along with a host of newcomers. The film is produced by M. Mani under his banner, Sunitha Productions. The music is composed by Bijibal and the editing is by Vinod Sukumaran. {{cite news
| author= Asha Prakash
| date= September 6, 2013
| title= Music Review: Artist
| newspaper= The Times of India
| location= 
| publisher= Indiatimes.com
| url= http://articles.timesofindia.indiatimes.com/2013-09-06/music-reviews/41803353_1_sad-song-shyamaprasad-mollywood Best Actress Best Actor (Fahadh Faasil).

==Plot==
Artist traces the journey of two artists — Michael Agnelo (Fahadh Faasil), son of a Goan businessman, and Gayatri (Ann Augustine), who hails from a conservative Brahmin family. Gayatri shocks her parents by choosing to study Arts in college, then decides to drop out of college and start living with Michael, an eccentric genius with a promising career. Their new life together does not eradicate her isolation, and as Michael wraps further layers of self-centeredness around the cocoon that he has built for himself, Gayatri looks around for ways to keep her passion for him intact. Gayathri is barely out of her teens when she starts working to support her partner even though she does not get even emotional support from him. Her woes increase when Michael loses his eyesight in a road accident. The challenges of the ungrateful Michael who struggles to transform his artistry on the canvas and how the youngsters tackle egoism, selfishness and their artistic demands form the crux of the story.

==Cast==
* Fahadh Faasil as Michael
* Ann Augustine as Gayatri
* Sreeram Ramachandran as Abhinav
* Sidhartha Siva as Roy, a curator
* Krishnachandran as Gayatris father
* Vanitha Krishnachandran as Gayatris mother
* Srinda Ashab as Ruchi
* Rudran as Surgeon
* Sandeep as Doctor
* Syamala S. Nair as Landlady

==Production==
===Novel=== UP parents. Paritosh is a graduate from IIT Madras, and has a full-time job as a software professional, based in Pune. Dreams In Prussian Blue was published in 2010. Paritosh Uttam says, "A few months after its release Shyamaprasad contacted me through my website and said that he had read it and had liked it because the themes it dealt with vibed with his thoughts. He said he would like to make a film based on it. I found out that Shyamaprasad is an acclaimed director and was glad to say yes. We talked over email and phone about how to take this forward. The novel is my work but its adaptation to the screen is fully Shyamaprasads". {{cite news
| author= Pramod Thomas 
| date= September 3, 2013
| title= The man behind artist
| newspaper= The New Indian Express
| location= Kochi
| publisher= Newindianexpress.com
| url= http://newindianexpress.com/cities/kochi/The-man-behind-artist/2013/09/03/article1765606.ece 
| accessdate=September 14, 2013}}  Shyamaprasad says that transforming this story narrated in words onto the visual medium was certainly a challenge. The film is set in Kerala in 2013, unlike the book, which had its backdrop in Mumbai in 2010. {{cite news
| author= Mythily Ramachandran
| date= September 5, 2013
| title= Shyamaprasad is back with ‘Artist’
| newspaper= Gulf News
| location=Dubai
| publisher= Gulfnews.com
| url= http://gulfnews.com/arts-entertainment/celebrity/shyamaprasad-is-back-with-artist-1.1227231
| accessdate=September 14, 2013}} 

===Filming===
The films shooting commenced in February 2013. {{cite news
| author= Parvathy S Nayar
| date= February 20, 2013
| title= Shyamaprasad begins filming Artist
| newspaper= The Times of India
| location=
| publisher= Indiatimes.com
| url= http://articles.timesofindia.indiatimes.com/2013-02-20/news-and-interviews/37199282_1_fahadh-faasil-ann-augustine-shyamaprasad College of Fine Arts in the city. {{cite news
| author= Saraswathy Nagarajan
| date= August 29, 2013
| title= Artistic blues
| newspaper= The Hindu
| location= Thiruvananthapuram
| publisher= Thehindu.com
| url= http://www.thehindu.com/features/cinema/artistic-blues/article5071407.ece
| accessdate=September 14, 2013}}  Like Shyamaprasads previous film English (film)|English, Artist was also shot in sync sound. Sohail Sanwari was the sound designer and Shamdat the cinematographer of the film. {{cite news
| author= Athira M.
| date= March 28, 2013
| title= Art of romance
| newspaper= The Hindu
| location= Thiruvananthapuram
| publisher= Thehindu.com
| url= http://www.thehindu.com/features/cinema/art-of-romance/article4557941.ece
| accessdate=September 14, 2013}} 

==Reception==
The film received critical acclaim. Aswin J. Kumar of The Times of India said, "Artist is a well-assembled film, beautifully crafted and portrayed by a stunning cast." {{cite news
| author= Aswin J. Kumar
| date= September 2, 2013
| title= Artist
| newspaper= The Times of India
| location=
| publisher= Indiatimes.com
| url= http://articles.timesofindia.indiatimes.com/2013-09-02/movie-reviews/41688319_1_fahadh-ann-augustine-shyamaprasad
| accessdate=September 14, 2013}}  Paresh C. Palicha of Rediff.com stated that the film "keeps the viewer intrigued and involved till the very end". He rated the film 3/5 and regarded it as one of Shyamaprasads best works till date. {{cite web
| author= Paresh C. Palicha
| date= September 2, 2013
| title= Review: Malayalam movie Artist is worth a watch
| location= 
| publisher= Rediff.com
| url= http://www.rediff.com/movies/report/south-review-malayalam-movie-artist-is-worth-a-watch/20130902.htm
| accessdate=September 14, 2013}}  Veeyen of Nowrunning.com rated the film 3/5 and said, "The soul stifling experience that Shyamaprasads Artist is, leads us on to a world of paints and easels, palettes and pastels, where ingenuity and inventiveness flow on, in strokes and flourishes from the tip of a brush. The painterly detail that the director has on offer for us in Artist , makes it a visually sumptuous film that engrosses us with its enchanting tale." {{cite web
| author= Veeyen
| date= September 5, 2013
| title= Artist Review
| location=
| publisher= Nowrunning.com
| url= http://www.nowrunning.com/movie/12220/malayalam/artist/4378/review.htm
| accessdate=September 14, 2013}} 

==Awards==
; Kerala State Film Awards Best Director - Shyamaprasad Best Actress - Ann Augustine Best Actor - Fahadh Faasil

==References==
 

 

 
 
 
 
 
 
 