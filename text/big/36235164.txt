Mythili Ennai Kaathali
{{Infobox film
| name           = Mythili Ennai Kaathali
| image          = 
| caption        =
| director       = Vijaya T. Rajendar
| producer       = Usha Rajendar
| writer         = Vijaya T. Rajendar
| screenplay     = Vijaya T. Rajendar Amala Senthamarai
| music          = Vijaya T. Rajendar
| cinematography = 
| editing        = K. R. Ramalingam
| distributor    = Thanjai Cine Arts
| studio         = Thanjai Cine Arts
| released       =  
| runtime        = 
| country        = India Tamil
| budget         =
| gross          =
}}
 1986 Cinema Indian Tamil Tamil film, Amala and Senthamarai in lead roles. The film had musical score by Vijaya T. Rajendar.  

==Cast==
 
*T. Rajendar
*Srividya Amala (debut)
*Senthamarai in Guest Appearance
*S. S. Chandran
*Venniradai Moorthy
*Y. Vijaya
*Usilaimani
*Sivaraman
*Nagaraja Chozhan
*Disco Shanti Thyagu
 

==Soundtrack== 
The music was composed by Vijaya T. Rajendar.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ada Ponnaana Manase || K. J. Yesudas, Vijaya T. Rajendar || || 
|- 
| 2 || Engum Maithili || S. P. Balasubrahmanyam || ||
|- 
| 3 || En Aasai Maithiliye || Vijaya T. Rajendar, Chorus || ||  
|-  Janaki || || 
|- 
| 5 || Mayil Vanthu || S. P. Balasubrahmanyam || ||  
|- 
| 6 || Paavaadai || Malaysia Vasudevan || || 
|- 
| 7 || Ponmaanai || S. P. Balasubrahmanyam || ||  
|-  Janaki || ||
|- 
| 9 || Saareeram || S. P. Balasubrahmanyam || ||
|- 
| 10 || Thaneerile || S. P. Balasubrahmanyam || || 
|}

==References== 
 

==External links==
*   
*  

 

 
 
 
 


 