Pardon My Sarong
{{Infobox film
| name = Pardon My Sarong
| image = a&csarong.jpg
| caption = VHS Cover
| director = Erle C. Kenton
| producer =  
| writer =  
| starring =  
| music = Charles Previn	
| cinematography = Milton R. Krasner
| editing = Arthur Hilton
| distributor = Universal Pictures
| released =  
| runtime = 84 minutes
| language = English
| budget = $400,000 
| gross = $2.2 million
}}

Pardon My Sarong is a 1942 comedy film starring Abbott and Costello. 

==Plot==
Tommy Layton (Robert Paige), a wealthy bachelor, rents a city bus to take him from Chicago to Los Angeles. Once there he intends to participate in a yacht race to Hawaii. The bus drivers, Algy (Bud Abbott) and Wellington (Lou Costello), are chased by a detective (William Demarest) hired by the bus company. They escape capture by driving the bus off a fishing pier. Layton, who is now on his yacht, rescues them and hires them as his crew for the race. A competitor of his in the race, Joan Marshall (Virginia Bruce) has fired his original crew without his knowledge. He enacts revenge by kidnapping her and taking her along on the race.

While on course to Hawaii, they encounter a hurricane and land on an uncharted island, which is also the home of Dr. Varnoff (Lionel Atwill), a mysterious scientist. The island natives mistake Wellington as a legendary hero and inform him that he must marry Princess Luana (Nan Wynn). Meanwhile, Varnoffs plan is to cause a volcano to erupt in order to trick the tribe into giving him their sacred jewel. The natives send Wellington (and the jewel) to the volcano to defeat the evil spirit of the volcano. Varnoff chases him to the volcano, where they are defeated by Wellington and Algy.

==Production==
Pardon My Sarong was filmed at Universal Studios from March 2 through April 28, 1942. The films original draft, dated July 19, 1941, was titled Road to Montezuma. 
 Bill Kenny and 2nd tenor Deek Watson was featured on "Shout Brother Shout" singing the lead part and playing Trumpet. The famous dance group "Tip, Tap, and Toe" danced during the night club scene.

==World premiere==
The film premiered in Costellos hometown of Paterson, New Jersey at a benefit for St. Anthonys Church. 

==Box office==
This film went on to be Universals top grosser of 1942 bringing in $2.2 million according to Variety. It was the second biggest hit of 1942. 

==DVD release==
This film has been released twice on DVD. The first time, on The Best of Abbott and Costello Volume One, on February 10, 2004, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 