Oru Maymasa Pulariyil
{{Infobox film
| name           = Oru Maymasa Pulariyil
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = V. R. Gopinath
| producer       = Thara Movies Ranjith
| screenplay     = V. R. Gopinath Murali Shari
| music          = Raveendran
| cinematography = Santosh Sivan
| editing        = Rajasekharan
| studio         = Thara Movies
| distributor    = 
| released       =  
| runtime        = 151 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Murali and Shari. It was the debut of Ranjith (director)|Ranjith, who wrote the story of the film.    

==Plot==
Balachandra Menons character comes to investigate the reasons for the apparent suicide of Sharis character.sharis character is the central theme of the plot for the film.

==Cast==
* Balachandra Menon Murali
* Shari
* Ashokan as Cricket Star   Parvathy
* Kaviyoor Ponnamma
* Sreeja

==Soundtrack==
The music was composed by Raveendran and lyrics was written by P. Bhaskaran.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Iru Hridayangalil || K. J. Yesudas, KS Chithra || P. Bhaskaran ||
|-
| 2 || Manushyan || K. J. Yesudas || P. Bhaskaran ||
|-
| 3 || Mummy Mummy || KS Chithra, Ajithan, Baiju || P. Bhaskaran ||
|-
| 4 || Pularkaala   || KS Chithra || P. Bhaskaran ||
|-
| 5 || Pularkala Sundara || KS Chithra || P. Bhaskaran ||
|}

==References==
 

==External links==
*  
*   at Cinemaofmalayalam.net

 
 

 