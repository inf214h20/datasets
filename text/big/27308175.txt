They Went That-A-Way & That-A-Way
{{Infobox film
| name = They Went That-A-Way & That-A-Way
| image = 
| alt = 
| caption = 
| director =    Edward Montagne
| producer = Lang Elliott
| writer = Tim Conway
| starring = Tim Conway  Chuck McCann  Richard Kiel  Dub Taylor  Reni Santoni  Lenny Montana  Sonny Shroyer  Ben L. Jones  Timothy Blake  Hank Worden
| music = Michael Leonard
| cinematography = Jacques Haitkin
| editing = Fabien D. Tordjmann
| studio = International Picture Show
| distributor = The International Picture Show Company
| released =  
| runtime = 96 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
They Went That-A-Way & That-A-Way is a 1978 slapstick comedy film written by and starring Tim Conway. The movie was directed by   and Edward Montagne.

==Plot summary==
Dewey (Tim Conway) and Wallace (Chuck McCann) are small-town lawmen who are ordered by the governor to go undercover as prison inmates to find out where a gang of thieves have hidden their loot. While theyre undercover, however, the governor dies, and because no one else knows about the ruse Dewey and Wallace are stranded in prison.

==Availability==
This movie was released on DVD by MGM Home Entertainment on October 8, 2002, as part of a double feature with The Longshot, another Tim Conway movie.

==Notes==
* Two of the actors in this movie, Sonny Shroyer and Ben L. Jones, are known for being regular actors on The Dukes of Hazzard.
* Richard Kiel, who stars in this film as the character "Duke," played an identical role in an episode of the 1980s TV series, "The Fall Guy," episode "Thats Right, Were Bad."

==References==
*  
*  
*  

 
 
 
 
 
 


 