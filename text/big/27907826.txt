Mankatha
 
 
{{Infobox film
| name = Mankatha 
| image = Mankathathefilm01.jpg
| caption = Promotional poster
| director = Venkat Prabhu
| producer = Dhayanidhi Alagiri Vivek Rathnavel
| screenplay =Venkat Prabhu
| story = Venkat Prabhu Anjali Premji Amaren
| music = Yuvan Shankar Raja
| cinematography = Sakthi Saravanan
| editing = Praveen K. L. N. B. Srikanth
| studio = Cloud Nine Movies Radaan Mediaworks  Ayngaran International  (United Kingdom)  FiveStar  (Malaysia)  Sri Sai Ganesh Productions  (Telugu language|Telugu) 
| released =    (Tamil)        (Telugu)  
| runtime = 160 minutes 
| country = India
| language = Tamil
| budget =   
| gross =
}}
 Tamil action action thriller film written and directed by Venkat Prabhu.    It features Ajith Kumar in the lead role, starring in his 50th film,  along with an ensemble cast including Arjun Sarja, Trisha Krishnan, Vaibhav Reddy, Lakshmi Rai, Andrea Jeremiah, Premji Amaren, Mahat Raghavendra and Anjali (actress born 1986)|Anjali. It was produced by Dhayanidhi Alagiris Cloud Nine Movies  while Yuvan Shankar Raja composed the musical score and Mankatha (soundtrack)|soundtrack, with Sakthi Saravanan working as the cinematographer and the duo Praveen K. L. and N. B. Srikanth as editors. The story, set in Mumbai, revolves around a heist of cricket betting money, executed by a gang of four thieves, who are joined by a fifth unknown man, and its aftermaths.
 Telugu as Gambler and released in Andhra Pradesh  ten days later while it was a box-office hit in Kerala as well. 

==Plot== encounter killing faked his death in a secret mission to draw attention to the betting scandals and returns under the name Praveen Kumar. Arumuga Chettiar (Jayaprakash), an influential local illegal business dealer and the head of Faizal, owns "Golden Theatres" in Mumbai, which has been converted into a gambling den and forms the front for all his illegal businesses. Arumuga Chettiyar uses his links with dons in Mumbai and tries to route through his old theatre, a cash of over   to be used in betting. Vinayak is introduced to Arumuga Chettiyar through his girlfriend Sanjana (Trisha Krishnan|Trisha). Sanjana is in love with Vinayak, but Vinayak just pretends to love her.

Sumanth (Vaibhav Reddy), a goon working for Arumuga Chettiyar, hatches a conspiracy to rob the money in the company of his friends&nbsp;– Ganesh (Ashwin Kakumanu), a local Sub-Inspector, Mahat (Mahat Raghavendra), who owns a bar in Mumbai and Mahats friend Prem (Premji Amaren), an IIT graduate. Vinayak befriends the boys at Sumanths marriage with Suchithra (Anjali (actress born 1986)|Anjali). One late evening, Vinayak meets Prem, who becomes inebriated by him and reveals their heist plan. Vinayak starts spying on them and confronts them on the day of the planned heist, stepping in. The four, reluctantly take him in, promising him a fifth of the share. Vinayak, however, has other plans. He wants to kill his four accomplices and take the entire amount. He promises to help them and divide it between them. After looting the money they leave the money in an abandoned godown. Later, all of them celebrate the turn of events at Mahats bar. But Sumanth is identified at the party by Faizal and is later caught by him. Sumanth is cornered by Chettiar, who orders Faizal to kill him for his treachery but is rescued in time by Ganesh and Vinayak, and the trio escape from the hide-out taking Chettiar as hostage. While driving back to the godown, Vinayak finds Sanjana on the way and in front of her, he brutally shoves Chettiar out of the vehicle. Sanjana engulfs in grief when she comes to know about Vinayaks true intentions.
 approver and divulge everything. He is however killed when Prithvis wife Sabitha Prithviraj (Andrea Jeremiah) is kidnapped and threatened by Vinayak.

Vinayak comes to know of the whereabouts of Mahat and Prem through Ganesh and along with him, starts pursuing them. Prithvi and the others also follow suit and all of them are holed up in a highway resort with the money. Sequence of events lead to the murders of the gang members one by one—Mahat is killed by Sona who betrays him, Prem is killed by Prithvi who he mistakenly assumes was about to kill him, and Sona is killed by Vinayak—with Ganesh and Vinayak remaining alive. A final fight ensues between Vinayak and Prithvi. The two actually prove to be equal. At the final moment of the fight, Kamal throws a gun to Prithvi who shoots Vinayak, and a huge explosion rocks the shack seemingly ending the fight.

After several days, the police gets information about Ganesh to be living in Thailand. Kamal (under the name of Praveen Kumar) arrives there, but instead comes across Vinayak. Kamal confronts him and calls up Prithvi to inform him of Vinayaks presence, but then it is then revealed that Prithviraj and Vinayak are actually best friends since their college days and took police training together. They had come to know about the betting money scheme by Chettiar and operated the plan together (including Vinayaks fake death). Ganesh had also been killed by Vinayak as a part of the plan, and both Prithvi and Vinayak escaped the explosion with the  , each taking    as their share. Prithvi informs Vinanyak that their money was safe in the Bank of England, and asks him to deal with Kamal. Vinayak then snatches Praveens gun and points it at his forehead, saying "Game Over".


* Ajith Kumar as Vinayak Mahadevan (ex-police officer of Dharavi)
* Arjun Sarja as Prithviraj (the CBI officer
* Trisha Krishnan as Sanjana (Arumugam Chettiyars Daughter)
* Vaibhav Reddy as Sumanth Mankatha (DVD): clip from 2.36.24 to 2.37.00 
* Mahat Raghavendra as Mahat  (Leisure Bar owner)
* Premji Amaren as Prem  (Mahats Friend)
* Ashwin Kakumanu as Ganesh  (Sub-Inspector of Dharavi)
* Lakshmi Rai as Sona 
* Jayaprakash as Arumuga Chettiyar 
* Aravind Akash as Faizal  (Aarumuga Chettiyaars goon) Anjali as Suchitra (Sumanths wife)
* Andrea Jeremiah as Sabitha (Prithvirajs wife)
* Subbu Panchu Arunachalam as Kamal Ekambaram/Praveen Kumar 
* Vijay Vasanth in a cameo appearance
* Debi Dutta in a special appearance
* Kainaat Arora in a special appearance
* Rachel White in a special appearance
* Dipali Singh in a special appearance
 

==Production==
===Development=== 2010 season The Joker The Dark Knight. With the protagonist role in Mankatha incidentally being such a character, Ajith immediately accepted the role, turning the film into a high-profile production.     Prabhu further emphasised that the script had been altered due to Ajiths entry and he had incorporated "certain elements" that Ajiths fans would expect in a film and also said that "I approached Mankatha as a fan and asked myself how I would want to see Ajith sir on screen and then set out".    
 numerological reasons. Ocean Eleven Hindi film Jannat (film)|Jannat that was based on match fixing.  However, Dhayanidhi and Venkat Prabhu quickly denied the news and assured that Mankatha was original.  Upon completion of filming, Prabhu named it "his favourite film so far" and "close to my heart". 

===Casting=== Nandha later Jai in 2013 revealed that he was initially roped in to play the police officer role but that Venkat Prabhu replaced him with Arjun after Ajith Kumar became part of the project. 
 Sneha was reported to be added to the cast to be paired opposite Arjun,  however the role was later finalised with Andrea Jeremiah portraying that character.  She was also expected to perform a song for the soundtrack album. 
 Vijay would appear in a cameo role were dismissed by the producer, who clarified that Vijay Vasanth would appear in a pivotal role.  Concerning the film crew, Venkat Prabhu renewed his previous associations with his cousin Yuvan Shankar Raja, for the background score and soundtrack of Mankatha,  Sakthi Saravanan, who would handle the cinematography, and Praveen K. L., who along with N. B. Srikanth, would take care of the editing.  Vasuki Bhaskar and Kalyan remained the costume designer and the main choreographer, respectively, with Shoby joining the latter for a couple of songs, while Selva was assigned as the stunt coordinator. 

===Filming=== CGI special effects, was filmed in early November,  in a Chennai studio nearby the East Coast Road.     From 10 November onwards, the "introduction" song was shot for five days in Bangkok, Thailand, with Ajith Kumar, Lakshmi Rai and some foreigners participating.     

The films second schedule was planned to begin on 6 December 2010 in a studio in Chennai,  which was slightly delayed due to heavy rain,  and started couple of days later.  This led to speculation that the film had been shelved due to financial constraints, which was quickly denied by Venkat Prabhu.  During the schedule, all important stunt sequences were canned at Binny Mills in Perambur,   while simultaneously a grand set, resembling the Dharavi slum in Mumbai was erected in a Chennai studio.   Ajith Kumar also performed one of the action choreographies with the use of a body mounted camera, weighing around 30&nbsp;kg.    In late December, the third song, a "high-spirited peppy number", was shot for five days, with Shobi choreographing the steps.  An item number, titled "Machi, Open The Bottle", it featured actresses Debi Dutta and Kainaat Arora dancing to the song along with Ajith Kumar and the rest of the gang.   The schedule was wrapped up by early February,    with which approximately fifty per cent of the film was reportedly completed. 

The remaining part of the film was supposedly to be shot during the third and last schedule to be held in   portion was planned to be filmed at Madurai, which was considered as "apt" for the "action-oriented" sequence, but was eventually filmed in Chennai as well, while the remaining scenes were to be canned in Hyderabad, India|Hyderabad.  During the first week of June, Ajith had reportedly completed his portion, with his last day shoot being held in Hyderabad,  while sources confirmed that filming was still being carried on later that month in Hyderabad.  Shooting was further extended,  with the crew leaving for Bangkok again in late June for a ten-day schedule to shoot the pending scenes, including a lengthy fight sequence and a song, involving Premji and Lakshmi Rai.  Despite earlier announcement that Ajith had finished his portions, a "special scene" featuring Ajith in a different look was filmed on one day during the first week of July.  By 10 June 2011, the crew returned to Chennai and announced the completion of the entire filming. 
 CGI special dubbing for their characters,  including Trisha who on Venkat Prabhu insistence spoke dubbing for herself in the film,  which became only the third film to feature her original voice.  Rekhs, who had previously subtitled films including Enthiran and Vinnaithaandi Varuvaayaa, subtitled Mankatha during the first week of August,  while Yuvan Shankar Raja worked on the Re-recording (filmmaking)|re-recording,  being assisted by Premji.

===Marketing=== Naan Mahaan single track on 20 May 2011, creating positive media response.  

==Music==
  club mix, promotional track single track, "Vilaiyaadu Mankatha", was released in mid-May 2011.  The music rights were secured by Sony Music who had reportedly offered   10&nbsp;million.   The soundtrack album, following several postponements,    was released on 10 August 2011 at Radio Mirchis Chennai station,  while two days later the team arranged a press meet, showcasing two songs and the trailer of the film.  The album was reported to have achieved record breaking sales.  The songs received mixed response, with their placements in the film being criticizied, while the films score was widely appreciated.

==Release== Sun TV. 2011 Tamil DMK president M. Karunanidhi.   Cloud Nine Movies began negotiations with other production houses to sell the domestical theatrical rights, however talks with UTV Motion Pictures and Gemini Film Circuit resulted in failure.   On 22 August 2011, Gnanavelraja confirmed that his production house Studio Green, had purchased the Indian domestic theatrical and the television rights of the film at an undisclosed record amount.  
 opposition party, All India Anna Dravida Munnetra Kazhagam|AIADMK.  On 24 August, Azhagiri announced that Kalanidhi Marans Sun Pictures had bought the theatrical and satellite rights of the film and would distribute it along with Cloud Nine Movies.  Udhayanidhi Stalin was said to have negotiated the deal and united the production houses to release the film jointly.  Mankatha thus became the first Ajith Kumar film under Sun Pictures banner as well as their first release after the assembly election.  Actress Raadhika Sarathkumars Radaan Mediaworks distributed the film to Tamil Nadu theatres. 
 subtitles in English, simultaneously opening across Singapore, Malaysia, Sri Lanka, United Kingdom, the United States, Australia, Canada and many other parts of Middle East and Europe.  Noted Telugu producer Bellamkonda Suresh acquired the films dubbing rights by late August 2011 and released a dubbed Telugu version titled Gambler on 9 September 2011 across 225 screens, enabling the biggest opening for a dubbed version of a Tamil film.    The film became scheduled for a release on 1 September 2011 in order to cash in on the Vinayaka Chaturthi-Ramadan weekend,  before Ayngaran International eventually finalised 31 August 2011 as the release date in overseas theatres few days later.  In the United States, the film was released at 34 theatres. 

==Reception==

===Critical reception=== Outlook said, "The thing about testosterone is it can either excite or frustrate. There is no in-between. Tamil superstar Ajith’s much-anticipated 50th film is a very “male film”, no doubt. There’s a heist, a few chases, gunfights, cusswords, three good-looking women and a salt-and-pepper-haired protagonist, who is naughty at forty. Only, none of it excites." 

===Box office===
Mankatha released in 1000 screens worldwide and the film had a solo opening in Tamil Nadu on 31 August. It was said to have collected  252&nbsp;million nett from 370 screens in Tamil Nadu during the opening five-day weekend, and around  300&nbsp;million nett in its first week.  The film became the biggest grosser of the year as well as that of Ajiths career,    while also garnering the second-highest opening after Enthiran (2010).  In Chennai city alone, the film earned  27.2&nbsp;million in the first weekend from 19 screens. The multiplexes gave it the maximum number of shows including morning shows in all screens.    At the Mayajaal multiplex, Mankatha was screened in all 14 screens on the first day, resulting in 70 shows per day, all being sold out,  while Sathyam Cinemas reported a net of  3.4&nbsp;million from two screens for the five-day weekend.  The film grossed  65&nbsp;million in 19 days in Chennai.  The Telugu version Gambler, which released in 225 screens.  In Kerala, the film was released in the original language in Thiruvananthapuram and Palakkad districts on 31 August while a dubbed version released all over the state on 9 September, opening at first rank, outclassing other Malayalam releases. The film bought for  60 lakh in Kerala was expected to get distributor share of  14&nbsp;million.    The Telugu version got high opening compared to other mainstream films   It was successful at the Bangalore box office. 
 blockbuster as well as the years biggest commercial success.       The film completed a 50-day run at the box office and was ranked as the fourth biggest grosser in Tamil film history.   Brand merchandise related to the film were launched for sale after the 50th day. Items included sun glasses, T-shirts, hand cuffs and lockets, all on a limited edition basis. 

==Legacy==
Google Zeitgeist 2011, a compilation of the years most frequent search queries, placed Mankatha at 7th rank, becoming the only Tamil film to secure a place in the list.   Sudhish Kamath of The Hindu included Mankatha in his list "Year of Anti-Hero" stating that "Mankatha earns its place here simply because it took a fairly dark genre like noir and celebrated evil minus the darkness" and also went to wrote that "this comic heist film is the best thing Ajith has ever done since Billa". 

==Awards and nominations==
;Chennai Times Awards
* Best Actor – Ajith Kumar
* Best Youth Film – Dayanidhi Azhagiri
* Best Newcomer Male – Ashwin Kakumanu
* Best Negative Role Female – Lakshmi Rai

;59th Filmfare Awards South Best Tamil Film – Dayanidhi Azhagiri (Nominated) Best Tamil Director – Venkat Prabhu (Nominated) Best Tamil Actor – Ajith Kumar (Nominated) Best Tamil Supporting Actress – Lakshmi Rai (Nominated)

;Edison Awards (India)
* Best Debut Actor – Mahat Raghavendra
* Best Comedian – Premji Amaren
 International Tamil Film Awards (ITFA) Best Movie – Dayanidhi Azhagiri Best Director – Venkat Prabhu Best Supporting Actor – Premji Amaren Best Cinematographer – Sakthi Saravanan Best Female Playback Singer – Suchitra for "Vaada Bin Lada"

;Mirchi Music Awards South
* Best Upcoming Lyricist – Niranjan Bharathi for "Nee Naan"
* Technical Sound Engineer – Kumaraguru Paran for "Vilaiyaadu Mankatha"

;1st SIIMA Awards Best Tamil Actor – Ajith Kumar (Nominated) Best Actor in a Negative Role – Ajith Kumar (Nominated)
* Best Male Playback Singer – S. P. B. Charan for "Nee Naan" (Nominated)

;6th Vijay Awards Best Villain – Ajith Kumar Favorite Hero – Ajith Kumar Favorite Director – Venkat Prabhu Favourite Film – Mankatha (Nominated) Favourite Heroine – Trisha Krishnan (Nominated) Favourite Song – Yuvan Shankar Raja for "Vilaiyaadu Mankatha" (Nominated)

==Sequel==
Shortly after the films release and its high commercial success, Venkat Prabhu confirmed the possibility of a sequel, provided that Ajith Kumar accepted.  Sources also reported that Prabhu planned to use the initial script that he had penned before Ajith Kumar became part of the film.  In November 2013, Dhayanidhi said, "That people are still talking about Mankatha 2 shows the grand victory that Mankatha achieved when it released. People cant forget such a movie and I want to make sure that Brand Mankatha stays on in peoples minds. The sequel might happen in future but we havent started any discussions in this regard. Good news is that Venkat Prabhu has already readied the plot for the sequel. To me, Ajith signifies Mass in Tamil Nadu". 

==Remake==
A Hindi remake of the film has been confirmed by Gnanavel Raja of Studio Green. The cast and crew are yet to be finalised. 

==References==
 

==External links==
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 