The Flying Matchmaker
{{Infobox Film |
 name     = The Flying Matchmaker|
 image          =Shneikunileml.jpg|
 caption  =Shnei Kuni Leml DVD cover |
 director       =Israel Becker|
 writer         =Israel Becker Avraham Goldfaden (operetta) |
 starring       =Mike Burstyn (double role) Raphael Klatchkin Jermain Unikovsky Shmuel Rodensky Elisheva Michaeli Rina Ganor Aharon Meskin | Adam Greenberg Romulo Grounni|
 producer       =Mordecai Navon|
 released   = |
 runtime        =120 minutes |
 country = Israel |
 language =Yiddish, Hebrew 
}} musical directed Best Foreign Language Film at the 39th Academy Awards, but was not accepted as a nominee. 

==Plot== French teacher, Max. He arranges to show up in Kuni Lemels place, disguised as Kuni Lemel, so he can marry Ganor.  Confusion ensues as both Max and Kuni Lemel show up to court Ganor.

==Cast==
* Mike Burstyn as Max / Kuni Leml
* Raphael Klatchkin as Matchmaker
* Germaine Unikovsky as Matchmakers Daughter (as Jermain Unikovsky)
* Shmuel Rodensky as Rebbe Pinchas
* Elisheva Michaeli as Rebbe Pinchas Wife
* Rina Ganor as Rebbe Pinchass Daughter
* Aharon Meskin as Kuni Lemls Father
* Ari Kutai as Maxs Father

==See also==
* List of submissions to the 39th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  

 
 
 
 
 
 

 
 