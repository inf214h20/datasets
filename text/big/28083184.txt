Naalaaga Endaro
{{Infobox film
| name           = Naalaga Endaro
| image          =
| image_size     =
| caption        =
| director       = Eeranki Sharma
| producer       = 
| writer         = 
| narrator       = Narayana Rao Roopa 
| music          = M. S. Viswanathan
| cinematography = 
| editing        = 
| studio         =
| distributor    =
| released       = 1978
| runtime        =
| country        = India Telugu
| budget         =
}}

Naalaaga Endaro ( ) is 1978 Telugu film directed by Eeranki Sharma.

==Soundtrack==
* Anubhavalaku Adikavyam Aadadani Jeevitam (Lyrics: Acharya Atreya)
* Kalyanini... Kanulunna Manasuku Kanipinchu Roopanni (Lyrics: Acharya Atreya)

==Awards==
* The film won Nandi Award for Best Feature Film from Government of Andhra Pradesh in 1978.
* Hema Sunder won Nandi Award for Best Actor in this film. 
* S. P. Balasubrahmanyam won Nandi Award for Best Male Playback Singer in 1978 for the first time. 

==References==
 

 
 
 


 