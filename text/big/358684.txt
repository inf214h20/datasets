The Return of Jafar
 
{{Infobox film
| name        = The Return of Jafar
| image       = Returnofjafar.jpg
| caption     = VHS cover
| director    = {{Plainlist|
* Tad Stones
* Alan Zaslove }}
| producer    = {{Plainlist|
* Tad Stones
* Alan Zaslove }}
| writer      = {{Plainlist|
* Tad Stones
* Mark McCorkle
* Robert Schooley }}
| starring    = {{Plainlist|
* Scott Weinger Jonathan Freeman
* Gilbert Gottfried
* Dan Castellaneta
* Linda Larkin
* Jason Alexander }}
| studio      = Walt Disney Television Animation  Walt Disney Home Video
| released    = {{plainlist|
 
 
}}
| runtime     = 69 minutes
| country     = United States
| language    = English
| budget      = $3.5 million  - $5 million    
}} animated series. Culled from material originally intended for the first five episodes of the Aladdin TV series,    The Return of Jafar was the first Disney direct-to-video animated feature release. {{cite news|last=Breznican |first=Anthony |url=http://news.google.com/newspapers?nid=1916&dat=20020217&id=PockAAAAIBAJ&sjid=ZHUFAAAAIBAJ&pg=1102,2300694 |title=The Boy Who Never Grew Up Makes Comeback In Disneys Peter Pan Sequel
 |date=2002-02-17 |accessdate=2014-06-22 }}  Another Aladdin direct-to-video sequel, Aladdin and the King of Thieves, followed in 1996.
 Jafar escapes Aladdin and Iago (now turned against Jafar).

==Plot== Aladdin and List of Disneys Aladdin characters#Abu|Abu. Aladdin distributes the treasure amongst the poor of Agrabah - with the exception of a jewel flower, which Aladdin gives to Jasmine.
 Iago manages to dig himself and Jafar (Aladdin)|Jafars genie lamp out of the sand, where they were exiled by the Genie. Jafar orders Iago to release him, but Iago rebels against Jafar and throws the lamp into a nearby well. He returns to Agrabah, hoping to gain favor with Aladdin, in order to return to the palace. When he encounters Aladdin, his claim of being a slave under hypnosis does not fare well and is pursued. Aladdin runs into Abis Mal and his bandits, but is inadvertently rescued by Iago. Aladdin returns to the palace and jails Iago, promising to allow a fair trial. He and Jasmine are greeted by the Genie (Aladdin)|Genie, who has returned from seeing the world and is content with staying with his friends.

At a special dinner held in Aladdins honor, the Sultan announces that he wants to make Aladdin his new grand vizier. Trying to draw on the good mood, Aladdin attempts to persuade the Sultan to forgive Iago, but Iago inadvertently ruins the dinner when Rajah chases him into the room. The Sultan and Jasmine are furious at Aladdin, and Jasmine leaves the room heartbroken that Aladdin did not trust even her about this. With Iagos help, though, Jasmine eventually reconciles with Aladdin.

While Abis Mal is washing himself at a well, he finds Jafars lamp and when it is rubbed, Jafar appears as an abominable genie. Although bound by the laws of obedience, Jafar manipulates his master into wasting his first two wishes, but forms an alliance to the end of exacting revenge upon Aladdin. The pair travel to Agrabah, where Jafar reveals himself to Iago and coerces him into complying with his schemes. The next day, Aladdin and the Sultan depart to have a discussion about Iagos fate, while Jafar confronts the Genie and Abu in the Palace gardens and imprisons them.

When Aladdin makes good progress with his discussions with the sultan, he is ambushed by Abis Mal, who is supported by Jafars sorcery. The Sultan is kidnapped and Aladdin thrown into the raging river. Jafar frames Aladdin for the alleged murder of the sultan by leaving fake evidence and disguises himself as Jasmine to implicate Aladdin. Aladdin is thrown into the dungeon, to be executed by beheading come morning.
 Carpet engage Jafar in combat, but even when bound by the rules of the Genie, he easily outmatches them, using his tremendous powers to stop them from obtaining the lamp. His indiscriminate use of power opens a fissure in the ground which is filled with magma. Thoroughly trapped, Aladdin, Jasmine, the Genie, and Abu face certain death when suddenly Iago reappears and grabs the lamp. Jafar blasts him, leaving him for dead, but Iago manages to recover and uses his last ounce of strength to kick the lamp into the magma before apparently dying. The lamp melts and submerges, and Jafar is destroyed once and for all. Aladdin barely manages to rescue Iagos body and escape the fissure as Jafars power vanishes and everything is restored to normal. As Aladdin and the others mourn him, Iago is revealed to have survived, but barely, since genies cannot kill.

Iago is welcomed to the palace as a trusted friend and slowly recovers from his wounds. Aladdin announces to the Sultan that he is not yet ready to become a grand vizier, because he first wants to see the world. Jasmine declares that she will join him, but Iago objects to this and continues to rant as the film ends.

The following post-credits scene shows that Abis Mal, who survived the fall but is stuck in a tree, suddenly realizes that he will never have his third wish, because Jafar and the lamp are gone.

==Cast==
  Aladdin
** Brad Kane as Aladdin (singing voice) Jonathan Freeman Jafar
* Iago
* Genie
* Linda Larkin as Princess Jasmine
** Liz Callaway as Princess Jasmine (singing voice)
* Jason Alexander as Abis Mal
* Frank Welker as Abu and Rajah
* Val Bettin as Sultan
* Jim Cummings as Razoul

== Songs == Arabian Nights"
*"Im Looking Out for Me"
*"Nothing in the World (Quite Like a Friend)"
*"Forget About Love"
*"Youre Only Second Rate"

==Production== Aladdin and the other characters was another reason for a speedy follow-up.     Due to a well publicized bitter fall-out over the use of his voice in the marketing campaign for Aladdin, Robin Williams refused to reprise the role of the Genie (Aladdin)|Genie, and was instead replaced by Dan Castellaneta (best known for voicing Homer Simpson).  This was also the first Aladdin full-length production without the original voice of Sultan, Douglas Seale. He was replaced by Val Bettin, who also voiced the Sultan in the franchises animated series and in Aladdin and the King of Thieves.

==Reception==
Critical reception has been generally mixed to negative. Rotten Tomatoes, the film has an overall approval rating of 27% based on 11 reviews collected, with a weighted average score of 3.9/10. 

David Nusair of reelfilm.com summed up most of the negative feelings that contributed to this rating:

 
 Siskel & Ebert, the film received a "two thumbs up" from Gene Siskel and Roger Ebert.  Writing for Entertainment Weekly, Steve Daly graded the sequel a C- criticizing it as a "knockoff" that "carries the Disney label and costs about as much as a tape of Aladdin, but its clear from the first jerky frame that the same time, care, and creativity didnt go into it." 

===Home video=== Walt Disney Home Video Presents collection series.  In its first two days, it had sold more than 1.5 million VHS copies.  By June 1994, more than 4.6 million VHS copies were sold in less than a week.  Ultimately, more than ten million copies were sold ranking among the top 15 top-selling videos of all time (at the time), grossing $150 million in profits. 

The trailer for the film was seen on the 1994   ("placed back into the Disney Vault") on January 31, 2008 in the U.S., and February 4, 2008 in the U.K. 

==Adaptations==

===Comic===
When Disney was publishing their own comics in the mid-90s, they produced a two issue Aladdin comic presenting an alternate version of The Return of Jafar. It was titled The Return of Aladdin. The comic is introduced by the Merchant from the first movie.

The story starts off showing that Aladdin has been particularly bored of palace life. Meanwhile, Jafar has escaped the Cave of Wonders. Iago is given the task of finding the right master for Jafar to manipulate. Their search seems hopeless as some people are able to enjoy all three wishes or messing up.

They find someone to use the lamp, who is known as Isabella, a master magician. Isabella is similar in appearance to Jafar (except his clothing is green). His first wish is to return to Agrabah Palace (as he performed entertainment to the sultan in #1). His second wish is for an army of soldiers to pursue Aladdin and Jasmine when they catch on to Jafars presence. He is persuaded to use his third wish to trap Jafar and Iago in the lamp again, sending them back to the cave.

Due to persuasion by the Genie, the Sultan hires Isabella to a permanent entertainment job at the palace. The end of the story shows the merchant having a black lamp similar to Jafars, but he claims it to be worthless.

===Video game===
The plot of this film is loosely used in Agrabah, one of the worlds in Kingdom Hearts II, only with Abis Mal being replaced by the Peddler from the first film. As in the film, Iago escapes from Jafar and does his best to make amends with Aladdin and Jasmine, as well as with Sora, Donald and Goofy, although Jafar coerces him into aiding him in his revenge, almost damaging Iagos friendship with Aladdin and Sora, but he redeems himself after taking a blow for Aladdin which almost claims his life. The Peddler, at the beginning, comes across Jafars lamp, but sells it to Aladdin, Sora, Donald and Goofy for a rare artifact in the Cave of Wonders. Despite Aladdin sealing the lamp in the palace dungeon, the greedy Peddler breaks into the dungeon and frees Jafar, unleashing his fury on Agrabah until he is destroyed by Sora and company. The Peddlers fate is left ambiguous. This was the first Disney sequel to have its plot adapted into a level in the Kingdom Hearts series, which was then followed by the Grid being an adaptation of Tron Legacy.

Furthermore, there is a mild allusion to the Agrabah boss battle in  , and its PlayStation 2 remake. In both versions of Chain of Memories, the boss fight is due to the majority of the game being illusions created from Soras memories. A second playable character, Riku, also fights the boss in his mode.  This battle is once again visited in  .

==References==
 

==External links==
 
 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 