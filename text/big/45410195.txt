The Doctor and the Monkey
{{Infobox film
| name           = The Doctor and the Monkey
| image          = Le Savant et le Chimpanzé.jpg
| alt            = 
| caption        = A frame from the film
| film name      = 
| director       = Georges Méliès
| starring       = Georges Méliès
| studio         = Star Film Company
| distributor    =  
| released       =  
| runtime        = 20 meters (approx. 1.5 minutes at 12 frames per second|fps)
| country        = France
| language       = Silent
}}
 short silent silent comedy film directed by Georges Méliès.

==Plot== cross section design of the set allows the viewer to see both levels), a doctor is studying a caged monkey. When the doctor leaves for a moment, the monkey escapes from the cage and begins wreaking havoc on both floors. The monkeys tail detaches and takes on a life of its own, attaching itself to the doctors nose. Both the doctor and a housekeeper run about attempting to restore order, but the monkey continues to overturn furniture, and tears off the housekeepers skirt, leaving her in her pantalettes. As the short film ends, the whole house is in shambles.

==Production and release==
Méliès himself plays the doctor.  The double-decker set was reused later the same year in What Is Home Without the Boarder, in which Méliès plays one of the tenants. 

The film was released by Mélièss Star Film Company and is numbered 317 in its catalogues, where it was advertised as a scène comique.   

==Reception== Donkey Kong (1981).  Rae Beth Gordon, in a study of pathology in Mélièss work, comments that the film, with its antic monkey provoking equally antic behavior from a human, evokes themes of evolution and of humanitys "tendency to ape what we see" (tendance de singer ce quon voit).    Gordon also notes that monkeys appear in several other films by Méliès, including The Merry Frolics of Satan; their roles in these films, as imitators and bringers of chaos, closely resemble the similar functions of imps and demons in many of Mélièss comedies. 

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 