The Mountain Road
 
{{Infobox film
| name           = The Mountain Road
| image          = Mountain Road Poster.jpg
| image_size     = 
| caption        = Original film poster
| director       = Daniel Mann
| producer       = William Goetz
| based on       =   Alfred Hayes
| narrator       = 
| starring       = James Stewart Lisa Lu Glenn Corbett
| music          = Jerome Moross
| cinematography = Burnett Guffey
| editing        = Edward Curtiss
| distributor    = Columbia Pictures
| released       =  
| runtime        = 102 min, filmed in 1.85: 1 widescreen
| country        = United States English
| budget         = 
| gross          = 
}}
 OSS Major Frank Gleason Jr.,  who served as head of the demolition crew featured in the story and film.  Gleason was later hired as an (uncredited) technical consultant for the film. 

With his background as a World War II veteran, Stewart had vowed never to make a war movie, concerned they were hardly ever realistic.  The Mountain Road was the only war movie set during World War II in which he starred as a combatant. Stewart, however, had been featured in a wartime short, Winning Your Wings (1942) and in a civilian role in Malaya (film)|Malaya (1949). Harry Morgan said he believed that Stewart made an "exception for this film because it was definitely anti-war." 

==Plot== Alan Baxter) is reluctant to send Baldwin due to his inexperience as a commander, but relents. Baldwin, accompanied by reluctant conscripts, Sergeant Michaelson (Harry Morgan), Prince (Mike Kellin), Lewis (Eddie Firestone), Miller (Rudy Bond) and Collins (Glenn Corbett), the demolition teams translator, Baldwin finds out from Colonel Li (Leo Chen), the Chinese commander that the Japanese are about to capture a munitions dump. Colonel Kwan (Frank Silvera) is assigned to the team but before they can embark, Madame Sue-Mei Hung (Lisa Lu), the American-educated widow of a Chinese officer, joins them, with Baldwin gradually becoming attracted to the widow.  

Baldwin blows up a bridge and pushes a truck over a cliff to keep on pace, trying to reach the munitions dump before the Japanese. Sue-Mei and Baldwin are at odds over his cavalier treatment of the Chinese when he resorts to blowing up a mountain road, leaving thousands of local Chinese residents homeless. After stopping at a village because Miller is ill, Collins tries to give out the surplus food the team has brought, but is trampled to death by starving villagers. Baldwin is furious and resolute in trying to complete his mission, finally successful in blowing up the munitions storage, but when one of his trucks is stolen by Chinese bandits, Miller and Lewis are also killed. Baldwin exacts revenge by rolling a gas barrel into the bandits outpost and setting the village on fire. Baldwin asks Sue-Mei to understand why he had to act that way, but there is no reconciliation between them as the gulf of two divergent cultures is too great and she leaves him. Although recognizing his retribution was fundamentally excessive and brutal, Baldwin radios his report to headquarters, and is praised for fulfilling his mission.

 

==Cast==
Actor and screen credits:   Turner Classic Movies. Retrieved: June 10, 2012. 
* James Stewart as Major Baldwin
* Lisa Lu as Madame Su-Mei Hung (film debut)
* Glenn Corbett as Collins
* Harry Morgan as Sergeant Michaelson
* Mike Kellin as Prince 
* Rudy Bond as Miller
* Eddie Firestone as Lewis 
* Frank Silvera as Colonel Kwan
* James Best as Niergaard Alan Baxter as General Loomis
* Leo Chen as Colonel Li 
* P. C. Lee as Chinese general 
* Richard Tien Te Wang (uncredited)
* C. N. Hu (uncredited)

==Production==
Although the Japanese invaders were the feared antagonists, they never appear, as The Mountain Road diverges from the typical World War II actioner in dealing with a more sensitive sub-plot, delving into the cultural misunderstanding and racial prejudice between American soldiers and their Chinese allies. Whites original story had a serious message that stemmed from his extended sojourn in China, first as a freelance reporter in 1938 and shortly after as correspondent for Time  magazine. White found his stories depicting the corruption of the Nationalist government and warning of the growing threat of Communism being rewritten by Chinese government officials with the cooperation of editors at his magazine. When he left his post and returned to the United States, in 1946, White and colleague Annalee Jacoby, wrote a best-selling nonfiction book Thunder Out of China, describing the country in wartime. His follow-up novel, The Mountain Road also reflected his interest in a China in turmoil. 
 second banana" in films. Lisa Lu, who played "Madame Sue-Mei Hung", in her first major role,  recruited P. C. Lee, Leo Chen, Richard Wang and C. N. Hu, faculty members from the Chinese Mandarin Department, Army Language School, to appear in the film. Nixon, Rob.   Turner Classic Movies. Retrieved: June 10, 2012.    

Principal photography began on June 9, 1959 with location filming taking place at Arizona locations. The set for the Chinese village was erected on the Horse Mesa Dam Road, 40 miles east of Phoenix, Arizona|Phoenix; another set was erected in the vicinity of Superstition Mountain. The Fish Creek Hill Bridge on the Apache Trail was revamped to resemble the Chinese wooden bridge that is blown up in the action and the temple set, ammunition and supply station as well as the airfield were erected in Nogales, Arizona|Nogales. The battle scenes were filmed at the Columbia Ranch in Burbank, California. The extreme heat at the locations caused frequent cases of heat prostration among the cast and crew. Production wrapped on August 20, 1959.   Turner Classic Movies. Retrieved: June 10, 2012. 

==Reception==
Although a minor film in James Stewarts repertoire, The Mountain Road was received favorably, if considered somewhat puzzling. The New York Times reviewer, Howard Thompson noted, "Even with its final, philosophical overtones, this remains a curiously taciturn, dogged and matter-of-fact little picture—none too stimulating ... bluntly, and none too imaginatively."  Variety focused on Stewarts role, "As played by James Stewart, the American major holds the film together." 

White himself had mixed feelings about the film. In his memoirs he describes seeing it at a theater in Times Square where a group of teen-agers sitting behind him cheered the explosions and the "mad American destruction" of the village. Then their leader said "The hell with it. Thats the best part of the picture. The rest is crap." White wrote that he came to agree, saying that he had written the ending based on his experience as a reporter at the time, "refusing to acknowledge guilt in Asia. . . . " But by the time he wrote his memoirs, he had come to feel that the "reality of the twenty-five-year-long American record in Asia was that of genuine good will exercised in mass killing, a grisly irony which White could master neither in film nor book. Asia was a bloody place; we had no business there; novel and movie should have said just that at whatever risk." 

===Home media===
The Mountain Road is available on VHS tape (1.33 : 1 P&S), but not released on DVD. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
* Hyams, Jay. War Movies. New York: W.H. Smith Publishers, Inc., 1984. ISBN 978-0-8317-9304-3.
* Jones, Ken D., Arthur F. McClure and Alfred E. Twomey. The Films of James Stewart. New York: Castle Books, 1970.
* Munn, Michael. Jimmy Stewart: The Truth Behind The Legend. Fort Lee, New Jersey: Barricade Books, 2006. ISBN 978-1-5698-0310-3. 
* Pakkula, Hannah. The Last Empress: Madame Chiang Kai-Shek and the Birth of Modern China. London: Hachette UK, 2010. ISBN 978-1-4391-4893-8. 
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 