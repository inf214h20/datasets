Love In Kerala
{{Infobox film 
| name           = Love In Kerala
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = KP Kottarakkara
| writer         = KP Kottarakkara
| screenplay     = KP Kottarakkara Baby Padmini
| music          = MS Baburaj
| cinematography = CJ Mohan
| editing        = TR Sreenivasalu
| studio         = Ganesh Pictures
| distributor    = Ganesh Pictures
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, Baby Padmini in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Prem Nazir
*Adoor Bhasi
*Jose Prakash Baby Padmini
*Friend Ramaswamy
*K. P. Ummer
*N. Govindankutty

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme Mahaakaaliyamme || CO Anto, KP Udayabhanu || Sreekumaran Thampi || 
|-
| 2 || Athidhi Athidhi || S Janaki || Sreekumaran Thampi || 
|-
| 3 || Kudukuduthira Kummi || P. Leela, Kamala || Sreekumaran Thampi || 
|-
| 4 || Love in Kerala || LR Eeswari, Zero Babu, Archie Hutton || Sreekumaran Thampi || 
|-
| 5 || Madhupakarna Chundukalil || P Jayachandran, B Vasantha || Sreekumaran Thampi || 
|-
| 6 || Nooru Nooru Pularikal || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 7 || Om Namashivaaya   || Chorus, Jose Prakash || Sreekumaran Thampi || 
|-
| 8 || Premikkan Marannu || P. Leela, Mahalakshmi || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 