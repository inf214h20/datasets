Pretty Poison (film)
{{Infobox Film
| name           = Pretty Poison
| image          = Prettypoison1.jpg
| caption        = original film poster
| director       = Noel Black
| producer       = Lawrence Turman Marshal Backlar Noel Black
| based on       = Novel: Stephen Geller
| writer         = Lorenzo Semple Jr. John Randolph Dick ONeill
| music          = Johnny Mandel
| cinematography = David Quaid
| editing        = William Ziegler 20th Century Fox
| released       = 18 September 1968 (US)
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $1.3 million Producer Goes From Graduate to White Hope: Turman Bucks the Tide in Movie Turman
Warga, Wayne. Los Angeles Times (1923-Current File)   09 Nov 1969: p1. 
| preceded_by    = 
| followed_by    = 
}}

Pretty Poison (1968) is a psychological thriller/black comedy    film directed by Noel Black, starring Anthony Perkins and Tuesday Weld, about an ex-convict and high school cheerleader who commit a series of crimes.

While not generally considered an example of neo-noir, the film does include certain elements of the genre, including a femme fatale, a character trapped into circumstances beyond his control, criminal protagonists and, of course, murder.

The film was based on the novel She Let Him Continue by Stephen Geller; this was also the working title of the film. He, She and He---a Wow Triangle
Haber, Joyce. Los Angeles Times (1923-Current File)   07 Mar 1968: d19. 

There was a 1996 TV movie remake with the same title and plot.

==Plot==
Dennis Pitt is a disturbed young man on parole from a mental institution who becomes attracted to teenager Sue Ann Stepenek. He tells her that he is a secret agent, and takes her along on a series of "missions" that eventually end in murder. While Dennis is racked with guilt over both what he has done and what he has allowed to happen, Sue Ann is excited by the "adventure" and entreats Dennis to run away with her to Mexico. First, however, they have to get rid of her disapproving mother.
 prison for life.  Dennis is more than happy to be locked up, as it keeps him away from Sue Ann, of whom he is now quite frightened.  While Dennis refuses to tell his skeptical parole officer Azenauer the truth, he asks him to "see what Sue Ann is up to" in hopes she will be exposed for what she really is. The film ends with Sue Ann meeting a young man and lamenting to him that the people who took her in after her mothers death wont let her stay out late; it is implied that she will use and destroy him just as she did Dennis. But Dennis parole officer is indeed watching as she departs with her latest victim.

==Production==
The novel She Let Him Continue was published in 1966. Readers: Report
By MARTIN LEVIN. New York Times (1923-Current file)   06 Mar 1966: BR26.  The Los Angeles Times called it "an interesting if not overly impressive debut." An Outsider in the Great Society
Gold, Irwin. Los Angeles Times (1923-Current File)   15 Mar 1966: c5. 

The novel was optioned for the movies. Lawrence Turman agreed to act as executive producer for producer Marshall Backlar and director Noel Black, who had just made the short Skaterdater together. Skaterdater had been made for $17,000 and sold to United Artists for $50,000, winning the Palm dOr for Beat Short Film at the 1966 Cannes Film Festival, and being nominated for an Oscar for Best Short Film.   accessed 22 Feb 2015 

Turman described the novel as "a hip horror story about todays alienated youth." Accent On (Troubled) Youth: JUNIOR LEAGUE DEBUT SWITCH
By A.H. WEILER. New York Times (1923-Current file)   14 May 1967: D11.  Black said it was about "a Walter Mitty type who comes up against a teenybopper Lady Macbeth." 

Turman had just made The Flim Flam Man for 20th Century Fox and obtained finance from the same studio. Lorenzo Semple Jnr was hired to write the screenplay. Turman Continue Producer
Martin, Betty. Los Angeles Times (1923-Current File)   17 May 1967: d14.  Turman says Semple turned in a "magnificent script in six weeks." 

Tuesday Weld signed to play the female lead with Antony Perkins as costar. Psychotic Role for Tuesday
Martin, Betty. Los Angeles Times (1923-Current File)   16 June 1967: c13.  Continue Role for Perkins
Martin, Betty. Los Angeles Times (1923-Current File)   11 Aug 1967: d18.  It was the first film Perkins made in Hollywood since Psycho. A PERSONAL REVOLUTION: Anthony Perkins Trying to Mature Boyish Image ANTHONY PERKINS
Thomas, Kevin. Los Angeles Times (1923-Current File)   20 Dec 1967: c1. 
 Friendly Persuasion and Fear Strikes Out, not Psycho (1960 film)|Psycho, although commentators naturally made the comparison between Norman Bates and the character in Pretty Poison.” 

The film was shot on location at Great Barrington, Massachusetts. Premiere Slated for Pretty Poison
Los Angeles Times (1923-Current File)   19 Aug 1968: f29. 

Filming was difficult, with problems between Weld and Black. Actor John Randolph recalled that, "Noel knew how to set up shots, but he knew nothing about acting. Tuesday Weld was neurotic as hell. She would break down and cry. She hated the director, and she permitted that hatred to color everything she did." 

In 1971 Weld would call the movie her least favourite:
 The least creative experience I ever had. Constant hate, turmoil and dissonance. Not a day went by without a fight. Noel Black, the director, would come up to me before a scene and say, Think about Coca Cola. I finally said, Look, just give the directions to Tony Perkins and hell interpret for me. Tuesdays got her dukes up: Watch out, Tuesdays got her dukes up
Reed, Rex. Chicago Tribune (1963-Current file)   31 Oct 1971: r7.  
Black latre reflected:
 Tuesday and Tony got on professionally, though she probably resented how much more in tune he was with me than she was. He was the quintessential professional. Even though he had made 20 or so movies and this was my first, he listened to everything I had to tell him. What he brought was a personal sense of humanity and dignity, which gave the character a sympathetic quality.  
Beverly Garland was cast as Welds mother. "I loved the part," she said later. "I felt it was one of the best things Id done... I thought it was a great movie, well directed. Noel Black did a great job. But the studios got very upset with him because it took a long while to shoot. Studio people kept arriving and saying, Youre taking too long and they had him under a lot of pressure... Noel had some wonderful ideas and some camera stuff that took time. He went to great pains with that movie and the studio got very upset with him. But I think the movie shows that he took the time." 

==Reception==
Fox had difficulty securing a release for the film in New York so instead opened it in Los Angeles in September 1968. The Los Angeles Times reviewer called it "a small, stunning, thoughtful exploration of degrees of madness and of sanity." Poison in Premiere at Vogue: Pretty Poison at the Vogue Theater
Champlin, Charles. Los Angeles Times (1923-Current File)   20 Sep 1968: c1. 

However the film flopped. When Fox did secure a New York release, they claimed they  had trouble getting critics to attend a screening, and persuading the two stars to promote the movie. 

Black later claimed, "their unwarrantable action was partly explained by it being the year   of the double assassination of Robert Kennedy and Martin Luther King, and a corrosive tale of insidious madness in which a teenage girl shoots her own mother, seemed, to timid studio chiefs, excessive.” 

The film opened in New York on 23 October, to poor reviews from the daily papers. The New York Times claimed that "everything that was spare in the novel somehow becomes overblown" and that while Perkins "is quite good... Tuesday Weld is numbingly dull as the girl." Pretty Poison
V.C.. New York Times (1923-Current file)   24 Oct 1968: 55 

It received excellent reviews from magazines and Sunday papers - notably Pauline Kael in The New Yorker - but that was not enough to save the film commercially. Film Students Seek to Learn, Not Emulate: Noel Black Talks to Boston U. Seminar About Directing His Pretty Poison Elicits a Variety of Reactions
By ROBERT REINHOLDSpecial to The New York Times. New York Times (1923-Current file)   13 Feb 1969: 50.  A return engagement in Los Angeles in December was not successful. Citywide Engagement for Pretty Poison
Los Angeles Times (1923-Current File)   12 Dec 1968: f35. 

"One of the best films of 1968 remains a pleasant memory for the few of us lucky enough to see it," wrote Rex Reed, who thought it was "an offbeat, original, totally irreverent examination of violence in America, refreshing in its subtlety and intelligent in its delivery." Remember My Forgotten Movie
By REX REED. New York Times (1923-Current file)   02 Feb 1969: D13. 

Gene Siskel of the Chicago Tribune named the film as one of the ten best of the year. MOVIES: 1969s ten best movies--from Z to B & C & T & A
Siskel, Gene. Chicago Tribune (1963-Current file)   02 Jan 1970: a1 

Lawrence Turman later said "the critics, of all people, rescued Pretty Poison by continually writing about it. Its easy to second guess a studio and it doesnt help. What matters is that the film came off the map from being nothing and nowhere to finding its audience. Its a special film. Im very proud of it, but like all films it didnt satisfy all my dreams. The direct cost was $1.3 million and I think well turn the corner on the television sale." 

"I dont care if critics like it; I hated it," said Weld. "I cant like or be objective about films I had a terrible time doing." 

==Awards and honors==
* 1968 New York Film Critics Circle Award, Best Screenplay - Lorenzo Semple Jr. Critics Name Lion in Winter Best Film by 13-11
By A. H. WEILER. New York Times (1923-Current file)   31 Dec 1968: 20. 

==References==
 

==External links==
* 
* 
* 
*  at TCMDB
 

 
 
 
 
 
 
 
 