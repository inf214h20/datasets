BABO
 
 
{{Infobox film
| name           = BA:BO
| image          = BABO film poster.jpg
| film name      = {{Film name
 | hangul         =  
 | rr             = Babo
 | mr             = Pabo}}
| director       = Kim Jeong-kwon
| producer       = 
| based on       =  
| starring       = Cha Tae-hyun Ha Ji-won
| music          = 
| cinematography = 
| editing        = 
| distributor    = CJ Entertainment
| released       =  
| runtime        = 98 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = $6,453,485 
}}
BA:BO (Hangul|바보) is a 2008 South Korean film. Based on a popular webcomic, the film was directed by Kim Jeong-kwon, and stars Cha Tae-hyun and Ha Ji-won in the lead roles.

== Plot ==
Ji-ho is a promising pianist who has been studying and playing abroad for years, but her career takes a blow when she is struck with stage fright. Returning home, Ji-ho is reunited with her old school friend, Seung-ryong, who, although now in his twenties, has been left with the mind of a six-year-old following an accident.

Seung-ryongs only family is his younger sister, Ji-in, and he takes care of her by trying to sell toast outside her school, much to her embarrassment. Ji-in later becomes ill, and Seung-ryongs other friend, Sang-soo, falls into trouble with some gangsters. With all of his friends and family facing problems, Seung-ryong becomes an unlikely saviour.

== Background ==
BA:BO was adapted from a popular webcomic of the same name created by Kang Full, which ran from January to April 2004. Director Kim Jeong-kwon, himself a fan of the comic, was approached directly by Kang, and described the film as being faithful to the source material. Lee Hyo-won,  , The Korea Times, January 29, 2008. Retrieved on April 9, 2008.  

== Cast ==
* Cha Tae-hyun as Seung-ryong. A fan of the original comic, Tae-hyun stated at a press conference that he "cried   eyes out" when reading it. Rather than draw his inspiration from mentally challenged people in reality or as portrayed in other media, he felt it necessary to play the character as written in the comic. Cha gained eight kilogrammes during production, and was unable to lose all of that weight for his wedding in 2006. 
** Seo Dae-han as young Seung-ryong.
* Ha Ji-won as Ji-ho. Playing the role of a frustrated pianist, Ji-won performed on the piano herself for the film. Although she had studied piano as a child, she received tutoring from singer/songwriter No Young-shim, who also taught her subtle gestures and postures.  Choi Sulli as young Ji-ho.
* Park Hee-soon as Sang-soo.
* Park Ha-sun as Ji-in.
* Park Grina as Hee-yeong.
* Jeon Mi-seon as young Seung-ryongs mother.
* Lee Ki-young as "Small Star" president.
* Lee Sang-hoon as junk seller.

== Box office ==
BA:BO was released on February 28, 2008,  , HanCinema. Retrieved on April 8, 2008.  and was ranked third at the box office on its opening weekend, grossing United states dollar|$2,302,058.  By April 6 the film had grossed a total of $6,450,178,  and as of March 23 the total number of tickets sold was 951,573. 

== References ==
 

== External links ==
*  
*   at HanCinema

 
 
 
 
 
 