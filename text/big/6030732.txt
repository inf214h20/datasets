Runaway (2005 film)
Runaway Tim McCann. run away from home and attempt to start a new life because of a pedophile father.
Michael, who had been previously molested by his father, tries to protect his brother from their dad.

Runaway was written by Bill True and won the Best Narrative Feature award at the 2005 Austin Film Festival.   

The film was produced by Alan Klingenstein.   

==Credits==
*Aaron Stanford - Michael Adler 
*Robin Tunney - Carly 
*Phillip Blancero - Customer in Mos 
*Karla Cary - Customer in Mos 
*Anthony Corrado - Motel Manager 
*Jennifer Dempster - News Reporter 
*Tanya Ferro - Police Officer 
*Michael Gaston - Jesse Adler 
*Peter Gerety - Mo
*Jason Gervais - Police Officer 
*Robert Grcywa - Bert 
*James W. Harrison III - Quarrelling Meth Addict 
*Andrew D. Jones - Police Officer 
*Ray Kennedy - Police Officer 
*Terry Kinney - Dr. Maxim 
*Brian Kozloski - Police Officer 
*Melissa Leo - Lisa Adler 
*Patrick McCulloch - Police Officer 
*Smokey Nelson - Quarreling Meth Addict 
*Zack Savage - Dylan 
*Bill Wolff - Eyewitness on TV
*Danny Waer - Police Officer

==References==
 

==External links==
*  

 
 
 


 