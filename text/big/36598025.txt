The Big Wedding
 
{{Infobox film
| name           = The Big Wedding
| image          = The Big Wedding Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Justin Zackham
| producer       = Justin Zackham Anthony Katagas Clay Pecorin Richard Salvatore Harry J. Ufland
| screenplay     = Justin Zackham
| based on       =   Ben Barnes Susan Sarandon Robin Williams
| music          = Nathan Barr
| cinematography = Jonathan Brown
| editing        = Jon Corn
| studio         = Two Ton Films Millennium Films
| distributor    = Lionsgate   
| released       =  
| runtime        = 89 minutes  
| country        = United States
| language       = English
| budget         = $32 million     , Box Office Mojo 
| gross          = $35,770,721 
}}
The Big Wedding is a 2013 American comedy film written and directed by Justin Zackham. It is an American remake of the original 2006 Swiss/French film   (My Brother is Getting Married), written by Jean-Stéphane Bron and Karine Sudan.
 Ben Barnes, Susan Sarandon, and Robin Williams. It was released on April 26, 2013 by Lionsgate in the United States and Canada. 

==Plot==
Don and Ellie Griffin were a New England couple married for twenty years before they divorced. They have three children from their marriage – Lyla, Jared, and adopted son Alejandro, who was from Colombia.

In preparation for Alejandros wedding, Ellie arrives at Dons (and her old) home, and lets herself in. She interrupts Don just as he is about to perform cunnilingus on Bebe, his girlfriend of eight years (and Ellies former best friend). All are embarrassed, but they make small talk, and he shows Ellie to her room. Meanwhile, Alejandro and his fiancée Missy are meeting with Father Moinighan, the priest who will be marrying them. It is revealed that Alejandros biological mother Madonna is going to be coming from Colombia to the wedding, which upsets Alejandro since he does not have a "traditional" family, and his Catholic mother would not approve of that or the fact that Don and Ellie had been divorced.

Lyla, who reveals she is separated, goes to the hospital and sees her 29-year-old brother Jared, an Obstetrics and gynaecology|OB, after passing out. They talk and it is revealed that he is a virgin and is waiting for the "right one". Back at home, Alejandro tells Ellie about his mother coming to the wedding. Explaining that since she is a devout Catholic and doesnt believe in divorce, Alejandro asks Ellie and Don to pretend to be married for the next three days. Hearing this, Bebe becomes upset and leaves the house, clearly upset at Don. Alejandros mother arrives with his biological 20-year-old sister, Nuria. Later, Nuria flirts with Jared, after she brazenly strips naked to skinnydip in the familys lake as he watches.

That evening, the family goes out to dinner with Missy and her parents Muffin and Barry, and Bebe shows up as their waitress, which surprises everyone. Meanwhile, Nuria starts fondling Jared under the table, and Ellie sees Nuria giving Jared a handjob. She brings Nuria to the restroom for a chat, telling her that American women behave differently with men. When they arrive home, Jared tells Nuria he wants to have sex, as she had suggested earlier, but she tells him "No", asking him instead to do romantic things for her such as read her poetry. Don and Ellie, meanwhile, end up having sex after Ellie, still pretending to be Dons wife, sleeps in Dons room.

Ellie and Madonna go for a walk in the woods and talk. Neither understands the others language, though they think they are communicating on some level. At the same time, Don and Lyla talk privately and Lyla reveals she is pregnant. On the wedding day, before the ceremony, Don tells Bebe he had sex with Ellie. Bebe says she forgives them but then punches him in the face. She also reveals that Ellie cheated on Don with Missys father before Don cheated on her. Muffin says that she knows about Ellie and Barry, and tells them that she is bisexual, implying that she is interested in a sexual affair with both Bebe and Ellie. Meanwhile, Missy and Alejandro have decided to get married on the family dock to escape the chaos. The family runs after them and, after some of them fall into the lake, the wedding reception is on.

During the reception, Jared goes upstairs to talk to Nuria, who tells him she has decided to no longer follow Ellies advice (about not being so available sexually), and they sleep together. Back at the reception, Ellie and Bebe have made up. Don surprises Bebe by proposing to her, and they get married on the spot. Lylas husband Andrew arrives at the wedding and, upon finding out that Lyla is pregnant, reconciles with her. Alejandros mother realizes she has been lied to about his family and he runs after her as she starts to leave. But she reveals that her own past was not as pure as he thought, that she too had lied to protect him, and she forgives him.

Time passes and it is revealed that Lyla has had a daughter named Jane, as Don attaches a plaque with her name to their family tree.

==Cast==
* Robert De Niro as Donald Robert "Don" Griffin
* Susan Sarandon as Beatrice Martha "Bebe" McBride
* Diane Keaton as Ellie Griffin
* Katherine Heigl as Lyla Griffin
* Robin Williams as Father Moinighan
* Amanda Seyfried as Melissa "Missy" OConnor
* Topher Grace as Jared Griffin Ben Barnes as Alejandro Soto Griffin
* Christine Ebersole as Muffin OConnor
* Patricia Rae as Madonna Soto
* Ana Ayora as Nuria Soto
* Kyle Bornheimer as Andrew
* David Rasche as Barry OConnor
* Megan Ketch as Jane

==Production==
The film was previously titled The Wedding. It is an American remake of the original 2006 French film   (My brother is getting married) written by Jean-Stéphane Bron and Karine Sudan.

Principal photography took place in Greenwich, Connecticut. Locations included St. Agnes Church (Greenwich, Connecticut)|St. Agnes Church in Greenwich, Christ Church Greenwich, Gabriele’s Italian Steakhouse, and a private home in the town’s Stanwich section. 

==Reception==
The film received largely negative reviews. The Big Wedding holds a 7% rating on Rotten Tomatoes based on 96 reviews with the sites consensus stating that its "all-star cast is stranded in a contrived, strained plot that features broad stabs at humor but few laughs."  On Metacritic, the film scores 28% based on reviews from 32 critics.  One observer pointed out that The Big Weddings reviews are among the worst of the year.  Lou Lumenick of the New York Post wrote, "Id rather gouge my eyes out with hot spoons! De Niro exclaims at one point. Im not sure exactly what he was talking about, but Id like to think it referred to the prospect of being forced to watch The Big Wedding." 

According to business outlets, the movie "was a massive flop at theaters", even though it recouped its production costs by a few million dollars.  It opened with a $7.5 million at 2,633 North American locations, leading one observer to state, "Expect domestic exhibitors to file for divorce in the very near future."  Another commentator stated that "Theres little reason to suspect it will stick around any longer than theater owners are contractually obligated to keep it."  , The Numbers, April 30, 2013. 

Heigl was nominated for  . 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 