Immortal Beloved (film)
 
 
{{Infobox film
| name           = Immortal Beloved
| image          = Immortal beloved film.jpg
| caption        = Immortal Beloved theatrical poster Bernard Rose
| producer       =  Stephen McEveety    Bruce Davey
| writer         = Bernard Rose
| starring       = Gary Oldman    Jeroen Krabbé
| music          = George Fenton   Ludwig van Beethoven   Gioacchino Rossini 
| studio         = Icon Productions
| distributor    = Columbia Pictures
| released       = December 16, 1994
| runtime        = 121 minutes
| country        = United Kingdom United States Hungarian
| gross          = $9,914,409   
}}
Immortal Beloved is a 1994 film about the life of composer Ludwig van Beethoven (played by Gary Oldman). The story follows Beethovens secretary and first biographer Anton Schindler (Jeroen Krabbé) as he attempts to ascertain the true identity of the Unsterbliche Geliebte (Immortal Beloved) addressed in three letters found in the late composers private papers. Schindler journeys throughout the Austrian Empire interviewing women who might be potential candidates as well as through Beethovens own tumultuous life.

==Plot summary== Johanna Reiss, the daughter of Anton Van Reiss, a prosperous Viennese upholsterer. In the film, she becomes pregnant by Beethoven; when by an accidental turn of events he does not marry her in time, she marries his brother, Kaspar Anton Karl van Beethoven|Kaspar. Their son, Karl van Beethoven, is raised by Ludwig in the vain hope of making him an important musician in his own right.

== Cast ==
*Gary Oldman as Ludwig van Beethoven 
*Jeroen Krabbé as Anton Felix Schindler 
*Isabella Rossellini as Anna-Marie Erdödy  Johanna Reiss Kaspar van Beethoven 
*Marco Hofschneider as Karl van Beethoven 
*Miriam Margolyes as Nanette Streicherova  Clemens Metternich 
*Valeria Golino as Giulietta Guicciardi
*Alexandra Pigg as Therese Obermayer Josephine von Brunsvik Theresa von Brunsvik

==Production==
 
Filming took place in the Czech cities of Prague, Kroměříž on the Milotice chateau and Buchlovice chateau, and the Zentralfriedhof in Vienna, Austria between May 23 and July 29, 1994.

While the soundtrack utilizes modern instruments, the pianos that appear in the film actually date back to Beethovens time.

==Historical background==
 

After Beethovens death in 1827, a three-part letter was found among his private papers addressed to a woman whom he called "immortal beloved". Written in the summer of 1812 from the spa town of Teplice, the letter has generated a great deal of speculation and debate amongst scholars and writers as to her identity. Among the candidates are (or were) Giulietta Guicciardi, Thérèse von Brunswick, Josephine Brunsvik, Antonie Brentano, and Anna-Marie Erdödy (some of whom appear in the film).

The films writer and director,   and Beethoven." The Musical Quarterly 81/2, 1997, pp.&nbsp;190–198.

==Music==
*The Orchestra: London Symphony Orchestra, conducted by Georg Solti

*Instrumental soloists
**Murray Perahia
(in order of appearance)

* Symphony No 5, Op. 67.
* Für Elise (complete).
* Symphony No 3 In E-Flat Major Op.55 Eroica.
* Piano Sonata No 14, Moonlight: Adagio Sostenuto.
* Symphony No 6, Op. 68, Pastoral: Storm.
* Piano Trio No 5 In D Major, Op. 70, No 1 Ghost.
* Violin Concerto in D Major, Op. 61.
* Piano Sonata No 8, Pathetique.
* Piano Concerto No 5, Emperor (love theme, ending credits).
* Missa Solemnis: Kyrie.
* Symphony No 7, Op. 92: Allegretto (Karls theme)
* Violin Sonata In A Major, Op. 47, Kreutzer: Adagio sostenuto- Presto.
* Symphony No 9, Op 125: Ode to Joy.

==Reception==

===Critical reaction===
Reviews for Immortal Beloved were mixed. From the 31 reviews collected from notable publications by review aggregator   also offered a positive review, stating: "thanks to its hugely effective use of Beethovens most thrilling, tumultuous music, this film exerts much the same hypnotic power". She praised the performance of Oldman, writing that "he captures Beethoven as a believably brilliant figure struggling with his deafness and other demons". 
 Oscar nomination."  Also that year Josh Winning of Total Film named Oldmans portrayal of Beethoven as one of the five best performances of his career, saying: "If ever there was a better filmic chameleon than Oldman, weve yet to find one. Immersing himself fully into the role of the German composer, Oldman is here damn near unrecognisable." 

===Box Office===
The movie debuted strongly  and was a modest success, generating $9,914,409 in a domestic-only release. 

== References ==
 

== External links ==
 
*  
*   filming locations on Movieloci.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 