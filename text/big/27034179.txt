Unchained Memories
{{Infobox film
| name = Unchained Memories
| director = Ed Bell and Thomas Lennon
| executive producer = Donna Brown Guillaume
| co-producer = Juliet Weber
| writer = Mark Jonathan Harris
| genre = Documentary
| awards = Nominated 4 Primetime Emmys. 1 Black Reel
| narrator = Whoopi Goldberg
| music engineer = Corey Folta
| music editor = John Davis
| distributor = HBO
| rated = NR
| released = 2003
| runtime = 75 Min
| country = United States
| language = English   
}}
 WPA Slave Narrative Collection. This HBO film interpretation directed by Ed Bell and Thomas Lennon    is a compilation of slave narratives, narrated by actors, emulating the original conversation with the interviewer.  The slave narratives may be the most accurate in terms of the everyday activities of the enslaved, serving as personal memoirs of more than two thousand former slaves.  The documentary depicts the emotions of the slaves and what they endured.  The "Master" had the opportunity to sell, trade, or kill the enslaved, for retribution should one slave not obey.

==History== WPA to Federal Writers Project involved interviews with thousands of former slaves in 17 states.  The oral history interview project yielded an extraordinary set of 2,300 autobiographical documents known as the Slave Narrative Collection.  What emerges from these documents are pictures of living standards, the daily chores, and long days, were brought to light along with the stories of the good and bad "Master."  The  brutality, torture, and abuse under slavery are themes in the interviews. 

After the Civil War ended in 1865, more than four million slaves were set free.    The main objectives were to inform the public, and describe the history and life of the former slaves.  More than 2,000 slave narratives, along with 500 photos are available online at the   as part of the "Born in Slavery" project.

==Slaves and readers==
*Narrated by: Whoopi Goldberg
*Sarah Gudger, North Carolina (Vol.11)—Read by: LaTanya Richardson
*Charley Williams, Oklahoma (Vol 13th) –Read by: Ruben Santiago-Hudson
*Martin Jackson, Texas (Vol 16) - Read by: Robert Guillaume
*Henry Coleman, South Carolina (Vol 14) – Read by:Roscoe Lee Browne
*Jennie Proctor, Texas (Vol 16) – Read by: Oprah Winfrey
*Elizabeth Sparks, Virginia (Vol 17) – Read by: Angela Bassett
*Rosa Starke, South Carolina (Vol 14) --
*Cato Carter, Texas (Vol 16) – Read by: Roger Guenveur Smith
*Mary Reynolds, Texas (Vol 16) – Read by: Angela Bassett
*Rev. Ishrael Massie, Virginia –
*Fannie Berry, Virginia – Read by: CCH Pounder
*Mary Estes Peters, Arkansas (Vol 2) – Read by : Jasmine Guy
*Sarah Ashley, Texas (Vol 16) – Read by: LaTanya Richardson
*Charles Grandy, Virginia—Read by: Ruben Santiago-Hudson
*William Colbert, Alabama (Vol 1) – Read by: Courtney B. Vance
*Katie Darling, Texas (Vol 16) – Read by: Jasmine Guy Jasmine Guy
*Vinnie Brunson, Texas (Vol 3) – Read by: Vanessa L. Williams
*Fannie Berry, Read by: CCH Pounder
*Jack & Rosa Maddox, Texas (Vol 7) – Read by: Ossie Davis and Ruby Dee
*Mary Reynolds, Texas (Vol 16) – Read by: Angela Bassett
*Louisa Adams, North Carolina (Vol 11) – Read by: LaTanya Richardson

==Production==
All that follow beneath are from the credits in Unchained Memories
 Thomas Lennon
*Co-Producer and edited by: Juliet Weber
*Executive Producer: Donna Brown Guillaume Thomas Lennon
*Written by: Mark Jonathan Harris
*Historical Advisors:
**Ira Berlin (Senior Advisor)
**Henry Louis Gates, Jr.
**James Oliver Horton
**Charles L. Perdue
**Margaret Washington

==Music and sound==
All that follow beneath are from the Internet Movie Database

*Music Engineer: Corey Folta 
*Music Composer: William "Spaceman" Patterson
*Sound Editors: Ira Spiegel, Marlena Grzaslewicz 
*Assistant Sound Editor: Mariusz Glabinski 
*Music Editor: John M. Davis 
*Sound Mixers: Peter Waggoner, Sound One 
*Sound: Doug Donald, Charles Hunt, Jim Mansen, Bruce Perlman, Michael Riley, Sasumo Tokunaw, Anna Delanzo 
*People in total who were in charge of the sound of Unchained Memories were Doug Donald, Charles Hunt, Jim Mansen, Bruce Perlman, Michael Riley, Sasumo Tokunaw, Anna Delanzo 
*Harpo Productions Audio Director- JR Chappell, Audio Assistant- Stacey Hempel 
*Choral Performers- Sound- Jim Hawkins, Steve Smith, Dennis Towns 

===Musical selections used in Unchained Memories===
All that follow beneath are from the credits in Unchained Memories

*"No More" - Performed by The Blind Boys of Alabama. Real World Records 
*"O Day" - Performed by Bessie Jones and Alan Lomax. Rounder Records 
*"Early in The Mornin"- Arranged by "22" and Alan Lomax. Performed by Prisoners 22, Little Red, Tangle Eye, and Hard Hair, Rounder Records 
*"Im Goin Up North" Traditional, Performed by the Children of East York School Folkways Recordings 
*"Hard Times in Ol Virginia" Arranged by John Davis and Alan Lomax, Performed by John Davis. Rounder Records  
*"Enter Mozelles House", "Another Dead Snake", "Mozelle", "In The Mirror", "Mozelles Theme", "Carriage House", Original musical score from "Eves Bayou" Composed and Performed by Terence Blanchard, Courtesy of Lions Gate Entertainment Group 
*"Adam & Eve", "Wade The Water To My Knee", "Move Daniel", "Blow Gabriel" 
*Traditional African-American ring shouts, Performed and Arranged by The McIntosh County Shouters 

==Photography==
All that follow beneath are from the credits in Unchained Memories

*Photo Animation: Cort Tramontin 
*Design/Art Direction: Clive Helfet 
*Camera: Wes Dorman, Chris Hall, Richard Mort, Joe Pausabeck 

==See also==
*List of films featuring slavery

==References==
 
 

==Further reading==
*A sketch of the laws relating to slavery in the several states of the United States of America. Stroud, George M. (George McDowell), 1795-187
*An Introduction to the WPA Slave Narratives. Yetman, Norman R.
*When I Was a Slave: Memoirs from the Slave Narrative Collection. Yetman, Norman R.

==External links==
* 
* 
* 
 

 
 
 
 