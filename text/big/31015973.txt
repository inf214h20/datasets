Bad Night for the Blues
{{Infobox film
| name = Bad Night for the Blues
| image =
| director = Chris Shepherd
| producer = Maria Manton
| writer = Chris Shepherd
| starring = Jean Boht Keiran Lynn Lloyd McGuire Amanda Walker Marline Sidaway Ann Beach Kay Noone
| editing = Nick Fenton
| distributor = Dazzel, Chris Shepherd Films
| released =  
| runtime = 15 minutes
| country = United Kingdom
| language = English
}}
Bad Night for the Blues is a 15-minute comedy film written and directed by Chris Shepherd and produced by Maria Manton. First transmitted on the BBC on the 27 February 2011 and later in France on the 2 April 2011 as a part of Mickrocine on Canal+ Cinecinema. Other countries to transmit the film include Spain, Italy  and Africa. It is the last film to be made by production company Slinky Pictures.

Based on a real life event. Chris takes his Aunty to her local Conservative Club for Christmas party only to find his Aunty has a grudge with another club member Elizabeth. As the night unfolds Chis has trouble controlling his Aunty as the night descends further into  embarrassing conflict.

==Awards==

===2011===
*Winner of International Canal+ Award at Clermont Ferrand International Film Festival.

UK Film Council and BBC Film Network presents a Slinky Pictures production made in association with vision+media.

==External links==
*  
*  
* http://www.chrisshepherdfilms.com

 
 
 
 
 
 
 
 


 
 