Summer City
 
 
{{Infobox film
 | image = Summer City VideoCover.jpeg
 | image size     =
 | alt = 
 | caption = 
 | director = Christopher Fraser
 | producer = Phillip Avalon
 | writer = Phillip Avalon James Elliott Abigail Ward "Pally" Austin
 | music = Phil Butkus
 | cinematography = Jerry Marek
 | editing = David Stiven
 | studio = Avalon Film Corporation Studio
 | distributor = Intertropic films
 | released = 22 December 1977
 | runtime = 83 min.
 | country = Australia
 | language = English
 | budget = A$66,000    or $200,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 322 
 | gross = 
}}

Summer City also known as Coast of Terror is a 1977 Australian surfer film, filmed in Newcastle, Australia. The film also features (a then-unknown) Mel Gibson in his debut role.

==Plot==
In the early 1960s, Sandy (John Jarratt), Boo (Steve Bisley), Scollop (Mel Gibson) and Robbie (Phil Avalon) drive to the beaches north of Sydney for a surfing weekend. The boys are planning to give Sandy a memorable ‘one last fling’ before his impending marriage. Tension flares between university-educated Sandy and ocker Boo when Sandy decides not to join in the fun. At a local dance, Boo seduces Caroline (Debbie Forman), the teenage daughter of a caravan park owner (James Elliott) who discovers what has happened and comes looking for Boo with a gun. 

==Cast==
*John Jarratt as Sandy
*Phillip Avalon as Robbie
*Steve Bisley as Boo
*Mel Gibson as Scollop James Elliott as Carolines father
*Debbie Forman as Caroline Abigail as the woman in pub
*Ward "Pally" Austin as himself
*Judith Woodroffe as the waitress
*Carl Rorke as Giuseppe
*Ross Bailey as Nail
*Hank Tick as Caveman
*Bruce Cole as the man in car
*Vicki Hekimian as Donna
*Karen Williams as Gloria

==Production==
The film was shot on 16mm and blown up to 35&nbsp;mm. Shooting began in October 1976 and took place near Sydney and Newcastle, especially in the town of Catherine Hill Bay.

==Release== Breaking Loose (1988).

==References==
 

== External links ==
*  
* 
*  at Oz Movies
*  at Australian Screen Online
 

 
 
 
 
 

 
 