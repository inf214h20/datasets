Seven Servants
 
{{Infobox film
| name           = Seven Servants
| image          = 
| alt            = 
| caption        = 
| director       = Daryush Shokof
| producers      = Bahman Maghsoudlou, Stefan Jonas
| writer        = Juliane Schulze, Daryush Shokof
| screenplay     = 
| story          = 
| based on       =  
| starring       = Anthony Quinn
| narrator       =  
| music          = Gato Barbieri
| cinematography = Stefan Jonas Henry Richardson, Siegi Jonas
| studio         = Das Werk
| distributor    =  
| released       =  
| runtime        = 88 min.
| country        = USA, Germany
| language       = English
| budget         = 
| gross          =  
}} 1996 cinema German drama — comedy film  directed by Daryush Shokof. The movie is about a man named Archie, portrayed by Anthony Quinn, who wishes to unite and "connect" the races until his last breath.

The film premiered at the 1996 Locarno Film Festival. 

==Plot==
A wealthy old man named Archie hires seven servants of both genders and different races to get connected to him by plugging his body openings until his last breath. The servants stay connected to the old mans body for the next ten days. They eat, sleep, and even use the bathroom and fight thieves together. They overcome many difficulties by staying "united", until finally, an opera singer arrives to plug up his final facial opening and take the last breath from him. Archie dies in peace, pleasure and harmony because he has the people around him "united" at last, even if it is only a "bodily" unification and through him as the catalyst.   

==Cast==
*Anthony Quinn David Warner
*Alexandra Stewart
*Sonja Kirchberger
*Audra McDonald 
*Johnathan Staci Kim
*Reza Davoudi Ken Ard

==Music==
Gato Barbieri composed the music for the film.

==References==
 
*   Der Spiegel 31/1995 vom 31.07.1995, p 166a
* " ," August 24, 1995, Daily News of Los Angeles
*  
* Eric Gutierrez, Aug. 6, 1996, Los Angeles Times
* Brook Comer, "Digital Manipulation takes new ....in ,Seven Servants", Dec.01, 1996, American Cinematographer

==External links==
* 
* 

 
 
 
 


 
 