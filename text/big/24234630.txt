Midnight Phantom
{{Infobox film
| name           = Midnight Phantom
| image_size     =
| image	=	Midnight Phantom FilmPoster.jpeg
| caption        =
| director       = Bernard B. Ray
| producer       = Harry S. Webb (associate producer) Bernard B. Ray (producer)
| writer         = John T. Neville (story and screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Pliny Goodfriend
| editing        = Arthur Hilton
| distributor    =
| released       = 27 November 1935
| runtime        = 63 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Midnight Phantom is a 1935 American film directed by Bernard B. Ray.

== Plot summary ==
 

== Cast == Reginald Denny as Prof. David Graham
*Claudia Dell as Diana Sullivan (misspelled Diane Sullivan in opening credits)
*Lloyd Hughes as Police Lt. Dan Burke Jim Farley as Police Chief James A. Sullivan Barbara Bedford as Kathleen Ryan
*Mary Foy as Mary Ryan John Elliott as Capt. Bill Withers
*Francis Sayles as Police Surgeon Kelly
*Al St. John as Radio Officer Jones
*Henry Roquemore as Dr. McNeil
*Lee Prather as Police Capt. Perkins Robert Walker as Police Capt. Jim Phillips
*Jack Kenny as Police Inspector Silverstein

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 