Signal 30
 
Signal 30 is a 1959 social guidance film made by the Highway Safety Foundation in the vicinity of Mansfield, Ohio.  The film, shown widely to high school students across the country during the 1960s, was produced by Richard Wayman and narrated by Wayne Byers, and takes its name from the radio code used by the Ohio State Highway Patrol for a fatal traffic accident. Alexandra Heller-Nicholas Found Footage Horror Films: Fear and the Appearance of Reality 2014 
 0786470771  "The Highway Safety Foundation became a non-profit organization, and in October 1959 Signal 30 was screened."   

Similar to Red Asphalt, Signal 30 features graphic footage of crashed automobiles and their horrifically injured and dismembered occupants. Despite its gruesome nature, the film later won the National Safety Council Award. It was followed by two sequels, entitled Mechanized Death and Wheels of Tragedy, and inspired a whole genre of similarly gory road safety films.  

==In popular culture== an episode RiffTrax commentaries.
 Public Service Broadcasting.

==References==
 

==External links==
*  (contains graphic imagery)

 
 

 