Feudin' Fools
{{Infobox film
| name           = Feudin Fools
| image          = 
| image_size     = 
| caption        = 
| director       = William Beaudine Jerry Thomas Tim Ryan Bert Lawrence
| narrator       = 
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey
| music          = Edward J. Kay
| cinematography = Marcel LePicard
| editing        = William Austin
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Feudin Fools is a 1952 comedy film starring The Bowery Boys. The film was released on September 21, 1952 by Monogram Pictures and is the twenty-seventh film in the series.

==Plot==
Sach inherits a farm and they boys travel to it.  They discover that their neighbors are the Smiths, who have feuded with the Joneses, of which Sach is one.  They keep Sachs identity secret, and become friends with them.  A gang of bank robbers arrive and hide out in the boys house.  The Smiths arrive and, thinking the robbers names are Jones, begin shooting at them.  The law arrives and takes the criminals away, but Slip accidentally says he is "Mr. Jones" and the Smiths begin shooting at him!

==Production==
This is the first film where the gang consists of only four members, a size it would stay until the end of the series.

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Chuck (Credited as David Condon)
*Bennie Bartlett as Butch

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Anne Kimbell as Ellie Mae Smith
*Dorothy Ford as Tiny Smith Paul Wexler as Luke Smith Robert Easton as Caleb Smith

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Three" on October 1, 2013.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Here Come the Marines 1952 No Holds Barred 1952}}
 

 
 

 
 
 
 
 
 


 