Rocks in My Pockets
 
{{Infobox film
| name           = Rocks in My Pockets
| image          = Rocks in My Pockets.jpg
| caption        = Film poster
| director       = Signe Baumane
| producer       = Signe Baumane, Sturgis Warner, Co-Producer
| writer         = Signe Baumane
| starring       = Signe Baumane
| music          = Kristian Sensini
| cinematography = 
| editing        = 
| distributor    = Zeitgeist Films, New Europe Film Sales, Locomotive Productions, Yekra, Ltd.
| released       =  
| runtime        = 88 minutes
| country        = Latvia United States
| language       = English Latvian
| budget         = 
}}

Rocks in My Pockets ( ) is a 2014 animated film written, produced, directed and animated by Signe Baumane. It was originally created in English and a Latvian version was thereafter translated and produced. The film is based on true events about five women of the filmmakers family, including herself, and their battles with depression and suicide.
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.    The soundtrack composed by Kristian Sensini has received a nomination at the Jerry Goldsmith Film Music Awards in the Best Movie Soundtrack category.

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Latvian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 