José and Pilar
 
 
 
{{Infobox film
| name = José and Pilar
| image = Joseepilardvd.png
| caption = Cinematic release poster
| director = Miguel Gonçalves Mendes
| runtime = 117 minutes
| producer = Miguel Gonçalves Mendes Ana Jordão  Daniela Siragusa
| starring = José Saramago Pilar del Río Gael García Bernal Fernando Meirelles
| music = Adriana Calcanhoto Bruno Palazzo Camané Luís Cila Noiserv Pedro Granato Pedro Gonçalves
| cinematography = Daniel Neves
| editing = Cláudia Rita Oliveira (film editor)|Cláudia Rita Oliveira
| studio = JumpCut O2 El Deseo
| country = Portugal
| language = Portuguese
| released =  
}}

José and Pilar ( ) is a Portuguese documentary directed by Miguel Gonçalves Mendes following the last years of the Nobel Prize winner José Saramago, chiefly through his relationship with his resolute wife, Pilar del Río. Highly praised by the critics and the audience, the film seems to have accomplished to show the tenderness, the genuine integrity and the deeply concerned humanity behind such controversial figure and his spouse. It gathers sequences from Madrid to Helsinki to Rio de Janeiro and covers Jose and Pilars life in Lanzarote, their trips around the world (presenting Josés books, signing autographs, making speeches) and their most simple, transient and quotidian moments, as for during the period José writes his "The Elephants Journey". The film was produced by Miguel Mendes JumpCut (Portugal), Fernando Meirelless O2 and Pedro Almodóvars El Deseo.

== Plot==
"The Elephants Journey", in which Saramago narrates the adventures and antics of an elephant transported from the court of King John III of Portugal to that of the Austrian Archduke Maximillian, is the starting point of José and Pilar.

The film shows us their daily life in Lanzarote and their trips around the globe, and is a surprising portrayal of an author throughout the creative process of a couple decided to change the world, or at least to make it a better place. The film shows us an hidden Saramago, unravels any preconceived ideas we may have about the man and proves that genius and simplicity are indeed compatible. José and Pilar is a glimpse into one of the greatest creators of the Twentieth Century and shows us that, as Saramago said, "everything can be told in a different way."

== Cast ==
* José Saramago, writer (Nobel Prize winner in Literary, 1998).
* Pilar del Río, journalist.
* Gael García Bernal, actor.
* Fernando Meirelles, filmmaker.

== Accomplishments and nominations ==
* Five months in the portuguese theaters (historical maximum).
* The first Portuguese film co-produced with internationally high ranked film production companies (O2 and El Deseio).
* First Portuguese film to ever open the International Documentary Film Festival of Lisbon (DocLisboa) (2010).
* The Portuguese (Portugal) film with the widest distribution and the largest audience to ever premiere in Brazil.
* One of the greatests word-to-mouth successes of the 2010 Rio de Janeiro Film Festival (world premiere), among three hundred other films.
* Opened the Ronda International Political Cinema Film Festival, introduced by the Judge Baltazar Garzón.
* Got a 1300 people audience at the spanish premiere, in Madrid, on January 17.
* Attended the Guadalajara International Film Festival, one of the most prestigious film festivals in Latin America (out of competition).
* Winner of the Audience Award in the São Paulo Film Festival.
* Nominated by the Portuguese Authors Society for Best Film (2011). Time Out Portugal.
* Nominated for Best Film, Best Editing and Best Soundtrack by the Brazilian Academy of Cinema.
* Portuguese submission for a nomination for the Academy Award for Best Foreign Film.      

== Press ==
 
* " (...) its so carefully constructed that at times it feels like fiction, shuttling easily and with a surprising level of intimacy between Saramago, the public persona, and Saramago, the private man.” in Variety (magazine)|Variety (USA).
* "The film portrays Joses clearheaded pessimism in his quests for human rights." in Cahiers du cinéma (Spain).
* "An amazing film about the love that tied them and the exhausting daily routine of a public figure." in Diário de Notícias (Portugal).
* "José and Pilar is a documentary, but a documentary that dissolves the illusion of the pure documentary. The end of a cicle. A monument to the glory of a writer." in Público (Portugal)|Público (Portugal).
* "Miguel Gonçalves Mendes took four years to make this documentary but watching it makes us realize it will last for much longer." in Expresso (Portuguese newspaper)|Expresso (Portugal) Time Out (Portugal).

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Portuguese submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*  
* http://www.josesaramago.org/
*   (Portugal)

 
 
 
 
 
 