The Resurrected
{{Infobox Film
| name           = The Resurrected
| image          = TheResurrected.jpg
| image_size     = 
| caption        = Original poster
| director       = Dan OBannon
| producer       = Mark Borde Kenneth Raich
| writer         = Brent V. Friedman	 John Terry Jane Sibbett Chris Sarandon Robert Romanus
| cinematography = Irv Goodnoff
| editing        = Russell Livingstone	  Scotti Bros. Pictures Metro-Goldwyn-Mayer Live Home Video Lionsgate
| released       =  
| runtime        = 108 min.
| country        = United States English
| budget         = $5,000,000 est.
}}
 1992 Cinema American horror John Terry, Jane Sibbett, Chris Sarandon and Robert Romanus.  It is an adaptation of the H. P. Lovecraft novella The Case of Charles Dexter Ward. Originally intended for a theatrical release, the film had a brief theatrical run before going direct to video in 1992.

==Plot==
Claire Ward (Sibbett) hires private investigator John March (Terry) to look into the increasingly bizarre activities of her husband Charles Dexter Ward (Sarandon).  Ward has become obsessed with the occult practices of raising the dead once practiced by his ancestor Joseph Curwen (Sarandon in a dual role).  As the investigators dig deeper, they discover that Ward is performing a series of grisly experiments in an effort to actually resurrect his long-dead relative Curwen.

==Cast== John Terry as John March
* Jane Sibbett as Claire Ward
* Chris Sarandon as Charles Dexter Ward/Joseph Curwen
* Robert Romanus as Lonnie Peck

==Production history==
Director OBannon and screenwriter Brent V. Friedman had both developed the Lovecraft property over the years, independent of each other; Friedmans version of the script was titled Shatterbrain. While Friedman receives sole writing credit, OBannon did incorporate some of his own ideas into the project.   OBannons original title for the film was The Ancestor, which was later changed to The Resurrected after being re-cut and altered by the studio for theatrical release.  

==Critical reception==
In their book  , Andrew Migliore and John Strysik write: "The Resurrected is the best serious Lovecraftian screen adaptation to date, with a solid cast, decent script, inventive direction, and excellent special effects that do justice to one of   darker tales." 

==Release==
The film was given a short-lived theatrical release on June 1, 1991.  It was then released direct to video on April 15, 1992 by Live Home Video in the United States. It was later released on DVD internationally through Metro Goldwyn Mayer in 2003, and in the United States through Lionsgate in 2005.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 