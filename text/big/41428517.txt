Sora no Otoshimono Final: Eternal My Master
{{Infobox film
| name           = Sora no Otoshimono Final: Eternal My Master
| image          =  
| image size     = 
| border         = 
| alt            = 
| caption        = Japanese theatrical release poster featuring Ikaros and Tomoki flying to Synapse.
| director       = Hisashi Saitō
| producer       = Kadokawa Shoten
| writer         = Jiyū Ōgi
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Motoyoshi Iwasaki
| cinematography = 
| editing        = 
| studio         = Production IMS
| distributor    = 
| released       =   
| runtime        = 50 Minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 Sora no Otoshimono Final: Etānaru Mai Masutā}} is a 2014 Japanese anime film based on the manga series Heavens Lost Property. 

==Synopsis==
The scene opens with the Zeus destruction above Synapse, Tomoki calls for Nymph to return immediately; unfortunately Ikaros responds that Nymph isnt coming. Tomoki commands Ikaros to take him straight to Synapse as the Earths surface deliberately vanishes. While flying towards Synapse, Ikaros tells Tomoki a story how long ago due to malfunction she was once ordered by a "boy" to attack Synapse out of desperation. Remarking a dying Sugata, Ikaros body begins burning away because of a device installed within her whenever she approaches Synapse without permission. Having second thoughts, Tomoki orders Ikaros to go back but cannot since there is nowhere else to go to.

The film shifts to where Tomoki wakes up by Ikaros who attempted to kiss him. Tomoki has a rough morning when Nymph invites him on a date, with Astraea and Chaos joining in. Then meets up with Sohara as Chaos questions what a date is. Meanwhile, Hiyori visits Ikaros to see how well shes cultivating watermelons, and then Sugata and Mikako come over to interrogate the existence of Synapse or about Hiyoris return. Though Ikaros defies anything regarding Synapse, Hiyori reveals the reason of her "return" was due to Tomokis unique existence in the particular world they are living in now. Sugata concludes the world continuum is all connected to Synapse. As everyone comes home, Tomoki wonders why Ikaros doesnt show emotions and assumes it because of the imprinting chain. Nymph and Astraea get into a sister fight. Tomoki is getting tired of the troublesome routine and tries to kick them out but they shout his own words back at him that Angeloids are freeloaders. Tomoki then breaks Ikaros imprinting, with Daedalus and Sohara muttering their dismayed. Tomokis day becomes even rougher as everyone are condemning him. Mikako takes Ikaros to her household for the time-being as to aggravate Tomoki.

Worried that Ikaros is now in Mikakos care and Tomoki still being stubborn, Sohara, Nymph, and Astraea, dredge Tomokis room for a restoration card. Scanning, they found one in Tomokis pants, which activates it sending them to a dimension on a train in a blizzard with Tomokis grandfather there. In order to return home, they have to oblige at whenever station they get off at. Passing by stations for a chance to date or kiss Tomoki, things get worse as the farther stations become more ecchi. Sohara, Nymph, and Astraea jump out of the train, but find themselves in a freezing cabin as the last stop, where they are forced to warm up naked with Tomoki.

Meanwhile, back in the real world, Mikako scans Ikaros memory (possibly that Mikako was an Angeloid) and does something strange to her. The next day, the girls try to make breakfast for Tomoki however their quality doesnt meet any standard. Then Ikaros crashes in picking up Tomoki in a bold manner saying she wont hand over him to anyone. Flying in the air, Ikaros recklessly drops Tomoki in a restaurant on his command as their date. Proposing they should go somewhere with just the two of them, Ikaros chases after Tomoki begging him to be her master again. Arriving at the sakura tree, Tomoki reminisces all that happened since he met Ikaros. The others show up lecturing him that he needs to think deeply how Ikaros feels and what she wants. Mikako then "orders" Ikaros to throw Tomoki somewhere high up, as a way of to provoke their feelings for each other. Streaming out of despair to what she was made for and with no other place to go to, Ikaros tearfully begs Tomoki to be her master as he is all she ever wants. Realizing his foolishness he accepts her imprinting and returns to everyone happily. Daedalus replies "Thats a relief".

Back to the opening scene, Ikaros kisses Tomoki showing her smile for the first time before disappearing in his arms, arriving in Synapse; ending the film.

==References==
 

== External links ==
*  

 
 
 
 