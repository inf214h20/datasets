Millionaires Express
 
 
{{Infobox film
| name           = Millionaires Express
| image          = MillionairesExpress.jpg
| image_size     =
| caption        = Hong Kong film poster
| director       = Sammo Hung
| producer       = Alfred Cheung Raymond Chow Leonard Ho Wu Ma
| writer         = Sammo Hung
| narrator       =
| starring       = Sammo Hung Yuen Biao Rosamund Kwan Mei-sheng Fan Hwang Jang Lee
| music          = Stephen Shing Anders Nelsson The Melody Bank Alastair Monteith-Hodge
| cinematography = Arthur Wong
| editing        = Peter Cheung Golden Harvest
| released       =  
| runtime        = 101 min.
| country        = Hong Kong Japanese
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 1986 Cinema Hong Kong martial arts action Western western film written and directed by Sammo Hung. Film starred Hung, Yuen Biao, Rosamund Kwan, Mei-sheng Fan and Hwang Jang Lee.

==Plot==
Ching Fong-Tin (Sammo Hung) goes to Russia to steal goods from the Russian soldiers. Unfortunately he is caught and the soldiers make him strip his clothes down to his underwear. They force him to wear a brassiere and the head of a mop as a wig, and he is made to dance for their amusement. However, he makes his escape, grabbing his grenades as he goes, and throws one into the cabin. Fook Loi (Kenny Bee) catches Ching and lectures him about his crimes. Ching tries to escape, but in the scuffle, he and Fook end up rolling into a snowball. In the final moments after they both got out of the snowball, Fook tries to look for Ching and is taken by surprise when he jumps down from a tree, grabs his clothes and rushes off back to his hometown.

Bandits dressed in police uniforms (Eric Tsang, Yuen Wah, Lam Ching Ying, Wu Ma and Mang Hoi) set fire to a large building. Tsao Cheuk Kin (Yuen Biao) and his fire team race to the scene and save a fat lady and a blind woman. While the fire rages, the bandits rob the town bank, but the manager manages to alert the townfolk before they can escape and two of the bandits are jailed. With the loss of money stolen by the bandits, Mayor Yi (Woo Fung) gives a negative speech. In contrast, Tsao encourages the townfolk and he is given the job of mayor and head of the towns security.

Ching and his assistant move from a hotel to his home town, arriving with a car full of women (Olivia Cheng, Rosumand Kwan, Emily Chu). Chings ambition is to get money into the town, so his hotel is cleaned up and redressed. A train is due to pass, carrying numerous passengers, including some criminals and some Japanese tourists (Yasuaki Kurata, Yukari Oshima, Hwang Jang Lee). In order to bring custom to his hotel, Ching hatches a plan to force the train to stop at the town. He heads towards the railway station on his motorcycle and sidecar, but is chased away by Tsao riding on horseback. Ching swings a tree branch, which knocks Tsao from his horse. Ching takes the horse and rides the rest of the way. As he prepares the dynamite, Ching is discovered, and ends up fighting with Tsao. After winning, Ching continues to wait for the train.

As the train travels through the country, passenger Han (Richard Ng) sneaks back and forth on roof of the train between his fat wife (Lydia Shum) and his beautiful mistress. The bandits from the town try to board the train. The first bandit (Eric Tsang), with his body covered in magnets inadvertenly gets stuck to the train, whilst the second bandit (Lam Ching Ying) using the rope to climb aboard. However, instead of lassooing the train, his rope catches Han, who is forced to hang on for dear life, whilst the bandit runs alongside the train. The remaining bandits struggle with a cart, but finally manage to get on board. When the train reaches the station, Ching blows the dynamite, derailing the train.

As Ching had planned, the passengers spend time in his home town awaiting the trains repair. In the hotel, the criminals devise a plan to get into the room housing Hans mistress, posing as Japanese tourists. Unfortunately they dont dont know the language, and are forced to hide when the train captain (Billy Lau) and his mistress enter the room. After the train captain goes into the bathroom, the bad guys try to sneak out quietly, but soon have to hide again. Han climbs down the hotel roof, inadvertently scaring the train captains mistress. The commotion alerts his wife, who accuses him of cheating. To explain the situation, he claims that he is actually an agent spying on the Japanese, and the bad guys use this excuse to come out of hiding.

When Ching asks Chi (Rosamund Kwan) to distract Tsao she comes with all sorts of things most of them saying that she loves him.
Tsao then tells Chi that hes busy and cant dream with her and leaves. Chi captures up with him and asks him not to go and Tsao swings his hat to the side and takes Chi into his hands and starts snogging her. Tsao then says something that makes Chi want to kiss him more.When Chi wants to kiss him more he signals his helpers to take his place and unfortunately Chi opens her eyes and runs away.

Fook Loi returns from Russia and uses Tsao to capture Ching and put him into jail. At night, Siu-Hon (Olivia Cheng) and her group of ladies free Ching and allow him to make up his mind. The rest of the bad guys arrive on horseback, storming the town and capturing the Japanese tourists.

After Ching makes up his mind, he comes back to his hometown and free the bandits, plus Fook, Tsao and Siu-Hon. He decides to turf the bad guys out of town, beginning by using a chaingun, and later using martial arts. A huge fight breaks out, until the bad guys are dealt with and Ching and Tsao finally take back the map from the Japanese.

==Cast==
 
* Sammo Hung - Ching Fong-Tin
* Yuen Biao - Fire Chief Tsao Cheuk Kin
* Rosamund Kwan - Chi
* Kenny Bee - Fook Loi / Lai Fu
* Peter Chan - Firefighter/Security officer
* Olivia Cheng - Siu-Hon
* Chin Kar-lok - Firefighter / Security Officer
* Emily Chu - Prostitute Employee Bo
* Chung Fat - Bandit
* Fan Mei Sheng - Bandit (as Mei-sheng Fan) Hsiao Ho - Siu Hau
* Hwang Jang Lee - Yukio Fushiki
* Yasuaki Kurata - Samurai
* Phillip Ko - Bandit
* Lam Ching Ying - Security officer / Bank Robber
* Billy Lau - Train Captain
* Lau Chau Sang - Fireman, security officer
* Lau Kar Wing - Kang
* Mang Hoi - Security officer / convict / Bank Robber (as Randy Mang)
* Richard Ng - Han
* Lydia Shum - Hans wife  Richard Norton - Bandit Yukari Ôshima - Samurai
* Cynthia Rothrock - Bandit
* Shih Kien - Master Sek James Tien - Yun Shiyu / Shi Yu
* Eric Tsang - Jook Bo 
* Jimmy Wang Yu - Master Wong Kei Ying
* Dick Wei - Bandit
* Woo Fung - Mayor Yi
* Shirley Gwan - Lai
* Wu Ma - Security officer / convict / Bank Robber
* Pauline Wang Yu-wan - Prostitute
* Bolo Yeung - Cotton weaver
* Yuen Wah - Security officer/convict
* Wu Ma - Bank Robber
* Yuen Miu - Bank Robber
* Johnny Wang - Train Robber
* Cho Tat Wah - Shanghai Chief Inspector Teddy Yip - Siu Hon Kisser
* Teresa Ha - Cotton weavers wife
* Meg Lam - 7th Aunt
* Shirley Gwan - Lai   
* Deborah Dik
* Yuen Tak
* Chin Siu Ho (deleted scene) Anthony Chan
* Alfred Cheung
* Patrick Tse
* Danny Yip
* Yip Seung-Wa
* Poon Lai-Yin
* Yeung Yau-Cheung
* Kam Hing
* Lui Hung (deleted scene) Tai Po
 

==DVD releases==
The film was initially released on DVD in Hong Kong by Universe Laser (Region 0). It was re-released in 2006 by Joy Sales (Region 3).

In the US, it was released in 1999 by Tai Seng, under the title The Millionaires Express (with a leading "The" and the apostrophe omitted). It was re-released in 2007 by Dragon Dynasty, under the export title Shanghai Express.

In the UK, it was released in 2005 by Hong Kong Legends / Contender Entertainment Group, under its original title.

==See also==
* List of Dragon Dynasty releases

==External links==
*  
*  

 

 
 
 
 
 
 
 
 