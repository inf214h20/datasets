The Law Commands
{{Infobox film
| name           = The Law Commands
| image          =The Law Commands.jpg
| image_size     =
| caption        =
| director       = William Nigh
| producer       = E.B. Derr (producer) Bernard A. Moriarty (associate producer)
| writer         = Bennett Cohen (story and screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Arthur Martinelli
| editing        = Donald Barratt
| distributor    =
| released       = 7 May 1937
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Law Commands is a 1937 American film directed by William Nigh.

== Cast == Tom Keene as Dr. Keith Kenton
*Lorraine Randall as Mary Lee Johnson Robert Fiske as John Abbott
*Budd Buster as Kentuck Jones
*Matthew Betz as Frago, lead henchman
*John Merton as Frank Clark
*Carl Stockdale as Jed Johnson
*Marie Stoddard as Min Jones
*Charlotte Treadway as Martha Abbott David Sharpe as Danny Johnson
*Allan Cavan as Judge
*Horace B. Carpenter as Jason, lead farmer

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 


 