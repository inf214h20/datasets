Fatty's Reckless Fling
 
{{Infobox film
| name           = Fattys Reckless Fling
| image          = Fattys Reckless Fling 1915.jpg
| caption        = Film still Fatty Arbuckle
| producer       = Mack Sennett
| writer         =  Fatty Arbuckle
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 13 minutes
| country        = United States  Silent (English intertitles)
| budget         = 
}}
 short comedy Fatty Arbuckle.

==Cast== Roscoe Fatty Arbuckle - Fatty
* Edgar Kennedy - Neighbor
* Minta Durfee - Neighbors Wife
* Katherine Griffith - Fattys Wife
* Billie Walsh - Bartender
* Glen Cavender - House detective Frank Hayes - Card Player (uncredited)
* Ted Edwards - Card Player (uncredited)
* Grover Ligon - Card Player (uncredited)
* Venice Hayes - Girl Fatty Sits On (uncredited)
* Harry McCoy - Drunk (uncredited)
* George Ovey - Card Player (uncredited)

==See also==
* List of American films of 1915
* Fatty Arbuckle filmography

==External links==
* 

 
 
 
 
 
 
 
 

 