Mohalla Assi
{{Infobox film
| name           = Mohalla Assi
| image          = 
| director       = Chandra Prakash Dwivedi
| producer       = Vinay Tiwari
| writer         = Kashi Nath Singh|Dr. Kashi Nath Singh
| screenplay     = Chandra Prakash Dwivedi|Dr. Chandra Prakash Dwivedi
| story          = Kashi Nath Singh|Dr. Kashi Nath Singh
| based on       =  
| starring       = Sunny Deol Ravi kishan Sakshi Tanwar Saurabh Shukla Mukesh Tiwari Rajendra Gupta Mithilesh Chaturvedi Seema Azmi
| cinematography = Vijay Arora
| editing        = Aseem Sinha
| studio         = Crossword Entertainment Pvt. Ltd.
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Mohalla Assi (  film starring Sunny Deol, and directed by Chandra Prakash Dwivedi who previously directed Pinjar (film)|Pinjar (2003), starring Urmila Matondkar and Manoj Bajpai, and most known for television epic Chanakya (TV series)|Chanakya.   
 Kashinath Singhs popular Hindi novel Kashi Ka Assi. Assi Ghat is a ghat in Varanasi (Banaras) on the banks of Ganges River, and the film is based in a famous and historical Mohalla (locality) by the ghat, on the southern end of Banaras. Also starring Ravi Kishen and Sakshi Tanwar, the film is set in the post-independence period. Currently film is in post-production stage and likely to release in January, 2013.       As of November 2013, the film is delayed as the film director has stopped the work claiming that he has not been paid his dues. 

Sunny Deol plays the lead role of Sanskrit teacher and an orthodox religious priest (Pundit) while Sakshi Tanwar plays his wife. 

The release date of Mohalla Assi has been repeatedly pushed back and allegations of non-payment have continued to surface.
 
 

==Production==
The film was shot on the banks of The Ganga and on the sets erected at the Film City, Mumbai The film is nearing completion and is slated to release around post monsoon all over India and oversears.    
Ameesha Patel was offered the female lead role but turned it down due to financial differences with the films producer.  Sakshi Tanwar was a cast a replacement.

==Cast==
* Sunny Deol 
* Ravi Kishan
* Sakshi Tanwar
* Saurabh Shukla

==References==
 

 
 
 
 
 
 
 