The Last Shot You Hear
{{Infobox film
| name           = The Last Shot You Hear
| image_size   =
| image	      = 
| caption         =
| director        = Gordon Hessler
| producer      = Jack Parsons
| screenplay    = Tim Shields
| based on      =  
| narrator       =
| starring        = Hugh Marlowe Zena Walker Patricia Haines William Dysart
| music           = Bert Shefter
| cinematography = David Holmes
| editing         = Robert Winter
| studio          = Lippert Pictures
| distributor    = 20th Century Fox
| released       = 1969
| runtime        = 90 minutes
| country         = United Kingdom
| language      = English
| budget         = 
| gross            =  
}} 1969 British thriller film directed by Gordon Hessler and starring Hugh Marlowe, Zena Walker, Patricia Haines, and William Dysart. It was Marlowes last film appearance.

==Cast==
* Hugh Marlowe as Charles Nordeck
* Zena Walker as Eileen Forbes
* Patricia Haines as Anne Nordeck
* William Dysart as Peter Marriott
* Thorley Walters as Gen. Jowett
* Lionel Murton as Rubens
* Joan Young as Mrs. Jowett
* Helen Horton as Dodie Rubens John Nettleton as Det. Inspector Nash John Wentworth as Chambers
* Alister Williamson as CID Man
* Julian Holloway as Brash Young Man

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 
 