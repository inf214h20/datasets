Big Money Hustlas
{{Infobox film
| name           = Big Money Hustlas
| image          = Big Money Hustlas poster.jpeg
| image_size     = 175px
| caption        = Promotional poster for the films home video release.
| director       = John Cafiero
| producer       = John Cafiero Suzanne Cafiero
| writer         = Joseph Bruce
| narrator       = Violent J Shaggy 2 Jamie Madrox Monoxide Child the Misfits
| music          = Mike E. Clark
| cinematography = James Carman
| editing        = Psychopathic Films Non-Homogenized Productions
| released       =  
| runtime        = Approx. 105 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}} Joseph "Violent Joseph "Shaggy The Misfits. certified platinum Western genre follow-up, Big Money Rustlas, was released direct-to-video on August 17, 2010.

==Plot==
Sugar Bear (Shaggy 2 Dope), a streetwise detective from San Francisco, is brought to New York City by its chief of police (John G. Brennan) to take down Big Baby Sweets (Violent J), a notorious crime lord who controls the entirety of the citys criminal underworld with his right-hand men Big Stank (Jamie Madrox) and Lil Poot (Monoxide Child), and his personal security ninja Hack Benjamin (Robert Bruce). After getting a firsthand look at the police forces incompetence via Officer Harry Cox (Harland Williams), Sugar Bear prevents a robbery of a local doughnut shop by one of Big Baby Sweets thugs, Ape Boy, and begins a romance with a 300-pound stripper, Missy (Sindee Williams).

He soon arrests Big Baby Sweets, Big Stank and Lil Poot himself, but the police are forced to let them go because of a lack of evidence. The gangsters retaliate by terrorizing the city and sending a pair of stealthy Magic Ninjas to murder Missy, leading Sugar Bear to depressed alcoholism. Sugar Bears idol, Dolemite (Rudy Ray Moore), appears before him to reassure him and begin training him to bring down Sweets evil empire. Sugar Bear kills the Magic Ninjas and Hack Benjamin, and has Big Stank and Lil Poot carted off by their wealthy, upper-class parents before defeating another one of Big Baby Sweets henchmen, Cactus Sac (Mick Foley), in a wrestling match. During Big Baby Sweets personal confrontation with Sugar Bear, Sweets is shot by his mother, and Sugar Bear removes Sweets face paint, revealing him to be Harry Cox.

==Cast== Violent J — Big Baby Sweets / Ape Boy Shaggy 2 Dope — Sugar Bear
*Harland Williams — Officer Harry Cox
*John G. Brennan — The Chief
*Rudy Ray Moore — Dolemite Jamie Madrox — Big Stank  Monoxide Child — Lil Poot
*Myzery — Green Willie
*Kayla Kleevage — Phat Tittie Kittie
*Sindee Williams — Missy 
*Bob Greenberg — Magic Ninja 1
*Lee Willet — Magic Ninja 2
*Stefan Kudek — Dr. Dinglenut
*Mick Foley — Cactus Sac Jumpsteady — Hack Benjamin
*Floyd Vivino — Announcer

==Production==
Big Money Hustlas was inspired by the video Big Ballers.    Insane Clown Posse and Twiztid had seen the movie and loved the videos low-budget comedy style. Using the ideas that he, Joseph Utsler, James Spaniolo, and Paul Methric created, Joseph Bruce wrote the entire script himself in one month.  Island Records gave him $250,000 to produce the film. 

Big Money Hustlas was shot in New York.  Most of the crew disliked the movie and the cast. They went on strike twice, while only a few crew members continued to work.  The movie was shot in two months, but went way over budget.  Halfway through the movie, Bruce had to pay $100,000 of his own money to continue filming.  Island never paid the crew for the last two weeks of work due to the film going so far over budget. 
 Robert Bruce, Kamal Ahmed of The Jerky Boys appeared separately, because they were unable to get along with each other during the filming. 
 The Misfits, he stuck around for the entire shoot.  Jerry Onlys son plays the altar boy in the opening scene. 

In addition to playing Hack Benjamin, Jumpsteady also plays several background characters.  Violent J also appeared in a gorilla costume as "Ape Boy"; Jamie Madrox recorded a dub for the characters voice.  Although a production company was paid $8,000 to replace the original audio with the dub, they told the director that they were unable to do it, and a decision was made to instead keep the original audio track, with Violent Js voice. 

==Soundtrack==
{{Infobox album  
| Name        = Big Money Hustlas
| Type        = soundtrack
| Artist      = various artists
| Cover       = BigMoneyHustlas_ST.jpg 
| Released    = June 26, 2001
| Recorded    = 
| Genre       = Midwest hip hop
| Length      =  Psychopathic
| Producer    = Mike E. Clark
| Reviews     = 
}}

The films music was written by Psychopathic Records producer Mike E. Clark. A soundtrack album was released in 2001, and featured new music from the film, in addition to previously released tracks by Insane Clown Posse and Twiztid, and the theme to the 1976 film The Human Tornado, performed by Rudy Ray Moore. While not included in the official soundtrack, key songs from the movie were used from Gert Wildens album Schoolgirl Report. The song "Girl Faces" was used during the bedroom love scene, and "Little Girls" played in the background at the gentlemens club. Gert Wilden was noteworthy for erotic music in the 1970s, and thus the reason his music was used in the film.

 
{| class="wikitable"
|-
!align="center" width="30"|# Title
!align="center" Time
!align="center" width="135"|Performer(s)
|- 1
|"Intro"
|0:33
|
|- 2
|"Big $"
|3:55 Violent J Twiztid
|- 3
|"Sugar Bear"
|3:16 Shaggy 2 Dope
|- 4
|"Fuck the World"
|4:03 Insane Clown Posse
|- 5
|"Cotton Candy"
|4:36 Insane Clown Posse
|- 6
|"Bury Me Alive"
|4:17 Twiztid
|- 7
|"Bring It On"
|3:55 Insane Clown Posse
|- 8
|"Ninjas"
|4:58 Insane Clown Posse
|- 9
|"Bitches"
|3:27 Insane Clown Posse Ol Dirty Bastard
|- 10
|"Rock the Dead"
|5:14 Twiztid
|- 11
|"Spin the Bottle"
|4:41 Insane Clown Posse Twiztid
|- 12
|"The Human Tornado"
|2:23 Rudy Ray Moore
|}

==Impact==
 platinum certification. Western genre,   Big Money Rustlas was released on DVD on August 17, 2010.  The record label Majik Ninja Entertainment, formed by Twiztid in 2014, is named after a line which they delivered in the film.

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 