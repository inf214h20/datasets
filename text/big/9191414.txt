Hum Paanch (film)
{{Infobox film
| name           = Hum Paanch
| image          = HumPaanch5.jpg
| image_size     = 
| caption        =  Bapu
| producer       = Boney Kapoor Surinder Kapoor S. K. Films Enterprises
| writer         =
| screenplay     = Mullapudi Venkata Ramana
| story          = Puttanna Kanagal|S. R. Puttanna Kanagal
| dialogue       = Rahi Masoom Reza Vinay Shukla
| narrator       =  Sanjeev Kumar Nasiruddin Shah Raj Babbar Gulshan Grover Amrish Puri
| music          = Laxmikant-Pyarelal, S. P. Balasubrahmanyam
| cinematography = Sharad Kadwe
| editing        = Kamlakar Karkhanis
| studio         = Essel Studios, Mumbai Mehboob Studios, Mumbai
| distributor    = Eros International Shemaroo Video Pvt. Ltd.
| released       =     
| runtime        = 180 mins
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1980 Hindi Bapu and Sanjeev Kumar, Nasiruddin Shah, Raj Babbar, Gulshan Grover and Amrish Puri. A young Anil Kapoor who is producer Boney Kapoors brother has a cameo appearance. This film is a remake of 1978 Kannada film Paduvaaralli Pandavaru, directed by renowned director Puttanna Kanagal. The film was distributed by noted distributor R. N. Mandre. And also remade in Telugu as Mana Voori Pandavulu (1978).  The movie was an adaptation of the Mahabharata, fit to a rural scenario with some hints to the various clashes between the Pandavas and the Kauravas and to the characters in the epic.

==Cast==
*Sanjeev Kumar  as  Krishna
*Shabana Azmi  as  Sundariya Mittun Chakraborthy  as  Bhima
*Deepti Naval  as  Lajiya Nasiruddin Shah  as  Suraj
*Raj Babbar  as  Arjun
*Gulshan Grover  as  Mahavir
*Amrish Puri  as  Vir Pratap Singh
*Aruna Irani  as  Nishi
*A. K. Hangal  as  Pandit
*Geeta Siddharth  as  Vir Prataps Sister
*Uday Chandra  as  Swaroop
*Roopesh Kumar  as  Vijay Kanhaiyalal  as  Lala Nainsukh Prasad Srivastav
*Kalpana Iyer  as  Cameo, in song "Aiye Meherban"
*Leena Das  as  Cameo, in song "Aiye Meherban"
*Phiroza Cooper  as  Cameo, in song "Aiye Meherban"
*Sujata Bakshi  as  Cameo, in song "Aiye Meherban"
*C. S. Dubey
*Anil Kapoor Dulari 
*Jugal Kishore
*Sunitha Shivaramakrishnan (South India Actress)
*Sunder

==Crew== Bapu
*Story &ndash; Puttanna Kanagal|S. R. Puttanna Kanagal
*Screenplay &ndash; Mullapudi Venkata Ramana
*Dialogue &ndash; Rahi Masoom Reza, Vinay Shukla
*Producer &ndash; Surinder Kapoor, Boney Kapoor
*Production Company &ndash; S. K. Films Enterprises
*Cinematographer &ndash; Sharad Kadwe
*Editor &ndash; Kamlakar Karkhanis
*Art Director &ndash; M. S. Shinde
*Costume Designer &ndash; Mrs. Kamal Bakshi, Prem Suri
*Action Director &ndash; Veeru Devgan
*Choreographer &ndash; Kamal Kumar Universal Music India Limited)

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Anand Bakshi
| all_music       = Laxmikant-Pyarelal, S. P. Balasubramaniam

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Aaiye Meharban
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Usha Uthup
| length1         = 5:40

| title2          = Aapno Se Munh Mod Ke
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Mohammad Rafi
| length2         = 1:45

| title3          = Aati Hai Palki
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Mahendra Kapoor, Kishore Kumar
| length3         = 6:25

| title4          = Dheere Chal Zara
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Mohammad Rafi
| length4         = 5:15

| title5          = Hum Paanch Pandav
| note5           =
| writer5         =
| lyrics5         =
| music5          = Shailendra Singh, Suresh Wadekar
| length5         = 4:50

| title6          = Ka Jaanu Main
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Amit Kumar, Lata Mangeshkar
| length6         = 5:15

| title7          = Kya Kahiye Bhagwan Se
| note7           =
| writer7         =
| lyrics7         =
| music7          =
| extra7          = Mohammad Rafi
| length7         = 2:35

| title8          = Ye Soye Insaan
| note8           =
| writer8         =
| lyrics8         =
| music8          =
| extra8          = Mohammad Rafi
| length8         = 1:45
}}

==References==
 

== External links ==
*  

 
 
 
 
 
 