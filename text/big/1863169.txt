Secondhand Lions
{{Infobox film 

| name = Secondhand Lions
| image = SecondhandLions.jpg
| caption = Secondhand Lions film poster
| director = Tim McCanlies Scott Ross Corey Sienega
| writer = Tim McCanlies
| starring = Haley Joel Osment Robert Duvall Michael Caine Kyra Sedgwick
| music = Patrick Doyle
| cinematography = Jack N. Green
| editing = David Moritz
| distributor = New Line Cinema
| released =  
| runtime = 111 minutes
| country = United States
| language = English
| budget = $30 million   
| gross = $47,902,566 
}}

:This article is about the film.  For the stage musical, see  .
 American comedy-drama film written and directed by Tim McCanlies, tells the story of an introverted young boy (Haley Joel Osment) who is sent to live with his eccentric great-uncles (Robert Duvall and Michael Caine) on a farm in Texas.

== Plot == court reporting school which his mother told him she was attending only to learn she isnt there. The two uncles find him; Hub is glad to see him on his way, but Garth convinces Hub to bring him back home. 
 Greasers enter, annoying Hub who easily beats them in a fight. In their absence, Ralph and Helens sons accidentally release Jasmine from her crate just as Hub, Garth and Walter return. Walter searches for Jasmine, and finds her in the cornfield, which becomes her new "jungle" home. Hub and Garth decide to let Walter keep Jasmine as a pet, knowing the lioness will keep Ralph and Helen away. When Walter notices Hub lecturing the four toughs, Garth explains its Hubs special speech for "what every boy needs to know about being a man".
 shanghaied and gold pieces on Hubs head, keeping them in constant peril from assassins and bounty hunters. Finally, Hub arranged for Garth, disguised as a bounty hunter, to get him close to the Sheik, while Garth collected the reward. Hub then fought and won a duel against the Sheik but spared his life. He warned the Sheik if this vendetta didnt end, his life would; this ends the Sheiks manhunt. When Walter asks to hear more, Garth says he must find out the rest from Hub.

Later, Walter awakens Hub from a bout of sleepwalking to ask about Jasmines fate. Hub reveals Jasmine and their unborn child died in childbirth. Knowing no other life, Hub returned to the Legion to escape his grief, until he retired with Garth to their Texas farm. Walter then realizes Garths stories might be true, but asks Hub to confirm it, since his mother lies to him. Hub responds with a piece of his "What Every Boy Needs to Know ..." speech, that the actual truth is not as important as the belief in ideals like good winning over evil, honor, and true love. Seeing how much Hub misses his Jasmine, Walter asks Hub to promise to be around to give him the rest of the speech when hes old enough; Hub grudgingly agrees. As a result, Walter and his uncles form an even closer bond. Late one evening, Walter awakens to see Garth walking out to the barn and secretly follows him, trailing him to a room underneath the barn, which is filled with money.
 bank robbers, that Jasmine was their accomplice, and the money is theirs for the taking. To Maes dismay, Walter chooses to believe in his uncles instead of her. Angered, Stan drags Walter to the barn. Walter then kicks him between the legs and runs to the house, past the cornfield. Stan pins Walter down and begins beating him. Sensing Walter in danger, the lioness emerges from the cornfield and mauls Stan. Awakened by the ruckus, Hub and Garth find the old lioness died of heart failure. Hub and Garth explain Jasmine was "protecting her cub", and Walter proudly observes she was "a real lion ... at the end".
 PTA meetings, and to stop doing dangerous stunts, as he wants them to die of old age.

Seventeen years later, an adult Walter (Josh Lucas), has become the cartoonist of a comic strip Walter and Jasmine, based on his experiences with his uncles, now both in their 90s. He is alerted by the sheriff of his uncles deaths from a failed flying stunt with their biplane. Arriving at their farm, Walter is given his uncles will declaring "The kid gets it all. Just plant us in the damn garden, next to the stupid lion." A helicopter bearing the logo Sahara Petroleum then touches down near the homestead, and a man (Eric Balfour) steps out with his young son. Approaching Walter, he explains while visiting nearby for a business trip, he heard about Hub and Garths deaths on the news and recognized the names as the two Americans in tales told to him as a young boy by his grandfather, "a very wealthy sheik. He called them &nbsp;my most honored adversaries. The only men who ever outsmarted me.&nbsp;" When the mans young son asks Walter if his uncles were indeed real, that they really lived, Walter confirms, "Yeah. They really lived."

==Cast==
* Haley Joel Osment as Walter Caldwell
* Robert Duvall as Hub McCann
* Michael Caine as Garth McCann
* Kyra Sedgwick as Mae Caldwell
* Nicky Katt as Stan
* Josh Lucas as Adult Walter Caldwell Michael ONeill as Ralph Deirdre OConnell as Helen
* Christian Kane as Young Hub
* Daniel Brooks as Sheiks Great-Grandson
* Kevin Haberer as Young Garth
* Eric Balfour as Sheiks grandson
* Emmanuelle Vaugier as Jasmine
* Adam Ozturk as The Sheik
* Adrian Pasdar as Skeet Machine Salesman, aka clay pigeon launcher
* Mitchel Musso as Boy
* Marc Musso as Boy
* Jennifer Stone as Martha
*   as Jasmine the Lion
* Billy Joe Shaver as delivery truck driver

==Production== test audience and a new ending was shot.    In the original ending to the film, instead of the sheiks grandson, a tractor-trailer pulls up at the gravesite and a detachment from the French Foreign Legion rides out on horseback and act as an Honor Guard escorting two riderless horses with boots inserted backwards in the stirrups in honor of the brothers. Shortly thereafter, the sheik himself, elderly and using a wheelchair, arrives in a limousine surrounded by his harem to pay his respects. The four greasers that Hub beat up also make an appearance at the funeral, showing that Hubs speech did have an impact, as the men are now mature and respectable. 

The comic strips drawn by the adult Walter in the film were drawn by Berkeley Breathed, creator of Bloom County. 

==Music== Help Me" Sonny Boy Williamson).
 The 5th Avenue Theatre in 2013 from September 6 to October 7.  Music and lyrics were by Zachary and Weiner, with book by Rupert Holmes. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 