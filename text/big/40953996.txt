Call Center Girl
{{infobox film
| image            = Call_Center_Girl.jpg
| caption          = Theatrical movie poster
| name             = Call Center Girl
| director         = Don Cuaresma
| producer         =  
| music            = Vincent de Jesus
| cinematography   = Charlie Peralta
| story            = Enrico Santos
| screenplay       =  
| writer           = Kriz Garmen 
| editing          = Beng Bandong
| starring         =  
| studio           = Skylight Films
| distributor      = Star Cinema
| released         =  
| country          = Philippines
| language         =  
| runtime          = 107 minutes
| budget           =
| gross            = PHP 50,105,891  
 
}}
 family  comedy drama drama film directed by Don Cuaresma, starring Pokwang, Jessy Mendiola, and Enchong Dee. The film is produced by Skylight Films and Star Cinema. It premiered on November 27, 2013 as part of Star Cinemas 20th Anniversary presentation.            

The film also marks as Pokwangs 11th film under Star Cinema. 

== Cast ==

===Main Cast===
*Pokwang as Teresa "Terry" Manlapat
*Jessy Mendiola as Regina "Reg" Manlapat
*Enchong Dee as Vince Sandoval
*K Brosas as Lolay
*John Lapus as Ritchie
*Alex Castro as Martin
*Ogie Diaz as Midang
*Ejay Falcon as Dennis

===Supporting Cast===
*Arron Villaflor as Perry Manlapat
*Dianne Medina as Claire Manlapat
*Natasha Cabrera as Teresas Co Call Center Agent
*Dawn Jimenez as Lea

===Special Participation===
*Jestoni Alarcon as Raul Manlapat
*Janice de Belen as Vinces Mother
*Jayson Gainza as Vendor
*Pooh as Interviewer
*Chokoleit as Trainer Chokie
*Rufa Mi
*Thou Reyes
*Tado Jimenez as Security Guard
*Cheridel Alejandrino as Elevator Girl

== References ==
 

==External links==

 
 
 
 
 
 
 