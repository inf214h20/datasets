Bill Bergson Lives Dangerously (1996 film)
{{Infobox film
| name           = Kalle Blomkvist &ndash; Mästerdetektiven lever farligt
| image          = 
| image size     = 
| director       = Göran Carmback
| producer       = Waldemar Bergendahl
| screenplay     = Johanna Hald
| based on       =  
| starring       = 
| music          = Peter Grönvall
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} the novel with the same name, written by Astrid Lindgren.
 Fredrik and Christina Ådén.

==About the film==
An earlier film based on the book was recorded in 1957, see Bill Bergson Lives Dangerously (1957 film). In this second film one thing has been changed – in the book the city is called Lillköping but in this film it is called Storköping, according to the address the murderer wrote on a letter he sent to Eva-Lotta.

The city scenes were recorded in Norrtälje and the countryside scenes were recorded in Västerhaninge.

==Cast==
*Malte Forsberg as Kalle Blomkvist, "Vita Rosen"
*Josefin Årling as Eva-Lotta Lisander, "Vita Rosen"
*Totte Steneby as Anders, "Vita Rosen"
*Victor Sandberg as Sixten, "Röda Rosen"
*Johan Stattin as Jonte, "Röda Rosen"
*Bobo Steneby as Benke, "Röda Rosen"
*Claes Malmberg as Björk, policeman Peter Andersson as commissioner Strand
*Lakke Magnusson as Gren
*Krister Henriksson as Grens murder
*Leif Andrée as the baker, Eva-Lottas father
*Catherine Hansson as Eva-Lottas mother
*Ulla Skoog as Ada, Sixtens aunt
*Erika Höghede as Sixtens mother
*Toni Wilkens as Sixtens father
*Jacob Nordenson as the doctor, Benkes father
*Gerd Hegnell as Mrs Karlsson
*Per Morberg as the contributing editor of newspaper
*David Olsson as Fredrik

==External links==
 
*  
*  
*  

 

 
 
 
 


 