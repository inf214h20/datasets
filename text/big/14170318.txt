Macabre (1958 film)
{{Infobox film
| name           = Macabre
| image          = Macabre-large.jpg
| image_size     =
| caption        =
| director       = William Castle
| producer       = William Castle
| writer         = Robb White Theo Durrant (novel)
| narrator       = William Prince Christine White Jacqueline Scott Susan Morrow
| music          = Les Baxter
| cinematography = Carl E. Guthrie
| editing        = John F. Schreyer Allied Artists
| released       = October 1958
| runtime        = 72 minutes
| country        = United States English
| budget         = $90,000 (estimated)
| gross          = $5,000,000 (USA) ( January 1970) (sub-total)
| preceded_by    =
| followed_by    =
}} William Prince, Christine White, Jacqueline Scott, and Susan Morrow. Some critics call this a suspense film, rather than a horror film, having established a "racing against time" plot-line. 

It involved one of Castles first forays into using the   that later made him famous.   A certificate for a $1,000 life insurance policy from Lloyds of London was given to each customer in case he/she should die of fright during the film.     Some showings also had ushers dressed in surgical garb with an ambulance parked outside.

== Plot ==
 
Doctor Rod Barretts (William Prince) young daughter has been kidnapped by a mysterious maniac who has buried her alive "in a large coffin". The doctor has five hours in which to find and rescue her before her air runs out and she suffocates.  The maniacal killer apparently, also murdered Doctor Barretts wife and sister.

Various family members and friends become potential suspects as they help in the search, wandering through dark graveyards, crypts, thunder and lightning, and red-herrings. The somewhat muddy plot leads to a surprise conclusion that reveals the guilty party and the motive.  At the films end, a narrator requests that the audience not reveal the unexpected ending to others.

== Production ==
Castle mortgaged his house to make the thriller independently.  Filming took place from August 15 to 23, 1958.

== Cast == William Prince as Doctor Rodney Barrett
*Jim Backus as Jim Tyloe, the town police chief Christine White as Nancy Wetherby
*Jacqueline Scott as Polly Baron, Barretts assistant
*Susan Morrow as Sylvia Stevenson

== Reception ==
 

== References ==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 