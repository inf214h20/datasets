Reckless (1935 film)
{{Infobox film
| name           = Reckless
| image          = Reckless1935movie.JPG
| starring       = Jean Harlow William Powell Franchot Tone May Robson 
| director       = Victor Fleming
| producer       = David O. Selznick Oliver Jeffries 
| screenplay     = P. J. Wolfson
| distributor    = 
| released       =  
| runtime        = 97 minutes
| language       = English
| music          =   George Folsey
| editing        = Margaret Booth
| country        = United States
}}
Reckless is a 1935 American musical film directed by Victor Fleming and starring Jean Harlow, William Powell and Franchot Tone. David O. Selznick wrote the story, using the pseudonym Oliver Jeffries, basing it loosely based on the scandal of the 1931 marriage between torch singer Libby Holman and tobacco heir Zachary Smith Reynolds and his death by a gunshot wound to the head. {{cite book
| last1                 = Sragow
| first1                = Michael
| author-link1          = Michael Sragow
| year                  = 2013
| title                 = Victor Fleming: An American Movie Master
| publisher             = University Press of Kentucky
| page                  = 220
| isbn                  = 9780813144436
}} 

==Plot==
Musical stage star Mona Leslie (Jean Harlow), jailed for reckless driving, is bailed out by her friend, sports promoter and gambler Ned Riley (William Powell), to headline a charity event. However, she finds that all the seats have been bought by wealthy Bob Harrison Jr. (Franchot Tone), president and only member of S.A.M.L. (the Society for the Admiration of Mona Leslie). Mona begins dating Bob, with Neds approval. Monas Granny (May Robson) tells Ned that her granddaughter would break it off if he asked her to. Ned is reluctant at first, but eventually buys a wedding ring. However, he is too late.

One night, while they are very drunk, Mona and Bob get married. The next day, Mona is pleased, but Bob becomes depressed when he considers what his upper class friends and family will think, especially his father, Colonel Harrison (Henry Stephenson), and his fiancée and friend since childhood, Jo Mercer (Rosalind Russell). Though Jo welcomes Mona without resentment, the colonel and the rest of Bobs social circle are cold toward her. Bob wants to run back to New York, but Mona advises him to stay and stick it out.

Bobs ambivalent feelings emerge when Jo gets married. He avoids the wedding and starts drinking, unable to endure the thought of Jo with another man. When he shows up and speaks to Jo privately, he tells her how he really feels. Mona overhears when he says he was trapped into marriage. With no place else to go, she asks Ned to take her to his hotel suite. Bob follows and tries to pick a fight, but is too drunk to do anything serious. Ned and Mona put him to bed, but when they leave the room, Bob kills himself.

Both Ned and Mona are subjected to a coroners inquest and suspected of murder, but Bobs death is ruled a suicide. However, in the eyes of the public, Mona is still guilty of driving Bob to his death.

Mona gives birth to Bobs son. She offers to give up her inheritance of one million dollars if Colonel Harrison will agree not to seek custody of her child. He agrees.

To support her son, Mona tries to go back to work, but outraged people organize a campaign against her and nobody will hire her other than a sleazy promoter who wants to take advantage of her notoriety. Ned secretly finances a show for her, but his lawyer, worried that Ned is risking bankruptcy, tells Mona. She offers to stop production, but Ned refuses to listen and the show goes on.

On opening night, Jo and Colonel Harrison are in the audience. Mona starts off with a song, but hecklers make it impossible to continue. She quiets the crowd with a forceful justification of her actions and starts over. When she is finished, the audience gives her a standing ovation. During her next song, Ned proposes to her from the sideline.

==Cast==
 
 
 
 
*Jean Harlow as Mona
*William Powell as Ned Riley
 
*Franchot Tone as Bob Harrison
*May Robson as Granny
*Ted Healy as Smiley
*Nat Pendleton as Blossom
*Rosalind Russell as Jo
 
*Mickey Rooney as Eddie
*Henry Stephenson as Harrison
*Man Mountain Dean|Man-Mountain Dean as Himself
*Robert Light as Paul Mercer Allan Jones as Allan
*Carl Randall as Himself
 
 
Actress Nina Mae McKinney also appears in a credited role as Herself in the film.

==Music==
Jean Harlows voice was dubbed by vocalist Virginia Verrill who also performed songs in Suzy (1936 film)|Suzy and The Goldwyn Follies. Harlow later sang the title track for a radio broadcast in January 1935. 
#"Reckless" - Virginia Verrill  Allan Jones
#"Evrythings Been Done Before" - Allan Jones
# "Hear What My Heart Is Saying" - Virginia Verrill

==Production==
Reckless had several working titles including Salute, There Goes Romance, and A Woman Called Cheap. Ten writers, including Joseph Mankiewicz, Philip Barry, S. N. Behrman, and Val Lewton had some involvement, but only P. J. Wolson got credit for the final script.  Joan Crawford was cast as the lead. However, one week before production Harlow replaced Crawford, as David O. Selznick had decided that Powells real-life romance with Harlow would help to publicize the film. Harlow was reluctant to be in the film as her husband Paul Bern (like the husband of her character Mona Leslie) had committed suicide two years earlier. 

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 