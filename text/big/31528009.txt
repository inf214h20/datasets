The $5,000,000 Counterfeiting Plot
{{Infobox film
| name           = The $5,000,000 Counterfeiting Plot
| image          = 
| image_size     = 
| caption        = 
| director       = Bertram Harrison
| producer       = 
| writer         = 
| narrator       = 
| starring       = William J. Burns
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =   reels
| country        = United States Silent
| budget         = 
}}
 thriller directed by  Bertram Harrison.   

==Synopsis==
A member of a counterfeiting gang gives a forged note to his daughter. When she spends it on a dress the note ends up in the hands of the secret service, who then bring the entire gang to justice.

==Cast==
{| class="wikitable"
|-
! Actor/Actress || Role
|-
| William J. Burns || Himself
|- Glen White || John Walton
|- Joseph Sullivan || Joseph Fennell
|-
| Cliff Saum || William Kendall
|-
| Hector Dion || Edward Jackson
|-
| Jack Sharkey || Frank Tyler
|-
| William Cavanaugh || Arthur Borden
|- Charles Graham || James Long
|-
| Harry Lillford || George Peters
|-
| James Ayling || Robert Smith
|-
| John Ransom || John Knox
|-
| Arthur Morrison || Charles Carruthers
|-
| Frank Carrington || William Myers
|-
| Henry Driscoll || Samuel Parks
|-
| Jack Drumier || Chief of the Secret Service
|}

==References==
 

 
 
 
 
 
 

 
 