Kaadhal Kondein
 
 
{{Infobox film
| name           = Kaadhal Kondein
| image          = Kaadhal Kondein poster.jpg
| caption        = 
| director       = Selvaraghavan
| producer       = Dr. K. Vimalageetha
| writer         = Selvaraghavan Dhanush Sonia Nagesh Sudeep Daniel Balaji Sreekanth
| music          = Yuvan Shankar Raja
| cinematography = Arvind Krishna
| editing        = V. T. Avinash
| studio         = R. K. Productions
| distributor    = ITV Bombay
| released       = 4 July 2003
| runtime        = 165 mins
| language       = Tamil
| country        = India
}}
 Indian Tamil Tamil psychological Nagesh and Telugu in Veda replacing Kannada in Bengali in 2010, as Amanush (2010 film)|Amanush starring Soham Chakraborty and Srabanti Malakar.

The story explores the mind of a youth who is mentally and physically abused in his childhood. The lack of a mothers love haunts the protagonist throughout the film as the girl of his infatuation is killed. Becoming a psychopath, he desperately tries to woo his newly found lady love and his efforts culminate in a superb nail-biting cliffhanger.

==Plot==
Vinod (Dhanush (actor)|Dhanush), who has grown up under the care of a church father (Nagesh (actor)|Nagesh), is an introvert but a genius. He is forcibly sent to college by the father but is a complete misfit in class. Though shunned by the rest of his class, Divya (Sonia Agarwal) becomes his friend and he gradually warms up to her too. His feelings soon turn into love but he realizes that Divya considers him as only a friend. But he is unwilling to let her go. Meanwhile Vinod learns that Divya is in love with another classmate, Aadhi (Sudeep).

Divyas father is enraged on learning about her love. He shuts her up and prevents her from contacting anyone. But Vinod comes and meets her on the pretext of getting some old clothes for himself to wear. Pitied by Vinod, her father allows him. But Vinod uses the chance and escapes with Divya. He convinces her that she will meet Aadhi at Ooty.

Vinod has set up a secret place in Ooty for executing his plan of wooing Divya. He makes her stay with him, while convincing her by talking about the never-impending Aadhis arrival. On one such day, he reveals his miserable past, when he was made to work for paid labour after being orphaned at an early age. He revolts against the oppression one day against the illegal child labour in vogue at his place. Promptly he is beaten black and blue for his profanity. Moreover he also loses his girlfriend to rapists in that place, who also kill her. After some days he kills the woman running the illegal child labour place and also kills the rapist along with other children. They manage to break the place and escape from there and seeks refuge in the place of a church father.

Divya is really touched by his past. Incidentally the police and Aadhi arrive at the place. While Vinod was away to get some food, they try to make Divya understand that Vinod was a psychopath. Yet Divya scoffs at their claims, citing his gentlemanly behaviour over the days she was put up alone with him. Vinod, learning that the police have arrived at the scene, begins to indulge in mass violence. He opens fire, killing a police constable. Forcing them out of their hideout, he manages to evade the police Inspector and Aadhi and successfully brings Divya back to their original place of stay.

Divya soon identifies the tiger out of the cows skin. Vinod pleads with her, telling her that all he wanted in his life was her presence with him. But Divya called him a friend and stated her inability to accept him as her partner for life.

Meanwhile Aadhi regains consciousness and comes back to attack Vinod and rescue his girlfriend. A violent fight follows, where Vinod defies his puny self and treats Aadhi with disdain. The fight culminates with Vinod, Aadhi and Divya teetering at the edge of a slippery cliff.

While Divya clutches a tree bark tightly, Vinod and Aadhi slip out and barely manage to hold either of her hands. Divya is forced to a situation where she needs to choose between her boyfriend and friend. Aadhis pleas notwithstanding, Divya doesnt have the heart to kill Vinod. The epic of a cliffhanger finally ends with Vinod smiling wryly at Divya and letting go of her hands himself. He falls to his death into the abyss.

==Cast== Dhanush as Vinod
* Sonia Agarwal as Divya
* Sudeep as Aadhi Nagesh as Father Rozario
* Daniel Balaji as Police Inspector
* Sreekanth as Divyas father
* Gowthami
* Rambo Rajkumar

==Soundtrack==
{{Infobox album |  
| Name = Kaadhal Kondein
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Kadhal konden cd cover.jpg
| Released = 20 March 2003
| Recorded = 2002 / 2003 Feature film soundtrack
| Length = 34:34 30:18 (OST release)
| Label = Five Star
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Winner (film)|Winner (2003)
| This album  = Kaadhal Kondein (2003)
| Next album  = Aadanthe Ado Type (2003)
}}

For Kaadhal Kondein, director Selvaraghavan and music composer Yuvan Shankar Raja came together again after their earlier successful collaboration in Thulluvadho Ilamai (2001), for which Selvaraghavan worked as a writer. The soundtrack of Kaadhal Kondein released on 20 March 2003, featuring seven tracks with lyrics written by Pazhani Bharathi and Na. Muthukumar. The music, especially the film score, received universal critical acclaim, establishing composer Yuvan Shankar Raja as one of the "most sought after music directors" in the Tamil film industry. 

More than one year after the release of the film, an "original soundtrack" was released, that followed the Hollywood-style. It was said to be the first time, that an original soundtrack was released for a film in India as the soundtracks released in India do not contain any film score pieces but full songs that feature in the film itself. The OST of Kaadhal Kondein contains 20 tracks overall, which includes the seven earlier released tracks, four "montage" bit songs, that featured in the film, but not in the soundtrack, and nine pieces from the actual film score, which were titled as "Theme Music".

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyricist !! Notes
|- 1 || "Devathaiyai Kandaen" ||   ||
|- 2 || "Manasu Rendum" || Shankar Mahadevan || 6:41 || Pazhani Bharathi ||
|- 3 || Sujatha || 6:25 || Na. Muthukumar ||

|- 4 || "Kadhal Mattum Purivathillai" || Vijay Yesudas || 6:07 || Pazhani Bharathi ||
|- 5 || "Thottu Thottu" || Harish Raghavendra || 5:16 || Na. Muthukumar ||
|- 6 || "18 Vayathil" || Yuvan Shankar Raja || 4:49 || Na. Muthukumar ||
|- 7 || "Kaadhal Kondein" (Theme Music) || Instrumental || 1:07 || ||
|}

{{tracklist
| headline     = Original Soundtrack release
| extra_column = Artist(s)
| total_length = 30:18
| all_music    = Yuvan Shankar Raja
| title8       = Thathi Thathi
| extra8       = Yuvan Shankar Raja
| length8      = 2:48
| title9       = Natpinilae
| extra9       = Harish Raghavendra
| length9      = 1:19
| title10      = Unnai Thozhi Ranjith
| length10     = 2:05
| title11      = Kai Padamalae
| extra11      = Yuvan Shankar Raja
| length11     = 2:16
| title12      = Theme Music 1
| extra12      = Instrumental
| length12     = 2:42
| title13      = Theme Music 2
| extra13      = Instrumental
| length13     = 2:12
| title14      = Theme Music 3
| extra14      = Instrumental
| length14     = 0:58
| title15      = Theme Music 4
| extra15      = Instrumental
| length15     = 2:05
| title16      = Theme Music 5
| extra16      = Instrumental
| length16     = 2:24
| title17      = Theme Music 6
| extra17      = Instrumental
| length17     = 1:46
| title18      = Theme Music 7
| extra18      = Instrumental
| length18     = 2:29
| title19      = Theme Music 8
| extra19      = Instrumental
| length19     = 4:31
| title20      = Theme Music 9
| extra20      = Instrumental
| length20     = 2:43
}}

==Awards==
* 2004: Filmfare Best Female Debut (South) - Sonia Agarwal
;Nominations Filmfare Best Director Award - Selvaraghavan

==Box office==
*The film grossed    at the box office. 
*The story also opened up a new avenue of films with similar storylines like Chellamae and Kudaikkul Mazhai.

==External links==
*  

==References==
 

 

 
 
 
 
 
 
 
 