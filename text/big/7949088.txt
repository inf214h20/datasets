The Story of Woo Viet
 
 
 
{{Infobox film
| name           = The Story of Woo Viet
| caption        = 
| image	=	The Story of Woo Viet FilmPoster.jpeg
| director       = Ann Hui
| producer       = Teddy Robin Kwan
| writer         = Alfred Cheung
| starring       = Chow Yun-fat, Cora Miao, Lo Lieh, Cherie Chung
| music          =  )
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 92 min
| country        = Hong Kong  Cantonese
| budget         = 
}}
 action choreographer was Ching Siu-tung.
 Vietnamese refugees sovereignty at the time. It is also the second part of Ann Huis Vietnamese trilogy.

The movie features a famous Cantonese song, "This is Love", sung by Teddy Robin Kwan, the producer of this movie.

In the United States, the film is marketed under the title God of Killers, capitalizing on Chows popularity in heroic bloodshed films.

Woo Viet, as he is Vietnamese, should actually have the surname of Hồ (as in the "Ho" in Ho Chi Minh) rather than the Cantonese translation of the Hu / Woo / Ho surname. 

==Plot==
The story features Woo Viet, who wants to leave his country, Vietnam, behind and start over in the United States. But he first must make his way to Hong Kong. In a refugee detention camp there, he discovers many of his countrymen are disappearing under mysterious circumstances. As Woo tries to find out what is happening, he realizes his life is in danger, and has to leave for the United States immediately using a false passport instead of seeking Right of asylum|asylum. In the process, he meets a beautiful woman, Cham Thanh, who then travels with him. When Woo and his new love stop over in the Philippines, they discover that the females are conned to stay there to become prostitutes. Instead of taking the plane to the United States, Woo Viet decides to stay in the Philippines to save his love. However, as he is stranded in Manilas Chinatown, Woo Viet is forced to work as a hired killer.

==Cast==
*Chow Yun-fat
*Cora Miao
*Lo Lieh
*Cherie Chung
*Dave Brodett

==External links==
* 
*  

 

 
 
 
 


 