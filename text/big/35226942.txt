Sundara Pandian
: Lakshmi Menon.
{{Infobox film
| name           = Sundara Pandian
| image          = 
| caption        = 
| director       = R. Raghu
| producer       = R. Raghu
| writer         = T. K. Bose
| starring       =   Deva
| cinematography = B. Balamurugan
| editing        = P. R. Shanmugam
| studio         = Jupiter Film Makers
| distributor    = Jupiter Film Makers
| released       =  
| country        = India
| runtime        = 135 min
| language       = Tamil
}}
 1998 Cinema Indian  Tamil film, Swathi and Deva and was released in 1998.  
 
==Plot==
Pandi (Karthik (actor)|Karthik) is an uneducated villager who hes in love with his niece Karthika (Swathi (actress)|Swathi) but she doesnt love him. 
Sundar, son of a rich businessman, lives without his fathers love in the city.

One day, Pandi decides to leave his village to earn money and find a wife. Meanwhile, Sundar comes to Pandis village to swim at the river, and the villagers catch him. After seeing Pandis mother, Sundar stays there and he gets the mother affection.

Pandi doesnt find a job, Sundars father brought Pandi with him and thought that his son became mad. There, Ramya (Heera Rajagopal), a police officer falls in love with Pandi. Kottaval (Manivannan), Ashok Rajs enemy, escapes from the psychiatrist hospital to kill Ashok Raj and his son Sundar.

Karthika falls in love with Sundar and Karthikas father (Alex (actor)|Alex) prevents her to forget him. The enemy of Karthikas father decides to kill Karthikas father but Sundar saves him. Karthikas father apologize to his elder sister and plans to marry his daughter to his savior. Sundar reveals to Karthika his real identity.

Sundar leaves the village to find Pandi. Kottaval and his henchmen kidnap Ashok Raj and Pandi. Sundar saves them and sends Kottaval to jail.

==Cast== Karthik as Pandi / Sundar Swathi as Karthika
*Heera Rajagopal as Ramya
*Vadivelu as Pavadai
*Manivannan as Kottaval Alex as Karthikas father
*Vasu Vikram as Vasu

== Soundtrack ==
{{Infobox album |  
  Name        = Sundara Pandian |
  Type        = soundtrack | Deva |
  Cover       = |
  Released    = 1998 |
  Recorded    = 1998 | Feature film soundtrack |
  Length      = |
  Label       = | Deva |  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1998, features 5 tracks with lyrics written by Kalidasan, Ponniyin Selvan and Vasan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration 
|-  1 || Mano || 5:14
|- 2 || "Beeda Beeda" || Krishna Raj ||  4:18
|- 3 || "Sendhoora Poove" || Mano, Devi || 5:12
|- 4 || "Kayil Vandha" || S. P. Balasubrahmanyam, K. S. Chitra || 5:08
|- 5 || "Vaadi Pula" || Malaysia Vasudevan || 4:18
|}

==References==
 

 
 
 
 
 