Jill Rips
{{Infobox film
| name           = Jill Rips
| image          = Jill the rip dvd cover.png
| caption        = DVD cover
| director       = Anthony Hickox
| producer       = Damian Lee
| writer         = Gareth Wardell Kevin Bernhardt
| starring       = Dolph Lundgren Kristi Angus
| music          = Thomas Barquee Steve Gurevitch
| cinematography = David Pelletier
| editing        = Brett Hedlund
| distributor    = Sony Pictures Home Entertainment
| released       = 1999
| runtime        = 94 minutes
| country        = United States
| language       = English
| Budget         = 
| preceded by    =
| followed by    =
| awards         =
| Gross          =
}}
 novel by Scottish  writer Frederic Lindsay. The film is also known as Jill the Ripper and Tied Up. 

==Plot==
Matt Sorenson (Dolph Lundgren), a former boxer and San Francisco cop, now makes a living collecting debts for small businesses. The brutal death of his high-powered younger brother, Michael, changes all that forever. Intent on finding his brothers killer, Sorenson infiltrates the powerful inner world of politics, business intrigue, and casual sex. Rejecting the police and media theory that the murder is the work of a female prostitute, Sorensens focus falls on the corrupt big city businessman, Jim Conway. His obsession to discover the killers identity mounts as other men are found murdered in a similar fashion. Sorenson loses all objectivity and becomes a vigilante.

==Cast==
*Dolph Lundgren ... as Matt Sorenson 
*Danielle Brett ... as Irene 
*Sandi Ross ... as Mary 
*Charles Seixas ... as Big Jim Conway 
*Kristi Angus ... as Frances

==Novel reviews==
The novel had been described by the Today programme as harrowing, but   grim, poetic vision makes it the best novel of its kind for years.
The Sunday Times said that violent and vicious, Lindsays unsparing tale beds down with the imagination like a succubus.
Daily Express called it tautly and skillfully written—a genuine, cant-put-it-down, turn-off-the-telly-read.

==DVD release==
The movie was released on DVD on December 7, 2004 by Sony Pictures Home Entertainment. 

==References==
 

==External links==
* 

 

 
 
 


 