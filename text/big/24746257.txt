30 Days of Night: Dark Days
{{Infobox film
| name           = 30 Days of Night: Dark Days
| image          = 30 Days of Night - Dark Days.png
| caption        = DVD release cover
| director       = Ben Ketai
| producer       = Vicki Sotheran
| screenplay     = Steve Niles Ben Ketai
| based on       =  
| starring       = Kiele Sanchez Rhys Coiro Diora Baird Harold Perrineau Mia Kirshner
| music          = Andres Boulton
| cinematography = Eric Maddison
| editing        = Ben Ketai
| studio         = Dark Horse Entertainment Ghost House Pictures Stage 6 Films RCR Media Group 
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
30 Days of Night: Dark Days is a 2010 American   was released on  .

==Plot==
A year after the Alaskan town of Barrows population was decimated by vampires during its annual month-long polar night, Stella Oleson (Kiele Sanchez) travels the world trying to convince others that vampires exist.  She is fully aware of the risk to the life that her work could bring, but does not care due to her grief over the death of her husband Eben. 

Following instructions from a mysterious individual named Dane, she travels to Los Angeles to give a lecture on the existence of vampires. Aware that vampires attend when she speaks, she activates overhead ultraviolet lamps that incinerate several of the vampires in the audience in front of the humans. She is quickly arrested and harassed by Agent Norris who she learns is one of the human followers of the vampires, placed to keep their activities covered up. After her release from custody, she returns to her hotel to find Paul (Rhys Coiro), Amber (Diora Baird) and Todd (Harold Perrineau), sent by Dane to recruit her to hunt the vampire queen Lilith. As Lilith is responsible for the vampires every move and for keeping them hidden, the hunters are convinced that once she is eliminated, the vampires will fall into dormancy. When Stella is told Lilith was responsible for the incident at Barrow, she agrees to meet Dane (Ben Cotton) and is shocked to discover that he too is a vampire. Due to a superficially inflicted wound, he has maintained a grasp of humanity, only drinking blood from packaged hospital stocks he keeps. Stella is hesitant to join a plan to attack a vampire nest,  but Paul eventually convinces her, telling of his daughters death by vampire and his accusations of vampire involvement resulting in a divorce with his wife.

The following day, the four hunters enter a vampire nest and are ambushed by a group of them. During their attempt to flee, Todd is bitten. After the four lock themselves in a cellar room, Todd turns into a vampire. When Paul hesitates to act against his friend, Stella manages to kill Todd by smashing in his head with a cinder block. They decide to wait for night when the vampires go to feed in order to make their escape. After night falls, Dane comes and frees them. On their way out they capture a vampire and interrogate him with the ultraviolet lamps, eventually following him back to another nest. They invade the nest and rescue Jennifer, a captive used as a feeding station. Jennifers knowledge of Liliths lair being aboard a ship in the bay allows the hunters to plan an attack on Lilith directly. Returning to Danes place, Stella and Paul become intimate.

Meanwhile, Lilith (Mia Kirshner) decides that Agent Norris should prove his worth to become a vampire (in order to cure the cancer he has been suffering from). He bites the neck of a captive girl, Stacey (Katharine Isabelle), drinking her blood until dead. Satisfied, Lilith turns him to hunt Stella and the others.

Norris kills Dane and the others flee with Jennifer to a boat yard where Jennifer points out the boat that the vampires are set to sail to Alaska in for another 30 day feeding period. After telling Jennifer to leave, the three hunters stow away on the ship where they discover that they can be resurrected after death if they are fed human blood. At gunpoint, they confront the human captain who says he is cooperating because the vampires had threatened his family. Amber is suddenly pulled away from behind, causing her gun to fire and kill the captain. Stella and Paul are too late to save her from being eaten and are quickly captured by Norris and Lilith who orders that they be bled dry. Stella manages to free herself when they are alone with Norris and kills him, but they are subsequently attacked by Lilith when attempting to sabotage the ship and Paul is killed. After being outmatched in hand-to-hand combat, Stella hides from Lilith and when the queen comes looking for her, Stella emerges from her tub of blood and manages to decapitate her. The other vampires appear, but seeing that she killed Lilith, they quietly stand aside and let her pass without a fight, and she returns to Barrow.

Stella digs up Ebens grave and recovers his body to feed him her own blood. It appears not to work and she lies down slowly dying from blood loss. After a time, she sees Eben has returned to his former health and she stands to greet him with a hug. As they embrace, Eben pulls back her shoulder and his sharp teeth come down on her neck before the screen goes dark.

==Cast==
* Kiele Sanchez as Stella Oleson
* Rhys Coiro as Paul
* Diora Baird as Amber Lilith
* Monique Ganderton as Betty
* Harold Perrineau as Todd	
* Katharine Isabelle as Stacey
* Ben Cotton as Dane
* Troy Ruptash as Agent Norris
* Katie Keating as Jennifer
* Martha McBriar as Sandbag Jane
* Tapas Choudhury as Sandbag

Andrew Stehlin re-appears as Arvin in flashbacks using archive footage from the first film.

==Production==
Ben Ketai co-wrote the script with franchise creator Steve Niles.  Josh Hartnett (Eben) and Melissa George (Stella) did not return to their roles from the previous film.  The film was shot in Vancouver, British Columbia. 

==Release== 2010 San Diego Comic-Con with the completed cast.  The film was released direct to DVD and Blu-ray Disc in the United States on October 5, 2010 and includes an audio commentary, a featurette on the making of the film and exclusive wallpapers for the PlayStation 3. 

==Reception==
Dreadcentral.com gave the film three out of five stating, "Its dark, disturbing and nihilistic, and one hell of a ride."   Fangoria gave the film one and a half stars out of four stating, "Rather than attempt to stay true to the source material...   departs from it as the film progresses. What follows is a generic story with flat characters set in an uninspired locale."   Overall, the film received poor reviews from critics and it currently holds a 17% rating on Rotten Tomatoes. 

==Soundtrack==
Venezuelan guitarist and composer Andres Boulton wrote the music score. 

==See also==
*Vampire film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 