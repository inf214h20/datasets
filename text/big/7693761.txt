It's Always Fair Weather
 
{{Infobox film
| name           = Its Always Fair Weather
| image          = Its Always Fair Weather (1955 film) poster (yellow background).jpg
| image_size     =
| caption        =
| director       =  
| producer       = Arthur Freed
| writer         =  
| starring       =  
| music          = André Previn
| cinematography = Robert J. Bronner
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       = September 1, 1955
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget = $2,771,000  . 
| gross = $2,374,000   
}} MGM musical satire scripted by Betty Comden and Adolph Green, who also wrote the shows lyrics, with music by André Previn and starring Gene Kelly, Dan Dailey, Cyd Charisse, Dolores Gray, and dancer/choreographer Michael Kidd in his first film acting role.

The film, co-directed by Kelly and Stanley Donen, was made in CinemaScope and Eastmancolor. Although well-received critically at the time, it was not a commercial success, and is widely regarded as the last of the major MGM musicals. In recent years it has been recognized as a seminal film because of the inventiveness of its dance routines.

Its Always Fair Weather is noted for its downbeat theme, which may have hurt it at the box office, and has been called a rare "cynical musical".   

==Plot==

Three ex-G.I. (military)|G.I.s, Ted Riley (Kelly), Doug Hallerton (Dailey) and Angie Valentine (Kidd)  have served in World War II together and become best friends. At the beginning of the film, set in October 1945, they dance through the street celebrating their upcoming release from the service ("The Binge") and meet at their favorite New York bar. They vow eternal friendship, and before going their separate ways, promise to reunite exactly ten years later at the same spot.

In the years after the war, have taken entirely different paths ("10-Year Montage"). Riley, who had wanted to become an idealistic lawyer, has become a fight promoter and gambler, associating with shady underworld characters. Hallerton, who had planned to become a painter, has gone into a high-stress job in advertising, and his marriage is crumbling. Valentine, who had planned to become a gourmet chef, is now running a hamburger stand in Schenectady, New York that he calls "The Cordon Bleu", and has a wife and children.

The three men keep their promise to meet at the bar ten years later, and quickly realize that they now have nothing in common and dislike each other. Hallerton and Riley view Valentine as a "hick", while Riley and Valentine think Hallerton is a "snob", and Hallerton and Valentine think Riley is a "crook". Sitting together in an expensive restaurant as Hallertons guest, munching celery, they silently express their regrets in "I Shouldnt Have Come", sung to the tune of "The Blue Danube".

At the restaurant, they encounter some people from Hallertons advertising agency, including Jackie Leighton (Charisse), an attractive and brainy advertising person. Jackie gets the idea of reuniting the three men later that evening on a TV show hosted by Madeline Bradville (Gray). She and Riley gradually become involved, though Jackie seems motivated by wanting to get Riley on her show. She joins Riley at Lou Stillman|Stillmans gym, where Jackie demonstrates a deep knowledge of boxing while cavorting with beefy boxers ("Baby You Knock Me Out").

Riley gets into trouble with gangsters because he refuses to fix a fight. As he seeks to evade gangsters from a roller skating ring, he skates out on the streets of Manhattan, where he realizes that Jackies affection for him has built up his self-esteem, and he dances exuberantly on roller skates ("I Like Myself"). Hallerton, meanwhile, has misgivings about the corporate life ("Situation-Wise").

The three men are reluctantly coaxed into the TV reunion, with the gangsters tracking Riley to the studio. They jointly fight the gangsters, which brings them back together. At the end they are friends again, but go their separate ways without making plans for another reunion ("The Time for Parting").

== Cast ==
*Gene Kelly as Ted Riley
*Dan Dailey as Doug Hallerton
*Cyd Charisse as Jackie Leighton
*Dolores Gray as Madeline Bradville
*Michael Kidd as Angie Valentine David Burns as Tim
*Jay C. Flippen as Charles Z. Culloran

==Production history==
 
 , Gene Kelly and Dan Dailey dancing on trash can lids in the "Binge" number]] On the Invitation to the Dance, to take advantage of a tax law for resident Americans. But the films in Europe failed and the tax law was revoked, forcing Kelly to return to America.
 Seven Brides Guys and Dolls, as well as The Band Wagon). Kelly was also forced to shoot the movie in Cinemascope, which he felt did not suit screen dancing. Many of the numbers in the film, such as "The Binge" and "Once Upon a Time" show Kellys efforts to make use of Cinemascope. Comden and Green wrote the songs with André Previn providing the music as well as the accompanying score; it was his first major assignment on an MGM film.

==Reception==

Its Always Fair Weather received good reviews when it came out, Variety (magazine)|Variety calling it a "delightful musical satire",  while the New York Times said it spoofed "the whiskers off TV".  It was also voted one of the years 10 best films by the NY Film Critics.   However, the studio did not open it with the fanfare it had given previous musicals. Instead it was released as part of a drive-in double bill with Bad Day at Black Rock and the studio did not make their money back. The films bleakness may have had something to do with it (audiences at the time were not accustomed to unhappy musicals); but also, more Americans were staying at home with television than going to the movies at this time. André Previn claims the films failure had to do with its being a musical: he felt that it would have been a good film had it not had any songs.

In her book 5001 Nights at the Movies, critic Pauline Kael called the film a "delayed hangover", and said that its "mixture of parody, cynicism and song and dance is perhaps a little sour". She singled out for praise Daileys "Situationwise" number, and said that "to a great extent this is Daileys movie".   

The film was also nominated in 1955 for Academy Awards for Best Scoring of a Musical Picture and for Best Story and Screenplay. 

In recent years, the film has gained reputation in the minds of musical aficionados and Kelly fans, who point to his tap dance on roller skates, "I Like Myself", as the last great dance solo of his career.    Scenes from the film were included in MGMs Thats Entertainment, Part II, hosted by Kelly and Fred Astaire.

==Box office==
According to MGM records the film earned $1,380,000 in the US and Canada and $994,000 elsewhere resulting in a loss of $1,675,000. 

==Soundtrack==
{{Infobox album  
| Name        = Its Always Fair Weather
| Type        = soundtrack
| Artist      = André Previn
| Cover       = Its Always Fair Weather (André Previn album) coverart.jpg
| Released    =
| Length      = 41:28
| Genre       = Jazz
| Label       = Sony Music (1991), Rhino Handmade
| Producer    = Dan Rivard
}}

Soundtrack recordings have been issued by Rhino Records and in 1991 by Sony Music.

Track listing:

Lyrics by Betty Comden and Adolph Green; music score by André Previn. All pieces played by MGM Studio Orchestra conducted by André Previn. Between brackets the singers.

# "Overture" 1:04
# "March, March" (Gene Kelly, Dan Dailey, Michael Kidd) 1:21
# "The Binge" 5:07
# "The Time For Parting" (Gene Kelly, Dan Dailey, Michael Kidd) 2:01
# "10-Year Montage" 2:18
# "The Blue Danube (I Shouldnt Have Come)" (Gene Kelly, Dan Dailey, Michael Kidd) 2:30
# "Music Is Better Than Words" (Dolores Gray) 2:10
# "Stillmans Gym" (Lou Lubin) 2:10
# "Baby You Knock Me Out" (Cyd Charisse, Lou Lubin) 2:40
# "The Ad Men" (Dan Dailey, Paul Maxey) 0:48
# "Once Upon A Time" (Gene Kelly, Dan Dailey, Michael Kidd) 3:33
# "Situation-Wise" (Dan Dailey) 2:49
# "The Chase" 1:04
# "I Like Myself" (Gene Kelly) 4:10
# "Klenzrite" (Dolores Gray) 1:34
# "Thanks A Lot, But No, Thanks" (Dolores Gray) 3:47 David Burns and chorus) 1:46

==References==
===Citations===
 

===Additional sources===
* 
*Hugh Fordin: The World of Entertainment, ISBN 978-0-380-00754-7

==External links==
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 