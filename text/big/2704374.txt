The Ring Virus
 
{{Infobox film
| name           = The Ring Virus
| image          = The Ring Virus.jpg
| caption        = Theatrical poster
| hangul         = 링(링 바이러스)
| rr             = Ring (Ring baireoseu)
| mr             = Ring (Ring pairŏsŭ)
| director       = Kim Dong-bin
| producer       = Jonathan Kim
| based on       =  
| writer         = Kim Dong-bin
| starring       = Shin Eun-kyung Lee Seung-hyeon Jung Jin-young
| music          = Il Won
| cinematography = Hwang Chul-hyun
| editing        = Kyung Min-ho
| distributor    =
| released       = 12 June 1999
| runtime        = 108 min.
| country        = South Korea
| language       = Korean
| budget         =
| admissions     = 332,354 (Seoul) 
}}
The Ring Virus ( ; lit. "Ring") is a South Korean horror film adapted from the Japanese novel Ring (Suzuki novel)|Ring by Koji Suzuki. A joint project between Japan and Korea, this version has Park Eun-Suh as the creator of the cursed videotape. Although the filmmakers claimed that the film was adapted from the novel, there are various scenes in the film that match the 1998 film Ring (film)|Ring, such as the sex of the lead character, some of the scenes on the videotape as well as copying other film scenes directly from the original film, including the films climax.

==Plot==
Followed by the death of her friends, journalist Hong Sun-Joo comes across a videotape containing incomprehensible images. Towards the end of the tape, she finds the curse which states that the viewer would die at the same time next week if he/she does not perform certain tasks. However, the next scene explaining the nature of the task has been erased. Sun-Joo and a doctor named Choi-Yul embark on a journey to break the curse placed upon them. They discover that the videotape was made by the psychic called Park Eun-Suh. Eun-Suh was an illegitimate daughter of a female psychic and was born a hermaphrodite. She was romantically involved with her half-brother and worked in a night club for a while. There, a man who found out about her secrets was killed as she had the uncanny ability to protect herself. The video tape is the medium Eun-Suh uses to reveal herself to the society. Her first exposure to the media was a painful experience, which caused her to withdraw from the outside world. When it became difficult for her to relate to the society, she retaliated by infiltrating it like a virus. The way of infiltration is one-way only and any attempt to block the process ends in extremely negative consequences.

==Differences between film and book== Koji Suzukis storyline, while keeping some elements first introduced with Ring.
* The main character from the novel is a man called Kazuyuki Asakawa, while in the film, the main character is a woman called Sun-Joo Hong.
* The villain from the novel is named Sadako Yamamura, while in the movie, she is called Park Eun-Suh.
* Ryuji in the novel is a philosophy professor who also achieved medical studies, whereas in the movie, Choi is a doctor who made a mystical pronouncement at the scene of one the deaths about supernatural forces having been at work.

==Similarities between film and book== Hideo Nakatas adaptation.
* The cursed tape in the movie is very similar to the one in the book, although the book-version is much longer and more complicated. Both videotapes feature a message at the beginning along the lines of "Watch until the end, you will be eaten by the lost," and ending with "Those that have viewed this tape are   to die at this exact time seven days from now. In order to survive, you must-" The rest of the end message is taped over, and it isnt until the end that Sun-Joo realizes that the rest of the message is about copying the tape and showing it to someone else.
* Choi also analyzes the tape sequences the same way Ryuji does in the book. He categorizes the parts into two categories: real scenes and abstract scenes. The realistic scenes are easy to spot, since they have dark blurry edges, and instants of darkness. Choi quickly concludes that those instants of darkness are eye blinks. The average man blinks twenty times per minute, whereas the average woman blinks fifteen times per minute. Considering this fact, this video was created by a woman, scenes filmed through her own eyes and images in her mind. testicular feminization syndrome, meaning she is anatomically female, except she has a pair of testes beneath her vagina (she evidently does not have a penis). The movie starts with Sun-Joo interviewing a gallery owner that explains the theme of her exposition: the beauty of women and the strength of men combine in one individual, a hint for those who have read the book.
* The movie is faithful to the storyline of the book, including the search for Sadako / Eun-Suhs clinical records, her origin story and her rape before being murdered.
* Like Sadako in the novels and nearly in Ring Kanzenban, Eun-Suh was raped before being thrown down the well. The rapist was her half-brother. Similarly to Sadako in Ring Kanzenban, she struggles to escape, defends herself by biting and drawing blood from her captives shoulder, and is ashamed when he discovers her Testicular Feminization Syndrome. Though its not as clearly explained, Eun-Suh appears to telepathically threaten to kill him for discovering her secret. Horrified, he strangles her unconscious before dropping her into a nearby well. In Eun-Suhs cursed tape this is briefly shown.
*Even though the film kills its supporting character the same way Ring did, the movie shares the book views on a pseudo-science-fictitious medical-mystery approach with its title and Chois scientific research on viruses, and the conclusions he makes before his death.

==References==
 

==External links==
* 
* 
*  Comparison of Ring, The Ring, and The Ring Virus movies.

 

 

 
 
 
 
 
 
 
 