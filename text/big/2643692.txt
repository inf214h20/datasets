Lord of War
 
{{Infobox film
| name           = Lord of War
| image_size     = 215px
| image          = Lordofwar.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew Niccol Chris Roberts Nicolas Cage
| writer         = Andrew Niccol
| narrator       = Nicolas Cage
| starring       = Nicolas Cage Jared Leto Bridget Moynahan Ian Holm Ethan Hawke Antonio Pinto
| cinematography = Amir Mokri
| editing        = Zach Staenberg
| studio         =   Ascendant Pictures Saturn Films
| distributor    = Lionsgate Films
| released       =   January 4, 2006   February 16, 2006  }}
| runtime        = 123 minutes
| country        = United States Germany    France
| language       = English 
| budget         = $42 million   
| gross          = $72.6 million 
}}
 crime war film  written, produced and directed by Andrew Niccol and co-produced by and starring Nicolas Cage. It was released in the United States on September 16, 2005, with the DVD following on January 17, 2006 and the Blu-ray Disc on July 27, 2006. Cage plays an illegal arms dealer with similarities to post-Soviet arms dealer Viktor Bout.    The film was officially endorsed by the human rights group Amnesty International for highlighting the arms trafficking by the international arms industry.  

==Plot details== For What Its Worth", and depict the life of a 7.62×39mm cartridge from construction in a  Soviet Union weapons factory, to being shipped across the world to an African warzone, loaded into the magazine of an AK-47, and fired into the head of a child soldier.
 Russian mobster kills two would-be assassins. He is inspired to go into the arms trade, comparing the constant need for weapons to the similar human need for food. At his fathers synagogue, he contacts an Israeli to obtain an illegal Uzi. After completing the first sale, Yuri convinces his brother Vitaly (Jared Leto) to become his partner, and they leave their jobs at the family restaurant behind.

Yuris first big break comes in the 1982 Lebanon War, when he sells guns to all sides of the conflict, despite witnessing war crimes and atrocities. As Yuri becomes more successful in the wars aftermath, his business comes to the attention of Interpol, and in particular idealistic agent Jack Valentine (Ethan Hawke). Valentine is after glory rather than money, making it impossible for Yuri to bribe him as he does other government agents within Interpol and elsewhere in 1989.

During a sale in Colombia, a drug lord pays with six kilos of cocaine instead of cash, and shoots Yuri with one of his own pistols when the two argue. Yuri relents, later finding the sale of the cocaine paid better than money would have.  After sampling their profits, Vitaly becomes heavily addicted and eventually burns through an entire kilo. After several months, Yuri checks Vitaly into drug rehabilitation|rehab, and continues alone. He lures childhood crush Ava Fontaine (Bridget Moynahan) to a false photo shoot and subsequently marries her. They later have a son, Nikolai (Nicky).
 Soviet General. arms dealer, prompting her to confront him and demand he stop his illegal business. For a time, Yuri agrees, but Andre Baptiste Sr. offers him even more money and soon he goes back.
 RUF soldiers shoot him. Yuri approaches his dying brother, and restores a grenade pin and tosses the deactivated grenade to one of the killers responsible. The diamond payment is halved for the remaining weapons.

At home, Ava follows Yuri to his secret shipping container of supplies. She leaves with their son and Yuris parents disown him after learning the truth. When the U.S. Customs finds a bullet in Vitalys corpse, Valentine arrests Yuri, who predicts, correctly, that a knock at the door will signal his release as a "necessary evil" who distributes weapons so major governments can deny involvement. Despite his losses, Yuri returns to arms dealing. Yuri remarks that its what he does best, and that arms dealers are most likely to inherit the world one day "because everyone else is too busy killing each other." His final advice to the viewer is, "Never go to war, especially with yourself."

An onscreen postscript states that private dealers conduct less business than the five largest arms exporters the United States, United Kingdom, Russia, France, and China who  are the five permanent members of the United Nations Security Council.

==Cast==
* Nicolas Cage as Yuri Orlov
* Ethan Hawke as Jack Valentine
* Jared Leto as Vitaly Orlov
* Bridget Moynahan as Ava Fontaine
* Eamonn Walker as André Baptiste Sr.
* Ian Holm as Simeon Weisz
* Tanit Phoenix as Candy
* Donald Sutherland (voice only) as Colonel Oliver Southern
* Weston Coppola Cage as Vladimir
* Sammi Rotibi as André Baptiste, Jr. Eugene Lazarev as General Dmitri Orlov
* Kobus Marx as Boris
* Liya Kebede as Faith
* Jasmine Sais Burgess as Gloria

==Historical accuracy==
Plot details on the illegal arms market, particularly regarding purchases for West Africa in early 1990s, are closely based on real stories and people originating from the former Soviet Union.
* The main protagonist, Yuri Orlov, is loosely based on several people. Armenian arm dealer Sarkis Soghanalian.
** He shares his surname with Oleg Orlov, a Russian businessman arrested in Ukraine on suspicion of smuggling missiles to Iran. In 2007, Oleg Orlov was strangled in Kievs Lukyanivska Prison during the investigation into his activities.    History (The History Channel) claims that Orlovs life is based on Viktor Bout, a convicted arms dealer notorious for smuggling arms and other merchandise through several aviation-company Front organization|fronts.  , New York Times, 2 Nov 2011.  Russian organized crime. Charles Taylor, the President of Liberia until 2003.  Saddam Husseins the US-led invasion of Iraq.
* The character Colonel Oliver Southern hints at Oliver North, known for his involvement in the Iran–Contra affair|Iran-Contra scandal.

The conflicts portrayed in the film are all real conflicts in real countries, particularly those in Lebanon, Sudan, Cambodia, Afghanistan, Liberia, Colombia and Sierra Leone. Conversely, the image of Interpol as an acting security agency is entirely fictional.

==Production== Eugene Lazarev Russian mat wording, translated by far softer expressions in the original English subtitles. It is unclear whether these pieces were part of the script, or Lazarevs improvisation.

A scene in the film featured 50 tanks, which were provided by a Czech source. The tanks were only available until December of the year of filming, as the dealer needed them to sell in Libya.  The production team rented 3000 real SA Vz. 58 rifles to stand in for AK 47s because they were cheaper than prop guns.   

==Release==

===Critical reception===
Lord of War received fairly positive reviews from critics; the film received a 61% rating on  .

It received a 62/100 score from Metacritic. 

===Box office=== Just Like Heaven and The Exorcism of Emily Rose. After the films 7 weeks of release it grossed a total of $24,149,632 on the domestic market in the US, and $48,467,436 overseas, for a worldwide total of $72,617,068.   

==Home media==
The UK DVD release of Lord of War includes, prior to the film, an advert for Amnesty International, showing the AK-47 being sold on a shopping channel of the style popular on cable networks. The American DVD release includes a bonus feature that shows the various weapons used in the film, allowing viewers to click on each weapon to get statistics about their physical dimensions and histories. The DVD bonus section also contains a public service announcement from Nicolas Cage that addresses the issue of illicit arms sales.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
   
   
 
 
   
 
   
   
   
 
 
 
 
 
 
 
 
 
 
 
 
 