H.P. Lovecraft's: Necronomicon
 
 
 
{{Infobox film
| name = H.P. Lovecrafts: Necronomicon
| image = Necronomicon.jpg
| caption = DVD cover for H.P. Lovercrafts Necronomicon
| director = Brian Yuzna ("The Library" and "Whispers") Christophe Gans ("The Drowned") Shusuke Kaneko ("The Cold")
| writer = Brent V. Friedma] ("The Library") Christophe Gans ("The Drowned") Kazunori Itō ("The Cold") Brian Yuzna ("Whispers") H. P. Lovecraft (short stories) Richard Lynch Belinda Bauer David Warner Maria Ford
| producer = Takashige Ichise Brian Yuzna Samuel Hadida Aki Komine Gary Schmoelier
| distributor = Davis-Films
| released =  
| runtime = 96 minutes
| country = United States English
| budget = $4,000,000 (estimated)
}} American anthology Richard Lynch Belinda Bauer David Warner as Dr. Madden.

The three stories in the film are based on three  ,  The Cold is based on Cool Air,  and Whispers is based on The Whisperer in Darkness. 

==Plot==
 

The film is broken into four separate features: "The Library", "The Drowned", "The Cold" and "Whispers". "The Library" segment is the wrap-around story, which begins and ends the movie.

==="The Library"===
* Part 1
In the wrap-around story of the film, H. P. Lovecraft (Jeffrey Combs) learns of a monastery where a copy of the Necronomicon is held. Having been a regular there for his research, he sets up an appointment, his cab driver told to wait outside. Taking insult when the head monk calls his work "fiction", Lovecraft insists that all his writings are true. Requesting to read the Alchemical Encyclopedia Vol. III, Lovecraft steals a key from another monk and flees to the cellar where the Necronomicon is being held. Unknown to him, a monk has seen him. Unlocking the vault where the book is held, the door closes behind Lovecraft unexpectedly, making him drop the key down a grating and into the water below.  As that happens, one of the seals is opened.

Lovecraft sits to read and record what he is reading. Its not specified if he sees visions of the future through the book, or if the book contains future accounts. Its likely the stories will come to pass, and for the Necronomicon have already passed, alluding to the Necronomicons timelessness, as all the stories take place well beyond the 1920s.

==="The Drowned"===
Edward De LaPoer, a member of the De La Poer family, is tracked down in Sweden after inheriting an old, abandoned family hotel. Left a sealed envelope from Jethro De La Poer, he learns of his uncles tragic death. Upon a boat trip return to New England, a crash on the shore killed Jethros wife and son. Distraught, Jethro picked up a copy of the Holy Bible in front of several funeral mourners, tossed it into the fireplace and announced that any god who would take from him is not welcome in his home. 
That night, an odd fishman arrives and tells him he is "not alone", then leaves behind an English translation of the Necronomicon. Using the book, Jethro brings his family back to life. However, they are revived as unholy monsters with green glowing eyes and tentacles in their mouths. Feeling guilty, he chooses to commit suicide by casting himself off an upper floor balcony.

Edward, distraught over a car accident years before which killed his wife, Clara, finds the Necronomicon and performs the ritual to revive her. That night, Clara arrives and asks to be invited in. Edward apologizes for the accident. Clara regurgitates tentacles from her mouth, and in a panic, Edward pushes her away. Clara angrily attacks, but Edward, with a sword taken from a nearby wall, cuts her. She turns into a tentacle leading underneath the floor. Drawn underground from the injury, the creature below destroys the main floor and rises, a gigantic monster with tentacles, one eye and a large mouth. Edward cuts a rope holding the chandelier, jumps to it and climbs to the ceiling. "Clara" again tries to restrain him, but Edward destroys a stained glass window, the sunlight driving her away.

Edward pushes the chandelier rope free from the pulley, the pointed bottom piercing the monster in the eye, presumably killing it. Now on the roof, Edward has avoided the same fate that Jethro had years before, and decides to live.

==="The Library"===
* Part 2
As the first story ends, Lovecraft becomes suspicious he is being watched. The water beneath him churns, and another seal opens, revealing another. One of the monks finds Lovecrafts hat where he was designated to remain, and tells the head monk. The two of them discuss whether or not Lovecraft would be brainless enough to try to discern the secrets of the Necronomicon, which the head monk affirms, commenting that he is "only human".

Lovecraft continues to read the Necronomicon and inscribe stories.

==="The Cold"===
Reporter Dale Porkel is suspicious of a string of strange murders in Boston over the past several decades. Confronting a woman at a local apartment building, he is invited in only to find the entire place is very cold. The woman he has confronted claims to suffer a rare skin condition which has left her sensitive to heat and light. Demanding the truth or his story runs as-is, Dale is told the story of Emily Ostermans arrival to Boston twenty years before.

Emily had supposedly taken residence in the apartment building, and told by Lena, the owner, not to disturb the other tenant, Dr. Richard Madden, a scientist. Her first night, she is attacked by her sexually abusive stepfather, Sam, who has tracked her down. Running away, the two struggle on the steps leading to the next apartment. Dr. Madden opens his door, grabs Sams arm and stabs his hand with a scalpel. The fall down from the stairs kills him. Emily is bandaged up and given medication. That night, Emily is roused by drilling noises and blood dripping from her ceiling. Heading upstairs, she finds Dr. Madden and Lena mutilating Sam. She passes out, to awaken later in her bed with a clean ceiling. Dr. Madden assures her she was having a nightmare.

The next day while job hunting, Emily sees two cops with a flyer asking for information about the murder of Sam. She confronts Dr. Madden, and he comes clean: Though Sam was already dead from the fall, Dr. Madden claims he would have killed Sam regardless for what he had done to Emily. Dr. Madden reveals his copy of the Necronomicon and how he learned of its information on sustaining life. In the greenhouse, Dr. Madden proves this by injecting a wilted rose with a compound to revive it, claiming that as long as it is kept out of the sun, it will never die. The two have sex, with a distraught Lena spying on them.

That night, Lena threatens to kill Emily if Emily will not kill her, as Lena is in love with Dr. Madden, a feeling that has never been returned. Emily flees, only to return months later. Upon arrival, Emily finds her boss from the diner in Dr. Maddens apartment, struggling to avoid death. Lena stabs the man in the back, killing him. Lena insists on killing Emily, but Dr. Madden will not allow it, the struggle destroying lab equipment in the process. The resulting fire injures Dr. Madden severely, and without his fresh injection of pure spinal fluid, feels no pain as his body disintegrates before he dies. Lena shoots Emily with a shotgun in revenge. Emily announces her pregnancy, and Lena, feeling a loyalty to Dr. Madden, saves her.

Dale suspects the woman hes talking to is not Emilys daughter, but Emily herself, having contracted a disease from Dr. Madden during intercourse. Emily reveals he is right, and that she is still pregnant, hoping one day that her baby may be born. She also reveals that she has continued murdering for spinal fluid, and chooses to keep a supply stockpiled. Dale realizes his coffee has been drugged as an aged Lena approaches him, brandishing a syringe.

==="The Library"===
* Part 3
Lovecraft continues to read from the Necronomicon and copy it. Suddenly feeling paranoid, he begins looking around, grabbing his cane and brandishing it. The head monk looks through a trap door to confirm Lovecraft is indeed with the Necronomicon, and tries to open the door leading into the room where Lovecraft is, but finds that it is locked. A third seal opens up as Lovecraft continues to read.

==="Whispers"===
During a pursuit of a suspect known as "the Butcher", two police officers, Paul and Sarah of the Philadelphia Police Department, are arguing over their failed relationship and the coming baby. The argument leads to a crash, flipping the cruiser upside down. Paul, having unbuckled his seatbelt in the argument, is knocked out and dragged off by an unseen person. Sarah unbuckles herself, breaks the window and exits the vehicle. Unable to call for backup, she follows a blood trail alone.

Inside the old warehouse, Sarah follows as Paul is taken down a service elevator. Sarah trips on a rope and falls through to the floor, saved from impact by the rope around her ankle. The rope breaks a second after. As she gets up, she finds a man in glasses, Mr. Benedict. Insisting he is merely the landlord of the warehouse and the Butcher is a tenant, he offers to lead her to him. Downstairs, the two are shot at by Mrs. Benedict, a blind old woman. Sarah, sick of getting a run-around, takes the shotgun and orders the two to lead her to the Butcher. Mrs. Benedict indulges in gossip first, insisting shes not really Benedicts wife. She also claims the Butcher is an alien.

Mr. Benedict finally takes Sarah below the warehouse, where there are tunnels of an ancient, almost Mayan construction with murals depicting human sacrifice. Mr. Benedict talks about how the Butcher is not an alien, but works for aliens, and the tunnels predate human society, once home to beings who do not follow any god. Sarah is suddenly attacked by Mrs. Benedict and shoved down a hole that has a rope ladder. As its pulled up, Sarah finds herself in a cave filled with old bones and fresh corpses. Strange bats fly about, and when Sarah shoots the cave walls, green blood oozes forth.

Sarah hears Paul, only to find a jittering body with the back of its head open and no brain. A creature eventually emerges from Pauls body, one of the bat-things. Speaking from an odd orifice in the chest, Paul reveals that it is him, his brain now in the bat-things body. It is how these creatures reproduce, by stealing human brains. Eventually, the brain dies. Sarah breaks down and is attacked by the Benedicts. She learns Mrs. Benedict is in fact one of the aliens, a "queen", and that Mr. Benedict is the Butcher, and willingly helps the aliens gather humans in exchange for his freedom. In addition to requiring human brains, they also feed on bone marrow. Sarah passes out as one of her arms is severed.

Waking up, she finds herself in the hospital and is told by her doctor (Mr. Benedict) that she was in a severe accident and shes been hallucinating in response. Her mother (Mrs. Benedict) is there to console her, but tells her the baby is lost and that Paul is brain dead. The illusion is quickly torn away as Mr. Benedict reveals Pauls mangled body in the bed next to her. Pulling the sheet down, Sarah finds her left arm and both legs amputated. The walls melt away as Mrs. Benedict removes Sarahs right arm, various creatures in the background holding her limbs and draining the marrow. Sarahs screaming melts into faded, mindless laughter.

(While the story takes minor elements from "The Whisperer in Darkness", the bat-like aliens have little in common with the Mi-go. The only real similarity seems to be in their ability to transfer a human brain into a new container, albeit one of their own bodies rather than a canister, as a means to expand their population. They also bear no physical resemblance aside from wings.)

==="The Library"===
* Final Segment
With the conclusion of the third tale, Lovecraft is confronted by the head monk, who assures him that all will be fine if he opens the door. Lovecraft admits he dropped the key. Furious, the monk warns Lovecraft to replace the book, but the author is attacked by a monster in the water beneath him, and the last of the seals opens up. The head monk reveals himself to not be human at all, as he begins stretching his body through the bars to enter the room, and Lovecraft uses a sword in his cane to defeat the monster in the water. The monk mocks Lovecraft, telling him the secrets of the Necronomicon do not come cheaply, and it will cost your life. Lovecraft is forced to use his sword once more to wound and stun the monk, who is then devoured by a nameless being who comes through the dimensional barrier, which is then sealed once more by the seals as the vault closes.

Gathering his things and grabbing the book, Lovecraft begins to depart, being caught by one of the monks who warns him of the foolishness of his actions, telling him he will pay for his misdeeds. Lovecraft then escapes to the taxi and orders it to leave, and it leaves unpursued.

==Reception==
Necronomicon was well received upon its initial VHS release in the USA, but did substantially better in European and Asian markets.  With regards to the acting, one reviewer said that Payne is especially effective because of his suppression of his tortured grief, adding considerable power to his scenes.  The film won the award for the best special effects at the 1994 Fantafestival.

In their book  , Andrew Migliore and John Strysik write: "Unfortunately,   does not deliver on what should have been a great idea. In fact the film loses focus, speed, and atmosphere after the first segment, "The Drowned," almost as though the production had run out of money and time." 

==Cast==
"The Library"
*Jeffrey Combs as H. P. Lovecraft
*Tony Azito as Librarian
*Brian Yuzna as Cabbie

"The Drowned"
*Bruce Payne as Edward De Lapoer Belinda Bauer as Nancy Gallmore Richard Lynch as Jethro De Lapoer
*Maria Ford as Clara
*Peter Jasienski as Jethros son
*Denice D. Lewis as Emma De Lapoer
*Vladimir Kulich as a villager

"The Cold" David Warner as Dr. Madden
*Bess Meyer as Emily Osterman
*Millie Perkins as Lena
*Dennis Christopher as Dale Porkel
*Gary Graham as Sam
*Curt Lowens as Mr. Hawkins

"Whispers"
*Signy Coleman as Sarah
*Obba Babatundé as Paul
*Don Calfa as Mr. Benedict
*Judith Drake as Mrs. Benedict

== References ==
 

==External links==
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 