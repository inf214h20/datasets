Hex (film)
{{Infobox Film
| name           = Hex
| image          = Hex_1973_film_still.jpg
| image_size     = 
| caption        = 
| director       = Leo Garen
| producer       = Clark Paylow Stephen Katz Vernon Zimmerman (story)
| narrator       =  Hillarie Thompson Mike Combs Scott Glenn Gary Busey Dan Haggerty Charles Bernstein Patrick Williams
| cinematography = 
| editing        = 
| distributor    = Twentieth Century Fox (USA)
| released       = 1973
| runtime        = 92 min.
| country        = United States English
| budget         = $980,000 
| preceded_by    = 
| followed_by    = 
}}
 American horror Hillarie Thompson Phil Hardys The Encyclopedia of Horror Movies, Hex "crosses elements of the bike film with those of the post-western and the supernatural tale... The film scarcely succeeds in welding its disparate elements together, but still makes a distinctive, atmospheric impression." 

==Plot== Native American shaman. The bikers are soon discovered by the sisters, who reluctantly allow them to stay overnight. One of the bikers attempts to rape the younger sister, after which the older sister dons her fathers shaman regalia and casts a hex on the gang. The bikers soon start departing this world in not so natural ways.

==Cast==
*Keith Carradine
*Christina Raines (appearing as Tina Herazo) Hillarie Thompson
*Mike Combs
*Scott Glenn
*Gary Busey Robert Walker
*Doria Cook
*Dan Haggerty
*Iggie Wolfington

==Releases==
It has also been released under the titles Charms and The Shrieking.

The film is currently released  as Charms on DVD by Smooth Motion Pictures Inc.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 