Los Marziano
{{Infobox film
| name           = Los Marziano
| image          = LosMarziano2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       =  
| producer       =  
| writer         = Ana Katz & Daniel Katz
| starring       =  
| music          = 
| cinematography = Julián Apezteguia
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}}
Los Marziano is a 2010 Argentine film about a reunion between two brothers after a long period of estrangement. Filming began on February 15, 2010.

== Production ==
Oscar Kramer & Hugo Sigman Films

== References ==

*{{cite news
 | title = Francella to star in Katzs Marziano. Argentine pic also features Puig, Corsese
 | author =Charles Newbery, John Hopewell
 | first = 
 | last = 
 | authorlink = 
 | author2 = 
 | author3 = 
 | author4 = 
 | author5 = 
 | author6 = 
 | author7 = 
 | url = http://www.variety.com/article/VR1118012381.html?categoryid=1446&cs=1
 | format = 
 | agency = 
 | newspaper = Variety
 | publisher = 
 | location = 
 | isbn = 
 | issn = 
 | oclc = 
 | pmid = 
 | pmd = 
 | bibcode = 
 | doi = 
 | id = 
 | date = December 8, 2009 
 | page = 
 | pages = 
 | at =
 | accessdate = February 15, 2010
 | language = 
 | trans_title =
 | quote = 
 | archiveurl = 
 | archivedate = 
 | ref = 
 }}
*  
*  
*  
* 

== External links ==
*  
*  
*  

 
 
 


 