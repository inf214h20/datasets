Fifty Pills
{{Infobox film
| name = Fifty Pills
| image = Fifty Pills DVD cover.jpg
| caption = DVD cover
| director = Theo Avgerinos
| producer = Matthew Pernicaro Jake Demaray Kevin Mann
| writer = Matthew Pernicaro
| starring = Lou Taylor Pucci Kristen Bell John Hensley Nora Zehetner Michael Peña Diora Baird Jane Lynch Monica Keena Eddie Kaye Thomas
| music = David Manning
| cinematography = Harris Charalambous
| editing = Adam B. Stein
| distributor = PalmStar Entertainment
| released =   (Tribeca Film Festival|Tribeca)  
| runtime = 83 minutes
| country = United States
| language = English
| budget = $250,000  
}}
Fifty Pills (also known as 50 Pills) is the debut feature film of director Theo Avgerinos, which premiered at the 2006 Tribeca Film Festival.

==Plot== ecstasy pills. If he sells the pills he could make $1000 and would have enough money to stay in school.

Darren sells to many weirdos, including a dominatrix who wants the pills so her "pets" can be numb when she has sex with them. When visiting his girlfriend, her roommate sets Darren up with a connection. The brother of this roommate is a seemingly retarded and overly-zealous white-collar employee named Ralphie (Eddie Kaye Thomas) is obsessed with Diffrent Strokes. He makes Darren watch four hours of the show, but Darren leaves. Upon leaving he is harassed by a drug dealer known as The Seoul Man (Ron Yuan), who almost kills him. Once again his luck turns sour when his girlfriend finds out that he is selling drugs and breaks it off with him.

Coleman owes money to a drug dealer, so a group of three armed thugs are sent to extract the money for him at his dorm. Not having the money, he says he will call Darren and they can take whatever pills he has left as collateral. Darren comes back to the dorm, and gives the pills to the thugs, who leave. The next day, Darren feels defeated, but discovers that Coleman paid his tuition in full and is leaving the college. Reinvigorated, Darren begins dating Gracie and the movie ends with the two sitting on a bench with Darren taking a picture on his phone, saying that Gracie is about to make his parents very happy.

==Cast==
* Lou Taylor Pucci as Darren
* Kristen Bell as Gracie
* John Hensley as Coleman
* Nora Zehetner as Michelle
* Diora Baird as Tiffany
* Michael Peña as Eduardo
* Jane Lynch as Doreen
* Monica Keena as Petunia
* Eddie Kaye Thomas as Ralphie
* John Kapelos as Harold
* Donnell Rawlings as C-Low
* Rachel Boston as Lindsay
* John Marshall Jones as Housing manager
* Chris J. Johnson as Paul
* Ron Yuan as The Seoul Man

==Release and reception== premiered at the 2006 Tribeca Film Festival on April 26, 2006. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 