?: A Question Mark
{{Infobox film
| name           = Question Mark (2012 film)
| image          = http://www.trippletakemp.com/thumbs/media/41.jpg
| caption        = Theatrical release poster
| director       = Allyson Patel Yash Dave
| producer       = Trippletake Motion Pictures
| writer            = Allyson Patel   Yash Dave
| starring       = Sonam Mukherjee Maanvi Gagroo Akhlaque Khan Yaman Chatwal Varun Thakur Kiran Bhatia Chirag Jain
| cinematography = M. Anandan Allyson Patel
| editing        = Udhaya Rajani
| Sound Design        = Sanjay-Allwyn
| distributor    = Percept Picture Company (Shailendra Singh)
| released       =  
| runtime        = 82 minutes
| country        = India
| language       = Hindi
| budget         = Rs.40,00,000
}}

Question Mark is a 2012 Hindi language horror film written and directed by Allyson Patel & Yash Dave and produced by Trippletake Motion Pictures. It was distributed by Percept Picture Company. The film received nine nominations and two awards at the St. Tropez International Film Festival, France. Akhlaque Khan won the best actor while Udhaya Rajni won the award for best editing. The film also received a standing ovation at the Oaxaca Film Festival in Mexico.

This was the first time not only in Indian Cinema but world over that a film was being released without a title. Instead, to describe the unseen, unheard story, all that was used was a symbol, "?" However, the movie had to be released with a written title "Question Mark" due to censorship rules.

It delves into the found footage genre which is generally a subgenre in horror where the lines between reel and real become difficult to distinguish.

==Plot==
In November 2010, a group of friends went to a place to shoot their final year project film but never returned. A few days later, their camera was found. What happened with them was captured in the camera. The film is a compilation of the footage found from their camera.

==Cast==
* Maanvi Gagroo as Maanvi 
* Akhlaque Khan as Akki
* Yaman Chatwal as Vicky
* Varun Thakur as Varun
* Sonam Mukherji as Simran
* Kiran Bhatia as Kiran
* Chirag Jain as C.J.

== Production ==

===Development===
The idea of this film germinated from an absolute desperation to make a film but not getting to make it. The first thing that got fixed, before any idea or concept, was the budget. A measly budget meant that a regular film, with all its regular requirements would be almost impossible to complete.

Yash-Allyson had to make a film with a shoe string budget and still be effective. That is when the idea of found footage genre struck them. It was a genre which had yet not been explored in India and probably with good reason. The pattern followed by a found footage film is way different from the normally loud and colorful Hindi films. With no stars, no songs, no background score and shaky camera work, they were defying every norm there was in the Indian Film Industry. The subjects of the paranormal are always quite intriguing and what better than making a paranormal flick in the found footage genre. However, their aim was to "Indianize" it and still keep it as authentic as possible. And so began the research as they visited several places where they witnessed people being actually possessed. they wanted to depict the possession exactly how it occurred. Certain scenes in the movie were twenty minutes long which brought with them a feeling of real time action making the draining of the characters more believable. One thing was always clear in the directors minds, they could compromise on the spooks but not on the believability of the film.

===Casting===
After auditioning around 120 people, the directors finally zeroed in on the seven cast members. Each actor brought in their own personality to make the characters more believable. To help the actors be as natural as possible, they used their real names and all the scenes were improvised so as to appear real. The actors were made to rehearse the whole film several times before they came on the sets. Akhlaque Khan (Akki) was not a part of the cast initially. However, with just a couple of weeks to go before the shoot, the actor assigned to play Akkis part bowed out and in came Akhlaque who, eventually won the award for the best actor at the St. Tropez International Film Festival, France St. Tropez International Film Festival, France 2013. 

== Release ==
The film was released in theaters across India on February 17, 2012 by Percept Picture Company and worldwide as video on demand on the IDream Motion Pictures YouTube Channel. 

== Notes ==
 

 
 
 
 