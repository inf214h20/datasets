Hollow Triumph
{{Infobox film
| name           = Hollow Triumph
| image          = Hollow Triumph poster.jpg
| alt            =
| caption        = Theatrical release lobby card
| director       = Steve Sekely
| producer       = Paul Henreid
| screenplay     = Daniel Fuchs
| based on       =  
| starring       = Paul Henreid Joan Bennett
| music          = Sol Kaplan
| cinematography = John Alton Fred Allen
| distributor    = Eagle-Lion Films|Eagle-Lion Films Inc.
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Hollow Triumph (also known as The Scar in the United Kingdom) is a 1948 American film noir  directed by Steve Sekely and starring Paul Henreid and Joan Bennett. It was released by Eagle-Lion Films, based on the 1946 novel of the same title written by Murray Forbes.

==Plot==
Just released from prison, John Muller (Paul Henreid) masterminds a holdup at an illegal casino run by Rocky Stansyck. The robbery goes bad, and the mobsters capture some of Mullers men and force them to identify the rest before killing them. Stansyck has a reputation for tracking down and killing his enemies, no matter how long it takes, so Muller decides to leave town and hide.  He takes an office job recommended by his law-abiding brother, Frederick (Eduard Franz), but quickly decides that working for a living is not for him.  
 
A chance encounter with dentist Dr. Swangron (John Qualen) reveals that Muller looks exactly like a psychoanalyst who works in the same building, Dr. Bartok, the only difference being a large scar on the left side of the doctors face. Seizing the opportunity, he begins researching Bartok, even slipping into his office to examine his records. He is discovered by the doctors secretary, Evelyn Hahn (Joan Bennett). She mistakes him for her employer and kisses him, but quickly realizes he is someone else. He persuades her to go out with him, though she has become embittered and claims to have given up any dreams of finding love.

Muller sets out to impersonate Bartok, aided by the fact he studied psychoanalysis in medical school before dropping out. He takes a photograph of the doctor and uses it as a guide to cut an identical scar on his own face. Unfortunately, the developers of the photograph reversed the negative, so now Muller has the scar on the wrong side.  He discovers the mistake only after he has already murdered Bartok and is preparing to dump the body in the river. He has no choice but to go through with the plan anyway. Luckily, no one (except the office cleaning lady, whose suspicions he manages to lull) notices the difference, not even Evelyn or Bartoks patients.

Muller discovers "he" has a girlfriend, Virginia Taylor (Leslie Brooks), and that they frequent Maxwells, a high class casino. It also turns out Bartok has been losing heavily.

When a worried Frederick Muller tries to contact his brother, the trail leads to Bartok. The scar convinces Frederick that the man he sees is merely a lookalike.  Evelyn, previously unaware of the switch (but now very suspicious), reveals that John Muller said he was going to Paris. Frederick Muller tells "Bartok" that his brother no longer has to hide; Stansyck was convicted for "income tax problems" and is scheduled to be deported.

Afterward, Evelyn realizes that Muller is an imposter and that he must have killed the psychoanalyst. Though he admits to her he did, she does not turn him in to the police; instead she purchases a ticket to sail to Honolulu. Muller finds out and promises he will go with her, but she does not believe he would leave such an opportunity to enrich himself. Muller arranges for other doctors to take over his patients and heads to the dock. There, however, he is intercepted by two men who want to discuss Bartoks $90,000 gambling debt. When Muller tries to break away, they fatally shoot him. Evelyn sails away, unaware that Muller lies dying on the dock.

==Cast==
 
 
* Paul Henreid as John Muller / Dr. Victor Emil Bartok
* Joan Bennett as Evelyn Hahn
* Eduard Franz as Frederick Muller
* Leslie Brooks as Virginia Taylor
* John Qualen as Swangron
* Mabel Paige as Charwoman
* Herbert Rudley as Marcy
* Charles Arnt as Coblenz
 
* George Chandler as Artell, Assistant
* Sid Tomack as Aubrey, Manager
* Alvin Hammer as Jerry
* Ann Staunton as Blonde
* Paul E. Burns as Harold
* Charles Trowbridge as Deputy
* Morgan Farley as Howard Anderson
* Jack Webb as "Bullseye"
 

==Reception==

===Critical response===
The New York Times critic Thomas M. Pryor called it, "an adequate examination of an intelligent criminal type. There is not quite enough logic in the plot to enable it to stand up under scrutiny, but the story moves along briskly, the performances are sound and there is always the promise of more violence just around the corner." 

Alain Silver in Film Noir: An Encyclopedic Reference to the American Style notes "As in many of these B thrillers, the plot is contrived although the films conclusion is as downbeat as any noir film since Scarlet Street. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 