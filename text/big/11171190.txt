Up the Chastity Belt
 
 
{{Infobox film
| name           = Up the Chastity Belt
| image          = "Up_The_Chastity_Belt"_(1971).jpg
| image_size     = 
| caption        = Australian poster
| director       = Bob Kellett
| producer       = Terry Glinwood   Ned Sherrin Alan Simpson
| narrator       =
| starring       = Frankie Howerd
| music          = Carl Davis Ian Wilson
| editing        = Al Gell
| distributor    = Anglo-EMI
| released       = 1971
| runtime        = 94 min
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Up the Chastity Belt (also released as Naughty Knights in the USA) is a 1971 British film, a spin-off from the TV series Up Pompeii! that starred Frankie Howerd and was directed by Bob Kellett.

==Synopsis==
Howerd played dual roles as King Richard the Lionheart and cowardly peasant Lurkalot (his character in Up Pompeii was the slave Lurcio). The plot served as an excuse to serve up the usual collection of puns and double entendres that characterised most British film comedy in the 1970s, but did feature Eartha Kitt singing "A Knight for My Nights" and Hugh Paddick (of Julian and Sandy fame in the Round the Horne BBC radio series) as a notable Robin Hood, leader of a band of men who were camp homosexual rather than merry.

==Plot==
Eleanor of Aquitaine gave birth to twin sons – Lurkalot first and then Richard. But the nobles of the country – led by Sir Braggart de Bombast (Bill Fraser) intercepted Lurkalot, stole him away and abandoned him in a forest to infanticide|die. He was raised by a family of pigs who belonged to Sir Coward de Custard (Graham Crowden) who realised he wasnt a pig and took him in as a serf. By way of remittance, Lurkalot aids his master by selling love potions and chastity belts as well as some unusual inventions in the local village as Sir Coward isnt particularly successful a noble. Lurkalot is also visited by strange "voices" in the middle of the night who speak to him and try to tell him who he really is but get drowned out by events like lightning.
 Prince John. Offended, Sir Braggart challenges Sir Coward to a duel with Lady Lobelia (Anne Aston) as the prize. Lurkalot, although not a Knight, takes up the challenge as the "Man with no name" and defeats Sir Braggarts champion, Sir Grumbell de Grunt (David Prowse) with the aid of a giant magnet. Realising that the rules of chivalry have not been met, Sir Braggart declares the duel void and declares war. Lurkalot and Lady Lobelia flee the scene to his workshop where to protect his masters daughter, he locks her up with a chastity belt.

Sir Coward responds to the challenge by running away to join the crusades whilst Lurkalot, encouraged by his voices, goes to find both him and Richard the Lionheart. Once in the Holy Land, he discovers that the "crusades" are actually a Bacchanalian orgy, an excuse to leave the wives and families for a few years. Saladin (Derek Griffiths) is actually a friend of a crusader and started everything. Richard wont leave the Holy Land as hes in his tent with Scheherazade (Eartha Kitt) and insists on trying every position in the Kama Sutra. Lurkalots voices provide inspiration and he takes all the unused weaponry and fashions them into chastity belts. He then brings feminism to Saladins women, who go on strike, and Richard is forced to return home, albeit very unwillingly.

In Germany, Richard meets a local woman and decides to stay with her. He casually tosses Lurkalot the crown and says that if he looks like him, he can be him. Lurkalot returns to his home and attempts to rally the people but he is recognised as Lurkalot and is accused of witchcraft following his earlier escape from the castle where he used his flying machine. After being Cucking stool|ducked, he is sentenced to death by burning, but is rescued by Robin Hood (Hugh Paddick). They plan an attack on Sir Braggart and are joined by Sir Coward, who is fed up with being bullied and fortified by smoking from a hookah pipe. Meanwhile Richard has had to flee to England following an ignoble episode with his German woman and returns to Lurkalots village, where he is captured by people thinking he is Lurkalot.
 saltpetre by gone powder" and they use it to open the castle gate, where Robin Hood can attack. Meanwhile Sir Braggart duels Lurkalot and reveals that he must be Richards twin brother. The two battle all over the castle and he is eventually forced to flee in disgrace. Richard then resumes his rule and everything is right in the land. He agrees to marry Lady Lobelia and makes Lurkalot a Knight but in the final scene, Lurkalot gets Lady Lobelia whilst Richard returns to Scheherazade.

==Cast==
*Frankie Howerd as Lurkalot / Richard the Lionheart
*Graham Crowden as Sir Coward de Custard
*Bill Fraser as Sir Braggart de Bombast
*Hugh Paddick as Robin Hood
*Anna Quayle as Lady Ashfodel
*Eartha Kitt as Scheherazade
*Roy Hudd as Nick the Pick
*Godfrey Winn as Archbishop of all England
*Anne Aston as Lobelia
*Lance Percival as Reporter
*Royce Mills as Knotweed
*Fred Emney as Mortimer Dave King as Landlord of the Blue Boar
*David Prowse as Sir Grumbel de Grunt
*Nora Swinburne as Lady in Waiting
*Judy Huxtable as Gretel
*Lally Bowers as The Voice (voice)
*Derek Griffiths as Saladin
*Iain Cuthbertson as Teutonic Knight Billy Walker as Chopper
*Rita Webb as Maid Marian
*Long John Baldry as Little John
*David Kernan as Troubador
*Frank Thornton as Master of Ceremonies
*David Battley as Yokel
*Norman Beaton as Blacksmith
*Sam Kydd as Locksmith
*Aubrey Woods as Vegetable Stall Owner
*Christopher Timothy as Vendor

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 