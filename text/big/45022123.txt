Venus in the East
{{Infobox film
| name           = Venus in the East
| image          = 
| alt            = 
| caption        =
| director       = Donald Crisp
| producer       = Jesse L. Lasky
| screenplay     = Gardner Hunting Wallace Irwin
| starring       = Bryant Washburn Margery Wilson Anna Q. Nilsson Guy Oliver Clarence Burton Julia Faye
| music          = 
| cinematography = Victor L. Ackland Charles Edgar Schoenbaum
| editor         = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Donald Crisp and written by Gardner Hunting and Wallace Irwin. The film stars Bryant Washburn, Margery Wilson, Anna Q. Nilsson, Guy Oliver, Clarence Burton and Julia Faye. The film was released on January 26, 1919, by Paramount Pictures.  

The film is now lost film|lost. 

==Plot==
 

==Cast==
*Bryant Washburn as Buddy McNair
*Margery Wilson as Martha
*Anna Q. Nilsson as Mrs. Pat Dyvenot
*Guy Oliver as Doc Naylor
*Clarence Burton as Pontius Blint
*Julia Faye as Doric Blint
*Helen Dunbar as Mrs. Blint
*Arthur Edmund Carewe as Middy Knox
*Henry A. Barrows as Terrill Overbeck
*Clarence Geldart as Jass
*Charles K. Gerrard as Maddie Knox

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 