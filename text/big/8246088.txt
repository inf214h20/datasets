Raising Genius
{{Infobox film
| name           = Raising Genius
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       =  	
| producer       = Bess Wiley
| writer         = Linda Voorhees
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = 
| cinematography =  
| editing        =  
| production companies = Bathroom Boy Productions
| distributor    = Allumination Filmworks
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} American comedy film written by Linda Voorhees and co-directed by her with Bess Wiley.  The film stars Justin Long, Wendie Malick, Ed Begley, Jr., Stephen Root, Danica McKellar, Mark DeCarlo, Tippi Hedren, Shirley Jones, Clint Howard, and Sam Huntington.  .  It was a selection of the Paris Film Festival in Paris, France, in 2005,  and also played at the Waterfront Film Festival in South Haven, Michigan in 2005.   at the Waterfront Film Festival, retrieved 2014-10-15. 

==Plot==
Nancy Nestor (Wendie Malick) is the perfect wife and mother now at her wits end when her teenage genius son Hal (Justin Long) looks himself in the bathroom for months to work on a mathematical equation which involves studying of Lacy Baldwin (Danica McKellar), a cheerleader next door, as she bounces up and down on a trampoline. Nancys husband Dwight (Stephen Root) is too wrapped up in his own petty concerns to help her.  When a burglary brings the police to inspect the crime scene, they find Hal locked in the bathroom and suspect he is being held against his will and call Social Services. 

==Cast==
 
* Justin Long as Hal Nestor
* Wendie Malick as Nancy Nestor
* Ed Begley Jr. as Dr. Curly Weeks
* Stephen Root as Dwight Nestor
* Danica McKellar as Lacy Baldwin
* Megan Cavanagh as Charlene Hobbs
* Mark DeCarlo as Officer Hunter
* Tippi Hedren as Babe
* Clint Howard as Mr. Goss
* Sam Huntington as Bic
* Shirley Jones as Aunt Sis
* Joel David Moore as Rolf
* Travis Wester as Rudy
* J. Bretton Truett as Dr. Van Heizen
 

==References==
 

==External links==
*  

 
 
 

 