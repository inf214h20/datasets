Love in Thoughts
{{Infobox film
 | name = Love in Thoughts
 | image = Love in Thoughts film.jpg
 | caption = Love in Thoughts DVD cover
 | director = Achim von Borries
 | writer = Hendrik Handloegten Annette Hess
 | starring = Daniel Brühl August Diehl Anna Maria Mühe Thure Lindhardt
 | producer = Stefan Arndt
 | distributor = Warner Home Video
 | budget =
 | released = 24 November 2004 ( Germany )
 | runtime = 89 min.
 | language = German
  | }} German film directed by Achim von Borries. It was released in Germany on 24 November 2004. The main characters are played by August Diehl, Daniel Brühl, Anna Maria Mühe and Jana Pallaske.

==Plot==
The movie takes place in late 1920s Berlin. It opens with Paul being questioned by police about a note he had written. The scene then fades out, and the movie shows what happened. Paul, a shy virgin poet who is tired of being alone and heartbroken, is friends with an openly gay aristocrat boy, Guenther, who is suffering unrequited love for Hans. Paul is staying at Guenthers parents country home over the weekend. The parents are absent. Guenthers sister Hilde, who stole Hans heart besides, is loved by Paul, for whom Guenther has budding feelings, which complicates the brother-sister relationship. Hilde has no interest in committing to a relationship with Paul, however. Guenther invites some people over to have an all-night party, filled with alcohol, music, and sex. It is one of their last parties, since Paul and Guenther have made a suicide pact. Guenther, Paul, Hans and Hilde go through a series of couplings, conversation and partying before proceeding to Hilde and Guenthers parents apartment in the city. There the drama ends with gunshots. The question is what actually happened. The film is based on a true story, the so-called  .

==Awards==
;Won
*2005:  
*2004:  
*2004:  
*2004:  
*2004:  
*2004: Verona Love Screens Film Festival - for Best Film: Love in Thoughts
;Nominated
*2005:  
*2004: Brussels European Film Festival - Golden Iris : Love in Thoughts

==External links==
* 
*   
* 

 
 
 
 
 
 
 
 
 
 


 
 