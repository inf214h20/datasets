Jar City (film)
{{Infobox film
| name           = Jar City
| image          = Myrin ver2.jpg
| caption        = 
| director       = Baltasar Kormákur
| producer       = Agnes Johansen Lilja Pálmadóttir
| writer         = Arnaldur Indriðason
| starring       = Ágústa Eva Erlendsdóttir Ingvar Eggert Sigurðsson
| music          = Mugison
| cinematography = Bergsteinn Björgúlfsson
| editing        = Elísabet Ronaldsdóttir
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Iceland Germany Denmark
| language       = Icelandic
| budget         = 
| gross          = $748,315  
}}
Jar City  (  directed by Baltasar Kormákur. It is based on Mýrin, a novel written by Arnaldur Indriðason and released in English as Jar City.

Kormákur is in the midst of producing an English-language remake, also called Jar City, which will be set in Louisiana. 

==Plot==
A world-weary cop comes to believe a recent murder of an elderly man is linked to a case of rape three decades earlier and works to put the pieces together.

Like the book on which it is based, the film is implicitly a fierce critique of the gene-gathering work of the Icelandic company deCODE genetics. Lucy Burke, Genetics and the Scene of the Crime: DeCODING Tainted Blood, Journal of Literary & Cultural Disability Studies, 6 (2012), 193–208. doi:10.3828/jlcds.2012.16.
 

==Cast==
* Ingvar E. Sigurðsson as Erlendur
* Ágústa Eva Erlendsdóttir as Eva Lind
* Björn Hlynur Haraldsson as Sigurður Óli
* Ólafía Hrönn Jónsdóttir as Elínborg 
* Atli Rafn Sigurðsson as Örn
* Kristbjörg Kjeld as Katrín
* Þorsteinn Gunnarsson as Holberg
* Theódór Júlíusson as Elliði
* Þórunn Magnea Magnúsdóttir as Elín
* Guðmunda Elíasdóttir as Theodóra
* Walter Grímsson as Handrukkarar
* Sveinn Olafur Gunnarsson
* Magnús Ragnarsson as Lögfræðingur
* Rafnhildur Rósa Atladótir as Kola
* Jón Sigurbjörnsson as Albert

==Soundtrack==
The score was composed by Mugison.

Track listing:
# "Til eru fræ"
# "Sveitin milli sanda"
# "Bíum bíum bambaló"
# "Erlendur"
# "Elliði"   
# "Á Sprengisandi"
# "Fyrir átta árum"
# "Áfram veginn – Nikka"
# "Áfram veginn"
# "Halabalúbbúlúbbúlei"
# "Malakoff"
# "Bí bí og blaka I"
# "Myrra"
# "Kirkjuhvoll"
# "Bí bí og blaka II"
# "Dagný"
# "Heyr, Ó Gud raust mína"
# "Lyrik"
# "Nú hnígur sól"
# "Sofðu unga ástin mín"
# "Ódur til Hildigunnar" 
# "Svefnfræ"
# "Fræsvefn"
# "Svefnfræ, söngur"
# "Nú legg ég augun aftur"

Incidental music:
Extract from George Frideric Handels The Arrival of the Queen of Sheba from the oratorio Solomon

==Prizes==
The film was awarded the Crystal Globe Grand Prix at the Karlovy Vary International Film Festival in 2007.  It also won the Breaking Waves Award at the 15th Titanic International Film Festival in Budapest with a €10 000 prize, the film was screened with the title Bloodline. 

==DVD==
A Blockbuster Exclusive Region 1 DVD was released in the U.S. and Canada. Otherwise, the film was not released commercially in America. It has also been released on DVD in Europe and is available on iTunes. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 