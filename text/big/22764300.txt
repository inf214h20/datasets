Wounded Game
 
{{Infobox film
| name           = Wounded Game
| image          = 
| caption        = 
| director       = Nikolai Gubenko
| producer       = 
| writer         = Nikolai Gubenko
| starring       = Juozas Budraitis
| music          = 
| cinematography = Aleksandr Knyazhinsky
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

Wounded Game ( , Transliteration|translit.&nbsp;Podranki) is a 1977 Soviet drama film directed by Nikolai Gubenko. It was entered into the 1977 Cannes Film Festival.     

==Cast==
* Juozas Budraitis
* Aleksandr Kalyagin
* Zhanna Bolotova
* Rolan Bykov
* Bukhuti Zaqariadze
* Evgeni Evstigneev
* Aleksei Cherstvov - (as Alyosha Cherstvov)
* Nikolai Gubenko
* Georgi Burkov - Sergei Pogartsev
* Natalya Gundareva
* Olga Strogova
* Pantelejmon Krymov
* Zoya Yevseyeva
* Daniil Netrebin
* Lyudmila Shagalova

==References==
 

==External links==
* 

 
 
 
 
 
 