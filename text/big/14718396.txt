Ultraman Cosmos vs. Ultraman Justice: The Final Battle
{{Infobox film |
| name = Ultraman Cosmos VS Ultraman Justice: THE FINAL BATTLE
| image =
| runtime = 77 mins
| creator = Tsuburaya Productions
| starring = Taiyo Sugiura Daisuke Shima Kaori Sakagami Hidekazu Ichinose Koichi Sudo Mayuka Suzuki Kazue Fukiishi
| country = Japan
}}

  is Ultraman Cosmos s 3rd theatrical film adaptation and Kaiju film. It was released in Japan on August 2, 2003.

==Plot==
While EYES prepare to safely transport the worlds monsters to a far off planet where they will no longer be troubled by humans, an army of giant alien robots, called Gloker Pawns appear to stop the project. Cosmos arrives in time to temporarily stop the attack when Ultraman Justice enters the battle aiding the alien invaders. Cosmos is defeated by Justice and the aliens destroy the spacecraft that were to be used as transports for the monsters. It becomes apparent that these aliens dont want Earths species to leave the planet.

With Cosmos out of the way the invaders begin an onslaught against the Earths major cities. Every attack is witnessed by a mysterious woman in black who shows no emotion and no desire to interact with any of the victims. Without Cosmos the world is defenseless.

It is soon revealed that the mysterious woman Julie is Justice in disguise who has been sent by a Universal peace-keeping organisation called the Universal Justice to oversee their plans. Justice appears at a meeting of top level EYES leaders to deliver  a message from the organisation: In the near future life on Earth will become a threat to peace in the galaxy from the conclusion of the incident of Sandros (see  ). The aliens have come to destroy all life on Earth then restart it with new species that will be more acceptable to the other planets with their ultimate weapon, a massive space craft called Giga Endra. The destruction of Earth will begin in 35 hours and there will be no negotiations. As the Earths forces try to stop the advance of Giga Endra, nothing in their weapons are powerful enough to so much as scratch it. EYES at this time, tried to protect the people from the wrath of Gloker Pawns while putting all their efforts in getting a communication with Delaxion, one of the higher officials of the organisation in order to convince her to stop the attack.

Through interaction with a kind young girl and her puppy, Justice is able to see the good in mankind that Cosmos mentioned as two Gloker Pawns combine to form the Gloker Rook as theyre being attacked by Earths monsters and defeat them. Justice attacks the Gloker Rook and after a fierce battle, with EYES help and changing into his Crusher Mode, destroys the robot with Crusher Victorium Ray. But the aliens release their next robot, the Gloker Bishop. Try as he might, Justice is not strong enough to defeat it but Cosmos friends combine their energies to resurrect Cosmos. Holding no bad will against Justice, Cosmos changed into Future Form and recharged Justice and the two take on the Bishop.

With the combined efforts of the Ultramen the Gloker Bishop is overcome, and all is left is the life resetter, Giga Endra. The massive robot cannon is far too powerful for the duo to overcome and killed the two. But they were combined to form Ultraman Legend. The super Ultraman catches Giga Endras resetter beam and pushes back before loosing his ultimate move, the Light of Legend (The Spark Legend), and obliterating Giga Endra. With Giga Endra destroyed, Delaxion appeared before Ultraman Legend, asking him why the legendary warrior is going that far to protect the life on Earth. Legend then split back into Cosmos and Justice which both of them convinced the Universal Justice to retreat their forces. Before fully retreat, Delaxion stated that they should believe in the warriors of light as well as the humans who keep on sending messages to Delaxion herself.

In the ending scene, Julie and Musashi was eating sweet bits given by the girl she saved. Musashis friends waves to him and leaves Julie behind. She smiled to him and everyone in the end of the scene.

==Members==

===New Team Eyes===
New Team Eyes are members formed by Keisuke Fubuki (now Captain) and his 4 teammates.

*Keisuke Fubuki (Captain)
*Kashima Eiichi (Deputy Captain)
*Kuramoto Natsuki (Team Member)
*Watarai Kazuomi (Team Member)
*Shiyouda Riyoujirou (Team Member)

===First Team Eyes (THE EYES)===
The previous members of Team Eyes are Musashi Haruno, Ayano Morimoto, Koji Doigaki, Harumitsu Hiura, and Shinobu Mizuki.

==Ultramen==

===Ultraman Cosmos===
*Luna Mode
*Eclipse Mode
*Space Corona Mode
*Future Mode

===Ultraman Justice===
* Height: 47 meters
* Weight: 41,000 tons
* Flying speed: Mach 35
* Traveling speed: Mach 12
* Underwater speed: Mach 9
* Potential area speed: Mach 9
* Jump power: 4000 meters
* Grasping power: 200,000 ton
Modes/forms:
*Standard Mode
*Crusher Mode

==Cast==
* Taiyo Sugiura - Musashi Haruno/Ultraman Cosmos
* Daisuke Shima - Captain Harumitsu Hiura
* Kaori Sakagami - Deputy-Captain Shinobu Mizuki
* Hidekazu Ichinose - Keisuke Fubuki
* Koichi Sudo - Koji Doigaki
* Mayuka Suzuki - Ayano Morimoto
* Kazue Fukiishi - Julie/Ultraman Justice

== Music ==
;Theme song: "High HOPE" by Project DMM

== External links ==
*  

 
 

 
 
 
 
 