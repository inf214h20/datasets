Asignatura pendiente
{{Infobox film
| name           = Asignatura pendiente
| image          = Asignatura pendiente.jpg
| image size     =
| caption        = Theatrical release poster
| director       = José Luis Garci
| producer       = José Luis Tafur	
| writer         = José Luis Garci José María González Sinde
| narrator       =
| starring       = José Sacristán Fiorella Faltoyano
| music          =
| cinematography = Manuel Rojas
| editing        = Miguel González Sinde
| distributor    =
| released       = April 18, 1977 Spain
| runtime        = 109minutes
| country        = Spain Spanish
| budget         =
}} 1977 Spain|Spanish drama film co- written and directed  by José Luis Garci.

The film was one of the most successful films in the 70s with 2,305,924 admissions.

Nowadays, it is viewed as a portrait of society during the Spanish transition to democracy.

==Plot summary==
José and Elena were friends. They share everything, except for some unfinished business: sex. Now, with the return of democracy to the country, both have remained friends, but married to other people, and now become lovers. José is involved in politics.

==Cast==
*José Sacristán (José)
*Fiorella Faltoyano (Elena)
*Antonio Gamero
*Silvia Tortosa
*Héctor Alterio
*Simón Andreu (Paco)
*María Casanova (Pili)

==External links==
*  

 
 
 
 
 


 