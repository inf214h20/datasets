Farewell (2009 film)
{{Infobox film
| name           = Farewell
| image          = Laffaire farewell ver2.jpg
| caption        = United States theatrical poster
| director       = Christian Carion
| producer       =
| writer         =
| narrator       =
| starring       = Guillaume Canet Emir Kusturica Willem Dafoe and Fred Ward
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = France
| language       = French Russian English
| budget         =
}} French film directed by Christian Carion, starring Guillaume Canet and Emir Kusturica.  The film is an espionage thriller loosely based on actions of the high-ranking KGB official, Vladimir Vetrov.  It was released in the United States in June 2010.  It was adapted from the book Bonjour Farewell: La vérité sur la taupe française du KGB (1997) by Serguei Kostine.

==Synopsis== Soviet secrets, Socialist in Communist Party. Queen cassette tapes for his son, some Cognac (brandy)|cognac, or books of French poetry.  As Farewells prodigious output blossoms, the French are bewildered by the sheer scale and yield of top Western technology transferred covertly to the Soviets.
 dossier of invaluable Farewell data during the Ottawa summit.  The Americans are astounded with it and other information provided by Farewell, culminating in the full "List X" of Soviet spies within the highest echelons of the Western scientific and industrial apparatus.  They embark on an ambitious plan to feed the Soviets erroneous or defective data; shortly after, the network of Soviet technology spies in the West is rolled up, and Reagan announces the Strategic Defense Initiative|"Star Wars" antimissile shield project.  Deprived of hi-tech information from the West, and with their own laboratories behind, the Soviet leadership panics.  Seeing this desperate impasse for what it is, Mikhail Gorbachev, then an upwards-mobile party official, starts preparing the reform policies he is to pursue in the future.

Grigorievs superior, a double agent for the CIA, is directed by them to sacrifice Grigoriev and save the Froments, all unbeknownst to the French.  Grigoriev, under arrest and KGB interrogation, plays to buy the Froments time to escape.  They cover their traces and flee in their car to the Finnish border.  While in West Germany for debriefing, Froment pleads with the CIA Director to save Grigoriev, praising the integrity and selflessness of the man.  The director refuses as a policy principle, having brought the other agent to the West.  Grigoriev is granted his request of execution by a marksman on the jetty of the snow-clad lake he loves.  The Froments are offered a company job in Manhattan.

==Cast==
* Emir Kusturica – Sergei Grigoriev (the character based on Vetrov)
* Guillaume Canet – Pierre Froment, an engineer working in the Moscow branch of French electronics conglomerate Thomson-CSF 
* Alexandra Maria Lara – Jessica, his wife
* Willem Dafoe – Feeney, Director of the Central Intelligence Agency
* Fred Ward – U.S. President Ronald Reagan
* Philippe Magnan – President François Mitterrand of France DST 
* David Soul – Hutton, aide to President Reagan
* Ingeborga Dapkunaite – Natasha, Grigorievs wife
* Dina Korzun – Alina, a colleague and mistress that Grigoriev is having an affair with
* Yevgeni Vasilyevich Kharlanov – Igor, the Grigorievs rebellious son
* Christian Sandström – Federal Bureau of Investigation agent Gary Lewis and Alex Ferns, amongst others, have cameos.

==Reception==
The film received positive reviews from United States critics, as measured by the review aggregators Metacritic  and Rotten Tomatoes. 

==Notes==
 

==See also==
Farewell Dossier

==Further reading==
*Thomas C. Reed, At the Abyss: An Insider’s History of the Cold War (2004)
* Sergei Kostin and Eric Raynaud, Adieu Farewell (Laffont, Paris, 2009, in French); "Farewell" (AmazonCrossing, Aug. 2011, in English). First complete investigation of the Farewell Dossier and its international impact.
* Michel Louyot, Le Violon de neige (Publibook, Paris, 2008; soon to be available in English).

==External links==
* 
*   
*   
*   
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 