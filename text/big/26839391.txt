Cock-A-Doodle Deux Deux
{{Infobox Hollywood cartoon|
| cartoon_name      = Cock-A-Doodle Deux-Deux
| series            = The Inspector
| image             = Cock a Doodle Deux Deux title.jpg
| caption           = 
| director          = Robert McKimson 
| story_artist      = Michael OConnor
| animator          = Manny Perez Don Williams Warren Batchelder Ted Bonnicksen Norm McCabe George Grandpré Bob Matz
| voice_actor       = Pat Harrington, Jr. Paul Frees Helen Gerald
| musician          = William Lava
| producer          = David DePatie Friz Freleng
| distributor       = United Artists
| release_date      = June 15, 1966 DeLuxe
| runtime           = 6 08" English
| preceded_by       = Plastered in Paris
| followed_by       = Ape Suzette
}}
 short in theatrical cartoons. A total of 34 entries were produced between 1965 and 1969.

== Plot ==
The Inspector is assigned to guard Madame Anne Pouletbons jewel—the "Plymouth Rock"—during the annual charity ball held at her chateau. The diamond is then stolen, forcing the Inspector and Sgt. Deux Deux to find it, as per orders from the Commissioner. The Madames past life raising chickens has resulted in all of her chickens being employed as servants.

The Inspector dresses in a chicken disguise and learns that the butler had indeed stolen the Plymouth Rock and  stashed it inside an egg. However, it is kept in a roomful of identical eggs, forcing the Inspector and Deux Deux to sift through each and every one. While sifting, Deux-Deux finds a nice thing inside an egg. The Inspector wants to see, and its revealed to be a video of women performing the can-can. They find the diamond, but are distracted by the movies inside the other eggs, including dancing can-can girls, episodes of Barney and Friends or Thomas and Friends, Ramahfool Thomas videos that are funny, Screaming Goat songs, Disney movie clips, and other nice or humorous things.

== Production notes ==
An alternate version of the Inspector theme "A Shot in the Dark" is featured during the credits.
 Boomerang TV channel.
==See also== List of The Pink Panther cartoons

== References ==
 

== External links ==
*  
*  


 
 
 

 
 
 
 
 


 