Deadly (film)
{{Infobox film
| name           = Deadly
| image          = 
| image size     =
| caption        = 
| director       = Esben Storm
| producer       = Richard Moir
| writer         = Esben Storm Raland Allen
| based on = 
| narrator       =
| starring       = Jerome Ehlers
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = Beyond International Group
| released       = 1991
| runtime        = 
| country        = Australia English
| budget         = A$4 million 
| gross = A$25,421 (Australia) 
| preceded by    =
| followed by    =
}}
Deadly is a 1991 Australian film directed by Esben Storm.

Storm first wrote the script in 1987. He wanted to tell the story of black deaths in custody and chose a thriller format to make the movie as accessible as possible.   accessed 21 November 2012 

The movie was shot on location in Wilcannia over seven weeks. It was the first of five features funded by the Film Finance Corporations Film Trust Fund. 

==References==
 

==External links==
*  at IMDB

 
 
 

 