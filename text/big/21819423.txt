The Forest Rangers (film)
 
{{Infobox film
| name           = The Forest Rangers
| image          = The Forest Rangers - 1942 Poster.png
| image_size     =
| caption        = 1942 Theatrical Poster George Marshall
| producer       = Robert Sisk
| writer         = Harold Shumate
| narrator       =
| starring       = Fred MacMurray Paulette Goddard Susan Hayward
| music          = Victor Young Friedrich Hollaender Joseph J. Lilley
| cinematography = Charles Lang William V. Skall
| editing        = Paul Weatherwax
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} George Marshall, written by Harold Shumate based on story by Thelma Strabel, and starring Fred MacMurray, Paulette Goddard and Susan Hayward.

==Cast==
*Fred MacMurray as  Don Stuart 
*Paulette Goddard as Celia Huston Stuart 
*Susan Hayward as  Tana Butch Mason 
*Lynne Overman as Jammer Jones 
*Albert Dekker as  Twig Dawson 
*Eugene Pallette as  Howard Huston 
*Regis Toomey as  Frank Hatfield  Rod Cameron as  Jim Lawrence 
*Clem Bevans as Terry McCabe 
*James Brown as  George Tracy 
*Kenneth Griffith as  Ranger 
*Keith Richards as  Ranger 
*William Cabanne as  Ranger 
*Jimmy Conlin as  Otto Hanson

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 


 