Shanghai Dreams
{{Infobox film
| name           = Shanghai Dreams
| image          = Shanghai Dreams.jpg
| caption        = 
| director       = Wang Xiaoshuai
| producer       = Pi Li
| writer         = Wang Xiaoshuai Lao Ni Li Bin Tang Yang
| music          = Zhang Wu Wu Di
| editing        = Yang Hongyu
| distributor    = Fortissimo Films
| released       =  
| runtime        = 121 minutes
| country        = China
| language       = Mandarin Shanghainese
| budget         = 
}} 2005 Chinese Li Bin, Tang Yang, Wang Xiaoyang, and Yao Anlian. The film was produced by Stellar Megamedia, Debo Films Ltd. and Kingwood Ltd.
 Jury Prize at the 2005 Cannes Film Festival.   

==Synopsis== Third Front, intended as an industrial base if China were invaded. The father, obsessed with returning to Shanghai for a better life for his family, attempts to prevent a romance between his daughter and a local worker. The film is partly autobiographical in nature, as Wangs family was also sent to Guizhou as a child.

==Background==
Shanghai Dreams represents the first time a director has approached the subject of the "Third Line of Defense" and its consequences.    This is in no small part due to the director, Wang Xiaoshuais own upbringing. Like the Wu family, the Wang family was also originally from Shanghai and who moved to Guiyang due to the need to create an industrial third line of defense.   This third line of defense was set away from cities and the seacoast, in order to protect Chinas industrial capability in case of a war against the Soviet Union. With the opening up of China in the 1980s, however, many of these urban exiles were given the opportunity to return home to the cities, even as their families had already laid down roots in their new homes. Wang has stated that he set the story in the 1980s in that it represented an era of transition, wherein the Cultural Revolution had ended, but much of the old ways were still ingrained in society. 

==Plot==
The main character Qinghong is a 19 year old student living with her overly repressive father, mother and younger brother in a typical small apartment. Her boyfriend Honggen, a working local boy who plays only a minor role in the film and develops an obsession with Qihong to her fathers contempt. Confiding in her best friend Xiao Zhen, Qinghong strives for love and independence. 

Qinghongs father Wu Zemin is a stubborn and aggressive man, who has never forgiven his wife for persuading him to move to rural Guiyang. He regularly meets with other Third Line volunteers to discuss strategies for returning to Shanghai. He becomes increasingly strict with Qinghong, often following her home from school to ensure a restricted social life. He forbids her from seeing her boyfriend Honggen, discourages her from spending time with Xiao Zhen and after discovering that she has sneaked out to an underground dance party confines her to the house. Xiao Zhen has meanwhile fallen for the local boy Lu Jun, the son of another Third Line volunteer couple. Lu gets a local girl pregnant and is forced by his angry father to marry her. Soon after the wedding, though, he runs away with Xiao Zhen, causing panic in the local community.
Honggen stalks Qinghong and she promises to meet him secretly one evening. She slips out of the house while her father hosts a meeting with other “Third Line” friends to discuss a plan to flee to Shanghai without official permission. When Qinghong tells Honggen that they cannot be together because her family will soon leave, the nervous Honggen loses control of himself and rapes her. Qinghong totters home, muddied and bleeding. Her father initially tries to retaliate by beating up  Honggen at work, but subsequently alerts the police and has Honggen arrested. 
Traumatised, Qinghong attempts suicide. She is recovering when a sad and chastened Xiaon Zhen returns to Guiyang.

Very early one morning, as dawn is breaking, the Wu family boards a van for the drive to Shanghai. They are delayed in the streets of Guiyang by the crowds gathering to watch a round of public executions. Called out on loudspeaker are the names of those to be executed, with the last name called Honggen. 

==Release in China==
Shanghai Dreams marks the first time Wang was given a wide degree of freedom by the Film Bureau in comparison to his previous films, notably Beijing Bicycle, which suffered from censorship woes.    Wang has noted that authorities approached him after realizing that foreign blockbusters were slowly taking over the Chinese market.    Wang was then asked to send in a 1000 word summary of the film, after which they asked to see the entire script; both requests were honored and neither was "softened" in Wangs words, as a means to test the openness of the "new" bureau.  Wang noted that the film does not represent a "typical" Chinese film; as stated in an interview, Wang states: 
 
Nevertheless the film was released in China theaters shortly after its premier at Cannes.

==Awards== Jury Prize. 

===Awards and nominations=== Cannes Film 2005
** Palme dOr- official selection
** Prix du Jury - winner  2005
** Best Film - winner
* Tallinn Black Nights Film Festival
** EurAsia Grand Prix

==Cast== Guizhou Province in the 1960s. Her name is the original Chinese title of the film.
*Yao Anlian as Wu Zemin, Qinghongs father, obsessed with a return to Shanghai after years in Guizhou. Li Bin as Fan Honggen, a local boy whom Qinghong falls in love with.
*Tang Yang as Meifen, Qinghongs mother, and Zemins wife.
*Wang Xueyang as Xiaozhen, Qinghongs friend, a local Guizhou girl.
*Qin Hao as Lu Jun, a local boy, Xiaozhens object of desire.
*Wang Xiaofen as Qinghongs brother
*Dai Wenyan as Xiaozhens mother
*Lin Yuan as Xiaozhens father
*You Fangming as Lu Juns father
*Sun Qinchang as Wang Erhua

==References==
 

==External links==
*  
*  
*  
*   at the Chinese Movie Database

 
 

 
 
 
 
 
 
 
 