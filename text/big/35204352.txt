Gandha (film)
{{Infobox film
| name           = Gandha
| image          = Gandha.jpg
| alt            =  
| caption        = DVD Cover
| director       = Sachin Kundalkar
| producer       = Ranjit Gugle 
| writer         = Archana Kundalkar (story)   Sachin Kundalkar (story, screenplay & dialogues)
| based on       =  
| starring       = Girish Kulkarni   Amruta Subhash  Milind Soman   Sonali Kulkarni   Neena Kulkarni  
| music          = Shalendra Barve
| cinematography = Amalendu Chowdhari
| editing        = Abhijeet Deshpande
| studio         = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}} Marathi Indian film. The film is directed by Sachin Kundalkar and produced by Ranjit Gugle under the banner Flashbulbs Ventures. The film blends three independent stories where the main characters of all stories go through experiences involving their sense of smell. The stories are written by Kundalkar and his mother Archana Kundalkar. Sachin Kuldalkar won Best Screenplay Award for the film at Pune International Film Festival. 

The film stars Girish Kulkarni and Amruta Subhash in the first story, Milind Soman and Sonali Kulkarni in the second story and Neena Kulkarni in the third story as lead actors.

==Plot==
Lagnaachya Vayachi Mulgi (English: A Bride To Be)

Veena is a young girl who works in a college and is now a bride to be. Her parents are looking for a suitable groom for her. But due to her dark complexion, she is not able to get any groom. Mangesh, an arts student from the college accidentally meets her and she starts loving the fragrance that he brings with himself. She hears a rumour from her colleague that Mangesh is alcoholic and hence is drowsy in his classes and comes with red-eyes. One day she follows him to his home and then comes to know of the truth that Mangesh works in an incense stick factory at night. This explains the fragrance he brings and his drowsy look. They both talk and fall in love and decide to get married.

Aushadh Ghenara Manus (English: A Man On Medicines)

Sarang is a HIV+ patient and lives on his medicines. His wife, Raavi, has abandoned him from one and half years just like his parents. One day Raavi comes to meet him to talk about their divorce. She is irritated by some disgusting smell that comes from the whole house. They both reminisce over the past and she has a thought of giving their marriage another chance. But the weird unknown smell keeps irritating her. Her irritation for the smell and Sarangs silence on how he turned out HIV+ turns her mad and she ransacks the whole house for the source of smell. She finally find a dead rat under the bed. Sarang then realizes that he has lost his sense of smell.

Baajoola Basleli Baai (English: A Woman Sitting Aside)
 practice in various societies in India, she is refrained from domestic activities in home for 4 days. Her sister-in-law goes into Childbirth|labour. Due to heavy rains, the baby needs to be delivered in their home. Janakis mother-in-law is left alone to do all the work at home without Janakis help. Janaki is continuously taunted by her mother-in-law for not having her own child till now. Janaki is worried about her husband who is stuck in another town due to rains. All she can do is worry and smell things from her room upstairs as she is not allowed to be involved in the home nor see the new born baby.

==Cast==
* Girish Kulkarni as Mangesh Nadkarni
* Amruta Subhash as Veena
* Jyoti Subhash as Veenas mother
* Milind Soman as Sarang
* Sonali Kulkarni as Raavi
* Neena Kulkarni as Janaki
* Yatin Karyekar as Janakis husband
* Vidula Jawalgikar as Laxmi
* Seema Deshmukh as Jankis sister-in-law
* Leena Bhagwat as Chanda Tai

The film marks debut of Model (person)|model-turned-actor Soman in Marathi cinema.  Also, Jyoti Subhash, who is the real life mother of Amruta Subhash is seen playing this same role in film.   

==Awards== National Film Best Screenplay Best Audiography category. The award for Screenplay was given "for its remarkable integration of three different plots using the sense of smell at as a liet motif to focus sensitively on human relationships" and for Audiography "for its use of dramatically scripted sounds to heighten the mood of the film".      

* National Film Award for Best Screenplay - Sachin Kundalkar
* National Film Award for Best Audiography - Pramod J. Thomas and Anmol Bhave
* Best Screenplay Award at Pune International Film Festival - Sachin Kundalkar

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 