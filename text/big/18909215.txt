Climbing High
 
 
{{Infobox film
| name = Climbing High
| image =
| caption =
| director = Carol Reed
| producer = Michael Balcon
| writer = Marion Dix Sonnie Hale Lesser Samuels
| starring = Michael Redgrave Jessie Matthews Noel Madison Margaret Vyner Alastair Sim
| music =
| cinematography = Mutz Greenbaum
| editing = Al Barnes Michael Gordon Twentieth Century Fox Film Corporation(USA)
| released = November 1938 (UK) 1 December 1938 (USA) 19 October 1942 (Sweden)
| runtime = 78 min.
| country = United Kingdom
| awards = English
| budget =
| preceded_by =
| followed_by =
}}

Climbing High is a 1938 British comedy film directed by Carol Reed.

==Plot==
Nicky Brooke (Redgrave) is a wealthy young man who despite his engagement to the aristocratic (and broke) Lady Constance Westaker (Vyner), falls for hard-up model Diana Castles (Matthews) after nearly running her over with his car. In an effort to distance himself from tabloid created tales of his playboy lifestyle, he changes his name and attempts to woo Diana by pretending to be poor.

==Cast==
*Jessie Matthews as Diana Castles
*Michael Redgrave as Nicky Brooke
*Noel Madison as Gibson
*Alastair Sim as Max
*Margaret Vyner as Lady Constance Westaker
*Leslie Phillips as a child actor.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 