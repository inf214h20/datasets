On the Jump
{{Infobox film
| name           = On the Jump
| image          = 
| caption        = 
| director       = Alfred J. Goulding
| producer       = 
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = 
| editing        = 
| distributor    = Pathé Exchange
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd. A print of the film survives in the film archive of the Museum of Modern Art.   

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels 
* William Blaisdell
* Sammy Brooks
* Lige Conley - (as Lige Cromley)
* Billy Fay
* Helen Gilmore
* Lew Harvey
* June Havoc - (as June Hovick)
* Gus Leonard
* James Parrott Charles Stevenson - (as Charles E. Stevenson)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 