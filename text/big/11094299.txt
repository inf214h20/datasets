The Magnetic Monster
{{Infobox Film 
| name           = The Magnetic Monster
| image          = The Magnetic Monster Poster.jpg
| caption        = Film poster
| director       = Curt Siodmak Herbert L. Strock
| producer       = Ivan Tors George Van Marter
| writer         = Curt Siodmak Ivan Tors
| narrator       = Richard Carlson Richard Carlson King Donovan 
| music          = Blaine Sanford
| cinematography = Charles Van Enger
| editing        = Herbert L. Strock
| studio         = Ivan Tors Films
| distributor    = United Artists
| released       =  
| runtime        = 76 minutes
| language       = English
| budget         = $105,000 (estimated) 
}}
 The Maze Voyage to the Bottom of the Sea.   

It is the first episode in Ivan Tors Office of Scientific Investigation (OSI) trilogy, followed by Riders to the Stars and Gog (film)|Gog.

==Plot==
A pair of agents from the Office of Scientific Investigation (OSI) are sent to investigate a local appliance store, where all of the clocks have stopped at the same time, and metal items in the store have been magnetized.  The source of this is traced to an office located directly above the store, where scientific equipment is found, along with a dead body.  There are also signs of radioactivity, but the cause of the difficulties itself is clearly no longer in the room, or the immediate area.  
 artificial radioactive isotope, serranium, which he had bombarded with alpha particles for 200 hours (8 days and 8 hours). Unfortunately, his so-far microscopic creation has taken on a life of its own:  it must absorb energy from its surroundings every 11 hours, and in the process doubles in size and mass each time, releasing deadly radiation and incredibly intense magnetic energy.  

The OSI officials realize that, with its rate of growth, it will only be a matter of days before it becomes large enough to affect the Earths rotation on its axis and spin it out of orbit.  They also discover that the isotope is impervious to any known means of destroying it or even rendering it inert.  The only answer appears to be to use a Canadian experimental power generator being constructed in a cavern under the ocean, with the hopes of bombarding the element with so much energy in one surge that it neutralizes itself with its own "gluttony".  

The two governments agree on this proposal, and the isotope is transferred to the project, dubbed the Deltatron, but there is a last minute objection from the engineer in charge of the project.  With no time to lose, the lead OSI agent commandeers the machine and barely escapes, risking his own life to activate it before sealing the cavern-filling, multi-story device off from the rest of the underground operation, which is filled with project workers.  The isotope is successfully stopped, as evidenced by the disappearance of the trace magnetism which it has produced following every energy absorption, although the Deltatron is also destroyed in the process.  As the film comes to an end, the lead OSI agent and his wife (who is expecting their first child) complete the purchase of their first house, and begin moving in.

==Cast== Richard Carlson as Dr. Jeffrey Stewart  
* King Donovan as Dr. Dan Forbes  
* Jean Byron as Connie Stewart  
* Harry Ellerbe as Dr. Allard  
* Leo Britt as Dr. Benton  
* Leonard Mudie as Howard Denker  
* Byron Foulger as Mr. Simon   Michael Fox as Dr. Serny  
* John Zaremba as Chief Watson (as John Zarimba)  
* Lee Phelps as City Engineer  
* Watson Downs as Mayor 
* Roy Engel as Gen. Behan (as Roy Engle)   
* Frank Gerstle as Col. Willis  
* John Vosper as Capt. Dyer   John Dodsworth as Dr. Carthwright
* Robert Carson as the Pilot (uncredited)
* Donald Kerr as Nova Scotia Lab Worker (uncredited)
* Kathleen Freeman as Nellie (the operator)

==Production notes==
* The film used ten minutes of footage of an atom smasher from the German science fiction thriller Gold (1934 film)|Gold (1934) directed by Karl Hartl and produced by Universum Film AG|UFA.   

* Although the music was composed by Blaine Sanford, it was actually performed by Korla Pandit (uncredited).

* The Los Alamos MANIAC computer was featured in an effort to lend a scientific air to the program. Also, the UCLA differential analyzer is shown briefly.

==References==
 	

== External links ==
*  
*  
*  


 

 
 
 
 
 