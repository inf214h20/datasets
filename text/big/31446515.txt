Marma Veeran
 
{{Infobox film
| name           = Marma Veeran மர்ம வீரன்
| image          =
| image_size     =
| caption        =
| director       = T. R. Raghunath 
| producer       = V. Govindarajan Sriram
| writer         = A. L. Narayanan 
| narrator       =
| starring       = Sriram Vyjayanthimala N. T. Rama Rao Sivaji Ganesan Gemini Ganesan Rajasulochana Chittor V. Nagaiah
| music          = S. Vedha
| cinematography = R. Sampath
| editing        = Kandaswamy
| studio         = Mehboob Studio
| distributor    =
| released       = August 3, 1956
| runtime        =
| country        = India Tamil
| budget         =
}} 1956 Tamil film written by A. L. Narayanan and directed by T. R. Raghunath and was produced by actor Sriram. The film starred Sriram and Vyjayanthimala in the lead roles with N. T. Rama Rao, Sivaji Ganesan and Gemini Ganesan in guest appearances with Rajasulochana, Chittor V. Nagaiah, P. S. Veerappa, J. P. Chandrababu, K. A. Thangavelu, M. N. Rajam and T. S. Balaiah forms an ensemble cast. V. Govindarajan of Jubilee Films was the co-producer. S . Vedha was introduced as the music director in the film. R. Sampath was the Director of Photography.

==Cast==
* Sriram as  Mahindra / Paramveer 
* Vyjayanthimala as Rajkumari Vijaya 
* Rajasulochana as  Kamini
* M. N. Rajam as  Mohini
* Chittor V. Nagaiah
* T. S. Balaiah
* J. P. Chandrababu
* P. S. Veerappa
* K. A. Thangavelu
* T. K. Ramachandran Helen as Narthaki in Special appearance 
* N. T. Rama Rao in Guest appearance
* Sivaji Ganesan in Guest appearance
* Gemini Ganesan in Guest appearance

==Crew==
*Producer: V. Govindarajan & Sriram 
*Production Company: 
*Director: T. R. Raghunath
*Music: S. Vedha
*Lyrics: Sundar Kannan, A. L. Narayanan, Thanjai N. Ramaiah Doss, Villiputhan & A. Maruthakasi
*Story: A. L. Narayanan
*Screenplay: A. L. Narayanan
*Dialogues: A. L. Narayanan
*Art Direction: 
*Editing: Kandaswamy
*Choreography: 
*Cinematography: R. Sampath
*Stunt: None Helen

==Soundtrack== Playback singers are T. M. Soundararajan, A. M. Rajah, T. A. Mothi, V. N. Sundharam, S. C. Krishnan, P. Leela, Jikki, P. Suseela, Raavu Balasaraswathi|R. Balasaraswathi Devi, K. Jamuna Rani & K. Rani.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Thudikkum Vaalibame || Raavu Balasaraswathi|R. Balasaraswathi Devi ||  || 03:22
|- 
| 2 || Aasai Ellam Niraasai || Jikki || ||
|- 
| 3 || Pavazha Naattu Ellaiyile Mullai Aadudhu || P. Suseela & K. Rani || || 03:16
|- 
| 4 || Sooriyanai.... Kaathirundhen Romba Nalla || K. Jamuna Rani || || 03:34
|- 
| 5 || Vizhi Pesuthe Vilaiyaadudhe || P. Suseela || || 04:24
|- 
| 6 || Anbirukkudhu Arivirukkudhu Panbirukkudhu || A. M. Rajah, T. A. Mothi & S. C. Krishnan || || 03:21
|- 
| 7 || O Aiyaa O Ammaa  || J. P. Chandrababu & K. Rani || || 00:43
|- 
| 8 || Munnale Pogaame  || T. M. Soundararajan & V. N. Sundharam || || 02:47
|- 
| 9 || Itthanai Naalaaga  || P. Leela & Jikki || || 04:12
|}

==References==
 

==External links==
*  
*   at Upperstall.com

 
 
 
 