The Thundering Mantis
 
 
{{Infobox film
| name             = The Thundering Mantis
| image            = TheThunderingMantis.jpg
| caption          = Film poster
| film name = {{Film name| traditional      = 癲螳螂
 | simplified       = 癲螳螂
 | pinyin           = Diān Táng Láng
 | jyutping         = Din1 Tong4 Long4}}
| director         =  Teddy Yip
| producer         = Stanley Chow
| writer           = 
| screenplay       = Cheung San-yee
| starring         = Bryan Leung Cheng Feng Eddy Ko| Eddy Ko Hung
| cinematography   = Cheung Tak-kon
| editing          = Wong Chau-kwai  Cheung Kwok-kuen 
| music            = Stanley Chow
| studio =  Goldig Films (H.K.) Ltd.  East Asia (H.K.) Film Co.
| distributor      = Goldig Films (H.K.) Ltd. (Hong Kong)
| released         =  
| runtime          = 85 minutes
| country          = Hong Kong
| language         = Cantonese
| budget           = 
| gross            = 
}} martial arts action film directed by Teddy Yip, starring Bryan Leung, Cheng Feng and Eddy Ko| Eddy Ko Hung.

==Plot==
A martial artist Ah Chi (Bryan Leung who hooks up with a kid sidekick, as a result of an old grudge the boys uncle is found murdered and the boy kidnapped. Ah Chi attempts to rescue the boy but the boy end up being tortured to death in before Ah Chis very eyes. The experience drives him to insanity, he eventually breaking free and seeks revenge on his captors. 

==Cast==
* Bryan Leung as Ah Chi, a fishmonger
* Cheng Feng as Shu Len
* Eddy Ko| Eddy Ko Hung as Shiao Tse-tung
* Wong Yat-lung	as The Kid
* Chin Yuet-sang as Grand Dad
* Lee Kwan as Fish shop owner
* Fang Mian as Shu Lens Dad
* Suen Lam as Pharmacy boss
* Ma Chin-ku as White suit underboss
* Shih Ting-ken	as White Suits lead thug

==Home media releases==

===VHS===
* In the United States the film was released on VHS 23 February 1999 by Xenon.  
* In the United Kingdom the film was released on VHS 11 March 1996 by Eastern Heroes  and later again 18 June 2001 by Mia. 

===DVD===
* In the United States the film was released on DVD on 26 February 2002 by Xenon.  Later 8 July 2003 Xenon re-released a digitally remastered version of the movie. 
* In the United Kingdom the film was released on DVD on 25 July 2005 by Mia. 

== References ==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 