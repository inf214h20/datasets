Accident on Hill Road
{{Infobox Film
| name           = Accident on Hill Road
| image          = Accident_on_Hill_Road_poster.jpg
| caption        =
| director       = Mahesh Nair
| producer       = Nari Hira
| story          = Mahesh Nair Siddharth Parmar Stuart Gordon
| starring       = Farooq Sheikh Abhimanyu Singh Celina Jaitley 
Rukhsar Rehman
| music          = Raju Singh
| cinematography = Ravi Walia
| editing        = Amit Parmar
| distributor    = Magna Film Production
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 2.5 crore 
| gross          = 
}}
Accident on Hill Road is a 2009 Bollywood film, directed by Mahesh Nair and produced by Nari Hira, starring Farooq Sheikh, Abhimanyu Singh, Celina Jaitley. The film is an authorised remake of the 2007 Hollywood film Stuck (2007 film)|Stuck     which is in turn based on the true story of Chante Mallard. 

==Plot==
When Sonam (Celina Jaitley) runs over a man Prakash (Farooq Sheikh) on her way home from a party one night she tries to dispose of him so no one will find out. She enlists the help of her drug dealer boyfriend Sid (Abhimanyu Singh). As soon as Sid begins to help the plan goes in an unexpected direction. It is now revealed that Prakash is alive. He, however plasters himself for sometime following to his injuries. Now Sonam finds out that Prakash is alive. To avoid a police case or any other hazard, she plans to kill him at any cost.

==Cast==
* Farooq Sheikh as Prakash Shrivastava
* Abhimanyu Singh as Sid
* Celina Jaitley as Sonam Chopra
* Manmeet Singh as Cab driver
* Rukhsar Rehman

==Release==

===Critical reception===
Accident on Hill Road received mostly negative reviews from critics. Rajeev Masand said the film is "unintentionally comical",  Taran Adarsh gave it only one star out of five  and Nithya Ramani of Rediff called it "a terrible excuse for a film". 

==See also== The Hitch-hiker, in which a similar vehicular homicide was portrayed where the zombie-like victim followed the driver all the way back to her garage.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 