Violent Life
 {{Infobox film
 | name = Violent Life (Una vita violenta)
 | image =  Una vita violenta.JPG
 | caption =
 | director =  Paolo Heusch   Brunello Rondi
 | writer = Pier Paolo Pasolini (novel), Paolo Heusch, Brunello Rondi, Pier Paolo Pasolini, Franco Solinas
 | music =   Piero Piccioni
 | cinematography =  Armando Nannuzzi 
 | editing =    Nino Baragli
 | starring = Franco Citti, Enrico Maria Salerno
 | producer =
 | distributor = Zebra Film
 | released =  1962
 | runtime = 115 min
 | awards =
 | country = Italy
 | language = Italian
 | budget = 
 }} 1961 Cinema Italian drama film by Paolo Heusch and Brunello Rondi, at his directorial debut. It is based on a novel with the same name by Pier Paolo Pasolini.   

The film was shown as part of a retrospective "Questi fantasmi: Cinema italiano ritrovato" at the 65th Venice International Film Festival. 

== Plot ==
The film tells the story of a group of kids who lives in a township of the poorest and most disreputable streets of Rome. The story is set in the typical climate of the Second World Wars end. Thomas is a guy almost adult who lives at the expense of others, like all his other companions, stealing and wasting time. In fact there is nothing in his area, there is no education, no job, no social centers... Thomas in fact goes along with cronies stealing garbage and pieces of old iron to resell them to messy and equally poor body of the township. Thus, gleaned a few hundred pounds, Thomas and friends can have fun going to prostitutes or watching movies at the cinema. Thomas lives for some years with these gimmicks, returning home only for dinner and going on nights like a whore into prostitution just to earn more money. But one day after a theft Thomas was arrested and when he comes out of prison his ideas change. Especially when he takes tuberculosis also has an inner struggle that leads him to think about his future as a human being and change their lives. Become honest, Thomas gets engaged to the beautiful Irene and also decided to participate in political activity, joining the Communist Party. However, a sudden accident truncates the young life of Thomas and all his brilliant projects.

== Cast ==
* Franco Citti: Tommaso
* Serena Vergano: Irene 
* Enrico Maria Salerno: Bernardini, the trade-union representative 
* Angelo Maria Santiamantini: Lello

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 