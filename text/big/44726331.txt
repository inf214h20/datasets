Thelma, Louise et Chantal
 
{{Infobox film
| name     = Thelma, Louise et Chantal
| image    =
| director = Benoît Pétré
| producer = Vérane Frédiani Franck Ribière Vincent Brançon Priscilla Bertin
| writer = Benoît Pétré Catherine Jacob   Jane Birkin   Caroline Cellier
| cinematography = Stephan Massis
| music = Keren Ann
| country = France
| language = French
| runtime = 90 minutes
| released =  
| gross = $2,749,009 
}}
Thelma, Louise et Chantal is a 2010 French comedy film directed by Benoît Pétré.

== Plot ==
Gabrielle, Nelly and Chantal are three girlfriends. They decide to travel together to the wedding of their ex at La Rochelle. During this trip, they share their blows of heart and rants.

== Cast ==
* Jane Birkin as Nelly
* Caroline Cellier as Gabrielle Catherine Jacob as Chantal
* Thierry Lhermitte as Phillipe
* Michèle Bernier as The bride
* Alysson Paradis as Elisa
* Sébastien Huberdeau as Mathieu
* Micheline Presle as Huguette
* Joséphine de Meaux as Sophie
* Arié Elmaleh as Nicolas
* Benoît Pétré as Hugo
* Stéphane Metzger as Bruno
* Nicolas Schweri as Clément
* Jean-Pierre Martins as the mechanic
* Eriq Ebouaney as the realtor
* Brice Fournier as Patrick
* Adeline Giroudon as Nadège
* Julie Meunier as Chloé

== References ==
 

== External links ==
* 

 
 
 

 