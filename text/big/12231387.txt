Perempuan Bergairah
{{Infobox film
| name = Perempuan Bergairah
| image = Perempuan_Bergairah_VCD.jpg
| caption =
| director = Jopi Burnama
| producer = Dhamoo Punjabi Raam Punjabi
| writer = Deddy Armand
| starring = Eva Arnaz Barry Prima
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 95 minutes
| country = Indonesia
| language = Indonesian
| budget =
}} Indonesian action film|action-drama film directed by Jopi Burnama and starring 1980s Indonesian action film stars Barry Prima and Eva Arnaz. 

==Plot==
Renny Basuki (Arnaz) is a young woman and a former judo champion who, after her fathers demise, tries to look after her impoverished family. When her younger brother is diagnosed with a deadly disease, she is desperate to afford his surgery costs. One day, Indra (Prima), a professional wrestling manager, offers Renny and her friend Mia (Diana Suarkom) to join a female wrestling troupe. They agree but Rennys mother disapproves her wrestling career.

==Ferocious Female Freedom Fighters==
Although Perempuan Bergairah also had an English-dubbed version titled The Fighters, Charles Kaufman, the brother of Lloyd Kaufman, president of Troma Entertainment, who distributed the film in the United States turned the footage into a dub parody that was released as Ferocious Female Freedom Fighters.

==References==
 

==External links==
* 
* 

 
 
 
 
 

 