Yaad Rakhegi Duniya
{{Infobox film
| name           = Yaad Rakhegi Duniya
| image          = Yaad Rakhegi Duniya.jpg
| image size      = 140px
| caption        =
| director       = Deepak Anand
| producer       = Nandu G. Tolani
| writer         = Anees Bazmee
| starring       = Aditya Pancholi  Rukhsar Rehman  Tinnu Anand
| music          = Anand-Milind
| cinematography = Manmohan Singh
| editing        =Deepak Anand
| distributor    = Paras Films International
| released       = 1 March 1992
| runtime        =
| country        = India
| language       = Hindi
| Budget         =
| preceded by    =
| followed by    =
| awards         =
| Gross          =
}}
 1992 Bollywood romantic comedy film directed by Deepak Anand on his debut. Starring Aditya Pancholi and Rukhsar, it premiered on 1 March 1992 in Mumbai. It is a remake of Telugu blockbuster Geethanjali (1989 film)|Geethanjali directed by Mani Ratnam.

==Plot==
Vicky Anand (Aditya Pancholi) has just graduated from college with honors and decides to celebrate. An accident leads him to the hospital and the shocking discovery that hes suffering from a terminal illness. He decides to move to the lush, green locales of Ooty, a hill-station hoping to find peace and solitude, where he meets  Naina Rukhsar Rehman, a precocious young woman who enjoy playing pranks. Vicky finds support and encouragement befriending her and eventually falls in love with the mischievous and outgoing, Naina, who also suffers from a terminal illness. Vicky comes to know about Nainas illness, and discusses this with her doctor dad, who confirms it, saying that there is no cure. Despite of this, Vicky wants to marry her. When Vickys mom comes to visit her son, he tells her about his love for Naina, and she is delighted. She rushes over to meet Naina for the first time, and is pleased with Vickys choice. Then unknowingly she blurts out a truth so devastating, that it will change Nainas final remaining days forever.

==Cast==
* Aditya Pancholi as  Vicky Anand
* Rukhsar Rehman as  Naina
* Tinnu Anand as Shikari
* Vikram Gokhale as Doctor, Nainas dad
* Anjana Mumtaz as Mrs. Anand, Vickys mother
* Yunus Parvez as Shikaris advisor
* Dina Pathak as  Nainas paternal grandmother
* Radha Seth as  Komal, Shikaris wife

==Trackslist==
The music was composed by Anand-Milind while Sameer wrote the lyrics. The soundtrack is remembered for the melodious song Tere Liye Saari Umar sung by Amit Kumar.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
|"Tujhe Rab Ne Banaya" Mohammad Aziz, Sadhana Sargam
|-
| 2
|"Ae Hawa Aake Thaam Le Anchal"
| Sadhana Sargam
|-
| 3
| "Gali Gali Mein Gana" Amit Kumar
|-
| 4
| "Tooti Khidki makdi ka Jangla"
| Amit Kumar, Kavita Krishnamurthy
|-
| 5
| "Tere Liye Saari Umar"
| Amit Kumar
|}

== External links ==
*  

 
 
 
 
 
 


 
 