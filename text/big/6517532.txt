Roselyne et les lions
{{Infobox film
| name           = Roselyne et les lions
| image          = 
| image_size     = 
| caption        = 
| director       = Jean-Jacques Beineix
| producer       = 
| writer         = Jean-Jacques Beineix Jacques Forgeas Thierry Le Portier
| starring       = Isabelle Pasco Gérard Sandoz Gabriel Monnet
| music          = Reinhardt Wagner
| cinematography = Jean-François Robin
| editing        = Anick Baly Marie Castro Danielle Fillios	 
| production_company = Cargo Films Gaumont Production
| distributor    = 
| released       = April 12, 1989 (France)  	
| runtime        = 
| country        = France
| awards         =  French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Roselyne et les lions (Roselyne and the Lions) is a 1989 French film directed by Jean-Jacques Beineix.

Thierry does odd jobs in a zoo in Marseille in exchange for lessons from Frazier the lion trainer. He meets Roselyne and they leave together to find work. They are taken on by a circus in Munich and achieve success as lion trainers when the trainer Klint loses his nerve.

==Cast==
* Isabelle Pasco - Roselyne
* Gérard Sandoz - Thierry
* Gabriel Monnet - Frazier
* Philippe Clévenot - Bracquard
* Günter Meisner - Klint
* Wolf Harnisch - Koenig
* Jacques Le Carpentier - Markovitch
* Carlos Pavlidis - Petit Prince
* Jacques Mathou - Armani
* Dumitru Furdui - Stainer
* Jaroslav Vízner - Gunter
* Patrice Abbou - Ben Jemoul 2
* Silvio Bolino - Touron
* François Boum - Ben Jemoul 1
* Guillaume Briat - Matou

== External links ==
*  

 
 
 
 
 
 

 