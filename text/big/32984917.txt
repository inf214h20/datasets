Oliyambukal
{{Infobox film 
| name           = Oliyambukal
| image          = Oliyambukal Film Poster.jpg
| alt            =
| caption        = Cover of Oliyambukal Hariharan
| producer       = K.R.Gopalakrishnan
| writer         = Dennis Joseph Rekha Aishwarya Aishwarya Thilakan Sukumaran Rajan P. Dev
| music          = MS Viswanathan
| cinematography = Anandakkuttan
| editing        = MS Mani
| studio         = K. R. Gangadharan|K.R.G
| distributor    = K. R. Gangadharan|K.R.G Release
| released       =  
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Oliyambukal is a 1990 Malayalam film. Directed by Hariharan, this film revolved around a young mans revenge against his fathers killers though all possible ways.      

==Cast==
{| class="wikitable sortable"
|-
! Artist 
|-
| Mammootty
|- Rekha
|- Aishwarya
|-
| Thilakan
|-
| Sukumaran
|-
| Rajan P. Dev
|-
| Jagathi Sreekumar
|-
| Charan Raj
|}
 
 

==Music==
{| class="wikitable"
|- style="background:#d1e4fd;"
! Song !! Singers!! Lyrics !! Music
|-
|  Aadi prakrithi  || MG Sreekumar, Sujatha Mohan  || ONV Kurup  || MS Viswanathan
|-
| Vishukkili || MG Sreekumar, P Susheela  || ONV Kurup  || MS Viswanathan
|}
  

==External links==
* 

==References==
 

 
 
 
 

 