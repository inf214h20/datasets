Metropolis (1927 film)
 
 
{{Infobox film 
| name           = Metropolis
| image          = Metropolisposter.jpg
| alt            = 
| caption        = original 1927 theatrical release poster
| director       = Fritz Lang
| producer       = Erich Pommer
| writer         = Thea von Harbou Fritz Lang (uncredited)
| starring       = Alfred Abel Brigitte Helm Gustav Fröhlich Rudolf Klein-Rogge
| music          = Gottfried Huppertz (original score)
| cinematography = Karl Freund Günther Rittau Walter Ruttmann
| editing        = UFA Paramount Pictures (US)
| released       = 10 January 1927 
| runtime        = 153 minutes   118 minutes   148 minutes  
| country        = Germany (Weimar Republic)
| language       = Silent film German intertitles
| budget         = 5,100,000 Reichsmarks (estimated)
| gross          = 75,000 Reichsmarks (estimated)
}} German expressionist epic Science fiction film|science-fiction drama film directed by Fritz Lang. Lang and his wife Thea von Harbou wrote the silent film, which starred Brigitte Helm, Gustav Fröhlich, Alfred Abel and Rudolf Klein-Rogge. Erich Pommer produced it in the Babelsberg Studios for Universum Film A.G..  It is regarded  as a pioneering work of the science-fiction genre in movies, being among the first feature length movies of the genre.
 Weimar Period, Metropolis is set in a futuristic urban dystopia and follows the attempts of Freder, the wealthy son of the citys ruler, and Maria, a poor worker, to overcome the vast gulf separating the classes of their city. Filming took place in 1925 at a cost of approximately five million Reichsmarks, making it the most expensive film ever released up to that point. The motion pictures futuristic style shows the influence of the work of the Futurist Italian architect Antonio SantElia. {{cite web|url=http://www.irishartsreview.com/green-energy/?showall=1|title= Irish Arts Review
|publisher= Irishartsreview.com|accessdate= 27 September 2014| deadurl= no
}} 

The film met with a mixed response upon its initial release, with many critics praising its technical achievements and social metaphors while others derided its "simplistic and naïve" presentation. Because of its long running-time and the inclusion of footage which censors found questionable, Metropolis was cut substantially after its German premiere: large portions of the film went missing over the subsequent decades.

Numerous attempts have been made to restore the film since the 1970s-1980s. Music producer Giorgio Moroder released a version with a soundtrack by rock artists such as Freddie Mercury, Loverboy and Adam Ant in 1984. A new reconstruction of Metropolis was shown at the Berlin Film Festival in 2001, and the film was inscribed on UNESCOs Memory of the World Register in the same year, the first film thus distinguished. In 2008 a damaged print of Langs original cut of the film was found in a museum in Argentina. After a long restoration process, the film was 95% restored and shown on large screens in Berlin and Frankfurt simultaneously on 12 February 2010.

==Plot==
In the future of 2026 (100 years after the film was produced), wealthy industrialists rule the vast city of Metropolis from high-rise tower complexes, while a lower class of underground-dwelling workers toil constantly to operate the machines that provide its power. The Master of Metropolis is the ruthless Joh Fredersen, whose son Freder idles away his time in a pleasure garden with the other children of the rich. Freder is interrupted by the arrival of a young woman named Maria, who has brought a group of workers children to see the privileged lifestyle led by the rich. Maria and the children are quickly ushered away, but Freder is fascinated by Maria and descends to the workers city in an attempt to find her.

Freder finds himself in the machine rooms and watches in horror as a huge machine explodes, causing several injuries and deaths, after one of its operators collapses from exhaustion. Freder runs to tell his father; Grot (Heinrich George), one of the foremen, arrives soon afterward to deliver maps found on the bodies of the dead workers. Fredersen is angered that his assistant Josaphat has failed to be the first to bring him news of either the explosion or the maps, and fires him. Knowing that he can only go into the depths and become a worker, Josaphat attempts suicide but is stopped by Freder, who sends him home to wait for him. Concerned by Freders unusual behavior, Fredersen dispatches the Thin Man to keep track of his movements.

Returning to the machine rooms, Freder encounters the worker Georgy and takes his place when he collapses at his post. The two men trade clothes, with Freder instructing Georgy to go to Josaphats apartment and wait for him. However, while being driven away by Freders chauffeur, Georgy becomes distracted by the sights and sounds in the licentious Yoshiwara nightclub and spends the evening there instead. Meanwhile, Freder finds a map in his pocket and learns of a secret meeting from another worker as he suffers hallucinations brought on by the exhausting shift.

Fredersen takes the maps brought by Grot to the inventor Rotwang in order to learn their meaning. Rotwang had been in love with a woman named Hel, who left him to marry Fredersen; she died giving birth to Freder. He has since built a robot (a Maschinenmensch, or Machine-Human) to "resurrect" her. The maps show the layout of a network of ancient catacombs beneath Metropolis, and the two men leave to investigate. They eavesdrop on a gathering of workers, including Freder, and find Maria waiting to address them.

 , Germany]]
Maria prophesies the arrival of a mediator who can bring the working and ruling classes together, and urges the workers to have patience. Freder comes to believe that he could fill the role, and after the meeting breaks up, he declares his love for her. They agree to meet in the city cathedral the next day, then part. Fredersen orders Rotwang to give Marias likeness to the robot so that it can ruin her reputation among the workers, but does not know of Rotwangs secret plan to destroy Freder as revenge for losing Hel. Rotwang chases Maria up through the catacombs and kidnaps her.

The next morning, the Thin Man catches Georgy leaving Yoshiwara, orders him to return to his post, and takes Josaphats address from him. Freder goes to Josaphats apartment in search of Georgy, but finds that Georgy never arrived. After telling Josaphat of his time in the workers city, Freder leaves for the cathedral, just missing the arrival of the Thin Man. Josaphat rebuffs the Thin Mans attempts to bribe and intimidate him into leaving Metropolis; the two fight, and Josaphat escapes to hide in the workers city.
 The Seven Deadly Sins, he begs them not to harm Maria, then leaves to search for her. He hears her cries while passing Rotwangs house and ends up trapped inside until the robot has been fully transformed into Marias double. Rotwang sends her to greet Fredersen; Freder finds the two embracing in his office and faints, falling into a prolonged delirium. The false Maria begins to unleash chaos throughout Metropolis, driving men to murder out of lust for her in Yoshiwara and stirring dissent amongst the workers.

Freder recovers 10 days later and seeks out Josaphat, who tells him of the spreading trouble. At the same time, the real Maria escapes from Rotwangs house after Fredersen breaks in to fight with him, having learned of Rotwangs treachery. Descending to the catacombs, Freder and Josaphat find the false Maria urging the workers to rise up and destroy the machines. When Freder accuses her of not being the real Maria, the workers recognize him as Fredersens son and rush him, but Georgy protects him and is stabbed to death. Fredersen orders that the workers be allowed to rampage, so that he can justifiably use force against them at a later time.

The workers follow the false Maria from their city to the machine rooms, unknowingly leaving their children behind. They abandon their posts and destroy the Heart Machine, the central power station for Metropolis, after its foreman Grot reluctantly grants them access to it on Fredersens orders. As all systems above and below ground fail, Maria descends to the workers city, which begins to flood due to the stopped water pumps. She gathers the children in the main square, and with help from Freder and Josaphat, they escape from the workers city as it crumbles in the flood.

In the machine rooms, Grot gets the attention of the wildly celebrating workers and berates them for their out-of-control actions. Realizing that they left their children behind in the now-flooded city, the workers go mad with grief and storm out to avenge themselves upon the "witch" (the false Maria), who spurred them on and has since slipped away to join the revelry at Yoshiwara. Meanwhile, Rotwang has fallen under the delusion that Maria is Hel and sets out to find her. The mob captures the false Maria and burns her at the stake; a horrified Freder watches, not understanding the deception until the outer covering disintegrates to reveal the robot underneath.

Rotwang chases Maria to the roof of the cathedral, pursued by Freder, and the two men fight as Fredersen and the workers watch from the street. Josaphat tells the workers of their childrens safety to stop them from harming Fredersen. Rotwang loses his balance and falls to his death. On the cathedral steps, Freder fulfills his role as mediator ("heart"), linking the hands of Fredersen (the citys "head") and Grot (its "hands") to bring them together.

==Cast==
* Gustav Fröhlich as Freder robot double 
* Alfred Abel as Joh Fredersen, the master of Metropolis and Freders father.
* Rudolf Klein-Rogge as Rotwang, a scientist
* Heinrich George as Grot, the foreman of the Heart Machine
* Fritz Rasp as The Thin Man, Fredersens spy
* Theodor Loos as Josaphat, Fredersens assistant
* Erwin Biswanger as Georgy (or 11811), a worker
* Helen von Münchofen as a Lady in the Pleasure Garden

==Influences==
{{multiple image
 | align     = right
 | direction = vertical
 | header    =
 | width     = 267
 | image1    = Metropolis-new-tower-of-babel.png
 | caption1  = The New Tower of Babel, Fredersens headquarters in Metropolis.
 | image2    = Pieter Bruegel the Elder - The Tower of Babel (Vienna) - Google Art Project - edited.jpg this 1563 Pieter Brueghel. 
}}
Metropolis features a range of elaborate special effects and set designs, ranging from a huge gothic cathedral to a futuristic cityscape. In an interview, Fritz Lang reported that "the film was born from my first sight of the skyscrapers in New York in October 1924". He had visited New York for the first time and remarked "I looked into the streets &ndash; the glaring lights and the tall buildings &ndash; and there I conceived Metropolis."  Describing his first impressions of the city, Lang said that "the buildings seemed to be a vertical sail, scintillating and very light, a luxurious backdrop, suspended in the dark sky to dazzle, distract and hypnotize".  He added "The sight of Neuyork   alone should be enough to turn this beacon of beauty into the center of a film" 

The appearance of the city in Metropolis is strongly informed by the Art Deco movement; however it also incorporates elements from other traditions. Ingeborg Hoesterey described the architecture featured in Metropolis as eclectic, writing how its locales represent both “functionalist modernism   art deco” whilst also featuring “the scientist’s archaic little house with its high-powered laboratory, the catacombs   the Gothic cathedral”. The film’s use of art deco architecture was highly influential, and has been reported to have contributed to the style’s subsequent popularity in Europe and America. 

The film drew heavily on biblical sources for several of its key set-pieces. During her first talk to the workers, Maria uses the story of the Tower of Babel to highlight the discord between the intellectuals and the workers. Additionally, a delusional Freder imagines the false-Maria as the Whore of Babylon, riding on the back of a many-headed dragon. Also, the name of the Yoshiwara club alludes to the famous red-light district of Tokyo. 

==Production==

===Pre-production=== Weimar Germany. The films plot originated from a novel of the same title written by Harbou for the sole purpose of being made into a film. The novel in turn drew inspiration from H. G. Wells, Shelley and Villiers dIsle Adams works and other German dramas.  The novel featured strongly in the films marketing campaign, and was serialized in the journal Illustriertes Blatt in the run-up to its release. Harbou and Lang collaborated on the screenplay derived from the novel, and several plot points and thematic elements — including most of the references to magic and occultism present in the novel — were dropped. The screenplay itself went through many re-writes, and at one point featured an ending where Freder would have flown to the stars; this plot element later became the basis for Langs Woman in the Moon. 

===Filming===
Metropolis began principal photography on 22 May 1925 with an initial budget of 1.5 million reichsmarks.  The cast of the film was mostly composed of unknown actors; Heinrich George was a theater actor, Gustav Fröhlich was a journalist and 19-year-old Brigitte Helm who had no previous film experience though she had given the trial shots for the film Die Nibelungen. 

Shooting of the film was a draining experience for the actors involved, due to the demands that Lang placed on them. For the scene where the workers city was flooded, Helm and 500 children from the poorest districts of Berlin had to work for 14 days in a pool of water that Lang intentionally kept at a low temperature.  Lang would frequently demand numerous re-takes, and took two days to shoot a simple scene where Freder collapses at Marias feet; by the time Lang was satisfied with the footage he had shot, actor Gustav Fröhlich found he could barely stand.  Other anecdotes involve Langs insistence on using real fire for the climactic scene where the false Maria is burnt at the stake (which resulted in Helms dress catching fire), and his ordering extras to throw themselves towards powerful jets of water when filming the flooding of the workers city.     UFA invited several trade journal representatives and several film critics to see the films shooting as parts of its promotion campaign. 

Helm recalled her experiences of shooting the film in a contemporary interview, saying that "the night shots lasted three weeks, and even if they did lead to the greatest dramatic moments — even if we did follow Fritz Lang’s directions as though in a trance, enthusiastic and enraptured at the same time — I can’t forget the incredible strain that they put us under. The work wasn’t easy, and the authenticity in the portrayal ended up testing our nerves now and then. For instance, it wasn’t fun at all when Grot drags me by the hair, to have me burned at the stake. Once I even fainted: during the transformation scene, Maria, as the android, is clamped in a kind of wooden armament, and because the shot took so long, I didn’t get enough air." 

Shooting lasted over a year, and was finally completed on 30 October 1926.  By the time shooting finished, the films budget leapt to 5.1 million reichsmarks. 

===Special effects=== miniatures of the city, a camera on a swing, and most notably, the Schüfftan process,  in which mirrors are used to create the illusion that actors are occupying miniature sets. This new technique was seen again just two years later in Alfred Hitchcocks film Blackmail (1929 film)|Blackmail (1929). 

The Maschinenmensch — the robot built by Rotwang to resurrect his lost love Hel — was created by sculptor Walter Schulze-Mittendorff. A whole-body plaster cast was taken of actress Brigitte Helm, and the costume was then constructed around it. A chance discovery of a sample of "plastic wood" (a pliable substance designed as wood-filler) allowed Schulze-Mittendorff to build a costume that would both appear metallic and allow a small amount of free movement.  Helm sustained cuts and bruises while in character as the robot, as the costume was rigid and uncomfortable. 

==Music==

===Original score=== score was composed for a large orchestra by Gottfried Huppertz. Huppertz drew inspiration from Richard Wagner and Richard Strauss, and combined a classical orchestral voice with mild modernist touches to portray the films massive industrial city of workers.  Nestled within the original score were quotations of Claude Joseph Rouget de Lisles "La Marseillaise" and the traditional "Dies Irae," the latter of which was matched to the films apocalyptic imagery. Huppertzs music played a prominent role during the films production; oftentimes, the composer played piano on Langs set in order to inform the actors performances.
 Rundfunksinfonieorchester Saarbrücken. It was the first release of the reasonably reconstructed movie to be accompanied by Huppertzs original score. In 2007, Huppertzs score was also played live by the VCS Radio Symphony, which accompanied the restored version of the film at Brenden Theatres in Vacaville, California.  The score was also produced in a salon orchestration, which was performed for the first time in the United States in August 2007 by The Bijou Orchestra under the direction of Leo Najar as part of a German Expressionist film festival in Bay City, Michigan.  The same forces also performed the work at the Traverse City Film Festival in Traverse City, Michigan in August 2009.
 Berlin Radio Symphony Orchestra, conducted by Frank Strobel. Strobel also conducted the premiere of the reconstructed score at Berlin Friedrichstadt Palast|Friedrichstadtpalast.

===Other soundtracks=== Hugh Davies. techno score for Metropolis which was released as an album. He also performed the score live at public screenings of the film. In 2004 Abel Korzeniowski created a score for Metropolis played live by a 90-piece orchestra and a choir of 60 voices and two soloists. The first performance took place at the Era Nowe Horyzonty Film Festival in Poland. The same year, Ronnie Cramer produced a score and effects soundtrack for Metropolis that won two Aurora awards.  The New Pollutants (Mister Speed and DJ Tr!p) has performed Metropolis Rescore live for festivals since 2005  and are rescoring to the 2010 version of the film for premiere at the 2011 Adelaide Film Festival.  In 2010, the Alloy Orchestra has scored four different versions of the film, most recently for the American premiere of the 2010 restoration. In 2014 the pianist/composer, Dmytro Morykit, created a new live piano score which received a standing ovation to a sell-out audience at Wiltons Music Hall in London. 

==Release== premiere at Channing Pollock to write a simpler version of the film that could be assembled using the existing material. Pollock shortened the film dramatically, altered its inter-titles and removed all references to the character of Hel (as the name sounded too similar to the English word Hell), thereby removing Rotwangs original motivation for creating his robot. In Pollocks cut, the film ran for 3170 meters, or approximately 115 minutes. This version of Metropolis premiered in the US in March 1927, and was released in the UK around the same time with different title cards. 
 nationalist businessman, cancelled UFAs debt to Paramount and Metro-Goldwyn-Mayer after taking charge of the company in April 1927, and chose to halt distribution in German cinemas of Metropolis in its original form. Hugenberg had the film cut down to a length of 3241 meters, removing the films perceived "inappropriate" communist subtext and religious imagery. Hugenbergs cut of the film was released in German cinemas in August 1927. UFA distributed a still shorter version of the film (2530 meters, 91 minutes) in 1936, and an English version of this cut was archived in the MOMA film library. 

===Reception===
Despite the films later reputation, some contemporary critics panned it. The New York Times critic Mordaunt Hall called it a "technical marvel with feet of clay".    The Times went on the next month to publish a lengthy review by H. G. Wells who accused it of "foolishness, cliché, platitude, and muddlement about mechanical progress and progress in general." He faulted Metropolis for its premise that automation created drudgery rather than relieving it, wondered who was buying the machines output if not the workers, and found parts of the story derivative of Shelleys Frankenstein, Karel Čapeks robot stories, and his own The Sleeper Awakes.  Wells called Metropolis "quite the silliest film."

Writing in The New Yorker, Oliver Claxton called it "unconvincing and overlong", faulting much of the plot as "laid on with a terrible Teutonic heaviness, and an unnecessary amount of philosophizing in the beginning" that made the film "as soulless as the city of its tale." He also described the acting as "uninspired with the exception of Brigitte Helm". Nevertheless, Claxton wrote that "the setting, the use of people and their movement, and various bits of action stand out as extraordinary and make it nearly an obligatory picture." 

Nazi propagandist Joseph Goebbels was impressed with the films message of social justice. In a 1928 speech he declared that "the political bourgeoisie is about to leave the stage of history. In its place advance the oppressed producers of the head and hand, the forces of Labor, to begin their historical mission". 

Fritz Lang himself later expressed dissatisfaction with the film. In an interview with Peter Bogdanovich (in Who The Devil Made It: Conversations with Legendary Film Directors, published in 1998), he expressed his reservations:
 
The main thesis was Mrs. Von Harbous, but I am at least 50 percent responsible because I did it. I was not so politically minded in those days as I am now. You cannot make a social-conscious picture in which you say that the intermediary between the hand and the brain is the heart. I mean, thats a fairy tale&nbsp;– definitely. But I was very interested in machines. Anyway, I didnt like the picture&nbsp;– thought it was silly and stupid&nbsp;– then, when I saw the astronauts: what else are they but part of a machine? Its very hard to talk about pictures—should I say now that I like Metropolis because something I have seen in my imagination comes true, when I detested it after it was finished?
 
In his profile for Lang featured in the same book, which prefaces the interview, Bogdanovich suggested that Langs distaste for his own film also stemmed from the Nazi Partys fascination with the film. Von Harbou became a passionate member of the Nazi Party in 1933. They divorced the following year. 

====Contemporary acclaim====
Roger Ebert noted that "Metropolis is one of the great achievements of the silent era, a work so audacious in its vision and so angry in its message that it is, if anything, more powerful today than when it was made."  The film also has a 99% rating at Rotten Tomatoes, based on 116 reviews.  The film was ranked No. 12 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010, {{cite web
| title = The 100 Best Films Of World Cinema &#124; 12. Metropolis
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=12
| work = Empire
}}  and it was ranked number 2 in a list of the 100 greatest films of the Silent Era.  The 2002 version awarded the "New York Film Critics Circle Awards" "Special Award" to Kino International for the restoration. In 2012, in correspondence with the Sight & Sound Poll, the British Film Institute called   ″Metropolis″ the 35th greatest film of all time.   

===Restorations===
 ]]
The original premiere cut of Metropolis has been lost, and for decades the film could be seen only in heavily truncated edits that lacked nearly a quarter of the original length. However, over the years, various elements of footage have been rediscovered, so that by 2010 it is possible to see the film in almost its original form. 
 Love Kills" and Worst Musical Score for Moroder.  In August 2011, after years of the Moroder version being unavailable on video in any format due to music licensing issues, it was announced that Kino International had managed to resolve the issues, and not only would the film be released on both Blu-Ray and DVD in November of that year, but it would also have a limited theatrical re-release. 

The moderate commercial success of the Moroder version of the film inspired Enno Patalas to make an exhaustive attempt to restore the movie in 1986. This version was the most accurate reconstruction until that time, being based on the film’s script and musical score. The basis of Patalas work was a copy in the Museum of Modern Arts collection.  Kino International, Metropolis’s current copyright holder, the F.W. Murnau Foundation released a digitally restored version of the film in 2002 entitled the Restored Authorized Edition. This edition includes the film’s original music score and title cards that describe the events featured in missing sequences. The footage was digitally cleaned and repaired to remove defects.

On 1 July 2008, film experts in Berlin announced that a 16mm reduction negative of the original cut of the film had been discovered in the archives of the Museo del Cine   in Buenos Aires, Argentina.  The print had been in circulation since 1928, starting off with a film distributor, and subsequently being passed to a private collector, an art foundation, and finally the Museo del Cine. The print was investigated by the museum’s curator, Argentinian film collector, curator and historian Fernando Martín Peña, after he heard an anecdote from a cinema club manager expressing surprise at the length of a print of Metropolis he had viewed.   
 National Film Archive of New Zealand. Organ discovered that the print contained scenes missing from other copies of the film. After hearing of the discovery of the Argentine print of the film, and the restoration project currently under way, Organ contacted the German restorers about his find. The New Zealand print contained 11 missing scenes and featured some brief pieces of footage that were used to restore damaged sections of the Argentine print. It is believed that the Australian, New Zealand and Argentine prints were all scored from the same master. The newly discovered footage was used in the restoration project.    The Argentine print was in poor condition and required considerable restoration before it was re-premiered in February 2010. Two short sequences from the film, depicting a monk preaching in the cathedral and a fight between Rotwang and Fredersen, were in extremely poor condition and could not be salvaged, according to explanatory information included within the restored film. This new restoration was released on DVD and Blu-Ray by Kino Video in 2010 under the title The Complete Metropolis.

===Copyright issues===
The American copyright lapsed in 1953, which eventually led to a proliferation of versions being released on video. Along with other foreign-made works, the films U.S. copyright was restored in 1996,  but the constitutionality of this copyright extension was challenged in Golan v. Gonzales and, as Golan v. Holder, it was ruled that "In the United States, that body of law includes the bedrock principle that works in the public domain remain in the public domain. Removing works from the public domain violated Plaintiffs vested First Amendment interests."  This only applied to the rights of so-called reliance parties, i.e. parties who had previously relied on the public domain status of restored works. The case was overturned on appeal to the Tenth Circuit,  and that decision was upheld by the US Supreme Court on 18 January 2012. This had the effect of restoring the copyright in the work as of 1 January 1996. Under current US copyright law, it remains copyrighted until 1 January 2023. § 65 co-authors, cinematographic works, musical composition with words
 
(1) If the copyright in the work is owned by several co-authors (§ 8), it continues for seventy years after the death of the last surviving author. 
(2) In the case of film works and works similar to cinematographic works, copyright expires seventy years after the death of the last survivor of the following persons: the principal director, author of the screenplay, author of the dialogue, the composer of music for the cinematographic music. 
(3) The term of protection of a musical composition with words shall expire 70 years after the death of the last survivor of the following persons: Author of the text, the composer of the musical composition, provided that both contributions specifically for the particular musical composition with words were created. This applies regardless of whether such persons have been designated as co-authors.  

==Adaptations==
* Several adaptations have been made of the original Metropolis, including at a 1989 musical theatre adaptation, Metropolis (musical)|Metropolis. 
* In December 2007, producer Thomas Schühly (Alexander (2004 film)|Alexander, The Adventures of Baron Munchausen) gained the remake rights to Metropolis, but it is still unknown if the remake is in production.  Metropolis Manga feature length anime, released in 2001.

==In popular culture== Express Yourself" pays homage to the film and Fritz Lang.     Some scenes from the film were featured in the music video for Queen (band)|Queens 1984 hit "Radio Ga Ga". The tracks were prepared by Freddie Mercury.  Queen of the Night" includes clips from the film as well as Houston wearing a shiny metallic ensemble resembling Maschinenmensch. The Man-Machine into Maria.
    released mid-2007 and The ArchAndroid released in 2009. The latter also included an homage to Metropolis on the album cover, with the film version of the Tower of Babel among the remainder of the city. The albums follow the adventures of Monáes alter-ego and robot, Cindi Mayweather, as a messianic figure to the android community of Metropolis.  
Pop singer-songwriter Lady Gaga has made a series of references to Langs film within her music videos. Visual allusions to the film are noted most predominantly in her music videos for Alejandro, Born this Way and Applause.  metal band Sepultura named their 2013 album The Mediator Between Head and Hands Must Be the Heart after a quote from the film.  The 2014 music video "Digital Witness" by St. Vincent (musician)|St. Vincent in collaboration with Chino Moya presents "a surreal, pastel-hued future" in which lead singer Annie Clark is a stand-in for Maria. 
Soap&Skin uses many scenes from the movie in the music video for Sugarbread (EP)|Sugarbread.

==See also==
 
* List of dystopian films
* List of films featuring surveillance
* List of films in the public domain in the United States
* List of German films 1919–1933
* List of most expensive non-English language films
* List of rediscovered films

==Notes==
 

==References==
Explanatory notes
 

Citations
 

Bibliography
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 