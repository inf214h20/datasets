American History X
{{Infobox film
| name = American History X
| image = American history x poster.jpg
| caption = Theatrical release poster Tony Kaye
| producer = John Morrissey David McKenna
| starring = {{Plain list|
* Edward Norton
* Edward Furlong
* Fairuza Balk
* Stacy Keach
* Ethan Suplee
* Elliott Gould
* Avery Brooks
* Beverly DAngelo
 
}}
| music          = Anne Dudley
| cinematography = Tony Kaye
| editing = {{Plain list| Jerry Greenberg
* Alan Heim
}}
| studio =
| distributor = New Line Cinema
| released =     (DVD)
| runtime = 119 minutes
| country = United States
| language = English
| budget = $20 million   
| gross = $23.9 million 
}}
 Tony Kaye David McKenna. It stars Edward Norton and Edward Furlong, and co-stars Fairuza Balk, Stacy Keach, Elliott Gould, Avery Brooks, Ethan Suplee and Beverly DAngelo. The film was released in the United States on October 30, 1998 and was distributed by New Line Cinema.
 MPAA for "graphic brutal violence including rape, pervasive language, strong sexuality and nudity". Made on a budget of $20 million, it grossed over $23 million at the international box office.
 Best Actor. In September 2008, Empire (magazine)|Empire magazine named it the 311th Greatest Movie of All Time. 

==Plot==
Danny Vinyard (Edward Furlong), a high school student and budding Neo-Nazism|neo-Nazi in Venice Beach, California, receives an assignment from  Murray (Elliott Gould), his history teacher, to write a paper on "any book which relates to the struggle for human rights." Knowing Murray is Jewish, Danny writes his paper on Adolf Hitlers Mein Kampf. Murray attempts to get Danny expelled for doing this, but Principal Dr. Bob Sweeney (Avery Brooks) — who is black — refuses, instead informing Danny that he will study history and current events under Sweeney, and that the class will be called "American History X." Dannys first assignment is to prepare a paper on his brother Derek (Edward Norton), a former neo-Nazism|neo-Nazi leader who has just been released from prison after serving three years for voluntary manslaughter. Danny is warned that failing to submit the paper the next morning will result in his expulsion. The rest of the movie alternates between a series of vignettes from Danny and Dereks shared past (distinguished by being shown in black and white), and present day events (shown in color).
 Richard Wrights white supremacist gang called the D.O.C.
 Crips in which the prize is control of the recreation center basketball courts. Derek and his friends win the game. Later that evening, Derek leads a large gang of skinheads to attack a supermarket that has now lawfully been taken over and owned by a Korean who was accused of hiring Mexicans to take control of the store. They wreck the store, robbing it, and Derek tortures an African-American woman before escaping after the robbery. The next day, his mother Doris (Beverly DAngelo) invites Murray, whom she is dating, home for dinner. A dinnertime discussion about Rodney King and police brutality turns into a full-blown argument between Derek and Murray. When Derek reveals his swastika tattoo and threatens Murray with violence for "invading his family", Murray leaves and Doris orders Derek out of her home. That night, as Derek and his girlfriend Stacey (Fairuza Balk) have sex, Danny hears people (the three gang members whom Derek beat at basketball) attempting to steal Dereks truck. Derek grabs a pistol and heads outside. He shoots one of the thieves to death and curb stomps another. Immediately arrested, Derek is sentenced to three years at the California Institution for Men in Chino, California|Chino.

Derek is given a job in the prison laundry and is assigned to be the partner of Lamont (Guy Torry), a black man who is serving six years for assault. Lamont stole a television set from a store and broke the arresting officers foot when he accidentally dropped the television on him. The pair develop a rapport from their shared love of basketball.

In prison, Derek joins the Aryan Brotherhood but, after about a year, he becomes disillusioned with the racist gang, particularly over the groups hypocritical friendly relations with a Mexican gang member, and their trafficking of narcotics. In response to Dereks criticisms, Aryan Brotherhood members savagely beat and rape him in the shower. While recovering from the attack, Derek is visited by Sweeney, whom he asks for help to be paroled. Sweeney informs him of Dannys involvement with neo-Nazis, and warns that he is on the same path as his older brother. Sweeney confesses that he hated white people as a youth, but eventually realized that hatred is pointless.

Derek further distances himself from the Aryan Brotherhood and changes his outlook on life. He spends the remainder of his time in prison alone, reading books that Sweeney sends him. He fears that the prisons black inmates will attack him, but they leave him alone, thanks to Lamonts persuasion. Finally realizing the error of his ways, Derek leaves prison a changed man.
 white power posters from their bedroom walls.

The following morning, Danny finishes his paper and Derek gets ready for a meeting with his parole officer. Derek walks Danny to school before his meeting, and on their way they stop at a diner where they are met by Sweeney and a police officer. They tell Derek that Cameron and Seth were attacked the previous night and have been hospitalized.

At school, Danny is confronted by a young black student named Little Henry, with whom he had a confrontation the previous day. Little Henry pulls out a gun and shoots Danny in the chest, killing him. When Derek arrives at the school, he runs into the bathroom and tearfully cradles his dead brother in his arms.

The film ends with a voice over of Danny reading the final lines of his paper for Dr. Sweeney. Stating "Hate is baggage. Lifes too short to be pissed off all the time. Its just not worth it." and then quoting the final stanza of Abraham Lincolns first inaugural address.

==Cast==
 
* Edward Norton as Derek Vinyard
* Edward Furlong as Danny Vinyard
* Beverly DAngelo as Doris Vinyard
* Jennifer Lien as Davina Vinyard
* Ethan Suplee as Seth Ryan
* Fairuza Balk as Stacey
* Avery Brooks as Dr. Bob Sweeney
* Elliott Gould as Murray
* Stacy Keach as Cameron Alexander
* William Russ as Dennis Vinyard
* Guy Torry as Lamont 
* Joseph Cortese as Rasmussen
* Alex Sol as Mitch McKormick
* Keram Malicki-Sánchez as Chris
* Giuseppe Andrews as Jason
* Christopher Masterson as Daryl Dawson
* Paul Le Mat as McMahon
* Tara Blanchard as Ally Vinyard
* Nigel Miguel as Basketball Player 
 

==Soundtrack==
{{tracklist
| all_music        = Anne Dudley
| title1           = American History X
| note1            = 
| length1          = 4:46
| title2           = The Assignment
| note2            = 
| length2          = 2:36
| title3           = Venice Beach
| note3            = 
| length3          = 1:28
| title4           = Playing to Win
| note4            = 
| length4          = 3:49
| title5           = People Look at Me and See My Brother
| note5            = 
| length5          = 1:41
| title6           = If I Had Testified
| note6            = 
| length6          = 4:05
| title7           = A Stranger at My Table
| note7            = 
| length7          = 3:31
| title8           = Putting Up a Flag
| note8            = 
| length8          = 2:06
| title9           = Raiders
| note9            = 
| length9          = 3:02
| title10          = Complications
| note10           = 
| length10         = 1:38
| title11          = Starting to Remind Me of You
| note11           = 
| length11         = 1:43
| title12          = The Right Questions
| note12           =
| length12         = 3:24
| title13          = The Parth to Redemption
| note13           =
| length13         = 2:56
| title14          = We Are Not Enemies
| note14           =
| length14         = 2:05
| title15          = Two Brothers
| note15           =
| length15         = 2:31
| title16          = Storm Clouds Gathering
| note16           =
| length16         = 2:04
| title17          = Benedictus
| note17           =
| length17         = 3:35
}}

==Production== Tony Kaye made a second heavily shortened cut, which New Line rejected as it bore little resemblance to the first. Film editor Jerry Greenberg was brought in to cut a third version with Edward Norton.

Kaye disowned the third version as the final cut of the film, as he did not approve of its quality.    He tried and failed to have his name removed from the film credits|credits,     openly telling some interviewers he tried to invoke the Alan Smithee pseudonym which the Directors Guild of America used to reserve for such cases.  When his request was denied, Kaye tried "Humpty Dumpty" as an alternative name.

Joaquin Phoenix was offered the role of Derek Vinyard but turned it down. 

==Reception==

===Box office===
American History X was released on October 30, 1998 and grossed $156,076 in seventeen theaters during its opening weekend. The film went on to gross $6,719,864 from 513 theaters in the United States, and a total of $23,875,127 worldwide. 

===Critical reception=== Best Actor]]
The film received critical acclaim upon release with many critics directing particular praise towards Edward Nortons performance. Based on the reviews of 82 critics collected on  , the film holds a 62/100 average rating based on 32 reviews of top mainstream critics, indicating "generally favorable reviews". 

Gene Siskel of the Chicago Tribune, awarding American History X four stars out of four, described it as "a shockingly powerful screed against racism that also manages to be so well performed and directed that it is entertaining as well" and stated that it was "also effective at demonstrating how hate is taught from one generation to another." Siskel singled out Nortons performance and called him "the immediate front-runner" for an Academy Awards|Oscar.  Todd McCarthy, writing for Variety (magazine)|Variety, gave the film a positive review stating, "This jolting, superbly acted film will draw serious-minded upscale viewers interested in cutting-edge fare." He gave special praise to Nortons performance, saying "His Derek mesmerizes even as he repels, and the actor fully exposes the human being behind the tough poses and attitudinizing."  The New York Times s Janet Maslin wrote, "Though its story elements are all too easily reduced to a simple outline, American History X has enough fiery acting and provocative bombast to make its impact felt. For one thing, its willingness to take on ugly political realities gives it a substantial raison dêtre. For another, it has been directed with a mixture of handsome photo-realism and visceral punch."  Roger Ebert of the Chicago Sun-Times gave the film three stars out of four, stating that it was "always interesting and sometimes compelling, and it contains more actual provocative thought than any American film on race since Do the Right Thing." He was critical though of the films underdeveloped areas, stating that "the movie never convincingly charts Dereks path to race hatred" and noting that "in trying to resolve the events of four years in one day, it leaves its shortcuts showing." Nevertheless, Ebert concluded, "This is a good and powerful film. If I am dissatisfied, it is because it contains the promise of being more than it is." 

 , writing for The Washington Post was highly critical of the film and gave it a negative review. He called it "a mousy little nothing of a picture, an old melodramatic formula hidden under pretentious TV-commercial-slick photography, postmodernist narrative stylings and violations of various laws of probability." 

===Accolades===
Norton was nominated for an Academy Award for Best Actor for his performance as Derek Vinyard, but lost to Roberto Benigni for Life is Beautiful. In 2006, the film was nominated for AFIs 100 Years...100 Cheers. 

Nortons performance was ranked by Total Film as the 72nd greatest film performance of all time.  Nortons Academy Award loss was also included on Empire (magazine)|Empire list of "22 Incredibly Shocking Oscars Injustices". {{cite web|last=De Semlyen |first=Phil |title= 	 Empire |date=February 27, 2014 |accessdate=August 11, 2014 }} 

{| class="wikitable"
! Award !! Category !! Recipient(s) and nominee(s) !! Result !! Ref.
|- Academy Awards Best Actor
| Edward Norton
|  
| style="text-align:center;"|    
|- Chicago Film Critics Association Awards Best Actor
| Edward Norton
|  
| style="text-align:center;"|    
|-
| Golden Reel Awards
| Best Sound Editing: Music Score in a Feature Film  Richard Ford
|  
| style="text-align:center;"|    
|- Golden Satellite Awards Best Original Screenplay David McKenna
|  
| rowspan="3" style="text-align:center;"|    
|- Best Actor – Motion Picture Drama
| Edward Norton
|  
|- Best Supporting Actress – Motion Picture Drama
| Beverly DAngelo
|  
|- Online Film Critics Society Awards Best Actor
| Edward Norton
|  
| style="text-align:center;"|    
|-
| Political Film Society Awards Peace
| style="text-align:center;"| —
|  
| style="text-align:center;"|    
|- Saturn Awards Best Actor
| Edward Norton
|  
| style="text-align:center;"|    
|- Southeastern Film Critics Association Awards Best Actor
| Edward Norton
|  
| style="text-align:center;"|    
|-
| Taormina International Film Festival
| Best Actor
| Edward Norton
|  
| style="text-align:center;"|    
|- Youth in Film Awards
| Best Supporting Young Actor in a Feature Film
| Edward Furlong
|  
| style="text-align:center;"|    
|}

==References==
 

==Further reading==
* 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 