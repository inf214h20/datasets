Love's Option
 
 
{{Infobox film
| name           = Loves Option
| image          =
| caption        = George Pearson 
| producer       = 
| writer         = Douglas Newton (novel)
| starring       = Dorothy Boyd  Patrick Aherne   James Carew   Henry Vibart
| music          =
| cinematography =  Bernard Knowles
| editing        = Thorold Dickinson
| studio         = Welsh-Pearson-Elder
| distributor    = Paramount British Pictures
| released       = 25 September 1928
| runtime        = 66 minutes 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent adventure George Pearson yearly quota set down by the British government. The film follows several rivals attempting to gain control of a valuable Spanish copper mine. It was known by the alternative title A Girl of Today.

==Cast==
* Dorothy Boyd as Dorothy 
* Patrick Aherne as John Dacre
* James Carew as Simon Wake 
* Henry Vibart as Lucien Wake 
* Scotch Kelly as Pat Kelly 
* Philip Hewland as Tom Bartlett 
* Cecil Barry

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B film. British Film Institute, 2007.
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 