Ninja Hattori-kun
 
{{Infobox animanga/Header
| name            = Ninja Hattori-kun
| image           =  
| caption         = Hattori-kun
| ja_kanji        = 忍者ハットリくん
| ja_romaji       = 
| genre           = Adventure (genre)|Adventure, comedy, martial arts

}}
{{Infobox animanga/Print
| type            = manga
| author          = Fujiko Fujio
| publisher       = Shogakukan Children
| magazine        = CoroCoro Comic
| first           = 1964
| last            = 1971
| volumes         = 16
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = drama
| title           = Ninja Hattori-kun Ninja Hattori-kun + Ninja Monster Jippou
| director        = Shoichi Shimazu
| writer          = Hisashi Inoue
| music           = 
| studio          = Hiroki Ogawa (1st season)  Akira Yuyama (2nd season)
| network         = TV Asahi
| first           = 1966
| last            = 1968
| episodes        = 
| episode_list    = 
}}
{{Infobox animanga/Video
| type            = tv series
| director        = Fumio Ikeno, Hiroshi Sasagawa
| writer          = 
| music           = 
| studio          = Shin-Ei Animation
| network         = TV Asahi
| first           = September 28, 1981
| last            = December 25, 1987
| episodes        = 694
| episode_list    =
}}
{{Infobox animanga/Video
| type            = film
| title           = Ninja Hattori-kun NinxNin Furusato Daisakusen no Maki
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = Shin-Ei Animation
| released        = March 12, 1983
| runtime         = 53 minutes
}}
{{Infobox animanga/Video
| type            = live film
| title           = NIN×NIN Ninja Hattori-Kun The Movie
| director        = Masayuki Suzuki
| producer        = Kazutoshi Wadakura Ko Wada Madoka Takiyama Ryoichi Fukuyama Toru Miyazawa Motoo Abiko Magii
| music           = Takayuki Hattori
| studio          = Fuji TV
| released        = August 28, 2004
| runtime         = 100 minutes
}}
{{Infobox animanga/Game Ninja Hattori-kun
| developer       = Hudson Soft
| publisher       = 
| genre           = 
| ratings         = 
| platforms       = NES PlayStation 2
| released        = March 5, 1986 
}}
{{Infobox animanga/Video
| type            = TV series
| title           = Ninja Hattori-Kun Returns
| director        = Tetsuo Yasumi
| producer        = Takahiro Kishimoto Satoshi Kaishō Kei Mizutani Motomichi Araki
| writer          = Tetsuo Yasumi
| music           = Rahul Bhatt
| studio          = Shin-Ei Animation Reliance MediaWorks Cartoon Network Korea NET.
| first           = May 13, 2013 (Japan)
| last            = 
| episodes        = 52
| episode_list    =
}}
 
 Motoo Abiko, later adapted into a television drama, an anime series, a video game and a live-action movie. It was remade as a 2013 anime series as a joint venture between India and Japan and is currently on the air in several Asian countries. Spacetoon (NET.) in Television of Indonesia

== Plot ==
11-year-old Kenichi Mitsuba is an average kid who goes to secondary  Koga Ninja and his ninja cat kagekiyo always trouble Kenichi. Kenichi asks Hattori to take revenge. Although Hattori is a good friend, Kenichi sometimes fights with Hattori due to misunderstandings created by Kemumaki. Sometimes Jippou, Togejirou and Tsubame helps him.

== Characters ==
*  (voice:  ), aka Hattori-kun, is the protagonist of the series, a little ninja. In the Indian (Tamil, Telugu, Hindi and English) versions, Hattori is his first name and his last name (along with Shinzus) changes between episodes, an inconsistency. He is 11 years old and 40&nbsp;kg and is 140&nbsp;centimeters  tall. He is the boy who usually contests Kemumaki when he performs mischief. Hattoris main weakness is that he is afraid of frogs and lizards; this often leads him into trouble and becomes helpless. He wears blue coloured ninja robes. He always explains what a good ninja does and what others should also do. His powers can be compared to a master ninja. He also has a strange habit of saying ~degozaru or nin nin after almost every sentence. His girlfriend seems to be Tsubame.

*  (voice: Yūko Mita, drama actor: Shigeki Nakajō), aka Shin-chan, is Kanzos younger brother. However, in the Indian (Tamil, Telegu, Hindi and English) versions, his gender is kept inconsistent, sometimes (correctly) referred to as "he" and sometimes incorrectly (namely the "Ambidexterity" episode) as she. A good point to note is that while Kanzo is referred to as "Hattori", Shinzo is usually called by his first name, where his surname is inconsistent in the Indian (Tamil, Telugu, Hindi and English) versions. He uses weapons made from wood. He is a ninja-in-training who learns to be a good ninja, like Kanzo. He wears red coloured ninja robes. Shinzo seems to have a good heart and sticks to being loyal to his brother and sometimes gets excited when others are excited. Some of the times, Shinzo tricks Hattori to giving him things. He is also known to cry very loudly, which makes people faint. He is also very powerful for his age. He helps Kanzo fight Kemumaki, with his real weapons and control his loud crying (Which immobilizes opponents from attacking) and bites the head of the opponents.
 Kenichi Ogata) is a ninja dog living with Kenichi, who came along with Kanzo Hattori and Shinzo Hattori. He is a casual behaving dog who at times becomes lazy and stubborn. Along with Shinzo,the two of them wind up doing mischief and creating trouble. He has an attack of turning in a fireball when provoked. He also has the ability to change form into any other animal. His fur color is yellow. His ninja mark on his forehead is very important for him as it is an identification of him as a ninja. He loves to eat all kinds of delicious treats, especially a fish sausage, or chocolate roll.

*  (voice:  ) is a 10-year-old boy who goes to middle school and is poor in his studies. He likes Yumako, however also Kemumaki is always after her. He also tricks Hattori to help him in any situation. He also does not follow his mothers instructions properly and often gets a severe scolding from his mother.Though he doesnt study well,he has a good habit of helping others. He is always the victim of Kemumakis mischievous plans. Ever since Kanzo befriended Kenichi, he keeps on addressing him as "Lord Kenichi".

*  (voice:  Fuyumi Shiraishi), aka Tsubame-ko, a kunoichi and classmate of Hattori-kun. She likes Hattori and always wishes to marry him. She has a dislike for Kemumaki and Kagekiyo. She wears pink coloured ninja robes. She seems to own a recorder (musical instrument)|recorder, a clarinet, a flute, a piccolo and a bassoon, five woodwind instruments. She is called as Sonam in the Hindi version.

*  (voice:  ) is the antagonist of Ninja Hattori-kun.  He is called as Amara in the Indian (Hindi) versions, but retains his name in the Indian English version. He, along with his cat Kagechiyo (Kemuzou), always are the ones who cause trouble. Strength-wise, he has competition with Hattori and Shinzo. He is 11 years old and is shorter than Kenichi but a lot more fitter and stronger than him. He competes with Kenichi for Yumeko-chan; however when he uses one of his ninja techniques in order to win, Hattori usually steps in to save the day. He wears green colored ninja robes. Only Kagechiyo, Kenichi and the Kanzo family know about his duel life that hes a ninja like Hattori while he joined Kenichis school as a normal boy in the middle of the series. His parents whereabouts are unknown, but his mother is noted several times in the series.
*  (voice: Eiko Yamada, is a talking animal-ninja of the Koga-ryu, Kagechiyo is the helping antagonist in the series. He is called Kiyyo or Keo in the Indian versions (Hindi, Tamil, Telugu and English). Usually Kemumaki gives a big task in his plans for Kagechiyo to carry out, which he often does not succeed in. This is because he doesnt get enough training from Kemumaki and is seen sleeping on the streets. He is noticed by Hattori several times. He is seen to have a rivalry with Shishimaru. His fur color is black and white. He normally hides in the Mitsuba house to hear Kenichi and his friends plans and afterwards informs Kemumaki about them, acting more like a communication device. Sometimes he hates Kemumaki for his strictness and imagines living a life of luxury as a normal cat in some episodes. He enjoys eating fish. He obtains an attack of static electricity on rubbing a shining metal plate against his back. If he uses this attack too much, then he feels weak. 
 bell peppers.

*Aiko-Maam (voice: Yōko Kawanami):One of Kenichis  teacher. Koike-sensei has a crush on her. The subject she teaches is  unknown.
*Hattori Jinchuu (voice: Tadao Futami): Hattori-kun and Shinzos father. He also appear in Perman, when he fights with Perman 1.
*Jippou (voice: Junpei Takiguchi, drama voice: Hiroko Maruyama): A giant turtle monster ninja.He and Hattori are ninja partners.
*Koike-Sir (voice:   and sometimes appear as a cameo in Doraemon where he eats ramen. He always scolds Kenichi similar to when Nobita is scolded by his teacher in Doraemon. He is caricatured after animator Shinichi Suzuki. He also appears in the cartoon "Biriken" and "Ultra B" as Michios father.
*Kentaru Mitsuba (voice: Yuzuru Fujimoto, drama actor: Teizō Muta): Kenichis father. He usually smokes and comes from his office late in the evening. He likes eating and golf a lot. His name is never mentioned in the Indian (Tamil, Telugu, Hindi and English) versions.
Though a rather stout man,in some of the episodes, his size seems to change, going from chubbier to slimmer.
*Amma (voice: Yuri Nishiwa, drama actor: Chiharu Kuri): The mother of Kenichi. She likes Tsubame and thinks that Kemumaki is a good boy. She also appears in an episode of Doraemon, in one of Doraemons dreams.
*Professor Shinobino (voice: Reizō Nomoto): A professor who lives in America and invented Togejirou.
*Cacto-Chan (voice: Hiroko Maruyama): A cactus having supernatural powers is sent by Professor Shinobino from America. Shisimaru does not like Togejirou and loves to compete with him. In the English and Hindi version, he is called Cactochan (Kak-toe-Chan).

==Locations==
There are five main Locations : Tokyo City, Shinto Temple, Ega Town, Ega Mountains, Koga Valley
really

== Media ==

=== Manga ===

=== Anime === Nikkei announced on its website that a remake of the anime series under production by Shin-Ei Animation and Indian production company Reliance MediaWorks.  The announcement was part of a move to produce several remakes of popular anime television series to be broadcast across television stations in the Asian market to counteract Japans stagnating domestic anime marketplace due to its declining birthrate.  The new series began airing in India and Indonesia on May of the same year, as well as in China (unknown month). They are currently not available in English, though dubs have been advertised on TV and feature Indian characters (though minor) such as Anand, who teaches Hattori and his friends cricket.

=== Live-action drama ===

=== Live-action film ===

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 