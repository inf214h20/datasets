The Empty Beach
 
{{Infobox film
| name           = The Empty Beach
| image          = 
| image size     =
| caption        = 
| director       = Chris Thomson
| producer       = John Edwards Tim Read
| writer         = Keith Dewhurst
| based on       = novel by Peter Corris
| narrator       = John Wood
| music          = Martin Armiger Red Symons
| cinematography = John Seale
| editor         = Lindsay Frazer
| studio         = Jethro Films Pty Ltd
| distributor    = Hoyts 
| released       = 12 September 1985 (Australia)
| runtime        = 89 minutes
| country        = Australia English
| budget         = A$1.8 million 
| gross = AU $34,341 (Australia) 
| preceded by    =
| followed by    =
}}
The Empty Beach is a 1985 Australian thriller film starring Bryan Brown as private investigator Cliff Hardy inquires into the disappearance of a beautiful womans wealthy husband from Bondi Beach. Based on the 1983 novel by Peter Corris of the same name.

==Cast==
*Bryan Brown as Cliff Hardy
*Anna Maria Monticelli as Anne Winter
*Belinda Giblin as Marion Singer
*Ray Barrett as McLean John Wood as Parker
*Peter Collingwood as Ward
*Nick Tate as Henneberry
*Kerry Mack as Hildegard
*Joss McWilliam as Tal
*Sally Cooper as Sandy Modesto

==Production==
In the early 1980s Bryan Brown was attached to star as Cliff Hardy in an adaptation of an earlier Corris novel, White Meat. This was to be adapted by Corris, directed by Stephen Wallace and produced by Richard Mason. However no film eventuated. Then John Edwards and Tim Read acquired the rights to the series and approached Brown again. Dorre Koeser, "Life of Bryan", Cinema Papers, September 1985 p16-19 

Corris wrote the first few drafts and was unhappy with the final adaptation, although he liked Bryan Browns performance. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p232-234 

==Release==
The film performed poorly at the box office in Australia although it sold widely around the world.  There were plans for Brown to appear in further Hardy adventures but this did not eventuate. 

==References==
 

==External links==
*  at IMDB

 
 
 
 
 

 