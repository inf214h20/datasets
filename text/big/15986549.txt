Solovey iz sela Marshintsy
 
{{Infobox film name = Solovey iz sela Marshintsy image = caption = director = Sofia Rotaru producer = Sofia Rotaru writer = starring = Sofia Rotaru music = V. Matetskiy distributor = Ukrtelefilm released =   January 1, 1966 runtime = 12 min. country = Soviet Union budget = gross = language = Ukrainian (predominately) Russian
}}

Solovey iz sela Marshintsy ( ) is a 1966 short film. It was the first studio filmed movie starring Sofia Rotaru.

Rotaru was 19 when she starred in the film as a performer of Ukrainian and Moldavian folk songs. The setting is the premises of the Chernivsti Philharmonic Society. The name of the film was ultimately adopted as Rotarus nickname in Soviet Union media reports of her career in the 1970s.

The singer appears in the titles with her Ukrainified (first version) last name - Rotar (Rotaru in Moldavian).

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 
 