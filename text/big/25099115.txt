Pig Business
 
{{Infobox film name        = Pig Business image       = EndCard1_(1).JPG caption     = director  Tracy Worcester producer    = Tracy Worcester, Alastair Kenneil writer      = Tracy Worcester starring    = Bobby Kennedy Jr, Tracy Worcester
| released =   runtime     = 73 minutes country     = United Kingdom language    = English budget      = £140,000
}} Tracy Worcester, a former actress and now an environmental campaigner. It is a feature documentary exposing the huge hidden costs behind the pork and processed meat products on our supermarket shelves, and shows viewers and consumers how they can use their buying power to help create a more compassionate world. 

==Plot summary==
Beginning with an introduction from Tracy Worcester, the film highlights the practices of factory farms with a particular focus on Poland.

The film shows footage of pregnant pigs in steel crates too small to even turn around and reveals how pig waste pollutes the air and drinking water and makes beautiful lakes unsafe to swim in.
 MRSA and E. coli entering the food chain.

The film focuses on the expansion of the multi-national Smithfield Foods of America, based in Virginia which has been the target of impassioned protests by prominent ecologists, from Bobby Kennedy Jr  to The Ecologist s Zac Goldsmith. Initially focusing on the United States, Worcester explains how large corporations such as Smithfield Foods – an organisation that processes 27 million pigs in 15 countries, producing sales of $12 billion every year – have effective control over the whole market, providing cheap meat to supermarkets which in turn sends small, independent farms out of business. 

The film documents the pig industrys record of human and environmental disasters in America and its expansion into the EU market by moving to Poland. When the excreta of 10,000 pigs (the equivalent of 100,000 humans daily waste) is stored in massive anaerobic lagoons (cesspools) and sprayed over surrounding fields, local residents report unusually frequent headaches, eye irritation, excessive coughing, nausea and asthma. At one moment Worcester gamely climbed over a barbed wire fence where she filmed thousands of pigs on bare slatted floors before being chased away by the pig attendant.

More shocking footage included in the film shot by Compassion in World Farming shows dead piglets floating in a lagoon and dead pigs amongst the living inside a factory farm.

The film incorporates interviews with medical experts, politicians, industry heads, farmers and villagers from both sides of the debate including a centrepiece interview with Smithfield Foods vice-president of environmental and corporate affairs. It also draws heavily on Bobby Kennedy Jr., nephew of the late US President John F. Kennedy, an environmental lawyer who has led successful legal actions against factory farms. 

One scientist identifies studies which show that a significant percentage of workers in these super-farms can develop chronic respiratory illness. Because of the EUs agriculture policy, thousands of Polish farmers who have helped to keep their landscape an "unspoilt jewel of Central Europe", are being driven out of business and into unemployment and migration. The advance of factory farms in Poland has been helped by huge multi-million dollar loans from the European Bank for Reconstruction and Development - a bank guaranteed and subsidised by the taxpayer.

The film also points out that supermarkets choose to bulk-buy from the super-farms and inadequate labelling often makes it unclear where and how the pork has been produced. UK independent pig farmers claim they are being ruined because they cannot compete with the low cost of imported pork produced to lower standards and with cheaper labour in Europe.

In closing, the documentary describes solutions to the issues it highlights. It advocates buying British to ensure the sows are not kept in crates for their entire pregnancy and ideally buying organic, free range or outdoor reared pork from farmers markets and high street butchers to support independent farmers.

==Controversy==
The film which took five years to make was initially refused for broadcasting by Channel 4 for fear of being sued by Smithfield.

In February 2009 Channel 4 postponed broadcasting the film. Parts of the documentary were then subsequently remade to ensure it was "libel proof". 

The controversy was largely because Pig Business criticises the practices of the worlds largest pork processor, Smithfield Foods of America, claiming it is responsible for environmental pollution and health problems among residents near its factories.  Smithfields lawyers wrote a letter saying that the film was defamatory and included untrue claims. The company denies claims that pigs are mistreated. 

The film in altered form then made its UK debut at the Barbican on 20 May 2009, with Zac Goldsmith on hand to lend support. This showing only went ahead when the filmmaker, Tracy Worcester, signed an indemnity taking personal responsibility for its content. The film was also shown at the 2009 Hay Festival where Bobby Kennedy Jr was present and criticised Smithfields attempts to stop the film being shown on UK television and public screenings. 

In the US, insurers are not willing to underwrite the film unless Lady Worcester pays the first $300,000 of damages and, though Worcester was warned by lawyers not to put it on YouTube, it is now published there. Smithfield Foods has not yet taken legal action.
 La Gloria, Mexico, where as many as 1,800 villagers living near the plant had already complained of respiratory problems and 400 had been treated before the 2009 swine flu outbreak.  

==See also==
* Intensive pig farming
* Compassion in World Farming
* Soil Association
* Swine influenza

==References==
 

==External links==
*  

 
 
 
 