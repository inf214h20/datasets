Never Again (2001 film)
{{Infobox film
| name           = Never Again
| image          = Never Again poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Eric Schaeffer
| producer       = Robert Kravitz Terence Michael Eric Schaeffer Dawn Wolfrom 
| writer         = Eric Schaeffer
| starring       = Jeffrey Tambor Jill Clayburgh Caroline Aaron Bill Duke Sandy Duncan Michael McKean
| music          = Amanda Kravat 	
| cinematography = Thomas Ostrowski 	
| editing        = Mitchel Stanley 
| studio         = Five Minutes Before the Miracle Michael/Finney Productions
| distributor    = USA Films
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Never Again is a 2001 American comedy film written and directed by Eric Schaeffer. The film stars Jeffrey Tambor, Jill Clayburgh, Caroline Aaron, Bill Duke, Sandy Duncan and Michael McKean. The film was released on July 12, 2002, by USA Films. 

==Plot==
 

== Cast ==
*Jeffrey Tambor as Christopher
*Jill Clayburgh as Grace
*Caroline Aaron as Elaine
*Bill Duke as Earl
*Sandy Duncan as Natasha
*Michael McKean as Alex The Transvestite
*Danl Linehan as Leather Go-Go Boy
*Bill Weeden as Mr. Speedy
*Eric Axen as Male College Go-Go Boy David Bailey as Chad
*Trazana Beverley as Night Nurse 
*Tom Cappadona as Waiter
*Caitlin Clarke as Allison
*India Cooper as Nurse
*Peter Dinklage as Harry Appleton
*Jenny Kravat as Big Sister
*Kasia Ostlun as College Girl
*Douglas Ladnier as Craig
*Meredith Lauren as Waitress / Body Double
*Rachel Jéan Marteen as Carrie 
*Melissa Maxwell as Doctor
*Dolores McDougal as Mrs. Feinstein
*Abigail Morgan as Girl #1
*Ebon Moss-Bachrach as Andy
*Jimmy Noonan as Muscular Waiter 
*Peter Reardon as Runner
*Manuel E. Santiago as Doorman 
*Charlie Schroeder as Waiter Eric Scott as Arthur
*Suzanne Shepherd as Mother
*Edward Steele as Handicapped Man
*Victor Truro as Sex Shop Clerk
*Anne Leighton as Girl #2
*Lily Rabe as Tess

==Reception==
Never Again received negative reviews from critics. On Rotten Tomatoes, the film has a rating of 31%, based on 61 reviews, with a rating of 4.4/10. The sites critical consensus reads, "The performances are excellent, but much of the story rings false." 

== References ==
 

== External links ==
*  

 
 
 
 
 

 