Jyothi (1981 film)
{{Infobox film
| name = Jyothi
| image = Jyothifilmhindi.png
| caption = Promotional Poster
| director = Pramod Chakravorty 
| writerr = Sachin Bhowmick
| starring = Ashok Kumar Jeetendra Hema Malini Vijayendra Ghatge Sasikala.T Deven Verma Padma Khanna Sulochana
| producer = 
| music = Bappi Lahiri
| editor =
| released =  
| runtime = Hindi
| budget =
}}
 1981 Bollywood|Hindi-language directed by Pramod Chakravorty, starring Jeetendra and Hema Malini in lead roles. 

==Cast==

*Ashok Kumar
*Jeetendra
*Hema Malini
*Vijayendra Ghatge
*Sasikala.T
*Deven Verma
*Padma Khanna
*Sulochana

==Other versions==

The story line has been inspiration for various movies and has had various remakes in Indian film industry.
 Tamil as Telugu as Ravichandran and Madhoo|Madhubala.

{| class="wikitable"
|- Year !! Title !! Language !! Director !! Cast
|-
!Step-mother !! Son !! Wife
|- Kannada || Rajkumar || B Sarojadevi 
|- Hindi || Pramod Chakravorty || Shashikala || Jeetendra|| Hema Malini
|- Tamil || K. Bhagyaraj || C. R. Saraswathy ||K. Bhagyaraj ||  Rada
|- Hindi || Indra Kumar || Aruna Irani || Anil Kapoor || Madhuri Dixit
|- Telugu || Venkatesh ||Meena Meena
|- Kannada || D Rajendra Babu || Aruna Irani || V. Ravichandran || Madhoo 
|}

==References==
 

==External links==
*  

 
 
 
 
 