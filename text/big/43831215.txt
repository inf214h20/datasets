Pachai Vilakku
{{Infobox film
| name           = Pachai Vilakku
| image          = Pachchai Vilakku.jpg
| image_size     =
| caption        = Poster
| director       = A. Bhim Singh
| producer       = Rama. Arangannal A. R. Hassan Khan T. S. Aadhi Narayanan
| writer         = G. K. Suriyam Rama. Arangannal (dialogues) Ko. Iraimudi Mani (dialogues)
| screenplay     = A. Bhim Singh
| starring       = Sivaji Ganesan S. S. Rajendran C. R. Vijayakumari S. V. Ranga Rao
| music          = Viswanathan–Ramamoorthy
| cinematography = G. Vittal Rao
| editing        = A. Bhim Singh Paul Duraisingam Thirumalai
| studio         =  Vel Pictures
| distributor    =  Vel Pictures
| released       =  
| country        = India Tamil
}}
 1964 Cinema Indian Tamil Tamil film, directed by A. Bhim Singh and produced by Rama. Arangannal, A. R. Hassan Khan and T. S. Aadhi Narayanan. The film stars Sivaji Ganesan, C. R. Vijayakumari, S. S. Rajendran and S. V. Ranga Rao in lead roles. The film had musical score by Viswanathan–Ramamoorthy.    

==Cast==
*Sivaji Ganesan
*C. R. Vijayakumari
*S. S. Rajendran
*Sowkar Janaki
*M. R. Radha
*S. V. Ranga Rao
*Nagesh
*A. V. M. Rajan
*Chittor V. Nagaiah
*Sriram
*Pushpalatha

==Soundtrack==
The music was composed by Viswanathan Ramamurthy.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kanni Venduma || L. R. Eswari, PB. Srinivas || Kannadasan || 03.25 
|- 
| 2 || Kelvi Piranthadhu || T. M. Soundararajan || Kannadasan || 05.03 
|- 
| 3 || Olimayamana || T. M. Soundararajan || Kannadasan || 04.24 
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 