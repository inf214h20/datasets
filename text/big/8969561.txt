Ekskul
 
{{Infobox film
| name     = Ekskul
| director       = Nayato Fio Nuala
| producer       = Indika Entertainment
| image          = Ekskul.jpg
| writer         = Eka D. Sitorus
| starring       = Ramon Y Tungka Metha Yunatria Sheila Marcia Indra Brasco Teguh Leo Samuel Z Heckenbucker Gabriel Martianie Mira Hera Waty Julivan Persada Inong Pipip Sasati Tizza Radia Boogie Samudra Erly Ashy
| music          =
| cinematography =
| editing        =
| distributor    =
| release_date   = May 18, 2006
| runtime        = 109 minutes
| country        = Indonesia
| awards         = Indonesian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 thriller film bullied at abused by parents at home, had enough and took several other students hostage.

==Reception== editing and Indonesian Film Festivals Citra Award for Best Picture, which prompted waves of protests from the Indonesian film community, particularly the younger generations. Several winners of previous Citra awards even returned their trophy to the IFF committee in protest.

==Plot==
The pre-title scene shows Joshua (Ramon), the main character, in a psychiatrists office. Joshua is clearly refusing to cooperate with the shrink, who seems to favor repressive in place of persuasive approach. The brief dialogue in this scene is mainly filled with rather cheesy joke exchanges about transsexual brothers. The title screen is then shown.

In the next scene, a huge number of police officers, including SWAT teams and snipers, is deploying around Joshuas school. Joshua has got a gun and has barricaded himself in the school counselors office along with six students as hostages.

Most of the film consist of flashbacks, depicting the events leading to the current situation. Some of these include Joshua being beaten by the school gang and his own father, being hanged by his collar from the school gate, and having his face shoved down the toilet.

The flashbacks also show his brief relationship with Cathy (Metha Yunatria), a popular student, and Sabina (Sheila Marcia), a beautiful but introverted girl who sympathized with him.

The police attempts to negotiate, aided by Mrs. Miranda, the school counselor, and Joshuas parents. During negotiations, Joshua tells about his frustration to everyone listening, including the press which is broadcasting the crisis nationally.

Eventually he releases the female hostages, which includes Cathy, but keeps the males which consist of the gang members which have tormented him for so long. Announcing to them "Its judgement time," he returns what they did to him. He makes them shove their leaders head into a toilet at gunpoint, and after demanding all the school students to come and watch, he hangs Jerry from the school roof.

Again, Mrs. Miranda pleads Joshua to let the hostages go. Joshua refuses, claiming they deserve to die. However, the police have arrested the man who sold Joshua his gun, and he told them that Joshua only has one bullet. Mrs. Miranda uses this information to force Joshua into surrendering, since he wont be able to shoot all three hostages. However, that is not really his plan all along. As a squad of police officers move up to the roof to apprehend him, he points the gun to his head and kills himself.

==External links==
* 
* 

 

 
 
 
 
 
 
 