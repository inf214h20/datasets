Tol'able David (1930 film)
{{infobox_film
| name           = Tolable David
| image          =File:Tolable David 1930.jpg
| imagesize      =
| caption        =Scene from the film.
| director       = John G. Blystone
| producer       = Harry Cohn
| writer         = Benjamin Glazer (screenplay)
| based on       =   Richard Cromwell Noah Beery
| cinematography = Ted Tetzlaff
| editing        = Glen Wheeler
| distributor    = Columbia Pictures
| released       = November 15, 1930
| runtime        = 65 minutes
| country        = United States
| language       = English

}} 1930 American Richard Cromwell in the Barthelmess part after he won an audition over thousands of hopefuls and Harry Cohn gave him his screen name and a $75/week contract. It is preserved in the Library of Congress.   

==Cast== Richard Cromwell - David Kinemon Noah Beery - Luke Hatburn
*Joan Peers - Esther Hatburn
*Henry B. Walthall - Amos Hatburn Tom Keene - Alan Kinemon
*Edmund Breese - Hunter Kinemon Barbara Bedford - Rose Kinemon
*Helen Ware - Mrs. Kinemon
*Harlan Knight - Iska Hatburn
*John Carradine - Buzzard Hatburn (*billed as Peter Richmond)
*James Bradbury, Sr. - Galt Richard Carlyle - Doctor (*the correct  )

==References==
 

==External links==
 
*  
* 

 
 
 
 
 
 
 

 