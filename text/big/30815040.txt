Against the Law (1997 film)
{{Infobox film
| name           = Against the Law
| image          = AgainsttheLawNancyAllenGrieco.jpg
| caption        = Promotional movie poster
| director       = Jim Wynorski
| producer       = Benjamin R. Reder Betsy Chasse
| writer         = Steve Mitchell Bob Sheridan Nancy Allen Nick Mancuso Jaime Pressly Gary Sandy Thomas Mikal Ford Heather Thomas
| music          = Kevin Kiner
| cinematography = Andrea V. Rossotto
| editing        = Richard Gentner
| distributor    = 
| released       = September 16, 1997
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Nancy Allen and Richard Grieco.

==Plot== Nancy Allen) after killing one of L.A.s most notorious drug dealers in a skilled shoot out. When Rex learns of this heroic deed, he becomes fixated on both Hewitt, who he begins to stalk and terrorize, and Shepard, who he vows to shoot and kill. As a tense high noon battle looms on the horizon, John and Maggie scramble to outwit and outshoot this delusional maniac. 

==Cast==
(in credits order...) Nancy Allen ...  Maggie Hewitt 
*Richard Grieco ...  Rex 
*Nick Mancuso ...  Det. John Shepard 
*Steven Ford ...  Lt. Bill Carpenter 
*Thomas Mikal Ford ...  Det. Siegel 
*Gary Sandy ...  Chief Leitner 
*Leslie Bega ...  Lucia
*James Stephens ...  Det. Ben Hamada  Herb Mitchell ...  Carl Stensgard 
*Heather Thomas ...  Felicity 
*Jaime Pressly ...  Sally
*B.K. Byron ...  Lars Reder 
*Tim Colceri ...  Officer I.Q. 
*Randy Crowder ...  Lt. Fuller 
*Billy Gallo ...  DJ 
*Randy Hall ...  Security Guard 
*Frank Lloyd ...  Officer Caultman

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 