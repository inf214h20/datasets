Oru Yathrayil
{{Infobox film
| name           = Oru Yathrayil
| image          = 
| caption        = 
| director       = {{Plainlist|
* Rajesh Amanakkara
* Mathews 
* Priyanandanan
* Major Ravi
}}
| producer       = {{Plainlist|
* Siby Thottappuram
* Joby Mundamattam
* Mathews
}}
| writer         = 
| starring       = {{Plainlist|
* Kannan Pattambi
* Pooja
* Jayan Cherthala
* Lakshmi Gopalaswami
* Vineeth Kumar
* Remya Nambeesan Janardhanan
* Sukumari
}}
| music          = Anil Panachooran
| cinematography = {{Plainlist|
* Sanjeev Shankar
* Pradeep Prathapan
* Vel Raj
}}
| editing        = 
| distributor    = S. J. M. Entertainment
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Oru Yathrayil is a 2013 Indian Malayalam anthology film. The film comprises four shorts namely Honeymoon, I Love My Appa, Marichavarude Kadal and Amma which all are half an hour long each. The four featurettes are directed by Rajesh Amanakkara, Mathews, Priyanandanan and Major Ravi respectively. Major Ravi coordinated the efforts for the film.

Another featurette titled Sarvashiksha Abhiyan directed by Vinod Vijayan was originally announced but was not included in the final print. 

== Films ==
{| class="wikitable"
|-
! No.
! Title
! Director
! Writer
! Editor
! Cinematographer
! Actors
|-
| 1 || Honeymoon|| Rajesh Amanakara ||  || Hariharaputhran || Sanjeev Shankar || Kannan Pattambi, Pooja
|-
| 2 || I Love My Appa || Mathews || Major Ravi  || Kapil G. ||  Sanjeev Shankar || Jayan Cherthala, Lakshmi Gopalaswami, Master Vivas
|-
| 3 || Marichavarude Kadal || Priyanandanan || Priyanandanan  || Venugopal || Pradeep Prathapan || Vineeth Kumar, Remya Nambeesan, C. K. Babu
|-
| 4 || Amma || Major Ravi || Major Ravi || Donmax  || Velraj || Janardhanan (actor)|Janardhanan, Sukumari, Jayakrishnan, Manikandan Pattambi
|}

==Plots== Gandhian principles in the modern era. It flits between past and present, as the tale of an elderly couple wedded to Gandhism is narrated, in the backdrop of sea. The sea, here is a symbol of the watery end of the great ideals of Mahatma Gandhi. Amma is a tale of an elderly housewife who yearns for love and attention from her husband and grown up children.

==References==
 

== External links ==
*  
*   at the Malayalam Movie Database
*    

 
 
 
 


 