Whitechapel (film)
{{Infobox film
| name           = Whitechapel 
| image          = 
| image_size     = 
| caption        = 
| director       = Ewald André Dupont 
| producer       = Hanns Lippmann
| writer         = Max Jungk   Julius Urgiss
| narrator       = 
| starring       = Guido Herzfeld   Hans Mierendorff   Otto Gebühr   Hermann Wlach
| music          = 
| editing        = 
| cinematography = Karl Hasselmann
| studio         = Gloria-Film UFA 
| released       = 1920
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} silent crime film directed by Ewald André Dupont and starring Guido Herzfeld, Hans Mierendorff and Otto Gebühr. The film was set around a variety theatre in Londons East End suburb of Whitechapel. 

==Cast==
* Guido Herzfeld as Der alte Feibel 
* Hans Mierendorff as Fred Hopkins 
* Otto Gebühr as Baron Harry 
* Hermann Wlach as Jac Fritz Schulz as David 
* Lu Juergens as Mintje 
* Rudolf Lettinger as Komissar 
* Fritz Beckmann  
* Henry Bender as Tom 
* Carl Clewing as Lord 
* Leo Connard as Juwelier 
* Carl Geppert as Will 
* Grit Hegesa as Rahel 
* Adolf E. Licho as Brillantenhändler van Zuider 
* Julia Serda   
* Ferry Sikla as Juwelenhändler Blocker 
* Marga von Kierska as Cecil Readings Verlobte

==References==
 

==Bibliography==
* Bergfelder, Tim & Cargnelli, Christian. Destination London: German-speaking emigrés and British cinema, 1925-1950. Berghahn Books, 2008.
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945.University of California Press, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 