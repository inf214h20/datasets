Jai Jagannatha
{{Infobox film  name = Jai Jagannatha image = caption = Movie poster for Jai Jagannatha starring = Sarat Pujari Sritam Das Jyoti Misra Mohini Shilalipi director = Sabyasachi Mohapatra producer = B.Chintu Mohapatra  writer = Sabyasachi Mohapatra editing = Rajendra Mohapatra  music =  Late Akshaya Mohanty cinematography = Aum Prakash Mohapatra  distributor = Adlabs  released       = 12 July 2007   runtime        =   127 minutes country = India language = Oriya Hindi English Bengali Assamese, Chhattisgarhi Bhojpuri Rajasthani Punjabi Gujarati Marathi Telugu Tamil Malayalam Nepali budget = $ 1,400,000
}}
 Oriya and Malayalam and Nepali languages.   

==Synopsis==
 
The story of Jai Jagannatha is based on the ancient scripture Laxmi Puran. The untouchables were not allowed to pray, worship and do rituals to God in the ancient ages. Sriya, one of the important characters in this story, dares to pray and worship and wins over the support of Goddess Lakshmi. The real drama begins when Lakshmi is separated by Lord Jagannath at the behest of his brother Balram (Balabhadra) because she ends discrimination on earth by encouraging even untouchables to conduct rituals and worship.

As Lakshmi moves out of Jagannath’s household, Jagannath and Balram undergo immense suffering &mdash; so much so that they have to starve without water and food. The curse of Lakshmi had such a severe impact on the brothers that for 12 years they had a tough time. Soon they realised the importance of Lakshmi and were keen to bring her back to their abode. 
Lakshmi returned to Jagannath’s abode on one condition: There will be no discrimination of caste and creed on earth.

This unique story highlighted that for God everyone was equal besides it reflected the reforms and progressive stance of Gods from the ancient times. Only in the end through Narad it is revealed that Jagannath to end casteism and discrimination had “set up” these series of events which highlighted the social message besides the strength of true spirituality. 

==Cast==
*Sritam Das as God Jagannath
*Jyoti Misra  as Goddess Laxmi
*Pintu Nanda as God Balabhadra
*Mohini Shilalip as Sriya Chandali
*Sadhu Meher as Sriyas father-in-law
*Sarat Pujari
*Sehenawaj Khan
*Sila Lipi

==Crew==
*Director: Sabyasachi Mohapatra
*Presenter: Bharat Shah
*Producer: B.Chintu Mohapatra
*Writer: Sabyasachi Mohapatra
*Editor: Rajendra Mohapatra
*Music: Late Akshaya Mohanty
*Background Score: Amar Haldipur
*Cinematography :Aum Prakash Mohapatra
*Sound: P. K. Misra
*Publicity: Rahul Nanda & Himanshu Nanda
*Banner: Mahapatra Movie Magic Pvt. ltd.
*Distributor: Adlabs
*Filming Location: Puri, Odisha & Umargaon

==Awards==
*Odisha State Film Award: Best jury award & best editor award.  

==Review== Oriya director Sabyasachi Mohapatra is being released with a record number of 13 languages apart from  Hindi and Oriya language|Oriya. Jai Jagannatha is a multilingual socio-mythological feature film. State of the art graphics, rich production values and divine music are the highpoints of Jai Jagannatha. This socio-mythological film has six songs in all.  

==References==
 

==External links==
* 
* 
*  
*  
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 