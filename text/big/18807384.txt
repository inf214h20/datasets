Warriors of the Year 2072
 
{{Infobox film
| name           = The New Gladiators
| image          = I Guerrieri dellanno 2072.jpg
| image_size     =
| caption        = Italian theatrical poster
| director       = Lucio Fulci
| producer       =
| writer         = Elisa Briganti Dardano Sacchetti
| narrator       = Howard Ross
| music          = Riz Ortolani
| cinematography = Giuseppe Pinori
| editing        = Vincenzo Tomassi
| distributor    = Troma Entertainment
| released       = Italy: 28 January 1984
| runtime        = 90 minutes
| country        = Italy
| language       = Italian (English dubbed)
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Italian 1984 1984 film directed by Lucio Fulci based on a story by Elisa Briganti. The film is currently distributed on DVD under the title The New Gladiators.
 Corridori G & A Cinematografica S.r.l.

==Plot summary==
In the year 2072, set in a dystopia Rome, Italy, WBS TVs chief of programming, Cortez (Claudio Cassinelli), is fuming at the constituently high ratings enjoyed by a rival American TV show, Kill-Bike. The show has features gladiatorial fights to the death on motorcycles, and has made a hero of a man called Drake (Jared Martin) the unbeaten champion. Cortez is all the more bitter because it was he who discovered Drake in the first place. WBSs current answer to the American show is The Danger Game, a simulation game showing the hallucinations of a contestant experiencing the approach of violent death. Fear, panic, and screaming at the extremely realistic simulations triggers a further increase in the intensity. Unfortunately for the station, even this spectacle fails to compete against the American show.

Cortez and his two female assistants, Sybil (Penny Brown) and Sarah (Eleanor Gold) receive a video message from the mysterious station boss, Sam, demanding that they recreate the formula of gladiatorial contest and set them back to where they began: in the Coliseum. Sam orders the WBS station, a sort-of flying saucer controlled by a computer system called Junior, to fly over there and initiate training of the future contestants who will be chosen from the Death Rows around the world. Meanwhile, Drake has been imprisoned for the murder of three men who killed his wife Susan (Valerie Jones) after they broke into his house, and he (conveniently for WBS) is now under the sentence of death for taking the law in his own hands.

Drake is brought to the training compound and a detector strip is seared into his wrist. There, he has to contend with the sadistic Chief of the Praetorian Guard, Raven (Howard Ross), and the hostility of the other future contestants. Some of them are introduced; Abdul (Fred Williamson) is an African-American Muslim extremist; Akira (Haruiko Yamanouchi) is a notorious Japanese serial killer; Kirk (Al Cliver) is a German-born robber/killer; and Tango (Tony Sanders) is a Latin American terrorist. Drake also meets an old friend of his from his WBS days, a deformed employee with a fiber optic eye called Monk (Donald OBrien), who offers Drake some reassurance.

The next day, Drake is strapped into a hate stimulator device, designed to measure the point when man can be provoked into murder. Despite the best efforts of the machine, which creates holographic images of his wifes killers taunting him, Drake does not crack. Cortezs assistant, Sarah, is perturbed by this and becomes attracted to the prisoner. Drake soon earns the respect of the other prisoners after facing off in a battle of wits with the vicious Raven, who is impotent to actually harm the prisoners because of the upcoming TV show. Sarah meets and befriends Drake and shows him videotape evidence she has discovered that proves he was set up for his wives murder merely to get him on the show. A strangely powerful microprocessor smuggled in by Monk facilities an escape attempt. When it fails, footage of Drake and the other prisoners in their escape attempt turns up immediately on the TV news. Who is the mystery cameraman? Raven is given limited access to torture the prisoners with electric shocks. But the harrowing experience bonds the principle gladiators even more. Meanwhile, Cortez is becoming more enraged by Sarahs investigation of Drake.

The following evening, Sarah visits Professor Towman (Cosimo Cinieri), the inventor of Junior, the WBS computer system, looking for a way to gain access to the restricted files. The professor has become a mystic and talks about his inventions soul. Professor Towman gives Sarah a pass-chip to access the computer when he is suddenly murdered by an unseen assailant. Sarah gives chase and sees that the killer is Sybil. But before Sarah can try to capture Sybil, she too is shot and killed by an unseen assailant.

Meanwhile, the WBS Gladiator Contest commences, with each contesting racing around the modified arena in modified motorcycle carts fighting in a chariot-like run. Many contestants are killed, including Tango. Suddenly, Sarah interrupts the games by riding into the arena on a motorcycle. She has discovered from the computer chip that Junior will trigger the bracelet devices of all the surviving gladiators to kill them all 20 minutes after the show ends. Revealing this information to Drake and the rest of the men, they mount an attack on the control tower, killing all the guards, including Raven. But most of the gladiators, including Akira, are killed in the assault. Drake, Sarah, Abdul and Kirk break into the main control room and discover that Cortez is found to have plotted the death of the survivors in order to discredit Sam and take over as station head. Cortez is killed by Abdul who shoots him. Then, a computer screen image of the company boss, Sam, appears and informs them that Sam is really just a video projection of the computer, emanating from a space satellite orbiting 200,000 miles above the Earth. The computer knew of Cortez plan but let it happen to a point.

The four surviving rebels force entry into the main terminal of Sam, with the aid of Sarahs pass key. But Kirk is killed when he unwisely tries to remove his bracelet himself. Then, the three surviving rebels are attacked in the control room by Monk, who is revealed to be the traitor who filmed the escape attempt with a mini-camera built into his eye. After a bitter fight, Drake kills him and Sarah uses some of the information stored on his camera chip to access the destruction codes. In the nick of time, Sam is blown up, and the deadly bracelets are deactivated. With the battle won, Drake and Sarah fly off together in a hover-vehicle to start a new life for themselves.

==Cast==
* Jared Martin as Drake
* Fred Williamson as Abdul Howard Ross as Raven
* Eleonora Brigliadori as Sarah
* Cosimo Cinieri as Professor Towman
* Claudio Cassinelli as Cortez
* Valeria Cavalli as Susan Donald OBrien as Monk
* Penny Brown as Sybil
* Al Cliver as Kirk
* Mario Novelli as Tango
* Hal Yamanouchi as Akira
* Lucio Fulci

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 