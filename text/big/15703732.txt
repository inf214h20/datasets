The Scarlet Pimpernel (1982 film)
{{Infobox film
| name           = The Scarlet Pimpernel
| image          = The_Scarlet_Pimpernel_1982_film_dvd_cover.jpg
| alt            = 
| caption        = DVD cover
| director       = Clive Donner
| producer       = David Conroy
| screenplay     = William Bast
| based on       =  
| starring       = {{Plainlist|
* Anthony Andrews Jane Seymour
* Ian McKellen
}}
| music          = Nick Bicât
| cinematography = Denis Lewiston
| editing        = Peter Tanner
| studio         = London Films CBS
| released       =  
| runtime        = 142 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Baroness Emmuska Jane Seymour as Marguerite St. Just, the love interest, and Ian McKellen as Chauvelin, the antagonist.
 King of France. The story differs from the book but is largely inspired by it.

==Plot== wayside flower. Jane Seymour) through her brother, Armand (Malcolm Jamieson), whom he rescued from an attack. Percy is attracted to Marguerite, but she is in a relationship with Paul Chauvelin (Ian McKellen), an agent of Maximilien Robespierre. Due to the Scarlet Pimpernels past successes, Chauvelin is assigned to discover his identity and capture him.
 King of France. Soon after, the Scarlet Pimpernel and his associates rescue de Tournays family.

Following a passionate courtship, Percy marries Marguerite, but soon their happiness is interrupted when he discovers that she apparently signed the death warrant of the Marquis de St. Cyr and his family, the man responsible for the previous attack on Armand. Believing that she was seeking revenge and is still in league with Chauvelin, Percy becomes distrustful of his new wife. Unaware that he thinks she signed the death warrant, Marguerite unhappily notices his growing disdain for her. Armand advises Percy to tell Marguerite about his suspicions so that she may defend herself, but Percy refuses, even though he admits he will love her until the day he dies.

Soon after, Chauvelin discovers that Armand is in league with the Scarlet Pimpernel, and summons him back to Paris. Blackmailing Marguerite by threatening her brothers life, Chauvelin coerces her into discovering the vigilantes identity. After finding that the Scarlet Pimpernel is to rendezvous at midnight, Marguerite tells Chauvelin. However, she immediately warns the Scarlet Pimpernel—actually her husband, unbeknownst to her—and adds that Chauvelin betrayed her trust and faked her signature. Percys faith in his wife is restored. Having been thwarted from encountering them, Chauvelin angrily leaves for Paris. Percy and his associates also depart for France to save Armand and the Dauphin. Marguerite notices that Percys family crest bears a scarlet pimpernel, and quickly deduces his identity.

After Armand arranges the firing of the gaolers in charge of the Dauphins care, Percy and his associates use the removal of their belongings to smuggle the Dauphin out of the city. The boy is taken to a castle on the French coast, but Percy is soon captured while trying to save Armand. Marguerite visits her husband in prison, where he tells her to arrange for the Baron de Batz—an Austrian interested in saving the Dauphin—to smuggle the boy out of France the following night. Later, Percy agrees to personally bring Chauvelin to the Dauphin. Chauvelin and Percy, along with Marguerite and Armand who are hostages, arrive at the castle, but the Dauphin has already been removed.

Angered by the deception, Chauvelin orders Percys execution, but the firing squad consists of members of the league of the Scarlet Pimpernel, disguised as Chauvelins troops. Percy is rescued and returns to duel with Chauvelin, and is victorious. Percy decides to leave Chauvelins fate to Robespierre. Impersonating Chauvelin to ensure their escape, Armand departs from the castle along with the French troops that Chauvelin had stationed there. Percy and Marguerite sail away, happily in love.

==Cast==
* Anthony Andrews as Sir Percy Blakeney/ the Scarlet Pimpernel, an English aristocrat who plays a foppish dandy in public, but is actually a bold adventurer who carries out daring rescue missions along with his band of followers known as the League of the Scarlet Pimpernel Jane Seymour as Marguerite Blakeney (née St. Just), a French actress whom Percy Blakeney meets while in France and subsequently marries Paul Chauvelin, the chief agent of the Committee of Public Safety tasked with capturing the Scarlet Pimpernel
* James Villiers as the Baron de Batz, an Austrian nobleman involved in a covert operation to rescue the young Dauphin
* Malcolm Jamieson as Armand St. Just, the brother of Marguerite, and later a member of the League of the Scarlet Pimpernel
* Eleanor David as Louise, a French actress and understudy of Marguerite St. Justs, and is Armand St. Justs lover
* Richard Morant as Robespierre, a leader of the French Revolution, who commands Chauvelin to find the identity of the Scarlet Pimpernel
* Dominic Jephcott as Sir Andrew Ffoulkes, a member of the League of the Scarlet Pimpernel
* Christopher Villiers as Lord Antony Dewhurst, a member of the League of the Scarlet Pimpernel
* Denis Lill as the Count de Tournay, a French aristocrat and friend of Percy Blakeney
* Ann Firbank as the Countess de Tournay, the wife of Count de Tournay
* Tracey Childs as Suzanne de Tournay, the daughter of Count and Countess de Tournay, and later fiancée of Sir Andrew Ffoulkes Prince of Wales, the somewhat foppish heir to the British throne and friend of Sir Percy Blakeney
* Mark Drewry as Lord Timothy Hastings, a member of the League of the Scarlet Pimpernel
* John Quarmby as Ponceau, a Frenchman who works for the Committee of Public Safety
* David Gant as Fouquet, a Frenchman who works for Robespierre and the Committee of Public Safety
* Geoffrey Toone as the Marquis de St. Cyr, a French aristocrat who later dies on the guillotine
* Joanna Dickens as Aunt Lulu, the aunt of Louise
* Richard Charles as Louis XVII|Louis-Charles, Dauphin of France, the young heir to the French throne being held prisoner by Robespierre
* Gordon Gostelow as Duval, the caretaker of the young Dauphin in prison
* Carol MacReady as Madame Duval, the wife of Duval
* Daphne Anderson as Lady Grenville, an English noblewoman
* Nick Brimble as Bibot, a French soldier
* Tony Caunter as Pochard, a French soldier
* Timothy Carlton as the Count de Beaulieu, a French aristocrat rescued from the guillotine
* Kate Howard as the Countess de Beaulieu, the wife of Count de Beaulieu

==Production==
  (pictured in 1982) starred as Sir Percival Blakeney and his alter ego the Scarlet Pimpernel]] Dauphin from a French prison. 
 Brideshead Revisited, Jane Seymour. East of La Révolution française. 

==Reception==
===Critical response===
The New York Times staff writer John J. OConnor praised Anthony Andrews performance as both the heroic and foppish Percy Blakeney. Ian McKellens acting as Paul Chauvelin was also highlighted, as OConnor felt he was "marvelously subtle" in giving "an intricately etched portrait of social envy and sexual jealousy."    People (magazine)|People called it a "rousingly suspenseful melodramatization". 

In his 2006 work Stage Combat Resource Materials: A Selected And Annotated Bibliography, author J. Michael Kirkland referred to the sword fight between Percy and Chauvelin as "nicely staged, if somewhat repetitious&nbsp;... but still entertaining." Kirkland also observed that the weapons used were in fact German sabres, which were not used during the Napoleonic era. 

===Awards and nominations===
At the 35th Primetime Emmy Awards, The Scarlet Pimpernels costume designer Phyllis Dalton won the Emmy Award for Outstanding Costume Design for a Limited Series or a Special. The film also received nominations for Outstanding Drama Special (for producer David Conroy and executive producer Mark Shelmerdine) and Outstanding Art Direction for a Limited Series or a Special (for production designer Tony Curtis and set decorator Carolyn Scott). 

==Legacy==
Romantic novelist Lauren Willig has cited the 1982 film as "the most direct inspiration" for her 2005 work The Secret History of the Pink Carnation. 

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 