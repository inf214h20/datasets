The Two Colonels
{{Infobox film
 | name =  The Two Colonels 
 | image =  The Two Colonels.jpg
 | caption = Steno
 | writer =Bruno Corbucci  Giovanni Grimaldi
 | starring =  Totò Walter Pidgeon
 | music =  Gianni Ferrio 
 | cinematography =  Gino Santoni
 | editing =  Giuliana Attenni
 | producer = Gianni Buffardi
 | distributor = Titanus
 | released = Italy: 1962 USA: 1966 
 | runtime = 96 min
 | awards =
 | country = Italy
 | language =  Italian
 | budget =
 }} 1962 Cinema Italian comedy film directed by Steno (director)|Steno.   The character of Totò took inspiration from a similar character he played in Totò Diabolicus. 

== Plot ==
The story is of Italian and British troops facing off on the Greek - Albanian border in 1943.  Both sides take, lose, and retake a border village countless times during the entire movie.  The village is taken and retaken by both sides so many times that the locals dont even pay attention to the battles any more and openly collaborate with whichever side is occupying the village at that time.  Both sides use the same hotel as their HQ and a friendship and mutual respect even develops between the two opposing commanding officers.  This goes on until the Germans arrive and order the Italian commander to destroy the village killing its inhabitants, an order the Italian commander refuses to carry out bringing on a death sentence.  His men refuse the German officers order to fire and are also condemned to death.  The British recapture the village just in time to save the Italians, all rejoice at the news that Italy has just asked for an armistice.

== Cast ==
*Totò: Colonel Antonio Di Maggio
*Walter Pidgeon: Colonel Timothy Henderson
*Nino Taranto: Sgt. Quaglia
*Giorgio Bixio: Soldier Giobatta Parodi
*Toni Ucci: Mazzetta 
*Nino Terzo: Soldier La Padula 
*Scilla Gabel: Iride
* Gérard Herter: German General 

==References==
 

==External links==
*  
 

 
 
  
 
 
 
 