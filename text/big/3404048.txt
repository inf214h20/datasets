Letter to Jane
{{Infobox film
| name = Letter to Jane
| image =
| caption =Jane Fonda in a LExpress photograph, as used in Letter to Jane.
| director = Jean-Luc Godard Jean-Pierre Gorin
| producer =
| writer =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1972
| runtime = 52 Minutes
| language = French
| budget =
}}

Letter to Jane (1972) is a postscript film to Tout va bien directed by Jean-Luc Godard and Jean-Pierre Gorin and made under the auspices of the Dziga Vertov Group. Narrated in a back-and-forth style by both Godard and Gorin, the film serves as a 52-minute cinematic essay that deconstructs a single news photograph of Jane Fonda in Vietnam. This was Godard and Gorins final collaboration.

In 2005, the film was made available as an extra on the Tout va Bien DVD released by the Criterion Collection.

==External links==
* 
* 
* 

 

 
 
 
 
 


 