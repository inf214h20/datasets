Assassination Games
 
{{Infobox film
| name           = Assassination Games
| image          = Assassination Games.jpg
| caption        = Theatrical release poster
| director       = Ernie Barbarash
| producer       = {{plainlist|
* Justin Bursch
* Brad Krevoy
* Patrick Newall
}}
| writer         = Aaron Rahsaan Thomas
| starring       = {{plainlist|
* Jean-Claude Van Damme
* Scott Adkins
}}
| music          = Neal Acree
| cinematography = Phil Parmet
| editing        = Peter Devaney Flanagan
| studio         = {{plainlist|
* MediaPro Studios
* Rodin Entertainment
}}
| distributor    = {{plainlist|
* Samuel Goldwyn Films
* Sony Pictures Home Entertainment
}}
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $8 million  
}}
Assassination Games is a 2011 American action film directed by Ernie Barbarash, and starring Jean-Claude Van Damme and Scott Adkins. The film was released in the United States on July 29, 2011.

==Plot==
After a drug dealer puts his wife in a coma, assassin Flint retires.  When a contract is put out on the drug dealer, Flint comes out of retirement, only to find that another assassin, Brazil, is also on the job due to the money.  The two assassins reluctantly partner in order to combat corrupt Interpol agents and gangsters.

==Cast==
* Jean-Claude Van Damme as Vincent Brazil
* Scott Adkins as Roland Flint
* Ivan Kaye as Polo Yakur
* Valentin Teodosiu as Blanchard
* Alin Panc as Kovacs
* Kevin Chapman as Culley
* Serban Celea as Wilson Herrod
* Michael Higgs as Godfrey
* Kristopher Van Varenberg as Schell
* Marija Karan as October Bianca Van Varenberg as Anna Flint
* Andrew French as Nalbandian

==Production==
Assassination Games began development under the working title The Weapon with Russel Mulcahy attached to direct.  Initially Steven Seagal had signed on to star alongside Van Damme.   After Seagal dropped out of the role, Vinnie Jones was considered to replace Steven Seagal, though the role eventually went to Scott Adkins.   Shooting took place in Bucharest, Romania, and New Orleans, Louisiana. 

==Release==
The film had a limited release on July 29, 2011.   Sony Pictures Home Entertainment released the DVD  in the United States on September 6, 2011,    and in the United Kingdom on October 10, 2011. 

==Reception==
IGN gave the film a 6 out of 10 stars and wrote, "If you need a quick dose of action, Assassination Games should do the trick. Dont expect a masterpiece and you should walk away moderately pleased with the experience".   Rohit Rao of DVD Talk rated it 2 out of 5 stars and wrote, "Van Damme and Adkins show up to the party, game for anything, but director Ernie Barbarash insists on weighing them down with a convoluted script that mistakes meanness for grit."    Gabe Toro of Indiewire rated it C and wrote, "The fight choreography doesn’t lift this effort above Van Damme’s usual direct-to-DVD offerings, but it does prove that there are still filmmakers who understand how to shoot action."   Ike Oden of DVD Verdict described it as "a cheesy, pretentious, and poorly paced action film with a jet black nihilistic streak". 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 