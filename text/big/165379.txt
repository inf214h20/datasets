Wild River (film)
:See also:U.S. National Wild and Scenic Rivers. Not to be confused with The River Wild.
{{Infobox film
| name           = Wild River
| image          = Wild River.jpg
| image_size     =
| caption        =
| director       = Elia Kazan
| producer       = Elia Kazan
| writer         = Borden Deal (novel) William Bradford Huie (novel) Paul Osborn
| narrator       =
| starring       = Montgomery Clift Lee Remick
| music          = Kenyon Hopkins
| cinematography = Ellsworth Fredricks, ASC William Reynolds
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $1,595,000 
| gross          = $1,500,000 (US/ Canada) 
}}
Wild River is a 1960 film directed by Elia Kazan starring Montgomery Clift, Lee Remick, Jo Van Fleet, Albert Salmi and Jay C. Flippen filmed on location in the Tennessee Valley. It was adapted by Paul Osborn from two novels – Borden Deals Dunbars Cove and William Bradford Huies Mud on the Stars, drawing for plot from Deals story of a battle of wills between the nascent Tennessee Valley Authority and generations-old land owners, and from Huies study of a rural Southern matriarchal family for characters and their reaction to destruction of their land.

In 2002, Wild River was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
In the early 1930s, Chuck Glover (Montgomery Clift) arrives in Garthville, Tennessee, upstream from a newly constructed hydroelectric dam of the Tennessee Valley Authority, to head the TVAs land purchasing office after its previous supervisor abruptly quit. He has the responsibility for supervising the clearing of land to be flooded but must first acquire Garth Island on the Tennessee River, the last piece of property yet to be sold to the government. The previous supervisor was unable to convince the elderly Ella Garth (Jo Van Fleet), matriarch of a large family that has lived on the island for generations, to sell her land to the government, which to avoid bad publicity the TVA wants to acquire without using force. The clearing of the land for the coming lake is also proceeding behind schedule because the local mayor, the towns barber, uses only white labor. Chuck crosses the ferry to Garth Island but Ella and the other Garth women, including Ellas granddaughter Carol Baldwin (Lee Remick), refuse to listen to him. He tries to reason with Ellas three grown sons, Hamilton (Jay C. Flippen), Cal (James Westerfield), and Joe John, but being relocated means working for a living and they have never worked in their lives. Joe John tosses Chuck into the river. Hamilton comes to Chucks room soon after to invite him to the island for a formal apology and to speak with Ella.

Chuck arrives the next day to find Ella castigating President Franklin D. Roosevelt and his New Deal to her black farm hands and their families. To make her point to Chuck, she feigns attempting to compel Sam Johnson (Robert Earl Jones), a field hand, to sell her his beloved hunting dog against his will. Chuck tries to reason with Ella, passionately laying out the benefits the dam will bring, but Ella denounces all dams and the taming of the river as going "against nature."  When he asks Ella whats going to happen to her, she takes him to the family cemetery plot on the highest point of the island. There he learns from Carol that she is a widow with two small children who moved back to the island when her husband died three years before. She is also expected to marry Walter Clark (Frank Overton), a businessman in town, but Chuck urges her not to if she doesnt love him. She walks Chuck back to the ferry where the anxious field hands are also waiting. Chuck invites them to come to his office to discuss working for the TVA, reasoning that when they leave the island, Ella will have no choice except to sell. On an impulse, Carol invites Chuck to see her former home above the landing on the opposite shore. Her loneliness and attraction to Chuck combine to make her beg him to spend the night with her there. The next morning, saying goodbye, it is apparent to both that they are falling in love.

Back in town Chuck apprises the mayor, who is sympathetic, of his plan but is warned that hiring "colored labor" will cause the white workers to quit and bring out other elements to oppose him. When Chuck hires Ellas field hands, three prominent businessmen urge Chuck to create segregated work gangs and pay the black workers less than the whites, which Chuck refuses to do despite insinuations that "less responsible" opponents will use harsher tactics. Carol and Chuck are confused by their passion for each other but spend another night together again at her house, unaware that Walter Clark has seen them. The next morning Ellas workers and their families pack up and leave, and Ella remains alone except for Sam, who loyally refuses to go. Ella knows what Carol and Chuck have done and when Carol begs her grandmother to join her at her own house, orders her off the island.
 declared legally incompetent to permit them to sell the land themselves, which Chuck rejects. He reluctantly asks the United States Marshals Service|U.S. marshal to have Ella removed the next day, then goes to the island to use the machinations of her sons in a final attempt to persuade her to leave on her own. She calls her sons worthless and refuses to talk to him further.

Carol knows that Chuck will be moving on to a new assignment within a few days and pleads with him to take her with him, telling him shed be a "damned good wife" to him. Chuck is still trying to cope with his new feelings and doesnt know what to say. Walter rushes to Carols house to warn them that Bailey and his men are coming to terrorize them. While the local sheriff stands aside, telling his deputy, "Theyre just having some fun," the thugs shoot out a window with a rifle, overturn Chucks car, and drive Walters truck into the side of the house. Refusing to be run out of town, Chuck confronts Bailey but is knocked out with one punch. Carol leaps on Bailey, who hits her too, finally forcing the sheriff to run off the crowd. Chuck proposes to Carol and they are married that night by a justice of the peace. The next day, with Chuck and Carol present, Ella is read the eviction notice and leaves her island as her former workers fell the trees. At her new home, Ella sits on the porch, dispiritedly refusing to speak. Soon after, while Chuck is supervising the burning of the Garth house, Carol tells him that Ella just died. Before leaving the valley, Chuck and Carol join her family and former workers in burying Ella in the family plot, now the only part of Garth Island above water in the new lake.

==Production==
Exterior locations for Wild River were filmed on Coon Denton Island on the Hiwassee River, upriver from Charleston, Tennessee; in the towns old business district; and on a peninsula west of Cleveland, Tennessee, on Chickamauga Lake. A studio for interior shooting was also created in the Cleveland armory. 

Jo Van Fleet was only 45 during the making of the film, 16 years younger than Jay C. Flippen who played her son. Wild River was Bruce Derns first film.

==Cast==
*Montgomery Clift as Chuck Glover
*Lee Remick as Carol Garth Baldwin
*Jo Van Fleet as Ella Garth
*Albert Salmi as R.J. Bailey
*Jay C. Flippen as Hamilton Garth
*James Westerfield as Cal Garth
*Barbara Loden as Betty Jackson
*Frank Overton as Walter Clark
*Malcolm Atterbury as Sy Moore
*Bruce Dern as Jack Roper (uncredited)
*Robert Earl Jones as Sam Johnson (uncredited)

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 