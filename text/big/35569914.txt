The Witch's Curse
{{Infobox film
| name           = The Witchs Curse
| image          = The Witchs Curse.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = US film poster
| director       = Riccardo Freda
| producer       = Luigi Carpentieri Ermanno Donati
| writer         = Oreste Biancoli Ennio De Concini Piero Pierotti
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Kirk Morris Hélène Chanel
| music          = Carlo Franci
| cinematography = Riccardo Pallottini
| editing        = Ornella Micheli
| studio         = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}} 1962 Sword-and-sandal|peplum-fantasy film, directed by Riccardo Freda. It is not a remake of a 1926 silent film with the same title directed by Guido Brignone.  The sequences set in the Hell were entirely filmed in the Castellana Caves, in the province of Bari.  Maciste, the leading character, was turned during the filming into an almost mute character as Freda was very unhappy with the acting skills of the main actor Kirk Morris.    The film is referred as "an interpretation of mythology cum the gothic horror genre". 

==Plot==
In the mid 1500s a witch is burned in Scotland and places a curse on the inhabitants before she dies.  One hundred years later the tree she was chained to and burned still stands with no one daring to destroy it.  The curse remains by forcing women to commit suicide.  The descendent of the witch, Martha Gunt is sentenced to be burned for witchcraft.

As she is placed in a prison cell, Maciste comes forth to fight evil.  When he uproots the cursed tree he finds an entrance to Hell where he attempts to find the original witch to rescind her curse and attempts to help the damned from their plight.

When Maciste looks into a pool to refresh his memory the film features sequences from Maciste nella valle dei re, Maciste nella terra dei ciclopi and Maciste alla corte del gran khan.

==Cast==
* Kirk Morris as Maciste
* Hélène Chanel as Fania
* Vira Silenti as Martha Gunt
* Andrea Bosic as Judge Parrish
* Charles Fernley Fawcett as the Doctor
* Remo De Angelis as Prometheus

==Production==
The Castellana caves  which still serve as a tourist attraction, were used for the underground scenes. 

==Biography==
* 

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 



 
 