Essex Boys
 
 
{{Infobox Film
| name = Essex Boys
| image = Essexboysdvd.jpg
| image_size = 
| caption = DVD cover image
| director = Terry Winsor
| producer = Jeff Pope
| writer = Terry Winsor Jeff Pope
| narrator = 
| starring = Sean Bean Ross Morgan Alex Kingston Charlie Creed-Miles Tom Wilkinson Holly Davidson Michael McKell
| music = Colin Towns
| cinematography = John Daly
| editing = Edward Mansell
| distributor = Miramax Home Video and Pathe-UK
| released = 14 July 2000
| runtime = 102 min
| country =   English
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
Essex Boys is a 2000 British crime film. It was directed by Terry Winsor and stars Sean Bean, Alex Kingston, Tom Wilkinson, Charlie Creed-Miles and Holly Davidson.

==Synopsis==
The film is based loosely around events in December 1995 that culminated in the murders of two top drug barons and their driver in Rettendon, Essex, UK. On 6 December 1995, Patrick Tate, Craig Rolfe and Tony Tucker, three drug dealers well known to the police, were lured to Workhouse Lane, Rettendon on the pretext of a lucrative drug deal. There they were killed by shotgun blasts to the head while sitting in their Range Rover. The bodies were found the following morning.

Two men, Jack Whomes and Michael Steele, were convicted of the murders after police informer Darren Nicholls gave evidence against his former friends at their Old Bailey trial. They have always protested their innocence.
 Bonded by Blood and the 2013 Direct-to-video|straight-to-video The Fall of the Essex Boys.

==Cast==

* Charlie Creed-Miles: Billy Reynolds/narrator
* Sean Bean: Jason Locke
* Alex Kingston: Lisa Locke
* Amelia Lowdell: Nicole
* Larry Lamb: Peter Chase
* Michael McKell: Wayne Lovell
* Holly Davidson: Suzy Welch
* Terence Rigby: Henry Hobbs
* Tom Wilkinson: John Dyke Billy Murray: Perry Elley
* George Jackos: Kiri Christos
* Sally Hurst: Beverley
* Louise Landon: Jemma
* Gary Love: Detective
* Rachel Darling: Club dancer

==Reception==
The film met with generally negative critical reviews, maintaining a 17% rating at Rotten Tomatoes. The viewer ratings have generally been more positive.

==External links==
*  
*  
*  
* {{cite web
 | last = OMahoney
 | first = Bernard
 | title = Research: Court Records & other articles
 | url=http://www.bernardomahoney.com/rrmurders/articles.shtml
 | accessdate = 18 September 2012 }}

 
 
 
 
 
 
 


 