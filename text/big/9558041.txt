A Girl like Me (film)
 
{{Infobox film
| name           = A Girl Like Me
| image          = 
| alt            =  
| caption        = 
| director       = Kiri Davis
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 7 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} documentary by Kiri Davis. The seven-minute documentary examines such things as the importance of color, hair and facial features for young African American women. It won the Diversity Award at the 6th Annual Media That Matters film festival in New York City, and has received coverage  on various American media sources, such as CNN, American Broadcasting Company|ABC, NPR. The documentary has been shown on HBO. The documentary was made as part of Reel Works Teen Filmmaking.

==Synopsis== Kenneth Clark in the 1940s where African-American children were asked to choose between black or white dolls. In the original experiment(s) the majority of the children choose the white dolls. When Davis repeated the experiment 15 out of 21 children also choose the white dolls over the black, giving similar reasons as the original subjects, associating white with being "pretty" or "good" and black with "ugly" or "bad". The dolls used in the documentary were identical except for skin colour.

==Awards==
*The Diversity Award at the 6th Annual Media That Matters film festival
*The SILVERDOCS Audience Award for a Short Documentary.

==Screenings==
*Tribeca Film Festival
*The 6th Annual Media That Matters. 
*Silverdocs: AFI/Discovery Channel Documentary Festival
*HBO

==External links==
*"Kiri Davis Official Website" 
*Edney, Hazel Trice. "New Doll Test Produces Ugly Results", Baltimore Times, August 16, 2006.  
*Johnson, L. A. (2006). Documentary, Studies Renew Debate about Skin Colors Impact. Pittsburgh Post-Gazette. Pittsburgh:  .
*A Girl Like Me, Entire documentary on  
*"A Girl Like Me", Media That Matters,   of Davis
*"A Girl Like Me",   of the background of making the documentary
* "BLACK KIDS’ SELF IMAGE-NO PROGRESS" by Marian Wright Edelman  
*"A Girl Like Me", Good Morning, America, ABC, October 11, 2006.
*"African-American Images: The New Doll Test", Talk of the Nation, NPR, October 2, 2006.  
* "A Girl Like Me" appears in    a public education program developed by the American Anthropological Association.

 
 
 
 
 
 
 