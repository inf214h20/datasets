Corações em Suplício
{{Infobox film
| name           = Corações em Suplício
| image          = 
| image_size     = 
| caption        = 
| director       = Eugenio Centenaro Kerrigan
| producer       = Américo Masotti, Carlos Masotti 
| writer         = Eugenio Centenaro Kerrigan 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Américo Masotti       
| editing        =  
| distributor    = Masotti Film
| released        = 1926
| runtime        = 
| country        = Brazil Silent
| budget         = 
}} 1926 Brazilian MG in 1925, the town where it was run.

The title Corações em Suplício translates as Tormented Hearts or Hearts under Emotional Torture .

==The Masotti Brothers==

Carlos Masotti was born May 28, 1887, in Lonato, nowadays Lonato del Garda in the Province of Brescia (Lombardy), at the shores of Lake Garda in Italy, to the baker Felipe Masotti and Armida Samaelle Masotti. In 1897, the Masotti family left Italy, emigrating to Brazil, where they fixed their first residence at Rua Rio de Janeiro in Belo Horizonte, Minas Gerais|MG. And it was here Carlos Masotti|Carlos brother Américo Masotti|Américo would be given birth August 14, 1901.
 Carlos gets to a job at the press. Excited about his job, he soon shows his talent as a handicraftsman by building a wooden printing machine which he uses to publish Belo Horizontes first newspaper ever in a foreign language, Un Fiore.
 Carlos has his own printing shop where he works together with his brother Américo Masotti|Américo and marries Maria Tortoro with whom hell have seven children.

==Masotti Film==
 Carlos and MG - Carlos and photographed by Américo Masotti. Despite no commercial exhibition is done, the documentarys success is enough for the brothers to build - with the support of Fernando Máximo, manager of the local bank Banco Campos, Lima & Cia. -  their own film studios and a laboratory for the developing and copying of movies and to found their own movie production company, Masotti Film.

==Eugenio Centenaro Kerrigan==

In 1925, a man who claims to be an American movie director appears in   and says he was born in Los Angeles, California|CA, in 1878. About his heavy Italian accent, he explains he descends from an Italian noble family and that his complete name is Count Eugenio Maria Piglione Rossiglioni de Farnet. As a matter of a fact, hes on the run.
 Sofrer para Kerrigan did not know one only word of English or Spanish. He had to get away from Campinas and APA Films owner Felipe Ricci takes over himself the part of A Carnes director.

First  , Minas Gerais|MG.
 Kerrigan was actually born in 1878, but in Modena or Genova, Italy, and his name was Eugenio Centennaro.

==Corações em Suplício - The Making of==
 Kerrigan visits Masotti brothers and proposes them his project of a feature film. They accept and Corações em Suplício is run.

Roteiro, argumento e direção ficam a cargo de Kerrigan, a produção corre por conta de Carlos Masotti, auxiliado pela mulher, encarregando-se Américo Masotti da fotografia.

Screenplay, argument and direction are Ernesto Centenaro Kerrigan|Kerrigans job, Carlos Masotti, aided by his wife, is in charge of production Americo Masotti does the photography.

Financing is done by the local Banco Campos, Lima & Cia. banks manager Fernando Máximo.

In Guaranésia there was a major difficulty to find persons willing to act in a movie. Like generally in Minas Gerais at that time, cinema people were often seen as immoral. So good part of the actors and actresses had to be brought from São Paulo. But yet the makers of the movie and the two daughters of Carlos Masotti would have to join the cast.

The cast for the main roles was composed by  ) and  ) play minor female roles .

==Corações em Suplício - Incidents==

* The main actress, Lilian Loty, constantly refused to use skirts when not filming and did not care at all for the scandal a woman in trousers would mean on Brazilian countryside in the 1920s.

* The scandal grew when Lilian Loty still started an affair with the local farmer Oliveira Ramos whos described as a respected and married gentleman.

* Theres cabaret scenes in Corações e Suplício, and a major trouble was how to run them since among the local young women nobody was willing to take a mundanes role. The producers made the trip to the neighboring Guaxupé and hired actual hookers at a brothel there. While these scenes were run, the prostitutes exceeded dancing and giving scandals in public so that a collective arrest was announced.

* For the cabaret scenes, Guaranésias young single men seemed to enjoy "helping out" with background roles: this caused the break-up of a huge amount of courtships and engagements.

==Corações em Suplício - The Movie==
 Mineiro movies from those years. Two examples stated are Pedro Comellos Os Três Irmãos (= The Three Brothers; Cataguases, 1925) and José Silva s Perante Deus (= Before God; Belo Horizonte, 1930).

The film premiered at   in Rio de Janeiro, Rio de Janeiro (state)|RJ.

No surviving copy is known of this movie is known despite one has been informed to exist, but not yet located. What remains  of Masotti brothers work are the saints in Guaranésias Matriz Church they fixed soon after their arrival in 1923.

==Corações em Suplício - Critics==

Cinema magazine   is not yet a practical director, nor does he seem to be the same for the whole movie". But  ian cities and abroad, including in the USA and France.

==Corações em Suplício - The Aftermath==
 Kerrigan had been extremely generous in taking bank loans - its said that specially for himself - accounted for Masotti Film.

Banco Campos, Lima & Cia. ends going bankrupt.

Carlos Masotti and Américo Masotti transfer themselves and their Masotti Film to Belo Horizonte where they still produce some documentaries, but they never recover financially and go into bankruptcy either. Carlos Masotti dies shortly after, December 10, 1927.
 Kerrigan leaves RS where in 1928 he runs Amor que Redime for Ita Film, a movie for which photography he calls his former sponsor Thomaz de Tullio from Campinas, São Paulo (estado)|SP. One year later, in 1929, he directs his last movie, Revelação for Porto Alegres Unifilmes studios.
 Kerrigan actually Kerrigan himself Kerrigan died in Porto Alegre December 25, 1956.

==Production==
* Carlos Masotti

==Photography==
* Américo Masotti

==Director==
* Eugenio Centenaro Kerrigan

==Cast==
* Lilian Loty (actually Isaura (Loty?), also known as Lia de Loty, Djenane de Loty and Lilian de Loty; female main role)
* José Rodrigues (Roberto Rodrigues, according to some sources; male main role)
* William Gautier Kerrigan took part in the cast)
* Miguel Ascoli
* Tonico Caravieri
* Lídia Clermont (actually Lídia Masotti, daughter of Carlos Masotti)
* Míriam Clermont (actually Míriam Masotti, daughter of Carlos Masotti)
* Hippólito Collomb
* José do Plínio
* Rosetti Finzi
* Eurico Flavi
* Fernando Latorre (in his civil life, a taylor in Guaranésia)
* Jacomino Pardini
* William Rodrigues
* Antonio Rolando

==Bibliography==

* Rubá de Andrade: O Ciclo de Guaranésia - São Paulo, SP: Secretaria da Cultura, Ciência e Tecnologia/ Museu da Imagem e do Som

* Paulo Emílio Salles Gomes: Humberto Mauro, Cataguases, Cinearte - São Paulo, SP: Editora Perspectiva / EDUSP, 1974

* Fernão Ramos (org.): História do Cinema Brasileiro - São Paulo, SP: Art Editora, 1987

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 