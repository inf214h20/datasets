Singapore (1960 film)
{{Infobox film
| name           = Singapore
| image          =
| director       = Shakti Samanta
| producer       = F.C. Mehra
| writer         =
| narrator       = Padmini  Agha  Helen
| music          = Shankar Jaikishan
| cinematography =
| editing        =
| distributor    =
| released       = 1960
| runtime        = 135 mins
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Helen and Maria Menado. The film was directed by Shakti Samanta. This was one of the first full length Bollywood feature films to be shot in the foreign location of Singapore.

==Plot==
Shayam (played by Shammi Kapoor) has deputed his manager Ramesh to sell off his rubber estate in Singapore. While going through the old records, Ramesh finds a map revealing that there is a huge treasure in the rubber estate. He immediately writes to Shyam. But to Rameshs surprise, neither does Shyam reply to his letters nor does he come to Singapore.

Ultimately, Shyam is contacted on the phone. But the line is cut in between the conversation. Failing to understand anything, Shyam flies to Singapore. Once in Singapore, Shyam comes to know that Ramesh has been missing since their conversation on phone was abruptly cut off. Shyam desperately starts searching for Ramesh. He informs the police and taps every source which could lead him to Ramesh. In his search he meets Lata (played by Padmini), an Indian dancer and comes to know that Latas sister Shobha (played by Shashikala) is infatuated by Ramesh. He starts visiting Latas place frequently, where he meets Shivdas (played by K N Singh), uncle of Lata.

One day, Lata, Shobha, Shivdas and Shyam go to the rubber estate for a picnic. At the very first opportunity, Shivdas steals the map leading to the treasure from Shyams bag. Shobha sees this and follows Shivdas into the estate. Shyam follows too, but just to realise that Shivdas is shot by hoodlums and then the body goes missing. Just when he recoups from the shock, the hoodlums start to attack him.

Once he escapes from them and is headed to inform Lata that her uncle is dead, the police arrive to arrest Shyam with circumstantial evidence of murdering Shivdas. With Chachoo (played by Agha), he proves that he is not guilty, and brings down the whole gang led by Chang (played by Madan Puri).

==Soundtrack==
# Yeh Sheher bada albela
# Aane Lage Jeene
# Dekhoji Dekho
# Hat Jao Dewane
# Tum Lakh Chupana
# Rata Tayaange Re (Rasa Sayang Re) 
# Dhikha Khayegi Na
# Main Dhundu Tujh

==Cast==
* Shammi Kapoor - Shyam Padmini - Lata
* Maria Menado - Maria
* Shashikala - Shobha Agha - Chacho
* K N Singh - Shivdas
* Madan Puri - Chang Helen - Cameo Role

==References==
 

==External links==
*  

 
 
 
 
 