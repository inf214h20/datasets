The Sun Moon Legend
{{Infobox film name = The Sun Moon Legend image = The Sun Moon Legend.jpg caption = DVD cover art traditional = 新月傳奇 simplified = 新月传奇}} director = Wang Yu producer =  writer = Gu Long starring =  music =  cinematography =  editing =  studio = 宙聲影業公司 distributor =  released =   runtime =  country = Taiwan language = Mandarin budget = gross = 
}}
The Sun Moon Legend, also known as The Young Moon Legend, is a 1980 Taiwanese film adapted from Xinyue Chuanqi of Gu Longs Chu Liuxiang novel series. The film was directed by Wang Yu and starred Meng Fei as the lead character.

==Cast==
*Meng Fei as Chu Liuxiang
*Ling Yun as Prince Arrow
*Wong Goon-hung as Bai Yunsheng
*Sek Fung as Hu Tiehua
*Yeung Gwan-gwan as Crescent
*Grace Chan as Aunt Hua
*Su Chen-ping as Manager Hua
*Got Heung-ting as Chao Lin
*Luk Yat-lung as King Warrior
*Wong Chung-yue as Black Bamboo
*Chan Gwan-biu
*Chan Chung-yung
*Li Jian-ping
*Fung Gwan-ping
*Ma Cheung
*Got Siu-bo

==External links==
* 

 

 
 
 
 
 

 