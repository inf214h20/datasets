Middle Age Spread
 
 
 
{{Infobox Film
| name           = Middle Age Spread
| image          = Middle Age Spread.jpg
| image_size     = 
| caption        =  John Reid
| producer       = John Barnett 
| writer         = Roger Hall (play) Keith Aberdein (screenplay)
| starring       = Grant Tilly Donna Akersten Dorothy McKegg Bridget Armstrong Bevan Wilson Peter Sumner
| music          = Stephen McCurdy
| cinematography = Alun Bollinger Michael Horton
| distributor    = Endeavour Productions
| released       = 
| runtime        = 98 minutes
| country        = New Zealand
| language       = English
| budget         = 
}}
 play of the same title.

==Cast and characters==
* Grant Tilly – Colin
* Donna Akersten – Judy
* Dorothy McKegg – Elizabeth
* Bridget Armstrong – Isobel
* Bevan Wilson – Robert
* Peter Sumner – Reg

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p69 (1997, Oxford University Press, Auckland) ISBN 019 558336 1  
 

==External links==
*  
*  
*  at New Zealand Feature Film Database
 
 
 
 
 
 


 
 