Streetwise (1998 film)
{{Infobox film
| name           = Streetwise
| caption        =
| director       = Bruce Brown
| music          = Boris Elkis
| country        = United States
| language       = English
}}
  Divided City.

The movie, filmed in 1994, debuted in 98, and released on DVD through Maverick Home Entertainment around 2004, is set mainly in SouthEast Washington, DC in the mid 1990s. The film deals with many social issues that plagued the African-American community in that decade, including the scourge of crack-cocaine, the ruthlessness of those who dealt in its trade,  and the drastic rise in new HIV infections.

Although Brown chose fictional characters, DC residents who know the story can draw comparison to the "tenure" of Rayful Edmond III, a legend amongst DC drug dealers who many agree was as ruthless in real life as the movies antagonist, appropriately named "Raymond".  However, in reality, Edmonds successful run in the 90s was abated by prison instead of death.

== Cast (in alphabetical order)==
*James Funk
*Sheila Hayes &mdash;  Alex
*Taraji P. Henson; This movie is believed to be her debut role
*DJ Kool &mdash;  Himself
*Kurt Matthews &mdash;  Dante
*David Jason Orr &mdash;  Tom Tom
*Kim Person &mdash;  Mercedes
*DC Scorpio &mdash; Loco
*Edmond Rodgers Shorty B AKA Sugar Bear 1130s Partner
*Paco Lopez &mdash;  Himself

== External links ==
*  

 
 
 
 
 
 


 