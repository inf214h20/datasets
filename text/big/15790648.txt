No Code of Conduct
{{Infobox Film
| name           = No Code of Conduct
| image          = NoCodeOfConduct.jpg
| image_size     = 
| caption        = 
| director       = Bret Michaels
| producer       = Danny Dimbort Charlie Sheen Shane Stanley Boaz Davidson Avi Lerner Elia Samaha Trevor Short
| writer         = Bret Michaels Charlie Sheen Shane Stanley Bill Gucwa Ed Masterson
| narrator       = 
| starring       = Charlie Sheen credited as Charles Sheen  Martin Sheen Mark Dacascos
| music          = Kyle Level Bret Michaels Adam Kane
| editing        = Shane Stanley
| distributor    = Nu Image Films
| released       = 
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $12,000,000
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

No Code of Conduct is a  , Sweden, Japan, the Czech Republic, Argentina, Brazil, Azerbaijan, Russia and Turkey. Bret Michaels is credited as Director, Screenwriter, Composer (Music Score), Actor and Executive Producer. Charlie Sheens credits in this release include Actor, Screenwriter and Executive Producer.

== Plot summary ==
Jake Peterson (Charlie Sheen) is a dedicated vice unit detective, whose strong-will and dedication to duty begins to take a toll on his marriage to wife Rebecca (Meredith Salenger). Jakes father, Bill Peterson (Martin Sheen) is a veteran detective in the same unit as Jake. As well as the strain of a troubled marriage, Jake is also under pressure to live up to his fathers reputation. After the death of a fellow detective, both Jake and his father make it their personal mission to bring the killer or killers to justice. In doing so, they uncover a drug ring smuggling drugs from Mexico into Phoenix, Arizona and quickly discover just how vast and dangerous the smuggling operation really is.

== Cast ==
*Charlie Sheen as Jake Peterson
*Martin Sheen as Bill Peterson
*Mark Dacascos as Paul DeLucca
*Joe Estevez as Pappy
*Bret Michaels as Frank "Shane" Fields
*Tina Nguyen as Shi
*Paul Gleason as John Bagwell
*Ron Masak as Julian Disanto
*Joe Lando as Willdog
*Courtney Gains as Cameron
*Meredith Salenger as Rebecca Peterson
*(Bruce Nelson) as Steve

== References ==
 

==External links==
* 

 

 
 