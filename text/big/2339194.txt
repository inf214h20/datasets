He Walked by Night
{{Infobox film
| name           = He Walked by Night
| image          = He Walked by Night poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Alfred L. Werker Anthony Mann
| producer       = Bryan Foy Robert Kane
| screenplay     = John C. Higgins Crane Wilbur
| story          = Crane Wilbur
| starring       = Richard Basehart Scott Brady Roy Roberts Jack Webb Whit Bissell
| music          = Leonid Raab
| cinematography = John Alton
| editing        = Alfred DeGaetano
| distributor    = Eagle-Lion Films 
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
}} Erwin "Machine-Gun" Walker, a former Glendale California police department employee and World War II veteran who unleashed a crime spree of burglaries, robberies, and shootouts in the Los Angeles area during 1945 and 1946.  

During production, one of the actors, Jack Webb, struck up a friendship with the police technical advisor, Detective Sergeant Marty Wynn, and was inspired by a conversation with Wynn to create the radio and later television program Dragnet (series)|Dragnet. 

He Walked by Night was released by Eagle-Lion Films and is notable for the camera work by renowned noir cinematographer John Alton.  Today the film is in public domain.

==Plot==
On a Los Angeles street, Officer Rawlins, a patrolman on his way home from work, stops a man he suspects of being a burglar and is shot and mortally wounded.  The minor clues lead nowhere. Two police detectives, Sergeants Marty Brennan (Brady) and Chuck Jones (Cardwell), are assigned to catch the killer, Roy Morgan (Basehart), a brilliant mystery man with no known criminal past, who is hiding in a Hollywood bungalow and listening to police calls on his custom radio in an attempt to avoid capture. His only relationship is with his little dog.

Roy consigns burgled electronic equipment to Paul Reeves (Whit Bissell), and on his fifth sale is nearly caught when he shows up to collect on his property. Reeves tells police that the suspect is a mystery man named Roy Martin. The case crosses the paths of Brennan and Jones, who stake out Reeves office to arrest and question Roy. He suspects a trap, however, and in a brief shootout shoots and paralyzes Jones. Jones wounds Roy, who performs surgery on himself to remove the bullet and avoid going to a hospital, where his gunshot wound would be reported to the police. With his knowledge of police procedures, Roy changes his modus operandi and becomes an armed robber. During one robbery he fires his semi-automatic pistol, and the police recover the ejected casing. Lee (Jack Webb), a forensics specialist, matches the ejector marks on the casing to those recovered in the killing of Officer Rawlins and the wounding of Sgt. Jones, connecting all three shootings to one suspect.
 composite photo of the killer. Reeves then identifies Roy from the composite. However, Roy hides in Reeves car and attempts to intimidate him into revealing details of the police investigation.  He barely eludes a stakeout of Reeves house. Because the police do not realize that Roy has inside knowledge of their work, the case goes nowhere. Breen takes Brennan off the case in an attempt to shake him up. Jones convinces his partner to stop viewing the case personally and to use his head. Plodding, methodical follow-up by Brennan, using the composite photograph, results in information that Roy, whose actual name is Roy Morgan, worked for a local police department as a civilian radio dispatcher before being drafted into the Army. Brennan tracks him down through post office mail carriers and disguises himself as a milkman to get a close look at Morgan and his apartment.
 dragnet and chase through the sewers. Roy is finally cornered by the police in a passage blocked by the wheel of a police car. As the police shoot tear gas at Roy, he staggers and attempts to fire at them. He is then shot down and killed. The final scene is notable for its resemblance to the final scene in The Third Man in which Orson Welles character is chased through the sewers of Vienna.  No known connection between the films has been established.

==Cast==
* Richard Basehart as Roy Martin/Roy Morgan
* Scott Brady as Sgt. Marty Brennan
* Roy Roberts as Captain Breen
* Whit Bissell as Paul Reeves an electronics dealer
* James Cardwell as Sgt. Chuck Jones
* Jack Webb as Lee

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine gave the film a positive review and wrote, "He Walked by Night is a high-tension crime thriller, supercharged with violence but sprung with finesse. Top credits for this films wallop is shared equally by the several scripters, director Alfred Werker and a small, but superb cast headed by Richard Basehart...Starting in high gear, the film increases in momentum until the cumulative tension explodes in a powerful crime-doesnt pay climax. Striking effects are achieved through counterpoint of the slayers ingenuity in eluding the cops and the police efficiency in bringing him to book. High-spot of the film is the final sequence which takes place in LAs storm drainage tunnel system where the killer tries to make his getaway. With this role, Basehart establishes himself as one of Hollywoods most talented finds in recent years. He heavily overshadows the rest of the cast, although Scott Brady, Roy Roberts and Jim Cardwell, as the detectives, deliver with high competence. Film is also marked by realistic camera work and a solid score." 

===Accolades===
Wins
* Locarno International Film Festival: Special Prize, Best Police Film, Alfred L. Werker; 1949.

==See also==
* List of films in the public domain in the United States

==References==
 

==External links==
*  
*  
*  
*  
*  
*   (example of John Altons cinematography)

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 