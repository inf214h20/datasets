Monalisa (film)
 
{{Infobox film name           = Monalisa image          = caption        = director       = Indrajit Lankesh producer       = K. S. Dushyanth K. Santhosh Kumar writer         = Indrajit Lankesh starring  Dhyan Sadha Darshan
|music          = Valisha-Sandeep cinematography = Krishna Kumar editing        = B. S. Kemparaj studio         = Lankesh Chitralaya released       =   runtime        = 144 minutes country        = India language       = Kannada budget         =
}} Kannada romantic Dhyan and Darshan and Rekha Vedavyas have done cameo appearances.  The film is produced by K. S. Dushyanth and K. Santhosh Kumar. 
 
The film was a blockbuster success upon release and also received critical appreciation. It went on to win numerous awards at the Filmfare Awards South and Karnataka State Film Awards for the year 2004.  It was also dubbed into Telugu with the same title name and found success upon release 

==Cast== Dhyan as Dhyan
* Sadha as Monalisa and Spandana Ramakrishna 
* Bhavya as Sarasu Sharan
* Rangayana Raghu as ACP Raveendra
* Umashree 
* Shankar Bhat
* Shakeela

===Guest appearance=== Darshan
* Rekha Vedavyas
* Vanitha Vasu
* Durga Shetty
* Ruchita Prasad

==Soundtrack==
The music of the film was composed by Valisha-Sandeep duo. 

{{Infobox album  
| Name        = Monalisa
| Type        = Soundtrack
| Artist      = Valisha-Sandeep
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Ashwini Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Chori Chori
| lyrics1  	= K. Kalyan
| extra1        = Shankar Mahadevan, Kalpana
| length1       = 
| title2        = Car Car Huduga
| lyrics2 	= Nagendra Prasad Karthik
| length2       = 
| title3        = Oh Priyathame
| lyrics3       = K. Kalyan
| extra3  	= Rajesh Krishnan
| length3       = 
| title4        = Nannusiru Neene
| extra4        = K. S. Chithra
| lyrics4 	 = Doddarangegowda
| length4       = 
| title5        = Ee Manasella Neene
| extra5        = Rajesh Krishnan, Shreya Ghoshal
| lyrics5       = K. Kalyan
| length5       = 
| title6        = Monalisa Monalisa
| extra6        = Sonu Nigam, K. S. Chithra
| lyrics6       = Nagendra Prasad
| length6       = 
}}

==Awards==
* Karnataka State Film Award for Best Film
* Karnataka State Film Award for Best Director

==References==
 

==External source==
*  
*  
*  
*  
*  

 
 
 
 
 

 