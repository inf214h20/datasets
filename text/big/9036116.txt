Les Cousins (film)
{{Infobox film
| name           = Les Cousins
| image          = LesCousins.jpg
| image size     = 
| caption        = Les Cousins film poster
| director       = Claude Chabrol
| producer       = Claude Chabrol
| writer         = Claude Chabrol (story) Paul Gégauff (dialogue)
| narrator       = 
| starring       = Gérard Blain Jean-Claude Brialy Juliette Mayniel
| music          = Paul Misraki
| cinematography = Henri Decaë
| editing        = Jacques Gaillard
| distributor    = Les Films Marceau (France)
| released       =  
| runtime        = 112 minutes
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}
Les Cousins is a 1959 French New Wave drama film directed by Claude Chabrol. It tells the story of two cousins, the decadent Paul and the naive Charles. Charles falls in love with Florence, one of Pauls friends. It won the Golden Bear at the 9th Berlin International Film Festival.   

==Plot==
Paul, a dissolute, profligate and jaded Parisian, takes in his naive, innocent and idealistic cousin Charles from the provinces who is something of a mamas boy while they both attend law school. Paul takes Charles to a club at which he meets the beautiful Florence, who has the reputation of being a slut because she has slept around with every man in Pauls circle of friends. She takes an interest in Charles, who knows nothing of her past, and he kisses and falls desperately in love with her. Paul refuses to study for their law-school exam, cavalierly boasting that he is smart enough to pass it without opening a book, while Charles studies frantically for it in order to make sure that he will not disappoint his mother, to whom he writes daily. But one day, through a misunderstanding, two hours before Charles had told Florence to meet him outside the law school after his class, she comes to meet him at Pauls flat. The only ones there are Paul and Clovis, a thoroughly corrupt friend of Pauls who operates as a kind of hustler, pimp and purveyor of bizarre entertainments for Paul and his friends; Clovis has previously expressed to Florence his disapproval and resentment of her trying to break away from her past by pretending to Charles to be the virtuous maid she isnt. Clovis then lewdly proposes with insidiously lascivious suggestiveness to Florence that she have sex with Paul, to which she succumbs, and they adjourn to the bedroom, so that, by the time Charles comes home he discovers that the Florence he loves has given herself to Paul. Paul, without studying at all, passes the law-school exam anyway, as he had predicted, but Charles, despite all his study, yet distraught and in an emotional turmoil over his loss of Florence to his cousin, flunks. Torn between a desire to kill Paul and to kill himself, Charles loads one of Pauls revolver pistols with a single bullet in one of its six chambers, spins the cylinder and pulls the trigger while pointing the gun at the sleeping Pauls head, only to hear just an empty click. Later, Paul, not realizing that the pistol has a bullet in it, points it playfully at Charles, whose panic-stricken gesticulations are not enough to dissuade Paul from pulling the trigger, thereby killing Charles. The doorbell rings and Paul goes to open it with the gun in his hand. The movie ends before Paul reaches the door.

==Selected cast==
*Gérard Blain as Charles
*Jean-Claude Brialy as Paul
*Juliette Mayniel as Florence
*Guy Decomble as Bookseller
*Geneviève Cluny as Geneviève
*Michèle Méritz as Yvonne
*Corrado Guarducci as Italian Count Minerva
*Stéphane Audran as Françoise
*Paul Bisciglia as Marc
*Jeanne Pérez as Cleaning lady
*Françoise Vatel as Martine

==See also==
* 1959 in film
* French films of 1959

==References==
 

==External links==
* 
*  
*  by Terrence Rafferty

 
 

 
 
 
 
 
 
 
 
 