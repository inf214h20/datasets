This Is 40
{{Infobox film
| name           = This Is 40
| image          = This_is_40.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Judd Apatow
| producer       = Judd Apatow Barry Mendel Clayton Townsend
| writer         = Judd Apatow
| based on       =   
| starring       = Paul Rudd Leslie Mann John Lithgow Megan Fox Chris ODowd Jason Segel Melissa McCarthy Graham Parker Albert Brooks  
| music          = Jon Brion
| cinematography = Phedon Papamichael
| editing        = Brent White Jay Deuby David Bertman
| studio         = Apatow Productions Universal Pictures
| released       =  
| runtime        = 133 minutes  
| country        = United States
| language       = English
| budget         = $35 million   
| gross          = $88.1 million 
}}
This Is 40  is a 2012 American spin-off comedy film written, co-produced and directed by Judd Apatow. It is a stand-alone sequel to the 2007 film Knocked Up and stars Paul Rudd and Leslie Mann. Filming was conducted in mid-2011, and the film was released in North America on December 21, 2012. The film follows the lives of middle-aged married couple Pete and Debbie as they each turn 40, with their jobs and daughters adding stress to their relationship.

This Is 40 received mixed reviews from critics, who praised its cast, acting and the films comedic moments and perceptive scenes, but criticized the films overlong running time and occasional aimlessness.

==Plot==
The film begins with couple Debbie (Leslie Mann) and Pete (Paul Rudd), first seen in Knocked Up; they are having sex on Debbies 40th birthday. Pete reveals that he took a Viagra given to him by his friend Barry (Rob Smigel); this enrages Debbie and they stop. Debbie is angered that she is turning 40.
 special cookies", and fantasize out loud about ways they would kill each other.
 Ryan Lee), who has been taunting Sadie. She yells at him so much that his mother, Catherine (Melissa McCarthy), gets into an argument with Pete. They later have a meeting with the principal, but the couple denies everything that happened. The couple is overjoyed when Catherine starts using the same language they used previously and the principal dismisses them.

One night between the school taunting sequences, Debbie takes Desi out dancing at a club, planning to confront her with her suspicions that she has been stealing money from the store. Debbie and Desi meet several players from the Philadelphia Flyers hockey team at the club. Debbie awkwardly finds out that one of the players wants to hang out with her and possibly sleep with her. She is proud that he wants to have sex with her, but admits that she is married, has two kids, and is pregnant. Afterwards, Debbie drops Desi off at her house and confronts her about the missing money. Desi reveals she is able to afford nice things because she is also an Call girl|escort. Later, Debbie meets up with Jodi, who confesses that she stole the money to buy Oxycontin. Debbie fires her and leaves. Meanwhile, Pete and Debbie are having to deal with Sadie and Charlotte fighting all the time, which results in arguments between the family.

On Petes 40th birthday party, he argues with his dad about the money he wants from them. Debbie argues with her dad about not spending too much time in her life, and how his is perfect. Oliver then explains that his life is not perfect, and how he has always cared about her and loved her. Later, Pete overhears Debbie talking about her pregnancy, and rides out of the house on his bicycle in anger. Debbie and Larry then go after Pete, trying to find him. Soon, they find that he wrecked after hitting his head on a car door. Pete then gets into an argument with the driver of the car who then punches him in the belly. Debbie and Larry take Pete to the hospital, where Larry and Debbie reconcile, with Larry advising Debbie that its because of her, that she keeps the family together. Debbie and Pete talk later and Pete explains that he is actually thrilled about having a third baby, and that he doesnt feel trapped, so the two reconcile. Sometime later, Pete and Debbie are watching a small concert with Ryan Adams performing. Debbie then suggests that Pete should sign him to his label and plan to talk to him as they finish watching the show.    After the main credits roll, theres an extended alternate take of Catherine ad-libbing insults during the conversation with Debbie, Pete, and the principal.

==Cast==
Characters from Knocked Up: 
* Paul Rudd as Pete, Debbies husband and a record label owner 
* Leslie Mann as Debbie, Petes wife and a shop owner 
* Maude Apatow as Sadie, their 13-year-old daughter 
* Iris Apatow as Charlotte, their 8-year-old daughter 
* Jason Segel as Jason, Debbies trainer 
* Charlyne Yi as Jodi, one of Debbies employees 
* Tim Bagley as Dr. Pellegrino, Debbies gynecologist 

Other characters:   from imdb.com 
 
* Melissa McCarthy as Catherine     
* Megan Fox   as Desi, one of Debbies employees 
* Albert Brooks as Larry, Petes father 
* John Lithgow   as Oliver, Debbies 65-year-old father  Ryan Lee as Joseph 
* Lena Dunham as Cat, one of Petes employees 
* Chris ODowd as Ronnie, one of Petes employees 
* Robert Smigel as Barry, Petes friend 
* Annie Mumolo as Barb, Debbies friend 
* Joanne Baron as Mrs. Laviati
* Billie Joe Armstrong as himself 
* Graham Parker as himself   
* Tom Freund as himself Bob Andrews as himself  Brinsley Schwarz as himself 
* Martin Belmont as himself 
* Andrew Bodnar as himself 
* Steve Goulding as himself 
 

==Production==
The film&mdash;Apatows fourth directorial effort&mdash;was announced in October 2010,  while he was producing Universals Wanderlust (2012 film)|Wanderlust.

==Release== Snow White Mirror Mirror, by Relativity Media. 

The premiere for This Is 40 was held on December 12, 2012 at the Graumans Chinese Theatre, in Los Angeles. The film was released on December 21, 2012, opening in 2,912 locations nationwide.  

===Box office===
During its opening weekend, This Is 40 grossed $11.58 million at the domestic box office. 

By the end of its theatrical run, This Is 40 grossed approximately $67.5 million at the domestic box office, and approximately $20.5 million at the foreign box office, with a worldwide total of $88,058,786.   While it had the lowest opening weekend for any of Apatows films, it was a greater box-office success than his prior film, Funny People. 

===Critical reception===
This Is 40 received mixed reviews from critics. It received 52% positive reviews out of a 201 review total, with a rating of 5.8/10, on  , the film received a score of 58 based on 38 reviews and a user score of 6.9 based on 69 reviews.   

Robbie Collin of The Daily Telegraph gave the film two stars out of five, commending its premise but criticizing its execution. "This Is 40 is a comedy film about the hell of getting older in a place where aging naturally is the last taboo, and I only wish it lived up to that utterly inspired concept...every scene feels like an airbrushed composite of dozens of rambling takes, and 133 minutes is drainingly long for a story this sitcom-slight", he wrote. 

Peter Travers of Rolling Stone gave the film three stars out of four, saying "There are big laughs here, and smaller ones that sting. Rudd and Mann are a joy to watch, especially when their comic darts draw blood, as when Debbie tells "charmboy" Pete that inside hes a dick. Cheers as well to a terrific supporting cast, including Melissa McCarthy as a mother from hell, John Lithgow as Debbies withdrawn father, and the priceless Albert Brooks as Petes dad, living off his sons dole to support his tow-headed triplets. This Is 40 doesnt build to a catharsis. It sometimes dawdles as it circles the spectacle of a marriage in flux. Yet Pete and Debbies sparring yields some of Apatows most personal observations yet on the feelings for husbands, wives, parents, and children that we categorize as love." 

 ." 

Media blog eatpraymedia.com gave the film 3.5 stars citing its overly bloated run time preventing the film from ever becoming one of Apatows great films but praised the performance of the supporting cast including John Lithgow and Jason Segel. 

Richard Roeper gave the film a C- and called the film "a huge disappointment." His main complaint about the film was its running time and most of the unnecessary supporting characters. 

However, The New Yorker s Richard Brody writes that This Is 40 "is the stuff of life, and it flows like life, and, like life, it would be good for it to last longer." 

==Accolades==
{| class="wikitable" rowspan="5;" style="text-align:center; background:#fff;"
|-
!Date of Ceremony!!Award!!Category!!Recipient(s)!!Result!!Ref.
|- June 20, 2013 American Society ASCAP Film Top Box Jon Brion Graham Parker|| || 
|- 18th Critics January 10, 2013 Broadcast Film Best Comedy Film|| ||rowspan="3"| 
|- Best Actor Paul Rudd|| 
|- Best Actress Leslie Mann|| 
|- 19th Empire March 30, 2014 Empire Awards||colspan="2"|Empire Best Comedy|| || 
|- May 3, 2013 Golden Trailer Best Comedy||"Trailer Universal Pictures Workshop Creative|| ||rowspan="3"| 
|- Best Comedy TV Spot||"Knocked Up" Universal Pictures|| 
|- Best Comedy Universal Pictures Cimarron Entertainment|| 
|- October 22, 2012 Hollywood Film Hollywood Comedy Judd Apatow|| || 
|- December 19, 2012 IGN Summer Best Comedy Movie|| || 
|- December 18, 2012 Phoenix Film Best Young Maude Apatow|| || 
|- 34th Young May 5, 2013 Young Artist 34th Young Best Performance in a Feature Film - Supporting Young Actress|| ||   
|}

==Home media==
 
This Is 40 was released for Blu-ray and DVD in the U.S. on March 22, 2013.  It is available for digital download on iTunes, Google Play, and other websites. The Blu-ray version is being sold as a single disc, and also a combo pack, which includes a DVD copy, digital copy, and UltraViolet (system)|Ultraviolet. The disc features an unrated and also theatrical version of the film, as well as numerous bonus features.

==Sequel==
Director Judd Apatow is considering such a film, shifting the focus off married couple Pete (Paul Rudd) and Debbie (Leslie Mann) and moving it onto their budding teenage daughter Sadie (Maude Apatow). During an interview on March 30, 2013 with Getty Images Entertainment (via Hey U Guys), Apatow was questioned about the prospect of a sequel to This Is 40. He admitted to being intrigued by the idea.  Though no further news has come since the interview.

==References==
{{Reflist|colwidth=30em|refs=
   
    
}}

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 