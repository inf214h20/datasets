Trois
{{Infobox film
| name = Trois
| image = Trois VHS cover.jpg
| image_size =
| border =
| alt =
| caption = VHS cover
| film name =
| director = Rob Hardy William Packer
| writer =
| screenplay =
| story =
| based on =  
| narrator = Thom Byrd
| music =
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| budget = $250,000 
| gross = $1.3 million   
}}

Trois is a 2000 erotic   and  .

==Synopsis==
Jermaine Davis (Dourdan) is a young attorney who is newly married and has recently moved to Atlanta, Georgia with his lovely and supportive wife Jasmine (Moore). While becoming settled into the new city and job, Jermaine becomes bored with his seemingly mundane lifestyle at home. He asks his wife to engage in a ménage à trois with another woman, in order to generate more excitement within their relationship and she reluctantly agrees.

Once theyve committed the act, Jermaine begins to feel the insecurities of bringing a stranger into his marriage. As a result, he attempts to sever all ties with the woman. Unfortunately, it proves more complicated to remove this person from their lives and he realizes that his curiosity has thrown him into battle with a dangerous lunatic and may cost him his marriage.

==Cast==
*Gary Dourdan as Jermaine Davis
*Kenya Moore as Jasmine Davis
*Gretchen Palmer as Jade Owens
*Bryce Wilson as Robert
*Chrystale Wilson as Tammy
*Soloman K. Smith as Terrance/Eric Thom Byrd as Thomas
*Donna Biscoe as Ms. Paul

==Reception== Toledo Blade gave Trois two stars, criticizing the movie as not being able to decide whether it wanted to be a "serious artistic endeavor" or a "cheap thrill" and suffering as a result. 

==Sequels==
Two further films were produced in the film series,   and  .  Neither film was a direct sequel to the first film and Pandoras Box was not filmed with the intent of creating it as a part of the Trois film series. Critical reception for the second film in the series was poor. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 