The Menace (1928 film)
 

 
{{Infobox film
| name           = The Menace
| image          = 
| image_size     =
| caption        = 
| director       = Cyril J. Sharpe
| producer       = Percy Juchau
| writer         = Louise Miller
| based on       = 
| narrator       =
| starring       = David Edelsten
| music          =
| cinematography = Jack Bruce
| editing        = 
| studio = Juchau Productions
| distributor    = 
| released       = 2 February 1928 (preview)
| runtime        =
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Menace is a 1928 Australian silent film about the drug trade in Sydney. It is considered a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 143. 

==Production==
Sydney businessman Percy Juchau formed a film company in August 1926. It employed five people who had experience working in Hollywood – director Cyril Sharpe, camera technician Al True, crew members Al True and Eric Wilkinson, and Louise Miller. This was the only movie they made.
 Sydney Showground. 

==Release==
The film was previewed but not released commercially. It was viewed by a film critic from the Sydney Morning Herald who criticised it for "unhealthy sensationalism". 

After filming, Cyril Sharpe left Juchau and became managing director of Commonwealth Film Laboratories in Sydney, with Jack Bruce as his chief technician. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 
 


 