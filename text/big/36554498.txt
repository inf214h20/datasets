How Molly Made Good
 
{{infobox_film
| name           = How Molly Made Good
| image          =File:How Molly Made Good poster.jpg
| imagesize      =
| caption        =Film poster
| director       = Lawrence B. McGill
| producer       = Lee Kugel
| writer         = Burns Mantle
| starring       = Marguerite Gale
| music          =
| cinematography =
| editing        =
| distributor    = Kulee Features
| released       =   reels
| country        = US
| language       = Silent English intertitles

}} silent drama film which is one of the first films to feature cameo appearances by major celebrities. It survives in the Library of Congress and is available on DVD.   

The opera star Madame Fjorde is possibly a created character for the film masquerading as a famous opera star. No information on her other than this film is found on the internet.

==Cast==
*Marguerite Gale - Molly Malone
*Helen Hilton - Alva Hinton, Rival Reporter
*John Reedy - Reed, The Photographer
*William H. Tooker - Editor
*William A. Williams - Journalist
*Armand Cortes - Benny the Dip
*James Bagley - Morrison, Associate Editor
*Edward P. Sullivan - Journalist
*Madame Fjorde - Herself, cameo
*Lulu Glaser - Herself, cameo
*May Robson - Herself, cameo
*Henry Kolker - Himself, cameo Cyril Scott - Himself, cameo
*Julian Eltinge - Himself, cameo
*Charles J. Ross - Himself, cameo
*Mabel Fenton - Herself, cameo
*Robert Edeson - Himself, cameo
*Leo Ditrichstein - Himself, cameo Julia Dean - Herself, cameo
*Henrietta Crosman - Herself, cameo

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 