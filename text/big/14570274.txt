The Yoke's on Me
{{Infobox Film |
  | name           = The Yokes on Me |
  | image          = Yokesonme44LOBBYTWO.jpg|
  | caption        = |
  | director       = Jules White
  | writer         = Clyde Bruckman | Robert McKenzie Eva McKenzie Emmett Lynn Al Thompson Victor Travers|
  | cinematography = Glen Gano | 
  | editing        = Charles Hochberg |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 08" |
  | country        = United States
  | language       = English
}}

The Yokes on Me is the 79th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
  Robert McKenzie) insists they aid the war effort instead by becoming farmers. Inspired, the trio sell their dilapidated car and buy an equally dilapidated farm. The farm contains no livestock except for one ostrich, which eats gunpowder. The boys then spot some pumpkins and decide to carve and sell them.

In the interim, several Japanese refugees escape a prison camp (known during World War II as relocation centers), and work their way onto the Stooges farm. Curly is the first to notice some suspicious activity (one of the refugees places the carved pumpkin on his head, spooking Curly). Eventually, Moe and Larry believe him, and realize that the farm is surrounded by the Japanese. Moe then throws an ostrich egg (laden with digested gunpowder) at the refugees, killing them.

==Production notes==
The Yokes on Me was filmed on November 8-12, 1943.    The films title is a pun on the expression, "the jokes on me." 

===Controversy===
During World War II, the Stooges released several comedies that engaged in propaganda against the then-enemy Japanese, including Spook Louder, No Dough Boys, Booby Dupes and The Yokes on Me. 

The Yokes on Me is especially singled out by modern critics. For many years, the film was blacklisted by some television stations, due to its treatment of Japanese American escapees from a relocation center (the character are not Japanese POWs). 

Author Jon Solomon has said, "no Stooge film so profoundly disturbs modern viewers as this one."    Author Michael Fleming put it more bluntly: "Knowing what we do now about how Japanese-born American citizens were mistreated and stripped of their belongings in relocation centers makes this as funny as a train wreck." 

==Quotes==
*Curly: "Look, look! A pelican!"
*Moe: "Thats no pelican, its a gander." Mahatma gander?"
*Moe: "No, a gander, a gander! A gooses husband."
*Larry:  "Yeah, a papa goose."
*Curly: "Do they have papa gooseses and mama gooseses?"
*Larry: "Oh sure. And little baby gooseses, too."
*Curly: "Oh, I read about them. They come from Germany...the Gestapo|goosetapo!"

== References ==
 

== External links ==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 