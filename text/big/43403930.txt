Moondraam Ullaga Por
 
 
{{Infobox film
| name           =  Moondraam Ullaga Por
| image          =  Moondraam Ullaga por1.jpg
| alt            =  
| caption        = Promotional poster
| director       = Sugan kartthi
| producer       = T.R.S Anbu,V.Sures Narayan
| writer         = Sugan kartthi
| starring       = Sunil Kumar, Akhila Kishore, Wilson Ng, Avinash, Ravi,  
| music          = S.Ved Shanker 
| cinematography = Dheva 
| editing        = Richard S
| Choreographer  = M.Sherief
| studio         = Aartin Frames( T.R.S Group)
| released       =  
| runtime        = 128 minutes
| country        = India
| language       = Tamil
| budget         = 
}}

Moondraam Ullaga Por ( ) is an upcoming 2015 Tamil-language Indian political drama-romance film directed by Sugan kartthi.The movie is produced by TRS Anbu & V.Sures Narayan under the banner Aartin Frames.
 Alexa camera, Edited by Richard S (Paalai) music by S.Ved Shanker (Paalai,Naduvula Konjam Pakkatha Kaanom),Lyrics Annamalai, and Art Director Sethu Ramesh,Choreographed by M.Sherief.

==Cast==
* Sunil Kumar as Saravanan
* Akhila Kishore as Madhivadhani
* Wilson Ng as Lim Bai Huai
* Avinash as Subramaniam
* Ravi-(Vijay TV Fame) as Ravi
* Jeniffer as Heros Mother
*   as Mugundhan
* Nafi as Dr.Nafi
* Hamsa as Sister of Heroine
* Raj Kish
* Siva Ganesh as Kiran Nair
* Aravind as Robhindra Singh
* SundaR B as Sandeep Rai Rathore
* Karthick Rajendran as Pon Manickam
* Prabhu Karthik as Chadrasekar Reddy

==Production==
Moondraam Ullaga Por Shooting completed on 10-Sep-2014, and now post production is in progress with extensive VFX work.

 
 
 
 
 

==Soundtrack==
The soundtrack album of Moondraam Ullaga Por, composed by S.Ved Shanker, consists of three tracks.
Sung by Shankar Mahadevan, Chinmayi, Shakthisree Gopalan and S.Ved Shanker.Lyricist Annamalai penned all the songs in the movie while sherief has done the choreography. Audio will be launched on May 2015.

==Release==
The first look motion poster teaser of Moondraam Ullaga Por    is released on 29 August 2014 on vinayagar sathurthi.
Moondraam Ullaga Por Teaser Launched on April 30, 2015 with rave reviews.  
The film is scheduled to release on 2015 June.

== References ==
 
http://cinema.dinakaran.com/cine-news-details.aspx?id=13654&id1=3

== External links ==
* 
*  

 
 
 


 