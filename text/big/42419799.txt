The Wedding Hotel
{{Infobox film
| name = The Wedding Hotel
| image =
| image_size =
| caption =
| director = Carl Boese
| producer =  Erich Holder  
| writer =  Gustav Kampendonk   Géza von Cziffra   Carl Boese
| narrator =
| starring = Karin Hardt   René Deltgen   Walter Janssen   Ernst Waldow 
| music = Willy Mattes  
| cinematography = Konstantin Irmen-Tschet  
| editing =  Willy Zeunert     UFA
| distributor = Deutsche Filmvertriebs 
| released = 12 December 1944 
| runtime = 84 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Wedding Hotel (German:Das Hochzeitshotel) is a 1944 German comedy film directed by Carl Boese and starring Karin Hardt, René Deltgen and Walter Janssen. Due to Allied bombing raids on German cities like Berlin, much of the film was shot around Kitzbühel in Tyrol (state)|Tyrol. It was one of a number of light-hearted German films made in the final year of the Third Reich. 

==Synopsis==
A group of artists and journalists enjoy a series of romantic entanglements in a country hotel. An author, Vera von Eichberg "of whom no photo exists," has mentioned the hotel in her work, increasing its clientele. When another female guest arrives, everyone assumes she is the author, despite her repeated assertions to the contrary. {{Cite web
| title = DAS HOCHZEITSHOTEL (1944)
| work = RAREFILMSANDMORE.COM.
| accessdate = 2014-04-09
| url = http://www.rarefilmsandmore.com/das-hochzeitshotel-1944
}} 

==Main cast==
*   Karin Hardt as Brigitte Elling, Verkäuferin 
* René Deltgen as Viktor Hoffmann, Pressephotograph 
* Walter Janssen as Burgmüller, Schriftsteller 
* Ernst Waldow as Alexander, sein Sekretär  Hermann Pfeiffer as Dr. Wolter, Burgmüllers Verleger 
* Hans Hermann Schaufuß as Rupp, Chef eines Pressedienstes 
* Georg Vogelsang as Nepomuk Balg, Amtsvorsteher 
* Helmuth Helsig as Hacke, Kriminalkommissar 
* Edwin Jürgensen as Berendt, Juwelier 
* Roma Bahn as Frau Berendt 
* Hans Fidesser as Geschäftsführer im "Seehotel" 
* Ernst Sattler as Portier im "Seehotel"  Franz Weber as Knauer, Herausgeber einer Provinzzeitung

== References ==
 

== Bibliography ==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999. 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 