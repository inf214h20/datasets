Diya Aur Toofan (1995 film)
{{Infobox film
 | name = Diya Aur Toofan
 | image = hin_diya_aur_toofan.jpg
 | caption = DVD Cover
 | director = K. Bapaiah
 | producer = 
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Madhoo Suresh Oberoi Mohnish Bahl Kader Khan
 | music = 
 | lyrics =
 | associate director = 
 | art director = 
 | choreographer = 
 | released = October 27, 1995
 | runtime = 125 min.
 | language = Hindi Rs 3 Crores
 | preceded_by = 
 | followed_by = 
}}
 1995 Hindi Indian feature directed by K. Bapaiah, starring Mithun Chakraborty, Madhoo, Suresh Oberoi, Mohnish Bahl and Kader Khan.

==Plot==
The film is a Science fiction, where dead Mithuns brain gets transplanted in Madhoo and she takes revenge of Mithuns killers.

==Summary==
Gajendra Singh is a very wealthy contractor who, with his partner Madanlal, sells government provided building materials into the black market in exchange for weak ones.  A civil engineer and his supervisor find out and upon their meeting are subsequently murdered by Gajendra.  While Dr. Vijay is unable to treat the two workers, he gets a letter informing himself that his friend Amar, a gold medalist engineer, is the replacement engineer for the crooked contractors.  Amar, just like his predecessors, is very honest and wont do with what his bosses tell him to.  After an attempt to bribe Amar, the contractors get beaten up and Amar threatens to expose them.  Joginder, Gajendras son, is a rogue who sets his eyes on Asha, Amars eventual wife-to-be.  On their wedding night, Gajendra, Madanlal, and Joginder all stab Amar, and Asha, when finding out, goes crazy. During a frenzied visit to the temple one night, she slips and falls down the stone steps leading to permanent brain damage.  Vijay then transplants Amars brain into Ashas body and Asha eventually kills the trio one by one.  Before she hangs Gajendra, she is shot by the police, and in a dying soliloquy, claims that she will be with Amar for eternity.

==Cast==
*Mithun Chakraborty  as  Amar
*Madhoo  as  Asha
*Suresh Oberoi  as  Dr. Vijay Mehra
*Prem Chopra  as  Thakur Gajendra Singh
*Mohnish Behl  as  Joginder Singh
*Shakti Kapoor   as  Madanlal Tabedaar
*Kader Khan  as  Gyaneshwar
*Asrani  as  Solanki
*Maya Alagh  as  Ashas mother Nanda

==References==
 

==External links==
*  

 
 
 
 
 
 


 