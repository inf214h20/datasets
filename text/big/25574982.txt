Before Sundown
 
{{Infobox film
| name           = Before Sundown
| image          = 
| caption        = 
| director       = Gottfried Reinhardt
| producer       = Artur Brauner
| writer         = Gerhart Hauptmann Jochen Huth
| starring       = Hans Albers
| music          = Werner Eisbrenner
| cinematography = Kurt Hasse
| editing        = Kurt Zeunert
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = West Germany
| language       = German
| budget         = 
}}
 Golden Bear (Audience award).   

==Cast==
* Hans Albers – Generaldirektor Mathias Clausen
* Annemarie Düringer – Inken Peters
* Martin Held – Erich Klamroth
* Claus Biederstaedt – Egbert Clausen
* Hannelore Schroth – Ottilie Klamroth, geb. Clausen
* Erich Schellow – Wolfgang Clausen
* Maria Becker – Bettina Clausen
* Johanna Hofer – Frau Peters, Inkens Mutter
* Inge Langen – Paula Clausen, geb. Rübsamen Hans Nielsen – Dr. Steynitz, Sanitätsrat
* Reginald Pasch – Diener
* Wolfgang Preiss – Dr. Hahnefeld, Syndikus der Clausen-Werke
* Kurt Vespermann – Wuttke, Fahrer bei Clausen Franz Weber – Gärtner

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 