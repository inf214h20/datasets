You're in the Navy Now
{{Infobox film
| name           = Youre in the Navy Now
| image          = Poster - Youre in the Navy Now (1951) 01.jpg
| caption        = Theatrical poster Richard Murphy Ray Collins Jack Webb
| director       = Henry Hathaway
| producer       = Fred Kohlmar
| music          = Cyril Mockridge
| cinematography = Joseph MacDonald James B. Clark
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.6 million (US rentals) 
}}
 1951 by Twentieth Century Fox about the United States Navy in the first months of World War II. Its initial release was titled USS Teakettle. Directed by Henry Hathaway, the film is a comedy starring Gary Cooper as a new officer wanting duty at sea but who is instead assigned to an experimental project without much hope of success.
 Richard Murphy was nominated by the Writers Guild of America for "Best Written American Comedy", basing his script on an article written by John W. Hazard in The New Yorker. Hazard, a professional journalist and naval reservist, had served during World War II as executive officer of the USS Castine (IX-211)|PC-452, a similar craft that served in 1943-44 as a test bed for steam turbine propulsion.

==Plot== Norfolk Naval condenser in the ship to test its feasibility in patrol craft and has assigned Harkness to conduct the sea trials.
 Regular Navy. chief boatswains 90 day wonders". The exec, Lt. (j.g.) Barron (Eddie Albert), is a good-natured idea-man whose knowledge of seamanship is out of books. The engineering officer, Ens. Barbo (Jack Webb), has no training, education, or experience in engineering. And the Materiel|supply-Mess officer, Ens. Dorrance (Richard Erdman), is plagued by seasickness.
 bow of Ray Collins). The first trial results in the ship being towed into port, disparaged as the "USS Teakettle" by the rest of the base. Reynolds restricts the crew to the ship until they make the system work, and as the failures mount, the crews morale plummets, threatening the entire project. Ellie, who is with the WAVES, gets information to her husband about Tennants activities.

The officers hit upon a scheme to enter a crewman in the base boxing championship to unite the crew. They train an engine room sailor, Wascylewski (Charles Bronson), to represent the ship. The crew bets heavily on their shipmate, and to ensure that the "Teakettle" does not fail a sea trial scheduled for the day of the fight, smuggles distilled water aboard. Wascylewski breaks his ribs during the sea trial, forcing Barbo to stand in, but surprisingly he wins the championship.

The film climaxes with the Official Sea Trial of the "Teakettle" in which the crew improvises a successful run. Even so, the trial ends in humiliation for the crew when the ship rams an aircraft carrier—again. At the board of inquiry that follows, Admiral Tennant reveals to Harkness that the selection of his crew was no fluke: the Navy already knew that experts could run the system; it needed to see if novice sailors, who made up the overwhelming percentage of the wartime Navy, could quickly learn to operate it.

==Cast==
*Gary Cooper - Lieutenant John Harkness
*Jane Greer - Ensign Ellie Harkness
*Millard Mitchell - Chief George Larrabee
*Eddie Albert - Lieutenant (j.g.) Bill Barron
*John McIntire - Commander W.R. Reynolds Ray Collins - Rear Admiral L.E. Tennant
*Jack Webb - Ensign Tony Barbo
*Richard Erdman - Ensign Chuck Dorrance
*Charles Bronson - Wascylewski
*Harry Von Zell - Captain Danny Eliot
*Ed Begley - Port commander
*Harvey Lembeck - Seaman Norelli
*Lee Marvin - signalman
*Jack Warden - Helmsman Morris

==Production== Norfolk Naval anachronistic to the date of the storyline.
 Roxy Theater in New York City on February 23, 1951.

== See also ==
*Youre in the Army Now, a 1941 comedy film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 