Xuoi nguoc duong tran
{{Infobox film
| name           = Xuoi nguoc duong tran
| image          = Áp_phích_đại_diện_cho_phim_Xuoi_nguoc_duong_tran.jpg
| caption        = Xuoi nguoc duong tran
| director       = Linh Nga(a.k.a. Lina Nga Nguyen) Tuan Anh Nguyen
| 1st AD         = The Như   Tam Anh
| producer       = Vietnam Television
| writer         = Linh Nga
| starring       = Linh Nga Mai Ngoc Can The Tuc Kim Xuyen
| music          = Dang Huu Phuc
| cinematography = The Manh
| makeup         = Tra Giang
| released       =  
| runtime        = 210 minutes
| country        = Vietnam
| language       = Vietnamese
| budget         = US$200 thousand    website         = https://www.facebook.com/XuoiNguocDuongTran/
}}
Xuoi nguoc duong tran ( ) ( ) is a 2002 Vietnamese TV series, written and directed by Linh Nga (a.k.a. Lina Nga Nguyen) and co. director Tuan Anh Nguyen, starring Linh Nga, Mai Ngoc Can, The Tuc and Kim Xuyen. It premiered on February 21, 2002 at the Vietnamese International Film Festival in Hanoi, Vietnam. The film won Academy International Award for Best TV series of 2002. It was released on May 27, 2002 in Hanoi and Ho Chi Minh City and other cities.   

==Plot==
 
The setting is 1990s, in   is a bridge to overcome suffering."
 

==Cast and characters==
*Linh Nga as Lien - The Vietnamese protagonist, who is young, kind-hearted. Lien is the main character who traditionally undergoes some sort of change, pain. She must usually overcome a lot of opposing force. In the film, the protagonist who tells her audience all about her life in an abusive, dysfunctional, day-nightmare family. We learn through her that the family relationships is just painfully black and white. 
*The Tuc as master - The leaders master who is sacrificed his life for Buddhism. He is the huge opened gate for Liens question:"What is the purpose of life? To get or to be content and happy? How to avoid of craving and physical, emotional suffering?"
*Kim Xuyen as cruel stepmother - A nightmare, devillish stepmother. She is the main reason of Liens father death. This unique, vivid character is a perfect background, an obstacle provider for Liens beautiful portrait.

==Production==
"Xuoi nguoc duong tran" ("In the shadow of life") cast and crew shot for 70 days in Vietnam, where the local film industry is still developing. They had to deal with a number of obstacles, including crew members who got sick, paperworks, actors who were injured, extra budget, and the bad prejudices of some Vietnamese people against Linh Nga as she was only 20 years old young female director. One of another the hardest problems in making the film was travelling across Vietnam to film the Sóc Trăng pagoda scenes. It was such a magical adventure for anyone who was a part of the trip.

Parts of the film, mainly buddhist scenes in the last part of the film, were shot in the ancient temples of Sóc Trăng.

==Theme==
Xuoi nguoc duong tran("In the shadow of life") is a film seeking to recover the voices of Vietnamese women, have long recognized the need to be resourceful in that task. As many have noticed, the legacy of womens public speeches is scattered in unlikely places and, when discovered, often difficult to authenticate. Thus, the film have cast a wide net in their effort to share, understand, love, and include womens experiences, point of views and positions in the very traditional Vietnamese life . Most importantly, the film is one of the deepest answers for the question:" How to overcome suffering in life?"   

==Honours and awards== VIFF 2002

==See also==
* Linh Nga

==References==
 

==External links==
 
 
*  
* 

 
 
 
 
 
 
 