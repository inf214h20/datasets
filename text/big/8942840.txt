Scudda Hoo! Scudda Hay!
{{Infobox film
| name           = Scudda Hoo! Scudda Hay!
| image          = Scudda1.jpg
| image_size     =
| caption        =
| director       = F. Hugh Herbert
| producer       = Walter Morosco
| screenplay     = F. Hugh Herbert
| story          =
| based on       =  
| narrator       =
| starring       = June Haver  Lon McCallister  Walter Brennan
| music          = Cyril Mockridge Ernest Palmer
| editing        = Harmon Jones
| studio         = 20th Century Fox 
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 95 
| country        = United States English
| budget         =
}}
Scudda Hoo! Scudda Hay! is a 1948 American comedy film, written and directed by F. Hugh Herbert, starring June Haver,  Lon McCallister and Walter Brennan. Released by 20th Century Fox,  it is known for Marilyn Monroes earliest speaking role, a one-line bit part.

The screenplay was adapted by the director F. Hugh Herbert from the novel of the same name by George Agnew Chamberlain.   

The film tells the story of two antagonistic stepbrothers living on a mid-western farm with their mother. One of them takes a job as a hired-hand with a neighboring farmer from whom he buys a pair of mules and must learn to train them, and whose daughter he is in love with, though she entices both brothers to compete for her affections.

Colleen Townsend is also featured in the movie, though not credited, playing a small role.

==Plot==
Farmer Milt Dominy (Henry Hull) and his son Daniel (Lon McCallister), who is called "Snug", commiserate with each other about their loathing of Judith Anne Revere, Milts second wife, and her brutish son Stretch (Robert Karnes). Milt decides to return to the sea while Snug takes a job as a hired hand with a neighboring farmer, Robert "Roarer" McGill (Tom Tully), with whose daughter, Rad (June Haver), he is in love, although the daughter gets her kicks out of keeping him guessing about her true feelings. Her father neither encourages nor endorses the courtship.

Some days later, Snug offers to buy two mules, named Crowder and Moonbeam, from his boss, to add to his income. Roarer agrees but warns Snug that ownership of the mules will revert to him if Snug misses even one payment. Snug then takes Crowder and Moonbeam to Tony (Walter Brennan)s farm, and Tony, who was once a dedicated mule driver before falling down on his luck and becoming an alcoholic. While learning about the mules, Snug also deals with Judith and Stretch, who are trying to take over the Dominy farm.

Eager to help Snug, Tony introduces him to logging foreman Mike Malone (G. Pat Collins), who offers him a well-paying job, which will start when Snug learns how to drive the mules. Tony teaches Snug the commands "scudda hoo" and "scudda hay," which mean "gee" and "haw," country slang for "left" and "right".

One day, Snugs deliberate insolence prompts Roarer to fire him, and Snug goes to work at the lumber camp. Snug intends to use his first weeks pay for another installment on the mules and is devastated when Tony, who was holding the money, returns home drunk and broke. Snug begs Roarer to accept a double payment in a few days, but Roarer refuses and asks Sheriff Tod Bursom to enforce his right to reclaim the mules. Seeing this, Roarers wife Lucy finally stands up to her overbearing husband and loans the money.

Meanwhile, Snug learns that his father has died, leaving him the Dominy farm, and Tony promises to consult Judge Stillwell about evicting Stretch and Judith. Soon after, Stretch places a wire snare in Crowder and Moonbeams stall in an attempt to cripple them. Snug and Rad, who are out on a date, return to Tonys house and there catch Stretch as Crowder is crushing him against the barn wall. Snug rescues Stretch from Crowder then throws him off Tonys property. Later, Judge Stillwell and Sheriff Bursom evict Stretch and his mother from the Dominy farm. As Snug, Rad and Tony are riding back to Tonys, they pass Roarer, whose tractor is stuck in the mud. Snug bets Roarer that if Moonbeam and Crowder can pull the tractor free, Roarer will forget Snugs debt, but if they fail, Roarer will reassume possession of them. Snug also asks for Roarers blessing of his marriage to Rad if he succeeds, and Roarer reluctantly agrees. Snug expertly drives the animals and soon the tractor is free. Finally, as a happy Rad joins Snug, Roarer concedes that at least the mules will still be in the family.

==Cast==
*June Haver as Rad McGill
*Lon McCallister as Daniel "Snug" Dominy
*Walter Brennan as Tony Maule
*Anne Revere as Judith Dominy
*Robert Karnes as Stretch Dominy
*Henry Hull as Milt Dominy
*Tom Tully as Robert McGill
*Natalie Wood as Eufraznee "Bean" McGill
*G. Pat Collins as Mike Malone (uncredited)
*Marilyn Monroe as Betty (Girl leaving Church Service greeting Rad) (uncredited)
*Colleen Townsend as Girl Leaving Church Service (uncredited)

==Marilyn Monroes participation==
After having been signed to 20th Century Fox as a contract player, Marilyn Monroe had her first bit part playing Betty in this film. Dressed in a pinafore and walking down the steps of a church, she says, "Hi, Rad" to Havers character, who responds, "Hi, Betty." After Monroes stardom, 20th Century Fox began claiming that Monroes only line in the film had been cut out, an anecdote Monroe repeated on Person to Person in 1955. But film historian James Haspiel says her line is intact, and she also appears in a shot with herself and another woman paddling a canoe.   

==In popular culture==
In a scene from the 1989 motion picture Driving Miss Daisy, the movie theatre marquee in town indicates Scudda Hoo! Scudda Hay! is playing when Hoke drives Miss Daisy to the Piggly Wiggly grocery store.

==References==
 

==External links==
*  
*  
*  
*  
* 

 
 
 
 
 
 
 