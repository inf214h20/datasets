A Congregation of Ghosts
{{Infobox film
| name           = A Congregation of Ghosts
| image          = A Congregation of Ghosts.jpg
| caption        = 
| director       = Mark Collicott
| producer       = Mark Collicott
| writer         = Mark Collicott Martyn Wade Tom Wnek
| starring       = Edward Woodward Nicholas Gleaves Susannah Doyle Murray McArthur Natasha Little
| music          = Luke Phillips
| cinematography = Ray Coates
| editing        = Rachael Spann
| released       = 
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
}}
A Congregation of Ghosts is a 2009 film directed by Mark Collicott. It is based on the true story of the Reverend Frederick Densham.    The film was shot on location in rural Cornwall in the United Kingdom. As of November 2012 the film has yet to be released.

==Background==
Densham was a naive and pious man who worked in Whitechapel in a boys home and also a home for inebriates. In 1921 he visited Natal in South Africa which is where he may have been influenced by the ideas of Gandhi, who had campaigned for the rights of "coloured people" in that province. There is no evidence for any missionary work in India, though it has been speculated that he visited both India and Germany at a time when foreign travel was considered much more adventurous than it is today. In 1931 Densham took up the post of vicar in the remote Cornish village of Warleggan on Bodmin Moor.

Denshams father, William, had been a Methodist preacher. It is not known why Densham was ordained into the Church of England. As a highly educated man for his time (BA, ACA) he may have felt that the Church of  England was more "intellectual". However, his heart and mind lay with Methodism and thus more "Low Church". He entered a church whose congregation was "High Church". Over the years, differences slowly deepened between him and several members of his congregation. The congregation had always been small (four to nine people) and Densham found himself on many occasions preaching to an empty church. He developed the habit of occasionally placing tiny cards in the first six pews, labelled with the names of vicars going back to the Romans. When he did so he noted this in the church records and many local people remember the tiny cards. Unfortunately, Daphne du Maurier imaginatively created a "cardboard cut-out" myth around this practice. This had led to many ill-founded film and website remarks about Densham. Local people, many of whom still remember him though he died in 1953, resent the crude caricatures of the rector and of themselves.

Although Densham was undoubtedly isolated by the Church of England congregation, he was popular among the local Methodists and often preached at Warleggan Chapel. He was a kindly and generous man, known for bringing rhododendron and camellias in spring to villagers and for sending milk to people who were ill. He longed for intellectual stimulation and wrote constantly, sending and receiving several letters a week. When he died, he was cremated as he had stipulated, though his wish that his ashes be scattered in a garden of remembrance that he had created in the grounds of the rectory were ignored.

==Cast==
* Edward Woodward as the Reverend Frederick Densham (Woodwards last film role)
* Nicholas Gleaves as Ellis Baxter
* Susannah Doyle as Barbara Baxter
* Murray McArthur as George Treddinick
* Natasha Little as Daphne du Maurier

==DVD Release==
In 2014 Visual Entertainment released the film in Region 1 as a part of the bonus content for the complete collection boxset of The Equalizer. 

==References==
 

==External links==
*  
*  
* " " article by The Times  

 
 
 
 
 