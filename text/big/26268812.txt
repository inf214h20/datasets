Bhakta Kannappa
{{Infobox film
| name           = Bhakta Kannappa
| image          = Bhakta Kannappa.jpg
| image_size     =
| caption        = Bapu
| producer       = U. Suryanarayana Raju
| writer         = Mullapudi Venkata Ramana
| narrator       =
| starring       = Krishnam Raju Vanisree M. Balaiah Rao Gopal Rao Sarathi Allu Ramalingaiah Sreedhar Surapaneni Mukkamala M. Prabhakar Reddy
| music          = Chellapilla Satyam
| cinematography = V. S. R. Swamy M. V. Raghu
| editing        = Mandapati Ramachandraiah
| studio         =
| distributor    =
| released       = 1976
| Awards        =Filmfare Best Actor
| country        = India
| language       = Telugu
| budget         =
}}

Bhakta Kannappa (English title: Devotee Kannappa) is a 1976 Telugu film directed by Bapu (artist)|Bapu. It is based on the life of Shaiva devotee, Kannappa Nayanar. 

==Credits==

===Cast=== Kannappa
* Vanisree ...  Neela
* M. Balaiah ...  Lord Shiva
* Rao Gopal Rao ...  Kailasanatha Shastry
* Sarathi ...  Kashinatha Sastry (Kailasanatha Shastrys son)
* Allu Ramalingaiah
* Sridhar ...  Mallanna
* M. Prabhakar Reddy
* Mukkamala Krishna Murthy ...  Pedavema Reddy
* P. R. Varalakshmi ...  Goddess Parvati
* Jhansi
* Baby Varalakshmi Baby Rohini
* Jaya Malini ...  Ranjanam

===Crew=== Bapu
* Writer: Mullapudi Venkata Ramana
* Producer: U. Suryanarayana Raju
* Production executive: Jayakrishna
* Production Company: Gopi Krishna Combines
* Original Music: Chellapilla Satyam
* Cinematography: V. S. R. Swamy
* Film Editing: Mandapati Ramachandraiah
* Art Direction: Vaali and V. Bhaskara Raju
* Camera Operator: S. Gopal Reddy
* Assistant Cameramen: M. V. Raghu and Sharath
* Playback singers: S. Janaki, S. P. Balasubramaniam, P. Susheela and V. Ramakrishna

==Production==
For songs recording, the unit went to tribal areas and composed their tunes and rhythms as songs for the film. In fact, the Enniyallo song was based on the research we have done. 

==Songs==
* "Aakasham Dinchala Nelavanka Tunchala" (Lyrics: Veturi; Singers: V. Ramakrishna and P. Susheela; Cast: Krishnam Raju and Vanisree)
* "Kanda Gelichindi Kanne Dorikindi" (Lyrics: C. Narayana Reddy; Singers: V. Ramakrishna and P. Susheela; Cast: Krishnam Raju, Vanisree and Others)
* "Om Namassivaya Srikantalokesha" (Lyrics: Veturi; Singer: V. Ramakrishna; Cast: Krishnam Raju and M. Balayya)
* "Paravasamuna Sivudu" (Kiratarjuneeyam) (Lyrics: Veturi; Singer:   and M. Balayya)
* "Siva Siva Ananelara" (Lyrics: C. Narayana Reddy; Singer: S. Janaki)
* "Siva Siva Sankara Bhaktava Sankara" (Lyrics: Veturi; Singer: V. Ramakrishna; Cast: Krishnam Raju)
* "Thalli Thandri" (Singer: P. Susheela)
* "Thinavayya"
* "Thakita Thakita" (Singer: S. P. Balasubrahmanyam)
* "Yenniyallo Yenniyallo Chandamama" (Lyrics: Arudra; Singers: V. Ramakrishna and P. Susheela; Cast: Krishnam Raju and Vanisree)

==Box-office==
The fim is a hit. 

==Awards==
* National Film Award for Best Audiography - S. P. Ramanathan

==References==
 

==External links==
*  

 
 
 
 
 
 

 