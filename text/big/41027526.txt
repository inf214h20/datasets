Plane Dippy
 
{{Infobox Hollywood cartoon|
|cartoon_name=Plane Dippy
|series=Looney Tunes
|image=
|caption= Fred Avery
|story_artist=
|animator=Sid Sutherland
|voice_actor=Billy Bletcher Joe Dougherty Jack Carr
|musician=Norman Spencer Bernard Brown
|producer=Leon Schlesinger
|distributor=Warner Bros.
|release_date=April 30, 1936 (USA)
|color_process=Black-and-white (later colorized in 1995)
|runtime=8 minutes
|movie_language=English
|preceded_by=
|followed_by=
}}
 1936 Looney Beans makes a cameo drawing a line on the floor during the "Spinning Test" sequence, Beans appearance before in Westward Whoa.
It is the last cartoon featuring Little Kitty.

==Plot== Porky is looking to join the military. He briefly considers the Armys infantry division and the Navy before deciding to join the Air Corps. He applied to one of the jobs. The sergeant (similar to MGMs Spike the Bulldog|Spike) sends Porky through a series of tests, which he fails disastrously. When the other soldiers are being issued rifles, Porky is issued a feather duster and ordered to clean a voice-activated robot plane. Meanwhile, Little Kitty is playing with a puppy, and the planes control unit picks up her voice. The plane takes Porky on an incredibly wild ride. It destroys a military balloon (the crew parachute to safety). It levels a building except for the clock tower. It crashes through a circus tent, causing trapeze performers to do tricks on his plane. It goes through the ocean, chasing a fish and getting chased in turn by a whale. It even crashes into a wagon load of hay, turning the cargo into straw hats. It nearly destroys several other planes, but they nimbly escape. Finally, a number of other children show up and shout constant commands at the puppy, causing the plane to go totally berserk. Finally, the exhausted puppys owner tells him to come home, and the plane does so, crashing into the hangar. Porky goes racing from the building and dashes into the office of the infantry division, proclaiming that he wants to "l-l-learn to m-m-march". The cartoon ends with Porky carrying a rifle and marching in formation with a number of other soldiers.

==References==
 

==External links==
* 
* 
 
 

 
 
 
 
 
 
 


 