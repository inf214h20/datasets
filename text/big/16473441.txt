Take a Deep Breath (film)
{{Infobox film
| name = Diši duboko/ Take a Deep Breath
| image = disi_duboko.jpg
| caption = DVD cover Dragan Marinković
| writer = Hajdana Baletić
| producer = Danica Milojković
| production = Norga Investment Inc. & DV Solution
| starring = Mira Furlan Bogdan Diklić Ana Franić Jelena Đokić Nikola Đuričko
| music = Vladan Marković
| sound design = Zoran Maksimović Ognjen Popić
| sound mix = Branko Neškov
| costume = Jasmina Sanader
| make-up = Stanislava Zarić
| scenography = Biljana & Dragan Sovilj
| director of photography = Boris Gortinski
| released = November 9, 2004 (Serbia|SRB)
| runtime = 80 min.
| country = Serbia and Montenegro Serbian
}} Dragan Marinković and written by Hajdana Baletić. This drama was promoted as the "first Serbian LGBT feature film", even though the writer herself stresses that it is more about the generation gap in the modern family.  The premiere was held on November 8, 2004 in Belgrades Sava Center.

== Tagline ==
The keynote quotes American comedian George Carlin:
"... And always remember: life is not measured by the number of breaths we take, but by the moments that take our breath away..."

== Plot == Belgrade Law School student who is still living with her parents, informs them about her decision to follow her boyfriend Stefan (Branislav Tomašević) to Canada since he had already accepted a job offer to coach water polo there. The parents who thought the young couple were about to announce their intention to get married can hardly hide their shock and disappointment at the unexpected turn of events. Sašas father Miloš (Bogdan Diklić), a conservative judge, is particularly not happy with the fact that shes leaving her studies. Saša also tells them that shes moving in with Stefan right away, even before her visa application is processed. The young couple then leaves the dinner in a bit of a huff.

While driving back to Stefans apartment, they get into a car accident. Luckily both survive: Saša only with scratches and Stefan with minor head and leg injuries that require hospitalization. In the hospital Saša meets Stefans sister Lana (Jelena Đokić), a charming Paris-based photographer whos come to Belgrade in order to take care of her injured brother. While Saša gets released from the hospital the same day she wakes up, Stefan is being held for a few more weeks.

While he recovers, his sister Lana seems to have forgotten the original intention of her arrival to Serbia, and spends all her time with Saša. Saša, who initially finds Lana to be very irritating, is being slowly seduced by her, and goes through the path of anger, denial, acceptance and outing in the time span of only few days.

Her parents, busy with their own separation and divorce, dont notice their daughters love life confusion right away - more precisely, they have to be told so. Both of them react very differently. Mother (Mira Furlan), who has just realized that life should not be wasted in compromises but should be lived to the fullest, is very happy that her daughter has finally found her true love. On the other hand, Sašas father, who tries to keep his family together at all costs, becomes very unstable and angry after hearing these news, which for him represent the last drop.

Releasing all his suppressed anger, he has Lana followed and arrested for one night on a traffic violation account, which results in Lanas change of spirit - from a joyful but a little superficial and inconsiderate girl she becomes a crushed pessimist, decided to escape the troublesome situation. She confronts Stefan and breaks up with Saša by telling her that "it was a nice dream" and that "she wishes for her to fall in love again", suggesting that she herself could not love again. She leaves for Paris, and things eventually work out for Stefan, who splits for Canada with a nurse from the hospital, and for Sašas mother too, who moves to Vienna with her new husband and her little stepdaughter. Sašas father dies on, so to say, the consequences of his bogus heart condition and Saša is left completely alone to stand still in a world where everybody else seems to be going places, even though in different directions.

A viewer is being left with a distinct feeling of "plots behind the plot", i.e. there are several stories from the life of the protagonists, which are implied to but not quite revealed. Those are for example: the story about Stefans and Lanas childhood and their relationship in general; the story about Sašas fathers youth and marriage; the relationship between Sašas father and his assistant; and the most important - what has happened that night in prison and why has Lana decided to drop the fight so quickly. Although it may sound as a disadvantage of the scenario, it actually complements it (except, maybe, the unclearance of Lanas mindchanging).

== Cast ==

* Ana Franić as Saša
* Jelena Đokić as Lana
* Mira Furlan as Lila
* Bogdan Diklić as Miloš
* Branislav Tomašević as Stefan
* Nikola Đuričko as Bojan
* Goran Šušljik as Siniša
* Ana Sakić as Ines Bojan Dimitrijević as Zoran
* Jelena Helc as the head nurse
* Tatjana Torbica as nurse
* Danijela Mihajlović as cleaning lady
* Mihajlo Bata Paskaljević as grandpa
* Petar Kralj as doctor
* Milan Marić as Miki

== Soundtrack ==

The most popular song from the film is a composition "Ja te sanjam" (I Dream About You), by Vladan Marković and performed by Jovana Nikolić.

== References ==

 
*  

== External links ==

*  
*   at BaLConn
*  

 
 
 
 
 