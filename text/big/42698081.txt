Parwana (1947 film)
{{Infobox film
| name           =Parwana
| image          =Parwana_1947.jpg 
| image_size     = 
| caption        = 
| director       =J. K. Nanda
| producer       =R. B. Haldia
| writer         =D. N. Madhok
| narrator       = 
| starring       =K. L. Saigal Suraiya Nazma K. N. Singh Baby Khurshid
| music          =Khurshid Anwar
| lyricist       =D. N. Madhok Naqshab
| cinematography =Dilip Gupta 
| editing        =N. R. Chowhan 
| distributor    =
| released       = 1947
| runtime        = 92 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| website        = 
}}
 1947 Hindi drama film. It was produced by R. B. Haldia and directed by J. K. Nanda. The songs were composed by Khurshid Anwar and became popular.    The lyrics were written by D. N. Madhok and Naqshab.  The film starred K. L. Saigal, Suraiya, Najma, K. N. Singh, Baby Khurshid and Azurie.    It was K. L. Saigals last film and released several months after his death in January 1947. Suraiya who had stated in an interview that she was a big fan of Saigals, had already acted in two films with him.    The first two were Tadbir (1945) and Omar Khaiyyam (1946). Parwana was their third film together and the most famous.    

The story is about Inder played by K. L. Saigal, who is from a well to do family and considerate towards the poor and needy. This causes jealousy and misunderstanding in his life. 

==Story==

Inder (K. L. Saigal) goes to the market-place and buys bananas along with the basket from a blind banana seller, which he gets distributed among the poor. He then leads the blind man to his house. He’s concerned about the old man’s young daughter Sakina (Baby Khurshid) who’s been unwell. He treats her as his sister and feels she should be shown to a doctor for her daily fever. Sakina makes wooden toys and she has made some for him. Inder pays her saying he’ll sell them to a shopkeeper from where he will get money. Inder stays with his rich father, mother and three small brothers. The father’s concerned about Inder’s benevolent nature and tells his wife that it’s time to get him married. The mother shows Inder a photo of Rupa (Nazma). Inder wants to meet Rupa and talk to her before getting married but Rupa’s father refuses. He says Rupa sees Inder through her father’s eyes and after marriage will see the world through her husband’s, that is, Inders eyes. Inder is not happy but he leaves. He and Rupa are married and he is enthralled by her beauty.

Sakina has made a shawl for Inders wife as a wedding gift and she asks Inder to present it to her. However, Rupa is suspicious of Inder’s staying out late and thinks the gift is actually for Inder. She tells him that he is a debauch spending time with loose character women who sell themselves for the price of a shawl. A message arrives for him that Sakina is not well. He insists on bringing her home in the hope of taking good care of her. Rupa sees a young woman who is Sakina’s Aunt accompanying them in the buggy and jumps to the conclusion that she’s the woman called Sakina with whom Inder spends his time. While Inder goes to ring for the doctor Rupa taunts Sakina about her Aunt and in the argument that follows slaps Sakina. Inder drops Sakina back home but himself goes into a state of shock. The doctor suggests a hill station for him to recover. His condition is further worsened when he finds out that Sakina has died. He refuses to interact with anyone and stops talking to his wife. With the love and pressure from his younger brothers he agrees to go to the hill station.

Gopi (Suraiya) lives with her brother Kishan (K. N. Singh) and his wife in a village up in the hills. It is to this place that Inder and Rupa arrive. Kishan suggests that they stay in part of their big house as it will help them monetarily. Gopi and Inder grow close as Rupa asks her to give Inder his medication and take him for walks so he can get better and he also enjoys her company. Soon there is gossip in the village about Inder and Gopi, which Kishan overhears. He realises that his sister is in love with Inder. He confronts Inder who admits he loves Gopi but not in the way they think. He has admired her free spirit and zest for life. He decides to leave the village for the city. Once back he again falls ill. The doctor says he’s in danger of losing his mind and could probably die. Rupa goes to the village to fetch Gopi as she feels that only she can make Inder better. Rupa has finally understood that Gopi had taken Sakina’s place in Inder’s heart.  After some hesitation Kishan agrees to let Gopi go and follows her. In the city Rupa confronts Inder who she thinks is in love with her but is scared to acknowledge it. Inder’s parents come into the room and insult and slap Gopi. Kishan who has heard everything takes her back to the village and prepares to get her married. On the wedding day Inder and Rupa arrive to bless the couple but there is a showdown between Gopi, Kishan and Inder where Gopi falls from the cliff and dies.

==Cast==

*K. L. Saigal: Inder
*Suraiya: Gopi
*Nazma: Rupa
*K. N. Singh: Gopi
*Azurie
*Baby Khurshid: Sakina
*S. Nazir
*Sharma

==K. L. Saigal and Songs==
 Shah Jehan (1946) after asking Saigal to sing without having alcohol, which Saigal did after some reluctance and uncertainty on his part.    The news spread and Khurshid Anwar then got Saigal to do the same with the songs in Parwana.    The music became very popular. Saigal’s songs had personified the musical era of the 1930s and 1940s, which was also known as the Saigal Era.   


{| class="wikitable"
|-
! Number !! Song !! Singer !! Lyricist
|- Biyahi Meri Rani Havaldar Ke Sangh || ||D. N. Madhok
|- Toot Gaye Sab Sapne ||K. L. Saigal||D. N. Madhok, Naqshab and Tanvir Naqvi	
|- Paapi Papiha Suraiya  ||D. N. Madhok
|- Saiyyan Ne Raj Kumari||D. N. Madhok  
|- Jeene Ka Dhang ||K. L. Saigal || D. N. Madhok
|- Us Mast Nazar Par/ Kahin Ulajh Mat Jaana ||K. L. Saigal || D. N. Madhok
|- Mohabbat Mein Kabhi Aisi Bhi Haalat ||K. L. Saigal || D. N. Madhok, Naqshab
|- Aaja Baalma Suraiya || D. N. Madhok
|- Jab Tum Suraiya ||D. N. Madhok,  Naqshab
|- Mere Mundere Suraiya ||D. N. Madhok
|- Mora Maika Chhudaiyo Dino Ram ||K. L. Saigal ||D. N. Madhok
|-
|}

==References==
 

==External Links==
*  
*  Copyright Khwaja Khurshid Anwar Trust and Irfan Anwar. Hindi Film Songs Surjit Singh.
* 



 
 
 