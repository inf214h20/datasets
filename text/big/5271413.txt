Toxic (film)
 
{{Infobox film
| name           = Toxic
| image          = Toxic2010Cover.jpg
| alt            =  
| caption        = DVD cover
| director       = Alan Pao
| producer       = Brian Hartman Corey Large Alan PaoBrian Hartman Corey Large Alan Pao
| writer         = Corey Large Alan Pao
| starring       = Master P Corey Large Dominique Swain Danny Trejo Damion Poitier Susan Ward Charity Shea
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = English
| budget         = $2 million
| gross          = 
}}
Toxic is a 2010 thriller film. It was released direct-to-video on July 8, 2010. The movie is told with past and present cut up and mixed together, rather than chronologically.  The story focuses on a girl name Lucille, who has a mental illness. After she kills a man, her mind takes on his identity and she starts to live life truly believing she is him.

==Plot==
Lucille has a mental disorder, which stems from her brothers death, which occurred when she was young, and which her father blamed her for, never forgiving her. It is never revealed how the brother died or why her father blames her but it made her mentally unstable.

While Lucille is telling Angel who her father is, Nadine rolls off the top of the building and kills herself. This leads Sid and Antoine to the brothel where they ask about Lucille. Angels goons attack them and soon Angel and more goons show up which causes an even bigger gun battle. 

From that moment on the character Lucille becomes the character Sid, the one we see working in the bar. The Sid that was looking for Lucille and was in the gun battle is dead and the Sid that is working in the bar and falling for Michelle is Lucille who believes she is Sid. For the rest of the plot, "Sid" is referring to Lucille as Sid and not the actual man. Also, all the other characters see Sid as Lucille, as a girl, and it is only Lucille that sees her reflection as that of Sid, a man.

Lucille, as Sid, leaves the apartment and starts walking down the road where Steve finds her and picks her up. He allows Sid to stay in the attic of his strip club in exchange for working there. Gus comes in one night and is shocked to see Lucille but surprised that she thinks she is Sid. He decides not to tell her directly and instead tries to use his psychology training to pull Lucille out again. Sid later begins to see things as Lucilles personality tries to "retake" her body and mind. 
Michelle decides to do some digging during this and calls Sid to tell him that she found some information on the girl he is "seeing". 

Gus shows up just then and Sid points the gun at him, asking what he did to Lucille. Gus says Lucille is dead because she couldnt live with what she had done.

==Cast==
*Susan Ward as Michelle
*Corey Large as Sid
*Master P as Angel
*Damion Poitier as Angels Thug
*Danny Trejo as Antoine
*Dominique Swain as Nadia
*Charity Shea as Lucille
*Bai Ling as Lena
*Kitana Baker as Crystal
*Mitchell Baker as Simon
*Steven Bauer as Conrad
*Bianca Chiminello as Raven
*Nick Chinlund as Rob Deluca
*James Duval as Brad
*John Enos III as Travis
*Mark Hewlett as DJ Lodgikal
*C. Thomas Howell as Joe
*Sally Jackson as Dr. Gowman
*Shar Jackson as Daphne
*Paul Johansson as Gus
*Nicole Marie Lenz as Rizzo
*Costas Mandylor as Steve
*Holt McCallany as Van
*Sandra McCoy as Jaymee
*Lochlyn Munro as R.M.
*Brande Roderick as Cherry
*Tom Sizemore as Van Sant
*Tabitha Stevens as Stripper
*Johann Urb as Greg
*Dominique Vandenberg as Stone
*Cerina Vincent as Malvi
*Jennifer Walcott as Stripper
*Luke Flynn as Max

== External links ==
*  

 
 