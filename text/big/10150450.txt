The Dove (1974 film)
{{Infobox film
| name           = The Dove
| image          = Thedovefilmposter.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Charles Jarrott
| producer       = Gregory Peck
| screenplay     = Peter S. Beagle Adam Kennedy
| based on       =  
| starring       = Joseph Bottoms Deborah Raffin John Barry
| cinematography = Sven Nykvist
| editing        = John Jympson
| distributor    = Paramount Pictures
| released       =   
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} biographical film directed by Charles Jarrott. The picture was produced by Gregory Peck, the third and last feature film he would produce.  Pecks gamble
Mills, Bart. Chicago Tribune (1963-Current file)   09 June 1974: h64. 

The drama is based on the real life experiences of Robin Lee Graham, a young man who spent five years sailing around the world as a Single-handed sailing|single-handed sailor, starting when he was 16-years old. The story is adapted from Dove (1972), the book Graham co-wrote with Derek L.T. Gill about his seafaring experiences.

==Plot== circumnavigate the world. He had planned the long trip with his sailor father Lyle Graham (John McLiam) for years.

He sets sail on his journey and on one of his stops he meets and falls in love with the gregarious and attractive young woman, Patti Ratteree (Deborah Raffin). After much banter, Patti decides to follow Graham throughout his long journey. She meets him in Fiji, Australia, South Africa, Panama, and the Galápagos Islands.
 Los Angeles with crowds welcoming him home.

==Cast==
 
 
* Joseph Bottoms as Robin Lee Graham
* Deborah Raffin as Patti Ratteree
* John McLiam as Lyle Graham
* Dabney Coleman as Charles Huntley John Anderson as Mike Turk
* Colby Chester as Tom Barkley
* Ivor Barry as Kenniston
* Setoki Ceinaturoga as Young Fijian
 
* Reverend Nikula as Minister
* Apenisa Naigulevu as Cruise Ship Captain
* John Meillon as Tim
* Gordon Glenwright as Darwin Harbour Master
* Garth Meade as South African Customs Official
* Peter Gwynne as Fred C. Pearson
* Cecily Polson as Mrs. Castaldi
 

==Background==

===Basis of film===
 
Robin Lee Graham (born 1949) is a man who set out to sail around the world alone as a teenager in the summer of 1965. National Geographic Magazine carried the story, and he co-wrote a book detailing his journey called Dove. Graham was just sixteen when he set out from Southern California and headed west in his 23-foot boat. He got married along the way, and after almost five years, sailed back into his home port. After he and his wife Patti attended Stanford University, they moved to Montana and settled down.

===Filming locations===
The film is a travelogue of sorts and the producers filmed on location throughout the world. Filming locations include: Cape Town, South Africa; Darwin, Northern Territory, Fremantle, Western Australia; Ecuador; Fiji; Los Angeles, California; Maputo|Lourenço Marques, Mozambique; Panama Canal, Panama; South Africa; and Suva, Fiji. 

==Reception==

===Critical response===
Critic Nora Sayre, film critic for The New York Times, thought the film was too wholesome, so much so that Sayre wanted harm to come to the characters. Yet she appreciated Sven Nykvists cinematography and wrote, "The Dove ... is probably far too wholesome for most of the families I know, although there may be a radiant audience lurking just outside the realms of my acquaintance...Joseph Bottoms, as the young sailor, smiles too much in the first half of the movie; after that, he cries too much. His initial overwhelming sunniness turns the viewer into a sadist: Youre glad when his cat gets killed or grateful when a shark appears in the ocean. Deborah Raffin, as his winsome girlfriend, is rarely allowed to stop laughing and wagging her head; the two grin and glow at each other until you yearn for a catastrophe." 

Others liked the film. Film critics Frederic and Mary Ann Brussat, who reviewed the film much later after the films release on their website Spirituality and Practice, appreciated the film and its message, and wrote, "Producer Gregory Peck was perceptive when he decided to make a film based on the true life on the youngest person to circumnavigate the world alone...Grahams exploits and his accompanying struggle to sort out his feelings about himself and his loyalties to family and girlfriend are fascinating and provocative." 

The staff at Variety (magazine)|Variety magazine said, "...an odyssey which provides nautical chills and thrills (as well as breathtaking scenics) aplenty...Pic really takes off when he meets the girl (played with gauche hesitation at first, but then with beauty and considerable charm by Deborah Raffin) ...Their yes-no yes-no-yes affair is nicely handled." 

===Accolades===
Wins
* Golden Globe Award: Golden Globe; Most Promising Male Newcomer, Joseph Bottoms; 1975.

Nominations
* Golden Globes: Golden Globe; Best Original Song, John Barry (composer) and Don Black (lyricist); for the song "Sail the Summer Winds"; 1975.

==Distribution==
The film opened in the United States in September 1974. Paramount released a video of the film on April 16, 1996. A DVD of the film has not been released.  It is available on Netflix for online viewing.

==Soundtrack==
  John Barry. A illegal CD version of the soundtrack with the 13 tracks was released January 28, 2009 by Harkit Records UK.

;Side 1
# "The Dove (Main Title)" (03:05)
# "Sail The Summer Winds" (Vocal by Lyn Paul) (03:09)
# "Hitch-hike To Darwin" (02:14)
# "Patty and Robin" (02:20)
# "Here There Be Dragons" (02:44)
# "Mozambique" (02:15)

;Side 2
# "The Motorbike and the Dove" (01:24)
# "Xingmombila" (02:09)
# "Alone On The Wide, Wide Sea" (03:52)
# "Porpoise Escort" (02:30)
# "After The Fire" (01:46)
# "Sail The Summer Winds" (Vocal by Lyn Paul) (02:21)
# "The Dove (End Title)" (01:54)
 Intrada released official CD premiere of the score, newly re-mixed and re-mastered from original 8-channel session masters  (Harkit bootleg edition was LP to CD transfer).
;Track list
Original 1974 soundtrack album 
1) The Dove (Main Title) (3:05) 
2) Sail The Summer Winds+ (3:11) 
3) Hitch-Hike To Darwin (2:14) 
4) Patty And Robin (2:20) 
5) Here There Be Dragons (3:09) 
6) Mozambique (2:16) 
7) The Motorbike And The Dove (1:24) 
8) Xing’mombila (2:10) 
9) Alone On The Wide, Wide Sea (3:52) 
10) Porpoise Escort (2:31) 
11) After The Fire (1:50) 
12) Sail The Summer Winds+ (2:21) 
13) The Dove (End Title) (1:55) 
The Extras – Stereo Album Mixes (No EFX) 
14) Xing’mombila – Part 1 (No EFX) (0:25) 
15) Xing’mombila – Part 2 (No EFX) (0:33) 
16) The Dove (End Title) (No EFX) (1:49) 
The Extras – Previously Unreleased Mono Score Cues 
17) Sorta Romantic (1:14) 
18) Rotten Cat (0:20) 
19) Starting Again (2:28) 
20) Near Miss (0:22) 
21) From The Depths (2:17) 
22) Unknown Seas (1:12) 
23) Alone On The Wide, Wide Sea (Complete) (5:00) 
24) His Decision (3:11) 
+Lyricist: Don Black - Vocalist: Lyn Paul

==References==
 

==External links==
*  
*  
*  
*   at the BlueMoment web site
*   the main title of film
*   "Sail The Summer Winds" vocal by Lyn Paul

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 