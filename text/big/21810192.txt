Salt (2010 film)
 
{{Infobox film
| name           = Salt
| image          = Salt film theatrical poster.jpg
| caption        = Theatrical poster
| alt            = A womans face in a shadowy environment. The word SALT is in the center, below it the question "Who is Salt?"
| director       = Phillip Noyce
| producer       = Lorenzo di Bonaventura Sunil Perkash
| writer         = Kurt Wimmer
| starring       = Angelina Jolie Liev Schreiber Chiwetel Ejiofor Daniel Olbrychski August Diehl John Gilroy
| music          = James Newton Howard
| cinematography = Robert Elswit
| studio         = Di Bonaventura Pictures Wintergreen Productions Rainmaker Digital Effects
| distributor    = Columbia Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English Russian
| budget         = $110 million 
| gross          = $293.5 million 
}}
 action Thriller thriller spy film directed by Phillip Noyce, written by Kurt Wimmer, and starring Angelina Jolie, Liev Schreiber, Daniel Olbrychski, August Diehl and Chiwetel Ejiofor. Jolie plays Evelyn Salt, who is accused of being a Russian sleeper agent and goes on the run to try to clear her name.

Originally written with a male protagonist, with Tom Cruise initially secured for the lead, the script was ultimately rewritten by Brian Helgeland for Jolie. Filming took place on location in Washington, D.C., the New York City area, and Albany, New York|Albany, New York, between March and June 2009, with reshoots in January 2010. Action scenes were primarily performed with practical stunts, computer-generated imagery being used mostly for creating digital environments.

The film had a panel at the San Diego Comic-Con on July 22 and was released in North America on July 23, 2010, and in the United Kingdom on August 18, 2010. Salt grossed $294 million at the worldwide box office and received generally positive reviews, with praise for the action scenes and Jolies performance, but drawing criticism on the writing, with reviewers finding the plot implausible and convoluted. The DVD and Blu-ray Disc were released December 21, 2010, and featured two alternate cuts providing different endings for the film.

==Plot== arachnologist Mike Krause (August Diehl), generates so much publicity that the CIA is forced to arrange a prisoner exchange, despite the agencys policy against it.  Salts CIA colleague Ted Winter (Liev Schreiber) greets Salt at the border.  As they drive away, Mike proposes, despite Salt admitting that she is in fact a CIA operative.
 Olek Krupa) American Vice President. Orlov reveals that KA-12 is named Evelyn Salt, and lie detectors confirm his entire story. Peabody orders Salt be detained, but Orlov kills two agents and escapes.  In the confusion, Salt is able to escape as well, running barefoot through the street. While she is being chased, she discovers that Mike was kidnapped.

Later, at the funeral, Salt appears to succeed in killing President Matveyev, and then surrenders herself. Matveyev is declared dead. Salt escapes again and heads to a barge where Orlov is hiding with other sleeper agents.  In a series of Flashback (narrative)|flashbacks, Salt recalls growing up in the Soviet Union and being trained with other children.  On the barge, Orlov welcomes her back and has Mike killed in front of her.  When Salt shows no reaction, Orlov is convinced she is loyal and begins briefing her on her next mission.  She is to rendezvous with another KA who will help her assassinate the American president. Salt then kills Orlov and all of the other agents on the barge. She then meets with KA Shnaider (Corey Stoll), who uses his cover as a NATO liaison to get Salt into the White House. Once inside, Shnaider launches a suicide attack to force agents to move the President (Hunt Block) to an underground bunker, accompanied by Winter. Salt follows them and manages to enter the bunker before it is sealed.

The US President learns that Russia has mobilized its nuclear arsenal in response to their presidents death. He orders American nuclear weapons readied in response. CIA Agent Winter then suddenly kills everyone except the President and introduces himself as Nikolai Tarkovsky, another KA.  Winter incapacitates the President and begins aiming nuclear missiles at  , with Salt aborting the missile strikes before being captured.  

As Salt is being led out in chains, Winter grabs a pair of scissors, apparently to attack her or to defend himself if necessary. She unexpectedly hooks her chain around Winters neck and jumps over the stair railing, choking him to death.  On the helicopter ride to be interrogated, Peabody questions her.  Salt explains that she killed Winter because he was a KA, and that she is loyal to the U.S.. Peabody is convinced after receiving a text that Salts fingerprints were found on the barge where the sleeper agents were killed, supporting her story. Salt is allowed to escape, jumping out of the helicopter into the river below and escaping into the woods, planning to hunt down the rest of the KA sleeper agents that are still out there.

==Cast==
 
* Angelina Jolie as Evelyn Salt/Natasha Chenkova
* Liev Schreiber as Theodore "Ted" Winter/Nikolai Tarkovsky
* Chiwetel Ejiofor as (Darryl) Peabody. His forename is not mentioned in the film. 
* Daniel Olbrychski as Oleg Vasilyevich Orlov
* August Diehl as Michael Krause
* Daniel Pearce as Young Orlov
* Hunt Block as US President Howard Lewis
* Andre Braugher as Secretary of Defense Olek Krupa as Russian President Boris Matveyev
* Cassidy Hinkle as (young) Natasha Chenkov
* Corey Stoll as Shnaider
* Vladislav Koulikov as Chenkovs father
* Olya Zueva as Chenkovs mother
* Kevin ODonnell as Young CIA officer
* Gaius Charles as CIA officer
 

==Production==

===Development and writing===
The early development of the script began while Kurt Wimmer was doing interviews promoting Equilibrium (film)|Equilibrium. In a November 2002 interview, he discussed what scripts he was working on.  He stated that "I have several scripts – foremost of which is one called The Far-Reaching Philosophy of Edwin A. Salt – kind of a high-action spy thriller..."  
In another interview, Wimmer described the project as "very much about me and my wife". 
The plot incorporated many elements from Equilibrium, with an oppressive and paranoid political system of brainwashing that gets overthrown by one of its high-ranking members who rebels due to an emotional transformation.  With the shortened title Edwin A. Salt, the script was sold to Columbia Pictures in January 2007.  By July 2007, the script had attracted the attention of Tom Cruise. 

Terry George was the first director to join the project, and he also did some revisions to the script, but he soon left the project.  Peter Berg was the next director to consider, but he too, eventually dropped out for undisclosed reasons.   A year later it was confirmed that Phillip Noyce would direct.  Noyce was attracted to Salt for its espionage themes, which are present in most of his filmography,  as well as the tension of a character that tries to prove his innocence yet also does what he was previously accused of. 

===Casting===
 . |alt=Liev Schreiber]]
Initial discussions took place in 2008 between   character Ethan Hunt.  Cruise decided to work on  Knight and Day instead. The filmmakers tried to differentiate the character from Hunt, but eventually came to accept they were too similar and decided not to change the characteristics of Salt. Noyce said "But, you know, he had a valid point. It was kind of returning to an offshoot of a character that he’d already played. It’s like playing the brother, or the cousin, of somebody that you played in another movie". 
 spy franchise. James Bond himself instead.  Jolie was sent Salts script in September 2008 and liked it. Wimmer, Noyce, and producer Lorenzo di Bonaventura went to visit Jolie at her home in France to discuss a possible script and character change. Writer Brian Helgeland helped with the character development and dialogue of the script based on the notes that came out of those discussions with Jolie and to accompany the gender change, the title characters name was changed to Evelyn Salt. 
 Bond films Bourne films".  
 Dirty Pretty Things, seemed to have the "intelligence and disarming sort of obsessiveness" that a counter-intelligence officer would need.  August Diehl, who played Salts husband Mike Krause, came after a recommendation from Jolies partner Brad Pitt, who had worked with Diehl in Inglourious Basterds, and Daniel Olbrychski was chosen for Orlov because Andrei Konchalovsky told Noyce that such an evil Russian character could only be played by a Polish actor. 

===Filming===
On a budget of $110&nbsp;million, principal photography took place mostly on location in New York and Washington, D.C.   from March&nbsp;to&nbsp;June&nbsp;2009.    Noyce decided to avoid "typical post-card views of Washington DC" to reflect "the more Albany on Washington Heights on 157th St and Riverside Dr. Some scenes were also filmed outside of Manhattan including The Bronx, Queens, Staten Island, and in Westchester County.

After Jolie had just given birth to twins,  she spent time training before filming to get fit in order to perform almost all of the stunts herself. Bonaventura said, "She is so prepared and so ready and gung-ho, shell do any stunt. We had her jumping out of helicopters, shooting, jumping off of all sorts of things and infiltrating places that are impossible to infiltrate".  Salts fighting style was described as a mixture of Muay Thai, which was considered by the stunt team the most fit for Jolies physique, and Krav Maga, for its rawness and aggressiveness. Noyce wanted to film the scene where Salt hangs from the edge of the building in a studio with chroma key, but Jolie insisted on doing it herself in the actual location.  On May&nbsp;29,&nbsp;2009, filming was temporarily halted after Jolie suffered a minor head injury during filming an action scene. She was taken to a hospital as a precautionary measure and released on the same day with no serious injuries, allowing filming to resume.  Salts escape after being captured in St. Bartholomews originally involved jumping her off a building into a window cleaning machine, but budgetary constraints caused the scene to be changed into a car chase. 
 CIS Vancouver and Framestore. CIS Vancouver recreated the White House since the crew did not have permission to shoot in the building, and made a digital elevator shaft for the scene where Salt goes down into the White House bunker.  Framestore was responsible for the assassination attempt on the Russian president, which combined actual shots of St. Bartholomews Church, a digital recreation of the churchs interior, and scenes with actor Olek Krupa falling down a collapsing floor.  

Female CIA officers were consulted about the creation of disguises, leading to the scene where Salt undergoes subtle changes to disguise herself as a Puerto Rican. The "sweet and caring" blonde Salt dyeing her hair black would represent the shift to Chenkov, the menacing Russian agent. For the scene where Salt disguises herself as a Major, pictures of Angelina Jolie were treated on Adobe Photoshop to create a believable male version, with the resulting image being used by the make-up team as an inspiration for the Prosthetic makeup|prosthetics. 

===Versions===
Director Phillip Noyce has said that due to the extensive usage of flashbacks, "there was always going to be a mountain of alternative material that would not fit into the theatrical version."  The film ended up having two extra versions, the Directors Cut and the Extended Cut – which Noyce refers to in his audio commentary as the films original cut – both included on the DVD and Blu-ray Disc deluxe editions. 

The Directors Cut was described by Noyce as "my own personal take on the material, free from the politics and restrictions of producers, studio or censorship ratings."  Four minutes of film are added, leading to a running time of 104 minutes.  More flashbacks are added, and the violence is amped up – for example Mike being drowned rather than shot to death.  The ending is also different: in the bunker scene, Winter shoots the President instead of only knocking him unconscious,   and a media report during the final scene reports that the new US President had been orphaned on a family visit to Russia, implying he is also a sleeper agent.  Noyce has described this ending as "an ending yet just a beginning – and its an ending that turns the whole story on its head". 

The Extended Cut increases the running time by only one minute, but rewrites the plot by removing, rearranging and adding scenes.  The ending has Salt escaping custody from the CIA and going to Russia, where she kills Orlov – his death scene at the barge does not appear in this cut – and destroys the facility where new child spies are being trained. 

== Soundtrack ==
 
{{Infobox album  
| Name        = Salt: Original Motion Picture Soundtrack
| Type        = Film score
| Artist      = James Newton Howard
| Cover       =  
| Released    =   (iTunes)   (Compact Disc|CD)
| Recorded    = 2010 Contemporary classical
| Length      =  
| Label       = Madison Gate Records
| Producer    = James Newton Howard The Last Airbender
| This album  = Salt
| Next album  = 
}}
Salt: Original Motion Picture Soundtrack was released on July 20, 2010 on iTunes  and on August 11, 2010 as on-demand CD-R from Amazon.com. The music was composed by James Newton Howard and released by Madison Gate Records. The song "Orlovs Story" includes a Russian lullaby which music editor Joe E. Rand found at Amoeba Music, and which served for inspiration for the choir heard in other tracks – but the chants in the rest of the score are only random syllables, as Rand and Howard thought actual Russian words would spoil about Salts allegiance. 

;Track listing
{{Tracklist
| collapsed       = 
| headline        = 
| total_length    = 59:10
| all_writing     = James Newton Howard
| title1   = Prisoner Exchange
| length1  = 4:09
| title2   = Escaping the CIA
| length2  = 5:20
| title3   = Cornered
| length3  = 1:09
| title4   = Orlovs Story
| length4  = 4:43
| title5   = Chase Across DC
| length5  = 6:51
| title6   = Hotel Room Preparations/Parade
| length6  = 3:59
| title7   = Attack On St. Barts Cathedral
| length7  = 3:10
| title8   = A Dark Goddamn Hole
| length8  = 1:47
| title9   = Taser Puppet
| length9  = 1:34
| title10  = You Are My Greatest Creation
| length10 = 4:13
| title11  = Destiny
| length11 = 2:22
| title12  = Barge Apocalypse
| length12 = 2:26
| title13  = Day X
| length13 = 1:37
| title14  = Im Going Home
| length14 = 2:16
| title15  = Eight Floors Down
| length15 = 2:51
| title16  = Arming the Football
| length16 = 2:11
| title17  = Not Safe with Me
| length17 = 2:27
| title18  = Youre About to Become Famous
| length18 = 1:38
| title19  = Mano a Mano
| length19 = 1:51
| title20  = Garroted
| length20 = 3:32
| title21  = Go Get Em
| length21 = 3:10
}}

==Release==
  episodic advergame titled "Day X Exists", where players watched webisodes and performed missions to unveil the terrorist plot.  It was released in North America on July&nbsp;23,&nbsp;2010. It was released on August 18 in the United Kingdom, despite poster advertisements suggesting it would be released on August 20.   The Deluxe Unrated Edition Blu-ray Disc and DVD was released on December 21, 2010 by Sony Pictures Home Entertainment. It includes three versions of the film: the original theatrical film and two additional unrated extended cuts not seen in theaters with two alternate endings. A Theatrical Edition DVD was also released.  In the home video charts, Salt debuted at first in the rentals and third in sales. 

===Box office===
Sony predicted an opening weekend take in the low-$30-million range, while commentators thought it would come in closer to $40 million and beat Inception for the number one spot at the box office. 
Salt opened in 3,612 theaters, with an opening day gross of US$12,532,333 – $3,470 per theater  – and on its opening weekend, $36,011,243 – $9,970&nbsp;per&nbsp;theater –  behind only Inception, which made $42,725,012 in its second weekend. Salt also grossed $15 million from 19 minor international markets.   On its second weekend, it declined in ticket sales by 45.9% making $19,471,355 – $5,391&nbsp;per&nbsp;theater and placed number&nbsp;three behind Dinner for Schmucks,  but by opening in 29 countries that same weekend, it grossed $25.4 million internationally.   Salt ended up grossing $118,311,368 in the United States and Canada and $175,191,986 in other countries, for a worldwide total of $293,503,354. 

===Critical reception===
 
Salt received generally positive reviews from critics.   , which assigns a weighted average score out of 100 to reviews from film critics, gave the film a score of 65 based on 42 reviews, indicating "generally favorable reviews."  Many reviewers pointed out the coincidence of Salt getting released shortly after the reveal of real Russian sleeper agents in the Illegals Program,    with a few comparing Salt to one of the agents, Anna Chapman.  
  in 2010 on the Salt panel. The actress performance was considered one of the films strong points.|alt=A woman in front of a microphone.]]  CGI gimmickry". Road Runner cartoon." 

Time (magazine)|Time s reviewer Richard Corliss praised the action scenes and Noyces persistence in keeping a serious tone – "he ignores the storys preposterous elements and lets the audience decide whether to laugh, shudder or both".    Empire (film magazine)|Empire s William Thomas praised Jolies performance remarking that "when it comes to selling incredible, crazy, death-defying antics, Jolie has few peers in the action business",  and Village Voice s Karina Longworth considered that original star Tom Cruise would never express the protagonists ambiguity as well as Jolie. 

Among negative responses, The New Yorkers David Denby said Salt "is as impersonal an action thriller as we’ve seen in years", finding the supporting cast underexplored – "the tricky plot locks them into purely functional responses".   Claudia Puig of the USA Today considered the film a "by-the-book thriller" with Jolies performance as the only distinguished feature.   Lawrence Toppman of The Charlotte Observer was mostly critical of the writing, describing the film as absurd, overplotted and incoherent, and saying the villainous schemes "would have been called off 20 years ago at the latest, when the Soviet Union dissolved".  Steven Rea of The Philadelphia Inquirer described Salt as "commendably swift and progressively inane", saying the script was a "sloppy concoction of story elements from 70s espionage classics" that ended up not working right with its "nonsensical setups and wildly illogical twists".  James Berardinelli of Reelviews considered that, while the film was fast-paced and the action scenes competently shot, the plot was predictable and "the spy aspects, which are by far the most intriguing elements of the movie, are shunted aside in favor of spectacular stunts and long chases". 

===Awards=== Best Sound Best Action/Adventure Golden Reel Award for Sound Effects and Foley,  a Peoples Choice Award for Favorite Action Movie,  and two Teen Choice Awards. 

==Sequel==
Director Phillip Noyce was optimistic about a sequel, saying "Hopefully within a couple of years, well have another one. Angelinas so great in this part. When audiences see the movie theyre going to feel like its only just the beginning."  Producer Lorenzo DiBonaventura also expressed further interest: "Angie, I know, loved that character, and would love to explore the character some more first and foremost."   

Noyce later said he had other projects and would not participate. "Those 3 Blu-ray Disc cuts represent just about everything I have to offer on Evelyn Salt. If there ever is a sequel, better its directed by someone with a completely fresh take on what I believe could be a totally entertaining and complex series of stories."   
 Seven Years Arthur Newman   ), as well as producers Lorenzo di Bonaventura and Sunil Perkash.   

==References==
{{Reflist|2|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

 

   

   

   

   

   

   

   

   

   

   

   

   

   

 

   

   

   

    -->

   

   

   

   

   

   

   

   

   

   

 

   

   

   

   

   

   

   
}}

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 