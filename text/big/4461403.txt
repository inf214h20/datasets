Hey, Hey, It's Esther Blueburger
{{Infobox film
|  name        = Hey Hey Its Esther Blueburger
|  image       = Esther Blueburger poster.jpg
|  caption     = Theatrical release poster
|  producer    = Miriam Stein
|  director    = Cathy Randall
|  writer      = Cathy Randall
|  starring    = Danielle Catanzariti Keisha Castle-Hughes Toni Collette
|  music       = Guy Gross Anna Howard
|  editing     = Dany Cooper Buena Vista International
|  released    = 20 March 2008
|  runtime     = 103 minutes
|  country     = Australia
|  language    = English
|  budget      =  
|  gross       =  
}} 

Hey Hey Its Esther Blueburger is a 2008 Australian   school, where she has no friends. That changes when she meets nonconformist Sunni (Castle-Hughes) from the local public school.

With Esthers character based loosely on her own adolescence, Randall was inspired to write the films script by what she saw as a lack of role models for teenage girls. In 2002, the script earned her a fellowship to the Los Angeles Film Schools Feature Development Programme, where she developed the project, and it was later picked up by Tama Films. Randall returned to Australia for casting, and Catanzariti was eventually chosen in the lead role after she attended an audition for minor roles and extra (actor)|extras. Production spanned from October to December 2006, with filming taking place in Adelaide, South Australia and Sydney, New South Wales.

The film premiered on 10 February 2008 at the Berlin International Film Festival and was released in Australia on 20 March 2008. It failed to earn back its   budget with a total domestic gross of approximately $800,000. The film attracted mixed reviews; some critics praised the originality and the acting, though others found Randalls direction dull and the script poorly written. Catanzariti won the Australian Film Institutes Young Actor Award for her performance, and the film received a further three AFI nominations and a nomination for a Film Critics Circle of Australia Award.

==Plot== Bat Mitzvah, Esther bumps into Sunni, a rebellious girl from the local public school, who she had observed and spoken to on previous occasions.

The two girls form a friendship, and Esther begins attending Sunnis school, unbeknownst to her parents, under the guise of a Swedish exchange student. She revels in the easygoing nature of the public school and enjoys spending time with Sunnis friends and Sunnis laid-back single mother, Mary, who works as a stripper. As Esther gains popularity and submits to numerous acts of peer pressure &ndash; including attacking a girl from her old school &ndash; her friendship with Sunni starts to deteriorate.  At her old school, meanwhile, her classmates have been led to believe that she was chosen for an elite social experiment, and when she returns she is treated like royalty.

Esther later discovers that Mary has died in a motorcycle accident, and a grieving Sunni is transferred to Esthers private school under her grandmothers care. Ultimately deciding that being true to herself is more important than fitting in, Esther discards her pretenses and renews her friendship with Sunni.

==Cast==
* Danielle Catanzariti as Esther Blueburger
* Keisha Castle-Hughes as Sunni Kaire
* Toni Collette as Mary
* Christian Byers as Jacob Blueburger
* Essie Davis as Grace Blueburger
* Russell Dykstra as Osmond Blueburger
* Jonny Pasvolsky as Mr. Hooper
* Cassandra Jinman as "The Slug"
* Janay Mosby as Vanessa
* Yen Yen Stender as Lissy

==Production==

===Development===
Cathy Randalls script for Hey, Hey, Its Esther Blueburger stemmed from her desire "to make a film about a kick-ass chick, a heroine for teenagers
and people of all ages", wanting to take a female twist on The Catcher in the Rye s Holden Caulfield.    She said that she had "always been struck by the fact that there are not enough role models for teenage girls".  Esthers character was drawn loosely from Randalls own adolescence; like Esther, she had a Bat Mitzvah, she had a twin brother and she attended both a private and a public school.    She explained, however, that the story was "warped and twisted and filtered through my imagination so, in fact, it doesnt feel like Esther resembles me at all". 
 AWGIE Award for Best Unproduced Screenplay.  Randalls script was subsequently picked up by producer Miriam Stein and her production company, Tama Films.  Stein said that "The script resonated with me from the start", particularly because of her similar adolescent experience as a Jewish girl who felt like an outsider.  Stein brought the film into production after recruiting Nice Pictures CEO Heather Ogilvie as executive producer, Los Angeles-based Harry Clein as associate producer and Buena Vista International to handle distribution in Australia and New Zealand. 

===Casting=== cattle call audition advertised in a local newspaper for minor roles and extra (actor)|extras.  Randall asked Catanzariti to stay behind and read a scene from the script; she went to a number of callback auditions and was later offered the main role.  Having been raised in the Catholic faith, Catanzariti took lessons in Jewish history and Hebrew to prepare for her Bar Mitzvah scenes. 
 New Zealand film Whale Rider (2002). Castle-Hughes agreed to star in the film when she first read the script at age 13, but was 16 by the time finance had been raised and filming began.     Castle-Hughes pregnancy was announced shortly before production was scheduled to begin but filming went ahead unaffected.  Toni Collette was confirmed to have joined the cast in May 2006;    her scenes were filmed over one week.   

===Filming=== St Peters College as Esthers school and Marryatville High School as Sunnis school. 

==Soundtrack==
{{Infobox album|
 | Name        = Hey, Hey, Its Esther Blueburger: Original Soundtrack
 | Type        = Soundtrack
 | Artist      = Various Artists
 | Cover       =
 | Released    =  
 | Length      =
 | Label       = Capitol Music Group
 | Producer    =
}} Sydney Childrens original score was composed by Guy Gross. 

===Track listing===
# "The Only One" &ndash; Paul Mac (featuring Bertie Blackman) Bob Evans
# "I Melt with You" &ndash; Sydney Childrens Choir
# "Ribbons" &ndash; Guy Gross
# "The Wrong Girls" &ndash; Missy Higgins
# "Bar Mitzvah Prep" &ndash; Guy Gross
# "Clapping Song" &ndash; Operator Please
# "Be a Woman" &ndash; Persian Rugs
# "Lucky Lipstick" &ndash; Surferosa
# "The Only One (Duck Dissection)" &ndash; Paul Mac (featuring the Sydney Childrens Choir)
# "Esther On Stage" &ndash; Guy Gross
# "6/8" &ndash; Operator Please
# "Duck Walk" &ndash; Guy Gross
# "Sometimes" &ndash; Danielle Catanzariti and the Sydney Childrens Choir
# "Long Live the Girls" &ndash; Sara Storer
# "Young Folks" &ndash; Chasing Bailey
# "Strange Little Girl" &ndash; Sydney Childrens Choir
# "Liar" &ndash; Bob Evans
# "The Only One (Toy Piano)" &ndash; Paul Mac
# "Bar Mitzvah Meldey Hora" &ndash; Ilan Kidron and Glass

==Release== Boston Jewish Film Festival  and the Stockholm International Film Festival. 
 Office of Film and Literature Classification although the film was targeted at preteens and teenagers.   

==Reception==

===Critical reaction=== At the Movies each gave the film 3.5 stars out of 5. Stratton enjoyed the "terrific" performances, namely from Catanzariti and Keisha Castle-Hughes, while Pomeranz praised the films "eccentricity" and the discomfort caused to the audience at times. 

Other critics were less positive. The Herald Sun s head reviewer Leigh Paatsch gave the film no stars, claiming the film tried to match the edginess of the 2007 comedy film Juno (film)|Juno but was, "way, way off the mark". He wrote,  "If you think the title screams Go watch something else, just wait until you get a load of this lame local production."  Jim Schembri of The Age opined that the film was an example of "just how bad local   films can get". In particular, he criticised the "stiff" performances, the "woeful" directing and the plots implausibility.  Variety (magazine)|Variety magazines Richard Kuipers wrote that the film was "sporadically amusing" and lacked "scripting smarts and pulling power across demographics". He praised the soundtrack and Castle-Hughes portrayal of Sunni, but felt that the film was brought down by dull cinematography and "uninspired dialogue and direction".  Bernadette McNulty of The Daily Telegraph noted that the films greatest downfall was that "the jokes arent funny enough and the sadness barely breaks your heart". She complimented Toni Collettes performance but felt that "her slight role is insufficient to make it fly the distance".  Luke Goodsell, writing for Empire (magazine)|Empire magazine, awarded the film 2 out of 5 stars and deemed it to be deserving of "an all-purpose warning label to stay the Hell away". 

===Awards and nominations=== AFI Award Best Screenplay &ndash; Original (Cathy Randall), Best Costume Design (Shareen Beringer) and Best Sound (Liam Egan, Tony Murtagh, Phil Judd and Des Keneally).  Catanzariti was nominated by the Film Critics Circle of Australia for the FCCA Best Actress Award.  Randall won an award for directing at the Hamburg Filmfest Michel Childrens and Youth Film Festival. 

==Box Office==
Hey, Hey, Its Esther Blueburger grossed $863,950 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 