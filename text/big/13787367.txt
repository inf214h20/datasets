Trans-Europ-Express (film)
{{Infobox film
| name = Trans-Europ-Express
| image = 
| director = Alain Robbe-Grillet
| producer = Samy Halfon
| writer = Alain Robbe-Grillet
| starring = Jean-Louis Trintignant Marie-France Pisier Nadine Verdier Christian Barbier Charles Millot Daniel Emilfork
| music = Michel Fano
| cinematography = Willy Kurant
| editing = Bob Wade
| distributor =
| released = 1966
| runtime = 105 minutes
| country = Belgium / France
| language = French
| budget =
}}Trans-Europ-Express is a 1966 film written and directed by Alain Robbe-Grillet and starring Jean-Louis Trintignant and Marie-France Pisier. The title refers to the Trans Europ Express, a former international rail network in Europe.

The film has been variously described as an erotic thriller, a mystery, and a film-within-a-film.  Also in the cast were Nadine Verdier, Christian Barbier, Charles Millot, Catherine Robbe-Grillet, and the director. The protagonist is Elias (Trintignant) who is on a Illegal drug trade|dope-running errand from Paris to Antwerp by the train which gives the film its title. The director appears as himself in some sequences which are inter-cut with the action in which Elias is involved. The relationship between Elias and Eva (Marie-France Pisier) involves elements of erotic fantasy.

Screenwriter Robert McKee classifies Trans-Europ-Express as a "nonplot" film, that is, a film that does not tell a story. 
 Redemption Films. 

==References==
 

==Further reading==
*Gardies, André, comp. (1972) Alain Robbe-Grillet. Paris: Seghers (includes: "documents" & "points de vue", pp. 126-32, 162)

==External links==
*  

 

 
 
 
 