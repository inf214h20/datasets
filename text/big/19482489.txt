Wholly Smoke
 
{{Infobox Hollywood cartoon
|series=Looney Tunes (Porky Pig)
|image=
|caption=
|director=Frank Tashlin
|story_artist= Robert Bentley	
|background_artist=
|layout_artist=
|voice_actor=Mel Blanc
|musician=Carl Stalling
|producer=Leon Schlesinger
|studio=Warner Bros. Cartoons
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|release_date=August 27, 1938
|color_process=Black-and-white
|runtime=7 minutes
|movie_language=English
}}
Wholly Smoke is a   is 7 minutes 14 seconds (6 minutes 56 seconds for the edited, redrawn version). This episode teaches about the dangers of smoking.

==Plot== church ringing bully standing while underage. sarcastically accusing bet to prove he is not a wimp. The deal being the cigar for the nickel. Enticed by the proposition, the bully quickly gives up his cigar. Porky in turn tries to repeat the same set of tricks — with disastrous results.

Porky soon goes into a haze and stumbles into a smoke shop. An anthropomorphic cloud shrinks Porky in size and then introduces himself as someone all smokers were well acquainted with, "Nick OTeen". Nick then offers Porky all the smoking he can handle, and suddenly, a wide variety of tobacco products and smoking devices come to life to force feed Porky everything from chewing tobacco to cigarettes, all set to the song "Little Boys Shouldnt Smoke". At the culmination of the nightmare Porky awakens and rushes to church. As he is sitting reading his hymnal the collection plate starts coming towards him when he starts to panic. He races out of the church and grabs the nickel from the bully. He thrusts the cigar into the bullys mouth as it promptly explodes. He hurries back to church just in time to give his offering and the cartoon ends with him promising never to smoke again.

==Edited versions==
*The version of this cartoon that aired on Nickelodeon, the syndicated Merrie Melodies Show, and Cartoon Network was a colorized (redrawn on Cartoon Network; computer-colorized on Nickelodeon and the syndicated Merrie Melodies Show) version that had the following edits from the original black-and-white version (though an uncut version of the redrawn version is said to exist): 
**The beginning of the "Little Boys Shouldnt Smoke" song where four matchsticks strike themselves and burn out to form blackface (while singing in the style of The Mills Brothers) was cut on Nickelodeon and The Merrie Melodies Show. On Cartoon Network, the scene was left intact, but the blackface was changed to red during the colorization process.
**The part where a pipe cleaner sticks his head in a dirty pipe and comes out looking and singing like Cab Calloway was cut on The Merrie Melodies Show and Cartoon Network. It was left uncut in the early 1990s on Nickelodeon, but by the mid-to-late 1990s, the scene was edited out, though in the flashback after the "NO SMOKING" cigarette march, one can see the part that was cut on Nickelodeon (the newer, redrawn version on Cartoon Network replaces the Cab Calloway part during the montage with the Indian cigars scene that was cut when Cartoon Network aired the redrawn version of this cartoon).
**The short shot of Porky tied to a post while a tribe of Indian pipes dance around him was cut on Cartoon Network (but left in on The Merrie Melodies Show and Nickelodeon).

==Availability==
Wholly Smoke can be seen (uncensored, uncut, digitally remastered, and in its original black-and-white format) on the   DVD set.

==References==
 

==External links==
*  

 
 
 
 
 
 
 