The Hoax
 
{{Infobox film
| name           = The Hoax
| image          = The Hoax film poster.jpg
| caption        = Theatrical poster
| director       = Lasse Hallström
| producer       = Mark Gordon Bob Yari Betsy Beers Leslie Holleran Joshua Maurer
| screenplay     = William Wheeler
| based on       =  
| starring       = Richard Gere Alfred Molina Marcia Gay Harden
| music          = Carter Burwell
| cinematography = Oliver Stapleton
| editing        = Andrew Mondshein Bob Yari The Mark Gordon Company City Entertainment Buena Vista Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $25,000,000 
| gross          = $11,772,183 
}} autobiography Irving supposedly helped Howard Hughes write.

Many of the events Irving described in his book were changed or completely eliminated from the film. The author later said, "I was hired by the producers as technical
adviser to the movie, but after reading the final script I asked that my name be removed
from the movie credits."   

==Plot==
In 1971, publishing executives at McGraw-Hill express an interest in Clifford Irvings novel, Rudnicks Problem, although Fake!, his previous work about art forger Elmyr de Hory, had sold poorly. Irving believes he has a breakout work at last, only to be told that the publishing house has decided against releasing the book after a Life (magazine)|Life editor deems it unsatisfactory.

Vacationing with his friend and researcher Richard Suskind, Irving is ejected from his hotel at 1 a.m, when Howard Hughes arrives and demands the entire building be vacated. Returning to New York City to meet with his publishers, Irving is upset to find that he has been fobbed off onto one of the assistants.

Irving storms into the board room to announce that his new project will be the "book of the century," and threatens to take it elsewhere if McGraw-Hill is not interested. He then struggles to come up with a suitable topic for his grandiose claim, rejecting numerous suggestions from Suskind. After catching sight of a magazine cover picturing Hughes, he decides to make him the subject of his book.

Irving approaches McGraw-Hill and claims he has been summoned by Hughes to help him write his autobiography, providing forged handwritten notes from Hughes as proof. When handwriting experts wrongly conclude the notes are genuine, the publishers strike a $500,000 deal for the book.

Because Hughes is so reclusive and notoriously wary of legal action, he is unlikely to sue Irving, and his eccentricities also mean any denials of the books authenticity likely will be treated as misdirection. Irving is convinced his hoax is the perfect crime.

Irving is having marital problems with his artist wife Edith. His affair with actress/singer Nina Van Pallandt has left Edith hurt and skeptical about her husbands ability to remain monogamy|monogamous. Irving assures her he will remain faithful as he leaves to begin researching the book with Suskind.

In order to create an authenticity that will fool even the experts, the two men devote days to studying documents pertaining to Hughes. They illicitly obtain a copy of a draft biography of Noah Dietrich, a retired Hughes aide, which provides details that add to the apparent authenticity of the work. Irving begins reciting passages for the book into a tape recorder in character as Hughes, going so far as to dress as Hughes and draw a Hughes-like mustache on himself during these sessions.

As work on the book progresses, a box containing explosive information about questionable dealings between Hughes and Richard Nixon is delivered to Irving. He assumes the package is from Hughes and convinces himself that Hughes wants the damaging material included in the book, a sign he supports the autobiography.

As publication date draws near, Irving steps up his pretense, including staging an aborted meeting between Hughes and the publishers. Denials that Hughes is involved in any way with the book are issued from his headquarters, but the McGraw-Hill executives are convinced it is a genuine work. Irving uses their increasing desire for the guaranteed bestseller to leverage larger payments for himself and (purportedly) Hughes. Then he and Edith concoct a scheme for her to deposit Hughes check, payable to H. R. Hughes, into a Swiss bank account using a forged passport with the name Helga R. Hughes.

Irving becomes increasingly paranoid. He experiences alcohol-fueled fantasies about being kidnapped by Hughes people. His affair with Van Pallandt has continued, and the pressure of keeping up a pretense of fidelity with his wife adds to his stress.

In what is implied to be a favor to Nixon, Hughes goes public via a televised conference call and denies any knowledge of Irving or the book. Irving is arrested and agrees to cooperate if Edith is granted immunity from prosecution|immunity. At a press conference, a government spokesman announces Irving, Edith, and Suskind have received short jail sentences.
 burglary and wiretapping of Democratic Headquarters at the Watergate Hotel, a historically disputed point.

==Cast==
* Richard Gere as Clifford Irving
* Alfred Molina as Richard Suskind
* Marcia Gay Harden as Edith Irving
* Hope Davis as Andrea Tate Nina van Pallandt
* Stanley Tucci as Shelton Fisher
* Eli Wallach as Noah Dietrich
* Christopher Evan Welch as Albert Vanderkamp

==Production==
Portions of the film were shot in the  ) 

==Critical reception==
On Rotten Tomatoes, the film received an 85% positive rate, based on 151 reviews,  while on Metacritic, the film scored 70 out of 100, based on 37 reviews. 

A.O. Scott of The New York Times said the film was "for the most part a jumpy, suspenseful caper, full of narrow escapes, improbable reversals and complicated intrigue. But it has a sinister, shadowy undertow, an intimation of dread that lingers after Irving’s game is up." 

Kenneth Turan of the Los Angeles Times called the film "an unexpectedly satisfying fantasia of reality and imagination, a meditation on the nature of lies and deception, on how we come to embrace not the truth but what it suits us to believe ... sharply written ... and gracefully directed." 

Peter Travers of Rolling Stone rated the film 31⁄2 out of four stars and called it a "devilish and devastating satire." He added, "Gere gives em the old razzle-dazzle with his roguish charm and sharp comic timing. The surprise is the unexpected feeling he brings to this challenging role." 

Deborah Young of Variety (magazine)|Variety called the film a "breezy, fast-paced, somewhat loose-ended account   offers a surprisingly layered vehicle for a maniacally conniving Richard Gere, backed up by a superb Alfred Molina as his accomplice." 

==Box office==
The Hoax was given a limited opening in 235 theaters in the United States and Canada on April 6, 2007 and earning $1,449,320 on its opening weekend. It eventually grossed $7,164,995 in the US and Canada and $4,607,188 in foreign markets for a total worldwide box office of $11,772,183.   

==Accolades==
The London Film Critics Circle nominated Alfred Molina for British Supporting Actor of the Year, and Richard Gere was nominated for the Satellite Award for Best Actor – Motion Picture Musical or Comedy.

==Accuracy== dramatic liberties and completely eliminates all scenes set in Ibiza, where Irving wrote much of his book in a farmhouse he owned there. The author described the film as "a historically cockeyed story" and decried its characterizations as inaccurate. He was unhappy with being portrayed as "desperate and humorless, a washed-up hack writer who lives in a conservative New York suburb." Irving observed, "The movie misses the point that the Howard Hughes hoax was a live-action adventure story concocted by two middle-aged hippie expat writers and a Swiss heiress. Edith, my then-wife, a woman of great zest, is portrayed as a dull hausfrau, and Nina van Pallandt, my Danish mistress, as barely one level above a New York hotel hooker. Dick Suskind, witty friend and co-conspirator, is offered to the public as a self-righteous, sweaty buffoon. The scenes that deal with Movie Clifford feuding with Movie Dick, getting him drunk and hiring a bargirl to seduce him, are totally fictional. The Hughes people mailing the package of files to me is also made up." 

==Films Related to Hughes==
 Diamonds are Forever (1971)
*The Amazing Howard Hughes (1977; TV movie)
*Melvin and Howard (1980)
*  (1988) The Rocketeer (1991) The Aviator (2004)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 