Knock on Wood (film)
 
 
{{Infobox film
| name           = Knock on Wood
| image          = Knock on Wood (1954 movie poster).jpg
| caption        = Promotional poster for the film
| director       = Melvin Frank Norman Panama
| producer       = Melvin Frank Norman Panama
| writer         = Melvin Frank Norman Panama David Burns Leon Askin
| music          = Sylvia Fine 
| cinematography = Daniel L. Fapp
| editing        = Alma Macrorie
| distributor    = Paramount Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| gross = $3.5 million (US) 
}} David Burns, and Leon Askin.  The film was written and directed by Melvin Frank and Norman Panama, with songs by Kayes wife, Sylvia Fine.

==Plot== ventriloquist who is having trouble with love:  just when his relationship with a woman gets around to marriage, his dummy turns jealous and spoils everything. Jerrys manager Marty threatens to quit unless Jerry sees a psychiatrist, Ilse Nordstrom (Zetterling), who tries to discover the source of his problem. The two of them eventually fall in love.

At the same time, Jerry becomes unwittingly intertwined with spies and has to run from the police. In his escape, he finds himself impersonating a British car salesman, trying to demonstrate a new convertible with loads of bells and whistles. Later on, he finds himself on stage in the middle of the performance of an exotic ballet.

==Production notes== Slavic names of the spies) which would also be a feature of Kayes later film The Court Jester (likewise written and directed by Frank and Panama).
 the character of the same name; or the song "Knock on Wood" from the 1942 film, Casablanca (film)|Casablanca (with music by M.K. Jerome and lyrics by Jack Scholl). Kaye performed renditions of both the title song from Knock on Wood and "The Woody Woodpecker Song," which are found on the album The Best of Danny Kaye  .

==Cast==
* Danny Kaye as Jerry Morgan and his father
* Mai Zetterling as Doctor Ilse Nordstrom
* Torin Thatcher as Godfrey Langston, the film evil David Burns as Marty Brown, Jerrys Manager
* Leon Askin as Lazlo Gromek, a spion
* Abner Biberman as Maurice Papinek, a spion
* Otto Waldis as Brodnik, a spion Gavin Gordon as a Car Salesman
* Steven Geray as Doctor Krüger
* Diana Adams as Princess Maya
* Patricia Denise as Jerrys Mother
* Virginia Houston as Audrey Greene Paul England as the Chief Inspector Wilton
* Johnstone White as Langstons Secretary Henry Brandon as the Man with a Trenchcoat Lewis Martin as Inspector Crawford

==DVD release==
Knock on Wood was issued as a region 2 DVD in March 2009, and is on sale through Amazon UK.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 