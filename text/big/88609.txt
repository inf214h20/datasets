Star Trek IV: The Voyage Home
 
{{Infobox film
| name           = Star Trek IV: The Voyage Home
| image          = Star Trek IV The Voyage Home.png
| image_size     =
| alt            =
| caption        = Theatrical release poster by Bob Peak
| director       = Leonard Nimoy
| producer       = Harve Bennett
| screenplay     = Steve Meerson Peter Krikes Nicholas Meyer Harve Bennett
| story          = Harve Bennett Leonard Nimoy See cast
| music          = Leonard Rosenman
| cinematography = Donald Peterman
| editing        = Peter E. Berger
| distributor    = Paramount Pictures
| released       =  
| runtime        = 122 minutes  
| country        = United States
| language       = English
| budget         = $21 million   
| gross          = $133 million   
}}
Star Trek IV: The Voyage Home is a 1986 American   (1984). It completes the story arc begun in   and continued in The Search for Spock. Intent on returning home to Earth to face trial for their perceived crimes, the former crew of the USS Enterprise finds the planet in grave danger from an alien probe attempting to contact now-extinct humpback whales. The crew travel to Earths past to find whales who can answer the probes call.

After directing The Search for Spock, cast member Leonard Nimoy was asked to direct the next feature, and given greater freedom regarding the films content. Nimoy and producer Harve Bennett conceived a story with an environmental message and no clear-cut villain. Dissatisfied with the first screenplay produced by Steve Meerson and Peter Krikes, Paramount hired The Wrath of Khan writer and director Nicholas Meyer. Meyer and Bennett divided the story between them and wrote different parts of the script, requiring approval from Nimoy, lead actor William Shatner, and Paramount.
 on location; many real settings and buildings were used as stand-ins for scenes set around and in the city of San Francisco. Special effects firm Industrial Light & Magic (ILM) assisted in postproduction and the films special effects. Few of the humpback whales in the film were real: ILM devised full-size animatronics and small motorized models to stand in for the real creatures.

The Voyage Home premiered on November 26, 1986, in North America, becoming the top-grossing film in the weekend box office. The films humor and unconventional story were well received by critics, fans of the series and the general audience. It was financially successful, earning $133&nbsp;million worldwide. Space Shuttle broke up 73 seconds after takeoff on the morning of January 28, 1986.  Principal photography for The Voyage Home began four weeks after Challenger and her crew were lost.
A sequel titled   was released on June 9, 1989.
==Plot==
In 2286, a large cylindrical probe moves through space, sending out an indecipherable signal and disabling the power of ships it passes. As it takes up orbit around Earth, its signal disables the global power grid and generates planetary storms, creating catastrophic, sun-blocking cloud cover. Starfleet Command sends out a planetary distress call and warns starships not to approach Earth.
 song of travel back in time via a slingshot maneuver around the Sun, planning to return with a whale to answer the alien signal.

Arriving in 1986, the crew finds their ships power drained. Hiding their ship in   and Spock attempt to locate humpback whales, while Montgomery Scott, Leonard McCoy, and Hikaru Sulu construct a tank to hold the whales they need for a return to the 23rd century. Uhura and Pavel Chekov are tasked to find a nuclear reactor, whose energy leakage will enable their ships power to be restored.

Kirk and Spock discover a pair of whales in the care of Dr. Gillian Taylor at a Sausalito museum, and learn they will soon be released into the wild. Kirk tells her of his mission and asks for the tracking frequency for the whales, but she refuses to cooperate. Meanwhile, Scott, McCoy, and Sulu trade the formula of transparent aluminum for the materials needed for the whale tank. Uhura and Chekov locate a nuclear-powered ship, the aircraft carrier USS Enterprise (CVN-65)|Enterprise. They collect the power they need, but are discovered on board. Uhura is beamed back but Chekov is captured and severely injured in an escape attempt.
 USS Enterprise (NCC-1701-A), and leaves on a new mission.

==Cast==
William Shatner portrays Admiral James T. Kirk, former captain of the Enterprise. Shatner was unwilling to reprise the role of Kirk until he received a salary of $2&nbsp;million and the promise of directing the next film.  Shatner described The Voyage Home s comic quality as one "that verges on tongue-in-cheek but isnt; its as though the characters within the play have a great deal of joy about themselves, a joy of living   you play it with the reality you would in a kitchen-sink drama written for todays life." 

Leonard Nimoy plays Spock, who was resurrected by the effects of a powerful terraforming device and had his "living spirit" restored to his body in the previous film. DeForest Kelley portrays Doctor Leonard McCoy, who is given many of the films comedic lines; Kelley biographer Terry Lee Rioux wrote that in the film "he seemed to be playing straight man to himself". On Earth McCoy was paired with engineer Montgomery Scott (James Doohan), as producer Harve Bennett felt that Kelley worked well with Doohans "old vaudeville comic". Rioux, 265.  The other members of the Enterprise crew include George Takei as helmsman Hikaru Sulu, Walter Koenig as Commander Pavel Chekov, and Nichelle Nichols as Uhura. Koenig commented that Chekov was a "delight" to play in this film because he worked best in comedic situations. 
 National Geographic documentary about whales. Fischer, 37.  Nimoy chose Hicks after inviting her to lunch with Shatner and witnessing a chemistry between the two.  Alex Henteloff played Dr. Nichols, plant manager of Plexicorp. 

 . In the final cut of the film, all references to her condition were dropped. Gire, 4. 
 Federation President, Fleet Admiral Cartwright. Okuda.  Grace Lee Whitney reprises her role as Janice Rand from the original television series. 

==Production==
===Development===
Before The Search for Spock was released, its director Leonard Nimoy was asked to return to direct the next film in the franchise. Whereas Nimoy had been under certain constraints in filming the previous picture, Paramount gave the director greater freedom for the sequel. "  said flat out that they wanted my vision," Nimoy recalled. Lee, 43.  In contrast to the drama-heavy and operatic events of the three previous Star Trek features, Nimoy and producer   (1987), with less-expensive, lesser-known actors.   

Despite Shatners doubts,  Nimoy and Bennett selected a time travel story in which the Enterprise crew encounters a problem that could only be fixed by something only available in the present day (the Star Trek characters past). They considered ideas about violin makers and oil drillers, or a disease that had its cure destroyed with the rainforests. "But the depiction of thousands of sick and dying people seemed rather gruesome for our light-hearted film, and the thought of our crew taking a 600-year round trip just to bring back a snail darter wasnt all that thrilling," explained Nimoy. The director read a book on extinct animals and conceived the storyline that was eventually adopted. Hughes, 31.  Nimoy hit upon the idea of humpback whales after talking with a friend—their song added mystery to the story, and their size added logistical challenges the heroes would have to overcome. 
 whale songs.  Murphy disliked the part, explaining he wanted to play an alien or a Starfleet officer,  and chose to make The Golden Child—a decision Murphy later said was a mistake. The character intended for Murphy was combined with those of a marine biologist and a female reporter to become Gillian Taylor. Hughes, 32–33. 

Paramount was dissatisfied with the script, so its head of production Dawn Steele asked  , to help rewrite it. Meyer never read the earlier script, reasoning it pointless to do so since the content had no appeal to the studio. He and Bennett split the task of conceiving the plot between them. Bennett wrote the first quarter of the story, up to the point where the crew goes back in time. Meyer wrote the storys middle portion, taking place on 20th-century Earth, and Bennett handled the ending. Fischer, 36.  After 12&nbsp;days of writing, Meyer and Bennett combined their separate portions.  In this version, Gillian Taylor stays on 1986s Earth and vows to ensure the survival of the humpback whale despite the paradox it could create. Meyer preferred this "righter ending"  to the film version, explaining "The end in the movie detracts from the importance of people in the present taking the responsibility for the ecology and preventing problems of the future by doing something today, rather than catering to the fantasy desires of being able to be transported in time to the near-utopian future."  Meyer and Bennett cut out Krikes and Meersons idea of having the Klingon Bird-of-Prey fly over the Super Bowl and the hint that Saavik remained on Vulcan because she was pregnant with Spocks child. 
 Time After Time, was largely based in San Francisco; when he was told by the producers that The Voyage Home had to be set in the same city, he took the opportunity to comment upon cultural aspects not covered by his earlier film, such as punk rock—The Voyage Home s scene where Kirk and Spock meet an annoying punk rocker was based on a similar scene cut from Time After Time. 

Meyer found writing the script to be a smooth process. He would write a few pages, show it to Nimoy and Bennett for consultation, and return to his office to write some more. Once Nimoy, Bennett, and Meyer were happy, they showed the script to Shatner, who offered his own notes for another round of rewrites. Fischer, 38.  The completed script was shown to Paramount executives, who were very pleased. 

===Design===
Industrial Light & Magic (ILM) was responsible for The Voyage Home s model design and optical effects.  The alien probe was the responsibility of ILMs model shop, which brought in outside help like illustrator Ralph McQuarrie for concept art. The modelmakers started with art director Nilo Rodis basic design, a simple cylinder with whalelike qualities. The prototype was covered with barnacles and colored. The ball-shaped antenna that juts out from the bottom of the probe was created out of a piece of irrigation pipe; internal machinery turned the device. Three sizes of the probe were created; the primary   probe model was supplemented by a smaller model for wide shots and a large   model that used forced perspective to give the probe the illusion of massive dimensions. Shay, 4. 

The effects crew focused on using in-camera tricks to realize the probe; postproduction effects were time-consuming, so lighting effects were done on stage while filming. Model shop supervisor Jeff Mann filled the probes antenna with tube lamps and halogen bulbs that were turned on in sequence for different exposures; three different camera passes for each exposure were combined for the final effect. After watching the first shot, the team found the original, whalelike probe design lacking in menace. The modelmakers repainted the probe a shiny black, pockmarked its surface for greater texture and interest, and re-shot the scene. Although they wanted to avoid postproduction effects work, the opticals team had to recolor the antenna ball in a blue hue, as the original orange looked too much like a spinning basketball. Shay, 5–6. 

Aside from the probe, The Voyage Home required no new starship designs. The USS Saratoga, the first Federation starship disabled by the probe, was the USS Reliant model from The Wrath of Khan. The Bird-of-Prey model from The Search for Spock was reused, but ILM built additional sturdy versions for The Voyage Home s action sequences. The inside of the Bird-of-Prey was represented by a different set than The Search for Spock, but the designers made sure to adhere to a sharp and alien architectural aesthetic. To give the set a smokier, atmospheric look, the designers rigged display and instrumentation lights to be bright enough that they could light the characters, rather than relying on ambient or rigged lighting.  While Paramount had instructed ILM to trash the large Spacedock model created for The Search for Spock, the team had been loath to discard the complicated model and its miles of fiber optic lighting. When The Voyage Home called for the return of Spacedock, ILM had only to reassemble the model from storage. Shay, 10. 
 Robert Fletcher served as costume designer for the film. During the Earth-based scenes, Kirk and his gang continue to wear their 23rd-century clothing. Nimoy debated whether the crew should change costumes, but after seeing how people in San Francisco are dressed, he decided they would still fit in. 

===Filming===
Nimoy chose Donald Peterman, American Society of Cinematographers|ASC, as director of photography. Lee, 45.  Nimoy said he regarded the cinematographer as a fellow artist, and that it was important for them to agree on "a certain look" that Peterman was committed to delivering. Nimoy had seen Petermans work and felt it was more nuanced than simply lighting a scene and capturing an image. Lee, 46. 

The films opening scenes aboard the starship Saratoga were the first to be shot; principal photography commenced on February 24, 1986.  The set was a redress of the science vessel Grissom s bridge from The Search for Spock, in turn a redress of the Enterprise bridge created for The Motion Picture. The scenes were filmed first to allow time for the set to be revamped as the bridge of the new Enterprise-A at the end of filming. 

As with previous Star Trek films, existing props and footage were reused where possible to save money, though The Voyage Home required less of this than previous films. The Earth Spacedock interiors and control booth sets were reused from The Search for Spock, although the computer monitors in these scenes featured new graphics—the old reels had deteriorated in storage. Stock footage of the destruction of the Enterprise and the Bird-of-Preys movement through space were reused. While the Bird-of-Prey bridge was a completely new design, other parts of the crafts interior were also redresses; the computer room was a modification of the reactor room where Spock died in The Wrath of Khan. After all other Bird-of-Prey bridge scenes were completed, the entire set was painted white for one shot that transitioned into a dream sequence during the time travel. 
 Will Rogers Park in western Los Angeles was used instead. 

When Kirk and Spock are traveling on a public bus, they encounter a punk rocker blaring his music on a boom box, to the discomfort of everyone around him. Spock takes matters into his own hands and performs a Vulcan nerve pinch. Part of the inspiration for the scene came from Nimoys personal experiences with a similar character on the streets of New York; "  by the arrogance of it, the aggressiveness of it, and I thought if I was Spock Id pinch his brains out!"  On learning about the scene, Kirk Thatcher, an associate producer on the film, convinced Nimoy to let him play the role; Thatcher shaved his hair into a mohawk and bought clothes to complete the part. Credited as "punk on bus", Thatcher (along with sound designer Mark Mangini) also wrote and recorded "I Hate You", the song in the scene, and it was his idea to have the punk—rendered unconscious by the pinch—hit the stereo and turn it off with his face.    

  mind meld—underwater. Footage of the actors shot in front of them as they reacted to a brick wall in the Aquarium was combined with shots taken from their rear as they stood in front of a large blue screen at ILM to produce this scene. The footage of Spocks melding with the whales was shot weeks later in a large water tank used to train astronauts for weightlessness. 
 USS Enterprise. USS Ranger (CV-61) was used.  Oakland International Airport was used for the foreground element of Starfleet Headquarters. Scenes in the San Francisco Bay were shot at a tank on Paramounts backlot. Reeves-Stevens, 240–241. 

The scene in which Uhura and Chekov question passersby about the location of nuclear vessels was filmed with a hidden camera. However, the people with whom Koenig and Nichols speak were extras hired off the street for that days shooting and, despite legends to the contrary, knew they were being filmed. In an interview with StarTrek.com, Layla Sarakalo, the extra who said, "I dont know if I know the answer to that... I think its across the bay, in Alameda, California|Alameda", stated that after her car was impounded because she refused to move it for the filming, she approached the assistant director about appearing with the other extras, hoping to be paid enough to get her car out of impoundment. She was hired and told not to answer Koenigs and Nichols questions. However, she answered them and the filmmakers kept her response in the film, though she had to be inducted into the Screen Actors Guild in order for her lines to be kept. 

 
Vulcan and the Bird-of-Prey exterior was created with a combination of matte paintings and a soundstage. Nimoy had searched for a suitable location for the scene of the Enterprise crews preparations to return to Earth, but various locations did not work, so the scene was instead filmed on a Paramount backlot. The production had to mask the fact that production buildings were   away.  A wide-angle shot of Spock on the edge of a cliff overlooking the scene was filmed at Vasquez Rocks, a park north of Los Angeles.  The Federation council chamber was a large set filled with representatives from many alien races. Production manager Jack T. Collis economized by building the set with only one end; reverse angle shots used the same piece of wall. The positions of the Federation Presidents podium and the actors on the seats were switched for each shot. Since The Voyage Home was the first Star Trek film to show the operations at Starfleet Command, Bennett and Nimoy visited NASAs Jet Propulsion Laboratory to learn how a real deep space command center might look and operate. Among the resulting sets features was a large central desk with video monitors that the production team nicknamed "the pool table"; the prop later became a fixture in USS Enterprise-Ds engine room on the television series Star Trek: The Next Generation. 

===Effects=== matte paintings to extend backgrounds and create establishing shots without the cost of building a set. Matte supervisor Chris Evans attempted to create paintings that felt less contrived and more real—while the natural instinct of filmmaking is to place important elements in an orderly fashion, Evans said that photographers would "shoot things that   are odd in some way" and end up with results that look natural instead. The task of establishing the location and atmosphere at Starfleet Headquarters fell to the matte department, who had to make it feel like a bustling futuristic version of San Francisco. The matte personnel and Ralph McQuarrie provided design input. The designers decided to make actors in the foreground more prominent, and filmed them on a large area of smooth concrete runway at the Oakland Airport. Elements like a shuttlecraft that thirty extras appeared to interact with were also mattes blended to appear as if they were sitting by the actors. Ultimately the artists were not satisfied with how the shot turned out; matte photography supervisor Craig Barron believed that there were too many elements in the scene. 

The scenes of the Bird-of-Prey on Vulcan were combinations of live-action footage—actors on a set in the Paramount parking lot that was covered with clay and used backdrops—and matte paintings for the ship itself and harsh background terrain. The scene of the ships departure from Vulcan for Earth was more difficult to accomplish; the camera pans behind live-action characters to follow the ship as it leaves the atmosphere, and other items like flaming pillars and a flaring sun had to be integrated into the shot.  Rather than try to match and combine camera pans of each element, each component was shot with a static camera and the pan was added to the resulting composite by a motion control camera. Shay, 7.  The sun (a light bulb focused by a convex lens) was shot in different passes to create realistic light effects on the Bird-of-Prey without having the light bleed around other elements in the shot. Shay, 9. 

The script called for the probe to vaporize the Earths oceans, generating heavy cloud cover. While effects cinematographer Don Dow wanted to go to sea and record plumes of water created by exploding detonating cords in the water, the team decided to create the probes climatic effect in another way after a government fishing agency voiced concerns for the welfare of marine life in the area. The team used a combination of baking soda and cloud tank effects; the swirling mist created by the water-filled tank was shot on black velvet, and color and dynamic swirls were added by injecting paint into the tank. These shots were composited onto a painting of the Earth along with overlaid lightning effects, created by double-exposing lights as they moved across the screen. 

The Bird-of-Preys travel through time was one of the most difficult effects sequences of the film. While ILM was experienced in creating the streaking warp effect they used for previous films, the sequence required the camera to trail a sustained warp effect as the Bird-of-Prey rounded the sun. Matching the effect to the model was accomplished through trial-and-error guesswork. The team did not have the time to wait for the animation department to create the sun for this shot. Assistant cameraman Pete Kozachic devised a way of creating the sun on-stage. He placed two sheets of textured plexiglass next to each other and backlit them with a powerful yellow light. The rig was rotated on a circular track and the sheet in front created a moire pattern as its position shifted. Animator John Knoll added solar flare effects to complete the look; Dow recalled that the effect came close to matching footage of the sun taken by the Jet Propulsion Laboratory. Shay, 13.  Shay, 14. 
 animatic to Cyberware and used the resulting data for the computer models. Shay, 14.  Because each head model had the same number of key points of reference, transforming one character into another was simple; more difficult, the animators recalled, was ensuring that the transformation looked "pleasing" and not "grotesque". The resulting thirty seconds of footage took weeks to render; the department used every spare computer they could find to help in the processing chores. ILMs stage, optical, and matte departments collaborated to complete other shots for the dream sequence. The shot of a mans fall to Earth was created by filming a small puppet on bluescreen. Shots of liquid nitrogen composited behind the puppet gave the impression of smoke. The background plate of the planet was a large matte that allowed the camera to zoom in very close. The final shot of marshy terrain was practical and required no effects. Shay, 17. 

The filmmakers knew from the beginning of production that the whales were their biggest effects concern; Dow recalled that they were prepared to change to another animal in case creating the whales proved too difficult. When Humphrey the Whale wandered into the San Francisco Bay, Dow and his camera crew attempted to gather usable footage of the humpback but failed to do so.  Other footage of real humpbacks either did not exist on 35mm film or would have been difficult to match to specific actions required by the script. Compositing miniatures shot against bluescreen on top of water backgrounds would not have provided realistic play of light. Creating full-size mechanical whales on tracks would severely limit the types of angles and shots. To solve the whale problem, Rodis hired robotics expert Walt Conti. While Conti was not experienced in film, Rodis believed his background in engineering and design made him well-equipped for Rodis planned solution: the creation of independent and self-contained miniature whale models. Shay, 19. 
 Serra High School in San Mateo, California, for two weeks of shooting; the operation of the whales required four handlers and divers with video cameras to help set up the shots. Accurately controlling the whales was difficult because of the murky water—ILM added diatomaceous earth to the water to match realistic ocean visibility. Shay, 21–22.  For a few shots, such as the whales breaching the water towards the end of the film, the creatures were represented by life-size animatronics shot at Paramount. 

Models of the starship   was also reused for the ending, although the   interior set had to be rebuilt. Graphic designer Michael Okuda designed smooth controls with backlit displays for the Federation. Dubbed "LCARS|Okudagrams", the system was also used for displays on the Klingon ship, though the buttons were larger. 

===Audio=== The Lord Planet of the Apes sequels. Breyer, 22.  Bond, 119.  Rosenman wrote an arrangement of Alexander Courages Star Trek television theme as the title music for The Voyage Home, but Nimoy requested an original composition. Music critic Jeff Bond writes, "The final result was one of the most unusual Star Trek movie themes," consisting of a six-note theme and variations set against a repetitious four-note brass motif; the themes bridge borrows content from Rosenmans "Frodo March" for The Lord of the Rings.  The melody is played in the beginning of the film on Vulcan and the scenes of Taylors search for Kirk to help find her whales. Bond, 120. 
 Vivaldiesque "whale fugue" begins. The first sighting of the Enterprise-A uses the Alexander Courage theme before the end titles. Bond, 120. 
 puppet show to explain. Nimoy and the other producers were unhappy with Manginis attempts to create the probes droning operating noise; after 18&nbsp;attempts, the sound designer finally asked Nimoy what he thought the probe should sound like, and recorded Nimoys response. Nimoys voice was distorted with "just the tiniest bit of dressing" and used as the final sound. Special features, "Below-the-Line: Sound Design". 

The punk music during the bus scene was written by Thatcher after he learned that the audio for the scene would be by "Duran Duran, or whoever" and not "raw" and authentic punk.  Thatcher collaborated with Mangini and two sound editors who were in punk bands to create their own music. They decided that punk distilled down to the sentiment of "I hate you", and wrote a song centered on the theme. Recording in the sound studio as originally planned produced too clean a sound, so they moved to the outside hallway and recorded the song in one take using cheap microphones to create a distorted sound.  The song was also used in Paramounts Back to the Beach. 

==Reception==
===Release=== World Wildlife Fund on June 26, 1987, in Moscow to celebrate a ban on whaling. Attending the screening with Nimoy, Bennett was amazed the film proved as entertaining to the Russians as it did with American audiences; he said "the single most rewarding moment of my Star Trek life" was when the Moscow audience applauded at McCoys line, "The bureaucratic mentality is the only constant in the universe. Well get a freighter." Bennett believed it was a clear "messenger of what was to come." Dillard, 85–87. 

Vonda N. McIntyre wrote a novelization that was released at the same time as the film. It was the biggest tie-in novel published by Pocket Books,  and spent eight weeks on The New York Times bestseller list,  peaking at #3.  MCA Records released the films soundtrack November 26, 1986.

In its first week, The Voyage Home ended  , who moved The Voyage Home s release from Christmas to Thanksgiving after research showed that the film might draw filmgoers away from The Golden Child.   

Despite grossing $6,000,000 less than Star Trek: The Motion Picture, The Voyage Home was the most profitable of the series, grossing $133,000,000 against a $21,000,000 budget.

===Critical response=== Christian Science Monitor praised the film for giving audiences a view of their modern life from a different perspective, while simultaneously proving that a film does not need to have murder, violence, innuendo or even a main villain for dramatic storytelling.   
 Courier Mail wrote that the film was funnier than its predecessors, and while not "flippant", a sense of humor was revealed through the efforts of the cast, writers and director.    Newsweek s David Ansen considered The Voyage Home not only the most light-hearted of the movie franchise, but the most true in spirit to the original television series.    A more negative review was offered by Liam Lacey of The Globe and Mail, who wrote that under Nimoys "choppy" direction there was a lack of comic timing paired with feeble humor.   

The special effects were generally well-received;  critics for the Sydney Morning Herald and Courier Mail noted that the effects played a lesser role in the film compared to the characters and dialogue.   Similarly, USA Today felt the lack of special effects allowed the cast to "prove themselves more capable actors than ever before."  Maslin wrote that Nimoys technical direction left "much to be desired" (pointing out a special effects scene where the Bird-of-Prey does not cast a shadow on the whaling ship as a mistake), but his "unmistakable" sincerity made up for these issues.   
 Terry Porter, David J. Hudson, Mel Metcalfe and Gene Cantamessa), Sound Effects Editing, and Original Score.  

===Home media===
The Voyage Home was first released on VHS home media on September 30, 1987. Paramount Home Video spent $20 million marketing the films release alongside 10 episodes of the original series.  The video sold hundreds of thousands of copies in the United States and Canadian markets,  and was in the top ten rankings for sales and rentals in December and January 1988. Paramount re-released the film on March 12, 1992, with Fatal Attraction as part of a "Directors Series"; these editions had additional commentary and were presented in a widescreen letterbox format to preserve the films original cinematography. Nimoy was interviewed on the Paramount lots and discussed his acting career as well as his favorable opinion of the widescreen format. 

A "bare bones" DVD of the film was released on November 9, 1999. Aside from the film, the contents include the original theatrical trailer and the introduction from the "Directors Series" VHS release. Three and a half years later, a two disc "Collectors Edition" was released with supplemental material and the same video transfer as the original DVD release. Among other special features, it contains a text commentary by Michael Okuda and an audio commentary from director Leonard Nimoy and star William Shatner. 
 Star Trek film.  

==Notes==
 
 

==References==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 