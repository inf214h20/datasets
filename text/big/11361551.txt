Brother Orchid
{{Infobox film
| name           = Brother Orchid
| image          = Brother Orchid poster.jpg
| caption        = Theatrical release poster
| image_size     =
| director       = Lloyd Bacon
| producer       = Hal B. Wallis Mark Hellinger
| writer         =   Richard Macaulay
| starring       = Edward G. Robinson Ann Sothern Humphrey Bogart
| music          = Heinz Roemheld
| cinematography = Tony Gaudio William Holmes
| studio         = Warner Bros.
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Brother Orchid is a 1940 American crime film|crime/comedy film directed by Lloyd Bacon and starring Edward G. Robinson, Ann Sothern and Humphrey Bogart, with featured performances by Donald Crisp, Ralph Bellamy and Allen Jenkins. The screenplay was written by Earl Baldwin, with uncredited contributions from Jerry Wald and Richard Macauley, based on a story by Richard Connell originally published in Colliers Magazine on May 21, 1938. 

==Plot==
Crime boss Little John Sarto (Edward G. Robinson) retires suddenly, giving leadership of his gang to Jack Buck (Humphrey Bogart), while he leaves for a tour of Europe to acquire "class". However, Sarto is repeatedly swindled and finally loses all his money. 

He decides to return home and take back his gang, as if nothing has changed after five years, but Buck has him thrown out of his office. The only ones who remain loyal to Sarto are his girlfriend Flo Addams (Ann Sothern) and Willie "the Knife" Corson (Allen Jenkins). Sarto raises a new gang and starts encroaching on Bucks territory.

When Flo tries to get Buck to reconcile with Sarto, Buck sees his chance. He agrees, getting Flo to lure Sarto to a tavern without telling him why. Flo is not totally fooled; she brings along a strong, good-natured admirer, mid-western rancher Clarence P. Fletcher (Ralph Bellamy), just in case, but he is knocked out by Bucks men. Sarto is taken for a ride, believing Flo has double crossed him.

Sarto escapes, but is shot several times. He manages to make his way to the Floracian monastery, run by Brother Superior (Donald Crisp). Finding it a good place to hide out, Sarto signs up as a novice, naming himself "Brother Orchid". At first, he treats it as a joke, calling the monks the "biggest chumps in the world", but the kindness and simple life of the brothers begins to change his opinion.

Then Sarto sees a newspaper announcement that Flo is going to marry Clarence. He rides into the city with Brother Superior when he goes to sell the flowers that provide the monasterys meager income. After Flo gets over the shock of seeing Sarto alive, she proves she did not betray him and agrees to break up with Clarence.
 protective association" run by Buck bans flower growers that do not pay for its services. Buck is hiding out from the police, but Sarto has a good idea where he is. Reinforced by Clarence and some of his friends from Montana, Sarto pays a visit to the association and a brawl breaks out. When the police arrive, Sarto presents them with Buck and his men. Then, he gives up Flo to Clarence and returns to the monastery, where he has finally found "real class".

==Cast==
{| width="67%"
|-
| width="50%" valign="top" |
*Edward G. Robinson as "Little" John T. Sarto
*Ann Sothern as Florence Addams
*Humphrey Bogart as Jack Buck
*Donald Crisp as Brother Superior
*Ralph Bellamy as Clarence P. Fletcher
*Allen Jenkins as Willie "the Knife" Corson
*Charles D. Brown as Brother Wren
*Cecil Kellaway as Brother Goodwin
*Morgan Conway as Philadelphia Powell
*Richard Lane as Mugsy ODay Paul Guilfoyle as Red Martin Charles Coleman as English Diamond Salesman
| width="20%" valign="top" |
 
|}

;Cast notes
*James Cagney was originally intended to play the lead role.  Lee Patrick to play the role of Flo, but producer Mark Hellinger appealed directly to Warner Bros. head of production Jack Warner to have Ann Sothern cast in the part. Nixon, Rob   
*At the time that the film was made, Robinson was attempting to expand the kind of roles he played, having gotten bored with playing gangsters.  He agreed to play the lead in Brother Orchid in return for being cast in the historical drama A Dispatch from Reuters. 
*Brother Orchid is one of five films that Edward G. Robinson and Humphrey Bogart made together.  The others were Bullets or Ballots (1936), Kid Galahad (1937), The Amazing Dr. Clitterhouse (1938) and Key Largo (1948).  As noted by Lauren Bacall, Robinson killed or otherwise subdued Bogart in every one of their films except for Key Largo, Bogart having by that time eclipsed Robinson as a star and leading man.

==Awards and Nominations==
American Film Institute
*AFIs 100 Years...100 Laughs - Nominated 
*AFIs 10 Top 10 - Nominated Gangster Film 

==See also==
* List of American films of 1940

==Notes==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 