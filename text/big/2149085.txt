Ghosts of the Abyss
{{Infobox film
| name           = Ghosts of the Abyss  Titanic 3D: Ghosts of the Abyss 
| image          = Ghosts of the abyss.jpg
| caption        = The theatrical poster for Ghosts of the Abyss.
| director       = James Cameron John Bruno James Cameron Chuck Comisky Janace Tashjian Andrew Wight
| writer         =
| narrator       =
| starring       = Bill Paxton James Cameron  Dr. John Broadwater Dr. Lori Johnston
| music          = Joel McNeely Lisa Torban
| cinematography = Vince Pace D.J. Roller
| editing        = David C. Cook Ed W. Marsh Sven Pape John Refoua
| studio         = Walt Disney Pictures Walden Media Earthship Productions Ascot Elite Entertainment Group Golden Village Telepool UGC PH Buena Vista Pictures UGC Fox Distribution  
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $28,780,668 
}} wreck of RMS RMS remotely operated vehicles, nicknamed The Blues Brothers|"Jake" and "Elwood", the audience too can see inside the Titanic and with the help of Computer-generated imagery|CGI, audiences can view the ships original appearance superimposed on the deep-dive images.
 1997 film. BFCA award Mir 1 and Mir 2 carried the filming team
on twelve dives. 
The film is also known as Titanic 3D: Ghosts of the Abyss.  

==Outline== RMS Titanic 35 mm 3D theaters. Cameron and his team bring audiences to sights not seen since the sinking to the filming and explore why the vessel continues to intrigue and fascinate the public.  While diving on September 11, 2001, the filming crew hears about the September 11 attacks|9/11 attacks on the World Trade Center and the Pentagon.  Afterward, they all compare and reflect on the tragedy of 9/11 with the tragedy of the Titanic.

== Cast ==

Throughout the movie, there are re-enactments of events that are discussed that use CGI recreations of the interior of the Titanic.
 Thomas Andrews
* Ken Marschall as J. Bruce Ismay
* Miguel Wilkins as Robert Hichens (RMS Titanic)|Qm. Robert Hichens
* Federico Zambrano as John Jacob Astor IV
* Dale Ridge as Elizabeth Lines
* Judy Prestininzi as Molly Brown
* Adrianna Valdez as Helen Churchill Candee Jack Phillips
* Thomas Kilroy as a person who plays poker 1st Off. William Murdoch
* Piper Gunnarson as Madeleine Astor
* John Donovan as Edward Smith (sea captain)|Capt. Edward Smith Edith Russell 2nd Off. Charles Lightoller
* Justin Baker as Harold Bride
* Aaron C. Fitzgerald as Frederick Fleet|L. Frederick Fleet

==Release==
The film was screened out of competition at the 2003 Cannes Film Festival.   

===Home media=== Titanic 5-Disc Deluxe Limited Edition.

Walt Disney Studios Home Entertainment released the film on a three-disc Blu-ray 3D, Blu-ray and DVD edition on September 11, 2012.  

==Soundtrack==
{{Infobox album
| Name        = Ghosts of the Abyss
| Type        = Soundtrack
| Longtype    = 
| Artist      = Joel Mcneely
| Cover       = GhostsoftheAbyssSoundtrack.jpg
| Released    = April 26, 2003
| Recorded    = 
| Genre       = 
| Length      = 58:29 Hollywood
| Producer    = 
|
}}
The official soundtracks songs were composed and conducted by Joel McNeely, and the orchestrations were conducted by David Brown, Marshall Bowen, and Frank Macchia. The album was also recorded and mixed by Rich Breen, edited by Craig Pettigrew, and mastered by Pat Sullivan. The album was ultimately produced by James Cameron, Randy Gerston and Joel McNeely and released by Disneys Hollywood Records label.

===Track listing===
{{Tracklist
| collapsed       = yes
| headline =
| extra_column = Vocalist
| all_music =  Joel McNeely, except where noted
| music_credits = yes
| extra1 = Glen Phillips
| title1 = Departure
| music1 = Glen Phillips
| length1 = 2:33
| title2 = Main Title
| length2 = 1:16
| title3 = Apprehension
| length3 = 1:29
| title4 = Getting Ready
| length4 = 1:20
| title5 = Titanic Revealed
| length5 = 3:11
| title6 = Floating Above the Deck
| length6 = 3:01
| title7 = Dangerous Recovery
| length7 = 1:28
| title8 = Valse Septembre
| music8 = Felix Godin
| length8 = 2:19
| title9 = The Windows
| length9 = 0:47
| title10 = Jake and Elwood
| length10 = 2:14
| title11 = The Bots Go In
| length11 = 1:33
| title12 = Titsy Bitsy Girl
| music12 = Ivan Caryll and Lionel Monckton
| length12 = 1:52
| title13 = The Grand Staircase
| length13 = 1:33
| title14 = Exploring the Staterooms
| length14 = 1:51
| title15 = Song Without Words
| music15 = Piotr Ilyich Tchaikovsky
| length15 = 2:26
| title16 = Elegance Past
| length16 = 2:10
| title17 = Building the Ship
| length17 = 1:28
| title18 = I... I Had to Go
| length18 = 1:54
| title19 = The Ships Engines
| length19 = 1:42
| title20 = Alexanders Ragtime Band
| music20 = Irving Berlin
| length20 = 1:53
| title21 = The Final Day
| length21 = 2:15
| title22 = The End
| length22 = 3:17
| title23 = Memorials
| length23 = 1:18
| title24 = Go Toward the Light
| length24 = 1:31
| title25 = The Next Morning
| length25 = 2:08
| title26 = Nearer My God to Thee
| music26 = John B. Dykes
| length26 = 0:55
| title27 = Saying Goodbye to Titanic
| length27 = 1:55
| title28 = Eternal Father, Strong to Save William Whiting
| length28 = 3:02
| extra29 = Lisa Torban
| title29 = Darkness, Darkness
| music29 = Jesse Colin Young
| length29 = 4:05
}}

==References==
{{Reflist|refs=
 
   "Titanic director revisits ships wreck with hi-tech help",
   New Zealand Herald, March 19, 2005, webpage:
    .
  
   "Titanic 3D: Ghosts Of The Abyss (2003)",
   Yahoo! Movies, 2012, webpage:
    .
 
}}

==External links==
*  
*  
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 