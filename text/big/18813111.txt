The Nearly Complete and Utter History of Everything (film)
 
 
{{Infobox film
| name           = The Nearly Complete and Utter History of Everything
| image          = 
| image_size     = 
| caption        = 
| director       = Dewi Humphreys
| producer       = Jon Plowman Patricia McGowan Ali Bryer Carron Mark Burton Kim Fuller
| narrator       =  Adam Blackwood Brian Blessed  et al.
| music          = Philip Pope
| cinematography = Francis De Groote
| editing        = Steve Punt
| distributor    = British Broadcasting Corporation (BBC) 1999
| runtime        = 
| country        = United Kingdom English
| budget         = 
}} produced in 1999) broadcast in two parts on 2 and 4 January 2000 on BBC 1.

==Cast==
===The Ages of Man===
*Ronnie Barker – Renaissance Man
*Ronnie Corbett – Medieval Man
*Stephen Fry – Modern Man

===1066===
*Clive Anderson – Archbishop of Canterbury
*Ray Clemence – Ray Clemence the Younger
*George Cohen – Master George Cohen
*James Fleet – William the Conqueror
*Archie Gemmill – Archie Gemmill, currently in exile
*David Gower – Lord Gower
*Ricky Grover
*Alan Hansen – Lord Alan Hansen
*Geoff Hurst – Sir Geoffrey of Hurst
*Gary Lineker – Lord Gary Lineker King Harold II
*Peter Osgood – Squire Peter Osgoode
*Martin Peters – Lord Martin Peters
*Steve Punt

===Geordie of the Antarctic===
*Amanda Holden – Geordies girlfriend
*Bob Mortimer – Geordie
*Vic Reeves – Captain Scott

===The First Spin Doctor=== King Henry V
*Angus Deayton – Lord Mandelson
*Martin Trenaman

===Treaty of Westphalia===
*Patrick Barlow – Advisor
*Robert Bathurst – English Ambassador
*James Dreyfus – Swedish Ambassador
*Stephen Fry – Ambassador
*Hugh Laurie – French Ambassador

===Marriage Guidance===
*Brian Blessed – King Henry VIII
*Jack Dee – Marriage guidance counsellor
*Julia Sawalha – Catherine Parr

===The Nice-But-Dim Family=== Adam Blackwood – Norman
*Richard Briers – Highway robbery victim
*Tim Brooke-Taylor – Earl of Sandwich Duke of Wellington
*Harry Enfield – Tim Nice-But-Dim
*Jessica Hynes – Woman with Black Death Victim (as Jessica Stevenson)
*Natasha Little
*Spike Milligan – Admiral Nelson
*Nigel Planer – Lord Cardigan
*Steve Punt – Lord Tampax
*Barbara Windsor – Highway robbery victim

===Philosophy of a Hairdresser===
*Thora Hird – Ida
*Victoria Wood – Moira

===Explorers Explorers Explorers===
*Robert Bathurst – Francis Drake
*Peter Davison – Ferdinand Magellan
*Angus Deayton – Sir Walter Raleigh

===When Columbo discovered America===
*Gareth Hale – Columbo
*Norman Pace

===Medieval NHS===
*Jack Dee – Dr Barber
*Gary Olsen – Mr Baker

===Early British Comedy===
*Matthew Ashforde – Reginald
*Helena Bonham Carter – Lily
*Neil Morrissey – Director
*Caroline Quentin – Marcia Bournemouth Richard Wilson – Monty DeLauncy

===Looking Forwards===
*Lenny Henry – Decos
*Clive Mantle – Dr. Mike Barratt (as Clive Mantel)


*Dawn French
*Maria McErlane
*Geraldine McNulty
*Chris Pavlo
*Trevor Peacock
*Lexie Peel
*Philip Pope
*Jennifer Saunders

*Rupert Vansittart Kim Wall

==External links==
* 
*http://www.memorabletv.com/global/uk-tv/the-nearly-complete-and-utter-history-of-everything/

 
 
 
 
 
 
 


 
 