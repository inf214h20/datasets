Gangmaster (film)
 
{{Infobox film
| name           = Gangmaster
| image          = gangmaster_cover.jpg
| image_size     =
| caption        =
| director       = B. Gopal
| producer       = T. Subbarami Reddy
| writer         = Mahesh Bhatt
| narrator       = Rajasekhar Nagma Brahmanandam
| music          = A. R. Rahman
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 15 July 1994
| runtime        =
| country        = India Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1994 Telugu film directed by B. Gopal. It has music composed by A. R. Rahman. It was dubbed into Tamil with the title Manitha Manitha. The film was a remake of Hindi film Sir (film)|Sir.

==Cast==
*Krishnam Raju
*Nagma
*Brahmanandam Rajasekhar
*Vani Viswanath
*Subhashri
*Selva

==Soundtrack==

{{Infobox album  
| Name        = Gangmaster / Manitha Manitha
| Type        = film
| Artist      = A. R. Rahman
| Cover       = gangmaster_cover.jpg
| Released    = 1994
| Recorded    = Panchathan Record Inn
| Genre       = Film soundtrack
| Length      =
| Label       =
| Producer    =
| Reviews     =
| Last album  = Palnati Pourusham (1994)
| This album  = Gangmaster (1994)
| Next album  = Bombay (soundtrack)|Bombay  (1995)
}}
The score and soundtrack for the film were composed by A. R. Rahman with lyrics by Veturi Sundararama Murthy.
The Tamil version was titled Manitha Manitha and had lyrics by Vairamuthu.

===Original version===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s)
|-
| "Nagu Momu"
| Mano (singer)|Mano, K. S. Chithra
|-
| "Misa Misalade"
| S. P. Balasubrahmanyam, K. S. Chithra
|-
| "Hello Hello"
| S. P. Balasubrahmanyam, K. S. Chithra
|-
| "Baddaragiri" Sujatha
|-
| "Kila Kilala"
| Mano, Minmini
|-
| "Aa Siggu Eggulenthavarku"
| Mano, Swarnalatha
|}

===Tamil version (Manitha Manitha)===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s)
|-
| "Hello Hello Anburani"
| Mano (singer)|Mano, K. S. Chithra
|-
| "Kisu Kisu Namakkul"
| Mano, K. S. Chithra
|-
| "Innal Oru Ponnal" Sujatha
|-
| "Kiliyerandu Konchikkolla"
| Mano, Minmini
|-
| "Nee Etty Etty Thottuvachukka"
| Mano, Swarnalatha
|-
| "Enna Mugam Kannamma"
| Mano, K. S. Chithra
|}

==External links==
*  

 

 
 
 
 
 
 


 