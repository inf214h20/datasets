University Heights (film)
{{Infobox Film
| name           = University Heights
| director       = Scott Beck
| producer       = Bryan Woods Scott Beck
| writer         = Scott Beck
| starring       = Jim Siokos, Shane Simmons, Travis Shepherd, Sabien Minteer, Justin Marxen, Lindsey Husak, Casey Campbell, Brad Fandel
| music          = Matthew Florianz, Vincent Gillioz, Burn Disco Burn
| distributor    = Bluebox Limited| Bluebox Limited Films
| released       = June 26, 2004
| runtime        = 96 minutes
| country        = United States English
}} The University of Iowa. The movie was subsequently recognized by MTV and the filmmakers signed a development deal with MTV Films. 

==Cast==
* Jim Siokos as Tom
* Shane Simmons as Grant
* Travis Shepherd as Jake
* Sabien Minteer as Brent
* Justin Marxen as Tim
* Lindsey Husak as Katie
* Casey Campbell as Lou
* Brad Fandel as Frank

==References==
 

==External links==
*  
*  
*  review by Linda Cook

 
 
 