Birth Control Revolution
{{Infobox film
| name = Birth Control Revolution
| image = Birth Control Revolution.jpg
| image_size = 
| caption = Theatrical poster for Birth Control Revolution (1967)
| director = Masao Adachi   
| producer = Kōji Wakamatsu
| writer = Masao Adachi
| narrator = 
| starring = Mikio Terajima Kozue Kashima
| music = Yoshiaki Ōya
| cinematography = Hideo Itō
| editing = Shōgo Sakai
| studio = Wakamatsu Productions
| distributor = Nihon Cinema
| released = February 21, 1967
| runtime = 76 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1967 Japanese pink film directed by Masao Adachi for Kōji Wakamatsus production studio. 

==Synopsis==
The insane gynaecologist, Dr. Marukido Sadao (Marquis de Sade), theorizes that a woman is unable to become pregnant if she is writhing in intense pain during intercourse. He sets about testing this new method of birth control by torturing women during sex.       

==Cast==
* Mikio Terajima ( ) as Dr. Marquis de Sade (Marukido Sadao -  ) 
* Kozue Kashima ( ) as Mitsuko Marukido
* Atsushi Yamatoya ( ) as Nishimura
* Kuniko Masuda ( )
* Hachirō Tobita ( )
* Hatsuo Yamaya ( ) as Saburō Kyōtani
* Kōji Wakamatsu as Weekly magazine photographer
* Shigeomi Satō ( ) as Blue film actor

==Background and critical appraisal==
Masao Adachi filmed Birth Control Revolution for Kōji Wakamatsus Wakamatsu Productions and it was released theatrically in Japan by Nihon Cinema on February 21, 1967.  Adachi used the character of the crazy gynaecologist, Marukido Sadao—a Japanese pun on "Marquis de Sade"—in his first pink film, Abortion (1966).    He used the name in one or two of his film scripts directed by Kōji Wakamatsu.   According to writer on Japanese cinema, Roland Domenig, these "mad gynaecologist" films, as well as Wakamatsus "embryo" or "return to the womb" films such as The Embryo Hunts in Secret (1966), represent a re-imagining and parody of the "birth control" films which lured in audiences with titillating marketing strategies during the 1950s. 

Allmovie notes that despite the main characters "silly name", the film is actually a quite grim "twisted softcore S & M film". The review warns off viewers who are not comfortable with sadism as entertainment, in the style of many pink films.  In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers also note that it is a dark film, and "one of those excursions into sado-erotic fare that makes Western audiences cringe". 

==Bibliography==

===English===
*  
*  
*  
*  
*  
*  
*  

===Japanese===
*  
*  
*  

==Notes==
 

 
 
 
 


 
 