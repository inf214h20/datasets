Side Streets
 
 
{{Infobox film
| name           = Side Street
| image          =
| caption        =
| director       = Tony Gerber
| producer       = Bruce Weiss Ismail Merchant (executive) Tom Borders (executive) Gregory Cascante (executive)
| eproducer      =
| aproducer      =
| writer         = Tony Gerber Lynn Nottage
| starring       = Valeria Golino Shashi Kapoor Shabana Azmi Miho Nikaido Art Malik Victor Argo Rosario Dawson Jennifer Esposito
| music          = Evan Lurie
| cinematography = Russell Lee Fine
| editing        = Kate Williams
| distributor    =
| released       = 6 September 1998 (Venice Film Festival) 25 April 1999 (UK)
| runtime        = 116 min
| country        = UK
| awards         = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} film directed by Tony Gerber. The film features Valeria Golino, Shashi Kapoor, Shabana Azmi, Miho Nikaido, Art Malik, Victor Argo, Rosario Dawson, and Jennifer Esposito.

==Cast==
*Valeria Golino - Sylvie Otti
*Shashi Kapoor - Vikram Raj
*Shabana Azmi - Mrs. Chandra Bipin Raj
*Miho Nikaido - Yuki Shimamura
*David Vadim - Josif Iscovescu
*Art Malik - Bipin Raj Leon - Errol Boyce
*Joanna P. Adler - Policewoman Two Peter Appel - Boomir
*Victor Argo - Albani Krug
*Marie Barrientos - Lydia
*Kalimi Baxter - Maggie
*Gregg Bello - Jimi
*Helene Cara - Mrs. Petrescu
*Kathleen Chalfant - Nanda
*Oscar A. Colon - Mr. Hidalgo
*Rosario Dawson - Marisol Hidalgo
*Marina Durell - Mrs. Hidalgo
*Aunjanue Ellis - Brenda Boyce
*Jennifer Esposito - Jessica
*Venida Evans - Lydia Moulet
*Dudley Findlay Jr. - Sinclair
*Mtume Gant - Night Watchman
*Neil Harris - Night Watchman
*Mirjana Joković - Elena Iscovescu
*Jake-Ann Jones - Nicole
*Peter Lewis - Waiter
*Domenick Lombardozzi - Policeman One
*Mark Margolis - Bartender
*Laurence Mason - Dennis
*Peter McRobbie - Stelu
*David Moscow - Bellboy
*John Ortiz - Ramon Yanes
*Imani Parks - Girl in Botanica
*Gary Perez - Victor
*Joselin Reyes - Last Years Miss Colita
*Michael Rogers - Larry
*Slava Schoot - Russian Thug Rob Sedgwick - Frank
*Bina Sharif - Vikrams Fan One
*Samia Shoaib - Dancing Party Guest
*Damon Taylor - Hotel Clerk
*Marc Tissot - Manuel
*Leonid Uscher-Citer - Boris

==External links==
*  

 

 
 
 


 