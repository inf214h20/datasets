Beneath Hill 60
 
{{Infobox film
| name           = Beneath Hill 60
| image          = Beneath_Hill_60_Poster.jpg
| image size     =
| alt            =
| caption        = Australian theatrical poster Jeremy Hartley Sims David Roach
| writer         = David Roach Anthony Hayes Steve Le Marquand
| music          = Cezary Skubiszewski
| cinematography = Toby Oliver
| editing        = Dany Cooper
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        =
| country        = Australia
| language       = English
| budget         =     
}}
Beneath Hill 60 is a 2010 Australian war film directed by Jeremy Sims (credited as Jeremy Hartley Sims) and written by David Roach.
 1st Australian mining in Western Front. series of explosive charges German lines British troops.    The screenplay is based on an account of the ordeal written by Captain Oliver Woodward,  who is portrayed by Brendan Cowell in the film.

Beneath Hill 60 was released in Australia on 15 April 2010. In July 2009 it was reported that there were plans to have the film showcased at the 2010 Cannes Film Festival.   

==Plot== flashbacks that war effort. pressure to 1st Australian commissioned to lead the unit.
 Western Front, Woodward meets Frank Tiffin, a young Australian soldier who is suffering from shell shock; it is later revealed that Tiffin, being under age, was initially relegated to being a stretcher bearer, where he saw firsthand the horrors of war. Woodward reassigns Tom Dwyer and Norman Morris to relieve Tiffin. Two German tunnelers break through into the tunnel. Morris and Dwyer dispatch both of them, but Dwyer is killed when a German explosive goes off, burying the tunnel on top of them; Morris is rescued by the other sappers.

Later, Woodward is tasked to destroy the Red House, a fortified German position raining enfilade fire down upon the frontmost section of the British trenches. Although Hardwood initially proposed tunneling beneath the Red House, his commanding officer, Colonel Wilson Rutledge, insisted that it be done by dawn. Along with Sgt. Bill Fraser and Morris, Woodward crosses No Mans Land. They manage to reach the Red House and plant explosives underneath it. However, as they make their way back to British lines, they discover that the reel of wire is too short, so Morris runs ahead to retrieve the exploder. While they wait, they discover mortally wounded Lt. Robert Clayton, who had been ordered to cut a gap in the wire for them. Morris arrives with the exploder and they destroy the Red House.
 rugby league Belgium front. Upon arrival, Tiffin, Walter, and Bacon are separated from the rest of the battalion and pinned down by German machine gun fire. Bacon, being the fastest runner, volunteers to distract the Germans while the others make a run for British lines. Although Tiffin and Walter make it, Bacon is killed mere inches from safety.

The unit continues on to Hill 60. During an inspection, it is revealed that the Canadian Engineers have been tunneling deep below the Messine Ridge for months, planting a million pounds of ammonium nitrate in the form of 21 massive land mine|mines. Woodward is tasked with maintaining the tunnels as the British military buildup begins; they hope to fill the ridge with as many German troops as possible. To this end, he constructs a massive shaft to keep the water table from inundating the explosives. He also digs multiple diversion tunnels to throw the Germans off. Sneddon is buried in one such tunnel when he is ordered into it by Rutledge, despite reporting that the Germans would likely be setting off explosives soon.
 two of Battle of Messines.

Woodward returns to Australia and the surviving members of the unit gather for his wedding with Marjorie.

==Cast==
* Brendan Cowell as Oliver Woodward
* Alan Dukes as Jim Sneddon
* Alex Thompson as Walter Sneddon
* Harrison Gilbertson as Frank Tiffin
* Duncan Young as Tom Dwyer
* Steve Le Marquand as Bill Fraser
* Gyton Grantley as Norman Morris
* Warwick Young as Percy Marsden
* Mark Coles Smith as Billy Bacon
* Martin Thomas as Ginger ODonnell
* Oliver Leimbach as Screaming Soldier (Youngston) Anthony Hayes as William McBride
* Leon Ford as Lt. Robert Clayton
* Fletcher Illidge as Colin Waddell
* Morgan Illidge as Gordon Waddell
* Jacqueline McKenzie as Mrs. Emma Waddell
* Juliana Dodd as Isabel Waddell
* Gerald Lepkowski as William Waddell
* Bella Heathcote as Marjorie Waddell (credited as Isabella Heathcote)
* Chris Haywood as Colonel Wilson Rutledge Bob Franklin as Potsy
* Anthony Ring as Stoat
* Nikki Fort as Mrs. Thorn
* Alice Cavanagh as Agnes
* Simon Coomes as Rex Astros
* Mahala Wallace as Eunice
* Jessica Robertson as Dotty (credited as Jessica Paige)
* Marcus Costello as Ernst Wagner
* Kenneth Spiteri as Karl Babek
* Aden Young as Major Brady North (3rd Canadian Tunnelling Company - Nova Scotia)
* David Ritchie as Otto Fusslein Tom Green as Warren Hutchings John Stanton as General Lambert
* Andy Bramble as Wilf Piggott

==Production==
Ross J. Timmings, a mining engineer and historian, met with producer Bill Leimbach and quickly convinced him that the story of Captain Oliver Woodward and the 1st Australian Tunnelling Company was "a story crying out to be told". Timmings knew descendants of Captain Oliver Woodward living in Melbourne, who agreed to have Woodwards diaries adapted into a screenplay. Leimbach recruited David Roach to write the film. Extensive research went into developing the characters and their environment, with Canberras Australian War Memorial Archives providing research material.   

The majority of the war scenes were written to take place inside the tunnels so a tense environment could be achieved while facilitating the films tight budget.    While searching for a director, Leimbach viewed Jeremy Simss 2006 film Last Train to Freo. Impressed with the intensity of the films claustrophobic action, Leimbach approached Sims, who agreed to direct Beneath Hill 60 after reading an early treatment. 

Actor Hugo Weaving showed interest in participating in the film and was offered the role of Oliver Woodward, but ultimately declined due to his claustrophobia.    The role went to Brendan Cowell in a decision driven by the actors experience and numerous accolades. 

Working titles for the film included The Silence    and The Silence Beneath.   Leimbach explained that the film was titled Beneath Hill 60 to maximise recognition for the international audience and felt that it was a more suitable title for a war film. 

 , which Jackman had starred in and produced.   The local   club provided the players for the rugby game and the following thigh slapping singing scene.

==Release==
Despite initial plans to have the film released on Anzac Day (25 April),    the film was released on 15 April 2010.         

==Reception==
Beneath Hill 60 received generally positive reviews. Review aggregate Rotten Tomatoes reports that 83% of critics have given the film a positive reviews based on 12 reviews, with an average score of 7/10. 

==See also== Battle of Western Front)
* Gallipoli (1981 film)|Gallipoli, a 1981 Australian film set during World War I
* The Lighthorsemen (film)|The Lighthorsemen, a 1987 Australian film set during World War I Charles Chauvel

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 