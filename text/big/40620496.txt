Of Horses and Men
 
{{Infobox film
| name           = Of Horses and Men
| image          = Of Horses and Men.jpg
| caption        = Film poster
| director       = Benedikt Erlingsson
| producer       = Friðrik Þór Friðriksson
| writer         = Benedikt Erlingsson
| starring       = Helgi Björnsson
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 
}}
 Best Foreign Language Film at the 86th Academy Awards,       but it was not nominated. The film won the 2014 Nordic Council Film Prize.

==Cast==
* Helgi Björnsson
* Charlotte Bøving
* Sigríður María Egilsdóttir
* Maria Ellingsen
* Juan Camillo Roman Estrada
* Halldóra Geirharðsdóttir
* Erlingur Gíslason
* Kristbjörg Kjeld
* Steinn Ármann Magnússon
* Kjartan Ragnarsson
* Atli Rafn Sigurðsson
* Ingvar Eggert Sigurðsson

==Reception==
Robbie Collin described Of Horses and Men as a "collection of six-or-so interlocking fables about a group of rural Icelanders’ relationships with their horses and each other, and which run the gamut from stony-black comedies of sex and death to chilly meditations on the blind cruelty of fate."  He gave it four stars out of five and called it "something truly and seductively strange" and "tenderly attuned to the weather and landscape, both of which are captured in you-could-almost-be-there vividness, and underscored by a heady swirl of choral works and primal drumming." 

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Icelandic submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 