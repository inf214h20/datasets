Dushman (1998 film)
{{Infobox film
| name = Dushman
| image = Dushman 1998.jpg
| alt =  
| caption = Poster
| director = Tanuja Chandra
| producer = Mukesh Bhatt   Pooja Bhatt
| writer = Mahesh Bhatt (screenplay)   Sachin Bhowmick (story)   Girish Dhamija (dialogue)
| based on =  
| starring = Sanjay Dutt Kajol   Ashutosh Rana
| music = Uttam Singh
| cinematography = Nirmal Jani
| editing = Waman B. Bhosle
| studio =
| distributor =
| released =  
| runtime = 145 minutes
| country = India
| language = Hindi
| budget =
| gross =   13,50,00,000  
}}
Dushman (  thriller film starring Sanjay Dutt, Kajol and Ashutosh Rana in lead roles. The film is directed by Tanuja Chandra and produced by Mukesh Bhatt and Pooja Bhatt.
 Eye for an Eye. 

==Plot==
Sonia Sehgal and Naina Sehgal (both played by Kajol) are twins. The two are completely different from each other, Sonia being the tomboyish twin and Naina being the shy one. Meanwhile, the police are hunting for a cold-blooded, sadistic killer and rapist, Gokul Pandit (Ashutosh Rana). However tragedy strikes when Gokul rapes and brutally kills Sonia. Naina is distraught and vows to hunt down Gokul. Gokul soon goes after Naina and she realizes she needs help to overcome her fear of Gokul. With revenge in her mind she meets Suraj Singh Rathod (Sanjay Dutt), a blind military veteran and he helps her physically and mentally to defeat her fear of Gokul. While Suraj trains Naina they develop feelings for each other. After an argument, Suraj refuses to meet Naina and she decides to go after Gokul all by herself. One day Gokul kidnaps Nainas younger sister Dia from school in order to scare and warn Naina with what risk she is playing. That day Nainas mother decides to leave for Naintal immediately as her daughters life is at risk. However Naina cannot control her hatred, and wanted to avenge her sister at any cost. Naina lays a trap for Gokul and tries to kill him but Gokul ties her up and tries to rape her like her sister. Suraj arrives at her house and fights Gokul but ends up being stabbed. Naina gets free and manages to shoot Gokul dead. 

Suraj recovers from his injuries and decides to go away from Naina, but she realizes that she loves him and cannot live without Suraj. The film ends on a good note as Suraj and Naina get together and move on with their lives.

==Cast==  
*Sanjay Dutt  as Major Suraj Singh Rathod 
*Kajol as Sonia Sehgal / Naina Sehgal (Mona Ghosh Shetty as the dubbing voice)
*Ashutosh Rana as Gokul Pandit 
*Jas Arora as Kabir Suraj 
*Tanvi Azmi as Mrs. Poornima Sehgal 
*Pramod Muthu as ACP Santosh Singh Sehgal
*Kunal Khemu as Bheem 
*Pratima Kazmi as Prosecuting Attorney 
*Anupam Shyam as Inspector Dubey 
*Vani Tripathi as Sunanda Tripathi
*Amardeep Jha as Jaya

==Soundtrack==
The music for the film was composed by Uttam Singh and Anand Bakshi penned the lyrics.
{| class="wikitable"
|-
! # !! Title !! Singer(s)
|-
| 1
| "Chitthi Na Koi Sandes (Male)" 
| Jagjit Singh
|- 
| 2
| "Aawaz Do Hamko" 
| Lata Mangeshkar & Udit Narayan 
|- 
| 3
| "Pyar Ko Ho Jane Do"
| Lata Mangeshkar & Kumar Sanu 
|- 
| 4
| "Aawaz Do Hamko (Sad)"
| Lata Mangeshkar & Udit Narayan
|- 
| 5
| "Chitthi Na Koi Sandes (Female)"
| Lata Mangeshkar
|-
| 6
| "Tuna Tuna"
| Shankar Mahadevan
|}

==Awards and nominations==
{| class="wikitable sortable"
|-
! Award !! Category !! Nominee !! Result
|- Best Performance in a Negative Role || Ashutosh Rana ||  
|- Best Actress || Kajol ||  
|- Best Actress || Kajol ||  
|- Best Villain || Ashutosh Rana ||  
|-
|}

==References==
 

==External links==
* 

 

 
 
 