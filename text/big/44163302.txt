Mrithunjayam
{{Infobox film
| name           = Mrithunjayam
| image          =
| caption        =
| director       = Paul Babu
| producer       =
| writer         = T. Damodaran
| screenplay     = T. Damodaran Devan Parvathy Parvathy Jagathy Sreekumar Nedumudi Venu
| music          = Ouseppachan
| cinematography = Saroj Padi
| editing        = NP Suresh
| studio         = Nancypriya
| distributor    = Nancypriya
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by Paul Babu. The film stars Devan (actor)|Devan, Parvathy (actress)|Parvathy, Jagathy Sreekumar and Nedumudi Venu in lead roles. The film had musical score by Ouseppachan.   

==Cast==
  Devan
*Parvathy Parvathy
*Jagathy Sreekumar
*Nedumudi Venu Ashokan
*Nahas
*Prathapachandran
*Ganesh Kumar
*Lalu Alex
*RK Nair
*T. P. Madhavan
*VK Sreeraman
*Valsala Menon
 

==Soundtrack==
The music was composed by Ouseppachan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Pon Peelikal || KS Chithra || Poovachal Khader || 
|-
| 2 || Pon Peelikal   ||  || Poovachal Khader || 
|-
| 3 || Priyathe En Priyathe || Ouseppachan || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 