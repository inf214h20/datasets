Quand la femme s'en mêle
{{Infobox film
| name           = Quand la femme sen mêle
| image          = 
| image size   =
| alt            =
| caption        = 
| director       = Yves Allégret
| producer       = Cino Del Duca Arys Nissotti Pierre OConnell
| writer         = Jean Meckert   Charles Spaak
| based on       =  
| narrator       =
| starring       = Edwige Feuillère Jean Servais Jean Debucourt
| music          = Paul Misraki
| cinematography = André Germain
| editing        = Claude Nicole
| studio         = Cino del Duca  Royal Films  Régina Films  Plazza Films Productions
| distributor   = Cinédis
| released       =  
| runtime        = 90 minutes
| country        = France Italy
| language       = French
| budget         =
| gross          = 913,880 admissions (France) 
}}
Quand la femme sen mêle is a film adaptation of Jean Amilas novel "Sans attendre Godot". Directed by Yves Allégret, it was Alain Delons and also Bruno Cremers film debut.   The film is also known as "Send a Woman When the Devil Fails". 

== Synopsis ==
Angèle (nicknamed "Maine") is the wife of Henri Godot who feels his marriage was menaced by a rival. When Angèle is approached by her former husband Félix he hires a young killer.

== Cast ==
* Edwige Feuillère as Angèle
* Jean Servais as Henri Godot
* Jean Debucourt as Auguste Coudert de la Taillerie
* Bernard Blier as Félix Seguin
* Bruno Cremer as Bernard
* Alain Delon as Jo
* Henri Cogan as Alberti

== References ==
 

== External links ==
*  
*  
* 

 
 
 
 
 

 