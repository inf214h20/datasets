Of Human Bondage (1934 film)
 
{{Infobox film
| name = Of Human Bondage
| image = Of Human Bondage Poster.jpg
| caption = Theatrical release poster
| producer = Pandro S. Berman John Cromwell
| writer = Lester Cohen Based on the novel by W. Somerset Maugham Leslie Howard Bette Davis Frances Dee
| music = Max Steiner
| cinematography = Henry W. Gerrard
| editing = William Morgan
| studio = RKO Radio Pictures
| distributor = RKO Radio Pictures
| released =   In New York, at its premiere at Radio City Music Hall, the audience found Bette Davis so good as the wicked Mildred, they applauded when she died. }}
| runtime = 83 minutes
| country = United States
| language = English
| budget = $403,000 Richard Jewel, RKO Film Grosses: 1931–1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p57  gross = $592,000 
}}
 American drama John Cromwell novel of remade in in 1964.

==Plot==
Sensitive, club foot|club-footed artist Philip Carey is a Briton who has been studying painting in Paris for four years. His art teacher tells him his work lacks talent, so he returns to London to become a medical doctor, but his moodiness and chronic self-doubt make it difficult for him to keep up in his schoolwork.

Philip falls passionately in love with vulgar tearoom waitress Mildred Rogers, even though she is disdainful of his club-foot and his obvious interest in her. Although he is attracted to the anemic and pale-faced woman, she is manipulative and cruel toward him when he asks her out. Her constant response to his romantic invitations is "I dont mind," an expression so uninterested that it infuriates him – which only causes her to use it all the more. His daydreams about her (her image appears over an illustration in his medical school anatomy textbook, and a skeleton in the classroom is transformed into Mildred) cause him to be distracted from his studies, and he fails his medical examinations.

 When Philip proposes to her, Mildred declines, telling him she will be marrying a loutish salesman Emil Miller instead. The self-centered Mildred vindictively berates Philip with nasty insults for becoming romantically interested in her.

Philip begins to forget Mildred when he falls in love with Norah, an attractive and considerate romance writer working under a male pseudonym. She slowly cures him of his painful addiction to Mildred. But just when it appears that Philip is finding happiness, Mildred returns, pregnant and claiming that Emil has abandoned her.

Philip provides a flat for her, arranges to take care of her financially, and breaks off his relationship with Norah. Norah and Philip admit how bondages exist between people (Philip was bound to Mildred, as Norah was to Philip, and as Mildred was to Miller).

Philips intention is to marry Mildred after her child is born, but a bored and restless Mildred is an uninterested mother, and gives up the babys care to a nurse.

At a dinner party celebrating their engagement, one of Philips medical student friends, Harry Griffiths, flirts with Mildred, who somewhat reciprocates. After Philip confronts Mildred, she runs off with Griffiths for Paris. A second time, Philip again finds some comfort in his studies, and with Sally Athelny, the tender-hearted daughter of one of his elderly patients in a charity hospital. The Athelny family is caring and affectionate, and they take Philip into their home.

Once again, Mildred returns with her baby, this time expressing remorse for deserting him. Philip cannot resist rescuing her and helping her to recover from another failed relationship. Things take a turn for the worse when Mildred moves in, spitefully wrecks his apartment and destroys his paintings and books, and burns the securities and bonds he was given by an uncle to finance his tuition. Philip is forced to quit medical school, but before he leaves the institution, an operation corrects his club foot. The Athelnys take Philip in when he is unable to find work and is locked out of his flat, and he takes a job with Sallys father as a window dresser.

As time progresses, a letter is sent to Philip which informs him that his uncle has died, leaving a small inheritance. With the inheritance money, Philip is able to return to medical school and pass his examinations to become a qualified doctor.

Later, Philip meets up with Mildred, now sick, destitute, and working as a prostitute. Mildreds baby has died, and she has become distraught and sick with tuberculosis. Before he can visit her again, she dies in a hospital charity ward. With Mildreds death, Philip is finally freed of his obsession, and he makes plans to marry Sally.

==Production== Leslie Howard and when he suggested Davis would be the perfect co-star, Berman agreed. Stine, Whitney, and Davis, Bette, Mother Goddam: The Story of the Career of Bette Davis. New York: Hawthorn Books 1974. ISBN 0-8015-5184-6, pp. 41–42, 50–51, 57–63, 68  Maugham also supported her being cast in the role. Higham, Charles, The Life of Bette Davis. New York: Macmillan Publishing Company 1981. ISBN 0-02-551500-4, pp. 66–72   

Screenwriter   but continued to harass Warner, who continued to resist because he felt the role of Mildred would destroy her glamorous image, the reason   wanted RKO contract player Irene Dunne for Sweet Adeline, the screen adaptation of the Jerome Kern-Oscar Hammerstein II musical, and the two studios agreed to trade actresses.  

 In order to prepare for the role, Davis hired an English housekeeper. "She had just the right amount of cockney in her speech for Mildred. I never told her she was teaching me cockney – for fear she would exaggerate her own accent."  Davis, Bette, A Lonely Life. New York: G.P. Putnams Sons 1962. ISBN 0-425-12350-2, pp. 173–176, 179–180  Her efforts failed to impress Leslie Howard who, along with other British cast members, was upset an American had been cast in the role. "I really couldnt blame them," Davis stated. But his behavior on the set was upsetting. "Mr. Howard would read a book off-stage, all the while throwing me his lines during my close-ups. He became a little less detached when he was informed that the kid was walking away with the picture."  
 Hays Code, deb had missed her noon nap. The last stages of tuberculosis|consumption, poverty and neglect are not pretty and I intended to be convincing-looking. We pulled no punches and Mildred emerged . . . as starkly real as a pestilence." 
 
Reflecting on her performance in later years, Davis said, "My understanding of Mildreds vileness – not compassion but empathy – gave me pause . . . I was still an innocent. And yet Mildreds machinations I miraculously understood when it came to playing her. I was often ashamed of this . . . I suppose no amount of rationalization can change the fact that we are all made up of good and evil." 
 Santa Barbara, although her mother Ruth and husband Harmon O. Nelson went. Ruth later related, "For one hour and a half of horrible realism, we sat riveted without speaking a word, with only a fleeting glance now and then at each other. We left the theater in absolute silence. Neither of us knew what to think, for we felt the picture would make or break her, but would the public like the unpleasant story as well as the people at the preview seemed to?"  Upon arriving home, her husband told Davis he thought her performance, while "painfully sincere," might harm her career. 
 motif for each of the principal characters. 

The film premiered at Radio City Music Hall on June 28, 1934,  and went into general release on July 20. The generally rave reviews upset Warner executives, who were embarrassed one of their contract players was being acclaimed for a film made at another studio, and they tried to exclude its title from any publicity about Davis.  Although her nomination for the Academy Award for Best Actress was considered a sure thing by many,  she was ignored in favor of Grace Moore for One Night of Love, Norma Shearer for The Barretts of Wimpole Street, and eventual winner Claudette Colbert for It Happened One Night. Angry voters ignored the nominees on their ballots and wrote in Davis name,  and it was announced she came in third after Colbert and Shearer. Price Waterhouse was hired to count the votes and initiated the custom of keeping the results a secret the following year,   when Davis was named Best Actress for Dangerous (film)|Dangerous. Entertainment Weekly called Daviss Oscar snub one of the worst ever. 

==Cast== Leslie Howard ..... Philip Carey
*Bette Davis ..... Mildred Rogers
*Frances Dee ..... Sally Athelny
*Kay Johnson ..... Norah Reginald Denny ..... Harry Griffiths Alan Hale ..... Emil Miller
*Reginald Sheffield ..... Cyril Dunsford
*Reginald Owen ..... Thorpe Athelny
*Tempe Pigott .....Agnes Hollet, Philips landlady

==Critical reception==
Mordaunt Hall of the New York Times said the Maugham novel "has come through the operation of being transferred to the screen in an unexpectedly healthy fashion. It may not possess any great dramatic strength, but the very lifelike quality of the story and the marked authenticity of its atmosphere cause the spectators to hang on every word uttered by the interesting group of characters." He thought Leslie Howards portrayal "excels any performance he has given before the camera. No more expert illustration of getting under the skin of the character has been done in motion pictures," and he described Bette Davis as "enormously effective."   Also that year, a reviewer in Life Magazine called Bette Davis performance the greatest ever recorded on screen by an actress .
 Imitation of Life and Cleopatra) with Shearer coming in second.  The non-nominated Davis came in third and reportedly the also non-nominated Myrna Loy came in to finish the top five for her performance in The Thin Man).

The film recorded a loss of $45,000. 

==DVD release== public domain (in the USA) due to the claimants failure to renew its copyright registration in the 28th year after publication,  and as such, there are numerous DVD and online download editions available of varying image and audio quality.

==References==
 

==External links==
 
*  on  
* 
* 
* 
*  NBC University Theater: November 14, 1948
* 

 

 
 
 
 
 
 
 
 
 
 