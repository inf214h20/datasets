Southern Crossing (film)
{{Infobox film
| name                 = Southern Crossing (film)
| image                = Southern Crossing.jpg
| caption              = Cover for the movie Southern Crossing
}}
{| class="wikitable"
|-
| Directed by || Robert Guillemot
|-
| Produced by || Richard Bradley
|-
| Executive Producers || Australian Film Commission, Richard Bradley Productions
|-
| Based on || 1st Sydney International Music Festival January 1980
|-
| Starring || Dave Brubeck, Hebie Mann
|-
| Sound Recording || Ron Purvis, Ted Otten
|-
| Cinematography || Guy Furner
|-
| Editing by || Barry Fawcett
|-
| Production Company  || Richard Bradley Productions
|-
| Distributed by || Richard Bradley Productions
|-
| Release date(s) || November 29, 1980
|-
| Running time || 92 minutes
|-
| Country|| Australia
|-
|}

 
  Richard Bradley. Regent Theatre in Sydney.
 Sydney Harbour and featured surfing scenes with surfer Terry Richardson and a jam session at the famous Basement Jazz Restaurant in Sydney.
 the 1960 The News that "the Aussie film makers covered it with skill and imagination and I recommend it to jazz buffs and anyone interested in movies for their own sake." 
 MIPTV in New York. Diners Club in 1984   and eventually a limited domestic DVD release in 2007 over 25 years after it was made.  The Sydney Morning Herald critic John Shand gave the film three stars after reviewing the DVD as a solid production. He also added that the Regent Theater where it was filmed was "so wonderful" they had to demolish it in references to the theaters heritage factor. 

== References ==

{{Reflist|refs=
*   
*   
*   
*   
*   
*   
*   
*   
*   
*   
*   
*   
*   
*    
*   
}}

== Further reading ==

* 

== External links ==

*   at Screen Australia

 
 
 