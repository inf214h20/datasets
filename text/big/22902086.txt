Dirt (1994 film)
{{Infobox film
| name           = Dirt
| film name = {{Film name| traditional    = 頭發亂了
| simplified     = 头发乱了
| pinyin         = Tóu fā luàn le}}
| director       = Guan Hu
| writer         = Guan Hu
| starring       = Kong Lin Geng Le Zhang Xiaotong Ding Jiali
| music          = Guo Xiaohu
| cinematography = Yao Xiaofeng Wu Qiao
| editing        = Feng Sihai
| studio         = Inner Mongolia Film Studio
| released       =  
| runtime        =
| country        = China language       = Mandarin
}}
Dirt is a Chinese film from 1994 which depicts the nascent rock music scene of Beijing. It is considered an important example of the Sixth Generation movement that emerged in China after the Tiananmen Square protests of 1989. Zhang, Yingjin & Xiao, Zhiwei (1998). "Dirt" in Encyclopedia of Chinese Film. Taylor & Francis, p. 142-43. ISBN 0-415-15168-6.   

While the films general pessimism about youth is shared by many other Sixth Generation films, a characters choice to have a child rather than an abortion has been seen by some scholars to have played a role in why the film garnered official permission to screen abroad, a stark contrast to the similarly themed Beijing Bastards. 

== Background ==
Dirt was filmed on a shoestring budget and was funded primarily by lead actress, Kong Lin.  Dirt is often compared with another major sixth generation film about the Beijing rock scene, Zhang Yuans Beijing Bastards. In contrast to that films underground status, however, Guan Hu paid nearly US$2000 for state studio affiliation, allowing the film to be distributed in China and screened abroad with approval from state regulators. 

== Plot ==
The film follows a nurse, Ye Tong (Kong Lin), who also serves as the films narrator. One day, Ye Tong reunites with some childhood friends, including Peng Wei, a disillusioned and long-haired young man who leads a local rock band. Ye finds herself attracted to Peng Weis lifestyle, despite the admonitions of her police officer friend, Zheng Weidong. When Zheng is injured by a mutual friend, Ye finds herself increasingly attracted to the strait-laced Zheng, while also finding herself attracted to Peng. 

== Cast ==
* Kong Lin as Ye Tong, a nurse sent from her hospital in Guangzhou to train in Beijing.
* Geng Le as Peng Wei, a long-haired musician and childhood friend of Ye Tong.
* Zhang Xiaotong as Zheng Weidong, a straight-laced police officer who disapporves of Peng Weis lifestyle.
* Ding Jiali as Zheng Weiping, Weidongs sister, Weiping works at a foreign company when she discovers that she is pregnant.

== References ==
 

== External links ==
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 
 
 

 