RocknRolla
 
 
{{Infobox film
| name           = RocknRolla
| image          = Rocknrolla ver3.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Guy Ritchie
| producer       = Steve Clark-Hall Susan Downey  Guy Ritchie Joel Silver
| writer         = Guy Ritchie Chris Bridges Jimi Mistry
| music          = Steve Isles
| cinematography = David Higgs
| editing        = James Herbert
| studio         = Dark Castle Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 114 minutes
| country        = United Kingdom
| language       = English, Russian
| budget         = 
| gross          = $25,739,015 
}} comedy crime film written and directed by Guy Ritchie, and starring Gerard Butler, Tom Wilkinson, Thandie Newton, Mark Strong, Idris Elba, Tom Hardy, Jimi Mistry and Toby Kebbell. It was released on 5 September 2008 in the United Kingdom|UK, hitting #1 in the UK box office in its first week of release. 

==Plot==
In London, the British mob boss Lenny Cole (Tom Wilkinson) rules the growing real estate business using a corrupt Councillor (Jimi Mistry) for the bureaucratic services and his henchman Archy (Mark Strong) for the dirty work. A billionaire Russian businessman, Uri Omovich (Karel Roden), plans a crooked land deal, and Londons crooks all want a piece of it. Other key players include the underhand accountant Stella (Thandie Newton) and ambitious small-time crook One-Two (Gerard Butler) leading a group called the "Wild Bunch" which includes Mumbles (Idris Elba) and Handsome Bob (Tom Hardy).
 managers Mickey Chris "Ludacris" Bridges) and Roman (Jeremy Piven) to track down Johnny.

After Uris money is stolen by the Wild Bunch a second time, his assistant Victor begins to suspect that it is Lenny who has been stealing the money and purposely keeping Uris painting from him to resell it. This theory enrages Uri, who lures Lenny to a private golf game in order to break his leg, warning him to return his painting without delay.
 Matt King) happens to buy the painting from some crackheads who had just stolen it from Johnnys hideout. Cookie then gives the painting to One-Two who, in turn, offers the painting to Stella (after a sexual encounter) as a token of appreciation. After Stella leaves his flat, One-Two is surprised by Uris henchmen but is rescued, and then kidnapped, by Archy and his goons who had come looking for Uris money.

Uri wants to marry Stella, whom he has long admired. At Stellas house he proposes, but he spots the painting. Stella lies and says she has had it for years. Uri, enraged by this and realizing that Stella betrayed him, orders Victor to kill her.

Archy brings Johnny, the Wild Bunch, Mickey, and Roman to Lennys warehouse, where Johnny provokes his crippled stepfather. Before Johnny can reveal a damning truth about Lenny in front of everyone, Lenny shoots him in the stomach. Lenny then orders Johnny executed. He demands that the Wild Bunch tell him where the money is. Handsome Bob calls out to Archy and offers the documents in his jacket pocket, confirming what Johnny had been about to reveal: that the Informant|"rat" in the ranks, codenamed "Sidney Shaw", was Lenny all along. Lenny arranged with the police to routinely throw many of his associates in prison for years at a time in order to secure his own freedom and sow fear among his gang. One-Two, Mumbles, and even Archy were among the people Lenny has ratted on over the years. Archy orders Lennys men to free the Wild Bunch and has Lenny executed by drowning.

In the lift on their way to their deaths, Johnny graphically explains to Mickey and Roman that they will be executed, and the manner of their executions. His description unnerves the man whos to execute the three men, prompting him to make a move prematurely. Having also already anticipated this move, Johnny warns Mickey and Roman to intervene and kill their would-be executioner. Johnny shoots two more men waiting at the top of the lift. They overcome the last of the gangsters (with the help of the Wild Bunch) and escape.

Later, Archy picks up Johnny from Drug rehabilitation|rehab. Archy gives the painting to Johnny as a peace offering. Archy reveals to Johnny that obtaining the painting "cost a very wealthy Russian an arm and a leg". Johnny proclaims that, with his new-found freedom from addiction and his father, he will do what he could not before: "become a real RocknRolla".

==Theme==
There are three themes in RocknRolla which recur throughout the film and are familiar to Guy Ritchie audiences. The first involves the mix of urban decay and urban exploitation which is part and parcel for Guy Ritchies portrayal of life in contemporary London. The second theme is the study of the rivalry and competition between gangs of varying size and power in Londons underground and subculture. The third theme which recurs throughout the film to its conclusion is the study of a vast collection of ethnic and cultural paradigms and stereotypes along with their prejudices, large and small. These are enumerated through the many districts and suburbs of London at all levels of society, national and international, from the vastly super-rich and famous all the way down to the most lowly and desperate inhabitants of the city.
 Great Train Robbery (of 1963) typified the organized crime that flourished in Britain in the 1960s and 1970s... That world ended in the 1970s and 1980s... Since then Britains organized crime scene has diversified sharply. Whereas gangs were once extremely local--defined by their own territory--crime is now much more globalized... One visible change is the arrival of criminals with foreign origins." This foreign influence is a key theme recurring throughout Ritchies portrayal in RocknRolla of present day corruption in contemporary London.

==Cast==
 ]]

* Mark Strong as Archy, Lenny Coles right-hand man and the films narrator.
* Tom Wilkinson as Lenny Cole, a head mobster part of Londons declining old school mob regime.
* Toby Kebbell as Johnny Quid, a musician and Lennys estranged, drug-addicted stepson. It is implied that he faked his own death after anticipating that the news of his demise would cause his music sales to go up. Scottish mobster who is a leader of The Wild Bunch.
* Tom Hardy as Handsome Bob, a member of The Wild Bunch who is closeted gay and has a semi-secret crush on One-Two. 
* Idris Elba as Mumbles, One-Twos partner and a member of The Wild Bunch.
* Karel Roden as the Russian business oligarch Uri Omovich, who is based on Roman Abramovich.
* Thandie Newton as Stella, Uris accountant and One-Twos love interest.
* Dragan Mićanović as Victor, Omovichs right-hand man
* David Bark-Jones as Bertie Matt King as Cookie Geoff Bell as Fred
* Ludacris as Mickey
* Gemma Arterton as June. 
* Jeremy Piven as Roman
* Jimi Mistry as The Councillor Robert Stone as The Nightclub Bouncer date = archivedate = accessdate = 25 June 2008
}} 

A scheduling conflict prevented director Guy Ritchie from casting actor Jason Statham, who had appeared in three of his previous films. {{cite web first = title = url = archiveurl = publisher = date = archivedate = accessdate = 22 November 2007
}} 

==Production==
In May 2007, director Guy Ritchie announced the production of RocknRolla, a film with a similar theme to two of his previous films, Lock, Stock and Two Smoking Barrels (1998) and Snatch (film)|Snatch (2000). RocknRolla, written by Ritchie, was produced by Joel Silvers Dark Castle Entertainment, Ritchies own company, Toff Guy Films, French company StudioCanal    and distributed by Warner Bros..  The following June, Ritchie hired the cast for RocknRolla, and filming began on location in London on 19 June 2007.  Two scenes were filmed at Stoke Park, Buckinghamshire; the opening scene on the grass tennis courts, and the round of golf which takes place on the 21st green with the impressive clubhouse in the background. 

==Reception==

===Critical response===
Critical reaction to the film has been mixed, with 59% positive out of 134 reviews on the film review aggregator website Rotten Tomatoes.    The website Metacritic, which compiles and then aggregates major film critics reviews, gave the film a 53 out of 100, which is categorized as having mixed or average reviews.    While the films unoriginal themes were criticized, the script and direction, as well as the performances of Strong, Butler and Kebbell, were praised.

IGN gave the film a positive review with four out of five stars, saying "  hardly re-inventing the wheel with this movie, but RocknRolla is nonetheless a comedy thriller that is every bit as accomplished as his early work, and without doubt a witty, adrenalin-fuelled blast from start to finish."      Roger Ebert gave the film three stars, stating that "It never slows down enough to be really good, and never speeds up enough to be the Bourne Mortgage Crisis, but theres one thing for sure: British actors love playing gangsters as much as American actors love playing cowboys, and its always nice to see people having fun."     

===Box office===
The film hit #1 at the UK box office in its first week of release.   

The film took a total gross of United States dollar|US$25,739,015 worldwide, compared to US$83,000,000 for Snatch (film)|Snatch, seeing a modest return on the films US$18,000,000 budget.   

==Soundtrack==
{{Infobox album
| Name = RocknRolla
| Type = soundtrack
| Artist = Various artists
| Cover = 
| Released = 30 September 2008 beat
| Label = Varese Sarabande
| Chronology =Guy Ritchie film soundtracks
| Last album = Revolver (2005 film)#Soundtrack|Revolver (2005)
| This album = RocknRolla (2008) Sherlock Holmes (2009)
}}

;United Kingdom edition
# "Dialogue Clip: People Ask the Question" - Mark Strong
# "Im a Man (Bo Diddley song)|Im a Man" - Black Strobe
# "Have Love, Will Travel" - The Sonics
# "Dialogue Clip: No School Like the Old School" - Various Artists
# "Bankrobber" - The Clash
# "The Trip" - Kim Fowley
# "Dialogue Clip: Slap Him!" - Various Artists
# "Ruskies" - Steve Isles War
# "Waiting for a Train" - Flash and the Pan
# "Dialogue Clip: Junkies" - Various Artists
# "Rock & Roll Queen" - The Subways
# "The Gun" - Lou Reed
# "The Stomp" - The Hives
# "We Had Love" - The Scientists
# "Dialogue Clip: Sausage & Beans" - Various Artists Mirror in The Beat
# "Funnel of Love" - Wanda Jackson
# "Such a Fool" - 22-20s
# "Dopilsya" - Sektor Gaza
# "Negra Leono" - Miguelito Valdés

==Sequels==
Newton revealed that Ritchie stated that he hopes to extend RocknRolla into a Film trilogy|trilogy, in the event that the film receives enough positive attention.    At the end of the film there is a title card stating "Johnny, Archy and the Wild Bunch will be back in The Real RocknRolla". According to both the audio commentary and an interview with Ritchie, the second film has been written and is awaiting studio approval. 
 Joel (Silver) Sherlock Holmes The Man from UNCLE, then it may not be happening soon."   

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  

 
{{succession box
| before = Tropic Thunder
| title = List of number-one DVDs of 2009 (UK)|Number-one DVDs of 2009 (UK)
| years = 8 February
| after = Taken (film)|Taken
}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 