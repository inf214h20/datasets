Law of the Timber
{{Infobox film
| name           = Law of the Timber
| image          =
| image_size     =
| caption        =
| director       = Bernard B. Ray
| producer       = Bernard B. Ray
| writer         = James Oliver Curwood (story) Jack Natteford (screenplay)
| narrator       =
| starring       = See below
| music          = Clarence Wheeler
| cinematography = Jack Greenhalgh
| editing        = Carl Himm
| distributor    =
| released       = 19 December 1941
| runtime        = 64 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Law of the Timber is a 1941 American film directed by Bernard B. Ray.

== Plot summary ==
Henry Lorimer is one of the two owners of down-sized H & L Lumber Company camp, who returns one day to give the good news that he has negotiated a new contract with the U.S. government.

The plan is to cut down the entire tree population in Antler Valley, a place where lumbering previously has been impossible because of the resistance from the fierce inhabitants. Adams, who is to co-sign the deal, expresses his surprise over the plan.

Lorimer is determined to go theough with the plan and brings his men to the site, led by the tough foreman Hodge Mason. As the work proceeds, a number of "accidents" occur, and Lorimer believes that the Cain family is responsible.

Lorimer goes to see them but they claim to have nothing to do with it, and the reason is they know Lorimer is working for the military and a valuable cause.

A young man named John Gordon comes looking for a job, and flirts with Lorimers daughter Perry as soon as he enters the company premises. She shows no interest in the arrogant man. Still, he is hired to help lumbering.

One night, John discovers a forest fire and the men start extinguishing it. Lorimer is killed by a tree falling in the effort and Perry takes charge of the company in his place.

The camp manager, Frank Barnes, asks Perry to marry him, and Adams tries to buy her share of the company, but she refuses them both. It turns out the entire Cain family was killed in the fire, and the suspicions about their involvement are abandoned.

John is put on household work after burning his hands in the fire, and the accidents continue to happen in the forest. When a man falls to his death after his safety rope is cut, Frank talks to the sheriff. Suspicions arise that the cook, Eric, who is an immigrant, is the one responsible.

The next accident is a dynamite explosion that causes the earth to move and stop the lumber transport by train to its destination at the sawmill. Eric is cleared of suspicion, and John tries to investigate the cause of the accident. He is almost hit by a bullet when he is riding in the forest, but instead the entire supply of dynamite is ignited and explodes. The others soon suspect John of trying to sabotage the lumbering.

More dynamite is delivered, and John develops a plan to reveal who the saboteur is by claiming to have proof of the culprit. He shows them a bullet as proof and hides the bullet by his bnk bed. In the night Mason, the foreman, retrieves the bullet from its hiding place and is seen by Olaf, who knows about the plan.

Olaf tells John about it by writing him a note which he passes to him during a card game in which both John and Mason takes part. Mason picks a fight with John in order to get a chance to shoot him, but Frank breaks it up.

The next day John tells Perry that he believes Frank to be in cahoots with Mason. He also reveals to her that he is the son of late owner Hamilton, who got a share of the company as inheritance on the condition he serve as a lumberjack for some time.

Since Frank is in the forest to clear up for the transport with dynamite, john and Perry suspect he is to do further sabotage to the train transport. They ride into the forest to stop him, but Perry is rendered unconscious after a blow to the head.

Frank lets the train run towards the stack of dynamite he has prepared, and where Mason is ready. The lumber cars manage to pass before the dynamite disintegrates the rail and the rest of the train. John finds Perry and brings her back to camp. The sheriff arrives to apprehend both Frank and Mason. 

== Cast ==
*Marjorie Reynolds as "Perry" Lorimer
*Monte Blue as Hodge Mason
*J. Farrell MacDonald as Adams
*Hal Brazeale as John Gordon
*Earl Eby as Barnes
*Sven Hugo Borg as Olaf
*George Humbert as Eric
*Milburn Morante as Abe Cain
*Betty Roadman as Ma Cain Eddie Phillips as Sam Cain
*Jack Holmes as Harry Lorimer
*Zero the Dog as Zero

== Soundtrack ==
 

== External links ==
* 
* 
* 

==References==
 

 

 
 
 
 
 
 
 