Adimakal Udamakal
{{Infobox film
| name           = Adimakal Udamakal
| image          = AdimakalUdamakal.png
| caption        = Promotional Poster designed by P. N. Menon (director)|P. N. Menon
| director       = I. V. Sasi
| writer         = T. Damodaran Nalini Seema Seema Urvashi Urvashi Ratheesh Lizy Thilakan Mukesh Captain Devan
| producer       = Century Films Shyam
| cinematography = Jayaram V.
| editing        = K. Narayanan
| distributor    = Century Films
| released       =  
| runtime        = 149 minutes
| country        = India
| language       = Malayalam
| gross          = 85 lakhs
}} 1987 Malayalam Indian feature directed by Seema  in the lead roles. Ratheesh, Mukesh (actor)|Mukesh,  Urvashi (actress)|Urvashi, Jagathi Sreekumar, Captain Raju,  Lizy (actress)|Lizy,  Sukumari and  Santhakumari in major supporting roles.

==Plot==

It is a political film on trade union and its functioning in factories. The story revolves a trade union and company owner. She brings new manager Mohan Cheriyan (Mohanlal) to solve the company issues with trade union leaders. When the company was about to lay off due to union problems, Raghavan (Mammootty)suggest a new formula for solving the company issues and this leads to save the company. finally Raghavan was killed by his own men. The film also shows the nexus between Politicians and factory owners dumping workers for their selfish deeds.

==Cast==
*Mammootty ...  Raghavan
*Mohanlal ... Mohan cheriyan Nalini ... Devootty Seema ... 	Radha Urvashi ... Indu
*Ratheesh ... Sukumaran
*Balan K. Nair ... Karunakaran Nambyar
*K.P. Ummer ... Andrews
*Jagathi Sreekumar ... Mukundan Lizy ... 	Raji
*Thilakan ... Gopalan
*Sankaradi  ... 	Govindan Mukesh ... Jayan
*Captain Raju ... Sathyan
*Mammukoya ... Poker Janardanan ... 	Ramachandran
*Sukumari ... Janu Devan ... Vijayan
*Valsala Menon ... 	Madhavi
*Kundara Johny 	... Johnnie
*Jagannatha Varma ... R. K. Shenoy
*Santhakumari ... Sarada
*T. P. Madhavan ... Minister Augustine ... 	Abootty

==External links==
*  

 
 
 
 
 

 