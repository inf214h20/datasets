Hwerow Hweg
 
Hwerow Hweg is a 2002 drama film directed by Hungarian film-maker Antal Kovacs and filmed in the native Cornish language, Kernowek.

==Plot==
Jack (Robert Williams) is released from prison after being framed in a drugs set-up by his girlfriend Becky (Helen Rule). Armed with a gun he goes in search of his lover with dire consequences.

==Cast==
*Robert Williams ... Jack
*Helen Rule... Becky
*Laurens Postma ... Paul
*Phillip Jacobs ... Magnus
*Soozie Tinn ... Iris

==Release and reception==
 Celtic Film and Television Festival. 

==Trivia==
The Cornish premier was 12 April 2003 at Pennseyphun Gernewek, during a language weekend  held at Pentewan Sands holiday park
The film was also made in English although this version has never been released or shown. Its English title is Bitter Sweet.

It is the first feature length film made in Cornish language|Cornish, a Celtic language that is undergoing a revival in Cornwall.

==References==
 

== External links ==
* 

 
 