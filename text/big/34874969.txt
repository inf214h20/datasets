Middle of Nowhere (2012 film)
{{Infobox film
| name           = Middle of Nowhere
| image          = Middle of Nowhere (2012 film).jpg
| alt            = 
| caption        = 
| director       = Ava DuVernay
| producer       = Ava DuVernay
| writer         = Ava DuVernay
| starring       = Emayatzy Corinealdi Omari Hardwick Edwina Findley Sharon Lawrence Lorraine Toussaint David Oyelowo
| music          = Kathryn Bostic
| cinematography = Bradford Young
| editing        = Spencer Averick
| studio         = 
| distributor    = AFFRM Participant Media
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $200,000 
| gross          = $236,806   
}}
Middle of Nowhere is 2012 independent feature film written and directed by Ava DuVernay and starring Emayatzy Corinealdi, David Oyelowo, Omari Hardwick and Lorraine Toussaint.   The film was the winner of the Directing Award for U.S. Dramatic Film at the 2012 Sundance Film Festival.

==Plot==
A promising medical student must come to terms with her husband receiving an eight year prison sentence.

==Production==

===Development===
  wrote, directed and produced Middle of Nowhere.]] Compton and I Will Follow (2010). The director considered herself fortunate that she could still explore the themes she was interested in during her second film. 

Of her viewpoint for the film, DuVernay told Allison Samuels of The Daily Beast, "This is a story I know very well. Im from Los Angeles and I know countless women who live this kind of life every day, year after year. You see women struggling to keep it all together while a loved one is in jail. But we dont hear about them or their struggles in a way that resonates with others. Their stories are so compelling. Its as if they are in their own little world and no one else sees them. I also wanted to talk about the love between two people in a setting that isnt the norm and how they survive." 

===Filming===
Middle of Nowhere had a budget of $200,000 and was shot over nineteen days, half of the studio average of forty days, in June 2011.       DuVernay told IndieWires Claire Easton that it was difficult filming over a short period of time and thought that she could have used a couple more days.  She continued "It really would just allow us to have more takes, and explore things more. But ultimately, you know, my first film was shot in 15 days, so I gained 4 days. So, one day maybe Ill get out of the teens!" 
 Leimert Park and East Los Angeles.  DuVernay wanted to film the movie in places that were authentic to her characters. She also made sure that the locations were used in such a way that they would not detract from the story. 

==Release== Sundance Film Festival held in January 2012. The film opened in the United States on October 12, 2012.   

==Reception==

===Box office===
Melissa Silverstein from IndieWire reported that Middle of Nowhere had the top per screen average during its opening weekend.    The filmed opened in six theaters, making $67,909.  The following week, it expanded to a further sixteen screens and earned $50,554.   As of January 12, 2013, Middle of Nowhere has grossed $236,806. 

===Critical response=== review aggregation rating average of 7.8 out of 10.  Metacritic, which assigns a score of 1–100 to individual film reviews, gave Middle of Nowhere an average rating of 75 based on 19 reviews. 

Roger Ebert gave the film three stars and praised Corinealdis performance, calling it "star-making".  Kenneth Turan from the Los Angeles Times stated "Though Middle of Nowhere is very much a character piece, it benefits from some intricate plotting, and going where you think it will go is not on this films mind. When you question everything about yourself, Ruby has to ask, what do you have to hold onto? We dont often have films that ask questions like these or ones that answer them as effectively."  Middle of Nowhere was named one of The New York Times Critics Pick and Manohla Dargis commented "A plaintive, slow-boiling, quietly soul-stirring drama about a woman coming into her own, Middle of Nowhere carries the imprimatur of Sundance, but without the dreary stereotypes or self-satisfied politics that can (at times unfairly) characterize its offerings." 

===Accolades===
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Date of ceremony Category
! Recipients and nominees Result
|-
! scope="row" rowspan="5" | African-American Film Critics Association|African-American Film Critics 
| rowspan="5" | December 16, 2012
| Best Actress
| Emayatzy Corinealdi
|  
|-
| Best Independent Film
| Middle of Nowhere
|  
|-
| Best Music
| Kathryn Bostic and Morgan Rhodes
|  
|-
| Best Picture
| Middle of Nowhere
|  
|-
| Best Screenplay
| Ava DuVernay
|  
|-
! scope="row" | Alliance of Women Film Journalists 
| January 7, 2013
| Best Woman Screenwriter
| Ava DuVernay
|  
|-
! scope="row" rowspan="9" | Black Reel Awards   February 7, 2013 Outstanding Actress
| Emayatzy Corinealdi
|  
|- Outstanding Breakthrough Performance
| Emayatzy Corinealdi
|  
|- Outstanding Director
| Ava DuVernay
|  
|- Outstanding Ensemble
| Middle of Nowhere&nbsp;– Aisha Coley
|  
|- Outstanding Motion Picture
| Middle of Nowhere
|  
|-
| Outstanding Score
| Kathryn Bostic
|  
|- Outstanding Screenplay
| Ava DuVernay
|  
|- Outstanding Supporting Actor
| David Oyelowo
|  
|- Outstanding Supporting Actress
| Lorraine Toussaint
|  
|-
! scope="row" rowspan="2" | Gotham Awards  November 26, 2012
| Best Film
| Middle of Nowhere
|  
|-
| Breakthrough Award
| Emayatzy Corinealdi
|  
|-
! scope="row" | Humanitas Prize 
| September 14, 2012
| Sundance Film
| Middle of Nowhere
|  
|-
! scope="row" rowspan="4" | Independent Spirit Awards  
| rowspan="4" | February 23, 2013
| Best Female Lead
| Emayatzy Corinealdi
|  
|-
| Best Supporting Female
| Lorraine Toussaint
|  
|-
| Best Supporting Male
| David Oyelowo
|  
|-
| John Cassavetes Award
| Ava DuVernay, Howard Barish, Paul Garnes
|  
|-
! scope="row" rowspan="2" | NAACP Image Award 
| rowspan="2"| February 1, 2013 Outstanding Actress in a Motion Picture
| Emayatzy Corinealdi
|  
|- Outstanding Supporting Actor in a Motion Picture
| David Oyelowo
|  
|-
! scope="row" rowspan="2" | Sundance Film Festival  January 29, 2012
| Directing Award
| Ava DuVernay
|  
|-
| Grand Jury Prize
| Ava DuVernay
|  
|-
! scope="row"| Women Film Critics Circle 
| January 1, 2013
| Josephine Baker Award
| Middle of Nowhere
|  
|-
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 