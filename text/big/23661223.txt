The Last Manuscript
{{Infobox Film
| name           = The Last Manuscript
| image          = 
| image_size     = 
| caption        = 
| director       = Károly Makk
| producer       = 
| writer         = Tibor Déry Zoltán Kamondi Károly Makk
| narrator       = 
| starring       = Jozef Króner
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 19 November 1987
| runtime        = 107 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Last Manuscript ( ) is a 1987 Hungarian drama film directed by Károly Makk. It was entered into the 1987 Cannes Film Festival.   

==Cast==
* Jozef Króner - György Nyáry
* Aleksander Bardini - Márk (as Aleksander Bardin)
* Eszter Nagy-Kálózy - Flóra
* Irén Psota - Vica - Mrs. Nyáry
* Béla Both - Franz
* Hédi Váradi - Emilia
* Gyula Babos
* János Bán
* László Dés
* Judit Hernádi
* László Mensáros - György Nyáry (voice)

==References==
 

==External links==
* 

 
 
 
 
 
 
 