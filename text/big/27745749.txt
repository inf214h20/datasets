JoJo's Bizarre Adventure: Phantom Blood
{{Infobox film
| name           = JoJo&#39;s Bizarre Adventure: Phantom Blood
| image          = Phantom Blood poster.jpg
| alt            = 
| caption        = 
| film name      = {{Infobox name module
| kanji          = ジョジョの奇妙な冒険 ファントムブラッド
| romaji         = JoJo no Kimyō na Bōken Fantomu Buraddo
}}
| director       = Jūnichi Hayama
| producer       = 
| writer         = 
| screenplay     = Kōyō Yamada
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = Marco d Ambrosio
| cinematography = Hideo Okazaki
| editing        = 
| studio         =  
| distributor    = KlockWorx
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2007 Japanese anime film based on Part 1 of the JoJos Bizarre Adventure manga series entitled Phantom Blood. The film was released on February 17, 2007, to commemorate JoJo author Hirohiko Arakis 25th year of publishing.   Animation was handled by Studio A.P.P.P., the same staff who handled the earlier JoJos Bizarre Adventure (OVA)|JoJos Bizarre Adventure OVA based on Part 3 Stardust Crusaders. The film has never been released on home media due to Shueisha terminating their relationship with A.P.P.P. as a result of the controversy surrounding the references to the Quran in the prior OVA series.

==Story==
The film generally follows the story of the Phantom Blood manga arc, featuring Jonathan Joestar in his fight against adoptive brother Dio Brando, who after years of attempting to break Jonathan (including kissing his childhood love Erina Pendleton) poisons their father and becomes an immortal vampire through the use of a Stone Mask. Jonathan receives training in an ancient martial arts technique known as Hamon from the eccentric Italian man Will A. Zeppeli, whose father originally discovered the Stone Mask, and the two attempt to kill Dio before he uses his newfound powers to enslave humanity.    

==Cast==
*Katsuyuki Konishi as Jonathan Joestar
*Hikaru Midorikawa as Dio Brando
*Nana Mizuki as Erina Pendleton
*Rikiya Koyama as Will A. Zeppeli
*Tsutomu Isobe as George Joestar I
*Yoshisada Sakaguchi as Tonpetty

The owarai duo "Speedwagon" (Jun Itoda and Kazuhiro Ozawa) who took their name from Phantom Blood character Robert E. O. Speedwagon (who does not appear in the film) have cameo appearances.   

==Soundtrack== Voodoo Kingdom" served as the films theme song.

==Tie-ins==
A video game for the PlayStation 2 was released on October 26, 2006, that was also based on Phantom Blood. It shares most of the films cast and includes characters that did not make the final cut of the film.  

==References==
 

 

 