Are Avaaj Konacha
{{multiple issues|
 
 
 
}}

 
 
{{Infobox film
| name           = Are Avaaj Konacha अरे आवाज कोणाचा
| director       = Hemant Deodhar
| producer       = Commonman Films
| Screenplay     = Pratap Gangavane
| writer         = Adv. Anish Padekar.
| starring       = Dr. Amol Kolhe   Uday Tikekar   Tushar Dalvi   Yashodhan Bal   Aishrya Narkar   Vishakha Subhedar   Manoj Joshi   Atharv Karve  Vidyadhar Joshi
| released       =       language = Marathi country = India

}}

Are Avaaj Konacha (Marathi: अरे आवाज कोणाचा) is a 2013 Marathi movie directed by Hemant Deodhar. The film will emphasize on the life of karyakarta, their problems, the challenges they face during addressing the problems of society, political pressures.

==Plot==
Are Avaaj Konacha …. ?, a slogan best known and said during many political campaigns by several opinion leaders. For years together this slogan have bought everyone together…. One inner voice ...one slogan ....Are avaaj konacha......? 
With the change in time and high exposure the charm of our own ideologies of togetherness is lost. But even in such a lifestyle, there are few people in this society who have kept themselves rooted with utmost sincerity, no selfish motives and with absolute urge to eradicate anti social elements from the society and support everyone for their well-being, these set of people are called KARYAKARTA (Social Worker), people who take an effort to solve the unanswered problems of the common man.

==Cast==
* Dr. Amol Kolhe As Uday Sawant
* Uday Tikekar As Annasaheb 
* Tushar Dalvi As Nanasaheb
* Aishwarya Narkar As Udays Mother (Aai)
* Vishakha Subhedar As Tai 
* Manoj Joshi As Commissioner 
* Atharv Karve As Child Uday Sawant
* Vidyadhar Joshi As Advocate Bhosale
* Yashodhan Bal As Mr Ganu

==Soundtrack==
{| class="wikitable"
|-
! No. !! Title !! Singer(s) !! Length
|-
| 1 || Jaighosh Chale Tujha Morya || Neha Rajpal, Vivek Naik, Harshal Garud. || 5:43
|-
| 2 || Are Avaaj Konacha || Neha Rajpal, Krushna. || 3:16
|-
| 3 || Aamhi Mavle Mavle || Anand Shinde. || 4:14
|-
| 4 || Re Swami Raya || Hariharan (singer)|Hariharan. || 5:42
|}

 
 
 


 