Thottavadi
{{Infobox film 
| name           = Thottavadi
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       =
| writer         = Parappurathu
| screenplay     = Parappurathu
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Sreelatha Namboothiri
| music          = LPR Varma
| cinematography = P Ramaswami
| editing        = VP Krishnan
| studio         = Athullya Productions
| distributor    = Athullya Productions
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Sreelatha Namboothiri in lead roles. The film had musical score by LPR Varma.   

==Cast==
*Prem Nazir as Dr John
*Jayabharathi as Savithri
*Adoor Bhasi as Dr Pushpangadathan
*T. R. Omana as Bhagiradiyamma
*Kottarakkara Sreedharan Nair as Vaasu/P. V. Pilla
*Prem Prakash as Babu
*Adoor Pankajam as Kamalamma
*Sankaradi as Kuttan Nair
*KPAC Lalitha as Gouri
*Paravoor Bharathan as Madhavan Meena as Subhashini
*T. S. Muthaiah as Priest
*Prem Navas as Pulluvan Prema as Pulluvathi 
*Usharani as Sarasamma

==Soundtrack==
The music was composed by LPR Varma and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aavemariya || S Janaki || Vayalar Ramavarma || 
|-
| 2 || Chembakamo Chandanamo || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Gothampu vayalukal ||  || Vayalar Ramavarma || 
|-
| 4 || Pithaave || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Upaasana Upaasana || P Jayachandran || Vayalar Ramavarma || 
|-
| 6 || Veene Veene || P Susheela, Raju Felix || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 