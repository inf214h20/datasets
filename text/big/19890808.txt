Five Golden Dragons
 
 
{{Infobox film
| name           = Five Golden Dragons
| image          = Five Golden Dragons 1967 Poster.jpg
| caption        = 1967 theatrical poster
| director       = Jeremy Summers
| producer       = Harry Alan Towers
| writer         = Harry Alan Towers Edgar Wallace
| starring       = Robert Cummings
| music          = 
| cinematography = John von Kotze
| editing        = Donald J. Cohen
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom 
| language       = English
| budget         = 
}} Margaret Lee and a cast of "guest stars".      

==Cast==
* Robert Cummings as Bob Mitchell Margaret Lee as Magda
* Rupert Davies as Comm. Sanders
* Klaus Kinski as Gert
* Maria Rohm as Ingrid
* Sieghardt Rupp as Peterson
* Roy Chiao as Inspector Chiao
* Brian Donlevy as Dragon #2
* Dan Duryea as Dragon #1
* Christopher Lee as Dragon #4
* George Raft as Dragon #3 Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 178 
* Maria Perschy as Margret

==See also==
* Christopher Lee filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 
 