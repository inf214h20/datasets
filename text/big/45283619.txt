Ruggles of Red Gap (1923 film)
{{Infobox film
| name           = Ruggles of Red Gap
| image          = 
| alt            = 
| caption        = 
| director       = James Cruze
| producer       = James Cruze  Walter Woods Lois Wilson William Austin
| music          =  Karl Brown
| editing        = Dorothy Arzner 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western silent Walter Woods. Lois Wilson, William Austin. The film was released on October 7, 1923, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Edward Everett Horton as Ruggles 
*Ernest Torrence as Cousin Egbert Floud Lois Wilson as Kate Kenner
*Fritzi Ridgeway as Emily Judson
*Charles Stanton Ogle as Jeff Tuttle
*Louise Dresser as Mrs. Effie Floud
*Anna Lehr as Mrs. Belknap-Jackson William Austin as Mr. Belknap-Jackson
*Lillian Leighton as Ma Pettingill
*Thomas Holding as Earl of Brinstead Frank Elliott as Honorable George
*Kalla Pasha as Herr Schwitz
*Sidney Bracey as Sam Henshaw
*Milton Brown as Sen. Pettingill
*Guy Oliver as Judge Ballard 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 