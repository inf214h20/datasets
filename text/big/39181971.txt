That Awkward Moment
 
{{Infobox film
| name           = That Awkward Moment
| image          = That Awkward Moment.jpg
| alt            = Three men sitting on a couch, one is hugging a teddy bear, the next is resting his arms on his knees and looks surprised, and the third is eating from a large tub of ice-cream. 
| caption        = Theatrical release poster
| director       = Tom Gormican
| producer       = {{Plain list |
* Justin Nappi
* Andrew OConnor
* Scott Aversano
* Kevin Turen
* Zac Efron
}}
| writer         = Tom Gormican
| starring       = {{Plain list |
* Zac Efron
* Miles Teller
* Michael B. Jordan
* Imogen Poots
* Mackenzie Davis
* Jessica Lucas
}}
| music          = David Torn
| cinematography = Brandon Trost
| editing        = {{Plain list |
* Shawn Paper
* Greg Tillman
}}
| studio         = {{Plain list |
* Treehouse Pictures Aversano Media
* What If It Barks Films Ninjas Runnin Wild Productions
* Virgin Produced
}}
| distributor    = Focus Features
| released       =  
| runtime        = 94 minutes  
| country        = United States
| language       = English
| budget         = $8 million  , Box Office Mojo 
| gross          = $40.5 million 
}}

That Awkward Moment (released as Are We Officially Dating? in Australia and New Zealand) is a 2014 American romantic comedy film written and directed by Tom Gormican in his directorial debut. The film stars Zac Efron, Miles Teller, Michael B. Jordan, Imogen Poots, Mackenzie Davis, and Jessica Lucas. The film had its Los Angeles premiere on January 27, 2014, and it was widely released on January 31 in the United States.

==Plot==
Jason (Zac Efron) is sitting on a bench in New York City waiting for someone to arrive. A voiceover explains that he has been waiting for a long time, but to explain why, he needs to go back to the beginning. Jason begins by telling the audience that every relationship reaches the "So&hellip;" moment, where someone in the relationship will want to take the relationship to a more serious place. At that point, Jason knows the relationship is over, as he is not ready to start dating. Jason is currently working with his best friend Daniel (Miles Teller) at a publishing house designing book covers. Their friend Mikey (Michael B. Jordan), a young doctor who has been married to Vera (Jessica Lucas) since the end of college, comes to them after Vera requests a divorce. The three decide to go out to a bar and celebrate being single. The group meets up with Daniels female Wingman (social)|wingman, Chelsea (Mackenzie Davis), as they try to get Mikeys mind off of his wife. Mikey meets a girl with glasses (Kate Simses), while Jason meets Ellie (Imogen Poots), and hits it off with her after teasing another man that was trying to buy her a drink. Mikey gets the girls number, but decides not to call, resolving to work it out with his wife. Jason sleeps with Ellie, but escapes her apartment when he discovers circumstantial evidence that she may be a prostitute.

The next day, Jason and Daniel make a book cover pitch to a new author, who happens to be Ellie. Jason is able to explain himself and the two begin seeing each other on a regular basis. Meanwhile, Daniel begins to fall for Chelsea, and the two begin seeing one another. Additionally, Mikey meets with his estranged wife, and when she claims that the reason their marriage fell apart is because he is not spontaneous enough, Mikey kisses her, and the two sleep together in the hospital, reigniting their romance. All three friends attempt to keep their relationships a secret, due to their earlier agreement that they would stay single. The relationships all come to a head during Thanksgiving, a time that the three friends usually spend together, but varying circumstances keep them apart. Jason agrees to attend a funeral for Ellies recently passed father, Mikey plans a Thanksgiving dinner with his wife, and Daniel attends the traditional Thanksgiving feast with Chelsea, free to openly tell the guests about their relationship.

Jason ultimately decides not to attend the funeral, not ready to fully commit to Ellie, and their relationship falls apart. Mikey has a serious conversation with his wife during their dinner, causing her to admit that she no longer loves him. Jason and Mikey head to the dinner where they discover Daniels relationship with Chelsea, and when he denies that they are dating, his relationship falls apart as well. Although the three fight about keeping their relationships secret, they repair their friendship and try to recover their relationships. Mikey calls glasses girl from the bar, setting up a date, and Daniel reunites with Chelsea after being hit by a taxi and ending up in the hospital. However, two months later, Jason has still not reconciled with Ellie, despite still being in love with her. Mikey and Daniel help Jason by encouraging him to tell her of his love at her weekly book readings, which are sparsely attended. However, upon their arrival, the reading is full and Jason is unable to figure out a way to talk with her. He decides to make a scene by improvising a book reading, referencing their first meeting and requesting that they start over by meeting in Gramercy Park.

Returning to the beginning, Jason is waiting for Ellie in Gramercy Park. Ellie arrives and sits on the bench with him, where Jason starts their talk, beginning with, "So&hellip;"

==Cast==
 
* Zac Efron as Jason
* Miles Teller as Daniel
* Michael B. Jordan as Mikey
* Imogen Poots as Ellie Andrews
* Mackenzie Davis as Chelsea
* Jessica Lucas as Vera Walker
* Addison Timlin as Alana
* Emily Meade as Christy
* Josh Pais as Fred
* John Rothman as Chelseas Father
* Tina Benko as Ellies Mom
* Victor Slezak as Older Gentleman
* Kate Simses as Glasses
* Alysia Reiner as Amanda
* Dan Bittner as Preppy Guy
* Evelina Turen as Sophie
* Lola Glaudini as Sharon (female boss)
 

==Production== Hollywood Black List of best un-produced screenplays. In September 2013, the films title changed to That Awkward Moment. 

Zac Efron was the first cast member to be announced in August 2012, alongside an announcement that production would begin in New York City in November 2012.  Miles Teller was reported to have joined the cast in October 2012,  with Imogen Poots and Michael B. Jordan following in November 2012.  

==Release==
In June 2013, the U.S. distribution rights for That Awkward Moment were acquired by FilmDistrict with a wide release set for January 31, 2014.  Because of a transition of FilmDistrict properties to Focus Features, That Awkward Moment was later absorbed by the reconstituted Focus Features for its January 2014 release. 
 red band trailer was released on October 14, 2013. 

 

=== Critical response ===
The film received negative reviews.  , which assigns a normalized rating out of 100 based on reviews from critics, the film has a score of 36 based on 33 reviews, indicating "generally unfavorable reviews". 

===Accolades===
{| class="wikitable" width="55%"
|-
!Year
!Award 
!Category
!Nominee(s)
!Result
|- 2014  MTV Movie Awards Best Shirtless Performance Zac Efron
| 
|-
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 