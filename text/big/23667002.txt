La Valse de Paris
{{Infobox film
| name           =La Valse de Paris
| image          =File:LA VALSE DE PARIS.jpg
| image_size     =
| caption        =La Valse de Paris poster (1950)
| director       =Marcel Achard
| producer       =
| writer         = Marcel Achard
| narrator       =
| starring       =
| music          =  Jacques Offenbach, arranged and directed by Louis Beydts
| cinematography =
| editor       =
| distributor    =
| released       = 1950
| runtime        = 92 minutes
| country        = France, Italy French
| budget         =
}}
 1950 France|French Italian musical film directed by Marcel Achard.

==Cast==
*Yvonne Printemps	... 	Hortense Schneider
*Pierre Fresnay	... 	Jacques Offenbach
*Jacques Charon	... 	Jean-François Berthelier
*Noëlle Norman	... 	Marie Pradeau Robert Manuel	... 	José Dupuis
*Pierre Dux	... 	General Danicheff
*Denise Provence	... 	Brigitte
*Jacques Castelot	... 	Le duc de Morny
*Raymonde Allain	... 	Limpératrice Eugénie
*Claude Sainval	... 	Le prince
*Lucien Nat	... 	Napoléon III
*Léa Gray	... 	La duchesse de Morny
*André Roussin	... 	Henri Meilhac
*Renée Sénac	... 	La mère dHortense
*Gabriel Gobin	... 	Chavert - le régisseur (as Gobin)

==External links==
*  

 
 
 
 
 

 
 