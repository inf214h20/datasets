Chinnari Muddula Papa
{{Infobox film
| name           = Chinnari Muddula Papa
| image          =
| caption        =
| writer         = Yadavalli  
| story          = Vadde Ramesh & Vaasi Reddy
| screenplay     = Vadde Ramesh & Vaasi Reddy 
| producer       = Vadde Ramesh
| director       = Vaasi Reddy
| starring       = Jagapathi Babu Kaveri
| music          = S. P. Kodandapani Eeswar
| cinematography = A. Mohan
| editing        = G. G. Krishna Rao
| studio         = Nallini Cini Creations
| released       =  
| runtime        = 124 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Hindi movie Heyy Babyy 2007.
   

==Plot==
Anand (Jagapathi Babu), Sudhakar (Betha Sudhakar|Sudhakar), and Siva (Sivaji Raja) are three single guys who share an apartment. They are fun loving young guys who just want to chase tail. They never take up any responsibilities and they like it. One day a baby, Maya (Baby Sowjanya) is found right outside their door. They do not know who left the infant there or why. At first they try to get rid of the baby, but their conscience takes over and slowly with time, they start to care for the baby. How this little baby changes the lives of these three unassuming men forms the crux of the story.

==Cast==
*Jagapathi Babu as Anand 
*Kaveri as Sunitha
*Kota Srinivasa Rao as Patil Sudhakar as Sudhakar
*Pradeep Shakthi as Anands father
*Sivaji Raja as Sivaji
*Raja as Mayas father
*R.V. Prasad as Johnson
*Bhimeeswara Rao as Doctor
*Kallu Chidambaram as Photographer
*Rajitha as Jyothi
*Disco Shanti as Shanti
*Sandhya as Jyothis mother
*Lakshmi Priya as Anands mother
*Baby Sowjanya as Maya

==Soundtrack==
{{Infobox album
| Name        = Chinnari Muddula Papa
| Tagline     = 
| Type        = film
| Artist      = S. P. Kodandapani Eeswar
| Cover       = 
| Released    = 1990
| Recorded    = 
| Genre       = Soundtrack
| Length      = 12:56
| Label       = AVM audio
| Producer    = S. P. Kodandapani Eeswar
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

The films music was composed by S. P. Kodandapani Eeswar and released on Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers!!lyrics!!length
|- 1
|Veeche gaali  SP Balu Sirivennela Sitaramasastri|Sirivennela Sitarama Sastry
|4:14
|- 2
|Mattugundi Gammattugundi SP Balu, Chitra
|Veturi Sundararama Murthy
|4:22
|- 3
|Idi Ramba Taalam SP Balu,Chitra Jonnavithhula Ramalingeswara Rao
|4:20
|}

==References==
 

 
 
 


 