Harry's War (1981 film)
 
 
{{Infobox film| name           = Harrys War (1981)
| image          = Harrys War 1981 Theatrical Poster.jpg
| director       = Kieth Merrill
| producer       = David B. Johnston
| writer         = Kieth Merrill
| starring       = Edward Herrmann
| music          = Merrill Jenson Reed Smoot
| editing        = Bert Lovitt
| distributor    = American Film Consortium & Taft International Pictures
| released       = March 1981
| runtime        = 98
| country        = USA English
}}
Harrys War is a feature length independent film (98 minutes) from American Film Consortium and Taft International Pictures, released in 1981. Starring Edward Herrmann, Geraldine Page, Karen Grassle, David Ogden Stiers, Elisha Cook, Salome Jens and Noble Willingham. It was written and directed by Kieth Merrill.

==Plot==
After his aunt dies of a heart attack while fighting the IRS, Harry Johnson decides to take up the cause in what may seem to be an unconventional manner: he declares war on the IRS. After the funeral of Harrys aunt, Harry uses a tank to sabotage a television interview of his IRS nemesis. Several violent outcomes occur with some anti-government rhetoric.

==Cast==
*Edward Herrmann - Harry Johnson
*Geraldine Page - Aunt Beverly Payne
*Karen Grassle - Kathy Johnson
*David Ogden Stiers - Ernie Scelera, IRS District Director
*Salome Jens - Wilda Crawley, IRS Agent
*Elisha Cook, Jr. (as Elisha Cook) - Sergeant Billy Floyd
*James Ray - Croft, IRS Commissioner 
*Douglas Dirkson - Francis Kane (alias Draper), IRS Agent
*Jim McKrell - Roger Scofield, Newsman
*Noble Willingham - Major F. Andrews, United States Army
*Alan Cherry - Chester Clim, IRS Agent #1
*Bruce Robinson - IRS Agent #2

==Theatrical run==
 
This film saw a limited two-week release in theaters in March, 1981.

==Television==
SelecTV (US TV channel)|SelecTV, ONTV (pay TV)|ONTV, HBO and other premium cable movie channels ran the film in 1982, but it has never seen a network television premiere or any other broadcast since.

==Home video release== Image Home Video released a VHS version of the film on April 1, 1988 and using the same video transfer also did a DVD release on August 1, 2005. Both are now out of print.

==Legacy==
In a mid-1980s interview with David Ogden Stiers, who played the IRS director in the film, he was asked what his favorite role had been and the interviewer was expecting him to say something about his character in M*A*S*H (TV series)|M*A*S*H, but instead he paused for a second and said "There was this movie about the IRS and I was the biggest   in the office...."

==See also==
*List of American films of 1981

==References==
 

==External links==
* 
* 
*http://www.rottentomatoes.com/m/harrys_war/
*http://movies.nytimes.com/movie/94380/Harry-s-War/overview

 

 
 
 
 