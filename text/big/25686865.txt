Lady by Choice
{{Infobox Film
| name           = Lady by Choice
| image_size     = 
| image	         = Lady by Choice FilmPoster.jpeg
| caption        = 
| director       = David Burton
| producer       = Robert North
| writer         = Dwight Taylor (story) Jo Swerling
| narrator       = 
| starring       = Carole Lombard May Robson
| music          = Louis Silvers
| cinematography = Ted Tetzlaff
| editing        = Viola Lawrence
| studio         = 
| distributor    = Columbia Pictures
| released       = October 15, 1934
| runtime        = 74-87 minutes; US 76 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1934 romantic drama film starring Carole Lombard as a fan dancer and May Robson as a homeless drunk who is asked to pose as the dancers mother for a publicity stunt, with unexpected consequences. Promoted as a follow-up to Frank Capras 1933 hit Lady for a Day (1933) resembles the earlier film only in its choice of leading lady, May Robson. 

==Plot==
Alabam Lee (Carole Lombard) is given a suspended sentence by Judge Daly (Walter Connolly). To help improve her image, her publicist Front OMalley (Raymond Walburn) comes up with the zany idea of "adopting" a mother. Her manager, Charlie Kendall (Arthur Hohl), thinks it is a great idea, so they head off to the nearest old ladies home with newspaper reporters and photographers in tow.
 Roger Pryor); the latter was asked by his now-deceased father to look after Patsy.

Patsy is touched by Alabams kind nature, and starts to reform both herself and her new daughter. She curtails her drinking and finds out that Kendall has been skimming off most of Alabams nightclub salary; Alabam fires Kendall as a result. Patsy gets in a crap game and wins $7000, which she passes off as an inheritance. The money comes in handy, as Alabam is now out of work.

She also gets Alabam to take acting, dancing, and elocution lessons, while she goes to see theatrical producer David Opper (Henry Kolker). It turns out that Patsy was once a star whose success made Opper a lot of money many years ago. Opper reluctantly agrees to give Alabam an audition, but she fails to impress him.

When Johnny drops by to see how Patsy is doing in her new surroundings, he meets Alabam and soon falls in love with her. Seeing that he is wealthy, Alabam decides the best way to provide for her now-uncertain future is to extract as much "loan" money as she can from him. When Patsy realizes what her protegee is doing, the two women quarrel, and Patsy walks out of Alabams life.

Johnny asks Alabam to marry him, then tells her that his mother has promised to disown him and leave him a poor man if they marry. Alabam, who has fallen in love despite herself, is relieved; now nobody will think she is marrying him for his money. After Patsy and Johnnys mother have been to Judge Daly asking him to stop this relationship,  Judge Daly calls Alabam into his office and threatens to unsuspend her sentence, but she is unfazed. However, when he tells her that Johnnys career and social standing will be ruined by her past, she gives up. She goes back to Kendall.

Patsy, who was initially also opposed to the marriage, changes her mind when she sees that Alabam is really in love. She reveals to Alabam that she was once in the same situation with Johnnys father. They broke up, but Patsy has regretted it ever since and does not want the younger woman to repeat her mistake.

Alabams fan dance at the nightclub is interrupted by the police, who take her to Judge Dalys office, where she is confronted by Daly, Patsy, and Johnny. Alabam gives in and embraces Johnny.

==Cast==
* Carole Lombard as Alabam Lee
* May Robson as Patricia "Patsy" Patterson Roger Pryor as Johnny Mills
* Walter Connolly as Judge Daly
* Arthur Hohl as Charlie Kendall
* Raymond Walburn as Front OMalley James Burke as Sergeant Brannigan
* Henry Kolker as David Opper
* Mariska Aldrich as Lucretia
* John T. Doyle as Walsh (as John Doyle)

==References==
 	

==External links==
*  
*  
*  

 

 
 
 
 
 
 