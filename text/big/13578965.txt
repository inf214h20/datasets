Taaqatwar
 
{{Infobox film
| name           = Taaqatwar
| image          = Taaqatwar.jpg
| image_size     = 
| caption        = CD soundtrack
| director       = David Dhawan
| producer       = Bholi Jagdish Raaj   Bobby Raj  Manu Talreja 
| writer         = David Dhawan Anwar Khan
| narrator       =  Govinda   Anita Raj  Neelam Kothari
| music          = Anu Malik
| cinematography = Shyam Rao Shiposkar
| editing        = David Dhawa Raaj 
| distributor    = Raaj N Raaj International Ultra Pictures 
| released       = 16th June 1989
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Govinda worked with David Dhawan for the first time in Taaqatwar. He then formed a successful collaboration with David Dhawan and went onto act in 17 movies directed by him, most of which were comedy films and were successful.

==Cast==
*Sanjay Dutt as  Police Inspector Amar Sharma  Govinda as  John DMello 
*Anita Raj as Anju Khurana 
*Neelam Kothari as John D Mellos girlfriend
*Paresh Rawal as  Ganguram Tulsiram  
*Tanuja as Mrs. Sharma 
*Anil Dhawan as  Peter DMello 
*Anupam Kher as  Municipal Officer Sharma 
*Gulshan Grover as  Khuranas son 
*Shakti Kapoor as  Munjal Khurana

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "John Dmellow Ding Dong wala"
| Amit Kumar
|-
| 2
| "Chadh Gayi Chadh Gayi"
| Amit Kumar, Alisha Chinai
|-
| 3
| "Aaiye Aap Ka Intezar Tha"
| Anu Malik, Alisha Chinai
|-
| 4
| "Choron Ki Toli Leke"
| Shabbir Kumar,  Amit Kumar, Anupama Deshpande, Chandrani Mukherji
|-
| 5
| "Dhak Dhak Karti Hai"
| Anuradha Paudwal
|}

== External links ==
*  

 

 
 
 
 
 
 


 
 