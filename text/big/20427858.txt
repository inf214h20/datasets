Don Camillo in Moscow
 
{{Infobox film
| name           = Il compagno Don Camillo
| image          = Il compagno Don Camillo.jpg
| image_size     =
| caption        =
| director       = Luigi Comencini
| producer       =
| writer         = Giovannino Guareschi (novel), Leo Benvenuti, Piero De Bernardi
| narrator       =
| starring       = Fernandel, Gino Cervi, Gianni Garko, Graziella Granata
| music          = Alessandro Cicognini
| cinematography = Armando Nannuzzi
| editor       = Nino Baragli
| distributor    =
| released       = September 18, 1965
| runtime        = 109 min
| country        = Italy Italian
| budget         =
}}
 1965 Italy|Italian comedy film directed by Luigi Comencini. Its the fifth and penultimate film in the Don Camillo series.

==Plot==
After receiving a tractor as a gift from the kolkhoz of an unnamed Soviet village, communist mayor Peppone plans to twin Brescello with the unnamed town. After some failed attempts to block the majors plan, Don Camillo ultimately tricks Peppone into including him (under a false name and with forged papers) among the Italian communist representatives going on the other side of the iron curtain to attend the twinning ceremonies. Only Peppone and the other comrades from Brescello will know the priests real identity. During the Russian stay they will face a series of situations that will show them both the political contradictions of Soviet Russia and the normal life of its common people. 

==Cast==
*Fernandel ...  Don Camillo
*Gino Cervi ...  Giuseppe Peppone Bottazzi
*Leda Gloria ...  Maria Bottazzi
*Gianni Garko ...  Scamoggia
*Saro Urzì ...  Brusco
*Graziella Granata ...  Nadia Paul Muller ...  Le pope
*Marco Tulli ...  Smilzo
*Jacques Herlin ...  Perletti
*Silla Bettini ...  Bigio
*Aldo Vasco ...  Un camarade
*Alessandro Gottlieb ...  Ivan
*Mirko Valentin ...  Le faux russe
*Ettore Geri ...  Oregov
*Margherita Sala ...  La femme dIvan

==External links==
*  

==References==
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 