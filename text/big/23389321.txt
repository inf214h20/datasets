Such Good Friends
 
{{Infobox film
| name           = Such Good Friends
| image          = SuchGoodFriendsPoster.jpg
| alt            = 
| caption        = Theatrical release film poster by Saul Bass
| director       = Otto Preminger
| producer       = Otto Preminger Esther Dale
| based on       =  
| starring       = Dyan Cannon James Coco Jennifer ONeill
| music          = Thomas Z. Shepard
| cinematography = Gayne Rescher
| editing        = Harry Howard Otto Preminger Films
| distributor    = Paramount Pictures
| released       =  
| runtime        = 102 minutes  
| country        = United States
| language       = English
}}
Such Good Friends is a 1971 American comedy-drama film directed by Otto Preminger. The screenplay by Esther Dale (a pseudonym for Elaine May) is based on the novel of the same title by Lois Gould. The film stars Dyan Cannon, James Coco, and Jennifer ONeill.

==Plot==
Manhattanite Julie Messinger, a complacent housewife and mother of two raucous young sons, is married to Richard, a chauvinistic and self-centered magazine art director and author of a best-selling childrens book. When he falls into a coma during minor surgery to remove a nonmalignant mole on his neck, Julie learns from his doctor, Dr. Timmy Spector, that another surgeon nicked his artery, necessitating a blood transfusion to which he had a rare allergic reaction. The following day, Julie is told Richard has overcome the reaction, but his liver has sustained serious damage requiring immediate treatment. In quick succession, all his organs begin to fail.

While trying to comfort Julie, family friend Cal Whiting reveals his girlfriend Miranda has confessed to having had a lengthy affair with Richard. Distressed by the news, Julie seeks advice from her egocentric mother but finds herself unable to discuss her husbands infidelity. She decides to confront Miranda and asks her what future she anticipated having with her husband. Miranda confesses she and Richard are deeply in love and have discussed marriage, although thus far she has been unable to make such a permanent commitment.

Julie begins to unravel emotionally. She visits Cal, whose attempted seduction of her fails due to impotence. At the hospital, she tells the unconscious Richard she will never divorce him and vows to ruin his reputation. Timmy invites her to his apartment for drinks and admits he was aware of Richards affair not only with Miranda, but with other women as well, and kept them secret out of a sense of loyalty to his friend. Stunned and confused, Julie lashes out at Timmy, then seduces him, and he succumbs to her advances. 

At home later that evening, Julie finds a black book in Richards desk and realizes it contains coded data about his numerous extramarital affairs, many of them with her friends. She gives it to Cal, who then shows it to Miranda to prove she was just one of Richards many conquests. The following day, Richard goes into cardiac arrest, and Julie realizes she wants him to survive despite his betrayal of her. When Timmy reports her husband has died, a grieving Julie takes her sons for a walk in Central Park to contemplate their future.

==Cast==
 
* Dyan Cannon as Julie Messinger 
* James Coco as Dr. Timmy Spector 
* Jennifer ONeill as Miranda Graham 
* Ken Howard as Cal Whiting 
* Nina Foch as Mrs. Wallman 
* Laurence Luckinbill as Richard Messinger 
* Louise Lasser as Marcy Berns 
* Burgess Meredith as Bernard Kalman 
* Sam Levene as Uncle Eddie  William Redfield as Barney Halsted 
* Elaine Joyce as Marian Spector 
* Doris Roberts as Mrs. Gold 
* Virginia Vestoff as Emily Lapham
* Oscar Grossman as the doorman
 

==Production==
Through his son Erik Lee Preminger|Erik, who was working as his story editor, producer Otto Preminger heard about a manuscript by Lois Gould that was rumored to be a hot property. He negotiated with the author and purchased the film rights for $200,000 in February 1970, three months before the book was published. 
 feminist who was more psychologically attuned to the character than he thought was necessary. He then hired Joan Didion and John Gregory Dunne and worked with them for several months. Finally, in early 1971, Elaine May, his original choice for screenwriter, became available. May worked on the script for ten weeks, although Preminger found it difficult to adjust to her method of writing. The two would meet for a story conference, then May would disappear and remain incommunicado for two weeks or so, finally emerging with a substantial part of the screenplay completed. Preminger would give her notes and she would disappear again, and this routine continued until the script was finished. Not wanting her name attached to work started by others, May insisted she did not want screen credit and used the pseudonym Esther Dale, the name of a Hollywood character actress, instead of her own. Preminger later used Mays involvement in the film to help promote it, a move the screenwriter resented, as she felt he was "more honorable than that."  

Preminger and leading lady Dyan Cannon clashed throughout filming. She was constantly late, one of the directors pet peeves, and the two disagreed about everything about her character, from how she should be portrayed to how she should be dressed. Uncomfortable with the directors perception of Julie, the actress frequently tried to incorporate some of her own vision into her interpretation, resulting in loud on-set arguments that left Cannon feeling alone, self-conscious, and very vulnerable. Upon the films completion, the two vowed never to work with each other again. 

==Critical reception==
The film earned mixed reviews at the time of its release, and some felt it was better than the failed films Preminger did between 1965-70. Roger Ebert of the Chicago Sun-Times called the film "a hard, unsentimental, deeply cynical comedy" and "Premingers best film in a long time, probably since Anatomy of a Murder in 1959." He added, "There are funny lines in the movie, but they are rarely allowed to be merely funny; they are also intended to hurt. People hurt and insult one another because, we sense, attack is the best form of defense inside this carnivorous society. Some of the dialog is in appallingly bad taste, and some of the critics have blamed the bad taste on Preminger, but it would have taken a lesser director to leave it out. The vulgarity belongs there because the movie is as tough as the people its about."  
 Hurry Sundown? — when hes trying to be serious."  

Tony Mastroianni of the Cleveland Press called it "one of those sick-funny films that asks you to laugh at tragedy and gets away with it." He continued, "It undoubtedly will offend many and for a number of reasons. It has a brand of caustic wit that somehow surmounts situations that are a blend of soap opera maudlin and ribald coarseness. The picture takes on such institutions as marriage, medicine and friendship and treats them all pretty roughly. What succeeds is a barrage of bright, witty, trenchant lines written by Elaine May operating under the pseudonym of Esther Dale. Director Otto Preminger, whose recent films looked as though they were directed by an ax murderer, does a better job this time out. There are still scenes that are all surface, some that are just crudely done. But in others, notably those involving large groups of people, he works out an interplay of parts that results in fascinating moments of counterpoint."  
 cameos are shunted too quickly out of sight."  

TV Guide rated the film one star, saying it "aimed at being contemporary but turned out contemptible. It does, however, provide a look at a tough-skinned New York lifestyle that Big Apple resident Preminger well understood."  

==Awards and nominations==
Cannon was nominated for the Golden Globe Award for Best Actress – Motion Picture Drama but lost to Jane Fonda in Klute.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 