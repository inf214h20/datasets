Vaaname Ellai
{{Infobox film
| name = Vaaname Ellai
| image = 
| caption = Official DVD Box Cover
| director = K. Balachander   
| producer = Rajam Balachander Pushpa Kandaswamy
| writer = K. Balachander
| narrator =
| starring = Anand Babu   Bhanupriya   Ramya Krishnan   Madhoo   Rajesh   Babloo Prithviraj   Vishali kannadasan Maragadha Mani
| cinematography = Raghunatha Reddy
| editing = Ganesh Kumar
| studio = Kavithalayaa Productions
| distributor = Kavithalayaa Productions
| released = 22 May 1992
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}

Vaaname Ellai is a 1992 Indian Tamil language Drama film directed by Kailasam Balachander|K. Balachander starring Anand Babu, Ramya Krishnan, Madhoo, Vishali Kannadasan, Rajesh and Babloo Prithviraj. 

The story involves five characters from different background, who got vexed with their lives, jointly decide to end their lives together, but begin a short journey of 100 days to live together happily before dying. K. Balachander won the Filmfare Award for Best Director – Tamil.

{{Infobox album  
| Name        = Vaaname Ellai
| Type        = soundtrack
| Artist      =
| Cover       =
| Caption     =
| Released    =    
| Recorded    =
| Genre       =
| Length      = Tamil
| Label       =
| Producer    =
| Reviews     =
| Compiler    =
| Misc        =
}}

== Plot ==

Five young people decide that life is not worth living anymore for various reasons.

Anand Babu, the son of a judge is an idealist and never suspects that the many gifts that are showered upon him by his father were things received as bribes. One day he shoots a video song about corruption in the society dressed as a robot. Seeing this, his friend remarks that his own father is very corrupt. The angered Anand Babu beats up his friend and challenges him that if that were true, he would commit suicide out of shame. Back at home, he sees his father taking a large amount of money as bribe for a lawsuit. The shocked Anand Babu argues with his father over his corrupt practices. His mother budges in and starts justifying corruption which has brought the family luxuries like the bungalow, car and Anands Yamaha bike. Besides, a large amount of money is needed for the marriage dowry of their two daughters (Anands sisters). Anand is unable to bear this and immediately sets fire to his new Yamaha bike. He leaves home.

Babloo is the only son of a rich business man (Cochin Haneefa). He is motherless. He is in love with the computer operator, Suguna (Vishali Kannadasan) working in his fathers office. He is a happy-go-lucky guy. One day his father comes to know of their love and despises it as he has big plans to marry his son to the daughter of a rich, potential business partner. He threatens Babloo to not marrying Suguna. But the much pampered Babloo is adamant in marrying her. Meanwhile, Y. Vijaya, the widowed mother of Suguna also objects to their love, fearing for problems arising due to the difference in their social status. Cochin Haneefa comes up with a plan to stop the couple from marrying. He convinces Y. Vijaya in remarrying him thereby making Babloo and Suguna as step-siblings. The couple is heartbroken. Babloo wants to commit suicide but Suguna wants to continue to live as the new step-daughter of the rich business man. She soon shows her own brand of revenge by heavy partying and boozing and having one night stands. Whenever arrested she proudly proclaims being the rich mans daughter.

Madhoo was forced into marriage with a very old but rich man and Ramya is a gang-rape victim. The other is a poor unemployed youth of high caste who does not get employment because of his forward caste status.

They all meet at suicide point and decide to live a happy life for 100 days and then end it all. In those 100 days, they have all sorts of fun. They also sing mourning songs for their own death. But one of them, the unemployed, secretly tries to change his friends minds away from suicide. But they tell that their mind is made up and he can leave if he chooses to. But he prepones his suicide and informs them that he did so make them realise that death is no joke and if his friends changed their minds, his death would not be in vain. They soon start getting doubts about going ahead with their suicides.

Meanwhile, they find a baby at their doorstep and have no choice but to take care of the child. They get emotionally close to the child. Finally the dead friends dad comes and meets them having tracked his sons letters with great difficulty. When informed of his sons death he mourns and accuses the remaining youths of being the cause of his death. The four decide to die immediately on hearing this. As they go to the suicide point, they meet their dead friend there. He says that he had faked his own death as well as arranged for the child and his father to dissuade them. His father takes the child to an orphanage and they meet people with various physical deformities trying to live a fruitful and cheerful life. After gaining inspiration from many of the disabled persons who had achieved things and also from the advice they give them, the four youths decide to life a long and brave life.

==Music== Maragadha Mani.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vairamuthu || 03:05
|-
| 2 || Jana Gana Mana || S. P. Balasubrahmanyam, K. S. Chithra || 05:37
|- Maragadha Mani, K. S. Chithra || 04:36
|-
| 4 || Nadodi Mannargale || S. P. Balasubrahmanyam, K. S. Chithra || 05:01
|-
| 5 || Nee Aandavana || S. P. Balasubrahmanyam, K. S. Chithra || 05:10
|-
| 6 || Siragillai || K. S. Chithra || 04:26
|-
| 7 || Sogam Eni Ellai || S. P. Balasubrahmanyam || 04:26
|- Maragadha Mani || 01:03
|}

== References ==
 

==External links==
*  in Raaga.com

 
 

 
 
 
 
 
 