Beloved Infidel
 
{{Infobox film
| name           = Beloved Infidel
| image          = Belovedinfideltc.jpeg
| caption        = Original film poster Henry King
| producer       = Jerry Wald
| based on       =  
| screenplay     = Sy Bartlett
| starring       = Gregory Peck Deborah Kerr Eddie Albert
| music          = Franz Waxman
| cinematography = Leon Shamroy
| editing        = William H. Reynolds
| distributor    = 20th Century Fox
| released       =  
| runtime        = 123 minutes
| country        = United States
| awards         =
| language       = English
| budget         = $2,340,000 
}}
 Henry King and produced by Jerry Wald from a screenplay by Sy Bartlett, based on the memoir by Sheilah Graham and Gerold Frank. The music score was by Franz Waxman, the cinematography by Leon Shamroy and the art direction by Lyle R. Wheeler and Maurice Ransford.

The film stars Gregory Peck and Deborah Kerr with Eddie Albert and Philip Ober.

==Plot==
Writer F. Scott Fitzgerald (Gregory Peck) is a washed-up has-been; his wife is in a mental institution and he has turned to alcohol. His only interests in life are his affair with radio host Sheilah Graham (Deborah Kerr), and his latest novel – which ultimately remains unfinished.
 

==Cast==
* Gregory Peck as F. Scott Fitzgerald
* Deborah Kerr as Sheilah Graham
* Eddie Albert as Bob Carter – Sheilahs friend
* Philip Ober as John Wheeler
* Herbert Rudley as Stan Harris John Sutton as Lord Donegall
* Karin Booth as Pierce
* Ken Scott as Robinson

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 