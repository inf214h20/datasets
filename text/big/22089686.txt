A Lawless Street
{{Infobox film
| name           = A Lawless Street
| image          = A_Lawless_Street_Poster.jpg
| border         = yes
| caption        = Theatrical release poster Joseph H. Lewis
| producer       = Harry Joe Brown
| screenplay     = Kenneth Gamet
| story          = Brad Ward
| starring       = {{Plainlist|
* Randolph Scott
* Angela Lansbury
}}
| music          = Paul Sawtell
| cinematography = Ray Rennahan
| editing        = Gene Havlick
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film Joseph H. Lewis and starring Randolph Scott and Angela Lansbury. The film is also known as The Marshal of Medicine Bend in the United States.   

==Plot==
The marshal of Medicine Bend, Calem Ware (Randolph Scott), tries to keep peace in a lawless town whilst trying to prevent himself from being killed. The arrival of a show troupe reunites the marshal with someone from his past, leading to a showdown with his would-be killers and his old flame, Tally Dickinson (Angela Lansbury).

==Cast==
* Randolph Scott as Marshal Calem Ware  
* Angela Lansbury as Tally Dickinson
* Warner Anderson as Hamer Thorne
* Jean Parker as Cora Dean
* Wallace Ford as Dr. Amos Wynn John Emery as Cody Clark James Bell as Asaph Dean
* Ruth Donnelly as Molly Higgins
* Harry Antrim as Mayor Kent 
* Michael Pate as Harley Baskam 
* Don Megowan as Dooley 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 