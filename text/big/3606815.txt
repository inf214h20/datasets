The Star Packer
{{Infobox Film
| name           = The Star Packer
| image          = The Star Packer FilmPoster.jpeg
| caption        = Film poster
| director       = Robert N. Bradbury
| starring       = {{plain list|
* John Wayne
* Verna Hillie
* George "Gabby" Hayes
* Yakima Canutt
* Earl Dwire
}}
| distributor    = Monogram Pictures
| released       =  
| runtime        = 53 minutes
| country        = United States
| language       = English
}} Western film directed by Robert N. Bradbury and starring John Wayne, George "Gabby" Hayes, Yakima Canutt, and Verna Hillie.

== Synopsis ==
U.S. Marshal John Travers (John Wayne), becomes sheriff of a town where several murders have occurred, hoping to flush out an outlaw chieftain known only as "The Shadow".

== Cast ==
* John Wayne as U.S. Marshal John Travers
* George "Gabby" Hayes as Matt Matlock (as George Hayes)
* Verna Hillie as Anita Matlock, Matt Matlocks Niece
* Yakima Canutt as Yak, Travers Indian Sidekick
* Billy Franey as Henchman in the stump
* Eddie Parker as Henchman Parker (as Ed Parker)
* Earl Dwire as Henchman Mason
* Thomas G. Lingham as Sheriff Al Davis (billed as Tom Lingham)

== See also ==
* John Wayne filmography

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 