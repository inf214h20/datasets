Dard Ka Rishta
 
{{Infobox film
| name           = Dard Ka Rishta
| image          = Dard Ka Rishta.Jpg
| caption        = T-Series CD Cover
| director       = Sunil Dutt
| producer       = Sunil Dutt
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sunil Dutt Ashok Kumar  Reena Roy   Smita Patil
| music          = Rahul Dev Burman
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}} Indian Bollywood film produced and directed by Sunil Dutt who also stars. Smita Patil, Reena Roy and Ashok Kumar play pivotal roles.


== Plot==
Ravi Kant (Sunil Dutt) is a doctor who lives in New York with his wife Anuradha (Smita Patil), also a doctor. Anuradha is doing research into leukemia. Ravi is nostalgic about India and decides to return there when he receives an offer for a position as the head of surgery from Tata Memorial Hospital in Bombay. Anuradha wants to continue her research and does not want to go to India. Ravi returns to India after a divorce with Anuradha. Anuradha discovers that she is pregnant with Ravis child but Ravi is unaware of the fact. In India, while treating one of his patients Bhardwaj, he meets his daughter Asha (Reena Roy). Just before his death, Bhardwaj  is promised by Ravi that he will marry Asha. Asha and Ravi are married. Asha dies while giving birth to their daughter (Kushboo Sundar|Khushboo). Soon after Khushboos eleventh birthday, she is diagnosed with leukemia. On a recommendation from a fellow doctor at Tata Memorial, Ravi takes her to a hospital in New York where Anuradha is her doctor. To cure her cancer, Khushboo must have a bone marrow transplant from a donor with matching blood group and genes. Shashi (Anuradhas son) is found to have a perfect match and he donates the marrow. Ravi discovers that Shashi is the son of him and Anuradha. In the final scene, everyone comes together at the airport when Ravi, Khusboo and Shashi are coming to India. Anuradha joins them as well.

== Reception==
The movie was a hit. Dutt said that he would donate his entire earnings from the movie to the benefit of cancer patients and autistic children in India. The movie raised awareness of the cause of cancer in India. 

==Cast==
* Sunil Dutt  as  Dr. Ravi Kant Sharma
* Reena Roy  as  Asha Bhardwaj
* Smita Patil  as  Dr. Anuradha Khushboo  as  Ravi and Ashas teenage daughter
* Tirlok Malik  as  Dr. Khan
* Naaz Hussein  as  Dr. Najma
* Padmini Kolhapure  as  Guest artist
* Ashok Kumar

==External links==
* 

 
 
 
 
 
 

 