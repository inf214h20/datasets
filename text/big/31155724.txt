Ollathumathi
{{Infobox film 
| name           = Ollathumathi
| image          =
| caption        =
| director       = KS Sethumadhavan
| producer       = MP Chandrasekhara Pillai
| writer         = Chandran Jagathy NK Achari (dialogues)
| screenplay     = Jagathy NK Achari Sathyan Madhu Madhu Sheela
| music          = LPR Varma
| cinematography = Melli Irani
| editing        = Thankaraj
| studio         = Mathaji Pictures
| distributor    = Mathaji Pictures
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, Madhu and Sheela in lead roles. The film had musical score by LPR Varma.   

==Cast==
 
*Prem Nazir Sathyan
*Madhu Madhu
*Sheela
*Adoor Bhasi
*Muthukulam Raghavan Pillai
*Sankaradi
*Shobha Shubha
*T. R. Omana
*T. S. Muthaiah
*Adoor Pankajam
*Bahadoor GK Pillai
*Indirarani
*KM Warrier
*K. P. Ummer
*Kaduvakulam Antony
*Kamaladevi
*Kottarakkara Sreedharan Nair
*MG Menon Meena
*S. P. Pillai
*Susheela
 

==Soundtrack==
The music was composed by LPR Varma and lyrics was written by Vayalar Ramavarma, Kumaranasan, Ramachandran, P. Bhaskaran, SK Nair and Thikkurissi Sukumaran Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ajnaatha Sakhi || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Ee Valliyil Ninnu Chemme || AP Komala, Renuka || Kumaranasan || 
|-
| 3 || Maaran Varunnennu  || P. Leela, B Vasantha || Ramachandran || 
|-
| 4 || Njanoru Kashmeeri Sundari || AP Komala, B Vasantha, Renuka || P. Bhaskaran || 
|-
| 5 || Santhaapaminnu Naattaarkku || Kamukara || SK Nair || 
|-
| 6 || Shankuppilla Kannirukkumbol || Sarath Chandran || Thikkurissi Sukumaran Nair || 
|-
| 7 || Unni Virinjittum || Kamukara || SK Nair || 
|}

==References==
 

==External links==
*  

 
 
 

 