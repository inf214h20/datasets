The Keyhole
{{Infobox film
| name           = The Keyhole
| image          = The-keyhole-1933.jpg
| image_size     = 
| caption        = lobby card
| director       = Michael Curtiz
| producer       = Hal B. Wallis
| writer         =  
| narrator       = 
| starring       = Kay Francis George Brent
| music          = W. Franke Harling
| cinematography = Barney McGill
| editing        = Ray Curtiss
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| website        = 
}}

The Keyhole  is a 1933 film directed by Michael Curtiz.

==Plot==
 

==Cast==
* Kay Francis as Ann Brooks 
* George Brent as Mr. Neil Davis 
* Glenda Farrell as Dot 
* Monroe Owsley as Maurice Le Brun 
* Allen Jenkins as Hank Wales 
* Helen Ware as Portia Brooks 
* Henry Kolker as Schuyler Brooks 
* Ferdinand Gottschalk as Brooks Lawyer

==References==
 
 

==External links==
*  
*  
*  

 

 
 
 
 
 


 