Kusthi
{{Infobox film|
| name = Kusthi
| image =
| caption = Raj Kapoor
| writer = Prabhu Karthik Karthik Vadivelu Manya Flora Flora Vijayakumar Vijayakumar Radha Aarthi Manobala Chitti Babu Y. G. Mahendran
| producer =
| music = D. Imman
| editor =
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
}} Prabhu and Karthik came together after Agni Natchathiram, which was a blockbuster at the box office. Unfortunately, this movie failed at the box office, even though we have two established actors in the movie.

==Plot==

The movie begins with Jeeva (Prabhu), running away from his village to Chennai fearing a possible marriage with the daughter of a local chieftain (Radharavi), arranged by his father. He joins his old-friend Velu (Vadivelu), who runs an eatery in the city. Meanwhile, he saves a youth (Mahandahi Shankar), a henchman of a local goon Singam (Karthick) from some gangsters. Thus he gets the acquaintance of Singam. Meanwhile Singam come across Abi (Flora), a research scholar and  Jeeva meets Divya (Manya). Both fall in love with them. Enters another goon (Raj Kapoor), who leaves his stolen money with Singam. He runs behind Singam to get the money back.

Meanwhile Radharavi and his men come down to Chennai in search of Jeeva. Following a mishap, Jeeva admits Singam and an old woman Lakshmi (Latha) in a hospital. Mistaking Jeeva to be their missing grandson, Vijayakumar (father of Latha) and his relatives take him to Ooty. Fearing the Nattamai, Jeeva decides to go to Ooty and acts as their grandson.

Comes Singam with a plan to murder Vijayakumar. However he manages to win the heart of the family members and they arrange for Jeeva and Singams wedding with Vijayakumars grand daughters Abi and Divya. Raj Kapoor and Radharavi enter the scene and all confusion begins. The rest is all but how both Jeeva and Singam succeed in walking away with Abi and Divya.

==Cast==
 Prabhu as Jeeva Karthik as Singam
*Vadivelu as Velu Manya as Divya Flora as Abi Vijayakumar
*Radha Ravi Aarthi
*Manobala Chitti Babu
*Y. G. Mahendran

 
 
 
 


 