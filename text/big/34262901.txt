Berlin Correspondent
{{Infobox film
| name           = Berlin Correspondent
| image          =
| caption        =
| director       = Eugene Forde
| producer       = Bryan Foy
| writer         =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 20th Century Fox
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
}}

Berlin Correspondent is a 1942 American film.  

Dana Andrews portrays an American radio correspondent reporting from within Nazi Germany, whose principal source of information is an elderly philatelist.  His reports prove so embarrassing to the regime that Captain von Rau sends his own fiancée, Karen Hauen (Virginia Gilmore), to compromise the reporter.  As the philatelist is sent off to a concentration camp, it develops that she and the reporter are falling for each other, and the elderly source was actually her own father.

== Cast ==

* Virginia Gilmore as Karen Hauen
* Dana Andrews as Bill Roberts
* Mona Maris as Carla
* Martin Kosleck as Captain von Rau
* Sig Ruman as Dr. Dietrich
* Kurt Katch as Weiner
* Erwin Kalser as Mr. Hauen
* Torben Meyer as Manager William Edmunds as Hans Gruber
* Hans Schumm as Gunther
* Leonard Mudie as George, English Prisoner
* Hans von Morhart as The Actor
* Curt Furburg as Doctor  Henry Rowland as Pilot
* Christian Rub as Prisoner

== External links ==
*  

 
 
 
 
 
 

 