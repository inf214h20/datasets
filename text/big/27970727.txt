The New Deal Show
 
{{Infobox Hollywood cartoon|
| cartoon_name = The New Deal Show
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist = Lillian Friedman Myron Waldman
| voice_actor = Mae Questel
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = 22 October 1937
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}
The New Deal Show is a 1937 Fleischer Studios animated short film starring Betty Boop

==Synopsis==
Betty Boop produces pet show in which the pets use unusual devices to assist them in their normal behavior. Some ideas copied from Betty Boops Crazy Inventions (1933).

==External links==
* http://www.bcdb.com/cartoon/4368-New_Deal_Show.html
* http://www.imdb.com/title/tt0029302/
 

 
 
 
 
 


 