Bajo Bandera
{{Infobox film
| name           = Bajo Bandera (flag down)
| image          = BajoBandera.jpg
| caption        = 
| director       = Juan José Jusid
| producer       = Jaime Nuguer Pablo Rovito
| writer         = Juan José Jusid Guillermo Saccomanno
| starring       = Miguel Ángel Solá Federico Luppi
| music          = Federico Jusid
| cinematography = Paolo Carnera
| editing        = Jorge Valencia
| distributor    = 
| released       = August 21, 1997
| runtime        = 109 minutes
| country        = Argentina Spanish
| budget         = 
| followed_by    = 
}} 1997 Argentina|Argentine mystery drama film directed and written by Juan José Jusid with Guillermo Saccomanno. The film starred Miguel Ángel Solá and Federico Luppi.

The film won 7 awards and 2 nominations, including 3 Silver Condor awards at the Argentine Film Critics Association Awards
for Best Music, Best New Actor Nicolás Scarpino and Best Screenplay (Guillermo Saccomanno and Juan José Jusid). Miguel Ángel Solá was also nominated for Best Actor.

==Cast==
*Miguel Ángel Solá...  Mayor Molina
*Federico Luppi ...  Coronel Hellman
*Omero Antonutti ...  Padre Bruno
*Daniele Liotti ...  Soldado Repetto
*Andrea Tenuta ...  Fanny
*Andrea Pietra ...  Nora
*Carlos Santamaría ...  Capitán Roca
*Alessandra Acciai ...  Victoria
*Mónica Galán ...  Paula
*Betiana Blum ...  Bonavena
*Ariel Casas ...  Subteniente Trevi
*Juan Andrés Bracelli ...  Lito
*Nicolás Scarpino ...  Rosen
*Rolly Serrano ...  Cabo Benamino
*Walter Balzarini ...  Joaquín
*Alan McCormick ...  Polaco
*Damián Canduci ...  Perro

==External links==
*  

 
 
 
 
 


 
 