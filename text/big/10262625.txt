Once Before I Die
{{Infobox film
| name           = Once Before I Die
| image          = Oncebefore_mop.jpg
| image_size     =
| caption        =
| director       = John Derek
| producer       = John Derek
| writer         = Vance Skarstedt
| narrator       =
| starring       = Ursula Andress John Derek Richard Jaeckel
| music          =
| cinematography = Arthur E. Arling
| editing        =
| distributor    = Warner Bros./Seven Arts Pictures
| released       =  
| runtime        = 97 min
| country        = United States
| language       =
| budget         =
}} 1966 war drama starring Ursula Andress and her husband John Derek, who also directed.  The film was based on a 1945 novel Quit for the Next by Lieutenant Anthony March. 

In this film, a band of American soldiers and one woman get trapped behind enemy lines in the Philippines after the attack on Pearl Harbor. In harrowing circumstances, they struggle for courage and fight to stay alive.

==Summary==

  Japanese attack the Philippine islands. A group of polo-playing soldiers of the 26th Cavalry Regiment (United States) and their families are surprised far off in the countryside. Major Bailey (John Derek) leads them back to Manila, but the roads are jammed with fugitives.

On the trip, Baileys fiancée Alex (Ursula Andress) talks with a young shy soldier (Rod Lauren) who tells her of his fears to die and admits that he has never been with a woman. Only once before he dies, he would like to have sex.

==Production==

John Derek, a photographer and a former actor made his directing debut in this film.  Derek was married to Ursula Andress 1957–1966, who was then at the height of her popularity after films such as Dr. No (film)|Dr.No, Four for Texas, and Fun in Acapulco.

The film was titled The 26th Cavalry in the Philippines.  Jock Mahoney makes an uncredited appearance in the film.

During the filming Rod Lauren met Filipino actress Nida Blanca, whom he later married.

==Cast==
*Ursula Andress as  Alex
*John Derek as  Bailey
*Richard Jaeckel as  Lt. Custer
*Ron Ely as  Captain
*Rod Lauren as Soldier
*Vance Skarstedt
*Allen Pinson
*Gregg Martin
*Renato Robles
*Fred Galang
*Andres Centenera
*Rod Francisco
*Nello Nayo
*Mario Taquibulos
*Eva Vivar

==See also==
* List of American films of 1966

== References ==

 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 