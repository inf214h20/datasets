Raiders of the Living Dead
{{Infobox film
| name           = Raiders of the Living Dead
| image_size     =
| image	         = Raiders of the Living Dead FilmPoster.jpeg
| caption        =
| director       = Samuel M. Sherman Brett Piper (uncredited)
| producer       = Samuel M. Sherman Dan Q. Kennis
| writer         = Samuel M. Sherman Brett Piper
| starring       = Scott Schwartz Robert Allen Donna Asali Corri Burt Robert Deveau 
| music          = Tim Ferrante George Edward Ott (Opening Theme) 
| editing        = John Donaldson	 	
| distributor    = Independent International Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1986 motion horror film directed by Samuel M. Sherman from a script he co-wrote with Brett Piper.

==Plot== The Mummy, The Sin of Nora Moran). On the prison island, the zombies attack a security guard and tear him apart as the reporter and teenager venture into the prison caverns for a final showdown.

==Production==
This film was originally started in New Hampshire in 1983 by co-writer Brett Piper under the title Graveyard. It was later completed in three weeks by writer-producer Samuel Sherman. Veteran actress Zita Johann came out of retirement to appear in one scene as an elderly historian. 

==Cast==
*Robert Allen as Dr. Corstairs
*Donna Asali as Shelly Godwin 
*Corri Burt as Michelle   
*Robert Deveau as Morgan Randall 
*Zita Johann as Librarian 
*Bob Sacchetti as Man in Black 
*Scott Schwartz as Jonathan

==Release==
The film was given a limited release theatrically in the United States by Independent International Pictures in 1986. 

The film was given a 2-disc special edition release in the United States by Image Entertainment in 2003.  This version is currently out of print.   This set contains three version of the film:

:Version 1: the first cut called Dying Day, directed by Brett Piper
:Version 2: the second cut called Dark Night, directed by Brett Piper
:Version 3: the third cut called Raiders of the Living Dead, featuring new footage shot by Sherman

The film was released on DVD in the United Kingdom in 2003.  This version features solely Raiders of the Living Dead and its commentary. 

==References==
 

==External links==
* 

 
 
 
 
 

 