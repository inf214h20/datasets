The Devil Came on Horseback
{{Infobox film
| name           = The Devil Came on Horseback
| image          = Devil came on horseback.jpg
| caption        = Theatrical release poster
| director       = Ricki Stern Anne Sundberg Jane Wells, Gretchen Wallace
| writer         = Ricki Stern Anne Sundberg
| starring       = Brian Steidle
| music          = Paul Brill
| cinematography = Phil Cox Tim Hetherington William Rexer Jerry Risius Anne Sundberg John Keith Wasson	
| editing        = Joey Grossfield
| distributor    = Break Thru Films
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Captain Brian Steidle and his experiences while working for the African Union. The film asks viewers to become educated about the on-going genocide in Darfur and laments the failure of the US and others to end the crisis.  

==Details==
It is a Break Thru Films production in association with Global Grassroots and 3 Generations.

It premiered at Sundance Film Festival in 2007, was screened at the Laemmle Music Hall on Wilshire Blvd. in Los Angeles in June 2007 and opened its nationwide release at the IFC New York in July 2007. It was released on DVD on 30 October 2007.

==Awards==
*Working Films Award, Full Frame Documentary Film Festival, 2007. 
*Seeds of War Award, Full Frame Documentary Film Festival.  Silverdocs Film Festival, 2007. 
*Lena Sharpe / Women in Cinema Persistence of Cinema Award, Seattle International Film Festival. 
*Adrienne Shelly, Excellence in Filmmaking Award, Nantucket Film Festival. 
*Index on Censorship Film Award, Freedom of Expression Award 2009, Index on Censorship. 

==Book==
The book version is by Brian Steidle with his sister, Gretchen Steidle Wallace. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 