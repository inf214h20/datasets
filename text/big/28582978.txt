La Vie sexuelle des Belges 1950-1978
{{Infobox Film | name = The Sexual Life of the Belgians (original title: La Vie sexuelle des Belges 1950-78) 
  | image = Café Dolle Mol.jpg
  | caption = The Brussels café Dolle Mol where several of the films scenes were filmed. 
  | director = Jan Bucquoy 
  | producer = Francis De Smet, Transatlantic Films Bruxelles
  | writer = Jean-Phlippe Vidon
  | costumes = Mariska Clerebaut, Sabina Kumeling
  | starring = Jan Bucquoy, Noël Godin
  | music = Francis De Smet, Marc Aryan, Gene Vincent, Will Tura, Peter Benoit, Les Dominos.
  | cinematography = Michel Baudour
  | editing = 
  | distributor = 
  | released =  
  | country = Belgium
  | runtime = 89 minutes
  | language = French
  | budget = 
  }}
La Vie sexuelle des Belges 1950-78 is a 1994 film satire on Belgian provincialism that proved a major cinematic success in Belgium. It was the first film by now-famed Flemish provocateur and director Jan Bucquoy. It tells an autobiographical tale of a clueless young bumpkin, Jan, trying far from successfully to keep up with times failing equally at being a 60s free-love youth or political activist and finally sinking into a mundane bourgeoise life.

== Reception ==
The film received the André Cavens Award|André Cavens Award for Best Film by the Belgian Film Critics Association (UCC).

* "The cinematography is an unusual blend of the surreal and the mundane, infused with a quirky comic style which flitters between self-mockery and farce.  Bucquoys portrait of his own mother provides the film with its most enduring image, the possessive house-proud woman who casually quips when she notices her husband has died, "it isnt time", and repeatedly states when she finds a way to save money: "its cheaper that way". If the film is an accurate reflection of the truth, Bucquoy must have had one Hell of an upbringing...". )

== References ==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 


 