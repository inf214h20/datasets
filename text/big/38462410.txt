Cool Jazz and Coconuts
{{Infobox film
| name           = Cool Jazz and Coconuts
| image          = 
| caption        = 
| director       = Jakob Frímann Magnússon
| producer       = 
| writer         = Jakob Frímann Magnússon
| starring       = Ragnhildur Gísladóttir
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 
}}

Cool Jazz and Coconuts ( ) is a 1985 Icelandic drama film directed by Jakob Frímann Magnússon. It was entered into the 14th Moscow International Film Festival.   

==Cast==
* Ragnhildur Gísladóttir as Svala
* Egill Ólafsson as Oddur
* Tinna Gunnlaugsdóttir as Helga
* Þórhallur Sigurðsson as Karl
* Rúrik Haraldsson as Björn - sýslumaður
* Tyrone Nicholas Troupe III as General Reeves
* Jón Tryggvason as Dr. W. Vísindamaður
* Júlíus Agnarsson as Gunnar - Þórðarson
* Flosi Ólafsson as Bjarki - Tryggvason
* Herdís Þorvaldsdóttir as Skólastýra
* Sigurveig Jónsdóttir as Lovísa Símamær

==References==
 

==External links==
*  

 
 
 
 
 
 