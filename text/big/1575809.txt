I'm Gonna Git You Sucka
{{Infobox film
| name           = Im Gonna Git You Sucka
| image          = Im Gonna Git You Sucka film.jpg
| caption        = Theatrical release poster
| director       = Keenen Ivory Wayans
| producer       = Eric L. Gold Raymond Katz
| writer         = Keenen Ivory Wayans Steve James Antonio Fargas Jim Brown John Vernon Janet Dubois
| music          = David Michael Frank Tom Richmond
| editing        = Michael R. Miller
| studio         = United Artists
| distributor    = Metro-Goldwyn-Mayer|MGM/UA Communications Company
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $3 million
| gross          = $13,030,057
}}
 John Witherspoon, Damon Wayans, Clarence Williams III, and Chris Rock. 

The films main villain, "Mr. Big", was played by  the actor John Vernon. In the movie, Vernon states about on his role as "Mr. Big" that while he might seem to be "above playing an exploitation villain", many others (including Angie Dickinson, Jamie Lee Curtis, and Shelley Winters) have taken on similar roles.

==Plot== overdosed on gold chains. A brief image of Junebug is shown featuring him covered, head-to-toe, with gold chains. When not distracted by his old girlfriend and Junebugs widow Cheryl (Dawnn Lewis), Jack seeks revenge for his brothers death. 
 crime lord, Steve James). Together with his army, Jack sets out to eliminate the gold chain trade from the streets of his neighborhood and take down "Mr. Big".

==Cast==
*Keenen Ivory Wayans as Jack Spade
*Bernie Casey as John Slade
*Janet Dubois as Belle
*Isaac Hayes as Hammer 
*Jim Brown as Slammer 
*Antonio Fargas as Flyguy  Steve James as Kung Fu Joe
*John Vernon as "Mr. Big"
*Dawnn Lewis as Cheryl
*Kadeem Hardison as Willie
*Damon Wayans as Leonard
*Kim Wayans as Nightclub Singer
*Chris Rock as Rib Joint Customer
*Anne-Marie Johnson as Cherry
*Eve Plumb as Kalingas Wife Tony Cox as Wayne Evans
*Clarence Williams III as Kalinga
*David Alan Grier as Newsman
*Robin Harris as Bartender
*Marlon Wayans as Pedestrian
*Shawn Wayans as Pedestrian
*Gary Owens as Pimp of the Year pageant announcer Robert Townsend (uncredited)
*Peggy Lipton (uncredited)

==Reception==

The movie received mixed reviews to generally positive reviews, earning a "fresh" 62% on Rotten Tomatoes.   

==Television pilot== television pilot program called Hammer, Slammer, & Slade was shown on American Broadcasting Company|ABC-TV.  
 Steve James. Although Keenen Ivory Wayans wrote the pilot, and he served as the executive producer (television)|producer, he did not appear in this pilot. Instead, the character of Jack Spade was portrayed by Eriq La Salle, who later acted in the TV series ER (TV series)|ER. Also acting in the pilot was the little-known (at the time) Martin Lawrence in Kadeem Hardisons former role as Willie "Ya-te-dee". (Mr. Bigs nephew and one of his henchmen.)

Hammer, Slammer, & Slade was not sold to any TV network, but it was shown several times in television syndication|syndication.

==Release on DVD & HD==
* In 2001, Im Gonna Git You Sucka was published on DVD.
* In 2010, Im Gonna Git You Sucka was digitized in High Definition (1080i) and also broadcast on MGM HD.

==See also==
* Hollywood Shuffle (1987) Black Dynamite (2009)

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 