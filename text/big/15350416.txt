Mrs Ratcliffe's Revolution
{{Infobox Film
| name           = Mrs Ratcliffes Revolution
| image          = Mrs R Poster.jpg
| caption        = Poster of Mrs Ratcliffes Revolution
| director       = Bille Eltringham
| writer         = Bridget OConnor  Peter Straughan
| starring       = Catherine Tate   Iain Glen   Brittany Ashworth
| producer       = Hugo Heppell
| music          = Robert Lane
| cinematography =
| editing        =
| distributor    = Warner Bros.
| released       =  
| runtime        = 102&nbsp;minutes
| language       = English
| budget         =
| gross          =
}} 2007 British British family who move to East Germany in 1968, during the Cold War. It was filmed in Hungary and the United Kingdom (UK),  and was released on 9 July 2007 at the Cambridge Film Festival, and nationwide in the UK on 28 September.

==Cast==
* Catherine Tate - Dorothy Ratcliffe
* Iain Glen - Frank Ratcliffe
* Brittany Ashworth - Alex Ratcliffe
* Heike Makatsch - Frau Unger
* Jessica Barden - Mary Ratcliffe
* Christian Brassington - Thomas
* Nigel Betts - Uncle Philip
* Robert Daniel Lowe - Otto
* Ottilia Borbáth - Frau Glock
* Béla Fesztbaum - 1st Stasi Officer
* Fanni Futár - Uti
* Imola Gáspár - Art Teacher
* Ákos Horváth - 2nd Border Guard
* Barna Illyés - Gym Teacher
* John Kirk - Mr. Murray
* Karl Kranzkowski - Rector
* Uwe Lauer - Truck Driver
* Piroska Móga - Ursula
* Ben OBrien - 2nd Stasi Officer
* Gábor Pintér - 1st Border Guard
* Alexander Scheer - Willi
* Arndt Schwering-Sohnrey - Jerzy
* Katharina Thalbach - Anna
* Susan Tordoff - Marys Teacher
* Stephan Wolf-Schönburg - Herr Vort

==Reception==
Mrs Ratcliffes Revolution received high 80% Fresh on Rotten Tomatoes. Because of its limited release, there were few reviews, but Channel 4 Online and The Guardian Films cited the film as a vehicle for the blooming film careers of both Catherine Tate and Brittany Ashworth.   The film won the Audience Award at the Wurzburg International Film Weekend.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 