Cape Fear (1962 film)
 
{{Infobox film
| name = Cape Fear
| image = Cape fear1960s.jpg
| caption = Cape Fear movie poster
| director = J. Lee Thompson
| producer = Sy Bartlett James R. Webb
| based on =  
| starring =Robert Mitchum Gregory Peck Martin Balsam Polly Bergen Lori Martin
| music =Bernard Herrmann
| cinematography = Sam Leavitt
| editing = George Tomasini
| distributor = Universal Pictures
| released =  
| runtime = 105 minutes
| country = United States
| language = English
| budget =
}} James R. The Executioners by John D. MacDonald. It was directed by J. Lee Thompson and released on April 12, 1962. The movie concerns an attorney whose family is stalked by a criminal he helped to send to jail.
 remade in 1991 by Martin Scorsese. Peck, Mitchum and Balsam all appeared in the remake.

==Plot== Georgia lawyer whom he holds personally responsible for his conviction because Sam interrupted his attack and testified against him. Cady begins to stalk and subtly threaten Bowdens family. He kills the Bowden family dog, though Sam cannot prove Cady did it. A friend of Bowdens, police chief Mark Dutton (Martin Balsam), attempts to intervene on Bowdens behalf; but he cannot prove Cady guilty of any crime.
 promiscuous young woman named Diane Taylor (Barrie Chase) when she brings him home, but neither the private eye nor Bowden can persuade her to testify. Bowden hires three thugs to beat up Cady and persuade him to leave town, but the plan backfires when Cady gets the better of all three. Cadys lawyer vows to have Bowden disbarred.
 Cape Fear. In an attempt to trick Cady, Bowden makes it seem as though he has gone to a completely different location. He fully expects Cady to follow his wife and daughter, and he plans on killing Cady to end the battle. He and a local deputy hide nearby, but Cady realizes the deputy is there and kills him. Eluding Bowden, Cady first attacks Mrs. Bowden on the boat, causing Bowden to go to her rescue. Meanwhile, Cady swims back to shore to attack the daughter. Bowden realizes what has happened and also swims ashore.

The two men engage in a final violent fight on the riverbank. Bowden overpowers Cady but decides not to kill him, preferring to let him spend the rest of his life in jail. The film ends with the Bowden family sitting together on a boat the next morning.

==Cast==
* Gregory Peck as Sam Bowden
* Robert Mitchum as Max Cady
* Polly Bergen as Peggy Bowden
* Lori Martin as Nancy Bowden
* Martin Balsam as Mark Dutton
* Jack Kruschen as Dave Grafton
* Telly Savalas as Charlie Sievers
* Barrie Chase as Diane Taylor
* Paul Comi as George Garner
* Edward Platt as Judge

==Production==

===Casting===
Almost everyone in Hollywood wanted a role in the film. Rod Steiger wanted to play Max Cady, but he backed off when he heard Mitchum was considering the role. Telly Savalas was screentested for the role but later played private eye Charlie Sievers. 
     
Charles Bronson, James Coburn, Charlton Heston, Jack Palance and John Wayne, were all considered for the role of the attorney, Sam Bowden. Peck was a last-minute replacement for Heston, who was originally cast.  
 
Jim Backus was set to play attorney Dave Grafton, but had to drop out due to conflicts with his new show Gilligans Island. 

===Filming===
 
Thompson had always envisioned the film in black and white prior to production. As an Alfred Hitchcock fan, he wanted to have Hitchcockian elements in the film, such as unusual lighting angles, an eerie musical score, closeups and subtle hints rather than graphic depictions of the violence Cady has in mind for the family.

The outdoor scenes were filmed on location in Savannah, Georgia, Stockton, California and the Universal Studios backlot at Universal City, California. The indoor scenes were done at Universal Studios Soundstage. Mitchum had a real-life aversion to Savannah, where as a teenager, he had been charged with vagrancy and put on a chain gang. This resulted in a number of the outdoor scenes being shot at Ladds Marina in Stockton, including the culminating conflict on the houseboat at the end of the movie.

This scene where Mitchum attacks Polly Bergens character on the houseboat was almost completely improvised. Before the scene was filmed, Thompson suddenly told a crew member: "Bring me a dish of eggs!" Mitchums rubbing the eggs on Bergen was not scripted and Bergens reactions were real. She also suffered back injuries from being knocked around so much. She felt the impact of the "attack" for days.   While filming the scene, Mitchum cut open his hand, leading Bergen to recall: "his hand was covered in blood, my back was covered in blood. We just kept going, caught up in the scene. They came over and physically stopped us."   
 The Executioners, by John D. MacDonald, Cady was a soldier court-martialed and convicted on then Lieutenant Bowdens testimony for the brutal rape of a 14-year-old girl. The censors stepped in, banned the use of the word "rape", and stated that depicting Cady as a soldier reflected adversely on U.S. military personnel. 

===Music=== French Horns. No use is made of further wind instruments or percussion. 

==Distribution==
Although the word "rape" was entirely removed from the script before shooting, the film still enraged the censors, who worried that "there was a continuous threat of sexual assault on a child." In order to accept the film, British censors required extensive editing and deleting of specific scenes. After making around 6 minutes of cuts, the film still nearly garnered a British X rating (meaning at the time, "Suitable for those aged 16 and older", not necessarily meaning there was pornographic content). 

===Home media===
Cape Fear was first made available on VHS on March 1, 1992. It was later re-released on VHS as well as DVD on September 18, 2001. Currently, the film is set to be released onto Blu-ray on January 8, 2013. It will contain production photos as well as a "making-of" featurette. 

==Reaction==

===Critical response===
Upon its release, the film received positive but cautious feedback from critics due to the films content. Review aggregator Rotten Tomatoes reports that 95% of 20 critics have given the film a positive review, with a rating average of 7.6 out of 10. 
 trade magazine Variety (magazine)|Variety reviewed the film as "competent and visually polished", while commenting on Mitchums performance as a "menacing omnipresence." 

===Legacy===

Although it makes no acknowledgement of Cape Fear, the episode "The Force of Evil" from the 1977 NBC television series Quinn Martins Tales of the Unexpected uses virtually the same plot, merely introducing an additional supernatural element to the released prisoner.      

The film serves as the basis for the 1993 episode of The Simpsons "Cape Feare" in which Sideshow Bob, recently released from prison, stalks the Simpson family in an attempt to kill Bart.
 top 50 100 Scariest Movie Moments in 2004. 

A consumer poll on the Internet Movie Database rates Cape Fear as the 65th best trial film, although the trial scenes are merely incidental to the plot. 

== See also ==

* List of films featuring home invasions
* List of films featuring surveillance
* Trial movies

==Notes==
 

==Further reading==
* Bergman, Paul; Asimow, Michael. (2006) Reel justice: the courtroom goes to the movies (Kansas City: Andrews and McMeel). ISBN 0-7407-5460-2; ISBN 978-0-7407-5460-9; ISBN 0-8362-1035-2; ISBN 978-0-8362-1035-4.
*  . Thain, Gerald J., "Cape Fear, Two Versions and Two Visions Separated by Thirty Years." ISBN 0-631-22816-0, ISBN 978-0-631-22816-5. 176 pages.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 