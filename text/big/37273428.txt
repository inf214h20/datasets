Quiet Killer
{{Infobox film
| name           = Quiet Killer
| image          = 
| image_size     =
| caption        =
| director       = Sheldon Larry
| producer       = Paul Saltzman
| writer         = Gwyneth Cravens (book) John S. Marr (book) I. C. Rapoport
| narrator       = 
| starring       = Kate Jackson
| music          = Marty Simon 
| cinematography = Ron Orieux
| editing        = David Rosenbloom
| studio         =
| distributor    = CBS
| released       = March 24, 1992
| runtime        = 90 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1992 television medical disaster film directed by Sheldon Larry. The thriller, based on the 1977 novel The Black Death by Gwyneth Cravens and John S. Marr  and adapted by I. C. Rapoport, stars Kate Jackson and was originally broadcast on CBS. The film was released on VHS under the title Black Death.

==Plot==
When Sara Dobbs (Robertson), the teenage daughter of a wealthy New York City family returns home while feeling sick, nobody suspects a thing. At home, her health deteriorates quickly, resulting in a painful death on the streets before her Manhattan home. In the hospital, it does not take long before Dr. Nora Hart (Jackson) concludes that Sara has died of the Black Death, which has not occurred in centuries. Realizing that the disease is extremely contagious, she tries to push the authorities to warn the New York citizens, but the Mayor is reluctant to cause a widespread panic. Meanwhile, more citizens who have been in direct contact with Sara start to perish. Nora and her new colleague Dr. Jake Prescott (Nordling) - whom she becomes romantically involved with - start a race against the clock to locate and treat all the people who might be infected, while trying to prevent the city from panicking. In the end, Nora is successful in finding everyone who is infected, and treats most of them successfully. Within a week, a pandemic is ended after 22 deaths.

==Cast==
*Kate Jackson as Dr. Nora Hart
*Al Waxman as Mayor Andy Carmichael
*Jeffrey Nordling as Dr. Jake Prescott
*Chip Zien as Dr. Lionel Katz Barbara Williams as Charlene
*David Hewlett as Nyles Chapman
*Jerry Orbach as Dr. Vincent Califano
*Howard Hesseman as Congressman Calvin Phillips
*Alma Martinez as Dolores Rosales
*Tom Mardirosian as Deputy Mayor Kaprow
*Kathleen Robertson as Sara Dobbs
*Luis Guzmán as Adelaido Ortiz
*Don Francks as Dr. Martin

==References==
 

==External links==
* 

 
 
 
 
 
 
 