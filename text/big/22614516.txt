Madrasta
 
{{Infobox Film | name = Madrasta
  | image           =
  | director = Olivia Lamasan
  | writer = Olivia Lamasan Raymond Lee
  | producer = Charo Santos-Concio Malou N. Santos Vic del Rosario
  | starring = Sharon Cuneta Christopher de Leon Zsa Zsa Padilla  
  | distributor = Star Cinema 
  | released =   
  | country = Philippines
  | runtime = 106 minutes Tagalog English English Filipino Filipino
}}
 Filipino movie directed by Olivia Lamasan starring Sharon Cuneta and Christopher De Leon. The family drama clinched  the Megastar a Grandslam Best Actress honor, garnering best actress recognition from all major award-giving bodies in the Philippines, while its strong showing at the box-office conferred to Sharon Cuneta|Sharon, another Box-Office Queen award. The Star Cinema produced drama was Sharons first movie outside her home studio of more than twenty years, Viva Films. Madrasta had its Asian TV screening via the movie channel Cinemax.

The film is set to be restored by ABS-CBN Film Archive.

==Synopsis==
The loving and nurturing Mariel (Sharon Cuneta) marries Edward (Christopher De Leon) after his first wife abandons him and took responsibility as a mother to his  three children (Claudine Barretto, Patrick Garcia and Camille Prats). Mariel strives to win the acceptance and affection of her new stepchildren, even as she tries to define her role in the family she is still just a stepmother no matter what and she must learn how to cope and be a part of their lives. Directed by Olivia M. Lamasan, this provocative film explores issues of love, trust and what it means to be a family.

==Cast==

* Sharon Cuneta- Mariel
* Christopher De Leon- Edward
* Zsa Zsa Padilla- Sandra
* Nida Blanca- Fides
* Tita Muñoz- Ninay
* Eula Valdez- Irene
* Claudine Barretto- Rachel
* Patrick Garcia- Ryan
* Camille Prats- Liza
* Rico Yan- Dodie
* Teresa Loyzaga- Luchie
* Cris Villanueva- Dan
* Koko Trinidad- Lolo
* Vangie Labalan- Manang
* Cheng Avellana - Lenlen

==Awards==
===FAMAS (1997)===
* Best Actress
Sharon Cuneta

* Best Child Actor
Patrick Garcia

* Best Actor
Christopher De Leon (Nominated)

* Best Director
Olivia M. Lamasan (Nominated)

* Best Editing
Edgardo Vinarao (Nominated)
Rudy Hukom (Nominated)

* Best Movie Theme Song
Willy Cruz (Nominated)
For the song "Hanggang Kailan Kita Mamahalin?".

* Best Picture (Nominated)

* Best Screenplay
Ricardo Lee (Nominated)
Olivia M. Lamasan (Nominated)

* Best Supporting Actress
Zsa Zsa Padilla  (Nominated)

* Best Supporting Actor
Koko Trinidad  (Nominated)

===FAP Awards (1997)===

* Best Actress
Sharon Cuneta

* Best Actor
Christopher De Leon (Nominated)

* Best Director
Olivia M. Lamasan  (Nominated)

* Best Editing
Edgardo Vinarao  (Nominated)
Rudy Hukom  (Nominated)

* Best Musical Score
unknown  (Nominated)

* Best Original Song
Willy Cruz  (Nominated)
For the song "Madrasta".

* Best Picture

* Best Screenplay
Ricardo Lee  (Nominated)
Olivia M. Lamasan (Nominated)

* Best Supporting Actor
Patrick Garcia  (Nominated)

* Best Supporting Actress
Zsa Zsa Padilla  (Nominated)

===Gawad Urian (1997)===

* Best Actress 
Sharon Cuneta
Tied with Nora Aunor for Bakit may kahapon pa? (1996).

* Best Actor 
Christopher De Leon  (Nominated)

* Best Direction 
Olivia M. Lamasan  (Nominated)

* Best Editing 
Edgardo Vinarao (Nominated)
John David Hukom (Nominated)

* Best Picture 

* Best Screenplay 
Ricardo Lee  (Nominated)
Olivia M. Lamasan (Nominated)

===Star Awards for Movies (1997)===

* Actor of the Year
Christopher De Leon

*Actress of the Year
Sharon Cuneta

* Director of the Year
Olivia M. Lamasan  (Nominated)

* Movie Theme Song of the Year
Willy Cruz  (Nominated)
For the song "Hanggang Kailan Kita Mamahalin".

* Movie of the Year (Nominated)

* Musical Scorer of the Year
unknown  (Nominated)

* Supporting Actor of the Year
Patrick Garcia  (Nominated)

* Supporting Actress of the Year
Zsa Zsa Padilla  (Nominated)

 
 
 
 
 
 