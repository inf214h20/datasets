Aces Go Places
 
 
 
{{Infobox film
| name           = Aces Go Places
| image          = Aces Go Places.jpg
| caption        =
| director       = Eric Tsang
| producer       = Karl Maka Dean Shek Raymond Wong
| narrator       =
| starring       = Samuel Hui Karl Maka
| music          = Samuel Hui Teddy Robin Kwan Henry Chan
| editing        = Tony Chow
| distributor    = Cinema City & Films Co.
| released       = Hong Kong:   Germany:  
| runtime        = 93 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK $26,043,773
}}

Aces Go Places, ( ), also known in the United States as Diamondfinger or Mad Mission 1, is a 1982 Hong Kong action/comedy film directed by Eric Tsang, and starring Sam Hui and Karl Maka.

==Plot==
A suave, smooth burglar named King Kong tries to make up for his thieving ways by teaming up with an Albert Baldy Au, a bumbling Taishanese police detective from the United States. Both work together to try to find a set of stolen diamonds; the diamonds are also being tracked by a European criminal known as White Gloves. The two heroes are supervised by Superintendent Nancy Ho, who has a temper.

==Cast==
* Sylvia Chang as Supt. Nancy Ho
* Lindzay Chan as Ballerina
* Chan Sing as Mad Max
* Cho Tat-wah as Hua
* Carroll Gordon as Marge
* Samuel Hui as King Kong
* George Lam as Ambulance driver
* Karl Maka as Albert Au
* Dean Shek as Gigolo Joe
* Tsui Hark as Cameo appearance Raymond Wong as Priest
* Chi Yi-Hsiung
* Piao Chin
* Fung Yun-chuen

 
 

==See also==
* Aces Go Places (film series)

==References==
 

==External links==
*   from HKCuk.co.uk
*  
*  

 
 
 
 
 
 
 


 
 