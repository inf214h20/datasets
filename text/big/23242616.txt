Second to Die
{{Infobox Film
| name           = Second to Die
| image          = second-to-die.jpg
| caption        = 
| director       = Brad Marlowe
| producer       = Paula Silver Lara Levicki-Lavi George Morgan Anita Doohan Adrienne Armstrong
| starring       = Erika Eleniak Jerry Kroll Colleen Camp Kimberly Rowe John Wesley Shipp Paul Winfield
| music          = Andrew Dorfman
| cinematography = Moshe Levin
| editing        = Gena Bleier Pam Wise
| distributor    = 
| released       = April 23, 2002
| runtime        = 89 minutes
| country        = United States English
}}
 thriller film released in 2002. The film stars Erika Eleniak, Jerry Kroll, and Colleen Camp. Its tagline was "One murder is never enough".

==Synopsis==
Sara Morgan (Eleniak) is a woman who thinks the murder of her husband (John Wesley Shipp) will solve all her problems. However, she realizes that it wont be enough.

Her sister, Amber, finds her diary after Sara is found dead. Through the pages, flashbacks are seen to Saras marriage, the death of her husband, her second marriage, and twist ending that exposes the truth behind all the relationships.

Her husband has a "second to die" life insurance policy, where the beneficiary receives the money after both people die—in this case, both Sara and her husband. 

After the plane explosion, Sara marries her husbands friend, "Scooch". What she doesnt know is that hes involved with her neighbor, Cynthia, who is seen putting on a wig. Her wig becomes crucial later on.

"Scooch" arranges Saras death in search of the three million dollar payout. The showdown between husband and wife ends with Sara shot dead and floating in the pool.

==Filming==
The film was shot in Los Angeles, California.

==Cast==
* Erika Eleniak as Sara Morgan Bratchett Scucello
* Jerry Kroll as Raymond "Scooch" Scucello
* Colleen Camp as Cynthia
* Kimberly Rowe as Amber
* John Wesley Shipp as Jim Bratchett
* Paul Winfield as Detective Grady

==Release==
The film was first released on video in Spain. Later, on April 23, 2002, the film was released in United States.

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 