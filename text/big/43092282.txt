Blood of Redemption
{{Infobox film
| name           = Blood of Redemption
| image          = Blood of Redemption DVD cover.jpg
| caption        = Movie Poster
| director       = {{plainlist|
* Giorgio Serafini
* Shawn Sourgose}}
| producer       = {{plainlist|
* Brittany Bowden
* Gianni Capaldi
* Andre Relis}}
| writer         = {{plainlist|
* Rey Reyes
* Giorgio Serafini
* Shawn Sourgose}}
| starring       = {{plainlist|
* Dolph Lundgren
* Billy Zane
* Gianni Capaldi
* Vinnie Jones
* Robert Davi}}
| music          = Riccardo Eberspacher
| cinematography = Marco Cappetta
| editing        = Danny Saphire
| studio         =  {{plainlist|
* High Five Films 
* VMI Worldwide
}}
| distributor    = Entertainment One Films
| released       =   }}
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $2 million 
}}
Blood of Redemption is a 2013 American crime film directed by Giorgio Serafini and Shawn Sourgose.    The film was released on direct-to-video|direct-to-DVD and Blu-ray in the United States and Canada on September 24, 2013.  The film stars Dolph Lundgren, Billy Zane, Gianni Capaldi, Vinnie Jones and Robert Davi.   

== Plot ==
Quinn (Billy Zane) is the son of a famous and powerful mafioso. In the course of one night, he loses everything. Betrayed by someone from his inner circle, Quinn is set up and arrested. His father (the patriarch of the criminal empire) is killed, and his brother suspects he is behind it all. When hes released from jail, he tries to escape the demons from his past, but that becomes an impossible task. Campbell (Vinnie Jones) is the ruthless new leader of "The Company", and he wont let Quinn live in peace. So instead of escaping them, Quinn begins to fight back. He joins forces with his former henchman and friend, The Swede (Dolph Lundgren), and also fights his enemies head on.

== Cast ==
 
* Dolph Lundgren as Axel (also known as The Swede)
* Billy Zane as Quinn Forte
* Gianni Capaldi as Kurt
* Vinnie Jones as Campbell
* Robert Davi as Hayden
* Massi Furlan as Boris
* LaDon Drummond as Agent West
* Stephanie Rae Anderson as Escort #1
* Manny Ayala as Bum Hitman
* Al Burke as Officer Bauer
* Zoli Dora as Campbell Henchman #2
* Mario E. Garcia as Officer Paul Crain
* Jelly Howie as Loryn
* Clint Glenn Hummel as Private Evans
* Elisabeth Hunter as Officer Smith
* Scott Ly as Lin Chau
* Gus Lynch as Officer Brown
* Marcus Natividad as Asian Assassin
* Brad Nelson as Junkyard
* Tasha Reign as Senators Escort #1
* Raven Rockette as Lin Chaus Escort
* Gilbert Rosales as Man Outside Bar
* Chuck Saale as LAPD Captain Bruce
* Jenny Shakeshaft as Call Girl
* Rikki Six as Senators Escort #2 James Storm as Senator Roswald
* Reena Tolentino as Gorgeous Woman
* Scott Vancea as Quinn
* Franco Vega as Officer Hunter
 

== Critical reception ==
Nav Qateel of Influx Magazine gave the film a C+ rating and called it a "guilty pleasure".   Ian Jane of DVD Talk rated it 2/5 stars and wrote that the film fails to live up to its potential as a fun B film.   David Johnson of DVD Verdict wrote, "While Blood of Redemption isnt embarrassingly terrible, it leaves much to be desired as a crime thriller, burdened by clunky storytelling, indifferent acting, and unsatisfactory plot twists. As an action film, its even more wanting." 

== Production == Los Angeles, California in 21 days from February 8 until March 1, 2013.

== Release == Region 1.

== See also == Dolph Lundgren filmography

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 