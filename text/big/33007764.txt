Jeeva (1986 film)
 
{{Infobox film
| name           = Jeeva
| image          = Jeeva 1986.jpg
| caption        = Poster
| director       = Raj N. Sippy
| producer       = Romu N. Sippy
| story          = P. D. Mehra   Vinay Shukla (dialogues)
| screenplay     =   Mandakini
| music          =  R D Burman
| cinematography =  Anwar Siraj
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        =  
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} action Thriller thriller film starring Sanjay Dutt and Mandakini (actress)|Mandakini. The film is directed by Raj N Sippy and produced by his brother Romu N Sippy under the production house Rupam Pictures.

==Plot==
The plot revolves around Jeevan Thakurs (Sanjay Dutt) fight against the local cunning money lender Lala (Pran (actor)|Pran) & his ally Inspector Dushant Singh (Anupam Kher) with revenge for killing his parents. Orphaned in his young age, Jeevan is raised in a gang of dacoits where he takes the name Jeeva. After the death of the leader of the gang, Jeeva gets in bitter terms with another contender for leadership, Lakhan (Shakti Kapoor).
  The action thriller movie gets its romantic touch when Jeeva fall in love with Nalini (Mandakini (actress)|Mandakini).

==Cast==
*Sanjay Dutt  as   Jeeva / Jeevan Thakur Mandakini  as  Nalini
*Amjad Khan  as   Sardar
*Shakti Kapoor  as   Lakhan Sachin  as   Gopal Singh
*Gulshan Grover
*Anupam Kher  as   Inspector Dushant Singh Pran  as   Lala
*Shreeram Lagoo  as  Dr. Shreeram Lagoo
*Beena Banerjee  as  Beena
*Satyendra Kapoor  as   Judge
*Vidya Sinha

==Soundtrack==
Penned by Gulzar and composed by R D Burman, the film features following songs.

{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = no
| title1 = Roz Roz Aankhon Tale | extra1 = Asha Bhosle, Amit Kumar | lyrics1 = Gulzar | length1 = 04:14
| title2 = Dil Pukaare Jeeva Re Aare | extra2 = Asha Bhosle | lyrics2 = Gulzar | length2 = 04:54
| title3 = Bas Ek Nazar Pe Jaan Ka | extra3 = Asha Bhosle | lyrics3 = Gulzar | length3 = 05:11
| title4 = Chal Aaj Ke Din Raat Ka | extra4 = Asha Bhosle, Suresh Wadkar | lyrics4 = Gulzar | length4 = 04:37
| title5 = Aa Jagmagata Chand Hai | extra5 = Asha Bhosle | lyrics5 = Gulzar | length5 = 05:00
}}

==References==
 

==External links==
*   at the  Internet Movie Database

 
 
 
 
 