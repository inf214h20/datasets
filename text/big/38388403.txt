Say It Again (film)
{{Infobox film
| name            = Say It Again
| image           =
| caption         =
| director        = Gregory La Cava
| producer        = Adolph Zukor Jesse Lasky
| writer          = Ray Harris(story & screenplay) Luther Reed(story) Richard M. Friel
| starring        = Richard Dix Alyce Mills
| cinematographer = Edward Cronjager
| editing         =
| distributor     = Paramount Pictures
| released        =  
| runtime         = 80 minutes; 8 reels
| country         = United States
| language        = Silent film(English intertitles)
}} lost  1926 silent film comedy-romance produced by Famous Players-Lasky and released through Paramount Pictures. It starred Richard Dix and was directed by Gregory La Cava.  

==Cast==
*Richard Dix - Bob Howard
*Alyce Mills - Princess Elena
*Chester Conklin - Prince Otto V
*Gunboat Smith - Gunner Jones
*Bernard Randall - Baron Ertig
*Paul Porcasi - Count Tanza
*Ida Waterman - Marguerite
*William Ricciardi - Prime Minister Stemmler

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 