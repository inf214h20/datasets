7 Boxes
 

{{Infobox film
| title = 7 Boxes
| image = 7 Boxes film poster.png
| caption = Film poster
| director = Juan Carlos Maneglia, Tana Schémbori
| producer = Vicky Jou   Camilo Ramirez Guanes
| screenplay = Juan Carlos Maneglia
| music = Fran Villalba
| cinematography = Richard Careaga
| country = Paraguay
| released =  
| runtime = 110 minutes Spanish and Guarani Korean Korean
| budget   = $650,000
| gross = $1 million
}} Spanish as thriller film directed by Juan Carlos Maneglia and Tana Schémbori.
 San Sebastian in Spain.
 critics and the public as well as breaking box office records in Paraguayan cinemas. 7 Boxes won the "Films in Progress" in what was the first unanimous decision in the festivals history.

==Plot==
On a hot Friday in April 2005 in  , which the contractor uses to keep track of his progress, Victor begins the journey accompanied by a hyperactive young woman named Liz (Lali Gonzalez). While crossing the eight blocks covering the market, one of the boxes is stolen and Victor loses the cell phone, and the police are roaming the market searching for something. Meanwhile, a group of porters is ready to escort the boxes for almost nothing. Unknowingly, Victor, Liz, and their pursuers are involved in a crime of which they know nothing; not the cause, nor the victim or perpetrator. As night falls Victor realizes that he is now an accomplice in a dangerous crime.

==Cast==
* Celso Franco as Victor, a porter who aspires to become a famous actor. He is recruited to transport seven mysterious boxes in exchange for one hundred dollars.
* Lali Gonzalez as Liz, a young woman who accompanies Victor on his mission.
* Nelly Davalos as Tamara
* Jim Hyuk Johnny Kim as Jim
* Víctor Sosa as Nelson
* Nico García as Luis

==Accolades== Best Spanish Language Foreign Film representing Paraguay.
 San Sebastian.

==Critical reception==
7 Boxes was met with universal acclaim. The film scored a perfect rating of 100% on the review aggregate website Rotten Tomatoes based on 25 reviews as well as receiving a score of 92% on the websites audience approval rating. 

==Filming== Mercado 4, and in 2004, he began planning to film the porters and vendors who worked there. The shooting of the film took place mainly at night. 7 Boxes had a cast of 30 people and a large crew. The production included an office near the shopping area, with the support of the leadership of the Municipal Market No. 4 for logistics and safety of the film crew. The National Police accompanied the filmmakers for some sequences in which some sectors needed to be closed off for location shooting. The script provides about 75 locations for about 179 scenes. The filming of 7 Boxes lasted two months and two days of shooting, where more than 40 technicians and actors participated in the filming.

==Production==
7 Boxes was directed by Juan Carlos Maneglia and Tana Schémbori. The original script was cowritten by Maneglia and Tito Chamorro. Richard Careaga performed the cinematography, and the Synchro team carried out the coordination of the technical operation. The original music was composed by Fran Villalba, and production and post-production were performed by Schémbori and Maneglia. The executive producers are Jou Vicky Ramirez, Camilo Guanes and Rocio Galiano, while Oniria is the advertising agency of the movie.

==External links==
*  
*  
*  
*   at The International Film Festivals official website
*   at The Globe and Mail

==References==
 

 
 
 
 
 
 
 
 