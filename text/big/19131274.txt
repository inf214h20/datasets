People Meet and Sweet Music Fills the Heart
{{Infobox film
| name           = People Meet and Sweet Music Fills the Heart
| image          = People Meet and Sweet Music Fills the Heart.JPG
| caption        = Front cover of the Danish DVD
| director       = Henning Carlsen
| producer       = Göran Lindgren Henning Carlsen
| writer         = Poul Borum Henning Carlsen
| starring       = Harriet Andersson Preben Neergaard
| music          = Krzysztof Komeda
| cinematography = Henning Kristiansen
| editing        = Henning Carlsen
| distributor    = Nordisk Film
| released       = 24 November 1967
| runtime        = 111 minutes
| country        = Denmark Sweden
| language       = Danish
| budget         = 
| gross          = 
}}
 Swedish romantic comedy directed by Henning Carlsen and starring Harriet Andersson and Preben Neergaard. The film is based upon the 1944 novel by Jens August Schade.

==Cast==
{| class="wikitable" |- bgcolor="#CCCCCC" 
!   Actor !! Role
|- Harriet Andersson ||	Sofia Persson
|- Preben Neergaard || 	Sjalof Hansen
|- Eva Dahlbeck || 	Devah Sørensen
|- Erik Wedersøe || 	Hans Madsen
|- Lone Rode || 	Evangeline Hansen
|- Lotte Horne || 	Mithra
|- Elin Reimer || 	Calcura
|- Bent Christensen Bent Christensen || Ramon Salvador
|- Lotte Tarp || 	Kose
|- Knud Rex || 	Ramon Salvador
|- Georg Rydeberg || 	Robert Clair de Lune
|- Cassandra Mahon || Josefa Swell
|- Zito Kerras || 	Young Men in New York
|- Ove Rud || 	Clergyman
|- Benny Juhlin || 	Fresh Young Man
|-
|}

==Awards== Best Foreign Language Film at the 41st Academy Awards, but was not accepted as a nominee. 

==See also==
 
* List of submissions to the 41st Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  at IMDb
*  at Den Danske Film Database (in Danish)
*  at Det Danske Filminstitut (in Danish)
*  

 

 
 
 
 
 
 


 
 