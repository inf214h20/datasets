The Midnight Game
 
 
{{Infobox film
| name           = The Midnight Game
| image          = TMG-One-Sheet-2013.jpg
| caption        = 
| director       = A.D. Calvo
| producer       = A.D. Calvo
| story          = A.D. Calvo
| screenplay     = Rick Dahl
| starring       =  
| cinematography = Eun-ah Lee
| editing        = Michael Taylor
| studio         = Budderfly / Goodnight Film
| music          = 
| released       =  
| distributor    = Anchor Bay Entertainment
| country        = United States
| language       = English
| runtime        = 
}} thriller film directed by A.D. Calvo based on the creepypasta of the same name. The movie had its world release on March 2, 2013 at the Miami International Film Festival and was released to DVD on August 12, 2014.  Filming took place Wallingford, Connecticut in April and May 2012,  and stars Renee Olstead. 

==Plot== Guy Wilson) and Jeff (Spencer Daniels). Not wanting to seem uncool, Kaitlan allows the boys to stay and party. Eventually Shane suggests that they play The Midnight Game, where participants confess their deepest fears while performing a pagan ritual. Anyone who performs the ritual inaccurately runs the risk of running into the Midnight Man, a terrifying figure that can bring the participants fears to life. They must carry candles and if the candle extinguishes then they must relight it in ten seconds or surround themselves instantly with a circle of salt, or the Midnight Man will catch them. The teenagers originally think the game sounds fun and do not take the idea of the ritual seriously, beginning the game gleefully. Nothing happens for the first hour and a half, yet just as the group is starting to become complacent they begin to hear noises, and their candles start to go out. The boys decide to explore upstairs where they can hear unusual sounds, whilst the girls remain downstairs. Jeff and Shane discover a crucifix upstairs which unnerves them, and they are drawn back downstairs by the sound of the girls screaming; they saw a dark figure moving around outside the house. The candles all start to go out, and the group panics. Kaitlan attempts to end the game by turning on the lights, only to find that they do not work even though the power for the house is still running. After some time has passed, they eventually draw a circle of salt around themselves and huddle together, eventually falling asleep. At 3:33 AM the lights turn back on and the group awakes, thinking that the game is over. They do not realise that having failed to relight their candles in ten seconds and drawing the circle of salt well after they needed to, the midnight man has won the game.

The next morning, Rose awakes to find Jeff is gone. She goes downstairs to the kitchen, and finds that no one knows where Jeff is; Shane assumes that he has gone to basketball practice and texts him, with no response. Jenna, unprovoked, begins to verbally abuse Rose in a bout of sudden anger, accusing her of wanting to steal Shane and always trying to copy her. The others are stunned by Jennas sudden moodswing, and later she apologises. Shane, Kaitlan and Jenna decide to go for a hike, whilst Rose chooses to stay at the house and take a nap. On the hike, Jenna begins accusing Shane of cheating on her and acts extremely angry for no reason, until her mood suddenly changes and she is abnormally happy, seeing shapes in the clouds that do not exist and skipping along. Meanwhile, back at the house Rose begins to hear strange noises and hallucinate unnerving things. Eventually, she walks into a room and sees the Midnight Man, a demonic pitch black figure. On the hike, Shane receives a text from Rose begging him to help her, and despite Jenna screaming for no reason when he suggests they go back, he and the others return to the house to find Rose huddled in the kitchen, clutching a knife and having a panic attack from seeing the Midnight Man. Shane coaxes Rose to drop the knife, and as she does Jenna begins panicking and acting paranoid. The group deduces that this must be because of the Midnight Game, and Rose decides to go home.

On her way home, Rose starts to hallucinate the Midnight Man again. She sees Jeff at the side of the road and pulls over to talk to him, only to find that he is undead. Rose backs away, and is hit by a car. She then wakes up in the car and realises that this was a hallucination, and decides to go back to the house. Back at the house, Jenna has been growing increasingly panicked and paranoid, hallucinating that Shane cheated on her with Kaitlan. She breaks down, and Shane attempts to comfort her.

The group realise that the reason these things are happening to them is because they did not follow the rules of the Midnight Game, and they decide to play again to see if that will end the chain of horrific events. As they begin the game for the second time, Shane recalls a blog he read on the internet about a boy who did not follow the rules of the game and ended up reliving his worst fears over and over until his death, and the theories he has read that people get trapped in an endless cycle of reliving their death over and over. He fears that this will happen to them. As they play the game, the candles blow out and they are unable to relight them. They intend to surround themselves with salt, but accidentally spill the salt, leaving them trapped to face the Midnight Man. Jenna goes upstairs, where she is taunted by a hallucination saying that everyone is against her and is driven to hang herself. Kaitlan is pushed off the balcony by the midnight man. Shane receives a text from Jeff stating he is in the basement, and discovers Jeffs corpse in a casket. When he returns to the house, Rose States that "he is behind you", and he turns to see the midnight man. Shane runs out the house, pursued by the Midnight Man.

Suddenly, the clock turns to 3:33, and Shane, Rose, Kaitlan and Jenna all wake up on the floor, still alive. Jeff enters the house with a bottle of wine, and they are shocked to see that he is okay. It seems as though the five have survived the midnight game and that things will return to normal. However, the house is then shown to have been vacated for two years, and a Realtor shows a prospective Tenant around the house. He is put off buying it when he realizes it is where "the incident with the high school kids" took place and discusses the incident with a Realtor; how four high school students died, and one (Shane) disappeared, believed to have killed the others. As the man and the Realtor pull away from the house, Roses car is shown pulling up to the front as it had at the beginning of the film, and the events from the beginning play out for a few minutes. As the camera pans away from the girls entering the house, Shanes corpse is shown in a pile of leaves across the street, vaguely hinting that the five teenagers are all dead and are stuck in an endless cycle of the Midnight Game.

==Cast==
*Renee Olstead as Kaitlan
*Shelby Young as Rose Guy Wilson as Shane
*Valentina de Angelis as Jenna
*Spencer Daniels as Jeff
*Robert Romanus as Derrick

==Critical reception==
 
Fangoria gave The Midnight Game two and a half out of four skulls, writing "THE MIDNIGHT GAME could probably delve a little deeper into its characters’ hauntings/psychotic episodes and even add a few more visceral scares throughout, but Calvo’s ability to create atmosphere and build suspense lends welcome dimension to a film that could also have easily devolved into a series of shock cuts and big bangs—and a new blade on a vintage saw is rarely a bad thing."  Bloody Disgusting rated the movie at three out of five skulls, commenting that while some of the film was "generic" it was also enjoyable. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 