Tetro
{{Infobox film
| name           = Tetro
| image          = Tetroposter.jpg
| caption        = Promotional film poster
| director       = Francis Ford Coppola
| producer       = Francis Ford Coppola
| writer         = Francis Ford Coppola
| starring       = Vincent Gallo Alden Ehrenreich Maribel Verdú Klaus Maria Brandauer Carmen Maura
| music          = Osvaldo Golijov
| cinematography = Mihai Malaimare Jr.
| editing        = Walter Murch
| distributor    = Alta Films American Zoetrope
| released       =  
| runtime        = 126 minutes
| country        = United States Argentina Spain English Italian Italian Spanish Spanish
| budget         = 
| gross          = $2,636,774 
}}

Tetro is a 2009 drama film written, directed and produced by Francis Ford Coppola and starring Vincent Gallo, Alden Ehrenreich and Maribel Verdú.  Filming took place in 2008 in Buenos Aires, Patagonia, and Spain. Tetro received a limited release in the United States on June 11, 2009. 

==Plot== Italian immigrant family. 

==Cast==
* Vincent Gallo as Tetro, the protagonist. Coppola said of his casting choice, "I know choosing Vincent Gallo to star in my film will raise a few eyebrows, but Im betting that seeing him in the role will open some eyes."  Prior to Gallo, Matt Dillon and Joaquin Phoenix were up for the role. 
* Alden Ehrenreich as Bennie, Tetros younger brother.   
* Maribel Verdú as Miranda, Tetros girlfriend. 
* Sofia Gala as Maria Luisa.
* Carmen Maura as Alone, a literary critic and Tetros mentor.   The character was originally written to be male, and actor Javier Bardem was previously attached to the role. Coppola explained the change in sex, "As I read and reread (the script), I felt that the interaction between the two characters would be far more intriguing if they were of the opposite sex."    
* Klaus Maria Brandauer as Carlo Tetrocini, Tetros father. 
Also cast in the film are Rodrigo de la Serna, Leticia Brédice, Mike Amigorena and Jean-Francois Casanovas.  The film features a brief cameo as well by Argentinian film star Susana Giménez in her first performance after a ten-year hiatus from film acting. 

==Production==
In February 2007,  .   Filming took place in La Boca in Buenos Aires and other parts of the capital city.  Filming also followed in the Andean foothills in Patagonia and at the Ciudad de la Luz studios in Alicante, Spain.   Production concluded in June.   

In May 2008, during filming in Argentina, the Argentina Actors Association, an actors union, claimed that production of Tetro was shut down due to union members working on the film without a contract.  According to The Hollywood Reporter, "Local press reports say that script changes and communication problems between the multi-national cast and crew have extended filming days beyond regularly scheduled hours, and that some of the Argentine actors are still not certain of their salary."  The directors spokesperson, Kathleen Talbert, denied that production was halted, saying, "There are no holds on shooting, no problem with actors. In fact, the majority of the Argentine actors have already wrapped the shooting."   By the end of the month, the union said the issue was resolved, reporting, "The lawyers for the producers presented the necessary documentation and recognized the errors that they had made. So now they are able to continue with production."  In contrast, Talbert reiterated that there had been no issue, and production was never halted.   
 

The entire project was edited using Final Cut Pro on Apple Mac computers in a specially designed large screen edit suite built by Masa Tsuyuki. 

==Reception==
The film received generally positive reviews from critics. On Metacritic, the film has an average metascore of 65 based on 26 reviews.  Rotten Tomatoes reported that 71% of critics gave positive reviews based on 99 reviews with an average score of 6.3/10.    Overall, the Rotten Tomatoes consensus was: "A complex meditation on family dynamics, Tetros arresting visuals and emotional core compensate for its uneven narrative." 

Roger Ebert of the Chicago Sun-Times gave the film 3 stars, praising the film for being "boldly operatic, involving family drama, secrets, generations at war, melodrama, romance and violence". Ebert also praised Vincent Gallos performance and claimed Alden Ehrenreich is "the new Leonardo DiCaprio".  Todd McCarthy of Variety (magazine)|Variety gave the film a B+ judging that "when   finds creative nirvana, he frequently has trouble delivering the full goods."  Richard Corliss of Time (magazine)|TIME gave the film a mixed review, praising Ehrenreichs performance, but claiming Coppola "has made a movie in which plenty happens but nothing rings true." 

===Top ten lists===
6th - Cahiers du cinéma 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 