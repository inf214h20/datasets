On a Clear Day (film)
{{Infobox Film
| name           = On a Clear Day
| image          = On A Clear Day film poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Gaby Dellal
| producer       = Dorothy Berwin Sarah Curtis
| writer         = Alex Rose
| narrator       = 
| starring       = Peter Mullan Brenda Blethyn Sean McGinley
| music          = Stephen Warbeck
| cinematography = David Johnson
| editing        = Robin Sales John Wilson
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Scotland
| language       = English
| budget         = 
}} Billy Boyd, among others.
 BAFTA Scotland Awards for Best Film and Best Screenplay.

==Plot summary== RFA Mounts Bay, Frank Redmond (played by Peter Mullan) and a few of his co-workers are laid off from the shipyards after 36 years service. This, along with his grief still suffered over the drowning of one of his sons many years ago, plummets Frank into a deep depression. He gets on well with his wife, Joan (Brenda Blethyn), but their relationship is distant. His other son, Rob (Jamie Sives), is a devoted house husband who looks after his twin sons, while his wife, Angela (Jodhi May) works full-time at the local Jobcentre. Rob has a troubled relationship with his father, feeling the guilt of being the surviving son.
 Billy Boyd), Eddie (Sean McGinley) and Norman (Ron Cook) until he feels he is fit and ready for the attempt. A successful crossing alleviates the family tensions.

==Filming locations==
The production visited Kent to shoot at the foot of the White Cliffs of Dover where Frank starts his swim of the English Channel and the famous cliffs can also be seen in the background throughout his challenge. The Port of Dover was used for the scenes where Frank’s family and friends race to meet him in France at the end of his swim. 

==Cast==
* Peter Mullan — Frank Redmond
* Brenda Blethyn — Joan Redmond
* Jamie Sives — Rob Redmond
* Jodhi May — Angela Redmond Billy Boyd — Danny
* Benedict Wong — Chan
* Sean McGinley — Eddie
* Ron Cook — Norman
* Shaun Dingwall — Observer Tony Roper — Merv

==External links==
*  

==References==
 

 
 
 
 