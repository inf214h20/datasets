Once a Lady
{{infobox film
| name           = Once a Lady
| image          =
| imagesize      =
| caption        =
| director       = Guthrie McClintic
| producer       = Adolph Zukor
| writer         = Rudolf Bernauer (play Das Zweite Leben) Rudolf Osterreicher (play Das Zweite Leben) Zoë Akins (writer) Samuel Hoffenstein (writer)
| starring       = Ruth Chatterton
| music          =
| cinematography = Charles Lang
| editing        =
| distributor    = Paramount Pictures
| released       = November 7, 1931
| runtime        = 80 minutes
| country        = United States
| language       = English
}}
Once a Lady (1931 in film|1931) is an American drama film directed by Guthrie McClintic and starring Ruth Chatterton, Ivor Novello and Jill Esmond. The film, produced and distributed by Paramount Pictures, is a remake of the Pola Negri silent film Three Sinners (1928).    The film is notable as the final attempt of the British matinée idol Novello to break into Hollywood films. 

==Synopsis==
A young Russian woman (Chatterton) marries a wealthy Englishman (Novello), and has a daughter with him. After she has an affair with one of his friends, she is forced to leave Britain and moves to Paris. Many years later, her daughter approaches her, needing her help.

==Cast==
*Ruth Chatterton - Anna Keremazoff
*Ivor Novello - Bennett Cloud
*Jill Esmond - Faith Penwick the Girl
*Geoffrey Kerr - Jimmy Fenwick
*Doris Lloyd - Lady Ellen Somerville
*Herbert Bunston - Roger Fenwick
*Gwendolyn Logan - Mrs. Fenwick
*Stella Moore - Alice Fenwick
*Edith Kingdon - Caroline Gryce
*Bramwell Fletcher - Allen Corinth
*Theodore von Eltz - Harry Cosden
*Ethel Griffies - Miss Bleeker Claude King - Sir William Gresham
*Lillian Rich - Jane Vernon
*Susanne Ransom - Faith Fenwick, the Child

==References==
 

==Bibliography==
* Williams, Michael. Ivor Novello: Screen Idol. British Film Institute, 2003.

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 