Happy Birthday (2002 film)
{{Infobox film
| name = Happy Birthday
| image = Happy-birthday-by-yen-tan.jpg
| image_size =
| caption =
| director = Yen Tan
| producer = Mark Buchanan
| writer = Yen Tan
| narrator =
| starring =
| music = Steve Whitehouse
| cinematography = Jack Burroughs
| editing = Jay Wesson
| distributor =
| released =  
| runtime = 70 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Happy Birthday is a 2002 film by Yen Tan, his debut long feature film, starring Benjamin Patrick, Michelle E. Michael and John Frazier.

==Synopsis==
The film recounts two days in the lives of five very different characters all born on July 12, that are faced with problems as their birthday approaches. Jim (Benjamin Patrick), a gay, overweight telemarketer working for a weight loss program is facing self-esteem issues, whereas Ron (John Frazier), a church minister who preaches about conversion but himself is addicted to watching gay porn, and Javed (Devashish Saxena), a Pakistani lives in the U.S. with a gay porn actor but is faced by the double dilemma of being condemned by his Muslim family and is in imminent danger of being deported from the States.

Meanwhile Kelly (Michelle E. Michael), a young lesbian weathers a breakup with her lover and considers an earlier affair and Tracy (Ethel Lung), an Asian lesbian, goes back in the closet when her mother renders a visit.

==Cast==
*Benjamin Patrick as Jim
*Michelle E. Michael as Kelly
*John Frazier as Ron
*Devashish Saxena as Javed
*Ethel Lung as Tracy
*Denton Blane Everett as Greg
*Xiao Fei Zhao as Mom
*Lynn Chambers as Julie
*Derik Webb as Troy
*Chip Gilliam as Brian	
*Natalie Thrash as Tricia
*Debbie Rey as Sophia
*Ryan Harper as Ricky
*James M. Johnston as Porn director
*David Lowery as Videographer

==Awards==
*In 2002, won the Jury Prize for "Best Feature - Gay Male" at the Philadelphia International Gay & Lesbian Film Festival
*In 2002, director Yen Tan won New Directors Showcase - Bets Feature award for Happy Birthday at the Portland LGBT Film Festival
*Also earned an honorable mention at Image+Nation in Montreal

==External links==
* 
* 

 
 
 
 
 
 