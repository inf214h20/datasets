Monkeys in Winter
  Bulgarian film directed by Milena Andonova.

The film took part in a number of film festivals and won a series of rewards, the most significant of them are following:
* Karlovy Vary International Film Festival, 2006 - best film in competition program "East of West"
* Bulgarian Film Festival "Golden Rose", Varna, Bulgaria, September 2006 - the Grand Prize for Bulgarian cinema and other rewards (for the 3 actresses in the main roles)
* National Film Center, Sofia - Best Feature Film for 2006

It was Bulgarias submission to the 79th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.      

==See also==
 
*Cinema of Bulgaria
*List of submissions to the 79th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 

 