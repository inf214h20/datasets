Gopura Deepam
{{Infobox film
| name           = Gopura Deepam
| image          =
| image_size     =
| caption        =
| director       = Ramarajan
| producer       = Ashok Samraj
| writer         = Ramarajan
| screenplay     = Ramarajan Sukanya R. Sundarrajan Senthil
| music          = Sowndharyan
| cinematography = K. B. Dhayalan
| editing        = L. Kesavan
| studio         = Kashtoori Films International
| distributor    = Kashtoori Films International
| released       =  
| country        = India Tamil
}}
 1997 Cinema Indian Tamil Tamil film,  directed by  Ramarajan and produced by Ashok Samraj. The film stars Ramarajan, Sukanya (actress)|Sukanya, R. Sundarrajan and Senthil in lead roles. The film had musical score by Soundaryan.  

==Cast==
 
*Ramarajan Sukanya
*R. Sundarrajan
*Senthil Pandu
*Madhan Bob
*Kovai Sarala
*Karikalan
*Balu Ananth
*Varalakshmi
*R. S. Sivaji
*Alwa Vasu
*Thiruppur Ramasamy
*Vittal Prasath
*Samikannu
*T. S. Ragavendhar
*Oru Viral Krishna Rao
*Kullamani
*Arjunan
*Vellai Subbiah
*Somanathan
*Theni Kunjaramma
*Nikila
*Vijayasanthi
*Arunadevi
 

==Soundtrack==
The music was composed by Soundaryan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maama Yeai || Mano (singer)|Mano, Swarnalatha || Vairamuthu || 5:01
|- Mano || Vairamuthu || 4:12
|-
| 3 || En Vaazhkkai Mannavane || K. S. Chitra || Vairamuthu || 5:16
|-
| 4 || Ullame Unakkuthan || S. P. Balasubrahmanyam, Anuradha Sriram || Vairamuthu || 5:08
|-
| 5 || Gangai Kaayum || S. P. Balasubrahmanyam, Swarnalatha || Vairamuthu || 4:34
|-
| 6 || Saanja || Mano (singer)|Mano, Swarnalatha || Vairamuthu || 4:45
|}

==References==
 

==External links==
*  

 
 
 
 


 