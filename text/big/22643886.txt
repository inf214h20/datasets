Jenny (1958 film)
 
{{Infobox film
| name           = Jenny
| image          = 
| image_size     = 
| caption        = 
| director       = Willy van Hemert
| producer       = 
| writer         = Erich Waschneck (screenplay) & Hanns H. Fischer (writer)
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = February 21, 1958
| runtime        = 98 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1958 Netherlands|Dutch film directed by Willy van Hemert. It is considered the first "chick flick".

The movie is known as the first Dutch film in color, the production is a remake of the 1932 German-film Acht madel im boot.

storyline is about the happy 18 year old Jenny, who is a sport-athleet, but get suddenly pregnant from her boyfriend Ed. He leaves her and she must make a difficult decision, but everything comes to good ends.

==Cast==
*Ellen van Hemert	... 	Jenny Roders
*Maxim Hamel	... 	Ed van Rijn
*Andrea Domburg	... 	Greet
*Kees Brusse	... 	Dr. Henk Ebeling
*Ko van Dijk	... 	Meneer Roders (as Ko van Dijk jr.)
*Teddy Schaank	   	...     Mevrouw Gonzales
*Bert van der Linden	... 	Kees
*Nell Knoop		

== External links ==
*  

 
 
 

 