Taur Mittran Di
 
 
{{Infobox film
| name           = Taur Mittran Di
| image          =
| alt            =
| caption        =
| director       = Navaniat Singh
| producer       = Eros Entertainment & Jimmy Sheirgill Productions
| writer          = Dheeraj Rattan
| starring       = Amrinder Gill Rannvijay Singh Mukesh Rishi Surveen Chawla Amita Pathak
| music          = Jaidev Kumar	
| released       = 11 May 2012
| language       = Punjabi
| country        = India
| budget         =  
| gross         =  
}}
 action comedy comedy film starring Amrinder Gill, Rannvijay Singh, Surveen Chawla, Amita Pathak and Mukesh Rishi. The movie is directed by Navaniat Singh and produced by Eros Entertainment & Jimmy Sheirgill Productions. The movie was released on 11 May 2012 .This is the first film in Punjabi film ind. where the youth power and love for hockey is shown. Japji Khera did a cameo role in this film.

==Cast==
* Amrinder Gill
* Rannvijay Singh
* Mukesh Rishi
* Surveen Chawla
* Amita Pathak
* Binnu Dhillon
* Japji Khera(Guest appearance)

==Soundtrack==
{{tracklist
| extra_column = Artist(s)
| title1 = Assi Munde Haan Punjabi
| extra1 = Amrinder Gill, Praky B
| length1 = 3:24
| title2 = Dil Tera Ho Gaya
| extra2 = Amrinder Gill, Sumitra Iyyer
| length2 = 3:58
| title3 = Darshan Di Bukh
| extra3 = Amrinder Gill, Jaggi Singh, Shipra Goyal
| length3 = 4:36
| title4 = Nai Rukna
| extra4 = Amrinder Gill, Manpal Singh
| length4 = 4:21
| title5 = Taur Mittran Di
| extra5 = Amrinder Gill, Gajendra Verma
| length5 = 4:34
}}

==References==
 

 

 
 
 

 