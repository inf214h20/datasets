A Fire Has Been Arranged
{{Infobox film
| name           = A Fire Has Been Arranged
| image          = 
| caption        = 
| director       = Leslie S. Hiscott
| producer       = Julius Hagen
| writer         = Michael Barringer James A. Carter H. Fowler Mear H. Fowler Mear
| screenplay     = 
| story          = 
| based on       =  
| starring       = Chesney Allen Bud Flanagan Harold French
| music          = W.L. Trytel
| cinematography = Sydney Blythe
| editing        = Michael C. Chorlton
| studio         = 
| distributor    = 
| released       = 
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
A Fire Has Been Arranged is a 1935 British comedy film directed by Leslie S. Hiscott and starring Chesney Allen, Bud Flanagan and Alastair Sim. 

After a spell in prison three criminals return to recover their loot only to find the place where they have stashed it has been turned into a department store. They take jobs at the store in order to locate the whereabouts of their loot.  The three discover that the unscrupulous managers of the store, Shuffle and Cutte, are keen to be accomplices in their plot.

It was made at Twickenham Studios. The film ends with the song Where the Arches Used To Be.

==Cast==
* Chesney Allen - Ches
* Bud Flanagan - Bud
* Hal Walters - Hal
* Harold French - Toby Mary Lawson - Betty
* Alastair Sim - Cutte
* C. Denier Warren - Shuffle
* Robb Wilton - Oswald
* Vincent Holman - Ex-detective
* Jack Vyvian - Prison Warder
* The Buddy Bradley Rhythm Girls - Shop Girls

==References==
 

==Bibliography==
* Sutton, David R. A chorus of raspberries: British film comedy 1929-1939. University of Exeter Press, 2000.

==External links==
*  

 
 
 
 
 
 
 
 


 