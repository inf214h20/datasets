The Magnificent Ambersons (film)
{{Infobox film
| name           = The Magnificent Ambersons
| image          = Magnificent_ambersons_movieposter.jpg
| caption        = Theatrical poster with art by Norman Rockwell Freddie Fleck (assistant)
| producer       = Orson Welles
| screenplay     = Orson Welles
| based on       = The Magnificent Ambersons by Booth Tarkington
| narrator       = Orson Welles	
| starring       =  {{Plain list|
* Joseph Cotten
* Dolores Costello
* Anne Baxter
* Tim Holt
* Agnes Moorehead Ray Collins
* Erskine Sanford Richard Bennett
}} No credit in film  
| cinematography = Stanley Cortez
| editing        = Robert Wise Mercury Productions RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   (USA) 1946 (France)
| runtime        = 88 minutes 148 minutes (original) 131 minutes (preview)
| country        = United States
| language       = English
| budget         = $1.1 million  , 2006, ISBN 0-8131-2410-7  
| gross          = 210,966 admissions (France, 1946)   at Box Office Story 
}} period drama 1918 novel, social changes Ray Collins, with Welles providing the narration. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p173 

Welles lost control of the editing of The Magnificent Ambersons to RKO, and the final version released to audiences differed significantly from his rough cut of the film. More than an hour of footage was cut by the studio, which also shot and substituted a happier ending. Although Welless extensive notes for how he wished the film to be cut have survived, the excised footage was destroyed. Composer Bernard Herrmann insisted his credit be removed when, like the film itself, his score was heavily edited by the studio.
 Academy Awards, Best Picture, and it was added to the National Film Registry of the Library of Congress in 1991.

==Plot== Ray Collins]]
The Ambersons are by far the wealthiest family in the small midwestern city of Indianapolis. It is the turn of the 20th century, and life is peaceful. Eugene Morgan as a young man courts Isabel Amberson, but she rejects him even though she loves him. Isabel instead marries Wilbur Minafer, a passionless man she does not love. They have a child, George, whom she spoils and who becomes the terror of the town.

George Amberson Minafer, on break from college, returns to his home. His mother Isabel and Major Amberson, his grandfather, hold a reception in his honor.  Among the guests are the widowed Eugene Morgan, now a prosperous automobile manufacturer who has just returned to town after a 20-year absence, and his daughter Lucy. George instantly takes to the beautiful and charming Lucy, but takes as quick a dislike to Eugene.

 , Tim Holt]]

Georges father Wilbur dies. As Eugenes automobile plant prospers, the industrialist builds a mansion to rival the magnificence of that of Major Amberson (where his daughter and George also live). During a dinner party, George tells Eugene that he thinks "automobiles are a useless nuisance, which had no business being invented."  The other family members are taken aback by his rudeness, but Eugene says that George may turn out to be right, since he knows that automobiles are going to drastically alter human civilization, for better or worse.

During the evening George learns from his uncle Jack and his aunt Fanny that Isabel and Eugene were an item and is enraged when Fanny implies that Isabel loved Eugene, not Georges father.

Eugene courts Isabel again and decides to ask her to marry him. Sensing the developing intensity of their relationship, George takes control and rebuffs a planned visit from Eugene at the door of the Amberson mansion. Isabels love for her son overrides her love for Eugene, so she complies with Georges demands, although she knows that he is trying to separate her from Eugene. George takes Isabel on a world tour, ostensibly to get away from the "scandalous" talk in the town, but also to remove her from the possibility of a relationship with him. Before leaving for Europe, George tries to learn what Lucy is feeling, but she feigns cheerful insouciance, concealing her pain.

George and Isabel travel and live in Europe for a while. After she becomes ill, they return home, where George acts as gatekeeper for the dying Isabel. Eugene comes to the house to visit, but George refuses to let him see Isabel, who is on her deathbed.
 North American chieftain who was "pushed out on a canoe into the sea" when he became too obnoxious and overbearing, which Eugene understands to be an analogy for George.

With the entire Amberson fortune depleted, George gives up his job at a law firm for higher-paying work in dangerous trades that will enable him to care for Fanny, who has descended into psychosis. The film ends with George wandering around a polluted city, confused and disoriented by the industrial society that has developed around him.

Additional ending scenes show George getting injured in an automobile accident, and Eugene and Lucy reconciling with him at the hospital.

==Cast==
 
  
 
* Joseph Cotten as Eugene
* Dolores Costello as Isabel
* Anne Baxter as Lucy
* Tim Holt as George
* Agnes Moorehead as Fanny Ray Collins as Jack
* Erskine Sanford as Roger Bronson Richard Bennett as Major Amberson
* Don Dillaway as Wilbur
* Orson Welles as The Narrator
 
==Production==

===Adaptation history=== Vitagraph Pictures, David Smith.  About 30 minutes of its original 70 minute running time is known to have survived.
 Mercury Players on The Campbell Playhouse, with Orson Welles portraying George Minafer, and providing narration. While Welles supplied narration to the film adaptation, Ray Collins was the only actor from that production to appear in the film.

===Production history===
The Magnificent Ambersons was in production October 28, 1941 – January 22, 1942, at RKOs Gower Street studios in Los Angeles. The set for the Amberson mansion was constructed like a real house, but it had walls that could be rolled back, raised or lowered to allow the camera to appear to pass through them in a continuous take.  RKO later used many of the films sets for its low-budget films, including a series of horror films produced by Val Lewton.
 East Los Angeles.  Snow scenes were shot in the Union Ice Company ice house in downtown L.A.    The film was budgeted at $853,950 but this went over during the shoot and ultimately exceeded $1 million. Richard B. Jewell, RKO Radio Pictures: A Titan is Born, University of California 2012 p 239-240 

The original rough cut of the film was approximately 135 minutes in length. Welles felt that the film needed to be shortened and, after receiving a mixed response from a March 17 preview audience in Pomona, California|Pomona, film editor Robert Wise removed several minutes from it.  The film was previewed again, but the audiences response did not improve.

Because Welles had conceded his original contractual right to the final cut (in a negotiation with RKO over a film which he was obliged to direct but never did), RKO took over editing once Welles had delivered a first cut. RKO deleted more than 40 additional minutes and reshot the ending in late April and early May, in changes directed by assistant director Fred Fleck, Robert Wise, and Jack Moss, the business manager of Welless Mercury Theatre. The retakes replaced Welless original ending with a happier one that broke significantly with the films elegiac tone. The reshot ending is the same as in the novel.
 another project for RKO – Nelson Rockefeller had personally asked him to make a film in Latin America as part of the wartime Good Neighbor Policy    – his attempts to protect his version ultimately failed. Details of Welless conflict over the editing are included in the 1993 documentary about the Brazilian film Its All True (film)|Its All True.   

"Of course I expected that there would be an uproar about a picture which, by any ordinary American standards, was much darker than anybody was making pictures," Welles told biographer Barbara Leaming. "There was just a built-in dread of the downbeat movie, and I knew Id have that to face, but I thought I had a movie so good — I was absolutely certain of its value, much more than of  Kane … Its a tremendous preparation for the boardinghouse … and the terrible walk of George Minafer when he gets his comeuppance. And without that, there wasnt any plot. Its all about some rich people fighting in their house."    

"And you know, I never would have gone to South America without a guarantee that Id be able to finish my picture there," Welles said. "That I think is a matter of record. And they absolutely betrayed me and never gave me a shot at it. You know, all I could do was send wires … But I couldnt walk out on a job which had diplomatic overtones. I was representing America in Brazil, you see. I was a prisoner of the Good Neighbor Policy. Thats what made it such a nightmare. I couldnt walk out on Mr. Roosevelts Good Neighbor Policy with the biggest single thing that theyd done on the cultural level, and simply walk away. And I coudnt get my film in my hands."  

The negatives for the excised portions of The Magnificent Ambersons were later destroyed in order to free vault space. IMDB    A print of the rough cut sent to Welles in Brazil has yet to be found and is generally considered to be lost, along with the prints from the previews. Robert Wise maintained that the original was not better than the edited version. 

The film features what could be considered an  , with a photo of Joseph Cotten, who portrayed Leland in the earlier film.

===Budget===
The budget for The Magnificent Ambersons was set at $853,950, roughly the final cost of Citizen Kane. During shooting the film went over budget by 19 percent ($159,810), bringing the cost of the Welles cut to $1.0 million. RKOs subsequent changes cost $104,164. The final cost of the motion picture was $1.1 million.  

===Deleted footage===
 
 
More than 40 minutes of Welless original footage was deleted, with portions reshot. Welles later said, "They destroyed Ambersons, and it destroyed me."

Among the material deleted:

# As Morgan went to court Isabel, he waved hello to some townspeople.
# Welless two favorite scenes of the film were the ball sequence and the boarding house finale. When the film was reedited, the ball sequence was severely reduced, and the finale was removed. Welles said that the ball sequence, in its original form, was the greatest technical achievement of the film. It consisted of a long take. It had a continuous, carefully choreographed crane shot that traveled up the three floors of the mansion to the ballroom on the top floor. Various characters conversed and moved in and out of the frame as the camera wove around them. To pick up the pace, editors removed the middle section of the shot.
# In the ball scene, George mentions to Lucy that he saw some people who were in a club he was in many years ago. He also mentions that he was the president of this club. A film passage showed the past references unfolding. George joined the Friends of the Ace (the local secret society). He insisted on being president of the society and threatened to take away their clubroom if not elected - he became president.
# The kitchen scene, where George and Jack tease Fanny until she runs away crying, was the beginning of a longer scene. George spots his grandfathers new construction through the window. He runs out into the rain and shouts his outrage over the roar of the storm. At the Pomona preview, audiences screamed with laughter during this dramatic scene.
# The first of two porch scenes, a long take that lasted six minutes without a cut. It showed Isabel and Fanny chatting about their changing town while George sits lost in his own thoughts. Isabel goes inside, and Fanny worries aloud to George that Isabel is being too hasty to finish mourning her late husband Wilbur. After his aunt Fanny leaves, George fantasizes about Lucys begging his forgiveness. He next imagines her socializing with other men without thinking of him at all.
# The second porch scene, also a long take, lasting three minutes. Fanny and Major Amberson discussed his financial problems. The two decided to invest in a headlight company. It is this investment that later ruins them. This was Richard Bennetts (the Major) longest scene in the film.
# After George and Jack argued about Morgan and Isabel, there was an additional scene. George is furious and tries to avoid his mother Isabel by going into the ballroom. When she follows, George stands still with his back to her. She tries to wish him goodnight, not knowing that he is upset with her, but he is quiet and utters only a few words.
# Isabel and George discuss her relationship with Morgan in her bedroom. The version that appears in the released film is not that of Welles. In the original version, George was more vicious when he condemned Morgan.
# Isabel writes a letter to George, asking for his forgiveness for the suffering she has caused him over Eugene. She reassures him that she will no longer see Eugene. She slips the letter under his door.
# After talking with George, Lucy went into a nearby shop and fainted in front of the clerk. Later the clerk visited a pool hall and told his friends about the pretty young woman fainting in his shop that day.
#When Isabel is dying, Eugene Morgan arrives to see her. In Welless version, Fanny whispers to George that Eugene has arrived. George tells her to get rid of him, motivated by anger and spite. Fanny, alone (and in a harsh manner), tells Eugene to leave. In the altered cut, Jack, George and Fanny all tell Morgan he should leave, with suggestions they are thinking of Isabels comfort.
# After the death of his daughter Isabel, Major Amberson has a major scene in which he ponders the origin and meaning of life. In the recut version, after the scene ends, the major is never seen again (he seems to pass away off screen). In the original cut, Major Amberson continues to think aloud, and says, "I wish someone would tell me....." as the film dissolves to the graves of Isabel and her father. Audiences did not like the occurrence of Isabel and the Majors deaths happening in such quick succession.
# In Agnes Mooreheads signature scene, Aunt Fanny breaks down in front of George in the boiler room. The original scene showed Fanny as extremely distraught and out of control. While this was said to be Mooreheads best scene, the test screening audience laughed as they had at many of her scenes. The scene was reshot with Moorehead playing a more subdued Fanny.
# After Fanny and George move out of the Amberson mansion, George wanders through town and gets his comeuppance. Cinematographer Stanley Cortez filmed a long tracking shot revealing the empty and decaying mansion. Cortez was extremely proud of this shot, but Welles did not use it in his final cut.

===Original ending===
  Chekhov than Tarkington. Audiences were uncomfortable with it and the final scene was reshot. The new "happy ending" shows Morgan and Fanny leaving the hospital after having seen George. Morgan talks about their reconciliation, and when he finishes with "true at last to my true love", Fanny smiles happily and they leave together.

Before the final editing, Welles proposed to the studio that they keep his ending, but have a "cheerful closing credits" sequence to send audiences out happy. This would have included showing an oval picture of a younger Major Amberson (Richard Bennett) in Civil War uniform; an image of Ray Collins sitting on a veranda with the ocean behind him; Agnes Moorehead busily playing cards with friends in the boarding house, and Joseph Cotten looking out a window as Tim Holt and Anne Baxter drive away together waving to him. Similar images would have been shown for Dolores Costello, Erskine Sanford and Don Dillaway.

===Score===
Like the film itself, Bernard Herrmanns score for The Magnificent Ambersons was heavily edited by RKO. When more than half of his score was removed from the soundtrack, Herrmann bitterly severed his ties with the film and promised legal action if his name were not removed from the credits. 

Portions of Herrmanns score were replaced with music by Roy Webb. The movie also included other music not by Herrmann — for example, an arrangement of the obscure Parisian waltz Toujours ou jamais by Émile Waldteufel. 
 Wuthering Heights (1943–51).  Coincidentally, Orson Welles was invited to direct the premiere production of the opera in Portland, Oregon in 1982, but he declined. 

===Spoken credits===
 
The Magnificent Ambersons is one of the earliest films in movie history in which nearly all the credits are spoken by an off-screen voice and not shown printed onscreen — a technique used before only by the French director and player Sacha Guitry. The only credits shown onscreen are the RKO logo, "A Mercury Production by Orson Welles", and the films title, shown at the beginning of the picture. At the end of the film, Welless voice announces all the main credits. Each actor in the film is shown as Welles announces the name. As he speaks each technical credit, a machine is shown performing that function. 

Welles reads his own credit — "My name is Orson Welles" — over top of an image of a microphone which then recedes into the distance. 
 the radio in a way they were used to hearing on our shows. In those days we had an enormous public — in the millions — who heard us every week, so it didnt seem pompous to end a movie in our radio style." Welles, Orson, and Peter Bogdanovich, edited by Jonathan Rosenbaum, This is Orson Welles. New York: HarperCollins Publishers 1992 ISBN 0-06-016616-9  

===Welless 1970s revisit===
In conversations (1969–1975) with Peter Bogdanovich compiled in This is Orson Welles, Welles confirmed that he had planned to reshoot the ending of The Magnificent Ambersons with the principal cast members who were still living:

 Yes, I had an outside chance to finish it again just a couple of years ago, but I couldnt swing it. The fellow who was going to buy the film for me disappeared from view. The idea was to take the actors who are still alive now — Cotten, Baxter, Moorehead, Holt — and do quite a new end to the movie, twenty years after. Maybe that way we could have got a new release and a large audience to see it for the first time.  You see, the basic intention was to portray a golden world — almost one of memory — and then show what it turns into. Having set up this dream town of the "good old days," the whole point was to show the automobile wrecking it — not only the family but the town. All this is out. Whats left is only the first six reels. Then theres a kind of arbitrary bringing back down the curtain by a series of clumsy, quick devices. The bad, black world was supposed to be too much for people. My whole third act is lost because of all the hysterical tinkering that went on. And it was hysterical. Everybody they could find was cutting it.   

==Reception==

===Box office===
The film recorded a loss of $620,000. Richard Jewell, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p45 

===Critical===
The film has been very well received by critics. Rotten Tomatoes, the review aggregator, reports that 96% of the critics gave the film a positive review, with only one negative. While not as acclaimed as Citizen Kane, it is considered one of Welless best works. It and Citizen Kane were the only of Welless films to be nominated for Best Picture at the Academy Awards.

===Accolades===
In 1991, The Magnificent Ambersons was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". The film was included in Sight and Sounds 1972 list of the top ten greatest films ever made,  and again in 1982s list. 

====Awards====
;Wins
* New York Film Critics Circle Awards: NYFCC Award; Best Actress, Agnes Moorehead; 1942.
* National Board of Review: Best Acting, Tim Holt & Agnes Moorehead, 1942

Academy Award Nominations    Best Actress in a Supporting Role - Agnes Moorehead Best Black-and-White Art Direction-Interior Decoration - Albert S. DAgostino, A. Roland Fields and Darrell Silvera Best Black-and-White Cinematography - Stanley Cortez Best Picture - Orson Welles

==Film memorabilia==
In an auction April 26, 2014, a script of The Magnificent Ambersons was sold for $10,625  and a collection of approximately 275 stills and production photos sold for $2,750.  The materials were among those found in boxes and trunks of Welless personal possessions by his daughter Beatrice Welles. 

==Home video releases==
* 1985: RKO Home Video, VHS (2073), 1985
* 1986: The Voyager Company (The Criterion Collection), Laserdisc, 1986—audio commentary by Robert Carringer  Turner Home Entertainment, VHS, December 27, 1989, colorized version 
* 2011: Warner Home Video, Region 1 DVD, September 13, 2011 (Amazon.com exclusive); January 31, 2012 (general release) ISBN 0-7806-7648-3

==Soundtrack releases==
A CD of the soundtrack to this film was released in 1990 in the US. The pieces were totally re-recorded. 

All pieces by Bernard Herrmann. Re-recorded by the Australian Philharmonic Orchestra conducted by Tony Bremner.

#"Theme and Variations/Georges Homecoming" (07:18)
#"Snow Ride" (03:05)
#"The Door/Death and Youth" (00:56)
#"Toccata" (01:12)
#"Pleasure Trip" (01:06)
#"Prelude" (01:30)
#"First Nocturne" (04:08)
#"Garden Scene" (01:14)
#"Fantasia" (02:11)
#"Scene Pathetique" (02:19)
#"Waiting" (01:32)
#"Ostinato" (01:52)
#"First Letter Scene" (03:25)
#"Second Letter Scene/Romanza" (02:12)
#"Second Nocturne" (03:22)
#"Departure/Isabels Death" (01:47)
#"First Reverie/Second Reverie" (02:40)
#"The Walk Home" (02:49)
#"Garden Music" (02:59)
#"Elegy" (01:23)
#"End Title" (02:20)

==Remake== The Magnificent Ambersons was made as an A&E Network original film for television, using the Welles screenplay and his editing notes. Directed by Alfonso Arau, the film stars Madeleine Stowe, Bruce Greenwood, Jonathan Rhys Meyers, Gretchen Mol and Jennifer Tilly.  This film does not strictly follow Welless screenplay. It lacks several scenes included in the 1942 version, and has essentially the same happy ending.

==See also==
*List of incomplete or partially lost films
*List of films cut over the directors opposition

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   (public domain) at YouTube
*   website

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 