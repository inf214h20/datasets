Dark Blue World
{{Infobox film
| name           = Dark Blue World
| image          = Dark blue world dvd.jpg
| caption        = Australian DVD Cover
| director       = Jan Svěrák
| producer       = Eric Abraham
| writer         = Zdeněk Svěrák
| starring       = Ondřej Vetchý Tara FitzGerald Oldřich Kaiser Charles Dance Kryštof Hádek
| music          = Ondřej Soukup
| cinematography = Vladimír Smutný
| editing        = Alois Fišárek
| distributor    = Columbia TriStar Film Distributors (media worldwide)
| released       =        
| runtime        = 115 minutes
| country        = Czech Republic United Kingdom
| language       = Czech English German Slovak 
| budget         = € 8 million
| gross          =  $2,300,000 (Worldwide)
| preceded_by    = 
| followed_by    = 
}} Czech director Czech pilots Second World War. The screenplay was written by Zdeněk Svěrák, the directors father. The film stars Czech actors Ondřej Vetchý, Kryštof Hádek and Oldřich Kaiser.  British actors include Tara Fitzgerald, Charles Dance and Anna Massey.

==Plot== German invasion of Czechoslovakia. After the invasion, the Czechoslovakian military is disbanded and has to give up its aircraft. However, young pilots Franta and his friend Karel Vojtíšek (Kryštof Hádek), among others, refuse to submit to their occupiers and flee to the United Kingdom to join the RAF.

The British make the Czechoslovaks retrain from the basics, which infuriates them, especially Karel, who is both impatient to fight the Germans and humiliated at being retaught what he already knows. Karel also sees the compulsory English language lessons as a pointless waste of his time.

The RAF is in such a dire need of pilots during the  . Franta becomes the unit commander, with the younger Karel under his charge.
 Spitfire fighter aircraft. However, he manages to bail out and find his way to a farm. There he meets and falls in love with Susan (Tara Fitzgerald), although she thinks he is far too young. The next day, after returning to the aerodrome, Karel brings Franta to meet Susan. The latter begins to get on well with Susan, although Karel believes that he is still Susans boyfriend.
 squadron attacks a train, Karel is shot down, but Franta lands and rescues him, a move that shows that the their friendship endures. Soon after, Karel learns a sort of love triangle has developed, with Susan being involved with Franta, which leads to a quarrel between the two friends.

A few missions later, while escorting American bombers, Frantas Spitfire malfunctions and he is forced to ditch into the ocean. His life raft bursts as he tries to inflate it, so Karel decides  to drop his own raft, but he flies too low and fatally crashes. The raft emerges from the water, allowing Franta to survive until he is rescued.

Afterward, when the war is over, Franta drives to Susans home, only to find her with her injured husband recently returned from fighting overseas. Knowing he has no future with Susan and wanting to preserve her honour, he pretends to have lost his way and asks directions to the next town. 

Disappointed by what has happened, Franta returns to Czechoslovakia and finds his old girlfriend has married the neighbourhood jobsworth, has given birth to a child, and has taken over Barcha, his dog. All Franta can do is endure the situation as stoically as he can. Arrested and thrown in prison, he only has his memories of his friendship with Karel to sustain him.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Ondřej Vetchý ||  František Sláma  
|-
| Kryštof Hádek ||  Karel Vojtíšek 
|-
| Tara FitzGerald ||  Susan 
|-
| Charles Dance ||  Wing Commander Bentley 
|-
| Oldřich Kaiser ||  Machatý 
|-
| David Novotný ||  Bedřich Mrtvý 
|-
| Linda Rybová ||  Hanička 
|-
| Jaromír Dulava ||  Kaňka 
|-
| Lukáš Kantor ||  "Tom Tom" 
|-
| Radim Fiala ||  Sysel 
|-
| Juraj Bernáth ||  Gregora 
|-
| Miroslav Táborský ||  Houf 
|-
| Hans-Jörg Assmann ||  Dr. Blaschke 
|-
| Thure Riefenstein ||  Oberleutnant Hesse 
|-
| Anna Massey ||  English Teacher 
|-
| Čestmír Řanda Jr. ||  Pavlata
|}
==Production==
 
Principal photography for the film involved a large number of locations: Hradcany Airport, Czech Republic, Dover, England, Germany and South Africa. 
 Battle of Memphis Belle were also incorporated. Director Jan Svěrák played a number of roles, including practically all the crew members of an Allied North American B-25 Mitchell bomber in the scene where a damaged bomber is escorted.

==Reaction==
Dark Blue World opened in both the U.S. and Europe at major international film festivals in London and Toronto, to generally positive reviews, making it one of the most popular aviation war films made. 
Rex Reed described the film in The New York Observer as an "epic that blends action, romance and tragedy. Brilliantly directed and sublimely acted."  

However, other reviewers were not as enthused. Leonard Maltin commented that the love triangle provided a "more novel and interesting" aspect but the "surprisingly elaborate" flying scenes helped make the film less of a "capable but uninspired yarn", not much different from other World War II features.  Peter Bradshaws review in The Guardian echoed a similar view, "A by-the-numbers WW2 romantic tale of two Czech pilots in love with the same British woman, which plays like a mixture of Pearl Harbor and Two Little Boys by Rolf Harris."    
 

===Box office=== America on December 28, 2001, and grossed $258,771.   The film grossed $2,300,000 worldwide. 

==Awards and honours==
Dark Blue World was a major winner at the 2002 Czech Lion Awards with Box Office Award, Critics Award
Jan Sverák for Best Director, Vladimír Smutný for Best Cinematography, Ondrej Soukup for Best Music and Alois Fisárek for Best Editing. The film was also nominated for Best Film, Ondrej Vetchý for Best Actor, Krystof Hádek for Best Supporting Actor, Linda Rybová for Best Supporting Actress, Zbynek Mikulik for Best Sound, Vera Mirová for Best Costumes and Jan Vlasák for Best Art Direction. 

Dark Blue World also won the 2001 National Board Review award for Best Foreign Film and the 2002 Love is Folly International Film Festival (Bulgaria), Golden Aphrodite Award (Best Film) for Jan Sverák. Ondrej Vetchý was also nominated for the Audience Award (Best Actor) in the 2001 European Film Awards.  

==See also==
*František Fajtl
*Nebeští jezdci aka Sky Riders
==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
 

==External links==
*  
*  
*  
*   

 

 
 
 
 
 
 
 
 
 
 
 
 
 