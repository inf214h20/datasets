Sweet Alibis
{{Infobox film
| name      = Sweet Alibis
| image     = Sweet Alibis poster.jpg
| caption   = 
| director  = Lien Yi-chi
| producer  =  
| story     = 
| screenplay= 
| starring  = Alec Su Ariel Lin Matt Wu Lei Hong Lang Tsu-yun Ken Lin Chu Chih-ying Austin Lin Kao Meng-chieh
| music     = 
| cinematography = Randy Che
| editing   = 
| released  =  
| runtime   = 112 minutes
| country   = Taiwan Mandarin Taiwanese Taiwanese
| budget    = 
| gross     = 
| film name =  
}}
Sweet Alibis is a 2014 Taiwanese comedy film starring Alec Su as a coward veteran cop and Ariel Lin as an overzealous rookie who team up to solve crimes in Kaohsiung.

==Plot==
Starting from a seemingly pointless case of a puppys accidental death, from eating chocolate, the essentially incompatible pair unexpectedly digs up the clues to a series of mysterious deaths.

==Cast==
* Alec Su 
* Ariel Lin
* Matt Wu
* Lei Hong
* Lang Tsu-yun
* Ken Lin
* Chu Chih-ying
* Austin Lin
* Kao Meng-chieh
* Ma Nien-hsien
* Bebe Du
* Tao Chuan-cheng
* Lin Chia-ling
* Lee Kuo-hung
* Ruby Lin - Cameo

==Theme song==
* "Lao Tian You Yan" (老天有眼) performed by Alec Su (originally sang by Hei-pao in the 1980s)

==Reception==
It was the number-one film in Taiwan box office for a week in January 2014,  but overall grossing was disappointing with only   (2012) — in first-week grossing.  It grossed ¥36.6 million (roughly $6 million) in mainland China. 

==Awards==
*2014 51st Golden Horse Film Festival and Awards
**Nominated — Lang Tzu-yun, Best Supporting Actress

*2014 Osaka Asian Film Festival
**Nominated — Grand Prix

*2014 6th Straits Film and Television Awards
**Won — Favourite Taiwanese film in mainland China

==Trivia==
There are in-jokes in the film referencing characters from the TV series In Time with You (2011) starring Ariel Lin and Romance in the Rain (2001) starring Alec Su and Ruby Lin.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 