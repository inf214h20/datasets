Idle Roomers (1944 film)
 Idle Roomers}}
{{Infobox Film |
  | name           = Idle Roomers |
  | image          = Idleroomers44lobby.jpg|
  | caption        = |
  | director       = Del Lord
  | writer         = Del Lord Elwood Ullman |
  | starring       = Moe Howard Larry Fine Curly Howard Christine McIntyre Vernon Dent Duke York Eddie Laughton |
  | cinematography = Glen Gano | 
  | editing        = Henry Batista |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =  |
  | runtime        = 16 49"   |
  | country        = United States
  | language       = English
}}

Idle Roomers is the 80th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
 ) gives the Stooges a scare in Idle Roomers. York died only 6 days after Curly, January 24, 1952.]]
The Stooges are bellhops at Hotel Snazzy Plaza, and pound each other in order to get some face time with an attractive woman (the lovely Christine McIntyre, in her debut appearance with the team). Unfortunately, she has an evil mean-tempered husband (Vernon Dent) who happens to excel in knife throwing. The husband is also secretly importing Lupe the Wolf Man (Duke York) who goes berserk when he hears music. Later on, when Curly is cleaning their room, he snaps on the radio, and the wolf man goes on the rampage. The stooges head for the elevator back to the lobby which contains the Wolf Man inside who is playing with the elevator switch which cause to crash through the roof and sends the trio and the Wolf Man high into the sky.

==Curly Howard fades==
Curly Howards voice begins to deepen with this film. Since his 1940 divorce from Elaine Ackerman, Curly had lived a wild life, making merry on a regular basis, and drinking until the wee hours of the morning. Columbia cinematographer Henry Freulich stated in a 1984 interview that it was not unusual to see Curly stumbling into work looking like "he had himself a heluva time!"  By 1944, the effects of Curlys lifestyle began to catch up with him. Idle Roomers marks the first time his acting seems a little slower. The deeper voice confirms this assessment.   

==Production notes==
The title Idle Roomers is a pun on "idle rumors."  The plot device of bellhops pursuing the affections of an attractive female hotel guest would be used in the 1953 Woody Woodpecker cartoon Belle Boys. 
 Shemp era.
  of the trio (housed at the   in Spring House, Pennsylvania) depict a publicity photo from Idle Roomers]]

==References==
 

==External links==
<!-- *  .
*  . -->
*  
*  
*  

 

 
 
 
 
 
 
 