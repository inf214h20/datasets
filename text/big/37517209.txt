Garbo (film)
{{Infobox film
| name           = Garbo
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ron Cobb
| producer       = Neill Gladwin Steve Kearney Margot McDonald Hugh Rule Patrick Cook  Neill Gladwin Steve Kearney
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Max Cullen Neill Gladwin Steve Kearney
| music          = Allan Zavod
| cinematography = Geoff Burton
| editing        = Neil Thumpston
| studio         = Eclectic Films
| distributor    = Hoyts Beyond Films
| released       =  
| runtime        = 100 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = A$32,227 (Australia) 
}}
 1992 Cinema Australian film garbagemen (garbos Australian slang) who admire Greta Garbo.

The Celtic punk band The Pogues recorded a song, "In and Out," for the films soundtrack.  Kate Ceberano and Yothu Yindi also contributed music for the film. 

==Cast==
*Steve Kearney as Steve
*Neill Gladwyn as Neill
*Max Cullen as Wal
*Simon Chilvers as Detective Gerard Kennedy as Trevor
*Moya OSullivan as Freda
*Imogen Annesley as Jane

==See also==
*Los Trios Ringbarkus

==References==
 

==External links==
* 
* 
*  at The New York Times
*  at Screen Australia

 
 
 
 


 
 