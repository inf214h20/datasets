You're Not Built That Way
{{Infobox Hollywood cartoon|
| cartoon_name = Youre Not Built That Way
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist =
| animator = Hicks Lokey Myron Waldman
| voice_actor = Mae Questel
| musician = Sammy Timberg (uncredited)
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = July 17, 1936
| color_process = CINECOLOR but almost all prints were released on black and white film stock,color prints are really rare but  some exists on 16 and 35mm.
| runtime = 6 mins
| movie_language = English
}}
Youre Not Built That Way is a 1936 Fleischer Studios animated short film starring Betty Boop and featuring Pudgy the Puppy.

==Plot summary==
Pudgy the Puppy, out for a stroll, meets a mean bulldog. Impressed by the tough dog, Pudgy imitates his behavior. Betty, seeing this, sings the title song to Pudgy in an attempt to stop this. Pudgy ignores her and follows the bulldog, but his attempts to be tough only land him in trouble. After an attempt to steal a meal from the butcher nearly gets him skewered, Pudgy runs back to Betty, who welcomes him home.

==Production notes==
This short makes use of Fleischers multiplane camera to film the backgrounds.

==External links==
*  
 

 
 
 
 
 


 