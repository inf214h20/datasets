O21 (film)
{{Infobox film
| name = O21
| image          = File:Operation 021.jpg
| caption        = Theatrical release poster Jami  Summer Nicks  (co-directed)
| producer       = Zeba Bakhtiar  Azaan Sami Khan
| writer         = {{Plainlist |
* Jami
* Summer Nicks
* Azaan Sami Khan
* Joe Towne
}}
| starring       = {{Plainlist |
* Shaan Shahid
* Shamoon Abbasi
* Aamina Sheikh
* Ayub Khoso
* Gohar Rasheed
}}
| music          = Alfonso González Aguilar
| cinematography = Mo Azmi
| editing        = Rizwan A.Q / Team 021
| studio         = One Motion Pictures  Azad Film Company  Interactive Studios
| distributor    = Distribution Club
| released       =  
| runtime        = 125 minutes
| country        = Pakistan
| language       = Urdu 
| budget         =  
| gross          =   
}} Pakistani spy spy thriller film,     directed by Jami (director)|Jami, co-directed by Summer Nicks  and produced by Zeba Bakhtiar and her son Azaan Sami Khan.    It is the first spy action thriller movie from Pakistan.   
 New York International Film Festival, the renowned actor Shaan Shahid who has received accolades through countless Pakistani blockbusters, most recently Waar. O21 is the 576th feature film of Shaans career. 

== Plot outline == Shaan Shahid) to save the two neighboring countries from further turmoil through a plan that could risk the lives of them and their families. A plan that has 21 hours to be executed. 

== Cast ==
* Shaan Shahid   as Kashif Siddiqui
* Ayub Khoso  as Abdullah
* Aamina Sheikh as Natasha
* Shamoon Abbasi as Danish
* Ayaz Samoo as Mani Abbas
* Mustafa Changazi as Shayan
* Hameed Sheikh
* Tatmain ul Qulb
* Gohar Rasheed
* Bilal Ashraf
* Abdullah Ghaznavi
* Joe Towne as Nathan
* James Hallett as Stan
* Wendy Haines
* Summer Nicks
* Daniyal Raheel

== Production ==
The filming began in Karachi in the first quarter of 2013 and over a course of 61 days of shooting spread over 3 spells was completed in the first quarter of 2014. Filming took place  throughout Karachi, Lahore, Balochistan and the Afghan Border. O21 was primarily filmed on Red Epic Cameras with Cooke s4/i lenses however the ARRI Alexa XR was used in certain scenes as well.
It is also the first Pakistani film to be mixed in Dolby Atmos. 

==Music==
{{Infobox album
| Name        = O21
| Type        = Soundtrack
| Artist      = 
| Cover       = O21 music album cover.jpg
| Cover Size  =
| Frame       =
| alt         =
| Released    =  
| Recorded    = 2014
| Genre       = Film score
| Length      =  
| Label       = One Motion Pictures
| Producer    = Alfonso González Aguilar
| This album  = O21 (2014)
}}
The music album of film was released on 6 October 2014 while films OST The Kidnapping was released in September. The film score contains 3 tracks which are composed and conducted by Spain born music producer Alfonso González Aguilar. 

===Track listing===

{{track listing 
| music_credits = yes total_length =  
 title1  = Kill Them All music1  = Alfonso Gonzalez Aguilar length1 = 2:06
 title2  = Final Move  music2  = Alfonso Gonzalez Aguilar length2 = 6:07
 title3  = The Kidnapping  music3  = Alfonso Gonzalez Aguilar length3 = 10:43

}}

== Reception ==

=== Release ===
The first look teaser was released on May 24, 2014.  while the theatrical trailer was unveiled on September 21, 2014 in a press event held in Nueplex Cinemas Karachi. Film is scheduled to release on Eid Al-Adha 2014     clearing out all rumors of releasing on Independence day.   Rumors of films delay in release were also made but Azaan Sami Khan dispelled those rumors on official Facebook page.  It is the first Pakistani film to be released in 22 countries simultaneously including the United States, United Arab Emirates and United Kingdom.

=== Critical reception ===
Hala Syed of DAWN.com rated film 4/5 and wrote "It does not glorify war but depicts it in a gritty realistic, thought provoking way. Its engaging, unflinching and unique in that it dares to tell the dark and intense stories in the shadows." 

Aayan Mirza of Galaxy Lollywood rated 4 out of 5 and wrote "O21 is the most intelligent Pakistani cinema has ever been. It won’t spoon feed you everything, you have to make deliberate efforts to remain attentive, conscious and seriously into the film. You can’t miss to watch even an inch of the screen. O21 is absolutely not a propaganda film, but instead presents an intelligent case. It might compel you enough to Google certain things when you get back home. It is a masterpiece that can easily be put right there with any spy-thriller in the world." 

Zia Shahid of Sabz Irtiqa rated it 8 out of 10 and wrote "Jami’s direction can be explained in just one word “Stunning”, people who knows Jami for his previous work and have seen his music video’s knew that O21 won’t be a straight forward film, Jami have certainly made the audience to put their thinking cap on while watching the movie or else they won’t understand the film. On the whole O21 is dark and mysterious and it has taken the Pakistani cinema to the new level, thank you Jami for proving that Pakistani cinema is not just about item numbers and Bollywood stereo types." 

Faria Syed of HiP wrote "The film is thrilling, visually stunning and well-acted with multiple layers of story. There are no songs to titillate, no simple solutions to facilitate those who would rather not think about what they are watching." 

Umer Ali of Skotato rated 4/5 and wrote "It is the best film ever produced in Pakistan in terms of brains and execution and it is a must watch if you are a spy thriller geek." 

Zeeshan Ahmed of The Express Tribune rated the film 3.5 out of 5 and given a verdict "It’s a dense film, one you wouldn’t want to watch again. But do watch it once to embrace the fact that even a Pakistani film can opt for a slow and steady approach towards storytelling." 

Afra Jamal of Daily Times (Pakistan)|Daily Times wrote "Operation O21 is poised to be a game changer that challenges the palate and forges its own identity from a fragmented landscape. It’s a start." 

== Box office ==
 

O21 made   on its first day of release.  At the end of Eid Week(Monday-Sunday) film collected huge   from all over Pakistan. But from next Monday onwards numbers started falling heavily which resulted in mere   over Weekdays taking Extended Week One of 11 Days to   which is 6th biggest of 2014 after Kick (2014 film), Na Maloom Afraad, Bang Bang!, Khoobsurat (2014 film) and Jai Ho (film).  After low weekdays of week one, movie dipped further in week two and collected mere   taking grand total to  . 

== Controversy == Sultanat and now O21." 

On October 13, 2014 films production team gathered at the Karachi Press Club to address the “injustice” meted out to them by cinema owners of Bambino, Capri and Dreamland for taking down O21 after 25 minutes of screening. On the situation, Muhammad Rizwan of Distribution Club said "The events at Capri, Bambino and Dreamland have not only damaged the film business but our reputation as a distribution company; and therefore not only will we be taking legal action against these cinema owners but we will also stop providing films to these cinemas." 

==See also==
* List of highest-grossing Pakistani films
* List of Pakistani films of 2014

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 