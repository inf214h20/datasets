Baabarr
 
{{Infobox film
| name           = Baabaarrr
| image          = Baabarr1.jpg
| alt            =  
| caption        = 
| director       = Ashu Trikha
| producer       = Sunil Saini Mukesh Shah
| writer         = Ikram Akhtar
| starring       = Sohum Shah Mithun Chakraborty Urvashi Sharma Mukesh Tiwari Govind Namdeo Om Puri
| music          = Anand Raj Anand
| cinematography = Suhass Gujarathi
| editing        = 
| studio         = 
| distributor    = Ridhi Sidhi Films
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =   
| gross          =      (India nett)
}}
Baabarr ( ) is a 2009 gangster Hindi film directed by Ashu Trikha. The film stars Sohum Shah, Mithun Chakraborty and Urvashi Sharma. 

==Cast==
*Sohum Shah as Baabarr Qureshi
*Mithun Chakraborty as SP Dwivedi
*Urvashi Sharma as Ziya
*Mukesh Tiwari as Nawaz
*Govind Namdeo as Bhaiyaa Jee
*Om Puri as Daroga Chaturvedi
*Tinu Anand as Mamu
*Sushant Singh as Tabrez
*Kashish as Aafreen
*Shakti Kapoor as Sarfaraaz
*Rakesh Deewana as Nanha

==Legal issues== PIL was filed in the Bombay High Court seeking a ban on Baabarr for alleged negative portrayal of Muslims. The petitioner also charged that characters in the film were based on Mulayam Singh Yadav and Mayawati. 

==Critical reception==
Baabarr is not much liked by the critics. Taran Adarsh of Bollywood Hungama gave 3.5 out of 5 stars.  The Times Of India gave 2.5 out of 5 stars & stated that "The film is not much impressive as expected, director Ashu Trikha & writer Ikram Akhtar did a well job but the thing making it unimpressive is that the director gets only the mood right, not the scenes, though the reviewer praised the performance of Om Puri by saying that it is Om Puri as the oily, wily and crooked cop, who steals the show".   Martin DSouza of Bollywood Trade News Network gave it 1 out of 5 stars & said by calling the film a repetition that "the film is of old story with new cast & crew, Martin specially criticises the decision of making film with lead role of Soham Shah" 

==Box office==
The film was not good at the box office as it hardly netted an amount of   at the end of its first week after release & getting through the cinemas across India Baabarr at last managed to gross   & determined a Flop.  

Baabarr grossed Pound sign|£2,748 on 18 Sep 2009 & Pound sign|£1,439 on 11 Sep 2009 according to Bollywood hungama. 

==Awards==
Recently the cast and crew of the film visited Asian Academy Of Film & Television and they have been honoured with the life membership of International Film And Television Club. 

==References==
 

==External links==
*  
*  

 
 
 