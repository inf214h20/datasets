Iphigenia (film)
{{Infobox film name       = Iphigenia image      = Iphigeniacover.jpg image_size = director   = Michael Cacoyannis producer   = Michael Cacoyannis writer     = Michael Cacoyannis Euripedes editing     = Takis Yanopoulos music       = Mikis Theodorakis cinematography = Giorgos Arvanitis runtime     = 127 minutes country     = Greece language    = Greek
}}
 Greek myth The Trojan Women in 1971), from his stage production of Euripides play Iphigenia at Aulis. The film stars Tatiana Papamoschou as Iphigenia, Kostas Kazakos as Agamemnon, and the legendary actress Irene Papas as Clytemnestra. The score was composed by Mikis Theodorakis.
 Best Foreign Language Film.    It was also nominated for the Palme dOr at the 1977 Cannes Film Festival.    Iphigenia received the 1978 Belgian Femina Award, and received the Best Film Award at the 1977 Thessaloniki Film Festival, where Tatiania Papamoschou also received the Best Leading Actress Award for her role as Iphigenia.

==Cast== Iphigenia
* Irene Papas as Clytemnestra
* Kostas Kazakos as Agamemnon
* Costas Carras as Menelaus
* Christos Tsagas as Odysseus
* Panos Mihalopoulos as Achilles
* Dimitri Aronis as Calchas
==Plot==
Approximately 2,500 years ago, the Athenian tragedians -- Sophocles, Aeschylus, Euripides -- said it all in the most eloquent and poetic terms. Everyone else has followed in their footsteps, from Goethe and Shakespeare to Mann and Beckett. More than ever, the ancient tragedies are relevant today, for their subject matter is the universal nature of man: his hunger for power, his greed, and his lust. The plays are the confirmation of the human tragedy for all times, and all places. The Greek myths upon which these tragedies are based combine marvelous storytelling with symbolic associations that are the foundations of the collective unconscious of our modern world.

Euripides left behind some of the most astonishing expressions of human passion and compassion ever set into dramatic form. His plays express a range of delight, hope, and terror unmatched in classical drama. Of these, certainly none display more range than the inexorable bloodletting of the stories of the House of Atreus and the Trojan War, which Euripides treats with characteristic balance in several of his tragedies.

For the cinema, Greek myths and ancient tragedies are made-to-order screenplays. Many film directors have tried to take advantage of this gold mine, with varying degrees of success. For some, they have proven to be very expensive failures, as exemplified recently by a prominent Hollywood director, Wolfgang Petersen. Other directors, reworking particular tragedies, have produced freshly reincarnated masterpieces. Such is "Iphigenia" (1977), by Greek director Mikhali (Michael) Cacoyannis, based on Euripedes tragedy, "Iphigenia in Aulis."

"Iphigenia" relates the story of an incident that took place just prior to the Trojan War. Helen, wife of Menelaus, king of Sparta, had eloped to Troy with Paris, son of King Priam. Menelaus brother, Agamemnon, King of Argos, had assembled a huge Greek expeditionary force on the shores of Aulis that he planned to lead to Troy in order to reclaim his brothers wife. The Goddess Artemis, taking revenge for an insult done to her by Agamemnons father, King Atreus, created a meteorological problem by sending storms, or calms, to prevent the Greek fleet from sailing to Troy. This is where the film begins.

The Greek armies have waited for what seems an eternity for the winds to rise, blow eastward, and carry their boats to Troy. The men are tired, bored, hungry, and anxious to go into battle. In a public relations gesture intended to placate the men, Agamemnon (Costa Kazakos) directs them to go and help themselves to a flock of sheep that belong to the nearby temple dedicated to Artemis. In the ensuing mayhem, Artemis sacred deer is accidentally slain. Calchas (Dimitris Aronis), high priest of Artemis temple, is incensed by the sacrilege. He delivers an oracle to Agamemnon, with Menelaus (Kostas Karras) and Odysseus (Christos Tsagas) also present. The oracle, according to Calchas emanating from Artemis herself, demands that Agamemnon offers a sacrifice to atone for the defiling of the holy ground and the killing of the sacred stag. Once the sacrifice is made, Artemis will consent for the armies to sail to Troy by allowing the winds to blow eastward. The sacrifice is to be Agamemnons first-born daughter, Iphigenia (Tatiana Papamoschou). The news of "the deal" soon spreads through the armies ranks, although the nature of the sacrifice remains temporarily unknown to them.

After considerable argument and recrimination between the two brothers, Agamemnon sends a message to his wife Clytemnestra (Irene Papas), in Argos. In his letter, Agamemnon is asking his wife to send their daughter Iphigenia, to Aulis, ostensibly to wed Achilles (Panos Mihalopoulos). Achilles, leader of the Mymirdon army, is a member of Agamemnons expeditionary forces. Against her husbands instructions, Clytemnestra decides to accompany her daughter to Aulis.

From this point forward to the climax, the tempo and the development of the tragedy stretches tighter. Agamemnon has second thoughts about his plan. After confessing his ruse to his old servant (Angelos Yannoulis), Agamemnon dispatches him with another letter to Clytemnestra that reveals the truth and tells her to cancel Iphigenias trip. The old man is intercepted on the road by Menelaus men and returned to Aulis. In the ensuing confrontation, Menelaus rebukes his brother for betraying the honor of Greece for his personal benefit. Agamemnon argues persuasively, and convinces Menelaus that no war is worth the life of a child. Following their understanding, Agamemnon decides to personally carry the letter to Clytemnestra. Too late. A messenger announces the imminent arrival of the wedding party, which includes Clytemnestra. Agamemnon is stunned by the announcement, and he resigns himself to the worst: "From now on fate rules. Not I."

Clytemnestra arrives at Aulis filled with happiness over her daughters prospective wedding the famous Myrmidon leader, Achilles. Iphigenias first meeting with her father is couched in double entendre which is devastating: as she talks about her upcoming wedding, he talks about her upcoming sacrifice. They use the same words, but the meanings could not be more horribly apart. When Agamemnon meets with Clytemnestra, he still vainly tries to convince her to return to Argos without witnessing the "wedding." Clytemnestra and Achilles soon learn the truth from Agamemnons old servant. Achilles is overcome with shame and rage when he learns of the deceit that has involved him in this tragedy. Clytemnestra rises into a fury, and in desperation, confronts her husband one last time. Agamemnon, however, is trapped in his own web and cannot now back down, as Odysseus has threatened to inform the army of the exact nature of the sacrifice if Agamemnon does not follow through on the oracles demand.

Meanwhile, preparations for the sacrifice are proceeding. "Lets not delay, the wind is rising," says Calchas. Odysseus finally forces the situation when he tells the army who is to be the sacrificial victim. Now, there is no turning back. Iphigenia briefly escapes, but she is soon recaptured by Odysseus soldiers. In a poignant scene, suggestive of the scene of the slowly dying sacred stag at the beginning of the film, Iphigenia is caught lying down, panting and out of breath, "dying," on the forest floor. Her captors return her to the camp to face her executioners. Now resigned to her fate, she has a last, heartrending meeting with her father, before walking up the hill toward her final destiny. While Agamemnon, surrounded by his cheering army, watches helplessly on the steps below, Iphigenia reaches the top and is quickly grabbed by Calchas. At that same moment, upon seeing the wind rising. Agamemnon runs up the steps and as he reaches the top of hill, his face reflects what we assume is the sight of the dead Iphigenia. However, Cacoyannis chooses to leave the ending somewhat ambiguous: is Iphigenia supernaturally whisked away, as in Euripides play, or is she sacrificed, in agreement with the tradition? Not having Agamemnons eyes, well never know for sure.

A strong wind now blows. The men run to the beach, push their ships into the sea, and sail toward Troy and its promised treasures.

==Divergences from the original play== Greek tragic chorus, originally employed to explain key scenes, and replaces it in some cases with a chorus of Greek soldiers. He also adds new characters who were not present, but who were mentioned, in the original play, Odysseus and Calchas, to further the plot and voice certain themes.

As in Euripides original work, Cacoyannis deliberately renders the end of the story ambiguous. Though Greek myth states that Iphigenia was miraculously saved by the gods at the very instant of her death, this event is not directly depicted in either the play or the film, leaving Iphigenias true fate in question. In Euripides Iphigenia at Aulis, Iphigenias rescue is described second-hand by a messenger. In Iphigenia, there is no overt reference at all to this event; the audience sees only the knife fall, followed by a shot of Agamemnons shocked expression.

==See also==
* Iphigeneia
* Iphigeneia at Aulis
* Iphigeneia in Tauris
* List of historical drama films
* Greek mythology in popular culture
* List of submissions to the 50th Academy Awards for Best Foreign Language Film
* List of Greek submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 