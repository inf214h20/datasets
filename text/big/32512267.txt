Die Entlassung
{{Infobox film
| name           = Die Entlassung
| image          =
| image_size     =
| caption        =
| director       = Wolfgang Liebeneiner
| producer       = Emil Jannings (producer) Fritz Klotsch (line producer) Walter Lehmann (executive producer)
| writer         = Alexander Lernet-Holenia (story) Curt J. Braun Felix von Eckardt
| narrator       =
| starring       = See below
| music          = Herbert Windt
| cinematography = Fritz Arno Wagner
| editing        = Martha Dübber
| studio         =
| distributor    =
| released       = 1942
| runtime        = 110 minutes 100 minutes (West Germany cut version)
| country        = Nazi Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Reich Propaganda Ministry Censorship Office. 

The success of the 1940 film Bismarck (film)|Bismarck led to this film as a sequel. 

The film is also known as Bismarcks Dismissal in the United Kingdom, Schicksalswende (West German rerun title) and Wilhelm II. und Bismarck (new West German title).

== Plot summary ==
 
The film shows Bismarck being dismissed by Wilhelm II of Germany and the dilettantes who surround him.   An unscrupulous schemer plays on the kings desire to lead and so persuades him to the dismissal.   This results in a disastrous two-front war by destroying Bismarcks treaty with Russia and leaving him to lament with the question of who would complete his work. Robert Edwin Hertzstein, The War That Hitler Won p305-6 ISBN 0-399-11845-4 

== Cast ==
*Emil Jannings as Fürst Bismarck
*Margarete Schön as Fürstin Johanna Bismarck
*Christian Kayßler as Graf Herbert Bismarck
*Theodor Loos as Kaiser Wilhelm I
*Karl Ludwig Diehl as Kaiser Friederich III.
*Hildegard Grethe as Kaiserin Friedrich
*Werner Hinz as Kaiser Wilhelm II
*Werner Krauss as Geheimrat von Holstein
*Otto Graf as Graf Eulenburg Paul Hoffmann as Graf Waldersee
*Paul Bildt as Minister von Bötticher
*Walther Süssenguth as Zar Alexander
*Franz Schafheitlin as Botschafter Graf Schuwalow
*Herbert Hübner as Generaladjutant von Hahnke
*Rudolf Blümner as Chef des Zivilkabinetts von Lucanus
*Fritz Kampers as Professor Dr. Schwenninger
*Emil Heß as Grossfürst Wladimir
*Heinrich Schroth as General von Caprivi

== Soundtrack ==
 

== Release and reception ==
The war with Russia delayed its release, and it was not exported, owing to the obvious parallels. Robert Edwin Hertzstein, The War That Hitler Won p306 ISBN 0-399-11845-4 

==Motifs==

Much emphasis was laid on Bismarcks notion of Greater Germany.   His failure was depicted as leading to Versailles. 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 