A Certain Mister
{{Infobox film
| name           = Un certain monsieur
| image          = 
| alt            =  
| caption        = 
| director       = Yves Ciampi
| producer       = Éclectiques Films
| writer         = Yannick Boysivon
| starring       = Louis de Funès
| music          = Georges Van Parys
| cinematography = 
| editing        = 
| studio         = 
| distributor    = U.F.P.C.
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 French Crime crime film from 1950, directed by Yves Ciampi, written by Yannick Boysivon, and starring Louis de Funès. The script is based on Jean Le Halliers novel Prix du Quai des Orfèvres (1947). 

== Cast ==
* Louis de Funès: Thomas Boudeboeuf, journalist of "lavenir Sauveterrois"
* Hélène Perdrière: LIndex, member of the band
* René Dary: Le Pouce, member of the band
* Pierre Destailles: Le Majeur, member of the band
* Louis Seigner: Commissioner Clergé
* Marc Cassot: inspector César alias André Paris 
* Junie Astor: Edmée Lamour
* Lise Delamare: Mrs. Lecorduvent
* Alice Field: Mrs. Léonard

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 