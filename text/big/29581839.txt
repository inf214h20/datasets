Kiles
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Passion
| image          = Kiles (movie poster).jpg
| alt            =  
| caption        = The promotional poster
| director       = Reath Narith
| producer       = Sin Chan Saya
| writer         = Mao Ayom
| starring       = Sara Chantra Sarin Bophal Hong Tin
| music          = 
| cinematography = Prom Measa
| editing        = 
| studio         = Motion Studios
| distributor    = Kambuja Pictures
| released       =  
| runtime        = 90 minutes
| country        = Cambodia
| language       = Khmer French
| budget         = 
| gross          = 
}}
 Cambodian Drama film written by local award-winning director, Mao Ayom. With the French colonization period setting during Post-World War II Cambodia, it tells the story of a passion sexual desire of an old rich man named Kiles who deeply fell in lust and love toward his debt slaves young daughter named Teay.

The film is known to be the first production by the Cambodia Film Commission, a government-funded initiative meant to raise the standard of Cambodian film that nowaday face a decline in quality and quantity.

==Synopsis==
The sounds of chanting plays over the opening scene as an old rich man named Kiles is lying in bed, lonely and weak. It doesn’t look like he has long to live, but, as luck would have it, the frail fellow recovers and returns to live with his four wives and countless servants.
The news keeps getting better for the geezer when one of these servants reads his palm and tells him that his fifth wife will be a beautiful young woman. Kiles isn’t a man to wait around for fortune to find him, so he tells his future-seeing househelp to track down someone who owes him money and demand that they give him their daughter’s hand in marriage to clear their debt.The unfortunate and indebted man who the servant finds conveniently has a beautiful daughter named Teuy. Her devoted boyfriend Plok is a cremator, and he has been taking the golden coins from the mouths of corpses to save for their wedding. When the servant comes knocking he has 99 of the 100 coins needed to get engaged.
Close doesn’t cut it for Teuy’s father, who predictably agrees to marry her off to erase his debts.
It’s not long before old man Kiles and young beauty Teuy are preparing to be married.

== External links ==
* 

 
 
 
 

 