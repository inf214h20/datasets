Piranha 3DD
{{Infobox film
| name           = Piranha 3DD
| image          = Piranha-3dd-poster-2.jpg
| image_size     = 250px
| alt            = 
| caption        = Theatrical poster
| director       = John Gulager
| producer       = Mark Canton Marc Toberoff Joel Soisson
| screenplay     = Patrick Melton Marcus Dunstan Joel Soisson
| based on       =   Matt Bush David Koechner Chris Zylka Katrina Bowden Gary Busey Christopher Lloyd David Hasselhoff
| music          = Elia Cmiral
| cinematography = Alexandre Lehmann
| editing        = Devin C. Lussier Martin Bernfeld Kirk Morri
| studio         = The Weinstein Company|Radius-TWC Mark Canton/IPW Neo Art & Logic
| distributor    = Dimension Films
| released       =  
| runtime        = 83 minutes  
| country        = United States
| language       = English
| budget         = $5 million   
| gross          = $8.4 million   
}} 3D comedy Matt Bush, David Koechner, Chris Zylka, Katrina Bowden, Gary Busey, Christopher Lloyd, and David Hasselhoff. Production began on April 27, 2011 with a release scheduled for November 23, 2011, but a month prior to release this date was revised to an unspecified 2012 date. The film was eventually released in the UK on May 11, 2012 and in the U.S. on June 1, 2012.

==Plot==
A year after the attack on Lake Victoria by prehistoric piranhas, an eradication campaign has left the lake uninhabitable, and the town itself has been largely abandoned as a result of the drying-up of the Lake. Meanwhile at Cross Lake two farmers search the waters to recover the body of a dead cow. Piranha eggs that have been laid inside the cow hatch, and the farmers are killed by the swarm, but before they both die, one farmer pops out of the water, takes a piranha, bites it, and then spits it up before sinking back into the water.
 waterpark she co-owns. She finds to her horror that the other co-owner, her step-father Chet, plans to add an adult-themed section to the waterpark with "water-certified strippers", and re-open it as "Big Wet". At a party at the waterpark that night, Maddy encounters several old acquaintances, including her policeman ex-boyfriend Kyle Knight,   (who she later has sex with) and Barry who has secretly had a crush on her since grade school. She also runs into two of her close friends, Ashley and Shelby. Shelby and her boyfriend Josh go skinny-dipping in the lake, where a piranha makes its way inside her vagina. Meanwhile, Ashley and her boyfriend Travis begin to have sex in their van, but Ashley accidentally trips the handbrake with her foot, causing the van to roll into the lake, where Travis is devoured while Ashley is on the roof of the van, calling for help but no one can hear her, and is presumably killed by the piranhas.

The next day, Maddy is consoling Shelby about their missing friends. While sitting on a jetty, they are both attacked by the swarm of piranhas. They manage to kill one, and Maddy, Kyle and Barry bring it to Carl Goodman to examine. He informs them that the piranhas may be moving via sewage pipes and underground rivers between lakes, attracted by chemicals involved in swimming pool cleansers that match their spawning routes.  However, the wider world wouldnt listen to him, based upon his theory that the fish would evolve to become terrain-viable. The trio return to the lake, where they establish that the piranhas cannot make their way into the outflow pipes connecting the lake and the waterpark. While Shelby and Josh are having sex, the piranha in Shelbys vagina bites Joshs penis, forcing him to chop the organ off with a knife. Both are hospitalized, but survive. Kyle is revealed to be corrupt and taking pay-offs from Chet, who is secretly pumping water from an underground river into the waterpark. Chet orders Kyle to keep Maddy from finding out the secret.

"Big Wet" opens the next day. Among the first guests are Deputy Fallon, who survived his previous ordeal with the piranhas but lost his legs, and former cameraman Andrew Cunningham (Paul Scheer). While the duo attempts to overcome their fear of the water after they were attacked a year ago, David Hasselhoff also makes an appearance as a celebrity lifeguard.

Discovering the connection between the park and the underground river, Maddy attempts to shut the waterpark down, but is stopped by Chet and Kyle. The piranhas make their way to the area and attack, killing many of the lifeguards and waterpark-goers. Fallon attaches a shotgun prosthesis to his legs in order to save the visitors, while Hasselhoff, after rescuing a small boy named David, becomes pleased that he has finally become a real lifeguard. In the chaos, Chet refuses to help any survivors, including Kiki (Irina Voronina), who is eaten by the fish, and a young girl whose mother has also been eaten. He soon kills the young girl with his golf cart by accident. Chet is then decapitated by a hanging cable when he is about to drive away and escape.

Maddy instructs Barry to begin draining the pools; however Maddy, who is rescuing people from the water, becomes caught in the suction and dragged down to the bottom of the pool. After Kyle refuses to save her because of his shock of piranhas, Barry, despite being unable to swim, leaps down and brings her to the surface, whereupon Maddy is revived. She and Barry then kiss.

Another employee, Big Dave, pours pure chlorine into the pipes, followed by a lit Joint (cannabis)|joint. The resulting explosion kills most of the piranhas, while Kyle is killed by a falling trident. The celebrations are cut short however, when Maddy takes a phone call from a horrified Mr. Goodman, who informs them that the piranhas are evolving and are now able to move on land after seeing that the one he had in the tank had escaped, to which Maddy informs him that she knows. The film ends as one such piranha emerges from the pool and decapitates David (the small child which Hasselhoff saved earlier), to which Hasselhoff comments, "Little ginger moron" while the survived visitors pick up their phones and take a picture of the dead body of David.

In a post-credits scene, Hasselhoff is running on a beach holding a trident, which is an advertisement for a film titled Fishhunter.

==Cast==
* Danielle Panabaker as Maddy Matt Bush as Barry
* David Koechner as Chet
* Chris Zylka as Kyle
* Katrina Bowden as Shelby
* Gary Busey as Clayton
* Christopher Lloyd as Carl Goodman
* David Hasselhoff as Himself
* Adrian Martinez as Big Dave
* Paul Scheer as Andrew Cunningham
* Clu Gulager as Mo
* Jean-Luc Bilodeau as Josh
* Meagan Tandy as Ashley
* Paul James Jordan as Travis
* Sierra Fisk as Bethany
* Matthew Lintz as David (Freckled Boy)
* Sylvia Jefferies as Young Mother
* Irina Voronina as Kiki
* Ving Rhames  as Deputy Fallon

==Production==
In October 2010, Dimension Films announced that they had secured John Gulager to direct the film based on a script by Saw 3D scribes Patrick Melton and Marcus Dunstan. Filming was intended to take place between January 17 and February 18 in Baton Rouge, Louisiana with a release date set for August 2011,  but this became impractical because of cold weather and the requirement for most of the cast to be wearing little or no clothing.  In March 2011, production on the film was delayed and Joel Soisson was brought in to produce the film and rewrite the Dunstan-Melton script. 

Principal photography began in Wilmington, North Carolina on April 25, 2011,   with parts of filming occurring at Jungle Rapids water park and Shaw-Speaks community center. Soisson claimed that in choosing a shooting location he was looking for an "iconic America town" that "could be anywhere". Soisson also indicated that tax rebates and the variety of geography in North Carolina had convinced them to choose the location over the alternative of Louisiana. 

Filming was completed on May 27, 2011 after 33 days  with three weeks of filming occurring at the Jungle Rapids water park.  The film was shot using 3D rigs, as opposed to converting the film to 3D in post-production.  Piranha 3DD is cinematographer Alexandre Lehmanns first 3D film.  Devin C. Lussier and Martin Bernfeld were hired to edit the film. 

==Release== VOD services.  On March 14, it was announced that the film would be released on June 1, 2012 in the United States.  The movie was released on May 11, 2012 in the UK.  In the United States, it received only a limited release, being shown in only 75 theaters. 

===Box office===
The film made very small impact on its debut weekend of release in the UK; its box receipts for the opening weekend were just £242,889, placing it at number 8. Lower-budget films that were not new releases such as Salmon Fishing in the Yemen reached #7, one place above Piranha 3DD.  By the end of its limited run, it had garnered a total UK box office of $688,269. 

The film did not gross well in North America either; it grossed a mere $376,512 in its 3-week run.  In total, Piranha 3DD did significantly better internationally, accumulating 95% of its $8,493,728 gross overseas and narrowly scraping a profit.

===Home media===
The film was released on September 4, 2012 in a 3D Blu-ray Disc|Blu-ray "combo pack", Blu-ray, and DVD with all three editions containing digital copies in the US. In the UK it was released on September 3, 2012 on DVD and Blu-ray 3D (with a 2D Blu-ray version included). The film was distributed earlier to rent via Redbox. The DVD is known as Piranha DD.

==Reception==
Piranha 3DD has received mostly negative reviews and currently holds a 13% approval rating and average rating of 3.2/10 on  , which is "generally unfavorable" on their rating system. 
 original film. 

The film is nominated for two  .

==References==
{{Reflist|30em|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   
   

<!--
   
-->
}}

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 