Leviathan (2014 film)
 
{{Infobox film
| name = Leviathan
| image = Leviathan 2014 poster.jpg
| alt = 
| caption = Film poster
| director = Andrey Zvyagintsev
| producer = Alexander Rodnyansky
| writer = Andrey Zvyagintsev Oleg Negin Aleksei Serebryakov Elena Lyadova Vladimir Vdovichenkov Roman Madyanov
| cinematography = Mikhail Krichman
| editing = 
| studio = 
| distributor = 
| released =  
| runtime = 141 minutes  
| country = Russia
| language = Russian
| budget = RUB 220 Mn.
| gross = $1,092,800 
}} Aleksei Serebryakov, Elena Lyadova, and Vladimir Vdovichenkov. According to Zvyagintsev, the story of Marvin Heemeyer in the United States inspired him and it was adapted into a Russian setting,  but critics compare the story to the more similar biblical story of Naboths Vineyard, where a King vies for his subjects land and is motivated by his Queen to obtain it in a sly manner. The character development of the protagonist parallels another biblical figure, Book of Job|Job.  The producer Alexander Rodnyansky has said: "It deals with some of the most important social issues of contemporary Russia while never becoming an artists sermon or a public statement; it is a story of love and tragedy experienced by ordinary people".    Critics noted the film as being formidable,     dealing with quirks of fate, power and money. 
 Best Foreign Film.

==Plot summary==
 
The film is set in the coastal town of Teriberka,  Murmansk, Russia. The story revolves around a hotheaded out-of-work simpleton named Kolya (Alexei Serebriakov) and the events in his life. The films narrative is focused on the dark and icy aspects of human nature and the modern fissures found in social contracts,  particularly the ones found in the abuses of modern law. The film tries to unmask the truth behind moral aspects of superficial friendliness, blind love and undeserved trust. Events seem to unravel tragically for Kolya when crooked mayor Vadim (Roman Madyanov) eyes Kolyas property ( ) and repossesses it via self-serving legal measures for a grossly-undervalued sum. Meanwhile, Kolyas family life is also in total disarray, complicated further by his discontented wife Lilia (Elena Lyadova) and teenage delinquent son Roma (Sergey Pokhodaev) from Kolyas first marriage. Kolya has friends of varying social standing, but they only seem to care for his talent as a skilled mechanic.

Kolya enlists the help of his old army friend Dmitri (Vladimir Vdovichenkov), now a sharp lawyer in Moscow, to fight the illegal repossession of his property. Dmitri uses compromising documents to blackmail mayor Vadim and this seems to work for a while. However, the initial success brought by Dmitris assistance fades when Lilia and Dmitri begin an extramarital affair.

After Kolya finds out about the affair, he eventually reconciles with his wife. Meanwhile mayor Vadim goes to his friend, a local Orthodox bishop, for spiritual comfort and is ambiguously encouraged to solve his problems by the force of his power as mayor. This motivates Vadim and he hires thugs to intimidate Dmitri with a mock execution and orders him to return quietly to Moscow, which he does. Vadim orders his office workers to go forward in repossessing Kolyas property. Lilia, driven by guilt by Kolyas kindness and forgiveness, commits suicide. Subsequently, Kolya is arrested and accused of murdering Lilia. Kolya breaks down when as he finds out his so-called friends testified against him; Kolya is tried and sentenced to fifteen years in a maximum-security prison by Vadims unscrupulous friends in the Justice system. Kolyas friends becomes guardians of Roma, driven by guilty for giving testimony against him. Finally, Kolyas homestead is torn down for Vadims and the Churchs projects.

The film concludes with the sermon of the local bishop, in the new church built on Kolyas land. The bishop, in a paradoxical manner, addresses the congregation to be open to the truth and to champion it, because that is true freedom and that if anyone tries to convince them with actions of destruction, hatred and violence justified as interventions with good intentions, those are lies passed off as truth. When this sermon is delivered, responses from various groups inside the church are shown in a subtly satirical manner. The bishop ends his sermon by stating the significance of upholding cardinal virtues in this ever-changing world, and he offers benedictions.

==Cast== Aleksei Serebryakov as Kolya
* Roman Madyanov as Vadim, the mayor.
* Vladimir Vdovichenkov as Dmitri, the lawyer friend.
* Elena Lyadova as Lilia
* Sergey Pokhodaev as Roma

==Production==
When Andrey Zvyagintsev produced a short film in the United States, he was told the story of Marvin Heemeyer.  He was amazed by this story and wanted initially to make his film in the US, but then changed his mind.   The screenplay was written by Zvyagintsev and Oleg Negin and is loosely adapted from the biblical stories of Job from Uz and King Ahab of Samaria and Heinrich von Kleists novella Michael Kohlhaas. The script features more than fifteen characters, which is unusually many for a film by Zvyagintsev.

Principal photography took place in towns Kirovsk, Murmansk Oblast|Kirovsk, Monchegorsk, Olenegorsk, Murmansk Oblast|Olenegorsk, near Murmansk on the Kola Peninsula. Preparations on the set began in May 2013. Principal photography took place during three months from August to October the same year.    Filming of exterior scenes for Leviathan took place in the town of Teriberka on the Barents Sea coast. 

==Release== premiered at the 2014 Cannes Film Festival, where it was screened on 23 May. It will be distributed by Sony Pictures Classics in the United States, Curzon Cinemas in the United Kingdom and by Palace Entertainment in Australia and New Zealand. 

==Critical reception==
Peter Bradshaw, writing a full five-star review for The Guardian, gave the film huge praise. Bradshaw thought that the film was "acted and directed with unflinching ambition" and described the film as "a forbidding and intimidating piece of work... a movie with real grandeur". 

On Metacritic, based on 27 reviews, Leviathan held an average score of 91 out of 100, indicating "universal acclaim". It also has a 99% rating on Rotten Tomatoes. 

===Criticism=== Ministry of Culture.    However, Vladimir Medinsky, Minister of Culture, a conservative historian, acknowledged that the film showed talented moviemaking, but said he does not like it.     He sharply criticized its portrayal of ordinary Russians as swearing, vodka-swigging people, which he does not recognize from his experience as a Russian or that of "Real Russians". He thought it as strange that there is not a single positive character in the movie and implied that the director was not fond of Russians but rather “fame, red carpets and statuettes". The Ministry of Culture has now proposed guidelines which would ban movies that "defile" the national culture.  In turn, when appearing on oppositional TV channel Dozhd, director Zvyagintsev was criticised by journalist Ksenia Sobchak for accepting government subsidies. Specifically, Sobchak asked whether government funding had had no influence on the content of the movie. In response, Zvyagintsev maintained that he had always felt completely independent from the Ministry in writing and shooting the movie. 

Vladimir Posner, a veteran Russian journalist said that the ruckus is because, “Anything seen as being critical of Russia in any way is automatically seen as either another Western attempt to denigrate Russia and the Orthodox Church or it as the work of some kind of fifth column of Russia-phobes who are paid by the West to do their anti-Russian work or are simply themselves profoundly anti-Russian.” 

Metropolitan Simon of Murmansk and Monchegorsk, the diocese where the movie was filmed, issued a statement calling it "honest". He said that Leviathan raised important questions about the state of the country. 

==Accolades== submission for the Academy Award for Best Foreign Language Film at the 87th Academy Awards.       It made the January Shortlist of nine films,    before being nominated later that month.   
 Best Foreign 45th International Film Festival of India.

Following the Golden Globe Award for Best Foreign Language film Leviathan was leaked online among some of the other Oscar 2015 nominated films. On 12 January the website "Thank you, Leviathan filmmakers" appeared on the Internet encouraging social media users to contribute any amount as a gratitude to the filmmakers.    Alexander Rodnyanskiy, Leviathans Producer, supported Slavas Smirnov (websites author; independent digital producer) initiative and asked to transfer the money to the "Podari Zhizn" ( , Give Life) charity fund which is held by actresses Chulpan Khamatova and Dina Korzun.   

{| class="wikitable"
|+ List of awards and nominations
! Award !! Category !! Recipients and nominees !! Result
|-
| rowspan="2" | 2014 Cannes Film Festival Best Screenplay Best Screenplay
| Andrey Zvyagintsev and Oleg Negin
|  
|- Palme dOr
| Andrey Zvyagintsev
|  
|-
| rowspan="2" | European Film Award
| Best Film
| Andrey Zvyagintsev
|  
|-
| Best Actor
| Aleksei Serebryakov
|  
|- International Film 45th International Film Festival of India Golden Peacock
| Andrey Zvyagintsev
|  
|- International Art Festival of Cinematography Best Cinematographer
| Mikhail Krichman
|  
|- 58th BFI London Film Festival Best Film
| Andrey Zvyagintsev and Alexander Rodnyansky
|  
|- 68th British Academy Film Awards Best Film Not in the English Language
| Andrey Zvyagintsev
|  
|- 32nd Filmfest Munich Film Festival Best Film
| Andrey Zvyagintsev and Alexander Rodnyansky
|  
|- 8th Abu Dhabi Film Festival Best Narrative  Best Actor
| Andrey Zvyagintsev, Alexey Serebryakov
|  
|- Palm Springs International Film Festival Best Foreign Language Film 
| Andrey Zvyagintsev 
|  
|- Asia Pacific Screen Awards Best Feature Film 
| Andrey Zvyagintsev 
|  
|- 72nd Golden Globe Awards Golden Globe Best Foreign Language Film
| Andrey Zvyagintsev and Alexander Rodnyansky
|  
|- 13th Golden Golden Eagle Award Best Direction Best Leading Actress Best Film Editing 
Best Supporting Actor
| Andrey Zvyagintsev, Elena Lyadova, Anna Mann and Roman Madyanov Jr.
|  
|- 87th Academy Awards List of Best Foreign Language Film
| Andrey Zvyagintsev
|  
|}

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Russian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 