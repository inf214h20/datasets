'It's Alive!'
{{Infobox film
| name           = Its Alive!
| image          = 
| alt            =  
| caption        = 
| director       = Larry Buchanan
| producer       = Larry Buchanan (producer) Edwin Tobolowsky (associate producer)
| writer         = Richard Matheson (story "Being")  (uncredited) Larry Buchanan  (uncredited)
| starring       = See below
| music          = 
| cinematography = Robert B. Alcott
| editing        = Larry Buchanan
| studio         = 
| distributor    = American International Pictures
| released       =   
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Its Alive! is a 1969 American film directed by  . 

== Plot ==
 
Norman and Leela Sterns are newlyweds who are driving from their home in New York to Los Angeles. They become lost and are stranded in rural Ozark countryside because they are running out of gasoline. They meet a friendly paleontologist named Wayne Thomas (Tommy Kirk). Wayne suggests that they visit the nearest farm that could give them some gasoline. They find that the farm is run by a strange farmer named Greely, who tells them that the gasoline truck was supposed to arrive the previous day, but since it didnt, he expects it there any minute. Greely suggests that they go inside to the parlor, where its cooler. On the way up to the house, he asks if they know anybody out here, in case they may be waiting for them. They say no, and when they get inside, Greely goes off to tell his "housekeeper", Bella to make some iced tea. She argues with him on what he will "do with them", but Greely smacks her, and threatens that she will "take their place" if she doesnt serve them some tea. 

Wayne Thomas arrives and Greely goes outside. He tells them that their car wont start. Wayne decides to take a look at the engine and tells Greely to go back to his truck and get a tool. Greely instead beats him on the back of the head with it and drags his body off. Meanwhile, Leela appears to be worried about Greely, because he is acting strange and his eyes dont look right. She then compares his eyes to a stuffed lizard across the room. Greely comes back inside and tells them that he had to do a chore. Leela wants to go back outside, but Greely tells them that they could check out his "collection" while waiting for the truck. 

He takes them out to the yard and shows them his "zoo", which includes ordinary animals like turtles, rattlesnakes, a bobcat, and some coyotes. Greely then tells them that they should have a look at his "prize", which is located deep in a mountain cave system behind his home. He puts them in a small room which he claims he had set up for tourists while he goes to turn on the rest of the power. However, it was a trap, as Greely pulls a lever and drops some bars down, blocking their way out. Greely leaves the cave, laughing as Leela discovers that Wayne Thomas is inside the cell as well, badly wounded but alive. He tells them that Greely threw him into another cavern below them, and left him there, but he found a way out and crawled in the cell, right as they arrived. 

Norman suggests that there may be a way out down there, and begins a descent into the cavern. A reluctant Wayne and Leela follow and discover a   prehistoric, aquatic dinosaur coming out of a spring, which Greely apparently feeds live victims to. Greely catches them in the enclosure and points a pistol at them, attempting to force them down in there to be eaten. Norman attacks him and tries to overpower Greely, but the pistol falls into the enclosure. Greely tells him that it wont do him any good, and leaves. Norman rushes down to get the pistol, but the monster kills him before he gets the chance. Greely, but the monster kills and eats Norman before he can do so. Bella arrives and reveals that she is not Greelys housekeeper; he kidnapped her and abused her until her will was broken and she agreed to do whatever he told her to do to avoid being killed. Wayne convinces Bella to help them.  

Wayne remembers that he has some dynamite in his car, and he asks Bella to sneak upstairs and bring back some of it. Greely becomes suspicious of Bella, and he drugs the coffee that she brings to the prisoners. Leela and Wayne are overcome by the drug, but not before Wayne hides the dynamite.  When Wayne comes to, he retrieves it, but Greely intervenes and threatens to feed Leela to the dinosaur if she will not willingly become his new servant. Bella, having heard that he plans to dispose of her and gets down there. 

Greely recovers his pistol and shoots Wayne, but Wayne overcomes Greely and knocks him unconscious. Bella ignites the dynamite and explains to Greely that she plans to blow up the cave to kill both the dinosaur and Greely. Greely grabs his pistol and kills her, right as the monster is about to kill Greely. The dynamite explodes, collapsing the cavern and burying the dinosaur and Greely. Wayne and Leela escape in Waynes car to an unknown future.

The title screen ends with the words: The End?

== Cast ==
*Tommy Kirk as Wayne Thomas, the main hero of the film. He is Leelas love interest.
*Shirley Bonne as Leilla Sterns, Normans wife, she falls in love with Wayne after Norman is killed.
*Bill Thurman as Greely, the mad farmer who discovered the monster and kept it in his cave.
*Annabelle Weenick as Bella, a timid woman who was captured and forced to assist Greely in feeding victims to the dinosaur.
*Corveth Ousterhouse as Norman Sterns, Leelas whiny and controlling husband. He is killed and eaten by the dinosaur.

==Production==
Buchanan says the film was shot over seven days. Goodsell, Greg, "The Weird and Wacky World of Larry Buchanan", Filmfax, No. 38 April/May 1993 p 64-65  Tommy Kirk later called it "a monster movie so cheap that the monster wore a scuba suit and had Ping-Pong balls for eyes." Disney kid Tommy Kirk a cheerful has-been at 48
Edwards, Don. Chicago Tribune (1963-Current file)   06 Nov 1990: N_B7. 

==Reception==
The film received extremely negative reviews for its poor editing, bad writing, bad acting, and terrible special effects.

==Parody==
Caribou Productions, the makers of the sketch comedy show The Caribou Show, made a feature length screwball parody of Its Alive! in 2005, parodying the title by calling it Its Really Something. The film was relocated to South Dakota, with Greelys name being changed to Luke Garoo. It parodied various aspects of the film, ranging from Greelys collection, which is shown to have ostriches, which are not native to South Dakota, to the ending, where, after it said The End? It says NO! And shows Wayne and Leela hitchhiking and being picked up by the monster, who drives them into town to get married. They are shown to be adopting the monster, and the words The End? flash on screen again, saying NO! and continuing with a montage of family activities Wayne, Leela, and the dinosaur are doing. In these shots, the dinosaurs feet are seen on backwards, with the shoes of the actor playing it clearly visible. The film is scheduled to be released on DVD by Troma Entertainment.
==References==
 

== External links ==
*  
*  
* http://www.monstershack.net/sp/index.php/its-alive-1969/
* http://www.obscurehorror.com/horror813.html

 

 
 
 
 
 
 