Jeena Hai Toh Thok Daal
 
{{Infobox film
| name           = Jeena Hai Toh Thok Daal
| image          = Jeena Hai Toh Thok Daal Movie Poster.jpg
| producer       = Aparna Hoshing
| director       = Manish Vatsalya
| screenplay     = Saurabh Choudhary Yashpal Sharma Rahul Kumar Hazel Crowney
| music          =  Shadaab - Abhik Siddhant Madhav
| cinematography = Neelaabh Kaul
| editing        = Nipun Ashok Gupta
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Yashpal Sharma, Rahul Kumar and Hazel Crowney. The movies storyline is a hard hitting gangster movie which shows horrid realities of the underworld crime as well as the changing times of Bihar state. The film was released on September 14, 2012 and was written off as a box office disaster in the very first week. 

==Cast==
*Ravi Kishen as Chandrabhan
*Manish Vatsalya as Atka Yashpal Sharma as Mahkoo Rahul Kumar as Bitwa
*Hazel Crowney as Shrishti
*Sharat Saxena as Rana Murli Sharma as Hanumant Singh
*Ashwini Kalsekar as I. G.

==Controversy==
Jeena Hai Toh Thok Daal has received news coverage due to its controversial subject and the song "Mooh Mein Le" has not gone down well with Shiv Sena. 

==Critical Reception==
Overall the movie received negative reviews but actor Manish Vatsalya was acknowledged for his acting skills.  Mid Day rated it as 0.5,  while The Times of India and Daily Bhaskar ratings were 1.   The film won the 2013 Foreign Language Feature Film award at the Mexico International Film Festival.  

==References==
 

== External links ==
*  

 
 
 
 
 
 
 