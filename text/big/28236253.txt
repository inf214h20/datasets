Mickey the Detective
{{Infobox film
| name           = Mickey the Dective
| image          =
| image_size     =
| caption        =
| director       = Albert Herman
| producer       = Larry Darmour
| writer         =
| narrator       =
| starring       = Mickey Rooney Delia Bogard Marvin Stephens Junior Johnston Kendall McComas
| music          =
| cinematography =
| editing        =
| distributor    = Film Booking Offices of America
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
 silent short Mickey McGuire series starring a young Mickey Rooney. Directed by Albert Herman, the two-reel short was released to theaters in 1928 by Film Booking Offices of America|FBO.

==Plot==
Mickey and the Scorpions starts his own detective agency. Stinkie Davis kidnaps Mickeys Kid Brudder and hides him in a box headed for a science professor. Mickey and the gang catch up with Mickeys brother, but soon get into a fight with Stinkie and his pals. The kids use some of the Professors bombs as ammunition; these bombs just so happen to contain chemicals similar to laughing gas, and they literally make the kids high as a kite. Stinkie finally throws a high explosive bomb, which lands in the mouth of Buster, the Scorpions dog. The bomb is set to go off at a certain time of the day. Wanting to avoid getting blown up, the kids are forced to avoid Buster at all costs.

==Notes==
*An edited version was released to television in the 1960s as a part of the Those Lovable Scallawags With Their Gangs series.
 Mickey McGuire Jimmy Robinson. Another kid takes over for the role of Hambone.

==Cast==
*Mickey Rooney - Mickey McGuire
*Unknown - Hambone Johnson
*Delia Bogard - Tomboy Taylor
*Marvin Stephens - Katrink
*Junior Johnston - Stinkie Davis
*Kendall McComas - Scorpions member

== External links ==
*  

 
 
 
 
 
 
 
 
 


 