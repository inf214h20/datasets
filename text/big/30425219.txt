The Strange Case of Angelica
{{Infobox film
| name           = The Strange Case of Angelica
| image          = The Strange Case of Angelica.jpg
| caption        = Film poster
| director       = Manoel de Oliveira
| producer       = Jacques Arhex
| writer         = Manoel de Oliveira
| starring       = Pilar López de Ayala
| music          = 
| cinematography = Sabine Lancelin
| editing        = Valérie Loiseleux
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
}}
The Strange Case of Angelica ( ) is a 2010 Portuguese drama film directed by Manoel de Oliveira. It was entered into the Un Certain Regard section of the 2010 Cannes Film Festival.     De Oliveira conceived the idea for the film in 1946 and initially wrote the script in 1952, updating it with modern elements. 

==Cast==
* Pilar López de Ayala as Angélica
* Filipe Vargas as Marido
* Ricardo Trêpa as Isaac
* Leonor Silveira as Mother
* Ana Maria Magalhães as Clementina
* Isabel Ruth as Doméstica
* Luís Miguel Cintra as Engineer
* Ricardo Aibéo
* Paulo Matos as Homem da Gabardine
* Adelaide Teixeira as Justina

==References==
 

==External links==
*  
*  
*  
*  
*   at Metacritic
*  

 

 
 
 
 
 
 
 
 