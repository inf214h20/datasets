A Jester's Tale
{{Infobox film
| name = A Jesters Tale
| image = Blaznova kronika 1964 poster.jpg
| alt=
| caption = Original poster
| director = Karel Zeman
| producer = Josef Ouzký
| writer =  
| starring =   Jan Novák
| cinematography = Václav Huňka
| editing = Miroslav Hájek
| studio = Filmové studio Barrandov
| distributor =
| released =  
| runtime = 81 minutes
| country = Czechoslovakia Czech

}}
 1964 Cinema Czech film directed by Karel Zeman. Described by Zeman as a "pseudo-historical fiction|historical" film, it is an anti-war black comedy set during the Thirty Years War. The film combines live action with animation to suggest the artistic style of the engraver Matthäus Merian. 

The film was a notable success at the 1964 San Francisco International Film Festival, winning Best Picture and Best Director. It was also voted Best Film at the 1964 Addis Ababa IFF in Ethiopia, and was honored in three categories at Cannes. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 