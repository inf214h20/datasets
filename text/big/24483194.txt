Paper Bullets
 
{{Infobox film
| name           = Paper Bullets
| image          =Paper Bullets DVD cover.jpg
| image_size     =
| caption        =
| director       = Phil Rosen Maurice King (producer)
| writer         = Martin Mooney
| narrator       =
| starring       = Joan Woodbury Jack La Rue Linda Ware
| music          =
| cinematography = Arthur Martinelli
| editing        = Martin G. Cohn
| distributor    = Producers Releasing Corporation
| released       = 13 June 1941
| runtime        = 72 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 King Brothers.

== Plot summary ==
 

== Cast ==
*Joan Woodbury as Rita Adams
*Jack La Rue as Mickey Roman
*Linda Ware as Donna Andrews John Archer as Bob Elliott
*Vince Barnett as Scribbler, a petty forger
*Alan Ladd as Jimmy Kelly aka Bill Dugan Gavin Gordon as Kurt Parrish
*Phillip Trent as Harold DeWitt William Halligan as Police Chief Flynn
*George Pembroke as Clarence DeWitt
*Selmer Jackson as District Attorney
*Kenneth Harlan as Jim Adams
*Bryant Washburn as Attorney Bruce King
*Alden "Stephen" Chase as Detective Joe Kent Robert Strange as Lou Wood
*Alex Callam as Joe Fagan
*Harry Depp as Johnny Mason

== Soundtrack ==
* "I Know, I Know" (by Vic Knight, Johnny Lange and Lew Porter)
* "Blue Is the Day" (by Maurice Kozinsky, Johnny Lange and Lew Porter)

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 