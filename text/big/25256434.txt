The Don's Analyst
{{Infobox Film
| name = The Dons Analyst
| image = DonsAnalyst.jpg
| image_size =
| caption =
| director = David Jablin
| producer = David Jablin James P. Jimirro Larry Rapaport David Hurwitz
| narrator =
| starring = Robert Loggia Kevin Pollak
| music =
| cinematography =
| editing =
| distributor = Showtime Networks Paramount Pictures
| released = September 6, 1997 (USA)
| runtime = 103 min
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
}} Showtime in 1997. Starring Robert Loggia, it pre-dated the very similarly plotted movie Analyze This.

==Plot==
Don Vito Leoni, the Godfather, is clinically depressed. The world has changed and he hasnt. Hed like to retire, but if he left the "family business" to his two idiot sons, theyd be dead in a minute. So he decides to go legit, which convinces everyone that he must be completely off the deep end. To preserve their cushy lives, his dysfunctional family conspires to get him some psychotherapy. So his boys kidnap a "paisan" shrink, and order him to "fix" their father. 

==Cast==
* Rick Aiello - Frankie Leoni
* Robert Cicchini - Donnie Leoni
* Joe Pingue - Hood #1
* Robert Loggia - Don Vito Leoni
* Howard Jerome - Tomaso
* Louis Di Bianco - Salvatore
* David Calderisi - Anthony
* Lou Pitoscia - Tommy The Rope
* George Santino - Paul Tondini
* Angie Dickinson - Victoria Leoni
* Angelica Trevena - Stripper
* Alan C. Peterson - Nicky Tits (as Alan Peterson)
* Gino Marrocco - Gino
* Kevin Pollak - Dr. Julian Riceputo
* Lucy Webb - Dr. Susan Riceputo
* Joe Flaherty - Dr. Lowell Royce David Bolt - Dr. Jack Schweigert
* Sherilyn Fenn - Isabella Leoni
* Paulo Costanzo - Young Vito
* Courtney Hawkrigg - Young Victoria
* Daniel DeSanto - Young Vincent
* Joseph Bologna - Vincent DeMarco
* Peter Blais - Dr. Byron Frohlich
* Philip Akin - Dr. Lusting

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 


 