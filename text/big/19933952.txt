The Net (1975 film)
 
{{Infobox film
| name           = The Net
| image          = 
| caption        = 
| director       = Manfred Purzer
| producer       = Luggi Waldleitner
| writer         = Hans Habe Manfred Purzer
| starring       = Mel Ferrer Elke Sommer Klaus Kinski Heinz Bennent
| music          = 
| cinematography = Charly Steinberger
| editing        = Corinna Dietz Ingeborg Taschner
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

The Net ( ) is a 1975 West German drama film directed by Manfred Purzer and starring Mel Ferrer.     

==Cast==
In alphabetical order
* Heinz Bennent as Canonica
* Carlo De Mejo
* Maria DIncoronato
* Mel Ferrer as Aurelio Morelli
* Claudio Gora
* Giovannella Grifeo
* Sonja Jeannine as Vera Pisenti
* Klaus Kinski as Emilio Bossi
* Andrea Rau
* Willi Rose
* Franz Rudnick
* Elke Sommer as Christa Sonntag
* Susanne Uhlen as Agnese
* Sabine von Maydell

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 