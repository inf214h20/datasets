Nobody Lives Forever (1946 film)
{{Infobox film
| name           = Nobody Lives Forever
| image          = Nobody Lives Forever.jpg
| alt            =
| caption        = Theatrical release poster
| producer       = Robert Buckner
| director       = Jean Negulesco
| screenplay     = W.R. Burnett
| based on       =  
| starring       = John Garfield Geraldine Fitzgerald Walter Brennan Faye Emerson
| music          = Adolph Deutsch
| cinematography = Arthur Edeson
| editing        = Rudi Fehr
| distributor    = Warner Bros. 
| released       =   
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} noir directed by Jean Negulesco and based on the novel I Wasnt Born Yesterday by W.R. Burnett. It starred John Garfield and Geraldine Fitzgerald. 

==Plot==
Former conman Nick Blake (John Garfield), a soldier returning to New York City after World War II, looks up his old girlfriend Toni Blackburn (Faye Emerson) to get the money she has been holding for him while he was in the army.  She claims that she lost the money investing in a nightclub before selling it to her boyfriend, Chet King (Robert Shayne). Unconvinced, Nick get his money back from his ex-girlfriends new beau and leaves town.

Nick meets up with old conman Pop Gruber (Walter Brennan), who tells him that hes getting too old for the con game and that if he keeps it up, he will end up as "selling pencils on the side of the road."  Another con artist, Doc Ganson (George Coulouris), has spotted a sucker, but has neither the money nor the looks necessary for the job. When he hears that Nick is in town, he reluctantly approaches Pop to recruit him, even though there is bad blood between them. The plan is to have Nick, a ladys man, romance rich recent widow Gladys Halvorsen (Geraldine Fitzgerald) and persuade her to invest in a phony tugboat business.  Nick agrees on condition that he get two thirds of the proceeds, increasing Docs bitter resentment of the younger, more successful man.

The plan hits a snag when Nick falls in love with the intended victim and attempts to back out of the "big con".  When Charles Manning (Richard Gaines), Gladyss business manager, finds out about Nicks criminal past, Nick admits the truth to Gladys. However, she believes he can change and refuses to let him go.

Nick decides to pay the others the $30,000 he promised them using his own money. However, Toni shows up and learns of the aborted scheme. When she tells Doc that she is sure Nick intends to marry Gladys (and her $2,000,000), the gang kidnaps the widow for a larger share of her money. Luckily, Pop is able to follow them to their hideout. In the ensuing gunfight, Nick rescues Gladys, but both Doc and Pop are killed.

== Cast ==
* John Garfield as Nick Blake
* Geraldine Fitzgerald as Gladys Halvorsen
* Walter Brennan as Pop Gruber
* Faye Emerson as Toni Blackburn
* George Coulouris as Doc Ganson
* George Tobias as Al Doyle, Nicks friend
* Robert Shayne as Chet King
* Richard Gaines as Charles Manning
* Richard Erdman as Bellboy
* James Flavin as Shake Thomas, one of Docs men
* Ralph Peters as Windy Mather, Docs other associate

==Reception==

===Critical response===
Film critic Bosley Crowther while stating that the production team of the film "managed a craftsmanlike job", he nonetheless, made the case the viewers will find the films "repetition just a bit wearisome and even dull. They are likely to find the dialogue — although flavored with such racy words as pitch and plant and sucker — rather heavily loaded with cliches. And they will certainly find nothing original in the easy solution of the plot." 

==Adaptation==
The film was adapted for radio on the Lux Radio Theatre. The production featured Jane Wyman and Ronald Reagan and was aired on November 17, 1947. 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*  

===Streaming audio===
*   on Lux Radio Theatre: November 17, 1947

 

 
 
 
 
 
 
 
 