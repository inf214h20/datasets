Death at Broadcasting House
{{Infobox film
| name           = Death at Broadcasting House
| image          = "Death_at_Broadcasting_House"_(1934).png
| alt            =
| caption        = Jack Hawkins as Herbert Evans
| film name      = 
| director       = Reginald Denham
| producer       =  Hugh Perceval 
| writer         = 
| screenplay     = Val Gielgud, Holt Marvell & Basil Mason
| story          = 
| based on       =   Ian Hunter Austin Trevor Lilian Oldland
| music          = Ord Hamilton
| cinematography = Günther Krampf
| editing        = Reginald Beck
| production companies = Phoenix Films
| distributor    = Associated British Film Distributors  (UK)
| released       =  
| runtime        = 75 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}} British mystery Ian Hunter, Henry Kendall, and Jack Hawkins. 

==Novel==
The original plot comes from a novel of the same name,  set in what was then the mysterious world of radio in what was then the BBCs new broadcasting centre, Broadcasting House. It was written in 1934 by Val Gielgud – brother of John Gielgud, and then the BBCs Head of Productions –  and "Holt Marvell" - actually Eric Maschwitz, a lyricist and writer for films and the BBC.

The plot revolves round a live broadcast of a play, using multiple studios (as was common at the time). One actor has a scene by himself, at the end of which the script calls for him to be strangled: he plays this alone in a separate studio, but at the end of the play is discovered to have been strangled in reality. The book goes through all the usual procedures of a detective novel, tracking the motives and opportunities of the suspects. The book isnt at all badly written, though perhaps a little plodding in places, but is of particular interest because the authors have been careful to keep it realistic: the exact layout of Broadcasting House is made use of (floor plans are provided) and the technique of radio drama is accurately represented: it does provide an intriguing glimpse into the workings of the BBC.

==Film== Adventures of Robin Hood) as the detective, Donald Wolfit as the murder victim, and Val Gielgud himself as the drama producer, Julian Caird. The film sticks closely to the plot of the book, but is lighter in tone, and capitalizes on the glamour of broadcasting by including a number of cameo appearances by radio stars, among them Hannen Swaffer, Gillie Potter, Elisabeth Welch and Percival Mackey.

Again, the feeling of period broadcasting is reasonably authentic: but when it was last shown on television (many years ago) a well-respected technical journalist was trapped into recommending readers to watch it because it provides a "fascinating glimpse into Broadcasting House in the 1930s". Of course it does nothing of the sort. Quite apart from the technical difficulties in filming in relatively small studios (the camera wouldnt have been able to get far enough back) the BBC would hardly have welcomed a large film crew underfoot for the month it took to shoot.
 Blattnerphone (an early tape recorder using huge reels of steel tape), but unfortunately they werent able to show it - either it was technically impracticable, or perhaps the BBC wouldnt co-operate.

The most dramatic difference caused a lot of amusement among Control Room staff at the time  When the murderer is revealed there is a chase through Broadcasting House, including the Control Room on the eighth floor. In the book, the murderer makes his way onto the roof, is shot after himself shooting at a policeman, and falls a hundred and twelve feet sheer to the pavement below. In the film he rushes into a small room off Control Room, and evidently flings himself across some terminals as there is a flash, all the lights go out, and he is dead by electrocution. This was the 50 volt relay supply.

==Cast== Ian Hunter as Detective Inspector Gregory 
* Austin Trevor as Leopold Dryden 
* Lilian Oldland as Joan Dryden Henry Kendall as Rodney Fleming 
* Val Gielgud as Julian Caird 
* Peter Haddon as Guy Bannister 
* Betty Ann Davies as Poppy Levine 
* Jack Hawkins as Herbert Evans 
* Donald Wolfit as Sydney Parsons 
* Robert Rendel as Sir Herbert Farquharson
* Bruce Lester as Peter Ridgewell

==References==
 

==External links==
*  

 

 
 
 
 