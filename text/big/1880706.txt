Payback (1999 film)
 
{{Infobox film
| name           = Payback
| image          = PaybackPoster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Brian Helgeland Paul Abascal  }}
| producer       = Bruce Davey
| screenplay     = Brian Helgeland Terry Hayes
| based on       =   John Glover Jack Conley James Coburn Kris Kristofferson
| music          = Chris Boardman
| cinematography = Ericson Core
| editing        = Kevin Stitt
| studio         = Icon Productions
| distributor    = Paramount Pictures (US) Warner Bros. (non-US)
| released       =  
| runtime        = 90 minutes (Directors Cut)  101 minutes (Theatrical Cut)
| country        = United States
| language       = English
| budget         = $90 million 
| gross          = $161.6 million 
}} action crime The Hunter Point Blank, directed by John Boorman and starring Lee Marvin. 

The film was Helgelands directorial debut after a career as a screenwriter. Helgeland in 2006 issued a directors cut that differs substantially from the version released by the studio.

==Plot==
In a filthy kitchen of an underground abortionist, an unkempt man puts on surgical gloves and quickly downs a full glass of cheap whiskey. Face down on the kitchen table is a barely conscious Porter (Mel Gibson), severely wounded with two large bullet wounds in his back. The doctor pours whiskey on Porters back to sterilize the area and digs out the bullets. Porter spends five months recuperating. Porter narrates that he had $70,000 taken from him and that is what he was going to get back.
 Chinese triads. After Lynn shot Porter and the two left him for dead, Val rejoined the Outfit, a powerful criminal organization, using $130,000 of the heist money to repay an outstanding debt. Porter is intent on reclaiming his $70,000 cut.
 Jack Conley).

Resnick is seeing a dominatrix (Lucy Liu) when Porter violently re-enters his life. Resnick goes to the Outfit to explain why Porter is demanding $70,000. Told to handle it himself, Resnick tries to, but is shot by Porter in Rosies apartment as Porter catches him abusing Rosie when Porter returns to collect his forgotten cigarettes.
 John Glover), who have been sent by Carter (William Devane), their immediate superior with the Outfit, to "Stitch this mutt up."

Threatening to kill Carter next if somebody higher in the organization wont pay his $70,000, Porter hears the refusal of mob boss Bronson (Kris Kristofferson) on the phone, so he carries out his threat.

With the aid of Rosie, he kidnaps Bronsons son Johnny, keeping him tied up. He arranges for Hicks and Leary to be busted by their own colleagues in Internal Affairs by planting Learys fingerprints on the gun Porter used to kill Resnick. He pick-pockets Hickss badge, then leaves it with the gun in the vicinity of Resnicks dead body.

Bronson and his mob associate Fairfax (James Coburn) join the hunt to take him down. Porter is captured by the Outfit after a wild chain of events involving the triads. He is tortured by having his toes smashed with a hammer.

Porter is locked inside a car trunk and taken by Bronson and his men to an apartment that had previously been rigged by the Outfits men to a phone connected to plastic explosive. After his captors meet an explosive demise, Porter is picked up by Rosie (with her dog, also named Porter). When she sees his injuries and asks what happened, Porter replies, "I got hammered." They drive off to Canada to begin a new life.

==Directors Cut==
The Directors Cut (dubbed Payback: Straight Up) has a largely similar foundation but explores the betrayal of Porter through flashbacks and most significantly removes the Bronson character from the screen. Instead a female voice (belonging to Sally Kellerman) on the telephone replaces Kris Kristofferson. The scene with the booby-trapped telephone is eliminated, as is the kidnapping of Bronsons son. In this version, Val kills Rosies dog Porter.

The simplified story line ends with Porter collecting his money at an arranged drop in a train station where he has several shootouts with syndicate hit-men staking out the station. He is seriously wounded and seemingly near death before being driven away by Rosie with the money. The Directors Cut also lacks the theatrical versions voice-over narration by Porter.

A June 4, 2012 look at "movies improved by directors cuts" by the AV Club described Payback: Straight Up as "a marked improvement on the unrulier original." 

==Cast==
* Mel Gibson as Porter
* Gregg Henry as Val Resnick
* Maria Bello as Rosie Lucy Alexis Liu as Pearl Deborah Kara Unger as Lynn Porter
* David Paymer as Arthur Stegman
* Bill Duke as Detective Hicks Jack Conley as Detective Leary John Glover as Phil
* William Devane as Carter
* James Coburn as Fairfax
* Kris Kristofferson as Bronson (Theatrical Cut)
* Sally Kellerman as Bronson (Directors Cut)
* Trevor St. John as Johnny Bronson (Theatrical Cut) Freddy Rodriguez as Valet
* Manu Tupou as Pawnbroker

==Production==
The film was shot during September/November 1997, in Chicago and Los Angeles, though neither city is referred to in the film. Although credited as director, Brian Helgelands cut of the film was not the theatrical version released to audiences. After the end of principal photography, Helgelands version was deemed too dark for the mainstream public. Following a script rewrite by   walked on as a new villain. 

After her stint on TVs ER (TV series)|ER, this was Maria Bellos first major film. Lucy Liu, appearing in one of her first films, is billed as "Lucy Alexis Liu."

Helgelands version, Straight Up: The Directors Cut, was released on DVD, Blu-ray, and HD DVD on April 10, 2007, after an October 2006 run at the Austin Film Festival. The Directors Cut version features a female Bronson, voiced by Sally Kellerman, does not include the voice-over by Porter and several Bronson-related scenes, and has an entirely different, ambiguous ending.
 Twilight Zone episode, "The Jeopardy Room", directed by Richard Donner, who has worked with Mel Gibson.

==Editing==
On the DVD release there is a short interview with Mel Gibson in which he stated that it "Wouldve been ideal to shoot in black and white". He noted that "people want a color image" and that the actual film used a "Bleach bypass process" to tint the movie. In addition to this the production design used muted shades of red, brown and grey for costumes, sets and the cars for further effect.  

==Reception   ==
Payback was well received at the box office. The film made $21,221,526 in its opening weekend in North America. It eventually grossed $81,526,121 in North America and $80,100,000 in other territories, totaling $161,626,121 worldwide. 

 
The film garnered mixed reviews. Review aggregator Rotten Tomatoes reported that 54% of 74 sampled critics gave Payback positive reviews and that it got a rating average of 5.8 out of 10. The critical consensus states "Sadistic violence and rote humor saddle a predictable action premise." 
Roger Ebert gave the film a three-star rating (out of four) in his review, writing, "There is much cleverness and ingenuity in Payback, but Mel Gibson is the key. The movie wouldnt work with an actor who was heavy on his feet, or was too sincere about the material." 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 