Japanese Story
 
{{Infobox film
| name           = Japanese Story
| image          = Japanese Story movie.jpg
| caption        =
| director       = Sue Brooks
| writer         = Alison Tilson
| starring       = Toni Collette Gotaro Tsunashima
| producer       = Sue Maslin
| music          = Elizabeth Drake Ian Baker
| editing        = Jill Bilcock
| distributor    = Samuel Goldwyn Films
| budget         = $5,740,000 
| gross          = $4,050,497 
| released       = 25 September 2003  (Australia) 
| runtime        = 110 minutes
| country        = Australia
| language       = English, Japanese
}}

Japanese Story is a 2003 Australian romantic drama film directed by Sue Brooks. It was screened in the Un Certain Regard section at the 2003 Cannes Film Festival.   

==Plot== director in a company designing geological software in Perth, Western Australia. Her business partner manipulates her into agreeing to act as a guide for a Japanese businessman visiting mines in the Pilbara desert, in hopes that the businessman will purchase her product and have a good impression during his trip. When Hiromitsu Tachibana  (Gotaro Tsunashima) arrives, he treats Sandy like an ordinary driver, with more intent on discovering himself in the wilderness than on buying computer software. At first Sandy is angered by his private and demanding nature. During their first travel in the desert, Hiromitsu, feeling insecure, speaks more on his phone with friends in Japan than he does to Sandy driving next to him, and insists that she drives farther than planned. The terrain becomes too much for the pairs vehicle which becomes bogged down. After a series of desperate attempts to release the vehicle, including digging a dead man anchor,    their winch burns out. Sandy tries to contact other people for rescue through cellphone, but Hiromitsu keeps stopping her from doing so. This forces them to spend the night stranded together. The next day Hiromitsu, conscious that his actions had placed them in danger, wakes up much earlier than Sandy and builds a wooden sticks track over which they can drive out of the sand. Now that they are on the road again, the ice breaks and a friendship starts between them that, in isolated surroundings uninterrupted by their work, grows quickly and honestly. Later on at a motel, they have sexual intercourse. Only after does Sandy learn that Hiromitsu has a wife and children in Japan.

On another travel to other nature spots, Hiromitsu and Sandy share a quiet moment and kiss each other, eventually having sex again. Afterwards, Sandy takes off her shirt and runs into a swimming hole nearby, where Hiromitsu dives into shallow water before she can warn him. After a moment of frantic calling from Sandy, Hiromitsus corpse resurfaces. In shock at his sudden death, Sandy struggles in extremity with the necessities of the situation, dragging his body into their vehicle then carefully washing it before driving for hours to reach the nearest town. Back in Perth, Sandy cannot comprehend the violent end to her journey. Reality intrudes in the form of Hiromitsus grieving widow, Yukiko, leaving Sandy attempting to understand how Hiromitsus life had ended before she had understood his place in hers.

==Cast==
* Toni Collette – Sandy Edwards
* Gotaro Tsunashima – Hiromitsu Tachibana
* Matthew Dyktynski – Bill Baird
* Lynette Curran – Mum
* Yumiko Tanaka – Yukiko Tachibana Kate Atkinson – Jackie Bill Young – Jimmy Smithers
* Reg Evans – Bloke in Row Boat
* George Shevtsov – James
* Justine Clarke – Jane
* Igor Sas – Fraser
* Mike Frencham – Blake John Howard – Richards
* Phil Bennett – Barman
* Heath Bergersen – Petrol Bloke

==Box office==
Japanese Story grossed $4,520,000 at the box office in Australia. 

==Reception and accolades==

Japanese Story received mixed to positive reviews, currently holding an approval rating of 68% on Rotten Tomatoes.

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award  (2003 AFI Awards)  AACTA Award Best Film Sue Maslin
| 
|- AACTA Award Best Direction Sue Brooks
| 
|- AACTA Award Best Original Screenplay Alison Tilson
| 
|- AACTA Award Best Actor Gotaro Tsunashima
| 
|- AACTA Award Best Actress Toni Collette
| 
|- AACTA Award Best Cinematography Ian Baker Ian Baker
| 
|- AACTA Award Best Editing Jill Bilcock
| 
|- AACTA Award Best Original Music Score Elizabeth Drake
| 
|- AACTA Award Best Sound Livia Ruzic
| 
|- Peter Grace
| 
|- Peter Smith
| 
|- AACTA Award Best Production Design Paddy Reardon
| 
|- AWGIE Awards|AWGIE Award Best Writing in a Feature Film - Original Alison Tilson
| 
|- Bangkok International Film Festival Golden Kinnaree Award for Best Film Sue Brooks
| 
|- Chicago International Film Festival Gold Hugo - New Directors Competition
| 
|- Film Critics FCCA Awards Best Film Sue Maslin
| 
|- Best Director Sue Brooks
| 
|- Best Original Screenplay Alison Tilson
| 
|- Best Female Actor Toni Collette
| 
|- Best Music Score Elizabeth Drake
| 
|- Best Editing Jill Bilcock
| 
|- Best Cinematography Ian Baker
| 
|- Inside Film Awards Best Feature Film Sue Maslin
| 
|- Best Direction Sue Brooks
| 
|- Best Script Alison Tilson
| 
|- Best Actor Gotaro Tsunashima
| 
|- Best Actress Toni Collette
| 
|- Best Cinematography Ian Baker
| 
|- Best Editing Jill Bilcock
| 
|- Best Music Elizabeth Drake
| 
|- Miami International Film Festival FIPRESCI Prize Sue Brooks
| 
|- Satellite Award Satellite Award Best Actress Toni Collette
| 
|- Screen Music Awards, Australia Best Feature Film Score Elizabeth Drake
| 
|-
|}

==See also==
 
* Cinema of Australia

==References==
 

==External links==
* 
*  - Palace Films
*  at the National Film and Sound Archive

 
 

 
 
 
 
 
 
 
 