Faithful (1936 film)
{{Infobox film
| name           = Faithful
| image          =
| image_size     =
| caption        =
| director       = Paul L. Stein
| producer       = Irving Asher
| writer         = Brock Williams Jean Muir Hans Söhnker
| music          = Pierre Neuville
| cinematography = Basil Emmott Leslie Norman Warner Brothers-First First National Productions
| released       = March 1936
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
}} Jean Muir and Hans Söhnker.

The film was a quota quickie production, with an original screenplay by Brock Williams and music by Pierre Neuville.  The plot deals with two pupils from a provincial music conservatory who elope, marry, and come to London to try their luck.   The husband becomes a singer in a nightclub, and is soon targeted by a predatory socialite.  They start an affair, the wife finds out about it and decides to leave her husband, until matters are smoothed over by a third-party who wishes the couple well. Faithful is now classed as a lost film. 

==Cast== Jean Muir as Marilyn Koster
* Hans Söhnker as Carl Koster
* Chili Bouchier as Pamela Carson
* Gene Gerrard as Danny Reeves

==References==
 

==External links==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 
 


 