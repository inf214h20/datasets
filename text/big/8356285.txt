Kondaveeti Raja
  Tollywood film Radha and Rao Gopal Rao. This film was directed by K. Raghavendra Rao. The film released on January 31, 1986.

==Plot==
In Kondaveedu, there is an old Gadwal fort and every one in that village believes that there is a hidden treasure inside. Rao Gopal Rao, a rich man in the village, tries every possible way to explore that treasure. An archeological assistant, Raja (Chiranjeevi), comes to this village in disguise as an ordinary youth in search of a job. He unearths the secret of this hidden treasure. Raja becomes a problem for Raogopalrao by interfering in all his misdeeds.
Unexpectedly, Rajas sisters daughter Padma is in the same village and she meets Raja to gives her heart to him. But Raja falls in love with another village belle, Rani. Raja realizes the truth that it was Rao Gopal Rao, who killed his sister and plans to take revenge for his sisters murder. He succeeds in his attempts at the cost of Ranis love, who sacrifices her love to help Raja. The hidden treasure is recovered and handed over to the government, and Raja and Padma marry.

==Cast==
* Chiranjeevi
* Vijayashanti Radha
* Rao Gopala Rao
* Kaikala Satyanarayana
* Srividhya
* Amrish Puri
* Sharadha



 
 
 
 


 