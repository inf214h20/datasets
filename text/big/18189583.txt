The Tiger Woman (1917 film)
 
{{Infobox film
| name           = The Tiger Woman
| image          = The Tiger Woman-1917-newspaperad.jpg
| caption        = Contemporary newspaper advertisement. George Bellamy William Fox
| writer         = Adrian Johnson (scenario)
| story          = James W. Adams
| starring       = Theda Bara Edward Roseman
| cinematography = Phil Rosen
| editing        = Fox Film Corporation
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         =
}}
 silent drama George Bellamy and starring Theda Bara. The film is now considered lost film|lost.   

==Plot==
Countess Irma (Theda Bara) is a Russian villainess who becomes the ruthless Princess Petrovich, who loves only her pearls. Her husband, the Prince (Edward Roseman), sells state secrets to a spy to pay her exorbitant bills, and her response is to report him to the secret police.

Then she runs off to Monte Carlo with her lover, Count Zerstoff (Emil DeVarney), but she poisons him after he racks up a load of gambling losses. She goes to America followed by Stevan, a disgruntled servant (John Webb Dillon) and there she wreaks more havoc.
 Glen White). He dumps his fiancée (Florence Martin) for the vamp and steals money from his father (Edward Holt). The shock kills the father and the Princess has Edwin sent off to jail. She next becomes involved with Edwins brother, Mark (Herbert Heyes), inspiring him to leave his wife (Mary Martin) and child (Kittens Reichert).

Finally Edwin and Stevan (who also has been sent to jail through the Princess machinations) get away from their confinement and head over to the vamps. She tries to stab Stevan, but he turns the knife onto herself and she is fatally stabbed. But before she dies she confesses all, which clears the name of both Harris brothers, and Mark returns to his wife.

==Cast==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#CCCCCC" align="center"
|- bgcolor="#CCCCCC" align="center"
|-
| Theda Bara || Princess Petrovitch
|-
| Edward Roseman || Prince Petrovitch
|-
| Louis Dean || The Baron
|-
| Emil DeVarney || The Count Zerstoff
|-
| John Webb Dillon || Stevan
|- Glen White || Edwin Harris
|-
| Mary Martin || Mrs. Edwin Harris
|-
| Herbert Heyes || Mark Harris
|-
| Kittens Reichert || Harris Child
|-
| Edward Holt || Harris Boys Father
|-
| Florence Martin || Marion Harding
|-
| George Clarke || Marions Father
|-
| Kate Blancke || Marions Mother
|-
| Charles McCann || Butler
|-
| Hans Unterkircher || Uncredited
|-
|}

==Brazil==
The film was released in Brazil with title Mulher Tigre on July 26, 1917  at Cine Ideal, situated at the centre of Rua da Carioca 60-62, Rio de Janeiro. It was a hit amongst the Brazilian audience for six weeks and it was also exhibited on Cines Capitolio and Pathé from August 5, 1917.  Cine Pathé was a cinema located at Floriano square also called Cinelândia. Cine Ideal belonged to the group Severiano Ribeiro, which still holds in its storehouse a couple of old silent films.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 