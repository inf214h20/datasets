The Gladiators (film)
 

 
 
{{Infobox film
| name           = The Gladiators
| image_size     =
| image	         = The Gladiators FilmPoster.jpeg
| caption        =
| director       = Peter Watkins
| producer       = Göran Lindgren
| writer         = Peter Watkins Nicholas Gosling
| narrator       =
| starring       =
| music          = Claes af Geijerstam
| cinematography = Peter Suschitzky
| editing        =
| studio         =
| distributor    =
| released       = 25 June 1969
| runtime        = 92 min.
| country        = Sweden English Cantonese Cantonese French French German German Swedish Swedish
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 science fiction film directed by Peter Watkins.

==Plot==
In order to prevent a Third World War, the superpowers decide to introduce “international peace games,” a deadly miniature battle fought between small teams of drafted  teenage soldiers from each country and broadcast on TV around the world as the most popular reality TV program.

==Cast==
* Arthur Pentelow as British General
* Frederick Danner as British Staff Officer
* Hans Bendrik as Capt. Davidsson
* Daniel Harle as French Officer
* Hans Berger as West German Officer
* Rosario Gianetti as American Officer
* Tim Yum as Chinese Staff Officer
* Kenneth Lo as Chinese Colonel
* Björn Franzen as Swedish Colonel
* Christer Gynge as Assistant Controller
* Jürgen Schilling as East German Officer
* Stefan Dillan as Russian Officer
* Ugo Chiari as Italian Officer
* Chandrakant Desai as Indian Officer George Harris as Nigerian Officer
* Pik-Sen Lim as C-2

==External links==
*   from Peter Watkins website
*  

 

 
 
 
 
 
 

 