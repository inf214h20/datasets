A Place in the Stars
{{Infobox film
| name           = A Place in the Stars
| image          = A Place in the Stars poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Steve Gukas
| producer       = Steve Gukas   Nosa Obayuwana
| writer         = Ita Hozaife   J.K. Amalou
| starring       =  
| music          = George Kallis
| cinematography = Harald Beeker   John L. Demps Jr.   Manu Lapière 
| editing        = Antonio Rui Ribeiro 
| studio         = Natives Filmworks   Jungle FilmWorks
| distributor    = Silverbird Film Distribution
| released       =  
| runtime        = 112 minutes
| country        = Nigeria
| language       = English
| budget         = ₦160 million (est)    
| gross          = 
}} crime thriller film written by Ita Hozaife and J.K. Amalou, co-produced and directed by Steve Gukas. It stars Gideon Okeke, Segun Arinze, Matilda Obaseki, Yemi Blaq, Femi Branch and Dejumo Lewis.    The film was inspired by the tenure of Late Professor Dora Akunyili as the Director General of the National Agency for Food and Drug Administration and Control (NAFDAC). 

The film is set in 2006,    and narrates the story of Kim Dakim (Gideon Okeke), a young lawyer who has information about the trafficking of counterfeit drugs and stands to make millions of dollars if he doesn’t pass on the information. However, the drug baron, Diokpa Okonkwo (Segun Arinze) is ready to kill in order protect his thriving illegal trade and Kim has him to contend with, while battling with his conscience and everything he has been raised to believe is right.

==Plot==
 

==Cast==
*Gideon Okeke as Kim Dakim 
*Segun Arinze as Diokpa Okonkwo
*Dejumo Lewis as Pa Dakim
*Matilda Obaseki as Tara
*Femi Branch as Young Pa Dakim
*Yemi Blaq as Dayo Thomas
*Julian Mcdowell as MD Rasco Mining
*Armajit Deu as Veejay
*Amaka Mgbor as Vickie
*Ladi Alpha as Simi Dakim
*Zubairu J. Attah as Charles Coker
*Lantana Ahmed as Ngo Simi

==Production==
A Place in the Stars focuses on "how fake and adulterated medical drugs are being traded for billions of dollars".  Gukas decided to make a film on fake drugs, having been inspired by the tenure of the now late Dora Akunyili as the Director General of the National Agency for Food and Drug Administration and Control (NAFDAC). According to him, the zeal and dedication of Akinyili helped a lot to reduce the prevalence of fake drugs in Nigeria to a minimum level at the time.  

The film which was partly funded by Nigerias National Film and Video Censors Board,  is a co-production of Native Filmworks with Jungle FilmWorks, Massive Entertainment, Consolidated Media Associates, Pepper Fruit Consult, MichelAngelo Productions and T-Large Media.   According to Gukas, the total amount spent on producing the film is over a million dollars.  It was shot in Jos, Abuja and Lagos.  The films development through production took a period of five years.     The director noted the funding style as the reason behind the long duration.   

===Music and soundtrack=== Chocolate City music label was released on 21 September 2014, along with its music video.  

==Promotion and release==
  for his #APlaceInTheStars campaign]]
The film was officially announced to the press on 20 August 2014 at Freedom Park, Lagos Island.         The official trailer for the film was unveiled at the event, before it was uploaded on YouTube the same day.     Exclusive behind-the-scenes footage; including the making of the film, the music score and music video of the films soundtrack were also screened for guests at the event.      On 1 October, a social media campaign was launched on Twitter, which involves users, including celebrities, twitting the name of people who they think deserve a place in the stars, with the hash-tag "#APlaceInTheStars".    A private screening was held for the film at the Four Points Sheraton Hotel, Victoria Island, Lagos on 12 October 2014.  The film premiered on 7 November 2014 at Landmark Centre, Victoria Island, Lagos,    and slated for a general release on 14 November.   

==Reception==
===Critical reception===
Wilfred Okiche of YNaija says A Place in the Stars is "a disappointing film", faulting almost everything in it. He cited Gideon Okeke as an unengaging leading man, says the screenplay is not dramatic enough, and concludes : "Any director would have presented A Place in the stars in this way and it would be terrible. But Gukas, whose profile is enough to inspire certain lofty expectations, proceeds with this effort, to dash every single one of them. Nothing inspiring, entertaining or admirable about this  ".  Oris Aigbokhaevbolo of "True Nollywood Stories" gave 5.4 stars, also generally panning the film and Okekes performance, but commended its motive. He concludes: "Okeke’s smug-plated acting aside, this is the trouble with A Place in the Stars: it is an NGO enlightenment campaign for television. Screenwriter Ita Hozaife’s and J.K Amalou’s hearts are in the right place, but their film is showing at the wrong space". 

===Accolades===
A Place in the Stars was nominated in eleven categories at the 2015 Africa Magic Viewers Choice Awards, and won the award for "Best Drama Film". 
{| class="wikitable sortable" style="width:100%"
|+Complete list of Awards
|-
!  Award !! Category !! Recipients and nominees !! Result
|- Multichoice   (2015 Africa Magic Viewers Choice Awards)  
| Best Movie of 2014
| Steve Gukas
|  
|-
| Best Movie (Drama)
| Steve Gukas
|  
|-
| Best Movie Director
| Steve Gukas
|  
|-
| Best Supporting Actor
| Segun Arinze
|  
|-
| Best Actress in a Drama
| Amaka Mgbor
|  
|-
| Best Actor in a Drama
| Gideon Okeke
|  
|-
| Best Cinematography
| John Demps, Harald Beeker & Manuel Lapiere
|  
|-
| Best Make-Up Artist 
| Chinwem Elevoh & Sandra Udoewah
|  
|-
| Best Sound Editing
| Susan Penington
|  
|-
| Best Video Editing
| Antonio Rui Ribeiro
|  
|-
| Best Drama Writer
| Ita Hozaife, J.K. Amalou
|  
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 