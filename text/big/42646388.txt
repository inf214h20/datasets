Chaar (film)
{{Infobox film
| name           = Chaar
| image          = Chaar_official_poster.jpg
| alt            = 
| caption        = Chaar movie poster
| director       = Sandip Ray
| producer       = Shrikant Mohta   Mahendra Soni  Nispal Singh
| writer         = Sandip Ray Parasuram  Porikkha by Sharadindu Bandyopadhyay  Kagtarua and Dui Bondhu by Satyajit Ray
| starring       = Saswata Chatterjee,  Abir Chatterjee, Koel Mallick, Paran Bandopadhyay, Sreelekha Mitra, Sudipta Chakraborty, Pijush Ganguly
| music          = Sandip Ray
| cinematography =
| editing        = Subrata Roy
| studio         = Shree Venkatesh Films   Surinder Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali anthology film, directed by Sandip Ray and produced by Shrikant Mohta, Mahendra Soni and Nispal Singh under the banner of Shree Venkatesh Films and Surinder Films. It based on four stories by different writers- Bateswarer Abodan by Rajshekhar Basu|Parasuram, Porikkha by Sharadindu Bandyopadhyay, Kagtarua and Dui Bondhu by Satyajit Ray. The film features Saswata Chatterjee,  Abir Chatterjee, Koel Mallick, Paran Bandopadhyay in the lead roles, and Sreelekha Mitra, Sudipta Chakraborty, Pijush Ganguly in the supporting roles.   

==Cast==
* Saswata Chatterjee as Sanjib Chatterjee / The Doctor
* Paran Bandopadhyay as Bateswar
* Subhrajit Dutta as Priyabrata
* Sreelekha Mitra as Anita / Kadambanila "Kadu" Chatterjee
* Pijush Ganguly
* Rajatava Dutta as Kishorilal
* Sudipta Chakraborty
* Abir Chatterjee as Binayak
* Koel Mallick as Monika Nandi

==Synopsis==
Chaar film contains four short stories by different writers: "Bateswarer Abodan" by Rajshekhar Basu|Parasuram, "Porikkha" by Sharadindu Bandyopadhyay, "Kagtarua" by Satyajit Ray and "Dui Bondhu" by Satyajit Ray.

==See also==
* Jekhane Bhooter Bhoy

==References==
 

==External links==
*   on Facebook

 
 

 
 
 


 