Our Daily Bread (1934 film)
{{Infobox film
| name           = Our Daily Bread
| image          = Ourdailybreaddvd.jpg
| image_size     =
| caption        = DVD cover for the film
| director       = King Vidor
| producer       = King Vidor
| writer         = King Vidor (story) Elizabeth Hill (scenario) Joseph L. Mankiewicz (dialogue)
| narrator       = Tom Keene Barbara Pepper Alfred Newman
| cinematography = Robert H. Planck
| editing        = Lloyd Nosler
| distributor    = United Artists
| released       =  
| runtime        = 80 min. 90 min. (American premiere) 74 min. (TCM print)
| country        = United States
| language       = English
| budget         = $125,000 (estimate)
| gross          =
}}
 1934 film Tom Keene, The Crowd (1928), using the same characters although with different actors. Vidor tried to interest Irving Thalberg of MGM in the project, but Thalberg, who had greenlighted the earlier film, rejected the idea. Vidor then produced the film himself and released it through United Artists.

The film is also known as Hells Crossroads, an American reissue title.

== Plot summary ==
The film depicts a couple, down on their luck during the Great Depression, who move to a farm to try to make a go of living off the land.  They dont have a clue at first, but soon find other people down on their luck to help them.  Soon they have a collective of people, some from the big city, who work together on a farm. There is a severe drought, killing the crops. The people then dig a ditch by hand almost two miles long to divert water from a creek to irrigate the crops. The film is an entertaining, uplifting political allegory about the virtues of collective, non-corporate action, self-sufficiency, and the rewards of hard-work rather than the rewards of rapacious finance capitalism; it is not an instructional "how-to" film from an agricultural institute; consequently, the film ends with the people celebrating wildly in the water then harvesting the crops, not showing how they managed to direct the narrow stream of water over the huge plain to evenly irrigate the crops.

==Cast==
*Karen Morley as Mary Sims Tom Keene as John Sims
*Barbara Pepper as Sally
*Addison Richards as Louie Fuente
*John Qualen as Chris Larsen
*Lloyd Ingraham as Uncle Anthony
*Sidney Bracey as Rent Collector Henry Hall as Frank - the Carpenter
*Nellie V. Nichols as Mrs. Cohen Frank Minor as Plumber
*Bud Ray as Stonemason

==Reception==
The film was a box office disappointment. 

== Soundtrack ==
* Sidney Bracey – "Just Because Youre You"
* The farmers – "Youre in the Army Now" (traditional)
* Musicians at the farm – "Camptown Races" (music by Stephen Foster)
* Tom Keene – "Oh! Susanna" with modified lyrics (music and lyrics by Stephen Foster)

==References==
 

== External links ==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 