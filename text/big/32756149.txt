His Butler's Sister
 
{{Infobox film
| name           = His Butlers Sister
| image          = His Butlers SIster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Frank Borzage
| producer       = Felix Jackson
| screenplay     = {{Plainlist|
* Samuel Hoffenstein
* Elizabeth Reinhardt
}}
| starring       = Deanna Durbin
| music          = Hans J. Salter
| cinematography = Elwood Bredell
| editing        = Ted J. Kent
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Sound Recording Pat OBrien, Akim Tamiroff, Evelyn Ankers and Hans Conreid.

==Cast==
* Deanna Durbin as Ann Carter
* Franchot Tone as Charles Gerard Pat OBrien as Martin Murphy
* Akim Tamiroff as Popoff
* Alan Mowbray as Buzz Jenkins
* Walter Catlett as Mortimer Kalb
* Elsa Janssen as Severina
* Evelyn Ankers as Elizabeth Campbell
* Frank Jenks as Emmett
* Sig Arno as Moreno
* Hans Conried as Reeves
* Florence Bates as Lady Sloughberry
* Roscoe Karns as Fields
* Iris Adrian as Sunshine Twin
* Robin Raymond as Sunshine Twin

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 