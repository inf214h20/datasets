Alice Adams (film)
{{Infobox film
| name           = Alice Adams  
| image          = Alice Adams.jpeg
| caption        = DVD Cover
| director       = George Stevens 
| producer       = Pandro S. Berman
| writer         = Dorothy Yost, Mortimer Offner, and Jane Murfin
| starring       = Katharine Hepburn Fred MacMurray Fred Stone Evelyn Venable 
| music          = Max Steiner and Roy Webb 
| cinematography = Robert De Grasse
| editing        = Jane Loring
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   In New York, the film premiered at Radio City Music Hall. }}
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $342,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p55 
| gross = $770,000 
}} Alice Adams, by Booth Tarkington. The music score was by Max Steiner and Roy Webb, and the cinematography by Robert De Grasse.
 Morning Glory; her performance in Alice Adams made her a public favorite again. 

==Plot==
Alice Adams (Katharine Hepburn) is the youngest daughter of the Adams family. Her father (Fred Stone) is an invalid who used to work in Mr. Lambs factory as a clerk.  Her mother (Ann Shoemaker) is embittered by her husbands lack of ambition and upset by the snubs her daughter endures because of their poverty. Alices older brother, Walter (Frank Albertson), is a gambler who cannot hold a job and who associates with African Americans (which, given the time period in which the film is set, is considered a major social embarrassment).  As the film begins, Alice attends a dance given by the wealthy Henrietta Lamb (Janet McLeod). She has no date, and is escorted to the occasion by Walter. Alice is a social climber like her mother, and engages in socially inappropriate behavior and conversation in an attempt to impress others. At the dance, Alice meets wealthy Arthur Russell (Fred MacMurray), who is charmed by her despite her poverty.

Alices father is employed as a clerk in a factory owned by Mr. Lamb (Charles Grapewin), who has kept Adams on salary for years despite his lengthy  illness.  Alices mother nags her husband into quitting his job and pouring his life savings into a glue factory.  Mr. Lamb ostracizes Mr. Adams from society, believing that Adams stole the glue formula from him.  Alice is the subject of cruel town gossip, which Russell ignores.

Alice invites Russell to the Adams home for a fancy meal.  She and her mother put on airs, the entire family dresses inappropriately in formal wear despite the hot summer night, and the Adamses pretend that they eat caviar and fancy, rich-tasting food all the time.  The dinner is ruined by the slovenly behavior and poor cooking skills of the maid the Adamses have hired for the occasion, Malena (Hattie McDaniel).  Mr. Adams unwittingly embarrasses Alice by exposing the many lies she has told Russell. When Walter shows up with bad financial news, Alice gently expels Russell from the house now that everything is "ruined".

Walter reveals that "a friend" has gambling debts, and that he stole $150 from Mr. Lamb to cover them. Mr. Adams decides to take out a loan against his new factory to save Walter from jail. Just then, Mr. Lamb appears at the Adams house. He accuses Adams of stealing the glue formula from him, and declares his intention to ruin Adams by building a glue factory directly across the street from the Adams plant. The men argue violently, but their friendship is saved when Alice confesses that her parents took the glue formula only so she could have a better life and some social status.  Lamb and Adams reconcile, and Lamb indicates he will not prosecute Walter.

Alice wanders out onto the porch, where Russell has been waiting for her. He confesses his love for her, despite her poverty and family problems.

== Cast ==
*Katharine Hepburn as Alice Adams
*Fred MacMurray as Arthur Russell
*Fred Stone as Mr. Adams
*Evelyn Venable as Mildred Palmer
*Frank Albertson as Walter Adams
*Ann Shoemaker as Mrs. Adams
*Charles Grapewin as Mr. Lamb
*Grady Sutton as Frank Dowling
*Hedda Hopper as Mrs. Palmer
*Hattie McDaniel as Malena
*Jonathan Hale as Mr. Palmer
*Janet McLeod as Henrietta Lamb
*Virginia Howell as Mrs. Dowling
*Zeffie Tilbury as Mrs. Dresser
*Ella McKenzie as Ella Dowling

==Academy Awards nominations== Academy Award Best Picture, Best Actress. Hepburn received the second most votes, after winner Bette Davis in Dangerous (film)|Dangerous.  

==Production==
The 1935 film of Alice Adams is the second adaptation of the Tarkington novel. A silent film version had been made in 1923, directed by Rowland V. Lee. 
 David Copperfield.  Cukor advised her to choose either William Wyler or George Stevens to direct. Although Hepburn favored the German-born and Swiss-educated Wyler, producer Pandro S. Berman favored American George Stevens. 
 Alice Adams, in significant ways. Most importantly, the novel depicts Alice estranged from Russell. The original script by Dorothy Yost and Jane Murfin ended with Alice and Russell in love.  But Stevens was so unhappy with the script and the ending that he, his friend Mortimer Offner, and Hepburn discarded most of it and rewrote it (using dialogue taken from the novel).  Their script ended with Alices relationship with Russell up in the air, and finished with a scene in which Alice goes to secretarial school. But Berman and RKO executives wanted a happy ending in which Alice gets Russell.  Stevens and Hepburn opposed this change.  Berman enlisted the aid of Cukor, who agreed that the more realistic ending would be box office poison. So the script was changed to allow Russell to fall in love with Alice and win her over. 

==Reception==
After the cinema circuits deducted their exhibition percentage of boxoffice ticket sales the film made a nice profit of $164,000. 

==References==
 

==External links==
 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 