All of a Sudden Peggy
{{infobox film
| title          = All of a Sudden Peggy
| image          = Allofasuddenpeggy-newspaperad-1920.jpg
| imagesize      =
| caption        = Newspaper advertisement. Walter Edwards
| producer       = Adolph Zukor
| writer         = Ernest Denny (play) Edith M. Kennedy
| starring       = Marguerite Clark Jack Mulhall
| music          = William Marshall
| editing        =
| distributor    = Paramount Pictures
| released       = February 1, 1920
| runtime        = 5 reels (4,448 feet)
| country        = USA Silent (English intertitles)
}} Walter Edwards and  starring Marguerite Clark and Jack Mulhall.   It was produced by Famous Players-Lasky and distributed by Paramount Pictures. It is based on a 1907 Broadway play All-of-a-Sudden-Peggy which starred the much older Henrietta Crosman.  It is Clarks third to last film. Director Edwards died in Hawaii that same year of 1920.

==Cast==
*Marguerite Clark - Peggy OHara
*Jack Mulhall - Honorable Jimmy Keppel
*Lillian Leighton - Mrs. OMara
*Maggie Fisher - Lady Crackenthorpe
*Orral Humphrey - Anthony, Lord Crackenthorpe
*Sylvia Jocelyn - Millicent Keppel
*A. Edward Sutherland - Jack Menzies
*Tom Ricketts - Major Archie Phipps
*Virginia Foltz - Mrs. Colquhoun

==References==
 

==External links==
* 
* 
* 
*  (Univ. of Washington, Sayre collection)

 
 
 
 
 
 
 
 
 


 
 