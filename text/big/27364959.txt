The Case Against Brooklyn
{{Infobox film
| name           = The Case Against Brooklyn
| image_size     = 
| image	=	The Case Against Brooklyn FilmPoster.jpeg
| caption        = Original film poster
| director       = Paul Wendkos Bernard Gordon (uncredited) Julian Zimet
| narrator       = 
| starring       = Darren McGavin
| music          = 
| cinematography = 
| editing        = 
| studio         = Columbia Pictures
| distributor    = 
| released       = June 1958
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} True Magazine article I Broke the Brooklyn Graft Scandal by crime reporter Ed Reid.  It featured depictions of American police corruption though no police officer in uniform is shown to be corrupt.

==Genre==
The film has the style of a documentary film and at first seems to be a police Procedural (genre)|procedural. It actually has film noir elements, such as a love affair between a married rookie cop and the widow of a mobster.  Park (20011), p. 141 

==Plot==
Ex-Marine Pete Harris, formerly with Military Intelligence is taken out of his Police Academy due to his experience to work undercover to investigate police corruption.

==Cast==
*Darren McGavin as Pete Harris
*Margaret Hayes as Lil Polombo (née Alexander)
*Warren Stevens as Rudi Franklin
*Peggy McCay as Mrs. Jane Harris
*Tol Avery as Dist. Atty. Michael W. Norris

==References==
*  
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 