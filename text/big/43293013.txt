The Kiss of Hate
{{infobox film
| title          = The Kiss of Hate
| image          =
| imagesize      =
| caption        =
| director       = William Nigh
| producer       = Columbia Pictures Corp.
| writer         = Fred De Gresac
| starring       = H. Cooper Cliffe Ethel Barrymore
| music
| cinematography = Arthur A. Caldwell (*as A. A. Caldwell)
| editing        =
| distributor    = Metro Pictures
| released       = April 3, 1916
| runtime        = 50 minutes
| country        = USA
| language       = Silent film..(English intertitles)
}}
The Kiss of Hate is a lost  1916 silent film drama starring Ethel Barrymore and H. Cooper Cliffe.  

The film had exclusive engagements, sometimes playing for only a day. 

==Plot==
The story takes place in Czarist Russia and concerns an anti-Semetic police commissioner, or Prefect, Count Orzoff(Cliffe) and a woman Nadia(Barrymore) who has spurned his amorous attentions . The commissioner seeks revenge against Nadia by killing her father(W. L. Abingdon). Orzoff becomes governor and Nadia plots revenge against him and is aided by a group of Jews who are also scheming against Orzoff. In further plot twists Nadia becomes romantic with Orzoffs son Sergius(Robert Elliott) and in a bit of payback stabs Sergius in retribution for her fathers murder. Sergius is only wounded and Nadia thinking she has killed him tries to commit suicide but Commissioner Orzofs guards stop her.  For the rescue of his son, Nadia somehow gets Count Orzoff to sign papers admitting to persecuting Russian Jews. Nadia is in Orzoffs mansion completing the deal, when that group of Jews set fire to the house killing Nadia, Orzoff and Sergius.

==Cast==
*Ethel Barrymore - Nadia Turgeneff
*H. Cooper Cliffe - Michael Orzoff Robert Elliott - Sergius Orzoff
*Roy Applegate - Goliath
*Niles Welch - Paul Turgeneff
*William L. Abingdon - Count Peter Turgeneff
*Victor De Linsky - Vernik
*Martin J. Faust - Nicholas
*William "Stage" Boyd - Isaac 
*Frank Montgomery - Samuels
*Ilean Hume - Leah
*Daniel Sullivan - Police Spy

==References==
 

==External links==
* 
* 
* (with rot link url as watermark)

 

 
 
 
 
 
 
 
 
 


 