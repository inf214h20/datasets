Saw V
 
{{Infobox film
| name            = Saw V
| image           = Saw V New Poster.jpg
| alt             = 
| caption         = Promotional poster
| director        = David Hackl
| producer        = Gregg Hoffman Mark Burg Oren Koules
| writer          = Patrick Melton Marcus Dunstan Scott Patterson Betsy Russell Mark Rolston Julie Benz Carlo Rota Meagan Good  
| music           = Charlie Clouser
| cinematography  = David Armstrong
| editing         = Kevin Greutert
| released        =  
| studio          = Twisted Pictures
| distributor     = Lionsgate Films
| runtime         = 92 minutes  
| country         = Canada United States
| language        = English
| budget          = $10.8 million
| gross           = $113.8 million 
}} Scott Patterson, Betsy Russell, Mark Rolston, Julie Benz, Carlo Rota, and Meagan Good. It is the fifth installment of the Saw (franchise)|Saw franchise, and was released on October 23, 2008 in Australia and October 24, 2008 in North America.

David Hackl, who served as the production designer of Saw II, Saw III, and Saw IV, and second-unit director for Saw III and Saw IV made his directorial debut with Saw V.  Patrick Melton and Marcus Dunstan, the writers of the previous film, returned to write the film. Charlie Clouser, who provided the score for all previous Saw films, also returned to compose the score for the film. Saw creators James Wan and Leigh Whannell served as executive producers.
 Jigsaw Killer, as well as his efforts to prevent anyone else from learning his secret.

==Plot== Seth Baxter, Peter Strahm Jeff Denlon Corbett Denlon to the police, claiming they are the only survivors, and is shocked when Strahm is brought out alive as well.
 Jill Tuck John Kramers Lieutenant Daniel Steven Sing, Allison Kerry, Eric Matthews; Lindsey Perez. Dan Erickson, who takes him off the case. Now suspicious of Hoffman and determined to piece together his involvement, Strahm takes case files of past Jigsaw victims and begins researching them on his own. 
 Luba (Meagan Good), a city planner; and List of Saw characters#Mallick|Mallick, an arsonist - awaken with collars connected to mounted blades locked around their necks. The keys are in glass boxes across the room, though advancing will set off a one-minute timer. A videotape advises them to "do the opposite" of their instincts throughout the tests and states that all five are connected. Mallick activates the timer and everyone reaches their keys in time except for Ashley, who is decapitated when the collars retract. Their starting room and each subsequent room  has timed nail bombs in the corners, forcing the group to move forward. The remaining four learn more about each other in the next room, and a videotape informs them that the keys to three bomb shelters are in overhead jars. Charles strikes Mallick and starts smashing the overhead jars one by one. Brit and Luba each find a key while Charles takes Mallicks, but Charles is struck from behind by Luba and left to die when the bombs explode. In the third room, five short cables must be connected to a full bathtub to unlock the door. Luba attacks Mallick, intent on using his body to close the circuits, but Brit stabs her and they use her body instead. In the fourth room, they find a machine fitted with circular saws that contains a beaker requiring ten pints of blood to open the door. Mallick notes the five armholes and they realize that every test was set up for all five victims to work together to survive. Brit also pieces together what they learned about each other and realizes that they were all connected through a building fire that killed eight people. With no other options, they saw their arms to provide the blood.
 nerve gas house. Strahm also learns that everyone was meant to die in the plant except for Corbett and Hoffman, who would appear to be a hero. Strahms activities soon draw Ericksons worry, which is fueled when Jill approaches Erickson, and claims that Strahm is stalking her. He also receives a call from Hoffman, who tells him about Strahms theory of a second accomplice, and puts a tracker on Strahms phone to locate him.

Hoffman plants the stolen phone and Ericksons personnel file in the observation room of the current game. Erickson follows the tracker to the room and finds the planted items as well as Brit, who managed to crawl out of the fourth room after Mallick passed out from blood loss. After calling for medical attention for the victims, Erickson puts out an all-points bulletin on Strahm. Strahm follows Hoffman to the renovated nerve gas house where he finds an underground room containing a clear box filled with broken glass. Hoffmans tape urges Strahm to enter the box, but he stops it short and ambushes Hoffman, sealing him in the box after a brief struggle, and causing the rooms door to seal itself. Hoffman points to the tape, which warns Strahm that if he doesnt enter the box, he will "simply vanish" and Hoffmans legacy will become his. The box is safely lowered into the floor as the walls close in on Strahm, who attempts in vain to escape through the ceiling grid, and is crushed to death.

==Cast==
 
  John Kramer Detective Mark Hoffman Agent Peter Strahm Jill Tuck Agent Dan Erickson Brit
* Mallick
* Luba Gibbs Charles
* Ashley
* Seth Baxter Corbett Denlon Detective Fisk Pamela Jenkins Angelina Hoffman Paul Stallberg Obi Tate Gus Colyard Detective Eric Matthews
 

==Production==
Saw V was written by   took place from March 17, 2008 to April 28, 2008 in  .   , Shocktillyoudrop. Retrieved July 24, 2008.  The website opened on August 6, 2008. On September 17, 2008, a new clip was available on the Saw V website, depicting the Pendulum Trap.

==Release==
The film was released in Australia on October 23, 2008,    in North America and the United Kingdom on October 24, 2008,   and in New Zealand on October 30, 2008. 

===Box office=== first film. It is Lionsgates tenth highest-grossing film in the United States and Canada. 

===Critical reception===
The film received generally negative reviews from critics. The review aggregator Rotten Tomatoes reported that 12% of critics gave the film positive reviews, making it the second poorest-reviewed film in the series, losing to the most recent Saw 3D. The Rotten Tomatoes consensus states "If its plot were as interesting as its torture devices, or its violence less painful than its performances, perhaps Saw V might not feel like it was running on fumes."  Metacritic reported the film had an average score of 20 out of 100, based on 13 reviews. 

Elizabeth Weizman of the New York   wrote that "The virtues of the individual films are almost beside the point, since its hard to imagine why anyone would want to pick up the thread at this late date, but Saw V is a particularly dull and discombobulated affair, shot and acted with all the flair of a basic-cable procedural".   

Some reviews were positive, however. The British website   awarded the film with 3 out of 5 stars stating that the film ties up most of the loose ends of the previous 4 installments while also having a more straightforward and less complicated storyline. They also praised the traps for being the most inventive and best that the Saw franchise has had to offer.   

==Home media==
===Unrated Directors Cut===
During an interview at the 2008 Scream Awards, Hackl claimed that his directors cut of Saw V (released on DVD January 20, 2009   ), would run approximately 14 minutes longer than the theatrical cut. Hackl also stated that a number of scenes in the film would be re-ordered and arranged differently than in the theatrical cut.  However, only a few changes were made and the extra footage was never released, running only four minutes longer than the theatrical version.

===Unrated Collectors Edition===
A collectors edition is available with exclusive packaging. It features sound effects, a collectors booklet, and a spinning "saw blade". The features on the DVD itself are the same as the standard Unrated Directors Cut.      

===Soundtrack===
 
The soundtrack was released on October 21, 2008 by Artists Addiction Records.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 