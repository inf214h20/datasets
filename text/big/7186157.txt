The City of Violence
 
{{Infobox film name           = The City of Violence image          = Poster for the Korean film The City of Violence.jpg caption        = Film poster director       = Ryoo Seung-wan producer       = Kim Jeong-min Ryoo Seung-wan writer         = Kim Jeong-min Lee Won-jae Ryoo Seung-wan starring       = Ryoo Seung-wan Jung Doo-hong Lee Beom-soo music          = Bang Jun-seok  cinematography = Kim Yeong-cheol  editing        = Nam Na-yeong distributor    = CJ Entertainment released       =   runtime        = 92 minutes  country        = South Korea language       = Korean budget         =   gross          =   
| film name = {{Film name hangul         =   hanja          = rr             = Jjakpae mr             = Tchakp‘ae}}
}} 2006 Cinema South Korean action film co-written and  directed by Ryoo Seung-wan, who stars in the film opposite action director and longtime collaborator Jung Doo-hong. The story re-unites three former childhood friends for their friends funeral, which prompts two vowing to find his murderer.    

==Plot==
Ex-gangster Wang-jae (Ahn Gil-kang) chases a gang of punks into an alley where hes fatally stabbed. His four childhood friends reunite, first time in nearly twenty years, at Wang-jaes funeral.

Up to then, each has gone their own way: Tae-su (Jung Doo-hong) became a Seoul police detective. Pil-hos (Lee Beom-soo) taken over his brother-in-law Wang-jaes throne as a mobster. Seok-hwan (City of Violence director Ryoo Seung-wan) who works as a debt collector while his older brother Dong-hwan struggles as a math teacher. A flashback reveals a pact they made on School Picnic Day before they fought with other youths. After the funeral, Tae-su decides to investigate the murder within a week before he would return to his job in Seoul. Meanwhile, Seok-hwan decides to find and kill Wang-jaes murderers.

While investigating, Tae-su is attacked by youth gangs, who use an array of weapons including baseball bats, hip hop, bikes, hockey sticks, and yo-yos. Tae-su barely escapes with his life after Seok-hwans unexpected arrival. They decide to work together. After hunting the gangs, they discover Wang-jaes death isnt a random mindless attack. It was a planned murder. The revelation leads them to Seok-hwans own brother, who confesses a secret. Its Pil-ho who was behind the plan, which was hatched after Wang-jae disapproved Pil-hos plans to turn their city into a tourist district.

After strangers tried to kill him as part of tying up Pil-hos loose ends, Wang-jaes young murderer agrees to testify against Pil-ho. A killer douses the young murderer in gasoline and sets him on fire. When Tae-su realizes theres no legal way to take Pil-ho down, he confronts Pil-ho, but he ends up badly beaten. Meanwhile Seok-hwan, Dong-hwan and their mother are on their way to a restaurant when a truck smashes into their car. After Dong-hwan and his mothers funeral, Seok-hwan and Wang-jaes widow leave the funeral house and sees Tae-su waiting outside. Tae-su persuades Wang-jaes widow into revealing information on her brother Pil-hos whereabouts.

No longer bound by law, Tae-su and Seok-hwan storm Pil-hos fortress where they fight their way through swarms of armed cooks and bodyguards until the banquet room. They witness Pil-ho killing a Seoul president, which prompts all guests to leave just Tae-su, Seok-hwan, Pil-ho and his four elite guards alone in the room. The elite guards immediately take Tae-su and Seok-hwan on.

Two men, victorious but exhausted, set to take on Pil-ho, but Pil-ho takes them by surprise by attacking Seok-hwan, who loses his fingers. Pil-ho turns and stabs Tae-sus stomach, ignoring Seok-hwan whos binding the katana to his hand with torn table cloth. Tae-su informs Pil-ho that the last man who stands last wins. Before Pil-ho could react, Seok-hwan stabs him through the chest, killing him.

As Tae-su bleeds to death, he recalls the day he and his childhood friends walk home from the School Picnic, talking about future. Seok-hwan says they didnt win, but his older brother Dong-hwan says they did. Seok-hwan insists they have nothing to show for it. Wang-jae disagrees, pointing out they have the snake tonic. They will drink it in twenty years time when they become rich men. One wonder what would happen if it didnt work out. "Doesnt matter," Wang-jae says. "We wont amount to much, anyway!"

Back at Pil-hos place, the exhausted Seok-hwan glances around, noting the carnage he and his late friend Tae-su had created, and sighs heavily. Finally he says, "Fuck it."

==Cast==
*Ryoo Seung-wan ... Seok-hwan 
*Jung Doo-hong ... Tae-su 
*Lee Beom-soo ... Pil-ho 
*Ahn Gil-kang ... Wang-jae
*Kim Shi-hoo ... young Seok-hwan
*On Joo-wan ... young Tae-su
*Kim Dong-young ... young Pil-ho
*Jung Woo ... young Wang-jae
*Jung Suk-yong ... Dong-hwan
*Kim Seo-hyung ... Mi-ran
*Kim Byung-ok ... Youth president
*Lee Joo-shil ... Seok-hwans mother
*Jo Deok-hyun ... Boss Jo
*Kim Gi-cheon ... Sal-soo
*Kim Kkot-bi ... high school girl with razor blade
*Kim Su-hyeon ... Seoul detective
*Im Jun-il ... Team leader Im
*Lee Na-ri ... Miss Bae
*Park Young-seo ... young Dong-hwan 
*Park Ji-hwan ... teen gang boss
*Lee Hong-pyo ... Onsung area cop
*Oh Joo-hee ... hanbok-wearing woman in special room 2
*Kim Hyo-sun ... secretary

==Awards and nominations==
2006 Chunsa Film Art Awards
* Best Supporting Actor - Lee Beom-soo

2006 Busan Film Critics Awards 
* Best Cinematography - Kim Yeong-cheol

2006 Blue Dragon Film Awards
* Nomination - Best Supporting Actor - Lee Beom-soo

2006 Korean Film Awards
* Best Supporting Actor - Lee Beom-soo
* Nomination - Best Editing - Nam Na-yeong
* Nomination - Best Music - Bang Jun-seok
* Nomination - Best Sound

2007 Grand Bell Awards
* Nomination - Best New Director - Ryoo Seung-wan
* Nomination - Best Supporting Actor - Lee Beom-soo
* Nomination - Best Cinematography - Kim Yeong-cheol
* Nomination - Best Editing - Nam Na-yeong

== See also ==
List of Dragon Dynasty releases

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 