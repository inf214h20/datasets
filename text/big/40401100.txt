God's Not Dead (film)
 
 
{{Infobox film
| name           = Gods Not Dead
| image          = Gods Not Dead.jpg
| caption        = Theatrical released poster
| director       = Harold Cronk
| producer       = Michael Scott Russell Wolfe Anna Zielinski
| writer         = Cary Solomon Chuck Konzelman
| based on       =  
| starring       = Kevin Sorbo Shane Harper David A. R. White Dean Cain Newsboys Willie Robertson Korie Robertson
| music          = Will Musser Newsboys
| cinematography = Brian Shanley
| editing        = Vance Null
| studio         = Pure Flix Entertainment Red Entertainment Group
| distributor    = Pure Flix Entertainment
| released       =  
| runtime        = 113 minutes 
| country        = United States
| language       = English
| budget         = $2 million 
| gross          = $62,630,732   
}}
 Christian drama evangelical Christian college student (Shane Harper) whose faith is challenged by a philosophy professor who declares God a pre-scientific fiction.

==Plot==
Josh Wheaton (Shane Harper), an evangelical college student, enrolls in a philosophy class taught by Professor Jeffrey Radisson (Kevin Sorbo), an atheism|atheist, who demands that his students sign a declaration that "God is dead" to get a passing grade. Josh is the only student in the class who refuses to sign. Radisson requires Josh to debate the topic with him but agrees to let the class members decide who wins.

Radisson gives Josh twenty minutes at the end of the first three lecture sessions to argue that God exists. In the first two debates, Radisson has counter arguments for all of Joshs points. Joshs girlfriend Kara (Cassidy Gifford) demands Josh either sign the statement, "God is dead," or drop Radissons class, because standing up to Radisson will jeopardize their academic future. Kara breaks up with Josh for insisting on confessing his belief in God. Ultimately, it comes down to the third and final debate between Radisson and Josh, who again both make compelling points. Josh then halts his line of debate to pose a question to Radisson: "Why do you hate God?" After Josh repeats the question twice more, Radisson explodes in rage, confirming he hates God for his mothers death that left him alone despite his prayers. Josh then casually asks Radisson how he can hate someone that does not exist. In the end, Martin (Paul Kwo), a student from China whose father had forbidden him from even talking about God to avoid jeopardizing Martins brothers chance at overseas study, stands up and says, "Gods not dead." Almost the entire class follows Martins lead, causing Radisson to leave the room in defeat.

Against the backdrop of the debates, a series of peripherally related subplots develop. Radisson dates Mina (Cory Oliver), an evangelical whom he belittles in front of his fellow atheist colleagues. Her brother Mark (Dean Cain), a successful businessman and atheist, refuses to visit their mother, who suffers from dementia. Marks girlfriend, Amy (Trisha LaFache), is a left-wing blogger who writes articles critical of Duck Dynasty. When she is diagnosed with cancer, Mark dumps her. A Muslim student named Ayisha (Hadeel Sittu) secretly converts to Christianity and is disowned by her infuriated father when he finds out.

After the final debate, Josh invites Martin to attend the Newsboys concert that is in town. Radisson reads a letter from his late mother, and is moved to reconcile with Mina. Amy confronts the Newsboys in their dressing room, only to admit that she wants to get to know God. While on his way to find Mina, Radisson is struck by a car and fatally injured. Reverend Dave (David A. R. White) is at the intersection and guides Radissons dying acceptance of Christ as savior. Mark at last visits his mother, only to taunt her; she responds that all of his financial success was given to him by Satan to keep him from turning to God.

As the film ends, the Newsboys play a video clip of Willie Robertson congratulating Josh. The Newsboys then play their song "Gods Not Dead", dedicating it to Josh. 

==Cast==
 
* Shane Harper as Josh Wheaton
* Kevin Sorbo as Professor Jeffery Radisson
* David A. R. White as Reverend Dave
* Trisha LaFache as Amy Ryan
* Hadeel Sittu as Ayisha 
* Marco Khan as Misrab
* Cory Oliver as Mina
* Dean Cain as Mark
* Jim Gleason as Ward Wheaton
* Benjamin Onyango as Reverend Jude 
* Cassidy Gifford as Kara
* Paul Kwo as Martin Yip
 

The film also includes Cameo appearances by the Christian pop-rock band Newsboys and by Willie Robertson and his wife, Korie, of Duck Dynasty fame.

==Production==
The film was shot from October to November 2012, in Baton Rouge, Louisiana, with the concert scene done in Houston, Texas. 

Russell Wolfe, the CEO of Pure Flix Entertainment, stated that:  

==Reception==

===Critical reception===
The film has been panned by critics, currently holding a score of 16 out of 100 on Metacritic indicating "overwhelming dislike", based on six critics,  and a rating of 17% on Rotten Tomatoes based on 18 reviews  . 

Writing for The A.V. Club, Todd VanDerWerff gave the film a D-, saying "Even by the rather lax standards of the Christian film industry, Gods Not Dead is a disaster. Its an uninspired amble past a variety of Christian-email-forward bogeymen that feels far too long at just 113 minutes".  Reviewer Scott Foundas of Variety (magazine)|Variety wrote "...even grading on a generous curve, this strident melodrama about the insidious efforts of Americas university system to silence true believers on campus is about as subtle as a stack of Bibles falling on your head...."  Steve Pulaski of Influx Magazine, however, was less critical of the film, giving it a C+ and stating "Gods Not Dead has issues, many of them easy to spot and heavily distracting. However, its surprisingly effective in terms of message, acting, and insight, which are three fields Christian cinema seems to struggle with the most". 

A number of sources have cited the films similarities to a popular urban legend. The basic premise of an evangelical student debating an atheist professor and winning in front of the class (who then applauds) has been the subject of a popular Chick tract.   

===Evangelical and Roman Catholic response===
The Alliance Defending Freedom, American Heritage Girls, Faith Driven Consumer, Denison Forum on Truth and Culture, Trevecca Nazarene University, The Dove Foundation and Ratio Christi have all endorsed the film.   

 
 Catholic World Report gave it his highest rating of five reels, calling the film "a tremendously entertaining film that leads to God, not in addition to its quality but through its quality."   Vincent Funaro of The Christian Post praised the film for being "a hit for believers and may even appeal to skeptics searching for answers." 

Evangelical Michael Gerson, however, was highly critical of the film and its message, writing "The main problem with God’s Not Dead is not its cosmology or ethics but its anthropology. It assumes that human beings are made out of cardboard. Academics are arrogant and cruel. Liberal bloggers are preening and snarky. Unbelievers disbelieve because of personal demons. It is characterization by caricature."  John Mulderig echoed similar concerns in his review for the Catholic News Service, stating: "There might be the kernel of an intriguing documentary buried within director Harold Cronks stacked-deck drama, given the extent of real-life academic hostility toward religion. But even faith-filled moviegoers will sense the claustrophobia of the echo chamber within which this largely unrealistic picture unfolds." 

On the other side of the spectrum, the young Earth creationist apologetics ministry, Answers in Genesis, would not endorse the film because of the promotion of several elements which they deemed to be "unbiblical." 

===Commercial performance===
Although critically panned, the film has met with significant success at the box office. In its first weekend of release, the film earned $8.6 million domestically from 780 theaters, causing Entertainment Weeklys Adam Markovitz to refer to it as "The biggest surprise of the weekend". 

The film began its international roll-out in Mexico on April 4, 2014. The movie grossed $89,021 its opening weekend. As of June 18, 2014, the movie has a worldwide total of $62 million, against the $2 million budget. 

==Sequel==
It has been announced that Pure Flix Entertainment is planning to produce a sequel, Gods Not Dead 2. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 