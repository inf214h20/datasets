Oorige Upakari
{{Infobox film 
| name           = Oorige Upakari
| image          =  
| caption        = 
| director       = Joe Simon
| producer       = A. Radhakrishna Raju V. L. Srinivasa Murthy
| story          = Ajantha Combines
| writer         = Kunigal Nagabhushan (dialogues)
| screenplay     = Joe Simon Vishnuvardhan Padmapriya Vajramuni Dheerendra Gopal
| music          = Chellapilla Satyam
| cinematography = H. G. Raju
| editing        = P. Venkateshwara Rao
| studio         = Premalaya Films
| distributor    = Premalaya Films
| released       =  
| runtime        = 149 min
| country        = India Kannada
}}
 1982 Cinema Indian Kannada Kannada film, directed by Joe Simon and produced by A. Radhakrishna Raju and V. L. Srinivasa Murthy. The film stars Vishnuvardhan (actor)|Vishnuvardhan, Padmapriya, Vajramuni and Dheerendra Gopal in lead roles. The film had musical score by Chellapilla Satyam.  

==Cast==
  Vishnuvardhan
*Padmapriya
*Vajramuni
*Dheerendra Gopal
*Musuri Krishnamurthy
*Chethan Ramarao
*Sadashiva Brahmavar
*Rajanand
*Prabhakar
*Rajasulochana
*K. Vijaya
*Uma Shivakumar
*Ashakiran
*Mangala Bhushan
*Rathnamma
*Shanthamma
*Master Naveen
*Dwarakish in Guest Appearance
*Jyothi Lakshmi in Guest Appearance
 

==Soundtrack==
The music was composed by Satyam.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Snehadali Sandiside || S. Janaki|Janaki, S. P. Balasubrahmanyam || R. N. Jayagopal ||
|-
| 2 || Hasiru Hasiru Bhoomiyalella || S. Janaki|Janaki, S. P. Balasubrahmanyam || R. N. Jayagopal ||
|- Janaki || Chi. Udaya Shankar || 
|-
| 4 || Chandamundaranu || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 