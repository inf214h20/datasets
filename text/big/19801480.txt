Det stod i avisen
 
{{Infobox film
| name           = Det stod i avisen
| image          = 
| caption        = 
| director       = Peer Guldbrandsen
| producer       = Peer Guldbrandsen
| writer         = Peer Guldbrandsen
| starring       = Henning Moritzen
| music          = Sven Gyldmark
| cinematography = Karl Andersson
| editing        = Jon Branner
| studio         = Saga Studios
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Det stod i avisen is a 1962 Danish family film directed by Peer Guldbrandsen and starring Henning Moritzen.

==Cast==
* Henning Moritzen - Johan Jespersen
* Hanne Borchsenius - Kitty
* Susse Wold - Johans forlovede
* Ebbe Langberg - Søren
* Asbjørn Andersen - Manden, der har 19 års bryllupsdag
* Karin Nellemose - Kvinde, der udlejer værelse til Søren
* Ove Sprogøe - Læge S. Gregersen
* Astrid Villaume - Grete Gregersen
* Axel Strøbye - Peter
* Poul Reichhardt - Ingrids forlovede
* Lily Broberg - Ingrid
* Ebbe Rode - Poul
* Berthe Qvistgaard - Pouls hustru
* Tove Maës - Kvinde med barnevogn til salg
* Karen Lykkehus - Hustruen, der har 19 års bryllupsdag
* Johannes Meyer - Direktøren
* Peer Guldbrandsen - Fortælleren (voice) (uncredited)

==External links==
* 

 
 
 
 
 
 