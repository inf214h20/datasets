Ezhunirangal
{{Infobox film 
| name           = Ezhunirangal
| image          = Ezhunirangal.jpg
| image_size     =
| caption        =
| director       = Jesey
| producer       = MO Joseph
| writer         = Pulimoottil Sankaranarayanan Mani Muhammed (dialogues)
| screenplay     = Mani Muhammed Jose Vidhubala Sukumari Sankaradi
| music          = G. Devarajan
| cinematography = Vipin Das
| editing        = MS Mani
| studio         = Manjilas
| distributor    = Manjilas
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by Jesey and produced by MO Joseph. The film stars Jose (actor)|Jose, Vidhubala, Sukumari and Sankaradi in lead roles. The film had musical score by G. Devarajan.   

==Cast== Jose as Balachandran
*Vidhubala
*Sukumari
*Sankaradi Shubha
*Bahadoor as Mathachan
*Janardanan Meena
*Paravoor Bharathan
*Veeran

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ezhunirangal  || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Indrachaapam Nabhassil || P. Madhuri, Chorus || P. Bhaskaran || 
|-
| 3 || Ithranaal ithranaal || K. J. Yesudas || P. Bhaskaran || 
|-
| 4 || Tharivala chirikkunna || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 