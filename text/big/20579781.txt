Insaan Aur Shaitan
{{Infobox film
| name           = Insaan Aur Shaitan
| image          = 
| image_size     = 
| caption        = 
| director       = Aspi Irani
| producer       = 
| writer         =
| narrator       = 
| starring       =Sanjeev Kumar and Aruna Irani
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood action film directed by Aspi Irani. The film stars Sanjeev Kumar and Aruna Irani.

==Cast==
*Sanjeev Kumar   
*Faryal   
*Iftekhar   
*Sheikh Mukhtar   
*Gajanan Jagirdar   
*Aruna Irani   
*Hiralal   
*M.B. Shetty   
*Chaman Puri

==Soundtrack==
{| class="wikitable"
! Serial !! Song Title !! Singer(s)
|- 1 || "Chalte Chalo" || Mohammed Rafi
|- 2 || "Humne Jo Tumko" || Asha Bhosle
|- 3 || "Koyi Aaye Baahon Mein" || Asha Bhosle
|- 4 || "Teri Ada" || Asha Bhosle
|-
|}
 

==References==
 

==External links==
*  

 
 
 
 
 


 
 