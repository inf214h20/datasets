Absurda
 
{{Infobox film
| name           = Absurda
| image          = 
| alt            = 
| caption        = 
| director       = David Lynch
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 2 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Absurda is a David Lynch film shown at the 2007 Cannes Film Festival.  The film is approximately two and a half minutes long.  Employing dream-like imagery, the visuals are stationary, showing a theater and the screen on which nightmarish images are projected.         Fest Poster Boy, WKW, Set for Cannes Close-Up; New Market Faces; IETFF|url=http://www.indiewire.com/article/cannes_07_daily_dispatch_fest_poster_boy_wkw_set_for_cannes_close-up_new_ma|publisher=indiewire.com|accessdate=16 December 2013}}       

== Synopsis ==
A group of four teens walk into the theater, thinking they are going to see a dance. A giant pair of scissors are sticking out of the screen and the teens start to talk to the man in the projection room. The screen starts showing one teen, Cindy, dancing in a tutu and another teen, Tom, with a bloody face. Tom begins to say he feels strange and begins to look at Cindy in a weird way. A scream and the words "No, Tom, no" are audible as smoke consumes most of the theater. In the smoke, the audience can see Cindy dancing in a tutu while she says, "So, I went dancing. Ive always loved to dance."

The film is stationary in the sense the camera stays in one spot, showing the theater and the screen. The group of teens in the film are not seen, but they are heard.

==References==
 

==External links==
*  

 

 
 
 
 
 


 