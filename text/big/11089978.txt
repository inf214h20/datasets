Along the Great Divide
{{Infobox film
| name           = Along the Great Divide
| image          = AlongtheGreatDivide.jpg
| image size     =
| caption        = Poster
| director       = Raoul Walsh 
Oren W. Haglund (assistant director)
| producer       = Anthony Veiller
| writer         = Walter Doniger Lewis Meltzer
| narrator       =
| starring       = Kirk Douglas Virginia Mayo John Agar Walter Brennan
| music          = David Buttolph
| cinematography = Sidney Hickox
| editing        = Thomas Reilly
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.4 million (US rentals) 
}}
Along the Great Divide  is a 1951 American western film directed by Raoul Walsh and starring Kirk Douglas, Virginia Mayo, John Agar, and Walter Brennan.

==Plot==
Federal marshal Len Merrick (Douglas) and his two deputies rescue cattle rustler and murder suspect Tim "Pop" Keith (Brennan) from a lynch mob headed by grieving rancher Ned Roden (Morris Ankrum), whose beloved son was shot in the back. Merrick insists on taking Keith to Santa Loma to stand trial. 
 James Anderson), to gather his ranch hands while he attends to the burial. Merrick offers to help, but is met with implacable hostility. After Roden leaves, Merrick finds a pocket watch.

Keith suggests that they spend the night at his home, as it is nearby. Merrick accepts, but has cause to regret his decision when Keiths daughter Ann (Mayo) ambushes them. Fortunately, Merrick is able to disarm her with no harm done. When they leave, Ann decides to go with them. 

After he is warned of Rodens intentions by fellow ranchers, Merrick decides to take an unexpected desert route, where he can see if he is being trailed. The ploy fails, however, and the party is overtaken by Roden and his men. In the ensuing gunfight, Merricks best friend and deputy, Billy Shear (Agar), is wounded. Merrick forces Roden to go away by capturing his son Dan. As they travel on, Billy dies. 

Merrick and Ann start falling in love. The marshal reveals that his unswerving devotion to duty is because the one time he neglected it, it cost his father his life. He was a deputy to his marshal father, and refused to help escort two prisoners. All three were lynched. Ann sympathizes, but warns him that her first loyalty is to her father.

Meanwhile, Dan convinces the remaining deputy, Lou Gray (Ray Teal), to help him escape by the bribe of a ranch. When the group reaches a waterhole, only to find the water undrinkable, a disagreement breaks out. All but Merrick want to head to a river half a day to the south. Worried because the river is on the Mexican border, Merrick insists on continuing on to Santa Loma. Gray quickly draws his gun, but Merrick is faster on the draw and shoots it out of his hand. Now, he has three prisoners.

After two days without sleep, an exhausted Merrick drops from his horse. Keith grabs his gun, but is unwilling to shoot. When Gray goes for his rifle, Keith kills him, then hands the gun back to Merrick.

Keith is tried in Santa Loma. Merrick tells the jury that he is sure Keith is not a killer, but all the evidence and witnesses are against him, and a guilty verdict is reached. Just before Keith is to be hanged, Merrick notices that the watch he found has an inscription to Dan. Confronted with the proof that he killed his own brother, Dan draws his revolver and grabs Ann as a shield. When his father approaches, Dan kills him and tries to flee on horseback, but is shot in the back, just like his brother, by Merrick.

==Cast==
*Kirk Douglas as Marshal Len Merrick
*Virginia Mayo as Ann Keith
*John Agar as Deputy Billy Shear
*Walter Brennan as Tim "Pop" Keith
*Ray Teal as Deputy Lou Gray
*Hugh Sanders as Sam Weaver
*Morris Ankrum as Ned Roden James Anderson as Dan Roden Charles Meredith as Judge Marlowe Sam Ash as Defense counsel
*Lane Chandler as Sheriff
*Zon Murray as Jake Wilson
*Guy Wilkerson as Jury foreman

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 