Sinister 2
{{Infobox film
| name           = Sinister 2
| image          = Sinister2Poster.jpg
| alt            = 
| caption        = 
| director       = Ciaran Foy
| producer       = Jason Blum   Scott Derrickson   Brian Kavanaugh-Jones
| writer         = Scott Derrickson   C. Robert Cargill
| starring       = James Ransone   Shannyn Sossamon
| music          = 
| cinematography = Amy Vincent
| editing        = Ken Blackwell
| studio         = eOne Entertainment Automatik Entertainment Blumhouse Productions
| distributor    = Focus Features
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Sinister 2 is an upcoming American supernatural horror film directed by Ciaran Foy and written by Scott Derrickson and C. Robert Cargill. It is a sequel to 2012 film Sinister (film)|Sinister. It stars Shannyn Sossamon and James Ransone reprising his role from the original film.

== Plot ==
A young mother and her twin sons move into a rural house thats marked for death.

== Cast ==
*Shannyn Sossamon as Courtney
*James Ransone as Deputy So & So
*Dartanian Sloan as Zach
*Robert Daniel Sloan as Dylan
*Jaden Klein as Ted
*Lucas Jade Zumann as Milo
*Laila Haley as Emma
*Olivia Rainey as Catherine
*Caden M. Fritz as Peter
*Lea Coco as Clint
*Nicholas King as Bughuul
*Tate Ellington as Dr. Stomberg

== Production ==
A sequel was announced to be in the works in March 2013, with Scott Derrickson in talks to co-pen the script with C. Robert Cargill, but not to direct, as Derrickson did on the first film.  On 17 April 2014, it was announced that Ciaran Foy will direct the film, Brian Kavanaugh-Jones, Charles Layton, Xavier Marchand and Patrice Théroux will executive produce the sequel with eOne Entertainment.  The film is set for release on 21 August 2015.

=== Filming === Grant Park. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 