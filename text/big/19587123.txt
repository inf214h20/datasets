The Last Confederate: The Story of Robert Adams
 
{{Infobox film
| name           = The Last Confederate: The Story of Robert Adams
| image          = The_Last_Confederate_poster.jpg
| caption        = Poster for The Last Confederate: The Story of Robert Adams
| director       = A. Blaine Miller Weston Adams Julian Adams Billy Fox co-producers Robert Filion Steve Purcell Weston Adams Weston Adams Weston Adams Eric Holloway Gwendolyn Edwards Joshua Lindsey
| music          = Atli Örvarsson
| cinematography = Shawn Lewallen
| editing        = Billy Fox Steve Purcell
| studio         = Solar Filmworks
| distributor    = ThinkFilm
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English 
}}
  Confederate Captain Robert Adams II.  It was released in 2007 by ThinkFilm, and garnered 10 awards on the film festival circuit.  The film was produced by Weston Adams (diplomat), Julian Adams and Billy Fox.   

== Plot ==
The love story of Confederate Captain Robert Adams II and northern girl, Eveline McCord.  The story begins with Captain Adamss falling in love with Eveline, a governess from Pennsylvania who moved to South Carolina to work for Roberts first cousin, Governor James Hopkins Adams.  As they fall in love in antebellum South Carolina, Captain Adams is launched into the outbreak of the American Civil War, despite the protests of Eveline.  The film follows Adams through battles in Virginia, and his ultimate capture and subsequent imprisonment in the Federal Military Prison in Elmira, New York.  The film was written and produced by the descendants of Robert and Eveline.

== Cast ==
 
* Julian Adams as Robert Adams
* Mickey Rooney as David McCord
* Weston Adams (ambassador) as Grandfather Adams
* Tippi Hedren as Mrs. Adams
* Amy Redford as Sylvia McCord 
* Gwendolyn Edwards as Eveline McCord Adams
* Eric Holloway as Benjamin Young
* Joshua Lindsey as Nelson McCord
 

== Release ==

=== Box office performance ===
The film was given a limited release, opening in theaters in Los Angeles, Nashville, Houston, Columbia, S.C, Charleston, S.C. and other cities.

=== Critical response ===
The film garnered ten awards at film festivals throughout the country.

== Home media ==
The Last Confederate: The Story of Robert Adams was released on DVD in 2007, courtesy of ThinkFilm.    Bonus features include a featurette, outtakes, and deleted scenes.

== Accolades ==
* Breckenridge Festival of Film Award for Best of the Fest Drama — Winner 
* Great Lakes Film Festival Best Narrative Feature Award — Winner 
* Great Lakes Film Festival Best Actor Award (Julian Adams) — Winner Long Island International Film Expo Jury Award — Winner 
* Park City Film Music Festival Audience Choice Award — Winner
* Solstice Film Festival Best Cinematography Award — Winner
* Tahoe Reno International Film Festival Best New American Film Award — Winner
* Westwood Film Festival Sony Like No Other Award — Winner    

== References ==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 