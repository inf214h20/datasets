Mofturi 1900
{{Infobox film
| name           = Mofturi 1900
| image          = Mofturi_1900_Poster.jpg
| image_size     = 
| alt            = The picture of the poster of the 1964 Romanian film, Mofturi 1900
| caption        = Original movie poster
| director       = Jean Georgescu
| producer       = 
| screenplay     = Jean Georgescu
| based on       =  
| starring       = {{ubl|
*Iurie Darie
*Ion Dichiseanu
*Grigore Vasiliu Birlic}}
| music          = Gheorghe Mărăi
| cinematography = Ion Cosma
| editing        = 
| studio         = Cinematographic Studio of Bucharest
| distributor    = 
| released       = 1964
| runtime        = 78 minutes
| country        = Romania
| language       = Romanian
| budget         = 
| gross          =
}} comedic film directed by Jean Georgescu and based on Ion Luca Caragiales many Moments and Sketches. The sketches used are "Diplomatie" ("Diplomacy"), "Amicii" ("Pals"), "O lacuna" ("A gap"), "Bubico", "C.F.R." (abbreviation of "Romanian Railways") and "Din statiune" ("From resort").  The film stars Grigore Vasiliu Birlic, Iurie Darie and Ion Dichiseanu; and features Ioana Bulcă, Alexandru Giugaru, Mircea Crisan, Geo Barton and others. Set at the beginning of the 20th century, the story revolves around two Mitică characters sitting at a cafe telling stories to each other.

==Reception==

The movie is one of the best Caragiale movie and quickly made its way to becoming a classic Romanian film. Prof. D.I.Suchianu said: "A Caragiale movie is one where the images explain the speech, not the other way around. A new genre entirely, I could even say an international premiere! Its Jean Georgescu great merit to bring upon the world this new born,  >."

"The most experienced romanian director to creating Caragiale moviessaid in his fascinating  >:  > Id like to mention that Jean Georgescu harnessed successfully this special trait of the author" commented Calin Caliman.

==External links==
* 

 
 
 
 
 
 
 

 