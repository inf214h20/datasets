Troublesome Night 9
 
 
{{Infobox film
| name = Troublesome Night 9
| image =
| caption =
| film name = {{Film name| traditional = 陰陽路九之命轉乾坤
| simplified = 阴阳路九之命转乾坤
| pinyin = Yīn Yáng Lù Jǐu Zhī Mìng Zhuǎn Qián Kūn
| jyutping = Jam1 Joeng4 Lou6 Gau2 Zi1 Meng6 Zyun2 Kin4 Kwan1}}
| director = Ivan Lai
| producer = Nam Yin
| writer = Leung Po-on Rex Hon
| starring = 
| music = Mak Jan-hung
| cinematography = Paul Yip
| editing = Eric Cheung
| studio = Nam Yin Production Co., Ltd. East Entertainment Limited B&S Limited
| distributor = B&S Films Distribution Company Limited
| released =  
| runtime =  90 minutes
| country = Hong Kong
| language = Cantonese
| budget =
| gross = HK$31,310
}}
Troublesome Night 9 is a 2001 Hong Kong horror comedy film produced by Nam Yin and directed by Ivan Lai. It is the ninth of the 19 films in the Troublesome Night film series.

==Plot==
Ms Liu, a compulsive gambler, meets Mrs Bud Lungs son, Bud Pit, on a cruise ship. He tries to help her win but his girlfriend Moon is unhappy, because Liu neglects her dying grandmother due to her gambling addiction. Liu visits the Bud family for fortune-telling and help with winning, but Mrs Bud Lung gives her other alternatives instead. She is told to go to her parents graves to clear weeds, but she puts the rubbish on nearby graves. The spirits of the dead are angered and they haunt her.

==Cast==
* Simon Lui as Bud Pit
* Law Lan as Mrs Bud Lung
* Maggie Cheung Ho-yee as Ms Liu
* Halina Tam as Moon
* Kau Man-lung as Kau
* Kwai Chung as Baat
* Cheng Chu-fung as Cruise manager
* Sherming Yiu as Cruise gambler
* Wayne Lai as Cruise gambler
* Tong Ka-fai as Bud Gay
* Jameson Lam as Gambler in dream
* Ho Chung-wai as Jason
* Hui Pik-kei as Nurse
* Tung Nam-fei
* Siu Hung

==External links==
*  
*  

 

 
 
 
 
 
 
 


 
 