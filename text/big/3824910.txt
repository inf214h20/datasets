Bunshinsaba
 
 
{{Infobox film
| name     = Bunshinsaba
| image    = Bunshinsaba movie poster.jpg
| caption  = Poster for Bunshinsaba (2004)
| film name      = {{Film name
 | hangul   =  
 | rr       = Bunshinsaba
 | mr       = Punsinsaba}}
| director = Ahn Byeong-ki
| producer = Kim Yong-dae
| writer   = Ahn Byeong-ki Kim Gyu-ri Lee Se-eun Lee Yoo-ri Choi Seong-min Choi Jung-yoon
| music    = Lee Sang-ho
| cinematography = Kim Dong-cheon
| editing  = Park Sun-deok Buena Vista
| released =  
| runtime  = 92 minutes
| country  = South Korea
| language = Korean
| budget   = 
}}
Bunshinsaba ( ) is a 2004 South Korean horror film directed by Ahn Byeong-ki.

In 2004, it screened at the 8th annual Puchon International Fantastic Film Festival. The film had its American premiere at the 2005 New York Korean Film Festival, and was shown later that year at the 5th annual Screamfest Horror Film Festival.

In 2012 Ahn Byeong-ki directed a Chinese film also titled Bunshinsaba (2012 film)|Bunshinsaba but with an unrelated plot. 

==Plot==
Lee Yoo-jin is a transfer student from Seoul, and along with two of her friends, she is constantly being bullied by a group of classmates. One night, Yoo-jin and her friends decide to place a curse on their enemies by creating a Ouija board on which they write the names of the female bullies. Using the Bunshinsaba curse, her friend warns the others not to open their eyes until the spell is finished. The calling takes effect, and Yoo-jin, somewhat curious, opens her eyes. To her shock and horror, she sees an image of a pale-like dead girl with long hair beside her.
 possessing her. She was the one who killed all of those bullies, even though she doesnt remember doing it.

Eun-ju also senses a terrible force and unearthly presence surrounding Yoo-jin. Mr. Han, Yoo-jins class adviser, decides to help out by consulting his friend on what is causing her to act strangely. Through hypnotism, they are able to see a vision of the past showing how Kim In-sook and her mother Chun-hee were brutally killed by the villagers, and before dying, they placed a curse that for generations to come, whoever left the village would die. As Chun-hee finally takes possession of Eun-jus body, she exacts punishment on the people who wronged them, slaying the schools principal but sparing Mr. Hans life.

Not long after, Eun-ju gives birth to a girl and within that girls body is the spirit of Kim In-sook.

==Cast== Kim Gyu-ri as Lee Eun-ju
*Lee Yoo-ri as Kim In-sook 
*Lee Se-eun as Lee Yoo-jin
*Choi Seong-min as Han Jae-hoon/Mr. Han
*Choi Jung-yoon as Ho-kyung

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 