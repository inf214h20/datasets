The Lawless Nineties
{{Infobox Film 
| name           = The Lawless Nineties
| image          = The Lawless Nineties 1936 poster.jpg
| caption        = Film poster
| director       = Joseph Kane
| producer       = Trem Carr
| writer         = {{plainlist|
*Scott Pembroke
*Joseph F. Poland
}} George Hayes Arthur Kay William Nobles
| editing        = Joseph Kane
| distributor    = Republic Pictures
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
}} Western film George Hayes.

==Plot== Harry Woods) George Hayes), his daughter Janet (Ann Rutherford) and their servant Moses (Fred Toones). 
 editor and publisher of the local newspaper the Crocket City Blade, and when he announces plans to use the power of the press to fight lawlessness and aid the statehood cause, he is threatened by villain Charles Plummer and subsequently shot and murdered by one of his men in a staged fight. 

When Plummers henchmen eventually kill Bridger, after learning of his status as a government agent, Tipton fights on. He sends fake telegrams that trap some of Plummers men. Then he organizes the ranchers and on election day they descend on the town barricaded by Plummers gang and defeated the gang leader and his henchmen.

On the day of the election the villains actually initially stop the homesteaders from voting but Tipton leads in a bunch of agents and ranchers to crush the outlaws. It results in all the baddies brought to justice, Wyoming becoming a state and Wayne gets the pretty girl Ann Rutherford.

==Cast==
*John Wayne as John Tipton
*Ann Rutherford as Janet Carter Harry Woods as Charles K. Plummer George Hayes as Maj. Carter
*Al Bridge as Steele 
*Fred Toones as Moses
*Etta McDaniel as  Mandy Lou Schaefer 
*Tom Brower as Marshal Bowen 
*Lane Chandler as  Bridger Cliff Lyons as Davis 
*Jack Rockwell as  Smith  Al Taylor as Henchman Red Charles King as  Henchman Hartley 
*George Chesebro as Henchman Green 
*Tracy Lane as Belden

==Production notes==
* The working title of this film was G-Men of the Nineties
* According to Hollywood Reporter, Warner Bros. "laid claim to the word G-man" (which was the title of a 1935 Warner film starring James Cagney) and threatened to sue any company which used it, causing Republic to change this films name to avoid litigation.  
* The film premiered on February 15, 1936. 
* The film, although based in Wyoming, was filmed at the Trem Carr Ranch in Newhall, California.

==See also==
* John Wayne filmography

==External links==
*  

 

 
 
 
 
 
 
 
 