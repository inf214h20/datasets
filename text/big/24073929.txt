Mazeppa (film)
 
{{Infobox film
| name           = Mazeppa
| image          = Mazeppa (film).jpg
| caption        = Film poster
| director       = Bartabas
| producer       = Yvon Crenn Marin Karmitz
| writer         = Bartabas Claude-Henri Buffard
| starring       = Miguel Bosé
| music          = 
| cinematography = Bernard Zitzermann
| editing        = Joseph Licidé
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = France
| language       = French
| budget         = 
}}

Mazeppa is a 1993 French drama film directed by Bartabas. It was entered into the 1993 Cannes Film Festival where it won the Technical Grand Prize.   

==Plot== 
Based loosely on French painter Théodore Géricaults life who met the famous equestrian Antonio Franconi, the director of the Cirque Olympique. Gericault decided to stay and live with the circus and painted only horses to try and understand the mystery of this animal. Mazeppa embodies a man carried away by his passion.

==Cast==
* Miguel Bosé - Gericault
* Bartabas - Franconi
* Brigitte Marty - Mouste
* Eva Schakmundes - Alexandrine
* Fatima Aibout - Cascabelle
* Bakary Sangaré - Joseph
* Norman Calabrese - Ami de Géricault
* Henri Carballido - Ami de Géricault
* Frédéric Chavan - Ami de Géricault
* Patrick Kabakdjian - Ami de Géricault
* Michel Lacaille - Ami de Géricault
* Claire Leroy - Amie de Géricault
* Bernard Malandain - Ami de Géricault

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 