The Fireman (1931 film)
{{Infobox Hollywood cartoon|
| cartoon_name = The Fireman
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Clyde Geronimi Manuel Moreno Ray Abrams Fred Avery Lester Kline Chet Karrberg Charles Hastings Pinto Colvig
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = April 6, 1931
| color_process = Black and white
| runtime = 6:55 English
| preceded_by = The Farmer
| followed_by = Sunny South
}}

The Fireman is a short animated film distributed by Universal Pictures, and stars Oswald the Lucky Rabbit. It is the 39th Oswald film by Walter Lantz Productions, and the 95th in the entire series.

==Plot==
Oswald and his buddy Hoodoo the polecat are stowing away on a fire truck. But as the fire truck runs a slightly rough ride, Oswald and Hoodoo fall off the vehicle. Momentarily Hoodoos weepy nephew comes to the scene, desperately wanting to be with them. Oswald does not think Hoodoos nephew should come along but decides to play with the little polekitten for a moment. The three go on to play hide and seek where in Oswald and Hoodoo hide, and the polekitten will try to find them. The rabbit and the polecat try to run as far they could while the polekitten is counting. Nevertheless, Hoodoos nephew gets to them quickly.

Oswald, Hoodoo, and Hoodoos nephew continue walking. At the end of their journey, they find a soda festival being held at some fair grounds. Some of the participants are the firemen who were with them on the fire truck. After drinking some beverage, Oswald and his two companions see a trio of rats who wear sunglasses and are holding polo mallets. The three rats come to them to ask for spare change. Oswald is kind to give each rat a nickel. Hoodoos nephew, however, is quite rude as the polekitten steals the rodents coins.

The rats, who are very annoyed, start chasing Hoodoos nephew. The polekitten tries to hide in a log but only half of him goes in. The rodents begin striking the polekitten in the rear with their polo mallets, and Oswald laughs. To get back at the rats, Hoodoos nephew uses a knife to slice off their tails. The rats, however, are able to fuse back their tails. To get back at Oswald, Hoodoos nephew moves a giant mouse trap behind the rabbit. The mouse trap snaps on Oswalds bottom not once but two times.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 