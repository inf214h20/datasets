Maattrraan
 
 
{{Infobox film
| name           = Maattrraan
| image          = Maatraan.jpg
| alt            = 
| caption        = Film poster
| director       = K. V. Anand
| producer       = Kalpathi S. Aghoram Kalpathi S. Ganesh Kalpathi S. Suresh Subha  (Dialogue) 
| screenplay     =  K. V. Anand Subha 
| story          =  K. V. Anand Subha Suriya Kajal Tara
| music          = Harris Jayaraj
| cinematography = S. Sounder Rajan Anthony
| studio         = AGS Entertainment
| released       =   Multi Dimensional Films  (Telugu language|Telugu)  
| runtime        = 168 minutes     
| country        = India Tamil
| budget         =   
| gross        = 
}}
 Suriya who Tara play Telugu version titled Brothers, receiving positive to mixed reviews from critics.  The film was  average at the box office due to mixed reactions from the critics and audience alike.       It has also been dubbed in Hindi under the title No. 1 Judwa-The Unbreakable.

==Plot==
Ramachandran (Sachin Khedekar) is a genetic scientist who doesnt get due credit and funds for his research. He tries creating a human with several talents through baby designing, which thus leads to the birth of his sons, who are conjoined together above the waist. Since they share a common heart, doctors suggest a sacrificial surgery, to which their mother Sudha (Tara (Kannada actress)|Tara) objects. They begin raising their children, Vimalan and Akhilan (both played by Suriya (actor)|Suriya). In the following years, Ramachandran makes it big with the help of Sudha. His company, Locus Lacto Products, makes huge profit through their product "Energion", the top-selling children’s powdered milk energy drink in the market.

Vimalan and Akhilan are poles apart in character, the former being decent and good at studies while the latter is care-free and poor at studies. Anjali (Kajal Aggarwal) joins their company as a translator. Both Vimalan and Akhilan fall for her. She, along with her Russian friend Volga, a journalist, spend their time with the brothers. Meanwhile, Anjali falls for Vimalan and Akhilan is happy for them.
 
Volga is then revealed to be a spy who tries to steal the trade secrets of Energion. She is exposed and sent out by Ramachandran. She then takes the brothers to their cattle farm under the pretext of an interview where she takes pictures and even collects a milk sample from their farm. When Vimalan questions her, he is warned by her that Energion was an adulterated product and could lead to the death of thousands of children. She also exposes their father of killing the head of their R & D department and setting the lab on fire, thereby destroying evidence. Consequently, she is murdered, but, she swallows a pen-drive containing evidences of the foul-play before dying. Anjali acquires it from the person who performs Volga’s autopsy. She hands the pen-drive over to Vimalan. Following this, Vimalan and Akhilan are confronted by goons who try to get the pen-drive from him. Akhilan is convinced that the attack was only meant for robbing them. During this fight, Vimalan suffers a huge blow on the head.

Vimalan is declared brain-dead and his heart is transplanted into Akhilan. After the twins are separated through a surgery, Anjali grows closer to Akhilan. Sudha is warned that Energion is adulterated and she confronts her husband who asks the food safety department to raid their company. But to her surprise, Energion is declared safe and hygienic. Meanwhile, Akhilan discovers that his father is behind Vimalan’s death by tracking his assistant, Dinesh (Ravi Prakash). He gets hold of the pen-drive that contains several photos, where ingredients used as cattle feed at their farm are declared as no feed. The beginning of the scheme is traced back to the fictional republic of Ukvania.

Accompanied by Anjali, Akhilan sets out to solve the mystery. They get her friend Ashok’s help, but he is subsequently killed. The photos are revealed to be athletes who were competing as Unified team under the Olympic flag in the 1992 Barcelona Olympics. The athletes were declared dead in a plane crash. The investigation leads them to the army medical research centre. The truth is then revealed that Energion was originally an undetectable steroid invented by Ramachandran to improve the performance of the athletes at the world games. Though the country performed well, some athletes began suffering from similar health problems leading to their deaths and that their death from a plane crash was faked to avoid national shame. They also learn that the adulterant in Energion could be detected only with the addition of ionization enhancer. After being assaulted by a local mafia led by Dinesh, Akhilan manages to kill him and takes the remedial ingredients back to India.

Once they are in India, Ramachandran is exposed and is about to be arrested. Akhilan requests his father to surrender, but he discovers his father’s ugly side and finds out that he and his brother are just a result of their father’s failed experiment. Ramachandran also states that he used the whole society as his research lab, through Energion. His leg is then crushed by a rock after being unsuccessful in killing his son. Akhilan then leaves his father to die at the hands of rats which chew on his flesh.

Akhilan gets National recognition for bravery. He later gets married to Anjali, and the couple subsequently become parents to conjoined twins.

==Cast== Surya as Akhilan and Vimalan
* Kajal Agarwal as Anjali
* Sachin Khedekar as Ramachandran Tara as Sudha
* Ravi Prakash as Dinesh
* Shankar Krishnamurthy as Varadharajan
* Irina Maleeva as Volga
* Ajay Ratnam as Ajay Ratnam
* Julia Bliss as Nadia
* Isha Sharvani in a Special appearance

==Production==

===Development===
  Suriya again after the success of Ayan (film)|Ayan, further noting that it would be produced by AGS Entertainment and would begin once Suriya finished shooting for A. R. Murugadoss sci-fi thriller 7aum Arivu. Anand had supposedly narrated the script of Maatraan to Suriya in 2009, but the project failed to launch then, due to the lack of technology. In an interview he stated that the film was an inspired from the true story of Thailand-based Siamese twins Ying and Sang. "I read an article about them, which inspired me to come up with a similar story. I imagined how would it be if two people, who are physically conjoined, completely differ in their ideologies," expressed KV Anand. 

===Casting=== Tara was signed on to play the mother to Suriyas character.  Prakash Raj was dropped from the film and was replaced by Sachin Khedekar who had played notable roles in Yaavarum Nalam and Deiva Thirumagal.  Daniel Balaji was said to be a part of the cast but he denied that he was a part of the crew.  Hindi actor Milind Soman was also wrongly reported to be working for the film. 

===Filming=== Macedonia and Kajal and Suriya was shot at Wai, a village near Pune in Maharashtra.  Moreover, many CG scenes were shot at Balu Mahendra studios.  A song featuring over 500 junior artists and Suriya was shot in Jodhpur, touted to be the last phase of canning songs.  Next, filming for an important talkie portion was held at Bhuj in Gujarat near the safe zone along the Indo-Pakistani border that lasted for five days. K. V. Anand and cinematographer Sounder Rajan subsequently were in Madagascar, hunting for a forest location to shoot a song for the film. However, since Suriya could not afford filming there due to his other commitments, Anand decided to erect a similar set in India to create the same look as in Madagascar.  The shooting which was to be held in the US, was cancelled in June 2012.  The entire filming was wrapped up in Norway by completing the "Naani Koni" song in outdoor scenes shot at Trollstigen, Geiranger, Atlanterhavsveien, Måløy and Aurlandsfjorden.  

==Soundtrack==
{{Infobox album|  
| Name        = Maattrraan
| Longtype    =
| Type        = soundtrack
| Artist      = Harris Jayaraj
| Released    = 9 August 2012
| Cover       = Maattrraan Album Art.jpg
| Caption     = Feature film soundtrack
| Length      = 26:47 Tamil
| Label       = Sony Music
| Producer    = Harris Jayaraj
| Last album  = Oru Kal Oru Kannadi (2012)
| This album  = Maattrraan (2012)
| Next album  = Thuppakki (2012)
}}

Harris Jayaraj composed the music; the soundtrack features five tracks that belong to varied genres.  As per K. V. Anands idea, he and Jayaraj sailed on a ship in the Mediterranean Sea where most of the songs were roughly composed. 
 Krish and Charulatha Mani. The events satellite rights were secured by Jaya TV for an undisclosed price. 

The soundtrack received generally positive reviews from critics. IndiaGlitz wrote: "Harris Jayaraj has not disappointed, the music composer has equipped Maattrraan with songs that make an instant impact", and called the album "an interesting treat to the fans".  In contrast, BehindWoods said "On screen, KV Anands magic might do the trick but the songs as such arent special. Most of them fall under the heard before category...", and gave it 2 out of 5 stars. 

{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 26:47
| all_music =
| lyrics_credits = yes
| title1 = Rettai Kathirae
| extra1 = Krish (singer)|Krish, MK Balaji, Mili Nair & Sharmila
| lyrics1 = Na. Muthukumar
| length1 = 4:50
| title2 = Nani Koni
| extra2 = Vijay Prakash, Karthik & Shreya Ghoshal
| length2 = 5:26
| lyrics2 = Viveka
| title3 = Theeye Theeye
| extra3 = Franco, Charulatha Mani, Sathyan (singer)|Sathyan, Aalap Raju, & Suchitra
| lyrics3 = Pa. Vijay
| length3 = 5:23
| title4 = Yaaro Yaaro Karthik & Priya Himesh
| length4 = 5:35
| lyrics4 = Thamarai
| title5 = Kaal Mulaitha Poovae
| extra5 = Javed Ali & Mahalakshmi Iyer
| length5 = 5:31
| lyrics5 = Madhan Karky
}}

==Release== Telugu distribution US ATMUS distributed the film in 63 centers, making it the widest release of a Tamil film in the country.    The film opened in over 1200 screens across the globe with 32 screens in Chennai city alone on 12 October 2012. 

Maattrraans official teaser was released in Chennai and simultaneously uploaded to YouTube on 12 July 2012.  The second official teaser trailer was released on 9 August 2012. During the late production stage the title was slightly changed from Maatraan to Maattrraan.  The film was censored on 3 October 2012 and it was given a clean "U" certificate by the Indian Censor Board without cuts. 

===Critical reception===
{{Album ratings
| rev1 = The Times of India
| rev1Score =      
| rev2 = Indiaglitz
| rev2Score =      
| rev3 = Sify
| rev3Score = Fun Ride   
| rev4 = Behindwoods
| rev4Score =       
| rev5 = OneIndia
| rev5Score =      
| rev6 = Rediff
| rev6Score =      
| rev7 = Nowrunning
| rev7Score =  "    
|noprose=yes}}

Maattraan received positive to mixed reviews from critics.   rated the film 3 out of 5 stars, stating that it had "rare characters of conjoined twins but has a predictable story and appears like a second part of Suriyas last movie 7aum Arivu directed by A. R. Murugadoss" and that it was "good, not brilliant". 

Pavithra Srinivasan of Rediff gave the film 2.5 out of 5 stars, concluding that it had a "great premise, great characters and actors who could have pulled off a complicated story. Sadly, the movie never capitalises on its strengths".  S. Viswanath from Deccan Herald cited that "despite its interesting theme, the film is, however, done in by its rather long running time as also painfully sluggish first half seeking to establish the plot but brimming with comic capers", summing up that it was "an ensemble entertainer but could have been much better".  Haricharan Pudipeddi of NowRunning gave the film 2.5/5 stars, stating that it "only promises the potential of Suriya, but fails to arouse interest due to its stretched second half and lacklustre narration."  The New Indian Express critic Malini Mannath claimed that the film "with its whimsical screenplay and lackadaisical narration, turns out to be a huge disappointment", going on to add that it "smacks of overconfidence, and an utter disregard for the sense and sensibility of a viewer".  J Hurtado of Twitch Film said, "Maatraan is two decent films split down the middle with little connective tissue to bind them, not unlike its protagonists" and concluded, "See it at your own risk." 

In response to most of the reviews which mainly criticized the "film being very long and dragging towards the climax", Maattrraan was re-edited to make itself "more slick and racy, to appeal to a larger section of the audience". 1 minute and 23 seconds of the first half and 19 minutes and 30 seconds of the second half were trimmed from the film. The trimmed version earned favorable response from fans, who called it "more racy and entertaining". 

===Box office===
Maattrraan had a good opening at the domestic box office, collecting   nett in Tamil Nadu,    with   in Chennai alone in its opening weekend.  The film also collected   in Kerala in the opening weekend.  The film stayed in the first position for three consecutive weeks in Chennai but was later overtaken by horror film Pizza (2012 film)|Pizza at the box office.    The film was declared as an Above average by Behindwoods. The film is reported to have completed a 50 day run in a few theaters across Tamil Nadu.  
 UK and USA in the opening weekend.   The film overall collected  ,  and   in UK and the USA, respectively. 

===Accolades===

{| class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|- Vijay Awards Vijay Award Best Actor Suriya
| 
|- Vijay Award Best Art Director Rajeevan
| 
|- Vijay Award Best Stunt Director Peter Hein
| 
|- Vijay Award Favorite Hero Suriya
| 
|-  CineMAA Awards CineMAA Awards#Best CineMAA Award for Best Actor - Male (Tamil) Suriya
| 
|- 2nd South Indian International Movie Awards Best Director
|K. V. Anand
| 
|- SIIMA Award Best Actor Suriya
| 
|- Best Actor in a Negative Role Sachin Khedekar
| 
|- Best Fight Choreographer Peter Hein
| 
|- Best Dance Choreographer Brinda - "Rettai Kathire"
| 
|}
Maattrraan won the EME (Excellence in Media & Entertainment) award for the Best VFX in an Indian feature film category for 2012, recognized as the highest honor for Visual Effect works. 

==Controversies==
When Maattrraan was touted to be the first Indian film based on conjoined twins, it created a stir after two other films with the same concept -Charulatha and Iruvan, were launched later. It was reported to have shared the same storyline as Charulatha, but director K. V. Anand however denied the reports and said, "After reading such reports, I watched the original version (Charulatha was based on 2007 Thai movie Alone (2007 film)|Alone). There isn’t any connection between the two movies, except for the fact that the protagonists are conjoined twins." 
 Stuck on You,  with the posters of both films also being described as similar to each other. Lead actor Suriya however denied this and said, "I have been seeing comments and links on social networking sites saying that Maattrraan is based on some world movie. Only after seeing those links, I came to know that such a film even exists!" 
 Telugu dubbed version Brothers was allowed to be released in the state. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 