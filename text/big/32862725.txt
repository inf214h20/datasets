The Man Who Forgot (1927 film)
 
{{Infobox film
  | name     = The Man Who Forgot
  | image    = 
  | caption  = 
  | director = A. R. Harwood		
  | producer = A. R. Harwood
  | writer   = 
  | starring = Walter Nicholls William Hallam
  | music    = 
  | cinematography = William Hallam
  | editing  = 
  | distributor = Walter Nicholls William Hallam A. R. Harwood
  | released = 1927
  | runtime  = 5,000 feet
  | language = 
  | country = Australia
  | budget   = 
}}

The Man Who Forgot is a 1927 silent Australian feature film which marked the directorial debut of A. R. Harwood. It is considered a lost film.

==Production==
Little is known about the movie apart from the fact it was a low-budget melodrama shot outdoors to save on studio costs. The story included scenes at the Ascot racecourse in Melbourne, a fight on the brink of the Werribee Gorge, timber felling in the Dandenong Rangers, and an escape by the hero in an aeroplane at the Essendon Aerodrome. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p141 

==Release==
Harwood distributed the film himself in partnership with his leading actors Nicholls and Hallam. They showed it on a double bill with Jewelled Nights (1925) around rural Victoria. Harwood, Nicholls and Hallam then announced plans to make a second feature together, a farce called Struth, but the project was abandoned and Harwood spent the next few years in distribution. 

==Cast==
* William Hallam as Crazy Dan
* Walter Nicholls as Stephen Jackson

==References==
 

==External links==
*  

 

 
 
 


 