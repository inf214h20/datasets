It Doesn't Hurt Me
{{Infobox film
| name           = It Doesnt Hurt Me
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = Мне не больно (Mne ne bolno)
| director       = Aleksey Balabanov
| producer       = 
| writer         = Valeri Mnatsakanov
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Renata Litvinova, Aleksandr Yatsenko, Dmitriy Dyuzhev
| music          = Vadim Samoylov
| cinematography = Sergey Astakhov
| editing        = Tatyana Kuzmichyova
| studio         = Kinokompaniya CTB
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Russia
| language       = Russian
| budget         = 
| gross          = 
}}
It Doesnt Hurt Me (Russian: Мне не больно: Mne ne bolno) is a 2006 Russian film directed by Aleksey Balabanov.  The film was released on June 15, 2006 in Russia and stars Renata Litvinova, Aleksandr Yatsenko, and Dmitriy Dyuzhev as three young adults living in St.Petersburg.  

==Synopsis==
Tata (Renata Litvinova) is a vivacious young woman whose health is deteriorating due to leukemia. When she meets Misha (Aleksandr Yatsenko) and Oleg (Dmitriy Dyuzhev), the three hit it off well and Tata begins to date Misha. The problem is that she keeps her diagnosis from Misha, which complicates the relationship.

==Cast==
*Renata Litvinova as Tata
*Aleksandr Yatsenko as Misha
*Dmitriy Dyuzhev as Oleg
*Nikita Mikhalkov as Sergei Sergeyevich
*Inga Strelkova-Oboldina as Alya (as Inga Oboldina-Strelkova)
*Valentin Kuznetsov as Vasya
*Sergey Makovetskiy as Doctor
*Mark Rudinshtejn as Zilberman
*Marina Solopchenko as Zilbermans wife
*Mariya Nikiforova as Doctors wife
*Ilya Mozgovoy as Security guard Sasha
*Sami Hurskulahti as Otto
*Veronika Dmitriyeva as Girl in train
*Dariya Utkina as Tanya (as Dasha Utkina)
*Marina Shpakovskaya as Prostitute

==Reception== Time Out Russia gave the movie a positive review and called it a "strong melodrama".  Seans (magazine)|Seans, a Saint Petersburg-published magazine specializing on cinema published a number of short review of the most influential authors. The reviews were generally positive, and, in particular, they emphasized a good performance of Litvinova. 

===Awards===
*Best Actress at the MTV Movie Awards, Russia (2007, nominated - Renata Litvinova) Sochi Open Russian Film Festival (2006, won - Aleksandr Yatsenko)   
*Best Actress at the Sochi Open Russian Film Festival (2006, won - Renata Litvinova) 
*Grand Prize at the Sochi Open Russian Film Festival (2006, nominated) Golden Eagle Award (2006, nominated) 

==Further reading==
*Florian Weinhold (2013), Path of Blood: The Post-Soviet Gangster, His Mistress and Their Others in Aleksei Balabanovs Genre Films, Reaverlands Books: North Charleston, SC: pp. 139-163.

==References==
 

==External links==
*  
*  