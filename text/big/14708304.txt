Crank: High Voltage
 
{{Infobox film
| name           = Crank: High Voltage
| image          = Crank two ver2.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Neveldine/Taylor
| producer       = {{Plainlist|
* Gary Lucchesi
* Tom Rosenberg
* Skip Williamson
* Richard Wright}}
| writer         = Neveldine/Taylor
| starring       = {{Plainlist|
* Jason Statham
* Amy Smart
* Clifton Collins, Jr.
* Efren Ramirez
* Bai Ling
* David Carradine
* Dwight Yoakam}}
| music          = Mike Patton
| cinematography = Brandon Trost
| editing        = Fernando Villena
| studio         = {{Plainlist|
* Lakeshore Entertainment
* @radical.media}} Lionsgate
| released       =  
| runtime        = 95 minutes  
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $34.6 million 
}} Mark Neveldine and Brian Taylor and stars Jason Statham reprising his role as Chev Chelios. The story of the film resumes shortly after the first film left off, retaining its Real time (media)|real-time presentation and adding more special effects. The film was released in the United Kingdom on April 15, 2009, two days prior to its North American release date.

==Plot== the previous film, Chev Chelios (Jason Statham) lands in the middle of an intersection after falling out of a helicopter. He is scooped off the street via snow shovel by a group of Chinese medics and removed from the scene. Chev wakes up in a makeshift hospital and sees doctors removing his heart while Johnny Vang (Art Hsu) watches. The doctors place Chevs heart in a white cooler with a padlock, and place a clear plastic artificial heart in his chest. He wakes up sometime later and escapes. He notices a yellow battery pack is attached to him. After a gunfight and interrogation of a thug, he learns the location of Johnny Vang: the Cypress Social Club.

Chev calls Doc Miles (Dwight Yoakam), who says that Chev has been fitted with an AbioCor artificial heart. Miles informs Chev that once the external battery pack runs out, the internal battery will kick in and he will have 60 minutes before it stops working. While driving Chev crashes his car which destroys his external battery pack. After getting directions from a driver, Chelios has the driver use his jumper cables on him. At the club, Chev loses Vang but picks up a hooker named Ria (Bai Ling) who sends him to a strip club where Vang is hiding out. In the club, Chev finds Eve (Amy Smart), now a stripper (It is humorously revealed that the phone call Chev made to Eve while falling to his supposed death at the end of Crank was completely incomprehensible due to the wind rushing by, and the farewell he had left on her answering machine was merely how he imagined it would sound). A group of Mexican mobsters, led by Chico, show up looking for Chelios. After a gunfight, Chev learns that a mobster named "El Hurón" ("The Ferret") wants to kill him, but he doesnt find out why.

Outside of the strip club, Chev commandeers a police cruiser with Eve and another stripper. The stripper tells Chev that he should look at the Hollywood Racetrack for Johnny Vang. Along the way, Chev meets Venus (Efren Ramirez), who reveals himself to be Kaylos brother. Wanting his help, he tells Venus that El Huron was involved in his brothers death, but escaped. At the horse track Chev begins losing energy again. Another call from Doc Miles informs him that friction will cause static electricity to power the internal battery. Eve shows up and they have sex on the racetrack before Chev spots Vang and leaves Eve behind. Vang escapes, and Chev is about to be subdued by security when Don Kim picks Chev up in his limo. He informs Chev that there is a leader in the Triads named Poon Dong (David Carradine), who was in need of a heart transplant and chose Chevs to replace his. Chev kills Don Kim and his henchmen upon learning that Don Kim wishes to return him to Poon Dong for a reward. Meanwhile, Venus calls in Orlando (Reno Wilson) to assist in tracking down El Huron.

While searching for Vang, Chev boards an ambulance and steals a battery pack for his artificial heart. Chev exits the ambulance upon seeing Johnny Vang on the street outside and a shootout ensues while before Chev subdues Vang. Chev discovers that Vangs red cooler doesnt contain his heart and then learns via cellphone from Doc Miles that his heart has already been transplanted into Poon Dong. Johnny Vang is shot and killed by Chico as Chev interrogates him, after which Chev is knocked unconscious. Doc Miles uses his secretary to locate Poon Dong to retrieve Chevs heart.
 middle finger to the audience.

Doc Miles replaces Chevs heart. At first, it looks like a failure but Chelioss eyes open wide and his heart monitor indicates normal activity.

==Cast==
* Jason Statham as Chev Chelios
* Amy Smart as Eve Lydon
* Efren Ramirez as Venus
* Dwight Yoakam as Doc Miles
* Reno Wilson as Orlando
* Clifton Collins, Jr. as El Huron
* Bai Ling as Ria
* Art Hsu as Johnny Vang
* David Carradine as Poon Dong
* Geri Halliwell as Karen Chelios
* Yeva-Genevieve Lavlinski as Pepper
* Corey Haim as Randy 
* John de Lancie as Fish Halman

Celebrity cameos include Ron Jeremy, Ed Powers, Jenna Haze, Nick Manning, Lexington Steele, Chester Bennington, Glenn Howerton, Maynard James Keenan, Danny Lohner, Keith Jardine, Lauren Holly, and Lloyd Kaufman.

==Production==
Dismissing that a sequel is not possible, Neveldine and Taylor wrote a script for a second film, which was greenlit by Lionsgate. Statham turned down other projects in order to re-appear as the protagonist.  Lions Gate Entertainment handled North American distribution of the film, while Lakeshore Entertainment and Sony Pictures handled international distribution.
 Filming started in April 2008. In order to help keep costs low, the filmmakers took advantage of low-cost prosumer HDV cameras such as the Canon XH-A1, as well as a consumer model, the Canon HF10. 

===Soundtrack===
 

Linkin Parks song, "Given Up", was featured in the trailer for the film. The majority of the soundtrack was done by Mike Patton.  The soundtrack received an "Incredible" 9.5 out of 10 from IGN.

Original songs not scored by Mike Patton that appear in the film are as follows: Keep on Loving You" by REO Speedwagon
* "The Stroke" by Billy Squier
* "Heard It in a Love Song" by Marshall Tucker Band

==Reception==

===Box office===
Crank: High Voltage opened in 2,223 theaters in North America and grossed $6,963,565 with an average of $3,133 per theater and ranking #6 at the box office. The film ended up earning $13,684,249 domestically and $20,876,328 internationally for a total of $34,560,577. 

===Critical response===
The film received mixed to positive reviews and has a rating of 63% on   based on 15 reviews indicating Mixed or average reviews. 

==Home media==
 

Crank: High Voltage was released via DVD and Blu-ray Disc|Blu-ray on September 8, 2009 in the United States. At the DVD sales chart, Crank opened at #2, selling 305,000 units which translates to $5,345,078 in revenue. As per the latest figures, 827,000 units have been sold, acquiring revenue of over $15 million. This does not include Blu-ray sales or DVD rentals.  In Germany, the uncut DVD and Blu-ray was indexed on March 31, 2010. 

==Possible sequel==
In an interview, when asked about a third Crank film, actress Amy Smart said "Its been talked about," but no actual statement from the writers has been made.  Also in an interview with Amy Smart after the release of High Voltage, she mentioned that Crank 3 might be made in 3-D film|3-D. 

During an Ask me anything on Reddit, Brian Taylor gave a possible 2013 release date for Crank 3. 

In March 2015, Statham gave an update on the sequel, saying that hed love to do it and he was waiting for Taylor and Neveldine to "get their heads together." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  , DVD Talk

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 