Delamu
{{Infobox film
  | name         = Delamu
  | image        = Delamu.jpg
  | caption      = Theatrical poster
  | writer       = Tian Zhuangzhuang
  | starring     = 
  | director     = Tian Zhuangzhuang
  | producer     = Takahiro Hamano Yang Zhao
  | editing      = Cui Jian Zhang Dalong Wang Yu Wu Qiao
  | distributor  = 110 min.
  | released     = 
  | runtime      =  Mandarin Standard Tibetan
  | rating       =
  | music        = Li Zhao
  | budget       =
  | preceded_by  =
  | followed_by  = China
    }} Fifth Generation Nujiang River Valley, along the Tea Horse Road, an ancient trade route between Chinas Yunnan province and Tibet. The film was jointly produced by companies in the Peoples Republic of China, and Japan. It had its American premier at the 2004 Tribeca Film Festival.

The title, "Delamu" refers to the Tibetan word for "peaceful angel", and the name of one of the mules owned by a villager in the film.   

==The Tea Horse Road==
Stretching across Yunnan, Tibet, and into the Himalayas, the heart of Delamu is the "Tea Horse Road" ( ). One of the oldest caravan routes in Asia, the film documents one such caravan as it transfers raw material to a modern construction site.

As Tian travels with the caravan, he interviews people who have lived along the road for decades, including a priest who was thought to have disappeared during the Cultural Revolution, a 104 year old woman, and a mule driver who owns the titular Delamu.

==Reception==
Though quiet and a far cry from either the insulated Springtime in a Small Town or the epic The Blue Kite, Tians Delamu has nevertheless garnered both praise and some criticism. On the one hand it has been well received by critics in Asia. The inaugural Chinese Film Directors Association Awards bestowed its honor for best director to Tian for Delamu.  It has similarly been well received in the West. In its premier at Tribeca, Delamus cinematography of the stunning landscape was praised by critics. 

On the other hand, many critics often cannot help but to compare the film to Tians account of the Cultural Revolution, The Blue Kite, often negatively. One notes the "travelogue sheen" as preventing real penetration into the subject matter.  Another (admittedly a socialist critic) complained that Delamu despite its beauty, was a "National Geographic style travelogue   broke no new ground." 

==Notes==
 

==External links==
* 
* 
*   with Tian Zhuangzhuang on Delamu

 

 
 
 
 
 
 