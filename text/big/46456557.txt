Victor and Victoria (1957 film)
{{Infobox film
| name = Victor and Victoria 
| image =
| image_size =
| caption =
| director = Karl Anton
| producer =  Waldemar Frank
| writer =  Reinhold Schünzel (1933 screenplay)   Curt J. Braun 
| narrator =
| starring = Johanna von Koczian   Georg Thomalla   Johannes Heesters
| music = Heino Gaze   
| cinematography = Willy Winterstein   
| editing =     Annemarie Rokoss    
| studio = Central-Europa Film 
| distributor = Prisma
| released = 
| runtime = 107 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Victor and Victoria (German:Viktor und Viktoria) is a 1957 German musical comedy film directed by Karl Anton and starring Johanna von Koczian, Georg Thomalla and Johannes Heesters. A woman gains success on the stage by pretending to be a female impersonator. It is a remake of the 1933 film Victor and Victoria, which had starred Renate Müller.  The films sets were designed by Emil Hasler and Walter Kutz.

==Cast==
*  Johanna von Koczian as Erika Lohr 
* Georg Thomalla as Viktor Hempel 
* Johannes Heesters as Jean Perrot 
* Annie Cordy as Titine 
* Boy Gobert as Lacoste 
* Carola Höhn as Marquise de Sevigné 
* Werner Finck as Hinz 
* Franz-Otto Krüger as Kriminalkommissar
* Kurt Pratsch-Kaufmann as Wurstmaxe  
* Gerd Frickhöffer as Hotelportier  
* Stanislav Ledinek as Wirt im Bierlokal
* Waltraut Runze as Pressedame  
* Kurt Vespermann as Intendant  
* Henry Lorenzen as Tier-Dresseur  
* Erich Poremski 
* Valérie Camille as Tänzerin 
* Alicia Márquez as Tänzerin  Jack Del Rio as Tänzer  
* Les Romanos Brothers as Tänzer  
* Die Vier Sunnies as Sänger  
* Das Cornell-Trio as Sänger 
* Ernie Bieler as Singer  
* Wolfgang Gruner as Friseur  
* Hilla Höfer as Oberkellnerin  
* Edhilt Rochell as Sekretärin  
* Ralf Wolter as Friseur

== References ==
 

== Bibliography ==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2007. 

== External links ==
* 

 

 
 
 
 
 
 
 

 