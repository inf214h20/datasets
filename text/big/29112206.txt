Dus Tola
 
 

{{Infobox film
| name           = Dus Tola
| image          = Dus tola poster.jpg
| caption        = 
| director       = Ajoy Varma
| producer       = Kunwar Pragy Arya Sanjay Bhattacharjee Sushma Reddy
| writer         = Raghunath Paleri 
| starring       = Manoj Bajpai Aarti Chhabria Siddharth Makkar Pallavi Sharda Govind Namdeo Asrani Dilip Prabhawalkar Ninad Kamat
| cinematography = S. Kumar
| film editing    = Sajit Unnikrishnan
| music          = Sandesh Shandilya
| released       =  
| country        = India
| language       = Hindi
| Time           = 2:03:29
| budget         = 
| gross          =  
}}
 Hindi comedy drama film, remade from the Hit Malayalam film of the 80s Ponmuttayidunna Tharavu, directed by Ajoy Varma and starring Manoj Bajpai in the lead role. Dus Tola was released on 22 October 2010.

Distributed in the U.S. and co-produced by Warner Bros., it is the fourth Bollywood movie made and distributed in partnership with a major Hollywood studio, following Sonys Saawariya (2007) and Walt Disney Pictures animated feature Roadside Romeo (2008) and Warner Bros. Pictures Chandni Chowk To China (2009).  It is the second Hindi film under the banner of Warner Bros. Pictures.

==Plot==
The story starts in a small town Sonapur. Shankar (Manoj Bajpai) is a local Goldsmith making jewelry. Shankar is in love with his next-door neighbor Suvarnalata (Sonu, Aarti Chabria) who loves him too. But her father Daya Shastri (Dilip Prabhavalkar) is opposed to their relationship. Shankar and Suvarnalata always meet after Suvarnalata computer class. He gave her father some earrings. The father and mother (Lakshmi) call a pandit (Aatmaram) and asks for a rich boy from Dubai. The pandit is Shankars friend and he asks Sonu if she is ready to marry Shankar.

Sonu asks him to make her a dus tola necklace to induce her father to give her hand to Shankar. Shankar toils to make the perfect dus tola necklace for Sonu which she accepts. But when her father finds it, he tells her to accept the necklace as a gift from an older brother. The next day Daya shows the necklace off to local people and says he gave it to his daughter as a gift before her marriage. Shankar is shocked and faints in the middle of the town. When he wakes up, he sees Geeta (Pallavi Sharda the village dance teacher), Bholenath, Atmaram, Sarpanch and Abdul(who owns a shop under Geetas dance studio) around him. During the night Shankar goes to Sonus house and challenges her to tell everyone that he gave her the necklace in order to declare her love for him. The next day, Shankar and his friends go to the house of Daya & Lakshmi but Sonu comes and says she doesnt know about any necklace except the one that her father gave her. Shankar is heartbroken and Sonu marries a wealthy man from Dubai named Ravi (Siddharth Makkar). Ravi goes to Dubai for a while and Sonu bears him a child. Meanwhile Shankar tries to move on with his life and he opens his own shop called Shankar and Sons. While at the grand opening, Geeta admires Shankar and falls for him. At the grand opening Qazi, the richest guy in town, gives Shankar 10,000 rupee and tells him to use it when he really needs it.

Two months pass and Ravi comes back to India to live with Sonu and his daughter after his Dubai visa expired, he says he spent two months in jail. Meanwhile, Geeta comes to Shankars shop to sell her earrings as she is short of money to send home to her family. Shankar remembers that Qazi gave him money so he gives it to Geeta, she realises Shankar really has a heart of gold. Ravi needs money to buy a truck and he comes to Shankar to sell the dus tola necklace which Sonu is always wearing, the very necklace that Shankar toiled to make for her. Shankar tests the gold on the necklace and reveals that it is fake! Suvarnalata reveals to Ravi that Shankar made the fake necklace and Ravi goes in search of him.

In a quiet peaceful field Shankar and his friends are talking about the dus tola necklace when Ravi comes with townspeople and a fight begins. Atmaram escapes and calls Qazis wife as he is being injured. She comes running for him and her face is revealed to everyone. Shankars father is brought in to see what the commotion is about, he reprimands Shankar for making a fake necklace but Shankar explains he had no choice, he had no money so he would have worked hard to give her a new real necklace after their marriage. Shankar admits his mistakes to Ravi who forgives him and returns to his wife Suvarnalata. The next time Shankar sees Geeta standing at a bus stop he pauses for her, realising that she is in fact the real gold. The two of them find a happy ending.

==Cast==
*Manoj Bajpai as Shankar Sunar
*Pallavi Sharda as Geeta
*Aarti Chhabria as Suvarnalata Shastri
*Siddharth Makkar as Ravi
*Govind Namdeo as Qazi
*Asrani as Sarpanch
*Dilip Prabhawalkar as Pandit Daya Shastri
*Ninad Kamat as Bholenath
*Asif Basra as Aatmaram

==Music==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; "
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
|"Aisa Hota Tha"
| Mohit Chauhan
|-
|"Lal Lal Aag Hua"
| Sukhwinder Singh  
|-
| "Tumse Kya Kehna"
| Sonu Nigam
|-
|"Jee Na Jalaiyo(Female)"
| Sunidhi Chauhan
|-
|"Jee Na Jalaiyo (Male)"
| Sukhwinder Singh
|-
|}

==Cricitical reception==
The Times of India stated that Dus Tola is its archaic story which simply doesnt connect with the new age audience. Of course, there have been so many small town stories that have managed to find applause even today: Udaan, Manorama Six Feet Under, Well Done Abba. But that was because they dealt with a relevant issue.  Komal Nahta stated that Dus Tola is an entertaining fare, no doubt, but it wont work at the box-office because it is more like a TV serial or a stage-play and less like a film. 

==References==
 

==External links==
*  
*  
*  

 
 
 