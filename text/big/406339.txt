The Crow (1994 film)
{{Infobox film
| name           = The Crow
| image          = Crow ver2.jpg
| caption        = Theatrical release poster
| director       = Alex Proyas
| producer       = {{plainlist|
* Jeff Most
* Edward R. Pressman
}}
| writer         = {{plainlist|
* David J. Schow
* John Shirley
}}
| based on       =  
| starring       = {{plainlist|
* Brandon Lee
* Ernie Hudson
* Michael Wincott
}}
| music          = Graeme Revell
| cinematography = Dariusz Wolski
| editing        = {{plainlist|
* Dov Hoenig
* M. Scott Smith
}}
| studio         = Dimension Films
| distributor    = Miramax Films
| released       =  
| runtime        = 102 minutes
| language       = English
| country        = United States
| budget         = $23 million 
| gross          = $50.7 million 
}} fantasy action comic book Eric Draven (Lee), a rock musician who is revived from the dead to avenge his murder and death of his fiancée.

The lead actor, Brandon Lee, was accidentally killed on the set during filming by a defective blank. Unfinished scenes that were to feature him were dealt with by rewrites and digital special effects. The film is dedicated to Lee and his fiancée, Eliza.
 cult following.

==Plot==
  drug addict who ignores her. Albrecht tells her everything will be okay and that Shelly will be fine.

A year later, Sarah visits Erics and Shellys graves. As she leaves the cemetery a crow lands on Erics headstone and taps it. That night, Eric awakens from death and climbs out of his grave, wracked with convulsions. Eric follows the crow through the streets of Detroit to his old apartment, finding it derelict. He is met by his cat, Gabriel, who remembers his old master. Eric experiences flashbacks of his own death, remembering that he and Shelly were murdered by local thugs T-Bird, Tin-Tin, Funboy, and Skank, who work for a gang boss named Top Dollar (Michael Wincott) and his lover/half-sister Myca (Bai Ling). Eric swings out the window he was thrown out of, piercing his hands on shards of glass. He sees his wounds heal immediately. He replaces his burial clothes with a dark, imposing costume and paints his face in a parody of a porcelain harlequin mask, decorating his lips and eyes with black, scar-like slashes. Guided by the crow, he sets out to avenge his and Shellys deaths.

Eric learns that he can see what the crow sees telepathically. The crow helps Eric locate Tin-Tin, and they engage in a one-on-one street fight after which Eric kills Tin-Tin with his own knives. Eric takes Tin-Tins coat, leaving a large crow-shaped bloodstain on the wall of the alley as his calling card. He goes to Gideon (Jon Polito)s pawn shop where Tin-Tin pawned Shellys engagement ring. Eric forces his way into the shop, then forces Gideon to return the ring and interrogates him about Tin-Tins associates. Gideon tells him that they hang out at a club called The Pit and that Funboy lives upstairs. Eric starts throwing rings at Gideon, telling him that each one is a life he helped destroy. Gideon pleads for his life, and Eric lets him live to warn the rest of the gang. Eric blows up the shop by firing a shotgun loaded with rings into a puddle of spilled gasoline.

Eric meets Albrecht at the scene of the explosion and calls out his name which surprises Albrecht, who does not remember or recognize him. Eric asks if he knows Shelly Webster. Albrecht answers that she is dead and tells him to sit at the curb. He turns away, distracted by looters, and Eric vanishes. Later, Sarah leaves The Pit on her skateboard and is nearly hit by a passing taxi when Eric saves her, hiding his identity.

Eric finds Funboy getting high on morphine in an apartment with Darla. As he is jokingly talking to Funboy, Funboy shoots him in the hand, which heals itself. After shooting Eric several times, Funboy is injured and passes out. After Eric drags him into the shower, he confronts a hysterical Darla, grabbing her arm and showing her the reflection of her track marks in the mirror as the morphine pushes its way back out of her arm. Eric tells her to quit drugs in order to be a good mother to Sarah. Seeing Darla fleeing the bar, Top Dollars bodyguard Grange (Tony Todd) goes upstairs to investigate, finding Funboy dead with syringes stabbed into his chest.

Eric visits Albrecht at his apartment. They discuss Eric and Shellys murder and Albrecht relates that he watched as Shelly suffered for 30 hours before dying, and that he was demoted for asking too many questions about the crime. Eric touches Albrechts head, and all the pain and memories of Shellys death are transferred to Eric. Meanwhile, Gideon meets with Top Dollar and Myca, and passes on Erics warning. Not believing Gideons story, Top Dollar stabs him through the neck and then shoots him.
 Yugo but is hit by a pursuing police car. Eric drives T-Bird to the docks, then kills him with pyrotechnics while a horrified Skank watches in the distance. Eric leaves a fiery symbol in the shape of a crow burning at the scene.

The next morning, Sarah and Darla begin repairing their relationship. Sarah, having realized Erics identity, goes to his old apartment and tells him that she misses him and Shelly. Eric explains that, although he cannot be friends with her now that he is dead, he still cares for her and asks her to take care of Gabriel. After questioning a now-paranoid Skank, Top Dollar and Myca learn more about Erics existence and his actions. They hold a meeting with their associates where they discuss new plans for their annual Devils Night crime spree. Eric arrives at the meeting, looking for Skank, and a massive gunfight ensues. Top Dollar escapes with Myca and Grange, while Eric kills everyone in the room and throws Skank out a window to his death.

Having supposedly finished his quest, Eric returns to his grave where he sees Sarah. She says goodbye to him and he gives her Shellys engagement ring. She is then abducted by Grange, who takes her into a nearby church where Top Dollar and Myca are waiting. Through his telepathic link to the crow, Eric realizes what has happened and goes to the church to rescue her. Grange shoots the crow as it flies into the church, causing Eric to lose his immortality. Just after Top Dollar shoots and wounds Eric, Albrecht arrives, intending to pay his respects to Eric. During a shootout Grange is killed and Albrecht is wounded. Myca grabs the wounded crow, intending to take its mystical power for herself. Top Dollar ties Sarah up and climbs the bell tower.

Pursuing Top Dollar, Eric encounters Myca. As she is about to shoot Eric, the crow escapes Mycas grip and pecks out her eyes, causing her to fall down the bell tower to her death. Eric reaches the roof of the church and fights Top Dollar; due to Erics weakened condition, Top Dollar gains the upper hand. While Eric is down, Top Dollar admits ultimate responsibility for what happened to Eric and Shelly. As Top Dollar is about to kill him, Eric lunges forward and telepathically gives him the 30 hours of pain that Shelly suffered. An overwhelmed Top Dollar falls off the roof of the church to be fatally impaled on the horns of a gargoyle. Eric saves Sarah and tells her to stay with Albrecht until help arrives. Eric makes his way to Shellys grave. As he succumbs to his injuries, he is approached by Shellys spirit - reunited in death, knowing that both will be able to rest in peace.

Sarah visits the cemetery and sees that Eric and Shellys graves lie undisturbed. The crow, perched on Erics headstone, gives her Shellys engagement ring before soaring over the city and into the night.

==Cast== Eric Draven/The Crow
*Michael Wincott as Top Dollar
*Ernie Hudson as Sgt. Albrecht
*Rochelle Davis as Sarah
*Bai Ling as Myca
*David Patrick Kelly as T-Bird
*Angel David as Skank
*Jon Polito as Gideon
*Tony Todd as Grange
*Sofia Shinas as Shelly Webster
*Michael Massee as Funboy
*Laurence Mason as Tin-Tin
*Anna Levine as Darla
*Bill Raymond as Mickey Marco Rodríguez as Torres

==Death of Brandon Lee==
Brandon Lee died of a gunshot wound on March 31, 1993, after an accidental shooting on set at EUE Screen Gems Studios in Wilmington, North Carolina.
 powder or primer (firearm)|primer, to be loaded in the revolver.  For close-up scenes which utilize a revolver, where the bullets are clearly visible from the front, and do not require the gun to actually be fired, dummy cartridges provide a more realistic appearance than blank rounds, which have no bullet. Instead of purchasing commercial dummy cartridges, the films prop crew, hampered by time constraints, created their own by pulling the bullets from live rounds, dumping the powder charge then reinserting the bullets.   However, they unknowingly left the live percussion primer in place at the rear of the cartridge. At some point during filming the revolver was apparently discharged with one of these improperly-deactivated cartridges in the chamber, setting off the primer with enough force to drive the bullet partway into the barrel, where it became stuck (a condition known as a squib load). The prop crew either failed to notice or failed to recognize the significance of this issue.

In the fatal scene, which called for the revolver to be actually fired at Lee from a distance of 12–15 feet, the dummy cartridges were exchanged for blank rounds, which feature a live powder charge and primer, but no bullet, thus allowing the gun to be fired without the risk of an actual projectile. As the production company had sent the firearms specialist home early, responsibility for the guns was given to a prop assistant who was not aware of the rule for checking all firearms before and after any handling. Therefore, the barrel was not checked for obstructions when it came time to load it with the blank rounds.   Since the bullet from the dummy round was already trapped in the barrel, this caused the .44 Magnum bullet to be fired out of the barrel with virtually the same force as if the gun had been loaded with a live round, and it struck Lee in the abdomen, mortally wounding him. 

He was rushed to the New Hanover Regional Medical Center in Wilmington, NC where he underwent 6 hours of surgery. However, attempts to save him were unsuccessful, and Lee was pronounced dead at 1:03pm on March 31, 1993 at the age of 28. The shooting was ruled an accident.

After Lees death, the producers were faced with the decision of whether or not to continue with the film. Lee had completed most of his scenes for the film and was scheduled to shoot for only three more days.  Sofia Shinas, who had witnessed the accident, did not want to continue and went home to Los Angeles. The rest of the cast and crew, except for Ernie Hudson, whose brother-in-law had just died, stayed in Wilmington. Paramount, who were initially interested in distributing The Crow theatrically (originally a direct-to-video feature), opted out of involvement due to delays in filming and some controversy over the violent content being inappropriate given Lees death. However, Miramax picked it up with the intention of releasing it in theatres and injected a further $8 million to complete the production, taking its budget to approximately $23 million.  The cast and crew then took a break for script rewrites of the flashback scenes that had yet to be completed.  The script was rewritten by Walon Green, Rene Balcer and Michael S. Chernuchin, adding narration and new scenes.  CGI was used to digitally composite Lees face onto a stunt double to complete his few remaining scenes.

==Reception==
===Box office===
The Crow was a sleeper hit at the box office. The film opened at no.1 in The United States in 1,573 theaters with $11,774,332 and averaging $7,485 per theater.  The film ultimately grossed United States dollar|$50,693,129, above its $23 million budget. It ranked at #24 for all films released in the US in 1994 and 10 for R-rated films released that year. 

===Critical response===
The Crow was well received by critics and has a "certified fresh" score of 82% on Rotten Tomatoes based on 50 reviews with an average rating of 7 out of 10. The critical consensus states "Filled with style and dark, lurid energy, The Crow is an action-packed visual feast that also has a soul in the performance of the late Brandon Lee."  The film also has a score of 71 out of 100 on Metacritic based on 14 critics indicating "Generally favorable reviews." 

Reviewers praised the action and visual style.   Rolling Stone called it a "dazzling fever dream of a movie", Caryn James writing for The New York Times called it "a genre film of a high order, stylish and smooth", and Roger Ebert called it "a stunning work of visual style".    The Los Angeles Times praised the movie also.  
 specter will always hang over The Crow".    Berardinelli called it an appropriate epitaph to Lee, Howe called it an appropriate sendoff, and Ebert stated that not only was this Lees best film, but it was better than any of his fathers (Bruce Lee).    Critics generally thought that this would have been a breakthrough film for Lee, although James disagreed.    The changes made to the film after Lees death were noted by reviewers, most of whom saw them as an improvement. Howe said that it had been transformed into something compelling.  James, although terming it a genre film, said that it had become more mainstream because of the changes. 

The film was widely compared to other films, particularly Tim Burtons Batman in film#Tim Burton/Joel Schumacher series|Batman movies and Blade Runner.   Critics described The Crow as a darker film than the others;  Ebert called it a grungier and more forbidding story than those of Batman and Blade Runner, and Todd McCarthy of Variety (magazine)|Variety wrote that the generic inner city of Detroit portrayed in The Crow "makes Gotham City look like the Emerald City". 
 universe he had seen.  McCarthy agreed, calling it "one of the most effective live-actioners ever derived from a comic strip".  Critics felt that the soundtrack complemented this visual style, calling it blistering, edgy and boisterous.    Graeme Revell was praised for his "moody" score;  Howe said that it "drapes the story in a postmodern pall." 

Negative reviews of the film were generally similar in theme to the positive ones but said that the interesting and "OK" special effects did not make up for the "superficial" plot, "badly-written" screenplay and "one-dimensional" characters.  

The Crow is mentioned in Empire (magazine)|Empire s 2008 list of the 500 greatest movies of all time; it ranked at number 468. 

===Accolades=== BMI film MTV Movie Award for Best Song for "Big Empty".  Also at the MTV Movie Awards, the film was nominated for Best Film, and Brandon Lee was nominated for Best Male Performance.  The film received four Saturn Award nominations from the Academy of Science Fiction, Fantasy & Horror Films, USA, for Best Costumes, Best Director, Best Horror film and Best Special Effects. At the Fangoria Chainsaw Awards the film won Best Wide-Release Film and Brandon Lee won Best Actor.

==Soundtracks==
  main theme), The Jesus and Mary Chain, Rage Against the Machine, and Helmet (band)|Helmet, among many others.
 demo form, but it was replaced following Lees death. 
 Medicine and My Life with the Thrill Kill Kult make cameo appearances in the film, on stage in the nightclub below Top Dollars headquarters.

  consists of original, mostly orchestral music, with some electronic and guitar elements, written for the film by Graeme Revell.

==Sequels==
In 1996, a sequel was released, called  . In this film, Vincent Pérez plays Ashe Corven, who, along with his son Danny, is killed by criminals. Ashe is resurrected as a new Crow. The character of Sarah (Mia Kirshner) reappears in this film and assists Ashe.  The film was followed by a television series and two direct-to-video sequels, each with a different person as The Crow.

  was a 1998 Canadian television series created by Bryce Zabel and starring Mark Dacascos in the lead role as Eric Draven, reprising the role originally played by Brandon Lee.
 The Lazarus Heart. After its distributor cancelled the intended theatrical release due to The Crow: City of Angels receiving negative critical reception, The Crow: Salvation was released directly to video with mixed reviews.

The third sequel,  , was released in 2005. Directed by Lance Mungia, it stars Edward Furlong, David Boreanaz, Tara Reid, Tito Ortiz, Dennis Hopper, Emmanuelle Chriqui and Danny Trejo. It was inspired by Norman Partridges novel of the same title. It had a one-week theatrical première on June 3, 2005, at AMC Pacific Place Theatre in Seattle, Washington, before being released to video on July 19, 2005. Like the other sequels, it had a poor critical reception, and it was considered the worst of the four films.

The Crow: 2037 was a planned sequel/reboot written and scheduled to be directed by Rob Zombie in the late 1990s;  however, it was never made.   

==Reboot==
On December 14, 2008,  s Relativity Media is currently negotiating with Edward R. Pressman for both the films rights and financing. {{citation
  | first =
  | authorlink =
  | coauthors =
  | title = The Crow Relaunch Moves Forward With Casting
  | work =
  | publisher = i09
  | date =
  | url = http://io9.com/5410643/the-crow-relaunch-moves-forward-with-casting
  | doi =
  | accessdate =2009-11-25 }}
 
 the film adaptation of Watchmen.  In mid-August 2011, it was announced that Cooper had dropped out due to scheduling difficulties and Mark Wahlberg, who was originally in talks for the lead in 2010, is up for the part, with additional rumors of Channing Tatum or Ryan Gosling possibly taking the role, as well James McAvoy.   
In October 2011 it was reported that Fresnadillo had departed the project as well. 
It was confirmed in January 2012 that Francisco Javier Gutiérrez had signed on to direct the remake,  with Edward R. Pressman and Jeff Most on producing duties. On April 1, 2012, it was announced as an April Fools joke that Skrillex was going to be starring as Eric. 

According to Edward R. Pressman, "The original 1994 Crow film holds a special place in my heart. The current film is a reinvention of James OBarrs graphic novel for the 21st century. Were thrilled to have teamed with director Javier Gutiérrez and screenwriter Jesse Wigutow on this story, which remains true to the core of Eric Dravens plight for revenge. Giving too much away wouldnt be any fun. Disorder, chaos, anarchy — now thats fun!". 

On the news of future remakes, OBarr stated, "  I dont have great expectations. I think the reality is, no matter who you get to star in it, or if you get Ridley Scott to direct it and spend 200 million dollars, youre still not gonna top what Brandon Lee and Alex Proyas did in that first ten million dollar movie."  On April 19, 2013, it was announced that Tom Hiddleston is in talks to play Eric.  That same month, there were reports that Hiddleston is not doing the film but Alexander Skarsgård is being eyed for the part. A week later Skarsgård said hes not attached to the film.  
 Luke Evans is cast as Eric Draven.  Evans revealed to Superhero Hype that the film would be as faithful as possible to the original.  On July 3, 2013, The Crow s creator James OBarr is named as the creative consultant of the film.  On November 21, 2013, Schmoes Know has reports that Norman Reedus is up for the role of the character "James", and Kristen Stewart was up for the part of Shelly; however, she didnt get the role.  In December 2014, the studio hired Corin Hardy to direct the film.   Evans told Den of Geek in an interview that he may not do the film, it was revealed that Evans has dropped out of the film due to other projects.    On February 9, 2015, OBarr told Blastr in an interview that hes interested in Sam Witwer in the role.  The film is set to start production in Spring 2015.  On February 25, 2015, it was reported that Jack Huston would be starring in the film.   On March 14, 2015, OBarr has confirmed to Dread Central at The Lexington Comic and Toy Convention that Huston is cast as Draven in the reboot and at a Q&A on the convention, Jessica Brown Findlay is cast as Shelly Webster.  

==Home video==
First released onto VHS on September 14, 1994. On October 18, 2011, The Crow was released on Blu-ray. The consensus among high-definition enthusiast sites is that the video and audio quality are excellent. 

==See also==
 
*List of film accidents

==References==
{{Reflist|colwidth=30em|refs=

<!--
 {{Citation | last = Brennan | first = Judy | title = Miramaxs Crow Quietly Takes Flight | work = Los Angeles Times | publisher = Tribune Company | date = April 29, 1994 | url = http://articles.latimes.com/1994-04-29/entertainment/ca-51705_1_brandon-lee | accessdate =March 12, 2011
}} 
-->

 {{Citation | last = Ascher-Walsh | first = Rebecca | title = How the Crow Flew | work = Entertainment Weekly | publisher = Time Inc. | date = May 13, 1994 | url = http://www.ew.com/ew/article/0,,302195,00.htm | accessdate =March 12, 2011
}}   Conner & Zuckerman, pp. 35–36 
 {{Citation | last = Brown | first = Dave | title = Filming with Firearms | work = Film Courage | url = http://filmcourage.com/content/filming-firearms | accessdate =August 15, 2014
}} 
 {{Citation | last = Howe | first = Desson | author-link = Desson Howe | title = The Crow (R) | work = The Washington Post | publisher = The Washington Post Company | date = May 13, 1994 | url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/thecrowrhowe_a0b055.htm | accessdate =March 12, 2011
}} 
 {{Citation | last = Ebert | first = Roger | author-link = Roger Ebert | title = The Crow | work = Chicago Sun-Times | publisher = Sun-Times Media Group | date = May 13, 1994 | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19940513/REVIEWS/405130302/1023 | accessdate =March 12, 2011
}}  Wenner Media|date=May 11, 1994|url=http://www.rollingstone.com/movies/reviews/the-crow-19940511|accessdate=March 12, 2011
}} 
 {{Citation|last=James|first=Caryn|title=Eerie Links Between Living and Dead|work=The New York Times|publisher=The New York Times Company|date=May 11, 1994|url=http://movies.nytimes.com/movie/review?_r=1&res=950CEEDE1039F932A25756C0A962958260|accessdate=March 12, 2011
}} 
 {{Citation | last = Rainer | first = Peter | title = Movie Review: The Crow Flies With Grim Glee | work = Los Angeles Times | publisher = Tribune Company | date = May 11, 1994 | url = http://articles.latimes.com/1994-05-11/entertainment/ca-56162_1_brandon-lee | accessdate =March 12, 2011
}} 
 {{Citation | last = Welkos | first = Robert W. | title = Movie Review: Life After Death: A Hit in the Offing? | work = Los Angeles Times | publisher = Tribune Company | date = May 11, 1994 | url = http://articles.latimes.com/1994-05-11/entertainment/ca-56213_1_small-movie | accessdate =March 12, 2011
}}  url = http://www.reelviews.net/movies/c/crow.html | accessdate =March 12, 2011
}}  publisher = Reed Business Information|date=April 28, 1994|url=http://www.variety.com/review/VE1117902661?refcatid=31|accessdate=March 12, 2011
}} 
 {{Citation | last = Hicks | first = Chris | title = The Crow | work = Deseret News | publisher = Deseret News Publishing Company | date = September 20, 2001 | url = http://www.deseretnews.com/familymedia/detail/-4788/The-Crow | accessdate =March 12, 2011
}} 
 {{Citation | last = | first = | author-link = | title = The Crow | work = Montreal Film Journal | publisher = | date = | url = http://www.montrealfilmjournal.com/review.asp?R=R0000132 | accessdate =March 12, 2011
}}  Empire | publisher = Bauer Media Group | date = | url = http://www.empireonline.com/500/6.asp | accessdate =March 12, 2011
}} 
 {{Citation | last = Fox | first = David J. | title = The Crow Takes Off at Box Office | work = Los Angeles Times | publisher = Tribune Company | date = May 16, 1994 | url = http://articles.latimes.com/1994-05-16/entertainment/ca-58401_1_box-offices | accessdate =March 12, 2011
}} 
 {{Citation | title = The Crow (1994) | work = Box Office Mojo | publisher = Amazon.com | url = http://www.boxofficemojo.com/movies/?id=crow.htm | accessdate =March 12, 2011
}}  archiveurl = archivedate = April 7, 2008 | accessdate =March 12, 2011
}}  archiveurl = archivedate = December 17, 2007 | accessdate = March 12, 2011
}} 
   
   
 
}}

 
*{{Citation
  | last = Conner
  | first = Jeff
  | last2 = Zuckerman
  | first2 = Robert
  | title = The Crow: The Movie
  | publisher = Kitchen Sink Press
  | year = 1993
  | isbn = 0-87816-285-2
  | ref =Conner }}
 

==External links==
 
*  
*  
*  
*  
*  
*   at MusicBrainz
*   at MusicBrainz
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 