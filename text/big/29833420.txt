Shikari (2012 film)
{{Infobox film
| name = Shikari
| image = Shikari_2.jpg
| caption =
| director = Abhaya Simha
| producer = K. Manju
| writer = Abhaya Simha Mohan Innocent Innocent
| music = V. Harikrishna Pritam
| cinematography = Dr. Vikram Shrivastava
| editing = Chotta Raman
| studio = K. Manju Films
| distributor =
| released =   
| runtime =
| country = India
| language = Malayalam Kannada
}} Kannada and Mohan and Innocent doing other major roles. The film is the Kannada debut of Megastar Mammootty, and also the Malayalam debut of director Abhay Simha and music director V. Harikrishna. Both Mammooty and Poonam Bajwa play double roles, set in two different timelines.  The movie was opened to a negative responses from the Malayalam version but it got amazing positive reviews from Kannada version.

==Plot==

Software professional Abhijit (Abhilash in Malayalam) (Mammooty) is Arun (Karunan in Malayalam) when he delves in to the pages of a novel written by Gopinath Shindhe (Gopinatha Pillai in malayalam). As Abijit evince interest in knowing more about the past history because the pages in the book are missing. He comes to Manjinadka the beautiful place rich in environment. Abijith comes to the place of Gopinath Shinde house that is almost on the verge of sale by his daughter Nanditha. Interesting here is that Abijith visualizes Nanditha as Aruna in the novel and he explains his intentions of knowing what happened further.
As the story peddles from past to present the emergence of British rule, the tiger suffocating the people life in the village, the mad man surprises Abijith. The twist in the tale comes when the last clue to the past is cremated. Nanditha at this point of time disclose that after she came to know that Aruna is herself in the eyes of Abijit it became obvious for her to create the further missing links to the story. The intention of Nanditha is to get Abijith as her life partner because she is alone in her life. 

==Cast==
* Mammootty as Karunan and Abhilash
* Poonam Bajwa as Nandita and Renuka

==Production==
The title role of the film is played by  . May 26, 2010. Retrieved April 10, 2011  Mammootty
plays a software professional who chances upon a manuscript of an unfinished novel and falls in love with a character.  Poonam Bajwa has been signed in to play the heroine.  She will be playing the love interest of character played by Mammootty. Sharmila Mandre was also approached to do this role.   Aditya, who thinks working with Mammootty is an honour, says about his role: "
"I play a dream-seller and my character is required to appear in the movie on and off". 

==Critical Reception==

===Kannada Version===
The movie received positive reviews.
* G S Kumar of Times of India gave the movie (4/5) and said that " This is not a movie for those who solely enjoy the dishum dishum or running-around-the-tree sequences. Its for movie lovers who enjoy a mix of romance, satire, revolutionary ideas and struggle for freedom, that has been excellently executed by director Abhaya Simha." 
* Indiaglitz gave the movie (4/5) and commented that "Shikari is a very decent film. It is a new experience for the viewers.". 

* Nowrunning gave the movie a strong rating of (3/5). . 

===Malayalam Version===
The movie got negative reviews.
* Times Of India rated the film (1.5/5) and said that "Shikari is redolent of the proverbial grandma tales, one dependent on the viewers suspension of disbelief and the storytellers skill. In the event, the movie fails to keep its readers spellbound." 

==References==
 

==Further reading==
*  
*  
*  

==External links==
*  
*  

 
 
 
 
 