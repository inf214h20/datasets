The Case of the Lucky Legs
{{Infobox film
| name           = The Case of the Lucky Legs
| image_size     = 190px
| image	         = The Case of the Lucky Legs FilmPoster.jpeg
| caption        =
| director       = Archie Mayo
| producer       =
| writer         = Erle Stanley Gardner (novel, uncredited) Jerome Chodorov (adaptation) Brown Holmes Ben Markson
| starring       = Warren William Genevieve Tobin Patricia Eills Lyle Talbot
| music          =
| cinematography =
| editing        =
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Case of the Lucky Legs is a 1935 mystery film, the third in a series of Perry Mason films starring Warren William as the famed lawyer.

==Plot==
A woman wins $1,000 in a pretty legs contest for a company, but has trouble collecting her prize when the promoter turns up dead.

==Cast==
*Warren William as Perry Mason
*Genevieve Tobin as Della Street
*Patricia Eills as Margie Clune
*Lyle Talbot as Dr. Bob Doray
*Allen Jenkins as Spudsy Drake
*Barton MacLane as Detective Bisonette (as Barton Mac Lane)
*Peggy Shannon as Thelma Bell
*Porter Hall as Colonel Bradbury
*Anita Kerry as Eva Lamont Craig Reynolds as Frank Patton
*Henry ONeill as District Attorney Manchester

==DVD Release==

On October 23, 2012, Warner Home Video released the film on DVD in Region 1 via their Warner Archive Collection alongside The Case of the Howling Dog, The Case of the Curious Bride, The Case of the Velvet Claws, The Case of the Black Cat and  The Case of the Stuttering Bishop in a set entitled Perry Mason: The Original Warner Bros. Movies Collection. This is a manufacture-on-demand (MOD) release, available exclusively through Warners online store and only in the US.

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 

 