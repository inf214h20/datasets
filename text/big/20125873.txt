And You Thought Your Parents Were Weird
 
{{Infobox film
| name           = And You Thought Your Parents Were Weird
| image          = AYTYPWW cover art.jpg
| image size     =
| caption        =
| director       = Tony Cookson
| producer       = Just Betzer
| writer         = Tony Cookson
| narrator       =
| starring       = Marcia Strassman Joshua John Miller Edan Gross John Quade Sam Behrens Alan Thicke Susan Gibney A.J. Langer Gustav Vintas Eric Walker Bill Smillie Robert Clotworthy Armin Shimerman Allan Wasserman Susan Brecht Randy Miller Paul Elliott
| editing        = Michael D. Ornstein
| distributor    = Trimark Pictures
| released       = November 15, 1991
| runtime        = 92 minutes
| country        = United States English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

And You Thought Your Parents Were Weird is a 1991 science-fiction family film written and directed by Tony Cookson; foreign language releases were titled RoboDad.

==Plot==
Two boys, Joshua (Joshua John Miller) and Max (Edan Gross) attempt to invent a fully mobile robot with advanced artificial intelligence to help their mother, Sarah (Marcia Strassman) with household chores.  However, after a playfully performed séance on Halloween, the ghost of their late father, Matthew (Alan Thicke) possesses the robot. The boys are overjoyed at the return of their father, but it soon becomes apparent that the people who stole their fathers work are after their robot, Newman.  Eventually, Matthew returns to the afterlife after setting his boys on the right path as they sell the plans for their robot to a rich Texan investor.

==External links==
*   at IMDB

 
 
 
 
 


 
 