Just Around the Corner (1921 film)
{{infobox film
| name           = Just Around the Corner
| image          =File:Just Around the Corner poster.jpg
| imagesize      =
| caption        =Film poster
| director       = Frances Marion
| producer       = William Randolph Hearst (thru Cosmopolitan Productions)
| writer         = Frances Marion (scenario)
| based on       =  
| starring       = Margaret Seddon Lewis Sargent Sigrid Holmquist
| cinematography = Henry Cronjager
| editing        =
| distributor    = Paramount Pictures
| released       = December 11, 1921
| runtime        = 7 reels (6,173 feet)
| country        = United States
| language       = Silent (English intertitles)

}}
Just Around the Corner is an extant 1921 American silent drama film produced by William Randolph Hearsts Cosmopolitan Productions and distributed through Paramount Pictures. The film is based on a short story, Just Around the Corner, by Fannie Hurst and was directed by Frances Marion, a prolific Hollywood scenarist. Director Frances Marion is the star in this rare directorial turnout from her. 

The cast are competent silent actors but no big names. Sigrid Holmquist came from Sweden but was no Garbo and Fred Thomson had married Marion in 1919, and later became a big cowboy star. Marion directed one other picture, her friend Mary Pickfords better-known The Love Light (1921).  

==Cast==
*Margaret Seddon - Ma Birdsong
*Lewis Sargent - Jimmie Birdsong
*Sigrid Holmquist - Essie Birdsong
*Edward Phillips - Joe Ullman
*Fred Thomson - The Real Man
*Peggy Parr - Lulu Pope
*Rosa Rosanova - Mrs. Finshreiber
*William Nally - Mr. Blatsky

==Preservation status==
This film exists in the collection of the Library of Congress.  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 