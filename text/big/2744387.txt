Starfire video prototype
Starfire SunSoft sought to create a more realistic look at how computer technology and interfaces would improve. The project drew together the talents of more than 100 engineers, designers, futurists, and filmmakers in an effort to both predict and guide the future of computing.

The film is set in the year 2004 and features a protagonist interacting by voice, mouse, and stylus with a   . The story concerns an executive at an auto-maker who must make a compelling presentation for her design.
 

The video predicted the rise of a new technology that would become known as the World Wide Web (board room scene).

Popular Science Magazine reported, in March 2009, that Microsoft had just produced a new video showing life in the year 2019: "The 2019 Microsoft details with this video is almost identical to the 2004 predicted in this video produced by Sun Microsystems in 1992."

In addition to the film, the project also produced:
* Tog on Software Design, which not only covers the film in intimate detail, but lays out several more equally thought-provoking scenarios, even if they were not enshrined in celluloid.
* Starfire, the Paper, published in the CHI Proceedings, outlining the rules followed in attempting to build a scientifically legitimate video prototype, as opposed to simply confabulating a fanciful, but non-implementable, vision.

The film was released as the Starfire video, in NTSC format, and later made available as part of a collection of human-computer interaction videos.

== Similar ==
* Microsoft Surface - uses similar technology  such as multi touch screen
* Corning Inc.|Cornings 2011 A Day Made of Glass  promotional video has similar elements

==References==
 

== External links ==
*  .
*  , includes the full original video.
*  —a review of Starfire in Wired Magazine issue
*  —author: Will Kreth.

 
 
 
 


 