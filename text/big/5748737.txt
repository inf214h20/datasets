Fudge 44
{{Infobox film
| name        = Fudge 44
| image       = The Puppets from Fudge 44.jpg
| caption     = The Puppets from Fudge 44  Graham Jones 
| producer    = José Naghmar  Graham Jones Garret Sexton 
| starring    = Terauchi Aritomo Hatoyama Ichiro Ito Keigo Miki Kiyotaka Sakichi Masatake Yudia Masatake Ohira Masayoshi Yonai Mitsumasa Yutaka Takashi Kuroda Takeo Katsura Taro Yamagata Toyoda Toshiki Uchimura Toyoda Uchimura 
| movie_music = Kevin MacLeod Oliver Reichardt WC Turner 
| cinematography = John G  Maarse
| editing     = Joe Sharngam
| distributor = Marcs Floor
| runtime     = 71 minutes
| country     = Ireland Japanese with English translation  
| awards      = BACKSEAT FILM FESTIVAL PHILADELPHIA 07, RHIFF TORONTO 06
|}}
 Irish director Graham Jones. It is a mockumentary about six puppets in an insolvent Tokyo childrens puppet theatre who locals believe came to life and robbed a nearby bank to avoid being put out of business. 

The Irish premiere took place on June 24, 2006 at the 7th International Darklight Festival,   the Canadian premiere at RHIFF in Toronto on June 20 where it won an experimental award and the World Premiere at The Delray Beach Film Festival in Florida on March 10.   The film was also winner of the 2007 Most Original Film Award at The Backseat Film Festival in Philadelphia and nominated for a 2006 Irish Digital Media Award.

It was suggested by some that the shooting technique adopted by Jones, which involved falsely translating Japanese interviewees, was questionable.

Jones earlier film How To Cheat In The Leaving Certificate was also controversial - leading to condemnation by then Junior Minister for Education Willie ODea.

==References==
 

==External links==
* 
* 

 
 
 


 
 