The Pruitt-Igoe Myth
{{Infobox film
| name        = The Pruitt-Igoe Myth: An Urban History
| image       = File:Pruitt-Igoe Myth, documentary poster.jpg
| caption     = 
| director    = Chad Freidrichs
| producer    = Paul Fehler, Chad Freidrichs, Jaime Freidrichs, Brian Woodman
| writer      = Chad Freidrichs, Jaime Freidrichs
| starring    = 
| distributor = 
| released    =  
| runtime     = 79 minutes
| country     = United States
| language    = English
| budget      = 
| gross       = $44,683 
}}

The Pruitt-Igoe Myth is a 2011 documentary film detailing the history of the Pruitt–Igoe public housing complex in St. Louis, Missouri, and the eventual decision to implode the entire complex in 1976. 

The documentary argues that the violent social collapse within the Pruitt-Igoe complex was not due to the demographic composition of its residents,  but was a result of wider, external social forces, namely the declining economic fortunes of St. Louis and the resulting impact upon employment opportunities. 

==Plot== modernist style as thirty-three 11-floor buildings.     
 planned implosion between 1972 and 1976.    

The film takes issue with the various explanations for the failure of the Pruitt-Igoe complex that have developed since the demolition, including that it was the fault of the modernist theory of architecture itself (an explanation developed by architectural historian Charles Jencks ) or of the general concept of public housing.  

Instead, the explanation offered is that the fate of the Pruitt-Igoe project was determined by the declining population and industrial base in St. Louis after World War II.   The documentary argues that this process left few jobs for the remaining residents, thus reducing funds available for maintenance and security of the buildings,  which were planned to be paid for by tenant rents even as residents grew poorer and the living conditions in the project deteriorated. 

==Reception== Best Documentary Film. It also won the International Documentary Associations ABC News VideoSource Award for best use of archival footage.

==See also==
*Pruitt–Igoe
*Urban decay
*History of St. Louis (1905–80)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 