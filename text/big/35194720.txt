Kamui Gaiden
{{Infobox film
| name           = Kamui Gaiden
| image          = Kamui_Gaiden_ME.jpg
| image size     = 220px
| caption        = Cover of the Manga Entertainment release
| director       = Yōichi Sai
| producer       = Akira Morishige Yui Tamae
| writer         =
| screenplay     = Yōichi Sai Kankurō Kudō
| story          = Sampei Shirato
| based on       =  
| narrator       =
| starring       = Kenichi Matsuyama Koyuki Ekin Cheng Kōichi Satō Hideaki Itō
| music          = Taro Iwashiro
| cinematography = Tomoo Ezaki Junichi Fujisawa
| editing        = Isao Kawase
| studio         = Horipro Shochiku Shogakukan Shueisha Yahoo Japan
| distributor    = Shochiku (Japan)   Funimation (USA)      Manga Entertainment (UK)    Madman Entertainment (Australia) 
| released       = September 16, 2009 
| runtime        = 120 min
| country        = Japan
| language       = Japanese
| budget         = $18 million 
| gross          = $11,891,739 
}}
 of the same title. The film is written by Sai and Kankurō Kudō, starring Kenichi Matsuyama in the titular role. It premiered at Toronto International Film Festival on September 16, 2009,  to mixed reviews.

==Plot==
The young Kamui is a runaway ninja who has abandoned his clan, now constantly pursued by assassins. His travels bring him to a seashore village where he meets Hanbei, a fisherman who shares the former ninjas sense of honor. Though Hanbeis wife is wary of the stranger, the fisherman and Kamui become good friends. Life at the seaside seems idyllic but Kamui does not get to enjoy the peace for very long when his past life is catching up on him, and everything and everyone is not as it seems. Now he must draw upon his shadowy arts if he hopes to escape with his life.

==Cast==
*Kenichi Matsuyama as Kamui Kaoru Kobayashi as Hanbei
*Koyuki as Sugaru
*Kōichi Satō as Gumbei
*Hideaki Itō as Fudo
*Suzuka Ohgo as Sayaka
*Ekin Cheng as Dumok
*Sei Ashina as Mikumo
*Yûta Kanai as Yoshito
*Yukio Sakaguchi as Imperial Guard

==Production and release==
Kamui Gaiden was filmed in 2007-2008 at   and Blu-ray Disc releases by Manga Entertainment;  it was also re-titled as Kamui - The Last Ninja by Cine-Asia in Germany. 

==Reception==
English-language critical reception of the film was generally mixed. Rob Nelson of   offers a more compelling scenario and characters and that is not saying much."  Heroic Cinema, rating the film 6/10, stated that "when it comes to a movie that could have been great —should have been great— that’s a whole new level of disappointment."  Cinema Verdict reviewer rated it 5/10, also finding Kamui to be a disappointment, "built upon a story and characters who didn’t capture my attention with its overall fake look."  Nevertheless, James Mudge of BeyondHollywood.com wrote that Kamui "stands out as one of the better examples of the ninja genre," stating that Sai brought "Sanpei Shirato’s vision to life without too much grand standing, and the film is all the more enjoyable for paying equal amounts of attention to character and excitement." 

==See also== manga series.

==References==
 

==External links==
*   
*  at the Internet Movie Database

 

 
 
 
 
 
 
 
 