Shunyo
 
 

{{Infobox film
| director       = Rudranil Ghosh Workshop
| writer         = Ansuman Chakraborty Arunava Khasnobis
| starring       = Anjan Dutt Rudranil Ghosh
| music          = Neel Dutt
| cinematography = Supriyo Dutta Workshop
| released       =  
| country        = India Bengali
| budget         =  
| gross          = 
| film name = শূণ্য
}}

Shunyo (Bengali: শূণ্য; English: Zero) is an upcoming Bengali film directed by Rudranil Ghosh, produced by Workshop Productions Pvt. Ltd. and starring Anjan Dutt.

== Plot ==
The story actually revolves around the daughter of a frontline businessman of the city named Neha Jaiswal, who is desperate to tie her knot with Gogol, a boy belonging to a middle-class family. They go on a college trip to Kalimpong but however they does not return alive. The mystery starts at this very moment. The character of Neha is played by newcomer Rai and Gogol is played by Debanjan. “It was actually so great working with Rudra Da (Rudranil Ghosh). He taught us every moment what to do and what not to. It was more of a learning experience for us”, said Rai who has also appeared for his class 10 CBSE board exams this year.

The film also features veterans like Anjan Dutta and Kaushik Ganguly. Anjan Dutta plays a political leader where as Kaushik played the role of Animesh Ganguly, a city based lawyer who tries to solve the mystery. Several well known faces from Nepalese and Hindi theatre backgrounds also appear in the film. Rajatava Dutta and Tanushree Chakrabory are also playing other important characters.

The screenplay of the film is written by Rudranil, Parambrata Chatterji, Angshuman Chakroborty and Arunava. The music and the background score is made by Neel Dutt whereas the theme song of the film is composed by Rupam Islam.

The film is produced by Apurba Saha of the Pailan Group.
== Cast ==
* Rudranil Ghosh
* Anjan Dutt as Dada
* Kaushik Ganguly as Animesh Goswami
* Tanushree Chakraborty as Subha Rai || Neha Jaiswal

== Soundtrack ==
The films soundtrack is to be composed by Neel Dutt.

== References ==
*  

 
 
 


 