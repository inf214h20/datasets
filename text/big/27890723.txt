Wedding in Blood
 
{{Infobox film
| name           = Wedding in Blood
| caption        = Film poster
| image	         = Wedding in Blood FilmPoster.jpeg
| director       = Claude Chabrol
| producer       = André Génovès
| writer         = Claude Chabrol
| starring       = Stéphane Audran Michel Piccoli
| music          = Pierre Jansen
| cinematography = Jean Rabier
| editing        = Jacques Gaillard Monique Gaillard
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
}}
Wedding in Blood ( ) is a 1973 French thriller film directed by Claude Chabrol.   It was entered into the 23rd Berlin International Film Festival.   

==Plot==
Pierre Maury, a local politician in a small French village, is the lover of Lucienne Delamare. Her husband is the local mayor and he eventually comes across this secret relationship. Instead of being jealous Paul Delamare reacts by taking advantage of this situation. He blackmails Pierre Maury.

==Cast==
* Stéphane Audran as Lucienne Delamare
* Michel Piccoli as Pierre Maury
* Claude Piéplu as Paul Delamare
* Clotilde Joano as Clotilde Maury
* Eliana De Santis as Hélène Chevalier, Luciennes daughter
* François Robert as Auriol
* Daniel Lecourtois as Prefet / Department governor
* Pippo Merisi as Berthier
* Ermanno Casanova as the counseller

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 