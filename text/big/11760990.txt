It2i2
{{Infobox film
| name           = it²i²
| image          = it2i2_DVD.jpg
| caption        = The cover of the DVD, and the DVD itself
| director       = Robert Llewellyn
| producer       = Robert Llewellyn
| writer         = Robert Llewellyn
| starring       = Robert Llewellyn  Tony Hawks
| music          = 
| cinematography = 
| editing        = Robert Llewellyn
| distributor    = 20th Century Fox
| released       =  
| runtime        = 43 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
it²i² is a 2006 independent mockumentary from comedian and Red Dwarf actor Robert Llewellyn. A direct to DVD release, it was released in March, 2006. In the film, Llewellyn plays two roles. Himself, and "uber nerd" John Silverstine.

The it²i² DVD was released in March 2006. It includes the movie, with some special features and each copy is hand signed by Robert Llewellyn .

==Story==
Scientists have been working on creating Artificial Intelligence, knowing that there are approximately 50 billion synaptic connections between neurons in the average human brain. They have failed, and during the timespan, many millions of computers have linked up together over the internet. In March, the fifty billionth computer went online, which was Robert Llewellyns new Macintosh computer. All the interlinked computers begin reaching the complexity of a human brain. Then, the monstrously obese computer programmer John Silverstine noticed the link up. He called it "it²i²", for, following as René Descartes stated, "I think, therefore, I am." So it²i² agrees, "It thinks, therefore, it is." Robert Llewellyn was making a documentary for the BBC about geek culture, when he became aware of this.

==Production==
The idea for it²i² was created in Los Angeles in the year 1999. Robert Llewellyn was at a conference about "new media". The dot com boom was at its peak. More and more computers were going online. In 2002, Robert Llewellyn decided after the successful release of his DVD comedy WomanWizard, to create the mockumentary. He wrote fifteen drafts of the script, and got a professional cameraman to help him in September, 2005. It²i² was shot using a Sony Z1 high definition camera and then edited on three Macintosh computers, and several professional movie making programs. The movie was mostly shot in actors homes or offices.
The DVD also contains bonus material, included in which is an interview with cybernetics professor Kevin Warwick. Additional writing, special effects, and art production and additional writing was contributed by Mark S Lowe, with additional special effects and editing by Charlie Dancey.

==References==
 
 

==External links==
* 
* 

 
 
 
 
 
 