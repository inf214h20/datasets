The Orator
 
{{Infobox film
| name           = The Orator
| image          = The Orator.jpg
| caption        = 
| director       = Tusi Tamasese
| producer       = Catherine Fitzgerald
| writer         = Tusi Tamasese
| narrator       = 
| starring       = Faafiaula Sagote Tausili Pushparaj
| music          =  , 10 November 2011 
| cinematography = Leon Narbey Simon Price 
| distributor    = Transmission Films
| released       =  
| runtime        = 110 min
| country        = Samoa New Zealand
| language       = Samoan
| budget         = 
| gross          = 
}}
The Orator ( ) is a 2011   at the 84th Academy Awards,    but it did not make the final shortlist.    It is the first time New Zealand have submitted a film in this category.    

==Plot== Samoan culture". 
 
  It shows Samoans "surrounded by family and support", in accordance with faaSamoa (the "Samoan way"). 
The   has described it as "a beautiful and poignant love story" which brings "the finest aspects of traditions of our Samoan culture into the international spotlight". 

The main character, Saili, a "simple villager", a taro farmer and a dwarfism|dwarf, must "find the strength" to "defend his land and family, which are threatened by powerful adversaries". "He ultimately attempts to reclaim his fathers chiefly status, even if the current ageing village chief does not believe he has the physique or the oratory skill required."        

Tamasese described his film as "my image of what I see of growing up in Samoa", and "a bit like a tour. You get thrown into this place and you are seeing things", witnessing aspects of Samoan life without explanation - such as evening prayer time (sa), or ritual   (ifoga). 

==Cast==
* Faafiaula Sagote as Saili
* Tausili Pushparaj as his wife Vaaiga
* Salamasina Mataia as Vaaigas daughter Litia
* Ioata Tanielu as Vaaigas brother Poto 

==Production and distribution== Tusi Tamasese (previously the writer and director of short film Va Tapuia), The Orator is produced by Catherine Fitzgerald, shot by Leon Narbey, and financed by the New Zealand Film Commission and by the Samoan government. Maiava Nathaniel Lees and Michael Eldred are associate producers, and Samoan chief Manu Asafo served as cultural advisor.  

Filming was completed in January 2011, with the film scheduled to be screened in cinemas later in the year. 

The Orator "will be distributed in New Zealand, Australia and the Pacific by Transmission Films", while NZ Film will handle distribution beyond Oceania.  Misa Telefoni, who is also Samoan Minister for Tourism, has expressed hope that the film will attract international attention to Samoa, and promote the country as a tourist destination. 
 Venice Film Festival, in Italy on 3 September 2011. This was the first time that Samoa were represented at the festival and saw a return for New Zealand after a 4 year absence (the last film shown at the festival was Cargo (2007 film)|Cargo). 

The Orator premiered in Samoas only cinema -Magik Cinema in Apia- on 1 October 2011. It will be screened in cinemas in New Zealand on 6 October. 

==Reception==
The New Zealand Herald noted:
:"Thanks in large part to the almost edibly gorgeous cinematography of  ; water runs mercury-silver off taro leaves; tiny details like a snail on a gravestone are lingered over lovingly. The sound design is equally precise and evocative.
:Importantly, its a film of great patience and watchfulness. Shots of 10 seconds are the rule, not the exception; you can feel your heartbeat slow as you watch it." 

Variety (magazine)|Variety described it as "a compelling drama", an "exploration of love, death and bitter family conflict that unfolds in sync with the relaxed rhythms of Pacific island life", and an "ausipicious debut" for Tamasese. Sagote, in the lead role, was "tremendously soulful-eyed", while Pushparaj was "excellent", "exuding the dignity of a queen":
:"Tamasese gradually weaves the separate story strands together (fittingly, given the centrality of straw mats that various characters weave and give as gifts in the story), culminating in a scene in which Saili must make a ceremonial oration after a tragedy - a touchingly crafted and performed sequence that grips as drama and as an insightful look at the Samoan way of life.
:Script offers an insiders view of a society that just about keeps a lid on simmering violence through complex, ritualized forms of group interaction and humor, a portrait that goes some way toward exploding the myth of Samoans as peace-loving, noble-savage proto-hippies. Balance of cultural insights and storytelling makes for a universally appealing yarn that renders the exotic comprehensible, although the films stately pace may prove challenging to viewers with shorter attention spans." 

The Hollywood Reporter praised the film as "a beautifully nuanced debut" with a "deeply moving climax", which both "succeeds on one level as an insider’s intricate cultural study" and "is powered by a slow-burning underdog drama that canvasses weighty themes of family honor, courage and redemption". It also praised Sagote as "already a master of non-verbal communication. Quiet and watchful, he speaks volumes with his eyes". 

The Camden Advertiser encouraged readers to see the film, saying its most interesting aspect was "the observation of the intricacies of the Samoan culture - which hitherto had not been committed to cinema" and which are conveyed "very subtly within a very universal tale". 

Writing for the New Zealand Herald, Samoan New Zealander Cherelle Jackson said the film was culturally accurate, touching and "beautifully done", and that it "makes no pretence, it doesnt make the Samoan culture look beautiful and admirable, it draws out the violence, the hatred, the slanted hierarchy and ultimately the discriminating nature of our people in a story line that happens in real life".   

The West Australian wrote: "The Orator is a fascinating journey into Samoan life, but the slow pace of this way of life does make for a slow-paced film. There are lots of scenes where Saili is shown deep in thought, or his wife Vaaiga is sitting weaving the mats that are a major cultural occupation. The Orator does have its dramatic highpoints and the verbal-jousting conclusion is well staged by the cast of Samoans, who were drawn from the villages on Upolu". 

==Awards==
Special Mention from the jury of the Orizzonti section of the Venice International Film Festival; Art Cinema Award from the CICAE jury of the Festival; CinemAvvenire Best Film Award from the jury of the Associazione Centro Internazionale CinemAvvenire;  shortlisted for the Best Feature Film Script Award of the Script Writers Awards of New Zealand; Audience Award at the Brisbane International Film Festival.  , New Zealand Film Commission press release, 2 December 2011 
 2012 Sundance International Film Festival". 

Faafiaula Sagote: finalist for the Best Performance by an Actor Award at the 5th Asia Pacific Screen Awards. 

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of New Zealand submissions for the Academy Award for Best Foreign Language Film

==External links==
*   on NZonScreen
* 
*   Official website

==References==
 

 
 
 
 
 
 
 