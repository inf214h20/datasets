Durian Durian
 
 
 
{{Infobox film
| name           = Durian Durian 榴槤飄飄
| image          = DurianDurian.jpg
| image_size     = 
| caption        = Movie poster for Durian Durian
| director       = Fruit Chan
| producer       = Carrie Wong
| writer         = Fruit Chan, Chan Wai-Keung, Zhi Min Sheng
| narrator       = 
| starring       = Mak Wai Fan, Qin Hailu
| music          = Chu Hing-Cheung, Lam Wah-Chuen
| cinematography = Lam Wah-Chuen
| editing        = Tin Sam-Fat
| distributor    = 
| released       = 2000-11-16
| runtime        = 116 Hong Kong Mandarin
| budget         = 
| gross          = HK$523,015.00 
| preceded_by    = 
| followed_by    = 
}} 2000 Cinema Hong Kong film directed by Fruit Chan.  The film portrays the experiences of a young girl, Fan (Mak Wai-Fan) and her sex worker neighbour, Yan (Qin Hailu) in Hong Kong.

==Plot==
Yan is a prostitute from the mainland in Hong Kong, living near Fan and her family, who is staying in the area illegally. Yan meets Fan in a laneway behind Portland Street and become friends after Yans pimp is assaulted in front of Fan by an assailant wielding a durian fruit.

Yan services dozens of clients per day and showers compulsively.  After her 3-month-stay in Hong Kong, Yan returns to her family and her ex-fiance in Northeast China to invest what she has earned.    Yan remains in contact with Fan, receiving a durian from her as a gift.

==Connection to Fruit Chans other work==
Fan was featured in Little Cheung, a film which also deals with poverty and life as an immigrant. This film also centres upon Portland Street in Kowloon. 

==Awards== Golden Horse Awards, with Qin winning the Best Actress and Best New Performer awards. 

==Reception==
In addition to numerous awards, the film has received critical acclaim.  Reviews have praised director Fruit Chan and actresses Qin Hailu and Mak Wai-Fan, and emphasise themes of contrast, urban squalor, youthful optimism, and alienation.       The film as been called "deliberate and brooding".   

==See also==
* Prostitution in Hong Kong

==References==
 
 

==External links==
* 
* 
* 
*  
*  

   
{{succession box
| title = Golden Horse Awards for Best Film
| years = 2001
| before= Crouching Tiger, Hidden Dragon The Best of Times
}}
 

 
 

 
 
 
 