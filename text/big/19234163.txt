Private Potter
{{Infobox Film
| name           = Private Potter
| caption        = 
| image	=	Private Potter FilmPoster.jpeg
| director       = Casper Wrede
| producer       = Ben Arbeid 
| writer         = Casper Wrede, Ronald Harwood Ronald Fraser James Maxwell Frank Finlay 
| music          = George Hall    
| cinematography = Arthur Lavis   
| editing        = John Pomeroy    
| distributor    = 
| released       = 1962
| runtime        = 89 min.
| country        = 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        =
}} 1962 British Ronald Fraser, James Maxwell.

==Plot==
The eponymous Private Potter is a soldier who claims that the reason he cried out leading to the death of a comrade was that he saw a vision of God. There is then a debate over whether he should be court-martialled.

==Production== ITV in RADA and the leading role of the fragile young soldier who wilts under pressure was his first film appearance.

==Cast==
* Tom Courtenay as Private Potter 
* Mogens Wieth as Yannis  Ronald Fraser as Doctor  James Maxwell as Lieutenant Colonel Harry Gunyon 
* Ralph Michael as Padre 
* Brewster Mason as Brigadier 
* Eric Thompson as Captain John Knowles  John Graham as Major Sims 
* Frank Finlay as Captain Patterson 
* Harry Landis as Lance Corporal Lamb  Michael Coles as Private Robertson 
* Jeremy Geidt as Major Reid 
* Fulton Mackay as Soldier

== External links ==
* 

 
 
 
 
 
 


 