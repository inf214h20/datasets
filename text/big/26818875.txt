What's Your Number?
 
{{Infobox film
| name           = Whats Your Number?
| image          = Whats Your Number? Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Mark Mylod
| producer       = Beau Flynn Tripp Vinson
| screenplay     = Gabrielle Allen Jennifer Crittenden
| based on       =   Chris Evans Ari Graynor Dave Annable Joel McHale Ed Begley, Jr. Blythe Danner Heather Burns Eliza Coupe Kate Simses Tika Sumpter Oliver Jackson-Cohen
| music          = Aaron Zigman
| cinematography = J. Michael Muro
| editing        = Julie Monroe
| studio         = Regency Enterprises
| distributor    = 20th Century Fox
| released       =  
| runtime        = 106 minutes  
| country        = United States
| language       = English
| budget         = $20 million  
| gross          = $30,426,096 
}} Chris Evans. It is based on Karyn Bosnaks book 20 Times a Lady. The film was released on  , 2011.

==Plot== Chris Evans) into her apartment so that way Roger will leave. It turns out that Colin was only over to avoid a girl that he slept with, because he doesnt want to give the women he sees any expectations. Ally then runs into "Disgusting Donald" (Chris Pratt), her once overweight ex-boyfriend whos now successful and good-looking. She decides to track down all of her ex-boyfriends in the hope that one of them will have grown into the man she wants to marry, and therefore the number of men she has slept with will never have to increase. She gets help from Colin in exchange for letting him stay in her place after his one-night stands, but things do not quite work out the way she had expected and Ally remembers why it didnt work out with those men in the first place. After a falling out with Colin, Ally thinks she finally finds her match - an old flame, Jake Adams (Dave Annable). She attends her sisters wedding with Jake only to realize that the man she truly loves is Colin. Ally runs across the city to find Colin and the two reunite with a kiss. Afterwards, Ally gets a call from an old boyfriend (Aziz Ansari) telling her that they in fact did not sleep together. Ally rejoices in the fact that Colin is indeed the 20th and last man she ever slept with.

==Cast==
 
* Anna Faris as Ally Darling Chris Evans as Colin Shea
* Ari Graynor as Daisy Anne Darling
* Blythe Danner as Ava Darling
* Ed Begley, Jr. as Mr. Darling
* Oliver Jackson-Cohen as Eddie Vogel
* Dave Annable as Jake Adams
* Heather Burns as Eileen
* Eliza Coupe as Sheila
* Tika Sumpter as Jamie
* Joel McHale as Roger the Boss
* Chris Pratt as Disgusting Donald
* Denise Vasi as Cara
* Zachary Quinto as Rick
* Mike Vogel as Dave Hansen
* Martin Freeman as Simon
* Andy Samberg as Jerry Perry Thomas Lennon as Dr. Barrett Ingold
* Anthony Mackie as Tom Piper
* Ivana Miličević as Jacinda
* Aziz Ansari as Jay
 

==Release==

===Box office===
Whats Your Number? grossed $5,421,669 in its opening weekend at #8  and grossed a domestic total of $14,011,084 by the end of its run almost two months later. The worldwide total is $30,426,096.   

===Critical reception===
The film received generally negative reviews with a 24% "rotten" rating at   is "generally unfavorable", receiving a score of 35 from 31 professional critics. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 