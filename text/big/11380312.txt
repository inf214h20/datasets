The Devil's Eye
 
{{Infobox film
| name           = The Devils Eye
| image          = The_Devils_Eye_by_Bergman.jpg
| caption        = Film poster
| director       = Ingmar Bergman
| producer       = 
| writer         = Ingmar Bergman
| starring       = Jarl Kulle Bibi Andersson Stig Järrel
| music          = 
| cinematography = 
| editing        = Oscar Rosander
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

The Devils Eye ( ) is a 1960 Swedish fantasy-comedy film written and directed by Ingmar Bergman.   

==Plot==
The devil has a stye in his eye, caused by the purity of a vicars daughter. To get rid of it, he sends Don Juan up from hell to seduce the 20-year-old Britt-Marie and to rob her of her virginity and her belief in love. She however can resist him and things even get turned around when Don Juan falls in love with her. The fact that he feels love for the first time now makes him even less attractive to her and Don Juan returns to hell.

==Cast==
* Jarl Kulle as Don Juan
* Bibi Andersson as Britt-Marie
* Stig Järrel as Satan
* Nils Poppe as The Vicar
* Gertrud Fridh as Renata
* Sture Lagerwall as Pablo
* Georg Funkquist as Count Armand de Rochefoucauld
* Gunnar Sjöberg as Marquis Giuseppe Maria de Macopanza
* Gunnar Björnstrand as The Actor

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 


 
 