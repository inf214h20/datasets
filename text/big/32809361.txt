Johnny One-Eye
{{Infobox film
| name           = Johnny One-Eye
| image_size     =
| image	=	Johnny One-Eye FilmPoster.jpeg
| caption        =
| director       = Robert Florey
| producer       = Benedict Bogeaus (producer) James Stacy (assistant producer)
| writer         = Richard H. Landau Damon Runyon (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Lucien N. Andriot
| editing        = Frank Sullivan
| distributor    =
| released       = 1950
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Johnny One-Eye is a 1950 American film directed by Robert Florey.

== Plot summary == Pat OBrien) has become the target of a politically ambitious district attorney, who has offered immunity for Martins former partner in crime Dane Cory in exchange for his testimony. After being informed about the deal and narrowly escaping arrest, Martin pays a visit to Cory to persuade him not to testify. The meeting ends up with a shootout, with Martin killing one of Corys henchmen and being hit himself before fleeing. With his picture on newspaper front pages and a reward on his head, Martin decides to hide in an abandoned house. While recovering to prepare a final assault on Cory, he adopts an injured dog that strays into his hideout and names him Johnny One-Eye.

== Cast == Pat OBrien as Martin Martin Wayne Morris as Dane Cory
*Dolores Moran as Lily White
*Gayle Reed as Elsie White Donald Woods as Vet
*Barton Hepburn as Cory Henchman
*Raymond Largay as Lawbooks
*Lawrence Cregar as Ambrose
*Forrest Taylor as Man on Street who quotes Lord Byron
*Lester Allen as Designer-Choreographer Jimmy Little as Captain of Police
*Jack Overman as Lippy
*Lyle Talbot as Official from District Attorneys Office
*Harry Bronson as Cute Freddy

== Soundtrack ==
 

== External links ==
* 
* 
 

 
 
 
 
 
 
 
 
 


 