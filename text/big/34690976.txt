Kalpana (2012 film)
{{Infobox film
| name = Kalpana
| image = Kalpana Poster.jpg
| caption = Theatrical release poster
| director = Rama Narayanan
| producer = Sri Thenaandaal Films
| writer = Raghava Lawrence
| Based on Kanchana, the tamil film 
| dialogues = Dwarki
| starring = {{plainlist | 
* Upendra
* Lakshmi Rai Saikumar
* Babu Antony
}}
| music = V. Harikrishna
| cinematography = K. S. Selvaraj
| editing = 
| background score = 
| distributor =  
| released =  
| runtime = 
| country = India
| language = Kannada
| budget =  4 Crore
| gross =  11 Crore    (Satellite & Audio Rights :  2 Crore) 
}}
 Saikumar and Tamil film Saikumar reprises Tamil and Telugu versions. Shruti and Umashri play supporting roles originally played by Devadarshini and Kovai Sarala.
 Sixth Highest Udaya Film Award for Best Male Actor for his performance in the film. 


==Plot==
Raghava (Upendra) is a typical jobless youth who spends his days playing cricket with friends. He suffers from an irrational fear of ghosts, and retreats to the safety of his home after sunset. So great is his fear, that he prefers to sleep with his mother (Umashri) and have her accompany him to the bathroom at night. This creates major annoyance in the household, including Raghavas brother (Achyuth Kumar), sister-in-law (Shruti (actress)|Shruti) and their children.

One day, Raghava and his friends are forced to abandon their usual cricket ground and find a new one; they unwittingly select an abandoned ground which is rumored to be haunted. A bizarre weather change scares them away. Raghava brings home his cricket stumps, which have been stained with blood from a buried corpse in the ground. He focuses on wooing Priya (Lakshmi Rai), the sister of his sister-in-law. In the following days, his mother and sister-in-law are witness to several paranormal phenomena at night; prominently a ghost haunting the hallways. On consulting a priest, they perform 3 rituals to ascertain if the house is haunted:

1. They keep a coconut on a rangoli and pray to Lord Shiva. The coconut rotates on its own.

2. They make a cow eat food. The cow runs out of the house without eating the food. 

3. They leave a lit lamp and two drops of blood and leave the house. A ghost of a woman appears before them and licks the blood.

Scared senseless, Raghavas mother and sister-in-law hire two priests ( , who successfully drives the spirit away from Raghavas body. The ghost of the woman, trapped, reveals her story.
 MLA Shankar. Kalpana angrily confronts the MLA, who cunningly kills her. He also kills Babu Antony and his son. Before she died, she vowed to kill Shankar, his wife, and his henchmen. The bodies are then buried in Kalpanas own ground.
 symbiotically in Raghavas body to help him out when the need rises.

==Cast==
* Upendra as Raghava Saikumar as kalpana
* Lakshmi Rai Priya Shruti as Jaanki
* Umashri as Upendras mother
* Satyajit
* Shobraj
* Babu Antony
* Achyuth Kumar as Jaankis husband
* Om Prakash Rao in a Special Appearance
* Bullet Prakash in a Special Appearance

==Production== Sai Kumar was cast to play a special role in the film which was played by actor Sarath Kumar in the original. Umashri, Shruti and Achyut Kumar were also cast in supporting roles in the movie. 

==Songs==
The music for the film was composed by V. Harikrishna.
* "Nillu Maga Nillu Maga"
* "Bayanno Bambada"
* "Hei Madana Kamaraya"
* "Bandhelu Bandhelu"

==Reception== DNA rated IBN Live gave the film a positive review by stating, "Kalpana is a good option for horror film lovers. Upendra is fabulous in his performance."  Deccan Herald also gave the film a positive review and stated, "The Dialogue King Sai Kumar mesmerises as transgender Kalpana, who is wronged by one and all. Upendras abundant energy gets another outlet in Kalpana and his transformation is beautiful. Kalpana is for Uppi’s fans and for those who keep hiding their fears under jokes." 

Rediff gave 3 out of 5 stars for Kalpana and stated, " Sai Kumar, who enacts the part of a transgender, surprises with his superb body language and dialogue delivery. Kalpana is an enjoyable horror film. If you like horror films you may like this."  Oneindia gave the film 3 out of 5 stars and stated, "Kalpana is engaging and entertaining. The flow of the story keeps the audience to watch the movie on the edge of their seats. It is Sai Kumar, who steals the show in the movie. His dialogue delivery and body language of a transgender are treat to watch."  Supergood Movies termed Kalpana as a terrific film, giving it 3.5 out of 5 stars. The reviewer praised Upendras performance stating, "Upendra from the days of A (Kannada film)|A has liking to this kind of variety in the role. He is a fantastic performer. He has given his best."  Indiaglitz also gave the film a positive review and stated, "Kalpana is a stunner. Upendra is fabulous in his performance."  Newstrack India wrote positive words on the movie and gave a final verdict, "If you look in totality, Kalpana is one thoroughly enjoyable film, particularly for children and family audience, despite it getting an A certificate." 

==Box office==
 sixth highest grossing Kannada film of 2012. 

==References==
 

==External links==
*  
* http://www.supergoodmovies.com/34774/sandalwood/kanchana-is-kalpana-in-kannada-news-details
* http://www.chitraloka.com/2011/12/14/kalpana-launched-with-upendra-priyanka-anniversary/
* http://www.youtube.com/thenandalfilms/
* http://www.raaga.com/channels/kannada/moviedetail.asp?mid=K0001872

 

 
 
 
 
 
 
 
 
 