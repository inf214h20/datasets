Lost City of the Jungle
{{Infobox film
| name           = Lost City of the Jungle
| image size     =
| image	=	Lost City of the Jungle FilmPoster.jpeg
| caption        = Ray Taylor Joseph ODonnell Paul Huston Tom Gibson
| narrator       = Jane Adams Lionel Atwill Keye Luke
| music          =
| cinematography = Gus Peterson
| editing        = Irving Birnbaum Jack Dolan Joseph Gluck D. Pat Kelley Alvin Todd Edgar Zane
| distributor    = Universal Pictures
| released       = 23 April 1946
| runtime        = 13 chapters (265 min)
| country        =   United States English
| budget         =
| preceded by    =
| followed by    =
}}
 Universal Serial movie serial.

==Storyline==
Recent atom tests show a certain element that can be used as defense against the atomic bomb. War monger Sir Eric Hazarias (Lionel Atwill) has traced the element, dubbed Meteorium 245, to the Himalayan province of Pendrang, ruled over by casino owner Indra (Helen Bennett). Hazarias fakes his own death and shows up in Pendrang as philanthropist Geoffrey London along with his secretary Malborn (John Mylong), who is secretly the real war monger, Hazarias being his beard, and they start an archaeological dig for the legendary Lost City of Pendrang as a cover for their search for Meteorium. United Peace Foundation operative Rod Stanton (Russell Hayden) arrives in Pendrang soon after having trailed Hazarias there with a twofold mission, prove London is really Hazarias and find out what he is looking for in Pendrang.

==Cast==
* Russell Hayden as Rod Stanton, United Peace Foundation operative Jane Adams as Marjorie Elmore
* Lionel Atwill as Sir Eric Hazarias/ Geoffrey London
* Keye Luke as Tal Shan Helen Bennett as Indra
* Ted Hecht as Doc Harris John Eldredge as Doctor Elmore
* John Mylong as Malborn, the "real" villain of the serial, posing as Geoffrey Londons servant
* John Miljan as Doctor Gaffron
* John Gallaudet as Professor Grebb George Lynn as Marlow, one of Hazarias henchmen
* Dick Curtis as Johnson, one of Hazarias henchmen
* Arthur Space as "System" Reeves
* Frances Chung as Lakana Shan
* Gene Roth as Police Captain Hammond

==Production==
Lionel Atwill died of cancer and pneumonia while filming this serial.  Atwill was playing the mastermind villain, Sir Eric Hazarias, a foreign spy chief.  Universal could not afford to throw out the footage already filmed so they were forced to adapt the serial.  First, another villain (Malborn, played by John Mylong, who was originally just a servant of Sir Eric) was introduced as the boss of Atwills character to take over most of the villain requirements of the film.  Secondly, a double of Atwill was used to complete his remaining scenes.  The double was filmed from behind and remained silent.  The villains henchmen were filmed repeating their orders back to the silent double and stock footage of Atwill was edited in to show a response. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 363–364
 | chapter = 14. The Villains "All Bad, All Mad"
 }} 

Lost City of the Jungle was Universals penultimate serial; the final Universal serial was The Mysterious Mr. M, which was also released in 1946. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 35
 | chapter = 3. The Six Faces of Adventure
 }} 

==Chapter titles==
# Himalaya Horror
# The Death Flood
# Wave Length for Doom
# The Pit of Pendrang
# Fiery Danger
# Deaths Shining Face
# Speedboat Missing
# Fire Jet Torture
# Zalabor Death Watch
# Booby Trap Rendezvous
# Pendrang Guillotine
# Jungle Smash-up
# Atomic Vengeance
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 243
 | chapter = Filmography
 }} 

==See also==
* List of American films of 1946
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{Succession box Universal Serial Serial
| before=The Scarlet Horsemen (1946)
| years=Lost City of the Jungle (1946)
| after=The Mysterious Mr. M (1946)}}
 

 

 
 
 
 
 
 
 
 
 