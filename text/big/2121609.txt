Japanese Devils
{{Infobox film
| name = Riben guizi
| image = 
| caption = 
| director = Minoru Matsui 
| producer = Minoru Matsui, Kenichi Oguri
| writer = Minoru Matsui (screenplay)
| starring = Yoshio Tsuchiya, Hakudo Nagatomi, Yoshio Shinozuka
| music = Ryosuke Sato 
| cinematography = Kenichi Oguri 
| editing = Minoru Matsui 
| distributor = 
| released = 2001
| runtime = 160 minutes
| country = Japan
| language = Japanese
| budget = 
}}
 Japanese documentary war crimes committed by the Imperial Japanese Army during World War II. The film was made under the auspices of Chukiren, the Association of Returnees from China, and features stories retold by 14 retired former soldiers of the Imperial Army, such as Yasuji Kaneko and Yoshio Shinozuka.
 POWs of China and were subjected to a long "re-education" by the Chinese government in Fushun War Criminals Management Centre. {{cite news|url=http://search.japantimes.co.jp/cgi-bin/ff20011205a2.html
|title=Face to face with Imperial evil
|author=Mark Schilling
|date=December 5, 2001
|publisher=Japan Times
|accessdate=2008-01-17}} 

The   is a common ethnic slur against the Japanese people (See Anti-Japanese sentiment in China).

==See also==
* Saburo Ienaga, a Japanese educator who fought government censorship of Imperial Japanese atrocities in World War II after the war.
*Eighth Route Army
*Imperial Japanese Army
*Japanese war crimes
*Nanking Massacre
*National Revolutionary Army
*Second Sino-Japanese War
*Unit 731
*Yasuji Kaneko
*Yun Chung-Ok
*Torn Memories of Nanjing

==References==
 

==External links==
*   - North American distributor of the film
**  
**  
*   (CNN.com)
*  }}

 
 
 
 
 
 
 
 


 
 