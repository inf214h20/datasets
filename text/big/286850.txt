Holy Smoke!
 
{{Infobox film
| name           = Holy Smoke!
| image          = Holy smoke ver1.jpg
| caption        = Original poster
| writer         = Anna Campion Jane Campion
| starring       = Kate Winslet Harvey Keitel
| director       = Jane Campion
| producer       = Jan Chapman
| music          = Angelo Badalamenti
| cinematography = Dion Beebe
| editing        = Veronika Jenet
| distributor    = Miramax Films
| released       = France: 24 November 1999 United States: 3 December 1999 Australia: 26 December 1999 United Kingdom: 31 March 2000
| runtime        = 115 minutes
| country        = Australia
| language       = English
| budget         =
| gross          = $1,765,545
}} 

Holy Smoke! is a 1999 Australian drama film directed by Jane Campion, who co-wrote the screenplay with her sister Anna. It premiered at the Venice Film Festival and was shown at the New York Film Festival and the Taipei Golden Horse Film Festival before being released theatrically.

==Plot synopsis== Sans Souci, her parents are appalled to learn their daughter now answers to the name Nazni and has no intention of returning. They concoct a tale about her father Gilbert having had a stroke and being on the verge of death, and her mother Miriam travels to India in hopes of convincing her to come home, with no success until she suffers a serious asthma attack. Ruth agrees to accompany her on her return flight.
 exit counselor deprograms members of religious cults. In a remote cabin, he isolates Ruth, separates her from her sari and religious props, challenges her faith in Baba, and slowly wears her down. As she begins to weaken, Waters finds himself sexually attracted to her, and in time Ruth allows him to seduce her. She then turns the tables on him, as she discovers her sexuality allows her to make mincemeat of his machismo.

==Principal cast==
* Kate Winslet as Ruth Barron
* Harvey Keitel as P.J. Waters
* Julie Hamilton as Miriam Barron
* Tim Robertson as Gilbert Barron
* Sophie Lee as Yvonne
* Daniel Wyllie as Robbie Paul Goddard as Tim
* Pam Grier as Carol
* Dhritiman Chatterjee as Baba

==Production notes== Hawker in the Flinders Ranges in South Australia. Interiors were filmed at Fox Studios Australia.

The film grossed $1,758,780 in the US and $1,821,943 in foreign markets for a worldwide box office of $3,580,723. 

Angelo Badalamentis soundtrack is performed by artists including Annie Lennox, Alanis Morissette, Burt Bacharach, Neil Diamond and Chloe Goodchild. http://www.imdb.com/title/tt0144715/soundtrack www.imdb.com 

==Critical reception==
Holy Smoke! received mixed reviews from critics, where it holds a 45% rating on Rotten Tomatoes based on 80 reviews.

In her review in The New York Times, Janet Maslin said, "As Holy Smoke moves from its early mix of rapture and humor into   more serious, confrontational stage, it runs into trouble . . . the screenplay . . . threatens to become heavy-handedly ideological beneath its outward whimsy . . . it turns out to be more fundamentally conventional than might be expected . . . Shot so beautifully by Dion Beebe that it seems bathed in divine light,   has a sensual allure that transcends its deep-seated ponderousness. The richly colored Indian scenes have a hallucinogenic magic, while exquisite desert vistas radiate an attunement with nature. And the steamily claustrophobic look of the intense scenes between Ms. Winslet and Keitel have an eroticism that will not surprise viewers of The Piano." 
 mystic Travel travelogue into feminist parable . . . Winslet and Keitel are both interesting in the film, and indeed Winslet seems to be following Keitels long-standing career plan, which is to go with intriguing screenplays and directors and let stardom take care of itself . . . A smaller picture like this, shot out of the mainstream, has a better chance of being quirky and original. And quirky it is, even if not successful." 

In Variety (magazine)|Variety, David Rooney stated, "Original in every sense, this often difficult film about family, relationships, sexual politics, spiritual questing, faith and obsession further explores the directors abiding fascinations in excitingly unconventional terms. Mainstream audiences may be unwilling to surrender to the pull of a unique journey that strips away its characters masks and refuses easy solutions, and many men especially will find it too confronting. But others will embrace its thematic and stylistic complexity as qualities all too rare in contemporary cinema." 

Bob Graham of the San Francisco Chronicle said, "Holy Smoke sometimes has the mentality, for better or worse, of an encounter group. It also has a terrific subject and the spirit to bring it off." 

==Awards and nominations==
At the Venice Film Festival, Jane Campion and Kate Winslet won the Elvira Notari Prize. Campion was nominated for the Golden Lion but lost to Zhang Yimou for Not One Less.

==Box office==
Holy Smoke! grossed $1,380,029 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 