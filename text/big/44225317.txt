Malayala Masom Chingam Onnu
{{Infobox film
| name           = Malayala Masom Chingam Onnu (1996)
| image          =
| caption        =
| director       = Nissar
| producer       = Khader Nazir, Pawankumar, Ajith for Jisna Movies
| writer         = .AR. Mukesh
| screenplay     = 
| starring       = Dileep, Biju Menon, Prem Kumar, innocent, Harisri Ashokan, Sainedeen, Rajan P,Dev, Rudra, Shabnam, Kalpana, Philomina etc
| music          = Raveendran
| cinematography = Venugopal
| editing        = G Murali
| studio         = Ankith & Jisna Movies
| distributor    = Ankith & Jisna Movies
| released       =  
| country        = India Malayalam
}}
 1996 Cinema Indian Malayalam Malayalam film, Kalpana and Prem Kumar in lead roles. The film had musical score by Raveendran.   

==Cast== Dileep
*Biju Menon Kalpana
*Prem Kumar
*Rajan P Dev
*Harishree Ashokan
*Philomina

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaasham || KG Markose || Gireesh Puthenchery || 
|-
| 2 || Aakaasham (F) || KS Chithra || Gireesh Puthenchery || 
|-
| 3 || Aaro Thinkathidambo (D) || K. J. Yesudas, Sindhu Premkumar || Gireesh Puthenchery || 
|-
| 4 || Aaro thinklathidambo || K. J. Yesudas || Gireesh Puthenchery || 
|-
| 5 || Kaalam kalikaalam || Ambili, Krishnachandran, Natesh Shankar || Gireesh Puthenchery || 
|-
| 6 || Kunjikkuyil || Biju Narayanan || Gireesh Puthenchery || 
|-
| 7 || Vilolayaay || K. J. Yesudas || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 

 