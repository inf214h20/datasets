Doctor Sleep (film)
 
{{Infobox film
| name           = Doctor Sleep
| image          = 
| alt            = 
| caption        = 
| director       = Nick Willing
| producer       = {{Plainlist|
* Michele Camarda
* Gary Tuck
}}
| screenplay     = {{Plainlist|
* Nick Willing
* William Brookfield
}}
| based on       =  
| starring       = {{Plainlist|
* Shirley Henderson
* Goran Visnjic
* Miranda Otto
* Sophie Stuckey
* Josh Richards John Rogan
* Claire Rushbrook
* Gabrielle Volpert
* Sarah Woodward
* Corin Redgrave
* Paddy Considine
}}
| music          = Simon Boswell
| cinematography = Peter Sova
| editing        = Niven Howie
| studio         = BBC Films
| distributor    = 
| released       =  
| runtime        = 108
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}

Doctor Sleep, also known as Close Your Eyes (USA), and Hypnotic (International: English title), is a 2002 film directed by Nick Willing based on the book of the same name written by Madison Smartt Bell.

The film stars Goran Visnjic as Dr. Michael Strother, Shirley Henderson as Detective Janet Losey and Paddy Considine as Elliot Spruggs. It was nominated at the 2003 Cinénygma - Luxembourg International Film Festival, Fantasporto International Fantasy Film Awards and won in three categories at the Paris Film Festival and Sweden Fantastic Film Festival.

==Plot==

While treating a policewoman for smoking, hypnotherapist Michael Strother has a telepathic vision of a young girl floating beneath the surface of a stream. The escaped victim of a ritualistic serial killer, the girl has become mute, and Michael is called upon by Scotland Yard to unlock the secrets she holds in order to catch a man who believes he has discovered the key to immortality.

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 