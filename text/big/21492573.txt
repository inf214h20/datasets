Morphine (film)
{{Infobox film
| name           = Morphine
| image          =
| image_size     =
| caption        =
| director       = Aleksei Balabanov
| producer       =
| writer         = Mikhail Bulgakov, Sergei Bodrov, Jr.
| starring       = Leonid Bichevin, Ingeborga Dapkūnaitė, Andrei Panin
| music          =
| cinematography = Alexandr Simonov
| editing        =
| studio         =
| distributor    =
| released       = 27 November 2008
| runtime        =
| country        = Russia
| language       = Russian
| budget         =
| gross          =
}}

Morphine is a 2008 Russian film by Aleksei Balabanov.

The film is based on semi-autobiographical short stories by Mikhail Bulgakov.

==Plot== doctor called Mikhail Polyakov (Leonid Bichevin) arrives at a small hospital in a remote village. Having freshly graduated from medical school, with little experience, he is the only doctor in the rural district. He works hard, earning the respect of his small staff (one paramedic and two nurses).

After an allergic reaction to a diphtheria vaccination, he has his nurse Anna give him morphine to negate the effects. Gradually he slips into Substance dependence|addiction.

==External links==
*  
*  

 
 
 
 
 
 

 