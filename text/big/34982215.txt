Tafasiry
{{Infobox film
| name           = Tafasiry
| image          = 
| caption        = 
| director       = Toky Randriamahazosoa
| producer       = Toky Randriamahazosoa
| writer         = 
| starring       = Jeanine, Olivier Mahajanjy, Rado Randrianiainalalainimanana
| distributor    = 
| released       = 2007
| runtime        = 10
| country        = Madagascar
| language       = 
| budget         = 
| gross          = 
| screenplay     = Toky Randriamahazosoa
| cinematography = Liantsoa Andrianiana Ramilison
| sound          = 
| editing        = Toky Randriamahazosoa
| music          = 
}}

Tafasiry is a 2007 film.

== Synopsis ==
One night, a young traveller seeks shelter in a small and isolated coastal village. After his departure the next morning, the villagers realize something strange has happened: all the children have vanished.

== Awards ==
* Rencontres du Film Court Madagascar 2007

== References ==
 

 
 

 