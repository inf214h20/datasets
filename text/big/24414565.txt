The Mysterious Rider (1938 film)
{{Infobox film
| name           = The Mysterious Rider
| image          = Poster of the movie The Mysterious Rider.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = Harry Sherman
| screenplay     = Maurice Geraghty
| based on       =  
| starring       = {{Plainlist|
* Douglass Dumbrille
* Sidney Toler
* Russell Hayden
* Stanley Andrews
* Charlotte Field
}}
| music          = Gerard Carbonara
| cinematography = Russell Harlan
| editing        = Sherman A. Rose
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Lesley Selander and starring Douglass Dumbrille, Sidney Toler, and Russell Hayden. Written by Maurice Geraghty based on the 1921 novel The Mysterious Rider by Zane Gray, the film is about a notorious outlaw who returns to the ranch he once owned and takes a job disguised as a ranch hand. Unrecognized by the ranchs current owner, he waits patiently for an opportunity to expose the men who murdered his partner twenty years ago, framed him for the crime, and then stole his ranch.    The film was later released for television in the United States as Mark of the Avenger.

==Plot==
In the Arizona desert in the late 1800s, famed outlaw Pecos Bill (Douglass Dumbrille) and his sidekick Frosty Kilburn (Sidney Toler) hold up a stagecoach. During the raid, Bill takes money from a wealthy passenger and gives it to a poor woman passenger, then rides away. Having lived an outlaw life for the past twenty years, and still wanted for a murder he did not commit, Pecos Bill, whose real name is Ben Wade, decides to return to his home town disguised as a regular cowboy to see his now grown daughter.

Meanwhile, Jack Bellounds (Weldon Heyburn), the son of the corrupt rancher William Bellounds (Stanley Andrews) who stole Bens White Slider ranch, returns from prison and is given a ride back to the ranch by Bens beautiful daughter Collie Wade (Charlotte Field), who was raised by his father. When Collie refuses his offer of marriage, the arrogant Jack races their horse-drawn carriage wildly through the desert. Seeing the runaway carriage, the gallant ranch forman Wils Moore (Russell Hayden) chases them down and stops the horses. As Wils rides off, Jack notices that Collie is attracted to Wils.

When Ben and Frosty arrive at White Slider ranch, they get hired on as saddle makers, with Ben going by the name of Red Johnson. No one recognizes him—not even his daughter who hasnt seen him since her childhood. After settling in, Ben learns that 200 head of cattle have been stolen, and that Bellounds seems uninterested in finding the thieves. With the help of Frosty and Wils—one of the few honest men at the ranch—Ben intends to put a stop to the cattle rustling and restore the ranch to his daughter, the rightful owner.

Meanwhile, Jack meets with Cap Folsom (Monte Blue), the leader of the cattle rustlers and Jacks former boss. Cap is also the man who murdered Bens partner and framed him for the crime. Jack complains about the timing of the recent raid, but Folsom dismisses him, saying he can never turn his back on his outlaw past. At the ranch, Ben is tasked with working with the dogs on the ranch, and soon he uses his position to guard the cattle. One afternoon, Ben meets his daughter on the range and they talk about her future. Concerned that she may end up with a lout like Jack, he tells her that she will need to decide between Jack and Wils. Soon after, Ben is alerted by his barking dogs to rustlers in the area, and dressed in black as Pecos Bill, he rides after them. The rustlers recognize the approaching outlaw and take cover. During the ensuing gunfight, Wils arrives with his trustworthy men, sees Pecos Bill, and chases after him instead of the rustlers.

Back at the ranch, Wils attempts to capture Ben, but is thwarted by a well-placed knife throw by Frosty. Knowing that Wils is on the right side of the law, Ben reveals his true identity and the truth behind how he lost his ranch—that he left his foreman William Bellounds to look after things, but he stole the ranch for himself, and now is rustling his own cattle for profit. Convinced he is on the level, Wils pledges to help Ben. When the other ranchers get together to search for the rustlers, Ben dresses up in black like Pecos Bill, draws their attention, and leads them to the rustlers hideout. The plan backfires when the ranchers discover that Wils helped Pecos Bill to escape. Just as the mob prepares to lynch Wils, Pecos Bill rides in to the rescue.

When Bellounds figures out that Pecos Bill is in fact Ben Wade, his former boss, he reveals his discovery to the leader of the rustlers, Cap Folsom. Soon, Bellounds is gunned down in cold blood. Jack goes after Cap to avenge his fathers death, and is himself soon killed. Ben, Wils, and Frosty go after Cap and they meet in a fierce gunfight. During the battle, Ben tracks down Cap in the rustlers hideout, gives him a chance to draw, and then shoots him dead. Sometime later, Ben and Frosty say their goodbyes to Collie and Wils and ride away into the Arizona desert.

==Cast==
* Douglass Dumbrille as Pecos Bill/ Ben Wade
* Sidney Toler as Frosty Kilburn
* Russell Hayden as Wils Moore
* Stanley Andrews as William Bellounds
* Charlotte Field as Collie Wade
* Weldon Heyburn as Jack Bellounds
* Monte Blue as Cap Folsom
* Earl Dwire as Sheriff Burley
* Glenn Strange as Henchman Cramer
* Jack Rockwell as Lem
* Leo J. McMahon as Cowhand Montana
* Arch Hall Sr. as Rancher Andrews

==See also==
*Gleeson Jail

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 