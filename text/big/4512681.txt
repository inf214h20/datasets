Blackballed (film)
{{Infobox film
| name = Blackballed: The Bobby Dukes Story
| image = Blackballedmovieposter.jpg
| caption = Movie Poster
| director = Brant Sersen
| producers = Peter Gelles Chris Lechler Brant Sersen Brian Steinberg
| writers = Brant Sersen Brian Steinberg DJ Hazard
| music =
| cinematography =
| editing =  Chris Lechler Paul Grass
| distributor = The 7th Floor
| released =  
| runtime = 91 minutes
| language = English
| budget = 
}} DJ Hazard. It was also Ed Helms acting debut.

==Plot==
Paintballs first superstar, Bobby Dukes, led his team, the River Rats, to an unprecedented three consecutive victories at The Hudson Valley National Paintball Classic. On May 28, 1993, the River Rats were competing for their fourth "Classic" victory when disaster struck.  Bobby, attempting one of his signature moves, was shot.  Desperate to stay in the game, Dukes intentionally wiped the paint from his jersey, thus committing paintballs most heinous crime.  A zealous referee spotted the wipe and ejected Bobby from the game. The three-time champ was embarrassed, went to Venezuela, and left the game for 10 years while being the laughingstock of the paintball community. Deemed a cheater, disgraced and humiliated, Bobby disappeared. Ten years later an older and wiser Dukes returns to reclaim his title and erase the memory of his tainted past.  He enters the 2003 Classic but finds no self-respecting paintball player will be caught dead on his team. On the verge of giving up hope, Bobby joins forces with the most unlikely of allies... the referee that caught him cheating. Now, the two improbable partners must recruit a team of paintball misfits and take back the Hudson Valley Paintball Classic taking place at Liberty Paintball in Patterson, New York.

==Awards==
This film won the Audience Award at the 2004 South By Southwest Film Festival and the 2005 Boston Independent Film Festival.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 