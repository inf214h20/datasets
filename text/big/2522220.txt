The Hidden (film)
{{Infobox film
| name           = The Hidden
| image          = Hiddenposter1987.jpg
| image size     = 190px
| caption        = Theatrical release poster
| director       = Jack Sholder
| producer       = Stephen Diener Dennis Harris Jeffrey Klein Lee Muhl Michael L. Meltzer Gerald T. Olson Robert Shaye
| writer         = Bob Hunt (pen name of Jim Kouf)
| starring       = Michael Nouri Kyle MacLachlan
| music          = Michael Convertino
| cinematography = Jacques Haitkin Maureen O’Connell
| distributor    = New Line Cinema
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = $9,747,988
}}
The Hidden is an American science fiction action film produced and released in 1987 by New Line Cinema. The film was written by Bob Hunt (pen name of writer/producer/director Jim Kouf) and directed by Jack Sholder. The cast featured Kyle MacLachlan and Michael Nouri with supporting roles by Clu Gulager, Chris Mulkey, Ed ORoss, Clarence Felder, Claudia Christian and Larry Cedar.
 MPAA rating of R, and was filmed in color with mono sound. The DVD version was remastered in Dolby Digital 5.1 surround sound. At the time it was released it was an independent film and had been produced for less than US$5 million. A sequel, The Hidden II, directed by Seth Pinsker was released in 1993.

In the documentary Behind the Curtain Part II, Jack Sholder, director of The Hidden, had this to say about the film:

 Most people who know my work would say that The Hidden was my best film. And I would tend to agree with them... When I watch The Hidden, I feel like Ive pretty much gotten it right. 

==Plot==
Jack DeVries (Chris Mulkey), a quiet citizen with no criminal past, robs a Los Angeles Wells Fargo bank, kills all of the security guards inside, and leads the Los Angeles Police Department on a high-speed chase. The chase ends when DeVries encounters a police blockade overseen by detective Thomas Beck (Michael Nouri). DeVries is shot several times, smashes through the blockade and crashes the Ferrari he is driving. DeVries is taken to a hospital, where a doctor informs Beck and his partner, Det. Cliff Willis (Ed ORoss) that DeVries is not expected to survive the night.
 FBI Special Agent Lloyd Gallagher (Kyle MacLachlan), who informs them that Beck has been assigned to work with Gallagher to track down DeVries. When told of DeVriess condition, Gallagher rushes off to the hospital.

Meanwhile, at the hospital, DeVries suddenly awakens. Disconnecting his life-support equipment, he approaches the comatose man in the next bed, Jonathan P. Miller (William Boyett). After DeVries forces Millers mouth open, a slug-like alien emerges from DeVries mouth and transfers itself into Millers body. Gallagher arrives to find DeVries dead on the floor and Millers bed abandoned. Gallagher tells Beck to put out an alert on Miller, who refuses, because of Millers lack of a criminal history.

Miller goes to a record store where he beats the stores owner to death.  He then goes to a car dealership, where he kills three men and steals a red Ferrari.  He then visits a strip club, where the alien leaves Millers body and takes over the body of a stripper named Brenda (Claudia Christian). She kills a man and takes his car. Gallagher and Beck pursue her to a rooftop, where they mortally wound her in a gun battle. As Brenda dies, Gallagher points a strangely-shaped, alien weapon at her; however, she leaps from the roof. As Masterson arrives from his house to take charge of the scene, the alien transfers itself from Brendas dying body to Mastersons dog.
 thrill killer who has the ability to take over human bodies. Beck dismisses the story as insane and leaves "Gallager" incarcerated in a jail cell at the police station.

Back at Mastersons house, the alien leaves the dogs body and takes over the lieutenants body. In the morning Masterson goes to the police station and seizes a number of weapons, sparking a shootout between himself and the stations police officers as he attempts to track down "Gallagher". Convinced of "Gallagher"s story by Mastersons behavior, Beck releases him from his cell, and the two confront  Masterson. During the resulting shootout, Masterson confirms that "Gallagher" is an alien law enforcer named Alhague who has been pursuing the alien ever since it murdered his family and his partner on another planet. Though Beck manages to stop Masterson, Alhague/Gallagher reveals that his weapon can only kill the alien when it is between bodies, thus requiring him to be present when it is transferring hosts. They are unable to stop the alien from abandoning Mastersons body for that of Becks partner Willis, who then escapes the station.

Using Willis credentials, the alien tries to gain access to Senator Holt, a likely presidential candidate, at the hotel where the senator is staying. Alhague/Gallagher and Beck follow Willis, and a shootout ensues between Beck and Willis, during which Beck is severely wounded. As Willis, the alien corners Senator Holt and enters his body before Alhague/Gallagher can stop him. "Holt" then calls a press conference and announces his candidacy for the presidency. Alhague/Gallagher is forced to attack Holt in the middle of the press conference; though shot several times by the police and the senators bodyguards, Alhague/Gallagher is able to get close enough to use a flamethrower on Holt. As the alien emerges from Holts charred body, Alhague/Gallagher kills it with his weapon before himself collapsing.

Taken to the hospital where Beck is being treated, Alhague/Gallagher discovers that Beck is close to death. Witnessing the emotional suffering of Becks wife and daughter, Alhague/Gallagher transfers his life force from Gallagher to Beck as Beck dies. When she sees her miraculously "recovered" father, Becks daughter initially hesitates when he reaches out to her, but then smiles and takes his hand.

==Cast==

* Kyle MacLachlan as Agent Lloyd Gallagher/Robert Stone/Alhague
* Michael Nouri as Det. Thomas Beck Richard Brooks as Det. Sanchez
* Claudia Christian as Brenda Lee Van Buren
* Chris Mulkey as Jack DeVries
* William Boyett as Jonathan P. Miller
* John McCann as Senator Holt
* Clarence Felder as Lt. John Masterson
* Clu Gulager as Lt. Ed Flynn
* Ed ORoss as Det. Cliff Willis
* Steve Eastin as Agent Stadt
* Katherine Cannon as Barbara Beck
* Larry Cedar as Brem
* Danny Trejo as a prisoner

==Soundtrack==
The soundtrack was released on Varese Sarabande Records, Cassettes and Cds with the score by Michael Convertino.
The end credits states a soundtrack was released on I.R.S. Records, though it hasnt been seen anywhere online. 

Main Title 	1:45
Political Rally 	1:42
Back At Home 	1:01
Transference 	2:16
Miller In Apartment 	1:50
Lloyd And The Little Girl 	1:10
Lloyd Alone 	1:10
Final Transference 	3:14
The Dog 	1:20
Rampage 	7:27
Shoot-Out 	5:55
Stripper 	1:43
Road Block 	1:45
Mannequin


Songs:
 The Truth
* "Black Girl White Girl" - The Lords of the New Church
* "Is There Anybody In There?" - Hunters & Collectors Say Good Bye" - Hunters & Collectors
* "On Your Feet" - Shok Paris
* "Going Down Fighting" - Shok Paris
* "Weapons of Love" - The Truth
* "Still in Hollywood" - Concrete Blonde
* "Your Haunted Head" - Concrete Blonde
* "While the Goings Good" - Twin Set & the Pearls
* "You Make Me Feel So Young" - Brian Gunboty
* "Out of Control (In My Car)" - ULI
* "Bad Girl" - Mendy Lee
* "Over Your Shoulder" - Concrete Blonde

==Release==
The film was released theatrically in the United States by New Line Cinema in October 1987.  It turned out to be a modest hit for the company, grossing $9,747,988 at the box office. 

The film was released on VHS and laserdisc by Media Home Entertainment in 1988.  In August 1997, New Line Home Video re-released the film on VHS.

In 2000, New Line Home Entertainment released the film on special edition DVD. The film was re-released in a set including the sequel The Hidden II in 2005.

==Awards==
*Jack Sholder won the Grand Prize at the Avoriaz Fantastic Film Festival in 1988.
*Jack Sholder won Best Director at Fantasporto in 1988. It was also nominated for Best Film at that festival. Catalonian International Film Festival in 1987. Jack Sholder took Prize of the International Critics’ Jury at the same festival.
*At the 1988 Saturn Awards, Michael Nouri was nominated for Best Actor, Jack Sholder was nominated for Best Director, Jim Kouf was nominated for best writing, and The Hidden was nominated for Best Science Fiction Film.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 