Make Mine a Million
{{Infobox film
| name           = Make Mine a Million
| image          = "Make_Mine_a_Million"_(1959).jpg
| image_size     =
| caption        = British theatrical poster
| director       = Lance Comfort John Baxter
| writer         = Arthur Askey Peter Blackmore Jack Francis Talbot Rothwell
| narrator       =
| starring       = Arthur Askey Dermot Walsh Sid James Olga Lindo Arthur Grant
| editing        = Peter Pitt
| music          = Stanley Black
| studio         = Elstree Studios
| distributor    = British Lion Film Corporation
| released       = 1959
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
Make Mine a Million is a 1959 British comedy film starring Arthur Askey, Sid James, and Bernard Cribbins. It was directed by Lance Comfort.  The film parodies the stuffiness of the 1950s BBC and the effect of television advertising in the era.

==Plot==
Arthur Ashton is a makeup man working for National Television (a parody of the BBC). During a visit to the local laundry, he meets Sid Gibson a shady salesmen who is trying to flog Bonko, a brand of washing powder, but who cant afford to advertise on TV. The fairly clueless Arthur agrees to help him, and they manage to plug an advert for Bonko on National Television by interrupting the live feed. This causes quite a stir amongst the National heads, who have Arthur fired. Despite this, the advert proves extremely popular and demand for the product soars. 
 Ascot Races, Sid, realising that this is potentially a huge moneymaker, does a deal with an advertising executive and, with Arthurs help, they plug cake mix at the Edinburgh Festival. After a narrow escape, Arthur wants to quit, but Sid persuades him to do one final job—interrupting a press conference between the British Prime Minister and the American President. On the way, the Post Office van they are using is hijacked by criminals. Arthur, who is in the back of the van, contacts the police to thwart the robbery, leading to the final barnyard showdown. In the end, Arthur, now a hero and celebrity, gets his own TV show, brokered by Sid, of course.

==Cast==
* Arthur Askey - Arthur Ashton
* Sid James - Sid Gibson
* Dermot Walsh - Martin Russell
* Olga Lindo - Mrs. Burgess
* Clive Morton - Director General
* Sally Barnes - Sally
* George Margo - Assistant
* Lionel Murton - Commercial TV director
* Bernard Cribbins - Jack
* Kenneth Connor - Anxious Husband
* Barbara Windsor - Switchboard Operator Martin Benson - Chairman
* David Nettheim - Professor
* Bruce Seton - Superintendent James
* Tommy Trinder  -Himself, Cameo appearance 
* Dickie Henderson - Himself, Cameo appearance 
* Evelyn Laye - Herself, Cameo appearance 
* Dennis Lotis - Himself, Cameo appearance 
* Anthea Askey - Herself, Cameo appearance 
* Raymond Glendenning - Himself, Cameo appearance 
* Patricia Bredin - Herself, Cameo appearance 
* Leonard Weir - Himself, Cameo appearance  Sabrina - Herself, Cameo appearance 
* Gillian Lynne - Herself, Cameo appearance 
* Peter Noble -   Himself, Cameo appearance as TV Host 
* Prince Monolulu - Himself, Cameo appearance 
* Leigh Madison - Diana 
* Sam Kydd - Mail Van Robber  
* Edwin Richfield - Plainclothes Policeman 
* Bill Shine - Outside Broadcast Producer

==Reception==
The Radio Times Guide to Films gives the film three stars out of five, describing it as a "pacey romp". 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 

 
 