Mädchen in Uniform
 
{{Infobox film
 | name     = Mädchen in Uniform
 | image    = Madchen In Uniform Video Cover.jpg
 | caption  = US VHS video release cover
 | director = Leontine Sagan
 | producer = Carl Froelich
 | writer   = Christa Winsloe (screenplay and theatre play)   (screenplay)
 | starring = Hertha Thiele Dorothea Wieck
 | music    =  
 | cinematography =    
 | editing  = Oswald Hafenrichter
 | studio   = Deutsche Film-Gemeinschaft
 | distributor = Bild und Ton GmbH (Germany) Filmchoice (US) Janus Films (1978 VHS release)
 | released = 27 November 1931 (Germany) 20 September 1932 (US)
 | runtime  = 98 minutes
 | country  = Weimar Republic German
 | budget   =
 }} German feature-length play  cult classic. 

== Cast ==
*Hertha Thiele as Manuela von Meinhardis
*Dorothea Wieck as Governess Fräulein von Bernburg Mother Fräulein von Nordeck zur Nidden, headmistress
*Gertrud de Lalsky as her Excellency von Ehrenhardt, Manuelas aunt protector of the school
*Hedwig Schlichter as Fräulein von Kesten
*Lene Berdolt as Fräulein von Garschner
*Lisi Scheerbach as Mademoiselle Oeuillet
*Margory Bodker as Miss Evans
*Erika Mann as Fräulein von Atems (or Attems)
*  as Elise, wardrobe mistress
*  as Ilse von Westhagen
*Ilse Winter as Marga von Rasso
*  as Ilse von Treischke
*Erika Biebrach as Lilli von Kattner
*  as Oda von Oldersleben Komtessse von Mengsberg
*Ilse Vigdor as Anneliese von Beckendorf
*Barabara Pirk as Mia von Wollin
*Doris Thalmer as Mariechen von Ecke

==Production==
Winsloes stage play had previously appeared under the title   (Knight Nérestan) in Leipzig with Hertha Thiele and Claire Harden in the lead roles. After Leipzig the play was produced on the stage in Berlin as    with a different cast and a more prominent lesbian theme, which was again toned down somewhat for the film.

Having mostly played the same roles on stage, the cast was able to produce the film at speed and on a low budget of Reichsmark|RM55,000. It was largely shot at the Potsdam military orphanage, now a teacher training college for women. Carl Froelichs studio in Berlin-Tempelhof was also used. The films original working title was   (Yesterday and Today) but this was thought too insipid and changed to increase the chances of box-office success. Although sound had only been used for two years in cinema, it was used artfully.

The film was groundbreaking in a number of ways: firstly for its all-female cast; secondly for its sympathetic portrayal of lesbian "pedagogical eros" (see Gustav Wyneken) and homoeroticism revolving around the passionate love of a fourteen-year-old (Manuela) for her teacher (von Bernburg); and thirdly for its co-operative and profit-sharing financial arrangements (although in practice these ultimately failed).

During an interview about the film decades later, Thiele said: Empress Augusta Winsloe was educated. Actually there really was a Manuela, who remained lame all of her life after she threw herself down the stairs. She came to the premiere of the film. I saw her from a distance, and at the time Winsloe told me, "The experience is one which I had to write from my heart." Winsloe was a lesbian. 

Thiele also said, "However, I really dont want to make a great deal of   or account for a film about lesbianism here. Thats far from my mind, because the whole thing of course is also a revolt against the cruel Prussian education system."

After many screen tests, Winsloe had insisted that her friend Thiele play the lead role. Director Sagan would have preferred   who had done the role on stage in Berlin, but along with having played Manuela in Leipzig, Thiele had already played a young lesbian in Ferdinand Bruckners stage play   (The Creature) and although twenty-three years old when filming began, she was considered to be more capable of portraying a fourteen-year-old.

==Reaction== Der blaue Engel (1930). The film did however generate large amounts of fan mail to the stars from all over Germany and was considered a success throughout much of Europe. The goodnight kiss Thiele received from Wieck was especially popular: one distributor even asked for more footage of other kisses like it to splice into prints of the film.

From its premiere at the Capitol cinema in Berlin until 1934 the film is said to have grossed some RM6,000,000. Despite the collective nature of the filming for which cast and crew received only a quarter of the normal wage, none saw a share of the 6,000,000 marks and Thiele later hinted that the profits had been mostly retained by the producers.

The film was distributed outside Germany and was a huge success in Romania. During a 1980 interview Thiele said the school play scene caused a "longstockings and kissing" cult when the film was first shown there. It was also distributed in Japan, the United States (where it was first banned, then released in a heavily cut version), England and France.

  won the audience referendum for Best Technical Perfection at the Venice Film Festival in 1932 and received the Japanese Kinema Junpo Award for Best Foreign Language Film (Tokyo, 1934).
 Nazi ideals enabled continued screening in German cinemas, but eventually even this version of the film was banned as decadent by the Nazi regime, which reportedly attempted to burn all of the existing prints, but by then several had been dispersed around the world. Sagan and many others associated with the film fled Germany soon after the banning. Many of the cast and crew were Jewish, and those who could not escape from Germany died in the camps. "You were only first aware that they were Jewish when fascism was there and you lost your friends," said Thiele, who left Germany in 1937. Assistant director Walter Supper killed himself when it became clear his Jewish wife would be arrested.

Despite its later banning,   was followed by several German films about intimate relationships among women, such as   (Eight Girls in a Boat, 1932) and   (1933), which also starred Wieck and Thiele but was banned by the Nazis soon after its opening night, along with Ich für dich, du für mich (Me for You, You for Me, 1934).
 remake in 1958, directed by Géza von Radványi and starring Lilli Palmer, Romy Schneider, and Therese Giehse. 

==Censorship and surviving version== censored until the 1970s, and it was not shown again in Germany until 1977 when it was screened on television there. 

In 1978, Janus Films and Arthur Krim arranged for a limited re-release in the US in 35mm, including a screening at the Roxie Cinema in San Francisco. Also in 1978, the film was released in its surviving form by Janus Films on VHS with English subtitles. 

Versions were later released in the US (1994) and the UK (2000) by the British Film Institute. Even this version is probably missing some brief scenes. For a full understanding of what may have been censored, a viewing of the film might be followed with a reading of the 1933 novel by Christa Winsloe  /The Child Manuela (Virago Press, 1994).

==Quotation from the film==
*"What you call sin, I call the great spirit of love, which takes a thousand forms." (Spoken in reference to the boycott.)

==In popular culture==
* In the novel The Acceptance World (1955), the narrator, Nick Jenkins, is re-united with his first major love, Jean Templer, after Jean and her sister-in-law, Mona, have returned to the Ritz (London) on New Years Eve 1931, following a screening of the film. Jean is accompanied by her brother Peter. Nick (who has seen the film) is mildly mocked by his old schoolfriend Peter (who has not), for saying that the film is not primarily about lesbians.
* In the film Henry & June (1990), this is one of the films shown in the small art-house theater frequented by the main characters.
*The film Loving Annabelle (2006) was reportedly inspired by Mädchen in Uniform.

== See also ==
*List of German films 1919–1933
*German Expressionism
*List of lesbian, gay, bisexual or transgender-related films

==References==
 

==Further reading==
* Sara Gwenllian Jones. "Mädchen in Uniform: the story of a film". PerVersions: the international journal of gay and lesbian studies, issue 6, Winter 1995/96.
*  , no. 24/25, March 1981 and Radical America, Vol. 15, no. 6, 1982; and also reprinted with additional material in B. Ruby Rich, Chick Flicks: Theories and Memories of the Feminist Film Movement (Durham, NC: Duke University Press, 1998)
* Loren Kruger, Lights and Shadows: The Autobiography of Leontine Sagan (Johannesburg, South Africa: Witwatersand University Press, 1996)

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 