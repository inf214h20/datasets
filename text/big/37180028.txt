Iron to Gold
{{infobox film
| name           = Iron to Gold
| image          = Iron to Gold (1922) - 1.jpg
| image_size     =
| caption        = Newspaper ad
| director       = Bernard J. Durning William Fox John Stone
| starring       = Dustin Farnum Marguerite Marsh
| music          =
| cinematography = Don Short (aka Donovan Short)
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent (English intertitles)

}} lost  western film produced and distributed by Fox Film Corporation. Based on a short story by Max Brand, writing as George Owen Baxter, the film starred Dustin Farnum and was directed by Bernard J. Durning. 

==Cast==
*Dustin Farnum - Tom Curtis
*Marguerite Marsh - Anne Kirby
*William Conklin - George Kirby
*William Elmer - Bat Piper
*Lionel Belmore - Sheriff
*Glen Cavender - Sloan
*Bob Perry - Creel (credited as Robert Perry)
*Dan Mason - Lem Baldwin (hotel keeper)

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 
 