The Servant (2010 film)
{{Infobox film name           = The Servant image          = TheServant2010Poster.jpg caption        = Film poster director       = Kim Dae-woo producer       = Moon Yang-kwon   Im Seung-yong  Seo Woo-sik  Park Dae-hee  writer         = Kim Dae-woo  starring       = Kim Joo-hyuk Jo Yeo-jeong   Ryoo Seung-bum  music          = Mok Young-jin cinematography = Kim Young-min  editing        = Kim Sang-bum distributor    = CJ Entertainment released       =   runtime        = 124 minutes country        = South Korea language       = Korean budget         =  gross          =   
| film name = {{Film name hangul         =    hanja          =   rr             = Bang-ja-jeon mr             = Pang-ja-jŏn}}
}} historical romantic drama film starring Kim Joo-hyuk, Jo Yeo-jeong and Ryoo Seung-bum. It re-tells the origins of the famous Korean folktale Chunhyangjeon from the perspective of the male protagonist Lee Mong-ryongs servant.    

The film was a box office hit, receiving 3,014,523 admissions.    . Koreanfilm.org. Retrieved 2012-06-04. 
 
==Plot== 
Throughout the movie, the scene switches between the present Bang providing the narration, and the flashback events of his past.
 
In his capacity as a servant Bangja shares his rooms with Mr. Ma, a notorious womanizer and self-stylized Lothario. Bang escorts his master (Mong-ryong) to an evening out at the local pleasure house, where they are witness to a performance by the madams daughter Chunhyang. While trying to arrange a meeting between Chunhyang and his rather clumsy and socially awkward master, Bang defends Mong-ryong from a larger, disgruntled patron and inadvertently impresses both Chunhyang and her maidservant Hyangdan.
 
Mr. Ma begins to coach the simple, honest Bangja in the ways of seducing women, which Bang uses to secure a picnic outing for Mong-ryong with Chunhyang through Hyangdan. During the excursion, Bangja so astounds the ladies by cooking meat to perfection, recovering Chunhyangs slipper from the waterfall pool, and carrying her on his back after she injured her ankle, that a love triangle rapidly begins to form between the two women and Bangja, much to the consternation of Mong-ryong, though he boasts that he is slowly luring Chunhyang to him by playing hard to get.
 
While his master continues to study, Bangja attempts to court Chunhyang. Mr. Ma continues to provide instruction to Bangja, assuaging his concerns when he thinks his master may have slept with her, and pushing him to seize upon spending the night with her before his master can. Though Bangjas seduction is clumsy and his approach very tentative, Chunhyang begins to fall for him and makes love to him on several occasions. Due to his low social-standing, however, she seeks to marry Mong-ryong and enlists Bangjas help in order to make this plan a reality.
 
When Mong-ryong is called away to Seoul to finish studying and take his exam he asks Bangja to recover a written promise he gave Chunhyang about marrying her. She catches Bangja as he tries to steal the paper, and switches it for a confession letter she wrote and got him to sign the night the two of them first had sex. Mong-ryong reads the letter and dismisses Bangja from his service for having deceived him.
 
Three years pass, and Bangja and Chunhyang grow closer and continue to love each other while Bangja becomes the servant of Chunhyangs house and runs errands for a local strong man. However, Chunhyang begins to take after her mother and grows increasingly manipulative despite her burgeoning love for Bangja.
 
In Seoul, Mong-ryong takes his exams and becomes a Royal Inspector. After insulting the court eunuchs however, he is accorded a lowly position in his home town, subservient to the new governor.
 
Mong-ryong finds Hyangdan has become the madam of her own house and runs a successful business. She sleeps with Mong-ryong and asks him if she is not more desirable and pleasing than Chunhyang, but expresses remorse that Bangja chose Chunhyang over her.
 
Mong-ryong discusses women with his magistrate and later returns to Chunhyangs house. Mong-ryong goes for a walk with Chunhyang, and though it is not revealed what the two discussed, Chunhyang returns with a pleased expression and an expectation of seeing Mong-ryong again.
 
The magistrate visits Chunhyangs house and after a confrontation with his own clerks and then Bangja, is greeted by a beautiful, but uncooperative Chunhyang who refuses to sit and pour his drinks because she is not a gisaeng. Enraged by her arrogance, the governor beats Bangja when he attempts to interfere, and has Chunhyang imprisoned. Bangja goes to Mong-ryong and implores his old master to help save her life.
 
Later, during a celebration, the magistrate is seen in a back room attempting to sexually excite Chunhyang because Mong-ryong had told him that she would only bow to his wishes and fulfill his desires if he were violent with her. Bangja causes a commotion to get the magistrate to release her, but is saved from being beaten by the guards when Mong-ryong arrives with a large contingent of guards. Mong-ryong arrests the magistrate and has Chunhyang whipped for her insolence until Bangja interferes, claiming that she has a husband whom she was remaining faithful to. Chunhyang stabs herself with a small blade, saying to Mong-ryong (whose face was hidden) that she wanted news of her death taken to Master Lee Mong-ryong.
 
Bangja is visited in his cell, first by Mr. Ma, who warns him to never beg a woman to stay, then by Chunhyang, who reveals that she and Mong-ryong planned the whole event after he returned from his exams. For the first time, Bangja confesses his love to Chunhyang, who leaves him. Distraught by these event, she then tells Mong-ryong that she will not leave without Bangja, and so the three of them depart the city together. When the trio stop at the waterfall where they had their first excursion years before, Mong-ryong pushes Chunhyang down the falls and she is seen face-down in the water. Bangja dives in to save her and runs away, carrying her on his back as he did when she injured her ankle.
 
The present Bangja tells the writer that he ran from Mong-ryong and his agents for a long time after those events. The last thing he does to bring the story to close is bring the writer to the back of his warehouse to see Chunhyang who survived the fall, but was left with brain damage and has become a child.
 
The writer declares that Bangja is an amazing man and will make him the hero of a wonderful story about a servants love, but Bangja insists that the story be told with her fabricated fidelity being the truth, and Chunhyang living happily ever after with a Lee Mong-ryong who loved her and returned for her. When asked why, he says it was because it was something she never got to have, and he is happy with being the hero in his heart.
 
Bangja requests one scene be written to demonstrate the love between the two characters, which he demonstrates by carrying Chunhyang around the room on his back and singing a variation of the song from the pansori Chunhyangga while she smiles lovingly and snow flakes slowly fall on them from the open roof.
 
The last scenes are of people in the village where they lived, the times when she entertained guests at her house with her singing, and of her shoe in the ice below the waterfall where he saved her.
==Cast==
*Kim Joo-hyuk ... Bang-ja
*Jo Yeo-jeong ... Chun-hyang 
*Ryoo Seung-bum ... Lee Mong-ryong 
*Oh Dal-su ... Mr. Ma 
*Ryu Hyun-kyung ... Hyang-dan  
*Song Sae-byeok ... Byeon Hak-do 
*Jung Yang ... Wol-rae 
*Kim Sung-ryung ... Wol-mae 
*Gong Hyung-jin ... man with colored glasses
*Kim Min-kyo ... Eunuch

==Awards and nominations==
2010 19th Buil Film Awards
*Best New Actor - Song Sae-byeok

2010 47th Grand Bell Awards
*Best Supporting Actor - Song Sae-byeok
*Best Costume Design - Jung Kyung-hee
*Nomination - Best Film
*Nomination - Best Actress - Jo Yeo-jeong

2010 8th Korean Film Awards
*Best Art Direction - Park Il-hyun
*Nomination - Best Film
*Nomination - Best Supporting Actor - Song Sae-byeok
*Nomination - Best Supporting Actress - Ryu Hyun-kyung
*Nomination - Best New Actor - Song Sae-byeok
*Nomination - Best Screenplay - Kim Dae-woo
 31st Blue Dragon Film Awards
*Popular Star Award - Jo Yeo-jeong
*Nomination - Best Supporting Actor - Oh Dal-su
*Nomination - Best Supporting Actress - Ryu Hyun-kyung
*Nomination - Best New Actress - Jo Yeo-jeong
*Nomination - Best Art Direction - Park Il-hyun
*Nomination - Best Original Screenplay - Kim Dae-woo
 47th Baeksang Arts Awards
*Nomination - Best Actress - Jo Yeo-jeong
*Nomination - Best New Actor - Song Sae-byeok
*Nomination - Best Screenplay - Kim Dae-woo

==References==
 

== External links ==
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 
 
 
 