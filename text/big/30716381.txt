The Long Weekend (O' Despair)
The Long Weekend (O Despair) is a 1989 American drama film directed by Gregg Araki. The film follows three couples, one gay, one lesbian and one heterosexual, spending a weekend together.

==Cast==
* Bretton Vail as Michael
* Maureen Dondanville as Rachel
* Andrea Beane as Leah
* Nicole Dillenberg as Sara
* Marcus DAmico as Greg
* Lance Woods as Alex

==Production==
Araki shot The Long Weekend in black and white on a budget of $5,000. 

==Reception==
The New York Times reviewer Vincent Canby congratulated Araki for making an attractive-appearing film on a minuscule budget but found the film hard to watch. Faulting the films "extremely self-conscious, neo-sitcom dialogue", Canby felt that Arakis ingenuity as a filmmaker was not matched by his talent.   

The Long Weekend (O Despair) won the 1989 Los Angeles Film Critics Association Independent-Experimental Award. 

==References==
 

==External links==
*   at the Internet Movie Database

 

 
 
 
 
 
 


 