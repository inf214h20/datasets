Bell, Book and Candle
 
 
 
{{Infobox film
| name           = Bell, Book and Candle
| image          = Bellbookandcandleposter.jpg
| caption        = Promotional film poster
| director       = Richard Quine
| producer       = Julian Blaustein
| based on       =  
| writer         = Daniel Taradash
| starring       = James Stewart Kim Novak Jack Lemmon Janice Rule Elsa Lanchester Ernie Kovacs Hermione Gingold
| music          = George Duning
| cinematography = James Wong Howe
| editing        = Charles Nelson
| distributor    = Columbia Pictures
| released       =  
| country        = United States
| runtime        = 106 minutes
| language       = English 
| gross          = $2.5 million (estimated US/ Canada rentals) 
}}

Bell, Book and Candle is a 1958 American romantic comedy Technicolor film directed by Richard Quine, based on the successful Broadway play by John Van Druten, which stars James Stewart and Kim Novak in their second on-screen pairing (after Vertigo (film)|Vertigo, released earlier the same year). 

The film, adapted by Daniel Taradash, was Stewarts last film as a romantic lead. Columbia Pictures head Harry Cohn agreed to allow Novak to appear in Vertigo as a last-minute replacement for pregnant Vera Miles, so long as Stewart appeared in this film with Novak. The supporting cast includes Jack Lemmon and Ernie Kovacs.

The original 1950 play starred Rex Harrison, his then-wife Lilli Palmer, Jean Adair and Larry Gates. The play is set entirely in the Holroyd residence and is written for a cast of five. The additional characters that appear in the film are only mentioned by name in the play.

==Plot==
During the Christmas holiday season, Greenwich Village witch Gillian Holroyd (Kim Novak), a free spirit with a penchant for going barefoot, has been unlucky in love and restless in life. She admires from afar her neighbor, publisher Shep Henderson (James Stewart), who one day walks into her gallery of African art to use the telephone (after Gillians aunt Elsa Lanchester put a spell on his phone). When she learns he is about to marry an old college enemy of hers, Merle Kittridge (Janice Rule), Gillian takes revenge by casting a love spell on Shep, and she eventually falls for him herself. She must make a choice, as witches who fall in love lose their supernatural powers. When she decides to love Shep, Gillians cat and familiar spirit|familiar, Pyewacket (familiar spirit)|Pyewacket, becomes agitated and leaves.

Sidney Redlitch (Ernie Kovacs), the author of the best-selling book Magic in Mexico, arrives in Sheps office (thanks to a little magic) after Gillian discovers Sheps interest in meeting him.  Redlitch is researching a book on witches in New York, and he acquires an "inside" collaborator when Gillians warlock brother Nicky (Jack Lemmon) volunteers his services in exchange for a portion of the proceeds.

Gillian uses her magic to make Shep lose interest in Nicky and Redlitchs book, and then confesses her identity as a witch to Shep. Shep becomes angry, believing she enchanted him just to spite Merle, and the two quarrel. Gillian threatens to cast various spells on Merle, such as making her fall in love with the first man who walks into her apartment, but she finds that she has lost her powers because of her love for Shep.  Meanwhile, Shep finds he literally cannot leave Gillian, because of the spell. To escape, he turns to another witch, Bianca de Passe (Hermione Gingold), who breaks the spell. Shep confronts Gillian and leaves her heartbroken. He then tries unsuccessfully to explain to Merle that Gillian is a witch. Months later, Shep returns and discovers Gillian has lost her magic powers because of her love for him. When he realizes her love is true, the two reconcile.

==Cast==
 
* James Stewart as Shepherd "Shep" Henderson
* Kim Novak as Gillian "Gil" Holroyd
* Jack Lemmon as Nicky Holroyd
* Ernie Kovacs as Sidney Redlitch
* Elsa Lanchester as Aunt Queenie Holroyd
* Hermione Gingold as Bianca de Passe
* Janice Rule as Merle Kittridge
* Howard McNear as Andy White, Sheps co-publisher
* Dick Crockett as Ad-lib Bit
* Bek Nelson as Tina, Sheps Secretary
 

==Production and release==
 
The title "Bell, Book and Candle" is a reference to excommunication, which is performed by bell, book, and candle. It is opened with "Ring the bell, open the book, light the candle," and closed with "Ring the bell, close the book, quench the candle."  In the film, this is misidentified as exorcism.

Cary Grant had wanted to play the lead in this film. The following year, however, Grant starred in Hitchcocks North by Northwest, a movie Stewart had wanted badly to play. Hitchcock cast Grant instead, blaming the critical and commercial failure of Vertigo on Stewarts appearance, believing Stewart looked too old to draw audiences as a leading man and casting Grant (who was four years older but looked younger) in the part. 
 The Brothers Candoli who appeared in the film in cameo appearances, also found success. 

==Soundtrack==
Songs from the film include:
 Stormy Weather", Pete and Conte Candoli|Conte—on trumpets, and Elek Bacsik on guitar)
*" Deck the Halls", performed by James Stewart by whistling
*" Jingle Bells", played during the opening credits

==Influence==
Fans of the film point to similarities among it, the earlier I Married a Witch (1942), and the 1960s television series Bewitched (produced by Columbias television division).  Bewitched creator Sol Saks revealed in his book The Craft of Comedy Writing that he drew on these and other sources, such as folktales. 

"Bell, Book, and Candle" is also the name of the shop owned and operated by the fictional Cassandra Nightingale in the Hallmark movie series The Good Witch.

==Awards==
===Academy Awards===
Nominated:    Best Art Direction – (Cary Odell and Louis Diage) Best Costume Design – (Jean Louis)

===Golden Globes===
Nominated: Best Motion Picture – Comedy

==See also==
 
 
* 1958 in film
* List of American films of 1958
* List of Christmas films
* List of Columbia Pictures films
* List of fantasy films of the 1950s
* List of films based on stage plays or musicals
* List of films set in New York City
* List of romantic comedy films
 
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 