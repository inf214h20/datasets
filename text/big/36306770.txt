The Taking of Tiger Mountain
{{Infobox film
| name           = The Taking of Tiger Mountain
| image          = Tracks in the Snowy Forest poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical poster
| director       = Tsui Hark
| producer       = Huang Jianxin Yu Dong
| writer         = 
| screenplay     = 
| story          =  Qu Bo
| narrator       = 
| starring       = Zhang Hanyu   Tony Leung Ka-fai   Lin Gengxin   Yu Nan   Tong Liya
| music          = Wu Wai-lap
| cinematography = 
| editing        = 
| studio         = Bona Film Group
| distributor    = 
| released       =  
| runtime        = 141 minutes
| country        = China Hong Kong
| language       = Mandarin
| budget         = 
| gross          = US$150 million (China) 
}} epic action Qu Bo.    It was released on December 23, 2014.     

  and  , the two protagonists of the story.  While Yang Zirong (1917－1947) is based on the real-life person, the other hero of the story Shao Jianbo is fictional, and Qu Bo created the character based on himself, as the story is seen through the point of view of Shao Jianbo.

Bona Film Group bought the rights to the novel in 2009 and has been planning the adaptation since. 

Qu Bos novel was also famously adapted into the opera Taking Tiger Mountain by Strategy, from which the film takes its title.

==Cast==
*   
* Tony Leung Ka-fai as Hawk
*  
* Yu Nan as Qinglian
* Lin Gengxin as Captain 203
* Tong Liya as Little Dove
* Han Geng as Jimmy
* Chen Xiao as Gao

== Reception ==
=== Box office === tenth highest-grossing film of all time in China.   

=== Critical response ===
Film Business Asias Derek Elley gave the film an 8 out of 10, calling it "bracingly effective popcorn action." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 
 