Uro (film)
{{Infobox film
| name           = Uro
| image          = 
| image size     = 
| caption        = 
| director       = Stefan Faldbakken
| producer       = Knud Bjørne Larsen Frida Ohrvik
| writer         = Harald Rosenløw Eeg
| narrator       = 
| starring       = Nicolai Cleve Broch
| music          = 
| cinematography = John Andreas Andersen
| editing        = Vidar Flataukan
| distributor    = 
| released       = 25 August 2006
| runtime        = 104 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| preceded by    = 
| followed by    = 
}}

  ( ) is a 2006 Norwegian crime film starring Nicolai Cleve Broch and Ane Dahl Torp. It was directed by Stefan Faldbakken.            It was screened in the Un Certain Regard section at the 2006 Cannes Film Festival.   

==Plot==

A former delinquent, Petter (HP) becomes a policeman. He is so involved when he is infiltrated that he sometimes forgets that he is a cop. His mission is to arrest a drug dealer Marco. But this time is he going too far?

==Cast==
* Nicolai Cleve Broch – Hans Petter
* Ane Dahl Torp – Mette
* Ahmed Zeyan – Marco
* Bjørn Floberg – Frank Hermansen
* Ingar Helge Gimle – Makker
* Eivind Sander – Henning
* Kim Sørensen – Anders
* Anne Krigsvoll – Mother
* Thorsten Flinck – Radovan
* Nicholas Hope – The dealer
* Bartek Kaminski – Vekteren
* Jørgen Emmanuel – Tim
* Anne Ryg – Avdelingsleder
* Heidi Gjermundsen – Lege (as Heidi Gjermundsen Broch)
* Marlene Vilberg – Danskens datter

==References==
 

==External links==
* 

 
 
 
 
 


 
 