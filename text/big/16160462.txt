Sherlock Holmes and Dr. Watson
{{Infobox film
| name           = Sherlock Holmes and Dr. Watson
| caption        =
| director       = Igor Maslennikov
| producer       = Lenfilm
| writer         = Arthur Conan Doyle (novel) Igor Maslennikov Yuli Dunsky Valeri Frid
| narrator       =
| starring       = Vasily Livanov Vitaly Solomin Rina Zelyonaya
| music          = Vladimir Dashkevich (original score) Boris Andreev (sound department)
| cinematography = Yuri Veksler Mark Kaplan (artist)
| distributor    = Lenfilm Gosteleradio
| released       =  
| runtime        = 135 minutes (in 2 episodes)
| country        = Soviet Union
| language       = Russian
| budget         =
}} Soviet film The Adventures of Sherlock Holmes and Dr. Watson.

The film was divided into two episodes - "The Acquaintance" ( , based on The Adventure of the Speckled Band) and "Bloody Inscription" ( , based on A Study in Scarlet).

==Cast==
* Vasily Livanov as Sherlock Holmes
* Vitaly Solomin as Dr. Watson
* Rina Zelyonaya as Mrs. Hudson

===The Acquaintance===
* Gennady Bogachov as Stamford
* Maria Solomina as Helen Stoner and Julia Stoner
* Fedor Odinokov as Dr. Roylott (voiced by Igor Yefimov)

===Bloody Inscription===
* Borislav Brondukov as Inspector Lestrade
* Igor Dmitriev as Inspector Gregson
* Nikolai Karachentsov as Jefferson Hope Viktor Aristov as Joseph Stangerson
* A. Aristov		
* Adolf Ilyin as Enoch Drebber

==External links==
*  
*  
*  (see English title in the description)

 
 

 
 
 
 
 
 
 
 
 
 
 


 
 