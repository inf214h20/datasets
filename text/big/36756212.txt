Ecstasy of Order: The Tetris Masters
 
{{Infobox film
| name = Ecstasy of Order: The Tetris Masters| image =Ecstasy_of_Order_Poster.png
| caption = Theatrical Poster
| tagline = "Nothing ever just falls into place"
| director = Adam Cornelius
| producer = Adam Cornelius Robin Mihara Vince Clemente
| executive producer = Dain Anderson Nathan Graybeal Leah McGlynn
| associate producer = Andrew Lavorgna
| starring = Thor Aackerlund Robin Mihara Harry Hong Jesse Kelkar Ben Mullen Chris Tang Jonas Neubauer Dana Wilcox
| cinematography = Dan Billups
| score = Chris Pickolick
| studio = Reclusion Films
| released = October 21, 2011
| runtime = 93 minutes
| country = United States
| language = English
| budget = N/A
| gross = $100,000 (est) 
}}
Ecstasy of Order: The Tetris Masters is a 2011 American documentary film that follows the lives of several gamers from around the country as they prepare to compete in the 2010 Classic Tetris World Championship held in Los Angeles, California.  It recounts the development and rise of Tetris as one of the most-played video games of all-time, the role it has played in shaping the lives of the gamers it chronicles, the mystery surrounding the whereabouts of former Nintendo World Champion Thor Aackerlund, and the conception & execution of the first ever Classic Tetris World Championship by gaming enthusiast Robin Mihara.

The film was directed by Adam Cornelius and screened at over 15 film festivals both domestic and internationally, beginning with its World Premiere at the Austin Film Festival on October 21, 2011 and including an appearance at the International Documentary Film Festival Amsterdam, the largest film festival of its kind. It debuted on DVD and Video On Demand (VOD) on August 21, 2012.  An expanded version of the films soundtrack from composer Chris Pickolick has also been released.

==Cast==
* Harry Hong
* Jonas Neubauer
* Robin Mihara
* Dana Wilcox
* Ben Mullen
* Jesse Kelkar
* Thor Aackerlund
* Trey Harrison
* Chris Tang
* Matt Buco
* Alex Kerr

==Plot==
An Oregon-based Tetris enthusiast by the name of Robin Mihara, longing to track down the best Tetris players in the nation for the purpose of organizing a competition, happens upon a website called Twin Galaxies which has been tracking video game world records since the 1980s. From this website he learns of record-holders and top scorers like Jonas Neubauer & Harry Hong (both of whom have maxed out with a high score of 999,999), Ben Mullen (world-record holder for most lines with 296), Dana Wilcox, and Jesse Kelkar.  In addition, he seeks to reconnect with several of the top players from the heyday of NES and Tetris—competitors from 1990s Nintendo World Championships—Trey Harrison and Thor Aackerlund, the latter of whom claims to have reached Level 30. Little has been heard of Aackerlund since his rise to notoriety as the poster child for video gaming in the 1990s, and being successful at playing video games is described by Mihara in the film as "the cornerstone of a very difficult life  ."  Regardless of his reluctance, Aackerlund is able to put the past behind him and agrees to participate. With the addition of competitors like Matt Buco and Bay area Tetris Grandmaster Alex Kerr, the stage is set to for the top eight competitors to go head-to-head in Los Angeles, vying for the title of Tetris Champion in the first ever Classic Tetris World Championship.

The film integrates a fair amount of Tetris gameplay footage and strategy with the stories of the individual gamers and also features interviews with Tetris developer Alexey Pajitnov, Former Twin Galaxies Senior Referee Mr. Kelly R. Flewin, multi-platform champion gamer Chris Tang, and a special appearance by The Tetris Company CEO Henk Rogers.

==Reception==
The film received the prestigious Audience Award for Documentary Feature at the 2011 Austin Film Festival, sharing the honor with "Stories from an Undeclared War."   It received the award for Best Feature Film (any genre) at the 2012 Phoenix Comicon Film Festival.  The film has received generally favorable reviews from critics.  Dennis Harvey of Variety (magazine) noted that it will "delight fans of the highly addictive game"  and Allistair Pinsof of Flixist calls it "one of the best videogame films of all time." 

==References==
 

==External links==
*  
*  
*   - Broken link
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 