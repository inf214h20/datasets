40 Carats (film)
{{Infobox Film
| name           = Forty Carats
| image          = FortyCarats2.jpg
| caption        = Film poster
| director       = M.J. Frankovich Milton Katselas
| producer       = 
| writer         = 
| narrator       = 
| starring       = Liv Ullmann Edward Albert Gene Kelly Binnie Barnes
| music          = Michel Legrand	 
| cinematography = Charles Lang	
| editing        = David E. Blewitt Frankovich Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross = $2,100,000 (US/Canada rentals) 
}} the play of the same name by Jay Presson Allen. The screenplay was written by Leonard Gershe (who significantly changed the ending) and directed by Milton Katselas.

The cast includes Liv Ullman, Edward Albert, Gene Kelly, Binnie Barnes, Deborah Raffin, Nancy Walker, and Natalie Schafer. Ullman was nominated for a Golden Globe as Best Motion Picture Actress, Musical or Comedy, and the Writers Guild of America nominated Gershes screenplay for Best Comedy Adapted from Another Medium.

==Plot==
Ann Stanley, who sells real estate in New York City, is on vacation   with her mother in Greece when her car breaks down. To her rescue comes a young man on a motor   bike, Peter Latham, who has a difficult time persuading Ann to accept a ride.

They become better acquainted, drink ouzo and ultimately have sex on a beach. Ann enjoys his company very much, but still views their relationship as a summer fling.

Back home, at a party one night, Ann is stunned when her grown daughter turns up with Peter as her date  . It turns out, however, that Peters goal is to resume his romantic acquaintance with Ann, having developed feelings for her during the summer. The age difference embarrasses Ann greatly. He is 22 and she is not even the 36 she admitted in Greece to being, but, in reality, 40.

Friends and associates of Ann are somewhat aghast at her behavior as the persistent Peter refuses to take no for an answer. In time, after demonstrating a great deal of reluctance, Ann finally acknowledges that the only thing thats important to her is true love.

==Cast==
*Liv Ullmann as Ann Stanley
*Edward Albert as Peter Latham
*Gene Kelly as Billy Boylan
*Binnie Barnes as Maud Ericson
*Deborah Raffin as Trina Stanley
*Billy Green Bush as J.D. Rogers
*Nancy Walker as Mrs. Margolin
*Don Porter as Mr. Latham
*Rosemary Murphy as Mrs. Latham
*Natalie Schafer as Mrs. Adams
*Claudia Jennings as Gabriella
*Sam Chew Jr. as Arthur Forbes
*Brooke Palance as Polly
*Andrea True as Extra

==Production==
Audrey Hepburn, Elizabeth Taylor, Joanne Woodward, Doris Day, Glenda Jackson, Shirley MacLaine, and Sophia Loren were all considered for the role of Ann Stanley before the original director (William Wyler) bowed out of the production. 
 Broadway stage in 1968, the role was originated by Julie Harris, who won a Tony Award for her performance. Actresses such as June Allyson, Joan Fontaine and Zsa Zsa Gabor succeeded her on Broadway in the play, which ran for 780 performances.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 