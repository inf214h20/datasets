Dogs of War (film)
 
{{Infobox film
| name           = Dogs of War!
| image          = Dogs of war TITLE.JPEG
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       = Jack Davis Allen Hoskins Mary Kornman Ernie Morrison Andy Samuel Harold Lloyd
| music          = Brian Benison
| cinematography =
| editing        =
| distributor    = Pathé Exchange
| released       =  
| runtime        = 26:17 
| country        = United States 
| language       = Silent film English intertitles
| budget         =
}} silent short feature comedy produced by Roach and starring Harold Lloyd, who makes a cameo appearance in Dogs of War as himself.

==Plot==
Near West Coast Studios in Hollywood, the gang is waging a street war against a rival group of kids; their ammunition primarily consisting of old vegetables. The battle is halted when Mary is called to act in one of the West Coast films. The rest of the gang tries to crash the studio gates and get a role in the picture, but the casting director throws them out. Farina manages to sneak into the studio, however, prompting the other kids to sneak in after her. Several chases throughout the property then ensue and the gang eventually escapes - with a little help from Harold Lloyd.

  Sunshine Sammy goes to war Mickey Daniels is a "genrel" Mary Kornman is the troops nurse Joe Cobb is making a "pitchur" Jackie Condon Farina in Dogs of War!
 

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickey Jack Davis as Jack
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Ernest Morrison as Ernie

===Additional cast===
* Andy Samuel as Rival gang member
* George "Freckles" Warde as Rival gang member
* Gabe Saienz as Rival gang member
* Elmo Billings as Rival gang member
* Harold Lloyd as Himself
* Jobyna Ralston as Herself
* Roy Brooks as Studio receptionist
* Sammy Brooks as Studio crew member
* Bob Davis as Truck driver
* Dick Gilbert as Studio guard William Gillespie as Studio director
* Clara Guiol as Studio actress
* Wallace Howe as Actor around the lot
* Walter Lundin as Harold Lloyds cameraman
* Joseph Morrison as Studio assistant director
* Fred Newmeyer as Harold Lloyds cameraman Charles Stevenson as Studio actor
* Leo White as Actor around the lot
* Charley Young as Studio cameraman

==Notes==
*When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. Footage from the first half of this film was released into television syndication as Mischief Makers in 1960 under the title Battleground.
*Footage from the second half of this film was released to the series in the hybrid episode "Hollywood U.S.A.".

==See also==
* Our Gang filmography

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 