The Call of the Savage
{{Infobox film
| name           = The Call of the Savage
| image_size     =
| image	         = The Call of the Savage FilmPoster.jpeg
| caption        =
| director       = Lew Landers
| producer       = Milton Gatzert Henry MacRae
| writer         = Basil Dickey Nate Gatzert George H. Plympton
| narrator       =
| starring       = Noah Beery Jr. Dorothy Short Harry Woods Bryant Washburn
| music          = Richard Fryer
| editing        = Irving Applebaum Saul A. Goodkind Alvin Todd Edward Todd
| distributor    = Universal Pictures
| released       =  
| runtime        = 12 chapters (231 min)
| country        = United States English
| budget         =
}} Universal Serial serial based on the story Jan of the Jungle by Otis Adelbert Kline. It was directed by Lew Landers and released by Universal Pictures.

==Plot==
Two teams of scientists scour the dark jungles of Africa to find a secret formula.

==Production==
Call of the Savage features "Jan, the Jungle Boy" and was based on "Jan of the Jungle" by Otis Adelbert Kline, a successful pulp story which rivalled the Tarzan series. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 132
 | chapter = 6. Jungle "Look Out The Elephants Are Coming!"
 }}  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 11
 | chapter = 2. In Search of Ammunition
 }} 

In 1956 material from this serial was edited into a 70-minute film called Savage Fury.

==Chapter titles==
# Shipwrecked
# Captured by Cannibals
# Stampeding Death
# Terrors of the Jungle
# The Plunge of Peril
# Thundering Waters
# The Hidden Monster
# Jungle Treachery
# The Avenging Fire God
# Descending Doom
# The Dragon Strikes
# The Pit of Flame
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 212
 | chapter = Filmography
 }} 

==See also==
* List of American films of 1935
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 

 
{{succession box  Universal Serial Serial 
| before=Rustlers of Red Dog (1935)
| years=The Call of the Savage (1935)
| after=The Roaring West (1935)}}
 

 
 

 
 
 
 
 
 
 
 
 
 


 