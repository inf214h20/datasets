Kangaroo Jack
 
 
{{Infobox film
| name = Kangaroo Jack
| image = Kangaroo jack.jpg
| caption = Theatrical release poster
| alt = A kangaroo wearing sunglasses and read Brooklyn jacket David McNally
| producer = Jerry Bruckheimer
| writer = Steve Bing  Barry O Brien  Scott Rosenberg
| starring = {{Plain list | 
* Jerry OConnell
* Anthony Anderson
* Estella Warren
* Michael Shannon
* Christopher Walken
}}
| music = Trevor Rabin
| cinematography = Peter Menzies Jr. John Murray  William Goldenberg
| studio = Castle Rock Entertainment  Jerry Bruckheimer Films
| distributor = Warner Bros. Pictures
| released =  
| runtime = 89 minutes 
| country= United States  Australia
| language = English
| budget = $60 million
| gross = $88,929,111
}}
Kangaroo Jack is a 2003 American  , was produced and released on video in 2004.

The film was panned by critics. Critics overall expressed dislike for the acting, directing, length, writing and humor inappropriate for a film clearly aimed at children. It received an average rating of 8% on Rotten Tomatoes. 
Despite the poor critical reception, the film was a commercial success.  

==Plot==
In 1982, a boy named Charlie Carbone (Jerry OConnell) is about to become the stepson of a mobster named Salvatore Maggio (Christopher Walken). On that same day, he meets his new best friend, Louis Booker (Anthony Anderson), who saves him from drowning, and the mobsters apprentice Frankie Lombardo (Michael Shannon), who tried to drown Charlie intentionally.

Twenty years later in 2002, Charlie has his own beauty salon, and Louis is still his best friend, but Sals Goons take a majority of the profit leaving Charlie very little for improvements. After they botch the job of hiding some stolen goods, resulting in some of Sals men getting arrested, Sal gives Charlie and Louis one more chance. Under instructions from Frankie, they have to deliver a package to Australia to a man named Mr. Smith. Frankie also tells them that should they run into trouble, they should call Mr. Smith at the phone number he gives them. Unknown to Charlie and Louis, Sal tells his Capo that he is "cancelling their return trip."

Once on the plane Louis peeks into the package, to find $50,000 in cash.

On their way to Mr. Smith, they run over a  ) and tell him about the situation. Mr. Smith tells Louis that they had better have his money when he comes after them, which he does.

Back in New York, Salvatore gets a call from Mr. Smith saying that Charlie and Louis havent arrived yet. Salvatore sends Frankie and some men to Australia to look into it.

Meanwhile, one of the attempts to reclaim the money strands Charlie and Louis in the desert. They finally get help from a woman named Jessie (Estella Warren) to help them catch the kangaroo at the nearest valley.

Quite unexpectedly, they get attacked by Mr. Smith and his henchmen. Charlie and Louis outsmart them, only to find Frankie joining the battle.

After getting the money back from the kangaroo, they learn from Frankie that Sal really sent them to Australia to pay for their own execution, but all of a sudden, police come at the right moment and arrest Frankie, Mr. Smith, and their henchmen and Charlie reclaims Louiss lucky jacket from the kangaroo.

One year later, Charlie and Jessie are married and sell their new shampoo, Frankie and his men have been imprisoned for life, which Sal has failed at avoiding. Louis is Charlies advertising partner. As for the kangaroo (called Kangaroo Jack), he is still hopping around the outback.

==Cast==
* Jerry OConnell as Charlie Carbone
* Anthony Anderson as Louis Booker
* Estella Warren as Jessie
* Christopher Walken as Salvatore Sal Maggio
* Dyan Cannon as Anna Carbone
* Adam Garcia as Kangaroo Jackie Legs Jack (voice, uncredited)
* Michael Shannon as Frankie Lombardo
* Marton Csokas as Mr. Smith Bill Hunter as Blue
* Tony Nikolakopoulos as Sals Capo

==Reception==

 

==Box office== MLK opening weekend, and $21,895,483 over the 4-day MLK weekend, ranking #1 that weekend. It grossed $66,934,963 at the North American domestic box office and $21,994,148 internationally for a worldwide total of $88,929,111.

===Critical response===
Review aggregation website Rotten Tomatoes gave the film a rating of 8% based on 113 reviews, with an average score of 3.2 out of 10.  Joe McGovern in the Village Voice described Kangaroo Jack as "witless" and stated "The colorless script...seems to have written itself from a patchwork of Wile E. Coyote cartoons, camel farts, and every high-pitched Aussie cliché to have echoed on these shores".
 Joe McGovern, " ". Village Voice.
January 18, 2003. Retrieved January 17, 2014.  Nathan Rabin, reviewing the film
for the AV Club, remarked "Kangaroo Jacks premise, trailer, and commercials promise little more than the spectacle of two enthusiastic actors being kicked over and over again by a sassy, computer-animated kangaroo—and, sadly, the film fails to deliver even that." Nathan Rabin, " ". The AV Club. January 27, 2003.
Retrieved January 17, 2014.  Gary Slaymaker in the British newspaper The Western Mail said "Kangaroo Jack is the most witless, pointless, charmless
drivel unleashed on an unsuspecting public". 

===Accolades===
  
For their performances, Anthony Anderson and Christopher Walken were both nominated for Worst Supporting Actor at the  . The Australian
newspaper The Age included Kangaroo Jack on its list of "worst films ever made". 

Jerry Bruckheimer started working exclusively with Disney following the release of Bad Boys II six months later.  

==Sequel==
An animated  , was released direct-to-video on November 16, 2004.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 