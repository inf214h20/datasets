L.A. Without a Map
 
 
{{Infobox film
| name = L.A. Without a Map
| image = 
| alt = 
| caption = 
| director = Mika Kaurismäki
| producer = {{Plainlist|
* Pierre Assouline
* Julie Baines
* Sarah Daniel
* Deepak Nayar}}
| screenplay = Richard Rayner
| story = Mika Kaurismäki
| based on =  
| starring = {{Plainlist|
* David Tennant
* Vinessa Shaw
* Julie Delpy
* Vincent Gallo}}
| music = Sébastien Cortella
| cinematography = Michel Amathieu
| editing = Ewa J. Lind
| production companies = {{Plainlist|
* Dan Films
* Euro American Films
* Marianna Films}}
| distributor = United Media
| released =  
| runtime = 107 minutes  
| country = {{Plainlist|
* France
* United Kingdom
* Finland}}
| language = {{Plainlist|
* English
* French}}
}} romantic comedy-drama British and Finnish production.

Also known under the titles: Los Angeles Without a Map, and I Love L.A. (France)

==Plot summary==
While visiting a Northern English city, Barbara (Vinessa Shaw), an aspiring Hollywood actress, has a fling with the town undertaker Richard (David Tennant), who also writes obituaries for the local paper and has written one unpublished novel, Uzi Suicide. Barbara returns home, where she works as a waitress at a Japanese restaurant. She brags about the handsome writer she met while away. Unfortunately, Richard follows her to Hollywood.

==Plot details==
 
The film opens with Richard explaining to the audience about his existence in Bradford. While attending one of his funerals, Richard meets Barbara, an aspiring American actress passing through the city for the day. The two enjoy their limited time together before Barbara is due to return to Los Angeles, where she makes a living working as a waitress in a French restaurant in the hopes that she is spotted by a talent scout. Unable to forget about her, Richard promptly leaves Bradford without announcing so to his fiancé.

Richard quickly finds Barbara who is shocked to see that he has followed her to America – she had told her friends at the restaurant of Richard, allowing them to believe that he is a famous British writer. Embarrassed, Barbara shoos Richard away, asking him to accompany her on a photo shoot outside the town. Richard charms Barbara throughout their second day together, defending her honor when the photographer tried to seduce her. However, moments after he announced that the two of them should be married immediately he is introduced to Patterson, Barbaras boyfriend. She asks Richard, who doesnt drive, to take her car as she is leaving with Patterson – he does so regardless and finds himself surrounded by Hollywood cops. He must leave the car in an uneasy neighbourhood before returning to his hotel.

When Richard returns to the car he finds that it has been vandalized by the locals. As he panics over what Barbara would think, a man named Moss approaches him, and proceeds to show Richard around the house opposite, which has recently become available to rent. Despite its more-than-run-down appearance, Richard agrees to rent the place. As he is making the house a little more inhabitable, Moss drops by to let Richard know that he convinced the neighbors to fix up Barbaras car as Richard is now like one of the family.  Richard calls Barbara at the restaurant to let her know that hes renting a place and they arrange to meet the next day. Barbara asks Richard to play along with the game that he is a photographer of Barbaras previous shoot so that Patterson doesnt suspect that there could be something between them. However, Patterson makes it clear that if Richard was to try to be more than friends with Barbara, terrible things would be done to him. A little shaken, Richard returns home.

Moss helps Richard get a job for Clear Blue Yonder, a pool cleaning company. The pair visit wealthy Hollywood homes to clean the swimming pools of the rich. Despite trying to keep busy, Richard remains impatient to see Barbara again. In his desperation, he blows his credit on a new guitar for Moss and he spends his free time smoking, drinking and sitting by the phone. Barbara finally calls; she and Richard go to the beach and she tells Richard that, although she likes him, its safer for her future career if she stays with Patterson. In response he tells her that hed have gone to the North Pole for her.

Richard takes Barbara to a club, where Moss and his band (Leningrad Cowboys) are playing. Richard introduces Moss to Barbaras waitress friend Julie and the two get along famously. Meanwhile, Barbara sees Patterson getting too friendly with the Head Waitress at the restaurant and she finally leaves him. Richard, Barbara, Moss and Julie spend the night on the road, before Moss and Julie leave to spend time together in a hotel. After testing Richards sincerity about loving her, Barbara agrees to marry him and the pair have a wedding.

Richard and Barbara are married and Barbara has an audition for a film. She has also arranged a meeting for Richard with a well-known screenwriting agent, Takowsky. Richard hands over Uzi Suicide and returns home to work on more script ideas. Barbara arrives back in a fantastic mood after her successful audition, and she, Richard, Moss and Julie all go out to celebrate. Following a day at Speedzone, the four finish their celebrations at a bar, where it is revealed that Patterson is the director of Barbaras film.  Richard, in an attempt to appear better than Patterson, gets himself drunk and embarrasses not only himself, but Barbara too.

Soon after the party Barbara tells Richard that shes no longer in the film, and Richard suggests that they call Moss and Julie to go out for a drink together. Moss and Julie let slip that Patterson had told Barbara that she could have the part in the movie if she went to bed with him – Richard asks Barbara if she was tempted, and she was. The couples marriage begins to deteriorate from there on out. Richard follows Barbara (in the boot of her car) to her meeting with Patterson, but that doesnt get that far following an incident in a car wash, and Barbara leaves Richard to return to Patterson. Impatient to get Barbara back in his life, Richard finds himself friendless and alone. He wanders through a cemetery where he meets a man on a bench, and they share a bucket of chicken together and swap stories of lost loves.

Richard visits the restaurant, where Julie tells him that Barbara doesnt wish to see him anymore. He overhears that Barbara will be waiting at a party later and is advised by the restaurants owner that if he cant win Barbara back, then he should kill her. Deciding that killing Barbara would be unwise, Richard acquires a drug to knock her unconscious in case he cannot convince her to return to his side. Moss accompanies him to the party and he finds Barbara in hardly any time at all. After an argument Richard whips out the drug, but Barbara hits him in the face knocking him to the ground.

Richard spends the night in prison and is released with the help of Takowsky. He returns to Bradford, where his funeral business has been kept afloat by a colleague. He and his ex-fiancé are on speaking terms only, which is fine by him. And then he sees Barbara, in the same place as where they first saw each other. She has brought him back his Uzi Suicide, which has been approved by Takowsky, along with his first pay check. They decide to patch up their relationship starting afresh in Bradford.

==Cast==
 
 
* David Tennant as Richard
* Vinessa Shaw as Barbara
* Julie Delpy as Julie
* Vincent Gallo as Moss
* Joe Dallesandro as Michael
 
* Amanda Plummer as Red Pool owner
* Lisa Edelstein as Sandra
* Cameron Bancroft as Patterson
* Saskia Reeves as Joy
* Matthew Faber as Joel
 
* Anouk Aimée as herself
* Monte Hellman as himself
* Robert Davi as himself Spielberg man
* Malcolm Tierney as Joys father
 
* Dominic Gould and Andre Royo as Music store clerks
* Dijon Talton as Kid
* Leningrad Cowboys as themselves William Blake
* Mika Kaurismäki (uncredited) as Michael Bambihill
 

==Notes== Little Germany", and Undercliffe Cemetery in Bradford, West Yorkshire.
* The film was released on DVD, notably in France and Germany.  The German edition is in spoken English with optional German subtitles. The French edition is dubbed in French.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 