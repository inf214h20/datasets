Majajan
 
{{Infobox film
| name = Majajan
| image = Saima in majajan.jpg
| image_size = 
| caption = Theatrical release poster
| director = Syed Noor
| producer = Saima Syed Noor
| writer = Rukhsana Noor Shaan Saima Madiha Shah
| music = Wajid Ali Nashad 
| cinematography = Waqar Bokhari
| editing = Aqeel Ali Asghar 
| released = January 6, 2006
| runtime = 140 min
| country = Pakistan Punjabi
| budget = 
| domestic gross = 
| preceded_by = 
| followed_by =
}}

Majajan is a Pakistani Punjabi film directed by Syed Noor which was released across theaters in Pakistan in January 2006. It is reportedly doing very well at the box office. Director of Majajan Syed Noor married the heroine Saima during this film. Majajan is a love story Syed Noor says he "made most passionately". Inspired by the life of Baba Bulleh Shah and his ishq with his murshad,

==Synopsis== Shaan plays Zil-E-Shah an unhappily married man who belongs to the Syed clan, who falls in love with a courtesan, by the name of
Taari (Saima)who arrives to perform in his village. His wife (Madiha Shah) and family condemn their relationship.

==Box office performance==
Majajan and has crossed Diamond Jubilee (100 weeks) in Lahores cinemas. It also did Solo Silver Jubilees on its two main cinemas Metropole and Sozo Gold in Lahore. This film also defeated three great Bollywood films Mughl-e-Azam, Taj Mahal and Bride & Prejudice .

==Inspiration==
In a newspaper interview, director Syed Noor said, ""When I read the verses of Baba Bulleh Shah I promised myself that I will make a film on the great Sufi poets ishq and I was looking for an opportunity to do that. So, I discussed the story with my wife (Rukhsana Noor) who is also a writer. She converted it into a full-fledged script and finally I created Majajan... (Saima) didnt compromise on quality, and spent lavishly on making Majajan a masterpiece." 
 Shaan is playing this kind of role for the first time.

==Cast==
* Shafqat Cheema 
* Saima 
* Madhiha Shah 
* Shaan Shahid

==See also==
* Syed Noor
* Jhoomar
* Lollywood

==References==
 

==External links==
*  

 

 
 
 
 
 


 