The Beloved (2011 film)
{{Infobox film
| name = Beloved
| image = The Beloved.jpg
| alt = 
| caption = 
| director = Christophe Honoré
| producer = 
| writer = Christophe Honoré Paul Schneider
| music = Alex Beaupain
| cinematography = Rémy Chevrin
| editing = 
| studio = Why Not Productions
| distributor = Le Pacte
| released =  
| runtime =
| country = France
| language = French
| budget = € 6.8 million
| gross =
}} Paul Schneider, Michel Delpech and Chiara Mastroianni. The story is set in the 1960s through the 2000s in Paris, Rheims, Montreal, Prague and London.  While not a musical, the characters use musical narration and dialogues throughout the film.

==Production==
The film is produced by Why Not Productions.    The project received 228,000 euro in support from the Ile-de-France Regional Support Fund for Technical Film and Audiovisual Industries.  Apart from the French investment, 20% of the 6.8-million-euro budget came from the United Kingdom and 10% from the Czech Republic.  Filming started on 18 October 2010. 

==Release==
The film closed the 2011 Cannes Film Festival. The French release was 17 August 2011 through Le Pacte.  It also premiered at the Toronto International Film Festival on 11 September 2011.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 