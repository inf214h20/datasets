Uthrada Rathri
{{Infobox film
| name = Uthrada Rathri
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer = L Rajalekshmi Kunjamma
| writer = Balachandra Menon
| screenplay = Balachandra Menon Madhu Shobha Sukumaran Sasi
| music = Jaya Vijaya
| cinematography = Hemachandran
| editing = P Raman Nair
| studio = Nangasserry Films
| distributor = Nangasserry Films
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by Balachandra Menon and produced by L Rajalekshmi Kunjamma. The film stars Madhu (actor)|Madhu, Shobha, Sukumaran and Sasi in lead roles. The film had musical score by Jaya Vijaya.   

==Cast==
   Madhu 
*Shobha 
*Sukumaran 
*Sasi
*Kaviyoor Ponnamma 
*Sankaradi 
*Adoor Bhavani 
*Aranmula Ponnamma 
*Kanakadurga 
*Kuthiravattam Pappu 
*Mallika Sukumaran 
*Ravi Menon 
 

==Soundtrack==
The music was composed by Jaya Vijaya. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Bhramanapadham Vazhi || K. J. Yesudas || Bichu Thirumala || 
|- 
| 2 || Manju Pozhiyunnu || Vani Jairam || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 


 