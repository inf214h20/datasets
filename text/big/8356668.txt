Tycus
 

Tycus is a 1998 direct-to-video movie starring Dennis Hopper, Peter Onorati, Finola Hughes, Alexander Cleir and others. It has been released in 2000, in the United Kingdom, France and the United States.

== Plot summary ==
 
In 1989, astronomer Dr. Peter Crawford discovers a comet on a collision with Earth. He reports his findings to the council, but they refuse to listen to him. Ten years later the comet, now called Tycus, is rapidly closing on the Earth. But Tycus is not on a collision course with the Earth; Tycus is going to impact the moon. If Tycus does impact the moon, it will be shattered and the fragments will be pulled in by Earths gravity.

Dr. Crawford begins the construction of a vast city located under the Sierra Mountains. His city would provide a safe haven for a few people who would become the beginning of the new age after the catastrophe. A young reporter, Jake Lowe, is investigating the building site when he finds out that only a few people get to inhabit the city. So he requests to Dr. Crawford that he and his pregnant wife be allowed entrance into the city because he discovered the city’s existence. Dr. Crawford agrees as he also wants his wife and daughter in the city. At the same time, a nuclear missile fails to do anything to stop Tycus.

They plan to fly Crawfords jet to Los Angeles, now in a state of chaos as news of the comet is made public. Jake finds his wife and gets her to the rendezvous point. Unfortunately, someone has already taken the jet so Jake, his wife, and Crawfords family use a pick-up truck to reach the city. Tycus then smashes into the moon, shattering it.

As the fragments approach, Crawfords group sees a group of people desperately waiting for the doors to the city to open. Crawford activates the elevator and walks towards the door. After they all entered the elevator, Crawford is grabbed by the group of people and he is forced to stay behind. The elevator descends into the underground city.

The debris starts to rain down to Earth. The fragments obliterate every major city in the world, cause massive disasters like tsunamis, and much of the human race is wiped out.

In the year 2029, after the Earth has recovered from the disaster, a young woman, implied to be Jakes daughter, tells a group of people sitting in the Sierra Mountains of an ancient world where highways and buildings covered the Earths surface; she tells of Dr. Crawford, who first discovered the little dot in the sky and how that little dot would lead to the downfall of mankind. As the story continues, She tells of how thousands of moons reminds the survivors of the New Age and we are shown that the pieces left of the moon have formed an asteroid belt around the Earth…
 Air America amongst several films.

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 
 