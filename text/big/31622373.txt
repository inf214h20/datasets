Bbuddah... Hoga Terra Baap
 
 

{{Infobox film
| name = Bbuddah... Hoga Terra Baap
| image = Bbuddah Poster.jpg
| caption = Theatrical release poster
| director = Puri Jagannadh
| producer = Abhishek Bachchan Puri Jagannadh Viacom 18 Motion Pictures
| screenplay =
| story = Puri Jagannadh
| based on =  
| starring = Amitabh Bachchan Hema Malini Sonu Sood Prakash Raj Sonal Chauhan Raveena Tandon Charmy Kaur Ajaz Khan
| music = Songs: Vishal-Shekhar Background Score: Anoop Rubens 
| cinematography = Amol Rathod
| editing = S R Shekhar
| studio =
| distributor = Amitabh Bachchan Corporation Limited Viacom 18 Motion Pictures
| released =  
| runtime = 114 minutes
| country = India
| language = Hindi
| budget =  
| gross =  
}}

Bbuddah... Hoga Terra Baap (Literal translation: Oldman...is your dad, Meaningful translation: Not me, Old man might be your dad) (earlier titled Buddah) is a 2011  . The film stars Amitabh Bachchan, Hema Malini, Sonu Sood, Prakash Raj, Charmy Kaur, Sonal Chauhan, Raveena Tandon. The film released on 1 July 2011, and received positive response from critics. 

==Plot==
ACP Karan Malhotra (Sonu Sood) has declared that he will eliminate all gangsters from Mumbai within two months. Gangster Kabir Bhai (Prakash Raj) decides to eliminate ACP Karan instead. So in comes Vijju (Amitabh Bachchan), a hitman who returns to Mumbai after a long exile in Paris, France in order to perform one last job.

While the gangsters and Vijju try to take down ACP Karan, ACP Karan is trying to woo his old college friend Tanya (Sonal Chauhan) and Tanyas friend Amrita (Charmee Kaur) is trying to figure out the relationship between Vijju and her mother (Raveena Tandon). Vijju later reveals that he is not the contract killer and is rather trying to protect his son, ACP Karan.

The story rolls on showing Vijju climbing the ladder of underworld to reach Kabir Bhai and plot with him the murder of Karan. One of Kabir Bhais associate Year (Subbaraju) goes against his decision of roping in Vijju and attempts to shoot Karan. He misses narrowly and returns to base, facing mockery from Vijju. Meanwhile Vijju meets his estranged wife Sita (Hema Malini) and tries to patch the relationship. Year attempts once again and is successful to pump some bullets into Karan, failing Vijjus attempts to stop him from going to the place of shoot-out. A hurt and angry Vijju then admits Karan to a hospital and visits Kabir Bhais den. After reciting a short story and explaining three morals, Vijju informs that Karan is alive and the den is surrounded by police. A fierce shoot-out ensues in which Vijju kills all the gangsters at the den except Kabir Bhai and his aide Mac (Makrand Deshpande) who roped in Vijju to the underworld don. A surprised Kabir Bhai is shot in the head by Vijju but he leaves his aide. The movie ends at the hospital where Vijju asks his wife to decide if she would reveal his identity to Karan. He also says that he would come back in case either his wife or son are in trouble ever and if he does not come back, it would mean that he has passed away, given his old age. To this, Sita responds "Bbuddah... Hoga Terra Baap."

==Cast==
* Amitabh Bachchan as Vijay Vijju Malhotra
* Hema Malini as Sita
* Raveena Tandon as Kamini (Special Appearance)
* Sonu Sood as ACP Karan Malhotra
* Rajeev Verma as Mirchi Baba
* Sonal Chauhan as Tanya
* Prakash Raj as Kabir
* Charmy Kaur as Amrita
* Makrand Deshpande as Mac
* Subbaraju as Yera
* Vishwajeet Pradhan as Sub-Inspector Shindey
* Ajaz Khan as supari killer

==Production==
The director Jagannath wanted to rope in three leading heroines for the movie. Finally he zeroed down on Hema Malini, Raveena Tandon and Charmy Kaur, an actress from South India who will be making her Bollywood debut in this movie.    Actress Neha Sharma was also interested in doing Kaurs role, but eventually the role went to Kaur.  Tandon later said that she signed the movie to work with Bachchan and Jagannath.    For another role in the movie, both Sonal Chauhan and Kangana Ranaut were in discussion, but eventually the role went to Chauhan.   

The mahurat shot of the movie was done in March 2011 at Khoja Bungalow in Versova. While Jagannath broke the coconut, actor Sonu Sood featured in the mahurat shot.    The event was also attended by Bachchan, his son Abhishek Bachchan, daughter-in-law Aishwarya Rai Bachchan, and actress Sonal Chauhan.   

On 26 April 2011, movies shooting was disrupted, when Raj Thackeray led Maharashtra Navnirman Sena claimed that there were around 40 foreigners shooting in the movie without proper work permits and visas. MNS filed a police complaint in this regard and later stalled the shooting.         Following day, director Jagannath apologised to the outfit stating that there were indeed few junior foreign artists in the shoot without proper documentation, and assured to be more vigilant in future.    Raveena Tandon has also shot for an item song in the film, titled "Main Chandigarh Di Star".

==Marketing== IPL match of Mumbai Indians and Rajasthan Royals.   

==Critical reception==
Raja Sen of Rediff gave it four out of five stars and stated "Bbuddah Hoga Tera Baap is not a particularly well-crafted film but none of that matters as Amitabh Bachchan makes it work."  Behindwoods gave a score of two out of five and said "If Amitabh is all that matters, watch it."  Nikhat Kazmi from the Times of India gave a three and half stars and stated "Bbuddah... is a high dose entertainer when the veteran actor never stops amazing you with the range of his histrionics. Despite his age, he grabs eyeballs with his action cuts, his comic cameos, his romantic ditties (with Hema), his libidinous encounters (with Raveena), his emotional bytes, his derring-do, and his over-the top sartorial sense."  Critic Taran Adarsh gave a four out of five stars and noted "On the whole, Bbuddah Hoga Terra Baap is a must-watch for Bachchan fans. Even if youre not a fan of this iconic actor, watch it for a simple reason: They dont make them like Amitabh Bachchan anymore. A masala entertainer all the way. Bachchan is truly the Baap and this film reiterates this fact yet again. His character, his attitude and the dialogue he delivers will remain etched in your memory for a long, long time. Prakash Raj is super. In fact, its a treat to watch powerful actors like Bachchan and Prakash Raj embroiled in a confrontation." 

Filmfare gave a four star rating and said "Bbuddah... Hoga Terra Baap is a feast for Amitabh Bachchan buffs. The film ends with a disclaimer saying it’s a tribute to the phenomenon and that says it all."  Zee News also gave four stars and stated "‘Bbuddah Hoga Terra Baap is a typical Bollywood masala flick with all the ingredients to make it a Box Office hit. An out-an-out Amitabh Bachchan film, Bbuddah Hoga Terra Baap presents Big B in never before seen role, something which reminds you the exhilarating performance of the iconic star in his earlier films."  Sify gave a two and half star rating and explained "Big B`s presence is so overwhelming he makes the character— a violent, unlikable one—into a somewhat charismatic one. The altercations between Bachchan and villain (Prakash Raj, fab) are to watch out for especially towards the end. Also worth savoring are the Amitabh-Hema portions where they create magic momentarily. It`s an average story with archaic storytelling. If you`re watching it, keep in mind that Big B is the only thing going for the film."  Komal Nahta of Koimoi gave it 2 stars. 

==Box office==
Bbuddah... Hoga Terra Baap was made at a controlled budget of approximately Rs. 100&nbsp;million. The film had a slow start on the first day but picked up over the weekend with major patronage from family audiences and Amitabh Bachchan fans and declared a super Hit.  The domestic opening weekend collection were around  .  In the overseas markets, the film collected   from US, UK, UAE and Australia over the opening weekend.  Within nearly a week post the release, the film collected   net domestically and another   net overseas.  The film collected   nett over its second weekend,  bringing the two-week total to  .  BHTB Recovered all its production cost from theatrical revenues and additional non-theatrical earnings thus ensuring it to be a good profitable venture.  The satellite rights fetched the producers approximately   

==Soundtrack==
{{Infobox album
| Name = Bbuddah Hoga Terra Baap
| Type = Soundtrack
| Artist = Vishal-Shekhar
| Cover =
| Released = 13 June 2011
| Recorded =
| Genre = Film soundtrack
| Length =
| Label = T-Series
| Producer =
}}

Music of the film is composed by Vishal-Shekhar while the lyrics are penned by Anvita Dutt Guptan, Vishal Dadlani and Swanand Kirkire.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| title1 = Bbuddah Hoga Terra Baap (Accapella)
| extra1 = Amitabh Bachchan
| lyrics1 = Vishal Dadlani
| length1 = 2:46
| title2 = Bbuddah Hoga Terra Baap (Dub Step)
| extra2 = Amitabh Bachchan, Vishal Dadlani
| lyrics2 = Vishal Dadlani
| length2 = 3:21
| title3 = Go Meera Go
| extra3 = Amitabh Bachchan, Abhishek Bachchan
| length3 = 6:42
| lyrics3 = Anvita Dutt Guptan
| title4 = Haal-E-Dil
| extra4 = Amitabh Bachchan, Monali Thakur, Shekhar Ravjiani
| lyrics4 = Swanand Kirkire, Anvita Dutt Guptan
| length4 = 5:20
| title5 = Main Chandigarh Di Star
| extra5 = Sunidhi Chauhan
| lyrics5 = Anvita Dutt Guptan
| length5 = 3:18
}}

==References==
 

==External links==
*  
*  

 

 
 
 