Reign Over Me
 
{{Infobox film
| name = Reign Over Me
| image = ReignPoster.jpg
| caption = Promotional poster
| alt = 
| producer = Jack Binder Michael Rotenberg
| director = Mike Binder
| writer = Mike Binder
| starring = {{Plain list | 
* Adam Sandler
* Don Cheadle
* Jada Pinkett Smith
* Liv Tyler
* Saffron Burrows
* Donald Sutherland
* Mike Binder
}}
| music = Rolfe Kent
| cinematography = Russ Alsobrook
| editing = Steve Edwards  Jeremy Roush
| studio = Relativity Media  Happy Madison|Mr. Madison 23 Productions  Sunlight Productions
| distributor = Columbia Pictures
| released =  
| country = United States
| runtime = 124 minutes
| language = English
| budget = United States dollar|US$20 million    
| gross = $22,222,308  
}} buddy comedy-drama film  written and directed by Mike Binder, and produced by his brother Jack Binder.  The film stars Adam Sandler, Don Cheadle, Jada Pinkett Smith, Liv Tyler, Donald Sutherland, Saffron Burrows and Mike Binder himself.

Distributed by Columbia Pictures, the film was released on March 23, 2007. The film was released to DVD, and Blu-ray on October 9, 2007. The film received mixed reviews.

== Plot == When the withdrawn shadow of his former self.
 fate brings he wears a headset constantly to let music drown out the upsetting memories of his wife and children.

Though on the surface it would appear that Alan, a successful dentist, has it all, the pressures of a family and career have been weighing heavily on him. At a pivotal moment when Charlie and Alan both need a trusted friend, the restorative power of a rekindled friendship provides a lifeline needed to move forward.

Alan endeavors to bring Charlie out of his shell by convincing him to see a therapist (Liv Tyler). Charlie is barely communicative, however, ending their sessions after only a couple of minutes. His therapist says he needs to tell the story about his family to someone eventually. Charlie soon tells Alan his tragic story, but afterwards tries to commit suicide by cop and ends up in a sanatorium|sanitarium.
 commit Charlie to psychiatric care against his will. The judge leaves the decision to Charlies in-laws, asking them to think of what their daughter would want for Charlie. They decide that he should not be committed; instead, Charlie moves to a new apartment, leaving behind the painful memories associated with his former home. At the end of the film, Alan visits Charlie for the day and his wife calls and tells him "I love you and just want you to come home."

==Cast==
* Adam Sandler as Dr. Charlie Fineman
* Don Cheadle as Dr. Alan Johnson
* Jada Pinkett Smith as Janeane Johnson  
* Liv Tyler as Dr. Angela Oakhurst
* Saffron Burrows as Donna Remar
* Donald Sutherland as Judge David Raines
* Robert Klein as Jonathan Timpleman
* Melinda Dillon as Ginger Timpleman
* Mike Binder as Bryan Sugarman
* Jonathan Banks as Stelter
* John de Lancie as Nigel Pennington
* Rae Allen as Adell Modell
* Paula Newsome as Melanie
* Ted Raimi as Peter Saravino
* B. J. Novak as Fallon
* Joey King as Gina Fineman (uncredited)

Tom Cruise and Javier Bardem were initially signed on to play Adam Sandlers role and Don Cheadles role, respectively. Jennifer Garner was initially signed on to play Liv Tylers role. When Cruise dropped out, Bardem suggested Sandler after seeing him in Punch-Drunk Love. Although Sandler was initially hesitant about the project, he signed on after reading the script for a second time. Bardem later dropped from the project, so Cheadle was given the role.

== Soundtrack ==
  
As music was an important component to the plot, various songs were used during different parts of the film, such as Bruce Springsteens "Out In The Street" and "Drive All Night", "Simple Man" by Graham Nash, and a few songs by The Who, including the titular "Love, Reign oer Me". The latter song appears on the films soundtrack along with a cover version recorded specifically for the film by Pearl Jam. Televised trailers features the songs "Ashes" by English band Embrace (English band)|Embrace, "All These Things That Ive Done" by The Killers, and "In This Life" by Chantal Kreviazuk. The score was written by Rolfe Kent, and orchestrated by Tony Blondal.

== Reception ==
  
The film opened at #8 with a gross of $7,460,690 from 1,671 theaters, for an average of $4,465 per venue. The film closed on April 29, 2007, with a final domestic gross of $19,661,987. It made another $2,560,321 internationally for a total worldwide gross of $22,222,308, against its $20 million budget. 

   normalized rating average score of 61, based on 33 reviews, indicating "generally favorable reviews".    Entertainment Weekly gives Reign Over Me a B&minus; rating, calling the film "a strange, black-and-blue therapeutic drama equally mottled with likable good intentions and agitating clumsiness."  Reviewer Lisa Schwarzbaum shares her own discomfort with seeing the September 11 attacks casually included as a plot device in a fictional dramedy, while praising the films performance and story.

The New York Times found the film "maddeningly uneven", adding, "Its rare to see so many moments of grace followed by so many stumbles and fumbles, or to see intelligence and discretion undone so thoroughly by glibness and grossness. And it is puzzling, and ultimately draining, to see a film that waves the flag of honesty—Face your demons! Speak from your heart! Open up!—turn out to be so phony." 

The video gaming blog Kotaku praised Reign Over Me  s inclusion of the video game Shadow of the Colossus, stating that it "must be one of the first Hollywood films, if not the first, to deal with games thematically and intelligently." 

==See also==
* List of cultural references to the September 11 attacks

==References==
 

==External links==
*  
*  
*  
*   in Berkeley, CA with Adam Sandler, Don Cheadle and director Mike Binder on Sidewalks Entertainment

 

 
 
 
 
 
 
 
 
 
 
 
 