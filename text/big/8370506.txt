Anna Christie (1930 English-language film)
{{Infobox film
| name           = Anna Christie
| image          = Anna Christie 1930 film.jpg
| imagesize      =
| caption        = Theatrical release poster
| director       = Clarence Brown
| producer       = Clarence Brown Paul Bern Irving Thalberg
| writer         = Frances Marion (screenplay) Eugene ONeill  (play)
| starring       = Greta Garbo Charles Bickford George F. Marion Marie Dressler
| music          = William Daniels
| editing        = Hugh Wynn
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = gross = $1.5 million   accessed 19 April 2014 
}} play of the same name by Eugene ONeill. It was adapted by Frances Marion, produced and directed by Clarence Brown with Paul Bern and Irving Thalberg as co-producers. The cinematography was by William H. Daniels, the art direction by Cedric Gibbons and the costume design by Adrian (costume designer)|Adrian.
 Swedish Anna. Broadway production and in both the 1923 and 1930 film adaptations.
 Best Actress Best Cinematography Best Director.

==Plot==
Chris Christofferson (George F. Marion), the alcoholic skipper of a coal barge in New York, receives a letter from his estranged twenty-year-old daughter Anna "Christie" Christofferson (Greta Garbo), telling him that shell be leaving Minnesota to stay with him. Chris left Anna to be raised by relatives on a St. Paul farm 15 years before, and hasnt seen her since.

Anna arrives an emotionally wounded woman with a dishonorable, hidden past: she has worked in a brothel for two years. One night, Chris rescues Matt (Charles Bickford) and two other displaced sailors from the sea. Anna and Matt soon fall in love and Anna has the best days of her life. But when Matt proposes to her, she is reluctant and haunted by her recent past. Matt insists and compels Anna to tell him the truth. She opens her heart to Matt and her father, disclosing her dark secrets.

==Cast==
*Greta Garbo as Anna Christie
*Charles Bickford as Matt Burke
*George F. Marion as Chris Christofferson
*Marie Dressler as Marthy Owens
*James T. Mack as Johnny the Harp
*Lee Phelps as Larry

== Academy Award nominations ==

Anna Christie was the highest-grossing film of 1930 and received the following Academy Award nominations:     
 Best Actress&nbsp;– Greta Garbo Best Director&nbsp;– Clarence Brown Best Cinematography&nbsp;– William Daniels

==German-language version==
In the early years of sound films, it was common for   and Salka Viertel playing Matt, Chris and Marthy. Garbos famous first line became "Whisky&nbsp;– aber nicht zu knapp!" ("Whiskey, but not too short"). The English and German-language versions grossed a combined total of $1,499,000.

== Reception == Capitol Theatre upon its premiere there  and grossing over $1 million nationwide. 
 John Mosher of The New Yorker thought it "implausible that a woman so markedly beautiful should have such an extraordinarily difficult time", he called Garbos performance "effective" and wrote that Bickford and Marion were "both excellent", concluding that it was "a picture of his play that Eugene ONeill, I should think, would approve."   

Contemporary reviews also remarked on the low tone of Garbos voice coming as surprising but soon accepted. Hall wrote that "although the low-toned voice is not what is expected from the alluring actress, one becomes accustomed to it, for it is a voice undeniably suited to the unfortunate Anna."   Variety opined that "La Garbos accent is nicely edged with a Norse "yah", but once the ear gets the pitch its okay and the spectator is under the spell of her performance."   Mosher called it "a boys voice, really, rather flat, rather toneless, yet growing more attractive as the picture advances and you become somewhat accustomed to it."  

In 1962, film historian Richard Schickel reviewed the film negatively, describing it as "dull", with Marie Dressler providing "the only vitality in an otherwise static and ludicrous" film.   

== References ==

 

== External links ==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 