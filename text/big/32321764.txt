Illalu
{{Infobox film
| name           = Illalu
| image          = Illalu 1940.JPG
| image_size     =
| caption        = Illalu 1940 film poster
| director       = Gudavalli Ramabrahmam
| producer       = 
| writer         = Tapi Dharma Rao   (Dialogues )  Tapi Dharma Rao and Basavaraju Apparao  (lyrics) 
| narrator       =
| starring       = V. Umamaheswara Rao Kanchanamala Lakshmirajyam 
| music          = Saluri Rajeswara Rao
| cinematography = 
| editing        = 
| studio         =
| distributor    =
| released       = 1940
| runtime        =
| country        = India Telugu
| budget         =
}}
 
Illalu (English title: Housewife;  ) is a 1940 Telugu Drama film directed by Gudavalli Ramabrahmam.

==The Plot==
Murthy (Umamaheswara Rao) is married to Indira (Kanachanamala) and they live happily. Subsequently he falls in love with Leela (Lakshmi Rajyam), an educated and sophisticated lady. He brings her home as his second wife, making Indira unhappy. However Indira performs her duties as the devoted housewife on one side and wins over her husband with patience and perseverance. Finally Indira emerges successful and they lived happily.

==Cast==
{| class="wikitable"
|-
! Actor / Actress !! Character
|-
| V. Umamaheswara Rao || Murthy
|-
| Kanchanamala || Indira, wife of Murthy
|-
| Lakshmi Rajyam || Leela
|-
| Saluri Rajeswara Rao || Madhu
|-
| Raavu Balasaraswathi || Sarala, wife of Madhu
|-
| S. Varalakshmi ||
|-
| P. Suribabu || Radio artist
|}

==Soundtrack==
{{Infobox album  
| Name = Illalu
| Type = soundtrack
| Longtype =
| Artist = S. Rajeswara Rao
| Cover = 
| Cover size =
| Caption =
| Released =  
| Recorded =
| Genre =
| Length =    Telugu
| Label =
| Producer =
| Reviews = 
| Compiler =
| Misc =
}}
There are some melodious songs written by Tapi Dharma Rao. Music is scored by Saluri Rajeswara Rao. 
# Dina Dinamu Papadni Deevinchipondi Devalokamuloni Devathallaara (Singer: Kanchanamala)
# Jalavihagaali Gaana Vinoda (Singers: Umamaheswara Rao and Lakshmi Rajyam)
# Kavyapaanamu Chesi Kaipekkinaane (Singers: Rajeswara Rao and Balasaraswati)
# Koyilokasari Vachi Koosi Poyane (Singer: S. Varalakshmi)
# Neepai Mohamuno Krishna Nilupagalemoyi Krishna (Singers: P. Suribabu and S. Varalakshmi)
# Suma Komala Kanulela Kala Kaadanuma Bala (Singers: Rajeswara Rao and Balasaraswati)

==Boxoffice==
* It was a Box-office hit those days, resulting in production of many films with similar concept. 
* Andhra Patrika conducted a film ballot. Illalu won the best film, Gudavalli as the best director and Kanchanamala as the best actress awards.

==References==
 

==External links==
*  

 
 
 
 


 