Caine's Arcade
  arcade created by then 9-year-old Caine Monroy out of cardboard boxes and everyday objects. The boy ran his arcade from his fathers auto parts store in Los Angeles during mid-2011. Mullick was inspired to make the documentary after unexpectedly coming across the arcade while getting a part for his car, discovering the level of commitment, work, and thought Caine had put into the arcade, and becoming Caines first customer. In the course of filming the documentary, Mullick arranged for a flash mob from social media sites to come by the arcade, increasing the awareness of the arcade.

The film, on its release to Vimeo, soon became a viral video. Numerous people donated money towards Caines college fund. Subsequently, schools around the world have recreated similar cardboard arcades, and from the positive response, inspired Mullick and the team behind the film to start the Imagination Foundation, a non-profit group aimed to find and foster creativity and entrepreneurship in children across the world.

==Background==
Before 2011, Caine had spent weekends with his father George Monroy at the store, during which he had tried selling goods, including yard signs for supporting sports teams and snacks and drinks from vending machines. He found little success; Monroys store was in an area of East Los Angeles with little foot traffic, and his store was more a physical warehouse for sales made over the Internet.  In summer 2011, Caine found many of the discarded boxes from the store and asked for his fathers permission to create the arcade in the front, which George supported. Caine built and designed all the games himself, creating a ticket and prize redemption system, originally using some of his old toys such as Hot Wheels cars and then into items bought at dollar stores as prizes.     Caine would operate the arcade "machines", retrieving balls and dispensing tickets to the player.    During a family vacation to Palm Springs, Caine had asked for a t-shirt, with "Caines Arcade" on one side and "Staff" on the other to be made for him, despite not knowing what "staff" meant at the time but knowing that he had seen that word used in other places. 

Because of the stores location along an industrial park, the arcade had no customers until Mullick, who needed a new door handle, went to Georges shop. While there, he saw the arcade and talked to Caine about it. Mullick was particularly impressed with some of the basic business fundamentals that Caine implemented, such as offering a $2 "Fun Pass" that offered 500 plays of the games, compared to the four plays one would have gotten with just one dollar.  Mullick proceed to buy a funpass and play the games, unaware that he was Caines first customer, only learning this after speaking later with George, who had said that even one customer made Caine happy. 

==Documentary==
After speaking about the arcade with a friend who was organizing a DIY Days event at UCLA, Mullick imagined that a documentary on the arcade would not only be successful but would also draw more customers to it. He consulted with George on the concept and got permission to film. Mullick organized a flash mob event in October 2011 on Facebook, which ultimately was linked through Reddit.  George arranged to take Caine out for pizza in the afternoon, giving the crowd of more than a hundred time to gather and construct signs. On their return, the crowd cheered on Caine, who was surprised and elated at the demonstration.  Mullick worked to prepare a 14 minute rough cut of the documentary, which premiered at the UCLA DIY Days event later that month. Caine was in attendance, and set up his arcade. 

Mullick released the final 11-minute documentary to the video site Vimeo on April 9, 2012, as well as posting it to popular sites including Reddit and Boing Boing.  The documentary soon propagated in viral video fashion, with over 1 million views the first day, and more than 5 million views within the first five days.    In addition to numerous responses, several emotional video responses were received, which Mullick described as "This is the video that is making grown men cry."   

Mullick is exploring making the short documentary into a feature film after completing other projects related the short films success and influence;    Mullick has stated that he has received offers from film publishers to create the feature-length film. 

==Impact== Fast Company have highlighted the factors that made Caines arcade work that can be applied to any developing business, such as Caines perseverance and optimism.     The arcade was temporarily displayed in the Exploratorium in San Francisco.  Caine continues to operate the arcade on Saturdays to a steady stream of guests, more than five months after the documentarys release.    Other nearby businesses and local musicians have worked to create a street fair-like environment around the arcade on the days that it is open.  Caine was offered a full-scholarship to attend Colorado State University after Mullick and Caine spoke in Denver at the 2013 Colorado Innovation Network Summit.
 Maker Space in Boyle Heights where children will be able to work together to build similar devices out of cardboard and other common objects, and has become an inspirational speaker. 

Mullick began receiving positive feedback from parents and teachers shortly after the release of the video, including video clips of similar cardboard games created by children; one such clip included actor-musician Jack Black and his children.    From this, Mullick worked with volunteer teachers to start the creation of an open-ended curriculum around allowing students to create something that they enjoy in the same manner that Caine approached his arcade with.  In September 2012, nearing the anniversary of the flash mob event, Mullick released a second video, Caines Arcade 2, which primarily was a montage of these video clips.   As a means of launching the Imagination Foundation, Mullick arranged the Global Cardboard Challenge, to encourage creativity with cardboard, and arranged over 270 events across 41 countries, including one at Caines Arcade.  A similar challenge was held in October 2013, with the aim to get more children involved in more countries. 

Two years after its opening, Caine retired from running his arcade, partially as he was entering junior high school, but also to start a new business of a bicycle shop to help repair and remake existing bicycles. 

The Imagination Foundation and the Cardboard Challenge have continued to grow.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 