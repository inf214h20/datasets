Underworld U.S.A.
 
{{Infobox film
| name           = Underworld U.S.A.
| image          = Underworldusa2.jpg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = Samuel Fuller
| producer       = Samuel Fuller
| screenplay     = Samuel Fuller
| based on       =  
| narrator       =
| starring       = Cliff Robertson Dolores Dorn Beatrice Kay Richard Rust
| music          = Harry Sukman
| cinematography = Hal Mohr
| editing        = Jerome Thoms
| studio         = Globe Enterprises
| distributor    = Columbia Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} mobsters who beat his father to death. It stars Cliff Robertson, Dolores Dorn, and Beatrice Kay. 

==Plot== moll (Dolores Dorn) in favor of cold, hard vengeance.
 nihilistic Feud|vendetta eventually robs him of his own humanity (and more).

==Cast==
* Cliff Robertson as Tolly Devlin
* Dolores Dorn as Cuddles
* Beatrice Kay as Sandy
* Paul Dubov as Gela
* Robert Emhardt as Earl Connors
* Larry Gates as John Driscoll
* Richard Rust as Gus Cottahee
* Gerald Milton as Gunther
* Allan Gruener as Smith
* David Kent as Tolly as a teen

==Production==
Producer Ray Stark asked Fuller to write and direct a film based on the title of a magazine article written by Joseph F. Dinneen. Fuller also was inspired by a book, Here Is to Crime, by newspaperman Riley Cooper. 

An opening scene with a Union of Prostitutes was deleted by Sam Briskin and other Columbia executives.   Fullers character Tolly is a loner motivated by revenge using the United States Government as well as his own devices to even the score.  Fuller heard the reaction of a real life gangster who reportedly said "If only my son would have that kind of affection for me!". 

==Reception==

===Critical response=== FBI is shown as stymied without the help of the public coming forth with information or willing to act as witnesses despite the risks, but the lawmen do not play fair by not telling how dangerous its to be a witness against the mob." 

===Legacy===
A wanted poster of Tolly Devlin appears in a police station in Columbias film of Sail a Crooked Ship (1961).

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 