Bol Radha Bol
 
{{Infobox film
| name = Bol Radha Bol
| image = BolRadha.jpg
| director       = David Dhawan
| producer       = Nitin Manmohan
| starring       = Juhi Chawla Rishi Kapoor
| music          = Anand-Milind
| released       = 3 July 1992
| country        = India
| language       = Hindi
}} Hindi film directed by David Dhawan and starring Juhi Chawla and Rishi Kapoor in the lead roles.The film is perhaps best remembered for the hit track "Tu Tu Tu Tara". The film was a big hit at the time of its release and was one of the highest grossing films of the year. Juhi Chawla received a Filmfare Best Actress Award nomination for her performance in the film. The success of the film was attributed to its leading lady, Juhi Chawla due to her immense popularity at that time.

==Plot==

Kishen Malhotra is an industrialist who heads the Malhotra empire. His relatives are his mother (Sushma Seth), his uncle (Alok Nath) and his cousin Bhanu(Mohnish Behl). One day, he finds his cousin defrauding the company and banishes him from the house. To expand his business, Kishen decides to investigate prospects in a village. Here he meets a village belle named Radha (Juhi Chawla).

Besides his work, Kishen starts teaching English to Radha and some other people in village. Kishen and Radha slowly fall in love with each other. Unknown to Kishen, somebody is stalking him and reading his mails. Kishen leaves the village (and Radha) and  promises to come back.

On reaching home, Kishen is surprised to see a mourning at his home. He is even more confused to see people looking astonishingly at him. But, he gets a shock of his life when he sees the portrait of his mother, signifying that his mother is dead. The real sucker punch comes when he sees his duplicate mourning next to his mothers portrait.

When he tells that he is Kishen, his duplicate claims the same. When he prods the duplicate to tell something about real Kishen, the duplicate astonishes him by telling him about his life as well as about Radha. Kishen is unable to think as to how the duplicate could know about Radha, but he knows that his pet dog will identify the real master. When the dog, too, goes to the duplicate, Kishen is thrown out of his own home.

Meanwhile Kishen learns about sudden death of one of his old servants, and he smells foul play. He somehow succeeds to infiltrate into his own office, which is now run by his duplicate. But, he gets arrested. Another servant tells Kishen about his mothers accident, which probably killed her. Kishen also notices that Bhanu is back in home.

Meanwhile, Radha comes to meet Kishen in the city. But, when she sees "Kishen" partying with some girls, she thinks that Kishen has forgotten her. She turns to go back, but the real Kishen has escaped police custody. He stops her and explains the whole story. He, Radha and Goonga (Shakti Kapoor), a dumb convict who has escaped with Kishen, plan to set things straight.

The trio hide themselves in a boat which takes them to Goa. Here, Kishen meets a girl who calls him Tony. Slowly, he comes to realize that his duplicates name is Tony Braganza. Tony works as a saxophonist in a Goan club called Three Aces and has a girlfriend there. Kishen starts gathering all information about Tony.

The trio return to Mumbai. Radha pretends to be a girl named Rita and gains access to Kishens home. Kishen has identified key players by now: Bhanu, Tony and Inspector T.T. Dholak (Kiran Kumar). He creates a rift between the trio. An enraged Tony threatens to expose his partners by what he calls his trump card.

The angered partners take him to the real head behind the game, who is revealed to be Kishens uncle. Now, Tony shows his trump card &mdash; he tells them that Kishens mother is still alive and his men have kept her in captivity. She knows nothing about Tony or the events happening.

Meanwhile, Kishen discovers the depth of the trios planning. He finds that his dog could not identify him as it was not his dog. Like Kishen, his dog, too, had been replaced. Now, Kishen comes back, impersonating Tony and makes Tonys partners throw Tony out the same way Kishen was thrown.

Tony comes to the place where he has hid Kishens mother and convinces her that he is Kishen. Kishen arrives there, too. Kishens mother tricks the duo and finds out who is her son. Just then, all the villains show up at Tonys den. Kishens uncle goes on to explain that he never liked the way his dead brother left the entire estate to Kishens mother.

He explains that Bhanu was not the only one looting the money. When Bhanu was thrown out, he went till Goa to find him, where he met Tony. As the whole plot is explained, Kishen surprises by saying that he had anticipated this. The money he used to lure out the villains was fake.

In another twist, Dholak comes out holding a trigger on Radhas head. Even Kishen is surprised when Goonga re-introduces himself as Inspector Bhinde. He explains that he was sent undercover by the commissioner as even he smelled a rat.

In a melee that ensues, the villains are overpowered and arrested. Tony surprises by acknowledging his true identity and saying that there can be only one Kishen. Kishen and Radha are united.

==Cast==
* Juhi Chawla as Radha
* Rishi Kapoor as Kishen Malhotra/ Tony Breganza double role
* Sushma Seth as Sumitra Malhotra
* Alok Nath as Shanti Prasad
* Mohnish Behl as Kishens cousin Bhanu
* Kiran Kumar as Inspector T.T. Dholak
* Kader Khan as Jugnu
* Kunika as Bijlee
* Yunus Parvez as Bijlees Father
* Shakti Kapoor as Goonga/Inspector Bhende
* Harish Patel as office Servant
* Suresh Bhagvat as Charku Vaid Javed Khan as Chamba village postman
* Anjan Srivastav as Sevak Ram (Kishan Servant)

==Soundtrack==
The album was one of the best selling albums of 1992   and the song Tu Tu Tu Tu Tara was a rage. Anand-Milind composed while Sameer penned the lyrics.

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bol Radha Bol"
| Suresh Wadkar, Sadhana Sargam
|-
| 2
| "Deewana Dil Beqarar Tha"
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
|-
| 3
| "Hawa Sard Hai"
| Abhijeet, Kavita Krishnamurthy
|-
| 4
| "Main Hoon Gaon Ki Gori" Poornima
|-
| 5
| "Tu Tu Tu Tu Tara"
| Kumar Sanu, Poornima
|}

==References==
 

==External links==
* 

 

 
 
 
 