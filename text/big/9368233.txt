Danger Within
 
 
{{Infobox film
| name           = Danger Within
| director       = Don Chaffey
| producer       = Colin Lesslie
| associate producer = Adrian D. Worker Frank Harvey Michael Gilbert (novel) Michael Wilding Richard Attenborough
| music          = Francis Chagrin Arthur Grant
| editing        = John Trumper
| released       = 1959
| distributor    = British Lion Films
| runtime        = 101 min
| country        = United Kingdom
| language       = English
| budget         =
}}
Danger Within is a 1959 British war film set in a prisoner of war camp in Northern Italy during the summer of 1943. A combination of POW escape drama and whodunnit, the movie is based upon the novel Death in Captivity by Michael Gilbert, who had been a prisoner of war, held by the Italians.

==Plot==

A cleverly planned escape attempt, which seemed guaranteed to work, ends in disaster: the would-be escapee is caught and killed by sadistic Capitano Benucci (Peter Arne) within seconds of leaving the POW camp. This incident is witnessed by the other prisoners, who notice that Benucci seemed to be waiting for the escapee to arrive before shooting him dead in cold blood.
 Greek officer, Italian captors, roof fall.
 Peter Jones Michael Wilding) rugby posts. Unfortunately, Benucci and his men are concealed just outside the fence with a machinegun mounted on the back of a truck, and promptly mow them all down. This is the second occasion on which Benucci has deliberately killed escaping POWs in cold blood, when it would have been very easy to capture them alive, instead. The POW escape committee realise that Benucci knew exactly when and where the three POWs planned to escape, and that he had positioned himself in the best place to ambush them. The only logical explanation is that there genuinely is a traitor among the POWs who has betrayed them by passing information to Benucci. This also means that Benucci must already know about another tunnel they are working on, intended for a mass escape of POWs. The prisoners realise that Benucci could easily intervene to prevent the next escape attempt from taking place, if he wanted to. However, Benucci prefers to let preparations continue for sinister reasons: the informer is certain to pass on the date and time of the escape, allowing Benucci to wait at the other end of the tunnel with a machinegun to shoot as many POWs as he can.
 Germans as Italian Armistice. The escape plan devised by Lieutenant Colonel Huxley (Bernard Lee) is for the prisoners to make their escape during the day, under cover of a production of Hamlet in the theatre hut by a group of POWs led by Captain Rupert Callender (Dennis Price). Benucci would never imagine that the POWs will try to escape in broad daylight, which is precisely why the POWs intend to do it.

==Cast==
*Richard Todd as Lieutenant Colonel David Baird
*Bernard Lee as Lieutenant Colonel Huxley Michael Wilding as Major Charles Marquand
*Richard Attenborough as Captain Bunter Phillips
*Dennis Price as Captain Rupert Callender
*Donald Houston as Captain Roger Byfold
*William Franklyn as Captain Tony Long
*Vincent Ball as Captain Pat Foster
*Peter Arne as Capitano Benucci Peter Jones as Captain Alfred Piker Ronnie Stevens as Lieutenant Meynell, The Sewer Rat 
*Terence Alexander as Lieutenant Gibbs
*Andrew Faulds as Lieutenant Commander Dopey Gibbon, R.N. 
*Steve Norbert as Lieutenant Pierre Dessin  
*Cyril Shaps as Lieutenant Cyriakos Coutoules  
*Eric Lander as Lieutenant Tim OBrien
*John Dearth as Lieutenant Robson
*Robert Bruce as Doc Simmonds, R.A.M.C.
*Harold Siddons as Captain Tag Burchnall
*Ian Whittaker as 2nd Lieutenant Betts-Hanger
*Michael Caine as POW (uncredited)

==Filming locations==

Locations for the film were Chobham Common, Surrey and Shepperton Studios.

==Notes==

The film was retitled Breakout for the US market.

==See also==
*Stalag 17 (similar war film involving an informer inside an American POW camp)

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 