Let's Hope It's a Girl
{{Infobox film
| name = Lets Hope Its a Girl
| image = Speriamo che sia femmina.jpg
| caption =
| director = Mario Monicelli
| writer = Mario Monicelli, Suso Cecchi dAmico, Leo Benvenuti, Piero De Bernardi, Tullio Pinelli
| starring =
| music = Nicola Piovani
| cinematography = Camillo Bazzoni
| editing = Ruggero Mastroianni
| producer = Giovanni Di Clemente
| distributor = Clemi Film
| released =  
| runtime = 120 minutes
| awards =
| country = Italy
| language =  Italian
| budget =
}} 1986 Cinema Italian comedy Best Director the same Best Supporting Best Supporting Best Producer, Best Editing Best Script. 

== Plot ==
Elena Leonardi abandoned her husband and left Rome. While she lives with her daughters and other relatives in the Tuscany region, her husband dies in a car accident. The grieving daughters blame their mother and decide to return to Rome. But, unforeseen twists and turns prevent that from happening. In the end the family reunites and continues to live together in the countryside.

== Cast ==
* Liv Ullmann: Elena 
* Catherine Deneuve: Claudia 
* Philippe Noiret: Count Leonardo 
* Giuliana De Sio: Franca 
* Stefania Sandrelli: Lori Samuelli 
* Bernard Blier: Uncle Gugo
* Giuliano Gemma: Guido Nardoni 
* Athina Cenci: Fosca 
* Paolo Hendel: Mario Giovannini 
* Lucrezia Lante della Rovere: Malvina 
* Adalberto Maria Merli: Cesare Molteni 
* Nuccia Fumo: Rosa Nardoni  Paul Muller: Priest
* Carlo Monni: Autista

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 