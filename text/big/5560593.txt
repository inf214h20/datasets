The Puffy Chair
{{Infobox film |
  name           = The Puffy Chair|
  image          =  The Puffy Chair poster.jpg|
  writer         = Mark Duplass Jay Duplass |
  starring       = Mark Duplass Katie Aselton Rhett Wilkins Bari Dahan Gerald Finnegan Julie Fischer |
  director       = Jay Duplass Mark Duplass  (uncredited)  |
  producer       = Mark Duplass Jay Duplass  (uncredited)  |
  studio         = Netflix |
  distributor    = Roadside Attractions |
  released       =   |
  runtime        = 85 minutes |
  music          = |
  country        = United States
  language       = English |
  budget         = $15,000 |
  gross          = $192,467 |
}}

The Puffy Chair is a 2005 road movie written and directed by the brothers Jay Duplass and Mark Duplass.  It was screened at the 2005 Sundance Film Festival and won the Audience Award at the 2005 SXSW Film Festival. It is considered part of the Mumblecore movement. Extensive improvisation is used in the film. The Duplasss mother and father play the mother and father in the movie. The film was shot on the Panasonic AG-DVX100.

==Plot==
The film concerns the relationships between men, women, brothers, mothers, fathers and friends. The protagonist discovers on eBay a replica of a lounge chair that was used by his father long ago. The resulting road trip to pick up and deliver the chair as a birthday present for the father in Atlanta takes interesting twists.

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 