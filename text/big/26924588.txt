Cruel Map of Women's Bodies
{{Infobox film
| name = Cruel Map of Womens Bodies
| image = Cruel Map of Womens Bodies.jpg
| image_size = 
| caption = Theatrical poster for Cruel Map of Womens Bodies (1967)
| director = Masanao Sakao 
| producer = 
| writer = 
| narrator = 
| starring = Naomi Tani
| music = 
| cinematography = 
| editing = 
| studio = OP Eiga|Ōkura Eiga
| distributor = Ōkura Eiga
| released = October 28, 1967
| runtime = 71 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1967 Japanese pink film directed by Masanao Sakao. It is significant for being future "Sadomasochism|S&M Queen" Naomi Tanis first starring role in a film dealing primarily with S&M.

==Synopsis==
Yōko is a young woman who is forced into prostitution by a yakuza gang. She repeatedly escapes from the gang, and is repeatedly captured, and repeatedly tortured at length.       

==Cast==
* Naomi Tani as Yōko 
* Sachiko Inoue
* Miki Hayashi as Harue
* Jōji Nagaoka as Asada
* Akio Shirakawa as Akagawa
* Sanpei Nawa as Aoki
* Hiroko Fuji

==Background==
Director Masanao Sakao filmed Cruel Map of Womens Bodies for OP Eiga|Ōkura Eiga and it was released theatrically in Japan by that studio on October 28, 1967.    Sakao made several films about prostitutes throughout his career.  In Virgins With Bad Reputations (1967), he directed Naomi Tani again, as a former prostitute trying to escape her past.  Sakao found his greatest success at Nihon Cinema studio with Virgins and Pimp (1968). 

Actress Naomi Tani, who would become known as the "Queen" of S&M film had her first experience with cinematic S&M theme in director Kinya Ogawas Memoirs Of A Modern Female Doctor (also 1967). However, Cruel Map of Womens Bodies was her first film which fully explored S&M as its central theme. 

==Bibliography==

===English===
*  
*  
*  
*  
*  

===Japanese===
*  
*  

==Notes==
 

 
 
 
 
 


 
 