Never Been Kissed
 
{{Infobox film
| name = Never Been Kissed
| image = Never Been Kissed film poster.jpg
| caption = Theatrical release poster
| director = Raja Gosnell
| producer = Sandy Isaac Nancy Juvonen
| writer = Abby Kohn Marc Silverstein Jeremy Jordan Molly Shannon Garry Marshall John C. Reilly  David Newman
| cinematography = Alex Nepomniaschy
| editing = Debra Chiate
| studio = Fox 2000 Pictures Flower Films Bushwood Pictures Never Been Kissed Productions
| distributor = 20th Century Fox
| released = April 9, 1999
| runtime = 107 minutes
| country = United States
| language = English
| budget = United States dollar|$25 million   
| gross = $84,565,230 
}}
 Jeremy Jordan, Molly Shannon, Garry Marshall, John C. Reilly, and James Franco in his film debut.

== Plot ==
Josie Geller (Drew Barrymore) is an insecure copy editor for the Chicago Sun-Times who has never had a real relationship. One day, her editor-in-chief, Rigfort (Garry Marshall) assigns her to report undercover at a high school to help parents become more aware of their childrens lives.
 Jeremy Jordan), the schools most attractive, popular student. Josie loses hope, but is reassured when a kind-hearted nerd named Aldys (Leelee Sobieski) befriends her. Aldys, who loathes Guy and his gang, invites Josie to join The Denominators, a group of intelligent students. 

Josie develops a crush on her English teacher, Sam Coulson (Michael Vartan), and becomes the top student in his class. After reciting a romantic excerpt from Shakespeare to Sam, Josie has horrible flashbacks to when she read a romantic poem aloud in class to her high school crush, a popular boy named Billy Prince (Denny Kirkwood), who later asked her to their senior prom, making her dream come true. However, on the night of the prom, Billy arrives with another girl and both of them hurl eggs and insults at Josie, humiliating her and breaking her heart.

One night while out driving with Aldys, Josie encounters Guy and his gang at a local hangout called "The Court" where promiscuity and underage drinking take place. Her managing editor Augustus "Gus" Strauss (John C. Reilly) loses patience with Josie after a rival paper scoops The Court story, and orders Josie to become friends with the popular kids. He arranges for her to wear a hidden camera, and soon the whole office becomes obsessed with her story. 

Josie confides in her brother Rob (David Arquette) about her fears. Rob, who was their high schools most popular boy in his teens, urges her to let go of her old self and start anew. To help her, Rob enrolls as a student and becomes an instant hit. He then uses his influence to draw Josie into the cool crowd, much to the dismay of Aldys.
 Rosalind and Orlando from Shakespeares As You Like It. Anita, Gus and Josies other co-workers watch through the camera and are overjoyed as she is voted prom queen. As Guy dances with Aldys as an alleged act of friendship, the mean girls attempt to dump dog food over Aldys. Outraged, Josie throws her crown away and reveals her true identity. She praises Aldys for her kindness and warns the students that ones persona in high school means nothing in the real world. Sam is hurt by her lies and states he wants nothing to do with her. Also angered is Rob, who as a phony student received a second chance at baseball. Josie, ultimately making amends, secures him a coaching job. 

Josie vows to give Gus a story and writes an account of her experience. In it, she admits shes never been kissed, describes the students of South Glen South, and avows her love for Sam; the entire city is moved by it. She writes she will stand in the middle of the baseball field and wait for Sam to come and kiss her. Josie waits, but the clock runs out with no sign of Sam. On the verge of giving up...cheers, then a booming roar, as Sam emerges to give her a romantic kiss.

== Cast ==
* Drew Barrymore as Josie Geller
* David Arquette as Rob Geller
* Michael Vartan as Sam Coulson
* Leelee Sobieski as Aldys 
* Molly Shannon as Anita
* John C. Reilly as Augustus "Gus" Strauss Jeremy Jordan as Guy Perkins
* Jessica Alba as Kirsten
* Jordan Ladd as Gibby
* Marley Shelton as Kristen
* Garry Marshall as Rigfort
* James Franco as Jason
* Denny Kirkwood as Billy Prince
* Marissa Jaret Winokur as Sheila
* Maya McLaughlin as Lara
* Giuseppe Andrews as Denominator
* Octavia Spencer as Cynthia
* Branden Williams as Tommy
* Cress Williams as George
* Sean Whalen as Merkin
* Martha Hackett as Mrs. Knox
* Jenny Bicks as Miss Haskell
* Katie Lansdale as Tracy

== Soundtrack ==

* During the scene where Josie and Aldys are talking to each other on the football field, the band plays the theme song from The Simpsons.
* During a scene where Josie is remembering her bullying in high school, Cyndi Laupers "She Bop" is played.
* "(I Just) Died In Your Arms" by Cutting Crew plays when Josie first sees Guy entering the classroom.
* American ska band Spring Heeled Jack U.S.A. submitted a song named "Josie" for the films soundtrack. The band had previously released a single titled "Jolene" which was about their tour van, but when given the opportunity to submit a song for the soundtrack, they simply replaced the name Jolene with Josie to make it relevant to Barrymores character in the film. It was later rejected.
* The morning after Josies experience with marijuana, "Me, Myself and I" by De La Soul is heard playing. Like a Like a Prayer, can be heard in the background.
* The single "Seventeen" by Jimmy Eat World is featured in the films soundtrack. The version used in the film differs from the version released commercially.  Hole appears in the film.
* The Latin funk band Ozomatli makes a cameo.
* During the climax, in the scene when Josie receives her first kiss from Sam on the baseball field, the song "Dont Worry Baby" by The Beach Boys is played.
* While Josie and Sam are dancing, the song "Please, Please, Please, Let Me Get What I Want" from the 1984 album Hatful of Hollow by The Smiths can be heard in the background.
* The song "Erase/Rewind" by The Cardigans is played,in the background, towards the end of the movie, at the prom night when the prom king and queen dance.
* The song Watching the Wheels by John Lennon appears in the movie.
* Kottonmouth Kings "Suburban Life" plays when Josie pulls up to the school and her car backfires. BTK is featured in the movie.

== Filming locations ==

* South Glen High School was filmed at John Burroughs Middle School located in the Hancock Park area of Los Angeles at 600 South McCadden Place 
* Josies childhood home is located at 368 North Ridgewood Place in the Hancock Park area of Los Angeles. 
* Nanas coffee shop where Josie and Aldys have lunch is the Monrovia Coffee Company located at 425 South Myrtle Avenue in Monrovia, CA. 
* The Tiki Post where Rob worked is now a Cold Stone Creamery located at 408 South Myrtle Avenue in Monrovia, Ca. 
* The exterior scenes for the prom were filmed at 4526 Wilshire Boulevard in Los Angeles, CA. The interiors were filmed at the Ebell located at 743 South Lucerne Boulevard in Los Angeles, CA. 
* Josies office and job scenes were filmed in Chicago and readers of the Chicago Sun-Times were shot on the Chicago Avenue stop on the CTA Brown Line.

== Critical reception ==

Critics gave mixed reviews to the film, with a "Rotten" score of 57% on review aggregation website Rotten Tomatoes, and its consensus reading: "Unoriginal and unremarkable high school satire adds little to the genre."  

== References ==
  

== External links ==

*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 