Fine Feathers (1937 film)
{{Infobox film
| name =  Fine Feathers 
| image =
| image_size =
| caption =
| director = Leslie S. Hiscott  Herbert Smith 
| writer =  Michael Barringer  
| narrator = Jack Hobbs   Renee Houston   Francis L. Sullivan 
| music = John Blore Borelli 
| cinematography = George Stretton
| editing =  British Lion
| distributor = British Lion
| released = 1937
| runtime = 68 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jack Hobbs, Renee Houston and Francis L. Sullivan. Its plot concerns a woman out on a picnic who becomes lost and stumbles across a gang who persuade her to impersonate the mistress of the Crown Prince of Boravia.  It was made at Beaconsfield Studios. 

==Cast==
* Stella Arbenina as Elizabeth  Jack Hobbs as Felix 
* Renee Houston as Teenie McPherson 
* Marcelle Rogez as Mme. Barescon 
* Francis L. Sullivan as Hugo Steinway  
* Henry Victor as Gibbons 
* Robb Wilton as Tim McPherson  

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
*  

 
 
 
 
 
 
 
 

 
 