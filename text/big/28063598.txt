Apoorvaragam
 
{{Infobox film
| name           = Apoorvaragam
| image          = Apoorva Ragam.jpg
| alt            =  
| caption        = 
| director       = Sibi Malayil
| producer       = Siyad Koker
| writer         = G. S. Anand Najeem Koya Nishan Asif Ali Nithya Menon Vinay Forrt Abhishek Hima Vidyasagar
| cinematography = Ajayan Vincent
| editing        = Bijith Bala
| studio         = Sree Gokulam Films
| distributor    = 
| released       =  
| runtime        = 152 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} 
Apoorvaragam ( ) is a 2010 Malayalam thriller film written by G. S. Anand and Najeem Koya, and directed by Sibi Malayil. The film stars Nishan (actor)|Nishan, Asif Ali, Nithya Menon, Vinay Forrt, Abhilash and Hima. It released on 16 July 2010 across 42 theaters in Kerala. The film was a sleeper hit at the box office.It is later dubbed into Telugu as 50% Love due to Nithya Menons fame in Telugu.

== Plot == Nishan ), Nancy (Nithya Menen) and Tommy (Asif Ali). Roopesh has always been in love with Nancy, but has never had the nerve to express it. For Nancy, love is something that is beyond words. Tommy is the perfect Cupid, who would make things happen for these two. Slowly Roopesh and Nancy fall in love. Behind their back Tommy calls Nancys father and informs him that his daughter is dating someone from the college. Her daddy trusts her and so does not take this seriously at first. But later when he grows more suspicious, he decides to marry her off to someone he knows personally.

Nancy and Roopesh register their marriage. Tommy makes a call again to her father and informs him that his daughter has got married. Her father after verifying the correctness of this information, comes and meet Roopesh. He tells Roopesh that he is ready to give any amount to him to withdraw from the relation. Roopesh tells him that he is not in love with Nancy and is only doing this for money. Roopesh demands Rs. 1 crore to back out from the relation and Nancys father gives it to save the child.

Roopesh, Tommy and their third companion Narayanan is later seen trying to do the same with another rich mans daughter. While the trap was being set, Roopesh gets in touch with Nancy again and tells her the truth. He steals their ransom money from their hideout and gives it back to her dad in the hope to reunite with Nancy. But Nancy revels that she was only pretending to be in love with him because she wanted the money back and also that she wanted to break his heart in return. She gets engaged to the guy chosen by her father in front of Roopesh.

Tommy and Narayanan follows them back to this place and abducts Roopesh and Nancy. In the fight that follows Roopesh grabs the gun and shoots both Tommy and Narayanan. He then calls Nancys father to come and pick her up. When she was about to leave, Roopesh bids her adieu and shoots himself.Nancy cries out loud suggesting that she was still in love with Roopesh

== Cast == Nishan as Roopesh
* Asif Ali as Tommy
* Nithya Menen as Nancy
* Vinay Forrt as Narayanan
* Abhilash as Firoz
* Hima Sankar
* Santhosh Jogi as Sethu
* Suman  as pilipose melipra
* Jagathy Sreekumar as adv.charly
* Sruthi Menon

==Reception==
The film had a cold reception and neared failure at the box office but word of mouth dragged the film on to run which completed 60 days all over Kerala with average response but there was 35 % removals in its 50th day. Even better it is regarded as one of the best plot twists in  Malayalam movie industry .It also had an untold ever love story .

==Soundtrack==
{{Infobox album 
| Name       = Apoorva Ragam
| Type       = soundtrack Vidyasagar
| Cover      = 
| Released   = 2010
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      =  Vidyasagar
}} Vidyasagar with lyrics penned by Santhosh Varma. The campaign song and re-recording of the songs were done by Bijibal.
 
{{Track listing
| extra_column = Performer(s)
| title1 = Noolilla Pattangal | extra1 = Devanand, Renjith, Benny Dayal, Naveen Anand, Cicily, Suchithra
| title2 = Maanathe | extra2 = Karthik (singer)|Karthik, Renjith, Yasir
| title3 = Aathira | extra3 = Sowmya
}}

== External links ==
*  
*  

 
 
 
 