Kaveri (1986 film)
{{Infobox film
| name           = Kaveri
| image          =
| caption        =
| director       = Rajeevnath
| producer       =
| writer         = Rajeevnath Nedumudi Venu (dialogues)
| screenplay     = Nedumudi Venu Rajeevnath
| starring       = Mohanlal Mammootty Adoor Bhasi Nedumudi Venu
| music          = V. Dakshinamoorthy Ilayaraja
| cinematography = Madhu Ambatt
| editing        = Ravi
| studio         = Dakshineswari Films
| distributor    = Dakshineswari Films
| released       =  
| country        = India Malayalam
| Rs 24 Lakhs
| runtime        = 130 min.
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by Rajeevnath. The film stars Mohanlal, Mammootty, Adoor Bhasi and Nedumudi Venu in lead roles. The film had musical score by V. Dakshinamoorthy and Ilayaraja.   

==Plot==
Kaveri is a family film of love and sacrifice.

==Cast==
*Mohanlal
*Mammootty
*Adoor Bhasi
*Nedumudi Venu Sithara

==Soundtrack==
The music was composed by V. Dakshinamoorthy and Ilayaraja and lyrics was written by Kavalam Narayana Panicker. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Heramba || V. Dakshinamoorthy, Chorus, Eeswari Panicker || Kavalam Narayana Panicker || 
|-
| 2 || Janmangal || M Balamuralikrishna, Eeswari Panicker || Kavalam Narayana Panicker || 
|-
| 3 || Neelalohitha || M Balamuralikrishna || Kavalam Narayana Panicker || 
|-
| 4 || Oru veena than || M Balamuralikrishna, Eeswari Panicker || Kavalam Narayana Panicker || 
|-
| 5 || Swarnasandhya || M Balamuralikrishna || Kavalam Narayana Panicker || 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 