Ningen (1962 film)
{{Infobox film
| name           = Ningen 人間
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Kaneto Shindo
| producer       = 
| writer         = Kaneto Shindo
| screenplay     = 
| story          = 
| based on       =  
| starring       = Taiji Tonoyama Nobuko Otowa Kei Sato
| music          = Hikaru Hayashi
| cinematography = 
| editing        = 
| studio         = Kindai Eiga Kyokai
| distributor    = 
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 1962 Japanese drama film directed by Kaneto Shindo, and starring Taiji Tonoyama, Kei Sato, and Nobuko Otowa, produced by Shindos company Kindai Eiga Kyokai.  

==Plot==
A ship loses all means of navigation in a storm. The crew become increasingly desperate as food and water run out. The captain, Kamegoro (Taiji Tonoyama), prays to the sailors god Kompira to rescue them and rations their food and water. His grandson, Sankichi (Kei Yamamoto), follows his grandfather, but the other two crew members, Hachizo (Kei Sato) and Gorosuke (Nobuko Otowa) rebel and insist on eating their rations of food all at once.

In a vision, Kamegoro sees Kompira, who promises to deliver rain. Then rain comes and the threat of dying of thirst is gone, but there is no food. Each member of the crew revisits pleasant times, which are recreated as flashbacks in the film. Kamegoro also has less pleasant memories of his war service, where he saw another soldier turn to cannibalism. After weeks of hunger, Hachizo and Gorosuke think of killing and eating Sankichi. They trick him with the promise of food and then kill him with an axe. Kamegoro goes to find him. Hachizo tries to fight but they are both weakened by hunger. Gorosuke repents and begs for forgiveness. They bury Sankichi at sea.

Kamegoro lies to the two remaining crew members that he has had another vision of Kompira, who has blown a ship from San Francisco off course with a typhoon so that it will find them. A ship which genuinely has been blown off course exactly as Kamegoro says then appears. They are rescued. While recovering on the deck of the ship, Gorosuke goes into a mad dance, falls into a cargo hold, and dies. Then Hachizo also commits suicide, leaving only Kamegoro alive.

==Cast==
*Taiji Tonoyama as Kamegoro
*Nobuko Otowa as Gorozuke
*Kei Sato as Hachizo
*Kei Yamamoto as Sankichi
*Hideo Kanze as Kompira
*Kentaro Kaji as the bosun

==Production==

The story is based on a work Kaijinmaru (海神丸) by Yaeko Nogami,  which itself is based on a true story. 

==Reception==
The film won an award at the National Arts Festival (文部省芸術祭文部大臣賞). 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 