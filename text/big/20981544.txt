My Secret Cache
{{Infobox film
| name           = My Secret Cache
| image_size     =
| image	         = My Secret Cache FilmPoster.jpeg
| caption        =
| director       = Shinobu Yaguchi
| producer       =
| writer         = Takuji Suzuki Shinobu Yaguchi
| narrator       =
| starring       = Naomi Nishida Gô Rijû
| music          =
| cinematography = Masahiro Kishimoto
| editing        = Miho Yoneda
| distributor    = Toho Company (Japan) Geneon Entertainment (USA)
| released       =  
| runtime        = 83 min
| country        = Japan
| language       = Japanese
| gross          =
}}
 Japanese film. Its original Japanese title is  . It was directed by Shinobu Yaguchi.

== Plot ==
The film begins with a prologue that quickly sketches the backstory of the protagonist: since childhood, Sakiko Suzuki was obsessed with cash. She scared off potential friends by constantly talking about her savings account, and potential suitors by demanding that they give her up front the money they would have spent taking her out on a date. After college, her family suggests she get a job as a bank teller. Shes promptly hired, but soon grows disillusioned by the job, because shes counting other peoples cash. Then, bank robbers kidnap her and stuff her in a trunk with a bright yellow suitcase filled with stolen money. The thieves take a wrong turn in the woods and crash their car. Sakiko escapes with the briefcase but falls into a river and winds up in an underground pool, where she tries to kill herself by drowning after letting go of the suitcase. Instead, she washes up downstream and is given medical leave by the bank.

The rest of the film traces Sakikos quest to retrieve the money. One Sunday, she convinces her family to take her to the woods, but they give up as soon as it rains. Undeterred, Sakiko goes on without them but suffers an accident and winds up in the hospital again. So she resolves to prepare more carefully. Moving out of her familys house, she returns to college to study geology, and also takes lessons in swimming, scuba diving, mountain climbing and driving, and buys lots of surveying equipment. Along the way, she wins swimming and mountain climbing competitions. But shes uninterested in pursuing fame in either as a swimmer or mountain climber, wanting only to earn money to fund her quest. After her apartments floor collapses under the weight of too much equipment and another stint at the hospital, Sakiko escapes, steals from her family and frames a research assistant at the college for stealing a bag of money from the bank; she steals his truck and goes to the woods and retrieves the suitcase. She formally apologizes to her family for stealing from them. On the radio, Sakiko hears about hidden treasures in the Bermuda triangle and decides to get a sailing license. The movie ends with Sakiko hiding the yellow briefcase in the woods.

==Reception==
My Secret Cache does not yet have a rating from Rotten Tomatoes. 

In the United States, the film has been released to DVD (region 1) by Geneon Entertainment. The average Netflix custormer rates the film 2.9 stars out of five.

==Awards== Japanese Academy Awards for Best Newcomer, and at the Japanese Professional Movie Awards for Best Actress. 

==See also==
*Swing Girls (2004)

==Further reading==
*  
*    

==Notes==
 

== External links ==
*  at Rotten Tomatoes
* 
*  

 

 
 
 
 