Shirin Farhad Ki Toh Nikal Padi
 
 
{{Infobox film
| name           = Shirin Farhad Ki Toh Nikal Padi
| image          = SHIRIN_FARHAD_KI_TOH_NIKAL_PADI_poster.jpg
| caption        =
| director       = Bela Bhansali Sehgal
| producer       = Sanjay Leela Bhansali Sunil A Lulla
| writer         = Sanjay Leela Bhansali
| starring       = Farah Khan Boman Irani Kavin Dave Shammi Kurush Deboo Daisy Irani
| music          = Jeet Ganguly
| cinematography = Mahesh Aney
| editing        = Bela Bhansali Sehgal Rajesh Pandey
| distributor    = Bhansali Productions Eros International
| released       =  
| runtime        =  122 minutes approx.
| country        = India
| language       = Hindi
| budget	 =
| gross          =   (1 week domestic) 
}}
 Daisy Irani.

==Plot==
Farhad Pastakia (Boman Irani) has a dream job as a bra-and-panty salesman. Except that he is a 45-year old Parsi bachelor still living with his overbearing mother and grandmother. But it’s not like he’s stopped trying. Desperate to get him married, Farhad’s mother drags him to see women and even to embarrassing Parsi matrimony services.

In the midst of all the daily humdrum, Farhad meets the woman of his dreams: Shirin Fugawala (Farah Khan), who drops by his store. Shirin, who works at the Parsi Trust, hits it off with Farhad from the start. Everything seems to be perfect and Farhad gets ready to introduce Shirin to his mother. But mummy becomes the villain in their story when she discovers that Shirin is the devious Parsi Trust Secretary who got the illegal water tank in their home demolished. The water tank being the aakhri nishaani of Farhad’s late father doesn’t help matters.

The ups and downs in Shirin-Farhad’s relationship and how the two try to make it work is the rest of the journey.

The proposal scene in the movie where Shrin accidentally swallows a diamond ring was plagiarized from an episode of Two and a Half Men where Chelsie swallows a diamond ring that Charlie Harper had put in her champagne glass.

The plot of the movie is similar to an old Hollywood classic " Martys"

==Cast==
* Farah Khan as Shirin Fuggawala
* Boman Irani as Farhad Pastakia
* Kavin Dave
* Shammi
* Kurush Deboo Daisy Irani as Nargis Pastakia
* Dinyar Contractor
* Nauheed Cyrusi as Anahita
* Mahabanoo Modi Kotwal
*  Beroze Farhads aunt

==Soundtrack==
{{Infobox album
|Name=Shirin Farhad Ki Toh Nikal Padi Longtype to=Shirin Farhad Ki Toh Nikal Padi
|Type=Soundtrack
|Artist=Jeet Ganguly
|Cover=
|Border=Yes
|Alt=
|Caption=
|Released=July 2012
|Recorded=2012 Feature film soundtrack
|Length=
|Label=T-Series
|Producer=Sanjay Leela Bhansali
}}
* "Khatti Meethi" by Shreya Ghosal
* "Ishq Mein Tere Bina" by KK (singer)|KK, Shreya Ghosal KK
* "Kukuduku" by Mohit Chauhan
* "Title Song" by Neeraj Shridhar
* "Ramba Mein Samba" by Usha Uthup

==Reception==
  at the unveilig of first look of the film.]]
Shirin Farhad Ki Toh Nikal Padi released in 24 August 2012. 

===Critical reception=== Rediff gave DNA India DNA India gave the movie 3/5, stating that "Shirin Farhad Ki Toh Nikal Padi is too sweet and too simple, but makes for a fun watch."     Madhureeta Mukherjee of The Times of India gave the movie 3/5, concluding that "Shirin Farhad Ki Toh Nikal Padi is no epic love tale, but its worth a watch for those who like their cinema as buttered as their bun maska."     Saibal Chatterjee of NDTV gave the movie 2.5/5, commenting that "Shirin Farhad Ki To Nikal Padi is warm-hearted and generally watchable without being exhilarating."     Taran Adarsh of Bollywood Hungama gave the movie 3.5/5, saying that "SHIRIN FARHAD KI TOH NIKAL PADI is a simple, unfussy and heartfelt movie that hits the right notes." Salman Khan praised the movie and told at least once it is watchable.    

===Box office===
Shirin Farhad Ki Toh Nikal Padi had a good opening at the box office at 20%,   collecting   in its first day.  The movie collected   in its first weekend, collecting   in its second day and   in its third day.  The film collected    nett in week one. {{cite web|title=Shirin Farhad Ki Toh Nikal Padi Week One Territorial Breakdown.The collection seems to rise in next week.
 |url=http://boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=4858&nCat=|publisher=boxofficeindia|accessdate=3 August 2012}} 

==References==
 

==External links==
* 
*  

 

 
 
 