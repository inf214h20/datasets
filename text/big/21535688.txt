Eva (1958 film)
{{Infobox film
| name           = Eva
| image          = 
| caption        = 
| director       = Rolf Thiele
| producer       = Karl Ehrlich
| writer         = Hans Jacoby Fritz Rotter
| starring       = Romy Schneider
| music          = 
| cinematography = Klaus von Rautenfeld
| editing        = Henny Brünsch
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Austria
| language       = German
| budget         = 
}}

Eva ( ) is a 1958 Austrian comedy film directed by Rolf Thiele. It was entered into the 1959 Cannes Film Festival.   

==Cast==
* Romy Schneider as Nicole
* Carlos Thompson as Irving
* Magda Schneider as Dassou
* Gertraud Jesserer as Brigitte
* Alfred Costas as Thomas
* Richard Eybner (as Richard Eÿbner)
* Rudolf Forster
* Fritz Heller
* Benno Hoffmann
* Helmut Lohner
* Erni Mangold
* Josef Meinrad
* Dorothea Neff
* Guido Wieland

==References==
 

==External links==
* 

 
 
 
 
 


 
 