Melody in May
{{Infobox film
| name           = Melody in May
| image          = 
| image_size     = 
| caption        = 
| director       = Ben Holmes
| producer       = Bert Gilroy (associate producer) Lee S. Marcus (producer)
| writer         = Stanley Rauh (story)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Nicholas Musuraca
| editing        = Edward Mann
| studio         = 
| distributor    = 
| released       = 1936
| runtime        = 20 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Melody in May is a 1936 American short film directed by Ben Holmes.

== Plot summary ==
 

== Cast ==
*Ruth Etting as Herself
*Frank Coghlan Jr. as Tommy Bradshaw
*Margaret Armstrong as Ma Bradshaw
*Joan Sheldon as Mary Callahan
*Kenneth Howell as Chuck
*Robert Meredith as Boy

== Soundtrack ==
*Ruth Etting - "St. Louis Blues" (By Handy) It Had to Be You" (By Isham Jones and Gus Kahn (lyrics))

== External links ==
* 
* 

 
 
 
 
 
 
 


 