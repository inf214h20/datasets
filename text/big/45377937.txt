Dharmatma (1935 film)
{{Infobox film
| name           = Dharmatma
| image          = Dharmatma_1935.jpg 
| image_size     = 
| caption        = Screen shot of Dharmatma
| director       = V. Shantaram
| producer       = Prabhat Film Company
| writer         = K. Narayan Kale
| narrator       =  Chandra Mohan Ratnaprabha Vasanti
| music          = Master Krishnarao
| cinematography = Keshavrao Dhaiber 
| editing        = 
| distributor    =
| studio         = Prabhat Film Company
| released       = 1935
| runtime        = 144 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1935 Hindi Marathi and Chandra Mohan as the villain,    with Master Chhotu,  K. Narayan Kale, Ratnaprabha, Vasanti and Hari Pandit.   

The film is about the "legendary" Marathi religious poet and scholar Sant Eknath (1533–99),    author of a variation of the Bhagavata Purana called Eknathi Bhagvata, numerous abhangs and the bharuda form of solo performances.   
The story revolves around his teachings regarding social injustices concerning untouchability, equality and humanity.   

==Plot== Chandra Mohan) who opposes such practices. Things come to a head when at a prayer meeting, Eknath feeds the untouchables first, before the Brahmins, as would be the normal custom. Eknath does not differentiate between castes and eats at their house too. This enrages the Mahant and he has Eknath ex-communicated. Eknaths son Hari Pandit (Kale) has joined the people and the Mahant who oppose his fathers practices. Eknath finally arrives at Kashi and defends his behaviour by reciting verses of his poems to Pradayananda Shastri.

==Cast==
* Bal Gandharva as Sant Eknath Chandra Mohan as Mahant
* Ratnaprabha
* Keshav Narayan Kale as Hari Pandit
* Master Chhotu as Shrikhandya
* Vasanti
* Kelkar
* Vasant Desai
* Budasaheb
* Rajni

==Production== Sant Tukaram Achhut Kanya (1936) which had a "contemporary setting".

==Soundtrack==
The music direction was by Master Krishnarao Phulambrikar, a classical musician who combined with another classicist Bal Gandharva in the film to create "a milestone in Indian cinema".    The  lyrics were by Narottam Vyas. There were sixteen songs in the film and the singers were Bal Gandharva, Vasant Desai, Ratnaprabha, Vasanti and Master Chhotu. 

===Songs===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1
| Aaj Bhaag Jaag Gaye Logo Hamare
| Vasant Desai
|-
| 2
| Vharan Kamal Mein Prabhu Ke Maath Namaun Aaj
| Bal Gandharva
|-
| 3
| Hai Boli Amrut Si Ma Ke Naam Ke Jaadu Se Sab
| Ratnaprabha
|-
| 4
| Dhanya Dhanya Naaro Jaati
| Bal Gandharva
|-
| 5
| Kaana Re Aa Zara Misri  Maakhan Khaaye
| Vasanti
|-
| 6
| Jan Pyare Sab Tumhe Ek Si Nazar Hai Din Nath Ki
| Ratnaprabha
|-
| 7
| Kaise Bayan Karun Prabho Kya Kya Badhai Aapki
| Ratnaprabha
|-
| 8
| Prabhu Sahare Ek Tumhi Ho Laaj Tumhare Haath Hai
| Bal Gandharva
|-
| 9
| Prem Brij Bihari Prem Hai Pujari
| Bal Gandharva
|-
| 10
| Rote Bure Na Hote
| Master Chhotu
|-
| 11
| Narayan Nar Ke Rakhwar Kaun Bigaad Karega Tera
| Vasanti
|-
| 12
| Sab Kehte Jisko Neecha
| Bal Gandharva
|-
| 13
| Tan Ko Saaf Man Ko Saaf Kar Ke Hari Pyare
| Bal Gandharva
|-
| 14
| Yadi Chaahi Bharat Ban Jaave Swarg Sukhon Ka
| Bal Gandharva
|-
| 15
| Yeh Jag Ki Phulwari Prabhuji Nath
| Bal Gandharva
|-
| 16
| Yeh Samta Barat Hamara Hai
| Ratnaprabha
|}
  
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 