7 Days (film)
{{Infobox film
| name           = 7 Days
| image          = Les 7 Jours du Talion.jpg
| caption        = Official teaser poster
| director       = Daniel Grou
| producer       = Nicole Robert
| writer         = Patrick Senécal
| starring       = Rémy Girard Claude Legault Fanny Mallette
| cinematography = Bernard Couture
| editing        = Valérie Héroux
| studio         = Go Films
| distributor    = Alliance Vivafilm Seville Pictures
| released       =   
| runtime        = 111 minutes
| country        = Canada
| language       = French
}}
7 Days ( ) is a 2010 Canadian thriller film directed by Daniel Grou  and starring Claude Legault.  The screenplay was written by Patrick Senécal and based on his novel Les sept jours du talion. 

==Plot==
The ordinary life of surgeon Bruno Hamel (Claude Legault) is destroyed when his daughter Jasmine (Rose-Marie Coallier) is raped and murdered in a park. Upon learning that the police apprehended the prime suspect, laborer Anthony Lemaire (Martin Dubreuil), Hamel plans to take revenge. He abducts Lemaire while he is being brought to his trial by drugging the officer driving the transport vehicle and brings him to a secluded cabin. Using a remote-controlled computer to conceal his location, Hamel calls the police to inform them that he plans to murder Lemaire in seven days, the seventh day being Jasmines birthday. After killing him, Hamel will give himself up to the police.

Police detective Mercure (Rémy Girard) leads the investigation to discover Hamels whereabouts. Mercure himself suffered a personal tragedy when his wife was killed during a grocery store robbery. Though Mercure acknowledges that the imprisonment of his wifes killer has not made his life more bearable, he becomes determined to stop Hamel before he commits murder. 

Over the course of the seven days, Hamel brutally tortures Lemaire. Initially frightened and in incredible pain, Lemaire starts to accept his fate and mocks Hamel for not enjoying himself as he inflicts painful injuries on him. Lemaire eventually admits to raping and murdering Jasmine, along with three other girls. Hamel contacts a news station to have the families of Lemaires victims informed about his captives confession. When the mother of one of Lemaires victims disapproves of his actions, Hamel kidnaps her and forces her to see Lemaire.

By the seventh day, the police locate Hamels cabin. Hamel gives himself up and lets Lemaire live. As the police lead him away, a reporter asks him if he still believes vengeance is right. Hamel responds with a "No." However, when asked if he regrets what he has done, he gives the same answer.

==Cast==
* Claude Legault as Bruno Hamel
* Rémy Girard as Hervé Mercure
* Martin Dubreuil as Anthony Lemaire
* Fanny Mallette as Sylvie Hamel
* Rose-Marie Coallier as Jasmine Hamel
* Alexandre Goyette as  Michel Boisevert 
* Dominique Quesnel as Maryse Pleau
* Pascale Delhaes as Diane Masson
* Pascal Contamine as Gaétan Morin
* Daniel Desputeau as Gilles, Médecin

==Release and reception==
The film premiered on 22 January 2010 at Sundance Film Festival,  as World Premiere. 

Upon release, critics reacted favorably towards the film. On Rotten Tomatoes, the film has a tomatometer rating of 83%, based on 12 reviews.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 