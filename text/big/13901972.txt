Falling for Grace
{{Infobox film
| name           = Falling for Grace
| image          = Falling_for_Grace_poster.jpg
| caption        = Falling for Grace theatrical poster
| director       = Fay Ann Lee
| producer       = Fay Ann Lee Michelle Botticelli Susan Batson Carl Rumbaugh Stephanie Wang
| writer         = Fay Ann Lee Karen Rousso
| starring       = Fay Ann Lee Gale Harold Margaret Cho Stephanie March Lewis Black Roger Rees Ken Leung Christine Baranski
| music          = Andrew Hollander
| cinematography = Luke Geissbuhler Toshiaki Ozawa
| editing        = Michelle Botticelli
| distributor    = Canal Street Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English, Cantonese
| budget         = 
}}
Falling for Grace is a 2006 romantic comedy film directed by Asian-American Fay Ann Lee, who also co-wrote the film with Karen Rousso, and stars alongside Gale Harold.  It debuted at the 2006 Tribeca Film Festival (as East Broadway).  New York (magazine)|New York magazine called the film one of the two best entries in the "New York, New York" competition that year. 

==Plot synopsis==
Grace Tang (Fay Ann Lee) is an ambitious Wall Street investment banker raised in New York’s Chinatown, Manhattan|Chinatown. Though she has achieved financial success and stability as a mergers and acquisitions associate, Grace still yearns for social acceptance among the Upper East Side elite. When she is finally invited to her first high-end soiree, a Junior Committee meet-and-greet for a prestigious opera company, she is accidentally mistaken for an heiress from Hong Kong, also named Grace Tang. Her efforts to correct the mistake lose some of their forcefulness when she is subsequently introduced to handsome Andrew James Barrington, Jr. (Gale Harold), who is dating committee-member Kay Douglas (Stephanie March).
 Chinese restaurant, the two begin to see more of each other, and Grace’s personal, professional, and family interests become increasingly entangled and conflicting. Andrew, the son of a prominent attorney (Roger Rees), works in the New York State Attorney Generals office in Manhattan, and has been passionately pursuing a case against a network of Chinatown sweatshops — in one of which Graces mother works. Grace, unable to extricate herself gracefully from what she saw initially as an innocuous white lie, finds herself pretending that her parents are an old couple whom she visits as a volunteer. Meanwhile, Andrew Sr. is helping to shepherd a fashion-company buyout at Graces bank, with a company that exploits sweatshop works. Grace finds herself secretly caught in the middle

When her brother Ming (Ken Leung) inadvertently reveals the truth to Andrew, Andrew leaves the budding romance, of which Kay is unaware. With Graces help, however, Andrew gets documents that prove the fashion companys sweatshop connection, which causes the companys and his fathers downfalls. Andrew leaves Kay to move to Hong Kong, where Grace has a new position with her company.

==Cast==
*Fay Ann Lee as Grace Tang
*Gale Harold as Andrew Barrington, Jr.
*Margaret Cho as Janie Wong
*Christine Baranski as Bree Barrington
*Roger Rees as Andrew Barrington, Sr.
*Ken Leung as Ming Tang
*Clem Cheung as Ba
*Elizabeth Sung as Ma
*Ato Essandoh as Jamal Taylor
*Stephanie March as Kay Douglas
*Lewis Black as Rob York

==Competitions==
The original screenplay, East Broadway, by Fay Ann Lee, was a quarterfinalist for the 2003 Nicholl Fellowships  (given by the Academy of Motion Picture Arts and Sciences), a semi-finalist at the Chesterfield Writer’s Film Project (run by Paramount Pictures), and a finalist at the Asian American International Film Festival’s Screenwriting Competition.

==Production==

The romantic comedy was supposed to be billed as the feature film directorial debut of acclaimed actor and Tony Award winning B.D. Wong.  Wong left the project at some point very, very late in production, citing “artistic differences” that grew between Wong and the producers. Fischer, Martha. "Tribeca Review: East Broadway". http://blog.moviefone.com Posted Apr 29th 2006, 5:32PM.  He was replaced as director by Fay Ann Lee, the film’s writer and star. Subsequently, Wong request that his name be completely removed from the movie’s credits, despite the fact that he plays a major supporting role. 

==Festivals==
The film debuted at the 2006  ,  and the Los Angeles Asian Pacific Film Festival, where it was nominated for the Grand Jury Award; in 2008, the Phoenix Film Festival;  and in 2009, the Reel Film Festival for Women in Los Angeles, where it won Best Dramatic Feature,  and the  Delray Beach Film Festival, where it won the Best Feature Film Audience Award. 

==Distribution== Latino American (for a star like Jennifer Lopez).   Lee opted not to make the change, and decided instead to get it produced herself, raising the production budget  over the course of 4 years.
 world premiere at the 2006 Tribeca Film Festival. It was not picked up by a film distributor|distributor. Filmmaker Lee said distributors told her a mainstream audience would not pay to see an Asian American protagonist in an American romantic comedy. 

Lee release the film herself in cities outside New York and Los Angeles She began by four-walling at the Sundance Kabuki theater in San Francisco. There, Falling for Grace averaged over $5,000 per screen for three weeks.  Michael Coppola, owner of the Fleur Cinema in Des Moines, Iowa, saw the film with his wife at the Camelview 5, Harkins Theatres in Scottsdale, Arizona|Scottsdale, Arizona. "The minute I left the movie," said Coppola, "I looked at my wife and said, this is the movie I have to get."   Falling for Grace opened at the Fleur Cinema in August 2008. 
 Grass Valley, Grass Valley, Palm Springs, Des Moines; Lakeside and Scottsdale, Arizona; and in Vancouver, British Columbia.

The film was made available on Netflix in 2011  

===Austrian previews===
In 2007 a tourist from Austria saw Falling for Grace in San Francisco and she lobbied successfully for the Austrian movie theater chain Cineplexx (part of Constantin Film) and others to tour the movie as a sneak preview in nine theaters in Austria and one in Germany. 

==College speaking tour==
Filmmaker Fay Ann Lee has addressed students across the country and in China regarding her experience making the film.   
 Johns Hopkins, UC Berkeley, James Madison, UMass - Amherst, {{Cite web | title = Amherst Events Listserv, APA Events Calendar | url=https://listserv.amherst.edu/scripts/wa.exe?A3=ind0704&L=ASA-L&E=quoted-printable&P=11576&B=--Apple-Mail-9-294119795&T=text%2Fhtml;%20charset=ISO-8859-1  
 | accessdate = December 17, 2008}}  Arizona State University,  Harvard, {{Cite web | title = Harvard College Womens Center Events Calendar | url= http://www.hcwc.fas.harvard.edu/events.html the Wharton School of Business (Lees alma mater), and Tsinghua University in Beijing, China. 

After Lee screened the film and spoke at Harvard University, she was invited to act as an official Harvard Guest Speaker at the 2010 Young Leaders Conference in Beijing. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
   
 
 