Deadwood '76
{{Infobox film
| name           = Deadwood 76
| image          =
| image_size     =
| caption        = James Landis
| producer       = Arch Hall Sr. (producer) James Landis (writer)
| narrator       =
| starring       = See below
| music          = Michael Terr
| cinematography = Lewis Guinn Vilmos Zsigmond
| editing        = Anthony M. Lanza
| distributor    =
| released       = 30 June 1965
| runtime        = 97 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} James Landis.

==Plot summary==
A drifter, Billy May (Arch Hall Jr.) arrives in Deadwood . A child misidentifies him as Billy the Kid and the town turns against him. He becomes a fugitive chased by Wild Bill Hickok.  He runs into his long lost father who was a Civil War Veteran for the Confederacy.  His father has made friends with a tribe of Indians and admires the leadership of the elders in the tribe.  He plans on joining forces and tries to get Billy to join but Billy is not interested.  He is interested in finding gold and living in peace however that is not what is in store for poor Billy.

==Cast==
*Arch Hall Jr. as Billy May
*Jack Lester as Tennessee Thompson
*La Donna Cottier as Little Bird
*Arch Hall Sr. as Boone May
*Liz Renay as Poker Kate
*Robert Dix as Wild Bill Hickok
*Richard Cowl as Preacher Smith
*Jonny Bryant as Hubert Steadman
*David Reed as Fancy Poggin
*Gordon Schwenk as Spotted Snake
*Ray Zachary as Spec Greer
*Barbara Moore as Montana
*Hal Bizzy as Curt Aiken
*Read Morgan as Ben Hayes
*Rex Marlow as Sam Bass
*Joan Howard as Mrs. Steadman
*John Bud Cardos as Hawk Russell
*Ray Vegas as Indian
*Jack Little as Bear Creek
*Bobby Means as Doctor
*Willard W. Willingham as Deputy Harding

==Soundtrack==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 