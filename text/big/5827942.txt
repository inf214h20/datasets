Blackwoods (film)
 
{{Infobox film
 | name = Blackwoods
 | image = Blackwoods VideoCover.jpeg
 | director = Uwe Boll
 | producer = Shawn Williamson
 | writer = Uwe Boll  Robert Dean Klein
 | starring = Patrick Muldoon  Clint Howard  Keegan Connor Tracy
 | music = Reinhard Besser
 | cinematography = Mathias Neumann
 | editing = David M. Richardson
 | distributor = Velocity Home Entertainment
 | released =  
 | runtime = 90 minutes
 | country = Canada  Germany
 | language = English
 | budget = $3 million
 | gross = $1,500  (US only) 
}}

Blackwoods is a 2001 psychological thriller film, directed by Uwe Boll, making it his sixth feature length film and his second film in English, and starring Patrick Muldoon and Clint Howard.  It is set in the titular Blackwoods.

==Plot== vacation to Blackwoods, only to discover a motel run by a motel clerk, Greg (Clint Howard), a deranged family, and a horrific secret.

Matt is haunted by the death of a girl from a car accident he caused years ago. Matt was drunk and as he reached for the car radio, he struck the girl as she crossed the road. The guilt that he feels has altered his sense of reality, making Matts life a mystery full of shadows and phantoms. Now, years later Matt goes away for weekend with his new girlfriend Dawn. After a wild session of lovemaking, Dawn goes for a walk. While she is away a strange man with an ax comes into the motel room and attacks Matt. After that incident Matt goes into the woods, looking for Dawn. There he encounters Dawns family who tie him down and put him on trial for the murder of the girl years before. They find him guilty and he is sent back into the forest to be hunted down by the family. The deeper Matt runs into the forest the farther his mind is lost to the Blackwoods. 

==Cast==
* Patrick Muldoon as Matt Sullivan
* Keegan Connor Tracy as Dawn / Molly
* Will Sanderson as Jim
* Michael Paré as Sheriff Harding
* Clint Howard as Greg / Motel Clerk
* Anthony Harrison as Tail Man / Dr. Kelly Matthew Walker as Pa Franklin
* Janet Wright as Ma Franklin
* Sean Campbell as Jack Franklin
* Ben Derrick as John Franklin
* Michael Eklund as Billy / Man
* Samantha Ferris as Waitress / Beth
* Patrick Dahlquist as Mrs. Sullivan

==Release==
The film was released direct-to-video on September 3, 2002, in North America.

==Critical reception==
Stephen Holden from the New York Times gave Blackwoods a positive review, describing it as "smarter and more diabolical than you could have guessed at the beginning." Lou Lumenick of the New York Post gave a more negative review, calling Blackwoods a "low rent, direct-to-video-caliber thriller." 
The film contains several Boll trademarks, including but not limited to, multiple flashbacks, repeated use of slow-motion, and a twist ending.
It currently has an 11% rotten rating on Rotten Tomatoes. Until Rampage (2009 film)|Rampage in 2009, this film was known as Uwe Bolls "best" film. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 