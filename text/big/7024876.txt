Hello, Dolly! (film)
 
{{Infobox film
| name           = Hello, Dolly!
| image          = HelloDollyFilmPoster.jpg
| image_size     = 215px
| alt            = 
| caption        = Original film poster by Richard Amsel 
| director       = Gene Kelly
| producer       = Ernest Lehman
| screenplay     = Ernest Lehman
| based on       =  
| starring       = Barbra Streisand Walter Matthau Michael Crawford Marianne McAndrew E. J. Peaker and Louis Armstrong
| music          = Jerry Herman
| cinematography = Harry Stradling Sr. William Reynolds
| studio         = Chenault Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 148 minutes
| country        = United States
| language       = English
| budget         = $25 million
| gross          = $26 million  )}}
}} romantic comedy Broadway production of the same name. The film follows the story of Dolly Levi (a strong-willed matchmaker), as she travels to Yonkers, New York, to find a match for the miserly "well-known unmarried half-a-millionaire" Horace Vandergelder. In doing so she convinces his niece, his nieces intended, and Horaces two clerks to travel to New York City.
 adapted and title tune Harry Stradling, Sr.

==Plot==
In 1890, all of New York City is excited because widowed, brassy Dolly Levi is in town ("Call On Dolly"). Dolly makes a living through matchmaking and numerous sidelines ("Just Leave Everything to Me"). She is currently seeking a wife for grumpy Horace Vandergelder, the well-known "half-a-millionaire", but it becomes clear that Dolly intends to marry Horace herself. Dolly travels to Yonkers, New York to visit Horace. Ambrose Kemper, a young artist, wants to marry Horaces weepy niece, Ermengarde, but Horace opposes this because Ambroses vocation does not guarantee a steady living. Horace, who is the owner of Vandergelders Hay and Feed, explains to his two clerks, Cornelius Hackl and Barnaby Tucker, that he is going to get married because "It Takes a Woman" to cheerfully do all the household chores. He plans to travel to New York City to propose to Irene Molloy, who owns a hat shop there. Dolly arrives in Yonkers and sends Horace ahead to the city. Before leaving he tells Cornelius and Barnaby to mind the store.

Cornelius decides that he and Barnaby need to get out of Yonkers. Dolly knows two ladies in New York they should call on: Irene Molloy and her shop assistant, Minnie Fay. She enters Ermengarde and Ambrose in the upcoming polka competition at the fancy Harmonia Gardens Restaurant in New York City, so Ambrose can demonstrate his ability to be a bread winner to Uncle Horace. Cornelius, Barnaby, Ambrose, Ermengarde and Dolly take the train to New York ("Put on Your Sunday Clothes"). Irene and Minnie open their hat shop for the afternoon. Irene does not love Horace Vandergelder and declares that she will wear an elaborate hat to impress a gentleman ("Ribbons Down My Back"). Cornelius and Barnaby arrive at the shop and pretend to be rich. Horace and Dolly arrive and Cornelius and Barnaby hide. Minnie screams when she finds Cornelius hiding in an armoire. Horace is about to open the armoire himself, but Dolly "searches" it and pronounces it empty. After hearing Cornelius sneeze, Horace storms out upon realizing there are men hiding in the shop, although he is unaware that they are his clerks. Dolly arranges for Cornelius and Barnaby, who are still pretending to be rich, to take the ladies out to dinner to the Harmonia Gardens to make up for their humiliation. She teaches Cornelius and Barnaby how to dance since they always have dancing at such establishments ("Dancing"). The clerks and the ladies go to watch the Fourteenth Street Association Parade together. Alone, Dolly asks her first husband Ephrams permission to marry Horace, requesting a sign. She resolves to move on with life ("Before the Parade Passes By"). After meeting an old friend, Gussie Granger, on a float in the parade, Dolly catches up with the annoyed Vandergelder as he is marching in the parade. She tells him the heiress Ernestina Semple (changed from the stage versions Ernestina Money) would be perfect for him and asks him to meet her at the Harmonia Gardens that evening.

Cornelius is determined to get a kiss before the night is over. Since the clerks have no money to hire a carriage, they tell the girls that walking to the restaurant shows that theyve got "Elegance". In a quiet flat, Dolly prepares for the evening ("Love is Only Love"). At the Harmonia Gardens Restaurant, Rudolph, the head waiter, whips his crew into shape for Dolly Levis return. Horace arrives to meet his date, who is really Dollys friend Gussie. As it turns out, she is not rich or elegant as Dolly implied, and she soon leaves after being bored by Horace, just as she and Dolly planned. 

Cornelius, Barnaby and their dates arrive and are unaware that Horace is also at the restaurant. Dolly makes her triumphant return to the Harmonia Gardens and is greeted in style by the staff ("Hello, Dolly! (song)|Hello, Dolly!"). She sits in the now-empty seat at Horaces table and proceeds to tell him that no matter what he says, she will not marry him. Fearful of being caught, Cornelius confesses to the ladies that he and Barnaby have no money, and Irene, who knew they were pretending all along, offers to pay for the meal. She then realizes that she left her handbag with all her money in it at home. The four try to sneak out during the polka contest, but Horace recognizes them and also spots Ermengarde and Ambrose. In the ensuing confrontation, Vandergelder fires Cornelius and Barnaby (although they claim to have already quit) and they are forced to flee as a riot breaks out. Cornelius professes his love for Irene because "It Only Takes a Moment". Horace declares that he wouldnt marry Dolly if she were the last woman in the world. Dolly angrily bids him farewell; while hes bored and lonely, shell be living the high life ("So Long, Dearie").

The next morning, back at the hay and feed store, Cornelius and Irene, Barnaby and Minnie, and Ambrose and Ermengarde each come to collect the money Vandergelder owes them. Chastened, he finally admits that he needs Dolly in his life, but she is unsure about the marriage until Ephram sends her a sign. Vandergelder spontaneously repeats a saying of Ephrams: "Money, pardon the expression, is like manure. Its not worth a thing unless its spread about, encouraging young things to grow." Cornelius becomes Horaces business partner at the store, and Barnaby fills in Cornelius old position. Horace tells Dolly life would be dull without her, and she promises that shell "never go away again" ("Finale").

==Cast==
 
* Barbra Streisand as Dolly Levi
* Walter Matthau as Horace Vandergelder
* Michael Crawford as Cornelius Hackl
* Marianne McAndrew as Irene Molloy
* E. J. Peaker as Minnie Fay
* Danny Lockin as Barnaby Tucker
* Joyce Ames as Ermengarde Vandergelder
* Tommy Tune as Ambrose Kemper
 
* Judy Knaiz as Gussie Granger/Ernestina Semple
* David Hurst as Rudolph Reisenweber
* Fritz Feld as Fritz, German waiter
* Richard Collier as Joe, Vandergelders barber
* J. Pat OMalley as Policeman in park
* Louis Armstrong as Orchestra leader
* Tucker Smith (uncredited) as Dancer
* Jennifer Gan (uncredited) as Miss Bolivia
 

==Musical numbers==
 
# "Call On Dolly"
# "Just Leave Everything To Me"
# "Main Titles (Overture)"
# "It Takes a Woman"
# "It Takes a Woman (Reprise)"
# "Put on Your Sunday Clothes"
# "Ribbons Down My Back"
# "Dancing"
# "Before the Parade Passes By"
 
# "Intermission"
# "Elegance"
# "Love is Only Love"
# "Hello, Dolly! (song)|Hello, Dolly!"
# "It Only Takes a Moment"
# "So Long, Dearie"
# "Finale"
# "End Credits"
 

==Production==

===Filming=== Pennsylvania Railroad 1223 (now located in the Railroad Museum of Pennsylvania) retrofitted to resemble a New York Central & Hudson River locomotive. The locomotive used in "Put on Your Sunday Clothes" was restored specifically for the film and can be seen at the Railroad Museum of Pennsylvania in Strasburg, Pennsylvania.

The Poughkeepsie (Metro-North station) trackside platform was used at the beginning when Dolly was on her way to Yonkers.

The church scene was filmed on the grounds of the United States Military Academy at West Point, New York, but the churchs facade was constructed only for the film. New York City scenes were filmed on the 20th Century-Fox lot in California. Some of the exteriors still exist.

===Music===
Most of the original Broadway productions score was preserved for the film; however, "Just Leave Everything to Me" and "Love Is Only Love" were not in the stage show. Jerry Herman wrote "Just Leave Everything to Me" especially for Streisand; it effectively replaced "I Put My Hand In" from the Broadway production. However, an instrumental version of parts of "I Put My Hand In" can be heard in the film during the dance competition at the Harmonia Gardens.    Herman had previously written "Love is Only Love" for the stage version of Mame (musical)|Mame, but it was cut before its Broadway premiere. It occurred in the story as Mame tried to explain falling in love to her young nephew Patrick. A brief prologue of "Mrs. Horace Vandergelder" was added to the song to integrate it into this film. Konder, George C. Hello, Dolly!: Original Motion Picture Soundtrack Album (CD Re-issue). Liner notes dated September 1994. Philips Records, 810 368-2 

Working under the musical direction of Lionel Newman and Lennie Hayton, the very large team of orchestrators included film stalwarts Herbert W. Spencer and Alexander Courage; the original Broadway production arranger, Philip J. Lang, making a rare film outing; and pop arrangers Don Costa and Frank Comstock. All of the actors did their own singing, except for Marianne McAndrew (Irene Molloy) whose singing was dubbed by Melissa Stafford and Gilda Maiken.  Choreography was by Michael Kidd.

==Release==

===US premieres=== New York wrapped more Fox negotiated and paid an "early release" escape payment to release the film "Dolly" at an estimated $1–2 million. 

===Critical reception===
Critical reaction was mixed. Vincent Canby in his New York Times review said that the producer and director "merely inflated the faults to elephantine proportions." 

===Box office=== theatrical rental (the distributors share of the box office after deducting the exhibitors cut)     of $15.2 million,       ranking it in the top five highest-grossing films of the 1969–1970 season.        In total, it earned $26 million in theatrical rentals for Fox,  against its $25.335 million production budget.  Despite performing well at the box office, it still lost its backers an estimated $10 million. 

===Home Media===
In April 2013, the movie was released in Blu-ray format.

===Awards and nominations===
;Academy Awards
The film won three Academy Awards and was nominated for another four.       
 
;Won Best Art George James Hopkins, and Raphael Bretton (Set Decoration) Best Music, Score of a Musical Picture Lennie Hayton and Lionel Newman Best Sound Jack Solomon and Murray Spivack
 
;Nominated Best Picture Ernest Lehman Best Cinematography Harry Stradling posthumous nomination) Best Costume Design Irene Sharaff Best Film Editing William H. Reynolds
 

;Other awards
* 23rd British Academy Film Awards 
** BAFTA Award for Best Actress in a Leading Role Barbra Streisand Nomination
** BAFTA Award for Best Actor in a Leading Role Walter Matthau Nomination (also for his role in The Secret Life of an American Wife) BAFTA Award for Best Art Direction John Decuir Nomination
** BAFTA Award for Best Cinematography Harry Stradling Nomination

* 27th Golden Globe Awards
** Golden Globe Award for Best Motion Picture - Musical or Comedy Nomination
** Golden Globe Award for Best Director Gene Kelly Nomination
** Golden Globe Award for Best Actress – Motion Picture Musical or Comedy Barbra Streisand Nomination
** Golden Globe Award for Best Supporting Actress – Motion Picture Marianne McAndrew Nomination
** Golden Globe Award for New Star of the Year – Actress Marianne McAndrew Nomination
* Directors Guild of America Awards 1970
** Outstanding Directorial Achievement in Feature Film Gene Kelly Nomination
* American Cinema Editors
** Best Edited Feature Film William H. Reynolds Won

==Cultural influence==
* Songs as well as footage from scenes "Put on Your Sunday Clothes" and "It Only Takes a Moment", were prominently featured in the 2008 Disney-Pixar film, WALL-E.

* The songs "Elegance" and "Put on Your Sunday Clothes" are heard through any day at the Main Street section of the Magic Kingdom in Walt Disney World

==See also==
* Hello, Dolly! (musical)|Hello, Dolly! stage play
* The Matchmaker (1958 film)|The Matchmaker
* WALL-E#Music|WALL-E

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 