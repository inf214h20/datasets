The Story of Dr. Wassell
{{Infobox film
| name           = The Story of Dr. Wassell
| image          = The Story of Dr. Wassell 1944 Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| writer         = 
| screenplay     = {{Plainlist|
* Alan Le May
* Charles Bennett
}}
| story          = {{Plainlist|
* Corydon M. Wassell
* James Hilton
}}
| starring       = {{Plainlist|
* Gary Cooper
* Laraine Day
* Signe Hasso
* Dennis OKeefe
* Carol Thurston 
}}
| music          = Victor Young
| cinematography = {{Plainlist|
* Victor Milner
* William E. Snyder
}}
| editing        = Anne Bauchens
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 140 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 James Hilton, his only nonfiction book.
 President Roosevelt in a radio broadcast made in April 1942. The appropriate section of this broadcast appears toward the end of the film.

For their work on this film, Farciot Edouart, Gordon Jennings and George Dutton received a nomination for the Academy Award for Best Visual Effects.   

== Cast ==
* Gary Cooper as Dr. Corydon M. Wassell
* Laraine Day as Madeleine
* Signe Hasso as Bettina
* Dennis OKeefe as Benjamin Hoppy Hopkins
* Carol Thurston as Tremartini 
* Carl Esmond as Lt. Dirk Van Daal Paul Kelly as Murdock
* Elliott Reid as William Andy Anderson
* Stanley Ridges as Cmdr. William B. Bill Goggins
* Renny McEvoy as Johnny Leeweather
* Oliver Thorndike as Alabam
* Philip Ahn as Ping
* Barbara Britton as Ruth Gavin Muir, Jack Norton and Philip Van Zandt

==Production==
It was originally announced that Yvonne de Carlo would play the role of the Javanese nurse. DRAMA: Cousin Rewrite Set; Hubbard Joining Cast
Schallert, Edwin. Los Angeles Times (1923-Current File)   18 Feb 1943: A8. 

De Mille wanted Alan Ladd to play the role of Hoppy but he had to go into military service. DRAMA AND FILM: OKeefe Wins Hoppy Role in Dr. Wassell Carmen Miranda Lively Addition to Greenwich Village at 20th
Schallert, Edwin. Los Angeles Times (1923-Current File)   23 June 1943: A8. 

==Reception==
The film was the seventh most popular film of the year released in Australia in 1945. 

It was the fifth most popular movie of 1946 in France with admissions of 5,866,693.http://translate.google.com.au/translate?hl=en&sl=fr&u=http://www.boxofficestory.com/&prev=search

==In popular culture==
 Breakfast at Tiffanys, Holly Golightly was to have auditioned for the role of Dr. Wassels nurse, but impulsively left for New York City.

==References==
 

==External links==
 
* 
* 
* , April 28, 1942

 

 
 
 
 
 
 
 
 
 


 