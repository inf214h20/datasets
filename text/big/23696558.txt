Ivar Vivahitharayal
{{Infobox film
| name           = Ivar Vivahitharayal ?
| image          = Ivar vivahitharayal poster.jpg
| alt            =  
| caption        = 
| director       = Saji Surendran
| producer       = S. Gopakumar
| writer         = Krishna Poojappura
| starring       = Jayasurya Bhama Samvrutha Sunil
| music          = M. Jayachandran
| cinematography = Anil Nair Manoj
| photographer   = Mahadevan Thampi
| distributor    = 
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Malayalam language comedy drama film starring Jayasurya, Bhama, and Samvrutha Sunil in leading roles.    The film is the directorial debut of Saji Surendran, who has been a television serial director. 

==Plot==
 MBA degree from Pondicherry University. Even while as a student, the main ambition of Vivek is to get married as soon as he is back home after his studies. Vivek has a circle of very close friends of four, with whom he discusses everything. Navya Nair appears in a cameo role in the film as his dream wife. Prominent among these friends is Teresa (Samvrutha Sunil). Teresa and other friends advises Vivek not to get married so early and to wait till he finds a job, but Vivek had already made up his mind.

Viveks parents, Adv.Ananthan Menon (Siddique (actor)|Siddique) and Adv.Nandini (Rekha (Tamil actress)|Rekha) lives separated at adjacent flats in the same apartment. Vivek returns home after studies. Vivek stays on each of his parents private apartments on alternate days. Vivek loves his parents so much and wishes to rejoin them somehow. The spoiled brat child, Vivek is called as Anathutten by his father Ananthan and Nanthutten by his mother Nandini.

Suraj Venjarammoodu plays the role of Adv.Mannanthala Susheel Kumar, an assistant advocate to Adv. Ananthan Menon.
 FM radio channel. Vivek expects Kavya to be an orthodox and simple wife of his dreams and Kavya turns out to be otherwise. Vivek discovers on the first night after marriage, that he is the same person that caused Kavya lose her job at the FM station. Vivek decides to reveal the truth to Kavya one day but he couldnt do it. Kavya gets rejected at other FM channels due her notorious happening at her previous FM station.

Vivek finds that he did not clear his MBA degree exam and subsequent quarrel with his father, leads to his way out from his fathers house. Vivek is now out of his own house without a job or money to survive. Teresa helps Vivek and Kavya to get a rented house next to Teresas house. Vivek finds difficult to do his responsibilities as an husband but he hates to accept that.

Kavya doubts Teresas over friendly attachment towards Vivek which results in quarrels among them. She is also worried of Viveks lack of responsibilities and sufficient care for her and her feelings. Kavya also discovers from Teresa that it was the because of Vivek, she had lost her earlier job. Kavya decides to leave for her own home, but promises to stay with him and Viveks parents who are reunited now.

After a month, Vivek and Kavya files a joint petition at the court for their divorce. Vivek plans to leave for Dubai, but Vivek and Kavya discovers their emotions towards each other at the end of the film and all is well now.

The movie had a dream run at the box office.

==Cast==

* Jayasurya    as Vivek

* Bhama  as Kavya

* Samvrutha Sunil as Treesa
 Siddique as Adv. Ananthan Menon
 Rekha as Adv. Nandini
 Ganesh Kumar as Freddy Uncles son Jeevan.

* Nedumudi Venu as Freddy Uncle

* Suraj Venjarammoodu as Adv. Mannanthala Susheel Kumar
 Devan as Treesas father

* Rehana as Annamma

* Kalaranjini as Kavyas mother

* Mallika Sukumaran as Treesas mother

* Anoop Menon as Ajay Menon, manager of Club FM channel (Special Appearance)
 Suresh Krishna as manager of MIF Group (Special Appearance)

* Babu Swamy

* Navya Nair As Herself (Special Appearance)

==Production==
Mahadevan Thampi is the still photographer of this movie.

==Soundtrack==

* "Enikku Padanoru" - Sainoj
* "Poomukha Vathilkal" - Vijay Yesudas
* "Sunday Sooryan" - Tippu (singer)|Tippu, Anand Narayan, Vipin Saviour, Sooraj, Charu Hariharan
* "Pazhmulam Thandil" - Ratheesh

==References==
 

==External links==
*  

 
 
 
 