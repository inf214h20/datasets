Her Final Role
{{Infobox film
| name =Her Final Role
| image =
| image_size =
| caption =
| director = Jean Gourguet
| producer = 
| writer =  Lajos Zilahy   (play)   Jean-Paul Le Chanois 
| narrator =
| starring = Gaby Morlay   Jean Debucourt   Marcel Dalio
| music =   René Sylviano 
| cinematography = 
| editing = 
| studio = Société Française de Production 
| distributor =Union Française de Production Cinématographique 
| released =   24 July 1946 
| runtime = 90 minutes
| country = France  French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Her Last Part or Her Final Role (French:Son dernier rôle) is a 1946 French drama film directed by Jean Gourguet and starring Gaby Morlay, Jean Debucourt and Marcel Dalio.  A top actress discovers that she is seriously ill.

==Cast==
*    Gaby Morlay as Hermine Wood  
* Jean Debucourt as Le professeur Mercier  
* Marcel Dalio as Ardouin  
* Jean Tissier as Lhôtelier 
* Georges Chamarat as Le suicidé 
* Germaine Charley  
* Paula Dehelly    Paul Demange  
* Gabrielle Fontan
* Germaine Ledoyen    
* Héléna Manson 
* Nina Myral   
* Roger Vincent

== References ==
 

== Bibliography ==
* Rège, Philippe. Encyclopedia of French Film Directors, Volume 1. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 
 

 