Naanayam
 
{{Infobox film
| name              = Naanayam
| image             = Naanayam.jpg
| caption           = 
| director          = Shakti S. Rajan
| producer          = S. P. B. Charan
| writer            = Shakti S. Rajan
| starring          =  
| music             = James Vasanthan  S. Thaman (Background Score) Omprakash  
| editing           = Praveen K. L. N. B. Srikanth
| studio            = Capital Film Works
| distributor       = 
| released          =  
| runtime           = 136 mins
| country           = India
| language          = Tamil
| budget            = 
| gross             = 
}}
 Tamil thriller thriller film Prasanna as the protagonist, Sibiraj as the antagonist   and Ramya Raj, who has appeared in the films Sandai and Thee (2009 film)|Thee,  debutante Yasmin and Charans father, singer S. P. Balasubrahmanyam in pivotal roles. The film, which has music scored by Subramaniapuram fame James Vasanthan, was released on 14 January 2010.

The film is adapted from James Hadley Chase novel My Laugh Comes Last and also filming partially inspired by the Hollywood films The Bank Job and Inside Man.

==Plot==
The plot opens with the introduction of Ravi (Prasanna) an electronics engineer,who introduces himself as a youthful guy who aspires to become a businessman. He meets Viswanath (S. P. Balasubrahmanyam) when he fights to grab a bag belonging to Viswanath from a stranger.Ravi chases the man but unable to catch him but gets a glimpse of the mans face. Viswanath offers Ravi a job in the Trust bank in which he is the CEO. The bank promotes its new branch as the Worlds safest bank with the security locker design of Ravi. Ravi also applies for a loan amount of Rs.2 Crores to start his own business.

Ravi meets a girl Nandhini (Ramya Raj), a divorced news reporter in a golf club and friendship,love develops step by step between them. Ravi once during his date with Nandhini is attacked by Nandhinis ex-husband Raghu. He counterattacks him and goes home. While in his home, he meets Fareed(Sibi Raj) who tells that Ravi killed Raghu and there are evidences to prove. Fareed also kidnaps Nandhini. Despite Ravi is confident he did not kill him he has no other go but to obey the words of helping them to rob the bank to save himself and Nandhini. Ravi and Fareed plan to rob the bank which has infra red,laser, four leveled lock system Vault. Meanwhile Ravi records the conversations going on between them and hands it over to his assistant Devaki to save himself. Ravi demands Rs.2 Crore, the photographic evidences of the murder from the robbers before the theft. Fareed initially gives it but takes it back and gives Ravi another bag similar to it.

As per the plans Fareed enter into the vault and after theft, attempts to kill Ravi but Ravi manages to escape by locking them inside the vault. He meets Viswanath where he also sees Nandhini with him. The robbery is actually a plan of Viswanath who did it in order to grab the bag which has evidences of his illegal affair with Nandhini actually named Eswari. The stranger who was chased by Ravi when he first met Viswanath was Eswaris ex-husband. The man safely places the evidence in Trust banks locker.  So to steal the evidence he plans a robbery with Fareed. The man who Ravi was told to be killed by him was actually a new employee in Fareeds gang. Viswanath apologises for the trouble created by him to Ravi and he telephones to surrender to the police but kills himself. Fareed,Eswari are arrested and all his gang members die inside the vault due to the lack of oxygen supply. Ravi destroys all the evidence of the illegal affair of Viswanath and finds Rs.2 Crore in the back side of his car which actually was demanded by him from Fareed and which he actually opt for to start his new business.

==Cast== Prasanna as Ravi
* Sibi Raj as Fareed
* S. P. Balasubrahmanyam as Viswanath
* Ramya Raj as Eswari/Nandhini
* Yasmin as Devaki Jasper as a henchman

==Production== Prasanna being signed on to play the lead role, whilst before the end of the year, Sibiraj and Ramya Raj also signed on to be a part of Charans project.   The film progressed quietly through 2008, with the subject being described as a "crime thriller".  The music for the film released in November 2009, whilst the film released the following January coinciding with the Tamil festival of Thai Pongal. 

==Soundtrack==
{{Infobox album|  
| Name = Naanayam
| Type = Soundtrack
| Artist = James Vasanthan
| Cover = 
| Released = 6 November 2009
| Recorded = 2009 Feature film soundtrack
| Length =  Tamil
| Sony Music
| Producer = James Vasanthan
| Reviews =  
| Last album = Yaathumagi (2009)
| This album = Naanayam (2009)
| Next album = Eesan (2010)
}} Silambarasan and music composer Devi Sri Prasad sung together for one song,  apart from the likes of S. P. Balasubrahmanyam, K. S. Chithra and Sunitha Sarathy.
{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 29.06
| lyrics_credits  = yes
| title1          = Naanayam (Version 1)
| lyrics1          = Dr. Krudhaya Ranjith
| length1         = 2:01
| title2          = Naan Pogiren
| lyrics2          = Thamarai
| extra2          = S. P. Balasubrahmanyam, K. S. Chithra
| length2         = 6:31
| title3          = Aasa Aasa
| lyrics3          = James Vasanthan
| extra3          = Kannan, Megha (singer)|Megha, Ramya, Sheba & Chorus
| length3         = 5:43
| title4          = Ka Ka Ka
| lyrics4          = Kavivarman
| extra4          = Silambarasan Rajendar|Silambarasan, Devi Sri Prasad
| length4         = 5:19
| title5          = Kooda Kooda
| lyrics5          = Yugabarathi
| extra5          = Sunitha Sarathy
| length5         = 5:55
| title6          = Naanayam (Version 2)
| lyrics6          = Dr. Krudhaya Ranjith
| length6         = 3:37
}}

==Release==

===Box Office===
The film opened in most centres across Chennai, Tamil Nadu to a good opening. The film which grossed Rs. 7,99,274 in the opening weekend in Chennai  The film did well in other states also and declared Hit.

===Reviews=== Prasanna is "sweet, savvy, intense and desperate as the situation demands" and that his diction "too is superb". It went on to claim that Sibiraj who makes more of an impact despite, that "he apes his esteemed father often, especially in tone of voice and modulation" but "he seems to have had a blast doing it". Ramya Raj has "more than one role to play, and does justice to it" whilst SPB plays his role with "élan".  In reference to the script Behindwoods criticized it by labelling that "the script employs countless twists and turns, although only a few of them really work&nbsp;– before which the damage of ruining it is already done", whilst also mentioning that "Naanayam also feels like it is inspired from a vaguely familiar Hollywood movie." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 