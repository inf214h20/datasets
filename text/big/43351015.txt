Muhurtham Pathnonnu Muppathinu
{{Infobox film
| name           = Muhurtham Pathnonnu Muppathinu
| image          =
| caption        =
| director       = Joshiy
| producer       =
| writer         =
| screenplay     =
| starring       = Mammootty Saritha Jagathy Sreekumar Ratheesh Shyam
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, directed by Joshiy. The film stars Mammootty, Saritha, Jagathy Sreekumar and Ratheesh in lead roles. The film had musical score by Shyam (composer)|Shyam.    

==Cast==
 
*Mammootty
*Saritha
*Jagathy Sreekumar
*Ratheesh
*V. D. Rajappan Shankar
*Prathapachandran Baby Shalini
*Kannur Sreelatha Kunchan
*Lalithasree
*Lalu Alex
*Paravoor Bharathan
*Surekha
 

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ayyayyo Ammavi || Vani Jairam, Krishnachandran || Poovachal Khader || 
|-
| 2 || Nishayude Thaazhvarayil || K. J. Yesudas, KS Chithra || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 