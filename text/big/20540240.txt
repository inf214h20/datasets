Martin Luther, Heretic (1983 film)
{{Infobox Film
| name           = Martin Luther, Heretic
| image          = 
| image_size     = 
| caption        = 
| director       = Norman Stone
| producer       = David M. Thompson William Nicholson
| narrator       =  John Nettleton Clive Swift Hugh Laurie
| music          = Roger Limb
| cinematography = Keith Hopper
| editing        = Pauline Dykes
| distributor    = British Broadcasting Corporation (BBC) Family Films Concordia Films
| released       =   November 8, 1983   November 10, 1983
| runtime        = 70 min
| country        = UK/US
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} dramatic troupes performances of mystery plays provide the unifying motif for a parallel telling of the story of the film. When Luthers carriage en route from the Wartburg to Wittenberg it is shown passing an actor wearing a devils mask. When this scene is revisited at the end of the film, the actor slips this mask off his face.

==Historical inconsistencies==
*Some  could interpret the portrayal of Martin Luther returning to Wittenberg, which occurred in early 1522, as his approving of the radical changes instigated by Andreas Karlstadt the year before. However, historically, Luther opposed some of the changes as well as the rashness of the changes he did approve of. The movie makes the out-break of the radical changes the instigation of his return thus implying, probably rightly, the need he felt to moderate them. He preached his Martin Luther#Return_to_Wittenberg|"Invocavit Sermons" against this radical reform in Wittenberg, and he had some of the changes rolled back.

==External links==
* 

 
 
 


 
 