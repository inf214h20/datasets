McKinley at Home, Canton, Ohio
{{Infobox film
| name = McKinley at Home, Canton, Ohio
| image = William McKinley at Home Canton Ohio 1896 1st United States President on film.webm
| image_size = 
| caption = 
| director = 
| producer = American Mutoscope and Biograph Company
| writer = 
| narrator = 
| starring = William McKinley Ida Saxton McKinley George Cortelyou|
| music = 
| cinematography = 
| editing = 
| distributor = 
| released = September 1896
| runtime = 
| country = United States English intertitles
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}} Republican nomination Ida can be seen in a rocking chair on the porch. McKinley is seen removing his hat and wiping his forehead with a handkerchief after receiving the news. It was filmed by a two man crew for American Mutoscope and Biograph Company on 68&nbsp;mm film, 60.02 m in length. McKinleys brother Abner and  former US president Benjamin Harrison were stockholders in the film company.

==See also==
*American Mutoscope and Biograph Company
*Silent films
*William McKinley
*Ida Saxton McKinley
*George Cortelyou

==External links==
*  
*   on YouTube

 
 
 
 
 
 


 