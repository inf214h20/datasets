Kasargod Khader Bhai
{{Infobox film
| name           = Kasargod Khader Bhai
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Thulasidas
| producer       = Changanassery Basheer
| story          = Kalabhavan Ansar
| screenplay     = Kaloor Dennis
| starring       = Jagadeesh, Siddique (actor)|Siddique, Sunitha (actress)|Sunitha, Zainuddin (actor)|Zainuddin, Ashokan (actor)|Ashokan, Innocent (actor)|Innocent, Alummodan, Babu Antony, Suchitra (actress)|Suchitra, Philomina Johnson
| cinematography = Saloo George
| editing        = G. Murali
| studio         = Simple Productions
| distributor    = 
| released       =  
| runtime        = 151 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}

Kasargod Khader Bhai is a 1992 Malayalam comedy film directed by Thulasidas and starring  Siddique (actor)|Siddique, Sunitha (actress)|Sunitha, Zainuddin (actor)|Zainuddin, Jagadeesh, Ashokan (actor)|Ashokan, Innocent (actor)|Innocent, Alummodan, Babu Antony, Philomina and Suchitra (actress)|Suchitra. The film is a sequel to 1991 film Mimics Parade. In 2010, it spawned a sequel titled Again Kasargod Khader Bhai.

==Plot==
The film is about how Kasargod Khader Bhai (Alummoodan) and his son Kasimbhai (Babu Antony) trying to take revenge on the mimicry artists who send Khader Bhai to jail (This is shown in the prequel).

==Cast== Siddique as Sabu Sunitha as Sandhya Cheriyan
* Jagadish as Unni Zainuddin as Nissam Ashokan as Jimmy
* Baiju as Manoj
* Ansar Kalabhavan as Anwar Mahesh as Jayan Saikumar as Sreenivasan Menon Innocent as Fr. Tharakkandam
* Mala Aravindan as Mammootty
* Philomina as Thandamma Suchitra as Latha
* Shankaradi as Pachalam Pappachan 
* Alummoodan as Kasargod Khader Bhai
* Babu Antony as Kasimbhai
* Mohanraj as Gunda
*Nadirsha
* Sivaji  as Frederik Cheriyan, Sandhyas brother
* Sadiq as Stephen Cheriyan, Sandhyas younger brother
* Prathapachandran as Cheriyan, Sandhyas father
* Kanakalatha as Lathas mother

==External links==
*  
*  
*   at the Malayalam Movie Database
*   at Cinemaofmalayalam.net

 
 
 
 


 
 