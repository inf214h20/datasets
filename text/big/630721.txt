Punch-Drunk Love
{{Infobox film
| name           = Punch-Drunk Love
| image          = Posterpdl.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Paul Thomas Anderson
| producer       = {{Plainlist|
* JoAnne Sellar
* Daniel Lupi
* Paul Thomas Anderson
}}
| writer         = Paul Thomas Anderson
| starring       = {{Plainlist|
* Adam Sandler
* Emily Watson
* Philip Seymour Hoffman
* Luis Guzmán
* Mary Lynn Rajskub
}}
| music          = Jon Brion
| cinematography = Robert Elswit Leslie Jones
| studio         = {{Plainlist|
* Revolution Studios
* New Line Cinema
* Mr. Madison 23 Productions
}}
| distributor    = {{Plainlist|
* New Line Cinema  
* Columbia Pictures  
}}
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $24.5 million 
}} romantic comedy-drama film written and directed by Paul Thomas Anderson  and starring Adam Sandler, Emily Watson, Mary Lynn Rajskub, Philip Seymour Hoffman and Luis Guzmán.

After the release of his previous film Magnolia (film)|Magnolia (which ran over three hours), Anderson stated that he would like to work with Adam Sandler in the future and that he was determined to make his next film ninety minutes long.    The film was produced by Revolution Studios and New Line Cinema, and distributed by Columbia Pictures; it features the video art of Jeremy Blake in the form of visual Entracte|interludes. Despite positive reviews, particularly for Sandlers first major departure from his trademark comedic roles, the film was a box office bomb.

==Plot==
  harmonium from the street, and encounters Lena Leonard (Emily Watson), a coworker of his sisters, Lena having orchestrated this meeting after seeing him in a family picture belonging to his sister Elizabeth (Mary Lynn Rajskub).
 frequent flyer buying large quantities of pudding. After Lena leaves for Hawaii on a business trip, Barry decides to follow her. He arrives and calls one of his manipulative sisters to learn where Lena is staying. When his sister starts abusing him again, Barry snaps and demands she give him the information, which she does. Lena is overjoyed to see Barry, and they later have sex. At first, Barry explains that he is in Hawaii on a business trip by coincidence, but he soon admits that he came only for her. The romance  develops further, and Barry finally feels some relief from the emotional isolation he has endured.

After they return home, the four brothers ram their car into Barrys, leaving Lena mildly injured. With his new-found freedom from loneliness in jeopardy, a surprisingly aggressive and poised Barry adeptly fights off all four of the goons in a matter of seconds. Suspecting that Lena will leave him if she finds out about the phone-sex fiasco, Barry leaves Lena at the hospital and tries to end the harassment by calling the phone-sex line back and speaking to the "supervisor", who turns out to be Dean Trumbell (Philip Seymour Hoffman), who is also the owner of a mattress store. Barry travels to the mattress store in Provo, Utah, to confront Dean face to face. Dean, at first trying to intimidate Barry, finds Barry much more intimidating and Barry compels Dean to leave him alone.

Barry decides to tell Lena about his phone-sex episode and begs her for forgiveness, pledging his loyalty and to use his frequent-flier miles to accompany her on all future business trips. She readily agrees, and they embrace happily. Lena approaches Barry in his office while he plays the harmonium. She puts her arms around him and says, "So, here we go."

==Main cast==
* Adam Sandler as Barry Egan
* Emily Watson as Lena Leonard
* Philip Seymour Hoffman as Dean Trumbell
* Mary Lynn Rajskub as Elizabeth Egan
* Luis Guzmán as Lance
* Robert Smigel as Walter the Dentist

==Reception==
 , Paul Thomas Anderson, Emily Watson and Philip Seymour Hoffman at Cannes in 2002]]
Punch-Drunk Love received generally positive reviews and has a rating of 79% on Rotten Tomatoes based on 184 reviews with an average rating of 7.4 out of 10. The consensus states "Odd, touching, and unique, Punch-Drunk Love is also delightfully funny, utilizing Adam Sandlers comic persona to explore the life of a lonely guy who finds love." The film also has a score of 78 out of 100 on Metacritic based on 37 reviews.  The movie came at #33 in the AV Clubs "Top 50 films of the 00s".  

Roger Ebert praised Sandlers performance in his review for the Chicago Sun-Times, saying, "Sandler, liberated from the constraints of formula, reveals unexpected depths as an actor. Watching this film, you can imagine him in Dennis Hopper roles. He has darkness, obsession and power. He cant go on making those moronic comedies forever, can he?"  Sandler went on to win Best Actor at the Gijón International Film Festival for his performance and was also nominated for the Golden Globe Award for Best Actor – Motion Picture Musical or Comedy.
    Best Director Grand Prix Belgian Syndicate of Cinema Critics.

Filmmakers Lee Unkrich and Judd Apatow have cited it as one of their favorite films.   Actor Bill Nighy has stated that this is one of his favorite films. 

==Box office== limited domestic release, beginning on October 11, 2002 and totaling $17,791,032 in box office receipts; an international box office of about $6,800,000 resulted in a worldwide box office of $24,591,032. 

==Awards and nominations==
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|-
|Gijón International Film Festival Award Best Actor Adam Sandler
| 
|- Golden Globe Award Golden Globe Best Actor - Motion Picture Musical or Comedy
| 
|- MTV Movie MTV Movie Award MTV Movie Best Kiss
| 
|- Emily Watson
| 
|- Toronto Film Toronto Film Critics Association Award Toronto Film Best Supporting Actress
| 
|- Vancouver Film Critics Circle Award Vancouver Film Best Supporting Actress
| 
|-
|}

==Score and soundtrack==
 
The score to Punch-Drunk Love was composed by Jon Brion. As with Magnolia (film)|Magnolia, Brion and director Paul Thomas Anderson collaborated heavily for the production of the films score. However, rather than scoring the film after rough footage had been shot, Brion made compositions during the filming of Punch-Drunk Love. During the scoring process, Brion would experiment with tones and sounds, carefully making note of what Anderson would respond to. Anderson himself would create the vocal tempos he would envision in the score and use them on set, even to the extent of inspiring the pace of Adam Sandlers performance.

The films score features heavy use of the Pump organ|harmonium, an instrument that Anderson knew he wanted in the film before he had even completed the script.  Brion introduced Anderson to this instrument and many scenes between Adam Sandlers character and the instrument were inspired by Brion. For instance, Brion once found a harmonium with a hole in its bellows before going on tour with Aimee Mann. To fix the problem, he covered the hole with duct tape. The situation is mirrored in the film.

==References==
{{reflist|colwidth=32em|refs=
   
}}

==External links==
 
*   The Numbers
*  
*  
*   from dailyscript.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 