Butley (film)
 
 play of Michael Byrne.

The title character, a literature professor and longtime T. S. Eliot scholar with a recently developed interest in Beatrix Potter, is a suicidal alcoholic, who loses his wife and his male lover on the same day. The dark comedy encompasses several hours in which he bullies students, friends, and colleagues, while falling apart at the seams. Apart from an opening sequence of Butley waking in his flat with a hangover and taking the Underground and occasional shots in the corridor and the pub at lunchtime, the entire film takes place in Butleys office.

In his introduction to the trade edition of the play, the films director Harold Pinter wrote:

 

The film lasts 130 minutes and was shot at Shepperton Studios. The Executive Producer was Otto Plaschkes and the cinematographer was Gerry Fisher.

==Cast==
*Alan Bates ...  Ben Butley 
*Jessica Tandy ...  Edna Shaft 
*Richard OCallaghan ...  Joey Keyston 
*Susan Engel ...  Anne Butley  Michael Byrne ...  Reg Nuttall 
*Georgina Hale ...  Miss Heasman 
*Simon Rouse ...  Mr. Gardner 
*John Savident ...  James 
*Oliver Maguire ...  Train Passenger 
*Colin Haigh ...  male student
*Darien Angadi ... male student

== References ==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 