Suburban Gothic (film)
{{Infobox film
| name           = Suburban Gothic
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Richard Bates Jr.
| producer       = 
| writer         = Richard Bates Jr. Mark Bruner
| screenplay     = 
| story          = 
| based on       =  
| starring       = Matthew Gray Gubler Kat Dennings Ray Wise
| narrator       = 
| music          = Michl Britsch
| cinematography = Lucas Lee Graham
| editing        = Steve Ansell Yvonne Valdez
| production companies = New Normal Films
| distributor    = 
| released       =   
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
Suburban Gothic is a 2014 horror film directed by Richard Bates Jr.. It had its world premiere on 19 July 2014 at the Fantasia International Film Festival and stars Matthew Gray Gubler as a young man who returns home only to find himself faced with the supernatural. 

==Synopsis==
Unable to find a job that utilizes his new MBA, Raymond (Matthew Gray Gubler) is forced to move back home to live with his parents. His mother Eve (Barbara Niven) is ecstatic about this and cant wait for him to arrive, while his father Donald (Ray Wise) believes Raymond to be a complete and utter disappointment and frequently berates him about this. To make matters worse, it seems that most of his high school classmates stayed in town and none of them are willing to let him forget that he was previously an overweight loser with a belief in the paranormal. Raymond does manage to connect with his former classmate Becca (Kat Dennings), who works as the local bartender. Things take a turn for the strange when the body of a young girl is unearthed in his parents backyard during some construction, which ends up awakening a spirit intent on harvesting the souls of everyone in the town.

==Cast==
*Matthew Gray Gubler as Raymond
*Kat Dennings as Becca
*Ray Wise as Donald
*Barbara Niven as Eve
*Muse Watson as Ambrose
*Sally Kirkland as Virginia
*Mel Rodriguez as Hector
*Jeffrey Combs as Dr. Carpenter
*John Waters as Cornelius
*Ronnie Gene Blevins as Pope
*Jack Plotnick as Cousin Freddy
*Ray Santiago as Alberto
*Shanola Hampton as Ticona
*Mackenzie Phillips as Mrs. Richards
*Jessica Camacho as Noelle

==Reception==
Critical reception for Suburban Gothic has been predominantly positive.   Frequent elements of praise centered upon the films acting and script,   which Film School Rejects cited as a highlight.  Shock Till You Drop gave a predominantly positive review, but commented that they would like to see Bates "drop the humor and go right for the throat with a scary haunted house film or supernatural thriller."  The Hollywood Reporter was more negative in their review, criticizing it for "  out of steam in its second half" and stating that the romance between Gubler and Dennings was unsatisfying and that "The perfunctory resolution to their would-be romance is no more gratifying than a climax in which both spirit-world drama and Raymonds domestic conflict are wrapped up in a single tidy event." 

At Screamfest 2014, Gubler was awarded with an award for Best Actor for his performance.

==References==
 

==External links==
*  
*  

 
 
 
 