Smart Alecks
{{Infobox film
| name           = Smart Alecks
| image_size     = 
| image	=	Smart Alecks FilmPoster.jpeg
| caption        = 
| director       = Wallace Fox Barney A. Sarecky (associate producer)
| writer         = Harvey Gates (original story and screenplay)
| narrator       = 
| starring       = East Side Kids
| music          = 
| cinematography = Mack Stengler
| editing        = Robert Golden
| distributor    = 
| studio         = Monogram Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Smart Alecks is a 1942 American film directed by Wallace Fox.

==Plot==
Hank Salko (Gabriel Dell), a member of the Eastside youth gang in New York, is initiated into the world of adult crime when two gangsters, Mike (Joe Kirk) and Butch Brocalli (Maxie Rosenbloom), hire him to stand watch while they rob a bank. When Hank tries to share some of his ill-gotten money with the East Side Kids, they suspend him from the club, and Hank is then arrested by Joe Reagan (Roger Pryor), the local policeman. 

While playing baseball in the street, the Kids accidentally send a ball crashing through the window of the apartment in which Butch and Mike are hiding. Butch emerges and refuses to give the ball back, so Danny (Bobby Jordan) runs after him. When Joe recognizes Butch, he chases him and Danny trips the criminal, who is then arrested along with Mike. Danny is awarded $200 for capturing the criminals and plans to buy baseball uniforms for his friends. They are unaware of his plans, however, and, thinking he is keeping the money to be selfish, steal it from him and ban him from the club. 

Dannys sister Ruth (Gale Storm) calls Joe, who is her boyfriend, and he has the Kids arrested for thievery. The arrest deepens the Kidss resentment of Danny, even though Danny insists that they be released. Once free, the Kids buy an old car with the $200. A month later, Hank breaks out of jail with Butch and Mike and warns Mugs (Leo Gorcey), the leader of the Eastside Kids, and the rest of the group that the gangsters are after Danny for getting them arrested. 

The gang responds immediately out of deeply rooted loyalty for their friend, but are too late and find him severely beaten in a warehouse, where Butch and Mike have left him. When they learn that only ace brain surgeon Ormsby (Walter Woolf King) can save their friend, they go to Ormsbys house and plead for their friends life, offering their beat-up jalopy as payment. Ormsby is touched by their concern and agrees to forego a conference in order to operate on Danny. The surgery is successful and Ormsby refrains from charging for it, but Danny does not rally to recover. 

Joe reveals to the East Side Kids Dannys real intentions for the $200, and the remorseful boys go to his bedside and, after inviting him back into the club, urge him to recover. Ruth is later taken hostage at her apartment by Butch and Mike, but the gang sneaks into the apartment and attacks the thugs. Hank is instrumental in knocking Mike out, and after the gangsters are arrested, Hank is released from his sentence. The East Side Kids reunite in Dannys hospital room with the new baseball uniforms that they bought after selling the car.

==Notes==
The film features appearances by Gale Storm and Marie Windsor.  

Stanley Clements is given an "Introducing" credit.  In the film he irritates Muggs by imitating him; Clements later replaced Leo Gorcey as leader of the gang when Gorcey left the series. Clements character inexplicably vanishes during moments of the film.

==Cast==

===The East Side Kids===
*Leo Gorcey as Muggs McGinnis
*Bobby Jordan as Danny Stevens
*Huntz Hall as Glimpy
*Gabriel Dell as Henry Hank Salko
*Stanley Clements as Stash
*Bobby Stone as Skinny Sammy Morrison as Scruno
*David Gorcey as Peewee

===Additional Cast===
*Maxie Rosenbloom as Butch Brocalli
*Gale Storm as Ruth Stevens Roger Pryor as Joe Reagan
*Herbert Rawlinson as Police Captain Bronson
*Walter Woolf King as Dr. Ormsby
*Sam Bernard as Dr. Thomas
*Dick Ryan as Prison Warden
*Joe Kirk as Mike
*Marie Windsor as a Nurse

== Soundtrack ==
*Huntz Hall and Ernest Morrison - "When You and I Were Young, Maggie" (Music by J.A. Butterfield, lyrics by George W. Johnson)

==Quotes==
Muggs McGinnis: Whered you just come from?  
Hank Salka: From the closet.  
Muggs McGinnis: What are ya doin in the closet?  
Glimpy: Hes got secrets.

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 