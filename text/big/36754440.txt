Seven Days' Leave (1930 film)
{{Infobox film
| name           = Seven Days Leave
| image          = Seven Days Leave 1930 Poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster Richard Wallace
| producer       = {{Plainlist|
* Louis D. Lighton
* Richard Wallace  
}}
| writer         = {{Plainlist|
* Richard H. Digges Jr.  
* John Farrow
* Dan Totheroh
}}
| based on       =  
| starring       = {{Plainlist|
* Gary Cooper
* Beryl Mercer
* Daisy Belmore
}}
| music          = Frank Terry
| cinematography = Charles Lang
| editing        = George Nichols Jr.
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes 9 reels, 7,534 ft
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Richard Wallace and starring Gary Cooper, Beryl Mercer, and Daisy Belmore. Based on the play The Old Lady Shows Her Medals by J. M. Barrie, the film is about a young Canadian soldier who is wounded while fighting in World War I. While recovering from his wounds in London, a YMCA worker tells him that a Scottish widow without a son believes that he is in fact her son. To comfort the widow, the soldier agrees to pretend to be her Scottish son. After fighting with British sailors who make fun of his kilts, he wants to desert, but moved by his mothers patriotism he returns to the war front and is killed in battle. Later the proud Scottish window receives the medals that her "son" was awarded for bravery. Produced by Louis D. Lighton and Richard Wallace for Paramount Pictures, the film was released on January 25, 1930 in the United States.   

==Cast==
* Gary Cooper as Kenneth Downey
* Beryl Mercer as Sarah Ann Dowey
* Daisy Belmore as Emma Mickelham
* Nora Cecil as Amelia Twymley
* Tempe Pigott as Mrs. Haggerty
* Arthur Hoyt as Mr. Willings
* Arthur Metcalfe as Colonel
* Basil Radford as Corporal
* Larry Steers as Aide-de-Camp 

==Critical response==
In his review for the New York Times, Mordaunt Hall described the film as a "sensitive production" that was "intensely interesting" and "tender, charming and whimsical".    Hall credits the films success to the direction of Richard Wallace and the performances of Beryl Mercer—reprising her role as the elderly charwoman in the original 1917 New York stage production—and the young Gary Cooper.
 
Hall praised Wallaces realistic depictions of London and the charwomen, and noted the Paramount audiences response of laughter and applause to several scenes. Hall also described the screen adaptation by John Farrow and Dan Totheroh as "a capital piece of work in blending the Barrie lines with scenes that were left to the imagination in the play". 

==Production==
The screenplay is based on the play The Old Lady Shows Her Medals by J. M. Barrie, which premiered in New York on May 14, 1917.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 