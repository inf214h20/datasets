Swagatham (1989 film)
{{Infobox film
| name = Swagatham
| image =
| caption =
| director = Venu Nagavally
| producer = K.T.Muhammad for Anand Movie Arts
| writer = Venu Nagavally
| screenplay = Venu Nagavally
| starring = Jayaram, Parvathy, Urvashi, Ashokan, Jagadheesh, Srinath, Nedumudi Venu, Sulakshana, Sukumari etc
| music = Rajamani
| cinematography = Vipin Mohan
| editing = Hariharaputhran
| studio = Anand Movie Arts
| distributor = Anand Movie Arts
| released =  
| country = India Malayalam
}}
 1989 Cinema Indian Malayalam Malayalam film, Ashokan and Parvathy in lead roles. The film had musical score by Rajamani.   

==Cast==
  
*Jayaram  Urvasi  Ashokan  Parvathy 
*Jagathy Sreekumar  Innocent 
*Nedumudi Venu 
*Adoor Pankajam 
*Bahadoor 
*Jagadish  Jagannathan 
*Sreenath 
 

==Soundtrack==
The music was composed by Rajamani.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Jagannathan || Bichu Thirumala || 
|- 
| 2 || Fifi Fifi || M. G. Sreekumar, Minmini, Pattanakkad Purushothaman || Bichu Thirumala || 
|- 
| 3 || Manjin Chirakulla || G. Venugopal, M. G. Sreekumar, Minmini || Bichu Thirumala || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 