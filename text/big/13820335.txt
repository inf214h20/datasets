Karagattakaran
{{Infobox Film |		
| name = 	Karakattakaran	
| image = Karagattakaran.jpg
| caption = 	Karakattakaran
| director = 	Gangai Amaran   
| producer = 	Karumari Kandasamy J. Durai
| writer = 	Gangai Amaran Kanaka  Chandrasekhar   Senthil   Kovai Sarala   Kanthimathi   Shanmuga Sundaram
| music = 	Ilaiyaraaja	
| cinematography = A. Sabapathy
| editing = 	 B. Lenin   V. T. Vijayan
| studio = Vijaya Movies
| distributor =  Vijaya Movies
| released =	 16 June 1989
| runtime = 	138 mins	
| country =	 India Tamil	
| budget =
| gross = 
| preceded_by = 		
| followed_by =	}}		
		 Tamil film Kanaka in lead roles while Goundamani, Senthil, Santhana Bharathi, Chandrasekhar (Tamil actor)|Chandrasekhar, Kanthimathi and Kovai Sarala play supporting roles. Its story is about a male Karagattam dancer who falls in love with a female Karagattam dancer who reciprocates his feelings, but unfortunate circumstances and their egoistic nature prevents them from confessing their love for one another. How they overcome forms the rest of the story. 

The soundtrack was composed by Ilayaraja and all the songs were well-received and, in particular, the song "Maanguyilae Poonguyile" has become a classic. The film was released in June 16, 1989 and was a blockbuster and completed a 425-day run at the box office

==Plot==
The story is a romantic comedy set in rural Tamil Nadu and revolves around a dance troupe who perform the Tamil dance form of karakattam.

==Cast==
* Ramarajan as Muththaiyaa Kanaka as Kamakshi
* Santhana Bharathi as Chinraasu Chandrasekhar as Kamakshis uncle
* Goundamani as Thangavelu Senthil as Naathas
* Kovai Sarala
* Shanmuga Sundaram
* Kanthimathi
* Kokila as Chandrasekhars wife
* Ragapriya

==Production==
Gangai Amaran revealed that he didn’t plan anything for the movie and went on scene by scene and also said that Ilayaraja had composed the music without knowing the plot.  Kanaka, daughter of late actress Devika made her acting debut with this film. 

==Soundtrack==
The music composed by Ilaiyaraaja.  All the songs especially "Maanguyile Poonguyile" became famous and still remain as chartbusters till date. The song "Ooru Vittu" is based on Shanmugapriya Raga while "Mariyamma" is based on Mayamalavagowla Raga.   

Ilayaraja later adapted "Maanguyile" as "Endhirayyo" for the Telugu film Shiva Shankar (2004).  The song "Nandhavanathil Oru" was adapted by Yuvan Shankar Raja as "Muttathu Pakkathile" in Kunguma Poovum Konjum Puravum (2009).  The song "Ooru Vittu Ooru Vandhu" was remixed by Natarajan Shankaran in Kappal (2014).  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Gangai Amaran || 04:35
|-
| 2 || Kudagu Malai || Mano (singer)|Mano, K. S. Chithra || 04:31
|-
| 3 || Maanguyilae (Men) || S. P. Balasubrahmanyam || 04:37
|-
| 4 || Maanguyilae (Duet) || S. P. Balasubrahmanyam, S. Janaki || 04:25
|-
| 5 || Mariyamma Mariyamma || Malaysia Vasudevan, K. S. Chithra || 04:31
|-
| 6 || Mundhi Mundhi || Mano (singer)|Mano, K. S. Chithra || 03:20
|-
| 7 || Nandhavanathil || Gangai Amaran || 01:05
|-
| 8 || Ooruvittu Ooruvanthu || Malaysia Vasudevan, Gangai Amaran || 04:34
|-
| 9 || Paattaalae Buddhi || Ilaiyaraaja || Ilaiyaraaja || 04:37
|}

==Themes==
{{multiple image
 
| align     = right
| direction = vertical
| footer    = Musicians (top) and Dancers (bottom) known for their contributions in Karagattam.
| width     = 
 
| image1    = Dance_musicians,Karakattam,TamilNadu355.jpeg
| width1    = 140
| alt1      = 
| caption1  = 
 
| image2    = Dancers,Karakattam,Tamil Nadu357.jpeg
| width2    = 140
| alt2      = 
| caption2  = 
}}
  (1968), this film showed the traditional arts of folk dance. http://www.behindwoods.com/tamil-movie-articles/movies-01/19-04-06-365-days-tamil-films.html   

==Release==
Distributors refused to buy the film as they felt that the rural setting and full length comedy would not be acceptable.  http://www.behindwoods.com/features/Slideshows/slideshows2/delayed_movies/tamil-cinema-karakattagaran.html  The film ran for one year in Natana Theatre at Madurai. 

==Reception==
Behindwoods wrote: "Gangai Amaran’s classy direction, a simple and neat story, good performance of Ramarajan and Kanaga, believable stunt scenes, Ilayaraja’s excellent songs and finally Goundamani, Senthil’s comedy made this movie a block buster". 

==Legacy==
Karagattakaran became cult film for bringing the art of "Karagam" into prominence.  The film became one of the successful films in the career of Ramarajan.  The comedy sequences from the film especially the joke revolving around Banana still remains as cult classic.   The success of the film prompted Gangai Amaran to direct Villu Pattukaran (1992) which featured more or less same cast and crew from the previous film.  The car Chevrolet Impala, 1960s model used in the film became popular after the films release.  In an interview to Hindu in 2002, Somasundaram, real-life Karagattam dancer was critical of the film stating that it is an "insult to the dance form". 

In an interview to a television channel, Venkat Prabhu was asked if he would ever remake Karakattakaran, he said that it would be extremely difficult to do justice to the original. Regarding the casting, in place of Goundamani and Senthil, he said he would prefer Santhanam and Premji respectively. 

==In popular culture==
In a comedy scene from Thangamana Raasa (1990), Goundamani who is jailed for petty crime, dreams of singing under the music of Ilaiyaraja, he sings "Maanguyile" to Vinu Chakravarthy.  In Saroja (film)|Saroja (2008), when the friends witness the car which they are going to travel, the theme music of Karagattakaran is used as background music for the scene.  Ooru Vittu Ooru Vanthu (1990) also directed by Gangai Amaran was named after a song from the film.  The scene where Shanmugasundaram pleads ignorance about himself to his sister (Gandhimathi) has been parodied by various mimicry artists in various shows.

==References==
 

==External links==
* 
* 

==Bibliography==
*  

 
 
 
 
 
 
 

 