Blind (2014 film)
 
{{Infobox film
| name           = Blind
| image          = Blind film.jpg
| caption        = Film poster
| director       = Eskil Vogt
| producer       = Sigve Endresen Hans-Jørgen Osnes
| writer         = Eskil Vogt
| starring       = 
| music          = Henk Hofstede
| cinematography = Thimios Bakatakis
| editing        = Jens Christian Fodstad
| studio         = Lemming Film Motlys
| distributor    = Cinéart
| released       =  
| runtime        = 96 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}
Blind is a 2014 Norwegian drama film written and directed by Eskil Vogt. The film premiered in-competition in the World Cinema Dramatic Competition at 2014 Sundance Film Festival on 19 January 2014.  Vogt received the Screenwriting Award for Blind at the 2014 Sundance Film Festival.  

The film was later screened in the Panorama section of the 64th Berlin International Film Festival.    The film was nominated for the 2014 Nordic Council Film Prize.  

==Plot==
Having recently lost her sight, Ingrid retreats to the safety of her home - a place where she can feel in control, alone with her husband and her thoughts. But Ingrids real problems lie within, not beyond the walls of her apartment, and her deepest fears and repressed fantasies soon take over.

==Cast==
* Ellen Dorrit Petersen as Ingrid
* Henrik Rafaelsen as Morten
* Vera Vitali as Elin

==Reception==
Blind received positive reviews upon its premiere at the 2014 Sundance Film Festival. Review aggregator Rotten Tomatoes reports that 100% of 20 film critics have given the film a positive review, with a rating average of 7.7 out of 10.  

Scott Foundas of Variety (magazine)|Variety, said in his review that "Ace Norwegian scribe Eskil Vogt makes a sparkling directorial debut with an alternately tragic and playful tale of a blind authoress."  Boyd van Hoeij in his review for The Hollywood Reporter called the film "An ambitiously constructed screenplay translates into a film thats easier to admire than to love."  William Bibbiani from CraveOnline praised the film by saying that "Blind exists as a nebulous construction, ever shifting but ultimately centered around a lovely and funny love-quadrangle with curious characters and consistent insight. The films curious blend of the sensual and the cerebral manages to engage even when you begin to lack confidence about whether anything is actually happening at all." 

==Accolades==
 .]]
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="3"| 2014 Sundance Film Festival
| World Cinema Grand Jury Prize: Dramatic
| Eskil Vogt 
|  
|-
| Screenwriting Award: World Cinema Dramatic
| Eskil Vogt 
|  
|- Berlin International Film Festival
| Label Europa Cinemas
| Eskil Vogt 
|   
|}
 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 