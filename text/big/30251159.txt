The Jade Mask
{{Infobox Film
| name           = The Jade Mask
| director       = Phil Rosen
| producer       = James S. Burkett
| writer         = Earl Derr Biggers (characters) George Callahan
| starring       = Sidney Toler
| music          = Dave Torbett
| cinematography = Harry Neumann
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| country        = United States
| language       = English
}}
The Jade Mask is a 1945 film featuring Sidney Toler as Charlie Chan and the only appearance of Number Four Son, Eddie Chan, played by Edwin Luke, the real-life younger brother of Keye Luke, who had depicted Number One Son all through the 1930s.

== Symopsis ==
Charlie Chan, along with #4 son Eddie and chauffeur, Birmingham Brown, looks into the apparent murder of an eccentric scientist in a spooky mansion.

== Cast ==
* Sidney Toler as Charlie Chan
* Edwin Luke as Eddie Chan
* Mantan Moreland as Birmingham Brown
* Hardie Albright as Walter Meeker
* Frank Reicher as Harper
* Janet Warren as Jean Kent
* Cyril Delevanti as Roth Alan Bridge as Sheriff Mack

==External links==
*  
*  

 

 
 
 
 
 
 
 

 