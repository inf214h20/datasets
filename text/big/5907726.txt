Oru Kalluriyin Kathai
{{Infobox film
| name = Oru Kalluriyin Kathai
| image = 
| director = Nandha Periyasamy
| writer = Nandha Periyasamy Arya Sonia Santhanam Charuhasan
| producer = Sakthi Sanghavi Mohana Sanghavi
| music = Yuvan Shankar Raja
| cinematography = Madhi (cinematographer)|R. Madhi
| editing = Kola Bhaskar
| studio = Chozha Creations
| distributor =
| released =  
| runtime = 156 minutes
| language = Tamil
| country = India
| budget =
}} Indian Tamil Tamil romantic Arya and Telugu and released as College Days in 2008.

==Plot==
Satya (Arya) is told to meet five friends at their old college at Valentines Day, when his dad comes in a van. Unfortunate Satya in coma. In a physcho test, conducted by doctor (Charuhasan), it turns out that he`s in love with Jothi (Sonia Agarwal) for five years, but never confesses it to her during college days. On the last day of college, his friends and he decided to meet there again after 5 years. But his friends are shocked to see him, his mind is not completely sound. What happen to Satya, does he become alright, and where does Jothi fit in all, this is what this movie is about..

==Cast== Arya as Satya
* Sonia Agarwal as Jothi
* Jaivarma as Chandru Santhanam
* Charuhasan as Doctor
* Pyramid Natarajan
* Mouli as Principal Charlie
* Sasi
* Raja
* Nizhalgal Ravi
* Thalaivasal Vijay as Taxi driver
* Manju
* Sai Madhavi

==Soundtrack==
{{Infobox album
| Name = Oru Kalluriyin Kathai
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Oru kalluriyin kathai cd cover.jpg
| Released =  
| Recorded = 2005 Feature film soundtrack
| Length = 32:06
| Label = Five Star
| Producer =  Yuvan Shankar Raja
| Reviews =
| Last album  = Daas (2005)
| This album  = Oru Kalluriyin Kathai (2005)
| Next album  = Kanda Naal Mudhal (2005)
}}
The music, including film score and soundtrack, was composed by Yuvan Shankar Raja. The soundtrack released on 4 August 2005 and features 8 tracks with lyrics penned by Na. Muthukumar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Notes
|- 1 || "Kadhal Enbathu" || Chinmayi, Harish Raghavendra || 5:25 ||
|- 2 || Karthik (singer)|Karthik || 4:54 ||
|- 3 || "Kangal Kandadhu" || KK (singer)|KK, Sujatha Mohan || 4:50 ||
|- 4 || Ranjith || 4:04 ||
|- 5 || "Unakku Endru Oruthi" || Unni Menon || 1:07 ||
|- 6 || "Dhalappa Kattuda" || Sriram || 3:03 ||
|- 7 || Ranjith || 4:48 ||
|- 8 || "Geetha Mela" || Devan, Ranjith (singer)|Ranjith, Sounder Rajan || 3:55 ||
|}

==Trivia==
*Named by Deepam TV as a top 10 film of 2005.

==External links==
*  
*   at Behindwoods

 
 
 
 
 
 