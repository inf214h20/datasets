The Gunslinger (1991 film)
The Gunslinger is a 1991 Western film written and directed by Willie George, based on his TV series The Gospel Bill Show. It features Kenneth Copeland in the title role of Robert "Wichita Slim" Owens, a former outlaw about to be christened a United States Marshal in the series main setting of Dry Gulch. 

==Plot==

Two years ago, Robert "Wichita Slim" Owens was released from prison after serving time for bank robbery in Dry Gulch. During his sentence, Owens accepted Jesus Christ as his Lord and Savior, remaining on "the right side of the law" ever since. On his way to be awarded a position with the United States Marshal Service, "Slim" (as many call him) crosses paths with his former outlaw partner Dawson McClure, now heading a new gang of his own. Escorting a preacher and his family to Dry Gulch, Owens is made a U.S. Marshal, only for the ceremony to be interrupted by McClures gang. Hit from behind after a confrontation with hotshot gunman "Lightning" Johnny Silvers, Slim begins suffering from amnesia, and returns to his outlaw ways. Made a Territorial Ranger by the Governor, "Gospel Bill" is granted 15 days to bring Owens back...dead or alive. 

==Cast==

*Kenneth Copeland as Robert "Wichita Slim" Owens
*Willie George as Dry Gulch sheriff and U.S. Marshal William "Gospel Bill" Gunter
*Ken Blount as Nicodemus
*Joey Smith as Dawson McClure
*Lana Osborn as Miss Lana
*Jeff Merrill as Elmer Barnes
*Rodney Lynch as Vester Holmes / Dry Gulch Prosecutor
*Markus Bishop as "Crazy" Stick Callahan
*Tony Leech as Charlie White Bear
*John Witty as T.W. Tutwater / Dry Gulch Doctor
*Bob Maras as Mitch
*John Copeland as "Lightning" Johnny Silvers 

==References==
 

 
 
 

 