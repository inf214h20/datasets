K-9 (film)
 K9}}
{{Infobox film
| name           = K-9
| image          = K nine.jpg
| caption        = Theatrical release poster
| producer       = {{plainlist| Lawrence Gordon
* Charles Gordon
}}
| director       = Rod Daniel
| writer         = {{plainlist|
* Steven Siegel
* Scott Myers
* Lloyd Levin
* Donna Smith
}}
| starring       = {{plainlist|
* James Belushi
* Jerry Lee
* Mel Harris
}}
| music          = Miles Goodman
| cinematography = Dean Semler
| editing        = Lois Freeman-Fox
| studio         = Gordon Company
| distributor    = Universal Studios
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $17 million  
| gross          = $78.2 million 
}} Lawrence Gordon and Charles Gordon, and released by Universal City Studios. 

Belushi plays bad-tempered   who works only when and how he wants to. Many of the movies gags revolve around Jerry Lees playfully destructive episodes. 

The film has two   (2002), both being direct-to-video.

==Plot==
 
San Diego Police Detective Michael Dooley leaves his car to contact his girlfriend, Tracy, when a helicopter suddenly appears and opens fire on his car, which ignites.  Presuming him dead, the assassins leave the scene.  At the police station, Dooley argues with his captain, refusing to take a partner; instead, he decides to get a police dog.  At home, he finds Tracy with another man and spends the night in his new car.  The next day, Dooley coerces Freddie, an informant, into revealing that the drug lord Ken Lyman is responsible for the attack.  Dooley is assigned a German Shepherd named Jerry Lee,    whom he dislikes.

Dooley and Jerry Lee head to a warehouse assumed to be filled with drugs.  When Jerry Lee does not follow Dooleys orders, the workers laugh at him.  Dooley is forced to leave after Jerry Lee finds only a cigarette when commanded to find drugs.  The duo drive to a pub where Dooley stakes out Benny the Mule in an attempt to charge Lyman.  When his cover is blown, Jerry Lee saves Dooley from a beating; with the dogs help, Dooley subdues Benny and learns the location of Lymans next shipment.  Meanwhile, Lyman kills Freddie and demands that his henchman Dillon kill Dooley before the shipment arrives.

At Dooleys apartment, Jerry Lee steals the spotlight after Tracy accepts Dooleys story that he rescued the dog from the streets.  The next day, Dooley and Jerry Lee bond when they eat together and spy on Lyman.  The two are nearly killed when someone shoots at them, and the two chase the assailant to an empty building.  Jerry Lee leads Dooley to the man, who falls to his death after a fistfight with Dooley.  In the mans car, Dooley finds a clue that leads him to an auto-dealer shop.  There, Jerry Lee identifies a red Mercedes owned by Lyman, and Dooley learns from Halstead, the owner of the dealership, that he works for Lyman.  Later, Jerry Lee falls in love with a poodle to the disapproval of its owner.

When Dooley returns home, he discovers Lyman has kidnapped Tracy.  Infuriated, Dooley crashes a party  at Lymans mansion and demands her return.  Lyman pretends to know nothing, and Dooley is arrested by an officer from his own department and put in a squad car.  Angry, Dooleys captain calls him crazy.  When Jerry Lees flatulence annoys the other officers, Dooley uses it to his advantage and escapes with the dog.  As Dooley tells Jerry Lee how he met Tracy, he spots a truck driven by Halstead that is pulling a trailer with Lymans Mercedes.  Dooley purses the truck, and Halstead blows a tire.  After Halstead shoots at Dooley, Jerry Lee kills Halstead.

Meanwhile, in a stranded desert in San Diego, Lyman holds Tracy hostage in his limo and becomes suspicious when Halstead is late.  Dooley arrives with the truck and trailer, which is revealed to be the next shipment of drugs.  Not worrying about the case anymore, Dooley orders Lyman to surrender his girlfriend to him, or he will blow up the truck.  Lyman calls Dooleys bluff, and a shootout ensues.  Dooley kills Lymans henchmen, and Jerry Lee chases Lyman as he runs for his helicopter.  Unable to outrun the dog, Lyman shoots Jerry Lee; enraged, Dooley shoots and kills Lyman.  Dooley and Tracy rush Jerry Lee to a hospital, where the reluctant surgeon operates.  In the recovery room, Dooley Dooley delivers a eulogy to Jerry Lee, not knowing that he is alive.  When the surgeon tells him he is going to be fine, Dooley responds in anger, thinking he was speaking to a dead dog.  Jerry Lee licks Dooleys face out of love, making him give in.

To take a break from police work, Dooley, Tracy, Jerry Lee, and a poodle spend a vacation together in Las Vegas.

==Cast==
* James Belushi as Detective Michael Dooley
* Mel Harris as Tracy
* Kevin Tighe as Lyman
* Ed ONeill as Police dog|K-9 Sergeant|Sgt. Brannigan
* Jerry Lee as Himself
* James Handy as Lieutenant|Lt. Byers
* Sherman Howard as Dillon
* Daniel Davis as Halstead
* Cotter Smith as Gilliam John Snyder as Freddie
* Pruitt Taylor Vince as Benny the Mule
* Jeff Allin as Chad
* David Haskell as Doctor
* Alan Blumenfeld as Rental Salesman
* Colleen Morris as Rolls Blonde
* Wendel Meldrum as Woman with obedient dog  William Sadler (credited as Bill Sadler) as Salesman Don 
* Dan Castallaneta as Maître dhôtel (cameo appearance)
 backups and stand-ins.  

==Reception==
Kevin Thomas of the Los Angeles Times praised the actors but not the routine plot. Thomas wrote, "Its enjoyable, thanks not only to its charismatic duo, but also to the skilled comedy direction of Rod Daniel."  Additionally, Stephen Holden of The New York Times gave the film a mixed review, stating it had "no shred of credibility", yet contains "cutesy, surefire dog tricks" and a "breezy pacing".  Roger Ebert of the Chicago Sun-Times gave the film "2 stars".  Rita Kempley of The Washington Post complimented Jerry Lees performance.  

K-9 currently holds a 22% rating on Rotten Tomatoes based on nine reviews; the average rating is 4.1/10. 

Although K-9 was released previously (exactly three months earlier), Turner & Hooch (with Tom Hanks) became more popular and seemingly overshadowed this films greater box office success, even though they had very similar plots.

==Soundtrack== Theme from Jaws)"
Music composed by John Williams / Courtesy of MCA Records

*"Iko Iko"
Written by Barbara Ann Hawkins (as Hawkins), Joe Jones (as Jones), Rosa Lee Hawkins (as Hawkins), 
Marilyn Jones (as Johnson), Sharon Jones (as Jones) and Jessie Thomas (as Thomas) of The Dixie Cups / Performed by Amy Holland

*"I Got You (I Feel Good)"
Written by James Brown / Performed by James Brown / Courtesy of PolyGram Special Products, a division of
PolyGram Records, Inc.
 Oh Yeah" Boris Blank and Dieter Meier / Performed by Yello / Courtesy of PolyGram Special Products, a division of
PolyGram Records, Inc.
 Car Wash"
Written by Norman Whitfield / Performed by Rose Royce / Courtesy of MCA Records

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 